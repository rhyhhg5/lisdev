package com.sinosoft.lis.wiitb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title:团单整单删除UI层 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: sinosoft</p>
 * @author zhangrong
 * @version 1.0
 */
public class BriGrpContDeleteData {
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();
    
    private String mOperate = "";

    /** 数据操作字符串 */
    private String mOperator;
    private String mManageCom;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /**业务处理相关变量*/
    private GlobalInput mGlobalInput = new GlobalInput();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LCDelPolLogSchema mLCDelPolLog = new LCDelPolLogSchema();

    //统一更新日期，时间
    private String theCurrentDate = PubFun.getCurrentDate();
    private String theCurrentTime = PubFun.getCurrentTime();

    public BriGrpContDeleteData() {
    }

    /**
     * 处理实际的业务逻辑。
     * @param cInputData VData   从前台接收的表单数据
     * @param cOperate String    操作字符串
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将数据取到本类变量中
        mInputData = (VData) cInputData.clone();
        
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }
    	
        return true;
    }
    private MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData, cOperate))
        {
            return null;
        }

        if (!checkData())
        {
            return null;
        }

        if (!dealData())
        {
            return null;
        }

        return mMap;
    }
    
    private boolean getInputData(VData cInputData, String cOperate)
    {
    	mOperate =  cOperate;
    	
    	//全局变量实例
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mGlobalInput == null) {
            mErrors.addOneError(new CError("没有得到全局量信息"));

            return false;
        }

        mOperator = mGlobalInput.Operator;
        mManageCom = mGlobalInput.ManageCom;

        //团体保单实例
        mLCGrpContSchema.setSchema((LCGrpContSchema) mInputData.
                                   getObjectByObjectName("LCGrpContSchema", 0));

        if (mLCGrpContSchema == null) {
            this.mErrors.addOneError(new CError("传入的团单信息为空！"));

            return false;
        }

        System.out.println("完成数据解析 结果为 " + mLCGrpContSchema.getPrtNo() + "  " +
                           mLCGrpContSchema.getGrpContNo());
        return true;
    }
    private boolean checkData()
    {
        return true;
    }
    private boolean dealData()
    {
//    	获取删除保单数据
    	MMap delGrpContMMap = dealGrpContData();
    	if(delGrpContMMap == null){
    		buildError("dealData","获取待删除保单数据失败！");
    	}
    	mMap.add(delGrpContMMap);
    	
        return true;
    }

    /**
     * 对业务数据进行加工
     * 对于新增的操作，这里需要有生成新合同号和新客户号的操作。
     * @return boolean
     */
    private MMap dealGrpContData() {
    	
    	MMap GrpContMMap = new MMap();
        
        String tGrpContNo = mLCGrpContSchema.getGrpContNo();
        String tPrtNo = mLCGrpContSchema.getPrtNo();
        if(tGrpContNo == null || "".equals(tGrpContNo)){
        	this.mErrors.addOneError(new CError("新单删除时，团单号码为空！"));
            return null;
        }
        if(tPrtNo == null || "".equals(tPrtNo)){
        	buildError("dealGrpContData","新单删除时，印刷号为空！");
            return null;
        }
        String tSql = "";
        mLCDelPolLog.setOtherNo(StrTool.cTrim(tGrpContNo));
        mLCDelPolLog.setOtherNoType("0");
        mLCDelPolLog.setPrtNo(mLCGrpContSchema.getPrtNo());
        mLCDelPolLog.setIsPolFlag(mLCGrpContSchema.getAppFlag());
        mLCDelPolLog.setOperator(mOperator);
        mLCDelPolLog.setManageCom(mManageCom);
        mLCDelPolLog.setMakeDate(theCurrentDate);
        mLCDelPolLog.setMakeTime(theCurrentTime);
        mLCDelPolLog.setModifyDate(theCurrentDate);
        mLCDelPolLog.setModifyTime(theCurrentTime);

        Reflections tReflections = new Reflections();
        
        tSql = "select * from lccont where grpcontno = '"+tGrpContNo+"'";
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = tLCContDB.executeQuery(tSql);
//    	if(tLCContSet == null || tLCContSet.size() <=0){
//    		this.mErrors.addOneError(new CError("获取保单数据失败！"));
//            return null;
//    	}
        
        for(int i=1;i<=tLCContSet.size();i++){
        	
        	LCContSchema tLCContSchema = tLCContSet.get(i);
        	BriContDeleteData tBriContDeleteDate = new BriContDeleteData();
        	VData tVData = new VData();
    		tVData.add(tLCContSchema);
    		tVData.add(mGlobalInput);
        	if (!tBriContDeleteDate.submitData(tVData, ""))
    		{
    			// @@错误处理
    			this.mErrors.copyAllErrors(tBriContDeleteDate.mErrors);
    			return null;
    		}else{
    			MMap contMMap = tBriContDeleteDate.getMMap();
    			if(contMMap == null){
    				this.mErrors.addOneError(new CError("获取保单待删除数据失败！"));
                    return null;
    			}
    			GrpContMMap.add(contMMap);
    		}
        }
        
        //删除被保人列表
        GrpContMMap.put("delete from LCInsuredList where GrpContNo = '" + tGrpContNo + "'", "DELETE");
        
        LCContPlanDB tLCContPlanDB = new LCContPlanDB();
//        tLCContPlanDB.setGrpContNo(tGrpContNo);
        tSql = "select * from LCContPlan where grpcontno = '"+tGrpContNo+"' ";
        LCContPlanSet tLCContPlanSet = tLCContPlanDB.executeQuery(tSql);        
        LOBContPlanSet tLOBContPlanSet = new LOBContPlanSet();
        LOBContPlanSchema tLOBContPlanSchema = new LOBContPlanSchema();
        tLOBContPlanSet.add(tLOBContPlanSchema);
        tReflections.transFields(tLOBContPlanSet,tLCContPlanSet);        
        GrpContMMap.put(tLOBContPlanSet, SysConst.DELETE_AND_INSERT);
        GrpContMMap.put(tLCContPlanSet, SysConst.DELETE);
        
        LCContPlanDutyParamDB tLCContPlanDutyParamDB = new LCContPlanDutyParamDB();
//        tLCContPlanDutyParamDB.setGrpContNo(tGrpContNo);
        tSql = "select * from LCContPlanDutyParam where grpcontno = '"+tGrpContNo+"' ";
        LCContPlanDutyParamSet tLCContPlanDutyParamSet = tLCContPlanDutyParamDB.executeQuery(tSql);        
        LOBContPlanDutyParamSet tLOBContPlanDutyParamSet = new LOBContPlanDutyParamSet();
        LOBContPlanDutyParamSchema tLOBContPlanDutyParamSchema = new LOBContPlanDutyParamSchema();
        tLOBContPlanDutyParamSet.add(tLOBContPlanDutyParamSchema);
        tReflections.transFields(tLOBContPlanDutyParamSet,tLCContPlanDutyParamSet);        
        GrpContMMap.put(tLOBContPlanDutyParamSet, SysConst.DELETE_AND_INSERT);
        GrpContMMap.put(tLCContPlanDutyParamSet, SysConst.DELETE);
        
        LCContPlanRiskDB tLCContPlanRiskDB = new LCContPlanRiskDB();
//        tLCContPlanRiskDB.setGrpContNo(tGrpContNo);
        tSql = "select * from LCContPlanRisk where grpcontno = '"+tGrpContNo+"' ";
        LCContPlanRiskSet tLCContPlanRiskSet = tLCContPlanRiskDB.executeQuery(tSql);        
        LOBContPlanRiskSet tLOBContPlanRiskSet = new LOBContPlanRiskSet();
        LOBContPlanRiskSchema tLOBContPlanRiskSchema = new LOBContPlanRiskSchema();
        tLOBContPlanRiskSet.add(tLOBContPlanRiskSchema);
        tReflections.transFields(tLOBContPlanRiskSet,tLCContPlanRiskSet);        
        GrpContMMap.put(tLOBContPlanRiskSet, SysConst.DELETE_AND_INSERT);
        GrpContMMap.put(tLCContPlanRiskSet, SysConst.DELETE);
        
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
//        tLCGrpAppntDB.setGrpContNo(tGrpContNo);
        tSql = "select * from LCGrpAppnt where grpcontno = '"+tGrpContNo+"' ";
        LCGrpAppntSet tLCGrpAppntSet = tLCGrpAppntDB.executeQuery(tSql);        
        LOBGrpAppntSet tLOBGrpAppntSet = new LOBGrpAppntSet();
        LOBGrpAppntSchema tLOBGrpAppntSchema = new LOBGrpAppntSchema();
        tLOBGrpAppntSet.add(tLOBGrpAppntSchema);
        tReflections.transFields(tLOBGrpAppntSet,tLCGrpAppntSet);        
        GrpContMMap.put(tLOBGrpAppntSet, SysConst.DELETE_AND_INSERT);
        GrpContMMap.put(tLCGrpAppntSet, SysConst.DELETE);
        
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
//        tLCGrpPolDB.setGrpContNo(tGrpContNo);
        tSql = "select * from LCGrpPol where grpcontno = '"+tGrpContNo+"' ";
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.executeQuery(tSql);        
        LOBGrpPolSet tLOBGrpPolSet = new LOBGrpPolSet();
        LOBGrpPolSchema tLOBGrpPolSchema = new LOBGrpPolSchema();
        tLOBGrpPolSet.add(tLOBGrpPolSchema);
        tReflections.transFields(tLOBGrpPolSet,tLCGrpPolSet);        
        GrpContMMap.put(tLOBGrpPolSet, SysConst.DELETE_AND_INSERT);
        GrpContMMap.put(tLCGrpPolSet, SysConst.DELETE);
        
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
//        tLCGrpContDB.setGrpContNo(tGrpContNo);
        tSql = "select * from LCGrpCont where grpcontno = '"+tGrpContNo+"' ";
        LCGrpContSet tLCGrpContSet = tLCGrpContDB.executeQuery(tSql);        
        LOBGrpContSet tLOBGrpContSet = new LOBGrpContSet();
        LOBGrpContSchema tLOBGrpContSchema = new LOBGrpContSchema();
        tLOBGrpContSet.add(tLOBGrpContSchema);
        tReflections.transFields(tLOBGrpContSet,tLCGrpContSet);        
        GrpContMMap.put(tLOBGrpContSet, SysConst.DELETE_AND_INSERT);
        GrpContMMap.put(tLCGrpContSet, SysConst.DELETE);
        
        
        LCGrpFeeDB tLCGrpFeeDB = new LCGrpFeeDB();
//        tLCGrpFeeDB.setGrpContNo(tGrpContNo);
        tSql = "select * from LCGrpFee where grpcontno = '"+tGrpContNo+"' ";
        LCGrpFeeSet tLCGrpFeeSet = tLCGrpFeeDB.executeQuery(tSql);        
        LOBGrpFeeSet tLOBGrpFeeSet = new LOBGrpFeeSet();
        LOBGrpFeeSchema tLOBGrpFeeSchema = new LOBGrpFeeSchema();
        tLOBGrpFeeSet.add(tLOBGrpFeeSchema);
        tReflections.transFields(tLOBGrpFeeSet,tLCGrpFeeSet);        
        GrpContMMap.put(tLOBGrpFeeSet, SysConst.DELETE_AND_INSERT);
        GrpContMMap.put(tLCGrpFeeSet, SysConst.DELETE);
        
//        扫描件
        if(!"UpGrpDelete".equals(mOperate)){
        	ES_DOC_MAINDB tES_DOC_MAINDB = new ES_DOC_MAINDB();
//          tES_DOC_MAINDB.setDocCode(tPrtNo);
          tSql = "select * from ES_DOC_MAIN where DocCode = '"+tPrtNo+"' ";
          ES_DOC_MAINSet tES_DOC_MAINSet = tES_DOC_MAINDB.executeQuery(tSql);
          if(tES_DOC_MAINSet != null && tES_DOC_MAINSet.size()>0){
          	
          	ES_DOC_PAGESSet tES_DOC_PAGESSet = new ES_DOC_PAGESSet();
          	ES_DOC_RELATIONSet tES_DOC_RELATIONSet = new ES_DOC_RELATIONSet();
          	
          	for(int i=1;i<=tES_DOC_MAINSet.size();i++){
          		double tDocid = tES_DOC_MAINSet.get(i).getDocID();
          		
          		ES_DOC_PAGESDB tES_DOC_PAGESDB = new ES_DOC_PAGESDB();
//                  tES_DOC_PAGESDB.setDocID(tDocid);
          		tSql = "select * from ES_DOC_PAGES where DocID = "+tDocid+" ";
                  ES_DOC_PAGESSet tempES_DOC_PAGESSet = tES_DOC_PAGESDB.executeQuery(tSql);        
                  tES_DOC_PAGESSet.add(tempES_DOC_PAGESSet);
                  
                  ES_DOC_RELATIONDB tES_DOC_RELATIONDB = new ES_DOC_RELATIONDB();
//                  tES_DOC_RELATIONDB.setDocID(tDocid);
                  tSql = "select * from ES_DOC_RELATION where DocID = "+tDocid+" ";
                  ES_DOC_RELATIONSet tempES_DOC_RELATIONSet = tES_DOC_RELATIONDB.executeQuery(tSql);   
                  tES_DOC_RELATIONSet.add(tempES_DOC_RELATIONSet);
          		
          	}
          	
          	ES_DOC_PAGESBSet tES_DOC_PAGESBSet = new ES_DOC_PAGESBSet();
          	ES_DOC_PAGESBSchema tES_DOC_PAGESBSchema = new ES_DOC_PAGESBSchema();
          	tES_DOC_PAGESBSet.add(tES_DOC_PAGESBSchema);
          	tReflections.transFields(tES_DOC_PAGESBSet,tES_DOC_PAGESSet);
          	GrpContMMap.put(tES_DOC_PAGESBSet, SysConst.DELETE_AND_INSERT);
          	GrpContMMap.put(tES_DOC_PAGESSet, SysConst.DELETE);
          	
          	ES_DOC_RELATIONBSet tES_DOC_RELATIONBSet = new ES_DOC_RELATIONBSet();
          	ES_DOC_RELATIONBSchema tES_DOC_RELATIONBSchema = new ES_DOC_RELATIONBSchema();
          	tES_DOC_RELATIONBSet.add(tES_DOC_RELATIONBSchema);
          	tReflections.transFields(tES_DOC_RELATIONBSet,tES_DOC_RELATIONSet);
          	GrpContMMap.put(tES_DOC_RELATIONBSet, SysConst.DELETE_AND_INSERT);
          	GrpContMMap.put(tES_DOC_RELATIONSet, SysConst.DELETE);
          	
          	ES_DOC_MAINBSet tES_DOC_MAINBSet = new ES_DOC_MAINBSet();
          	ES_DOC_MAINBSchema tES_DOC_MAINBSchema = new ES_DOC_MAINBSchema();
          	tES_DOC_MAINBSet.add(tES_DOC_MAINBSchema);
          	tReflections.transFields(tES_DOC_MAINBSet,tES_DOC_MAINSet);
          	GrpContMMap.put(tES_DOC_MAINBSet, SysConst.DELETE_AND_INSERT);
          	GrpContMMap.put(tES_DOC_MAINSet, SysConst.DELETE);
              
          }
        }
        
//        工作流
        String strSql = "select missionid from lwmission where (MissionProp1 = '" +
        tPrtNo + "' or MissionProp2 = '" + tPrtNo + "' or MissionProp3 = '" + tPrtNo + "' or MissionProp5 = '" + tPrtNo + "')";
        SSRS tMissionidSSRS = new ExeSQL().execSQL(strSql);
        
        if(tMissionidSSRS != null &&  tMissionidSSRS.MaxRow>0){
        	for(int i=1;i<=tMissionidSSRS.MaxRow;i++){
        		String tMissionid = tMissionidSSRS.GetText(i, 1);
        		if(tMissionid != null && !"".equals(tMissionid)){
                	tSql = "select * from lwmission where missionid = '"+tMissionid+"' ";
                	LWMissionDB tLWMissionDB = new LWMissionDB();
                	LWMissionSet tLWMissionSet = tLWMissionDB.executeQuery(tSql);
                	LBMissionSet tLBMissionSet = new LBMissionSet();
                	LBMissionSchema tLBMissionSchema = new LBMissionSchema();
                	tLBMissionSet.add(tLBMissionSchema);
                    tReflections.transFields(tLBMissionSet,tLWMissionSet); 
                    GrpContMMap.put(tLBMissionSet, SysConst.DELETE_AND_INSERT);
                    GrpContMMap.put(tLWMissionSet, SysConst.DELETE);
                }
        	}
        }
        GrpContMMap.put(mLCDelPolLog, SysConst.INSERT);
        
        return GrpContMMap;
    }
    
    
    /**
     * 操作结果
     * @return VData
     */
    public MMap getMMap() {
        return mMap;
    }
    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "GroupContBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }
    
    public static void main(String[] args){
    	BriGrpContDeleteData tBriGrpContDeleteBL = new BriGrpContDeleteData();
    	GlobalInput tGlobalInput = new GlobalInput();
    	tGlobalInput.ManageCom = "86110000";
    	tGlobalInput.Operator = "ly";
    	
    	LCGrpContDB tLCGrpContDB = new LCGrpContDB();
    	tLCGrpContDB.setPrtNo("99000400001");
    	LCGrpContSet tLCGrpContSet = tLCGrpContDB.query();
    	
    	VData tVData = new VData();
    	tVData.add(tGlobalInput);
    	tVData.add(tLCGrpContSet.getObj(1));
    	tBriGrpContDeleteBL.submitData(tVData, "test");
    }
}
