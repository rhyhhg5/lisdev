package com.sinosoft.lis.agent;

import com.cbsws.obj.RspUniSalescod;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAgenttempDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LAQualificationDB;
import com.sinosoft.lis.db.LATreeBDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAAgenttempSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LAQualificationBSchema;
import com.sinosoft.lis.schema.LAQualificationSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.sys.UnisalescodFun;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LAQualificationSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LAWarrantorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: AgentSystem</p>
 * <p>Description: 销售管理——增员管理</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author xjh
 * @version 1.0
 */

public class LABankAgentBLF {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private VData mInputData1 = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private MMap map = new MMap();
    private MMap map1 = new MMap();
    private String mIsManager;
    private String mOrphanCode = "N";
    private String mBranchattr;
    private boolean changeManagerFlag=false;
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAAgenttempSchema mLAAgenttempSchema = new LAAgenttempSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private LABranchGroupSchema mLABranchGroupSchema=new LABranchGroupSchema();
    private LABranchGroupBSchema mLABranchGroupBSchema=new LABranchGroupBSchema();
    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();
    private LABranchGroupSet mLABranchGroupSet1 = new LABranchGroupSet();
    private LABranchGroupDB mLABranchGroupDB = new LABranchGroupDB();
    private LAWarrantorSet mLAWarrantorSet = new LAWarrantorSet();
    private LARearRelationSet mLARearRelationSet = new LARearRelationSet();
    private LAQualificationSchema mLAQualificationSchema = new LAQualificationSchema();
    private LATreeInterface mLATreeInterface;
    private LAAgentInterface mLAAgentInterface;
    private LAAscriptionInterface mLAAscriptionInterface;
    private LAOrphanPolicyInterface mLAOrphanPolicyInterface;
    private LARearInterface mLARearInterface;
    private LARecomInterface mLARecomInterface;
    private TransferData mTransferData;
    
    private String mOldEmploydate = "";
    /** 数据操作字符串 */
  private String currentDate = PubFun.getCurrentDate();
  private String currentTime = PubFun.getCurrentTime();

    private String mAgentCode = "";
    private String mAgentState = "";
    private String mEdorNo = "";

    public String getAgentCode() {
        return mAgentCode;
    }
    
    public String getAgentState() {
        return mAgentState;
    }
    
    public LABankAgentBLF() {
    }

    public static void main(String[] args) {
        LABankAgentBLF LABankAgentBLF = new LABankAgentBLF();
    }
   
    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Begin LABankAgentBLF.submitData.........");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //修改入司时间时增加相关校验（新人即没有算过薪资的人员可以修改入司时间）
        if(!check())
        {
        	return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        if(this.mLAAgentSchema.getBranchType().equals("3")
        		&&this.mLAAgentSchema.getBranchType2().equals("01")
        		&&this.mOperate.equals("INSERT||MAIN")){
        	if(!dealAgentTemp()){
        		return false;
        	}
        }
        /**获取集团工号Unisalescod
         * 如果获取集团工号失败，则入司不成功
         * */
//        UniCommon tUniCommon = new UniCommon();
//        boolean tFlag = tUniCommon.getGroupAgentCode(mOperate, mAgentCode, mLAAgentSchema, mLAQualificationSchema.getQualifNo());
//        if(!tFlag){
//        	this.mErrors.copyAllErrors(tUniCommon.mErrors) ;
//        	return false;
//        }
        System.out.println("美女：");
        UnisalescodFun fun=new UnisalescodFun();
        RspUniSalescod mRspUniSalescod = new RspUniSalescod();
        String mGroupAgentCode="";
        System.out.println("樱桃："+mOperate);
        if(mOperate.equals("INSERT||MAIN")){
        	System.out.println("苹果：");
        	mRspUniSalescod=fun.getUnisalescod(mOperate, mLAAgentSchema, null,null,mLAAgentSchema.getQuafNo());
        	mGroupAgentCode=mRspUniSalescod.getUNI_SALES_COD();
        	System.out.println("007："+mGroupAgentCode);
        }
        if("01".equals(mRspUniSalescod.getMESSAGETYPE()))
		{
			 CError tError = new CError();
	         tError.moduleName = "LABankAgentBLF";
	         tError.functionName = "dealData";
	         tError.errorMessage = mRspUniSalescod.getERRDESC();
	         this.mErrors.addOneError(tError);
			return false;
		}
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start LABankAgentBLF Submit...");
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LABankAgentBLF";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("橘子："+mOperate+";agentcode ="+mAgentCode);
        if(mOperate.equals("INSERT||MAIN")){
        	String LAagenSQL="update laagent set groupagentcode='"+mGroupAgentCode
        	+ "' where agentcode='"+this.mAgentCode+"'";
         System.out.println("修改laagent的SQL为："+LAagenSQL);
         ExeSQL tExeSQL1 = new ExeSQL();
         boolean tValue = tExeSQL1.execUpdateSQL(LAagenSQL);
         System.out.println("葡萄："+tValue);
        }
        if(changeManagerFlag){
                if(!setBranchManager()){
                        CError tError = new CError();
                tError.moduleName = "LABankAgentBLF";
                tError.functionName = "setBranchManager";
                tError.errorMessage = "更改团队主管时出错！";
                this.mErrors.addOneError(tError);
                return false;
                }
                tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(mInputData1, "UPDATE")) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LABankAgentBLF";
                tError.functionName = "submitData";
                tError.errorMessage = "修改团队主管失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        //针对新人（未参与过薪资计算）修改入司时间
        	if(this.mOldEmploydate!=null&&!this.mOldEmploydate.equals("")&&!this.mOldEmploydate.equals(mLAAgentSchema.getEmployDate()))
    		{
    		String LAtreeSQL="update latree set startdate='"+this.mLAAgentSchema.getEmployDate()+"' " 
    				+ " ,astartdate='"+this.mLAAgentSchema.getEmployDate()+"',operator='"+this.mGlobalInput.Operator+"' " 
    			    + " ,modifydate='"+this.currentDate+"' , modifytime='"+this.currentTime+"' where agentcode='"+this.mLAAgentSchema.getAgentCode()+"'";
    		System.out.println("修改latree的SQL为："+LAtreeSQL);
    		
    		
    		this.map.put(LAtreeSQL, "UPDATE");
    		
    		}
        return true;
    }
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealAgentTemp(){
    	if(this.mLAAgenttempSchema.getAgentCode()==null
    			||this.mLAAgenttempSchema.getAgentCode().equals("")){
    		return true;
    	}
    	LAAgenttempDB tLAAgenttempDB=new LAAgenttempDB();
    	tLAAgenttempDB.setAgentCode(this.mLAAgenttempSchema.getAgentCode());
    	if(!tLAAgenttempDB.getInfo()){
            CError tError = new CError();
            tError.moduleName = "LABankAgentBLF";
            tError.functionName = "submitData";
            tError.errorMessage = "获取代理人录入基础信息失败!";
            this.mErrors.addOneError(tError);
            return false;
    	}
    	this.mLAAgenttempSchema=tLAAgenttempDB.getSchema();
    	this.mLAAgenttempSchema.setAgentState("2");
    	this.mLAAgenttempSchema.setOperator(this.mGlobalInput.Operator);
    	this.mLAAgenttempSchema.setModifyDate(this.currentDate);
    	this.mLAAgenttempSchema.setModifyTime(this.currentTime);
    	this.map.put(this.mLAAgenttempSchema, "UPDATE");
    	return true;
    }
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量

        String tBranchType = "";
        String tBranchType2 = "";
        String tFirstGrade = "";

        System.out.println("Begin LABankAgentBLF.getInputData.........");
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mIsManager = (String) cInputData.getObject(1);
        System.out.print(mIsManager);

        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                                      getObjectByObjectName("LAAgentSchema", 0));
        this.mTransferData=(TransferData)cInputData.getObjectByObjectName("TransferData", 0);
        this.mLATreeSchema.setSchema((LATreeSchema) cInputData.
                                     getObjectByObjectName("LATreeSchema", 0));
        this.mLAWarrantorSet.set((LAWarrantorSet) cInputData.
                                 getObjectByObjectName("LAWarrantorSet", 0));
        this.mLARearRelationSet.set((LARearRelationSet) cInputData.
                                    getObjectByObjectName("LARearRelationSet",
                0));
        if(mLAAgentSchema.getBranchType().equals("3") &&
                mLAAgentSchema.getBranchType2().equals("01")){
        	this.mLAAgenttempSchema.setAgentCode(mLAAgentSchema.getAgentCode());
        	//this.mLAAgenttempSchema.setAgentCode(mLAAgentSchema.getGroupAgentCode());
		System.out.println("我是可爱的苹果"+mLAAgenttempSchema.getAgentCode());	
        }
        if (mLAAgentSchema.getBranchType().equals("1") &&
            mLAAgentSchema.getBranchType2().equals("01")) {
            this.mOrphanCode = (String) cInputData.getObject(6);
            System.out.print("mOrphanCode===="+mOrphanCode);
        }
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABankAgentBLF";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        mBranchattr=mLAAgentSchema.getAgentGroup();
        mAgentState=mLAAgentSchema.getAgentState();
        tBranchType = mLATreeSchema.getBranchType();
        tBranchType2 = mLATreeSchema.getBranchType2();
        //
        this.mOldEmploydate=(String)this.mTransferData.getValueByName("OldEmployDate");
        if(this.mOperate.equals("INSERT||MAIN")){
        	if(mLATreeSchema.getAgentGrade()!=null && !"".equals(mLATreeSchema.getAgentGrade())){
                tFirstGrade = mLATreeSchema.getAgentGrade().substring(0, 1);
                if(tBranchType.equals("3")&& tBranchType2.equals("01") && tFirstGrade.equals("G") )
                    changeManagerFlag=true;
            }
        }
        System.out.println("getInputData end");
        return true;
    }
    
    //校验入司日期
    private boolean check()
    {
    	if(mLAAgentSchema.getAgentCode()!=null&&!mLAAgentSchema.getAgentCode().equals(""))
    	{
		ExeSQL tExeSQL1=new ExeSQL();
    	if(this.mOldEmploydate!=null&&!this.mOldEmploydate.equals("")&&!this.mOldEmploydate.equals(mLAAgentSchema.getEmployDate()))
		{
		String CheckSQL="select indexcalno from lawage where agentcode='"+this.mLAAgentSchema.getAgentCode()+"'";
		String tindexcalno=tExeSQL1.getOneValue(CheckSQL);
		  if(tindexcalno!=null&&!tindexcalno.equals(""))
		  {
			  CError tError = new CError();
              tError.moduleName = "LABankAgentBLF";
              tError.functionName = "check";
              tError.errorMessage = "业务员"+mLAAgentSchema.getAgentCode()+"已进行过薪资计算,不能修改其入司日期";
              this.mErrors.addOneError(tError);
              return false;
		  }
		}
    	}
    	return true;
    }
    
//  处理团队主管信息
public boolean setBranchManager(){
	Reflections tReflections = new Reflections();
	LATreeDB tLATreeDB=new LATreeDB();
	LATreeBDB tLATreeBDB=new LATreeBDB();
	LATreeSet tLATreeSet=new LATreeSet();
	LATreeBSet tLATreeBSet=new LATreeBSet();
	LATreeSchema tLATreeSchema=new LATreeSchema();
	LATreeBSchema tLATreeBSchema=new LATreeBSchema();
	String tSQL ="select * from latree where agentcode in (select agentcode from laagent where name='"
		+mLAAgentSchema.getName()+"' and idno='"+mLAAgentSchema.getIDNo()
		+"' and makedate=current date) ";
	tLATreeSet=tLATreeDB.executeQuery(tSQL);
	tLATreeSchema.setSchema(tLATreeSet.get(1));
	tSQL="select branchmanager from labranchgroup where agentgroup in ("
		+"select upbranch from labranchgroup where branchtype='"
    	+mLAAgentSchema.getBranchType()+"' and branchtype2='"
    	+mLAAgentSchema.getBranchType2()+"' and branchattr='"+mBranchattr
    	+"' and upbranch is not null) and agentgroup is not null";
	ExeSQL tExeSQL = new ExeSQL();
	SSRS tSSRS = tExeSQL.execSQL(tSQL);
	if(tSSRS.MaxNumber>0){
		tReflections.transFields(tLATreeBSchema, tLATreeSchema);
		tLATreeBSchema.setEdorNO(PubFun1.CreateMaxNo("EdorNo", 20));
		tLATreeBSchema.setRemoveType("05");
		tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
		tLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
		tLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
		tLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
		tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
		tLATreeBSchema.setMakeDate(currentDate);
		tLATreeBSchema.setMakeTime(currentTime);
		tLATreeBSchema.setModifyDate(currentDate);
		tLATreeBSchema.setModifyTime(currentTime);
		tLATreeBSchema.setOperator(mGlobalInput.Operator);
		tLATreeSchema.setUpAgent(tSSRS.GetText(1,1));
		tLATreeSchema.setOperator(this.mGlobalInput.Operator);
		tLATreeSchema.setModifyDate(this.currentDate);
		tLATreeSchema.setModifyTime(this.currentTime);
	}
    tSQL="select * from latree where agentgroup in ("
    	+"select agentgroup from labranchgroup where branchtype='"
    	+mLAAgentSchema.getBranchType()+"' and branchtype2='"
    	+mLAAgentSchema.getBranchType2()+"' and branchattr='"+mBranchattr+"')"
    	+" and agentcode in (select agentcode from laagent where branchtype='"
    	+mLAAgentSchema.getBranchType()+"' and branchtype2='"
    	+mLAAgentSchema.getBranchType2()+"' and agentstate<'06') and "
    	+"agentcode<>'"+tLATreeSchema.getAgentCode()+"'";
    tLATreeSet=tLATreeDB.executeQuery(tSQL);
//    if(tLATreeSet.size()>0){
//    	tReflections.transFields(tLATreeBSet, tLATreeSet);
//    }
    LATreeBSchema ttLATreeBSchema;
    for(int i=1;i<=tLATreeSet.size();i++){
    	ttLATreeBSchema=new LATreeBSchema();
    	tReflections.transFields(ttLATreeBSchema,tLATreeSet.get(i));
    	tLATreeBSet.add(ttLATreeBSchema);
    }
    for(int i=1;i<=tLATreeSet.size();i++){
    	tLATreeBSet.get(i).setEdorNO(PubFun1.CreateMaxNo("EdorNo", 20));
    	tLATreeBSet.get(i).setRemoveType("05");
    	tLATreeBSet.get(i).setMakeDate2(tLATreeSet.get(i).getMakeDate());
    	tLATreeBSet.get(i).setMakeTime2(tLATreeSet.get(i).getMakeTime());
    	tLATreeBSet.get(i).setModifyDate2(tLATreeSet.get(i).getModifyDate());
    	tLATreeBSet.get(i).setModifyTime2(tLATreeSet.get(i).getModifyTime());
    	tLATreeBSet.get(i).setOperator2(tLATreeSet.get(i).getOperator());
    	tLATreeBSet.get(i).setMakeDate(currentDate);
    	tLATreeBSet.get(i).setMakeTime(currentTime);
    	tLATreeBSet.get(i).setModifyDate(currentDate);
    	tLATreeBSet.get(i).setModifyTime(currentTime);
    	tLATreeBSet.get(i).setOperator(mGlobalInput.Operator);
    	tLATreeSet.get(i).setUpAgent(tLATreeSchema.getAgentCode());
    	tLATreeSet.get(i).setOperator(this.mGlobalInput.Operator);
    	tLATreeSet.get(i).setModifyDate(this.currentDate);
    	tLATreeSet.get(i).setModifyTime(this.currentTime);
    }
    if(tSSRS.MaxNumber>0){
    	tLATreeBSet.add(tLATreeBSchema);
    	tLATreeSet.add(tLATreeSchema);
    }
    tSQL="select * from labranchgroup where branchtype='"
    	+mLAAgentSchema.getBranchType()+"' and branchtype2='"
    	+mLAAgentSchema.getBranchType2()+"' and branchattr='"+mBranchattr+"'";
    mLABranchGroupSet=mLABranchGroupDB.executeQuery(tSQL);
    if (mLABranchGroupDB.mErrors.needDealError()) {
        this.mErrors.copyAllErrors(mLABranchGroupDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "LABankAgentBLF";
        tError.functionName = "setBranchManager";
        tError.errorMessage = "查询" + mLAAgentSchema.getAgentGroup() + "的机构信息失败！";
        this.mErrors.addOneError(tError);
        return false;
    }
    mLABranchGroupSchema=mLABranchGroupSet.get(1);
    tReflections.transFields(mLABranchGroupBSchema, mLABranchGroupSchema);
    //mLABranchGroupBSchema=mLABranchGroupSchema.clone();
    mLABranchGroupBSchema.setEdorNo(PubFun1.CreateMaxNo("EdorNo", 20));
    mLABranchGroupBSchema.setEdorType("07");
    mLABranchGroupBSchema.setMakeDate2(mLABranchGroupSchema.getMakeDate());
    mLABranchGroupBSchema.setMakeTime2(mLABranchGroupSchema.getMakeTime());
    mLABranchGroupBSchema.setModifyDate2(mLABranchGroupSchema.getModifyDate());
    mLABranchGroupBSchema.setModifyTime2(mLABranchGroupSchema.getModifyTime());
    mLABranchGroupBSchema.setOperator2(mLABranchGroupSchema.getOperator());
    mLABranchGroupBSchema.setMakeDate(currentDate);
    mLABranchGroupBSchema.setMakeTime(currentTime);
    mLABranchGroupBSchema.setModifyDate(currentDate);
    mLABranchGroupBSchema.setModifyTime(currentTime);
    mLABranchGroupBSchema.setOperator(mGlobalInput.Operator);
    mLABranchGroupSchema.setBranchManager(tLATreeSchema.getAgentCode());
    mLABranchGroupSchema.setBranchManagerName(mLAAgentSchema.getName());
    mLABranchGroupSchema.setOperator(mGlobalInput.Operator);
    mLABranchGroupSchema.setModifyDate(currentDate);
    mLABranchGroupSchema.setModifyTime(currentTime);
    mLABranchGroupSet1.add(mLABranchGroupSchema);
    map1.put(mLABranchGroupSet1, "UPDATE");
    map1.put(mLABranchGroupBSchema,"INSERT");
    map1.put(tLATreeSet,"UPDATE");
    map1.put(tLATreeBSet,"INSERT");
    mInputData1.add(map1);
        return true;
}
    /**
     * 业务处理主函数
     * @return boolean
     */
    public boolean dealData() {
        System.out.println("Begin LABankAgentBLF.dealData.........");
        //查找项目组名称
        String tSQL = "select VarValue from LASysVar where VarType = 'prname' ";
        ExeSQL tExeSQL = new ExeSQL();
        String Project = "" + tExeSQL.getOneValue(tSQL);
        //如果没有查到应该调用的程序,则调用product对应的程序组。
        Project = (Project.equals("") || Project.equals("null")) ? "Product" :
                  Project;
        System.out.println("Project:" + Project);
        
 
        /**生成代理编码AgentCode*/
        if (!getCreateCodeClass(Project)) {
            return false;
        }

        if (mOrphanCode.equals("Y")) {
            if (!getLAAscriptionClass(Project)) {
                return false;
            }

            //传入参数
            mLAAscriptionInterface.setAgentCode(mAgentCode);
            mLAAscriptionInterface.setGlobalInput(mGlobalInput);
            //处理
            if (!mLAAscriptionInterface.dealdata()) {
                mErrors.copyAllErrors(mLAAscriptionInterface.mErrors);
                return false;
            }
            //获取处理结果
            MMap tMap8 = mLAAscriptionInterface.getResult();
            map.add(tMap8);

            if (!getLAOrphanPolicyClass(Project)) {
                return false;
            }
            //传入参数
            mLAOrphanPolicyInterface.setAgentCode(mAgentCode);
            mLAOrphanPolicyInterface.setGlobalInput(mGlobalInput);
            //处理
            if (!mLAOrphanPolicyInterface.dealdata()) {
                mErrors.copyAllErrors(mLAOrphanPolicyInterface.mErrors);
                return false;
            }
            //获取处理结果
            MMap tMap9 = mLAOrphanPolicyInterface.getResult();
            map.add(tMap9);

        }
        /**处理基本信息*/
        //获取处理类
        if (!getAgentClass(Project)) {
            return false;
        }

        //传入参数
        mLAAgentInterface.setOperate(mOperate);
        mLAAgentInterface.setAgentCode(mAgentCode);
        mLAAgentInterface.setEdorNo(mEdorNo);
        mLAAgentInterface.setGlobalInput(mGlobalInput);
        mLAAgentInterface.setLAAgentSchema(mLAAgentSchema);
        mLAAgentInterface.setLATreeSchema(mLATreeSchema);
        mLAAgentInterface.setLAWarrantorSet(mLAWarrantorSet);
        mLAAgentInterface.setOperate(mOperate);
        //处理
        if (!mLAAgentInterface.dealdata()) {
            mErrors.copyAllErrors(mLAAgentInterface.mErrors);
            return false;
        }
        //获取处理结果
        MMap tMap1 = mLAAgentInterface.getResult();
        map.add(tMap1);
        /**处理行政信息*/
        //获取处理类
        if (!getTreeClass(Project)) {
            return false;
        }
        //传入参数
        mLATreeInterface.setOperate(mOperate);
        mLATreeInterface.setAgentCode(mAgentCode);
        mLATreeInterface.setEdorNo(mEdorNo);
        mLATreeInterface.setGlobalInput(mGlobalInput);
        mLATreeInterface.setIsManager(mIsManager);
        mLATreeInterface.setLATreeSchema(mLATreeSchema);
        mLATreeInterface.setLAAgentSchema(mLAAgentInterface.
                                          getLAAgentSchema());
        //处理
        if (!mLATreeInterface.dealdata()) {
            mErrors.copyAllErrors(mLATreeInterface.mErrors);
            return false;
        }
        //获取处理结果
        MMap tMap2 = mLATreeInterface.getResult();
        map.add(tMap2);
        if (mLAAgentSchema.getBranchType().equals("1")) {
            if (!getRecomClass(Project)) {
                return false;
            }
            //传入推荐关系参数，save页面将推荐人放在了mLATreeSchema的IntroAgency字段
            mLATreeSchema.setAgentCode(mAgentCode);
            mLARecomInterface.setOperate(mOperate);
            mLARecomInterface.setGlobalInput(mGlobalInput);
            mLARecomInterface.setLATreeSchema(mLATreeInterface.
                                              getLATreeSchema());
            mLARecomInterface.setEdorNo(mEdorNo);

            //处理推荐关系
            if (!mLARecomInterface.dealdata()) {
                mErrors.copyAllErrors(mLATreeInterface.mErrors);
                return false;
            }
            //获取处理结果
            MMap tMap3 = mLARecomInterface.getResult();
            map.add(tMap3);

            /**处理育成关系*/
            //获取处理类
            if (!getRearClass(Project)) {
                return false;
            }
            //传入参数
            mLARearInterface.setOperate(mOperate);
            mLARearInterface.setLATreeSchema(mLATreeInterface.
                                             getLATreeSchema());
            mLARearInterface.setEdorNo(mEdorNo);
            mLARearInterface.setGlobalInput(mGlobalInput);
            //处理
            if (!mLARearInterface.dealdata()) {
                mErrors.copyAllErrors(mLARearInterface.mErrors);
                return false;
            }
            //获取处理结果
            MMap tMap4 = mLARearInterface.getResult();
            map.add(tMap4);
        }

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            System.out.println("Begin LABankAgentBLF.prepareOutputData.........");
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABankAgentBLF";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 如果AgentCode不存在（即新增人员），生成AgentCode
     * @param cName String
     * @return boolean
     */
    private boolean getCreateCodeClass(String cName) {
        LACreateCodeInterface tLACreateCodeInterface;
        try {
            Class mCreateCodeClass = Class.forName(
                    "com.sinosoft.lis.agent.LACreateCode" + cName + "BL");
            tLACreateCodeInterface = (LACreateCodeInterface)
                                     mCreateCodeClass.
                                     newInstance();
        } catch (ClassNotFoundException ex1) {
            System.out.println(
                    ".........Not Found LACreateCode" + cName + "BL，错误");
            if (!cName.equals("Product")) {
                if (!getCreateCodeClass("Product")) {
                    return false;
                }
            }
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LABankAgentBLF";
            tError.functionName = "getCreateCodeClass";
            tError.errorMessage = "没有找到LACreateCode" + cName + "BL类！";
            this.mErrors.addOneError(tError);
            return false;
        } catch (Exception ex1) {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAAgentDealBL";
            tError.functionName = "getCreateCodeClass";
            tError.errorMessage = "调用LACreateCode" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        tLACreateCodeInterface.setManagecom(mLAAgentSchema.getManageCom());
        tLACreateCodeInterface.setOperate(mOperate);
        tLACreateCodeInterface.setBranchType(mLAAgentSchema.getBranchType());
        tLACreateCodeInterface.setBranchType2(mLAAgentSchema.getBranchType2());
        //处理
        if (!tLACreateCodeInterface.dealdata()) {
            mErrors.copyAllErrors(tLACreateCodeInterface.mErrors);
            return false;
        }
        if (mOperate.equals("INSERT||MAIN")) {
            //获取处理结果
            mAgentCode = tLACreateCodeInterface.getAgentCode();
            if(mLAAgentSchema.getQuafNo()!=null&&!mLAAgentSchema.getQuafNo().equals("")){
	            this.mLAQualificationSchema.setAgentCode(mAgentCode);
	        	this.mLAQualificationSchema.setQualifNo(mLAAgentSchema.getQuafNo());
	        	this.mLAQualificationSchema.setGrantUnit("中国保险监督管理委员会");
	        	this.mLAQualificationSchema.setValidStart(mLAAgentSchema.getQuafStartDate());
	        	this.mLAQualificationSchema.setValidEnd(mLAAgentSchema.getQuafEndDate());
	        	this.mLAQualificationSchema.setOperator(this.mGlobalInput.Operator);
	        	this.mLAQualificationSchema.setMakeDate(this.currentDate);
	        	this.mLAQualificationSchema.setMakeTime(this.currentTime);
	        	this.mLAQualificationSchema.setModifyDate(this.currentDate);
	        	this.mLAQualificationSchema.setModifyTime(this.currentTime);          	
	            this.map.put(this.mLAQualificationSchema, "INSERT");
            }
        } else {
        	
            mAgentCode = mLAAgentSchema.getAgentCode();
            if(mLAAgentSchema.getQuafNo()!=null&&!mLAAgentSchema.getQuafNo().equals("")){
	            LAQualificationDB tLAQualificationDB=new LAQualificationDB();
	        	LAQualificationSet tLAQualificationSet=new LAQualificationSet();
	        	tLAQualificationDB.setAgentCode(mAgentCode);
	        	tLAQualificationSet=tLAQualificationDB.query();
	        	if(tLAQualificationSet.size()>=1){
	        	LAQualificationSchema oldmLAQualificationSchema=new LAQualificationSchema();
	        	oldmLAQualificationSchema=tLAQualificationSet.get(1);    
	        	System.out.println(oldmLAQualificationSchema.getQualifNo());
	        	
	        	LAQualificationBSchema mLAQualificationBSchema=new LAQualificationBSchema();        	
	            Reflections tReflections1 = new Reflections();
	            tReflections1.transFields(mLAQualificationBSchema, oldmLAQualificationSchema);
	            mLAQualificationBSchema.setEdorNo(PubFun1.CreateMaxNo("EdorNo", 19));
	            mLAQualificationBSchema.setOperator1(this.mGlobalInput.Operator);
	            mLAQualificationBSchema.setMakeDate1(this.currentDate);
	            mLAQualificationBSchema.setMakeTime1(this.currentTime);
	            this.map.put(mLAQualificationBSchema, "INSERT");  
	            this.map.put(oldmLAQualificationSchema, "DELETE");    
	        	}
	            this.mLAQualificationSchema.setAgentCode(this.mLAAgentSchema.getAgentCode());
	            	this.mLAQualificationSchema.setQualifNo(mLAAgentSchema.getQuafNo());
	            	this.mLAQualificationSchema.setGrantUnit("中国保险监督管理委员会");
	            	this.mLAQualificationSchema.setValidStart(mLAAgentSchema.getQuafStartDate());
	            	this.mLAQualificationSchema.setValidEnd(mLAAgentSchema.getQuafEndDate());
	            	this.mLAQualificationSchema.setOperator(this.mGlobalInput.Operator);
	            	this.mLAQualificationSchema.setMakeDate(this.currentDate);
	            	this.mLAQualificationSchema.setMakeTime(this.currentTime);
	            	this.mLAQualificationSchema.setModifyDate(this.currentDate);
	            	this.mLAQualificationSchema.setModifyTime(this.currentTime);          	
	                this.map.put(this.mLAQualificationSchema, "INSERT");
	        }
        }
        mEdorNo = tLACreateCodeInterface.getEdorNo();
        System.out.println("B表流水号mEdorNo:  " + mEdorNo);
        return true;
    }

    /**
     * 获得代理人基本信息处理类
     * @param cName String
     * @return boolean
     */
    private boolean getAgentClass(String cName) {
        try {
            Class mLAAgentClass = Class.forName(
                    "com.sinosoft.lis.agent.LAAgent" + cName + "BL");
            mLAAgentInterface = (LAAgentInterface) mLAAgentClass.
                                newInstance();
        } catch (ClassNotFoundException ex1) {
            System.out.println(
                    ".........Not Found LAAgent" + cName + "BL，错误");
            if (!cName.equals("Product")) {
                if (!getAgentClass("Product")) {
                    return false;
                }
            }
            return true;
        } catch (Exception ex1) {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAAgentDealBL";
            tError.functionName = "getAgentClass";
            tError.errorMessage = "调用LAAgent" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 获得     基本信息处理类
     * @param cName String
     * @return boolean
     */
    private boolean getLAAscriptionClass(String cName) {
        try {
            Class mLAAscriptionClass = Class.forName(
                    "com.sinosoft.lis.agent.LAAscription" + cName + "BL");
            mLAAscriptionInterface = (LAAscriptionInterface) mLAAscriptionClass.
                                     newInstance();
        } catch (ClassNotFoundException ex1) {
            System.out.println(
                    ".........Not Found LAAscription" + cName + "BL，错误");
            if (!cName.equals("Product")) {
                if (!getLAAscriptionClass("Product")) {
                    return false;
                }
            }
            return true;
        } catch (Exception ex1) {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAAscriptionDealBL";
            tError.functionName = "getLAAscriptionClass";
            tError.errorMessage = "调用LAAscription" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 获得     基本信息处理类
     * @param cName String
     * @return boolean
     */
    private boolean getLAOrphanPolicyClass(String cName) {
        try {
            Class mLAOrphanPolicyClass = Class.forName(
                    "com.sinosoft.lis.agent.LAOrphanPolicy" + cName + "BL");
            mLAOrphanPolicyInterface = (LAOrphanPolicyInterface)
                                       mLAOrphanPolicyClass.
                                       newInstance();
        } catch (ClassNotFoundException ex1) {
            System.out.println(
                    ".........Not Found LAOrphanPolicy" + cName + "BL，错误");
            if (!cName.equals("Product")) {
                if (!getLAOrphanPolicyClass("Product")) {
                    return false;
                }
            }
            return true;
        } catch (Exception ex1) {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAOrphanPolicyDealBL";
            tError.functionName = "getLAOrphanPolicyClass";
            tError.errorMessage = "调用LAOrphanPolicy" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 获得行政信息处理类
     * @param cName String
     * @return boolean
     */
    private boolean getTreeClass(String cName) {
        try {
            Class mLATreeClass = Class.forName(
                    "com.sinosoft.lis.agent.LATree" + cName + "BL");
            mLATreeInterface = (LATreeInterface) mLATreeClass.
                               newInstance();
        } catch (ClassNotFoundException ex1) {
            System.out.println(
                    ".........Not Found LATree" + cName + "BL，错误");
            if (!getTreeClass("Product")) {
                return false;
            }
            return true;
        } catch (Exception ex1) {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAAgentDealBL";
            tError.functionName = "getTreeClass";
            tError.errorMessage = "调用LATree" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 获得育成关系处理类
     * @param cName String
     * @return boolean
     */
    private boolean getRearClass(String cName) {
        try {
            Class mLARearClass = Class.forName(
                    "com.sinosoft.lis.agent.LARear" + cName + "BL");
            mLARearInterface = (LARearInterface) mLARearClass.
                               newInstance();
        } catch (ClassNotFoundException ex1) {
            System.out.println(
                    ".........Not Found LARear" + cName + "BL，错误");
            if (!getRearClass("Product")) {
                return false;
            }
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAAgentDealBL";
            tError.functionName = "getRearClass";
            tError.errorMessage = "没有找到LARear" + cName + "BL类！";
            this.mErrors.addOneError(tError);
            return false;
        } catch (Exception ex1) {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAAgentDealBL";
            tError.functionName = "getRearClass";
            tError.errorMessage = "调用LARear" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 获得推荐关系处理类
     * @param cName String
     * @return boolean
     */
    private boolean getRecomClass(String cName) {
        try {
            Class mLARecomClass = Class.forName(
                    "com.sinosoft.lis.agent.LARecom" + cName + "BL");
            mLARecomInterface = (LARecomInterface) mLARecomClass.
                                newInstance();
        } catch (ClassNotFoundException ex1) {
            System.out.println(
                    ".........Not Found LARecom" + cName + "BL，错误");
            if (!getRecomClass("Product")) {
                return false;
            }
            return true;
        } catch (Exception ex1) {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LABankAgentBLF";
            tError.functionName = "getRecomClass";
            tError.errorMessage = "调用LARecom" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
