package com.sinosoft.lis.agent;

import java.sql.*;

import com.cbsws.obj.RspUniSalescod;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.sys.UnisalescodFun;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.agent.ALADimissionAppUI;
import com.sinosoft.lis.agentassess.LAAssessInputUI;

/**
 * 考核计算回退，
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class LAECAgentComBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 往后面传输数据的容器 ，进行回退计算*/
    private VData mVData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private String mAgentCode;
    //**
    private MMap mMMap=new MMap();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();  //要的人员
    private LAAgentBSchema mLAAgentBSchema = new LAAgentBSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private LATreeBSchema mLATreeBSchema = new LATreeBSchema();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    public LAECAgentComBL()
    {
    }
    public static void main(String args[])
    {
    }

    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            return true;
        }
        else
        {
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("Start LAECAgentComBL Submit...");
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAECAgentComBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            UniCommon tUniCommon = new UniCommon();
            boolean tFlag = tUniCommon.getGroupAgentCode(mOperate, mAgentCode, mLAAgentSchema, null);
            if(!tFlag){
            	this.mErrors.copyAllErrors(tUniCommon.mErrors) ;
            	return false;
            }
            
        }
        mInputData = null;
        return true;
    }
    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
      mInputData.add(mMMap);
        return true;
    }


    /**
     * 业务处理方法
     * @return boolean
     */
    private boolean dealData()
    {
    	/**获取集团工号Unisalescod*/
        
        if (mOperate.equals("INSERT||MAIN"))
        {

            String tManageCom=mLAAgentSchema.getManageCom();
            if(tManageCom==null || tManageCom.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "LAECAgentComBL";
                tError.functionName = "dealDate";
                tError.errorMessage = "管理机构不能为空!";
                this.mErrors.addOneError(tError);
                return false;
            }
            String tComCode=tManageCom.substring(2,4);
            
            String tPrefix = "";
            if(mLAAgentSchema.getBranchType().equals("8")&&mLAAgentSchema.getBranchType2().equals("02")){
            	tPrefix = tManageCom.substring(2, 4)+"10";
                this.mAgentCode = tManageCom.substring(2, 4)+"10"+PubFun1.CreateMaxNo("AgentCode" + tPrefix, 6);
            }else{
//            	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAECAgentComBL";
                tError.functionName = "dealData";
                tError.errorMessage = "未配置渠道信息，无法生成业务员工号！";
                this.mErrors.addOneError(tError);
            	return false;
            }
            System.out.println("----->>"+this.mAgentCode);
            
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(mAgentCode);
            if(tLAAgentDB.getInfo())//校验是否存在此人，如果存在，不能再增加
            {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LAECAgentComBL";
                    tError.functionName = "dealDate";
                    tError.errorMessage = "代码为"+mAgentCode+"的人员信息已经存在，分公司不能再增加公司业务!";
                    this.mErrors.addOneError(tError);
                    return false;
            }

            mLAAgentSchema.setAgentCode(mAgentCode);
            //modify by zhuxt 20140930 证件号默认为agentcode
            mLAAgentSchema.setIDNo(mAgentCode);
            mLAAgentSchema.setOperator(mGlobalInput.Operator);
            mLAAgentSchema.setMakeDate(currentDate);
            mLAAgentSchema.setMakeTime(currentTime);
            mLAAgentSchema.setModifyDate(currentDate);
            mLAAgentSchema.setModifyTime(currentTime);

            mLATreeSchema.setAgentCode(mAgentCode);
            mLATreeSchema.setOperator(mGlobalInput.Operator);
            mLATreeSchema.setMakeDate(currentDate);
            mLATreeSchema.setMakeTime(currentTime);
            mLATreeSchema.setModifyDate(currentDate);
            mLATreeSchema.setModifyTime(currentTime);
            //获取集团工号
//            mRspUniSalescod=fun.getUnisalescod(mOperate, mLAAgentSchema, null,null,null);
//        	mLAAgentSchema.setGroupAgentCode(mRspUniSalescod.getUNI_SALES_COD());
//        	if("01".equals(mRspUniSalescod.getMESSAGETYPE()))
//    		{
//    			 CError tError = new CError();
//    	         tError.moduleName = "LAECAgentComBL";
//    	         tError.functionName = "dealData";
//    	         tError.errorMessage = mRspUniSalescod.getERRDESC();
//    	         this.mErrors.addOneError(tError);
//    			return false;
//    		}
            mMMap.put(mLAAgentSchema, "INSERT");
            mMMap.put(mLATreeSchema,"INSERT");
        }
        else if(mOperate.equals("UPDATE||MAIN"))
        {
            String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            //备份
            String tAgentCode=mLAAgentSchema.getAgentCode();
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tAgentCode);
            if(!tLAAgentDB.getInfo())
            {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LAECAgentComBL";
                    tError.functionName = "dealDate";
                    tError.errorMessage = "没有查询到代码为"+tAgentCode+"的人员信息!";
                    this.mErrors.addOneError(tError);
                    return false;
            }
            LAAgentSchema tLAAgentSchema = new LAAgentSchema();
            tLAAgentSchema=tLAAgentDB.getSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(mLAAgentBSchema,tLAAgentSchema);
            mLAAgentBSchema.setEdorType("05");
            mLAAgentBSchema.setEdorNo(tEdorNo);
            mLAAgentSchema.setIDNo(tAgentCode);
            //备份完成
            mLAAgentSchema.setOperator(mGlobalInput.Operator);
            mLAAgentSchema.setModifyDate(currentDate);
            mLAAgentSchema.setModifyTime(currentTime);

            //备份
            LATreeDB tLATreeDB = new LATreeDB();
            tLATreeDB.setAgentCode(tAgentCode);
            if(!tLATreeDB.getInfo())
            {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LAECAgentComBL";
                    tError.functionName = "dealDate";
                    tError.errorMessage = "没有查询到代码为"+tAgentCode+"的人员信息!";
                    this.mErrors.addOneError(tError);
                    return false;
            }
            LATreeSchema tLATreeSchema = new LATreeSchema();
            tLATreeSchema=tLATreeDB.getSchema();
            tReflections = new Reflections();
            tReflections.transFields(mLATreeBSchema,tLATreeSchema);
            mLATreeBSchema.setEdorNO(tEdorNo);
            mLATreeBSchema.setRemoveType("05");
            mLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
            mLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
            mLATreeBSchema.setMakeTime(tLATreeSchema.getMakeTime());
            mLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
            mLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
            mLATreeBSchema.setOperator(mGlobalInput.Operator);
            mLATreeBSchema.setMakeDate(currentDate);
            mLATreeBSchema.setMakeTime(currentTime);
            mLATreeBSchema.setModifyDate(currentDate);
            mLATreeBSchema.setModifyTime(currentTime);


            //备份完成

            mLATreeSchema.setOperator(mGlobalInput.Operator);
            mLATreeSchema.setModifyDate(currentDate);
            mLATreeSchema.setModifyTime(currentTime);


            mMMap.put(mLAAgentBSchema,"INSERT");
            mMMap.put(mLAAgentSchema, "UPDATE");
            mMMap.put(mLATreeBSchema, "INSERT");
            mMMap.put(mLATreeSchema,"UPDATE");

        }
        return true;
    }

    private boolean check()
    {

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("Begin LAECAgentComBL.getInputData.........");
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                                      getObjectByObjectName("LAAgentSchema", 0));
        this.mLATreeSchema.setSchema((LATreeSchema) cInputData.
                                     getObjectByObjectName("LATreeSchema", 0));
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAECAgentComBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    public String getAgentCode()
    {

        return mAgentCode;
    }



}
