package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import java.math.BigInteger;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun1;

/*
 * <p>ClassName: LAComPersonalBL </p>
 * <p>Description: LAComPersonalBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 代理机构
 * @CreateDate：2005-11-01
 */

public class LAComPersonalBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    private String mPersonalCode = "";

    private MMap map = new MMap();

    /** 业务处理相关变量 */
    private LAComPersonalSchema mLAComPersonalSchema = new LAComPersonalSchema();

    public LAComPersonalBL()
    {
    }

    public static void main(String[] args)
    {
    }

    public String getPersonalCode()
    {
        return mPersonalCode;
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComPersonalBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAComPersonalBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAComPersonalBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        int tCount = 0;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        if (this.mOperate.equals("INSERT||MAIN"))
        {

            System.out
                    .println("与机构关系放入完毕LACompersonalBL.INSERT||MAIN");
            String tUserType = mLAComPersonalSchema.getUserType();
            String tPersonalCode = createAgentCode(tUserType);
            if (tPersonalCode.equals("") || tPersonalCode == null)
            {
                CError tError = new CError();
                tError.moduleName = "LAComPersonalBL";
                tError.functionName = "dealdata";
                tError.errorMessage = " 生成代理机构人员代码失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
            mPersonalCode = tPersonalCode;
            this.mLAComPersonalSchema.setPersonalCode(tPersonalCode);
            //代理机构代码自己填写
//            String tAgentCom = mLAComPersonalSchema.getAgentCom();
//            System.out.println("与机构关系放入完毕111111111111111111111111111111111111!"
//                    + tAgentCom);
//            LAComDB tLAComDB = new LAComDB();
//            // tLAComDB.setAgentCom(mLAComSchema.getAgentCom());
//            tLAComDB.setAgentCom(tAgentCom);
//            tCount = tLAComDB.getCount();
//            if (tCount <= 0)
//            {
//                // @@错误处理
//                this.mErrors.copyAllErrors(tLAComDB.mErrors);
//                CError tError = new CError();
//                tError.moduleName = "ALAComBL";
//                tError.functionName = "dealData()";
//                tError.errorMessage = "查询代理机构" + tAgentCom + "的信息失败";
//                this.mErrors.addOneError(tError);
//                return false;
//            }
            this.mLAComPersonalSchema.setOperator(mGlobalInput.Operator);
            this.mLAComPersonalSchema.setMakeDate(currentDate);
            this.mLAComPersonalSchema.setMakeTime(currentTime);
            this.mLAComPersonalSchema.setModifyDate(currentDate);
            this.mLAComPersonalSchema.setModifyTime(currentTime);
            map.put(mLAComPersonalSchema, "INSERT");
        }
        else if (this.mOperate.equals("UPDATE||MAIN")
                || this.mOperate.equals("DELETE||MAIN"))
        {
            mPersonalCode = mLAComPersonalSchema.getPersonalCode();
            LAComPersonalDB tLAComPersonalDB = new LAComPersonalDB();
            tLAComPersonalDB.setPersonalCode(mLAComPersonalSchema
                    .getPersonalCode());
            tCount = tLAComPersonalDB.getCount();
            if (tCount == -1)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAComPersonalDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAComPersonalBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "代理机构人员信息操作失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tCount == 0)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAComPersonalDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAComPersonalBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "该代理机构人员代码不存在！";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLAComPersonalDB = new LAComPersonalDB();
            tLAComPersonalDB.setLoginName(mLAComPersonalSchema.getLoginName());
            tLAComPersonalDB.getInfo();
            if (this.mOperate.equals("UPDATE||MAIN"))
            {
                this.mLAComPersonalSchema.setOperator(mGlobalInput.Operator);
                this.mLAComPersonalSchema.setMakeDate(tLAComPersonalDB
                        .getMakeDate());
                this.mLAComPersonalSchema.setMakeTime(tLAComPersonalDB
                        .getMakeTime());
                this.mLAComPersonalSchema.setModifyDate(currentDate);
                this.mLAComPersonalSchema.setModifyTime(currentTime);
                this.mLAComPersonalSchema
                        .setOperator(this.mGlobalInput.Operator);
                map.put(mLAComPersonalSchema, "UPDATE");
            }
        }
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAComPersonalSchema.setSchema((LAComPersonalSchema) cInputData
                .getObjectByObjectName("LAComPersonalSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData
                .getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComPersonalBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public String getAgentCom()
    {
        return this.mLAComPersonalSchema.getPersonalCode();
    }

    public String createAgentCode(String tUserType)
    {
        String tAgentCode = "";
        if (tUserType.equals("02"))
        {
            tAgentCode = mLAComPersonalSchema.getAgentCode();
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tAgentCode);
            if (!tLAAgentDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAComPersonalBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "查询业务员代码信息失败!";
                this.mErrors.addOneError(tError);
                return null;
            }
        }
        else
        {
            String tAgentCom = mLAComPersonalSchema.getAgentCom();
            LAComDB tLAComDB = new LAComDB();
            LAComSchema tLAComSchema = new LAComSchema();
            // tLAComDB.setAgentCom(mLAComSchema.getAgentCom());
            tLAComDB.setAgentCom(tAgentCom);
            if (!tLAComDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAComDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAComPersonalBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "查询代理机构信息失败!";
                this.mErrors.addOneError(tError);
                return null;
            }
            if (tLAComDB.getACType().equals("") || tLAComDB.getACType() == null)
            {
                this.mErrors.copyAllErrors(tLAComDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAComPersonalBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "查询代理机构信息失败!";
                this.mErrors.addOneError(tError);
                return null;
            }
            if (tLAComDB.getManageCom().equals("")
                    || tLAComDB.getManageCom() == null)
            {
                this.mErrors.copyAllErrors(tLAComDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAComPersonalBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "查询代理机构信息失败!";
                this.mErrors.addOneError(tError);
                return null;
            }
            String tPrefix = tLAComDB.getManageCom().substring(2, 4);
            tPrefix = tPrefix.trim() + "03".trim()
                    + tLAComDB.getACType().trim();
            tPrefix = tPrefix.trim();
            tAgentCode = PubFun1.CreateMaxNo("Personal" + tPrefix, 6);
            if (tAgentCode == null || tAgentCode.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "LAComPersonalBL";
                tError.functionName = "dealdata";
                tError.errorMessage = " 生成代理机构人员代码失败！";
                this.mErrors.addOneError(tError);
                return null;
            }
            tAgentCode = tPrefix + tAgentCode;
        }
        return tAgentCode;
    }

}
