package com.sinosoft.lis.agent;


import com.cbsws.obj.RspUniSalescod;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAgentGradeDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LAQualificationDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LAQualificationBSchema;
import com.sinosoft.lis.schema.LAQualificationSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.schema.LAWarrantorSchema;
import com.sinosoft.lis.sys.UnisalescodFun;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LAQualificationBSet;
import com.sinosoft.lis.vschema.LAQualificationSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LAWarrantorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: AgentSystem</p>
 * <p>Description: 销售管理——增员管理</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author xjh
 * @version 1.0
 */

public class LAInteractionAgentBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();


	/** 数据操作字符串 */
	private String mOperate;
	/** 全局数据 */
	private MMap map = new MMap();
	private GlobalInput mGlobalInput = new GlobalInput();

	private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
	private LATreeSchema mLATreeSchema = new LATreeSchema();
	
	private LAWarrantorSet mLAWarrantorSet = new LAWarrantorSet();
	private LAQualificationSchema mLAQualificationSchema = new LAQualificationSchema();
	
	private LAQualificationSet mdelLAQualificationSet = new LAQualificationSet();
	private LAQualificationBSet mLAQualificationBSet = new LAQualificationBSet();
	

	private String mAgentCode = "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();

    private LATreeSet mLATreeSet = new LATreeSet();
    private LATreeBSet mLATreeBSet = new LATreeBSet();

    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();
    private LABranchGroupBSet mLABranchGroupBSet = new LABranchGroupBSet();
   
    private LAWarrantorSet mInLAWarrantorSet = new LAWarrantorSet();
    
    private String mEdorNo = "";
    
    


	public String getAgentCode() {
		return mAgentCode;
	}

	public LAInteractionAgentBL() {
	}

	public static void main(String[] args)
	{

	}

	/**
	 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		if(!checkData())
		{
			return false;
		}
		//进行业务处理
		if (!dealData()) {
			return false;
		}
		//准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LAInteractionAgentBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		UniCommon tUniCommon = new UniCommon();
        boolean tFlag = tUniCommon.getGroupAgentCode(mOperate, mAgentCode, mLAAgentSchema, mLAQualificationSchema.getQualifNo());
        if(!tFlag){
        	this.mErrors.copyAllErrors(tUniCommon.mErrors) ;
        	return false;
        }
		
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData)
	{
		//全局变量
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData
				.getObjectByObjectName("LAAgentSchema", 0));
		this.mLATreeSchema.setSchema((LATreeSchema) cInputData
				.getObjectByObjectName("LATreeSchema", 0));
		
		this.mLAWarrantorSet.set((LAWarrantorSet) cInputData
				.getObjectByObjectName("LAWarrantorSet", 0));
		this.mLAQualificationSchema
				.setSchema((LAQualificationSchema) cInputData
						.getObjectByObjectName("LAQualificationSchema", 0));

		if (mGlobalInput == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAInteractionAgentBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的信息！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public boolean checkData()
	{
		return true;
	}
	/**
	 * 业务处理主函数
	 * @return boolean
	 */
	public boolean dealData() {
		/**获取集团工号Unisalescod*/
//		ExeSQL lExeSQL = new ExeSQL();
//        String tSQL = "select GroupAgentCode from laagent where agentcode=getagentcode('"+mLAAgentSchema.getAgentCode()+"')";
//        String mGroupAgentCode = lExeSQL.getOneValue(tSQL);
//        UnisalescodFun fun=new UnisalescodFun();
//        RspUniSalescod mRspUniSalescod = new RspUniSalescod();
//        if(mOperate.equals("INSERT||MAIN")){
//        	mRspUniSalescod=fun.getUnisalescod(mOperate, mLAAgentSchema, null,null,mLAQualificationSchema.getQualifNo());
//        	mLAAgentSchema.setGroupAgentCode(mRspUniSalescod.getUNI_SALES_COD());
//        }else if(mOperate.equals("UPDATE||MAIN")){
//        	mRspUniSalescod=fun.getUnisalescod(mOperate, mLAAgentSchema, null,mGroupAgentCode,null );
//        	mLAAgentSchema.setGroupAgentCode(mGroupAgentCode);
//        }
//        if("01".equals(mRspUniSalescod.getMESSAGETYPE()))
//		{
//			 CError tError = new CError();
//	         tError.moduleName = "LAInteractionAgentBL";
//	         tError.functionName = "dealData";
//	         tError.errorMessage = "获取集团工号时报文格式错误";
//	         this.mErrors.addOneError(tError);
//			return false;
//		}
////////////
		
		if(this.mLAQualificationSchema.getValidStart()!=null&&!this.mLAQualificationSchema.getValidStart().equals("")){//取消资格证必录校验add by wrx2015-7-9
			if(this.mLAQualificationSchema.getValidStart().compareTo(this.mLAQualificationSchema.getValidEnd())>0)
				{
					CError tError = new CError();
					tError.moduleName = "LAInteractionAgentBL";
					tError.functionName = "submitData";
					tError.errorMessage = "资格证有效止期应大于资格者证有效起期!";
					this.mErrors.addOneError(tError);
					return false;
					
				}
		}
		mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
		if("INSERT||MAIN".equals(this.mOperate))
		{
	    	
            String tPrefix = "";
            tPrefix = this.mLAAgentSchema.getManageCom().substring(2, 4)+"11";
            this.mAgentCode = mLAAgentSchema.getManageCom().substring(2, 4)+"11"+PubFun1.CreateMaxNo("AgentCode" + tPrefix, 6);
			// LAAgent表信息处理
			// 入司时，AgentState 为01
            this.mLAAgentSchema.setAgentCode(this.mAgentCode);
			this.mLAAgentSchema.setAgentState("01");
			
			//操作信息
			this.mLAAgentSchema.setOperator(this.mGlobalInput.Operator);
			this.mLAAgentSchema.setMakeDate(this.currentDate);
			this.mLAAgentSchema.setMakeTime(this.currentTime);
			this.mLAAgentSchema.setModifyDate(this.currentDate);
			this.mLAAgentSchema.setModifyTime(this.currentTime);			
			
			// LATree表信息处理
			// 职级信息处理
			
			this.mLATreeSchema.setInitGrade(this.mLATreeSchema.getAgentGrade());
			String tAgentGrade = this.mLATreeSchema.getAgentGrade();
			LAAgentGradeDB tLAAgentGradeDB = new LAAgentGradeDB();
			tLAAgentGradeDB.setGradeCode(tAgentGrade);
			if(!tLAAgentGradeDB.getInfo())
			{
				CError tError = new CError();
				tError.moduleName = "LAInteractionAgentBL";
				tError.functionName = "submitData";
				tError.errorMessage = "职级信息查询失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
			this.mLATreeSchema.setAgentCode(this.mAgentCode);
			this.mLATreeSchema.setAgentSeries(tLAAgentGradeDB.getGradeProperty2());
			this.mLATreeSchema.setStartDate(this.mLAAgentSchema.getEmployDate());
			this.mLATreeSchema.setAstartDate(this.mLAAgentSchema.getEmployDate());
			this.mLATreeSchema.setBranchCode(this.mLATreeSchema.getAgentGroup());
			this.mLATreeSchema.setInitGrade(this.mLATreeSchema.getAgentGrade());
			
            //操作信息
			this.mLATreeSchema.setOperator(this.mGlobalInput.Operator);
			this.mLATreeSchema.setMakeDate(this.currentDate);
			this.mLATreeSchema.setMakeTime(this.currentTime);
			this.mLATreeSchema.setModifyDate(this.currentDate);
			this.mLATreeSchema.setModifyTime(this.currentTime);
			

			
			if("0".equals(mLATreeSchema.getAgentSeries()))
			{
				//如果是业务职级序列，需查看是否有用户上级
				LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
				tLABranchGroupDB.setAgentGroup(this.mLAAgentSchema.getAgentGroup());
				if(!tLABranchGroupDB.getInfo())
				{
					CError tError = new CError();
					tError.moduleName = "LAInteractionAgentBL";
					tError.functionName = "submitData";
					tError.errorMessage = "职级序列信息查询失败!";
					this.mErrors.addOneError(tError);
					return false;
				}
				mLATreeSchema.setUpAgent(tLABranchGroupDB.getBranchManager());				
			}else{
				// 如果入司是主管序列人员。
				//1、需将其入司团队下所有在职人上级置为新入司主管
				//2、将所属团队主管编码、主管名称字段置对应值
				LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
				tLABranchGroupDB.setAgentGroup(this.mLAAgentSchema.getAgentGroup());
				if(!tLABranchGroupDB.getInfo())
				{
					CError tError = new CError();
					tError.moduleName = "LAInteractionAgentBL";
					tError.functionName = "submitData";
					tError.errorMessage = "团队信息查询报错!";
					this.mErrors.addOneError(tError);
					return false;
				}
				LABranchGroupSet  tLABranchGroupSet = tLABranchGroupDB.query();
				for(int i = 1;i<=tLABranchGroupSet.size();i++)
				{					
					LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
					LABranchGroupBSchema tLABranchGroupBSchema = new LABranchGroupBSchema();					
					tLABranchGroupSchema = tLABranchGroupSet.get(i);
					Reflections tReflections = new Reflections();
			        tReflections.transFields(tLABranchGroupBSchema, tLABranchGroupSchema);
			        
			        tLABranchGroupBSchema.setEdorType("05");
			        tLABranchGroupBSchema.setEdorNo(this.mEdorNo);
			        tLABranchGroupBSchema.setMakeDate(this.currentDate);
			        tLABranchGroupBSchema.setMakeTime(this.currentTime);
			        tLABranchGroupBSchema.setModifyDate(this.currentDate);
			        tLABranchGroupBSchema.setModifyTime(this.currentTime);
			        tLABranchGroupBSchema.setOperator(this.mGlobalInput.Operator);
			        
			        tLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.getOperator());
			        tLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.getMakeDate());
			        tLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.getMakeTime());
			        tLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.getModifyDate());
			        tLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.getModifyTime());
			        
			        tLABranchGroupSchema.setBranchManager(this.mAgentCode);
			        tLABranchGroupSchema.setBranchManagerName(this.mLAAgentSchema.getName());
			        tLABranchGroupSchema.setModifyDate(currentDate);
			        tLABranchGroupSchema.setModifyTime(currentTime);
			        tLABranchGroupSchema.setOperator(this.mGlobalInput.Operator);
			        
			        
			        this.mLABranchGroupSet.add(tLABranchGroupSchema);
			        this.mLABranchGroupBSet.add(tLABranchGroupBSchema);
					
				}
				// 处理人员
				String sql = "Select * From LAAgent where 1=1 and BranchType = '"+this.mLAAgentSchema.getBranchType()+"'"
				+" and BranchType2 = '"+this.mLAAgentSchema.getBranchType2()+"' " 
				+" and AgentGroup = '"+this.mLAAgentSchema.getAgentGroup()+"'"
				+" and AgentState <'06'"; 
				LAAgentDB tLAAgentDB = new LAAgentDB();
				LAAgentSet tLAAgentSet = new LAAgentSet();
				tLAAgentSet = tLAAgentDB.executeQuery(sql);
				if(tLAAgentSet.size()>0)
				{
					for(int i = 1;i<=tLAAgentSet.size();i++)
					{
						LAAgentSchema tLAAgentSchema = new LAAgentSchema();
						LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
						tLAAgentSchema = tLAAgentSet.get(i);
						Reflections tReflections = new Reflections();
				        tReflections.transFields(tLAAgentBSchema, tLAAgentSchema);

				        tLAAgentBSchema.setEdorNo(this.mEdorNo);
				        tLAAgentBSchema.setEdorType("05");
				        tLAAgentBSchema.setModifyDate(this.currentDate);
				        tLAAgentBSchema.setModifyTime(this.currentTime);
				        tLAAgentBSchema.setOperator(this.mGlobalInput.Operator);
				        				        
				        mLAAgentBSet.add(tLAAgentBSchema);
				        
				        tLAAgentSchema.setModifyDate(currentDate);
				        tLAAgentSchema.setModifyTime(currentTime);
				        tLAAgentSchema.setOperator(this.mGlobalInput.Operator);
				        
				        mLAAgentSet.add(tLAAgentSchema);
				        
				        LATreeDB tLATreeDB = new LATreeDB();
				        tLATreeDB.setAgentCode(tLAAgentSchema.getAgentCode());
				        if(!tLATreeDB.getInfo())
				        {
							CError tError = new CError();
							tError.moduleName = "LAInteractionAgentBL";
							tError.functionName = "submitData";
							tError.errorMessage = "人员信息查询报错!";
							this.mErrors.addOneError(tError);
							return false;
				        }
				        
				        LATreeSchema tLATreeSchema = tLATreeDB.query().get(1);				        
				        LATreeBSchema tLATreeBSchema = new LATreeBSchema();
				        tReflections.transFields(tLATreeBSchema, tLATreeSchema);
				        
				        tLATreeBSchema.setMakeDate(currentDate);
				        tLATreeBSchema.setMakeTime(currentTime);
				        tLATreeBSchema.setModifyDate(currentDate);
				        tLATreeBSchema.setModifyTime(currentTime);
				        tLATreeBSchema.setOperator(mGlobalInput.Operator);
				        tLATreeBSchema.setRemoveType("05");
				        tLATreeBSchema.setEdorNO(this.mEdorNo);
				        
				        tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
				        tLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
				        tLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
				        tLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
				        tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
				        
				        mLATreeBSet.add(tLATreeBSchema);
				        
				        tLATreeSchema.setUpAgent(this.mAgentCode);
				        tLATreeSchema.setModifyDate(currentDate);
				        tLATreeSchema.setModifyTime(currentTime);
				        this.mLATreeSet.add(tLATreeSchema);																		
					}					
				}										
			}
			// 资格证信息处理
			// 资格证信息校验
			//////////////
			if(this.mLAQualificationSchema.getQualifNo()!=null&&!this.mLAQualificationSchema.getQualifNo().equals("")){
				String strSQL = "Select 1 From LAQualification where AgentCode <> '"+this.mAgentCode+"' " 
						+" and qualifno ='"+this.mLAQualificationSchema.getQualifNo()+"' " 
						+" and exists(Select 1 from laagent where agentcode = LAQualification.agentcode and branchtype= '"+this.mLAAgentSchema.getBranchType()+"' and branchtype2 = '"+this.mLAAgentSchema.getBranchType2()+"' " 
						+" and agentstate < '06') fetch first 1 rows only";
				System.out.println("打印sql:"+strSQL);
				ExeSQL tExeSQL = new ExeSQL();
				String tResult = tExeSQL.getOneValue(strSQL);
				if(tResult!=null&&"1".equals(tResult))
				{
					CError tError = new CError();
					tError.moduleName = "LAInteractionAgentBL";
					tError.functionName = "submitData";
					tError.errorMessage = "录入资格证系统已存在,烦请重新进行录入!";
					this.mErrors.addOneError(tError);
					return false;
					
				}
				
	
				mLAQualificationSchema.setAgentCode(this.mAgentCode);
				mLAQualificationSchema.setreissueDate(this.currentDate);
				mLAQualificationSchema.setMakeDate(this.currentDate);
				mLAQualificationSchema.setMakeTime(this.currentTime);
				mLAQualificationSchema.setModifyDate(this.currentDate);
				mLAQualificationSchema.setModifyTime(this.currentTime);
				mLAQualificationSchema.setOperator(this.mGlobalInput.Operator);
				map.put(mLAQualificationSchema, "INSERT");
			}else{
				ExeSQL tExeSQL = new ExeSQL();
				String tSQL = "select qualifno from LAQualification where agentcode = '"+this.mLAAgentSchema.getAgentCode()+"'";
				SSRS tSSRS = tExeSQL.execSQL(tSQL);
				if(tSSRS.getMaxRow()>0){
					String aQualifno = tSSRS.GetText(1, 1);
					LAQualificationDB tLAQualificationDB = new LAQualificationDB();
					tLAQualificationDB.setAgentCode(this.mLAAgentSchema.getAgentCode());
					tLAQualificationDB.setQualifNo(aQualifno);
					if(tLAQualificationDB.getInfo())
					{
						this.mdelLAQualificationSet = tLAQualificationDB.query();
						map.put(mdelLAQualificationSet, "DELETE");
					}
				}
			}
			//担保人信息处理
			for(int i = 1;i<=this.mLAWarrantorSet.size();i++)
			{
				LAWarrantorSchema tLAWarrantorSchema = new LAWarrantorSchema();
				tLAWarrantorSchema = mLAWarrantorSet.get(i);
				tLAWarrantorSchema.setAgentCode(this.mAgentCode);
				tLAWarrantorSchema.setSerialNo(i);
				tLAWarrantorSchema.setAgentCode(this.mAgentCode);
				tLAWarrantorSchema.setMakeDate(this.currentDate);
				tLAWarrantorSchema.setMakeTime(this.currentTime);
				tLAWarrantorSchema.setModifyDate(this.currentDate);
				tLAWarrantorSchema.setModifyTime(this.currentTime);
				tLAWarrantorSchema.setOperator(this.mGlobalInput.Operator);
								
				mInLAWarrantorSet.add(tLAWarrantorSchema);
				
				
			}
			
			
			map.put(mLAAgentSchema, "INSERT");
			map.put(mLATreeSchema, "INSERT");						
			map.put(mLATreeSet, "UPDATE");
			map.put(mLATreeBSet, "INSERT");									
			map.put(mLAAgentSet, "UPDATE");	
			map.put(mLAAgentBSet, "INSERT");						
			map.put(mLABranchGroupSet, "UPDATE");
			map.put(mLABranchGroupBSet, "INSERT");			
			map.put(mInLAWarrantorSet, "INSERT");
			
		}
		if("UPDATE||MAIN".equals(this.mOperate))
		{
			LAAgentDB tLAAgentDB = new LAAgentDB();
			tLAAgentDB.setAgentCode(this.mLAAgentSchema.getAgentCode());
			if(!tLAAgentDB.getInfo())
			{
				CError tError = new CError();
			    tError.moduleName = "LAInteractionAgentBL";
			    tError.functionName = "prepareOutputData";
			    tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			    this.mErrors.addOneError(tError);
			    return false;				
			}
			LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
			Reflections tReflections = new Reflections();
			tReflections.transFields(tLAAgentBSchema, tLAAgentDB.getSchema());
			
	        tLAAgentBSchema.setEdorNo(this.mEdorNo);
	        tLAAgentBSchema.setEdorType("05");
	        tLAAgentBSchema.setModifyDate(this.currentDate);
	        tLAAgentBSchema.setModifyTime(this.currentTime);
	        tLAAgentBSchema.setOperator(this.mGlobalInput.Operator);	        				        
	        mLAAgentBSet.add(tLAAgentBSchema);
			
			this.mLAAgentSchema.setMakeDate(tLAAgentDB.getMakeDate());
			this.mLAAgentSchema.setMakeTime(tLAAgentDB.getMakeTime());
			
			
			this.mLAAgentSchema.setOperator(this.mGlobalInput.Operator);
			this.mLAAgentSchema.setModifyDate(this.currentDate);
			this.mLAAgentSchema.setModifyTime(this.currentTime);
			
			
			LATreeDB tLATreeDB = new LATreeDB();
			tLATreeDB.setAgentCode(this.mLATreeSchema.getAgentCode());
			if(!tLATreeDB.getInfo())
			{
				CError tError = new CError();
			    tError.moduleName = "LAInteractionAgentBL";
			    tError.functionName = "dealdata";
			    tError.errorMessage = "人员行政信息查询失败。";
			    this.mErrors.addOneError(tError);
			    return false;									
			}
			LATreeBSchema tLATreeBSchema = new LATreeBSchema();
			tReflections.transFields(tLATreeBSchema, tLATreeDB.getSchema());
			
	        tLATreeBSchema.setRemoveType("05");
	        tLATreeBSchema.setEdorNO(this.mEdorNo);
	        tLATreeBSchema.setMakeDate(currentDate);
	        tLATreeBSchema.setMakeTime(currentTime);
	        tLATreeBSchema.setModifyDate(currentDate);
	        tLATreeBSchema.setModifyTime(currentTime);
	        tLATreeBSchema.setOperator(mGlobalInput.Operator);
	        
	        tLATreeBSchema.setMakeDate2(tLATreeDB.getMakeDate());
	        tLATreeBSchema.setMakeTime2(tLATreeDB.getMakeTime());
	        tLATreeBSchema.setModifyDate2(tLATreeDB.getModifyDate());
	        tLATreeBSchema.setModifyTime2(tLATreeDB.getModifyTime());
	        tLATreeBSchema.setOperator2(tLATreeDB.getOperator());
	        
	        this.mLATreeBSet.add(tLATreeBSchema);
	        
	        this.mLATreeSchema.setUpAgent(tLATreeDB.getUpAgent()); // add by fuxin 2014-12-7
			this.mLATreeSchema.setAgentSeries(tLATreeDB.getAgentSeries());
			this.mLATreeSchema.setStartDate(tLATreeDB.getStartDate());
			this.mLATreeSchema.setAstartDate(tLATreeDB.getAstartDate());
			this.mLATreeSchema.setBranchCode(tLATreeDB.getAgentGroup());
			this.mLATreeSchema.setInitGrade(tLATreeDB.getAgentGrade());
			
            //操作信息
			this.mLATreeSchema.setOperator(this.mGlobalInput.Operator);
			this.mLATreeSchema.setMakeDate(this.currentDate);
			this.mLATreeSchema.setMakeTime(this.currentTime);
			this.mLATreeSchema.setModifyDate(this.currentDate);
			this.mLATreeSchema.setModifyTime(this.currentTime);
			
			// 资格证信息处理
			//add by wrx 2015-7-9
			if(this.mLAQualificationSchema.getQualifNo()!=null&&!this.mLAQualificationSchema.getQualifNo().equals("") ){
				LAQualificationDB tLAQualificationDB = new LAQualificationDB();
				tLAQualificationDB.setAgentCode(this.mLAAgentSchema.getAgentCode());
				tLAQualificationDB.setQualifNo(this.mLAQualificationSchema.getQualifNo());
//				if(!tLAQualificationDB.getInfo())
//				{
//					CError tError = new CError();
//					tError.moduleName = "LAInteractionAgentBL";
//					tError.functionName = "submitData";
//					tError.errorMessage = "资格证信息查询失败!";
//					this.mErrors.addOneError(tError);
//					return false;				
//					
//				}
				this.mdelLAQualificationSet =tLAQualificationDB.query();
				if(this.mdelLAQualificationSet.size()>0)
				{
					for(int i = 1;i<=this.mdelLAQualificationSet.size();i++)
					{
						LAQualificationSchema tLAQualificationSchema = new LAQualificationSchema();
						tLAQualificationSchema = mdelLAQualificationSet.get(i);
						LAQualificationBSchema tLAQualificationBSchema = new LAQualificationBSchema();
						tReflections.transFields(tLAQualificationBSchema, tLAQualificationSchema);
						
						tLAQualificationBSchema.setEdorNo(this.mEdorNo);
						tLAQualificationBSchema.setMakeDate(this.currentDate);
						tLAQualificationBSchema.setMakeTime(this.currentTime);
						tLAQualificationBSchema.setModifyDate(this.currentDate);
						tLAQualificationBSchema.setModifyTime(this.currentTime);
						tLAQualificationBSchema.setOperator(this.mGlobalInput.Operator);
						
						tLAQualificationBSchema.setMakeDate1(tLAQualificationSchema.getMakeDate());
						tLAQualificationBSchema.setMakeTime1(tLAQualificationSchema.getMakeTime());
						tLAQualificationBSchema.setOperator1(tLAQualificationSchema.getOperator());
						this.mLAQualificationBSet.add(tLAQualificationBSchema);
						
					}
				}
				
				
				
				String strSQL = "Select 1 From LAQualification where AgentCode <> '"+this.mLAAgentSchema.getAgentCode()+"' " 
						+" and qualifno ='"+this.mLAQualificationSchema.getQualifNo()+"' " 
						+" and exists(Select 1 from laagent where agentcode = LAQualification.agentcode and branchtype= '"+this.mLAAgentSchema.getBranchType()+"' and branchtype2 = '"+this.mLAAgentSchema.getBranchType2()+"' "
						+" and agentstate < '06') fetch first 1 rows only";
				
				System.out.println("打印sql:"+strSQL);
				ExeSQL tExeSQL = new ExeSQL();
				String tResult = tExeSQL.getOneValue(strSQL);
				if(tResult!=null&&"1".equals(tResult))
				{
					CError tError = new CError();
					tError.moduleName = "LAInteractionAgentBL";
					tError.functionName = "submitData";
					tError.errorMessage = "录入资格证系统已存在,烦请重新进行录入!";
					this.mErrors.addOneError(tError);
					return false;
					
				}
				
				mLAQualificationSchema.setAgentCode(this.mLAAgentSchema.getAgentCode());
				mLAQualificationSchema.setMakeDate(this.currentDate);
				mLAQualificationSchema.setMakeTime(this.currentTime);
				mLAQualificationSchema.setModifyDate(this.currentDate);
				mLAQualificationSchema.setModifyTime(this.currentTime);
				mLAQualificationSchema.setOperator(this.mGlobalInput.Operator);
	            map.put(mdelLAQualificationSet, "DELETE");
	            map.put(mLAQualificationBSet, "INSERT");
	            map.put(mLAQualificationSchema, "INSERT");
			}else{
				ExeSQL tExeSQL = new ExeSQL();
				String tSQL = "select qualifno from LAQualification where agentcode = '"+this.mLAAgentSchema.getAgentCode()+"'";
				SSRS tSSRS = tExeSQL.execSQL(tSQL);
				if(tSSRS.getMaxRow()>0){
					String aQualifno = tSSRS.GetText(1, 1);
					LAQualificationDB tLAQualificationDB = new LAQualificationDB();
					tLAQualificationDB.setAgentCode(this.mLAAgentSchema.getAgentCode());
					tLAQualificationDB.setQualifNo(aQualifno);
					if(tLAQualificationDB.getInfo())
					{
						this.mdelLAQualificationSet = tLAQualificationDB.query();
						map.put(mdelLAQualificationSet, "DELETE");
					}
				}
			}
			
			//担保人信息处理
			for(int i = 1;i<=this.mLAWarrantorSet.size();i++)
			{
				LAWarrantorSchema tLAWarrantorSchema = new LAWarrantorSchema();
				tLAWarrantorSchema = mLAWarrantorSet.get(i);
				tLAWarrantorSchema.setAgentCode(this.mLAAgentSchema.getAgentCode());
				tLAWarrantorSchema.setMakeDate(this.currentDate);
				tLAWarrantorSchema.setSerialNo(i);
				tLAWarrantorSchema.setMakeTime(this.currentTime);
				tLAWarrantorSchema.setModifyDate(this.currentDate);
				tLAWarrantorSchema.setModifyTime(this.currentTime);
				tLAWarrantorSchema.setOperator(this.mGlobalInput.Operator);
								
				mInLAWarrantorSet.add(tLAWarrantorSchema);
				
				
			}
		
			map.put(mLAAgentSchema, "UPDATE");
			map.put(mLATreeSchema, "UPDATE");						

			map.put(mLATreeBSet, "INSERT");									
			map.put(mLAAgentBSet, "INSERT");				
            
			map.put(mInLAWarrantorSet, "DELETE&INSERT");
			
			
			
			
		}
		return true;
	}

	/**
	 * 准备往后层输出所需要的数据
	 * 输出：如果准备数据时发生错误则返回false,否则返回true
	 */
	private boolean prepareOutputData() {
		try {
			mInputData.clear();
			mInputData.add(map);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAInteractionAgentBL";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

   /**
    *  获取人员信息
    */
	private String getAgentName(String tAgentCode)
	{
		String result = "";
		LAAgentDB tLAAgentDB = new LAAgentDB();
		tLAAgentDB.setAgentCode(tAgentCode);
		if(!tLAAgentDB.getInfo())
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAInteractionAgentBL";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "人员基础信息查询失败。";
			this.mErrors.addOneError(tError);
			return null;
			
		}
		result = tLAAgentDB.getName();
		return result;
	}

}
