package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAgenttempDB;
import com.sinosoft.lis.db.LAQualificationDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAAgenttempSchema;
import com.sinosoft.lis.schema.LAQualificationBSchema;
import com.sinosoft.lis.schema.LAQualificationSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.schema.LATreeTempSchema;
import com.sinosoft.lis.vschema.LAQualificationSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.vschema.LAWarrantorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: AgentSystem</p>
 * <p>Description: 销售管理——增员管理</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author xjh
 * @version 1.0
 */

public class LABankAgentNACBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private VData mInputData1 = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private MMap map = new MMap();
    private MMap map1 = new MMap();
    private String mIsManager;
    private String mOrphanCode = "N";
    private String mBranchattr;
    private boolean changeManagerFlag=false;
//    private StringBuffer tSB=new StringBuffer();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAgenttempSchema mLAAgenttempSchema = new LAAgenttempSchema();
    private LATreeTempSchema mLATreeTempSchema = new LATreeTempSchema();
    private LAWarrantorSet mLAWarrantorSet = new LAWarrantorSet();
    private LARearRelationSet mLARearRelationSet = new LARearRelationSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private LATreeBSchema mLATreeBSchema = new LATreeBSchema();
    private LAAgentBSchema mLAAgentBSchema = new LAAgentBSchema();
    private LAQualificationSchema mLAQualificationSchema = new LAQualificationSchema();
    /** 数据操作字符串 */
  private String currentDate = PubFun.getCurrentDate();
  private String currentTime = PubFun.getCurrentTime();

    private String mAgentCode = "";
    private String mEdorNo = "";
    private String mmAgentCode="";
    private TransferData mTransferData;
    private String mOldEmploydate = "";

    public String getAgentCode() {
        return mAgentCode;
    }

    public LABankAgentNACBL() {
    }

    public static void main(String[] args) {
        LABankAgentNACBL LABankAgentNACBL = new LABankAgentNACBL();
    }

    /**
      传输数据的公共方法X
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Begin LABankAgentNACBL.submitData.........");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //修改入司时间时增加相关校验（新人即没有算过薪资的人员可以修改入司时间）
        if(!check())
        {
        	return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start LABankAgentNACBL Submit...");
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LABankAgentNACBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量

        System.out.println("Begin LABankAgentNACBL.getInputData.........");
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mIsManager = (String) cInputData.getObject(1);
        System.out.print(mIsManager);

        this.mLAAgenttempSchema.setSchema((LAAgenttempSchema) cInputData.
                                      getObjectByObjectName("LAAgenttempSchema", 0));
        this.mLATreeTempSchema.setSchema((LATreeTempSchema) cInputData.
                                     getObjectByObjectName("LATreeTempSchema", 0));
        this.mLAWarrantorSet.set((LAWarrantorSet) cInputData.
                                 getObjectByObjectName("LAWarrantorSet", 0));
        this.mLARearRelationSet.set((LARearRelationSet) cInputData.
                                    getObjectByObjectName("LARearRelationSet",
                0));
        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                getObjectByObjectName("LAAgentSchema", 0));
        this.mLATreeSchema.setSchema((LATreeSchema) cInputData.
                getObjectByObjectName("LATreeSchema", 0));
        this.mTransferData=(TransferData)cInputData.getObjectByObjectName("TransferData", 0);
        this.mOldEmploydate=(String)this.mTransferData.getValueByName("OldEmployDate");
        
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABankAgentNACBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        mBranchattr=mLAAgenttempSchema.getAgentGroup();
        return true;
    }
    /**
     * 业务处理主函数
     * @return boolean
     */
    public boolean dealData() {
        System.out.println("Begin LABankAgentNACBL.dealData.........");
        /**生成代理编码AgentCode*/
        if(this.mOperate.equals("INSERT||MAIN")){
        	String tTempAgentCode=PubFun1.CreateMaxNo("TEMPAGENTCODE", 10);
        	this.mAgentCode=tTempAgentCode;
        	this.mLAAgenttempSchema.setAgentCode(tTempAgentCode);
        	this.mLAAgenttempSchema.setAgentState("0");
        	this.mLAAgenttempSchema.setOperator(this.mGlobalInput.Operator);
        	this.mLAAgenttempSchema.setMakeDate(this.currentDate);
        	this.mLAAgenttempSchema.setMakeTime(this.currentTime);
        	this.mLAAgenttempSchema.setModifyDate(this.currentDate);
        	this.mLAAgenttempSchema.setModifyTime(this.currentTime);
        	this.mLATreeTempSchema.setAgentCode(tTempAgentCode);
        	this.mLATreeTempSchema.setCValiMonth("000000");
        	this.mLATreeTempSchema.setTempCount(0);
        	this.mLATreeTempSchema.setCValiFlag("0");
        	this.mLATreeTempSchema.setStartDate(this.mLAAgenttempSchema.getEmployDate());        	
        	this.mLATreeTempSchema.setOperator(this.mGlobalInput.Operator);
        	this.mLATreeTempSchema.setMakeDate(this.currentDate);
        	this.mLATreeTempSchema.setMakeTime(this.currentTime);
        	this.mLATreeTempSchema.setModifyDate(this.currentDate);
        	this.mLATreeTempSchema.setModifyTime(this.currentTime);
        	this.map.put(this.mLAAgenttempSchema, "INSERT");
        	this.map.put(this.mLATreeTempSchema, "INSERT");
        	
        }else if(this.mOperate.equals("UPDATE||MAIN")){
        	this.mAgentCode=this.mLAAgenttempSchema.getAgentCode();
        	LAAgenttempDB tLAAgenttempDB=new LAAgenttempDB();
        	tLAAgenttempDB.setAgentCode(this.mLAAgenttempSchema.getAgentCode());
        	this.mmAgentCode=this.mLAAgentSchema.getAgentCode();
        	LATreeDB tLATreeDB=new LATreeDB();
        	tLATreeDB.setAgentCode(mLAAgentSchema.getAgentCode());
        	LAAgentDB tLAAgentDB=new LAAgentDB();
        	tLAAgentDB.setAgentCode(mLAAgentSchema.getAgentCode());
        	if(!tLAAgenttempDB.getInfo()&&!tLAAgentDB.getInfo()){
        		CError tError = new CError();
                tError.moduleName = "LABankAgentNACBL";
                tError.functionName = "prepareOutputData";
                tError.errorMessage = "获取代理人录入信息失败。";
                this.mErrors.addOneError(tError);
                return false;
        	}
        	if(tLAAgenttempDB.getInfo())
        	{
        	this.mLAAgenttempSchema.setAgentState("0");
        	this.mLAAgenttempSchema.setOperator(this.mGlobalInput.Operator);
        	this.mLAAgenttempSchema.setMakeDate(tLAAgenttempDB.getMakeDate());
        	this.mLAAgenttempSchema.setMakeTime(tLAAgenttempDB.getMakeTime());
        	this.mLAAgenttempSchema.setModifyDate(this.currentDate);
        	this.mLAAgenttempSchema.setModifyTime(this.currentTime);
        	this.mLATreeTempSchema.setCValiMonth("000000");
        	this.mLATreeTempSchema.setTempCount(0);
        	this.mLATreeTempSchema.setCValiFlag("0");
        	this.mLATreeTempSchema.setStartDate(this.mLAAgenttempSchema.getEmployDate());        	
        	this.mLATreeTempSchema.setOperator(this.mGlobalInput.Operator);
        	this.mLATreeTempSchema.setMakeDate(tLAAgenttempDB.getMakeDate());
        	this.mLATreeTempSchema.setMakeTime(tLAAgenttempDB.getMakeTime());
        	this.mLATreeTempSchema.setModifyDate(this.currentDate);
        	this.mLATreeTempSchema.setModifyTime(this.currentTime);
        	
        	this.map.put(this.mLAAgenttempSchema, "UPDATE");
        	this.map.put(this.mLATreeTempSchema, "UPDATE");
        	}
        	else if(tLAAgentDB.getInfo())
        	{
        		LAAgentSchema tLAAgentSchema=new LAAgentSchema();
        		tLAAgentSchema=tLAAgentDB.getSchema();
        		LATreeSchema tLATreeSchema=new LATreeSchema();
        		tLAAgentDB.setAgentCode(mLAAgentSchema.getAgentCode());
        		tLATreeSchema=tLATreeDB.query().get(1);
                //针对新人（未参与过薪资计算）修改入司时间
            	if(this.mOldEmploydate!=null&&!this.mOldEmploydate.equals("")&&!this.mOldEmploydate.equals(mLAAgentSchema.getEmployDate()))
        		{
        		String LAtreeSQL="update latree set startdate='"+this.mLAAgentSchema.getEmployDate()+"' " 
        				+ " ,astartdate='"+this.mLAAgentSchema.getEmployDate()+"',operator='"+this.mGlobalInput.Operator+"' " 
        			    + " ,modifydate='"+this.currentDate+"' , modifytime='"+this.currentTime+"' where agentcode='"+this.mLAAgentSchema.getAgentCode()+"'";
        		System.out.println("修改latree的SQL为："+LAtreeSQL);
        		
        		Reflections tReflections=new Reflections();
                tReflections.transFields(mLATreeBSchema, tLATreeSchema);
                mLATreeBSchema.setAgentCode(mLAAgentSchema.getAgentCode());
                mLATreeBSchema.setAgentGroup(mLAAgentSchema.getAgentGroup());
                mLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
                mLATreeBSchema.setOperator(this.mGlobalInput.Operator);
                mLATreeBSchema.setEdorNO(PubFun1.CreateMaxNo("EdorNo", 20));
                mLATreeBSchema.setRemoveType("13");
                mLATreeBSchema.setModifyDate(this.currentDate);
                mLATreeBSchema.setModifyTime(this.currentTime);
                mLATreeBSchema.setMakeDate(this.currentDate);
                mLATreeBSchema.setMakeTime(this.currentTime);
                mLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
                mLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
                mLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
                mLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
                
                this.map.put(mLATreeBSchema, "INSERT");
        		this.map.put(LAtreeSQL, "UPDATE");
        		
        		}
        		this.mLAAgentSchema.setAgentCode(mmAgentCode);
        		this.mLAAgentSchema.setQuafNo(mLAAgentSchema.getQuafNo());
        		this.mLAAgentSchema.setQuafStartDate(mLAAgentSchema.getQuafStartDate());
        		this.mLAAgentSchema.setQuafEndDate(mLAAgentSchema.getQuafEndDate());
        		//this.mLAAgentSchema.setSaleQuaf("Y");
        		this.mLAAgentSchema.setSaleQuaf(mLAAgentSchema.getSaleQuaf());
        		this.mLAAgentSchema.setPhone(mLAAgentSchema.getPhone());
        		this.mLAAgentSchema.setOperator(this.mGlobalInput.Operator);
        		this.mLAAgentSchema.setBranchCode(tLAAgentSchema.getBranchCode());
        		this.mLAAgentSchema.setMakeDate(tLAAgentSchema.getMakeDate());
        		this.mLAAgentSchema.setMakeTime(tLAAgentSchema.getMakeTime());
        		this.mLAAgentSchema.setModifyDate(this.currentDate);
        		this.mLAAgentSchema.setModifyTime(this.currentTime);
        		
        		Reflections tReflections=new Reflections();
        		tReflections.transFields(mLAAgentBSchema, tLAAgentSchema);
        		mLAAgentBSchema.setEdorNo(PubFun1.CreateMaxNo("EdorNo", 19));
        		mLAAgentBSchema.setEdorType("13");
        		mLAAgentBSchema.setOperator(tLAAgentSchema.getOperator());
        		mLAAgentBSchema.setAgentCode(tLAAgentSchema.getAgentCode());
        		mLAAgentBSchema.setMakeDate(currentDate);
        		mLAAgentBSchema.setMakeTime(currentTime);
        		mLAAgentBSchema.setModifyDate(currentDate);
        		mLAAgentBSchema.setModifyTime(currentTime);
        		
        		this.map.put(this.mLAAgentSchema, "UPDATE");
        		this.map.put(this.mLAAgentBSchema, "INSERT");
        		System.out.println("mLAAgentSchema.getQuafNo()"+mLAAgentSchema.getQuafNo());
        		if(mLAAgentSchema.getQuafNo()!=null&&!mLAAgentSchema.getQuafNo().equals("")){
        			System.out.println("mLAAgentSchema.getQuafNo()2222"+mLAAgentSchema.getQuafNo());
        		LAQualificationDB tLAQualificationDB=new LAQualificationDB();
        		LAQualificationSet tLAQualificationSet=new LAQualificationSet();
        		tLAQualificationDB.setAgentCode(this.mLAAgentSchema.getAgentCode());
            	tLAQualificationSet=tLAQualificationDB.query();
            	if(tLAQualificationSet.size()>=1){
                	LAQualificationSchema oldmLAQualificationSchema=new LAQualificationSchema();
                	oldmLAQualificationSchema=tLAQualificationSet.get(1);    
                	System.out.println(oldmLAQualificationSchema.getQualifNo());
                	
                	LAQualificationBSchema mLAQualificationBSchema=new LAQualificationBSchema();        	
                    Reflections tReflections1 = new Reflections();
                    tReflections1.transFields(mLAQualificationBSchema, oldmLAQualificationSchema);
                    mLAQualificationBSchema.setEdorNo(PubFun1.CreateMaxNo("EdorNo", 19));
                    mLAQualificationBSchema.setOperator1(this.mGlobalInput.Operator);
                    mLAQualificationBSchema.setMakeDate1(this.currentDate);
                    mLAQualificationBSchema.setMakeTime1(this.currentTime);
                    mLAQualificationBSchema.setMakeTime(this.currentTime);
                    mLAQualificationBSchema.setMakeDate(this.currentTime);
                    this.map.put(mLAQualificationBSchema, "INSERT");  
                    this.map.put(oldmLAQualificationSchema, "DELETE"); 
                	}
            	this.mLAQualificationSchema.setAgentCode(this.mLAAgentSchema.getAgentCode());
            	this.mLAQualificationSchema.setQualifNo(mLAAgentSchema.getQuafNo());
            	this.mLAQualificationSchema.setGrantUnit("中国保险监督管理委员会");
            	this.mLAQualificationSchema.setValidStart(mLAAgentSchema.getQuafStartDate());
            	this.mLAQualificationSchema.setValidEnd(mLAAgentSchema.getQuafEndDate());
            	this.mLAQualificationSchema.setOperator(this.mGlobalInput.Operator);
            	this.mLAQualificationSchema.setMakeDate(this.currentDate);
            	this.mLAQualificationSchema.setMakeTime(this.currentTime);
            	this.mLAQualificationSchema.setModifyDate(this.currentDate);
            	this.mLAQualificationSchema.setModifyTime(this.currentTime);          	
                this.map.put(this.mLAQualificationSchema, "INSERT");
        		}
        	}
        }else if(this.mOperate.equals("DELETE||MAIN")){
        	this.map.put(this.mLAAgenttempSchema, "DELETE");
        	this.map.put(this.mLATreeTempSchema, "DELETE");
        }
        return true;
    }
    
    //校验入司日期
    private boolean check()
    {
    	if(mLAAgentSchema.getAgentCode()!=null&&!mLAAgentSchema.getAgentCode().equals(""))
    	{
		ExeSQL tExeSQL1=new ExeSQL();
    	if(this.mOldEmploydate!=null&&!this.mOldEmploydate.equals("")&&!this.mOldEmploydate.equals(mLAAgentSchema.getEmployDate()))
		{
		String CheckSQL="select indexcalno from lawage where agentcode='"+this.mLAAgentSchema.getAgentCode()+"'";
		String tindexcalno=tExeSQL1.getOneValue(CheckSQL);
		  if(tindexcalno!=null&&!tindexcalno.equals(""))
		  {
			  CError tError = new CError();
              tError.moduleName = "LABankAgentBLF";
              tError.functionName = "check";
              tError.errorMessage = "业务员"+mLAAgentSchema.getAgentCode()+"已进行过薪资计算,不能修改其入司日期";
              this.mErrors.addOneError(tError);
              return false;
		  }
		}
    	}
    	return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            System.out.println("Begin LABankAgentNACBL.prepareOutputData.........");
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABankAgentNACBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

}
