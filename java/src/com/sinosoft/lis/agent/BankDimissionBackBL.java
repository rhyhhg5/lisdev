/*
 * <p>ClassName: BankDimissionBackBL </p>
 * <p>Description: BankDimissionBackBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agent;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LADimissionDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.vschema.LADimissionSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.lis.schema.LARecomRelationSchema;
import com.sinosoft.lis.agentbranch.ChangeAgentToBranch;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.agentbranch.ChangeRearBranch;
import com.sinosoft.lis.schema.LARearRelationSchema;
import com.sinosoft.lis.schema.LARearRelationBSchema;
import com.sinosoft.lis.vschema.LARearRelationBSet;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LARecomRelationBSchema;
import com.sinosoft.lis.vschema.LARecomRelationBSet;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LAOrphanPolicySet;
import com.sinosoft.lis.schema.LAOrphanPolicySchema;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.db.LAOrphanPolicyDB;
import com.sinosoft.utility.SSRS;

public class BankDimissionBackBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LADimissionSchema mLADimissionSchema = new LADimissionSchema();
    private LADimissionSet mLADimissionSet = new LADimissionSet();
    private LABranchGroupSet  mupLABranchGroupSet = new LABranchGroupSet();
    private LARecomRelationSet mLARecomRelationSet = new LARecomRelationSet();
    private LARecomRelationBSet mLARecomRelationBSet = new LARecomRelationBSet();
    private LARearRelationSet mLARearRelationSet = new LARearRelationSet();
    private LARearRelationBSet mLARearRelationBSet = new LARearRelationBSet();
    private LABranchGroupBSet  minLABranchGroupBSet= new LABranchGroupBSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LAAgentBSchema mLAAgentBSchema = new LAAgentBSchema();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LATreeBSet mLATreeBSet = new LATreeBSet();
    private LAOrphanPolicySet mLAOrphanPolicySet = new LAOrphanPolicySet();
    private MMap mMap = new MMap();
    private String currentDate ="";
    private String currentTime ="";
    private String  mEdorNo="";
    private String  mFlag="";
    public BankDimissionBackBL()
    {
    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankDimissionBackBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败BankDimissionBackBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "BankDimissionBackBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
         }

        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        currentDate = PubFun.getCurrentDate();
        currentTime = PubFun.getCurrentTime();
        //离职登记回退 只需变更laagent表中的agentstate字段，删除ladimission表
        if (mFlag.equals("Enrol"))
        {
                //在LADimission表里删除人员信息
              LADimissionDB tLADimissionDB = new LADimissionDB();
              LADimissionSchema tLADimissionSchema = new LADimissionSchema();
              tLADimissionDB.setAgentCode(mLADimissionSchema.getAgentCode());
              tLADimissionDB.setDepartTimes(mLADimissionSchema.getDepartTimes());
              if (!tLADimissionDB.getInfo())
              {
                  CError tError = new CError();
                  tError.moduleName = "BankDimissionBackBL";
                  tError.functionName = "dealData";
                  tError.errorMessage = "查询代理人原离职信息失败!";
                  this.mErrors.addOneError(tError);
                  return false;
              }
            mLADimissionSchema=tLADimissionDB.getSchema();

                //变更人员的agentstate字段
            LAAgentDB tLAAgentDB = new LAAgentDB();
            String tAgentCode=this.mLADimissionSchema.getAgentCode();//离职人员人员
            LAAgentSchema tLAAgentSchema = new LAAgentSchema();
            tLAAgentDB.setAgentCode(tAgentCode);
            tLAAgentDB.getInfo();
            mLAAgentSchema=tLAAgentDB.getSchema();
            Reflections  tReflections= new Reflections();
            tReflections.transFields(mLAAgentBSchema,tLAAgentDB.getSchema());
            mLAAgentBSchema.setEdorNo(mEdorNo);
            mLAAgentBSchema.setEdorType("11");//离职登记回退

            if(mLADimissionSchema.getDepartState().equals("03"))
            {
                mLAAgentSchema.setAgentState("01");
            }
            else if(mLADimissionSchema.getDepartState().equals("04"))
            {
                mLAAgentSchema.setAgentState("02");
            }
            mLAAgentSchema.setOperator(mGlobalInput.Operator.toString());
            mLAAgentSchema.setModifyDate(currentDate);
            mLAAgentSchema.setModifyTime(currentTime);
           }
           //离职确认回退
           //Laagent表中agentstate,outworkdate字段变更
           //latree 如果为主管upagent字段 还有团队labranchgroup的主管字段
           //删除ladimission表数据
           //删除laorphanpolicy表数据
        else if(mFlag.equals("Affirm")){
                //在LADimission表里删除人员信息
                  LADimissionDB tLADimissionDB = new LADimissionDB();
                  LADimissionSchema tLADimissionSchema = new LADimissionSchema();
                  tLADimissionDB.setAgentCode(mLADimissionSchema.getAgentCode());
                  tLADimissionDB.setDepartTimes(mLADimissionSchema.getDepartTimes());
                  if (!tLADimissionDB.getInfo())
                  {
                      CError tError = new CError();
                      tError.moduleName = "BankDimissionBackBL";
                      tError.functionName = "dealData";
                      tError.errorMessage = "查询代理人原离职信息失败!";
                      this.mErrors.addOneError(tError);
                      return false;
                  }
                mLADimissionSchema=tLADimissionDB.getSchema();

                //变更人员的agentstate字段
                LAAgentDB tLAAgentDB = new LAAgentDB();
                String tAgentCode=this.mLADimissionSchema.getAgentCode();//离职人员人员
                LAAgentSchema tLAAgentSchema = new LAAgentSchema();
                tLAAgentDB.setAgentCode(tAgentCode);
                tLAAgentDB.getInfo();
                mLAAgentSchema=tLAAgentDB.getSchema();
                Reflections  tReflections= new Reflections();
                tReflections.transFields(mLAAgentBSchema,tLAAgentDB.getSchema());
                mLAAgentBSchema.setEdorNo(mEdorNo);
                mLAAgentBSchema.setEdorType("12");//离职确认回退

                if(mLADimissionSchema.getDepartState().equals("06"))
                {
                    mLAAgentSchema.setAgentState("01");
                }
                else if(mLADimissionSchema.getDepartState().equals("07"))
                {
                    mLAAgentSchema.setAgentState("02");
                }
                mLAAgentSchema.setOutWorkDate("");
                mLAAgentSchema.setOperator(mGlobalInput.Operator.toString());
                mLAAgentSchema.setModifyDate(currentDate);
                mLAAgentSchema.setModifyTime(currentTime);

                //删除laorphanpolicy表数据
                LAOrphanPolicyDB tLAOrphanPolicyDB = new LAOrphanPolicyDB();
                LAOrphanPolicySchema tLAOrphanPolicySchema = new LAOrphanPolicySchema();
                tLAOrphanPolicyDB.setAgentCode(mLADimissionSchema.getAgentCode());
                mLAOrphanPolicySet=tLAOrphanPolicyDB.query();

                //处理latree表数据
                LATreeDB tLATreeDB = new LATreeDB();
                LATreeSchema tLATreeSchema = new LATreeSchema();
                tLATreeDB.setAgentCode(tAgentCode);
                tLATreeDB.getInfo();
                tLATreeSchema=tLATreeDB.getSchema();
                String tAgentGroup=tLATreeSchema.getAgentGroup();//离职人员的团队
                String tBranchCode = tLATreeSchema.getBranchCode();//离职人员的团队
                String tAgentGrade = tLATreeSchema.getAgentGrade();//离职人员的行政职级
                String tAgentSeris=AgentPubFun.getAgentSeries(tAgentGrade);
                String tAgentGrade1 = tLATreeSchema.getAgentGrade1();//离职人员的业务职级

                if(tAgentSeris.equals("1")   )
                {
                 //组经理的处理离职,不包括见习经理
                 if (!dealManager(tAgentCode,tAgentGroup)) {
                  return false;
                  }
                }
                else if (tAgentSeris.equals("2"))
                {
                   //部经理离职的处理
                  if (!dealWarden(tAgentCode, tAgentGroup, tBranchCode))
                  {
                   return false;
                   }
                  }
                else if(tAgentSeris.equals("0"))
                {
                    if (!dealupAgentOne(tAgentGroup,"0")) {
                     buildError("dealupAgentOne", "修改团队主管时出错!");
                   return false;
                   }


                }

        }


        tReturn = true;
        return tReturn;
    }

    /**
     *  查询该需离职确认的业务员的所有保单，记录成孤儿单
     * @param cAgentCode String
     * @return boolean
     */
    private boolean createOrphanPolicy(String cAgentCode)
    {
        LATreeDB tLATreeDB = new LATreeDB();
        LATreeSchema tLATreeSchema = new LATreeSchema();
        tLATreeDB.setAgentCode(cAgentCode);
        tLATreeDB.getInfo();
        tLATreeSchema = tLATreeDB.getSchema();
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = new LCContSet();
        tLCContDB.setAgentCode(cAgentCode);
        tLCContSet = tLCContDB.query();
        if (tLCContSet.size()>0)
        {

            for (int i = 1; i <= tLCContSet.size(); i++)
            {
                //判断是否为撤销保单，要是为撤销保单则不生成孤儿单
                String tSQL = "select '1' from lccont where uwflag<>'a' and contno='"+tLCContSet.get(i).getContNo()+"'";
                ExeSQL tExeSQL = new ExeSQL();
                String Flag = tExeSQL.getOneValue(tSQL);
                if(Flag!=null&&!Flag.equals(""))
                {
                LAOrphanPolicySchema tLAOrphanPolicySchema = new LAOrphanPolicySchema();
                tLAOrphanPolicySchema.setContNo(tLCContSet.get(i).getContNo());
                tLAOrphanPolicySchema.setManageCom(tLCContSet.get(i).getManageCom());
                tLAOrphanPolicySchema.setAgentCode(cAgentCode);
                tLAOrphanPolicySchema.setAppntNo(tLCContSet.get(i).getAppntNo());
                tLAOrphanPolicySchema.setReasonType("0");
                tLAOrphanPolicySchema.setBranchType(tLATreeSchema.getBranchType());
                tLAOrphanPolicySchema.setBranchType2(tLATreeSchema.getBranchType2());
                tLAOrphanPolicySchema.setAgentGroup(tLATreeSchema.getBranchCode());
                tLAOrphanPolicySchema.setFlag("1");  //正常参与分配
                tLAOrphanPolicySchema.setMakeDate(PubFun.getCurrentDate());
                tLAOrphanPolicySchema.setMakeTime(PubFun.getCurrentTime());
                tLAOrphanPolicySchema.setModifyDate(PubFun.getCurrentDate());
                tLAOrphanPolicySchema.setModifyTime(PubFun.getCurrentTime());
                tLAOrphanPolicySchema.setOperator(mGlobalInput.Operator);
                mLAOrphanPolicySet.add(tLAOrphanPolicySchema);
            }
            }
        }
        return true;
    }

    /*处经理离职回退
    * 置入upagent
     * 此团队回复主管
    */
    private boolean dealManager(String tAgentCode,String tAgentGroup)
    {

        LATreeSet tLATreeSet = new LATreeSet();
        LATreeDB tLATreeDB = new LATreeDB();
        //查询团队内除了离职人员 的所有人,以便进行人员调动
        String tSql = "select agentgroup  from  latree a where agentgroup='" +tAgentGroup + "' "
                      +" and agentseries='1' and agentcode<>'" + tAgentCode + "'  and  not exists "
                      + " (select b.agentcode from laagent b where a.agentcode=b.agentcode and b.agentstate>='06' ) "
                      +" union "
                      +" select agentgroup from labranchgroup where agentgroup='"+tAgentGroup+"' and branchmanager is not null ";
        //tLATreeSet = tLATreeDB.executeQuery(tSql);
        ExeSQL mExeSQL = new ExeSQL();
        SSRS mSSRS = new SSRS();
        mSSRS = mExeSQL.execSQL(tSql);
        System.out.println(tSql);

        if(mSSRS.getMaxRow()>0)
        {
            buildError("dealManager", "回退人员的团队已经存在主管职级人员，无法进行回退!");
            return false;
        }
        else
        {
        //  团
        if (!dealBranch(tAgentGroup)) {
            buildError("dealManager", "修改团队为无主管团队时出错!");
            return false;
        }
        //置
        if (!dealupAgent(tAgentGroup,"1")) {
            buildError("dealManager", "修改团队为无主管团队时出错!");
            return false;
        }
        }
        return true;
    }
     //区经理的处理
    private boolean dealWarden(String tAgentCode,String tAgentGroup,String tBranchCode)
    {

        LATreeSet tLATreeSet = new LATreeSet();
        LATreeDB tLATreeDB = new LATreeDB();
        //查询
        String tSql = "select agentgroup from  latree a where agentgroup='" + tAgentGroup+ "' "
                      +" and agentseries='2' and agentcode<>'" + tAgentCode + "'"
                      +" and  not exists "
                      +" (select b.agentcode from laagent b where a.agentcode=b.agentcode and b.agentstate>='06' ) "
                      +" union "
                      +" select agentgroup from labranchgroup where agentgroup='"+tAgentGroup+"' and branchmanager is not null ";

       // tLATreeSet = tLATreeDB.executeQuery(tSql);
       ExeSQL mExeSQL = new ExeSQL();
      SSRS mSSRS = new SSRS();
      mSSRS = mExeSQL.execSQL(tSql);
      System.out.println(tSql);

      if(mSSRS.getMaxRow()>0)
      {
           buildError("dealWarden", "回退人员的团队已经存在主管职级人员，无法进行回退!");
           return false;
       }
       else
       {
               // 置原
           if (!dealBranch(tAgentGroup)) {
               buildError("dealWarden", "修改团队为无主管团队时出错!");
               return false;
           }
           //置upagent 为 空
           if (!dealupAgent(tAgentGroup,"2")) {
               buildError("dealWarden", "修改团队为无主管团队时出错!");
               return false;
           }
       }
        return true;
    }



    //处理主管所在的团队
    private boolean dealBranch(String tAgentGroup)
    {

        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(tAgentGroup);
        System.out.println(this.mLADimissionSchema.getAgentCode());
        tLABranchGroupSet = tLABranchGroupDB.query();
        for (int i = 1; i <= tLABranchGroupSet.size(); i++) {
            LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
            LABranchGroupBSchema tLABranchGroupBSchema = new LABranchGroupBSchema();
            tLABranchGroupSchema = tLABranchGroupSet.get(i);
            //备份labranchgroupb表
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLABranchGroupBSchema, tLABranchGroupSchema);
            System.out.println(tLABranchGroupBSchema.getAgentGroup() + "|||||2");
            tLABranchGroupBSchema.setEdorType("12"); //修改操作

            tLABranchGroupBSchema.setEdorNo(mEdorNo);
            tLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.getMakeDate());
            tLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.getMakeTime());
            tLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.
                                                 getModifyDate());
            tLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.
                                                 getModifyTime());
            tLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.getOperator());
            tLABranchGroupBSchema.setMakeDate(currentDate);
            tLABranchGroupBSchema.setMakeTime(currentTime);
            tLABranchGroupBSchema.setModifyDate(currentDate);
            tLABranchGroupBSchema.setModifyTime(currentTime);
            tLABranchGroupBSchema.setOperator(this.mGlobalInput.Operator);
            minLABranchGroupBSet.add(tLABranchGroupBSchema);
            //修改labranchgroup 中的主管为空
            tLABranchGroupSchema.setBranchManager(this.mLADimissionSchema.getAgentCode());
            tLABranchGroupSchema.setBranchManagerName(mLAAgentSchema.getName());
            tLABranchGroupSchema.setModifyDate(currentDate);
            tLABranchGroupSchema.setModifyTime(currentTime);
            tLABranchGroupSchema.setOperator(mGlobalInput.Operator);
            mupLABranchGroupSet.add(tLABranchGroupSchema);
        }
        return true ;
    }
    //处理主管所在的团队 (如果是区经理,则还要处理直辖处),使团队为无主管团队
    private boolean dealupAgent(String tAgentGroup,String tSeries)
    {
        //主管
        LATreeSet tLATreeSet = new LATreeSet();
        LATreeDB tLATreeDB = new LATreeDB();
        String tSql="";
        //查询
        if(tSeries.equals("1"))
        {
            tSql = "select * from  latree a where agentgroup='" + tAgentGroup+ "' "
                      +" and agentseries<'"+tSeries+"' "
                      +" and not exists "
                      +" (select b.agentcode from laagent b where a.agentcode=b.agentcode and b.agentstate>='06' ) ";

        }
        else if(tSeries.equals("2"))
        {
           tSql = "select * from  latree a  where "
                     +" a.agentgroup in (select agentgroup  from labranchgroup where branchtype='3' and branchtype2='01' "
                     +" and upbranch='"+tAgentGroup +"' and endflag='N')"
                      +" and a.agentseries='1' "
                      +" and not exists "
                      +" (select b.agentcode from laagent b where a.agentcode=b.agentcode and b.agentstate>='06' ) ";
        }

        tLATreeSet = tLATreeDB.executeQuery(tSql);
         System.out.println("come here 1"+tSql);
        for (int i=1;i<=tLATreeSet.size();i++)
        {
            LATreeSchema tLATreeSchema= new LATreeSchema();
            tLATreeSchema=tLATreeSet.get(i);
            LATreeBSchema tLATreeBSchema= new LATreeBSchema();
            Reflections tReflections= new Reflections();
            tReflections.transFields(tLATreeBSchema,tLATreeSchema) ;
            tLATreeBSchema.setEdorNO(mEdorNo);
            tLATreeBSchema.setRemoveType("12");
            tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
            tLATreeBSchema.setMakeDate2(tLATreeBSchema.getMakeDate());
            tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
            tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
            tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
             tLATreeBSchema.setOperator(mGlobalInput.Operator);
            tLATreeBSchema.setMakeDate(currentDate);
            tLATreeBSchema.setMakeTime(currentTime);
            tLATreeBSchema.setModifyDate(currentDate);
            tLATreeBSchema.setModifyTime(currentTime);
            mLATreeBSet.add(tLATreeBSchema);
            tLATreeSchema.setUpAgent(this.mLADimissionSchema.getAgentCode());
            tLATreeSchema.setModifyDate(currentDate);
            tLATreeSchema.setModifyTime(currentTime);
            mLATreeSet.add(tLATreeSchema);
        }
        if(tSeries.equals("1")){
            String tSql1 = "select * from labranchgroup  "
                           +" where agentgroup =(select upbranch from labranchgroup where agentgroup='" + tAgentGroup+ "')"
                           +" and branchmanager is not null"
                           +" and branchmanager in (select agentcode from laagent"
                           +" where agentgroup=(select upbranch from labranchgroup where agentgroup='" + tAgentGroup+ "') "
                         +"  and agentstate<='02')";

          LABranchGroupSet ttLABranchGroupSet = new LABranchGroupSet();
          LABranchGroupDB ttLABranchGroupDB = new LABranchGroupDB();
          ttLABranchGroupSet=ttLABranchGroupDB.executeQuery(tSql1);
          System.out.println("come here 2"+tSql1);
          if (ttLABranchGroupSet.size()>0)
          {
              String upagent=ttLABranchGroupSet.get(1).getBranchManager();
              LATreeSet ttLATreeSet = new LATreeSet();
              LATreeDB ttLATreeDB = new LATreeDB();
              ttLATreeDB.setAgentCode(this.mLADimissionSchema.getAgentCode());
              ttLATreeSet=ttLATreeDB.query();
              for (int i=1;i<=ttLATreeSet.size();i++)
             {
            LATreeSchema tLATreeSchema= new LATreeSchema();
            tLATreeSchema=ttLATreeSet.get(i);
            LATreeBSchema tLATreeBSchema= new LATreeBSchema();
            Reflections tReflections= new Reflections();
            tReflections.transFields(tLATreeBSchema,tLATreeSchema) ;
            tLATreeBSchema.setEdorNO(mEdorNo);
            tLATreeBSchema.setRemoveType("12");
            tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
            tLATreeBSchema.setMakeDate2(tLATreeBSchema.getMakeDate());
            tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
            tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
            tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
             tLATreeBSchema.setOperator(mGlobalInput.Operator);
            tLATreeBSchema.setMakeDate(currentDate);
            tLATreeBSchema.setMakeTime(currentTime);
            tLATreeBSchema.setModifyDate(currentDate);
            tLATreeBSchema.setModifyTime(currentTime);
            mLATreeBSet.add(tLATreeBSchema);
            tLATreeSchema.setUpAgent(upagent);
            tLATreeSchema.setModifyDate(currentDate);
            tLATreeSchema.setModifyTime(currentTime);
            mLATreeSet.add(tLATreeSchema);
        }

          }

        }
        return true ;
    }

    //处理主管所在的团队 (如果是区经理,则还要处理直辖处),使团队为无主管团队
       private boolean dealupAgentOne(String tAgentGroup,String tSeries)
       {
            if(tSeries.equals("0")){
               String tSql1 = "select * from labranchgroup  "
                              +" where agentgroup='" + tAgentGroup+ "'"
                              +" and branchmanager is not null"
                              +" and branchmanager in (select agentcode from laagent"
                              +" where  agentgroup='" + tAgentGroup+ "' "
                              +"  and agentstate<='02')";

             LABranchGroupSet ttLABranchGroupSet = new LABranchGroupSet();
             LABranchGroupDB ttLABranchGroupDB = new LABranchGroupDB();
             ttLABranchGroupSet=ttLABranchGroupDB.executeQuery(tSql1);
             System.out.println("come here 2"+tSql1);
             if (ttLABranchGroupSet.size()>0)
             {
                 String upagent=ttLABranchGroupSet.get(1).getBranchManager();
                 LATreeSet ttLATreeSet = new LATreeSet();
                 LATreeDB ttLATreeDB = new LATreeDB();
                 ttLATreeDB.setAgentCode(this.mLADimissionSchema.getAgentCode());
                 ttLATreeSet=ttLATreeDB.query();
                 for (int i=1;i<=ttLATreeSet.size();i++)
                {
               LATreeSchema tLATreeSchema= new LATreeSchema();
               tLATreeSchema=ttLATreeSet.get(i);
               LATreeBSchema tLATreeBSchema= new LATreeBSchema();
               Reflections tReflections= new Reflections();
               tReflections.transFields(tLATreeBSchema,tLATreeSchema) ;
               tLATreeBSchema.setEdorNO(mEdorNo);
               tLATreeBSchema.setRemoveType("12");
               tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
               tLATreeBSchema.setMakeDate2(tLATreeBSchema.getMakeDate());
               tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
               tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
               tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
                tLATreeBSchema.setOperator(mGlobalInput.Operator);
               tLATreeBSchema.setMakeDate(currentDate);
               tLATreeBSchema.setMakeTime(currentTime);
               tLATreeBSchema.setModifyDate(currentDate);
               tLATreeBSchema.setModifyTime(currentTime);
               mLATreeBSet.add(tLATreeBSchema);
               tLATreeSchema.setUpAgent(upagent);
               tLATreeSchema.setModifyDate(currentDate);
               tLATreeSchema.setModifyTime(currentTime);
               mLATreeSet.add(tLATreeSchema);
           }

             }

           }
           return true ;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
         this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        this.mLADimissionSchema.setSchema((LADimissionSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LADimissionSchema",
                                                  0));

        this.mFlag=(String)cInputData.getObjectByObjectName("String",0);

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start BankDimissionBackBLQuery Submit...");
        LADimissionDB tLADimissionDB = new LADimissionDB();
        tLADimissionDB.setSchema(this.mLADimissionSchema);
        this.mLADimissionSet = tLADimissionDB.query();
        this.mResult.add(this.mLADimissionSet);
        System.out.println("End BankDimissionBackBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLADimissionDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLADimissionDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "BankDimissionBackBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mMap.put(this.mLADimissionSchema,"DELETE");
            this.mMap.put(this.mLAAgentSchema,"UPDATE");
            this.mMap.put(this.mLAAgentBSchema,"INSERT");
            this.mMap.put(this.mupLABranchGroupSet,"UPDATE");
            this.mMap.put(this.minLABranchGroupBSet,"INSERT");
            this.mMap.put(this.mLATreeSet,"UPDATE");
            this.mMap.put(this.mLATreeBSet,"INSERT");
            this.mMap.put(this.mLAOrphanPolicySet,"DELETE");
            mInputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankDimissionBackBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    /**
     * 校验函数
     * 对传入的数据进行信息校验
     * @return boolean
     */
    private boolean checkData()
    {
        int tCount;
        String tAgentCode;
        tAgentCode = mLADimissionSchema.getAgentCode(); //得到离职人员编码

        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        //查询代理人表
        tCount = tLAAgentDB.getCount();
        System.out.println("tCount:" + Integer.toString(tCount));
        //对查询结果判定
        if (tCount == -1)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "BankDimissionBackBL";
            tError.functionName = "checkData()";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else if (tCount == 0)
        {
            //代理人编码录入错误
            CError tError = new CError();
            tError.moduleName = "BankDimissionBackBL";
            tError.functionName = "checkData()";
            tError.errorMessage = "没有这个代理人!";
            this.mErrors.addOneError(tError);
            return false;
        }
         return true;
    }
    //错误处理
    private void buildError(String szFunc, String szErrMsg) {
            CError cError = new CError();

            cError.moduleName = "BankDimissionBackBL";
            cError.functionName = szFunc;
            cError.errorMessage = szErrMsg;
            this.mErrors.addOneError(cError);
    }
}
