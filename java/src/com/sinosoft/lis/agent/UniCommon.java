package com.sinosoft.lis.agent;

import com.cbsws.obj.RspUniSalescod;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.sys.UnisalescodFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;

public class UniCommon {
	public CErrors mErrors = new CErrors();

	public boolean getGroupAgentCode(String mOperate,String aAgentCode,LAAgentSchema mLAAgentSchema,String tQualifNo){
		/**获取集团工号Unisalescod*/
        String tSQL = "select GroupAgentCode from laagent where agentcode='"+mLAAgentSchema.getAgentCode()+"'";
        String mGroupAgentCode = new ExeSQL().getOneValue(tSQL);
        UnisalescodFun fun=new UnisalescodFun();
        RspUniSalescod mRspUniSalescod = new RspUniSalescod();
        if(mOperate.equals("INSERT||MAIN")){
        	mRspUniSalescod=fun.getUnisalescod(mOperate, mLAAgentSchema, null,null,tQualifNo);
//        	mLAAgentSchema.setGroupAgentCode(mRspUniSalescod.getUNI_SALES_COD());
        	if(mRspUniSalescod.getUNI_SALES_COD() != null && !"".equals(mRspUniSalescod.getUNI_SALES_COD())){
        		String tTySQL = "update laagent set GroupAgentCode = '"+mRspUniSalescod.getUNI_SALES_COD()+"' where agentcode = '"+aAgentCode+"'";
            	new ExeSQL().execUpdateSQL(tTySQL);
        	}
        }else if(mOperate.equals("UPDATE||MAIN")){
        	mRspUniSalescod=fun.getUnisalescod(mOperate, mLAAgentSchema, null,mGroupAgentCode,null );
        }
        if("01".equals(mRspUniSalescod.getMESSAGETYPE()))
		{
			 CError tError = new CError();
	         tError.moduleName = "LAMediAgentBL";
	         tError.functionName = "dealData";
	         tError.errorMessage = mRspUniSalescod.getERRDESC();
	         mErrors.addOneError(tError);
	         System.out.println("通过集团调用统一工号失败："+mRspUniSalescod.getERRDESC());
			return false;
		}
	    return true;
	}
}