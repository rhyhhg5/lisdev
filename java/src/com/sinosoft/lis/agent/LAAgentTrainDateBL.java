package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;


/**
 * <p>Title: AgentSystem</p>
 * <p>Description: 销售管理——增员管理</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author xjh
 * @version 1.0
 */

public class LAAgentTrainDateBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private MMap map = new MMap();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();
    private LATreeBSet mLATreeBSet  = new LATreeBSet();
    
    private LAAgentSet mupLAAgentSet = new LAAgentSet();
    private LATreeSet mupLATreeSet = new LATreeSet();

    private String mEdorNo = "";


    public LAAgentTrainDateBL() {
    }


    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Begin LAAgentTrainDateBL.submitData.........");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentTrainDateBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                                      getObjectByObjectName("LAAgentSchema", 0));
     
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentTrainDateBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 业务处理主函数
     * @return boolean
     */
    public boolean dealData() {
    	LAAgentDB tLAAgentDB = new LAAgentDB();
    	tLAAgentDB.setAgentCode(this.mLAAgentSchema.getAgentCode());
    	if (!tLAAgentDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentTrainDateBL";
            tError.functionName = "setInDueFormDate";
            tError.errorMessage = "查询不到" + mLAAgentSchema.getAgentCode() + "的人员信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
    	LAAgentSchema tLAAgentSchema = new LAAgentSchema();
    	tLAAgentSchema =tLAAgentDB.getSchema();
    	if(!backAgent(tLAAgentSchema)) return false;
    	tLAAgentSchema.setNoWorkFlag(this.mLAAgentSchema.getNoWorkFlag());
    	tLAAgentSchema.setTrainDate(this.mLAAgentSchema.getTrainDate());
    	tLAAgentSchema.setWageVersion("2013A");
    	tLAAgentSchema.setModifyDate(this.currentDate);
    	tLAAgentSchema.setModifyTime(this.currentTime);
    	tLAAgentSchema.setOperator(this.mGlobalInput.Operator);
    	tLAAgentSchema.setGBuildFlag("");
    	tLAAgentSchema.setGBuildStartDate("");
    	tLAAgentSchema.setGBuildEndDate("");
    	this.mupLAAgentSet.add(tLAAgentSchema);
    	map.put(mupLAAgentSet, "UPDATE");
    	
    	 LATreeDB tLATreeDB = new LATreeDB();
         tLATreeDB.setAgentCode(tLAAgentSchema.getAgentCode());
         tLATreeDB.getInfo();
         LATreeSchema tLATreeSchema = new LATreeSchema();
         tLATreeSchema=tLATreeDB.getSchema();
         tLATreeSchema.setWageVersion("2013A");
         tLATreeSchema.setAstartDate(this.mLAAgentSchema.getTrainDate());
         tLATreeSchema.setModifyDate(this.currentDate);
         tLATreeSchema.setModifyTime(this.currentTime);
         tLATreeSchema.setOperator(this.mGlobalInput.Operator);
         this.mupLATreeSet.add(tLATreeSchema);
         map.put(mupLATreeSet, "UPDATE");
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentTrainDateBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean backAgent(LAAgentSchema tLAAgentSchema)
    {
    	mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
  	  Reflections tReflections = new Reflections();
  	  LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
        tReflections.transFields(tLAAgentBSchema, tLAAgentSchema);
        tLAAgentBSchema.setEdorNo(mEdorNo);
        tLAAgentBSchema.setEdorType("02");// 02 表示团队调动操作
        tLAAgentBSchema.setMakeDate(currentDate);
        tLAAgentBSchema.setMakeTime(currentTime);
        tLAAgentBSchema.setModifyDate(currentDate);
        tLAAgentBSchema.setModifyTime(currentTime);
        tLAAgentBSchema.setOperator(mGlobalInput.Operator);
        this.mLAAgentBSet.add(tLAAgentBSchema);
        map.put(mLAAgentBSet, "INSERT");

        LATreeDB tLATreeDB = new LATreeDB();
        System.out.println("人员编码--->>"+tLAAgentSchema.getAgentCode());
        tLATreeDB.setAgentCode(tLAAgentSchema.getAgentCode());
        tLATreeDB.getInfo();
        LATreeBSchema tLATreeBSchema = new LATreeBSchema();
        tReflections.transFields(tLATreeBSchema,
                                 tLATreeDB.getSchema());
        tLATreeBSchema.setEdorNO(mEdorNo);
        tLATreeBSchema.setRemoveType("02");
        tLATreeBSchema.setMakeDate2(tLATreeDB.getMakeDate());
        tLATreeBSchema.setMakeTime2(tLATreeDB.getMakeTime());
        tLATreeBSchema.setModifyDate2(tLATreeDB.getModifyDate());
        tLATreeBSchema.setModifyTime2(tLATreeDB.getModifyTime());
        tLATreeBSchema.setOperator2(tLATreeDB.getOperator());
        tLATreeBSchema.setMakeDate(currentDate);
        tLATreeBSchema.setMakeTime(currentTime);
        tLATreeBSchema.setModifyDate(currentDate);
        tLATreeBSchema.setModifyTime(currentTime);
        tLATreeBSchema.setOperator(mGlobalInput.Operator);
        this.mLATreeBSet.add(tLATreeBSchema);
        map.put(mLATreeBSet, "INSERT");
        
  	  return true;
    }

 

}
