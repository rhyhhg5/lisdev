package com.sinosoft.lis.agent;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LARearRelationDB;
import com.sinosoft.lis.db.LARecomRelationDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LARearRelationBSchema;
import com.sinosoft.lis.schema.LARearRelationSchema;
import com.sinosoft.lis.schema.LARecomRelationBSchema;
import com.sinosoft.lis.schema.LARecomRelationSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LARearRelationBSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.vschema.LARecomRelationBSet;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LANewToFormBL {
    public LANewToFormBL() {
    }
    //错误处理类
    public CErrors mErrors = new CErrors(); //错误处理类
          /** 全局数据 */
    private VData mInputData = new VData();
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String mNewEmploydate = "";// 页面接受新入司人员入司时间
    private String mAgentCode= "";     // 页面接受人员编码
    private String mIntroAgency= "";   // 页面接受新推荐人
    private String mNewTrainDate= "";  // 页面接受新筹备时间
    private String mAgentGrade = "";   // 职级
    private String mAgentGroup = "";
    
    private int mChoice;
    private String mEdorNo= "";
    private MMap map = new MMap();
    private TransferData mTransferData = new TransferData();    
    private  GlobalInput mGlobalInput = new GlobalInput();
    private Reflections ref = new Reflections();
    private VData mResult = new VData();
    private PubFun1 mPubFun1 = new PubFun1();
    
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();
    private LATreeBSet mLATreeBSet = new LATreeBSet();
    private LARecomRelationSet mIntroAgentcyReComSet = new LARecomRelationSet();//存储新推荐推荐关系
    
    private LARecomRelationSet mInLARecomRelationSet = new LARecomRelationSet();
    private LARecomRelationBSet mInLARecomRelationBSet = new LARecomRelationBSet();
    private LARecomRelationSet mDelLARecomRelationSet = new LARecomRelationSet();//删除的推荐关系
    
    private LARearRelationBSet mInLARearRelationBSet = new LARearRelationBSet();
    private LARearRelationSet mDelLARearRelationSet = new LARearRelationSet();
    private LARearRelationSet mInLARearRelationSet = new LARearRelationSet();
    

          /**
            传输数据的公共方法
           */
    public boolean submitData(VData cInputData, String cOperate) {
      System.out.println("Begin LANewToFormBL.submitData.........");
      //将操作数据拷贝到本类中
      this.mOperate = cOperate;
      //得到外部传入的数据,将数据备份到本类中
      if (!getInputData(cInputData)) {
        return false;
      }
      //进行业务处理
      if (!checkData()) {
          return false;
        }
      if (!dealData()) {
        return false;
      }
      //准备往后台的数据
      if (!prepareOutputData()) {
        return false;
      }
      PubSubmit tPubSubmit = new PubSubmit();
      System.out.println("Start LANewToFormBL Submit...");
      if (!tPubSubmit.submitData(mInputData, mOperate)) {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
        CError tError = new CError();
        tError.moduleName = "LANewToFormBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据提交失败!";
        this.mErrors.addOneError(tError);
        return false;
      }
      return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
      //全局变量
      try {
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mTransferData=(TransferData)cInputData.getObjectByObjectName("TransferData", 0);
        this.mAgentCode=(String)mTransferData.getValueByName("AgentCode");
        this.mNewEmploydate=(String)mTransferData.getValueByName("NewEmploydate");
        this.mIntroAgency=(String)mTransferData.getValueByName("IntroNewAgency");
        this.mNewTrainDate=(String)mTransferData.getValueByName("TrainDate");
        this.mAgentGroup=(String)mTransferData.getValueByName("AgentGroup");
        this.mAgentGrade=(String)mTransferData.getValueByName("AgentGrade");
      }
      catch (Exception ex) {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "LANewToFormBL";
        tError.functionName = "getInputData";
        tError.errorMessage = "在读取处理所需要的数据时出错。";
        this.mErrors.addOneError(tError);
        return false;
      }
      System.out.println("getInputData end ");
      return true;
    }
    /*
     *  简单数据校验
     */
    private boolean checkData()
    {
    	// 薪资校验

//    	String sql = "select distinct 1 from lawage where branchtype = '1' and branchtype2 = '01' and agentcode = '"+this.mAgentCode+"'";
     	ExeSQL tExeSQL = new ExeSQL();
//    	String Result=tExeSQL.getOneValue(sql);
//    	if(Result!=null&&!Result.equals(""))
//    	{
//    		CError.buildErr(this, "业务员已进行过薪资计算,无法变更业务员相关信息！");
//            return false;
//    	}
        // 筹备标记不为Y 不能录入筹备时间
    	String tNoworkflag = getNoworkflag(this.mAgentCode);
    	if(tNoworkflag==null||tNoworkflag.equals("N")||tNoworkflag.equals(""))
    	{
    		if(this.mNewTrainDate!=null&&!this.mNewTrainDate.equals(""))
    		{
    			CError.buildErr(this, "该业务员未进行筹备标记,不能修改筹备时间！");
                return false;
    		}
    	}
    	// 新推荐人必须和业务员所属同一个成本中心
    	if(this.mIntroAgency!=null&&!this.mIntroAgency.equals(""))
    	{
    		String tsql = "select count(distinct costcenter) from labranchgroup where 1=1 and "
                   +" agentgroup in (select agentgroup from laagent where agentcode in ('"+this.mAgentCode+"','"+this.mIntroAgency+"'))";
    		String tCount = tExeSQL.getOneValue(tsql);
    		if(tCount!=null&&!tCount.equals("1"))
    		{
    			CError.buildErr(this, "业务员与推荐人所属团队成本中心不一致！");
                return false;
    		}
    	}
    	
    	return true;
    }
   /*
    *  业务逻辑处理
    */
     
    private boolean dealData()
    {
    	this.mChoice=SelectProceed();
    	switch (mChoice) {
        case 0: //仅仅修改人员入司时间
            if (!Proceedzero()) 
            {
                CError.buildErr(this, "入司时间调整失败！");
                return false;
            }
            break;
        case 1: //仅仅修改筹备时间
            if (!Proceedone()) 
            {
                CError.buildErr(this, "筹备时间调整失败！");
                return false;
            }
            break;
        case 2://入司时间和筹备时间都调整
            if (!Proceedtwo()) 
            {
                CError.buildErr(this, "入司时间和筹备时间调整失败！");
                return false;
            }
            break;
        case 3://仅修改推荐人
            if (!Proceedthree()) 
            {
                CError.buildErr(this, "推荐人调整失败！");
                return false;
            }
            break;
        default: //没有任何变动
        }
    	return true;
    }
    /*
     *选择操作处理
     *返回：1-Proceedone  2-Proceedtwo  3-Proceedthree  ...-其它情况
     */
    private int SelectProceed() 
    {
      if(this.mNewEmploydate!=null&&!this.mNewEmploydate.trim().equals("")&&(this.mNewTrainDate==null||this.mNewTrainDate.trim().equals("")))
      {
    	  return 0;
      }	
      if((this.mNewEmploydate==null||this.mNewEmploydate.trim().equals(""))&&this.mNewTrainDate!=null&&!this.mNewTrainDate.trim().equals(""))
      {
    	  return 1;
      }      
      if(this.mNewEmploydate!=null&&!this.mNewEmploydate.trim().equals("")&&this.mNewTrainDate!=null&&!this.mNewTrainDate.trim().equals(""))
      {
    	  return 2;
      }
      if((this.mNewEmploydate==null||this.mNewEmploydate.trim().equals(""))&&(this.mNewTrainDate==null||this.mNewTrainDate.trim().equals("")))
      {
    	  return 3;
      }
      return -1;
    }
    
    /*
     *  仅仅修改人员入司时间
     */
    private boolean Proceedzero()
    {
    	this.mEdorNo=mPubFun1.CreateMaxNo("EdorNo", 20);
    	LAAgentDB tLAAgentDB = new LAAgentDB();
    	String sql = "select * from laagent where agentcode = '"+this.mAgentCode+"'";
    	System.out.println("业务员查询sql:"+sql);
    	LAAgentSet tLAAgentSet =tLAAgentDB.executeQuery(sql);
    	for(int i= 1;i<=tLAAgentSet.size();i++)
    	{
    		LAAgentSchema tLAAgentSchema = new LAAgentSchema();
    		tLAAgentSchema=tLAAgentSet.get(i);
    		LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
    		ref.transFields(tLAAgentBSchema, tLAAgentSchema);
 		    tLAAgentBSchema.setEdorType("98");// 新入司人员入司时间修改 edorno:98
 		    tLAAgentBSchema.setEdorNo(mEdorNo);
 		    mLAAgentBSet.add(tLAAgentBSchema);
 		    
 		    tLAAgentSchema.setEmployDate(this.mNewEmploydate);
 		    if(tLAAgentSchema.getNoWorkFlag()!=null&&tLAAgentSchema.getNoWorkFlag().equals("Y"))
 		    {
 		    	if(tLAAgentSchema.getTrainDate().compareTo(this.mNewEmploydate)<=0)
 		    	{
 		    		tLAAgentSchema.setTrainDate(this.mNewEmploydate);// 新入司时间大于筹备时间：以新筹备时间为准
 		    	}
 		    }
 		    tLAAgentSchema.setModifyDate(this.CurrentDate);
 		    tLAAgentSchema.setModifyTime(this.CurrentTime);
 		    
 		   
 		  LATreeDB tLATreeDB  = new LATreeDB();
 		  String sql_tree = "select * from latree where agentcode = '"+this.mAgentCode+"'";
 		  LATreeSet tLATreeSet = tLATreeDB.executeQuery(sql_tree);
 		  for(int j =1;j<=tLATreeSet.size();j++)
 		  {
 			 LATreeSchema tLATreeSchema = new LATreeSchema();
 			 tLATreeSchema=tLATreeSet.get(j);
 			 LATreeBSchema tLATreeBSchema = new LATreeBSchema();
 			 
 			 ref.transFields(tLATreeBSchema, tLATreeSchema);
			 tLATreeBSchema.setEdorNO(this.mEdorNo);	
			 tLATreeBSchema.setRemoveType("98");
			 tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
			 tLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
			 tLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
			 tLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
			 tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
			   
			 tLATreeBSchema.setOperator(mGlobalInput.Operator);
			 tLATreeBSchema.setMakeDate(this.CurrentDate);
			 tLATreeBSchema.setMakeTime(this.CurrentTime);
			 tLATreeBSchema.setModifyDate(this.CurrentDate);
			 tLATreeBSchema.setModifyTime(this.CurrentTime);
			 
			 this.mLATreeBSet.add(tLATreeBSchema);
			 
 			 tLATreeSchema.setStartDate(this.mNewEmploydate);
 			 tLATreeSchema.setModifyDate(this.CurrentDate);
 			 tLATreeSchema.setModifyTime(this.CurrentTime);

 				 tLATreeSchema.setAstartDate(this.mNewEmploydate);
 				 // 筹备版本调整
 	 		    if(tLAAgentSchema.getNoWorkFlag()!=null&&tLAAgentSchema.getNoWorkFlag().equals("Y"))
 	 		    {
 	 	 			 if(tLATreeSchema.getAstartDate().compareTo(this.mNewEmploydate)>=0)
 	 	 			 {
 	 	 				// 如果该业务员没有筹备,则将业务员职级最近次调整时间置为入司时间
 	 	 			 }
 	 			 if((("B".equals(tLATreeSchema.getAgentGrade().substring(0,1).trim()))&&this.mNewEmploydate.compareTo("2012-04-01")>=0&&mNewEmploydate.compareTo("2012-06-30")<=0)||
 	 			  (("B".equals(tLATreeSchema.getAgentGrade().substring(0,1).trim())&&mNewEmploydate.compareTo("2013-01-01")>=0)))
 	 			   {
 	 				  tLAAgentSchema.setWageVersion("2012A");
 	 				  tLATreeSchema.setWageVersion("2012A");
 	 			   }else{
 	 				  tLAAgentSchema.setWageVersion("2010A");
 	 				  tLATreeSchema.setWageVersion("2010A");
 	 			   }
 	 		    }else{
 	 		    	tLATreeSchema.setAstartDate(this.mNewEmploydate);
 	 		    }
 			 // 推荐关系调整
 			  if(this.mIntroAgency!=null&&!this.mIntroAgency.equals(""))
	  		    {
	 				tLATreeSchema.setIntroAgency(this.mIntroAgency);
	  			    if(!dealRecom()){return false;}
	  		    }
 			 this.mLATreeSet.add(tLATreeSchema);
 		  }
 		  this.mLAAgentSet.add(tLAAgentSchema);
 		  
    	}
    	return true;
    }
    
    /*
     *  仅仅修改人员筹备时间
     */
    private boolean Proceedone()
    {
    	this.mEdorNo=mPubFun1.CreateMaxNo("EdorNo", 20);
    	LAAgentDB tLAAgentDB = new LAAgentDB();
    	String sql = "select * from laagent where agentcode = '"+this.mAgentCode+"'";
    	LAAgentSet tLAAgentSet =tLAAgentDB.executeQuery(sql);
    	for(int i= 1;i<=tLAAgentSet.size();i++)
    	{
    		LAAgentSchema tLAAgentSchema = new LAAgentSchema();
    		tLAAgentSchema=tLAAgentSet.get(i);
    		LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
    		ref.transFields(tLAAgentBSchema, tLAAgentSchema);
 		    tLAAgentBSchema.setEdorType("98");// 新入司人员入司时间修改 edorno:98
 		    tLAAgentBSchema.setEdorNo(mEdorNo);
 		    mLAAgentBSet.add(tLAAgentBSchema);
 		    
 		    if(tLAAgentSchema.getEmployDate().compareTo(this.mNewTrainDate)>0)
 		    {
 		    	CError.buildErr(this, "人员新筹备时间应该小于入司时间！");
 		    	return false;
 		    }else{
 		    tLAAgentSchema.setModifyDate(this.CurrentDate);
 		    tLAAgentSchema.setModifyTime(this.CurrentTime);
 		    tLAAgentSchema.setTrainDate(this.mNewTrainDate);
 		    
 		  LATreeDB tLATreeDB  = new LATreeDB();
 		  String sql_tree = "select * from latree where agentcode = '"+this.mAgentCode+"'";
 		  LATreeSet tLATreeSet = tLATreeDB.executeQuery(sql_tree);
 		  for(int j =1;j<=tLATreeSet.size();j++)
 		  {
 			 LATreeSchema tLATreeSchema = new LATreeSchema();
 			 tLATreeSchema=tLATreeSet.get(j);
 			 LATreeBSchema tLATreeBSchema = new LATreeBSchema();
 			 
 			 ref.transFields(tLATreeBSchema, tLATreeSchema);
			 tLATreeBSchema.setEdorNO(this.mEdorNo);	
			 tLATreeBSchema.setRemoveType("98");
			 tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
			 tLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
			 tLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
			 tLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
			 tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
			   
			 tLATreeBSchema.setOperator(mGlobalInput.Operator);
			 tLATreeBSchema.setMakeDate(this.CurrentDate);
			 tLATreeBSchema.setMakeTime(this.CurrentTime);
			 tLATreeBSchema.setModifyDate(this.CurrentDate);
			 tLATreeBSchema.setModifyTime(this.CurrentTime);
			 
			 this.mLATreeBSet.add(tLATreeBSchema);
			 
 			 tLATreeSchema.setAstartDate(this.mNewTrainDate);
 			 tLATreeSchema.setModifyDate(this.CurrentDate);
 			 tLATreeSchema.setModifyTime(this.CurrentTime);
 				 // 筹备版本调整
 			if(tLAAgentSchema.getNoWorkFlag()!=null&&tLAAgentSchema.getNoWorkFlag().equals("Y"))
	 		    {
	 			 if((("B".equals(tLATreeSchema.getAgentGrade().substring(0,1).trim()))&&this.mNewEmploydate.compareTo("2012-04-01")>=0&&mNewEmploydate.compareTo("2012-06-30")<=0)||
	 			  (("B".equals(tLATreeSchema.getAgentGrade().substring(0,1).trim())&&mNewEmploydate.compareTo("2013-01-01")>=0)))
	 			   {
	 				  tLAAgentSchema.setWageVersion("2012A");
	 				  tLATreeSchema.setWageVersion("2012A");
	 			   }else{
	 				  tLAAgentSchema.setWageVersion("2010A");
	 				  tLATreeSchema.setWageVersion("2010A");
	 			   }
	 		    }
 			 // 推荐关系调整
 			if(this.mIntroAgency!=null&&!this.mIntroAgency.equals(""))
	  		    {
	 				tLATreeSchema.setIntroAgency(this.mIntroAgency);
	  			    if(!dealRecom()){return false;}
	  		    }
 			 this.mLATreeSet.add(tLATreeSchema);
 		     this.mLAAgentSet.add(tLAAgentSchema);
    	}
      }
    }    
    	return true;
    }
    
    /*
     * 入司时间和筹备时间都调整
     */
    private boolean Proceedtwo()
    {
    	this.mEdorNo=mPubFun1.CreateMaxNo("EdorNo", 20);
    	if(this.mNewEmploydate.compareTo(this.mNewTrainDate)>0)
    	{
    		CError.buildErr(this, "人员新筹备时间应该小于入司时间！");
		    return false;
    	}else{
    		this.mEdorNo=mPubFun1.CreateMaxNo("EdorNo", 20);
        	LAAgentDB tLAAgentDB = new LAAgentDB();
        	String sql = "select * from laagent where agentcode = '"+this.mAgentCode+"'";
        	LAAgentSet tLAAgentSet =tLAAgentDB.executeQuery(sql);
        	for(int i= 1;i<=tLAAgentSet.size();i++)
        	{
        		LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        		tLAAgentSchema=tLAAgentSet.get(i);
        		LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
        		ref.transFields(tLAAgentBSchema, tLAAgentSchema);
     		    tLAAgentBSchema.setEdorType("98");// 新入司人员入司时间修改 edorno:98
     		    tLAAgentBSchema.setEdorNo(mEdorNo);
     		    mLAAgentBSet.add(tLAAgentBSchema);
     		    
     		    tLAAgentSchema.setEmployDate(this.mNewEmploydate);
     		    tLAAgentSchema.setTrainDate(this.mNewTrainDate);
     		    tLAAgentSchema.setModifyDate(this.CurrentDate);
     		    tLAAgentSchema.setModifyTime(this.CurrentTime);
     		    
     		   
     		  LATreeDB tLATreeDB  = new LATreeDB();
     		  String sql_tree = "select * from latree where agentcode = '"+this.mAgentCode+"'";
     		  LATreeSet tLATreeSet = tLATreeDB.executeQuery(sql_tree);
     		  for(int j =1;j<=tLATreeSet.size();j++)
     		  {
     			 LATreeSchema tLATreeSchema = new LATreeSchema();
     			 tLATreeSchema=tLATreeSet.get(j);
     			 LATreeBSchema tLATreeBSchema = new LATreeBSchema();
     			 
     			 ref.transFields(tLATreeBSchema, tLATreeSchema);
    			 tLATreeBSchema.setEdorNO(this.mEdorNo);	
    			 tLATreeBSchema.setRemoveType("98");
    			 tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
    			 tLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
    			 tLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
    			 tLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
    			 tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
    			   
    			 tLATreeBSchema.setOperator(mGlobalInput.Operator);
    			 tLATreeBSchema.setMakeDate(this.CurrentDate);
    			 tLATreeBSchema.setMakeTime(this.CurrentTime);
    			 tLATreeBSchema.setModifyDate(this.CurrentDate);
    			 tLATreeBSchema.setModifyTime(this.CurrentTime);
    			 
    			 this.mLATreeBSet.add(tLATreeBSchema);
    			 
     			 tLATreeSchema.setStartDate(this.mNewEmploydate);
     			 tLATreeSchema.setAstartDate(this.mNewTrainDate);
     			 tLATreeSchema.setModifyDate(this.CurrentDate);
     			 tLATreeSchema.setModifyTime(this.CurrentTime);
     				 // 筹备版本调整
     			if(tLAAgentSchema.getNoWorkFlag()!=null&&tLAAgentSchema.getNoWorkFlag().equals("Y"))
 	 		    {
 	 			 if((("B".equals(tLATreeSchema.getAgentGrade().substring(0,1).trim()))&&this.mNewEmploydate.compareTo("2012-04-01")>=0&&mNewEmploydate.compareTo("2012-06-30")<=0)||
 	 			  (("B".equals(tLATreeSchema.getAgentGrade().substring(0,1).trim())&&mNewEmploydate.compareTo("2013-01-01")>=0)))
 	 			   {
 	 				  tLAAgentSchema.setWageVersion("2012A");
 	 				  tLATreeSchema.setWageVersion("2012A");
 	 			   }else{
 	 				  tLAAgentSchema.setWageVersion("2010A");
 	 				  tLATreeSchema.setWageVersion("2010A");
 	 			   }
 	 		    }
     			 // 推荐关系调整
     			if(this.mIntroAgency!=null&&!this.mIntroAgency.equals(""))
 	  		    {
 	 				tLATreeSchema.setIntroAgency(this.mIntroAgency);
 	  			    if(!dealRecom()){return false;}
 	  		    }
     			 this.mLATreeSet.add(tLATreeSchema);
     		  }
     		  this.mLAAgentSet.add(tLAAgentSchema);
        	}
    	}
    	
    	return true;
    }
    
    /*
     * 仅修改推荐人
     */
    private boolean Proceedthree()
    {
    	this.mEdorNo=mPubFun1.CreateMaxNo("EdorNo", 20);
    	if(this.mIntroAgency==null||this.mIntroAgency.equals(""))
    	{
    		CError.buildErr(this, "页面三个选项中,新入司时间、新推荐人或者筹备时间至少输入一个！");
		    return false;
    	}
    	  LATreeDB tLATreeDB  = new LATreeDB();
		  String sql_tree = "select * from latree where agentcode = '"+this.mAgentCode+"'";
		  LATreeSet tLATreeSet = tLATreeDB.executeQuery(sql_tree);
		  for(int j =1;j<=tLATreeSet.size();j++)
		  {
			 LATreeSchema tLATreeSchema = new LATreeSchema();
			 tLATreeSchema=tLATreeSet.get(j);
			 LATreeBSchema tLATreeBSchema = new LATreeBSchema();
			 
			 ref.transFields(tLATreeBSchema, tLATreeSchema);
			 tLATreeBSchema.setEdorNO(this.mEdorNo);	
			 tLATreeBSchema.setRemoveType("98");
			 tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
			 tLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
			 tLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
			 tLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
			 tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
			   
			 tLATreeBSchema.setOperator(mGlobalInput.Operator);
			 tLATreeBSchema.setMakeDate(this.CurrentDate);
			 tLATreeBSchema.setMakeTime(this.CurrentTime);
			 tLATreeBSchema.setModifyDate(this.CurrentDate);
			 tLATreeBSchema.setModifyTime(this.CurrentTime);
			 
			 this.mLATreeBSet.clear();//先清除下
			 this.mLATreeBSet.add(tLATreeBSchema);
			 tLATreeSchema.setModifyDate(this.CurrentDate);
			 tLATreeSchema.setModifyTime(this.CurrentTime);
			 // 推荐关系调整
			 
			 if(this.mIntroAgency!=null&&!this.mIntroAgency.equals(""))
 		    {
				tLATreeSchema.setIntroAgency(this.mIntroAgency);
 			    if(!dealRecom()){return false;}
 		    }
			 this.mLATreeSet.add(tLATreeSchema);
    	}
    	return true;
    }
    
    /*
     *  处理推荐人
     */
    private boolean dealRecom()
    {
    	
    	// 1.先处理自己的推荐关系
    	LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();
    	String sql_recom = "select * from larecomrelation where agentcode = '"+this.mAgentCode+"' and recomflag = '1' order by recomgens";
    	System.out.println("原先推荐关系查询SQL如下:"+sql_recom);
        LARecomRelationSet tDownIntroAgentcyReComSet = new LARecomRelationSet();
        tDownIntroAgentcyReComSet=tLARecomRelationDB.executeQuery(sql_recom);
        for(int i =1;i<=tDownIntroAgentcyReComSet.size();i++)
        {
        	  LARecomRelationSchema tLARecomRelationSchema = new LARecomRelationSchema();
			  tLARecomRelationSchema =tDownIntroAgentcyReComSet.get(i).getSchema();
			  if(!backReaCom(tLARecomRelationSchema)){return false;} //备份原有推荐关系
        }
        
			  LARecomRelationSchema t_LARecomRelationSchema = new LARecomRelationSchema();
			  t_LARecomRelationSchema.setAgentCode(this.mAgentCode);
			  t_LARecomRelationSchema.setAgentGroup(this.mAgentGroup);
			  t_LARecomRelationSchema.setRecomAgentCode(this.mIntroAgency);
			  //t_LARecomRelationSchema.setAgentGroup(this.mAgentGroup);
			  String tsql = "select branchlevel from labranchgroup where agentgroup='"+this.mAgentGroup+"'";
              ExeSQL texesql = new ExeSQL();
              String tRecomLevel = texesql.getOneValue(tsql);
              t_LARecomRelationSchema.setRecomLevel(tRecomLevel);
             // 注意地方
              if(!addReaCom(t_LARecomRelationSchema,0)) {return false;}//建立推荐关系
			
         
        // 2.处理群带推荐关系
		      LARecomRelationDB tDownLARecomRelationDB = new LARecomRelationDB();
              tLARecomRelationDB.setRecomAgentCode(this.mAgentCode);//作为推荐人
              tLARecomRelationDB.setRecomFlag("1");
		   LARecomRelationSet tDownLARecomRelationSet = new LARecomRelationSet();
		   tDownLARecomRelationSet=tLARecomRelationDB.query();
		   for(int del_1 = 1;del_1<=tDownLARecomRelationSet.size();del_1++)
		   { 
			   LARecomRelationSchema del_1LARecomRelationSchema = new LARecomRelationSchema();
			   del_1LARecomRelationSchema=tDownLARecomRelationSet.get(del_1);
			   if(!delReaCom(del_1LARecomRelationSchema)){return false;}// 处理原先关系  
			   
			   del_1LARecomRelationSchema.setRecomAgentCode(this.mIntroAgency);
			   //del_1LARecomRelationSchema.setRecomGens(0);
			   // 注意地方
			   if(!addReaCom(del_1LARecomRelationSchema,1)) {return false;}//建立推荐关系
			   
		   }

		// 如果修改业务员为主管 需相应处理育成关系
		 if(this.mAgentGrade.substring(0, 1).equals("B"))
		 {
			 if(!dealRear()) {return false;}
		 }
    	return true;
    
    }
    /*
     * 得到新推荐人上级推荐关系
     */
    private boolean getIntroAgencyReCom()
    {
    	LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();
    	String sql = "select * from LARecomRelation where agentcode = '"+this.mIntroAgency+"' and recomflag = '1' ";
    	System.out.println("新推荐人推荐关系查询SQL如下:"+sql);
    	this.mIntroAgentcyReComSet=tLARecomRelationDB.executeQuery(sql);
    	 return true;
    }


    /*
     *  建立间接的推荐关系
     */
    private boolean addReaCom(LARecomRelationSchema tLARecomRelationSchema,int i)
    {
    	 String tBranchLevel = tLARecomRelationSchema.getRecomLevel().trim();
         //按条件复制推荐人的所有推荐关系
    	 LARecomRelationSet tLARecomRelationSet = new LARecomRelationSet();
    	 LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();
  
    	 tLARecomRelationDB.setAgentCode(tLARecomRelationSchema.getRecomAgentCode());
    	 tLARecomRelationDB.setRecomFlag("1");
    	 // 存储到新的集合中去
    	 tLARecomRelationSet = tLARecomRelationDB.query();
    	 tLARecomRelationSet.add(tLARecomRelationSchema);
         for (int j = 1; j <= tLARecomRelationSet.size(); j++)
         {
        	 LARecomRelationSchema tempLARecomRelationSchema = new LARecomRelationSchema();
        	 tempLARecomRelationSchema = tLARecomRelationSet.get(j);
        	 tempLARecomRelationSchema.setRecomLevel(tBranchLevel);
        	 System.out.println("代数"+tempLARecomRelationSchema.getRecomGens()+"/人员编码:"+tempLARecomRelationSchema.getAgentCode());
        	 if(i==0){
        	 tempLARecomRelationSchema.setRecomGens(tempLARecomRelationSchema.getRecomGens()+1);
        	 }
        	 if(i==1)
        	 {
        		 tempLARecomRelationSchema.setRecomGens(tempLARecomRelationSchema.getRecomGens()+1); 
        	 }
        	 

        	 tempLARecomRelationSchema.setAgentCode(tLARecomRelationSchema.getAgentCode());//放新的
        	 System.out.println("这个是什么意思："+tLARecomRelationSchema.getAgentCode());
        	 tempLARecomRelationSchema.setAgentGroup(getGroup(tLARecomRelationSchema.getAgentCode()));
        	 tempLARecomRelationSchema.setStartDate(this.CurrentDate);
        	 tempLARecomRelationSchema.setRecomFlag("1");
        	 tempLARecomRelationSchema.setRecomStartYear("1");
             tempLARecomRelationSchema.setMakeDate(this.CurrentDate);
             tempLARecomRelationSchema.setMakeTime(this.CurrentTime);
             tempLARecomRelationSchema.setModifyDate(CurrentDate);
             tempLARecomRelationSchema.setModifyTime(CurrentTime);
             tempLARecomRelationSchema.setOperator(mGlobalInput.Operator);
        	 this.mInLARecomRelationSet.add(tempLARecomRelationSchema);
         }
         return true;

    }
    /*
     *  备份LAReCom表
     *  
     */
    private boolean backReaCom(LARecomRelationSchema tLARecomRelationSchema)
    {
    	 LARecomRelationBSchema tLARecomRelationBSchema = new LARecomRelationBSchema();
		  ref.transFields(tLARecomRelationBSchema, tLARecomRelationSchema);
		  tLARecomRelationBSchema.setEdorType("98");
		  tLARecomRelationBSchema.setEdorNo(this.mEdorNo);
		  tLARecomRelationBSchema.setMakeDate2(tLARecomRelationSchema.getMakeDate());
		  tLARecomRelationBSchema.setMakeTime2(tLARecomRelationSchema.getMakeTime());
		  tLARecomRelationBSchema.setModifyDate2(tLARecomRelationSchema.getModifyDate());
		  tLARecomRelationBSchema.setModifyTime2(tLARecomRelationSchema.getModifyTime());
		  tLARecomRelationBSchema.setOperator2(tLARecomRelationSchema.getOperator());
		  tLARecomRelationBSchema.setMakeDate(this.CurrentDate);
		  tLARecomRelationBSchema.setMakeTime(this.CurrentTime);
		  tLARecomRelationBSchema.setModifyDate(this.CurrentDate);
		  tLARecomRelationBSchema.setModifyTime(this.CurrentTime);
		  tLARecomRelationBSchema.setOperator(mGlobalInput.Operator);
		  mInLARecomRelationBSet.add(tLARecomRelationBSchema);
		  mDelLARecomRelationSet.add(tLARecomRelationSchema);
		  
    	return true;
    }
    
    /*
     *  处理原先间接推荐关系
     */
    private boolean delReaCom(LARecomRelationSchema tLARecomRelationSchema)
    {
    	String tAgentCode = tLARecomRelationSchema.getAgentCode();
    	int tReComGens = tLARecomRelationSchema.getRecomGens();
    	String del_sql = "select * from LARecomRelation where agentcode ='"+tAgentCode+"' and ReComGens> "+tReComGens+" and recomflag ='1'";
    	System.out.println("原先二代推荐关系如下："+del_sql);
    	LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();
    	LARecomRelationSet tLARecomRelationSet = new LARecomRelationSet();
    	tLARecomRelationSet = tLARecomRelationDB.executeQuery(del_sql);
    	for(int i = 1;i<=tLARecomRelationSet.size();i++)
    	{
    		LARecomRelationSchema tempLARecomRelationSchema = new LARecomRelationSchema();
    		tempLARecomRelationSchema=tLARecomRelationSet.get(i); 
    		if(!backReaCom(tempLARecomRelationSchema)){return false;}
    	}
    	return true;
    }
    /*
     *  得到业务员职级 
     */
    private String getGrade(String tAgentCode)
    {
    	String tResult="";
    	String sql = "select agentgrade from latree where agentcode = '"+tAgentCode+"'";
    	ExeSQL tExeSQL = new ExeSQL();
    	tResult=tExeSQL.getOneValue(sql);
    	return tResult;
    }
    /*
     *  处理育成关系
     */
    private boolean dealRear()
    {
    	LARearRelationDB tLARearRelationDB = new LARearRelationDB();
    	LARearRelationSet tLARearRelationSet = new LARearRelationSet();
    	String sql_rear = "select * from larearrelation where agentcode = '"+this.mAgentCode+"' and rearflag = '1'";
    	tLARearRelationSet = tLARearRelationDB.executeQuery(sql_rear);
    	for(int i = 1;i<=tLARearRelationSet.size();i++)
    	{
    		LARearRelationSchema tLARearRelationSchema = new LARearRelationSchema();
    		tLARearRelationSchema= tLARearRelationSet.get(i);
    		if(!backRear(tLARearRelationSchema)){return false;}
    	}
    		//入司时能生成育成关系,推荐人和入司人员职级皆为主管职级
    		// 1.处理直接育成关系
    		String tGradePro1 = getGradeProperty2(this.mAgentCode);// 人员序列
    		String tGradePro2 =getGradeProperty2(this.mIntroAgency);//推荐人序列
    		if(tGradePro1.equals(tGradePro2))
    		{
    			 LARearRelationSchema tRearRSchema = new LARearRelationSchema();
                 tRearRSchema.setAgentCode(this.mAgentCode);
                 tRearRSchema.setAgentGroup(this.mAgentGroup);
                 tRearRSchema.setRearAgentCode(this.mIntroAgency);
                 String tAgentGroup = getGroup(this.mAgentCode);
                 String tsql = "select branchlevel from labranchgroup where agentgroup='"+tAgentGroup+"'";
                 ExeSQL texesql = new ExeSQL();
                 String tgrade = texesql.getOneValue(tsql);
                 tRearRSchema.setRearLevel(tgrade);
                 //tRearRSchema.setRearedGens(1);
                 if(!addRear(tRearRSchema,0)){return false;}
    		}
    	
    		// 2.处理群代关系
    		 String sql_rear1 = "select * from larearrelation where rearagentcode = '"+this.mAgentCode+"' and rearflag = '1' ";
    		 LARearRelationSet t_LARearRelationSet = new LARearRelationSet();
    		 t_LARearRelationSet=tLARearRelationDB.executeQuery(sql_rear1);
    		 for(int j = 1;j<=t_LARearRelationSet.size();j++)
    		 {
    			 LARearRelationSchema t_LARearRelationSchema = new LARearRelationSchema();
    			 t_LARearRelationSchema = t_LARearRelationSet.get(j);
    			 // 查询需要删除的间接育成关系
    			 String t_sql = "select * from larearrelation where agentcode = '"+t_LARearRelationSchema.getAgentCode()+"' and rearedgens>"+t_LARearRelationSchema.getRearedGens()+" and rearflag = '1' ";
    			 LARearRelationSet temp_LARearRelationSet = new LARearRelationSet();
    			 temp_LARearRelationSet = tLARearRelationDB.executeQuery(t_sql);
    			 for(int k = 1;k<=temp_LARearRelationSet.size();k++)
    			 {
    				 LARearRelationSchema temp_LARearRelationSchema = new LARearRelationSchema();
    				 temp_LARearRelationSchema = temp_LARearRelationSet.get(k);
    				 if(!backRear(temp_LARearRelationSchema)){return false;}// 备份原先间接育成关系
    			 }
    				 String t_GradePro1 = getGradeProperty2(t_LARearRelationSchema.getAgentCode());// 人员序列
    		         String t_GradePro2 =getGradeProperty2(this.mIntroAgency);//推荐人序列
    		    		if(t_GradePro1.equals(t_GradePro2))
    		    		{
    		    			 LARearRelationSchema t_RearRSchema = new LARearRelationSchema();
    		                 t_RearRSchema.setAgentCode(t_LARearRelationSchema.getAgentCode());
    		                 t_RearRSchema.setAgentGroup(getGroup(t_LARearRelationSchema.getAgentCode()));
    		                 t_RearRSchema.setRearAgentCode(this.mIntroAgency);
    		                 String tsql = "select branchlevel from labranchgroup where agentgroup='"+getGroup(t_LARearRelationSchema.getAgentCode())+"'";
    		                 ExeSQL texesql = new ExeSQL();
    		                 String tgrade = texesql.getOneValue(tsql);
    		                 t_RearRSchema.setRearLevel(tgrade);
    		               
    		                 if(!addRear(t_RearRSchema,1)){return false;}
    		    		}
    			 
    		 }
    	return true;
    }
    /*
     * 备份育成关系表
     */
    private boolean backRear(LARearRelationSchema tLARearRelationSchema)
    {
    	LARearRelationBSchema tLARearRelationBSchema = new LARearRelationBSchema();
    	ref.transFields(tLARearRelationBSchema, tLARearRelationSchema);
    	tLARearRelationBSchema.setEdorType("98");
    	tLARearRelationBSchema.setEdorNo(this.mEdorNo);
    	tLARearRelationBSchema.setMakeDate2(tLARearRelationSchema.getMakeDate());
    	tLARearRelationBSchema.setMakeTime2(tLARearRelationSchema.getMakeTime());
    	tLARearRelationBSchema.setModifyDate2(tLARearRelationSchema.getModifyDate());
    	tLARearRelationBSchema.setModifyTime2(tLARearRelationSchema.getModifyTime());
    	tLARearRelationBSchema.setOperator2(tLARearRelationSchema.getOperator());
    	tLARearRelationBSchema.setMakeDate(this.CurrentDate);
		tLARearRelationBSchema.setMakeTime(this.CurrentTime);
		tLARearRelationBSchema.setModifyDate(this.CurrentDate);
		tLARearRelationBSchema.setModifyTime(this.CurrentTime);
		tLARearRelationBSchema.setOperator(mGlobalInput.Operator);
		this.mInLARearRelationBSet.add(tLARearRelationBSchema);
		this.mDelLARearRelationSet.add(tLARearRelationSchema);
    	
    	return true;
    }
    /*
     * 得到职级序列
     */
    private String getGradeProperty2(String tAgentcode)
    {
    	String Result = "";
    	String sql = "select GradeProperty2 from laagentgrade where" +
    			" gradecode=(select agentgrade from latree where agentcode ='"+tAgentcode+"' )";
    	ExeSQL tExeSQL = new ExeSQL();
    	Result=tExeSQL.getOneValue(sql);
    	return Result;
    }
    /*
     *  得到人员所属团队
     */
    private String getGroup(String tAgentcode)
    {
    	String Result = "";
    	String sql = "select agentgroup from laagent where agentcode = '"+tAgentcode+"'";
    	ExeSQL tExeSQL = new ExeSQL();
    	Result=tExeSQL.getOneValue(sql);
    	return Result;
    }
    /*
     *  育成关系处理
     */
    private boolean addRear(LARearRelationSchema cLARearRScehma,int i) {
        //将被推荐人的所在机构全部找出
        String tBranchLevel = cLARearRScehma.getRearLevel().trim();
        //按条件复制推荐人的所有推荐关系
        LARearRelationSet tLARearRelationSet = new LARearRelationSet();
        LARearRelationDB tLARearRelationDB = new LARearRelationDB();
        tLARearRelationDB.setAgentCode(cLARearRScehma.getRearAgentCode());
        tLARearRelationDB.setRearFlag("1");
        tLARearRelationSet = tLARearRelationDB.query();
        tLARearRelationSet.add(cLARearRScehma);
        for (int j = 1; j <= tLARearRelationSet.size(); j++)
        {
            LARearRelationSchema tLARearRelationSchema =tLARearRelationSet.get(j);
            tLARearRelationSchema.setRearLevel(tBranchLevel);
            if(i==0)
            {
            tLARearRelationSchema.setRearedGens(tLARearRelationSchema.getRearedGens() +1);
            }
            if(i==1)
            {
            	tLARearRelationSchema.setRearedGens(tLARearRelationSchema.getRearedGens() +1);
            }
            tLARearRelationSchema.setAgentCode(cLARearRScehma.getAgentCode());
            tLARearRelationSchema.setAgentGroup(cLARearRScehma.getAgentGroup());
            tLARearRelationSchema.setstartDate(this.CurrentDate);
            tLARearRelationSchema.setRearFlag("1");
            tLARearRelationSchema.setRearCommFlag("0");
            tLARearRelationSchema.setRearStartYear("1");
            tLARearRelationSchema.setMakeDate(this.CurrentDate);
            tLARearRelationSchema.setMakeTime(this.CurrentTime);
            tLARearRelationSchema.setModifyDate(CurrentDate);
            tLARearRelationSchema.setModifyTime(CurrentTime);
            tLARearRelationSchema.setOperator(mGlobalInput.Operator);
            mInLARearRelationSet.add(tLARearRelationSchema);
        }
        return true;
    }
    /*
     * 获取人员Noworkflag
     */
    private String getNoworkflag(String tAgentcode)
    {
    	String Results= "";
    	String sql = "select noworkflag from laagent where agentcode = '"+tAgentcode+"'";
    	ExeSQL tExeSQL = new ExeSQL();
    	Results=tExeSQL.getOneValue(sql);
    	return Results;
    }
    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
private boolean prepareOutputData() {
  try {
    System.out.println(
        "Begin LANewToFormBL.prepareOutputData.........");
    mInputData.clear();
    map.put(this.mLAAgentSet, "UPDATE");
    map.put(this.mLAAgentBSet, "INSERT");
    map.put(this.mLATreeSet, "UPDATE");
    map.put(this.mLATreeBSet, "INSERT");
    
    
    map.put(this.mDelLARecomRelationSet, "DELETE");
    map.put(this.mInLARecomRelationBSet, "INSERT");
    map.put(this.mInLARecomRelationSet, "INSERT");
    
    
    map.put(this.mDelLARearRelationSet, "DELETE");
    map.put(this.mInLARearRelationBSet, "INSERT");
    map.put(this.mInLARearRelationSet, "INSERT");
    
    mInputData.add(map);
  }
  catch (Exception ex) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "LANewToFormBL";
    tError.functionName = "prepareOutputData";
    tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
    this.mErrors.addOneError(tError);
    return false;
  }
  return true;
}
}
