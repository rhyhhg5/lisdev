/*
 * <p>ClassName: LAGrpDimissionCancelBL </p>
 * <p>Description: LAGrpDimissionCancelBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agent;
import com.sinosoft.lis.agentbranch.ChangeAgentToBranch;
import com.sinosoft.lis.agentbranch.ChangeRearBranch;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LADimissionDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.lis.schema.LARearRelationBSchema;
import com.sinosoft.lis.schema.LARearRelationSchema;
import com.sinosoft.lis.schema.LARecomRelationSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LADimissionSet;
import com.sinosoft.lis.vschema.LARearRelationBSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.vschema.LARecomRelationBSet;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LAGrpDimissionCancelBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String mAdjustDate = "";
    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LADimissionSchema mLADimissionSchema = new LADimissionSchema();
    private LABranchGroupSet  mupLABranchGroupSet = new LABranchGroupSet();
    
    private LABranchGroupBSet  minLABranchGroupBSet= new LABranchGroupBSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAAgentBSchema mLAAgentBSchema = new LAAgentBSchema();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LATreeBSet mLATreeBSet = new LATreeBSet();
    private MMap mMap = new MMap();
    private String currentDate ="";
    private String currentTime ="";
    private String  mEdorNo="";
    private String mAgentSeries="";
    private String mAgentGroup="";
    private String mBranchManager="";
    public LAGrpDimissionCancelBL()
    {
    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";
        String tsql="select *  from  ladimission where  agentcode='1101000114' ";
        LADimissionSchema tLADimissionSchema = new LADimissionSchema();
        LADimissionSet tLADimissionSet = new LADimissionSet();
        LADimissionDB tLADimissionDB = new LADimissionDB();
        tLADimissionSet=tLADimissionDB.executeQuery(tsql);
        tLADimissionSchema=tLADimissionSet.get(1);
        tLADimissionSchema.setDepartDate("2007-01-01");
        VData tVData = new VData();
        tVData.addElement(tLADimissionSchema);
        tVData.add(tG);

        LAGrpDimissionCancelBL  tLAGrpDimissionCancelBL= new LAGrpDimissionCancelBL();
        tLAGrpDimissionCancelBL.submitData(tVData,"INSERT||MAIN");
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGrpDimissionCancelBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAGrpDimissionCancelBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAGrpDimissionCancelBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
         }

        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        currentDate = PubFun.getCurrentDate();
        currentTime = PubFun.getCurrentTime();
        Reflections tReflections = new Reflections();
        
        LADimissionDB tLADimissionDB = new LADimissionDB();
        LADimissionSchema tLADimissionSchema = null;
        LAAgentDB tLAAgentDB = new LAAgentDB();
        LAAgentSchema tLAAgentSchema = null;
        LAAgentBSchema tLAAgentBSchema = null;
        LATreeDB tLATreeDB = new LATreeDB();
        LATreeSchema tLATreeSchema = null;
        LATreeBSchema tLATreeBSchema = null;
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSchema tLABranchGroupSchema = null;
        LABranchGroupBSchema tLABranchGroupBSchema = null;
        	tLADimissionDB.setAgentCode(mLADimissionSchema.getAgentCode());
            tLADimissionDB.setDepartTimes(mLADimissionSchema.getDepartTimes());
            if (!tLADimissionDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "ALADimissionBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询代理人原离职信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLADimissionSchema=tLADimissionDB.getSchema();
            this.mMap.put(tLADimissionSchema, "DELETE");
            tLAAgentDB.setAgentCode(this.mLADimissionSchema.getAgentCode());
            if (!tLAAgentDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "ALADimissionBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询代理人信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLAAgentSchema=tLAAgentDB.getSchema();
            tLAAgentBSchema=new LAAgentBSchema();
            tReflections.transFields(tLAAgentBSchema,tLAAgentSchema);
            tLAAgentBSchema.setEdorNo(PubFun1.CreateMaxNo("EdorNo", 20));
            tLAAgentBSchema.setEdorType("33");
            this.mMap.put(tLAAgentBSchema, "INSERT");
            tLAAgentSchema.setOutWorkDate("");
            tLAAgentSchema.setOperator(this.mGlobalInput.Operator);
            tLAAgentSchema.setModifyDate(currentDate);
            tLAAgentSchema.setModifyTime(currentTime);
            if(this.mLADimissionSchema.getDepartState().equals("03")){
            	tLAAgentSchema.setAgentState("01");
            }else if(this.mLADimissionSchema.getDepartState().equals("04")){
            	tLAAgentSchema.setAgentState("02");
            }else if(this.mLADimissionSchema.getDepartState().equals("06")){
            	tLAAgentSchema.setAgentState("01");
            }else if(this.mLADimissionSchema.getDepartState().equals("07")){
            	tLAAgentSchema.setAgentState("02");
            }
        this.mMap.put(tLAAgentSchema, "UPDATE");
        if(this.mLADimissionSchema.getDepartState().equals("03")
        		||this.mLADimissionSchema.getDepartState().equals("04")){
        	return true;
        }else{
        tLATreeDB.setAgentCode(this.mLADimissionSchema.getAgentCode());
        if (!tLATreeDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "ALADimissionBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询代理人信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        tLATreeSchema=tLATreeDB.getSchema();
        tLATreeBSchema=new LATreeBSchema();
        tReflections.transFields(tLATreeBSchema,tLATreeSchema);
        tLATreeBSchema.setEdorNO(PubFun1.CreateMaxNo("EdorNo", 20));
        tLATreeBSchema.setRemoveType("33");
        tLATreeBSchema.setMakeDate2(tLATreeBSchema.getMakeDate());
        tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
        tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
        tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
        tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
        tLATreeBSchema.setMakeDate(currentDate);
        tLATreeBSchema.setMakeTime(currentTime);
        tLATreeBSchema.setModifyDate(currentDate);
        tLATreeBSchema.setModifyTime(currentTime);
        tLATreeBSchema.setOperator(this.mGlobalInput.Operator);
        this.mMap.put(tLATreeBSchema, "INSERT");
        tLATreeSchema.setOperator(this.mGlobalInput.Operator);
        tLATreeSchema.setModifyDate(currentDate);
        tLATreeSchema.setModifyTime(currentTime);
        
        if(this.mAgentSeries.equals("0")){
        	if(this.mBranchManager==null){
        		tLATreeSchema.setUpAgent("");
        	}else{
        		tLATreeSchema.setUpAgent(this.mBranchManager);
        	}
        	this.mMap.put(tLATreeSchema, "UPDATE");
        }else if(this.mAgentSeries.equals("1")){
        	this.mMap.put(tLATreeSchema, "UPDATE");
        	tLABranchGroupDB.setAgentGroup(this.mAgentGroup);
            if (!tLABranchGroupDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "ALADimissionBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询代理人信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLABranchGroupSchema=tLABranchGroupDB.getSchema();
            tLABranchGroupBSchema=new LABranchGroupBSchema();
            tReflections.transFields(tLABranchGroupBSchema,tLABranchGroupSchema);
            tLABranchGroupBSchema.setEdorNo(PubFun1.CreateMaxNo("EdorNo", 20));
            tLABranchGroupBSchema.setEdorType("33");
            tLABranchGroupBSchema.setMakeDate2(tLABranchGroupBSchema.getMakeDate());
            tLABranchGroupBSchema.setMakeTime2(tLABranchGroupBSchema.getMakeTime());
            tLABranchGroupBSchema.setModifyDate2(tLABranchGroupBSchema.getModifyDate());
            tLABranchGroupBSchema.setModifyTime2(tLABranchGroupBSchema.getModifyTime());
            tLABranchGroupBSchema.setOperator2(tLABranchGroupBSchema.getOperator());
            tLABranchGroupBSchema.setMakeDate(currentDate);
            tLABranchGroupBSchema.setMakeTime(currentTime);
            tLABranchGroupBSchema.setModifyDate(currentDate);
            tLABranchGroupBSchema.setModifyTime(currentTime);
            tLABranchGroupBSchema.setOperator(this.mGlobalInput.Operator);
            this.mMap.put(tLABranchGroupBSchema, "INSERT");
            tLABranchGroupSchema.setOperator(this.mGlobalInput.Operator);
            tLABranchGroupSchema.setModifyDate(currentDate);
            tLABranchGroupSchema.setModifyTime(currentTime);
            tLABranchGroupSchema.setBranchManager(this.mLADimissionSchema.getAgentCode());
            tLABranchGroupSchema.setBranchManagerName(this.mLADimissionSchema.getDepartRsn());
            this.mMap.put(tLABranchGroupSchema, "UPDATE");
            if(!dealupAgent()){
//            	 @@错误处理
                this.mErrors.copyAllErrors(tLADimissionDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAGrpDimissionCancelBL";
                tError.functionName = "submitData";
                tError.errorMessage = "处理团队业务员上级主管信息时失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        }
        tReturn = true;
        return tReturn;
    }

    //处理主管所在的团队 (如果是区经理,则还要处理直辖处),使团队为无主管团队
    private boolean dealupAgent()
    {
    	LATreeDB tLATreeDB=new LATreeDB();
    	String tSQL="select * from latree where agentcode in (" 
    		+"select agentcode from laagent where branchtype='2' and branchtype2='01'" 
    		+" and agentstate<'06') and agentgroup='"
    		+this.mAgentGroup+"' and branchtype='2' and branchtype2='01'";
    	LATreeSet tLATreeSet=new LATreeSet();
    	tLATreeSet=tLATreeDB.executeQuery(tSQL);
        //主管离职的处理 团队停业
        if(tLATreeSet==null || tLATreeSet.size()<=0)
        {
            return true ;
        }
        for (int i=1;i<=tLATreeSet.size();i++)
        {
            LATreeSchema tLATreeSchema= new LATreeSchema();
            tLATreeSchema=tLATreeSet.get(i);
            LATreeBSchema tLATreeBSchema= new LATreeBSchema();
            Reflections tReflections= new Reflections();
            tReflections.transFields(tLATreeBSchema,tLATreeSchema) ;
            tLATreeBSchema.setEdorNO(PubFun1.CreateMaxNo("EdorNo", 20));
            tLATreeBSchema.setRemoveType("33");
            tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
            tLATreeBSchema.setMakeDate2(tLATreeBSchema.getMakeDate());
            tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
            tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
            tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
             tLATreeBSchema.setOperator(mGlobalInput.Operator);
            tLATreeBSchema.setMakeDate(currentDate);
            tLATreeBSchema.setMakeTime(currentTime);
            tLATreeBSchema.setModifyDate(currentDate);
            tLATreeBSchema.setModifyTime(currentTime);
            mLATreeBSet.add(tLATreeBSchema);
            tLATreeSchema.setOperator(this.mGlobalInput.Operator);
            tLATreeSchema.setUpAgent(this.mLADimissionSchema.getAgentCode());
            tLATreeSchema.setModifyDate(currentDate);
            tLATreeSchema.setModifyTime(currentTime);
            mLATreeSet.add(tLATreeSchema);
        }
        this.mMap.put(mLATreeBSet, "INSERT");
        this.mMap.put(mLATreeSet, "UPDATE");
        return true ;
    }
    //处理主管所在的团队 (如果是区经理,则还要处理直辖处),使团队停业
    private boolean endBranch()
    {
        //主管离职的处理  团队停业
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setBranchManager(this.mLADimissionSchema.getAgentCode());
        System.out.println(this.mLADimissionSchema.getAgentCode());

        tLABranchGroupSet = tLABranchGroupDB.query();
        for (int i = 1; i <= tLABranchGroupSet.size(); i++) {
            LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
            LABranchGroupBSchema tLABranchGroupBSchema = new LABranchGroupBSchema();
            tLABranchGroupSchema = tLABranchGroupSet.get(i);
            //备份labranchgroupb表
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLABranchGroupBSchema, tLABranchGroupSchema);
            System.out.println(tLABranchGroupBSchema.getAgentGroup() + "|||||2");
            tLABranchGroupBSchema.setEdorType("07"); //修改操作
            tLABranchGroupBSchema.setEdorNo(mEdorNo);
            tLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.getMakeDate());
            tLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.getMakeTime());
            tLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.
                                                 getModifyDate());
            tLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.
                                                 getModifyTime());
            tLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.getOperator());
            tLABranchGroupBSchema.setMakeDate(currentDate);
            tLABranchGroupBSchema.setMakeTime(currentTime);
            tLABranchGroupBSchema.setModifyDate(currentDate);
            tLABranchGroupBSchema.setModifyTime(currentTime);
            tLABranchGroupBSchema.setOperator(this.mGlobalInput.Operator);
            minLABranchGroupBSet.add(tLABranchGroupBSchema);
            //修改labranchgroup 中的主管为空
            tLABranchGroupSchema.setEndDate(mLADimissionSchema.getDepartDate());
            tLABranchGroupSchema.setEndFlag("Y");
            tLABranchGroupSchema.setModifyDate(currentDate);
            tLABranchGroupSchema.setModifyTime(currentTime);
            tLABranchGroupSchema.setOperator(mGlobalInput.Operator);
            mupLABranchGroupSet.add(tLABranchGroupSchema);
        }
        return true ;
    }
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLADimissionSchema.setSchema((LADimissionSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LADimissionSchema",
                                                  0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start LAGrpDimissionCancelBLQuery Submit...");
        LADimissionDB tLADimissionDB = new LADimissionDB();
        tLADimissionDB.setSchema(this.mLADimissionSchema);
        System.out.println("End LAGrpDimissionCancelBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLADimissionDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLADimissionDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAGrpDimissionCancelBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            mInputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGrpDimissionCancelBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    /**
     * 校验函数
     * 对传入的数据进行信息校验
     * @return boolean
     */
    private boolean checkData()
    {
    	if(this.mLADimissionSchema.getAgentCode()==null
    			||this.mLADimissionSchema.getAgentCode().equals("")){
            CError tError = new CError();
            tError.moduleName = "LAGrpDimissionCancelBL";
            tError.functionName = "checkData()";
            tError.errorMessage = "操作错误，没有得到离职回退的业务员编码！";
            this.mErrors.addOneError(tError);
            return false;
    	}else if(this.mLADimissionSchema.getDepartState()==null
    			||this.mLADimissionSchema.getDepartState().equals("")){
            CError tError = new CError();
            tError.moduleName = "LAGrpDimissionCancelBL";
            tError.functionName = "checkData()";
            tError.errorMessage = "操作错误，没有得到离职回退的业务员离职状态！";
            this.mErrors.addOneError(tError);
            return false;
    	}
    	ExeSQL tExeSQL=new ExeSQL();
    	StringBuffer tSB=new StringBuffer();
    	SSRS tSSRS=new SSRS();
    	tSB.append("select a.endflag,a.branchmanager,b.agentseries,b.agentgroup from labranchgroup a,latree b where ")
    		.append("a.agentgroup=b.agentgroup and b.agentcode='")
    		.append(this.mLADimissionSchema.getAgentCode())
    		.append("'");
    	tSSRS=tExeSQL.execSQL(tSB.toString());
    	if(tSSRS.MaxRow<1){
    		CError tError = new CError();
            tError.moduleName = "LAGrpDimissionCancelBL";
            tError.functionName = "checkData()";
            tError.errorMessage = "操作错误，获取业务员职级和团队信息失败！";
            this.mErrors.addOneError(tError);
            return false;
    	}else if(tSSRS.GetText(1,1)!=null&&tSSRS.GetText(1,1).equals("Y")){
    		CError tError = new CError();
            tError.moduleName = "LAGrpDimissionCancelBL";
            tError.functionName = "checkData()";
            tError.errorMessage = "操作错误，业务员离职前所在团队已停业，回退失败！";
            this.mErrors.addOneError(tError);
            return false;
    	}else if(tSSRS.GetText(1,3).equals("1")
    			&&tSSRS.GetText(1,2)!=null
    			&&!tSSRS.GetText(1,2).equals("")
    			&&!tSSRS.GetText(1,2).equals(this.mLADimissionSchema.getAgentCode())){
    		System.out.println("..........................here"+tSSRS.GetText(1,1));
    		CError tError = new CError();
            tError.moduleName = "LAGrpDimissionCancelBL";
            tError.functionName = "checkData()";
            tError.errorMessage = "操作错误，业务员为主管，但离职前所在团队已有主管，回退失败！";
            this.mErrors.addOneError(tError);
            return false;
    	}
    	this.mBranchManager=tSSRS.GetText(1,2);
    	this.mAgentSeries=tSSRS.GetText(1,3);
    	this.mAgentGroup=tSSRS.GetText(1,4);
        return true;
    }
    //错误处理
    private void buildError(String szFunc, String szErrMsg) {
            CError cError = new CError();

            cError.moduleName = "AdjustAgentBL";
            cError.functionName = szFunc;
            cError.errorMessage = szErrMsg;
            this.mErrors.addOneError(cError);
    }
}
