/*
 * <p>ClassName: LAassessmainBL </p>
 * <p>Description: LAassessmainBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 
 * @CreateDate：2017-04-01
 */
package com.sinosoft.lis.agent;

import java.math.BigInteger;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

public class  UpdateLaagentBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    String currentDate = PubFun.getCurrentDate();
    String currentTime = PubFun.getCurrentTime();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAAgentSet tLAAgentSet=new LAAgentSet();
    private MMap mMap = new MMap();
    public UpdateLaagentBL()
    {}
    public static void main(String[] args)
    {
        VData tVData = new VData();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        LAComSchema tLAComSchema = new LAComSchema();
        tLAComSchema.setAgentCom("18001100001001");
        tLAComSchema.setAreaType("A");
        tLAComSchema.setChannelType("D");
        tLAComSchema.setOperator("001");
        tVData.add(tLAComSchema);
        tVData.add(tG);
        LACombankcodeBL tALAComBL = new  LACombankcodeBL();
        tALAComBL.submitData(tVData, "UPDATE||MAIN");
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if(!checkData())
        {
        	 // @@错误处理
            CError tError = new CError();
           tError.moduleName = "UpdateLaagentBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败UpdateLaagentBL-->checkData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UpdateLaagentBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败UpdateLaagentBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
        }
        else
        {
            System.out.println("Start UpdateLaagentBL Submit...");
        	PubSubmit tPubSubmit = new PubSubmit();            
            //如果有需要处理的错误，则返回
            if (!tPubSubmit.submitData(mInputData,mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "UpdateLaagentBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }
  /**
   * 进行提交前的数据校验
   */
    private boolean checkData()
    {
    	return true;
    }
    
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        
        System.out.println("我是苹果");
            if (this.mOperate.equals("UPDATE"))
            {
            	System.out.println("修改时从前台获取的managecom为："+this.mLAAgentSchema.getManageCom());
                System.out.println(mLAAgentSchema.getAgentCode());
                System.out.println("apple:"+ this.mLAAgentSchema.getAgentState());
                System.out.println("cindy:"+ this.mLAAgentSchema.getName());
                System.out.println("狗狗:"+ this.mLAAgentSchema.getOutWorkDate());
                String tSQL="";
                if(this.mLAAgentSchema.getOutWorkDate()==null||this.mLAAgentSchema.getOutWorkDate().equals("") ){
                	tSQL= "update laagent set agentstate='"+this.mLAAgentSchema.getAgentState()+"',outworkdate="+this.mLAAgentSchema.getOutWorkDate() +",modifydate=current date ,modifytime=current time where "
                    + " agentcode = '" +this.mLAAgentSchema.getAgentCode() +
                    "' and ManageCom='" + this.mLAAgentSchema.getManageCom()+"'";
                }else{
                	 tSQL = "update laagent set agentstate='"+this.mLAAgentSchema.getAgentState()+"',outworkdate='"+this.mLAAgentSchema.getOutWorkDate() +"',modifydate=current date ,modifytime=current time where "
                    + " agentcode = '" +this.mLAAgentSchema.getAgentCode() +
                    "' and ManageCom='" + this.mLAAgentSchema.getManageCom()+"'";
                }
               System.out.println("最后更新lacom表中state记录的SQL: " + tSQL);
               ExeSQL tExeSQL = new ExeSQL();
               System.out.println("1111111111111111111");
                boolean tValue = tExeSQL.execUpdateSQL(tSQL);
       if (tExeSQL.mErrors.needDealError()) {
         // @@错误处理
         this.mErrors.copyAllErrors(tExeSQL.mErrors);
         CError tError = new CError();
         tError.moduleName = "UpdateLaagentBL";
         tError.functionName = "returnState";
         tError.errorMessage = "执行SQL语句,更新laagent表记录失败!";
         this.mErrors.addOneError(tError);
         System.out.println("执行SQL语句,更新laagent表表中state记录失败!");
         return false;
          }
       }
        return true;
    }
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                                    getObjectByObjectName("LAAgentSchema", 0));
        System.out.println("wwwwwwww:"+this.mLAAgentSchema.getSchema().getAgentCode());
        System.out.println("RRRRR:"+this.mLAAgentSchema.getSchema().getAgentState());
        System.out.println("QQQQQQ:"+this.mLAAgentSchema.getSchema().getManageCom());
        System.out.println("TTTTTT:"+this.mLAAgentSchema.getSchema().getName());
        System.out.println("熊猫:"+this.mLAAgentSchema.getSchema().getOutWorkDate());
        return true;
    }



    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
           this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UpdateLaagentBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    public VData getResult()
    {
        return this.mResult;
    }
}
