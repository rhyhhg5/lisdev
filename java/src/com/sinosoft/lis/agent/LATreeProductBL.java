package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAgentGradeDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentGradeSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public class LATreeProductBL implements LATreeInterface
{
    MMap map = new MMap();
    String mOperate = new String();
    String mIsManager = new String();
    GlobalInput mGlobalInput = new GlobalInput();
    LATreeSchema mLATreeSchema = new LATreeSchema();
    LATreeBSchema mLATreeBSchema = new LATreeBSchema();
    LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    String mAgentCode = new String();
    String mBranchCode = new String();
    String mAgentGroup = new String();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String originalDate = "";
    private String originalTime = "";
    String mEdorNo = new String();
    LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();

    public void setOperate(String cOperate)
    {
        mOperate = cOperate;
    }

    public void setIsManager(String cIsManager)
    {
        mIsManager = cIsManager;
    }

    public void setGlobalInput(GlobalInput cGlobalInput)
    {
        mGlobalInput.setSchema(cGlobalInput);
    }

    public void setLATreeSchema(LATreeSchema cLATreeSchema)
    {
        mLATreeSchema.setSchema(cLATreeSchema);
    }

    public LATreeSchema getLATreeSchema()
    {
        return mLATreeSchema;
    }

    public void setLAAgentSchema(LAAgentSchema cLAAgentSchema)
    {
        mLAAgentSchema.setSchema(cLAAgentSchema);
    }

    public void setAgentCode(String cAgentCode)
    {
        mAgentCode = cAgentCode;
    }

    public MMap getResult()
    {
        return map;
    }

    public void setEdorNo(String cEdorNo)
    {
        mEdorNo = cEdorNo;
    }

    public LATreeProductBL()
    {
    }

    public static void main(String[] args)
    {
        LATreeProductBL latreeproductbl = new LATreeProductBL();
    }

    public boolean dealdata()
    {
        System.out.println("....Enter LATreeProductBL.DealData ......");
        if (this.mOperate.indexOf("UPDATE") != -1)
        {
            mAgentCode = mLAAgentSchema.getAgentCode();
            LATreeDB tLATreeDB = new LATreeDB();
            tLATreeDB.setAgentCode(mAgentCode);
            if (!tLATreeDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAAgentBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询原行政信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //备份行政信
            Reflections tReflections = new Reflections();
            tReflections.transFields(this.mLATreeBSchema, tLATreeDB.getSchema());
            this.mLATreeBSchema.setAstartDate(tLATreeDB.getAstartDate());
            this.mLATreeBSchema.setEdorNO(mEdorNo);
            this.mLATreeBSchema.setRemoveType("05");
            this.mLATreeBSchema.setMakeDate2(this.mLATreeBSchema.getMakeDate());
            this.mLATreeBSchema.setMakeTime2(this.mLATreeBSchema.getMakeTime());
            this.mLATreeBSchema.setModifyDate2(this.mLATreeBSchema.
                                               getModifyDate());
            this.mLATreeBSchema.setModifyTime2(this.mLATreeBSchema.
                                               getModifyTime());
            this.mLATreeBSchema.setOperator2(this.mLATreeBSchema.getOperator());
            this.mLATreeBSchema.setMakeDate(currentDate);
            this.mLATreeBSchema.setMakeTime(currentTime);
            this.mLATreeBSchema.setModifyDate(currentDate);
            this.mLATreeBSchema.setModifyTime(currentTime);
            this.mLATreeBSchema.setOperator(this.mGlobalInput.Operator);
            map.put(mLATreeBSchema, "INSERT");
            //xjh Modify 2005/04/01
            //修改时备份的原因是在人员是主管并且发生机构主管变动的情况才备份
            //所以这里要添加限制条件
            if (this.mIsManager.equals("true"))
            {
                LATreeSet tLATreeSet = new LATreeSet();
                LATreeDB tLATreeDB1 = new LATreeDB();
                tLATreeDB1.setUpAgent(mAgentCode);
                tLATreeSet = tLATreeDB1.query();
                for (int i = 1; i <= tLATreeSet.size(); i++)
                {
                    tLATreeSet.get(i).setUpAgent("");
                    tLATreeSet.get(i).setMakeDate(currentDate);
                    tLATreeSet.get(i).setMakeTime(currentTime);
                    tLATreeSet.get(i).setOperator(mGlobalInput.Operator);
                }
                map.put(tLATreeSet, "UPDATE");
                LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
                LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                tLABranchGroupDB.setBranchManager(mAgentCode);
                tLABranchGroupSet = tLABranchGroupDB.query();
                for (int i = 1; i <= tLABranchGroupSet.size(); i++)
                {
                    tLABranchGroupSet.get(i).setBranchManager("");
                    tLABranchGroupSet.get(i).setBranchManagerName("");
                    tLABranchGroupSet.get(i).setMakeDate(currentDate);
                    tLABranchGroupSet.get(i).setMakeTime(currentTime);
                    tLABranchGroupSet.get(i).setOperator(mGlobalInput.Operator);
                }
                map.put(tLABranchGroupSet, "UPDATE");
            }
            //没有备份机构信息
            originalDate = tLATreeDB.getMakeDate();
            originalTime = tLATreeDB.getMakeTime();
        }
        if (this.mOperate.equals("INSERT||MAIN") ||
            this.mOperate.equals("UPDATE||MAIN") ||
            this.mOperate.equals("UPDATE||ALL"))
        {
            System.out.println(
                    "....Enter LATreeProductBL.DealData ++++++......");
            System.out.println("^^^^^" + this.mLATreeSchema.getAgentGrade());
            this.mLATreeSchema.setAgentCode(mAgentCode);
            this.mLATreeSchema.setAgentGroup(mLAAgentSchema.getAgentGroup());
            this.mLATreeSchema.setBranchCode(mLAAgentSchema.getBranchCode());
            //upagent
            String tUpAgent = getUpAgent(mLAAgentSchema.getAgentGroup().trim());
            System.out.println(tUpAgent);
            if (tUpAgent == null)
            {
                return false;
            }
            else if (!tUpAgent.equals(""))
            {
                this.mLATreeSchema.setUpAgent(tUpAgent.trim());
            }
            //agentgrade agentkind
            System.out.println("^^^^^********4*****");
            if (this.mLATreeSchema.getAgentGrade() != null &&
                !this.mLATreeSchema.getAgentGrade().equals(""))
            {
                //AgentSeries
                String tAgentSeries = getAgentSeries(mLATreeSchema.
                        getAgentGrade().
                        trim());
                if (tAgentSeries == null)
                {
                    return false;
                }
                this.mLATreeSchema.setAgentSeries(tAgentSeries.trim());
            }
            System.out.println("^^^^^*************");
            this.mLATreeSchema.setAssessType("0"); //正常
            this.mLATreeSchema.setState("0");
            if (this.mLATreeSchema.getIntroAgency() != null &&
                !this.mLATreeSchema.getIntroAgency().equals(""))
            {
                this.mLATreeSchema.setIntroBreakFlag("0");
                this.mLATreeSchema.setIntroCommStart(this.mLAAgentSchema.
                        getEmployDate());
            }
            //设置内外勤标志
            if (!setInsideFlag())
            {
                return false;
            }
            this.mLATreeSchema.setStartDate(mLAAgentSchema.getEmployDate());
            this.mLATreeSchema.setAstartDate(mLAAgentSchema.getEmployDate());
            if (this.mOperate.indexOf("UPDATE") != -1)
            {
                this.mLATreeSchema.setMakeDate(originalDate);
                this.mLATreeSchema.setMakeTime(originalTime);
            }
            else
            {
                this.mLATreeSchema.setMakeDate(currentDate);
                this.mLATreeSchema.setMakeTime(currentTime);
            }
            this.mLATreeSchema.setModifyDate(currentDate);
            this.mLATreeSchema.setModifyTime(currentTime);
            this.mLATreeSchema.setOperator(mGlobalInput.Operator);
            System.out.println("^^^^^****$$$$$$$$$$$*********");
            //设置销售机构管理人员信息
            if (this.mIsManager.equals("true"))
            {
                System.out.println("111111111111++++++11111111111111");
                if (!setGroupManager(this.mLATreeSchema.getAgentGroup(),
                                     mAgentCode,
                                     this.mLAAgentSchema.getName()))
                {
                    return false;
                }
                //if (this.mOperate.equals("UPDATE||ALL"))
                if (!setDirInst(this.mLATreeSchema.getAgentGroup(),
                                mAgentCode))
                {
                    return false;
                }

            }
            //设置同业衔接属性
            LAAgentGradeDB tLAAgentGradeDB=new LAAgentGradeDB();
            tLAAgentGradeDB.setGradeCode(mLATreeSchema.getAgentGrade());
             if (!tLAAgentGradeDB.getInfo())
             {
                 this.mErrors.copyAllErrors(tLAAgentGradeDB.mErrors);
                 CError tError = new CError();
                 tError.moduleName = "ALAAgentBL";
                 tError.functionName = "dealData";
                 tError.errorMessage = "未查询到职级"+mLATreeSchema.getAgentGrade()+"的描述信息!";
                 this.mErrors.addOneError(tError);
                 return false;

             }
            if (tLAAgentGradeDB.getGradeProperty4() != null &&
                tLAAgentGradeDB.getGradeProperty4().equals("1"))
            {
                this.mLATreeSchema.setisConnMan("0");
            }
            else
            {
                this.mLATreeSchema.setisConnMan("1");
                if (this.mLATreeSchema.getAgentLine().trim().equals("A"))
                {
                    this.mLATreeSchema.setInitGrade(this.mLATreeSchema.
                            getAgentGrade());
                }
                else
                {
                    this.mLATreeSchema.setInitGrade(this.mLATreeSchema.
                            getAgentKind());
                }
            }
            System.out.println("^^^^^*************%@#^&*(^&*(^*(");
            //放入MAP中
            if (this.mOperate.equals("INSERT||MAIN"))
            {
                map.put(this.mLATreeSchema, "INSERT");
            }
            else if (this.mOperate.indexOf("UPDATE") != -1)
            {
                map.put(this.mLATreeSchema, "UPDATE");
            }
            //setupagent
        }
        return true;
    }

    /**
     * setInsideFlag
     *
     * @return boolean
     */
    private boolean setInsideFlag()
    {
        String tAgentGrade = this.mLATreeSchema.getAgentGrade();
        if (tAgentGrade == null || tAgentGrade.equals(""))
        {
            String tAgentKind = this.mLATreeSchema.getAgentKind();
            if (tAgentKind == null || tAgentKind.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "LAAgentProductBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "查询不到此代理人的级别信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            else
            {
                LAAgentGradeDB tLAAgentGradeDB = new LAAgentGradeDB();
                tLAAgentGradeDB.setGradeCode(tAgentKind.trim());
                if (!tLAAgentGradeDB.getInfo())
                {
                    CError tError = new CError();
                    tError.moduleName = "LAAgentProductBL";
                    tError.functionName = "dealdata";
                    tError.errorMessage = "查询不到" + tAgentKind + "的级别信息!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                LAAgentGradeSchema tLAAgentGradeSchema = new LAAgentGradeSchema();
                tLAAgentGradeSchema = tLAAgentGradeDB.getSchema();
                mLATreeSchema.setInsideFlag(tLAAgentGradeSchema.
                                            getGradeProperty7());
            }
        }
        else
        {
            LAAgentGradeDB tLAAgentGradeDB = new LAAgentGradeDB();
            tLAAgentGradeDB.setGradeCode(tAgentGrade.trim());
            if (!tLAAgentGradeDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "LAAgentProductBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "查询不到" + tAgentGrade + "的级别信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LAAgentGradeSchema tLAAgentGradeSchema = new LAAgentGradeSchema();
            tLAAgentGradeSchema = tLAAgentGradeDB.getSchema();
            mLATreeSchema.setInsideFlag(tLAAgentGradeSchema.
                                        getGradeProperty7());
        }
        return true;
    }

    /**
     * setGroupManager
     *
     * @param cAgentGroup String
     * @param cAgentCode String
     * @param cName String
     * @return boolean
     */
    private boolean setGroupManager(String cAgentGroup, String cAgentCode,
                                    String cName)
    {
        //设置机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(cAgentGroup);
        if (!tLABranchGroupDB.getInfo())
        {
            return false;
        }
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = tLABranchGroupDB.getSchema();
        //设置机构属性
        tLABranchGroupSchema.setBranchManager(cAgentCode);
        tLABranchGroupSchema.setBranchManagerName(cName);
        tLABranchGroupSchema.setModifyDate(currentDate);
        tLABranchGroupSchema.setModifyTime(currentTime);
        tLABranchGroupSchema.setOperator(this.mGlobalInput.Operator);
        System.out.println(tLABranchGroupSchema.getAgentGroup());
        //放入MAP
        map.put(tLABranchGroupSchema, "UPDATE");
        //查找下级直辖机构
        LABranchGroupDB upLABranchGroupDB = new LABranchGroupDB();
        upLABranchGroupDB.setUpBranch(cAgentGroup);
        upLABranchGroupDB.setUpBranchAttr("1");
        //没有找到下级直辖机构，返回
        if (!upLABranchGroupDB.getInfo())
        {
            return true;
        }
        LABranchGroupSchema upLABranchGroupSchema = new LABranchGroupSchema();
        upLABranchGroupSchema = upLABranchGroupDB.getSchema();
        //设置下级机构属性
        return setGroupManager(upLABranchGroupSchema.getAgentGroup().trim(),
                               cAgentCode, cName);
    }

    /**
     * 获得上级代理人编码
     * @param agentGroup String
     * @return String
     */
    private String getUpAgent(String agentGroup)
    {
        //当一个团队主管降级到自己下属机构(降一级)时他的上级主管为空
        //新机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(agentGroup);
        if (!tLABranchGroupDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LATreeProductBL";
            tError.functionName = "getUpAgent";
            tError.errorMessage = "查询不到" + agentGroup + "的机构信息!";
            this.mErrors.addOneError(tError);
            return null;
        }
        //新机构ＩＤ　
        String tSQL0 =
                "select branchlevelid from labranchlevel where branchtype = '"
                + mLAAgentSchema.getBranchType().trim() + "' and branchtype2 = '"
                + mLAAgentSchema.getBranchType2().trim() +
                "' and branchlevelcode = '" +
                tLABranchGroupDB.getBranchLevel().trim() + "'";
        ExeSQL tExeSQL0 = new ExeSQL();
        String tCurLevel = tExeSQL0.getOneValue(tSQL0);
        if (tCurLevel == null || tCurLevel.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "LATreeProductBL";
            tError.functionName = "getUpAgent";
            tError.errorMessage = "查询LABranchLevel纪录出错！";
            this.mErrors.addOneError(tError);
            return null;
        }
        int iLevel = Integer.parseInt(tCurLevel.trim());
        System.out.println(iLevel);
        //旧机构信息
        String oldManager;
        String oldAgentGroup;
        int iOldLevel;
        if (!mOperate.equals("INSERT||MAIN"))
        {
            LABranchGroupDB tLABranchGroupDB1 = new LABranchGroupDB();
            LAAgentSchema tLAAgentSchema = new LAAgentSchema();
            tLAAgentSchema = getAgentBasicInfo(mAgentCode);
            if (tLAAgentSchema == null)
            {
                return null;
            }
            tLABranchGroupDB1.setAgentGroup(tLAAgentSchema.getAgentGroup().trim());
            if (!tLABranchGroupDB1.getInfo())
            {
                this.mErrors.copyAllErrors(tLABranchGroupDB1.mErrors);
                CError tError = new CError();
                tError.moduleName = "LATreeProductBL";
                tError.functionName = "getUpAgent";
                tError.errorMessage = "查询不到" + agentGroup + "的机构信息!";
                this.mErrors.addOneError(tError);
                return null;
            }
            //旧机构ＩＤ　
            String tSQL2 =
                    "select branchlevelid from labranchlevel where branchtype = '"
                    + mLAAgentSchema.getBranchType().trim() +
                    "' and branchtype2 = '"
                    + mLAAgentSchema.getBranchType2().trim() +
                    "' and branchlevelcode = '" +
                    tLABranchGroupDB.getBranchLevel().trim() + "'";
            ExeSQL tExeSQL2 = new ExeSQL();
            String tOldLevel = tExeSQL2.getOneValue(tSQL2);
            if (tOldLevel == null || tOldLevel.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "LATreeProductBL";
                tError.functionName = "getUpAgent";
                tError.errorMessage = "查询LABranchLevel纪录出错！";
                this.mErrors.addOneError(tError);
                return null;
            }
            iOldLevel = Integer.parseInt(tOldLevel.trim());
            System.out.println(iOldLevel);
            oldAgentGroup = tLABranchGroupDB1.getAgentGroup().trim();
            oldManager = tLABranchGroupDB.getBranchManager();
            if (oldManager == null)
            {
                oldManager = "";
            }
            else
            {
                oldManager = oldManager.trim();
            }
        }
        else
        {
            oldManager = "";
            oldAgentGroup = "";
            iOldLevel = 0;
        }
        System.out.println("A");
        if (mIsManager.equals("true"))
        {
            //主管降一级
            System.out.println("B");
            if (iLevel == iOldLevel - 1)
            {
                //当前上级机构
                String newUpBranch = tLABranchGroupDB.getUpBranch();
                if (newUpBranch == null || newUpBranch.equals(""))
                {
                    System.out.println("No UpBranch");
                }
                else
                {
                    if (newUpBranch.trim().equals(oldAgentGroup) &&
                        oldManager.equals(mAgentCode))
                    {
                        return "";
                    }
                }
            }
            System.out.println("C");
            String tSQL1 =
                    "select max(branchlevelid) from labranchlevel where branchtype = '"
                    + mLAAgentSchema.getBranchType().trim() +
                    "' and branchtype2 = '"
                    + mLAAgentSchema.getBranchType2().trim() + "'";
            ExeSQL tExeSQL1 = new ExeSQL();
            String tMaxLevel = tExeSQL1.getOneValue(tSQL1);
            if (tMaxLevel == null || tMaxLevel.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "LATreeProductBL";
                tError.functionName = "getUpAgent";
                tError.errorMessage = "查询LABranchLevel纪录出错！";
                this.mErrors.addOneError(tError);
                return null;
            }
            int iMax = Integer.parseInt(tMaxLevel.trim());
            System.out.println(iMax);
            if (iMax == iLevel)
            {
                return "";
            }
            else
            {
                System.out.println("D");
                String upbranch = tLABranchGroupDB.getUpBranch();
                tLABranchGroupDB = new LABranchGroupDB();
                tLABranchGroupDB.setAgentGroup(upbranch);
                if (!tLABranchGroupDB.getInfo())
                {
                    this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LATreeProductBL";
                    tError.functionName = "getUpAgent";
                    tError.errorMessage = "查询不到" + upbranch + "的机构信息!";
                    this.mErrors.addOneError(tError);
                    return null;
                }
                return tLABranchGroupDB.getBranchManager() == null ? "" :
                        tLABranchGroupDB.getBranchManager();
            }
        }
        else
        {
            //最低级主管降为普通业务员
            System.out.println("E");
            if ((iLevel == iOldLevel - 1) && agentGroup.equals(oldAgentGroup) &&
                oldManager.equals(mAgentCode))
            {
                return "";
            }
            else
            {
                return tLABranchGroupDB.getBranchManager() == null ? "" :
                        tLABranchGroupDB.getBranchManager();
            }
        }
    }

    /**
     * 根据代理人职级确定代理人系列
     * @param cAgentGrade String
     * @return String
     */
    private String getAgentSeries(String cAgentGrade)
    {
        String tAgentSeries = "";
//        String tSQL =
//                "select code2 from ldcodeRela where relaType = 'gradeserieslevel' "
//                + "and code1 = '" + cAgentGrade + "'";
//        ExeSQL tExeSQL = new ExeSQL();
//        tAgentSeries = tExeSQL.getOneValue(tSQL);
//        if (tExeSQL.mErrors.needDealError())
//        {
//            // @@错误处理
//            this.mErrors.copyAllErrors(tExeSQL.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "ALAgentBL";
//            tError.functionName = "dealData";
//            tError.errorMessage = "执行SQL语句：从表中取值失败!";
//            this.mErrors.addOneError(tError);
//            return null;
//        }
        tAgentSeries = AgentPubFun.getAgentSeries(cAgentGrade);
        if (tAgentSeries == null)
        {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "dealData";
            tError.errorMessage = "执行SQL语句：从表中取值失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tAgentSeries;
    }

    /**
     * 递归设置当前机构的所有下属机构的管理人员的上级代理人，
     * 包括当前机构的直辖机构的下属机构，直到当前机构的直辖组
     *
     * @param tAgentGroup String
     * @param tManager String
     * @return boolean
     */
    private boolean setDirInst(String tAgentGroup, String tManager)
    {
        String strSQL = "select * from labranchgroup where UpBranch='" +
                        tAgentGroup +
                        "' and (endflag is null or endflag<>'Y') ";
        //mAdjustDate=this.mLABranchGroupSchema .getModifyDate();
        System.out.println("------strSQL---" + strSQL);
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(strSQL);
        if (tLABranchGroupDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALABranchManagerBL";
            tError.functionName = "setDirInst";
            tError.errorMessage = "查询当前机构" + tAgentGroup + "的下级机构时出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("--tLABranchGroupSet.size()---" +
                           tLABranchGroupSet.size());
        /**如果该机构有下级机构，则
         * 逐机构修改机构主管的行政信息并修改机构主管的上级代理人信息
         * 如果没有，那么该机构一定是个组机构，则需要修改该组机构所有成员的
         * 上级代理人
         */
        if (tLABranchGroupSet.size() > 0)
        {
            //查询该机构的下级非直辖机构
            strSQL = "select * from labranchgroup where UpBranch='" +
                     tAgentGroup +
                     "' and upbranchattr='0' and (endflag is null or endflag<>'Y' ) ";
            System.out.println("*******strSQL----" + strSQL);
            tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupSet = new LABranchGroupSet();
            tLABranchGroupSet = tLABranchGroupDB.executeQuery(strSQL);
            if (tLABranchGroupDB.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALABranchManagerBL";
                tError.functionName = "setDirInst";
                tError.errorMessage = "查询当前机构" + tAgentGroup + "的下级机构时出错!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("-----tLABranchGroupSet.size()---" +
                               tLABranchGroupSet.size());
            for (int i = 1; i <= tLABranchGroupSet.size(); i++)
            {
                LABranchGroupSchema tLABranchGroupSchema = tLABranchGroupSet.
                        get(i);
                LATreeSchema tLATreeSchema = new LATreeSchema();
                LATreeBSchema tLATreeBSchema = new LATreeBSchema();
                LAAgentSchema ttLAAgentSchema = new LAAgentSchema();
                String currManager = tLABranchGroupSchema.getBranchManager();
                System.out.println("--tManager--" + tManager +
                                   "----currManager---" + currManager);
                //备份行政信息
                if ((currManager == null) || currManager.trim().equals(""))
                {
                    continue;
                }
                tLATreeSchema = getAgentTreeInfo(currManager);
                ttLAAgentSchema = getAgentBasicInfo(currManager);
                if (ttLAAgentSchema.getAgentState().compareTo("02") > 0)
                {
                    continue;
                }
                if ((tLATreeSchema.getUpAgent() == null) ||
                    tLATreeSchema.getUpAgent().equals("") ||
                    tLATreeSchema.getUpAgent().equals(tManager))
                {
                    continue;
                }
                tLATreeBSchema = new LATreeBSchema();
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLATreeBSchema, tLATreeSchema);
                tLATreeBSchema.setAstartDate(tLATreeSchema.getAstartDate());
                tLATreeBSchema.setEdorNO(mEdorNo);
                tLATreeBSchema.setRemoveType("07"); //主管任命
                tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
                tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
                tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
                tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
                tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
                tLATreeBSchema.setMakeDate(currentDate);
                tLATreeBSchema.setMakeTime(currentTime);
                tLATreeBSchema.setModifyDate(currentDate);
                tLATreeBSchema.setModifyTime(currentTime);
                tLATreeBSchema.setOperator(this.mGlobalInput.Operator);
                map.put(tLATreeBSchema, "INSERT");
                //修改行政信息
                tLATreeSchema.setAstartDate(currentDate);
                System.out.println(tLATreeSchema.getAgentCode() + "" +
                                   tLATreeSchema.getAstartDate());
                tLATreeSchema.setUpAgent(tManager);
                tLATreeSchema.setModifyDate(currentDate);
                tLATreeSchema.setModifyTime(currentTime);
                tLATreeSchema.setOperator(this.mGlobalInput.Operator);
                map.put(tLATreeSchema, "UPDATE");
            }
            //循环的到各级直辖机构
            if (!getDirectDownBranch(tAgentGroup))
            {
                return false;
            }
            if (mLABranchGroupSet.size() > 0)
            {
                strSQL =
                        "select * from latree where AgentCode in (Select AgentCode From LAAgent "
                        +
                        " Where AgentState in ('01','02')) and agentgroup in (";
                for (int i = 1; i <= mLABranchGroupSet.size(); i++)
                {
                    LABranchGroupSchema tLABranchGroupSchema = new
                            LABranchGroupSchema();
                    tLABranchGroupSchema = mLABranchGroupSet.get(i);
                    strSQL = strSQL + tLABranchGroupSchema.getAgentGroup();
                    if (i != mLABranchGroupSet.size())
                    {
                        strSQL = strSQL + ",";
                    }
                }
                strSQL = strSQL + ")";
                System.out.println("------strSQL---" + strSQL);
                if (!setUpAgent(strSQL, tAgentGroup, tManager))
                {
                    return false;
                }
            }
        }
        //如果没有下属机构，则该机构一定时一个组，则置组内的人员的上级代理人为新任主管，并且将该组的信息进行备份与更新
        else
        {
            strSQL = "select * from latree where agentgroup='" + tAgentGroup +
                     "'" + " And AgentCode in (Select AgentCode From LAAgent " +
                     " Where AgentState in ('01','02'))";
            System.out.println("------strSQL---" + strSQL);
            //LATreeSet tLATreeSet = new LATreeSet();
            if (!setUpAgent(strSQL, tAgentGroup, tManager))
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 查询tAgentCode的基础信息
     *
     * @param tAgentCode String
     * @return com.sinosoft.lis.schema.LAAgentSchema
     */
    private LAAgentSchema getAgentBasicInfo(String tAgentCode)
    {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALABranchManagerBL";
            tError.functionName = "getAgentBasicInfo";
            tError.errorMessage = "查询代理人" + tAgentCode + "的基础信息失败！";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLAAgentDB.getSchema();
    }


    /**
     * 查询代理人编码为agentCode的代理人的行政信息
     *
     * @param agentCode String
     * @return com.sinosoft.lis.schema.LATreeSchema
     */
    private LATreeSchema getAgentTreeInfo(String agentCode)
    {
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(agentCode);
        if (!tLATreeDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALABranchManagerBL";
            tError.functionName = "getAgentTreeInfo";
            tError.errorMessage = "查询" + agentCode + "的行政信息失败！";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLATreeDB.getSchema();
    }

    private boolean getDirectDownBranch(String cBranch)
    {
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setUpBranch(cBranch);
        tLABranchGroupDB.setUpBranchAttr("1");
        //没有找到下级直辖机构，返回本身
        if (!tLABranchGroupDB.getInfo())
        {
            return true;
        }
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = tLABranchGroupDB.getSchema();
        mLABranchGroupSet.add(tLABranchGroupSchema);
        return getDirectDownBranch(tLABranchGroupSchema.getAgentGroup().trim());
    }

    private boolean setUpAgent(String cSQL, String cAgentGroup, String cManager)
    {
        System.out.println(cManager);
        LATreeSchema tLATreeSchema = new LATreeSchema();
        LATreeDB tLATreeDB = new LATreeDB();
        LATreeSet tLATreeSet = tLATreeDB.executeQuery(cSQL);
        if (tLATreeDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALABranchManagerBL";
            tError.functionName = "setDirInst";
            tError.errorMessage = "查询" + cAgentGroup + "的所有成员时失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("----tLATreeSet.size----" + tLATreeSet.size());
        for (int i = 1; i <= tLATreeSet.size(); i++)
        {
            //备份组员的行政信息
            tLATreeSchema = tLATreeSet.get(i);
            System.out.println(tLATreeSchema.getAgentCode());
            if (tLATreeSchema.getAgentCode().equals(cManager))
            {
                continue; //如果主管在这个组中，则主管的上级代理人信息不在这做修改
            }
            if (tLATreeSchema.getUpAgent() != null &&
                tLATreeSchema.getUpAgent().equals(cManager))
            {
                continue; //如果上级代理人并没有变，则不用备份修改
            }
            System.out.println("A+" + i);
            LATreeBSchema tLATreeBSchema = new LATreeBSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLATreeBSchema, tLATreeSchema);
            tLATreeBSchema.setAstartDate(tLATreeSchema.getAstartDate());
            tLATreeBSchema.setEdorNO(mEdorNo);
            tLATreeBSchema.setRemoveType("07"); //主管任命
            tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
            tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
            tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
            tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
            tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
            tLATreeBSchema.setMakeDate(currentDate);
            tLATreeBSchema.setMakeTime(currentTime);
            tLATreeBSchema.setModifyDate(currentDate);
            tLATreeBSchema.setModifyTime(currentTime);
            tLATreeBSchema.setOperator(this.mGlobalInput.Operator);
            map.put(tLATreeBSchema, "INSERT");
            //更新组员的行政信息
            System.out.println("B+" + i);
            tLATreeSchema.setAstartDate(currentDate);
            tLATreeSchema.setUpAgent(cManager);
            tLATreeSchema.setModifyDate(currentDate);
            tLATreeSchema.setModifyTime(currentTime);
            tLATreeSchema.setOperator(this.mGlobalInput.Operator);
            map.put(tLATreeSchema, "UPDATE");
        }
        return true;
    }
}
