package com.sinosoft.lis.agent;

import utils.system;

import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAUpBranchAttrBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors=new CErrors();
	private VData mResult = new VData();
	/** 往后面传输数据的容器 */
	private VData mInputData= new VData();
	private MMap map = new MMap();
	/** 统一更新日期 */
	private String mCurrentDate = PubFun.getCurrentDate();
	/** 统一更新时间 */
	private String mCurrentTime = PubFun.getCurrentTime();
	/** 全局数据 */
	private GlobalInput mGlobalInput =new GlobalInput() ;
	/** 数据操作字符串 */
	private String mOperate;
	/** 业务处理相关变量 */
	private LABranchGroupSchema laBranchGroupSchema = new LABranchGroupSchema();
	public LAUpBranchAttrBL(){
	}
	public static void main(String[] args) {
	}
	/**
	* 传输数据的公共方法
	* @param: cInputData 输入的数据
	*         cOperate 数据操作
	* @return:
	*/
	public boolean submitDate(VData cInputData,String cOperate){
		//将操作数据拷贝到本类中
		this.mOperate=cOperate;
		System.out.print("LAUpBranchAttrBL submitData mOperate:"+mOperate);
		//得到外部传入的数据，将数据备份到本类中
		if(!getInputData(cInputData)){
			System.out.println("getInputData error");
			return false;
			}
		//进行业务处理
		if(!dealData()){
			System.out.println("dealData error");
			//@@错误处理
			CError tError = new CError();
	          tError.moduleName = "LAUpBranchAttrBL";
	          tError.functionName = "submitData";
	          tError.errorMessage = "数据处理失败LAUpBranchAttrBL-->dealData!";
	          this.mErrors .addOneError(tError) ;
	          return false;
		}
		//准备往后台的数据
		if(!prepareOutputData()){
			System.out.println("prepareOutputData error");
			return false;
		}
		else{
			PubSubmit tPubSubmit=new PubSubmit(); 
			boolean flag=tPubSubmit.submitData(mInputData, mOperate);
		    System.out.println("flag:"+flag);
			if(!flag){
				//@@错误处理
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				CError tError=new CError();
				tError.moduleName="PubSubmit";
				tError.functionName="submitData";
				tError.errorMessage="数据提交失败！";
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		mInputData = null;
	    return true;
	}
	 /**
	 * 根据前面的输入数据，进行BL逻辑处理
	 * 如果在处理过程中出错，则返回false,否则返回true
	*/
	private boolean dealData()
	{
	    if (mOperate.equals("UPDATE"))
	    {
	        String sql = "Update LABranchGroup set " +
	                     "UpBranchAttr = '" + laBranchGroupSchema.getUpBranchAttr() + "', " +
                         "Operator = '" + mGlobalInput.Operator + "', " +
	                     "ModifyDate = '" + mCurrentDate + "', " +
	                     "ModifyTime = '" + mCurrentTime + "' " +
	                     "Where  AgentGroup = '" + laBranchGroupSchema.getAgentGroup() + "' ";
	        map.put(sql, "UPDATE"); //修改
	    }
	      return true;
	}
	/**
	* 根据前面的输入数据，进行BL逻辑处理
	* 如果在处理过程中出错，则返回false,否则返回true
	*/
	private boolean updateData()
	{
	   return true;
	}
	/**
	* 根据前面的输入数据，进行BL逻辑处理
	* 如果在处理过程中出错，则返回false,否则返回true
	*/
	private boolean deleteData()
	{
	  return true;
	}
	 /**
	 * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData)
	{
	     this.laBranchGroupSchema.setSchema((LABranchGroupSchema)cInputData.getObjectByObjectName("LABranchGroupSchema",0));
	     System.out.println("LABranchGroupSchema:"+(LABranchGroupSchema)cInputData.getObjectByObjectName("LABranchGroupSchema",0));
	    return true;
	}
     private boolean prepareOutputData()
	 {
	   try
		{
	            this.mInputData.clear();
	            this.mInputData.add(this.laBranchGroupSchema);
	            mInputData.add(this.map);
	            mResult.clear();
	            mResult.add(this.laBranchGroupSchema);
	        }
		catch(Exception ex)
		{
	 		// @@错误处理
			CError tError =new CError();
	 		tError.moduleName="LAUpBranchAttrBL";
	 		tError.functionName="prepareData";
	 		tError.errorMessage="在准备往后层处理所需要的数据时出错。";
	 		this.mErrors .addOneError(tError) ;
			return false;
		}
		return true;
		}
		public VData getResult()
		{
	  	return this.mResult;
		}

}
