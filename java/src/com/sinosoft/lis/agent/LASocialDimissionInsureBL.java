/*
 * <p>ClassName: ALADimissionBL </p>
 * <p>Description: ALADimissionBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agent;
import com.cbsws.obj.RspUniSalescod;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LADimissionDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LADimissionSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.lis.schema.LARecomRelationSchema;
import com.sinosoft.lis.agentbranch.DimChangeAgentToBranch;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.agentbranch.DimChangeRearBranch;
import com.sinosoft.lis.schema.LARearRelationSchema;
import com.sinosoft.lis.schema.LARearRelationBSchema;
import com.sinosoft.lis.vschema.LARearRelationBSet;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LARecomRelationBSchema;
import com.sinosoft.lis.vschema.LARecomRelationBSet;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LAOrphanPolicySet;
import com.sinosoft.lis.schema.LAOrphanPolicySchema;
import com.sinosoft.lis.sys.UnisalescodFun;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.vschema.LCGrpContSet;

public class LASocialDimissionInsureBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String mAdjustDate = "";
    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LADimissionSchema mLADimissionSchema = new LADimissionSchema();
    private LADimissionSet mLADimissionSet = new LADimissionSet();
    private LABranchGroupSet  mupLABranchGroupSet = new LABranchGroupSet();
    private LABranchGroupSet  mlvLABranchGroupSet = new LABranchGroupSet();
    
    private LARecomRelationSet mLARecomRelationSet = new LARecomRelationSet();
    private LARecomRelationBSet mLARecomRelationBSet = new LARecomRelationBSet();
    private LARearRelationSet mLARearRelationSet = new LARearRelationSet();
    private LARearRelationBSet mLARearRelationBSet = new LARearRelationBSet();
    private LABranchGroupBSet  minLABranchGroupBSet= new LABranchGroupBSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAAgentBSchema mLAAgentBSchema = new LAAgentBSchema();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LATreeBSet mLATreeBSet = new LATreeBSet();
    private LAOrphanPolicySet mLAOrphanPolicySet = new LAOrphanPolicySet();
    private MMap mMap = new MMap();
    private String currentDate ="";
    private String currentTime ="";
    private String  mEdorNo="";
    private String branchtype="";
    private String branchtype2="";
    public LASocialDimissionInsureBL()
    {
    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";
        String tsql="select *  from  ladimission where  agentcode='1101000114' ";
        LADimissionSchema tLADimissionSchema = new LADimissionSchema();
        LADimissionSet tLADimissionSet = new LADimissionSet();
        LADimissionDB tLADimissionDB = new LADimissionDB();
        tLADimissionSet=tLADimissionDB.executeQuery(tsql);
        tLADimissionSchema=tLADimissionSet.get(1);
        tLADimissionSchema.setDepartDate("2007-01-01");
        VData tVData = new VData();
        tVData.addElement(tLADimissionSchema);
        tVData.add(tG);

        ALADimissionBL  tALADimissionBL= new ALADimissionBL();
        tALADimissionBL.submitData(tVData,"INSERT||MAIN");
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALADimissionBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALADimissionBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALADimissionBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
         }

        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    { 
        boolean tReturn = true;
        /**获取集团工号Unisalescod*/
        String tSQL = "select * from laagent where agentcode='"+mLADimissionSchema.getAgentCode()+"'";
        LAAgentDB mLAAgentDB = new LAAgentDB();
        LAAgentSet tlaagentset=new LAAgentSet();
        tlaagentset= mLAAgentDB.executeQuery(tSQL);
        UnisalescodFun fun=new UnisalescodFun();
        RspUniSalescod mRspUniSalescod = new RspUniSalescod();
        mRspUniSalescod=fun.getUnisalescod("UPDATE||MAIN", tlaagentset.get(1), mLADimissionSchema, tlaagentset.get(1).getGroupAgentCode(),null);
        if ("01".equals(mRspUniSalescod.getMESSAGETYPE())) {
			CError tError = new CError();
			tError.moduleName = "ALADimissionBL";
			tError.functionName = "dealData";
			tError.errorMessage = mRspUniSalescod.getERRDESC();
			this.mErrors.addOneError(tError);
			return false;
		} 
        mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        currentDate = PubFun.getCurrentDate();
        currentTime = PubFun.getCurrentTime();
        if (mOperate.equals("INSERT||MAIN"))
        {
            //在LADimission表里插入离职状态信息
             LADimissionDB tLADimissionDB = new LADimissionDB();
             tLADimissionDB.setAgentCode(mLADimissionSchema.getAgentCode());
             tLADimissionDB.setDepartTimes(mLADimissionSchema.getDepartTimes());
             if (!tLADimissionDB.getInfo())
             {
                 CError tError = new CError();
                 tError.moduleName = "ALADimissionBL";
                 tError.functionName = "dealData";
                 tError.errorMessage = "查询代理人原离职信息失败!";
                 this.mErrors.addOneError(tError);
                 return false;
             }


             if (this.mLADimissionSchema.getDepartTimes() == 1)
            {
                this.mLADimissionSchema.setDepartState("06"); //离职确认
            }
            else
            {
                this.mLADimissionSchema.setDepartState("07"); //二次离职确认
            }
            mLADimissionSchema.setBranchAttr(AgentPubFun.getAgentBranchAttr(
            mLADimissionSchema.getAgentCode())); //转换机构编码
            mLADimissionSchema.setMakeDate(tLADimissionDB.getMakeDate());
            mLADimissionSchema.setMakeTime(tLADimissionDB.getMakeTime());
            mLADimissionSchema.setModifyDate(currentDate);
            mLADimissionSchema.setModifyTime(currentTime);
            mLADimissionSchema.setOperator(mGlobalInput.Operator);
        }

            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(this.mLADimissionSchema.getAgentCode());
            System.out.println(this.mLADimissionSchema.getAgentCode());
            if (!tLAAgentDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ALADimissionBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询代理人信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            Reflections  tReflections= new Reflections();
            tReflections.transFields(mLAAgentBSchema,tLAAgentDB.getSchema());
            mLAAgentBSchema.setEdorNo(mEdorNo);
            mLAAgentBSchema.setEdorType("08");
            tLAAgentDB.setOutWorkDate(this.mLADimissionSchema.getDepartDate());
            if (this.mLADimissionSchema.getDepartTimes() == 1)
            {
                tLAAgentDB.setAgentState("06"); //一次离职确认
            }
            else
            {
                tLAAgentDB.setAgentState("06"); //二次离职确认  原来状态为07，现在统一变为06状态   update by lh 2009-7-1
            }
            tLAAgentDB.setOperator(this.mGlobalInput.Operator);
            tLAAgentDB.setModifyDate(currentDate);
            tLAAgentDB.setModifyTime(currentTime);
            this.mLAAgentSchema.setSchema(tLAAgentDB);


          branchtype=this.mLADimissionSchema.getBranchType();
          branchtype2=this.mLADimissionSchema.getBranchType2();
          if(branchtype.equals("2")&&branchtype2.equals("03")){
        	LATreeDB tLATreeDB = new LATreeDB();
            String tAgentCode = this.mLADimissionSchema.getAgentCode(); //离职人员人员
            LATreeSchema tLATreeSchema = new LATreeSchema();
            tLATreeDB.setAgentCode(tAgentCode);
            tLATreeDB.getInfo();
            tLATreeSchema = tLATreeDB.getSchema();

            String tAgentGrade = tLATreeSchema.getAgentGrade();
            String tAgentSeris = AgentPubFun.getAgentSeries(tAgentGrade);
            
            if (tAgentSeris.equals("1"))
            {
           	 if (!dealBranch()) {
                    buildError("dealWarden", "修改团队为无主管团队时出错!");
                    return false;
                }
                LATreeSet tLATreeSet = new LATreeSet();
                //查询团队下除了离职主管人员 的所有人,以便进行人员调动
                String tSql = "select * from  latree a where upagent='" +
                              tAgentCode
                              + "' and agentcode<>'" + tAgentCode + "'"
                              + "  and  not exists "
                              + " (select b.agentcode from laagent b where a.agentcode=b.agentcode and b.agentstate>='06' ) ";
                tLATreeSet = tLATreeDB.executeQuery(tSql);
                //置upagent 为 空
                if (!dealupAgent(tLATreeSet)) {
                    buildError("dealWarden", "修改团队为无主管团队时出错!");
                    return false;
                }
            }
            
        }
        tReturn = true;
        return tReturn;
    }
    //处理主管所在的团队 (如果是区经理,则还要处理直辖处),使团队为无主管团队
    private boolean dealBranch()
    {
        //主管离职的处理 团队停业
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setBranchManager(this.mLADimissionSchema.getAgentCode());
        System.out.println(this.mLADimissionSchema.getAgentCode());

        tLABranchGroupSet = tLABranchGroupDB.query();
        for (int i = 1; i <= tLABranchGroupSet.size(); i++) {
            LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
            LABranchGroupBSchema tLABranchGroupBSchema = new LABranchGroupBSchema();
            tLABranchGroupSchema = tLABranchGroupSet.get(i);
            //备份labranchgroupb表
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLABranchGroupBSchema, tLABranchGroupSchema);
            System.out.println(tLABranchGroupBSchema.getAgentGroup() + "|||||2");
            tLABranchGroupBSchema.setEdorType("08"); //修改操作

            tLABranchGroupBSchema.setEdorNo(mEdorNo);
            tLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.getMakeDate());
            tLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.getMakeTime());
            tLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.
                                                 getModifyDate());
            tLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.
                                                 getModifyTime());
            tLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.getOperator());
            tLABranchGroupBSchema.setMakeDate(currentDate);
            tLABranchGroupBSchema.setMakeTime(currentTime);
            tLABranchGroupBSchema.setModifyDate(currentDate);
            tLABranchGroupBSchema.setModifyTime(currentTime);
            tLABranchGroupBSchema.setOperator(this.mGlobalInput.Operator);
            minLABranchGroupBSet.add(tLABranchGroupBSchema);
            //修改labranchgroup 中的主管为空
            tLABranchGroupSchema.setBranchManager("");
            tLABranchGroupSchema.setBranchManagerName("");
            tLABranchGroupSchema.setModifyDate(currentDate);
            tLABranchGroupSchema.setModifyTime(currentTime);
            tLABranchGroupSchema.setOperator(mGlobalInput.Operator);
            mupLABranchGroupSet.add(tLABranchGroupSchema);
        }
        return true ;
    }
    //处理主管所在的团队 (如果是区经理,则还要处理直辖处),使团队为无主管团队
    private boolean dealupAgent(LATreeSet tLATreeSet)
    {
        //主管离职的处理 团队停业
        if(tLATreeSet==null || tLATreeSet.size()<=0)
        {
            return true ;
        }
        for (int i=1;i<=tLATreeSet.size();i++)
        {
            LATreeSchema tLATreeSchema= new LATreeSchema();
            tLATreeSchema=tLATreeSet.get(i);
            LATreeBSchema tLATreeBSchema= new LATreeBSchema();
            Reflections tReflections= new Reflections();
            tReflections.transFields(tLATreeBSchema,tLATreeSchema) ;
            tLATreeBSchema.setEdorNO(mEdorNo);
            tLATreeBSchema.setRemoveType("08");
            tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
            tLATreeBSchema.setMakeDate2(tLATreeBSchema.getMakeDate());
            tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
            tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
            tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
             tLATreeBSchema.setOperator(mGlobalInput.Operator);
            tLATreeBSchema.setMakeDate(currentDate);
            tLATreeBSchema.setMakeTime(currentTime);
            tLATreeBSchema.setModifyDate(currentDate);
            tLATreeBSchema.setModifyTime(currentTime);
            mLATreeBSet.add(tLATreeBSchema);
            tLATreeSchema.setUpAgent("");
            tLATreeSchema.setModifyDate(currentDate);
            tLATreeSchema.setModifyTime(currentTime);
            mLATreeSet.add(tLATreeSchema);
        }

        return true ;
    }
 
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLADimissionSchema.setSchema((LADimissionSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LADimissionSchema",
                                                  0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        mAdjustDate=mLADimissionSchema.getDepartDate();
        //当月的一日进行人员调动或则团队调动
        String newStartDate = AgentPubFun.formatDate(mAdjustDate, "yyyyMM");
        mAdjustDate = newStartDate+"01" ;
        mAdjustDate=AgentPubFun.formatDate(mAdjustDate, "yyyy-MM-dd");
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start ALADimissionBLQuery Submit...");
        LADimissionDB tLADimissionDB = new LADimissionDB();
        tLADimissionDB.setSchema(this.mLADimissionSchema);
        this.mLADimissionSet = tLADimissionDB.query();
        this.mResult.add(this.mLADimissionSet);
        System.out.println("End ALADimissionBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLADimissionDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLADimissionDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALADimissionBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mMap.put(this.mLADimissionSchema,"UPDATE");
            this.mMap.put(this.mLAAgentSchema,"UPDATE");
            this.mMap.put(this.mLAAgentBSchema,"INSERT");
            this.mMap.put(this.mupLABranchGroupSet,"UPDATE");
            this.mMap.put(this.mlvLABranchGroupSet,"UPDATE");
            this.mMap.put(this.minLABranchGroupBSet,"INSERT");
            this.mMap.put(this.mLARecomRelationSet,"UPDATE");
            this.mMap.put(this.mLARearRelationSet,"DELETE");
            this.mMap.put(this.mLARearRelationBSet,"INSERT");
            this.mMap.put(this.mLATreeSet,"UPDATE");
            this.mMap.put(this.mLATreeBSet,"INSERT");
            this.mMap.put(this.mLAOrphanPolicySet,"DELETE&INSERT");
            mInputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALADimissionBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    /**
     * 校验函数
     * 对传入的数据进行信息校验
     * @return boolean
     */
    private boolean checkData()
    {
        int tCount;
        String tAgentCode;
        tAgentCode = mLADimissionSchema.getAgentCode(); //得到离职人员编码
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        //查询代理人表
        tCount = tLAAgentDB.getCount();
        System.out.println("tCount:" + Integer.toString(tCount));
        //对查询结果判定
        if (tCount == -1)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALADimissionBL";
            tError.functionName = "checkData()";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else if (tCount == 0)
        {
            //代理人编码录入错误
            CError tError = new CError();
            tError.moduleName = "ALADimissionBL";
            tError.functionName = "checkData()";
            tError.errorMessage = "没有这个代理人!";
            this.mErrors.addOneError(tError);
            return false;
        }
        // 验证离职类型是否正确
        LDCodeDB tLDCodeDB = new LDCodeDB();
        LDCodeSet tLDCodeSet = new LDCodeSet();
        tLDCodeDB.setCodeType("departrsn");
        tLDCodeDB.setCode(mLADimissionSchema.getDepartRsn());
        tLDCodeSet = tLDCodeDB.query();
        if(!tLDCodeDB.mErrors.needDealError())
        {
            if(tLDCodeSet.size()==0)
            {
                //代理人编码录入错误
                CError tError = new CError();
                tError.moduleName = "ALADimissionBL";
                tError.functionName = "checkData()";
                tError.errorMessage = "离职类型设置不正确！请选择录入正确的离职类型！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }
    //错误处理
    private void buildError(String szFunc, String szErrMsg) {
            CError cError = new CError();

            cError.moduleName = "AdjustAgentBL";
            cError.functionName = szFunc;
            cError.errorMessage = szErrMsg;
            this.mErrors.addOneError(cError);
    }
}
