/*
 * <p>ClassName: ALAComBL </p>
 * <p>Description: ALAComBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 代理机构
 * @CreateDate：2003-01-09
 */
package com.sinosoft.lis.agent;

import java.math.BigInteger;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LAContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAComBSchema;
import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.lis.schema.LAContSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LAComSet;
import com.sinosoft.lis.vschema.LAContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

public class   LAAgentdeleBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    String currentDate = PubFun.getCurrentDate();
    String currentTime = PubFun.getCurrentTime();
    /** 数据操作字符串 */
    private String mOperate;
    private String mEndFlag1;
    private String Bank;
    private String mCoverFlag=null;
    private String mStartDate=null;
    private String mEndDate=null;
    private String mProtocolNo=null;
    /** 业务处理相关变量 */
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAAgentSet mLAAgentSet= new LAAgentSet();
    private MMap mMap = new MMap();
    public  LAAgentdeleBL()
    {}
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("哆啦A梦："+mOperate);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if(!checkData())
        {
        	 // @@错误处理
            CError tError = new CError();
           tError.moduleName = "LAAgentdeleBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAAgentdeleBL-->checkData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentdeleBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAAgentdeleBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start LAAgentdeleBL Submit...");
        	PubSubmit tPubSubmit = new PubSubmit();            
            //如果有需要处理的错误，则返回
            if (!tPubSubmit.submitData(mInputData,mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentdeleBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }
  /**
   * 进行提交前的数据校验
   */
    private boolean checkData()
    {
    	return true;
    }
    
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        
        int tCount = 0;
        LAComSet tempLAComSet = new LAComSet();
        
        System.out.println("我是苹果");
        if (this.mOperate.equals("DELETE"))
        {
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(this.mLAAgentSchema.getAgentCode());
            tLAAgentDB.setManageCom(this.mLAAgentSchema.getManageCom());
            tLAAgentDB.setAgentGroup(this.mLAAgentSchema.getAgentGroup());
            tLAAgentDB.setBranchCode(this.mLAAgentSchema.getBranchCode());
            if (!tLAAgentDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentdeleBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "该代理机构代码不存在！";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (this.mOperate.equals("DELETE"))
            {
            	System.out.println("修改时从前台获取的agentcode为："+this.mLAAgentSchema.getAgentCode());
                System.out.println(mLAAgentSchema.getManageCom());
                System.out.println("apple:"+ this.mLAAgentSchema.getIDNo());
                String tSQL1 = "delete from laagent where agentcode='"+tLAAgentDB.getAgentCode()+"' and managecom='"+
                tLAAgentDB.getManageCom()+"' and idno='"+tLAAgentDB.getIDNo()+"'";
               System.out.println("最后更新lacom表中state记录的SQL: " + tSQL1);
               ExeSQL tExeSQL = new ExeSQL();
               System.out.println("1111111111111111111");
                boolean tValue = tExeSQL.execUpdateSQL(tSQL1);
                String tSQL2="delete from laqualification where agentcode='"+tLAAgentDB.getAgentCode()+"'";
                ExeSQL tExeSQL1 = new ExeSQL();
                boolean tValue1 = tExeSQL1.execUpdateSQL(tSQL2);
                String tSQL3="delete from lacertification where agentcode='"+tLAAgentDB.getAgentCode()+"'";
                ExeSQL tExeSQL2 = new ExeSQL();
                boolean tValue2 = tExeSQL2.execUpdateSQL(tSQL3);
                String tSQL4="delete from lawarrantor where agentcode='"+tLAAgentDB.getAgentCode()+"'";
                ExeSQL tExeSQL3 = new ExeSQL();
                boolean tValue3 = tExeSQL3.execUpdateSQL(tSQL4);
                String tSQL6="delete from larecomrelation where agentcode='"+tLAAgentDB.getAgentCode()+"'";
                ExeSQL tExeSQL5 = new ExeSQL();
                boolean tValue5 = tExeSQL5.execUpdateSQL(tSQL6);
                String tSQL7="delete from larearrelation where agentcode='"+tLAAgentDB.getAgentCode()+"'";
                ExeSQL tExeSQL6 = new ExeSQL();
                boolean tValue6 = tExeSQL6.execUpdateSQL(tSQL7);
                String tSQL9="select agentgrade from latree where agentcode='"+tLAAgentDB.getAgentCode()+"'";
                ExeSQL tExeSQL8 = new ExeSQL();
                String agentgrade= tExeSQL8.getOneValue(tSQL9);
                String tSQL10="select branchmanager from labranchgroup where agentgroup='"+tLAAgentDB.getAgentGroup()+"'";
                ExeSQL tExeSQL9 = new ExeSQL();
                String branchmanager= tExeSQL8.getOneValue(tSQL10);
                System.out.println("(●—●)大白："+branchmanager);
                if(tLAAgentDB.getAgentCode().equals(branchmanager)&& agentgrade.compareTo("B01")>0){
                	System.out.println("大熊猫");
                	 String tSQL11="update labranchgroup set branchmanager=null,branchmanagername=null,modifydate=current date,modifytime=current time where agentgroup='"+tLAAgentDB.getAgentGroup()+"'";
                     ExeSQL tExeSQL10 = new ExeSQL();
                     boolean tValue10 = tExeSQL10.execUpdateSQL(tSQL11);
                     String tSQL12="update labranchgroup set branchmanager=null,branchmanagername=null,modifydate=current date,modifytime=current time where agentgroup='"+tLAAgentDB.getBranchCode()+"'";
                     ExeSQL tExeSQL11 = new ExeSQL();
                     boolean tValue11 = tExeSQL11.execUpdateSQL(tSQL12);
                }
                String tSQL5="delete from latree where agentcode='"+tLAAgentDB.getAgentCode()+"'";
                ExeSQL tExeSQL4 = new ExeSQL();
                boolean tValue4 = tExeSQL4.execUpdateSQL(tSQL5);
       if (tExeSQL.mErrors.needDealError()) {
         // @@错误处理
         this.mErrors.copyAllErrors(tExeSQL.mErrors);
         CError tError = new CError();
         tError.moduleName = "LAAgentdeleBL";
         tError.functionName = "returnState";
         tError.errorMessage = "执行SQL语句,删除表记录失败!";
         this.mErrors.addOneError(tError);
         System.out.println("执行SQL语句,删除表记录失败!");
         return false;
     }
            }
        }
        return true;
    }
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                getObjectByObjectName("LAAgentSchema", 0));
        System.out.println("wwwwwwww:"+this.mLAAgentSchema.getSchema().getAgentCode());
        System.out.println("RRRRR:"+this.mLAAgentSchema.getSchema().getManageCom());
        System.out.println("QQQQQQ:"+this.mLAAgentSchema.getSchema().getIDNo());
        return true;
    }
    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start LAAgentdeleBL Submit...");
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setSchema(this.mLAAgentSchema);
        this.mLAAgentSet = tLAAgentDB.query();
        this.mResult.add(this.mLAAgentSet);
        System.out.println("End LAAgentdeleBL Submit...");
        //如果有需要处理的错误，则返回
        if (tLAAgentDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentdeleBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }


    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
           this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentdeleBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    public VData getResult()
    {
        return this.mResult;
    }
}
