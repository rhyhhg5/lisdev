/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentBlacklistDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeAccessoryDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeAccessoryBSchema;
import com.sinosoft.lis.schema.LATreeAccessorySchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAgentBlacklistSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LATreeAccessoryBSet;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.lis.vschema.LAWarrantorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;


/*
 * <p>ClassName: LAAgentSecondBL </p>
 * <p>Description: LAAgentSecondBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-09
 */
public class LAAgentSecondBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();

    /** 数据操作字符串 */
    private String mOperate;
    private String mIsManager;
    private String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
    private Reflections tReflections = new Reflections();

    /** 业务处理相关变量 */
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAAgentBSchema mLAAgentBSchema = new LAAgentBSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private LATreeBSchema mLATreeBSchema = new LATreeBSchema();
    private LATreeAccessorySet mLATreeAccessorySet = new LATreeAccessorySet();
    private LATreeAccessoryBSet mLATreeAccessoryBSet = new LATreeAccessoryBSet();
    private LABranchGroupSchema mLABranchGroupSchema = new LABranchGroupSchema();
    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();
    private LAWarrantorSet mLAWarrantorSet = new LAWarrantorSet();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    public LAAgentSecondBL()
    {
    }

    public static void main(String[] args)
    {
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //校验该代理人是否在黑名单中存在
        if (!checkData())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentSecondBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAAgentSecondBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("over dealData");
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start LAAgentSecondBL Submit...");
            LAAgentSecondBLS tLAAgentSecondBLS = new LAAgentSecondBLS();
            tLAAgentSecondBLS.submitData(mInputData, cOperate);
            System.out.println("End LAAgentSecondBL Submit...");
            //如果有需要处理的错误，则返回
            if (tLAAgentSecondBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentSecondBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentSecondBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mResult.add(this.mLAAgentSchema.getAgentCode());
        }
        mInputData = null;
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String tAgentCode = "", tNewAgent = "";
        String tAgentGroup = "", tZSGroupCode = "";
        //代理人代码
        tAgentCode = this.mLAAgentSchema.getAgentCode();
        String tPrefix = this.mLAAgentSchema.getManageCom().substring(0, 4);
        tNewAgent = tPrefix + PubFun1.CreateMaxNo("AgentCode" + tPrefix, 6);
        //确定代理人系列
        String tAgentGradeSeries = this.mLATreeSchema.getAgentGrade().trim();
        String tSQL =
                "select code2 from ldcodeRela where relaType = 'gradeserieslevel' "
                + "and code1 = '" + tAgentGradeSeries + "'";
        ExeSQL tExeSQL = new ExeSQL();
        tAgentGradeSeries = tExeSQL.getOneValue(tSQL).trim();
        if (tExeSQL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "dealData";
            tError.errorMessage = "执行SQL语句：从表中取值失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //确定销售机构AgentGroup
        tZSGroupCode = this.mLAAgentSchema.getBranchCode().trim(); //直辖组的隐式代码
        tAgentGroup = this.mLAAgentSchema.getAgentGroup().trim(); //显示代码
        String tAgentGrade = this.mLATreeSchema.getAgentGrade().trim();
        switch (Integer.parseInt(tAgentGrade.substring(1)))
        {
            case 6:
            case 7:
            {
                tAgentGroup = tAgentGroup.substring(0, tAgentGroup.length() - 3); //高级经理
                break;
            }
            case 8:
            {
                tAgentGroup = tAgentGroup.substring(0, tAgentGroup.length() - 6); //督导长
                break;
            }
            case 9:
            {
                tAgentGroup = tAgentGroup.substring(0, tAgentGroup.length() - 8); //区域督导长
                break;
            }
        }
        System.out.println("----查询职级对应的实际的机构隐式代码");
        tSQL = "select AgentGroup from laBranchGroup where BranchAttr = '" +
               tAgentGroup + "'";
        tExeSQL = new ExeSQL();
        tAgentGroup = tExeSQL.getOneValue(tSQL).trim();
        if (tExeSQL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "dealData";
            tError.errorMessage = "执行SQL语句：从表中取值失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //备份代理人信息
        if (!prepareCopyAgent(tAgentCode))
        {
            return false;
        }
        this.mLAAgentBSchema.setNewAgentCode(tNewAgent);
        this.mLAAgentBSchema.setAgentKind("01"); //客户经理
        this.mLAAgentBSchema.setMakeDate(currentDate);
        this.mLAAgentBSchema.setMakeTime(currentTime);
        this.mLAAgentBSchema.setModifyDate(currentDate);
        this.mLAAgentBSchema.setModifyTime(currentTime);
        this.mLAAgentBSchema.setOperator(mGlobalInput.Operator);
        //二次增员的新信息
        this.mLAAgentSchema.setAgentCode(tNewAgent);
        this.mLAAgentSchema.setAgentGroup(tAgentGroup);
        this.mLAAgentSchema.setAgentState("02");
        this.mLAAgentSchema.setEmployDate(currentDate);
        this.mLAAgentSchema.setModifyDate(currentDate);
        this.mLAAgentSchema.setModifyTime(currentTime);
        this.mLAAgentSchema.setMakeDate(currentDate);
        this.mLAAgentSchema.setMakeTime(currentTime);
        this.mLAAgentSchema.setOperator(mGlobalInput.Operator);

        //备份离职前的行政信息
        if (!prepareCopyTree(tAgentCode))
        {
            return false;
        }
        this.mLATreeBSchema.setMakeDate(currentDate);
        this.mLATreeBSchema.setMakeTime(currentTime);
        this.mLATreeBSchema.setModifyDate(currentDate);
        this.mLATreeBSchema.setModifyTime(currentTime);
        this.mLATreeBSchema.setOperator(mGlobalInput.Operator);
        //二次增员的行政信息
        this.mLATreeSchema.setAgentCode(tNewAgent);
        this.mLATreeSchema.setAgentGroup(tAgentGroup);
        this.mLATreeSchema.setAgentSeries(tAgentGradeSeries);
        this.mLATreeSchema.setAssessType("0"); //正常
        this.mLATreeSchema.setState("0");
        if ((this.mLATreeSchema.getIntroAgency() != null) &&
            (!this.mLATreeSchema.getIntroAgency().equals("")))
        {
            this.mLATreeSchema.setIntroBreakFlag("0");
            this.mLATreeSchema.setIntroCommStart(this.mLAAgentSchema.
                                                 getEmployDate());
        }
        this.mLATreeSchema.setStartDate(this.mLAAgentSchema.getEmployDate());
        this.mLATreeSchema.setAstartDate(this.mLAAgentSchema.getEmployDate());
        this.mLATreeSchema.setOperator(mGlobalInput.Operator);
        this.mLATreeSchema.setModifyDate(currentDate);
        this.mLATreeSchema.setModifyTime(currentTime);
        this.mLATreeSchema.setMakeDate(currentDate);
        this.mLATreeSchema.setMakeTime(currentTime);

        //行政附属信息 及备份信息
        if (!prepareCopyTreeAccessory(tAgentCode))
        {
            return false;
        }
        if (tAgentGrade.compareTo("A04") >= 0)
        {
            LATreeAccessorySchema tLATreeAccessorySchema = new
                    LATreeAccessorySchema();
            tLATreeAccessorySchema.setAgentCode(tNewAgent);
            tLATreeAccessorySchema.setAgentGroup(tAgentGroup);
            tLATreeAccessorySchema.setAgentGrade(this.mLATreeSchema.
                                                 getAgentGrade());
            tLATreeAccessorySchema.setAgentGroup(this.mLATreeSchema.
                                                 getAgentGroup());
            /*lis5.3 update
             tLATreeAccessorySchema.setManageCom(this.mLATreeSchema.getManageCom());
             */
            if ((this.mLATreeSchema.getEduManager() != null) &&
                (!this.mLATreeSchema.getEduManager().equals("")))
            {
                tLATreeAccessorySchema.setRearAgentCode(this.mLATreeSchema.
                        getEduManager());
            }
            tLATreeAccessorySchema.setCommBreakFlag("0");
            tLATreeAccessorySchema.setIntroBreakFlag("0");
            tLATreeAccessorySchema.setBackCalFlag("0"); //不回算
            tLATreeAccessorySchema.setstartdate(this.mLAAgentSchema.
                                                getEmployDate());
            tLATreeAccessorySchema.setMakeDate(currentDate);
            tLATreeAccessorySchema.setMakeTime(currentTime);
            tLATreeAccessorySchema.setModifyDate(currentDate);
            tLATreeAccessorySchema.setModifyTime(currentTime);
            tLATreeAccessorySchema.setOperator(this.mGlobalInput.Operator);
            String tAscript = this.mLATreeSchema.getAscriptSeries().trim();
            prepareTreeAccessory(tAscript, tLATreeAccessorySchema,
                                 this.mLATreeSchema.getAgentGrade());
            this.mLATreeAccessorySet.add(tLATreeAccessorySchema);
        }
        //this.mLATreeAccessorySchemaB. to do list
        //设置销售机构管理人员信息
        if (this.mIsManager.equals("true"))
        {
            String tZSvalue = "";
            if (!getInfo(tZSGroupCode))
            {
                return false;
            }
            this.mLABranchGroupSchema.setBranchManager(tNewAgent);
            this.mLABranchGroupSchema.setBranchManagerName(this.mLAAgentSchema.
                    getName());
            this.mLABranchGroupSchema.setModifyDate(currentDate);
            this.mLABranchGroupSchema.setModifyTime(currentTime);
            this.mLABranchGroupSchema.setOperator(this.mGlobalInput.Operator);
            LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
            tLABranchGroupSchema.setSchema(this.mLABranchGroupSchema);
            this.mLABranchGroupSet.add(tLABranchGroupSchema);
            if (tAgentGrade.compareTo("A05") > 0)
            {
                String tMinGroup = tZSGroupCode;
                do
                {
                    tSQL =
                            "select UpBranch from LABranchGroup where upBranchAttr = '1' and AgentGroup = '" +
                            tMinGroup + "'";
                    tExeSQL = new ExeSQL();
                    tMinGroup = tExeSQL.getOneValue(tSQL).trim();
                    if (tExeSQL.mErrors.needDealError())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tExeSQL.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "ALAgentBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = "执行SQL语句：从表中取值失败!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    tSQL =
                            "select UpBranchAttr from LABranchGroup where AgentGroup = '" +
                            tMinGroup + "'";
                    tExeSQL = new ExeSQL();
                    tZSvalue = tExeSQL.getOneValue(tSQL).trim();
                    if (tExeSQL.mErrors.needDealError())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tExeSQL.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "ALAgentBL";
                        tError.functionName = "setZSGroupManager";
                        tError.errorMessage = "执行SQL语句：从表中取值失败!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    if (tZSvalue.equals("0") && (!tMinGroup.equals(tAgentGroup)))
                    {
                        CError tError = new CError();
                        tError.moduleName = "ALAgentBL";
                        tError.functionName = "setZSGroupManager";
                        tError.errorMessage = "所输销售机构不是该代理人的直辖组!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    if (!getInfo(tMinGroup))
                    {
                        return false;
                    }
                    this.mLABranchGroupSchema.setBranchManager(tNewAgent);
                    this.mLABranchGroupSchema.setBranchManagerName(this.
                            mLAAgentSchema.getName());
                    this.mLABranchGroupSchema.setModifyDate(currentDate);
                    this.mLABranchGroupSchema.setModifyTime(currentTime);
                    this.mLABranchGroupSchema.setOperator(this.mGlobalInput.
                            Operator);
                    tLABranchGroupSchema = new LABranchGroupSchema();
                    tLABranchGroupSchema.setSchema(this.mLABranchGroupSchema);
                    this.mLABranchGroupSet.add(tLABranchGroupSchema);
                }
                while (!tMinGroup.equals(tAgentGroup));
            }
            else if (!tNewAgent.equals(""))
            {
                tSQL =
                        "select UpBranchAttr from LABranchGroup where AgentGroup = '" +
                        tZSGroupCode + "'";
                tExeSQL = new ExeSQL();
                tZSvalue = tExeSQL.getOneValue(tSQL).trim();
                if (tExeSQL.mErrors.needDealError())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tExeSQL.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ALAgentSecondBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "执行SQL语句：从表中取值失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (tZSvalue.equals("1"))
                {
                    CError tError = new CError();
                    tError.moduleName = "ALAgentSecondBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "该代理人所输职级与其机构不对应!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
        }

        //担保人信息表
        int aCount = this.mLAWarrantorSet.size();
        System.out.println(Integer.toString(aCount));
        for (int i = 1; i <= aCount; i++)
        {
            this.mLAWarrantorSet.get(i).setAgentCode(tNewAgent);
            this.mLAWarrantorSet.get(i).setSerialNo(i);
            this.mLAWarrantorSet.get(i).setOperator(mGlobalInput.Operator);
            this.mLAWarrantorSet.get(i).setMakeDate(currentDate);
            this.mLAWarrantorSet.get(i).setMakeTime(currentTime);
            this.mLAWarrantorSet.get(i).setModifyDate(currentDate);
            this.mLAWarrantorSet.get(i).setModifyTime(currentTime);
        }
        return tReturn;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mIsManager = (String) cInputData.get(1);
        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                                      getObjectByObjectName("LAAgentSchema", 0));
        this.mLATreeSchema.setSchema((LATreeSchema) cInputData.
                                     getObjectByObjectName("LATreeSchema", 0));
        this.mLAWarrantorSet.set((LAWarrantorSet) cInputData.
                                 getObjectByObjectName("LAWarrantorSet", 0));
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start LAAgentBLQuery Submit...");
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setSchema(this.mLAAgentSchema);
        this.mLAAgentSet = tLAAgentDB.query();
        this.mResult.add(this.mLAAgentSet);
        System.out.println("End LAAgentBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLAAgentDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentSecondBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLAAgentSchema);
            this.mInputData.add(this.mLAAgentBSchema);
            this.mInputData.add(this.mLATreeSchema);
            this.mInputData.add(this.mLATreeBSchema);
            this.mInputData.add(this.mLATreeAccessorySet);
            this.mInputData.add(this.mLATreeAccessoryBSet);
            this.mInputData.add(this.mLABranchGroupSet);
            this.mInputData.add(this.mLAWarrantorSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentSecondBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private boolean prepareCopyTree(String cAgentCode)
    {
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(cAgentCode);
        if (!tLATreeDB.getInfo())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentSecondBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            tReflections.transFields(this.mLATreeBSchema, tLATreeDB.getSchema());
            this.mLATreeBSchema.setAstartDate(tLATreeDB.getAstartDate());
            this.mLATreeBSchema.setEdorNO(tEdorNo);
            this.mLATreeBSchema.setMakeDate2(tLATreeDB.getMakeDate());
            this.mLATreeBSchema.setMakeTime2(tLATreeDB.getMakeTime());
            this.mLATreeBSchema.setManageCom(tLATreeDB.getManageCom());
            this.mLATreeBSchema.setModifyDate2(tLATreeDB.getModifyDate());
            this.mLATreeBSchema.setModifyTime2(tLATreeDB.getModifyTime());
            this.mLATreeBSchema.setOperator2(tLATreeDB.getOperator());
            this.mLATreeBSchema.setRemoveType("04"); //转储类型:二次增员
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentSecondBL";
            tError.functionName = "prepareCopyTree";
            tError.errorMessage = "行政信息备份出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean prepareCopyAgent(String cAgentCode)
    {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(cAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "ALAAgentBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "查询代理人原信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            tReflections.transFields(this.mLAAgentBSchema, tLAAgentDB.getSchema());
            this.mLAAgentBSchema.setAgentCode(cAgentCode);
            this.mLAAgentBSchema.setEdorNo(this.tEdorNo);
            this.mLAAgentBSchema.setEdorType("04"); //转储类型:二次增员
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentSecondBL";
            tError.functionName = "prepareCopyTree";
            tError.errorMessage = "代理人基本信息备份出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean prepareCopyTreeAccessory(String cAgentCode)
    {
        LATreeAccessorySet tLATreeAccessorySet = new LATreeAccessorySet();
        LATreeAccessoryDB tLATreeAccessoryDB = new LATreeAccessoryDB();
        tLATreeAccessoryDB.setAgentCode(cAgentCode);
        tLATreeAccessorySet = tLATreeAccessoryDB.query();
        if (tLATreeAccessoryDB.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "ALAAgentBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "查询代理人原信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LATreeAccessoryBSchema tLATreeAccessoryBSchema = null;
        for (int i = 1; i <= tLATreeAccessorySet.size(); i++)
        {
            tLATreeAccessoryBSchema = new LATreeAccessoryBSchema();
            try
            {
                tReflections.transFields(tLATreeAccessoryBSchema,
                                         tLATreeAccessorySet.get(i));
                tLATreeAccessoryBSchema.setAgentCode(cAgentCode);
                tLATreeAccessoryBSchema.setEdorNo(this.tEdorNo);
                tLATreeAccessoryBSchema.setEdorType("04"); //转储类型:二次增员
                tLATreeAccessoryBSchema.setModifyDate(currentDate);
                tLATreeAccessoryBSchema.setModifyTime(currentTime);
                tLATreeAccessoryBSchema.setMakeDate(currentDate);
                tLATreeAccessoryBSchema.setMakeTime(currentTime);
                tLATreeAccessoryBSchema.setOperator(this.mGlobalInput.Operator);
                this.mLATreeAccessoryBSet.add(tLATreeAccessoryBSchema);
            }
            catch (Exception ex)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAAgentSecondBL";
                tError.functionName = "prepareCopyTreeAccessory";
                tError.errorMessage = "行政附属信息备份出错。";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }

    private boolean getInfo(String cAgentGroup)
    {
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(cAgentGroup);
        if (!tLABranchGroupDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBL";
            tError.functionName = "getInfo";
            tError.errorMessage = "查询机构信息出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        //this.mLABranchGroupSchema=null;
        this.mLABranchGroupSchema.setSchema(tLABranchGroupDB.getSchema());
        return true;
    }

    //育成人的个数决定插入行政附属表中的记录数
    private void prepareTreeAccessory(String cAscriptSeries,
                                      LATreeAccessorySchema cTASchema,
                                      String cAgentGrade)
    {
        String tAscriptSeries = cAscriptSeries;
        if (tAscriptSeries.indexOf(":") == -1)
        {
            tAscriptSeries = "";
        }
        else
        {
            tAscriptSeries = tAscriptSeries.substring(0,
                    tAscriptSeries.lastIndexOf(":"));
            LATreeAccessorySchema tLATreeAccessorySchema2 = null;
            Reflections tReflections = new Reflections();
            String tAscriptCode = "";
            int count = 1;
            do
            {
                tLATreeAccessorySchema2 = new LATreeAccessorySchema();
                tReflections.transFields(tLATreeAccessorySchema2, cTASchema);
                tAscriptCode = tAscriptSeries.indexOf(":") != -1 ?
                               tAscriptSeries.substring(0,
                        tAscriptSeries.indexOf(":")) : tAscriptSeries;
                System.out.print("----AscriptCode:" + tAscriptCode);
                switch (count)
                {
                    case 1:
                        tLATreeAccessorySchema2.setAgentGrade("A04"); //业务经理一级
                        break;
                    case 2:
                        tLATreeAccessorySchema2.setAgentGrade("A06"); //高级业务经理一级
                        break;
                    case 3:
                        tLATreeAccessorySchema2.setAgentGrade("A08"); //督导长
                        break;
                }
                tAscriptSeries = tAscriptSeries.indexOf(":") != -1 ?
                                 tAscriptSeries.substring(tAscriptSeries.
                        indexOf(":") + 1) : "";
                if (!tAscriptCode.equals("")) //育成人为空则不插入记录
                {
                    tLATreeAccessorySchema2.setRearAgentCode(tAscriptCode);
                    this.mLATreeAccessorySet.add(tLATreeAccessorySchema2);
                }
                count++;
            }
            while (!tAscriptSeries.equals(""));
        }
    }

    //代理人校验
    private boolean checkData()
    {
        LAAgentBlacklistDB tLAAgentBlackDB = new LAAgentBlacklistDB();
        tLAAgentBlackDB.setName(this.mLAAgentSchema.getName());
        tLAAgentBlackDB.setSex(this.mLAAgentSchema.getSex());
        tLAAgentBlackDB.setIDNo(this.mLAAgentSchema.getIDNo());
        LAAgentBlacklistSet tLAAgentBlackSet = tLAAgentBlackDB.query();
        if (tLAAgentBlackDB.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "checkData";
            tError.errorMessage = "查询黑名单表出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLAAgentBlackSet.size() > 0)
        {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "checkData()";
            tError.errorMessage = "该代理人已列入黑名单中，无法增员！";
            this.mErrors.addOneError(tError);
            return false;
        }
        LAAgentDB tLAAgentDB = new LAAgentDB();
        String sql = "select * from laagent where idno='" +
                     mLAAgentSchema.getIDNo() + "' and agentstate<'03' ";
        LAAgentSet tLAAgentSet = new LAAgentSet();
        tLAAgentSet = tLAAgentDB.executeQuery(sql);
        if (tLAAgentSet.size() > 0)
        {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "checkData()";
            tError.errorMessage = "该代理人已经在职,无法二次增员!";
            this.mErrors.addOneError(tError);
            return false;

        }
        return true;
    }

}
