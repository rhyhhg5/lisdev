package com.sinosoft.lis.agent;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

import com.sinosoft.lis.db.LAManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class LAManagerBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData  mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAManagerSchema mLAManagerSchema = new LAManagerSchema();
    private MMap map = new MMap();
    private String tManagerCode="";
    public LAManagerBL()
    {
    }

    public static void main(String[] args)
    {
    	String testNo = PubFun1.CreateMaxNo("ManagerNO", 10);
    	System.out.println("生成的最大号是"+testNo);
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAManagerBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAManagerBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("over dealData");
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        System.out.println("Start LAManagerBL Submit...");
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAManagerBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("End LATrainCourseBL Submit...");

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        tManagerCode = this.mLAManagerSchema.getManagerCode();
        if(tManagerCode.equals("")||tManagerCode==null){
            String tPrefix = "";
            String mManagecom=mLAManagerSchema.getCManageCom();
            if (mManagecom.length() < 4)

            {
                CError tError = new CError();
                               tError.moduleName = "LACreateCodePICCHBL";
                               tError.functionName = "dealdata";
                               tError.errorMessage = "管理机构长度不足！";
                                this.mErrors.addOneError(tError);
                                return false;


            }
            tPrefix = mManagecom.substring(2, 4);
            tPrefix=tPrefix+"22";
            String tManagerCode1 = PubFun1.CreateMaxNo("ManagerCode" + tPrefix, 6);
            if (tManagerCode1 == null || tManagerCode1.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "LAManagerBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "生成营业部经理工号错误！";
                this.mErrors.addOneError(tError);
                return false;
            }
            tManagerCode=tPrefix+tManagerCode1;
            System.out.println("营业部经理工号：" + tManagerCode);
            this.mLAManagerSchema.setManagerCode(tManagerCode);

        }
        
        if (this.mOperate.equals("INSERT||MAIN"))
        {
        	
        	String agentGroup = "";
        	String tSql = "select  agentgroup from labranchgroup where branchattr = '"+mLAManagerSchema.getAgentGroup()+"' and "
        	    + " branchtype = '1' and branchtype2 = '01'";
        	ExeSQL tExeSQL = new ExeSQL();
        	agentGroup=tExeSQL.getOneValue(tSql);
            this.mLAManagerSchema.setManagerCode(tManagerCode);
            this.mLAManagerSchema.setAgentGroup(agentGroup);
            this.mLAManagerSchema.setOperator(this.mGlobalInput.Operator);
            this.mLAManagerSchema.setMakeDate(currentDate);
            this.mLAManagerSchema.setMakeTime(currentTime);
            this.mLAManagerSchema.setModifyDate(currentDate);
            this.mLAManagerSchema.setModifyTime(currentTime);
            this.map.put(mLAManagerSchema, "INSERT");
            
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            tManagerCode = this.mLAManagerSchema.getManagerCode();
            LAManagerDB tLAManagerDB = new LAManagerDB();
            tLAManagerDB.setManagerCode(tManagerCode);
            if (!tLAManagerDB.getInfo())
            {
                 // @@错误处理
                this.mErrors.copyAllErrors(tLAManagerDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAManagerBL";
                tError.functionName = "submitData";
                tError.errorMessage = "没有查询到，相关人员信息，数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        	String agentGroup = "";
        	String tSql = "select  agentgroup from labranchgroup where branchattr = '"+mLAManagerSchema.getAgentGroup()+"' and "
        	    + " branchtype = '1' and branchtype2 = '01'";
        	ExeSQL tExeSQL = new ExeSQL();
        	agentGroup=tExeSQL.getOneValue(tSql);
            this.mLAManagerSchema.setAgentGroup(agentGroup);
            this.mLAManagerSchema.setMakeDate(tLAManagerDB.getMakeDate());
            this.mLAManagerSchema.setMakeTime(tLAManagerDB.getMakeTime());
            this.mLAManagerSchema.setModifyTime(currentTime);
            this.mLAManagerSchema.setModifyDate(currentDate);
            this.mLAManagerSchema.setOperator(mGlobalInput.Operator);
            this.map.put(mLAManagerSchema, "UPDATE");
        }
       
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mLAManagerSchema.setSchema((LAManagerSchema)
                                               cInputData.getObjectByObjectName(
                "LAManagerSchema", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */

    private boolean prepareOutputData()
    {
        try
        {
            System.out.println("Begin LAManagerBL.prepareOutputData.........");
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAManagerBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public String getResult()
    {
        return this.tManagerCode;
    }
}
