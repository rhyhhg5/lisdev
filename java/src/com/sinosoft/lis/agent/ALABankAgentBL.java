/*
 * <p>ClassName: ALABankAgentBL </p>
 * <p>Description: ALABankAgentBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-09
 */
package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentBlacklistDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDCodeRelaDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.schema.LDCodeRelaSchema;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.vschema.LAAgentBlacklistSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LDCodeRelaSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ALABankAgentBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;
    private String mIsManager;
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();

    /** 业务处理相关变量 */
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAAgentBSchema mLAAgentBSchema = new LAAgentBSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private LATreeBSchema mLATreeBSchema = new LATreeBSchema();
    private LABranchGroupSchema mLABranchGroupSchema = new LABranchGroupSchema();
    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    public ALABankAgentBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //校验该代理人是否在黑名单中存在
        if (!checkData())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALABankAgentBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALABankAgentBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("over dealData");
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start ALABankAgentBL Submit...");
            ALABankAgentBLS tALABankAgentBLS = new ALABankAgentBLS();
            tALABankAgentBLS.submitData(mInputData, cOperate);
            System.out.println("End ALABankAgentBL Submit...");
            //如果有需要处理的错误，则返回
            if (tALABankAgentBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tALABankAgentBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALABankAgentBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mResult.add(this.mLAAgentSchema);
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String tAgentCode = "";
        String tSQL = "";
        ExeSQL tExeSQL;
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            //生成代理人编码(分公司＋顺序号)
            String tPrefix = this.mLAAgentSchema.getManageCom().substring(2, 4);
            String tBranchType = mLAAgentSchema.getBranchType();
            if (tBranchType.equals("1"))
                tPrefix = tPrefix + "01";
            if (tBranchType.equals("2"))
                tPrefix = tPrefix + "02";

            //自动生成代理人编码，四位机构＋四位流水
            tAgentCode = tPrefix + PubFun1.CreateMaxNo("AgentCode" + tPrefix, 6);
            //确定代理人系列
            String tAgentGradeSeries = this.mLATreeSchema.getAgentGrade().trim();

            tAgentGradeSeries = getAgentSeries(tAgentGradeSeries);
            if (tAgentGradeSeries == null)
            {
                return false;
            }
            //代理人基本信息
            this.mLAAgentSchema.setAgentCode(tAgentCode);
            this.mLAAgentSchema.setInDueFormDate(this.mLAAgentSchema.
                                                 getEmployDate());
            this.mLAAgentSchema.setOperator(mGlobalInput.Operator);
            this.mLAAgentSchema.setMakeDate(currentDate);
            this.mLAAgentSchema.setMakeTime(currentTime);
            this.mLAAgentSchema.setModifyDate(currentDate);
            this.mLAAgentSchema.setModifyTime(currentTime);

            String SQL =
                    "select gradeproperty7 from laagentgrade where gradecode='" +
                    mLATreeSchema.getAgentGrade() + "'";
            ExeSQL aExeSQL = new ExeSQL();
            System.out.println(SQL);
            mLAAgentSchema.setInsideFlag(aExeSQL.getOneValue(SQL));
            //行政信息

            this.mLATreeSchema.setAgentCode(tAgentCode);
            this.mLATreeSchema.setAgentSeries(tAgentGradeSeries);
            this.mLATreeSchema.setAssessType("0"); //正常
            this.mLATreeSchema.setState("0");
            this.mLATreeSchema.setStartDate(mLAAgentSchema.getEmployDate());
            this.mLATreeSchema.setAstartDate(mLAAgentSchema.getEmployDate());

            this.mLATreeSchema.setMakeDate(currentDate);
            this.mLATreeSchema.setMakeTime(currentTime);
            this.mLATreeSchema.setModifyDate(currentDate);
            this.mLATreeSchema.setModifyTime(currentTime);
            this.mLATreeSchema.setOperator(mGlobalInput.Operator);
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            tAgentCode = this.mLAAgentSchema.getAgentCode().trim();
            //确定代理人系列
            String tAgentGradeSeries = this.mLATreeSchema.getAgentGrade().trim();
            tAgentGradeSeries = getAgentSeries(tAgentGradeSeries);
            if (tAgentGradeSeries == null)
            {
                return false;
            }
            //代理人基本信息
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tAgentCode);
            if (!tLAAgentDB.getInfo())
            {
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALABankAgentBL";
                tError.functionName = "submitData";
                tError.errorMessage = "查询代理人信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LATreeDB ttLATreeDB = new LATreeDB();
            ttLATreeDB.setAgentCode(tAgentCode);
            if (!ttLATreeDB.getInfo())
            {
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALABankAgentBL";
                tError.functionName = "submitData";
                tError.errorMessage = "查询代理人信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

            //备份原代理人信息
            Reflections tReflections = new Reflections();
            tReflections.transFields(this.mLAAgentBSchema, tLAAgentDB.getSchema());
            tReflections.transFields(this.mLATreeBSchema, ttLATreeDB.getSchema());
            this.mLAAgentBSchema.setEdorNo(tEdorNo);
            this.mLAAgentBSchema.setEdorType("05");
            this.mLAAgentBSchema.setMakeDate(currentDate);
            this.mLAAgentBSchema.setMakeTime(currentTime);
            this.mLAAgentBSchema.setModifyDate(currentDate);
            this.mLAAgentBSchema.setModifyTime(currentTime);
            this.mLAAgentBSchema.setOperator(this.mGlobalInput.Operator);
            //修改原因：银代人员信息修改界面要求不能修改岗位名称、组别、渠道名称
            this.mLAAgentSchema.setAgentKind(tLAAgentDB.getAgentKind());
            this.mLAAgentSchema.setAgentGroup(tLAAgentDB.getAgentGroup());
            this.mLAAgentSchema.setChannelName(tLAAgentDB.getChannelName());

            this.mLAAgentSchema.setInDueFormDate(this.mLAAgentSchema.
                                                 getEmployDate());
            this.mLAAgentSchema.setMakeDate(tLAAgentDB.getMakeDate());
            this.mLAAgentSchema.setMakeTime(tLAAgentDB.getMakeTime());
            this.mLAAgentSchema.setModifyDate(currentDate);
            this.mLAAgentSchema.setModifyTime(currentTime);
            this.mLAAgentSchema.setOperator(mGlobalInput.Operator);

//修改原因：银代人员信息修改界面要求不能修改职级信息
//      if(!ttLATreeDB.getAgentGrade().substring(0,1).equals(mLATreeSchema.getAgentGrade().substring(0,1)))
//      {
//        String SQL="select gradeproperty7 from laagentgrade where gradecode='"+mLATreeSchema.getAgentGrade()+"'";
//        ExeSQL aExeSQL = new ExeSQL();
//        mLAAgentSchema.setInsideFlag(aExeSQL.getOneValue(SQL));
//      }
            String ttEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            this.mLATreeBSchema.setEdorNO(ttEdorNo);
            this.mLATreeBSchema.setMakeDate(currentDate);
            this.mLATreeBSchema.setMakeTime(currentTime);
            this.mLATreeBSchema.setModifyDate(currentDate);
            this.mLATreeBSchema.setModifyTime(currentTime);
            this.mLATreeBSchema.setOperator(this.mGlobalInput.Operator);
            this.mLATreeBSchema.setRemoveType("02");
            this.mLATreeBSchema.setMakeDate2(ttLATreeDB.getMakeDate());
            this.mLATreeBSchema.setMakeTime2(ttLATreeDB.getMakeTime());
            this.mLATreeBSchema.setModifyDate2(ttLATreeDB.getMakeDate());
            this.mLATreeBSchema.setModifyTime2(ttLATreeDB.getMakeTime());
            this.mLATreeBSchema.setOperator2(ttLATreeDB.getOperator());

            //行政信息
            LATreeDB tLATreeDB = new LATreeDB();
            tLATreeDB.setAgentCode(tAgentCode);
            if (!tLATreeDB.getInfo())
            {
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALABankAgentBL";
                tError.functionName = "submitData";
                tError.errorMessage = "查询代理人行政信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //设置转正日期（当其职级由高于B01,D01职级调为B01,D01时要置空）
            if ((tLATreeDB.getAgentGrade().substring(1).compareTo("01") > 0)
                && this.mLATreeSchema.getAgentGrade().substring(1).equals("01"))
            {
                this.mLAAgentSchema.setInDueFormDate("");
            }

//修改原因：银代人员信息修改界面要求不能修改职级、组别信息
            this.mLATreeSchema.setAgentGrade(tLATreeDB.getAgentGrade());
            this.mLATreeSchema.setAgentKind(tLATreeDB.getAgentKind());
            this.mLATreeSchema.setAgentGroup(tLATreeDB.getAgentGroup());

            this.mLATreeSchema.setAgentSeries(tAgentGradeSeries);
            this.mLATreeSchema.setAssessType("0"); //正常
            this.mLATreeSchema.setState("0");
//      this.mLATreeSchema.setStartDate(mLAAgentSchema.getEmployDate());
            this.mLATreeSchema.setAstartDate(currentDate);
            this.mLATreeSchema.setMakeDate(tLATreeDB.getMakeDate());
            this.mLATreeSchema.setMakeTime(tLATreeDB.getMakeTime());
            this.mLATreeSchema.setModifyDate(currentDate);
            this.mLATreeSchema.setModifyTime(currentTime);
            this.mLATreeSchema.setOperator(mGlobalInput.Operator);

            //取得修改前设置的LABranchGroup表中的BranchManager
            LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setBranchManager(this.mLAAgentSchema.getAgentCode());
            tLABranchGroupSet = tLABranchGroupDB.query();
            for (int i = 1; i <= tLABranchGroupSet.size(); i++)
            {
                tLABranchGroupSet.get(i).setBranchManager("");
                tLABranchGroupSet.get(i).setBranchManagerName("");
            }
            this.mLABranchGroupSet.add(tLABranchGroupSet);
        }
        if (this.mOperate.equals("INSERT||MAIN") ||
            this.mOperate.equals("UPDATE||MAIN"))
        {
            //设置销售机构管理人员信息
            System.out.println("come in");
            if (this.mIsManager.equals("true"))
            {
                System.out.println("come in true;");
                String tAgentGroup = this.mLAAgentSchema.getAgentGroup();
                LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                tLABranchGroupDB.setAgentGroup(tAgentGroup);
                if (!tLABranchGroupDB.getInfo())
                {
                    this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ALABankAgentBL";
                    tError.functionName = "submitData";
                    tError.errorMessage = "查询代理人所属机构信息失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                this.mLABranchGroupSchema.setSchema(tLABranchGroupDB.getSchema());
                this.mLABranchGroupSchema.setBranchManager(tAgentCode);
                this.mLABranchGroupSchema.setBranchManagerName(this.
                        mLAAgentSchema.
                        getName());
                this.mLABranchGroupSchema.setModifyDate(currentDate);
                this.mLABranchGroupSchema.setModifyTime(currentTime);
                this.mLABranchGroupSchema.setOperator(this.mGlobalInput.
                        Operator);
                this.mLABranchGroupSet.add(this.mLABranchGroupSchema);
            }
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setBranchManager(this.mLAAgentSchema.getAgentCode());
            this.mLABranchGroupSet = tLABranchGroupDB.query();
            for (int i = 1; i <= mLABranchGroupSet.size(); i++)
            {
                this.mLABranchGroupSet.get(i).setBranchManager("");
                this.mLABranchGroupSet.get(i).setBranchManagerName("");
            }
        }
        mLAAgentSchema.setBranchCode(mLAAgentSchema.getAgentGroup());
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        mIsManager = (String) cInputData.get(1);
        System.out.println(mIsManager);
        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                                      getObjectByObjectName("LAAgentSchema", 0));
        this.mLATreeSchema.setSchema((LATreeSchema) cInputData.
                                     getObjectByObjectName("LATreeSchema", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start ALABankAgentBLQuery Submit...");
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setSchema(this.mLAAgentSchema);
        this.mLAAgentSet = tLAAgentDB.query();
        this.mResult.add(this.mLAAgentSet);
        System.out.println("End ALABankAgentBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLAAgentDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALABankAgentBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLAAgentSchema);
            this.mInputData.add(this.mLAAgentBSchema);
            this.mInputData.add(this.mLATreeSchema);
            this.mInputData.add(this.mLABranchGroupSet);
            this.mInputData.add(this.mLATreeBSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALABankAgentBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    //根据代理人职级确定代理人系列
    private String getAgentSeries(String cAgentGrade)
    {
        String tAgentSeries = "";
        String tSQL =
                "select code2 from ldcodeRela where relaType = 'gradeserieslevel' "
                + "and code1 = '" + cAgentGrade + "'";
        ExeSQL tExeSQL = new ExeSQL();
        tAgentSeries = tExeSQL.getOneValue(tSQL).trim();
        if (tExeSQL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "dealData";
            tError.errorMessage = "执行SQL语句：从表中取值失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tAgentSeries;
    }

    //代理人校验
    private boolean checkData()
    {
        LAAgentBlacklistDB tLAAgentBlackDB = new LAAgentBlacklistDB();
        tLAAgentBlackDB.setName(this.mLAAgentSchema.getName());
        tLAAgentBlackDB.setSex(this.mLAAgentSchema.getSex());
        tLAAgentBlackDB.setIDNo(this.mLAAgentSchema.getIDNo());
        LAAgentBlacklistSet tLAAgentBlackSet = tLAAgentBlackDB.query();
        if (tLAAgentBlackDB.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "checkData";
            tError.errorMessage = "查询黑名单表出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLAAgentBlackSet.size() > 0)
        {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "checkData()";
            tError.errorMessage = "该代理人已列入黑名单中，无法增员！";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println(mLATreeSchema.getBranchType() +
                           "  ********************");
        if (mLATreeSchema.getBranchType().compareTo("2") == 0 &&
            this.mOperate.equals("INSERT||MAIN"))
        {
            //校验统计团队客户经理人数是否超出范围
            PubCheckField checkField1 = new PubCheckField();
            VData cInputData = new VData();
            TransferData tTransferData = new TransferData();
            //设置计算时要用到的参数值
            String tManageCom = mLAAgentSchema.getManageCom();
            String tAgentGroup = mLAAgentSchema.getAgentGroup();
            String tBranchType = mLATreeSchema.getBranchType();
            tTransferData = new TransferData();
            tTransferData.setNameAndValue("BranchType", tBranchType);
            tTransferData.setNameAndValue("AgentGroup", tAgentGroup);
            tTransferData.setNameAndValue("ManageCom", tManageCom);
            LMCheckFieldDB tLMCheckFieldDB = new LMCheckFieldDB();
            LMCheckFieldSet tLMCheckFieldSet = new LMCheckFieldSet();
            String tSql =
                    "select * from lmcheckfield where riskcode = '000000'"
                    +
                    "  and fieldname = 'ALABankAgentBL' order by serialno asc";
            System.out.println(tSql);
            tLMCheckFieldSet = tLMCheckFieldDB.executeQuery(tSql);
            cInputData.add(tTransferData);
            cInputData.add(tLMCheckFieldSet);
            if (!checkField1.submitData(cInputData, "CKBYSET"))
            {
                System.out.println("Enter Error Field!");
                //此判断是用来区分是程序处理过程中报的错误，还是校验时报的错误
                if (checkField1.mErrors.needDealError())
                {
                    this.mErrors.copyAllErrors(checkField1.mErrors);
                    return false;
                }
                else
                {
                    VData t = checkField1.getResultMess();
                    CError tError = new CError();
                    tError.moduleName = "ALABranchGroupBL";
                    tError.functionName = "dealData()";
                    tError.errorMessage = t.get(0).toString();
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }

            //对代理人所担任的职务校验
            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("agentkind");
            tLDCodeDB.setCodeAlias("2");
            tLDCodeDB.setCode(this.mLATreeSchema.getAgentKind());
            LDCodeSet tLDCodeSet = tLDCodeDB.query();
            if (tLDCodeSet.size() > 0)
            {
                LDCodeSchema tLDCodeSchema = tLDCodeSet.get(1);
                //判定当前职务是否为主管职务，如果不是校验结束
                if (tLDCodeSchema.getComCode().compareTo("Y") == 0)
                {
                    //校验该职级客户经理是否有资格担当团队主管
                    LDCodeRelaDB tLDCodeRelaDB = new LDCodeRelaDB();
                    tLDCodeRelaDB.setRelaType("agentgradetokind");
                    tLDCodeRelaDB.setOtherSign("2");
                    tLDCodeRelaDB.setCode1(this.mLATreeSchema.getAgentGrade());
                    LDCodeRelaSet tLDCodeRelaSet = tLDCodeRelaDB.query(); //查询LDCodeRela
                    if (tLDCodeRelaSet.size() > 0)
                    {
                        LDCodeRelaSchema tLDCodeRelaSchema = tLDCodeRelaSet.get(
                                1);
                        //表示该客户担当团队主管职务
                        if (tLDCodeRelaSchema.getCode2().compareTo(this.
                                mLATreeSchema.
                                getAgentKind()) >= 0)
                        {
                            //System.out.println("mIsManager" + this.mIsManager);

                            //查询职务和机构级别是否对应，及主管是否存在
                            LABranchGroupDB tLABranchGroupDB = new
                                    LABranchGroupDB();
                            tLABranchGroupDB.setAgentGroup(this.mLAAgentSchema.
                                    getAgentGroup());
                            LABranchGroupSet tLABranchGroupSet =
                                    tLABranchGroupDB.query();
                            //判定查询结果是否存在
                            if (tLABranchGroupSet.size() > 0)
                            {
                                LABranchGroupSchema tLABranchGroupSchema =
                                        tLABranchGroupSet.get(
                                                1);
                                //判定该机构是否存在主管
                                if (tLABranchGroupSchema.getBranchManager() != null)
                                {
                                    //                if (tLABranchGroupSchema.getBranchManager().compareTo(this.
                                    //                    mLAAgentSchema.
                                    //                    getAgentCode()) != 0) {
                                    //表示机构已经存在主管！
                                    CError tError = new CError();
                                    tError.moduleName = "ALAgentBL";
                                    tError.functionName = "checkData()";
                                    tError.errorMessage = "该机构已经存在主管！";
                                    this.mErrors.addOneError(tError);
                                    return false;
                                    //                }
                                }

                                else
                                {
                                    //查询职务和机构对应关系
                                    tLDCodeRelaDB = new LDCodeRelaDB();
                                    tLDCodeRelaDB.setRelaType(
                                            "branchleveltokind");
                                    tLDCodeRelaDB.setOtherSign("2");
                                    tLDCodeRelaDB.setCode2(this.mLATreeSchema.
                                            getAgentKind());
                                    tLDCodeRelaSet = tLDCodeRelaDB.query(); //查询LDCodeRela
                                    //判定查询结果
                                    if (tLDCodeRelaSet.size() > 0)
                                    {
                                        tLDCodeRelaSchema = tLDCodeRelaSet.get(
                                                1);
                                        //匹配职务和机构级别关系
                                        if (tLDCodeRelaSchema.getCode1().
                                            compareTo(
                                                tLABranchGroupSchema.
                                                getBranchLevel()) != 0)
                                        {
                                            //表示代理人职务和机构不匹配！
                                            CError tError = new CError();
                                            tError.moduleName = "ALAgentBL";
                                            tError.functionName = "checkData()";
                                            tError.errorMessage =
                                                    "该代理人职务和机构不匹配！";
                                            this.mErrors.addOneError(tError);
                                            return false;
                                        }
                                        else
                                        {
                                            //表示代理人职务和机构匹配，校验结束
                                            this.mIsManager = "true"; //表示执行团队主管任命
                                        }
                                    }
                                }
                            }
                            else
                            {
                                CError tError = new CError();
                                tError.moduleName = "ALAgentBL";
                                tError.functionName = "checkData()";
                                tError.errorMessage = "没有该机构信息！";
                                this.mErrors.addOneError(tError);
                                return false;
                            }
                        }
                        else
                        {
                            CError tError = new CError();
                            tError.moduleName = "ALAgentBL";
                            tError.functionName = "checkData()";
                            tError.errorMessage = "该代理人职级不能出任此团队主管一职！";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                    }
                    else
                    {
                        //如果查询失败，表示职务描述有问题！
                        CError tError = new CError();
                        tError.moduleName = "ALAgentBL";
                        tError.functionName = "checkData()";
                        tError.errorMessage = "该代理人职务" +
                                              this.mLATreeSchema.getAgentKind() +
                                              "描述出错！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
            }
            else
            {
                //如果查询失败，表示职务录入错误！
                CError tError = new CError();
                tError.moduleName = "ALAgentBL";
                tError.functionName = "checkData()";
                tError.errorMessage = "该代理人职务出错！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }
}
