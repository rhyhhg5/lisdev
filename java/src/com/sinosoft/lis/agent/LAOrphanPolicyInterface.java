package com.sinosoft.lis.agent;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAWarrantorSet;
import com.sinosoft.utility.CErrors;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public interface LAOrphanPolicyInterface {

    public CErrors mErrors = new CErrors();

   public void setAgentCode(String mAgentCode); //设置

   public boolean dealdata(); //处理函数

   public MMap getResult(); //获得处理结果

   public void setGlobalInput(GlobalInput cGlobalInput); //设置全局变量


}
