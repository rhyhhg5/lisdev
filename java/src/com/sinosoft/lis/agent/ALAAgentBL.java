/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentBlacklistDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeAccessoryDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeAccessorySchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAgentBlacklistSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.lis.vschema.LAWarrantorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;


/*
 * <p>ClassName: ALAAgentBL </p>
 * <p>Description: ALAAgentBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-09
 */
public class ALAAgentBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;
    private String mIsManager;
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();

    /** 业务处理相关变量 */
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAAgentBSchema mLAAgentBSchema = new LAAgentBSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private LATreeBSchema mLATreeBSchema = new LATreeBSchema();
    private LATreeAccessorySet mLATreeAccessorySet = new LATreeAccessorySet();
    private LABranchGroupSchema mLABranchGroupSchema = new LABranchGroupSchema();
    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();
    private LAWarrantorSet mLAWarrantorSet = new LAWarrantorSet();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    public ALAAgentBL()
    {
    }

    public static void main(String[] args)
    {
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //校验该代理人是否在黑名单中存在
        if (!checkData())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALAAgentBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("over dealData");
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start ALAAgentBL Submit...");
            ALAAgentBLS tALAAgentBLS = new ALAAgentBLS();
            tALAAgentBLS.submitData(mInputData, cOperate);
            System.out.println("End ALAAgentBL Submit...");
            //如果有需要处理的错误，则返回
            if (tALAAgentBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tALAAgentBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAAgentBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mResult.add(this.mLAAgentSchema);
        }
        mInputData = null;
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String tAgentCode = "";
        String tAgentGroup = "";
        String tZSGroupCode = "";
        String tSQL = "";
        ExeSQL tExeSQL;
        System.out.println(mOperate);

        if (this.mOperate.equals("INSERT||MAIN"))
        {
            //生成代理人编码(分公司＋顺序号4位流水)
            String tPrefix = this.mLAAgentSchema.getManageCom().substring(2, 4);
            String tBranchType=mLAAgentSchema.getBranchType() ;
            if (tBranchType.equals("1") )
                tPrefix=tPrefix+"01";
            if (tBranchType.equals("2") )
                tPrefix=tPrefix+"02";
            tAgentCode = tPrefix + PubFun1.CreateMaxNo("AgentCode" + tPrefix, 4);
            System.out.println("代理人编码：" + tAgentCode);
            //确定代理人系列
            String tAgentGradeSeries = this.mLATreeSchema.getAgentGrade().trim();
            tAgentGradeSeries = getAgentSeries(tAgentGradeSeries);
            if (tAgentGradeSeries == null)
            {
                return false;
            }
            //确定销售机构AgentGroup
            tZSGroupCode = this.mLAAgentSchema.getBranchCode().trim(); //直辖组的隐式代码
            tAgentGroup = this.mLAAgentSchema.getAgentGroup().trim(); //显示代码
            String tAgentGrade = this.mLATreeSchema.getAgentGrade().trim();
            System.out.println(tAgentGrade + " " + tAgentGroup);
            tAgentGroup = getAgentGradevsGroup(tZSGroupCode, tAgentGrade,
                                               tAgentGroup);
            if (tAgentGroup == null)
            {
                return false;
            }
            //代理人基本信息
            this.mLAAgentSchema.setAgentCode(tAgentCode);
            this.mLAAgentSchema.setAgentGroup(tAgentGroup);
            this.mLAAgentSchema.setAgentKind("01");
            this.mLAAgentSchema.setEmployDate(currentDate); //入司日期取系统日期
            if (tAgentGrade.compareTo("A01") > 0)
            {
                this.mLAAgentSchema.setInDueFormDate(mLAAgentSchema.
                        getEmployDate());
            }
            this.mLAAgentSchema.setOperator(mGlobalInput.Operator);
            this.mLAAgentSchema.setMakeDate(currentDate);
            this.mLAAgentSchema.setMakeTime(currentTime);
            this.mLAAgentSchema.setModifyDate(currentDate);
            this.mLAAgentSchema.setModifyTime(currentTime);
            //行政信息
            this.mLATreeSchema.setAgentCode(tAgentCode);
            this.mLATreeSchema.setAgentGroup(tAgentGroup);
            String upAgent = this.getUpAgent(tAgentGroup, tAgentGradeSeries);
//      if (upAgent == null||upAgent.trim() .equals("") ) {
//        CError tError = new CError();
//        tError.moduleName = "ALAAgentBL";
//        tError.functionName = "submitDat";
//        tError.errorMessage = "查询"+tAgentCode+"的上级代理人信息失败，请先维护上级代理人信息!";
//        this.mErrors.addOneError(tError);
//        return false;
//
//      }
            this.mLATreeSchema.setUpAgent(upAgent);
            this.mLATreeSchema.setAgentSeries(tAgentGradeSeries);
            this.mLATreeSchema.setAssessType("0"); //正常
            this.mLATreeSchema.setState("0");
            if ((this.mLATreeSchema.getIntroAgency() != null) &&
                (!this.mLATreeSchema.getIntroAgency().equals("")))
            {
                this.mLATreeSchema.setIntroBreakFlag("0");
                this.mLATreeSchema.setIntroCommStart(this.mLAAgentSchema.
                        getEmployDate());
            }
            this.mLATreeSchema.setStartDate(this.mLAAgentSchema.getEmployDate());
            this.mLATreeSchema.setAstartDate(this.mLAAgentSchema.getEmployDate());
            this.mLATreeSchema.setMakeDate(currentDate);
            this.mLATreeSchema.setMakeTime(currentTime);
            this.mLATreeSchema.setModifyDate(currentDate);
            this.mLATreeSchema.setModifyTime(currentTime);
            this.mLATreeSchema.setOperator(mGlobalInput.Operator);
            //行政附属信息
            if (tAgentGrade.compareTo("A04") >= 0)
            {
                LATreeAccessorySchema tLATreeAccessorySchema = new
                        LATreeAccessorySchema();
                tLATreeAccessorySchema.setAgentCode(tAgentCode);
                tLATreeAccessorySchema.setAgentGrade(this.mLATreeSchema.
                        getAgentGrade());
                tLATreeAccessorySchema.setAgentGroup(tAgentGroup);
                /*lis5.3 update
                 tLATreeAccessorySchema.setManageCom(this.mLATreeSchema.getManageCom());
                 */
                if ((this.mLATreeSchema.getEduManager() != null) &&
                    (!this.mLATreeSchema.getEduManager().equals("")))
                {
                    //tLATreeAccessorySchema.setRearAgentCode(tAgentGrade.equals("A09")?"":this.mLATreeSchema.getEduManager());
                    tLATreeAccessorySchema.setRearAgentCode(this.mLATreeSchema.
                            getEduManager());
                }
                tLATreeAccessorySchema.setCommBreakFlag("0");
                tLATreeAccessorySchema.setIntroBreakFlag("0");
                tLATreeAccessorySchema.setRearFlag("0");
                tLATreeAccessorySchema.setBackCalFlag("0"); //不回算
                tLATreeAccessorySchema.setstartdate(this.mLAAgentSchema.
                        getEmployDate());
//           tLATreeAccessorySchema.setstartdate(currentDate);
                tLATreeAccessorySchema.setOperator(mGlobalInput.Operator);
                tLATreeAccessorySchema.setMakeDate(currentDate);
                tLATreeAccessorySchema.setMakeTime(currentTime);
                tLATreeAccessorySchema.setModifyDate(currentDate);
                tLATreeAccessorySchema.setModifyTime(currentTime);
                String tAscript = this.mLATreeSchema.getAscriptSeries().trim();
                prepareTreeAccessory(tAscript, tLATreeAccessorySchema,
                                     tAgentGrade);
                this.mLATreeAccessorySet.add(tLATreeAccessorySchema);
            }
            //设置销售机构管理人员信息
            if (this.mIsManager.equals("true"))
            {
                if (!setZSGroupManager(tZSGroupCode, tAgentGroup, tAgentCode,
                                       this.mLAAgentSchema.getName(),
                                       tAgentGrade))
                {
                    return false;
                }

            }

        }
        if (this.mOperate.equals("UPDATE||ALL"))
        {
            String tOldAgentGroup = "", tOldAgentGrade = "";
            String tNewAgentGrade = "", tOldMinGroup = "";
            tAgentCode = this.mLAAgentSchema.getAgentCode().trim();
            String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            //查询出AgentGroup和AgentGrade，判断是否修改
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tAgentCode);
            if (!tLAAgentDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAAgentBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "查询原代理人信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //备份原代理人信息
            Reflections tReflections = new Reflections();
            tReflections.transFields(this.mLAAgentBSchema, tLAAgentDB.getSchema());
            this.mLAAgentBSchema.setEdorNo(tEdorNo);
            this.mLAAgentBSchema.setEdorType("05");
            this.mLAAgentBSchema.setMakeDate(currentDate);
            this.mLAAgentBSchema.setMakeTime(currentTime);
            this.mLAAgentBSchema.setModifyDate(currentDate);
            this.mLAAgentBSchema.setModifyTime(currentTime);
            this.mLAAgentBSchema.setOperator(this.mGlobalInput.Operator);

            tOldMinGroup = tLAAgentDB.getBranchCode(); //组隐式代码
            tOldAgentGroup = tLAAgentDB.getAgentGroup(); //老的职级对应的机构
            LATreeDB tLATreeDB = new LATreeDB();
            tLATreeDB.setAgentCode(tAgentCode);
            if (!tLATreeDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAAgentBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询原行政信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //备份行政信息
            tReflections.transFields(this.mLATreeBSchema, tLATreeDB.getSchema());
            this.mLATreeBSchema.setAstartDate(tLATreeDB.getAstartDate());
            this.mLATreeBSchema.setEdorNO(tEdorNo);
            this.mLATreeBSchema.setRemoveType("05");
            this.mLATreeBSchema.setMakeDate2(this.mLATreeBSchema.getMakeDate());
            this.mLATreeBSchema.setMakeTime2(this.mLATreeBSchema.getMakeTime());
            this.mLATreeBSchema.setModifyDate2(this.mLATreeBSchema.
                                               getModifyDate());
            this.mLATreeBSchema.setModifyTime2(this.mLATreeBSchema.
                                               getModifyTime());
            this.mLATreeBSchema.setOperator2(this.mLATreeBSchema.getOperator());
            this.mLATreeBSchema.setMakeDate(currentDate);
            this.mLATreeBSchema.setMakeTime(currentTime);
            this.mLATreeBSchema.setModifyDate(currentDate);
            this.mLATreeBSchema.setModifyTime(currentTime);
            this.mLATreeBSchema.setOperator(this.mGlobalInput.Operator);

            tOldAgentGrade = tLATreeDB.getAgentGrade();
            tZSGroupCode = this.mLAAgentSchema.getBranchCode().trim(); //直辖组的隐式代码
            tNewAgentGrade = this.mLATreeSchema.getAgentGrade();
            tAgentGroup = this.mLAAgentSchema.getAgentGroup(); //显式代码
            if (tNewAgentGrade.compareTo("A06") < 0)
            { //高级经理以下
                tAgentGroup = tZSGroupCode;
            }
            else
            {
                tAgentGroup = getAgentGradevsGroup(tZSGroupCode, tNewAgentGrade,
                        tAgentGroup);
                if (tAgentGroup == null)
                {
                    return false;
                }
            }
            //确定代理人系列
            String tAgentSeries = "";
            if (tNewAgentGrade.equals(tOldAgentGrade))
            {
                tAgentSeries = tLATreeDB.getAgentSeries();
            }
            else
            {
                tAgentSeries = getAgentSeries(tNewAgentGrade);
                if (tAgentSeries == null)
                {
                    return false;
                }
            }
            //准备销售机构更新的纪录
            //--将以前设置的机构经理置空
            if (tOldAgentGrade.compareTo("A03") > 0)
            {
                if (!setZSGroupManager(tOldMinGroup, tOldAgentGroup, "", "",
                                       tOldAgentGrade))
                {
                    return false;
                }
//        if (!clearOldGroupManager(tAgentCode))
//        {
//          return false;
//        }
            }
            System.out.println("------------------over ba ---------");
            //--设置现在的相应机构经理
            if (this.mIsManager.equals("true"))
            {
                System.out.println("------------------come in le");
                if (!setZSGroupManager(tZSGroupCode, tAgentGroup, tAgentCode,
                                       this.mLAAgentSchema.getName(),
                                       tNewAgentGrade))
                {
                    return false;
                }
            }
            //代理人信息
            //设置转正日期（当其职级由高于A01职级调为A01时要置空）
//       if ((tLATreeDB.getAgentGrade().compareTo("A01")>0)&&this.mLATreeSchema.getAgentGrade().equals("A01"))
//          this.mLAAgentSchema.setInDueFormDate("");
            if (tNewAgentGrade.equals("A01"))
            {
                mLAAgentSchema.setInDueFormDate("");
            }
            else if (tLAAgentDB.getInDueFormDate() == null ||
                     tLAAgentDB.getInDueFormDate().equals(""))
            {
                this.mLAAgentSchema.setInDueFormDate(tLAAgentDB.getEmployDate());
            }
            else
            {

                //tjj add 1229bug 未置转正日期
                this.mLAAgentSchema.setInDueFormDate(tLAAgentDB.
                        getInDueFormDate());
            }

            this.mLAAgentSchema.setAgentGroup(tAgentGroup);
            this.mLAAgentSchema.setAgentKind("01"); //客户经理
            this.mLAAgentSchema.setMakeDate(tLAAgentDB.getMakeDate());
            this.mLAAgentSchema.setMakeTime(tLAAgentDB.getMakeTime());
            this.mLAAgentSchema.setModifyDate(currentDate);
            this.mLAAgentSchema.setModifyTime(currentTime);
            this.mLAAgentSchema.setOperator(mGlobalInput.Operator);
            //行政信息
            this.mLATreeSchema.setAgentGroup(tAgentGroup);
            String upAgent = this.getUpAgent(tAgentGroup, tNewAgentGrade);
//      if (upAgent == null) {
//        return false;
//      }
            this.mLATreeSchema.setUpAgent(upAgent);
            this.mLATreeSchema.setAgentSeries(tAgentSeries);
            this.mLATreeSchema.setAssessType("0"); //正常
            this.mLATreeSchema.setState("0");
            if ((this.mLATreeSchema.getIntroAgency() != null) &&
                (!this.mLATreeSchema.getIntroAgency().equals("")))
            {
                this.mLATreeSchema.setIntroBreakFlag("0");
                this.mLATreeSchema.setIntroCommStart(this.mLAAgentSchema.
                        getEmployDate());
            }
            if (!tOldAgentGrade.equals(tNewAgentGrade))
            {
                mLATreeSchema.setAgentLastGrade(tLATreeDB.getAgentGrade());
                mLATreeSchema.setAgentLastSeries(tLATreeDB.getAgentSeries());
                mLATreeSchema.setOldStartDate(tLATreeDB.getOldStartDate());
            }
            this.mLATreeSchema.setStartDate(this.mLAAgentSchema.getEmployDate());
            this.mLATreeSchema.setAstartDate(this.mLAAgentSchema.getEmployDate());
            this.mLATreeSchema.setMakeDate(tLATreeDB.getMakeDate());
            this.mLATreeSchema.setMakeTime(tLATreeDB.getMakeTime());
            this.mLATreeSchema.setModifyDate(currentDate);
            this.mLATreeSchema.setModifyTime(currentTime);
            this.mLATreeSchema.setOperator(mGlobalInput.Operator);

            //行政附属信息（先删后插方式）
            if (tNewAgentGrade.compareTo("A04") >= 0)
            {
                LATreeAccessorySchema tLATreeAccessorySchema = new
                        LATreeAccessorySchema();
                tLATreeAccessorySchema.setAgentCode(tAgentCode);
                tLATreeAccessorySchema.setAgentGrade(this.mLATreeSchema.
                        getAgentGrade());
                tLATreeAccessorySchema.setAgentGroup(tAgentGroup);
                /*lis5.3 update
                 tLATreeAccessorySchema.setManageCom(this.mLATreeSchema.getManageCom());
                 */
                if ((this.mLATreeSchema.getEduManager() != null) &&
                    (!this.mLATreeSchema.getEduManager().equals("")))
                {
                    //tLATreeAccessorySchema.setRearAgentCode(tNewAgentGrade.equals("A09")?"":this.mLATreeSchema.getEduManager());
                    tLATreeAccessorySchema.setRearAgentCode(this.mLATreeSchema.
                            getEduManager());
                }
                tLATreeAccessorySchema.setCommBreakFlag("0");
                tLATreeAccessorySchema.setIntroBreakFlag("0");
                tLATreeAccessorySchema.setRearFlag("0");
                tLATreeAccessorySchema.setstartdate(this.mLAAgentSchema.
                        getEmployDate());
                tLATreeAccessorySchema.setBackCalFlag("0");
                tLATreeAccessorySchema.setOperator(mGlobalInput.Operator);
                tLATreeAccessorySchema.setMakeDate(currentDate);
                tLATreeAccessorySchema.setMakeTime(currentTime);
                tLATreeAccessorySchema.setModifyDate(currentDate);
                tLATreeAccessorySchema.setModifyTime(currentTime);
                String tAscript = this.mLATreeSchema.getAscriptSeries().trim();
                prepareTreeAccessory(tAscript, tLATreeAccessorySchema,
                                     tNewAgentGrade);
                this.mLATreeAccessorySet.add(tLATreeAccessorySchema);
            }
        }
        if (this.mOperate.equals("UPDATE||PART"))
        {
            tAgentCode = this.mLAAgentSchema.getAgentCode().trim();
            String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            //查询出AgentGroup和AgentGrade，判断是否修改
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tAgentCode);
            if (!tLAAgentDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAAgentBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "查询原代理人信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //备份原代理人信息
            Reflections tReflections = new Reflections();
            tReflections.transFields(this.mLAAgentBSchema, tLAAgentDB.getSchema());
            this.mLAAgentBSchema.setEdorNo(tEdorNo);
            this.mLAAgentBSchema.setEdorType("05");
            this.mLAAgentBSchema.setMakeDate(currentDate);
            this.mLAAgentBSchema.setMakeTime(currentTime);
            this.mLAAgentBSchema.setModifyDate(currentDate);
            this.mLAAgentBSchema.setModifyTime(currentTime);
            this.mLAAgentBSchema.setOperator(this.mGlobalInput.Operator);
            //代理人信息
            //设置转正日期（当其职级由高于A01职级调为A01时要置空）
            this.mLAAgentSchema.setInDueFormDate(tLAAgentDB.getInDueFormDate());
            this.mLAAgentSchema.setAgentGroup(tLAAgentDB.getAgentGroup());
            this.mLAAgentSchema.setAgentKind("01"); //客户经理
            this.mLAAgentSchema.setMakeDate(tLAAgentDB.getMakeDate());
            this.mLAAgentSchema.setMakeTime(tLAAgentDB.getMakeTime());
            this.mLAAgentSchema.setModifyDate(currentDate);
            this.mLAAgentSchema.setModifyTime(currentTime);
            this.mLAAgentSchema.setOperator(mGlobalInput.Operator);
        }
        if (this.mOperate.equals("INSERT||MAIN") ||
            this.mOperate.indexOf("UPDATE") != -1)
        {
            //担保人信息表
            int aCount = this.mLAWarrantorSet.size();
            System.out.println(Integer.toString(aCount));
            for (int i = 1; i <= aCount; i++)
            {
                if (this.mOperate.equals("INSERT||MAIN"))
                {
                    this.mLAWarrantorSet.get(i).setAgentCode(tAgentCode);
                }
                this.mLAWarrantorSet.get(i).setSerialNo(i);
                this.mLAWarrantorSet.get(i).setOperator(mGlobalInput.Operator);
                this.mLAWarrantorSet.get(i).setMakeDate(currentDate);
                this.mLAWarrantorSet.get(i).setMakeTime(currentTime);
                this.mLAWarrantorSet.get(i).setModifyDate(currentDate);
                this.mLAWarrantorSet.get(i).setModifyTime(currentTime);
            }
        }
        //if (this.mOperate.equals("DELETE||MAIN")&& this.mIsManager.equals("true"))
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setBranchManager(this.mLAAgentSchema.getAgentCode());
            this.mLABranchGroupSet = tLABranchGroupDB.query();
            for (int i = 1; i <= mLABranchGroupSet.size(); i++)
            {
                this.mLABranchGroupSet.get(i).setBranchManager("");
                this.mLABranchGroupSet.get(i).setBranchManagerName("");
            }
        }
        else
        {
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setBranchManager(this.mLAAgentSchema.getAgentCode());
            LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
            tLABranchGroupSet = tLABranchGroupDB.query();
            for (int i = 1; i <= tLABranchGroupSet.size(); i++)
            {
                LABranchGroupSchema tLABranchGroupSchema = tLABranchGroupSet.
                        get(i);
                if (tLABranchGroupSchema.getBranchManagerName() != null &&
                    tLABranchGroupSchema.getBranchManagerName().equals(
                            mLAAgentSchema.getName()))
                {
                    continue;
                }
                else
                {
                    tLABranchGroupSchema.setBranchManagerName(mLAAgentSchema.
                            getName());
                    System.out.println(
                            "before add to LABranchGroupSet....4...." +
                            tLABranchGroupSchema.getBranchAttr());
//          mLABranchGroupSet.add(tLABranchGroupSchema) ;
                }
            }
        }

        return tReturn;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        mIsManager = (String) cInputData.get(1);
        System.out.println(mIsManager);
        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                                      getObjectByObjectName("LAAgentSchema", 0));
        this.mLATreeSchema.setSchema((LATreeSchema) cInputData.
                                     getObjectByObjectName("LATreeSchema", 0));
        this.mLAWarrantorSet.set((LAWarrantorSet) cInputData.
                                 getObjectByObjectName(
                                         "LAWarrantorSet", 0));
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start ALAAgentBLQuery Submit...");
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setSchema(this.mLAAgentSchema);
        this.mLAAgentSet = tLAAgentDB.query();
        this.mResult.add(this.mLAAgentSet);
        System.out.println("End ALAAgentBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLAAgentDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAAgentBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLAAgentSchema);
            this.mInputData.add(this.mLAAgentBSchema);
            this.mInputData.add(this.mLATreeSchema);
            this.mInputData.add(this.mLATreeBSchema);
            this.mInputData.add(this.mLATreeAccessorySet);
            this.mInputData.add(this.mLABranchGroupSet);
            this.mInputData.add(this.mLAWarrantorSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private boolean getInfo(String cAgentGroup)
    {
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(cAgentGroup);
        if (!tLABranchGroupDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBL";
            tError.functionName = "getInfo";
            tError.errorMessage = "查询机构信息出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        //this.mLABranchGroupSchema = null;
        this.mLABranchGroupSchema.setSchema(tLABranchGroupDB.getSchema());
        return true;
    }


    //根据直辖组的显式代码得到代理人职级对应的机构隐式代码
    private String getAgentGradevsGroup(String cZSGroup, String cAgentGrade,
                                        String cBranchAttr)
    {
        String tGradeGroup = cBranchAttr;
        if (cAgentGrade.compareTo("A05") > 0)
        {
            System.out.println("come in");
            switch (Integer.parseInt(cAgentGrade.substring(1)))
            {
                case 6:
                {
                    tGradeGroup = tGradeGroup.substring(0,
                            tGradeGroup.length() - 3); //高级经理
                    break;
                }
                case 7:
                {
                    tGradeGroup = tGradeGroup.substring(0,
                            tGradeGroup.length() - 3); //高级经理
                    break;
                }
                case 8:
                {
                    tGradeGroup = tGradeGroup.substring(0,
                            tGradeGroup.length() - 6); //督导长
                    break;
                }
                case 9:
                {
                    tGradeGroup = tGradeGroup.substring(0,
                            tGradeGroup.length() - 8); //区域督导长
                    System.out.println(tGradeGroup);
                    break;
                }
            }
            String tSQL =
                    "select AgentGroup from laBranchGroup where BranchAttr = '" +
                    tGradeGroup + "'";
            ExeSQL tExeSQL = new ExeSQL();
            tGradeGroup = tExeSQL.getOneValue(tSQL).trim();
            if (tExeSQL.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAgentBL";
                tError.functionName = "getAgentGradevsGroup";
                tError.errorMessage = "执行SQL语句：从表中取值失败!";
                this.mErrors.addOneError(tError);
                return null;
            }
        }
        else
        {
            tGradeGroup = cZSGroup;
        }
        return tGradeGroup;
    }


    //根据代理人职级确定代理人系列
    private String getAgentSeries(String cAgentGrade)
    {
        String tAgentSeries = "";
        String tSQL =
                "select code2 from ldcodeRela where relaType = 'gradeserieslevel' "
                + "and code1 = '" + cAgentGrade + "'";
        ExeSQL tExeSQL = new ExeSQL();
        tAgentSeries = tExeSQL.getOneValue(tSQL).trim();
        if (tExeSQL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "dealData";
            tError.errorMessage = "执行SQL语句：从表中取值失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tAgentSeries;
    }

    private boolean clearOldGroupManager(
            String ManagerCode
            )
    {
        String sql = "select * from labranchgroup where branchmanager='" +
                     ManagerCode + "'";
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(sql);
        for (int i = 1; i <= tLABranchGroupSet.size(); i++)
        {
            LABranchGroupSchema tLABranchGroupSchema = tLABranchGroupSet.get(i);
            tLABranchGroupSchema.setBranchManager("");
            tLABranchGroupSchema.setBranchManagerName("");
            System.out.println("before add to LABranchGroupSet....1...." +
                               tLABranchGroupSchema.getBranchAttr());
            this.mLABranchGroupSet.add(tLABranchGroupSchema);
        }
        return true;

    }

    private boolean setZSGroupManager(String cMinGroup, String cMaxGroup,
                                      String ManagerCode,
                                      String ManagerName, String cAgentGrade)
    {
        String tSQL = "", tZSvalue = "";
        ExeSQL tExeSQL;

        if (!getInfo(cMinGroup))
        {
            return false;
        }
        this.mLABranchGroupSchema.setBranchManager(ManagerCode);
        this.mLABranchGroupSchema.setBranchManagerName(ManagerName);
        this.mLABranchGroupSchema.setModifyDate(currentDate);
        this.mLABranchGroupSchema.setModifyTime(currentTime);
        this.mLABranchGroupSchema.setOperator(this.mGlobalInput.Operator);
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema.setSchema(this.mLABranchGroupSchema);
        System.out.println("before add to LABranchGroupSet....2...." +
                           tLABranchGroupSchema.getBranchAttr());
        this.mLABranchGroupSet.add(tLABranchGroupSchema);
        if (cAgentGrade.compareTo("A05") > 0)
        { //高级经理以上执行
            String tMinGroup = cMinGroup;
            do
            {
                tSQL =
                        "select UpBranch from LABranchGroup where AgentGroup = '" +
                        tMinGroup + "'";
                tExeSQL = new ExeSQL();
                tMinGroup = tExeSQL.getOneValue(tSQL).trim();
                if (tExeSQL.mErrors.needDealError())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tExeSQL.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ALAgentBL";
                    tError.functionName = "setZSGroupManager";
                    tError.errorMessage = "执行SQL语句：从表中取值失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                tSQL =
                        "select UpBranchAttr from LABranchGroup where AgentGroup = '" +
                        tMinGroup + "'";
                tExeSQL = new ExeSQL();
                tZSvalue = tExeSQL.getOneValue(tSQL).trim();
                if (tExeSQL.mErrors.needDealError())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tExeSQL.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ALAgentBL";
                    tError.functionName = "setZSGroupManager";
                    tError.errorMessage = "执行SQL语句：从表中取值失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                System.out.println(tMinGroup + "   " + tZSvalue + "  " +
                                   cMaxGroup);
                if ((tZSvalue.equals("0")) && (!tMinGroup.equals(cMaxGroup)))
                {
                    CError tError = new CError();
                    tError.moduleName = "ALAgentBL";
                    tError.functionName = "setZSGroupManager";
                    tError.errorMessage = "所输销售机构不是该代理人的直辖组!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (!getInfo(tMinGroup))
                {
                    return false;
                }
                this.mLABranchGroupSchema.setBranchManager(ManagerCode);
                this.mLABranchGroupSchema.setBranchManagerName(ManagerName);
                this.mLABranchGroupSchema.setModifyDate(currentDate);
                this.mLABranchGroupSchema.setModifyTime(currentTime);
                this.mLABranchGroupSchema.setOperator(this.mGlobalInput.
                        Operator);
                tLABranchGroupSchema = new LABranchGroupSchema();
                tLABranchGroupSchema.setSchema(this.mLABranchGroupSchema);
                System.out.println("before add to LABranchGroupSet....3...." +
                                   tLABranchGroupSchema.getBranchAttr());
                this.mLABranchGroupSet.add(tLABranchGroupSchema);
            }
            while (!tMinGroup.equals(cMaxGroup));
        }
        else if (!ManagerCode.equals(""))
        {
            tSQL =
                    "select UpBranchAttr from LABranchGroup where AgentGroup = '" +
                    cMinGroup + "'";
            tExeSQL = new ExeSQL();
            tZSvalue = tExeSQL.getOneValue(tSQL).trim();
            if (tExeSQL.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAgentBL";
                tError.functionName = "setZSGroupManager";
                tError.errorMessage = "执行SQL语句：从表中取值失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println(cMinGroup + "   " + tZSvalue + "  " + cMaxGroup);
            if ((tZSvalue.equals("1")))
            {
                CError tError = new CError();
                tError.moduleName = "ALAgentBL";
                tError.functionName = "setZSGroupManager";
                tError.errorMessage = "该代理人所输职级与其机构不对应！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }


    //育成人的个数决定插入行政附属表中的记录数
    /**
     * 根据界面传入的育成链数据，来决定是否需要往LATreeAccessory表中插入育成记录
     * 蔡刚2004-05-28 修改
     * @param cAscriptSeries
     * @param cTASchema
     * @param cAgentGrade
     */
    private void prepareTreeAccessory(String cAscriptSeries,
                                      LATreeAccessorySchema cTASchema,
                                      String cAgentGrade)
    {

        String currAgentGrade = cTASchema.getAgentGrade();
        String agentCode = cTASchema.getAgentCode();
        String tAscriptSeries = cAscriptSeries;
        System.out.println("育成链：" + tAscriptSeries);
        if (tAscriptSeries.indexOf(":") == -1)
        {
            tAscriptSeries = "";
        }
        if (!tAscriptSeries.equals(""))
        {
            tAscriptSeries = tAscriptSeries.substring(0,
                    tAscriptSeries.lastIndexOf(":"));
            if (tAscriptSeries.length() > 1)
            {

                String[] rearAgentCodes = PubFun.split(tAscriptSeries, ":");
                int rearLength = rearAgentCodes.length;
                String agentGroup = cTASchema.getAgentGroup();
                for (int i = rearLength - 1; i >= 0; i--)
                {
                    if (rearAgentCodes[i].equals(""))
                    {
                        continue;
                    }
                    System.out.println("rearAgentCode[" + i + "]:" +
                                       rearAgentCodes[i]);
                    LATreeAccessorySchema tLATreeAccessorySchema = this.
                            getLATreeAccessorySchema(agentCode, i);
                    if (tLATreeAccessorySchema == null)
                    {
                        tLATreeAccessorySchema = new LATreeAccessorySchema();
                        agentGroup = this.getAgentGroupByAgentSeries(agentGroup,
                                i);
                        String agentGrade = this.getGradeBySeries(i);
                        System.out.println(
                                "there is no record in latreeaccessory primary key:" +
                                agentCode +
                                ":" + agentGrade);
                        tLATreeAccessorySchema.setAgentCode(agentCode);
                        tLATreeAccessorySchema.setAgentGrade(agentGrade);
                        tLATreeAccessorySchema.setAgentGroup(agentGroup);
                        /*lis5.3 update
                         tLATreeAccessorySchema.setManageCom(cTASchema.getManageCom());
                         */
                        tLATreeAccessorySchema.setBackCalFlag("0");
                        tLATreeAccessorySchema.setCommBreakFlag("0");
                        tLATreeAccessorySchema.setIntroBreakFlag("0");
                        tLATreeAccessorySchema.setRearFlag("0");
                        tLATreeAccessorySchema.setMakeDate(currentDate);
                        tLATreeAccessorySchema.setMakeTime(currentTime);
                        tLATreeAccessorySchema.setModifyDate(currentDate);
                        tLATreeAccessorySchema.setModifyTime(currentTime);
                        tLATreeAccessorySchema.setOperator(this.mGlobalInput.
                                Operator);
                        tLATreeAccessorySchema.setRearAgentCode(rearAgentCodes[
                                i]);
                        tLATreeAccessorySchema.setstartdate(this.mLAAgentSchema.
                                getEmployDate());

                    }
                    else
                    {
                        //update
                        agentGroup = this.getAgentGroupByAgentSeries(agentGroup,
                                i);
                        tLATreeAccessorySchema.setRearAgentCode(rearAgentCodes[
                                i]);
                        tLATreeAccessorySchema.setAgentGroup(agentGroup);
                        tLATreeAccessorySchema.setCommBreakFlag("0");
                        tLATreeAccessorySchema.setIntroBreakFlag("0");
                        tLATreeAccessorySchema.setRearFlag("0");

                    }
                    this.mLATreeAccessorySet.add(tLATreeAccessorySchema);
                }
            }
        }

//     String tAscriptSeries = cAscriptSeries;
//     System.out.println("育成链："+tAscriptSeries);
//     if (tAscriptSeries.indexOf(":")==-1)
//       tAscriptSeries = "";
//     else
//     {
//         tAscriptSeries = tAscriptSeries.substring(0,tAscriptSeries.lastIndexOf(":"));
//         System.out.println("截调尾巴的育成链："+tAscriptSeries);
//         LATreeAccessorySchema tLATreeAccessorySchema2 = null;
//         Reflections tReflections =  new Reflections();
//         String tAscriptCode = "";
//         int count = 1;
//         do{
//             tLATreeAccessorySchema2 = new LATreeAccessorySchema();
//             tReflections.transFields(tLATreeAccessorySchema2,cTASchema);
//             tAscriptCode = tAscriptSeries.indexOf(":")!=-1?tAscriptSeries.substring(0,tAscriptSeries.indexOf(":")):tAscriptSeries;
//             System.out.println("----AscriptCode:"+tAscriptCode+"--");
//             String agentgroup=getAgentGroupByAgentSeries(tLATreeAccessorySchema2.getAgentCode(),count);
//             switch (count)
//             {
//                 case 1:
//                     tLATreeAccessorySchema2.setAgentGrade("A04");//业务经理一级
//                     break;
//                 case 2:
//                     tLATreeAccessorySchema2.setAgentGrade("A06");//高级业务经理一级
//                     break;
//                 case 3:
//                     tLATreeAccessorySchema2.setAgentGrade("A08");//督导长
//                     break;
//                 case 4:
//                     tLATreeAccessorySchema2.setAgentGrade("A09");//区域督导长
//                     break;
//             }
//             tLATreeAccessorySchema2.setAgentGroup(agentgroup);
//             tLATreeAccessorySchema2.setCommBreakFlag("0");
//             tLATreeAccessorySchema2.setIntroBreakFlag("0");
//
//             tAscriptSeries = tAscriptSeries.indexOf(":")!=-1?tAscriptSeries.substring(tAscriptSeries.indexOf(":")+1):"";
//
//             System.out.println("育成链："+tAscriptSeries);
//             if (!tAscriptCode.equals("")) //育成人为空则不插入记录
//             {
//                 tLATreeAccessorySchema2.setRearAgentCode(tAscriptCode);
//                 this.mLATreeAccessorySet.add(tLATreeAccessorySchema2);
//             }
//             count++;
//         }while(!tAscriptSeries.equals(""));
//     }
    }


    private LATreeAccessorySchema getLATreeAccessorySchema(String AgentCode,
            int i)
    {
        String agentgrade = "";
        switch (i)
        {
            case 0:
                agentgrade = " and ( agentgrade='A04' or agentgrade='A05' )";
                break;
            case 1:
                agentgrade = " and ( agentgrade='A06' or agentgrade='A07') ";
                break;
            case 2:
                agentgrade = " and  agentgrade='A08'  ";
                break;
            case 3:
                agentgrade = " and agentgrade='A09' ";
                break;
        }
        String tSQL = "select * from latreeaccessory where agentcode='" +
                      AgentCode +
                      "'";
        tSQL = tSQL + agentgrade;
        System.out.println(tSQL);
        LATreeAccessoryDB tLATreeAccessoryDB = new LATreeAccessoryDB();
        LATreeAccessorySet tLATreeAccessorySet = new LATreeAccessorySet();
        tLATreeAccessorySet = tLATreeAccessoryDB.executeQuery(tSQL);
        if (tLATreeAccessorySet.size() == 0)
        {
            return null;
        }
        else
        {
            return tLATreeAccessorySet.get(1);
        }
    }

    private String getAgentGroupByAgentSeries(String agentGroup, int Series)
    {
        String branchLevel = "";
        switch (Series)
        {
            case 0:
                branchLevel = "01";
                break;
            case 1:
                branchLevel = "02";
                break;
            case 2:
                branchLevel = "03";
                break;
            case 3:
                branchLevel = "04";
                break;
        }
        String sql =
                "select nvl(agentgroup,'NotFound') from labranchgroup where upBranch='" +
                agentGroup + "' and upBranchAttr='" + 1 + "'";
        ExeSQL tExeSQL = new ExeSQL();
        String agentgroup = tExeSQL.getOneValue(sql);
        System.out.println(sql);
        if (agentgroup.equals("NotFound"))
        {
            agentgroup = "";
        }
        return agentgroup;
    }

    private String getGradeBySeries(int Series)
    {
        String agentGrade = "";
        switch (Series)
        {
            case 0:
                agentGrade = "A04";
                break;
            case 1:
                agentGrade = "A06";
                break;
            case 2:
                agentGrade = "A08";
                break;
            case 3:
                agentGrade = "A09";
                break;
        }
        return agentGrade;
    }

    private String getUpAgent(String agentGroup, String agentGrade)
    {
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(agentGroup);
        if (!tLABranchGroupDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "getUpAgent";
            tError.errorMessage = "查询不到" + agentGroup + "的机构信息!";
            this.mErrors.addOneError(tError);
            return null;
        }
        String branchManager = "";
        if (agentGrade.compareTo("A04") < 0)
        {
            branchManager = tLABranchGroupDB.getBranchManager();
        }
        else
        {
            String upBranch = tLABranchGroupDB.getUpBranch();
            if (upBranch != null && !upBranch.equals(""))
            {
                tLABranchGroupDB.setAgentGroup(upBranch);
                if (!tLABranchGroupDB.getInfo())
                {
                    this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ALAgentBL";
                    tError.functionName = "getUpAgent";
                    tError.errorMessage = "查询不到" + upBranch + "的机构信息!";
                    this.mErrors.addOneError(tError);
                    return null;

                }
            }
            branchManager = tLABranchGroupDB.getBranchManager();
        }
        if (branchManager == null)
        {
            branchManager = "";
        }
//    System.out.println("has found out upAgent:" + branchManager);
//
//    System.out.println("has found out upAgent:" + branchManager);
//    branchManager=null;

        return branchManager;

    }

    private boolean checkData()
    {

        if (this.mOperate.equals("INSERT||MAIN") ||
            this.mOperate.indexOf("UPDATE") != -1)
        {
            String tIDNo = this.mLAAgentSchema.getIDNo();
            String sql = "select count(*) from laagent where idno='" + tIDNo +
                         "' and agentstate<'03' ";
            if (mLAAgentSchema.getAgentCode() != null &&
                !mLAAgentSchema.getAgentCode().equals(""))
            {
                sql = sql + " and agentcode<>'" + mLAAgentSchema.getAgentCode() +
                      "' ";
            }
            ExeSQL tExeSQL = new ExeSQL();
            System.out.println(sql);
            String result = tExeSQL.getOneValue(sql);
            if (Integer.parseInt(result) > 0)
            {
                CError tError = new CError();
                tError.moduleName = "ALAgentBL";
                tError.functionName = "checkData";
                tError.errorMessage = "身份证号" + tIDNo + "已经使用过";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        LAAgentBlacklistDB tLAAgentBlackDB = new LAAgentBlacklistDB();
        tLAAgentBlackDB.setName(this.mLAAgentSchema.getName());
        tLAAgentBlackDB.setSex(this.mLAAgentSchema.getSex());
        tLAAgentBlackDB.setIDNo(this.mLAAgentSchema.getIDNo());
        LAAgentBlacklistSet tLAAgentBlackSet = tLAAgentBlackDB.query();
        if (tLAAgentBlackDB.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "checkData";
            tError.errorMessage = "查询黑名单表出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLAAgentBlackSet.size() > 0)
        {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "checkData()";
            tError.errorMessage = "该代理人已列入黑名单中，无法增员！";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

}
