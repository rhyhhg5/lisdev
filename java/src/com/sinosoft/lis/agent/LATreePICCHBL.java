package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAgentGradeDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentGradeSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.*;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public class LATreePICCHBL implements LATreeInterface {
    MMap map = new MMap();
    String mOperate = new String();
    String mIsManager = new String();
    GlobalInput mGlobalInput = new GlobalInput();
    LATreeSchema mLATreeSchema = new LATreeSchema();
    LATreeBSchema mLATreeBSchema = new LATreeBSchema();
    LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    String mAgentCode = new String();
    String mBranchCode = new String();
    String mAgentGroup = new String();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String originalDate = "";
    private String originalTime = "";
    private String originalStartDate = "";
    private String originalAstartDate = "";
    String mEdorNo = new String();
    LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();

    public void setOperate(String cOperate) {
        mOperate = cOperate;
    }

    public void setIsManager(String cIsManager) {
        mIsManager = cIsManager;
    }

    public void setGlobalInput(GlobalInput cGlobalInput) {
        mGlobalInput.setSchema(cGlobalInput);
    }

    public void setLATreeSchema(LATreeSchema cLATreeSchema) {
        mLATreeSchema.setSchema(cLATreeSchema);
    }

    public LATreeSchema getLATreeSchema() {
        return mLATreeSchema;
    }

    public void setLAAgentSchema(LAAgentSchema cLAAgentSchema) {
        mLAAgentSchema.setSchema(cLAAgentSchema);
    }

    public void setAgentCode(String cAgentCode) {
        mAgentCode = cAgentCode;
    }

    public MMap getResult() {
        return map;
    }

    public void setEdorNo(String cEdorNo) {
        mEdorNo = cEdorNo;
    }

    public LATreePICCHBL() {
    }

    public static void main(String[] args) {
        LATreePICCHBL latreeproductbl = new LATreePICCHBL();
    }

    public boolean dealdata() {
        System.out.println("....Enter LATreeProductBL.DealData ......");
        //
        if (this.mOperate.indexOf("UPDATE") != -1) {
        	  //查找人员行政信息
            mAgentCode = mLAAgentSchema.getAgentCode();
            LATreeDB tLATreeDB = new LATreeDB();
            tLATreeDB.setAgentCode(mAgentCode);
            if (!tLATreeDB.getInfo()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAAgentBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询原行政信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

            //备份行政信
            Reflections tReflections = new Reflections();
            tReflections.transFields(this.mLATreeBSchema, tLATreeDB.getSchema());
            this.mLATreeBSchema.setAstartDate(tLATreeDB.getAstartDate());
            this.mLATreeBSchema.setEdorNO(mEdorNo);
            LAAgentSchema tLAAgentSchema = new LAAgentSchema();
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(mLAAgentSchema.getAgentCode());
            tLAAgentDB.getInfo();
            tLAAgentSchema = tLAAgentDB.getSchema();
            //二次增员(离职人员状态>=03且前台传入的状态为02,是二次增员操作),不需的到保留原来的数据
            if (mLAAgentSchema.getAgentState().equals("02") &&
                tLAAgentSchema.getAgentState().compareTo("03") >= 0) {
                this.mLATreeBSchema.setRemoveType("04");
            }
            //原始修改
            else {
                this.mLATreeBSchema.setRemoveType("05");
            }
            this.mLATreeBSchema.setMakeDate2(this.mLATreeBSchema.getMakeDate());
            this.mLATreeBSchema.setMakeTime2(this.mLATreeBSchema.getMakeTime());
            this.mLATreeBSchema.setModifyDate2(this.mLATreeBSchema.getModifyDate());
            this.mLATreeBSchema.setModifyTime2(this.mLATreeBSchema.getModifyTime());
            this.mLATreeBSchema.setOperator2(this.mLATreeBSchema.getOperator());
            this.mLATreeBSchema.setMakeDate(currentDate);
            this.mLATreeBSchema.setMakeTime(currentTime);
            this.mLATreeBSchema.setModifyDate(currentDate);
            this.mLATreeBSchema.setModifyTime(currentTime);
            this.mLATreeBSchema.setOperator(this.mGlobalInput.Operator);
            map.put(mLATreeBSchema, "INSERT");

            /*********************************************************/
            ExeSQL tExe = new ExeSQL();
            SSRS tSSRS = new SSRS();
            String strSQL = " SELECT b.branchmanagername,b.agentgroup FROM  laagent a,labranchgroup b "
                           +" WHERE a.agentcode=b.branchmanager AND a.agentgroup = b.agentgroup "
                           +" AND a.agentcode = '" +this.mAgentCode + "'";
            tSSRS = tExe.execSQL(strSQL);

            if (tExe.mErrors.needDealError()) {
                this.mErrors.copyAllErrors(tExe.mErrors);
                CError tError = new CError();
                tError.moduleName = "LATreePICCHBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询代理人" + mAgentCode +"信息出错！";
                this.mErrors.addOneError(tError);
                return false;
            }

            if (tSSRS.getMaxRow() > 0) {
                String strName = tSSRS.GetText(1, 1);
                String strAgentGroup = tSSRS.GetText(1, 2);

                if (!this.mLAAgentSchema.getName().equals(strName)) {
                    String strUpSQL ="UPDATE LABRANCHGROUP SET BRANCHMANAGERNAME = '" +
                            this.mLAAgentSchema.getName() +"' WHERE AGENTGROUP = '" + strAgentGroup + "'";

                    tExe.execUpdateSQL(strUpSQL);

                    if (tExe.mErrors.needDealError()) {
                        this.mErrors.copyAllErrors(tExe.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "LATreePICCHBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = "更新代理人" + mAgentCode +"信息出错！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
            }
            else {
                tSSRS.Clear();
            }

            /*********************************************************/

            //xjh Modify 2005/04/01
            //修改时备份的原因是在人员是主管并且发生机构主管变动的情况才备份
            //所以这里要添加限制条件
            if (this.mIsManager.equals("true")) {
                LATreeSet tLATreeSet = new LATreeSet();
                LATreeDB tLATreeDB1 = new LATreeDB();
                tLATreeDB1.setUpAgent(mAgentCode);
                tLATreeSet = tLATreeDB1.query();
                for (int i = 1; i <= tLATreeSet.size(); i++) {
                    tLATreeSet.get(i).setUpAgent("");
                    tLATreeSet.get(i).setMakeDate(currentDate);
                    tLATreeSet.get(i).setMakeTime(currentTime);
                    tLATreeSet.get(i).setOperator(mGlobalInput.Operator);
                }
                map.put(tLATreeSet, "UPDATE");
                LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
                LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                tLABranchGroupDB.setBranchManager(mAgentCode);
                tLABranchGroupDB.setBranchManagerName(mLAAgentSchema.getName());
                tLABranchGroupSet = tLABranchGroupDB.query();

                for (int i = 1; i <= tLABranchGroupSet.size(); i++) {
                    tLABranchGroupSet.get(i).setBranchManager("");
                    tLABranchGroupSet.get(i).setBranchManagerName("");
                    tLABranchGroupSet.get(i).setMakeDate(currentDate);
                    tLABranchGroupSet.get(i).setMakeTime(currentTime);
                    tLABranchGroupSet.get(i).setOperator(mGlobalInput.Operator);
                }
                map.put(tLABranchGroupSet, "UPDATE");
            }
            //没有备份机构信息
            originalDate = tLATreeDB.getMakeDate();
            originalTime = tLATreeDB.getMakeTime();
            originalStartDate = tLATreeDB.getStartDate();
            originalAstartDate = tLATreeDB.getAstartDate();
        }
        //
        if (this.mOperate.equals("INSERT||MAIN") ||
            this.mOperate.equals("UPDATE||MAIN") ||
            this.mOperate.equals("UPDATE||ALL")) {
            System.out.println("....Enter LATreeXHBL.DealData......"+ this.mLATreeSchema.getAgentGrade());
            //个险判断人员与团队关系，及相互关系
            if (mLAAgentSchema.getBranchType().equals("1")&&mLAAgentSchema.getBranchType2().equals("01")&&
            		!"".equals(mLATreeSchema.getIntroAgency())&&this.mLATreeSchema.getIntroAgency() != null) {
            	//判断推荐是否在职，在职校验，不在职不校验
            	LAAgentDB tagent=new LAAgentDB();
            	LAAgentSchema sagent=new LAAgentSchema();
            	tagent.setAgentCode(mLATreeSchema.getIntroAgency());
            	tagent.getInfo();
            	sagent=tagent.getSchema();
            	String  tagentstate=sagent.getAgentState();
            	if(tagentstate.compareTo("02")<=0){
                String tsql =" select GradeProperty2 from latree a, laagentgrade b "
                        + " where b.gradecode=a.agentgrade and a.agentcode='"+ mLATreeSchema.getIntroAgency() + "'";
                ExeSQL texesql = new ExeSQL();
                String tupgrade = texesql.getOneValue(tsql);
                if (tupgrade.equals("3")) {
                    CError.buildErr(this, "部经理不能成为推荐人");
                    return false;
                }
                tsql ="select GradeProperty2 from laagentgrade where gradecode='"+ mLATreeSchema.getAgentGrade() + "'";
                String tgrade = texesql.getOneValue(tsql);
                //区经理推荐普通业务员，该业务员应归属到其直辖处
                if (tupgrade.equals("2") && tgrade.equals("0")) {
                    tsql ="select agentgroup from labranchgroup where UpBranchAttr='1' "
                    	  + " and BranchManager = '" +mLATreeSchema.getIntroAgency() 
                          + "' and (endflag<>'Y' or endflag is null) ";
                    String tagentgroup = "" + texesql.getOneValue(tsql);
                    if (tagentgroup.equals("") || tagentgroup.equals("null")) {
                        CError.buildErr(this, "推荐失败，该区经理下属没有直辖机构！");
                        return false;
                    }
                    System.out.println("mLAAgentSchema.getAgentGroup()***:" +mLAAgentSchema.getAgentGroup());
                    //zyy修改去掉这个校验
                	  // if (!mLAAgentSchema.getAgentGroup().equals(tagentgroup)) {
                     	  System.out.println("小熊猫好可爱！");
                      // CError.buildErr(this, "推荐人是区经理，该业务员只能归属到区经理下属的直辖机构"+ tagentgroup + "下！");
                      //  return false;
                      // }
                }
                //会产生育成关系，因此给LATree，现在已经不建立育成关系了
                if (!tgrade.equals("0") &&!mLATreeSchema.getAgentGrade().equals("B01")&& tgrade.equals(tupgrade)) {
                    mLATreeSchema.setEduManager(mLATreeSchema.getIntroAgency());
                }
            	}
            }
            this.mLATreeSchema.setAgentCode(mAgentCode);
            this.mLATreeSchema.setAgentGroup(mLAAgentSchema.getAgentGroup());
            this.mLATreeSchema.setBranchCode(mLAAgentSchema.getBranchCode());
            //upagent

            String tUpAgent = getUpAgent(mLAAgentSchema.getAgentGroup().trim());
            System.out.println(tUpAgent);
            if (tUpAgent == null) {
            	System.out.println("LATreePICCHBL出错了tUpAgent："+tUpAgent);
                return false;
            }
            else if (!tUpAgent.equals("")) {
                if (this.mOperate.indexOf("UPDATE") != -1) {
                }
                else {
                    this.mLATreeSchema.setUpAgent(tUpAgent.trim());
                }
            }
            //获取职级对应的层级
            System.out.println("^^^^^层级^^^^^");
            if (this.mLATreeSchema.getAgentGrade() != null &&!this.mLATreeSchema.getAgentGrade().equals("")) {
                String tAgentSeries = getAgentSeries(mLATreeSchema.getAgentGrade().trim());
                if (tAgentSeries == null) {
                    return false;
                }
                this.mLATreeSchema.setAgentSeries(tAgentSeries.trim());
            }
            System.out.println("^^^^^推荐人^^^^^");
            this.mLATreeSchema.setAssessType("0"); //正常
            this.mLATreeSchema.setState("0");
            if (this.mLATreeSchema.getIntroAgency() != null &&!this.mLATreeSchema.getIntroAgency().equals("")) {
                this.mLATreeSchema.setIntroBreakFlag("0");
                this.mLATreeSchema.setIntroCommStart(this.mLAAgentSchema.getEmployDate());
            }
            //设置内外勤标志,这里由于银代的agentGrade ,agentKind 不是必录项，所以暂时不进行标记 modified by huxl @ 20080306
            if (!mLAAgentSchema.getBranchType().equals("3")) {
                if (!setInsideFlag()) {
                	System.out.println("LATreePICCHBL出错了setInsideFlag："+tUpAgent);
                    return false;
                }
            }
            //xjh modify 2005/04/11
            //插入操作时，StartDate或AstartDate为雇佣日期，
            if (this.mOperate.indexOf("UPDATE") == -1) {
                this.mLATreeSchema.setStartDate(mLAAgentSchema.getEmployDate());
                //新增加的筹备人员，要区分一下，筹备人员职级考核开始日期为筹备开始日期
                 if (this.mLAAgentSchema.getNoWorkFlag() != null &&!this.mLAAgentSchema.getNoWorkFlag().equals("")
                     &&this.mLAAgentSchema.getNoWorkFlag().equals("Y"))
                 {
                    this.mLATreeSchema.setAstartDate(mLAAgentSchema.getTrainDate());
                 }
                 else
                 {
                    this.mLATreeSchema.setAstartDate(mLAAgentSchema.getEmployDate());
                 }
            }
             //修改操作时，取当前系统日期。
            else {
                //xianchun  修改操作时 考核开始日期为原来的日期,二次增员(新增）为新录入的雇佣日期
                LAAgentSchema tLAAgentSchema = new LAAgentSchema();
                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(mLAAgentSchema.getAgentCode());
                tLAAgentDB.getInfo();
                tLAAgentSchema = tLAAgentDB.getSchema();
                //二次增员(离职人员状态大于等于03且前台传入的状态为 02,是二次增员操作)
                if (mLAAgentSchema.getAgentState().equals("02") &&
                    tLAAgentSchema.getAgentState().compareTo("03") >= 0) {
                    this.mLATreeSchema.setStartDate(mLAAgentSchema.getEmployDate());
                    if (this.mLAAgentSchema.getNoWorkFlag() != null &&!this.mLAAgentSchema.getNoWorkFlag().equals("")
                     &&this.mLAAgentSchema.getNoWorkFlag().equals("Y"))
                    {
                 	     this.mLATreeSchema.setAstartDate(mLAAgentSchema.getTrainDate());
                    }
                    else
                    {
                       this.mLATreeSchema.setAstartDate(mLAAgentSchema.getEmployDate());
                    }
                }
                else {
                    this.mLATreeSchema.setStartDate(originalStartDate);
                    if (this.mLAAgentSchema.getNoWorkFlag() != null &&!this.mLAAgentSchema.getNoWorkFlag().equals("")
                     &&this.mLAAgentSchema.getNoWorkFlag().equals("Y"))
                    {
                 	     this.mLATreeSchema.setAstartDate(mLAAgentSchema.getTrainDate());
                    }
                    else
                    {
                       this.mLATreeSchema.setAstartDate(originalAstartDate);
                    }

                }
            }
            if (this.mOperate.indexOf("UPDATE") != -1) {
                this.mLATreeSchema.setMakeDate(originalDate);
                this.mLATreeSchema.setMakeTime(originalTime);
            } else {
                this.mLATreeSchema.setMakeDate(currentDate);
                this.mLATreeSchema.setMakeTime(currentTime);
            }
            this.mLATreeSchema.setModifyDate(currentDate);
            this.mLATreeSchema.setModifyTime(currentTime);
            this.mLATreeSchema.setOperator(mGlobalInput.Operator);
            System.out.println("^^^^^管理人员^^^^^");
            //设置销售机构管理人员信息
            if (this.mIsManager.equals("true")) {
                if (!setGroupManager(this.mLATreeSchema.getAgentGroup(),
                                     mAgentCode,
                                     this.mLAAgentSchema.getName())) {
                    return false;
                }
                //if (this.mOperate.equals("UPDATE||ALL"))
                //部经理不建立任何关系，不更改其下级机构任何信息
                System.out.println("建立关系..."+mLATreeSchema.getAgentGrade().substring(0,2));
                if (!(mLATreeSchema.getAgentGrade()).substring(0,2).equals("B2")) {
                if (!setDirInst(this.mLATreeSchema.getAgentGroup(),mAgentCode)) {
                    return false;
                }
                }
            }
            //xjh Modify 2005/04/13
            //应该使用根据职级置同业衔接属性
            //设置同业衔接属性
//            if (this.mLAAgentSchema.getEmployDate() == null ||
//                this.mLAAgentSchema.getEmployDate().equals(""))
//            {
//                this.mLATreeSchema.setisConnMan("0");
//            }
//            else
//            {
//                this.mLATreeSchema.setisConnMan("1");
//                if (this.mLATreeSchema.getAgentLine().trim().equals("A"))
//                {
//                    this.mLATreeSchema.setInitGrade(this.mLATreeSchema.
//                            getAgentGrade());
//                }
//                else
//                {
//                    this.mLATreeSchema.setInitGrade(this.mLATreeSchema.
//                            getAgentKind());
//                }
//            }
            //这里由于银代的agentGrade ,agentKind 不是必录项，所以暂时不进行标记 modified by huxl @ 20080306
            if (!mLAAgentSchema.getBranchType().equals("3")) {
                if (!setConnMan()) {
                    return false;
                }
            }
            System.out.println("^^^^^放入MAP中^^^^^");
            //放入MAP中
            if (this.mOperate.equals("INSERT||MAIN")) {
                map.put(this.mLATreeSchema, "INSERT");
            } else if (this.mOperate.indexOf("UPDATE") != -1) {
                map.put(this.mLATreeSchema, "UPDATE");
            }
        }
        return true;
    }

    /**
     * setInsideFlag
     *
     * @return boolean
     */
    private boolean setInsideFlag() {
        String tAgentGrade = this.mLATreeSchema.getAgentGrade();
        if (tAgentGrade == null || tAgentGrade.equals("")) {
            String tAgentKind = this.mLATreeSchema.getAgentKind();
            if (tAgentKind == null || tAgentKind.equals("")) {
                CError tError = new CError();
                tError.moduleName = "LAAgentXHBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "查询不到此代理人的级别信息!";
                this.mErrors.addOneError(tError);
                return false;
            } else {
                LAAgentGradeDB tLAAgentGradeDB = new LAAgentGradeDB();
                tLAAgentGradeDB.setGradeCode(tAgentKind.trim());
                if (!tLAAgentGradeDB.getInfo()) {
                    CError tError = new CError();
                    tError.moduleName = "LAAgentXHBL";
                    tError.functionName = "dealdata";
                    tError.errorMessage = "查询不到" + tAgentKind + "的级别信息!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                LAAgentGradeSchema tLAAgentGradeSchema = new LAAgentGradeSchema();
                tLAAgentGradeSchema = tLAAgentGradeDB.getSchema();
                mLATreeSchema.setInsideFlag(tLAAgentGradeSchema.
                                            getGradeProperty7());
            }
        } else {
            LAAgentGradeDB tLAAgentGradeDB = new LAAgentGradeDB();
            tLAAgentGradeDB.setGradeCode(tAgentGrade.trim());
            if (!tLAAgentGradeDB.getInfo()) {
                CError tError = new CError();
                tError.moduleName = "LAAgentXHBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "查询不到" + tAgentGrade + "的级别信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LAAgentGradeSchema tLAAgentGradeSchema = new LAAgentGradeSchema();
            tLAAgentGradeSchema = tLAAgentGradeDB.getSchema();
            mLATreeSchema.setInsideFlag(tLAAgentGradeSchema.
                                        getGradeProperty7());
        }
        return true;
    }

    /**
     * setGroupManager
     *
     * @param cAgentGroup String
     * @param cAgentCode String
     * @param cName String
     * @return bfoolean
     */
    private boolean setGroupManager(String cAgentGroup, String cAgentCode,
                                    String cName) {
        //设置机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(cAgentGroup);
        if (!tLABranchGroupDB.getInfo()) {
            return false;
        }
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = tLABranchGroupDB.getSchema();
        //设置机构属性
        tLABranchGroupSchema.setBranchManager(cAgentCode);
        tLABranchGroupSchema.setBranchManagerName(cName);
        tLABranchGroupSchema.setModifyDate(currentDate);
        tLABranchGroupSchema.setModifyTime(currentTime);
        tLABranchGroupSchema.setOperator(this.mGlobalInput.Operator);
        System.out.println(tLABranchGroupSchema.getAgentGroup());
        //放入MAP
        map.put(tLABranchGroupSchema, "UPDATE");
        //查找下级直辖机构
        LABranchGroupDB upLABranchGroupDB = new LABranchGroupDB();
        upLABranchGroupDB.setUpBranch(cAgentGroup);
        upLABranchGroupDB.setUpBranchAttr("1");
        LABranchGroupSet tLABranchGroupSet = upLABranchGroupDB.query();
        if (mLAAgentSchema.getBranchType().equals("1")&&!mLAAgentSchema.getBranchType2().equals("05")) {
            String tsql =
                    "select GradeProperty2 from laagentgrade where gradecode='"
                    + mLATreeSchema.getAgentGrade() + "'";
            ExeSQL texesql = new ExeSQL();
            String tgrade = texesql.getOneValue(tsql);
            if (tgrade.equals("2") &&
                cAgentGroup.equals(mLATreeSchema.getAgentGroup())) {
                if (tLABranchGroupSet.size() <= 0) {
                    CError.buildErr(this, "本区尚无直辖处，不能增加区经理！");
                    return false;
                } else {
                    mLATreeSchema.setBranchCode(tLABranchGroupSet.get(1).
                                                getAgentGroup());
                    String usql = "update laagent set branchcode='"
                                  + tLABranchGroupSet.get(1).getAgentGroup()
                                  + "' where agentcode='" + cAgentCode + "'";
                    map.put(usql, "UPDATE");
                }
            }
        }

        //没有找到下级直辖机构，返回
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
            return true;
        }

        LABranchGroupSchema upLABranchGroupSchema = tLABranchGroupSet.get(1);
        // LABranchGroupSchema upLABranchGroupSchema = upLABranchGroupDB.getSchema() ;


        //设置下级机构属性
        return setGroupManager(upLABranchGroupSchema.getAgentGroup().trim(),
                               cAgentCode, cName);
    }

    /**
     * 获得上级代理人编码
     * @param agentGroup String
     * @return String
     */
    private String getUpAgent(String agentGroup) {
        //当一个团队主管降级到自己下属机构(降一级)时他的上级主管为空
        //新机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSchema mLABranchGroupSchema = new LABranchGroupDB();

        tLABranchGroupDB.setAgentGroup(agentGroup);
        if (!tLABranchGroupDB.getInfo()) {
            this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LATreeXHBL";
            tError.functionName = "getUpAgent";
            tError.errorMessage = "查询不到" + agentGroup + "的机构信息!";
            this.mErrors.addOneError(tError);
            return null;
        }
        //新机构ＩＤ　
        String tSQL0 =
                "select branchlevelid from labranchlevel where branchtype = '"
                + mLAAgentSchema.getBranchType().trim() +
                "' and branchtype2 = '"
                + mLAAgentSchema.getBranchType2().trim() +
                "' and branchlevelcode = '" +
                tLABranchGroupDB.getBranchLevel().trim() + "'";
        ExeSQL tExeSQL0 = new ExeSQL();
        String tCurLevel = tExeSQL0.getOneValue(tSQL0);
        if (tCurLevel == null || tCurLevel.equals("")) {
            CError tError = new CError();
            tError.moduleName = "LATreeXHBL";
            tError.functionName = "getUpAgent";
            tError.errorMessage = "查询LABranchLevel纪录出错！";
            this.mErrors.addOneError(tError);
            return null;
        }
        int iLevel = Integer.parseInt(tCurLevel.trim());
        System.out.println(iLevel);
        //旧机构信息
        String oldManager;
        String oldAgentGroup;
        int iOldLevel;
        if (!mOperate.equals("INSERT||MAIN")) {
            LABranchGroupDB tLABranchGroupDB1 = new LABranchGroupDB();
            LAAgentSchema tLAAgentSchema = new LAAgentSchema();
            tLAAgentSchema = getAgentBasicInfo(mAgentCode);
            if (tLAAgentSchema == null) {
                return null;
            }
            tLABranchGroupDB1.setAgentGroup(tLAAgentSchema.getAgentGroup().trim());
            if (!tLABranchGroupDB1.getInfo()) {
                this.mErrors.copyAllErrors(tLABranchGroupDB1.mErrors);
                CError tError = new CError();
                tError.moduleName = "LATreeXHBL";
                tError.functionName = "getUpAgent";
                tError.errorMessage = "查询不到" + agentGroup + "的机构信息!";
                this.mErrors.addOneError(tError);
                return null;
            }
            //旧机构ＩＤ　
            String tSQL2 =
                    "select branchlevelid from labranchlevel where branchtype = '"
                    + mLAAgentSchema.getBranchType().trim() +
                    "' and branchtype2 = '"
                    + mLAAgentSchema.getBranchType2().trim() +
                    "' and branchlevelcode = '" +
                    tLABranchGroupDB1.getBranchLevel().trim() + "'";
            ExeSQL tExeSQL2 = new ExeSQL();
            String tOldLevel = tExeSQL2.getOneValue(tSQL2);
            if (tOldLevel == null || tOldLevel.equals("")) {
                CError tError = new CError();
                tError.moduleName = "LATreeXHBL";
                tError.functionName = "getUpAgent";
                tError.errorMessage = "查询LABranchLevel纪录出错！";
                this.mErrors.addOneError(tError);
                return null;
            }
            iOldLevel = Integer.parseInt(tOldLevel.trim());
            System.out.println(iOldLevel);
            oldAgentGroup = tLABranchGroupDB1.getAgentGroup().trim();
            oldManager = tLABranchGroupDB1.getBranchManager();
            if (oldManager == null) {
                oldManager = "";
            } else {
                oldManager = oldManager.trim();
            }
        } else {
            oldManager = "";
            oldAgentGroup = "";
            iOldLevel = 0;
        }
        System.out.println("A");
        if (mIsManager.equals("true")) {
            //主管降一级
            System.out.println("B");
            if (iLevel == iOldLevel - 1) {
                //当前上级机构
                String newUpBranch = tLABranchGroupDB.getUpBranch();
                if (newUpBranch == null || newUpBranch.equals("")) {
                    System.out.println("No UpBranch");
                } else {
                    if (newUpBranch.trim().equals(oldAgentGroup) &&
                        oldManager.equals(mAgentCode)) {
                        return "";
                    }
                }
            }
            System.out.println("C");
            String tSQL1 =
                    "select max(branchlevelid) from labranchlevel where branchtype = '"
                    + mLAAgentSchema.getBranchType().trim() +
                    "' and branchtype2 = '"
                    + mLAAgentSchema.getBranchType2().trim() + "'";
            ExeSQL tExeSQL1 = new ExeSQL();
            String tMaxLevel = tExeSQL1.getOneValue(tSQL1);
            if (tMaxLevel == null || tMaxLevel.equals("")) {
                CError tError = new CError();
                tError.moduleName = "LATreeXHBL";
                tError.functionName = "getUpAgent";
                tError.errorMessage = "查询LABranchLevel纪录出错！";
                this.mErrors.addOneError(tError);
                return null;
            }
            int iMax = Integer.parseInt(tMaxLevel.trim());
            System.out.println(iMax);
            if (iMax == iLevel) {
                return "";
            } else {
                System.out.println("D");
                String upbranch = tLABranchGroupDB.getUpBranch();
                tLABranchGroupDB = new LABranchGroupDB();
                tLABranchGroupDB.setAgentGroup(upbranch);
                if (!tLABranchGroupDB.getInfo()) {
                    this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LATreeProductBL";
                    tError.functionName = "getUpAgent";
                    tError.errorMessage = "查询不到" + upbranch + "的机构信息!";
                    this.mErrors.addOneError(tError);
                    return null;
                }
                return tLABranchGroupDB.getBranchManager() == null ? "" :
                        tLABranchGroupDB.getBranchManager();
            }
        } else {
            //最低级主管降为普通业务员
            System.out.println("E");
            if ((iLevel == iOldLevel - 1) && agentGroup.equals(oldAgentGroup) &&
                oldManager.equals(mAgentCode)) {
                return "";
            } else {
                return tLABranchGroupDB.getBranchManager() == null ? "" :
                        tLABranchGroupDB.getBranchManager();
            }
        }
    }

    /**
     * 根据代理人职级确定代理人系列
     * @param cAgentGrade String
     * @return String
     */
    private String getAgentSeries(String cAgentGrade) {
        String tAgentSeries = "";
//        String tSQL =
//                "select code2 from ldcodeRela where relaType = 'gradeserieslevel' "
//                + "and code1 = '" + cAgentGrade + "'";
//        ExeSQL tExeSQL = new ExeSQL();
//        tAgentSeries = tExeSQL.getOneValue(tSQL);
//        if (tExeSQL.mErrors.needDealError())
//        {
//            // @@错误处理
//            this.mErrors.copyAllErrors(tExeSQL.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "ALAgentBL";
//            tError.functionName = "dealData";
//            tError.errorMessage = "执行SQL语句：从表中取值失败!";
//            this.mErrors.addOneError(tError);
//            return null;
//        }
        tAgentSeries = AgentPubFun.getAgentSeries(cAgentGrade);
        if (tAgentSeries == null) {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "dealData";
            tError.errorMessage = "执行SQL语句：从表中取值失败!";
            this.mErrors.addOneError(tError);
            return null;
        }

        return tAgentSeries;
    }

    /**
     * 递归设置当前机构的所有下属机构的管理人员的上级代理人，
     * 包括当前机构的直辖机构的下属机构，直到当前机构的直辖组
     *
     * @param tAgentGroup String
     * @param tManager String
     * @return boolean
     */
    private boolean setDirInst(String tAgentGroup, String tManager) {
        String strSQL = "select * from labranchgroup where UpBranch='" +
                        tAgentGroup +
                        "' and (endflag is null or endflag<>'Y') ";
        //mAdjustDate=this.mLABranchGroupSchema .getModifyDate();
        System.out.println("------strSQL---" + strSQL);
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(strSQL);
        if (tLABranchGroupDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALABranchManagerBL";
            tError.functionName = "setDirInst";
            tError.errorMessage = "查询当前机构" + tAgentGroup + "的下级机构时出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("--tLABranchGroupSet.size()---" +
                           tLABranchGroupSet.size());
        /**如果该机构有下级机构，则
         * 逐机构修改机构主管的行政信息并修改机构主管的上级代理人信息
         * 如果没有，那么该机构一定是个组机构，则需要修改该组机构所有成员的
         * 上级代理人
         */
        if (tLABranchGroupSet.size() > 0) {
            //查询该机构的下级非直辖机构
            strSQL = "select * from labranchgroup where UpBranch='" +
                     tAgentGroup +
                     "' and upbranchattr='0' and (endflag is null or endflag<>'Y' ) ";
            System.out.println("*******strSQL----" + strSQL);
            tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupSet = new LABranchGroupSet();
            tLABranchGroupSet = tLABranchGroupDB.executeQuery(strSQL);
            if (tLABranchGroupDB.mErrors.needDealError()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALABranchManagerBL";
                tError.functionName = "setDirInst";
                tError.errorMessage = "查询当前机构" + tAgentGroup + "的下级机构时出错!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("-----tLABranchGroupSet.size()---" +
                               tLABranchGroupSet.size());
            for (int i = 1; i <= tLABranchGroupSet.size(); i++) {
                LABranchGroupSchema tLABranchGroupSchema = tLABranchGroupSet.
                        get(i);
                LATreeSchema tLATreeSchema = new LATreeSchema();
                LATreeBSchema tLATreeBSchema = new LATreeBSchema();
                LAAgentSchema ttLAAgentSchema = new LAAgentSchema();
                String currManager = tLABranchGroupSchema.getBranchManager();
                System.out.println("--tManager--" + tManager +
                                   "----currManager---" + currManager);
                //备份行政信息
                if ((currManager == null) || currManager.trim().equals("")) {
                    continue;
                }
                tLATreeSchema = getAgentTreeInfo(currManager);
                ttLAAgentSchema = getAgentBasicInfo(currManager);
                if (ttLAAgentSchema.getAgentState().compareTo("02") > 0) {
                    continue;
                }
                if ((tLATreeSchema.getUpAgent() == null) ||
                    tLATreeSchema.getUpAgent().equals("") ||
                    tLATreeSchema.getUpAgent().equals(tManager)) {
                    continue;
                }
                tLATreeBSchema = new LATreeBSchema();
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLATreeBSchema, tLATreeSchema);
                tLATreeBSchema.setAstartDate(tLATreeSchema.getAstartDate());
                tLATreeBSchema.setEdorNO(mEdorNo);
                tLATreeBSchema.setRemoveType("07"); //主管任命
                tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
                tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
                tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
                tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
                tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
                tLATreeBSchema.setMakeDate(currentDate);
                tLATreeBSchema.setMakeTime(currentTime);
                tLATreeBSchema.setModifyDate(currentDate);
                tLATreeBSchema.setModifyTime(currentTime);
                tLATreeBSchema.setOperator(this.mGlobalInput.Operator);
                map.put(tLATreeBSchema, "INSERT");
                //修改行政信息  个险主管人员在增员、二次入司不会变更下属人员的考核开始日期2010-09-14
                //tLATreeSchema.setAstartDate(currentDate);
                System.out.println(tLATreeSchema.getAgentCode() + "" +
                                   tLATreeSchema.getAstartDate());
                tLATreeSchema.setUpAgent(tManager);
                tLATreeSchema.setModifyDate(currentDate);
                tLATreeSchema.setModifyTime(currentTime);
                tLATreeSchema.setOperator(this.mGlobalInput.Operator);
                map.put(tLATreeSchema, "UPDATE");
            }
            //循环的到各级直辖机构
            if (!getDirectDownBranch(tAgentGroup)) {
                return false;
            }
            if (mLABranchGroupSet.size() > 0) {
                strSQL =
                        "select * from latree where AgentCode in (Select AgentCode From LAAgent "
                        +
                        " Where AgentState in ('01','02')) and agentgroup in ('";
                for (int i = 1; i <= mLABranchGroupSet.size(); i++) {
                    LABranchGroupSchema tLABranchGroupSchema = new
                            LABranchGroupSchema();
                    tLABranchGroupSchema = mLABranchGroupSet.get(i);
                    strSQL = strSQL + tLABranchGroupSchema.getAgentGroup();
                    if (i != mLABranchGroupSet.size()) {
                        strSQL = strSQL + "','";
                    }
                }
                strSQL = strSQL + "')";
                System.out.println("------strSQL---" + strSQL);
                if (!setUpAgent(strSQL, tAgentGroup, tManager)) {
                    return false;
                }
            }
        }
        //如果没有下属机构，则该机构一定时一个组，则置组内的人员的上级代理人为新任主管，并且将该组的信息进行备份与更新
        else {
            strSQL = "select * from latree where agentgroup='" + tAgentGroup +
                     "'" + " And AgentCode in (Select AgentCode From LAAgent " +
                     " Where AgentState in ('01','02'))";
            System.out.println("------strSQL---" + strSQL);
            //LATreeSet tLATreeSet = new LATreeSet();
            if (!setUpAgent(strSQL, tAgentGroup, tManager)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 查询tAgentCode的基础信息
     *
     * @param tAgentCode String
     * @return com.sinosoft.lis.schema.LAAgentSchema
     */
    private LAAgentSchema getAgentBasicInfo(String tAgentCode) {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        if (!tLAAgentDB.getInfo()) {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALABranchManagerBL";
            tError.functionName = "getAgentBasicInfo";
            tError.errorMessage = "查询代理人" + tAgentCode + "的基础信息失败！";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLAAgentDB.getSchema();
    }


    /**
     * 查询代理人编码为agentCode的代理人的行政信息
     *
     * @param agentCode String
     * @return com.sinosoft.lis.schema.LATreeSchema
     */
    private LATreeSchema getAgentTreeInfo(String agentCode) {
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(agentCode);
        if (!tLATreeDB.getInfo()) {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALABranchManagerBL";
            tError.functionName = "getAgentTreeInfo";
            tError.errorMessage = "查询" + agentCode + "的行政信息失败！";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLATreeDB.getSchema();
    }

    private boolean getDirectDownBranch(String cBranch) {
        System.out.println("getDirectDownBranch..................");
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupDB.setUpBranch(cBranch);
        tLABranchGroupDB.setUpBranchAttr("1");
        tLABranchGroupSet = tLABranchGroupDB.query();
        if (tLABranchGroupDB.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LARearPublic";
            tError.functionName = "findSelfBranch";
            tError.errorMessage = "查询机构信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("getDirectDownBranch" + tLABranchGroupSet.size());
        //没有找到下级直辖机构，返回本身
        if (tLABranchGroupSet.size() == 0) {
            return true;
        }
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        // tLABranchGroupSchema = tLABranchGroupDB.getSchema();
        tLABranchGroupSchema = tLABranchGroupSet.get(1);
        mLABranchGroupSet.add(tLABranchGroupSet);
        return getDirectDownBranch(tLABranchGroupSchema.getAgentGroup().trim());
    }

    private boolean setUpAgent(String cSQL, String cAgentGroup, String cManager) {
        System.out.println(cManager);
        LATreeSchema tLATreeSchema = new LATreeSchema();
        LATreeDB tLATreeDB = new LATreeDB();
        LATreeSet tLATreeSet = tLATreeDB.executeQuery(cSQL);
        if (tLATreeDB.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALABranchManagerBL";
            tError.functionName = "setDirInst";
            tError.errorMessage = "查询" + cAgentGroup + "的所有成员时失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("----tLATreeSet.size----" + tLATreeSet.size());
        for (int i = 1; i <= tLATreeSet.size(); i++) {
            //备份组员的行政信息
            tLATreeSchema = tLATreeSet.get(i);
            System.out.println(tLATreeSchema.getAgentCode());
            if (tLATreeSchema.getAgentCode().equals(cManager)) {
                continue; //如果主管在这个组中，则主管的上级代理人信息不在这做修改
            }
            if (tLATreeSchema.getUpAgent() != null &&
                tLATreeSchema.getUpAgent().equals(cManager)) {
                continue; //如果上级代理人并没有变，则不用备份修改
            }
            System.out.println("A+" + i);
            LATreeBSchema tLATreeBSchema = new LATreeBSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLATreeBSchema, tLATreeSchema);
            tLATreeBSchema.setAstartDate(tLATreeSchema.getAstartDate());
            tLATreeBSchema.setEdorNO(mEdorNo);
            tLATreeBSchema.setRemoveType("07"); //主管任命
            tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
            tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
            tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
            tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
            tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
            tLATreeBSchema.setMakeDate(currentDate);
            tLATreeBSchema.setMakeTime(currentTime);
            tLATreeBSchema.setModifyDate(currentDate);
            tLATreeBSchema.setModifyTime(currentTime);
            tLATreeBSchema.setOperator(this.mGlobalInput.Operator);
            map.put(tLATreeBSchema, "INSERT");
            //更新组员的行政信息,业务人员不修改astartdate  个险主管人员在增员、二次入司不会变更下属人员的考核开始日期2010-09-14
            System.out.println("B+" + i);
            //tLATreeSchema.setAstartDate(currentDate);
            tLATreeSchema.setUpAgent(cManager);
            tLATreeSchema.setModifyDate(currentDate);
            tLATreeSchema.setModifyTime(currentTime);
            tLATreeSchema.setOperator(this.mGlobalInput.Operator);
            map.put(tLATreeSchema, "UPDATE");
        }
        return true;
    }

    /**
     * 设置同业衔接属性
     * @return boolean
     */
    private boolean setConnMan() {
        String tAgentGrade = this.mLATreeSchema.getAgentGrade();
        String tAgentKind = this.mLATreeSchema.getAgentKind();
        String tAgentLine = this.mLATreeSchema.getAgentLine();
        if (tAgentGrade == null) {
            tAgentGrade = "";
        }
        if (tAgentKind == null) {
            tAgentKind = "";
        }
        if (tAgentLine == null) {
            tAgentLine = "";
        }
        if ((tAgentGrade.equals("") && tAgentKind.equals("")) ||
            tAgentLine.equals("")) {
            CError tError = new CError();
            tError.moduleName = "LAAgentProductBL";
            tError.functionName = "setInDueFormDate";
            tError.errorMessage = "查询不到此代理人的级别信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LAAgentGradeDB tLAAgentGradeDB = new LAAgentGradeDB();
        //注意这里AgentLine写成B，是死程序
        if (tAgentLine.trim().equals("B")) {
            tLAAgentGradeDB.setGradeCode(tAgentKind.trim());
        } else {

            tLAAgentGradeDB.setGradeCode(tAgentGrade.trim());
        }
        if (!tLAAgentGradeDB.getInfo()) {
            CError tError = new CError();
            tError.moduleName = "LAAgentProductBL";
            tError.functionName = "setInDueFormDate";
            tError.errorMessage = "查询不到" + tAgentGrade + "的级别信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LAAgentGradeSchema tLAAgentGradeSchema = new LAAgentGradeSchema();
        tLAAgentGradeSchema = tLAAgentGradeDB.getSchema();
        String isNormal = tLAAgentGradeSchema.getGradeProperty4();
        if (isNormal == null || isNormal.equals("")) {
            CError tError = new CError();
            tError.moduleName = "LAAgentProductBL";
            tError.functionName = "setInDueFormDate";
            tError.errorMessage = "查询" + tAgentGrade + "的级别信息错误!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (isNormal.trim().equals("1")) {
            this.mLATreeSchema.setisConnMan("1");
        }
        else {
            this.mLATreeSchema.setisConnMan("0");
        }
        if (this.mLATreeSchema.getAgentLine().trim().equals("A")) {
            String tFlag = "1";
            if (this.mOperate.indexOf("UPDATE") != -1) { //修改操作（除了二次增员）不要修改InitGrade
                tFlag = "0";
                LAAgentSchema tLAAgentSchema = new LAAgentSchema();
                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(mLAAgentSchema.getAgentCode());
                tLAAgentDB.getInfo();
                tLAAgentSchema = tLAAgentDB.getSchema();
                if (mLAAgentSchema.getAgentState().equals("02") &&
                    tLAAgentSchema.getAgentState().compareTo("03") >= 0) {
                    tFlag = "1"; //二次增员要修改入司职级
                }
            }
            if (tFlag.equals("1")) {
                this.mLATreeSchema.setInitGrade(this.mLATreeSchema.
                                                getAgentGrade());
            }
        } else {
            this.mLATreeSchema.setInitGrade(this.mLATreeSchema.
                                            getAgentKind());
        }

        return true;
    }
}
