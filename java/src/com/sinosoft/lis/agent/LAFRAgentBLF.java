package com.sinosoft.lis.agent;

import com.cbsws.obj.RspUniSalescod;
import com.sinosoft.lis.db.LAQualificationDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAQualificationBSchema;
import com.sinosoft.lis.schema.LAQualificationSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.sys.UnisalescodFun;
import com.sinosoft.lis.vschema.LAQualificationSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.vschema.LAWarrantorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;

/**
 * <p>Title: AgentSystem</p>
 * <p>Description: 销售管理——增员管理</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author xjh
 * @version 1.0
 */

public class LAFRAgentBLF {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private MMap map = new MMap();
    private String mIsManager;
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private LAQualificationSchema mLAQualificationSchema=new LAQualificationSchema();
    private LATreeInterface mLATreeInterface;
    private LAAgentInterface mLAAgentInterface;
    private String currentdate=PubFun.getCurrentDate();
    private String currenttime=PubFun.getCurrentTime();
    private String mAgentCode = "";
    private String mEdorNo = "";

    public String getAgentCode() {
        return mAgentCode;
    }

    public LAFRAgentBLF() {
    }

    public static void main(String[] args) {
        LAFRAgentBLF LAFRAgentBLF = new LAFRAgentBLF();
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Begin LAFRAgentBLF.submitData.........");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start LAFRAgentBLF Submit...");
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAFRAgentBLF";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //modify by fuxin 2014-12-3
        UniCommon tUniCommon = new UniCommon();
        boolean tFlag = tUniCommon.getGroupAgentCode(mOperate, mAgentCode, mLAAgentSchema, "");
        if(!tFlag){
        	this.mErrors = tUniCommon.mErrors;
        	System.out.println("*************LAFRAgentBLF*************");
        	return false;
        }
        
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        System.out.println("Begin LAFRAgentBLF.getInputData.........");
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mIsManager = (String) cInputData.getObject(1);
        System.out.print(mIsManager);

        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                                      getObjectByObjectName("LAAgentSchema", 0));
        this.mLATreeSchema.setSchema((LATreeSchema) cInputData.
                                     getObjectByObjectName("LATreeSchema", 0));
        this.mLAQualificationSchema.setSchema((LAQualificationSchema) cInputData.
                getObjectByObjectName("LAQualificationSchema", 0));
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAFRAgentBLF";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 业务处理主函数
     * @return boolean
     */
    public boolean dealData() {
        System.out.println("Begin LAFRAgentBLF.dealData.........");
        //查找项目组名称
        String tSQL = "select VarValue from LASysVar where VarType = 'prname' ";
        ExeSQL tExeSQL = new ExeSQL();
        String Project = "" + tExeSQL.getOneValue(tSQL);
        //如果没有查到应该调用的程序,则调用product对应的程序组。
        Project = (Project.equals("") || Project.equals("null")) ? "Product" :
                  Project;
        System.out.println("Project:" + Project);

       
        /**生成代理编码AgentCode*/
        if (!getCreateCodeClass(Project)) {
            return false;
        }

        /**处理基本信息*/
        //获取处理类
        if (!getAgentClass(Project)) {
            return false;
        }

        //传入参数
        mLAAgentInterface.setOperate(mOperate);
        mLAAgentInterface.setAgentCode(mAgentCode);
        mLAAgentInterface.setEdorNo(mEdorNo);
        mLAAgentInterface.setGlobalInput(mGlobalInput);
        mLAAgentInterface.setLAAgentSchema(mLAAgentSchema);
        mLAAgentInterface.setLATreeSchema(mLATreeSchema);
        mLAAgentInterface.setOperate(mOperate);
        //处理
        if (!mLAAgentInterface.dealdata()) {
            mErrors.copyAllErrors(mLAAgentInterface.mErrors);
            return false;
        }
        //获取处理结果
        MMap tMap1 = mLAAgentInterface.getResult();
        map.add(tMap1);
        System.out.println(mOperate);
        
        
        /**处理行政信息*/
        //获取处理类
        if (!getTreeClass(Project)) {
            return false;
        }
        //传入参数
        mLATreeInterface.setOperate(mOperate);
        mLATreeInterface.setAgentCode(mAgentCode);
        mLATreeInterface.setEdorNo(mEdorNo);
        mLATreeInterface.setGlobalInput(mGlobalInput);
        mLATreeInterface.setIsManager(mIsManager);
        mLATreeInterface.setLATreeSchema(mLATreeSchema);
        mLATreeInterface.setLAAgentSchema(mLAAgentInterface.
                                          getLAAgentSchema());
        //处理
        if (!mLATreeInterface.dealdata()) {
            mErrors.copyAllErrors(mLATreeInterface.mErrors);
            return false;
        }
        //获取处理结果
        MMap tMap2 = mLATreeInterface.getResult();
        map.add(tMap2);
        
        if(mOperate.equals("INSERT||MAIN")){
        	if(mLAQualificationSchema.getQualifNo()!=null&&!mLAQualificationSchema.getQualifNo().equals("")||
        			(mLAQualificationSchema.getGrantUnit()!=null&&!mLAQualificationSchema.getGrantUnit().equals(""))||
					(mLAQualificationSchema.getValidStart()!=null&&!mLAQualificationSchema.getValidStart().equals(""))||
							(mLAQualificationSchema.getValidEnd()!=null&&!mLAQualificationSchema.getValidEnd().equals(""))||
									(mLAQualificationSchema.getGrantDate()!=null&&!mLAQualificationSchema.getGrantDate().equals(""))||
											(mLAQualificationSchema.getState()!=null&&!mLAQualificationSchema.getState().equals("")))
        	{
        		if(mLAQualificationSchema.getQualifNo()==null||mLAQualificationSchema.getQualifNo().equals(""))
        		{
        			 CError tError = new CError();
        	         tError.moduleName = "LAAgentBLF";
        	         tError.functionName = "dealData";
        	         tError.errorMessage = "资格证信息录入不完全,需录入资格证号";
        	         this.mErrors.addOneError(tError);
        			return false;
        		}
        		if(mLAQualificationSchema.getGrantUnit()==null||mLAQualificationSchema.getGrantUnit().equals(""))
        		{
        			 CError tError = new CError();
        	         tError.moduleName = "LAFRAgentBLF";
        	         tError.functionName = "dealData";
        	         tError.errorMessage = "资格证信息录入不完全,需录入资格证批准单位";
        	         this.mErrors.addOneError(tError);
        			return false;
        		}
        		if(mLAQualificationSchema.getValidStart()==null||mLAQualificationSchema.getValidStart().equals(""))
        		{
        			 CError tError = new CError();
        	         tError.moduleName = "LAFRAgentBLF";
        	         tError.functionName = "dealData";
        	         tError.errorMessage = "资格证信息录入不完全,需录入资格证有效起期";
        	         this.mErrors.addOneError(tError);
        			return false;
        		}
        		if(mLAQualificationSchema.getValidEnd()==null||mLAQualificationSchema.getValidEnd().equals(""))
        		{
        			 CError tError = new CError();
        	         tError.moduleName = "LAFRAgentBLF";
        	         tError.functionName = "dealData";
        	         tError.errorMessage = "资格证信息录入不完全,需录入资格证有效止期";
        	         this.mErrors.addOneError(tError);
        			return false;
        		}
        		if(mLAQualificationSchema.getGrantDate()==null||mLAQualificationSchema.getGrantDate().equals(""))
        		{
        			 CError tError = new CError();
        	         tError.moduleName = "LAFRAgentBLF";
        	         tError.functionName = "dealData";
        	         tError.errorMessage = "资格证信息录入不完全,需录入资格证发放日期";
        	         this.mErrors.addOneError(tError);
        			return false;
        		}
        		if(mLAQualificationSchema.getState()==null||mLAQualificationSchema.getState().equals(""))
        		{
        			 CError tError = new CError();
        	         tError.moduleName = "LAFRAgentBLF";
        	         tError.functionName = "dealData";
        	         tError.errorMessage = "资格证信息录入不完全,需录入资格证书状态";
        	         this.mErrors.addOneError(tError);
        			return false;
        		}
        	this.mLAQualificationSchema.setAgentCode(mAgentCode);
        	this.mLAQualificationSchema.setMakeDate(currentdate);
            this.mLAQualificationSchema.setMakeTime(currenttime);
            this.mLAQualificationSchema.setModifyDate(currentdate);
            this.mLAQualificationSchema.setModifyTime(currenttime);
            this.mLAQualificationSchema.setOperator(mGlobalInput.Operator);
            this.map.put(this.mLAQualificationSchema, "INSERT");
        	}
        }
        if(mOperate.equals("UPDATE||MAIN")){
        	if(mLAQualificationSchema.getQualifNo()!=null&&!mLAQualificationSchema.getQualifNo().equals("")||
        			(mLAQualificationSchema.getGrantUnit()!=null&&!mLAQualificationSchema.getGrantUnit().equals(""))||
					(mLAQualificationSchema.getValidStart()!=null&&!mLAQualificationSchema.getValidStart().equals(""))||
							(mLAQualificationSchema.getValidEnd()!=null&&!mLAQualificationSchema.getValidEnd().equals(""))||
									(mLAQualificationSchema.getGrantDate()!=null&&!mLAQualificationSchema.getGrantDate().equals(""))||
											(mLAQualificationSchema.getState()!=null&&!mLAQualificationSchema.getState().equals("")))
        	{
        		if(mLAQualificationSchema.getQualifNo()==null||mLAQualificationSchema.getQualifNo().equals(""))
        		{
        			 CError tError = new CError();
        	         tError.moduleName = "LAAgentBLF";
        	         tError.functionName = "dealData";
        	         tError.errorMessage = "资格证信息录入不完全,需录入资格证号";
        	         this.mErrors.addOneError(tError);
        			return false;
        		}
        		if(mLAQualificationSchema.getGrantUnit()==null||mLAQualificationSchema.getGrantUnit().equals(""))
        		{
        			 CError tError = new CError();
        	         tError.moduleName = "LAFRAgentBLF";
        	         tError.functionName = "dealData";
        	         tError.errorMessage = "资格证信息录入不完全,需录入资格证批准单位";
        	         this.mErrors.addOneError(tError);
        			return false;
        		}
        		if(mLAQualificationSchema.getValidStart()==null||mLAQualificationSchema.getValidStart().equals(""))
        		{
        			 CError tError = new CError();
        	         tError.moduleName = "LAFRAgentBLF";
        	         tError.functionName = "dealData";
        	         tError.errorMessage = "资格证信息录入不完全,需录入资格证有效起期";
        	         this.mErrors.addOneError(tError);
        			return false;
        		}
        		if(mLAQualificationSchema.getValidEnd()==null||mLAQualificationSchema.getValidEnd().equals(""))
        		{
        			 CError tError = new CError();
        	         tError.moduleName = "LAFRAgentBLF";
        	         tError.functionName = "dealData";
        	         tError.errorMessage = "资格证信息录入不完全,需录入资格证有效止期";
        	         this.mErrors.addOneError(tError);
        			return false;
        		}
        		if(mLAQualificationSchema.getGrantDate()==null||mLAQualificationSchema.getGrantDate().equals(""))
        		{
        			 CError tError = new CError();
        	         tError.moduleName = "LAFRAgentBLF";
        	         tError.functionName = "dealData";
        	         tError.errorMessage = "资格证信息录入不完全,需录入资格证发放日期";
        	         this.mErrors.addOneError(tError);
        			return false;
        		}
        		if(mLAQualificationSchema.getState()==null||mLAQualificationSchema.getState().equals(""))
        		{
        			 CError tError = new CError();
        	         tError.moduleName = "LAFRAgentBLF";
        	         tError.functionName = "dealData";
        	         tError.errorMessage = "资格证信息录入不完全,需录入资格证书状态";
        	         this.mErrors.addOneError(tError);
        			return false;
        		}
        		this.mLAQualificationSchema.setAgentCode(mAgentCode);
            	this.mLAQualificationSchema.setOperator(this.mGlobalInput.Operator);
            	this.mLAQualificationSchema.setModifyDate(this.currentdate);
            	this.mLAQualificationSchema.setModifyTime(this.currenttime);          	
            	//
        	    
        	}
        	LAQualificationDB tLAQualificationDB=new LAQualificationDB();
        	LAQualificationSet tLAQualificationSet=new LAQualificationSet();
        	tLAQualificationDB.setAgentCode(mAgentCode);
        	tLAQualificationSet=tLAQualificationDB.query();
        	if(tLAQualificationSet.size()>=1){
        	LAQualificationSchema oldmLAQualificationSchema=new LAQualificationSchema();
        	oldmLAQualificationSchema=tLAQualificationSet.get(1);    
        	System.out.println(oldmLAQualificationSchema.getQualifNo());
        	
        	LAQualificationBSchema mLAQualificationBSchema=new LAQualificationBSchema();        	
            Reflections tReflections1 = new Reflections();
            tReflections1.transFields(mLAQualificationBSchema, oldmLAQualificationSchema);
            mLAQualificationBSchema.setEdorNo(PubFun1.CreateMaxNo("EdorNo", 19));
            mLAQualificationBSchema.setOperator1(this.mGlobalInput.Operator);
            mLAQualificationBSchema.setMakeDate1(this.currentdate);
            mLAQualificationBSchema.setMakeTime1(this.currenttime);
            this.map.put(mLAQualificationBSchema, "INSERT");  
            this.map.put(oldmLAQualificationSchema, "DELETE");            
        	}
        	if(this.mLAQualificationSchema.getQualifNo()!=null&&!"".equals(this.mLAQualificationSchema.getQualifNo()))
            {
            	this.map.put(this.mLAQualificationSchema, "INSERT");
            }
        }
        /**获取集团工号Unisalescod*/
//        tSQL = "select GroupAgentCode from laagent where agentcode='"+mLAAgentSchema.getAgentCode()+"'";
//        String mGroupAgentCode = tExeSQL.getOneValue(tSQL);
//        UnisalescodFun fun=new UnisalescodFun();
//        RspUniSalescod mRspUniSalescod = new RspUniSalescod();
//        if(mOperate.equals("INSERT||MAIN")){
//        	mLAAgentSchema.setAgentCode(mAgentCode);
//        	mRspUniSalescod=fun.getUnisalescod(mOperate, mLAAgentSchema, null,null,mLAQualificationSchema.getQualifNo());
//        	mLAAgentSchema.setGroupAgentCode(mRspUniSalescod.getUNI_SALES_COD());
//        }else if(mOperate.equals("UPDATE||MAIN")){
//        	mRspUniSalescod=fun.getUnisalescod(mOperate, mLAAgentSchema, null,mGroupAgentCode,null );
//        	mLAAgentSchema.setGroupAgentCode(mGroupAgentCode);
//        }
//        if("01".equals(mRspUniSalescod.getMESSAGETYPE()))
//		{
//			 CError tError = new CError();
//	         tError.moduleName = "LAFRAgentBLF";
//	         tError.functionName = "dealData";
//	         tError.errorMessage = mRspUniSalescod.getERRDESC();
//	         this.mErrors.addOneError(tError);
//			return false;
//		}
//        mLAAgentInterface.getLAAgentSchema().setGroupAgentCode(mLAAgentSchema.getGroupAgentCode());
//        if(mOperate.equals("INSERT||MAIN")){
//        	this.map.put(mLAAgentInterface.getLAAgentSchema(), "DELETE&INSERT");
//        	this.map.put(mLATreeInterface.getLATreeSchema(), "DELETE&INSERT");
//        }else if(mOperate.equals("UPDATE||MAIN")){
//        	this.map.put(mLAAgentInterface.getLAAgentSchema(), "UPDATE");
//        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            System.out.println("Begin LAFRAgentBLF.prepareOutputData.........");
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAFRAgentBLF";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 如果AgentCode不存在（即新增人员），生成AgentCode
     * @param cName String
     * @return boolean
     */
    private boolean getCreateCodeClass(String cName) {
        LACreateCodeInterface tLACreateCodeInterface;
        try {
            Class mCreateCodeClass = Class.forName(
                    "com.sinosoft.lis.agent.LACreateCode" + cName + "BL");
            tLACreateCodeInterface = (LACreateCodeInterface)
                                     mCreateCodeClass.
                                     newInstance();
        } catch (ClassNotFoundException ex1) {
            System.out.println(
                    ".........Not Found LACreateCode" + cName + "BL，错误");
            if (!cName.equals("Product")) {
                if (!getCreateCodeClass("Product")) {
                    return false;
                }
            }
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAFRAgentBLF";
            tError.functionName = "getCreateCodeClass";
            tError.errorMessage = "没有找到LACreateCode" + cName + "BL类！";
            this.mErrors.addOneError(tError);
            return false;
        } catch (Exception ex1) {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAFRAgentBLF";
            tError.functionName = "getCreateCodeClass";
            tError.errorMessage = "调用LACreateCode" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        tLACreateCodeInterface.setManagecom(mLAAgentSchema.getManageCom());
        tLACreateCodeInterface.setOperate(mOperate);
        tLACreateCodeInterface.setBranchType(mLAAgentSchema.getBranchType());
        tLACreateCodeInterface.setBranchType2(mLAAgentSchema.getBranchType2());
        //处理
        if (!tLACreateCodeInterface.dealdata()) {
            mErrors.copyAllErrors(tLACreateCodeInterface.mErrors);
            return false;
        }
        if (mOperate.equals("INSERT||MAIN")) {
            //获取处理结果
            mAgentCode = tLACreateCodeInterface.getAgentCode();
        } else {
            mAgentCode = mLAAgentSchema.getAgentCode();
        }
        mEdorNo = tLACreateCodeInterface.getEdorNo();
        System.out.println("B表流水号mEdorNo:  " + mEdorNo);
        return true;
    }

    /**
     * 获得代理人基本信息处理类
     * @param cName String
     * @return boolean
     */
    private boolean getAgentClass(String cName) {
        try {
            Class mLAAgentClass = Class.forName(
                    "com.sinosoft.lis.agent.LAAgent" + cName + "BL");
            mLAAgentInterface = (LAAgentInterface) mLAAgentClass.
                                newInstance();
        } catch (ClassNotFoundException ex1) {
            System.out.println(
                    ".........Not Found LAAgent" + cName + "BL，错误");
            if (!cName.equals("Product")) {
                if (!getAgentClass("Product")) {
                    return false;
                }
            }
            return true;
        } catch (Exception ex1) {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAFRAgentBLF";
            tError.functionName = "getAgentClass";
            tError.errorMessage = "调用LAAgent" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }



    /**
     * 获得行政信息处理类
     * @param cName String
     * @return boolean
     */
    private boolean getTreeClass(String cName) {
        try {
            Class mLATreeClass = Class.forName(
                    "com.sinosoft.lis.agent.LATree" + cName + "BL");
            mLATreeInterface = (LATreeInterface) mLATreeClass.
                               newInstance();
        } catch (ClassNotFoundException ex1) {
            System.out.println(
                    ".........Not Found LATree" + cName + "BL，错误");
            if (!getTreeClass("Product")) {
                return false;
            }
            return true;
        } catch (Exception ex1) {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAFRAgentBLF";
            tError.functionName = "getTreeClass";
            tError.errorMessage = "调用LATree" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

}
