package com.sinosoft.lis.agent;

import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public class LACreateCodeProductBL implements LACreateCodeInterface
{
    String mOperate = "";
    String mAgentCode = "";
    String mManagecom = "";
    String mEdorNo = "";
    String mBranchType = "";
    String mBranchType2 = "";

    public void setOperate(String cOperate)
    {
        mOperate = cOperate.trim();
    }

    public void setManagecom(String cManagecom)
    {
        mManagecom = cManagecom.trim();
    }

    public String getAgentCode()
    {
        return mAgentCode;
    }

    public String getEdorNo()
    {
        return mEdorNo;
    }

    public void setBranchType(String cBranchType)
    {
        mBranchType = cBranchType.trim();
    }

    public void setBranchType2(String cBranchType2)
    {
        mBranchType2 = cBranchType2.trim();
    }

    public LACreateCodeProductBL()
    {
    }


    public static void main(String[] args)
    {
        LACreateCodeProductBL lacreatecodeproductbl = new LACreateCodeProductBL();
    }

    public boolean dealdata()
    {
        if (mOperate.equals("INSERT||MAIN"))
        {
            if (mManagecom.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "LACreateCodeProductBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "没有管理机构！";
                this.mErrors.addOneError(tError);
            }
            String tPrefix = "";
            String Zero = "00000000000000000000";
            if (mManagecom.length() > 4)
            {
                tPrefix = mManagecom.substring(0, 4);
            }
            else if (mManagecom.length() == 4)
            {
                tPrefix = mManagecom;
            }
            else
            {
                tPrefix = mManagecom + Zero.substring(mManagecom.length(), 4);
            }
            String tAgentCode = PubFun1.CreateMaxNo("AgentCode" + tPrefix, 6);
            if (tAgentCode == null || tAgentCode.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "LACreateCodeProductBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "生成代理人代码错误！";
                this.mErrors.addOneError(tError);
            }
            mAgentCode = tPrefix + tAgentCode;
            System.out.println("代理人编码：" + mAgentCode);
        }
        //这里对BranchType2有可能需要增加控制
        if (this.mOperate.indexOf("UPDATE") != -1 || mBranchType.equals("2"))
        {
            String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            if (tEdorNo == null || tEdorNo.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "LACreateCodeProductBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "生成转储代码错误！";
                return false;
            }
            mEdorNo = tEdorNo;
            System.out.println("转储代码：" + mEdorNo);
        }
        return true;
    }
}
