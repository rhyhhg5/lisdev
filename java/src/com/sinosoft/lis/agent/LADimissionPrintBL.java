package com.sinosoft.lis.agent;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: sinosoft</p>
 * @author XX
 * @version 1.0
 */
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;

import com.sinosoft.lis.agentprint.LISComparator;
import com.sinosoft.lis.agent.*;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;

public class LADimissionPrintBL {

        /** 错误处理类，每个需要错误处理的类中都放置该类 */
        public CErrors mErrors = new CErrors();
        private VData mResult = new VData();
        /** 全局变量 */
        private GlobalInput mGlobalInput = new GlobalInput() ;
        private String mAgentCode="";
        private String mAgentName="";
        private String mStartDate= "";
        private String mEndDate = "";

        private VData mInputData = new VData();
        private String mOperate = "";
        private SSRS mSSRS1 = new SSRS();
        private XmlExport mXmlExport = null;
        private PubFun mPubFun = new PubFun();
        private ListTable mListTable = new ListTable();
        private TransferData mTransferData = new TransferData();

    public LADimissionPrintBL() {
    }

    /**
* 传输数据的公共方法
*/
public boolean submitData(VData cInputData, String cOperate)
{

 mOperate = cOperate;
 mInputData = (VData) cInputData;
 if (mOperate.equals("")) {
     this.bulidError("submitData", "数据不完整");
     return false;
 }

 if (!mOperate.equals("PRINT")) {
     this.bulidError("submitData", "数据不完整");
     return false;
 }

 // 得到外部传入的数据，将数据备份到本类中
   if (!getInputData(mInputData)) {
       return false;
   }

   // 进行数据查询
if (!dealdate()) {
    return false;
}
System.out.println("dayin");

//进行数据打印
if (!getPrintData()) {
    this.bulidError("getPrintData", "查询数据失败！");
    return false;
}
System.out.println("dayinchenggong1232121212121");

   return true;
}

/**
 * 取得传入的数据
 * @return boolean
 */
private boolean getInputData(VData cInputData)
{

    try
    {
        mGlobalInput.setSchema((GlobalInput) cInputData.
                               getObjectByObjectName("GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
         //页面传入的数据 三个
         this.mAgentCode = (String) mTransferData.getValueByName("tAgentCode");
 //        this.mAgentName = (String) mTransferData.getValueByName("tAgentName");
         this.mOperate = (String) mTransferData.getValueByName("tOperation");
         String sql = "select name from laagent where  agentcode='" + mAgentCode +
                      "'";

         SSRS tSSRS = new SSRS();

         ExeSQL tExeSQL = new ExeSQL();

         tSSRS = tExeSQL.execSQL(sql);
         this.mAgentName = tSSRS.GetText(1, 1);

         System.out.println(mAgentCode);
//         System.out.println(mAgentName);

    } catch (Exception ex) {
        this.mErrors.addOneError("");
        return false;
    }

    return true;

}

/**
* 获取打印所需要的数据
* @param cFunction String
* @param cErrorMsg String
*/
private void bulidError(String cFunction, String cErrorMsg) {

   CError tCError = new CError();

   tCError.moduleName = "LADimissionPrintBL";
   tCError.functionName = cFunction;
   tCError.errorMessage = cErrorMsg;

   this.mErrors.addOneError(tCError);

}


/**
* 业务处理方法
* @return boolean
*/

private boolean dealdate()
{
  System.out.println("chuli1");
  //查询数据
  if (!getAgentNow())
  {
     return false;
  }

  return true;
}

/**
* 查询数据
* @return boolean
*/
private boolean getAgentNow() {

   ExeSQL tExeSQL = new ExeSQL();
   String tSql =
	   "select a.prtno,a.appntname,case  when a.appntsex='0' then '男'  else '女' end,"
	    +"(select b.phone from lcaddress b,lcappnt c where b.customerno=a.appntno  and b.addressno=c.addressno "
	    +"  and b.customerno=appntno and c.contno=a.contno),a.prem,a.makedate,a.signdate,a.customgetpoldate,"
	    +"case a.stateflag when '2' then '是' else '否' end "
	    +" from  lccont  a where  a.agentcode='"+mAgentCode
	    +"' and (a.appflag<>'1' or a.appflag is null ) and a.stateflag<>'3' and a.grpcontno='00000000000000000000' "
	    +"and ((a.uwflag<>'a' and a.uwflag<>'8' and a.uwflag<>'1' and uwflag<>'2') or a.uwflag is null) "
	    +"and not exists (select 'X' from lcrnewstatelog where newcontno = a.contno)	"
	    +" union "
	    +"select a.prtno,a.appntname,case  when a.appntsex='0' then '男' else '女' end,"
	    +"(select b.phone from lcaddress b,lcappnt c where b.customerno=a.appntno  and b.addressno=c.addressno "
	    +"  and b.customerno=appntno and c.contno=a.contno),a.prem,a.makedate,a.signdate,"
	    +"case when a.customgetpoldate is null then a.customgetpoldate else a.getpoldate end, "
	    +"case a.stateflag when '2' then '是' else '否' end "
	    +"from  lccont  a where  a.agentcode='"+mAgentCode
	    +"'  and  (a.customgetpoldate is null or a.getpoldate is null)  and a.stateflag<>'3' and a.grpcontno='00000000000000000000' "
	    +" and  ((a.uwflag<>'a' and a.uwflag<>'8' and a.uwflag<>'1' and uwflag<>'2') or a.uwflag is null) "
	    +"and not exists (select 'X' from lcrnewstatelog where newcontno = a.contno) "
	    +" union "
	    +"select a.prtno,a.grpname,'',"
	    +"(select b.phone from lcaddress b,lcappnt c where b.customerno=a.appntno  and b.addressno=c.addressno "
	    +"  and b.customerno=appntno and c.grpcontno=a.grpcontno),a.prem,a.makedate,a.signdate,a.customgetpoldate,"
	    +"case a.stateflag when '2' then '是' else '否' end "
	    +" from  lcgrpcont  a where  a.agentcode='"+mAgentCode
	    +"' and (a.appflag<>'1' or a.appflag is null ) and a.stateflag<>'3' "
	    +"and ((a.uwflag<>'a' and a.uwflag<>'8' and a.uwflag<>'1' and uwflag<>'2') or a.uwflag is null) "
	    +"and not exists (select 'X' from lcrnewstatelog where newcontno = a.grpcontno)	"
	    +" union "
	    +"select a.prtno,a.grpname,'',"
	    +"(select b.phone from lcaddress b,lcappnt c where b.customerno=a.appntno  and b.addressno=c.addressno "
	    +" and b.customerno=appntno and c.grpcontno=a.grpcontno),a.prem,a.makedate,a.signdate,"
	    +"case when a.customgetpoldate is null then a.customgetpoldate else a.getpoldate end, "
	    +"case a.stateflag when '2' then '是' else '否' end "
	    +"from  lcgrpcont  a where  a.agentcode='"+mAgentCode
	    +"' and  (a.customgetpoldate is null or a.getpoldate is null)  and a.stateflag<>'3' "
	    +" and ((a.uwflag<>'a' and a.uwflag<>'8' and a.uwflag<>'1' and uwflag<>'2') or a.uwflag is null) "
	    +"and not exists (select 'X' from lcrnewstatelog where newcontno = a.grpcontno)	"
	    ;


   mSSRS1 = tExeSQL.execSQL(tSql);
   System.out.println(tSql);
   if (tExeSQL.mErrors.needDealError()) {
       CError tCError = new CError();
       tCError.moduleName = "MakeXMLBL";
       tCError.functionName = "creatFile";
       tCError.errorMessage = "查询XML数据出错！";
       this.mErrors.addOneError(tCError);
       return false;

   }
   if (mSSRS1.getMaxRow() <= 0) {
       CError tCError = new CError();
       tCError.moduleName = "MakeXMLBL";
       tCError.functionName = "creatFile";
       tCError.errorMessage = "没有符合条件的信息！";
       this.mErrors.addOneError(tCError);

       return false;
   }

   return true;
}

/**
 *
 * @return boolean
 */
private boolean getPrintData() {
    TextTag tTextTag = new TextTag();
    mXmlExport = new XmlExport();
    //设置模版名称
    mXmlExport.createDocument("LADimissionPrint.vts", "printer");

    String tMakeDate = "";
    String tMakeTime = "";

    tMakeDate = mPubFun.getCurrentDate();
    tMakeTime = mPubFun.getCurrentTime();

    System.out.print("dayin252");
/**   if (!getManageName()) {
        return false;
    }
 */


    tTextTag.add("MakeDate", tMakeDate);
    tTextTag.add("MakeTime", tMakeTime);
    tTextTag.add("tCode", mAgentCode);
    tTextTag.add("tName", mAgentName);
    tTextTag.add("StartDate",tMakeDate);
    tTextTag.add("EndDate",tMakeTime);


    System.out.println("1212121" + tMakeDate);
    if (tTextTag.size() < 1) {
        return false;
    }

    mXmlExport.addTextTag(tTextTag);

    String[] title = {"", "", "", "", "" ,"","" ,"",""};

    if (!getListTable()) {
        return false;
    }
    System.out.println("111");
    mXmlExport.addListTable(mListTable, title);
    System.out.println("121");
    mXmlExport.outputDocumentToFile("c:\\", "new1");
    this.mResult.clear();

    mResult.addElement(mXmlExport);

    return true;
}

/**
 * 查询列表显示数据
 * @return boolean
 */
private boolean getListTable() {
    System.out.println("dayimboiap288");
    if (mSSRS1.getMaxRow() > 0) {
        for (int i = 1; i <= mSSRS1.getMaxRow(); i++) {
            String Info[] = new String[8];
            Info[0] = mSSRS1.GetText(i, 1);
            Info[1] = mSSRS1.GetText(i, 2);
            Info[2] = mSSRS1.GetText(i, 3);
            Info[3] = mSSRS1.GetText(i, 4)==null?"":mSSRS1.GetText(i, 4);
           // Info[4] = "111";
            Info[4] = mSSRS1.GetText(i, 5);
            Info[5] = mSSRS1.GetText(i, 6);
            Info[6] = mSSRS1.GetText(i, 7)==null?"":mSSRS1.GetText(i, 7);
            Info[7] = mSSRS1.GetText(i, 8)==null?"":mSSRS1.GetText(i, 8);


            mListTable.add(Info);

            System.out.println(Info[3]);
            System.out.println("dayin305");
        }
         mListTable.setName("Order");
    } else {
        CError tCError = new CError();
        tCError.moduleName = "CreateXml";
        tCError.functionName = "creatFile";
        tCError.errorMessage = "没有符合条件的信息！";
        this.mErrors.addOneError(tCError);
        return false;
    }
    return true;
}



/**
 * 获取打印所需要的数据
 * @param cFunction String
 * @param cErrorMsg String
 */


/**
 *
 * @return VData
 */
public VData getResult() {
    return mResult;
}


}
