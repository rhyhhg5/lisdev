package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LAInteractiveBL {
    public LAInteractiveBL() {
    }
    //错误处理类
    public CErrors mErrors = new CErrors(); //错误处理类
          //业务处理相关变量
          /** 全局数据 */
    private VData mInputData = new VData();
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();

    
    private MMap map = new MMap();
    public  GlobalInput mGlobalInput = new GlobalInput();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LAAgentSet tLAAgentSet = new LAAgentSet();
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();

    private Reflections ref = new Reflections();


    public static void main(String args[])
    {

    }

    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
          return false;
        }
        if(!checkData()){
        	return false;
        }
        //进行业务处理
        if (!dealData()) {
          return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
          return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start LANewToFormBL Submit...");
        if (!tPubSubmit.submitData(mInputData,"")) {
          // @@错误处理
          this.mErrors.copyAllErrors(tPubSubmit.mErrors);
          CError tError = new CError();
          tError.moduleName = "LANewToFormBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
          this.mErrors.addOneError(tError);
          return false;
        }
        return true;
      }
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
    	System.out.println("数据到这里了么");
      //全局变量
      try {
          this.mErrors.clearErrors();
          this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                      getObjectByObjectName("GlobalInput", 0));
          this.mLAAgentSet= (LAAgentSet)cInputData.getObjectByObjectName("LAAgentSet", 1);
          if(mLAAgentSet.size()<1)
          {
        	  CError tError = new CError();
        	    tError.moduleName = "LAInteractiveBL";
        	    tError.functionName = "getInputData";
        	    tError.errorMessage = "请选择需要变更的业务员";
        	    this.mErrors.addOneError(tError);
        	    return false;  
        	  
          }  
      }
      catch (Exception ex) {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "LAInteractiveBL";
        tError.functionName = "getInputData";
        tError.errorMessage = "在读取处理所需要的数据时出错。";
        this.mErrors.addOneError(tError);
        return false;
      }
      System.out.println("getInputData end ");
      return true;
    }
    
    private boolean dealData(){
       if(mOperate.equals("DoSave"))
       {
        for(int i= 1;i<=mLAAgentSet.size();i++)
        {
            String tEdorNo = "";
            tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            System.out.println("生成edorno值为："+tEdorNo);
        	LAAgentDB tLAAgentDB = new LAAgentDB();
        	LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        	LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
        	tLAAgentSchema =mLAAgentSet.get(i);
        	tLAAgentDB.setAgentCode(tLAAgentSchema.getAgentCode().trim());
        	if(!tLAAgentDB.getInfo())
        	{
        		 CError tError = new CError();
        	     tError.moduleName = "LAInteractiveBL";
        	     tError.functionName = "dealData";
        	     tError.errorMessage = "未找到业务员"+tLAAgentSchema.getAgentCode().trim()+"相关信息！";
        	     this.mErrors.addOneError(tError);
        	     return false;
        	}
           tLAAgentSchema= tLAAgentDB.getSchema();
           //如果该业务员已经有该标志了 跳过不处理
           if(tLAAgentSchema.getTogaeFlag()!=null&&!tLAAgentSchema.getTogaeFlag().equals(""))
           {
              System.out.println("能打印出来么"+tLAAgentSchema.getTogaeFlag());
              if(tLAAgentSchema.getTogaeFlag()=="Y"||(tLAAgentSchema.getTogaeFlag().equals("Y")))
                {
        	      continue;
                }
           }
           //备份B表数据,标志暂时标记为09
           ref.transFields(tLAAgentBSchema, tLAAgentSchema);
           tLAAgentBSchema.setEdorNo(tEdorNo);
           tLAAgentBSchema.setEdorType("09");
           tLAAgentBSchema.setMakeDate(CurrentDate);
           tLAAgentBSchema.setMakeTime(CurrentTime);
           tLAAgentBSchema.setModifyDate(CurrentDate);
           tLAAgentBSchema.setModifyTime(CurrentTime);
           tLAAgentBSchema.setOperator(this.mGlobalInput.Operator);
           this.mLAAgentBSet.add(tLAAgentBSchema);
           
           //处理A表数据
           tLAAgentSchema.setTogaeFlag("Y");
           tLAAgentSchema.setModifyDate(CurrentDate);
           tLAAgentSchema.setModifyTime(CurrentTime);
           tLAAgentSchema.setOperator(this.mGlobalInput.Operator);
           this.tLAAgentSet.add(tLAAgentSchema);
        }
       }
       if(mOperate.equals("DoDelete"))
       {
    	  for(int j= 1;j<=mLAAgentSet.size();j++)
           {
              String tEdorNo = "";
              tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
              System.out.println("生成edorno值为："+tEdorNo);
       	      LAAgentDB tLAAgentDB = new LAAgentDB();
       	      LAAgentSchema t_LAAgentSchema = new LAAgentSchema();
       	      LAAgentBSchema t_LAAgentBSchema = new LAAgentBSchema();
       	      t_LAAgentSchema =mLAAgentSet.get(j);
       	      tLAAgentDB.setAgentCode(t_LAAgentSchema.getAgentCode().trim());
       	        if(!tLAAgentDB.getInfo())
       	           {
       		         CError tError = new CError();
       	             tError.moduleName = "LAInteractiveBL";
       	             tError.functionName = "dealData";
       	             tError.errorMessage = "未找到业务员"+t_LAAgentSchema.getAgentCode().trim()+"相关信息！";
       	             this.mErrors.addOneError(tError);
       	             return false;
       	            }
       	     t_LAAgentSchema= tLAAgentDB.getSchema();
       	     
      
       	 if(t_LAAgentSchema.getTogaeFlag()!=null&&!t_LAAgentSchema.getTogaeFlag().equals(""))
      	  {
       	     
       	//如果该业务员已经有该标志了 跳过不处理
       	  System.out.println("能打印出来么"+t_LAAgentSchema.getTogaeFlag());
               if(t_LAAgentSchema.getTogaeFlag()=="N"||(t_LAAgentSchema.getTogaeFlag().equals("N")))
                 {
       	           continue;
                 }
       	 }
               //备份B表数据,标志暂时标记为09
               ref.transFields(t_LAAgentBSchema, t_LAAgentSchema);
               t_LAAgentBSchema.setEdorNo(tEdorNo);
               t_LAAgentBSchema.setEdorType("09");
               t_LAAgentBSchema.setMakeDate(CurrentDate);
               t_LAAgentBSchema.setMakeTime(CurrentTime);
               t_LAAgentBSchema.setModifyDate(CurrentDate);
               t_LAAgentBSchema.setModifyTime(CurrentTime);
               t_LAAgentBSchema.setOperator(this.mGlobalInput.Operator);
               this.mLAAgentBSet.add(t_LAAgentBSchema);
          
                //处理A表数据
               t_LAAgentSchema.setTogaeFlag("N");
               t_LAAgentSchema.setModifyDate(CurrentDate);
               t_LAAgentSchema.setModifyTime(CurrentTime);
               t_LAAgentSchema.setOperator(this.mGlobalInput.Operator);
               this.tLAAgentSet.add(t_LAAgentSchema);
       	     }
    	  
//    	  else
//       	     {
//       	    	 CError tError = new CError();
//   	             tError.moduleName = "LAInteractiveBL";
//   	             tError.functionName = "dealData";
//   	             tError.errorMessage = "业务员"+t_LAAgentSchema.getAgentCode().trim()+"没有互动标记,不能去除！";
//   	             this.mErrors.addOneError(tError);
//   	             return false;
//       	     }
//         }
       }
    	return true;
    }
 
private boolean checkData()
{
	if(mOperate.equals("DoSave"))
    {
//		for(int i = 1;i<=mLAAgentSet.size();i++)
//		{
//			LAAgentSchema tLAAgentSchema = new LAAgentSchema();
//			tLAAgentSchema = mLAAgentSet.get(i);
//			String tFlag = tLAAgentSchema.getTogaeFlag();
//			System.out.println("互动制标志为："+tFlag);
//			if(tFlag!=null&&!tFlag.equals(""))
//			{
//				 CError tError = new CError();
//   	             tError.moduleName = "LAInteractiveBL";
//   	             tError.functionName = "dealData";
//   	             tError.errorMessage = "选择添加的数据必须是没有互动标记的数据！";
//   	             this.mErrors.addOneError(tError);
//   	             return false;
//			}
//		}
    }
	if(mOperate.equals("DoDelete"))
    {
//		for(int j = 1;j<=mLAAgentSet.size();j++)
//		{
//			LAAgentSchema tLAAgentSchema = new LAAgentSchema();
//			tLAAgentSchema = mLAAgentSet.get(j);
//			String tFlag = tLAAgentSchema.getTogaeFlag();
//			System.out.println("互动制标志为："+tFlag);
//		  if(tFlag!=null&&!tFlag.equals(""))
//      	     {
//			    if(tFlag!="Y"&&!tFlag.equals("Y"))
//			      {
//				    CError tError = new CError();
//   	                tError.moduleName = "LAInteractiveBL";
//   	                tError.functionName = "dealData";
//   	                tError.errorMessage = "选择去除的数据必须是有互动标记为是的数据！";
//   	                this.mErrors.addOneError(tError);
//   	                return false;
//			      }
//		   }
//		  if(tFlag==null&&tFlag.equals(""))
//            {
//			   CError tError = new CError();
// 	             tError.moduleName = "LAInteractiveBL";
// 	             tError.functionName = "dealData";
// 	             tError.errorMessage = "选择去除的数据必须是互动标记为是的数据！";
// 	             this.mErrors.addOneError(tError);
// 	             return false;
//		   }
//		}
    }
	
     return true;	
}
    
    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
private boolean prepareOutputData() {
  try {
    mInputData.clear();
    map.put(this.mLAAgentBSet, "INSERT");
    map.put(this.tLAAgentSet, "UPDATE");
    System.out.println("这里人的个数："+map.size());
    mInputData.add(map);
  }
  catch (Exception ex) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "LAInteractiveBL";
    tError.functionName = "prepareOutputData";
    tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
    this.mErrors.addOneError(tError);
    return false;
  }
  return true;
}
}