/*
 * <p>ClassName: ALADimissionBLS </p>
 * <p>Description: ALADimissionBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agent;

import java.sql.Connection;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LADimissionDB;
import com.sinosoft.lis.vdb.LABranchGroupDBSet;
import com.sinosoft.lis.vdb.LABranchGroupBDBSet;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class ALADimissionBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    public ALADimissionBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        System.out.println("Start ALADimissionBLS Submit...");
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            tReturn = saveLADimission(cInputData);
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            tReturn = deleteLADimission(cInputData);
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            tReturn = updateLADimission(cInputData);
        }
        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End ALADimissionBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean saveLADimission(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALADimissionBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LADimissionDB tLADimissionDB = new LADimissionDB(conn);
            LADimissionSchema tLADimissionSchema = (LADimissionSchema)
                    mInputData.getObjectByObjectName("LADimissionSchema", 0);
            tLADimissionDB.setSchema(tLADimissionSchema);
            if (!tLADimissionDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLADimissionDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALADimissionBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            //修改代理人信息表中的代理人状态字段
            LAAgentDB tLAAgentDB = new LAAgentDB(conn);
            tLAAgentDB.setSchema((LAAgentSchema) mInputData.
                                 getObjectByObjectName("LAAgentSchema", 0));
            if (!tLAAgentDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALADimissionBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "修改代理人状态失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
//处理主管离职 labranchgroup    branchmanager,branchmanagername 为空

            LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
            tLABranchGroupSet.set((LABranchGroupSet) mInputData.getObjectByObjectName(
            "LABranchGroupSet",
            0));
            LABranchGroupDBSet tLABranchGroupDBSet = new LABranchGroupDBSet(conn);
            tLABranchGroupDBSet.set(tLABranchGroupSet);
            if (!tLABranchGroupDBSet.update())
           {
               // @@错误处理
               this.mErrors.copyAllErrors(tLABranchGroupDBSet.mErrors);
               CError tError = new CError();
               tError.moduleName = "ALADimissionBLS";
               tError.functionName = "saveData";
               tError.errorMessage = "修改主管信息失败!";
               this.mErrors.addOneError(tError);
               conn.rollback();
               conn.close();
               return false;
           }
           LABranchGroupBSet tLABranchGroupBSet = new LABranchGroupBSet();
           tLABranchGroupBSet.set((LABranchGroupBSet) mInputData.getObjectByObjectName(
           "LABranchGroupBSet",
           0));
           LABranchGroupBDBSet tLABranchGroupBDBSet = new LABranchGroupBDBSet(conn);
           tLABranchGroupBDBSet.set(tLABranchGroupBSet);
           System.out.println("|||"+tLABranchGroupBSet.size() );
       //    System.out.println(tLABranchGroupBSet.get(1));
           if (!tLABranchGroupBDBSet.insert())
          {
              // @@错误处理
              this.mErrors.copyAllErrors(tLABranchGroupBDBSet.mErrors);
              CError tError = new CError();
              tError.moduleName = "ALADimissionBLS";
              tError.functionName = "saveData";
              tError.errorMessage = "备份主管信息失败!";
              this.mErrors.addOneError(tError);
              conn.rollback();
              conn.close();
              return false;
           }
            //更新LATree 和 LATreeAccessory表中的字段
            /*by zhanghui 2005.3.21 不需要修改LATree 和 LATreeAccessory表中的字段
            ExeSQL tExeSQL = new ExeSQL(conn);
            String tSql = "Update LATree set State = '3'  Where AgentCode = '" +
                          tLAAgentDB.getAgentCode() + "'";
            if (!tExeSQL.execUpdateSQL(tSql))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALADimissionBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "修改行政信息表中的状态信息失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            tSql =
                    "Update LATreeAccessory set State = '3' ,rearflag='1' Where AgentCode = '" +
                    tLAAgentDB.getAgentCode() + "'";
            tExeSQL = new ExeSQL(conn);
            if (!tExeSQL.execUpdateSQL(tSql))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALADimissionBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "修改行政附属信息表中的状态信息失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }*/
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALADimissionBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean deleteLADimission(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALADimissionBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LADimissionDB tLADimissionDB = new LADimissionDB(conn);
            tLADimissionDB.setSchema((LADimissionSchema) mInputData.
                                     getObjectByObjectName("LADimissionSchema",
                    0));
            if (!tLADimissionDB.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLADimissionDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALADimissionBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALADimissionBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean updateLADimission(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "ALADimissionBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LADimissionDB tLADimissionDB = new LADimissionDB(conn);
            tLADimissionDB.setSchema((LADimissionSchema) mInputData.
                                     getObjectByObjectName("LADimissionSchema",
                    0));
            if (!tLADimissionDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLADimissionDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALADimissionBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            //修改代理人信息表中的代理人状态字段
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setSchema((LAAgentSchema) mInputData.
                                 getObjectByObjectName("LAAgentSchema", 0));
            if (!tLAAgentDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALADimissionBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "修改代理人状态失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALADimissionBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }
}
