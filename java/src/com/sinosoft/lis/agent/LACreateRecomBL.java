package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LARearRelationDB;
import com.sinosoft.lis.db.LARecomRelationDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LARearRelationSchema;
import com.sinosoft.lis.schema.LARecomRelationSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LACreateRecomBL {
    public LACreateRecomBL() {
    }
    //错误处理类
    public CErrors mErrors = new CErrors(); //错误处理类
          //业务处理相关变量
          /** 全局数据 */
    private VData mInputData = new VData();
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String mAgentCode= "";
    private String mManageCom="";
    private String mRecomAgentCode="";
    private String mAgentGrade= "";
    private MMap map = new MMap();
    public  GlobalInput mGlobalInput = new GlobalInput();
    private Reflections ref = new Reflections();
    private VData mResult = new VData();

    public static void main(String args[]){
    	//addRecomRelation("1101000515","1101000297");
    }

    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Begin LANewToFormBL.submitData.........");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
          return false;
        }
        //进行业务处理
        if (!dealData()) {
          return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
          return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start LANewToFormBL Submit...");
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
          // @@错误处理
          this.mErrors.copyAllErrors(tPubSubmit.mErrors);
          CError tError = new CError();
          tError.moduleName = "LANewToFormBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
          this.mErrors.addOneError(tError);
          return false;
        }
        return true;
      }
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
      //全局变量
      try {
        System.out.println("Begin LANewToFormBL.getInputData.........");
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mManageCom = (String)cInputData.get(1);
        mAgentCode = (String)cInputData.get(0);
        mRecomAgentCode = (String)cInputData.get(2);
        mAgentGrade=(String)cInputData.get(3);
        System.out.println(mAgentGrade);
        System.out.println(mManageCom);
      }
      catch (Exception ex) {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "LANewToFormBL";
        tError.functionName = "getInputData";
        tError.errorMessage = "在读取处理所需要的数据时出错。";
        this.mErrors.addOneError(tError);
        return false;
      }
      System.out.println("getInputData end ");
      return true;
    }
    
    private boolean dealData(){
    	if(!addRecomRelation(this.mAgentCode,this.mRecomAgentCode)){
    		CError tError = new CError();
            tError.moduleName = "LANewToFormBL";
            tError.functionName = "submitData";
            tError.errorMessage = "未知错误，建立推荐关系失败!";
            mErrors.addOneError(tError);
            return false;
    	}
    	if(mAgentGrade!=null&&!mAgentGrade.equals("")&&mAgentGrade.compareTo("B01")>0){
    		System.out.println("哆啦A梦");
    	   if(!addRearRelation(this.mAgentCode,this.mRecomAgentCode)){
    		CError tError = new CError();
            tError.moduleName = "LANewToFormBL";
            tError.functionName = "submitData";
            tError.errorMessage = "未知错误，建立养成关系失败!";
            mErrors.addOneError(tError);
            return false;
    	  }
    	}
    	return true;
    }
    private boolean addRecomRelation(String tAgentCode,String tRecomAgentCode){
    	String tSQL="update latree set IntroAgency='" +tRecomAgentCode+"',modifydate=current date,modifytime=current time where agentcode='"+tAgentCode+"'";
    	 ExeSQL tExeSQL = new ExeSQL();
         boolean tValue = tExeSQL.execUpdateSQL(tSQL);
         String sql="select * from larecomrelation where agentcode='"+tAgentCode+"'";
         ExeSQL tExeSQL1 = new ExeSQL();
         String tResult=tExeSQL1.getOneValue(sql);
         if(tResult!=null&&!tResult.equals("")){
        	 String tSQL1="delete from larecomrelation where agentcode='"+tAgentCode+"'";
        	 ExeSQL tExeSQL2 = new ExeSQL();
             boolean tValue1 = tExeSQL2.execUpdateSQL(tSQL1);
         }
    	LATreeDB tLATreeDB=new LATreeDB();
    	tLATreeDB.setAgentCode(tAgentCode);
    	tLATreeDB.getInfo();
    	String tAgentSeries=tLATreeDB.getSchema().getAgentSeries();
    	LATreeDB tLATreeDB2=new LATreeDB();
    	tLATreeDB2.setAgentCode(tRecomAgentCode);
    	tLATreeDB2.getInfo();
        LAAgentDB tLAAgentDB=new LAAgentDB();
    	tLAAgentDB.setAgentCode(tAgentCode);
    	tLAAgentDB.getInfo();
    	String tEmployDate=tLAAgentDB.getSchema().getEmployDate();
    	String tAgentGroup=tLATreeDB.getSchema().getAgentGroup();
    	String tRecomLevel="";
    	if(tAgentSeries.equals("2")){
    		tRecomLevel="02";
    	}else{
    		tRecomLevel="01";
    	}
    	LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();
    	LARecomRelationSet tLARecomRelationSet = new LARecomRelationSet();
    	tLARecomRelationDB.setRecomLevel(tRecomLevel);
    	tLARecomRelationDB.setRecomGens("1");
    	tLARecomRelationDB.setAgentCode(tAgentCode);
    	tLARecomRelationDB.setRecomAgentCode(tRecomAgentCode);
    	tLARecomRelationDB.setAgentGroup(tAgentGroup);
    	tLARecomRelationDB.setStartDate(tEmployDate);
    	tLARecomRelationDB.setEndDate(" ");
    	tLARecomRelationDB.setRecomFlag("1");
    	tLARecomRelationDB.setRecomComFlag("1");
    	tLARecomRelationDB.setRecomStartYear("1");
    	tLARecomRelationDB.setRate("0");
    	tLARecomRelationDB.setMakeDate(CurrentDate);
    	tLARecomRelationDB.setMakeTime(CurrentTime);
    	tLARecomRelationDB.setModifyDate(CurrentDate);
    	tLARecomRelationDB.setModifyTime(CurrentTime);
    	tLARecomRelationDB.setOperator("zyy");
    	tLARecomRelationDB.setDeductFlag("");
    	tLARecomRelationDB.setInheritCalFlag("");
    	tLARecomRelationDB.setRecomBonusFlag("");
    	tLARecomRelationDB.setDeductRate("0");
    	tLARecomRelationDB.setAgentGrade(tLATreeDB.getSchema().getAgentGrade());
    	tLARecomRelationDB.setRecomAgentGrade(tLATreeDB2.getSchema().getAgentGrade());
    	tLARecomRelationSet.add(tLARecomRelationDB);
    	map.put(tLARecomRelationSet, "INSERT");
    	//查询推荐人推荐关系
    	LARecomRelationDB tLARecomRelationDB1 = new LARecomRelationDB();
    	tLARecomRelationDB1.setAgentCode(tRecomAgentCode);
    	tLARecomRelationDB1.setRecomFlag("1");//已失效的关系不需要再进行复制
    	LARecomRelationSet tLARecomRelationSet1 = new LARecomRelationSet();
    	tLARecomRelationSet1=tLARecomRelationDB1.query();
    	if(tLARecomRelationSet1!=null&&!tLARecomRelationSet1.equals("")){
    		LARecomRelationSet tLARecomRelationSet2 = new LARecomRelationSet();
    		for(int i=1;i<=tLARecomRelationSet1.size();i++){
    			LARecomRelationSchema tLARecomRelationSchema=new LARecomRelationSchema();
    			ref.transFields(tLARecomRelationSchema, tLARecomRelationSet1.get(i));
    			tLARecomRelationSchema.setRecomLevel(tLARecomRelationSchema.getRecomLevel());
    			tLARecomRelationSchema.setRecomGens(tLARecomRelationSchema.getRecomGens()+1);
    			tLARecomRelationSchema.setAgentCode(tAgentCode);
    			tLARecomRelationSchema.setRecomAgentCode(tLARecomRelationSchema.getRecomAgentCode());
    			tLARecomRelationSchema.setAgentGroup(tAgentGroup);
    			tLARecomRelationSchema.setStartDate(tEmployDate);
    			tLARecomRelationSchema.setEndDate("");
    			tLARecomRelationSchema.setRecomFlag("1");
    			tLARecomRelationSchema.setRecomComFlag("1");
    			tLARecomRelationSchema.setRecomStartYear("1");
    			tLARecomRelationSchema.setRate("0");
    			tLARecomRelationSchema.setMakeDate(CurrentDate);
    			tLARecomRelationSchema.setMakeTime(CurrentTime);
    			tLARecomRelationSchema.setModifyDate(CurrentDate);
    			tLARecomRelationSchema.setModifyTime(CurrentTime);
    			tLARecomRelationSchema.setOperator("zyy");
    			tLARecomRelationSchema.setDeductFlag("");
    			tLARecomRelationSchema.setInheritCalFlag("");
    			tLARecomRelationSchema.setRecomBonusFlag("");
    			tLARecomRelationSchema.setDeductRate("0");
    			tLARecomRelationSchema.setAgentGrade(tLATreeDB.getSchema().getAgentGrade());
    			tLARecomRelationSchema.setRecomAgentGrade(tLARecomRelationSchema.getRecomAgentGrade());
    			tLARecomRelationSet2.add(tLARecomRelationSchema);
    		}
    		map.put(tLARecomRelationSet2, "INSERT");
    	}
    	return true;
    }
    private boolean addRearRelation(String tAgentCode,String tRecomAgentCode){
    	System.out.println("大熊猫");
    	LATreeDB tLATreeDB=new LATreeDB();
    	tLATreeDB.setAgentCode(tAgentCode);
    	tLATreeDB.getInfo();
    	String tAgentSeries=tLATreeDB.getSchema().getAgentSeries();
    	LATreeDB tLATreeDB2=new LATreeDB();
    	tLATreeDB2.setAgentCode(tRecomAgentCode);
    	tLATreeDB2.getInfo();
      	String tAgentSeries2=tLATreeDB2.getSchema().getAgentSeries();
        LAAgentDB tLAAgentDB=new LAAgentDB();
    	tLAAgentDB.setAgentCode(tAgentCode);
    	tLAAgentDB.getInfo();
    	String tEmployDate=tLAAgentDB.getSchema().getEmployDate();
    	String tAgentGroup=tLATreeDB.getSchema().getAgentGroup();
    	String RearLevel="";
    	if(tAgentSeries.equals("2")){
    		RearLevel="02";
    	}else{
    		RearLevel="01";
    	}
    	String sql="select * from larearrelation where agentcode='"+tAgentCode+"'";
          ExeSQL tExeSQL1 = new ExeSQL();
          String tResult=tExeSQL1.getOneValue(sql);
          if(tResult!=null&&!tResult.equals("")){
         	 String tSQL1="delete from larearrelation where agentcode='"+tAgentCode+"'";
         	 ExeSQL tExeSQL2 = new ExeSQL();
              boolean tValue1 = tExeSQL2.execUpdateSQL(tSQL1);
          }
          LARearRelationDB tLARearRelationDB = new LARearRelationDB();
          LARearRelationSet tLARearRelationSet = new LARearRelationSet();
          tLARearRelationDB.setRearLevel(RearLevel);
          tLARearRelationDB.setRearedGens("1");
          tLARearRelationDB.setAgentCode(tAgentCode);
          tLARearRelationDB.setRearAgentCode(tRecomAgentCode);
          tLARearRelationDB.setAgentGroup(tAgentGroup);
          tLARearRelationDB.setstartDate(tEmployDate);
          tLARearRelationDB.setEndDate("");
          tLARearRelationDB.setRearFlag("1");//育成关系必须是有效的
          tLARearRelationDB.setRearCommFlag("1");
          tLARearRelationDB.setRearStartYear("1");
          tLARearRelationDB.setMakeDate(CurrentDate);
          tLARearRelationDB.setMakeTime(CurrentTime);
          tLARearRelationDB.setModifyDate(CurrentDate);
          tLARearRelationDB.setModifyTime(CurrentTime);
          tLARearRelationDB.setOperator("zyy");
          tLARearRelationSet.add(tLARearRelationDB);
          LARearRelationDB tLARearRelationDB1 = new LARearRelationDB();
          LARearRelationSet tLARearRelationSet1 = new LARearRelationSet();
          tLARearRelationDB1.setAgentCode(tRecomAgentCode);
          tLARearRelationDB1.setRearFlag("1");//推荐人育成关系必须是有效的
          tLARearRelationSet1=tLARearRelationDB1.query();
          if(tLARearRelationSet1.size()>=1){
        	  LARearRelationSet tLARearRelationSet2= new LARearRelationSet();
        	  for(int i=1;i<=tLARearRelationSet1.size();i++){
        		  LARearRelationSchema tLARearRelationSchema=new LARearRelationSchema();
        		  ref.transFields(tLARearRelationSchema, tLARearRelationSet1.get(i));
        		  tLARearRelationSchema.setRearLevel(tLARearRelationSchema.getRearLevel());
        		  tLARearRelationSchema.setRearedGens(tLARearRelationSchema.getRearedGens()+1);
        		  tLARearRelationSchema.setAgentCode(tAgentCode);
        		  tLARearRelationSchema.setRearAgentCode(tLARearRelationSchema.getRearAgentCode());
        		  tLARearRelationSchema.setAgentGroup(tAgentGroup);
        		  tLARearRelationSchema.setstartDate(tEmployDate);
        		  tLARearRelationSchema.setEndDate("");
        		  tLARearRelationSchema.setRearFlag("1");
        		  tLARearRelationSchema.setRearCommFlag("1");
        		  tLARearRelationSchema.setRearStartYear("1");
        		  tLARearRelationSchema.setMakeDate(CurrentDate);
        		  tLARearRelationSchema.setMakeTime(CurrentTime);
        		  tLARearRelationSchema.setModifyDate(CurrentDate);
        		  tLARearRelationSchema.setModifyTime(CurrentTime);
        		  tLARearRelationSchema.setOperator("zyy");
        		  tLARearRelationSet2.add(tLARearRelationSchema);
        	  }
        	  if(tAgentSeries.equals(tAgentSeries2)){
        		  map.put(tLARearRelationSet2, "INSERT");
          	}
          }
          if(tAgentSeries.equals(tAgentSeries2)){
        	  map.put(tLARearRelationSet, "INSERT");
      	}
          System.out.println("dabai");
    	return true;
    } 
    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
private boolean prepareOutputData() {
  try {
    System.out.println(
        "Begin LANewToFormBL.prepareOutputData.........");
    mInputData.clear();
    mInputData.add(map);
  }
  catch (Exception ex) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "LANewToFormBL";
    tError.functionName = "prepareOutputData";
    tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
    this.mErrors.addOneError(tError);
    return false;
  }
  return true;
}
}
