package com.sinosoft.lis.agent;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

import java.io.File;

import com.sinosoft.lis.db.LAAgentBlacklistDB;
import com.sinosoft.lis.db.LATrainCourseDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LATrainCourseSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class LATrainCourseBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData  mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String path="";
    /** 业务处理相关变量 */
    private LATrainCourseSchema mLATrainCourseSchema = new LATrainCourseSchema();
    private MMap map = new MMap();
    private String tCourseNo = "";
    public LATrainCourseBL()
    {
    }

    public static void main(String[] args)
    {
    	String testNo = PubFun1.CreateMaxNo("COURSENO", 10);
    	System.out.println("生成的最大号是"+testNo);
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LATrainCourseBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LATrainCourseBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("over dealData");
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        System.out.println("Start LATrainCourseBL Submit...");
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LATrainCourseBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("End LATrainCourseBL Submit...");

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        if (this.mOperate.equals("INSERT||MAIN"))
        {
        	
        	tCourseNo = PubFun1.CreateMaxNo("COURSENO", 10);
        	String agentGroup = "";
        	String tSql = "select  agentgroup from labranchgroup where branchattr = '"+mLATrainCourseSchema.getAgentGroup()+"' and "
        	    + " branchtype = '1' and branchtype2 = '01'";
        	ExeSQL tExeSQL = new ExeSQL();
        	agentGroup=tExeSQL.getOneValue(tSql);
            this.mLATrainCourseSchema.setCourseNo(tCourseNo);
            this.mLATrainCourseSchema.setAgentGroup(agentGroup);
            this.mLATrainCourseSchema.setOperator(this.mGlobalInput.Operator);
            this.mLATrainCourseSchema.setMakeDate(currentDate);
            this.mLATrainCourseSchema.setMakeTime(currentTime);
            this.mLATrainCourseSchema.setModifyDate(currentDate);
            this.mLATrainCourseSchema.setModifyTime(currentTime);
            this.map.put(mLATrainCourseSchema, "INSERT");
            
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            String tCourseNo = "";
            tCourseNo = this.mLATrainCourseSchema.getCourseNo();
            LATrainCourseDB tLATrainCourseDB = new LATrainCourseDB();
            tLATrainCourseDB.setCourseNo(tCourseNo);
            if (!tLATrainCourseDB.getInfo())
            {
                 // @@错误处理
                this.mErrors.copyAllErrors(tLATrainCourseDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LATrainCourseBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "没有查询到，相关人员信息，数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        	String agentGroup = "";
        	String tSql = "select  agentgroup from labranchgroup where branchattr = '"+mLATrainCourseSchema.getAgentGroup()+"' and "
        	    + " branchtype = '1' and branchtype2 = '01'";
        	ExeSQL tExeSQL = new ExeSQL();
        	agentGroup=tExeSQL.getOneValue(tSql);
            this.mLATrainCourseSchema.setAgentGroup(agentGroup);
            this.mLATrainCourseSchema.setCourseList(tLATrainCourseDB.getCourseList());
            this.mLATrainCourseSchema.setTraineeList(tLATrainCourseDB.getTraineeList());
            this.mLATrainCourseSchema.setTrainPicture(tLATrainCourseDB.getTrainPicture());
            this.mLATrainCourseSchema.setTrainConclusion(tLATrainCourseDB.getTrainConclusion());
            this.mLATrainCourseSchema.setMakeDate(tLATrainCourseDB.getMakeDate());
            this.mLATrainCourseSchema.setMakeTime(tLATrainCourseDB.getMakeTime());
            this.mLATrainCourseSchema.setModifyTime(currentTime);
            this.mLATrainCourseSchema.setModifyDate(currentDate);
            this.mLATrainCourseSchema.setOperator(mGlobalInput.Operator);
            this.map.put(mLATrainCourseSchema, "UPDATE");
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
        	
            String tCourseNo = "";
            tCourseNo = this.mLATrainCourseSchema.getCourseNo();
            LATrainCourseDB tLATrainCourseDB = new LATrainCourseDB();
            tLATrainCourseDB.setCourseNo(tCourseNo);
            if (!tLATrainCourseDB.getInfo())
            {
                 // @@错误处理
                this.mErrors.copyAllErrors(tLATrainCourseDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LATrainCourseBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "没有查询到，相关人员信息，数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mLATrainCourseSchema.setSchema(tLATrainCourseDB.getSchema());
            this.map.put(mLATrainCourseSchema, "DELETE");
        	String filePath = "temp/train";
            String fileName = "";
        	for(int i=1;i<=4;i++){
	            try{
//	            	String fileName = "";
					//依次删除4个文件
	            	switch(i){
	            	 	case 1:
	            	 		fileName=mLATrainCourseSchema.getTraineeList();
	            	 		break;
	            	 	case 2:
	            	 		fileName=mLATrainCourseSchema.getTrainPicture();
	            	 		break;
	            	 	case 3:
	            	 		fileName=mLATrainCourseSchema.getTrainConclusion();
	            	 		break;
	            	 	case 4:
	            	 		fileName =mLATrainCourseSchema.getCourseList(); 	
	            	}
					File tFile = new File(path + filePath + "/"+ fileName);
					System.out.println("删除文件路径:"+path + filePath + "/"+ fileName);
					if (tFile.exists()) {
						if (tFile.isFile()) {
							if (!tFile.delete()) {
								System.out.println("文件删除失败");
							}
						}
					}
	            }catch(Exception e){
	            	e.printStackTrace();
	            }
        	}
        }
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mLATrainCourseSchema.setSchema((LATrainCourseSchema)
                                               cInputData.getObjectByObjectName(
                "LATrainCourseSchema", 0));
        this.path=(String)cInputData.getObject(2);
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */

    private boolean prepareOutputData()
    {
        try
        {
            System.out.println("Begin LATrainCourseBL.prepareOutputData.........");
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LATrainCourseBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public String getResult()
    {
        return this.tCourseNo;
    }
}
