package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LARearRelationDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LARearRelationBSchema;
import com.sinosoft.lis.schema.LARearRelationSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.vschema.LARearRelationBSet;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: AgentSystem</p>
 * <p>Description: LIS - 销售管理</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author xjh
 * @version 1.0
 */
public class LARearPICCHBL implements LARearInterface {
    MMap map = new MMap();
    String mOperate = "";
    GlobalInput mGlobalInput = new GlobalInput();
    LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    LATreeSchema mLATreeSchema = new LATreeSchema();
    LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();
    LARearRelationSet mLARearRelationSet = new LARearRelationSet();
    LARearRelationSet mNewAddLARearRelationSet = new LARearRelationSet();
    LARearRelationSchema mLARearRelationSchema;
    String mEdorNo = "";
    private Reflections ref = new Reflections();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String mMakeContNum="";
    public void setOperate(String cOperate) { //设置操作数
        mOperate = cOperate;
    }

    public void setGlobalInput(GlobalInput cGlobalInput) { //设置全局变量
        mGlobalInput.setSchema(cGlobalInput);
    }

    public void setLARearRelationSet(LARearRelationSet cLARearRelationSet) { //设置育成信息
        mLARearRelationSet.set(cLARearRelationSet);
    }

    public void setLATreeSchema(LATreeSchema cLATreeSchema) {
        mLATreeSchema.setSchema(cLATreeSchema);
    }

    public void setEdorNo(String cEdorNo) { //设置转储编码
        mEdorNo = cEdorNo;
    }

    public boolean dealdata(){
        if(1==1){
    	//if (this.mOperate.equals("INSERT||MAIN") ){
    	 String tAgentCode=""+mLATreeSchema.getAgentCode();
       mMakeContNum = PubFun1.CreateMaxNo("EdorNo", 20);
       LARearRelationSet tLARearRelationSet1 = new  LARearRelationSet();
       LARearRelationDB tLARearRelationDB = new  LARearRelationDB();
       LARearRelationBSet  tmLARearRelationBSet = new  LARearRelationBSet();
       tLARearRelationDB.setAgentCode(tAgentCode);
       tLARearRelationSet1 = tLARearRelationDB.query();
       System.out.println("备份推荐表的代数为："+tLARearRelationSet1.size());
       if(tLARearRelationSet1.size()>0)
       {
          System.out.println(tLARearRelationSet1.size());
               for (int k = 1; k <= tLARearRelationSet1.size(); k++) {
                   System.out.println("开始备份");
                   LARearRelationBSchema  tmLARearRelationBSchema=new LARearRelationBSchema();
                   ref.transFields(tmLARearRelationBSchema,
                                   tLARearRelationSet1.get(k));
                   tmLARearRelationBSchema.setEdorNo(mMakeContNum);
                   tmLARearRelationBSchema.setEdorType("09"); //二次增员更改推荐人
                   tmLARearRelationBSchema.setOperator2(mGlobalInput.Operator);
                   tmLARearRelationBSchema.setMakeDate2(currentDate);
                   tmLARearRelationBSchema.setMakeTime2(currentTime);
                   tmLARearRelationBSchema.setModifyDate2(currentDate);
                   tmLARearRelationBSchema.setModifyTime2(currentTime);
                   tmLARearRelationBSet.add(tmLARearRelationBSchema);
                   System.out.println("1111111111111");
           }
           this.map.put(tmLARearRelationBSet, "INSERT");
       }
          if (!tLARearRelationSet1.mErrors.needDealError()) {

               this.map.put(tLARearRelationSet1, "DELETE");
           }
            String tRearAgentCode = ""+mLATreeSchema.getIntroAgency();
            if (tRearAgentCode.equals("")) {
                System.out.println("没有推荐人");
                return true;
            }
            String tsql = "select agentgrade from latree a where  a.agentcode='"+tRearAgentCode+"'";
            ExeSQL tExeSQL = new ExeSQL();
            String tRearGrade = tExeSQL.getOneValue(tsql);

            tsql = "select GradeProperty2 from   laagentgrade A where a.gradecode='"+tRearGrade+"' ";
            ExeSQL texesql = new ExeSQL();
            String tupgrade = texesql.getOneValue(tsql);

            tsql = "select GradeProperty2 from laagentgrade where gradecode='"
                   +mLATreeSchema.getAgentGrade()+"'";
            String tgrade = texesql.getOneValue(tsql);
            //当增加的业务员为主任及以上级别，且新增业务员与推荐人的级别相同，生成育成关系
            if (!tgrade.equals("0")&&!mLATreeSchema.getAgentGrade().equals("B01")
                &&tgrade.equals(tupgrade) && !tRearGrade.equals("B01")) {
                LARearRelationSchema tRearRSchema = new LARearRelationSchema();
                tRearRSchema.setAgentCode(mLATreeSchema.getAgentCode());
                tRearRSchema.setAgentGroup(mLATreeSchema.getAgentGroup());
                tRearRSchema.setRearAgentCode(tRearAgentCode);
                String tAgentGroup = mLATreeSchema.getAgentGroup();
                tsql = "select branchlevel from labranchgroup where agentgroup='"+tAgentGroup+"'";
                tgrade = texesql.getOneValue(tsql);
                tRearRSchema.setRearLevel(tgrade);
                return addRear(tRearRSchema);
           }
        }
        return true;
    }

    public MMap getResult() {
        return map;
    }

    public LARearPICCHBL() {
    }

    public boolean addRear(LARearRelationSchema cLARearRScehma) {
        //将被推荐人的所在机构全部找出
//        mLABranchGroupSet.clear();
//        if (!findSelfBranch(tAgentCode, tAgentGroup)) {
//            return false;
//        }
        String tBranchLevel = cLARearRScehma.getRearLevel().trim();
        //按条件复制推荐人的所有推荐关系
        LARearRelationSet tLARearRelationSet = new LARearRelationSet();
        LARearRelationDB tLARearRelationDB = new LARearRelationDB();
        tLARearRelationDB.setAgentCode(cLARearRScehma.getRearAgentCode());
        tLARearRelationDB.setRearFlag("1");
        tLARearRelationSet = tLARearRelationDB.query();
        tLARearRelationSet.add(cLARearRScehma);
        for (int j = 1; j <= tLARearRelationSet.size(); j++) {
            LARearRelationSchema tLARearRelationSchema =
                    tLARearRelationSet.get(j);
            tLARearRelationSchema.setRearLevel(tBranchLevel);
            tLARearRelationSchema.setRearedGens(tLARearRelationSchema.
                                                getRearedGens() + 1);
            tLARearRelationSchema.setAgentCode(cLARearRScehma.getAgentCode());
            tLARearRelationSchema.setAgentGroup(cLARearRScehma.getAgentGroup());
            tLARearRelationSchema.setstartDate(currentDate);
            tLARearRelationSchema.setRearFlag("1");
            tLARearRelationSchema.setRearCommFlag("0");
            tLARearRelationSchema.setRearStartYear("1");
            tLARearRelationSchema.setMakeDate(currentDate);
            tLARearRelationSchema.setMakeTime(currentTime);
            tLARearRelationSchema.setModifyDate(currentDate);
            tLARearRelationSchema.setModifyTime(currentTime);
            tLARearRelationSchema.setOperator(mGlobalInput.Operator);
        }
        map.put(tLARearRelationSet, "INSERT");
        return true;
    }

    /**
     * 基本单元，失效一条直接育成关系（单人离职有向上/向下两条直接育成关系）
     * 及若干间接育成关系
     * @return boolean
     */
    public boolean invalidateRear(boolean Single,String cDate){
        if (mLATreeSchema==null){
            CError.buildErr(this,"没有传入业务人员信息!");
            return false;
        }
        String tAgentCode = mLATreeSchema.getAgentCode();
        String tAgentGroup = ""+mLATreeSchema.getAgentGroup();
        String ugrouppart = "";
        if(!tAgentGroup.equals("")&&!tAgentGroup.equals("null"))
            ugrouppart = ",agentgroup='"+tAgentGroup+"' ";
        String upsql = "update larearrelation set rearflag='0',enddate='"
                      +cDate+"'"+ugrouppart+" where agentcode= '"+tAgentCode+"'";
        map.put(upsql, "UPDATE");
        if(Single){
            //如果单人离职，还需要断向下的推荐关系
            String downsql =
                    "update larearrelation set rearflag='0',enddate='"
                    + cDate + "' where rearagentcode= '" + tAgentCode +
                    "'";
            map.put(downsql, "UPDATE");
        }else{
            //带走其下级关系后，则其下级与其上级推荐关系断裂
            String indsql = "update larearrelation set rearflag='0',enddate='"
                            +cDate+"' where rearagentcode in "
                            +"(select rearagentcode from larearrelation b "
                            +"where  b.agentcode='"+tAgentCode+"') "
                            +"and agentcode in (select agentcode from larearrelation b "
                            +"where  b.rearagentcode='"+tAgentCode+"')";
            map.put(indsql, "UPDATE");
        }
        return true;
    }

    /**
     * 基本单元，删除育成关系（单人离职有向上/向下两条直接育成关系）
     * 及若干间接育成关系
     * @return boolean
     */
    public boolean deleteRear(String tAgentCode,String tEdorNo,String tEdorType){
        if (tAgentCode==null){
            CError.buildErr(this,"没有传入业务人员信息!");
            return false;
        }
        LARearRelationBSet tLARearRelationBSet = new LARearRelationBSet();
        LARearRelationSet tLARearRelationSet = new LARearRelationSet();
        tLARearRelationSet= getRearRelation(tAgentCode);
        if(tLARearRelationSet==null || tLARearRelationSet.size()<=1)
        {
            return true ;
        }
        map.put(tLARearRelationSet, "DELETE");
        for (int i=1;i<=tLARearRelationSet.size();i++)
        {
            LARearRelationBSchema tLARearRelationBSchema = new LARearRelationBSchema();
            Reflections tReflections  = new Reflections();
            tReflections.transFields(tLARearRelationBSchema,tLARearRelationSet.get(i));
            tLARearRelationBSchema.setEdorNo(tEdorNo);
            tLARearRelationBSchema.setEdorType(tEdorType);
            tLARearRelationBSchema.setMakeDate2(tLARearRelationBSchema.getMakeDate());
            tLARearRelationBSchema.setMakeTime2(tLARearRelationBSchema.getMakeTime());
            tLARearRelationBSchema.setModifyDate2(tLARearRelationBSchema.getModifyDate());
            tLARearRelationBSchema.setModifyTime2(tLARearRelationBSchema.getModifyTime());
            tLARearRelationBSchema.setOperator2(tLARearRelationBSchema.getOperator());

            tLARearRelationBSchema.setOperator(mGlobalInput.Operator);
            tLARearRelationBSchema.setMakeDate(currentDate);
            tLARearRelationBSchema.setMakeTime(currentTime);
            tLARearRelationBSchema.setModifyDate(currentDate);
            tLARearRelationBSchema.setModifyTime(currentTime);
            tLARearRelationBSet.add(tLARearRelationBSchema);
        }
        map.put(tLARearRelationBSet, "INSERT");
        return true;
    }
    /**
     * 基本单元，查询育成关系（单人离职有向上/向下两条直接育成关系）
     * 及若干间接育成关系
     * @return boolean
     */
    public LARearRelationSet getRearRelation(String tAgentCode)
    {
        LARearRelationSet tgetLARearRelationSet = new LARearRelationSet();
        LARearRelationSet tLARearRelationSet = new LARearRelationSet();
        LARearRelationDB tLARearRelationDB = new LARearRelationDB();
        //查询本人为被育成人的推荐关系
        String upsql = "select  * from larearrelation  where agentcode= '" + tAgentCode +
                       "'";
       tLARearRelationSet=tLARearRelationDB.executeQuery(upsql);
       tgetLARearRelationSet.add(tLARearRelationSet);
        //查询本人为育成人的推荐关系
        String downsql =
                "select * from larearrelation   where rearagentcode= '" + tAgentCode + "'";
        tLARearRelationSet=tLARearRelationDB.executeQuery(downsql);
        tgetLARearRelationSet.add(tLARearRelationSet);

        // 查询被育成人为其下级与育成人为其上级的推荐关系
        String indsql = "select * from larearrelation  where rearagentcode in "
                        + "(select rearagentcode from larearrelation b "
                        + "where  b.agentcode='" + tAgentCode + "') "
                        +
                        "and agentcode in (select agentcode from larearrelation b "
                        + "where  b.rearagentcode='" + tAgentCode + "')";
        tLARearRelationSet=tLARearRelationDB.executeQuery(indsql);
        tgetLARearRelationSet.add(tLARearRelationSet);
        return tgetLARearRelationSet ;
    }




    /**
        * 直接育成关系（得到育成人为 tAgentCode的直接培养关系）
        * @return LARearRelationSet
        */
    public LARearRelationSet getDirectDownRelation(String tAgentCode)
    {
        LARearRelationSet tgetLARearRelationSet = new LARearRelationSet();
        LARearRelationDB tLARearRelationDB = new LARearRelationDB();
        //查询本人为育成人的直接培养关系
        String downsql =
                "select * from larearrelation   where rearagentcode= '" +
                tAgentCode + "' and rearedgens=1 ";
        tgetLARearRelationSet = tLARearRelationDB.executeQuery(downsql);
        return tgetLARearRelationSet;
    }
    /**
        * 间接育成关系（得到育成人为 tAgentCode的直接培养关系）
        * @return LARearRelationSet
    */
    public LARearRelationSet getinDirectDownRelation(String tAgentCode)
    {
        LARearRelationSet tgetLARearRelationSet = new LARearRelationSet();
        LARearRelationDB tLARearRelationDB = new LARearRelationDB();
        //查询本人为被育成人的直接培养关系
        String downsql =
                "select * from larearrelation   where Rearagentcode= '" +
                tAgentCode + "' and rearedgens>1 ";
        tgetLARearRelationSet = tLARearRelationDB.executeQuery(downsql);
        return tgetLARearRelationSet;
   }

    /**
           * 直接育成关系（得到被育成人为 tAgentCode的直接培养关系）
           * @return LARearRelationSet
   */

   public LARearRelationSet getDirectUpRelation(String tAgentCode)
   {
       LARearRelationSet tgetLARearRelationSet = new LARearRelationSet();
       LARearRelationDB tLARearRelationDB = new LARearRelationDB();
       //查询本人为被育成人的直接培养关系
       String downsql =
               "select * from larearrelation   where agentcode= '" +
               tAgentCode + "' and rearedgens=1  and rearflag='1' ";
       tgetLARearRelationSet = tLARearRelationDB.executeQuery(downsql);
       return tgetLARearRelationSet;
   }
   /**
           * 间接育成关系（得到被育成人为 tAgentCode的间接培养关系）
           * @return LARearRelationSet
   */

   public LARearRelationSet getinDirectUpRelation(String tAgentCode)
   {
       LARearRelationSet tgetLARearRelationSet = new LARearRelationSet();
       LARearRelationDB tLARearRelationDB = new LARearRelationDB();
       //查询本人为被育成人的间接培养关系
       String downsql =
               "select * from larearrelation   where agentcode= '" +
               tAgentCode + "' and rearedgens > 1  and rearflag='1' ";
       tgetLARearRelationSet = tLARearRelationDB.executeQuery(downsql);
       return tgetLARearRelationSet;
   }
   /**
           * 间接育成关系（得到 与 tAgentCode有关的间接培养关系,tAgentCode不是培养人,也不是被培养人）
           * @return LARearRelationSet
   */

   public LARearRelationSet getAsRelation(String tAgentCode)
   {
       LARearRelationSet tgetLARearRelationSet = new LARearRelationSet();
       LARearRelationDB tLARearRelationDB = new LARearRelationDB();
       //查询本人为被育成人的间接培养关系
       String downsql = "select * from larearrelation  where rearagentcode in "
                + "(select rearagentcode from larearrelation b "
                + "where  b.agentcode='" + tAgentCode + "') "
                +
                "and agentcode in (select agentcode from larearrelation b "
                + "where  b.rearagentcode='" + tAgentCode + "') and rearedgens > 1  ";

       tgetLARearRelationSet = tLARearRelationDB.executeQuery(downsql);
       return tgetLARearRelationSet;
   }
   /**
           * 育成关系（直接和间接,得到被育成人为 tAgentCode的培养关系）
           * @return LARearRelationSet
   */

   public LARearRelationSet getUpRelation(String tAgentCode)
   {
       LARearRelationSet tgetLARearRelationSet = new LARearRelationSet();
       LARearRelationDB tLARearRelationDB = new LARearRelationDB();
       //查询本人为被育成人的培养关系
       String downsql =  "select * from larearrelation   where agentcode= '" +
              tAgentCode + "' ";
       tgetLARearRelationSet = tLARearRelationDB.executeQuery(downsql);
       return tgetLARearRelationSet;
   }
   /**
           * 育成关系（直接和间接,得到育成人为 tAgentCode的培养关系）
           * @return LARearRelationSet
   */

   public LARearRelationSet getDownRelation(String tAgentCode)
   {
       LARearRelationSet tgetLARearRelationSet = new LARearRelationSet();
       LARearRelationDB tLARearRelationDB = new LARearRelationDB();
       //查询本人为被育成人的培养关系
       String downsql =  "select * from larearrelation   where rearagentcode= '" +
              tAgentCode + "' ";
       tgetLARearRelationSet = tLARearRelationDB.executeQuery(downsql);
       return tgetLARearRelationSet;
   }

    public boolean ModifyRear() {
        //查找出AgentCode为被育成人的信息，删除
        String tAgentCode = mLAAgentSchema.getAgentCode();
        LARearRelationSet tLARearRelationSet = new LARearRelationSet();
        LARearRelationDB tLARearRelationDB = new LARearRelationDB();
        tLARearRelationDB.setAgentCode(tAgentCode);
        tLARearRelationSet = tLARearRelationDB.query();
        if (tLARearRelationDB.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LARearPublic";
            tError.functionName = "ModifyRear";
            tError.errorMessage = "查询育成信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        map.put(tLARearRelationSet, "DELETE");
        //转储
        if (!edorRear(tLARearRelationSet)) {
            return false;
        }
        //生成新的育成关系
        if (!addRear(tLARearRelationSet.get(1))) {
            return false;
        }
        //查找出RearAgentCode为被育成人的信息
        tLARearRelationSet = new LARearRelationSet();
        tLARearRelationDB = new LARearRelationDB();
        tLARearRelationDB.setRearAgentCode(tAgentCode);
        tLARearRelationSet = tLARearRelationDB.query();
        if (tLARearRelationDB.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LARearPublic";
            tError.functionName = "ModifyRear";
            tError.errorMessage = "查询育成信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //循环处理每条信息
        String tRearLevel = "";
        LARearRelationSchema tLARearRelationSchema;
        LARearRelationSet tLARearRelationSet1;
        LARearRelationDB tLARearRelationDB1;
        for (int i = 1; i <= tLARearRelationSet.size(); i++) {
            tAgentCode = "";
            tLARearRelationSchema = new
                                    LARearRelationSchema();
            tLARearRelationSchema = tLARearRelationSet.get(i);
            tRearLevel = tLARearRelationSchema.getRearLevel();
            tAgentCode = tLARearRelationSchema.getAgentCode();
            tLARearRelationSet1 = new LARearRelationSet();
            tLARearRelationDB1 = new LARearRelationDB();
            tLARearRelationDB1.setAgentCode(tAgentCode);
            tLARearRelationDB1.setRearLevel(tRearLevel);
            tLARearRelationSet1 = tLARearRelationDB1.query();
            if (tLARearRelationDB1.mErrors.needDealError()) {
                CError tError = new CError();
                tError.moduleName = "LARearPublic";
                tError.functionName = "ModifyRear";
                tError.errorMessage = "查询育成信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            map.put(tLARearRelationSet1, "DELETE");
            //转储
            if (!edorRear(tLARearRelationSet)) {
                return false;
            }
            //生成新的育成关系
            LAAgentSchema tLAAgentSchema = getLAAgentSchema(tAgentCode);
            LATreeSchema tLATreeSchema = getLATreeSchema(tAgentCode);
            if (tLAAgentSchema == null || tLATreeSchema == null) {
                CError tError = new CError();
                tError.moduleName = "LARearPublic";
                tError.functionName = "ModifyRear";
                tError.errorMessage = "查询代理人信息(育成)失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mLABranchGroupSet.clear();
            if (!findSelfBranch(tAgentCode, tLAAgentSchema.getAgentGroup().trim())) {
                return false;
            }
            String tRearGroup = "";
            for (int j = 1; j <= mLABranchGroupSet.size(); j++) {
                LABranchGroupSchema tLABranchGroupSchema = new
                        LABranchGroupSchema();
                tLABranchGroupSchema = mLABranchGroupSet.get(j);
                if (tLABranchGroupSchema.getBranchLevel().trim().equals(
                        tRearLevel)) {
                    tRearGroup = tLABranchGroupSchema.getAgentGroup();
                }
            }
            for (int j = 1; j <= mNewAddLARearRelationSet.size(); j++) {
                LARearRelationSchema tLARearRelationSchema1 = new
                        LARearRelationSchema();
                tLARearRelationSchema1 = tLARearRelationSet.get(j);
                tLARearRelationSchema1.setRearLevel(tRearLevel);
                tLARearRelationSchema1.setRearedGens(tLARearRelationSchema.
                        getRearedGens() + tLARearRelationSchema1.getRearLevel());
                tLARearRelationSchema1.setAgentCode(tAgentCode);
                tLARearRelationSchema1.setRearAgentCode(tLARearRelationSchema1.
                        getAgentCode());
                tLARearRelationSchema1.setAgentGroup(tRearGroup);
                tLARearRelationSchema1.setstartDate(tLATreeSchema.getStartDate());
                tLARearRelationSchema1.setRearFlag("1");
                //EndDate
                tLARearRelationSchema1.setRearCommFlag("1");
                tLARearRelationSchema1.setRearStartYear("1");
                tLARearRelationSchema1.setMakeDate(currentDate);
                tLARearRelationSchema1.setMakeTime(currentTime);
                tLARearRelationSchema1.setModifyDate(currentDate);
                tLARearRelationSchema1.setModifyTime(currentTime);
                tLARearRelationSchema1.setOperator(mGlobalInput.Operator);
                map.put(tLARearRelationSchema1, "INSERT");
            }
        }
        return true;
    }

    public boolean edorRear(LARearRelationSet cLARearRelationSet) {
        Reflections tReflections;
        LARearRelationSchema tLARearRelationSchema;
        LARearRelationBSchema tLARearRelationBSchema;
        for (int i = 1; i <= cLARearRelationSet.size(); i++) {
            tLARearRelationSchema = new
                                    LARearRelationSchema();
            tLARearRelationBSchema = new
                                     LARearRelationBSchema();
            tLARearRelationSchema = cLARearRelationSet.get(i);
            tReflections = new Reflections();
            tReflections.transFields(tLARearRelationBSchema,
                                     tLARearRelationSchema);
            tLARearRelationBSchema.setEdorNo(mEdorNo);
            tLARearRelationBSchema.setEdorType("06");
            tLARearRelationBSchema.setMakeDate(currentDate);
            tLARearRelationBSchema.setMakeTime(currentTime);
            tLARearRelationBSchema.setModifyDate(currentDate);
            tLARearRelationBSchema.setModifyTime(currentTime);
            tLARearRelationBSchema.setOperator(this.mGlobalInput.Operator);
            map.put(tLARearRelationBSchema, "INSERT");
        }
        return true;
    }
    //

    public LAAgentSchema getLAAgentSchema(String cAgentCode) {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(cAgentCode);
        if (!tLAAgentDB.getInfo()) {
            CError tError = new CError();
            tError.moduleName = "LARearPublic";
            tError.functionName = "ModifyRear";
            tError.errorMessage = "查询代理人基本信息失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLAAgentDB.getSchema();
    }

    public LATreeSchema getLATreeSchema(String cAgentCode) {
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(cAgentCode);
        if (!tLATreeDB.getInfo()) {
            CError tError = new CError();
            tError.moduleName = "LARearPublic";
            tError.functionName = "ModifyRear";
            tError.errorMessage = "查询代理人行政信息失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLATreeDB.getSchema();
    }

    public boolean findSelfBranch(String cAgentCode, String cAgentGroup) {
        //所在各级直辖机构,非最高级
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setBranchManager(cAgentCode);
        tLABranchGroupDB.setUpBranchAttr("1");
        mLABranchGroupSet = tLABranchGroupDB.query();
        if (!tLABranchGroupDB.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LARearPublic";
            tError.functionName = "findSelfBranch";
            tError.errorMessage = "查询机构信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //所在最高级机构
        tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(cAgentGroup);
        if (!tLABranchGroupDB.getInfo()) {
            CError tError = new CError();
            tError.moduleName = "LARearPublic";
            tError.functionName = "findSelfBranch";
            tError.errorMessage = "查询机构信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLABranchGroupSet.set(mLABranchGroupSet.size() + 1,
                              tLABranchGroupDB.getSchema());
        return true;
    }
}
