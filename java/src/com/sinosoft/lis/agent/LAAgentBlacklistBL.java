package com.sinosoft.lis.agent;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

import com.sinosoft.lis.db.LAAgentBlacklistDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBlacklistSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAAgentBlacklistBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAAgentBlacklistSchema mLAAgentBlacklistSchema = new
            LAAgentBlacklistSchema();
    private String tCount = "";
    public LAAgentBlacklistBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentBlacklistBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAAgentBlacklistBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("over dealData");
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        System.out.println("Start LAAgentBlacklistBL Submit...");
        LAAgentBlacklistBLS tLAAgentBlacklistBLS = new LAAgentBlacklistBLS();
        tLAAgentBlacklistBLS.submitData(mInputData, cOperate);
        System.out.println("End LAAgentBlacklistBL Submit...");
        //如果有需要处理的错误，则返回
        if (tLAAgentBlacklistBLS.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAAgentBlacklistBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentBlacklistBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        this.mResult.add(this.mLAAgentBlacklistSchema);
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            tCount = PubFun1.CreateMaxNo("BlackListCode", 10);
            System.out.println("tCount:" + tCount);
            this.mLAAgentBlacklistSchema.setBlackListCode(tCount);
            this.mLAAgentBlacklistSchema.setOperator(mGlobalInput.Operator);
            this.mLAAgentBlacklistSchema.setMakeDate(currentDate);
            this.mLAAgentBlacklistSchema.setMakeTime(currentTime);
            this.mLAAgentBlacklistSchema.setModifyDate(currentDate);
            this.mLAAgentBlacklistSchema.setModifyTime(currentTime);
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            String tBlackListCode = "";
            tBlackListCode = this.mLAAgentBlacklistSchema.getBlackListCode();
            LAAgentBlacklistDB tLAAgentBlacklistDB = new LAAgentBlacklistDB();
            tLAAgentBlacklistDB.setBlackListCode(tBlackListCode);
            if (!tLAAgentBlacklistDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentBlacklistDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentBlacklistBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }

            tLAAgentBlacklistDB.setName(this.mLAAgentBlacklistSchema.getName());
            tLAAgentBlacklistDB.setSex(this.mLAAgentBlacklistSchema.getSex());
            tLAAgentBlacklistDB.setBirthday(this.mLAAgentBlacklistSchema.
                                            getBirthday());
            tLAAgentBlacklistDB.setIDNo(this.mLAAgentBlacklistSchema.getIDNo());
            tLAAgentBlacklistDB.setPostalAddress(this.mLAAgentBlacklistSchema.
                                                 getPostalAddress());
            tLAAgentBlacklistDB.setZipCode(this.mLAAgentBlacklistSchema.
                                           getZipCode());
            tLAAgentBlacklistDB.setSource(this.mLAAgentBlacklistSchema.
                                          getSource());
            tLAAgentBlacklistDB.setWorkAge(this.mLAAgentBlacklistSchema.
                                           getWorkAge());
            tLAAgentBlacklistDB.setInsurerCompany(this.mLAAgentBlacklistSchema.
                                                  getInsurerCompany());
            tLAAgentBlacklistDB.setHeadShip(this.mLAAgentBlacklistSchema.
                                            getHeadShip());
            tLAAgentBlacklistDB.setBusiness(this.mLAAgentBlacklistSchema.
                                            getBusiness());
            tLAAgentBlacklistDB.setBlacklistReason(this.mLAAgentBlacklistSchema.
                    getBlacklistReason());
            tLAAgentBlacklistDB.setPhone(this.mLAAgentBlacklistSchema.getPhone());
            this.mLAAgentBlacklistSchema.setSchema(tLAAgentBlacklistDB);
            this.mLAAgentBlacklistSchema.setModifyTime(currentTime);
            this.mLAAgentBlacklistSchema.setModifyDate(currentDate);
            this.mLAAgentBlacklistSchema.setOperator(mGlobalInput.Operator);
        }
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mLAAgentBlacklistSchema.setSchema((LAAgentBlacklistSchema)
                                               cInputData.getObjectByObjectName(
                "LAAgentBlacklistSchema", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLAAgentBlacklistSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentBlacklistBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public String getResult()
    {
        return this.tCount;
    }
}
