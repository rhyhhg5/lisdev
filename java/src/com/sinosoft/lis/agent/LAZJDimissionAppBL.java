/*
 * <p>ClassName: LAZJDimissionAppBL </p>
 * <p>Description: LAZJDimissionAppBL类文件 </p>
 * <p>Copyright: Copyright (c) 2005.3</p>
 * <p>Company: sinosoft </p>
 * @author zhanghui
 * @Database: 销售管理
 * @CreateDate：2005-03-18
 */
package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LADimissionDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.lis.vschema.LADimissionSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;

public class LAZJDimissionAppBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private MMap mMap = new MMap();
    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LADimissionSchema mLADimissionSchema = new LADimissionSchema();
    private LADimissionSet mLADimissionSet = new LADimissionSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    public LAZJDimissionAppBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAZJDimissionAppBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAZJDimissionAppBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start LAZJDimissionAppBLS Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            tPubSubmit.submitData(mInputData, "");
            //如果有需要处理的错误，则返回
            if (tPubSubmit.mErrors.needDealError()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError.buildErr(this, "数据提交失败!");
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        if (mOperate.equals("INSERT||MAIN"))
        {
            //操作员
            //在LADimission表里插入离职状态信息
            if (!checkInsert())
            {
                return false;
             }
             if (this.mLADimissionSchema.getDepartTimes() == 1)
            {
                this.mLADimissionSchema.setDepartState("03"); //离职未确认
            }
            else
            {
                 this.mLADimissionSchema.setDepartState("04"); //二次离职未确认
            }

            mLADimissionSchema.setBranchAttr(AgentPubFun.getAgentBranchAttr(
                    mLADimissionSchema.getAgentCode())); //转换机构编码
            mLADimissionSchema.setMakeDate(currentDate);
            mLADimissionSchema.setMakeTime(currentTime);
            mLADimissionSchema.setModifyDate(currentDate);
            mLADimissionSchema.setModifyTime(currentTime);
            mLADimissionSchema.setOperator(mGlobalInput.Operator);
        }
        if (mOperate.equals("UPDATE||MAIN"))
        {
            LADimissionDB tLADimissionDB = new LADimissionDB();
            tLADimissionDB.setAgentCode(mLADimissionSchema.getAgentCode());
            tLADimissionDB.setDepartTimes(mLADimissionSchema.getDepartTimes());
            if (!tLADimissionDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "LAZJDimissionAppBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询代理人原离职信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mLADimissionSchema.setBranchAttr(AgentPubFun.
                                                  getAgentBranchAttr(
                    mLADimissionSchema.getAgentCode()));

            this.mLADimissionSchema.setModifyDate(currentDate);
            this.mLADimissionSchema.setModifyTime(currentTime);
            mLADimissionSchema.setOperator(mGlobalInput.Operator);
        }
        //修改代理人信息表中的代理人状态字段和离职日期
        if (this.mOperate.equals("INSERT||MAIN") ||
            this.mOperate.equals("UPDATE||MAIN"))
        {
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(this.mLADimissionSchema.getAgentCode());
            if (!tLAAgentDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAZJDimissionAppBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询代理人信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //tLAAgentDB.setOutWorkDate(this.mLADimissionSchema.getDepartDate());
            System.out.println(this.mLADimissionSchema.getDepartDate());
            if (this.mLADimissionSchema.getDepartTimes() == 1)
            {
                tLAAgentDB.setAgentState("03"); //离职未确认

            }
            else
            {
                tLAAgentDB.setAgentState("04"); //二次离职未确认

            }
            tLAAgentDB.setOperator(this.mGlobalInput.Operator);
            tLAAgentDB.setModifyDate(currentDate);
            tLAAgentDB.setModifyTime(currentTime);
            this.mLAAgentSchema.setSchema(tLAAgentDB);
        }
        //缺少主管离职的处理
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLADimissionSchema.setSchema((LADimissionSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LADimissionSchema",
                                                  0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start LAZJDimissionAppBLQuery Submit...");
        LADimissionDB tLADimissionDB = new LADimissionDB();
        tLADimissionDB.setSchema(this.mLADimissionSchema);
        this.mLADimissionSet = tLADimissionDB.query();
        this.mResult.add(this.mLADimissionSet);
        System.out.println("End LAZJDimissionAppBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLADimissionDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLADimissionDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAZJDimissionAppBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mMap.put(this.mLADimissionSchema,"INSERT");
            this.mMap.put(this.mLAAgentSchema,"UPDATE");
            this.mInputData.add(mMap);

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAZJDimissionAppBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    /**
     * 校验函数
     * 对传入的数据进行信息校验
     * @return boolean
     */
    private boolean checkData()
    {
        int tCount;
        String tAgentCode;
        tAgentCode = mLADimissionSchema.getAgentCode(); //得到离职人员编码

        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        //查询代理人表
        tCount = tLAAgentDB.getCount();
        System.out.println("tCount:" + Integer.toString(tCount));
        //对查询结果判定
        if (tCount == -1)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAZJDimissionAppBL";
            tError.functionName = "checkData()";
            tError.errorMessage = "操作失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else if (tCount == 0)
        {
            //代理人编码录入错误
            CError tError = new CError();
            tError.moduleName = "LAZJDimissionAppBL";
            tError.functionName = "checkData()";
            tError.errorMessage = "没有这个代理人!";
            this.mErrors.addOneError(tError);
            return false;
        }
      return true;
    }
    private boolean checkInsert()
    {
        String tAgentCode;
        tAgentCode = mLADimissionSchema.getAgentCode(); //得到离职人员编码
        LADimissionDB tLADimissionDB=new LADimissionDB();
        LADimissionSet tLADimissionSet=new LADimissionSet();
        tLADimissionDB.setAgentCode(tAgentCode);
        tLADimissionSet=tLADimissionDB.query();
        if(tLADimissionSet.size()>0)
        {
            for(int i=1;i<=tLADimissionSet.size();i++)
            {
                LADimissionSchema tLADimissionSchema=new LADimissionSchema();
                tLADimissionSchema=tLADimissionSet.get(i);
                if(tLADimissionSchema.getDepartState().equals("03")||tLADimissionSchema.getDepartState().equals("04"))
                {
                    CError tError = new CError();
                    tError.moduleName = "ALADimissionBL";
                    tError.functionName = "checkInsert()";
                    tError.errorMessage = "此代理人已离职登记，不能再进行离职！";
                    this.mErrors.addOneError(tError);
                     return false;
                }
            }
        }
        return true;
    }
}
