package com.sinosoft.lis.agent;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.db.LAAgentBlacklistDB;
import com.sinosoft.lis.schema.LAAgentBlacklistSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

public class LAAgentBlacklistBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    public LAAgentBlacklistBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        System.out.println("Start LAAgentBlacklistBLS Submit...");
        System.out.println(mOperate);
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            tReturn = saveLAAgentBlacklist(cInputData);
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            tReturn = deleteLAAgentBlacklist(cInputData);
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            tReturn = updateLAAgentBlacklist(cInputData);
        }
        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End LAAgentBlacklistBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean saveLAAgentBlacklist(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentBlacklistBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 代理人信息保存...");
            LAAgentBlacklistDB tLAAgentBlacklistDB = new LAAgentBlacklistDB(
                    conn);
            tLAAgentBlacklistDB.setSchema((LAAgentBlacklistSchema) mInputData.
                                          getObjectByObjectName(
                                                  "LAAgentBlacklistSchema", 0));
            if (!tLAAgentBlacklistDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentBlacklistDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentBlacklistBLS";
                tError.functionName = "saveAgentData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            System.out.println("end 代理人信息保存...");
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentBlacklistBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean deleteLAAgentBlacklist(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Delete...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentBlacklistBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 删除代理人信息...");
            LAAgentBlacklistDB tLAAgentBlacklistDB = new LAAgentBlacklistDB(
                    conn);
            LAAgentBlacklistSchema tLAAgentBlacklistSchema = new
                    LAAgentBlacklistSchema();
            tLAAgentBlacklistSchema = (LAAgentBlacklistSchema) mInputData.
                                      getObjectByObjectName(
                                              "LAAgentBlacklistSchema", 0);
            tLAAgentBlacklistDB.setSchema(tLAAgentBlacklistSchema);
            if (!tLAAgentBlacklistDB.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentBlacklistDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentBlacklistBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "代理人数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            System.out.println("end 代理人信息删除...");
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentBlacklistBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean updateLAAgentBlacklist(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentBlacklistBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 代理人信息保存...");
            LAAgentBlacklistDB tLAAgentBlacklistDB = new LAAgentBlacklistDB(
                    conn);
            LAAgentBlacklistSchema tLAAgentBlacklistSchema = (
                    LAAgentBlacklistSchema) mInputData.getObjectByObjectName(
                            "LAAgentBlacklistSchema", 0);
            tLAAgentBlacklistDB.setSchema(tLAAgentBlacklistSchema);
            if (!tLAAgentBlacklistDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentBlacklistDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentBlacklistBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            System.out.println("Start 行政信息保存...");
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentBlacklistBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }
}
