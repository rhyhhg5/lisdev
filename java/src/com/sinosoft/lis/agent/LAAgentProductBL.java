package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAgentGradeDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LAWarrantorDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentGradeSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAWarrantorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.vschema.LABranchGroupSet;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public class LAAgentProductBL implements LAAgentInterface
{
    MMap map = new MMap();
    String mOperate = new String();
    GlobalInput mGlobalInput = new GlobalInput();
    LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    LAAgentBSchema mLAAgentBSchema = new LAAgentBSchema();
    LATreeSchema mLATreeSchema = new LATreeSchema();
    LAWarrantorSet mLAWarrantorSet = new LAWarrantorSet();
    String mAgentCode = new String();
    String mBranchCode = new String();
    String mAgentGroup = new String();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String originalDate = "";
    private String originalTime = "";
    String mEdorNo = new String();

    public void setOperate(String cOperate)
    {
        mOperate = cOperate;
    }

    public void setGlobalInput(GlobalInput cGlobalInput)
    {
        mGlobalInput.setSchema(cGlobalInput);
    }

    public void setLAAgentSchema(LAAgentSchema cLAAgentSchema)
    {
        mLAAgentSchema.setSchema(cLAAgentSchema);
    }

    public LAAgentSchema getLAAgentSchema()
    {
        return mLAAgentSchema;
    }

    public void setLATreeSchema(LATreeSchema cLATreeSchema)
    {
        mLATreeSchema.setSchema(cLATreeSchema);
    }

    public void setLAWarrantorSet(LAWarrantorSet cLAWarrantorSet)
    {
        mLAWarrantorSet.set(cLAWarrantorSet);
    }

    public void setAgentCode(String cAgentCode)
    {
        mAgentCode = cAgentCode;
    }

    public MMap getResult()
    {
        return map;
    }

    public void setEdorNo(String cEdorNo)
    {
        mEdorNo = cEdorNo;
    }

    public LAAgentProductBL()
    {
    }

    public static void main(String[] args)
    {
        LAAgentProductBL laagentproductbl = new LAAgentProductBL();
    }

    public boolean dealdata()
    {
        //如果是修改操作，备份人员基本信息
        if (this.mOperate.indexOf("UPDATE") != -1)
        {
            //修改操作使用原代理人编码
            mAgentCode = this.mLAAgentSchema.getAgentCode().trim();
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(mAgentCode);
            if (!tLAAgentDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAAgentBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "查询原代理人信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //备份原代理人信息
            Reflections tReflections = new Reflections();
            tReflections.transFields(this.mLAAgentBSchema, tLAAgentDB.getSchema());
            this.mLAAgentBSchema.setEdorNo(mEdorNo);
            this.mLAAgentBSchema.setEdorType("05");
            this.mLAAgentBSchema.setMakeDate(currentDate);
            this.mLAAgentBSchema.setMakeTime(currentTime);
            this.mLAAgentBSchema.setModifyDate(currentDate);
            this.mLAAgentBSchema.setModifyTime(currentTime);
            this.mLAAgentBSchema.setOperator(this.mGlobalInput.Operator);
            map.put(mLAAgentBSchema, "INSERT");
            originalDate = tLAAgentDB.getMakeDate();
            originalTime = tLAAgentDB.getMakeTime();
            //清空管理信息
        }
        if (this.mOperate.equals("UPDATE||PART"))
        {
            if (!setInDueFormDate())
            {
                return false;
            }
            //this.mLAAgentSchema.setAgentGroup(tLAAgentDB.getAgentGroup());

            this.mLAAgentSchema.setModifyDate(currentDate);
            this.mLAAgentSchema.setModifyTime(currentTime);
            this.mLAAgentSchema.setOperator(mGlobalInput.Operator);
            map.put(mLAAgentSchema, "UPDATE");
        }
        if (this.mOperate.equals("INSERT||MAIN") ||
            this.mOperate.equals("UPDATE||MAIN") ||
            this.mOperate.equals("UPDATE||ALL"))
        {
            //代理人基本信息
            System.out.println("11+++++11" + mAgentCode);
            this.mLAAgentSchema.setAgentCode(mAgentCode);
            System.out.println("111ddd111" + this.mLAAgentSchema.getAgentCode());
            //agentgroup branchcode
            String tAgentGroup = this.mLAAgentSchema.getAgentGroup().trim();
            System.out.println("+++++++++++++++++++++++" + tAgentGroup);
            //确定销售机构AgentGroup,从前台传入的是外部编码
            String SQL =
                    "select agentgroup from labranchgroup where branchattr='" +
                    tAgentGroup + "' and branchtype = '" +
                    this.mLAAgentSchema.getBranchType().trim() +
                    "' and branchtype2 = '" +
                    this.mLAAgentSchema.getBranchType2().trim() + "'";
            ExeSQL tExeSQL = new ExeSQL();
            System.out.println(SQL);
            tAgentGroup = tExeSQL.getOneValue(SQL);
            System.out.println("+++++++++++++++++++++++" + tAgentGroup);
            if (tAgentGroup == null || tAgentGroup.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "ALABankAgentBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询机构信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tAgentGroup = tAgentGroup.trim();
            //BrachCode应该置为他所在的最低机构里，AgentGroup为主管所在最高机构
            //查找机构输入属性
            String tSQL =
                    "select VarValue from LASysVar where VarType = 'branch' ";
            ExeSQL tExeSQL2 = new ExeSQL();
            String branchinput = tExeSQL2.getOneValue(tSQL);
            if (branchinput == null)
            {
                branchinput = "";
            }
            branchinput = branchinput.trim();
            System.out.println("+++++++++++++++++++++++" + tAgentGroup);
            if (branchinput.equals("0")) //0表示机构输入最高级,1表示机构输入最低级
            {
                if (!setBranchHigh(tAgentGroup))
                {
                    return false;
                }
            }
            else
            {
                if (!setBranchLow(tAgentGroup))
                {
                    return false;
                }
            }
            System.out.println("++++++++666++++++++++");
            if (this.mLAAgentSchema.getEmployDate() == null ||
                this.mLAAgentSchema.getEmployDate().equals(""))
            {
                this.mLAAgentSchema.setEmployDate(currentDate);
            }
            if (!setInDueFormDate())
            {
                return false;
            }
            this.mLAAgentSchema.setOperator(mGlobalInput.Operator);
            if (this.mOperate.indexOf("UPDATE") != -1)
            {
                this.mLAAgentSchema.setMakeDate(originalDate);
                this.mLAAgentSchema.setMakeTime(originalTime);
            }
            else
            {
                this.mLAAgentSchema.setMakeDate(currentDate);
                this.mLAAgentSchema.setMakeTime(currentTime);
            }
            this.mLAAgentSchema.setModifyDate(currentDate);
            this.mLAAgentSchema.setModifyTime(currentTime);
            //放入MAP中
            System.out.println("111111111" + this.mLAAgentSchema.getAgentCode());
            if (this.mOperate.equals("INSERT||MAIN"))
            {
                map.put(this.mLAAgentSchema, "INSERT");
            }
            else if (this.mOperate.indexOf("UPDATE") != -1)
            {
                map.put(this.mLAAgentSchema, "UPDATE");
            }
            //查出原有担保人信息删除
            LAWarrantorDB tLAWarrantorDB = new LAWarrantorDB();
            LAWarrantorSet tLAWarrantorSet = new LAWarrantorSet();
            tLAWarrantorDB.setAgentCode(mAgentCode);
            tLAWarrantorSet = tLAWarrantorDB.query();
            map.put(tLAWarrantorSet, "DELETE");
            //添加新担保人信息表
            for (int i = 1; i <= this.mLAWarrantorSet.size(); i++)
            {
                if (this.mOperate.equals("INSERT||MAIN"))
                {
                    this.mLAWarrantorSet.get(i).setAgentCode(mAgentCode);
                }
                this.mLAWarrantorSet.get(i).setSerialNo(i);
                this.mLAWarrantorSet.get(i).setOperator(mGlobalInput.Operator);
                this.mLAWarrantorSet.get(i).setMakeDate(currentDate);
                this.mLAWarrantorSet.get(i).setMakeTime(currentTime);
                this.mLAWarrantorSet.get(i).setModifyDate(currentDate);
                this.mLAWarrantorSet.get(i).setModifyTime(currentTime);
            }
            map.put(mLAWarrantorSet, "INSERT");
        }
        return true;
    }

    /**
     * setInDueFormDate
     *
     * @return boolean
     */
    private boolean setInDueFormDate()
    {
        //蔡刚添加限制，如果已经转正，则不能重新置转正日期
        if (mLAAgentSchema.getInDueFormDate() !=null && !mLAAgentSchema.getInDueFormDate() .trim() .equals("") )
        {
            return true;
        }
        String tAgentGrade = this.mLATreeSchema.getAgentGrade();
        String tAgentKind = this.mLATreeSchema.getAgentKind();
        String tAgentLine = this.mLATreeSchema.getAgentLine();
        if (tAgentGrade == null)
        {
            tAgentGrade = "";
        }
        if (tAgentKind == null)
        {
            tAgentKind = "";
        }
        if (tAgentLine == null)
        {
            tAgentLine = "";
        }
        if ((tAgentGrade.equals("") && tAgentKind.equals("")) ||
            tAgentLine.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentProductBL";
            tError.functionName = "setInDueFormDate";
            tError.errorMessage = "查询不到此代理人的级别信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LAAgentGradeDB tLAAgentGradeDB = new LAAgentGradeDB();
        //注意这里AgentLine写成B，是死程序,cg负责此处问题
        if (tAgentLine.trim().equals("B"))
        {

            tLAAgentGradeDB.setGradeCode(tAgentKind.trim());
        }
        else
        {

            tLAAgentGradeDB.setGradeCode(tAgentGrade.trim());
        }
        if (!tLAAgentGradeDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentProductBL";
            tError.functionName = "setInDueFormDate";
            tError.errorMessage = "查询不到" + tAgentGrade + "的级别信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LAAgentGradeSchema tLAAgentGradeSchema = new LAAgentGradeSchema();
        tLAAgentGradeSchema = tLAAgentGradeDB.getSchema();
        //InDueFormDate
        String isNormal = tLAAgentGradeSchema.getGradeProperty4();
        if (isNormal == null || isNormal.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentProductBL";
            tError.functionName = "setInDueFormDate";
            tError.errorMessage = "查询" + tAgentGrade + "的级别信息错误!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (isNormal.trim().equals("1"))
        {
            this.mLAAgentSchema.setInDueFormDate(this.mLAAgentSchema.
                                                 getEmployDate());
        }
        return true;
    }


    /**
     * setBranchDESC
     * 设置AgentGroup,BranchCode
     * @return boolean
     */
    private boolean setBranchHigh(String cBranch)
    {
        String tAgentGroup = cBranch;
        this.mLAAgentSchema.setAgentGroup(tAgentGroup);
        //递归查询最低级的下级直辖机构
        String tBranchCode = getDirectDownBranch(cBranch);
        this.mLAAgentSchema.setBranchCode(tBranchCode);
        return false;
    }

    /**
     * setBranchASC
     * 设置AgentGroup,BranchCode
     * @return boolean
     */
    private boolean setBranchLow(String cBranch)
    {
        String tBranchCode = cBranch;
        this.mLAAgentSchema.setBranchCode(tBranchCode);
        //递归查询最高级的上级直辖机构
        String tAgentGroup = getDirectUpBranch(cBranch);
        if (tAgentGroup == null)
        {
            return false;
        }
        this.mLAAgentSchema.setAgentGroup(tAgentGroup);
        return true;
    }

    private String getDirectUpBranch(String cBranch)
    {
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(cBranch);
        if (!tLABranchGroupDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentProductBL";
            tError.functionName = "getDirectUpBranch";
            tError.errorMessage = "查询不到" + cBranch + "的机构信息!";
            this.mErrors.addOneError(tError);
            return null;
        }
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = tLABranchGroupDB.getSchema();
        String tUpBranchAttr = tLABranchGroupSchema.getUpBranchAttr();
        if (tUpBranchAttr == null || tUpBranchAttr.equals("") ||
            tUpBranchAttr.trim().equals("0"))
        {
            return cBranch;
        }
        else
        {
            //如果取上级机构出错，那就是机构建立时出错导致
            return getDirectUpBranch(tLABranchGroupSchema.getUpBranch().trim());
        }
    }


        private String getDirectDownBranch(String cBranch)
   {
       LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
       LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
       tLABranchGroupDB.setUpBranch(cBranch);
       tLABranchGroupDB.setUpBranchAttr("1");
       //没有找到下级直辖机构，返回本身
       tLABranchGroupSet = tLABranchGroupDB.query();
       if (tLABranchGroupDB.mErrors.needDealError())
       {
           return null;
       }
       else
       {
           if(tLABranchGroupSet.size() == 0)
           {
               return cBranch;
           }
           else
           {
               LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
               tLABranchGroupSchema = tLABranchGroupSet.get(1);
               return getDirectDownBranch(tLABranchGroupSchema.getAgentGroup().trim());
           }
       }


    }
}
