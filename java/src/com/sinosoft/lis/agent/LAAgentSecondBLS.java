/*
 * <p>ClassName: LAAgentSecondBLS </p>
 * <p>Description: LAAgentSecondBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-09
 */
package com.sinosoft.lis.agent;

import java.sql.Connection;

import com.sinosoft.lis.db.LAAgentBDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LATreeBDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vdb.LABranchGroupDBSet;
import com.sinosoft.lis.vdb.LATreeAccessoryBDBSet;
import com.sinosoft.lis.vdb.LATreeAccessoryDBSet;
import com.sinosoft.lis.vdb.LAWarrantorDBSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LATreeAccessoryBSet;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.lis.vschema.LAWarrantorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

public class LAAgentSecondBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    public LAAgentSecondBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        System.out.println("Start LAAgentSecondBLS Submit...");
        tReturn = saveLAAgent(cInputData);
        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End LAAgentSecondBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean saveLAAgent(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentSecondBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 代理人信息保存...");
            LAAgentBDB tLAAgentBDB = new LAAgentBDB(conn);
            tLAAgentBDB.setSchema((LAAgentBSchema) mInputData.
                                  getObjectByObjectName("LAAgentBSchema", 0));
            String oldAgentCode = tLAAgentBDB.getAgentCode();
            if (!tLAAgentBDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentBDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentSecondBLS";
                tError.functionName = "saveAgentData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LAAgentDB tLAAgentDB = new LAAgentDB(conn);
//           tLAAgentDB.setAgentCode(oldAgentCode);
//           if (!tLAAgentDB.delete())
//           {
//                // @@错误处理
//                   this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
//                CError tError = new CError();
//                tError.moduleName = "LAAgentSecondBLS";
//                tError.functionName = "saveAgentData";
//                tError.errorMessage = "数据保存失败!";
//                this.mErrors .addOneError(tError) ;
//                conn.rollback();
//                conn.close();
//                return false;
//            }
            tLAAgentDB.setSchema((LAAgentSchema) mInputData.
                                 getObjectByObjectName("LAAgentSchema", 0));
            if (!tLAAgentDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentSecondBLS";
                tError.functionName = "saveAgentData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            System.out.println("end 代理人信息保存...");
            System.out.println("start 行政信息保存...");
            LATreeDB tLATreeDB = new LATreeDB(conn);
//            tLATreeDB.setAgentCode(oldAgentCode);
//            if (!tLATreeDB.delete())
//            {
//                // @@错误处理
//                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
//                CError tError = new CError();
//                tError.moduleName = "LAAgentSecondBLS";
//                tError.functionName = "saveAgentData";
//                tError.errorMessage = "数据保存失败!";
//                this.mErrors .addOneError(tError) ;
//                conn.rollback();
//                conn.close();
//                return false;
//            }
            tLATreeDB.setSchema((LATreeSchema) mInputData.getObjectByObjectName(
                    "LATreeSchema", 0));
            if (!tLATreeDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentSecondBLS";
                tError.functionName = "saveAgentData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LATreeBDB tLATreeBDB = new LATreeBDB(conn);
            tLATreeBDB.setSchema((LATreeBSchema) mInputData.
                                 getObjectByObjectName("LATreeBSchema", 0));
            if (!tLATreeBDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeBDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentSecondBLS";
                tError.functionName = "saveAgentData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

//             LATreeAccessoryDB tLATreeAccessoryDB = new LATreeAccessoryDB(conn);
//             tLATreeAccessoryDB.setAgentCode(oldAgentCode);
//             //tLATreeAccessoryDB.setAgentGrade(tLATreeBDB.getAgentGrade());
//             if (!tLATreeAccessoryDB.deleteSQL())
//             {
//                 // @@错误处理
//                 this.mErrors.copyAllErrors(tLATreeAccessoryDB.mErrors);
//                 CError tError = new CError();
//                 tError.moduleName = "ALAAgentBLS";
//                 tError.functionName = "saveAgentData";
//                 tError.errorMessage = "行政附属信息删除失败!";
//                 this.mErrors .addOneError(tError) ;
//                 conn.rollback();
//                 conn.close();
//                 return false;
//            }
            LATreeAccessorySet tLATreeAccessorySet = new LATreeAccessorySet();
            tLATreeAccessorySet.set((LATreeAccessorySet) mInputData.
                                    getObjectByObjectName("LATreeAccessorySet",
                    0));
            if (tLATreeAccessorySet.size() > 0)
            {
                LATreeAccessoryDBSet tLATreeAccessoryDBSet = new
                        LATreeAccessoryDBSet();
                tLATreeAccessoryDBSet.set(tLATreeAccessorySet);
                if (!tLATreeAccessoryDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLATreeAccessoryDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ALAAgentBLS";
                    tError.functionName = "saveAgentData";
                    tError.errorMessage = "行政附属信息删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            LATreeAccessoryBSet tLATreeAccessoryBSet = new LATreeAccessoryBSet();
            tLATreeAccessoryBSet.set((LATreeAccessoryBSet) mInputData.
                                     getObjectByObjectName(
                                             "LATreeAccessoryBSet", 0));
            if (tLATreeAccessoryBSet.size() > 0)
            {
                LATreeAccessoryBDBSet tLATreeAccessoryBDBSet = new
                        LATreeAccessoryBDBSet(conn);
                tLATreeAccessoryBDBSet.set(tLATreeAccessoryBSet);
                if (!tLATreeAccessoryBDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLATreeAccessoryBDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ALAAgentBLS";
                    tError.functionName = "saveAgentData";
                    tError.errorMessage = "行政附属信息删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            System.out.println("end 行政信息保存...");
            System.out.println("begin 销售机构管理人员更新...");
            LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
            //LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
            tLABranchGroupSet.set((LABranchGroupSet) mInputData.
                                  getObjectByObjectName("LABranchGroupSet", 0));
            if (tLABranchGroupSet.size() > 0)
            {
                LABranchGroupDBSet tLABranchGroupDBSet = new LABranchGroupDBSet(
                        conn);
                tLABranchGroupDBSet.set(tLABranchGroupSet);
                if (!tLABranchGroupDBSet.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLABranchGroupDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ALAAgentBLS";
                    tError.functionName = "saveAgentData";
                    tError.errorMessage = "销售机构管理人员更新失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            System.out.println("end 销售机构管理人员更新...");
            System.out.println("start 担保人信息保存...");
            //删除原有记录
//            LAWarrantorDB tLAWarrantorDB=new LAWarrantorDB(conn);
//            //LAWarrantorSchema tLAWarrantorSchema = new LAWarrantorSchema();
//            //tLAWarrantorSchema.setAgentCode(tLAAgentBDB.getAgentCode());
//            tLAWarrantorDB.setAgentCode(tLAAgentBDB.getAgentCode());
//            //tLAWarrantorDB.setSchema(tLAWarrantorSchema);
//            if (!tLAWarrantorDB.deleteSQL())
//            {
//                  // @@错误处理
//                 this.mErrors.copyAllErrors(tLAWarrantorDB.mErrors);
//                 CError tError = new CError();
//                 tError.moduleName = "LAAgentSecondBLS";
//                 tError.functionName = "deleteWarrantorData";
//                 tError.errorMessage = "数据保存失败!";
//                 this.mErrors .addOneError(tError) ;
//                 conn.rollback();
//                 conn.close();
//                 return false;
//           }
            LAWarrantorDBSet tLAWarrantorDBSet = new LAWarrantorDBSet(conn);
            tLAWarrantorDBSet.set((LAWarrantorSet) mInputData.
                                  getObjectByObjectName("LAWarrantorSet", 0));
            if (!tLAWarrantorDBSet.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAWarrantorDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentSecondBLS";
                tError.functionName = "saveWarrantorData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            System.out.println("end 担保人信息保存...");
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentSecondBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }
}
