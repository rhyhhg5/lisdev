package com.sinosoft.lis.agent;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public interface LARecomInterface
{
    public CErrors mErrors = new CErrors();

    public void setOperate(String cOperate); //设置操作数

    public void setGlobalInput(GlobalInput cGlobalInput); //设置全局变量

    public void setLARecomRelationSet(LARecomRelationSet cLARecomRelationSet); //设置育成信息

    public void setLATreeSchema(LATreeSchema cLATreeSchema); //设置代理人行政信息

    public void setEdorNo(String cEdorNo); //设置转储编码

    public boolean dealdata(); //处理函数

    public MMap getResult(); //获得处理结果

    public LATreeSchema getLATreeSchema(); //获得Schema
}
