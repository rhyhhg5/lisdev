package com.sinosoft.lis.agent;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.vschema.LAWarrantorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public class LAHealthManagementCompanyAgentComUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private String mIsManager;
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private LAWarrantorSet mLAWarrantorSet = new LAWarrantorSet();
    private LARearRelationSet mLARearRelationSet = new LARearRelationSet();

    private String mAgentCode = "";

    public String getAgentCode()
    {
        return mAgentCode;
    }

    public LAHealthManagementCompanyAgentComUI()
    {
    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "Admin";
        tG.ComCode = "001";

        String tIsManager = "true";

        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        tLAAgentSchema.setAgentCode("");
        tLAAgentSchema.setName("aaa");
        tLAAgentSchema.setSex("0");
        tLAAgentSchema.setBirthday("2005-01-17");
        tLAAgentSchema.setAgentGroup("861100001801");
        tLAAgentSchema.setBranchType("1");
        tLAAgentSchema.setBranchType2("11");
        tLAAgentSchema.setManageCom("86110000");
        tLAAgentSchema.setAssuMoney("1");
        //tLAAgentSchema.setBranchCode("000000000479");
        tLAAgentSchema.setIDNo("1814117111111111111");
        tLAAgentSchema.setIDNoType("0");

        LATreeSchema tLATreeSchema = new LATreeSchema();
        tLATreeSchema.setAgentCode("8611000141");
        //tLATreeSchema.setAgentGroup("861100007777");
        tLATreeSchema.setManageCom("86110000");
        tLATreeSchema.setAgentGrade("A03");
        tLATreeSchema.setAgentLine("A");
        tLATreeSchema.setBranchType("1");
        tLATreeSchema.setBranchType2("11");



        VData tVData = new VData();
        tVData.add(tG);
        tVData.add(tIsManager);
        tVData.addElement(tLAAgentSchema);
        tVData.addElement(tLATreeSchema);
    //    tVData.addElement(tLAWarrantorSet);

        LAHealthManagementCompanyAgentComUI tLAAgent = new LAHealthManagementCompanyAgentComUI();
        tLAAgent.submitData(tVData, "INSERT||MAIN");
        //tLAAgent.submitData(tVData, "INSERT||MAIN");
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("Begin LAHealthManagementCompanyAgentComUI.submitData.........");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        System.out.println("Start LAHealthManagementCompanyAgentComBL Submit...");
        LAHealthManagementCompanyAgentComBL tLAHealthManagementCompanyAgentComBL = new LAHealthManagementCompanyAgentComBL();

        tLAHealthManagementCompanyAgentComBL.submitData(mInputData, mOperate);
        //如果有需要处理的错误，则返回
        if (tLAHealthManagementCompanyAgentComBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAHealthManagementCompanyAgentComBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAHealthManagementCompanyAgentComUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mAgentCode = tLAHealthManagementCompanyAgentComBL.getAgentCode();
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("Begin LAHealthManagementCompanyAgentComUI.getInputData.........");
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                                      getObjectByObjectName("LAAgentSchema", 0));
        this.mLATreeSchema.setSchema((LATreeSchema) cInputData.
                                     getObjectByObjectName("LATreeSchema", 0));
        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAHealthManagementCompanyAgentComUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        System.out.println("Begin LAHealthManagementCompanyAgentComUI.dealData.........");
        String tIDNo = this.mLAAgentSchema.getIDNo();
        String tIDNoType = this.mLAAgentSchema.getIDNoType();
        if(this.mOperate.equals("UPDATE||MAIN")) {
	        if (tIDNo == null || tIDNoType == null || tIDNo.equals("") ||
	            tIDNoType.equals(""))
	        {
	            CError tError = new CError();
	            tError.moduleName = "ALAgentBL";
	            tError.functionName = "dealData";
	            tError.errorMessage = "没有证件类型或证件号";
	            this.mErrors.addOneError(tError);
	            return false;
	        }
        }
        if (this.mOperate.equals("INSERT||MAIN") ||
            this.mOperate.indexOf("UPDATE") != -1)
        {


        }
        String tName = this.mLAAgentSchema.getName();
        if (tName == null || tName.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "dealData";
            tError.errorMessage = "人员姓名为空";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tSex = this.mLAAgentSchema.getSex();
        if (tSex == null || tSex.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "dealData";
            tError.errorMessage = "人员性别为空";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            System.out.println("Begin LAHealthManagementCompanyAgentComUI.prepareOutputData.........");
            mInputData.clear();
            mInputData.add(this.mGlobalInput);
         //   mInputData.add(this.mIsManager);
            mInputData.add(this.mLAAgentSchema);
            mInputData.add(this.mLATreeSchema);
         //   mInputData.add(this.mLAWarrantorSet);
          //  mInputData.add(this.mLARearRelationSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAHealthManagementCompanyAgentComUI";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
