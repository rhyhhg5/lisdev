/*
 * <p>ClassName: ALABankTreeBL </p>
 * <p>Description: ALABankTreeBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Modified by huxl
 * @Database: 销售银代人员级别调整
 * @CreateDate：2008-03-17
 */
package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAComToAgentDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAComToAgentBSchema;
import com.sinosoft.lis.schema.LAComToAgentSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.vschema.LAComToAgentBSet;
import com.sinosoft.lis.vschema.LAComToAgentSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LATreeBSet;
public class LABankTreeBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String tAgentCode;
    private String tAgentGroup;
    private String tNewAgentGrade1;
    private String tNewAgentGrade;
    private String tBranchType;
    private String tBranchType2;
    private String tDate;
    private String tNewAgentSeries = "";
    /** 数据操作字符串 */
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    /** 业务处理相关变量 */
    private LAAgentSchema mOldLAAgentSchema = new LAAgentSchema();
    private LATreeSchema mOldLATreeSchema = new LATreeSchema();
    private LABranchGroupSchema mNewLABranchGroupSchema = new
            LABranchGroupSchema();
    private LABranchGroupSchema mLABranchGroupSchema = new
    LABranchGroupSchema();
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAAgentBSchema mLAAgentBSchema = new LAAgentBSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private LATreeBSchema mLATreeBSchema = new LATreeBSchema();
    //备份代理人和代理机构关联表lacomtoagent及备份表lacomtoagentb
    private LAComToAgentSet mLAComToAgentSet=new LAComToAgentSet();
    private LAComToAgentBSet mLAComToAgentBSet=new LAComToAgentBSet();
    private MMap mMap = new MMap();
    public LABankTreeBL() {
    }

    public static void main(String[] args) {
    }
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!check()) {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALABankAgentBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALABankTreeBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("over dealData");
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        
        System.out.println(this.mMap.get(mLABranchGroupSchema));
        System.out.println("~~~~~~~~~~"+mLABranchGroupSchema.getAgentGroup());
        if (!tPubSubmit.submitData(this.mInputData, "")) {
            if (tPubSubmit.mErrors.needDealError()) {
                CError tError = new CError();
                tError.moduleName = "ALABankTreeBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败ALABankTreeBL-->submitData!";
                this.mErrors.addOneError(tError);
                return false;
            }
            return false;
        }
        this.mResult.add(this.mLAAgentSchema);
        mInputData = null;
        this.mMap = null;
        return true;
    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        Reflections tReflections = new Reflections();
        String ttEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        String tNewAgentSeries1 = "";
        if (tNewAgentGrade1 == null || tNewAgentGrade1.equals("")) {
            //没有业务职级,表示是部经理职级
        	tNewAgentGrade1 = tNewAgentGrade; 
            tNewAgentSeries1 = getAgentSeries(this.tNewAgentGrade); //获取前台传入的新业务职级序列
            tNewAgentSeries = tNewAgentSeries1; 
            
        } else if (tNewAgentGrade == null || this.tNewAgentGrade.equals("")) {
            
        	tNewAgentGrade = tNewAgentGrade1; 
            tNewAgentSeries1 = getAgentSeries(this.tNewAgentGrade1); 
            tNewAgentSeries = tNewAgentSeries1;
        } else {
            tNewAgentSeries1 = getAgentSeries(this.tNewAgentGrade1); //获取前台传入的新业务职级序列
            tNewAgentSeries = getAgentSeries(this.tNewAgentGrade); //获取前台传入的新行政职级序列
        }
//        if (mOldLATreeSchema.getAgentGrade1().equals(tNewAgentGrade1) &&
//            mOldLATreeSchema.getAgentGrade().equals(tNewAgentGrade)) {
//            CError tError = new CError();
//            tError.moduleName = "LABankTreeBL";
//            tError.functionName = "dealData";
//            tError.errorMessage = " 该业务员的职级没有发生变动，不需要处理!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
        tReflections.transFields(this.mLATreeBSchema, this.mOldLATreeSchema);
        
       //获取新团队的信息       
        if(tAgentGroup!=null&&!tAgentGroup.equals(""))
        {
	        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
	        tLABranchGroupDB.setAgentGroup(tAgentGroup);
	        this.mNewLABranchGroupSchema = tLABranchGroupDB.getSchema(); 
	        //System.out.println(".................branchseries1:"+this.mNewLABranchGroupSchema.getBranchSeries());

        }
        else
        {//如果没有录入团队代码,则用现团队代码
        	String SQL = "select agentgroup from laagent where agentcode='"+this.tAgentCode+"'";
        	ExeSQL tExeSQL = new ExeSQL();
        	String agentgroup = tExeSQL.getOneValue(SQL);
        	LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
	        tLABranchGroupDB.setAgentGroup(agentgroup);
	        this.mNewLABranchGroupSchema = tLABranchGroupDB.getSchema(); 
//	        //System.out.println(".................agentgroup2:"
//	        		+this.mNewLABranchGroupSchema.getAgentGroup());
//	        System.out.println(".................branchseries2:"
//	        		+this.mNewLABranchGroupSchema.getBranchSeries());
        }
              
        this.mLATreeBSchema.setEdorNO(ttEdorNo);
        this.mLATreeBSchema.setMakeDate(currentDate);
        this.mLATreeBSchema.setMakeTime(currentTime);
        this.mLATreeBSchema.setModifyDate(currentDate);
        this.mLATreeBSchema.setModifyTime(currentTime);
        this.mLATreeBSchema.setOperator(this.mGlobalInput.Operator);
        this.mLATreeBSchema.setRemoveType("06");
        this.mLATreeBSchema.setMakeDate2(mOldLATreeSchema.getMakeDate());
        this.mLATreeBSchema.setMakeTime2(mOldLATreeSchema.getMakeTime());
        this.mLATreeBSchema.setModifyDate2(mOldLATreeSchema.getMakeDate());
        this.mLATreeBSchema.setModifyTime2(mOldLATreeSchema.getMakeTime());
        this.mLATreeBSchema.setOperator2(mOldLATreeSchema.getOperator());
        //this.mMap.put(mLATreeBSchema, "INSERT")	

        tReflections.transFields(this.mLATreeSchema, mOldLATreeSchema);
        this.mLATreeSchema.setAstartDate(this.tDate);
        this.mLATreeSchema.setStartDate(this.tDate);
        this.mLATreeSchema.setAgentGrade1(this.tNewAgentGrade1);
        this.mLATreeSchema.setAgentSeries1(tNewAgentSeries1);
        this.mLATreeSchema.setStartDate1(this.tDate);
        this.mLATreeSchema.setAgentGrade(this.tNewAgentGrade);
        this.mLATreeSchema.setAgentSeries(tNewAgentSeries);
        this.mLATreeSchema.setModifyDate(currentDate);
        this.mLATreeSchema.setModifyTime(currentTime);
        this.mLATreeSchema.setOperator(mGlobalInput.Operator);
        //下面处理同序列职级调整
        if (mOldLATreeSchema.getAgentSeries().equals(tNewAgentSeries)) {    
        	this.mMap.put(mLATreeBSchema, "INSERT");
        	this.mMap.put(mLATreeSchema, "UPDATE");
            return true;
        }else
        {//不同序列的需要进行业绩的处理
        	System.out.println("BL:tNewAgentSeries"+tNewAgentSeries);
        	//this.mMap.put(mLATreeSchema, "UPDATE");
//        	if(!changeCommision(mNewLABranchGroupSchema,this.tAgentCode)){
//        		return false;
//        	}
            /*
             * 代码修改说明
             * 操作代码：苗祥征0626001
             * 日期：2008-07-21
             * 责任人：苗祥征
             * 修改类别：新增
             * 原代码：无
             * 修改原因：银代人员职级调整时，没有同步修改lacomtoagent和lacomtoagentb表
             * 此处操作：查询出与被调动银代人员关联的代理机构并作相应的修改。
             */
        	String tSQL ="select max(edorno) from lacomtoagentb ";
            ExeSQL tExeSQL =new ExeSQL();
            String tEdorNo2=""+tExeSQL.getOneValue(tSQL);
            if(tEdorNo2=="" || tEdorNo2==null){
            	CError tError = new CError();
                tError.moduleName = "LABankTreeBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "从代理人机构关联表中获取最大流水号失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
            LAComToAgentDB tLAComToAgentDB=null;
            LAComToAgentSet tLAComToAgentSet=new LAComToAgentSet();
            tSQL="select * from lacomtoagent where agentcom in(select distinct agentcom from lacomtoagent where  agentcode='"
            	+this.mLATreeSchema.getAgentCode()+"')";
            tLAComToAgentDB=new LAComToAgentDB();
            tLAComToAgentSet=tLAComToAgentDB.executeQuery(tSQL);
            if (tLAComToAgentDB.mErrors.needDealError()) {
                this.mErrors.copyAllErrors(tLAComToAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LABankTreeBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询业务员" + mLATreeSchema.getAgentCode() +
                                      "的关联代理机构信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LAComToAgentSchema tLAComToAgentSchema;
            LAComToAgentBSchema tLAComToAgentBSchema;
            System.out.println("BL:dealdata:tLAComToAgentSet.size"+tLAComToAgentSet.size());
            for(int m=1;m<=tLAComToAgentSet.size();m++){
                tEdorNo2=(Integer.parseInt(tEdorNo2)+1)+"";
                System.out.println("bl:dealdata:tEdorNo2"+tEdorNo2);
                tLAComToAgentSchema=new LAComToAgentSchema();
                tLAComToAgentBSchema=new LAComToAgentBSchema();
                tLAComToAgentSchema=tLAComToAgentSet.get(m);
                tReflections = new Reflections();
                //System.out.println("here1");
                tReflections.transFields(tLAComToAgentBSchema, tLAComToAgentSchema);
                //System.out.println("here2");
                tLAComToAgentBSchema.setMakeDate(currentDate);
                tLAComToAgentBSchema.setMakeTime(currentTime);
                tLAComToAgentBSchema.setModifyDate(currentDate);
                tLAComToAgentBSchema.setModifyTime(currentTime);
                tLAComToAgentBSchema.setOperator(mGlobalInput.Operator);
                //System.out.println("here3");
                tLAComToAgentBSchema.setEdorNo(tEdorNo2);
                tLAComToAgentBSchema.setEdorType("03");
                mLAComToAgentBSet.add(tLAComToAgentBSchema);
                //System.out.println("here4");
                tLAComToAgentSchema.setModifyDate(currentDate);
                tLAComToAgentSchema.setModifyTime(currentTime);
                tLAComToAgentSchema.setOperator(mGlobalInput.Operator);
                tLAComToAgentSchema.setAgentGroup(mNewLABranchGroupSchema.getAgentGroup());
                //System.out.println("here5");
                mLAComToAgentSet.add(tLAComToAgentSchema);
                //System.out.println("here6");
            }
            this.mMap.put(mLAComToAgentBSet, "INSERT");
            this.mMap.put(mLAComToAgentSet, "UPDATE");
            /*
             * 苗祥征0721001结束
             */

        //下面处理升职到营业部经理的情况
        if (tNewAgentSeries.equals("2")) {
            if (!checkAgentGroup("42")) {
                return false;
            }
            //修改团队的主管信息。
            //备份团队信息
            backupLABranchGroup(mNewLABranchGroupSchema, ttEdorNo,
                                mOldLAAgentSchema.getAgentCode(),
                                mOldLAAgentSchema.getName());
          try{
            updateLABranchGroup(mNewLABranchGroupSchema,
                    mOldLAAgentSchema.getAgentCode(),
                    mOldLAAgentSchema.getName());
          }
          catch(Exception ex)
          {
        	  ex.getStackTrace();
          }
            //修改团队主管信息
            
            //修改业务员的营业部信息
            mLATreeSchema.setAgentGroup(mNewLABranchGroupSchema.getAgentGroup());
            mLATreeSchema.setUpAgent("");
            mLATreeSchema.setBranchCode(mNewLABranchGroupSchema.getAgentGroup());
            //this.mMap.put(mLATreeSchema, "UPDATE");

            tReflections.transFields(this.mLAAgentBSchema, mOldLAAgentSchema);
            this.mLAAgentBSchema.setEdorNo(ttEdorNo);
            this.mLAAgentBSchema.setMakeDate(currentDate);
            this.mLAAgentBSchema.setMakeTime(currentTime);
            this.mLAAgentBSchema.setModifyDate(currentDate);
            this.mLAAgentBSchema.setModifyTime(currentTime);
            this.mLAAgentBSchema.setOperator(this.mGlobalInput.Operator);
            this.mLAAgentBSchema.setEdorType("06");
            this.mMap.put(mLAAgentBSchema, "INSERT");

            tReflections.transFields(this.mLAAgentSchema, mOldLAAgentSchema);
            this.mLAAgentSchema.setAgentGroup(mNewLABranchGroupSchema.
                                              getAgentGroup());
            this.mLAAgentSchema.setBranchCode(mNewLABranchGroupSchema.getAgentGroup());
            this.mLAAgentSchema.setModifyDate(currentDate);
            this.mLAAgentSchema.setModifyTime(currentTime);
            this.mLAAgentSchema.setOperator(mGlobalInput.Operator);
            this.mMap.put(mLAAgentSchema, "UPDATE");

            //处理营业部下的渠道经理的上级为该营业部经理
            LATreeSet tLATreeSet = new LATreeSet();
            String strSQL =
                    "select * from LATree a where BranchType = '3' and BranchType2='01' and AgengSeries = '1' and exists " +
                    " (select 1 from LABranchGroup where AgentGroup = a.AgentGroup and UpBranch = '" +
                    mNewLABranchGroupSchema.getAgentGroup() + "') ";
            tLATreeSet = new LATreeDB().executeQuery(strSQL);
            updateLATreeUpAgent(tLATreeSet, ttEdorNo,
                                mOldLAAgentSchema.getAgentCode());

            //如果原先是渠道经理，需要将原先渠道下的业务员的上级的UpAgent置空。将原先的营业组的branchManager,BranchManagerName置空。
            if (mOldLATreeSchema.getAgentSeries().equals("1")) {
                LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                LABranchGroupSchema tOldLABranchGroupSchema = new
                        LABranchGroupSchema();
                tLABranchGroupDB.setAgentGroup(mOldLATreeSchema.getAgentGroup());
                if (!tLABranchGroupDB.getInfo()) {
                    CError tError = new CError();
                    tError.moduleName = "LABankTreeBL";
                    tError.functionName = "check";
                    tError.errorMessage = "没有查询到业务员原营业组" +
                                          mOldLATreeSchema.getBranchCode() +
                                          "的机构信息!";
                    this.mErrors.addOneError(tError);
                    return false;
                } else {
                    tOldLABranchGroupSchema = tLABranchGroupDB.getSchema();
                }
                backupLABranchGroup(tOldLABranchGroupSchema, ttEdorNo, "", "");
                updateLABranchGroup(mNewLABranchGroupSchema, "", "");
                tLATreeSet = new LATreeSet();
                strSQL =
                        "select * from LATree a where BranchType = '3' and BranchType2='01' and AgengSeries = '0' and AgentGroup = '" +
                        mOldLATreeSchema.getAgentGroup() + "' ";
                tLATreeSet = new LATreeDB().executeQuery(strSQL);
                updateLATreeUpAgent(tLATreeSet, ttEdorNo, "");
            }
            //return true;
        } else if (tNewAgentSeries.equals("1")) {
            if (!checkAgentGroup("41")) {
                return false;
            }
            //修改团队的主管信息。
            try
            {
            backupLABranchGroup(mNewLABranchGroupSchema, ttEdorNo,
                                mOldLAAgentSchema.getAgentCode(),
                                mOldLAAgentSchema.getName());
            updateLABranchGroup(mNewLABranchGroupSchema, 
                    mOldLAAgentSchema.getAgentCode(),
                    mOldLAAgentSchema.getName());
                    
            }
            catch(Exception ex)
            {
            	ex.getStackTrace();
            }
            //修改业务员的营业组信息
            mLATreeSchema.setAgentGroup(mNewLABranchGroupSchema.getAgentGroup());
            String Sql =
            	
                    "select BranchManager from LABranchGroup where AgentGroup = '" +
                    mNewLABranchGroupSchema.getUpBranch() + "'";
            mLATreeSchema.setUpAgent(new ExeSQL().getOneValue(Sql)); //设置为新营业组的上级的主管
            mLATreeSchema.setBranchCode(mNewLABranchGroupSchema.getAgentGroup());
            //this.mMap.put(mLATreeSchema, "UPDATE");

            tReflections.transFields(this.mLAAgentBSchema, mOldLAAgentSchema);
            this.mLAAgentBSchema.setEdorNo(ttEdorNo);
            this.mLAAgentBSchema.setMakeDate(currentDate);
            this.mLAAgentBSchema.setMakeTime(currentTime);
            this.mLAAgentBSchema.setModifyDate(currentDate);
            this.mLAAgentBSchema.setModifyTime(currentTime);
            this.mLAAgentBSchema.setOperator(this.mGlobalInput.Operator);
            this.mLAAgentBSchema.setEdorType("06");
            this.mMap.put(mLAAgentBSchema, "INSERT");

            tReflections.transFields(this.mLAAgentSchema, mOldLAAgentSchema);
            this.mLAAgentSchema.setAgentGroup(mNewLABranchGroupSchema.
                                              getAgentGroup());
            this.mLAAgentSchema.setBranchCode(mNewLABranchGroupSchema.getAgentGroup());
            this.mLAAgentSchema.setModifyDate(currentDate);
            this.mLAAgentSchema.setModifyTime(currentTime);
            this.mLAAgentSchema.setOperator(mGlobalInput.Operator);
            this.mMap.put(mLAAgentSchema, "UPDATE");

            //处理营业组下的业务员的上级为该渠道经理
            LATreeSet tLATreeSet = new LATreeSet();
            String strSQL =
                    "select * from LATree a where BranchType = '3' and BranchType2='01' and AgengSeries = '0' and AgentGroup = ' " +
                    mNewLABranchGroupSchema.getAgentGroup() + "' ";
            tLATreeSet = new LATreeDB().executeQuery(strSQL);
            updateLATreeUpAgent(tLATreeSet, ttEdorNo,
                                mOldLAAgentSchema.getAgentCode());
            //如果原先是营业部经理，需要将原先营业组下的渠道经理的UpAgent置空。将原先的营业部的branchManager,BranchManagerName置空。
            if (mOldLATreeSchema.getAgentSeries().equals("2")) {
                LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                LABranchGroupSchema tOldLABranchGroupSchema = new
                        LABranchGroupSchema();
                tLABranchGroupDB.setAgentGroup(mOldLATreeSchema.getAgentGroup());
                if (!tLABranchGroupDB.getInfo()) {
                    CError tError = new CError();
                    tError.moduleName = "LABankTreeBL";
                    tError.functionName = "check";
                    tError.errorMessage = "没有查询到业务员原营业组" +
                                          mOldLATreeSchema.getBranchCode() +
                                          "的机构信息!";
                    this.mErrors.addOneError(tError);
                    return false;
                } else {
                    tOldLABranchGroupSchema = tLABranchGroupDB.getSchema();
                }
                backupLABranchGroup(tOldLABranchGroupSchema, ttEdorNo, "", "");
                updateLABranchGroup(tOldLABranchGroupSchema,  "", "");
                tLATreeSet = new LATreeSet();
                strSQL =
                        "select * from LATree a where BranchType = '3' and BranchType2='01' and AgengSeries = '1' and UpAgent = '" +
                        mOldLATreeSchema.getAgentCode() + "' ";
                tLATreeSet = new LATreeDB().executeQuery(strSQL);
                updateLATreeUpAgent(tLATreeSet, ttEdorNo, "");
            }
            //return true;
        } else if (tNewAgentSeries.equals("0")) {
            //如果原先是营业部经理,需要修改业务员的营业组信息
            if (this.mOldLATreeSchema.getAgentSeries().equals("2")) {
                if (!checkAgentGroup("41")) {
                    return false;
                }
                mLATreeSchema.setAgentGroup(mNewLABranchGroupSchema.
                                            getAgentGroup());
                mLATreeSchema.setUpAgent(mNewLABranchGroupSchema.
                                         getBranchManager());
                mLATreeSchema.setBranchCode(mNewLABranchGroupSchema.getAgentGroup()
                                            );
                //this.mMap.put(mLATreeSchema, "UPDATE");

                tReflections.transFields(this.mLAAgentBSchema,
                                         mOldLAAgentSchema);
                this.mLAAgentBSchema.setEdorNo(ttEdorNo);
                this.mLAAgentBSchema.setMakeDate(currentDate);
                this.mLAAgentBSchema.setMakeTime(currentTime);
                this.mLAAgentBSchema.setModifyDate(currentDate);
                this.mLAAgentBSchema.setModifyTime(currentTime);
                this.mLAAgentBSchema.setOperator(this.mGlobalInput.Operator);
                this.mLAAgentBSchema.setEdorType("06");
                this.mMap.put(mLAAgentBSchema, "INSERT");

                tReflections.transFields(this.mLAAgentSchema, mOldLAAgentSchema);
                this.mLAAgentSchema.setAgentGroup(mNewLABranchGroupSchema.
                                                  getAgentGroup());
                this.mLAAgentSchema.setBranchCode(mNewLABranchGroupSchema.getAgentGroup());
                this.mLAAgentSchema.setModifyDate(currentDate);
                this.mLAAgentSchema.setModifyTime(currentTime);
                this.mLAAgentSchema.setOperator(mGlobalInput.Operator);
                this.mMap.put(mLAAgentSchema, "UPDATE");
            }
            //原先的营业组或部的branchManager,BranchManagerName置空。原业务上级为该业务人员的upAgent全部置空
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            LABranchGroupSchema tOldLABranchGroupSchema = new
                    LABranchGroupSchema();
            tLABranchGroupDB.setAgentGroup(mOldLATreeSchema.getAgentGroup());
            if (!tLABranchGroupDB.getInfo()) {
                CError tError = new CError();
                tError.moduleName = "LABankTreeBL";
                tError.functionName = "check";
                tError.errorMessage = "没有查询到业务员原营业组" +
                                      mOldLATreeSchema.getBranchCode() +
                                      "的机构信息!";
                this.mErrors.addOneError(tError);
                return false;
            } else {
                tOldLABranchGroupSchema = tLABranchGroupDB.getSchema();
            }
            backupLABranchGroup(tOldLABranchGroupSchema, ttEdorNo, "", "");
            updateLABranchGroup(tOldLABranchGroupSchema, "", "");
            LATreeSet tLATreeSet = new LATreeSet();
            String strSQL =
                    "select * from LATree a where BranchType = '3' and BranchType2='01' and UpAgent = '" +
                    mOldLATreeSchema.getAgentCode() + "' ";
            tLATreeSet = new LATreeDB().executeQuery(strSQL);
            updateLATreeUpAgent(tLATreeSet, ttEdorNo, "");
        }
        this.mMap.put(mLATreeBSchema, "INSERT");
        this.mMap.put(mLATreeSchema, "UPDATE");
        System.out.println("BL:changeCommision"+this.mNewLABranchGroupSchema.getAgentGroup());
         //不同序列的需要进行业绩的处理
        if(!changeCommision(this.mNewLABranchGroupSchema,this.tAgentCode)){
        		return false;
       	}
        System.out.println("3333333333______");
        return true;
        }
    }
    /**
     * 团队发生变化后,需要进行业绩的修改
     * @param mNewLABranchGroupSchema
     * @param changeCommision
     * @return
     */
    private boolean changeCommision(LABranchGroupSchema cNewLABranchGroupSchema,
    		                        String mAgentCode){ 
    	if(this.tAgentGroup!=null && !this.tAgentGroup.equals("")){
    		
    	
    	//只能对还未进行过薪资计算月份的业绩修改
    	String SQL = "select * from lacommision where agentcode = '"+mAgentCode+"'" +
    			" and branchtype='3' and branchtype2='01' and (caldate>='"+tDate+"' or caldate is null)";
    	LACommisionSet tLACommisionSet = new LACommisionSet();
        LACommisionDB tLACommisionDB = new LACommisionDB();
        tLACommisionSet = tLACommisionDB.executeQuery(SQL);
        for (int j = 1; j <= tLACommisionSet.size(); j++) {
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            tLACommisionSchema = tLACommisionSet.get(j);
            tLACommisionSchema.setAgentGroup(cNewLABranchGroupSchema.getAgentGroup());
            tLACommisionSchema.setBranchCode(cNewLABranchGroupSchema.getAgentGroup()); 
            tLACommisionSchema.setBranchSeries(cNewLABranchGroupSchema.getBranchSeries());
            tLACommisionSchema.setBranchAttr(cNewLABranchGroupSchema.getBranchAttr());
            tLACommisionSchema.setModifyDate(currentDate);
            tLACommisionSchema.setModifyTime(currentTime);
            tLACommisionSchema.setOperator(mGlobalInput.Operator);
            this.mLACommisionSet.add(tLACommisionSchema);
        }
        this.mMap.put(this.mLACommisionSet, "UPDATE");
    	//业务表lccont修改
    	 String tAgentGroup = cNewLABranchGroupSchema.getAgentGroup();
    	 System.out.println("tAgentGroup:"+tAgentGroup);
         String LccontSql = "update lccont  set  agentgroup='" + tAgentGroup+
                       "',ModifyTime = '" + currentTime +
                       "',ModifyDate = '" + currentDate +
                       "',operator='"+mGlobalInput.Operator+"'  where agentcode='"+mAgentCode+"'";
         this.mMap.put(LccontSql, "UPDATE");
         String LcpolSql = "update lcpol  set  agentgroup='" + tAgentGroup+
                "',ModifyTime = '" + currentTime +
                "',ModifyDate = '" + currentDate +
                "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";

         this.mMap.put(LcpolSql, "UPDATE");
         String LjsSql = "update ljspayperson  set  agentgroup='" + tAgentGroup+
                "',ModifyTime = '" + currentTime +
                "',ModifyDate = '" + currentDate +
                "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";

         this.mMap.put(LjsSql, "UPDATE");
         String LjspaySql = "update ljspay  set  agentgroup='" + tAgentGroup+
                "',ModifyTime = '" + currentTime +
                "',ModifyDate = '" + currentDate +
                "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";
         this.mMap.put(LjspaySql, "UPDATE");
    	}
    	return true;
    }
    private boolean checkAgentGroup(String aBranchLevel) {
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(this.tAgentGroup);
        if (!tLABranchGroupDB.getInfo()) {
            CError tError = new CError();
            tError.moduleName = "LABankTreeBL";
            tError.functionName = "check";
            tError.errorMessage = "没有查询到" + tAgentGroup + "的机构信息!";
            this.mErrors.addOneError(tError);
            return false;
        } else {
            this.mNewLABranchGroupSchema = tLABranchGroupDB.getSchema();
        }
        if (!tNewAgentSeries.equals("0")) {
            if (mNewLABranchGroupSchema.getBranchManager() != null &&
                !mNewLABranchGroupSchema.getBranchManager().equals("")) {
                CError tError = new CError();
                tError.moduleName = "LABankTreeBL";
                tError.functionName = "dealData";
                tError.errorMessage = " 销售团队已经存在主管!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        if (!mNewLABranchGroupSchema.getBranchLevel().equals(aBranchLevel)) {
            CError tError = new CError();
            tError.moduleName = "LABankTreeBL";
            tError.functionName = "dealData";
            if (aBranchLevel.equals("42")) {
                tError.errorMessage = " 所选销售团队非营业部!";
            } else {
                tError.errorMessage = " 所选销售团队非营业组!";
            }
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!mNewLABranchGroupSchema.getManageCom().equals(this.
                mOldLAAgentSchema.getManageCom())) {
            CError tError = new CError();
            tError.moduleName = "LABankTreeBL";
            tError.functionName = "dealData";
            tError.errorMessage = " 暂时不允许跨机构任命!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
 private void updateLATreeUpAgent(LATreeSet aLATreeSet, String aEdorNo,
            String aUpAgent) {
			Reflections tReflections = new Reflections();
			LATreeSet tUpdateLATreeSet = new LATreeSet();
			LATreeBSet tLATreeBSet = new LATreeBSet();
			for (int i = 1; i <= aLATreeSet.size(); i++) {
			LATreeSchema tLATreeSchema = aLATreeSet.get(i);
			LATreeBSchema tLATreeBSchema = new LATreeBSchema();
			tReflections.transFields(tLATreeBSchema, tLATreeSchema);
			tLATreeBSchema.setEdorNO(aEdorNo);
			tLATreeBSchema.setRemoveType("06");
			tLATreeBSchema.setMakeDate(currentDate);
			tLATreeBSchema.setMakeTime(currentTime);
			tLATreeBSchema.setModifyDate(currentDate);
			tLATreeBSchema.setModifyTime(currentTime);
			tLATreeBSchema.setOperator(this.mGlobalInput.Operator);
			tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
			tLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
			tLATreeBSchema.setModifyDate2(tLATreeSchema.getMakeDate());
			tLATreeBSchema.setModifyTime2(tLATreeSchema.getMakeTime());
			tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
			tLATreeBSet.add(tLATreeBSchema);
			tLATreeSchema.setUpAgent(aUpAgent);
			tLATreeSchema.setModifyDate(currentDate);
			tLATreeSchema.setModifyTime(currentTime);
			tLATreeSchema.setOperator(this.mGlobalInput.Operator);
			tUpdateLATreeSet.add(tLATreeSchema);
			}
			this.mMap.put(tLATreeBSet, "INSERT");
			this.mMap.put(tUpdateLATreeSet, "UPDATE");
}
 /*
  * 团队信息备份
  */
private void backupLABranchGroup(LABranchGroupSchema aLABranchGroupSchema,
            String aEdorNo, String aAgentCode,
            String aName) {
		Reflections tReflections = new Reflections();
		LABranchGroupBSchema tLABranchGroupBSchema = new
		LABranchGroupBSchema();
		LABranchGroupSchema tLABranchGroupSchema = aLABranchGroupSchema.getSchema();
		tReflections.transFields(tLABranchGroupBSchema, aLABranchGroupSchema);
		tLABranchGroupBSchema.setEdorNo(aEdorNo);
		tLABranchGroupBSchema.setMakeDate(currentDate);
		tLABranchGroupBSchema.setMakeTime(currentTime);
		tLABranchGroupBSchema.setModifyDate(currentDate);
		tLABranchGroupBSchema.setModifyTime(currentTime);
		tLABranchGroupBSchema.setOperator(mGlobalInput.Operator);
		tLABranchGroupBSchema.setEdorType("06"); //这里类型不确定
		tLABranchGroupBSchema.setMakeDate2(aLABranchGroupSchema.getMakeDate());
		tLABranchGroupBSchema.setMakeTime2(aLABranchGroupSchema.getMakeTime());
		tLABranchGroupBSchema.setModifyDate2(aLABranchGroupSchema.getMakeDate());
		tLABranchGroupBSchema.setModifyTime2(aLABranchGroupSchema.getMakeTime());
		tLABranchGroupBSchema.setOperator2(aLABranchGroupSchema.getOperator());
		this.mMap.put(tLABranchGroupBSchema, "INSERT");
		
}
/**
 * 团队主管任命
 */
private boolean updateLABranchGroup(LABranchGroupSchema aLABranchGroupSchema,String aAgentCode,
        String aName)
        {
			Reflections tReflections = new Reflections();
			mLABranchGroupSchema = new LABranchGroupSchema();
			mLABranchGroupSchema = aLABranchGroupSchema.getSchema();
			LABranchGroupSchema mLABranchGroupSchemaTemp = new LABranchGroupSchema();
			tReflections.transFields(mLABranchGroupSchemaTemp, mLABranchGroupSchema);
			System.out.println(mLABranchGroupSchemaTemp.getAgentGroup());
			mLABranchGroupSchemaTemp.setBranchManager(aAgentCode);
			mLABranchGroupSchemaTemp.setBranchManagerName(aName);
			mLABranchGroupSchemaTemp.setModifyDate(currentDate);
			mLABranchGroupSchemaTemp.setModifyTime(currentTime);
			mLABranchGroupSchemaTemp.setOperator(this.mGlobalInput.Operator); 
			System.out.println("~~~~~~~~~~~~~~~~~~~"+mLABranchGroupSchemaTemp.getBranchManager());
			System.out.println("~~~~~~~~~~~~~~~~~~~"+mLABranchGroupSchemaTemp.getAgentGroup());
			System.out.println("updateLABranchGroup:mMap.size"+this.mMap.size());
			this.mMap.put(mLABranchGroupSchemaTemp, "UPDATE");	
			System.out.println("AAAAAAAAAAAAA"+this.mMap.size());
			System.out.println("BBBBBBBBBBBBBBB"+this.mMap.get(mLABranchGroupSchemaTemp).toString());
			return true;
	    }
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("begin getInputData");
        tAgentCode = (String) cInputData.get(0);
        tAgentGroup = (String) cInputData.get(1);
        tNewAgentGrade1 = (String) cInputData.get(2);
        tNewAgentGrade = (String) cInputData.get(3);
        tDate = (String) cInputData.get(4);
        
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                getObjectByObjectName("GlobalInput", 5));
        tBranchType=(String) cInputData.get(6);
       
        tBranchType2=(String) cInputData.get(7);     
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);
            System.out.println("mMap.size"+mMap.size());
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LRGetCessDataBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    public VData getResult() {
        return this.mResult;
    }

    //根据代理人职级确定代理人系列
    private String getAgentSeries(String cAgentGrade) {
        String tAgentSeries = "";
        String tSQL =
                "select GradeProperty2 from LAAgentGrade where BranchType = '3' and BranchType2 = '01'"
                + "and GradeCode = '" + cAgentGrade + "'";
        ExeSQL tExeSQL = new ExeSQL();
        tAgentSeries = tExeSQL.getOneValue(tSQL).trim();
        if (tExeSQL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LABankTreeBL";
            tError.functionName = "dealData";
            tError.errorMessage = "执行SQL语句：从表中取值失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tAgentSeries;
    }

    private boolean check() {
        System.out.println("---start check----");
        //代理人基本信息
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        if (!tLAAgentDB.getInfo()) {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALABankAgentBL";
            tError.functionName = "check";
            tError.errorMessage = "查询代理人基础信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        } else {
            this.mOldLAAgentSchema = tLAAgentDB.getSchema();
        }

        LATreeDB ttLATreeDB = new LATreeDB();
        ttLATreeDB.setAgentCode(tAgentCode);
        if (!ttLATreeDB.getInfo()) {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALABankAgentBL";
            tError.functionName = "check";
            tError.errorMessage = "查询代理人行政信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        } else {
            this.mOldLATreeSchema = ttLATreeDB.getSchema();
        }
        //校验调动日期
        String newStartDate = AgentPubFun.formatDate(this.tDate, "yyyy-MM-dd");
        System.out.println("newStartDate:" + newStartDate);
        String tDay = newStartDate.substring(newStartDate.lastIndexOf("-") + 1);
        if (!tDay.equals("01")) {
            CError tError = new CError();
            tError.moduleName = "LABankTreeBL";
            tError.functionName = "check";
            tError.errorMessage = "调整日期必须是从某个月的1号开始!";
            this.mErrors.addOneError(tError);
            return false;
        } else {

        	return true;
        }
    }
}