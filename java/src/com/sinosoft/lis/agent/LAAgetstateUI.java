/*
 * <p>ClassName: ALAComUI </p>
 * <p>Description: ALAComUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 代理机构
 * @CreateDate：2003-01-09
 */
package com.sinosoft.lis.agent;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.lis.schema.LAContSchema;
import com.sinosoft.lis.vschema.LAComSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;


public class  LAAgetstateUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 存放查询结果 */
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAgentSchema tLAAgentSchema = new LAAgentSchema();
    public  LAAgetstateUI()
    {
    }
    	public boolean submitData(VData mInputData,String cOperate) {
    		System.out.println("88888888888888" + mInputData);
    		System.out.println("99999999999999" + cOperate);
    		LAAgetstateBL bl = new  LAAgetstateBL();
    		if (!bl.submitData(mInputData, cOperate)) {
    			mErrors.copyAllErrors(bl.mErrors);
    			return false;
    		}

    		return true;
    	}

    }
