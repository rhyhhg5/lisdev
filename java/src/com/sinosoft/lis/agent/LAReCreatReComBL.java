package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LARearRelationDB;
import com.sinosoft.lis.db.LARecomRelationDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LARearRelationSchema;
import com.sinosoft.lis.schema.LARecomRelationSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.lis.vschema.LAWarrantorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

/**
 * <p>Title: AgentSystem</p>
 * <p>Description: 销售管理——增员管理</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author xjh
 * @version 1.0
 */

public class LAReCreatReComBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private MMap map = new MMap();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String AgentName="";
    private String mIsManager;
    private String mOrphanCode = "N";
    private String mReComAgentCode="";
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private LAWarrantorSet mLAWarrantorSet = new LAWarrantorSet();
    private LARearRelationSet mLARearRelationSet = new LARearRelationSet();
    private Reflections ref = new Reflections();
    private LATreeInterface mLATreeInterface;
    private LAAgentInterface mLAAgentInterface;
    private LAAscriptionInterface mLAAscriptionInterface;
    private LAOrphanPolicyInterface mLAOrphanPolicyInterface;
    private LARearInterface mLARearInterface;
    private LARecomInterface mLARecomInterface;
    private String mAgentCode = "";
    private String mEdorNo = "";
    private String mAgentGrade="";

    public String getAgentCode() {
        return mAgentCode;
    }

    public LAReCreatReComBL() {
    }

    public static void main(String[] args) {
    	LAReCreatReComBL laagentblf = new LAReCreatReComBL();
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Begin LAAgentEditBLF.submitData.........");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //处理修改推荐人
        if(mReComAgentCode!=null && !"".equals(mReComAgentCode))
        {
        	if(!dealReferData(mReComAgentCode))
            {
            	return false;
            }
        }
        
        
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start LLAAgentEditLF Submit...");
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentBLF";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //modify by fuxin  2014-12-3
        UniCommon tUniCommon = new UniCommon();
        boolean tFlag = tUniCommon.getGroupAgentCode(mOperate, mAgentCode, mLAAgentSchema, "");
        if(!tFlag){
        	this.mErrors = tUniCommon.mErrors;
        	return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        System.out.println("Begin LAAgentEditBLF.getInputData.........");
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mIsManager = (String) cInputData.getObject(1);
        System.out.print(mIsManager);
        this.AgentName=(String)cInputData.getObject(2);
        System.out.print(AgentName);
        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                                      getObjectByObjectName("LAAgentSchema", 0));
        this.mLATreeSchema.setSchema((LATreeSchema) cInputData.
                                     getObjectByObjectName("LATreeSchema", 0));
        this.mLAWarrantorSet.set((LAWarrantorSet) cInputData.
                                 getObjectByObjectName("LAWarrantorSet", 0));
        this.mLARearRelationSet.set((LARearRelationSet) cInputData.
                                    getObjectByObjectName("LARearRelationSet",
                0));
        if (mLAAgentSchema.getBranchType().equals("1") &&
            mLAAgentSchema.getBranchType2().equals("01")) {
            this.mOrphanCode = (String) cInputData.getObject(7);
            System.out.print(mOrphanCode);
        }
        this.mReComAgentCode = (String)cInputData.getObject(8);
        System.out.println(mReComAgentCode);
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentEditBLF";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 业务处理主函数
     * @return boolean
     */
    public boolean dealData() {
    	//miaoxz adds here at 2008-9-5
    	//当修改主管名字的时候也同时要修改labranchgroup表的branchmanagername字段
    	System.out.println(".................dealdata here.....");
    	System.out.println("..........this.agentname:"+this.AgentName);
    	System.out.println("..........this.mLAAgentSchema.getName():"+this.mLAAgentSchema.getName());
    	System.out.println("..........this.mIsManager:"+this.mIsManager);
    	if(this.mReComAgentCode != null && !"".equals(this.mReComAgentCode))
    	{
    		mLATreeSchema.setIntroAgency(this.mReComAgentCode);
    	}
    	if(!this.AgentName.equals(this.mLAAgentSchema.getName())
    			&&checkIsManager(this.mLAAgentSchema.getAgentCode())){
    		try{
    			System.out.println(".................dealdata here.....begin dealManagerName");
    			dealManagerName();
    		}catch(Exception e){
    			e.printStackTrace();
    			CError tError = new CError();
                tError.moduleName = "LAAgentEditBLF";
                tError.functionName = "dealManagerName";
                tError.errorMessage = "修改相应的团队主管姓名时出错。";
                this.mErrors.addOneError(tError);
                return false;
    		}
    	}
        System.out.println("Begin LAAgentEditDealBL.dealData.........");
        //查找项目组名称
        String tSQL = "select VarValue from LASysVar where VarType = 'prname' ";
        ExeSQL tExeSQL = new ExeSQL();
        String Project = "" + tExeSQL.getOneValue(tSQL);
        //如果没有查到应该调用的程序,则调用product对应的程序组。
        Project = (Project.equals("") || Project.equals("null")) ? "Product" :
                  Project;
        System.out.println("Project:" + Project);
        System.out.println("mOrphanCode:" + mOrphanCode);
//        /**获取集团工号Unisalescod*/
//        tSQL = "select GroupAgentCode from laagent where agentcode='"+mLAAgentSchema.getAgentCode()+"'";
//        String mGroupAgentCode = tExeSQL.getOneValue(tSQL);
//        UnisalescodFun fun=new UnisalescodFun();
//        RspUniSalescod mRspUniSalescod = new RspUniSalescod();
//        mRspUniSalescod=fun.getUnisalescod(mOperate, mLAAgentSchema, null,mGroupAgentCode,null );
//        if("01".equals(mRspUniSalescod.getMESSAGETYPE()))
//		{
//			 CError tError = new CError();
//	         tError.moduleName = "LAAgentEditBLF";
//	         tError.functionName = "dealData";
//	         tError.errorMessage = mRspUniSalescod.getERRDESC();
//	         this.mErrors.addOneError(tError);
//			return false;
//		}else{
//			 mLAAgentSchema.setGroupAgentCode(mGroupAgentCode);
//		}
        /**生成代理编码AgentCode*/
        if (!getCreateCodeClass(Project)) {
            return false;
        }

        if (mOrphanCode.equals("Y")) {
            if (!getLAAscriptionClass(Project)) {
                return false;
            }

            //传入参数
            mLAAscriptionInterface.setAgentCode(mAgentCode);
            mLAAscriptionInterface.setGlobalInput(mGlobalInput);
            //处理
            if (!mLAAscriptionInterface.dealdata()) {
                mErrors.copyAllErrors(mLAAscriptionInterface.mErrors);
                return false;
            }
            //获取处理结果
            MMap tMap8 = mLAAscriptionInterface.getResult();
            map.add(tMap8);

            if (!getLAOrphanPolicyClass(Project)) {
                return false;
            }
            //传入参数
            mLAOrphanPolicyInterface.setAgentCode(mAgentCode);
            mLAOrphanPolicyInterface.setGlobalInput(mGlobalInput);
            //处理
            if (!mLAOrphanPolicyInterface.dealdata()) {
                mErrors.copyAllErrors(mLAOrphanPolicyInterface.mErrors);
                return false;
            }
            //获取处理结果
            MMap tMap9 = mLAOrphanPolicyInterface.getResult();
            map.add(tMap9);

        }
        /**处理基本信息*/
        //获取处理类
        if (!getAgentClass(Project)) {
            return false;
        }

        //传入参数
        mLAAgentInterface.setOperate(mOperate);
        mLAAgentInterface.setAgentCode(mAgentCode);
        mLAAgentInterface.setEdorNo(mEdorNo);
        mLAAgentInterface.setGlobalInput(mGlobalInput);
        mLAAgentInterface.setLAAgentSchema(mLAAgentSchema);
        mLAAgentInterface.setLATreeSchema(mLATreeSchema);
        mLAAgentInterface.setLAWarrantorSet(mLAWarrantorSet);
        mLAAgentInterface.setOperate(mOperate);
        //处理
        if (!mLAAgentInterface.dealdata()) {
            mErrors.copyAllErrors(mLAAgentInterface.mErrors);
            return false;
        }
        //获取处理结果
        MMap tMap1 = mLAAgentInterface.getResult();
        map.add(tMap1);

        /**处理行政信息*/

        //获取处理类
        if (!getTreeClass(Project)) {
            return false;
        }
        //传入参数
        mLATreeInterface.setOperate(mOperate);
        mLATreeInterface.setAgentCode(mAgentCode);
        mLATreeInterface.setEdorNo(mEdorNo);
        mLATreeInterface.setGlobalInput(mGlobalInput);
        mLATreeInterface.setIsManager(mIsManager);
        mLATreeInterface.setLATreeSchema(mLATreeSchema);
        mLATreeInterface.setLAAgentSchema(mLAAgentInterface.
                                          getLAAgentSchema());
        
        //处理
        if (!mLATreeInterface.dealdata()) {
            mErrors.copyAllErrors(mLATreeInterface.mErrors);
            return false;
        }
        //获取处理结果
        MMap tMap2 = mLATreeInterface.getResult();
        map.add(tMap2);

  /**
        if (mLAAgentSchema.getBranchType().equals("1")) {
            if (!getRecomClass(Project)) {
                return false;
            }
            //传入推荐关系参数，save页面将推荐人放在了mLATreeSchema的IntroAgency字段
            mLATreeSchema.setAgentCode(mAgentCode);
            mLARecomInterface.setOperate(mOperate);
            mLARecomInterface.setGlobalInput(mGlobalInput);
            //mLARecomInterface.setLATreeSchema(mLATreeInterface.
            //                                  getLATreeSchema());
            mLARecomInterface.setLATreeSchema(mLATreeSchema);
            mLARecomInterface.setEdorNo(mEdorNo);

            //处理推荐关系
            if (!mLARecomInterface.dealdata()) {
                mErrors.copyAllErrors(mLATreeInterface.mErrors);
                return false;
            }
            //获取处理结果
            MMap tMap3 = mLARecomInterface.getResult();
            map.add(tMap3);

            //处理育成关系
            //获取处理类
            if (!getRearClass(Project)) {
                return false;
            }
            //传入参数
            mLARearInterface.setOperate(mOperate);
            mLARearInterface.setLATreeSchema(mLATreeInterface.
                                             getLATreeSchema());
            mLARearInterface.setEdorNo(mEdorNo);
            mLARearInterface.setGlobalInput(mGlobalInput);
            //处理
            if (!mLARearInterface.dealdata()) {
                mErrors.copyAllErrors(mLARearInterface.mErrors);
                return false;
            }
            //获取处理结果
            MMap tMap4 = mLARearInterface.getResult();
            map.add(tMap4);
        }
*/
        return true;
    }
     
    public boolean dealReferData(String cAgentCode)
    {
    	if(!addRecomRelation(this.mAgentCode,cAgentCode)){
    		CError tError = new CError();
            tError.moduleName = "LANewToFormBL";
            tError.functionName = "submitData";
            tError.errorMessage = "未知错误，建立推荐关系失败!";
            mErrors.addOneError(tError);
            return false;
    	}
    	mAgentGrade = this.mLATreeSchema.getAgentGrade();
    	if(mAgentGrade!=null&&!mAgentGrade.equals("")&&mAgentGrade.compareTo("B01")>0){
    		System.out.println("哆啦A梦");
    	   if(!addRearRelation(this.mAgentCode,cAgentCode)){
    		CError tError = new CError();
            tError.moduleName = "LANewToFormBL";
            tError.functionName = "submitData";
            tError.errorMessage = "未知错误，建立养成关系失败!";
            mErrors.addOneError(tError);
            return false;
    	  }
    	}
    	
    	return true;
    }
    
    private boolean addRecomRelation(String tAgentCode,String tRecomAgentCode){
         String sql="select * from larecomrelation where agentcode='"+tAgentCode+"'";
         ExeSQL tExeSQL1 = new ExeSQL();
         String tResult=tExeSQL1.getOneValue(sql);
         if(tResult!=null&&!tResult.equals("")){
        	 String tSQL1="delete from larecomrelation where agentcode='"+tAgentCode+"'";
        	 ExeSQL tExeSQL2 = new ExeSQL();
             boolean tValue1 = tExeSQL2.execUpdateSQL(tSQL1);
         }
    	LATreeDB tLATreeDB=new LATreeDB();
    	tLATreeDB.setAgentCode(tAgentCode);
    	tLATreeDB.getInfo();
    	
    	//修改业务员原有的推荐关系
    	
    	String tAgentSeries=tLATreeDB.getSchema().getAgentSeries();
    	LATreeDB tLATreeDB2=new LATreeDB();
    	tLATreeDB2.setAgentCode(tRecomAgentCode);
    	tLATreeDB2.getInfo();
        LAAgentDB tLAAgentDB=new LAAgentDB();
    	tLAAgentDB.setAgentCode(tAgentCode);
    	tLAAgentDB.getInfo();
    	String tEmployDate=tLAAgentDB.getSchema().getEmployDate();
    	String tAgentGroup=tLATreeDB.getSchema().getAgentGroup();
    	String tRecomLevel="";
    	if(tAgentSeries.equals("2")){
    		tRecomLevel="02";
    	}else{
    		tRecomLevel="01";
    	}
    	LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();
    	LARecomRelationSet tLARecomRelationSet = new LARecomRelationSet();
    	tLARecomRelationDB.setRecomLevel(tRecomLevel);
    	tLARecomRelationDB.setRecomGens("1");
    	tLARecomRelationDB.setAgentCode(tAgentCode);
    	tLARecomRelationDB.setRecomAgentCode(tRecomAgentCode);
    	tLARecomRelationDB.setAgentGroup(tAgentGroup);
    	tLARecomRelationDB.setStartDate(tEmployDate);
    	tLARecomRelationDB.setEndDate(" ");
    	tLARecomRelationDB.setRecomFlag("1");
    	tLARecomRelationDB.setRecomComFlag("1");
    	tLARecomRelationDB.setRecomStartYear("1");
    	tLARecomRelationDB.setRate("0");
    	tLARecomRelationDB.setMakeDate(currentDate);
    	tLARecomRelationDB.setMakeTime(currentTime);
    	tLARecomRelationDB.setModifyDate(currentDate);
    	tLARecomRelationDB.setModifyTime(currentTime);
    	tLARecomRelationDB.setOperator(mGlobalInput.Operator);
    	tLARecomRelationDB.setDeductFlag("");
    	tLARecomRelationDB.setInheritCalFlag("");
    	tLARecomRelationDB.setRecomBonusFlag("");
    	tLARecomRelationDB.setDeductRate("0");
    	tLARecomRelationDB.setAgentGrade(tLATreeDB.getSchema().getAgentGrade());
    	tLARecomRelationDB.setRecomAgentGrade(tLATreeDB2.getSchema().getAgentGrade());
    	tLARecomRelationSet.add(tLARecomRelationDB);
    	map.put(tLARecomRelationSet, "INSERT");
    	//查询推荐人推荐关系
    	LARecomRelationDB tLARecomRelationDB1 = new LARecomRelationDB();
    	tLARecomRelationDB1.setAgentCode(tRecomAgentCode);
    	tLARecomRelationDB1.setRecomFlag("1");//已失效的关系不需要再进行复制
    	LARecomRelationSet tLARecomRelationSet1 = new LARecomRelationSet();
    	tLARecomRelationSet1=tLARecomRelationDB1.query();
    	if(tLARecomRelationSet1!=null&&!tLARecomRelationSet1.equals("")){
    		LARecomRelationSet tLARecomRelationSet2 = new LARecomRelationSet();
    		for(int i=1;i<=tLARecomRelationSet1.size();i++){
    			LARecomRelationSchema tLARecomRelationSchema=new LARecomRelationSchema();
    			ref.transFields(tLARecomRelationSchema, tLARecomRelationSet1.get(i));
    			tLARecomRelationSchema.setRecomLevel(tLARecomRelationSchema.getRecomLevel());
    			tLARecomRelationSchema.setRecomGens(tLARecomRelationSchema.getRecomGens()+1);
    			tLARecomRelationSchema.setAgentCode(tAgentCode);
    			tLARecomRelationSchema.setRecomAgentCode(tLARecomRelationSchema.getRecomAgentCode());
    			tLARecomRelationSchema.setAgentGroup(tAgentGroup);
    			tLARecomRelationSchema.setStartDate(tEmployDate);
    			tLARecomRelationSchema.setEndDate("");
    			tLARecomRelationSchema.setRecomFlag("1");
    			tLARecomRelationSchema.setRecomComFlag("1");
    			tLARecomRelationSchema.setRecomStartYear("1");
    			tLARecomRelationSchema.setRate("0");
    			tLARecomRelationSchema.setMakeDate(currentDate);
    			tLARecomRelationSchema.setMakeTime(currentTime);
    			tLARecomRelationSchema.setModifyDate(currentDate);
    			tLARecomRelationSchema.setModifyTime(currentTime);
    			tLARecomRelationSchema.setOperator(mGlobalInput.Operator);
    			tLARecomRelationSchema.setDeductFlag("");
    			tLARecomRelationSchema.setInheritCalFlag("");
    			tLARecomRelationSchema.setRecomBonusFlag("");
    			tLARecomRelationSchema.setDeductRate("0");
    			tLARecomRelationSchema.setAgentGrade(tLATreeDB.getSchema().getAgentGrade());
    			tLARecomRelationSchema.setRecomAgentGrade(tLARecomRelationSchema.getRecomAgentGrade());
    			tLARecomRelationSet2.add(tLARecomRelationSchema);
    		}
    		map.put(tLARecomRelationSet2, "INSERT");
    	}
    	return true;
    }
    private boolean addRearRelation(String tAgentCode,String tRecomAgentCode){
    	System.out.println("大熊猫");
    	LATreeDB tLATreeDB=new LATreeDB();
    	tLATreeDB.setAgentCode(tAgentCode);
    	tLATreeDB.getInfo();
    	String tAgentSeries=tLATreeDB.getSchema().getAgentSeries();
    	LATreeDB tLATreeDB2=new LATreeDB();
    	tLATreeDB2.setAgentCode(tRecomAgentCode);
    	tLATreeDB2.getInfo();
      	String tAgentSeries2=tLATreeDB2.getSchema().getAgentSeries();
        LAAgentDB tLAAgentDB=new LAAgentDB();
    	tLAAgentDB.setAgentCode(tAgentCode);
    	tLAAgentDB.getInfo();
    	String tEmployDate=tLAAgentDB.getSchema().getEmployDate();
    	String tAgentGroup=tLATreeDB.getSchema().getAgentGroup();
    	String RearLevel="";
    	if(tAgentSeries.equals("2")){
    		RearLevel="02";
    	}else{
    		RearLevel="01";
    	}
    	String sql="select * from larearrelation where agentcode='"+tAgentCode+"'";
          ExeSQL tExeSQL1 = new ExeSQL();
          String tResult=tExeSQL1.getOneValue(sql);
          if(tResult!=null&&!tResult.equals("")){
         	 String tSQL1="delete from larearrelation where agentcode='"+tAgentCode+"'";
         	 ExeSQL tExeSQL2 = new ExeSQL();
              boolean tValue1 = tExeSQL2.execUpdateSQL(tSQL1);
          }
          LARearRelationDB tLARearRelationDB = new LARearRelationDB();
          LARearRelationSet tLARearRelationSet = new LARearRelationSet();
          tLARearRelationDB.setRearLevel(RearLevel);
          tLARearRelationDB.setRearedGens("1");
          tLARearRelationDB.setAgentCode(tAgentCode);
          tLARearRelationDB.setRearAgentCode(tRecomAgentCode);
          tLARearRelationDB.setAgentGroup(tAgentGroup);
          tLARearRelationDB.setstartDate(tEmployDate);
          tLARearRelationDB.setEndDate("");
          tLARearRelationDB.setRearFlag("1");//育成关系必须是有效的
          tLARearRelationDB.setRearCommFlag("1");
          tLARearRelationDB.setRearStartYear("1");
          tLARearRelationDB.setMakeDate(currentDate);
          tLARearRelationDB.setMakeTime(currentTime);
          tLARearRelationDB.setModifyDate(currentDate);
          tLARearRelationDB.setModifyTime(currentTime);
          tLARearRelationDB.setOperator(mGlobalInput.Operator);
          tLARearRelationSet.add(tLARearRelationDB);
          LARearRelationDB tLARearRelationDB1 = new LARearRelationDB();
          LARearRelationSet tLARearRelationSet1 = new LARearRelationSet();
          tLARearRelationDB1.setAgentCode(tRecomAgentCode);
          tLARearRelationDB1.setRearFlag("1");//推荐人育成关系必须是有效的
          tLARearRelationSet1=tLARearRelationDB1.query();
          if(tLARearRelationSet1.size()>=1){
        	  LARearRelationSet tLARearRelationSet2= new LARearRelationSet();
        	  for(int i=1;i<=tLARearRelationSet1.size();i++){
        		  LARearRelationSchema tLARearRelationSchema=new LARearRelationSchema();
        		  ref.transFields(tLARearRelationSchema, tLARearRelationSet1.get(i));
        		  tLARearRelationSchema.setRearLevel(tLARearRelationSchema.getRearLevel());
        		  tLARearRelationSchema.setRearedGens(tLARearRelationSchema.getRearedGens()+1);
        		  tLARearRelationSchema.setAgentCode(tAgentCode);
        		  tLARearRelationSchema.setRearAgentCode(tLARearRelationSchema.getRearAgentCode());
        		  tLARearRelationSchema.setAgentGroup(tAgentGroup);
        		  tLARearRelationSchema.setstartDate(tEmployDate);
        		  tLARearRelationSchema.setEndDate("");
        		  tLARearRelationSchema.setRearFlag("1");
        		  tLARearRelationSchema.setRearCommFlag("1");
        		  tLARearRelationSchema.setRearStartYear("1");
        		  tLARearRelationSchema.setMakeDate(currentDate);
        		  tLARearRelationSchema.setMakeTime(currentTime);
        		  tLARearRelationSchema.setModifyDate(currentDate);
        		  tLARearRelationSchema.setModifyTime(currentTime);
        		  tLARearRelationSchema.setOperator(mGlobalInput.Operator);
        		  tLARearRelationSet2.add(tLARearRelationSchema);
        	  }
        	  if(tAgentSeries.equals(tAgentSeries2)){
        		  map.put(tLARearRelationSet2, "INSERT");
          	}
          }
          if(tAgentSeries.equals(tAgentSeries2)){
        	  map.put(tLARearRelationSet, "INSERT");
      	}
    	return true;
    } 
    /**
     *
     */
    private boolean checkIsManager(String agentcode){
    	String tSql="select agentgroup from labranchgroup where branchmanager='"
    		+agentcode+"'";
    	ExeSQL tExeSQL=new ExeSQL();
    	String tResult=tExeSQL.getOneValue(tSql);
    	if(tResult!=null && !tResult.equals("")){
    		return true;
    	}else {
    		return false;
    	}

    }
    /**
     * miaoxz adds here in 2008.09.08
     * 当修改主管系列代理人名字时，同步更新其所领导团队的主管姓名。
     */
    private void dealManagerName() throws Exception{
    	String tSql="select * from labranchgroup where branchmanager='"
    		+this.mLAAgentSchema.getAgentCode()+"'";
    	LABranchGroupDB tLABranchGroupDB=new LABranchGroupDB();
    	LABranchGroupSet tLABranchGroupSet=new LABranchGroupSet();
    	tLABranchGroupSet=tLABranchGroupDB.executeQuery(tSql);
    	MMap tMMap=new MMap();
    	String tEdorNo="";
    	for(int i=1;i<=tLABranchGroupSet.size();i++){
    		LABranchGroupSchema tLABranchGroupSchema=new LABranchGroupSchema();
    		LABranchGroupBSchema tLABranchGroupBSchema=new LABranchGroupBSchema();
    		tLABranchGroupSchema=tLABranchGroupSet.get(i);
    		Reflections tReflections = new Reflections();
            tReflections.transFields(tLABranchGroupBSchema, tLABranchGroupSchema);
            tEdorNo=PubFun1.CreateMaxNo("EdorNo", 20);
    		System.out.println(".........edorno here "+tEdorNo);
    		tLABranchGroupBSchema.setEdorNo(tEdorNo);
    		tLABranchGroupBSchema.setEdorType("05");
    		tLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.getMakeDate());
    		tLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.getMakeTime());
    		tLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.
                                                 getModifyDate());
    		tLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.
                                                 getModifyTime());
    		tLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.getOperator());
    		tLABranchGroupBSchema.setMakeDate(this.currentDate);
    		tLABranchGroupBSchema.setMakeTime(this.currentTime);
    		tLABranchGroupBSchema.setModifyDate(this.currentDate);
    		tLABranchGroupBSchema.setModifyTime(this.currentTime);
    		tLABranchGroupBSchema.setOperator(this.mGlobalInput.Operator);
    		tMMap.put(tLABranchGroupBSchema,"INSERT");
//    		tLABranchGroupBSchema=(LABranchGroupBSchema)tLABranchGroupSchema.;
    		tLABranchGroupSchema.setBranchManagerName(this.mLAAgentSchema.getName());
    		tLABranchGroupSchema.setModifyDate(this.currentDate);
    		tLABranchGroupSchema.setModifyTime(this.currentTime);
    		tLABranchGroupSchema.setOperator(this.mGlobalInput.Operator);
    		tMMap.put(tLABranchGroupSchema,"UPDATE");
    	}
    	if(tMMap.size()>=1){
    		System.out.println("...............add map here");
    		this.map.add(tMMap);
    	}
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            System.out.println("Begin LAAgentEditBLF.prepareOutputData.........");
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentEditBLF";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 如果AgentCode不存在（即新增人员），生成AgentCode
     * @param cName String
     * @return boolean
     */
    private boolean getCreateCodeClass(String cName) {
        LACreateCodeInterface tLACreateCodeInterface;
        try {
            Class mCreateCodeClass = Class.forName(
                    "com.sinosoft.lis.agent.LACreateCode" + cName + "BL");
            tLACreateCodeInterface = (LACreateCodeInterface)
                                     mCreateCodeClass.
                                     newInstance();
        } catch (ClassNotFoundException ex1) {
            System.out.println(
                    ".........Not Found LACreateCode" + cName + "BL，错误");
            if (!cName.equals("Product")) {
                if (!getCreateCodeClass("Product")) {
                    return false;
                }
            }
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAAgentDealBL";
            tError.functionName = "getCreateCodeClass";
            tError.errorMessage = "没有找到LACreateCode" + cName + "BL类！";
            this.mErrors.addOneError(tError);
            return false;
        } catch (Exception ex1) {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAAgentDealBL";
            tError.functionName = "getCreateCodeClass";
            tError.errorMessage = "调用LACreateCode" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        tLACreateCodeInterface.setManagecom(mLAAgentSchema.getManageCom());
        tLACreateCodeInterface.setOperate(mOperate);
        tLACreateCodeInterface.setBranchType(mLAAgentSchema.getBranchType());
        tLACreateCodeInterface.setBranchType2(mLAAgentSchema.getBranchType2());
        //处理
        if (!tLACreateCodeInterface.dealdata()) {
            mErrors.copyAllErrors(tLACreateCodeInterface.mErrors);
            return false;
        }
        if (mOperate.equals("INSERT||MAIN")) {
            //获取处理结果
            mAgentCode = tLACreateCodeInterface.getAgentCode();
        } else {
            mAgentCode = mLAAgentSchema.getAgentCode();
        }
        mEdorNo = tLACreateCodeInterface.getEdorNo();
        System.out.println("B表流水号mEdorNo:  " + mEdorNo);
        return true;
    }

    /**
     * 获得代理人基本信息处理类
     * @param cName String
     * @return boolean
     */
    private boolean getAgentClass(String cName) {
        try {
            Class mLAAgentClass = Class.forName(
                    "com.sinosoft.lis.agent.LAAgent" + cName + "BL");
            mLAAgentInterface = (LAAgentInterface) mLAAgentClass.
                                newInstance();
        } catch (ClassNotFoundException ex1) {
            System.out.println(
                    ".........Not Found LAAgent" + cName + "BL，错误");
            if (!cName.equals("Product")) {
                if (!getAgentClass("Product")) {
                    return false;
                }
            }
            return true;
        } catch (Exception ex1) {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAAgentDealBL";
            tError.functionName = "getAgentClass";
            tError.errorMessage = "调用LAAgent" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 获得     基本信息处理类
     * @param cName String
     * @return boolean
     */
    private boolean getLAAscriptionClass(String cName) {
        try {
            Class mLAAscriptionClass = Class.forName(
                    "com.sinosoft.lis.agent.LAAscription" + cName + "BL");
            mLAAscriptionInterface = (LAAscriptionInterface) mLAAscriptionClass.
                                     newInstance();
        } catch (ClassNotFoundException ex1) {
            System.out.println(
                    ".........Not Found LAAscription" + cName + "BL，错误");
            if (!cName.equals("Product")) {
                if (!getLAAscriptionClass("Product")) {
                    return false;
                }
            }
            return true;
        } catch (Exception ex1) {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAAscriptionDealBL";
            tError.functionName = "getLAAscriptionClass";
            tError.errorMessage = "调用LAAscription" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 获得     基本信息处理类
     * @param cName String
     * @return boolean
     */
    private boolean getLAOrphanPolicyClass(String cName) {
        try {
            Class mLAOrphanPolicyClass = Class.forName(
                    "com.sinosoft.lis.agent.LAOrphanPolicy" + cName + "BL");
            mLAOrphanPolicyInterface = (LAOrphanPolicyInterface)
                                       mLAOrphanPolicyClass.
                                       newInstance();
        } catch (ClassNotFoundException ex1) {
            System.out.println(
                    ".........Not Found LAOrphanPolicy" + cName + "BL，错误");
            if (!cName.equals("Product")) {
                if (!getLAOrphanPolicyClass("Product")) {
                    return false;
                }
            }
            return true;
        } catch (Exception ex1) {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAOrphanPolicyDealBL";
            tError.functionName = "getLAOrphanPolicyClass";
            tError.errorMessage = "调用LAOrphanPolicy" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 获得行政信息处理类
     * @param cName String
     * @return boolean
     */
    private boolean getTreeClass(String cName) {
        try {
            Class mLATreeClass = Class.forName(
                    "com.sinosoft.lis.agent.LATree" + cName + "BL");
            mLATreeInterface = (LATreeInterface) mLATreeClass.
                               newInstance();
        } catch (ClassNotFoundException ex1) {
            System.out.println(
                    ".........Not Found LATree" + cName + "BL，错误");
            if (!getTreeClass("Product")) {
                return false;
            }
            return true;
        } catch (Exception ex1) {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAAgentDealBL";
            tError.functionName = "getTreeClass";
            tError.errorMessage = "调用LATree" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 获得育成关系处理类
     * @param cName String
     * @return boolean
     */
    private boolean getRearClass(String cName) {
        try {
            Class mLARearClass = Class.forName(
                    "com.sinosoft.lis.agent.LARear" + cName + "BL");
            mLARearInterface = (LARearInterface) mLARearClass.
                               newInstance();
        } catch (ClassNotFoundException ex1) {
            System.out.println(
                    ".........Not Found LARear" + cName + "BL，错误");
            if (!getRearClass("Product")) {
                return false;
            }
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAAgentDealBL";
            tError.functionName = "getRearClass";
            tError.errorMessage = "没有找到LARear" + cName + "BL类！";
            this.mErrors.addOneError(tError);
            return false;
        } catch (Exception ex1) {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAAgentDealBL";
            tError.functionName = "getRearClass";
            tError.errorMessage = "调用LARear" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 获得推荐关系处理类
     * @param cName String
     * @return boolean
     */
    private boolean getRecomClass(String cName) {
        try {
            Class mLARecomClass = Class.forName(
                    "com.sinosoft.lis.agent.LARecom" + cName + "BL");
            mLARecomInterface = (LARecomInterface) mLARecomClass.
                                newInstance();
        } catch (ClassNotFoundException ex1) {
            System.out.println(
                    ".........Not Found LARecom" + cName + "BL，错误");
            if (!getRecomClass("Product")) {
                return false;
            }
            return true;
        } catch (Exception ex1) {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAAgentBLF";
            tError.functionName = "getRecomClass";
            tError.errorMessage = "调用LARecom" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
