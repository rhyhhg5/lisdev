/**
 * <p>ClassName: BankDimissionUI </p>
 * <p>Description: BankDimissionUI类文件 </p>
 * <p>Copyright: Copyright (c) 2005.3</p>
 * @Database: 销售管理
 * <p>Company: sinosoft</p>
 * @author zhanghui
 */
package com.sinosoft.lis.agent;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.LADimissionSet;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
public class LAZJDimissionBackUI
{

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    private String mOperate;


    public LAZJDimissionBackUI()
    {
    }

    public boolean submitData(VData cInputData,String cOperate)
    {
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        LAZJDimissionBackBL bl = new LAZJDimissionBackBL();
        if (!bl.submitData(mInputData,mOperate))
        {
            this.mErrors.copyAllErrors(bl.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAZJDimissionBackUI";
            tError.functionName = "submitData";
            tError.errorMessage = bl.mErrors.getFirstError();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
