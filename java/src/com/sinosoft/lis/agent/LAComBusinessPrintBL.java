package com.sinosoft.lis.agent;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LAComBusinessPrintBL {
	 /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    
    private String mSqlTitle = null;
    private String mSql = null;
    private String mTitle = null;
    private String mOutXmlPath = null;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    SSRS tSSRSmSqlTitle=new SSRS();
    SSRS tSSRSmTitle=new SSRS();
    SSRS tSSRSmSql=new SSRS();
    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        
        if(!check()){
        	return false;
        }
        //进行业务处理
        if (!dealData()) {
        	
            return false;
        }
       
        return true;
    }
  

    /**
     * 业务处理方法
     * @return boolean
     */
    private boolean dealData()
    {
    	//得到报表表头
    	ExeSQL tExeSQL = new ExeSQL();
    	tSSRSmTitle = tExeSQL.execSQL(mTitle); 
    	if(tSSRSmTitle.mErrors.needDealError()){
    		CError tError = new CError();
    		  tError.moduleName = "LAComBusinessPrintBL";
              tError.functionName = "dealData";
              tError.errorMessage = "没有查询到需要下载的数据";
              mErrors.addOneError(tError);
    		return false;
    	}
    	
//    	得到列表列名
    	tSSRSmSqlTitle = tExeSQL.execSQL(mSqlTitle); 
    	if(tSSRSmSqlTitle.mErrors.needDealError()){
    		CError tError = new CError();
    		  tError.moduleName = "LAComBusinessPrintBL";
              tError.functionName = "dealData";
              tError.errorMessage = "没有查询到需要下载的数据";
              mErrors.addOneError(tError);
    		return false;
    	}
    	
//    	得到真正的数据
    	tSSRSmSql = tExeSQL.execSQL(mSql); 
    	if(tSSRSmSql.mErrors.needDealError()){
    		CError tError = new CError();
    		  tError.moduleName = "LAComBusinessPrintBL";
              tError.functionName = "dealData";
              tError.errorMessage = "没有查询到需要下载的数据";
              mErrors.addOneError(tError);
    		return false;
    	}
    	
    	String[][] mToExcel = new String[tSSRSmSql.getMaxRow()+4][20];
    	//将表头放在列表的【0】0】位置
    	mToExcel[0][0] = tSSRSmTitle.GetText(1, 1);
    	//列表名放在第二行
    	for(int row=1; row <=tSSRSmSqlTitle.getMaxRow();row++){
    		
    		for(int col = 1;col<=tSSRSmSqlTitle.getMaxCol();col++){
    			mToExcel[row][col-1]=tSSRSmSqlTitle.GetText(row, col);
    		}
    	}
//    	数据从第三行开始循环向下
    	for(int row=1; row <=tSSRSmSql.getMaxRow();row++){
    		
    		for(int col = 1;col<=tSSRSmSql.getMaxCol();col++){
    			mToExcel[row+1][col-1]=tSSRSmSql.GetText(row, col);
    		}
    	}
    	try{
    		WriteToExcel wte = new WriteToExcel("");
        	wte.createExcelFile();
        	String[] sheetName ={PubFun.getCurrentDate()};
        	wte.addSheet(sheetName);
        	wte.setData(0, mToExcel);
        	wte.write(mOutXmlPath);
    	} catch(Exception ex)
        {
            ex.printStackTrace();
        }
    	
        return true;
    }
    
    
    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean check()
    {

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        TransferData tfd = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        
        
        if (mGlobalInput == null||tfd==null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentComBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        mSql = (String) tfd.getValueByName("querySql");
        mSqlTitle = (String) tfd.getValueByName("querySqlTitle");
        mTitle = (String) tfd.getValueByName("Title");
        mOutXmlPath = (String) tfd.getValueByName("OutXmlPath");
        
        
        return true;
    }
  


}
