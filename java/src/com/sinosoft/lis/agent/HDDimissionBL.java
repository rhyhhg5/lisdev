/*
 * <p>ClassName: BankDimissionBL </p>
 * <p>Description: BankDimissionBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agent;
import com.cbsws.obj.RspUniSalescod;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LADimissionDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LADimissionSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.lis.schema.LARecomRelationSchema;
import com.sinosoft.lis.agentbranch.ChangeAgentToBranch;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.agentbranch.ChangeRearBranch;
import com.sinosoft.lis.schema.LARearRelationSchema;
import com.sinosoft.lis.schema.LARearRelationBSchema;
import com.sinosoft.lis.vschema.LARearRelationBSet;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LARecomRelationBSchema;
import com.sinosoft.lis.vschema.LARecomRelationBSet;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LAOrphanPolicySet;
import com.sinosoft.lis.schema.LAOrphanPolicySchema;
import com.sinosoft.lis.sys.UnisalescodFun;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.vschema.LCContSet;

public class HDDimissionBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LADimissionSchema mLADimissionSchema = new LADimissionSchema();
    private LADimissionSet mLADimissionSet = new LADimissionSet();
    private LABranchGroupSet  mupLABranchGroupSet = new LABranchGroupSet();
    private LARecomRelationSet mLARecomRelationSet = new LARecomRelationSet();
    private LARecomRelationBSet mLARecomRelationBSet = new LARecomRelationBSet();
    private LARearRelationSet mLARearRelationSet = new LARearRelationSet();
    private LARearRelationBSet mLARearRelationBSet = new LARearRelationBSet();
    private LABranchGroupBSet  minLABranchGroupBSet= new LABranchGroupBSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAAgentBSchema mLAAgentBSchema = new LAAgentBSchema();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LATreeBSet mLATreeBSet = new LATreeBSet();
    private LAOrphanPolicySet mLAOrphanPolicySet = new LAOrphanPolicySet();
    private MMap mMap = new MMap();
    private String currentDate ="";
    private String currentTime ="";
    private String  mEdorNo="";
    public HDDimissionBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankDimissionBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败BankDimissionBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "BankDimissionBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
         }

        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String tSQL = "select * from laagent where agentcode='"+mLADimissionSchema.getAgentCode()+"'";
        LAAgentDB mLAAgentDB = new LAAgentDB();
        LAAgentSet tlaagentset=new LAAgentSet();
        tlaagentset= mLAAgentDB.executeQuery(tSQL);
        UnisalescodFun fun=new UnisalescodFun();
        RspUniSalescod mRspUniSalescod = new RspUniSalescod();
        mRspUniSalescod=fun.getUnisalescod("UPDATE||MAIN", tlaagentset.get(1), mLADimissionSchema, tlaagentset.get(1).getGroupAgentCode(),null);
        if ("01".equals(mRspUniSalescod.getMESSAGETYPE())) {
			CError tError = new CError();
			tError.moduleName = "BankDimissionBL";
			tError.functionName = "dealData";
			tError.errorMessage = mRspUniSalescod.getERRDESC();
			this.mErrors.addOneError(tError);
			return false;
		} 
        mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        currentDate = PubFun.getCurrentDate();
        currentTime = PubFun.getCurrentTime();
        if (mOperate.equals("INSERT||MAIN"))
        {
            //确定离职次数

            //操作员
            //在LADimission表里插入离职状态信息
             LADimissionDB tLADimissionDB = new LADimissionDB();
             tLADimissionDB.setAgentCode(mLADimissionSchema.getAgentCode());
             tLADimissionDB.setDepartTimes(mLADimissionSchema.getDepartTimes());
             if (!tLADimissionDB.getInfo())
             {
                 CError tError = new CError();
                 tError.moduleName = "BankDimissionBL";
                 tError.functionName = "dealData";
                 tError.errorMessage = "查询代理人原离职信息失败!";
                 this.mErrors.addOneError(tError);
                 return false;
             }


             if (this.mLADimissionSchema.getDepartTimes() == 1)
            {
                this.mLADimissionSchema.setDepartState("06"); //离职确认
            }
            else
            {
                this.mLADimissionSchema.setDepartState("07"); //二次离职确认
            }
            mLADimissionSchema.setBranchAttr(AgentPubFun.getAgentBranchAttr(
            mLADimissionSchema.getAgentCode())); //转换机构编码
            mLADimissionSchema.setMakeDate(tLADimissionDB.getMakeDate());
            mLADimissionSchema.setMakeTime(tLADimissionDB.getMakeTime());
            mLADimissionSchema.setModifyDate(currentDate);
            mLADimissionSchema.setModifyTime(currentTime);
            mLADimissionSchema.setOperator(mGlobalInput.Operator);
        }
        if (mOperate.equals("UPDATE||MAIN"))
        {
            System.out.println("1111111111111111111111111111111111111111111");
            LADimissionDB tLADimissionDB = new LADimissionDB();
            tLADimissionDB.setAgentCode(mLADimissionSchema.getAgentCode());
            tLADimissionDB.setDepartTimes(mLADimissionSchema.getDepartTimes());
            if (!tLADimissionDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "BankDimissionBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询代理人原离职信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mLADimissionSchema.setBranchAttr(AgentPubFun.getAgentBranchAttr(
                   mLADimissionSchema.getAgentCode()));
            this.mLADimissionSchema.setDepartState(tLADimissionDB.getDepartState());
            this.mLADimissionSchema.setMakeDate(tLADimissionDB.getMakeDate());
            this.mLADimissionSchema.setMakeTime(tLADimissionDB.getMakeTime());
            this.mLADimissionSchema.setModifyDate(currentDate);
            this.mLADimissionSchema.setModifyTime(currentTime);
            mLADimissionSchema.setOperator(mGlobalInput.Operator);
        }
        //修改代理人信息表中的代理人状态字段和离职日期
        if (this.mOperate.equals("INSERT||MAIN") ||
            this.mOperate.equals("UPDATE||MAIN"))
        {
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(this.mLADimissionSchema.getAgentCode());
            System.out.println(this.mLADimissionSchema.getAgentCode());
            if (!tLAAgentDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "BankDimissionBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询代理人信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            Reflections  tReflections= new Reflections();
            tReflections.transFields(mLAAgentBSchema,tLAAgentDB.getSchema());
            mLAAgentBSchema.setEdorNo(mEdorNo);
            mLAAgentBSchema.setEdorType("08");
            tLAAgentDB.setOutWorkDate(this.mLADimissionSchema.getDepartDate());
            if (this.mLADimissionSchema.getDepartTimes() == 1)
            {
                tLAAgentDB.setAgentState("06"); //一次离职确认
            }
            else
            {
                tLAAgentDB.setAgentState("07"); //二次离职确认
            }
            tLAAgentDB.setOperator(this.mGlobalInput.Operator);
            tLAAgentDB.setModifyDate(currentDate);
            tLAAgentDB.setModifyTime(currentTime);
            this.mLAAgentSchema.setSchema(tLAAgentDB);
        }
//        LATreeDB tLATreeDB = new LATreeDB();
//        String tAgentCode=this.mLADimissionSchema.getAgentCode();//离职人员人员
//        LATreeSchema tLATreeSchema = new LATreeSchema();
//        tLATreeDB.setAgentCode(tAgentCode);
//        tLATreeDB.getInfo();
//        tLATreeSchema=tLATreeDB.getSchema();
//        String tAgentGroup=tLATreeSchema.getAgentGroup();//离职人员的团队
//        String tBranchCode = tLATreeSchema.getBranchCode();//离职人员的团队
//
//        String tAgentGrade = tLATreeSchema.getAgentGrade();
//        String tAgentSeris=AgentPubFun.getAgentSeries(tAgentGrade);

//        if(tAgentSeris.equals("0")   )
//        {
//            //组经理的处理离职,不包括见习经理
//            if (!dealManager(tAgentCode,tAgentGroup)) {
//                return false;
//            }
//        }
//        else if (tAgentSeris.equals("1"))
//        {
//            //部经理离职的处理
//            if (!dealWarden(tAgentCode, tAgentGroup, tBranchCode))
//            {
//                return false;
//            }
//        }

        tReturn = true;
        return tReturn;
    }

    /**
     *  查询该需离职确认的业务员的所有保单，记录成孤儿单
     * @param cAgentCode String
     * @return boolean
     */
    private boolean createOrphanPolicy(String cAgentCode)
    {
        LATreeDB tLATreeDB = new LATreeDB();
        LATreeSchema tLATreeSchema = new LATreeSchema();
        tLATreeDB.setAgentCode(cAgentCode);
        tLATreeDB.getInfo();
        tLATreeSchema = tLATreeDB.getSchema();
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = new LCContSet();
        tLCContDB.setAgentCode(cAgentCode);
        tLCContSet = tLCContDB.query();
        if (tLCContSet.size()>0)
        {

            for (int i = 1; i <= tLCContSet.size(); i++)
            {
                //判断是否为撤销保单，要是为撤销保单则不生成孤儿单
                String tSQL = "select '1' from lccont where uwflag<>'a' and contno='"+tLCContSet.get(i).getContNo()+"'";
                ExeSQL tExeSQL = new ExeSQL();
                String Flag = tExeSQL.getOneValue(tSQL);
                if(Flag!=null&&!Flag.equals(""))
                {
                LAOrphanPolicySchema tLAOrphanPolicySchema = new LAOrphanPolicySchema();
                tLAOrphanPolicySchema.setContNo(tLCContSet.get(i).getContNo());
                tLAOrphanPolicySchema.setManageCom(tLCContSet.get(i).getManageCom());
                tLAOrphanPolicySchema.setAgentCode(cAgentCode);
                tLAOrphanPolicySchema.setAppntNo(tLCContSet.get(i).getAppntNo());
                tLAOrphanPolicySchema.setReasonType("0");
                tLAOrphanPolicySchema.setBranchType(tLATreeSchema.getBranchType());
                tLAOrphanPolicySchema.setBranchType2(tLATreeSchema.getBranchType2());
                tLAOrphanPolicySchema.setAgentGroup(tLATreeSchema.getBranchCode());
                tLAOrphanPolicySchema.setFlag("1");  //正常参与分配
                tLAOrphanPolicySchema.setMakeDate(PubFun.getCurrentDate());
                tLAOrphanPolicySchema.setMakeTime(PubFun.getCurrentTime());
                tLAOrphanPolicySchema.setModifyDate(PubFun.getCurrentDate());
                tLAOrphanPolicySchema.setModifyTime(PubFun.getCurrentTime());
                tLAOrphanPolicySchema.setOperator(mGlobalInput.Operator);
                mLAOrphanPolicySet.add(tLAOrphanPolicySchema);
            }
            }
        }
        return true;
    }

    /*处经理的处理,不包括见习经理
     * 处经理离职后,其下面的人员需要调动到 处经理的直接培养人的团队,如果没有直接培养人,则不作处理,
     * 此团队为无主管团队
    */
    private boolean dealManager(String tAgentCode,String tAgentGroup)
    {

        LATreeSet tLATreeSet = new LATreeSet();
        LATreeDB tLATreeDB = new LATreeDB();
        //查询团队内除了离职人员 的所有人,以便进行人员调动
        String tSql = "select * from  latree a where agentgroup='" +
                      tAgentGroup
                      + "' and agentcode<>'" + tAgentCode + "'"
                      + "  and  not exists "
                      + " (select b.agentcode from laagent b where a.agentcode=b.agentcode and b.agentstate>='06' ) ";
        tLATreeSet = tLATreeDB.executeQuery(tSql);

        //  团队为无主管团队
        if (!dealBranch()) {
            buildError("dealWarden", "修改团队为无主管团队时出错!");
            return false;
        }
        //置upagent 为 空
        if (!dealupAgent(tLATreeSet)) {
            buildError("dealWarden", "修改团队为无主管团队时出错!");
            return false;
        }

        return true;
    }
     //区经理的处理
    private boolean dealWarden(String tAgentCode,String tAgentGroup,String tBranchCode)
    {

        LATreeSet tLATreeSet = new LATreeSet();
        LATreeDB tLATreeDB = new LATreeDB();
        //查询团队下除了离职主管人员 的所有人,以便进行人员调动
        String tSql = "select * from  latree a where upagent='" +
                      tAgentCode
                      + "' and agentcode<>'" + tAgentCode + "'"
                      + "  and  not exists "
                      + " (select b.agentcode from laagent b where a.agentcode=b.agentcode and b.agentstate>='06' ) ";
        tLATreeSet = tLATreeDB.executeQuery(tSql);
        // 置原团队为无主管团队
        if (!dealBranch()) {
            buildError("dealWarden", "修改团队为无主管团队时出错!");
            return false;
        }
        //置upagent 为 空
        if (!dealupAgent(tLATreeSet)) {
            buildError("dealWarden", "修改团队为无主管团队时出错!");
            return false;
        }
        return true;
    }



    //处理主管所在的团队 (如果是区经理,则还要处理直辖处),使团队为无主管团队
    private boolean dealBranch()
    {
        //主管离职的处理 团队停业
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setBranchManager(this.mLADimissionSchema.getAgentCode());
        System.out.println(this.mLADimissionSchema.getAgentCode());

        tLABranchGroupSet = tLABranchGroupDB.query();
        for (int i = 1; i <= tLABranchGroupSet.size(); i++) {
            LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
            LABranchGroupBSchema tLABranchGroupBSchema = new LABranchGroupBSchema();
            tLABranchGroupSchema = tLABranchGroupSet.get(i);
            //备份labranchgroupb表
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLABranchGroupBSchema, tLABranchGroupSchema);
            System.out.println(tLABranchGroupBSchema.getAgentGroup() + "|||||2");
            tLABranchGroupBSchema.setEdorType("08"); //修改操作

            tLABranchGroupBSchema.setEdorNo(mEdorNo);
            tLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.getMakeDate());
            tLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.getMakeTime());
            tLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.
                                                 getModifyDate());
            tLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.
                                                 getModifyTime());
            tLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.getOperator());
            tLABranchGroupBSchema.setMakeDate(currentDate);
            tLABranchGroupBSchema.setMakeTime(currentTime);
            tLABranchGroupBSchema.setModifyDate(currentDate);
            tLABranchGroupBSchema.setModifyTime(currentTime);
            tLABranchGroupBSchema.setOperator(this.mGlobalInput.Operator);
            minLABranchGroupBSet.add(tLABranchGroupBSchema);
            //修改labranchgroup 中的主管为空
            tLABranchGroupSchema.setBranchManager("");
            tLABranchGroupSchema.setBranchManagerName("");
            tLABranchGroupSchema.setModifyDate(currentDate);
            tLABranchGroupSchema.setModifyTime(currentTime);
            tLABranchGroupSchema.setOperator(mGlobalInput.Operator);
            mupLABranchGroupSet.add(tLABranchGroupSchema);
        }
        return true ;
    }
    //处理主管所在的团队 (如果是区经理,则还要处理直辖处),使团队为无主管团队
    private boolean dealupAgent(LATreeSet tLATreeSet)
    {
        //主管离职的处理 团队停业
        if(tLATreeSet==null || tLATreeSet.size()<=0)
        {
            return true ;
        }
        for (int i=1;i<=tLATreeSet.size();i++)
        {
            LATreeSchema tLATreeSchema= new LATreeSchema();
            tLATreeSchema=tLATreeSet.get(i);
            LATreeBSchema tLATreeBSchema= new LATreeBSchema();
            Reflections tReflections= new Reflections();
            tReflections.transFields(tLATreeBSchema,tLATreeSchema) ;
            tLATreeBSchema.setEdorNO(mEdorNo);
            tLATreeBSchema.setRemoveType("08");
            tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
            tLATreeBSchema.setMakeDate2(tLATreeBSchema.getMakeDate());
            tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
            tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
            tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
             tLATreeBSchema.setOperator(mGlobalInput.Operator);
            tLATreeBSchema.setMakeDate(currentDate);
            tLATreeBSchema.setMakeTime(currentTime);
            tLATreeBSchema.setModifyDate(currentDate);
            tLATreeBSchema.setModifyTime(currentTime);
            mLATreeBSet.add(tLATreeBSchema);
            tLATreeSchema.setUpAgent("");
            tLATreeSchema.setModifyDate(currentDate);
            tLATreeSchema.setModifyTime(currentTime);
            mLATreeSet.add(tLATreeSchema);
        }

        return true ;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLADimissionSchema.setSchema((LADimissionSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LADimissionSchema",
                                                  0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start BankDimissionBLQuery Submit...");
        LADimissionDB tLADimissionDB = new LADimissionDB();
        tLADimissionDB.setSchema(this.mLADimissionSchema);
        this.mLADimissionSet = tLADimissionDB.query();
        this.mResult.add(this.mLADimissionSet);
        System.out.println("End BankDimissionBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLADimissionDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLADimissionDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "BankDimissionBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mMap.put(this.mLADimissionSchema,"UPDATE");
            this.mMap.put(this.mLAAgentSchema,"UPDATE");
            this.mMap.put(this.mLAAgentBSchema,"INSERT");
            this.mMap.put(this.mupLABranchGroupSet,"UPDATE");
            this.mMap.put(this.minLABranchGroupBSet,"INSERT");
            this.mMap.put(this.mLATreeSet,"UPDATE");
            this.mMap.put(this.mLATreeBSet,"INSERT");
            this.mMap.put(this.mLAOrphanPolicySet,"DELETE&INSERT");
            mInputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankDimissionBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    /**
     * 校验函数
     * 对传入的数据进行信息校验
     * @return boolean
     */
    private boolean checkData()
    {

        String tAgentCode;
        tAgentCode = mLADimissionSchema.getAgentCode(); //得到离职人员编码
        System.out.println("离职人员代码："+mLADimissionSchema.getAgentCode());
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        //查询代理人表
        //对查询结果判定
        if (!tLAAgentDB.getInfo())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "BankDimissionBL";
            tError.functionName = "checkData()";
            tError.errorMessage = "查询失败,不存在该代理人";
            this.mErrors.addOneError(tError);
            return false;
        }
        else 
        {
        	if(!"03".equals(tLAAgentDB.getSchema().getAgentState())){
	            //代理人编码录入错误
	            CError tError = new CError();
	            tError.moduleName = "BankDimissionBL";
	            tError.functionName = "checkData()";
	            tError.errorMessage = "代理人未离职登记，请先离职登记";
	            this.mErrors.addOneError(tError);
	            return false;
	        }
        }
        ExeSQL tExeSQL = new ExeSQL();
        
        //离职确认前，关于业务未签单和未回执回销的保单校验
        StringBuffer agentContSql= new StringBuffer();
                     agentContSql=agentContSql.append("select '1' from lccont")
                     			 .append(" where grpcontno = '00000000000000000000' and agentcode = '")
                     			 .append(tAgentCode)
                     			 .append("' and (appflag <> '1' or appflag is null) and stateflag <> '3'")
                     			 .append(" and ((uwflag <> 'a' and uwflag <> '8' and uwflag <> '1') or uwflag is null)")
                     			 .append(" and not exists  (select 'X' from lcrnewstatelog where newcontno = lccont.contno)")
                     			 .append(" union ")
                     			 .append("select '1' from lccont")
                     			 .append(" where grpcontno = '00000000000000000000' and agentcode = '")
                     			 .append(tAgentCode)
                     			 .append("'  and (customgetpoldate is null or getpoldate is null) and stateflag <> '3'")
                     			 .append(" and ((uwflag <> 'a' and uwflag <> '8' and uwflag <> '1') or uwflag is null)")
                     			 .append(" and not exists  (select 'X' from lcrnewstatelog where newcontno = lccont.contno)")
                     			 .append(" union ")
                     			 .append("select '1' from lcgrpcont")
                     			 .append(" where grpcontno <> '00000000000000000000' and agentcode = '")
                     			 .append(tAgentCode)
                     			 .append("' and (appflag <> '1' or appflag is null) and stateflag <> '3'")
                     			 .append(" and ((uwflag <> 'a' and uwflag <> '8' and uwflag <> '1') or uwflag is null)")
                     			 .append(" and not exists  (select 'X' from lcrnewstatelog where newgrpcontno = lcgrpcont.grpcontno)")
                     			 .append(" union ")
                     			 .append("select '1' from lcgrpcont")
                     			 .append(" where grpcontno <> '00000000000000000000' and agentcode = '")
                     			 .append(tAgentCode)
                     			 .append("'  and (customgetpoldate is null or getpoldate is null) and stateflag <> '3'")
                     			 .append(" and ((uwflag <> 'a' and uwflag <> '8' and uwflag <> '1') or uwflag is null)")
                     			 .append(" and not exists  (select 'X' from lcrnewstatelog where newgrpcontno = lcgrpcont.grpcontno)");
       System.out.println(agentContSql.toString());           
       String contResult = tExeSQL.getOneValue(agentContSql.toString());
       
       if("1".equals(contResult)){
           CError tError = new CError();
           tError.moduleName = "BankDimissionBL";
           tError.functionName = "checkData()";
           tError.errorMessage = "该业务员还有未签单和未回执回销的保单,不能进行离职确认";
           this.mErrors.addOneError(tError);
           System.out.println("业务员"+tAgentCode+"还有未签单和未回执回销的保单,不能进行离职确认");
           return false;
       }
       //离职确认前，应分配续期保单明细校验
       String renewalSql = "select '1' from lcpol a where a.grpcontno='00000000000000000000' "
    	   				+"and a.stateflag='1' and exists (select 1 from lmriskapp where riskcode=a.riskcode and riskperiod ='L') " 
    	   				+"and payintv<>0 and a.agentcode='"+tAgentCode +"'"
    	   				+" union "
    	   				+"select '1' from lcgrppol a where a.grpcontno<>'00000000000000000000'" 
    	   				+" and a.stateflag='1' and exists (select 1 from lmriskapp where riskcode=a.riskcode and riskperiod ='L') " 
    	   				+"and payintv<>0 and a.agentcode='"+tAgentCode +"'";
       String renewalResult = tExeSQL.getOneValue(renewalSql);
       if("1".equals(renewalResult)){
           CError tError = new CError();
           tError.moduleName = "BankDimissionBL";
           tError.functionName = "checkData()";
           tError.errorMessage = "该业务员还有未分配的续期保单,不能进行离职确认";
           this.mErrors.addOneError(tError);
          	System.out.println("业务员"+tAgentCode+"还有未分配的续期保单,不能进行离职确认");
          	return false;
       }
       
         return true;
    }
    //错误处理
    private void buildError(String szFunc, String szErrMsg) {
            CError cError = new CError();

            cError.moduleName = "AdjustAgentBL";
            cError.functionName = szFunc;
            cError.errorMessage = szErrMsg;
            this.mErrors.addOneError(cError);
    }
}
