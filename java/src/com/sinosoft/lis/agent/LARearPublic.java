package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LARearRelationDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LARearRelationBSchema;
import com.sinosoft.lis.schema.LARearRelationSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.Reflections;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
abstract public class LARearPublic implements LARearInterface
{
    MMap map = new MMap();
    String mOperate = "";
    GlobalInput mGlobalInput = new GlobalInput();
    LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    LATreeSchema mLATreeSchema = new LATreeSchema();
    LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();
    LARearRelationSet mLARearRelationSet = new LARearRelationSet();
    LARearRelationSet mNewAddLARearRelationSet = new LARearRelationSet();
    LARearRelationSchema mLARearRelationSchema;
    String mEdorNo = "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    public void setOperate(String cOperate) //设置操作数
    {
        mOperate = cOperate;
    }

    public void setGlobalInput(GlobalInput cGlobalInput) //设置全局变量
    {
        mGlobalInput.setSchema(cGlobalInput);
    }

    public void setLARearRelationSet(LARearRelationSet cLARearRelationSet) //设置育成信息
    {
        mLARearRelationSet.set(cLARearRelationSet);
    }

    public void setLAAgentSchema(LAAgentSchema cLAAgentSchema) //设置代理人编码
    {
        mLAAgentSchema.setSchema(cLAAgentSchema);
    }

    public void setLATreeSchema(LATreeSchema cLATreeSchema)
    {
        mLATreeSchema.setSchema(cLATreeSchema);
    }

    public void setEdorNo(String cEdorNo) //设置转储编码
    {
        mEdorNo = cEdorNo;
    }

    public abstract boolean dealdata();

    public MMap getResult()
    {
        return map;
    }

    public LARearPublic()
    {
    }

    public boolean addRear()
    {
        String tAgentCode = mLAAgentSchema.getAgentCode();
        String tAgentGroup = mLAAgentSchema.getAgentGroup().trim();
        String tStartDate = mLATreeSchema.getStartDate().trim();
        //将被育成人的所在机构全部找出
        mLABranchGroupSet.clear();
        if (!findSelfBranch(tAgentCode, tAgentGroup))
        {
            return false;
        }
        //循环处理每条育成信息
        for (int i = 1; i <= mLARearRelationSet.size(); i++)
        {
            String tRearGroup = "";
            mLARearRelationSchema = new LARearRelationSchema();
            mLARearRelationSchema = mLARearRelationSet.get(i);
            String tBranchLevel = mLARearRelationSchema.getRearLevel().trim();
            for (int j = 1; j <= mLABranchGroupSet.size(); j++)
            {
                LABranchGroupSchema tLABranchGroupSchema = new
                        LABranchGroupSchema();
                tLABranchGroupSchema = mLABranchGroupSet.get(j);
                if (tLABranchGroupSchema.getBranchLevel().trim().equals(
                        tBranchLevel))
                {
                    tRearGroup = tLABranchGroupSchema.getAgentGroup();
                }
            }
            String tRearAgentCode = mLARearRelationSchema.getRearAgentCode().
                                    trim();
            //按条件复制育成人的所有记录
            LARearRelationSet tLARearRelationSet = new LARearRelationSet();
            LARearRelationDB tLARearRelationDB = new LARearRelationDB();
            tLARearRelationDB.setAgentCode(tRearAgentCode);
            tLARearRelationDB.setRearLevel(tBranchLevel);
            tLARearRelationSet = tLARearRelationDB.query();
            if (tLARearRelationDB.mErrors.needDealError())
            {
                CError tError = new CError();
                tError.moduleName = "LARearPublic";
                tError.functionName = "addRear";
                tError.errorMessage = "查询育成信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            for (int j = 1; j <= tLARearRelationSet.size(); j++)
            {
                LARearRelationSchema tLARearRelationSchema = new
                        LARearRelationSchema();
                tLARearRelationSchema = tLARearRelationSet.get(j);
                tLARearRelationSchema.setRearLevel(tBranchLevel);
                tLARearRelationSchema.setRearedGens(tLARearRelationSchema.
                        getRearedGens() + 1);
                tLARearRelationSchema.setAgentCode(tAgentCode);
                tLARearRelationSchema.setRearAgentCode(tRearAgentCode);
                tLARearRelationSchema.setAgentGroup(tRearGroup);
                tLARearRelationSchema.setstartDate(tStartDate);
                tLARearRelationSchema.setRearFlag("1");
                //EndDate
                tLARearRelationSchema.setRearCommFlag("1");
                tLARearRelationSchema.setRearStartYear("1");
                tLARearRelationSchema.setMakeDate(currentDate);
                tLARearRelationSchema.setMakeTime(currentTime);
                tLARearRelationSchema.setModifyDate(currentDate);
                tLARearRelationSchema.setModifyTime(currentTime);
                tLARearRelationSchema.setOperator(mGlobalInput.Operator);
                mNewAddLARearRelationSet.add(tLARearRelationSchema);
                map.put(tLARearRelationSchema, "INSERT");
            }
            //插入被育成人一代记录
            //mLARearRelationSchema.setRearLevel(tBranchLevel);
            mLARearRelationSchema.setRearedGens(1);
            mLARearRelationSchema.setAgentCode(tAgentCode);
            //mLARearRelationSchema.setRearAgentCode(tRearAgentCode);
            mLARearRelationSchema.setAgentGroup(tRearGroup);
            mLARearRelationSchema.setstartDate(tStartDate);
            mLARearRelationSchema.setRearFlag("1");
            //EndDate
            mLARearRelationSchema.setRearCommFlag("1");
            mLARearRelationSchema.setRearStartYear("1");
            mLARearRelationSchema.setMakeDate(currentDate);
            mLARearRelationSchema.setMakeTime(currentTime);
            mLARearRelationSchema.setModifyDate(currentDate);
            mLARearRelationSchema.setModifyTime(currentTime);
            mLARearRelationSchema.setOperator(mGlobalInput.Operator);
            mNewAddLARearRelationSet.add(mLARearRelationSchema);
            map.put(mLARearRelationSchema, "INSERT");
        }
        return true;
    }

    public boolean ModifyRear()
    {
        //查找出AgentCode为被育成人的信息，删除
        String tAgentCode = mLAAgentSchema.getAgentCode();
        LARearRelationSet tLARearRelationSet = new LARearRelationSet();
        LARearRelationDB tLARearRelationDB = new LARearRelationDB();
        tLARearRelationDB.setAgentCode(tAgentCode);
        tLARearRelationSet = tLARearRelationDB.query();
        if (tLARearRelationDB.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "LARearPublic";
            tError.functionName = "ModifyRear";
            tError.errorMessage = "查询育成信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        map.put(tLARearRelationSet, "DELETE");
        //转储
        if (!edorRear(tLARearRelationSet))
        {
            return false;
        }
        //生成新的育成关系
        if (!addRear())
        {
            return false;
        }
        //查找出RearAgentCode为被育成人的信息
        tLARearRelationSet = new LARearRelationSet();
        tLARearRelationDB = new LARearRelationDB();
        tLARearRelationDB.setRearAgentCode(tAgentCode);
        tLARearRelationSet = tLARearRelationDB.query();
        if (tLARearRelationDB.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "LARearPublic";
            tError.functionName = "ModifyRear";
            tError.errorMessage = "查询育成信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //循环处理每条信息
        String tRearLevel = "";
        LARearRelationSchema tLARearRelationSchema;
        LARearRelationSet tLARearRelationSet1;
        LARearRelationDB tLARearRelationDB1;
        for (int i = 1; i <= tLARearRelationSet.size(); i++)
        {
            tAgentCode = "";
            tLARearRelationSchema = new
                                    LARearRelationSchema();
            tLARearRelationSchema = tLARearRelationSet.get(i);
            tRearLevel = tLARearRelationSchema.getRearLevel();
            tAgentCode = tLARearRelationSchema.getAgentCode();
            tLARearRelationSet1 = new LARearRelationSet();
            tLARearRelationDB1 = new LARearRelationDB();
            tLARearRelationDB1.setAgentCode(tAgentCode);
            tLARearRelationDB1.setRearLevel(tRearLevel);
            tLARearRelationSet1 = tLARearRelationDB1.query();
            if (tLARearRelationDB1.mErrors.needDealError())
            {
                CError tError = new CError();
                tError.moduleName = "LARearPublic";
                tError.functionName = "ModifyRear";
                tError.errorMessage = "查询育成信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            map.put(tLARearRelationSet1, "DELETE");
            //转储
            if (!edorRear(tLARearRelationSet))
            {
                return false;
            }
            //生成新的育成关系
            LAAgentSchema tLAAgentSchema = getLAAgentSchema(tAgentCode);
            LATreeSchema tLATreeSchema = getLATreeSchema(tAgentCode);
            if (tLAAgentSchema == null || tLATreeSchema == null)
            {
                CError tError = new CError();
                tError.moduleName = "LARearPublic";
                tError.functionName = "ModifyRear";
                tError.errorMessage = "查询代理人信息(育成)失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mLABranchGroupSet.clear();
            if (!findSelfBranch(tAgentCode, tLAAgentSchema.getAgentGroup().trim()))
            {
                return false;
            }
            String tRearGroup = "";
            for (int j = 1; j <= mLABranchGroupSet.size(); j++)
            {
                LABranchGroupSchema tLABranchGroupSchema = new
                        LABranchGroupSchema();
                tLABranchGroupSchema = mLABranchGroupSet.get(j);
                if (tLABranchGroupSchema.getBranchLevel().trim().equals(
                        tRearLevel))
                {
                    tRearGroup = tLABranchGroupSchema.getAgentGroup();
                }
            }
            for (int j = 1; j <= mNewAddLARearRelationSet.size(); j++)
            {
                LARearRelationSchema tLARearRelationSchema1 = new
                        LARearRelationSchema();
                tLARearRelationSchema1 = tLARearRelationSet.get(j);
                tLARearRelationSchema1.setRearLevel(tRearLevel);
                tLARearRelationSchema1.setRearedGens(tLARearRelationSchema.
                        getRearedGens() + tLARearRelationSchema1.getRearLevel());
                tLARearRelationSchema1.setAgentCode(tAgentCode);
                tLARearRelationSchema1.setRearAgentCode(tLARearRelationSchema1.
                        getAgentCode());
                tLARearRelationSchema1.setAgentGroup(tRearGroup);
                tLARearRelationSchema1.setstartDate(tLATreeSchema.getStartDate());
                tLARearRelationSchema1.setRearFlag("1");
                //EndDate
                tLARearRelationSchema1.setRearCommFlag("1");
                tLARearRelationSchema1.setRearStartYear("1");
                tLARearRelationSchema1.setMakeDate(currentDate);
                tLARearRelationSchema1.setMakeTime(currentTime);
                tLARearRelationSchema1.setModifyDate(currentDate);
                tLARearRelationSchema1.setModifyTime(currentTime);
                tLARearRelationSchema1.setOperator(mGlobalInput.Operator);
                map.put(tLARearRelationSchema1, "INSERT");
            }
        }
        return true;
    }

    public boolean edorRear(LARearRelationSet cLARearRelationSet)
    {
        Reflections tReflections;
        LARearRelationSchema tLARearRelationSchema;
        LARearRelationBSchema tLARearRelationBSchema;
        for (int i = 1; i <= cLARearRelationSet.size(); i++)
        {
            tLARearRelationSchema = new
                                    LARearRelationSchema();
            tLARearRelationBSchema = new
                                     LARearRelationBSchema();
            tLARearRelationSchema = cLARearRelationSet.get(i);
            tReflections = new Reflections();
            tReflections.transFields(tLARearRelationBSchema,
                                     tLARearRelationSchema);
            tLARearRelationBSchema.setEdorNo(mEdorNo);
            tLARearRelationBSchema.setEdorType("06");
            tLARearRelationBSchema.setMakeDate(currentDate);
            tLARearRelationBSchema.setMakeTime(currentTime);
            tLARearRelationBSchema.setModifyDate(currentDate);
            tLARearRelationBSchema.setModifyTime(currentTime);
            tLARearRelationBSchema.setOperator(this.mGlobalInput.Operator);
            map.put(tLARearRelationBSchema, "INSERT");
        }
        return true;
    }

    public LAAgentSchema getLAAgentSchema(String cAgentCode)
    {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(cAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "LARearPublic";
            tError.functionName = "ModifyRear";
            tError.errorMessage = "查询代理人基本信息失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLAAgentDB.getSchema();
    }

    public LATreeSchema getLATreeSchema(String cAgentCode)
    {
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(cAgentCode);
        if (!tLATreeDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "LARearPublic";
            tError.functionName = "ModifyRear";
            tError.errorMessage = "查询代理人行政信息失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLATreeDB.getSchema();
    }

    public boolean findSelfBranch(String cAgentCode, String cAgentGroup)
    {
        //所在各级直辖机构,非最高级
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setBranchManager(cAgentCode);
        tLABranchGroupDB.setUpBranchAttr("1");
        mLABranchGroupSet = tLABranchGroupDB.query();
        if (!tLABranchGroupDB.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "LARearPublic";
            tError.functionName = "findSelfBranch";
            tError.errorMessage = "查询机构信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //所在最高级机构
        tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(cAgentGroup);
        if (!tLABranchGroupDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "LARearPublic";
            tError.functionName = "findSelfBranch";
            tError.errorMessage = "查询机构信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLABranchGroupSet.set(mLABranchGroupSet.size() + 1,
                              tLABranchGroupDB.getSchema());
        return true;
    }
}
