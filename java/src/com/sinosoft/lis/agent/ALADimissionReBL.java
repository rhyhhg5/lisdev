/*
 * <p>ClassName: ALADimissionReBL </p>
 * <p>Description: ALADimissionReBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2004-12-01
 */
package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LADimissionDB;
import com.sinosoft.lis.db.LATreeAccessoryDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.lis.schema.LATreeAccessorySchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LADimissionSet;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class ALADimissionReBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String currentDate = PubFun.getCurrentDate();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private String tagentcode = "";
    private LADimissionSchema mLADimissionSchema = new LADimissionSchema();
    private LADimissionSet mLADimissionSet = new LADimissionSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private LATreeAccessorySet mLATreeAccessorySet = new LATreeAccessorySet();
    public ALADimissionReBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //校验
        if (!check())
        {
            return false;
        }
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALADimissionReBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALADimissionReBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("Start ALADimissionBL Submit...");

        ALADimissionReBLS tALADimissionReBLS = new ALADimissionReBLS();
        tALADimissionReBLS.submitData(mInputData, cOperate);
        System.out.println("End ALADimissionBL Submit...");

        //如果有需要处理的错误，则返回
        if (tALADimissionReBLS.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tALADimissionReBLS.mErrors);

            CError tError = new CError();
            tError.moduleName = "ALADimissionBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        //修改在LAAgent的纪录
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tagentcode);
        if (!tLAAgentDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALADimissionReBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询代理人信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLAAgentDB.getAgentState().equals("06"))
        {
            tLAAgentDB.setAgentState("01");
        }
        else if (tLAAgentDB.getAgentState().equals("07"))
        {
            tLAAgentDB.setAgentState("02");
        }
        tLAAgentDB.setOutWorkDate("");
        tLAAgentDB.setModifyDate(currentDate);
        this.mLAAgentSchema.setSchema(tLAAgentDB);
        //删除该离职人员的在LADimission表里的信息
        LADimissionDB tLADimissionDB = new LADimissionDB();
//    tLADimissionDB.setAgentCode(tagentcode);
//    tLADimissionDB.setDepartTimes(1) ;

        String sql = "select * from ladimission where agentcode='" + tagentcode +
                "' and departtimes=(select max(departtimes) from ladimission where agentcode='" +
                     tagentcode + "')";
        LADimissionSet tLADimissionSet = tLADimissionDB.executeQuery(sql);
        if (tLADimissionSet == null || tLADimissionSet.size() == 0)
        {
            this.mErrors.copyAllErrors(tLADimissionDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALADimissionReBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询代理人原离职信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        this.mLADimissionSchema.setSchema(tLADimissionSet.get(1));
        //修改在LATree表里的纪录
     /* by zhanghui 2005.3.21 不需要撤销LATree和LATreeAccessory表的记录(离职时没有修改)
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(tagentcode);
        if (!tLATreeDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "ALADimissionReBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询代理人行政信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        tLATreeDB.setState("1");
        tLATreeDB.setModifyDate(currentDate);
        this.mLATreeSchema.setSchema(tLATreeDB);
        //修改在LATreeAccessory表里的纪录
        LATreeAccessoryDB tLATreeAccessoryDB = new LATreeAccessoryDB();
        tLATreeAccessoryDB.setAgentCode(tagentcode);
        LATreeAccessorySet tSet = new LATreeAccessorySet();
        String tsql = "select * from latreeaccessory where agentcode='" +
                      tagentcode + "'";
        tSet = tLATreeAccessoryDB.executeQuery(tsql);
        int tCount = tSet.size();
        if (tCount > 0)
        {
            for (int i = 1; i <= tCount; i++)
            {
                LATreeAccessorySchema tSch = new LATreeAccessorySchema();
                tSch = tSet.get(i);
                String tagentgrade = tSch.getAgentGrade();
                String tAgentGrade = tLATreeDB.getAgentGrade();
                if (tagentgrade.compareToIgnoreCase(tAgentGrade) > 0)
                {
                    tSch.setRearFlag("1");
                }
                if (tagentgrade.compareToIgnoreCase(tAgentGrade) <= 0)
                {
                    tSch.setRearFlag("0");
                }
                tSch.setState("1");
                mLATreeAccessorySet.add(tSch);
            }

        }*/
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        tagentcode = (String) cInputData.get(0);
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void dealError(String FuncName, String ErrMsg)
    {
        CError tError = new CError();
        tError.moduleName = "ALADimissionReBL";
        tError.functionName = FuncName;
        tError.errorMessage = ErrMsg;
        mErrors.addOneError(tError);
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.tagentcode);
            this.mInputData.add(this.mLAAgentSchema);
            this.mInputData.add(this.mLADimissionSchema);
            this.mInputData.add(this.mLATreeAccessorySet);
            this.mInputData.add(this.mLATreeSchema);

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALADimissionReBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    private boolean check()
    {
        String asql = "select agentcode from laagent where idno=(select idno from laagent where agentcode='"
                      + tagentcode + "') and agentstate<='02'";
        SSRS aSSRS = new SSRS();
        ExeSQL aExeSQL = new ExeSQL();
        aSSRS = aExeSQL.execSQL(asql);
        int tcount = aSSRS.getMaxRow();
        if (tcount != 0)
        {
            String tAgentCode = aSSRS.GetText(1, 1);
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALADimissionReBL";
            tError.functionName = "check";
            tError.errorMessage = "该人员在系统中存在一个对应的代理人编码" + tAgentCode +
                                  "已经处于在职状态";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
