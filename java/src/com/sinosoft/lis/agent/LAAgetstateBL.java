/*
 * <p>ClassName: LAAgetstateBL </p>
 * <p>Description: ALADimissionBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agent;


import com.sinosoft.lis.db.LADimissionDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.lis.vschema.LADimissionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;

public class LAAgetstateBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LADimissionSchema mLADimissionSchema = new LADimissionSchema();
    private LADimissionSet mLADimissionSet = new LADimissionSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private MMap mMap = new MMap();
    private String mAgentState;
    public LAAgetstateBL()
    {
    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";
        String tsql="select *  from  ladimission where  agentcode='1101000114' ";
        LADimissionSchema tLADimissionSchema = new LADimissionSchema();
        LADimissionSet tLADimissionSet = new LADimissionSet();
        LADimissionDB tLADimissionDB = new LADimissionDB();
        tLADimissionSet=tLADimissionDB.executeQuery(tsql);
        tLADimissionSchema=tLADimissionSet.get(1);
        tLADimissionSchema.setDepartDate("2007-01-01");
        VData tVData = new VData();
        tVData.addElement(tLADimissionSchema);
        tVData.add(tG);

        ALADimissionBL  tALADimissionBL= new ALADimissionBL();
        tALADimissionBL.submitData(tVData,"INSERT||MAIN");
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgetstateBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAAgetstateBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgetstateBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
         }

        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        if (mOperate.equals("update"))
        {   
            System.out.println("我是美女！");
             //操作员
            //删除LADimission表里离职状态信息  
            LADimissionDB tLADimissionDB = new LADimissionDB();
            String tSQL = "select * from ladimission where agentcode='"+mLAAgentSchema.getAgentCode()+ "'";
            mLADimissionSet= tLADimissionDB.executeQuery(tSQL);
            if (!mLADimissionSet.equals("")&&mLADimissionSet!=null&&mLADimissionSet.size()>0)
            {   System.out.println("熊猫:");
              mLADimissionSchema=mLADimissionSet.get(1);
              String tSQL1="";
              if(this.mAgentState.equals("03")||this.mAgentState.equals("04")){
            	   tSQL1="delete from ladimission where agentcode='"+mLADimissionSchema.getAgentCode()+"'and departstate='"+this.mAgentState+"'";
              }
              else {
               tSQL1="delete from ladimission where agentcode='"+mLADimissionSchema.getAgentCode()+
              "' and departdate='"+mLADimissionSchema.getDepartDate()+"'";
              }
              ExeSQL tExeSQL = new ExeSQL();
              System.out.println("1111111111111111111");
              boolean tValue = tExeSQL.execUpdateSQL(tSQL1);
            }
               System.out.println("我是可爱的月亮！");
              //修改代理人信息表中的代理人状态字段和离职日期
              System.out.println(this.mLAAgentSchema.getAgentCode());
              String tSQL2="update laagent set agentstate='01',outworkdate=null,modifydate=current date,modifytime=current time where agentcode='"
            	  +this.mLAAgentSchema.getAgentCode()+"'";
              ExeSQL tExeSQL1 = new ExeSQL();
              boolean tValue1 = tExeSQL1.execUpdateSQL(tSQL2);
          	  LATreeDB tLATreeDB = new LATreeDB();
              String tAgentCode = this.mLAAgentSchema.getAgentCode(); //离职人员人员
              LATreeSchema tLATreeSchema = new LATreeSchema();
              tLATreeDB.setAgentCode(tAgentCode);
              tLATreeDB.getInfo();
              tLATreeSchema = tLATreeDB.getSchema();
              String tAgentGroup = tLATreeSchema.getAgentGroup(); //离职人员的团队
              String tBranchCode = tLATreeSchema.getBranchCode(); //离职人员的团队
              String tAgentGrade = tLATreeSchema.getAgentGrade();
              String tAgentSeris = AgentPubFun.getAgentSeries(tAgentGrade);
              if (tAgentSeris.equals("1")&&!tAgentGrade.equals("B01"))
              {
            	System.out.println("大熊猫");
             	 String tSQL3="update labranchgroup set branchmanager='"+tAgentCode+"',branchmanagername='"+this.mLAAgentSchema.getName()+"',modifydate=current date,modifytime=current time where agentgroup='"+tAgentGroup+"'";
                  ExeSQL tExeSQL2 = new ExeSQL();
                  boolean tValue2 = tExeSQL2.execUpdateSQL(tSQL3);
                  String tSQL9="update larearrelation set rearflag='1',modifydate=current date,modifytime=current time where agentcode='"+tAgentCode+"' and modifydate='"+this.mLAAgentSchema.getOutWorkDate()+"'";
                  ExeSQL tExeSQL8 = new ExeSQL();
                  boolean tValue8 = tExeSQL8.execUpdateSQL(tSQL9);
                  String tSQL10="delete from  larearrelationb where agentcode='"+tAgentCode+"' and modifydate='"+this.mLAAgentSchema.getOutWorkDate()+"'";
                  ExeSQL tExeSQL9 = new ExeSQL();
                  boolean tValue9 = tExeSQL9.execUpdateSQL(tSQL10);
              }else if(tAgentSeris.equals("2")&&tAgentGrade.equals("B11")){
            	  System.out.println("大熊猫1");
              	 String tSQL4="update labranchgroup set branchmanager='"+tAgentCode+"',branchmanagername='"+this.mLAAgentSchema.getName()+"',modifydate=current date,modifytime=current time where agentgroup='"+tAgentGroup+"'";
                   ExeSQL tExeSQL3 = new ExeSQL();
                   boolean tValue3 = tExeSQL3.execUpdateSQL(tSQL4);
                   String tSQL5="update labranchgroup set branchmanager='"+tAgentCode+"',branchmanagername='"+this.mLAAgentSchema.getName()+"',modifydate=current date,modifytime=current time where agentgroup='"+tBranchCode+"'";
                   ExeSQL tExeSQL4 = new ExeSQL();
                   boolean tValue4 = tExeSQL4.execUpdateSQL(tSQL5);
                   String tSQL9="update larearrelation set rearflag='1',modifydate=current date,modifytime=current time where agentcode='"+tAgentCode+"' and modifydate='"+this.mLAAgentSchema.getOutWorkDate()+"'";
                   ExeSQL tExeSQL8 = new ExeSQL();
                   boolean tValue8 = tExeSQL8.execUpdateSQL(tSQL9);
                   String tSQL10="delete from  larearrelationb where agentcode='"+tAgentCode+"' and modifydate='"+this.mLAAgentSchema.getOutWorkDate()+"'";
                   ExeSQL tExeSQL9 = new ExeSQL();
                   boolean tValue9 = tExeSQL9.execUpdateSQL(tSQL10);
              }
              String tSQL6="delete from laorphanpolicy where agentcode='"+tAgentCode+"'";
              ExeSQL tExeSQL5 = new ExeSQL();
              boolean tValue5 = tExeSQL5.execUpdateSQL(tSQL6);
              String tSQL7="update larecomrelation set recomflag='1',modifydate=current date,modifytime=current time where agentcode='"+tAgentCode+"' and modifydate='"+this.mLAAgentSchema.getOutWorkDate()+"'";
              ExeSQL tExeSQL6 = new ExeSQL();
              boolean tValue6 = tExeSQL6.execUpdateSQL(tSQL7);
              String tSQL8="delete from  larecomrelationb where agentcode='"+tAgentCode+"' and modifydate='"+this.mLAAgentSchema.getOutWorkDate()+"'";
              ExeSQL tExeSQL7 = new ExeSQL();
              boolean tValue7 = tExeSQL7.execUpdateSQL(tSQL8);
             
        }
        tReturn = true;
        return tReturn;
    }
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LAAgentSchema",
                                                  0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
       this.mAgentState=(String)cInputData.get(3);
       System.out.println("apple:"+this.mAgentState);
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start LAAgetstateBLQuery Submit...");
        LADimissionDB tLADimissionDB = new LADimissionDB();
        tLADimissionDB.setSchema(this.mLADimissionSchema);
        this.mLADimissionSet = tLADimissionDB.query();
        this.mResult.add(this.mLADimissionSet);
        System.out.println("End LAAgetstateBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLADimissionDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLADimissionDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgetstateBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
    	 try
         {
             this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
             this.mInputData.add(this.mMap);
         }
         catch (Exception ex)
         {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "LAAgentdeleBL";
             tError.functionName = "prepareData";
             tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
             this.mErrors.addOneError(tError);
             return false;
         }
         return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    /**
     * 校验函数
     * 对传入的数据进行信息校验
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
//    }
    }
}
