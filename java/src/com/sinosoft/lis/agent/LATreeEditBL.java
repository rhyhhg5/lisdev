/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LATreeAccessoryDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LATreeAccessorySchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


/*
 * <p>ClassName: LATreeEditBL </p>
 * <p>Description: LATreeEditBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-09
 */
public class LATreeEditBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();


    /** 业务处理相关变量 */
    private LAAgentBSchema mLAAgentBSchema = new LAAgentBSchema();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private LATreeBSchema mLATreeBSchema = new LATreeBSchema();
    private LATreeAccessorySet mLATreeAccessorySet = new LATreeAccessorySet();
//    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();
//    private LAAgentSet mLAAgentSet = new LAAgentSet();
    public LATreeEditBL()
    {
    }

    public static void main(String[] args)
    {
        LATreeEditBL tLATreeEditBL = new LATreeEditBL();
        LATreeSchema tLATreeSchema = new LATreeSchema();
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86110000";
        tG.Operator = "aa";
        tLATreeSchema.setAgentCode("8613000222");
        tLATreeSchema.setAgentGrade("A05");
        tLATreeSchema.setAscriptSeries("::8611000001");
        tLATreeSchema.setAstartDate("2003-07-01");
        tLATreeSchema.setEduManager("8611000001");
        VData tVData = new VData();
        tVData.add(tG);
        tVData.add(tLATreeSchema);
        if (!tLATreeEditBL.submitData(tVData, "UPDATE||MAIN"))
        {
            System.out.println("Error:" + tLATreeEditBL.mErrors.getFirstError());
        }
        else
        {
            System.out.println("---------ok-----------");
        }
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LATreeEditBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!check())
        {
            return false;
        }

        //校验该代理人是否在黑名单中存在
        //if (!checkData())
        //return false;
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LATreeEditBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LATreeEditBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("over dealData");
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start LATreeEditBL Submit...");
            LATreeEditBLS tLATreeEditBLS = new LATreeEditBLS();
            tLATreeEditBLS.submitData(mInputData, cOperate);
            System.out.println("End LATreeEditBL Submit...");
            //如果有需要处理的错误，则返回
            if (tLATreeEditBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeEditBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "LATreeEditBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //this.mResult.add(this.mLAAgentSchema);
        }
        mInputData = null;
        return true;
    }


//pengcheng 2004-08-31  添加入司时间与考核时间校验
    private boolean check()
    {
        String tAgentCode = this.mLATreeSchema.getAgentCode();
        String tAstartDate = this.mLATreeSchema.getAstartDate();
        String tnewAstartDate = AgentPubFun.formatDate(tAstartDate,
                "yyyy-MM-dd");

        PubCheckField checkField1 = new PubCheckField();
        VData cInputData = new VData();
        TransferData tTransferData = new TransferData();
        String tIndexDate = PubFun.calDate(tnewAstartDate, -1, "M", null); //将任命时间置前一个月为考核时间
        String tIndexCalNo = AgentPubFun.formatDate(tIndexDate, "yyyyMM");

        tTransferData.setNameAndValue("IndexCalNo", tIndexCalNo);
        tTransferData.setNameAndValue("AgentCode", tAgentCode);
        //通过 CKBYSET 方式校验
        LMCheckFieldDB tLMCheckFieldDB = new LMCheckFieldDB();
        LMCheckFieldSet tLMCheckFieldSet = new LMCheckFieldSet();
        String tSql = "select * from lmcheckfield where riskcode = '000000'"
                      + " and fieldname = 'LATreeEditBL' order by serialno asc";
        tLMCheckFieldSet = tLMCheckFieldDB.executeQuery(tSql);
        cInputData.add(tTransferData);
        cInputData.add(tLMCheckFieldSet);

        if (!checkField1.submitData(cInputData, "CKBYSET"))
        {
            System.out.println("Enter Error Field!");
            //此判断是用来区分是程序处理过程中报的错误，还是校验时报的错误

            if (checkField1.mErrors.needDealError()) //程序错误处理
            {

                this.mErrors.copyAllErrors(checkField1.mErrors);
                return false;
            }
            else
            {
                VData t = checkField1.getResultMess(); //校验错误处理
                buildError("dealData", t.get(0).toString());
                return false;
            }
        }
        return true;

    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String tAgentCode = "";
        String tAgentGroup = "";
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            String tOldAscription = "", tNewAscription = "";
            String tNewAgentGrade = "", tOldAgentGrade = "";
            String tNewIntroAgency = "", tOldIntroAgency = "";
            String tNewBranchCode = "", tOldBranchCode = "";
            String tEdorNo = "";
            tAgentCode = this.mLATreeSchema.getAgentCode().trim();
            LATreeDB tLATreeDB = new LATreeDB();
            tLATreeDB.setAgentCode(tAgentCode);
            if (!tLATreeDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LATreeEditBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询原行政信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //判断行政信息是否修改
            tNewAgentGrade = this.mLATreeSchema.getAgentGrade() == null ? ""
                             : this.mLATreeSchema.getAgentGrade().trim();
            tOldAgentGrade = tLATreeDB.getAgentGrade().trim();
            tNewIntroAgency = this.mLATreeSchema.getIntroAgency() == null ? ""
                              : this.mLATreeSchema.getIntroAgency().trim();
            tOldIntroAgency = tLATreeDB.getIntroAgency() == null ? ""
                              : tLATreeDB.getIntroAgency().trim();
            tNewAscription = this.mLATreeSchema.getAscriptSeries() == null ? ""
                             : this.mLATreeSchema.getAscriptSeries().trim();
            tOldAscription = tLATreeDB.getAscriptSeries() == null ? ""
                             : tLATreeDB.getAscriptSeries().trim();
            //修改：对待遇职级是否发生变化进行判断
            //修改时间、地点： 2004-04-01 LL
            //String tNewBranchCode  = "",tOldBranchCode = "";
            tNewBranchCode = this.mLATreeSchema.getBranchCode() == null ? ""
                             : this.mLATreeSchema.getBranchCode().trim();
            tOldBranchCode = tLATreeDB.getBranchCode() == null ? ""
                             : tLATreeDB.getBranchCode().trim();

            if (tNewAgentGrade.equals(tOldAgentGrade)
                && tNewIntroAgency.equals(tOldIntroAgency)
                && tNewAscription.equals(tOldAscription)
                && tNewBranchCode.equals(tOldBranchCode))
            {
                CError tError = new CError();
                tError.moduleName = "LATreeEditBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "行政信息没有修改！";
                this.mErrors.addOneError(tError);
                return false;
            }
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tAgentCode);
            if (!tLAAgentDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LATreeEditBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "查询原代理人信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            //备份原代理人信息
            Reflections tReflections = new Reflections();
            tReflections.transFields(this.mLAAgentBSchema, tLAAgentDB.getSchema());
            this.mLAAgentBSchema.setEdorNo(tEdorNo);
            this.mLAAgentBSchema.setEdorType("06");
            this.mLAAgentBSchema.setMakeDate(currentDate);
            this.mLAAgentBSchema.setMakeTime(currentTime);
            this.mLAAgentBSchema.setModifyDate(currentDate);
            this.mLAAgentBSchema.setModifyTime(currentTime);
            this.mLAAgentBSchema.setOperator(this.mGlobalInput.Operator);

            this.mLAAgentSchema.setSchema(tLAAgentDB.getSchema());
            //置转正日期
            System.out.println("indueformdate:" + tLAAgentDB.getInDueFormDate());
            if (tLAAgentDB.getInDueFormDate() == null ||
                tLAAgentDB.getInDueFormDate().equals(""))
            {
                //this.mLAAgentSchema.setInDueFormDate(tLAAgentDB.getEmployDate());
                this.mLAAgentSchema.setModifyDate(currentDate);
                this.mLAAgentSchema.setModifyTime(currentTime);
                this.mLAAgentSchema.setOperator(this.mGlobalInput.Operator);
            }
            //备份行政信息
            tReflections.transFields(this.mLATreeBSchema, tLATreeDB.getSchema());
            this.mLATreeBSchema.setAstartDate(tLATreeDB.getAstartDate());
            this.mLATreeBSchema.setEdorNO(tEdorNo);
            this.mLATreeBSchema.setRemoveType("06");
            this.mLATreeBSchema.setMakeDate2(this.mLATreeBSchema.getMakeDate());
            this.mLATreeBSchema.setMakeTime2(this.mLATreeBSchema.getMakeTime());
            this.mLATreeBSchema.setModifyDate2(this.mLATreeBSchema.
                                               getModifyDate());
            this.mLATreeBSchema.setModifyTime2(this.mLATreeBSchema.
                                               getModifyTime());
            this.mLATreeBSchema.setOperator2(this.mLATreeBSchema.getOperator());
            this.mLATreeBSchema.setMakeDate(currentDate);
            this.mLATreeBSchema.setMakeTime(currentTime);
            this.mLATreeBSchema.setModifyDate(currentDate);
            this.mLATreeBSchema.setModifyTime(currentTime);
            this.mLATreeBSchema.setOperator(this.mGlobalInput.Operator);

            //行政信息
            LATreeSchema tLATreeSchema = new LATreeSchema();
            tLATreeSchema.setSchema(tLATreeDB.getSchema());
            tLATreeSchema.setBranchCode(this.mLATreeSchema.getBranchCode());
            if (!tNewAgentGrade.equals(tOldAgentGrade))
            {
                tLATreeSchema.setStartDate(this.mLATreeSchema.getAstartDate()); //调整日期
                tLATreeSchema.setAgentGrade(this.mLATreeSchema.getAgentGrade());
                tLATreeSchema.setAgentSeries(this.mLATreeSchema.getAgentSeries());
                tLATreeSchema.setAgentLastGrade(tLATreeDB.getAgentGrade());
                tLATreeSchema.setAgentLastSeries(tLATreeDB.getAgentSeries());
                tLATreeSchema.setOldStartDate(tLATreeDB.getStartDate());
                tLATreeSchema.setOldEndDate(PubFun.calDate(mLATreeSchema.
                        getAstartDate(), -1, "D", null));
                //确定代理人系列
                String tAgentSeries = getAgentSeries(tNewAgentGrade);
                if (tAgentSeries != null)
                {
                    tLATreeSchema.setAgentSeries(tAgentSeries);
                }
                else
                {
                    return false;
                }
                //tjj add 040401 至转正日期。
                if (mLATreeSchema.getAgentGrade().compareTo("A01") > 0)
                {
                    System.out.println("agentgradenew:" +
                                       mLATreeSchema.getAgentGrade());
                    if (tLAAgentDB.getInDueFormDate() == null ||
                        tLAAgentDB.getInDueFormDate().trim().equals(""))
                    {
                        this.mLAAgentSchema.setInDueFormDate(tLATreeSchema.
                                getStartDate());
                    }
                }
            }
            tLATreeSchema.setAstartDate(this.mLATreeSchema.getAstartDate());
            tLATreeSchema.setIntroAgency(this.mLATreeSchema.getIntroAgency());
            tLATreeSchema.setAscriptSeries(this.mLATreeSchema.getAscriptSeries());
            tLATreeSchema.setModifyDate(currentDate);
            tLATreeSchema.setModifyTime(currentTime);
            tLATreeSchema.setOperator(this.mGlobalInput.Operator);
//      if ((this.mLATreeSchema.getIntroAgency()!=null)&&(!this.mLATreeSchema.getIntroAgency().equals("")))
//      {
//        this.mLATreeSchema.setIntroBreakFlag("0");
//        this.mLATreeSchema.setIntroCommStart(this.mLAAgentSchema.getEmployDate());
//      }
//      this.mLATreeSchema.setStartDate(this.mLAAgentSchema.getEmployDate());

            //行政附属信息（先删后插方式）
            if (tNewAgentGrade.compareTo("A04") >= 0)
            {
                tAgentGroup = tLATreeSchema.getAgentGroup();
                LATreeAccessorySchema tLATreeAccessorySchema = new
                        LATreeAccessorySchema();
                tLATreeAccessorySchema = queryOriginTreeAccessory(tAgentCode,
                        tLATreeSchema.getAgentGrade());
                if (tLATreeAccessorySchema == null)
                {
                    return false;
                }
                if (tLATreeAccessorySchema.getMakeDate() == null) //该职级的原行政附属信息不存在
                {
                    System.out.println("职级" + tLATreeSchema.getAgentGrade() +
                                       "无附属信息");
                    tLATreeAccessorySchema.setAgentCode(tAgentCode);
                    tLATreeAccessorySchema.setAgentGrade(tLATreeSchema.
                            getAgentGrade());
                    tLATreeAccessorySchema.setAgentGroup(tAgentGroup);
                    /*lis5.3 update
                     tLATreeAccessorySchema.setManageCom(tLATreeSchema.getManageCom());
                     */
                    if ((this.mLATreeSchema.getEduManager() != null) &&
                        (!this.mLATreeSchema.getEduManager().equals("")))
                    {
                        tLATreeAccessorySchema.setRearAgentCode(this.
                                mLATreeSchema.getEduManager());
                    }
                    tLATreeAccessorySchema.setCommBreakFlag("0");
                    tLATreeAccessorySchema.setIntroBreakFlag("0");
                    tLATreeAccessorySchema.setstartdate(this.mLATreeSchema.
                            getAstartDate());
                    tLATreeAccessorySchema.setBackCalFlag("0"); //不回算
                    tLATreeAccessorySchema.setOperator(mGlobalInput.Operator);
                    tLATreeAccessorySchema.setMakeDate(currentDate);
                    tLATreeAccessorySchema.setMakeTime(currentTime);
                    tLATreeAccessorySchema.setModifyDate(currentDate);
                    tLATreeAccessorySchema.setModifyTime(currentTime);
                }
                else
                {
                    tLATreeAccessorySchema.setAgentGrade(tLATreeSchema.
                            getAgentGrade());
                    if ((this.mLATreeSchema.getEduManager() != null) &&
                        (!this.mLATreeSchema.getEduManager().equals("")))
                    {
                        tLATreeAccessorySchema.setRearAgentCode(this.
                                mLATreeSchema.getEduManager());
                    }
                    else
                    {
                        tLATreeAccessorySchema.setRearAgentCode(null);
                    }
                    tLATreeAccessorySchema.setCommBreakFlag("0");
                    tLATreeAccessorySchema.setIntroBreakFlag("0");
                    tLATreeAccessorySchema.setstartdate(this.mLATreeSchema.
                            getAstartDate()); //调整日期
                    tLATreeAccessorySchema.setOperator(mGlobalInput.Operator);
                    tLATreeAccessorySchema.setModifyDate(currentDate);
                    tLATreeAccessorySchema.setModifyTime(currentTime);
                }
                if (!prepareTreeAccessory(tNewAscription,
                                          tLATreeAccessorySchema,
                                          tNewAgentGrade))
                {
                    return false;
                }
                this.mLATreeAccessorySet.add(tLATreeAccessorySchema);
            }
            this.mLATreeSchema.setSchema(tLATreeSchema);
        }
        return tReturn;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mLATreeSchema.setSchema((LATreeSchema) cInputData.
                                     getObjectByObjectName("LATreeSchema", 0));
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLAAgentBSchema);
            this.mInputData.add(this.mLATreeSchema);
            this.mInputData.add(this.mLATreeBSchema);
            this.mInputData.add(this.mLATreeAccessorySet);
            this.mInputData.add(this.mLAAgentSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LATreeEditBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }


    /**
     * 根据代理人职级确定代理人系列
     */
    private String getAgentSeries(String cAgentGrade)
    {
        String tAgentSeries = "";
        String tSQL =
                "select code2 from ldcodeRela where relaType = 'gradeserieslevel' "
                + "and code1 = '" + cAgentGrade + "'";
        ExeSQL tExeSQL = new ExeSQL();
        tAgentSeries = tExeSQL.getOneValue(tSQL).trim();
        if (tExeSQL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "dealData";
            tError.errorMessage = "执行SQL语句：从表中取值失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tAgentSeries;
    }


    //育成人的个数决定插入行政附属表中的记录数
    private boolean prepareTreeAccessory(String cAscriptSeries,
                                         LATreeAccessorySchema cTASchema,
                                         String cAgentGrade)
    {
        String tAscriptSeries = cAscriptSeries;
        if (tAscriptSeries.indexOf(":") == -1)
        {
            tAscriptSeries = "";
        }
        else
        {
            tAscriptSeries = tAscriptSeries.substring(0,
                    tAscriptSeries.lastIndexOf(":"));
            LATreeAccessorySchema tLATreeAccessorySchema2 = null;
            Reflections tReflections = new Reflections();
            String tAscriptCode = "";
            String tAgentCode = cTASchema.getAgentCode();
            String[] tGrade =
                    {
                    "A04", "A06", "A08"};
            int count = 1;
            do
            {
                //截取育成人
                tAscriptCode = tAscriptSeries.indexOf(":") == -1 ?
                               tAscriptSeries
                               :
                               tAscriptSeries.substring(0,
                        tAscriptSeries.indexOf(":"));
                System.out.println("----AscriptCode:" + tAscriptCode);
                tAscriptCode = tAscriptCode.trim();
                if (!tAscriptCode.trim().equals(""))
                {
                    tLATreeAccessorySchema2 = new LATreeAccessorySchema();
                    tLATreeAccessorySchema2 = queryOriginTreeAccessory(
                            tAgentCode, tGrade[count - 1]);
                    if (tLATreeAccessorySchema2 == null)
                    {
                        return false;
                    }
                    if (tLATreeAccessorySchema2.getMakeDate() == null) //原来不存在该职级的附属信息
                    {
                        System.out.println("new");
                        tReflections.transFields(tLATreeAccessorySchema2,
                                                 cTASchema);
                        String sql = "";

                        //职级
                        switch (count)
                        {
                            case 1:
                                tLATreeAccessorySchema2.setAgentGrade(tGrade[
                                        count - 1]); //业务经理一级
                                sql =
                                        "select agentgroup from labranchgroup where branchmanager='" +
                                        tLATreeAccessorySchema2.getAgentCode() +
                                        "' and branchlevel='01'";
                                break;
                            case 2:
                                tLATreeAccessorySchema2.setAgentGrade(tGrade[
                                        count - 1]); //高级业务经理一级
                                sql =
                                        "select agentgroup from labranchgroup where branchmanager='" +
                                        tLATreeAccessorySchema2.getAgentCode() +
                                        "' and branchlevel='02'";
                                break;
                            case 3:
                                tLATreeAccessorySchema2.setAgentGrade(tGrade[
                                        count - 1]); //督导长
                                sql =
                                        "select agentgroup from labranchgroup where branchmanager='" +
                                        tLATreeAccessorySchema2.getAgentCode() +
                                        "' and branchlevel='03'";
                                break;
                        }
                        ExeSQL tExeSQL = new ExeSQL();
                        String tAgentGroup = tExeSQL.getOneValue(sql);
                        tLATreeAccessorySchema2.setAgentGroup(tAgentGroup);
                        tLATreeAccessorySchema2.setCommBreakFlag("0");
                        //回算标志
                        tLATreeAccessorySchema2.setBackCalFlag("0");
                    }
                    else
                    {
                        System.out.println("old");
                        tLATreeAccessorySchema2.setOperator(cTASchema.
                                getOperator());
                        tLATreeAccessorySchema2.setModifyDate(cTASchema.
                                getModifyDate());
                        tLATreeAccessorySchema2.setModifyTime(cTASchema.
                                getModifyTime());
                    }
                }
                tAscriptSeries = tAscriptSeries.indexOf(":") != -1
                                 ?
                                 tAscriptSeries.substring(tAscriptSeries.
                        indexOf(":") + 1) : "";
                System.out.println("育成链：" + tAscriptSeries);
                if (!tAscriptCode.equals(""))
                {
                    System.out.println("add");
                    tLATreeAccessorySchema2.setRearAgentCode(tAscriptCode);
                    this.mLATreeAccessorySet.add(tLATreeAccessorySchema2);
                }
                count++;
                System.out.println("count:" + count);
            }
            while (!tAscriptSeries.equals(""));
        }
        return true;
    }


    /**
     * 根据代理人代码和职级查询原行政附属信息
     * @return LATreeAccessorySchema
     */
    private LATreeAccessorySchema queryOriginTreeAccessory(String cAgentCode,
            String cAgentGrade)
    {
        String tSql = "Select * From LATreeAccessory Where AgentCode = '" +
                      cAgentCode + "' ";
        if (cAgentGrade.equals("A04") || cAgentGrade.equals("A05"))
        {
            tSql += "And AgentGrade in ('A04','A05')";
        }
        else if (cAgentGrade.equals("A06") || cAgentGrade.equals("A07"))
        {
            tSql += "And AgentGrade in ('A06','A07')";
        }
        else
        {
            tSql += "And AgentGrade = '" + cAgentGrade + "'";
        }
        System.out.println("sql:" + tSql);
        LATreeAccessoryDB tLATreeAccessory = new LATreeAccessoryDB();
        LATreeAccessorySet tLATreeAccessorySet = new LATreeAccessorySet();
        tLATreeAccessorySet = tLATreeAccessory.executeQuery(tSql);
        if (tLATreeAccessory.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLATreeAccessory.mErrors);
            CError tError = new CError();
            tError.moduleName = "LATreeEditBL";
            tError.functionName = "queryOriginTreeAccessory";
            tError.errorMessage = "查询代理人" + cAgentCode + "职级为" + cAgentGrade +
                                  "的行政附属信息出错！";
            this.mErrors.addOneError(tError);
            return null;
        }
        System.out.println("LATreeAccessorySet size:" +
                           tLATreeAccessorySet.size());
        if (tLATreeAccessorySet.size() <= 0)
        {
            return new LATreeAccessorySchema();
        }
        else
        {
            return tLATreeAccessorySet.get(1);
        }
    }
}
