/*
 * <p>ClassName: LATreeEditBLS </p>
 * <p>Description: LATreeEditBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-09
 */
package com.sinosoft.lis.agent;

import java.sql.Connection;

import com.sinosoft.lis.db.LAAgentBDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LATreeAccessoryDB;
import com.sinosoft.lis.db.LATreeBDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vdb.LATreeAccessoryDBSet;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

public class LATreeEditBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    public LATreeEditBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     *传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        System.out.println("Start LATreeEditBLS Submit...");
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            tReturn = saveLAAgent(cInputData);
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            tReturn = deleteLAAgent(cInputData);
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            tReturn = updateLAAgent(cInputData);
        }
        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End LATreeEditBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean saveLAAgent(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LATreeEditBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LATreeEditBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean deleteLAAgent(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Delete...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LATreeEditBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LATreeEditBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean updateLAAgent(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "LATreeEditBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 代理人备份信息保存...");
            LAAgentBDB tLAAgentBDB = new LAAgentBDB(conn);
            tLAAgentBDB.setSchema((LAAgentBSchema) mInputData.
                                  getObjectByObjectName("LAAgentBSchema", 0));
            if (!tLAAgentBDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentBDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LATreeEditBLS";
                tError.functionName = "updateData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            System.out.println("Start 行政信息保存...");
            LATreeDB tLATreeDB = new LATreeDB(conn);
            tLATreeDB.setSchema((LATreeSchema) mInputData.getObjectByObjectName(
                    "LATreeSchema", 0));
            String tAgentCode = tLATreeDB.getAgentCode().trim();
            if (!tLATreeDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LATreeEditBLS";
                tError.functionName = "updateData";
                tError.errorMessage = "LATree数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LATreeBDB tLATreeBDB = new LATreeBDB(conn);
            tLATreeBDB.setSchema((LATreeBSchema) mInputData.
                                 getObjectByObjectName("LATreeBSchema", 0));
            if (!tLATreeBDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeBDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LATreeEditBLS";
                tError.functionName = "updateData";
                tError.errorMessage = "LATreeB数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LATreeAccessoryDB tLATreeAccessoryDB = new LATreeAccessoryDB(conn); ;
            tLATreeAccessoryDB.setAgentCode(tAgentCode);
            if (!tLATreeAccessoryDB.deleteSQL())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeAccessoryDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LATreeEditBLS";
                tError.functionName = "updateData";
                tError.errorMessage = "行政附属信息删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LATreeAccessorySet tLATreeAccessorySet = new LATreeAccessorySet();
            tLATreeAccessorySet.set((LATreeAccessorySet) mInputData.
                                    getObjectByObjectName("LATreeAccessorySet",
                    0));
            if (tLATreeAccessorySet.size() > 0)
            {
                LATreeAccessoryDBSet tLATreeAccessoryDBSet = new
                        LATreeAccessoryDBSet(conn);
                tLATreeAccessoryDBSet.set(tLATreeAccessorySet);
                if (!tLATreeAccessoryDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLATreeAccessoryDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LATreeEditBLS";
                    tError.functionName = "updateData";
                    tError.errorMessage = "行政附属信息保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            LAAgentSchema tLAAgentSchema = new LAAgentSchema();
            tLAAgentSchema.setSchema((LAAgentSchema) mInputData.
                                     getObjectByObjectName("LAAgentSchema", 0));
            if (tLAAgentSchema.getAgentCode() != null &&
                !tLAAgentSchema.getAgentCode().trim().equals(""))
            {
                LAAgentDB tLAAgentDB = new LAAgentDB(conn);
                tLAAgentDB.setSchema(tLAAgentSchema);
                if (!tLAAgentDB.update())
                {
                    this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LATreeEditBLS";
                    tError.functionName = "updateData";
                    tError.errorMessage = "更新代理人信息表出错！";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LATreeEditBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch
                    (Exception e)
            {}
        }
        return tReturn;
    }
}
