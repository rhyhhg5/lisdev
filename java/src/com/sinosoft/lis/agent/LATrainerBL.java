package com.sinosoft.lis.agent;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

import utils.system;

import com.sinosoft.lis.db.LATrainerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LATrainerSchema;
import com.sinosoft.lis.vschema.LARateCommisionSet;
import com.sinosoft.lis.vschema.LATrainerSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class LATrainerBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData  mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LATrainerSet mLATrainerSet = new LATrainerSet();
    private MMap map = new MMap();
    private String tTrainerNo = "";
    private String tTrainerCode="";

    public LATrainerBL()
    {
    }

    public static void main(String[] args)
    {
    	String testNo = PubFun1.CreateMaxNo("TRAINERNO", 10);
    	System.out.println("生成的最大号是"+testNo);
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LATrainerBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LATrainerBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("over dealData");
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        System.out.println("Start LATrainerBL Submit...");
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LATrainerBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("End LATrainCourseBL Submit...");

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        	tTrainerCode=this.mLATrainerSet.get(1).getTrainerCode();;
	        if(tTrainerCode.equals("")||tTrainerCode==null){
	            String tPrefix = "";
	            String mManagecom=this.mLATrainerSet.get(1).getCManageCom();
	            if (mManagecom.length() < 4)
	
	            {
	                CError tError = new CError();
                    tError.moduleName = "LACreateCodePICCHBL";
                    tError.functionName = "dealdata";
                    tError.errorMessage = "管理机构长度不足！";
                    this.mErrors.addOneError(tError);
                    return false;
	
	
	            }
	            tPrefix = mManagecom.substring(2, 4);
	            tPrefix=tPrefix+"11";
	            String tTrainerCode1 = PubFun1.CreateMaxNo("TrainerCode" + tPrefix, 6);
	            if (tTrainerCode1 == null || tTrainerCode1.equals(""))
	            {
	                CError tError = new CError();
	                tError.moduleName = "LATrainerBL";
	                tError.functionName = "dealdata";
	                tError.errorMessage = "生成组训人员工号错误！";
	                this.mErrors.addOneError(tError);
	                return false;
	            }
	            tTrainerCode=tPrefix+tTrainerCode1;
	            System.out.println("组训人员工号：" + tTrainerCode);
        }

        if (this.mOperate.equals("INSERT||MAIN"))
        {
            LATrainerSet tLATrainerSetI = new LATrainerSet();
            for (int i = 1; i <= mLATrainerSet.size(); i++) {
            	LATrainerSchema mLATrainerSchema = new LATrainerSchema();
		        mLATrainerSchema=mLATrainerSet.get(i);
		    	tTrainerNo = PubFun1.CreateMaxNo("TRAINERNO", 10);
		    	System.out.println(tTrainerNo);
		    	String agentGroup = "";
		    	String tSql = "select  agentgroup from labranchgroup where branchattr = '"+mLATrainerSchema.getAgentGroup()+"' and "
		    	    + " branchtype = '1' and branchtype2 = '01'";
		    	ExeSQL tExeSQL = new ExeSQL();
		    	agentGroup=tExeSQL.getOneValue(tSql);
		    	mLATrainerSchema.setTrainerCode(tTrainerCode);
		        mLATrainerSchema.setTrainerNo(tTrainerNo);
		        mLATrainerSchema.setAgentGroup(agentGroup);
		        mLATrainerSchema.setOperator(this.mGlobalInput.Operator);
		        mLATrainerSchema.setMakeDate(currentDate);
		        mLATrainerSchema.setMakeTime(currentTime);
		        mLATrainerSchema.setModifyDate(currentDate);
		        mLATrainerSchema.setModifyTime(currentTime);
		        tLATrainerSetI.add(mLATrainerSchema);
            }
            map.put(tLATrainerSetI, "INSERT");
            
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            LATrainerSet tLATrainerSetU = new LATrainerSet();
	        for (int i = 1; i <= mLATrainerSet.size(); i++) {
	        	LATrainerSchema mLATrainerSchema = new LATrainerSchema();
		        mLATrainerSchema=mLATrainerSet.get(i);
	            tTrainerNo = mLATrainerSchema.getTrainerNo();
	            LATrainerDB tLATrainerDB = new LATrainerDB();
	            tLATrainerDB.setTrainerNo(tTrainerNo);
	            if (!tLATrainerDB.getInfo())
	            {
	                 // @@错误处理
	                this.mErrors.copyAllErrors(tLATrainerDB.mErrors);
	                CError tError = new CError();
	                tError.moduleName = "LATrainerBL";
	                tError.functionName = "submitData";
	                tError.errorMessage = "没有查询到，相关人员信息，数据提交失败!";
	                this.mErrors.addOneError(tError);
	                return false;
	            }
	        	String agentGroup = "";
	        	String tSql = "select  agentgroup from labranchgroup where  (branchattr = '"+mLATrainerSchema.getAgentGroup()+"' or agentgroup='"+mLATrainerSchema.getAgentGroup()+"') and "
	        	    + " branchtype = '1' and branchtype2 = '01'";
	        	ExeSQL tExeSQL = new ExeSQL();
	        	agentGroup=tExeSQL.getOneValue(tSql);
	            mLATrainerSchema.setAgentGroup(agentGroup);
	            mLATrainerSchema.setMakeDate(tLATrainerDB.getMakeDate());
	            mLATrainerSchema.setMakeTime(tLATrainerDB.getMakeTime());
	            mLATrainerSchema.setModifyTime(currentTime);
	            mLATrainerSchema.setModifyDate(currentDate);
	            mLATrainerSchema.setOperator(mGlobalInput.Operator);
	            tLATrainerSetU.add(mLATrainerSchema);
	        }
                map.put(tLATrainerSetU, "UPDATE");
        }
        
        
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        try {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mLATrainerSet.set( (LATrainerSet) cInputData.getObjectByObjectName("LATrainerSet",0));

        }catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LATrainerBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "在读取处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
		}
        return true;

    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */

    private boolean prepareOutputData()
    {
        try
        {
            System.out.println("Begin LATrainerBL.prepareOutputData.........");
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LATrainerBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

}
