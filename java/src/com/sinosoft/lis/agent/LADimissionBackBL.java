/*
 * <p>ClassName: LADimissionBackBL </p>
 * <p>Description: LADimissionBackBL类文件 </p>
 * <p>Copyright: Copyright (c) 2005.3</p>
 * <p>Company: sinosoft </p>
 * @author
 * @Database: 销售管理
 * @CreateDate：
 */
package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LADimissionDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.lis.vschema.LADimissionSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.ExeSQL;

public class LADimissionBackBL
{
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LADimissionSchema mLADimissionSchema = new LADimissionSchema();
    private LADimissionSet mLADimissionSet = new LADimissionSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private MMap map = new MMap();
    private String mAgentCode="";
    public LADimissionBackBL()
    {
    }

    public static void main(String[] args)
    {
    }


        /**
          传输数据的公共方法
         */
        public boolean submitData(VData cInputData, String cOperate) {
          System.out.println("Begin LADimissionBackBL.submitData.........");
          //将操作数据拷贝到本类中
          this.mOperate = cOperate;
          //得到外部传入的数据,将数据备份到本类中
          if (!getInputData(cInputData)) {
            return false;
          }
          //进行业务处理
          if (!dealData()) {
            return false;
          }
          //准备往后台的数据
          if (!prepareOutputData()) {
            return false;
          }
          PubSubmit tPubSubmit = new PubSubmit();
          System.out.println("Start LADimissionBackBL Submit...");
          if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LADimissionBackBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
          }
          return true;
        }



      /**
         * 从输入数据中得到所有对象
         *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
         */
        private boolean getInputData(VData cInputData) {
          //全局变量
          try {
            System.out.println("Begin LADimissionBackBL.getInputData.........");
           this.mLADimissionSchema.setSchema((LADimissionSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LADimissionSchema",
                                                  0));
           this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
          }
          catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LADimissionBackBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "在读取处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
          }
          System.out.println("getInputData end ");
          return true;
        }



        /**
         * 业务处理主函数
         */
        private boolean dealData() {
          System.out.println("Begin LADimissionBackBL.dealData........."+mOperate);
          try {
            if (mOperate.equals("UPDATE||MAIN") )
            {
               mAgentCode=mLADimissionSchema.getAgentCode();
               ExeSQL tExe = new ExeSQL();
               String tSql =
                 "select agentstate from laagent where agentcode='"+mAgentCode+"'";
               String mAgentstate = "";
              mAgentstate = tExe.getOneValue(tSql);
              if ( !mAgentstate.equals("03")&& !mAgentstate.equals("04"))
                  {
               //原销售人员没有离职
               CError tError = new CError();
               tError.moduleName = "LADimissionBackBL";
               tError.functionName = "dealData";
               tError.errorMessage = "回退人员不是离职登记业务员！";
               this.mErrors.addOneError(tError);
               return false;
                   }

              else
                  {
                     //更新
                  String SQL = "update LAAgent  set agentstate='01' ";
                  SQL+=",operator='"+mGlobalInput.Operator+"'";
                  SQL+=",ModifyDate='"+CurrentDate+"'";
                  SQL+=",ModifyTime='"+CurrentTime+"'";
                  SQL+="  where agentcode='"+mLADimissionSchema.getAgentCode()+"'";
                  map.put(SQL ,"UPDATE");
                  map.put(mLADimissionSchema, "DELETE");
            }
           }
          }

          catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LADimissionBackBL";
            tError.functionName = "dealData";
            tError.errorMessage = "在处理所数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
          }
          return true;
        }

        /**
         * 准备往后层输出所需要的数据
         * 输出：如果准备数据时发生错误则返回false,否则返回true
         */
        private boolean prepareOutputData() {
          try {
            System.out.println(
                "Begin LADimissionBackBL.prepareOutputData.........");
            mInputData.clear();
            mInputData.add(map);
          }
          catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LADimissionBackBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
          }
          return true;
        }

    public VData getResult()
      {
          return this.mResult;
      }



}
