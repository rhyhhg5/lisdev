/**
 * <p>ClassName: ALADimissionAppUI </p>
 * <p>Description: ALADimissionAppUI类文件 </p>
 * <p>Copyright: Copyright (c) 2005.3</p>
 * @Database: 销售管理
 * <p>Company: sinosoft</p>
 * @author zhanghui
 */
package com.sinosoft.lis.agent;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.LADimissionSet;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
public class ALADimissionAppUI {
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
         public CErrors mErrors = new CErrors();
         private VData mResult = new VData();
         /** 往后面传输数据的容器 */
         private VData mInputData = new VData();
         /** 数据操作字符串 */
         private String mOperate;
//业务处理相关变量
         /** 全局数据 */
         private GlobalInput mGlobalInput = new GlobalInput();
         private LADimissionSchema mLADimissionSchema = new LADimissionSchema();
         private LADimissionSet mLADimissionSet = new LADimissionSet();
         public ALADimissionAppUI()
         {
         }

         /**
          传输数据的公共方法
          */
         public boolean submitData(VData cInputData, String cOperate)
         {
             //将操作数据拷贝到本类中
             this.mOperate = cOperate;
             System.out.println("UI in...");
             //得到外部传入的数据,将数据备份到本类中
             if (!getInputData(cInputData))
             {
                 return false;
             }
             //进行业务处理
             if (!dealData())
             {
                 return false;
             }
             //准备往后台的数据
             if (!prepareOutputData())
             {
                 return false;
             }
             System.out.println("up...");
             ALADimissionAppBL tALADimissionAppBL = new ALADimissionAppBL();
             System.out.println("Start LADimissionApp UI Submit...");
             tALADimissionAppBL.submitData(mInputData, mOperate);
             System.out.println("End LADimissionApp UI Submit...");
             //如果有需要处理的错误，则返回
             if (tALADimissionAppBL.mErrors.needDealError())
             {
                 // @@错误处理
                 this.mErrors.copyAllErrors(tALADimissionAppBL.mErrors);
                 CError tError = new CError();
                 tError.moduleName = "ALADimissionAppUI";
                 tError.functionName = "submitData";
                 tError.errorMessage = "数据提交失败!";
                 this.mErrors.addOneError(tError);
                 return false;
             }
             if (mOperate.equals("QUERY||MAIN"))
             {
                 this.mResult.clear();
                 this.mResult = tALADimissionAppBL.getResult();
             }
             mInputData = null;
             return true;
         }

         public static void main(String[] args) {
             LADimissionSchema tLADimissionSchema = new LADimissionSchema();
             ALADimissionUI tLADimission = new ALADimissionUI();
             tLADimissionSchema.setAgentCode("8611000510");
             tLADimissionSchema.setDepartDate("2003-6-11");
             tLADimissionSchema.setDepartRsn("DepartRsn");
             tLADimissionSchema.setNoti("Noti");
             tLADimissionSchema.setOperator("001");
             GlobalInput tGlobalInput = new GlobalInput();
             tGlobalInput.Operator = "001";
             tGlobalInput.ComCode = "8611";
             VData tVData = new VData();

             tVData.addElement(tLADimissionSchema);
             tVData.add(tGlobalInput);
             if (tLADimission.submitData(tVData, "operator")) {
                 VData rVData = tLADimission.getResult();
                 System.out.println("Submit Failed! " + (String) rVData.get(0));
             } else System.out.println("Submit Succed!");

         }

         /**
          * 准备往后层输出所需要的数据
          * 输出：如果准备数据时发生错误则返回false,否则返回true
          */
         private boolean prepareOutputData()
         {
             try
             {
                 mInputData.clear();
                 mInputData.add(this.mGlobalInput);
                 mInputData.add(this.mLADimissionSchema);
             }
             catch (Exception ex)
             {
                 // @@错误处理
                 CError tError = new CError();
                 tError.moduleName = "ALADimissionAppUI";
                 tError.functionName = "prepareData";
                 tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
                 this.mErrors.addOneError(tError);
                 return false;
             }
             return true;
         }

         /**
          * 根据前面的输入数据，进行UI逻辑处理
          * 如果在处理过程中出错，则返回false,否则返回true
          */
         private boolean dealData()
         {
             boolean tReturn = false;
             //此处增加一些校验代码
             tReturn = true;
             return tReturn;
         }

         /**
          * 从输入数据中得到所有对象
          *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
          */
         private boolean getInputData(VData cInputData)
         {
             //全局变量
             mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                     "GlobalInput", 0));
             this.mLADimissionSchema.setSchema((LADimissionSchema) cInputData.
                                               getObjectByObjectName(
                                                       "LADimissionSchema", 0));
             if (mGlobalInput == null)
             {
                 // @@错误处理
                 CError tError = new CError();
                 tError.moduleName = "ALADimissionAppUI";
                 tError.functionName = "getInputData";
                 tError.errorMessage = "没有得到足够的信息！";
                 this.mErrors.addOneError(tError);
                 return false;
             }
             return true;
         }

         public VData getResult()
         {
             return this.mResult;
         }
     }
