/*
 * <p>ClassName: ALADimissionReBLS </p>
 * <p>Description: ALADimissionReBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2004-12-07
 */
package com.sinosoft.lis.agent;

import java.sql.Connection;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LADimissionDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vdb.LATreeAccessoryDBSet;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

public class ALADimissionReBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    private GlobalInput mGlobalInput = new GlobalInput();
    private LADimissionSchema mLADimissionSchema = new LADimissionSchema();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private LATreeAccessorySet mLATreeAccessorySet = new LATreeAccessorySet();


    public ALADimissionReBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        if (!getInputDate(cInputData))
        {
            return false;
        }
        System.out.println("Start ALADimissionReBLS Submit...");
        if (!updateLADimission(cInputData))
        {
            return false;
        }
        return true;
    }

    /**
     * 保存函数
     */
    private boolean updateLADimission(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "ALADimissionReBLS";
            tError.functionName = "deleteData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LADimissionDB tLADimissionDB = new LADimissionDB(conn);
            tLADimissionDB.setSchema((LADimissionSchema) mInputData.
                                     getObjectByObjectName("LADimissionSchema",
                    0));
            if (!tLADimissionDB.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLADimissionDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALADimissionReBLS";
                tError.functionName = "deleteData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            //修改代理人信息表中的代理人状态字段
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setSchema((LAAgentSchema) mInputData.
                                 getObjectByObjectName("LAAgentSchema", 0));
            if (!tLAAgentDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALADimissionReBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "修改代理人状态失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            /*LATreeDB tLATreeDB = new LATreeDB();
            tLATreeDB.setSchema((LATreeSchema) mInputData.getObjectByObjectName(
                    "LATreeSchema", 0));
            if (!tLATreeDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALADimissionReBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "修改代理人状态失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LATreeAccessoryDBSet tLATreeAccessoryDBSet = new
                    LATreeAccessoryDBSet();
            tLATreeAccessoryDBSet.add((LATreeAccessorySet) mInputData.
                                      getObjectByObjectName(
                    "LATreeAccessorySet", 0));
            if (!tLATreeAccessoryDBSet.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeAccessoryDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALADimissionReBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "修改代理人状态失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }*/
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALADimissionReBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    private boolean getInputDate(VData cInputData)
    {
        try
        {
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            mLADimissionSchema = (LADimissionSchema) cInputData.
                                 getObjectByObjectName("LADimissionSchema", 0);
            mLAAgentSchema = (LAAgentSchema) cInputData.getObjectByObjectName(
                    "LAAgentSchema", 0);
            mLATreeSchema = (LATreeSchema) cInputData.getObjectByObjectName(
                    "LATreeSchema", 0);
            mLATreeAccessorySet = (LATreeAccessorySet) cInputData.
                                  getObjectByObjectName("LATreeAccessorySet", 0);
            //this.mLADimissionSchema.setSchema((LADimissionSchema)cInputData.getObjectByObjectName("LADimissionSchema",0));
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
}
