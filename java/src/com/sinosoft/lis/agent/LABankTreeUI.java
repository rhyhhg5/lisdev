/*
 * <p>ClassName: ALABankAgentUI </p>
 * <p>Description: ALABankAgentUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-09
 */
package com.sinosoft.lis.agent;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LABankTreeUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private String tAgentCode;
    private String tAgentGroup;
    private String tNewAgentGrade1;
    private String tNewAgentGrade;
    private String tDate;
    private String tBranchtype;
    private String tBranchtype2;
//业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    public LABankTreeUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        LABankTreeBL tLABankTreeBL = new LABankTreeBL();
        System.out.println("Start LABankTreeUI Submit...");
        tLABankTreeBL.submitData(mInputData, mOperate);
        System.out.println("End LABankTreeUI Submit...");
        //如果有需要处理的错误，则返回
        if (tLABankTreeBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLABankTreeBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LABankTreeUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    public static void main(String[] args)
    {
        VData mInputData = new VData();
        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86";
        mGlobalInput.ComCode = "";
        mGlobalInput.Operator = "zy";
        mInputData.add("8633000001");
//         System.out.println("tagentcode2-----"+tAgentCode);
        mInputData.add("AA");
        mInputData.add("02");
        mInputData.add("B11");
        mInputData.add("2004-4-2");
        mInputData.add(mGlobalInput);
        LABankTreeBL tLABankTreeBL = new LABankTreeBL();
        tLABankTreeBL.submitData(mInputData, "");

    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            System.out.println("prepareOuptDAte");
            mInputData.clear();
            mInputData.add(tAgentCode);
            System.out.println("tagentcode2-----" + tAgentCode);
            mInputData.add(tAgentGroup);
            mInputData.add(tNewAgentGrade1);
            mInputData.add(tNewAgentGrade);
            mInputData.add(tDate);
            mInputData.add(this.mGlobalInput);
            mInputData.add(tBranchtype);
            mInputData.add(tBranchtype2);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABankTreeUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        tAgentCode = (String) cInputData.get(0);
        tAgentGroup = (String) cInputData.get(1);
        tNewAgentGrade1 = (String) cInputData.get(2);
        tNewAgentGrade = (String) cInputData.get(3);
        tDate = (String) cInputData.get(4);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        tBranchtype=(String)cInputData.get(6);
        tBranchtype2=(String)cInputData.get(7);
        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABankTreeUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
