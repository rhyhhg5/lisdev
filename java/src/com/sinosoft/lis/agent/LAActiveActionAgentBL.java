package com.sinosoft.lis.agent;


import com.cbsws.obj.RspUniSalescod;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAgentGradeDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LAQualificationDB;
import com.sinosoft.lis.db.LARearRelationDB;
import com.sinosoft.lis.db.LARecomRelationDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LAQualificationBSchema;
import com.sinosoft.lis.schema.LAQualificationSchema;
import com.sinosoft.lis.schema.LARearRelationBSchema;
import com.sinosoft.lis.schema.LARearRelationSchema;
import com.sinosoft.lis.schema.LARecomRelationBSchema;
import com.sinosoft.lis.schema.LARecomRelationSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.schema.LAWarrantorSchema;
import com.sinosoft.lis.sys.UnisalescodFun;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LAQualificationBSet;
import com.sinosoft.lis.vschema.LAQualificationSet;
import com.sinosoft.lis.vschema.LARearRelationBSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.vschema.LARecomRelationBSet;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LAWarrantorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: AgentSystem</p>
 * <p>Description: 销售管理——增员管理</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author xjh
 * @version 1.0
 */

public class LAActiveActionAgentBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();


	/** 数据操作字符串 */
	private String mOperate;
	/** 全局数据 */
	private MMap map = new MMap();
	private GlobalInput mGlobalInput = new GlobalInput();

	private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
	private LATreeSchema mLATreeSchema = new LATreeSchema();
	
	private LAWarrantorSet mLAWarrantorSet = new LAWarrantorSet();
	private LAQualificationSchema mLAQualificationSchema = new LAQualificationSchema();
	
	private LAQualificationSet mdelLAQualificationSet = new LAQualificationSet();
	private LAQualificationBSet mLAQualificationBSet = new LAQualificationBSet();
	

	private String mAgentCode = "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();

    private LATreeSet mLATreeSet = new LATreeSet();
    private LATreeBSet mLATreeBSet = new LATreeBSet();

    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();
    private LABranchGroupBSet mLABranchGroupBSet = new LABranchGroupBSet();
   
    private LAWarrantorSet mInLAWarrantorSet = new LAWarrantorSet();
    
    private String mEdorNo = "";
    private LARearInterface mLARearInterface;
    private LARecomInterface mLARecomInterface;
    private String mMakeContNum="";
    private Reflections ref = new Reflections();
    
    


	public String getAgentCode() {
		return mAgentCode;
	}

	public LAActiveActionAgentBL() {
	}

	public static void main(String[] args)
	{

	}

	/**
	 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		if(!checkData())
		{
			return false;
		}
		//进行业务处理
		if (!dealData()) {
			return false;
		}
		//处理育成关系
		if(!dealLAreal()){
			return false;
		}
		//处理推荐关系
		if(!dealLARecom()){
			return false;
		}
			
		//准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LAActiveActionAgentBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		
		UniCommon tUniCommon = new UniCommon();
        boolean tFlag = tUniCommon.getGroupAgentCode(mOperate, mAgentCode, mLAAgentSchema, mLAQualificationSchema.getQualifNo());
        if(!tFlag){
        	this.mErrors.copyAllErrors(tUniCommon.mErrors) ;
        	return false;
        }
		
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData)
	{
		//全局变量
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData
				.getObjectByObjectName("LAAgentSchema", 0));
		this.mLATreeSchema.setSchema((LATreeSchema) cInputData
				.getObjectByObjectName("LATreeSchema", 0));
		
		this.mLAWarrantorSet.set((LAWarrantorSet) cInputData
				.getObjectByObjectName("LAWarrantorSet", 0));
		this.mLAQualificationSchema
				.setSchema((LAQualificationSchema) cInputData
						.getObjectByObjectName("LAQualificationSchema", 0));

		if (mGlobalInput == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAActiveActionAgentBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的信息！";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public boolean checkData()
	{
		return true;
	}
	/**
	 * 业务处理主函数
	 * @return boolean
	 */
	public boolean dealData() {
		/**获取集团工号Unisalescod*/
//		ExeSQL lExeSQL = new ExeSQL();
//        String tSQL = "select GroupAgentCode from laagent where agentcode=getagentcode('"+mLAAgentSchema.getAgentCode()+"')";
//        String mGroupAgentCode = lExeSQL.getOneValue(tSQL);
//        UnisalescodFun fun=new UnisalescodFun();
//        RspUniSalescod mRspUniSalescod = new RspUniSalescod();
//        if(mOperate.equals("INSERT||MAIN")){
//        	mRspUniSalescod=fun.getUnisalescod(mOperate, mLAAgentSchema, null,null,mLAQualificationSchema.getQualifNo());
//        	mLAAgentSchema.setGroupAgentCode(mRspUniSalescod.getUNI_SALES_COD());
//        }else if(mOperate.equals("UPDATE||MAIN")){
//        	mRspUniSalescod=fun.getUnisalescod(mOperate, mLAAgentSchema, null,mGroupAgentCode,null );
//        	mLAAgentSchema.setGroupAgentCode(mGroupAgentCode);
//        }
//        if("01".equals(mRspUniSalescod.getMESSAGETYPE()))
//		{
//			 CError tError = new CError();
//	         tError.moduleName = "LAActiveActionAgentBL";
//	         tError.functionName = "dealData";
//	         tError.errorMessage = "获取集团工号时报文格式错误";
//	         this.mErrors.addOneError(tError);
//			return false;
//		}
////////////
		
		if(this.mLAQualificationSchema.getValidStart()!=null&&!this.mLAQualificationSchema.getValidStart().equals("")){//取消资格证必录校验add by wrx2015-7-9
			if(this.mLAQualificationSchema.getValidStart().compareTo(this.mLAQualificationSchema.getValidEnd())>0)
				{
					CError tError = new CError();
					tError.moduleName = "LAActiveActionAgentBL";
					tError.functionName = "submitData";
					tError.errorMessage = "资格证有效止期应大于资格者证有效起期!";
					this.mErrors.addOneError(tError);
					return false;
					
				}
		}
		mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
		if("INSERT||MAIN".equals(this.mOperate))
		{
	    	
            String tPrefix = "";
            tPrefix = this.mLAAgentSchema.getManageCom().substring(2, 4)+"11";
            this.mAgentCode = mLAAgentSchema.getManageCom().substring(2, 4)+"11"+PubFun1.CreateMaxNo("AgentCode" + tPrefix, 6);
			// LAAgent表信息处理
			// 入司时，AgentState 为01
            this.mLAAgentSchema.setAgentCode(this.mAgentCode);
			this.mLAAgentSchema.setAgentState("01");
			
			//操作信息
			this.mLAAgentSchema.setOperator(this.mGlobalInput.Operator);
			this.mLAAgentSchema.setMakeDate(this.currentDate);
			this.mLAAgentSchema.setMakeTime(this.currentTime);
			this.mLAAgentSchema.setModifyDate(this.currentDate);
			this.mLAAgentSchema.setModifyTime(this.currentTime);			
			
			// LATree表信息处理
			// 职级信息处理
			
			this.mLATreeSchema.setInitGrade(this.mLATreeSchema.getAgentGrade());
			String tAgentGrade = this.mLATreeSchema.getAgentGrade();
			LAAgentGradeDB tLAAgentGradeDB = new LAAgentGradeDB();
			tLAAgentGradeDB.setGradeCode(tAgentGrade);
			if(!tLAAgentGradeDB.getInfo())
			{
				CError tError = new CError();
				tError.moduleName = "LAActiveActionAgentBL";
				tError.functionName = "submitData";
				tError.errorMessage = "职级信息查询失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
			this.mLATreeSchema.setAgentCode(this.mAgentCode);
			this.mLATreeSchema.setAgentSeries(tLAAgentGradeDB.getGradeProperty2());
			this.mLATreeSchema.setStartDate(this.mLAAgentSchema.getEmployDate());
			this.mLATreeSchema.setAstartDate(this.mLAAgentSchema.getEmployDate());
			this.mLATreeSchema.setBranchCode(this.mLATreeSchema.getAgentGroup());
			this.mLATreeSchema.setInitGrade(this.mLATreeSchema.getAgentGrade());
			
            //操作信息
			this.mLATreeSchema.setOperator(this.mGlobalInput.Operator);
			this.mLATreeSchema.setMakeDate(this.currentDate);
			this.mLATreeSchema.setMakeTime(this.currentTime);
			this.mLATreeSchema.setModifyDate(this.currentDate);
			this.mLATreeSchema.setModifyTime(this.currentTime);
			

			
			if("0".equals(mLATreeSchema.getAgentSeries()))
			{
				//如果是业务职级序列，需查看是否有用户上级
				LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
				tLABranchGroupDB.setAgentGroup(this.mLAAgentSchema.getAgentGroup());
				if(!tLABranchGroupDB.getInfo())
				{
					CError tError = new CError();
					tError.moduleName = "LAActiveActionAgentBL";
					tError.functionName = "submitData";
					tError.errorMessage = "职级序列信息查询失败!";
					this.mErrors.addOneError(tError);
					return false;
				}
				mLATreeSchema.setUpAgent(tLABranchGroupDB.getBranchManager());				
			}else{
				// 如果入司是主管序列人员。
				//1、需将其入司团队下所有在职人上级置为新入司主管
				//2、将所属团队主管编码、主管名称字段置对应值
				LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
				tLABranchGroupDB.setAgentGroup(this.mLAAgentSchema.getAgentGroup());
				if(!tLABranchGroupDB.getInfo())
				{
					CError tError = new CError();
					tError.moduleName = "LAActiveActionAgentBL";
					tError.functionName = "submitData";
					tError.errorMessage = "团队信息查询报错!";
					this.mErrors.addOneError(tError);
					return false;
				}
				LABranchGroupSet  tLABranchGroupSet = tLABranchGroupDB.query();
				for(int i = 1;i<=tLABranchGroupSet.size();i++)
				{					
					LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
					LABranchGroupBSchema tLABranchGroupBSchema = new LABranchGroupBSchema();					
					tLABranchGroupSchema = tLABranchGroupSet.get(i);
					Reflections tReflections = new Reflections();
			        tReflections.transFields(tLABranchGroupBSchema, tLABranchGroupSchema);
			        
			        tLABranchGroupBSchema.setEdorType("05");
			        tLABranchGroupBSchema.setEdorNo(this.mEdorNo);
			        tLABranchGroupBSchema.setMakeDate(this.currentDate);
			        tLABranchGroupBSchema.setMakeTime(this.currentTime);
			        tLABranchGroupBSchema.setModifyDate(this.currentDate);
			        tLABranchGroupBSchema.setModifyTime(this.currentTime);
			        tLABranchGroupBSchema.setOperator(this.mGlobalInput.Operator);
			        
			        tLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.getOperator());
			        tLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.getMakeDate());
			        tLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.getMakeTime());
			        tLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.getModifyDate());
			        tLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.getModifyTime());
			        
			        tLABranchGroupSchema.setBranchManager(this.mAgentCode);
			        tLABranchGroupSchema.setBranchManagerName(this.mLAAgentSchema.getName());
			        tLABranchGroupSchema.setModifyDate(currentDate);
			        tLABranchGroupSchema.setModifyTime(currentTime);
			        tLABranchGroupSchema.setOperator(this.mGlobalInput.Operator);
			        
			        
			        this.mLABranchGroupSet.add(tLABranchGroupSchema);
			        this.mLABranchGroupBSet.add(tLABranchGroupBSchema);
					
				}
				// 处理人员
				String sql = "Select * From LAAgent where 1=1 and BranchType = '"+this.mLAAgentSchema.getBranchType()+"'"
				+" and BranchType2 = '"+this.mLAAgentSchema.getBranchType2()+"' " 
				+" and AgentGroup = '"+this.mLAAgentSchema.getAgentGroup()+"'"
				+" and AgentState <'06' "; 
				LAAgentDB tLAAgentDB = new LAAgentDB();
				LAAgentSet tLAAgentSet = new LAAgentSet();
				tLAAgentSet = tLAAgentDB.executeQuery(sql);
				if(tLAAgentSet.size()>0)
				{
					for(int i = 1;i<=tLAAgentSet.size();i++)
					{
						LAAgentSchema tLAAgentSchema = new LAAgentSchema();
						LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
						tLAAgentSchema = tLAAgentSet.get(i);
						
						Reflections tReflections = new Reflections();
				        tReflections.transFields(tLAAgentBSchema, tLAAgentSchema);

				        tLAAgentBSchema.setEdorNo(this.mEdorNo);
				        tLAAgentBSchema.setEdorType("05");
				        tLAAgentBSchema.setModifyDate(this.currentDate);
				        tLAAgentBSchema.setModifyTime(this.currentTime);
				        tLAAgentBSchema.setOperator(this.mGlobalInput.Operator);
				        				        
				        mLAAgentBSet.add(tLAAgentBSchema);
				        
				        tLAAgentSchema.setModifyDate(currentDate);
				        tLAAgentSchema.setModifyTime(currentTime);
				        tLAAgentSchema.setOperator(this.mGlobalInput.Operator);
				        
				        mLAAgentSet.add(tLAAgentSchema);
				        
				        LATreeDB tLATreeDB = new LATreeDB();
				        tLATreeDB.setAgentCode(tLAAgentSchema.getAgentCode());
				        if(!tLATreeDB.getInfo())
				        {
							CError tError = new CError();
							tError.moduleName = "LAActiveActionAgentBL";
							tError.functionName = "submitData";
							tError.errorMessage = "人员信息查询报错!";
							this.mErrors.addOneError(tError);
							return false;
				        }
				        
				        LATreeSchema tLATreeSchema = tLATreeDB.query().get(1);				        
				        LATreeBSchema tLATreeBSchema = new LATreeBSchema();
				        tReflections.transFields(tLATreeBSchema, tLATreeSchema);
				        
				        tLATreeBSchema.setMakeDate(currentDate);
				        tLATreeBSchema.setMakeTime(currentTime);
				        tLATreeBSchema.setModifyDate(currentDate);
				        tLATreeBSchema.setModifyTime(currentTime);
				        tLATreeBSchema.setOperator(mGlobalInput.Operator);
				        tLATreeBSchema.setRemoveType("05");
				        tLATreeBSchema.setEdorNO(this.mEdorNo);
				        
				        tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
				        tLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
				        tLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
				        tLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
				        tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
				        
				        mLATreeBSet.add(tLATreeBSchema);
				        
				        tLATreeSchema.setUpAgent(this.mAgentCode);
				        tLATreeSchema.setModifyDate(currentDate);
				        tLATreeSchema.setModifyTime(currentTime);
				        this.mLATreeSet.add(tLATreeSchema);																		
					}					
				}										
			}
			// 资格证信息处理
			// 资格证信息校验
			//////////////
			if(this.mLAQualificationSchema.getQualifNo()!=null&&!this.mLAQualificationSchema.getQualifNo().equals("")){
				String strSQL = "Select 1 From LAQualification where AgentCode <> '"+this.mAgentCode+"' " 
						+" and qualifno ='"+this.mLAQualificationSchema.getQualifNo()+"' " 
						+" and exists(Select 1 from laagent where agentcode = LAQualification.agentcode and branchtype= '"+this.mLAAgentSchema.getBranchType()+"' and branchtype2 = '"+this.mLAAgentSchema.getBranchType2()+"' " 
						+" and agentstate < '06') fetch first 1 rows only";
				System.out.println("打印sql:"+strSQL);
				ExeSQL tExeSQL = new ExeSQL();
				String tResult = tExeSQL.getOneValue(strSQL);
				if(tResult!=null&&"1".equals(tResult))
				{
					CError tError = new CError();
					tError.moduleName = "LAActiveActionAgentBL";
					tError.functionName = "submitData";
					tError.errorMessage = "录入资格证系统已存在,烦请重新进行录入!";
					this.mErrors.addOneError(tError);
					return false;
					
				}
				
	
				mLAQualificationSchema.setAgentCode(this.mAgentCode);
				mLAQualificationSchema.setreissueDate(this.currentDate);
				mLAQualificationSchema.setMakeDate(this.currentDate);
				mLAQualificationSchema.setMakeTime(this.currentTime);
				mLAQualificationSchema.setModifyDate(this.currentDate);
				mLAQualificationSchema.setModifyTime(this.currentTime);
				mLAQualificationSchema.setOperator(this.mGlobalInput.Operator);
				map.put(mLAQualificationSchema, "INSERT");
			}else{
				ExeSQL tExeSQL = new ExeSQL();
				String tSQL = "select qualifno from LAQualification where agentcode = '"+this.mLAAgentSchema.getAgentCode()+"'";
				SSRS tSSRS = tExeSQL.execSQL(tSQL);
				if(tSSRS.getMaxRow()>0){
					String aQualifno = tSSRS.GetText(1, 1);
					LAQualificationDB tLAQualificationDB = new LAQualificationDB();
					tLAQualificationDB.setAgentCode(this.mLAAgentSchema.getAgentCode());
					tLAQualificationDB.setQualifNo(aQualifno);
					if(tLAQualificationDB.getInfo())
					{
						this.mdelLAQualificationSet = tLAQualificationDB.query();
						map.put(mdelLAQualificationSet, "DELETE");
					}
				}
			}
			//担保人信息处理
			for(int i = 1;i<=this.mLAWarrantorSet.size();i++)
			{
				LAWarrantorSchema tLAWarrantorSchema = new LAWarrantorSchema();
				tLAWarrantorSchema = mLAWarrantorSet.get(i);
				tLAWarrantorSchema.setAgentCode(this.mAgentCode);
				tLAWarrantorSchema.setSerialNo(i);
				tLAWarrantorSchema.setAgentCode(this.mAgentCode);
				tLAWarrantorSchema.setMakeDate(this.currentDate);
				tLAWarrantorSchema.setMakeTime(this.currentTime);
				tLAWarrantorSchema.setModifyDate(this.currentDate);
				tLAWarrantorSchema.setModifyTime(this.currentTime);
				tLAWarrantorSchema.setOperator(this.mGlobalInput.Operator);
								
				mInLAWarrantorSet.add(tLAWarrantorSchema);
				
				
			}
			
			
			map.put(mLAAgentSchema, "INSERT");
			map.put(mLATreeSchema, "INSERT");						
			map.put(mLATreeSet, "UPDATE");
			map.put(mLATreeBSet, "INSERT");									
			map.put(mLAAgentSet, "UPDATE");	
			map.put(mLAAgentBSet, "INSERT");						
			map.put(mLABranchGroupSet, "UPDATE");
			map.put(mLABranchGroupBSet, "INSERT");			
			map.put(mInLAWarrantorSet, "INSERT");
			
		}
		if("UPDATE||MAIN".equals(this.mOperate))
		{
			LAAgentDB tLAAgentDB = new LAAgentDB();
			tLAAgentDB.setAgentCode(this.mLAAgentSchema.getAgentCode());
			if(!tLAAgentDB.getInfo())
			{
				CError tError = new CError();
			    tError.moduleName = "LAActiveActionAgentBL";
			    tError.functionName = "prepareOutputData";
			    tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			    this.mErrors.addOneError(tError);
			    return false;				
			}
			LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
			Reflections tReflections = new Reflections();
			tReflections.transFields(tLAAgentBSchema, tLAAgentDB.getSchema());
			
	        tLAAgentBSchema.setEdorNo(this.mEdorNo);
	        tLAAgentBSchema.setEdorType("05");
	        tLAAgentBSchema.setModifyDate(this.currentDate);
	        tLAAgentBSchema.setModifyTime(this.currentTime);
	        tLAAgentBSchema.setOperator(this.mGlobalInput.Operator);	        				        
	        mLAAgentBSet.add(tLAAgentBSchema);
			
			this.mLAAgentSchema.setMakeDate(tLAAgentDB.getMakeDate());
			this.mLAAgentSchema.setMakeTime(tLAAgentDB.getMakeTime());
			
			
			this.mLAAgentSchema.setOperator(this.mGlobalInput.Operator);
			this.mLAAgentSchema.setModifyDate(this.currentDate);
			this.mLAAgentSchema.setModifyTime(this.currentTime);
			
			
			LATreeDB tLATreeDB = new LATreeDB();
			tLATreeDB.setAgentCode(this.mLATreeSchema.getAgentCode());
			if(!tLATreeDB.getInfo())
			{
				CError tError = new CError();
			    tError.moduleName = "LAActiveActionAgentBL";
			    tError.functionName = "dealdata";
			    tError.errorMessage = "人员行政信息查询失败。";
			    this.mErrors.addOneError(tError);
			    return false;									
			}
			LATreeBSchema tLATreeBSchema = new LATreeBSchema();
			tReflections.transFields(tLATreeBSchema, tLATreeDB.getSchema());
			
	        tLATreeBSchema.setRemoveType("05");
	        tLATreeBSchema.setEdorNO(this.mEdorNo);
	        tLATreeBSchema.setMakeDate(currentDate);
	        tLATreeBSchema.setMakeTime(currentTime);
	        tLATreeBSchema.setModifyDate(currentDate);
	        tLATreeBSchema.setModifyTime(currentTime);
	        tLATreeBSchema.setOperator(mGlobalInput.Operator);
	        
	        tLATreeBSchema.setMakeDate2(tLATreeDB.getMakeDate());
	        tLATreeBSchema.setMakeTime2(tLATreeDB.getMakeTime());
	        tLATreeBSchema.setModifyDate2(tLATreeDB.getModifyDate());
	        tLATreeBSchema.setModifyTime2(tLATreeDB.getModifyTime());
	        tLATreeBSchema.setOperator2(tLATreeDB.getOperator());
	        
	        this.mLATreeBSet.add(tLATreeBSchema);
	        
	        this.mLATreeSchema.setUpAgent(tLATreeDB.getUpAgent()); // add by fuxin 2014-12-7
			this.mLATreeSchema.setAgentSeries(tLATreeDB.getAgentSeries());
			this.mLATreeSchema.setStartDate(tLATreeDB.getStartDate());
			this.mLATreeSchema.setAstartDate(tLATreeDB.getAstartDate());
			this.mLATreeSchema.setBranchCode(tLATreeDB.getAgentGroup());
			this.mLATreeSchema.setInitGrade(tLATreeDB.getAgentGrade());
			
            //操作信息
			this.mLATreeSchema.setOperator(this.mGlobalInput.Operator);
			this.mLATreeSchema.setMakeDate(this.currentDate);
			this.mLATreeSchema.setMakeTime(this.currentTime);
			this.mLATreeSchema.setModifyDate(this.currentDate);
			this.mLATreeSchema.setModifyTime(this.currentTime);
			
			// 资格证信息处理
			//add by wrx 2015-7-9
			if(this.mLAQualificationSchema.getQualifNo()!=null&&!this.mLAQualificationSchema.getQualifNo().equals("") ){
				LAQualificationDB tLAQualificationDB = new LAQualificationDB();
				tLAQualificationDB.setAgentCode(this.mLAAgentSchema.getAgentCode());
				tLAQualificationDB.setQualifNo(this.mLAQualificationSchema.getQualifNo());
//				if(!tLAQualificationDB.getInfo())
//				{
//					CError tError = new CError();
//					tError.moduleName = "LAActiveActionAgentBL";
//					tError.functionName = "submitData";
//					tError.errorMessage = "资格证信息查询失败!";
//					this.mErrors.addOneError(tError);
//					return false;				
//					
//				}
				this.mdelLAQualificationSet =tLAQualificationDB.query();
				if(this.mdelLAQualificationSet.size()>0)
				{
					for(int i = 1;i<=this.mdelLAQualificationSet.size();i++)
					{
						LAQualificationSchema tLAQualificationSchema = new LAQualificationSchema();
						tLAQualificationSchema = mdelLAQualificationSet.get(i);
						LAQualificationBSchema tLAQualificationBSchema = new LAQualificationBSchema();
						tReflections.transFields(tLAQualificationBSchema, tLAQualificationSchema);
						
						tLAQualificationBSchema.setEdorNo(this.mEdorNo);
						tLAQualificationBSchema.setMakeDate(this.currentDate);
						tLAQualificationBSchema.setMakeTime(this.currentTime);
						tLAQualificationBSchema.setModifyDate(this.currentDate);
						tLAQualificationBSchema.setModifyTime(this.currentTime);
						tLAQualificationBSchema.setOperator(this.mGlobalInput.Operator);
						
						tLAQualificationBSchema.setMakeDate1(tLAQualificationSchema.getMakeDate());
						tLAQualificationBSchema.setMakeTime1(tLAQualificationSchema.getMakeTime());
						tLAQualificationBSchema.setOperator1(tLAQualificationSchema.getOperator());
						this.mLAQualificationBSet.add(tLAQualificationBSchema);
						
					}
				}
				
				
				
				String strSQL = "Select 1 From LAQualification where AgentCode <> '"+this.mLAAgentSchema.getAgentCode()+"' " 
						+" and qualifno ='"+this.mLAQualificationSchema.getQualifNo()+"' " 
						+" and exists(Select 1 from laagent where agentcode = LAQualification.agentcode and branchtype= '"+this.mLAAgentSchema.getBranchType()+"' and branchtype2 = '"+this.mLAAgentSchema.getBranchType2()+"' "
						+" and agentstate < '06') fetch first 1 rows only";
				
				System.out.println("打印sql:"+strSQL);
				ExeSQL tExeSQL = new ExeSQL();
				String tResult = tExeSQL.getOneValue(strSQL);
				if(tResult!=null&&"1".equals(tResult))
				{
					CError tError = new CError();
					tError.moduleName = "LAActiveActionAgentBL";
					tError.functionName = "submitData";
					tError.errorMessage = "录入资格证系统已存在,烦请重新进行录入!";
					this.mErrors.addOneError(tError);
					return false;
					
				}
				
				mLAQualificationSchema.setAgentCode(this.mLAAgentSchema.getAgentCode());
				mLAQualificationSchema.setMakeDate(this.currentDate);
				mLAQualificationSchema.setMakeTime(this.currentTime);
				mLAQualificationSchema.setModifyDate(this.currentDate);
				mLAQualificationSchema.setModifyTime(this.currentTime);
				mLAQualificationSchema.setOperator(this.mGlobalInput.Operator);
	            map.put(mdelLAQualificationSet, "DELETE");
	            map.put(mLAQualificationBSet, "INSERT");
	            map.put(mLAQualificationSchema, "INSERT");
			}else{
				ExeSQL tExeSQL = new ExeSQL();
				String tSQL = "select qualifno from LAQualification where agentcode = '"+this.mLAAgentSchema.getAgentCode()+"'";
				SSRS tSSRS = tExeSQL.execSQL(tSQL);
				if(tSSRS.getMaxRow()>0){
					String aQualifno = tSSRS.GetText(1, 1);
					LAQualificationDB tLAQualificationDB = new LAQualificationDB();
					tLAQualificationDB.setAgentCode(this.mLAAgentSchema.getAgentCode());
					tLAQualificationDB.setQualifNo(aQualifno);
					if(tLAQualificationDB.getInfo())
					{
						this.mdelLAQualificationSet = tLAQualificationDB.query();
						map.put(mdelLAQualificationSet, "DELETE");
					}
				}
			}
			
			//担保人信息处理
			for(int i = 1;i<=this.mLAWarrantorSet.size();i++)
			{
				LAWarrantorSchema tLAWarrantorSchema = new LAWarrantorSchema();
				tLAWarrantorSchema = mLAWarrantorSet.get(i);
				tLAWarrantorSchema.setAgentCode(this.mLAAgentSchema.getAgentCode());
				tLAWarrantorSchema.setMakeDate(this.currentDate);
				tLAWarrantorSchema.setSerialNo(i);
				tLAWarrantorSchema.setMakeTime(this.currentTime);
				tLAWarrantorSchema.setModifyDate(this.currentDate);
				tLAWarrantorSchema.setModifyTime(this.currentTime);
				tLAWarrantorSchema.setOperator(this.mGlobalInput.Operator);
								
				mInLAWarrantorSet.add(tLAWarrantorSchema);
				
				
			}
		
			map.put(mLAAgentSchema, "UPDATE");
			map.put(mLATreeSchema, "UPDATE");						

			map.put(mLATreeBSet, "INSERT");									
			map.put(mLAAgentBSet, "INSERT");				
            
			map.put(mInLAWarrantorSet, "DELETE&INSERT");
			
			
			
			
		}
		
		return true;
	}
    private boolean dealLAreal(){

    	//if (this.mOperate.equals("INSERT||MAIN") ){
    	 String tAgentCode=""+mLATreeSchema.getAgentCode();
       mMakeContNum = PubFun1.CreateMaxNo("EdorNo", 20);
       LARearRelationSet tLARearRelationSet1 = new  LARearRelationSet();
       LARearRelationDB tLARearRelationDB = new  LARearRelationDB();
       LARearRelationBSet  tmLARearRelationBSet = new  LARearRelationBSet();
       tLARearRelationDB.setAgentCode(tAgentCode);
       tLARearRelationSet1 = tLARearRelationDB.query();
       System.out.println("备份推荐表的代数为："+tLARearRelationSet1.size());
       if(tLARearRelationSet1.size()>0)
       {
          System.out.println(tLARearRelationSet1.size());
               for (int k = 1; k <= tLARearRelationSet1.size(); k++) {
                   System.out.println("开始备份");
                   LARearRelationBSchema  tmLARearRelationBSchema=new LARearRelationBSchema();
                   ref.transFields(tmLARearRelationBSchema,
                                   tLARearRelationSet1.get(k));
                   tmLARearRelationBSchema.setEdorNo(mMakeContNum);
                   tmLARearRelationBSchema.setEdorType("09"); //二次增员更改推荐人
                   tmLARearRelationBSchema.setOperator2(mGlobalInput.Operator);
                   tmLARearRelationBSchema.setMakeDate2(currentDate);
                   tmLARearRelationBSchema.setMakeTime2(currentTime);
                   tmLARearRelationBSchema.setModifyDate2(currentDate);
                   tmLARearRelationBSchema.setModifyTime2(currentTime);
                   tmLARearRelationBSet.add(tmLARearRelationBSchema);
                   System.out.println("1111111111111");
           }
           this.map.put(tmLARearRelationBSet, "INSERT");
       }
          if (!tLARearRelationSet1.mErrors.needDealError()) {

               this.map.put(tLARearRelationSet1, "DELETE");
           }
            String tRearAgentCode = ""+mLATreeSchema.getIntroAgency();
            if (tRearAgentCode.equals("")) {
                System.out.println("没有推荐人");
                return true;
            }
            String tsql = "select agentgrade from latree a where  a.agentcode='"+tRearAgentCode+"'";
            ExeSQL tExeSQL = new ExeSQL();
            String tRearGrade = tExeSQL.getOneValue(tsql);

            tsql = "select GradeProperty2 from   laagentgrade A where a.gradecode='"+tRearGrade+"' ";
            ExeSQL texesql = new ExeSQL();
            String tupgrade = texesql.getOneValue(tsql);

            tsql = "select GradeProperty2 from laagentgrade where gradecode='"
                   +mLATreeSchema.getAgentGrade()+"'";
            String tgrade = texesql.getOneValue(tsql);
            //当增加的业务员为主任及以上级别，且新增业务员与推荐人的级别相同，生成育成关系
            if (!tgrade.equals("0")&&!mLATreeSchema.getAgentGrade().equals("B01")
                &&tgrade.equals(tupgrade) && !tRearGrade.equals("B01")) {
                LARearRelationSchema tRearRSchema = new LARearRelationSchema();
                tRearRSchema.setAgentCode(mLATreeSchema.getAgentCode());
                tRearRSchema.setAgentGroup(mLATreeSchema.getAgentGroup());
                tRearRSchema.setRearAgentCode(tRearAgentCode);
                String tAgentGroup = mLATreeSchema.getAgentGroup();
                tsql = "select branchlevel from labranchgroup where agentgroup='"+tAgentGroup+"'";
                tgrade = texesql.getOneValue(tsql);
                tRearRSchema.setRearLevel(tgrade);
                return addRear(tRearRSchema);
           }
        
    	return true;
    }   
	/**
	 * 准备往后层输出所需要的数据
	 * 输出：如果准备数据时发生错误则返回false,否则返回true
	 */
	private boolean prepareOutputData() {
		try {
			mInputData.clear();
			mInputData.add(map);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAActiveActionAgentBL";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

   /**
    *  获取人员信息
    */
	private String getAgentName(String tAgentCode)
	{
		String result = "";
		LAAgentDB tLAAgentDB = new LAAgentDB();
		tLAAgentDB.setAgentCode(tAgentCode);
		if(!tLAAgentDB.getInfo())
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAActiveActionAgentBL";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "人员基础信息查询失败。";
			this.mErrors.addOneError(tError);
			return null;
			
		}
		result = tLAAgentDB.getName();
		return result;
	}
	
    public boolean addRear(LARearRelationSchema cLARearRScehma) {
        //将被推荐人的所在机构全部找出
//        mLABranchGroupSet.clear();
//        if (!findSelfBranch(tAgentCode, tAgentGroup)) {
//            return false;
//        }
        String tBranchLevel = cLARearRScehma.getRearLevel().trim();
        //按条件复制推荐人的所有推荐关系
        LARearRelationSet tLARearRelationSet = new LARearRelationSet();
        LARearRelationDB tLARearRelationDB = new LARearRelationDB();
        tLARearRelationDB.setAgentCode(cLARearRScehma.getRearAgentCode());
        tLARearRelationDB.setRearFlag("1");
        tLARearRelationSet = tLARearRelationDB.query();
        tLARearRelationSet.add(cLARearRScehma);
        for (int j = 1; j <= tLARearRelationSet.size(); j++) {
            LARearRelationSchema tLARearRelationSchema =
                    tLARearRelationSet.get(j);
            tLARearRelationSchema.setRearLevel(tBranchLevel);
            tLARearRelationSchema.setRearedGens(tLARearRelationSchema.
                                                getRearedGens() + 1);
            tLARearRelationSchema.setAgentCode(cLARearRScehma.getAgentCode());
            tLARearRelationSchema.setAgentGroup(cLARearRScehma.getAgentGroup());
            tLARearRelationSchema.setstartDate(currentDate);
            tLARearRelationSchema.setRearFlag("1");
            tLARearRelationSchema.setRearCommFlag("0");
            tLARearRelationSchema.setRearStartYear("1");
            tLARearRelationSchema.setMakeDate(currentDate);
            tLARearRelationSchema.setMakeTime(currentTime);
            tLARearRelationSchema.setModifyDate(currentDate);
            tLARearRelationSchema.setModifyTime(currentTime);
            tLARearRelationSchema.setOperator(mGlobalInput.Operator);
        }
        map.put(tLARearRelationSet, "INSERT");
        return true;
    }
    private boolean dealLARecom(){

        if(1==1){
   // if (this.mOperate.equals("DELETE||INSERT") ){
            //增员管理
       String tAgentCode=""+mLATreeSchema.getAgentCode();
       mMakeContNum = PubFun1.CreateMaxNo("EdorNo", 20);
       LARecomRelationSet tLARecomRelationSet1 = new  LARecomRelationSet();
       LARecomRelationDB tLARecomRelationDB = new  LARecomRelationDB();
       LARecomRelationBSet  tmLARecomRelationBSet = new  LARecomRelationBSet();
       tLARecomRelationDB.setAgentCode(tAgentCode);
       tLARecomRelationSet1 = tLARecomRelationDB.query();
       System.out.println("备份推荐表的代数为："+tLARecomRelationSet1.size());
       if(tLARecomRelationSet1.size()>0)
       {
          System.out.println(tLARecomRelationSet1.size());
               for (int k = 1; k <= tLARecomRelationSet1.size(); k++) {
                   System.out.println("33333333333333");
                   LARecomRelationBSchema  tmLARecomRelationBSchema=new LARecomRelationBSchema();
                   ref.transFields(tmLARecomRelationBSchema,
                                   tLARecomRelationSet1.get(k));
                   tmLARecomRelationBSchema.setEdorNo(mMakeContNum);
                   tmLARecomRelationBSchema.setEdorType("09"); //二次增员更改推荐人
                   tmLARecomRelationBSchema.setOperator2(mGlobalInput.Operator);
                   tmLARecomRelationBSchema.setMakeDate2(currentDate);
                   tmLARecomRelationBSchema.setMakeTime2(currentTime);
                   tmLARecomRelationBSchema.setModifyDate2(currentDate);
                   tmLARecomRelationBSchema.setModifyTime2(currentTime);
                   tmLARecomRelationBSet.add(tmLARecomRelationBSchema);
  //                 System.out.println("1111111111111"+tmLARecomRelationBSet.get(k).getAgentCode());
           }
           this.map.put(tmLARecomRelationBSet, "INSERT");
       }
          if (!tLARecomRelationSet1.mErrors.needDealError()) {

               this.map.put(tLARecomRelationSet1, "DELETE");
           }

            String tRecomAgentCode = ""+mLATreeSchema.getIntroAgency();
            System.out.println("tRecomAgentCode:"+tRecomAgentCode);
            if (tRecomAgentCode.equals("")) {
                System.out.println("没有推荐人");
                return true;
            }
            LARecomRelationSchema tRecomRSchema = new LARecomRelationSchema();
            tRecomRSchema.setAgentCode(mLATreeSchema.getAgentCode());
            tRecomRSchema.setAgentGrade(mLATreeSchema.getAgentGrade());
            tRecomRSchema.setAgentGroup(mLATreeSchema.getAgentGroup());

            //=====startdate======//
            tRecomRSchema.setStartDate(mLATreeSchema.getIntroCommStart());

            tRecomRSchema.setRecomAgentCode(tRecomAgentCode);
            LATreeDB tLATreeDB = new LATreeDB();
            tLATreeDB.setAgentCode(tRecomAgentCode);
            if(!tLATreeDB.getInfo()){
                CError.buildErr(this,"推荐人职级信息查询失败！");
                return false;
            }
            tRecomRSchema.setRecomAgentGrade(tLATreeDB.getAgentGrade());
            ExeSQL texesql = new ExeSQL();
            String tAgentGroup = mLATreeSchema.getAgentGroup();
            String tsql = "select branchlevel from labranchgroup where agentgroup='"+tAgentGroup+"'";
            String tgrade = texesql.getOneValue(tsql);

            tRecomRSchema.setRecomLevel(tgrade);

            return addRecom(tRecomRSchema);
     }
        return true;
    
    }
    public boolean addRecom(LARecomRelationSchema tRecomRSchema) {

        String tAgentCode = tRecomRSchema.getAgentCode();
        String tAgentGroup = tRecomRSchema.getAgentGroup();
        String tStartDate = tRecomRSchema.getStartDate();
        //按条件复制推荐人的所有推荐关系
        LARecomRelationSet tLARecomRelationSet = new LARecomRelationSet();
        LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();
        tLARecomRelationDB.setAgentCode(tRecomRSchema.getRecomAgentCode());
        tLARecomRelationDB.setRecomFlag("1");//已失效的关系不需要再进行复制
        tLARecomRelationSet = tLARecomRelationDB.query();
        tLARecomRelationSet.add(tRecomRSchema);
        for (int j = 1; j <= tLARecomRelationSet.size(); j++) {
            LARecomRelationSchema tLARecomRelationSchema = tLARecomRelationSet.get(j);
            tLARecomRelationSchema.setRecomGens(tLARecomRelationSchema.
                                                getRecomGens() + 1);
            tLARecomRelationSchema.setAgentCode(tAgentCode);
            tLARecomRelationSchema.setAgentGroup(tAgentGroup);
            tLARecomRelationSchema.setStartDate(tStartDate);
            tLARecomRelationSchema.setRecomFlag("1");
            tLARecomRelationSchema.setRecomComFlag("1");
            tLARecomRelationSchema.setRecomStartYear("1");
            tLARecomRelationSchema.setMakeDate(currentDate);
            tLARecomRelationSchema.setMakeTime(currentTime);
            tLARecomRelationSchema.setModifyDate(currentDate);
            tLARecomRelationSchema.setModifyTime(currentTime);
            tLARecomRelationSchema.setOperator(mGlobalInput.Operator);
        }
        map.put(tLARecomRelationSet, "INSERT");
        return true;
    
    }
}
