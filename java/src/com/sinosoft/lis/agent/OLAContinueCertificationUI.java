/*
 * <p>ClassName: OLACertificationUI </p>
 * <p>Description: OLACertificationUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:07:04
 */
package com.sinosoft.lis.agent;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.pubfun.*;

public class OLAContinueCertificationUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private String  mCertifNoQuery;
    private GlobalInput mGlobalInput = new GlobalInput();
    //业务处理相关变量
    /** 全局数据 */
    private LACertificationSchema mLACertificationSchema = new
            LACertificationSchema();
    public OLAContinueCertificationUI(){}

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("iiiii");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;

        //进行业务处理
        if (!dealData())
            return false;

        //准备往后台的数据
        if (!prepareOutputData())
            return false;

        OLAContinueCertificationBL tOLACertificationBL = new OLAContinueCertificationBL();

        System.out.println("Start OLACertification UI Submit...");
        tOLACertificationBL.submitData(mInputData, mOperate);
        System.out.println("End OLACertification UI Submit...");
        //如果有需要处理的错误，则返回
        if (tOLACertificationBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tOLACertificationBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "OLACertificationUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mOperate.equals("INSERT||MAIN"))
        {
            this.mResult.clear();
            this.mResult = tOLACertificationBL.getResult();
        }
        mInputData = null;
        return true;
    }

    public static void main(String[] args)
    {
        LACertificationSchema tLACertificationSchema = new
                LACertificationSchema();
        tLACertificationSchema.setAgentCode("1101000051");
        tLACertificationSchema.setCertifNo("86110d00024");
        // tLACertificationSchema.setIdx(request.getParameter("Idx"));
        tLACertificationSchema.setAuthorUnit("8611000024");
        tLACertificationSchema.setGrantDate("2005-12-01");
        //tLACertificationSchema.setInvalidDate(request.getParameter("InvalidDate"));
        tLACertificationSchema.setInvalidRsn("");
        //tLACertificationSchema.setreissueDate(request.getParameter("reissueDate"));
        //tLACertificationSchema.setreissueRsn(request.getParameter("reissueRsn"));
        tLACertificationSchema.setValidStart("2006-01-01");
        tLACertificationSchema.setValidEnd("2008-12-31");
        tLACertificationSchema.setState("0");
        //tLACertificationSchema.setMakeDate(request.getParameter("MakeDate"));
        //tLACertificationSchema.setMakeTime(request.getParameter("MakeTime"));
        //tLACertificationSchema.setModifyDate(request.getParameter("ModifyDate"));
        //tLACertificationSchema.setModifyTime(request.getParameter("ModifyTime"));
        VData tVData = new VData();
        GlobalInput tG = new GlobalInput();
        tG.ComCode = "86";
        tG.Operator = "001";
        tVData.add("");
        tVData.add(tLACertificationSchema);
        tVData.add(tG);
        OLAContinueCertificationUI tOLACertificationUI = new OLAContinueCertificationUI();
        String transact = "INSERT||MAIN";
        tOLACertificationUI.submitData(tVData, transact);

    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(mCertifNoQuery);
            mInputData.add(this.mLACertificationSchema);
            mInputData.add(this.mGlobalInput);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LACertificationUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        this.mLACertificationSchema.setSchema((LACertificationSchema)
                                              cInputData.getObjectByObjectName(
                "LACertificationSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mCertifNoQuery=String.valueOf(cInputData.get(0));
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
