package com.sinosoft.lis.agent;


import com.sinosoft.lis.db.LAOrphanPolicyDB;
import com.sinosoft.lis.db.LAOrphanPolicyBDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LAOrphanPolicySchema;
import com.sinosoft.lis.schema.LAOrphanPolicyBSchema;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.vschema.LAOrphanPolicySet;
import com.sinosoft.lis.vschema.LAOrphanPolicyBSet;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.PubFun1;
/**

 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LAOrphanPolicyPICCHBL  implements  LAOrphanPolicyInterface{

    MMap map = new MMap();
    String mOperate = new String();
    String tAgentCode = new String();
    GlobalInput mGlobalInput = new GlobalInput();
   private String tContno="";
   private   LAOrphanPolicySchema tLAOrphanPolicySchema = new LAOrphanPolicySchema();
   private   LAOrphanPolicyBSchema tLAOrphanPolicyBSchema = new LAOrphanPolicyBSchema();
   private   LAOrphanPolicySet  tLAOrphanPolicySet = new LAOrphanPolicySet();
   private   LAOrphanPolicyBSet  tLAOrphanPolicyBSet = new LAOrphanPolicyBSet();
   private String currentDate = PubFun.getCurrentDate();
   private String currentTime = PubFun.getCurrentTime();

   public LAOrphanPolicyPICCHBL() {
    }

   public void setAgentCode(String mAgentCode)
   {
        tAgentCode = mAgentCode;
   }
   public void setGlobalInput(GlobalInput cGlobalInput) {
       mGlobalInput.setSchema(cGlobalInput);
   }

    public boolean dealdata()
    {
        String tSQL = "";

        //得到要删除和插入备份的数据
        tSQL = "select * from laorphanpolicy where  flag='1'  ";
        tSQL += "and  agentcode = '" + tAgentCode + "'";

        //执行查询语句
        LAOrphanPolicyDB tLAOrphanPolicyDB = new LAOrphanPolicyDB();
        tLAOrphanPolicySet = tLAOrphanPolicyDB.executeQuery(tSQL);
        System.out.println(tSQL);
        //存在
        if (tLAOrphanPolicySet.size() >= 1) {
            String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            System.out.println(tLAOrphanPolicySet.size());
            for (int i = 1; i <= tLAOrphanPolicySet.size(); i++)
            {
                LAOrphanPolicySchema tLAOrphanPolicySchema = new  LAOrphanPolicySchema();
                LAOrphanPolicyBSchema tLAOrphanPolicyBSchema = new LAOrphanPolicyBSchema();
                tLAOrphanPolicySchema = tLAOrphanPolicySet.get(i);
                //tLAOrphanPolicySet.add(tLAOrphanPolicySchema);
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLAOrphanPolicyBSchema,tLAOrphanPolicySchema);
                tLAOrphanPolicyBSchema.setEdorNo(tEdorNo);
                tLAOrphanPolicyBSchema.setEdorType("08");//08是孤儿单回退
                tLAOrphanPolicyBSet.add(tLAOrphanPolicyBSchema);
            }
            map.put(tLAOrphanPolicyBSet, "INSERT");
            map.put(tLAOrphanPolicySet, "DELETE");
                 System.out.println("11111222222");
        }
        return true;
    }
     public MMap getResult()
     {
          return map;
     }

}
