package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgenttempDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgenttempSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: AgentSystem</p>
 * <p>Description: 销售管理——增员管理</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author xjh
 * @version 1.0
 */

public class LABankRejectAgentBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private MMap map = new MMap();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAgenttempSchema mLAAgenttempSchema = new LAAgenttempSchema();
    /** 数据操作字符串 */
  private String currentDate = PubFun.getCurrentDate();
  private String currentTime = PubFun.getCurrentTime();
    public LABankRejectAgentBL() {
    }

    public static void main(String[] args) {
        LABankRejectAgentBL LABankRejectAgentBL = new LABankRejectAgentBL();
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Begin LABankRejectAgentBL.submitData.........");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start LABankRejectAgentBL Submit...");
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LABankRejectAgentBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量

        System.out.println("Begin LABankRejectAgentBL.getInputData.........");
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLAAgenttempSchema.setSchema((LAAgenttempSchema) cInputData.
                                      getObjectByObjectName("LAAgenttempSchema", 0));
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABankRejectAgentBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    /**
     * 业务处理主函数
     * @return boolean
     */
    public boolean dealData() {
    	LAAgenttempDB tLAAgenttempDB=new LAAgenttempDB();
    	tLAAgenttempDB.setAgentCode(this.mLAAgenttempSchema.getAgentCode());
    	if(!tLAAgenttempDB.getInfo()){
            CError tError = new CError();
            tError.moduleName = "LABankRejectAgentBL";
            tError.functionName = "submitData";
            tError.errorMessage = "获取代理人录入基础信息失败!";
            this.mErrors.addOneError(tError);
            return false;
    	}
    	String tempSource=this.mLAAgenttempSchema.getSource();
    	this.mLAAgenttempSchema=tLAAgenttempDB.getSchema();
    	this.mLAAgenttempSchema.setAgentState("1");
    	this.mLAAgenttempSchema.setSource(tempSource);
    	this.mLAAgenttempSchema.setOperator(this.mGlobalInput.Operator);
    	this.mLAAgenttempSchema.setModifyDate(this.currentDate);
    	this.mLAAgenttempSchema.setModifyTime(this.currentTime);
    	this.map.put(this.mLAAgenttempSchema, "UPDATE");
    	return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            System.out.println("Begin LABankRejectAgentBL.prepareOutputData.........");
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABankRejectAgentBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

}
