package com.sinosoft.lis.agent;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.vschema.LAWarrantorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public class LAGrpAgentBLF
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private MMap map = new MMap();
    private String mIsManager;
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private LAWarrantorSet mLAWarrantorSet = new LAWarrantorSet();
    private LARearRelationSet mLARearRelationSet = new LARearRelationSet();
    //处理类
    //private LAAgentDealBL mLAAgentDealBL = new LAAgentDealBL();
    private Class mCreateCodeClass;
    private LACreateCodeInterface mLACreateCodeInterface;
    private Class mLATreeClass;
    private LATreeInterface mLATreeInterface;
    private Class mLAAgentClass;
    private LAAgentInterface mLAAgentInterface;
    private Class mLARearClass;
    private LARearInterface mLARearInterface;
    private String mAgentCode = "";
    private String mAgentGroup = "";
    private String mBranchCode = "";
    private String mEdorNo = "";

    public String getAgentCode()
    {
        return mAgentCode;
    }

    public LAGrpAgentBLF()
    {
    }

    public static void main(String[] args)
    {
        LAGrpAgentBLF laagentblf = new LAGrpAgentBLF();
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("Begin LAAgentBLF.submitData.........");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start LAAgentBLF Submit...");
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentBLF";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("Begin LAAgentBLF.getInputData.........");
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mIsManager = (String) cInputData.getObject(1);
        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                                      getObjectByObjectName("LAAgentSchema", 0));
        this.mLATreeSchema.setSchema((LATreeSchema) cInputData.
                                     getObjectByObjectName("LATreeSchema", 0));
        this.mLAWarrantorSet.set((LAWarrantorSet) cInputData.
                                 getObjectByObjectName("LAWarrantorSet", 0));
        this.mLARearRelationSet.set((LARearRelationSet) cInputData.
                                    getObjectByObjectName("LARearRelationSet",
                0));
        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentBLF";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

//    /**
//     * 根据前面的输入数据，进行UI逻辑处理
//     * 如果在处理过程中出错，则返回false,否则返回true
//     */
//    private boolean dealData()
//    {
//        System.out.println("Begin LAAgentBLF.dealData.........");
//        //往LAAgentChoiceBL里传入参数
//        mLAAgentDealBL.setOperate(mOperate);
//        mLAAgentDealBL.setGlobalInput(mGlobalInput);
//        mLAAgentDealBL.setLAAgentSchema(mLAAgentSchema);
//        mLAAgentDealBL.setLARearRelationSet(mLARearRelationSet);
//        mLAAgentDealBL.setLATreeSchema(mLATreeSchema);
//        mLAAgentDealBL.setLAWarrantorSet(mLAWarrantorSet);
//        mLAAgentDealBL.setIsManager(mIsManager);
//        //处理
//        if (!mLAAgentDealBL.dealData())
//        {
//            return false;
//        }
//        map = mLAAgentDealBL.getResult();
//        return true;
//    }

    /**
     * 业务处理主函数
     * @return boolean
     */
    public boolean dealData()
    {
        System.out.println("Begin LAAgentDealBL.dealData.........");
        //查找项目组名称
        String tSQL = "select VarValue from LASysVar where VarType = 'prname' ";
        ExeSQL tExeSQL = new ExeSQL();
        String Project = tExeSQL.getOneValue(tSQL);
        if (Project == null)
        {
            Project = "";
        }
        Project = Project.trim();
        System.out.println("Project:"+Project) ;

        /**生成编码*/
        //获取处理类
        if (Project.equals(""))
        {
            if (!getCreateCodeClass("Product"))
            {
                return false;
            }
        }
        else
        {
            if (!getCreateCodeClass(Project))
            {
                return false;
            }
        }
        System.out.println("Project:"+Project) ;
        //传入参数
        mLACreateCodeInterface.setManagecom(mLAAgentSchema.getManageCom());
        mLACreateCodeInterface.setOperate(mOperate);
        mLACreateCodeInterface.setBranchType(mLAAgentSchema.getBranchType());
        mLACreateCodeInterface.setBranchType2(mLAAgentSchema.getBranchType2());
        //处理
        if (!mLACreateCodeInterface.dealdata())
        {
            mErrors.copyAllErrors(mLACreateCodeInterface.mErrors);
            return false;
        }
        //获取处理结果
        if (mOperate.equals("INSERT||MAIN"))
        {
            mAgentCode = mLACreateCodeInterface.getAgentCode();
            System.out.println(mLACreateCodeInterface.getAgentCode());
        }
        else
        {
            mAgentCode = mLAAgentSchema.getAgentCode();
        }
        mEdorNo = mLACreateCodeInterface.getEdorNo();
        /**处理基本信息*/
        //获取处理类
        if (Project.equals(""))
        {
            if (!getAgentClass("Product"))
            {
                return false;
            }
        }
        else
        {
            if (!getAgentClass(Project))
            {
                return false;
            }
        }
        //传入参数
        System.out.println("))))))))))" + mAgentCode);
        mLAAgentInterface.setOperate(mOperate);
        mLAAgentInterface.setAgentCode(mAgentCode);
        mLAAgentInterface.setEdorNo(mEdorNo);
        mLAAgentInterface.setGlobalInput(mGlobalInput);
        mLAAgentInterface.setLAAgentSchema(mLAAgentSchema);
        mLAAgentInterface.setLATreeSchema(mLATreeSchema);
        mLAAgentInterface.setLAWarrantorSet(mLAWarrantorSet);
        mLAAgentInterface.setOperate(mOperate);
        //处理
        if (!mLAAgentInterface.dealdata())
        {
            mErrors.copyAllErrors(mLAAgentInterface.mErrors);
            return false;
        }
        //获取处理结果
        MMap tMap1 = mLAAgentInterface.getResult();
        map.add(tMap1);
        /**处理行政信息*/
        //获取处理类
        if (Project.equals(""))
        {
            if (!getTreeClass("Product"))
            {
                return false;
            }
        }
        else
        {
            if (!getTreeClass(Project))
            {
                return false;
            }
        }
        //传入参数
        mLATreeInterface.setOperate(mOperate);
        mLATreeInterface.setAgentCode(mAgentCode);
        mLATreeInterface.setEdorNo(mEdorNo);
        mLATreeInterface.setGlobalInput(mGlobalInput);
        mLATreeInterface.setIsManager(mIsManager);
        mLATreeInterface.setLATreeSchema(mLATreeSchema);
        mLATreeInterface.setLAAgentSchema(mLAAgentInterface.getLAAgentSchema());
        //处理
        if (!mLATreeInterface.dealdata())
        {
            mErrors.copyAllErrors(mLATreeInterface.mErrors);
            return false;
        }
        //获取处理结果
        MMap tMap2 = mLATreeInterface.getResult();
        map.add(tMap2);
        /**处理育成关系*/
        if (mLARearRelationSet.size() > 0)
        {
            //获取处理类
            if (Project.equals(""))
            {
                if (!getRearClass("Product"))
                {
                    return false;
                }
            }
            else
            {
                if (!getRearClass(Project))
                {
                    return false;
                }
            }
            //传入参数
            mLARearInterface.setOperate(mOperate);

            mLARearInterface.setLATreeSchema(mLATreeInterface.getLATreeSchema());
            mLARearInterface.setEdorNo(mEdorNo);
            mLARearInterface.setGlobalInput(mGlobalInput);
            mLARearInterface.setLARearRelationSet(mLARearRelationSet);
            //处理
            if (!mLARearInterface.dealdata())
            {
                mErrors.copyAllErrors(mLARearInterface.mErrors);
                return false;
            }
            //获取处理结果
            MMap tMap3 = mLARearInterface.getResult();
            map.add(tMap3);
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            System.out.println("Begin LAAgentBLF.prepareOutputData.........");
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentBLF";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 获得创建代理人编码创建类
     * @param cName String
     * @return boolean
     */
    private boolean getCreateCodeClass(String cName)
    {
        try
        {
            mCreateCodeClass = Class.forName(
                    "com.sinosoft.lis.agent.LACreateCode" + cName + "BL");
            mLACreateCodeInterface = (LACreateCodeInterface) mCreateCodeClass.
                                     newInstance();
        }
        catch (ClassNotFoundException ex1)
        {
            System.out.println(
                    ".........Not Found LACreateCode" + cName + "BL，错误");
            if (!cName.equals("Product"))
            {
                if (!getCreateCodeClass("Product"))
                {
                    return false;
                }
            }
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAAgentDealBL";
            tError.functionName = "getCreateCodeClass";
            tError.errorMessage = "没有找到LACreateCode" + cName + "BL类！";
            this.mErrors.addOneError(tError);
            return false;
        }
        catch (Exception ex1)
        {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAAgentDealBL";
            tError.functionName = "getCreateCodeClass";
            tError.errorMessage = "调用LACreateCode" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 获得代理人基本信息处理类
     * @param cName String
     * @return boolean
     */
    private boolean getAgentClass(String cName)
    {
        try
        {
            mLAAgentClass = Class.forName(
                    "com.sinosoft.lis.agent.LAAgent" + cName + "BL");
            mLAAgentInterface = (LAAgentInterface) mLAAgentClass.
                                newInstance();
        }
        catch (ClassNotFoundException ex1)
        {
            System.out.println(
                    ".........Not Found LAAgent" + cName + "BL，错误");
            if (!cName.equals("Product"))
            {
                if (!getAgentClass("Product"))
                {
                    return false;
                }
            }
            return true;
        }
        catch (Exception ex1)
        {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAAgentDealBL";
            tError.functionName = "getAgentClass";
            tError.errorMessage = "调用LAAgent" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 获得行政信息处理类
     * @param cName String
     * @return boolean
     */
    private boolean getTreeClass(String cName)
    {
        try
        {
            mLATreeClass = Class.forName(
                    "com.sinosoft.lis.agent.LATree" + cName + "BL");
            mLATreeInterface = (LATreeInterface) mLATreeClass.
                               newInstance();
        }
        catch (ClassNotFoundException ex1)
        {
            System.out.println(
                    ".........Not Found LATree" + cName + "BL，错误");
            if (!cName.equals("Product"))
            {
                if (!getTreeClass("Product"))
                {
                    return false;
                }
            }
            return true;
        }
        catch (Exception ex1)
        {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAAgentDealBL";
            tError.functionName = "getTreeClass";
            tError.errorMessage = "调用LATree" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 获得育成关系处理类
     * @param cName String
     * @return boolean
     */
    private boolean getRearClass(String cName)
    {
        try
        {
            mLARearClass = Class.forName(
                    "com.sinosoft.lis.agent.LARear" + cName + "BL");
            mLARearInterface = (LARearInterface) mLARearClass.
                               newInstance();
        }
        catch (ClassNotFoundException ex1)
        {
            System.out.println(
                    ".........Not Found LARear" + cName + "BL，错误");
            if (!cName.equals("Product"))
            {
                if (!getRearClass("Product"))
                {
                    return false;
                }
            }
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAAgentDealBL";
            tError.functionName = "getRearClass";
            tError.errorMessage = "没有找到LARear" + cName + "BL类！";
            this.mErrors.addOneError(tError);
            return false;
        }
        catch (Exception ex1)
        {
            ex1.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "LAAgentDealBL";
            tError.functionName = "getRearClass";
            tError.errorMessage = "调用LARear" + cName + "BL的方法时失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
