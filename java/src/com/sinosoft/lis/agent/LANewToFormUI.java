package com.sinosoft.lis.agent;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LANewToFormUI {
    public LANewToFormUI() {
    }
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors(); //错误处理类
  private VData mResult = new VData();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  //业务处理相关变量

  private LAAgentSchema mLAAgentSchema= new LAAgentSchema();
  private LATreeSchema mLATreeSchema = new LATreeSchema();

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();
  LANewToFormBL tLANewToFormBL = new LANewToFormBL();

  /**
   传输数据的公共方法
   */
  /**
    传输数据的公共方法
    */
   public boolean submitData(VData cInputData, String cOperate)
   {
       System.out.println("Begin LANewToFormUI.submitData.........");
              //将操作数据拷贝到本类中
              this.mOperate = cOperate;
              try
              {
                tLANewToFormBL.submitData(cInputData, mOperate);
              }
              catch(Exception ex){
                ex.toString();
              }
              System.out.println("End LANewToFormUI Submit...");
              //如果有需要处理的错误，则返回
              if (tLANewToFormBL.mErrors.needDealError())
              {
                      // @@错误处理
                      this.mErrors.copyAllErrors(tLANewToFormBL.mErrors);
                      CError tError = new CError();
                      tError.moduleName = "LAGroupStandPremUI";
                      tError.functionName = "submitData";
                      tError.errorMessage = "数据提交失败!";
                      this.mErrors.addOneError(tError);
                      return false;
              }
              mInputData = null;
              return true;

   }

   public VData getResult()
   {
       return this.mResult;
   }
}
