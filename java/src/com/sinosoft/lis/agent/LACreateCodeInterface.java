package com.sinosoft.lis.agent;

import com.sinosoft.utility.CErrors;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public interface LACreateCodeInterface
{
    public CErrors mErrors = new CErrors();

    public void setOperate(String cOperate); //设置操作数

    public void setManagecom(String cManagecom); //设置管理机构

    public void setBranchType(String cBranchType);

    public void setBranchType2(String cBranchType2);

    public boolean dealdata(); //处理主方法

    public String getEdorNo(); //获得修改时获得转储号码

    public String getAgentCode(); //获得代理人编码
}
