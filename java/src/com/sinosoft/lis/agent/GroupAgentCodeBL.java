package com.sinosoft.lis.agent;

import com.cbsws.obj.RspUniSalescod;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LGWorkTraceSchema;
import com.sinosoft.lis.sys.UnisalescodFun;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class GroupAgentCodeBL {
	public CErrors mErrors = new CErrors();


	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	private MMap map = new MMap();


	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	/** 数据操作字符串 */
	private String mOperate;

	private LAAgentSchema tLAAgentSchema   = new LAAgentSchema();
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData))
			return false;
		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		// 准备往后台的数据
		if (!prepareOutputData())
			return false;
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "GroupAgentCodeBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";

			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		if(this.mErrors.getErrorCount()>0){
			return false;
		}
		return true;
	}

	private boolean getInputData(VData cInputData) {
		this.tLAAgentSchema.setSchema((LAAgentSchema) cInputData
				.getObjectByObjectName("LAAgentSchema", 0));
		this.mGlobalInput.setSchema((GlobalInput) cInputData
				.getObjectByObjectName("GlobalInput", 0));
		return true;
	}

	private boolean prepareOutputData() {
		try {
			this.mInputData.clear();
			mInputData.add(this.map);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GroupAgentCodeBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean dealData() {
		String sql="select * from laagent where managecom like '"+tLAAgentSchema.getManageCom()+"%' ";
		String tsql ="";
		String name=tLAAgentSchema.getName();
		String brachTypeCode=tLAAgentSchema.getBranchType();
		String agentCode=tLAAgentSchema.getAgentCode();
		String agentState=tLAAgentSchema.getAgentState();
		if(name!=null&&!"".equals(name)){
			tsql+="and name='"+name+"' ";
		}
		if(agentCode!=null&&!"".equals(agentCode)){
			tsql+="and agentCode='"+agentCode+"' ";
		}
		if(brachTypeCode!=null&&!"".equals(brachTypeCode)){
			String branchtype=brachTypeCode.substring(0,1);
			String branchtype2=brachTypeCode.substring(1,3);
			tsql+="and branchtype='"+branchtype+"' and branchtype2='"+branchtype2+"' ";
		}
		if(agentState!=null&&!"".equals(agentState)){
			if("1".equals(agentState)){
				tsql+="and AgentState<'06' ";
			}else if("1".equals(agentState)){
				tsql+="and AgentState>='06' ";
			}
		}
		tsql += " and (groupagentcode is null or groupagentcode = '') ";
		System.out.println("查询没有集团统一工号的业务员SQL：" + sql+tsql);
		LAAgentDB tLAAgentDB=new LAAgentDB();
		LAAgentSet tLAAgentSet=new LAAgentSet();
		tLAAgentSet=tLAAgentDB.executeQuery(sql+tsql);
		for(int i=1;i<=tLAAgentSet.size();i++){ 
			LAAgentSchema mLAAgentSchema = new LAAgentSchema();
			mLAAgentSchema=tLAAgentSet.get(i);
			if(mLAAgentSchema.getGroupAgentCode()!=null&&!"".equals(mLAAgentSchema.getGroupAgentCode())){
				 CError tError = new CError();
		         tError.moduleName = "GroupAgentCodeBL";
		         tError.functionName = "dealData";
		         tError.errorMessage = "已经获取集团工号，请不要重复获取！";
		         this.mErrors.addOneError(tError);
				return false;
			}
			 /**获取集团工号Unisalescod*/
	        String tSQL = "select GroupAgentCode from laagent where agentcode='"+mLAAgentSchema.getAgentCode()+"'";
	        ExeSQL tExeSQL = new ExeSQL();
	        String mGroupAgentCode = tExeSQL.getOneValue(tSQL);
	        UnisalescodFun fun=new UnisalescodFun();
	        RspUniSalescod mRspUniSalescod = new RspUniSalescod();
	        ExeSQL tExeSQL1=new ExeSQL();
	        String tmsql = "select QualifNo from  LAQualification where agentcode='"+mLAAgentSchema.getAgentCode()+"'";
    		String qualifiNo = tExeSQL1.getOneValue(tmsql);
        	mRspUniSalescod=fun.getUnisalescod("INSERT||MAIN", mLAAgentSchema, null,null,qualifiNo);
        	mLAAgentSchema.setGroupAgentCode(mRspUniSalescod.getUNI_SALES_COD());
        	mLAAgentSchema.setModifyDate(CurrentDate);
        	mLAAgentSchema.setModifyTime(CurrentTime);
	        if("01".equals(mRspUniSalescod.getMESSAGETYPE()))
			{
				 CError tError = new CError();
		         tError.moduleName = "GroupAgentCodeBL";
		         tError.functionName = "dealData";
		         tError.errorMessage ="业务员内部编码："+ mLAAgentSchema.getAgentCode() + mRspUniSalescod.getERRDESC();
		         if(mRspUniSalescod.getERRDESC().contains("统一工号为")){
		        	 System.out.println("集团存在统一工号，但核心没有！");
		        	 int begin = mRspUniSalescod.getERRDESC().indexOf("统一工号为");
		        	 String tGroupAgentCode = mRspUniSalescod.getERRDESC().substring(begin+5, begin+15);
		        	 System.out.println("截取出的统一工号："+tGroupAgentCode);
		        	 String tHSQL = "select agentcode from laagent where GroupAgentCode = '"+tGroupAgentCode+"' ";
		        	 String tHaveAgentCode = new ExeSQL().getOneValue(tHSQL);
		        	 if("".equals(tHaveAgentCode)){
		        		 if("2".equals(tGroupAgentCode.substring(0, 1))){
		        			 String tTySQL = "update laagent set GroupAgentCode = '"+tGroupAgentCode+"' where agentcode = '"+mLAAgentSchema.getAgentCode()+"'";
				        	 new ExeSQL().execUpdateSQL(tTySQL);
		        		 }else{
		        			 String tErrorString = "业务员内部工号："+mLAAgentSchema.getAgentCode()+"，获取的新工号"+tGroupAgentCode+"，不是健康险工号。";
		        			 System.out.println(tErrorString);
			        		 this.mErrors.addOneError(tError);
		        		 }
			        	 continue;
		        	 }else{
		        		 String tErrorString = "业务员内部工号："+mLAAgentSchema.getAgentCode()+"，获取的新工号"+tGroupAgentCode+"，已被"+tHaveAgentCode+"使用。";
		        		 System.out.println(tErrorString);
		        		 this.mErrors.addOneError(tError);
		        		 continue;
		        	 }
		        	 
		         }else{
		        	 this.mErrors.addOneError(tError);
					 continue;
		         }
			}
			map.put(mLAAgentSchema, "UPDATE"); 
		}
		return true;
	}
}
