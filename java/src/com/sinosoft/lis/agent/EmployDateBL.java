/*
 * <p>ClassName: EmployDateBL </p>
 * <p>Description: EmployDateBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2017-11-30
 */
package com.sinosoft.lis.agent;


import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Company: Sinosoft</p>
 * @author yangjian
 * @version 1.0
 * 
 *
 */
public class EmployDateBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 全局变量 */
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAAgentSet mLAAgentSet = new LAAgentSet();     
    public EmployDateBL()
    {
    }
    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
	    //得到外部传入的数据,将数据备份到本类中
	    if (!getInputData(cInputData))
	    {
	        return false;
	    }
	    //进行业务处理
	    if (!dealData())
	    {
	        // @@错误处理
	        CError tError = new CError();
	        tError.moduleName = "EmployDateBL";
	        tError.functionName = "DealData";
	        tError.errorMessage = "数据处理失败EmployDateBL-->dealData!";
	        this.mErrors.addOneError(tError);
	        return false;
	    }
	    try {
	    	PubSubmit tPubSubmit = new PubSubmit();
	    	if(!tPubSubmit.submitData(this.mInputData, "UPDATE")){
	    		CError tError = new CError();
	    		tError.moduleName = "OutWorkDateBL";
	 	        tError.functionName = "submitData";
	    		tError.errorMessage = "数据更新失败";
	    		this.mErrors.addOneError(tError);
		        return false;
	    	}
			
		} catch (Exception e) {
			CError tError = new CError();
	        tError.moduleName = "EmployDateBL";
	        tError.functionName = "submitData";
	        tError.errorMessage = "数据处理失败EmployDateBL-->dealData!";
	        this.mErrors.addOneError(tError);
	        return false;
		}
	    return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {   
    	MMap tMMap= new MMap();
        //修改基础数据
        for (int i = 1; i <= this.mLAAgentSet.size(); i++)
        {   
        	mLAAgentSchema = mLAAgentSet.get(i);
        	String tAgentCode = mLAAgentSchema.getAgentCode();
        	String tEmployDate = mLAAgentSchema.getEmployDate();
        	String tBranchtype=this.mLAAgentSchema.getBranchType();
        	String tBranchtype2=this.mLAAgentSchema.getBranchType2();
        	String tUpSQL ="";
        	if(tBranchtype.equals("1")&&tBranchtype2.equals("01")){
        		/**LAAgent人员基础信息表中EmployDate、Operator、ModifyDate、ModifyTime字段*/
            	tUpSQL="update laagent set employdate='"+tEmployDate+"',operator='zyy',modifydate=current date,modifytime=current time where agentcode='"
            	      +tAgentCode+"'";
            	tMMap.put(tUpSQL, "UPDATE");
            	/**LATree职级表中IntroCommStart、AstartDate、StartDate、Operator、ModifyDate、ModifyTime字段*/
            	tUpSQL="update latree set IntroCommStart='"+tEmployDate+"',startdate='"+tEmployDate+"',astartdate='"+tEmployDate+"',operator='zyy',modifydate=current date,modifytime=current time where agentcode='"
      	              +tAgentCode+"'";
            	tMMap.put(tUpSQL, "UPDATE");
            	/**LARecomRelation推荐关系表中StartDate、Operator、ModifyDate、ModifyTime字段*/
            	tUpSQL="update LARecomRelation set startdate='"+tEmployDate+"',operator='zyy',modifydate=current date,modifytime=current time where agentcode='"
      	        +tAgentCode+"'";
      	        tMMap.put(tUpSQL, "UPDATE");
            	/**LARearRelation推荐关系表中startDate、Operator、ModifyDate、ModifyTime字段*/
      	       tUpSQL="update LARearRelation set startdate='"+tEmployDate+"',operator='zyy',modifydate=current date,modifytime=current time where agentcode='"
    	        +tAgentCode+"'";
    	        tMMap.put(tUpSQL, "UPDATE");
        	}
        	else{
        		/**LAAgent人员基础信息表中EmployDate、Operator、ModifyDate、ModifyTime字段*/
            	tUpSQL="update laagent set employdate='"+tEmployDate+"',operator='zyy',modifydate=current date,modifytime=current time where agentcode='"
            	      +tAgentCode+"'";
            	tMMap.put(tUpSQL, "UPDATE");
            	/**LATree职级表中IntroCommStart、AstartDate、StartDate、Operator、ModifyDate、ModifyTime字段*/
            	tUpSQL="update latree set IntroCommStart='"+tEmployDate+"',startdate='"+tEmployDate+"',astartdate='"+tEmployDate+"',operator='zyy',modifydate=current date,modifytime=current time where agentcode='"
      	              +tAgentCode+"'";
            	tMMap.put(tUpSQL, "UPDATE");	
        	}
        }
        this.mInputData.add(tMMap);
        return true;
    } 
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAAgentSet.set((LAAgentSet)cInputData.getObjectByObjectName("LAAgentSet", 0));
        this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput", 0));
        if (this.mGlobalInput == null)
        {
            CError tError = new CError();
            tError.moduleName = "EmployDateBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "mGlobalInput为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (null == this.mLAAgentSet||0==this.mLAAgentSet.size())
        {
            CError tError = new CError();
            tError.moduleName = "EmployDateBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "mLAAgentSchema为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

  
    public VData getResult()
    {
        return this.mResult;
    }
}

