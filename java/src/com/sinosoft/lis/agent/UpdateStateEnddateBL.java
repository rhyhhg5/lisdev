/*
 * <p>ClassName: LABranchGroupBuildBL </p>
 * <p>Description: LABranchGroupBuildBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2013-05-17
 */
package com.sinosoft.lis.agent;

import java.util.Date;

import utils.system;

import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LAWageHistoryDB;
import com.sinosoft.lis.db.LAWageLogDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LAWageHistorySchema;
import com.sinosoft.lis.schema.LAWageLogSchema;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LAWageHistorySet;
import com.sinosoft.lis.vschema.LAWageLogSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

import examples.newsgroups;

/**
 * <p>Company: Sinosoft</p>
 * @author yangjian
 * @version 1.0
 * 
 *
 */
public class UpdateStateEnddateBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap mMMap = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 当前日期*/
	private String tCurrentDate = PubFun.getCurrentDate();
	private String tCurrentTime = PubFun.getCurrentTime();
    /*private String mOperate2;*/
    
    /** 全局变量 */
    private LAWageLogSchema mLaWageLogSchema = new LAWageLogSchema();
    private LAWageLogSet mLAWageLogSet = new LAWageLogSet(); 
    private LACommisionSchema mLaCommisionSchema=new LACommisionSchema();
    private LACommisionSet mLaCommisionSet=new LACommisionSet();
    private LAWageHistorySchema mLAWageHistorySchema = new LAWageHistorySchema();
    private LAWageHistorySet mLAWageHistorySet = new LAWageHistorySet(); 
    
    public UpdateStateEnddateBL()
    {
    }

    public static void main(String[] args)
    {
    	
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate /*,String mOperate2*/)
    {
    	this.mOperate = cOperate;
    	
    	System.out.println(this.mOperate+"--------------=====updatestatebl");
	    //得到外部传入的数据,将数据备份到本类中
	    if (!getInputData(cInputData))
	    {
	        return false;
	    }
	    //进行业务处理
	    if (!dealData())
	    {
	        // @@错误处理
	        CError tError = new CError();
	        tError.moduleName = "UpdateStateEnddateBL";
	        tError.functionName = "DealData";
	        tError.errorMessage = "数据处理失败UpdateStateEnddateBL-->dealData!";
	        this.mErrors.addOneError(tError);
	        return false;
	    }
	    try {
	    	PubSubmit tPubSubmit = new PubSubmit();
	    	tPubSubmit.submitData(this.mInputData, "UPDATE");
			
		} catch (Exception e) {
			CError tError = new CError();
	        tError.moduleName = "UpdateStateEnddateBL";
	        tError.functionName = "submitData";
	        tError.errorMessage = "数据处理失败UpdateStateEnddateBL-->dealData!";
	        this.mErrors.addOneError(tError);
	        return false;
		}
	    return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	//修改lawagehistory表状态
    	if (this.mOperate.equals("UPDATEHISTORY")){
    		System.out.println("进入修改lawagehistory方法********************************");
    		LAWageHistorySet tUpdateLAWageHistorySet = new LAWageHistorySet();
            //设置基础数据
            for (int i = 1; i <= this.mLAWageHistorySet.size(); i++)
            {
            	mLAWageHistorySchema = mLAWageHistorySet.get(i);
            	String tManageCom = mLAWageHistorySchema.getManageCom();
            	String tWageNo = mLAWageHistorySchema.getWageNo();
            	String tBranchtype = mLAWageHistorySchema.getBranchType();
            	String tBranchtype2 = mLAWageHistorySchema.getBranchType2();
            	String tNewState = mLAWageHistorySchema.getState();
            	
            	LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();
            	tLAWageHistoryDB.setBranchType(tBranchtype);
            	tLAWageHistoryDB.setBranchType2(tBranchtype2);
            	tLAWageHistoryDB.setManageCom(tManageCom);
            	tLAWageHistoryDB.setWageNo(tWageNo);
            	tLAWageHistoryDB.setAClass("03");
            	LAWageHistorySchema tLAWageHistorySchema = new LAWageHistorySchema();
            	LAWageHistorySet tLAWageHistorySet = new LAWageHistorySet();
            	
            	
            	tLAWageHistorySet = tLAWageHistoryDB.query();
            	if(null == tLAWageHistorySet||0==tLAWageHistorySet.size()){
            		CError tError = new CError();
        	        tError.moduleName = "UpdateStateBL";
        	        tError.functionName = "submitData";
        	        tError.errorMessage = "该数据已不存在!";
        	        this.mErrors.addOneError(tError);
            		return false;
            	}
            	tLAWageHistorySchema = tLAWageHistorySet.get(1);
            	
            	tLAWageHistorySchema.setState(tNewState);
            	tUpdateLAWageHistorySet.add(tLAWageHistorySchema);
            	
            }
            mMMap.put(tUpdateLAWageHistorySet,"UPDATE");
    	}
    	
    	
    	//批量删除lacommision表数据
    	if (this.mOperate.equals("DELETEMORE")){
    		System.out.println("进入批量删除删除删除方法+++++++++++++++++++++++++++++++++");
    		String tManageCom = this.mLaWageLogSchema.getManageCom();
    		String tBranchType = this.mLaWageLogSchema.getBranchType();
    		String tBranchType2 = this.mLaWageLogSchema.getBranchType2();
    		String tWageNo = this.mLaWageLogSchema.getWageNo();
    		String tState = this.mLaWageLogSchema.getState();
    		String tEndDate = this.mLaWageLogSchema.getEndDate();
    		String tSQL="";
    		tSQL="delete from LACommision where managecom like'"+tManageCom+"%' and tmakedate='"+tEndDate
    				+ "' and branchtype='"+tBranchType+"' and branchtype2='"+tBranchType2+"'"   				
    				;
    		System.out.println("批量删除lacommision表记录的SQL: " + tSQL);
    		ExeSQL tExeSQL=new ExeSQL();
    		boolean tValue = tExeSQL.execUpdateSQL(tSQL);	
    		if (!tValue) {
    			if (tExeSQL.mErrors.needDealError()) {
       	         // @@错误处理
       	         this.mErrors.copyAllErrors(tExeSQL.mErrors);
       	         CError tError = new CError();
       	         tError.moduleName = "UpdateStateEnddateBL";
       	         tError.functionName = "returnState";
       	         tError.errorMessage = "执行SQL语句,批量删除lacommision表记录失败!";
       	         this.mErrors.addOneError(tError);
       	         System.out.println("执行SQL语句,批量删除lacommision表表中state记录失败!");
       	         return false;
       	          }
    			 CError tError = new CError();
      	         tError.moduleName = "UpdateStateEnddateBL";
      	         tError.functionName = "returnState";
      	         tError.errorMessage = "执行SQL语句,批量删除lacommision表记录失败!";
      	         this.mErrors.addOneError(tError);
      	         System.out.println("执行SQL语句,批量删除lacommision表表中state记录失败!");
      	         return false;
			}
    		
    		
    	}
    	
    	
    	if (this.mOperate.equals("UPDATEMORE")) {
    		System.out.println("进入批量修改修改修改方法——————————————————————————————————");
    		String tManageCom = this.mLaWageLogSchema.getManageCom();
    		String tBranchType = this.mLaWageLogSchema.getBranchType();
    		String tBranchType2 = this.mLaWageLogSchema.getBranchType2();
    		String tWageNo = this.mLaWageLogSchema.getWageNo();
    		String tState = this.mLaWageLogSchema.getState();
    		String tEndDate = this.mLaWageLogSchema.getEndDate();
    		
    		
    		String tSQL="";
    		tSQL="update LAWageLog set WageNo='"+tWageNo+"',BranchType='"+tBranchType+"',BranchType2='"
    				+ tBranchType2 +"',EndDate='"+tEndDate+"',State='"+tState+"',ModifyDate='"
    				+ tCurrentDate +"',ModifyTime='"+tCurrentTime +"',Operator='it001'"+" where ManageCom like '"
    				+ tManageCom +"%' and WageNo='" +tWageNo +"' and BranchType='"+tBranchType 
    				+ "' and BranchType2='"+tBranchType2+"'";
    		System.out.println("批量更新lawagelog表中state记录的SQL: " + tSQL);
    		ExeSQL tExeSQL=new ExeSQL();
    		boolean tValue = tExeSQL.execUpdateSQL(tSQL);
    		if(!tValue){
    		if (tExeSQL.mErrors.needDealError()) {
    	         // @@错误处理
    	         this.mErrors.copyAllErrors(tExeSQL.mErrors);
    	         CError tError = new CError();
    	         tError.moduleName = "UpdateStateEnddateBL";
    	         tError.functionName = "returnState";
    	         tError.errorMessage = "执行SQL语句,更新lawagelog表记录失败!";
    	         this.mErrors.addOneError(tError);
    	         System.out.println("执行SQL语句,更新lawagelog表表中state记录失败!");
    	         return false;
    	          }
    		 CError tError = new CError();
	         tError.moduleName = "UpdateStateEnddateBL";
	         tError.functionName = "returnState";
	         tError.errorMessage = "执行SQL语句,更新lawagelog表记录失败!";
	         this.mErrors.addOneError(tError);
	         System.out.println("执行SQL语句,更新lawagelog表表中state记录失败!");
	         return false;
    		
    	}
		}
    	
    	
    	if(this.mOperate.equals("UPDATE")){
    	LAWageLogSet tUpdateLAWageLogSet = new LAWageLogSet();
        //设置基础数据
        for (int i = 1; i <= this.mLAWageLogSet.size(); i++)
        {
        	mLaWageLogSchema = mLAWageLogSet.get(i);
        	
        	String tManageCom = mLaWageLogSchema.getManageCom();
        	
        	
        	String tWageNo = mLaWageLogSchema.getWageNo();
        	String tBranchtype = mLaWageLogSchema.getBranchType();
        	String tBranchtype2 = mLaWageLogSchema.getBranchType2();
        	String tNewState = mLaWageLogSchema.getState();
        	
        	String tNewEnddate=mLaWageLogSchema.getEndDate();

        	
        	LAWageLogDB tLaWageLogDB = new LAWageLogDB();
        	tLaWageLogDB.setBranchType(tBranchtype);
        	tLaWageLogDB.setBranchType2(tBranchtype2);
        	tLaWageLogDB.setManageCom(tManageCom);
        	tLaWageLogDB.setWageNo(tWageNo);
        	//tLaWageLogDB.setAClass("03");
        	
        	//tLaWageLogDB.setEndDate(date);;
        	//tLaWageLogDB.setEndDate(tNewEnddate);
        	
        	LAWageLogSchema tLAWageLogSchema = new LAWageLogSchema();
        	LAWageLogSet tLAWageLogSet = new LAWageLogSet();
        	
        	
        	tLAWageLogSet = tLaWageLogDB.query();
        	if(null == tLAWageLogSet||0==tLAWageLogSet.size()){
        		CError tError = new CError();
    	        tError.moduleName = "UpdateStateEnddateBL";
    	        tError.functionName = "submitData";
    	        tError.errorMessage = "该数据已不存在!";
    	        this.mErrors.addOneError(tError);
        		return false;
       	}
        	tLAWageLogSchema = tLAWageLogSet.get(1);
        	
        	tLAWageLogSchema.setState(tNewState);
        	tLAWageLogSchema.setEndDate(tNewEnddate);//字符串格式
        	//tLAWageLogSchema.setEndDate(tNewEnddate);//日期格式
        	tLAWageLogSchema.setModifyDate(tCurrentDate);
        	tLAWageLogSchema.setModifyTime(tCurrentTime);
        	tLAWageLogSchema.setOperator("it001");
        	
        	tUpdateLAWageLogSet.add(tLAWageLogSchema);
        	
        }
    	
        mMMap.put(tUpdateLAWageLogSet,"UPDATE");
    	}//结束
    	//进行删除操作
    	if(this.mOperate.equals("DELETE")){
    		
    		System.out.println("进入删除方法了111");
    		LACommisionSet tDeleteCommisionSet=new LACommisionSet();
    		for (int i = 1; i <= this.mLaCommisionSet.size(); i++)
            {
            	mLaCommisionSchema = mLaCommisionSet.get(i);
            	String tManageCom = mLaCommisionSchema.getManageCom();
            	//String tWageNo = mLaWageLogSchema.getWageNo();
            	String tBranchtype = mLaCommisionSchema.getBranchType();
            	String tBranchtype2 = mLaCommisionSchema.getBranchType2();
            	//String tNewState = mLaWageLogSchema.getState();
            	
            	String ttmakedate=mLaCommisionSchema.getTMakeDate();
//            	String ttNewEnddateate= tNewEnddate.replace('-', '/');
//            	Date date= new Date(ttNewEnddateate);
//            	System.out.println("------------"+tManageCom);
//            	System.out.println(tBranchtype);
//            	System.out.println(tNewState);
//            	System.out.println("------------"+tNewEnddate);
//            	System.out.println(ttNewEnddateate);
            	
            	LACommisionDB tLaCommisionDB = new LACommisionDB();      //有两个db需复核
            	tLaCommisionDB.setBranchType(tBranchtype);
            	tLaCommisionDB.setBranchType2(tBranchtype2);
            	tLaCommisionDB.setManageCom(tManageCom);
            	tLaCommisionDB.setTMakeDate(ttmakedate);
            	System.out.println(ttmakedate);
            	System.out.println("BL删除机构"+tManageCom);
            	//tLaWageLogDB.setWageNo(tWageNo);
            	//tLaWageLogDB.setAClass("03");
            	
            	//tLaWageLogDB.setEndDate(date);;
            	//tLaWageLogDB.setEndDate(tNewEnddate);
            	
            	LACommisionSchema tLaCommisionSchema = new LACommisionSchema();
            	LACommisionSet tLaCommisionSet= new LACommisionSet();
            	
            	
            	tLaCommisionSet = tLaCommisionDB.query();
            	/*if(null == tLaCommisionSet||0==tLaCommisionSet.size()){
            		CError tError = new CError();
        	        tError.moduleName = "UpdateStateEnddateBL";
        	        tError.functionName = "submitData";
        	        tError.errorMessage = "该数据已不存在!";
        	        this.mErrors.addOneError(tError);
            		return false;
            	}*/
            	for (int j = 1; j < tLaCommisionSet.size()+1; j++) {
            		tLaCommisionSchema = tLaCommisionSet.get(j);
                	
                	tLaCommisionSchema.setManageCom(tManageCom);
                	tLaCommisionSchema.setBranchType(tBranchtype);//字符串格式
                	tLaCommisionSchema.setBranchType2(tBranchtype2);
                	tLaCommisionSchema.setTMakeDate(ttmakedate);
                	
                	tDeleteCommisionSet.add(tLaCommisionSchema);	
				}
            	
            }
            	mMMap.put(tDeleteCommisionSet, "DELETE");
    	}
        this.mInputData.add(mMMap);
        return true;
    	
    	
    } 
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAWageLogSet.set((LAWageLogSet) cInputData.getObjectByObjectName("LAWageLogSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        this.mLaCommisionSet.set((LACommisionSet) cInputData.getObjectByObjectName("LACommisionSet", 0));
        this.mLaWageLogSchema.setSchema((LAWageLogSchema) cInputData.getObjectByObjectName("LAWageLogSchema", 0));
        this.mLAWageHistorySet.set((LAWageHistorySet) cInputData.getObjectByObjectName("LAWageHistorySet", 0));
        
        /*if (null == this.mLaWageLogSchema)
        {
            CError tError = new CError();
            tError.moduleName = "UpdateStateEnddateBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "mLaWageLogSchema为空！";
            this.mErrors.addOneError(tError);
            return false;
        }*/
        
        /*if (null == this.mLaCommisionSet||0==this.mLaCommisionSet.size())
        {
            CError tError = new CError();
            tError.moduleName = "UpdateStateEnddateBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "mLaCommisionSet为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (this.mGlobalInput == null)
        {
            CError tError = new CError();
            tError.moduleName = "UpdateStateEnddateBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "mGlobalInput为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (null == this.mLAWageLogSet||0==this.mLAWageLogSet.size())
        {
            CError tError = new CError();
            tError.moduleName = "UpdateStateEnddateBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "mLAWageLogSchema为空！";
            this.mErrors.addOneError(tError);
            return false;
        }*/
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
        	mInputData.clear();
        	mInputData.add(mMMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UpdateStateBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
  
    public VData getResult()
    {
        return this.mResult;
    }
}

