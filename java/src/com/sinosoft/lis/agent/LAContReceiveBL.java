/**
 * 交单录入
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: </p>
 * @author  xiongxin
 * @version 1.0
 */

package com.sinosoft.lis.agent;

import java.sql.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.Reflections;


public class LAContReceiveBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String InputDate = PubFun.getCurrentDate();
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String mMakeContNum;

    //录入
    private LAMakeContSchema mLAContReceiveSchema = new LAMakeContSchema();
    private LAMakeContSchema mupLAContReceiveSchema = new LAMakeContSchema();
    private MMap mMap = new MMap();
    //////////////////////////////////////////////

    public LAContReceiveBL() {
    }


/////////////////////////////////////////////
    ///000
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAContReceiveBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAContReceiveBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("Start LAContReceiveBL Submit...");
            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAContReceiveBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    ////////////////////////////////////////////////
    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        this.mLAContReceiveSchema.setSchema((LAMakeContSchema) cInputData.
                                            getObjectByObjectName(
                "LAMakeContSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        //     this.mContType=(String)cInputData.getObjectByObjectName("String",0);

        return true;

    }

////////////////////////////////////////////////////////
    private boolean dealData() {
        mMakeContNum = PubFun1.CreateMaxNo("MakeContNo", 12);
        if (mOperate.equals("INSERT||MAIN")) {

            LAMakeContSet tLAMakeContSet = new LAMakeContSet();
            LAMakeContDB tLAMakeContDB = new LAMakeContDB();
            tLAMakeContDB.setProposalContNo(mLAContReceiveSchema.
                                            getProposalContNo());
            tLAMakeContSet = tLAMakeContDB.query();
            if (tLAMakeContSet != null && tLAMakeContSet.size() > 0) {
                this.mErrors.copyAllErrors(tLAMakeContDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAContReceiveBL";
                tError.functionName = "dealData";
                tError.errorMessage = "投保单号" +
                                      mLAContReceiveSchema.getProposalContNo() +
                                      "信息已经存在，不能再次保存！";
                this.mErrors.addOneError(tError);
                return false;
            }
            mLAContReceiveSchema.setMakeContNo(mMakeContNum);
            mLAContReceiveSchema.setRiskCode("0000");
            mLAContReceiveSchema.setOperator(mGlobalInput.Operator);
            mLAContReceiveSchema.setInputDate(InputDate);
            mLAContReceiveSchema.setMakeDate(CurrentDate);
            mLAContReceiveSchema.setMakeTime(CurrentTime);
            mLAContReceiveSchema.setModifyDate(CurrentDate);
            mLAContReceiveSchema.setModifyTime(CurrentTime);
            mMap.put(this.mLAContReceiveSchema, "INSERT");
            System.out.println("yijingzhixing");
        } else if (mOperate.equals("DELETE||MAIN")) {

            LAMakeContSet tLAMakeContSet = new LAMakeContSet();
            LAMakeContDB tLAMakeContDB = new LAMakeContDB();
            tLAMakeContDB.setProposalContNo(mLAContReceiveSchema.
                                            getProposalContNo());
            tLAMakeContSet = tLAMakeContDB.query();
            if (tLAMakeContSet == null || tLAMakeContSet.size() == 0) {
                this.mErrors.copyAllErrors(tLAMakeContDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAContReceiveBL";
                tError.functionName = "dealData";
                System.out.println("yijing111111111");
                tError.errorMessage = "投保单号" +
                                      mLAContReceiveSchema.getProposalContNo() +
                                      "信息不存在，无法删除！";
                this.mErrors.addOneError(tError);
                return false;
            } else {

                mMap.put(tLAMakeContSet, "DELETE");
                System.out.println("yijing");
            }
        }

        else if (mOperate.equals("UPDATE||MAIN")) {
            //LAMakeContSet tupLAMakeContSet = new LAMakeContSet();
            LAMakeContDB tLAMakeContDB = new LAMakeContDB();
            tLAMakeContDB.setMakeContNo(mLAContReceiveSchema.getMakeContNo());
            if (!tLAMakeContDB.getInfo()) {
                this.mErrors.copyAllErrors(tLAMakeContDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "tLAMakeContBL";
                tError.functionName = "dealData";
                tError.errorMessage = "投保单" +
                                      mLAContReceiveSchema.getMakeContNo() +
                                      "出错，需查询信息后修改！";
                this.mErrors.addOneError(tError);
                return false;
            }
            mupLAContReceiveSchema = tLAMakeContDB.getSchema();
            LAMakeContBSchema mLAContReceiveBSchema = new LAMakeContBSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(mLAContReceiveBSchema, mupLAContReceiveSchema);
            mupLAContReceiveSchema.setProposalContNo(mLAContReceiveSchema.getProposalContNo());
            //mupLAContReceiveSchema.setAgentCode(mLAContReceiveSchema.getAgentCode());
           // mupLAContReceiveSchema.setManageCom(mLAContReceiveSchema.getManageCom());
            //mupLAContReceiveSchema.setBranchAttr(mLAContReceiveSchema.getBranchAttr());
            mupLAContReceiveSchema.setAppntName(mLAContReceiveSchema.getAppntName());
            mupLAContReceiveSchema.setPrem(mLAContReceiveSchema.getPrem());
            //mupLAContReceiveSchema.setBranchSeries(mLAContReceiveSchema.getBranchAttr());

            mupLAContReceiveSchema.setModifyDate(CurrentDate);
            mupLAContReceiveSchema.setModifyTime(CurrentTime);
            //tupLAMakeContSet.add(mupLAContReceiveSchema);


           // tReflections.transFields(mLAContReceiveBSchema, mupLAContReceiveSchema);
            mMap.put(this.mupLAContReceiveSchema, "UPDATE");

             }
            return true;

    }
////////////////////////////////////////////////////////

        private boolean submitquery() {
            return true;
        }
////////////////////////////////////////////////////////

        /**
         * 准备后台的数据
         * @return boolean
         */
        private boolean prepareOutputData() {
            try {
                mInputData = new VData();
                this.mInputData.add(mMap);
            } catch (Exception ex) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAContReceiveBL";
                tError.functionName = "prepareData";
                tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
                this.mErrors.addOneError(tError);
                return false;
            }
            return true;
        }

//////////////////////////////////////////////

        public VData getResult() {
            this.mResult.clear();
            return mResult;
        }

        public static void main(String[] args) {
            LAContReceiveBL tLAContReceiveBL = new LAContReceiveBL();
            LAMakeContSchema mLAContReceiveSchema = new LAMakeContSchema();
            //mLAContReceiveSchema.setMakeContNo("000000000009");
            mLAContReceiveSchema.setAgentCode("1101000001");
            mLAContReceiveSchema.setProposalContNo("456");
            mLAContReceiveSchema.setInputDate("2007-7-1");

            mLAContReceiveSchema.setManageCom("86110000");
            mLAContReceiveSchema.setBranchAttr("000000000127");
            mLAContReceiveSchema.setRiskCode("1102");
            mLAContReceiveSchema.setAppntName("xxin");
            mLAContReceiveSchema.setPrem("1300.00");

            VData tVData = new VData();
            tVData.add(mLAContReceiveSchema);
            GlobalInput tG = new GlobalInput();
            tG.Operator = "001";
            tG.ManageCom = "86";
            tVData.addElement(tG);
            System.out.println("111");
            if (tLAContReceiveBL.submitData(tVData, "INSERT||MAIN")) {

                System.out.println("right");
            } else
                System.out.println("error");
        }


}
