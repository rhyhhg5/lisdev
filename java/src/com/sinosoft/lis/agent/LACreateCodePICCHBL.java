 package com.sinosoft.lis.agent;

import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public class LACreateCodePICCHBL implements LACreateCodeInterface
{
    String mOperate = "";
    String mAgentCode = "";
    String mManagecom = "";
    String mEdorNo = "";
    String mBranchType = "";
    String mBranchType2= "";

    public void setOperate(String cOperate)
    {
        mOperate = cOperate.trim();
    }

    public void setManagecom(String cManagecom)
    {
        mManagecom = cManagecom.trim();
    }

    public String getAgentCode()
    {
        return mAgentCode;
    }

    public String getEdorNo()
    {
        return mEdorNo;
    }

    public void setBranchType(String cBranchType)
    {
        mBranchType = cBranchType.trim();
    }

    public void setBranchType2(String cBranchType2)
    {
        mBranchType2 = cBranchType2.trim();
    }


    public LACreateCodePICCHBL()
    {
    }


    public static void main(String[] args)
    {
        LACreateCodePICCHBL LACreateCodePICCHBL = new LACreateCodePICCHBL();
    }

    public boolean dealdata()
    {
        if (mOperate.equals("INSERT||MAIN"))
        {
            if (mManagecom.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "LACreateCodePICCHBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "没有管理机构！";
                this.mErrors.addOneError(tError);
            }
            String tPrefix = "";
            String Zero = "00000000000000000000";
            if (mManagecom.length() < 4)

            {
                CError tError = new CError();
                               tError.moduleName = "LACreateCodePICCHBL";
                               tError.functionName = "dealdata";
                               tError.errorMessage = "管理机构长度不足！";
                                this.mErrors.addOneError(tError);
                                return false;


            }

            else
            {
                tPrefix = mManagecom.substring(2, 4);
            }
             if (mBranchType == null || mBranchType.equals(""))
             {
                              CError tError = new CError();
                              tError.moduleName = "LACreateCodePICCHBL";
                              tError.functionName = "dealdata";
                              tError.errorMessage = "展业类型不能为空！";
                             this.mErrors.addOneError(tError);
                             return false;

             }
             if (mBranchType2 == null || mBranchType2.equals(""))
             {
                              CError tError = new CError();
                              tError.moduleName = "LACreateCodePICCHBL";
                              tError.functionName = "dealdata";
                              tError.errorMessage = "渠道类型不能为空！";
                             this.mErrors.addOneError(tError);
                             return false;

             }

           // tPrefix=tPrefix+"0"+mBranchType.trim();
            if (mBranchType.equals("1")&&mBranchType2.equals("01") )
            {
                tPrefix=tPrefix+"01";
            }
            if(mBranchType.equals("2")&&mBranchType2.equals("01")  )
            {
                tPrefix=tPrefix+"02";
            }
            if(mBranchType.equals("2")&&mBranchType2.equals("02")  )
            {
                tPrefix=tPrefix+"03";
            }

            if(mBranchType.equals("3")&&mBranchType2.equals("01")  )
            {
                tPrefix=tPrefix+"04";
            }
            if(mBranchType.equals("4")&&mBranchType2.equals("01")  )
            {
                tPrefix=tPrefix+"05";
            }
            //财代键渠道
            if(mBranchType.equals("2")&&mBranchType2.equals("04")  )
                       {
                           tPrefix=tPrefix+"06";
            }
            //寿代键渠道
            if(mBranchType.equals("2")&&mBranchType2.equals("05")  )
                                  {
                                      tPrefix=tPrefix+"07";
            }
            if (mBranchType.equals("1")&&mBranchType2.equals("02") )
            {
                tPrefix=tPrefix+"08";
            }
            if (mBranchType.equals("1")&&mBranchType2.equals("04") )
            {
                tPrefix=tPrefix+"09";
            }
            //modify by wyd 增加个险续收渠道
            if (mBranchType.equals("1")&&mBranchType2.equals("05") )
            {
                tPrefix=tPrefix+"10";
            }
            String tAgentCode = PubFun1.CreateMaxNo("AgentCode" + tPrefix, 6);
            if (tAgentCode == null || tAgentCode.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "LACreateCodePICCHBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "生成代理人代码错误！";
                this.mErrors.addOneError(tError);
                return false;
            }
            mAgentCode = tPrefix + tAgentCode;
            System.out.println("代理人编码：" + mAgentCode);
            String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            if (tEdorNo == null || tEdorNo.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "LACreateCodePICCHBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "生成转储代码错误！";
                return false;
            }
            mEdorNo = tEdorNo;
            System.out.println("转储代码：" + mEdorNo);

        }
        if (this.mOperate.indexOf("UPDATE") != -1 || mBranchType.equals("2"))
        {
            String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            if (tEdorNo == null || tEdorNo.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "LACreateCodePICCHBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "生成转储代码错误！";
                return false;
            }
            mEdorNo = tEdorNo;
            System.out.println("转储代码：" + mEdorNo);
        }
        return true;
    }
}
