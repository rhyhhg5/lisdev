package com.sinosoft.lis.agent;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public interface LATreeInterface
{
    public CErrors mErrors = new CErrors();

    public void setOperate(String cOperate); //设置操作数

    public void setIsManager(String cIsManager); //设置是否是管理员参数

    public void setGlobalInput(GlobalInput cGlobalInput); //设置全局变量

    public void setLAAgentSchema(LAAgentSchema cLAAgentSchema); //设置代理人基本信息

    public void setLATreeSchema(LATreeSchema cLATreeSchema); //设置代理人行政信息

    public void setAgentCode(String cAgentCode); //设置代理人编码

    public void setEdorNo(String cEdorNo); //设置转储编码

    public boolean dealdata(); //处理函数

    public MMap getResult(); //获得处理结果

    public LATreeSchema getLATreeSchema(); //获得Schema
}
