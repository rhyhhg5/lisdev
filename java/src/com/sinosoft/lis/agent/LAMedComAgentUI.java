package com.sinosoft.lis.agent;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAMedComAgentUI
{
    /**������Ϣ����*/
    public CErrors mErrors = new CErrors();

    public LAMedComAgentUI()
    {
        System.out.println("LAMedComAgentUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
    	LAMedComAgentBL bl = new LAMedComAgentBL();
        if(!bl.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    	LAMedComAgentUI tLAMedComAgentUI = new   LAMedComAgentUI();
         System.out.println("LAMedComAgentUI");
    }
}
