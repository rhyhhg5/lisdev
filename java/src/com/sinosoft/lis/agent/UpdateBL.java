/*
 * <p>ClassName: LAassessmainBL </p>
 * <p>Description: LAassessmainBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 
 * @CreateDate：2017-04-01
 */
package com.sinosoft.lis.agent;

import java.math.BigInteger;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

public class  UpdateBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData=new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    String currentDate = PubFun.getCurrentDate();
    String currentTime = PubFun.getCurrentTime();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAAgentSet mLAAgentSet=new LAAgentSet();
    private MMap mMap = new MMap();
    public UpdateBL()
    {}
    public static void main(String[] args)
    {
        VData tVData = new VData();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        LAComSchema tLAComSchema = new LAComSchema();
        tLAComSchema.setAgentCom("18001100001001");
        tLAComSchema.setAreaType("A");
        tLAComSchema.setChannelType("D");
        tLAComSchema.setOperator("001");
        tVData.add(tLAComSchema);
        tVData.add(tG);
        LACombankcodeBL tALAComBL = new  LACombankcodeBL();
        tALAComBL.submitData(tVData, "UPDATE||MAIN");
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if(!checkData())
        {
        	 // @@错误处理
            CError tError = new CError();
           tError.moduleName = "UpdateLaagentBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败UpdateLaagentBL-->checkData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UpdateLaagentBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败UpdateLaagentBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
        	System.out.println("我是001");
        	PubSubmit tPubSubmit = new PubSubmit();
        	System.out.println("我是002");
        	tPubSubmit.submitData(this.mInputData, " ");
        	System.out.println("我是007");
		} catch (Exception e) {
			CError tError = new CError();
            tError.moduleName = "UpdateBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败UpdateBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
		}
        return true;
    }
  /**
   * 进行提交前的数据校验
   */
    private boolean checkData()
    {
    	return true;
    }
    
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        
        System.out.println("我是苹果");
            if (this.mOperate.equals("UPDATE"))
            {  
             for(int i=1;i<=this.mLAAgentSet.size();i++){
            	String tManageCom=mLAAgentSet.get(i).getManageCom();
            	String tagentcode=mLAAgentSet.get(i).getAgentCode();
            	String toutworkdate=mLAAgentSet.get(i).getOutWorkDate();
            	String tbranchtype=mLAAgentSet.get(i).getBranchType();
            	String tbranchtype2=mLAAgentSet.get(i).getBranchType2();
            	System.out.println("修改时从前台获取的managecom为："+tManageCom);
                System.out.println(i+"狗狗:"+tagentcode);
                System.out.println("apple:"+toutworkdate);
                System.out.println("cat:"+tbranchtype);
                System.out.println("小黄人:"+tbranchtype2);
                if(tbranchtype.equals("1")&&tbranchtype2.equals("01") ){
                	String tSQL = "update laagent set outworkdate='"+toutworkdate +"',operator='zyy',modifydate=current date ,modifytime=current time where "
                     + " agentcode = '" +tagentcode +"'";
                	System.out.println("SQL: "+ tSQL );
                	 mMap.put(tSQL,"UPDATE");
                	 String tSQL1="update ladimission set DepartDate='"+toutworkdate+"',operator='zyy',modifydate=current date,modifytime=current time where "
                      +" agentcode='"+tagentcode+"'";
                	 System.out.println("SQL: "+ tSQL1 );
                	 mMap.put(tSQL1,"UPDATE");
                	 String tSQL2="update laorphanpolicy set CalDate='"+toutworkdate+"',operator='zyy',modifydate=current date,modifytime=current time where "
                	 +" agentcode='"+tagentcode+"'";
                	 System.out.println("SQL: "+ tSQL2 );
                	 mMap.put(tSQL2,"UPDATE");
                }else{
                	String tSQL3 = "update laagent set outworkdate='"+toutworkdate +"',operator='zyy',modifydate=current date ,modifytime=current time where "
                    + " agentcode = '" +tagentcode +"'";
                	mMap.put(tSQL3,"UPDATE");
               	    String tSQL4="update ladimission set DepartDate='"+toutworkdate+"',operator='zyy',modifydate=current date,modifytime=current time where "
                     +" agentcode='"+tagentcode+"'";
               	 mMap.put(tSQL4,"UPDATE");
                }
               System.out.println("最后更新离职日期记录的SQL: "  );
            }
       }	
        System.out.println("我是003");
        this.mInputData.add(mMap);
        System.out.println("我是004");
        return true;
    }
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAAgentSet.set((LAAgentSet) cInputData.
                                    getObjectByObjectName("LAAgentSet", 0));
        System.out.println("apple:"+this.mLAAgentSet.size());
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
