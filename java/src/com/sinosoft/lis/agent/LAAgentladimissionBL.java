/*
 * <p>ClassName: ALADimissionBL </p>
 * <p>Description: ALADimissionBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LADimissionDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LADimissionSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.lis.schema.LARecomRelationSchema;
import com.sinosoft.lis.agentbranch.DimChangeAgentToBranch;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.agentbranch.DimChangeRearBranch;
import com.sinosoft.lis.schema.LARearRelationSchema;
import com.sinosoft.lis.schema.LARearRelationBSchema;
import com.sinosoft.lis.vschema.LARearRelationBSet;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LAOrphanPolicySet;
import com.sinosoft.lis.schema.LAOrphanPolicySchema;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.vschema.LCGrpContSet;

public class LAAgentladimissionBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String mAdjustDate = "";
    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LADimissionSchema mLADimissionSchema = new LADimissionSchema();
    private LADimissionSet mLADimissionSet = new LADimissionSet();
    private LABranchGroupSet  mupLABranchGroupSet = new LABranchGroupSet();
    private LABranchGroupSet  mlvLABranchGroupSet = new LABranchGroupSet();
    
    private LARecomRelationSet mLARecomRelationSet = new LARecomRelationSet();
    private LARearRelationSet mLARearRelationSet = new LARearRelationSet();
    private LARearRelationBSet mLARearRelationBSet = new LARearRelationBSet();
    private LABranchGroupBSet  minLABranchGroupBSet= new LABranchGroupBSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAAgentSet mLAAgentSet=new LAAgentSet();
    private LAAgentBSchema mLAAgentBSchema = new LAAgentBSchema();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LATreeBSet mLATreeBSet = new LATreeBSet();
    private LAOrphanPolicySet mLAOrphanPolicySet = new LAOrphanPolicySet();
    private MMap mMap = new MMap();
    private String currentDate =PubFun.getCurrentDate();
    private String currentTime =PubFun.getCurrentTime();
    private String  mEdorNo="";
    public LAAgentladimissionBL()
    {
    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";
        String tsql="select *  from  ladimission where  agentcode='1101000114' ";
        LADimissionSchema tLADimissionSchema = new LADimissionSchema();
        LADimissionSet tLADimissionSet = new LADimissionSet();
        LADimissionDB tLADimissionDB = new LADimissionDB();
        tLADimissionSet=tLADimissionDB.executeQuery(tsql);
        tLADimissionSchema=tLADimissionSet.get(1);
        tLADimissionSchema.setDepartDate("2007-01-01");
        VData tVData = new VData();
        tVData.addElement(tLADimissionSchema);
        tVData.add(tG);

        ALADimissionBL  tALADimissionBL= new ALADimissionBL();
        tALADimissionBL.submitData(tVData,"INSERT||MAIN");
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentladimissionBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAAgentladimissionBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAgentladimissionBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
         }

        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        if (mOperate.equals("update"))
        {   
            System.out.println("我是美女！");
             //操作员
            //在LADimission表里插入离职状态信息  
            LADimissionDB tLADimissionDB = new LADimissionDB();
            String tSQL = "select * from ladimission where agentcode='"+mLAAgentSchema.getAgentCode()+"'";
            mLADimissionSet= tLADimissionDB.executeQuery(tSQL);
            if (!mLADimissionSet.equals("")&&mLADimissionSet!=null&&mLADimissionSet.size()>0)
            {   System.out.println("熊猫:");
              mLADimissionSchema=mLADimissionSet.get(1);
             System.out.println("苹果:"+mLADimissionSchema.getDepartTimes());
               System.out.println("我是可爱的月亮！");
                if (mLADimissionSchema.getDepartTimes() == 1)
                {   System.out.println("大白:");
                    this.mLADimissionSchema.setDepartState("06"); //离职确认
                }
                else
                {   System.out.println("猴子:");
                    this.mLADimissionSchema.setDepartState("07"); //二次离职确认
                }
              
              mLADimissionSchema.setDepartDate(currentDate);
              mLADimissionSchema.setModifyDate(currentDate);
              mLADimissionSchema.setModifyTime(currentTime);
              mLADimissionSchema.setOperator(mGlobalInput.Operator);
              
              //修改代理人信息表中的代理人状态字段和离职日期
              LAAgentDB tLAAgentDB = new LAAgentDB();
              tLAAgentDB.setAgentCode(this.mLAAgentSchema.getAgentCode());
              System.out.println(this.mLAAgentSchema.getAgentCode());
              if (!tLAAgentDB.getInfo())
              {
                  // @@错误处理
                  CError tError = new CError();
                  tError.moduleName = "LAAgentladimissionBL";
                  tError.functionName = "dealData";
                  tError.errorMessage = "查询代理人信息失败!";
                  this.mErrors.addOneError(tError);
                  return false;
              }
              Reflections  tReflections= new Reflections();
              tReflections.transFields(mLAAgentBSchema,tLAAgentDB.getSchema());
              mLAAgentBSchema.setEdorNo(mEdorNo);
              mLAAgentBSchema.setEdorType("08");
              tLAAgentDB.setOutWorkDate(currentDate);
              if (this.mLADimissionSchema.getDepartTimes() == 1)
              {
                  tLAAgentDB.setAgentState("06"); //一次离职确认
              }
              else
              {
                  tLAAgentDB.setAgentState("07"); //二次离职确认  原来状态为07，现在统一变为06状态   update by lh 2009-7-1
              }
              tLAAgentDB.setOperator(this.mGlobalInput.Operator);
              tLAAgentDB.setModifyDate(currentDate);
              tLAAgentDB.setModifyTime(currentTime);
              this.mLAAgentSchema.setSchema(tLAAgentDB);
          
          	  LATreeDB tLATreeDB = new LATreeDB();
              String tAgentCode = this.mLAAgentSchema.getAgentCode(); //离职人员人员
              LATreeSchema tLATreeSchema = new LATreeSchema();
              tLATreeDB.setAgentCode(tAgentCode);
              tLATreeDB.getInfo();
              tLATreeSchema = tLATreeDB.getSchema();
              String tAgentGroup = tLATreeSchema.getAgentGroup(); //离职人员的团队
              String tBranchCode = tLATreeSchema.getBranchCode(); //离职人员的团队
              String tAgentGrade = tLATreeSchema.getAgentGrade();
              String tAgentSeris = AgentPubFun.getAgentSeries(tAgentGrade);
              if (tAgentSeris.equals("1"))
              {
             	 if (!dealBranch()) {
                      buildError("dealWarden", "修改团队为无主管团队时出错!");
                      return false;
                  }
                  LATreeSet tLATreeSet = new LATreeSet();
                  //LATreeDB tLATreeDB = new LATreeDB();
                  //查询团队下除了离职主管人员 的所有人,以便进行人员调动
                  String tSql = "select * from  latree a where upagent='" +
                                tAgentCode
                                + "' and agentcode<>'" + tAgentCode + "'"
                                + "  and  not exists "
                                + " (select b.agentcode from laagent b where a.agentcode=b.agentcode and b.agentstate>='06' ) ";
                  tLATreeSet = tLATreeDB.executeQuery(tSql);
                  //置upagent 为 空
                  if (!dealupAgent(tLATreeSet)) {
                      buildError("dealWarden", "修改团队为无主管团队时出错!");
                      return false;
                  }
              }
              //个险渠道
              if (tAgentSeris.equals("1") && !tAgentGrade.equals("B01")) {
                  //处经理的处理离职,不包括见习经理
                  if (!dealManager(tAgentCode, tAgentGroup)) {
                      return false;
                  }
              } else if (tAgentSeris.equals("2")) {
                  //区经理离职的处理
                  if (!dealWarden(tAgentCode, tAgentGroup, tBranchCode)) {
                      return false;
                  }
              }else if (tAgentSeris.equals("3")) {
                  //区经理离职的处理
                  if (!dealDepartManager(tAgentCode, tAgentGroup, tBranchCode)) {
                      return false;
                  }
              }
              
              //失效此人的直接推荐关系
              if (!invalidRecomRelation(this.mLAAgentSchema.getAgentCode())) {
                  return false;
              }
              //处理育成关系
              if (!invalidRearRelation(this.mLAAgentSchema.getAgentCode())) {
                  return false;
              }
              //将业务员服务的保单记录到孤儿单信息表中
              if (!createOrphanPolicy(this.mLAAgentSchema.getAgentCode())) {
                  return false;
              }
           }else{
          //修改代理人信息表中的代理人状态字段和离职日期
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(this.mLAAgentSchema.getAgentCode());
            System.out.println("科比："+this.mLAAgentSchema.getAgentCode());
            if (!tLAAgentDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAAgentladimissionBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询代理人信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            Reflections  tReflections= new Reflections();
            tReflections.transFields(mLAAgentBSchema,tLAAgentDB.getSchema());
            mLAAgentBSchema.setEdorNo(mEdorNo);
            mLAAgentBSchema.setEdorType("08");
            tLAAgentDB.setOutWorkDate(currentDate);
            String SQL = "select agentstate from laagent where agentcode='"+mLAAgentSchema.getAgentCode()+"'";
           // mLAAgentSet=tLAAgentDB.executeQuery(SQL);
            //mLAAgentSchema=mLAAgentSet.get(1);
            ExeSQL tExeSQL = new ExeSQL();
            String agentstate=tExeSQL.getOneValue(SQL);
            System.out.println("鹿晗007:"+agentstate);
            if (agentstate.equals("01"))
            {
                tLAAgentDB.setAgentState("06"); //一次离职确认
            }
            else
            {
                tLAAgentDB.setAgentState("07"); //二次离职确认  原来状态为07，现在统一变为06状态   update by lh 2009-7-1
            }
            tLAAgentDB.setOperator(this.mGlobalInput.Operator);
            tLAAgentDB.setModifyDate(currentDate);
            tLAAgentDB.setModifyTime(currentTime);
            this.mLAAgentSchema.setSchema(tLAAgentDB);
        
        	LATreeDB tLATreeDB = new LATreeDB();
            String tAgentCode = this.mLAAgentSchema.getAgentCode(); //离职人员人员
            LATreeSchema tLATreeSchema = new LATreeSchema();
            tLATreeDB.setAgentCode(tAgentCode);
            tLATreeDB.getInfo();
            tLATreeSchema = tLATreeDB.getSchema();
            String tAgentGroup = tLATreeSchema.getAgentGroup(); //离职人员的团队
            String tBranchCode = tLATreeSchema.getBranchCode(); //离职人员的团队
            String tAgentGrade = tLATreeSchema.getAgentGrade();
            String tAgentSeris = AgentPubFun.getAgentSeries(tAgentGrade);
            if (tAgentSeris.equals("1"))
            {
           	 if (!dealBranch()) {
                    buildError("dealWarden", "修改团队为无主管团队时出错!");
                    return false;
                }
                LATreeSet tLATreeSet = new LATreeSet();
                //LATreeDB tLATreeDB = new LATreeDB();
                //查询团队下除了离职主管人员 的所有人,以便进行人员调动
                String tSql = "select * from  latree a where upagent='" +
                              tAgentCode
                              + "' and agentcode<>'" + tAgentCode + "'"
                              + "  and  not exists "
                              + " (select b.agentcode from laagent b where a.agentcode=b.agentcode and b.agentstate>='06' ) ";
                tLATreeSet = tLATreeDB.executeQuery(tSql);
                //置upagent 为 空
                if (!dealupAgent(tLATreeSet)) {
                    buildError("dealWarden", "修改团队为无主管团队时出错!");
                    return false;
                }
            }
            //个险渠道
            if (tAgentSeris.equals("1") && !tAgentGrade.equals("B01")) {
                //处经理的处理离职,不包括见习经理
                if (!dealManager(tAgentCode, tAgentGroup)) {
                    return false;
                }
            } else if (tAgentSeris.equals("2")) {
                //区经理离职的处理
                if (!dealWarden(tAgentCode, tAgentGroup, tBranchCode)) {
                    return false;
                }
            }else if (tAgentSeris.equals("3")) {
                //区经理离职的处理
                if (!dealDepartManager(tAgentCode, tAgentGroup, tBranchCode)) {
                    return false;
                }
            }
            
            //失效此人的直接推荐关系
            if (!invalidRecomRelation(this.mLAAgentSchema.getAgentCode())) {
                return false;
            }
            //处理育成关系
            if (!invalidRearRelation(this.mLAAgentSchema.getAgentCode())) {
                return false;
            }
            //将业务员服务的保单记录到孤儿单信息表中
            
             if (!createOrphanPolicy(this.mLAAgentSchema.getAgentCode())) {
                return false;
               }
            
          }
        }
        tReturn = true;
        return tReturn;
    }

    /**
     *  查询该需离职确认的业务员的所有保单，记录成孤儿单
     * @param cAgentCode String
     * @return boolean
     */
    private boolean createOrphanPolicy(String cAgentCode)
    {
        LATreeDB tLATreeDB = new LATreeDB();
        LATreeSchema tLATreeSchema = new LATreeSchema();
        tLATreeDB.setAgentCode(cAgentCode);
        tLATreeDB.getInfo();
        tLATreeSchema = tLATreeDB.getSchema();
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = new LCContSet();
        tLCContDB.setAgentCode(cAgentCode);
        tLCContDB.setGrpContNo("00000000000000000000");
        tLCContSet = tLCContDB.query();
        if (tLCContSet.size()>0)
        {
            for (int i = 1; i <= tLCContSet.size(); i++)
            {
                //判断是否为撤销保单，要是为撤销保单则不生成孤儿单
            	String tSQL = "select '1' from lccont where uwflag<>'a' and contno='"+tLCContSet.get(i).getContNo()+"'";
            	ExeSQL tExeSQL = new ExeSQL();
            	String Flag = tExeSQL.getOneValue(tSQL);
            	if(Flag!=null&&!Flag.equals(""))
            	{
            	LAOrphanPolicySchema tLAOrphanPolicySchema = new LAOrphanPolicySchema();
                tLAOrphanPolicySchema.setContNo(tLCContSet.get(i).getContNo());
                tLAOrphanPolicySchema.setManageCom(tLCContSet.get(i).getManageCom());
                tLAOrphanPolicySchema.setAgentCode(cAgentCode);
                tLAOrphanPolicySchema.setAppntNo(tLCContSet.get(i).getAppntNo());
                tLAOrphanPolicySchema.setReasonType("0");//标记个单
                tLAOrphanPolicySchema.setBranchType(tLATreeSchema.getBranchType());
                tLAOrphanPolicySchema.setBranchType2(tLATreeSchema.getBranchType2());
                tLAOrphanPolicySchema.setAgentGroup(tLATreeSchema.getBranchCode());
                tLAOrphanPolicySchema.setFlag("1");  //正常参与分配
                if(this.mLAAgentSchema.getOutWorkDate().compareTo("2015-04-01")>=1)
        		{
                	tLAOrphanPolicySchema.setCalFlag("N");
                	tLAOrphanPolicySchema.setCalDate(this.mLAAgentSchema.getOutWorkDate());
        		}
                tLAOrphanPolicySchema.setMakeDate(PubFun.getCurrentDate());
                tLAOrphanPolicySchema.setMakeTime(PubFun.getCurrentTime());
                tLAOrphanPolicySchema.setModifyDate(PubFun.getCurrentDate());
                tLAOrphanPolicySchema.setModifyTime(PubFun.getCurrentTime());
                tLAOrphanPolicySchema.setOperator(mGlobalInput.Operator);
                mLAOrphanPolicySet.add(tLAOrphanPolicySchema);
            }
            }
        }
        
        
        
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        tLCGrpContDB.setAgentCode(cAgentCode);
        tLCGrpContSet = tLCGrpContDB.query();
        if (tLCGrpContSet.size()>0)
        {
            for (int i = 1; i <= tLCGrpContSet.size(); i++)
            {
                //判断是否为撤销保单，要是为撤销保单则不生成孤儿单
            	String tSQL = "select '1' from lcgrpcont where uwflag<>'a' and grpcontno='"+tLCGrpContSet.get(i).getGrpContNo()+"'";
            	ExeSQL tExeSQL = new ExeSQL();
            	String Flag = tExeSQL.getOneValue(tSQL);
            	if(Flag!=null&&!Flag.equals(""))
            	{
            	LAOrphanPolicySchema tLAOrphanPolicySchema = new LAOrphanPolicySchema();
                tLAOrphanPolicySchema.setContNo(tLCGrpContSet.get(i).getGrpContNo());
                tLAOrphanPolicySchema.setManageCom(tLCGrpContSet.get(i).getManageCom());
                tLAOrphanPolicySchema.setAgentCode(cAgentCode);
                tLAOrphanPolicySchema.setAppntNo(tLCGrpContSet.get(i).getAppntNo());
                tLAOrphanPolicySchema.setReasonType("1");//标记团单
                tLAOrphanPolicySchema.setBranchType(tLATreeSchema.getBranchType());
                tLAOrphanPolicySchema.setBranchType2(tLATreeSchema.getBranchType2());
                tLAOrphanPolicySchema.setAgentGroup(tLATreeSchema.getBranchCode());
                tLAOrphanPolicySchema.setFlag("1");  //正常参与分配
                if(this.mLAAgentSchema.getOutWorkDate().compareTo("2015-04-01")>=1)
        		{
                	tLAOrphanPolicySchema.setCalFlag("N");
                	tLAOrphanPolicySchema.setCalDate(this.mLAAgentSchema.getOutWorkDate());
        		}
                tLAOrphanPolicySchema.setMakeDate(PubFun.getCurrentDate());
                tLAOrphanPolicySchema.setMakeTime(PubFun.getCurrentTime());
                tLAOrphanPolicySchema.setModifyDate(PubFun.getCurrentDate());
                tLAOrphanPolicySchema.setModifyTime(PubFun.getCurrentTime());
                tLAOrphanPolicySchema.setOperator(mGlobalInput.Operator);
                mLAOrphanPolicySet.add(tLAOrphanPolicySchema);
            }
            }
        }
        return true;
    }

    /*处经理的处理,不包括见习经理
     * 处经理离职后,其下面的人员需要调动到 处经理的直接培养人的团队,如果没有直接培养人,则不作处理,
     * 此团队为无主管团队
    */
    private boolean dealManager(String tAgentCode,String tAgentGroup)
    {
        //查询他的直接培养关系,离职人员为被培养人
        LARearRelationSet tLARearRelationSet = new LARearRelationSet();

        LARearPICCHBL tLARearPICCHBL = new LARearPICCHBL();
        tLARearRelationSet=tLARearPICCHBL.getDirectUpRelation(tAgentCode);
        LATreeSet tLATreeSet = new LATreeSet();
        LATreeDB tLATreeDB = new LATreeDB();
        //查询团队内除了离职人员 的所有人,以便进行人员调动
        String tSql = "select * from  latree a where agentgroup='" +
                      tAgentGroup
                      + "' and agentcode<>'" + tAgentCode + "'"
                      +"  and  not exists "
                      +" (select b.agentcode from laagent b where a.agentcode=b.agentcode and b.agentstate>='06' ) " ;
        tLATreeSet = tLATreeDB.executeQuery(tSql);
        //有被培养人,进行人员调动,否则不处理(为无主管团队)
        if(tLARearRelationSet!=null && tLARearRelationSet.size()>=1)
        {
            //得到育成人所在处
            tLATreeDB = new LATreeDB();
            String tRearAgentCode = tLARearRelationSet.get(1).getRearAgentCode();
            tLATreeDB.setAgentCode(tRearAgentCode);
            tLATreeDB.getInfo();
            LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tLATreeDB.getBranchCode());
            tLABranchGroupDB.getInfo();
            tLABranchGroupSchema = tLABranchGroupDB.getSchema();
            if (tLATreeSet != null && tLATreeSet.size() >= 1) {
                VData tVData = new VData();
                tVData.add(mGlobalInput);
                tVData.addElement(tLABranchGroupSchema);
                tVData.addElement(tLATreeSet);
                tVData.add(mAdjustDate);
                //进行人员调动
                DimChangeAgentToBranch tDimChangeAgentToBranch = new
                        DimChangeAgentToBranch();
                if (!tDimChangeAgentToBranch.submitData(tVData, "INSERT||MAIN"))
                {
                    this.mErrors.copyAllErrors(tDimChangeAgentToBranch.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAAgentladimissionBL";
                    tError.functionName = "dealWarden";
                    tError.errorMessage = "数据提交失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                MMap tMap = new MMap();
                tMap=tDimChangeAgentToBranch.getResult();
                mMap.add(tMap);
            }
            //如果有培养人则,原团队停业
            if(!endBranch())
            {
                buildError("dealWarden","修改团队为无主管团队时出错!");
                return false ;
            }
        }
        return true;
    }
     //区经理的处理
    private boolean dealWarden(String tAgentCode,String tAgentGroup,String tBranchCode)
    {
        //查询他的直接培养关系,离职人员为被培养人
        LARearRelationSet tLARearRelationSet = new LARearRelationSet();

        LARearPICCHBL tLARearPICCHBL = new LARearPICCHBL();
        tLARearRelationSet = tLARearPICCHBL.getDirectUpRelation(tAgentCode);
        //有被培养人,进行人员调动,否则不处理(为无主管团队)
        if (tLARearRelationSet != null && tLARearRelationSet.size() >= 1)
        {
            //得到育成人所在的直辖处
            LATreeDB tLATreeDB = new LATreeDB();
            String tRearAgentCode = tLARearRelationSet.get(1).getRearAgentCode();
            tLATreeDB.setAgentCode(tRearAgentCode);
            tLATreeDB.getInfo();
            String tRearAgentGroup=tLATreeDB.getAgentGroup();//育成区
            String tRearBranchCode=tLATreeDB.getBranchCode();//育成人的直辖处

            LABranchGroupSchema tLABranchGroupSchema = new
                    LABranchGroupSchema();
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tRearBranchCode);
            tLABranchGroupDB.getInfo();
            tLABranchGroupSchema = tLABranchGroupDB.getSchema();

            LATreeSet tLATreeSet = new LATreeSet();
            tLATreeDB = new LATreeDB();
            //--------------------------------
            //直辖团队所有人,进行人员调动
            String tSql = "select * from  latree a where agentgroup='" +
                          tBranchCode + "' and agentcode<>'" + tAgentCode + "'  "
                          +" and not exists "
                          +" (select b.agentcode from laagent b where a.agentcode=b.agentcode and b.agentstate>='06' ) " ;
            tLATreeSet = tLATreeDB.executeQuery(tSql);
            if (tLATreeSet != null && tLATreeSet.size() >= 1)
            {
                VData tVData = new VData();
                tVData.add(mGlobalInput);
                tVData.addElement(tLABranchGroupSchema);
                tVData.addElement(tLATreeSet);
                tVData.add(mAdjustDate);
                //进行人员调动
                DimChangeAgentToBranch tDimChangeAgentToBranch = new
                        DimChangeAgentToBranch();
                if (!tDimChangeAgentToBranch.submitData(tVData, "INSERT||MAIN"))
                {
                    this.mErrors.copyAllErrors(tDimChangeAgentToBranch.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAAgentladimissionBL";
                    tError.functionName = "dealWarden";
                    tError.errorMessage = "数据提交失败!";
                    this.mErrors.addOneError(tError);
                    return false;
                }

                MMap tMap = new MMap();
                tMap = tDimChangeAgentToBranch.getResult();
                mMap.add(tMap);
            }
            //--------------------------------------------
            //把离职人员所管的处(直辖处除外) 进行团队调动
            //得到育成人所在区
            LABranchGroupSchema tAimLABranchGroupSchema = new   LABranchGroupSchema();
            tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tRearAgentGroup);
            tLABranchGroupDB.getInfo();
            tAimLABranchGroupSchema = tLABranchGroupDB.getSchema();
            //得到离职区经理所管处(直辖处除外)
            tLABranchGroupDB = new LABranchGroupDB();
            LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
            tSql = "select * from  labranchgroup where upbranch='" +
                          tAgentGroup + "' and  upbranchattr='0' and ( endflag='N' or endflag is null )";

            tLABranchGroupSet = tLABranchGroupDB.executeQuery(tSql);
            if(tLABranchGroupSet!=null && tLABranchGroupSet.size()>=1)
            {
                DimChangeRearBranch tDimChangeRearBranch = new  DimChangeRearBranch();
                for(int i=1;i<=tLABranchGroupSet.size();i++)
                {
                    LABranchGroupSchema tAdjustLABranchSchema = new   LABranchGroupSchema();
                    tAdjustLABranchSchema=tLABranchGroupSet.get(i);
                    VData tVData = new VData();
                    tVData.add(mGlobalInput);
                    tVData.addElement(tAdjustLABranchSchema);
                    tVData.addElement(tAimLABranchGroupSchema);
                    tVData.add(mAdjustDate);

                    //进行团队调动
                    if (!tDimChangeRearBranch.submitData(tVData, "INSERT||MAIN")) {
                        this.mErrors.copyAllErrors(tDimChangeRearBranch.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "LAAgentladimissionBL";
                        tError.functionName = "dealWarden";
                        tError.errorMessage = "数据提交失败!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    MMap tMap = new MMap();
                    tMap = tDimChangeRearBranch.getResult();
                    mMap.add(tMap);
                }
            }
            //如果有培养人则,停业团队
            if(!endBranch())
            {
                buildError("dealWarden","停业团队时出错!");
                return false ;
            }

        }
        else
        {
            //没有培养人,则置原团队为无主管团队
            if (!dealBranch()) {
                buildError("dealWarden", "修改团队为无主管团队时出错!");
                return false;
            }
            LATreeSet tLATreeSet = new LATreeSet();
            LATreeDB tLATreeDB = new LATreeDB();
            //查询团队下除了离职主管人员 的所有人,以便进行人员调动
            String tSql = "select * from  latree a where upagent='" +
                          tAgentCode
                          + "' and agentcode<>'" + tAgentCode + "'"
                          + "  and  not exists "
                          + " (select b.agentcode from laagent b where a.agentcode=b.agentcode and b.agentstate>='06' ) ";
            tLATreeSet = tLATreeDB.executeQuery(tSql);
            //置upagent 为 空
            if (!dealupAgent(tLATreeSet)) {
                buildError("dealWarden", "修改团队为无主管团队时出错!");
                return false;
            }
        }

        return true ;
    }

    //部经理的处理
    private boolean dealDepartManager(String tAgentCode,String tAgentGroup,String tBranchCode)
    {
            //没有培养人,则置原团队为无主管团队
            if (!dealBranch()) {
                buildError("dealWarden", "修改团队为无主管团队时出错!");
                return false;
            }
            LATreeSet tLATreeSet = new LATreeSet();
            LATreeDB tLATreeDB = new LATreeDB();
            //查询团队下除了离职主管人员 的所有人,以便进行人员调动
            String tSql = "select * from  latree a where upagent='" +
                          tAgentCode
                          + "' and agentcode<>'" + tAgentCode + "'"
                          + "  and  not exists "
                          + " (select b.agentcode from laagent b where a.agentcode=b.agentcode and b.agentstate>='06' ) ";
            tLATreeSet = tLATreeDB.executeQuery(tSql);
            //置upagent 为空
            if (!dealupAgent(tLATreeSet)) {
                buildError("dealWarden", "修改团队为无主管团队时出错!");
                return false;
            }
        return true ;
    }

    //失效推荐关系
    private boolean invalidRecomRelation(String tAgentCode)
    {
        LARecomRelationSet tLARecomRelationSet = new LARecomRelationSet();
        LARecomRelationSet taddLARecomRelationSet = new LARecomRelationSet();
        LARecomPICCHBL tLARecomPICCHBL = new LARecomPICCHBL();
        //得到此人为推荐人的推荐关系
        tLARecomRelationSet=tLARecomPICCHBL.getDownRelation(tAgentCode);
        taddLARecomRelationSet.add(tLARecomRelationSet);

        //得到此人为被推荐人的直接推荐关系
        tLARecomRelationSet = new LARecomRelationSet();
        tLARecomRelationSet=tLARecomPICCHBL.getUpRelation(tAgentCode);
        taddLARecomRelationSet.add(tLARecomRelationSet);
        if(taddLARecomRelationSet==null || taddLARecomRelationSet.size()<=0)
        {
            return true ;//没有直接推荐关系,返回
        }
        for(int i=1;i<=taddLARecomRelationSet.size();i++)
        {
//            LARecomRelationBSchema tLARecomRelationBSchema = new LARecomRelationBSchema();
//            LARecomRelationSchema tLARecomRelationSchema = new LARecomRelationSchema();
//            tLARecomRelationSchema=taddLARecomRelationSet.get(i);
//            //人员离职，需要删除推荐关系表（以后二次入司要重置推荐关系，modify by xiangchun,20070427)
//            mLARecomRelationSet.add(tLARecomRelationSchema);
//            Reflections  tReflections= new Reflections();
//            tReflections.transFields(tLARecomRelationBSchema,tLARecomRelationSchema);
//            tLARecomRelationBSchema.setEdorNo(mEdorNo);
//            tLARecomRelationBSchema.setEdorType("08");
//            mLARecomRelationBSet.add(tLARecomRelationBSchema);

            LARecomRelationSchema tLARecomRelationSchema = new LARecomRelationSchema();
            tLARecomRelationSchema=taddLARecomRelationSet.get(i);
            //如果不是有效的推荐关系(即已经为失效),不需要修改
            System.out.println(tLARecomRelationSchema.getAgentCode());
            System.out.println(tLARecomRelationSchema.getRecomAgentCode());
             System.out.println(tLARecomRelationSchema.getRecomFlag());
             String flag="";

            if(tLARecomRelationSchema.getRecomFlag().equals("1"))
            {
                System.out.println("11111111" + mLARecomRelationSet.size());

                for (int j = 1; j <= mLARecomRelationSet.size(); j++) {

                    String a = mLARecomRelationSet.get(j).getAgentCode();
                    String b = mLARecomRelationSet.get(j).getRecomLevel();
                    int c = mLARecomRelationSet.get(j).getRecomGens();

                    System.out.println(a + b + c);
                    if (a.equals(tLARecomRelationSchema.getAgentCode())
                        && b.equals(tLARecomRelationSchema.getRecomLevel())
                        && (c == (tLARecomRelationSchema.getRecomGens()))) {
                        mLARecomRelationSet.get(j).setRecomFlag("0");
                        mLARecomRelationSet.get(j).setModifyDate(currentDate);
                        mLARecomRelationSet.get(j).setModifyTime(currentTime);
                        flag = "Y";
                        break;

                    }
                }
                /**  String SQL="";
                  SQL="update LARecomRelation set recomflag='0' ,"+
                 "modifydate='" +currentDate+"',modifytime='" +currentTime+"'"
                 +"where agentcode='"+tLARecomRelationSchema.getAgentCode()+"'"
                 +"and  recomlevel='"+tLARecomRelationSchema.getRecomLevel()+"'"
                 +"and  recomgens="+tLARecomRelationSchema.getRecomGens();
                 }  // mMap.put(SQL ,"UPDATE");*/
                if (flag.equals("")) {
                    tLARecomRelationSchema.setRecomFlag("0"); //无效
                    tLARecomRelationSchema.setModifyDate(currentDate);
                    tLARecomRelationSchema.setModifyTime(currentTime);
                    mLARecomRelationSet.add(tLARecomRelationSchema);

                }

            }
        }
        return true;
    }
    //断裂培养关系
    private boolean invalidRearRelation(String tAgentCode)
    {
        LARearRelationSet tLARearRelationSet = new LARearRelationSet();
        LARearPICCHBL tLARearPICCHBL = new LARearPICCHBL();
        //得到此人为培养人的直接培养关系
        tLARearRelationSet=tLARearPICCHBL.getDownRelation(tAgentCode);
        mLARearRelationSet.add(tLARearRelationSet);
        //得到此人为被培养人的直接培养关系
        tLARearRelationSet = new LARearRelationSet();
        tLARearRelationSet=tLARearPICCHBL.getUpRelation(tAgentCode);
        mLARearRelationSet.add(tLARearRelationSet);
        if(mLARearRelationSet==null || mLARearRelationSet.size()<=0)
        {
            return true ;//没有直接培养关系,返回
        }
        for(int i=1;i<=mLARearRelationSet.size();i++)
        {
            LARearRelationSchema tLARearRelationSchema = new
                    LARearRelationSchema();
            tLARearRelationSchema = mLARearRelationSet.get(i);

            LARearRelationBSchema tLARearRelationBSchema = new
                    LARearRelationBSchema();
            Reflections tReflections =new   Reflections();
            tReflections.transFields(tLARearRelationBSchema,tLARearRelationSchema);
            tLARearRelationBSchema.setMakeDate2(tLARearRelationBSchema.getMakeDate());
            tLARearRelationBSchema.setMakeTime2(tLARearRelationBSchema.getModifyTime());
            tLARearRelationBSchema.setModifyDate2(tLARearRelationBSchema.getModifyDate()) ;
            tLARearRelationBSchema.setModifyTime2(tLARearRelationBSchema.getModifyTime());
            tLARearRelationBSchema.setOperator2(tLARearRelationBSchema.getOperator() ) ;

            tLARearRelationBSchema.setMakeDate(currentDate);
            tLARearRelationBSchema.setMakeTime(currentTime);
            tLARearRelationBSchema.setModifyDate(currentDate) ;
            tLARearRelationBSchema.setModifyTime(currentTime);
            tLARearRelationBSchema.setOperator(mGlobalInput.Operator ) ;
            tLARearRelationBSchema.setEdorNo(mEdorNo);
            tLARearRelationBSchema.setEdorType("08");
            mLARearRelationBSet.add(tLARearRelationBSchema) ;
        }
        return true;
    }

    //处理主管所在的团队 (如果是区经理,则还要处理直辖处),使团队为无主管团队
    private boolean dealBranch()
    {
        //主管离职的处理 团队停业
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setBranchManager(this.mLAAgentSchema.getAgentCode());
        System.out.println(this.mLAAgentSchema.getAgentCode());

        tLABranchGroupSet = tLABranchGroupDB.query();
        for (int i = 1; i <= tLABranchGroupSet.size(); i++) {
            LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
            LABranchGroupBSchema tLABranchGroupBSchema = new LABranchGroupBSchema();
            tLABranchGroupSchema = tLABranchGroupSet.get(i);
            //备份labranchgroupb表
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLABranchGroupBSchema, tLABranchGroupSchema);
            System.out.println(tLABranchGroupBSchema.getAgentGroup() + "|||||2");
            tLABranchGroupBSchema.setEdorType("08"); //修改操作
            tLABranchGroupBSchema.setEdorNo(mEdorNo);
            tLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.getMakeDate());
            tLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.getMakeTime());
            tLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.
                                                 getModifyDate());
            tLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.
                                                 getModifyTime());
            tLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.getOperator());
            tLABranchGroupBSchema.setMakeDate(currentDate);
            tLABranchGroupBSchema.setMakeTime(currentTime);
            tLABranchGroupBSchema.setModifyDate(currentDate);
            tLABranchGroupBSchema.setModifyTime(currentTime);
            tLABranchGroupBSchema.setOperator(this.mGlobalInput.Operator);
            minLABranchGroupBSet.add(tLABranchGroupBSchema);
            //修改labranchgroup 中的主管为空
            tLABranchGroupSchema.setBranchManager("");
            tLABranchGroupSchema.setBranchManagerName("");
            tLABranchGroupSchema.setModifyDate(currentDate);
            tLABranchGroupSchema.setModifyTime(currentTime);
            tLABranchGroupSchema.setOperator(mGlobalInput.Operator);
            mupLABranchGroupSet.add(tLABranchGroupSchema);
        }
        return true ;
    }
    //处理主管所在的团队 (如果是区经理,则还要处理直辖处),使团队为无主管团队
    private boolean dealupAgent(LATreeSet tLATreeSet)
    {
        //主管离职的处理 团队停业
        if(tLATreeSet==null || tLATreeSet.size()<=0)
        {
            return true ;
        }
        for (int i=1;i<=tLATreeSet.size();i++)
        {
            LATreeSchema tLATreeSchema= new LATreeSchema();
            tLATreeSchema=tLATreeSet.get(i);
            LATreeBSchema tLATreeBSchema= new LATreeBSchema();
            Reflections tReflections= new Reflections();
            tReflections.transFields(tLATreeBSchema,tLATreeSchema) ;
            tLATreeBSchema.setEdorNO(mEdorNo);
            tLATreeBSchema.setRemoveType("08");
            tLATreeBSchema.setOperator2(tLATreeSchema.getOperator());
            tLATreeBSchema.setMakeDate2(tLATreeBSchema.getMakeDate());
            tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
            tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
            tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
             tLATreeBSchema.setOperator(mGlobalInput.Operator);
            tLATreeBSchema.setMakeDate(currentDate);
            tLATreeBSchema.setMakeTime(currentTime);
            tLATreeBSchema.setModifyDate(currentDate);
            tLATreeBSchema.setModifyTime(currentTime);
            mLATreeBSet.add(tLATreeBSchema);
            tLATreeSchema.setUpAgent("");
            tLATreeSchema.setModifyDate(currentDate);
            tLATreeSchema.setModifyTime(currentTime);
            mLATreeSet.add(tLATreeSchema);
        }

        return true ;
    }
    //处理主管所在的团队 (如果是区经理,则还要处理直辖处),使团队停业
    private boolean endBranch()
    {
        //主管离职的处理  团队停业
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setBranchManager(this.mLADimissionSchema.getAgentCode());
        System.out.println(this.mLADimissionSchema.getAgentCode());

        tLABranchGroupSet = tLABranchGroupDB.query();
        for (int i = 1; i <= tLABranchGroupSet.size(); i++) {
            LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
            LABranchGroupBSchema tLABranchGroupBSchema = new LABranchGroupBSchema();
            tLABranchGroupSchema = tLABranchGroupSet.get(i);
            //备份labranchgroupb表
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLABranchGroupBSchema, tLABranchGroupSchema);
            System.out.println(tLABranchGroupBSchema.getAgentGroup() + "|||||2");
            tLABranchGroupBSchema.setEdorType("07"); //修改操作
            tLABranchGroupBSchema.setEdorNo(mEdorNo);
            tLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.getMakeDate());
            tLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.getMakeTime());
            tLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.
                                                 getModifyDate());
            tLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.
                                                 getModifyTime());
            tLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.getOperator());
            tLABranchGroupBSchema.setMakeDate(currentDate);
            tLABranchGroupBSchema.setMakeTime(currentTime);
            tLABranchGroupBSchema.setModifyDate(currentDate);
            tLABranchGroupBSchema.setModifyTime(currentTime);
            tLABranchGroupBSchema.setOperator(this.mGlobalInput.Operator);
            minLABranchGroupBSet.add(tLABranchGroupBSchema);
            //修改labranchgroup 中的主管为空
            tLABranchGroupSchema.setEndDate(mLADimissionSchema.getDepartDate());
            tLABranchGroupSchema.setEndFlag("Y");
            tLABranchGroupSchema.setModifyDate(currentDate);
            tLABranchGroupSchema.setModifyTime(currentTime);
            tLABranchGroupSchema.setOperator(mGlobalInput.Operator);
            mupLABranchGroupSet.add(tLABranchGroupSchema);
        }
        return true ;
    }
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LAAgentSchema",
                                                  0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        mAdjustDate=currentDate;
        //当月的一日进行人员调动或则团队调动
        String newStartDate = AgentPubFun.formatDate(mAdjustDate, "yyyyMM");
        mAdjustDate = newStartDate+"01" ;
        mAdjustDate=AgentPubFun.formatDate(mAdjustDate, "yyyy-MM-dd");
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start LAAgentladimissionBLQuery Submit...");
        LADimissionDB tLADimissionDB = new LADimissionDB();
        tLADimissionDB.setSchema(this.mLADimissionSchema);
        this.mLADimissionSet = tLADimissionDB.query();
        this.mResult.add(this.mLADimissionSet);
        System.out.println("End LAAgentladimissionBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLADimissionDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLADimissionDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentladimissionBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            if(!mLADimissionSet.equals("")&&mLADimissionSet!=null&&mLADimissionSet.size()>0){
            this.mMap.put(this.mLADimissionSchema,"UPDATE");
            this.mMap.put(this.mLAAgentSchema,"UPDATE");
            this.mMap.put(this.mLAAgentBSchema,"INSERT");
            this.mMap.put(this.mupLABranchGroupSet,"UPDATE");
            this.mMap.put(this.mlvLABranchGroupSet,"UPDATE");
            this.mMap.put(this.minLABranchGroupBSet,"INSERT");
            this.mMap.put(this.mLARecomRelationSet,"UPDATE");
            this.mMap.put(this.mLARearRelationSet,"DELETE");
            this.mMap.put(this.mLARearRelationBSet,"INSERT");
            this.mMap.put(this.mLATreeSet,"UPDATE");
            this.mMap.put(this.mLATreeBSet,"INSERT");
            this.mMap.put(this.mLAOrphanPolicySet,"DELETE&INSERT");
            mInputData.add(mMap);
            }else{
            	this.mMap.put(this.mLAAgentSchema,"UPDATE");
                this.mMap.put(this.mLAAgentBSchema,"INSERT");
                this.mMap.put(this.mupLABranchGroupSet,"UPDATE");
                this.mMap.put(this.mlvLABranchGroupSet,"UPDATE");
                this.mMap.put(this.minLABranchGroupBSet,"INSERT");
                this.mMap.put(this.mLARecomRelationSet,"UPDATE");
                this.mMap.put(this.mLARearRelationSet,"DELETE");
                this.mMap.put(this.mLARearRelationBSet,"INSERT");
                this.mMap.put(this.mLATreeSet,"UPDATE");
                this.mMap.put(this.mLATreeBSet,"INSERT");
                this.mMap.put(this.mLAOrphanPolicySet,"DELETE&INSERT");
                mInputData.add(mMap);
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentladimissionBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    /**
     * 校验函数
     * 对传入的数据进行信息校验
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
//    }
    }
    //错误处理
    private void buildError(String szFunc, String szErrMsg) {
            CError cError = new CError();

            cError.moduleName = "AdjustAgentBL";
            cError.functionName = szFunc;
            cError.errorMessage = szErrMsg;
            this.mErrors.addOneError(cError);
    }
}
