package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgenttempDB;
import com.sinosoft.lis.db.LAQualificationDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentTempBSchema;
import com.sinosoft.lis.schema.LAAgenttempSchema;
import com.sinosoft.lis.schema.LAMakeContBSchema;
import com.sinosoft.lis.schema.LAQualificationBSchema;
import com.sinosoft.lis.schema.LAQualificationSchema;
import com.sinosoft.lis.vschema.LAQualificationSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

/**
 * <p>Title: AgentSystem</p>
 * <p>Description: 销售管理——增员管理</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author xjh
 * @version 1.0
 */

public class LABankComAgentBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private MMap map = new MMap();


    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAgenttempSchema mLAAgenttempSchema = new LAAgenttempSchema();
    private LAQualificationSchema mLAQualificationSchema = new LAQualificationSchema();

    /** 数据操作字符串 */
  private String currentDate = PubFun.getCurrentDate();
  private String currentTime = PubFun.getCurrentTime();

    private String mAgentCode = "";
    private String mEdorNo = "";

    public String getAgentCode() {
        return mAgentCode;
    }

    public LABankComAgentBL() {
    }

    public static void main(String[] args) {
    	LABankComAgentBL tLABankComAgentBL = new LABankComAgentBL();
    }

    /**
      传输数据的公共方法X
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Begin LAMedComAgentBL.submitData.........");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        if(!check())
        {
        	return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start LAMedComAgentBL Submit...");
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAMedComAgentBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量

        System.out.println("Begin LAMedComAgentBL.getInputData.........");
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        this.mLAAgenttempSchema.setSchema((LAAgenttempSchema) cInputData.getObjectByObjectName("LAAgenttempSchema", 0));
        this.mLAQualificationSchema.setSchema((LAQualificationSchema) cInputData.getObjectByObjectName("LAQualificationSchema", 0));
      
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAMedComAgentBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }
    /**
     * 业务处理主函数
     * @return boolean
     */
    public boolean dealData() {
        System.out.println("Begin LAMedComAgentBL.dealData.........");
        /**生成代理编码AgentCode*/
        if(this.mOperate.equals("INSERT||MAIN")){
        	String tTempAgentCode=PubFun1.CreateMaxNo("TEMPAGENT", 8);
        	tTempAgentCode=mLAAgenttempSchema.getEntryNo().substring(0, 2)+tTempAgentCode;
        	this.mLAAgenttempSchema.setAgentCode(tTempAgentCode);
        	this.mLAAgenttempSchema.setAgentState("3");
        	this.mLAAgenttempSchema.setSex("2");
        	this.mLAAgenttempSchema.setBirthday("1900-1-1");
        	this.mLAAgenttempSchema.setOperator(this.mGlobalInput.Operator);
        	this.mLAAgenttempSchema.setMakeDate(this.currentDate);
        	this.mLAAgenttempSchema.setMakeTime(this.currentTime);
        	this.mLAAgenttempSchema.setModifyDate(this.currentDate);
        	this.mLAAgenttempSchema.setModifyTime(this.currentTime);        	
        	this.map.put(this.mLAAgenttempSchema, "INSERT");
        	
        	this.mLAQualificationSchema.setAgentCode(tTempAgentCode);
        	this.mLAQualificationSchema.setMakeDate(currentDate);
            this.mLAQualificationSchema.setMakeTime(currentTime);
            this.mLAQualificationSchema.setModifyDate(currentDate);
            this.mLAQualificationSchema.setModifyTime(currentTime);
            this.mLAQualificationSchema.setOperator(mGlobalInput.Operator);
            this.map.put(this.mLAQualificationSchema, "INSERT");
        	
        }else if(this.mOperate.equals("UPDATE||MAIN")){
        	this.mAgentCode=this.mLAAgenttempSchema.getAgentCode();
        	LAAgenttempDB tLAAgenttempDB=new LAAgenttempDB();
        	tLAAgenttempDB.setAgentCode(this.mLAAgenttempSchema.getAgentCode());
        	if(!tLAAgenttempDB.getInfo()){
        		CError tError = new CError();
                tError.moduleName = "LAMedComAgentBL";
                tError.functionName = "prepareOutputData";
                tError.errorMessage = "获取代理人录入信息失败。";
                this.mErrors.addOneError(tError);
                return false;
        	}
        	LAAgenttempSchema oldmLAAgenttempSchema=new LAAgenttempSchema();
        	oldmLAAgenttempSchema=tLAAgenttempDB.getSchema();        	
        	LAAgentTempBSchema mLAAgenttempBSchema=new LAAgentTempBSchema();        	
            Reflections tReflections = new Reflections();
            tReflections.transFields(mLAAgenttempBSchema, oldmLAAgenttempSchema);
            mLAAgenttempBSchema.setEdorNo(PubFun1.CreateMaxNo("EdorNo", 20));
            mLAAgenttempBSchema.setEdorType("01");
            mLAAgenttempBSchema.setOperator2(this.mGlobalInput.Operator);
            mLAAgenttempBSchema.setMakeDate2(oldmLAAgenttempSchema.getMakeDate());
            mLAAgenttempBSchema.setMakeTime2(oldmLAAgenttempSchema.getMakeTime());
            mLAAgenttempBSchema.setModifyDate2(oldmLAAgenttempSchema.getModifyDate());
            mLAAgenttempBSchema.setModifyTime2(oldmLAAgenttempSchema.getModifyTime());
            mLAAgenttempBSchema.setMakeDate(this.currentDate);
            mLAAgenttempBSchema.setMakeTime(this.currentTime);
            mLAAgenttempBSchema.setModifyDate(this.currentDate);
            mLAAgenttempBSchema.setModifyTime(this.currentTime);
           this.map.put(mLAAgenttempBSchema, "INSERT");
           this.map.put(oldmLAAgenttempSchema, "DELETE");
        	this.mLAAgenttempSchema.setOperator(this.mGlobalInput.Operator);
        	this.mLAAgenttempSchema.setModifyDate(this.currentDate);
        	this.mLAAgenttempSchema.setModifyTime(this.currentTime);
        	this.mLAAgenttempSchema.setMakeDate(this.currentDate); 
        	this.mLAAgenttempSchema.setMakeTime(this.currentTime); 
        	this.map.put(this.mLAAgenttempSchema, "INSERT");
        	
        	String mQualifNo=mLAQualificationSchema.getQualifNo();
        	LAQualificationDB tLAQualificationDB=new LAQualificationDB();
        	LAQualificationSet tLAQualificationSet=new LAQualificationSet();
        	tLAQualificationDB.setAgentCode(this.mLAAgenttempSchema.getAgentCode());
        	tLAQualificationSet=tLAQualificationDB.query();
        	if(tLAQualificationSet.size()>=1){
        	LAQualificationSchema oldmLAQualificationSchema=new LAQualificationSchema();
        	oldmLAQualificationSchema=tLAQualificationSet.get(1);    
        	System.out.println(oldmLAQualificationSchema.getQualifNo());
        	
        	LAQualificationBSchema mLAQualificationBSchema=new LAQualificationBSchema();        	
            Reflections tReflections1 = new Reflections();
            tReflections1.transFields(mLAQualificationBSchema, oldmLAQualificationSchema);
            mLAQualificationBSchema.setEdorNo(PubFun1.CreateMaxNo("EdorNo", 19));
            mLAQualificationBSchema.setOperator1(this.mGlobalInput.Operator);
            mLAQualificationBSchema.setMakeDate1(this.currentDate);
            mLAQualificationBSchema.setMakeTime1(this.currentTime);
            mLAQualificationBSchema.setMakeTime(this.currentTime);
            mLAQualificationBSchema.setMakeDate(this.currentTime);
            this.map.put(mLAQualificationBSchema, "INSERT");  
            this.map.put(oldmLAQualificationSchema, "DELETE"); 
        	}
            this.mLAQualificationSchema.setAgentCode(mLAAgenttempSchema.getAgentCode());
        	this.mLAQualificationSchema.setOperator(this.mGlobalInput.Operator);
        	this.mLAQualificationSchema.setModifyDate(this.currentDate);
        	this.mLAQualificationSchema.setModifyTime(this.currentTime);          	
        	this.map.put(this.mLAQualificationSchema, "INSERT");
        	
        }else if(this.mOperate.equals("DELETE||MAIN")){
        	this.map.put(this.mLAAgenttempSchema, "DELETE");
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            System.out.println("Begin LAMedComAgentBL.prepareOutputData.........");
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAMedComAgentBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean check(){
    	  if(this.mOperate.equals("INSERT||MAIN")){
  		  ExeSQL tExeSQL1=new ExeSQL();
  		  String SQL="select a.qualifno from LAQualification a where a.qualifno = '"+mLAQualificationSchema.getQualifNo()+"' and a.agentcode in (select b.agentcode from laagenttemp b where b.branchtype='"+mLAAgenttempSchema.getBranchType()+"' and b.branchtype2='"+mLAAgenttempSchema.getBranchType2()+"')";
  		  String tqualifno=tExeSQL1.getOneValue(SQL);
  		  if(tqualifno!=null&&!tqualifno.equals(""))
  		  {
  			  CError tError = new CError();
              tError.moduleName = "LABankComAgentBL";
              tError.functionName = "check";
              tError.errorMessage = "资格证书号"+mLAQualificationSchema.getQualifNo()+"已存在未离职";
              this.mErrors.addOneError(tError);
              return false;
  		  }
    	  }

  	  return true;
  }
}


