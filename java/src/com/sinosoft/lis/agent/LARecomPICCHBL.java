package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LARecomRelationDB;
import com.sinosoft.lis.db.LARecomRelationBDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vdb.LARecomRelationBDBSet;
import com.sinosoft.lis.vschema.LARecomRelationBSet;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LARecomRelationBSchema;
import com.sinosoft.lis.schema.LARecomRelationSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.PubFun1;

/**
 * <p>Title: AgentSystem</p>
 * <p>Description: 销售管理，处理推荐关系表LLRecomRelation</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author xjh
 * @version 1.0
 */
public class LARecomPICCHBL implements LARecomInterface {
    MMap map = new MMap();
    String mOperate = "";
    GlobalInput mGlobalInput = new GlobalInput();
    LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    LATreeSchema mLATreeSchema = new LATreeSchema();
    LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();
    LARecomRelationSet mLARecomRelationSet = new LARecomRelationSet();
    LARecomRelationSet mNewAddLARecomRelationSet = new LARecomRelationSet();
    String mEdorNo = "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private Reflections ref = new Reflections();
    private String mMakeContNum="";
    public void setOperate(String cOperate) { //设置操作类型数
        mOperate = cOperate;
    }

    public void setGlobalInput(GlobalInput cGlobalInput) { //设置全局变量
        mGlobalInput.setSchema(cGlobalInput);
    }

    public void setLARecomRelationSet(LARecomRelationSet cLARecomRelationSet) { //设置育成信息
        mLARecomRelationSet.set(cLARecomRelationSet);
    }

    public void setLATreeSchema(LATreeSchema cLATreeSchema) {
        mLATreeSchema.setSchema(cLATreeSchema);
    }

    public void setEdorNo(String cEdorNo) { //设置转储编码
        mEdorNo = cEdorNo;
    }


    public LATreeSchema getLATreeSchema() {
        return null;
    }

    public boolean dealdata(){
        if(1==1){
   // if (this.mOperate.equals("DELETE||INSERT") ){
            //增员管理
       String tAgentCode=""+mLATreeSchema.getAgentCode();
       mMakeContNum = PubFun1.CreateMaxNo("EdorNo", 20);
       LARecomRelationSet tLARecomRelationSet1 = new  LARecomRelationSet();
       LARecomRelationDB tLARecomRelationDB = new  LARecomRelationDB();
       LARecomRelationBSet  tmLARecomRelationBSet = new  LARecomRelationBSet();
       tLARecomRelationDB.setAgentCode(tAgentCode);
       tLARecomRelationSet1 = tLARecomRelationDB.query();
       System.out.println("备份推荐表的代数为："+tLARecomRelationSet1.size());
       if(tLARecomRelationSet1.size()>0)
       {
          System.out.println(tLARecomRelationSet1.size());
               for (int k = 1; k <= tLARecomRelationSet1.size(); k++) {
                   System.out.println("33333333333333");
                   LARecomRelationBSchema  tmLARecomRelationBSchema=new LARecomRelationBSchema();
                   ref.transFields(tmLARecomRelationBSchema,
                                   tLARecomRelationSet1.get(k));
                   tmLARecomRelationBSchema.setEdorNo(mMakeContNum);
                   tmLARecomRelationBSchema.setEdorType("09"); //二次增员更改推荐人
                   tmLARecomRelationBSchema.setOperator2(mGlobalInput.Operator);
                   tmLARecomRelationBSchema.setMakeDate2(currentDate);
                   tmLARecomRelationBSchema.setMakeTime2(currentTime);
                   tmLARecomRelationBSchema.setModifyDate2(currentDate);
                   tmLARecomRelationBSchema.setModifyTime2(currentTime);
                   tmLARecomRelationBSet.add(tmLARecomRelationBSchema);
  //                 System.out.println("1111111111111"+tmLARecomRelationBSet.get(k).getAgentCode());
           }
           this.map.put(tmLARecomRelationBSet, "INSERT");
       }
          if (!tLARecomRelationSet1.mErrors.needDealError()) {

               this.map.put(tLARecomRelationSet1, "DELETE");
           }

            String tRecomAgentCode = ""+mLATreeSchema.getIntroAgency();
            System.out.println("tRecomAgentCode:"+tRecomAgentCode);
            if (tRecomAgentCode.equals("")) {
                System.out.println("没有推荐人");
                return true;
            }
            LARecomRelationSchema tRecomRSchema = new LARecomRelationSchema();
            tRecomRSchema.setAgentCode(mLATreeSchema.getAgentCode());
            tRecomRSchema.setAgentGrade(mLATreeSchema.getAgentGrade());
            tRecomRSchema.setAgentGroup(mLATreeSchema.getAgentGroup());

            //=====startdate======//
            tRecomRSchema.setStartDate(mLATreeSchema.getIntroCommStart());

            tRecomRSchema.setRecomAgentCode(tRecomAgentCode);
            LATreeDB tLATreeDB = new LATreeDB();
            tLATreeDB.setAgentCode(tRecomAgentCode);
            if(!tLATreeDB.getInfo()){
                CError.buildErr(this,"推荐人职级信息查询失败！");
                return false;
            }
            tRecomRSchema.setRecomAgentGrade(tLATreeDB.getAgentGrade());
            ExeSQL texesql = new ExeSQL();
            String tAgentGroup = mLATreeSchema.getAgentGroup();
            String tsql = "select branchlevel from labranchgroup where agentgroup='"+tAgentGroup+"'";
            String tgrade = texesql.getOneValue(tsql);

            tRecomRSchema.setRecomLevel("01");

            return addRecom(tRecomRSchema);
     }
        return true;
    }
    public MMap getResult() {
        return map;
    }

    public LARecomPICCHBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 修改推荐关系
     * @return boolean
     */
    public boolean ModifyRecom() {
        return true;
    }

    /**
     * 基本单元，新增一条直接推荐关系
     * 及相关的间接推荐关系
     * 增员时产生的一定是一个叶子节点
     * @return boolean
     */
    public boolean addRecom(LARecomRelationSchema tRecomRSchema) {
        String tAgentCode = tRecomRSchema.getAgentCode();
        String tAgentGroup = tRecomRSchema.getAgentGroup();
        String tStartDate = tRecomRSchema.getStartDate();
        //按条件复制推荐人的所有推荐关系
        LARecomRelationSet tLARecomRelationSet = new LARecomRelationSet();
        LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();
        tLARecomRelationDB.setAgentCode(tRecomRSchema.getRecomAgentCode());
        tLARecomRelationDB.setRecomFlag("1");//已失效的关系不需要再进行复制
        tLARecomRelationSet = tLARecomRelationDB.query();
        tLARecomRelationSet.add(tRecomRSchema);
        for (int j = 1; j <= tLARecomRelationSet.size(); j++) {
            LARecomRelationSchema tLARecomRelationSchema = tLARecomRelationSet.get(j);
            tLARecomRelationSchema.setRecomGens(tLARecomRelationSchema.
                                                getRecomGens() + 1);
            tLARecomRelationSchema.setAgentCode(tAgentCode);
            tLARecomRelationSchema.setAgentGroup(tAgentGroup);
            tLARecomRelationSchema.setStartDate(tStartDate);
            tLARecomRelationSchema.setRecomFlag("1");
            tLARecomRelationSchema.setRecomComFlag("1");
            tLARecomRelationSchema.setRecomStartYear("1");
            tLARecomRelationSchema.setMakeDate(currentDate);
            tLARecomRelationSchema.setMakeTime(currentTime);
            tLARecomRelationSchema.setModifyDate(currentDate);
            tLARecomRelationSchema.setModifyTime(currentTime);
            tLARecomRelationSchema.setOperator(mGlobalInput.Operator);
        }
        map.put(tLARecomRelationSet, "INSERT");
        return true;
    }

    /**
     * 基本单元，失效一条直接推荐关系（单人离职有向上/向下两条直接推荐关系）
     * 及若干间接推荐关系
     * @return boolean
     */
    public boolean invalidateRecom(boolean Single,String cDate){
        if (mLATreeSchema==null){
            CError.buildErr(this,"没有传入业务人员信息!");
            return false;
        }
        String tAgentCode = mLATreeSchema.getAgentCode();
        String tAgentGroup = ""+mLATreeSchema.getAgentGroup();
        String tAgentGrade = ""+mLATreeSchema.getAgentGrade();
        String ugrouppart = "";
        String ugradepart = "";
        String urgradepart = "";
        if(!tAgentGroup.equals("")&&!tAgentGroup.equals("null"))
            ugrouppart = ",agentgroup='"+tAgentGroup+"' ";
        if(!tAgentGrade.equals("")&&!tAgentGrade.equals("null")){
            ugradepart = ",agentgrade='" + tAgentGrade + "' ";
            urgradepart = ",recomagentgrade='" + tAgentGrade + "' ";
        }
        String upsql = "update larecomrelation set recomflag='0',enddate='"
                      +cDate+"'"+ugrouppart+ugradepart+" where agentcode= '"+tAgentCode+"'";
       System.out.println(upsql);
        map.put(upsql, "UPDATE");
        if(Single){
            //如果单人离职，还需要断向下的推荐关系
            String downsql =
                    "update larecomrelation set recomflag='0',enddate='"
                    + cDate + "'"+urgradepart
                    +" where recomagentcode= '" + tAgentCode +"'";
            System.out.println(downsql);
            map.put(downsql, "UPDATE");
        }else{
            //带走其下级关系后，则其下级与其上级推荐关系断裂
            String indsql = "update larecomrelation set recomflag='0',enddate='"
                            +cDate+"' "+ugrouppart+urgradepart+" where recomagentcode in "
                            +"(select recomagentcode from larecomrelation b "
                            +"where  b.agentcode='"+tAgentCode+"') "
                            +"and agentcode in (select agentcode from larecomrelation b "
                            +"where  b.recomagentcode='"+tAgentCode+"')";
            String groupsql = "update larecomrelation set modifydate='"+PubFun.getCurrentDate()
                              +"'"+ugrouppart
                              +" where recomagentcode= '" + tAgentCode +"' and recomflag='1'";
            System.out.println(indsql);
            map.put(indsql, "UPDATE");
            map.put(groupsql, "UPDATE");
        }

        return true;
    }

    /**
     *
     * @param cLARecomRelationSet LARecomRelationSet
     * @return boolean
     */
    public boolean edorRecom(LARecomRelationSet cLARecomRelationSet) {
        Reflections tReflections;
        LARecomRelationSchema tLARecomRelationSchema;
        LARecomRelationBSchema tLARecomRelationBSchema;
        for (int i = 1; i <= cLARecomRelationSet.size(); i++) {
            tLARecomRelationSchema = new
                                     LARecomRelationSchema();
            tLARecomRelationBSchema = new
                                      LARecomRelationBSchema();
            tLARecomRelationSchema = cLARecomRelationSet.get(i);
            tReflections = new Reflections();
            tReflections.transFields(tLARecomRelationBSchema,
                                     tLARecomRelationSchema);
            tLARecomRelationBSchema.setEdorNo(mEdorNo);
            tLARecomRelationBSchema.setEdorType("06");
            tLARecomRelationBSchema.setMakeDate(currentDate);
            tLARecomRelationBSchema.setMakeTime(currentTime);
            tLARecomRelationBSchema.setModifyDate(currentDate);
            tLARecomRelationBSchema.setModifyTime(currentTime);
            tLARecomRelationBSchema.setOperator(this.mGlobalInput.Operator);
            map.put(tLARecomRelationBSchema, "INSERT");
        }
        return true;
    }
    /**
        * 直接育成关系（得到育成人为 tAgentCode的直接推荐关系）
        * @return LARecomRelationSet
        */
    public LARecomRelationSet getDirectDownRelation(String tAgentCode)
    {
        LARecomRelationSet tgetLARecomRelationSet = new LARecomRelationSet();
        LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();
        //查询本人为育成人的直接推荐关系
        String downsql =
                "select * from lARecomrelation   where recomagentcode= '" +
                tAgentCode + "' and recomgens=1 ";
        tgetLARecomRelationSet = tLARecomRelationDB.executeQuery(downsql);
        return tgetLARecomRelationSet;
    }
    /**
        * 间接育成关系（得到育成人为 tAgentCode的直接推荐关系）
        * @return LARecomRelationSet
    */
    public LARecomRelationSet getinDirectDownRelation(String tAgentCode)
    {
        LARecomRelationSet tgetLARecomRelationSet = new LARecomRelationSet();
        LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();
        //查询本人为被育成人的直接推荐关系
        String downsql =
                "select * from lARecomrelation   where recomagentcode= '" +
                tAgentCode + "' and recomgens>1 ";
        tgetLARecomRelationSet = tLARecomRelationDB.executeQuery(downsql);
        return tgetLARecomRelationSet;
   }

    /**
           * 直接育成关系（得到被育成人为 tAgentCode的直接推荐关系）
           * @return LARecomRelationSet
   */

   public LARecomRelationSet getDirectUpRelation(String tAgentCode)
   {
       LARecomRelationSet tgetLARecomRelationSet = new LARecomRelationSet();
       LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();
       //查询本人为被育成人的直接推荐关系
       String downsql =
               "select * from lARecomrelation   where agentcode= '" +
               tAgentCode + "' and recomgens=1 ";
       tgetLARecomRelationSet = tLARecomRelationDB.executeQuery(downsql);
       return tgetLARecomRelationSet;
   }
   /**
           * 间接育成关系（得到被育成人为 tAgentCode的间接推荐关系）
           * @return LARecomRelationSet
   */

   public LARecomRelationSet getinDirectUpRelation(String tAgentCode)
   {
       LARecomRelationSet tgetLARecomRelationSet = new LARecomRelationSet();
       LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();
       //查询本人为被育成人的间接推荐关系
       String downsql =
               "select * from lARecomrelation   where agentcode= '" +
               tAgentCode + "' and recomgens > 1 ";
       tgetLARecomRelationSet = tLARecomRelationDB.executeQuery(downsql);
       return tgetLARecomRelationSet;
   }
   /**
           * 间接推荐关系（得到 与 tAgentCode有关的间接推荐关系,tAgentCode不是推荐人,也不是被推荐人）
           * @return LARecomRelationSet
   */

   public LARecomRelationSet getAsRelation(String tAgentCode)
   {
       LARecomRelationSet tgetLARecomRelationSet = new LARecomRelationSet();
       LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();
       //查询本人为被育成人的间接推荐关系
       String downsql = "select * from lARecomrelation  where recomagentcode in "
                + "(select recomagentcode from lARecomrelation b "
                + "where  b.agentcode='" + tAgentCode + "') "
                +
                "and agentcode in (select agentcode from lARecomrelation b "
                + "where  b.recomagentcode='" + tAgentCode + "') and recomgens > 1  ";

       tgetLARecomRelationSet = tLARecomRelationDB.executeQuery(downsql);
       return tgetLARecomRelationSet;
   }
   /**
           * 推荐关系（包括直接和间接,得到被推荐人为 tAgentCode的推荐关系）
           * @return LARecomRelationSet
   */

   public LARecomRelationSet getUpRelation(String tAgentCode)
   {
       LARecomRelationSet tgetLARecomRelationSet = new LARecomRelationSet();
       LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();
       //查询本人为被接推人的推荐关系 包括直接和间接
       String downsql = "select * from lARecomrelation   where agentcode= '" +
               tAgentCode + "'   ";
       tgetLARecomRelationSet = tLARecomRelationDB.executeQuery(downsql);
       return tgetLARecomRelationSet;
   }
   /**
           * 推荐关系（包括直接和间接,得到推荐人为 tAgentCode的推荐关系）
           * @return LARecomRelationSet
   */
   public LARecomRelationSet getDownRelation(String tAgentCode)
   {
       LARecomRelationSet tgetLARecomRelationSet = new LARecomRelationSet();
       LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();
       //查询本人为接推人的推荐关系 包括直接和间接
       String downsql = "select * from lARecomrelation   where recomagentcode= '" +
               tAgentCode + "'   ";
       tgetLARecomRelationSet = tLARecomRelationDB.executeQuery(downsql);
       return tgetLARecomRelationSet;
   }

    private void jbInit() throws Exception {
    }

}
