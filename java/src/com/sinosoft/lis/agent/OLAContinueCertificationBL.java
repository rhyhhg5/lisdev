/*
 * <p>ClassName: OLACertificationBL </p>
 * <p>Description: OLACertificationBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:07:04
 */
package com.sinosoft.lis.agent;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class OLAContinueCertificationBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
private String mCertifNoQuery;
/** 业务处理相关变量 */
private LACertificationSchema mLACertificationSchema=new LACertificationSchema();
private LACertificationSet mLACertificationSet=new LACertificationSet();
private LACertificationSchema mupLACertificationSchema=new LACertificationSchema();
private LACertificationSet mupLACertificationSet=new LACertificationSet();
private LAAgentSet mLAAgentSet=new LAAgentSet();

private MMap map = new MMap();
private LACertificationSet mdelLACertificationSet = new LACertificationSet();
private LACertificationBSet mLACertificationBSet = new LACertificationBSet();

public OLAContinueCertificationBL() {
}
public static void main(String[] args) {
}
/**
* 传输数据的公共方法
	* @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) return false;
     
    //进行业务处理
    if (!dealData())
    {
       // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OLACertificationBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据处理失败OLACertificationBL-->dealData!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    //准备往后台的数据 
    if (!prepareOutputData()) return false;
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "OLACertificationBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mInputData=null;
    return true;
}

 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
    boolean tReturn=true;
    String currentDate = PubFun.getCurrentDate();
    String currentTime = PubFun.getCurrentTime();
    String tCertifNo = this.mLACertificationSchema.getCertifNo();
    String tAgentCode = this.mLACertificationSchema.getAgentCode();
    
    LACertificationDB tLACertificationDB=new LACertificationDB();
    LACertificationSet tLACertificationSet=new LACertificationSet();
    LAAgentDB tLAAgentDB=new LAAgentDB();
    LAAgentSet tLAAgentSet=new LAAgentSet();
    // 校验录入业务员是否有效
    String sql="select * from LAAgent where AgentCode='"+tAgentCode+"' and branchtype='1' ";
    tLAAgentSet=tLAAgentDB.executeQuery(sql);
    if (tLAAgentSet==null || tLAAgentSet.size() ==0)
    {
        CError tError = new CError();
        tError.moduleName = "OLACertificationBL";
        tError.functionName = "submitDat";
        tError.errorMessage = "系统中没有营销员"+tAgentCode+"信息!";
        this.mErrors.addOneError(tError);
        return false;

    }
    if (mOperate.equals("INSERT||MAIN"))
    {
    	// 校验页面录入展业证号系统中是否已存在
        String tSQL="select certifno from LACertification where CertifNo='"+tCertifNo+"' and state = '0' ";
        ExeSQL tExeSQL = new ExeSQL();
        String tcheck = tExeSQL.getOneValue(tSQL);
        if (tcheck==null||("").equals(tcheck))
        {
           // 起始日期、截止日期逻辑校验
           if (mLACertificationSchema.getValidStart().compareTo(mLACertificationSchema.getValidEnd())>0)
           {
                CError tError = new CError();
                tError.moduleName = "OLACertificationBL";
                tError.functionName = "dealData";
                tError.errorMessage = "有效起始日期应小于有效截止日期!";
                this.mErrors.addOneError(tError);
                return false;
            } 
            // 校验业务员原展业证是否已失效 0-有效 1-失效
            String cSQL="select * from LACertification where AgentCode ='"+tAgentCode+"' and State='0'";
            LACertificationDB nLACertificationDB=new LACertificationDB();
            LACertificationSet ntLACertificationSet=new LACertificationSet();
            ntLACertificationSet=nLACertificationDB.executeQuery(cSQL);
            if (ntLACertificationSet!=null && ntLACertificationSet.size()>0)
            {
                CError tError = new CError();
                tError.moduleName = "OLACertificationBL";
                tError.functionName = "dealData";
                tError.errorMessage = "原展业证书号"+ntLACertificationSet.get(1).getCertifNo()+"依然有效!";
                this.mErrors.addOneError(tError);
                return false;

            }
           this.mLACertificationSchema.setIdx(1); 
           this.mLACertificationSchema.setMakeDate(currentDate);
           this.mLACertificationSchema.setMakeTime(currentTime);
           this.mLACertificationSchema.setModifyDate(currentDate);
           this.mLACertificationSchema.setModifyTime(currentTime);
           this.mLACertificationSchema.setOperator(mGlobalInput.Operator);
           this.mLACertificationSchema.setreissueDate(currentDate);// 存储展业证有效开始日期
           map.put(this.mLACertificationSchema, "INSERT");
           // 历史数据处理
           cSQL ="select * from LACertification where agentcode = '"+tAgentCode+"'";
           System.out.println("查询打印SQL如下："+cSQL);
           this.mdelLACertificationSet=tLACertificationDB.executeQuery(cSQL);
           String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 19);// 获取变更批次号
           System.out.println(this.mdelLACertificationSet.size());
           for(int i = 1;i<=this.mdelLACertificationSet.size();i++)
           {
        	   LACertificationSchema tLACertificationSchema = new LACertificationSchema();
        	   LACertificationBSchema tLACertificationBSchema = new LACertificationBSchema();
        	   tLACertificationSchema = mdelLACertificationSet.get(i).getSchema();
        	   Reflections tReflections = new Reflections();
        	   tReflections.transFields(tLACertificationBSchema, tLACertificationSchema);
        	   tLACertificationBSchema.setEdorNo(tEdorNo);
        	   tLACertificationBSchema.setOldMakeDate(tLACertificationSchema.getMakeDate());
        	   tLACertificationBSchema.setOldMakeTime(tLACertificationSchema.getMakeTime());
        	   tLACertificationBSchema.setOldModifyDate(tLACertificationSchema.getModifyDate());
        	   tLACertificationBSchema.setOldModifyTime(tLACertificationSchema.getModifyTime());
        	   tLACertificationBSchema.setOldOperator(tLACertificationSchema.getOperator());
        	   tLACertificationBSchema.setMakeDate(currentDate);
        	   tLACertificationBSchema.setMakeTime(currentTime);
        	   tLACertificationBSchema.setModifyDate(currentDate);
        	   tLACertificationBSchema.setModifyTime(currentTime);
        	   tLACertificationBSchema.setOperator(this.mGlobalInput.Operator);
        	   this.mLACertificationBSet.add(tLACertificationBSchema);
           }
           map.put(this.mdelLACertificationSet,"DELETE");
           map.put(this.mLACertificationBSet, "INSERT");
           
           // laagent 表信息调整
           for(int a_count = 1;a_count<=tLAAgentSet.size();a_count++)
           {
        	   LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        	   tLAAgentSchema =tLAAgentSet.get(a_count).getSchema();
        	   tLAAgentSchema.setDevNo1(this.mLACertificationSchema.getCertifNo());
        	   this.mLAAgentSet.add(tLAAgentSchema);
 
           }
           map.put(this.mLAAgentSet, "UPDATE");
        }
        else
        {
                this.mErrors.copyAllErrors(tLACertificationDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLACertificationBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "系统已存在有效录入展业证书号!";
                this.mErrors.addOneError(tError);
                return false;
          }
    }
    if (this.mOperate.equals("UPDATE||MAIN"))
    {
        if(mCertifNoQuery==null || mCertifNoQuery.equals(""))
       {
           tLACertificationDB.setCertifNo(mLACertificationSchema.getCertifNo());
       }
       else
       {
         tLACertificationDB.setCertifNo(mCertifNoQuery);
       }
        tLACertificationDB.setAgentCode(mLACertificationSchema.getAgentCode());
            if (!tLACertificationDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "OLACertificationBL";
                tError.functionName = "dealData";
                tError.errorMessage = "原展业证书信息查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mupLACertificationSchema=tLACertificationDB.getSchema();
            // 基本数据校验 修改时，所属业务员是否存有有效展业证数据
            String cSQL = "select certifno from lacertification where agentcode = '"+tAgentCode+"' and certifno <>'"+mCertifNoQuery+"' and state = '0'";
            ExeSQL tExeSQL = new ExeSQL();
            String tResult = tExeSQL.getOneValue(cSQL);
            if(tResult==null||tResult.equals(""))
            {
            	// 展业证有效起期应小于展业证有效止期
              if (mLACertificationSchema.getValidStart().compareTo(mLACertificationSchema.getValidEnd())>0)
              {
                   CError tError = new CError();
                   tError.moduleName = "OLACertificationBL";
                   tError.functionName = "dealData";
                   tError.errorMessage = "有效起始日期应小于有效截止日期!";
                   this.mErrors.addOneError(tError);
                   return false;
               }
               // 置展业证失效时，需录入展业证失效日期
              if("1".equals(mLACertificationSchema.getState()))
              {
            	  if(mLACertificationSchema.getInvalidDate()==null||mLACertificationSchema.getInvalidDate().equals(""))
            	  {
            		  CError tError = new CError();
                      tError.moduleName = "OLACertificationBL";
                      tError.functionName = "dealData";
                      tError.errorMessage = "页面展业证状态为失效时，需录入对应失效日期!";
                      this.mErrors.addOneError(tError);
                      return false;
            	  }
              }
            	String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 19);
            	// 先备份自己
            	Reflections t1 = new Reflections();
            	LACertificationBSchema tLACertificationBSchema = new LACertificationBSchema();
            	t1.transFields(tLACertificationBSchema, mupLACertificationSchema);
         	    tLACertificationBSchema.setEdorNo(tEdorNo);
        	    tLACertificationBSchema.setOldMakeDate(mupLACertificationSchema.getMakeDate());
        	    tLACertificationBSchema.setOldMakeTime(mupLACertificationSchema.getMakeTime());
        	    tLACertificationBSchema.setOldModifyDate(mupLACertificationSchema.getModifyDate());
        	    tLACertificationBSchema.setOldModifyTime(mupLACertificationSchema.getModifyTime());
        	    tLACertificationBSchema.setOldOperator(mupLACertificationSchema.getOperator());
        	    tLACertificationBSchema.setMakeDate(currentDate);
        	    tLACertificationBSchema.setMakeTime(currentTime);
        	    tLACertificationBSchema.setModifyDate(currentDate);
        	    tLACertificationBSchema.setModifyTime(currentTime);
        	    tLACertificationBSchema.setOperator(this.mGlobalInput.Operator);
        	    this.mLACertificationBSet.add(tLACertificationBSchema);

        	    
        	    
            	
              // 三个字段不能进行修改 agentcode certifno idx
                if("0".equals(mLACertificationSchema.getState())&&("1").equals(mupLACertificationSchema.getState()))
                {
              	  mupLACertificationSchema.setreissueDate(currentDate);
                }
              mupLACertificationSchema.setAuthorUnit(this.mLACertificationSchema.getAuthorUnit());//批准单位
              mupLACertificationSchema.setGrantDate(this.mLACertificationSchema.getGrantDate());//发放日期
              mupLACertificationSchema.setState(this.mLACertificationSchema.getState());// 状态
              if("1".equals(this.mLACertificationSchema.getState()))
              {
                  mupLACertificationSchema.setInvalidDate(this.mLACertificationSchema.getInvalidDate());// 失效日期 
                  mupLACertificationSchema.setInvalidRsn(this.mLACertificationSchema.getInvalidRsn());// 失效原因
              }else{
                  mupLACertificationSchema.setInvalidDate("");// 失效日期 
                  mupLACertificationSchema.setInvalidRsn("");// 失效原因
              }

              mupLACertificationSchema.setValidStart(this.mLACertificationSchema.getValidStart());// 起始有效期
              mupLACertificationSchema.setValidEnd(this.mLACertificationSchema.getValidEnd());// 有效止期
            //  mupLACertificationSchema.setMakeDate(this.mLACertificationSchema.getMakeDate());
            //  mupLACertificationSchema.setMakeTime(this.mLACertificationSchema.getMakeTime());
              mupLACertificationSchema.setModifyDate(currentDate);
              mupLACertificationSchema.setModifyTime(currentTime);
              map.put(mupLACertificationSchema, "UPDATE");
              
              // 备份其他信息
              cSQL = "select * from lacertification where agentcode = '"+tAgentCode+"' and certifno<>'"+mCertifNoQuery+"'  "; 
              this.mdelLACertificationSet=tLACertificationDB.executeQuery(cSQL);
              for(int j=1;j<=mdelLACertificationSet.size();j++)
              {
            	  LACertificationSchema tLACertificationSchema = new LACertificationSchema();
            	  tLACertificationSchema =mdelLACertificationSet.get(j).getSchema();
            	  LACertificationBSchema t_LACertificationBSchema = new LACertificationBSchema();
            	  Reflections tReflections = new Reflections();
            	  tReflections.transFields(t_LACertificationBSchema, tLACertificationSchema);
            	  t_LACertificationBSchema.setEdorNo(tEdorNo);
            	  t_LACertificationBSchema.setOldMakeDate(tLACertificationSchema.getMakeDate());
            	  t_LACertificationBSchema.setOldMakeTime(tLACertificationSchema.getMakeTime());
            	  t_LACertificationBSchema.setOldModifyDate(tLACertificationSchema.getModifyDate());
            	  t_LACertificationBSchema.setOldModifyTime(tLACertificationSchema.getModifyTime());
            	  t_LACertificationBSchema.setOldOperator(tLACertificationSchema.getOperator());
            	  t_LACertificationBSchema.setMakeDate(currentDate);
            	  t_LACertificationBSchema.setMakeTime(currentTime);
            	  t_LACertificationBSchema.setModifyDate(currentDate);
            	  t_LACertificationBSchema.setModifyTime(currentTime);
            	  t_LACertificationBSchema.setOperator(this.mGlobalInput.Operator);
            	  this.mLACertificationBSet.add(t_LACertificationBSchema); 
              }
              map.put(this.mLACertificationBSet, "INSERT");
              map.put(this.mdelLACertificationSet,"DELETE");
              
              // laagent 表信息调整
              if("0".equals(this.mLACertificationSchema.getState()))
              {
               for(int a_count = 1;a_count<=tLAAgentSet.size();a_count++)
                {
           	      LAAgentSchema tLAAgentSchema = new LAAgentSchema();
           	      tLAAgentSchema =tLAAgentSet.get(a_count).getSchema();
           	      tLAAgentSchema.setDevNo1(this.mLACertificationSchema.getCertifNo());
           	      this.mLAAgentSet.add(tLAAgentSchema);    
              }
              }
              map.put(this.mLAAgentSet, "UPDATE");
            	
            }else{
           	 // @@错误处理
                CError tError = new CError();
                tError.moduleName = "OLAQualificationBL";
                tError.functionName = "dealData";
                tError.errorMessage = "系统中存有该人员有效展业证信息（对应展业证号为"+tResult+"）,烦请先进行有效展业证信息处理操作!";
                this.mErrors.addOneError(tError);
                return false;
            	
            }
    }
    return tReturn;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
         this.mLACertificationSchema.setSchema((LACertificationSchema)cInputData.getObjectByObjectName("LACertificationSchema",0));
         this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         this.mCertifNoQuery=String.valueOf(cInputData.get(0));
         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
    this.mResult.clear();
    LACertificationDB tLACertificationDB=new LACertificationDB();
    tLACertificationDB.setSchema(this.mLACertificationSchema);
		//如果有需要处理的错误，则返回
		if (tLACertificationDB.mErrors.needDealError())
 		{
		  // @@错误处理
 			this.mErrors.copyAllErrors(tLACertificationDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "LACertificationBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors .addOneError(tError) ;
 			return false;
    }
    mInputData=null;
    return true;
}
 private boolean prepareOutputData()
 {
   try
	{
		this.mInputData.clear();
		 mInputData.add(map);
	}
	catch(Exception ex)
	{
 		// @@错误处理
		CError tError =new CError();
 		tError.moduleName="LACertificationBL";
 		tError.functionName="prepareData";
 		tError.errorMessage="在准备往后层处理所需要的数据时出错。";
 		this.mErrors .addOneError(tError) ;
		return false;
	}
	return true;
	}
	public VData getResult()
	{
  	return this.mResult;
	}
}
