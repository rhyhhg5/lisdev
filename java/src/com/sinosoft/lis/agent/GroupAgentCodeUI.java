package com.sinosoft.lis.agent;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class GroupAgentCodeUI {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;


	public boolean submitData(VData cInputData, String cOperate) {
		this.mOperate = cOperate;
		GroupAgentCodeBL tGroupAgentCodeBL = new GroupAgentCodeBL();
		tGroupAgentCodeBL.submitData(cInputData, mOperate);
		if (tGroupAgentCodeBL.mErrors.needDealError()) {
			this.mErrors.copyAllErrors(tGroupAgentCodeBL.mErrors);
			CError tError = new CError();
			tError.moduleName = "GroupAgentCodeUI";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}
}

