package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentBlacklistDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAAgenttempSchema;
import com.sinosoft.lis.schema.LARearRelationSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.schema.LATreeTempSchema;
import com.sinosoft.lis.schema.LAWarrantorSchema;
import com.sinosoft.lis.vschema.LAAgentBlacklistSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.vschema.LAWarrantorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public class LABankAgentNACUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private String mIsManager;
    private String mOrphanCode="N";
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAgenttempSchema mLAAgenttempSchema = new LAAgenttempSchema();
    private LATreeTempSchema mLATreeTempSchema = new LATreeTempSchema();
    private LAWarrantorSet mLAWarrantorSet = new LAWarrantorSet();
    private LARearRelationSet mLARearRelationSet = new LARearRelationSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private TransferData mTransferData;
    LABankAgentNACBL mLABankAgentNACBL = new LABankAgentNACBL();

    private String mAgentCode = "";

    public String getAgentCode() {
        return mAgentCode;
    }

    public LABankAgentNACUI() {
    }

    public static void main(String[] args) {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ComCode = "86";

        String tIsManager = "true";

        LAAgenttempSchema tLAAgenttempSchema = new LAAgenttempSchema();
        tLAAgenttempSchema.setAgentCode("");
        tLAAgenttempSchema.setName("aaa");
        tLAAgenttempSchema.setSex("0");
        tLAAgenttempSchema.setBirthday("2005-01-17");
        tLAAgenttempSchema.setAgentGroup("861100000201");
        tLAAgenttempSchema.setBranchType("1");
        tLAAgenttempSchema.setBranchType2("01");
        tLAAgenttempSchema.setManageCom("86110000");
        tLAAgenttempSchema.setAssuMoney("1");
        tLAAgenttempSchema.setBranchCode("000000000386");
        tLAAgenttempSchema.setIDNo("1814117111111111111");
        tLAAgenttempSchema.setIDNoType("0");
        
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        tLAAgentSchema.setAgentCode("");
        tLAAgentSchema.setName("aaa");
        tLAAgentSchema.setSex("0");
        tLAAgentSchema.setBirthday("2005-01-17");
        tLAAgentSchema.setAgentGroup("861100000201");
        tLAAgentSchema.setBranchType("1");
        tLAAgentSchema.setBranchType2("01");
        tLAAgentSchema.setManageCom("86110000");
        tLAAgentSchema.setAssuMoney("1");
        tLAAgentSchema.setBranchCode("000000000386");
        tLAAgentSchema.setIDNo("1814117111111111111");
        tLAAgentSchema.setIDNoType("0");

        LATreeTempSchema tLATreeTempSchema = new LATreeTempSchema();
        tLATreeTempSchema.setAgentCode("");
        tLATreeTempSchema.setAgentGroup("000000000386");
        tLATreeTempSchema.setManageCom("86110000");
        tLATreeTempSchema.setAgentGrade("B11");
        tLATreeTempSchema.setBranchType("1");
        tLATreeTempSchema.setBranchType2("11");
        
        LATreeSchema tLATreeSchema = new LATreeSchema();
        tLATreeSchema.setAgentCode("");
        tLATreeSchema.setAgentGroup("000000000386");
        tLATreeSchema.setManageCom("86110000");
        tLATreeSchema.setAgentGrade("B11");
        tLATreeSchema.setBranchType("1");
        tLATreeSchema.setBranchType2("11");

        LAWarrantorSet tLAWarrantorSet = new LAWarrantorSet();
        LAWarrantorSchema tLAWarrantorSchema = new LAWarrantorSchema();
        tLAWarrantorSchema.setAgentCode("8611000141");
        tLAWarrantorSchema.setCautionerName("da");
        tLAWarrantorSchema.setCautionerSex("0");
        tLAWarrantorSchema.setCautionerID("110011200501171111");
        tLAWarrantorSet.add(tLAWarrantorSchema);

        LARearRelationSet tLARearRelationSet = new LARearRelationSet();
        LARearRelationSchema tLARearRelationSchema = new LARearRelationSchema();
        tLARearRelationSet.add(tLARearRelationSchema);

        VData tVData = new VData();
        tVData.add(tG);
        tVData.add(tIsManager);
        tVData.addElement(tLAAgenttempSchema);
        tVData.addElement(tLATreeTempSchema);
        tVData.addElement(tLAWarrantorSet);

        LABankAgentNACUI tLAAgent = new LABankAgentNACUI();
        tLAAgent.submitData(tVData, "INSERT||MAIN");
        //tLAAgent.submitData(tVData, "INSERT||MAIN");
        System.out.println("------Error------:" +
                           tLAAgent.mErrors.getFirstError());
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Begin LABankAgentNACUI.submitData.........");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("Start LABankAgentNACBL Submit...");
        mLABankAgentNACBL.submitData(mInputData, mOperate);
        System.out.println("End LABankAgentNACBL Submit...");
        //如果有需要处理的错误，则返回
        if (mLABankAgentNACBL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(mLABankAgentNACBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LABankAgentNACUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mAgentCode = mLABankAgentNACBL.getAgentCode();
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        System.out.println("Begin LABankAgentNACUI.getInputData.........");
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mIsManager = (String) cInputData.getObject(1);

        this.mLAAgenttempSchema.setSchema((LAAgenttempSchema) cInputData.
                                      getObjectByObjectName("LAAgenttempSchema", 0));
        this.mLATreeTempSchema.setSchema((LATreeTempSchema) cInputData.
                                     getObjectByObjectName("LATreeTempSchema", 0));
        this.mLAWarrantorSet.set((LAWarrantorSet) cInputData.
                                 getObjectByObjectName("LAWarrantorSet", 0));
        this.mLARearRelationSet.set((LARearRelationSet) cInputData.
                                    getObjectByObjectName("LARearRelationSet",
                0));
        this.mLATreeSchema.setSchema((LATreeSchema) cInputData.
                getObjectByObjectName("LATreeSchema", 0));
        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                getObjectByObjectName("LAAgentSchema", 0));
        this.mTransferData=(TransferData) cInputData.getObject(8);
        
       // System.out.println(mIsManager+"***********"+mOrphanCode);
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABankAgentNACUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("Begin LABankAgentNACUI.dealData.........");
        String tIDNo = this.mLAAgenttempSchema.getIDNo();
        String tIDNo1=this.mLAAgentSchema.getIDNo();
        System.out.println("111111111111111111111111111111111"+tIDNo1+tIDNo);
        String tIDNoType = this.mLAAgenttempSchema.getIDNoType();
        String tIDNoType1 = this.mLAAgentSchema.getIDNoType();
        System.out.println("222222222222222222222222222222222"+tIDNoType1+tIDNoType);
        if ((tIDNo == null || tIDNoType == null || tIDNo.equals("") ||
            tIDNoType.equals("")) && (tIDNo1 == null || tIDNoType1 == null ||tIDNo1.equals("") ||
            tIDNoType1.equals(""))) {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有证件类型或证件号";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tName = this.mLAAgenttempSchema.getName();
        String tName1= this.mLAAgentSchema.getName();
        if ((tName == null || tName.equals(""))&& (tName1 == null || tName1.equals(""))) {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "dealData";
            tError.errorMessage = "代理人姓名为空";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tSex = this.mLAAgenttempSchema.getSex();
        String tSex1 = this.mLAAgentSchema.getSex();
        if ((tSex == null || tSex.equals("")) && (tSex1 == null || tSex1.equals(""))) {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "dealData";
            tError.errorMessage = "代理人性别为空";
            this.mErrors.addOneError(tError);
            return false;
        }
        LAAgentBlacklistDB tLAAgentBlackDB = new LAAgentBlacklistDB();
        tLAAgentBlackDB.setName(tName);
        tLAAgentBlackDB.setSex(tSex);
        tLAAgentBlackDB.setIDNo(tIDNo);
        tLAAgentBlackDB.setIDNoType(tIDNoType);
        LAAgentBlacklistSet tLAAgentBlackSet = tLAAgentBlackDB.query();
        if (tLAAgentBlackDB.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "checkData";
            tError.errorMessage = "查询黑名单表出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLAAgentBlackSet.size() > 0) {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "checkData()";
            tError.errorMessage = "该代理人已列入黑名单中，无法增员！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            System.out.println("Begin LABankAgentNACUI.prepareOutputData.........");
            mInputData.clear();
            mInputData.add(this.mGlobalInput);
            mInputData.add(this.mIsManager);
            mInputData.add(this.mTransferData);

            mInputData.add(this.mLAAgenttempSchema);
            mInputData.add(this.mLATreeTempSchema);
            mInputData.add(this.mLAWarrantorSet);
            mInputData.add(this.mLARearRelationSet);
            mInputData.add(this.mLAAgentSchema);
            mInputData.add(this.mLATreeSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABankAgentNACUI";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
