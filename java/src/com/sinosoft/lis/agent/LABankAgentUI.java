package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentBlacklistDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LARearRelationSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.schema.LAWarrantorSchema;
import com.sinosoft.lis.vschema.LAAgentBlacklistSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.vschema.LAWarrantorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public class LABankAgentUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private String mIsManager;
    private String mOrphanCode="N";
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private LAWarrantorSet mLAWarrantorSet = new LAWarrantorSet();
    private LARearRelationSet mLARearRelationSet = new LARearRelationSet();
    private TransferData mTransferData;
    LABankAgentBLF mLABankAgentBLF = new LABankAgentBLF();

    private String mAgentCode = "";
    private String mAgentState = "";
    
    public String getAgentCode() {
        return mAgentCode;
    }
    
    public String getAgentState() {
        return mAgentState;
    }
    
    public LABankAgentUI() {
    }

    public static void main(String[] args) {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ComCode = "86";

        String tIsManager = "true";

        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        tLAAgentSchema.setAgentCode("");
        tLAAgentSchema.setName("aaa");
        tLAAgentSchema.setSex("0");
        tLAAgentSchema.setBirthday("2005-01-17");
        tLAAgentSchema.setAgentGroup("861100000201");
        tLAAgentSchema.setBranchType("1");
        tLAAgentSchema.setBranchType2("01");
        tLAAgentSchema.setManageCom("86110000");
        tLAAgentSchema.setAssuMoney("1");
        tLAAgentSchema.setBranchCode("000000000386");
        tLAAgentSchema.setIDNo("1814117111111111111");
        tLAAgentSchema.setIDNoType("0");

        LATreeSchema tLATreeSchema = new LATreeSchema();
        tLATreeSchema.setAgentCode("");
        tLATreeSchema.setAgentGroup("000000000386");
        tLATreeSchema.setIntroAgency("1101000179");
        tLATreeSchema.setManageCom("86110000");
        tLATreeSchema.setAgentGrade("B11");
        tLATreeSchema.setAgentLine("A");
        tLATreeSchema.setBranchType("1");
        tLATreeSchema.setBranchType2("11");

        LAWarrantorSet tLAWarrantorSet = new LAWarrantorSet();
        LAWarrantorSchema tLAWarrantorSchema = new LAWarrantorSchema();
        tLAWarrantorSchema.setAgentCode("8611000141");
        tLAWarrantorSchema.setCautionerName("da");
        tLAWarrantorSchema.setCautionerSex("0");
        tLAWarrantorSchema.setCautionerID("110011200501171111");
        tLAWarrantorSet.add(tLAWarrantorSchema);

        LARearRelationSet tLARearRelationSet = new LARearRelationSet();
        LARearRelationSchema tLARearRelationSchema = new LARearRelationSchema();
        tLARearRelationSet.add(tLARearRelationSchema);

        VData tVData = new VData();
        tVData.add(tG);
        tVData.add(tIsManager);
        tVData.addElement(tLAAgentSchema);
        tVData.addElement(tLATreeSchema);
        tVData.addElement(tLAWarrantorSet);

        LABankAgentUI tLAAgent = new LABankAgentUI();
        tLAAgent.submitData(tVData, "INSERT||MAIN");
        //tLAAgent.submitData(tVData, "INSERT||MAIN");
        System.out.println("------Error------:" +
                           tLAAgent.mErrors.getFirstError());
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Begin LABankAgentUI.submitData.........");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("Start LABankAgentBLF Submit...");
        mLABankAgentBLF.submitData(mInputData, mOperate);
        System.out.println("End LABankAgentBLF Submit...");
        //如果有需要处理的错误，则返回
        if (mLABankAgentBLF.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(mLABankAgentBLF.mErrors);
            CError tError = new CError();
            tError.moduleName = "LABankAgentUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mAgentCode = mLABankAgentBLF.getAgentCode();
        mAgentState=mLABankAgentBLF.getAgentState();
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        System.out.println("Begin LABankAgentUI.getInputData.........");
        mGlobalInput.setSchema((GlobalInput) cInputData.get(0));
        mIsManager = (String) cInputData.getObject(2);

        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                                      getObjectByObjectName("LAAgentSchema", 0));
        this.mLATreeSchema.setSchema((LATreeSchema) cInputData.
                                     getObjectByObjectName("LATreeSchema", 0));
        this.mLAWarrantorSet.set((LAWarrantorSet) cInputData.
                                 getObjectByObjectName("LAWarrantorSet", 0));
        this.mLARearRelationSet.set((LARearRelationSet) cInputData.
                                    getObjectByObjectName("LARearRelationSet",
                0));
        this.mTransferData=(TransferData)cInputData.get(1);

        if(mLAAgentSchema.getBranchType().equals("1")&&
          mLAAgentSchema.getBranchType2().equals("01"))
        {
            this.mOrphanCode = (String) cInputData.getObject(6);
            System.out.println("***********"+mOrphanCode);
        }

       // System.out.println(mIsManager+"***********"+mOrphanCode);
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABankAgentUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("Begin LABankAgentUI.dealData.........");
        String tIDNo = this.mLAAgentSchema.getIDNo();
        String tIDNoType = this.mLAAgentSchema.getIDNoType();
        if (tIDNo == null || tIDNoType == null || tIDNo.equals("") ||
            tIDNoType.equals("")) {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有证件类型或证件号";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (this.mOperate.equals("INSERT||MAIN") ||
            this.mOperate.indexOf("UPDATE") != -1) {

            String sql = "select count(*) from laagent where idno='" + tIDNo
                         + "' and idnotype ='" + tIDNoType
                         + "' and agentstate<'03' and branchType='" +
                         mLAAgentSchema.getBranchType() + "' and BranchType2='" +
                         mLAAgentSchema.getBranchType2() + "'";
            if (mLAAgentSchema.getAgentCode() != null &&
                !mLAAgentSchema.getAgentCode().equals("")) {
                sql = sql + " and agentcode<>'" + mLAAgentSchema.getAgentCode() +
                      "' ";
            }
            ExeSQL tExeSQL = new ExeSQL();
            System.out.println(sql);
            String result = tExeSQL.getOneValue(sql);
            if (Integer.parseInt(result) > 0) {
                CError tError = new CError();
                tError.moduleName = "ALAgentBL";
                tError.functionName = "dealData";
                tError.errorMessage = "证件号已经使用过";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        String tName = this.mLAAgentSchema.getName();
        if (tName == null || tName.equals("")) {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "dealData";
            tError.errorMessage = "代理人姓名为空";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tSex = this.mLAAgentSchema.getSex();
        if (tSex == null || tSex.equals("")) {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "dealData";
            tError.errorMessage = "代理人性别为空";
            this.mErrors.addOneError(tError);
            return false;
        }
        LAAgentBlacklistDB tLAAgentBlackDB = new LAAgentBlacklistDB();
        tLAAgentBlackDB.setName(tName);
        tLAAgentBlackDB.setSex(tSex);
        tLAAgentBlackDB.setIDNo(tIDNo);
        tLAAgentBlackDB.setIDNoType(tIDNoType);
        LAAgentBlacklistSet tLAAgentBlackSet = tLAAgentBlackDB.query();
        if (tLAAgentBlackDB.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "checkData";
            tError.errorMessage = "查询黑名单表出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLAAgentBlackSet.size() > 0) {
            CError tError = new CError();
            tError.moduleName = "ALAgentBL";
            tError.functionName = "checkData()";
            tError.errorMessage = "该代理人已列入黑名单中，无法增员！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            System.out.println("Begin LABankAgentUI.prepareOutputData.........");
            mInputData.clear();
            mInputData.add(this.mGlobalInput);
            mInputData.add(this.mIsManager);
            mInputData.add(this.mTransferData);

            mInputData.add(this.mLAAgentSchema);
            mInputData.add(this.mLATreeSchema);
            mInputData.add(this.mLAWarrantorSet);
            mInputData.add(this.mLARearRelationSet);
            mInputData.add(this.mOrphanCode);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABankAgentUI";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
