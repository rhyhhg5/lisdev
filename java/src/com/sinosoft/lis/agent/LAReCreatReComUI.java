package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentBlacklistDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LAWarrantorDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAgentBlacklistSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.vschema.LAWarrantorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public class LAReCreatReComUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private String mAgentName;
    private String mSex;
    private String mIDNoType;
    private String mIDNo;
    private String mBirthDay;
    private String mAgentCode;
    private String mCreComAgentCode;
    private String mAgentGrade;
    private String mIsManager;
    private String mOrphanCode;
    private String AgentName;
    private String mReComAgentCode;
    
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private LAWarrantorSet mLAWarrantorSet = new LAWarrantorSet();
    private LARearRelationSet mLARearRelationSet = new LARearRelationSet();
    LAReCreatReComBL mLAReCreatReComBL = new LAReCreatReComBL();



    public LAReCreatReComUI() {
    }


    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Begin LAReCreatComUI.submitData.........");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("Start LAAgentEditBLF Submit...");
        mLAReCreatReComBL.submitData(mInputData, mOperate);
        System.out.println("End LAAgentEditBLF Submit...");
        //如果有需要处理的错误，则返回
        if (mLAReCreatReComBL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(mLAReCreatReComBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mAgentCode = mLAReCreatReComBL.getAgentCode();
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        
    	 try {
    	        System.out.println("Begin LANewToFormBL.getInputData.........");
    	        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
    	                "GlobalInput", 0));
    	        mAgentName = (String)cInputData.get(1);
    	        mIsManager = (String)cInputData.get(2);
    	        mOrphanCode =(String)cInputData.get(3);
    	        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                        getObjectByObjectName("LAAgentSchema", 0));
    	        mReComAgentCode = (String)cInputData.get(5);
    	        
    	        System.out.println(mIsManager);
    	        System.out.println(mAgentName);
    	        System.out.println(mReComAgentCode); 
    	      }
    	      catch (Exception ex) {
    	        // @@错误处理
    	        CError tError = new CError();
    	        tError.moduleName = "LAReCreatReComUI";
    	        tError.functionName = "getInputData";
    	        tError.errorMessage = "在读取处理所需要的数据时出错。";
    	        this.mErrors.addOneError(tError);
    	        return false;
    	      }
    	      System.out.println("getInputData end ");
    	      return true;
    	    }
    	

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("Begin LAReCreatComUI.dealData.........");
        String  tIDNo = mLAAgentSchema.getIDNo();
        String tIDNoType = mLAAgentSchema.getIDNoType();
        if (tIDNo == null || tIDNoType == null || tIDNo.equals("") ||
            tIDNoType.equals("")) {
            CError tError = new CError();
            tError.moduleName = "LAReCreatReComUI";
            tError.functionName = "dealData";
            tError.errorMessage = "没有证件类型或证件号";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (this.mOperate.equals("INSERT||MAIN") ||
            this.mOperate.indexOf("UPDATE") != -1) {
            LATreeDB tLATreeDB = new LATreeDB();
            mAgentCode = mLAAgentSchema.getAgentCode();
            tLATreeDB.setAgentCode(mAgentCode);
            if(!tLATreeDB.getInfo())
            {
            	CError tError = new CError();
                tError.moduleName = "LAReCreatReComUI";
                tError.functionName = "dealData";
                tError.errorMessage = "查询原代理人行政信息失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            mLATreeSchema =  tLATreeDB.getSchema();
//            mLATreeSchema.setIntroAgency(mReComAgentCode);
            String sql = "select count(*) from laagent where idno='" + tIDNo
                         + "' and idnotype ='" + tIDNoType
                         + "' and agentstate<'03' and branchType='" +
                         mLAAgentSchema.getBranchType() + "' and BranchType2='" +
                         mLAAgentSchema.getBranchType2() + "' and agentcode <> '"+mAgentCode+"' ";
            
            ExeSQL tExeSQL = new ExeSQL();
            System.out.println(sql);
            String result = tExeSQL.getOneValue(sql);
            if (Integer.parseInt(result) > 0) {
                CError tError = new CError();
                tError.moduleName = "LAReCreatReComUI";
                tError.functionName = "dealData";
                tError.errorMessage = "证件号已经使用过";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        String tName = mLAAgentSchema.getName();
        if (tName == null || tName.equals("")) {
            CError tError = new CError();
            tError.moduleName = "LAReCreatReComUI";
            tError.functionName = "dealData";
            tError.errorMessage = "代理人姓名为空";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tSex = this.mLAAgentSchema.getSex();
        if (tSex == null || tSex.equals("")) {
            CError tError = new CError();
            tError.moduleName = "LAReCreatReComUI";
            tError.functionName = "dealData";
            tError.errorMessage = "代理人性别为空";
            this.mErrors.addOneError(tError);
            return false;
        }
        LAAgentBlacklistDB tLAAgentBlackDB = new LAAgentBlacklistDB();
        tLAAgentBlackDB.setName(tName);
        tLAAgentBlackDB.setSex(tSex);
        tLAAgentBlackDB.setIDNo(tIDNo);
        tLAAgentBlackDB.setIDNoType(tIDNoType);
        LAAgentBlacklistSet tLAAgentBlackSet = tLAAgentBlackDB.query();
        if (tLAAgentBlackDB.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LAReCreatReComUI";
            tError.functionName = "checkData";
            tError.errorMessage = "查询黑名单表出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLAAgentBlackSet.size() > 0) {
            CError tError = new CError();
            tError.moduleName = "LAReCreatReComUI";
            tError.functionName = "checkData()";
            tError.errorMessage = "该代理人已列入黑名单中，无法修改业务员信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        //查询业务员的担保人信息
        LAWarrantorDB tLAWarrantorDB = new LAWarrantorDB();
        tLAWarrantorDB.setAgentCode(mAgentCode);
       
        mLAWarrantorSet = tLAWarrantorDB.query();
        if(mLAWarrantorSet.size()<= 0)
        {
        	CError tError = new CError();
        	tError.moduleName = "LAReCreatReComUI";
            tError.functionName = "checkData()";
            tError.errorMessage = "该代理人担保人查询失败,无法修改业务员信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            System.out.println("Begin LAReCreatComUI.prepareOutputData.........");
            mInputData.clear();
            mInputData.add(this.mGlobalInput);
            mInputData.add(this.mIsManager);
            mInputData.add(this.mAgentName);
            mInputData.add(this.mLAAgentSchema);
            mInputData.add(this.mLATreeSchema);
            mInputData.add(this.mLAWarrantorSet);
            mInputData.add(this.mLARearRelationSet);
            mInputData.add(this.mOrphanCode);
            mInputData.add(this.mReComAgentCode);
            
            
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAReCreatComUI";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
