/*
 * <p>ClassName: ALABankAgentBLS </p>
 * <p>Description: ALABankAgentBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-09
 */
package com.sinosoft.lis.agent;

import java.sql.Connection;

import com.sinosoft.lis.db.LAAgentBDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LATreeBDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

public class LABankTreeBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    public LABankTreeBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Start LABankTreeBLS Submit...");
        tReturn = updateLAAgent(cInputData);
        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End ALABankAgentBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean updateLAAgent(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "ALABankAgentBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 代理人信息保存...");
            String tFlag = (String) mInputData.get(5);
            if (tFlag.equals("Y"))
            {
                LAAgentDB tLAAgentDB = new LAAgentDB(conn);
                LAAgentSchema tLAAgentSchema = (LAAgentSchema) mInputData.
                                               getObjectByObjectName(
                        "LAAgentSchema", 0);
                tLAAgentDB.setSchema(tLAAgentSchema);
                if (!tLAAgentDB.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ALABankAgentBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }

                System.out.println("Start 代理人备份信息保存...");
                LAAgentBDB tLAAgentBDB = new LAAgentBDB(conn);
                tLAAgentBDB.setSchema((LAAgentBSchema) mInputData.
                                      getObjectByObjectName("LAAgentBSchema", 0));
                if (!tLAAgentBDB.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAAgentBDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ALAAgentBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            System.out.println("Start 代理人行政信息备份保存...");
            LATreeBDB tLATreeBDB = new LATreeBDB(conn);
            tLATreeBDB.setSchema((LATreeBSchema) mInputData.
                                 getObjectByObjectName("LATreeBSchema", 0));
            if (!tLATreeBDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeBDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAAgentBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            System.out.println("Start 行政信息保存...");
            LATreeDB tLATreeDB = new LATreeDB(conn);
            tLATreeDB.setSchema((LATreeSchema) mInputData.getObjectByObjectName(
                    "LATreeSchema", 0));
            if (!tLATreeDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALABankAgentBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALABankAgentBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }
}
