package com.sinosoft.lis.agent;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAgentGradeDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LAWarrantorDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentGradeSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAWarrantorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.schema.LAQualificationSchema;
import com.sinosoft.lis.vschema.LAQualificationSet;
import com.sinosoft.lis.db.LAQualificationDB;
import com.sinosoft.lis.vschema.LAAgentSet;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public  class LAAgentPICCHBL implements LAAgentInterface
{
    MMap map = new MMap();
    String mOperate = new String();
    GlobalInput mGlobalInput = new GlobalInput();
    LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    LAAgentBSchema mLAAgentBSchema = new LAAgentBSchema();
    LATreeSchema mLATreeSchema = new LATreeSchema();
    LAWarrantorSet mLAWarrantorSet = new LAWarrantorSet();
    LAQualificationSchema mLAQualificationSchema = new LAQualificationSchema();

    String mAgentCode = new String();
    String mBranchCode = new String();
    String mAgentGroup = new String();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String originalDate = "";
    private String originalTime = "";
    String mEdorNo = new String();

    public void setOperate(String cOperate)
    {
        mOperate = cOperate;
    }

    public void setGlobalInput(GlobalInput cGlobalInput)
    {
        mGlobalInput.setSchema(cGlobalInput);
    }

    public void setLAAgentSchema(LAAgentSchema cLAAgentSchema)
    {
        mLAAgentSchema.setSchema(cLAAgentSchema);
    }

    public LAAgentSchema getLAAgentSchema()
    {
        return mLAAgentSchema;
    }

    public void setLATreeSchema(LATreeSchema cLATreeSchema)
    {
        mLATreeSchema.setSchema(cLATreeSchema);
    }

    public void setLAWarrantorSet(LAWarrantorSet cLAWarrantorSet)
    {
        mLAWarrantorSet.set(cLAWarrantorSet);
    }


    public void setLAQualificationSchema(LAQualificationSchema cLAQualificationSchema)
       {
           mLAQualificationSchema.setSchema(cLAQualificationSchema);
    }

    public void setAgentCode(String cAgentCode)
    {
        mAgentCode = cAgentCode;
    }

    public MMap getResult()
    {
        return map;
    }

    public void setEdorNo(String cEdorNo)
    {
        mEdorNo = cEdorNo;
    }

    public LAAgentPICCHBL()
    {
    }

    public static void main(String[] args)
    {
        //LAAgentPICCHBL laagentxhbl = new LAAgentPICCHBL();
    }

    public boolean dealdata()
    {
        //判断是否已经有转正日期，有则不置，无则置值
        boolean tHaveInDueFormDate = false;
        //如果是修改操作，备份人员基本信息
        if (this.mOperate.indexOf("UPDATE") != -1)
        {
            //修改操作使用原代理人编码
            mAgentCode = this.mLAAgentSchema.getAgentCode().trim();
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(mAgentCode);
            if (!tLAAgentDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAAgentBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "查询原代理人信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tLAAgentDB.getInDueFormDate() != null &&
                tLAAgentDB.getInDueFormDate().trim().equals(""))
            {
                tHaveInDueFormDate = true;
            }
            //备份原代理人信息
            Reflections tReflections = new Reflections();
            tReflections.transFields(this.mLAAgentBSchema, tLAAgentDB.getSchema());
            this.mLAAgentBSchema.setEdorNo(mEdorNo);

            LAAgentSchema tLAAgentSchema = new LAAgentSchema();
            LAAgentDB tpreLAAgentDB = new LAAgentDB();
            tpreLAAgentDB.setAgentCode(mLAAgentSchema.getAgentCode());
            tpreLAAgentDB.getInfo();
            tLAAgentSchema = tpreLAAgentDB.getSchema();
            if (mLAAgentSchema.getAgentState().equals("02") &&
                tLAAgentSchema.getAgentState().compareTo("03") >= 0) { //二次增员(离职人员状态大于等于03  且 前台传入的 状态为 02 是为二次增员操作)
             this.mLAAgentBSchema.setEdorType("04");
            }
            else
            {
              this.mLAAgentBSchema.setEdorType("05");
            }
            System.out.println(tLAAgentSchema.getBranchType());
            System.out.println(tLAAgentSchema.getBranchType2());
            //需要给个险筹备人员添加筹备结束日期
            if(tLAAgentSchema.getBranchType().equals("1")&&
               tLAAgentSchema.getBranchType2().equals("01")){
              if (mLAAgentSchema.getPreparaType() != null && !mLAAgentSchema.getPreparaType().trim().equals("") ) {
               String tQ = "select enddate from lastatsegment "
                           + "where stattype='1' and yearmonth=int('" +
                           this.mLAAgentSchema.getPreparaType() + "')";
               ExeSQL tExeSQL = new ExeSQL();
               String tPreparaEndDate = tExeSQL.getOneValue(tQ);
               if (tPreparaEndDate == null || tPreparaEndDate.equals("")) {
               } else {
                   tPreparaEndDate = tPreparaEndDate.trim();
                   this.mLAAgentBSchema.setPrepareEndDate(tPreparaEndDate);
                   this.mLAAgentSchema.setPreparaType("");
               }
           }
       }
            //
            this.mLAAgentBSchema.setMakeDate(currentDate);
            this.mLAAgentBSchema.setMakeTime(currentTime);
            this.mLAAgentBSchema.setModifyDate(currentDate);
            this.mLAAgentBSchema.setModifyTime(currentTime);
            this.mLAAgentBSchema.setOperator(this.mGlobalInput.Operator);
            map.put(mLAAgentBSchema, "INSERT");
            originalDate = tLAAgentDB.getMakeDate();
            originalTime = tLAAgentDB.getMakeTime();
            //清空管理信息
        }
        if (this.mOperate.equals("UPDATE||PART"))
        {
            if (tHaveInDueFormDate == false)
            {
                if (!setInDueFormDate())
                {
                    return false;
                }
            }
            //this.mLAAgentSchema.setAgentGroup(tLAAgentDB.getAgentGroup());
            this.mLAAgentSchema.setModifyDate(currentDate);
            this.mLAAgentSchema.setModifyTime(currentTime);
            this.mLAAgentSchema.setOperator(mGlobalInput.Operator);
            map.put(mLAAgentSchema, "UPDATE");
        }
        if (this.mOperate.equals("INSERT||MAIN") ||
            this.mOperate.equals("UPDATE||MAIN") ||
            this.mOperate.equals("UPDATE||ALL"))
        {
            //代理人基本信息
            System.out.println("agentcode:" + mAgentCode);
            this.mLAAgentSchema.setAgentCode(mAgentCode);
            System.out.println("getagentcode:" + this.mLAAgentSchema.getAgentCode());
            //modify by zhuxt
            //个险直销的代理人合同号为代理人编码
            if(this.mLAAgentSchema.getBranchType().equals("1")&&
                    this.mLAAgentSchema.getBranchType2().equals("01")     )
                 {
            	    this.mLAAgentSchema.setAgentType("1");
            		this.mLAAgentSchema.setRetainContNo(mAgentCode);
                 }
            
            //agentgroup branchcode
            String tAgentGroup = this.mLAAgentSchema.getAgentGroup().trim();
            System.out.println("AgentGroup:" + tAgentGroup);
            //确定销售机构AgentGroup,从前台传入的是外部编码

            String SQL =
                    "select agentgroup from labranchgroup where branchattr='" +
                    tAgentGroup + "' and branchtype = '" +
                    this.mLAAgentSchema.getBranchType().trim() +
                    "' and branchtype2 = '" +
                    this.mLAAgentSchema.getBranchType2().trim() + "'";
            ExeSQL tExeSQL = new ExeSQL();
            System.out.println("find agentgroup sql is "+SQL);
            tAgentGroup = tExeSQL.getOneValue(SQL);
            System.out.println("zzz:" + tAgentGroup);
            if (tAgentGroup == null || tAgentGroup.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "LAAgentPICCHBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询机构信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tAgentGroup = tAgentGroup.trim();
            //BrachCode应该置为他所在的最低机构里，AgentGroup为主管所在最高机构
            //查找机构输入属性
            String tSQL =
                    "select VarValue from LASysVar where VarType = 'branch' ";
            ExeSQL tExeSQL2 = new ExeSQL();
            String branchinput = tExeSQL2.getOneValue(tSQL);
            if (branchinput == null)
            {
                branchinput = "";
            }
            branchinput = branchinput.trim();
            System.out.println("+++++++++++++++++++++++" + tAgentGroup);
            if (branchinput.equals("0")) //0表示机构输入最高级,1表示机构输入最低级
            {
                if (!setBranchHigh(tAgentGroup))
                {
                    return false;
                }
            }
            else
            {
                if (!setBranchLow(tAgentGroup))
                {
                    return false;
                }
            }
            System.out.println("++++++++666++++++++++");
            if (this.mLAAgentSchema.getEmployDate() == null ||
                this.mLAAgentSchema.getEmployDate().equals(""))
            {
                this.mLAAgentSchema.setEmployDate(currentDate);
            }


            System.out.println("tHaveInDueFormDate is "+tHaveInDueFormDate);
            if(!this.mLAAgentSchema.getBranchType().equals("2")&&
               !this.mLAAgentSchema.getBranchType2().equals("01")     )
            {
            if (tHaveInDueFormDate == false)
            {
                if (!setInDueFormDate())
                {
                    return false;
                }
            }
            }
            this.mLAAgentSchema.setOperator(mGlobalInput.Operator);
            if (this.mOperate.indexOf("UPDATE") != -1)
            {
                this.mLAAgentSchema.setMakeDate(originalDate);
                this.mLAAgentSchema.setMakeTime(originalTime);
            }
            else
            {
                this.mLAAgentSchema.setMakeDate(currentDate);
                this.mLAAgentSchema.setMakeTime(currentTime);
            }
            this.mLAAgentSchema.setModifyDate(currentDate);
            this.mLAAgentSchema.setModifyTime(currentTime);
            //放入MAP中
            System.out.println("111111111" + this.mLAAgentSchema.getAgentCode());
            if (this.mOperate.equals("INSERT||MAIN"))
            {
                map.put(this.mLAAgentSchema, "INSERT");
            }
            else if (this.mOperate.indexOf("UPDATE") != -1)
            {
                map.put(this.mLAAgentSchema, "UPDATE");
            }
            //查出原有担保人信息删除
            LAWarrantorDB tLAWarrantorDB = new LAWarrantorDB();
            LAWarrantorSet tLAWarrantorSet = new LAWarrantorSet();
            tLAWarrantorDB.setAgentCode(mAgentCode);
            tLAWarrantorSet = tLAWarrantorDB.query();
            map.put(tLAWarrantorSet, "DELETE");
            //添加新担保人信息表
            for (int i = 1; i <= this.mLAWarrantorSet.size(); i++)
            {
                if (this.mOperate.equals("INSERT||MAIN"))
                {
                    this.mLAWarrantorSet.get(i).setAgentCode(mAgentCode);
                }
                this.mLAWarrantorSet.get(i).setSerialNo(i);
                this.mLAWarrantorSet.get(i).setOperator(mGlobalInput.Operator);
                this.mLAWarrantorSet.get(i).setMakeDate(currentDate);
                this.mLAWarrantorSet.get(i).setMakeTime(currentTime);
                this.mLAWarrantorSet.get(i).setModifyDate(currentDate);
                this.mLAWarrantorSet.get(i).setModifyTime(currentTime);
            }
            map.put(mLAWarrantorSet, "INSERT");

            if(this.mLAAgentSchema.getBranchType().equals("1")&&
               this.mLAAgentSchema.getBranchType2().equals("01")     )
            {
          //查出原有担保人信息删除
          if (this.mOperate.equals("INSERT||MAIN"))
          {
        	 
           String tQualifNo = this.mLAQualificationSchema.getQualifNo();
           if(!tQualifNo.equals("")&&tQualifNo!=null){
           String ttSQL="select * from LAQualification where QualifNo='" +tQualifNo+"' " +
           		" and exists (select 1 from laagent a where a.agentcode = LAQualification.agentcode and a.agentstate < '06'" +
           		" and a.branchtype = '"+this.mLAAgentSchema.getBranchType()+"' and a.branchtype2 = '"+this.mLAAgentSchema.getBranchType2()+"' )";
           System.out.println("tSQL:"+ttSQL);
          LAQualificationDB tLAQualificationDB=new LAQualificationDB();
          LAQualificationSet tLAQualificationSet=new LAQualificationSet();
          tLAQualificationSet=tLAQualificationDB.executeQuery(ttSQL);
          if (tLAQualificationSet.size()==0)
          {
            if (mLAQualificationSchema.getValidStart().compareTo(mLAQualificationSchema.getValidEnd())>0)
            {
               CError tError = new CError();
               tError.moduleName = "LAAgentPICCHBL";
               tError.functionName = "dealData";
               tError.errorMessage = "有效起始日期应小于有效截止日期!";
               this.mErrors.addOneError(tError);
               return false;
            }
          this.mLAQualificationSchema.setAgentCode(mAgentCode);
          this.mLAQualificationSchema.setIdx(1);
          this.mLAQualificationSchema.setreissueDate(currentDate);
          this.mLAQualificationSchema.setMakeDate(currentDate);
          this.mLAQualificationSchema.setMakeTime(currentTime);
          this.mLAQualificationSchema.setModifyDate(currentDate);
          this.mLAQualificationSchema.setModifyTime(currentTime);
          this.mLAQualificationSchema.setOperator(mGlobalInput.Operator);
          }
       
       else
       {
               CError tError = new CError();
               tError.moduleName = "OLAQualificationBL";
               tError.functionName = "submitDat";
               tError.errorMessage = "该资格证书号已存在!";
               this.mErrors.addOneError(tError);
               return false;
         }
         map.put(mLAQualificationSchema, "INSERT");
        }
   }

}
        }
        return true;
    }

    /**
     * setInDueFormDate
     *
     * @return boolean
     */
    private boolean setInDueFormDate()
    {
        String tAgentGrade = this.mLATreeSchema.getAgentGrade();
        String tAgentKind = this.mLATreeSchema.getAgentKind();
        String tAgentLine = this.mLATreeSchema.getAgentLine();
        System.out.println("AgentLine:"+tAgentLine);
         System.out.println("Agentgrade:"+tAgentGrade);
        if (tAgentGrade == null)
        {
            tAgentGrade = "";
        }
        if (tAgentKind == null)
        {
            tAgentKind = "";
        }
        if (tAgentLine == null)
        {
            tAgentLine = "";
        }
        if ((tAgentGrade.equals("") && tAgentKind.equals("")) ||
            tAgentLine.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentXHBL";
            tError.functionName = "setInDueFormDate";
            tError.errorMessage = "查询不到此代理人的级别信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LAAgentGradeDB tLAAgentGradeDB = new LAAgentGradeDB();
        //注意这里AgentLine写成B，是死程序,cg负责此处问题
        if (tAgentLine.trim().equals("B"))
        {

            tLAAgentGradeDB.setGradeCode(tAgentKind.trim());
        }
        else
        {

            tLAAgentGradeDB.setGradeCode(tAgentGrade.trim());
        }
        if (!tLAAgentGradeDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentXHBL";
            tError.functionName = "setInDueFormDate";
            tError.errorMessage = "查询不到" + tAgentGrade + "的级别信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LAAgentGradeSchema tLAAgentGradeSchema = new LAAgentGradeSchema();
        tLAAgentGradeSchema = tLAAgentGradeDB.getSchema();
        //InDueFormDate
        String isNormal = tLAAgentGradeSchema.getGradeProperty4();
        if (isNormal == null || isNormal.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentXHBL";
            tError.functionName = "setInDueFormDate";
            tError.errorMessage = "查询" + tAgentGrade + "的级别的转正属性信息错误!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (isNormal.trim().equals("1"))
        {
            this.mLAAgentSchema.setInDueFormDate(this.mLAAgentSchema.
                                                 getEmployDate());
        }
        return true;
    }


    /**
     * setBranchDESC
     * 设置AgentGroup,BranchCode
     * @return boolean
     */
    private boolean setBranchHigh(String cBranch)
    {
        String tAgentGroup = cBranch;
        this.mLAAgentSchema.setAgentGroup(tAgentGroup);
        //递归查询最低级的下级直辖机构
        String tBranchCode = getDirectDownBranch(cBranch);
        this.mLAAgentSchema.setBranchCode(tBranchCode);
        return true;
    }

    /**
     * setBranchASC
     * 设置AgentGroup,BranchCode
     * @return boolean
     */
    private boolean setBranchLow(String cBranch)
    {
        String tBranchCode = cBranch;
        this.mLAAgentSchema.setBranchCode(tBranchCode);
        //递归查询最高级的上级直辖机构
        String tAgentGroup = getDirectUpBranch(cBranch);
        if (tAgentGroup == null)
        {
            return false;
        }
        if (this.mLATreeSchema.getAgentGrade().compareTo("B11")>=0)
        {
            this.mLAAgentSchema.setAgentGroup(tAgentGroup);
        }
        else
        {
            this.mLAAgentSchema.setAgentGroup(cBranch);
        }

        return true;
    }

    private String getDirectUpBranch(String cBranch)
    {
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(cBranch);
        if (!tLABranchGroupDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentXHBL";
            tError.functionName = "getDirectUpBranch";
            tError.errorMessage = "查询不到" + cBranch + "的机构信息!";
            this.mErrors.addOneError(tError);
            return null;
        }
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = tLABranchGroupDB.getSchema();
        String tUpBranchAttr = tLABranchGroupSchema.getUpBranchAttr();
        if (tUpBranchAttr == null || tUpBranchAttr.equals("") ||
            tUpBranchAttr.trim().equals("0"))
        {
            return cBranch;
        }
        else
        {
            //如果取上级机构出错，那就是机构建立时出错导致
            return getDirectUpBranch(tLABranchGroupSchema.getUpBranch().trim());
        }
    }

    private String getDirectDownBranch(String cBranch)
    {
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupDB.setUpBranch(cBranch);
        tLABranchGroupDB.setUpBranchAttr("1");
        //没有找到下级直辖机构，返回本身
        tLABranchGroupSet = tLABranchGroupDB.query();
        if (tLABranchGroupDB.mErrors.needDealError())
        {
            return null;
        }
        else
        {
            if(tLABranchGroupSet.size() == 0)
            {
                return cBranch;
            }
            else
            {
                LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
                tLABranchGroupSchema = tLABranchGroupSet.get(1);
                return getDirectDownBranch(tLABranchGroupSchema.getAgentGroup().trim());
            }
        }
    }
}
