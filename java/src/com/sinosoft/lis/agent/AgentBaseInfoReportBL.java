package com.sinosoft.lis.agent;
 
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.StrTool;

public class AgentBaseInfoReportBL{
      /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private VData mResult = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    private String mOperate="";
    private String mManageCom="";
    private String mAgentCom="";
    private String mState="";
    private String mBranchType="";
    private String mBranchType2="";
    private String mAgentSeries="";
    private String mCountDate="";
    
    
    
    private MMap mMap=new MMap();
    public static void main(String[] args)
    {
    }
    public AgentBaseInfoReportBL()
    {
    }

    private boolean getInputData(VData cInputData) {
        try {
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
           
            if (mGlobalInput == null || mTransferData==null) {
                CError tError = new CError();
                tError.moduleName = "AgentGroupActiveRateBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有得到足够的信息！";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            this.mManageCom = (String) mTransferData.getValueByName("ManageCom");
            this.mState = (String) mTransferData.getValueByName("State");
            this.mAgentSeries= (String) mTransferData.getValueByName("AgentSeries");
            this.mBranchType= (String) mTransferData.getValueByName("BranchType");
            this.mBranchType2 = (String) mTransferData.getValueByName("BranchType2");
            this.mCountDate= (String) mTransferData.getValueByName("CountDate");
            return true;
        }
        catch (Exception e) {
          // @@错误处理
          CError.buildErr(this, "接收数据失败");
          return false;
        }
    }
    public boolean checkData()
    {
        return true;
    }
    public boolean submitData(VData cInputData, String cOperate) {
        this.mOperate=cOperate;
        if(!getInputData(cInputData))
        {
            return false;
        }
        if(!checkData())
        {
            return false;
        }
        if(!dealDate())
        {
              CError tError = new CError();
              tError.moduleName = "AgentGroupActiveRateBL";
              tError.functionName = "submitData";
              tError.errorMessage = "数据处理失败-->dealData!";
              this.mErrors.addOneError(tError);
              return false;
        }
        return true;
    }

    public boolean dealDate()
    {
        
        String sql="";
        String condition="";
        if(!this.mManageCom.equals("")){
            condition+=" and a.ManageCom like '"+mManageCom+"%'";
        }
        if(!this.mState.equals("")){
            if(this.mState.equals("0")){
                condition+=" and AgentState in ('01','02') ";
            }
            else if(this.mState.equals("1")){
                condition+=" and AgentState in ('03','04','05') ";
            }
            else if(this.mState.equals("2")){
                condition+=" and AgentState not in ('01','02','03','04','05') ";
            }
        }
        if(!this.mAgentSeries.equals("")){
            condition+=" and AgentSeries='"+mAgentSeries+"' ";
        }
        if(!this.mCountDate.equals("")){
            condition+=" and EmployDate<='"+this.mCountDate+"' ";
        }
        sql="select a.ManageCom,(select name from labranchgroup where agentgroup=a.AgentGroup),a.name,a.agentcode, "
        +" case sex when '0' then '男' when '1' then '女' when '2' then '其他' end sex, "
				+" (days(current date)-days(Birthday))/365 age,Birthday,EmployDate, "
				+" case Degree when '0' then '博士以上' when '1' then '硕士' when '2' then '大本' when '3' then '大专' when '4' then '中专' when '5' then '高中' when '6' then '高中以下' when '7' then '其他' end degreee, "
				+" case salequaf when 'Y' then '是' when 'N' then '否' else '' end salequaf,a.QuafNo,a.Quafstartdate,a.Quafenddate,year(current date-EmployDate) workage, "
   			+"  case Isconnman when 'Y' then '是' when 'N' then '否' else '' end Isconnman,b.VIPProperty , "
   			+" a.Mobile,a.IDNo, "
				+" (select gradename from laagentgrade where gradecode=b.agentgrade) grade, "
				+" case AgentSeries when '0' then '员工'  when '1' then '经理' end AgentSeries, "
				+" case AgentState when '01' then '在职' when '02' then '在职' when '03' then '离职登记' when '04' then '离职等级' when '05' then '离职等级' else '离职' end state, "
				+" case when a.agentstate>'02' and a.agentstate<'06' then (select max(applydate) from ladimission where agentcode=a.agentcode) else a.outworkdate end " 
				+" from laagent a,latree b "
				+" where a.agentcode=b.agentcode and a.BranchType='"+this.mBranchType+"' and a.BranchType2='"+this.mBranchType2+"' "
				+condition
                +" with ur ";
        String strArr[] = null;
        XmlExport xmlexport = new XmlExport();
        xmlexport.createDocument("AgentInfoReport.vts", "printer");
        SSRS tSSRS=new ExeSQL().execSQL(sql);
        System.out.println("SQL:"+sql);
        ListTable tlistTable = new ListTable();
        tlistTable.setName("Get");
        if(tSSRS!=null){
            for (int i = 1; i <= tSSRS.MaxRow; i++)
            {
                strArr = new String[30];
             
                strArr[0] = String.valueOf(i);
                String tManageCom =tSSRS.GetText(i, 1);
                strArr[1]="";
                if(tManageCom!=null && tManageCom.length()>=4){
                        SSRS  ttSSRS=new ExeSQL().execSQL("select name from ldcom where comcode='"+tManageCom.substring(0,4)+"0000"+"'");
                        if(ttSSRS!=null){                       	
                            strArr[1]= ttSSRS.GetText(1, 1);
                        }
                        ttSSRS=new ExeSQL().execSQL("select name from ldcom where comcode='"+tManageCom+"'");
                        if(ttSSRS!=null){                           
                            strArr[2]= ttSSRS.GetText(1, 1);
                        }
                }
                strArr[3]=tSSRS.GetText(i, 2);
                strArr[4]=tSSRS.GetText(i, 3);
                strArr[5]=tSSRS.GetText(i, 4); 
                strArr[6]=tSSRS.GetText(i, 5);
                strArr[7]=tSSRS.GetText(i, 6);
                strArr[8]=tSSRS.GetText(i, 7);
                strArr[9]=tSSRS.GetText(i, 8);
                strArr[10]=tSSRS.GetText(i, 9);
                strArr[11]=tSSRS.GetText(i, 10);
                strArr[12]=tSSRS.GetText(i, 11);
                strArr[13]=tSSRS.GetText(i, 12);
                strArr[14]=tSSRS.GetText(i, 13);
                strArr[15]=tSSRS.GetText(i, 14);
                strArr[16]=tSSRS.GetText(i, 15);
                strArr[17]=tSSRS.GetText(i, 16);
                strArr[18]=tSSRS.GetText(i, 17);
                strArr[19]=tSSRS.GetText(i, 18);
                strArr[20]=tSSRS.GetText(i, 19);
                strArr[21]=tSSRS.GetText(i, 20);
                strArr[22]=tSSRS.GetText(i, 21);
                strArr[23]=tSSRS.GetText(i, 22);
                tlistTable.add(strArr);
            }
        }
        TextTag texttag = new TextTag();
        tSSRS=new ExeSQL().execSQL("select name from ldcom where comcode='"+mManageCom+"'");
        if(tSSRS!=null){
            texttag.add("ManageCom", tSSRS.GetText(1, 1)+"公司");
        }else{
            texttag.add("ManageCom", "");
        }
        String currentDate=this.mCountDate.substring(0,4)+"年"+mCountDate.substring(5,7)+"月份";
        texttag.add("CurrentDate", currentDate);
        strArr = new String[20];
        for(int i=0;i<20;i++){
        	strArr[i]="";
        }
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }

    public boolean prepareOutputData()
    {
         try
         {
             mInputData = new VData();
             mInputData.add(mMap);
         }
         catch(Exception e)
         {
             CError.buildErr(this,"提交数据失败！");
             return false;
         }
         return true;
    }
    public VData getResult()
    {
        return mResult;
    }
}

