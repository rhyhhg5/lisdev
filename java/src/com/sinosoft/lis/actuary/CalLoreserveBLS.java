package com.sinosoft.lis.actuary;
import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统投保暂交费功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author YT
 * @version 1.0
 */

public class CalLoreserveBLS
{
  private VData mInputData ;
  public  CErrors mErrors = new CErrors();
  public CalLoreserveBLS() {}
  public static void main(String[] args)
  {
  }
  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData=(VData)cInputData.clone() ;
    System.out.println("Start Register BLS Submit...");
    if (!this.saveData())
      return false;
    System.out.println("End Register BLS Submit...");
    mInputData=null;
    return true;
  }

  private boolean saveData()
  {
    LOReserveSet tLOReserveSet = new LOReserveSet();
    tLOReserveSet=(LOReserveSet)mInputData.getObjectByObjectName("LOReserveSet",0);
    Connection conn = DBConnPool.getConnection();
    if (conn==null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "RegisterBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    try
    {
      conn.setAutoCommit(false);
      LOReserveDBSet tLOReserveDBSet = new LOReserveDBSet();
      tLOReserveDBSet.set(tLOReserveSet);
      if (tLOReserveDBSet.insert() == false)
			{
				// @@错误处理
			  this.mErrors.copyAllErrors(tLOReserveDBSet.mErrors);
				CError tError = new CError();
				tError.moduleName = "CaseBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "保存数据失败!";
				this.mErrors .addOneError(tError) ;
	       conn.rollback() ;
	       conn.close();
				return false;
			}
			conn.commit() ;
			conn.close();
		} // end of try
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "RegisterBLS";
			tError.functionName = "submitData";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);
			try
			{
				conn.rollback() ;
				conn.close();
			}
			 catch(Exception e)
			 {
			}
			return false;
		}
    return true;
  }
}