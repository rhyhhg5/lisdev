/*
 * <p>ClassName: OLOGrpPolDataBL </p>
 * <p>Description: OLOGrpPolDataBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database:
 * @CreateDate：2003-12-17 15:01:33
 */
package com.sinosoft.lis.actuary;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class OLOGrpPolDataBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
/** 业务处理相关变量 */
private LOGrpPolDataSchema mLOGrpPolDataSchema=new LOGrpPolDataSchema();
//private LOGrpPolDataSet mLOGrpPolDataSet=new LOGrpPolDataSet();
public OLOGrpPolDataBL() {
}
public static void main(String[] args) {
}
/**
* 传输数据的公共方法
	* @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "OLOGrpPolDataBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败OLOGrpPolDataBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
      System.out.println("Start OLOGrpPolDataBL Submit...");
      OLOGrpPolDataBLS tOLOGrpPolDataBLS=new OLOGrpPolDataBLS();
      tOLOGrpPolDataBLS.submitData(mInputData,mOperate);
      System.out.println("End OLOGrpPolDataBL Submit...");
      //如果有需要处理的错误，则返回
      if (tOLOGrpPolDataBLS.mErrors.needDealError())
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tOLOGrpPolDataBLS.mErrors);
        CError tError = new CError();
        tError.moduleName = "OLOGrpPolDataBL";
        tError.functionName = "submitDat";
        tError.errorMessage ="数据提交失败!";
        this.mErrors .addOneError(tError) ;
        return false;
      }
    }
    mInputData=null;
    return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
  if (this.mOperate!="QUERY||MAIN")
  {
    if (mLOGrpPolDataSchema.getPolType()==null)
    {
      CError tError =new CError();
      tError.moduleName="LOGrpPolDataBL";
      tError.functionName="prepareData";
      tError.errorMessage="类型不能为空。";
      this.mErrors.addOneError(tError) ;
      return false;
    }
    if (this.mOperate.equals("INSERT||MAIN"))
    {
    mLOGrpPolDataSchema.setMakeDate(PubFun.getCurrentDate());
    mLOGrpPolDataSchema.setMakeTime(PubFun.getCurrentTime());
    }
    else
    {
      LOGrpPolDataDB tLOGrpPolDataDB=new LOGrpPolDataDB();
      tLOGrpPolDataDB.setSchema(mLOGrpPolDataSchema);
      if (tLOGrpPolDataDB.getInfo())
      {
        mLOGrpPolDataSchema.setMakeDate(tLOGrpPolDataDB.getMakeDate());
        mLOGrpPolDataSchema.setMakeTime(tLOGrpPolDataDB.getMakeTime());
      }
      else
      {
        CError tError =new CError();
        tError.moduleName="LOGrpPolDataBL";
        tError.functionName="dealData";
        tError.errorMessage="没有查询到原来的数据。";
        this.mErrors.addOneError(tError) ;
        return false;
      }
    }
    mLOGrpPolDataSchema.setOperator(mGlobalInput.Operator);
    mLOGrpPolDataSchema.setModifyDate(PubFun.getCurrentDate());
    mLOGrpPolDataSchema.setModifyTime(PubFun.getCurrentTime());
  }
  return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
         this.mLOGrpPolDataSchema.setSchema((LOGrpPolDataSchema)cInputData.getObjectByObjectName("LOGrpPolDataSchema",0));
         this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
    this.mResult.clear();
    LOGrpPolDataDB tLOGrpPolDataDB=new LOGrpPolDataDB();
    tLOGrpPolDataDB.setSchema(this.mLOGrpPolDataSchema);
		//如果有需要处理的错误，则返回
		if (tLOGrpPolDataDB.mErrors.needDealError())
 		{
		  // @@错误处理
 			this.mErrors.copyAllErrors(tLOGrpPolDataDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "LOGrpPolDataBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors .addOneError(tError) ;
 			return false;
    }
    mInputData=null;
    return true;
}
 private boolean prepareOutputData()
 {
   try
	{
		this.mInputData.clear();
		this.mInputData.add(this.mLOGrpPolDataSchema);
		mResult.clear();
    mResult.add(this.mLOGrpPolDataSchema);
	}
	catch(Exception ex)
	{
 		// @@错误处理
		CError tError =new CError();
 		tError.moduleName="LOGrpPolDataBL";
 		tError.functionName="prepareData";
 		tError.errorMessage="在准备往后层处理所需要的数据时出错。";
 		this.mErrors .addOneError(tError) ;
		return false;
	}
	return true;
	}
	public VData getResult()
	{
  	return this.mResult;
	}
}
