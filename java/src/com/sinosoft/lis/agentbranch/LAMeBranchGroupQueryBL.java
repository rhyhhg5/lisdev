package com.sinosoft.lis.agentbranch;
	/**
	 * <p>Title: AgentSystem</p>
	 *
	 * <p>Description: </p>
	 *
	 * <p>Copyright: Copyright (c) 2005</p>
	 *
	 * <p>Company: Sinosoft</p>
	 *
	 * @author
	 * @version 1.0
	 */
	import com.sinosoft.utility.*;
	import com.sinosoft.lis.pubfun.*;
	import com.sinosoft.lis.vschema.*;
	import com.sinosoft.lis.db.*;
	import com.sinosoft.lis.schema.*;
	import java.io.FileWriter;
	import java.io.*;
	import java.text.DecimalFormat;
	import java.sql.ResultSet;
	import java.sql.Connection;
	import java.sql.PreparedStatement;

	public class LAMeBranchGroupQueryBL
	{
	    /**错误信息容器*/
	    public CErrors mErrors = new CErrors();

	    private GlobalInput mGI = null;
	    private String mSql = null;
	    private String mStartMonth = null;
	    private String mEndMonth = null;
	    private String mOutXmlPath = null;
	    //private String mtype = null;

	    private String mCurDate = PubFun.getCurrentDate();
	    private String mCurTime = PubFun.getCurrentTime();
	    SSRS tSSRS=new SSRS();
	    public LAMeBranchGroupQueryBL()
	    {
	      }
	    public boolean submitData(VData cInputData, String operate)
	    {

	        if(!getInputData(cInputData))
	        {
	            return false;
	        }

	        if(!checkData())
	        {
	            return false;
	        }

	        if(!dealData())
	        {
	            return false;
	        }

	        return true;
	    }


	    /**
	     * 校验操作是否合法
	     * @return boolean
	     */
	    private boolean checkData()
	    {

	        return true;
	    }

	    /**
	     * dealData
	     * 处理业务数据
	     *
	     * @return boolean：true提交成功, false提交失败
	     */
	    private boolean dealData()
	    {
	        ExeSQL tExeSQL = new ExeSQL();
	        System.out.println("BL->dealDate()");
	        System.out.println(mSql);
	        System.out.println(mOutXmlPath);
	        tSSRS = tExeSQL.execSQL(mSql);

	        if(tExeSQL.mErrors.needDealError())
	        {
	            System.out.println(tExeSQL.mErrors.getErrContent());

	            CError tError = new CError();
	            tError.moduleName = "LABranchGroupQueryBL";
	            tError.functionName = "dealData";
	            tError.errorMessage = "没有查询到需要下载的数据";
	            mErrors.addOneError(tError);
	            System.out.println(tError.errorMessage);
	            return false;
	        }
	       String[][] mToExcel = new String[tSSRS.getMaxRow() + 4][30];
	        mToExcel[0][0] = "团险中介团队报表";
	        
	        mToExcel[1][0] = "销售团队代码";
	        mToExcel[1][1] = "销售团队名称";
	        mToExcel[1][2] = "管理机构";
	        mToExcel[1][3] = "管理机构名称";
	        mToExcel[1][4] = "主管代码";        
	        mToExcel[1][5] = "主管姓名";
	        mToExcel[1][6] = "成立日期";
	        mToExcel[1][7] = "停业";
//	        mToExcel[1][8] = "停业";
//	        mToExcel[1][9] = "销售团队内部代码";
//	        mToExcel[1][10] = "团队托管状态";


	        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
	        {
	            for(int col = 1; col <= tSSRS.getMaxCol()-1; col++)
	            {
	                mToExcel[row +1][col - 1] = tSSRS.GetText(row, col);
	            }
	        }

	        try
	        {
	            WriteToExcel t = new WriteToExcel("");
	            t.createExcelFile();
	            String[] sheetName ={PubFun.getCurrentDate()};
	            t.addSheet(sheetName);
	            t.setData(0, mToExcel);
	            t.write(mOutXmlPath);
	        }
	        catch(Exception ex)
	        {
	            ex.toString();
	            ex.printStackTrace();
	        }

	        return true;
	    }

	    /**
	     * getInputData
	     *将外部传入的数据分解到本类的属性中
	     * @param cInputData VData：submitData中传入的VData对象
	     * @return boolean：true提交成功, false提交失败
	     */
	    private boolean getInputData(VData data)
	    {
	        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
	        TransferData tf = (TransferData) data
	                          .getObjectByObjectName("TransferData", 0);

	        if(mGI == null || tf == null)
	        {
	            CError tError = new CError();
	            tError.moduleName = "LABranchGroupQueryBL";
	            tError.functionName = "getInputData";
	            tError.errorMessage = "传入的信息不完整";
	            mErrors.addOneError(tError);
	            System.out.println(tError.errorMessage);
	            return false;
	        }

	        mSql = (String) tf.getValueByName("querySQL");
	        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
	       //mtype = (String) tf.getValueByName("Type");
	        if(mSql == null || mOutXmlPath == null)
	        {
	            CError tError = new CError();
	            tError.moduleName = "LABranchGroupQueryBL";
	            tError.functionName = "getInputData";
	            tError.errorMessage = "传入的信息不完整2";
	            mErrors.addOneError(tError);
	            System.out.println(tError.errorMessage);
	            return false;
	        }

	        return true;
	    }

	  
//
//	    /**
//	    * 执行SQL文查询结果
//	    * @param sql String
//	    * @return double
//	    */
//	   private double execQuery(String sql)
//
//	   {
//	      Connection conn;
//	      conn = null;
//	      conn = DBConnPool.getConnection();
//
//	      System.out.println(sql);
//
//	      PreparedStatement st = null;
//	      ResultSet rs = null;
//	      try {
//	          if (conn == null)return 0.00;
//	          st = conn.prepareStatement(sql);
//	          if (st == null)return 0.00;
//	          rs = st.executeQuery();
//	          if (rs.next()) {
//	              return rs.getDouble(1);
//	          }
//	          return 0.00;
//	      } catch (Exception ex) {
//	          ex.printStackTrace();
//	          return -1;
//	      } finally {
//	          try {
//	             if (!conn.isClosed()) {
//	                 conn.close();
//	             }
//	             try {
//	                 st.close();
//	                 rs.close();
//	             } catch (Exception ex2) {
//	                 ex2.printStackTrace();
//	             }
//	             st = null;
//	             rs = null;
//	             conn = null;
//	           } catch (Exception e) {}
//
//	      }
//	     }

	    public static void main(String[] args)
	    {
	        LAMeBranchGroupQueryBL LABranchGroupQueryBL = new
	            LAMeBranchGroupQueryBL();
	    System.out.println("11111111");
	    }
	}

