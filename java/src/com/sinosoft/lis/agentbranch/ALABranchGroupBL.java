/*
 * <p>ClassName: ALABranchGroupBL </p>
 * <p>Description: ALABranchGroupBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-09
 */
package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.db.LABranchGroupBDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LABranchLevelDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LABranchLevelSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ALABranchGroupBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();


    /** 往后面传输数据的容器 */
    private VData mInputData;


    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();


    /** 数据操作字符串 */
    private String mOperate;
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String mCostCenter="";

    /** 业务处理相关变量 */
    private LABranchGroupSchema mLABranchGroupSchema = new LABranchGroupSchema();
    private LABranchGroupBSchema mLABranchGroupBSchema = new
            LABranchGroupBSchema();
    private LATreeBSchema mLATreeBSchema = new LATreeBSchema();
    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();

    public ALABranchGroupBL()
    {
    }

    public static void main(String[] args)
    {
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        //System.out.println("BLGETinputdata:here");
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!check())
        {
            return false;
        }
        //System.out.println("BLdealdata:here");
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALABranchGroupBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALABranchGroupBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
       // System.out.println("BLprepareoutputdata:here");
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            //System.out.println("Start ALABranchGroupBL Submit...");
            ALABranchGroupBLS tALABranchGroupBLS = new ALABranchGroupBLS();
            tALABranchGroupBLS.submitData(mInputData, cOperate);
           // System.out.println("End ALABranchGroupBL Submit...");
            //如果有需要处理的错误，则返回
            if (tALABranchGroupBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tALABranchGroupBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALABranchGroupBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }
   private boolean check()
   {
       String tComStyle="";
       String tAreaType="";
//       String temptemp="";
//       temptemp="wwwwwwwww";
//       System.out.println(",,,,,,,,,,,,,,"+temptemp);
       //System.out.println("BLincheck:here");
      // System.out.println("BLincheck:managecom:here"+mLABranchGroupSchema.getManageCom());
       tAreaType= AgentPubFun.getAreaType(mLABranchGroupSchema.getManageCom().substring(0, 4),"02");
      // System.out.println("BL:checktAreaType:here   "+tAreaType);
       tComStyle=mLABranchGroupSchema.getComStyle()==null?"":mLABranchGroupSchema.getComStyle();
      // System.out.println("BL:checktComStyle:here   "+tComStyle);
       if(tComStyle.equals(""))
       {
           //return true ;
       }else if(tComStyle.compareTo(tAreaType)<0)
       {
           CError tError = new CError();
           tError.moduleName = "ALABranchGroupBL";
           tError.functionName = "dealData()";
           tError.errorMessage = "营业部的机构地区类别不得高于其管理机构的机构地区类别!";
           this.mErrors.addOneError(tError);
           return false;
       }
       /*
        * miaoxz adds here in 2008-9-9
        * 在保存和修改的时候新增成本中心系统唯一性校验
        * 校验规则：如果是保存，不能和系统中没有停业的团队的成本中心重复；
        * 如果是修改，如果没有修改成本中心的值，不做校验，这里主要是兼容以前系统可能重复的老数据。
        * 如果修改了成本中心，校验方法同保存。
        * 校验范围：整个核心业务系统的所有团队，即个、团、银、中介、交叉销售
        */
       if(this.mLABranchGroupSchema.getBranchType().equals("2")
       		&&(this.mLABranchGroupSchema.getBranchType2().equals("02")
    				||this.mLABranchGroupSchema.getBranchType2().equals("04"))){
       
    	   if(this.mOperate==null){
        	   CError tError = new CError();
               tError.moduleName = "ALABranchGroupBL";
               tError.functionName = "check()";
               tError.errorMessage = "没有得到操作类型！不能确定该操作是修改或保存。";
               this.mErrors.addOneError(tError);
               return false;
           }
    	   if(this.mOperate.equals("INSERT||MAIN")){
        	   String tSql="select distinct 'existing' from labranchgroup where endflag='N' and costcenter='"
        		   +this.mLABranchGroupSchema.getCostCenter()+"' with ur";
        	   ExeSQL tExeSQL=new ExeSQL();
        	   String tResult=tExeSQL.getOneValue(tSql);
        	   if(tResult.equals("existing")){
        		   CError tError = new CError();
                   tError.moduleName = "ALABranchGroupBL";
                   tError.functionName = "check()";
                   tError.errorMessage = "该成本中心编码已在系统中存在，不能重复录入。";
                   this.mErrors.addOneError(tError);
                   return false;
        	   }else{
        		   return true;
        	   }
           }else if(this.mOperate.equals("UPDATE||MAIN")){
        	   if(this.mCostCenter.equals(this.mLABranchGroupSchema.getCostCenter())){
        		   return true;
        	   }else{
        		   String tSql="select distinct 'existing' from labranchgroup where endflag='N' and costcenter='"
            		   +this.mLABranchGroupSchema.getCostCenter()+"' with ur";
            	   ExeSQL tExeSQL=new ExeSQL();
            	   String tResult=tExeSQL.getOneValue(tSql);
            	   if(tResult.equals("existing")){
            		   CError tError = new CError();
                       tError.moduleName = "ALABranchGroupBL";
                       tError.functionName = "check()";
                       tError.errorMessage = "该成本中心编码已在系统中存在，不能重复录入。";
                       this.mErrors.addOneError(tError);
                       return false;
            	   }else{
            		   return true;
            	   }
        	   }
        	   
           }else{
        	   CError tError = new CError();
               tError.moduleName = "ALABranchGroupBL";
               tError.functionName = "check()";
               tError.errorMessage = "不可知的错误的操作类型。";
               this.mErrors.addOneError(tError);
               return false;
           }
       }else{
    	   return true;
       }
       
   }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String tAgentGroup = "";
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            System.out.println("进入新增模块！！");
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setBranchAttr(this.mLABranchGroupSchema.
                                           getBranchAttr().
                                           trim());
          //  System.out.println("BranchType in BL:"+mLABranchGroupSchema.getBranchType() ) ;
            /*xjh Modify , 2005/2/24 团队是否已经存在,外部编码可能因个团险区分而不唯一*/
            tLABranchGroupDB.setBranchType(this.mLABranchGroupSchema.
                                           getBranchType().trim());
            tLABranchGroupDB.setBranchType2(this.mLABranchGroupSchema.
                                           getBranchType2().trim());
            LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.query();
            if (tLABranchGroupSet.size() != 0)
            {
                CError tError = new CError();
                tError.moduleName = "ALABranchGroupBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "机构表中已经存在该管理机构的信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LABranchGroupBDB tLABranchGroupBDB = new LABranchGroupBDB();
            tLABranchGroupBDB.setBranchAttr(this.mLABranchGroupSchema.
                                           getBranchAttr().trim());
            /*xjh Modify , 2005/2/24 团队是否已经存在,外部编码可能因个团险区分而不唯一*/
            tLABranchGroupBDB.setBranchType(this.mLABranchGroupSchema.
                                            getBranchType().trim());
            tLABranchGroupBDB.setBranchType2(this.mLABranchGroupSchema.
                                            getBranchType2().trim());
            LABranchGroupBSet tLABranchGroupBSet = tLABranchGroupBDB.query();
            if (tLABranchGroupBSet.size() != 0)
            {
                CError tError = new CError();
                tError.moduleName = "ALABranchGroupBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "机构备份表中已经存在该管理机构的信息!";
                this.mErrors.addOneError(tError);
                return false;
            }

            /*xjh Add , 2005/2/18 团队长度是否符合要求*/
            //查询获得团队级别对应的
            LABranchLevelSchema tLABranchLevelSchema = new LABranchLevelSchema();
            LABranchLevelDB tLABranchLevelDB = new LABranchLevelDB();
            tLABranchLevelDB.setBranchLevelCode(this.mLABranchGroupSchema.
                                                getBranchLevel());
            tLABranchLevelDB.setBranchType(this.mLABranchGroupSchema.getBranchType()) ;
            tLABranchLevelDB.setBranchType2(this.mLABranchGroupSchema.getBranchType2()) ;
            //System.out.println("BL:branchtype2"+this.mLABranchGroupSchema.getBranchType2());
            if (!tLABranchLevelDB.getInfo())
            { 
            	System.out.println("error here!");
                CError tError = new CError();
                tError.moduleName = "ALABranchGroupBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询LABranchLevel纪录出错！";
                this.mErrors.addOneError(tError);
                return false;
            }
            //长度是否符合要求
            tLABranchLevelSchema.setSchema(tLABranchLevelDB.getSchema());
            String len = tLABranchLevelSchema.getBranchLevelProperty2().trim();
            int ilen = Integer.parseInt(len);
           // System.out.println(ilen);
           // System.out.println(this.mLABranchGroupSchema.getBranchAttr().length());
            if (this.mLABranchGroupSchema.getBranchAttr().length() != ilen)
            {
                CError tError = new CError();
                tError.moduleName = "ALABranchGroupBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "该机构的编码长度不符合要求!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //获得BranchLevelType并设置BranchJobType，表示内外勤
            this.mLABranchGroupSchema.setBranchJobType(tLABranchLevelSchema.
                    getBranchLevelType());



            //校验管理机构和展业机构代码的前几位是否一致或其他方式较检
            PubCheckField checkField1 = new PubCheckField();
            VData cInputData = new VData();
            TransferData tTransferData = new TransferData();
            //设置计算时要用到的参数值
            String tManageCom = mLABranchGroupSchema.getManageCom();
            String tBranchAttr = mLABranchGroupSchema.getBranchAttr();
            String tBranchType = mLABranchGroupSchema.getBranchType();
            String tBranchType2 = mLABranchGroupSchema.getBranchType2();
            tTransferData = new TransferData();
            tTransferData.setNameAndValue("BranchType", tBranchType);
            tTransferData.setNameAndValue("BranchType2", tBranchType2);
            tTransferData.setNameAndValue("BranchAttr", tBranchAttr);
            tTransferData.setNameAndValue("ManageCom", tManageCom);
            LMCheckFieldDB tLMCheckFieldDB = new LMCheckFieldDB();
            LMCheckFieldSet tLMCheckFieldSet = new LMCheckFieldSet();
            String tSql =
                    "select * from lmcheckfield where riskcode = '000000'"
                    +
                    "  and fieldname = 'ALABranchGroupBL' order by serialno asc";
            tLMCheckFieldSet = tLMCheckFieldDB.executeQuery(tSql);
            cInputData.add(tTransferData);
            cInputData.add(tLMCheckFieldSet);
            if (!checkField1.submitData(cInputData, "CKBYSET"))
            {
                System.out.println("Enter Error Field!");
                //此判断是用来区分是程序处理过程中报的错误，还是校验时报的错误
                if (checkField1.mErrors.needDealError())
                {
                    this.mErrors.copyAllErrors(checkField1.mErrors);
                    return false;
                }
                else
                {
                    VData t = checkField1.getResultMess();
                    CError tError = new CError();
                    tError.moduleName = "ALABranchGroupBL";
                    tError.functionName = "dealData()";
                    tError.errorMessage = t.get(0).toString();
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }

            //生成内部机构代码
            tAgentGroup = PubFun1.CreateMaxNo("AgentGroup", 12);
            System.out.println("AgentGroup:" + tAgentGroup);
            this.mLABranchGroupSchema.setAgentGroup(tAgentGroup);

            if (!setUpBranch())
            {
                return false;
            }
            String tUpBranch = mLABranchGroupSchema.getUpBranch();
            if (tUpBranch == null || tUpBranch.equals(""))
            {
                tUpBranch = "";
            }
            //xjh Add 2005/03/16 判断直辖属性
            if (!isDirect())
            {
                return false;
            }
            this.mLABranchGroupSchema.setOperator(mGlobalInput.Operator);
            this.mLABranchGroupSchema.setMakeDate(currentDate);
            this.mLABranchGroupSchema.setMakeTime(currentTime);
            this.mLABranchGroupSchema.setModifyDate(currentDate);
            this.mLABranchGroupSchema.setModifyTime(currentTime);

        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            System.out.println("进入修改模块！");
            if (this.mLABranchGroupSchema.getAgentGroup().equals(""))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ALABranchGroupBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "不存在所操作的销售单位!(隐式机构代码为空)";
                this.mErrors.addOneError(tError);
                return false;
            }
            //校验管理机构和展业机构代码的前八位是否一致
            PubCheckField checkField1 = new PubCheckField();
            VData cInputData = new VData();
            TransferData tTransferData = new TransferData();
            //设置计算时要用到的参数值
            String tManageCom = mLABranchGroupSchema.getManageCom();
            String tBranchAttr = mLABranchGroupSchema.getBranchAttr();
            String tBranchType = mLABranchGroupSchema.getBranchType();
            String tBranchType2 = mLABranchGroupSchema.getBranchType2();
            tTransferData = new TransferData();
            tTransferData.setNameAndValue("BranchAttr", tBranchAttr);
            tTransferData.setNameAndValue("ManageCom", tManageCom);
            tTransferData.setNameAndValue("BranchType", tBranchType);
            tTransferData.setNameAndValue("BranchType2", tBranchType2);
            LMCheckFieldDB tLMCheckFieldDB = new LMCheckFieldDB();
            LMCheckFieldSet tLMCheckFieldSet = new LMCheckFieldSet();
            String tSql =
                    "select * from lmcheckfield where riskcode = '000000'"
                    +
                    "  and fieldname = 'ALABranchGroupBL' order by serialno asc";
            tLMCheckFieldSet = tLMCheckFieldDB.executeQuery(tSql);
            cInputData.add(tTransferData);
            cInputData.add(tLMCheckFieldSet);
            if (!checkField1.submitData(cInputData, "CKBYSET"))
            {
                System.out.println("Enter Error Field!");
                //此判断是用来区分是程序处理过程中报的错误，还是校验时报的错误
                if (checkField1.mErrors.needDealError())
                {
                    this.mErrors.copyAllErrors(checkField1.mErrors);
                    return false;
                }
                else
                {
                    VData t = checkField1.getResultMess();
                    CError tError = new CError();
                    tError.moduleName = "ALABranchGroupBL";
                    tError.functionName = "dealData()";
                    tError.errorMessage = t.get(0).toString();
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }

            //修改：2004-09-14 蔡刚
            //如果置机构停业，则停业日期不能为空
            if (this.mLABranchGroupSchema.getEndFlag().equals("Y") &&
                (this.mLABranchGroupSchema.getEndDate() == null ||
                 this.mLABranchGroupSchema.getEndDate().trim().equals("")))
            {
                CError tError = new CError();
                tError.moduleName = "ALABranchGroupBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "机构置停业时停业日期不能为空!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (this.mLABranchGroupSchema.getEndDate() != null &&
                this.mLABranchGroupSchema.getBranchType().equals("1"))
            {
                if (this.mLABranchGroupSchema.getEndFlag() == null ||
                    this.mLABranchGroupSchema.getEndFlag().equals("N"))
                {
                    CError tError = new CError();
                    tError.moduleName = "ALABranchGroupBL";
                    tError.functionName = "dealData()";
                    tError.errorMessage = "机构未停业，停业日期必须为空!";
                    this.mErrors.addOneError(tError);
                    return false;

                }
            }

            //修改：2004-06-07 LL
            //增加机构停业的校验：只有机构下没有人才能停业
            //只处理个人情况：BranchType = 1的情况
            //xjh Modify 2005/03/15
            if (this.mLABranchGroupSchema.getEndFlag().equals("Y")
                    //&& this.mLABranchGroupSchema.getBranchType().equals("1")
                    )
            {

                System.out.println("BranchAttr:" +
                                   this.mLABranchGroupSchema.getBranchAttr());
                System.out.println("AgentGroup:" +
                                   this.mLABranchGroupSchema.getAgentGroup());
                System.out.println("BranchType:" +
                                   this.mLABranchGroupSchema.getBranchType());
                String tFlag1 = this.haveMember(this.mLABranchGroupSchema.
                                                getAgentGroup().trim(),
                                                this.mLABranchGroupSchema.
                                                getBranchType().trim());
                //查询信息时出错
                if (tFlag1.equals("0"))
                {
                    CError tError = new CError();
                    tError.moduleName = "ALABranchGroupBL";
                    tError.functionName = "dealData()";
                    tError.errorMessage = "查询机构信息信息出错！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                //机构下有未离职人员
                if (tFlag1.equals("2"))
                {
                    CError tError = new CError();
                    tError.moduleName = "ALABranchGroupBL";
                    tError.functionName = "dealData()";
                    tError.errorMessage = "此机构下还有未离职人员，不能对此机构停业！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }

            //获取‘入机日期和时间'
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(this.mLABranchGroupSchema.
                                           getAgentGroup());
            if (!tLABranchGroupDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALABranchGroupBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "不存在所要修改的机构!";
                this.mErrors.addOneError(tError);
                return false;
            }

            LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
            tLABranchGroupSchema = tLABranchGroupDB.getSchema();
            //备份操作
//      LABranchGroupBSchema tLABranchGroupBSchema=new LABranchGroupBSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(mLABranchGroupBSchema,
                                     tLABranchGroupSchema);
            String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            //蔡刚2004-08-18添加，机构主管从库中取

            mLABranchGroupBSchema.setEdorNo(tEdorNo);
            mLABranchGroupBSchema.setEdorType("02"); //修改操作
            mLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.getMakeDate());
            mLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.getMakeTime());
            mLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.
                                                 getModifyDate());
            mLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.
                                                 getModifyTime());
            mLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.getOperator());
            mLABranchGroupBSchema.setMakeDate(currentDate);
            mLABranchGroupBSchema.setMakeTime(currentTime);
            mLABranchGroupBSchema.setModifyDate(currentDate);
            mLABranchGroupBSchema.setModifyTime(currentTime);
            mLABranchGroupBSchema.setOperator(this.mGlobalInput.Operator);


            if (mLABranchGroupSchema.getBranchAttr() == null ||
                mLABranchGroupSchema.getBranchAttr().equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "ALABranchGroupBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "修改操作不允许将机构编码置空！";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (!mLABranchGroupSchema.getBranchAttr().equals(tLABranchGroupSchema.getBranchAttr() ))
            {
                CError tError = new CError();
                tError.moduleName = "ALABranchGroupBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "修改操作不允许将修改机构编码！";
                this.mErrors.addOneError(tError);
                return false;
            }

            //添加一个展业机构系列号，更新的时候自动带出，没必要重新计算
            this.mLABranchGroupSchema.setBranchSeries(tLABranchGroupSchema.
                    getBranchSeries());
            this.mLABranchGroupSchema.setBranchManager(tLABranchGroupSchema.
                    getBranchManager());
            this.mLABranchGroupSchema.setBranchManagerName(tLABranchGroupSchema.
                    getBranchManagerName());
            //xjh Add 2005/03/16
            this.mLABranchGroupSchema.setUpBranch(tLABranchGroupSchema.
                                                  getUpBranch());
            //判断直辖属性
            if (!isDirect())
            {
                return false;
            }
            this.mLABranchGroupSchema.setMakeDate(tLABranchGroupDB.getMakeDate());
            this.mLABranchGroupSchema.setMakeTime(tLABranchGroupDB.getMakeTime());
            this.mLABranchGroupSchema.setModifyDate(currentDate);
            this.mLABranchGroupSchema.setModifyTime(currentTime);
            this.mLABranchGroupSchema.setOperator(mGlobalInput.Operator);
        }

        tReturn = true;
        return tReturn;
    }

    /**
     * isDirect
     * 判断直辖关系 xjh Add 2005/02/16
     * @return boolean
     */
    private boolean isDirect()
    {
        //如果上级机构不为空，则必须置直辖关系
        //如果上级机构为空，UpBranchAttr为空或为0
        String tUpBranch = mLABranchGroupSchema.getUpBranch();
        String tUpBranchAttr = mLABranchGroupSchema.getUpBranchAttr();
        String tAgentGroup = mLABranchGroupSchema.getAgentGroup();
        System.out.println("4442222222222" + tUpBranchAttr);
        if (tUpBranch == null)
        {
            tUpBranch = "";
        }
        if (tUpBranchAttr == null)
        {
            tUpBranchAttr = "0";
            mLABranchGroupSchema.setUpBranchAttr("0");
        }
        if (tUpBranch.equals(""))
        {
            if (!tUpBranchAttr.equals("") &&
                !tUpBranchAttr.trim().equals("0"))
            {
                CError tError = new CError();
                tError.moduleName = "ALABranchGroupBL";
                tError.functionName = "isDirect";
                tError.errorMessage = "与上级直辖属性错误";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        else
        {
            //如果为有直辖关系，则上级机构是否已经有直辖下级机构，一个机构只能有一个直辖下级
            if (tUpBranchAttr.trim().equals("1"))
            {
                String tSQL1 =
                        "select distinct agentgroup from LABranchGroup where UpBranch = '"
                        + tUpBranch.trim() + "' and UpBranchAttr = '1'  and " +
                        "(EndFlag is null or EndFlag <> 'Y') and BranchType='"+mLABranchGroupSchema.getBranchType()
                        +"' and BranchType2='"+mLABranchGroupSchema.getBranchType2()+"'";
                ExeSQL tExeSQL1 = new ExeSQL();
                String tCount = tExeSQL1.getOneValue(tSQL1).trim();
                if (tCount == null || tCount.equals(""))
                {
                    return true;
                }
                if(!tCount.trim().equals(mLABranchGroupSchema.getAgentGroup()))
                {
                    CError tError = new CError();
                    tError.moduleName = "ALABranchGroupBL";
                    tError.functionName = "isDirect";
                    tError.errorMessage = "上级机构已经有直辖下级机构";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
        }
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLABranchGroupSchema.setSchema((LABranchGroupSchema) cInputData.
                                            getObjectByObjectName(
                "LABranchGroupSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        if(this.mLABranchGroupSchema.getBranchType().equals("2")
        		&&(this.mLABranchGroupSchema.getBranchType2().equals("02")
        				||this.mLABranchGroupSchema.getBranchType2().equals("04"))){
        	this.mCostCenter=(String)cInputData.getObject(2);
        }
        if (this.mGlobalInput == null)
        {
            CError tError = new CError();
            tError.moduleName = "ALABranchGroupBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "mGlobalInput为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (this.mLABranchGroupSchema == null)
        {
            CError tError = new CError();
            tError.moduleName = "ALABranchGroupBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "mLABranchGroupSchema为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start ALABranchGroupBLQuery Submit...");
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setSchema(this.mLABranchGroupSchema);
        this.mLABranchGroupSet = tLABranchGroupDB.query();
        this.mResult.add(this.mLABranchGroupSet);
        System.out.println("End ALABranchGroupBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLABranchGroupDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALABranchGroupBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLABranchGroupSchema);
            this.mInputData.add(this.mLABranchGroupBSchema);
            //this.mInputData.add(this.mLATreeSchema);
            //this.mInputData.add(this.mLATreeBSchema);
            //this.mInputData.add(this.mLAAgentSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALABranchGroupBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

//    public boolean prepareCopyTree(LATreeSchema cLATreeSchema)
//    {
//        String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
//        try
//        {
//            this.mLATreeBSchema.setAgentCode(cLATreeSchema.getAgentCode());
//            this.mLATreeBSchema.setAgentGrade(cLATreeSchema.getAgentGrade());
//            this.mLATreeBSchema.setAgentGroup(cLATreeSchema.getAgentGroup());
//            this.mLATreeBSchema.setAgentLastGrade(cLATreeSchema.
//                                                  getAgentLastGrade());
//            this.mLATreeBSchema.setAgentLastSeries(cLATreeSchema.
//                    getAgentLastSeries());
//            this.mLATreeBSchema.setAgentSeries(cLATreeSchema.getAgentSeries());
//            this.mLATreeBSchema.setAssessType(cLATreeSchema.getAssessType());
//            this.mLATreeBSchema.setAstartDate(cLATreeSchema.getAstartDate());
//            this.mLATreeBSchema.setEdorNO(tEdorNo);
//            this.mLATreeBSchema.setEduManager(cLATreeSchema.getEduManager());
//            this.mLATreeBSchema.setIntroAgency(cLATreeSchema.getIntroAgency());
//            this.mLATreeBSchema.setIntroBreakFlag(cLATreeSchema.
//                                                  getIntroBreakFlag());
//            this.mLATreeBSchema.setIntroCommEnd(cLATreeSchema.getIntroCommEnd());
//            this.mLATreeBSchema.setIntroCommStart(cLATreeSchema.
//                                                  getIntroCommStart());
//            this.mLATreeBSchema.setMakeDate2(cLATreeSchema.getMakeDate());
//            this.mLATreeBSchema.setMakeTime2(cLATreeSchema.getMakeTime());
//            this.mLATreeBSchema.setManageCom(cLATreeSchema.getManageCom());
//            this.mLATreeBSchema.setModifyDate2(cLATreeSchema.getModifyDate());
//            this.mLATreeBSchema.setModifyTime2(cLATreeSchema.getModifyTime());
//            this.mLATreeBSchema.setOldEndDate(cLATreeSchema.getOldEndDate());
//            this.mLATreeBSchema.setOldStartDate(cLATreeSchema.getOldStartDate());
//            this.mLATreeBSchema.setOperator2(cLATreeSchema.getOperator());
//            this.mLATreeBSchema.setOthUpAgent(cLATreeSchema.getOthUpAgent());
//            this.mLATreeBSchema.setRearBreakFlag(cLATreeSchema.getRearBreakFlag());
//            this.mLATreeBSchema.setRearCommEnd(cLATreeSchema.getRearCommEnd());
//            this.mLATreeBSchema.setRearCommStart(cLATreeSchema.getRearCommStart());
//            this.mLATreeBSchema.setRemoveType("03"); //转储类型:新增机构
//            this.mLATreeBSchema.setStartDate(cLATreeSchema.getState());
//            this.mLATreeBSchema.setState(cLATreeSchema.getState());
//            this.mLATreeBSchema.setUpAgent(cLATreeSchema.getUpAgent());
//            this.mLATreeBSchema.setOperator(this.mGlobalInput.Operator);
//            this.mLATreeBSchema.setMakeDate(currentDate);
//            this.mLATreeBSchema.setMakeTime(currentTime);
//            this.mLATreeBSchema.setModifyDate(currentDate);
//            this.mLATreeBSchema.setModifyTime(currentTime);
//        }
//        catch (Exception ex)
//        {
//            // @@错误处理
//            CError tError = new CError();
//            tError.moduleName = "AdjustAgentBL";
//            tError.functionName = "prepareCopy";
//            tError.errorMessage = "备份调动人员信息出错！";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
//        return true;
//    }

    private String getManagerGrade()
    {
        //查询LDCodeRELA表，取出该机构级别对应的职级、系列
        String tSQL =
                "select code1 from LDCodeRELA where relaType = 'gradeserieslevel' "
                + "and code3 = '" + this.mLABranchGroupSchema.getBranchLevel() +
                "' "
                + "and othersign = '1' order by code1";
        ExeSQL tExeSQL = new ExeSQL();
        String tGrade = tExeSQL.getOneValue(tSQL);
        tGrade = tGrade.trim();
        if ((tGrade == null) || (tGrade.equals("")))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALABranchGroupBL";
            tError.functionName = "getManagerGrade";
            tError.errorMessage = "查询机构负责人对应的职级失败!";
            this.mErrors.addOneError(tError);
            return null;
        }
        System.out.println("职级：" + tGrade);
        return tGrade;
    }

    /**
     * xjh Modify 2005/03/15,较检上级机构
     * 注意当跨级时编码规则为中间级别部分填0
     * @return boolean
     */
    private boolean setUpBranch()
    {
        String tUpBranch = this.mLABranchGroupSchema.getUpBranch();
        String tCurBranch = this.mLABranchGroupSchema.getBranchAttr().trim();
        String tCurLevel = this.mLABranchGroupSchema.getBranchLevel().trim();
        String tBranchtype = this.mLABranchGroupSchema.getBranchType().trim();
        String tBranchtype2=mLABranchGroupSchema.getBranchType2().trim() ;
        /**如果是最高级，应该没有上级机构,不是最高级都有上级机构*/
        //取得当前级别对应的ID
        String tSQL0 =
                "select BranchLevelID from LABranchLevel where BranchLevelCode = '"
                + tCurLevel + "' and BranchType = '" + tBranchtype + "' and BranchType2='"+tBranchtype2+"'";
        ExeSQL tExeSQL0 = new ExeSQL();
        String tCurID = tExeSQL0.getOneValue(tSQL0).trim();
        int iCurLevel = Integer.parseInt(tCurID);
        //取得当前机构的最高级别ID
        String tSQL1 =
                "select max(BranchLevelID) from LABranchLevel where BranchType = '"
                + tBranchtype + "' and BranchType2='"+tBranchtype2+"'";
        ExeSQL tExeSQL1 = new ExeSQL();
        String tTopLevel = tExeSQL1.getOneValue(tSQL1).trim();
        int iTopLevel = Integer.parseInt(tTopLevel);
        if (iCurLevel > iTopLevel)
        { //当前级别大于最高级别时错误
            CError tError = new CError();
            tError.moduleName = "ALABranchGroupBL";
            tError.functionName = "dealData()";
            tError.errorMessage = "该机构的级别错误";
            this.mErrors.addOneError(tError);
            return false;
        }
        else if (iCurLevel == iTopLevel)
        { //相等时没有上级机构，赋空
            mLABranchGroupSchema.setBranchSeries(mLABranchGroupSchema.getAgentGroup());
            if (!(tUpBranch == null || tUpBranch.equals("")))
            {
                CError tError = new CError();
                tError.moduleName = "ALABranchGroupBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "该机构的处于最高级别,应该没有上级机构";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        else
        {
            //不是最高级应该都有上级机构
            if (tUpBranch == null || tUpBranch.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "ALABranchGroupBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "请输入本机构的上级机构";
                this.mErrors.addOneError(tError);
                return false;
            }
            //查看上级机构是否存在
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tUpBranch.trim());
            tLABranchGroupDB.setBranchType(tBranchtype);
            tLABranchGroupDB.setBranchType2(tBranchtype2);
            LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.query();
            if (tLABranchGroupSet.size() == 0)
            {
                CError tError = new CError();
                tError.moduleName = "ALABranchGroupBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "不存在该机构的上级机构信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //取得上级机构信息
            LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
            tLABranchGroupSchema = tLABranchGroupSet.get(1).getSchema();
            String tUpAgentGroup = tLABranchGroupSchema.getAgentGroup().trim();
            String tUpLevel = tLABranchGroupSchema.getBranchLevel().trim();
            String tUpManageCom = tLABranchGroupSchema.getManageCom().trim();
            String tUpBranchType = tLABranchGroupSchema.getBranchType().trim();
            String tUpBranchType2=tLABranchGroupSchema.getBranchType2().trim() ;
            //查看展业类型是否一致
            if (!tUpBranchType.equals(tBranchtype))
            {
                CError tError = new CError();
                tError.moduleName = "ALABranchGroupBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "该机构与其上级机构的展业类型不一致";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (!tUpBranchType2.equals(tBranchtype2))
            {
                CError tError = new CError();
                tError.moduleName = "ALABranchGroupBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "该机构与其上级机构的渠道不一致";
                this.mErrors.addOneError(tError);
                return false;
            }

            //查看管理机构是否一致
            if (!tUpManageCom.trim() .equals(mLABranchGroupSchema.getManageCom().trim()))
            {
                CError tError = new CError();
                tError.moduleName = "ALABranchGroupBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "该机构与其上级机构的管理机构不一致";
                this.mErrors.addOneError(tError);
                return false;
            }
            //查看上级机构的级别是否比自己高
            String tSQL2 =
                    "select BranchLevelID from LABranchLevel where BranchLevelCode = '"
                    + tUpLevel + "' and BranchType = '" + tUpBranchType + "' and branchtype2='"+tBranchtype2+"'";
            ExeSQL tExeSQL2 = new ExeSQL();
            String tUpID = tExeSQL2.getOneValue(tSQL2);
            int iUpLevel = Integer.parseInt(tUpID);
            if (iCurLevel > iUpLevel)
            {
                CError tError = new CError();
                tError.moduleName = "ALABranchGroupBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "该机构的级别应该小于其上级机构";
                this.mErrors.addOneError(tError);
                return false;
            }
            iCurLevel = iCurLevel + 1; //当前机构的上级机构ID
            String tSQL3 =
                    "select BranchLevelProperty2 from LABranchLevel where BranchLevelID = "
                    + iUpLevel + " and BranchType = '" + tBranchtype +
                    "' and BranchType2='"+tBranchtype2+"'";
            ExeSQL tExeSQL3 = new ExeSQL();
            String tUpLen = tExeSQL3.getOneValue(tSQL3);
            String tSQL4 =
                    "select BranchLevelProperty2 from LABranchLevel where BranchLevelID = "
                    + iCurLevel + " and BranchType = '" +
                    tBranchtype +
                    "' and BranchType2='"+tBranchtype2+"'";
            ExeSQL tExeSQL4 = new ExeSQL();
            String tCurLen = tExeSQL4.getOneValue(tSQL4);
            int iCurLen = Integer.parseInt(tCurLen);
            int iUpLen = Integer.parseInt(tUpLen);
            //如果是跨级建立的机构，看是否符合规则
            int ilen = iUpLevel - iCurLevel;
            //不跨级检查代码
            if (ilen == 0)
            {
                mLABranchGroupSchema.setUpBranch(tUpAgentGroup);
                String tBranchSeries="";
                tBranchSeries = AgentPubFun.getBranchSeries(tUpAgentGroup);
                if (tBranchSeries == null || tBranchSeries.equals(""))
               {
                   tBranchSeries = mLABranchGroupSchema.getAgentGroup() ;
               }
               else
               {

                   tBranchSeries+=":"+mLABranchGroupSchema.getAgentGroup() ;
               }
               mLABranchGroupSchema.setBranchSeries(tBranchSeries) ;

            }
            else if (ilen < 1)
            {
                CError tError = new CError();
                tError.moduleName = "ALABranchGroupBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "该机构的级别应该小于其上级机构";
                this.mErrors.addOneError(tError);
                return false;
            }
            else if (ilen >= 1)
            {
                //这里限制了对于跨级建立的机构时机构外部编码中间级别部分必须是0
                String Zero = "000000000000000000";
                if (!tCurBranch.substring(iUpLen,
                                          iCurLen).equals(Zero.substring(
                                                  iUpLen, iCurLen)))
                {
                    CError tError = new CError();
                    tError.moduleName = "ALABranchGroupBL";
                    tError.functionName = "dealData()";
                    tError.errorMessage = "跨级别机构编码不符规则";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                mLABranchGroupSchema.setUpBranch(tUpAgentGroup);
                String tBranchSeries = "";
                tBranchSeries = AgentPubFun.getBranchSeries(tUpAgentGroup);
                if (tBranchSeries == null || tBranchSeries.equals(""))
                {
                    tBranchSeries = mLABranchGroupSchema.getAgentGroup() ;
                }
                else
                {
                    while (ilen > 0)
                    {
                        tBranchSeries = tBranchSeries + ":" +
                                       "000000000000";
                       ilen--;
                    }
                    tBranchSeries+=":"+mLABranchGroupSchema.getAgentGroup() ;
                }
                mLABranchGroupSchema.setBranchSeries(tBranchSeries) ;

            }
        }
        return true;
    }

    /**
     * 编写日期：2004-06-07 LL //xjh Modify 2005/03/16
     * 功能说明：判断某个机构下是否存在没有离职人员
     * 返回值说明；
     *    0 - 程序出错；
     *    1 - 此机构下没有离职人员；
     *    2 - 此机构下有未离职人员；
     */
    private String haveMember(String cAgentGroup, String cBranchType)
    {

        //循环处理每一个组
        ExeSQL tExeSQL;
        String str = "";
        int tCount = 0;

        tExeSQL = new ExeSQL();
        //查找组下没离职人员
        String tSQL2 = "select count(*) from laagent where BranchCode in ("
                       +
                       " select agentgroup from labranchgroup where branchseries like '%" +
                       cAgentGroup
                       + "%' and BranchType = '" + cBranchType
                       + "' and (endflag is null or endflag <> 'Y'))"
                       + " and AgentState < '03'";
        System.out.println("组管理机构查询SQL:" + tSQL2);
        str = tExeSQL.getOneValue(tSQL2).trim();
        System.out.println("组管理机构下在职人员人数为：" + str);
        tCount = Integer.parseInt(str);
        //有人存在组中，返回
        if (tCount > 0)
        {
            return "2";
        }
        //此机构下没有离职人员
        return "1";
    }

    /**
     * 编写日期：2004-06-17 LL
     * 功能说明：判断机构代码与机构级别之间的对应关系
     * 返回值说明；
     *    0 - 程序出错；
     *    1 - 对应关系正确；
     *    2 - 对应关系错误；
     */
    private String judgeGroupLevel(String tBranchAttr, String tBranchLevel)
    {
        //获得界面录入的机构代码长度
        int len1 = tBranchAttr.trim().length();
        //获得机构级别对应的机构代码长度
        int len2 = 0;

        LABranchLevelDB tLABranchLevelDB = new LABranchLevelDB();
        tLABranchLevelDB.setBranchLevelCode(tBranchLevel);

        //校验是否出错
        if (!tLABranchLevelDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLABranchLevelDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LABranchGroupBL";
            tError.functionName = "judgeGroupLevel";
            tError.errorMessage = "查询LABranchLevel表中信息出错！";
            this.mErrors.addOneError(tError);
            System.out.println("查询LABranchLevel表中信息出错！");
            return "0";
        }
        String length = tLABranchLevelDB.getBranchLevelProperty2();
        len2 = Integer.parseInt(length);
        System.out.println("录入的机构代码长度为：" + len1);
        System.out.println("级别" + tBranchLevel + "对应的机构代码长度为：" + len2);
        //判断长度是否相等
        if (len1 == len2)
        {
            return "1";
        }
        else
        {
            return "2";
        }
    }

    /**
     * 编写日期：2004-06-17 LL
     * 功能说明：判断此机构下是否有直辖机构存在
     * 返回值说明；
     *    0 - 程序出错；
     *    1 - 有直辖机构存在；
     *    2 - 没有直辖机构存在；
     */
    private String haveZXGroup(String tUpAgentGroup, String tAgentGroup)
    {
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        String tSql = "select * from labranchgroup where UpBranch = '" +
                      tUpAgentGroup + "' "
                      + " and EndFlag = 'N' and UpBranchAttr = '1'";
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(tSql);
        //校验是否出错
        if (tLABranchGroupDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LABranchGroupBL";
            tError.functionName = "haveZXGroup";
            tError.errorMessage = "在LABranchGroup表中查上级机构信息出错！";
            this.mErrors.addOneError(tError);
            System.out.println("在LABranchGroup表中查上级机构信息出错！！");
            return "0";
        }
        System.out.println("管理机构" + tUpAgentGroup + "下直辖机构个数为：" +
                           tLABranchGroupSet.size());

        //处理有直辖机构存在的情况，这样是错误的，只可能有一个直辖机构存在
        if (tLABranchGroupSet.size() > 1)
        {
            this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LABranchGroupBL";
            tError.functionName = "haveZXGroup";
            tError.errorMessage = "有多个直辖机构存在！";
            this.mErrors.addOneError(tError);
            System.out.println("有多个直辖机构存在！！");
            return "0";
        }

        //修改：2004-06-23 LL
        //修改内容：如果要修改的机构既为直辖机构，则直接返回 - 2
        if (tLABranchGroupSet.size() == 1
            && tAgentGroup.equals(tLABranchGroupSet.get(1).getAgentGroup()))
        {
            return "2";
        }
        //判断是否有没有停业的直辖机构存在
        if (tLABranchGroupSet.size() != 0)
        {
            return "1";
        }
        else
        {
            return "2";
        }
    }
}
