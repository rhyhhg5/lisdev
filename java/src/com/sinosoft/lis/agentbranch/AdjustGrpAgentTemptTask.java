/*
 * <p>ClassName: AdjustAgentBL </p>
 * <p>Description: AdjustAgentBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-27
 */
package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeAccessoryDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeAccessorySchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LATreeAccessoryBSet;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LABranchChangeTempSchema;
import com.sinosoft.lis.vschema.LABranchChangeTempSet;
import com.sinosoft.lis.db.LABranchChangeTempDB;
import com.sinosoft.lis.schema.LATreeTempSchema;
import com.sinosoft.lis.vschema.LATreeTempSet;

public class AdjustGrpAgentTemptTask
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;
    private VData mInInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String mOperate;
    private String tEdorNo;
    private String mAimManageCom;
    private String mAStartDate;

    /** 业务处理相关变量 */
    private LABranchGroupSchema mLABranchSchema = new LABranchGroupSchema();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LACommisionSet mLACommisionSet=new LACommisionSet();
    private LABranchChangeTempSet mLABranchChangeTempSet=new LABranchChangeTempSet();
    private LABranchChangeTempSet mnewLABranchChangeTempSet=new LABranchChangeTempSet();
    private LABranchChangeTempSet mdeLABranchChangeTempSet=new LABranchChangeTempSet();
    private LABranchChangeTempSet minLABranchChangeTempSet=new LABranchChangeTempSet();

    //更新所有调动人员和管理人员在行政表中的AgentGroup UpBranch
    private LATreeSet mUpdateLATreeSet = new LATreeSet();

    //备份所有调动人员和管理人员
    private LATreeBSet mLATreeBSet = new LATreeBSet();

    //更新所有调动人员在LAAgent表中的AgentGroup
    private LAAgentSet mLAAgentSet = new LAAgentSet();

    //备份代理人记录 add by jiangcx minLABranchGroupBSet
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();
    //备份代理人记录 labranchgroupb表中的branchmanager
    private LABranchGroupBSet minLABranchGroupBSet = new LABranchGroupBSet();

    //更新所有调动人员
    private LABranchGroupSet mupLABranchGroupSet = new LABranchGroupSet();
    private MMap mMap=new MMap();

    private VData mOutputDate=new VData();

    public AdjustGrpAgentTemptTask()
    {
    }

    public static void main(String[] args)
    {
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "AdjustGrpAgentTemptTask";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        this.mInputData =(VData)cInputData.clone() ;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustGrpAgentBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败AdjustGrpAgentBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit=new PubSubmit();
        tPubSubmit.submitData(mOutputDate,mOperate) ;
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "AdjustGrpAgentBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }
    private boolean dealData()
    {
        if(mLABranchChangeTempSet!=null && mLABranchChangeTempSet.size()>=1)
        {
            for (int i = 1; i <= mLABranchChangeTempSet.size(); i++) {
                LABranchChangeTempSchema tLABranchChangeTempSchema = new
                        LABranchChangeTempSchema();
                tLABranchChangeTempSchema = mLABranchChangeTempSet.get(i);
                mLABranchSchema.setAgentGroup(tLABranchChangeTempSchema.
                                              getAgentGroup());
                LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                LABranchGroupSchema tnewLABranchGroupSchema = new
                        LABranchGroupSchema();
                tLABranchGroupDB.setAgentGroup(mLABranchSchema.getAgentGroup());
                tLABranchGroupDB.getInfo();
                tnewLABranchGroupSchema = tLABranchGroupDB.getSchema();

                mLABranchSchema.setBranchAttr(tLABranchChangeTempSchema.
                                              getAgentGroup());

                mLABranchSchema.setBranchLevel(tnewLABranchGroupSchema.
                                               getBranchLevel());
                mLABranchSchema.setBranchManager(tnewLABranchGroupSchema.
                                                 getBranchManager());
                mLABranchSchema.setUpBranch(tnewLABranchGroupSchema.getUpBranch());
                mLABranchSchema.setBranchType(tLABranchChangeTempSchema.
                                              getBranchType());
                mLABranchSchema.setBranchType2(tLABranchChangeTempSchema.
                                               getBranchType2());

                LATreeSchema tLATreeSchema = new LATreeSchema();

                LATreeSchema toldLATreeSchema = new LATreeSchema();
                LATreeDB tLATreeDB = new LATreeDB();
                tLATreeDB.setAgentCode(tLABranchChangeTempSchema.getAgentCode());
                tLATreeDB.getInfo();
                toldLATreeSchema = tLATreeDB.getSchema();
                tLATreeSchema.setAgentCode(tLABranchChangeTempSchema.
                                           getAgentCode());
                tLATreeSchema.setAgentGrade(toldLATreeSchema.getAgentGrade());
                tLATreeSchema.setBranchType(tLABranchChangeTempSchema.
                                            getBranchType());
                tLATreeSchema.setBranchType2(tLABranchChangeTempSchema.
                                             getBranchType2());
                tLATreeSchema.setAstartDate(tLABranchChangeTempSchema.
                                            getStartDate());
                mLATreeSet.add(tLATreeSchema);
            }
        }
        if(mLATreeSet!=null && mLATreeSet.size()>=1)
        {
            VData tVData = new VData();
            tVData.add(mGlobalInput);
            tVData.addElement(mLABranchSchema);
            tVData.addElement(mLATreeSet);
            AdjustGrpAgentBL tAdjustGrpAgentBL = new AdjustGrpAgentBL();
            System.out.println("Start AdjustGrpAgentTemp Submit...");
            tAdjustGrpAgentBL.submitData(tVData, mOperate);
            System.out.println("End AdjustGrpAgentTemp Submit...");
            //如果有需要处理的错误，则返回
            if (tAdjustGrpAgentBL.mErrors.needDealError()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tAdjustGrpAgentBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "AdjustGrpAgentUI";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mMap = tAdjustGrpAgentBL.getMapResult();
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLABranchChangeTempSet.set((LABranchChangeTempSet) cInputData.
                                           getObjectByObjectName(
                    "LABranchChangeTempSet",0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                        getObjectByObjectName("GlobalInput",
                    0));
        //如果当时进行生效处理，则把mInInputData传入到AdjustGrpAgentBL
//        mInInputData.add(this.mGlobalInput);
//        mInInputData.add(this.mLABranchSchema);
//        mInInputData.add(this.mLATreeSet);
        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustGrpAgentBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
          this.mOutputDate.add(mMap) ;
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustGrpAgentBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
