/*
 * <p>ClassName: AdjustAgentBL </p>
 * <p>Description: AdjustAgentBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理银代人员团队调动
 * @CreateDate：2008-03-11
 */
package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAComToAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAComToAgentSchema;
import com.sinosoft.lis.schema.LAComToAgentBSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LAComToAgentSet;
import com.sinosoft.lis.vschema.LAComToAgentBSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.agent.LARecomPICCHBL;
import com.sinosoft.lis.agent.LARearPICCHBL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.lis.schema.LARecomRelationSchema;
import com.sinosoft.lis.db.LARecomRelationDB;

public class AdjustBankAgentBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String mOperate;
    private String mBranchType;
    private String mBranchType2;
    private String tEdorNo;
    private String mAimManageCom;
    private String mOldManageCom;
    private String mOldAgentGroup;
    private String mAStartDate;
    private boolean IsSingle = true;

    /** 业务处理相关变量 */
    private LABranchGroupSchema mLABranchSchema = new LABranchGroupSchema();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LACommisionSet mLACommisionSet = new LACommisionSet();

    //更新所有调动人员和管理人员在行政表中的AgentGroup UpBranch
    private LATreeSet mUpdateLATreeSet = new LATreeSet();

    //备份所有调动人员和管理人员
   // private LATreeBSet mLATreeBSet = new LATreeBSet();

    //更新所有调动人员在LAAgent表中的AgentGroup
    //private LAAgentSet mLAAgentSet = new LAAgentSet();

    //备份代理人记录 add by jiangcx minLABranchGroupBSet
   // private LAAgentBSet mLAAgentBSet = new LAAgentBSet();
    //备份代理人和代理机构关联表lacomtoagent及备份表lacomtoagentb
    private LAComToAgentSet mLAComToAgentSet=new LAComToAgentSet();
    private LAComToAgentBSet mLAComToAgentBSet=new LAComToAgentBSet();
    //备份代理人记录 labranchgroupb表中的branchmanager
    private LABranchGroupBSet minLABranchGroupBSet = new LABranchGroupBSet();

    private LARecomRelationSet mLARecomRelationSet = new LARecomRelationSet();
    //更新所有调动人员
    private LABranchGroupSet mupLABranchGroupSet = new LABranchGroupSet();
    
    //修改 ---处理不明原因
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAAgentBSchema mLAAgentBSchema = new LAAgentBSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private LATreeBSchema mLATreeBSchema = new LATreeBSchema();
    private String mLccontSql;
    private String mLbcontSql;
    private String mLcpolSql;
    private String mLbpolSql;
    private String mLjsSql;
    private String mLjspaySql;
    private String mLcgrpcontSql;
    private String mLcgrppolSql;
    private String mLbgrpcontSql;
    private String mLbgrppolSql;
    private String mLpgrpcontSql;
    private String mLpgrppolSql;
    private String mLpcontSql;
    private String mLppolSql;
    private String mLjspaygrpSql;
    private String mLjsgetendorseSql;
    
    private MMap mMap = new MMap();

    private VData mOutputDate = new VData();

    public AdjustBankAgentBL() {
    }

    public static void main(String[] args) {
        LABranchGroupSchema mLABranchSchema = new LABranchGroupSchema();
        LATreeSchema mManagerTreeSchema = new LATreeSchema();
        LATreeSet mLATreeSet = new LATreeSet();
        AdjustBankAgentUI mAdjustAgentUI = new AdjustBankAgentUI();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "mak001";
        tG.ComCode = "86";
        tG.ManageCom = "86";
        mLABranchSchema.setAgentGroup("000000000671");
        mLABranchSchema.setBranchAttr("86110000080101");
        mLABranchSchema.setBranchLevel("01");
        mLABranchSchema.setBranchManager("1101000408");
        mLABranchSchema.setUpBranch("");
        mLABranchSchema.setBranchType("1");
        mLABranchSchema.setBranchType2("01");

        LATreeSchema tLATreeSchema = new LATreeSchema();
        tLATreeSchema.setAgentCode("1101000583");
        tLATreeSchema.setAgentGrade("A02"); //代理人职级
        tLATreeSchema.setAgentGroup("000000000160");
        tLATreeSchema.setBranchType("1");
        tLATreeSchema.setBranchType2("01");
        tLATreeSchema.setAstartDate("2007-01-01");
        mLATreeSet.add(tLATreeSchema);

        TransferData tdata = new TransferData();
        tdata.setNameAndValue("IsSingle", "1");
        VData tVData = new VData();
        tVData.add(tG);
        tVData.addElement(mLABranchSchema);
        tVData.addElement(mLATreeSet);
        tVData.addElement(tdata);
        mAdjustAgentUI.submitData(tVData, "INSERT||MAIN");
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "AdjustAgentBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 校验的代码
     * @return
     */
    private boolean check() {
        System.out.println("---start check----");
        if (mLATreeSet == null || mLATreeSet.size() == 0) {
            CError tError = new CError();
            tError.moduleName = "AdjustBankAgentBL";
            tError.functionName = "check";
            tError.errorMessage = "请选择需要调整的人员!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("-----------");
        //查询目的机构是否存在
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setBranchAttr(this.mLABranchSchema.getBranchAttr());
        tLABranchGroupDB.setBranchType(this.mLABranchSchema.getBranchType());
        tLABranchGroupDB.setBranchType2(this.mLABranchSchema.getBranchType2());
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupSet = tLABranchGroupDB.query();
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
            CError tError = new CError();
            tError.moduleName = "AdjustBankAgentBL";
            tError.functionName = "check";
            tError.errorMessage = "没有查询到" + this.mLABranchSchema.getBranchAttr() +
                                  "的机构信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = tLABranchGroupSet.get(1);
        mLABranchSchema.setAgentGroup(tLABranchGroupSchema.getAgentGroup());
        System.out.println(mLABranchSchema.getAgentGroup());
        if (tLABranchGroupSchema.getEndFlag() != null && tLABranchGroupSchema.
            getEndFlag().equals("Y")) {
            CError tError = new CError();
            tError.moduleName = "AdjustBankAgentBL";
            tError.functionName = "check";
            tError.errorMessage = "机构" + this.mLABranchSchema.getBranchAttr() +
                                  "已经停业，不能继续调入人员!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!tLABranchGroupSchema.getBranchLevel().equals("43")) {
            buildError("check", "目标机构必须是营业部！");
            return false;
        }
        if (!tLABranchGroupSchema.getManageCom().equals(mOldManageCom)) {
            buildError("check", "人员不能跨管理机构调动！");
            return false;
        }

        //校验调动日期
        String tStartDate = this.mLATreeSet.get(1).getAstartDate();
        String AgentCode = this.mLATreeSet.get(1).getAgentCode();
        String newStartDate = AgentPubFun.formatDate(tStartDate, "yyyy-MM-dd");
        String tDay = newStartDate.substring(newStartDate.lastIndexOf("-") + 1);
        if (!tDay.equals("01")) {
            CError tError = new CError();
            tError.moduleName = "AdjustBankAgentBL";
            tError.functionName = "check";
            tError.errorMessage = "调整日期必须是从某个月的1号开始!";
            this.mErrors.addOneError(tError);
            return false;
        } 
        else {
            String sql = "select max(indexcalno) from lawage where managecom='" +
                         mOldManageCom + "' and branchtype='"+mBranchType+"' and branchtype2='"+mBranchType2+"'"
                        ;
            ExeSQL tExeSQL = new ExeSQL();
            String maxIndexCalNo = tExeSQL.getOneValue(sql);
            System.out.println("---最近发工资的日期---" + maxIndexCalNo);
            String lastDate = PubFun.calDate(tStartDate, -1, "M", null);
            lastDate = AgentPubFun.formatDate(lastDate, "yyyyMM");
            //如果没算过薪资，则调整日期为入司日期吧
            if (maxIndexCalNo == null || maxIndexCalNo.trim().equals("")) {
//                CError tError = new CError();
//                tError.moduleName = "AdjustBankAgentBL";
//                tError.functionName = "check";
//                tError.errorMessage = "管理机构" + mOldManageCom + "未计算过佣金，不能做调动!";
//                this.mErrors.addOneError(tError);
//                return false;
            	return true;
            }
            if (maxIndexCalNo.trim().compareTo(lastDate.trim())<=0) {
                return true;
            } 
            else {
                CError tError = new CError();
                tError.moduleName = "AdjustBankAgentBL";
                tError.functionName = "check";
                tError.errorMessage = "上次发工资是" + maxIndexCalNo.substring(0, 4) +
                                      "年" + maxIndexCalNo.substring(4).trim() +
                                      "月，因此调整日期必须大于等于这个月的下一个月1号";
                this.mErrors.addOneError(tError);
                return false;
            }
        	
        }

    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!check()) {
            return false;
        }
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustAgentBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败AdjustAgentBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mOutputDate, "");
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "AdjustAgentBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        boolean tReturn = true;
        int i = 0;
        LABranchGroupDB tLABranchDB = new LABranchGroupDB();
        tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        String tSQL ="select max(edorno) from lacomtoagentb with ur";
        ExeSQL tExeSQL =new ExeSQL();
        String tEdorNo2=""+tExeSQL.getOneValue(tSQL);
        if(tEdorNo2=="" || tEdorNo2==null){
        	CError tError = new CError();
            tError.moduleName = "AdjustBankAgentBL";
            tError.functionName = "dealdata";
            tError.errorMessage = "从代理人机构关联表中获取最大流水号失败！";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备更新机构表中的管理人员的纪录

        tLABranchDB.setAgentGroup(this.mLABranchSchema.getAgentGroup()); //隐式
        if (!tLABranchDB.getInfo()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLABranchDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "AdjustBankAgentBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询目标单位信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLABranchSchema = tLABranchDB.getSchema();

        //准备更新所有调动人员和管理人员在行政表中的AgentGroup和UpAgent的纪录
        String tAimBranch = this.mLABranchSchema.getAgentGroup();
        String tUpAgent = this.mLABranchSchema.getBranchManager();
        mAimManageCom = this.mLABranchSchema.getManageCom();

        tUpAgent = tUpAgent == null ? "" : tUpAgent;

        LATreeDB tLATreeDB = null;
        LAAgentDB tLAAgentDB = null;
        LAComToAgentDB tLAComToAgentDB=null;
        LAComToAgentSet tLAComToAgentSet=new LAComToAgentSet();
        int tCount = this.mLATreeSet.size();
        System.out.println("调动人数：" + tCount);
        String aimManageCom = this.mAimManageCom;
        for (i = 1; i <= tCount; i++) {
            tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(this.mLATreeSet.get(i).getAgentCode()); //jiangcx add for BK 主键

            if (!tLAAgentDB.getInfo()) {
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "AdjustBankAgentBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询业务员" + mLATreeSet.get(i).getAgentCode() +
                                      "的基础信息失败!";
                this.mErrors.addOneError(tError);
                return false;

            }
         
            this.mLATreeSchema  = this.mLATreeSet.get(i);//直接用schema 来存储 不用set集合
            
            tLATreeDB = new LATreeDB();
            tLATreeDB.setAgentCode(this.mLATreeSet.get(i).getAgentCode());
            if (!tLATreeDB.getInfo()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "AdjustBankAgentBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询目标单位信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mLATreeSchema=tLATreeDB.getSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(this.mLATreeBSchema, this.mLATreeSchema);
            mLATreeBSchema.setOperator2(mLATreeSchema.getOperator());
            mLATreeBSchema.setOperator(mGlobalInput.Operator);
            mLATreeBSchema.setEdorNO(tEdorNo);
            mLATreeBSchema.setRemoveType("03");
            mLATreeBSchema.setMakeDate2(mLATreeSchema.getMakeDate());
            mLATreeBSchema.setMakeTime2(mLATreeSchema.getMakeTime());
            mLATreeBSchema.setModifyDate2(mLATreeSchema.getModifyDate());
            mLATreeBSchema.setModifyTime2(mLATreeSchema.getModifyTime());
            mLATreeBSchema.setMakeDate(currentDate);
            mLATreeBSchema.setMakeTime(currentTime);
            mLATreeBSchema.setModifyDate(currentDate);
            mLATreeBSchema.setModifyTime(currentTime);


            String AdjustDate=mLATreeSchema.getAstartDate();
            if(AdjustDate.compareTo(tLAAgentDB.getEmployDate())<=0)
            {
            	AdjustDate = tLAAgentDB.getEmployDate();
            }
            mLATreeSchema.setUpAgent(tUpAgent);      //设置上级代理人
            mLATreeSchema.setAgentGroup(tAimBranch); //设置所属管理机构
            System.out.println("tAimBranchtAimBranch:" + tAimBranch);
            mLATreeSchema.setManageCom(mLABranchSchema.getManageCom());
            mLATreeSchema.setOperator(this.mGlobalInput.Operator);
            mLATreeSchema.setManageCom(aimManageCom);
            mLATreeSchema.setModifyDate(currentDate);
            mLATreeSchema.setModifyTime(currentTime);
            mLATreeSchema.setBranchCode(tAimBranch); //设置所属管理机构
            mLATreeSchema.setAstartDate(AdjustDate); //调整日期

            this.mLAAgentSchema = tLAAgentDB.getSchema();
            System.out.println("怎么么数据了呢"+mLAAgentSchema.getAgentCode());
            tReflections = new Reflections();
            tReflections.transFields(this.mLAAgentBSchema, mLAAgentSchema);
            mLAAgentBSchema.setMakeDate(currentDate);
            mLAAgentBSchema.setMakeTime(currentTime);
            mLAAgentBSchema.setModifyDate(currentDate);
            mLAAgentBSchema.setModifyTime(currentTime);
            mLAAgentBSchema.setOperator(mGlobalInput.Operator);
            mLAAgentBSchema.setEdorNo(tEdorNo);
            mLAAgentBSchema.setEdorType("03");
            

           

            mLAAgentSchema.setModifyDate(currentDate);
            mLAAgentSchema.setModifyTime(currentTime);
            mLAAgentSchema.setAgentGroup(tAimBranch);
            mLAAgentSchema.setBranchCode(tAimBranch);
            mLAAgentSchema.setOperator(mGlobalInput.Operator);
            mLAAgentSchema.setManageCom(mLABranchSchema.getManageCom());
            System.out.println("能流转到这里么。。。agentcode = "+mLAAgentSchema.getAgentCode());

           
            
            tSQL="select * from lacomtoagent where agentcom in (select distinct agentcom from lacomtoagent where agentcode='"+this.mLATreeSet.get(i).getAgentCode()+"')";
            tLAComToAgentDB=new LAComToAgentDB();
            tLAComToAgentSet=tLAComToAgentDB.executeQuery(tSQL);
            if (tLAComToAgentDB.mErrors.needDealError()) {
                this.mErrors.copyAllErrors(tLAComToAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "AdjustBankAgentBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询业务员" + mLATreeSet.get(i).getAgentCode() +
                                      "的关联代理机构信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            LAComToAgentSchema tLAComToAgentSchema;
            LAComToAgentBSchema tLAComToAgentBSchema;
            for(int m=1;m<=tLAComToAgentSet.size();m++){
                tEdorNo2=(Integer.parseInt(tEdorNo2)+1)+"";
                System.out.println("bl:dealdata:tEdorNo2"+tEdorNo2);
                tLAComToAgentSchema=new LAComToAgentSchema();
                tLAComToAgentBSchema=new LAComToAgentBSchema();
                tLAComToAgentSchema=tLAComToAgentSet.get(m);
                tReflections = new Reflections();
                tReflections.transFields(tLAComToAgentBSchema, tLAComToAgentSchema);
                tLAComToAgentBSchema.setMakeDate(currentDate);
                tLAComToAgentBSchema.setMakeTime(currentTime);
                tLAComToAgentBSchema.setModifyDate(currentDate);
                tLAComToAgentBSchema.setModifyTime(currentTime);
                tLAComToAgentBSchema.setOperator(mGlobalInput.Operator);
                tLAComToAgentBSchema.setEdorNo(tEdorNo2);
                tLAComToAgentBSchema.setEdorType("03");
                mLAComToAgentBSet.add(tLAComToAgentBSchema);
                tLAComToAgentSchema.setModifyDate(currentDate);
                tLAComToAgentSchema.setModifyTime(currentTime);
                tLAComToAgentSchema.setOperator(mGlobalInput.Operator);
                tLAComToAgentSchema.setAgentGroup(tAimBranch);
                mLAComToAgentSet.add(tLAComToAgentSchema);
            }
           
            //这段没必要
//            LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
//            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
//            tLABranchGroupDB.setBranchManager(this.mLATreeSet.get(i).getAgentCode());
//            tLABranchGroupSet = tLABranchGroupDB.query();
//            for (int k = 1; k <= tLABranchGroupSet.size(); k++) {
//                LABranchGroupSchema tLABranchGroupSchema = new
//                        LABranchGroupSchema();
//                LABranchGroupBSchema tLABranchGroupBSchema = new
//                        LABranchGroupBSchema();
//                tLABranchGroupSchema = tLABranchGroupSet.get(k);
//                //备份labranchgroupb表
//                tReflections = new Reflections();
//                tReflections.transFields(tLABranchGroupBSchema,
//                                         tLABranchGroupSchema);
//               
//                //String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
//                tLABranchGroupBSchema.setEdorType("07"); //修改操作
//                tLABranchGroupBSchema.setEdorNo(tEdorNo);
//                tLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.
//                        getMakeDate());
//                tLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.
//                        getMakeTime());
//                tLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.
//                        getModifyDate());
//                tLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.
//                        getModifyTime());
//                tLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.
//                        getOperator());
//                tLABranchGroupBSchema.setMakeDate(currentDate);
//                tLABranchGroupBSchema.setMakeTime(currentTime);
//                tLABranchGroupBSchema.setModifyDate(currentDate);
//                tLABranchGroupBSchema.setModifyTime(currentTime);
//                tLABranchGroupBSchema.setOperator(this.mGlobalInput.Operator);
//                minLABranchGroupBSet.add(tLABranchGroupBSchema);
//                //修改labranchgroup 中的主管为空
//                tLABranchGroupSchema.setBranchManager("");
//                tLABranchGroupSchema.setBranchManagerName("");
//                //调整日期
//                String FoundDate = AgentPubFun.formatDate(tLABranchGroupSchema.
//                        getFoundDate(), "yyyy-MM-dd");
//                if (FoundDate.compareTo(mAStartDate) > 0) {
//                    tLABranchGroupSchema.setAStartDate(FoundDate);
//                } else {
//                    tLABranchGroupSchema.setAStartDate(mAStartDate);
//                }
//                tLABranchGroupSchema.setModifyDate(currentDate);
//                tLABranchGroupSchema.setModifyTime(currentTime);
//                tLABranchGroupSchema.setOperator(mGlobalInput.Operator);
//                mupLABranchGroupSet.add(tLABranchGroupSchema);
//
//            }
            //
            //不同序列的需要进行业绩的处理
            if(!changeCommision(this.mLABranchSchema,mLAAgentSchema.getAgentCode(),AdjustDate)){
            		return false;
           	}
            

            //修改推荐关系的 agentgroup
            String sql = "select * from laRecomRelation where agentcode='" +
            mLAAgentSchema.getAgentCode() + "'  ";
            LARecomRelationSet tLARecomRelationSet = new LARecomRelationSet();
            LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();
            tLARecomRelationSet = tLARecomRelationDB.executeQuery(sql);
            for (int kk = 1; kk <= tLARecomRelationSet.size(); kk++) {
                LARecomRelationSchema tLARecomRelationSchema =
                        tLARecomRelationSet.
                        get(kk);
                tLARecomRelationSchema.setAgentGroup(mLABranchSchema.
                        getAgentGroup());
                tLARecomRelationSchema.setModifyDate(currentDate);
                tLARecomRelationSchema.setModifyTime(currentTime);
                tLARecomRelationSchema.setOperator(mGlobalInput.Operator);
                mLARecomRelationSet.add(tLARecomRelationSchema);
            }
        }
        if (this.mErrors.needDealError()) {
            tReturn = false;
        } else {
            tReturn = true;
        }
        return tReturn;
    }

    
    
    /**
     * 团队发生变化后,需要进行业绩的修改
     * @param mNewLABranchGroupSchema
     * @param changeCommision
     * @return
     */
    private boolean changeCommision(LABranchGroupSchema cNewLABranchGroupSchema,String mAgentCode,String mDate){
    	
    	if(!cNewLABranchGroupSchema.getAgentGroup().equals("")){        
    	//只能对还未进行过薪资计算月份的业绩修改
    	String SQL = "select * from lacommision where agentcode = '"+mAgentCode+"'"
        +" and branchtype='3' and branchtype2='01' and (caldate>='"+mDate+"' or caldate is null)";
    	LACommisionSet tLACommisionSet = new LACommisionSet();
        LACommisionDB tLACommisionDB = new LACommisionDB();
        tLACommisionSet = tLACommisionDB.executeQuery(SQL);
        for (int j = 1; j <= tLACommisionSet.size(); j++) {
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            tLACommisionSchema = tLACommisionSet.get(j);
            tLACommisionSchema.setAgentGroup(cNewLABranchGroupSchema.getAgentGroup());
            LABranchGroupDB tLDB = new LABranchGroupDB();
            tLDB.setAgentGroup(cNewLABranchGroupSchema.getAgentGroup());
            tLDB.getInfo();
            cNewLABranchGroupSchema=tLDB.getSchema();
            tLACommisionSchema.setBranchCode(cNewLABranchGroupSchema.getAgentGroup());
            tLACommisionSchema.setBranchSeries(cNewLABranchGroupSchema.getBranchSeries());
            tLACommisionSchema.setBranchAttr(cNewLABranchGroupSchema.getBranchAttr());
            tLACommisionSchema.setModifyDate(currentDate);
            tLACommisionSchema.setModifyTime(currentTime);
            tLACommisionSchema.setOperator(mGlobalInput.Operator);
            this.mLACommisionSet.add(tLACommisionSchema);
        }
    	//业务表lccont修改
    	 String tAgentGroup = cNewLABranchGroupSchema.getAgentGroup();
    	 System.out.println("tAgentGroup:"+tAgentGroup);
    	 this.mLccontSql = "update lccont  set  agentgroup='" + tAgentGroup+
                       "',ModifyTime = '" + currentTime +
                       "',ModifyDate = '" + currentDate +
                       "',operator='"+mGlobalInput.Operator+"'  where agentcode='"+mAgentCode+"'";
         
         
         this.mLbcontSql = "update lbcont  set  agentgroup='" + tAgentGroup+
         "',ModifyTime = '" + currentTime +
         "',ModifyDate = '" + currentDate +
         "',operator='"+mGlobalInput.Operator+"'  where agentcode='"+mAgentCode+"'";
        
         
         this.mLcpolSql = "update lcpol  set  agentgroup='" + tAgentGroup+
                "',ModifyTime = '" + currentTime +
                "',ModifyDate = '" + currentDate +
                "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";
        
         
         this.mLbpolSql = "update lbpol  set  agentgroup='" + tAgentGroup+
         "',ModifyTime = '" + currentTime +
         "',ModifyDate = '" + currentDate +
         "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";
        
         
         this.mLjsSql = "update ljspayperson  set  agentgroup='" + tAgentGroup+
                "',ModifyTime = '" + currentTime +
                "',ModifyDate = '" + currentDate +
                "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";
       
         
         this.mLjspaySql = "update ljspay  set  agentgroup='" + tAgentGroup+
                "',ModifyTime = '" + currentTime +
                "',ModifyDate = '" + currentDate +
                "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";
         this.mLcgrpcontSql="update lcgrpcont set agentgroup='"+tAgentGroup+"',ModifyTime = '" + currentTime +
         "',ModifyDate = '" + currentDate +
         "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";
        this.mLcgrppolSql="update lcgrppol set agentgroup='"+tAgentGroup+"',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";
        this.mLpgrpcontSql="update lpgrpcont set agentgroup='"+tAgentGroup+"',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";
        this.mLpgrppolSql="update lpgrppol set agentgroup='"+tAgentGroup+"',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";
        this.mLbgrpcontSql="update lbgrpcont set agentgroup='"+tAgentGroup+"',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";
        this.mLbgrppolSql="update lbgrppol set agentgroup='"+tAgentGroup+"',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";
        this.mLpcontSql="update lpcont set agentgroup='"+tAgentGroup+"',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";
        this.mLppolSql="update lppol set agentgroup='"+tAgentGroup+"',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";
        this.mLjspaygrpSql= "update ljspaygrp  set  agentgroup='" + tAgentGroup+
        "',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";
        this.mLjsgetendorseSql= "update ljsgetendorse  set  agentgroup='" + tAgentGroup+
        "',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";
    	}
    	return true;
    }
  
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("GetInputData-AdJustAgentBL");
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLABranchSchema.setSchema((LABranchGroupSchema) cInputData.
                                  getObjectByObjectName(
                                          "LABranchGroupSchema", 1));
        this.mLATreeSet.set((LATreeSet) cInputData.getObjectByObjectName(
                "LATreeSet", 2));
        mBranchType=mLABranchSchema.getBranchType();
        mBranchType2=mLABranchSchema.getBranchType2();
        mAStartDate = mLATreeSet.get(1).getAstartDate(); //起前台录入的调整日期
        mOldManageCom = mLATreeSet.get(1).getManageCom();
        mOldAgentGroup = mLATreeSet.get(1).getAgentGroup();
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            
            mMap.put(this.mLAAgentSchema, "UPDATE");
            mMap.put(this.mLAAgentBSchema, "INSERT");
            mMap.put(this.mLATreeSchema, "UPDATE");
            mMap.put(this.mLATreeBSchema, "INSERT");
            
            
            mMap.put(this.mLAComToAgentBSet,"INSERT");
            mMap.put(this.mLAComToAgentSet,"UPDATE");
            mMap.put(this.mLACommisionSet, "UPDATE");
            mMap.put(mLccontSql, "UPDATE");
            mMap.put(mLbcontSql, "UPDATE");
            mMap.put(mLcpolSql, "UPDATE");
            mMap.put(mLbpolSql, "UPDATE");
            mMap.put(mLjsSql, "UPDATE");
            mMap.put(mLjspaySql, "UPDATE");
            mMap.put(mLcgrpcontSql, "UPDATE");
            mMap.put(mLcgrppolSql, "UPDATE");
            mMap.put(mLbgrppolSql, "UPDATE");
            mMap.put(mLbgrpcontSql, "UPDATE");
            mMap.put(mLpgrppolSql, "UPDATE");
            mMap.put(mLpgrpcontSql, "UPDATE");
            mMap.put(mLpcontSql, "UPDATE");
            mMap.put(mLppolSql, "UPDATE");
            mMap.put(mLjspaygrpSql, "UPDATE");
            mMap.put(mLjsgetendorseSql, "UPDATE");
           // mMap.put(this.minLABranchGroupBSet, "INSERT");
            //mMap.put(this.mupLABranchGroupSet, "UPDATE");
            //mMap.put(this.mLARecomRelationSet, "UPDATE");
            System.out.println("bl：here：mMap.size"+mMap.size());
            this.mOutputDate.add(mMap);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustAgentBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
