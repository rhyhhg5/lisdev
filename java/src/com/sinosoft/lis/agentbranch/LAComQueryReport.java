package com.sinosoft.lis.agentbranch;



/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;

public class LAComQueryReport
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

//取得的机构外部代码
    private String mAgentCom = "";

//取得的代理人离职日期
    private String mAgentComName = "";

//取得的代理人姓名
    private String mBankType = "";

//取得的保单号
    private String mSellFlag = "";
    private String mEndFlag = "";
    private String mBusiLicenseCode = "";
    //取
        private String mAgentCode = "";

//取
        private String mAgentName = "";


//取得的管理机构代码
    private String mManageCom = "";
//取得当前日期
    private String mdate = "";
 //取得当期时间
    private String mtime = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
//    private LJAGetSchema mLJAGetSchema = new LJAGetSchema();
//    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
//    private LDComSchema mLDComSchema = new LDComSchema();

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
    	System.out.println("...................lacomqueryreport:here begin submit");
    	if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
    	 mGlobalInput.setSchema((GlobalInput) cInputData.
                 getObjectByObjectName("GlobalInput", 0));
    	mManageCom = (String)cInputData.get(0);
    	mAgentCom = (String)cInputData.get(1);
    	mAgentComName = (String)cInputData.get(2);
    	mBankType = (String)cInputData.get(3);
    	mSellFlag = (String)cInputData.get(4);
        mAgentCode = (String)cInputData.get(5);
    	mAgentName = (String)cInputData.get(6);
    	mEndFlag= (String)cInputData.get(7);
    	mBusiLicenseCode= (String)cInputData.get(8);
    	System.out.println("BusiLicenseCode========="+mBusiLicenseCode);
    	System.out.println("...................lacomqueryreport:here begin getinputdata");
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
    	System.out.println("...................lacomqueryreport:here begin getPtintData");
    	TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport txmlexport = new XmlExport(); //新建一个XmlExport的实例
        txmlexport.createDocument("LAComQueryReport.vts", "printer"); //最好紧接着就初始化xml文档
        ListTable tListTable = new ListTable();
        tListTable.setName("LAComQueryReport");
        String[] title = {"", "", "", "", "" , "", "", "","","" ,""};

       //查询所有的记录
       msql = "select a.AgentCom,a.Name,"
              +"(Select c.Name From LAAgent c,lacomtoagent d Where c.AgentCode=d.AgentCode and a.AgentCom=d.AgentCom and d.RelaType='1'),"
              +"(select b.agentcode from lacomtoagent b where a.AgentCom=b.AgentCom and b.RelaType='1'),"
              +"a.managecom,"
    	   +" case a.endflag when 'Y' then '是' when 'N' then '否' else '否' end ,a.makedate,a.enddate, "
           +" case a.sellflag when 'Y' then '是' when 'N' then '否' else '否' end ,a.BusiLicenseCode  "
    	   +" from lacom a "
    	   +" where a.actype='01'   " ;
       if(this.mManageCom!=null&&!this.mManageCom.equals(""))
       {
    	   msql += " and a.managecom like '"+this.mManageCom+"%'";
       }
       if(this.mAgentCom!=null&&!this.mAgentCom.equals(""))
       {
    	   msql += " and a.AgentCom='"+this.mAgentCom+"'";
       }
       if(this.mAgentComName!=null&&!this.mAgentComName.equals(""))
       {
    	   msql+= " and a.name='"+this.mAgentComName+"'";
       }
       if(this.mBankType!=null&&!this.mBankType.equals(""))
       {
    	   msql+= " and a.banktype='"+this.mBankType+"'";
       }
       if(this.mSellFlag!=null&&!this.mSellFlag.equals(""))
       {
    	   msql += " and a.sellflag ='"+mSellFlag+"'";
       }
       if(this.mBusiLicenseCode!=null&&!this.mBusiLicenseCode.equals(""))
       {
    	   msql += " and a.BusiLicenseCode ='"+mBusiLicenseCode+"'";
       }
       if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
       {
           msql += "and a.agentcom in (select agentcom from lacomtoagent where agentcode='"+mAgentCode+"')";
       }
       if(this.mAgentName!=null&&!this.mAgentName.equals(""))
       {
           msql += " and a.agentcom in (select agentcom from lacomtoagent where agentcode in (select agentcode from laagent where name='"+mAgentName+"' and branchtype='3' and branchtype2='01'))";
       }
       if(this.mEndFlag!=null&&this.mEndFlag.equals("Y")){
    	   msql += " and a.endflag ='Y'";
       }
       if(this.mEndFlag!=null&&this.mEndFlag.equals("N")){
    	   msql += " and (a.endflag ='N' or a.endflag is null)";
       }
       msql += " order by a.agentcom ";
       System.out.println("BusiLicenseCode========="+mBusiLicenseCode);
      ExeSQL mExeSQL = new ExeSQL();
      SSRS mSSRS = new SSRS();
      mSSRS = mExeSQL.execSQL(msql);
      System.out.println(msql);
      if (mSSRS.mErrors.needDealError()) {
          CError tCError = new CError();
          tCError.moduleName = "LAComQueryReport.java";
          tCError.functionName = "getPrintData";
          tCError.errorMessage = "查询XML数据出错！";

          this.mErrors.addOneError(tCError);

          return false;

      }
      if (mSSRS.getMaxRow() <= 0) {
          CError tCError = new CError();
          tCError.moduleName = "LAComQueryReport.java";
          tCError.functionName = "getPrintData";
          tCError.errorMessage = "没有符合条件的信息！";

          this.mErrors.addOneError(tCError);

          return false;
      }
      if(mSSRS.getMaxRow()>=1)
      {
    	 for(int i=1;i<=mSSRS.getMaxRow();i++)
    	 {
    	  String Info[] = new String[12];
    	  //System.out.println(mSSRS.GetText(i, 1));
    	  Info[0] = mSSRS.GetText(i, 1);
    	  //System.out.println(mSSRS.GetText(i, 2));
    	  Info[1] = mSSRS.GetText(i, 2);
    	  Info[2] = mSSRS.GetText(i, 3);
    	  Info[3] = mSSRS.GetText(i, 4);
    	  Info[4] = mSSRS.GetText(i, 5);
    	  Info[5] = mSSRS.GetText(i, 6);
    	  Info[6] = mSSRS.GetText(i, 7);
    	  Info[7] = mSSRS.GetText(i, 8);
          Info[8] = mSSRS.GetText(i, 9);
          Info[9] = mSSRS.GetText(i, 10);
          //Info[10] = mSSRS.GetText(i, 11);
         // Info[11] = mSSRS.GetText(i, 12);
          System.out.println("传入数据aaaaaaaaaaaaaa="+mSSRS.GetText(i, 10));
    	  tListTable.add(Info);
    	 }
      }
      else {
          CError tCError = new CError();
          tCError.moduleName = "CreateXml";
          tCError.functionName = "creatFile";
          tCError.errorMessage = "没有符合条件的信息！";
          this.mErrors.addOneError(tCError);
          return false;
      }
        mdate = PubFun.getCurrentDate();
        mtime = PubFun.getCurrentTime();

         texttag.add("makedate", mdate); //日期
         texttag.add("maketime", mtime);
        if (texttag.size() > 0)
        {
            txmlexport.addTextTag(texttag);
        }
        //System.out.println(".............tListTable.size()"+texttag.size());
        //System.out.println(".............tListTable.size()"+tListTable.size());
        txmlexport.addListTable(tListTable,title);
        mResult.clear();
        mResult.addElement(txmlexport);
        System.out.println("...................exit java file here ");
        return true;
    }

    private String getComName(String strComCode)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("station");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            return "";
        }
        return tLDCodeDB.getCodeName();

    }
}



