/*
 * <p>ClassName: LAComToAgentBLS </p>
 * <p>Description: LAContBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-27
 */
package com.sinosoft.lis.agentbranch;

import java.sql.Connection;

import com.sinosoft.lis.db.LAComToAgentBDB;
import com.sinosoft.lis.db.LAComToAgentDB;
import com.sinosoft.lis.schema.LAComToAgentBSchema;
import com.sinosoft.lis.schema.LAComToAgentSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class ALAComToAgentBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    private String AgentCom;
    public ALAComToAgentBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) throws
            Exception
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        System.out.println("Start LAComToAgentBLS Submit...");
        System.out.println("操作符：" + this.mOperate);
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            tReturn = saveLACont(cInputData);
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            tReturn = deleteLACont(cInputData);
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            tReturn = updateLACont(cInputData);
        }
        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End LAComToAgentBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean saveLACont(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save Now...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComToAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAComToAgentDB tLAComToAgentDB = new LAComToAgentDB(conn);
            tLAComToAgentDB.setSchema((LAComToAgentSchema) mInputData.
                                      getObjectByObjectName(
                                              "LAComToAgentSchema", 1));
            System.out.println("员工代码" + tLAComToAgentDB.getAgentCode());
            if (!tLAComToAgentDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAComToAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAComToAgentBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComToAgentBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    /**
     * 删除函数
     */
    private boolean deleteLACont(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComToAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAComToAgentBDB tLAComToAgentBDB = new LAComToAgentBDB(conn);
            tLAComToAgentBDB.setSchema((LAComToAgentBSchema) mInputData.
                                       getObjectByObjectName(
                                               "LAComToAgentBSchema", 2));
            if (!tLAComToAgentBDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAComToAgentBDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAComToAgentBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LAComToAgentDB tLAComToAgentDB = new LAComToAgentDB(conn);
            tLAComToAgentDB.setSchema((LAComToAgentSchema) mInputData.
                                      getObjectByObjectName(
                                              "LAComToAgentSchema", 1));
            if (!tLAComToAgentDB.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAComToAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAComToAgentBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComToAgentBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean updateLACont(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAComToAgentBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAComToAgentBDB tLAComToAgentBDB = new LAComToAgentBDB(conn);
            tLAComToAgentBDB.setSchema((LAComToAgentBSchema) mInputData.
                                       getObjectByObjectName(
                                               "LAComToAgentBSchema", 2));
            AgentCom = (String) mInputData.get(3);
            if (!tLAComToAgentBDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAComToAgentBDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAComToAgentBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            if (AgentCom == "" || AgentCom == null)
            {
                return false;
            }
            LAComToAgentDB tLAComToAgentDB = new LAComToAgentDB(conn);
            tLAComToAgentDB.setSchema((LAComToAgentSchema) mInputData.
                                      getObjectByObjectName(
                                              "LAComToAgentSchema", 1));
            String tSQL = "update LAComToAgent set AgentCom='" +
                          tLAComToAgentDB.getAgentCom() +
                          "',RelaType='1',AgentCode='" +
                          tLAComToAgentDB.getAgentCode() + "',AgentGroup='" +
                          tLAComToAgentDB.getAgentGroup() + "',Operator='" +
                          tLAComToAgentDB.getOperator() + "',MakeDate='" +
                          tLAComToAgentDB.getMakeDate() + "',MakeTime='" +
                          tLAComToAgentDB.getMakeTime() + "',ModifyDate='" +
                          tLAComToAgentDB.getModifyDate() + "',ModifyTime='" +
                          tLAComToAgentDB.getModifyTime() +
                          "' where AgentCom='" + AgentCom +
                          "' and RelaType='1' and AgentGroup='" +
                          tLAComToAgentDB.getAgentGroup() + "'";
            ExeSQL tExeSQL = new ExeSQL();
            System.out.println("插入记录的SQL: " + tSQL);
            tReturn = tExeSQL.execUpdateSQL(tSQL);
            if (tExeSQL.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "CalIndex";
                tError.functionName = "insertAssess";
                tError.errorMessage = "执行SQL语句：判断记录是否存在失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComToAgentBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }
}
