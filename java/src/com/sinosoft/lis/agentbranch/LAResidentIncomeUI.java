package com.sinosoft.lis.agentbranch;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAResidentIncomeUI
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    public LAResidentIncomeUI()
    {
        System.out.println("LAFYCRCQueryUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
    	LAResidentIncomeBL tLAResidentIncomeBL = new LAResidentIncomeBL();
        if(!tLAResidentIncomeBL.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(tLAResidentIncomeBL.mErrors);
            return false;
        }

        return true;
    }
}
