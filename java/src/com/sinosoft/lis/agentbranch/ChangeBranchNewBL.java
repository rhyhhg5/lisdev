package com.sinosoft.lis.agentbranch;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LABranchLevelSchema;
import com.sinosoft.lis.db.LABranchLevelDB;
import com.sinosoft.lis.vschema.LABranchLevelSet;
import java.math.BigInteger;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.agent.LARearPICCHBL;
import com.sinosoft.lis.agent.LARecomPICCHBL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.db.LARearRelationDB;
import sun.reflect.Reflection;
import com.sinosoft.lis.schema.LARearRelationBSchema;
import com.sinosoft.lis.vschema.LARearRelationBSet;

public class ChangeBranchNewBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String mOperate;
    private boolean IsSingle;

    /** 业务处理相关变量 */
    private LABranchGroupSchema mAimLABranchSchema = new LABranchGroupSchema();
    private LABranchGroupSchema mAdjustLABranchSchema = new LABranchGroupSchema();
    private LARearRelationSet mLARearRelationSet = new LARearRelationSet();
    private LARearRelationBSet mLARearRelationBSet = new LARearRelationBSet();
    //插入到机构备份表,备份被调动机构的信息
    private LABranchGroupBSet mLABranchGroupBSet = new LABranchGroupBSet();

    //更新机构表中的upBranch，将被调动机构的上级机构相关信息更新
    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();

    private LATreeBSet mLATreeBSet = new LATreeBSet(); //备份管理人员的行政信息
    private LATreeSet mLATreeSet = new LATreeSet();
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    private boolean ifChangeManageCom = false; //是否跨了管理机构
    private String mUpBranchAttr = "";
    private String NewBranchAttr = "";
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String NewBranchSeries = "";
    private String mEdorNo = "";
    private String mBranchManager = new String();
    private String mAdjustDate = "";
    private int mCount = 0 ;  //表示第几次生成BranchAttr(培养团队需要逐次加1)
    private MMap mMap = new MMap();

    public ChangeBranchNewBL()
    {
    }


    /**
     * 校验的代码
     * @return
     */
    private boolean check()
    {
        String tStartDate = this.mAdjustDate;// 调整日期
        String tAdjustBranchAttr = this.mAdjustLABranchSchema.getBranchAttr();// 调整团队 外部编码
        //修改：LL 2004-10-19
        //修改原因：如果被调动组为直辖组则不让其调动
        String tSQL =
                "select upBranchAttr,Managecom from labranchgroup where branchattr = '" +
                tAdjustBranchAttr + "' and BranchType='"+mAdjustLABranchSchema.getBranchType() +"' "
                 + " and BranchType2='"+mAdjustLABranchSchema.getBranchType2()+"'"; //直辖非直辖
        ExeSQL mExeSQL = new ExeSQL();
        SSRS tSSRS=new SSRS();
        tSSRS=mExeSQL.execSQL(tSQL) ;
        System.out.println("tSQL"+tSQL);
        if (tSSRS == null || tSSRS.getMaxRow() ==0)
        {
            CError tError = new CError();
            tError.moduleName = "ChangeBranchNewBL";
            tError.functionName = "check";
            tError.errorMessage = "查询机构直辖属性时出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tValue = tSSRS.GetText(1,1) ;
        String tManageCom=tSSRS.GetText(1,2) ;
        System.out.println("被调整机构" + tAdjustBranchAttr + "直辖属性为：" + tValue);

        //直辖机构不能进行机构调整
        if (tValue.equals("1"))
        { //“1”为直辖
            CError tError = new CError();
            tError.moduleName = "ChangeBranchNewBL";
            tError.functionName = "check";
            tError.errorMessage = "直辖机构不能进行机构调整!";
            this.mErrors.addOneError(tError);
            return false;
        }
 
        String newStartDate = AgentPubFun.formatDate(tStartDate, "yyyy-MM-dd"); //调整日期转换格式
        String tDay = newStartDate.substring(newStartDate.lastIndexOf("-") + 1); //取停业所在月的日期
        if (!tDay.equals("01"))
        {
            CError tError = new CError();
            tError.moduleName = "ChangeBranchNewBL";
            tError.functionName = "check";
            tError.errorMessage = "调整日期必须是从某个月的1号开始!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
           String sql = "select max(indexcalno) from lawage where managecom='" 
            	+tManageCom +"' and branchtype='1' and branchtype2='01' " ;
            ExeSQL  tExeSQL = new ExeSQL();
            String maxIndexCalNo = tExeSQL.getOneValue(sql);
            System.out.println("---最近发工资的日期---" + maxIndexCalNo);
            String lastDate = PubFun.calDate(tStartDate, -1, "M", null);// 调动日期 - 1个月
            lastDate = AgentPubFun.formatDate(lastDate, "yyyyMM");

            if (maxIndexCalNo == null || maxIndexCalNo.trim().equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "ChangeBranchNewBL";
                tError.functionName = "check";
                tError.errorMessage = "管理机构" + tManageCom  +
                                      "未计算过佣金，不能做调动!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (maxIndexCalNo.trim().equals(lastDate.trim())) 
            {
            	return true; 
            }
            else
            {
                CError tError = new CError();
                tError.moduleName = "ChangeBranchNewBL";
                tError.functionName = "check";
                tError.errorMessage = "上次发工资是" + maxIndexCalNo.substring(0, 4) +
                                      "年" +
                                      maxIndexCalNo.substring(4).trim() +
                                      "月，因此调整日期必须从这个月的下一个月1号";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("Into ChangeBranchNewBL !!! ");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!check())
            return false;
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ChangeBranchNewBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ChangeBranchNewBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if(mLATreeSet!=null && mLATreeSet.size()>=1)
        {
            if (!chgRelation())
                return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        
        if(!dealGbuild())
        {
        	return false;
        }
        if(!prepareOutputData1())
        {
        	return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mInputData, "");
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "ChangeBranchNewBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {

        NewBranchAttr = this.mAimLABranchSchema.getBranchAttr();//调往团队外部编码
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupDB.setBranchAttr(mAdjustLABranchSchema.getBranchAttr());// 调整团队外部编码
        tLABranchGroupDB.setBranchType(mAdjustLABranchSchema.getBranchType() ) ;
        tLABranchGroupDB.setBranchType2(mAdjustLABranchSchema.getBranchType2());
        tLABranchGroupSet=tLABranchGroupDB.query();//查询出调离团队的信息
        if (tLABranchGroupSet==null || tLABranchGroupSet.size() ==0)
        {
            CError tError = new CError();
            tError.moduleName = "ChangeBranchNewBL";
            tError.functionName = "dealdata";
            tError.errorMessage = "未找到机构编码为" + mAimLABranchSchema.getBranchAttr() +
                                  "的机构信息!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mAdjustLABranchSchema = tLABranchGroupSet.get(1); // 调整团队信息
        
        
        tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setBranchAttr(mAimLABranchSchema.getBranchAttr());// 调往团队外部编码
        tLABranchGroupDB.setBranchType(mAimLABranchSchema.getBranchType());
        tLABranchGroupDB.setBranchType2(mAimLABranchSchema.getBranchType2());
        tLABranchGroupSet = tLABranchGroupDB.query();
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0)
        {
            this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ChangeBranchNewBL";
            tError.functionName = "dealdata";
            tError.errorMessage = "未找到机构编码为" + mAimLABranchSchema.getBranchAttr() +
                                  "的机构信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mAimLABranchSchema = tLABranchGroupSet.get(1);  // 调往团队信息
        if(mAimLABranchSchema.getCostCenter()==null||mAimLABranchSchema.getCostCenter().equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "ChangeBranchNewBL";
            tError.functionName = "dealdata";
            tError.errorMessage = "未找到调往机构编码为" + mAimLABranchSchema.getBranchAttr() +
                                  "的成本中心编码!";
            this.mErrors.addOneError(tError);
            return false;
        }
        // 注:增加校验 不能跨层级团队调动
        String c_sql = "select count(*) from labranchlevel where 1=1 and branchlevelcode >='"+mAdjustLABranchSchema.getBranchLevel()+"'"
                           + " and branchlevelcode <'"+mAimLABranchSchema.getBranchLevel()+"'"
                           + " and branchtype = '"+mAdjustLABranchSchema.getBranchType()+"' and branchtype2 = '"+mAdjustLABranchSchema.getBranchType2()+"'";
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("校验跨层级SQL:"+c_sql);
        
        if(Integer.parseInt(tExeSQL.getOneValue(c_sql))==2)
        {
     	   CError tError = new CError();
            tError.moduleName = "ChangeBranchNewBL";
            tError.functionName = "check";
            tError.errorMessage = "不能跨层级机机构调整";
            this.mErrors.addOneError(tError);
            return false;
        }
         String sql =
                 "select count(*) from labranchlevel where branchlevelcode>'" +
                 mAdjustLABranchSchema.getBranchLevel() +
                 "' and BranchType='" + mAdjustLABranchSchema.getBranchType() +
                 "' and BranchType2='" +
                 mAdjustLABranchSchema.getBranchType2() + "'";
         tExeSQL = new ExeSQL();
         if (Integer.parseInt(tExeSQL.getOneValue(sql)) == 0)
         {
             CError tError = new CError();
             tError.moduleName = "ChangeBranchNewBL";
             tError.functionName = "check";
             tError.errorMessage = "机构" +
                                   mAdjustLABranchSchema.getBranchAttr() +
                                   "已经是最高级别的机构了,不能进行调动!!";
             this.mErrors.addOneError(tError);
             return false;
         }
         
        mCount = mCount + 1 ;//带走培养关系时,团队代码+mCount
        NewBranchAttr = createBranchAttr(); //自动生成机构代码

        // 未生成外部编码 返回错误
        if (NewBranchAttr == null || NewBranchAttr.equals(""))
        {
            return false;
        }

        this.mBranchManager = mAdjustLABranchSchema.getBranchManager(); //被调整机构主管
        String NewManageCom = mAimLABranchSchema.getManageCom();//调往团队管理机构编码
        String OldManageCom = mAdjustLABranchSchema.getManageCom();//原先管理机构编码
        mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        if (NewManageCom.equals(OldManageCom))
        { //判断是否跨了管理机构调动
            ifChangeManageCom = false; // 不支持跨管理机构调动
        }
        else
        {
            ifChangeManageCom = true;
        }
        //对被调动机构的下属机构作调动处理 ---主要针对区级团队
         sql = "select * from labranchgroup where upbranch = '" +
                     mAdjustLABranchSchema.getAgentGroup() +
                     "' and (state is null or state<>'1') and (endflag is null or endflag<>'Y')";
        
        System.out.println(sql);
        
        tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(sql);
        for (int i = 1; i <= tLABranchGroupSet.size(); i++)
        {//如果存在下属机构
            LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
            tLABranchGroupSchema = tLABranchGroupSet.get(i);
            //处理下属团队
            if (!dealEachBranchGroup(tLABranchGroupSchema,"0"))//tLABranchGroupSchema 已经是下属机构了
            {
                return false;
            }
        }
        if (tLABranchGroupSet.size() == 0)
        {//如果不存在下属机构,处理被调动机构本身(不存在下属机构,则该机构为处,
         //只要处理主管
            dealEachBranchGroup(mAdjustLABranchSchema,"1");
        }
      //如果被调动的是营业区，则其下面所有处的主管人员也要变更astartdate
        if (!dealBranchManager(mAdjustLABranchSchema))
        {
            return false;
        }

        return true;
    }


    private String createBranchAttr()
    {
        String upBranchLevel = mAimLABranchSchema.getBranchLevel();// 调往团队的团队序列
        String curBranchLevel = mAdjustLABranchSchema.getBranchLevel();// 调整团队的团队序列
        System.out.println("mAdjustBranchSchema:"+mAdjustLABranchSchema.getBranchAttr() ) ;
        System.out.println("mAimLABranchSchema:"+mAimLABranchSchema.getBranchAttr() ) ;
        LABranchLevelSchema upBranchLevelSchema = new LABranchLevelSchema();
        LABranchLevelSchema cuBranchLevelSchema = new LABranchLevelSchema();
        LABranchLevelDB tLABranchLevelDB = new LABranchLevelDB();
        tLABranchLevelDB.setBranchLevelCode(upBranchLevel);
        tLABranchLevelDB.setBranchType(mAimLABranchSchema.getBranchType());
        tLABranchLevelDB.setBranchType2(mAimLABranchSchema.getBranchType2());
        tLABranchLevelDB.getInfo();
        upBranchLevelSchema = tLABranchLevelDB.getSchema();
        tLABranchLevelDB.setBranchLevelCode(curBranchLevel);
        tLABranchLevelDB.setBranchType(mAdjustLABranchSchema.getBranchType());
        tLABranchLevelDB.setBranchType2(mAdjustLABranchSchema.getBranchType2());
        tLABranchLevelDB.getInfo();
        cuBranchLevelSchema = tLABranchLevelDB.getSchema();
        System.out.println("cu:"+cuBranchLevelSchema.getBranchLevelID() );
        System.out.println("up:"+upBranchLevelSchema.getBranchLevelID() );
        if (cuBranchLevelSchema.getBranchLevelID() >=
            upBranchLevelSchema.getBranchLevelID())
        {
            CError tError = new CError();
            tError.moduleName = "ChangeBranchNewBL";
            tError.functionName = "createBranchAttr";
            tError.errorMessage = "被调动的机构级别应该比被调入的目标机构级别低!。";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (cuBranchLevelSchema.getBranchLevelID() + 1 ==
            upBranchLevelSchema.getBranchLevelID())
        {
            mUpBranchAttr = mAimLABranchSchema.getBranchAttr();// 相邻序列 直接形成上下级关系
        }
        else
        {   // 跨序列 这个比较复杂一点
            LABranchLevelSet tLABranchLevelSet = new LABranchLevelSet();
            tLABranchLevelDB=new LABranchLevelDB();
            tLABranchLevelDB.setBranchLevelID(cuBranchLevelSchema.getBranchLevelID() + 1);// 查调整团队上个层级信息
            tLABranchLevelDB.setBranchType(cuBranchLevelSchema.getBranchType());
            tLABranchLevelDB.setBranchType2(cuBranchLevelSchema.getBranchType2());
            tLABranchLevelSet = tLABranchLevelDB.query();
            if (tLABranchLevelSet == null || tLABranchLevelSet.size() == 0)
            {
                CError tError = new CError();
                tError.moduleName = "ChangeBranchNewBL";
                tError.functionName = "createBranchAttr";
                tError.errorMessage = "未查询到当前机构的上级机构。";
                this.mErrors.addOneError(tError);
                return null;
            }
            LABranchLevelSchema tLABranchLevelSchema = new LABranchLevelSchema();
            tLABranchLevelSchema = tLABranchLevelSet.get(1);
            int upLength = Integer.parseInt(tLABranchLevelSchema.
                                            getBranchLevelProperty2());// 得到调整团队上个层级对应团队序列信息
            int zeroLength = upLength-Integer.parseInt(upBranchLevelSchema.
                                              getBranchLevelProperty2());// 得到调往团队 对应团队序列信息 -- 结果肯定为2

            System.out.println(tLABranchLevelSchema.getBranchLevelProperty2()) ;
            System.out.println(upBranchLevelSchema.getBranchLevelProperty2()) ;
            System.out.println(zeroLength) ;
            String zero = "0000000000000000000000000".substring(0, zeroLength);
            System.out.println("getBranchAttr:"+mAimLABranchSchema.getBranchAttr() ) ;
            System.out.println("zero:"+zero) ;
            mUpBranchAttr = mAimLABranchSchema.getBranchAttr() + zero;// 等于在branchattr 后面又拼了2位
            System.out.println("这里打印出来"+mUpBranchAttr);
        }
        String maxBranchAttr = "";
        ExeSQL tExeSQL = new ExeSQL();
        String maxBranchAttr1 = "";
        String maxBranchAttr2 = "";
        String sql =
                "select max(branchattr) from labranchgroup where branchattr like '" +
                mUpBranchAttr + "%' and BranchLevel='"+mAdjustLABranchSchema.getBranchLevel() +"' and BranchType='" +
                upBranchLevelSchema.getBranchType() + "' and BranchType2='" +
                upBranchLevelSchema.getBranchType2() + "'";
        maxBranchAttr1 = tExeSQL.getOneValue(sql);
        if (maxBranchAttr1==null || maxBranchAttr1.equals("") )
        {
            maxBranchAttr1=mUpBranchAttr+"000000000000000".substring(0,Integer.parseInt(cuBranchLevelSchema.getBranchLevelProperty2())-mUpBranchAttr.length() );
        }
        BigInteger tBigInteger = new BigInteger(maxBranchAttr1);
        sql =
                "select max(branchattr) from labranchgroupb where branchattr like '" +
                mUpBranchAttr + "%' and BranchLevel='"+mAdjustLABranchSchema.getBranchLevel() +"'   and BranchType='" +
                upBranchLevelSchema.getBranchType() + "' and BranchType2='" +
                upBranchLevelSchema.getBranchType2() + "'";
        maxBranchAttr2 = tExeSQL.getOneValue(sql);
        if (maxBranchAttr2==null || maxBranchAttr2.equals("") )
        {
            maxBranchAttr2=mUpBranchAttr+"000000000000000".substring(0,Integer.parseInt(cuBranchLevelSchema.getBranchLevelProperty2())-mUpBranchAttr.length() );
        }

        BigInteger tBigInteger1 = new BigInteger(maxBranchAttr2);
        String tCount =String.valueOf(mCount);
        if (tBigInteger1.compareTo(tBigInteger) > 0)
        {

            BigInteger tAdd=new BigInteger(tCount);
            tBigInteger1=tBigInteger1.add(tAdd);
            maxBranchAttr=tBigInteger1.toString();
        }
        else
        {
            BigInteger tAdd=new BigInteger(tCount);
           tBigInteger=tBigInteger.add(tAdd);
           maxBranchAttr=tBigInteger.toString();

        }
        String tBranchSeries = "";
        tBranchSeries = AgentPubFun.getBranchSeries(mAimLABranchSchema.
                                                    getAgentGroup());
        if (tBranchSeries == null || tBranchSeries.equals(""))
        {
            tBranchSeries = mAdjustLABranchSchema.getAgentGroup();
        }
        else
        {
            int SubLevel = upBranchLevelSchema.getBranchLevelID() -
                           cuBranchLevelSchema.getBranchLevelID();
            while (SubLevel > 1)
            {
                tBranchSeries = tBranchSeries + ":" +
                                "000000000000";
                SubLevel--;
            }
            tBranchSeries +=":"+mAdjustLABranchSchema.getAgentGroup();
        }
        System.out.println("maxBranchAttr:"+maxBranchAttr) ;
        NewBranchSeries = tBranchSeries;// 新的团队序列 branchseries
        return maxBranchAttr;
    }

    private boolean dealBranchManager(LABranchGroupSchema cLABranchGroupSchema)
    {
        String upBranch = "";
        String upAgent = "";
        upBranch = mAimLABranchSchema.getAgentGroup();
        upAgent = mAimLABranchSchema.getBranchManager();
        LABranchGroupBSchema tLABranchGroupBSchema = new LABranchGroupBSchema();
        Reflections tReflections = new Reflections();
        tReflections.transFields(tLABranchGroupBSchema, cLABranchGroupSchema);
        tLABranchGroupBSchema.setEdorNo(mEdorNo);
        tLABranchGroupBSchema.setEdorType("02");
        tLABranchGroupBSchema.setOperator2(cLABranchGroupSchema.getOperator());
        tLABranchGroupBSchema.setOperator(mGlobalInput.Operator);
        tLABranchGroupBSchema.setMakeDate2(cLABranchGroupSchema.getMakeDate());
        tLABranchGroupBSchema.setMakeTime2(cLABranchGroupSchema.getMakeTime());
        tLABranchGroupBSchema.setModifyDate2(cLABranchGroupSchema.getModifyDate() );
        tLABranchGroupBSchema.setModifyTime2(cLABranchGroupSchema.getModifyTime());
        tLABranchGroupBSchema.setMakeDate(currentDate);
        tLABranchGroupBSchema.setMakeTime(currentTime);
        tLABranchGroupBSchema.setModifyDate(currentDate);
        tLABranchGroupBSchema.setModifyTime(currentTime);
//        tLABranchGroupBSchema.setAStartDate(mAdjustDate);  //备份表不能修改日期 xiangchun 2005-12-16

        this.mLABranchGroupBSet.add(tLABranchGroupBSchema);
        String branchAttr = cLABranchGroupSchema.getBranchAttr();

        cLABranchGroupSchema.setBranchAttr(NewBranchAttr);
        cLABranchGroupSchema.setBranchSeries(NewBranchSeries) ;

        if (this.ifChangeManageCom)
        {
            cLABranchGroupSchema.setManageCom(mAimLABranchSchema.getManageCom());
        }
        cLABranchGroupSchema.setUpBranch(upBranch);
        cLABranchGroupSchema.setModifyDate(currentDate);
        cLABranchGroupSchema.setModifyTime(currentTime);
        cLABranchGroupSchema.setOperator(mGlobalInput.Operator);
        cLABranchGroupSchema.setAStartDate(mAdjustDate);
        // tianjian    20120531
        cLABranchGroupSchema.setCostCenter(mAimLABranchSchema.getCostCenter());
        
        this.mLABranchGroupSet.add(cLABranchGroupSchema);
        //如果存在下属机构，如果区调动的话 下属处的主管人员应该变更astartdate 主管不为空
        String sql="select * from labranchgroup where upbranch='"+cLABranchGroupSchema.getAgentGroup()+"' and branchtype='1' and branchtype2='01' and branchmanager is not null  and  branchmanager<>''  and upbranchattr='0' and (state is null or state<>'1') and (endflag is null or endflag<>'Y')";
        System.out.println("打印出来的SQL为"+sql);
        LABranchGroupDB tempDB=new LABranchGroupDB();
        LABranchGroupSet tempSet = new LABranchGroupSet();
        tempSet = tempDB.executeQuery(sql);
        for (int i = 1; i <= tempSet.size(); i++)
       {
           LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
           tLABranchGroupSchema = tempSet.get(i);
           LATreeDB tLATreeDB = new LATreeDB();
           
           tLATreeDB.setAgentCode(tLABranchGroupSchema.getBranchManager());
           
           tLATreeDB.getInfo();
           LATreeBSchema tLATreeBSchema = new LATreeBSchema();
           tReflections.transFields(tLATreeBSchema, tLATreeDB.getSchema());
           tLATreeBSchema.setEdorNO(mEdorNo);
           tLATreeBSchema.setRemoveType("02");// 备份主管信息 edortype = 02
           tLATreeBSchema.setMakeDate2(tLATreeDB.getMakeDate());
           tLATreeBSchema.setMakeTime2(tLATreeDB.getMakeTime());
           tLATreeBSchema.setModifyDate2(tLATreeDB.getModifyDate());
           tLATreeBSchema.setModifyTime2(tLATreeDB.getModifyTime());
           tLATreeBSchema.setOperator2(tLATreeDB.getOperator());
           tLATreeBSchema.setMakeDate(currentDate);
           tLATreeBSchema.setMakeTime(currentTime);
           tLATreeBSchema.setModifyDate(currentDate);
           tLATreeBSchema.setModifyTime(currentTime);
           tLATreeBSchema.setOperator(mGlobalInput.Operator);
           this.mLATreeBSet.add(tLATreeBSchema);
           ////////////////////////////////////////////////////
           tLATreeDB.setModifyDate(currentDate);
           //下属人员的astartdate都不改了，以免团队调整影响薪资计算
          //tLATreeDB.setAstartDate(mAdjustDate);
           tLATreeDB.setModifyTime(currentTime);
           tLATreeDB.setOperator(mGlobalInput.Operator);
           this.mLATreeSet.add(tLATreeDB.getSchema());
         }

        if (mBranchManager != null && !mBranchManager.equals(""))
        {
        	// 不变动laagent表里的数据
            if (ifChangeManageCom)
            {
                LAAgentSchema tLAAgentSchema = new LAAgentSchema();
                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(mBranchManager);
                tLAAgentDB.getInfo();
                tLAAgentSchema = tLAAgentDB.getSchema();
                LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
                tReflections.transFields(tLAAgentBSchema, tLAAgentSchema);
                tLAAgentBSchema.setEdorNo(mEdorNo);
                tLAAgentBSchema.setEdorType("02");
                tLAAgentBSchema.setMakeDate(currentDate);
                tLAAgentBSchema.setMakeTime(currentTime);
                this.mLAAgentBSet.add(tLAAgentBSchema);
                tLAAgentSchema.setManageCom(mAimLABranchSchema.getManageCom());
                tLAAgentSchema.setModifyDate(currentDate);
                tLAAgentSchema.setModifyTime(currentTime);
                tLAAgentSchema.setOperator(mGlobalInput.Operator);
                this.mLAAgentSet.add(tLAAgentSchema);
            }
            LATreeDB tLATreeDB = new LATreeDB();
            tLATreeDB.setAgentCode(mBranchManager);
            tLATreeDB.getInfo();
            LATreeBSchema tLATreeBSchema = new LATreeBSchema();
            tReflections.transFields(tLATreeBSchema, tLATreeDB.getSchema());
            tLATreeBSchema.setEdorNO(mEdorNo);
            tLATreeBSchema.setRemoveType("02");
            tLATreeBSchema.setMakeDate2(tLATreeDB.getMakeDate());
            tLATreeBSchema.setMakeTime2(tLATreeDB.getMakeTime());
            tLATreeBSchema.setModifyDate2(tLATreeDB.getModifyDate());
            tLATreeBSchema.setModifyTime2(tLATreeDB.getModifyTime());
            tLATreeBSchema.setOperator2(tLATreeDB.getOperator());
            tLATreeBSchema.setMakeDate(currentDate);
            tLATreeBSchema.setMakeTime(currentTime);
            tLATreeBSchema.setModifyDate(currentDate);
            tLATreeBSchema.setModifyTime(currentTime);
            tLATreeBSchema.setOperator(mGlobalInput.Operator);
            this.mLATreeBSet.add(tLATreeBSchema);

            tLATreeDB.setManageCom(mAimLABranchSchema.getManageCom());
            tLATreeDB.setUpAgent(upAgent);
            tLATreeDB.setModifyDate(currentDate);
            //tLATreeDB.setAstartDate(mAdjustDate);
            tLATreeDB.setModifyTime(currentTime);
            tLATreeDB.setOperator(mGlobalInput.Operator);
            this.mLATreeSet.add(tLATreeDB.getSchema());

            LACommisionSet tLACommisionSet = new LACommisionSet();
            String aasql =
                    "select * from lacommision where agentcode='"+mBranchManager+"' and branchtype='" + mAdjustLABranchSchema.getBranchType() +
                    "' and branchType2='"
                    + mAdjustLABranchSchema.getBranchType2() +
                    "' and (caldate is null or caldate>='"
                    + this.mAdjustDate + "')";
            System.out.println(aasql);
            LACommisionDB tLACommisionDB = new LACommisionDB();
            tLACommisionSet = tLACommisionDB.executeQuery(aasql);
            for (int j = 1; j <= tLACommisionSet.size(); j++)
            {
                LACommisionSchema tLACommisionSchema = new LACommisionSchema();
                tLACommisionSchema = tLACommisionSet.get(j);
                String BranchAttr = tLACommisionSchema.getBranchAttr();
                String BranchSeries=tLACommisionSchema.getBranchSeries();
                System.out.println("BranchAttr1:" + BranchSeries);
                System.out.println("BranchAttr1:" + NewBranchSeries);
                BranchSeries=this.NewBranchSeries+BranchSeries.substring(NewBranchSeries.length() ) ;
                System.out.println("BranchAttr:" + BranchSeries);
                System.out.println("123:" + BranchAttr.substring(this.NewBranchAttr.length()));

                tLACommisionSchema.setBranchAttr(NewBranchAttr +BranchAttr.substring(this.NewBranchAttr.length()));
                tLACommisionSchema.setBranchSeries(BranchSeries) ;
                tLACommisionSchema.setManageCom(mAimLABranchSchema.getManageCom());
                tLACommisionSchema.setModifyDate(currentDate);
                tLACommisionSchema.setModifyTime(currentTime);
                this.mLACommisionSet.add(tLACommisionSchema);
            }
        }
        return true;

    }

    private boolean dealEachBranchGroup(LABranchGroupSchema
                                        cLABranchGroupSchema,String tFlag)
    {
        String curBranchAttr = cLABranchGroupSchema.getBranchAttr();
        String branchlevel = cLABranchGroupSchema.getBranchLevel();
        Reflections tReflections = new Reflections();
        
        // 只处理未停业的 其实现在系统最多只处理一次
        String sql = "select * from labranchgroup where branchattr ='" +
                     curBranchAttr +
                     "' and branchtype='" + cLABranchGroupSchema.getBranchType() +
                     "' and BranchType2='" + cLABranchGroupSchema.getBranchType2()
                     + "' and (state is null or state<>'1') and (endflag is null or endflag<>'Y') ";
        System.out.println(sql);
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(sql);
        for (int i = 1; i <= tLABranchGroupSet.size(); i++)
        {
            LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
            LABranchGroupBSchema tLABranchGroupBSchema = new    LABranchGroupBSchema();
            tLABranchGroupSchema = tLABranchGroupSet.get(i);
            tReflections.transFields(tLABranchGroupBSchema,tLABranchGroupSchema);
            tLABranchGroupBSchema.setEdorNo(mEdorNo);
            tLABranchGroupBSchema.setEdorType("02");// 团队调动标志
            tLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.getOperator());
            tLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.getMakeDate());
            tLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.getMakeTime());
            tLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.getModifyDate());
            tLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.getModifyTime());
            tLABranchGroupBSchema.setMakeDate(currentDate);
            tLABranchGroupBSchema.setMakeTime(currentTime);
            tLABranchGroupBSchema.setModifyDate(currentDate);
            tLABranchGroupBSchema.setModifyTime(currentTime);
            tLABranchGroupBSchema.setOperator(mGlobalInput.Operator);
   //         tLABranchGroupBSchema.setEndDate(mAdjustDate);        //没有停业日期，xiangchun 2005-12-16
            
            if (tFlag.equals("0"))// tflag = 0 表示 下属团队
            {
                this.mLABranchGroupBSet.add(tLABranchGroupBSchema);//备份下属团队信息
            }

            String branchAttr = tLABranchGroupSchema.getBranchAttr();//得到下属团队外部编码
            String newBranchAttr = NewBranchAttr +branchAttr.substring(this.NewBranchAttr.length());//沿用原来的处级外部编码。。。
            System.out.println("branchAttr:"+branchAttr) ;
            System.out.println("newBranchAttr:"+newBranchAttr) ;
            tLABranchGroupSchema.setBranchAttr(newBranchAttr);
            String BranchSeries=tLABranchGroupSchema.getBranchSeries() ;
            BranchSeries=this.NewBranchSeries+BranchSeries.substring(NewBranchSeries.length() ) ;
            tLABranchGroupSchema.setManageCom(mAimLABranchSchema.getManageCom());
            tLABranchGroupSchema.setBranchSeries(BranchSeries) ;
            tLABranchGroupSchema.setModifyDate(currentDate);
            tLABranchGroupSchema.setModifyTime(currentTime);
            tLABranchGroupSchema.setOperator(mGlobalInput.Operator);
            tLABranchGroupSchema.setAStartDate(mAdjustDate);// 团队调整日期
            // 增加 costcenter 字段       fanting  2012-5-31
            tLABranchGroupSchema.setCostCenter(mAimLABranchSchema.getCostCenter());
            if(tFlag.equals("0"))
            {
                this.mLABranchGroupSet.add(tLABranchGroupSchema);//修改下属团队行政信息
            }
            LACommisionSet tLACommisionSet = new LACommisionSet();
            String aasql =
                    "select * from lacommision where branchcode = '" +
                    tLABranchGroupSchema.getAgentGroup() +
                    "' and (caldate is null or caldate>='"
                    + this.mAdjustDate + "') and agentcode<>'"+mBranchManager+"'";// mBranchManager 被调团队主管	
            System.out.println(aasql);
            LACommisionDB tLACommisionDB = new LACommisionDB();
            tLACommisionSet = tLACommisionDB.executeQuery(aasql);
            for (int j = 1; j <= tLACommisionSet.size(); j++)
            {
                LACommisionSchema tLACommisionSchema = new LACommisionSchema();
                tLACommisionSchema = tLACommisionSet.get(j);
                String BranchAttr = tLACommisionSchema.getBranchAttr();
                tLACommisionSchema.setBranchAttr(newBranchAttr) ;
                tLACommisionSchema.setBranchSeries(BranchSeries) ;
                tLACommisionSchema.setManageCom(mAimLABranchSchema.getManageCom());
                tLACommisionSchema.setModifyDate(currentDate);
                tLACommisionSchema.setModifyTime(currentTime);
                this.mLACommisionSet.add(tLACommisionSchema);
            }
             // 这一块还需要好好理解理解
            if (this.ifChangeManageCom &&tLABranchGroupSchema.getBranchLevel().equals("01"))
            {
                String asql = "select * from laagent where branchcode ='" +
                              tLABranchGroupSchema.getAgentGroup() +
                              "' and agentstate<'06' ";
                if (mBranchManager != null && !mBranchManager.equals(""))
                {
                    asql = asql + "and agentcode<>'" + this.mBranchManager +
                           "'";
                }
                System.out.println(asql);
                LAAgentSet tLAAgentSet = new LAAgentSet();
                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentSet = tLAAgentDB.executeQuery(asql);
                for (int j = 1; j <= tLAAgentSet.size(); j++)
                {
                    LAAgentSchema tLAAgentSchema = new LAAgentSchema();
                    tLAAgentSchema = tLAAgentSet.get(j);
                    System.out.println(j + "当前处理机构:" +
                                       tLABranchGroupSchema.getBranchAttr());
                    System.out.println(j + "当前处理人员:" +
                                       tLAAgentSchema.getAgentCode());

                    LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
                    tReflections.transFields(tLAAgentBSchema, tLAAgentSchema);
                    tLAAgentBSchema.setEdorNo(mEdorNo);
                    tLAAgentBSchema.setEdorType("02");// 02 表示团队调动操作
                    tLAAgentBSchema.setMakeDate(currentDate);
                    tLAAgentBSchema.setMakeTime(currentTime);
                    tLAAgentBSchema.setModifyDate(currentDate);
                    tLAAgentBSchema.setModifyTime(currentTime);
                    tLAAgentBSchema.setOperator(mGlobalInput.Operator);
                    this.mLAAgentBSet.add(tLAAgentBSchema);

                    tLAAgentSchema.setManageCom(mAimLABranchSchema.getManageCom());
                    tLAAgentSchema.setModifyDate(currentDate);
                    tLAAgentSchema.setModifyTime(currentTime);
                    tLAAgentSchema.setOperator(mGlobalInput.Operator);
                    this.mLAAgentSet.add(tLAAgentSchema);
                    //////////////////////////////////////////////////
                    LATreeDB tLATreeDB = new LATreeDB();
                    tLATreeDB.setAgentCode(tLAAgentSchema.getAgentCode());
                    tLATreeDB.getInfo();
                    LATreeBSchema tLATreeBSchema = new LATreeBSchema();
                    tReflections.transFields(tLATreeBSchema,
                                             tLATreeDB.getSchema());
                    tLATreeBSchema.setEdorNO(mEdorNo);
                    tLATreeBSchema.setRemoveType("02");
                    tLATreeBSchema.setMakeDate2(tLATreeDB.getMakeDate());
                    tLATreeBSchema.setMakeTime2(tLATreeDB.getMakeTime());
                    tLATreeBSchema.setModifyDate2(tLATreeDB.getModifyDate());
                    tLATreeBSchema.setModifyTime2(tLATreeDB.getModifyTime());
                    tLATreeBSchema.setOperator2(tLATreeDB.getOperator());
                    tLATreeBSchema.setMakeDate(currentDate);
                    tLATreeBSchema.setMakeTime(currentTime);
                    tLATreeBSchema.setModifyDate(currentDate);
                    tLATreeBSchema.setModifyTime(currentTime);
                    tLATreeBSchema.setOperator(mGlobalInput.Operator);
                    this.mLATreeBSet.add(tLATreeBSchema);
                    ////////////////////////////////////////////////////
                    tLATreeDB.setManageCom(mAimLABranchSchema.getManageCom());
                    //业务人员不改
                    //tLATreeDB.setAstartDate(mAdjustDate);
                    tLATreeDB.setModifyDate(currentDate);
                    tLATreeDB.setModifyTime(currentTime);
                    tLATreeDB.setOperator(mGlobalInput.Operator);
                    this.mLATreeSet.add(tLATreeDB);
                    /////////////////////////////////////////////

                }
            }

        }
        return true;
    }

    private boolean chgRelation(){
        //处理育成培养关系
//        LARearPICCHBL tLARearPICCHBL = new LARearPICCHBL();
//        tLARearPICCHBL.setLATreeSchema(mLATreeSet.get(1));
//        if (!tLARearPICCHBL.invalidateRear(IsSingle,mAdjustDate)) {
//            mErrors.copyAllErrors(tLARearPICCHBL.mErrors);
//            return false;
//        }
//        MMap tMap2 = tLARearPICCHBL.getResult();
//        mMap.add(tMap2);
//
//        LARecomPICCHBL tLARecomPICCHBL = new LARecomPICCHBL();
//        tLARecomPICCHBL.setLATreeSchema(mLATreeSet.get(1));
//        if (!tLARecomPICCHBL.invalidateRecom(false,mAdjustDate)) {
//            mErrors.copyAllErrors(tLARecomPICCHBL.mErrors);
//            return false;
//        }
//        MMap tMap1 = tLARecomPICCHBL.getResult();
//        mMap.add(tMap1);
//        //失效与推荐人的关系

        String tBranchlevel=mAdjustLABranchSchema.getBranchLevel(); //得到被调动机构的级别

        if(IsSingle)
        {
           //断裂原有关系
            LARearRelationSet tLARearRelationSet = new LARearRelationSet();
            LARearRelationDB tLARearRelationDB = new LARearRelationDB();
            //查询本人为被育成人的直接培养关系
            String sql = "select  * from larearrelation  where agentcode= '" +
            mBranchManager +"'  ";
            tLARearRelationSet = tLARearRelationDB.executeQuery(sql);
            mLARearRelationSet.add(tLARearRelationSet);
            tLARearRelationSet = new LARearRelationSet();
            //查询本人为育成人的直接培养关系
            sql = "select  * from larearrelation  where rearagentcode= '" +
            mBranchManager + "'  ";
            tLARearRelationSet = tLARearRelationDB.executeQuery(sql);
            mLARearRelationSet.add(tLARearRelationSet);

            if (mLARearRelationSet != null && mLARearRelationSet.size() >= 1) 
            {
                for (int i = 1; i <= mLARearRelationSet.size(); i++) {
                    Reflections tReflections = new Reflections();
                    LARearRelationBSchema tLARearRelationBSchema = new LARearRelationBSchema();
                    tReflections.transFields(tLARearRelationBSchema,mLARearRelationSet.get(i));
                    tLARearRelationBSchema.setMakeDate2(mLARearRelationSet.get(i).getMakeDate());
                    tLARearRelationBSchema.setMakeTime2(mLARearRelationSet.get(i).getModifyTime());
                    tLARearRelationBSchema.setModifyDate2(mLARearRelationSet.get(i).getModifyDate());
                    tLARearRelationBSchema.setModifyTime2(mLARearRelationSet.get(i).getModifyTime());
                    tLARearRelationBSchema.setOperator2(mLARearRelationSet.get(i).getOperator());

                    tLARearRelationBSchema.setMakeDate(currentDate);
                    tLARearRelationBSchema.setMakeTime(currentTime);
                    tLARearRelationBSchema.setModifyDate(currentDate);
                    tLARearRelationBSchema.setModifyTime(currentTime);
                    tLARearRelationBSchema.setOperator(mGlobalInput.Operator);

                    tLARearRelationBSchema.setEdorNo(mEdorNo);
                    tLARearRelationBSchema.setEdorType("02");
                    mLARearRelationBSet.add(tLARearRelationBSchema);
                }
            }
        }
        else
        {
            //带走原有关系

            LARearRelationSet tLARearRelationSet = new LARearRelationSet();
            LARearRelationDB tLARearRelationDB = new LARearRelationDB();
            //查询本人为被育成人的培养关系
            String sql = "select  * from larearrelation  where agentcode= '" +
            mBranchManager + "'  ";
            System.out.println("被育成关系 断裂"+sql);
            tLARearRelationSet = tLARearRelationDB.executeQuery(sql);

            mLARearRelationSet.add(tLARearRelationSet);
            //调团队时 也会把相应的育成人也调离
            tLARearRelationSet = new LARearRelationSet();
            sql  = "select * from larearrelation  where rearagentcode in " //rearagentcode 育成人
                        + "(select rearagentcode from larearrelation b "
                        + "where  b.agentcode='" +mBranchManager + "') "
                        +
                        "and agentcode in (select agentcode from larearrelation b "
                        + "where  b.rearagentcode='" + mBranchManager + "')";
            System.out.println("二阶育成关系 断裂"+sql);
            tLARearRelationSet = tLARearRelationDB.executeQuery(sql);
            mLARearRelationSet.add(tLARearRelationSet);
            if (mLARearRelationSet != null && mLARearRelationSet.size() >= 1) {
                for (int i = 1; i <= mLARearRelationSet.size(); i++) {
                    Reflections tReflections = new Reflections();
                    LARearRelationBSchema tLARearRelationBSchema = new LARearRelationBSchema();
                    tReflections.transFields(tLARearRelationBSchema,mLARearRelationSet.get(i));
                    tLARearRelationBSchema.setMakeDate2(mLARearRelationSet.get(i).getMakeDate());
                    tLARearRelationBSchema.setMakeTime2(mLARearRelationSet.get(i).getModifyTime());
                    tLARearRelationBSchema.setModifyDate2(mLARearRelationSet.get(i).getModifyDate());
                    tLARearRelationBSchema.setModifyTime2(mLARearRelationSet.get(i).getModifyTime());
                    tLARearRelationBSchema.setOperator2(mLARearRelationSet.get(i).getOperator());

                    tLARearRelationBSchema.setMakeDate(currentDate);
                    tLARearRelationBSchema.setMakeTime(currentTime);
                    tLARearRelationBSchema.setModifyDate(currentDate);
                    tLARearRelationBSchema.setModifyTime(currentTime);
                    tLARearRelationBSchema.setOperator(mGlobalInput.Operator);

                    tLARearRelationBSchema.setEdorNo(mEdorNo);
                    tLARearRelationBSchema.setEdorType("02");
                    mLARearRelationBSet.add(tLARearRelationBSchema);
                }
            }

            //调走其所培养的团队
            tLARearRelationSet = new LARearRelationSet();
            tLARearRelationDB = new LARearRelationDB();
            //查询本人为育成人的培养关系,这些团队需要调动团队
            sql = "select  * from larearrelation  where rearagentcode= '" +
                mBranchManager+ "' and rearlevel= '"+tBranchlevel+"'";
            tLARearRelationSet = tLARearRelationDB.executeQuery(sql);
            System.out.println(sql);

            if (tLARearRelationSet != null && tLARearRelationSet.size() >= 1) {
                for (int i = 1; i <= tLARearRelationSet.size(); i++) {

                    LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                    LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
                    //tLABranchGroupDB.setAgentGroup(tLARearRelationSet.get(i).getAgentGroup());
                    sql = "select agentgroup from laagent where agentcode = '"+tLARearRelationSet.get(i).getAgentCode()+"'";
                    System.out.println("育成人查询SQL如下:"+sql);
                    ExeSQL tExeSQL = new ExeSQL();
                    String tAgentGroup = tExeSQL.getOneValue(sql);
                    tLABranchGroupDB.setAgentGroup(tAgentGroup);
                    
                    if(!tLABranchGroupDB.getInfo())
                    {
                    	CError tError = new CError();
                        tError.moduleName = "ChangeBranchNewBL";
                        tError.functionName = "chgRelation";
                        tError.errorMessage =
                                "未查询到育成人";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    tLABranchGroupSchema = tLABranchGroupDB.getSchema();
                    mAdjustLABranchSchema = new LABranchGroupSchema();
                    mAdjustLABranchSchema.setAgentGroup(tLABranchGroupSchema.getAgentGroup());
                    mAdjustLABranchSchema.setBranchLevel(tLABranchGroupSchema.getBranchLevel());
                    mAdjustLABranchSchema.setUpBranch(tLABranchGroupSchema.getUpBranch());
                    mAdjustLABranchSchema.setBranchAttr(tLABranchGroupSchema.getBranchAttr()); //增加了修改此属性
                    mAdjustLABranchSchema.setBranchType(mBranchType);
                    mAdjustLABranchSchema.setBranchType2(mBranchType2);
                    if (!check()) return false;
                    if (!dealData()) 
                    {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ChangeBranchNewBL";
                        tError.functionName = "chgRelation";
                        tError.errorMessage =
                                "数据处理失败ChangeBranchNewBL-->dealData!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
            }

        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
         
            this.mMap.put(mLABranchGroupSet, "UPDATE");
            this.mMap.put(mLABranchGroupBSet, "INSERT");
            this.mMap.put(mLATreeSet, "UPDATE");
            this.mMap.put(mLATreeBSet, "INSERT");
            this.mMap.put(mLAAgentBSet, "INSERT");
            this.mMap.put(mLAAgentSet, "UPDATE");
            this.mMap.put(mLACommisionSet, "UPDATE");
            this.mMap.put(mLARearRelationSet, "DELETE");
            this.mMap.put(mLARearRelationBSet, "INSERT");
           // this.mInputData.add(this.mMap);

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ChangeBranchNewBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    private boolean prepareOutputData1()
    {
        try
        {
        	this.mInputData = new VData();
            this.mInputData.add(this.mMap);

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ChangeBranchNewBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    public VData getResult()
    {
        mResult=new VData();
        mResult.add(NewBranchAttr) ;
        return this.mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        this.mAdjustLABranchSchema.setSchema((LABranchGroupSchema) cInputData.
                                             getObjectByObjectName(
                "LABranchGroupSchema", 1));// 调动团队信息
        this.mAimLABranchSchema.setSchema((LABranchGroupSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LABranchGroupSchema", 2));// 调往团队信息
        this.mAdjustDate = (String) cInputData.getObject(3);
        TransferData tdata = (TransferData) cInputData.getObjectByObjectName("TransferData",0);
        String ts = (String) tdata.getValueByName("IsSingle"); // 是否带走关系
        IsSingle = ts.equals("0")?true:false;
        System.out.println("这个能打印出来什么啊！"+IsSingle);
        mBranchType=mAdjustLABranchSchema.getBranchType();
        mBranchType2=mAdjustLABranchSchema.getBranchType2();
        System.out.println("mAdjustDate:" + mAdjustDate);
        System.out.println("over getInputData.." +this.mAdjustLABranchSchema.getAgentGroup());
        return true;
    }
  private boolean dealGbuild()
  {
	  System.out.println("-------->>>>>>"+this.mLABranchGroupSet.size());
	  for(int i = 1;i<=this.mLABranchGroupSet.size();i++)
	  {
		  LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
		  tLABranchGroupSchema =this.mLABranchGroupSet.get(i).getSchema();
		  String sql = "update labranchgroup set applygbflag= null,applygbstartdate= null,gbuildflag = null,gbuildstartdate= null,gbuildenddate = null" +
 			",modifydate = current date,modifytime = current time where agentgroup = '"+tLABranchGroupSchema.getAgentGroup()+"'";
 	      mMap.put(sql, "UPDATE");
 	     
 	     LAAgentDB tLAAgentDB = new LAAgentDB();
 	     LAAgentSet tLAAgentSet = new LAAgentSet();
 	     String getAgent ="select * from laagent where agentgroup = '"+tLABranchGroupSchema.getAgentGroup()+"' and agentstate<'06'";
 	   	  tLAAgentSet = tLAAgentDB.executeQuery(getAgent);
 	      if(!dealAgent(tLAAgentSet)) return false;// 处理调动主管
 
	  }
	  return true;
  }
  private boolean dealAgent(LAAgentSet tLAAgentSet)
  {
	  for(int i = 1;i<=tLAAgentSet.size();i++)
 	    {
 	    	LAAgentSchema tLAAgentSchema = new LAAgentSchema();
 	    	tLAAgentSchema =tLAAgentSet.get(i).getSchema();

 	    	if(!backAgent(tLAAgentSchema))
 	        {
 	    		return false;
 	        }
 	    	String updateAgent = "update laagent set noworkflag = 'N',traindate = null,wageversion = '2015',GBuildFlag=null,GBuildStartDate =null,GBuildEndDate=null," +
 	    			"modifydate = current date,modifytime = current time where agentcode = '"+tLAAgentSchema.getAgentCode()+"'";
 	    	mMap.put(updateAgent, "UPDATE");
 	    	
 	    	String updateTree = "update latree set wageversion = '2015',modifydate = current date,modifytime = current time where agentcode = '"+tLAAgentSchema.getAgentCode()+"'";
 	    	mMap.put(updateTree, "UPDATE");
 	    }
	  return true;
  }
  private boolean backAgent(LAAgentSchema tLAAgentSchema)
  {
   
		  Reflections tReflections = new Reflections();
		  LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
	      tReflections.transFields(tLAAgentBSchema, tLAAgentSchema);
	      tLAAgentBSchema.setEdorNo(mEdorNo);
	      tLAAgentBSchema.setEdorType("02");// 02 表示团队调动操作
	      tLAAgentBSchema.setMakeDate(currentDate);
	      tLAAgentBSchema.setMakeTime(currentTime);
	      tLAAgentBSchema.setModifyDate(currentDate);
	      tLAAgentBSchema.setModifyTime(currentTime);
	      tLAAgentBSchema.setOperator(mGlobalInput.Operator);
	      this.mLAAgentBSet.add(tLAAgentBSchema);

	      LATreeBSet tLATreeBSet = new LATreeBSet();
	      tLATreeBSet.add(this.mLATreeBSet);
	      for(int i=1;i<=tLATreeBSet.size();i++)
	      {
	    	  if(tLATreeBSet.get(i).getAgentCode().equals(tLAAgentSchema.getAgentCode()))
	    	  {

	    		   return true;
	    		
	    	  }
	    	
	      }
	      LATreeDB tLATreeDB = new LATreeDB();
	      tLATreeDB.setAgentCode(tLAAgentSchema.getAgentCode());
	      tLATreeDB.getInfo();
	      LATreeBSchema tLATreeBSchema = new LATreeBSchema();
	      tReflections.transFields(tLATreeBSchema,tLATreeDB.getSchema());
	      tLATreeBSchema.setEdorNO(mEdorNo);
	      tLATreeBSchema.setRemoveType("02");
	      tLATreeBSchema.setMakeDate2(tLATreeDB.getMakeDate());
	      tLATreeBSchema.setMakeTime2(tLATreeDB.getMakeTime());
	      tLATreeBSchema.setModifyDate2(tLATreeDB.getModifyDate());
	      tLATreeBSchema.setModifyTime2(tLATreeDB.getModifyTime());
	      tLATreeBSchema.setOperator2(tLATreeDB.getOperator());
	      tLATreeBSchema.setMakeDate(currentDate);
	      tLATreeBSchema.setMakeTime(currentTime);
	      tLATreeBSchema.setModifyDate(currentDate);
	      tLATreeBSchema.setModifyTime(currentTime);
	      tLATreeBSchema.setOperator(mGlobalInput.Operator);
	      this.mLATreeBSet.add(tLATreeBSchema);

	  return true;
  }
}
