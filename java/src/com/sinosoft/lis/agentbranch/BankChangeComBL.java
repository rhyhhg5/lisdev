package com.sinosoft.lis.agentbranch;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

import com.sinosoft.lis.db.LAComToAgentDB;
import com.sinosoft.lis.db.LAComToAgentBDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAComToAgentBSchema;
import com.sinosoft.lis.schema.LAComToAgentSchema;
import com.sinosoft.lis.vschema.LAComToAgentBSet;
import com.sinosoft.lis.vschema.LAComToAgentSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;
import java.math.BigInteger;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import sun.reflect.Reflection;

public class BankChangeComBL {
        //错误处理类
       public CErrors mErrors = new CErrors(); //错误处理类
        //业务处理相关变量
        /** 全局数据 */
        private VData mInputData = new VData();
        private String mOperate = "";
        private String CurrentDate = PubFun.getCurrentDate();
        private String CurrentTime = PubFun.getCurrentTime();
        private String mAdjustDate = "";
        private String mAgentCode= "";
        private MMap map = new MMap();
        public  GlobalInput mGlobalInput = new GlobalInput();
        private LAComToAgentSet maLAComToAgentSet = new LAComToAgentSet();
        private LAComToAgentSet mlLAComToAgentSet = new LAComToAgentSet();
        private LAComToAgentSet tLAComToAgentSet = new LAComToAgentSet();
        private LAComToAgentSchema maLAComToAgentSchema= new LAComToAgentSchema();
        private LAComToAgentSchema mlLAComToAgentSchema=  new LAComToAgentSchema();
        private LAComToAgentSchema mbLAComToAgentSchema=  new LAComToAgentSchema();
        private Reflections ref = new Reflections();
        private VData mResult = new VData();
        private String mMakeContNum="";


        /**
          传输数据的公共方法
         */
        public boolean submitData(VData cInputData, String cOperate) {
          System.out.println("Begin BankChangeComBL.submitData.........");
          //将操作数据拷贝到本类中
          this.mOperate = cOperate;
          //得到外部传入的数据,将数据备份到本类中
          if (!getInputData(cInputData)) {
            return false;
          }
          //进行业务处理
          if (!dealData()) {
            return false;
          }
          //准备往后台的数据
          if (!prepareOutputData()) {
            return false;
          }
          PubSubmit tPubSubmit = new PubSubmit();
          System.out.println("Start BankChangeComBL Submit...");
          if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "BankChangeComBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
          }
          return true;
        }

        /**
         * 从输入数据中得到所有对象
         *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
         */
        private boolean getInputData(VData cInputData) {
          //全局变量
          try {
            System.out.println("Begin BankChangeComBL.getInputData.........");
            this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                        getObjectByObjectName("GlobalInput", 0));
           System.out.println("GlobalInput get");
           // this.mLARateStandPremSet.set( (LARateStandPremSet) cInputData.get(1));

           this.maLAComToAgentSchema.setSchema((LAComToAgentSchema) cInputData.getObjectByObjectName(
                            "LAComToAgentSchema", 1));
            this.mlLAComToAgentSchema.setSchema((LAComToAgentSchema) cInputData.getObjectByObjectName(
                            "LAComToAgentSchema", 2));



            System.out.println("LAComToAgentSchema get"+maLAComToAgentSchema.getAgentCom());

            System.out.println("LAComToAgentSchema 111get"+mlLAComToAgentSchema.getAgentCom());
          }
          catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankChangeComBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "在读取处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
          }
          System.out.println("getInputData end ");
          return true;
        }

        /**
         * 业务处理主函数
         */
        private boolean dealData() {
          System.out.println("Begin BankChangeComBL.dealData........."+mOperate);
          mMakeContNum = PubFun1.CreateMaxNo("EdorNo", 20);
          try {
            if (mOperate.equals("UPDATE||INSERT") )
            {
               mAdjustDate=mlLAComToAgentSchema.getStartDate();
              if((mAdjustDate.compareTo(CurrentDate))>0)
                  {
              //调整日期不能大于当前日期
                CError tError = new CError();
                tError.moduleName = "BankChangeComBL";
                tError.functionName = "dealData";
                tError.errorMessage = "调整日期不能大于当前日期！";
                this.mErrors.addOneError(tError);
                return false;
                  }

              else if((mAdjustDate.compareTo(CurrentDate))==0)
                  {

                     //更新
                  String SQL = "update LAComToAgent set agentcode='"+mlLAComToAgentSchema.getAgentCode()+"'";
                  SQL+=",agentgroup =(select agentgroup from laagent where agentcode='"+mlLAComToAgentSchema.getAgentCode() +"')";
                  SQL+=",operator='"+mGlobalInput.Operator+"'";
                  SQL+=",MakeDate='"+CurrentDate+"'";
                  SQL+=",MakeTime='"+CurrentTime+"'";
                  SQL+=",ModifyDate='"+CurrentDate+"'";
                  SQL+=",ModifyTime='"+CurrentTime+"'";
                  SQL+=",StartDate='"+CurrentDate+"'";
                  SQL+=",EndDate=null";
                  SQL+="  where agentcode='"+maLAComToAgentSchema.getAgentCode()+"'";
                  SQL+="  and relatype='1'  and  agentcom='"+maLAComToAgentSchema.getAgentCom()+"'";
                   map.put(SQL ,"UPDATE");
                   //更新
               String SQL11 = "update LAComToAgent set agentgroup = (select agentgroup from laagent where agentcode='"+mlLAComToAgentSchema.getAgentCode() +"')";
               SQL11+=",operator='"+mGlobalInput.Operator+"'";
               SQL11+=",MakeDate='"+CurrentDate+"'";
               SQL11+=",MakeTime='"+CurrentTime+"'";
               SQL11+=",ModifyDate='"+CurrentDate+"'";
               SQL11+=",ModifyTime='"+CurrentTime+"'";
               SQL11+=",StartDate='"+CurrentDate+"'";
               SQL11+=",EndDate=null";
               SQL11+="  where agentcode='0000000000'";
               SQL11+="  and relatype='0'  and  agentcom='"+maLAComToAgentSchema.getAgentCom()+"'";
                map.put(SQL11 ,"UPDATE");

                       this.mbLAComToAgentSchema=maLAComToAgentSchema;
//                   mbLAComToAgentSchema.setOperator(mGlobalInput.Operator);
//                     mbLAComToAgentSchema.setAgentCode(mlLAComToAgentSchema.getAgentCode());
//                     mbLAComToAgentSchema.setAgentGroup(mlLAComToAgentSchema.getAgentGroup());
//                    mbLAComToAgentSchema.setMakeDate(CurrentDate);
//                   mbLAComToAgentSchema.setMakeTime(CurrentTime);
//                  mbLAComToAgentSchema.setModifyDate(CurrentDate);
//                     mbLAComToAgentSchema.setModifyTime(CurrentTime);
//                     mbLAComToAgentSchema.setStartDate(CurrentDate);
//                     mbLAComToAgentSchema.setEndDate("");
//                     map.put(maLAComToAgentSchema, "DELETE");
//                     map.put(mbLAComToAgentSchema,"INSERT");


                          //先存入备份数据
                LAComToAgentBSchema tLAComToAgentBSchema = new  LAComToAgentBSchema();
                ref.transFields(tLAComToAgentBSchema, maLAComToAgentSchema);

                 //获取最大的ID号
/**                 ExeSQL tExe = new ExeSQL();
                 String tSql = "select int(max(edorno)) from lacomtoagentb order by 1 desc ";
                 String strIdx = "";
                 int tMaxIdx = 0;
                 strIdx = tExe.getOneValue(tSql);
                 if (strIdx == null || strIdx.trim().equals(""))
                 {    tMaxIdx = 0;  }
                 else
                 {
                       tMaxIdx = Integer.parseInt(strIdx);
                            //System.out.println(tMaxIdx);
                   }
                   tMaxIdx++;
                   tLAComToAgentBSchema.setEdorNo(String.valueOf(tMaxIdx));
 */
                   tLAComToAgentBSchema.setEdorNo(mMakeContNum);
                   tLAComToAgentBSchema.setEdorType("02");
                   tLAComToAgentBSchema.setEndDate(PubFun.calDate(CurrentDate,-1,"D",null));
                   map.put(tLAComToAgentBSchema, "INSERT");



                   }

              else if((mAdjustDate.compareTo(CurrentDate))<0)
                  {
               mAgentCode=maLAComToAgentSchema.getAgentCode();
               ExeSQL tExe = new ExeSQL();
               String tSql =
                 "select agentstate from laagent where agentcode='"+mAgentCode+"'";
               String mAgentstate = "";

               mAgentstate = tExe.getOneValue(tSql);
              if ( mAgentstate.equals("01")|| mAgentstate.equals("02"))
                  {
               //原销售人员没有离职
               CError tError = new CError();
               tError.moduleName = "BankChangeComBL";
               tError.functionName = "dealData";
               tError.errorMessage = "调整日期必须为当前日期！";
               this.mErrors.addOneError(tError);
               return false;
                   }
              else
                   {
                 tExe = new ExeSQL();
                 String tSql1 =
                 "select outworkdate from laagent where agentcode='"+mAgentCode+"'";
                 String mOutWorkDate = "";
                 mOutWorkDate = tExe.getOneValue(tSql1);
                 if((mAdjustDate.compareTo(mOutWorkDate)) < 0)
                   {
                           //提示调整日期不能小于离职日期
                     CError tError = new CError();
                     tError.moduleName = "BankChangeComBL";
                     tError.functionName = "dealData";
                    tError.errorMessage = "调整日期不能小于离职日期！";
                    this.mErrors.addOneError(tError);
                    return false;
                    }
                 else
                    {
                            //更新
       String SQL = "update LAComToAgent set agentcode='"+mlLAComToAgentSchema.getAgentCode()+"'";
       SQL+=",agentgroup = (select agentgroup from laagent where agentcode='"+mlLAComToAgentSchema.getAgentCode() +"')";
       SQL+=",operator='"+mGlobalInput.Operator+"'";
       SQL+=",MakeDate='"+CurrentDate+"'";
       SQL+=",MakeTime='"+CurrentTime+"'";
       SQL+=",ModifyDate='"+CurrentDate+"'";
       SQL+=",ModifyTime='"+CurrentTime+"'";
       SQL+=",StartDate='"+CurrentDate+"'";
       SQL+=",EndDate=null";
       SQL+="  where agentcode='"+maLAComToAgentSchema.getAgentCode()+"'";
       SQL+="  and relatype='1'  and  agentcom='"+maLAComToAgentSchema.getAgentCom()+"'";
        map.put(SQL ,"UPDATE");

        //更新
             String SQL22 = "update LAComToAgent set agentgroup = (select agentgroup from laagent where agentcode='"+mlLAComToAgentSchema.getAgentCode() +"')";
             SQL22+=",operator='"+mGlobalInput.Operator+"'";
             SQL22+=",MakeDate='"+CurrentDate+"'";
             SQL22+=",MakeTime='"+CurrentTime+"'";
             SQL22+=",ModifyDate='"+CurrentDate+"'";
             SQL22+=",ModifyTime='"+CurrentTime+"'";
             SQL22+=",StartDate='"+CurrentDate+"'";
             SQL22+=",EndDate=null";
             SQL22+="  where agentcode='0000000000'";
             SQL22+="  and relatype='0'  and  agentcom='"+maLAComToAgentSchema.getAgentCom()+"'";
              map.put(SQL22 ,"UPDATE");

            this.mbLAComToAgentSchema=maLAComToAgentSchema;
//                   mbLAComToAgentSchema.setOperator(mGlobalInput.Operator);
//                     mbLAComToAgentSchema.setAgentCode(mlLAComToAgentSchema.getAgentCode());
//                     mbLAComToAgentSchema.setAgentGroup(mlLAComToAgentSchema.getAgentGroup());
//                    mbLAComToAgentSchema.setMakeDate(CurrentDate);
//                   mbLAComToAgentSchema.setMakeTime(CurrentTime);
//                  mbLAComToAgentSchema.setModifyDate(CurrentDate);
//                     mbLAComToAgentSchema.setModifyTime(CurrentTime);
//                     mbLAComToAgentSchema.setStartDate(CurrentDate);
//                     mbLAComToAgentSchema.setEndDate("");
//                     map.put(maLAComToAgentSchema, "DELETE");
//                     map.put(mbLAComToAgentSchema,"INSERT");


               //先存入备份数据
     LAComToAgentBSchema tLAComToAgentBSchema = new  LAComToAgentBSchema();
     ref.transFields(tLAComToAgentBSchema, maLAComToAgentSchema);

      //获取最大的ID号
/**      ExeSQL mExe = new ExeSQL();
      String mSql = "select int(max(edorno)) from lacomtoagentb order by 1 desc ";
      String strIdx = "";
      int tMaxIdx = 0;
      strIdx = mExe.getOneValue(mSql);
      if (strIdx == null || strIdx.trim().equals(""))
      {    tMaxIdx = 0;  }
      else
      {
            tMaxIdx = Integer.parseInt(strIdx);
                 //System.out.println(tMaxIdx);
        }
        tMaxIdx++;
        tLAComToAgentBSchema.setEdorNo(String.valueOf(tMaxIdx));
 */
  tLAComToAgentBSchema.setEdorNo(mMakeContNum);
        tLAComToAgentBSchema.setEdorType("02");
        tLAComToAgentBSchema.setEndDate(PubFun.calDate(CurrentDate,-1,"D",null));
        map.put(tLAComToAgentBSchema, "INSERT");


                   }
                   }
                   }

                     }
               }

          catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankChangeComBL";
            tError.functionName = "dealData";
            tError.errorMessage = "在处理所数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
          }
          return true;
        }

        /**
         * 准备往后层输出所需要的数据
         * 输出：如果准备数据时发生错误则返回false,否则返回true
         */
        private boolean prepareOutputData() {
          try {
            System.out.println(
                "Begin BankChangeComBL.prepareOutputData.........");
            mInputData.clear();
            mInputData.add(map);
          }
          catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankChangeComBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
          }
          return true;
        }
 public BankChangeComBL() {
    }
    public VData getResult()
      {
          return this.mResult;
      }



}
