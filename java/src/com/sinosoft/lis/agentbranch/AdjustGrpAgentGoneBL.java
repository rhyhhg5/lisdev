/*
 * <p>ClassName: AdjustAgentBL </p>
 * <p>Description: AdjustAgentBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-27
 */
package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeAccessoryDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeAccessorySchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LATreeAccessoryBSet;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.SSRS;




public class AdjustGrpAgentGoneBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private String mOperate;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();

    private String mBranchType="";
    private String mBranchType2="";
    private String mCvaliMonth;
    private String mAgentCode;
    private SSRS mCont;

    private String tEdorNo;
    private String mAimManageCom;
    private String mAStartDate;

    private String mSql_lcgrpcont;
    private String mSql_lcgrppol;
    private String mSql_ljspaygrp;
    private String mSql_ljspay;
    private String mSql_lccont;
    private String mSql_lcpol;
    private String mSql_ljspayperson;
    private String mSql_ljsgetendorse;
    private String mSql_lbgrpcont;
    private String mSql_lbgrppol;
    private String mSql_lbcont;
    private String mSql_lbpol;

    private String mSql_lpgrpcont;
    private String mSql_lpgrppol;
    private String mSql_lpcont;
    private String mSql_lppol;

    private String mSql_ljapaygrp;
    private String mSql_ljapay;
    private String mSql_ljapayperson;
    private String mSql_ljagetendorse;


    /** 业务处理相关变量 */
    private LABranchChangeTempSchema  mLABranchChangeTempSchema=new LABranchChangeTempSchema();
    private LABranchGroupSchema mLABranchSchema = new LABranchGroupSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LATreeSet mLATreeSet = new LATreeSet();

    //更新所有调动人员和管理人员在行政表中的AgentGroup UpBranch
    private LATreeSet mUpdateLATreeSet = new LATreeSet();
    //备份所有调动人员和管理人员
    private LATreeBSet mLATreeBSet = new LATreeBSet();

    //更新所有调动人员在LAAgent表中的AgentGroup
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    //备份代理人记录 add by jiangcx minLABranchGroupBSet
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();

    //备份代理人记录 labranchgroupb表中的branchmanager
    private LABranchGroupBSet minLABranchGroupBSet = new LABranchGroupBSet();
    //更新所有调动人员
    private LABranchGroupSet mupLABranchGroupSet = new LABranchGroupSet();

    private MMap mMap=new MMap();

    private VData mOutputDate=new VData();

    public AdjustGrpAgentGoneBL()
    {
    }

    public static void main(String[] args)
    {
        String as = "2003-11-21";
        System.out.println(PubFun.calDate(as, -1, "M", null));
        String tDay = as.substring(as.lastIndexOf("-") + 1);
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "AdjustGrpAgentGoneBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
       * 传输数据的公共方法
       * @param: cInputData 输入的数据
       * cOperate 数据操作
       * @return:
       */
      public boolean submitData(VData cInputData, String cOperate)
      {
          //将操作数据拷贝到本类中
          this.mOperate = cOperate;
          //得到外部传入的数据,将数据备份到本类中
          if (!getInputData(cInputData))
          {
              return false;
          }
          //进行业务处理
          //if (!check())
          //{
          //    return false;
          //}
          if (!dealData())
          {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "AdjustGrpAgentGoneBL";
              tError.functionName = "submitData";
              tError.errorMessage = "数据处理失败AdjustGrpAgentGoneBL-->dealData!";
              this.mErrors.addOneError(tError);
              return false;
          }
          //准备往后台的数据
          if (!prepareOutputData())
          {
              return false;
          }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start AdjustGrpAgentGoneBL Submit...");
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "AdjustGrpAgentGoneBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mInputData = null;
          return true;
    }


    /**
        * 从输入数据中得到所有对象
        *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
        */
       private boolean getInputData(VData cInputData)
       {
           System.out.println("GetInputData-AdJustAgentBL");
           mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                   "GlobalInput", 0));
           this.mLABranchChangeTempSchema.setSchema((LABranchChangeTempSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LABranchChangeTempSchema",
                                                  1));
            mAgentCode=mLABranchChangeTempSchema.getAgentCode();
           if (mGlobalInput == null)
           {
               // @@错误处理
               CError tError = new CError();
               tError.moduleName = "AdjustGrpAgentGoneBL";
               tError.functionName = "getInputData";
               tError.errorMessage = "没有得到足够的信息！";
               this.mErrors.addOneError(tError);
               return false;
           }
           return true;
    }




    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        System.out.println("进入dealData处理");
        tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        boolean tReturn = true;

        LABranchGroupDB tLABranchDB = new LABranchGroupDB();
        tLABranchDB.setAgentGroup(this.mLABranchChangeTempSchema.getAgentGroup());
        if (!tLABranchDB.getInfo())
        {
         // @@错误处理
         this.mErrors.copyAllErrors(tLABranchDB.mErrors);
         CError tError = new CError();
         tError.moduleName = "AdjustGrpAgentGoneBL";
         tError.functionName = "dealData";
         tError.errorMessage = "查询目标单位信息失败!";
         this.mErrors.addOneError(tError);
         return false;
         }

         mLABranchSchema=tLABranchDB.getSchema();
         //准备更新所有调动人员和管理人员在行政表中的AgentGroup和UpAgent的纪录
         String tAimBranch = this.mLABranchSchema.getAgentGroup();
         String tUpAgent = this.mLABranchSchema.getBranchManager();
         mAimManageCom = this.mLABranchSchema.getManageCom() ;
         System.out.println("UpAgent:" + tUpAgent + " --不管目标机构是否有管理人员");
         tUpAgent = tUpAgent == null ? "" : tUpAgent;


         LATreeDB tLATreeDB = new LATreeDB();
         tLATreeDB.setAgentCode(this.mLABranchChangeTempSchema.getAgentCode()); //jiangcx add for BK 主键
         if (!tLATreeDB.getInfo())
         {
         this.mErrors.copyAllErrors(tLATreeDB.mErrors);
         CError tError = new CError();
         tError.moduleName = "AdjustGrpAgentBL";
         tError.functionName = "dealData";
         tError.errorMessage = "查询业务员"+mLABranchChangeTempSchema.getAgentCode()  +"的基础信息失败!";
         this.mErrors.addOneError(tError);
         return false;
         }
         mLATreeSchema = tLATreeDB.getSchema();


         LAAgentDB tLAAgentDB = new LAAgentDB();
         tLAAgentDB.setAgentCode(this.mLABranchChangeTempSchema.getAgentCode()); //jiangcx add for BK 主键
         if (!tLAAgentDB.getInfo())
         {
          this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "AdjustGrpAgentBL";
          tError.functionName = "dealData";
          tError.errorMessage = "查询业务员"+mLABranchChangeTempSchema.getAgentCode()  +"的基础信息失败!";
          this.mErrors.addOneError(tError);
          return false;
          }

         mLAAgentSchema = tLAAgentDB.getSchema();

         String agentgrade=mLATreeSchema.getAgentGrade().substring(0,1);
         if(!tUpAgent.equals("")&&agentgrade.equals("E")){

         }
         else
         {
          //genxin  latree
          LATreeSchema bLATreeSchema=new LATreeSchema();
          LATreeBSchema tLATreeBSchema=new LATreeBSchema();
          bLATreeSchema=tLATreeDB.getSchema();
          Reflections tReflections=new Reflections();
          tReflections.transFields(tLATreeBSchema,bLATreeSchema);
          tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator()  ) ;
          tLATreeBSchema.setOperator(mGlobalInput.Operator );
          tLATreeBSchema.setEdorNO(tEdorNo) ;
          tLATreeBSchema.setRemoveType("03") ;
          tLATreeBSchema.setMakeDate2(tLATreeBSchema.getMakeDate() );
          tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
          tLATreeBSchema.setMakeDate(currentDate) ;
          tLATreeBSchema.setMakeTime(currentTime) ;
          this.mLATreeBSet .add(tLATreeBSchema) ;
          //设置上级代理人
          bLATreeSchema.setUpAgent(tUpAgent);
          bLATreeSchema.setAgentGroup(tAimBranch); //设置所属管理机构
          bLATreeSchema.setManageCom(mLABranchSchema.getManageCom() ) ;
          bLATreeSchema.setOperator(this.mGlobalInput.Operator);
          bLATreeSchema.setModifyDate(currentDate);
          bLATreeSchema.setModifyTime(currentTime);
          bLATreeSchema.setBranchCode(tAimBranch) ;
          bLATreeSchema.setAstartDate(mLABranchChangeTempSchema.getCValiDate()); //调整日期
          this.mUpdateLATreeSet.add(bLATreeSchema);

          LAAgentSchema tLAAgentSchema=new LAAgentSchema() ;
          tLAAgentSchema=tLAAgentDB.getSchema() ;
          LAAgentBSchema tLAAgentBSchema=new LAAgentBSchema();
          tReflections=new Reflections();
          tReflections.transFields(tLAAgentBSchema,tLAAgentSchema) ;
          tLAAgentBSchema.setMakeDate(currentDate) ;
          tLAAgentBSchema.setMakeTime(currentTime) ;
          tLAAgentBSchema.setOperator(mGlobalInput.Operator ) ;
          tLAAgentBSchema.setEdorNo(tEdorNo) ;
          tLAAgentBSchema.setEdorType("03") ;
          this.mLAAgentBSet.add(tLAAgentBSchema)  ;
          tLAAgentSchema.setModifyDate(currentDate) ;
          tLAAgentSchema.setModifyTime(currentTime);
          tLAAgentSchema.setAgentGroup(tAimBranch);
          tLAAgentSchema.setBranchCode(tAimBranch);
          tLAAgentSchema.setOperator(mGlobalInput.Operator ) ;
          tLAAgentSchema.setManageCom(mLABranchSchema.getManageCom() ) ;
          mLAAgentSet.add(tLAAgentSchema) ;
         }

          String qSql="select grpcontno from lccont where agentcode='"+mAgentCode+"'"
                      +" union select grpcontno from lcgrpcont where agentcode='"+mAgentCode+"' with ur";
          System.out.println("查询语句:" + qSql);
          ExeSQL qExeSQL = new ExeSQL();
          mCont = qExeSQL.execSQL(qSql);
          if(mCont.getMaxRow()>0){
          if (!dealCont(tAimBranch, mAgentCode,mAimManageCom)) {
             return false;
          }
          }

         if (this.mErrors .needDealError() )
         {
           tReturn=false;
         }
         else
         {
           tReturn = true;
          }
         return tReturn;
    }

    /*处理保单信息,修改
        *lcgrpcont、lcgrppol、ljspaygrp、lccont、lcpol、ljspayperson、ljsgetendorse
        *表的agentcode
        */
       private boolean dealCont(String cAgentGroup, String cAgentCode,String cManageCom) {
           mSql_lcgrpcont = " update lcgrpcont set managecom='" + cManageCom +
                            "',agentgroup='" + cAgentGroup + "' ,modifydate='" + currentDate +
                            "' ,modifytime='" + currentTime + "' where agentcode='" +
                            cAgentCode + "'";
           mSql_lcgrppol  = " update lcgrppol set managecom='" + cManageCom +
                            "',agentgroup='"+ cAgentGroup + "' ,modifydate='" + currentDate +
                            "' ,modifytime='" + currentTime + "' where agentcode='" +
                            cAgentCode +"'";
           mSql_ljspaygrp = " update ljspaygrp set  managecom='" + cManageCom +
                            "',agentgroup='" + cAgentGroup + "' ,modifydate='" + currentDate +
                            "' ,modifytime='" + currentTime + "' where agentcode='" +
                            cAgentCode + "'";
           mSql_ljspaygrp = " update ljspay set  managecom='" + cManageCom +
                            "',agentgroup='" + cAgentGroup + "' ,modifydate='" + currentDate +
                            "' ,modifytime='" + currentTime + "' where agentcode='" +
                            cAgentCode + "'";

           mSql_lccont    = " update lccont set  managecom='" + cManageCom +
                            "',agentgroup='" + cAgentGroup + "' ,modifydate='" + currentDate +
                            "' ,modifytime='" + currentTime + "' where agentcode='" +
                            cAgentCode + "'";

           mSql_lcpol     = " update lcpol set  managecom='" + cManageCom +
                            "',agentgroup='" + cAgentGroup + "' ,modifydate='" + currentDate +
                            "' ,modifytime='" + currentTime + "' where agentcode='" +
                            cAgentCode +"'";


          /**
           //续保后可以能回退，把b表的数据复制到c表，所以也要被分续保回退的数据
           String tSql = "select prtno from  lcgrpcont where agentcode='" +
                         cAgentCode
                         +
                         "'  ";
           ExeSQL tExeSQL = new ExeSQL();
           String tPrtNo = tExeSQL.getOneValue(tSql);

           if (tExeSQL.mErrors.needDealError()) {
               CError tError = new CError();
               tError.moduleName = "LAGrpAscrptionBL";
               tError.functionName = "dealAscript";
               tError.errorMessage = "查询保单原业务员出错";
               this.mErrors.addOneError(tError);
               return false;
           }
           }*/
           mSql_lbgrpcont = " update lbgrpcont set  managecom='" + cManageCom +
                            "',agentgroup='"+ cAgentGroup + "' ,modifydate='" + currentDate +
                            "' ,modifytime='" + currentTime + "' where agentcode='" +
                            cAgentCode +"'";

           mSql_lbgrppol  = " update lbgrppol set  managecom='" + cManageCom +
                            "',agentgroup='" + cAgentGroup + "' ,modifydate='" + currentDate +
                            "' ,modifytime='" + currentTime + "' where agentcode='" +
                            cAgentCode +"'";

            mSql_lbcont   = " update lbcont set  managecom='" + cManageCom +
                            "',agentgroup='"+ cAgentGroup + "' ,modifydate='" + currentDate +
                            "' ,modifytime='" + currentTime + "' where agentcode='" +
                            cAgentCode +"'";

           mSql_lbpol     = " update lbpol set  managecom='" + cManageCom +
                            "',agentgroup='"+ cAgentGroup + "' ,modifydate='" + currentDate +
                            "' ,modifytime='" + currentTime + "' where agentcode='" +
                            cAgentCode +"'";

           //修改保全p表，p表可能会回写c表
           mSql_lpgrpcont = " update lpgrpcont set  managecom='" + cManageCom +
                            "',agentgroup='"+ cAgentGroup + "' ,modifydate='" + currentDate +
                            "' ,modifytime='" + currentTime + "' where agentcode='" +
                            cAgentCode + "'";

           mSql_lpgrppol = " update lpgrppol set managecom='" + cManageCom +
                            "',agentgroup='"+ cAgentGroup + "' ,modifydate='" + currentDate +
                            "' ,modifytime='" + currentTime + "' where agentcode='" +
                            cAgentCode + "'";

            mSql_lpcont = " update lpcont set  managecom='" + cManageCom +
                          "',agentgroup='"+ cAgentGroup + "' ,modifydate='" + currentDate +
                          "' ,modifytime='" + currentTime + "' where agentcode='" +
                          cAgentCode +"'";

            mSql_lppol = " update lppol set  managecom='" + cManageCom +
                         "',agentgroup='"+ cAgentGroup + "' ,modifydate='" + currentDate +
                         "' ,modifytime='" + currentTime + "' where agentcode='" +
                         cAgentCode +"'";
            mSql_ljspayperson = " update ljspayperson set  managecom='" + cManageCom +
                                      "',agentgroup='"+ cAgentGroup + "' ,modifydate='" + currentDate +
                                      "' ,modifytime='" + currentTime + "' where agentcode='" +
                                      cAgentCode + "'";

            mSql_ljsgetendorse = " update ljsgetendorse set  managecom='" + cManageCom +
                                      "',agentgroup='"+ cAgentGroup + "' ,modifydate='" + currentDate +
                                      "' ,modifytime='" + currentTime + "' where agentcode='" +
                              cAgentCode + "'";
         //因为财务没有提数，修改当天的数据
         mSql_ljapay = " update ljapay set  managecom='" + cManageCom +
                                      "',agentgroup='"+ cAgentGroup + "' ,modifydate='" + currentDate +
                                      "' ,modifytime='" + currentTime + "' where agentcode='" +
                              cAgentCode + "' and makedate='"+currentDate+"'";
         mSql_ljapaygrp = " update ljapaygrp set  managecom='" + cManageCom +
                                            "',agentgroup='"+ cAgentGroup + "' ,modifydate='" + currentDate +
                                            "' ,modifytime='" + currentTime + "' where agentcode='" +
                                    cAgentCode + "' and makedate='"+currentDate+"'";
           mSql_ljapayperson = " update ljapayperson set  managecom='" + cManageCom +
                                     "',agentgroup='"+ cAgentGroup + "' ,modifydate='" + currentDate +
                                     "' ,modifytime='" + currentTime + "' where agentcode='" +
                             cAgentCode + "' and makedate='"+currentDate+"'";
         mSql_ljagetendorse = " update ljagetendorse set  managecom='" + cManageCom +
                                     "',agentgroup='"+ cAgentGroup + "' ,modifydate='" + currentDate +
                                     "' ,modifytime='" + currentTime + "' where agentcode='" +
                             cAgentCode + "' and makedate='"+currentDate+"'";




           mMap.put(mSql_lcgrpcont, "UPDATE");
           mMap.put(mSql_lcgrppol, "UPDATE");
           mMap.put(mSql_lbgrpcont, "UPDATE");
           mMap.put(mSql_lbgrppol, "UPDATE");
           mMap.put(mSql_lpgrpcont, "UPDATE");
           mMap.put(mSql_lpgrppol, "UPDATE");
           mMap.put(mSql_ljspaygrp, "UPDATE");
           mMap.put(mSql_ljspay, "UPDATE");
           mMap.put(mSql_lccont, "UPDATE");
           mMap.put(mSql_lcpol, "UPDATE");
           mMap.put(mSql_lbcont, "UPDATE");
           mMap.put(mSql_lbpol, "UPDATE");
           mMap.put(mSql_lpcont, "UPDATE");
           mMap.put(mSql_lppol, "UPDATE");
           mMap.put(mSql_ljspayperson, "UPDATE");
           mMap.put(mSql_ljsgetendorse, "UPDATE");
           mMap.put(mSql_ljapay, "UPDATE");
           mMap.put(mSql_ljapaygrp, "UPDATE");
           mMap.put(mSql_ljapayperson, "UPDATE");
           mMap.put(mSql_ljagetendorse, "UPDATE");

            return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
          mMap.put(this.mLAAgentBSet ,"INSERT") ;
          mMap.put(this.mLATreeBSet,"INSERT") ;
          mMap.put(this.mUpdateLATreeSet,"UPDATE") ;
          mMap.put(this.mLAAgentSet,"UPDATE") ;


          this.mInputData.add(mMap);
          return true;
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustGrpAgentGoneBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public MMap getMapResult() {
        return mMap;
    }
}
