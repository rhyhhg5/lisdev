package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class makeZhiXiaChu {
    public makeZhiXiaChu() {
    }

    public boolean submitData(){
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setBranchLevel("02");
        tLABranchGroupDB.setBranchType("1");
        LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.query();
        for(int i=1;i<=tLABranchGroupSet.size();i++){
            if(!getBaseData(tLABranchGroupSet.get(i)))
                System.out.println(tLABranchGroupSet.get(i).getAgentGroup()+"失败!!!!");
        }
        String sql0 = "set branchcode = "
                      +"(select c.agentgroup from labranchgroup c "
                      +" where c.upbranch=a.agentgroup and upbranchattr = '1' ) "
                      +" where a.agentcode in (select b.branchmanager from "
                      +" labranchgroup b where  b.branchlevel = '02' "
                      +"and b.branchtype='1' and b.branchtype2='01')";
        String sqlagent = "update laagent a "+sql0;
        String sqltree = "update latree a "+sql0;
        ExeSQL texesql = new ExeSQL();
        texesql.execUpdateSQL(sqlagent);
        texesql.execUpdateSQL(sqltree);
        return true;
    }

    private boolean getBaseData(LABranchGroupSchema uBranchGroupSchema){
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setUpBranch(uBranchGroupSchema.getAgentGroup());
        tLABranchGroupDB.setUpBranchAttr("1");
        LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.query();
        if(tLABranchGroupSet.size()>0){
            System.out.println(uBranchGroupSchema.getAgentGroup()+"已有直辖处!!!!");
            return false;
        }
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        ALABranchGroupUI tLABranchGroup = new ALABranchGroupUI();

        GlobalInput tG = new GlobalInput();
        tG.Operator = "mak001";
        tG.ComCode  = uBranchGroupSchema.getManageCom();

        tLABranchGroupSchema.setAgentGroup("");
        tLABranchGroupSchema.setSchema(uBranchGroupSchema);
        tLABranchGroupSchema.setName(uBranchGroupSchema.getName()+"直辖处");
        String sqlpart = "select substr(branchattr,13,2) from  ";
        String wherepart = " where branchlevel='01' and branchattr like '"
                      +uBranchGroupSchema.getBranchAttr()+"%'";
   String tSql = sqlpart +" labranchgroup "+ wherepart +" union "
                 +sqlpart +" labranchgroupb " +wherepart;
        System.out.println(tSql);
        ExeSQL texesql = new ExeSQL();
        SSRS tss = texesql.execSQL(tSql);
        int i=1;
        if(tss.getMaxRow()>0){
            for (i = 1; i < 100; i++) {
                boolean hascode = false;
                for(int j=1;j<=tss.getMaxRow();j++){
                    if(i==Integer.parseInt(tss.GetText(1,j))){
                        hascode = true;
                        break;
                    }
                }
                if(!hascode)
                    break;
            }
        }
        String sub = ""+i;
        if(i<10)
            sub = "0"+i;
        tLABranchGroupSchema.setBranchAttr(uBranchGroupSchema.getBranchAttr()+sub);
        tLABranchGroupSchema.setUpBranch(uBranchGroupSchema.getBranchAttr());
        tLABranchGroupSchema.setBranchType("1");
        tLABranchGroupSchema.setBranchType2("01");
        tLABranchGroupSchema.setBranchLevel("01");
        tLABranchGroupSchema.setUpBranchAttr("1");

        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.addElement(tLABranchGroupSchema);
        tVData.add(tG);
        try {
            tLABranchGroup.submitData(tVData, "INSERT||MAIN");
        } catch (Exception ex) {
          String Content = "保存失败，原因是:" + ex.toString();
          System.out.println(Content);
        }

        return true;
    }

    public static void main(String[] args) {
        makeZhiXiaChu makezhixiachu = new makeZhiXiaChu();
        makezhixiachu.submitData();
    }
}
