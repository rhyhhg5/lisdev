/*
 * <p>ClassName: ALABankAgentBranchManagerBL </p>
 * <p>Description: ALABankAgentBranchManagerBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售银代主管任命
 * @CreateDate：2008-03-03
 */
package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LARearRelationDB;

public class ALABankAgentBranchManagerBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;
    private String mBranchType;
    private String mBranchType2;
    private String mPartTime;
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();

    /** 业务处理相关变量 */
    private LABranchGroupSchema mLABranchGroupSchema = new LABranchGroupSchema();
    private TransferData mTransferData = new TransferData();
    private LATreeBSet mLATreeBSet = new LATreeBSet();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LARearRelationSet mLARearRelationSet = new LARearRelationSet();
    private LABranchGroupBSet mLABranchGroupBSet = new LABranchGroupBSet();
    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();
    private LAComToAgentSet mLAComToAgentSet=new LAComToAgentSet();
    private LAComToAgentBSet mLAComToAgentBSet=new LAComToAgentBSet();
    private String mAdjustDate = ""; //修改日期
    private String tEdorNo = "";
    private String mAgentKind = "";
    private String BranchAttr = "";
    private String BranchCode = "";
    private String AdjustDate = "";
    private String NewBranchManager = "";
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    private MMap mMap = new MMap();
    private String ttNewBranchManager = "";
    private String ttOldManager = "";

    public ALABankAgentBranchManagerBL() {
    }

    public static void main(String[] args) {
        GlobalInput GI = new GlobalInput();
        GI.Operator = "actest";
        GI.ManageCom = "8611";
        String tAdjustdate = "2006-01-01";
        String tStartDate = "2006-01-01";

        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup("000000000076");
        if (tLABranchGroupDB.getInfo()) {
            tLABranchGroupSchema.setSchema(tLABranchGroupDB);
            tLABranchGroupSchema.setBranchManager("8611000006");
            tLABranchGroupSchema.setModifyDate("2005-12-01");
            VData tVData = new VData();
            tVData.add(tLABranchGroupSchema);
            tVData.add(GI);
            tVData.add("1");
            tVData.add(tStartDate);
//            tVData.add(tAdjustdate);
            tVData.add("1102000024");
            ALAGrpBranchManagerBL obj = new ALAGrpBranchManagerBL();
            obj.submitData(tVData, "");
        }

    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ALAGrpBranchManagerBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 判断新任命的主管的级别是否有资格当该机构的主管
     * @return 如果有资格，返回true，否则返回false
     */
    private boolean check() {
        if ((NewBranchManager == null) || NewBranchManager.trim().equals("")) {
            CError tError = new CError();
            tError.moduleName = "ALABankAgentBranchManagerBL";
            tError.functionName = "check";
            tError.errorMessage = "请输入新任主管编码";
            this.mErrors.addOneError(tError);
            return false;
        }
        String newStartDate = AgentPubFun.formatDate(mAdjustDate, "yyyy-MM-dd");
        String tDay = AgentPubFun.formatDate(newStartDate, "dd");
        LATreeSchema tLATreeSchema;
        LAAgentSchema tLAAgentSchema;
        //校验调动日期是否为月初
        if (!tDay.equals("01")) {
            CError tError = new CError();
            tError.moduleName = "ALABankAgentBranchManagerBL";
            tError.functionName = "check";
            tError.errorMessage = "调整日期必须是从某个月的1号开始!";
            this.mErrors.addOneError(tError);
            return false;
        } else {
        	LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(NewBranchManager);
            if (!tLAAgentDB.getInfo()) {
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALABankAgentBranchManagerBL";
                tError.functionName = "check";
                tError.errorMessage = "未查询到主管" + NewBranchManager + "的基础信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLAAgentSchema = tLAAgentDB.getSchema();
            ttNewBranchManager = tLAAgentSchema.getGroupAgentCode();
            LATreeDB tLATreeDB = new LATreeDB();
            tLATreeDB.setAgentCode(NewBranchManager);
            if (!tLATreeDB.getInfo()) {
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALABankAgentBranchManagerBL";
                tError.functionName = "check";
                tError.errorMessage = "未查询到主管" + ttNewBranchManager +
                                      "的行政信息!";
                this.mErrors.addOneError(tError);
                return false;
            } else {
                if (tLATreeDB.getAgentGrade().compareTo("G51") < 0) {
                    CError tError = new CError();
                    tError.moduleName = "ALABankAgentBranchManagerBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "管理人员代码输入有误，必须是管理系列的人员";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            tLATreeSchema = tLATreeDB.getSchema();
            String sql = "select max(IndexCalNo) from lawage where agentcode='" +
                         NewBranchManager + "' and branchtype='"+mBranchType+"' and branchtype2='"+mBranchType2+"'";
            String MaxIndexCalNo = new ExeSQL().getOneValue(sql);
            if (MaxIndexCalNo != null && !MaxIndexCalNo.equals("")) {
                String WageCalDate = MaxIndexCalNo;
                WageCalDate += "01";
                WageCalDate = AgentPubFun.formatDate(WageCalDate, "yyyy-MM-dd");
                WageCalDate = PubFun.calDate(WageCalDate, 1, "M", "");
                if (WageCalDate.compareTo(this.mAdjustDate)>0) {
                    CError tError = new CError();
                    tError.moduleName = "ALABankAgentBranchManagerBL";
                    tError.functionName = "check";
                    tError.errorMessage = "业务员" + ttNewBranchManager +
                                          "最近一次计算佣金是" + MaxIndexCalNo 
//                                          +",因此调动日期必须大于" + WageCalDate 
                                          + "";
                    this.mErrors.addOneError(tError);
                    return false;
                } 
                else {
                    AdjustDate = this.mAdjustDate;
                }
            } 
            else {
                AdjustDate = tLAAgentSchema.getEmployDate();
            }

        }

        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(mLABranchGroupSchema.getAgentGroup());
        if (!tLABranchGroupDB.getInfo()) {
            this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALABankAgentBranchManagerBL";
            tError.functionName = "check";
            tError.errorMessage = "未找到被任命机构的机构信息";
            this.mErrors.addOneError(tError);
            return false;
        }
        LABranchLevelDB tLABranchLevelDB = new LABranchLevelDB();
        tLABranchLevelDB.setBranchLevelCode(tLABranchGroupDB.getBranchLevel());
        tLABranchLevelDB.setBranchType(tLABranchGroupDB.getBranchType());
        tLABranchLevelDB.setBranchType2(tLABranchGroupDB.getBranchType2());

        if (!tLABranchLevelDB.getInfo()) {
            this.mErrors.copyAllErrors(tLABranchLevelDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALABankAgentBranchManagerBL";
            tError.functionName = "check";
            tError.errorMessage = "未找到被任命机构的机构级别信息";
            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println(tLABranchLevelDB.getBranchType().trim());
        return true;
    }

    /**
     * 传输数据的公共方法
     *
     * @param cInputData VData 输入的数据
     * @param cOperate String 数据操作
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!check()) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAGrpBranchManagerBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALAGrpBranchManagerBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("业务处理成功！");

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mInputData, "");
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAGrpBranchManagerBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealData() {
        //这里没有兼职专职的概念，不再区分，modified by huxl at 20080303
        tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        String tAgentGroup = this.mLABranchGroupSchema.getAgentGroup();
        if (tAgentGroup.equals("")) {
            CError tError = new CError();
            tError.moduleName = "ALABankAgentBranchManagerBL";
            tError.functionName = "dealData()";
            tError.errorMessage = "不存在所操作的销售单位!(隐式机构代码为空)";
            this.mErrors.addOneError(tError);
            return false;
        }
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(NewBranchManager);
        if (!tLAAgentDB.getInfo()) {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALABankAgentBranchManagerBL";
            tError.functionName = "dealData()";
            tError.errorMessage = "不存在所任命的主管的基础信息!";
            this.mErrors.addOneError(tError);
            return false;

        }
        //对该主管的原团队进行处理
        if (!this.setOldBranchGroup(NewBranchManager)) {
            CError tError = new CError();
            tError.moduleName = "ALABankAgentBranchManagerBL";
            tError.functionName = "dealData()";
            tError.errorMessage = "清空" + ttNewBranchManager + "的以前机构的机构主管时出错";
            this.mErrors.addOneError(tError);
            return false;
        }
        //保存机构原有信息
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = getBranchGroupInfo(tAgentGroup);
        String tOldManager = tLABranchGroupSchema.getBranchManager();
        System.out.println("tOldManager:" + tOldManager);
        if (tLABranchGroupSchema == null) {
            CError tError = new CError();
            tError.moduleName = "ALAGrpBranchManagerBL";
            tError.functionName = "dealData()";
            tError.errorMessage = "" + tAgentGroup + "的机构信息为空";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLABranchGroupSchema = tLABranchGroupSchema;
        mLABranchGroupSchema.setBranchManager(NewBranchManager);
        mLABranchGroupSchema.setBranchManagerName(tLAAgentDB.getName());
        mLABranchGroupSchema.setModifyDate(currentDate);
        mLABranchGroupSchema.setModifyTime(currentTime);
        mLABranchGroupSchema.setOperator(mGlobalInput.Operator);
        String FoundDate = AgentPubFun.formatDate(mLABranchGroupSchema.
                                                  getFoundDate(), "yyyy-MM-dd");
        if (FoundDate.compareTo(AdjustDate) > 0) {
            mLABranchGroupSchema.setAStartDate(FoundDate);
        } else {
            mLABranchGroupSchema.setAStartDate(AdjustDate);
        }
        //主管信息
        LAAgentSchema tLAAgentSchema;
        LATreeSchema tLATreeSchema;
        LATreeBSchema tLATreeBSchema;

        /**
         * 如果上任主管不为空，判断上任主管的状态，如果上任主管依然对该机构有效，则不能任命新的主管
         * 所谓有效，即主管没有离职，没有调动，没有降职等等
         */
        if ((tOldManager != null) && !tOldManager.trim().equals("")) {
        	
        	tLAAgentSchema = new LAAgentSchema();
            tLAAgentSchema = getAgentBasicInfo(tOldManager);
            ttOldManager = tLAAgentSchema.getGroupAgentCode();
            if (tLAAgentSchema == null) {
                CError tError = new CError();
                tError.moduleName = "ALAGrpBranchManagerBL";
                tError.functionName = "dealData";
                tError.errorMessage = "未查出的" + tOldManager + "的代理人基础信息";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            if (tOldManager.equals(NewBranchManager)) {
                CError tError = new CError();
                tError.moduleName = "ALAGrpBranchManagerBL";
                tError.functionName = "dealData";
                tError.errorMessage = "该机构主管已经是" + ttOldManager + "";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            tLATreeSchema = this.getAgentTreeInfo(tOldManager);
            if (tLATreeSchema == null) {
                CError tError = new CError();
                tError.moduleName = "ALAGrpBranchManagerBL";
                tError.functionName = "dealData";
                tError.errorMessage = "未查出的" + ttOldManager + "的代理人行政信息";
                this.mErrors.addOneError(tError);
                return false;
            }
            String agentState = tLAAgentSchema.getAgentState();
            System.out.println("--OldManager--agentState---" + agentState);
            String agentGroup = tLAAgentSchema.getAgentGroup();
            System.out.println("--OldManager--agentGroup---" + agentGroup);

        }
        LABranchGroupBSchema tLABranchGroupBSchema = new
                LABranchGroupBSchema();
        Reflections tReflections = new Reflections();
        tReflections.transFields(tLABranchGroupBSchema,
                                 tLABranchGroupSchema);
        tLABranchGroupBSchema.setEdorNo(tEdorNo);
        tLABranchGroupBSchema.setEdorType("07"); //主管任命
        tLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.
                                           getMakeDate());
        tLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.
                                           getMakeTime());
        tLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.
                                             getModifyDate());
        tLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.
                                             getModifyTime());
        tLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.
                                           getOperator());
        tLABranchGroupBSchema.setMakeDate(currentDate);
        tLABranchGroupBSchema.setMakeTime(currentTime);
        tLABranchGroupBSchema.setModifyDate(currentDate);
        tLABranchGroupBSchema.setModifyTime(currentTime);
        tLABranchGroupBSchema.setOperator(this.mGlobalInput.Operator);
        this.mLABranchGroupBSet.add(tLABranchGroupBSchema);

        //获得新主管的信息
        tLAAgentSchema = new LAAgentSchema();
        tLAAgentSchema = this.getAgentBasicInfo(NewBranchManager);
        tLATreeSchema = new LATreeSchema();
        tLATreeSchema = this.getAgentTreeInfo(NewBranchManager);
        mLABranchGroupSchema.setBranchManager(NewBranchManager);
        mLABranchGroupSchema.setBranchManagerName(tLAAgentSchema.getName());
        this.mLABranchGroupSet.add(mLABranchGroupSchema);

        LABranchGroupSchema DirBranch = this.getDirTeam(this.
                mLABranchGroupSchema.
                getAgentGroup());
        //LAAgentB表备份
        tLATreeBSchema = new LATreeBSchema();
        tReflections = new Reflections();
        tReflections.transFields(tLATreeBSchema, tLATreeSchema);
        //tLATreeBSchema.setAstartDate(tLATreeSchema.getAstartDate());
        tLATreeBSchema.setEdorNO(tEdorNo);
        tLATreeBSchema.setRemoveType("07"); //主管任命
        tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
        tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
        tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
        tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
        tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
        tLATreeBSchema.setMakeDate(currentDate);
        tLATreeBSchema.setMakeTime(currentTime);
        tLATreeBSchema.setModifyDate(currentDate);
        tLATreeBSchema.setModifyTime(currentTime);
        tLATreeBSchema.setOperator(this.mGlobalInput.Operator);
        this.mLATreeBSet.add(tLATreeBSchema);
        //设置新主管行政信息
        tLATreeSchema.setAstartDate(AdjustDate);
        tLATreeSchema.setAgentGroup(this.mLABranchGroupSchema.getAgentGroup());
        tLATreeSchema.setBranchCode(DirBranch.getAgentGroup());
        tLATreeSchema.setManageCom(this.mLABranchGroupSchema.getManageCom());
        String tUpBranch = tLABranchGroupSchema.getUpBranch();
        if ((tUpBranch != null) && !tUpBranch.trim().equals("")) {
            LABranchGroupSchema tLABranch = this.getBranchGroupInfo(
                    tUpBranch);
            if (tLABranch == null) {
                CError tError = new CError();
                tError.moduleName = "ALAGrpBranchManagerBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询当前机构" +
                                      mLABranchGroupSchema.getAgentGroup() +
                                      "的上级机构" + tUpBranch + "的机构信息为空";
                this.mErrors.addOneError(tError);
                return false;
            }
            //此处设置这个主管的上级主管
            tLATreeSchema.setUpAgent(tLABranch.getBranchManager());
        } else {
            tLATreeSchema.setUpAgent("");
        }

        tLATreeSchema.setModifyDate(currentDate);
        tLATreeSchema.setModifyTime(currentTime);
        tLATreeSchema.setOperator(this.mGlobalInput.Operator);
        this.mLATreeSet.add(tLATreeSchema);
        //LAAgentB表备份
        LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
        tReflections = new Reflections();
        tReflections.transFields(tLAAgentBSchema, tLAAgentSchema);
        //tLATreeBSchema.setAstartDate(tLATreeSchema.getAstartDate());
        tLAAgentBSchema.setEdorNo(tEdorNo);
        tLAAgentBSchema.setEdorType("07"); //主管任命
        tLAAgentBSchema.setMakeDate(currentDate);
        tLAAgentBSchema.setMakeTime(currentTime);
        tLAAgentBSchema.setOperator(mGlobalInput.Operator);
        tLAAgentBSchema.setModifyDate(currentDate);
        tLAAgentBSchema.setModifyTime(currentTime);
        tLAAgentBSchema.setOperator(this.mGlobalInput.Operator);
        this.mLAAgentBSet.add(tLAAgentBSchema);
        //LAAgent表操作
        //注意AgentGroup和BranchCode的设置
        tLAAgentSchema.setAgentGroup(mLABranchGroupSchema.getAgentGroup());

        if (DirBranch == null) {
            return false;
        }
        tLAAgentSchema.setBranchCode(DirBranch.getAgentGroup());
        //caigang 2004-06-24 添加，机构主管任命的时候允许跨机构
        tLAAgentSchema.setManageCom(this.mLABranchGroupSchema.getManageCom());
        tLAAgentSchema.setModifyDate(currentDate);
        tLAAgentSchema.setModifyTime(currentTime);
        tLAAgentSchema.setOperator(mGlobalInput.Operator);
        mLAAgentSet.add(tLAAgentSchema);

        if (!this.setDirInst(tAgentGroup, NewBranchManager)) {
            CError tError = new CError();
            tError.moduleName = "ALAGrpBranchManagerBL";
            tError.functionName = "dealData";
            tError.errorMessage = "修改当前机构的下属机构的行政信息时失败。";
            this.mErrors.addOneError(tError);
            return false;
        }



        String tSQL ="select max(edorno) from lacomtoagentb ";
        ExeSQL tExeSQL =new ExeSQL();
        String tEdorNo2=""+tExeSQL.getOneValue(tSQL);
        if(tEdorNo2=="" || tEdorNo2==null){
            CError tError = new CError();
            tError.moduleName = "LABankNewTreeBL";
            tError.functionName = "dealdata";
            tError.errorMessage = "从代理人机构关联表中获取最大流水号失败！";
            this.mErrors.addOneError(tError);
            return false;
         }
         LAComToAgentDB tLAComToAgentDB=null;
         LAComToAgentSet tLAComToAgentSet=new LAComToAgentSet();
         tSQL="select * from lacomtoagent where agentcom in(select distinct agentcom from lacomtoagent where  agentcode='"
                 +this.NewBranchManager+"')";
         tLAComToAgentDB=new LAComToAgentDB();
         tLAComToAgentSet=tLAComToAgentDB.executeQuery(tSQL);
         if (tLAComToAgentDB.mErrors.needDealError()) {
               this.mErrors.copyAllErrors(tLAComToAgentDB.mErrors);
              CError tError = new CError();
              tError.moduleName = "LABankNewTreeBL";
              tError.functionName = "dealData";
              tError.errorMessage = "查询业务员" + ttNewBranchManager +
                          "的关联代理机构信息失败!";
              this.mErrors.addOneError(tError);
              return false;
          }
         LAComToAgentSchema tLAComToAgentSchema;
         LAComToAgentBSchema tLAComToAgentBSchema;
         for(int m=1;m<=tLAComToAgentSet.size();m++){
             tEdorNo2=(Integer.parseInt(tEdorNo2)+1)+"";
             System.out.println("bl:dealdata:tEdorNo2"+tEdorNo2);
             tLAComToAgentSchema=new LAComToAgentSchema();
    tLAComToAgentBSchema=new LAComToAgentBSchema();
    tLAComToAgentSchema=tLAComToAgentSet.get(m);
    tReflections = new Reflections();
    tReflections.transFields(tLAComToAgentBSchema, tLAComToAgentSchema);

    tLAComToAgentBSchema.setMakeDate(currentDate);
    tLAComToAgentBSchema.setMakeTime(currentTime);
    tLAComToAgentBSchema.setModifyDate(currentDate);
    tLAComToAgentBSchema.setModifyTime(currentTime);
    tLAComToAgentBSchema.setOperator(mGlobalInput.Operator);
    tLAComToAgentBSchema.setEdorNo(tEdorNo2);
    tLAComToAgentBSchema.setEdorType("03");
    mLAComToAgentBSet.add(tLAComToAgentBSchema);
    tLAComToAgentSchema.setModifyDate(currentDate);
    tLAComToAgentSchema.setModifyTime(currentTime);
    tLAComToAgentSchema.setOperator(mGlobalInput.Operator);
    tLAComToAgentSchema.setAgentGroup(tLAAgentSchema.getAgentGroup());
    mLAComToAgentSet.add(tLAComToAgentSchema);
    }
    this.mMap.put(mLAComToAgentBSet, "INSERT");
    this.mMap.put(mLAComToAgentSet, "UPDATE");

   //  不同序列的需要进行业绩的处理
    if(!changeCommision(this.mLABranchGroupSchema,tLAAgentSchema.getAgentCode(),AdjustDate)){
    		return false;
   	}
    
    return true;
}

    
    /**
     * 团队发生变化后,需要进行业绩的修改
     * @param mNewLABranchGroupSchema
     * @param changeCommision
     * @return
     */
    private boolean changeCommision(LABranchGroupSchema cNewLABranchGroupSchema,String mAgentCode,String mDate){
    	
    	if(!cNewLABranchGroupSchema.getAgentGroup().equals("")){        
    	//只能对还未进行过薪资计算月份的业绩修改
    	String SQL = "select * from lacommision where agentcode = '"+mAgentCode+"'"
        +" and branchtype='3' and branchtype2='01' and (caldate>='"+mDate+"' or caldate is null)";
    	LACommisionSet tLACommisionSet = new LACommisionSet();
        LACommisionDB tLACommisionDB = new LACommisionDB();
        tLACommisionSet = tLACommisionDB.executeQuery(SQL);
        for (int j = 1; j <= tLACommisionSet.size(); j++) {
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            tLACommisionSchema = tLACommisionSet.get(j);
            tLACommisionSchema.setAgentGroup(cNewLABranchGroupSchema.getAgentGroup());
            LABranchGroupDB tLDB = new LABranchGroupDB();
            tLDB.setAgentGroup(cNewLABranchGroupSchema.getAgentGroup());
            tLDB.getInfo();
            cNewLABranchGroupSchema=tLDB.getSchema();
            tLACommisionSchema.setBranchCode(cNewLABranchGroupSchema.getAgentGroup());
            tLACommisionSchema.setBranchSeries(cNewLABranchGroupSchema.getBranchSeries());
            tLACommisionSchema.setBranchAttr(cNewLABranchGroupSchema.getBranchAttr());
            tLACommisionSchema.setModifyDate(currentDate);
            tLACommisionSchema.setModifyTime(currentTime);
            tLACommisionSchema.setOperator(mGlobalInput.Operator);
            this.mLACommisionSet.add(tLACommisionSchema);
        }
        this.mMap.put(this.mLACommisionSet, "UPDATE");
    	//业务表lccont修改
    	 String tAgentGroup = cNewLABranchGroupSchema.getAgentGroup();
    	 System.out.println("tAgentGroup:"+tAgentGroup);
         String LccontSql = "update lccont  set  agentgroup='" + tAgentGroup+
                       "',ModifyTime = '" + currentTime +
                       "',ModifyDate = '" + currentDate +
                       "',operator='"+mGlobalInput.Operator+"'  where agentcode='"+mAgentCode+"'";
         this.mMap.put(LccontSql, "UPDATE");
         String LbcontSql = "update lbcont  set  agentgroup='" + tAgentGroup+
         "',ModifyTime = '" + currentTime +
         "',ModifyDate = '" + currentDate +
         "',operator='"+mGlobalInput.Operator+"'  where agentcode='"+mAgentCode+"'";
         this.mMap.put(LbcontSql, "UPDATE");
         String LcpolSql = "update lcpol  set  agentgroup='" + tAgentGroup+
                "',ModifyTime = '" + currentTime +
                "',ModifyDate = '" + currentDate +
                "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";
         this.mMap.put(LcpolSql, "UPDATE");
         String LbpolSql = "update lbpol  set  agentgroup='" + tAgentGroup+
         "',ModifyTime = '" + currentTime +
         "',ModifyDate = '" + currentDate +
         "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";
         this.mMap.put(LbpolSql, "UPDATE");
         String LjsSql = "update ljspayperson  set  agentgroup='" + tAgentGroup+
                "',ModifyTime = '" + currentTime +
                "',ModifyDate = '" + currentDate +
                "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";

         this.mMap.put(LjsSql, "UPDATE");
         String LjspaySql = "update ljspay  set  agentgroup='" + tAgentGroup+
                "',ModifyTime = '" + currentTime +
                "',ModifyDate = '" + currentDate +
                "',operator='"+mGlobalInput.Operator+"' where agentcode='"+mAgentCode+"'";
         this.mMap.put(LjspaySql, "UPDATE");
    	}
    	return true;
    }

    /**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        mLABranchGroupSchema.setSchema((LABranchGroupSchema) cInputData.
                                       getObjectByObjectName(
                                               "LABranchGroupSchema", 0));
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mBranchType=mLABranchGroupSchema.getBranchType();
        mBranchType2=mLABranchGroupSchema.getBranchType2();
        mPartTime = (String) cInputData.get(2);
        mAdjustDate = (String) cInputData.get(3);
        NewBranchManager = (String) cInputData.get(4);
        return true;
    }


    private boolean prepareOutputData() {
        try {
            this.mInputData = new VData();
            mMap.put(mLATreeSet, "UPDATE");
            mMap.put(mLATreeBSet, "INSERT");
            mMap.put(mLAAgentSet, "UPDATE");
            mMap.put(mLAAgentBSet, "INSERT");
            mMap.put(mLABranchGroupSet, "UPDATE");
            mMap.put(mLABranchGroupBSet, "INSERT");
            mMap.put(mLACommisionSet, "UPDATE");
            this.mInputData.add(mMap);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAGrpBranchManagerBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    /**
     * 获取tAgentGroup的直辖组的内部编码和外部编码，中间用"|"分割
     * 如果是个人，则获取的是直辖组的内部编码和外部编码
     * 如果是银代和法人，则获取的是当前机构的内部编码和外部编码
     *
     * @param tAgentGroup String
     * @return java.lang.String
     */
    private LABranchGroupSchema getDirTeam(String tAgentGroup) {
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = this.getBranchGroupInfo(tAgentGroup);
        if (tLABranchGroupSchema == null) {
            return null;
        }
        LABranchLevelSchema tLABranchLevelSchema = this.getBranchLevelInfo(
                tLABranchGroupSchema.getBranchLevel());
        System.out.println("-----tLABranchLevelSchema=====");
        if (tLABranchLevelSchema == null) {
            System.out.println("-----tLABranchLevelSchema失败=====");
            return null;
        }
        if (tLABranchLevelSchema.getSubjectProperty().equals("1")) { //有直辖机构
            String tSQL = "select * from labranchgroup where upbranch" + "='" +
                          tAgentGroup +
                          "' and upbranchattr='1' and EndFlag<>'Y'";
            System.out.println(tSQL);
            LABranchGroupSchema sch = new LABranchGroupSchema();
            LABranchGroupSet set = new LABranchGroupSet();
            LABranchGroupDB db = new LABranchGroupDB();
            set = db.executeQuery(tSQL);
            if (db.mErrors.needDealError()) {
                this.mErrors.copyAllErrors(db.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAGrpBranchManagerBL";
                tError.functionName = "getDirTeam";
                tError.errorMessage = "查询" + tAgentGroup + "的直辖机构失败！";
                this.mErrors.addOneError(tError);
                return null;
            }
            if (set.size() == 0) {
                CError tError = new CError();
                tError.moduleName = "ALAGrpBranchManagerBL";
                tError.functionName = "getDirTeam";
                tError.errorMessage = "未查到" + tAgentGroup + "的直辖机构！";
                this.mErrors.addOneError(tError);
                return null;
            }
            if (set.size() > 1) {
                CError tError = new CError();
                tError.moduleName = "ALAGrpBranchManagerBL";
                tError.functionName = "getDirDepartment";
                tError.errorMessage = "查询" + tAgentGroup + "直辖机构是发现多个结果！";
                this.mErrors.addOneError(tError);
                return null;
            }
            sch = set.get(1);
            String mAgentGroup = sch.getAgentGroup();
            return this.getDirTeam(mAgentGroup);
        } else {
            System.out.println("-----tLABranchGroupSchema.getAgentGroup()=====" +
                               tLABranchGroupSchema.getAgentGroup());
            return tLABranchGroupSchema;
        }
    }


    /**
     * 查询代理人编码为agentCode的代理人的行政信息
     *
     * @param agentCode String
     * @return com.sinosoft.lis.schema.LATreeSchema
     */
    private LATreeSchema getAgentTreeInfo(String agentCode) {
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode(agentCode);
        LAAgentDB ttLAAgentDB =new LAAgentDB();
        ttLAAgentDB.setAgentCode(agentCode);
        String aagent="";
        if(ttLAAgentDB.getInfo()){
        	aagent=ttLAAgentDB.getSchema().getGroupAgentCode();
        }else{
        	aagent=agentCode;
        }
        if (!tLATreeDB.getInfo()) {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAGrpBranchManagerBL";
            tError.functionName = "getAgentTreeInfo";
            tError.errorMessage = "查询" + aagent + "的行政信息失败！";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLATreeDB.getSchema();
    }


    /**
     * 查询机构编码为agentGroup的机构信息
     *
     * @param agentGroup String
     * @return com.sinosoft.lis.schema.LABranchGroupSchema
     */
    private LABranchGroupSchema getBranchGroupInfo(String agentGroup) {
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(agentGroup);
        if (!tLABranchGroupDB.getInfo()) {
            this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAGrpBranchManagerBL";
            tError.functionName = "getBranchGroupInfo";
            tError.errorMessage = "查询机构" + agentGroup + "的信息失败！";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLABranchGroupDB.getSchema();
    }

    /**
     * 查询tAgentCode的基础信息
     *
     * @param tAgentCode String
     * @return com.sinosoft.lis.schema.LAAgentSchema
     */
    private LAAgentSchema getAgentBasicInfo(String tAgentCode) {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        if (!tLAAgentDB.getInfo()) {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAGrpBranchManagerBL";
            tError.functionName = "getAgentBasicInfo";
            tError.errorMessage = "查询代理人" + tAgentCode + "的基础信息失败！";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLAAgentDB.getSchema();
    }

    /**
     * 判断当前代理人是否有资格做级别为branchLevel的机构主管
     *
     * @param agentKind String
     * @param branchLevel String
     * @return 有资格返回true，无资格返回false
     */
    private boolean isQualified(String agentKind, String branchLevel) {
        //增加银代、法人的校验
        if (mLABranchGroupSchema.getBranchType().equals("2") ||
            mLABranchGroupSchema.getBranchType().equals("3")) {
            String SQL =
                    "select trim(code1) from ldcoderela where relatype='branchleveltokind' and othersign='" +
                    mLABranchGroupSchema.getBranchType() +
                    "' and code2='" + agentKind + "'";
            ExeSQL zExeSQL = new ExeSQL();
            if (zExeSQL.getOneValue(SQL).compareTo(branchLevel) <= 0) {
                return true;
            }
            return false;
        } else {
            LDCodeRelaDB tLDCodeRelaDB = new LDCodeRelaDB();
            String tAgentKind = "";
            String tSQL =
                    "select * from LDCodeRela where RelaType='branchleveltokind' and code1='" +
                    branchLevel + "'";
            System.out.println("------===tSQL===---" + tSQL);
            LDCodeRelaSet tLDCodeRelaSet = new LDCodeRelaSet();
            tLDCodeRelaSet = tLDCodeRelaDB.executeQuery(tSQL);
            System.out.println("tLDCodeRelaDB");
            if (tLDCodeRelaDB.mErrors.needDealError()) {
                this.mErrors.copyAllErrors(tLDCodeRelaDB.mErrors);

                CError tError = new CError();
                tError.moduleName = "ALAGrpBranchManagerBL";
                tError.functionName = "isQualified";
                tError.errorMessage = "未查出代理branchLevel" + branchLevel +
                                      "的对应的agentKind信息！";
                this.mErrors.addOneError(tError);
                return true;
            }
            System.out.println("----tLDCodeRelaSet" + tLDCodeRelaSet.size());
            if ((tLDCodeRelaSet == null) || (tLDCodeRelaSet.size() == 0)) {
                return false;
            }
            tAgentKind = tLDCodeRelaSet.get(1).getCode2();
            if (!tAgentKind.equals(agentKind)) {
                CError tError = new CError();
                tError.moduleName = "ALAGrpBranchManagerBL";
                tError.functionName = "isQualified";
                tError.errorMessage = "代理人级别和机构级别不对应";
                this.mErrors.addOneError(tError);
                return false;
            }
            return true;
        }
    }


    /**
     * 递归设置当前机构的所有下属机构的管理人员的上级代理人，
     * 包括当前机构的直辖机构的下属机构，直到当前机构的直辖组
     *
     * @param tAgentGroup String
     * @param tManager String
     * @return boolean
     */
    private boolean setDirInst(String tAgentGroup, String tManager) {
        //xjh Add 2005/02/28
        if (this.mPartTime.equals("1")) { //专职
            //设置这个主管对应的旧的下一级机构的人员的上级主管为空，设置时应按照以下方式处理
            //查找当前表中所有LATree表中UpAgent为这个主管的人的UpAgent为空
            LATreeSchema tLATreeSchema;
            LATreeBSchema tLATreeBSchema;
            LATreeDB tLATreeDB = new LATreeDB();
            LATreeSet tLATreeSet = new LATreeSet();
            String sql = "select * from latree where agentcode<>'" + tManager +
                         "' and upagent='" + tManager + "' and exists (select 'X' from laagent where agentcode=latree.agentcode and agentstate<'06' )";
            System.out.println(sql);
            tLATreeSet = tLATreeDB.executeQuery(sql);
            if (tLATreeDB.mErrors.needDealError()) {
                mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAGrpBranchManagerBL";
                tError.functionName = "setUpAgent";
                tError.errorMessage = "获取" + tManager + "的相关信息时为空";
                this.mErrors.addOneError(tError);
                return false;
            }
            for (int i = 1; i <= tLATreeSet.size(); i++) {
                tLATreeSchema = new LATreeSchema();
                tLATreeSchema = tLATreeSet.get(i);
                //备份原有信息
                tLATreeBSchema = new LATreeBSchema();
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLATreeBSchema, tLATreeSchema);
                tLATreeBSchema.setEdorNO(tEdorNo);
                tLATreeBSchema.setRemoveType("07"); //主管任命
                tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
                tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
                tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
                tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
                tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
                tLATreeBSchema.setMakeDate(currentDate);
                tLATreeBSchema.setMakeTime(currentTime);
                tLATreeBSchema.setModifyDate(currentDate);
                tLATreeBSchema.setModifyTime(currentTime);
                tLATreeBSchema.setOperator(this.mGlobalInput.Operator);
                this.mLATreeBSet.add(tLATreeBSchema);
                //修改当前
                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(tLATreeSchema.getAgentCode());
                tLAAgentDB.getInfo();
                String employdate = AgentPubFun.formatDate(tLAAgentDB.
                        getEmployDate(), "yyyy-MM-dd");
                if (employdate.compareTo(AdjustDate) > 0) {
                    tLATreeSchema.setAstartDate(employdate);
                } else {
                    tLATreeSchema.setAstartDate(AdjustDate);
                }
                tLATreeSchema.setUpAgent("");
                tLATreeBSchema.setModifyDate(currentDate);
                tLATreeBSchema.setModifyTime(currentTime);
                tLATreeBSchema.setOperator(this.mGlobalInput.Operator);
                mLATreeSet.add(tLATreeSchema);
            }
        }
        LABranchGroupSchema rLABranchGroupSchema = new LABranchGroupSchema();
        rLABranchGroupSchema = getBranchGroupInfo(tAgentGroup);
        LAAgentSchema tLAAgentSchema = getAgentBasicInfo(tManager);
        String strSQL = "select * from labranchgroup where UpBranch='" +
                        tAgentGroup +
                        "' and (endflag is null or endflag<>'Y') ";
        //mAdjustDate=this.mLABranchGroupSchema .getModifyDate();
        System.out.println("------strSQL---" + strSQL);
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(strSQL);
        if (tLABranchGroupDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAGrpBranchManagerBL";
            tError.functionName = "setDirInst";
            tError.errorMessage = "查询当前机构" + tAgentGroup + "的下级机构时出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("--tLABranchGroupSet.size()---" +
                           tLABranchGroupSet.size());
        /**如果该机构有下级机构，则
         * 逐机构修改机构主管的行政信息并修改机构主管的上级代理人信息
         * 如果没有，那么该机构一定是个组机构，则需要修改该组机构所有成员的
         * 上级代理人
         */
        if (tLABranchGroupSet.size() > 0) {
            //查询该机构的下级非直辖机构
            strSQL = "select * from labranchgroup where UpBranch='" +
                     tAgentGroup +
                     "' and upbranchattr='0' and (endflag is null or endflag<>'Y' )";
            System.out.println("*******strSQL----" + strSQL);
            tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupSet = new LABranchGroupSet();
            tLABranchGroupSet = tLABranchGroupDB.executeQuery(strSQL);
            if (tLABranchGroupDB.mErrors.needDealError()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAGrpBranchManagerBL";
                tError.functionName = "setDirInst";
                tError.errorMessage = "查询当前机构" + tAgentGroup + "的下级机构时出错!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("-----tLABranchGroupSet.size()---" +
                               tLABranchGroupSet.size());
            for (int i = 1; i <= tLABranchGroupSet.size(); i++) {
                LABranchGroupSchema tLABranchGroupSchema = tLABranchGroupSet.
                        get(i);
                LATreeSchema tLATreeSchema = new LATreeSchema();
                LATreeBSchema tLATreeBSchema = new LATreeBSchema();
                LAAgentSchema ttLAAgentSchema = new LAAgentSchema();
                String currManager = tLABranchGroupSchema.getBranchManager();
                System.out.println("--tManager--" + tManager +
                                   "----currManager---" + currManager);
                //备份行政信息
                if ((currManager == null) || currManager.trim().equals("")) {
                    continue;
                }
                tLATreeSchema = getAgentTreeInfo(currManager);
                ttLAAgentSchema = getAgentBasicInfo(currManager);
                if (ttLAAgentSchema.getAgentState().compareTo("02") > 0) {
                    continue;
                }
                if (tLATreeSchema.getUpAgent() != null &&
                    tLATreeSchema.getUpAgent().equals(tManager)) {
                    continue;
                }
                tLATreeBSchema = new LATreeBSchema();
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLATreeBSchema, tLATreeSchema);
                tLATreeBSchema.setEdorNO(tEdorNo);
                tLATreeBSchema.setRemoveType("07"); //主管任命
                tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
                tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
                tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
                tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
                tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
                tLATreeBSchema.setMakeDate(currentDate);
                tLATreeBSchema.setMakeTime(currentTime);
                tLATreeBSchema.setModifyDate(currentDate);
                tLATreeBSchema.setModifyTime(currentTime);
                tLATreeBSchema.setOperator(this.mGlobalInput.Operator);
                this.mLATreeBSet.add(tLATreeBSchema);
                //修改行政信息
                System.out.println(tLATreeSchema.getAgentCode() + "" +
                                   tLATreeSchema.getAstartDate());
                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(tLATreeSchema.getAgentCode());
                tLAAgentDB.getInfo();
                String employdate = AgentPubFun.formatDate(tLAAgentDB.
                        getEmployDate(), "yyyy-MM-dd");
                if (employdate.compareTo(AdjustDate) > 0) {
                    tLATreeSchema.setAstartDate(employdate);
                } else {
                    tLATreeSchema.setAstartDate(AdjustDate);
                }

                tLATreeSchema.setUpAgent(tManager);
                tLATreeSchema.setModifyDate(currentDate);
                tLATreeSchema.setModifyTime(currentTime);
                tLATreeSchema.setOperator(this.mGlobalInput.Operator);
                this.mLATreeSet.add(tLATreeSchema);
            }
            /**
             * 如果该机构有直辖机构，则置直辖机构的机构主管（一般只有个人的部以
             * 及部以上的机构才有直辖机构）
             */
            System.out.println("-----getBranchLevel---" +
                               rLABranchGroupSchema.getBranchLevel());
            LABranchLevelSchema tLABranchLevelSchema = getBranchLevelInfo(
                    rLABranchGroupSchema.getBranchLevel().trim());
            if (tLABranchLevelSchema == null) {
                System.out.println("is null");
                return false;
            }
            System.out.println(tLABranchLevelSchema.getSubjectProperty());
            if (tLABranchLevelSchema.getSubjectProperty().trim().equals("1")) {
                System.out.println(tAgentGroup);
                strSQL = "select * from labranchgroup where UpBranch='" +
                         tAgentGroup +
                         "' and upbranchattr='1' and (endflag is null or endflag<>'Y' )";
                System.out.println(strSQL);
                LABranchGroupDB tLABranchDB = new LABranchGroupDB();
                LABranchGroupSet tLABranchSet = new LABranchGroupSet();
                tLABranchSet = tLABranchDB.executeQuery(strSQL);
                if (tLABranchDB.mErrors.needDealError()) {
                    this.mErrors.copyAllErrors(tLABranchDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ALAGrpBranchManagerBL";
                    tError.functionName = "setDirInst";
                    tError.errorMessage = "未查出" + tAgentGroup + "的直辖机构信息!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (tLABranchSet.size() != 1) {
                    CError tError = new CError();
                    tError.moduleName = "ALAGrpBranchManagerBL";
                    tError.functionName = "setDirInst";
                    tError.errorMessage = "查出" + tAgentGroup + "的直辖机构有" +
                                          tLABranchSet.size() + "个!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                LABranchGroupSchema tLABranchGroupSchema = tLABranchSet.get(1);
                if (tLABranchGroupSchema.getBranchManager() != null &&
                    !tLABranchGroupSchema.getBranchManager().equals(tManager)) {
                    //备份直辖机构的信息
                    LABranchGroupBSchema tLABranchGroupBSchema = new
                            LABranchGroupBSchema();
                    Reflections tReflections = new Reflections();
                    tReflections.transFields(tLABranchGroupBSchema,
                                             tLABranchGroupSchema);
                    tLABranchGroupBSchema.setEdorNo(tEdorNo);
                    tLABranchGroupBSchema.setEdorType("07"); //主管任命
                    tLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.
                            getMakeDate());
                    tLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.
                            getMakeTime());
                    tLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.
                            getModifyDate());
                    tLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.
                            getModifyTime());
                    tLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.
                            getOperator());
                    tLABranchGroupBSchema.setMakeDate(currentDate);
                    tLABranchGroupBSchema.setMakeTime(currentTime);
                    tLABranchGroupBSchema.setModifyDate(currentDate);
                    tLABranchGroupBSchema.setModifyTime(currentTime);
                    tLABranchGroupBSchema.setOperator(this.mGlobalInput.
                            Operator);
                    this.mLABranchGroupBSet.add(tLABranchGroupBSchema);
                }
                //更新直辖机构的主管信息
                tLABranchGroupSchema.setBranchManager(tManager);
                tLABranchGroupSchema.setBranchManagerName(tLAAgentSchema.
                        getName());
                tLABranchGroupSchema.setModifyDate(currentDate);
                tLABranchGroupSchema.setModifyTime(currentTime);
                tLABranchGroupSchema.setOperator(this.mGlobalInput.Operator);
                this.mLABranchGroupSet.add(tLABranchGroupSchema);
                this.setDirInst(tLABranchGroupSchema.getAgentGroup(),
                                tManager);
                LARearRelationSet tLARearRelationSet = new LARearRelationSet();
                LARearRelationDB tLARearRelationDB = new LARearRelationDB();
                String sql = "select * from larearRelation where agentcode='" +
                             tManager + "' and rearlevel='" +
                             tLABranchGroupSchema.getBranchLevel() + "'";
                tLARearRelationSet = tLARearRelationDB.executeQuery(sql);
                for (int kk = 1; kk <= tLARearRelationSet.size(); kk++) {
                    LARearRelationSchema tLARearRelationSchema =
                            tLARearRelationSet.get(kk);
                    tLARearRelationSchema.setAgentGroup(tLABranchGroupSchema.
                            getAgentGroup());
                    tLARearRelationSchema.setModifyDate(currentDate);
                    tLARearRelationSchema.setModifyTime(currentTime);
                    tLARearRelationSchema.setOperator(mGlobalInput.Operator);
                    mLARearRelationSet.add(tLARearRelationSchema);
                }
            }
        }
        //如果没有下属机构，则该机构一定时一个组，则置组内的人员的上级代理人为新任主管，并且将该组的信息进行备份与更新
        else {
            strSQL = "select * from latree where agentgroup='" + tAgentGroup +
                     "'" + " And exists (Select 'X' From LAAgent " +
                     " Where AgentState <'06' and agentcode=latree.agentcode)";

            System.out.println("------strSQL---" + strSQL);
            LATreeSet tLATreeSet = new LATreeSet();
            LATreeSchema tLATreeSchema = new LATreeSchema();
            LATreeDB tLATreeDB = new LATreeDB();
            tLATreeSet = tLATreeDB.executeQuery(strSQL);
            if (tLATreeDB.mErrors.needDealError()) {
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAGrpBranchManagerBL";
                tError.functionName = "setDirInst";
                tError.errorMessage = "查询" + tAgentGroup + "的所有成员时失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("----tLATreeSet.size----" + tLATreeSet.size());
            for (int i = 1; i <= tLATreeSet.size(); i++) {
                //备份组员的行政信息
                System.out.println("123234342323qqqqq");
                tLATreeSchema = tLATreeSet.get(i);
                System.out.println("123234342323qll+" +
                                   tLATreeSchema.getAgentCode() + "||" +
                                   tLATreeSchema.getUpAgent() + "||" + tManager);
                if (tLATreeSchema.getAgentCode().equals(tManager)) {
                    System.out.println("123234342323q12q");
                    continue; //如果主管在这个组中，则主管的上级代理人信息不在这做修改
                }
                if (tLATreeSchema.getUpAgent() != null &&
                    tLATreeSchema.getUpAgent().equals(tManager)) {
                    System.out.println("123234342323q12w");
                    continue; //如果上级代理人并没有变，则不用备份修改
                }
                System.out.println("123234342323q");
                LATreeBSchema tLATreeBSchema = new LATreeBSchema();
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLATreeBSchema, tLATreeSchema);
                tLATreeBSchema.setEdorNO(tEdorNo);
                tLATreeBSchema.setRemoveType("07"); //主管任命
                tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
                tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
                tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
                tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
                tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
                tLATreeBSchema.setMakeDate(currentDate);
                tLATreeBSchema.setMakeTime(currentTime);
                tLATreeBSchema.setModifyDate(currentDate);
                tLATreeBSchema.setModifyTime(currentTime);
                tLATreeBSchema.setOperator(this.mGlobalInput.Operator);
                this.mLATreeBSet.add(tLATreeBSchema);
                //更新组员的行政信息

                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(tLATreeSchema.getAgentCode());
                tLAAgentDB.getInfo();
                String employdate = AgentPubFun.formatDate(tLAAgentDB.
                        getEmployDate(), "yyyy-MM-dd");
                if (employdate.compareTo(AdjustDate) > 0) {
                    tLATreeSchema.setAstartDate(employdate);
                } else {
                    tLATreeSchema.setAstartDate(AdjustDate);
                }

                tLATreeSchema.setUpAgent(tManager);
                tLATreeSchema.setModifyDate(currentDate);
                tLATreeSchema.setModifyTime(currentTime);
                tLATreeSchema.setOperator(this.mGlobalInput.Operator);
                this.mLATreeSet.add(tLATreeSchema);
            }
            System.out.println("123234342323a");
            tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tAgentGroup);
            tLABranchGroupDB.getInfo();
            LARearRelationSet tLARearRelationSet = new LARearRelationSet();
            LARearRelationDB tLARearRelationDB = new LARearRelationDB();
            String sql = "select * from larearRelation where agentcode='" +
                         tManager + "' and rearlevel='" +
                         tLABranchGroupDB.getBranchLevel() + "'";
            System.out.println("123234342323");
            tLARearRelationSet = tLARearRelationDB.executeQuery(sql);
            for (int kk = 1; kk <= tLARearRelationSet.size(); kk++) {
                LARearRelationSchema tLARearRelationSchema = tLARearRelationSet.
                        get(kk);
                tLARearRelationSchema.setAgentGroup(tLABranchGroupDB.
                        getAgentGroup());
                tLARearRelationSchema.setModifyDate(currentDate);
                tLARearRelationSchema.setModifyTime(currentTime);
                tLARearRelationSchema.setOperator(mGlobalInput.Operator);
                mLARearRelationSet.add(tLARearRelationSchema);
            }
        }
        return true;
    }

    private boolean setOldBranchGroup(String cAgentCode) {
        //查询该代理人原先职务
        String tSQL = "select * from labranchgroup where branchmanager='" +
                      cAgentCode + "'";
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(tSQL);
        LAAgentDB ttLAAgentDB = new LAAgentDB ();
        ttLAAgentDB.setAgentCode(cAgentCode);
        String aagent="";
        if(ttLAAgentDB.getInfo()){
        	aagent=ttLAAgentDB.getSchema().getGroupAgentCode();
        }else{
        	aagent=cAgentCode;
        }
        //查询出错
        if (tLABranchGroupDB.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAGrpBranchManagerBL";
            tError.functionName = "setOldBranchGroup";
            tError.errorMessage = "查询" + aagent + "的展业机构信息失败，!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        for (int i = 1; i <= tLABranchGroupSet.size(); i++) {
            //如果发现该代理人有职务信息，则更新那些机构信息
            tLABranchGroupSchema = tLABranchGroupSet.get(i);
            Reflections tReflections = new Reflections();
            LABranchGroupBSchema tLABranchGroupBSchema = new
                    LABranchGroupBSchema();
            tReflections.transFields(tLABranchGroupBSchema,
                                     tLABranchGroupSchema);
            tLABranchGroupBSchema.setEdorNo(tEdorNo);
            tLABranchGroupBSchema.setEdorType("07"); //主管任命
            tLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.getMakeDate());
            tLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.getMakeTime());
            tLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.
                                                 getModifyDate());
            tLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.
                                                 getModifyTime());
            tLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.getOperator());
            tLABranchGroupBSchema.setMakeDate(currentDate);
            tLABranchGroupBSchema.setMakeTime(currentTime);
            tLABranchGroupBSchema.setModifyDate(currentDate);
            tLABranchGroupBSchema.setModifyTime(currentTime);
            tLABranchGroupBSchema.setOperator(this.mGlobalInput.Operator);
            this.mLABranchGroupBSet.add(tLABranchGroupBSchema);

            tLABranchGroupSchema.setBranchManager("");
            tLABranchGroupSchema.setBranchManagerName("");
            mLABranchGroupSet.add(tLABranchGroupSchema);
        }
        return true;
    }

    private LABranchLevelSchema getBranchLevelInfo(String tLevel) {
        LABranchLevelDB tLABranchLevelDB = new LABranchLevelDB();
        tLABranchLevelDB.setBranchLevelCode(tLevel);
        tLABranchLevelDB.setBranchType(mLABranchGroupSchema.getBranchType());
        tLABranchLevelDB.setBranchType2(mLABranchGroupSchema.getBranchType2());
        if (!tLABranchLevelDB.getInfo()) {
            this.mErrors.copyAllErrors(tLABranchLevelDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAGrpBranchManagerBL";
            tError.functionName = "setDirInst";
            tError.errorMessage = "未查出" + tLevel + "的展业机构级别信息!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLABranchLevelDB.getSchema();
    }

    public MMap getMapResult() {
        return mMap;
    }

}
