/*
 * <p>ClassName: ALAComBL </p>
 * <p>Description: ALAComBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 代理机构
 * @CreateDate：2003-01-09
 */
package com.sinosoft.lis.agentbranch;

import java.math.BigInteger;

import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LAContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAComBSchema;
import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.lis.schema.LAContSchema;
import com.sinosoft.lis.vschema.LAComSet;
import com.sinosoft.lis.vschema.LAContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

public class ALAComBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    String currentDate = PubFun.getCurrentDate();
    String currentTime = PubFun.getCurrentTime();
    /** 数据操作字符串 */
    private String mOperate;
    private String mEndFlag1;
    private String Bank;
    private String mCoverFlag=null;
    private String mStartDate=null;
    private String mEndDate=null;
    private String mProtocolNo=null;
    /** 业务处理相关变量 */
    private LAComSchema mLAComSchema = new LAComSchema();
    private LAComBSchema mLAComBSchema=new LAComBSchema();
    private LAComSet mLAComSet = new LAComSet();
    private LAComSet mLAComSet2 = new LAComSet();
    private LAContSchema mLAContSchema = new LAContSchema();
    private LAContSet mInLAContSet = new LAContSet();
    private LAContSet mUpLAContSet = new LAContSet();
    private MMap mMap = new MMap();
    public ALAComBL()
    {}

    public static void main(String[] args)
    {
        VData tVData = new VData();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        LAComSchema tLAComSchema = new LAComSchema();
        tLAComSchema.setAgentCom("18001100001001");
        tLAComSchema.setAreaType("A");
        tLAComSchema.setChannelType("D");
        tLAComSchema.setOperator("001");
        tVData.add(tLAComSchema);
        tVData.add(tG);
        ALAComBL tALAComBL = new ALAComBL();
        tALAComBL.submitData(tVData, "UPDATE||MAIN");
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if(!checkData())
        {
        	 // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAComBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALAComBL-->checkData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAComBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALAComBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start ALAComBL Submit...");
        	PubSubmit tPubSubmit = new PubSubmit();            
//            ALAComBLS tALAComBLS = new ALAComBLS();
//            tALAComBLS.submitData(mInputData, cOperate);
//            System.out.println("End ALAComBL Submit...");
            //如果有需要处理的错误，则返回
            if (!tPubSubmit.submitData(mInputData,mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAComBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }
  /**
   * 进行提交前的数据校验
   */
    private boolean checkData()
    {
//    	if(this.mLAComSchema.getEndFlag().equals("Y"))
//    	{//校验下级代理机构是否已经停业
//    		String MSQL = "select '1' FROM LACom WHERE  agentcom like '"+this.mLAComSchema.getAgentCom()+"%'" +
//    				" and agentcom<>'"+this.mLAComSchema.getAgentCom()+"' and endflag<>'Y' ";
//    		ExeSQL tExeSQL = new ExeSQL();
//    		String flag = tExeSQL.getOneValue(MSQL);
//    		if(flag==null||flag.equals(""))
//    		{
//    			  CError tError = new CError();
//    	          tError.moduleName = "ALAComBL";
//    	          tError.functionName = "submitData";
//    	          tError.errorMessage = "数据处理失败ALAComBL-->checkData!";
//    	          this.mErrors.addOneError(tError);
//    	          return false;
//    		}
//    	}
    	if (this.mOperate.equals("UPDATE||MAIN")){
    		if(this.mEndFlag1.equals("Y")
    				&&(this.mLAComSchema.getEndFlag().equals("N")
    						||this.mLAComSchema.getEndFlag()==null)){
    			if(!this.mLAComSchema.getBankType().equals("01")){
    				String tSQL="select endflag from lacom where agentcom='"
    					+this.mLAComSchema.getUpAgentCom()+"'";
    				ExeSQL tExeSQL=new ExeSQL();
    				String tResult=tExeSQL.getOneValue(tSQL);
    				if(tResult.equals("Y")){
    					CError tError = new CError();
    		            tError.moduleName = "ALAComBL";
    		            tError.functionName = "checkData()";
    		            tError.errorMessage = "该代理机构的上级机构为停业状态，该代理机构不能修改为开业状态！";
    		            this.mErrors.addOneError(tError);
    		            return false;
    				}
        		}
    		}
    		
    	}
    	return true;
    }
    
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        int tCount = 0;
        LAComSet tempLAComSet = new LAComSet();
        
        System.out.println("enddate"+this.mLAComSchema.getEndDate());
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            //根据上级和所属总行代码生成银行代码
            if(Bank.equals("")||Bank==null)
          {
            CError tError = new CError();
            tError.moduleName = "ALAComBL";
            tError.functionName = "dealData()";
            tError.errorMessage = "生成中介机构代码时出错！";
            this.mErrors.addOneError(tError);
            return false;
          }
           System.out.println("与机构关系放入完毕111111111111111111111111111111111111!"+mLAComSchema.getUpAgentCom());
            String tAgentCom=createAgentcom();
           this.mLAComSchema.setAgentCom(tAgentCom);
           this.mLAContSchema.setAgentCom(tAgentCom);
            //代理机构代码自己填写count(*)
            LAComDB tLAComDB = new LAComDB();
           // tLAComDB.setAgentCom(mLAComSchema.getAgentCom());
            tLAComDB.setAgentCom(tAgentCom);
            //tCount = tLAComDB.getCount( );
            if (tLAComDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAComDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAComBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "该代理机构代码已存在!";
                this.mErrors.addOneError(tError);
                return false;
            }
            /*
            else{
                // @@错误处理
                this.mErrors.copyAllErrors(tLAComDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAComBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "该代理机构代码已存在！";
                this.mErrors.addOneError(tError);
                return false;
            }
            */
            this.mLAComSchema.setMakeDate(currentDate);
            this.mLAComSchema.setMakeTime(currentTime);
            this.mLAComSchema.setModifyDate(currentDate);
            this.mLAComSchema.setModifyTime(currentTime);
            
            this.mLAContSchema.setProtocolNo(createProtocalNo());
            this.mLAContSchema.setOperator(this.mGlobalInput.Operator);
            this.mLAContSchema.setMakeDate(currentDate);
            this.mLAContSchema.setMakeTime(currentTime);
            this.mLAContSchema.setModifyDate(currentDate);
            this.mLAContSchema.setModifyTime(currentTime);
//            if(this.mCoverFlag.equals("Y")){
//            	if(!coverAgentCom(this.mLAComSchema.getAgentCom())){
//            		return false;
//            	}
//            }
            mMap.put(mLAComSchema, "INSERT");
            mMap.put(mLAContSchema, "INSERT");
//            mMap.put(this.mInLAContSet, "INSERT");
//            mMap.put(this.mUpLAContSet, "UPDATE");
//            mMap.put(this.mLAComSet2, "UPDATE");
        }
        else if (this.mOperate.equals("UPDATE||MAIN") ||
                 this.mOperate.equals("DELETE||MAIN"))
        {
            LAComDB tLAComDB = new LAComDB();
            tLAComDB.setAgentCom(mLAComSchema.getAgentCom());
            //tLAComDB.getInfo();
           // tCount = tLAComDB.getCount();
            System.out.println("tCount="+tCount);
            /*
            if (tCount == -1)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAComDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAComBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "代理机构个数查询操作失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            */
            if (!tLAComDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAComDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAComBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "该代理机构代码不存在！";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (this.mOperate.equals("UPDATE||MAIN"))
            {
            	System.out.println("修改时从前台获取的managecom为："+this.mLAComSchema.getManageCom());
                System.out.println(mLAComSchema.getUpAgentCom());
                this.mLAComSchema.setMakeDate(tLAComDB.getMakeDate());
                this.mLAComSchema.setMakeTime(tLAComDB.getMakeTime());
                this.mLAComSchema.setModifyDate(currentDate);
                this.mLAComSchema.setModifyTime(currentTime);
                this.mLAComSchema.setOperator(this.mGlobalInput.Operator);
                
                LAComSchema tLAComSchema= new LAComSchema();
                tLAComSchema=tLAComDB.getSchema();
                Reflections tReflections=new Reflections();
                tReflections.transFields(mLAComBSchema, tLAComSchema);
                mLAComBSchema.setEdorNo(PubFun1.CreateMaxNo("EdorNo", 19));
                mLAComBSchema.setEdorType("01");
                mLAComBSchema.setAreaType(mLAComSchema.getAreaType());
                mLAComBSchema.setOperator(tLAComSchema.getOperator());
                mLAComBSchema.setModifyDate(currentDate);
                mLAComBSchema.setModifyTime(currentTime);
                mLAComBSchema.setMakeDate(this.currentDate);
                mLAComBSchema.setMakeTime(this.currentTime);
                mLAComBSchema.setMakeDate2(tLAComSchema.getMakeDate());
                mLAComBSchema.setMakeTime2(tLAComSchema.getMakeTime());
                mLAComBSchema.setModifyDate2(tLAComSchema.getModifyDate());
                mLAComBSchema.setModifyTime2(tLAComSchema.getModifyTime());
                
                if(checkContExists(this.mLAContSchema,this.mLAComSchema.getAgentCom())){
                	if(this.mStartDate.equals(this.mLAContSchema.getSignDate())
                			&&this.mEndDate.equals(this.mLAContSchema.getEndDate())){
                		
                	}else{
                		this.mLAContSchema.setOperator(this.mGlobalInput.Operator);
                    	this.mLAContSchema.setModifyDate(this.currentDate);
                    	this.mLAContSchema.setModifyTime(this.currentTime);
                    	mMap.put(mLAContSchema, "UPDATE");
                	}
                }else{
                	this.mLAContSchema.setProtocolNo(createProtocalNo());
                	this.mLAContSchema.setOperator(this.mGlobalInput.Operator);
                	this.mLAContSchema.setMakeDate(this.currentDate);
                	this.mLAContSchema.setMakeTime(this.currentTime);
        			this.mLAContSchema.setModifyDate(this.currentDate);
        			this.mLAContSchema.setModifyTime(this.currentTime);
        			mMap.put(mLAContSchema, "INSERT");
                }
                if(this.mCoverFlag.equals("Y")){
                	if(!coverAgentCom(this.mLAComSchema.getAgentCom())){
                		return false;
                	}
                }
            }
            mMap.put(mLAComSchema, "UPDATE"); 
            mMap.put(this.mInLAContSet, "INSERT");
            mMap.put(this.mUpLAContSet, "UPDATE");
            mMap.put(this.mLAComSet2, "UPDATE");
            mMap.put(mLAComBSchema, "INSERT");
        
            if((this.mEndFlag1.equals("N")||this.mEndFlag1==null||this.mEndFlag1.equals(""))
            		&&this.mLAComSchema.getEndFlag().equals("Y")){
//            	如果将该代理机构置为停业，则需要处理代理机构下的所有代理机构
            	String upSQL = "update LACom set endflag='Y' , enddate='"+this.mLAComSchema.getEndDate()+"'" +
      	  			" where agentcom like '"+this.mLAComSchema.getAgentCom()+"%' " +
      	  				" and agentcom <> '"+this.mLAComSchema.getAgentCom()+"' " +
      	  						" and (endflag='N' or endflag is null)";
            	mMap.put(upSQL, "UPDATE");
            }//modified by miaoxz,at 2008-11-5,cbs00020684
            
        }
        tReturn = true;
        return tReturn;
    }
    
    private String createProtocalNo()
    {
            String protocolNo = null;
            if(this.mProtocolNo==null||this.mProtocolNo.equals("")){
            	String validDate = this.mLAContSchema.getStartDate();
            	validDate = com.sinosoft.lis.pubfun.AgentPubFun.formatDate(
                    validDate,
                    "yyyy");
            	//validDate = validDate.substring(2,4);
            	String tAgentCom = this.mLAContSchema.getAgentCom();
            	tAgentCom = tAgentCom.substring(0, 2);
            	String tManageCom = this.mLAContSchema.getManageCom();
            	tManageCom=tManageCom.substring(2,4);
            	tAgentCom = tAgentCom+tManageCom+"0000";
            	BigInteger tBigInteger = new BigInteger(tAgentCom.substring(2));
            	BigInteger tOneBigInteger = new BigInteger("1");
            	String sql =
                    "select Max(protocolno) from lacont where protocolno like '" +
                    tAgentCom + validDate + "%' ";
            	ExeSQL tExeSQL = new ExeSQL();
            	String maxProtocolNo = tExeSQL.getOneValue(sql);
            
            	if (maxProtocolNo == null || maxProtocolNo.equals(""))
                {
                    protocolNo = tAgentCom + validDate + "0001";
                    System.out.println("protocolNo:{" + protocolNo + "}");
                }
                else
                {
                    tBigInteger = new BigInteger(maxProtocolNo.substring(2));
                    tBigInteger = tBigInteger.add(tOneBigInteger);
                    protocolNo = tAgentCom.substring(0, 2) + tBigInteger.toString();
                }
            }else{
            	BigInteger tBigInteger = new BigInteger(this.mProtocolNo.substring(2));
            	BigInteger tOneBigInteger = new BigInteger("1");
            	tBigInteger = tBigInteger.add(tOneBigInteger);
                protocolNo = this.mLAContSchema.getAgentCom().substring(0, 2) + tBigInteger.toString();
            }
        this.mProtocolNo =protocolNo;   
        return protocolNo;
    }
    private boolean coverAgentCom(String tAgentCom){
    	StringBuffer tSB=new StringBuffer();
    	LAComDB tLAComDB=new LAComDB();
    	LAComSet tLAComSet=new LAComSet();
    	LAContSchema tLAContSchema=null;
    	
    	tSB.append("select * from lacom where branchtype='3' and branchtype2='01' ")
    		.append("and upagentcom='")
    		.append(tAgentCom)
    		.append("' with ur")
    		;
    	tLAComSet=tLAComDB.executeQuery(tSB.toString());
    	for(int i=1;i<=tLAComSet.size();i++){
    		coverAgentCom(tLAComSet.get(i).getAgentCom());
    		tLAComSet.get(i)
    		.setLicenseStartDate(this.mLAComSchema.getLicenseStartDate());
    		tLAComSet.get(i)
    		.setEndDate(this.mLAComSchema.getLicenseEndDate());
    		tLAComSet.get(i).setOperator(this.mGlobalInput.Operator);
    		tLAComSet.get(i).setModifyDate(this.currentDate);
    		tLAComSet.get(i).setModifyTime(this.currentTime);
    		this.mLAComSet2.add(tLAComSet.get(i));
    		tLAContSchema=new LAContSchema();
    		if(checkContExists(tLAContSchema,tLAComSet.get(i).getAgentCom())){
    			tLAContSchema.setAgentCom(tLAComSet.get(i).getAgentCom());
    			tLAContSchema.setManageCom(tLAComSet.get(i).getManageCom());
    			tLAContSchema.setProtocolType("0");
    			tLAContSchema.setSignDate(this.mLAContSchema.getSignDate());
    			tLAContSchema.setStartDate(this.mLAContSchema.getStartDate());
    			tLAContSchema.setEndDate(this.mLAContSchema.getEndDate());
    			tLAContSchema.setOperator(this.mGlobalInput.Operator);
    			tLAContSchema.setModifyDate(this.currentDate);
    			tLAContSchema.setModifyTime(this.currentTime);
    			this.mUpLAContSet.add(tLAContSchema);
    		}else{
    			tLAContSchema.setProtocolNo(createProtocalNo());
    			tLAContSchema.setAgentCom(tLAComSet.get(i).getAgentCom());
    			tLAContSchema.setManageCom(tLAComSet.get(i).getManageCom());
    			tLAContSchema.setProtocolType("0");
    			tLAContSchema.setSignDate(this.mLAContSchema.getSignDate());
    			tLAContSchema.setStartDate(this.mLAContSchema.getStartDate());
    			tLAContSchema.setEndDate(this.mLAContSchema.getEndDate());
    			tLAContSchema.setOperator(this.mGlobalInput.Operator);
    			tLAContSchema.setMakeDate(this.currentDate);
    			tLAContSchema.setMakeTime(this.currentTime);
    			tLAContSchema.setModifyDate(this.currentDate);
    			tLAContSchema.setModifyTime(this.currentTime);
    			this.mInLAContSet.add(tLAContSchema);
    		}
    		
    	}
    	return true;
    }
    private boolean checkContExists(LAContSchema tLAContSchema,String tAgentCom){
    	StringBuffer tSB=new StringBuffer();
    	LAContDB tLAContDB=new LAContDB();
    	LAContSet tLAContSet=new LAContSet();
    	tSB.append("select * from lacont where agentcom='")
		.append(tAgentCom)
		.append("' fetch first 1 rows only with ur")
		;
    	tLAContSet=tLAContDB.executeQuery(tSB.toString());
    	if(tLAContSet.size()>=1){
    		tLAContSchema.setProtocolNo(tLAContSet.get(1).getProtocolNo());
    		tLAContSchema.setMakeDate(tLAContSet.get(1).getMakeDate());
    		tLAContSchema.setMakeTime(tLAContSet.get(1).getMakeTime());
    		this.mStartDate=tLAContSet.get(1).getSignDate();
    		this.mEndDate=tLAContSet.get(1).getEndDate();
    		return true;
    	}else{
    		return false;
    	}
    	
    }
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAComSchema.setSchema((LAComSchema) cInputData.
                                    getObjectByObjectName("LAComSchema", 0));
        this.mLAContSchema.setSchema((LAContSchema) cInputData.
                					getObjectByObjectName("LAContSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
         Bank=cInputData.get(2).toString();
         mEndFlag1=cInputData.get(4).toString();
         mCoverFlag=cInputData.get(5).toString();
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start ALAComBLQuery Submit...");
        LAComDB tLAComDB = new LAComDB();
        tLAComDB.setSchema(this.mLAComSchema);
        this.mLAComSet = tLAComDB.query();
        this.mResult.add(this.mLAComSet);
        System.out.println("End ALAComBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLAComDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAComDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAComBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }


    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
   //         this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    public VData getResult()
    {
        return this.mResult;
    }
    public String getAgentCom()
    {
        return this.mLAComSchema.getAgentCom();
    }
    public String createAgentcom()
    {
        String tAgentCom = "";
        String tUpAgentCom = mLAComSchema.getUpAgentCom();
        String tBankType=mLAComSchema.getBankType();
        if (Bank.substring(0, 2).equals("PY"))
        {//所有都加3位代码
            if (tBankType.equals("01"))
            {

                String sql = "select max(agentcom) from lacom where  agentcom like'" +
                             Bank + "%' and BankType='01' and actype in('01','11')";
                ExeSQL tExeSQL = new ExeSQL();
                String maxProtocolNo = tExeSQL.getOneValue(sql);
                if (maxProtocolNo == null || maxProtocolNo.equals(""))
                {
                    tAgentCom = Bank + "001";
                }
                else 
                {
//  tBigInteger=new BigInteger(maxProtocolNo.substring(2) );
                    int len = maxProtocolNo.length();
                    len = len - 3;
                    BigInteger tBigInteger = new BigInteger(maxProtocolNo.
                            substring(len, len + 3));
                    BigInteger tOneBigInteger = new BigInteger("1");
                    tBigInteger = tBigInteger.add(tOneBigInteger);
                    String mint = tBigInteger.toString();
                    if (mint.length() == 1)
                    {
                        mint = "00" + mint;
                    }
                    else if (mint.length() == 2)
                    {
                        mint = "0" + mint;
                    }
                    tAgentCom = Bank + mint;
                }
                
            }
            else if(tBankType.equals("02")||
            		(tBankType.equals("03")&&tUpAgentCom.trim().length()==11)||
            		(tBankType.equals("04")&&tUpAgentCom.trim().length()==14))
            {
                String sql =
                        "select max(agentcom) from lacom where upagentcom='" +
                        tUpAgentCom + "' and agentcom like'" + Bank +
                        "%' and BankType<>'01' and actype='01'";
                ExeSQL tExeSQL = new ExeSQL();
                String maxProtocolNo = tExeSQL.getOneValue(sql);
                if (maxProtocolNo == null || maxProtocolNo.equals(""))
                {
                    tAgentCom = tUpAgentCom + "001";
                }
                else
                {
                    //  tBigInteger=new BigInteger(maxProtocolNo.substring(2) );
                    int len = maxProtocolNo.length() - 3;
                    BigInteger tBigInteger = new BigInteger(maxProtocolNo.
                            substring(len, len + 3));
                    BigInteger tOneBigInteger = new BigInteger("1");
                    tBigInteger = tBigInteger.add(tOneBigInteger);
                    String mint = tBigInteger.toString();
                    if (mint.length() == 1)
                    {
                        mint = "00" + mint;
                    }
                    else if (mint.length() == 2)
                    {
                      mint = "0" + mint;
                    }
                    tAgentCom = tUpAgentCom + mint;
                }

            }
            else 
            {
            	if(tBankType.equals("03")&&tUpAgentCom.trim().length()==8)
            		Bank = tUpAgentCom+"000";
            	else if (tBankType.equals("04")&&tUpAgentCom.trim().length()==8)
            		Bank = tUpAgentCom+"000000";	
            	else if(tBankType.equals("04")&&tUpAgentCom.trim().length()==11)
            		Bank = tUpAgentCom+"000";	
            	
            	System.out.println("上级机构编码:"+tUpAgentCom);    

                String sql =
                        "select max(agentcom) from lacom where upagentcom='" +
                        tUpAgentCom + "' and agentcom like'" + Bank +
                        "%' and BankType<>'01' and actype='01'";
                ExeSQL tExeSQL = new ExeSQL();
                String maxProtocolNo = tExeSQL.getOneValue(sql);
                if (maxProtocolNo == null || maxProtocolNo.equals(""))
                {
                	if(tBankType.equals("03")&&tUpAgentCom.trim().length()==8||
                			tBankType.equals("04")&&tUpAgentCom.trim().length()==11	)
                        tAgentCom = tUpAgentCom +"000001";//补上3为000
                	else if(tBankType.equals("04")&&tUpAgentCom.trim().length()==8)
                		tAgentCom = tUpAgentCom +"000000001";//补上6为000000 	
                		
                }
                else
                {
                    //  tBigInteger=new BigInteger(maxProtocolNo.substring(2) );
                    int len = maxProtocolNo.length() - 3;
                    BigInteger tBigInteger = new BigInteger(maxProtocolNo.
                            substring(len, len + 3));
                    BigInteger tOneBigInteger = new BigInteger("1");
                    tBigInteger = tBigInteger.add(tOneBigInteger);
                    String mint = tBigInteger.toString();
                    if (mint.length() == 1)
                    {
                        mint = "00" + mint;
                    }
                    else if (mint.length() == 2)
                    {
                      mint = "0" + mint;
                    }
                    tAgentCom = Bank + mint;
                }
            }
            
        }
        else if (Bank.substring(0, 2).equals("PJ"))
        {//交叉销售分公司+3位代码，支公司和营业部加两位代码
            if (tBankType.equals("01"))
            {

                String sql = "select max(agentcom) from lacom where  agentcom like'" +
                             Bank + "%' and BankType='01' and actype='05'";
                ExeSQL tExeSQL = new ExeSQL();
                String maxProtocolNo = tExeSQL.getOneValue(sql);
                if (maxProtocolNo == null || maxProtocolNo.equals(""))
                {
                    tAgentCom = Bank + "001";
                }
                else
                {
//  tBigInteger=new BigInteger(maxProtocolNo.substring(2) );
                    int len = maxProtocolNo.length();
                    len = len - 3;
                    BigInteger tBigInteger = new BigInteger(maxProtocolNo.
                            substring(len, len + 3));
                    BigInteger tOneBigInteger = new BigInteger("1");
                    tBigInteger = tBigInteger.add(tOneBigInteger);
                    String mint = tBigInteger.toString();
                    if (mint.length() == 1)
                    {
                        mint = "00" + mint;
                    }
                    else if (mint.length() == 2)
                    {
                        mint = "0" + mint;
                    }
                    tAgentCom = Bank + mint;
                }
            }
            else
            {
                String sql =
                        "select max(agentcom) from lacom where upagentcom='" +
                        tUpAgentCom + "' and agentcom like'" + Bank +
                        "%' and BankType<>'01' and actype='05'";
                ExeSQL tExeSQL = new ExeSQL();
                String maxProtocolNo = tExeSQL.getOneValue(sql);
                if (maxProtocolNo == null || maxProtocolNo.equals(""))
                {
                    tAgentCom = tUpAgentCom + "01";
                }
                else
                {
                    //  tBigInteger=new BigInteger(maxProtocolNo.substring(2) );
                    int len = maxProtocolNo.length() - 2;
                    BigInteger tBigInteger = new BigInteger(maxProtocolNo.
                            substring(len, len + 2));
                    BigInteger tOneBigInteger = new BigInteger("1");
                    tBigInteger = tBigInteger.add(tOneBigInteger);
                    String mint = tBigInteger.toString();
                    if (mint.length() == 1)
                    {
                        mint = "0" + mint;
                    }
                    tAgentCom = tUpAgentCom + mint;
                }

            }
        }
            return tAgentCom;

        }

}
