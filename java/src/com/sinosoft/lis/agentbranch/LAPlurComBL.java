package com.sinosoft.lis.agentbranch;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LAComToAgentDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.lis.schema.LAComToAgentBSchema;
import com.sinosoft.lis.schema.LAComToAgentSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LAComToAgentBSet;
import com.sinosoft.lis.vschema.LAComToAgentSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class LAPlurComBL
{

    public LAPlurComBL()
    {
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAComSchema mLAComSchema = new LAComSchema();
    private LAComToAgentSet mLAComToAgentSet = new LAComToAgentSet();
    private LAComToAgentSchema mLAComToAgentSchema = new LAComToAgentSchema();
    private LAComToAgentBSet mLAComToAgentBSet = new LAComToAgentBSet();
    private LAComToAgentBSchema mLAComToAgentBSchema = new LAComToAgentBSchema();

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAPlurComBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAPlurComBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("Start LAPlurComBL Submit...");
        LAPlurComBLS tLAPlurComBLS = new LAPlurComBLS();
        tLAPlurComBLS.submitData(mInputData, cOperate);
        System.out.println("End LAPlurComBL Submit...");
        //如果有需要处理的错误，则返回
        if (tLAPlurComBLS.mErrors.needDealError())
        {
// @@错误处理
            this.mErrors.copyAllErrors(tLAPlurComBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAPlurComBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        LAComToAgentSet tLAComToAgentSet = new LAComToAgentSet();
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            this.mLAComSchema.setMakeDate(currentDate);
            this.mLAComSchema.setMakeTime(currentTime);
            this.mLAComSchema.setModifyDate(currentDate);
            this.mLAComSchema.setModifyTime(currentTime);
            int num = this.mLAComToAgentSet.size();
            for (int i = 1; i <= num; i++)
            {
                this.mLAComToAgentSchema = this.mLAComToAgentSet.get(i);
                System.out.println("中介机构：" +
                                   this.mLAComToAgentSchema.getAgentCom());
                if (this.mLAComToAgentSchema.getRelaType().equals("0"))
                {
                    String tAgentGroup = this.mLAComToAgentSchema.getAgentGroup();
                    if (tAgentGroup == null || tAgentGroup.equals(""))
                    {
                        continue;
                    }
                    LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
                    LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                    tLABranchGroupDB.setBranchAttr(this.mLAComToAgentSchema.
                            getAgentGroup());
                    tLABranchGroupSet = tLABranchGroupDB.query();
                    if (tLABranchGroupSet.size() > 0)
                    {
                        this.mLAComToAgentSchema.setAgentGroup(
                                tLABranchGroupSet.get(1).getAgentGroup());
                    }
                    this.mLAComToAgentSchema.setAgentCode("");
                }
                else
                {
                    String tAgentCode = this.mLAComToAgentSchema.getAgentCode();
                    if (tAgentCode == null || tAgentCode.equals(""))
                    {
                        continue;
                    }
                    LAAgentDB tLAAgentDB = new LAAgentDB();
                    tLAAgentDB.setAgentCode(this.mLAComToAgentSchema.
                                            getAgentCode());
                    tLAAgentDB.getInfo();
                    this.mLAComToAgentSchema.setAgentGroup(tLAAgentDB.
                            getAgentGroup());
                }
                this.mLAComToAgentSchema.setModifyDate(currentDate);
                this.mLAComToAgentSchema.setModifyTime(currentTime);
                this.mLAComToAgentSchema.setMakeDate(currentDate);
                this.mLAComToAgentSchema.setMakeTime(currentTime);
                this.mLAComToAgentSchema.setOperator(mGlobalInput.Operator);
                tLAComToAgentSet.add(mLAComToAgentSchema);
            }
        }
        else if (this.mOperate.equals("UPDATE||MAIN"))
        {
            LAComDB tLAComDB = new LAComDB();
            tLAComDB.setAgentCom(mLAComSchema.getAgentCom());
            if (!tLAComDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAPlurComBL";
                tError.functionName = "dealData";
                tError.errorMessage = "在处理数据时出错。";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLAComDB.setAgentCom(mLAComSchema.getAgentCom());
            tLAComDB.setName(mLAComSchema.getName());
            tLAComDB.setBusinessType(mLAComSchema.getBusinessType());
            tLAComDB.setBusinessCode(mLAComSchema.getBusinessCode());
            tLAComDB.setBusiLicenseCode(mLAComSchema.getBusiLicenseCode());
            tLAComDB.setAppAgentCom(mLAComSchema.getAppAgentCom());
            tLAComDB.setLicenseNo(mLAComSchema.getLicenseNo());
            tLAComDB.setGrpNature(mLAComSchema.getGrpNature());
            tLAComDB.setLicenseStartDate(mLAComSchema.getLicenseStartDate());
            tLAComDB.setLicenseEndDate(mLAComSchema.getLicenseEndDate());
            tLAComDB.setCorporation(mLAComSchema.getCorporation());
            tLAComDB.setLinkMan(mLAComSchema.getLinkMan());
            tLAComDB.setAddress(mLAComSchema.getAddress());
            tLAComDB.setPhone(mLAComSchema.getPhone());
            tLAComDB.setRegionalismCode(mLAComSchema.getRegionalismCode());
            tLAComDB.setEMail(mLAComSchema.getEMail());
            tLAComDB.setChiefBusiness(mLAComSchema.getChiefBusiness());
            tLAComDB.setNoti(mLAComSchema.getNoti());
            tLAComDB.setUpAgentCom(mLAComSchema.getUpAgentCom());
            tLAComDB.setManageCom(mLAComSchema.getManageCom());
            tLAComDB.setACType(mLAComSchema.getACType());
            tLAComDB.setSellFlag(mLAComSchema.getSellFlag());
            tLAComDB.setAreaType(mLAComSchema.getAreaType());
            tLAComDB.setChannelType(mLAComSchema.getChannelType());
            tLAComDB.setModifyDate(currentDate);
            tLAComDB.setModifyTime(currentTime);
            tLAComDB.setOperator(mGlobalInput.Operator);
            mLAComSchema.setSchema(tLAComDB);

            int num = this.mLAComToAgentSet.size();
            for (int i = 1; i <= num; i++)
            {
                this.mLAComToAgentSchema = this.mLAComToAgentSet.get(i);
                System.out.println("中介机构：" +
                                   this.mLAComToAgentSchema.getAgentCom());
                if (this.mLAComToAgentSchema.getRelaType().equals("0"))
                {
                    String tAgentGroup = this.mLAComToAgentSchema.getAgentGroup();
                    if (tAgentGroup == null || tAgentGroup.equals(""))
                    {
                        continue;
                    }
                    LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
                    LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                    tLABranchGroupDB.setBranchAttr(this.mLAComToAgentSchema.
                            getAgentGroup());
                    tLABranchGroupSet = tLABranchGroupDB.query();
                    if (tLABranchGroupSet.size() > 0)
                    {
                        this.mLAComToAgentSchema.setAgentGroup(
                                tLABranchGroupSet.get(1).getAgentGroup());
                    }
                    this.mLAComToAgentSchema.setAgentCode("");
                }
                else
                {
                    String tAgentCode = this.mLAComToAgentSchema.getAgentCode();
                    if (tAgentCode == null || tAgentCode.equals(""))
                    {
                        continue;
                    }
                    LAAgentDB tLAAgentDB = new LAAgentDB();
                    tLAAgentDB.setAgentCode(this.mLAComToAgentSchema.
                                            getAgentCode());
                    tLAAgentDB.getInfo();
                    this.mLAComToAgentSchema.setAgentGroup(tLAAgentDB.
                            getAgentGroup());
                }
                this.mLAComToAgentSchema.setMakeDate(currentDate);
                this.mLAComToAgentSchema.setMakeTime(currentTime);
                this.mLAComToAgentSchema.setModifyDate(currentDate);
                this.mLAComToAgentSchema.setModifyTime(currentTime);
                this.mLAComToAgentSchema.setOperator(mGlobalInput.Operator);
                tLAComToAgentSet.add(mLAComToAgentSchema);
            }
            LAComToAgentDB aLAComToAgentDB = new LAComToAgentDB();
            aLAComToAgentDB.setAgentCom(mLAComToAgentSchema.getAgentCom());
            LAComToAgentSet aLAComToAgentSet = aLAComToAgentDB.query();
            for (int a = 1; a <= aLAComToAgentSet.size(); a++)
            {
                LAComToAgentSchema aLAComToAgentSchema = aLAComToAgentSet.get(a);
                backup("01", aLAComToAgentSchema);
            }
        }
        else if (this.mOperate.equals("DELETE||MAIN"))
        {
            int num = this.mLAComToAgentSet.size();
            for (int i = 1; i <= num; i++)
            {
                this.mLAComToAgentSchema = this.mLAComToAgentSet.get(i);
                System.out.println("中介机构：" +
                                   this.mLAComToAgentSchema.getAgentCom());

                if (this.mLAComToAgentSchema.getRelaType().equals("0"))
                {
                    String tAgentGroup = this.mLAComToAgentSchema.getAgentGroup();
                    if (tAgentGroup == null || tAgentGroup.equals(""))
                    {
                        continue;
                    }
                    LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
                    LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                    tLABranchGroupDB.setBranchAttr(this.mLAComToAgentSchema.
                            getAgentGroup());
                    tLABranchGroupSet = tLABranchGroupDB.query();
                    if (tLABranchGroupSet.size() > 0)
                    {
                        this.mLAComToAgentSchema.setAgentGroup(
                                tLABranchGroupSet.get(1).getAgentGroup());
                    }
                    this.mLAComToAgentSchema.setAgentCode("");
                }
                else
                {
                    String tAgentCode = this.mLAComToAgentSchema.getAgentCode();
                    if (tAgentCode == null || tAgentCode.equals(""))
                    {
                        continue;
                    }
                    LAAgentDB tLAAgentDB = new LAAgentDB();
                    tLAAgentDB.setAgentCode(this.mLAComToAgentSchema.
                                            getAgentCode());
                    tLAAgentDB.getInfo();
                    this.mLAComToAgentSchema.setAgentGroup(tLAAgentDB.
                            getAgentGroup());
                }

                tLAComToAgentSet.add(mLAComToAgentSchema);
            }

            LAComToAgentDB aLAComToAgentDB = new LAComToAgentDB();
            aLAComToAgentDB.setAgentCom(mLAComToAgentSchema.getAgentCom());
            LAComToAgentSet aLAComToAgentSet = aLAComToAgentDB.query();
            for (int a = 1; a <= aLAComToAgentSet.size(); a++)
            {
                LAComToAgentSchema aLAComToAgentSchema = aLAComToAgentSet.get(a);
                backup("02", aLAComToAgentSchema);
            }
        }
        mLAComToAgentSet.clear();
        mLAComToAgentSet.set(tLAComToAgentSet);
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mLAComSchema.setSchema((LAComSchema) cInputData.getObjectByObjectName(
                "LAComSchema", 0));
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLAComToAgentSet.set((LAComToAgentSet) cInputData.getObjectByObjectName(
                "LAComToAgentSet", 0));
        return true;
    }


    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mLAComSchema);
            this.mInputData.add(this.mLAComToAgentSet);
            this.mInputData.add(this.mLAComToAgentBSet);

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAPlurComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    public VData getResult()
    {
        return this.mResult;
    }

    private boolean backup(String aEdorType,
                           LAComToAgentSchema aLAComToAgentSchema)
    {
//      String currentDate=PubFun.getCurrentDate();
//      String currentTime=PubFun.getCurrentTime();
        this.mLAComToAgentBSchema.setAgentCode(aLAComToAgentSchema.getAgentCode());
        this.mLAComToAgentBSchema.setAgentCom(aLAComToAgentSchema.getAgentCom());
        this.mLAComToAgentBSchema.setAgentGroup(aLAComToAgentSchema.
                                                getAgentGroup());
        this.mLAComToAgentBSchema.setRelaType(aLAComToAgentSchema.getRelaType());
        this.mLAComToAgentBSchema.setEdorNo(PubFun1.CreateMaxNo("EdorNo", 20));
        this.mLAComToAgentBSchema.setEdorType(aEdorType);
        this.mLAComToAgentBSchema.setMakeDate(aLAComToAgentSchema.getMakeDate());
        this.mLAComToAgentBSchema.setMakeTime(aLAComToAgentSchema.getMakeTime());
        this.mLAComToAgentBSchema.setModifyDate(aLAComToAgentSchema.
                                                getModifyDate());
        this.mLAComToAgentBSchema.setModifyTime(aLAComToAgentSchema.
                                                getModifyTime());
        this.mLAComToAgentBSchema.setOperator(mGlobalInput.Operator);
        LAComToAgentBSchema aLAComToAgentBSchema = new LAComToAgentBSchema();
        aLAComToAgentBSchema.setSchema(mLAComToAgentBSchema);
        mLAComToAgentBSet.add(aLAComToAgentBSchema);
        return true;
    }

    public static void main(String[] args)
    {
        LAPlurComBL LAPlurComBL1 = new LAPlurComBL();
        LAComSchema tLAComSchema = new LAComSchema();
        tLAComSchema.setAgentCom("3");
        tLAComSchema.setOperator("001");
        tLAComSchema.setAreaType(" ");
        tLAComSchema.setChannelType(" ");
        GlobalInput tG = new GlobalInput();
        tG.ComCode = "86110000";
        tG.ManageCom = "86110000";
        tG.Operator = "001";
        LAComToAgentSchema tLAComToAgentSchema0 = new LAComToAgentSchema();
        LAComToAgentSchema tLAComToAgentSchema1 = new LAComToAgentSchema();
        LAComToAgentSet tLAComToAgentSet = new LAComToAgentSet();

        tLAComToAgentSchema0.setAgentCom("3");
        tLAComToAgentSchema0.setRelaType("0"); //与机构关联
        tLAComToAgentSchema0.setAgentGroup("86110000002");
        tLAComToAgentSchema0.setAgentCode("   ");
        tLAComToAgentSchema0.setOperator("001");
        tLAComToAgentSet.add(tLAComToAgentSchema0);

        tLAComToAgentSchema1.setAgentCom("3");
        tLAComToAgentSchema1.setRelaType("1"); //与代理人关联
        tLAComToAgentSchema1.setAgentCode("8611000506");
        tLAComToAgentSchema1.setAgentGroup("   ");
        tLAComToAgentSchema1.setOperator("001");
        tLAComToAgentSet.add(tLAComToAgentSchema1);

        VData tVData = new VData();
        tVData.add(tLAComSchema);
        tVData.add(tLAComToAgentSet);
        tVData.add(tG);
        LAPlurComBL1.submitData(tVData, "UPDATE||MAIN");
    }
}
