package com.sinosoft.lis.agentbranch;

import java.sql.Connection;

import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */

public class LAGrpSpecComBLS
{

    public LAGrpSpecComBLS()
    {
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 数据操作字符串 */
    private String mOperate;

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Start LASpecComBLS Submit...");
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            tReturn = saveLACom(cInputData);
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            tReturn = updateLACom(cInputData);
        }
        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End LAGrpSpecComBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean saveLACom(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LASpecComBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAComDB tLAComDB = new LAComDB(conn);
            tLAComDB.setSchema((LAComSchema) mInputData.getObjectByObjectName(
                    "LAComSchema", 0));
            if (!tLAComDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAComDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LASpecComBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGrpSpecComBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
            ;
        }
        try
        {
            conn.close();
        }
        catch (Exception e)
        {}
        ;
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean updateLACom(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "LASpecComBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAComDB tLAComDB = new LAComDB(conn);
            tLAComDB.setSchema((LAComSchema) mInputData.getObjectByObjectName(
                    "LAComSchema", 0));
            if (!tLAComDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAComDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LASpecComBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGrpSpecComBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
        }
        try
        {
            conn.close();
        }
        catch (Exception e)
        {}
        ;
        return tReturn;
    }
}
