/*
 * <p>ClassName: DimChangeAgentToBranch </p>
 * <p>Description: DimChangeAgentToBranch类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-27
 */
package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.agent.LARecomPICCHBL;
import com.sinosoft.lis.agent.LARearPICCHBL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.lis.schema.LARecomRelationSchema;
import com.sinosoft.lis.db.LARecomRelationDB;

public class DimChangeAgentToBranch {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String mOperate;
    private String tEdorNo;
    private String mAimManageCom;
    private String mOldManageCom;
    private String mOldAgentGroup;
    private String mAStartDate;
    private boolean IsSingle = true;

    /** 业务处理相关变量 */
    private LABranchGroupSchema mLABranchSchema = new LABranchGroupSchema();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LACommisionSet mLACommisionSet = new LACommisionSet();

    //更新所有调动人员和管理人员在行政表中的AgentGroup UpBranch
    private LATreeSet mUpdateLATreeSet = new LATreeSet();

    //备份所有调动人员和管理人员
    private LATreeBSet mLATreeBSet = new LATreeBSet();

    //更新所有调动人员在LAAgent表中的AgentGroup
    private LAAgentSet mLAAgentSet = new LAAgentSet();

    //备份代理人记录 add by jiangcx minLABranchGroupBSet
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();
    //备份代理人记录 labranchgroupb表中的branchmanager
    private LABranchGroupBSet minLABranchGroupBSet = new LABranchGroupBSet();

    private LARecomRelationSet mLARecomRelationSet = new LARecomRelationSet();
    //更新所有调动人员
    private LABranchGroupSet mupLABranchGroupSet = new LABranchGroupSet();
    private MMap mMap = new MMap();

    private VData mOutputDate = new VData();

    public DimChangeAgentToBranch() {
    }

    public static void main(String[] args) {
        LABranchGroupSchema mLABranchSchema = new LABranchGroupSchema();
        LATreeSchema mManagerTreeSchema = new LATreeSchema();
        LATreeSet mLATreeSet = new LATreeSet();
        AdjustAgentUI mAdjustAgentUI = new AdjustAgentUI();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "mak001";
        tG.ComCode = "86";
        tG.ManageCom = "86";
        mLABranchSchema.setAgentGroup("000000000671");
        mLABranchSchema.setBranchAttr("86110000080101");
        mLABranchSchema.setBranchLevel("01");
        mLABranchSchema.setBranchManager("1101000408");
        mLABranchSchema.setUpBranch("");
        mLABranchSchema.setBranchType("1");
        mLABranchSchema.setBranchType2("01");

        LATreeSchema tLATreeSchema = new LATreeSchema();
        tLATreeSchema.setAgentCode("1101000583");
        tLATreeSchema.setAgentGrade("A02"); //代理人职级
        tLATreeSchema.setAgentGroup("000000000160");
        tLATreeSchema.setBranchType("1");
        tLATreeSchema.setBranchType2("01");
        tLATreeSchema.setAstartDate("2007-01-01");
        mLATreeSet.add(tLATreeSchema);

        TransferData tdata = new TransferData();
        tdata.setNameAndValue("IsSingle", "1");
        VData tVData = new VData();
        tVData.add(tG);
        tVData.addElement(mLABranchSchema);
        tVData.addElement(mLATreeSet);
        tVData.addElement(tdata);
        mAdjustAgentUI.submitData(tVData, "INSERT||MAIN");
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "DimChangeAgentToBranch";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 校验的代码
     * @return
     */
    private boolean check() {
        System.out.println("---start check----");
        if (mLATreeSet == null || mLATreeSet.size() == 0) {
            CError tError = new CError();
            tError.moduleName = "DimChangeAgentToBranch";
            tError.functionName = "check";
            tError.errorMessage = "请选择需要调整的人员!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("-----------");
        //查询目的机构是否存在
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setBranchAttr(this.mLABranchSchema.getBranchAttr());
        tLABranchGroupDB.setBranchType(this.mLABranchSchema.getBranchType());
        tLABranchGroupDB.setBranchType2(this.mLABranchSchema.getBranchType2());
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupSet = tLABranchGroupDB.query();
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
            CError tError = new CError();
            tError.moduleName = "DimChangeAgentToBranch";
            tError.functionName = "check";
            tError.errorMessage = "没有查询到" + this.mLABranchSchema.getBranchAttr() +
                                  "的机构信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = tLABranchGroupSet.get(1);
        mLABranchSchema.setAgentGroup(tLABranchGroupSchema.getAgentGroup());
        System.out.println(mLABranchSchema.getAgentGroup());
        if (tLABranchGroupSchema.getEndFlag() != null && tLABranchGroupSchema.
            getEndFlag().equals("Y")) {
            CError tError = new CError();
            tError.moduleName = "DimChangeAgentToBranch";
            tError.functionName = "check";
            tError.errorMessage = "机构" + this.mLABranchSchema.getBranchAttr() +
                                  "已经停业，不能继续调入人员!";
            this.mErrors.addOneError(tError);
            return false;

        }
        if (!tLABranchGroupSchema.getBranchLevel().equals("01"))
        {
            buildError("check","目标机构必须是处级机构！");
            return false;
        }
        if(!tLABranchGroupSchema.getManageCom().equals(mOldManageCom))
        {
            buildError("check","人员不能跨管理机构调动！");
            return false;
        }
        if(tLABranchGroupSchema.getAgentGroup().equals(mOldAgentGroup))
        {
           buildError("check","人员不能在本机构内调动！");
           return false;
        }


        //校验调动日期
       // String tStartDate = this.mLATreeSet.get(1).getAstartDate();
        System.out.println("tStartDate:" + mAStartDate);
        String AgentCode = this.mLATreeSet.get(1).getAgentCode();
        System.out.println("AgentCode:" + AgentCode);
        String newStartDate = AgentPubFun.formatDate(mAStartDate, "yyyy-MM-dd");
        System.out.println("newStartDate:" + newStartDate);
        String tDay = newStartDate.substring(newStartDate.lastIndexOf("-") + 1);
        if (!tDay.equals("01")) {
            CError tError = new CError();
            tError.moduleName = "DimChangeAgentToBranch";
            tError.functionName = "check";
            tError.errorMessage = "调整日期必须是从某个月的1号开始!";
            this.mErrors.addOneError(tError);
            return false;
        } else {
            PubCheckField checkField1 = new PubCheckField();
            VData cInputData = new VData();
            TransferData tTransferData = new TransferData();

            String oldAgentGroup = mLATreeSet.get(1).getAgentGroup();
            String NewAgentGroup = tLABranchGroupSchema.getAgentGroup();

            //通过 CKBYSET 方式校验
            LMCheckFieldDB tLMCheckFieldDB = new LMCheckFieldDB();
            LMCheckFieldSet tLMCheckFieldSet = new LMCheckFieldSet();
            String tSql =
                    "select * from lmcheckfield where riskcode = '000000'"
                    + " and fieldname = 'DimChangeAgentToBranch' order by serialno asc";
            System.out.println("tSql is " + tSql);
            tLMCheckFieldSet = tLMCheckFieldDB.executeQuery(tSql);
            cInputData.add(tTransferData);
            cInputData.add(tLMCheckFieldSet);

            if (!checkField1.submitData(cInputData, "CKBYSET")) {
                System.out.println("Enter Error Field!");
                //此判断是用来区分是程序处理过程中报的错误，还是校验时报的错误

                if (checkField1.mErrors.needDealError()) { //程序错误处理

                    this.mErrors.copyAllErrors(checkField1.mErrors);
                    return false;
                } else {
                    VData t = checkField1.getResultMess(); //校验错误处理
                    buildError("dealData", t.get(0).toString());
                    return false;
                }
            }

        }
        return true;
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!check()) {
            return false;
        }
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "DimChangeAgentToBranch";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败DimChangeAgentToBranch-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("进入dealData处理");
        boolean tReturn = true;
        int i = 0;
        LABranchGroupDB tLABranchDB = new LABranchGroupDB();
        String[] newAgentcode = null;
        System.out.println("start to dealData...");
        tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        System.out.println("批改号：" + tEdorNo);
        //准备更新机构表中的管理人员的纪录

        tLABranchDB.setAgentGroup(this.mLABranchSchema.getAgentGroup()); //隐式
        if (!tLABranchDB.getInfo()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLABranchDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "DimChangeAgentToBranch";
            tError.functionName = "dealData";
            tError.errorMessage = "查询目标单位信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLABranchSchema = tLABranchDB.getSchema();

        //准备更新所有调动人员和管理人员在行政表中的AgentGroup和UpAgent的纪录
        String tAimBranch = this.mLABranchSchema.getAgentGroup();
        String tUpAgent = this.mLABranchSchema.getBranchManager();
        mAimManageCom = this.mLABranchSchema.getManageCom();
        System.out.println("UpAgent:" + tUpAgent + " --不管目标机构是否有管理人员");
        tUpAgent = tUpAgent == null ? "" : tUpAgent;

        LATreeDB tLATreeDB = null;
        LAAgentDB tLAAgentDB = null;

        int tCount = this.mLATreeSet.size();
        System.out.println("调动人数：" + tCount);
        String aimManageCom = this.mAimManageCom;
        for (i = 1; i <= tCount; i++) {
            tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(this.mLATreeSet.get(i).getAgentCode()); //jiangcx add for BK 主键
            if (!tLAAgentDB.getInfo()) {
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "DimChangeAgentToBranch";
                tError.functionName = "dealData";
                tError.errorMessage = "查询业务员" + mLATreeSet.get(i).getAgentCode() +
                                      "的基础信息失败!";
                this.mErrors.addOneError(tError);
                return false;

            }
            System.out.println(tLABranchDB.getManageCom() + "...." +
                               tLAAgentDB.getManageCom() + "......." +
                               this.mLABranchSchema.getBranchType());
            LATreeSchema tLATreeSchema = this.mLATreeSet.get(i);
            tLATreeDB = new LATreeDB();
            tLATreeDB.setAgentCode(this.mLATreeSet.get(i).getAgentCode());
            System.out.println("i=" + i + "  " + tLATreeDB.getAgentCode());
            if (!tLATreeDB.getInfo()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "DimChangeAgentToBranch";
                tError.functionName = "dealData";
                tError.errorMessage = "查询目标单位信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LATreeSchema bLATreeSchema = new LATreeSchema();
            LATreeBSchema tLATreeBSchema = new LATreeBSchema();
            bLATreeSchema = tLATreeDB.getSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLATreeBSchema, bLATreeSchema);
            tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
            tLATreeBSchema.setOperator(mGlobalInput.Operator);
            tLATreeBSchema.setEdorNO(tEdorNo);
            tLATreeBSchema.setRemoveType("03");
            tLATreeBSchema.setMakeDate2(tLATreeBSchema.getMakeDate());
            tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
            tLATreeBSchema.setMakeDate(currentDate);
            tLATreeBSchema.setMakeTime(currentTime);
            this.mLATreeBSet.add(tLATreeBSchema);

            String sql = "select max(IndexCalNo) from lawage where agentcode='" +
                         tLATreeDB.getAgentCode() + "'";
            ExeSQL tExeSQL = new ExeSQL();
            String MaxIndexCalNo = tExeSQL.getOneValue(sql);
            String AdjustDate = "";
            if (MaxIndexCalNo != null && !MaxIndexCalNo.equals("")) {
                String WageCalDate = MaxIndexCalNo;
                WageCalDate += "01";
                WageCalDate = AgentPubFun.formatDate(WageCalDate, "yyyy-MM-dd");
                WageCalDate = PubFun.calDate(WageCalDate, 1, "M", "");
//                if (!WageCalDate.equals(mAStartDate)) {
//                    CError tError = new CError();
//                    tError.moduleName = "DimChangeAgentToBranch";
//                    tError.functionName = "dealData";
//                    tError.errorMessage = "业务员" + mAStartDate +
//                                          "最近一次计算佣金是" + MaxIndexCalNo +
//                                          ",因此调动日期必须为" + WageCalDate + "";
//                    this.mErrors.addOneError(tError);
//                    continue;
//                } else {
                    AdjustDate = WageCalDate;
//                }
            } else {
                AdjustDate = tLAAgentDB.getEmployDate();
            }
            //设置上级代理人
            bLATreeSchema.setUpAgent(tUpAgent);
            bLATreeSchema.setAgentGroup(tAimBranch); //设置所属管理机构
            System.out.println("tAimBranchtAimBranch:" + tAimBranch);
            bLATreeSchema.setManageCom(mLABranchSchema.getManageCom());
            bLATreeSchema.setOperator(this.mGlobalInput.Operator);
            bLATreeSchema.setManageCom(aimManageCom);
            bLATreeSchema.setModifyDate(currentDate);
            bLATreeSchema.setModifyTime(currentTime);
            bLATreeSchema.setBranchCode(tAimBranch);
            //个险主管人员在增员、二次入司不会变更下属人员的考核开始日期2010-09-14
            //bLATreeSchema.setAstartDate(AdjustDate); //调整日期
            this.mUpdateLATreeSet.add(bLATreeSchema);
            LAAgentSchema tLAAgentSchema = new LAAgentSchema();
            tLAAgentSchema = tLAAgentDB.getSchema();
            LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
            tReflections = new Reflections();
            tReflections.transFields(tLAAgentBSchema, tLAAgentSchema);
            tLAAgentBSchema.setMakeDate(currentDate);
            tLAAgentBSchema.setMakeTime(currentTime);
            tLAAgentBSchema.setOperator(mGlobalInput.Operator);
            tLAAgentBSchema.setEdorNo(tEdorNo);
            tLAAgentBSchema.setEdorType("03");
            this.mLAAgentBSet.add(tLAAgentBSchema);
            tLAAgentSchema.setModifyDate(currentDate);
            tLAAgentSchema.setModifyTime(currentTime);
            tLAAgentSchema.setAgentGroup(tAimBranch);
            tLAAgentSchema.setBranchCode(tAimBranch);
            tLAAgentSchema.setOperator(mGlobalInput.Operator);
            tLAAgentSchema.setManageCom(mLABranchSchema.getManageCom());
            mLAAgentSet.add(tLAAgentSchema);

            //主管调动的处理,调动个险B01的主管,调动后职级不变,但不再是主管  2006-02-21添加  xiangchun
            LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setBranchManager(this.mLATreeSet.get(i).
                                              getAgentCode());
            System.out.println(this.mLATreeSet.get(i).getAgentCode());
            tLABranchGroupSet = tLABranchGroupDB.query();
            for (int k = 1; k <= tLABranchGroupSet.size(); k++) {
                LABranchGroupSchema tLABranchGroupSchema = new
                        LABranchGroupSchema();
                LABranchGroupBSchema tLABranchGroupBSchema = new
                        LABranchGroupBSchema();
                tLABranchGroupSchema = tLABranchGroupSet.get(k);
                //备份labranchgroupb表
                tReflections = new Reflections();
                tReflections.transFields(tLABranchGroupBSchema,
                                         tLABranchGroupSchema);
                System.out.println(tLABranchGroupBSchema.getAgentGroup() +
                                   "|||||" + tLABranchGroupSet.size() + "||" +
                                   k);
                //String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
                tLABranchGroupBSchema.setEdorType("07"); //修改操作
                tLABranchGroupBSchema.setEdorNo(tEdorNo);
                tLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.
                                                   getMakeDate());
                tLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.
                                                   getMakeTime());
                tLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.
                                                     getModifyDate());
                tLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.
                                                     getModifyTime());
                tLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.
                                                   getOperator());
                tLABranchGroupBSchema.setMakeDate(currentDate);
                tLABranchGroupBSchema.setMakeTime(currentTime);
                tLABranchGroupBSchema.setModifyDate(currentDate);
                tLABranchGroupBSchema.setModifyTime(currentTime);
                tLABranchGroupBSchema.setOperator(this.mGlobalInput.Operator);
                minLABranchGroupBSet.add(tLABranchGroupBSchema);
                //修改labranchgroup 中的主管为空
                tLABranchGroupSchema.setBranchManager("");
                tLABranchGroupSchema.setBranchManagerName("");
                //调整日期
                String FoundDate = AgentPubFun.formatDate(tLABranchGroupSchema.
                        getFoundDate(), "yyyy-MM-dd");
                if (FoundDate.compareTo(mAStartDate) > 0) {
                    tLABranchGroupSchema.setAStartDate(FoundDate);
                } else {
                    tLABranchGroupSchema.setAStartDate(mAStartDate);
                }
                tLABranchGroupSchema.setModifyDate(currentDate);
                tLABranchGroupSchema.setModifyTime(currentTime);
                tLABranchGroupSchema.setOperator(mGlobalInput.Operator);
                mupLABranchGroupSet.add(tLABranchGroupSchema);

            }
            sql = "select * from lacommision where agentcode='" +
                  tLAAgentSchema.getAgentCode() + "' and (caldate>='" +
                  AdjustDate + "' or caldate is null)";
            LACommisionSet tLACommisionSet = new LACommisionSet();
            LACommisionDB tLACommisionDB = new LACommisionDB();
            tLACommisionSet = tLACommisionDB.executeQuery(sql);
            for (int j = 1; j <= tLACommisionSet.size(); j++) {
                LACommisionSchema tLACommisionSchema = new LACommisionSchema();
                tLACommisionSchema = tLACommisionSet.get(j);
                tLACommisionSchema.setAgentGroup(mLABranchSchema.getAgentGroup());
                tLACommisionSchema.setBranchCode(mLABranchSchema.getAgentGroup());
                tLACommisionSchema.setBranchSeries(mLABranchSchema.
                                                   getBranchSeries());
                tLACommisionSchema.setBranchAttr(mLABranchSchema.getBranchAttr());
                tLACommisionSchema.setModifyDate(currentDate);
                tLACommisionSchema.setModifyTime(currentTime);
                tLACommisionSchema.setOperator(mGlobalInput.Operator);
                this.mLACommisionSet.add(tLACommisionSchema);
            }

            //修改推荐关系的 agentgroup
            sql = "select * from laRecomRelation where agentcode='" +
                  tLAAgentSchema.getAgentCode() + "'  ";
            System.out.println("123234342323"+sql);
            LARecomRelationSet tLARecomRelationSet = new LARecomRelationSet();
            LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();
            tLARecomRelationSet = tLARecomRelationDB.executeQuery(sql);
            for (int kk = 1; kk <= tLARecomRelationSet.size(); kk++) {
                LARecomRelationSchema tLARecomRelationSchema =
                        tLARecomRelationSet.
                        get(kk);
                tLARecomRelationSchema.setAgentGroup(mLABranchSchema.
                                                     getAgentGroup());
                tLARecomRelationSchema.setModifyDate(currentDate);
                tLARecomRelationSchema.setModifyTime(currentTime);
                tLARecomRelationSchema.setOperator(mGlobalInput.Operator);
                mLARecomRelationSet.add(tLARecomRelationSchema);
            }
        }
        if (this.mErrors.needDealError()) {
            tReturn = false;
        } else {
            tReturn = true;
        }
        return tReturn;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //清空上次的记录
        mMap = new MMap();
        mLABranchSchema = new LABranchGroupSchema();
        mLATreeSet = new LATreeSet();
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLABranchSchema.setSchema((LABranchGroupSchema) cInputData.
                                  getObjectByObjectName(
                                          "LABranchGroupSchema",1));
        this.mLATreeSet.set((LATreeSet) cInputData.getObjectByObjectName(
                "LATreeSet", 1));
        mAStartDate = (String) cInputData.get(3); //起那台录入的调整日期
        mOldManageCom=mLATreeSet.get(1).getManageCom();
        mOldAgentGroup=mLATreeSet.get(1).getAgentGroup();
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try
        {
            mMap.put(this.mLAAgentBSet, "INSERT");
            mMap.put(this.mLATreeBSet, "INSERT");
            mMap.put(this.mUpdateLATreeSet, "UPDATE");
            mMap.put(this.mLAAgentSet, "UPDATE");
            mMap.put(this.mLACommisionSet, "UPDATE");
            mMap.put(this.minLABranchGroupBSet, "INSERT");
            mMap.put(this.mupLABranchGroupSet, "UPDATE");
            mMap.put(this.mLARecomRelationSet, "UPDATE");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "DimChangeAgentToBranch";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public MMap getResult() {
        return this.mMap;
    }
}
