/*
 * <p>ClassName:  </p>
 * <p>Description: 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 
 * @CreateDate：
 */
package com.sinosoft.lis.agentbranch;

//
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;

public class LAChangeBranch2011UI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    public LAChangeBranch2011UI() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        mInputData = (VData) cInputData.clone();
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        LAChangeBranch2011BL tLAChangeBranch2011BL = new   LAChangeBranch2011BL();
        tLAChangeBranch2011BL.submitData(mInputData, mOperate);
        //如果有需要处理的错误，则返回
        if (tLAChangeBranch2011BL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAChangeBranch2011BL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAChangeBranch2011UI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mResult = tLAChangeBranch2011BL.getResult();
        return true;
    }

    public static void main(String[] args) {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ComCode = "86";

        LABranchGroupSchema mLABranchSchema = new LABranchGroupSchema();
        mLABranchSchema.setAgentGroup("000000000158");
        mLABranchSchema.setBranchLevel("01");
        mLABranchSchema.setUpBranch("000000000924");
        mLABranchSchema.setBranchAttr("86110000140103"); //增加了修改此属性的
        mLABranchSchema.setEndDate("2007-01-01");
        mLABranchSchema.setBranchType("1");
        mLABranchSchema.setBranchType2("01");
        LABranchGroupSchema mAimLABranchSchema = new LABranchGroupSchema();
        mAimLABranchSchema.setAgentGroup("000000000139"); //调整后的上级机构内部编码
        mAimLABranchSchema.setBranchLevel("02"); //调整后的上级机构级别
        mAimLABranchSchema.setName("第二营销服务部第一营业区"); //调整后的机构名称
        mAimLABranchSchema.setBranchAttr("861100000301");
        mAimLABranchSchema.setBranchType("1");
        mAimLABranchSchema.setBranchType2("01");
        TransferData tdata = new TransferData();
        tdata.setNameAndValue("IsSingle", "1");
        VData tVData = new VData();
        //  tVData.add(tVData);
        tVData.add(tG);
        tVData.addElement(mLABranchSchema);
        tVData.addElement(mAimLABranchSchema);
        tVData.addElement("2007-01-01");
        tVData.addElement(tdata);
        LAChangeBranch2011UI mChangeBranchUI = new LAChangeBranch2011UI();
        mChangeBranchUI.submitData(tVData, "INSERT||MAIN");

        System.out.println("Error---:" + mChangeBranchUI.mErrors.getFirstError());

    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }


    public VData getResult() {
        return this.mResult;
    }
}
