package com.sinosoft.lis.agentbranch;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LABaseWageSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LABranchLevelSchema;
import com.sinosoft.lis.db.LABranchLevelDB;
import com.sinosoft.lis.vschema.LABranchLevelSet;
import java.math.BigInteger;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.db.LARearRelationDB;
import com.sinosoft.lis.schema.LARearRelationBSchema;
import com.sinosoft.lis.vschema.LARearRelationBSet;

public class LAChangeBranch2011BL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();


    /** 业务处理相关变量 */
    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();
    private LABranchGroupSet mAimLABranchGroupSet = new LABranchGroupSet();
    //插入到机构备份表,备份被调动机构的信息
    private LABranchGroupBSet mLABranchGroupBSet = new LABranchGroupBSet();
    private String mManageCom = "";
    private String mEdorNo = "";    
    private LACommisionSet mLACommisionSet = new LACommisionSet(); 
    private String NewBranchAttr = "";
    private String mAdjustDate = "";
    private MMap mMap = new MMap();

    public LAChangeBranch2011BL() {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
//        //进行业务处理
//        if (!check()) {
//            return false;
//        }
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAChangeBranch2011BL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAChangeBranch2011BL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mInputData, "");
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAChangeBranch2011BL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        this.mLABranchGroupSet.set((LABranchGroupSet) cInputData.get(1));
        mAdjustDate = (String) cInputData.getObject(2);
        mManageCom=(String) cInputData.getObject(3);
        return true;
    }
    
    
    /**
     * 校验的代码
     * @return
     */
    private boolean check() {
        String tStartDate = mAdjustDate;
            String sql = "select max(indexcalno) from lawage where managecom='" +
                         mManageCom + "' and branchtype='3' and branchtype2='01'";
            ExeSQL tExeSQL = new ExeSQL();
            String maxIndexCalNo = tExeSQL.getOneValue(sql);
            System.out.println("---最近发工资的日期---" + maxIndexCalNo);
            String lastDate = PubFun.calDate(tStartDate, -1, "M", null);
            lastDate = AgentPubFun.formatDate(lastDate, "yyyyMM");

            if (maxIndexCalNo == null || maxIndexCalNo.trim().equals("")) {
                CError tError = new CError();
                tError.moduleName = "ChangeBranchNewBL";
                tError.functionName = "check";
                tError.errorMessage = "管理机构" + mManageCom +
                                      "未计算过佣金，不能做调动!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (maxIndexCalNo.trim().equals(lastDate.trim())) {
                return true;
            } 
            else {
                CError tError = new CError();
                tError.moduleName = "ChangeBranchNewBL";
                tError.functionName = "check";
                tError.errorMessage = "上次发工资是" + maxIndexCalNo.substring(0, 4) +
                                      "年" +
                                      maxIndexCalNo.substring(4).trim() +
                                      "月，因此调整日期必须从这个月的下一个月1号";
                this.mErrors.addOneError(tError);
                return false;
           }
    }
    
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {    	
    	for (int i = 1; i <= mLABranchGroupSet.size(); i++) {
    		LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
            tLABranchGroupDB.setAgentGroup(mLABranchGroupSet.get(i).getAgentGroup());
            
            if (!tLABranchGroupDB.getInfo() ) {
                CError tError = new CError();
                tError.moduleName = "LAChangeBranch2011BL";
                tError.functionName = "dealdata";
                tError.errorMessage = "未找到机构编码为" + mLABranchGroupSet.get(i).getAgentGroup() +
                                      "的机构信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LABranchGroupSchema mAdjustLABranchSchema=new LABranchGroupSchema();
            mAdjustLABranchSchema=tLABranchGroupDB.getSchema();
            
            Reflections tReflections = new Reflections();
            LABranchGroupBSchema tLABranchGroupBSchema = new   LABranchGroupBSchema();
            tReflections.transFields(tLABranchGroupBSchema,mAdjustLABranchSchema);
            mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            tLABranchGroupBSchema.setEdorNo(mEdorNo);//
            tLABranchGroupBSchema.setEdorType("9");
            tLABranchGroupBSchema.setOperator2(mAdjustLABranchSchema.getOperator());
            tLABranchGroupBSchema.setMakeDate2(mAdjustLABranchSchema.getMakeDate());
            tLABranchGroupBSchema.setMakeTime2(mAdjustLABranchSchema.getMakeTime());
            tLABranchGroupBSchema.setModifyDate2(mAdjustLABranchSchema.getModifyDate());
            tLABranchGroupBSchema.setModifyTime2(mAdjustLABranchSchema.getModifyTime());
            tLABranchGroupBSchema.setMakeDate(currentDate);
            tLABranchGroupBSchema.setMakeTime(currentTime);
            tLABranchGroupBSchema.setModifyDate(currentDate);
            tLABranchGroupBSchema.setModifyTime(currentTime);
            tLABranchGroupBSchema.setOperator(mGlobalInput.Operator);
            mLABranchGroupBSet.add(tLABranchGroupBSchema);
            
            mAdjustLABranchSchema.setBranchAttr(mLABranchGroupSet.get(i).getBranchAttr());
            mAdjustLABranchSchema.setName(mLABranchGroupSet.get(i).getName());
            mAdjustLABranchSchema.setBranchSeries(mLABranchGroupSet.get(i).getAgentGroup());
            mAdjustLABranchSchema.setUpBranch("");
            mAdjustLABranchSchema.setBranchLevel("43");
            mAdjustLABranchSchema.setUpBranchAttr("0");
            mAdjustLABranchSchema.setCostCenter(mLABranchGroupSet.get(i).getCostCenter());
            mAdjustLABranchSchema.setModifyDate(currentDate);
            mAdjustLABranchSchema.setModifyTime(currentTime);
            mAimLABranchGroupSet.add(mAdjustLABranchSchema);
            
            if (!dealEachBranchGroup(mAdjustLABranchSchema, "1")) {
                return false;
            }
            
    	}
        
        return true;
    }


    private boolean dealEachBranchGroup(LABranchGroupSchema  cLABranchGroupSchema, String tFlag) {
        String curAgentGroup = cLABranchGroupSchema.getAgentGroup();
     
            LACommisionSet tLACommisionSet = new LACommisionSet();
            String aasql =
                    "select * from lacommision where branchcode = '" +
                    curAgentGroup +
                    "' and (caldate is null or caldate>='" + this.mAdjustDate + "') ";
            System.out.println(aasql);
            LACommisionDB tLACommisionDB = new LACommisionDB();
            tLACommisionSet = tLACommisionDB.executeQuery(aasql);
            for (int j = 1; j <= tLACommisionSet.size(); j++) {
                LACommisionSchema tLACommisionSchema = new LACommisionSchema();
                tLACommisionSchema = tLACommisionSet.get(j);
                tLACommisionSchema.setBranchAttr(cLABranchGroupSchema.getBranchAttr());
                tLACommisionSchema.setBranchSeries(cLABranchGroupSchema.getBranchSeries());
                tLACommisionSchema.setModifyDate(currentDate);
                tLACommisionSchema.setModifyTime(currentTime);
                this.mLACommisionSet.add(tLACommisionSchema);
            }
          
        return true;
    }



    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            this.mInputData = new VData();
            this.mMap.put(mAimLABranchGroupSet, "UPDATE");
            this.mMap.put(mLABranchGroupBSet, "INSERT");
            this.mMap.put(mLACommisionSet, "UPDATE");
            this.mInputData.add(this.mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ChangeBranchNewBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        mResult = new VData();
        mResult.add(NewBranchAttr);
        return this.mResult;
    }


}
