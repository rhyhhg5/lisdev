/*
 * <p>ClassName: LAGroupUniteUI </p>
 * <p>Description: LAGroupUniteUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-07-26 09:58:25
 */
package com.sinosoft.lis.agentbranch;

import java.sql.Connection;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.sql.*;


public class LAGroupSplitBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局常量 */
    private int cAgentMaxCount = 15;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();
    private LATreeSet mLATreeSet = new LATreeSet();

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
            //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        //进行数据验证
        if(!check())
        {
            System.out.println("对合并机构的验证没有通过!");
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupSpliteBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAGroupSpliteBL-->dealData!";
            System.out.println("数据处理失败LAGroupSpliteBL-->dealData!");
            this.mErrors.addOneError(tError);
            return false;
        }

        // 准备传往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        return true;
    }

    /**
     * 进行业务处理
     * @return boolean
     */
    private boolean dealData()
    {
        //目标机构
        LABranchGroupSchema tLABranchGroupSchemaAim = new LABranchGroupSchema();
        //新建机构
        LABranchGroupSchema tLABranchGroupSchemaNew = new LABranchGroupSchema();

        //得到目标机构和新建机构信息
        tLABranchGroupSchemaAim.setSchema(mLABranchGroupSet.get(1).getSchema());
        tLABranchGroupSchemaNew.setSchema(mLABranchGroupSet.get(2).getSchema());
        System.out.println("目标机构:"+tLABranchGroupSchemaAim.getBranchAttr());
        System.out.println("分离出的新机构:"+tLABranchGroupSchemaNew.getBranchAttr());

        //建立新机构
        System.out.println("开始进行新单位的建立！");
        if(!createBranchGroupNew(tLABranchGroupSchemaNew))
        {
            return false;
        }

        //获得新建结构的详细信息
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = getGroupByBranchAttr(tLABranchGroupSchemaNew.getBranchAttr());

        System.out.println("开始进行人员调动！");
        //进行人员调动
        if(!AdjustAgent(tLABranchGroupSchema,mLATreeSet))
        {
            return false;
        }

        //进行拆分轨迹的记录
        System.out.println("开始进行轨迹记录!");
        if(!dealSplitContrail(tLABranchGroupSchema,
                              tLABranchGroupSchemaAim.getBranchAttr()))
        {
            return false;
        }

        return true;
    }

    /**
     * 进行单位拆分的轨迹记录
     * @param pmLABranchGroupSchema LABranchGroupSchema
     * @param pmLABranchAttrA String
     * @return boolean
     */
    private boolean dealSplitContrail(LABranchGroupSchema pmLABranchGroupSchema,
                                       String pmLABranchAttrA)
    {
        Reflections tReflections = new Reflections();
        LABranchGroupBSchema tLABranchGroupBSchema = new LABranchGroupBSchema();
        boolean tReturn = true;

        //批改号
        String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        //操作日期
        String tMakeDate = PubFun.getCurrentDate();
        //操作时间
        String tMakeTime = PubFun.getCurrentTime();

        //把团队表中相同的部分复制到备份表里
        tReflections.transFields(tLABranchGroupBSchema,pmLABranchGroupSchema);
        System.out.println(tLABranchGroupBSchema.getBranchAttr());
        System.out.println(tLABranchGroupBSchema.getAgentGroup());
        //设置备份信息
        tLABranchGroupBSchema.setEdorNo(tEdorNo);                   // 批改号
        tLABranchGroupBSchema.setEdorType("08");                    // 转储类型（拆分）
        tLABranchGroupBSchema.setOperator2(pmLABranchGroupSchema.getOperator());  // 原操作员
        tLABranchGroupBSchema.setMakeDate2(pmLABranchGroupSchema.getMakeDate());  // 原入机日期
        tLABranchGroupBSchema.setMakeTime2(pmLABranchGroupSchema.getMakeTime());  // 原入机时间
        tLABranchGroupBSchema.setModifyDate2(pmLABranchGroupSchema.getModifyDate());  // 原入机日期
        tLABranchGroupBSchema.setModifyTime2(pmLABranchGroupSchema.getModifyTime());  // 原入机时间
        tLABranchGroupBSchema.setOperator(mGlobalInput.Operator);  // 操作员
        tLABranchGroupBSchema.setMakeDate(tMakeDate);              // 入机日期
        tLABranchGroupBSchema.setMakeTime(tMakeTime);              // 入机时间
        tLABranchGroupBSchema.setModifyDate(tMakeDate);            // 修改时间
        tLABranchGroupBSchema.setUpBranch(pmLABranchAttrA);        // 被分离的机构

        //向数据库插入备份数据
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        if (conn == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupSplitBL";
            tError.functionName = "dealSplitContrail";
            tError.errorMessage = "数据库连接失败!";
            System.out.println("数据库连接失败!");
            this.mErrors.addOneError(tError);
            return false;
        }

        try
        {
            conn.setAutoCommit(false);
            //机构备份数据
            LABranchGroupBDB tLABranchGroupBDB = new LABranchGroupBDB(conn);
            tLABranchGroupBDB.setSchema(tLABranchGroupBSchema);
            //进行数据库操作
            if(!tLABranchGroupBDB.insert())
            {
                    // @@错误处理
                this.mErrors.copyAllErrors(tLABranchGroupBDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAGroupSplitBL";
                tError.functionName = "dealSplitContrail";
                tError.errorMessage = "插入管理机构备份数据信息失败!";
                this.mErrors.addOneError(tError);
                System.out.println("插入管理机构备份失败  "+tError.toString());
                conn.rollback();
                conn.close();

                tReturn = false;
            }
            //提交修改的数据
            conn.commit();
            //断开数据库连接
            conn.close();
        }
        catch(Exception ex)
        {// @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupSplitBL";
            tError.functionName = "dealSplitContrail";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }

        return tReturn;
    }

    /**
     * 把指定业务员向指定机构作人员调动
     * @param pmBranchAttr String
     * @param pmLATreeSet LATreeSet
     * @return boolean
     */
    private boolean AdjustAgent(LABranchGroupSchema pmLABranchGroupSchemaNew,
                                LATreeSet pmLATreeSet)
    {
        System.out.println("调动目标机构："+pmLABranchGroupSchemaNew.getBranchAttr());
        //输入参数
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        //LATreeSet tLATreeSet = new LATreeSet();
        AdjustAgentUI tAdjustAgentUI = new AdjustAgentUI();
        //错误信息
        String Content = "";
        String FlagStr = "";
        CErrors tError = null;

        //判断调动人员人数
        if(pmLATreeSet==null || pmLATreeSet.size()==0)
        {
            return true;
        }

//        tLABranchGroupSchema = getGroupByBranchAttr(
//            pmLABranchGroupSchemaNew.getBranchAttr());
        tLABranchGroupSchema.setSchema(pmLABranchGroupSchemaNew);
        if(tLABranchGroupSchema == null)
        {
            // @@错误处理
            CError ttError = new CError();
            ttError.moduleName = "LAGroupSplitBL";
            ttError.functionName = "AdjustAgent";
            ttError.errorMessage = "调动人员时失败，原因是:目标机构不存在!";
            System.out.println("调动人员时失败，原因是:目标机构不存在!");
            this.mErrors.addOneError(ttError);

            return false;
        }

        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.addElement(tLABranchGroupSchema);
        tVData.addElement(pmLATreeSet);

        try
        {
            tAdjustAgentUI.submitData(tVData, "INSERT||MAIN");
        }
        catch(Exception ex)
        {
            FlagStr = "Fail";
            //Content = "调动人员时失败，原因是:" + ex.toString();
            // @@错误处理
            CError ttError = new CError();
            ttError.moduleName = "LAGroupSplitBL";
            ttError.functionName = "AdjustAgent";
            ttError.errorMessage = "调动人员时失败，原因是:" + ex.toString();
            System.out.println("调动人员时失败，原因是:" + ex.toString());
            this.mErrors.addOneError(ttError);

            return false;
        }

        tError = tAdjustAgentUI.mErrors;
        if(tError.needDealError())
        {
            mErrors.copyAllErrors(tError);
            System.out.println("错误原因："+tError.getFirstError());
            return false;
        }

        return true;
    }

    /**
     * 根据传入的机构外部编码，查询指定机构信息
     * @param pmBranchAttr String
     * @return LABranchGroupSchema
     */
    private LABranchGroupSchema getGroupByBranchAttr(String pmBranchAttr) {
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();

        tLABranchGroupDB.setBranchAttr(pmBranchAttr);
        tLABranchGroupDB.setBranchType("2");
        tLABranchGroupDB.setBranchType2("01");
        tLABranchGroupSet = tLABranchGroupDB.query();
        //访问数据库失败
        if (tLABranchGroupDB.mErrors.needDealError()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupSplitBL";
            tError.functionName = "getGroupByBranchAttr";
            tError.errorMessage = "查询[" + pmBranchAttr + "]机构信息失败！";
            this.mErrors.addOneError(tError);

            return null;
        }
        //查询失败
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupSplitBL";
            tError.functionName = "getGroupByBranchAttr";
            tError.errorMessage = "查询[" + pmBranchAttr + "]机构信息失败！";
            this.mErrors.addOneError(tError);

            return null;
        }

        return tLABranchGroupSet.get(1);
    }

    /**
     * 创建新机构
     * @param pmLABranchGroupSchema LABranchGroupSchema
     * @return boolean
     */
    private boolean createBranchGroupNew(LABranchGroupSchema pmLABranchGroupSchema)
    {
        //新建机构类
        ALABranchGroupUI tALABranchGroupUI = new ALABranchGroupUI();
        //错误处理
        CErrors ttError = null;

        //准备数据
        VData tVData = new VData();
        tVData.addElement(pmLABranchGroupSchema);
        tVData.add(mGlobalInput);

        try
        {
            tALABranchGroupUI.submitData(tVData, "INSERT||MAIN");
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupSpliteBL";
            tError.functionName = "dealData";
            tError.errorMessage = "新建机构失败!原因是:" + ex.toString();
            this.mErrors.addOneError(tError);

            return false;
        }

        //判断是否有错误需要处理
        ttError = tALABranchGroupUI.mErrors;
        if (ttError.needDealError())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupSpliteBL";
            tError.functionName = "s";
            tError.errorMessage = "新建机构失败!原因是:" + ttError.getFirstError();
            System.out.println("新建机构失败!原因是:" + ttError.getFirstError());
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 准备传往后台的数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        return true;
    }

    /**
     * 进行数据验证
     * @return boolean
     */
    private boolean check()
    {
        if (mLATreeSet == null || mLATreeSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "LAGroupSplitBL";
            tError.functionName = "check";
            tError.errorMessage = "请选择需要调整的人员!";
            System.out.println("请选择需要调整的人员!");
            this.mErrors.addOneError(tError);
            return false;
        }

        // ↓ *** 2005-11-16 *** add *** 追加机构规模验证 *** LiuHao ***
        // 验证人员规模  根据基本法 业务系列人员超过15人才允许拆分
        String tSQL = "select count(*) from LAAgent b,LABranchGroup c";
        tSQL += " where c.BranchAttr = '" + this.mLABranchGroupSet.get(1).getBranchAttr() + "'";
        tSQL += "   and b.AgentCode <> c.BranchManager";
        tSQL += "   and c.AgentGroup = b.AgentGroup";
        tSQL += "   and (b.AgentState is null or b.AgentState < '03')";
        tSQL += "   and (c.state<>'1' or c.state is null)";
        tSQL += "   and b.branchtype='2' and b.BranchType2='01'";
        if(cAgentMaxCount > execQuery(tSQL))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupUniteBL";
            tError.functionName = "check";
            tError.errorMessage = "被拆分的机构业务员不足15人！";
            System.out.println("被拆分的机构业务员不足15人！");
            this.mErrors.addOneError(tError);

            return false;
        }

        // 验证保费规模  根据基本法 标准保费规模是否达标
        //                      A类地区 大于2400万
        //                      B类地区 大于2000万
        //                      C类地区 大于1500万
        //                      D类地区 大于1200万
        //  暂时未验证
        // ↑ *** 2005-11-16 *** add *** 追加机构规模验证 *** LiuHao ***

        //机构成立日期（人员调动日期）必须是01号
        if(!"01".equals(mLABranchGroupSet.get(2).getFoundDate().substring(8,10)))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupUniteBL";
            tError.functionName = "check";
            tError.errorMessage = "机构成立日期（人员调动日期）必须是01号！";
            System.out.println("机构成立日期（人员调动日期）必须是01号！");
            this.mErrors.addOneError(tError);

            return false;
        }
System.out.println("开始检验佣金计算时间");
        for(int i=1;i<=mLATreeSet.size();i++){
            //检验佣金计算日期
            String sql = "select max(IndexCalNo) from lawage where agentcode='" +
                         mLATreeSet.get(i).getAgentCode() + "'";
            ExeSQL tExeSQL = new ExeSQL();
            String MaxIndexCalNo = tExeSQL.getOneValue(sql);
            if (MaxIndexCalNo!=null && !"".equals(MaxIndexCalNo) )
            {
                String WageCalDate = MaxIndexCalNo;
                WageCalDate += "01";
                WageCalDate = AgentPubFun.formatDate(WageCalDate, "yyyy-MM-dd");
                WageCalDate = PubFun.calDate(WageCalDate, 1, "M", "");
                if (!WageCalDate.equals(mLATreeSet.get(i).getAstartDate())) {
                    CError tError = new CError();
                    tError.moduleName = "AdjustAgentBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "业务员" +
                                          mLATreeSet.get(i).getAstartDate() +
                                          "最近一次计算佣金是" + MaxIndexCalNo +
                                          ",因此调动日期必须为" + WageCalDate + "";
                    this.mErrors.addOneError(tError);

                    return false;
                    //continue;
                }
            }
        }

        return true;
    }

    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return double
     */
    private int execQuery(String sql)
    {
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null)return 0;
            st = conn.prepareStatement(sql);
            if (st == null)return 0;
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
      if (!conn.isClosed()) {
          conn.close();
      }
      try {
          st.close();
          rs.close();
      } catch (Exception ex2) {
          ex2.printStackTrace();
      }
      st = null;
      rs = null;
      conn = null;
    } catch (Exception e) {}

        }
    }


    /**
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                               "GlobalInput", 0));
        //机构
        mLABranchGroupSet.set((LABranchGroupSet)cInputData.getObjectByObjectName(
                              "LABranchGroupSet",0));
        //调动人员
        mLATreeSet.set((LATreeSet)cInputData.getObjectByObjectName(
                       "LATreeSet",0));

        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupUniteBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


}
