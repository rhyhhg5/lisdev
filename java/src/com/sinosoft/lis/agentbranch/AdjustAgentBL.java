/*
 * <p>ClassName: AdjustAgentBL </p>
 * <p>Description: AdjustAgentBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-27
 */
package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.agent.LARecomPICCHBL;
import com.sinosoft.lis.agent.LARearPICCHBL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.lis.schema.LARecomRelationSchema;
import com.sinosoft.lis.db.LARecomRelationDB;

public class AdjustAgentBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String mOperate;
    private String tEdorNo;
    private String mAimManageCom;
    private String mOldManageCom;
    private String mOldAgentGroup;
    private String mAStartDate;
    private boolean IsSingle = true;

    /** 业务处理相关变量 */
    private LABranchGroupSchema mLABranchSchema = new LABranchGroupSchema();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LACommisionSet mLACommisionSet = new LACommisionSet();

    //更新所有调动人员和管理人员在行政表中的AgentGroup UpBranch
    private LATreeSet mUpdateLATreeSet = new LATreeSet();

    //备份所有调动人员和管理人员
    private LATreeBSet mLATreeBSet = new LATreeBSet();

    //更新所有调动人员在LAAgent表中的AgentGroup
    private LAAgentSet mLAAgentSet = new LAAgentSet();

    //备份代理人记录 add by jiangcx minLABranchGroupBSet
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();
    //备份代理人记录 labranchgroupb表中的branchmanager
    private LABranchGroupBSet minLABranchGroupBSet = new LABranchGroupBSet();

    private LARecomRelationSet mLARecomRelationSet = new LARecomRelationSet();
    //更新所有调动人员
    private LABranchGroupSet mupLABranchGroupSet = new LABranchGroupSet();
    private MMap mMap = new MMap();

    private VData mOutputDate = new VData();

    public AdjustAgentBL() {
    }

    public static void main(String[] args) {
        LABranchGroupSchema mLABranchSchema = new LABranchGroupSchema();
        LATreeSchema mManagerTreeSchema = new LATreeSchema();
        LATreeSet mLATreeSet = new LATreeSet();
        AdjustAgentUI mAdjustAgentUI = new AdjustAgentUI();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "mak001";
        tG.ComCode = "86";
        tG.ManageCom = "86";
        mLABranchSchema.setAgentGroup("000000000671");
        mLABranchSchema.setBranchAttr("86110000080101");
        mLABranchSchema.setBranchLevel("01");
        mLABranchSchema.setBranchManager("1101000408");
        mLABranchSchema.setUpBranch("");
        mLABranchSchema.setBranchType("1");
        mLABranchSchema.setBranchType2("01");

        LATreeSchema tLATreeSchema = new LATreeSchema();
        tLATreeSchema.setAgentCode("1101000583");
        tLATreeSchema.setAgentGrade("A02"); //代理人职级
        tLATreeSchema.setAgentGroup("000000000160");
        tLATreeSchema.setBranchType("1");
        tLATreeSchema.setBranchType2("01");
        tLATreeSchema.setAstartDate("2007-01-01");
        mLATreeSet.add(tLATreeSchema);

        TransferData tdata = new TransferData();
        tdata.setNameAndValue("IsSingle", "1");
        VData tVData = new VData();
        tVData.add(tG);
        tVData.addElement(mLABranchSchema);
        tVData.addElement(mLATreeSet);
        tVData.addElement(tdata);
        mAdjustAgentUI.submitData(tVData, "INSERT||MAIN");
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "AdjustAgentBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 校验的代码
     * @return
     */
    private boolean check() {
        if (mLATreeSet == null || mLATreeSet.size() == 0) {
            CError tError = new CError();
            tError.moduleName = "AdjustAgentBL";
            tError.functionName = "check";
            tError.errorMessage = "请选择需要调整的人员!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //查询目的机构是否存在
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setBranchAttr(this.mLABranchSchema.getBranchAttr());
        tLABranchGroupDB.setBranchType(this.mLABranchSchema.getBranchType());
        tLABranchGroupDB.setBranchType2(this.mLABranchSchema.getBranchType2());
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupSet = tLABranchGroupDB.query();
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
            CError tError = new CError();
            tError.moduleName = "AdjustAgentBL";
            tError.functionName = "check";
            tError.errorMessage = "没有查询到" + this.mLABranchSchema.getBranchAttr() +
                                  "的机构信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        tLABranchGroupSchema = tLABranchGroupSet.get(1);
        mLABranchSchema.setAgentGroup(tLABranchGroupSchema.getAgentGroup());//mLABranchSchema.setAgentgroup 存的是目标机构agentgroup
        System.out.println(mLABranchSchema.getAgentGroup());
        if (tLABranchGroupSchema.getEndFlag() != null && tLABranchGroupSchema.
            getEndFlag().equals("Y")) {
            CError tError = new CError();
            tError.moduleName = "AdjustAgentBL";
            tError.functionName = "check";
            tError.errorMessage = "机构" + this.mLABranchSchema.getBranchAttr() +
                                  "已经停业，不能继续调入人员!";
            this.mErrors.addOneError(tError);
            return false;

        }
        //团险团队合并不需要校验这个
        if(!tLABranchGroupSchema.getBranchType().equals("2")&&!tLABranchGroupSchema.getBranchType2().equals("01"))
        {
            if (!tLABranchGroupSchema.getBranchLevel().equals("01"))
               {
                  buildError("check","目标机构必须是处级机构！");
                  return false;
                }
        }

        if(!tLABranchGroupSchema.getManageCom().equals(mOldManageCom))
        {
            buildError("check","人员不能跨管理机构调动！");
            return false;
        }
        if(tLABranchGroupSchema.getAgentGroup().equals(mOldAgentGroup))
        {
           buildError("check","人员不能在本机构内调动！");
           return false;
        }


        //校验调动日期
        String tStartDate = this.mLATreeSet.get(1).getAstartDate();// 目标人员调动生效日期
        System.out.println("tStartDate:" + tStartDate);
        String AgentCode = this.mLATreeSet.get(1).getAgentCode();
        System.out.println("AgentCode:" + AgentCode);
        String newStartDate = AgentPubFun.formatDate(tStartDate, "yyyy-MM-dd");//转换成日期格式
        System.out.println("newStartDate:" + newStartDate);
        String tDay = newStartDate.substring(newStartDate.lastIndexOf("-") + 1);//获得天数
        if (!tDay.equals("01")) {
            CError tError = new CError();
            tError.moduleName = "AdjustAgentBL";
            tError.functionName = "check";
            tError.errorMessage = "调整日期必须是从某个月的1号开始!";
            this.mErrors.addOneError(tError);
            return false;
        } else {
            PubCheckField checkField1 = new PubCheckField();
            VData cInputData = new VData();
            TransferData tTransferData = new TransferData();

            String oldAgentGroup = mLATreeSet.get(1).getAgentGroup();
            String NewAgentGroup = tLABranchGroupSchema.getAgentGroup();

            //通过 CKBYSET 方式校验
            LMCheckFieldDB tLMCheckFieldDB = new LMCheckFieldDB();
            LMCheckFieldSet tLMCheckFieldSet = new LMCheckFieldSet();
            String tSql =
                    "select * from lmcheckfield where riskcode = '000000'"
                    + " and fieldname = 'AdjustAgentBL' order by serialno asc";
            System.out.println("tSql is " + tSql);
            tLMCheckFieldSet = tLMCheckFieldDB.executeQuery(tSql);
            cInputData.add(tTransferData);
            cInputData.add(tLMCheckFieldSet);

            if (!checkField1.submitData(cInputData, "CKBYSET")) {
                System.out.println("Enter Error Field!");
                //此判断是用来区分是程序处理过程中报的错误，还是校验时报的错误

                if (checkField1.mErrors.needDealError()) { //程序错误处理

                    this.mErrors.copyAllErrors(checkField1.mErrors);
                    return false;
                } else {
                    VData t = checkField1.getResultMess(); //校验错误处理
                    buildError("dealData", t.get(0).toString());
                    return false;
                }
            }

        }
        return true;
    }

    private boolean getSubTree(){
        if(!IsSingle){
            LATreeSchema rLATreeSchema = mLATreeSet.get(1);
            String tAgentGroup = rLATreeSchema.getAgentGroup();
            String tAgentCode = rLATreeSchema.getAgentCode();
            String AdjustDate = rLATreeSchema.getAstartDate();
            String BranchType = rLATreeSchema.getBranchType();
            String BranchType2 = rLATreeSchema.getBranchType2();
            String tSql = "select agentcode,agentgrade,agentgroup from larecomrelation "
                          +" where recomagentcode = '"+tAgentCode
                          +"' and recomflag='1'";
            ExeSQL texesql = new ExeSQL();
            SSRS tss = texesql.execSQL(tSql);
            for(int i=1;i<=tss.getMaxRow();i++){
                if(!tAgentGroup.equals(tss.GetText(i,3))){
                    CError.buildErr(this,"被推荐人"+tss.GetText(i,1)+"属于另一机构");
                    continue;
                    //return false;
                }
                LATreeSchema tlatree = new LATreeSchema();
                tlatree.setAgentCode(tss.GetText(i,1));
                tlatree.setAgentGrade(tss.GetText(i,2));
                tlatree.setAstartDate(AdjustDate);
                tlatree.setBranchType(BranchType);
                tlatree.setBranchType2(BranchType2);
                mLATreeSet.add(tlatree);
            }
        }
        return true;
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!check()) {
            return false;
        }
//        if(!getSubTree())
//            return false;
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustAgentBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败AdjustAgentBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
//        if(!chgRelation()){
//            CError.buildErr(this,"处理人员推荐育成关系失败");
//            return false;
//        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mOutputDate, "");
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "AdjustAgentBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("进入dealData处理");
        boolean tReturn = true;
        int i = 0;
        LABranchGroupDB tLABranchDB = new LABranchGroupDB();
        String[] newAgentcode = null;
        System.out.println("start to dealData...");
        tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        System.out.println("批改号：" + tEdorNo);
        //准备更新机构表中的管理人员的纪录

        tLABranchDB.setAgentGroup(this.mLABranchSchema.getAgentGroup()); //隐式
        if (!tLABranchDB.getInfo()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLABranchDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "AdjustAgentBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询目标单位信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLABranchSchema = tLABranchDB.getSchema();

        //准备更新所有调动人员和管理人员在行政表中的AgentGroup和UpAgent的纪录
        String tAimBranch = this.mLABranchSchema.getAgentGroup();
        String tUpAgent = this.mLABranchSchema.getBranchManager();
        mAimManageCom = this.mLABranchSchema.getManageCom();// 查询到目标团队机构 managecom 字段
        System.out.println("UpAgent:" + tUpAgent + " --不管目标机构是否有管理人员");
        tUpAgent = tUpAgent == null ? "" : tUpAgent;

        LATreeDB tLATreeDB = null;
        LAAgentDB tLAAgentDB = null;

        int tCount = this.mLATreeSet.size();
        System.out.println("调动人数：" + tCount);
        String aimManageCom = this.mAimManageCom;
        for (i = 1; i <= tCount; i++) 
        {
            tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(this.mLATreeSet.get(i).getAgentCode()); //jiangcx add for BK 主键
            if (!tLAAgentDB.getInfo()) 
            {
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "AdjustAgentBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询业务员" + mLATreeSet.get(i).getAgentCode() +"的基础信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println(tLABranchDB.getManageCom() + "...." +
                               tLAAgentDB.getManageCom() + "......." +
                               this.mLABranchSchema.getBranchType());
         // tLATreeSchema 存的是mLATreeSet 中的内容 涉及:agentcode agentgroup astartdate agentgrade
            LATreeSchema tLATreeSchema = this.mLATreeSet.get(i);
            tLATreeDB = new LATreeDB();
            tLATreeDB.setAgentCode(this.mLATreeSet.get(i).getAgentCode());
            System.out.println("i=" + i + "  " + tLATreeDB.getAgentCode());
            if (!tLATreeDB.getInfo()) 
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "AdjustAgentBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询目标单位信息失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LATreeSchema bLATreeSchema = new LATreeSchema();
            LATreeBSchema tLATreeBSchema = new LATreeBSchema();
            bLATreeSchema = tLATreeDB.getSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLATreeBSchema, bLATreeSchema);
            tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
            tLATreeBSchema.setOperator(mGlobalInput.Operator);
            tLATreeBSchema.setEdorNO(tEdorNo);
            tLATreeBSchema.setRemoveType("03");// 人员调动
            tLATreeBSchema.setMakeDate2(tLATreeBSchema.getMakeDate());
            tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
            tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
            tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
            tLATreeBSchema.setMakeDate(currentDate);
            tLATreeBSchema.setMakeTime(currentTime);
            tLATreeBSchema.setModifyDate(currentDate);
            tLATreeBSchema.setModifyTime(currentTime);
            this.mLATreeBSet.add(tLATreeBSchema);

            //这个比较已比较合理 ******
            String sql = "select max(wageno) from lawagehistory where managecom='" +
                         tLATreeDB.getManageCom() + "' and branchtype = '"+tLATreeDB.getBranchType()+"' " +
                         "and branchtype2 = '"+tLATreeDB.getBranchType2()+"' and state = '14'";
            ExeSQL tExeSQL = new ExeSQL();
            String MaxIndexCalNo = tExeSQL.getOneValue(sql);//最大薪资月
            String AdjustDate = "";
            if (MaxIndexCalNo != null && !MaxIndexCalNo.equals("")) {
                String WageCalDate = MaxIndexCalNo;
                WageCalDate += "01";
                WageCalDate = AgentPubFun.formatDate(WageCalDate, "yyyy-MM-dd");//得到最大薪资月1号 标准日期格式
                WageCalDate = PubFun.calDate(WageCalDate, 1, "M", "");//最大薪资月下个月 1 日
                if (!WageCalDate.equals(tLATreeSchema.getAstartDate())) 
                {
                    CError tError = new CError();
                    tError.moduleName = "AdjustAgentBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "机构" + tLATreeSchema.getManageCom() +
                                          "最近一次计算佣金是" + MaxIndexCalNo +
                                          ",因此调动日期必须为" + WageCalDate + "";
                    this.mErrors.addOneError(tError);
                    continue;
                } else {
                    AdjustDate = WageCalDate;
                }
            } else {
                AdjustDate = tLAAgentDB.getEmployDate();//未算过薪资业务员调动日期为入司时间
            }
            //设置上级代理人
            bLATreeSchema.setUpAgent(tUpAgent);// tUpAgent 存的是目标团队 主管编码
            bLATreeSchema.setAgentGroup(tAimBranch); //设置所属管理机构 目标团队隐式主键
            System.out.println("tAimBranchtAimBranch:" + tAimBranch);
            bLATreeSchema.setManageCom(mLABranchSchema.getManageCom());
            bLATreeSchema.setOperator(this.mGlobalInput.Operator);
            bLATreeSchema.setManageCom(aimManageCom);
            bLATreeSchema.setModifyDate(currentDate);
            bLATreeSchema.setModifyTime(currentTime);
            bLATreeSchema.setBranchCode(tAimBranch);// 目标团队branchcode
            //bLATreeSchema.setAstartDate(AdjustDate); //调整日期  *** 人员团队调整不涉及变动astartdate
            
            LAAgentSchema tLAAgentSchema = new LAAgentSchema();
            tLAAgentSchema = tLAAgentDB.getSchema();
            LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
            tReflections = new Reflections();
            tReflections.transFields(tLAAgentBSchema, tLAAgentSchema);
            tLAAgentBSchema.setMakeDate(currentDate);
            tLAAgentBSchema.setMakeTime(currentTime);
            tLAAgentBSchema.setOperator(mGlobalInput.Operator);
            tLAAgentBSchema.setEdorNo(tEdorNo);
            tLAAgentBSchema.setEdorType("03");//备份laagent表 edortype = 03
            this.mLAAgentBSet.add(tLAAgentBSchema);
            tLAAgentSchema.setModifyDate(currentDate);
            tLAAgentSchema.setModifyTime(currentTime);
            tLAAgentSchema.setAgentGroup(tAimBranch);
            tLAAgentSchema.setBranchCode(tAimBranch);
            tLAAgentSchema.setOperator(mGlobalInput.Operator);
            tLAAgentSchema.setManageCom(mLABranchSchema.getManageCom());
            //处理团队建设
            if(!dealAgentGbuild(tLAAgentSchema,bLATreeSchema,tAimBranch,AdjustDate)) return false;
            this.mUpdateLATreeSet.add(bLATreeSchema);
            mLAAgentSet.add(tLAAgentSchema);

            //主管调动的处理,调动个险B01的主管,调动后职级不变,但不再是主管  2006-02-21添加  xiangchun
            LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setBranchManager(this.mLATreeSet.get(i).getAgentCode());
            System.out.println(this.mLATreeSet.get(i).getAgentCode());
            tLABranchGroupSet = tLABranchGroupDB.query();
            for (int k = 1; k <= tLABranchGroupSet.size(); k++) {
                LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
                LABranchGroupBSchema tLABranchGroupBSchema = new LABranchGroupBSchema();
                tLABranchGroupSchema = tLABranchGroupSet.get(k);
                //备份labranchgroupb表
                tReflections = new Reflections();
                tReflections.transFields(tLABranchGroupBSchema,tLABranchGroupSchema);
                System.out.println(tLABranchGroupBSchema.getAgentGroup() +
                                   "|||||" + tLABranchGroupSet.size() + "||" +
                                   k);
                //String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
                tLABranchGroupBSchema.setEdorType("07"); //修改操作
                tLABranchGroupBSchema.setEdorNo(tEdorNo);
                tLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.
                                                   getMakeDate());
                tLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.
                                                   getMakeTime());
                tLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.
                                                     getModifyDate());
                tLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.
                                                     getModifyTime());
                tLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.
                                                   getOperator());
                tLABranchGroupBSchema.setMakeDate(currentDate);
                tLABranchGroupBSchema.setMakeTime(currentTime);
                tLABranchGroupBSchema.setModifyDate(currentDate);
                tLABranchGroupBSchema.setModifyTime(currentTime);
                tLABranchGroupBSchema.setOperator(this.mGlobalInput.Operator);
                minLABranchGroupBSet.add(tLABranchGroupBSchema);
                //修改labranchgroup 中的主管为空
                tLABranchGroupSchema.setBranchManager("");
                tLABranchGroupSchema.setBranchManagerName("");
                //调整日期
                String FoundDate = AgentPubFun.formatDate(tLABranchGroupSchema.
                        getFoundDate(), "yyyy-MM-dd");
                if (FoundDate.compareTo(mAStartDate) > 0) {
                    tLABranchGroupSchema.setAStartDate(FoundDate);
                } else {
                    tLABranchGroupSchema.setAStartDate(mAStartDate);
                }
                tLABranchGroupSchema.setModifyDate(currentDate);
                tLABranchGroupSchema.setModifyTime(currentTime);
                tLABranchGroupSchema.setOperator(mGlobalInput.Operator);
                mupLABranchGroupSet.add(tLABranchGroupSchema);

            }
            
            sql = "select * from lacommision where agentcode='" +
                  tLAAgentSchema.getAgentCode() + "' and (caldate>='" +
                  AdjustDate + "' or caldate is null)";
            LACommisionSet tLACommisionSet = new LACommisionSet();
            LACommisionDB tLACommisionDB = new LACommisionDB();
            tLACommisionSet = tLACommisionDB.executeQuery(sql);
            for (int j = 1; j <= tLACommisionSet.size(); j++) {
                LACommisionSchema tLACommisionSchema = new LACommisionSchema();
                tLACommisionSchema = tLACommisionSet.get(j);
                tLACommisionSchema.setAgentGroup(mLABranchSchema.getAgentGroup());
                tLACommisionSchema.setBranchCode(mLABranchSchema.getAgentGroup());
                tLACommisionSchema.setBranchSeries(mLABranchSchema.getBranchSeries());
                tLACommisionSchema.setBranchAttr(mLABranchSchema.getBranchAttr());
                tLACommisionSchema.setModifyDate(currentDate);
                tLACommisionSchema.setModifyTime(currentTime);
                tLACommisionSchema.setOperator(mGlobalInput.Operator);
                this.mLACommisionSet.add(tLACommisionSchema);
            }
            
            
            if (!setContToGroupNew(this.mLATreeSet.get(i).getAgentCode(),mLABranchSchema))
            {
                CError.buildErr(this,"修改代理人" +mLATreeSet.get(i).getAgentCode() +"的业绩信息失败！");
                return false;
            }

            //修改推荐关系的 agentgroup 育成关系不需要相关调整
            sql = "select * from laRecomRelation where agentcode='" +
                  tLAAgentSchema.getAgentCode() + "'  ";
            System.out.println("123234342323"+sql);
            LARecomRelationSet tLARecomRelationSet = new LARecomRelationSet();
            LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();
            tLARecomRelationSet = tLARecomRelationDB.executeQuery(sql);
            for (int kk = 1; kk <= tLARecomRelationSet.size(); kk++) {
                LARecomRelationSchema tLARecomRelationSchema =
                        tLARecomRelationSet.
                        get(kk);
                tLARecomRelationSchema.setAgentGroup(mLABranchSchema.
                                                     getAgentGroup());
                tLARecomRelationSchema.setModifyDate(currentDate);
                tLARecomRelationSchema.setModifyTime(currentTime);
                tLARecomRelationSchema.setOperator(mGlobalInput.Operator);
                mLARecomRelationSet.add(tLARecomRelationSchema);
            }
        }
        if (this.mErrors.needDealError())
        {
            tReturn = false;
        } else 
        {
            tReturn = true;
        }
        return tReturn;
    }

    private boolean chgRelation(){
        LARecomPICCHBL tLARecomPICCHBL = new LARecomPICCHBL();
        LATreeSchema parent = mLATreeSet.get(1);
        parent.setAgentGroup(mLABranchSchema.getAgentGroup());
        tLARecomPICCHBL.setLATreeSchema(parent);
        if (!tLARecomPICCHBL.invalidateRecom(IsSingle,mAStartDate))
        {
            mErrors.copyAllErrors(tLARecomPICCHBL.mErrors);
            return false;
        }
        MMap tMap1 = tLARecomPICCHBL.getResult();
        mMap.add(tMap1);
        LARearPICCHBL tLARearPICCHBL = new LARearPICCHBL();
        tLARearPICCHBL.setLATreeSchema(parent);
        if (!tLARearPICCHBL.invalidateRear(true,mAStartDate))
        {
            mErrors.copyAllErrors(tLARearPICCHBL.mErrors);
            return false;
        }
        MMap tMap2 = tLARearPICCHBL.getResult();
        mMap.add(tMap2);
        //失效与推荐人的关系

        return true;
    }
    
    public boolean setContToGroupNew(String cAgentCode,LABranchGroupSchema cLABranchGroupSchema)
    {
        String tAgentGroup=cLABranchGroupSchema.getAgentGroup();
        String tSql = "update lccont  set  agentgroup='" + tAgentGroup+
                      "',ModifyTime = '" + currentTime +
                      "',ModifyDate = '" + currentDate +
                      "',operator='"+mGlobalInput.Operator+"'  where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        tSql = "update lbcont  set  agentgroup='" + tAgentGroup+
        "',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"'  where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        tSql = "update lpcont  set  agentgroup='" + tAgentGroup+
        "',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"'  where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        tSql = "update lcpol  set  agentgroup='" + tAgentGroup+
               "',ModifyTime = '" + currentTime +
               "',ModifyDate = '" + currentDate +
               "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        tSql = "update lbpol  set  agentgroup='" + tAgentGroup+
        "',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        tSql = "update lppol  set  agentgroup='" + tAgentGroup+
        "',ModifyTime = '" + currentTime +
        "',ModifyDate = '" + currentDate +
        "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        tSql = "update lcgrpcont  set  agentgroup='" + tAgentGroup+
                     "',ModifyTime = '" + currentTime +
                     "',ModifyDate = '" + currentDate +
                     "',operator='"+mGlobalInput.Operator+"'  where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
       tSql = "update lbgrpcont  set  agentgroup='" + tAgentGroup+
       "',ModifyTime = '" + currentTime +
       "',ModifyDate = '" + currentDate +
       "',operator='"+mGlobalInput.Operator+"'  where agentcode='"+cAgentCode+"'";
       mMap.put(tSql, "UPDATE");
       tSql = "update lpgrpcont  set  agentgroup='" + tAgentGroup+
       "',ModifyTime = '" + currentTime +
       "',ModifyDate = '" + currentDate +
       "',operator='"+mGlobalInput.Operator+"'  where agentcode='"+cAgentCode+"'";
       mMap.put(tSql, "UPDATE");
       tSql = "update lcgrppol  set  agentgroup='" + tAgentGroup+
              "',ModifyTime = '" + currentTime +
              "',ModifyDate = '" + currentDate +
              "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
       mMap.put(tSql, "UPDATE");
       tSql = "update lbgrppol  set  agentgroup='" + tAgentGroup+
       "',ModifyTime = '" + currentTime +
       "',ModifyDate = '" + currentDate +
       "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
       mMap.put(tSql, "UPDATE");
       tSql = "update lpgrppol  set  agentgroup='" + tAgentGroup+
       "',ModifyTime = '" + currentTime +
       "',ModifyDate = '" + currentDate +
       "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
       mMap.put(tSql, "UPDATE");
        tSql = "update ljspayperson  set  agentgroup='" + tAgentGroup+
               "',ModifyTime = '" + currentTime +
               "',ModifyDate = '" + currentDate +
               "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        tSql = "update ljspay  set  agentgroup='" + tAgentGroup+
               "',ModifyTime = '" + currentTime +
               "',ModifyDate = '" + currentDate +
               "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
        tSql = "update ljspaygrp  set  agentgroup='" + tAgentGroup+
              "',ModifyTime = '" + currentTime +
              "',ModifyDate = '" + currentDate +
              "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
        mMap.put(tSql, "UPDATE");
       tSql = "update ljsgetendorse  set  agentgroup='" + tAgentGroup+
              "',ModifyTime = '" + currentTime +
              "',ModifyDate = '" + currentDate +
              "',operator='"+mGlobalInput.Operator+"' where agentcode='"+cAgentCode+"'";
       mMap.put(tSql, "UPDATE");

       return true ;
       // mEdorNo=cEdorNo;
    }
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("GetInputData-AdJustAgentBL");
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLABranchSchema.setSchema((LABranchGroupSchema) cInputData.
                                  getObjectByObjectName(
                                          "LABranchGroupSchema",1));
        this.mLATreeSet.set((LATreeSet) cInputData.getObjectByObjectName(
                "LATreeSet", 2));
        TransferData tdata = (TransferData) cInputData.getObjectByObjectName("TransferData",0);
        //String ts = (String) tdata.getValueByName("IsSingle");
        //IsSingle = ts.equals("0")?true:false;
        //IsSingle = true;
        mAStartDate = mLATreeSet.get(1).getAstartDate(); //起那台录入的调整日期
        mOldManageCom=mLATreeSet.get(1).getManageCom();
        mOldAgentGroup=mLATreeSet.get(1).getAgentGroup();
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            mMap.put(this.mLAAgentBSet, "INSERT");
            mMap.put(this.mLATreeBSet, "INSERT");
            mMap.put(this.mUpdateLATreeSet, "UPDATE");
            mMap.put(this.mLAAgentSet, "UPDATE");
            mMap.put(this.mLACommisionSet, "UPDATE");
            mMap.put(this.minLABranchGroupBSet, "INSERT");
            mMap.put(this.mupLABranchGroupSet, "UPDATE");
            mMap.put(this.mLARecomRelationSet, "UPDATE");
            this.mOutputDate.add(mMap);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustAgentBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    /**
     *  人员“团队建设”起止日期调整
     * @return
     */
    private boolean dealAgentGbuild(LAAgentSchema tLAAgentSchema,LATreeSchema tLATreeSchema,String tAgentGroup,String tAstartDate)
    {
    	 ExeSQL tExeSQL = new ExeSQL();
    	 String tGbuidStartDate = AgentPubFun.formatDate(tAstartDate,"yyyy-MM-dd");//变更下条件
    	 //对于2015基本法中，薪资版本修改
    	 String tWageVersion = "2010";
    	 if("1".equals(tLAAgentSchema.getBranchType())&&"01".equals(tLAAgentSchema.getBranchType2()))
    	 {
    		 tWageVersion = "2015";
    	 }
         // 直接取消筹备标记 (针对2013版本筹备基本法)
    	if(tLAAgentSchema.getNoWorkFlag()!=null&&!tLAAgentSchema.getNoWorkFlag().equals("")&&tLAAgentSchema.getNoWorkFlag().equals("Y")&&tLAAgentSchema.getWageVersion().equals("2013A"))
    	{
  	        tLAAgentSchema.setNoWorkFlag("N");
	        tLAAgentSchema.setTrainDate("");
	        tLAAgentSchema.setWageVersion(tWageVersion);
	        tLATreeSchema.setWageVersion(tWageVersion);
    	}
    		LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
    		LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
    		tLABranchGroupDB.setAgentGroup(tAgentGroup);
            if (!tLABranchGroupDB.getInfo())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "AdjustAgentBL";
                tError.functionName = "dealData()";
                tError.errorMessage = "不存在所要修改的机构!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLABranchGroupSchema =tLABranchGroupDB.getSchema();
            // 人员团队建设标记跟随所属团队建设标记
            if(tLABranchGroupSchema.getApplyGBFlag()!=null&&!tLABranchGroupSchema.getApplyGBFlag().equals("")&&tLABranchGroupSchema.getApplyGBFlag().equals("Y")&&tGbuidStartDate.compareTo("2014-04-01")<0)
            {
            	// 先置空，条件满足再置回
            	if(tLABranchGroupSchema.getGBuildFlag()!=null&&!tLABranchGroupSchema.getGBuildFlag().equals(""))
            	{
            		if(tLAAgentSchema.getGBuildFlag()!=null&&!tLAAgentSchema.getGBuildFlag().equals(""))
            		{           			
                		String sql = "select distinct 'Y' from laindexinfo where branchtype = '1' and branchtype2 ='01' and indextype  = '01' and agentcode = '"+tLAAgentSchema.getAgentCode()+"' and t67 in(99,98) ";
                		String tResult=tExeSQL.getOneValue(sql);
                		if(tResult==null||tResult.equals(""))
                		{
                			  // 团队建设止期为2013-12-15	
                		    	tLAAgentSchema.setGBuildFlag(tLABranchGroupSchema.getGBuildFlag());
                                tLAAgentSchema.setGBuildStartDate(tAstartDate);
                               
                                String tsql = "select enddate from LASTATSEGMENT where char(yearmonth) = db2inst1.DATE_FORMAT(((date('"+tAstartDate+"') + 1 year) - 1 month),'yyyymm') and  stattype = '1'";
                  				String tEndDate = tExeSQL.getOneValue(tsql);
                  				if(tEndDate==null||tEndDate.equals(""))
                  				  {
                  					   CError tError = new CError();
                  			           tError.moduleName = "LABranchGroupBuildBL";
                  			           tError.functionName = "check";
                  			           tError.errorMessage = "团队建设起期格式录入有误!";
                  			           this.mErrors.addOneError(tError);
                  			           return false;  
                  				  }
                  				  tLAAgentSchema.setGBuildEndDate(tEndDate); 
                  				  tLAAgentSchema.setWageVersion("2013B");
                  				  tLATreeSchema.setWageVersion("2013B");       			     				
                		  }else{
                          	tLAAgentSchema.setGBuildFlag("");
                          	tLAAgentSchema.setGBuildStartDate("");
                          	tLAAgentSchema.setGBuildEndDate("");
                          	tLAAgentSchema.setWageVersion(tWageVersion);
                          	tLATreeSchema.setWageVersion(tWageVersion); 
                		  }
            		}else{
          		    	tLAAgentSchema.setGBuildFlag(tLABranchGroupSchema.getGBuildFlag());
                        tLAAgentSchema.setGBuildStartDate(tAstartDate);                       
                          String tsql = "select enddate from LASTATSEGMENT where char(yearmonth) = db2inst1.DATE_FORMAT(((date('"+tAstartDate+"') + 1 year) - 1 month),'yyyymm') and  stattype = '1'";
            				String tEndDate = tExeSQL.getOneValue(tsql);
            				if(tEndDate==null||tEndDate.equals(""))
            				  {
            					   CError tError = new CError();
            			           tError.moduleName = "LABranchGroupBuildBL";
            			           tError.functionName = "check";
            			           tError.errorMessage = "团队建设起期格式录入有误!";
            			           this.mErrors.addOneError(tError);
            			           return false;  
            				  }
            				  tLAAgentSchema.setGBuildEndDate(tEndDate); 	
            				  tLAAgentSchema.setWageVersion("2013B");
              				  tLATreeSchema.setWageVersion("2013B");
    			
            		}
            	}else{
                	tLAAgentSchema.setGBuildFlag("");
                	tLAAgentSchema.setGBuildStartDate("");
                	tLAAgentSchema.setGBuildEndDate("");
                	tLAAgentSchema.setWageVersion(tWageVersion);
                	tLATreeSchema.setWageVersion(tWageVersion);
            	}
            }else{
            	tLAAgentSchema.setGBuildFlag("");
            	tLAAgentSchema.setGBuildStartDate("");
            	tLAAgentSchema.setGBuildEndDate("");
            	tLAAgentSchema.setWageVersion(tWageVersion);
            	tLATreeSchema.setWageVersion(tWageVersion);
            }
            
            
    	return true;    	
    }
    public VData getResult() {
        return this.mResult;
    }
}
