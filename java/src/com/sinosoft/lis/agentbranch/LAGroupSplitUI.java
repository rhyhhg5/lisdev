/*
 * <p>ClassName: LAGroupUniteUI </p>
 * <p>Description: LAGroupUniteUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-07-26 09:58:25
 */
package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAGroupSplitUI
{
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    public static void main(String[] agrs)
    {
        String cOperate = "INSERT||MAIN";
        LABranchGroupSchema tLABranchGroupSchemaAim = new LABranchGroupSchema();
        LABranchGroupSchema tLABranchGroupSchemaNew = new LABranchGroupSchema();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        LATreeSchema tLATreeSchema = new LATreeSchema();   // 任命管理员信息
        LATreeSet tLATreeSet = new LATreeSet();            // 人员调动信息

        // 目标机构代码
        tLABranchGroupSchemaAim.setBranchAttr("8611000010");
        // 分离出的新机构
        tLABranchGroupSchemaNew.setName("北京团险销售第32部");
        tLABranchGroupSchemaNew.setManageCom("86110000");
        tLABranchGroupSchemaNew.setUpBranch("");
        tLABranchGroupSchemaNew.setBranchAttr("8611000032");
        tLABranchGroupSchemaNew.setBranchType("2");
        tLABranchGroupSchemaNew.setBranchType2("01");
        tLABranchGroupSchemaNew.setBranchLevel("21");
        tLABranchGroupSchemaNew.setBranchAddressCode("");
        tLABranchGroupSchemaNew.setBranchAddress("北京东三环北路36号");
        tLABranchGroupSchemaNew.setBranchPhone("010-12345678");
        tLABranchGroupSchemaNew.setBranchFax("");
        tLABranchGroupSchemaNew.setBranchZipcode("100089");
        tLABranchGroupSchemaNew.setFoundDate("2005-07-01");
        tLABranchGroupSchemaNew.setAStartDate("2005-07-01");
        tLABranchGroupSchemaNew.setEndDate("");
        tLABranchGroupSchemaNew.setEndFlag("N");
        //准备数据
        tLABranchGroupSet.add(tLABranchGroupSchemaAim);
        tLABranchGroupSet.add(tLABranchGroupSchemaNew);

        //准备调动人员
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setAgentCode("1102000010");
        LATreeSchema ttLATreeSchema = new LATreeSchema();
        ttLATreeSchema.setAgentCode("1102000010"); //代理人职级
        ttLATreeSchema.setAgentGrade("D01"); //代理人职级
        ttLATreeSchema.setBranchType("2");
        ttLATreeSchema.setBranchType2("01");
        ttLATreeSchema.setAstartDate("2005-07-01");
        //tLATreeSet = tLATreeDB.query();
        tLATreeSet.add(ttLATreeSchema);
        System.out.println("被调整的人数:"+tLATreeSet.size());

        //新机构信息
        VData cInputData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "mak002";
        tGlobalInput.ManageCom = "861100";
        tGlobalInput.ComCode = "mak002";

        cInputData.add(tGlobalInput);
        cInputData.add(tLABranchGroupSet);
        cInputData.add(tLATreeSet);
        //cInputData.add(tLATreeSchema);

        LAGroupSplitUI tLAGroupUniteUI = new LAGroupSplitUI();
        boolean actualReturn = tLAGroupUniteUI.submitData(cInputData, cOperate);
    }

    public boolean submitData(VData cInputData,String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        mInputData=(VData)cInputData.clone();

        LAGroupSplitBL tLAGroupSplitBL= new LAGroupSplitBL();
        tLAGroupSplitBL.submitData(mInputData,mOperate);
        //如果有需要处理的错误，则返回
        if (tLAGroupSplitBL.mErrors.needDealError())
        {
          // @@错误处理
          this.mErrors.copyAllErrors(tLAGroupSplitBL.mErrors);
          CError tError = new CError();
          tError.moduleName = "LAGroupSplitUI";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
          this.mErrors.addOneError(tError);
          System.out.println("LAGroupSplitUI --> 处理失败!");
          return false;
        }
        System.out.println("LAGroupSplitUI --> 处理成功!");
        return true;
    }

}
