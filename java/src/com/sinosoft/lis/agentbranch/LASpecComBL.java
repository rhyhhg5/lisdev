package com.sinosoft.lis.agentbranch;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */

import java.math.BigInteger;
import java.util.Date;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LAComToAgentDB;
import com.sinosoft.lis.db.LAContDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAComBSchema;
import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.lis.schema.LAComToAgentBSchema;
import com.sinosoft.lis.schema.LAComToAgentSchema;
import com.sinosoft.lis.schema.LAContSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LAComSet;
import com.sinosoft.lis.vschema.LAComToAgentBSet;
import com.sinosoft.lis.vschema.LAComToAgentSet;
import com.sinosoft.lis.vschema.LAContSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;


public class LASpecComBL
{

    public LASpecComBL()
    {
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 存放查询结果 */
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
 
    private String mAgentCom;
    
    private String mProtocolNo;
    private String a;
    /** 业务处理相关变量 */
    private LAComSchema mLAComSchema = new LAComSchema();
    private LAComToAgentSet mLAComToAgentSet = new LAComToAgentSet();
    private LAComToAgentSchema mLAComToAgentSchema = new LAComToAgentSchema();
    //private LAComToAgentSchema tempLAComToAgentSchema1 = new LAComToAgentSchema();
    //private LAComToAgentSchema tempLAComToAgentSchema2 = new LAComToAgentSchema();
    private LAComToAgentBSet mLAComToAgentBSet = new LAComToAgentBSet();
    private LAComToAgentBSchema mLAComToAgentBSchema = new LAComToAgentBSchema();
    
        
    private LAComToAgentSet mdelLAComToAgentSet = new LAComToAgentSet();//处理修改操作
    private LAComToAgentSet minLAComToAgentSet = new LAComToAgentSet();
    
    private LAContSchema mLAContSchema = new LAContSchema();
    private LAComBSchema mLAComBSchema=new LAComBSchema();
    private String signDate1=null;
    String currentDate = PubFun.getCurrentDate();
    String currentTime = PubFun.getCurrentTime();

    private MMap tmap = new MMap();
    private VData mOutputData = new VData();
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        
        if (!getInputData(cInputData))
        {
            return false;
        }
      
        if (!check())
        {
            return false;
        }
  
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LASpecComBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LASpecComBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
       
        if (!prepareOutputData())
        {
            return false;
        }
       
        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mOutputData, "");
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError())
        {
            // @@错误处理
            //this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAChargeToNxsLNBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);		      
            return false;
        }

        mInputData = null;
        return true;
    }


    private boolean check()
    {
        String tACType=mLAComSchema.getACType() ;
        System.out.println("操作符："+this.mOperate);
        if ("INSERT||MAIN".equals(this.mOperate))
        {
            System.out.println("代理机构代码："+mLAComSchema.getAgentCom());
            if(!"".equals(mLAComSchema.getAgentCom()) && mLAComSchema.getAgentCom() != null)
            {
                CError tError = new CError();
                tError.moduleName = "LASpecComBL";
                tError.functionName = "check";
                tError.errorMessage = "被选出来的机构，请使用修改功能!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

      
        if(this.mOperate.equals("UPDATE||MAIN"))
        {
        	LAComToAgentSchema  tempLAComToAgentSchema1 ;
        	LAComToAgentSchema  tempLAComToAgentSchema2 ;
        
        for(int i=1;i<=this.mLAComToAgentSet.size();i++)
        {
        	tempLAComToAgentSchema1=this.mLAComToAgentSet.get(i);
  		  for(int j=i+1;j<=this.mLAComToAgentSet.size();j++){
  			tempLAComToAgentSchema2=this.mLAComToAgentSet.get(j);
  			 if(tempLAComToAgentSchema1.getAgentCode()
					  .equals(tempLAComToAgentSchema2.getAgentCode())||tempLAComToAgentSchema1.getAgentCode().equals(tempLAComToAgentSchema2.getOperator())){
				  CError tError = new CError();
			      tError.moduleName = "LABComRateSetBL";
			      tError.functionName = "check()";
			      tError.errorMessage = "在验证操作数据时出错。修改数据中第"+i+"行与第"+j+"行人员编码重复。";
			      this.mErrors.clearErrors();
			      this.mErrors.addOneError(tError);
			      return false;
  		  }
        }

    }
        
    }
        return true;
    }
    /**
     * 编号方式为：中介机构代码二位（专业代理公司PI、经纪公司PN、兼业代理人PC）
     *                        +分支机构代码六位（规则为二位分公司代码+四位支公司代码）
     *                        +四位协议流水号，共16位
     * @return String
     */
    private String createAgentComNo()
    {
        // 查询不同代理类型对应的标记
        String tSql = "";               // 查询代码类型的sql文
        String tAgentComNo = "";        // 代理机构编码
        String tACType = "";            // 代理类型标记
        ExeSQL tExeSQL = new ExeSQL();

        tSql  = "select OtherSign from ldcode where codetype = 'actype'";
        tSql += " and code='" + mLAComSchema.getACType() + "'";

        // 专业代理公司PI、经纪公司PN、兼业代理人PC
        tACType = tExeSQL.getOneValue(tSql);
        System.out.println("运行到这了="+tACType);
        tAgentComNo = tACType;
        // 追加分支公司代码六位
        tAgentComNo += mLAComSchema.getManageCom().substring(2,8);
        // 追加四位协议流水号
        String SeriesNo = PubFun1.CreateMaxNo("Medi" + tACType, 4);
        tAgentComNo += SeriesNo;


        return tAgentComNo;
    }



    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        //得到当前系统时期
        String currentDate = PubFun.getCurrentDate();
        //得到当前系统时间
        String currentTime = PubFun.getCurrentTime();
        LAComToAgentSet tLAComToAgentSet = new LAComToAgentSet();
        System.out.println("操作符为:"+this.mOperate);
        if (("INSERT||MAIN").equals(this.mOperate))
        {
            mAgentCom=createAgentComNo();         
            System.out.println("声称的中介机构编码："+mAgentCom);            
            this.mLAComSchema.setAgentCom(mAgentCom) ;
            this.mLAComSchema.setMakeDate(currentDate);
            this.mLAComSchema.setMakeTime(currentTime);
            this.mLAComSchema.setModifyDate(currentDate);
            this.mLAComSchema.setModifyTime(currentTime);
            
            tmap.put(this.mLAComSchema, "INSERT");

            this.mLAContSchema.setAgentCom(mAgentCom);
            this.mLAContSchema.setProtocolNo(createProtocalNo());
            this.mLAContSchema.setAgentCom(mAgentCom);
            this.mLAContSchema.setOperator(this.mGlobalInput.Operator);
            this.mLAContSchema.setMakeDate(currentDate);
            this.mLAContSchema.setMakeTime(currentTime);
            this.mLAContSchema.setModifyDate(currentDate);
            this.mLAContSchema.setModifyTime(currentTime);
            tmap.put(mLAContSchema, "INSERT");
            int num = this.mLAComToAgentSet.size();
            System.out.println("=======人员次数"+num);
            for (int i = 1; i <= num; i++)
            {
                this.mLAComToAgentSchema = this.mLAComToAgentSet.get(i);
                if (this.mLAComToAgentSchema.getRelaType().equals("0"))
                {
                	
                    String tAgentGroup = this.mLAComToAgentSchema.getAgentGroup();
                    if (tAgentGroup == null || tAgentGroup.equals(""))
                    {
                        continue;
                    }
                    LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
                    LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                    tLABranchGroupDB.setBranchAttr(this.mLAComToAgentSchema.
                            getAgentGroup());
                    tLABranchGroupSet = tLABranchGroupDB.query();
                    if (tLABranchGroupSet.size() > 0)
                    {
                        this.mLAComToAgentSchema.setAgentGroup(
                                tLABranchGroupSet.get(1).getAgentGroup());
                    }
                    this.mLAComToAgentSchema.setAgentCode("");
                }
                else
                {
                    String tAgentCode = this.mLAComToAgentSchema.getAgentCode();
                    if (tAgentCode == null || tAgentCode.equals(""))
                    {
                        continue;
                    }
                    LAAgentDB tLAAgentDB = new LAAgentDB();
                    tLAAgentDB.setAgentCode(this.mLAComToAgentSchema.
                                            getAgentCode());
                    tLAAgentDB.getInfo();
                    this.mLAComToAgentSchema.setAgentGroup(tLAAgentDB.
                            getAgentGroup());
                }
                this.mLAComToAgentSchema.setAgentCom(mAgentCom);
                this.mLAComToAgentSchema.setModifyDate(currentDate);
                this.mLAComToAgentSchema.setModifyTime(currentTime);
                this.mLAComToAgentSchema.setMakeDate(currentDate);
                this.mLAComToAgentSchema.setMakeTime(currentTime);
                this.mLAComToAgentSchema.setOperator(mGlobalInput.Operator);
                tLAComToAgentSet.add(mLAComToAgentSchema);
           }
            tmap.put(tLAComToAgentSet, "INSERT");
        }
        else if (("UPDATE||MAIN").equals(this.mOperate))
        {
            
        	LAContDB tLAContDB = new LAContDB();
        	tLAContDB.setAgentCom(mLAContSchema.getAgentCom());
        	tLAContDB.setProtocolNo(mLAContSchema.getProtocolNo());
        	if (!tLAContDB.getInfo())
            {
        		this.mLAContSchema.setProtocolNo(createProtocalNo());
        		this.mLAContSchema.setAgentCom(mLAComSchema.getAgentCom());
                this.mLAContSchema.setOperator(this.mGlobalInput.Operator);
                this.mLAContSchema.setMakeDate(currentDate);
                this.mLAContSchema.setMakeTime(currentTime);
                this.mLAContSchema.setModifyDate(currentDate);
                this.mLAContSchema.setModifyTime(currentTime);
                tmap.put(mLAContSchema, "INSERT");
        		
            }
        	else 
        	{
        	LAContSchema tLAContSchema=new LAContSchema();
        	tLAContSchema=tLAContDB.getSchema();
        	if(tLAContSchema.getSignDate().equals(mLAContSchema.getSignDate()))
        	{
        		tLAContSchema.setEndDate(mLAContSchema.getEndDate());
        		tLAContSchema.setOperator(this.mGlobalInput.Operator);
        		tLAContSchema.setModifyDate(currentDate);
                tLAContSchema.setModifyTime(currentTime); 
                tmap.put(tLAContSchema, "UPDATE");
        	}
        	else
        	{
        		tLAContSchema.setSignDate(mLAContSchema.getSignDate());
        		tLAContSchema.setStartDate(mLAContSchema.getSignDate());
        		tLAContSchema.setEndDate(mLAContSchema.getEndDate());
        		tLAContSchema.setOperator(this.mGlobalInput.Operator);
        		tLAContSchema.setModifyDate(currentDate);
                tLAContSchema.setModifyTime(currentTime); 
                tmap.put(tLAContSchema, "UPDATE");
        	}    
        	}
            LAComDB tLAComDB = new LAComDB();
            tLAComDB.setAgentCom(mLAComSchema.getAgentCom());
            
            if (!tLAComDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LASpecComBL";
                tError.functionName = "dealData";
                tError.errorMessage = "未发现号码为"+mLAComSchema.getAgentCom() +"的中介机构！";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            LAComSchema tLAComSchema= new LAComSchema();
            tLAComSchema=tLAComDB.getSchema();
            Reflections tReflections=new Reflections();
            tReflections.transFields(mLAComBSchema, tLAComSchema);
            mLAComBSchema.setEdorNo(PubFun1.CreateMaxNo("EdorNo", 19));
            mLAComBSchema.setEdorType("01");
            mLAComBSchema.setAreaType(mLAComSchema.getAreaType());
            mLAComBSchema.setOperator(tLAComSchema.getOperator());
            mLAComBSchema.setModifyDate(currentDate);
            mLAComBSchema.setModifyTime(currentTime);
            mLAComBSchema.setMakeDate(this.currentDate);
            mLAComBSchema.setMakeTime(this.currentTime);
            mLAComBSchema.setMakeDate2(tLAComSchema.getMakeDate());
            mLAComBSchema.setMakeTime2(tLAComSchema.getMakeTime());
            mLAComBSchema.setModifyDate2(tLAComSchema.getModifyDate());
            mLAComBSchema.setModifyTime2(tLAComSchema.getModifyTime());
            
            mAgentCom=tLAComDB.getAgentCom() ;
            tLAComDB.setAgentCom(mLAComSchema.getAgentCom());
            tLAComDB.setName(mLAComSchema.getName());
            tLAComDB.setBusiLicenseCode(mLAComSchema.getBusiLicenseCode());
            tLAComDB.setAppAgentCom(mLAComSchema.getAppAgentCom());
            tLAComDB.setCorporation(mLAComSchema.getCorporation());
            tLAComDB.setLinkMan(mLAComSchema.getLinkMan());
            tLAComDB.setAddress(mLAComSchema.getAddress());
            tLAComDB.setPhone(mLAComSchema.getPhone());
            tLAComDB.setUpAgentCom(mLAComSchema.getUpAgentCom());
            tLAComDB.setManageCom(mLAComSchema.getManageCom());
            tLAComDB.setACType(mLAComSchema.getACType());
            System.out.println("展业类型编码："+mLAComSchema.getACType());
            tLAComDB.setChiefBusiness(mLAComSchema.getChiefBusiness());
            tLAComDB.setBankAccNo(mLAComSchema.getBankAccNo());
            tLAComDB.setBankCode(mLAComSchema.getBankCode());
            tLAComDB.setEndFlag(mLAComSchema.getEndFlag());
            tLAComDB.setEndDate(mLAComSchema.getEndDate());
            tLAComDB.setAssets(mLAComSchema.getAssets());
            tLAComDB.setProfits(mLAComSchema.getProfits());
            tLAComDB.setSellFlag(mLAComSchema.getSellFlag());
            tLAComDB.setAreaType(mLAComSchema.getAreaType());
            tLAComDB.setChannelType(mLAComSchema.getChannelType());
            tLAComDB.setLicenseNo(mLAComSchema.getLicenseNo());
            tLAComDB.setBranchType2(mLAComSchema.getBranchType2());
            tLAComDB.setModifyDate(currentDate);
            tLAComDB.setModifyTime(currentTime);
            tLAComDB.setOperator(mGlobalInput.Operator);
            tLAComDB.setLicenseStartDate(mLAComSchema.getLicenseStartDate());
            tLAComDB.setLicenseEndDate(mLAComSchema.getLicenseEndDate());
            tLAComDB.setProtocalNo(mLAComSchema.getProtocalNo());
            tLAComDB.setBankAccName(mLAComSchema.getBankAccName());
            tLAComDB.setBankAccOpen(mLAComSchema.getBankAccOpen());
            //添加对于 中介组织机构代码
            tLAComDB.setAgentOrganCode(mLAComSchema.getAgentOrganCode());
            mLAComSchema.setSchema(tLAComDB);
            
            
            tmap.put(mLAComSchema, "UPDATE");
            tmap.put(mLAComBSchema, "INSERT");
             LAComToAgentDB tLAComToAgentDB = new LAComToAgentDB();
            tLAComToAgentDB.setAgentCom(mLAComToAgentSet.get(1).getAgentCom());
            mdelLAComToAgentSet = tLAComToAgentDB.query();
            
            if(mdelLAComToAgentSet.size()>0)
            {
            	for (int a = 1; a <= mdelLAComToAgentSet.size(); a++)
                {
                    LAComToAgentSchema aLAComToAgentSchema = mdelLAComToAgentSet.get(a);
                    //backup("01", aLAComToAgentSchema);
                    this.mLAComToAgentBSchema.setAgentCode(aLAComToAgentSchema.getAgentCode());
                    this.mLAComToAgentBSchema.setAgentCom(aLAComToAgentSchema.getAgentCom());
                    this.mLAComToAgentBSchema.setAgentGroup(aLAComToAgentSchema.
                                                            getAgentGroup());
                    this.mLAComToAgentBSchema.setRelaType(aLAComToAgentSchema.getRelaType());
                    this.mLAComToAgentBSchema.setEdorNo(PubFun1.CreateMaxNo("EdorNo", 20));
                    this.mLAComToAgentBSchema.setEdorType("01");
                    this.mLAComToAgentBSchema.setMakeDate(aLAComToAgentSchema.getMakeDate());
                    this.mLAComToAgentBSchema.setMakeTime(aLAComToAgentSchema.getMakeTime());
                    this.mLAComToAgentBSchema.setModifyDate(aLAComToAgentSchema.
                                                            getModifyDate());
                    this.mLAComToAgentBSchema.setModifyTime(aLAComToAgentSchema.
                                                            getModifyTime());
                    this.mLAComToAgentBSchema.setOperator(mGlobalInput.Operator);
                    LAComToAgentBSchema aLAComToAgentBSchema = new LAComToAgentBSchema();
                    aLAComToAgentBSchema.setSchema(mLAComToAgentBSchema);
                    mLAComToAgentBSet.add(aLAComToAgentBSchema);
                }
            	tmap.put(mdelLAComToAgentSet, "DELETE");
            }
            for(int k=1;k<=mLAComToAgentSet.size();k++)
            {
   
            	LAComToAgentSchema tLAComToAgentSchema = new LAComToAgentSchema();
            	tLAComToAgentSchema = mLAComToAgentSet.get(k).getSchema();
            	System.out.println("------------->>"+tLAComToAgentSchema.getAgentCode());
            	if(tLAComToAgentSchema.getAgentCode()!=null&&!tLAComToAgentSchema.getAgentCode().equals(""))
            	{
            	  tLAComToAgentSchema.setMakeDate(currentDate);
            	  tLAComToAgentSchema.setMakeTime(currentTime);
            	  tLAComToAgentSchema.setModifyDate(currentDate);
            	  tLAComToAgentSchema.setModifyTime(currentTime);
            	  tLAComToAgentSchema.setOperator(mGlobalInput.Operator);
            	  minLAComToAgentSet.add(tLAComToAgentSchema);
            	}
            }
            tmap.put(minLAComToAgentSet, "INSERT");
            tmap.put(mLAComToAgentBSet, "INSERT");
            
        }
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mLAComSchema.setSchema((LAComSchema) cInputData.getObjectByObjectName(
                "LAComSchema", 0));
        System.out.println(mLAComSchema.getBankAccName());
        System.out.println(mLAComSchema.getBankAccOpen());
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLAComToAgentSet.set((LAComToAgentSet) cInputData.getObjectByObjectName(
                "LAComToAgentSet", 0));
        //=====fanting
        mLAContSchema.setSchema((LAContSchema) cInputData.getObjectByObjectName(
        		"LAContSchema", 0));
       
        this.signDate1=(String)cInputData.getObject(4);

        return true;
    }


    private boolean prepareOutputData()
    {
        try
        {
            this.mOutputData.add(tmap);
            System.out.println(tmap.size());

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LASpecComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    public VData getResult()
    {
        mResult=new VData();
        mResult.add(mAgentCom) ;
        return mResult;
    }

    private boolean backup(String aEdorType,
                           LAComToAgentSchema aLAComToAgentSchema)
    {
//      String currentDate=PubFun.getCurrentDate();
//      String currentTime=PubFun.getCurrentTime();
        this.mLAComToAgentBSchema.setAgentCode(aLAComToAgentSchema.getAgentCode());
        this.mLAComToAgentBSchema.setAgentCom(aLAComToAgentSchema.getAgentCom());
        this.mLAComToAgentBSchema.setAgentGroup(aLAComToAgentSchema.
                                                getAgentGroup());
        this.mLAComToAgentBSchema.setRelaType(aLAComToAgentSchema.getRelaType());
        this.mLAComToAgentBSchema.setEdorNo(PubFun1.CreateMaxNo("EdorNo", 20));
        this.mLAComToAgentBSchema.setEdorType(aEdorType);
        this.mLAComToAgentBSchema.setMakeDate(aLAComToAgentSchema.getMakeDate());
        this.mLAComToAgentBSchema.setMakeTime(aLAComToAgentSchema.getMakeTime());
        this.mLAComToAgentBSchema.setModifyDate(aLAComToAgentSchema.
                                                getModifyDate());
        this.mLAComToAgentBSchema.setModifyTime(aLAComToAgentSchema.
                                                getModifyTime());
        this.mLAComToAgentBSchema.setOperator(mGlobalInput.Operator);
        LAComToAgentBSchema aLAComToAgentBSchema = new LAComToAgentBSchema();
        aLAComToAgentBSchema.setSchema(mLAComToAgentBSchema);
        mLAComToAgentBSet.add(aLAComToAgentBSchema);
        System.out.println("哭了！！！");
        tmap.put(mLAComToAgentBSet, "INSERT");

        return true;
    }
     //fanting 得到protocalNo
    private String createProtocalNo()
    {
    	   System.out.println("=====");
            String protocolNo = null;
            if(this.mProtocolNo==null||this.mProtocolNo.equals("")){
            	String validDate = this.mLAContSchema.getStartDate();
            	//fanting 
            	System.out.println(validDate);
            	validDate = com.sinosoft.lis.pubfun.AgentPubFun.formatDate(
                    validDate,
                    "yyyy");
            	//validDate = validDate.substring(2,4);
            	String tAgentCom = this.mLAContSchema.getAgentCom();
            	System.out.println(tAgentCom+"===========aaabbb");
            	tAgentCom = tAgentCom.substring(0, 2);
            	String tManageCom = this.mLAContSchema.getManageCom();
            	tManageCom=tManageCom.substring(2,4);
            	tAgentCom = tAgentCom+tManageCom+"0000";
            	BigInteger tBigInteger = new BigInteger(tAgentCom.substring(2));
            	BigInteger tOneBigInteger = new BigInteger("1");
            	String sql =
                    "select Max(protocolno) from lacont where protocolno like '" +
                    tAgentCom + validDate + "%' ";
            	ExeSQL tExeSQL = new ExeSQL();
            	String maxProtocolNo = tExeSQL.getOneValue(sql);
              System.out.println(maxProtocolNo+"================aaaa");
            	if (maxProtocolNo == null || maxProtocolNo.equals(""))
                {
                    protocolNo = tAgentCom + validDate + "0001";
                    System.out.println("protocolNo:{" + protocolNo + "}");
                }
                else
                {
                    tBigInteger = new BigInteger(maxProtocolNo.substring(2));
                    tBigInteger = tBigInteger.add(tOneBigInteger);
                    protocolNo = tAgentCom.substring(0, 2) + tBigInteger.toString();
                }
            }else{
            	BigInteger tBigInteger = new BigInteger(this.mProtocolNo.substring(2));
            	BigInteger tOneBigInteger = new BigInteger("1");
            	tBigInteger = tBigInteger.add(tOneBigInteger);
                protocolNo = this.mLAContSchema.getAgentCom().substring(0, 2) + tBigInteger.toString();
            }
        this.mProtocolNo =protocolNo;   
        return protocolNo;
       
    }
    /**
    private boolean checkContExists(LAContSchema tLAContSchema,String tAgentCom){
    	StringBuffer tSB=new StringBuffer();
    	LAContDB tLAContDB=new LAContDB();
    	LAContSet tLAContSet=new LAContSet();
    	tSB.append("select * from lacont where agentcom='")
		.append(tAgentCom)
		.append("' fetch first 1 rows only with ur")
		;
    	tLAContSet=tLAContDB.executeQuery(tSB.toString());
    	if(tLAContSet.size()>=1){
    		tLAContSchema.setProtocolNo(tLAContSet.get(1).getProtocolNo());
    		tLAContSchema.setMakeDate(tLAContSet.get(1).getMakeDate());
    		tLAContSchema.setMakeTime(tLAContSet.get(1).getMakeTime());
    		this.mStartDate=tLAContSet.get(1).getSignDate();
    		this.mEndDate=tLAContSet.get(1).getEndDate();
    		return true;
    	}else{
    		return false;
    	}
    	
    }
    */
    /**
    private boolean coverAgentCom(String tAgentCom){
    	StringBuffer tSB=new StringBuffer();
    	LAComDB tLAComDB=new LAComDB();
    	LAComSet tLAComSet=new LAComSet();
    	LAContSchema tLAContSchema=null;
    	
    	tSB.append("select * from lacom where branchtype='3' and branchtype2='01' ")
    		.append("and upagentcom='")
    		.append(tAgentCom)
    		.append("' with ur")
    		;
    	tLAComSet=tLAComDB.executeQuery(tSB.toString());
    	for(int i=1;i<=tLAComSet.size();i++){
    		coverAgentCom(tLAComSet.get(i).getAgentCom());
    		tLAComSet.get(i)
    		.setLicenseStartDate(this.mLAComSchema.getLicenseStartDate());
    		tLAComSet.get(i)
    		.setEndDate(this.mLAComSchema.getLicenseEndDate());
    		tLAComSet.get(i).setOperator(this.mGlobalInput.Operator);
    		tLAComSet.get(i).setModifyDate(this.currentDate);
    		tLAComSet.get(i).setModifyTime(this.currentTime);
    		this.mLAComSet2.add(tLAComSet.get(i));
    		tLAContSchema=new LAContSchema();
    		if(checkContExists(tLAContSchema,tLAComSet.get(i).getAgentCom())){
    			tLAContSchema.setAgentCom(tLAComSet.get(i).getAgentCom());
    			tLAContSchema.setManageCom(tLAComSet.get(i).getManageCom());
    			tLAContSchema.setProtocolType("0");
    			tLAContSchema.setSignDate(this.mLAContSchema.getSignDate());
    			tLAContSchema.setStartDate(this.mLAContSchema.getStartDate());
    			tLAContSchema.setEndDate(this.mLAContSchema.getEndDate());
    			tLAContSchema.setOperator(this.mGlobalInput.Operator);
    			tLAContSchema.setModifyDate(this.currentDate);
    			tLAContSchema.setModifyTime(this.currentTime);
    			this.mUpLAContSet.add(tLAContSchema);
    		}else{
    			tLAContSchema.setProtocolNo(createProtocalNo());
    			tLAContSchema.setAgentCom(tLAComSet.get(i).getAgentCom());
    			tLAContSchema.setManageCom(tLAComSet.get(i).getManageCom());
    			tLAContSchema.setProtocolType("0");
    			tLAContSchema.setSignDate(this.mLAContSchema.getSignDate());
    			tLAContSchema.setStartDate(this.mLAContSchema.getStartDate());
    			tLAContSchema.setEndDate(this.mLAContSchema.getEndDate());
    			tLAContSchema.setOperator(this.mGlobalInput.Operator);
    			tLAContSchema.setMakeDate(this.currentDate);
    			tLAContSchema.setMakeTime(this.currentTime);
    			tLAContSchema.setModifyDate(this.currentDate);
    			tLAContSchema.setModifyTime(this.currentTime);
    			this.mInLAContSet.add(tLAContSchema);
    		}
    		
    	}
    	return true;
    }
    */
    //
    public static void main(String[] args)
    {
    	LASpecComBL tLASpecComBL = new LASpecComBL();
        LAComSchema tLAComSchema = new LAComSchema();
        LAContSchema tLAContSchema = new LAContSchema();
        
        tLAComSchema.setAgentCom("");
        tLAComSchema.setOperator("001");
        tLAComSchema.setAreaType("A");
        tLAComSchema.setChannelType(" ");
        tLAComSchema.setACType("01");
        tLAComSchema.setManageCom("86110000");
        
        tLAContSchema.setAgentCom("1");
        tLAContSchema.setProtocolContent("2");
        tLAContSchema.setStartDate("2011-11-01");
        tLAContSchema.setManageCom("86110000");
        
        GlobalInput tG = new GlobalInput();
        tG.ComCode = "86110000";
        tG.ManageCom = "86110000";
        tG.Operator = "001";
        LAComToAgentSchema tLAComToAgentSchema0 = new LAComToAgentSchema();
        LAComToAgentSchema tLAComToAgentSchema1 = new LAComToAgentSchema();
        LAComToAgentSet tLAComToAgentSet = new LAComToAgentSet();

        tLAComToAgentSchema0.setAgentCom("3");
        tLAComToAgentSchema0.setRelaType("1"); //与机构关联
        tLAComToAgentSchema0.setAgentGroup("86110000002");
        tLAComToAgentSchema0.setAgentCode("8611000502");
        tLAComToAgentSchema0.setOperator("001");
        tLAComToAgentSet.add(tLAComToAgentSchema0);

        tLAComToAgentSchema1.setAgentCom("3");
        tLAComToAgentSchema1.setRelaType("1"); //与代理人关联
        tLAComToAgentSchema1.setAgentCode("8611000506");
        tLAComToAgentSchema1.setAgentGroup("86110000002");
        tLAComToAgentSchema1.setOperator("001");
        tLAComToAgentSet.add(tLAComToAgentSchema1);

        VData tVData = new VData();
        
        tVData.add(tLAContSchema);
        tVData.add(tLAComSchema);
        tVData.add(tLAComToAgentSet);
        tVData.add(tG);
        tLASpecComBL.submitData(tVData, "UPDATE||MAIN");
    }
}
