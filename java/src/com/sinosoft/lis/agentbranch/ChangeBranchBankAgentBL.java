package com.sinosoft.lis.agentbranch;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LABranchLevelSchema;
import com.sinosoft.lis.db.LABranchLevelDB;
import com.sinosoft.lis.vschema.LABranchLevelSet;
import java.math.BigInteger;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.db.LARearRelationDB;
import com.sinosoft.lis.schema.LARearRelationBSchema;
import com.sinosoft.lis.vschema.LARearRelationBSet;

public class ChangeBranchBankAgentBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private boolean IsSingle;
    private String mAdjustNewName;

    /** 业务处理相关变量 */
    private LABranchGroupSchema mAimLABranchSchema = new LABranchGroupSchema();
    private LABranchGroupSchema mAdjustLABranchSchema = new LABranchGroupSchema();
    private LARearRelationSet mLARearRelationSet = new LARearRelationSet();
    private LARearRelationBSet mLARearRelationBSet = new LARearRelationBSet();
    //插入到机构备份表,备份被调动机构的信息
    private LABranchGroupBSet mLABranchGroupBSet = new LABranchGroupBSet();

    //更新机构表中的upBranch，将被调动机构的上级机构相关信息更新
    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();

    private LATreeBSet mLATreeBSet = new LATreeBSet(); //备份管理人员的行政信息
    private LATreeSet mLATreeSet = new LATreeSet();
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    private boolean ifChangeManageCom = false; //是否跨了管理机构
    private String mUpBranchAttr = "";
    private String NewBranchAttr = "";
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String NewBranchSeries = "";
    private String mEdorNo = "";
    private String mBranchManager = new String();
    private String mAdjustDate = "";
    private int mCount = 0; //表示第几次生成BranchAttr(培养团队需要逐次加1)
    private MMap mMap = new MMap();

    public ChangeBranchBankAgentBL() {
    }

    /**
     * 校验的代码
     * @return
     */
    private boolean check() {
        String tStartDate = mAdjustDate;
        String tAdjustBranchAttr = mAdjustLABranchSchema.getBranchAttr();
        String tSQL =
                "select upBranchAttr,Managecom from labranchgroup where branchattr = '" +
                tAdjustBranchAttr + "' and BranchType='" +
                mAdjustLABranchSchema.getBranchType() + "' "
                + " and BranchType2='" + mAdjustLABranchSchema.getBranchType2() +
                "'"; //直辖非直辖
        ExeSQL mExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = mExeSQL.execSQL(tSQL);
        if (tSSRS == null || tSSRS.getMaxRow() == 0) {
            CError tError = new CError();
            tError.moduleName = "ChangeBranchBankAgentBL";
            tError.functionName = "check";
            tError.errorMessage = "查询机构属性时出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tManageCom = tSSRS.GetText(1, 2);

        String newStartDate = AgentPubFun.formatDate(tStartDate, "yyyy-MM-dd"); //调整日期转换格式
        String tDay = newStartDate.substring(newStartDate.lastIndexOf("-") + 1); //取停业所在月的日期
        if (!tDay.equals("01")) {
            CError tError = new CError();
            tError.moduleName = "ChangeBranchNewBL";
            tError.functionName = "check";
            tError.errorMessage = "调整日期必须是从某个月的1号开始!";
            this.mErrors.addOneError(tError);
            return false;
        } else {
        	System.out.println("check here");
//            String sql = "select max(indexcalno) from lawage where managecom='" +
//                         tManageCom + "' and branchtype='"+mBranchType+"' and branchtype2='"+mBranchType2+"'";
//            ExeSQL tExeSQL = new ExeSQL();
//            String maxIndexCalNo = tExeSQL.getOneValue(sql);
//            System.out.println("---最近发工资的日期---" + maxIndexCalNo);
//            String lastDate = PubFun.calDate(tStartDate, -1, "M", null);
//            lastDate = AgentPubFun.formatDate(lastDate, "yyyyMM");
//
//            if (maxIndexCalNo == null || maxIndexCalNo.trim().equals("")) {
//                CError tError = new CError();
//                tError.moduleName = "ChangeBranchNewBL";
//                tError.functionName = "check";
//                tError.errorMessage = "管理机构" + tManageCom +
//                                      "未计算过佣金，不能做调动!";
//                this.mErrors.addOneError(tError);
//                return false;
//            }
//            if (maxIndexCalNo.trim().equals(lastDate.trim())) {
//                return true;
//            } else {
//                CError tError = new CError();
//                tError.moduleName = "ChangeBranchNewBL";
//                tError.functionName = "check";
//                tError.errorMessage = "上次发工资是" + maxIndexCalNo.substring(0, 4) +
//                                      "年" +
//                                      maxIndexCalNo.substring(4).trim() +
//                                      "月，因此调整日期必须从这个月的下一个月1号";
//                this.mErrors.addOneError(tError);
//                return false;
//            }
        	return true;
        }
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Into ChangeBranchBankAgentBL !!! ");
        //将操作数据拷贝到本类中
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!check()) {
            return false;
        }
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ChangeBranchBankAgentBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ChangeBranchBankAgentBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
//        if (mLATreeSet != null && mLATreeSet.size() >= 1) {
//            if (!chgRelation()) {
//                return false;
//            }
//        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mInputData, "");
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "ChangeBranchBankAgentBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        NewBranchAttr = this.mAimLABranchSchema.getBranchAttr();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupDB.setBranchAttr(mAdjustLABranchSchema.getBranchAttr());
        tLABranchGroupDB.setBranchType(mAdjustLABranchSchema.getBranchType());
        tLABranchGroupDB.setBranchType2(mAdjustLABranchSchema.getBranchType2());
        tLABranchGroupSet = tLABranchGroupDB.query();
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
            CError tError = new CError();
            tError.moduleName = "ChangeBranchBankAgentBL";
            tError.functionName = "dealdata";
            tError.errorMessage = "未找到机构编码为" + mAimLABranchSchema.getBranchAttr() +
                                  "的机构信息!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mAdjustLABranchSchema = tLABranchGroupSet.get(1);
        tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setBranchAttr(mAimLABranchSchema.getBranchAttr());
        tLABranchGroupDB.setBranchType(mAimLABranchSchema.getBranchType());
        tLABranchGroupDB.setBranchType2(mAimLABranchSchema.getBranchType2());
        tLABranchGroupSet = tLABranchGroupDB.query();
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
            this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ChangeBranchBankAgentBL";
            tError.functionName = "dealdata";
            tError.errorMessage = "未找到机构编码为" + mAimLABranchSchema.getBranchAttr() +
                                  "的机构信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mAimLABranchSchema = tLABranchGroupSet.get(1);

        mCount = mCount + 1; //带走培养关系时,团队代码+mCount
        NewBranchAttr = createBranchAttr(); //自动生成机构代码

        if (NewBranchAttr == null || NewBranchAttr.equals("")) {
            return false;
        }

        this.mBranchManager = mAdjustLABranchSchema.getBranchManager(); //被调整机构主管
        String NewManageCom = mAimLABranchSchema.getManageCom();
        String OldManageCom = mAdjustLABranchSchema.getManageCom();
        mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        if (NewManageCom.equals(OldManageCom)) { //判断是否跨了管理机构调动
            ifChangeManageCom = false;
        } else {
            ifChangeManageCom = true;
        }
        //对被调动机构的下属机构作调动处理
        String sql = "select * from labranchgroup where upbranch = '" +
                     mAdjustLABranchSchema.getAgentGroup() +
                     "' and (state is null or state<>'1') and (endflag is null or endflag<>'Y')";
        System.out.println(sql);
        tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(sql);
        for (int i = 1; i <= tLABranchGroupSet.size(); i++) { //如果存在下属机构
            LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
            tLABranchGroupSchema = tLABranchGroupSet.get(i);
            if (!dealEachBranchGroup(tLABranchGroupSchema, "0")) {
                return false;
            }
        }
        if (tLABranchGroupSet.size() == 0) {
            //如果不存在下属机构,处理被调动机构本身(不存在下属机构,则该机构为处,处一级的单位需要处理它下面的人员,其他只要处理主管
            dealEachBranchGroup(mAdjustLABranchSchema, "1");
        }
        if (!dealBranchManager(mAdjustLABranchSchema)) {
            return false;
        }

        return true;
    }


    private String createBranchAttr() {
        String upBranchLevel = mAimLABranchSchema.getBranchLevel();
        String curBranchLevel = mAdjustLABranchSchema.getBranchLevel();
        System.out.println("mAdjustBranchSchema:" +
                           mAdjustLABranchSchema.getBranchAttr());
        System.out.println("mAimLABranchSchema:" +
                           mAimLABranchSchema.getBranchAttr());
        LABranchLevelSchema upBranchLevelSchema = new LABranchLevelSchema();
        LABranchLevelSchema cuBranchLevelSchema = new LABranchLevelSchema();
        LABranchLevelDB tLABranchLevelDB = new LABranchLevelDB();
        tLABranchLevelDB.setBranchLevelCode(upBranchLevel);
        tLABranchLevelDB.setBranchType(mAimLABranchSchema.getBranchType());
        tLABranchLevelDB.setBranchType2(mAimLABranchSchema.getBranchType2());
        tLABranchLevelDB.getInfo();
        upBranchLevelSchema = tLABranchLevelDB.getSchema();
        tLABranchLevelDB.setBranchLevelCode(curBranchLevel);
        tLABranchLevelDB.setBranchType(mAdjustLABranchSchema.getBranchType());
        tLABranchLevelDB.setBranchType2(mAdjustLABranchSchema.getBranchType2());
        tLABranchLevelDB.getInfo();
        cuBranchLevelSchema = tLABranchLevelDB.getSchema();
        System.out.println("cu:" + cuBranchLevelSchema.getBranchLevelID());
        System.out.println("up:" + upBranchLevelSchema.getBranchLevelID());
        if (cuBranchLevelSchema.getBranchLevelID() >=
            upBranchLevelSchema.getBranchLevelID()) {
            CError tError = new CError();
            tError.moduleName = "ChangeBranchNewBL";
            tError.functionName = "createBranchAttr";
            tError.errorMessage = "被调动的机构级别应该比被调入的目标机构级别低!。";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (cuBranchLevelSchema.getBranchLevelID() + 1 ==
            upBranchLevelSchema.getBranchLevelID()) {
            mUpBranchAttr = mAimLABranchSchema.getBranchAttr();
        } else {
            LABranchLevelSet tLABranchLevelSet = new LABranchLevelSet();
            tLABranchLevelDB = new LABranchLevelDB();
            tLABranchLevelDB.setBranchLevelID(cuBranchLevelSchema.
                                              getBranchLevelID() + 1);
            tLABranchLevelDB.setBranchType(cuBranchLevelSchema.getBranchType());
            tLABranchLevelDB.setBranchType2(cuBranchLevelSchema.getBranchType2());
            tLABranchLevelSet = tLABranchLevelDB.query();
            if (tLABranchLevelSet == null || tLABranchLevelSet.size() == 0) {
                CError tError = new CError();
                tError.moduleName = "ChangeBranchNewBL";
                tError.functionName = "createBranchAttr";
                tError.errorMessage = "未查询到当前机构的上级机构。";
                this.mErrors.addOneError(tError);
                return null;
            }
            LABranchLevelSchema tLABranchLevelSchema = new LABranchLevelSchema();
            tLABranchLevelSchema = tLABranchLevelSet.get(1);
            int upLength = Integer.parseInt(tLABranchLevelSchema.
                                            getBranchLevelProperty2());
            int zeroLength = upLength - Integer.parseInt(upBranchLevelSchema.
                    getBranchLevelProperty2());

            System.out.println(tLABranchLevelSchema.getBranchLevelProperty2());
            System.out.println(upBranchLevelSchema.getBranchLevelProperty2());
            System.out.println(zeroLength);
            String zero = "0000000000000000000000000".substring(0, zeroLength);
            System.out.println("getBranchAttr:" +
                               mAimLABranchSchema.getBranchAttr());
            System.out.println("zero:" + zero);
            mUpBranchAttr = mAimLABranchSchema.getBranchAttr() + zero;
        }
        String maxBranchAttr = "";
        ExeSQL tExeSQL = new ExeSQL();
        String maxBranchAttr1 = "";
        String maxBranchAttr2 = "";
        String sql =
                "select max(branchattr) from labranchgroup where branchattr like '" +
                mUpBranchAttr + "%' and BranchLevel='" +
                mAdjustLABranchSchema.getBranchLevel() + "' and BranchType='" +
                upBranchLevelSchema.getBranchType() + "' and BranchType2='" +
                upBranchLevelSchema.getBranchType2() + "'";
        maxBranchAttr1 = tExeSQL.getOneValue(sql);
        if (maxBranchAttr1 == null || maxBranchAttr1.equals("")) {
            maxBranchAttr1 = mUpBranchAttr +
                             "000000000000000".substring(0,
                    Integer.parseInt(cuBranchLevelSchema.
                                     getBranchLevelProperty2()) -
                    mUpBranchAttr.length());
        }
        BigInteger tBigInteger = new BigInteger(maxBranchAttr1);
        sql =
                "select max(branchattr) from labranchgroupb where branchattr like '" +
                mUpBranchAttr + "%' and BranchLevel='" +
                mAdjustLABranchSchema.getBranchLevel() + "'   and BranchType='" +
                upBranchLevelSchema.getBranchType() + "' and BranchType2='" +
                upBranchLevelSchema.getBranchType2() + "'";
        maxBranchAttr2 = tExeSQL.getOneValue(sql);
        if (maxBranchAttr2 == null || maxBranchAttr2.equals("")) {
            maxBranchAttr2 = mUpBranchAttr +
                             "000000000000000".substring(0,
                    Integer.parseInt(cuBranchLevelSchema.
                                     getBranchLevelProperty2()) -
                    mUpBranchAttr.length());
        }

        BigInteger tBigInteger1 = new BigInteger(maxBranchAttr2);
        String tCount = String.valueOf(mCount);
        if (tBigInteger1.compareTo(tBigInteger) > 0) {

            BigInteger tAdd = new BigInteger(tCount);
            tBigInteger1 = tBigInteger1.add(tAdd);
            maxBranchAttr = tBigInteger1.toString();
        } else {
            BigInteger tAdd = new BigInteger(tCount);
            tBigInteger = tBigInteger.add(tAdd);
            maxBranchAttr = tBigInteger.toString();

        }
        String tBranchSeries = "";
        tBranchSeries = AgentPubFun.getBranchSeries(mAimLABranchSchema.
                getAgentGroup());
        if (tBranchSeries == null || tBranchSeries.equals("")) {
            tBranchSeries = mAdjustLABranchSchema.getAgentGroup();
        } else {
            int SubLevel = upBranchLevelSchema.getBranchLevelID() -
                           cuBranchLevelSchema.getBranchLevelID();
            while (SubLevel > 1) {
                tBranchSeries = tBranchSeries + ":" +
                                "000000000000";
                SubLevel--;
            }
            tBranchSeries += ":" + mAdjustLABranchSchema.getAgentGroup();
        }
        System.out.println("maxBranchAttr:" + maxBranchAttr);
        NewBranchSeries = tBranchSeries;
        return maxBranchAttr;
    }

    private boolean dealBranchManager(LABranchGroupSchema cLABranchGroupSchema) {
        String upBranch = "";
        String upAgent = "";
        upBranch = mAimLABranchSchema.getAgentGroup();
        upAgent = mAimLABranchSchema.getBranchManager();
        LABranchGroupBSchema tLABranchGroupBSchema = new LABranchGroupBSchema();
        Reflections tReflections = new Reflections();
        tReflections.transFields(tLABranchGroupBSchema, cLABranchGroupSchema);
        tLABranchGroupBSchema.setEdorNo(mEdorNo);
        tLABranchGroupBSchema.setEdorType("02");
        tLABranchGroupBSchema.setOperator2(cLABranchGroupSchema.getOperator());
        tLABranchGroupBSchema.setOperator(mGlobalInput.Operator);
        tLABranchGroupBSchema.setMakeDate2(cLABranchGroupSchema.getMakeDate());
        tLABranchGroupBSchema.setMakeTime2(cLABranchGroupSchema.getMakeTime());
        tLABranchGroupBSchema.setModifyDate2(cLABranchGroupSchema.getModifyDate());
        tLABranchGroupBSchema.setModifyTime2(cLABranchGroupSchema.getModifyTime());
        tLABranchGroupBSchema.setMakeDate(currentDate);
        tLABranchGroupBSchema.setMakeTime(currentTime);
        tLABranchGroupBSchema.setModifyDate(currentDate);
        tLABranchGroupBSchema.setModifyTime(currentTime);
//        tLABranchGroupBSchema.setAStartDate(mAdjustDate);  //备份表不能修改日期 xiangchun 2005-12-16

        this.mLABranchGroupBSet.add(tLABranchGroupBSchema);
        String branchAttr = cLABranchGroupSchema.getBranchAttr();

        cLABranchGroupSchema.setBranchAttr(NewBranchAttr);
        cLABranchGroupSchema.setBranchSeries(NewBranchSeries);
        cLABranchGroupSchema.setName(mAdjustNewName);

        if (this.ifChangeManageCom) {
            cLABranchGroupSchema.setManageCom(mAimLABranchSchema.getManageCom());
        }
        cLABranchGroupSchema.setUpBranch(upBranch);
        cLABranchGroupSchema.setModifyDate(currentDate);
        cLABranchGroupSchema.setModifyTime(currentTime);
        cLABranchGroupSchema.setOperator(mGlobalInput.Operator);
        cLABranchGroupSchema.setAStartDate(mAdjustDate);
        this.mLABranchGroupSet.add(cLABranchGroupSchema);

        if (mBranchManager != null && !mBranchManager.equals("")) {
            if (ifChangeManageCom) {
                LAAgentSchema tLAAgentSchema = new LAAgentSchema();
                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(mBranchManager);
                tLAAgentDB.getInfo();
                tLAAgentSchema = tLAAgentDB.getSchema();
                LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
                tReflections.transFields(tLAAgentBSchema, tLAAgentSchema);
                tLAAgentBSchema.setEdorNo(mEdorNo);
                tLAAgentBSchema.setEdorType("02");
                tLAAgentBSchema.setMakeDate(currentDate);
                tLAAgentBSchema.setMakeTime(currentTime);
                this.mLAAgentBSet.add(tLAAgentBSchema);
                tLAAgentSchema.setManageCom(mAimLABranchSchema.getManageCom());
                tLAAgentSchema.setModifyDate(currentDate);
                tLAAgentSchema.setModifyTime(currentTime);
                tLAAgentSchema.setOperator(mGlobalInput.Operator);
                this.mLAAgentSet.add(tLAAgentSchema);
            }

            LATreeDB tLATreeDB = new LATreeDB();
            tLATreeDB.setAgentCode(mBranchManager);
            tLATreeDB.getInfo();
            LATreeBSchema tLATreeBSchema = new LATreeBSchema();
            tReflections.transFields(tLATreeBSchema, tLATreeDB.getSchema());
            tLATreeBSchema.setEdorNO(mEdorNo);
            tLATreeBSchema.setRemoveType("02");
            tLATreeBSchema.setMakeDate2(tLATreeDB.getMakeDate());
            tLATreeBSchema.setMakeTime2(tLATreeDB.getMakeTime());
            tLATreeBSchema.setModifyDate2(tLATreeDB.getModifyDate());
            tLATreeBSchema.setModifyTime2(tLATreeDB.getModifyTime());
            tLATreeBSchema.setOperator2(tLATreeDB.getOperator());
            tLATreeBSchema.setMakeDate(currentDate);
            tLATreeBSchema.setMakeTime(currentTime);
            tLATreeBSchema.setModifyDate(currentDate);
            tLATreeBSchema.setModifyTime(currentTime);
            tLATreeBSchema.setOperator(mGlobalInput.Operator);
            this.mLATreeBSet.add(tLATreeBSchema);

            tLATreeDB.setManageCom(mAimLABranchSchema.getManageCom());
            tLATreeDB.setUpAgent(upAgent);
            tLATreeDB.setModifyDate(currentDate);
            tLATreeDB.setAstartDate(mAdjustDate);
            tLATreeDB.setModifyTime(currentTime);
            tLATreeDB.setOperator(mGlobalInput.Operator);
            this.mLATreeSet.add(tLATreeDB.getSchema());

            LACommisionSet tLACommisionSet = new LACommisionSet();
            String aasql =
                    "select * from lacommision where agentcode='" +
                    mBranchManager + "' and branchtype='" +
                    mAdjustLABranchSchema.getBranchType() +
                    "' and branchType2='"
                    + mAdjustLABranchSchema.getBranchType2() +
                    "' and (caldate is null or caldate>='"
                    + this.mAdjustDate + "')";
            System.out.println(aasql);
            LACommisionDB tLACommisionDB = new LACommisionDB();
            tLACommisionSet = tLACommisionDB.executeQuery(aasql);
            for (int j = 1; j <= tLACommisionSet.size(); j++) {
                LACommisionSchema tLACommisionSchema = new LACommisionSchema();
                tLACommisionSchema = tLACommisionSet.get(j);
                String BranchSeries = tLACommisionSchema.getBranchSeries();
                System.out.println("BranchAttr1:" + BranchSeries);
                System.out.println("BranchAttr1:" + NewBranchSeries);
                BranchSeries = this.NewBranchSeries;
//                               BranchSeries.substring(NewBranchSeries.length());
                System.out.println("BranchAttr:" + NewBranchAttr);
                System.out.println(NewBranchAttr.substring(NewBranchAttr.length() ) );
                tLACommisionSchema.setBranchAttr(NewBranchAttr);
                tLACommisionSchema.setBranchSeries(BranchSeries);
                tLACommisionSchema.setManageCom(mAimLABranchSchema.getManageCom());
                tLACommisionSchema.setModifyDate(currentDate);
                tLACommisionSchema.setModifyTime(currentTime);
                this.mLACommisionSet.add(tLACommisionSchema);
            }
        }
        return true;

    }

    private boolean dealEachBranchGroup(LABranchGroupSchema
                                        cLABranchGroupSchema, String tFlag) {
        String curBranchAttr = cLABranchGroupSchema.getBranchAttr();
        Reflections tReflections = new Reflections();

        String sql = "select * from labranchgroup where branchattr like '" +
                     curBranchAttr + "%' and branchtype='" +
                     cLABranchGroupSchema.getBranchType() +
                     "' and BranchType2='" +
                     cLABranchGroupSchema.getBranchType2() +
                     "' and (state is null or state<>'1') and (endflag is null or endflag<>'Y') ";
        System.out.println(sql);
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(sql);
        for (int i = 1; i <= tLABranchGroupSet.size(); i++) {
            LABranchGroupSchema tLABranchGroupSchema = tLABranchGroupSet.get(i);
            LABranchGroupBSchema tLABranchGroupBSchema = new
                    LABranchGroupBSchema();
            tReflections.transFields(tLABranchGroupBSchema,
                                     tLABranchGroupSchema);
            tLABranchGroupBSchema.setEdorNo(mEdorNo);
            tLABranchGroupBSchema.setEdorType("02");
            tLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.
                                               getOperator());
            tLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.
                                               getMakeDate());
            tLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.
                                               getMakeTime());
            tLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.
                                                 getModifyDate());
            tLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.
                                                 getModifyTime());
            tLABranchGroupBSchema.setMakeDate(currentDate);
            tLABranchGroupBSchema.setMakeTime(currentTime);
            tLABranchGroupBSchema.setModifyDate(currentDate);
            tLABranchGroupBSchema.setModifyTime(currentTime);
            tLABranchGroupBSchema.setOperator(mGlobalInput.Operator);
            if (tFlag.equals("0")) {
                mLABranchGroupBSet.add(tLABranchGroupBSchema);
            }

            String branchAttr = tLABranchGroupSchema.getBranchAttr();
            String newBranchAttr = NewBranchAttr ;
            System.out.println("branchAttr:" + branchAttr);
            System.out.println("newBranchAttr:" + newBranchAttr);
            tLABranchGroupSchema.setBranchAttr(newBranchAttr);
            tLABranchGroupSchema.setName(mAdjustNewName); //这里是由界面传过来的隐藏新机构名称，如果并发可能产生问题
            String BranchSeries = tLABranchGroupSchema.getBranchSeries();
            BranchSeries = NewBranchSeries;
//                         BranchSeries.substring(NewBranchSeries.length());
            tLABranchGroupSchema.setManageCom(mAimLABranchSchema.getManageCom());
            tLABranchGroupSchema.setBranchSeries(BranchSeries);
            tLABranchGroupSchema.setModifyDate(currentDate);
            tLABranchGroupSchema.setModifyTime(currentTime);
            tLABranchGroupSchema.setOperator(mGlobalInput.Operator);
            tLABranchGroupSchema.setAStartDate(mAdjustDate);
            if (tFlag.equals("0")) {
                this.mLABranchGroupSet.add(tLABranchGroupSchema);
            }
            LACommisionSet tLACommisionSet = new LACommisionSet();
            String aasql =
                    "select * from lacommision where branchcode = '" +
                    tLABranchGroupSchema.getAgentGroup() +
                    "' and (caldate is null or caldate>='"
                    + this.mAdjustDate + "') and agentcode<>'" + mBranchManager +
                    "'";
            System.out.println(aasql);
            LACommisionDB tLACommisionDB = new LACommisionDB();
            tLACommisionSet = tLACommisionDB.executeQuery(aasql);
            for (int j = 1; j <= tLACommisionSet.size(); j++) {
                LACommisionSchema tLACommisionSchema = new LACommisionSchema();
                tLACommisionSchema = tLACommisionSet.get(j);
                String BranchAttr = tLACommisionSchema.getBranchAttr();
                tLACommisionSchema.setBranchAttr(newBranchAttr);
                tLACommisionSchema.setBranchSeries(BranchSeries);
                tLACommisionSchema.setManageCom(mAimLABranchSchema.getManageCom());
                tLACommisionSchema.setModifyDate(currentDate);
                tLACommisionSchema.setModifyTime(currentTime);
                this.mLACommisionSet.add(tLACommisionSchema);
            }
            System.out.println("AAAAAAAAAAAAAAAAAAAAAAAA" + ifChangeManageCom);
            if (ifChangeManageCom &&
                tLABranchGroupSchema.getBranchLevel().equals("01")) {
                String asql = "select * from laagent where branchcode ='" +
                              tLABranchGroupSchema.getAgentGroup() +
                              "' and agentstate<'06' ";
                if (mBranchManager != null && !mBranchManager.equals("")) {
                    asql = asql + "and agentcode<>'" + mBranchManager + "'";
                }
                System.out.println(asql);
                LAAgentSet tLAAgentSet = new LAAgentSet();
                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentSet = tLAAgentDB.executeQuery(asql);
                for (int j = 1; j <= tLAAgentSet.size(); j++) {
                    LAAgentSchema tLAAgentSchema = new LAAgentSchema();
                    tLAAgentSchema = tLAAgentSet.get(j);
                    System.out.println(j + "当前处理机构:" +
                                       tLABranchGroupSchema.getBranchAttr());
                    System.out.println(j + "当前处理人员:" +
                                       tLAAgentSchema.getAgentCode());

                    LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
                    tReflections.transFields(tLAAgentBSchema, tLAAgentSchema);
                    tLAAgentBSchema.setEdorNo(mEdorNo);
                    tLAAgentBSchema.setEdorType("02");
                    tLAAgentBSchema.setMakeDate(currentDate);
                    tLAAgentBSchema.setMakeTime(currentTime);
                    tLAAgentBSchema.setModifyDate(currentDate);
                    tLAAgentBSchema.setModifyTime(currentTime);
                    tLAAgentBSchema.setOperator(mGlobalInput.Operator);
                    this.mLAAgentBSet.add(tLAAgentBSchema);

                    tLAAgentSchema.setManageCom(mAimLABranchSchema.getManageCom());
                    tLAAgentSchema.setModifyDate(currentDate);
                    tLAAgentSchema.setModifyTime(currentTime);
                    tLAAgentSchema.setOperator(mGlobalInput.Operator);
                    this.mLAAgentSet.add(tLAAgentSchema);
                    LATreeDB tLATreeDB = new LATreeDB();
                    tLATreeDB.setAgentCode(tLAAgentSchema.getAgentCode());
                    tLATreeDB.getInfo();
                    LATreeBSchema tLATreeBSchema = new LATreeBSchema();
                    tReflections.transFields(tLATreeBSchema,
                                             tLATreeDB.getSchema());
                    tLATreeBSchema.setEdorNO(mEdorNo);
                    tLATreeBSchema.setRemoveType("02");
                    tLATreeBSchema.setMakeDate2(tLATreeDB.getMakeDate());
                    tLATreeBSchema.setMakeTime2(tLATreeDB.getMakeTime());
                    tLATreeBSchema.setModifyDate2(tLATreeDB.getModifyDate());
                    tLATreeBSchema.setModifyTime2(tLATreeDB.getModifyTime());
                    tLATreeBSchema.setOperator2(tLATreeDB.getOperator());
                    tLATreeBSchema.setMakeDate(currentDate);
                    tLATreeBSchema.setMakeTime(currentTime);
                    tLATreeBSchema.setModifyDate(currentDate);
                    tLATreeBSchema.setModifyTime(currentTime);
                    tLATreeBSchema.setOperator(mGlobalInput.Operator);
                    this.mLATreeBSet.add(tLATreeBSchema);
                    tLATreeDB.setManageCom(mAimLABranchSchema.getManageCom());
                    tLATreeDB.setAstartDate(mAdjustDate);
                    tLATreeDB.setModifyDate(currentDate);
                    tLATreeDB.setModifyTime(currentTime);
                    tLATreeDB.setOperator(mGlobalInput.Operator);
                    this.mLATreeSet.add(tLATreeDB);
                }
            }
        }
        return true;
    }

    private boolean chgRelation() {
        //处理育成培养关系
//        LARearPICCHBL tLARearPICCHBL = new LARearPICCHBL();
//        tLARearPICCHBL.setLATreeSchema(mLATreeSet.get(1));
//        if (!tLARearPICCHBL.invalidateRear(IsSingle,mAdjustDate)) {
//            mErrors.copyAllErrors(tLARearPICCHBL.mErrors);
//            return false;
//        }
//        MMap tMap2 = tLARearPICCHBL.getResult();
//        mMap.add(tMap2);
//
//        LARecomPICCHBL tLARecomPICCHBL = new LARecomPICCHBL();
//        tLARecomPICCHBL.setLATreeSchema(mLATreeSet.get(1));
//        if (!tLARecomPICCHBL.invalidateRecom(false,mAdjustDate)) {
//            mErrors.copyAllErrors(tLARecomPICCHBL.mErrors);
//            return false;
//        }
//        MMap tMap1 = tLARecomPICCHBL.getResult();
//        mMap.add(tMap1);
//        //失效与推荐人的关系

        String tBranchlevel = mAdjustLABranchSchema.getBranchLevel(); //得到被调动机构的级别

        if (IsSingle) {
            //断裂原有关系
            LARearRelationSet tLARearRelationSet = new LARearRelationSet();
            LARearRelationDB tLARearRelationDB = new LARearRelationDB();
            //查询本人为被育成人的直接培养关系
            String sql = "select  * from larearrelation  where agentcode= '" +
                         mLATreeSet.get(1).getAgentCode() + "'  ";
            tLARearRelationSet = tLARearRelationDB.executeQuery(sql);
            mLARearRelationSet.add(tLARearRelationSet);
            tLARearRelationSet = new LARearRelationSet();
            //查询本人为育成人的直接培养关系
            sql = "select  * from larearrelation  where rearagentcode= '" +
                  mLATreeSet.get(1).getAgentCode() + "'  ";
            tLARearRelationSet = tLARearRelationDB.executeQuery(sql);
            mLARearRelationSet.add(tLARearRelationSet);

            if (mLARearRelationSet != null && mLARearRelationSet.size() >= 1) {
                for (int i = 1; i <= mLARearRelationSet.size(); i++) {
                    Reflections tReflections = new Reflections();
                    LARearRelationBSchema tLARearRelationBSchema = new
                            LARearRelationBSchema();
                    tReflections.transFields(tLARearRelationBSchema,
                                             mLARearRelationSet.get(i));
                    tLARearRelationBSchema.setMakeDate2(mLARearRelationSet.get(
                            i).
                            getMakeDate());
                    tLARearRelationBSchema.setMakeTime2(mLARearRelationSet.get(
                            i).
                            getModifyTime());
                    tLARearRelationBSchema.setModifyDate2(mLARearRelationSet.
                            get(i).
                            getModifyDate());
                    tLARearRelationBSchema.setModifyTime2(mLARearRelationSet.
                            get(i).
                            getModifyTime());
                    tLARearRelationBSchema.setOperator2(mLARearRelationSet.get(
                            i).
                            getOperator());

                    tLARearRelationBSchema.setMakeDate(currentDate);
                    tLARearRelationBSchema.setMakeTime(currentTime);
                    tLARearRelationBSchema.setModifyDate(currentDate);
                    tLARearRelationBSchema.setModifyTime(currentTime);
                    tLARearRelationBSchema.setOperator(mGlobalInput.Operator);

                    tLARearRelationBSchema.setEdorNo(mEdorNo);
                    tLARearRelationBSchema.setEdorType("02");
                    mLARearRelationBSet.add(tLARearRelationBSchema);
                }
            }
        } else {
            //带走原有关系

            LARearRelationSet tLARearRelationSet = new LARearRelationSet();
            LARearRelationDB tLARearRelationDB = new LARearRelationDB();
            //查询本人为被育成人的培养关系
            String sql = "select  * from larearrelation  where agentcode= '" +
                         mLATreeSet.get(1).getAgentCode() + "'  ";
            tLARearRelationSet = tLARearRelationDB.executeQuery(sql);

            mLARearRelationSet.add(tLARearRelationSet);
            //查询与此人有关的间接培养关系
            tLARearRelationSet = new LARearRelationSet();
            sql = "select * from larearrelation  where rearagentcode in "
                  + "(select rearagentcode from larearrelation b "
                  + "where  b.agentcode='" + mLATreeSet.get(1).getAgentCode() +
                  "') "
                  +
                  "and agentcode in (select agentcode from larearrelation b "
                  + "where  b.rearagentcode='" + mLATreeSet.get(1).getAgentCode() +
                  "')";
            tLARearRelationSet = tLARearRelationDB.executeQuery(sql);
            mLARearRelationSet.add(tLARearRelationSet);
            if (mLARearRelationSet != null && mLARearRelationSet.size() >= 1) {
                for (int i = 1; i <= mLARearRelationSet.size(); i++) {
                    Reflections tReflections = new Reflections();
                    LARearRelationBSchema tLARearRelationBSchema = new
                            LARearRelationBSchema();
                    tReflections.transFields(tLARearRelationBSchema,
                                             mLARearRelationSet.get(i));
                    tLARearRelationBSchema.setMakeDate2(mLARearRelationSet.get(
                            i).
                            getMakeDate());
                    tLARearRelationBSchema.setMakeTime2(mLARearRelationSet.get(
                            i).
                            getModifyTime());
                    tLARearRelationBSchema.setModifyDate2(mLARearRelationSet.
                            get(i).
                            getModifyDate());
                    tLARearRelationBSchema.setModifyTime2(mLARearRelationSet.
                            get(i).
                            getModifyTime());
                    tLARearRelationBSchema.setOperator2(mLARearRelationSet.get(
                            i).
                            getOperator());

                    tLARearRelationBSchema.setMakeDate(currentDate);
                    tLARearRelationBSchema.setMakeTime(currentTime);
                    tLARearRelationBSchema.setModifyDate(currentDate);
                    tLARearRelationBSchema.setModifyTime(currentTime);
                    tLARearRelationBSchema.setOperator(mGlobalInput.Operator);

                    tLARearRelationBSchema.setEdorNo(mEdorNo);
                    tLARearRelationBSchema.setEdorType("02");
                    mLARearRelationBSet.add(tLARearRelationBSchema);

                }
            }

            //调走其所培养的团队
            tLARearRelationSet = new LARearRelationSet();
            tLARearRelationDB = new LARearRelationDB();
            //查询本人为育成人的培养关系,这些团队需要调动团队
            sql = "select  * from larearrelation  where rearagentcode= '" +
                  mLATreeSet.get(1).getAgentCode() + "' and rearlevel= '" +
                  tBranchlevel + "'";
            tLARearRelationSet = tLARearRelationDB.executeQuery(sql);
            System.out.println(sql);

            if (tLARearRelationSet != null && tLARearRelationSet.size() >= 1) {
                for (int i = 1; i <= tLARearRelationSet.size(); i++) {

                    LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
                    LABranchGroupSchema tLABranchGroupSchema = new
                            LABranchGroupSchema();
                    tLABranchGroupDB.setAgentGroup(tLARearRelationSet.get(i).
                            getAgentGroup());
                    tLABranchGroupDB.getInfo();
                    tLABranchGroupSchema = tLABranchGroupDB.getSchema();
                    mAdjustLABranchSchema = new LABranchGroupSchema();
                    mAdjustLABranchSchema.setAgentGroup(tLABranchGroupSchema.
                            getAgentGroup());
                    mAdjustLABranchSchema.setBranchLevel(tLABranchGroupSchema.
                            getBranchLevel());
                    mAdjustLABranchSchema.setUpBranch(tLABranchGroupSchema.
                            getUpBranch());
                    mAdjustLABranchSchema.setBranchAttr(tLABranchGroupSchema.
                            getBranchAttr()); //增加了修改此属性
                    mAdjustLABranchSchema.setBranchType(mBranchType);
                    mAdjustLABranchSchema.setBranchType2(mBranchType2);
                    if (!check()) {
                        return false;
                    }
                    if (!dealData()) {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "ChangeBranchNewBL";
                        tError.functionName = "chgRelation";
                        tError.errorMessage =
                                "数据处理失败ChangeBranchNewBL-->dealData!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
            }

        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            this.mInputData = new VData();
            this.mMap.put(mLABranchGroupSet, "UPDATE");
            this.mMap.put(mLABranchGroupBSet, "INSERT");
            this.mMap.put(mLATreeSet, "UPDATE");
            this.mMap.put(mLATreeBSet, "INSERT");
            this.mMap.put(mLAAgentBSet, "INSERT");
            this.mMap.put(mLAAgentSet, "UPDATE");
            this.mMap.put(mLACommisionSet, "UPDATE");
            this.mMap.put(mLARearRelationSet, "DELETE");
            this.mMap.put(mLARearRelationBSet, "INSERT");
            this.mInputData.add(this.mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ChangeBranchNewBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        mResult = new VData();
        mResult.add(NewBranchAttr);
        return this.mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mAdjustLABranchSchema.setSchema((LABranchGroupSchema) cInputData.
                                        getObjectByObjectName(
                                                "LABranchGroupSchema", 1));
        mAimLABranchSchema.setSchema((LABranchGroupSchema) cInputData.
                                     getObjectByObjectName(
                                             "LABranchGroupSchema", 2));
        mAdjustDate = (String) cInputData.getObject(3);
        TransferData tdata = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mAdjustNewName = (String) tdata.getValueByName("AdjustNewName");
//        IsSingle = ts.equals("0") ? true : false;
        mBranchType = mAdjustLABranchSchema.getBranchType();
        mBranchType2 = mAdjustLABranchSchema.getBranchType2();
        System.out.println("mAdjustDate:" + mAdjustDate);
        System.out.println("over getInputData.." +
                           mAdjustLABranchSchema.getAgentGroup());
        return true;
    }
}
