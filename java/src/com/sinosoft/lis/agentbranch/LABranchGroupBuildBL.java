/*
 * <p>ClassName: LABranchGroupBuildBL </p>
 * <p>Description: LABranchGroupBuildBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2013-05-17
 */
package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;





public class LABranchGroupBuildBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    /** 全局变量 */
    private LABranchGroupSchema mLABranchGroupSchema = new LABranchGroupSchema();
    /** 需修改变量 */
    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet(); 
    private LABranchGroupBSet mLABranchGroupBSet = new LABranchGroupBSet();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LATreeBSet mLATreeBSet = new LATreeBSet();

    public LABranchGroupBuildBL()
    {
    }

    public static void main(String[] args)
    {
    }


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!check())
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABranchGroupBuildBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LABranchGroupBuildBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LABranchGroupBuildBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
   private boolean check()
   {	  
	   String tFoundDate = AgentPubFun.formatDate(this.mLABranchGroupSchema.getFoundDate(),"yyyy-MM-dd");
	   String tApplyGBFlag =this.mLABranchGroupSchema.getApplyGBFlag();// 申报标志
	   String tApplyGBStartDate = AgentPubFun.formatDate(this.mLABranchGroupSchema.getApplyGBStartDate(),"yyyy-MM-dd");// 申报起始日期
	   String tGBuildFlag = this.mLABranchGroupSchema.getGBuildFlag();// 团队建设类型
	   String tDay1 = tApplyGBStartDate.substring(tApplyGBStartDate.lastIndexOf("-") + 1);//获得天数
	   
	   
       //1、"申报标志"是"团队建设"的必要条件
	   if(tApplyGBFlag==null||tApplyGBFlag.equals(""))
	   {
		   CError tError = new CError();
           tError.moduleName = "LABranchGroupBuildBL";
           tError.functionName = "check";
           tError.errorMessage = "申报标志是团队建设的必要条件,页面需先录入申报标志!";
           this.mErrors.addOneError(tError);
           return false;
	   }
	   //2、申报标志开始日期必须大于等于2013-4-1
	   if(tApplyGBStartDate.compareTo("2013-04-01")<0)
	   {
		   CError tError = new CError();
           tError.moduleName = "LABranchGroupBuildBL";
           tError.functionName = "check";
           tError.errorMessage = "申报标志录入日期必须大于等于2013-04-01日!";
           this.mErrors.addOneError(tError);
           return false;
	   }
	   if(tApplyGBStartDate.compareTo("2014-04-01")>=0)
	   {
		   CError tError = new CError();
           tError.moduleName = "LABranchGroupBuildBL";
           tError.functionName = "check";
           tError.errorMessage = "申报标志录入日期必须小于2014-04-01日!";
           this.mErrors.addOneError(tError);
           return false;
	   }
	   //3、申报标志录入时间应大于等于营业部成立日期
	   if(tFoundDate.compareTo(tApplyGBStartDate)<=0)
	   {
		   if(tFoundDate.compareTo(tApplyGBStartDate)<0)
		   {
			   // 申报标志起期如果不等于该营业部成立日期，则必须为每个月1日
			   if(!"01".equals(tDay1))
			   {
				   CError tError = new CError();
		           tError.moduleName = "LABranchGroupBuildBL";
		           tError.functionName = "check";
		           tError.errorMessage = "申报标志起期如果不等于该营业部成立日期，则必须为每个月1日!";
		           this.mErrors.addOneError(tError);
		           return false;
			   }
			   if(this.mLABranchGroupSchema.getGBuildStartDate()!=null&&!this.mLABranchGroupSchema.getGBuildStartDate().equals(""))
			   {
				   String tGBuildStartDate = AgentPubFun.formatDate(this.mLABranchGroupSchema.getGBuildStartDate(),"yyyy-MM-dd");// 团队建设起始日期
				   // 4、团队建设录入起期必须大于等于申报标志录入起期
				   if(tApplyGBStartDate.compareTo(tGBuildStartDate)<=0)
				   {
					   if(tApplyGBStartDate.compareTo(tGBuildStartDate)<0)
					   {
						   String tDay2 = tGBuildStartDate.substring(tGBuildStartDate.lastIndexOf("-") + 1);//获得天数
						   if(!"01".equals(tDay2))
						   {
							   CError tError = new CError();
					           tError.moduleName = "LABranchGroupBuildBL";
					           tError.functionName = "check";
					           tError.errorMessage = "团队建设起期如果不等于该营业部成立日期，则必须为每个月1日!";
					           this.mErrors.addOneError(tError);
					           return false;
						   }
						   //如营业部申报标志录入起期为2013-4-1日，“团队建设”录入起期只能为2013-4-1日或2013-5-1日
						  if("2013-04-01".equals(tApplyGBStartDate))
						  {
							  if(!"2013-05-01".equals(tGBuildStartDate))
							  {
								  CError tError = new CError();
						           tError.moduleName = "LABranchGroupBuildBL";
						           tError.functionName = "check";
						           tError.errorMessage = "营业部申报标志录入起期为2013-4-1日，团队建设录入起期只能为2013-4-1日或2013-5-1日!";
						           this.mErrors.addOneError(tError);
						           return false;  
							  }
						  }else{
							  CError tError = new CError();
					           tError.moduleName = "LABranchGroupBuildBL";
					           tError.functionName = "check";
					           tError.errorMessage = "营业部申报标志录入起期若大于等于2013-5-1日，团队建设录入起期只能与申报标志录入起期一致!";
					           this.mErrors.addOneError(tError);
					           return false;  							  
						  }  
					   }
				   }else{
					   CError tError = new CError();
			           tError.moduleName = "LABranchGroupBuildBL";
			           tError.functionName = "check";
			           tError.errorMessage = "团队建设录入起期必须大于等于申报标志录入起期!";
			           this.mErrors.addOneError(tError);
			           return false;
					   
				   }
				  String tsql ="select enddate from LASTATSEGMENT where char(yearmonth) = db2inst1.DATE_FORMAT(((date('"+tGBuildStartDate+"') + 1 year) - 1 month),'yyyymm') and  stattype = '1'";
				  ExeSQL tExeSQL = new ExeSQL();
				  String tResult = tExeSQL.getOneValue(tsql);
				  if(tResult==null||tResult.equals(""))
				  {
					   CError tError = new CError();
			           tError.moduleName = "LABranchGroupBuildBL";
			           tError.functionName = "check";
			           tError.errorMessage = "团队建设起期格式录入有误!";
			           this.mErrors.addOneError(tError);
			           return false;  
				  }
				  this.mLABranchGroupSchema.setGBuildEndDate(tResult);
			   }else{
				   if(tApplyGBStartDate.compareTo("2013-06-01")>=0)
				   {
					   CError tError = new CError();
			           tError.moduleName = "LABranchGroupBuildBL";
			           tError.functionName = "check";
			           tError.errorMessage = "如果营业部只申请申报标志,则申报标志起期需小于2013-06-01!";
			           this.mErrors.addOneError(tError);
			           return false; 
				   }
				   
			   }
			   
		   }

		   if(this.mLABranchGroupSchema.getGBuildStartDate()!=null&&!this.mLABranchGroupSchema.getGBuildStartDate().equals(""))
		   {
			   String tGBuildStartDate = AgentPubFun.formatDate(this.mLABranchGroupSchema.getGBuildStartDate(),"yyyy-MM-dd");// 团队建设起始日期
			   // 4、团队建设录入起期必须大于等于申报标志录入起期
			   if(tApplyGBStartDate.compareTo(tGBuildStartDate)<=0)
			   {
				   if(tApplyGBStartDate.compareTo(tGBuildStartDate)<0)
				   {
					   String tDay2 = tGBuildStartDate.substring(tGBuildStartDate.lastIndexOf("-") + 1);//获得天数
					   if(!"01".equals(tDay2))
					   {
						   CError tError = new CError();
				           tError.moduleName = "LABranchGroupBuildBL";
				           tError.functionName = "check";
				           tError.errorMessage = "团队建设起期如果不等于该营业部成立日期，则必须为每个月1日!";
				           this.mErrors.addOneError(tError);
				           return false;
					   }
					   //如营业部申报标志录入起期为2013-4-1日，“团队建设”录入起期只能为2013-4-1日或2013-5-1日
					  if(tApplyGBStartDate.compareTo("2013-04-01")>=0&&tApplyGBStartDate.compareTo("2013-05-01")<0)
					  {
						  if(!"2013-05-01".equals(tGBuildStartDate))
						  {
							  CError tError = new CError();
					           tError.moduleName = "LABranchGroupBuildBL";
					           tError.functionName = "check";
					           tError.errorMessage = "营业部申报标志录入起期为"+tApplyGBStartDate+"，团队建设录入起期只能为2013-4-1日或2013-5-1日,且需大于等于申报标志录入起期!";
					           this.mErrors.addOneError(tError);
					           return false;  
						  }
					  }else{
						  CError tError = new CError();
				           tError.moduleName = "LABranchGroupBuildBL";
				           tError.functionName = "check";
				           tError.errorMessage = "营业部申报标志录入起期若大于2013-5-1日，团队建设录入起期只能与申报标志录入起期一致!";
				           this.mErrors.addOneError(tError);
				           return false;  							  
					  }  
				   }
			   }else{
				   CError tError = new CError();
		           tError.moduleName = "LABranchGroupBuildBL";
		           tError.functionName = "check";
		           tError.errorMessage = "团队建设录入起期必须大于等于申报标志录入起期!";
		           this.mErrors.addOneError(tError);
		           return false;
				   
			   }
			  String tsql = "select enddate from LASTATSEGMENT where char(yearmonth) = db2inst1.DATE_FORMAT(((date('"+tGBuildStartDate+"') + 1 year) - 1 month),'yyyymm') and  stattype = '1'";
			  ExeSQL tExeSQL = new ExeSQL();
			  String tResult = tExeSQL.getOneValue(tsql);
			  if(tResult==null||tResult.equals(""))
			  {
				   CError tError = new CError();
		           tError.moduleName = "LABranchGroupBuildBL";
		           tError.functionName = "check";
		           tError.errorMessage = "团队建设起期格式录入有误!";
		           this.mErrors.addOneError(tError);
		           return false;  
			  }
			  this.mLABranchGroupSchema.setGBuildEndDate(tResult);
		   }else{  
			   if(tApplyGBStartDate.compareTo("2013-06-01")>=0)
			   {
				   CError tError = new CError();
		           tError.moduleName = "LABranchGroupBuildBL";
		           tError.functionName = "check";
		           tError.errorMessage = "如果营业部只申请申报标志,则申报标志起期需小于2013-06-01!";
		           this.mErrors.addOneError(tError);
		           return false; 
			   }
		   }
	   }else{
		   CError tError = new CError();
           tError.moduleName = "LABranchGroupBuildBL";
           tError.functionName = "check";
           tError.errorMessage = "该营业部成立日期为"+tFoundDate+",申报标志录入时间应大于等于营业部成立日期!";
           this.mErrors.addOneError(tError);
           return false; 
	   }
       return true;
   }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
    	LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        // 团队信息修改
    	String sql = "select * from labranchgroup where 1=1 and branchtype ='"+this.mLABranchGroupSchema.getBranchType()+"'" +
    			" and branchtype2 = '"+this.mLABranchGroupSchema.getBranchType2()+"' and endflag ='N' and branchseries like '"+this.mLABranchGroupSchema.getAgentGroup()+"%' ";
    	tLABranchGroupSet =tLABranchGroupDB.executeQuery(sql);
    	String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
    	for(int i = 1;i<=tLABranchGroupSet.size();i++)
    	{
    		LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
    		tLABranchGroupSchema =tLABranchGroupSet.get(i).getSchema();
    		LABranchGroupBSchema tLABranchGroupBSchema = new LABranchGroupBSchema();
    		Reflections tReflections = new Reflections();
    		tReflections.transFields(tLABranchGroupBSchema, tLABranchGroupSchema);
    		// 简单日期调整
    		tLABranchGroupBSchema.setOperator2(tLABranchGroupSchema.getOperator());
    		tLABranchGroupBSchema.setMakeDate2(tLABranchGroupSchema.getMakeDate());
    		tLABranchGroupBSchema.setMakeTime2(tLABranchGroupSchema.getMakeTime());
    		tLABranchGroupBSchema.setModifyDate2(tLABranchGroupSchema.getModifyDate());
    		tLABranchGroupBSchema.setModifyTime2(tLABranchGroupSchema.getModifyTime());
    		tLABranchGroupBSchema.setOperator(this.mGlobalInput.Operator);
    		tLABranchGroupBSchema.setMakeDate(this.currentDate);
    		tLABranchGroupBSchema.setMakeTime(this.currentTime);
    		tLABranchGroupBSchema.setModifyDate(this.currentDate);
    		tLABranchGroupBSchema.setModifyTime(this.currentTime);
    		tLABranchGroupBSchema.setEdorType("10");
    		tLABranchGroupBSchema.setEdorNo(tEdorNo);
    		this.mLABranchGroupBSet.add(tLABranchGroupBSchema);
    		
    		// add new 增加团队成立日期校验
    		tLABranchGroupSchema.setApplyGBFlag(this.mLABranchGroupSchema.getApplyGBFlag());    		
    		tLABranchGroupSchema.setGBuildFlag(this.mLABranchGroupSchema.getGBuildFlag());
    		
    		tLABranchGroupSchema.setApplyGBStartDate(this.mLABranchGroupSchema.getApplyGBStartDate());
    		String tFoundDate = AgentPubFun.formatDate(tLABranchGroupSchema.getFoundDate(),"yyyy-MM-dd");
    		String tApplyGBStartDate =AgentPubFun.formatDate(this.mLABranchGroupSchema.getApplyGBStartDate(),"yyyy-MM-dd");
    		if(tFoundDate.compareTo(tApplyGBStartDate)>=0)
    		{
    			//如果团队申报标志日期小于团队成立日期，则默认申报标志日期为其团队成立日期
    			tLABranchGroupSchema.setApplyGBStartDate(tFoundDate);    			
    		}   
    		// 成立日期大于等于2014-04-01日成立的团队，不计入团建中
            if(tFoundDate.compareTo("2014-04-01")>=0)
            {
            	continue;
            }
    		tLABranchGroupSchema.setGBuildStartDate(this.mLABranchGroupSchema.getGBuildStartDate());
    		tLABranchGroupSchema.setGBuildEndDate(this.mLABranchGroupSchema.getGBuildEndDate());
    		tLABranchGroupSchema.setOperator(this.mGlobalInput.Operator);
    		tLABranchGroupSchema.setModifyDate(this.currentDate);
    		tLABranchGroupSchema.setModifyTime(this.currentTime);
    		if(tLABranchGroupSchema.getGBuildFlag()!=null&&!"".equals(tLABranchGroupSchema.getGBuildFlag()))
    		{
    			//日期部分调整
    		 String tGBuildStartDate = AgentPubFun.formatDate(tLABranchGroupSchema.getGBuildStartDate(),"yyyy-MM-dd");
    		 if(tFoundDate.compareTo(tGBuildStartDate)>=0)
    		 {
    			 tLABranchGroupSchema.setGBuildStartDate(tFoundDate);
    			 String tGBuildEndDate = getGBuildEnd(tFoundDate);
    			 if(tGBuildEndDate==null||tGBuildEndDate.equals(""))
    			 {
    			   CError tError = new CError();
  		           tError.moduleName = "LABranchGroupBuildBL";
  		           tError.functionName = "check";
  		           tError.errorMessage = "团队建设起期格式录入有误!";
  		           this.mErrors.addOneError(tError);
  		           return false; 
    			 }
    			 tLABranchGroupSchema.setGBuildEndDate(tGBuildEndDate);
    		 }
    		 if(!dealAgent(tLABranchGroupSchema,tEdorNo))
    		   {
    			 return false;
    		   }
    		}
    		this.mLABranchGroupSet.add(tLABranchGroupSchema);
    		
    		
    	}
    	map.put(mLABranchGroupBSet, "INSERT");
    	map.put(mLABranchGroupSet, "UPDATE");
     	map.put(mLAAgentBSet, "INSERT");
    	map.put(mLAAgentSet, "UPDATE");
    	map.put(mLATreeBSet, "INSERT");
    	map.put(mLATreeSet, "UPDATE");
        
        return true;
    }

    /**
     *  处理标记团队建设下属业务员
     * 
     */
     private boolean dealAgent(LABranchGroupSchema tLABranchGroupSchema,String tEdorNo)
     {
    	 Reflections tReflections = new Reflections();
    	 LAAgentDB tLAAgentDB = new LAAgentDB();
    	 String getAgent = "select * from laagent where agentgroup = '"+tLABranchGroupSchema.getAgentGroup()+"' and agentstate <'06'";

    	 LAAgentSet tLAAgentSet = new LAAgentSet();
    	 tLAAgentSet = tLAAgentDB.executeQuery(getAgent);
    	 if(tLAAgentSet.size()>=1)
    	 {
    		 for(int i = 1;i<=tLAAgentSet.size();i++)
    		 {
    			 LAAgentSchema tLAAgentSchema = new LAAgentSchema();
    			 tLAAgentSchema = tLAAgentSet.get(i).getSchema();
                 System.out.println("--->>"+tLAAgentSchema.getEmployDate());
    			 if(tLAAgentSchema.getEmployDate().compareTo("2014-04-01")>=0)
    			 {
    				 continue;
    			 }
    			 // 处理latree 表数据
    			 String sql = "select * from latree where agentcode = '"+tLAAgentSchema.getAgentCode()+"'";
    			 LATreeDB tLATreeDB = new LATreeDB();
    			 LATreeSet tLATreeSet = new LATreeSet();
    			 tLATreeSet =tLATreeDB.executeQuery(sql);
    			 for(int j = 1;j<=tLATreeSet.size();j++)
    			 {
    				 LATreeSchema tLATreeSchema = new LATreeSchema();
    				 tLATreeSchema =tLATreeSet.get(j).getSchema();
    				 LATreeBSchema tLATreeBSchema = new LATreeBSchema();
    				 tReflections.transFields(tLATreeBSchema, tLATreeSchema);
    				 tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
    		         tLATreeBSchema.setOperator(mGlobalInput.Operator);
    		         tLATreeBSchema.setEdorNO(tEdorNo);
    		         tLATreeBSchema.setRemoveType("05");// 团建设录入
    		         tLATreeBSchema.setMakeDate2(tLATreeBSchema.getMakeDate());
    		         tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
    		         tLATreeBSchema.setModifyDate2(tLATreeBSchema.getModifyDate());
    		         tLATreeBSchema.setModifyTime2(tLATreeBSchema.getModifyTime());
    		         tLATreeBSchema.setMakeDate(currentDate);
    		         tLATreeBSchema.setMakeTime(currentTime);
    		         tLATreeBSchema.setModifyDate(currentDate);
    		         tLATreeBSchema.setModifyTime(currentTime);
    		         this.mLATreeBSet.add(tLATreeBSchema);// 增加latreeb 表存储
    		         
    		         if(tLATreeSchema.getWageVersion()!=null&&!tLATreeSchema.getWageVersion().equals("2013A"))
    		         {
    		        	 tLATreeSchema.setWageVersion("2013B");// 增加薪资版本信息 如之前已经申请筹备，则默认不修改薪资版本
    		         }
    		         tLATreeSchema.setModifyDate(currentDate);
    		         tLATreeSchema.setModifyTime(currentTime);
    		         tLATreeSchema.setOperator(mGlobalInput.Operator);
    		         this.mLATreeSet.add(tLATreeSchema);
    			 }
    			 LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
    			
    			 tReflections.transFields(tLAAgentBSchema, tLAAgentSchema);
    			 tLAAgentBSchema.setEdorType("05");
    			 tLAAgentBSchema.setEdorNo(tEdorNo);
    			 this.mLAAgentBSet.add(tLAAgentBSchema);
    			 
    			 tLAAgentSchema.setGBuildFlag(tLABranchGroupSchema.getGBuildFlag());
    			 String tEmploydate = AgentPubFun.formatDate(tLAAgentSchema.getEmployDate(),"yyyy-MM-dd");
    			 String tGBuildStartDate =AgentPubFun.formatDate(tLABranchGroupSchema.getGBuildStartDate(),"yyyy-MM-dd");
    			 tLAAgentSchema.setGBuildStartDate(tLABranchGroupSchema.getGBuildStartDate());
    			 tLAAgentSchema.setGBuildEndDate(tLABranchGroupSchema.getGBuildEndDate());
    			 if(tEmploydate.compareTo(tGBuildStartDate)>=0)
    			 {
    				 tLAAgentSchema.setGBuildStartDate(tEmploydate);
    				 String tEndDate = getGBuildEnd(tEmploydate);
    				 if(tEndDate==null||tEndDate.equals(""))
        			 {
        			   CError tError = new CError();
      		           tError.moduleName = "LABranchGroupBuildBL";
      		           tError.functionName = "check";
      		           tError.errorMessage = "团队建设起期格式录入有误!";
      		           this.mErrors.addOneError(tError);
      		           return false; 
        			 }
    				 tLAAgentSchema.setGBuildEndDate(tEndDate);
    			 }
    			 tLAAgentSchema.setModifyDate(this.currentDate);
    			 tLAAgentSchema.setModifyTime(this.currentTime);
    			 if(tLAAgentSchema.getWageVersion()!=null&&!tLAAgentSchema.getWageVersion().equals("2013A"))
    			 {
        			 tLAAgentSchema.setWageVersion("2013B");// 增加薪资版本信息 如之前已经申请筹备，则默认不修改薪资版本
    			 }
    			 if(tLAAgentSchema.getWageVersion()!=null&&tLAAgentSchema.getWageVersion().equals("2013A"))
    			 {
    				 tLAAgentSchema.setGBuildFlag("");
    				 tLAAgentSchema.setGBuildStartDate("");
    				 tLAAgentSchema.setGBuildEndDate("");
    			 }
    			 tLAAgentSchema.setOperator(this.mGlobalInput.Operator);
    			 mLAAgentSet.add(tLAAgentSchema);

    		 }
    	 }

    	 return true;
     }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLABranchGroupSchema.setSchema((LABranchGroupSchema) cInputData.
                                            getObjectByObjectName(
                "LABranchGroupSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        if (this.mGlobalInput == null)
        {
            CError tError = new CError();
            tError.moduleName = "LABranchGroupBuildBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "mGlobalInput为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (this.mLABranchGroupSchema == null)
        {
            CError tError = new CError();
            tError.moduleName = "LABranchGroupBuildBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "mLABranchGroupSchema为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }




    private boolean prepareOutputData()
    {
        try
        {
        	mInputData.clear();
        	mInputData.add(map);
           
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABranchGroupBuildBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
  
    public VData getResult()
    {
        return this.mResult;
    }
    /**
     *  得到考核截止日期
     * @param GBuildStartDate
     * @return
     */
    private String getGBuildEnd(String GBuildStartDate)
    {
    	String tGBuildEndDate ="";
    	 String tsql = "select enddate from LASTATSEGMENT where char(yearmonth) = db2inst1.DATE_FORMAT(((date('"+GBuildStartDate+"') + 1 year) - 1 month),'yyyymm') and  stattype = '1'";
		  ExeSQL tExeSQL = new ExeSQL();
		  tGBuildEndDate =tExeSQL.getOneValue(tsql);
		  if(tGBuildEndDate==null||tGBuildEndDate.equals(""))
		  {
			   CError tError = new CError();
	           tError.moduleName = "LABranchGroupBuildBL";
	           tError.functionName = "check";
	           tError.errorMessage = "团队建设起期格式录入有误!";
	           this.mErrors.addOneError(tError);
	           return "";  
		  }
    	return tGBuildEndDate;
    }

}
