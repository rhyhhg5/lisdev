/*
 * <p>ClassName: AdjustAgentBL </p>
 * <p>Description: AdjustAgentBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-27
 */
package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeAccessoryDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeAccessorySchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LATreeAccessoryBSet;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LABranchChangeTempSchema;
import com.sinosoft.lis.vschema.LABranchChangeTempSet;
import com.sinosoft.lis.db.LABranchChangeTempDB;
import com.sinosoft.lis.schema.LATreeTempSchema;

public class ALAGrpBranchManagerTempBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;
    private VData mInInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String mOperate;
    private String tEdorNo;
    private String mAimManageCom;
    private String mAStartDate;
    private String mPartTime = "";
    private String mAdjustDate ="" ;
    private String NewBranchManager="" ;
    /** 业务处理相关变量 */
    private LABranchGroupSchema mLABranchGroupSchema = new LABranchGroupSchema();
    private LACommisionSet mLACommisionSet=new LACommisionSet();
    private LABranchChangeTempSet mnewLABranchChangeTempSet=new LABranchChangeTempSet();
    private LABranchChangeTempSet mdeLABranchChangeTempSet=new LABranchChangeTempSet();
    private LABranchChangeTempSet minLABranchChangeTempSet=new LABranchChangeTempSet();

    //更新所有调动人员和管理人员在行政表中的AgentGroup UpBranch
    private LATreeSet mUpdateLATreeSet = new LATreeSet();

    //备份所有调动人员和管理人员
    private LATreeBSet mLATreeBSet = new LATreeBSet();

    //更新所有调动人员在LAAgent表中的AgentGroup
    private LAAgentSet mLAAgentSet = new LAAgentSet();

    //备份代理人记录 add by jiangcx minLABranchGroupBSet
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();
    //备份代理人记录 labranchgroupb表中的branchmanager
    private LABranchGroupBSet minLABranchGroupBSet = new LABranchGroupBSet();

    //更新所有调动人员
    private LABranchGroupSet mupLABranchGroupSet = new LABranchGroupSet();
    private MMap mMap=new MMap();

    private VData mOutputDate=new VData();

    public ALAGrpBranchManagerTempBL()
    {
    }

    public static void main(String[] args)
    {
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "ALAGrpBranchManagerTempBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        this.mInputData =(VData)cInputData.clone() ;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustGrpAgentBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败AdjustGrpAgentBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit=new PubSubmit();
        tPubSubmit.submitData(mOutputDate,"") ;
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "AdjustGrpAgentBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }
    private boolean check(String tAgentCode)
    {
        String tStartDate=mAStartDate;
        String tlastdate = PubFun.calDate(currentDate, 1, "M", null);
        String tlastno=AgentPubFun.formatDate(tlastdate,"yyyyMM");
        String tcurrentno=AgentPubFun.formatDate(currentDate,"yyyyMM");

        String tindexcalno=AgentPubFun.formatDate(tStartDate,"yyyyMM");
        String tpreStartDate = PubFun.calDate(tStartDate, 1, "M", null);
        String tpreindexcalno=AgentPubFun.formatDate(tpreStartDate,"yyyyMM");
        if(!tindexcalno.equals(tlastno) && !tindexcalno.equals(tcurrentno) &&
           !tpreindexcalno.equals(tcurrentno))
        {
            CError tError = new CError();
            tError.moduleName = "LAAssessGrpInputBL";
            tError.functionName = "check";
            tError.errorMessage = "日期必须在本月或则是本月的前后一个月!";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tDay=AgentPubFun.formatDate(tStartDate,"dd");
        if(!tDay.equals("01"))
        {
            CError tError = new CError();
            tError.moduleName = "LAAssessGrpInputBL";
            tError.functionName = "check";
            tError.errorMessage = "生效日期必须是每个月一日!";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tsql="select  indexcalno from lawage  where  agentcode='"+tAgentCode
                    +"' and  indexcalno='"+tindexcalno+"'";
        ExeSQL tExeSQL = new ExeSQL();
        String tIndexCalNo=tExeSQL.getOneValue(tsql);
        if(tIndexCalNo!=null && !tIndexCalNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "LAAssessGrpInputBL";
            tError.functionName = "check";
            tError.errorMessage = "人员"+tAgentCode+"在月份"+tIndexCalNo+
                                  "已经计算过薪资,不能在"+tStartDate+"调整团队!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //查询团队是否已经有了主管
        // 查询本月之后该团队是否有有效的主管任命,如果有,则表示虽然该团队现在还没有主观,但是下个月一日
        //进行批处理时,该团队就会有主管,则默认该团队有了主管
        String tSQL="select cvalimonth  from  LABranchChangeTemp where  agentgroup='"+
                    mLABranchGroupSchema.getAgentGroup()
                    +"' and  cvaliflag='1' and BranchChangeType='04'  and cvalimonth>'"
                    +tcurrentno+"'";
        tExeSQL = new ExeSQL();
        String tcvalimonth=tExeSQL.getOneValue(tSQL);
        if(tcvalimonth!=null && !tcvalimonth.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "LAAssessGrpInputBL";
            tError.functionName = "check";
            tError.errorMessage = "团队"+mLABranchGroupSchema.getAgentGroup()+"在月份"+tcvalimonth+
                                  "任命了主管,不能在给该团队再任命主管!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //不同序列人员异动是否做了该团队的人员晋升，如果有从业务序列到主管序列的调整，
        //并且是在下个月以后生效，则应该
        return true;
    }
    private boolean dealData()
    {
       if (mLABranchGroupSchema!=null)
       {
              LABranchChangeTempSchema tLABranchChangeTempSchema = new
                       LABranchChangeTempSchema();
               String tIndexCalNo = AgentPubFun.formatDate(mAStartDate,"yyyyMM");
               String tAgentCode=mLABranchGroupSchema.getBranchManager();
               //进行交验
               if (!check(tAgentCode))
               {
                   return false;
               }
               String tsql = "select max(tempcount) from LABranchChangeTemp  where agentcode='" +
                               tAgentCode
                               + "' and cvalimonth='" + tIndexCalNo + "'";
               ExeSQL t1ExeSQL = new ExeSQL();
               String tMaxCount = t1ExeSQL.getOneValue(tsql);
               int tCount = 0;
               if (tMaxCount == null || tMaxCount.equals("")) {
                   tCount = 0;
               } else {
                   tCount = Integer.parseInt(tMaxCount) + 1;
               }

               tLABranchChangeTempSchema.setCValiMonth(tIndexCalNo);
               tLABranchChangeTempSchema.setTempCount(tCount);
               tLABranchChangeTempSchema.setAgentCode(tAgentCode);
               //新的机构信息
               tLABranchChangeTempSchema.setAgentGroup(mLABranchGroupSchema.
                       getAgentGroup());
               LABranchGroupDB tLABranchGroupDB = new  LABranchGroupDB();
               LABranchGroupSchema tnewLABranchGroupSchema = new  LABranchGroupSchema();
               tLABranchGroupDB.setAgentGroup(mLABranchGroupSchema.getAgentGroup());
               tLABranchGroupDB.getInfo();
               //添加交验

               tnewLABranchGroupSchema=tLABranchGroupDB.getSchema();
               tLABranchChangeTempSchema.setBranchAttr(tnewLABranchGroupSchema.getBranchAttr());
               tLABranchChangeTempSchema.setBranchSeries(tnewLABranchGroupSchema.getBranchSeries());
               tLABranchChangeTempSchema.setStartDate(mAStartDate);
               tLABranchChangeTempSchema.setName(tnewLABranchGroupSchema.getName());
               tLABranchChangeTempSchema.setManageCom(tnewLABranchGroupSchema.getManageCom());
               tLABranchChangeTempSchema.setBranchChangeType("04");//主管任命
               //人员原机构信息
               LAAgentSchema tLAAgentSchema = new LAAgentSchema();
               LAAgentDB tLAAgentDB = new LAAgentDB();
               tLAAgentDB.setAgentCode(tAgentCode);
               tLAAgentDB.getInfo();
               tLAAgentSchema=tLAAgentDB.getSchema();
               tLABranchGroupDB = new  LABranchGroupDB();
               LABranchGroupSchema toldLABranchGroupSchema = new  LABranchGroupSchema();
               tLABranchGroupDB.setAgentGroup(tLAAgentSchema.getAgentGroup());
               tLABranchGroupDB.getInfo();
               //添加交验
               toldLABranchGroupSchema=tLABranchGroupDB.getSchema();


               //得到变更月该人员的团队变更信息
               String tSql="select *  from  LABranchChangeTemp  where agentcode = '"+tAgentCode
                           +"' and CValiMonth='"+tIndexCalNo+"' and cvaliflag='1' "
                           +" order by startdate desc  fetch first 1 rows only ";
               LABranchChangeTempSchema toldLABranchChangeTempSchema = new  LABranchChangeTempSchema();
               LABranchChangeTempSet toldLABranchChangeTempSet = new  LABranchChangeTempSet();
               LABranchChangeTempDB tLABranchChangeTempDB = new LABranchChangeTempDB();
               toldLABranchChangeTempSet=tLABranchChangeTempDB.executeQuery(tSql);
               if(toldLABranchChangeTempSet!=null && toldLABranchChangeTempSet.size()>=1)
               {//如果当月已经修改过，则与原纪录一样
                   toldLABranchChangeTempSchema=toldLABranchChangeTempSet.get(1);
                   tLABranchChangeTempSchema.setBranchLastAttr(toldLABranchChangeTempSchema.getBranchLastAttr());
                   tLABranchChangeTempSchema.setAgentLastGroup(toldLABranchChangeTempSchema.getAgentLastGroup());
               }
               else
               {//如果没有做过修改，则业务员当前团队为原来的团队
                   tLABranchChangeTempSchema.setBranchLastAttr(
                           toldLABranchGroupSchema.getBranchAttr());
                   tLABranchChangeTempSchema.setAgentLastGroup(
                           toldLABranchGroupSchema.getAgentGroup());
               }
               //  处理原团队的开始日期和止期
               toldLABranchChangeTempSchema = new  LABranchChangeTempSchema();
               toldLABranchChangeTempSet = new  LABranchChangeTempSet();
               tLABranchChangeTempDB = new LABranchChangeTempDB();
               String tSQL="select *  from  LABranchChangeTemp  where agentcode = '"+tAgentCode
                           +"'  and cvaliflag='1' "
                       //    +" and CValiMonth<'"+tIndexCalNo+"' "
                           +" order by startdate desc  fetch first 1 rows only ";
               toldLABranchChangeTempSet=tLABranchChangeTempDB.executeQuery(tSQL);
               //如果做过团队变更
               if (toldLABranchChangeTempSet!=null && toldLABranchChangeTempSet.size()>=1)
               {

                   toldLABranchChangeTempSchema=toldLABranchChangeTempSet.get(1);
                   if(mAStartDate.compareTo(toldLABranchChangeTempSchema.getStartDate())<0)
                   {//现在调动日与原调动日比较
                       if(currentDate.compareTo(toldLABranchChangeTempSchema.getStartDate())>=0)
                       {//当前日期与原生效日比较,如果是大于等于,表示已经生效了
                           buildError("dealData","该人员在" + mAStartDate
                                      + "之后已经进行过团队调动的处理,调动日期必须大于或等于原来的变动日期");
                           return false;
                       }
                   }
                   tLABranchChangeTempSchema.setOldStartDate(toldLABranchChangeTempSchema.getStartDate());
                   tLABranchChangeTempSchema.setOldEndDate(PubFun.calDate(mAStartDate, -1,
                            "D", null));
               }
               //如果没有做过团队变更,人员上次团队的开始时间为人员的入司时间
               else
               {
//                   LAAgentSchema tLAAgentSchema = new LAAgentSchema();
//                   LAAgentDB tLAAgentDB = new LAAgentDB();
//                   tLAAgentDB.setAgentCode(tAgentCode);
//                   tLAAgentDB.getInfo();
//                   tLAAgentSchema=tLAAgentDB.getSchema();
                   tLABranchChangeTempSchema.setOldStartDate(tLAAgentSchema.getEmployDate());
                   tLABranchChangeTempSchema.setOldEndDate(PubFun.calDate(mAStartDate, -1,
                            "D", null));
               }
               tLABranchChangeTempSchema.setBranchType(mLABranchGroupSchema.getBranchType());
               tLABranchChangeTempSchema.setBranchType2(mLABranchGroupSchema.getBranchType2());
               tLABranchChangeTempSchema.setCValiFlag("1");
               tLABranchChangeTempSchema.setCValiDate(mAStartDate);
               tLABranchChangeTempSchema.setOperator(mGlobalInput.Operator);
               tLABranchChangeTempSchema.setMakeDate(currentDate);
               tLABranchChangeTempSchema.setMakeTime(currentTime);
               tLABranchChangeTempSchema.setModifyDate(currentDate);
               tLABranchChangeTempSchema.setModifyTime(currentTime);
               mnewLABranchChangeTempSet.add(tLABranchChangeTempSchema);
               //生效日后或相等为原始数据,置为无效
               tSQL="select *  from  LABranchChangeTemp  where agentcode = '"+tAgentCode
                           +"' and CValiMonth>='"+tIndexCalNo+"' and cvaliflag='1' " ;
              LABranchChangeTempDB toldLABranchChangeTempDB = new   LABranchChangeTempDB();
              toldLABranchChangeTempSet = new LABranchChangeTempSet();
              toldLABranchChangeTempSet=toldLABranchChangeTempDB.executeQuery(tSQL);
              if(toldLABranchChangeTempSet!=null || toldLABranchChangeTempSet.size()>=1)
              {
                  for(int k=1;k<=toldLABranchChangeTempSet.size();k++)
                  {
                      LABranchChangeTempSchema tupLABranchChangeTempSchema = new
                              LABranchChangeTempSchema();
                      tupLABranchChangeTempSchema = toldLABranchChangeTempSet.
                              get(k);
                      LABranchChangeTempSchema tdeLABranchChangeTempSchema = new
                              LABranchChangeTempSchema();
                      Reflections tReflections = new Reflections();
                      tReflections.transFields(tdeLABranchChangeTempSchema,
                                               tupLABranchChangeTempSchema);
                      mdeLABranchChangeTempSet.add(tdeLABranchChangeTempSchema);
                      tupLABranchChangeTempSchema.setCValiFlag("0");
                      tupLABranchChangeTempSchema.setModifyDate(currentDate);
                      tupLABranchChangeTempSchema.setModifyTime(currentTime);
                      tupLABranchChangeTempSchema.setOperator(mGlobalInput.
                              Operator);
                      minLABranchChangeTempSet.add(tupLABranchChangeTempSchema);
                  }
           }

           //当天大于原来的日期
           if(currentDate.compareTo(mAStartDate)>=0)
           {
              ALAGrpBranchManagerBL tALAGrpBranchManagerBL = new ALAGrpBranchManagerBL();
              System.out.println("Start AdjustGrpAgentTemp Submit...");
              tALAGrpBranchManagerBL.submitData(mInputData, mOperate);
              System.out.println("End AdjustGrpAgentTemp Submit...");
              //如果有需要处理的错误，则返回
              if (tALAGrpBranchManagerBL.mErrors.needDealError())
              {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tALAGrpBranchManagerBL.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "AdjustGrpAgentUI";
                  tError.functionName = "submitData";
                  tError.errorMessage = "数据提交失败!";
                  this.mErrors.addOneError(tError);
                  return false;
              }
              mMap=tALAGrpBranchManagerBL.getMapResult();
           }

       }

       return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLABranchGroupSchema.setSchema((LABranchGroupSchema) cInputData.
                                            getObjectByObjectName(
                "LABranchGroupSchema",
                0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput",
                                            0));
        this.mPartTime = (String) cInputData.get(2);
        this.mAStartDate =(String)cInputData.get(3) ;
        this.NewBranchManager=(String)cInputData.get(4) ;
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
          mMap.put(this.mnewLABranchChangeTempSet ,"INSERT") ;
          mMap.put(this.mdeLABranchChangeTempSet ,"DELETE") ;
          mMap.put(this.minLABranchChangeTempSet ,"INSERT") ;
          this.mOutputDate .add(mMap) ;
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustGrpAgentBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
