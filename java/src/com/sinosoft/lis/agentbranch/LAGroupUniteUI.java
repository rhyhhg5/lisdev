/*
 * <p>ClassName: LAGroupUniteUI </p>
 * <p>Description: LAGroupUniteUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-07-19 09:58:25
 */
package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAGroupUniteUI
{
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    public static void main(String[] agrs)
    {
        String cOperate = "INSERT||MAIN";
        LABranchGroupSchema tLABranchGroupSchemaA = new LABranchGroupSchema();
        LABranchGroupSchema tLABranchGroupSchemaU = new LABranchGroupSchema();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        LATreeSchema tLATreeSchema = new LATreeSchema();

        tLABranchGroupSchemaA.setBranchAttr("8611000043");  // 目标机构代码
        tLABranchGroupSchemaU.setBranchAttr("8611000044");  // 被合并机构代码
        tLABranchGroupSet.add(tLABranchGroupSchemaA);
        tLABranchGroupSet.add(tLABranchGroupSchemaU);

        tLATreeSchema.setAgentCode("1102000013");  // 被合并机构主管ID
        tLATreeSchema.setAgentGrade("D05");        // 职级
        tLATreeSchema.setAgentLastGrade("D08");    // 前职级
        tLATreeSchema.setAstartDate("200506");     // 考核年月
        //tLATreeSchema.setAssessType("Y");          // 待处理状态 'Y'待处理 'N'现处理
        tLATreeSchema.setStartDate("2005-07-01");

        VData cInputData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "mak002";
        tGlobalInput.ManageCom = "861100";
        tGlobalInput.ComCode = "mak002";

        cInputData.add(tGlobalInput);
        cInputData.add(tLABranchGroupSet);
        cInputData.add(tLATreeSchema);

        LAGroupUniteUI tLAGroupUniteUI = new LAGroupUniteUI();
        boolean actualReturn = tLAGroupUniteUI.submitData(cInputData, cOperate);
    }

    public boolean submitData(VData cInputData,String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        mInputData=(VData)cInputData.clone();

        LAGroupUniteBL tLAGroupUniteBL= new LAGroupUniteBL();
        tLAGroupUniteBL.submitData(mInputData,mOperate);
        //如果有需要处理的错误，则返回
        if (tLAGroupUniteBL.mErrors.needDealError())
        {
          // @@错误处理
          this.mErrors.copyAllErrors(tLAGroupUniteBL.mErrors);
          CError tError = new CError();
          tError.moduleName = "LAGroupUniteUI";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
          this.mErrors.addOneError(tError);
          return false;
        }

        return true;
    }

}
