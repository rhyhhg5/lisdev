package com.sinosoft.lis.agentbranch;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class LPrintTempBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSqlTitle = null;
    private String mSql = null;
    private String mTitle = null;
    private String mOutXmlPath = null;
    //private String mtype = null;
    SSRS tSSRS=new SSRS();
    SSRS tSSRS1=new SSRS();
    SSRS tSSRS2=new SSRS();
    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    public LPrintTempBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
    	
    	System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mSqlTitle);
        System.out.println(mOutXmlPath);
        
        //报表表头 和 列名
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(mSqlTitle);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LPrintTempBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        
        //数据
        tSSRS1 = tExeSQL.execSQL(mSql);       

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LPrintTempBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        
//      数据
        tSSRS2 = tExeSQL.execSQL(mTitle);       

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LPrintTempBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        
        
        String[][] mToExcel = new String[tSSRS1.getMaxRow() + 4][50];
        mToExcel[0][0] =  tSSRS2.GetText(1, 1);        

        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row][col - 1] = tSSRS.GetText(row, col);
            }
        }
        
        for(int row = 1; row <= tSSRS1.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS1.getMaxCol(); col++)
            {
                mToExcel[row +1][col - 1] = tSSRS1.GetText(row, col);
            }
        }
       
        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LPrintTempBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mSqlTitle = (String) tf.getValueByName("querySqlTitle");
        mTitle = (String) tf.getValueByName("Title");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        
        if(mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "LPrintTempBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }
  
        /**
         * 执行SQL文查询结果
         * @param sql String
         * @return double
         */
  
    public static void main(String[] args)
    {
    	LPrintTempBL tLPrintTempBL = new LPrintTempBL();
    System.out.println("11111111");
    }
}
