/*
 * <p>ClassName: AdjustAgentBL </p>
 * <p>Description: AdjustAgentBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-27
 */
package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeAccessoryDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeAccessorySchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LATreeAccessoryBSet;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.SSRS;




public class AdjustBankAgentTempBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private String mOperate;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();

    private String mBranchType="";
    private String mBranchType2="";
    private String mCvaliMonth;
    private SSRS mAgentCode;


    private String tEdorNo;
    private String mAimManageCom;
    private String mAStartDate;

    /** 业务处理相关变量 */
    private LABranchChangeTempSchema  mLABranchChangeTempSchema=new LABranchChangeTempSchema();
    private LABranchGroupSchema mLABranchSchema = new LABranchGroupSchema();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAComSchema mLAComSchema = new LAComSchema();
    private LAComToAgentSchema mLAComToAgentSchema = new LAComToAgentSchema();
    private LATreeSet mLATreeSet = new LATreeSet();

    //更新所有调动人员和管理人员在行政表中的AgentGroup UpBranch
    private LATreeSet mUpdateLATreeSet = new LATreeSet();
    //备份所有调动人员和管理人员
    private LATreeBSet mLATreeBSet = new LATreeBSet();

    //更新所有调动人员在LAAgent表中的AgentGroup
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    //备份代理人记录 add by jiangcx minLABranchGroupBSet
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();

    //备份代理人记录 labranchgroupb表中的branchmanager
    private LABranchGroupBSet minLABranchGroupBSet = new LABranchGroupBSet();
    //更新所有调动人员
    private LABranchGroupSet mupLABranchGroupSet = new LABranchGroupSet();

     //备份代理人和代理机构关联表lacomtoagent及备份表lacomtoagentb
    private LAComToAgentSet mLAComToAgentSet=new LAComToAgentSet();
    private LAComToAgentBSet mLAComToAgentBSet=new LAComToAgentBSet();
    private LAComSet mLAComSet=new LAComSet();

    private MMap mMap=new MMap();

    private VData mOutputDate=new VData();

    public AdjustBankAgentTempBL()
    {
    }

    public static void main(String[] args)
    {
        String as = "2003-11-21";
        System.out.println(PubFun.calDate(as, -1, "M", null));
        String tDay = as.substring(as.lastIndexOf("-") + 1);
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "AdjustBankAgentTempBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
       * 传输数据的公共方法
       * @param: cInputData 输入的数据
       * cOperate 数据操作
       * @return:
       */
      public boolean submitData(VData cInputData, String cOperate)
      {
          //将操作数据拷贝到本类中
          this.mOperate = cOperate;
          //得到外部传入的数据,将数据备份到本类中
          if (!getInputData(cInputData))
          {
              return false;
          }
          //进行业务处理
          //if (!check())
          //{
          //    return false;
          //}
          if (!dealData())
          {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "AdjustBankAgentTempBL";
              tError.functionName = "submitData";
              tError.errorMessage = "数据处理失败AdjustBankAgentTempBL-->dealData!";
              this.mErrors.addOneError(tError);
              return false;
          }
          //准备往后台的数据
          if (!prepareOutputData())
          {
              return false;
          }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start AdjustBankAgentTempBL Submit...");
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "AdjustBankAgentTempBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mInputData = null;
          return true;
    }


    /**
        * 从输入数据中得到所有对象
        *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
        */
       private boolean getInputData(VData cInputData)
       {
           System.out.println("GetInputData-AdjustBankAgentTempBL");
           mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                   "GlobalInput", 0));
           this.mLABranchChangeTempSchema.setSchema((LABranchChangeTempSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LABranchChangeTempSchema",
                                                  1));
           mBranchType=this.mLABranchChangeTempSchema.getBranchType();
           mBranchType2=this.mLABranchChangeTempSchema.getBranchType2();
           mCvaliMonth=this.mLABranchChangeTempSchema.getCValiMonth();
           System.out.println(mBranchType+"/"+mBranchType2+"/"+mCvaliMonth);
           if (mGlobalInput == null)
           {
               // @@错误处理
               CError tError = new CError();
               tError.moduleName = "AdjustBankAgentTempBL";
               tError.functionName = "getInputData";
               tError.errorMessage = "没有得到足够的信息！";
               this.mErrors.addOneError(tError);
               return false;
           }
           return true;
    }




    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        LAComToAgentDB tLAComToAgentDB=null;
        LAComToAgentSet tLAComToAgentSet=new LAComToAgentSet();
        System.out.println("进入dealData处理");
        tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        String tSQL ="select max(edorno) from lacomtoagentb ";
        ExeSQL tExeSQL =new ExeSQL();
        String tEdorNo2=""+tExeSQL.getOneValue(tSQL);
         boolean tReturn = true;
        String sql = "select distinct agentcode from LABranchChangeTemp "
                   +" where 1=1  and branchType='" + mBranchType + "'"
                   +" and branchType2='" + mBranchType2 + "'"
                   +" and branchchangetype='05'"  //团队人员调整
                   +" and cvalimonth='"+mCvaliMonth+"'"
                   +" and agentcode in (select agentcode from latree where agentgrade<'G41') with ur";

        System.out.println("查询语句:" + sql);
        tExeSQL = new ExeSQL();
        mAgentCode = tExeSQL.execSQL(sql);
        if (mAgentCode == null) {
            return false;
        }
        for (int x = 1; x <= mAgentCode.getMaxRow(); x++) {
            String tAgentCode = mAgentCode.GetText(x, 1);
            LABranchChangeTempDB tLABranchChangeTempDB = new LABranchChangeTempDB();
            LABranchChangeTempSet tLABranchChangeTempSet = new LABranchChangeTempSet();
            sql = "select * from LABranchChangeTemp where  branchtype = '" + mBranchType + "'"
                  + " and branchType2='" + mBranchType2 + "'"
                  + " And cvalimonth = '" + mCvaliMonth + "'"
                  + " And agentcode ='" + tAgentCode + "'"
                  + " order by tempcount desc fetch first 1 rows only";

            System.out.println("查询人员语句:" + sql);
            tLABranchChangeTempSet = tLABranchChangeTempDB.executeQuery(sql);

            if (tLABranchChangeTempDB.mErrors.needDealError()) {
                this.mErrors.copyAllErrors(tLABranchChangeTempDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentWageCalSaveNewBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询纪录存在是否出错！";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tLABranchChangeTempSet.size() == 0) {
                System.out.println("表中没有纪录！");
                CError tError = new CError();
                tError.moduleName = "AgentWageCalSaveNewBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询纪录存在是否出错！";
                this.mErrors.addOneError(tError);
                return false;
           }
           else
           {
               mLABranchChangeTempSchema=tLABranchChangeTempSet.get(1);
               LABranchGroupDB tLABranchDB = new LABranchGroupDB();
               tLABranchDB.setAgentGroup(this.mLABranchChangeTempSchema.getAgentGroup());
               if (!tLABranchDB.getInfo())
               {
                // @@错误处理
                this.mErrors.copyAllErrors(tLABranchDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "AdjustBankAgentTempBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询目标单位信息失败!";
                this.mErrors.addOneError(tError);
                return false;
                }

                mLABranchSchema=tLABranchDB.getSchema();
                //准备更新所有调动人员和管理人员在行政表中的AgentGroup和UpAgent的纪录
                String tAimBranch = this.mLABranchSchema.getAgentGroup();
                String tAimManage = this.mLABranchSchema.getManageCom();
                String tUpAgent = this.mLABranchSchema.getBranchManager();
                mAimManageCom = this.mLABranchSchema.getManageCom() ;
                System.out.println("UpAgent:" + tUpAgent + " --不管目标机构是否有管理人员");
                tUpAgent = tUpAgent == null ? "" : tUpAgent;


                LATreeDB tLATreeDB = new LATreeDB();
                tLATreeDB.setAgentCode(this.mLABranchChangeTempSchema.getAgentCode()); //jiangcx add for BK 主键
                if (!tLATreeDB.getInfo())
                {
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "AdjustBankAgentTempBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询业务员"+mLABranchChangeTempSchema.getAgentCode()  +"的基础信息失败!";
                this.mErrors.addOneError(tError);
                return false;
                 }
                mLATreeSchema = tLATreeDB.getSchema();


                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(this.mLABranchChangeTempSchema.getAgentCode()); //jiangcx add for BK 主键
                if (!tLAAgentDB.getInfo())
                {
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "AdjustBankAgentTempBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询业务员"+mLABranchChangeTempSchema.getAgentCode()  +"的基础信息失败!";
                this.mErrors.addOneError(tError);
                return false;
                }

                mLAAgentSchema = tLAAgentDB.getSchema();
                String agentgrade=mLATreeSchema.getAgentGrade().substring(0,1);

                String mSQL="select * from lacomtoagent where agentcom in (select distinct agentcom from lacomtoagent where agentcode='"+this.mLABranchChangeTempSchema.getAgentCode()+"')";
                tLAComToAgentDB=new LAComToAgentDB();
                tLAComToAgentSet=tLAComToAgentDB.executeQuery(mSQL);
                if (tLAComToAgentDB.mErrors.needDealError()) {
                this.mErrors.copyAllErrors(tLAComToAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "AdjustBankAgentTempBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询业务员" + mLABranchChangeTempSchema.getAgentCode() +
                                      "的关联代理机构信息失败!";
                this.mErrors.addOneError(tError);
                return false;
                }




                //genxin  latree
                LATreeSchema bLATreeSchema=new LATreeSchema();
                LATreeBSchema tLATreeBSchema=new LATreeBSchema();
                bLATreeSchema=tLATreeDB.getSchema();
                Reflections tReflections=new Reflections();
                tReflections.transFields(tLATreeBSchema,bLATreeSchema);
                tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator()  ) ;
                tLATreeBSchema.setOperator(mGlobalInput.Operator );
                tLATreeBSchema.setEdorNO(tEdorNo) ;
                tLATreeBSchema.setRemoveType("03") ;
                tLATreeBSchema.setMakeDate2(tLATreeBSchema.getMakeDate() );
                tLATreeBSchema.setMakeTime2(tLATreeBSchema.getMakeTime());
                tLATreeBSchema.setMakeDate(currentDate) ;
                tLATreeBSchema.setMakeTime(currentTime) ;
                this.mLATreeBSet .add(tLATreeBSchema) ;
                //设置上级代理人
                bLATreeSchema.setUpAgent(tUpAgent);
                bLATreeSchema.setAgentGroup(tAimBranch); //设置所属管理机构
                System.out.println("tAimBranchtAimBranch:"+tAimBranch);
                bLATreeSchema.setManageCom(mLABranchSchema.getManageCom() ) ;
                bLATreeSchema.setOperator(this.mGlobalInput.Operator);
                bLATreeSchema.setModifyDate(currentDate);
                bLATreeSchema.setModifyTime(currentTime);
                bLATreeSchema.setBranchCode(tAimBranch) ;
                bLATreeSchema.setAstartDate(mLABranchChangeTempSchema.getCValiDate()); //调整日期
                this.mUpdateLATreeSet.add(bLATreeSchema);

                //
                LAAgentSchema tLAAgentSchema=new LAAgentSchema() ;
                tLAAgentSchema=tLAAgentDB.getSchema() ;
                LAAgentBSchema tLAAgentBSchema=new LAAgentBSchema();
                tReflections=new Reflections();
                tReflections.transFields(tLAAgentBSchema,tLAAgentSchema) ;
                tLAAgentBSchema.setMakeDate(currentDate) ;
                tLAAgentBSchema.setMakeTime(currentTime) ;
                tLAAgentBSchema.setOperator(mGlobalInput.Operator ) ;
                tLAAgentBSchema.setEdorNo(tEdorNo) ;
                tLAAgentBSchema.setEdorType("03") ;
                this.mLAAgentBSet.add(tLAAgentBSchema)  ;
                tLAAgentSchema.setModifyDate(currentDate) ;
                tLAAgentSchema.setModifyTime(currentTime);
                tLAAgentSchema.setAgentGroup(tAimBranch);
                tLAAgentSchema.setBranchCode(tAimBranch);
                tLAAgentSchema.setOperator(mGlobalInput.Operator ) ;
                tLAAgentSchema.setManageCom(mLABranchSchema.getManageCom() ) ;
                mLAAgentSet.add(tLAAgentSchema) ;

                //genxin lacomtoagent
                LAComToAgentSchema tLAComToAgentSchema;
                LAComToAgentBSchema tLAComToAgentBSchema;
                System.out.println("BL:dealdata:tLAComToAgentSet.size"+tLAComToAgentSet.size());
                for(int m=1;m<=tLAComToAgentSet.size();m++){
                tEdorNo2=(Integer.parseInt(tEdorNo2)+1)+"";
                tLAComToAgentSchema=new LAComToAgentSchema();
                tLAComToAgentBSchema=new LAComToAgentBSchema();
                tLAComToAgentSchema=tLAComToAgentSet.get(m);
                tReflections = new Reflections();
                tReflections.transFields(tLAComToAgentBSchema, tLAComToAgentSchema);
                tLAComToAgentBSchema.setMakeDate(currentDate);
                tLAComToAgentBSchema.setMakeTime(currentTime);
                tLAComToAgentBSchema.setModifyDate(currentDate);
                tLAComToAgentBSchema.setModifyTime(currentTime);
                tLAComToAgentBSchema.setOperator(mGlobalInput.Operator);
                tLAComToAgentBSchema.setEdorNo(tEdorNo2);
                tLAComToAgentBSchema.setEdorType("03");
                mLAComToAgentBSet.add(tLAComToAgentBSchema);
                tLAComToAgentSchema.setModifyDate(currentDate);
                tLAComToAgentSchema.setModifyTime(currentTime);
                tLAComToAgentSchema.setOperator(mGlobalInput.Operator);
                tLAComToAgentSchema.setAgentGroup(tAimBranch);
                mLAComToAgentSet.add(tLAComToAgentSchema);
                }
                if(tLAComToAgentSet.size()>0){
                    String LACOMSQL = "update LACOM SET " +
                       "ManageCom  = '"+tAimManage+"'," +
                       "modifydate = '"+currentDate+"'," +
                       "modifytime = '"+currentTime+"'," +
                       "operator = '"+this.mGlobalInput.Operator+"' " +
                       "where branchtype='3' and branchtype2='01' and (endflag='N' or endflag is null)  "+
                       "and agentcom in (select distinct agentcom from lacomtoagent where agentcode='"+this.mLABranchChangeTempSchema.getAgentCode()+"')";
               mMap.put(LACOMSQL, "UPDATE");
                }
           }
        }
         if (this.mErrors .needDealError() )
        {
            tReturn=false;
        }
        else
        {
            tReturn = true;
        }
        return tReturn;
    }



    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
          mMap.put(this.mLAAgentBSet ,"INSERT") ;
          mMap.put(this.mLATreeBSet,"INSERT") ;
          mMap.put(this.mUpdateLATreeSet,"UPDATE") ;
          mMap.put(this.mLAAgentSet,"UPDATE") ;
          mMap.put(this.mLAComToAgentBSet,"INSERT") ;
          mMap.put(this.mLAComToAgentSet,"UPDATE") ;
          this.mInputData.add(mMap);
          return true;
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustBankAgentTempBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public MMap getMapResult() {
        return mMap;
    }
}
