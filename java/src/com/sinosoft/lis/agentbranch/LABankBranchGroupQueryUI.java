package com.sinosoft.lis.agentbranch;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LABankBranchGroupQueryUI
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    public LABankBranchGroupQueryUI()
    {
        System.out.println("LABankBranchGroupQueryUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
       LABankBranchGroupQueryBL bl = new LABankBranchGroupQueryBL();
        if(!bl.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        LABankBranchGroupQueryUI tLABankBranchGroupQueryUI = new   LABankBranchGroupQueryUI();
    }
}
