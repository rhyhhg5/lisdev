/*
 * <p>ClassName: ALABranchManagerBLS </p>
 * <p>Description: ALABranchManagerBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-09
 */
package com.sinosoft.lis.agentbranch;

import java.sql.Connection;

import com.sinosoft.lis.agentcalculate.DealBusinessData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vdb.LAAgentBDBSet;
import com.sinosoft.lis.vdb.LAAgentDBSet;
import com.sinosoft.lis.vdb.LABranchGroupBDBSet;
import com.sinosoft.lis.vdb.LABranchGroupDBSet;
import com.sinosoft.lis.vdb.LATreeAccessoryBDBSet;
import com.sinosoft.lis.vdb.LATreeAccessoryDBSet;
import com.sinosoft.lis.vdb.LATreeBDBSet;
import com.sinosoft.lis.vdb.LATreeDBSet;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LATreeAccessoryBSet;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ALABranchManagerBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();
    private LABranchGroupBSet mLABranchGroupDBSet = new LABranchGroupBSet();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LATreeBSet mLATreeBSet = new LATreeBSet();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();
    private LATreeAccessorySet mLATreeAccessorySet = new LATreeAccessorySet();
    private LATreeAccessoryBSet mLATreeAccessoryBSet = new LATreeAccessoryBSet();

    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();

    private String cAgentCode = "";
    private String cAgentGroup = "";
    private String cBranchCode = "";
    private String cBranchAttr = "";
    private String tPartTime = "";
    private String currTime = "";
    private String currDate = "";
    private String tAdjustDate = "";
    private String tBranchSeries = "";

    public ALABranchManagerBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     *传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        if (!getInputDate(cInputData))
        {
            return false;
        }
        System.out.println("Start ALABranchManagerBLS Submit...");
        if (!saveLABranchManager(cInputData))
        {
            return false;
        }

        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean getInputDate(VData cInputData)
    {
        try
        {
            mLABranchGroupSet = (LABranchGroupSet) cInputData.
                                getObjectByObjectName("LABranchGroupSet", 0);
            mLABranchGroupDBSet = (LABranchGroupBSet) cInputData.
                                  getObjectByObjectName("LABranchGroupSetB", 0);
            mLATreeSet = (LATreeSet) cInputData.getObjectByObjectName(
                    "LATreeSet", 0);
            mLATreeBSet = (LATreeBSet) cInputData.getObjectByObjectName(
                    "LATreeBSet", 0);
            mLAAgentSet = (LAAgentSet) cInputData.getObjectByObjectName(
                    "LAAgentSet", 0);
            mLAAgentBSet = (LAAgentBSet) cInputData.getObjectByObjectName(
                    "LAAgentBSet", 0);
            mLATreeAccessorySet = (LATreeAccessorySet) cInputData.
                                  getObjectByObjectName("LATreeAccessorySet", 0);
            mLATreeAccessoryBSet = (LATreeAccessoryBSet) cInputData.
                                   getObjectByObjectName("LATreeAccessoryBSet",
                    0);

            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);

            cAgentCode = (String) mTransferData.getValueByName("AgentCode");
            cAgentGroup = (String) mTransferData.getValueByName("AgentGroup");
            cBranchCode = (String) mTransferData.getValueByName("BranchCode");
            cBranchAttr = (String) mTransferData.getValueByName("BranchAttr");
            tPartTime = (String) mTransferData.getValueByName("PartTime");
            currDate = (String) mTransferData.getValueByName("ModifyDate");
            currTime = (String) mTransferData.getValueByName("ModifyTime");
            tAdjustDate = (String) mTransferData.getValueByName("AdjustDate");
            tBranchSeries = (String) mTransferData.getValueByName("BranchSeries");

            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    private boolean saveLABranchManager(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        //conn=PubFun1.getDefaultConnection();
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALABranchManagerBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 更新机构的主管信息...");
            LABranchGroupDBSet tLABranchGroupDBSet = new LABranchGroupDBSet(
                    conn);
            tLABranchGroupDBSet.add(mLABranchGroupSet);
            if (!tLABranchGroupDBSet.update())
            {
                System.out.println("error1");
                this.mErrors.copyAllErrors(tLABranchGroupDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALABranchManagerBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "更新机构的行政信息时失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            LABranchGroupBDBSet tLABranchGroupBDBSet = new LABranchGroupBDBSet(
                    conn);
            tLABranchGroupBDBSet.add(mLABranchGroupDBSet);
            System.out.println("Start 插入机构的备份信息...");
            if (!tLABranchGroupBDBSet.insert())
            {
                System.out.println("error2");
                this.mErrors.copyAllErrors(tLABranchGroupBDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALABranchManagerBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "插入管理机构的备份信息时失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            System.out.println("---start to insert LATreeB data---");
            LATreeBDBSet tLATreeBDBSet = new LATreeBDBSet(conn);
            tLATreeBDBSet.add(mLATreeBSet);
            if (!tLATreeBDBSet.insert())
            {
                System.out.println("error3");
                this.mErrors.copyAllErrors(tLATreeBDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALABranchManagerBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "插入个人行政信息备份数据时失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            System.out.println("---start to update LATree data---");
            LATreeDBSet tLATreeDBSet = new LATreeDBSet(conn);
            tLATreeDBSet.add(mLATreeSet);
            if (!tLATreeDBSet.update())
            {
                System.out.println("error4");
                this.mErrors.copyAllErrors(tLATreeDBSet.mErrors);
                CError tError = new CError();
                System.out.println("error");

                tError.moduleName = "ALABranchManagerBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "更新个人行政信息时失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            LAAgentBDBSet tLAAgentBDBSet = new LAAgentBDBSet(conn);
            tLAAgentBDBSet.add(mLAAgentBSet);
            if (!tLAAgentBDBSet.insert())
            {
                this.mErrors.copyAllErrors(tLAAgentBDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALABranchManagerBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "备份个人基础信息时失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LAAgentDBSet tLAAgentDBSet = new LAAgentDBSet(conn);
            tLAAgentDBSet.add(mLAAgentSet);
            if (!tLAAgentDBSet.update())
            {
                this.mErrors.copyAllErrors(tLAAgentDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALABranchManagerBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "更新个人基础信息时失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            //只有个人的机构主管任命才处理代理人行政附属表信息
            if (mLATreeAccessoryBSet.size() != 0)
            {
                LATreeAccessoryBDBSet tLATreeAccessoryBDBSet = new
                        LATreeAccessoryBDBSet(conn);
                tLATreeAccessoryBDBSet.add(mLATreeAccessoryBSet);
                if (!tLATreeAccessoryBDBSet.insert())
                {
                    this.mErrors.copyAllErrors(tLATreeAccessoryBDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ALABranchManagerBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "备份个人行政附属信息时失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
                LATreeAccessoryDBSet tLATreeAccessoryDBSet = new
                        LATreeAccessoryDBSet(conn);
                tLATreeAccessoryDBSet.add(mLATreeAccessorySet);
                if (!tLATreeAccessoryDBSet.update())
                {
                    this.mErrors.copyAllErrors(tLATreeAccessoryDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ALABranchManagerBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "更新个人行政附属信息时失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            if (tPartTime.equals("1"))
            {
                System.out.println("BranchAttr:" + cBranchAttr);
                if (!UpdateAgentGroup(cAgentCode, cAgentGroup, cBranchCode,
                                      cBranchAttr,
                                      tAdjustDate,tBranchSeries,conn))
                {
                    System.out.println("error5");
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            conn.commit();
//      conn.rollback() ;
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALABranchManagerBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return true;
    }

    //更新被任命主管在业务数据相关表的AgentGroup数据
    private boolean UpdateAgentGroup(String cAgentCode, String cAgentGroup,
                                     String cBranchCode, String cBranchAttr,
                                     String cAdjustDate, String cBranchSeries,
                                     Connection conn)
    {
        String tEndDate = "3000-12-31";
        String tFlag = "Y";
        DealBusinessData tDealData = new DealBusinessData();
        VData tVData = new VData();
        tVData.clear();
        tVData.add(cAgentCode);
        tVData.add(cAgentGroup);
        tVData.add(tFlag);
        tVData.add(cBranchAttr);
        tVData.add(null); //IndexCalNo
        tVData.add(cBranchCode);
        tVData.add(cBranchSeries);
        tDealData.inputPeriod(cAdjustDate, tEndDate);
        System.out.println("AgentCode:" + cAgentCode);
        System.out.println("AgentGroup:" + cAgentGroup);
        System.out.println("Flag:" + tFlag);
        System.out.println("cBranchAttr:" + cBranchAttr);
        System.out.println("cStartDate:" + cAdjustDate);
        System.out.println("tEndDate:" + tEndDate);
        if (!tDealData.updateAgentGroup(tVData, conn))
        {
            this.mErrors.copyAllErrors(tDealData.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALABranchManagerBLS";
            tError.functionName = "UpdateAgentGroup";
            tError.errorMessage = "更新业务数据失败！";
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception ex)
            {}
            ;
            return false;
        }
        return true;
    }

}
