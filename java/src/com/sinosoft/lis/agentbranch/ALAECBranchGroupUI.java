package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ALAECBranchGroupUI {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private String mCostCenter="";
//业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LABranchGroupSchema mLABranchGroupSchema = new LABranchGroupSchema();
    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();
    public ALAECBranchGroupUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        System.out.println("getinputdata");
        
        if (!getInputData(cInputData))
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        ALAECBranchGroupBL tALAECBranchGroupBL = new ALAECBranchGroupBL();
        System.out.println("Start ALAECBranchGroupUI Submit...");
        System.out.println("BranchType:" + mLABranchGroupSchema.getBranchType());
        tALAECBranchGroupBL.submitData(mInputData, mOperate);
        System.out.println("End ALAECBranchGroupUI Submit...");
        //如果有需要处理的错误，则返回
        if (tALAECBranchGroupBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tALAECBranchGroupBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAECBranchGroupUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mOperate.equals("UPDATE||MAIN"))
        {
            this.mResult.clear();
            this.mResult = tALAECBranchGroupBL.getResult();
        }

        mInputData = null;
        return true;
    }

    public static void main(String[] args)
    {
        VData tVData = new VData();
        String sqlstr =
                "select * from LABranchGroup where BranchAttr = '86110000180101'";
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        LABranchGroupDB tLABranchGroupDB = tLABranchGroupSchema.getDB();
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(sqlstr);
        if (tLABranchGroupSet.size() > 0)
        {
//            tLABranchGroupSchema = tLABranchGroupSet.get(1);
            tLABranchGroupSchema.setManageCom("86110000");
            tLABranchGroupSchema.setName("组");
            tLABranchGroupSchema.setBranchType("1");
            tLABranchGroupSchema.setBranchType2("11");
            tLABranchGroupSchema.setBranchAttr("86110000180102");
            tLABranchGroupSchema.setBranchLevel("01");
            tLABranchGroupSchema.setUpBranch("861100001801");
           // tLABranchGroupSchema.setUpBranchAttr("0");
            tLABranchGroupSchema.setAStartDate("2005-04-13");
            tLABranchGroupSchema.setFoundDate("2005-04-13");
            tLABranchGroupSchema.setEndDate("") ;
            tLABranchGroupSchema.setEndFlag("N") ;
        }
        else
        {
            System.out.println("error");
            return;
        }
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";
        tVData.clear();
        //提交
        tVData.addElement(tG);
        tVData.addElement("86110000");
        tVData.addElement(tLABranchGroupSchema);
        tVData.addElement("1");
        ALAECBranchGroupUI tLABG = new ALAECBranchGroupUI();
        ALAECBranchGroupUI tALAECBranchGroupUI = new ALAECBranchGroupUI();
        tALAECBranchGroupUI.submitData(tVData, "INSERT||MAIN");
        System.out.println("Error:" + tALAECBranchGroupUI.mErrors.getFirstError());

    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(this.mGlobalInput);
            mInputData.add(this.mLABranchGroupSchema);
            if(this.mLABranchGroupSchema.getBranchType().equals("2")
            		&&(this.mLABranchGroupSchema.getBranchType2().equals("02")
            				||this.mLABranchGroupSchema.getBranchType2().equals("04"))){
            	mInputData.add(this.mCostCenter);
            }
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAECBranchGroupUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLABranchGroupSchema.setSchema((LABranchGroupSchema) cInputData.
                                            getObjectByObjectName(
                "LABranchGroupSchema", 0));
        if(this.mLABranchGroupSchema.getBranchType().equals("2")
        		&&(this.mLABranchGroupSchema.getBranchType2().equals("02")
        				||this.mLABranchGroupSchema.getBranchType2().equals("04"))){
        	this.mCostCenter=(String)cInputData.getObject(0);
            if(mCostCenter==null){
            	CError tError = new CError();
                tError.moduleName = "ALAECBranchGroupUI";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有得到隐藏的成本中心编码信息！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        
        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAECBranchGroupUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

}
