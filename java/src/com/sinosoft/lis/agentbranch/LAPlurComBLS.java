package com.sinosoft.lis.agentbranch;


import java.sql.Connection;

import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LAComToAgentDB;
import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.lis.vdb.LAComToAgentBDBSet;
import com.sinosoft.lis.vdb.LAComToAgentDBSet;
import com.sinosoft.lis.vschema.LAComToAgentBSet;
import com.sinosoft.lis.vschema.LAComToAgentSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */

public class LAPlurComBLS
{

    public LAPlurComBLS()
    {
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Start LAPlurComBLS Submit...");
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            tReturn = saveLACom(cInputData);
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            tReturn = deleteLACom(cInputData);
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            tReturn = updateLACom(cInputData);
        }
        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End LAPlurComBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean saveLACom(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAPlurComBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAComDB tLAComDB = new LAComDB(conn);
            tLAComDB.setSchema((LAComSchema) mInputData.getObjectByObjectName(
                    "LAComSchema", 0));
            if (!tLAComDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAComDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAPlurComBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LAComToAgentDBSet tLAComToAgentDBSet = new LAComToAgentDBSet(conn);
            tLAComToAgentDBSet.set((LAComToAgentSet) mInputData.
                                   getObjectByObjectName("LAComToAgentSet", 0));
            if (tLAComToAgentDBSet.size() != 0)
            {
                if (!tLAComToAgentDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAComDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAPlurComBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            conn.commit();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAPlurComBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
            ;
        }
        try
        {
            conn.close();
        }
        catch (Exception e)
        {}
        ;
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean deleteLACom(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAPlurComBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAComDB tLAComDB = new LAComDB(conn);
            tLAComDB.setSchema((LAComSchema) mInputData.getObjectByObjectName(
                    "LAComSchema", 0));
            if (!tLAComDB.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAComDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAPlurComBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            LAComToAgentDBSet tLAComToAgentDBSet = new LAComToAgentDBSet(conn);
            tLAComToAgentDBSet.set((LAComToAgentSet) mInputData.
                                   getObjectByObjectName("LAComToAgentSet", 0));
            if (tLAComToAgentDBSet.size() != 0)
            {
                if (!tLAComToAgentDBSet.delete())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAComDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAPlurComBLS";
                    tError.functionName = "deleteData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            LAComToAgentBDBSet tLAComToAgentBDBSet = new LAComToAgentBDBSet(
                    conn);
            tLAComToAgentBDBSet.set((LAComToAgentBSet) mInputData.
                                    getObjectByObjectName("LAComToAgentBSet", 0));
            if (tLAComToAgentBDBSet.size() != 0)
            {
                if (!tLAComToAgentBDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAComDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAPlurComBLS";
                    tError.functionName = "deleteData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            conn.commit();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAPlurComBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
        }
        try
        {
            conn.close();
        }
        catch (Exception e)
        {}
        ;
        return tReturn;
    }


    /**
     * 保存函数
     */
    private boolean updateLACom(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAPlurComBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAComToAgentDB tLAComToAgentDB = new LAComToAgentDB(conn);
            LAComDB tLAComDB = new LAComDB(conn);
            tLAComDB.setSchema((LAComSchema) mInputData.getObjectByObjectName(
                    "LAComSchema", 0));
            tLAComToAgentDB.setAgentCom(tLAComDB.getAgentCom());
            if (!tLAComDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAComDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAPlurComBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LAComToAgentSet tLAComToAgentSet = tLAComToAgentDB.query();
            if (tLAComToAgentSet.size() != 0)
            {
                if (!tLAComToAgentDB.deleteSQL())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAComDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAPlurComBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            LAComToAgentDBSet tLAComToAgentDBSet = new LAComToAgentDBSet(conn);
            tLAComToAgentDBSet.set((LAComToAgentSet) mInputData.
                                   getObjectByObjectName("LAComToAgentSet", 0));
            if (tLAComToAgentDBSet.size() != 0)
            {
                if (!tLAComToAgentDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAComDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAPlurComBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            LAComToAgentBDBSet tLAComToAgentBDBSet = new LAComToAgentBDBSet(
                    conn);
            tLAComToAgentBDBSet.set((LAComToAgentBSet) mInputData.
                                    getObjectByObjectName("LAComToAgentBSet", 0));
            if (tLAComToAgentBDBSet.size() != 0)
            {
                if (!tLAComToAgentBDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAComDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAPlurComBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            conn.commit();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAPlurComBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
        }
        try
        {
            conn.close();
        }
        catch (Exception e)
        {}
        ;
        return tReturn;
    }

    public static void main(String[] args)
    {
        LAPlurComBLS LAPlurComBLS1 = new LAPlurComBLS();
    }
}
