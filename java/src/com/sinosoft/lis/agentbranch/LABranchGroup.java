/*
 * <p>ClassName: LAComToAgentBL </p>
 * <p>Description: LAContBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-27
 */
package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.db.LAComToAgentBDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAComToAgentSchema;
import com.sinosoft.lis.vschema.LAComToAgentBSet;
import com.sinosoft.lis.vschema.LAComToAgentSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class LABranchGroup
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /*时间变量*/
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAComToAgentSchema mLAComToAgentSchema = new LAComToAgentSchema();
    private LAComToAgentSet mLAComToAgentSet = new LAComToAgentSet();
//
    //当更新和删除的时候备份表
    private LAComToAgentBSet mLAComToAgentBSet = new LAComToAgentBSet();
    public LABranchGroup()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) throws
            Exception
    {
        //将操作数据拷贝到本类中
        System.out.println("操作符" + cOperate);
        this.mOperate = cOperate;
        System.out.println("come in BL");
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("come to check!");
        System.out.println("end check");
        //进行业务处理
        if (cOperate.equals("aa"))
        {
            if (!dealData())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAComToAgentBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败LAContBL-->dealData!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        else
        {
            if (!dealDatbb())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAComToAgentBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败LAContBL-->dealData!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        //准备往后台的数据
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        System.out.println("开始处理");
        boolean tReturn = true;
        for (int i = 1; i <= this.mLAComToAgentSet.size(); i++)
        {
            this.mLAComToAgentSchema = this.mLAComToAgentSet.get(i);
            System.out.println("中介机构：" + this.mLAComToAgentSchema.getAgentCom());
            if (this.mLAComToAgentSchema.getRelaType().equals("0"))
            {
                System.out.println("负责对应渠道组2:" +
                                   this.mLAComToAgentSchema.getAgentGroup());
                LAComToAgentBDB tLAComToAgentBDB = new LAComToAgentBDB();
                tLAComToAgentBDB.setAgentCode(mLAComToAgentSchema.getAgentCode());
                tLAComToAgentBDB.setAgentCom(mLAComToAgentSchema.getAgentCom());
                String bsql =
                        "select agentgroup from lacomtoagent where AgentCom='" +
                        mLAComToAgentSchema.getAgentCom() +
                        "' and RelaType='0'";
                ExeSQL aExeSQL = new ExeSQL();
                tLAComToAgentBDB.setAgentGroup(aExeSQL.getOneValue(bsql));
                System.out.println("Agaentgroup:" +
                                   tLAComToAgentBDB.getAgentGroup());
                tLAComToAgentBDB.setEdorType("01");
                String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
                tLAComToAgentBDB.setEdorNo(tEdorNo);
                tLAComToAgentBDB.setStartDate(mLAComToAgentSchema.getStartDate());
                tLAComToAgentBDB.setEndDate(mLAComToAgentSchema.getEndDate());
                tLAComToAgentBDB.setMakeDate(currentDate);
                tLAComToAgentBDB.setModifyDate(currentDate);
                tLAComToAgentBDB.setOperator(mGlobalInput.Operator);
                tLAComToAgentBDB.setRelaType("0");
                tLAComToAgentBDB.setMakeTime(currentTime);
                tLAComToAgentBDB.setModifyTime(currentTime);
                tLAComToAgentBDB.insert();
                String asql = "delete from LAComToAgent where AgentCom='" +
                              mLAComToAgentSchema.getAgentCom() +
                              "' and RelaType='0'";
                System.out.println("delete LAComToAgent:" + asql);
                ExeSQL tExeSQL = new ExeSQL();
                tReturn = tExeSQL.execUpdateSQL(asql);
                System.out.println("Return" + tReturn);
                System.out.println("删除branchattr……");
            }
        }
        tReturn = true;
        return tReturn;
    }

    private boolean dealDatbb()
    {
        System.out.println("开始处理");
        boolean tReturn = true;
        for (int i = 1; i <= this.mLAComToAgentSet.size(); i++)
        {
            this.mLAComToAgentSchema = this.mLAComToAgentSet.get(i);
            System.out.println("中介机构：" + this.mLAComToAgentSchema.getAgentCom());
            if (this.mLAComToAgentSchema.getRelaType().equals("1"))
            {
                System.out.println("负责对应渠道组2:" +
                                   this.mLAComToAgentSchema.getAgentCode());
                String bsql =
                        "select agentcode from LAComToAgent where AgentCom='" +
                        mLAComToAgentSchema.getAgentCom() +
                        "' and RelaType='1'";
                ExeSQL aExeSQL = new ExeSQL();
                String Agentcode = aExeSQL.getOneValue(bsql);
                System.out.println("代理人组别:" + Agentcode);
                String asql = "update LAComToAgent set agentcode=''" +
                              ",MakeDate='" + currentDate + "',ModifyDate='" +
                              currentDate + "',Operator='" +
                              mGlobalInput.Operator + "'  where Agentcode='" +
                              Agentcode + "'";
                System.out.println("update agentcode:" + asql);
                ExeSQL tExeSQL = new ExeSQL();
                tReturn = tExeSQL.execUpdateSQL(asql);
                System.out.println("Return" + tReturn);
                System.out.println("删除agentcode……");
            }
        }
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAComToAgentSet.set((LAComToAgentSet) cInputData.
                                  getObjectByObjectName("LAComToAgentSet", 1));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

}
