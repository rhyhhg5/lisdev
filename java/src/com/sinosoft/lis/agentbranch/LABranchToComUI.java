
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
package com.sinosoft.lis.agentbranch;

//
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;

public class LABranchToComUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
//业务处理相关变量
    private LABranchGroupSchema mAimLABranchSchema = new LABranchGroupSchema();
    private LABranchGroupSchema mAdjustLABranchSchema = new LABranchGroupSchema();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public LABranchToComUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        mInputData = (VData) cInputData.clone();
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据

        LABranchToComBL tLABranchToComBL = new LABranchToComBL();
        System.out.println("Start LABranchToComBL UI Submit...");
        tLABranchToComBL.submitData(mInputData, mOperate);
        System.out.println("End LABranchToComBL UI Submit...");
        //如果有需要处理的错误，则返回
        if (tLABranchToComBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLABranchToComBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LABranchToComBLUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mResult = tLABranchToComBL.getResult();
        return true;
    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ComCode = "86";


    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mAdjustLABranchSchema.setSchema((LABranchGroupSchema) cInputData.
                                             getObjectByObjectName(
                "LABranchGroupSchema", 1));
        this.mAimLABranchSchema.setSchema((LABranchGroupSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LABranchGroupSchema", 2));
        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABranchToComBLUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
