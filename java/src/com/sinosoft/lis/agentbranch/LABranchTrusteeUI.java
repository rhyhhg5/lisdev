package com.sinosoft.lis.agentbranch;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LABranchTrusteeUI {
   public CErrors mErrors=new CErrors();
   public VData mResult =new VData();
   private VData mInputData=new VData();
   private String mOperate="";

    public LABranchTrusteeUI() {
    }
    public boolean submitData(VData cInputData,String cOperate)
   {
       mOperate=cOperate;
       this.mInputData =(VData)cInputData.clone() ;
       LABranchTrusteeBL tLABranchTrusteeBL=new LABranchTrusteeBL();
       try
       {
           if (!tLABranchTrusteeBL.submitData(mInputData, mOperate))
           {
               this.mErrors.copyAllErrors(tLABranchTrusteeBL.mErrors);
               CError tError = new CError();
               tError.moduleName = "LABranchTrusteeUI";
               tError.functionName = "submitDat";
               tError.errorMessage = "BL�ദ��ʧ��!";
               this.mErrors.addOneError(tError);
               return false;
           }
       }catch(Exception e)
       {
           e.printStackTrace();
       }
       mResult=tLABranchTrusteeBL.getResult();
       return true;
   }

   public VData getResult()
   {
       return mResult;
   }


}
