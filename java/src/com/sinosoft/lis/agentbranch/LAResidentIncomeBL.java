package com.sinosoft.lis.agentbranch;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */

import com.sinosoft.lis.db.LAResidentIncomeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAResidentIncomeSchema;
import com.sinosoft.lis.vschema.LAResidentIncomeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LAResidentIncomeBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private MMap map = new MMap();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAResidentIncomeSchema mLAResidentIncomeSchema = new LAResidentIncomeSchema();
    public LAResidentIncomeBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println(mOperate);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAResidentIncomeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAResidentIncomeBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start LAResidentIncomeBL Submit...");
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
          // @@错误处理
          this.mErrors.copyAllErrors(tPubSubmit.mErrors);
          CError tError = new CError();
          tError.moduleName = "LAResidentIncomeBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
          this.mErrors.addOneError(tError);
          return false;
        }
        System.out.println("End LAResidentIncomeBL Submit...");
        return true;
    }

    /**
     * 
     * 校验数据
     */
    private boolean check(){
    	String startdate = this.mLAResidentIncomeSchema.getStartDate();
    	String enddate = this.mLAResidentIncomeSchema.getEndDate();
    	
    	SSRS ssRs = new SSRS();
    	ExeSQL exeSQL = new ExeSQL();
    	String sql  = "select * from LAResidentIncome where (startdate>'"+enddate+"' or "+
    	" enddate <'"+startdate+"') and ResidentType='"+mLAResidentIncomeSchema.getResidentType()+"'"
    	+" and managecom like '"+mLAResidentIncomeSchema.getManageCom()+"%'" +" and idx <> '"+mLAResidentIncomeSchema.getIdx()+"'";
    	String qsql  = "select * from LAResidentIncome where  ResidentType='"+mLAResidentIncomeSchema.getResidentType()+"'"
    	+" and managecom like '"+mLAResidentIncomeSchema.getManageCom()+"%'" +" and idx <> '"+mLAResidentIncomeSchema.getIdx()+"'";
    	if(exeSQL.execSQL(sql).getMaxRow()!=exeSQL.execSQL(qsql).getMaxRow()){
    		CError tError = new CError();
            tError.moduleName = "LAResidentIncomeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "该机构中同一居民类型下对应数据的日期区间与当前录入的日期区间相重叠，请重新录入！";
            this.mErrors.addOneError(tError);
    		return false;
    	}
    	return true;
    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        if (mOperate.equals("INSERT||MAIN"))
        {
            if(!check()){
            	return false;
            }
        	String idx = PubFun1.CreateMaxNo("LAResidentIDX", 6);
            this.mLAResidentIncomeSchema.setIdx(idx);
            this.mLAResidentIncomeSchema.setMakeDate(currentDate);
            this.mLAResidentIncomeSchema.setMakeTime(currentTime);
            this.mLAResidentIncomeSchema.setModifyDate(currentDate);
            this.mLAResidentIncomeSchema.setModifyTime(currentTime);
            mLAResidentIncomeSchema.setOperator(mGlobalInput.Operator);
            map.put(this.mLAResidentIncomeSchema,"INSERT" );
            
        }
        if (mOperate.equals("UPDATE||MAIN"))
        {
            if(!check()){
            	return false;
            }
            LAResidentIncomeDB tLAResidentIncomeDB = new LAResidentIncomeDB();
            tLAResidentIncomeDB.setIdx(mLAResidentIncomeSchema.getIdx());
            if (!tLAResidentIncomeDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAResidentIncomeBL";
                tError.functionName = "dealData";
                tError.errorMessage = "原居民收入查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LAResidentIncomeSchema tLAResidentIncomeSchema = new LAResidentIncomeSchema();
            tLAResidentIncomeSchema = tLAResidentIncomeDB.getSchema();
            tLAResidentIncomeSchema.setManageCom(mLAResidentIncomeSchema.getManageCom());
            tLAResidentIncomeSchema.setStartDate(mLAResidentIncomeSchema.getStartDate());
            tLAResidentIncomeSchema.setEndDate(mLAResidentIncomeSchema.getEndDate());
            tLAResidentIncomeSchema.setRecentPCDI(mLAResidentIncomeSchema.getRecentPCDI());
            tLAResidentIncomeSchema.setRecentPCNI(mLAResidentIncomeSchema.getRecentPCNI());
            tLAResidentIncomeSchema.setOperator(mGlobalInput.Operator);
            tLAResidentIncomeSchema.setModifyDate(currentDate);
            tLAResidentIncomeSchema.setModifyTime(currentTime);
            map.put(tLAResidentIncomeSchema,"UPDATE" );
        }
        
        if (mOperate.equals("DELETE||MAIN"))
        {
            LAResidentIncomeDB tLAResidentIncomeDB = new LAResidentIncomeDB();
            tLAResidentIncomeDB.setIdx(mLAResidentIncomeSchema.getIdx());
            LAResidentIncomeSchema tLAResidentIncomeSchema = new LAResidentIncomeSchema();
            tLAResidentIncomeSchema = tLAResidentIncomeDB.getSchema();
            if (!tLAResidentIncomeDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAResidentIncomeBL";
                tError.functionName = "dealData";
                tError.errorMessage = "原居民收入查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            map.put(tLAResidentIncomeSchema,"DELETE" );
        }
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAResidentIncomeSchema.setSchema((LAResidentIncomeSchema) cInputData.
                                         getObjectByObjectName(
                                                 "LAResidentIncomeSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        if (mGlobalInput == null || mLAResidentIncomeSchema == null) {
			CError tError = new CError();
			tError.moduleName = "LAResidentIncomeBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAResidentIncomeBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
