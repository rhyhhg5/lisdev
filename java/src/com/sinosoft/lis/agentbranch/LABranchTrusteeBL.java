/*
 * <p>ClassName: LABranchTrusteeBL </p>
 * <p>Description: LABranchTrusteeBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2005-07-25
 */


package com.sinosoft.lis.agentbranch;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.MMap;
import java.util.Date;
import  java.lang.Integer;
import java.math.BigInteger;
import com.sinosoft.utility.Reflections;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LABranchTrusteeBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
   public CErrors mErrors = new CErrors();
   private VData mResult = new VData();
   /** 往后面传输数据的容器 */
   private VData mInputData;
   /** 数据操作字符串 */
   private String mOperate;
   private String UpAgentCode;
   private String UpIdxNo;
   /** 全局数据 */
   private GlobalInput mGlobalInput = new GlobalInput();
   private MMap map = new MMap();

   private LABranchTrusteeSchema mLABranchTrusteeSchema = new LABranchTrusteeSchema();
   private LABranchTrusteeSchema UpLABranchTrusteeSchema=new LABranchTrusteeSchema();

    public LABranchTrusteeBL() {
    }
    public static void main(String[] args)
   {

   }

   public boolean submitData(VData cInputData, String cOperate)
 {
     String a="2001-01-01";
    //  Date tStartDate=(Date)a;   //将操作数据拷贝到本类中
     this.mOperate = cOperate;
     //得到外部传入的数据,将数据备份到本类中
     if (!getInputData(cInputData))
     {
         return false;
     }
     //进行业务处理
     if (!dealData())
     {
         // @@错误处理
         CError tError = new CError();
         tError.moduleName = "LABranchTrusteeBL";
         tError.functionName = "submitData";
         tError.errorMessage = "数据处理失败LABranchTrusteeBL-->dealData!";
         this.mErrors.addOneError(tError);
         return false;
     }
     //准备往后台的数据
     if (!prepareOutputData())
     {
         return false;
     }
     else
     {
     System.out.println("Start LABranchTrusteeBL Submit...");
     PubSubmit tPubSubmit = new PubSubmit();
     if (!tPubSubmit.submitData(mInputData, mOperate))
     {
       // @@错误处理
       this.mErrors.copyAllErrors(tPubSubmit.mErrors);
       CError tError = new CError();
       tError.moduleName = "LABranchTrusteeBL";
       tError.functionName = "submitData";
       tError.errorMessage = "数据提交失败!";
       this.mErrors.addOneError(tError);
       return false;
     }
     System.out.println("End LABranchTrusteeBL Submit...");
   }


     if (this.mOperate.equals("QUERY||MAIN"))
     {
         this.submitquery();
     }
//     else
//     {
//         System.out.println("Start LAAddressBL Submit...");
//         LABranchTrusteeBLS tLAAddressBLS = new LABranchTrusteeBLS();
//         tLAAddressBLS.submitData(mInputData, cOperate);
//         System.out.println("End LAAddressBL Submit...");
//         //如果有需要处理的错误，则返回
//         if (tLAAddressBLS.mErrors.needDealError())
//         {
//             // @@错误处理
//             this.mErrors.copyAllErrors(tLAAddressBLS.mErrors);
//             CError tError = new CError();
//             tError.moduleName = "LAAddressBL";
//             tError.functionName = "submitDat";
//             tError.errorMessage = "数据提交失败!";
//             this.mErrors.addOneError(tError);
//             return false;
//         }
//     }
     mInputData = null;
     return true;
 }

  private boolean getInputData(VData cInputData)
{
    this.mLABranchTrusteeSchema.setSchema((LABranchTrusteeSchema) cInputData.
                                 getObjectByObjectName(
                                         "LABranchTrusteeSchema", 0));
    this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                getObjectByObjectName("GlobalInput", 0));
    UpAgentCode=cInputData.get(2).toString();
    UpIdxNo=cInputData.get(3).toString();
//     this.mContType=(String)cInputData.getObjectByObjectName("String",0);

    return true;
}

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData()
 {
   boolean tReturn = true;
   String currentDate = PubFun.getCurrentDate();
   String currentTime = PubFun.getCurrentTime();
   String tBranchManager=mLABranchTrusteeSchema.getBranchManager();
   String tAgentCode=mLABranchTrusteeSchema.getAgentCode();
   int tIdx=mLABranchTrusteeSchema.getIdx();
   String tBranchType=mLABranchTrusteeSchema.getBranchType();
   String tBranchType2=mLABranchTrusteeSchema.getBranchType2();
   String tStartDate=mLABranchTrusteeSchema.getStartDate();
   String tEndDate=mLABranchTrusteeSchema.getEndDate();

   String unit="D";
   int tDataNo= PubFun.calInterval(tStartDate,tEndDate,unit);
   String Days="";
   Days=String.valueOf(tDataNo);
   System.out.println("||||||||||||||||||||||||||||||||||||||||||||||||||"+tDataNo);
   String tCount;
   int i;
   String tSQL = "select MAX(IdxNo) from LABranchTrustee where Idx="+tIdx+" and AgentCode = '"
                              + tAgentCode + "' and BranchManager='"+tBranchManager+"'";
   ExeSQL tExeSQL = new ExeSQL();
   tCount = tExeSQL.getOneValue(tSQL);
   System.out.println(tCount+"|||||||||||||||||||||||||||||||||||||||||||||||||||");
   if (tCount != null && !tCount.equals(""))
   {


   Integer tInteger = new Integer(tCount);
   i = tInteger.intValue();
   i = i + 1;
   }
   else
   {
   i = 1;
   }
   if (mOperate.equals("INSERT||MAIN"))
    {
      //得到最大的IdxNo

       System.out.println(i+"|||||||||||||||||||||||||||||||||||||||||||||||||||");
     String sql_hols="select * from lahols a,labranchgroup b where a.agentcode=b.branchmanager "
                      +" and a.agentcode='"+tBranchManager+"' and a.idx="+tIdx+" "
                      +" and a.BranchType='"+tBranchType+"' and a.BranchType2='"+tBranchType2+"'";
      SSRS tSSRS1 = new SSRS();
      ExeSQL tExeSQL1 = new ExeSQL();
      tSSRS1 = tExeSQL1.execSQL(sql_hols);
      int tCount1= tSSRS1.getMaxRow() ;
      if (tExeSQL1.mErrors.needDealError())
      {
          this.mErrors.copyAllErrors(tExeSQL1.mErrors);
          CError tError = new CError();
          tError.moduleName = "LABranchTrusteeBL";
          tError.functionName = "dealData";
          tError.errorMessage = "查询主管请假信息出错！";
          this.mErrors.addOneError(tError);
          return false;
      }
      if(tCount1<1)
      {
          CError tError = new CError();
          tError.moduleName = "LABranchTrusteeBL";
          tError.functionName = "dealData";
          tError.errorMessage = "查询主管请假信息失败！";
          this.mErrors.addOneError(tError);
          return false;
      }
      String sql_trustee="select * from labranchtrustee a where "
                      +"  a.BranchManager='"+tBranchManager+"' and a.idx="+tIdx+" "
                      +" and a.BranchType='"+tBranchType+"' and a.BranchType2='"+tBranchType2+"'";

      SSRS tSSRS2 = new SSRS();
      ExeSQL tExeSQL2 = new ExeSQL();
      tSSRS2 = tExeSQL2.execSQL(sql_trustee);
      int tCount2= tSSRS2.getMaxRow() ;
      if (tExeSQL2.mErrors.needDealError())
     {
          this.mErrors.copyAllErrors(tExeSQL2.mErrors);
          CError tError = new CError();
          tError.moduleName = "LABranchTrusteeBL";
          tError.functionName = "dealData";
          tError.errorMessage = "查询主管请假信息出错！";
          this.mErrors.addOneError(tError);
          return false;
      }
     System.out.println(tCount);
      if(tCount2>0)
      {
          CError tError = new CError();
          tError.moduleName = "LABranchTrusteeBL";
          tError.functionName = "dealData";
          tError.errorMessage = "该团队已经被托管,不能再进行托管,但可以修改该信息！";
          this.mErrors.addOneError(tError);
          return false;
      }

      mLABranchTrusteeSchema.setIdxNo(i);
      mLABranchTrusteeSchema.setAClass("01");
      mLABranchTrusteeSchema.setState("01");
      mLABranchTrusteeSchema.setVacDays(Days);
      mLABranchTrusteeSchema.setMakeDate(currentDate);
      mLABranchTrusteeSchema.setMakeDate(currentTime);
      mLABranchTrusteeSchema.setModifyDate(currentDate);
      mLABranchTrusteeSchema.setModifyTime(currentTime);
      map.put(mLABranchTrusteeSchema, "INSERT");
      System.out.println("|||||||||||||||||||||||||||||||||||||||||||||||||||"+mLABranchTrusteeSchema.getMakeDate());
    }
  if (mOperate.equals("UPDATE||MAIN"))
  {
      LABranchTrusteeDB tLABranchTrusteeDB = new LABranchTrusteeDB();
      tLABranchTrusteeDB.setAgentCode(UpAgentCode);
      tLABranchTrusteeDB.setIdx(this.mLABranchTrusteeSchema.
                                            getIdx());
      tLABranchTrusteeDB.setIdxNo(UpIdxNo);
      tLABranchTrusteeDB.setBranchManager(this.mLABranchTrusteeSchema.
                                            getBranchManager());
      if (!tLABranchTrusteeDB.getInfo())
      {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLABranchTrusteeDB.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "LABranchTrusteeBL";
                  tError.functionName = "dealData()";
                  tError.errorMessage = "不存在所要修改的机构!";
                  this.mErrors.addOneError(tError);
                  return false;
      }

      LABranchTrusteeSchema tLABranchTrusteeSchema = new LABranchTrusteeSchema();
      tLABranchTrusteeSchema = tLABranchTrusteeDB.getSchema();
      Reflections tReflections = new Reflections();

      tReflections.transFields(UpLABranchTrusteeSchema,tLABranchTrusteeSchema);

    //  UpLABranchTrusteeSchema=tLABranchTrusteeSchema;
      map.put(UpLABranchTrusteeSchema, "DELETE");
       System.out.println("||||||||||"+mLABranchTrusteeSchema.getAgentCode()+"||"+UpAgentCode);
      if(!UpAgentCode.equals(mLABranchTrusteeSchema.getAgentCode()))
      {
       System.out.println("||||||||||"+i+"|||");
       mLABranchTrusteeSchema.setIdxNo(i);
      }
      mLABranchTrusteeSchema.setMakeDate(tLABranchTrusteeSchema.getMakeDate());
      mLABranchTrusteeSchema.setMakeTime(tLABranchTrusteeSchema.getMakeTime());
      mLABranchTrusteeSchema.setAClass("01");
      mLABranchTrusteeSchema.setState("01");
      mLABranchTrusteeSchema.setVacDays(Days);
      mLABranchTrusteeSchema.setModifyDate(currentDate);
      mLABranchTrusteeSchema.setModifyTime(currentTime);
      map.put(mLABranchTrusteeSchema, "INSERT");
      System.out.println("|||||||||||||||||||||||||||||||||||123233|||||||||"+mLABranchTrusteeSchema.getVacDays());
  }
   return tReturn;
  }
   private boolean submitquery()
  {
        return true;
  }
   private boolean prepareOutputData()
  {
      try
      {
        this.mInputData = new VData();
        mInputData.add(map);
      }
      catch (Exception ex)
      {
            // @@错误处理
        CError tError = new CError();
        tError.moduleName = "LABranchTrusteeBL";
        tError.functionName = "prepareData";
        tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
        this.mErrors.addOneError(tError);
        return false;
      }
        return true;
  }

    public VData getResult()
    {
        return this.mResult;
    }

}
