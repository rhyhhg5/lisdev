/*
 * <p>ClassName: LAGroupUniteBL </p>
 * <p>Description: LAGroupUniteBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-07-19 09:58:25
 */
package com.sinosoft.lis.agentbranch;

import java.sql.Connection;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

public class LAGroupUniteBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();
    private LATreeSchema mLATreeSchema = new LATreeSchema();
    private String mWorkDate = PubFun.getCurrentDate();           // 程序运行日期
    private String mWorkTime = PubFun.getCurrentTime();           // 程序运行时间
    private String mAdjustDate = "";                             // 处理时间


    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        //进行数据验证
        if(!check())
        {
            System.out.println("对合并机构的验证没有通过!");
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupUniteBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAGroupUniteBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        // 准备传往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        return true;
    }

    /**
     * 准备传往后台的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        return true;
    }

    /**
     * 进行业务处理
     * @return boolean
     */
    private boolean dealData()
    {
        String tBranchAttrA = "";           // 目标机构外部编码
        String tBranchAttrU = "";           // 被合并机构外部编码
        String tBranchManagerU = "";        // 被合并机构管理员
        //被合并机构信息
        LABranchGroupSchema tLABranchGroupSchemaU = new LABranchGroupSchema();
        //被合并机构下业务人员
        LATreeSet tLATreeSet = new LATreeSet();
        //查询指定机构信息
        tLABranchGroupSchemaU = getGroupByBranchAttr(
            mLABranchGroupSet.get(2).getBranchAttr());
        if(tLABranchGroupSchemaU == null)
        {
            System.out.println("传入参数错误!");
            return false;
        }

        tBranchAttrA = mLABranchGroupSet.get(1).getBranchAttr();
        tBranchAttrU = mLABranchGroupSet.get(2).getBranchAttr();
System.out.println("合并机构:["+tBranchAttrU+"]到["+tBranchAttrA+"]");
        //查询机构下所有业务人员信息
        System.out.println("查询被合并机构人员！");
        tLATreeSet = getLATreeByAgentGroup(tLABranchGroupSchemaU.getBranchAttr(),
                                           tLABranchGroupSchemaU.getAgentGroup());
        if (tLATreeSet == null)
        {
            System.out.println("人员数据准备失败！");
            return false;
        }
        System.out.println("人员数据准备结束！");

        //进行人员调动

        System.out.println("开始进行人员调动：被合并机构 ---> 合并机构!" +mLATreeSchema.getStartDate());
        System.out.println("mLATreeSchema.getAstartDate()"+ mLATreeSchema.getAstartDate());
        if(!AdjustAgent(tBranchAttrA,tLABranchGroupSchemaU,tLATreeSet))
        {
            System.out.println("失败！人员调动动作结束！");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupUniteBL";
            tError.functionName = "dealData";
            tError.errorMessage = "在人员调动过程中失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        System.out.println("人员调动动作结束！");

        //判断如果机构有管理员对主管进行调动
        if(tLABranchGroupSchemaU.getBranchManager()!="" ||
                mLATreeSchema == null)
        {
            //处理被合并的机构主管到新岗位
            if (!removeAssess(mLATreeSchema.getAgentCode(), tBranchAttrA, tBranchAttrU)) {
                return false;
            }
        }
        //处理被合并机构的停业状态
        System.out.println("开始处理被合并机构状态！");
        if(!dealBranchGroup(tLABranchGroupSchemaU.getBranchAttr()))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupUniteBL";
            tError.functionName = "dealData";
            tError.errorMessage = "在处理被合并机构时失败!";
            this.mErrors.addOneError(tError);

            System.out.println("在处理被合并机构时失败!");
            return false;
        }
        System.out.println("被合并机构处理完毕！");

        return true;
    }

    /**
     * 对被合并的主管进行处理
     * @param pmAgentCode String
     * @param pmBranchAttrA String
     * @param pmBranchAttrU String
     * @return boolean
     */
    private boolean removeAssess(String pmAgentCode,
                                 String pmBranchAttrA,
                                 String pmBranchAttrU)
    {
        //人员行政信息
        LATreeSet tLATreeSet = new LATreeSet();
        LATreeSchema tLATreeSchema = new LATreeSchema();
        //人员行政信息
        LAAgentSet tLAAgentSet = new LAAgentSet();
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        //目标机构信息
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();

        //取得个人行政信息
        tLATreeSchema = queryLATreeByAgentCode(pmAgentCode);
        //判断查询是否成功
        if(tLATreeSchema == null)
        {
            return false;
        }

        //取得个人基本信息查询结果
        tLAAgentSchema = queryLAAgentByAgentCode(pmAgentCode);
        //判断是否获取成功
        if(tLAAgentSchema == null)
        {
            return false;
        }

        //取得目标机构的信息
        tLABranchGroupSchema = getGroupByBranchAttr(pmBranchAttrA);
        //判断查询是否成功
        if(tLABranchGroupSchema == null)
        {
            return false;
        }

        //设置被合并机构管理员在新机构中的相关信息
        tLAAgentSchema.setAgentGroup(tLABranchGroupSchema.getAgentGroup());
        tLAAgentSchema.setBranchCode(tLABranchGroupSchema.getAgentGroup());
        tLAAgentSchema.setModifyDate(mWorkDate);
        tLAAgentSchema.setModifyTime(mWorkTime);
        System.out.println("新职级:"+mLATreeSchema.getAgentGrade());
        tLATreeSchema.setAgentGrade(mLATreeSchema.getAgentGrade());
        System.out.println("原职级:"+mLATreeSchema.getAgentLastGrade());
        tLATreeSchema.setAgentLastGrade(mLATreeSchema.getAgentLastGrade());
        System.out.println("新职级起聘日期:"+mLATreeSchema.getStartDate());
        tLATreeSchema.setStartDate(mLATreeSchema.getStartDate());
        tLATreeSchema.setAgentGroup(tLABranchGroupSchema.getAgentGroup());
        tLATreeSchema.setBranchCode(tLABranchGroupSchema.getAgentGroup());
        tLATreeSchema.setModifyDate(mWorkDate);
        tLATreeSchema.setModifyTime(mWorkTime);

        //提交数据库
        if(!dealGroupManager(tLATreeSchema,tLAAgentSchema))
        {
            return false;
        }

        return true;
    }

    /**
     * 查询指定用户的行政信息
     * @param pmAgentCode String
     * @return LATreeSchema
     */
    private LATreeSchema queryLATreeByAgentCode(String pmAgentCode)
    {
        //人员基本信息
        LATreeDB tLATreeDB = new LATreeDB();
        LATreeSet tLATreeSet = new LATreeSet();

        //设置查询条件
        tLATreeDB.setAgentCode(pmAgentCode);
        //进行查询
        tLATreeSet = tLATreeDB.query();
        //访问数据库失败
        if (tLATreeDB.mErrors.needDealError()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupUniteBL";
            tError.functionName = "removeAssess";
            tError.errorMessage = "查询人员[" + pmAgentCode + "]失败！";
            this.mErrors.addOneError(tError);

            return null;
        }
        //查询无数据
        if (tLATreeSet == null || tLATreeSet.size() == 0) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupUniteBL";
            tError.functionName = "removeAssess";
            tError.errorMessage = "查询人员[" + pmAgentCode + "]失败！";
            this.mErrors.addOneError(tError);

            return null;
        }

        return tLATreeSet.get(1);
    }

    /**
     * 查询指定用户的基本信息
     * @param pmAgentCode String
     * @return boolean
     */
    private LAAgentSchema queryLAAgentByAgentCode(String pmAgentCode)
    {
        //人员基本信息
        LAAgentDB tLAAgentDB = new LAAgentDB();
        LAAgentSet tLAAgentSet = new LAAgentSet();

        //设置查询条件
        tLAAgentDB.setAgentCode(pmAgentCode);
        //进行查询
        tLAAgentSet = tLAAgentDB.query();
        //访问数据库失败
        if (tLAAgentDB.mErrors.needDealError())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupUniteBL";
            tError.functionName = "removeAssess";
            tError.errorMessage = "查询人员["+pmAgentCode+"]失败！";
            this.mErrors.addOneError(tError);

            return null;
        }
        //查询无数据
        if(tLAAgentSet == null || tLAAgentSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupUniteBL";
            tError.functionName = "removeAssess";
            tError.errorMessage = "查询人员["+pmAgentCode+"]失败！";
            this.mErrors.addOneError(tError);

            return null;
        }

        return tLAAgentSet.get(1);
    }

    /**
     * 进行后台处理,修改被合并机构的管理员信息
     * @param pmLATreeSchema LATreeSchema
     * @return boolean
     */
    private boolean dealGroupManager(LATreeSchema pmLATreeSchema,
                                     LAAgentSchema pmLAAgentSchema)
    {
        boolean tReturn = true;

        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        //conn=PubFun1.getDefaultConnection();
        conn = DBConnPool.getConnection();

        if (conn == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupUniteBL";
            tError.functionName = "dealGroupManager";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            //修改行政信息
            LATreeDB tLATreeDB = new LATreeDB(conn);
            tLATreeDB.setSchema(pmLATreeSchema);
            //进行数据库操作
            if (!tLATreeDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAGroupUniteBL";
                tError.functionName = "dealGroupManager";
                tError.errorMessage = "更新被合并机构管理员行政信息失败!";
                this.mErrors.addOneError(tError);
                System.out.println(tError.toString());
                conn.rollback();
                conn.close();

                return false;
            }
            //修改个人信息
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setSchema(pmLAAgentSchema);
            //进行数据库操作
            if(!tLAAgentDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                System.out.println(mErrors.getFirstError().toString());
                CError tError = new CError();
                tError.moduleName = "LAGroupUniteBL";
                tError.functionName = "dealGroupManager";
                tError.errorMessage = "更新被合并机构管理员基本信息失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();

                return false;
            }

            conn.commit();
            conn.close();
        }catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupUniteBL";
            tError.functionName = "dealGroupManager";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }

        return tReturn;
    }

    /**
     * 把被合并的机构制成停业状态
     * @param pmBranchAttrU String
     * @return boolean
     */
    private boolean dealBranchGroup(String pmBranchAttrU)
    {
        //被合并的管理机构
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        //设置查询条件
        tLABranchGroupDB.setBranchAttr(pmBranchAttrU);
        tLABranchGroupDB.setBranchType("2");
        tLABranchGroupDB.setBranchType2("01");
        tLABranchGroupSet = tLABranchGroupDB.query();
        CErrors tErrors = null;
        //访问数据库失败
        if (tLABranchGroupDB.mErrors.needDealError())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupUniteBL";
            tError.functionName = "submitData";
            tError.errorMessage = "查询["+pmBranchAttrU+"]机构失败！";
            this.mErrors.addOneError(tError);

            return false;
        }
        //查询数据失败
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupUniteBL";
            tError.functionName = "submitData";
            tError.errorMessage = "查询["+pmBranchAttrU+"]机构失败！";
            this.mErrors.addOneError(tError);

            return false;
        }

        //取得机构数据
        tLABranchGroupSchema = tLABranchGroupSet.get(1);
        //设置机构停业标志
        tLABranchGroupSchema.setEndFlag("Y");
        //设置机构停业日期
        tLABranchGroupSchema.setEndDate(mLATreeSchema.getStartDate());

        //创建管理机构管理类对象
        ALABranchGroupUI tLABranchGroup = new ALABranchGroupUI();

        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.addElement(tLABranchGroupSchema);

        try
        {
            tLABranchGroup.submitData(tVData, "UPDATE||MAIN");
        }
        catch(Exception ex)
        {
            // @@错误处理
            CError ttError = new CError();
            ttError.moduleName = "LAGroupUniteBL";
            ttError.functionName = "dealBranchGroup";
            ttError.errorMessage = "关闭被合并机构时失败，原因是:" + ex.toString();
            System.out.println("关闭被合并机构时失败，原因是:" + ex.toString());
            this.mErrors.addOneError(ttError);

            return false;
        }

        tErrors = tLABranchGroup.mErrors;
        if(tErrors.needDealError())
        {
            mErrors.copyAllErrors(tErrors);
            System.out.println("错误原因："+tErrors.getFirstError());
            return false;
        }

        return true;
    }

    /**
     * 把指定业务员向指定机构作人员调动
     * @param pmBranchAttr String
     * @param pmBranchManager String
     * @param pmLATreeSet LATreeSet
     * @return boolean
     */
    private boolean AdjustAgent(String pmBranchAttr,
                                LABranchGroupSchema pmLABranchGroupSchemaU,
                                LATreeSet pmLATreeSet)
    {
        System.out.println("目标机构："+pmBranchAttr);
        //输入参数
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        LATreeSet tLATreeSet = new LATreeSet();
        AdjustAgentUI tAdjustAgentUI = new AdjustAgentUI();
        //错误信息
        String Content = "";
        String FlagStr = "";
        CErrors tError = null;

        tLABranchGroupSchema = getGroupByBranchAttr(pmBranchAttr);
        System.out.println("mLATreeSchema.getAstartDate()"+ mLATreeSchema.getAstartDate());
        //获取要调动的人员信息
        for(int i=1; i<=pmLATreeSet.size();i++)
        {
            LATreeSchema tLATreeSchema = new LATreeSchema();
            //挑出允许调动的成员(排除部门主管)
            if(pmLATreeSet.get(i).getAgentCode()
               .equals(pmLABranchGroupSchemaU.getBranchManager()))
            {
                continue;
            }
            System.out.println("开始检验佣金计算时间");
            //检验佣金计算日期
            String sql = "select max(IndexCalNo) from lawage where agentcode='" +
                         pmLATreeSet.get(i).getAgentCode() + "'";
            ExeSQL tExeSQL = new ExeSQL();
            String MaxIndexCalNo = tExeSQL.getOneValue(sql);
            if (MaxIndexCalNo != null && !"".equals(MaxIndexCalNo)) {
                String WageCalDate = MaxIndexCalNo;
                WageCalDate += "01";
                WageCalDate = AgentPubFun.formatDate(WageCalDate, "yyyy-MM-dd");
                WageCalDate = PubFun.calDate(WageCalDate, 1, "M", "");
                System.out.println("WageCalDate"+ WageCalDate);
                System.out.println("pmLATreeSet.get(i).getAstartDate()"+ pmLATreeSet.get(i).getAstartDate());
                System.out.println("mAdjustDate"+ mAdjustDate);
                if (!WageCalDate.equals(mAdjustDate)) {  // pmLATreeSet.get(i).getAstartDate()
                    CError ttError = new CError();
                    ttError.moduleName = "AdjustAgentBL";
                    ttError.functionName = "dealData";
                    ttError.errorMessage = "业务员" +
                                          pmLATreeSet.get(i).getAstartDate() +
                                          "最近一次计算佣金是" + MaxIndexCalNo +
                                          ",因此调动日期必须为" + WageCalDate + "";
                    this.mErrors.addOneError(ttError);

                    return false;
                    //continue;
                }
            }

            tLATreeSchema.setSchema(pmLATreeSet.get(i));
            tLATreeSchema.setAstartDate(mAdjustDate);
            tLATreeSet.add(tLATreeSchema);
        }
        System.out.println("被调动的人数："+tLATreeSet.size());
        //如果机构中没人不做人员调动操作
        if(tLATreeSet.size()==0)
        {
            return true;
        }
System.out.println(tLATreeSet.get(1).getStartDate());
        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.addElement(tLABranchGroupSchema);
        tVData.addElement(tLATreeSet);

        try
        {
            tAdjustAgentUI.submitData(tVData, "INSERT||MAIN");
        }
        catch(Exception ex)
        {
            FlagStr = "Fail";
            //Content = "调动人员时失败，原因是:" + ex.toString();
            // @@错误处理
            CError ttError = new CError();
            ttError.moduleName = "LAGroupUniteBL";
            ttError.functionName = "AdjustAgent";
            ttError.errorMessage = "调动人员时失败，原因是:" + ex.toString();
            System.out.println("调动人员时失败，原因是:" + ex.toString());
            this.mErrors.addOneError(ttError);

            return false;
        }

        tError = tAdjustAgentUI.mErrors;
        if(tError.needDealError())
        {
            mErrors.copyAllErrors(tError);
            System.out.println("错误原因："+tError.getFirstError());
            return false;
        }

        return true;
    }

    /**
     * 根据机构内部编码查询机构下所有业务员
     * @param pmAgentGroup String
     * @return LATreeSchema
     */
    private LATreeSet getLATreeByAgentGroup(String pmBranchAttr,String pmAgentGroup)
    {
        //LATreeSchema tLATreeSchema = new LATreeSchema();
        LATreeSet tLATreeSet = new LATreeSet();
        LATreeDB tLATreeDB = new LATreeDB();

       /**
        tLATreeDB.setAgentGroup(pmAgentGroup);
        tLATreeDB.setBranchType("2");
        tLATreeDB.setBranchType2("01");
        tLATreeSet = tLATreeDB.query();
      */
     //校验人员时，去掉离职人员 modify by AILSA
        String tSql = "select * from  latree a where a.BranchType='2' and a.BranchType2='01' and a.agentgroup='" +
              pmAgentGroup
              + "' and a.agentcode not in "
              +" (select b.agentcode from laagent b where a.agentcode=b.agentcode and b.agentstate>='06' ) " ;
        tLATreeSet = tLATreeDB.executeQuery(tSql);


        //访问数据库失败
        if (tLATreeDB.mErrors.needDealError())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupUniteBL";
            tError.functionName = "submitData";
            tError.errorMessage = "查询["+pmBranchAttr+"]机构下业务员失败！";
            this.mErrors.addOneError(tError);

            return null;
        }
        //查询数据失败
        if (tLATreeSet == null || tLATreeSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupUniteBL";
            tError.functionName = "submitData";
            tError.errorMessage = "["+pmBranchAttr+"]机构下没有业务员，请直接对该机构作停业操作！";
            this.mErrors.addOneError(tError);

            return null;
        }

        return tLATreeSet;
    }

    /**
     * 根据传入的机构外部编码，查询指定机构信息
     * @param pmBranchAttr String
     * @return LABranchGroupSchema
     */
    private LABranchGroupSchema getGroupByBranchAttr(String pmBranchAttr)
    {
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();

        tLABranchGroupDB.setBranchAttr(pmBranchAttr);
        tLABranchGroupDB.setBranchType("2");
        tLABranchGroupDB.setBranchType2("01");
        tLABranchGroupSet = tLABranchGroupDB.query();
        //访问数据库失败
        if (tLABranchGroupDB.mErrors.needDealError())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupUniteBL";
            tError.functionName = "submitData";
            tError.errorMessage = "查询["+pmBranchAttr+"]机构信息失败！";
            this.mErrors.addOneError(tError);

            return null;
        }
        //查询失败
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupUniteBL";
            tError.functionName = "submitData";
            tError.errorMessage = "查询["+pmBranchAttr+"]机构信息失败！";
            this.mErrors.addOneError(tError);

            return null;
        }

        return tLABranchGroupSet.get(1);
    }

    /**
     * 进行数据验证
     * @return boolean
     */
    private boolean check()
    {
        // 验证被合并机构是否有业务员

        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLABranchGroupSet.set((LABranchGroupSet)cInputData.getObjectByObjectName(
                                   "LABranchGroupSet",0));
        this.mLATreeSchema.setSchema((LATreeSchema)cInputData.getObjectByObjectName(
                                     "LATreeSchema",0));
        this.mAdjustDate = mLATreeSchema.getStartDate();
        System.out.println("处理时间为 " + mAdjustDate);
        /*
        this.mLAAssessAccessorySet.set((LAAssessAccessorySet) cInputData.getObjectByObjectName(
                "LAAssessAccessorySet", 0));
        this.mBranchType = (String) cInputData.getObject(2);
        this.mBranchType2 = (String) cInputData.getObject(3);
        */
        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGroupUniteBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

}
