/*
 * <p>ClassName: AdjustAgentBLS </p>
 * <p>Description: AdjustAgentBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-27
 */
package com.sinosoft.lis.agentbranch;


import java.sql.Connection;

import com.sinosoft.lis.agentcalculate.DealBusinessData;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vdb.LAAgentBDBSet;
import com.sinosoft.lis.vdb.LAAgentDBSet;
import com.sinosoft.lis.vdb.LATreeAccessoryDBSet;
import com.sinosoft.lis.vdb.LATreeBDBSet;
import com.sinosoft.lis.vdb.LATreeDBSet;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

public class AdjustAgentBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /**
     * 数据操作字符串
     */
    private String mOperate;
    public AdjustAgentBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        System.out.println("Start AdjustAgentBLS Submit...");
        tReturn = saveAdjustAgent(cInputData);

        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End AdjustAgentBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     * @param mInputData VData
     * @return boolean
     */
    private boolean saveAdjustAgent(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            //更新机构表中的管理人员
            System.out.println("更新机构表中的管理人员...");
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB(conn);
            tLABranchGroupDB.setSchema((LABranchGroupSchema) mInputData.
                                       getObjectByObjectName(
                                               "LABranchGroupSchema", 1));
            String tBranch = tLABranchGroupDB.getAgentGroup();
            if ((tBranch != null) && (!tBranch.equals("")))
            {
                if (!tLABranchGroupDB.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLABranchGroupDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AdjustAgentBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "机构表中的管理人员更新失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            /*
             //备份修改了管理人员的机构纪录- -此段程序已无用
                        System.out.println("备份修改了管理人员的机构纪录...");
             LABranchGroupBDB tLABranchGroupBDB = new LABranchGroupBDB();
                        tLABranchGroupBDB.setSchema((LABranchGroupBSchema)mInputData.getObjectByObjectName("LABranchGroupBSchema",2));
                        tBranch = tLABranchGroupBDB.getAgentGroup();
                        if ((tBranch!=null)&&(!tBranch.equals("")))
                        {
               if (!tLABranchGroupBDB.insert())
              {
             // @@错误处理
                   this.mErrors.copyAllErrors(tLABranchGroupBDB.mErrors);
                   CError tError = new CError();
                   tError.moduleName = "AdjustAgentBLS";
                   tError.functionName = "saveData";
                   tError.errorMessage = "备份修改了管理人员的机构纪录保存失败!";
                   this.mErrors .addOneError(tError) ;
                   conn.rollback();
                   conn.close();
                   return false;
               }
                        }*/
            //更新所有调动人员和管理人员在行政表中的AgentGroup UpBranch
            System.out.println("更新所有调动人员和管理人员在行政表中的纪录...");
            LATreeSet tLATreeSet = new LATreeSet();
            tLATreeSet.set((LATreeSet) mInputData.getObjectByObjectName(
                    "LATreeSet",
                    3));
            LATreeDBSet tLATreeDBSet = new LATreeDBSet(conn);
            tLATreeDBSet.set(tLATreeSet);
            if (!tLATreeDBSet.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "AdjustAgentBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "更新所有调动人员和管理人员在行政表中的纪录失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            //备份所有调动人员和管理人员
            System.out.println("备份所有调动人员和管理人员...");
            LATreeBDBSet tLATreeBDBSet = new LATreeBDBSet(conn);
            tLATreeBDBSet.set((LATreeBSet) mInputData.getObjectByObjectName(
                    "LATreeBSet", 4));
            if (!tLATreeBDBSet.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeBDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "AdjustAgentBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "更新所有调动人员和管理人员在行政表中的纪录失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

//            LAAgentDBSet tLAAgentDBSet = new LAAgentDBSet(conn);
//            LAAgentBDBSet tLAAgentBDBSet = new LAAgentBDBSet(conn);
            if (((LAAgentSet) mInputData.getObjectByObjectName("LAAgentSet", 6)).
                size() != 0)
            { //jiangcx add for BK
                LAAgentDBSet tLAAgentDBSet = new LAAgentDBSet(conn);
                LAAgentBDBSet tLAAgentBDBSet = new LAAgentBDBSet(conn);
                tLAAgentDBSet.set((LAAgentSet) mInputData.getObjectByObjectName(
                        "LAAgentSet", 5));
                if (!tLAAgentDBSet.delete())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAAgentDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AdjustAgentBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "更新所有调动人员在LAAgent表的纪录失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
                tLAAgentDBSet.set((LAAgentSet) mInputData.getObjectByObjectName(
                        "LAAgentSet", 6));
                if (!tLAAgentDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAAgentDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AdjustAgentBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "插入所有调动人员在LAAgent表的纪录失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
                tLAAgentBDBSet.set((LAAgentBSet) mInputData.
                                   getObjectByObjectName(
                                           "LAAgentBSet", 7));
                if (!tLAAgentBDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAAgentDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AdjustAgentBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "备份所有调动人员在LAAgent表的纪录失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            else
            {
                LAAgentDBSet tLAAgentDBSet = new LAAgentDBSet(conn);
                //更新所有调动人员在LAAgent表中的AgentGroup
                System.out.println("更新所有调动人员在LAAgent表中的AgentGroup...");
                tLAAgentDBSet.set((LAAgentSet) mInputData.getObjectByObjectName(
                        "LAAgentSet", 5));
                if (!tLAAgentDBSet.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAAgentDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "AdjustAgentBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "更新所有调动人员在LAAgent表的纪录失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            //更新行政信息附属表
            LATreeAccessoryDBSet tLATreeAccessoryDBSet = new
                    LATreeAccessoryDBSet(
                            conn);
            LATreeAccessorySet tLATreeAccessorySet = (LATreeAccessorySet)
                    mInputData.
                    getObjectByObjectName("LATreeAccessorySet", 0);
            tLATreeAccessoryDBSet.add(tLATreeAccessorySet);
            if (!tLATreeAccessoryDBSet.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLATreeAccessoryDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "AdjustAgentBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "更新所有调动人员在LATreeAccessory表的纪录失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            //更新业绩
            String tBranchAttr = (String) mInputData.get(9);
            System.out.println("BranchAttr:" + tBranchAttr);
            if (!UpdateAgentGroup(tLATreeSet, tBranchAttr, conn))
            {
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustAgentBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    /**
     *更新所有调动的代理人在业务数据相关表的AgentGroup数据
     * @param cLATreeSet LATreeSet
     * @param cBranchAttr String
     * @param conn Connection
     * @return boolean
     */
    private boolean UpdateAgentGroup(LATreeSet cLATreeSet, String cBranchAttr,
                                     Connection conn)
    {
        String tAgentCode = "";
        String tStartDate = "";
        String tEndDate = "3000-12-31";
        String tAgentGroup = "";
        String tFlag = "Y";
        LATreeSchema tLATreeSchema;
        DealBusinessData tDealData;
        VData tVData = new VData();
        for (int i = 1; i <= cLATreeSet.size(); i++)
        {
            System.out.println("i:" + i);
            tDealData = new DealBusinessData();
            tLATreeSchema = cLATreeSet.get(i);
            tAgentCode = tLATreeSchema.getAgentCode();
            tAgentGroup = tLATreeSchema.getAgentGroup();
            tStartDate = tLATreeSchema.getAstartDate();
            tStartDate = AgentPubFun.ConverttoYMD(tStartDate);
            if (tStartDate == null)
            {
                CError tError = new CError();
                tError.moduleName = "AdjustAgentBLS";
                tError.functionName = "saveAdjustAgent";
                tError.errorMessage = "转换日期出错！";
                this.mErrors.addOneError(tError);
                return false;
            }
            tVData.clear();
            tVData.add(tAgentCode);
            tVData.add(tAgentGroup);
            tVData.add(tFlag);
            tVData.add(cBranchAttr);
            tVData.add(null); //IndexCalNo
            tVData.add(tAgentGroup);
            tDealData.inputPeriod(tStartDate, tEndDate);
            System.out.println("AgentCode:" + tAgentCode);
            System.out.println("AgentGroup:" + tAgentGroup);
            System.out.println("Flag:" + tFlag);
            System.out.println("cBranchAttr:" + cBranchAttr);
            System.out.println("tStartDate:" + tStartDate);
            System.out.println("tEndDate:" + tEndDate);
            if (!tDealData.updateAgentGroup(tVData, conn))
            {
                this.mErrors.copyAllErrors(tDealData.mErrors);
                CError tError = new CError();
                tError.moduleName = "AdjustAgentBLS";
                tError.functionName = "UpdateAgentGroup";
                tError.errorMessage = "更新业务数据失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }
}
