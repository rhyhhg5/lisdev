package com.sinosoft.lis.agentbranch;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class LAGrpSpecComBL
{

    public LAGrpSpecComBL()
    {
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LAComSchema mLAComSchema = new LAComSchema();

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LASpecComBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LASpecComBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("Start LAGrpSpecComBL Submit...");
        LAGrpSpecComBLS tLAGrpSpecComBLS = new LAGrpSpecComBLS();
        tLAGrpSpecComBLS.submitData(mInputData, cOperate);
        System.out.println("End LAGrpSpecComBL Submit...");
        //如果有需要处理的错误，则返回
        if (tLAGrpSpecComBLS.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAGrpSpecComBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "LASpecComBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            this.mLAComSchema.setMakeDate(currentDate);
            this.mLAComSchema.setMakeTime(currentTime);
            this.mLAComSchema.setModifyDate(currentDate);
            this.mLAComSchema.setModifyTime(currentTime);
        }
        else if (this.mOperate.equals("UPDATE||MAIN"))
        {
            mLAComSchema.setModifyDate(currentDate);
            mLAComSchema.setModifyTime(currentTime);
            mLAComSchema.setOperator(mGlobalInput.Operator);
        }
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mLAComSchema.setSchema((LAComSchema) cInputData.getObjectByObjectName(
                "LAComSchema", 0));
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mLAComSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGrpSpecComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public static void main(String[] args)
    {
        LASpecComBL LASpecComBL1 = new LASpecComBL();
        LAComSchema tLAComSchema = new LAComSchema();
        tLAComSchema.setAgentCom("3");
        tLAComSchema.setOperator("001");
        tLAComSchema.setAreaType(" ");
        tLAComSchema.setChannelType(" ");
        GlobalInput tG = new GlobalInput();
        tG.ComCode = "86110000";
        tG.ManageCom = "86110000";
        tG.Operator = "001";
        VData tVData = new VData();
        tVData.add(tLAComSchema);
        tVData.add(tG);
        LASpecComBL1.submitData(tVData, "UPDATE||MAIN");
    }
}
