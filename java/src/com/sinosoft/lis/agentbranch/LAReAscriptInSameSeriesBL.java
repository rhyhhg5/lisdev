package com.sinosoft.lis.agentbranch;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.vschema.LAAssessAccessorySet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LAAssessAccessorySchema;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.schema.LAAgentGradeSchema;
import com.sinosoft.lis.db.LAAgentGradeDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.db.LAWageDB;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.lis.vschema.LADimissionSet;
import com.sinosoft.lis.schema.LADimissionSchema;
import com.sinosoft.lis.db.LAAssessAccessoryDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.agentcalculate.CalFormDrawRate;

/**
 * <p>Title: AgentSystem</p>
 * <p>Description: 销售管理系统</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author caigang
 * @version 1.0
 */
public class LAReAscriptInSameSeriesBL {
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAssessAccessorySchema mLAAssessAccessorySchema = new
            LAAssessAccessorySchema();
    private LATreeSet mLATreeSet = new LATreeSet();
    private LATreeBSet mLATreeBSet = new LATreeBSet();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();
    private LADimissionSet mLADimissionSet = new LADimissionSet();
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    private LACommisionSet mgetLACommisionSet = new LACommisionSet();
    private VData mOutputData = new VData();
    private MMap mMap = new MMap();
    private String mAscriptDate = "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String empMonth = "";//入司月份
    private String empolyDate  = "";
    private String mIndexCalNo = "";
    private String mEdorNo = "";
    private String tEdorType = "31";
    public LAReAscriptInSameSeriesBL() {
    }

    private boolean getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLAAssessAccessorySchema = (LAAssessAccessorySchema) cInputData.
                                getObjectByObjectName("LAAssessAccessorySchema", 0);
        if (mLAAssessAccessorySchema == null || mAscriptDate == null) {
            CError tError = new CError();
            tError.moduleName = "LAAscriptInSameSeriesBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "未得到足够数据!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean check() {
        return true;
    }

    public boolean submitData(VData cInputData, String cOperate) {
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!check()) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mOutputData, "")) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAscriptInSameSeriesBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean dealData() {
    	  mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            boolean Flag = false;//用于判断是否是主管B01
            String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);//转储号
            String tStandAssessFlag = ""+mLAAssessAccessorySchema.getStandAssessFlag();
            String tIndexCalNo = mLAAssessAccessorySchema.getIndexCalNo();
            String tAgentCode = mLAAssessAccessorySchema.getAgentCode();
            String tAssessType = mLAAssessAccessorySchema.getAssessType();
            String tBranchType = mLAAssessAccessorySchema.getBranchType();
            String tBranchType2 = mLAAssessAccessorySchema.getBranchType2();
            String tAgentGrade = mLAAssessAccessorySchema.getAgentGrade();
            String tAgentGrade1 = mLAAssessAccessorySchema.getAgentGrade1();
            String tAgentSeries = ""+mLAAssessAccessorySchema.getAgentSeries();
            String tAgentSeries1 = ""+mLAAssessAccessorySchema.getAgentSeries1();
            if (!tAgentSeries.equals(tAgentSeries1)) {
                CError tError = new CError();
                tError.moduleName = "LAAscriptInSameSeriesBL";
                tError.functionName = "dealData";
                tError.errorMessage = "本模块只能处理同系列的职级的异动!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LAAgentDB tLAAgentDB = new LAAgentDB();
            LAAgentSchema tLAAgentSchema = new LAAgentSchema();
            tLAAgentDB.setAgentCode(tAgentCode);
            if (!tLAAgentDB.getInfo()) {
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAscriptInSameSeriesBL";
                tError.functionName = "dealData";
                tError.errorMessage = "未查询到" + tAgentCode + "的原始信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLAAgentSchema = tLAAgentDB.getSchema();
    		mAscriptDate = tIndexCalNo + "01";
    		mAscriptDate = AgentPubFun.formatDate(mAscriptDate, "yyyy-MM-dd");
    		mAscriptDate = PubFun.calDate(mAscriptDate, 1, "M", null);// 开始时间
//    		职级调整日期必须为考核月的下个月1号开始,除了新入司的以外
          	ExeSQL newExeSQL = new ExeSQL();
            String newSQL = "select employdate from laagent where agentcode='"+mLAAssessAccessorySchema.getAgentCode()+"' and branchtype='1' and branchtype2='05'";
            empolyDate = newExeSQL.getOneValue(newSQL);
            String empMonth= AgentPubFun.formatDate(empolyDate, "yyyyMM");
            mAscriptDate = tIndexCalNo +"01";
            mAscriptDate = AgentPubFun.formatDate(mAscriptDate, "yyyy-MM-dd");
            mAscriptDate = PubFun.calDate(mAscriptDate, 1, "M", null);
            
            String tAscriptTemp = AgentPubFun.formatDate(mAscriptDate, "yyyyMM");
            System.out.println("入司日期："+empMonth+"/归属日期："+tAscriptTemp);
            if(empMonth.compareTo(tIndexCalNo)==0){
            	mAscriptDate = empolyDate;
            } 
            if(empMonth.compareTo(tIndexCalNo)>0){
                CError tError = new CError();
                tError.moduleName = "LAAscriptInSameSeriesBL";
                tError.functionName = "dealData";
                tError.errorMessage = "职级调整日期不能早于入职日期!";
                this.mErrors.addOneError(tError);
                return false;
            } 
            String sql ="";
            String oldGP4 = ""+getAgentGradeInfo(tAgentGrade);
            String newGP4 = ""+getAgentGradeInfo(tAgentGrade1);
           
            if (oldGP4.equals("0") && newGP4.equals("1")) {
                if (tLAAgentSchema.getInDueFormDate() == null ||
                    tLAAgentSchema.getInDueFormDate().equals("")) {
                    LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
                    Reflections tReflections = new Reflections();
                    tReflections.transFields(tLAAgentBSchema, tLAAgentSchema);
                    tLAAgentBSchema.setIndexCalNo(tIndexCalNo);
                    tLAAgentBSchema.setEdorNo(tEdorNo);
                    tLAAgentBSchema.setEdorType(tEdorType);
                    tLAAgentBSchema.setMakeDate(currentDate);
                    tLAAgentBSchema.setMakeTime(currentTime);
                    tLAAgentBSchema.setModifyDate(currentDate);
                    tLAAgentBSchema.setModifyTime(currentTime);
                    tLAAgentBSchema.setOperator(mGlobalInput.Operator);
                    tLAAgentSchema.setInDueFormDate(mAscriptDate); //置转正日期
                    tLAAgentSchema.setModifyDate(currentDate);
                    tLAAgentSchema.setModifyTime(currentTime);
                    tLAAgentSchema.setOperator(mGlobalInput.Operator);
                    mLAAgentBSet.add(tLAAgentBSchema);
                }
                
//                sql =
//                        "update lacommision set fyc=directwage,fycrate=standfycrate,operator='" +
//                        mGlobalInput.Operator + "',ModifyDate='" + currentDate +
//                        "',ModifyTime='" + currentTime + "' where agentcode='" +
//                        tLAAgentSchema.getAgentCode() + "' and (caldate>='" +
//                        mAscriptDate + "' or caldate is null)";
//                mMap.put(sql, "UPDATE");
            }
            if (  (oldGP4.equals("0") && newGP4.equals("1") )||
               (oldGP4.equals("1") && newGP4.equals("0")) ){
                sql = "select * from lacommision where agentcode='" +tAgentCode
                      + "' and (caldate is null or caldate>='" + mAscriptDate +"')";
                LACommisionDB tLACommisionDB = new LACommisionDB();
                LACommisionSet tLACommisionSet = new LACommisionSet();
                tLACommisionSet = tLACommisionDB.executeQuery(sql);
                CalFormDrawRate tCalFormDrawRate = new CalFormDrawRate();
                VData temp = new VData();
                temp.clear();
                temp.add(tLACommisionSet);
                //传入的是一个AgentGrade，提数计算的时候传空，归属的时候传入的是新的职级
                temp.add(tAgentGrade1);

                if (!tCalFormDrawRate.submitData(temp, CalFormDrawRate.CALFLAG)) {
                    this.mErrors.copyAllErrors(tCalFormDrawRate.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAAscriptInDiffSeriesBL";
                    tError.functionName = "ascriptToBusSeries";
                    tError.errorMessage = "打折处理的时候出错！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                mgetLACommisionSet.clear();
                mgetLACommisionSet.set((LACommisionSet) tCalFormDrawRate.getResult().
                                    getObjectByObjectName("LACommisionSet", 0));
                mLACommisionSet.add(mgetLACommisionSet);
            }

            LATreeDB tLATreeDB = new LATreeDB();
            LATreeSchema tLATreeSchema = new LATreeSchema();
            tLATreeDB.setAgentCode(tAgentCode);
            if (!tLATreeDB.getInfo()) {
                this.mErrors.copyAllErrors(tLATreeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAscriptInSameSeriesBL";
                tError.functionName = "dealData";
                tError.errorMessage = "未查询到" + tAgentCode + "的行政信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLATreeSchema = tLATreeDB.getSchema();
            if (!tAgentGrade.equals(tAgentGrade1)) {
                LATreeBSchema tLATreeBSchema = new LATreeBSchema();
                Reflections tReflections = new Reflections();
                tReflections.transFields(tLATreeBSchema, tLATreeSchema);
                tLATreeBSchema.setEdorNO(tEdorNo);
                tLATreeBSchema.setRemoveType(tEdorType);
                tLATreeBSchema.setOperator2(tLATreeBSchema.getOperator());
                tLATreeBSchema.setMakeDate2(tLATreeSchema.getMakeDate());
                tLATreeBSchema.setMakeTime2(tLATreeSchema.getMakeTime());
                tLATreeBSchema.setModifyDate2(tLATreeSchema.getModifyDate());
                tLATreeBSchema.setModifyTime2(tLATreeSchema.getModifyTime());
                tLATreeBSchema.setMakeDate(currentDate);
                tLATreeBSchema.setMakeTime(currentTime);
                tLATreeBSchema.setModifyDate(currentDate);
                tLATreeBSchema.setModifyTime(currentTime);
                tLATreeBSchema.setIndexCalNo(tIndexCalNo);
                tLATreeBSchema.setOperator(mGlobalInput.Operator);
                mLATreeBSet.add(tLATreeBSchema);
                tLATreeSchema.setAgentGrade(tAgentGrade1);
                tLATreeSchema.setAgentSeries(AgentPubFun.getAgentSeries(tAgentGrade1));
                // ↓ *** add *** LiuHao *** 2005-11-16 *** 追加新机构代码 *****
//                tLATreeSchema.setAgentGroup(tAgentGroupNew);
//                tLATreeSchema.setBranchCode(tAgentGroupNew);
                // ↑ *** add *** LiuHao *** 2005-11-16 *** 追加新机构代码 *****

                tLATreeSchema.setAgentLastGrade(tAgentGrade);
                tLATreeSchema.setAgentLastSeries(AgentPubFun.getAgentSeries(tAgentGrade));
                tLATreeSchema.setOldStartDate(tLATreeSchema.getStartDate());
                tLATreeSchema.setOldEndDate(PubFun.calDate(mAscriptDate, -1,
                            "D", null));
                if(empMonth.compareTo(tIndexCalNo)==0)
                {
                	tLATreeSchema.setOldEndDate("");
                }

                if (tAgentGrade.compareTo(tAgentGrade1) > 0) { //降级
                    tLATreeSchema.setAssessType("2");
                } else if (tAgentGrade.compareTo(tAgentGrade1) < 0) {
                    tLATreeSchema.setAssessType("1"); //晋升
                } else {
                    tLATreeSchema.setAssessType("0"); //晋升
                }
                if(Flag)
                	tLATreeSchema.setUpAgent("");
                tLATreeSchema.setStartDate(mAscriptDate);
                tLATreeSchema.setAstartDate(mAscriptDate);
                tLATreeSchema.setModifyDate(currentDate);
                tLATreeSchema.setModifyTime(currentTime);
                if (tAgentSeries1 == null ||tAgentSeries1.equals("")) {
                        //被清退，置成清退状态
                    LADimissionSchema tLADimissionSchema = new
                            LADimissionSchema();
                    tLADimissionSchema.setAgentCode(tAgentCode);
                    tLADimissionSchema.setBranchType(tBranchType);
                    tLADimissionSchema.setBranchType2(tBranchType2);
                    tLADimissionSchema.setBranchAttr(AgentPubFun.
                            getAgentBranchAttr(tAgentCode));
                    tLADimissionSchema.setApplyDate(mAscriptDate);
                    tLADimissionSchema.setDepartRsn("考核清退");
                    tLADimissionSchema.setDepartTimes(1);
                    tLADimissionSchema.setDepartState("05");
                    tLADimissionSchema.setMakeDate(currentDate);
                    tLADimissionSchema.setMakeTime(currentTime);
                    tLADimissionSchema.setModifyDate(currentDate);
                    tLADimissionSchema.setModifyTime(currentTime);
                    tLADimissionSchema.setOperator(mGlobalInput.Operator);
                    mLADimissionSet.add(tLADimissionSchema);
                    tLAAgentSchema.setAgentState("05");

                }
            }
            mLAAssessAccessorySchema.setModifyDate(currentDate);
            mLAAssessAccessorySchema.setModifyTime(currentTime);
            mLAAssessAccessorySchema.setOperator(mGlobalInput.Operator);
            mLAAssessAccessorySchema.setState("2");
            // ↓ *** modify *** LiuHao *** 2005-11-16 *** 追加新机构代码 *****
            //tLAAssessAccessorySchema.setAgentGroupNew(tLAAssessAccessorySchema.getAgentGroup());
            // ↑ *** modify *** LiuHao *** 2005-11-16 *** 追加新机构代码 *****
            // ↓ *** add *** LiuHao *** 2005-11-16 *** 追加新机构代码 *****
//            tLAAgentSchema.setAgentGroup(tAgentGroupNew);
//            tLAAgentSchema.setBranchCode(tAgentGroupNew);
//            tLATreeSchema.setAgentGroup(tAgentGroupNew);
//            tLATreeSchema.setBranchCode(tAgentGroupNew);
            // ↑ *** add *** LiuHao *** 2005-11-16 *** 追加新机构代码 *****
            mLAAgentSet.add(tLAAgentSchema);
            mLATreeSet.add(tLATreeSchema);

//            if(tLAAssessAccessorySchema.getAgentGrade().equals("B01") && tLAAssessAccessorySchema.getAgentGrade1().compareTo("B01")>0)
//            {
//
//            }
         
        return true;
    }
    /*
    改变一个部下的所有的组长的上级代理人
    参数：cOldUpAgent-部的内部编码,cNewUpAgent-新的上级代理人的编码
    */
   public boolean changeUpAgentSet(String cOldUpAgent,String cNewUpAgent)
   {
       LATreeSet tLATreeSet=new LATreeSet();
       LATreeDB tLATreeDB=new LATreeDB();
       LATreeSchema tLATreeSchema;
       String strSQL="select * from latree a,laagent b where a.agentcode=b.agentcode and upagent='"
                     +cOldUpAgent+
                   //  "' and AgentSeries='"+cDownAgentSeries+
                     "' and b.agentstate<='04'  and a.agentcode<>'"+cOldUpAgent+"'";
       tLATreeSet=tLATreeDB.executeQuery(strSQL);
       if(tLATreeDB.mErrors.needDealError())
       {
           CError.buildErr(tLATreeDB.mErrors.getFirstError(),"ChangeUpAgentSet");
           return false;
       }
       for(int i=1;i<=tLATreeSet.size();i++)
       {
           tLATreeSchema=new LATreeSchema();
           tLATreeSchema=tLATreeSet.get(i);
           if (!backupAgentTree(tLATreeSchema.getSchema()))
           {
               CError.buildErr("备份代理人行政信息失败！", 
                       "ChangeUpAgentSet");
               return false;
           }
           tLATreeSchema.setUpAgent(cNewUpAgent);
           mLATreeSet.add(tLATreeSchema);
       }

       return true;
   }
   /**
	 * 
	 * 参数:cLATreeSchema-要备份的代理人行政信息
	 */
	private boolean backupAgentTree(LATreeSchema cLATreeSchema) {
		LATreeBSchema tLATreeBSchema = new LATreeBSchema();
		Reflections tReflections = new Reflections();
		tReflections.transFields(tLATreeBSchema, cLATreeSchema);
		tLATreeBSchema.setOperator2(cLATreeSchema.getOperator());
		tLATreeBSchema.setMakeDate2(cLATreeSchema.getMakeDate());
		tLATreeBSchema.setModifyDate2(cLATreeSchema.getModifyDate());
		tLATreeBSchema.setMakeTime2(cLATreeSchema.getMakeTime());
		tLATreeBSchema.setModifyTime2(cLATreeSchema.getModifyTime());

		tLATreeBSchema.setEdorNO(mEdorNo);
		tLATreeBSchema.setRemoveType("31");// 人员调岗结果
		tLATreeBSchema.setMakeDate(currentDate);
		tLATreeBSchema.setMakeTime(currentTime);
		tLATreeBSchema.setModifyDate(currentDate);
		tLATreeBSchema.setModifyTime(currentTime);
		tLATreeBSchema.setOperator(mGlobalInput.Operator);
		tLATreeBSchema.setIndexCalNo(mLAAssessAccessorySchema.getIndexCalNo());
		mLATreeBSet.add(tLATreeBSchema);
		return true;
	}
    private String getAgentGradeInfo(String aGradeCode){
        LAAgentGradeDB tLAAgentGradeDB = new LAAgentGradeDB();
        tLAAgentGradeDB.setGradeCode(aGradeCode);
        if (!tLAAgentGradeDB.getInfo()) {
            this.mErrors.copyAllErrors(tLAAgentGradeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAscriptInSameSeriesBL";
            tError.functionName = "getAgentGradeInfo";
            tError.errorMessage = "未查询到" + aGradeCode + "的描述信息!";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLAAgentGradeDB.getGradeProperty4();
    }

    private boolean prepareOutputData() {
        mOutputData = new VData();
       // mMap = new MMap();
        mMap.put(mLATreeSet, "UPDATE");
        mMap.put(mLATreeBSet, "INSERT");
        mMap.put(mLAAgentSet, "UPDATE");
        mMap.put(mLAAgentBSet, "INSERT");
        mMap.put(mLADimissionSet, "INSERT");
//        mMap.put(mLAAssessAccessorySchema, "UPDATE");
        mMap.put(mLACommisionSet, "UPDATE");
        mOutputData.add(mMap);
        return true;
    }

    public MMap getResult() {
        return mMap;
    }

}
