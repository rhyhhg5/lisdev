/*
 * <p>ClassName: ChangeBranchBLS </p>
 * <p>Description: ChangeBranchBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-27
 */
package com.sinosoft.lis.agentbranch;

import java.sql.Connection;

import com.sinosoft.lis.vdb.LAAgentBDBSet;
import com.sinosoft.lis.vdb.LAAgentDBSet;
import com.sinosoft.lis.vdb.LABranchGroupBDBSet;
import com.sinosoft.lis.vdb.LABranchGroupDBSet;
import com.sinosoft.lis.vdb.LACommisionDBSet;
import com.sinosoft.lis.vdb.LATreeAccessoryDBSet;
import com.sinosoft.lis.vdb.LATreeBDBSet;
import com.sinosoft.lis.vdb.LATreeDBSet;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

public class ChangeBranchNewBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    public ChangeBranchNewBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        System.out.println("Start ChangeBranchBLS Submit...");
        tReturn = saveChangeBranch(cInputData);

        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End ChangeBranchBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean saveChangeBranch(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ChangeBranchBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");

            LABranchGroupDBSet tLABranchGroupDBSet = new LABranchGroupDBSet(
                    conn);
            tLABranchGroupDBSet.set((LABranchGroupSet) mInputData.
                                    getObjectByObjectName("LABranchGroupSet", 0));
            if (tLABranchGroupDBSet != null && tLABranchGroupDBSet.size() > 0)
            {
                if (!tLABranchGroupDBSet.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLABranchGroupDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ChangeBranchBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "'上级机构'纪录集更新失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            //保存备份纪录集
            System.out.println("保存备份纪录集...");
            LABranchGroupBDBSet tLABranchGroupBDBSet = new LABranchGroupBDBSet(
                    conn);
            tLABranchGroupBDBSet.set((LABranchGroupBSet) mInputData.
                                     getObjectByObjectName("LABranchGroupBSet",
                    0));
            if (tLABranchGroupBDBSet != null && tLABranchGroupBDBSet.size() > 0)
            {
                if (!tLABranchGroupBDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLABranchGroupBDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ChangeBranchBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            System.out.println("start to 备份 LATreeB表");

            LATreeBDBSet tLATreeBDBSet = new LATreeBDBSet(conn);

            LATreeBSet tLATreeBSet = new LATreeBSet();
            tLATreeBDBSet.add((LATreeBSet) mInputData.getObjectByObjectName(
                    "LATreeBSet", 0));
            if (tLATreeBDBSet != null && tLATreeBDBSet.size() > 0)
            {
                if (!tLATreeBDBSet.insert())
                {
                    this.mErrors.copyAllErrors(tLATreeBDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ChangeBranchBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "人员行政备份数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;

                }
            }
            LATreeDBSet tLATreeDBSet = new LATreeDBSet(conn);
            LATreeSet tLATreeSet = new LATreeSet();
            tLATreeSet = (LATreeSet) mInputData.getObjectByObjectName(
                    "LATreeSet", 0);
            tLATreeDBSet.add(tLATreeSet);
            if (tLATreeDBSet != null && tLATreeDBSet.size() > 0)
            {
                if (!tLATreeDBSet.update())
                {
                    this.mErrors.copyAllErrors(tLATreeDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ChangeBranchBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "人员行政数据更新失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            LAAgentBDBSet tLAAgentBDBSet = new LAAgentBDBSet(conn);
            LAAgentBSet tLAAgentBSet = new LAAgentBSet();
            tLAAgentBSet = (LAAgentBSet) mInputData.getObjectByObjectName(
                    "LAAgentBSet", 0);
            tLAAgentBDBSet.add(tLAAgentBSet);
            if (tLAAgentBDBSet != null && tLATreeDBSet.size() > 0)
            {
                if (!tLAAgentBDBSet.insert())
                {
                    this.mErrors.copyAllErrors(tLAAgentBDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ChangeBranchBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "备份人员原始信息失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            LAAgentDBSet tLAAgentDBSet = new LAAgentDBSet(conn);
            LAAgentSet tLAAgentSet = new LAAgentSet();
            tLAAgentSet = (LAAgentSet) mInputData.getObjectByObjectName(
                    "LAAgentSet", 0);
            tLAAgentDBSet.add(tLAAgentSet);
            if (tLAAgentDBSet != null && tLAAgentDBSet.size() > 0)
            {
                if (!tLAAgentDBSet.update())
                {
                    this.mErrors.copyAllErrors(tLAAgentBDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ChangeBranchBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "更新人员原始信息失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            LATreeAccessoryDBSet tLATreeAccessoryDBSet = new
                    LATreeAccessoryDBSet(conn);
            LATreeAccessorySet tLATreeAccessorySet = new LATreeAccessorySet();
            tLATreeAccessorySet = (LATreeAccessorySet) mInputData.
                                  getObjectByObjectName("LATreeAccessorySet", 0);
            tLATreeAccessoryDBSet.add(tLATreeAccessorySet);
            if (tLATreeAccessoryDBSet != null &&
                tLATreeAccessoryDBSet.size() > 0)
            {
                if (!tLATreeAccessoryDBSet.update())
                {
                    this.mErrors.copyAllErrors(tLATreeAccessoryDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ChangeBranchBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "更新人员附属行政信息失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            LACommisionDBSet tLACommisionDBSet = new LACommisionDBSet(conn);
            tLACommisionDBSet.add((LACommisionSet) mInputData.
                                  getObjectByObjectName("LACommisionSet", 0));
            if (tLACommisionDBSet != null && tLACommisionDBSet.size() > 0)
            {
                if (!tLACommisionDBSet.update())
                {
                    this.mErrors.copyAllErrors(tLACommisionDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "ChangeBranchBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "更新业绩失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ChangeBranchBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

}
