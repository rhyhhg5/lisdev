package com.sinosoft.lis.agentbranch;


import java.sql.Connection;

import com.sinosoft.lis.db.LAComBDB;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LAComToAgentDB;
import com.sinosoft.lis.db.LAContDB;
import com.sinosoft.lis.schema.LAComBSchema;
import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.lis.schema.LAContSchema;
import com.sinosoft.lis.vdb.LAComToAgentBDBSet;
import com.sinosoft.lis.vdb.LAComToAgentDBSet;
import com.sinosoft.lis.vschema.LAComToAgentBSet;
import com.sinosoft.lis.vschema.LAComToAgentSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */

public class LAAMComBLS
{


    public LAAMComBLS()
    {
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    private VData mInputData = new VData();
    private String signDate1;

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Start LASpecComBLS Submit...");
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            tReturn = saveLACom(cInputData);
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            tReturn = deleteLACom(cInputData);
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            tReturn = updateLACom(cInputData);
        }
        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End LASpecComBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean saveLACom(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LASpecComBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAComDB tLAComDB = new LAComDB(conn);
            tLAComDB.setSchema((LAComSchema) mInputData.getObjectByObjectName(
                    "LAComSchema", 0));
            if (!tLAComDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAComDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LASpecComBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            System.out.println("dadadadadadadadad");
            LAComToAgentDBSet tLAComToAgentDBSet = new LAComToAgentDBSet(conn);
            tLAComToAgentDBSet.set((LAComToAgentSet) mInputData.
                                   getObjectByObjectName("LAComToAgentSet", 0));
            if (tLAComToAgentDBSet.size() != 0)
            {
                if (!tLAComToAgentDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAComDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LASpecComBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            System.out.println("dadadadad_________神马情况");
            /*
            LAContDB  tLAContDB = new LAContDB(conn);    
            tLAContDB.setSchema((LAContSchema) mInputData.getObjectByObjectName(
                    "LAContSchema", 0));
            System.out.println("插入不进去啊！！！！"+tLAContDB.getAgentCom());
            if (!tLAContDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAContDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LASpecComBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            */
            conn.commit();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LASpecComBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
            ;
        }
        try
        {
            conn.close();
        }
        catch (Exception e)
        {}
        ;
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean deleteLACom(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LASpecComBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAComDB tLAComDB = new LAComDB(conn);
            tLAComDB.setSchema((LAComSchema) mInputData.getObjectByObjectName(
                    "LAComSchema", 0));
            if (!tLAComDB.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAComDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LASpecComBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            LAComToAgentDBSet tLAComToAgentDBSet = new LAComToAgentDBSet(conn);
            tLAComToAgentDBSet.set((LAComToAgentSet) mInputData.
                                   getObjectByObjectName("LAComToAgentSet", 0));
            if (tLAComToAgentDBSet.size() != 0)
            {
                if (!tLAComToAgentDBSet.delete())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAComDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LASpecComBLS";
                    tError.functionName = "deleteData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            LAComToAgentBDBSet tLAComToAgentBDBSet = new LAComToAgentBDBSet(
                    conn);
            tLAComToAgentBDBSet.set((LAComToAgentBSet) mInputData.
                                    getObjectByObjectName("LAComToAgentBSet", 0));
            if (tLAComToAgentBDBSet.size() != 0)
            {
                if (!tLAComToAgentBDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAComDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LASpecComBLS";
                    tError.functionName = "deleteData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            LAContDB  tLAContDB = new LAContDB(conn);    
            tLAContDB.setSchema((LAContSchema) mInputData.getObjectByObjectName(
                    "LAContSchema", 0));
            if (!tLAContDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAContDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LASpecComBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LASpecComBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
        }
        try
        {
            conn.close();
        }
        catch (Exception e)
        {}
        ;
        return tReturn;
    }


    /**
     * 保存函数
     */
    private boolean updateLACom(VData mInputData)
    {
    	 
    	
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "LASpecComBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAComToAgentDB tLAComToAgentDB = new LAComToAgentDB(conn);
            LAComDB tLAComDB = new LAComDB(conn);
            tLAComDB.setSchema((LAComSchema) mInputData.getObjectByObjectName(
                    "LAComSchema", 0));
            tLAComToAgentDB.setAgentCom(tLAComDB.getAgentCom());
            if (!tLAComDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAComDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LASpecComBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LAComToAgentSet tLAComToAgentSet = tLAComToAgentDB.query();
            if (tLAComToAgentSet.size() != 0)
            {
                if (!tLAComToAgentDB.deleteSQL())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAComDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LASpecComBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            LAComToAgentDBSet tLAComToAgentDBSet = new LAComToAgentDBSet(conn);
            tLAComToAgentDBSet.set((LAComToAgentSet) mInputData.
                                   getObjectByObjectName("LAComToAgentSet", 0));
            if (tLAComToAgentDBSet.size() != 0)
            {
                if (!tLAComToAgentDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAComDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LASpecComBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            LAComToAgentBDBSet tLAComToAgentBDBSet = new LAComToAgentBDBSet(
                    conn);
            tLAComToAgentBDBSet.set((LAComToAgentBSet) mInputData.
                                    getObjectByObjectName("LAComToAgentBSet", 0));
            if (tLAComToAgentBDBSet.size() != 0)
            {
                if (!tLAComToAgentBDBSet.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLAComDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LASpecComBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            System.out.println("开始进行修改操作！！！！");
            
            /**
             *  关于lacomB 表数据修改
             */
            LAComBDB tLAComBDB = new LAComBDB(conn);
            tLAComBDB.setSchema((LAComBSchema)mInputData.getObjectByObjectName(
                    "LAComBSchema", 0));
            if (!tLAComBDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAComBDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LASpecComBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            
           /*
            this.signDate1 = (String)mInputData.getObject(4);
            LAContDB  tLAContDB = new LAContDB(conn);
            LAContSchema mLAContSchema = (LAContSchema) mInputData.getObjectByObjectName(
                    "LAContSchema", 0);
            tLAContDB.setSchema((LAContSchema) mInputData.getObjectByObjectName(
                    "LAContSchema", 0));
            System.out.println("数据传输过来");
           
            tLAContDB.setProtocolNo(mLAContSchema.getProtocolNo());
            System.out.println("开始了----------！！！！");
            if (!tLAContDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LASpecComBL";
                tError.functionName = "dealData";
                tError.errorMessage = "未发现号码为"+mLAContSchema.getAgentCom() +"的中介机构！";
                this.mErrors.addOneError(tError);
                return false;
            }
            
            System.out.println("过不去了！！");
            if (!tLAContDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAContDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LASpecComBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            */
            
            conn.commit();
        }
       
            /**
        if(signDate1.equals(mLAContSchema.getSignDate())){
        if (!tLAContDB.update())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAContDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LASpecComBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据保存失败!";
            this.mErrors.addOneError(tError);
            conn.rollback();
            conn.close();
            return false;
        }
       
    }
        else{
        tLAContDB.insert();
        }
        conn.commit();
       }
        */
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LASpecComBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
        }
        try
        {
            conn.close();
        }
        catch (Exception e)
        {}
        ;
        return tReturn;
    }

    public static void main(String[] args)
    {
        LASpecComBLS LASpecComBLS1 = new LASpecComBLS();
    }
}
