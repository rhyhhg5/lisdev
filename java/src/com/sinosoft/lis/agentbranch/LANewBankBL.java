package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAgenttempDB;
import com.sinosoft.lis.db.LAQualificationDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LAAgenttempSchema;
import com.sinosoft.lis.schema.LAQualificationBSchema;
import com.sinosoft.lis.schema.LAQualificationSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.schema.LATreeTempSchema;
import com.sinosoft.lis.vschema.LAQualificationSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.vschema.LAWarrantorSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: AgentSystem</p>
 * <p>Description: 销售管理——增员管理</p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: Sinosoft</p>
 * @author xjh
 * @version 1.0
 */

public class LANewBankBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private MMap map = new MMap();
//    private StringBuffer tSB=new StringBuffer();
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mAgentCode = "";
    private TransferData mTransferData;

    public String getAgentCode() {
        return mAgentCode;
    }

    public LANewBankBL() {
    }

    public static void main(String[] args) {
    	LANewBankBL tLANewBankBL = new LANewBankBL();
    }

    /**
      传输数据的公共方法X
     */
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Begin LANewBankBL.submitData.........");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start LANewBankBL Submit...");
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LANewBankBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量

        System.out.println("Begin LANewBankBL.getInputData.........");
        
        this.mTransferData=(TransferData)cInputData.getObjectByObjectName("TransferData", 0);
        
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LANewBankBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    /**
     * 业务处理主函数
     * @return boolean
     */
    public boolean dealData() {
        System.out.println("Begin LANewBankBL.dealData.........");
       if(this.mOperate.equals("INSERT||MAIN"))
       {
    	   String SQL="insert into ldcode values('bankno','"+this.mTransferData.getValueByName("Code")+"','"+this.mTransferData.getValueByName("CodeName")+"',NULL,NULL,NULL)";
    	   this.map.put(SQL, "INSERT");
       }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            System.out.println("Begin LANewBankBL.prepareOutputData.........");
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LANewBankBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

}
