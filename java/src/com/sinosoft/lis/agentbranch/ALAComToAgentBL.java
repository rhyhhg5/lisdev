/*
 * <p>ClassName: LAComToAgentBL </p>
 * <p>Description: LAContBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-
 */
package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAComToAgentBSchema;
import com.sinosoft.lis.schema.LAComToAgentSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ALAComToAgentBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /*时间变量*/
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    /** 数据操作字符串 */
    private String mOperate;
    private String AgentComB;
    private String AgentCom;
    private String AgentGroup;
    /** 业务处理相关变量 */
    private LAComToAgentSchema mLAComToAgentSchema = new LAComToAgentSchema();
    //当更新和删除的时候备份表
    private LAComToAgentBSchema mLAComToAgentBSchema = new LAComToAgentBSchema();
    public ALAComToAgentBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) throws
            Exception
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("come to check!");
        //给AgentGroup赋值
        if (!changeValiable())
        {
            return false;
        }
        System.out.println("end check");
        //备份操作要提前,置备份表参数
        this.backup();
        System.out.println("end backup");
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComToAgentBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAContBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        ALAComToAgentBLS tALAComToAgentBLS = new ALAComToAgentBLS();
        tReturn = tALAComToAgentBLS.submitData(mInputData, cOperate);
        if (!tReturn)
        {
            Exception ex = new Exception("程序抛出错误：保存失败！");
            throw ex;
        }
        return tReturn;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        System.out.println("中介机构：" + this.mLAComToAgentSchema.getAgentCom());
        this.mLAComToAgentSchema.setModifyDate(currentDate);
        this.mLAComToAgentSchema.setModifyTime(currentTime);
        this.mLAComToAgentSchema.setMakeDate(currentDate);
        this.mLAComToAgentSchema.setMakeTime(currentTime);
        this.mLAComToAgentSchema.setStartDate(currentDate);
        this.mLAComToAgentSchema.setOperator(mGlobalInput.Operator);
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAComToAgentSchema.setSchema((LAComToAgentSchema) cInputData.
                                           getObjectByObjectName(
                "LAComToAgentSchema", 1));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.AgentComB = (String) cInputData.get(2);
        this.AgentCom=(String) cInputData.get(3);
        this.AgentGroup=(String) cInputData.get(4);
        System.out.println("AgentComB" + AgentComB);
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLAComToAgentSchema);
            this.mInputData.add(this.mLAComToAgentBSchema);
            this.mInputData.add(this.AgentComB);
            this.mInputData.add(this.AgentCom);
            this.mInputData.add(this.AgentGroup);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComToAgentBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private boolean backup()
    {
        LAComToAgentSchema tLAComToAgentSchema;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        this.mLAComToAgentBSchema.setAgentCode(this.mLAComToAgentSchema.
                                               getAgentCode());
        this.mLAComToAgentBSchema.setAgentCom(this.mLAComToAgentSchema.
                                              getAgentCom());
        this.mLAComToAgentBSchema.setAgentGroup(this.mLAComToAgentSchema.
                                                getAgentGroup());
        this.mLAComToAgentBSchema.setRelaType(this.mLAComToAgentSchema.
                                              getRelaType());
        this.mLAComToAgentBSchema.setEdorNo(PubFun1.CreateMaxNo("EdorNo", 20));
        this.mLAComToAgentBSchema.setEdorType("1");
        this.mLAComToAgentBSchema.setMakeDate(currentDate);
        this.mLAComToAgentBSchema.setStartDate(currentDate);
        this.mLAComToAgentBSchema.setMakeTime(currentTime);
        this.mLAComToAgentBSchema.setModifyDate(currentDate);
        this.mLAComToAgentBSchema.setModifyTime(currentTime);
        this.mLAComToAgentBSchema.setOperator(mGlobalInput.Operator);
        return true;
    }

    private boolean changeValiable()
    {
        System.out.println("come to check in");
        if(!this.mLAComToAgentSchema.getAgentCode().equals("0000000000"))
        {
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(this.mLAComToAgentSchema.getAgentCode());
            tLAAgentDB.getInfo();
            if (tLAAgentDB.getAgentGroup() == null ||
                tLAAgentDB.getAgentGroup().length() == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAComToAgentBL";
                tError.functionName = "prepareData";
                tError.errorMessage = "不存在该代理人！";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mLAComToAgentSchema.setAgentGroup(tLAAgentDB.getAgentGroup());
        }
        return true;
    }
}
