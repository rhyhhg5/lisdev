package com.sinosoft.lis.agentbranch;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAgentBSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupBSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.vschema.LAAgentBSet;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LABranchGroupBSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LACommisionSet;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LABranchLevelSchema;
import com.sinosoft.lis.db.LABranchLevelDB;
import com.sinosoft.lis.vschema.LABranchLevelSet;
import java.math.BigInteger;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.agent.LARearPICCHBL;
import com.sinosoft.lis.agent.LARecomPICCHBL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.db.LARearRelationDB;
import sun.reflect.Reflection;
import com.sinosoft.lis.schema.LARearRelationBSchema;
import com.sinosoft.lis.vschema.LARearRelationBSet;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.db.LAGrpCommisionDetailDB;

import com.sinosoft.lis.vschema.LAGrpCommisionDetailSet;

import com.sinosoft.lis.schema.LAGrpCommisionDetailSchema;

import com.sinosoft.lis.schema.LAGrpCommisionDetailBSchema;
import com.sinosoft.lis.vschema.LAGrpCommisionDetailBSet;

public class LABranchToComBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String mOperate;
    private boolean IsSingle;

    /** 业务处理相关变量 */
    private  LAGrpCommisionDetailSet mLAGrpCommisionDetailSet = new LAGrpCommisionDetailSet();
    private  LAGrpCommisionDetailBSet mLAGrpCommisionDetailBSet = new LAGrpCommisionDetailBSet();
    private LABranchGroupSchema mAdjustLABranchSchema = new LABranchGroupSchema();
    private LARearRelationSet mLARearRelationSet = new LARearRelationSet();
    private LARearRelationBSet mLARearRelationBSet = new LARearRelationBSet();
    //插入到机构备份表,备份被调动机构的信息
    private LABranchGroupBSet mLABranchGroupBSet = new LABranchGroupBSet();

    //更新机构表中的upBranch，将被调动机构的上级机构相关信息更新
    private LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();

    private LATreeBSet mLATreeBSet = new LATreeBSet(); //备份管理人员的行政信息
    private LATreeSet mLATreeSet = new LATreeSet();
    private LAAgentBSet mLAAgentBSet = new LAAgentBSet();
    private LAAgentSet mLAAgentSet = new LAAgentSet();
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    private boolean ifChangeManageCom = false; //是否跨了管理机构
    private String mUpBranchAttr = "";
    private String NewBranchAttr = "";
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String NewBranchSeries = "";
    private String mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20); //一个人生成一个转储号
    private String mNewManageCom = "";

    private String mBranchManager = new String();
    private String mAdjustDate = "";
    private String[] mDayInfo = {"LAPresence","LAWelfareInfo","LAHols","LARewardPunish","LATrain","LAQualityAssess ","LAAnnuity","LAAnnuityFlag","LAMakeCont","LAOrphanPolicy","LAQualityAssess","LAViolatCont","LAAgenttemp"};
    private String[] mBussInfo = {"lccont","lcpol","lcgrpcont","lcgrppol","lpcont","lppol","lpgrpcont","lpgrppol"};
    private int mCount = 0 ;  //表示第几次生成BranchAttr(培养团队需要逐次加1)
    private MMap mMap = new MMap();

    public LABranchToComBL()
    {
    }
    /**
     * 校验的代码
     * @return
     */
    private boolean check()
    {
        String tStartDate = this.mAdjustDate;
        String tAdjustBranchAttr = this.mAdjustLABranchSchema.getBranchAttr();
        //


        String newStartDate = AgentPubFun.formatDate(tStartDate, "yyyy-MM-dd"); //调整日期转换格式
        String tDay = newStartDate.substring(newStartDate.lastIndexOf("-") + 1); //取停业所在月的日期
        if (!tDay.equals("01")) {
            CError tError = new CError();
            tError.moduleName = "LABranchToComBL";
            tError.functionName = "check";
            tError.errorMessage = "调整日期必须是从某个月的1号开始!";
            this.mErrors.addOneError(tError);
            return false;
        } else {
            String tOldManageCom = mAdjustLABranchSchema.getManageCom();
            String tNewManageCom = mNewManageCom;
            if (!tOldManageCom.substring(0,
                                         4).equals(tNewManageCom.substring(0, 4))) {
                CError.buildErr(this, "新的机构录入有误，只能在同一个分公司中调整！");
                return false;

            }

            for (int i = 1; i <= 2; i++) {
                String tManageCom;
                if(i==1)
                {
                    tManageCom=tOldManageCom;//必须是先校验来机构
                }
                else
                {
                    tManageCom=tNewManageCom;//新机构必须在老机构后面校验
                }
                String sql =
                        "select max(indexcalno) from lawage where managecom='" +
                        tManageCom + "'";
                ExeSQL tExeSQL = new ExeSQL();
                String maxIndexCalNo = tExeSQL.getOneValue(sql);
                System.out.println("---最近发工资的日期---" + maxIndexCalNo);
                String lastDate = PubFun.calDate(tStartDate, -1, "M", null);
                lastDate = AgentPubFun.formatDate(lastDate, "yyyyMM");

                if (maxIndexCalNo == null || maxIndexCalNo.trim().equals("")) {
                    //老机构没有计算新姿，则要报错
                    if (tOldManageCom.equals(tManageCom)) {
                        CError tError = new CError();
                        tError.moduleName = "LABranchToComBL";
                        tError.functionName = "check";
                        tError.errorMessage = "管理机构" + tManageCom +
                                              "未计算过工资，不能做调动!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    else
                    {
                        //新机构没有计算薪资可以调动，但是老机构必须上月计算过（i=1时已经校验）
                        continue;

                    }
                }
                if (maxIndexCalNo.trim().equals(lastDate.trim()))
                   continue;
                else {
                    CError tError = new CError();
                    tError.moduleName = "LABranchToComBL";
                    tError.functionName = "check";
                    tError.errorMessage = "上次发工资是" +
                                          maxIndexCalNo.substring(0, 4) +
                                          "年" +
                                          maxIndexCalNo.substring(4).trim() +
                                          "月，因此调整日期必须从这个月的下一个月1号！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }

        }
        return true;
    }
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */

     public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("Into LABranchToComBL !!! ");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!check())
            return false;
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABranchToComBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LABranchToComBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
            return false;

        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mInputData, "");
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LABranchToComBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupDB.setBranchAttr(mAdjustLABranchSchema.getBranchAttr());
        tLABranchGroupDB.setBranchType(mAdjustLABranchSchema.getBranchType() ) ;
        tLABranchGroupDB.setBranchType2(mAdjustLABranchSchema.getBranchType2());
        tLABranchGroupSet=tLABranchGroupDB.query();
        if (tLABranchGroupSet==null || tLABranchGroupSet.size() ==0)
        {
            CError tError = new CError();
            tError.moduleName = "LABranchToComBL";
            tError.functionName = "dealdata";
            tError.errorMessage = "未找到机构编码为" + mAdjustLABranchSchema.getBranchAttr() +
                                  "的机构信息!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mAdjustLABranchSchema = tLABranchGroupSet.get(1);

        if(!dealbranch(mAdjustLABranchSchema,mNewManageCom,""))
        {
            return false ;
        }
        return true;
    }

    private boolean dealbranch(LABranchGroupSchema cLABranchGroupSchema ,String cNewManageCom,String cUpBranchAttr)
    {
        String tBranchLevel=cLABranchGroupSchema.getBranchLevel();
        String tNewBranchAttr="";
        if(tBranchLevel.equals("03"))
        {//生成部代码
            tNewBranchAttr = createBranchAttr(cNewManageCom,
                                             cLABranchGroupSchema.
                                             getBranchLevel()); //自动生成机构代码

            if (tNewBranchAttr == null || tNewBranchAttr.equals("")) {
                return false;
            }

        }
        else
        {//生成其下级代码
             String tBranchAttr = cLABranchGroupSchema.getBranchAttr();
             tNewBranchAttr = cUpBranchAttr +
                                    tBranchAttr.substring(cUpBranchAttr.
                     length());

        }
        //备份数据
        if(!backupBranchGroup(cLABranchGroupSchema))
        {
            return false;
        }
        cLABranchGroupSchema.setBranchAttr(tNewBranchAttr);
        cLABranchGroupSchema.setManageCom(cNewManageCom);
        cLABranchGroupSchema.setModifyDate(currentDate);
        cLABranchGroupSchema.setModifyTime(currentTime);
        cLABranchGroupSchema.setOperator(mGlobalInput.Operator);
        this.mLABranchGroupSet.add(cLABranchGroupSchema);
        //处理扎账表信息
        if(!dealCommision(cLABranchGroupSchema.getAgentGroup(),tNewBranchAttr,cNewManageCom))
        {
            return false;
        }
        //处理不中的每个人员(用Agentgroup查询,比如是区,只查询区经理,区下的人员不查询)
        if(!dealAgent(cLABranchGroupSchema.getAgentGroup(),cNewManageCom))
        {
            return false;
        }

        //对被调动机构的下属机构作调动处理
        String sql = "select * from labranchgroup where upbranch = '" +
                     cLABranchGroupSchema.getAgentGroup() +
                     "' and (state is null or state<>'1') and (endflag is null or endflag<>'Y')";
        System.out.println(sql);
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(sql);
        for (int i = 1; i <= tLABranchGroupSet.size(); i++)
        {//如果存在下属机构
            LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
            tLABranchGroupSchema = tLABranchGroupSet.get(i);
            if (!dealbranch(tLABranchGroupSchema,mNewManageCom,cLABranchGroupSchema.getBranchAttr()))
            {
                return false;
            }

        }


        return true;

    }
  //备份数据
  /*
   备份目的机构
   cLABranchGroupSchema-要备份机构的信息
   */
  private boolean backupBranchGroup(LABranchGroupSchema cLABranchGroupSchema)
  {
      try{
          Reflections tReflections = new Reflections();
          LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
          LABranchGroupBSchema tLABranchGroupBDB = new LABranchGroupBSchema();
          tLABranchGroupSchema = cLABranchGroupSchema;
          tLABranchGroupBDB.setSchema((LABranchGroupBSchema) tReflections.
                                      transFields(
                                              tLABranchGroupBDB,
                                              tLABranchGroupSchema));
          tLABranchGroupBDB.setEdorNo(mEdorNo);
          tLABranchGroupBDB.setEdorType("02");
          tLABranchGroupBDB.setUpBranchAttr((cLABranchGroupSchema.getUpBranchAttr()==null||cLABranchGroupSchema.getUpBranchAttr().equals(""))?
                  "0":cLABranchGroupSchema.getUpBranchAttr());
          tLABranchGroupBDB.setOperator2(cLABranchGroupSchema.getOperator());
          tLABranchGroupBDB.setMakeDate2(cLABranchGroupSchema.getMakeDate());
          tLABranchGroupBDB.setModifyDate2(cLABranchGroupSchema.getModifyDate());
          tLABranchGroupBDB.setMakeTime2(cLABranchGroupSchema.getMakeTime());
          tLABranchGroupBDB.setModifyTime2(cLABranchGroupSchema.getModifyTime());
          tLABranchGroupBDB.setMakeDate(currentDate);
          tLABranchGroupBDB.setMakeTime(currentTime);
          tLABranchGroupBDB.setModifyDate(currentDate);
          tLABranchGroupBDB.setModifyTime(currentTime);
          tLABranchGroupBDB.setOperator(mGlobalInput.Operator);

          mLABranchGroupBSet.add(tLABranchGroupBDB);
      }
      catch(Exception e)
      {
          //dealErr(e.getMessage(),"AscriptBranchGroupOperateBL","backupBranchGroup");
           CError.buildErr(this, e.getMessage());
          return false;
      }
      return true;
    }
  //生成营业部（管理机构下最高级别的团队）
    private String createBranchAttr(String cManageCom,String cBranchLevel)
    {

        ExeSQL tExeSQL = new ExeSQL();
        String maxBranchAttr = "";
        String sql =
                "select max(branchattr) from labranchgroup where managecom like '" +
                cManageCom + "%' and BranchLevel='"+cBranchLevel+"' and BranchType='" +
                mBranchType + "' and BranchType2='" +
                mBranchType2+ "'";
        maxBranchAttr = tExeSQL.getOneValue(sql);
        if(maxBranchAttr==null || maxBranchAttr.equals(""))
        {
            maxBranchAttr=cManageCom+"01";
        }
        else
        {
            BigInteger tBigInteger = new BigInteger(maxBranchAttr);
            String tCount = String.valueOf(mCount);
            BigInteger tAdd = new BigInteger("1");
            tBigInteger = tBigInteger.add(tAdd);
            maxBranchAttr = tBigInteger.toString();
        }
        NewBranchAttr=maxBranchAttr;
        return maxBranchAttr;
    }
    private boolean dealAgent(String  cAgentGroup,String cNewManageCom)
    {
        String asql = "select * from laagent where AgentGroup ='" +
                      cAgentGroup + "' and agentstate<'06' ";

        System.out.println(asql);
        LAAgentSet tLAAgentSet = new LAAgentSet();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentSet = tLAAgentDB.executeQuery(asql);
        for(int i=1;i<=tLAAgentSet.size();i++)
        {
            LAAgentSchema tLAAgentSchema = new LAAgentSchema();
            tLAAgentSchema = tLAAgentSet.get(i);
            String tAgentCode = tLAAgentSchema.getAgentCode();
            LATreeDB tLATreeDB = new LATreeDB();
            tLATreeDB.setAgentCode(tLAAgentSchema.getAgentCode());
            tLATreeDB.getInfo();
            LATreeSchema tLATreeSchema = new LATreeSchema();
            tLATreeSchema = tLATreeDB.getSchema();

            if (!backupAgentTree(tLATreeSchema)) {
                return false;
            }
            if (!backupAgent(tLAAgentSchema)) {
                return false;
            }
            //update laagent
            tLAAgentSchema.setManageCom(cNewManageCom);
            tLAAgentSchema.setModifyDate(currentDate);
            tLAAgentSchema.setModifyTime(currentTime);
            tLAAgentSchema.setOperator(mGlobalInput.Operator);
            mLAAgentSet.add(tLAAgentSchema);
            //update latree
            tLATreeSchema.setManageCom(cNewManageCom);
            tLATreeSchema.setModifyDate(currentDate);
            tLATreeSchema.setModifyTime(currentTime);
            tLATreeSchema.setOperator(mGlobalInput.Operator);
            mLATreeSet.add(tLATreeSchema);
            //处理日常管理中的信息
            if (!dealDayInfo(tAgentCode,cNewManageCom)) {
                return false;
            }
            //处理核心业务信息
            if (!dealBussInfo(tAgentCode,cNewManageCom)) {
                            return false;
            }
        }

        return true;
    }
    private boolean dealDayInfo(String  cAgentCode,String cManageCom)
    {
        if (mDayInfo!=null && mDayInfo.length>0)
        {
            for(int i=0;i<mDayInfo.length;i++)
            {

                String tsql = "update "+mDayInfo[i] + " set  managecom='" + cManageCom +
                              "',ModifyTime = '" + currentTime +
                              "',ModifyDate = '" + currentDate +
                              "',operator='" + mGlobalInput.Operator +
                              "'  where agentcode='" + cAgentCode + "' ";
                mMap.put(tsql, "UPDATE");
            }
        }
        return true;
    }
    private boolean dealBussInfo(String  cAgentCode,String cManageCom)
    {
        if (mBussInfo!=null && mBussInfo.length>0)
        {
            for(int i=0;i<mBussInfo.length;i++)
            {

                String tsql = "update "+mBussInfo[i] + " set  managecom='" + cManageCom +
                              "',ModifyTime = '" + currentTime +
                              "',ModifyDate = '" + currentDate +
                              "',operator='" + mGlobalInput.Operator +
                              "'  where agentcode='" + cAgentCode + "' ";
                mMap.put(tsql, "UPDATE");

                //处理保单分配表
                tsql= "select * from lagrpcommisiondetail where agentcode='" + cAgentCode + "' ";
                LAGrpCommisionDetailDB tLAGrpCommisionDetailDB = new LAGrpCommisionDetailDB();
                LAGrpCommisionDetailSet tLAGrpCommisionDetailSet = new LAGrpCommisionDetailSet();
                tLAGrpCommisionDetailSet=tLAGrpCommisionDetailDB.executeQuery(tsql);
                for(int k=1;k<=tLAGrpCommisionDetailSet.size();k++)
                {
                    LAGrpCommisionDetailSchema tLAGrpCommisionDetailSchema = new LAGrpCommisionDetailSchema();
                    tLAGrpCommisionDetailSchema=tLAGrpCommisionDetailSet.get(k) ;
                    LAGrpCommisionDetailBSchema tLAGrpCommisionDetailBSchema = new LAGrpCommisionDetailBSchema();
                    //备份
                    Reflections tReflections= new Reflections();
                    tReflections.transFields(tLAGrpCommisionDetailBSchema,tLAGrpCommisionDetailSchema);
                    tLAGrpCommisionDetailBSchema.setMakeDate2(tLAGrpCommisionDetailBSchema.getMakeDate());
                    tLAGrpCommisionDetailBSchema.setMakeTime2(tLAGrpCommisionDetailBSchema.getMakeTime());
                    tLAGrpCommisionDetailBSchema.setModifyDate2(tLAGrpCommisionDetailBSchema.getModifyDate());
                    tLAGrpCommisionDetailBSchema.setModifyTime2(tLAGrpCommisionDetailBSchema.getModifyTime());
                    tLAGrpCommisionDetailBSchema.setEdorType("01");
                    tLAGrpCommisionDetailBSchema.setEdorNO(mEdorNo);
                    tLAGrpCommisionDetailSchema.setModifyDate(currentDate);
                    tLAGrpCommisionDetailSchema.setModifyTime(currentTime);
                    tLAGrpCommisionDetailSchema.setMakeDate(currentDate);
                    tLAGrpCommisionDetailSchema.setMakeTime(currentTime);
                    mLAGrpCommisionDetailBSet.add(tLAGrpCommisionDetailSchema);
                    //修改
                    tLAGrpCommisionDetailSchema.setMngCom(cManageCom);
                    tLAGrpCommisionDetailSchema.setModifyDate(currentDate);
                    tLAGrpCommisionDetailSchema.setModifyTime(currentTime);
                    mLAGrpCommisionDetailSet.add(tLAGrpCommisionDetailSchema);


                }


            }
        }
        return true;
    }
    private boolean dealCommision(String  cAgentGroup,String cBranchAttr,String cManageCom)
    {
        LACommisionSet tLACommisionSet = new LACommisionSet();
        String aasql = "select * from lacommision where agentgroup='" +
                       cAgentGroup + "' and branchtype='" +
                       mAdjustLABranchSchema.getBranchType() +
                       "' and branchType2='"
                       + mAdjustLABranchSchema.getBranchType2() +
                       "' and (caldate is null or caldate>='"
                       + this.mAdjustDate + "')";
        System.out.println(aasql);
        LACommisionDB tLACommisionDB = new LACommisionDB();
        tLACommisionSet = tLACommisionDB.executeQuery(aasql);
        for (int j = 1; j <= tLACommisionSet.size(); j++) {
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            tLACommisionSchema = tLACommisionSet.get(j);
            tLACommisionSchema.setBranchAttr(cBranchAttr);
            tLACommisionSchema.setManageCom(cManageCom);
            tLACommisionSchema.setModifyDate(currentDate);
            tLACommisionSchema.setModifyTime(currentTime);
            tLACommisionSchema.setOperator(mGlobalInput.Operator);
            this.mLACommisionSet.add(tLACommisionSchema);
        }
        return true;
    }

    /**
      *
      * 参数:cLATreeSchema-要备份的代理人行政信息
      */
     private boolean backupAgentTree(LATreeSchema cLATreeSchema)
     {
         LATreeBSchema tLATreeBSchema=new LATreeBSchema();
         Reflections tReflections=new Reflections();
         tReflections.transFields(tLATreeBSchema,cLATreeSchema);
         tLATreeBSchema.setOperator2(cLATreeSchema.getOperator());
         tLATreeBSchema.setMakeDate2(cLATreeSchema.getMakeDate());
         tLATreeBSchema.setModifyDate2(cLATreeSchema.getModifyDate());
         tLATreeBSchema.setMakeTime2(cLATreeSchema.getMakeTime());
         tLATreeBSchema.setModifyTime2(cLATreeSchema.getModifyTime());

         tLATreeBSchema.setEdorNO(mEdorNo);
         tLATreeBSchema.setRemoveType("02");//
         tLATreeBSchema.setMakeDate(currentDate);
         tLATreeBSchema.setMakeTime(currentTime);
         tLATreeBSchema.setModifyDate(currentDate);
         tLATreeBSchema.setModifyTime(currentTime);
         tLATreeBSchema.setOperator(mGlobalInput.Operator);

         mLATreeBSet.add(tLATreeBSchema);
         return true;
    }
    /*
         备份代理人的基本信息
         参数:cLAAgentSchema-要备份的代理人的基本信息
         */
    private boolean backupAgent(LAAgentSchema cLAAgentSchema)
    {
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        LAAgentBSchema tLAAgentBSchema = new LAAgentBSchema();
        Reflections tReflections = new Reflections();
        tLAAgentSchema = cLAAgentSchema;
        System.out.println(cLAAgentSchema.getAgentCode());
        tReflections.transFields(tLAAgentBSchema, tLAAgentSchema);
        System.out.println(tLAAgentBSchema.getAgentCode());
        tLAAgentBSchema.setEdorNo(mEdorNo);
        tLAAgentBSchema.setEdorType("02");
        tLAAgentBSchema.setOperator(mGlobalInput.Operator);

        mLAAgentBSet.add(tLAAgentBSchema);
        return true;

    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mMap.put(mLABranchGroupSet, "UPDATE");
            this.mMap.put(mLABranchGroupBSet, "INSERT");
            this.mMap.put(mLATreeSet, "UPDATE");
            this.mMap.put(mLATreeBSet, "INSERT");
            this.mMap.put(mLAAgentSet, "UPDATE");
            this.mMap.put(mLAAgentBSet, "INSERT");
            this.mMap.put(mLAGrpCommisionDetailSet, "UPDATE");
            this.mMap.put(mLAGrpCommisionDetailBSet, "INSERT");
            this.mMap.put(mLACommisionSet, "UPDATE");

            this.mInputData.add(this.mMap);

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABranchToComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        mResult=new VData();
        mResult.add(NewBranchAttr) ;
        return this.mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        this.mAdjustLABranchSchema.setSchema((LABranchGroupSchema) cInputData.
                                             getObjectByObjectName(
                                                     "LABranchGroupSchema", 1));
        this.mNewManageCom = (String) cInputData.getObject(2);
        this.mAdjustDate = (String) cInputData.getObject(3);

        mBranchType = mAdjustLABranchSchema.getBranchType();
        mBranchType2 = mAdjustLABranchSchema.getBranchType2();
        System.out.println("mAdjustDate:" + mAdjustDate);
        System.out.println("over getInputData.." +
                           this.mAdjustLABranchSchema.getAgentGroup());
        return true;
    }

}
