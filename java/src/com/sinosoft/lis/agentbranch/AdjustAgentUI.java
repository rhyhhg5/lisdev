/*
 * <p>ClassName: AdjustAgentUI </p>
 * <p>Description: AdjustAgentUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-27
 */
package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class AdjustAgentUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public AdjustAgentUI() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("end getInputData!");
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        AdjustAgentBL tAdjustAgentBL = new AdjustAgentBL();
        tAdjustAgentBL.submitData(cInputData,cOperate);
        //如果有需要处理的错误，则返回
        if (tAdjustAgentBL.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tAdjustAgentBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AdjustAgentUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustAgentUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "页面超时！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        return true;
    }
    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
        }
        catch (Exception ex) {
            return false;
        }
        return true;
    }



    public VData getResult() {
        return this.mResult;
    }
    public static void main(String[] args) {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";
        LABranchGroupSchema mLABranchSchema = new LABranchGroupSchema();
        mLABranchSchema.setAgentGroup("000000000474");
        mLABranchSchema.setBranchAttr("86110002");
        mLABranchSchema.setBranchLevel("22");
        mLABranchSchema.setBranchManager("");
        mLABranchSchema.setUpBranch("");
        mLABranchSchema.setBranchType("2"); //jiangcx add for BK
        LATreeSchema mManagerTreeSchema = new LATreeSchema();
        mManagerTreeSchema.setAgentCode("");
        LATreeSet mLATreeSet = new LATreeSet();
        LATreeSchema tLATreeSchema = new LATreeSchema();
        tLATreeSchema.setAgentCode("86110490"); //代理人代码
        tLATreeSchema.setAgentGrade("D08"); //代理人职级
        tLATreeSchema.setAstartDate("2005-01-01");
        mLATreeSet.add(tLATreeSchema);
        VData tVData = new VData();
        tVData.add(tG);
        tVData.addElement(mLABranchSchema);
        tVData.addElement(mManagerTreeSchema);
        tVData.addElement(mLATreeSet);
        AdjustAgentUI tAdjustAgentUI = new AdjustAgentUI();
        try {
            tAdjustAgentUI.submitData(tVData, "");
        } catch (Exception ex) {
            System.out.println("Error:" + tAdjustAgentUI.mErrors.getFirstError());
        }
        System.out.println("Error:" + tAdjustAgentUI.mErrors.getFirstError());
    }

}
