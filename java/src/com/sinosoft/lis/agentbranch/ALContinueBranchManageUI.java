/*
 * <p>ClassName: ALContinueBranchManageUI </p>
 * <p>Description: ALABankAgentBranchManagerUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 个险续期收费
 * @CreateDate：2019-02-19
 */
package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ALContinueBranchManageUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String mOperate;

//业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    public ALContinueBranchManageUI() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        mInputData = (VData) cInputData.clone();
        ALContinueBranchManageBL tALContinueBranchManageBL = new
        		ALContinueBranchManageBL();
        tALContinueBranchManageBL.submitData(mInputData, cOperate);
        System.out.println("End ALContinueBranchManageUI UI Submit...");
        //如果有需要处理的错误，则返回
        if (tALContinueBranchManageBL.mErrors.needDealError()) {
            // @@错误处理
            for (int i = 0;
                         i < tALContinueBranchManageBL.mErrors.getErrorCount();
                         i++) {
                System.out.println(tALContinueBranchManageBL.mErrors.
                                   getError(i).moduleName);
                System.out.println(tALContinueBranchManageBL.mErrors.
                                   getError(i).functionName);
                System.out.println(tALContinueBranchManageBL.mErrors.
                                   getError(i).errorMessage);
            }
            this.mErrors.copyAllErrors(tALContinueBranchManageBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALContinueBranchManageUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {

        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    public static void main(String[] args) {
        VData tVData = new VData();
        String sqlstr =
                "select * from LABranchGroup where AgentGroup = '000000000044'";
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        LABranchGroupDB tLABranchGroupDB = tLABranchGroupSchema.getDB();
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(sqlstr);
        if (tLABranchGroupSet.size() > 0) {
            tLABranchGroupSchema = tLABranchGroupSet.get(1);
            //tLABranchGroupSchema.setBranchManager("");
            //tLABranchGroupSchema.setBranchManagerName("");
            //tLABranchGroupSchema.setModifyDate("2005-08-01");
        } else {
            System.out.println("error");
            return;
        }
        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";
        tVData.clear();
        tVData.addElement(tG);
        tVData.addElement(tLABranchGroupSchema);
        tVData.add("1"); //PartTime
        tVData.add("2005-08-01"); //mAdjustDate
        tVData.add("1102000010"); //NewBranchManager
        ALABankAgentBranchManagerUI tALABankAgentBranchManagerUI = new
                ALABankAgentBranchManagerUI();
        if (!tALABankAgentBranchManagerUI.submitData(tVData, "")) {
            System.out.println("Error:" +
                               tALABankAgentBranchManagerUI.mErrors.
                               getFirstError());
        }
    }
}
