/*
 * <p>ClassName: LAComToAgentUI </p>
 * <p>Description: LAContUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-27
 */
package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAComToAgentSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ALAComToAgentUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private String AgentComB;
    private String AgentCom;
    private String AgentGroup;
//业务处理相关变量
    private LAComToAgentSchema mLAComToAgentSchema = new LAComToAgentSchema();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public ALAComToAgentUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) throws
            Exception
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("come in UI");
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("end getInputData!");
        //准备往后台的数据
        if (!prepareOutputData())
        {
            System.out.println("false in prepareoutput!");
            return false;
        }
        ALAComToAgentBL tALAComToAgentBL = new ALAComToAgentBL();
        System.out.println("操作" + cOperate);
        System.out.println("Start LAComToAgentUI Submit...");
        try
        {
            tALAComToAgentBL.submitData(mInputData, mOperate);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        System.out.println("End LAComToAgentUI Submit...");
        //如果有需要处理的错误，则返回
        if (tALAComToAgentBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tALAComToAgentBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAComToAgentUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mInputData = null;
        return true;
    }

    public static void main(String[] args)
    {
        LAComToAgentSchema tLAComToAgentSchema = new LAComToAgentSchema();
        ALAComToAgentUI tLAComToAgentUI = new ALAComToAgentUI();

        //输出参数
        CErrors tError = null;
        String tOperate = "DELETE||MAIN";
        GlobalInput tG = new GlobalInput();
        VData tVData = new VData();
        tVData.add(tG);
        try
        {
            System.out.println("start to UI");
            tLAComToAgentUI.submitData(tVData, tOperate);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(this.mGlobalInput);
            mInputData.add(this.mLAComToAgentSchema);
            mInputData.add(this.AgentComB);
            mInputData.add(this.AgentCom);
            mInputData.add(this.AgentGroup);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComToAgentUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        this.mLAComToAgentSchema = (LAComToAgentSchema) cInputData.
                                   getObjectByObjectName("LAComToAgentSchema",
                0);
        this.AgentComB = (String) cInputData.get(2);
        this.AgentCom=(String) cInputData.get(3);
        this.AgentGroup=(String) cInputData.get(4);
        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAComToAgentUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
