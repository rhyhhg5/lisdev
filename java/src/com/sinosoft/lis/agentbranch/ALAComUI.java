/*
 * <p>ClassName: ALAComUI </p>
 * <p>Description: ALAComUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 代理机构
 * @CreateDate：2003-01-09
 */
package com.sinosoft.lis.agentbranch;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAComSchema;
import com.sinosoft.lis.schema.LAContSchema;
import com.sinosoft.lis.vschema.LAComSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;


public class ALAComUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 存放查询结果 */
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private String Bank;
    private String tAcType="";//中介公司类型，PY银代，PJ交叉销售
    private String mEndFlag1;
    private String mAgentCom;
    private String mCoverFlag=null;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAComSchema mLAComSchema = new LAComSchema();
    private LAContSchema mLAContSchema = new LAContSchema();
    private LAComSet mLAComSet = new LAComSet();
    public ALAComUI()
    {
    }

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("over getInputData");
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        System.out.println("over dealData");
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        System.out.println("over prepareOutputData");
        ALAComBL tALAComBL = new ALAComBL();
        System.out.println("Start LACom UI Submit...");
        System.out.println("修改时从save页面获取的managecom为："+mLAComSchema.getManageCom());
        tALAComBL.submitData(mInputData, mOperate);
        mAgentCom=tALAComBL.getAgentCom();
        System.out.println("End LACom UI Submit...");
        //如果有需要处理的错误，则返回
        if (tALAComBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tALAComBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAComUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mOperate.equals("QUERY||MAIN"))
        {
            this.mResult.clear();
            this.mResult = tALAComBL.getResult();
        }
        mInputData = null;
        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(this.mGlobalInput);
            mInputData.add(this.mLAComSchema);
            mInputData.add(Bank);
            mInputData.add(tAcType);
            mInputData.add(mEndFlag1);
            mInputData.add(this.mCoverFlag);
            mInputData.add(this.mLAContSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAComUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = false;
        String SQL = "";
        //此处增加一些校验代码
        if (mLAComSchema.getBankType().equals("01"))
        {
  //          mLAComSchema.setUpAgentCom("");
        }
        else
        {
            SQL = "select 'X' From dual where '" + mLAComSchema.getUpAgentCom() +
                  "' like '" +Bank+ "%'";
            ExeSQL tExeSQL = new ExeSQL();
            if (!tExeSQL.getOneValue(SQL).equals("X") ||
                mLAComSchema.getAgentCom().equals(mLAComSchema.getUpAgentCom()))
            {
                System.out.println("-----------" + tExeSQL.getOneValue(SQL) +
                                   "----------");
                CError tError = new CError();
                tError.moduleName = "ALAComUI";
                tError.functionName = "dealData";
                tError.errorMessage = "上级代理机构录入有误！";
                this.mErrors.addOneError(tError);
                return false;
            }
            SQL = "select 'X' From lacom where agentcom ='"+mLAComSchema.getUpAgentCom()+"'";
            tExeSQL = new ExeSQL();
            if (!tExeSQL.getOneValue(SQL).equals("X") ||
                mLAComSchema.getAgentCom().equals(mLAComSchema.getUpAgentCom()))
            {
                System.out.println("-----------" + tExeSQL.getOneValue(SQL) +
                                   "----------");
                CError tError = new CError();
                tError.moduleName = "ALAComUI";
                tError.functionName = "dealData";
                tError.errorMessage = "上级代理机构录入有误！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        tReturn = true;
        return tReturn;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("12323");
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
            System.out.println("123234");
        this.mLAComSchema.setSchema((LAComSchema) cInputData.
                                    getObjectByObjectName("LAComSchema", 0));
        this.mLAContSchema.setSchema((LAContSchema) cInputData.
                getObjectByObjectName("LAContSchema", 0));
        System.out.println("123235");
        Bank=cInputData.get(2).toString();
         System.out.println("123235");
        tAcType=cInputData.get(3).toString();
        System.out.println("123236");
        mEndFlag1=cInputData.get(4).toString();
        this.mCoverFlag=cInputData.get(5).toString();
        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAComUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
    public String getAgentCom()
    {
        return mAgentCom;
    }


}
