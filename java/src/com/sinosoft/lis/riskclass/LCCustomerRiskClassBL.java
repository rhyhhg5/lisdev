package com.sinosoft.lis.riskclass;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LCCustomerRiskClassBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 往后面传输的数据库操作 */
    private MMap map = new MMap();
    /** 执行删除的数据库操作，放在最后 */
    private MMap mapDel = new MMap();
    /**用户登陆信息 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    //客户风险等级
    private LCCustomerRiskClassSet mLCCustomerRiskClassSet = new
            LCCustomerRiskClassSet();

    private LCCustomerRiskClassTraceSet mLCCustomerRiskClassTraceSet = new
            LCCustomerRiskClassTraceSet();

    //修改状态 初步分类为1 最终分类2
    private String mClassState = "";

    private String mOperator = "";

    private String mCurrentDate = "";

    public LCCustomerRiskClassBL() {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * @param: cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        System.out.println("after getInputData");

        //数据操作业务处理
        if (!dealData()) {
            return false;
        }

        System.out.println("after dealData");

        //准备给后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        System.out.println("after prepareOutputData");

        //数据提交
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCCustomerRiskClassBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;

        return true;
    }

    private boolean dealData() {

        LCCustomerRiskClassSet tLCCustomerRiskClassSet = new
                LCCustomerRiskClassSet();
        LCCustomerRiskClassTraceSet tLCCustomerRiskClassTraceSet = new
                LCCustomerRiskClassTraceSet();
        for (int i = 1; i <= mLCCustomerRiskClassSet.size(); i++) {
            String tCustomerNo = mLCCustomerRiskClassSet.get(i).getCustomerNo();
            System.out.println("客户号：" + tCustomerNo);
            if (tCustomerNo == null || "".equals(tCustomerNo)) {
                CError tError = new CError();
                tError.moduleName = "LCCustomerRiskClassBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "获取客户信息失败";
                mErrors.addOneError(tError);
                return false;
            }

            String tCustomerGrade = mLCCustomerRiskClassSet.get(i).
                                    getCustomerGrade();
            System.out.println("客户等级：" + tCustomerGrade);
            if (tCustomerGrade == null || "".equals(tCustomerGrade)) {
                CError tError = new CError();
                tError.moduleName = "LCCustomerRiskClassBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "获取客户等级信息失败";
                mErrors.addOneError(tError);
                return false;
            }

            String tClassReason = mLCCustomerRiskClassSet.get(i).getClassReason();
            System.out.println("分类原因：" + tClassReason);
            if (tClassReason == null || "".equals(tClassReason)) {
                CError tError = new CError();
                tError.moduleName = "LCCustomerRiskClassBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "获取分类原因息失败";
                mErrors.addOneError(tError);
                return false;
            }

            String tClassReasonDesc = mLCCustomerRiskClassSet.get(i).
                                      getClassReasonDesc();
            System.out.println("分类原因：" + tClassReasonDesc);
            if (tClassReasonDesc == null || "".equals(tClassReasonDesc)) {
                CError tError = new CError();
                tError.moduleName = "LCCustomerRiskClassBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "获取分类原因失败";
                mErrors.addOneError(tError);
                return false;
            }

            LCCustomerRiskClassDB tLCCustomerRiskClassDB = new
                    LCCustomerRiskClassDB();
            tLCCustomerRiskClassDB.setCustomerNo(tCustomerNo);
            if (!tLCCustomerRiskClassDB.getInfo()) {
                CError tError = new CError();
                tError.moduleName = "LCCustomerRiskClassBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "获取客户信息失败";
                mErrors.addOneError(tError);
                return false;
            }
            LCCustomerRiskClassSchema tLCCustomerRiskClassSchema =
                    tLCCustomerRiskClassDB.getSchema();
            tLCCustomerRiskClassSchema.setCustomerGrade(tCustomerGrade);
            tLCCustomerRiskClassSchema.setClassReason(tClassReason);
            tLCCustomerRiskClassSchema.setClassReasonDesc(tClassReasonDesc);
            tLCCustomerRiskClassSchema.setRemark(mLCCustomerRiskClassSet.
                                                 get(i).getRemark());
            if (!"2".equals(tLCCustomerRiskClassSchema.getClassState())) {
                tLCCustomerRiskClassSchema.setClassState(mClassState);
            }
            tLCCustomerRiskClassSchema.setConfirmOperator(mOperator);
            tLCCustomerRiskClassSchema.setConfirmDate(mCurrentDate);
            tLCCustomerRiskClassSchema.setOperator(mOperator);
            tLCCustomerRiskClassSchema.setModifyDate(mCurrentDate);
            tLCCustomerRiskClassSchema.setModifyTime(PubFun.getCurrentTime());

            tLCCustomerRiskClassSet.add(tLCCustomerRiskClassSchema);

            LCCustomerRiskClassTraceSchema tLCCustomerRiskClassTraceSchema = new
                    LCCustomerRiskClassTraceSchema();
            Reflections tref = new Reflections();
            tref.transFields(tLCCustomerRiskClassTraceSchema,
                             tLCCustomerRiskClassSchema);
            String tSerialNo = PubFun1.CreateMaxNo("LCCustRCNo",
                    tLCCustomerRiskClassSchema.getManageCom());
            tLCCustomerRiskClassTraceSchema.setSerialno(tSerialNo);
            tLCCustomerRiskClassTraceSet.add(tLCCustomerRiskClassTraceSchema);
        }
        map.put(tLCCustomerRiskClassSet, "UPDATE");
        map.put(tLCCustomerRiskClassTraceSet, "INSERT");
        return true;
    }


    private boolean getInputData(VData cInputData) {
        mGlobalInput.setSchema(
                (GlobalInput)
                cInputData.getObjectByObjectName("GlobalInput", 0));

        mLCCustomerRiskClassSet.add(
                (LCCustomerRiskClassSet)
                cInputData.getObjectByObjectName("LCCustomerRiskClassSet", 0));
        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));

        if (mLCCustomerRiskClassSet == null) {
            CError tError = new CError();
            tError.moduleName = "LCCustomerRiskClassBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "获取客户信息失败";
            mErrors.addOneError(tError);
            return false;
        }

        if (mLCCustomerRiskClassSet.size() <= 0) {
            CError tError = new CError();
            tError.moduleName = "LCCustomerRiskClassBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "获取客户信息失败";
            mErrors.addOneError(tError);
            return false;
        }

        if (mTransferData == null) {
            CError tError = new CError();
            tError.moduleName = "LCCustomerRiskClassBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "获取信息失败";
            mErrors.addOneError(tError);
            return false;
        }

        String tLoadFlag = (String) mTransferData.getValueByName("LoadFlag");
        if ("A".equals(tLoadFlag)) {
            mClassState = "1";
        } else if ("B".equals(tLoadFlag)) {
            mClassState = "2";
        } else {
            CError tError = new CError();
            tError.moduleName = "LCCustomerRiskClassBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "分类状态错误";
            mErrors.addOneError(tError);
            return false;
        }

        mOperator = mGlobalInput.Operator;
        mCurrentDate = PubFun.getCurrentDate();

        return true;
    }

    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCCustomerRiskClassBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错:" +
                                  ex.toString();
            mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    public static void main(String[] args) {
    }
}
