package com.sinosoft.lis.yinbaotongbank;

import java.text.DecimalFormat;

import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LDCode1Set;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BXHSendToBankABL {

	 /** 传入数据的容器 */
	  private VData mInputData = new VData();
	  /** 返回数据的容器 */
	  private VData mResult = new VData();
	  /** 提交数据的容器 */
	  private MMap map = new MMap();
	  /** 数据操作字符串 */
	  private String mOperate;
	  /** 错误处理类 */
	  public  CErrors mErrors = new CErrors();
	  /** 前台传入的公共变量 */
	  private GlobalInput mGlobalInput = new GlobalInput();

	  //业务数据
	  /** 提数开始日期 */
	  private String startDate = "";
	  /** 提数结束日期 */
	  private String endDate = "";
	  /** 银行编码 */
	  private String bankCode = "";
	  /** 总金额 */
	  private double totalMoney = 0;
	  /** 总笔数 */
	  private int sumNum = 0;

	  /** 批次号 */
	  private String serialNo = "";

	  private LJAGetSet outLJAGetSet = new LJAGetSet();
	  private LYSendToBankSet outLYSendToBankSet = new LYSendToBankSet();
	  private LYBankLogSet outLYBankLogSet = new LYBankLogSet();

	  public BXHSendToBankABL() {
	  }
	  
	  /**
	   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	   * @param cInputData 传入的数据,VData对象
	   * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
	   * @return 布尔值（true--提交成功, false--提交失败）
	   */
	  public boolean submitData(VData cInputData, String cOperate) {
	    //将操作数据拷贝到本类中
	    this.mInputData = (VData)cInputData.clone();
	    this.mOperate = cOperate;

	    //得到外部传入的数据,将数据备份到本类中
	    if (!getInputData()) return false;
	    System.out.println("---End getInputData---");

	    //进行业务处理
	    if (!dealData()) return false;
	    System.out.println("---End dealData---");

	    //银行代收
	    if (mOperate.equals("PAYMONEY")) {
	      //准备往后台的数据
	      if (!prepareOutputData()) return false;
	      System.out.println("---End prepareOutputData---");

	      System.out.println("Start PubSubmit BLS Submit...");
	      PubSubmit tPubSubmit = new PubSubmit();
	      if (!tPubSubmit.submitData(mInputData, mOperate)) {
	        // @@错误处理
	        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
	        mResult.clear();
	        return false;
	      }
	      System.out.println("End PubSubmit BLS Submit...");
	    }

	    return true;
	  }

	  /**
	   * 将外部传入的数据分解到本类的属性中
	   * @param: 无
	   * @return: boolean
	   */
	  private boolean getInputData()	{
	    try {
	      TransferData tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);
	      startDate = (String)tTransferData.getValueByName("startDate");
	      endDate = (String)tTransferData.getValueByName("endDate");
	      bankCode = (String)tTransferData.getValueByName("bankCode");
	      //typeFlag = (String)tTransferData.getValueByName("typeFlag");

	      mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0);
	    }
	    catch (Exception e) {
	      // @@错误处理
	      CError.buildErr(this, "接收数据失败");
	      return false;
	    }

	    return true;
	  }

	  /**
	   * 获取交费日期在设置的日期区间内的应付总表记录
	   * @param startDate
	   * @param endDate
	   * @return
	   */
	  private LJAGetSet getLJAGetByPaydate(String startDate, String endDate, String bankCode) {
	    String tSql = "";


	    //不指定开始时间
	    if (startDate.equals("")) {
	      //规则：最早应收日期<=结束时间，最晚交费日期>结束时间
	      //     银行编码匹配，机构编码向下匹配，不在途，有账号
	      //如果要支持更复杂规则，建议扩展成描述算法方式，在CalMode中进行描述
	      //#2191 社保通查勘费报销问题：按原始需求，仅支持现金结算
	      tSql = "select * from LJAGet where "
	                 + "  ShouldDate <= '" + endDate + "'"
	                 + " and BankCode = '" + bankCode + "'"
	                 + " and ManageCom like '" + mGlobalInput.ComCode + "%'"
	                 + " and PayMode='4'"
	                 + " and EnterAccDate is null and ConfDate is null"
	                 + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
	                 + " and (CanSendBank = '0' or CanSendBank is null )  and SumGetMoney<>0 "
	                 + " and (BankAccNo is not null) "
	                 + " and othernotype not in('19','23','SC','GB')";
	    }else
	    {
	    	tSql = "select * from LJAGet where "
	            + " ShouldDate >= '" + startDate + "'"
	            + " and ShouldDate <= '" + endDate + "'"
	            + " and BankCode = '" + bankCode + "'"
	            + " and ManageCom like '" + mGlobalInput.ComCode + "%'"
	            + " and PayMode='4'"
	            + " and EnterAccDate is null and ConfDate is null"
	            + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
	            + " and (CanSendBank is null or  CanSendBank = '0')"
	            + " and (BankAccNo is not null) and sumgetmoney<>0"
                + " and othernotype not in('19','23','SC','GB')";
	    }
	    
	    
	    System.out.println(tSql);

	    LJAGetDB tLJAGetDB = new LJAGetDB();
	    LJAGetSet tLJAGetSet = tLJAGetDB.executeQuery(tSql);

	    return tLJAGetSet;
	  }

	  private LJAGetSet getLJAGet(String startDate, String endDate) {
		  LJAGetSet tLJAGetSet = new LJAGetSet();

	    //获取银行信息，校验是否是银联
	    LDBankDB tLDBankDB = new LDBankDB();
	    tLDBankDB.setBankCode(bankCode);
	    if (!tLDBankDB.getInfo()) {
	      CError.buildErr(this, "获取银行信息（LDBank）失败");
	      return null;
	    }

	   /* //普通银行
	    if (tLDBankDB.getBankUniteFlag().equals("1")) {
	    	
	    	 LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();
	         tLDBankUniteDB.setBankUniteCode(bankCode);
	         LDBankUniteSet tLDBankUniteSet = tLDBankUniteDB.query();

	         if (tLDBankUniteSet.size()==0) {
	           CError.buildErr(this, "获取银联相关银行信息（LDBankUnite）失败");
	           return null;
	         }*/

	         String bankSql = "select distinct bankcode from LJAGet where "
	                 + "  ShouldDate <= '" + endDate + "'"
//	                 + " and BankCode = '" + bankCode + "'"
	                 + " and exists (select 1 from ldbankunite ld where bankunitecode = '"+bankCode+"'  and ld.bankcode=ljaget.bankcode) "
	                 + " and ManageCom like '" + mGlobalInput.ComCode + "%'"
	                 + " and PayMode='4'"
	                 + " and EnterAccDate is null and ConfDate is null"
	                 + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
	                 + " and (CanSendBank = '0' or CanSendBank is null )  and SumGetMoney<>0 "
	                 + " and (BankAccNo is not null) "
	                 ;   
	            System.out.println(bankSql);
	            SSRS tSSRS = new SSRS();
	            ExeSQL tExeSQL = new ExeSQL();
	            tSSRS = tExeSQL.execSQL(bankSql);

	            if(tSSRS.MaxRow == 0){
	            	System.out.println("该渠道下无待付费的数据");
	            	return null;
	            }
	         
	         for (int i = 1; i <= tSSRS.MaxRow; i++) {
	        	 tLJAGetSet.add(getLJAGetByPaydate(startDate, endDate, tSSRS.GetText(i, 1)));
	         }
//	    }

	    return tLJAGetSet;
	  }


	  /**
	   * 生成送银行表数据
	   * @param tLJSPaySet
	   * @return
	   */
	  private LYSendToBankSet getSendToBank(LJAGetSet tLJLJAGetSet) {
	    //总金额
	    double dTotalMoney = 0;
	    //生成批次号，要在循环外生成
	    serialNo = PubFun1.CreateMaxNo("1", 20);
	    LYSendToBankSet tLYSendToBankSet = new LYSendToBankSet();

	    for (int i=0; i<tLJLJAGetSet.size(); i++) {
	      LJAGetSchema tLJAGetSchema = tLJLJAGetSet.get(i + 1);

	      

	      //生成送银行表数据
	      LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
	      //设置统一的批次号
	      tLYSendToBankSchema.setSerialNo(serialNo);
	      //收费标记
	      tLYSendToBankSchema.setDealType("F");
	      tLYSendToBankSchema.setPayCode(tLJAGetSchema.getActuGetNo());
	      tLYSendToBankSchema.setBankCode(tLJAGetSchema.getBankCode());
	      tLYSendToBankSchema.setAccName(tLJAGetSchema.getAccName());
	      tLYSendToBankSchema.setAccNo(tLJAGetSchema.getBankAccNo());

	      tLYSendToBankSchema.setPolNo(tLJAGetSchema.getOtherNo());
	      //tLYSendToBankSchema.setNoType(tLJAGetSchema.getOtherNoType());
	      tLYSendToBankSchema.setComCode(tLJAGetSchema.getManageCom());
	      tLYSendToBankSchema.setAgentCode(tLJAGetSchema.getAgentCode());
	      tLYSendToBankSchema.setPayMoney(tLJAGetSchema.getSumGetMoney());
	      tLYSendToBankSchema.setSendDate(PubFun.getCurrentDate());
	      tLYSendToBankSchema.setDoType("0");
	      //因为没有为发送银行盘表设计操作员字段，所以暂时保存在备注字段中，add by Minim at 2004-2-5
	      tLYSendToBankSchema.setRemark(mGlobalInput.Operator);
	      tLYSendToBankSchema.setModifyDate(PubFun.getCurrentDate());
	      tLYSendToBankSchema.setModifyTime(PubFun.getCurrentTime());
	      if(!StrTool.cTrim(tLJAGetSchema.getDrawerID()).equals("")){
	          tLYSendToBankSchema.setIDType("0");
	          tLYSendToBankSchema.setIDNo(tLJAGetSchema.getDrawerID());
	          }
	          if(!StrTool.cTrim(tLJAGetSchema.getDrawer()).equals("")){
	          tLYSendToBankSchema.setName(tLJAGetSchema.getDrawer());
	          }
	      tLYSendToBankSet.add(tLYSendToBankSchema);

	      //累加总金额和总数量
	      dTotalMoney = dTotalMoney + tLJAGetSchema.getSumGetMoney();
	      //转换精度
	      dTotalMoney = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));
	      sumNum = sumNum + 1;
	    }
	    totalMoney = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));

	    return tLYSendToBankSet;
	  }

	  /**
	   * 修改应收表银行在途标志,记录发送银行次数
	   * @param tLJSPaySet
	   * @return
	   */
	  private LJAGetSet modifyBankFlag(LJAGetSet tLJAGetSet) {
	    for (int i=0; i<tLJAGetSet.size(); i++) {
	      LJAGetSchema tLJAGetSchema = tLJAGetSet.get(i+1);

	      tLJAGetSchema.setBankOnTheWayFlag("1");
	      tLJAGetSchema.setCanSendBank("4");
	      //记录发送银行次数
	      tLJAGetSchema.setSendBankCount(tLJAGetSchema.getSendBankCount() + 1);
	      tLJAGetSet.set(i+1, tLJAGetSchema);
	    }

	    return tLJAGetSet;
	  }

	  /**
	   * 生成银行日志表数据
	   * @return
	   */
	  private LYBankLogSchema getBankLog() {
	    LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();

	    serialNo = PubFun1.CreateMaxNo("1", 20);
	    
	    
	    tLYBankLogSchema.setSerialNo(serialNo);
	    tLYBankLogSchema.setBankCode(bankCode);
	    tLYBankLogSchema.setLogType("F");
	    tLYBankLogSchema.setStartDate(PubFun.getCurrentDate());
	    tLYBankLogSchema.setMakeDate(PubFun.getCurrentDate());
	    tLYBankLogSchema.setTotalMoney(totalMoney);
	    tLYBankLogSchema.setTotalNum(sumNum);
	    tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
	    tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
	    tLYBankLogSchema.setComCode(mGlobalInput.ComCode);

	    return tLYBankLogSchema;
	  }


	  /**
	   * 根据前面的输入数据，进行逻辑处理
	   * @return 如果在处理过程中出错，则返回false,否则返回true
	   */
	  private boolean dealData() {
	    try {
	      //银行代收（应收总表LJSPay）
	      if (mOperate.equals("PAYMONEY")) {

	        //总应收表处理（获取交费日期在设置的日期区间内的记录；获取银行在途标志为N的记录）
	        LJAGetSet tLJAGetSet = getLJAGet(startDate, endDate);
	        if(!CheckLJAGet(tLJAGetSet)){
	            return false;
	        }
	        if (tLJAGetSet == null) throw new NullPointerException("总应付表处理失败！");
	        if (tLJAGetSet.size() == 0) throw new NullPointerException("总应付表无数据！");
	        System.out.println("---End getLJSPayByPaydate---");
	       
	        //生成送银行表数据
//	        LYSendToBankSet tLYSendToBankSet = getSendToBank(tLJAGetSet);
//	        if (tLYSendToBankSet == null) throw new Exception("生成送银行表数据失败！");
//	        System.out.println("---End getSendToBank---");
	       

	        //修改应收表银行在途标志,记录发送银行次数
	        tLJAGetSet = modifyBankFlag(tLJAGetSet);
	        System.out.println("---End modifyBankFlag---");

	        //生成银行日志表数据
	        LYBankLogSchema tLYBankLogSchema = getBankLog();
	        System.out.println("---End getBankLog---");


	        outLJAGetSet.set(tLJAGetSet);
	        outLYBankLogSet.add(tLYBankLogSchema);
	      }
	    }
	    catch(Exception e) {
	      // @@错误处理
	      CError.buildErr(this, "数据处理错误:" + e.getMessage());
	      return false;
	    }

	    return true;
	  }

	  /**
	   * 准备往后层输出所需要的数据
	   * @return 如果准备数据时发生错误则返回false,否则返回true
	   */
	  private boolean prepareOutputData() {
	    try {
	      map.put(outLJAGetSet, "UPDATE");
	      //map.put(outLYSendToBankSet, "INSERT");
	      map.put(outLYBankLogSet, "INSERT");
//	      map.put(outLDBankSchema, "UPDATE");

	      mInputData.clear();
	      mInputData.add(map);
	    }
	    catch(Exception ex) {
	      // @@错误处理
	      CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
	      return false;
	    }

	    return true;
	  }

	  /**
	   * 数据输出方法，供外界获取数据处理结果
	   * @return 包含有数据查询结果字符串的VData对象
	   */
	  public VData getResult() {
	    return mResult;
	  }
	  public boolean CheckLJAGet(LJAGetSet tLJAGetSet){
		  LJAGetSet tempLJAGetSet=new LJAGetSet();
	    
	    for (int i = 1; i <= tLJAGetSet.size(); i++) {
	        if(tLJAGetSet.size()>0){
	            LJAGetSchema tLJAGetSchema=tLJAGetSet.get(i);
	            String Accno=tLJAGetSchema.getBankAccNo();
	            if(!IsNumeric(Accno)){
	            	tempLJAGetSet.add(tLJAGetSchema);
	                
	            	tLJAGetSchema.setCanSendBank("4");
	                this.map.put(tLJAGetSchema, "UPDATE");
	            }
	        
	        }
	    }
	    String failInfo="";
	    for (int i = 1; i <= tempLJAGetSet.size(); i++) {
	        if(!failInfo.equals("")){
	            failInfo+="、"+tempLJAGetSet.get(i).getOtherNo();
	        }else{
	            failInfo=tempLJAGetSet.get(i).getOtherNo();
	        }
	        tLJAGetSet.remove(tempLJAGetSet.get(i));
	    }
	    if(!failInfo.equals("")){
	        failInfo="业务号"+failInfo+"的帐号位数过长，已被锁定";
	    }
	    this.mResult.add(failInfo);
	    return true;
	  }
	  //数字判断2
	  public boolean IsNumeric(String s)
	  {
	      for (int i=0; i<s.length(); i++) {
	          char c = s.charAt(i);
	          if (c!=' ') {
	              if (c<'0' || '9'<c) {
	                  return false ;
	              }
	          }
	  }
	  return true;
	}
	  public static void main(String[] args) {
	   
}
}