package com.sinosoft.lis.yinbaotongbank;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.*;

public class YouChuInterFace {
	
	public LYSendToBankSet getFlag(TransferData transferData)
	{
		//数据类型标识位
		String flag = "";
		//得到参数 《管理机构》
		String ComCode = (String)transferData.getValueByName("ComCode");
		System.out.println(ComCode);
		//得到参数 《银行编码》
		String BankCode = (String)transferData.getValueByName("BankCode");
		System.out.println(BankCode);
		LYSendToBankSet mLYSendToBankSet = new LYSendToBankSet();
	//	mLYSendToBankSet=getLJaPaySet(ComCode,BankCode);
				
		mLYSendToBankSet.add(getLJAGetSet(ComCode,BankCode));
		return mLYSendToBankSet;
	}
	
//    public LYSendToBankSet getLJaPaySet(String tComcode,String tBankCode){
//    	LYSendToBankSet payLYSendToBankSet = new LYSendToBankSet();
    	
//		LJAPaySet mLJaPaySet=new LJAPaySet();
//		LJAPayDB mLJAPayDB=new LJAPayDB();
//		LJAPaySchema tLJAPaySchema=new LJAPaySchema();
//		String mljapaySql="select * from ljapay where "
//			+" managecom like '"+tComcode+"%'"
//			//+" and bankcode in (select bankcode from ldbankunite where bankunitecode= '"+tBankCode+"') " 
//			+" and confdate= current date with ur";	
//		mLJaPaySet=mLJAPayDB.executeQuery(mljapaySql);
//		System.out.println(mljapaySql);
//		for (int i=1;i<mLJaPaySet.size();i++){
//			tLJAPaySchema=mLJaPaySet.get(i);
//			LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
//			if("2".equals(tLJAPaySchema.getIncomeType())&&(getIsYingDai(tLJAPaySchema.getIncomeNo()))){
//				String mpaycount="select paycount from ljapayperson where payno='"+tLJAPaySchema.getPayNo()+"' fetch first 1 rows only ";
//				String mpaytodate="select paytodate from lccont where contno='"+tLJAPaySchema.getIncomeNo()+"' fetch first 1 rows only";
//				String mprem="select prem from lccont where contno='"+tLJAPaySchema.getIncomeNo()+"' fetch first 1 rows only";
//			 	 ExeSQL tExeSQL = new ExeSQL();
//		         String tpaycount = tExeSQL.getOneValue(mpaycount);
//		         System.out.println("续期收费缴费期数:"+tpaycount);
//		         String tpaytodate = tExeSQL.getOneValue(mpaytodate);
//		         System.out.println("续期收费下期应缴费日期:"+tpaytodate);
//		         String tprem = tExeSQL.getOneValue(mprem);
//		         System.out.println("续期收费下期应缴费金额:"+tprem);
//				
//				//数据类型
//				tLYSendToBankSchema.setDoType("2");
//				//保单号
//				tLYSendToBankSchema.setPolNo(tLJAPaySchema.getIncomeNo());
//				//交易日期
//				tLYSendToBankSchema.setModifyDate(tLJAPaySchema.getPayDate());
//				//交易金额
//				tLYSendToBankSchema.setPayMoney(tLJAPaySchema.getSumActuPayMoney());
//				//是否是犹豫期
//				tLYSendToBankSchema.setIDType("0");
//				//缴费期数
//				tLYSendToBankSchema.setNoType(tpaycount);
//				//下期应缴费日期
//				tLYSendToBankSchema.setBankDealDate(tpaytodate);
//				//下期应缴费金额
//				tLYSendToBankSchema.setAccNo(tprem);
//				//本期应缴费日期
//				tLYSendToBankSchema.setSendDate(tLJAPaySchema.getConfDate());
//				
//				tLYSendToBankSchema = setReleInfo(tLYSendToBankSchema, tLJAPaySchema.getPayNo());
//				
//				payLYSendToBankSet.add(tLYSendToBankSchema);
//			}else if("10".equals(tLJAPaySchema.getIncomeType())){
//				//追加保费的数据 这个类型全部是追加保费的数据
//				String tLJSGetEndorse="select contno from ljagetendorse where otherno='"+tLJAPaySchema.getIncomeNo()+"' and feeoperationtype='ZB' ";
//				//LJSGetEndorseSet tLJSGetEndorseSet =getLJSGetEndorseSet(tLJAPaySchema.getIncomeNo());
//				ExeSQL tExeSQL = new ExeSQL();
//		         String tcontno = tExeSQL.getOneValue(tLJSGetEndorse);
//				if(getIsYingDai(tcontno)){
//					
//					//数据类型
//					tLYSendToBankSchema.setDoType("5");
////					保单号
//					tLYSendToBankSchema.setPolNo(tcontno);
//					//交易日期
//					tLYSendToBankSchema.setModifyDate(tLJAPaySchema.getConfDate());
//					//交易金额
//					tLYSendToBankSchema.setPayMoney(tLJAPaySchema.getSumActuPayMoney());
//					//是否是犹豫期
//					tLYSendToBankSchema.setIDType("0");
//					//缴费期数
//					tLYSendToBankSchema.setNoType("0");
//					//下期应缴费日期
//						tLYSendToBankSchema.setBankDealDate(PubFun.getCurrentDate());
//					//下期应缴费金额
//					tLYSendToBankSchema.setAccNo("0");
//					//本期应缴费日期
//					tLYSendToBankSchema.setSendDate(PubFun.getCurrentDate());
//					tLYSendToBankSchema = setReleInfo(tLYSendToBankSchema,tLJAPaySchema.getPayNo());
//					
//					payLYSendToBankSet.add(tLYSendToBankSchema);
//				}else{
//					mLJaPaySet.remove(mLJaPaySet.get(i));
//				}	
//			}
//			
//		}
//		
//		return payLYSendToBankSet;
//    }
//	
   
    
    public LYSendToBankSet getLJAGetSet(String tComcode,String tBankCode){
    	LYSendToBankSet getLYSendToBankSet = new LYSendToBankSet();
    	LJAGetSet mLJAGetSet=new LJAGetSet();
    	LJAGetDB mLJAGetDB=new LJAGetDB();
    	LJAGetSchema mLJAGetSchema=new LJAGetSchema();
		String mLJagetSql = "select * " 
			+" from ljaget where "
			+" managecom like '"+tComcode+"%'"
			+" and makedate between '2010-12-1' and '2011-2-21'  and othernotype='10'";
		mLJAGetSet=mLJAGetDB.executeQuery(mLJagetSql);
		
		for (int i=1;i<=mLJAGetSet.size();i++){
			LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
			mLJAGetSchema=mLJAGetSet.get(i);
		//	if("10".equals(mLJAGetSchema.getOtherNoType())){
//				LJAGetEndorseSet tLJAGetEndorseSet =getLJAGetEndorseSet(mLJAGetSchema.getActuGetNo());				
//					String feeoperationtype=tLJAGetEndorseSet.get(1).getFeeOperationType();
//				String contno=tLJAGetEndorseSet.get(1).getContNo();
				
				LPEdorItemSet tLPEdorItemSet = getLPEdorItem(mLJAGetSchema.getOtherNo());
				String feeoperationtype= tLPEdorItemSet.get(1).getEdorType(); 
				String contno=tLPEdorItemSet.get(1).getContNo();
				if(getIsYingDai(contno)){
					
					if("WT".equals(feeoperationtype)){
						String mWT="select 1 from lbcont where contno='"+contno+"' and current date < (customgetpoldate + 10 day ) fetch first 1 rows only ";
						 ExeSQL tExeSQL = new ExeSQL();
				         String tWT = tExeSQL.getOneValue(mWT);
				         System.out.println("部分领取是否犹豫期:"+tWT);
				         
						//数据类型
						tLYSendToBankSchema.setDoType("0");
						//保单号
						tLYSendToBankSchema.setPolNo(contno);
						//交易日期
						tLYSendToBankSchema.setModifyDate(mLJAGetSchema.getMakeDate());
						//交易金额
						tLYSendToBankSchema.setPayMoney(mLJAGetSchema.getSumGetMoney());
						//是否是犹豫期
						System.out.println("aaaaaaaaaaaaaaaaa"+tWT);
						if(tWT.equals("1")){
							System.out.println("qqqqqqqqqqqqqqqqqqqq"+tWT);
							tLYSendToBankSchema.setIDType(tWT);
					     }else{
					    	 System.out.println("kkkkkkkkkkkkkkkkkkkkkk");
					    	 tLYSendToBankSchema.setIDType("0");
					    }
						//缴费期数
						tLYSendToBankSchema.setNoType("0");
						//下期应缴费日期
						tLYSendToBankSchema.setBankDealDate(PubFun.getCurrentDate());
						//下期应缴费金额
						tLYSendToBankSchema.setAccNo("0");
						//本期应缴费日期
						tLYSendToBankSchema.setSendDate(PubFun.getCurrentDate());
						tLYSendToBankSchema = setReleInfo(tLYSendToBankSchema, mLJAGetSchema.getActuGetNo());
						getLYSendToBankSet.add(tLYSendToBankSchema);
					}else if("CT".equals(feeoperationtype)||"XT".equals(feeoperationtype)){
					 //数据类型
						tLYSendToBankSchema.setDoType("1");
						//保单号
						tLYSendToBankSchema.setPolNo(contno);
						//交易日期
						if(mLJAGetSchema.getMakeDate()==null){
							tLYSendToBankSchema.setModifyDate(PubFun.getCurrentDate());
						}else{
							tLYSendToBankSchema.setModifyDate(mLJAGetSchema.getMakeDate());
						}
						//交易金额
						tLYSendToBankSchema.setPayMoney(mLJAGetSchema.getSumGetMoney());
						//是否是犹豫期
						tLYSendToBankSchema.setIDType("0");
						//缴费期数
						tLYSendToBankSchema.setNoType("0");
						//下期应缴费日期
						tLYSendToBankSchema.setBankDealDate(PubFun.getCurrentDate());
						//下期应缴费金额
						
						tLYSendToBankSchema.setAccNo("0");
						//本期应缴费日期
						tLYSendToBankSchema.setSendDate(PubFun.getCurrentDate());
						tLYSendToBankSchema = setReleInfo(tLYSendToBankSchema, mLJAGetSchema.getActuGetNo());
						getLYSendToBankSet.add(tLYSendToBankSchema);
					
					}else{
				    	mLJAGetSet.remove(mLJAGetSet.get(i));
				    //}
				}
			}
		}
		
				return getLYSendToBankSet;
		}
		    
//					}else if("LQ".equals(feeoperationtype)){
//					 	
//						//数据类型
//						tLYSendToBankSchema.setDoType("4");
//						//保单号
//						tLYSendToBankSchema.setPolNo(contno);
//						//交易日期
//						tLYSendToBankSchema.setModifyDate(mLJAGetSchema.getConfDate());
//						//交易金额
//						tLYSendToBankSchema.setPayMoney(mLJAGetSchema.getSumGetMoney());
//						//是否犹豫期
//						tLYSendToBankSchema.setIDType("0");
//						if(mWT.equals(null)){
//							tLYSendToBankSchema.setIDType("N");
//						}else{
//						tLYSendToBankSchema.setIDType(tWT);
//						}
//						缴费期数
//						tLYSendToBankSchema.setNoType("0");
//						//下期应缴费日期
//						tLYSendToBankSchema.setBankDealDate(PubFun.getCurrentDate());
//						//下期应缴费金额
//						tLYSendToBankSchema.setAccNo("0");
//						//本期应缴费日期
//						tLYSendToBankSchema.setSendDate(PubFun.getCurrentDate());
//						tLYSendToBankSchema = setReleInfo(tLYSendToBankSchema, mLJAGetSchema.getActuGetNo());
//						getLYSendToBankSet.add(tLYSendToBankSchema);
//					}
//				}else{
//					mLJAGetSet.remove(mLJAGetSet.get(i));
//				}
				//满期给付的数据
//			}else if("20".equals(mLJAGetSchema.getOtherNoType())){
//				LJSGetDrawSet tLJSGetDrawSet =getLJSGetDrawSet(mLJAGetSchema.getActuGetNo());
//				
//				String contno=tLJSGetDrawSet.get(1).getContNo();
//				if(getIsYingDai(contno)){
//					数据类型
//					tLYSendToBankSchema.setDoType("3");
//					//保单号
//					tLYSendToBankSchema.setPolNo(contno);
//					//交易日期
//					tLYSendToBankSchema.setModifyDate(mLJAGetSchema.getConfDate());
//					//交易金额
//					tLYSendToBankSchema.setPayMoney(mLJAGetSchema.getSumGetMoney());
////					是否是犹豫期
//					tLYSendToBankSchema.setIDType("0");
//					//缴费期数
//					tLYSendToBankSchema.setNoType("0");
//					//下期应缴费日期
//					tLYSendToBankSchema.setBankDealDate(PubFun.getCurrentDate());
//					//System.out.println("************"+tLYSendToBankSchema.setBankDealDate(" "));
//					//下期应缴费金额
//					tLYSendToBankSchema.setAccNo("0");
//					//本期应缴费日期
//					tLYSendToBankSchema.setSendDate(PubFun.getCurrentDate());
//					tLYSendToBankSchema = setReleInfo(tLYSendToBankSchema, mLJAGetSchema.getActuGetNo());
//					getLYSendToBankSet.add(tLYSendToBankSchema);
//				}else{
//					mLJAGetSet.remove(mLJAGetSet.get(i));
//				}	
				
					
		
    public boolean getIsYingDai(String contno){
    	boolean tisyouchu=false;
    	String mLJSPaySql = "select 1 from lccont where contno='"+contno+"' and substr(bankcode,1,2)='16' and salechnl='04'"
   	+" union all  select 1 from lbcont where contno='"+contno+"'  and substr(bankcode,1,2)='16'  and salechnl='04'  ";
    	 ExeSQL bankExeSQL = new ExeSQL();
         String UniteBank = bankExeSQL.getOneValue(mLJSPaySql);
         System.out.println("是否邮储投保的客户:"+UniteBank);
    	if("1".equals(UniteBank.trim())){
    		tisyouchu=true;
    	}
    	return tisyouchu;
    }
    
    public LJAGetEndorseSet getLJAGetEndorseSet(String tgetnoticeno){
    	String sql = " select * from ljagetendorse where actugetno = '"+tgetnoticeno+"' ";
		LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
		LJAGetEndorseSet tLJAGetEndorseSet = tLJAGetEndorseDB.executeQuery(sql);
		return tLJAGetEndorseSet;
    }
    public LJSGetEndorseSet getLJSGetEndorseSet(String tendorsementno){
    	String sql = " select * from ljsgetendorse where endorsementno = '"+tendorsementno+"' ";
		LJSGetEndorseDB tLJSGetEndorseDB = new LJSGetEndorseDB();
		LJSGetEndorseSet tLJSGetEndorseSet = tLJSGetEndorseDB.executeQuery(sql);
		return tLJSGetEndorseSet;
    }
    public LJSGetDrawSet getLJSGetDrawSet(String tgetnoticeno){
    	String sql = " select * from ljsgetdraw where getnoticeno = '"+tgetnoticeno+"' ";
    	LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
		LJSGetDrawSet tLJSGetDrawSet = tLJSGetDrawDB.executeQuery(sql);
		return tLJSGetDrawSet;
    }
    
    private LPEdorItemSet getLPEdorItem (String tEdoracceptno){
    	String sql = "select * from lpedoritem where edoracceptno = '"+tEdoracceptno+"' " ;
    	LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
    	LPEdorItemSet tLPEdoerItemSet = tLPEdorItemDB.executeQuery(sql);
    	
    	return tLPEdoerItemSet;
    	
    }
    
	 public String getContno(String tOthernotype,String tPolno){
	    	String tContno = "";
	    	if("10".equals(tOthernotype)){
	    	String endorsementnosql="select contno from lpedormain where edorno='"+tPolno+"' fetch first 1 rows only ";
	    	ExeSQL contnoSQL = new ExeSQL();
	    	tContno = contnoSQL.getOneValue(endorsementnosql);
//	    	}else if("5".equals(tOthernotype)||"C".equals(tOthernotype)||"F".equals(tOthernotype)){
//		    	String claimnosql="select contno from ljagetclaim where otherno='"+tPolno+"' fetch first 1 rows only ";
//		    	ExeSQL contnoSQL = new ExeSQL();
//		    	tContno = contnoSQL.getOneValue(claimnosql);
//		    }
	    	}
	    	return tContno;
	    }

	//add by linyun 2010.09.26 对于所有的业务，都增加批次号serialno、应收/实付号paycode字段
	private LYSendToBankSchema setReleInfo(LYSendToBankSchema tLYSendToBankSchema, String tNo){
		tLYSendToBankSchema.setPayCode(tNo);
		tLYSendToBankSchema.setSerialNo(PubFun1.CreateMaxNo("1", 16));
		return tLYSendToBankSchema;
	}
	public static void main(String[] args) {
		YouChuInterFace tYouChuInterFace=new YouChuInterFace();
		TransferData transferData =new TransferData();
		transferData.setNameAndValue("ComCode", "86");
		transferData.setNameAndValue("BankCode", "7703");
		LYSendToBankSet ttLYSendToBankSet=new LYSendToBankSet();
		ttLYSendToBankSet=tYouChuInterFace.getFlag(transferData);
		System.out.println("ttLYSendToBankSet::::::"+ttLYSendToBankSet.size());
		for(int i=1;i<=ttLYSendToBankSet.size();i++){
			System.out.println("ttLYSendToBankSet.get(i).getSerialNo(): "+ttLYSendToBankSet.get(i).getSerialNo());
			System.out.println("ttLYSendToBankSet.get(i).getPayCode(): "+ttLYSendToBankSet.get(i).getPayCode());
			System.out.println("ttLYSendToBankSet.get(i).getDoType(): "+ttLYSendToBankSet.get(i).getDoType());
			System.out.println("ttLYSendToBankSet.get(i).getPolNo(): "+ttLYSendToBankSet.get(i).getPolNo());
			System.out.println("ttLYSendToBankSet.get(i).getModifyDate(): "+ttLYSendToBankSet.get(i).getModifyDate());
			System.out.println("ttLYSendToBankSet.get(i).getPayMoney(): "+ttLYSendToBankSet.get(i).getPayMoney());
			
			System.out.println("ttLYSendToBankSet.get(i).getIDType(): "+ttLYSendToBankSet.get(i).getIDType());
			System.out.println("ttLYSendToBankSet.get(i).getNoType():"+ttLYSendToBankSet.get(i).getNoType());
			
			System.out.println("ttLYSendToBankSet.get(i).getBankDealDate(): "+ttLYSendToBankSet.get(i).getBankDealDate());
			System.out.println("ttLYSendToBankSet.get(i).getAccNo(): "+ttLYSendToBankSet.get(i).getAccNo());
			System.out.println("ttLYSendToBankSet.get(i).getSendDate():"+ttLYSendToBankSet.get(i).getSendDate());
			System.out.println("--------------------------------");
		}
	}
}
