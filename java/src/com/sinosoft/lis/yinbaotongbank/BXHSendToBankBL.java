package com.sinosoft.lis.yinbaotongbank;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

import java.text.DecimalFormat;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 业务数据转换到银行系统，银行代收</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class BXHSendToBankBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 返回数据的容器 */
  private VData mResult = new VData();
  /** 提交数据的容器 */
  private MMap map = new MMap();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public  CErrors mErrors = new CErrors();
  /** 前台传入的公共变量 */
  private GlobalInput mGlobalInput = new GlobalInput();

  //业务数据
  /** 提数开始日期 */
  private String startDate = "";
  /** 提数结束日期 */
  private String endDate = "";
  /** 银行编码 */
  private String bankCode = "";
  /** 提取数据范围类型，如果值是：ALLXQ，则只提取续期数据 */
  private String typeFlag = "";
  /** 总金额 */
  private double totalMoney = 0;
  /** 总笔数 */
  private int sumNum = 0;

  private  String flag="0";
  /** 批次号 */
  private String serialNo = "";

  private LJSPaySet outLJSPaySet = new LJSPaySet();
  private LYSendToBankSet outLYSendToBankSet = new LYSendToBankSet();
  private LYBankLogSet outLYBankLogSet = new LYBankLogSet();

  public BXHSendToBankBL() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) return false;
    System.out.println("---End getInputData---");

    //进行业务处理
    if (!dealData()) return false;
    System.out.println("---End dealData---");

    //银行代收
    if (mOperate.equals("GETMONEY")) {
      //准备往后台的数据
      if (!prepareOutputData()) return false;
      System.out.println("---End prepareOutputData---");

      System.out.println("Start PubSubmit BLS Submit...");
      PubSubmit tPubSubmit = new PubSubmit();
      if (!tPubSubmit.submitData(mInputData, mOperate)) {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
        mResult.clear();
        return false;
      }
      System.out.println("End PubSubmit BLS Submit...");
    }

    return true;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData()	{
    try {
      TransferData tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);
      startDate = (String)tTransferData.getValueByName("startDate");
      endDate = (String)tTransferData.getValueByName("endDate");
      bankCode = (String)tTransferData.getValueByName("bankCode");
      typeFlag = (String)tTransferData.getValueByName("typeFlag");

      mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0);
    }
    catch (Exception e) {
      // @@错误处理
      CError.buildErr(this, "接收数据失败");
      return false;
    }

    return true;
  }

  /**
   * 获取交费日期在设置的日期区间内的应收总表记录
   * @param startDate
   * @param endDate
   * @return
   */
  private LJSPaySet getLJSPayByPaydate(String startDate, String endDate, String bankCode) {
    String tSql = "";
    String tSql2 = "";

    //不指定开始时间
    if (startDate.equals("")) {
      //规则：最早应收日期<=结束时间，最晚交费日期>结束时间
      //     银行编码匹配，机构编码向下匹配，不在途，有账号
      //如果要支持更复杂规则，建议扩展成描述算法方式，在CalMode中进行描述
      String sql = "select * from LJSPay where StartPayDate <= '" + endDate + "'"
                 + " and PayDate >= '" + endDate + "'"
                 + " and BankCode = '" + bankCode + "'"
                 + " and ManageCom like '" + mGlobalInput.ComCode + "%'"
                 + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                 + " and (CanSendBank = '0' or CanSendBank is null )  and sumduepaymoney<>0 "
                 + " and (BankAccNo is not null) ";

      tSql = sql + " and OtherNoType not in('2','20') ";
      //续期划帐,提取所有续期续保数据
          tSql2 = "select * from LJSPay where StartPayDate <= '" + endDate + "'"
                   + " and  PayDate <= '" + endDate + "'"
                   + " and BankCode = '" + bankCode + "'"
                   + " and ManageCom like '" + mGlobalInput.ComCode + "%'"
                   + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                   + " and (CanSendBank = '0' or CanSendBank is null )  and sumduepaymoney<>0 "
                   + " and (BankAccNo is not null) "
                   + " and OtherNoType='2' and OtherNoType <> '20' ";
    }
    //指定开始时间和结束时间
    else {
      //规则：最早应收日期>=开始时间，最早应收日期<=结束时间，最晚交费日期>结束时间
      //     银行编码匹配，机构编码向下匹配，不在途，有账号
      //如果要支持更复杂规则，建议扩展成描述算法方式，在CalMode中进行描述
      String sql = "select * from LJSPay where StartPayDate >= '" + startDate  + "'"
           + " and StartPayDate <= '" + endDate + "'"
           + " and PayDate >= '" + endDate + "'"
           + " and BankCode = '" + bankCode + "'"
           + " and ManageCom like '" + mGlobalInput.ComCode + "%'"
           + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
           + " and (CanSendBank = '0' or CanSendBank is null ) and sumduepaymoney<>0 "
           + " and (BankAccNo is not null) ";

      tSql = sql  + " and OtherNoType not in('2','20')  with ur" ;
      

          tSql2 = "select * from LJSPay where StartPayDate <= '" + endDate + "' and PayDate >='"+startDate+"'"
                   + " and  PayDate >= '" + endDate + "'"
                   + " and BankCode = '" + bankCode + "'"
                   + " and ManageCom like '" + mGlobalInput.ComCode + "%'"
                   + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                   + " and (CanSendBank = '0' or CanSendBank is null )  and sumduepaymoney<>0 "
                   + " and (BankAccNo is not null) and 0=(select COUNT(enteraccdate) from ljtempfee where  tempfeeno=LJSPay.getnoticeno and tempfeetype='2' ) "
                   + " and OtherNoType='2' and OtherNoType <> '20' with ur";

    }
    System.out.println(tSql);
    System.out.println(tSql2);

    LJSPayDB tLJSPayDB = new LJSPayDB();
    LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(tSql);
    tLJSPaySet.add(tLJSPayDB.executeQuery(tSql2));

    return tLJSPaySet;
  }

  private LJSPaySet getLJSPay(String startDate, String endDate) {
    LJSPaySet tLJSPaySet = new LJSPaySet();

    //获取银行信息，校验是否是银联
    LDBankDB tLDBankDB = new LDBankDB();
    tLDBankDB.setBankCode(bankCode);
    if (!tLDBankDB.getInfo()) {
      CError.buildErr(this, "获取银行信息（LDBank）失败");
      return null;
    }

    //普通银行
   /* if (tLDBankDB.getBankUniteFlag().equals("1")) {
    	
    	 LDBankUniteDB tLDBankUniteDB = new LDBankUniteDB();
         tLDBankUniteDB.setBankUniteCode(bankCode);
         LDBankUniteSet tLDBankUniteSet = tLDBankUniteDB.query();

         if (tLDBankUniteSet.size()==0) {
           CError.buildErr(this, "获取银联相关银行信息（LDBankUnite）失败");
           return null;
         }*/

         String bankSql = "select distinct bankcode from LJSPay where StartPayDate <= '" + endDate + "'"
                 + " and PayDate >= '" + endDate + "'"
//                 + " and BankCode = '" + bankCode + "'"
                 + " and exists (select 1 from ldbankunite ld where bankunitecode = '"+bankCode+"'  and ld.bankcode=ljspay.bankcode and unitegroupcode in('1','3')) "
                 + " and ManageCom like '" + mGlobalInput.ComCode + "%'"
                 + " and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null)"
                 + " and (CanSendBank = '0' or CanSendBank is null )  and sumduepaymoney<>0 "
                 + " and (BankAccNo is not null) ";
         System.out.println(bankSql);
         SSRS tSSRS = new SSRS();
         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(bankSql);
         
         if(tSSRS.MaxRow == 0) {
             CError.buildErr(this, "无可提取的缴费数据");
             return null;
           }

         
         for (int i = 1; i <= tSSRS.MaxRow; i++) {
           tLJSPaySet.add(getLJSPayByPaydate(startDate, endDate, tSSRS.GetText(i, 1)));
         }
     
//    }

    return tLJSPaySet;
  }

  /**
   * 校验银行授权
   * @param tLJSPaySet
   */
  private LJSPaySet verifyBankAuth(LJSPaySet tLJSPaySet) {
    int i;
    LJSPaySet bankAuthLJSPaySet = new LJSPaySet();

    for (i=0; i<tLJSPaySet.size(); i++) {
      LJSPaySchema tLJSPaySchema = tLJSPaySet.get(i+1);

      LCBankAuthSchema tLCBankAuthSchema = new LCBankAuthSchema();
      tLCBankAuthSchema.setPolNo(tLJSPaySchema.getOtherNo());  //保单号
      tLCBankAuthSchema.setPayGetFlag("0");                    //收付费标志(0---表示收费)
      tLCBankAuthSchema.setPayValidFlag("1");                  //正常交费/领取代收开通标志(1 -- 开通银行代收)
      tLCBankAuthSchema.setBankCode(bankCode);                 //匹配银行编码

      //找到需要处理的数据
      if (tLCBankAuthSchema.getDB().query().size() > 0) {
        bankAuthLJSPaySet.add(tLJSPaySchema);
      }
    }

    if (bankAuthLJSPaySet.size() > 0) return bankAuthLJSPaySet;
    else return null;
  }

  /**
   * 获取银行账号信息
   * @param tLJSPaySchema
   * @return
   */
  private LCBankAccSchema getBankAcc(LJSPaySchema tLJSPaySchema) {
    try {
      LCBankAccSchema tLCBankAccSchema = new LCBankAccSchema();
      
      tLCBankAccSchema.setBankCode(bankCode);
      tLCBankAccSchema.setBankAccNo(tLJSPaySchema.getBankAccNo());

      tLCBankAccSchema.setSchema(tLCBankAccSchema.getDB().query().get(1));

      return tLCBankAccSchema;
    }
    catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  /**
   * 生成送银行表数据
   * @param tLJSPaySet
   * @return
   */
  private LYSendToBankSet getSendToBank(LJSPaySet tLJSPaySet) {
    //总金额
    double dTotalMoney = 0;
    //生成批次号，要在循环外生成
    serialNo = PubFun1.CreateMaxNo("1", 20);
    LYSendToBankSet tLYSendToBankSet = new LYSendToBankSet();

    for (int i=0; i<tLJSPaySet.size(); i++) {
      LJSPaySchema tLJSPaySchema = tLJSPaySet.get(i + 1);
      
        //校验该笔数据是否已经提盘
        LYSendToBankDB OldLYSendToBankDB = new LYSendToBankDB();
        LYSendToBankSet OldLYSendToBankSet = new LYSendToBankSet();
        OldLYSendToBankDB.setPayCode(tLJSPaySchema.getGetNoticeNo());
        OldLYSendToBankDB.setBankCode(tLJSPaySchema.getBankCode());
        OldLYSendToBankDB.setAccName(tLJSPaySchema.getAccName());
        OldLYSendToBankDB.setAccNo(tLJSPaySchema.getBankAccNo());
        OldLYSendToBankSet=OldLYSendToBankDB.query();
        if(OldLYSendToBankSet.size()>0){
            flag="1";
        }

      //生成送银行表数据
      LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
      //设置统一的批次号
      tLYSendToBankSchema.setSerialNo(serialNo);
      //收费标记
      tLYSendToBankSchema.setDealType("S");
      tLYSendToBankSchema.setPayCode(tLJSPaySchema.getGetNoticeNo());
      tLYSendToBankSchema.setBankCode(tLJSPaySchema.getBankCode());
//      tLYSendToBankSchema.setAccType(tLCBankAccSchema.getAccType());
      tLYSendToBankSchema.setAccName(tLJSPaySchema.getAccName());
      tLYSendToBankSchema.setAccNo(tLJSPaySchema.getBankAccNo());

      //因为改为前台录入财务数据，保单表中不一定有数据，所以不再从中取信息
//      if (tLJSPaySchema.getOtherNoType().equals("9")) {
//        LCPolDB tLCPolDB = new LCPolDB();
//        tLCPolDB.setPrtNo(tLJSPaySchema.getOtherNo());
//        tLCPolDB.setRiskCode(tLJSPaySchema.getRiskCode());
//        LCPolSchema tLCPolSchema = tLCPolDB.query().get(1);
//
//        tLYSendToBankSchema.setPolNo(tLCPolSchema.getPolNo());
//      }
//      else {
//        tLYSendToBankSchema.setPolNo(tLJSPaySchema.getOtherNo());
//      }

      tLYSendToBankSchema.setPolNo(tLJSPaySchema.getOtherNo());
      tLYSendToBankSchema.setNoType(tLJSPaySchema.getOtherNoType());
      tLYSendToBankSchema.setComCode(tLJSPaySchema.getManageCom());
      tLYSendToBankSchema.setAgentCode(tLJSPaySchema.getAgentCode());
      tLYSendToBankSchema.setPayMoney(tLJSPaySchema.getSumDuePayMoney());
      tLYSendToBankSchema.setSendDate(PubFun.getCurrentDate());
      tLYSendToBankSchema.setDoType("1");
      //因为没有为发送银行盘表设计操作员字段，所以暂时保存在备注字段中，add by Minim at 2004-2-5
      tLYSendToBankSchema.setRemark(mGlobalInput.Operator);
      tLYSendToBankSchema.setModifyDate(PubFun.getCurrentDate());
      tLYSendToBankSchema.setModifyTime(PubFun.getCurrentTime());
     // String IDNo=new ExeSQL().getOneValue("select idno from ldperson where customerno='"+tLJSPaySchema.getAppntNo()+"'");
     SSRS tSSRS=new ExeSQL().execSQL("select idno,idtype from ldperson where customerno='"+tLJSPaySchema.getAppntNo()+"'");
     if(tSSRS.getMaxRow()>0){
           tLYSendToBankSchema.setIDNo(tSSRS.GetText(1,1));
           tLYSendToBankSchema.setIDType(tSSRS.GetText(1,2));
     }else{
      tLYSendToBankSchema.setIDNo("");
     }
       tLYSendToBankSchema.setRiskCode(tLJSPaySchema.getRiskCode());
      tLYSendToBankSet.add(tLYSendToBankSchema);

      //累加总金额和总数量
      dTotalMoney = dTotalMoney + tLJSPaySchema.getSumDuePayMoney();
      //转换精度
      dTotalMoney = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));
      sumNum = sumNum + 1;
    }
    totalMoney = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));

    return tLYSendToBankSet;
  }

  /**
   * 修改应收表银行在途标志,记录发送银行次数
   * @param tLJSPaySet
   * @return
   */
  private LJSPaySet modifyBankFlag(LJSPaySet tLJSPaySet) {
    for (int i=0; i<tLJSPaySet.size(); i++) {
      LJSPaySchema tLJSPaySchema = tLJSPaySet.get(i+1);

      tLJSPaySchema.setBankOnTheWayFlag("1");
      
      tLJSPaySchema.setCanSendBank("4");
      //记录发送银行次数
      tLJSPaySchema.setSendBankCount(tLJSPaySchema.getSendBankCount() + 1);
      tLJSPaySet.set(i+1, tLJSPaySchema);
    }

    return tLJSPaySet;
  }

  /**
   * 生成银行日志表数据
   * @return
   */
  private LYBankLogSchema getBankLog() {
    LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();
	
    serialNo = PubFun1.CreateMaxNo("1", 20);
    
    tLYBankLogSchema.setSerialNo(serialNo);
    tLYBankLogSchema.setBankCode(bankCode);
    tLYBankLogSchema.setLogType("S");
    tLYBankLogSchema.setStartDate(PubFun.getCurrentDate());
    tLYBankLogSchema.setMakeDate(PubFun.getCurrentDate());
    tLYBankLogSchema.setTotalMoney(totalMoney);
    tLYBankLogSchema.setTotalNum(sumNum);
    tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
    tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
    tLYBankLogSchema.setComCode(mGlobalInput.ComCode);

    return tLYBankLogSchema;
  }

  /**
   * 更新银行表独立序号
   * @return
   */
  private LDBankSchema getLDBank() {
    LDBankDB tLDBankDB = new LDBankDB();
    tLDBankDB.setBankCode(bankCode);
    if (!tLDBankDB.getInfo()) {
      CError.buildErr(this, "获取银行信息（LDBank）失败");
      return null;
    }

    if (tLDBankDB.getSeqNo()==null || tLDBankDB.getSeqNo().equals("")) {
      tLDBankDB.setSeqNo("0");
    }
    else {
      tLDBankDB.setSeqNo(String.valueOf(Integer.parseInt(tLDBankDB.getSeqNo()) + 1));
    }

    return tLDBankDB.getSchema();
  }


  /**
   * 根据前面的输入数据，进行逻辑处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    try {
        //总应收表处理（获取交费日期在设置的日期区间内的记录；获取银行在途标志为N的记录）   收    S
        LJSPaySet tLJSPaySet = getLJSPay(startDate, endDate);
        if(!CheckLJSpay(tLJSPaySet)){//对accno进行的校验
            return false;
        }
        if (tLJSPaySet == null) throw new NullPointerException("总应收表处理失败！");
        if (tLJSPaySet.size() == 0) throw new NullPointerException("总应收表无数据！");
        System.out.println("---End getLJSPayByPaydate---");

        //修改应收表银行在途标志,记录发送银行次数
        tLJSPaySet = modifyBankFlag(tLJSPaySet);
        System.out.println("---End modifyBankFlag---");

        //生成银行日志表数据
        LYBankLogSchema tLYBankLogSchema = getBankLog();
        System.out.println("---End getBankLog---");


        outLJSPaySet.set(tLJSPaySet);
        outLYBankLogSet.add(tLYBankLogSchema);
      
    }
    catch(Exception e) {
      // @@错误处理
      CError.buildErr(this, "数据处理错误:" + e.getMessage());
      return false;
    }

    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * @return 如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      map.put(outLJSPaySet, "UPDATE");
      map.put(outLYBankLogSet, "INSERT");

      mInputData.clear();
      mInputData.add(map);
    }
    catch(Exception ex) {
      // @@错误处理
      CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
      return false;
    }

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }
  public boolean CheckLJSpay(LJSPaySet tLJSPaySet){
    LJSPaySet tempLJSPaySet=new LJSPaySet();
    
    for (int i = 1; i <= tLJSPaySet.size(); i++) {
        if(tLJSPaySet.size()>0){
            LJSPaySchema tLJSPaySchema=tLJSPaySet.get(i);
            String Accno=tLJSPaySchema.getBankAccNo();
            if(!IsNumeric(Accno)){
                tempLJSPaySet.add(tLJSPaySchema);
                
                tLJSPaySchema.setCanSendBank("4");
                this.map.put(tLJSPaySchema, "UPDATE");
            }
      
        }
    }
    String failInfo="";
    for (int i = 1; i <= tempLJSPaySet.size(); i++) {
        if(!failInfo.equals("")){
            failInfo+="、"+tempLJSPaySet.get(i).getOtherNo();
        }else{
            failInfo=tempLJSPaySet.get(i).getOtherNo();
        }
        tLJSPaySet.remove(tempLJSPaySet.get(i));
    }
    if(!failInfo.equals("")){
        failInfo="业务号"+failInfo+"的帐号位数过长，已被锁定";
    }
    this.mResult.add(failInfo);
    return true;
  }
  //数字判断2
  public boolean IsNumeric(String s)
  {
      for (int i=0; i<s.length(); i++) {
          char c = s.charAt(i);
          if (c!=' ') {
              if (c<'0' || '9'<c) {
                  return false ;
              }
          }
  }
  return true;
}
  public static void main(String[] args) {
    SendToBankBL getSendToBankBL1 = new SendToBankBL();

    double d;
    double f = Double.parseDouble("157574.57");
    d = f;
    System.out.println((new DecimalFormat("0.000000")).format(f));
    System.out.println((new DecimalFormat("0.000000")).format(d));

    LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
    tLYSendToBankDB.setSerialNo("00000000000000000288");
    LYSendToBankSet tLYSendToBankSet = tLYSendToBankDB.query();

    double dTotalMoney = 0.0;
    double totalMoney = 0.0;
    for (int i=0; i<tLYSendToBankSet.size(); i++) {
      LYSendToBankSchema tLYSendToBankSchema = tLYSendToBankSet.get(i + 1);

      //累加总金额和总数量
      dTotalMoney = dTotalMoney + tLYSendToBankSchema.getPayMoney();
      //转换精度
//      dTotalMoney = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));
    }
//    totalMoney = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));
    System.out.println("totalMoney:" + String.valueOf(dTotalMoney));
  }
}
