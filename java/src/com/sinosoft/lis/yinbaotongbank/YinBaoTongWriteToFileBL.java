package com.sinosoft.lis.yinbaotongbank;

import java.text.DecimalFormat;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 银行数据转换到文件模块</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */
public class YinBaoTongWriteToFileBL
{
	 /** 传入数据的容器 */
	  private VData mInputData = new VData();
	  /** 返回数据的容器 */
	  private VData mResult = new VData();
	  /** 提交数据的容器 */
	  private MMap map = new MMap();
	  /** 数据操作字符串 */
	  private String mOperate;
	  /** 错误处理类 */
	  public  CErrors mErrors = new CErrors();
	  /** 前台传入的公共变量 */
	  GlobalInput tG = new GlobalInput();
	  //业务数据
	  /** 提数开始日期 */
	  private String startDate = "";
	  /** 提数结束日期 */
	  private String endDate = "";
	  /** 银行编码 */
	  String bankCode = "";
	  /** 提取数据范围类型，如果值是：ALLXQ，则只提取续期数据 */
	  private String typeFlag = "";
	  /** 总金额 */
	  private double totalMoney = 0;
	  /** 总笔数 */
	  int sumNum = 0;

	  private  String flag="0";
	  /** 批次号 */
	  String serialNo = "";
	  /** 确定标记 */
	  private String tbuttontype = "";
	  
	  private LJSPaySet inLJSPaySet = new LJSPaySet();
	  private LJSPaySet outLJSPaySet = new LJSPaySet();
	  private LYSendToBankSet outLYSendToBankSet = new LYSendToBankSet();
	  private LYBankLogSet outLYBankLogSet = new LYBankLogSet();
//	  private LDBankSchema outLDBankSchema = new LDBankSchema();
	  

	  public YinBaoTongWriteToFileBL() {
	  }

	  /**
	   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	   * @param cInputData 传入的数据,VData对象
	   * @param cOperate 数据操作字符串，主要包括"GETMONEY"和"PAYMONEY"
	   * @return 布尔值（true--提交成功, false--提交失败）
	   */
	  public boolean submitData(VData cInputData, String cOperate) {
	    //将操作数据拷贝到本类中
	    this.mInputData = (VData)cInputData.clone();
	    this.mOperate = cOperate;

	    //得到外部传入的数据,将数据备份到本类中
	    if (!getInputData(cInputData)) return false;
	    System.out.println("---End getInputData---");

	    //进行业务处理
	    if (!dealData()) return false;
	    System.out.println("---End dealData---");

	    //银行代收
	  //  if (mOperate.equals("GETMONEY")) {
	      //准备往后台的数据
	      if (!prepareOutputData()) return false;
	      System.out.println("---End prepareOutputData---");

	      System.out.println("Start PubSubmit BLS Submit...");
	      PubSubmit tPubSubmit = new PubSubmit();
	      if (!tPubSubmit.submitData(mInputData, mOperate)) {
	        // @@错误处理
	        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
	        mResult.clear();
	        return false;
	      }
	      System.out.println("End PubSubmit BLS Submit...");
//	    }

	    return true;
	  }

	  /**
	   * 将外部传入的数据分解到本类的属性中
	   * @param: 无
	   * @return: boolean
	   */
	  private boolean getInputData(VData mInputData)	{
	    try {
	    	
	       inLJSPaySet.set((LJSPaySet)mInputData.getObjectByObjectName("LJSPaySet",0));
	       tG = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0);
	       tbuttontype=(String) mInputData.get(2);
	    }
	    catch (Exception e) {
	      // @@错误处理
	      CError.buildErr(this, "接收数据失败");
	      return false;
	    }

	    return true;
	  }

	 

	  private LJSPaySet getLJSPay(LJSPaySet tinLJSPaySet) {
	    LJSPaySet tLJSPaySet = new LJSPaySet();
        if(tinLJSPaySet.size()>0){
        	for(int i=1;i<=tinLJSPaySet.size();i++){
        		LJSPaySchema tLJSPaySchema=tinLJSPaySet.get(i);	
        		tLJSPaySchema = (tLJSPaySchema.getDB().query()).get(1);
        		tLJSPaySet.add(tLJSPaySchema);
        	}
        }
	    return tLJSPaySet;
	  }

	 

	 

	  /**
	   * 生成送银行表数据
	   * @param tLJSPaySet
	   * @return
	   */
	  public LYSendToBankSet getSendToBank(LJSPaySet tLJSPaySet) {
	    //总金额
	    double dTotalMoney = 0;
	    //生成批次号，要在循环外生成
	    serialNo = PubFun1.CreateMaxNo("1", 20);
	    LYSendToBankSet tLYSendToBankSet = new LYSendToBankSet();
        
	    for (int i=0; i<tLJSPaySet.size(); i++) {
	      LJSPaySchema tLJSPaySchema = tLJSPaySet.get(i + 1);
	     
	        //校验该笔数据是否已经提盘
	        LYSendToBankDB OldLYSendToBankDB = new LYSendToBankDB();
	        LYSendToBankSet OldLYSendToBankSet = new LYSendToBankSet();
	        OldLYSendToBankDB.setPayCode(tLJSPaySchema.getGetNoticeNo());
	        OldLYSendToBankDB.setBankCode(tLJSPaySchema.getBankCode());
	        OldLYSendToBankDB.setAccName(tLJSPaySchema.getAccName());
	        OldLYSendToBankDB.setAccNo(tLJSPaySchema.getBankAccNo());
	        OldLYSendToBankSet=OldLYSendToBankDB.query();
            System.out.println("OldLYSendToBankSet     @@@@@@@@    "+OldLYSendToBankSet.size());
	      //生成送银行表数据
	      LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
	      //设置统一的批次号
	      tLYSendToBankSchema.setSerialNo(serialNo);
	      //收费标记
	      tLYSendToBankSchema.setDealType("S");
	      tLYSendToBankSchema.setPayCode(tLJSPaySchema.getGetNoticeNo());
	      tLYSendToBankSchema.setBankCode(tLJSPaySchema.getBankCode());
	      tLYSendToBankSchema.setAccName(tLJSPaySchema.getAccName());
	      tLYSendToBankSchema.setAccNo(tLJSPaySchema.getBankAccNo());

	      tLYSendToBankSchema.setPolNo(tLJSPaySchema.getOtherNo());
	      tLYSendToBankSchema.setNoType(tLJSPaySchema.getOtherNoType());
	      tLYSendToBankSchema.setComCode(tLJSPaySchema.getManageCom());
	      tLYSendToBankSchema.setAgentCode(tLJSPaySchema.getAgentCode());
	      tLYSendToBankSchema.setPayMoney(tLJSPaySchema.getSumDuePayMoney());
	      tLYSendToBankSchema.setSendDate(PubFun.getCurrentDate());
	      tLYSendToBankSchema.setDoType("1");
	      //因为没有为发送银行盘表设计操作员字段，所以暂时保存在备注字段中，add by Minim at 2004-2-5
	      tLYSendToBankSchema.setRemark(tG.Operator);
	      tLYSendToBankSchema.setModifyDate(PubFun.getCurrentDate());
	      tLYSendToBankSchema.setModifyTime(PubFun.getCurrentTime());
	     System.out.println("tLJSPaySchema:::::"+tLJSPaySchema.getAppntNo());
	      SSRS tSSRS=new ExeSQL().execSQL("select idno,idtype from ldperson where customerno='"+tLJSPaySchema.getAppntNo()+"'");
	     if(tSSRS.getMaxRow()>0){
	           tLYSendToBankSchema.setIDNo(tSSRS.GetText(1,1));
	           tLYSendToBankSchema.setIDType(tSSRS.GetText(1,2));
	     }else{
	      tLYSendToBankSchema.setIDNo("");
	     }
	       tLYSendToBankSchema.setRiskCode(tLJSPaySchema.getRiskCode());
	      tLYSendToBankSet.add(tLYSendToBankSchema);

	      //累加总金额和总数量
	      dTotalMoney = dTotalMoney + tLJSPaySchema.getSumDuePayMoney();
	      //转换精度
	      dTotalMoney = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));
	      sumNum = sumNum + 1;
	    }
	    totalMoney = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));
       
	    return tLYSendToBankSet;
	  }

	  /**
	   * 修改应收表银行在途标志,记录发送银行次数
	   * @param tLJSPaySet
	   * @return
	   */
	  private LJSPaySet modifyBankFlag(LJSPaySet tLJSPaySet) {
	    for (int i=0; i<tLJSPaySet.size(); i++) {
	    	System.out.println("modifyBankFlag:");
	      LJSPaySchema tLJSPaySchema = tLJSPaySet.get(i+1);

	      if(tbuttontype.equals("Yes") )
	      {
	    	  tLJSPaySchema.setBankOnTheWayFlag("1");
		      tLJSPaySchema.setCanSendBank("7");
	      }else
	      {
	    	  tLJSPaySchema.setBankOnTheWayFlag("0");
		      tLJSPaySchema.setCanSendBank("0");
	      }
          //记录修改时间
	      tLJSPaySchema.setModifyDate(PubFun.getCurrentDate());
	      tLJSPaySchema.setModifyTime(PubFun.getCurrentTime());      
	     
	      //记录发送银行次数
	      tLJSPaySchema.setSendBankCount(tLJSPaySchema.getSendBankCount() + 1);
	      tLJSPaySet.set(i+1, tLJSPaySchema);
	    }

	    return tLJSPaySet;
	  }

	  /**
	   * 生成银行日志表数据
	   * @return
	   */
	  public LYBankLogSchema getBankLog() {
	    LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();

	    //tG = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0);
	    //tLYBankLogSchema.setSerialNo(serialNo);
	    tLYBankLogSchema.setBankCode(bankCode);
	    tLYBankLogSchema.setLogType("S");
	    tLYBankLogSchema.setStartDate(PubFun.getCurrentDate());
	    tLYBankLogSchema.setMakeDate(PubFun.getCurrentDate());
	    tLYBankLogSchema.setTotalMoney(totalMoney);
	    tLYBankLogSchema.setTotalNum(sumNum);
	    tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
	    tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
	    tLYBankLogSchema.setComCode(tG.ComCode);

	    return tLYBankLogSchema;
	  }

	


	  /**
	   * 根据前面的输入数据，进行逻辑处理
	   * @return 如果在处理过程中出错，则返回false,否则返回true
	   */
	  private boolean dealData() {
	    try {

	    	LJSPaySet tLJSPaySet = getLJSPay(inLJSPaySet);
	    	
	    	System.out.println("tLJSPaySet.size::"+tLJSPaySet.size());
	        if(!CheckLJSpay(tLJSPaySet)){
	            return false;
	        }
	        
	        if (tLJSPaySet == null) throw new NullPointerException("总应收表处理失败！");
	        if (tLJSPaySet.size() == 0) throw new NullPointerException("总应收表无数据！");
	        System.out.println("---End getLJSPayByPaydate---");
	        
	        
	        //<***********************************************************************>
	        
	        
	        //生成送银行表数据
//	        if(tbuttontype.equals("Yes") )
//	        {
//	        	LYSendToBankSet tLYSendToBankSet = getSendToBank(tLJSPaySet);
//	        	 
//	 	        if(flag.equals("1")){
//	 	           CError.buildErr(this, "接收数据失败");
//	 	           return false;
//	 	        }
//	 	        if (tLYSendToBankSet == null) throw new Exception("生成送银行表数据失败！");
//	 	        //outLYSendToBankSet.set(tLYSendToBankSet);
//	 		    //map.put(outLYSendToBankSet, "INSERT");
//	 		    
//	 	        System.out.println("---End getSendToBank---");
//	        }
	        
	        
	        //<***********************************************************************>
	        
	        
	        
	        //修改应收表银行在途标志,记录发送银行次数
	        modifyBankFlag(tLJSPaySet);
	        System.out.println("---End modifyBankFlag---");

	        
	        
	        
	        //<$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$>
	        
	        //生成银行日志表数据
	        //LYBankLogSchema tLYBankLogSchema = getBankLog();
	        //System.out.println("---End getBankLog---");
	        
	        
	        //<$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$>    
	        

	        outLJSPaySet.set(tLJSPaySet);
	        //outLYBankLogSet.add(tLYBankLogSchema);

	    }
	    catch(Exception e) {
	      // @@错误处理
	      CError.buildErr(this, "数据处理错误:" + e.getMessage());
	      return false;
	    }

	    return true;
	  }

	  /**
	   * 准备往后层输出所需要的数据
	   * @return 如果准备数据时发生错误则返回false,否则返回true
	   */
	  private boolean prepareOutputData() {
	    try {
	      map.put(outLJSPaySet, "UPDATE");

	      //map.put(outLYBankLogSet, "INSERT");

	      mInputData.clear();
	      mInputData.add(map);
	    }
	    catch(Exception ex) {
	      // @@错误处理
	      CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
	      return false;
	    }

	    return true;
	  }

	  /**
	   * 数据输出方法，供外界获取数据处理结果
	   * @return 包含有数据查询结果字符串的VData对象
	   */
	  public VData getResult() {
	    return mResult;
	  }
	  public boolean CheckLJSpay(LJSPaySet tLJSPaySet){
	    LJSPaySet tempLJSPaySet=new LJSPaySet();
	    
	    for (int i = 1; i <= tLJSPaySet.size(); i++) {
	        if(tLJSPaySet.size()>0){
	            LJSPaySchema tLJSPaySchema=tLJSPaySet.get(i);
	            String Accno=tLJSPaySchema.getBankAccNo();
	            System.out.println(tLJSPaySchema.getOtherNo());
	            tLJSPaySchema.setCanSendBank("7");
                this.map.put(tLJSPaySchema, "UPDATE");
	            tempLJSPaySet.add(tLJSPaySchema);
	           
	        }
	    }

	    return true;
	  }
	  //数字判断2
	  public boolean IsNumeric(String s)
	  {
	      for (int i=0; i<s.length(); i++) {
	          char c = s.charAt(i);
	          if (c!=' ') {
	              if (c<'0' || '9'<c) {
	                  return false ;
	              }
	          }
	  }
	  return true;
	}
	  public static void main(String[] args) {
	    SendToBankBL getSendToBankBL1 = new SendToBankBL();

	    double d;
	    double f = Double.parseDouble("157574.57");
	    d = f;
	    System.out.println((new DecimalFormat("0.000000")).format(f));
	    System.out.println((new DecimalFormat("0.000000")).format(d));

	    LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
	    tLYSendToBankDB.setSerialNo("00000000000000000288");
	    LYSendToBankSet tLYSendToBankSet = tLYSendToBankDB.query();

	    double dTotalMoney = 0.0;
	    double totalMoney = 0.0;
	    for (int i=0; i<tLYSendToBankSet.size(); i++) {
	      LYSendToBankSchema tLYSendToBankSchema = tLYSendToBankSet.get(i + 1);

	      //累加总金额和总数量
	      dTotalMoney = dTotalMoney + tLYSendToBankSchema.getPayMoney();
	      //转换精度
//	      dTotalMoney = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));
	    }
//	    totalMoney = Double.parseDouble((new DecimalFormat("0.00")).format(dTotalMoney));
	    System.out.println("totalMoney:" + String.valueOf(dTotalMoney));
	  }

	public LJSPaySet getInLJSPaySet() {
		return inLJSPaySet;
	}

	public void setInLJSPaySet(LJSPaySet inLJSPaySet) {
		this.inLJSPaySet = inLJSPaySet;
	}
	}
