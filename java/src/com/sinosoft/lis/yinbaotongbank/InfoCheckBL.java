package com.sinosoft.lis.yinbaotongbank;


import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class InfoCheckBL {

	  public CErrors mErrors = new CErrors();
	    private VData mResult = new VData();
	    private String mDay[] = null; //取得的时间
     //接收收费的类型：暂收OR预收
	    private String mChargeType = ""; //将收费类型转换成汉字的显示
	    private GlobalInput mGlobalInput = new GlobalInput(); // 全局数据
	    public InfoCheckBL()
	    {
	    }

	    /**
	     传输数据的公共方法
	     */
	    public boolean submitData(VData cInputData)
	    {

	        if (!getInputData(cInputData))
	        {
	            return false;
	        }
	        mResult.clear();
	        /**************************************************************************
	         * date:2003-05-28
	         * author:lys
	         * function:判断收费的标志，若是Pay则是“暂收”、若是YSPay则是“预收”
	         */
	            if (!getPrintDataPay())
	            {
	                return false;
	            }

	        return true;
	    }
	    private boolean getInputData(VData cInputData) //打印付费
	    {
	        mDay = (String[]) cInputData.get(0);
	        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
	                "GlobalInput", 0));
	        System.out.println("2003-05-28  BL ");
	        if (mGlobalInput == null)
	        {
	            buildError("getInputData", "没有得到足够的信息！");
	            return false;
	        }
	        return true;
	    }

	    public VData getResult()
	    {
	        return this.mResult;
	    }

	    private void buildError(String szFunc, String szErrMsg)
	    {
	        CError cError = new CError();
	        cError.moduleName = "InfoCheckBL";
	        cError.functionName = szFunc;
	        cError.errorMessage = szErrMsg;
	        this.mErrors.addOneError(cError);
	    }
	    private boolean getPrintDataPay()
	    {
	        SSRS tSSRS = new SSRS();
	        String strArr[] = null;
	        XmlExport xmlexport = new XmlExport();

	            xmlexport.createDocument("InfoCheck.vts", "printer");


	            ListTable tlistTable = new ListTable();
	            tlistTable.setName("BankInfo");
	            ExeSQL tExeSQL = new ExeSQL();

	            String msql="select polno,paycode,transno,(case opertype when '0' then '犹豫期退保' when '1' then '退保' else '' end), "+
	            " trandate,paymoney from LKDailyDetail where trandate between '"+mDay[0]+"' and '"+mDay[1]+"' ";

	            //mDay[1] mDay[0]

	            tSSRS = tExeSQL.execSQL(msql);
	            System.out.println("orrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrz:" + tSSRS.MaxRow);
	            double sumMoney = 0;
	            strArr = new String[6];
	            for (int i = 1; i <= tSSRS.MaxRow; i++)
	            {
	                for (int j = 1; j <= tSSRS.MaxCol; j++)
	                {
	                    strArr[j-1] = tSSRS.GetText(i, j);
	                    System.out.println("===> " + i + ":" + strArr[j-1]);
	                }
	                tlistTable.add(strArr);

	                //计算总额
	                sumMoney += Double.parseDouble(tSSRS.GetText(i, 6));
	            }
//	            strArr = new String[4];
//	            strArr[0] = "incomeno";
//	            strArr[1] = "sumactupaymoney";
//	            strArr[2] = "makedate";
//	            strArr[3] = "managecom";
	            xmlexport.addListTable(tlistTable, strArr);


//	        String nsql = "select Name from LDCom where ComCode='" +
//	                      mGlobalInput.ManageCom + "'";
//	        ExeSQL nExeSQL = new ExeSQL();
//	        nSSRS = nExeSQL.execSQL(nsql);
//	        String manageCom = nSSRS.GetText(1, 1);
	        TextTag texttag = new TextTag(); //新建一个TextTag的实例
	        texttag.add("StartDate", mDay[0]);
	        texttag.add("EndDate", mDay[1]);
//	        texttag.add("ManageCom", manageCom);
	        texttag.add("SumMoney", sumMoney);

	        if (texttag.size() > 0)
	        {
	            xmlexport.addTextTag(texttag);
	        }

	        mResult.clear();
	        mResult.addElement(xmlexport);


	        return true;
	    }

}
