package com.sinosoft.lis.yinbaotongbank;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class CCBReturnFileSave{
//	提交数据的容器 
	private MMap map = new MMap();
	
	private	MMap mmap = new MMap();
	//得到参数 《交易类型》
	private String DealType = null;
	//得到参数 《管理机构》
	private String ComCode =  null;
	//得到参数 《银行编码》
	private String BankCode =  null;
	//得到参数 《发盘日期》
	private String SendDate =  null;
	
	private PubSubmit tPubSubmit = new PubSubmit();	
	
	private GlobalInput tG = new GlobalInput();
	
	//传入数据的容器 
	private VData mInputData = new VData();
	
	private boolean ReturnFileSave(TransferData transferData){
		
		  try {
			  DealType = (String)transferData.getValueByName("DealType");
				//得到参数 《管理机构》
			    ComCode = (String)transferData.getValueByName("ComCode");
				//得到参数 《银行编码》
				BankCode = (String)transferData.getValueByName("BankCode");
				//得到参数 《发盘日期》	
				SendDate = (String)transferData.getValueByName("SendDate");
		    }
		    catch (Exception e) {
		      // @@错误处理
		      CError.buildErr(this, "接收数据失败");
		      return false;
		    }

		    return true;
		
		
	}
	
	public Object getData(TransferData transferData)
	{
		
		ReturnFileSave(transferData);
		
		LYSendToBankSet outLYSendToBankSet = new LYSendToBankSet();
		LYSendToBankSet mLYSendToBankSet = new LYSendToBankSet();
		LYBankLogSet  outLYBankLogSet = new LYBankLogSet();

		
		
		//生成LYSendToBank
		if( "S".equals(DealType))
		{
			CCBWriteToFileBL tCCBWriteToFileBL = new CCBWriteToFileBL();
			//查询出cansendbank状态为5的数据
			LJSPayDB tLJSPayDB = new LJSPayDB();
			String sql1 = "select * from ljspay a where cansendbank = '5' and BankOnTheWayFlag = '1' and  bankcode in "+
            "(select bankcode from ldbankunite where bankunitecode='"+BankCode+"' ) and "+ 
            "not exists(select 1 from lysendtobank where a.getnoticeno = paycode ) and managecom like  '"+ComCode+"%'";
			LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(sql1);
			System.out.println("!!!!!!!!!"+tLJSPaySet.size());
			LYSendToBankSet tLYSendToBankSet = tCCBWriteToFileBL.getSendToBank(tLJSPaySet);
			if(tLYSendToBankSet.size()!=0){
			String serialno = tLYSendToBankSet.get(1).getSerialNo();
			
			LYBankLogSchema tLYBankLogSchema = tCCBWriteToFileBL.getBankLog();
			tLYBankLogSchema.setSerialNo(serialno);
			tLYBankLogSchema.setBankCode(BankCode);
			tLYBankLogSchema.setComCode(ComCode);
			
			//提交数据
			outLYSendToBankSet.set(tLYSendToBankSet);
			outLYBankLogSet.add(tLYBankLogSchema);
 		    map.put(outLYSendToBankSet, "INSERT");
 		    map.put(outLYBankLogSet, "INSERT");
 		    
 		    mInputData.clear();
 	        mInputData.add(map);
 	        
 	        //邮储的发盘和回盘类
 	        tPubSubmit.submitData(mInputData, "");
		    }else{
			System.out.println("没有银保通模式收费数据！！！");
			return null;
		    }
		}else
		{
			CCBWriteToFileABL tCCBWriteToFileABL = new CCBWriteToFileABL();
			//查询出cansendbank状态为5的数据
			LJAGetDB tLJAGetDB = new LJAGetDB();
			String sql2 = "select * from ljaget a where cansendbank = '5' and BankOnTheWayFlag = '1' " +
			            "and bankcode in ( select bankcode from ldbankunite where bankunitecode='"+BankCode+"')"+
					    "and not exists(select 1 from lysendtobank where a.actugetno = paycode ) and managecom like '"+ComCode+"%' "
					    +" and not exists (select 1 from ljfiget where actugetno=a.actugetno) ";
			LJAGetSet tLJAGetSet = tLJAGetDB.executeQuery(sql2);
			System.out.println("通联代付数据量----------"+tLJAGetSet.size());
			String sql3 = "select * from ljaget a where cansendbank = '5' and BankOnTheWayFlag = '1' " +
            "and bankcode in ( select bankcode from ldbankunite where bankunitecode='"+BankCode+"')"+
		    "and not exists(select 1 from lysendtobank where a.actugetno = paycode ) and managecom like '"+ComCode+"%' "
		    +" and exists (select 1 from ljfiget where actugetno=a.actugetno) ";
            LJAGetSet eLJAGetSet = tLJAGetDB.executeQuery(sql3);
            System.out.println("通联代付错误数据量----------"+eLJAGetSet.size());
            if(eLJAGetSet.size()>0){
            	sendMail(eLJAGetSet);
            }
			LYSendToBankSet tLYSendToBankSet = tCCBWriteToFileABL.getSendToBank(tLJAGetSet);
			if(tLYSendToBankSet.size()!=0){
			String serialno = tLYSendToBankSet.get(1).getSerialNo();
			
			LYBankLogSchema tLYBankLogSchema = tCCBWriteToFileABL.getBankLog();
			tLYBankLogSchema.setSerialNo(serialno);
			tLYBankLogSchema.setBankCode(BankCode);
			tLYBankLogSchema.setComCode(ComCode);
			
			outLYSendToBankSet.set(tLYSendToBankSet);
			outLYBankLogSet.add(tLYBankLogSchema);
			
			map.put(outLYSendToBankSet, "INSERT");
			map.put(outLYBankLogSet, "INSERT");
			
			mInputData.clear();
 	        mInputData.add(map);
// 	     tPubSubmit.submitData(mInputData, "");
 	       PubSubmit ttPubSubmit = new PubSubmit();
 	      if(! ttPubSubmit.submitData(mInputData, ""))
 	      {
 	    	  System.out.println("数据提交失败！");
 	    	 return null;
 	      }
			 }else{
					System.out.println("没有银保通模式付费数据！！！");
					return null;
		     }
			
		}
		
		mLYSendToBankSet=setYingDaiRemark(outLYSendToBankSet);
		
		
		return mLYSendToBankSet;
	}
	private void sendMail(LJAGetSet ttLJAGetSet) {
		String tSQL = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		String mURL = new ExeSQL().getOneValue(tSQL);
		//mURL = "F:/";
		mURL+="vtsfile/";
		File mFileDir = new File(mURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录[" + mURL.toString() + "]失败！"
						+ mFileDir.getPath());
			}
		}
		String filename = mURL + "YCErrorLJAGET.txt";
		try {
			FileOutputStream tFileOutputStream = new FileOutputStream(filename, true);
			OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(tFileOutputStream, "GBK");
	    	BufferedWriter mBufferedWriter = new BufferedWriter(tOutputStreamWriter);  
			for(int i=1;i<=ttLJAGetSet.size();i++){
				String write="";
				write = ttLJAGetSet.get(i).getActuGetNo() ;
                mBufferedWriter.write(write);
	    		mBufferedWriter.newLine();
	    		mBufferedWriter.flush();
			}
			mBufferedWriter.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
		
		String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
		SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSql);
		MailSender tMailSender = new MailSender(tSSRS.GetText(1, 1), tSSRS
				.GetText(1, 2), "picchealth");
		tMailSender.setSendInf("邮储代付发盘错误","您好：错误数据见附件！", filename);
		String tSQLRMailSql = "select codename,codealias from ldcode where codetype = 'YCErrorLJ' ";
		SSRS tSSRSR = new ExeSQL().execSQL(tSQLRMailSql);
		tMailSender.setToAddress(tSSRSR.GetText(1, 1),
				tSSRSR.GetText(1, 2), null);
		// 发送邮件
		if (!tMailSender.sendMail()) {
			deleteFile(filename);
			System.out.println(tMailSender.getErrorMessage());
		}
		deleteFile(filename);
		
	}

	//为函数增加一个参数 2010-11-18
	 public LYSendToBankSet setYingDaiRemark(LYSendToBankSet tLYSendToBankSet){
		    LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
			LYSendToBankSet mLYSendToBankSet = new LYSendToBankSet();
			LYSendToBankSet oLYSendToBankSet = new LYSendToBankSet();
			
			//增加银代标识
//			String sql = "select * from lysendtobank where dealType ='"+DealType+"' " +
//					     " and comCode like '"+ComCode+"%' " +
//					     " and senddate = '"+SendDate+"'" +
//					     " and bankcode in (select bankcode from ldbankunite where '"+BankCode+"' = bankunitecode )";
//			mLYSendToBankSet = tLYSendToBankDB.executeQuery(sql);
		
//			for(int i = 1;i<=mLYSendToBankSet.size();i++)
//			{   LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
//				LYSendToBankSchema oLYSendToBankSchema = mLYSendToBankSet.get(i);
//				tLYSendToBankSchema=getNoType(oLYSendToBankSchema);
//
//				oLYSendToBankSet.add(tLYSendToBankSchema);
//				
//			}
			for(int i = 1;i<=tLYSendToBankSet.size();i++)
			{   LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
				LYSendToBankSchema oLYSendToBankSchema = tLYSendToBankSet.get(i);
				tLYSendToBankSchema=getNoType(oLYSendToBankSchema);

				oLYSendToBankSet.add(tLYSendToBankSchema);
				
			}
			
			mmap.put(oLYSendToBankSet, "UPDATE");
			
			mInputData.clear();
		    mInputData.add(mmap);
		    
		    PubSubmit ttPubSubmit = new PubSubmit();
	 	      if(! ttPubSubmit.submitData(mInputData, ""))
	 	      {
	 	    	 System.out.println("数据提交失败！");
	 	    	 return null;
	 	      }
		   // tPubSubmit.submitData(mInputData, "");
			
			return oLYSendToBankSet;
	    }
	 public LYSendToBankSchema getNoType(LYSendToBankSchema tLYSendToBankSchema){
	    	String tNoType = "";
	    	String flag = "";
	    	String tContno="";
	    	String tDealType=tLYSendToBankSchema.getDealType();
	    	String tOthernotype=tLYSendToBankSchema.getNoType();
	    	String tPolno=tLYSendToBankSchema.getPolNo();

	    	if("2".equals(tOthernotype)){
	    		tNoType="01";
	    		flag = getIsYingDai(tPolno);
	    		tContno=tPolno;
	    	}else if("9".equals(tOthernotype)||"16".equals(tOthernotype)){
	    		tNoType="02";
	    		tContno=tPolno;
	    	}else if("10".equals(tOthernotype)){
	    		tContno=getContno(tOthernotype,tPolno);
	    		flag=getIsYingDai(tContno);
	    		if("S".equals(tDealType)){
	    			tNoType="08";			
	    		}else{
	    			tNoType=getLJAGetEndorseType(tPolno);
	    		}
	    	}
	    	else if("5".equals(tOthernotype)||"C".equals(tOthernotype)||"F".equals(tOthernotype)){
	    		tContno=getContno(tOthernotype,tPolno);
	    		flag=getIsYingDai(tContno);	
	    	    tNoType="07";
	    	}
	    	if(tNoType!=""){
	    	tLYSendToBankSchema.setNoType(tNoType);
	    	}
	    	if(tContno!=""){
		    	tLYSendToBankSchema.setName(tContno);
		    	}
	    	tLYSendToBankSchema.setRemark(flag);
		    	
	    	return tLYSendToBankSchema;
	    }
	 
	 public String getContno(String tOthernotype,String tPolno){
	    	String tContno = "";
	    	if("10".equals(tOthernotype)){
	    	String endorsementnosql="select contno from lpedormain where edorno='"+tPolno+"' fetch first 1 rows only ";
	    	ExeSQL contnoSQL = new ExeSQL();
	    	tContno = contnoSQL.getOneValue(endorsementnosql);
	    	}else if("5".equals(tOthernotype)||"C".equals(tOthernotype)||"F".equals(tOthernotype)){
		    	String claimnosql="select contno from ljagetclaim where otherno='"+tPolno+"' fetch first 1 rows only ";
		    	ExeSQL contnoSQL = new ExeSQL();
		    	tContno = contnoSQL.getOneValue(claimnosql);
		    }
	    	return tContno;
	    }
	
	 public String getIsYingDai(String contno){
	    	String tisyouchu = "";
	    	String mLJSPaySql = "select 1 from lccont where contno='"+contno+"' and substr(bankcode,1,2)='16' and salechnl='04'"
	   	+" union all  select 1 from lbcont where contno='"+contno+"'  and substr(bankcode,1,2)='16'  and salechnl='04'  ";
	    	ExeSQL bankExeSQL = new ExeSQL();
	         String UniteBank = bankExeSQL.getOneValue(mLJSPaySql);
	         System.out.println("是否邮储投保的客户:"+UniteBank);
	    	if("1".equals(UniteBank.trim())){
	    		tisyouchu = "1";
	    	}
	    	return tisyouchu;
	    }

	  public String getLJAGetEndorseType(String tEndorsementno){
		  String tType="";
	    	String sql = " select FeeOperationType from ljagetendorse where endorsementno = '"+tEndorsementno+"' fetch first 1 rows only";
	    	ExeSQL tTypeSQL = new ExeSQL();
	    	tType = tTypeSQL.getOneValue(sql);
	    	if("WT".equals(tType)){
	    		tType="04";
	    	}else if("CT".equals(tType)){
	    		tType="05";
	    	}else if("MJ".equals(tType)){
	    		tType="06";
	    	}else{
	    		tType="03";
	    	}
			return tType;
	    }
	 
	public static void main(String[] args) {
		
		CCBReturnFileSave save = new CCBReturnFileSave();
		TransferData transferData = new TransferData();
		transferData.setNameAndValue("DealType","F");
		transferData.setNameAndValue("ComCode","86");
		transferData.setNameAndValue("BankCode","7703");
		
		transferData.setNameAndValue("SendDate","2010-11-06");
		
		save.getData(transferData);
		
	}
	
	private boolean deleteFile(String cFilePath) {
		File f = new File(cFilePath);
		if (!f.exists()) {
			System.out.println("没有找到需要删除的文件");
			return false;
		}
		f.delete();
		return true;
	}

}

