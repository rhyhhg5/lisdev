package com.sinosoft.lis.yinbaotongbank;

import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class QueryLYSendToBank {
	
	public Object queryData(TransferData transferData)
	{
		
		//得到参数 《管理机构》
		String ComCode = (String)transferData.getValueByName("ComCode");
		//得到参数 《银行编码》
		String BankCode = (String)transferData.getValueByName("BankCode");
		//得到参数 《发盘日期》
		String SendDate = (String)transferData.getValueByName("SendDate");
		
		
		//VData data = new VData();
		ExeSQL tExeSQL = new ExeSQL();
		//查询校验
		TransferData transferData1 = new TransferData();
		
		//查询交易类型为 F
		String fSQL = "select 1 from ljaget where "
	         		 +" bankcode in (select bankcode from ldbankunite where '"+BankCode+"' = bankunitecode )"
	         		 +" and managecom like '"+ComCode+"%'"
	         		 //+" and senddate = '"+SendDate+"'"
	         		 //+" and dealtype = 'F'"
	         		 +" and bankonthewayflag = '1'"
	         		 +" and cansendbank = '7'"
	         		 +" and not exists (select 1 from lysendtobank a where a.paycode = actugetno and dealtype = 'F')"
					 +" fetch first 1 rows only ";
		String tSSRS = tExeSQL.getOneValue(fSQL);
		//查询结果set到TransferData中
		transferData1.setNameAndValue("queryDealForF", tSSRS);
		
		//查询交易类型为 S 
		String sSQL = "select 1 from ljspay where "
			         +" bankcode in (select bankcode from ldbankunite where '"+BankCode+"' = bankunitecode )"
			         +" and managecom like '"+ComCode+"%'"
			         //+" and senddate = '"+SendDate+"'"
			         //+" and dealtype = 'S'"
			         +" and bankonthewayflag = '1'"
			         +" and cansendbank = '7'"
			         +" and not exists (select 1 from lysendtobank a where a.paycode = getnoticeno and dealtype = 'S')"
			         +" fetch first 1 rows only ";
		String sSSRS = tExeSQL.getOneValue(sSQL);
		//查询结果set到TransferData中
		transferData1.setNameAndValue("queryDealForS", sSSRS);
		
		
		
		return transferData1;
	} 

}
