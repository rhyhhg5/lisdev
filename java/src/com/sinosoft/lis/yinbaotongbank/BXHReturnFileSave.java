package com.sinosoft.lis.yinbaotongbank;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.MailSender;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LYBankLogSchema;
import com.sinosoft.lis.schema.LYSendToBankSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LYBankLogSet;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


public class BXHReturnFileSave{
//	提交数据的容器 
	private MMap map = new MMap();
	
	private	MMap mmap = new MMap();
	//得到参数 《交易类型》
	private String DealType = null;
	//得到参数 《管理机构》
	private String ComCode =  null;
	//得到参数 《银行编码》
	private String BankCode =  null;
	//得到参数 《发盘日期》
	private String SendDate =  null;
	
	private PubSubmit tPubSubmit = new PubSubmit();	
	
	private GlobalInput tG = new GlobalInput();
	
	private  int limite =  0;
	
	//传入数据的容器 
	private VData mInputData = new VData();
	
	private boolean ReturnFileSave(TransferData transferData){
		
		  try {
			  DealType = (String)transferData.getValueByName("DealType");
				//得到参数 《管理机构》
			    ComCode = (String)transferData.getValueByName("ComCode");
				//得到参数 《银行编码》
				BankCode = (String)transferData.getValueByName("BankCode");
				//得到参数 《发盘日期》	
				SendDate = (String)transferData.getValueByName("SendDate");
		    }
		    catch (Exception e) {
		      // @@错误处理
		      CError.buildErr(this, "接收数据失败");
		      return false;
		    }

		    return true;
		
		
	}
	
	public Object getData(TransferData transferData)
	{
		ReturnFileSave(transferData);
		LYSendToBankSet mLYSendToBankSet = new LYSendToBankSet();
//		LYBankLogSet outLYBankLogSet = new LYBankLogSet();
    	LYSendToBankSet outLYSendToBankSet = new LYSendToBankSet();

		//add jfr
		String sql_limit = "select code1 from ldcode1 where codetype='batchsendlimite' and code='ybtlimite' and codename = '"+BankCode+"' with ur";
        ExeSQL limiteSql = new ExeSQL();
        String limiteCont = limiteSql.getOneValue(sql_limit);
        try {
        	limite = Integer.parseInt(limiteCont);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
		
		//生成LYSendToBank
		if( "S".equals(DealType))
		{
        	
			//查询银行 add jfr  unitebankcode 大行编码
			String codealias="select distinct unitebankcode from ldbankunite where  bankunitecode='"+BankCode+"' with ur";
        	ExeSQL mExeSql = new ExeSQL();
    		SSRS tNoSSRS = mExeSql.execSQL(codealias);
    		for( int i=1 ; i<=tNoSSRS.getMaxRow(); i++ )
        	{
    			String codealias1=tNoSSRS.GetText(i, 1);
            	BXHWriteToFileBL tBXHWriteToFileBL = new BXHWriteToFileBL();
			//查询出cansendbank状态为10的数据
			LJSPayDB tLJSPayDB = new LJSPayDB();
			String sql1 = "select * from ljspay a where cansendbank = '10' and BankOnTheWayFlag = '1' and  exists "+
		            "(select bankcode from ldbankunite where bankunitecode='"+BankCode+"' and unitegroupcode in ('1','3') and unitebankcode in('"+codealias1+"') and bankcode = a.bankcode)  and "+ 
		            "not exists(select 1 from lysendtobank where a.getnoticeno = paycode ) and managecom like  '"+ComCode+"%'";
			System.out.println("sql1 :"+ sql1);
			LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(sql1);
			System.out.println("!!!!!!!!!"+tLJSPaySet.size());
			//add
			int tStartS = 1;
	        tLJSPaySet = tLJSPayDB.executeQuery(sql1,tStartS,limite);
	        if(0 == tLJSPaySet.size()){
	        	return null;
	        }
	        System.out.println("第一个收费批次的数据量" + tLJSPaySet.size());
	        
//            do{
            	LYBankLogSet outLYBankLogSet = new LYBankLogSet();
			if(tLJSPaySet!=null && tLJSPaySet.size() > 0){
				LYSendToBankSet tLYSendToBankSet = tBXHWriteToFileBL.getSendToBank(tLJSPaySet,codealias1,BankCode);
			
			if(tLYSendToBankSet.size()!=0){
				String serialno = tLYSendToBankSet.get(1).getSerialNo();
			LYBankLogSchema tLYBankLogSchema = tBXHWriteToFileBL.getBankLog();
			tLYBankLogSchema.setSerialNo(serialno);
			tLYBankLogSchema.setBankCode(BankCode);
			tLYBankLogSchema.setComCode(ComCode);
			
			//提交数据
			outLYSendToBankSet.set(tLYSendToBankSet);
			outLYBankLogSet.add(tLYBankLogSchema);
			MMap amap = new MMap();
			amap.put(outLYSendToBankSet, "INSERT");
			amap.put(outLYBankLogSet, "INSERT");
 		    
 		    mInputData.clear();
 	      mInputData.add(amap);
 	        
 	       PubSubmit ttPubSubmit = new PubSubmit();
  	      if(! ttPubSubmit.submitData(mInputData, ""))
  	      {
  	    	  System.out.println("数据提交失败！");
  	    	 return null;
  	      }
 			 }else{
 					System.out.println("没有直联模式付费数据！！！");
 					return null;
 		     }
            }
			tLJSPaySet = tLJSPayDB.executeQuery(sql1,tStartS,limite);
//         } while(tLJSPaySet!=null && tLJSPaySet.size() > 0);
        	
        	}
		}else
		{
			//查大行   add
			String codealias="select distinct unitebankcode from ldbankunite where  bankunitecode='"+BankCode+"' with ur";
        	ExeSQL mExeSql = new ExeSQL();
    		SSRS tNoSSRS = mExeSql.execSQL(codealias);
    		for( int i=1 ; i<=tNoSSRS.getMaxRow(); i++)
        	{
    	    String codealias1=tNoSSRS.GetText(i, 1);
			BXHWriteToFileABL tBXHWriteToFileABL = new BXHWriteToFileABL();
			//查询出cansendbank状态为10的数据  10---直联提数
			LJAGetDB tLJAGetDB = new LJAGetDB();
			String sql2 = "select * from ljaget a where cansendbank = '10' and BankOnTheWayFlag = '1' " +
			            "and exists (select bankcode from ldbankunite where bankunitecode='"+BankCode+"' and unitegroupcode in('2','3') and unitebankcode in ("+codealias1+") and bankcode = a.bankcode ) "+
					    "and not exists(select 1 from lysendtobank where a.actugetno = paycode ) and managecom like '"+ComCode+"%' "
					    +" and not exists (select 1 from ljfiget where actugetno=a.actugetno) ";
			LJAGetSet tLJAGetSet = tLJAGetDB.executeQuery(sql2);
			System.out.println("直联代付数据量----------"+tLJAGetSet.size());
			String sql3 = "select * from ljaget a where cansendbank = '10' and BankOnTheWayFlag = '1' " +
            "and exists (select bankcode from ldbankunite where bankunitecode='"+BankCode+"' and unitegroupcode in('2','3') and unitebankcode in ("+codealias1+") and bankcode = a.bankcode ) "+
		    "and not exists(select 1 from lysendtobank where a.actugetno = paycode ) and managecom like '"+ComCode+"%' "
		    +" and exists (select 1 from ljfiget where actugetno=a.actugetno) ";
            LJAGetSet eLJAGetSet = tLJAGetDB.executeQuery(sql3);
            System.out.println("直联代付已提数数据量----------"+eLJAGetSet.size());
            if(eLJAGetSet.size()>0){
            	sendMail(eLJAGetSet);
            }
            
            //add
            int tStartF = 1;
            tLJAGetSet = tLJAGetDB.executeQuery(sql2,tStartF,limite);
            if(tLJAGetSet.size() == 0){
            	return null;
            }
            System.out.println("第一个付费批次的数据量" + tLJAGetSet.size());
//           do{
        	   LYBankLogSet outLYBankLogSet = new LYBankLogSet();
			if(tLJAGetSet!=null && tLJAGetSet.size() > 0){
				LYSendToBankSet tLYSendToBankSet = tBXHWriteToFileABL.getSendToBank(tLJAGetSet,codealias1,BankCode);
			if(tLYSendToBankSet.size()!=0){
		    String serialno = tLYSendToBankSet.get(1).getSerialNo();
			LYBankLogSchema tLYBankLogSchema = tBXHWriteToFileABL.getBankLog();
			tLYBankLogSchema.setSerialNo(serialno);
			tLYBankLogSchema.setBankCode(BankCode);
			tLYBankLogSchema.setComCode(ComCode);
			
			outLYSendToBankSet.set(tLYSendToBankSet);
			outLYBankLogSet.add(tLYBankLogSchema);
			MMap map = new MMap();
			map.put(outLYSendToBankSet, "INSERT");
			map.put(outLYBankLogSet, "INSERT");
			
			mInputData.clear();
 	        mInputData.add(map);

 	        PubSubmit ttPubSubmit = new PubSubmit();
 	      if(! ttPubSubmit.submitData(mInputData, ""))
 	      {
 	    	  System.out.println("数据提交失败！");
 	    	 return null;
 	      }
			}else{
					System.out.println("没有直联模式付费数据！！！");
					return null;
		     }
			}
			tLJAGetSet = tLJAGetDB.executeQuery(sql2,tStartF,limite);
//           }while(tLJAGetSet.size()>0 && tLJAGetSet!=null);
		}
		}
		
		mLYSendToBankSet=setYingDaiRemark(outLYSendToBankSet);
		
		
		return mLYSendToBankSet;
	}
	private void sendMail(LJAGetSet ttLJAGetSet) {
		String tSQL = "select sysvarvalue from LDSysVar where sysvar='ServerRoot'";
		String mURL = new ExeSQL().getOneValue(tSQL);
		//mURL = "F:/";
		mURL+="vtsfile/";
		File mFileDir = new File(mURL);
		if (!mFileDir.exists()) {
			if (!mFileDir.mkdirs()) {
				System.err.println("创建目录[" + mURL.toString() + "]失败！"
						+ mFileDir.getPath());
			}
		}
		String filename = mURL + "YCErrorLJAGET.txt";
		try {
			FileOutputStream tFileOutputStream = new FileOutputStream(filename, true);
			OutputStreamWriter tOutputStreamWriter = new OutputStreamWriter(tFileOutputStream, "GBK");
	    	BufferedWriter mBufferedWriter = new BufferedWriter(tOutputStreamWriter);  
			for(int i=1;i<=ttLJAGetSet.size();i++){
				String write="";
				write = ttLJAGetSet.get(i).getActuGetNo() ;
                mBufferedWriter.write(write);
	    		mBufferedWriter.newLine();
	    		mBufferedWriter.flush();
			}
			mBufferedWriter.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
		
		String tSQLMailSql = "select code,codename from ldcode where codetype = 'premreceivablesmail' ";
		SSRS tSSRS = new ExeSQL().execSQL(tSQLMailSql);
		MailSender tMailSender = new MailSender(tSSRS.GetText(1, 1), tSSRS
				.GetText(1, 2), "picchealth");
		tMailSender.setSendInf("邮储代付发盘错误","您好：错误数据见附件！", filename);
		String tSQLRMailSql = "select codename,codealias from ldcode where codetype = 'YCErrorLJ' ";
		SSRS tSSRSR = new ExeSQL().execSQL(tSQLRMailSql);
		tMailSender.setToAddress(tSSRSR.GetText(1, 1),
				tSSRSR.GetText(1, 2), null);
		// 发送邮件
		if (!tMailSender.sendMail()) {
			deleteFile(filename);
			System.out.println(tMailSender.getErrorMessage());
		}
		deleteFile(filename);
		
	}

	//为函数增加一个参数 2010-11-18
	 public LYSendToBankSet setYingDaiRemark(LYSendToBankSet tLYSendToBankSet){
		 
		    LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
			LYSendToBankSet mLYSendToBankSet = new LYSendToBankSet();
			LYSendToBankSet oLYSendToBankSet = new LYSendToBankSet();

			for(int i = 1;i<=tLYSendToBankSet.size();i++)
			{   
				LYSendToBankSchema tLYSendToBankSchema = new LYSendToBankSchema();
				LYSendToBankSchema oLYSendToBankSchema = tLYSendToBankSet.get(i);
				tLYSendToBankSchema=getNoType(oLYSendToBankSchema);

				oLYSendToBankSet.add(tLYSendToBankSchema);
			}
			
			mmap.put(oLYSendToBankSet, "UPDATE");
			
			mInputData.clear();
		    mInputData.add(mmap);
		    
		    PubSubmit ttPubSubmit = new PubSubmit();
	 	      if(! ttPubSubmit.submitData(mInputData, ""))
	 	      {
	 	    	 System.out.println("数据提交失败！");
	 	    	 return null;
	 	      }
			
			return oLYSendToBankSet;
	    }
	 public LYSendToBankSchema getNoType(LYSendToBankSchema tLYSendToBankSchema){
	    	String tNoType = "";
	    	String flag = "";
	    	String tContno="";
	    	String tDealType=tLYSendToBankSchema.getDealType();
	    	String tOthernotype=tLYSendToBankSchema.getNoType();
	    	String tPolno=tLYSendToBankSchema.getPolNo();

	    	if("2".equals(tOthernotype)){
	    		tNoType="01";
	    		flag = getIsYingDai(tPolno);
	    		tContno=tPolno;
	    	}else if("9".equals(tOthernotype)||"16".equals(tOthernotype)){
	    		tNoType="02";
	    		tContno=tPolno;
	    	}else if("10".equals(tOthernotype)){
	    		tContno=getContno(tOthernotype,tPolno);
	    		flag=getIsYingDai(tContno);
	    		if("S".equals(tDealType)){
	    			tNoType="08";			
	    		}else{
	    			tNoType=getLJAGetEndorseType(tPolno);
	    		}
	    	}
	    	else if("5".equals(tOthernotype)||"C".equals(tOthernotype)||"F".equals(tOthernotype)){
	    		tContno=getContno(tOthernotype,tPolno);
	    		flag=getIsYingDai(tContno);	
	    	    tNoType="07";
	    	}
	    	if(tNoType!=""){
	    	tLYSendToBankSchema.setNoType(tNoType);
	    	}
	    	if(tContno!=""){
		    	tLYSendToBankSchema.setName(tContno);
		    	}
	    	tLYSendToBankSchema.setRemark(flag);
		    	
	    	return tLYSendToBankSchema;
	    }
	 
	 public String getContno(String tOthernotype,String tPolno){
	    	String tContno = "";
	    	if("10".equals(tOthernotype)){
	    	String endorsementnosql="select contno from lpedormain where edorno='"+tPolno+"' fetch first 1 rows only ";
	    	ExeSQL contnoSQL = new ExeSQL();
	    	tContno = contnoSQL.getOneValue(endorsementnosql);
	    	}else if("5".equals(tOthernotype)||"C".equals(tOthernotype)||"F".equals(tOthernotype)){
		    	String claimnosql="select contno from ljagetclaim where otherno='"+tPolno+"' fetch first 1 rows only ";
		    	ExeSQL contnoSQL = new ExeSQL();
		    	tContno = contnoSQL.getOneValue(claimnosql);
		    }
	    	return tContno;
	    }
	
	 public String getIsYingDai(String contno){
	    	String tisyouchu = "";
	    	String mLJSPaySql = "select 1 from lccont where contno='"+contno+"' and substr(bankcode,1,2)='16' and salechnl='04'"
	   	+" union all  select 1 from lbcont where contno='"+contno+"'  and substr(bankcode,1,2)='16'  and salechnl='04'  ";
	    	ExeSQL bankExeSQL = new ExeSQL();
	         String UniteBank = bankExeSQL.getOneValue(mLJSPaySql);
	         System.out.println("是否银保通投保的银行客户:"+UniteBank);
	    	if("1".equals(UniteBank.trim())){
	    		tisyouchu = "1";
	    	}
	    	return tisyouchu;
	    }

	  public String getLJAGetEndorseType(String tEndorsementno){
		  String tType="";
	    	String sql = " select FeeOperationType from ljagetendorse where endorsementno = '"+tEndorsementno+"' fetch first 1 rows only";
	    	ExeSQL tTypeSQL = new ExeSQL();
	    	tType = tTypeSQL.getOneValue(sql);
	    	if("WT".equals(tType)){
	    		tType="04";
	    	}else if("CT".equals(tType)){
	    		tType="05";
	    	}else if("MJ".equals(tType)){
	    		tType="06";
	    	}else{
	    		tType="03";
	    	}
			return tType;
	    }
	 
	public static void main(String[] args) {
		
		BXHReturnFileSave save = new BXHReturnFileSave();
		TransferData transferData = new TransferData();
		transferData.setNameAndValue("DealType","S");
		transferData.setNameAndValue("ComCode","86");
		transferData.setNameAndValue("BankCode","7711");
		transferData.setNameAndValue("SendDate","2018-12-18");
		
		save.getData(transferData);
		
	}
	
	private boolean deleteFile(String cFilePath) {
		File f = new File(cFilePath);
		if (!f.exists()) {
			System.out.println("没有找到需要删除的文件");
			return false;
		}
		f.delete();
		return true;
	}

}

