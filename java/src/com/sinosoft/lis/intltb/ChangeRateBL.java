package com.sinosoft.lis.intltb;

import com.sinosoft.lis.db.LCRiskFeeRateDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCRiskFeeRateSchema;
import com.sinosoft.lis.vschema.LCRiskFeeRateSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class ChangeRateBL {
	private LCRiskFeeRateSet mLCRiskFeeRateSet = new LCRiskFeeRateSet();

	private GlobalInput mGlobalInput = new GlobalInput();

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mInputData = new VData();

	private MMap map = new MMap();

	private String cOperate;
	
	private String wrapCode;
	private String riskCode;

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		this.cOperate = cOperate;
		System.out.println("----ChangeRateBL--Start----");
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		System.out.println("----dealData--Start----");
		if (!dealData()) {
			return false;
		}
		if (!prepareOutputData()) {
			return false;
		}
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, cOperate)) {
			// 错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}
		System.out.println("----ChangeRateBL--End----");
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput.setSchema((GlobalInput) cInputData
				.getObjectByObjectName("GlobalInput", 0));
		this.mLCRiskFeeRateSet = (LCRiskFeeRateSet) cInputData
				.getObjectByObjectName("LCRiskFeeRateSet", 0);
		if (this.mLCRiskFeeRateSet == null || this.mGlobalInput == null) {
			// 错误处理
			System.out.println("传入的处理信息不全");
			CError tError = new CError();
			tError.moduleName = "ChangeRateBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的处理信息不全！";
			this.mErrors.addOneError(tError);
			return false;
		}
		if (mLCRiskFeeRateSet.size() == 0) {
			// 错误处理
			System.out.println("插入数据库数据为空");
			CError tError = new CError();
			tError.moduleName = "ChangeRateBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "插入数据库数据为空，请重新进入界面填写";
			this.mErrors.addOneError(tError);
		}
		
		/*
		 * this.manCom = mGlobalInput.ManageCom; for (int i = manCom.length(); i
		 * < 8; i++) { manCom = manCom + "0"; }
		 */
		return true;
	}

	private boolean dealData() {
		boolean dealSuccFlag = false;

		// 太紧了 先这么改吧 方式要调
		String prtno = mLCRiskFeeRateSet.get(1).getPrtNo();
//		String sql = "select riskcode from lcpol where prtno='" + prtno + "' and riskcode in ('332901','333001','334601')";
		String sql = "select distinct riskwrapcode from lcriskdutywrap where prtno='" + prtno + "'";
		ExeSQL tExeSQL = new ExeSQL();
		wrapCode = tExeSQL.getOneValue(sql);
		
//		if ("332901".equals(riskCode)){
//			wrapCode = "WR0260";
//		} else if ("333001".equals(riskCode)){
//			wrapCode = "WR0270";
//		} else if ("334601".equals(riskCode)){
//			wrapCode = "WR0312";
//		} else {
//			wrapCode = "";
//			riskCode = "";
//		}
		
		if ("INSERT".equals(this.cOperate)) {
			dealSuccFlag = insertFeeRate();
		} else if ("UPDATE".equals(this.cOperate)) {
			dealSuccFlag = updateFeeRate();
		} else if ("DELETE".equals(this.cOperate)) {
			dealSuccFlag = deleteFeeRate();
		} else {
			// 错误处理
			System.out.println("操作符异常");
			CError tError = new CError();
			tError.moduleName = "ChangeRateBL";
			tError.functionName = "dealDate";
			tError.errorMessage = "操作符异常！";
			this.mErrors.addOneError(tError);
		}
		return dealSuccFlag;
	}

	private boolean insertFeeRate() {
		String sql = "select 1 from LCRiskFeeRate where prtno='"
				+ mLCRiskFeeRateSet.get(1).getPrtNo() + "'";

		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS.MaxRow != 0) {
			System.out.println("费率已存在，请点击修改按钮");
			CError tError = new CError();
			tError.moduleName = "ChangeRateBL";
			tError.functionName = "insertFeeRate";
			tError.errorMessage = "费率已存在，请点击修改按钮";
			this.mErrors.addOneError(tError);
			return false;
		}

		LCRiskFeeRateSet inLCRiskFeeRateSet = new LCRiskFeeRateSet();
		for (int num = 1; num <= mLCRiskFeeRateSet.size(); num++) {
			LCRiskFeeRateSchema tLCRiskFeeRateSchema = mLCRiskFeeRateSet
					.get(num);
			tLCRiskFeeRateSchema.setRiskWrapCode(wrapCode);
			tLCRiskFeeRateSchema.setRiskCode(wrapCode);
			tLCRiskFeeRateSchema.setOperator(mGlobalInput.Operator);
			tLCRiskFeeRateSchema.setMakeDate(PubFun.getCurrentDate());
			tLCRiskFeeRateSchema.setMakeTime(PubFun.getCurrentTime());
			tLCRiskFeeRateSchema.setModifyDate(PubFun.getCurrentDate());
			tLCRiskFeeRateSchema.setModifyTime(PubFun.getCurrentTime());
			if(!checkSchema(tLCRiskFeeRateSchema)){
				return false;
			}
			inLCRiskFeeRateSet.add(tLCRiskFeeRateSchema);
		}
		map.put(inLCRiskFeeRateSet, "INSERT");
		return true;
	}

	private boolean updateFeeRate() {
		String sql = "select 1 from LCRiskFeeRate where prtno='"
				+ mLCRiskFeeRateSet.get(1).getPrtNo() + "'";

		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS.MaxRow == 0) {
			System.out.println("费率尚未进行录入，请点击保存按钮");
			CError tError = new CError();
			tError.moduleName = "ChangeRateBL";
			tError.functionName = "updateFeeRate";
			tError.errorMessage = "费率尚未进行录入，请点击保存按钮";
			this.mErrors.addOneError(tError);
			return false;
		}

		LCRiskFeeRateSet upLCRiskFeeRateSet = new LCRiskFeeRateSet();
		for (int num = 1; num <= mLCRiskFeeRateSet.size(); num++) {
			// 先查询
			LCRiskFeeRateDB tLCRiskFeeRateDB = new LCRiskFeeRateDB();
			tLCRiskFeeRateDB.setPrtNo(mLCRiskFeeRateSet.get(num).getPrtNo());
			tLCRiskFeeRateDB.setRiskWrapCode(wrapCode);
			tLCRiskFeeRateDB.setRiskCode(wrapCode);
			tLCRiskFeeRateDB
					.setFeeYear(mLCRiskFeeRateSet.get(num).getFeeYear());
			if (!tLCRiskFeeRateDB.getInfo()) {
				System.out.println("费率信息查询失败");
				CError tError = new CError();
				tError.moduleName = "PayAdvanceBL";
				tError.functionName = "getInputData";
				tError.errorMessage = "费率信息查询失败！";
				this.mErrors.addOneError(tError);
				return false;
			}

			// 查询后修改字段
			LCRiskFeeRateSchema tLCRiskFeeRateSchema = tLCRiskFeeRateDB
					.getSchema();
			tLCRiskFeeRateSchema.setDrawRate(mLCRiskFeeRateSet.get(num)
					.getDrawRate());
			tLCRiskFeeRateSchema.setCancelRate(mLCRiskFeeRateSet.get(num)
					.getDrawRate());
			if(!checkSchema(tLCRiskFeeRateSchema)){
				return false;
			}
			upLCRiskFeeRateSet.add(tLCRiskFeeRateSchema);
		}
		map.put(upLCRiskFeeRateSet, "UPDATE");
		return true;
	}

	private boolean deleteFeeRate() {
		String sql = "select 1 from LCRiskFeeRate where prtno='"
			+ mLCRiskFeeRateSet.get(1).getPrtNo() + "'";

		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS.MaxRow == 0) {
			System.out.println("费率尚未进行录入");
			CError tError = new CError();
			tError.moduleName = "ChangeRateBL";
			tError.functionName = "updateFeeRate";
			tError.errorMessage = "费率尚未进行录入";
			this.mErrors.addOneError(tError);
			return false;
		}
		LCRiskFeeRateSet inLCRiskFeeRateSet = new LCRiskFeeRateSet();
		for (int num = 1; num <= mLCRiskFeeRateSet.size(); num++) {
			// 先查询
			LCRiskFeeRateDB tLCRiskFeeRateDB = new LCRiskFeeRateDB();
			if(mLCRiskFeeRateSet.get(num).getPrtNo() == null || mLCRiskFeeRateSet.get(num).getPrtNo().equals("")){
				// 错误处理
				System.out.println("插入数据库数据为空");
				CError tError = new CError();
				tError.moduleName = "ChangeRateBL";
				tError.functionName = "getInputData";
				tError.errorMessage = "插入数据库数据为空，请重新进入界面填写";
				this.mErrors.addOneError(tError);
				return false;
			}
			tLCRiskFeeRateDB.setPrtNo(mLCRiskFeeRateSet.get(num).getPrtNo());
			tLCRiskFeeRateDB.setRiskWrapCode(wrapCode);
			tLCRiskFeeRateDB.setRiskCode(wrapCode);
			tLCRiskFeeRateDB
					.setFeeYear(mLCRiskFeeRateSet.get(num).getFeeYear());
			if (!tLCRiskFeeRateDB.getInfo()) {
				System.out.println("费率信息查询失败");
				CError tError = new CError();
				tError.moduleName = "ChangeRateBL";
				tError.functionName = "updateFeeRate";
				tError.errorMessage = "费率信息查询失败！";
				this.mErrors.addOneError(tError);
				return false;
			}
			inLCRiskFeeRateSet.add(tLCRiskFeeRateDB.getSchema());
		}
		map.put(inLCRiskFeeRateSet, "DELETE");
		return true;
	}
	
	private boolean checkSchema(LCRiskFeeRateSchema tLCRiskFeeRateSchema){
		if(tLCRiskFeeRateSchema.getPrtNo() == null || tLCRiskFeeRateSchema.getPrtNo().trim().equals("")){
			System.out.println("费率信息查询失败");
			CError tError = new CError();
			tError.moduleName = "ChangeRateBL";
			tError.functionName = "checkSchema";
			tError.errorMessage = "费率信息查询失败！";
			this.mErrors.addOneError(tError);
			return false;
		} else if (tLCRiskFeeRateSchema.getDrawRate() > 0.02 || tLCRiskFeeRateSchema.getDrawRate() < 0){
			System.out.println("费率录入错误");
			CError tError = new CError();
			tError.moduleName = "ChangeRateBL";
			tError.functionName = "checkSchema";
			tError.errorMessage = "费率数值录入错误,费率需大于等于0、小于等于0.02";
			this.mErrors.addOneError(tError);
			return false;
		} else if (tLCRiskFeeRateSchema.getCancelRate() > 0.02 || tLCRiskFeeRateSchema.getCancelRate() < 0){
			System.out.println("费率录入错误");
			CError tError = new CError();
			tError.moduleName = "ChangeRateBL";
			tError.functionName = "checkSchema";
			tError.errorMessage = "费率数值录入错误,费率需大于等于0、小于等于0.02";
			this.mErrors.addOneError(tError);
			return false;
		} else if (!(tLCRiskFeeRateSchema.getCancelRate() + "").matches("0|(0.([0-9]?){3}(0*))") &&
				!(tLCRiskFeeRateSchema.getCancelRate() + "").matches("0|(0.([0-9]?){3}(0*))")){
			System.out.println("费率录入错误");
			CError tError = new CError();
			tError.moduleName = "ChangeRateBL";
			tError.functionName = "checkSchema";
			tError.errorMessage = "费率数值录入错误,费率小数点位数最大为3位";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean prepareOutputData() {
		try {
			mInputData.clear();
			mInputData.add(map);
		} catch (Exception ex) {
			// @@错误处理
			System.out.println(ex.getMessage());
			CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
			return false;
		}
		return true;
	}

	public static void main(String[] arr) {
	}
}