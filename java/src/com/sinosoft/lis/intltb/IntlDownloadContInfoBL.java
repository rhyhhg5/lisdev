package com.sinosoft.lis.intltb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.FileWriter;
import java.io.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 接收IntlDownloadContInfoUI.java传入的Sql信息直接进行查询，保证所见即所得。
 * 根据查询得到的保单信息，调用WriteToExcel生成excel。并调用通用下载组件下载生成的excel。
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class IntlDownloadContInfoBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;

    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    public IntlDownloadContInfoBL()
    {
    }

    /**
     *外部调用的接口。得到客户端的查询Sql，调用业务逻辑方法dealData生成excel。
     * @param cInputData VData：包括：
     * 1、GlobalInput对象，操作员信息
     * 2、BString对象：页面查询的sql
     * @param operate String： 数据操作字符串，
     * @return boolean：true提交成功, false提交失败
     */
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();

        System.out.println(mSql);
        SSRS tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "IntlDownloadContInfoBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        String[][] mToExcel = new String[tSSRS.getMaxRow() + 1][10];

        //标题
        mToExcel[0][0] = "印刷号";
        mToExcel[0][1] = "保险合同号";
        mToExcel[0][2] = "管理机构";
        mToExcel[0][3] = "销售渠道";
        mToExcel[0][4] = "业务员";
        mToExcel[0][5] = "投保人";
        mToExcel[0][6] = "生效日期";
        mToExcel[0][7] = "保险止期";
        mToExcel[0][8] = "保险费";
        mToExcel[0][9] = "录单员";

        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row][col - 1] = tSSRS.GetText(row, col);
            }
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "IntlDownloadContInfoBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("Sql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");

        if(mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "IntlDownloadContInfoBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }


    public static void main(String[] args)
    {
        IntlDownloadContInfoBL intldownloadcontinfobl = new
            IntlDownloadContInfoBL();
    }
}
