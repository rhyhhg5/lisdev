package com.sinosoft.lis.intltb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ChangeRateUI {
	private VData mInputData;
	public CErrors mErrors = new CErrors();
	/** 传出数据的容器 */
	  private VData mResult = new VData();

	// 传输数据的公共方法
	public boolean submitData(VData cInputData, String cOperate) {
		// 首先将数据在本类中做一个备份
		mInputData = (VData) cInputData.clone();

		ChangeRateBL tChangeRateBL = new ChangeRateBL();
		System.out.println("Start ChangeRate UI Submit...");
		if (!tChangeRateBL.submitData(mInputData, cOperate)) {
			this.mErrors.copyAllErrors(tChangeRateBL.mErrors);
			mResult.clear();
			return false;
		} 
		System.out.println("End ChangeRate UI Submit...");
		mInputData = null;
		return true;
	}

	/**
	 * 数据输出方法，供外界获取数据处理结果
	 * 
	 * @return 包含有数据查询结果字符串的VData对象
	 */
	public VData getResult() {
		return mResult;
	}
}