package com.sinosoft.lis.intltb;

import com.sinosoft.lis.brieftb.*;
import com.sinosoft.lis.cbcheck.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 接收ContInsuredIntlUI.java传入的数据，进行合同信息的处理，
 * 包括新增、删除、修改，若投保人是被保人，且已录入其险种信息，则需要重算保费。
 * 算费时，先通过LCRiskDutyWrap信息准备心中LCPol、责任LCDuty信息，调用CalBL进行保费计算。
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class ContInsuredIntlBL
{
    public final String INSERT = "INSERT||CONTINSURED";

    public final String UPDATE = "UPDATE||CONTINSURED";

    public final String DELETE = "DELETE||CONTINSURED";

    public final String DELETE_RISK = "DELETE||INSUREDRISK";

    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    //统一更新日期，时间
    private String mCurDate = PubFun.getCurrentDate();

    private String mCurTime = PubFun.getCurrentTime();

    /** 数据操作字符串 */
    private String mOperate;

    private GlobalInput tGI = null;

    private VData mInputData = null; //外部传入的待处理数据集合

    private LCContSchema mLCContSchema = new LCContSchema(); //合同信息

    private LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema(); //被保人信息

    private LCInsuredDB mOLDLCInsuredDB = new LCInsuredDB(); //记录需要删除或修改的客户

    private LDPersonSchema mLDPersonSchema = new LDPersonSchema(); //客户信息

    private LCAddressSchema mLCAddressSchema = new LCAddressSchema(); //被保人地址信息

    private LCRiskDutyWrapSet mLCRiskDutyWrapSet = new LCRiskDutyWrapSet(); //套餐信息

    private LCDiseaseResultSet mLCDiseaseResultSet = new LCDiseaseResultSet(); //告知整理

    private LCNationSet mLCNationSet = new LCNationSet(); //抵达国家

    private TransferData mTransferData = new TransferData(); //不能通过Schema传递的变量

    /** 往界面传输数据的容器 */
    private MMap map = new MMap();

    public ContInsuredIntlBL()
    {
    }

    /**
     * 外部调用的接口。获取被保人信息，调用业务逻辑方法dealData进行业务处理。
     * @param cInputData VData：包含：
     * A.	GlobalInput对象，完整的登陆用户信息。
     * B.	LCContSchemaSchema 对象：保单信息
     * C.	LDPersonSchema 对象：页面录入的被保人信息
     * D.	LCInsuredSchema 对象：页面录入的被保人信息
     * E.	LCAddressSchema 对象：页面录入的被保人地址信息
     * F.	LCRiskDutyWrapSet 对象：套餐信息
     * G.	LCDiseaseResultSet mLCDiseaseResultSet：被保人告知整理信息
     * @param cOperate String：操作方式，包括“INSERT”、“UPDATE”、“DELETE”，用来区分队被保人的不同的操作
     * @return MMap：处理后的结果集合
     */
    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        System.out.println("\n---In ContInsuredIntlBL.getSubmitMap---");

        mInputData = (VData) cInputData.clone();

        mOperate = cOperate;
        System.out.println("Operate==" + mOperate);

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(mInputData))
        {
            return null;
        }
        System.out.println("After getinputdata");

        if (!checkData())
        {
            return null;
        }

        //进行业务处理
        if (!dealData())
        {
            return null;
        }
        System.out.println("After dealData！");

        return map;
    }

    /**
     * 相关说明见getSubmitMap
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (getSubmitMap(cInputData, cOperate) == null)
        {
            return false;
        }

        VData data = new VData();
        data.add(map);
        printObjectNames(map);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            mErrors.copyAllErrors(p.mErrors);

            CError tError = new CError();
            tError.moduleName = "ContInsuredIntlBL";
            tError.functionName = "submitData";
            tError.errorMessage = "提交数据失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * getInputData
     * 将输入数据复制到本类全局对象中
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //注意，为在不同的类之间相互调用，并实时得到最新数据，以下Schema只能使用引用
        tGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);

        mLCContSchema = (LCContSchema) cInputData.getObjectByObjectName("LCContSchema", 0);
        mLDPersonSchema = (LDPersonSchema) cInputData.getObjectByObjectName("LDPersonSchema", 0);
        mLCInsuredSchema = (LCInsuredSchema) cInputData.getObjectByObjectName("LCInsuredSchema", 0);
        mLCAddressSchema = (LCAddressSchema) cInputData.getObjectByObjectName("LCAddressSchema", 0);
        mOLDLCInsuredDB = (LCInsuredDB) cInputData.getObjectByObjectName("LCInsuredDB", 0);
        mLCRiskDutyWrapSet = (LCRiskDutyWrapSet) cInputData.getObjectByObjectName("LCRiskDutyWrapSet", 0);
        mLCDiseaseResultSet = (LCDiseaseResultSet) cInputData.getObjectByObjectName("LCDiseaseResultSet", 0);
        mLCNationSet = (LCNationSet) mInputData.getObjectByObjectName("LCNationSet", 0);
        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);

        if (tGI == null || tGI.Operator == null)
        {
            CError tError = new CError();
            tError.moduleName = "ContInsuredIntlBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有传入操作员信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        if (mLCContSchema == null || mLCContSchema.getContNo() == null || "".equals(mLCContSchema.getContNo()))
        {
            CError tError = new CError();
            tError.moduleName = "ContInsuredIntlBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有读取到保单信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        if (mLDPersonSchema == null || mLCInsuredSchema == null || mLCAddressSchema == null || mOLDLCInsuredDB == null
                || mLCRiskDutyWrapSet == null || mLCDiseaseResultSet == null)
        {
            CError tError = new CError();
            tError.moduleName = "ContInsuredIntlBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "被保人信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * checkData
     * 校验是否可被保人信息处理，合同信息必须已保存，才能进行被保人信息保存。
     * 必须先保存被保人信息，才能进行被保人信息修改和删除。
     * @return boolean
     */
    private boolean checkData()
    {
        LCContDB db = new LCContDB();
        db.setContNo(mLCContSchema.getContNo());
        if (!db.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "ContInsuredIntlBL";
            tError.functionName = "checkData";
            tError.errorMessage = "没有查询到保单信息";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        mLCContSchema.setSchema(db.getSchema());

        return true;
    }

    /**
     * dealData
     * 进行业务逻辑处理，生成处理后的结果集合，放入map中。
     * 本方法中，调用ContInsuredBL进行被保人基本信息处理，
     * 调用ImpartToICDBL进行告知录入，
     * 调用BriefSingleContInputBL进行险种信息录入。
     * @return boolean
     */
    private boolean dealData()
    {
        if (this.INSERT.equals(this.mOperate))
        {
            try
            {
                map = dealInsert();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
        else if (this.DELETE.equals(this.mOperate))
        {
            map = dealDelete();
        }
        else if (this.UPDATE.equals(this.mOperate))
        {
            map = dealUpdate();
        }

        if (map == null)
        {
            return false;
        }

        return true;
    }

    /**
     * 处理修改被保人信息操作
     * 包含修改被保人基本信息，删除险种、删除告知信息
     * @return boolean
     */
    private MMap dealDelete()
    {
        MMap tMMapSub = new MMap();

        //多次删除投保人作为被保人的被保人数据会导致重复插入，所以这个地方每次都删除LOBInsured
        String temp = "delete from LOBInsured " + "where ContNo = '" + mLCContSchema.getContNo() + "' "
                + "   and InsuredNo = '" + mOLDLCInsuredDB.getInsuredNo() + "' ";
        tMMapSub.put(temp, SysConst.DELETE);

        // 被保人告知备份表同一保单只能存一回，因此，提前删除
        String tStrDelStr = null;
        tStrDelStr = "delete from LOBCustomerImpart " + "where ContNo = '" + mLCContSchema.getContNo() + "' "
                + " and CustomerNo = '" + mOLDLCInsuredDB.getInsuredNo() + "' ";
        tMMapSub.put(tStrDelStr, SysConst.DELETE);
        tStrDelStr = null;

        tStrDelStr = "delete from LOBCustomerImpartParams " + "where ContNo = '" + mLCContSchema.getContNo() + "' "
                + " and CustomerNo = '" + mOLDLCInsuredDB.getInsuredNo() + "' ";
        tMMapSub.put(tStrDelStr, SysConst.DELETE);
        tStrDelStr = null;

        tStrDelStr = "delete from LOBCustomerImpartDetail " + "where ContNo = '" + mLCContSchema.getContNo() + "' "
                + " and CustomerNo = '" + mOLDLCInsuredDB.getInsuredNo() + "' ";
        tMMapSub.put(tStrDelStr, SysConst.DELETE);
        // --------------------

        //修改被保人基本信息
        //        if(this.DELETE.equals(this.mOperate))
        //        {
        ContInsuredBL tContInsuredBL = new ContInsuredBL();
        MMap tMMap = tContInsuredBL.getSubmitMap((VData) mInputData.clone(), DELETE);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tContInsuredBL.mErrors);
            return null;
        }
        tMMapSub.add(tMMap);

        //将生成的被保人信息覆盖mInputData传入的对应schema
        //LCInsuredSchema schema = (LCInsuredSchema) tMMap
        //                         .getObjectByObjectName("LCInsuredSchema", 0);
        //if(schema != null)
        //{
        //    this.mLCInsuredSchema.setSchema(schema);
        //}
        //        }

        //删除险种、删除告知信息

        //删除被保人险种信息
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(mLCContSchema.getContNo());
        tLCPolDB.setInsuredNo(mLCInsuredSchema.getInsuredNo());
        LCPolSet set = tLCPolDB.query();

        VData data = new VData();
        for (int i = 1; i <= set.size(); i++)
        {
            TransferData tf = new TransferData();
            tf.setNameAndValue("PolNo", set.get(i).getPolNo());

            data.clear();
            data.add(tf);

            tContInsuredBL = new ContInsuredBL();
            MMap tMMapRisk = tContInsuredBL.getSubmitMap(data, DELETE_RISK);
            if (tMMapRisk == null)
            {
                mErrors.copyAllErrors(tContInsuredBL.mErrors);
                return null;
            }
            tMMapSub.add(tMMapRisk);
        }

        //删除告知信息
        ImpartToICDBL tImpartToICDBL = new ImpartToICDBL();
        tImpartToICDBL.setLCInsured(mLCInsuredSchema.getSchema());
        MMap tMMap2 = tImpartToICDBL.getSubmitMap((VData) mInputData.clone(), DELETE);
        if (tMMap2 == null)
        {
            mErrors.copyAllErrors(tImpartToICDBL.mErrors);
            return null;
        }
        tMMapSub.add(tMMap2);
        if (!this.UPDATE.equals(this.mOperate))
        {
            MMap tMMap3 = dealNation();
            if (tMMap3 == null)
            {
                return null;
            }
            tMMapSub.add(tMMap3);
        }
        return tMMapSub;
    }

    /**
     * 处理修改被保人信息操作
     * 1、修改被保人基本信息
     * 2、删除被保人险种信息
     * 3、生成新的被保人信息
     * @return boolean
     */
    private MMap dealUpdate()
    {
        //        MMap tMMapSub = new MMap();

        //        MMap tMMap1 = updateInsured();
        //        if(tMMap1 == null)
        //        {
        //            return null;
        //        }
        //        System.out.println("tMMap1:");
        //        printObjectNames(tMMap1);
        //
        //
        //        tMMapSub.add(tMMap1);
        //        System.out.println("tMMapSub:");
        //        printObjectNames(tMMapSub);

        MMap tMMap2 = this.dealDelete();
        if (tMMap2 == null)
        {
            return null;
        }
        System.out.println("tMMap2:");
        printObjectNames(tMMap2);

        //        tMMapSub.add(tMMap2);
        //        System.out.println("tMMapSub:");
        //        printObjectNames(tMMapSub);

        MMap tMMap3 = this.dealInsert();
        if (tMMap3 == null)
        {
            return null;
        }
        System.out.println("tMMap3:");
        printObjectNames(tMMap3);

        tMMap2.add(tMMap3);
        System.out.println("tMMapSub:");
        printObjectNames(tMMap2);

        //不知道为什么，在执行了tMMapTemp.add(tMMap3)后，LDPersonSchema对象会被LCRiskDutuWrapSet覆盖
        //在此先将LDPersonSchema从tMMap2中备份出来，再放到tMMapTemp。注意必须schema3.getSchema()
        //        LDPersonSchema schema3 = (LDPersonSchema)tMMap3.getObjectByObjectName("LDPersonSchema", 0);
        //        String action = (String) tMMap3.get(schema3);

        //        tMMapSub.put(schema3.getSchema(), action);

        LCInsuredSchema newLCInsuredSchema = (LCInsuredSchema) tMMap3.getObjectByObjectName("LCInsuredSchema", 0);

        MMap tMMap4 = dealOBData(newLCInsuredSchema.getInsuredNo());
        tMMap2.add(tMMap4);
        printObjectNames(tMMap2);

        MMap tMMap5 = dealNation();
        if (tMMap5 == null)
        {
            return null;
        }
        tMMap2.add(tMMap5);

        return tMMap2;
    }

    /**
     * 在修改被保人时，若被保人没有换号则将备份到OB表的数据删除，
     * 这样是为了避免再次修改时OB表主键冲突
     * @param newInsuredNo String
     * @return boolean
     */
    private MMap dealOBData(String newInsuredNo)
    {
        MMap tMMap = new MMap();

        if (!mOLDLCInsuredDB.getInsuredNo().equals(newInsuredNo))
        {
            return tMMap;
        }

        String[] tables1 = { "LOBDuty", "LOBPrem", "LOBGet" };

        String[] tables2 = { "LOBPol" };

        for (int i = 0; i < tables1.length; i++)
        {
            String sql = "delete from " + tables1[i] + " " + "where PolNo in " + "   (select PolNo from LCPol "
                    + "   where ContNo = '" + mLCContSchema.getContNo() + "') ";
            tMMap.put(sql, SysConst.DELETE);
        }

        for (int i = 0; i < tables2.length; i++)
        {
            String sql = "delete from " + tables2[i] + " " + "where ContNo = '" + mLCContSchema.getContNo() + "' "
                    + "   and InsuredNo = '" + mOLDLCInsuredDB.getInsuredNo() + "' ";
            tMMap.put(sql, SysConst.DELETE);
        }

        String sql = "delete from LOBDiseaseResult " + "where ContNo = '" + mLCContSchema.getContNo() + "' "
                + "   and CustomerNo = '" + mOLDLCInsuredDB.getInsuredNo() + "' ";
        tMMap.put(sql, SysConst.DELETE);

        return tMMap;
    }

    private void printObjectNames(MMap tMMap)
    {
        for (int j = 0; j < tMMap.keySet().size(); j++)
        {
            Object o = tMMap.getOrder().get(String.valueOf(j + 1));
            System.out.println(o);
        }
        System.out.println();
    }

    /**
     * updateInsured
     *修改被保人基本信息
     * @return MMap
     */
    private MMap updateInsured()
    {
        //修改被保人基本信息
        ContInsuredBL tContInsuredBL = new ContInsuredBL();
        MMap tMMap = tContInsuredBL.getSubmitMap((VData) mInputData.clone(), UPDATE);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tContInsuredBL.mErrors);
            return null;
        }

        //将生成的被保人信息覆盖mInputData传入的对应schema
        LCInsuredSchema schema = (LCInsuredSchema) tMMap.getObjectByObjectName("LCInsuredSchema", 0);
        this.mLCInsuredSchema.setSchema(schema);

        return tMMap;
    }

    /**
     * 处理生成被保人信息操作
     * @return boolean
     */
    private MMap dealInsert()
    {
        MMap tMMapSub = new MMap();

        //生成被保人基本信息
        //        if(this.INSERT.equals(mOperate))
        //        {
        ContInsuredBL tContInsuredBL = new ContInsuredBL();
        MMap tMMap = tContInsuredBL.getSubmitMap((VData) mInputData.clone(), this.INSERT);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tContInsuredBL.mErrors);
            return null;
        }

        tMMapSub.add(tMMap);

        //将生成的被保人信息覆盖mInputData传入的对应schema
        LCInsuredSchema schema = (LCInsuredSchema) tMMap.getObjectByObjectName("LCInsuredSchema", 0);
        if (schema != null)
        {
            this.mLCInsuredSchema.setSchema(schema);
        }
        LCContSchema contSchema = (LCContSchema) tMMap.getObjectByObjectName("LCContSchema", 0);
        if (contSchema != null)
        {
            this.mLCContSchema.setSchema(contSchema);
        }
        //        }

        //生成告知信息
        ImpartToICDBL tImpartToICDBL = new ImpartToICDBL();
        tImpartToICDBL.setLCInsured(mLCInsuredSchema.getSchema());
        MMap tMMap2 = tImpartToICDBL.getSubmitMap((VData) mInputData.clone(), INSERT);
        if (tMMap2 == null)
        {
            mErrors.copyAllErrors(tImpartToICDBL.mErrors);
            return null;
        }
        tMMapSub.add(tMMap2);

        //生成被保人险种信息
        BriefSingleContInputBL bl = new BriefSingleContInputBL();
        MMap tMMap3 = bl.getSubmitRiskMap((VData) mInputData.clone(), INSERT);
        if (tMMap3 == null)
        {
            mErrors.copyAllErrors(bl.mErrors);
            return null;
        }
        tMMapSub.add(tMMap3);

        if (!this.UPDATE.equals(this.mOperate))
        {
            MMap tMMap4 = dealNation();
            if (tMMap4 == null)
            {
                return null;
            }
            tMMapSub.add(tMMap4);

        }

        return tMMapSub;
    }

    /**
     *
     * @param args
     */
    private MMap dealNation()
    {
        System.out.println("into deal nation information ...");
        //抵达国家
        MMap mMap = new MMap();
        LDNationDB tLDNationDB = new LDNationDB();
        for (int i = 1; i <= this.mLCNationSet.size(); i++)
        {
            if (StrTool.cTrim(mLCNationSet.get(i).getNationNo()).equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "ContBL";
                tError.functionName = "dealDate";
                tError.errorMessage = "没有国家代码！";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return null;
            }
            tLDNationDB.setNationNo(mLCNationSet.get(i).getNationNo());
            if (!tLDNationDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "ContInsuredIntlBL";
                tError.functionName = "dealNation";
                tError.errorMessage = "没有找到国家代码对应的国家名称！";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return null;
            }
            mLCNationSet.get(i).setContNo(mLCContSchema.getContNo());
            mLCNationSet.get(i).setGrpContNo(SysConst.ZERONO);
            mLCNationSet.get(i).setEnglishName(tLDNationDB.getEnglishName());
            mLCNationSet.get(i).setOperator(this.tGI.Operator);
        }
        PubFun.fillDefaultField(mLCNationSet);

        if (this.INSERT.equals(this.mOperate))
        {
            mMap.put(mLCNationSet, "INSERT");
        }
        else if (this.DELETE.equals(this.mOperate))
        {
            mMap.put("delete from LCNation where ContNo='" + mLCContSchema.getContNo() + "'", "DELETE");
        }
        else if (this.UPDATE.equals(this.mOperate))
        {
            mMap.put("delete from LCNation where ContNo='" + mLCContSchema.getContNo() + "'", "DELETE");
            mMap.put(mLCNationSet, "INSERT");
        }
        System.out.println("完成抵达国家处理");
        return mMap;
    }

    public static void main(String[] args)
    {
        ContInsuredIntlBL bl = new ContInsuredIntlBL();
    }
}
