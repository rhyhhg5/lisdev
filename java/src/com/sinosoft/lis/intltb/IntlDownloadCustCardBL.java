package com.sinosoft.lis.intltb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.FileWriter;
import java.io.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 接收IntlDownloadCustCardUI.java传入的保单号，查询该保单被保人，并生保单客户卡信息。
 * 生成了客户卡信息后，往LCContPrint存储下载信息，用来记录下载信息。
 * 生成txt文件时使用FileWriter
 *  </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class IntlDownloadCustCardBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private LCContSet mLCContSet = null;
    private String mOutXmlPath = null;
    private String mOppositePath = null;
    private String mSuffix = null;

    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    private MMap map = new MMap();

    public IntlDownloadCustCardBL()
    {
    }

    /**
     * 外部调用的接口。获取被保人信息，调用业务逻辑方法dealData进行业务处理。
     * @param cInputData VData：包含：
     A.	GlobalInput对象，完整的登陆用户信息。
     B.	LCContSet mLCContSet：保单信息
     * @param cOperator String：操作方式，可为“”
     * @return boolean：成功true，否则false
     */
    public boolean submitData(VData cInputData, String operate)
    {
        if(getSubmitData(cInputData, operate) == null)
        {
            return false;
        }

        VData data = new VData();
        data.add(map);

        PubSubmit p = new PubSubmit();
        if(!p.submitData(data, ""))
        {
            CError tError = new CError();
            tError.moduleName = "CrmPolicyChangeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "提交数据失败";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * getSubmitData
     *
     * @param cInputData VData：submitData中传入的VData对象
     * @param operate String：submitData中传入的String对象
     * @return boolean：true提交成功, false提交失败
     */
    private MMap getSubmitData(VData cInputData, String operate)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        LCContDB db = new LCContDB();
        for(int i = 1; i <= mLCContSet.size(); i++)
        {
            db.setContNo(mLCContSet.get(i).getContNo());
            if(!db.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "IntlDownloadCustCardBL";
                tError.functionName = "checkData";
                tError.errorMessage = "没有找到保单，不能进行客户卡下载："
                                      + db.getContNo();
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }
            mLCContSet.set(i, db.getSchema());
        }

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        String strSQL = "select max(int(printid)) + 1 from lccontprint ";
        String value = tExeSQL.getOneValue(strSQL);
        if("".equals(value) || "null".equals(value))
        {
            value = "1";
        }

        int nextPrintID = Integer.parseInt(value);

        FileWriter fw = null;
        BufferedWriter bw = null;
        try
        {
            //生成文本文件
            if("txt".equals(this.mSuffix))
            {
                File f = new File(mOutXmlPath);
                f.delete();
                fw = new FileWriter(mOutXmlPath, true);
                bw = new BufferedWriter(fw);

                for(int i = 1; i <= mLCContSet.size(); i++)
                {
                    SSRS tSSRS = getOnCont(mLCContSet.get(i).getContNo());
                    if(tSSRS == null)
                    {
                        return false;
                    }

                    //循环每个被保人
                    for(int row = 1; row <= tSSRS.getMaxRow(); row++)
                    {
                        String insuredNo = tSSRS.GetText(row, 2);
                        //循环每个字段
                        for(int col = 3; col <= tSSRS.getMaxCol(); col++)
                        {
                            bw.write(tSSRS.GetText(row, col)); //以tab分隔字段
                            if(col != tSSRS.getMaxCol())
                            {
                                bw.write("\t");
                            }
                        }
                        bw.write("\r\n"); //以回车换行分割被保人

                        map.put(dealOneInsuredPrint(mLCContSet.get(i), insuredNo,
                                                    nextPrintID),
                                SysConst.INSERT);
                        nextPrintID++;
                    }
                }

                bw.close();
                fw.close();
            }
            //生成excel
            else if("xls".equals(this.mSuffix))
            {
                String[][] mToExcel = {{"保单号", "被保人姓", "性别", "出生日期",
                                  "证件类型", "证件号码", "保险起期", "保险止期"}};
                for(int i = 1; i <= mLCContSet.size(); i++)
                {
                    SSRS tSSRS = getOnCont(mLCContSet.get(i).getContNo());
                    if(tSSRS == null)
                    {
                        return false;
                    }

                    int lengthOld = mToExcel.length;

                    //申请更多空间存储数据
                    String[][] temp = new String[lengthOld + tSSRS.getMaxRow()]
                                      [tSSRS.getMaxCol()];
                    System.arraycopy(mToExcel, 0, temp, 0, mToExcel.length);
                    mToExcel = temp;

                    //循环每个被保人
                    for(int row = 1; row <= tSSRS.getMaxRow(); row++)
                    {
                        String insuredNo = tSSRS.GetText(row, 2);
                        //循环每个字段
                        for(int col = 3; col <= tSSRS.getMaxCol(); col++)
                        {
                            mToExcel[lengthOld + row - 1][col - 3]
                                = tSSRS.GetText(row, col);
                        }

                        map.put(dealOneInsuredPrint(mLCContSet.get(i),
                            insuredNo,
                            nextPrintID),
                                SysConst.INSERT);
                        nextPrintID++;
                    }
                }

                WriteToExcel t = new WriteToExcel("");
                t.createExcelFile();
                String[] sheetName = {PubFun.getCurrentDate()};
                t.addSheet(sheetName);
                t.setData(0, mToExcel);
                t.write(mOutXmlPath);
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "IntlDownloadCustCardBL";
            tError.functionName = "dealData";
            tError.errorMessage = "无法进行文件读写";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }


        return true;
    }

    /**
     * 得到一个保单的客户卡信息
     * @return SSRS
     */
    private SSRS getOnCont(String cContNo)
    {
        ExeSQL tExeSQL = new ExeSQL();
        String sql =
            "select b.ManageCom, b.InsuredNo, a.ContNo, a.Name, a.Sex, a.Birthday, "
            + "   a.IDType, a.IDNo, min(b.CValiDate),max(b.EndDate) "
            + "from LCInsured a, LCPol b "
            + "where a.ContNo = b.ContNo "
            + "   and a.InsuredNo = b.InsuredNo "
            + "   and a.ContNo = '" + cContNo + "' "
            + "group by b.ManageCom, b.InsuredNo, a.ContNo, a.Name, "
            + "   a.Sex, a.Birthday, a.IDType, a.IDNo "
            + "order by a.ContNo, b.InsuredNo ";
        System.out.println(sql);

        SSRS tSSRS = tExeSQL.execSQL(sql);
        if(tExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "IntlDownloadCustCardBL";
            tError.functionName = "getOnCont";
            tError.errorMessage = "查询保单客户卡信息出错" + cContNo;
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        return tSSRS;
    }

    /**
     * 存储一个保单的客户卡下载信息
     * @param tLCContSchema LCContSchema
     * @return LCContPrintSchema
     */
    private LCContPrintSchema dealOneInsuredPrint(LCContSchema tLCContSchema,
        String insuredNo, int nextPrintID)
    {
        //生成下载信息
        map.put("delete from LCContPrint "
                + "where PrtNo = '" + tLCContSchema.getPrtNo() + "' "
                + "   and OtherNoType = '3' "
                + "   and OtherNo = '" + insuredNo + "' ",
                SysConst.DELETE);

        LCContPrintSchema tLCContPrintSchema = new LCContPrintSchema();
        tLCContPrintSchema.setPrintID(nextPrintID + "");
        tLCContPrintSchema.setPrtNo(tLCContSchema.getPrtNo());
        tLCContPrintSchema.setOtherNo(insuredNo);
        tLCContPrintSchema.setOtherNoType("3");  //客户卡，被保人号
        tLCContPrintSchema.setPrintCount(0);
        tLCContPrintSchema.setDownloadCount(1);
        tLCContPrintSchema.setFilePath(mOppositePath);
        tLCContPrintSchema.setCValiDate(tLCContSchema.getCValiDate());
        tLCContPrintSchema.setDownloadDate(mCurDate);
        tLCContPrintSchema.setDownloadTime(mCurTime);
        tLCContPrintSchema.setManageCom(mGI.ComCode);
        tLCContPrintSchema.setOperator(mGI.Operator);
        tLCContPrintSchema.setMakeDate(mCurDate);
        tLCContPrintSchema.setMakeTime(mCurTime);
        tLCContPrintSchema.setModifyDate(mCurDate);
        tLCContPrintSchema.setModifyTime(mCurTime);
        tLCContPrintSchema.setDownOperator(mGI.Operator);

        return tLCContPrintSchema;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data
              .getObjectByObjectName("GlobalInput", 0);
        mLCContSet = (LCContSet) data
                     .getObjectByObjectName("LCContSet", 0);
        TransferData tf = (TransferData) data
                     .getObjectByObjectName("TransferData", 0);

        if(mGI == null || mLCContSet == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "IntlDownloadCustCardBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        mOppositePath = (String) tf.getValueByName("OppositePath");
        mSuffix = (String) tf.getValueByName("Suffix");
        if(mOutXmlPath == null || "".equals(mOutXmlPath)
           || mOppositePath == null || "".equals(mOppositePath)
           || mSuffix == null || "".equals(mSuffix))
        {
            CError tError = new CError();
            tError.moduleName = "IntlDownloadCustCardBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "请传入文件生成路径";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "group";
        gi.ComCode = "86";

        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema.setContNo("000000013000004");
        LCContSet tLCContSet = new LCContSet();
        tLCContSet.add(tLCContSchema);

        LCContSchema tLCContSchema2 = new LCContSchema();
        tLCContSchema2.setContNo("000000055000001");
        tLCContSet.add(tLCContSchema2);

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("OutXmlPath", "D:\\picch\\ui\\printdata\\data\\a.xls");
        tTransferData.setNameAndValue("Suffix", "xls");

        VData vData = new VData();
        vData.add(gi);
        vData.add(tLCContSet);
        vData.add(tTransferData);

        IntlDownloadCustCardBL tIntlDownloadCustCardBL = new IntlDownloadCustCardBL();
        if (!tIntlDownloadCustCardBL.submitData(vData, ""))
        {
            System.out.println(tIntlDownloadCustCardBL.mErrors.getErrContent());
        }


        IntlDownloadCustCardBL intldownloadcustcardbl = new
            IntlDownloadCustCardBL();
    }
}
