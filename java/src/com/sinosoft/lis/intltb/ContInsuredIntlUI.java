package com.sinosoft.lis.intltb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 处理被保人信息。
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class ContInsuredIntlUI
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    public ContInsuredIntlUI()
    {
    }

    /**
     * 相关说明见ContInsuredIntlBL.summitData
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        ContInsuredIntlBL bl = new ContInsuredIntlBL();
        if(!bl.submitData(cInputData, cOperate))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }


    public static void main(String[] args)
    {
        ContInsuredIntlUI continsuredintlui = new ContInsuredIntlUI();
    }
}
