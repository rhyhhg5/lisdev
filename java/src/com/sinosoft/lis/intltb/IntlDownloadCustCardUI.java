package com.sinosoft.lis.intltb;

import com.sinosoft.utility.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 客户卡信息下载业务接口
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class IntlDownloadCustCardUI
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    public IntlDownloadCustCardUI()
    {
    }

    /**
     * 接收页面传入的保单号， 调用IntlDownloadCustCardBL.java生成客户卡信息。
     * @param cInputData VData：包含：
     A.	GlobalInput对象，完整的登陆用户信息。
     B.	LCContSet mLCContSet：保单信息
     * @param cOperator String：操作方式，可为“”
     * @return boolean：成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperator)
    {
        IntlDownloadCustCardBL bl = new IntlDownloadCustCardBL();
        if(!bl.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        IntlDownloadCustCardUI intldownloadcustcardui = new
            IntlDownloadCustCardUI();
    }
}
