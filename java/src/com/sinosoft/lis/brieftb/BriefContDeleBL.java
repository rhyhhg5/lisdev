package com.sinosoft.lis.brieftb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LCContSchema;

/**
 *
 * <p>Title: 简易投保删除操作</p>
 *
 * <p>Description: 如果在合同维护中,进行了需要重打保单的操作则把保单移到备份表 </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author shaoax
 * @version 1.0
 */
public class BriefContDeleBL {
    public BriefContDeleBL() {
    }

    /**全局数据变量*/
    public CErrors mErrors = new CErrors();

    /** 传入数据的容器 */
    private VData mInputData = new VData();
    
    private TransferData mTransferData = new TransferData();

    /**操作字符*/
    private String mOperate = "";
    
    private String mPrtNo;
    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /**传输到后台处理的map*/
    private MMap mMap = new MMap();


    /**
     * UI接口
     * @param cInputData VData
     * @param mOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.mInputData = cInputData;
        this.mOperate = cOperate;

        if (!getInputData())
            return false;

        if (!checkData())
            return false;

        if (!dealData())
            return false;

        if (!prepareOutputData())
            return false;

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(this.mInputData, null)) {
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
        }
        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
    	mInputData.add(mMap);
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
      //保单处于投保和预打状态时,字段MissionProp1代表印刷号,签单状态时,字段MissionProp2代表印刷号
      String strSql = "select activityid from lwmission where (MissionProp1 = '"
            + mPrtNo
            + "' or MissionProp2 = '"
            + mPrtNo
            + "') and processid='0000000007'";
      //String tSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(strSql);
      if (tSSRS.getMaxRow() != 0)
      {
          for (int i = 1; i <= tSSRS.getMaxRow(); i++)
          {
              String activityid = tSSRS.GetText(i, 1);
              String tSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);
              if (activityid.equals("0000007001")
                      || activityid.equals("0000007003"))
              {
                  mMap.put("insert into LBmission (select '"
                                          + tSerielNo
                                          + "',lwmission.* from lwmission where MissionProp1 = '"
                                          + mPrtNo + "' and activityid='"
                                          + activityid + "')", "INSERT");
                  mMap.put("delete from lwmission where MissionProp1 = '"
                          + mPrtNo + "' and activityid='" + activityid + "'",
                          "DELETE");
              }
              if (activityid.equals("0000007002"))
              {
                  mMap.put("insert into LBmission (select '"
                                          + tSerielNo
                                          + "', lwmission.* from lwmission where MissionProp2 = '"
                                          + mPrtNo + "' and activityid='"
                                          + activityid + "')", "INSERT");
                  mMap.put("delete from lwmission where MissionProp2 = '"
                          + mPrtNo + "' and activityid='" + activityid + "'",
                          "DELETE");
              }
          }
      }
      return true;
    }
    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        if (StrTool.cTrim(mPrtNo).equals("")) {         
            CError tError = new CError();
            tError.moduleName = "BriefContDeleBL.java";
            tError.functionName = "checkData";
            tError.errorMessage = "传入的印刷号码为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
    	
    	mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
    	mPrtNo = (String) mTransferData.getValueByName("tPrtNo");
        return true;
    }

    /**
     * 操作结果
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }
}

