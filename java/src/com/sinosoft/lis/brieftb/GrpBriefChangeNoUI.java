package com.sinosoft.lis.brieftb;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author YangMing
 * @version 1.0
 */
public class GrpBriefChangeNoUI {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    public GrpBriefChangeNoUI() {
    }

    public boolean submitData(VData nInputData, String cOperate) {
        GrpBriefChangeNoBL tGrpBriefChangeNoBL = new GrpBriefChangeNoBL();
        if (!tGrpBriefChangeNoBL.submitData(nInputData, cOperate)) {
            this.mErrors.copyAllErrors(tGrpBriefChangeNoBL.mErrors);
            return false;
        } else {
            mResult = tGrpBriefChangeNoBL.getResult();
        }
        return true;
    }

    /**
     * �������
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }

}
