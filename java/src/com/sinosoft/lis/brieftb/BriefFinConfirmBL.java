package com.sinosoft.lis.brieftb;

import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.db.LJAPayGrpDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LJSPayBDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LJSPayGrpBDB;
import com.sinosoft.lis.db.LJSPayGrpDB;
import com.sinosoft.lis.db.LJSPayPersonBDB;
import com.sinosoft.lis.db.LJSPayPersonDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LJAPaySet;
import com.sinosoft.lis.vschema.LJSPayBSet;
import com.sinosoft.lis.vschema.LJSPayGrpBSet;
import com.sinosoft.lis.vschema.LJSPayGrpSet;
import com.sinosoft.lis.vschema.LJSPayPersonBSet;
import com.sinosoft.lis.vschema.LJSPayPersonSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class BriefFinConfirmBL {
    /** 报错对象 */
    public CErrors mErrors = new CErrors();
    /** 操作符 */
    private String mOperate;
    /** 传入要素 */
    private VData mInputData;
    /** 操作要素 */
    private TransferData mTransferData;
    /** 登陆信息 */
    private GlobalInput mGlobalInput;
    /** 批次号码 */
    private String BatchNo;
    /** 应收信息 */
    private LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet();
    private LJSPayGrpSet mLJSPayGrpSet = new LJSPayGrpSet();
    private LJSPaySet mLJSPaySet = new LJSPaySet();
    /** 应收备份 */
    private LJSPayPersonBSet mLJSPayPersonBSet = new LJSPayPersonBSet();
    private LJSPayGrpBSet mLJSPayGrpBSet = new LJSPayGrpBSet();
    private LJSPayBSet mLJSPayBSet = new LJSPayBSet();
    /** 实收 */
    private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();
    private LJAPayGrpSet mLJAPayGrpSet = new LJAPayGrpSet();
    private LJAPaySet mLJAPaySet = new LJAPaySet();
    /** 暂收表 */
    private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
    /** 递交信息 */
    private MMap map = new MMap();
    /** 结果集 */
    private VData mResult = new VData();
    public BriefFinConfirmBL() {
    }

    /**
     * submitData
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        mInputData = cInputData;
        mOperate = cOperate;

        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit ps = new PubSubmit();
        if (ps.submitData(this.mResult, null)) {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        map.put(mLJSPayPersonSet, "DELETE");
        map.put(mLJSPayGrpSet, "DELETE");
        map.put(mLJSPaySet, "DELETE");
        map.put(mLJSPayPersonBSet, "UPDATE");
        map.put(mLJSPayGrpBSet, "UPDATE");
        map.put(mLJSPayBSet, "UPDATE");
        map.put(mLJAPayPersonSet, "UPDATE");
        map.put(mLJAPayGrpSet, "UPDATE");
        map.put(mLJAPaySet, "UPDATE");
        map.put(mLJTempFeeSet, "UPDATE");
        this.mResult.add(map);
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        /** 开始进行财务核销 */
        System.out.println("暂收查询...");
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        tLJTempFeeDB.setTempFeeNo(this.BatchNo);
        this.mLJTempFeeSet = tLJTempFeeDB.query();
        if (mLJTempFeeSet == null || mLJTempFeeSet.size() <= 0) {
            buildError("dealData", "未查询到暂收信息！");
            return false;
        }
        System.out.println("暂收校验...");
        for (int i = 1; i <= mLJTempFeeSet.size(); i++) {
            if (mLJTempFeeSet.get(i).getEnterAccDate() == null) {
                buildError("dealData", "财务未到帐！");
                return false;
            }
            if (mLJTempFeeSet.get(i).getConfDate() != null) {
                buildError("dealData", "当前批次财务已核销！");
                return false;
            }
        }
        System.out.println("应收查询");
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setSerialNo(this.BatchNo);
        this.mLJSPaySet = tLJSPayDB.query();
        if (mLJSPaySet == null || mLJSPaySet.size() <= 0) {
            buildError("dealData", "根据批次号码查询应收数据失败！");
            return false;
        }
        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        tLJSPayBDB.setSerialNo(this.BatchNo);
        this.mLJSPayBSet = tLJSPayBDB.query();
        if (mLJSPayBSet == null || mLJSPayBSet.size() <= 0) {
            buildError("dealData", "根据批次号码查询应收备份数据失败！");
            return false;
        }
        if (mLJSPaySet.size() != mLJSPayBSet.size()) {
            buildError("dealData", "应收总表和应收总备份表数目不同！");
            return false;
        }
        LJSPayGrpDB tLJSPayGrpDB = new LJSPayGrpDB();
        tLJSPayGrpDB.setSerialNo(this.BatchNo);
        this.mLJSPayGrpSet = tLJSPayGrpDB.query();
        if (mLJSPayGrpSet == null || mLJSPayGrpSet.size() <= 0) {
            buildError("dealData", "根据批次号码查询集体应收数据失败！");
            return false;
        }
        LJSPayGrpBDB tLJSPayGrpBDB = new LJSPayGrpBDB();
        tLJSPayGrpBDB.setSerialNo(this.BatchNo);
        this.mLJSPayGrpBSet = tLJSPayGrpBDB.query();
        if (mLJSPayGrpBSet == null || mLJSPayGrpBSet.size() <= 0) {
            buildError("dealData", "根据批次号码查询集体应收备份数据失败！");
            return false;
        }
        if (mLJSPayGrpSet.size() != mLJSPayGrpBSet.size()) {
            buildError("dealData", "集体应收表和集体应收备份表数目不同！");
            return false;
        }

        LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
        tLJSPayPersonDB.setSerialNo(this.BatchNo);
        this.mLJSPayPersonSet = tLJSPayPersonDB.query();
        if (mLJSPayPersonSet == null || mLJSPayPersonSet.size() <= 0) {
            buildError("dealData", "根据批次号码查询个人应收数据失败！");
            return false;
        }

        LJSPayPersonBDB tLJSPayPersonBDB = new LJSPayPersonBDB();
        tLJSPayPersonBDB.setSerialNo(this.BatchNo);
        this.mLJSPayPersonBSet = tLJSPayPersonBDB.query();
        if (mLJSPayPersonBSet == null || mLJSPayPersonBSet.size() <= 0) {
            buildError("dealData", "根据批次号码查询个人应收备份数据失败！");
            return false;
        }
        if (mLJSPayPersonSet.size() != mLJSPayPersonBSet.size()) {
            buildError("dealData", "个人应收和个人应收备份表中数据不符！");
            return false;
        }
        //----------------------------------------------------------------//
        System.out.println("实收查旬....");
        LJAPayDB tLJAPayDB = new LJAPayDB();
        tLJAPayDB.setSerialNo(this.BatchNo);
        this.mLJAPaySet = tLJAPayDB.query();
        if (mLJAPaySet == null || mLJAPaySet.size() <= 0) {
            buildError("dealData", "根据批次号码查询实收总表数据失败！");
            return false;
        }
        if (mLJAPaySet.size() != mLJSPaySet.size()) {
            buildError("dealData", "实收总表和和应收总表数据不符！");
            return false;
        }

        LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
        tLJAPayGrpDB.setSerialNo(this.BatchNo);
        this.mLJAPayGrpSet = tLJAPayGrpDB.query();
        if (mLJAPayGrpSet == null || mLJAPayGrpSet.size() <= 0) {
            buildError("dealData", "根据批次号码查询集体实收表数据失败！");
            return false;
        }
        if (mLJAPayGrpSet.size() != mLJSPayGrpSet.size()) {
            buildError("dealData", "集体实收表和和集体应收表数据不符！");
            return false;
        }
        /** 校验财务暂收与实收是否对应 */
        if (!checkFeeValue()) {
            return false;
        }

        LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
        tLJAPayPersonDB.setSerialNo(this.BatchNo);
        this.mLJAPayPersonSet = tLJAPayPersonDB.query();
        if (mLJAPayPersonSet == null || mLJAPayPersonSet.size() <= 0) {
            buildError("dealData", "根据批次号码查询个人实收表数据失败！");
            return false;
        }
        if (mLJAPayPersonSet.size() != mLJSPayPersonSet.size()) {
            buildError("dealData", "个人实收表和和个人应收表数据不符！");
            return false;
        }

        for (int i = 1; i <= mLJSPayBSet.size(); i++) {
            mLJSPayBSet.get(i).setConfFlag("1");
            mLJSPayBSet.get(i).setDealState("1");
            mLJSPayBSet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLJSPayBSet.get(i).setModifyTime(PubFun.getCurrentTime());
            mLJSPayBSet.get(i).setOperator(mGlobalInput.Operator);
        }
        for (int i = 1; i <= mLJSPayGrpBSet.size(); i++) {
            mLJSPayGrpBSet.get(i).setConfFlag("1");
            mLJSPayGrpBSet.get(i).setDealState("1");
            mLJSPayGrpBSet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLJSPayGrpBSet.get(i).setModifyTime(PubFun.getCurrentTime());
            mLJSPayGrpBSet.get(i).setOperator(mGlobalInput.Operator);
        }
        for (int i = 1; i <= mLJSPayPersonBSet.size(); i++) {
            mLJSPayPersonBSet.get(i).setConfFlag("1");
            mLJSPayPersonBSet.get(i).setDealState("1");
            mLJSPayPersonBSet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLJSPayPersonBSet.get(i).setModifyTime(PubFun.getCurrentTime());
            mLJSPayPersonBSet.get(i).setOperator(mGlobalInput.Operator);
        }
        for (int i = 1; i <= mLJAPaySet.size(); i++) {
            mLJAPaySet.get(i).setConfDate(PubFun.getCurrentDate());
            mLJAPaySet.get(i).setEnterAccDate(this.mLJTempFeeSet.get(1).
                                              getEnterAccDate());
            mLJAPaySet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLJAPaySet.get(i).setModifyTime(PubFun.getCurrentTime());
            mLJAPaySet.get(i).setOperator(mGlobalInput.Operator);
        }
        for (int i = 1; i <= mLJAPayGrpSet.size(); i++) {
            mLJAPayGrpSet.get(i).setConfDate(PubFun.getCurrentDate());
            mLJAPayGrpSet.get(i).setEnterAccDate(this.mLJTempFeeSet.get(1).
                                                 getEnterAccDate());
            mLJAPayGrpSet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLJAPayGrpSet.get(i).setModifyTime(PubFun.getCurrentTime());
            mLJAPayGrpSet.get(i).setOperator(mGlobalInput.Operator);
        }
        for (int i = 1; i <= mLJAPayPersonSet.size(); i++) {
            mLJAPayPersonSet.get(i).setConfDate(PubFun.getCurrentDate());
            mLJAPayPersonSet.get(i).setEnterAccDate(this.mLJTempFeeSet.get(1).
                    getEnterAccDate());
            mLJAPayPersonSet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLJAPayPersonSet.get(i).setModifyTime(PubFun.getCurrentTime());
            mLJAPayPersonSet.get(i).setOperator(mGlobalInput.Operator);
        }
        for (int i = 1; i <= this.mLJTempFeeSet.size(); i++) {
            mLJTempFeeSet.get(i).setConfDate(PubFun.getCurrentDate());
            mLJTempFeeSet.get(i).setConfFlag("1");
        }
        return true;
    }

    /**
     * checkFeeValue
     *
     * @return boolean
     */
    private boolean checkFeeValue() {
        System.out.println("校验财务暂收和应收保费信息...");
        double sumPay = getSumPay();
        double sumDueMoney = getSumDueMoney();
        if (Math.abs(sumPay - sumDueMoney) > 0.001) {
            buildError("checkFeeValue", "暂收金额与应收金额不符！");
            return false;
        }
        return true;
    }

    /**
     * getSumDueMoney
     *
     * @return double
     */
    private double getSumDueMoney() {
        double sumDueMoney = 0.00;
        for (int i = 1; i <= this.mLJSPaySet.size(); i++) {
            sumDueMoney = sumDueMoney + mLJSPaySet.get(i).getSumDuePayMoney();
        }
        return sumDueMoney;
    }

    /**
     * getSumPay
     *
     * @return double
     */
    private double getSumPay() {
        double sumPay = 0.00;

        for (int i = 1; i <= this.mLJTempFeeSet.size(); i++) {
            sumPay = sumPay + this.mLJTempFeeSet.get(i).getPayMoney();
        }

        return sumPay;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        if (this.mTransferData == null) {
            buildError("checkData", " 传入参数为null！");
            return false;
        }
        BatchNo = (String) mTransferData.getValueByName("BatchNo");
        if (BatchNo == null || BatchNo.equals("")) {
            buildError("checkData", "传入批次号码为null！");
            return false;
        }

        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        System.out.println("解析数据......");
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        return true;

    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "BriefFinConfirmBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

    public static void main(String[] args) {
        String BatchNo = "81000002245";
        BriefFinConfirmBL t = new BriefFinConfirmBL();
        VData v = new VData();
        GlobalInput g = new GlobalInput();
        g.ManageCom = "86110000";
        g.ComCode = "86110000";
        g.Operator = "group";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("BatchNo", BatchNo);
        v.add(tTransferData);
        v.add(g);
        if (!t.submitData(v, null)) {
            System.out.println("报错信息:" + t.mErrors.getErrContent());
        }
    }
}
