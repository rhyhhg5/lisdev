package com.sinosoft.lis.brieftb;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class BriefInsuredModifyUI {
    public BriefInsuredModifyUI() {
    }

    /** �������� */
    public CErrors mErrors = new CErrors();

    public boolean submitData(VData cInputData, String cOperate) {
        BriefInsuredModifyBL tBriefInsuredModifyBL = new BriefInsuredModifyBL();
        if (!tBriefInsuredModifyBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tBriefInsuredModifyBL.mErrors);
            return false;
        }
        return true;
    }

}
