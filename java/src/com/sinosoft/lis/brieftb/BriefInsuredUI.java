package com.sinosoft.lis.brieftb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class BriefInsuredUI {
    public BriefInsuredUI() {
    }

    /** �������� */
    public CErrors mErrors = new CErrors();

    public boolean submitData(VData cInputData, String cOperate) {
        BriefInsuredBL BriefInsuredBL = new BriefInsuredBL();
        if (!BriefInsuredBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(BriefInsuredBL.mErrors);
            return false;
        }
        return true;
    }

}
