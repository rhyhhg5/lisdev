package com.sinosoft.lis.brieftb;

import java.util.Date;
import java.util.GregorianCalendar;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCGrpAddressDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LDGrpDB;
import com.sinosoft.lis.db.LDNationDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCCustomerImpartSchema;
import com.sinosoft.lis.schema.LCExtendSchema;
import com.sinosoft.lis.schema.LCGrpAddressSchema;
import com.sinosoft.lis.schema.LCGrpAppntSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LDGrpSchema;
import com.sinosoft.lis.tb.ExtendBL;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LCNationSet;
import com.sinosoft.lis.vschema.LDGrpSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LCContDB;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;

/**
 *
 * <p>Title: 简易投保数据处理过程 </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author YangMing
 * @version 1.0
 */
public class BriefGroupContInputBL
{
    public BriefGroupContInputBL()
    {
    }

    public static void main(String[] args)
    {
        BriefGroupContInputBL briefgroupcontinputbl = new BriefGroupContInputBL();
        String n = "INSERT||MAIN";
        System.out.println("n : " + n.substring(0, 6));
    }

    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /**操作字符*/
    private String mOperate = "";

    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /**传输到后台处理的map*/
    private MMap map = new MMap();

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    /**传入数据*/
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    private LCGrpAppntSchema mLCGrpAppntSchema = new LCGrpAppntSchema();

    private LDGrpSchema mLDGrpSchema = new LDGrpSchema();

    private LCGrpAddressSchema mLCGrpAddressSchema = new LCGrpAddressSchema();

    private GlobalInput mGlobalInput = new GlobalInput();

    private LCNationSet mLCNationSet = new LCNationSet();

    private LCCustomerImpartSchema mLCCustomerImpartSchema = new LCCustomerImpartSchema();

    private TransferData mTransferData = new TransferData();

    private String LoadFlag;

    /**全部号码信息**/
    private String mGrpContNo = "";

    private String mCustomerNo = "";

    private String mAddressNo = "";

    /**生成客户标记*/
    private boolean GrpIsNew = false;

    /**地址信息插入标记*/
    private boolean AddressIsNew = false;

    /**删除标志*/
    private boolean CanDelete = true;

    /** 添加日志对象 */
    private static Log log = LogFactory.getLog(BriefGroupContInputBL.class);
    
//  by gzh 20130408 对综合开拓数据处理
    private LCExtendSchema mLCExtendSchema = new LCExtendSchema();

    /**
     * UI接口
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        this.mInputData = cInputData;
        this.mOperate = cOperate;

        if (!getInputData())
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("开始数据递交");
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("mResult " + mResult.encode());
        if (!tPubSubmit.submitData(this.mResult, this.mOperate.substring(0,
                this.mOperate.indexOf("|"))))
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 获取前台数据
     * @return boolean
     */
    private boolean getInputData()
    {
        mLCGrpContSchema = (LCGrpContSchema) this.mInputData
                .getObjectByObjectName("LCGrpContSchema", 0);
        mLCGrpAppntSchema = (LCGrpAppntSchema) this.mInputData
                .getObjectByObjectName("LCGrpAppntSchema", 0);
        mLDGrpSchema = (LDGrpSchema) this.mInputData.getObjectByObjectName(
                "LDGrpSchema", 0);
        mLCGrpAddressSchema = (LCGrpAddressSchema) this.mInputData
                .getObjectByObjectName("LCGrpAddressSchema", 0);
        mLCGrpAddressSchema = (LCGrpAddressSchema) this.mInputData
                .getObjectByObjectName("LCGrpAddressSchema", 0);
        mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLCNationSet = (LCNationSet) mInputData.getObjectByObjectName(
                "LCNationSet", 0);
        mLCCustomerImpartSchema = (LCCustomerImpartSchema) mInputData
                .getObjectByObjectName("LCCustomerImpartSchema", 0);
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData != null)
        {
            LoadFlag = (String) this.mTransferData.getValueByName("LoadFlag");
        }
        //将所有号码初始化成为客户号
        this.mGrpContNo = mLCGrpContSchema.getGrpContNo() == null ? ""
                : mLCGrpContSchema.getGrpContNo();
        this.mCustomerNo = mLCGrpAppntSchema.getCustomerNo() == null ? ""
                : mLCGrpAppntSchema.getCustomerNo();
        this.mAddressNo = mLCGrpAddressSchema.getAddressNo() == null ? ""
                : mLCGrpAppntSchema.getAddressNo();
        mLCExtendSchema = (LCExtendSchema) mInputData.getObjectByObjectName("LCExtendSchema", 0);
        if(mLCExtendSchema == null ){
        	mLCExtendSchema = new LCExtendSchema();
        }
        return true;
    }

    /**
     * 一般性教研
     * @return boolean
     */
    private boolean checkData()
    {
        if (StrTool.cTrim(mLCGrpContSchema.getAgentCode()).equals(""))
        {
            System.out
                    .println("程序第192行出错，请检查BriefGroupContInputBL.java中的checkData方法！");
            CError tError = new CError();
            tError.moduleName = "BriefGroupContInputBL.java";
            tError.functionName = "checkData";
            tError.errorMessage = "代理录入错误！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (StrTool.cTrim(mLCGrpContSchema.getPrtNo()).equals(""))
        {
            System.out
                    .println("程序第202行出错，请检查BriefGroupContInputBL.java中的checkData方法！");
            CError tError = new CError();
            tError.moduleName = "BriefGroupContInputBL.java";
            tError.functionName = "checkData";
            tError.errorMessage = "印刷号录入错误！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (this.mOperate.equals("INSERT||GROUPPOL"))
        {
            LCGrpContDB aLCGrpContDB = new LCGrpContDB();
            LCGrpContSet aLCGrpContSet = new LCGrpContSet();
            aLCGrpContDB.setPrtNo(mLCGrpContSchema.getPrtNo());
            aLCGrpContSet = aLCGrpContDB.query();
            if (aLCGrpContSet.size() > 0)
            {
                System.out
                        .println("程序第216行出错，请检查BriefGroupContInputBL.java中的checkData方法！");
                CError tError = new CError();
                tError.moduleName = "BriefGroupContInputBL.java";
                tError.functionName = "checkData";
                tError.errorMessage = "该保单印刷号已经存在！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(mLCGrpContSchema.getAgentCode());
        if (!tLAAgentDB.getInfo())
        {
            System.out
                    .println("程序第108行出错，请检查BriefGroupContInputBL.java中的checkData方法！");
            CError tError = new CError();
            tError.moduleName = "BriefGroupContInputBL.java";
            tError.functionName = "checkData";
            tError.errorMessage = "录入的代理人代码不存在！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLAAgentDB.getManageCom() == null
                || this.mLCGrpContSchema.getManageCom() == null)
        {
            System.out
                    .println("程序第117行出错，请检查BriefGroupContInputBL.java中的checkData方法！");
            CError tError = new CError();
            tError.moduleName = "BriefGroupContInputBL.java";
            tError.functionName = "checkData";
            tError.errorMessage = "管理机构为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!tLAAgentDB.getManageCom().equals(
                this.mLCGrpContSchema.getManageCom()))
        {
            System.out
                    .println("程序第126行出错，请检查BriefGroupContInputBL.java中的checkData方法！");
            CError tError = new CError();
            tError.moduleName = "BriefGroupContInputBL.java";
            tError.functionName = "checkData";
            tError.errorMessage = "管理机构不对应！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!this.mCustomerNo.equals(""))
        {
            LDGrpDB tLDGrpDB = new LDGrpDB();
            tLDGrpDB.setCustomerNo(this.mCustomerNo);
            if (!tLDGrpDB.getInfo())
            {
                System.out
                        .println("程序第174行出错，请检查BriefGroupContInputBL.java中的checkData方法！");
                CError tError = new CError();
                tError.moduleName = "BriefGroupContInputBL.java";
                tError.functionName = "checkData";
                tError.errorMessage = "录入团体客户号码,但系统未找到团体客户信息！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        if (!this.mGrpContNo.equals(""))
        {
            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            tLCGrpContDB.setGrpContNo(this.mGrpContNo);
            if (!tLCGrpContDB.getInfo())
            {
                System.out
                        .println("程序第189行出错，请检查BriefGroupContInputBL.java中的checkData方法！");
                CError tError = new CError();
                tError.moduleName = "BriefGroupContInputBL.java";
                tError.functionName = "checkData";
                tError.errorMessage = "传入一个团体保单号,但找到该保单号对应的数据！";
                this.mErrors.addOneError(tError);
                return false;
            }
            else if (this.mOperate.equals("INSERT||GROUPPOL"))
            {
                String str = "此保单已保存,点击修改!";
                buildError("checkData", str);
                log.debug(str);
                return false;
            }
        }
        //校验地址代码是否应该重新生成
        if (!StrTool.cTrim(this.mLCGrpAddressSchema.getAddressNo()).equals(""))
        {
            LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
            tLCGrpAddressDB.setSchema(mLCGrpAddressSchema);
            if (tLCGrpAddressDB.query().size() <= 0)
            {
                AddressIsNew = true;
            }
        }
        //校验客户号码是否应该重新生成
        if (!StrTool.cTrim(this.mLDGrpSchema.getCustomerNo()).equals(""))
        {
            LDGrpDB tLDGrpDB = new LDGrpDB();
            tLDGrpDB.setSchema(mLDGrpSchema);
            LDGrpSet tLDGrpSet = tLDGrpDB.query();
            if (tLDGrpSet.size() <= 0)
            {
                GrpIsNew = true;
                AddressIsNew = true;
            }
            else
            {
                this.mLDGrpSchema.setGrpAppntNum(tLDGrpSet.get(1)
                        .getGrpAppntNum());
            }
        }

        if (mLCNationSet == null || mLCNationSet.size() <= 0)
        {
            // 对于紧急救援团单，校验抵达国家。
            if ("1".equals(mLCGrpContSchema.getCardFlag()))
            {
                buildError("checkData", "没有录入抵达国家！");
                return false;
            }
            else if (mLCNationSet == null)
            {
                mLCNationSet = new LCNationSet();
            }
            // -------------------------------
        }

        return true;
    }

    /**
     * 业务逻辑处理
     * @return boolean
     */
    private boolean dealData()
    {
        if (mOperate.equals("DELETE||GROUPPOL"))
        {
            return deleteData();
        }
        else
        {
            return insertData();
        }
    }

    /**
     * 删除操作
     * @return boolean
     */
    private boolean deleteData()
    {
        //做删除操作需要校验是否有险种信息，如果有险种信息就不能删除
        String checkRiskIsExist = "select count(1) from lcgrppol where grpcontno='"
                + this.mGrpContNo + "'";
        ExeSQL tExeSQL = new ExeSQL();
        if (Integer.parseInt(tExeSQL.getOneValue(checkRiskIsExist)) > 0)
        {
            CanDelete = false;
        }
        if (CanDelete)
        {
            map.put("delete from lcgrpcont where grpcontno='" + mGrpContNo
                    + "'", "DELETE");
            map.put("delete from lcgrpappnt where grpcontno='" + mGrpContNo
                    + "'", "DELETE");
            map
                    .put("delete from lcnation where grpcontno='" + mGrpContNo
                            + "'", "DELETE");
        }
        else
        {
            System.out
                    .println("程序第271行出错，请检查BriefGroupContInputBL.java中的deleteData方法！");
            CError tError = new CError();
            tError.moduleName = "BriefGroupContInputBL.java";
            tError.functionName = "deleteData";
            tError.errorMessage = "合同信息存在险种信息，请先删除险种信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 插入操作
     * @return boolean
     */
    private boolean insertData()
    {
        //在合同维护中需要判断是否需要将数据存储到备份表中,然后重新生成一个新的保单
        if (StrTool.cTrim(this.LoadFlag).equals("2"))
        {
            BriefGroupContDeleteBL tBriefGroupContDeleteBL = new BriefGroupContDeleteBL();
            if (!tBriefGroupContDeleteBL.submitData(this.mInputData,
                    "DELETE||CONT"))
            {
                this.mErrors.copyAllErrors(tBriefGroupContDeleteBL.mErrors);
                return false;
            }
            VData delData = tBriefGroupContDeleteBL.getResult();
            if (delData.size() <= 0)
            {
                System.out
                        .println("程序第304行出错，请检查BriefGroupContInputBL.java中的insertData方法！");
                CError tError = new CError();
                tError.moduleName = "BriefGroupContInputBL.java";
                tError.functionName = "insertData";
                tError.errorMessage = "重新生成保单失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.map.add((MMap) delData.getObjectByObjectName("MMap", 0));
        }
        //统一处理全部号码,合同号,客户号,地址号码
        if (!dealAllTheNo())
        {
            return false;
        }
        System.out.println("生成号码处理完成 　！ ");
        //处理团体合同
        if (!dealGrpCont())
        {
            return false;
        }
        System.out.println("团体和同信息处理完成 ！");
        //处理团体客户信息
        if (!dealLDGrp())
        {
            return false;
        }
        System.out.println("团体客户信息已生成 ！ ");
        //处理地址信息
        if (!dealLCGrpAddress())
        {
            return false;
        }
        //处理ＬＣＧｒｐＡｐｐｎｔ
        if (!dealLCGrpAppnt())
        {
            return false;
        }
        //处理抵达国家
        if (!dealLCNation())
        {
            return false;
        }
        System.out.println("团体地址信息已生成　！");
        //客户告知
        if (!dealLCCustomerImpart())
        {
            return false;
        }
        /*要更新所有的LCCont,LCInsured,LCPol,LCGrppol
         的ManageCom,SaleChnl，AgentCom，PayMode，AgentCode，AgentGroup,AgentCode,Agntno,AgentName,Cvalidate,Cinvalidate
         ,Remark
         */
        LCGrpContDB tLCGrpcontDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        tLCGrpcontDB.setGrpContNo(this.mLCGrpContSchema.getGrpContNo());
        tLCGrpContSet = tLCGrpcontDB.query();
        if (tLCGrpContSet.size() > 0)
        {
            if (!Update(tLCGrpContSet))
            {
                return false;
            }
        }
        //如果修改生效日期,险种的相关日期都需要修改,此处用于合同维护
        if (!dealCvailDate())
        {
            return false;
        }
        
//      by gzh 20130408 对综合开拓数据处理
        if (!"".equals(StrTool.cTrim(mLCExtendSchema.getAssistSalechnl())))
        {
            MMap tTmpMap = null;
            tTmpMap = delExtendInfo(mLCExtendSchema);
            if (tTmpMap == null)
            {
                return false;
            }
            map.add(tTmpMap);
        }else{
        	String tSql = "delete from LCExtend where prtno = '"+mLCExtendSchema.getPrtNo()+"'";
        	map.put(tSql, SysConst.DELETE);
        }
        return true;
    }

    /**
     * 主要要处理LCGrpPol LCPol LCGet LCDuty
     * 中的生效失效日期
     *
     * @return boolean
     */
    private boolean dealCvailDate()
    {
        System.out.println("AppFlag " + mLCGrpContSchema.getAppFlag());
        if (StrTool.cTrim(mLCGrpContSchema.getAppFlag()).equals("1")
                || StrTool.cTrim(mLCGrpContSchema.getAppFlag()).equals("9"))
        {
            //首先校验生效日期是否发生改变
            LCGrpContDB tLCGrpcontDB = new LCGrpContDB();
            LCGrpContSet tLCGrpContSet = new LCGrpContSet();
            tLCGrpcontDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            tLCGrpContSet = tLCGrpcontDB.query();
            System.out.println("baba" + tLCGrpContSet.get(1).getCValiDate());
            System.out.println("baba1" + mLCGrpContSchema.getCValiDate());
            if (StrTool.cTrim(tLCGrpContSet.get(1).getCValiDate()).equals(
                    StrTool.cTrim(mLCGrpContSchema.getCValiDate())))
            {
                //没有发生变化
                System.out.println("dsddddddddddddddddd");
            }
            else
            {
                System.out.println("ffffffffffffffffffff");
                //发生变化,需要更新生效日期,失效日期
                String CVailDate = this.mLCGrpContSchema.getCValiDate();
                String CInVailDate = this.mLCGrpContSchema.getCInValiDate();
                GregorianCalendar tInValiDate = new GregorianCalendar();
                Date tdate = (new FDate()).getDate(CInVailDate);
                tInValiDate.setTime(tdate);
                tInValiDate.add(tInValiDate.DATE, 1);
                StringBuffer sql = new StringBuffer();
                sql.append("update LCGrpPol set CValiDate='");
                sql.append(CVailDate);
                sql.append("' where grpcontno='");
                sql.append(this.mGrpContNo);
                sql.append("'");
                this.map.put(sql.toString(), "UPDATE");

                System.out.println("株株" + CVailDate);
                sql = new StringBuffer();
                sql.append("update LCPol set CValiDate='");
                sql.append(CVailDate);
                sql.append("',Enddate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("',getstartdate='");
                sql.append(CVailDate);
                sql.append("',PayToDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("',PayEndDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("' where grpcontno='");
                sql.append(this.mGrpContNo);
                sql.append("'");
                this.map.put(sql.toString(), "UPDATE");

                sql = new StringBuffer();
                sql.append("update LCCont set CValiDate='");
                sql.append(CVailDate);
                sql.append("',CInValiDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("' where grpcontno='");
                sql.append(this.mGrpContNo);
                sql.append("'");
                this.map.put(sql.toString(), "UPDATE");

                sql = new StringBuffer();
                sql.append("update LCGet set GetStartDate='");
                sql.append(CVailDate);
                sql.append("',GetEndDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("',GetToDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("' where grpcontno='");
                sql.append(this.mGrpContNo);
                sql.append("'");
                this.map.put(sql.toString(), "UPDATE");

                sql = new StringBuffer();
                sql.append("update LCprem set ");
                sql.append("PayToDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("',PayEndDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("' where grpcontno='");
                sql.append(this.mGrpContNo);
                sql.append("'");
                this.map.put(sql.toString(), "UPDATE");

                sql = new StringBuffer();
                sql.append("update LCDuty set PayToDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("',PayEndDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("',EndDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("',getstartdate='");
                sql.append(CVailDate);
                sql.append("' where contno in (select contno from lccont where grpcontno='");
                sql.append(this.mGrpContNo);
                sql.append("')");
                this.map.put(sql.toString(), "UPDATE");

                sql = new StringBuffer();
                sql.append("update LCGrpPol set PayEndDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("',PayToDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("' where grpcontno='");
                sql.append(this.mGrpContNo);
                sql.append("' and appflag='1'");
                this.map.put(sql.toString(), "UPDATE");

                sql = new StringBuffer();
                sql.append("update ljapayperson set CurPaytoDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("',lastPayToDate='");
                sql.append(CVailDate);
                sql.append("' where grpcontno='");
                sql.append(this.mGrpContNo);
                sql.append("'");
                this.map.put(sql.toString(), "UPDATE");

                sql = new StringBuffer();
                sql.append("update ljapaygrp set CurPaytoDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("',lastPayToDate='");
                sql.append(CVailDate);
                sql.append("' where grpcontno='");
                sql.append(this.mGrpContNo);
                sql.append("'");
                this.map.put(sql.toString(), "UPDATE");

                sql = new StringBuffer();
                sql.append("update lacommision set CurPaytoDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("',lastPayToDate='");
                sql.append(CVailDate);
                sql.append("',CValiDate='");
                sql.append(CVailDate);
                sql.append("' where grpcontno='");
                sql.append(this.mGrpContNo);
                sql.append("'");
                this.map.put(sql.toString(), "UPDATE");

            }
        }
        return true;
    }

    private boolean prepareOutputData()
    {
        this.mResult.clear();
        this.mResult.add(map);
        this.mResult.add(this.mLCGrpAddressSchema);
        this.mResult.add(this.mLCGrpAppntSchema);
        this.mResult.add(this.mLCGrpContSchema);
        this.mResult.add(this.mLDGrpSchema);
        return true;
    }

    /**
     * 操作结果
     * @return VData
     */
    public VData getResult()
    {
        return mResult;
    }

    /**
     * 处理合同号,地址号,客户号等号码信息信息
     * @return boolean
     */
    private boolean dealAllTheNo()
    {
        if (this.mGrpContNo.equals(""))
        {
            String CreatGrpContNo = PubFun1.CreateMaxNo("PROGRPCONTNO", null);
            if (!StrTool.cTrim(CreatGrpContNo).equals(""))
            {
                mGrpContNo = CreatGrpContNo;
                System.out.println("生成一个新的投保单号 : " + this.mGrpContNo);
            }
            else
            {
                System.out
                        .println("程序第360行出错，请检查BriefGroupContInputBL.java中的dealAllTheNo方法！");
                CError tError = new CError();
                tError.moduleName = "BriefGroupContInputBL.java";
                tError.functionName = "dealAllTheNo";
                tError.errorMessage = "生成团体合同号失败,原因可能是系统负担过重,请稍候！";
                this.mErrors.addOneError(tError);
                return false;
            }
            //号码已生成将全部需要团体合同号的表全部填充
            this.mLCGrpContSchema.setGrpContNo(this.mGrpContNo);
            this.mLCGrpContSchema.setProposalGrpContNo(this.mGrpContNo);
            this.mLCGrpAppntSchema.setGrpContNo(this.mGrpContNo);

            if (this.mLCCustomerImpartSchema != null)
            {
                this.mLCCustomerImpartSchema.setGrpContNo(this.mGrpContNo);
            }

            for (int i = 1; i <= mLCNationSet.size(); i++)
            {
                mLCNationSet.get(i).setGrpContNo(mGrpContNo);
            }

        }
        if (this.mCustomerNo.equals("") || this.GrpIsNew)
        {
            String CustomerNo = "";
            CustomerNo = PubFun1.CreateMaxNo("GRPNO", null);
            if (!StrTool.cTrim(CustomerNo).equals(""))
            {
                this.mCustomerNo = CustomerNo;
                this.GrpIsNew = true;
            }
            else
            {
                System.out
                        .println("程序第192行出错，请检查BriefGroupContInputBL.java中的insertData方法！");
                CError tError = new CError();
                tError.moduleName = "BriefGroupContInputBL.java";
                tError.functionName = "insertData";
                tError.errorMessage = "生成号错误！";
                this.mErrors.addOneError(tError);
                return false;
            }
            //将需要客户号的地方填充
            this.mLCGrpContSchema.setAppntNo(this.mCustomerNo);
            this.mLCGrpAppntSchema.setCustomerNo(this.mCustomerNo);
            this.mLCGrpAddressSchema.setCustomerNo(this.mCustomerNo);
            this.mLDGrpSchema.setCustomerNo(this.mCustomerNo);

            if (this.mLCCustomerImpartSchema != null)
            {
                this.mLCCustomerImpartSchema.setCustomerNo(this.mCustomerNo);
            }
        }
        if (StrTool.cTrim(this.mAddressNo).equals("") || AddressIsNew)
        {
            String sql = "Select Case When char(max(integer(AddressNo))) Is Null Then '0' Else char(max(integer(AddressNo))) End from LCGrpAddress where CustomerNo='"
                    + mLCGrpAppntSchema.getCustomerNo() + "'";
            ExeSQL tExeSQL = new ExeSQL();
            String AddressNo = String.valueOf(Integer.parseInt(tExeSQL
                    .getOneValue(sql)) + 1);
            if (!StrTool.cTrim(AddressNo).equals(""))
            {
                this.mAddressNo = AddressNo;
                this.AddressIsNew = true;
            }
            else
            {
                System.out
                        .println("程序第232行出错，请检查BriefGroupContInputBL.java中的insertData方法！");
                CError tError = new CError();
                tError.moduleName = "BriefGroupContInputBL.java";
                tError.functionName = "insertData";
                tError.errorMessage = "生成地址编码错误！";
                this.mErrors.addOneError(tError);
            }
            //填充需要填充地址码的表
            this.mLCGrpContSchema.setAddressNo(this.mAddressNo);
            this.mLCGrpAppntSchema.setAddressNo(this.mAddressNo);
            this.mLCGrpAddressSchema.setAddressNo(this.mAddressNo);
            for (int i = 1; i <= this.mLCNationSet.size(); i++)
            {
                this.mLCNationSet.get(i).setAddressNo(this.mAddressNo);
            }
        }

        return true;
    }

    /**
     * 先处理团体保单信息
     * 主要是标志保单各种
     * 标记，投保人相关信
     * 息
     * @return boolean
     */
    private boolean dealGrpCont()
    {
        //将投保信息纪录下来
        this.mLCGrpContSchema
                .setAppntNo(this.mLCGrpAppntSchema.getCustomerNo()); //投保单位名称
        this.mLCGrpContSchema.setGrpName(this.mLCGrpAppntSchema.getName()); //客户名称
        this.mLCGrpContSchema.setFax(this.mLCGrpAddressSchema.getFax1()); //传真,取出联系人的传真
        this.mLCGrpContSchema.setEMail(this.mLCGrpAddressSchema.getE_Mail1());
        //        if (mLCGrpContSchema.getUWFlag() == null ||
        //            mLCGrpContSchema.getUWFlag().equals("")) {
        //            this.mLCGrpContSchema.setUWFlag("0");
        //        }
        //联系人的E-mail
        //this.mLCGrpContSchema.setUWFlag("0"); //人工核保标志
        //        if (mLCGrpContSchema.getApproveFlag() == null ||
        //            mLCGrpContSchema.getApproveFlag().equals("")) {
        //            this.mLCGrpContSchema.setApproveFlag("0"); //投保单状态
        //        }
        this.mLCGrpContSchema.setSpecFlag("0"); //特约标志
        if (mLCGrpContSchema.getAppFlag() == null
                || mLCGrpContSchema.getAppFlag().equals(""))
        {
            this.mLCGrpContSchema.setAppFlag("0"); //投保单状态
            this.mLCGrpContSchema.setStateFlag("0"); //投保单状态
        }
        this.mLCGrpContSchema.setInputDate(PubFun.getCurrentDate());
        this.mLCGrpContSchema.setInputTime(PubFun.getCurrentTime());
        this.mLCGrpContSchema.setInputOperator(this.mGlobalInput.Operator);
        PubFun.fillDefaultField(this.mLCGrpContSchema); //默认时间字段
        mLCGrpContSchema.setOperator(this.mGlobalInput.Operator);
        this.mLCGrpContSchema.setUWFlag("9"); //正常通过
        this.mLCGrpContSchema.setUWOperator(mGlobalInput.Operator);
        this.mLCGrpContSchema.setUWTime(PubFun.getCurrentTime());
        this.mLCGrpContSchema.setUWDate(PubFun.getCurrentDate());
        this.mLCGrpContSchema.setPolApplyDate(PubFun.getCurrentDate());
        this.mLCGrpContSchema.setApproveDate(PubFun.getCurrentDate());
        this.mLCGrpContSchema.setApproveTime(PubFun.getCurrentTime());
        this.mLCGrpContSchema.setApproveFlag("9");
        /** 校验是否修改了生效日期 */
        if (!StrTool.cTrim(this.LoadFlag).equals("2"))
        {
            LCGrpContDB tLCGrpContDB = new LCGrpContDB();
            if (mLCGrpContSchema.getGrpContNo() != null
                    && !mLCGrpContSchema.getGrpContNo().equals(""))
            {
                tLCGrpContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
                if (tLCGrpContDB.getInfo())
                {
                    String inputCValiDate = StrTool.cTrim(mLCGrpContSchema
                            .getCValiDate());
                    String inputCInValiDate = StrTool.cTrim(mLCGrpContSchema
                            .getCInValiDate());
                    if (!inputCValiDate.equals(tLCGrpContDB.getCValiDate())
                            || !inputCInValiDate.equals(tLCGrpContDB
                                    .getCInValiDate()))
                    {
                        LCContDB tLCContDB = new LCContDB();
                        tLCContDB.setGrpContNo(tLCGrpContDB.getGrpContNo());
                        if (tLCContDB.getCount() > 0)
                        {
                            buildError("dealGrpCont",
                                    "保险期间发生变化,被保人已导入,目前暂时不支持被保人导入后进行保障期间的修改！");
                            return false;
                        }
                    }
                }
            }
        }
        this.map.put(this.mLCGrpContSchema, this.mOperate.substring(0, 6));
        if (!StrTool.cTrim(mGrpContNo).equals("")
                && StrTool.cTrim(this.LoadFlag).equals("2"))
        {
            this.map.put("update lacommision set appntno='"
                    + this.mLCGrpAppntSchema.getCustomerNo() + "',P11='"
                    + this.mLCGrpAppntSchema.getName() + "' where grpcontno='"
                    + this.mGrpContNo + "'", "UPDATE");
        }
        return true;
    }

    /**
     * 处理团体客户信息, 主要问题在于
     * 使否要生成新的团体客户信息
     * @return boolean
     */
    private boolean dealLDGrp()
    {
        //如果是客户信息是新建客户则插入数据库
        if (this.GrpIsNew)
        {
            //记录入机日期
            PubFun.fillDefaultField(mLDGrpSchema);
            mLDGrpSchema.setOperator(this.mGlobalInput.Operator);
            map.put(mLDGrpSchema, "INSERT");
        }
        else
        {
            PubFun.fillDefaultField(mLDGrpSchema);
            mLDGrpSchema.setOperator(this.mGlobalInput.Operator);
            map.put(mLDGrpSchema, "UPDATE");
        }
        return true;
    }

    /**
     * 处理投地址信息
     * @return boolean
     */
    private boolean dealLCGrpAddress()
    {
        if (this.AddressIsNew)
        {
            //记录入机时间
            PubFun.fillDefaultField(mLCGrpAddressSchema);
            mLCGrpAddressSchema.setOperator(mGlobalInput.Operator);
            map.put(mLCGrpAddressSchema, "INSERT");
        }
        else
        {
            PubFun.fillDefaultField(mLCGrpAddressSchema);
            mLCGrpAddressSchema.setOperator(mGlobalInput.Operator);
            map.put(mLCGrpAddressSchema, "UPDATE");
        }
        return true;
    }

    /**
     * 处理投保人信息表
     * @return boolean
     */
    private boolean dealLCGrpAppnt()
    {
        PubFun.fillDefaultField(this.mLCGrpAppntSchema);
        mLCGrpAppntSchema.setOperator(this.mGlobalInput.Operator);
        this.map.put(this.mLCGrpAppntSchema, this.mOperate.substring(0, 6));
        return true;
    }

    /**
     * 检查两个字段的字符串是否相同
     * @param str1 String
     * @param str2 String
     * @return boolean
     */
    private boolean checkField(String str1, String str2)
    {
        return StrTool.cTrim(str1).equals(StrTool.cTrim(str2));
    }

    /**
     * 处理抵达国家
     * @return boolean
     */
    private boolean dealLCNation()
    {
        if (mLCNationSet != null)
        {
            PubFun.fillDefaultField(this.mLCNationSet);
            for (int i = 1; i <= this.mLCNationSet.size(); i++)
            {
                this.mLCNationSet.get(i).setOperator(mGlobalInput.Operator);
                LDNationDB tLDNationDB = new LDNationDB();
                tLDNationDB.setNationNo(mLCNationSet.get(i).getNationNo());
                tLDNationDB.getInfo();
                mLCNationSet.get(i)
                        .setEnglishName(tLDNationDB.getEnglishName());
            }
            if (mLCNationSet.size() > 0)
            {
                map.put("delete from lcnation where grpcontno='"
                        + this.mLCNationSet.get(1).getGrpContNo() + "'",
                        "DELETE");
                map.put(mLCNationSet, "INSERT");
            }
        }
        return true;
    }

    private boolean dealLCCustomerImpart()
    {
        if (this.mLCCustomerImpartSchema != null)
        {
            this.mLCCustomerImpartSchema.setOperator(mGlobalInput.Operator);
            PubFun.fillDefaultField(this.mLCCustomerImpartSchema);
            map.put(this.mLCCustomerImpartSchema, "DELETE&INSERT");
        }
        return true;
    }

    private boolean Update(LCGrpContSet tLCGrpContSet)
    {

        String theCurrentDate = PubFun.getCurrentDate();
        String theCurrentTime = PubFun.getCurrentTime();
        String wherepart = "GrpContno='" + this.mLCGrpContSchema.getGrpContNo()
                + "'";
        if (!(StrTool.compareString(tLCGrpContSet.get(1).getCValiDate(),
                mLCGrpContSchema.getCValiDate())))
        {
            String updatelcpolsql = "update lcpol set Cvalidate='"
                    + mLCGrpContSchema.getCValiDate() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelcpolsql, "UPDATE");
            String updatelccontsql = "update lccont set Cvalidate='"
                    + mLCGrpContSchema.getCValiDate() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelccontsql, "UPDATE");
            String updatelcgrppolsql = "update lcgrppol set Cvalidate='"
                    + mLCGrpContSchema.getCValiDate() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelcgrppolsql, "UPDATE");
        }
        if (!(StrTool.compareString(tLCGrpContSet.get(1).getCInValiDate(),
                mLCGrpContSchema.getCInValiDate())))
        {
            String updatelccontsql = "update lccont set Cinvalidate=date('"
                    + mLCGrpContSchema.getCInValiDate() + "')+1 days," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelccontsql, "UPDATE");
        }

        if (!(StrTool.compareString(tLCGrpContSet.get(1).getAppntNo(),
                mLCGrpContSchema.getAppntNo())))
        {
            String updatelcpolsql = "update lcpol set AppntNo='"
                    + mLCGrpContSchema.getAppntNo() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelcpolsql, "UPDATE");
            String updatelccontsql = "update lccont set AppntNo='"
                    + mLCGrpContSchema.getAppntNo() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelccontsql, "UPDATE");
            String updatelcgrppolsql = "update lcgrppol set customerNo='"
                    + mLCGrpContSchema.getAppntNo() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelcgrppolsql, "UPDATE");
            String updatelcinsurdsql = "update lcinsured set AppntNo='"
                    + mLCGrpContSchema.getAppntNo() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelcinsurdsql, "UPDATE");
        }
        if (!(StrTool.compareString(tLCGrpContSet.get(1).getGrpName(),
                mLCGrpContSchema.getGrpName())))
        {
            String updatelcpolsql = "update lcpol set AppntName='"
                    + mLCGrpContSchema.getGrpName() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelcpolsql, "UPDATE");
            String updatelccontsql = "update lccont set AppntName='"
                    + mLCGrpContSchema.getGrpName() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelccontsql, "UPDATE");
            String updatelcgrppolsql = "update lcgrppol set Grpname='"
                    + mLCGrpContSchema.getGrpName() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelcgrppolsql, "UPDATE");
        }
        if (!(StrTool.compareString(tLCGrpContSet.get(1).getPolApplyDate(),
                mLCGrpContSchema.getPolApplyDate())))
        {
            String updatelcpolsql = "update lcpol set PolApplyDate='"
                    + mLCGrpContSchema.getPolApplyDate() + "',"
                    + "ModifyDate='" + theCurrentDate + "',ModifyTime='"
                    + theCurrentTime + "'" + "where " + wherepart;
            map.put(updatelcpolsql, "UPDATE");
            String updatelccontsql = "update lccont set PolApplyDate='"
                    + mLCGrpContSchema.getPolApplyDate() + "',"
                    + "ModifyDate='" + theCurrentDate + "',ModifyTime='"
                    + theCurrentTime + "'" + "where " + wherepart;
            map.put(updatelccontsql, "UPDATE");
        }
        if (!(StrTool.compareString(tLCGrpContSet.get(1).getAgentCode(),
                mLCGrpContSchema.getAgentCode())))
        {
            String updatelcpolsql = "update lcpol set agentcode='"
                    + mLCGrpContSchema.getAgentCode() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelcpolsql, "UPDATE");
            String updatelccontsql = "update lccont set agentcode='"
                    + mLCGrpContSchema.getAgentCode() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelccontsql, "UPDATE");
            String updatelcgrppolsql = "update lcgrppol set agentcode='"
                    + mLCGrpContSchema.getAgentCode() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelcgrppolsql, "UPDATE");
        }
        if (!(StrTool.compareString(tLCGrpContSet.get(1).getAgentCom(),
                mLCGrpContSchema.getAgentCom())))
        {
            String updatelcpolsql = "update lcpol set agentcom='"
                    + mLCGrpContSchema.getAgentCom() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelcpolsql, "UPDATE");
            String updatelccontsql = "update lccont set agentcom='"
                    + mLCGrpContSchema.getAgentCom() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelccontsql, "UPDATE");
            String updatelcgrppolsql = "update lcgrppol set AgentCom='"
                    + mLCGrpContSchema.getAgentCom() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelcgrppolsql, "UPDATE");
        }
        if (!(StrTool.compareString(tLCGrpContSet.get(1).getAgentGroup(),
                mLCGrpContSchema.getAgentGroup())))
        {
            String updatelcpolsql = "update lcpol set AgentGroup='"
                    + mLCGrpContSchema.getAgentGroup() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelcpolsql, "UPDATE");
            String updatelccontsql = "update lccont set AgentGroup='"
                    + mLCGrpContSchema.getAgentGroup() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelccontsql, "UPDATE");
            String updatelcgrppolsql = "update lcgrppol set AgentGroup='"
                    + mLCGrpContSchema.getAgentGroup() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelcgrppolsql, "UPDATE");
        }
        if (!(StrTool.compareString(tLCGrpContSet.get(1).getSaleChnl(),
                mLCGrpContSchema.getSaleChnl())))
        {
            String updatelcpolsql = "update lcpol set SaleChnl='"
                    + mLCGrpContSchema.getSaleChnl() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelcpolsql, "UPDATE");
            String updatelccontsql = "update lccont set SaleChnl='"
                    + mLCGrpContSchema.getSaleChnl() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelccontsql, "UPDATE");
            String updatelcgrppolsql = "update lcgrppol set SaleChnl='"
                    + mLCGrpContSchema.getSaleChnl() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelcgrppolsql, "UPDATE");
        }
        if (!(StrTool.compareString(tLCGrpContSet.get(1).getPayMode(),
                mLCGrpContSchema.getPayMode())))
        {
            String updatelcpolsql = "update lcpol set PayMode='"
                    + mLCGrpContSchema.getPayMode() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelcpolsql, "UPDATE");
            String updatelccontsql = "update lccont set PayMode='"
                    + mLCGrpContSchema.getPayMode() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelccontsql, "UPDATE");
            String updatelcgrppolsql = "update lcgrppol set PayMode='"
                    + mLCGrpContSchema.getPayMode() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelcgrppolsql, "UPDATE");
        }
        if (!(StrTool.compareString(tLCGrpContSet.get(1).getRemark(),
                mLCGrpContSchema.getRemark())))
        {
            String updatelcpolsql = "update lcpol set Remark='"
                    + mLCGrpContSchema.getRemark() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelcpolsql, "UPDATE");
            String updatelccontsql = "update lccont set Remark='"
                    + mLCGrpContSchema.getRemark() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelccontsql, "UPDATE");
            String updatelcgrppolsql = "update lcgrppol set Remark='"
                    + mLCGrpContSchema.getRemark() + "'," + "ModifyDate='"
                    + theCurrentDate + "',ModifyTime='" + theCurrentTime + "'"
                    + "where " + wherepart;
            map.put(updatelcgrppolsql, "UPDATE");
        }
        String updateMissionSql = "update lwmission set Missionprop1='"
                + mLCGrpContSchema.getGrpContNo() + "' where Missionprop2='"
                + mLCGrpContSchema.getPrtNo()
                + "' and activityid='0000005002' ";
        map.put(updateMissionSql, "UPDATE");
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "BriefGroupContInputBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }
    
    /**
     * 处理综合开拓数据。
     * @param cGrpContNo
     * @return
     */
    private MMap delExtendInfo(LCExtendSchema aLCExtendSchema)
    {
        MMap tMMap = null;

        ExtendBL tExtendBL = new ExtendBL();

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(aLCExtendSchema);

        tMMap = tExtendBL.submitData(tVData, "");

        if (tMMap == null)
        {
            buildError("delCoInsuranceInfo", "共保要素处理失败。");
            return null;
        }

        return tMMap;
    }

}
