package com.sinosoft.lis.brieftb;

import java.util.Vector;

import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.f1print.LCGrpSuccorUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GrpBriefChangeNoBL {
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private String mOperate;
    private VData mResult = new VData();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private String old_GrpContNo = null;
    private String new_GrpContNo = null;
    private String old_PolNo;
    private String new_PolNo;
    private String old_ContNo;
    private String new_ContNo;
    private String old_GrpPolNo;
    private String new_GrpPolNo;
    private MMap map = new MMap();
    private GlobalInput mGlobalInput;
    TransferData mTransferData;
    public GrpBriefChangeNoBL() {
    }

    /**
     * submitData
     *
     * @param nInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData nInputData, String cOperate) {
        this.mInputData = nInputData;
        this.mOperate = cOperate;

        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(this.mResult, "INSERT")) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
//        /** 处理成生Xml文件 */
//        if (!dealCreatXml()) {
//            return false;
//        }
        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        if (this.map.size() > 0) {
            this.mResult.addElement(map);
        }
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        String chkSql = "select count(1) from lobgrpcont where grpcontno='" +
                        mLCGrpContSchema.getGrpContNo() + "'";
        int chk = Integer.parseInt((new ExeSQL()).getOneValue(chkSql));
        System.out.println("判断是否需要更新保保单号码! " + chk);
        if (chk == 0) {
            return true;
        }

        if (!dealContNo()) {
            return false;
        }
        //生成新的合同号
        this.new_GrpContNo = PubFun1.CreateMaxNo("GRPCONTNO",
                                                 this.mLCGrpContSchema.
                                                 getAppntNo());
        if (StrTool.cTrim(this.new_GrpContNo).equals("")) {
            buildError("dealData", "生成合同号码失败！");
            return false;
        }
        this.old_GrpContNo = this.mLCGrpContSchema.getGrpContNo();
        System.out.println("生成的新团体保单号码" + this.new_GrpContNo);
        //拼接sql
        String[] GrpPolContTables = getTableName("GRPCONTNO");
        //其余表的更新sql
        String condition = " GrpContNo='" + new_GrpContNo + "'";
        String wherepart = " GrpContNo='" + old_GrpContNo + "'";
        Vector VecContPol = PubFun.formUpdateSql(GrpPolContTables, condition,
                                                 wherepart);
        for (int i = 0; i < VecContPol.size(); i++) {
            this.map.put((String) VecContPol.get(i), "UPDATE");
        }
        //处理财务部分的表
        map.put("update LJTempFee set OtherNo='" + new_GrpContNo +
                "' where otherno='" +
                old_GrpContNo + "'", "UPDATE");
        map.put("update LJAPay set IncomeNo='" + new_GrpContNo +
                "' where IncomeNo='" +
                old_GrpContNo + "'", "UPDATE");
        map.put("update LJAPayPerson set GrpContNo='" + new_GrpContNo +
                "' where GrpContNo='" +
                old_GrpContNo + "'", "UPDATE");
        map.put("update LJAPayGrp set GrpContNo='" + new_GrpContNo +
                "' where GrpContNo='" +
                old_GrpContNo + "'", "UPDATE");

        map.put("update LJSPay set OtherNo='" + new_GrpContNo +
                "' where OtherNo='" +
                old_GrpContNo + "'", "UPDATE");
        map.put("update LJSPayPerson set GrpContNo='" + new_GrpContNo +
                "' where GrpContNo='" +
                old_GrpContNo + "'", "UPDATE");
        map.put("update LJSPayGrp set GrpContNo='" + new_GrpContNo +
                "' where GrpContNo='" +
                old_GrpContNo + "'", "UPDATE");

        map.put("update LJSPayB set OtherNo='" + new_GrpContNo +
                "' where OtherNo='" +
                old_GrpContNo + "'", "UPDATE");
        map.put("update LJSPayPersonB set GrpContNo='" + new_GrpContNo +
                "' where GrpContNo='" +
                old_GrpContNo + "'", "UPDATE");
        map.put("update LJSPayGrpB set GrpContNo='" + new_GrpContNo +
                "' where GrpContNo='" +
                old_GrpContNo + "'", "UPDATE");

        map.put("update LJSPay set GetNoticeNo='" + new_GrpContNo +
                "' where GetNoticeNo='" +
                old_GrpContNo + "'", "UPDATE");
        map.put("update LJSPayPerson set GetNoticeNo='" + new_GrpContNo +
                "' where GetNoticeNo='" +
                old_GrpContNo + "'", "UPDATE");
        map.put("update LJSPayGrp set GetNoticeNo='" + new_GrpContNo +
                "' where GetNoticeNo='" +
                old_GrpContNo + "'", "UPDATE");

        map.put("update LJSPayB set GetNoticeNo='" + new_GrpContNo +
                "' where GetNoticeNo='" +
                old_GrpContNo + "'", "UPDATE");
        map.put("update LJSPayPersonB set GetNoticeNo='" + new_GrpContNo +
                "' where GetNoticeNo='" +
                old_GrpContNo + "'", "UPDATE");
        map.put("update LJSPayGrpB set GetNoticeNo='" + new_GrpContNo +
                "' where GetNoticeNo='" +
                old_GrpContNo + "'", "UPDATE");
        map.put("update lacommision set grpcontno='" + new_GrpContNo +
                "',contno='" + new_GrpContNo + "' where grpcontno='" +
                old_GrpContNo + "'", "UPDATE");
        map.put("update lacontfycrate set grpcontno='" + new_GrpContNo +
                "' where grpcontno='" +
                old_GrpContNo + "'", "UPDATE");
        map.put("update lacontfycrateb set grpcontno='" + new_GrpContNo +
                "' where grpcontno='" +
                old_GrpContNo + "'", "UPDATE");
        map.put("update LAGrpCommisionDetail set grpcontno='" + new_GrpContNo +
                "' where grpcontno='" +
                old_GrpContNo + "'", "UPDATE");
        map.put("update lacharge set grpcontno='" + new_GrpContNo +
                "',contno='" + new_GrpContNo + "' where grpcontno='" +
                old_GrpContNo + "'", "UPDATE");
        map.put("update laascription set grpcontno='" + new_GrpContNo +
                "',contno='" + new_GrpContNo + "' where grpcontno='" +
                old_GrpContNo + "'", "UPDATE");
        map.put("update LDGrp set GrpAppntNum=GrpAppntNum+1 where CustomerNo='" +
                this.mLCGrpContSchema.getAppntNo() + "'", "UPDATE");
        return true;
    }

    /**
     * getGrpContNoTableName
     *
     * @return String[]
     */
    private String[] getTableName(String columns) {
        String sql ="SELECT TBNAME FROM SYSIBM.SYSCOLUMNS a "
            + "WHERE  TBCREATOR='DB2INST1' AND NAME='" + columns + "' AND HIGH2KEY IS NOT NULL "
            + "AND (TBNAME LIKE 'LC%' OR TBNAME LIKE 'LD%' OR TBNAME LIKE 'LJ%' OR TBNAME='LBINSUREDLIST') "
            + "and exists (select 1 from SYSIBM.SYSTABLES b where a.TBName = b.Name and b.Type = 'T')";
        SSRS ssrs = (new ExeSQL()).execSQL(sql);
        String[] tTbName = new String[ssrs.getMaxRow()];
        for (int i = 0; i < ssrs.getMaxRow(); i++) {
            tTbName[i] = ssrs.GetText(i + 1, 1);
        }
        return tTbName;
    }

    /**
     * dealContNo
     *
     * @return boolean
     */
    private boolean dealContNo() {
        //需要处理团单下的全部合同
        StringBuffer sql = new StringBuffer(255);
        sql.append("select contno from lccont where grpcontno ='");
        sql.append(this.mLCGrpContSchema.getGrpContNo());
        sql.append("' ");
        ExeSQL tExeSQL = new ExeSQL();
        SSRS ssrs = tExeSQL.execSQL(sql.toString());
        for (int i = 1; i <= ssrs.MaxRow; i++) {
            String tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
            this.new_ContNo = PubFun1.CreateMaxNo("GRPPERSONCONTNO", tLimit);
            this.old_ContNo = ssrs.GetText(i, 1);
            String[] ContTables = getTableName("CONTNO");

            String condition = " ContNo='" + new_ContNo + "'";
            String wherepart = " ContNo='" + old_ContNo + "'";
            Vector VecContPol = PubFun.formUpdateSql(ContTables, condition,
                    wherepart);
            for (int m = 0; m < VecContPol.size(); m++) {
                this.map.put((String) VecContPol.get(m), "UPDATE");
            }
        }
        ssrs = null;
        sql = new StringBuffer(255);
        sql.append("select polno from lcpol where grpcontno ='");
        sql.append(this.mLCGrpContSchema.getGrpContNo());
        sql.append("' ");
        ssrs = tExeSQL.execSQL(sql.toString());
        for (int i = 1; i <= ssrs.MaxRow; i++) {
            String tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
            this.new_PolNo = PubFun1.CreateMaxNo("POLNO", tLimit);
            this.old_PolNo = ssrs.GetText(i, 1);

            String[] PolTables = getTableName("POLNO");

            String condition = " PolNo='" + new_PolNo + "'";
            String wherepart = " PolNo='" + old_PolNo + "'";
            Vector VecContPol = PubFun.formUpdateSql(PolTables, condition,
                    wherepart);
            for (int m = 0; m < VecContPol.size(); m++) {
                this.map.put((String) VecContPol.get(m), "UPDATE");
            }
        }
        ssrs = null;
        sql = new StringBuffer(255);
        sql.append("select grppolno from lcgrppol where grpcontno ='");
        sql.append(this.mLCGrpContSchema.getGrpContNo());
        sql.append("' ");
        ssrs = tExeSQL.execSQL(sql.toString());
        for (int i = 1; i <= ssrs.MaxRow; i++) {
            String tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
            this.new_GrpPolNo = PubFun1.CreateMaxNo("GRPPOLNO", tLimit);
            this.old_GrpPolNo = ssrs.GetText(i, 1);
            String[] GrpPolTables = {
                                    "LCAscriptionRuleFactory",
                                    "LCAscriptionRuleParams",
                                    "LCContPlanDutyParam", "LCFactory",
                                    "LCGeneralToRisk",
                                    "LCGrpFee", "LCGrpFeeParam", "LCGrpPol",
                                    "LCGUWError", "LCGUWMaster", "LCGUWSub",
                                    "LCInsureAcc",
                                    "LCInsureAccClass", "LCInsureAccClassFee",
                                    "LCInsureAccFee",
                                    "LCInsureAccTrace", "LCParam",
                                    "LCPayRuleFactory", "LCPayRuleParams",
                                    "LCPol", "LJAPayGrp", "LJAPayPerson",
                                    "LJSPayGrp", "LJSPayGrpB", "lacharge"
            };
            String condition = " GrpPolNo='" + new_GrpPolNo + "'";
            String wherepart = " GrpPolNo='" + old_GrpPolNo + "'";
            Vector VecContPol = PubFun.formUpdateSql(GrpPolTables, condition,
                    wherepart);
            for (int m = 0; m < VecContPol.size(); m++) {
                this.map.put((String) VecContPol.get(m), "UPDATE");
            }
            map.put("update lacommision set GrpPolNo='" + new_GrpPolNo +
                    "',polno='" + new_GrpPolNo
                    + "', mainpolno='" + new_GrpPolNo + "' where GrpPolNo='" +
                    old_GrpPolNo + "' and grpcontno='" +
                    this.mLCGrpContSchema.getGrpContNo() + "'", "UPDATE");
            map.put("update lacharge set GrpPolNo='" + new_GrpPolNo +
                    "',polno='" + new_GrpPolNo
                    + "', mainpolno='" + new_GrpPolNo + "' where GrpPolNo='" +
                    old_GrpPolNo + "' and grpcontno='" +
                    this.mLCGrpContSchema.getGrpContNo() + "'", "UPDATE");

        }

        return true;
    }

    /**
     * dealCreatXml
     *
     * @return boolean
     */
    private boolean dealCreatXml() {
        VData tVData = new VData();
        mLCGrpContSchema.setGrpContNo(this.new_GrpContNo);
        System.out.println("new_GrpContNo" + new_GrpContNo);
        tVData.addElement(mLCGrpContSchema);
        tVData.addElement(mGlobalInput);
        tVData.addElement((String) mTransferData.getValueByName("TemplatePath"));
        tVData.addElement((String) mTransferData.getValueByName("OutXmlPath"));
        tVData.addElement(mGlobalInput);
        LCGrpSuccorUI tLCGrpSuccorUI = new LCGrpSuccorUI();
        if (!tLCGrpSuccorUI.submitData(tVData, "PRINT")) {
            System.out.println(
                    "程序第93行出错，请检查BriefGrpContSignAfterInitService.java中的submitData方法！");
            CError tError = new CError();
            tError.moduleName = "BriefGrpContSignAfterInitService.java";
            tError.functionName = "submitData";
            tError.errorMessage = "生成Xml文件失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        if (StrTool.cTrim(mLCGrpContSchema.getGrpContNo()).equals("")) {
            System.out.println(
                    "程序第76行出错，请检查GrpBriefChangeNoBL.java中的checkData方法！");
            CError tError = new CError();
            tError.moduleName = "GrpBriefChangeNoBL.java";
            tError.functionName = "checkData";
            tError.errorMessage = "保单号为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        mLCGrpContSchema = (LCGrpContSchema)this.mInputData.
                           getObjectByObjectName(
                                   "LCGrpContSchema",
                                   0);
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setSchema(mLCGrpContSchema);
        if (!tLCGrpContDB.getInfo()) {
            System.out.println(
                    "程序第164行出错，请检查GrpBriefChangeNoBL.java中的getInputData方法！");
            CError tError = new CError();
            tError.moduleName = "GrpBriefChangeNoBL.java";
            tError.functionName = "getInputData";
            tError.errorMessage = "未找到团体保单信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        this.mLCGrpContSchema.setSchema(tLCGrpContDB.getSchema());
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput",
                0);
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        return true;
    }

    /**
     * getResult
     *
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }

    public static void main(String[] args) {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86";
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema.setGrpContNo("0000022705");
        VData tVData = new VData();
        tVData.add(tLCGrpContSchema);
        tVData.add(tG);
//        GrpBriefChangeNoUI tGrpBriefChangeNoUI = new GrpBriefChangeNoUI();
//        tGrpBriefChangeNoUI.submitData(tVData, "CHANGE||GROUPPOL");
        //测试getGrpContNoTableName
        GrpBriefChangeNoBL tGrpBriefChangeNoBL = new GrpBriefChangeNoBL();
        tGrpBriefChangeNoBL.testGetGrpContNoTableName();
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "GrpBriefChangeNoBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

    public boolean testGetGrpContNoTableName() {
        getTableName("GRPCONTNO");
        return true;
    }
}
