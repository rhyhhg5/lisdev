package com.sinosoft.lis.brieftb;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description: 先收费回盘文件读取
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author NicolE
 * @version 1.0
 */

public class ReadReturnFileUI {
	/** 传入数据的容器 */
	private VData mInputData = new VData();

	/** 传出数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	public ReadReturnFileUI() {
	}

	/**
	 * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	 * 
	 * @param cInputData
	 *            传入的数据,VData对象
	 * @param cOperate
	 *            数据操作字符串，主要包括"READ"和""
	 * @return 布尔值（true--提交成功, false--提交失败）
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 数据操作字符串拷贝到本类中
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;

		System.out.println("---ReadFromFile BL BEGIN---");
		ReadReturnFileBL tReadReturnFileBL = new ReadReturnFileBL();

		if (tReadReturnFileBL.submitData(mInputData, mOperate) == false) {
			mErrors.copyAllErrors(tReadReturnFileBL.mErrors);
			mResult.clear();
			mResult.add(mErrors.getFirstError());
			return false;
		} else {
			mResult = tReadReturnFileBL.getResult();
		}
		System.out.println("---ReadFromFile BL END---");

		return true;
	}

	/**
	 * 数据输出方法，供外界获取数据处理结果
	 * 
	 * @return 包含有数据查询结果字符串的VData对象
	 */
	public VData getResult() {
		return mResult;
	}

	public static void main(String[] args) {
		ReadReturnFileUI ReadReturnFileUI1 = new ReadReturnFileUI();

		TransferData transferData1 = new TransferData();
		transferData1.setNameAndValue("fileName",
				"D:\\PICCH\\V1.1\\ui\\bank\\ReturnFromBankFile\\C1821121.T10");
		transferData1.setNameAndValue("serialNo", "00000000000000000031");
		transferData1.setNameAndValue("bankCode", "9995");
		transferData1.setNameAndValue("bankUniteFlag", "MX");
		transferData1.setNameAndValue("bankInUniteCode", "0995");

		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.Operator = "001";

		VData inVData = new VData();
		inVData.add(transferData1);
		inVData.add(tGlobalInput);

		if (!ReadReturnFileUI1.submitData(inVData, "READ")) {
			VData rVData = ReadReturnFileUI1.getResult();
			System.out.println("Submit Failed! " + (String) rVData.get(0));
		} else
			System.out.println("Submit Succed!");
	}
}
