package com.sinosoft.lis.brieftb;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.PubSubmit;

/**
 *
 * <p>Title: 简易投保删除操作</p>
 *
 * <p>Description: 如果在合同维护中,进行了需要重打保单的操作则把保单移到备份表 </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author YangMing
 * @version 1.0
 */
public class BriefGroupContDeleteBL {
    public BriefGroupContDeleteBL() {
    }

    /**全局数据变量*/
    public CErrors mErrors = new CErrors();

    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /**操作字符*/
    private String mOperate = "";
    private String mGrpContNo;
    /** 往前面传输数据的容器 */
    private VData mResult = new VData();

    /**传输到后台处理的map*/
    private MMap map = new MMap();

    /**操作表**/
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();


    /**
     * UI接口
     * @param cInputData VData
     * @param mOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.mInputData = cInputData;
        this.mOperate = cOperate;

        if (!getInputData())
            return false;

        if (!checkData())
            return false;

        if (!dealData())
            return false;

        if (!prepareOutputData())
            return false;

//        PubSubmit tPubSubmit = new PubSubmit();
//        if (!tPubSubmit.submitData(this.mResult, "INSERT")) {
//            System.out.println("程序第68行出错，请检查BriefGroupContDeleteBL.java中的submitData方法！");
//            CError tError = new CError();
//            tError.moduleName = "BriefGroupContDeleteBL.java";
//            tError.functionName = "submitData";
//            tError.errorMessage = "递交失败！";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        this.mResult.add(this.map);
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        //整理全部的表
        if (mOperate.equals("DELETE||CONT")) {
            System.out.println("开始BL操作!");
            if (!dealAllTables())
                return false;
        }
        return true;
    }

    /**
     * dealAllTables
     *
     * @return boolean
     */
    private boolean dealAllTables() {
        map.put("insert into LOBInsuredRelated (select * from LCInsuredRelated where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                mGrpContNo + "'))", "INSERT");

        map.put("insert into LOBDuty (select * from LCDuty where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                mGrpContNo + "'))", "INSERT");

        map.put("insert into LOBPrem_1 (select * from LCPrem_1 where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                mGrpContNo + "'))", "INSERT");

        map.put("insert into LOBPremToAcc (select * from LCPremToAcc where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                mGrpContNo + "'))", "INSERT");

        map.put("insert into LOBGetToAcc (select * from LCGetToAcc where PolNo in (select PolNo from LCPol where GrpContNo = '" +
                mGrpContNo + "'))", "INSERT");

        map.put("insert into LOBPol (select * from LCPol where GrpContNo = '" + mGrpContNo +
                "')",
                "INSERT");

        map.put("insert into LOBBnf (select * from LCBnf where ContNo in (select ContNo from LCCont where GrpContNo = '" +
                mGrpContNo + "'))", "INSERT");

        map.put("insert into LOBCont (select * from LCCont where GrpContNo = '" + mGrpContNo +
                "')", "INSERT");

        map.put("insert into LOBGrpPol (select * from LCGrpPol where GrpContNo = '" +
                mGrpContNo +
                "')", "INSERT");

        map.put("insert into LOBPrem (select * from LCPrem where GrpContNo = '" + mGrpContNo +
                "')", "INSERT");

        map.put("insert into LOBGet (select * from LCGet where GrpContNo = '" + mGrpContNo +
                "')",
                "INSERT");

        map.put("insert into LOBInsureAccFee (select * from LCInsureAccFee where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put(
                "insert into LOBInsureAccClassFee (select * from LCInsureAccClassFee where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put(
                "insert into LOBInsureAccClass (select * from LCInsureAccClass where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put("insert into LOBInsureAcc (select * from LCInsureAcc where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put(
                "insert into LOBInsureAccTrace (select * from LCInsureAccTrace where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put("insert into LOBInsured (select * from LCInsured where GrpContNo = '" +
                mGrpContNo +
                "')", "INSERT");

        map.put(
                "insert into LOBCustomerImpart (select * from LCCustomerImpart where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put(
                "insert into LOBCustomerImpartParams (select * from LCCustomerImpartParams where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put("insert into LOBAppnt (select * from LCAppnt where GrpContNo = '" + mGrpContNo +
                "')", "INSERT");

        map.put("insert into LOBGrpAppnt (select * from LCGrpAppnt where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put("insert into LOBGrpFee (select * from LCGrpFee where GrpContNo = '" +
                mGrpContNo +
                "')", "INSERT");

        map.put("insert into LOBGrpFeeParam (select * from LCGrpFeeParam where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put("insert into LOBContPlan (select * from LCContPlan where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put("insert into LOBGeneral (select * from LCGeneral where GrpContNo = '" +
                mGrpContNo +
                "')", "INSERT");

        map.put(
                "insert into LOBGeneralToRisk (select * from LCGeneralToRisk where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");
        /**
         * modify by zhangxing
         * 2006-05-10
         * 修改原因：LCContPlanRisk的PK为ProposalGrpContNo,引起主键冲突
         */

        //        map.put("insert into LOBContPlanRisk (select * from LCContPlanRisk where GrpContNo = '" +
//                mGrpContNo + "')", "INSERT");

        map.put(
                "insert into LOBContPlanDutyParam (select * from LCContPlanDutyParam where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put(
                "insert into LOBContPlanFactory (select * from LCContPlanFactory where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put(
                "insert into LOBContPlanParam (select * from LCContPlanParam where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put("insert into LOBUWError (select * from LCUWError where GrpContNo = '" +
                mGrpContNo +
                "')", "INSERT");

        map.put("insert into LOBUWSub (select * from LCUWSub where GrpContNo = '" + mGrpContNo +
                "')", "INSERT");

        map.put("insert into LOBUWMaster (select * from LCUWMaster where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put("insert into LOBCUWError (select * from LCCUWError where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put("insert into LOBCUWSub (select * from LCCUWSub where GrpContNo = '" +
                mGrpContNo +
                "')", "INSERT");

        map.put("insert into LOBCUWMaster (select * from LCCUWMaster where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put("insert into LOBGUWError (select * from LCGUWError where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put("insert into LOBGUWSub (select * from LCGUWSub where GrpContNo = '" +
                mGrpContNo +
                "')", "INSERT");

        map.put("insert into LOBGUWMaster (select * from LCGUWMaster where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put("insert into LOBGCUWError (select * from LCGCUWError where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put("insert into LOBGCUWSub (select * from LCGCUWSub where GrpContNo = '" +
                mGrpContNo +
                "')", "INSERT");

        map.put("insert into LOBGCUWMaster (select * from LCGCUWMaster where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put("insert into LOBUWReport (select * from LCUWReport where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put("insert into LOBNotePad (select * from LCNotePad where GrpContNo = '" +
                mGrpContNo +
                "')", "INSERT");

        map.put("insert into LOBRReport (select * from LCRReport where GrpContNo = '" +
                mGrpContNo +
                "')", "INSERT");

        map.put("insert into LOBIssuePol (select * from LCIssuePol where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        /** @author:yangming 因为主健冲突因此不进备份表 */
//        map.put("insert into LOBGrpIssuePol (select * from LCGrpIssuePol where GrpContNo = '" +
//                mGrpContNo + "')", "INSERT");

        map.put("insert into LOBPENoticeItem (select * from LCPENoticeItem where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put("insert into LOBPENotice (select * from LCPENotice where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put("insert into LOBCGrpSpec (select * from LCCGrpSpec where GrpContNo = '" +
                mGrpContNo + "')", "INSERT");

        map.put("insert into LOBCSpec (select * from LCCSpec where GrpContNo = '" + mGrpContNo +
                "')", "INSERT");

        map.put("insert into LOBSpec (select * from LCSpec where GrpContNo = '" + mGrpContNo +
                "')", "INSERT");

        map.put("insert into LOBNation (select * from LCNation where GrpContNo = '" + mGrpContNo +
                "')", "INSERT");

        map.put("insert into LOBGrpCont (select * from LCGrpCont where GrpContNo = '" + mGrpContNo + "')", "INSERT");
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        if (StrTool.cTrim(this.mLCGrpContSchema.getGrpContNo()).equals("")) {
            System.out.println("程序第94行出错，请检查BriefGroupContDeleteBL.java中的checkData方法！");
            CError tError = new CError();
            tError.moduleName = "BriefGroupContDeleteBL.java";
            tError.functionName = "checkData";
            tError.errorMessage = "传入的保单号码为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        mGrpContNo = this.mLCGrpContSchema.getGrpContNo();
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        mLCGrpContSchema = (LCGrpContSchema)this.mInputData.getObjectByObjectName("LCGrpContSchema",
                0);
        return true;
    }

    /**
     * 操作结果
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }
}
