package com.sinosoft.lis.brieftb;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class LCContModifyUI {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    public LCContModifyUI() {
    }

    public boolean submitData(VData nInputData, String cOperate) {
        LCContModifyBL tLCContModifyBL = new
                LCContModifyBL();
        System.out.println("递交数据到BriefSingleContModifyBL！");
        if (!tLCContModifyBL.submitData(nInputData, cOperate)) {
            this.mErrors.copyAllErrors(tLCContModifyBL.mErrors);
            return false;
        } else {
            mResult = tLCContModifyBL.getResult();
        }
        return true;
    }

    /**
     * 操作结果
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }
}
