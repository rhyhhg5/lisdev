package com.sinosoft.lis.brieftb;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

import java.io.*;

import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import javax.xml.transform.Source;
import javax.xml.transform.Result;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamSource;

import com.sinosoft.workflow.brieftb.*;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description: 先收费回盘文件读取 处理逻辑：如果还没有录单，写一条汇总信息到LJTempFeeClass<br>
 * 如果已录单，则需令更新LJTempFee中的信息<br>
 * LJTempFeeClass中的TempFeeNo存入印刷号
 * </p>
 * <p>
 * Copyright: Copyright (c) 2007
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 *
 * @author NicolE
 * @version 1.0
 */

public class ReadReturnFileBL
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 传出数据的容器 */
    private VData mResult = new VData();

    /** 提交数据的容器 */
    private MMap map = new MMap();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    // 业务数据
    private TransferData inTransferData = new TransferData();

    private GlobalInput inGlobalInput = new GlobalInput();

    private LYReturnFromBankSet outLYReturnFromBankSet = new LYReturnFromBankSet();

    private LYBankLogSchema outLYBankLogSchema = new LYBankLogSchema();

    private LJTempFeeClassSet outLJTempFeeClassSet = new LJTempFeeClassSet();

    //避免outLJTempFeeClassSet被清空
    private LJTempFeeClassSet bakLJTempFeeClassSet = new LJTempFeeClassSet();

    private String fileName = "";

    /** 选择银行编码 */
    private String bankCode = "";

    private Document dataDoc = null;

    private Document resultDoc = null;

    // 批次号
    private String serialNo = "";
    
    private String insBankCode = "";
    private String insBankAccNo = "";

    // 总金额
    private double totalMoney = 0.0;

    // 总笔数
    private int totalNum = 0;

    private String manageCom = "";

    // 可签单标记
    private boolean signContFlag = false;

    // 只有已录单的数据才写数据到该表
    private LJTempFeeSet outLJTempFeeSet = new LJTempFeeSet();

    // 只有已录单并且金额校验通过的数据才写数据到该表
    private LJTempFeeClassBSet outLJTempFeeClassBSet = new LJTempFeeClassBSet();

    // 如果已录单则需更新lccont的appFlag字段为1
    private LCContSet outLCContSet = new LCContSet();

    // 如果已录单则需更新lcpol的appFlag字段为1
    private LCPolSet outLCPolSet = new LCPolSet();

    // 待签单的保单
    private LCContSet signLCContSet = new LCContSet();

    public ReadReturnFileBL()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     *
     * @param cInputData
     *            传入的数据,VData对象
     * @param cOperate
     *            数据操作字符串，主要包括"READ"和""
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        // 得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
            return false;
        System.out.println("---End getInputData---");

        // 进行业务处理
        if (!dealData())
            return false;
        System.out.println("---End dealData---");

        // 文件读取标识
        if (mOperate.equals("READ"))
        {
            // 准备往后台的数据
            if (!prepareOutputData())
                return false;
            System.out.println("---End prepareOutputData---");

            System.out.println("Start PubSubmit Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                System.out.println("回盘文件内容存储失败！");
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                mResult.clear();
                return false;
            }
            System.out.println("start to sign the conts!");
            // add by NicolE 2007.07.17 先收费签单
            //if (!signConts())
            //{
            //    System.out.println("Line 162,签单失败！");
            //    return false;
            //}
            //else
            //{
            System.out.println("开始备份数据到B表......");
            // 备份数据到B表
            boolean flag = bakDataToTempFeeClassB();
            if (!flag)
            {
                System.out.println("Line 170,备份数据到B表失败！");
                CError.buildErr(this, "备份数据到暂收备份表失败");
                return false;
            }
            VData data = new VData();
            MMap map = new MMap();
            map.put(outLJTempFeeClassBSet, "INSERT");
            data.add(map);
            PubSubmit tSubmit = new PubSubmit();
            if (!tSubmit.submitData(data, ""))
            {
                System.out.println("备份到B表失败！");
                this.mErrors.copyAllErrors(tSubmit.mErrors);
                return false;
            }
            System.out.println("备份到B表成功啦啦啦啦！");
            //}

            System.out.println("End PubSubmit Submit...");
        }
        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     *
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            inTransferData = (TransferData) mInputData.getObjectByObjectName(
                    "TransferData", 0);
            inGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                    "GlobalInput", 0);
        }
        catch (Exception e)
        {
            // @@错误处理
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 更新银行日志数据
     *
     * @param tLYSendToBankSchema
     * @return
     */
    private LYBankLogSchema initLYBankLog()
    {
        // 获取日志记录
        LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();
        // 如果不是银联文件，则修改主日志表
        tLYBankLogSchema.setSerialNo(serialNo);
        tLYBankLogSchema.setBankCode(bankCode);
        tLYBankLogSchema.setLogType("S");
        tLYBankLogSchema.setInFile(fileName);
        tLYBankLogSchema.setTotalNum(totalNum);
        tLYBankLogSchema.setTotalMoney(totalMoney);
        tLYBankLogSchema.setReturnDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setReturnOperator(inGlobalInput.Operator);
        tLYBankLogSchema.setStartDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setMakeDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setModifyDate(PubFun.getCurrentDate());
        tLYBankLogSchema.setModifyTime(PubFun.getCurrentTime());
        tLYBankLogSchema.setComCode(manageCom);
        tLYBankLogSchema.setDealState("1");// 已回盘

        return tLYBankLogSchema;
    }

    /**
     * 创建一个xml文档对象DOM
     *
     * @return
     */
    private Document buildDocument()
    {
        try
        {
            // Create the document builder
            DocumentBuilderFactory dbfactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder docbuilder = dbfactory.newDocumentBuilder();

            // Create the new document(s)
            return docbuilder.newDocument();
        }
        catch (Exception e)
        {
            System.out.println("Problem creating document: " + e.getMessage());
            return null;
        }
    }

    /**
     * 获取xsl文件路径
     *
     * @return
     */
    public String getXslPath() throws Exception
    {
        LDBankDB tLDBankDB = new LDBankDB();
        String xslPath = "";
        tLDBankDB.setBankCode(bankCode);
        if (!tLDBankDB.getInfo())
            throw new Exception("获取银行XSL描述信息失败！");
        xslPath = tLDBankDB.getAgentPayReceiveF();
        // 初始化回盘银行管理机构
        manageCom = tLDBankDB.getComCode();
        return xslPath;
    }

    /**
     * Simple sample code to show how to run the XSL processor from the API.
     */
    public boolean xmlTransform()
    {
        try
        {
            // 非银联或银联的汇总文件，使用第一个描述
            String xslPath = getXslPath();
            //本机测试
            //        xslPath = "D:/picc/picch/ui/bank/ReturnFromBankFile/xsl/return_gd_nongxingsheFirst.xsl"; 
            System.out.println("xslPath:" + xslPath);

            File fStyle = new File(xslPath);
            Source source = new DOMSource(dataDoc);
            Result result = new DOMResult(resultDoc);
            Source style = new StreamSource(fStyle);

            // Create the Transformer
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer(style);
            // Transform the Document
            transformer.transform(source, result);
            System.out.println("Transform Success!");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            // @@错误处理
            CError.buildErr(this, "Xml处理失败，具体原因是：" + e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * 将数据存入数据库
     *
     * @param tLYReturnFromBankSet
     * @return
     */
    private LYReturnFromBankSet xmlToDatabase()
    {
        try
        {
            LYReturnFromBankSet tLYReturnFromBankSet = new LYReturnFromBankSet();
            displayDocument(resultDoc);
            // get all rows
            NodeList rows = resultDoc.getDocumentElement().getChildNodes();
            for (int i = 0; i < rows.getLength(); i++)
            {
                // For each row, get the row element and name
                Element thisRow = (Element) rows.item(i);
                // Get the columns for thisRow
                NodeList columns = thisRow.getChildNodes();
                LYReturnFromBankSchema tLYReturnFromBankSchema = new LYReturnFromBankSchema();

                for (int j = 0; j < columns.getLength(); j++)
                {
                    Element thisColumn = (Element) columns.item(j);

                    String colName = thisColumn.getNodeName();
                    String colValue = thisColumn.getFirstChild().getNodeValue();
                    // 根据标签名往数据库的同名字段里插入数据
                    // 如果是银联汇总文件，不处理返回盘表，只纪录日志明细表
                    tLYReturnFromBankSchema.setV(colName, colValue);
                }
                String dealType = "S";
                tLYReturnFromBankSchema.setSerialNo(serialNo);
                tLYReturnFromBankSchema.setDealType(dealType);
                tLYReturnFromBankSchema.setBankCode(bankCode);
                tLYReturnFromBankSchema.setComCode(manageCom);
                tLYReturnFromBankSchema.setAgentCode(manageCom);
                tLYReturnFromBankSchema.setNoType("9");
               // tLYReturnFromBankSchema
               //         .setBankDealDate(PubFun.getCurrentDate());
                tLYReturnFromBankSchema
                        .setBankDealTime(PubFun.getCurrentTime());
                tLYReturnFromBankSchema.setModifyDate(PubFun.getCurrentDate());
                tLYReturnFromBankSchema.setModifyTime(PubFun.getCurrentTime());
                tLYReturnFromBankSchema.setRemark(inGlobalInput.Operator);
                tLYReturnFromBankSchema.setRiskCode("000000");
                tLYReturnFromBankSet.add(tLYReturnFromBankSchema);
            }

            if (tLYReturnFromBankSet.size() == 0)
                throw new NullPointerException("将数据存入数据库失败");

            totalNum = tLYReturnFromBankSet.size();
            for (int i = 0; i < tLYReturnFromBankSet.size(); i++)
            {
                totalMoney = totalMoney
                        + tLYReturnFromBankSet.get(i + 1).getPayMoney();
            }
            return tLYReturnFromBankSet;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            // @@错误处理
            CError.buildErr(this, "Xml转入数据库处理失败: " + e.getMessage());
            return null;
        }
    }

    /**
     * 读取银行返回文件
     *
     * @param fileName
     * @return
     */
    private boolean readBankFile(String fileName)
    {
        // Declare the document
        dataDoc = buildDocument();
        resultDoc = buildDocument();
        //本机测试
        //        fileName = "e:/1.txt";
        System.out.println("----FileName--->" + fileName);

        try
        {
            // 读入文件到BUFFER中以提高处理效率
            BufferedReader in = new BufferedReader(new FileReader(fileName));
            // 将所有文本以行为单位读入到VECTOR中
            String strLine = "";
            // 创建根标签
            Element dataRoot = dataDoc.createElement("BANKDATA");
            // 循环获取每一行
            while (true)
            {
                strLine = in.readLine();
                if (strLine == null)
                    break;
                strLine = strLine.trim();
                // 去掉空行
                if (strLine.length() < 3)
                    continue;
                System.out.println(strLine);
                // Create the element to hold the row
                Element rowEl = dataDoc.createElement("ROW");
                Element columnEl = dataDoc.createElement("COLUMN");
                columnEl.appendChild(dataDoc.createTextNode(strLine));
                rowEl.appendChild(columnEl);
                // Add the row element to the root
                dataRoot.appendChild(rowEl);
            }
            // Add the root to the document
            dataDoc.appendChild(dataRoot);
            // 显示XML信息，调试用
            displayDocument(dataDoc);
            in.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            // @@错误处理
            CError.buildErr(this, "读取银行返回文件失败");
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     *
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        try
        {
            // 获取银行文件数据
            if (mOperate.equals("READ"))
            {
                // 获取返回文件名称
                fileName = (String) inTransferData.getValueByName("fileName");
                fileName = fileName.replace('\\', '/');
                bankCode = (String) inTransferData.getValueByName("bankCode");
                serialNo = PubFun1.CreateMaxNo("1", 20);
                insBankCode = (String) inTransferData.getValueByName("getbankcode");
                insBankAccNo = (String) inTransferData.getValueByName("getbankaccno");
               // fileName="C:\\Documents and Settings\\Administrator\\桌面\\2008-03-25yingxl\\2010-3-4\\tianjin.txt";
                // 读取银行返回文件，公共部分
                if (!readBankFile(fileName))
                    throw new Exception("读取银行返回文件失败");

                // 转换xml，公共部分
                if (!xmlTransform())
                    throw new Exception("转换xml失败");

                // 将数据存入数据库，普通银行、银联汇总和银联明细各不相同
                outLYReturnFromBankSet = xmlToDatabase();
                if (outLYReturnFromBankSet == null
                        || outLYReturnFromBankSet.size() == 0)
                    return false;

                // 生成银行日志数据
                outLYBankLogSchema.setSchema(initLYBankLog());
                // 初始化暂缴费数据表
                if (!initLJTempFeeClass())
                {
                    throw new Exception("初始化暂交费分类表数据失败！");
                }
            }
        }
        catch (Exception e)
        {
            // @@错误处理
            CError.buildErr(this, "数据处理错误:" + e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     *
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            map.put(outLYReturnFromBankSet, "INSERT");
            map.put(outLYBankLogSchema, "INSERT");
            map.put(outLJTempFeeClassSet, "INSERT");
            map.put(outLJTempFeeSet, "INSERT");
            // 如已录单则回盘同时执行签单处理
//            if (signContFlag)
//            {
//                map.put(outLJTempFeeSet, "UPDATE");
//                //map.put(outLJTempFeeClassBSet, "INSERT");
//            }
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
            return false;
        }
        return true;
    }

    public static int num = 0;

    public void displayDocument(Node d)
    {
        num += 2;
        if (d.hasChildNodes())
        {
            NodeList nl = d.getChildNodes();
            for (int i = 0; i < nl.getLength(); i++)
            {
                Node n = nl.item(i);
                for (int j = 0; j < num; j++)
                {
                    System.out.print(" ");
                }
                if (n.getNodeValue() == null)
                {
                    System.out.println("<" + n.getNodeName() + ">");
                }
                else
                {
                    System.out.println(n.getNodeValue());
                }
                displayDocument(n);
                num -= 2;
                // System.out.println("num:" + num);
                if (n.getNodeValue() == null)
                {
                    for (int j = 0; j < num; j++)
                    {
                        System.out.print(" ");
                    }
                    System.out.println("</" + n.getNodeName() + ">");
                }
            }// end for
        }// end if
    }

    /**
     * 签单需满足两个条件：1，已录单；2，是成功数据(目前回盘的数据均为成功数据)
     *
     * @param tSchema
     * @return
     */
    private boolean initLJTempFeeClass()
    {
        System.out.println("Into ReadReturnFileBL.initLJTempFeeClass()...");
        LCContDB tLCContDB = new LCContDB();
        try
        {
            String tResult = "";
            String tResult1 = "";
            String tResult2 = "";
            String tResult3 = "";
            for (int i = 1; i <= outLYReturnFromBankSet.size(); i++)
            {
                LYReturnFromBankSchema tSchema = outLYReturnFromBankSet.get(i);
                LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
                if(StrTool.cTrim(tSchema.getPolNo()) == null || StrTool.cTrim(tSchema.getPolNo()).equals("")) {
                    mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
                    CError.buildErr("outputXML", "查询LJTempFeeClass的数据条件为空");
                    return false;
                }
                
                if(StrTool.cTrim(tSchema.getBankDealDate()).equals("")){
                    throw new Exception(" 业务号码："+tSchema.getPolNo()+",银行处理日期为空！");
                }
                if(PubFun.calInterval(PubFun.getCurrentDate(),tSchema.getBankDealDate(),"D")>0){
                    throw new Exception(" 缴费号是"+tSchema.getPolNo()+"，银行处理日期大于当前日期！");
                }
                
                tLJTempFeeClassDB.setTempFeeNo(StrTool
                        .cTrim(tSchema.getPolNo()));
                if (tLJTempFeeClassDB.query().size() > 0)
                {
                    if (tResult2.equals(""))
                    {
                        tResult2 = tResult2 + StrTool.cTrim(tSchema.getPolNo());
                    }
                    else
                    {
                        tResult2 = tResult2 + "、"
                                + StrTool.cTrim(tSchema.getPolNo());
                    }
                    //                    CError.buildErr(this, "印刷号或缴费凭证号是"+tSchema.getPolNo()+"的暂收已经存在，请确认后重新回盘");
                    continue;
                }

                LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
                // 由于各个银行缴费凭证号可能产生重复，先存入印刷号
                tLJTempFeeClassSchema.setTempFeeNo(tSchema.getPayCode());
                tLJTempFeeClassSchema.setPayMode("4"); // 4:银行转帐
                tLJTempFeeClassSchema.setChequeNo(tSchema.getPayCode());//  缴费凭证号
                tLJTempFeeClassSchema.setPayMoney(tSchema.getPayMoney());
                tLJTempFeeClassSchema.setConfFlag("0");
                tLJTempFeeClassSchema.setSerialNo(serialNo);
                if(tSchema.getComCode().length()==4){
                    tSchema.setComCode(tSchema.getComCode()+"0000");
                    System.out.println("银行转账管理机构："+tSchema.getComCode());
                }
                tLJTempFeeClassSchema.setManageCom(tSchema.getComCode());
                tLJTempFeeClassSchema.setPolicyCom(tSchema.getComCode());
                tLJTempFeeClassSchema.setBankCode(bankCode);
                tLJTempFeeClassSchema.setBankAccNo(tSchema.getAccNo());
                tLJTempFeeClassSchema.setAccName(tSchema.getAccName());
                tLJTempFeeClassSchema.setPayDate(PubFun.getCurrentDate());
                tLJTempFeeClassSchema.setEnterAccDate(tSchema.getBankDealDate());
System.out.print("getEnterAccDate:"+tLJTempFeeClassSchema.getEnterAccDate());
                tLJTempFeeClassSchema.setOperator(inGlobalInput.Operator);
                tLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
                tLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
                tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
                tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
                tLJTempFeeClassSchema.setConfMakeDate(PubFun.getCurrentDate());
                tLJTempFeeClassSchema.setConfMakeTime(PubFun.getCurrentTime());
                tLJTempFeeClassSchema.setInsBankAccNo(insBankAccNo);
                tLJTempFeeClassSchema.setInsBankCode(insBankCode);
                
                LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
                // 由于各个银行缴费凭证号可能产生重复，先存入印刷号
                tLJTempFeeSchema.setTempFeeNo(tSchema.getPayCode());
                tLJTempFeeSchema.setTempFeeType("11"); 
                tLJTempFeeSchema.setRiskCode("000000"); 
                tLJTempFeeSchema.setPayIntv("0");
                tLJTempFeeSchema.setOtherNo(tSchema.getPolNo());
                tLJTempFeeSchema.setOtherNoType("4");
                tLJTempFeeSchema.setPayMoney(tSchema.getPayMoney());
                tLJTempFeeSchema.setConfFlag("0");
                tLJTempFeeSchema.setSerialNo(serialNo);
                if(tSchema.getComCode().length()==4){
                    tSchema.setComCode(tSchema.getComCode()+"0000");
                    System.out.println("银行转账管理机构："+tSchema.getComCode());
                }
                tLJTempFeeSchema.setManageCom(tSchema.getComCode());
                tLJTempFeeSchema.setPolicyCom(tSchema.getComCode());
                tLJTempFeeSchema.setPayDate(PubFun.getCurrentDate());
               // tLJTempFeeSchema.setEnterAccDate(PubFun.getCurrentDate());
                tLJTempFeeSchema.setEnterAccDate(tSchema.getBankDealDate());
                tLJTempFeeSchema.setOperator(inGlobalInput.Operator);
                tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
                tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
                tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
                tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
                tLJTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
                tLJTempFeeSchema.setConfMakeTime(PubFun.getCurrentTime());
                // 验证是否已录单，如已录单则执行签单操作
                // 通过印刷号，缴费凭证号来查询是否已录单
                String tempFeeNo = StrTool.cTrim(tSchema.getPayCode());
                String prtNo = StrTool.cTrim(tSchema.getPolNo());
                if (tempFeeNo.equals("") && prtNo.equals(""))
                {
                    CError.buildErr(this, "印刷号或缴费凭证号为空！");
                    return false;
                }

                if (!prtNo.equals(""))
                {
                    tLCContDB.setPrtNo(tSchema.getPolNo());
                }
                LCContSet tLCContSet = tLCContDB.query();
                if (tLCContSet != null && tLCContSet.size() > 0)
                {
                    // 如果LCCont中的tempFeeNo与回盘文件中的不符，则不允许回盘
                    if (!tempFeeNo.equals(tLCContSet.get(1).getTempFeeNo()))
                    {
                        if (tLCContSet.get(1).getTempFeeNo() == null)
                        {
                            if (tResult1.equals(""))
                            {
                                tResult1 = tResult1
                                        + tLCContSet.get(1).getPrtNo();
                            }
                            else
                            {
                                tResult1 = tResult1 + "、"
                                        + tLCContSet.get(1).getPrtNo();
                            }
                            continue;
                        }
                        System.out.println("回盘文件中的缴费凭证号：" + tempFeeNo
                                + ";录单时录入的缴费凭证号："
                                + tLCContSet.get(1).getTempFeeNo());
                        CError.buildErr(this, "回盘文件中的缴费凭证号：" + tempFeeNo
                                + "与录单时录入的不符！");
                        return false;
                    }
                    //                  如果LCCont中的该保单已经签单，则不允许回盘
                    if (tLCContSet.get(1).getAppFlag() != null
                            && tLCContSet.get(1).getAppFlag().equals("1"))
                    {
                        if (tResult.equals(""))
                        {
                            tResult = tResult + tLCContSet.get(1).getPrtNo();
                        }
                        else
                        {
                            tResult = tResult + "、"
                                    + tLCContSet.get(1).getPrtNo();
                        }
                        continue;
                    }

                    if (tLCContSet.get(1).getCardFlag() != null
                            && !"0".equals(tLCContSet.get(1).getCardFlag())
                            && "1".equals(tLCContSet.get(1).getContType()))
                    {
                        // 校验金额是否相等
                        double payMoney = tSchema.getPayMoney();
                        double sumPrem = tLCContSet.get(1).getPrem();
                        if (Math.abs(payMoney - sumPrem) > 0.0001)
                        {
                            if (tResult3.equals(""))
                            {
                                tResult3 = tResult3
                                        + StrTool.cTrim(tSchema.getPolNo());
                            }
                            else
                            {
                                tResult3 = tResult3 + "、"
                                        + StrTool.cTrim(tSchema.getPolNo());
                            }
                            System.out.println("回盘金额：" + payMoney + ";应缴金额："
                                    + sumPrem);
                            continue;
                        }
                        signContFlag = true;
                        // 更新暂收表
                        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
                        tLJTempFeeDB.setTempFeeNo(tSchema.getPolNo());
                        tLJTempFeeDB.setTempFeeType("11");
                        LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.query();
                        if (tLJTempFeeSet != null && tLJTempFeeSet.size() > 0)
                        {
                            String tResultss = "暂收号是：" + tSchema.getPayCode() + "的保单无法做回盘操作，原因是该暂收号在暂收表已经存在";
                            CError.buildErr(this, tResultss);
                        }
                    }

                    // 签单数据准备
                    // signLCContSet.add(tLCContSet.get(1));
                }// 对已录单的情况处理完毕
                outLJTempFeeClassSet.add(tLJTempFeeClassSchema);
                outLJTempFeeSet.add(tLJTempFeeSchema);
            }
            if (!tResult.equals(""))
            {
                tResult = "印刷号是：" + tResult + "的保单无法做回盘操作，原因已经签单";
                CError.buildErr(this, tResult);
            }
            if (!tResult1.equals(""))
            {
                tResult1 = "印刷号是：" + tResult1 + "的保单无法做回盘操作，原因已经做过录单,但没有录入缴费凭证号";
                CError.buildErr(this, tResult1);
            }
            if (!tResult2.equals(""))
            {
                tResult2 = "印刷号是：" + tResult2 + "的保单无法做回盘操作，原因暂收数据已经存在";
                CError.buildErr(this, tResult2);
            }
            if (!tResult3.equals(""))
            {
                tResult3 = "印刷号是：" + tResult3 + "的保单无法做回盘操作，原因回盘金额与应缴金额不符";
                CError.buildErr(this, tResult3);
            }
            if (!tResult.equals("") || !tResult1.equals("")
                    || !tResult2.equals("") || !tResult3.equals(""))
            {
                return false;
            }
            bakLJTempFeeClassSet.add(outLJTempFeeClassSet);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CError.buildErr(this, "备份数据到暂收分类备份表失败！具体原因是：" + e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * 对一个保单进行签单动作
     * @param tLCContSchema
     * @return
     */
    private boolean signSingleCont(LCContSchema tLCContSchema)
    {
        //签单开始
        String sql = " select distinct a.missionprop1,a.missionprop2,a.missionprop4,a.missionid,a.submissionid ";
        sql += " from lwmission a,lccont b,ljtempfee c  ";
        sql += "where (a.processid = '0000000007' and a.activityid = '0000007002' or a.processid = '0000000009' and a.activityid = '0000009004') ";
        sql += " and a.missionprop2=b.prtno and a.MissionProp2=c.otherno and c.othernotype='4'  and c.confflag='0' ";
        sql += " and a.missionprop2='" + tLCContSchema.getPrtNo() + "' ";
        sql += " and a.MissionProp3 like '86%' fetch first 3000 rows only";
        System.out.println("ExecSQL:" + sql);
        LWMissionDB tLWMissionDB = new LWMissionDB();
        LWMissionSet tLWMissionSet = tLWMissionDB.executeQuery(sql);

        if (tLWMissionSet == null || tLWMissionSet.size() == 0)
        {
            CError.buildErr(this, "找不到工作流记录！");
            return false;
        }
        String tMissionID = tLWMissionSet.get(1).getMissionID();
        String tSubMissionID = tLWMissionSet.get(1).getSubMissionID();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("TemplatePath", "");
        tTransferData.setNameAndValue("OutXmlPath", "");
        tTransferData.setNameAndValue("MissionID", tMissionID);
        tTransferData.setNameAndValue("SubMissionID", tSubMissionID);
        VData data = new VData();
        data.add(tLCContSchema);
        data.add(tTransferData);
        data.add(inGlobalInput);
        BriefTbWorkFlowUI tBriefTbWorkFlowUI = new BriefTbWorkFlowUI();
        boolean bl = tBriefTbWorkFlowUI.submitData(data, "0000007002");
        int n = tBriefTbWorkFlowUI.mErrors.getErrorCount();
        String Content = "";
        if (n == 0)
        {
            if (!bl)
            {
                Content = "签单失败";
                CError.buildErr(this, Content);
                return false;
            }
        }
        else
        {
            String strErr = "";
            for (int m = 0; m < n; m++)
            {
                strErr += (m + 1) + ": "
                        + tBriefTbWorkFlowUI.mErrors.getError(m).errorMessage
                        + "; ";
                System.out
                        .println(tBriefTbWorkFlowUI.mErrors.getError(m).errorMessage);
            }
            if (!bl)
            {
                Content = "集体投保单签单失败，原因是: " + strErr;
                CError.buildErr(this, Content);
                return false;
            }
        } // end of if
        return true;
    }

    /**
     * 将所有的回盘对应的保单进行签单
     * @return
     */
    private boolean signConts()
    {
        if (signLCContSet.size() > 0)
        {
            for (int i = 1; i <= signLCContSet.size(); i++)
            {
                boolean flag = signSingleCont(signLCContSet.get(i));
                if (!flag)
                {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 签单成功后，备份数据到B表
     * @return
     */
    private boolean bakDataToTempFeeClassB()
    {
        System.out.println("Into ReadReturnFileBL.bakDataToTempFeeClassB()...");
        try
        {
            System.out.println("Line 771,LJTempFeeClassSet size is:"
                    + outLJTempFeeClassSet.size());
            if (outLJTempFeeClassSet == null
                    || outLJTempFeeClassSet.size() == 0)
            {
                System.out.println("Line 742,LJTempFeeClassSet is null!");
                return false;
            }
            String aSeqNo = "";
            Reflections tReflections = new Reflections();
            for (int i = 1; i <= outLJTempFeeClassSet.size(); i++)
            {
                LJTempFeeClassBSchema tLJTempFeeClassBSchema = new LJTempFeeClassBSchema();
                tReflections.transFields(tLJTempFeeClassBSchema,
                        outLJTempFeeClassSet.get(i));
                aSeqNo = PubFun1.CreateMaxNo("TEMPFEESEQNO", "");
                tLJTempFeeClassBSchema.setSeqNo(aSeqNo);
                outLJTempFeeClassBSet.add(tLJTempFeeClassBSchema);
                System.out.println("KeyWord ===>"
                        + outLJTempFeeClassBSet.get(1).getSeqNo());
            }

            if (outLJTempFeeClassBSet == null
                    || outLJTempFeeClassBSet.size() == 0)
            {
                System.out
                        .println("Line 788,从LJTempFeeClasss到LJTempFeeClasssB失败！");
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            String Content = "备份数据到暂收分类备份表失败，具体原因是: " + e.getMessage();
            CError.buildErr(this, Content);
            return false;
        }
        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     *
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        String sql = "select * from ljtempfeeclass where tempfeeno = '120004100200'";
        LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
        LJTempFeeClassSet tLJTempFeeClassSet = tLJTempFeeClassDB
                .executeQuery(sql);
        System.out.println("----" + tLJTempFeeClassSet.size());
        Reflections tReflections = new Reflections();
        LJTempFeeClassBSet tLJTempFeeClassBSet = new LJTempFeeClassBSet();
        try
        {
            for (int i = 1; i <= tLJTempFeeClassSet.size(); i++)
            {
                LJTempFeeClassBSchema aSchema = new LJTempFeeClassBSchema();
                tReflections.transFields(aSchema, tLJTempFeeClassSet.get(i));
                aSchema.setSeqNo(i + "");
                tLJTempFeeClassBSet.add(aSchema);
            }

            String tempfeeNo = tLJTempFeeClassBSet.get(1).getTempFeeNo();
            System.out.println("-----ss--" + tempfeeNo);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
