package com.sinosoft.lis.brieftb;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LDPersonDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LDPersonSchema;
import com.sinosoft.lis.tb.ContInsuredBL;
import com.sinosoft.lis.vschema.LCCustomerImpartSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LBInsuredListSchema;
import com.sinosoft.lis.db.LBInsuredListDB;
import com.sinosoft.lis.vschema.LBInsuredListSet;
import com.sinosoft.lis.db.LOBGrpContDB;
import com.sinosoft.lis.schema.LCInsuredListSchema;


/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class BriefInsuredModifyBL {
    /** 错误封装对象 */
    public CErrors mErrors = new CErrors();
    /** 操作符 */
    private String mOperate;
    /** 传入封装对象 */
    private VData mInputData;
    /** 被保人信息 */
    private LCInsuredSchema mLCInsuredSchema = new LCInsuredSchema();
    /** 登陆信息 */
    private GlobalInput mGlobalInput;
    /** 当前要修改的被保人的ContNo */
    private String mContNo;
    /** 客户信息 */
    private LDPersonSchema mLDPersonSchema = new LDPersonSchema();
    /** 个人合同信息 */
    private LCContSchema mLCContSchema;
    /** LBInsuredList */
    private LBInsuredListSchema mLBInsuredListSchema;
    /** 判断是否备份 */
    private String mLoadFlag;
    private LCInsuredListSchema mLCInsuredListSchema;
    public BriefInsuredModifyBL() {
    }

    /**
     * submitData
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.mInputData = cInputData;
        this.mOperate = cOperate;

        if (!getInputData()) {
            return false;
        }

        /** 开始校验数据 */
        if (!checkData()) {
            return false;
        }

        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        /** 五项基本信息 */
        String mInsuredName = mLCInsuredSchema.getName();
        String mInsuredSex = mLCInsuredSchema.getSex();
        String mInsuredBirthday = mLCInsuredSchema.getBirthday();
        String mInsuredIDType = mLCInsuredSchema.getIDType();
        String mInsuredIDNo = mLCInsuredSchema.getIDNo();
        String mOthIDNo = mLCInsuredSchema.getOthIDNo();
        String mOthIDType = mLCInsuredSchema.getOthIDType();
        String mEnglishName = mLCInsuredSchema.getEnglishName();
        /** 校验是否应该换号 */
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setContNo(this.mContNo);
        tLCInsuredDB.setName(mInsuredName);
        tLCInsuredDB.setBirthday(mInsuredBirthday);
        tLCInsuredDB.setIDType(mInsuredIDType);
        tLCInsuredDB.setIDNo(mInsuredIDNo);
        tLCInsuredDB.setSex(mInsuredSex);
        /** 判断是否生成客户信息 */
        boolean needCreat = false;
        LCInsuredSet tLCInsuredSet = tLCInsuredDB.query();
        if (tLCInsuredSet == null || tLCInsuredSet.size() <= 0) {
            needCreat = true;
        } else if (tLCInsuredSet.size() == 1) {
            LDPersonDB tLDPersonDB = new LDPersonDB();
            tLDPersonDB.setCustomerNo(tLCInsuredSet.get(1).getInsuredNo());
            tLCInsuredDB.setInsuredNo(tLCInsuredSet.get(1).getInsuredNo());
            if (!tLDPersonDB.getInfo()) {
                buildError("dealData", "被保人基本信息没有发生变化,但根据客户号码查询失败！");
                return false;
            }
            this.mLDPersonSchema = tLDPersonDB.getSchema();
            needCreat = false;
        } else {
            buildError("dealData", "出现同一合同下存在相同被保险人！");
            return false;
        }
        if (needCreat) {
//            mLCInsuredSchema.setInsuredNo(null);
            mLDPersonSchema.setName(mInsuredName);
            mLDPersonSchema.setBirthday(mInsuredBirthday);
            mLDPersonSchema.setIDType(mInsuredIDType);
            mLDPersonSchema.setIDNo(mInsuredIDNo);
            if (!StrTool.cTrim(mInsuredIDNo).equals("") &&
                StrTool.cTrim(mInsuredIDType).equals("")) {
                mInsuredIDType = "0";
            } else {
                mInsuredIDType = null;
            }
            mLDPersonSchema.setIDType(mInsuredIDType);
            mLDPersonSchema.setSex(mInsuredSex);
            mLDPersonSchema.setEnglishName(mEnglishName);
            if (!StrTool.cTrim(mOthIDNo).equals("") &&
                StrTool.cTrim(mOthIDType).equals("")) {
                mOthIDType = "0";
            } else {
                mOthIDType = null;
            }
            mLDPersonSchema.setOthIDNo(mOthIDNo);
            mLDPersonSchema.setOthIDType(mOthIDType);
        }
        VData tInputData = new VData();
        tLCInsuredDB.setSchema(mLCInsuredSchema);
        tLCInsuredDB.setGrpContNo(mLCContSchema.getGrpContNo());
        LCCustomerImpartSet impartSet = new LCCustomerImpartSet();
        TransferData transferData = new TransferData();
        transferData.setNameAndValue("FamilyType", "0");
        transferData.setNameAndValue("DiskImportFlag", "1");
        transferData.setNameAndValue("ContType", "2");
        transferData.setNameAndValue("PolTypeFlag", mLCContSchema.getPolType());
        transferData.setNameAndValue("InsuredPeoples",
                                     String.valueOf(mLCContSchema.getPeoples()));
        transferData.setNameAndValue("LoadFlag", this.mLoadFlag);
        transferData.setNameAndValue("GrpNo", mLCContSchema.getAppntNo());
        tInputData.add(mGlobalInput);
        tInputData.add(mLCContSchema);
        tInputData.add(this.mLCInsuredSchema);
        tInputData.add(tLCInsuredDB);
        LCAddressSchema inLCAddressSchema = new LCAddressSchema();
        tInputData.add(inLCAddressSchema);
        tInputData.add(mLDPersonSchema);
        tInputData.add(impartSet);
        tInputData.add(transferData);

        //提交生成或得到团单下个单合同
        ContInsuredBL contInsuredBl = new ContInsuredBL();

        boolean cRes = contInsuredBl.preparesubmitData(tInputData,
                "UPDATE||CONTINSURED");
        if (!cRes) {
            this.mErrors.copyAllErrors(contInsuredBl.mErrors);
            return false;
        }
        VData tResult = contInsuredBl.getResult();
        LCInsuredSchema tLCInsuredSchema =
                (LCInsuredSchema) tResult.getObjectByObjectName(
                        "LCInsuredSchema", 0);
        mLBInsuredListSchema.setInsuredNo(tLCInsuredSchema.getInsuredNo());
        mLBInsuredListSchema.setInsuredName(mInsuredName);
        mLBInsuredListSchema.setBirthday(mInsuredBirthday);
        mLBInsuredListSchema.setIDType(mInsuredIDType);
        mLBInsuredListSchema.setIDNo(mInsuredIDNo);
        mLBInsuredListSchema.setSex(mInsuredSex);
        mLBInsuredListSchema.setOthIDType(mOthIDType);
        mLBInsuredListSchema.setOthIDNo(mOthIDNo);
        mLBInsuredListSchema.setEnglishName(mEnglishName);

        MMap map = (MMap) tResult.getObjectByObjectName("MMap", 0);
        map.put(mLBInsuredListSchema, "UPDATE");
        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(tResult, null)) {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        return true;
    }


    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {

        System.out.println("开始进行数据校验_BriefInsuredModifyBL");

        if (this.mLCInsuredListSchema == null) {
            buildError("checkData", "传入被保人信息为null！");
            return false;
        }
//        this.mLCInsuredSchema.set(mLCInsuredListSchema.getContNo());
//        this.mLCInsuredSchema.setInsuredNo(mLCInsuredListSchema.getInsuredNo());
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        LCInsuredSet tLCInsuredSet = tLCInsuredDB.executeQuery("select * from lcinsured where contno=(select contno from lccont where proposalcontno='" +
                mLCInsuredListSchema.getContNo() + "')");
        if (tLCInsuredSet == null || tLCInsuredSet.size() <= 0) {
            buildError("checkData", "没有查询到该被保人信息！");
            return false;
        }
        if (tLCInsuredSet.size() > 1) {
            buildError("checkData", "没有查询到该被保人信息！");
            return false;
        }

        mLCInsuredSchema.setSchema(tLCInsuredSet.get(1));
        mLCInsuredSchema.setName(mLCInsuredListSchema.getInsuredName());
        mLCInsuredSchema.setSex(mLCInsuredListSchema.getSex());
        mLCInsuredSchema.setBirthday(mLCInsuredListSchema.getBirthday());
        mLCInsuredSchema.setIDType(mLCInsuredListSchema.getIDType());
        mLCInsuredSchema.setOthIDType(mLCInsuredListSchema.getOthIDType());
        mLCInsuredSchema.setIDNo(mLCInsuredListSchema.getIDNo());
        mLCInsuredSchema.setOthIDNo(mLCInsuredListSchema.getOthIDNo());
        mLCInsuredSchema.setEnglishName(mLCInsuredListSchema.getEnglishName());

        if (this.mGlobalInput == null) {
            buildError("checkData", "传入登陆信息为null,原因是页面已超时,请重新登陆！");
            return false;
        }

        mContNo = mLCInsuredSchema.getContNo();

        if (mContNo == null || mContNo.equals("")) {
            buildError("checkData", "传入合同号码为null！");
            return false;
        }

        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mContNo);

        if (!tLCContDB.getInfo()) {
            buildError("checkData", "传入合同号码系统中不存在！");
            return false;
        }
        mLCContSchema = tLCContDB.getSchema();
        LBInsuredListDB tLBInsuredListDB = new LBInsuredListDB();
        tLBInsuredListDB.setGrpContNo(mLCContSchema.getGrpContNo());
        tLBInsuredListDB.setContNo(mLCContSchema.getProposalContNo());
        LBInsuredListSet tLBInsuredListSet = tLBInsuredListDB.query();
        if (tLBInsuredListSet == null || tLBInsuredListSet.size() <= 0) {
            buildError("checkData", "查询被保人清单失败！");
            return false;
        }
        if (tLBInsuredListSet.size() > 1) {
            buildError("checkData", "查询被保人清单多于1条！");
            return false;
        }
        this.mLBInsuredListSchema = tLBInsuredListSet.get(1);
        LOBGrpContDB tLOBGrpContDB = new LOBGrpContDB();
        tLOBGrpContDB.setGrpContNo(mLCContSchema.getGrpContNo());
        if (tLOBGrpContDB.getCount() <= 0) {
            mLoadFlag = "2";
        } else {
            mLoadFlag = null;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        mLCInsuredListSchema = (LCInsuredListSchema) mInputData.
                               getObjectByObjectName(
                                       "LCInsuredListSchema", 0);
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "BriefInsuredModifyBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

    /**
     *
     * @param args String[]
     */
    public static void main(String[] args) {
        String mContNo = "13000083776";
        String mInsuredNo = "000041268";
        String mInsuredName = "董建斌2";
        String mInsuredSex = "0";
        String mInsuredBirthday = "1963-07-15";
        String mInsuredIDType = "";
        String mInsuredIDNo = "";
        String mOthIDNo = "p.2342342";
        String mOthIDType = "";
        String mEnglishName = "dongjb2";
        VData v = new VData();
        GlobalInput g = new GlobalInput();
        g.ManageCom = "86110000";
        g.ComCode = "86110000";
        g.Operator = "group";

        LCInsuredListSchema l = new LCInsuredListSchema();
        l.setContNo(mContNo);
        l.setInsuredNo(mInsuredNo);
        l.setInsuredName(mInsuredName);
        l.setSex(mInsuredSex);
        l.setBirthday(mInsuredBirthday);
        l.setIDType(mInsuredIDType);
        l.setIDNo(mInsuredIDNo);
        l.setOthIDNo(mOthIDNo);
        l.setOthIDType(mOthIDType);
        l.setEnglishName(mEnglishName);
        l.setContPlanCode("00");
        v.add(l);
        v.add(g);

        BriefInsuredModifyBL b = new BriefInsuredModifyBL();
        try {
            b.submitData(v, null);
        } catch (Exception ex) {
            System.out.println("发生异常错误" + ex.getMessage());
        }
        CErrors e = b.mErrors;
        if (e.needDealError()) {
            System.out.println(e.getErrContent());
        }
    }
}
