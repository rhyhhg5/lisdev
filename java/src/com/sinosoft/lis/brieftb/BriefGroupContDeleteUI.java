package com.sinosoft.lis.brieftb;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

/**
 *
 * <p>Title: 简易投保删除操作</p>
 *
 * <p>Description: 如果在合同维护中,进行了需要重打保单的操作则把保单移到备份表 </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author YangMing
 * @version 1.0
 */
public class BriefGroupContDeleteUI {
    public BriefGroupContDeleteUI() {
    }

    /**
     * 全局数据变量
     */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    public boolean submitData(VData nInputData, String cOperate) {
        BriefGroupContDeleteBL tBriefGroupContDeleteBL = new BriefGroupContDeleteBL();
        if (!tBriefGroupContDeleteBL.submitData(nInputData, cOperate)) {
            this.mErrors.copyAllErrors(tBriefGroupContDeleteBL.mErrors);
            return false;
        } else {
            mResult = tBriefGroupContDeleteBL.getResult();
        }
        return true;
    }

    /**
     * 操作结果
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }
}
