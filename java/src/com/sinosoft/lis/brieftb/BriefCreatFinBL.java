package com.sinosoft.lis.brieftb;

import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LJSPayBDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LJSPayGrpBDB;
import com.sinosoft.lis.db.LJSPayGrpDB;
import com.sinosoft.lis.db.LJSPayPersonBDB;
import com.sinosoft.lis.db.LJSPayPersonDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LJSPayBSet;
import com.sinosoft.lis.vschema.LJSPayGrpBSet;
import com.sinosoft.lis.vschema.LJSPayGrpSet;
import com.sinosoft.lis.vschema.LJSPayPersonBSet;
import com.sinosoft.lis.vschema.LJSPayPersonSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.vschema.LJAPaySet;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.db.LJAPayGrpDB;
import com.sinosoft.lis.db.LJAPayPersonDB;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class BriefCreatFinBL {
    /** 错误封装对象 */
    public CErrors mErrors = new CErrors();
    /** 全局操作符 */
    private String mOperate;
    /** 传入数据 */
    private VData mInputData;
    /** 传入参数 */
    private TransferData mTransferData;
    /** 登陆信息 */
    private GlobalInput mGlobalInput;
    /** 保单机构 */
    private String mManageCom;
    /** 保单代理机构 */
    private String mAgentCom;
    /** 抽档开始时间 */
    private String mStartDate;
    /** 抽档结束时间 */
    private String mEndDate;
    /** 应收*/
    private String mGetNoticeNo;
    /** 实收 */
    private String mOtherNo;
    /** 应收记录 */
    private LJSPaySet mLJSPaySet = new LJSPaySet();
    private LJSPayBSet mLJSPayBSet = new LJSPayBSet();
    private LJSPayGrpSet mLJSPayGrpSet = new LJSPayGrpSet();
    private LJSPayGrpBSet mLJSPayGrpBSet = new LJSPayGrpBSet();
    private LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet();
    private LJSPayPersonBSet mLJSPayPersonBSet = new LJSPayPersonBSet();
    /** 实收 */
    private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();
    private LJAPayGrpSet mLJAPayGrpSet = new LJAPayGrpSet();
    private LJAPaySet mLJAPaySet = new LJAPaySet();

    /** 打印管理表 */
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    /** 递交数据 */
    private MMap map = new MMap();
    /** 最终结果 */
    private VData mResult = new VData();
    public BriefCreatFinBL() {
    }

    /**
     * submitData
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        mInputData = cInputData;
        mOperate = cOperate;

        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(mResult, null)) {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        System.out.println("准备封装数据递交");
        map.put(this.mLJSPaySet, "UPDATE");
        map.put(this.mLJSPayBSet, "UPDATE");
        map.put(this.mLJSPayGrpSet, "UPDATE");
        map.put(this.mLJSPayGrpBSet, "UPDATE");
        map.put(this.mLJSPayPersonSet, "UPDATE");
        map.put(this.mLJSPayPersonBSet, "UPDATE");
        map.put(this.mLOPRTManagerSchema, "INSERT");
        map.put(this.mLJAPayPersonSet, "UPDATE");
        map.put(this.mLJAPayGrpSet, "UPDATE");
        map.put(this.mLJAPaySet, "UPDATE");
        mResult.add(map);
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        /** 查询应收数据，生成批次 */
        System.out.println("生成批次信息。。。。。");
        if (!fillLJSPay()) {
            return false;
        }
        /** 生成批次信息 */
        System.out.println("生成批次信息......");
        if (!dealBatchNo()) {
            return false;
        }
        return true;
    }

    /**
     * dealBatchNo
     *
     * @return boolean
     */
    private boolean dealBatchNo() {

        String tBatchNo = PubFun1.CreateMaxNo("PRTSEQNO", null);
        if (tBatchNo == null || tBatchNo.indexOf("null") != -1 ||
            tBatchNo.equals("")) {
            buildError("dealBatchNo", "生成批次号码失败！");
            return false;
        }

        for (int i = 1; i <= mLJSPaySet.size(); i++) {
            mLJSPaySet.get(i).setSerialNo(tBatchNo);
            mLJSPaySet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLJSPaySet.get(i).setModifyTime(PubFun.getCurrentTime());
            mLJSPaySet.get(i).setOperator(mGlobalInput.Operator);
        }
        for (int i = 1; i <= mLJSPayBSet.size(); i++) {
            mLJSPayBSet.get(i).setSerialNo(tBatchNo);
            mLJSPayBSet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLJSPayBSet.get(i).setModifyTime(PubFun.getCurrentTime());
            mLJSPayBSet.get(i).setOperator(mGlobalInput.Operator);
        }
        for (int i = 1; i <= mLJSPayGrpSet.size(); i++) {
            mLJSPayGrpSet.get(i).setSerialNo(tBatchNo);
            mLJSPayGrpSet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLJSPayGrpSet.get(i).setModifyTime(PubFun.getCurrentTime());
            mLJSPayGrpSet.get(i).setOperator(mGlobalInput.Operator);
        }
        for (int i = 1; i <= mLJSPayGrpBSet.size(); i++) {
            mLJSPayGrpBSet.get(i).setSerialNo(tBatchNo);
            mLJSPayGrpBSet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLJSPayGrpBSet.get(i).setModifyTime(PubFun.getCurrentTime());
            mLJSPayGrpBSet.get(i).setOperator(mGlobalInput.Operator);
        }
        for (int i = 1; i <= mLJSPayPersonSet.size(); i++) {
            mLJSPayPersonSet.get(i).setSerialNo(tBatchNo);
            mLJSPayPersonSet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLJSPayPersonSet.get(i).setModifyTime(PubFun.getCurrentTime());
            mLJSPayPersonSet.get(i).setOperator(mGlobalInput.Operator);
        }
        for (int i = 1; i <= mLJSPayPersonBSet.size(); i++) {
            mLJSPayPersonBSet.get(i).setSerialNo(tBatchNo);
            mLJSPayPersonBSet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLJSPayPersonBSet.get(i).setModifyTime(PubFun.getCurrentTime());
            mLJSPayPersonBSet.get(i).setOperator(mGlobalInput.Operator);
        }
        for (int i = 1; i <= mLJAPaySet.size(); i++) {
            mLJAPaySet.get(i).setSerialNo(tBatchNo);
            mLJAPaySet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLJAPaySet.get(i).setModifyTime(PubFun.getCurrentTime());
            mLJAPaySet.get(i).setOperator(mGlobalInput.Operator);
        }
        for (int i = 1; i <= mLJAPayGrpSet.size(); i++) {
            mLJAPayGrpSet.get(i).setSerialNo(tBatchNo);
            mLJAPayGrpSet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLJAPayGrpSet.get(i).setModifyTime(PubFun.getCurrentTime());
            mLJAPayGrpSet.get(i).setOperator(mGlobalInput.Operator);
        }
        for (int i = 1; i <= mLJAPayPersonSet.size(); i++) {
            mLJAPayPersonSet.get(i).setSerialNo(tBatchNo);
            mLJAPayPersonSet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLJAPayPersonSet.get(i).setModifyTime(PubFun.getCurrentTime());
            mLJAPayPersonSet.get(i).setOperator(mGlobalInput.Operator);
        }

        mLOPRTManagerSchema.setPrtSeq(tBatchNo);
        mLOPRTManagerSchema.setOtherNo(tBatchNo);
        mLOPRTManagerSchema.setOtherNoType("06");
        mLOPRTManagerSchema.setCode("15");
        mLOPRTManagerSchema.setManageCom(this.mManageCom);
        mLOPRTManagerSchema.setReqCom(mGlobalInput.ManageCom);
        mLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
        mLOPRTManagerSchema.setExeCom(this.mManageCom);
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        mLOPRTManagerSchema.setStandbyFlag1(mLJSPaySet.get(1).getAgentCom());

        return true;
    }

    /**
     * dealLJSPay
     *
     * @return boolean
     */
    private boolean fillLJSPay() {
        String wherePart = "where managecom='" +
                           this.mManageCom + "' and agentcom='" +
                           this.mAgentCom + "' and paydate>='" +
                           this.mStartDate + "' and paydate<='" +
                           this.mEndDate + "' and OtherNoType='14' "
                           + " and SerialNo is null and getnoticeno in "+
                           this.mGetNoticeNo;

        String sql_LJSPay = "select * from ljspay " + wherePart;
        LJSPayDB tLJSPayDB = new LJSPayDB();
        System.out.println("Sql : " + sql_LJSPay);
        mLJSPaySet = tLJSPayDB.executeQuery(sql_LJSPay);
        if (mLJSPaySet == null || mLJSPaySet.size() <= 0) {
            buildError("dealLJSPay", "没有待抽档的数据！");
            return false;
        }

        String sql_LJSPayB = "select * from ljspayB " + wherePart;
        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        System.out.println("Sql : " + sql_LJSPayB);
        mLJSPayBSet = tLJSPayBDB.executeQuery(sql_LJSPayB);
        if (mLJSPayBSet == null || mLJSPayBSet.size() <= 0) {
            buildError("dealLJSPay", "没有待抽档的数据！");
            return false;
        }

        if (mLJSPayBSet.size() != mLJSPaySet.size()) {
            buildError("dealLJSPay", "应收数据和应收备份数据不同,请检查！");
            return false;
        }

        String sql_LJSPayGrp = "select * from ljspayGrp "
                               + "where GetNoticeNo in ("
                               + "select GetNoticeNo from ljspay " + wherePart
                               + ")";
        LJSPayGrpDB tLJSPayGrpDB = new LJSPayGrpDB();
        System.out.println("Sql : " + sql_LJSPayGrp);
        mLJSPayGrpSet = tLJSPayGrpDB.executeQuery(sql_LJSPayGrp);
        if (mLJSPayGrpSet == null || mLJSPayGrpSet.size() <= 0) {
            buildError("dealLJSPay", "没有待抽档的数据！");
            return false;
        }

        String sql_LJSPayGrpB = "select * from ljspayGrpB "
                                + "where GetNoticeNo in ("
                                + "select GetNoticeNo from ljspayB "
                                + wherePart
                                + ")";
        LJSPayGrpBDB tLJSPayGrpBDB = new LJSPayGrpBDB();
        System.out.println("Sql : " + sql_LJSPayGrpB);
        mLJSPayGrpBSet = tLJSPayGrpBDB.executeQuery(sql_LJSPayGrpB);
        if (mLJSPayGrpBSet == null || mLJSPayGrpBSet.size() <= 0) {
            buildError("dealLJSPay", "没有待抽档的数据！");
            return false;
        }
        if (mLJSPayGrpBSet.size() != mLJSPayGrpSet.size()) {
            buildError("dealLJSPay", "应收数据和应收备份数据不同,请检查LJSPayGrp！");
            return false;
        }

        String sql_LJSPayPerson = "select * from ljspayperson "
                                  + "where GetNoticeNo in ("
                                  + "select GetNoticeNo from ljspay "
                                  + wherePart
                                  + ")";
        LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
        System.out.println("Sql : " + sql_LJSPayPerson);
        mLJSPayPersonSet = tLJSPayPersonDB.executeQuery(sql_LJSPayPerson);
        if (mLJSPayPersonSet == null || mLJSPayPersonSet.size() <= 0) {
            buildError("dealLJSPay", "没有待抽档的数据！");
            return false;
        }
        String sql_LJSPayPersonB = "select * from ljspaypersonB "
                                   + "where GetNoticeNo in ("
                                   + "select GetNoticeNo from ljspayB "
                                   + wherePart
                                   + ")";
        LJSPayPersonBDB tLJSPayPersonBDB = new LJSPayPersonBDB();
        System.out.println("Sql : " + sql_LJSPayPersonB);
        mLJSPayPersonBSet = tLJSPayPersonBDB.executeQuery(sql_LJSPayPersonB);
        if (mLJSPayPersonBSet == null || mLJSPayPersonBSet.size() <= 0) {
            buildError("dealLJSPay", "没有待抽档的数据！");
            return false;
        }
        if (mLJSPayGrpBSet.size() != mLJSPayGrpSet.size()) {
            buildError("dealLJSPay", "应收数据和应收备份数据不同,请检查LJSPayPerson！");
            return false;
        }

        /** 生成实收数据 */
        String wherePart_LJAPay = "where managecom='" +
                                  this.mManageCom + "' and agentcom='" +
                                  this.mAgentCom + "' and paydate>='" +
                                  this.mStartDate + "' and paydate<='" +
                                  this.mEndDate + "' and InComeType='14' "
                                  + " and SerialNo is null and incomeno in "+
                                  this.mOtherNo;

        String sql_LJAPay = "select * from ljApay " + wherePart_LJAPay;
        LJAPayDB tLJAPayDB = new LJAPayDB();
        System.out.println("Sql : " + sql_LJAPay);
        mLJAPaySet = tLJAPayDB.executeQuery(sql_LJAPay);
        if (mLJAPaySet == null || mLJAPaySet.size() <= 0) {
            buildError("dealLJSPay", "没有待抽档的数据！");
            return false;
        }
        if (mLJAPaySet.size() != mLJSPaySet.size()) {
            buildError("dealLJSPay", "应收数据和应收备份数据不同,请检查LJAPay！");
            return false;
        }
        String sql_LJAPayGrp = "select * from ljapayGrp "
                               + "where PayNo in ("
                               + "select PayNo from ljapay "
                               + wherePart_LJAPay
                               + ")";
        LJAPayGrpDB tLJAPayGrpDB = new LJAPayGrpDB();
        System.out.println("Sql : " + sql_LJAPayGrp);
        mLJAPayGrpSet = tLJAPayGrpDB.executeQuery(sql_LJAPayGrp);
        if (mLJAPayGrpSet == null || mLJAPayGrpSet.size() <= 0) {
            buildError("dealLJSPay", "没有待抽档的数据！");
            return false;
        }
        if (mLJAPayGrpSet.size() != mLJSPayGrpSet.size()) {
            buildError("dealLJSPay", "应收数据和应收备份数据不同,请检查！LJAPayGrp");
            return false;
        }

        String sql_LJAPayPerson = "select * from ljapayPerson "
                                  + "where PayNo in ("
                                  + "select PayNo from ljapay "
                                  + wherePart_LJAPay
                                  + ")";
        LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
        System.out.println("Sql : " + sql_LJAPayPerson);
        mLJAPayPersonSet = tLJAPayPersonDB.executeQuery(sql_LJAPayPerson);
        if (mLJAPayPersonSet == null || mLJAPayPersonSet.size() <= 0) {
            buildError("dealLJSPay", "没有待抽档的数据！");
            return false;
        }
        if (mLJAPayPersonSet.size() != mLJSPayPersonSet.size()) {
            buildError("dealLJSPay", "应收数据和应收备份数据不同,请检查！LJAPayPerson");
            return false;
        }
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        System.out.println("进行校验.....");
        if (mTransferData == null) {
            buildError("checkData", "传入参数为null！");
            return false;
        }
        if (mGlobalInput == null) {
            buildError("checkData", "登陆信息丢失,请重新登陆！");
            return false;
        }

        this.mManageCom = (String) mTransferData.getValueByName("ManageCom");
        this.mAgentCom = (String) mTransferData.getValueByName("AgentCom");
        this.mStartDate = (String) mTransferData.getValueByName("StartDate");
        this.mEndDate = (String) mTransferData.getValueByName("EndDate");
        this.mGetNoticeNo=(String) mTransferData.getValueByName("GetNoticeNo");
        this.mOtherNo=(String) mTransferData.getValueByName("OtherNo");

        if (mManageCom == null || mManageCom.equals("")) {
            buildError("checkData", "传入保单管理机构为空！");
            return false;
        }
        if (mAgentCom == null || mAgentCom.equals("")) {
            buildError("checkData", "传入保单中介机构为空！");
            return false;
        }
        if (mStartDate == null || mStartDate.equals("")) {
            buildError("checkData", "传入抽档开始日期！");
            return false;
        }
        if (mEndDate == null || mEndDate.equals("")) {
            buildError("checkData", "传入抽档结束日期！");
            return false;
        }

        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mManageCom);
        if (!tLDComDB.getInfo()) {
            buildError("checkData", "传入管理机构错误！");
            return false;
        }

        LAComDB tLAComDB = new LAComDB();
        tLAComDB.setAgentCom(mAgentCom);
        if (!tLAComDB.getInfo()) {
            buildError("checkData", "传入代理机构错误！");
            return false;
        }

        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "BriefCreatFinBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

    public static void main(String[] args) {
        /** 调试数据 */
        String tAgentCom = "PC1100000004";
        String tManageCom = "86110000";
        String tStartDate = "2006-7-1";
        String tEndDate = "2006-7-31";
        GlobalInput g = new GlobalInput();
        g.ManageCom = "86";
        g.ComCode = "86";
        g.Operator = "pc";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("AgentCom", tAgentCom);
        tTransferData.setNameAndValue("ManageCom", tManageCom);
        tTransferData.setNameAndValue("StartDate", tStartDate);
        tTransferData.setNameAndValue("EndDate", tEndDate);
        VData v = new VData();
        v.add(tTransferData);
        v.add(g);
        BriefCreatFinBL s = new BriefCreatFinBL();
        if (!s.submitData(v, null)) {
            System.out.println("报错: " + s.mErrors.getErrContent());
        }
    }
}
