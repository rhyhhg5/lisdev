package com.sinosoft.lis.brieftb;

import java.util.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;

public class LCContModifyBL {
    public CErrors mErrors = new CErrors();
    private VData mInputData;
    private String mOperate;
    private VData mResult = new VData();
    private LCContSchema mLCContSchema = new LCContSchema();
    private String old_PolNo;
    private String new_PolNo;
    private String old_ContNo;
    private String new_ContNo;
    private MMap map = new MMap();
    private MMap map1 = new MMap();
    private GlobalInput mGlobalInput;
   private ExeSQL tExeSQL=new ExeSQL();
    TransferData mTransferData;
    public LCContModifyBL() {
    }

    /**
     * submitData
     *
     * @param nInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData nInputData, String cOperate) {
        this.mInputData = nInputData;
        this.mOperate = cOperate;

        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(this.mResult, "INSERT")) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
      }
//        /** 处理成生Xml文件 */
//        if (!dealCreatXml()) {
//            return false;
//        }
        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        if(!StrTool.cTrim(mLCContSchema.getState()).equals("88888888")) {
            String tsql1="update lccont set state='' where prtno='"+mLCContSchema.getPrtNo()+"'";
            System.out.println("tsql1"+tsql1);
            map.put(tsql1, "UPDATE");
            if (this.map.size() > 0) {
            this.mResult.addElement(map);
             }
         }else{
              String tsql1="update lccont set state='' where prtno='"+mLCContSchema.getPrtNo()+"'";
              System.out.println("tsql1"+tsql1);
          map1.put(tsql1, "UPDATE");
          this.mResult.addElement(map1);
        }
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        if(!insertData()){
           return false;
        }
        if (!dealContNo()) {
            return false;
        }
        //处理财务部分的表
        map.put("update LJTempFee set OtherNo='" + new_ContNo +
                "' where otherno='" +
                old_ContNo + "'", "UPDATE");
        map.put("update LJAPay set IncomeNo='" + new_ContNo +
                "' where IncomeNo='" +
                old_ContNo + "'", "UPDATE");
        map.put("update LJAPayPerson set ContNo='" + new_ContNo +
                "' where ContNo='" +
                old_ContNo + "'", "UPDATE");
        map.put("update lacommision set contno='"+new_ContNo+"' where  contno='"+old_ContNo+"'","UPDATE");
        String Sql1="select count(1) from ldperson  where CustomerNo='"+mLCContSchema.getAppntNo()+"' ";
        String num=tExeSQL.getOneValue(Sql1);
        if(StrTool.cTrim(num).equals("1")){
        String Sql2="update LDPerson set AppntNum=(select to_zero(AppntNum)+1 from LDPerson where CustomerNo='"+mLCContSchema.getAppntNo()+"') where CustomerNo='"+mLCContSchema.getAppntNo()+"' ";
        map.put(Sql2, "UPDATE");
        }else{
            CError cError = new CError();
            cError.moduleName = "LCContModifyBL";
            cError.functionName = "dealData";
            cError.errorMessage = "没有查询到该客户的信息!";
            this.mErrors.addOneError(cError);
            System.out.println("程序报错：" + cError.errorMessage);
            return false;
        }
        return true;
    }

    /**
     * dealContNo
     *
     * @return boolean
     */
    private boolean dealContNo() {
        //需要处理团单下的全部合同
        StringBuffer sql = new StringBuffer(255);
        sql.append("select contno,appntno,insuredno from lccont where contno ='");
        sql.append(this.mLCContSchema.getContNo());
        sql.append("' ");
        ExeSQL tExeSQL = new ExeSQL();
        SSRS ssrs = tExeSQL.execSQL(sql.toString());
            String mAppntNo = ssrs.GetText(1, 2);
            this.new_ContNo =  PubFun1.CreateMaxNo("CONTNO", mAppntNo);
            this.old_ContNo = ssrs.GetText(1, 1);
            String[] ContTables = {
                                  "LCUWSub", "LCUWMaster", "LCUWError",
                                  "LCSpec", "LCSignLog", "LCServInfo",
                                  "LCRReportResult",
                                  "LCRReportItem", "LCRReport", "LCPrem",
                                  "LCPolSpecRela", "LCPol",
                                  "LCPENoticeResult", "LCPENoticeItem",
                                  "LCPENotice", "LCParam",
                                  "LCNotePad", "LCNation", "LCIssuePol",
                                  "LCInsuredList", "LCInsured",
                                  "LCInsureAccTrace", "LCInsureAccFee",
                                  "LCInsureAccClassFee",
                                  "LCInsureAccClass", "LCInsureAcc",
                                  "LCIndUWSub", "LCIndUWMaster",
                                  "LCIndUWError", "LCGet", "LCFactory",
                                  "LCDuty",
                                  "LCDiseaseResult", "LCCUWSub", "LCCUWMaster",
                                  "LCCUWError",
                                  "LCCustomerImpartParams",
                                  "LCCustomerImpartDetail",
                                  "LCCustomerImpart", "LCCSpec",
                                  "LCContCustomerRelaInfo",
                                  "LCCont", "LCBnf", "LCAppnt",
                                  "LJAPayPerson","laviolatcont","laascription"
            };
            String condition = " ContNo='" + new_ContNo + "'";
            String wherepart = " ContNo='" + old_ContNo + "'";
            Vector VecContPol = PubFun.formUpdateSql(ContTables, condition,
                    wherepart);
            for (int m = 0; m < VecContPol.size(); m++) {
                this.map.put((String) VecContPol.get(m), "UPDATE");
            }
        ssrs = null;
        sql = new StringBuffer(255);
        sql.append("select polno from lcpol where contno ='");
        sql.append(this.mLCContSchema.getContNo());
        sql.append("' ");
        ssrs = tExeSQL.execSQL(sql.toString());
        for (int i = 1; i <= ssrs.MaxRow; i++) {
            String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
            this.new_PolNo = PubFun1.CreateMaxNo("POLNO", tLimit);
            this.old_PolNo = ssrs.GetText(i, 1);
            String[] PolTables = {
                                 "LCBnf", "LCDuty", "LCErr", "LCFactory",
                                 "LCGet", "LCGetToAcc",
                                 "LCInsureAcc", "LCInsureAccClass",
                                 "LCInsureAccClassFee",
                                 "LCInsureAccFee", "LCInsureAccTrace",
                                 "LCInsuredRelated", "LCParam", "LCPol",
                                 "LCPolSpecRela", "LCPrem",
                                 "LCPrem_1", "LCPremToAcc", "LCRemark",
                                 "LCSpec", "LCTranPolErrLog",
                                 "LCUrgeLog", "LCUWError", "LCUWMaster",
                                 "LCUWSub",
                                 "LJAPayPerson"
            };
            String condition1 = " PolNo='" + new_PolNo + "'";
            String wherepart1 = " PolNo='" + old_PolNo + "'";
            Vector VecContPol1 = PubFun.formUpdateSql(PolTables, condition1,
                    wherepart1);
            for (int m = 0; m < VecContPol1.size(); m++) {
                this.map.put((String) VecContPol1.get(m), "UPDATE");
            }
            map.put("update lacommision set polno='"+new_PolNo+"',mainpolno='"+new_PolNo+"' where polno='"+old_PolNo+"' and contno='"+old_ContNo+"'","UPDATE");

        }
        return true;
    }
    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        if (StrTool.cTrim(mLCContSchema.getGrpContNo()).equals("")) {
            System.out.println(
                    "程序第76行出错，请检查GrpBriefChangeNoBL.java中的checkData方法！");
            CError tError = new CError();
            tError.moduleName = "GrpBriefChangeNoBL.java";
            tError.functionName = "checkData";
            tError.errorMessage = "保单号为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        mLCContSchema = (LCContSchema)this.mInputData.
                           getObjectByObjectName(
                                   "LCContSchema",
                                   0);
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setSchema(mLCContSchema);
        if (!tLCContDB.getInfo()) {
            System.out.println(
                    "程序第164行出错，请检查GrpBriefChangeNoBL.java中的getInputData方法！");
            CError tError = new CError();
            tError.moduleName = "GrpBriefChangeNoBL.java";
            tError.functionName = "getInputData";
            tError.errorMessage = "未找到团体保单信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        this.mLCContSchema.setSchema(tLCContDB.getSchema());
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput",
                0);

        return true;
    }

    /**
     * getResult
     *
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }

    public static void main(String[] args) {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86";
        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema.setPrtNo("20060524005");
        tLCContSchema.setContNo("00002917701");
        VData tVData = new VData();
        tVData.add(tLCContSchema);
        tVData.add(tG);
          LCContModifyBL tLCContModifyBL   = new LCContModifyBL();
          try{
          tLCContModifyBL.submitData( tVData, "END||MAIN" );
      }
      catch(Exception ex){
          ex.printStackTrace();
      }
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "GrpBriefChangeNoBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }
    private boolean insertData() {
            BriefContDeleteBL tBriefContDeleteBL = new
         BriefContDeleteBL();
           if (!tBriefContDeleteBL.submitData(this.mInputData,
         "END||MAIN")) {
              this.mErrors.copyAllErrors(tBriefContDeleteBL.mErrors);
             return false;
             }
             VData delData = tBriefContDeleteBL.getResult();
             if (delData.size() <= 0) {
                 System.out.println(
            "程序第304行出错，请检查BriefGroupContInputBL.java中的insertData方法！");
            CError tError = new CError();
            tError.moduleName = "BriefGroupContInputBL.java";
            tError.functionName = "insertData";
            tError.errorMessage = "重新生成保单失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        this.map.add((MMap) delData.getObjectByObjectName("MMap", 0));

               return true;
        }
}
