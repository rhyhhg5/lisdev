package com.sinosoft.lis.brieftb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LCDutySchema;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class BriefProposalBL {
    private VData mInputData;
    private String mOperate;
    /** 险种信息 */
    private LCPolSet mLCPolSet;
    /** 报错信息 */
    private CErrors mErrors = new CErrors();
    /** 责任信息 */
    private LCDutySet mLCDutySet = new LCDutySet();
    public BriefProposalBL() {
    }

    /**
     * submitData
     *
     * @param nInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData nInputData, String cOperate) {
        this.mInputData = nInputData;
        this.mOperate = cOperate;

        System.out.println("@@完成程序submitData第33行");

        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        if (!prepareOutputData()) {
            return false;
        }

        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        for (int i = 1; i <= this.mLCPolSet.size(); i++) {
            LMRiskDutyDB tLMRiskDutyDB = new LMRiskDutyDB();
            tLMRiskDutyDB.setRiskCode(mLCPolSet.get(i).getRiskCode());
            LMRiskDutySet tLMRiskDutySet = tLMRiskDutyDB.query();
            if (tLMRiskDutySet.size() <= 0) {
                // @@错误处理
                System.out.println("BriefProposalBL中"
                                   + "dealData方法报错，"
                                   + "在程序83行，Author:Yangming");
                CError tError = new CError();
                tError.moduleName = "BriefProposalBL";
                tError.functionName = "dealData";
                tError.errorMessage = "没有找险种" + mLCPolSet.get(i).getRiskCode() +
                                      "对应的责任信息！";
                this.mErrors.addOneError(tError);
                return false;
            }
            LCDutySet tLCDutySet = new LCDutySet();
            for (int m = 1; m <= tLMRiskDutySet.size(); m++) {
                LCDutySchema tLCDutySchema = new LCDutySchema();
                tLCDutySchema.setDutyCode(tLMRiskDutySet.get(m).getDutyCode());
            }
        }
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        if (this.mLCPolSet == null) {
            // @@错误处理
            System.out.println("BriefProposalBL中"
                               + "checkData方法报错，"
                               + "在程序83行，Author:Yangming");
            CError tError = new CError();
            tError.moduleName = "BriefProposalBL";
            tError.functionName = "checkData";
            tError.errorMessage = "传入的险种信息为null！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        mLCPolSet = (LCPolSet) mInputData.getObjectByObjectName("LCPolSet", 0);
        return true;
    }
}
