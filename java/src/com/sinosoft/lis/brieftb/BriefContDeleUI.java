package com.sinosoft.lis.brieftb;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.tb.*;

/**
 * <p>Title:简易保单整单删除UI层 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: sinosoft</p>
 * @author shaoax
 * @version 1.0
 */
public class BriefContDeleUI
{
	/** 传入数据的容器 */
	private VData mInputData = new VData();

	/** 往前面传输数据的容器 */
	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	public BriefContDeleUI()
	{
	}

	/**
	 * 根据cOperate判断要进行哪些操作，只传递数据给下一层
	 * @param cInputData VData
	 * @param cOperate String
	 * @return boolean
	 */
	public boolean submitData(VData cInputData, String cOperate)
	{
		// 数据操作字符串拷贝到本类中
		this.mOperate = cOperate;
		
		//对于已录单的简易件,调用正常投保单的删除类,但是这个类不能删除简易件的工作流节点
		//为了不改动这个类,要在后面再调用另一个类来删除工作流节点
		//对于未录单的简易件,则直接删除工作流节点即可
		if(this.mOperate.equals("DELETE"))
		{
			ContDeleteBL tContDeleteBL = new ContDeleteBL();

			if (tContDeleteBL.submitData(cInputData, mOperate) == false)
			{
				// @@错误处理
				this.mErrors.copyAllErrors(tContDeleteBL.mErrors);
				return false;
			}
		}
		//删除工作流节点
		BriefContDeleBL tBriefContDeleBL =new BriefContDeleBL();
		if(!tBriefContDeleBL.submitData(cInputData, cOperate))
		{
			// @@错误处理
			this.mErrors.copyAllErrors((tBriefContDeleBL.mErrors));
			return false;
		}
		return true;
	}

	/**
	 * 获取从BL层取得的结果
	 * @return VData
	 */
	public VData getResult()
	{
		return mResult;
	}

	public static void main(String[] agrs)
	{
	}
}
