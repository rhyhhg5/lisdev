package com.sinosoft.lis.brieftb;

import com.sinosoft.lis.db.LBInsuredListDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCInsuredListDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCPremDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.schema.LJAPayGrpSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.schema.LJSPayBSchema;
import com.sinosoft.lis.schema.LJSPayGrpBSchema;
import com.sinosoft.lis.schema.LJSPayGrpSchema;
import com.sinosoft.lis.schema.LJSPayPersonBSchema;
import com.sinosoft.lis.schema.LJSPayPersonSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.tb.LCGrpContSignBL;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LJSPayGrpBSet;
import com.sinosoft.lis.vschema.LJSPayGrpSet;
import com.sinosoft.lis.vschema.LJSPayPersonBSet;
import com.sinosoft.lis.vschema.LJSPayPersonSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.lis.vschema.LWMissionSet;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class BriefAgentSignBL {
    /** 错误容器 */
    public CErrors mErrors = new CErrors();
    /** 传入参数 */
    private VData mInputData;
    /** 操作符 */
    private String mOperate;
    /** 传入集体合同信息 */
    private LCGrpContSchema mLCGrpContSchema;
    /** 登陆信息 */
    private GlobalInput mGlobalInput;
    /** 应收信息 */
    private LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet();
    private LJSPayGrpSet mLJSPayGrpSet = new LJSPayGrpSet();
    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();
    /** 应收备份 */
    private LJSPayPersonBSet mLJSPayPersonBSet = new LJSPayPersonBSet();
    private LJSPayGrpBSet mLJSPayGrpBSet = new LJSPayGrpBSet();
    private LJSPayBSchema mLJSPayBSchema = new LJSPayBSchema();
    /** 实收 */
    private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();
    private LJAPayGrpSet mLJAPayGrpSet = new LJAPayGrpSet();
    private LJAPaySchema mLJAPaySchema = new LJAPaySchema();
    /** 实收号码 */
    private String mPayNo;
    /** 递交Map */
    private MMap map = new MMap();
    /** 结果 */
    private VData mResult = new VData();
    /** 工作流参数 */
    private TransferData mTransferData;
    /** MissionID */
    private String mMissionID;

    public BriefAgentSignBL() {
    }

    /**
     * submitData
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        mInputData = cInputData;
        mOperate = cOperate;

        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(mResult, null)) {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        return true;

    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        map.put(this.mLJSPayPersonSet, "INSERT");
        map.put(this.mLJSPayGrpSet, "INSERT");
        map.put(this.mLJSPaySchema, "INSERT");
        map.put(this.mLJSPayPersonBSet, "INSERT");
        map.put(this.mLJSPayGrpBSet, "INSERT");
        map.put(this.mLJSPayBSchema, "INSERT");
        map.put(this.mLJAPayPersonSet, "INSERT");
        map.put(this.mLJAPayGrpSet, "INSERT");
        map.put(this.mLJAPaySchema, "INSERT");
        map.put("UPDATE LCGRPCONT SET APPFLAG='1',SIGNDATE='" +
                PubFun.getCurrentDate() + "',SIGNTIME='" +
                PubFun.getCurrentTime() + "',SIGNCOM='" +
                mGlobalInput.ManageCom + "' WHERE GRPCONTNO='" +
                mLCGrpContSchema.getGrpContNo() + "'", "UPDATE");
//        map.put("UPDATE LCGRPPOL SET APPFLAG='1',SIGNDATE='" +
//                PubFun.getCurrentDate() + "',SIGNTIME='" +
//                PubFun.getCurrentTime() + "',SIGNCOM='" +
//                mGlobalInput.ManageCom + "' WHERE GRPCONTNO='" +
//                mLCGrpContSchema.getGrpContNo() + "'", "UPDATE");
        map.put("UPDATE LCCONT SET APPFLAG='1',SIGNDATE='" +
                PubFun.getCurrentDate() + "',SIGNTIME='" +
                PubFun.getCurrentTime() + "',SIGNCOM='" +
                mGlobalInput.ManageCom + "' WHERE GRPCONTNO='" +
                mLCGrpContSchema.getGrpContNo() + "'", "UPDATE");
        map.put("UPDATE LCPOL SET APPFLAG='1',SIGNDATE='" +
                PubFun.getCurrentDate() + "',SIGNTIME='" +
                PubFun.getCurrentTime() + "',SIGNCOM='" +
                mGlobalInput.ManageCom + "' WHERE GRPCONTNO='" +
                mLCGrpContSchema.getGrpContNo() + "'", "UPDATE");
        mResult.add(map);
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        System.out.println("完成完整性校验后首先进行预打保单处理,预打保单完成之后,维护Appflag标志");
        System.out.println("然后生成应收清单");

        if (!signGrpCont()) {
            return false;
        }
        System.out.println("完成完整校验后进行应收处理!");

        if (!dealLJSPay()) {
            return false;
        }

        if (!dealMission()) {
            return false;
        }
        return true;
    }

    /**
     * dealMission
     *
     * @return boolean
     */
    private boolean dealMission() {

        LWMissionDB tLWMissionDB = new LWMissionDB();
        tLWMissionDB.setMissionID(mMissionID);
        LWMissionSet tLWMissionSet = tLWMissionDB.query();
        for (int i = 1; i <= tLWMissionSet.size(); i++) {
            String activityid = tLWMissionSet.get(i).getActivityID();
            String sub = tLWMissionSet.get(i).getSubMissionID();
            String tSerielNo = PubFun1.CreateMaxNo("MissionSerielNo", 10);
            map.put("insert into LBmission (select '" + tSerielNo +
                    "', lwmission.* from lwmission where MissionID = '" +
                    mMissionID + "' and activityid='" + activityid +
                    "' and submissionid='" + sub + "')",
                    "INSERT");
        }
        map.put(tLWMissionSet, "DELETE");
        return true;
    }

    /**
     * dealLJSPay
     *
     * @return boolean
     */
    private boolean dealLJSPay() {
        String proposalGrpContNo = mLCGrpContSchema.getProposalGrpContNo();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setProposalGrpContNo(proposalGrpContNo);
        LCGrpContSet tLCGrpContSet = tLCGrpContDB.query();

        if (tLCGrpContSet == null || tLCGrpContSet.size() <= 0 ||
            tLCGrpContSet.size() > 1) {
            buildError("dealLJSPay", "合同签单换号后查询失败！");
            return false;
        }
        this.mLCGrpContSchema.setSchema(tLCGrpContSet.get(1));

        mPayNo = PubFun1.CreateMaxNo("PAYNO",
                                     PubFun.getNoLimit(mLCGrpContSchema.
                getManageCom()));

        /** 生成LJSPayPerson */
        if (!dealJSPayPerson()) {
            return false;
        }

        /** 生成LJSPayGrp */
        if (!dealJSPayGrp()) {
            return false;
        }

        /** 生成LJSPay */
        if (!dealJSPay()) {
            return false;
        }

        return true;
    }

    /**
     * dealJSPay
     *
     * @return boolean
     */
    private boolean dealJSPay() {
        mLJSPaySchema.setGetNoticeNo(mLCGrpContSchema.getGrpContNo());
        mLJSPaySchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
        mLJSPaySchema.setOtherNoType("14");
        mLJSPaySchema.setAppntNo(mLCGrpContSchema.getAppntNo());
        mLJSPaySchema.setSumDuePayMoney(mLCGrpContSchema.getPrem());
        mLJSPaySchema.setPayDate(PubFun.getCurrentDate());
//        mLJSPaySchema.setBankOnTheWayFlag();
//        mLJSPaySchema.setBankSuccFlag();
//        mLJSPaySchema.setSendBankCount();
        mLJSPaySchema.setApproveCode(mLCGrpContSchema.getApproveCode());
        mLJSPaySchema.setApproveDate(mLCGrpContSchema.getApproveDate());
//        mLJSPaySchema.setSerialNo();
        mLJSPaySchema.setOperator(mGlobalInput.Operator);
        mLJSPaySchema.setMakeDate(PubFun.getCurrentDate());
        mLJSPaySchema.setMakeTime(PubFun.getCurrentTime());
        mLJSPaySchema.setModifyDate(PubFun.getCurrentDate());
        mLJSPaySchema.setModifyTime(PubFun.getCurrentTime());
        mLJSPaySchema.setManageCom(mLCGrpContSchema.getManageCom());
        mLJSPaySchema.setAgentCom(mLCGrpContSchema.getAgentCom());
        mLJSPaySchema.setAgentType(mLCGrpContSchema.getAgentType());
        mLJSPaySchema.setBankCode(mLCGrpContSchema.getBankCode());
        mLJSPaySchema.setBankAccNo(mLCGrpContSchema.getBankAccNo());
//        mLJSPaySchema.setRiskCode(mLCGrpContSchema);
        mLJSPaySchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        mLJSPaySchema.setAgentGroup(mLCGrpContSchema.getAgentGroup());
        mLJSPaySchema.setAccName(mLCGrpContSchema.getAccName());
//        mLJSPaySchema.setStartPayDate();
//        mLJSPaySchema.setPayTypeFlag();
//        mLJSPaySchema.setCanSendBank();


        mLJSPayBSchema.setGetNoticeNo(mLCGrpContSchema.getGrpContNo());
        mLJSPayBSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
        mLJSPayBSchema.setOtherNoType("14");
        mLJSPayBSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
        mLJSPayBSchema.setSumDuePayMoney(mLCGrpContSchema.getPrem());
        mLJSPayBSchema.setPayDate(PubFun.getCurrentDate());
//        mLJSPayBSchema.setBankOnTheWayFlag();
//        mLJSPayBSchema.setBankSuccFlag();
//        mLJSPayBSchema.setSendBankCount();
        mLJSPayBSchema.setApproveCode(mLCGrpContSchema.getApproveCode());
        mLJSPayBSchema.setApproveDate(mLCGrpContSchema.getApproveDate());
//        mLJSPayBSchema.setSerialNo();
        mLJSPayBSchema.setOperator(mGlobalInput.Operator);
        mLJSPayBSchema.setMakeDate(PubFun.getCurrentDate());
        mLJSPayBSchema.setMakeTime(PubFun.getCurrentTime());
        mLJSPayBSchema.setModifyDate(PubFun.getCurrentDate());
        mLJSPayBSchema.setModifyTime(PubFun.getCurrentTime());
        mLJSPayBSchema.setManageCom(mLCGrpContSchema.getManageCom());
        mLJSPayBSchema.setAgentCom(mLCGrpContSchema.getAgentCom());
        mLJSPayBSchema.setAgentType(mLCGrpContSchema.getAgentType());
        mLJSPayBSchema.setBankCode(mLCGrpContSchema.getBankCode());
        mLJSPayBSchema.setBankAccNo(mLCGrpContSchema.getBankAccNo());
//        mLJSPayBSchema.setRiskCode(mLCGrpContSchema);
        mLJSPayBSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        mLJSPayBSchema.setAgentGroup(mLCGrpContSchema.getAgentGroup());
        mLJSPayBSchema.setAccName(mLCGrpContSchema.getAccName());
//        mLJSPayBSchema.setStartPayDate();
//        mLJSPayBSchema.setPayTypeFlag();
//        mLJSPayBSchema.setCanSendBank();
        mLJSPayBSchema.setDealState("0");

        mLJAPaySchema.setPayNo(mPayNo);
        mLJAPaySchema.setIncomeNo(mLCGrpContSchema.getGrpContNo());
        mLJAPaySchema.setIncomeType("14");
        mLJAPaySchema.setAppntNo(mLCGrpContSchema.getAppntNo());
        mLJAPaySchema.setSumActuPayMoney(mLCGrpContSchema.getPrem());
        mLJAPaySchema.setPayDate(PubFun.getCurrentDate());
//        mLJAPaySchema.setEnterAccDate();
//        mLJAPaySchema.setConfDate();
        mLJAPaySchema.setApproveCode(mLCGrpContSchema.getApproveCode());
        mLJAPaySchema.setApproveDate(mLCGrpContSchema.getApproveDate());
//        mLJAPaySchema.setSerialNo();
        mLJAPaySchema.setOperator(mGlobalInput.Operator);
        mLJAPaySchema.setMakeDate(PubFun.getCurrentDate());
        mLJAPaySchema.setMakeTime(PubFun.getCurrentTime());
        mLJAPaySchema.setGetNoticeNo(mLCGrpContSchema.getGrpContNo());
        mLJAPaySchema.setModifyDate(PubFun.getCurrentDate());
        mLJAPaySchema.setModifyTime(PubFun.getCurrentTime());
        mLJAPaySchema.setManageCom(mLCGrpContSchema.getManageCom());
        mLJAPaySchema.setAgentCom(mLCGrpContSchema.getAgentCom());
        mLJAPaySchema.setAgentType(mLCGrpContSchema.getAgentType());
        mLJAPaySchema.setBankCode(mLCGrpContSchema.getBankCode());
        mLJAPaySchema.setBankAccNo(mLCGrpContSchema.getBankAccNo());
//        mLJAPaySchema.setRiskCode();
        mLJAPaySchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        mLJAPaySchema.setAgentGroup(mLCGrpContSchema.getAgentGroup());
        mLJAPaySchema.setAccName(mLCGrpContSchema.getAccName());
//        mLJAPaySchema.setStartPayDate();
//        mLJAPaySchema.setPayTypeFlag();
//        mLJAPaySchema.setPrtStateFlag();
//        mLJAPaySchema.setPrintTimes();
        return true;
    }

    /**
     * dealJSPayGrp
     *
     * @return boolean
     */
    private boolean dealJSPayGrp() {
        System.out.println("开始进行团体险种应收数据的生成");
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(this.mLCGrpContSchema.getGrpContNo());
        LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();

        if (tLCGrpPolSet == null || tLCGrpPolSet.size() <= 0) {
            buildError("dealJSPayGrp", "签单后生成，查询团体集体险种信息失败！");
            return false;
        }

        for (int i = 1; i <= tLCGrpPolSet.size(); i++) {
            LCGrpPolSchema tLCGrpPolSchema = tLCGrpPolSet.get(i);
            LJSPayGrpSchema tLJSPayGrpSchema = new LJSPayGrpSchema();
            tLJSPayGrpSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
            tLJSPayGrpSchema.setPayCount(1);
            tLJSPayGrpSchema.setGrpContNo(tLCGrpPolSchema.getGrpContNo());
            tLJSPayGrpSchema.setManageCom(tLCGrpPolSchema.getManageCom());
            tLJSPayGrpSchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
            tLJSPayGrpSchema.setAgentType(tLCGrpPolSchema.getAgentType());
            tLJSPayGrpSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
            tLJSPayGrpSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
            tLJSPayGrpSchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());
//            tLJSPayGrpSchema.setPayTypeFlag();
            tLJSPayGrpSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
            tLJSPayGrpSchema.setGetNoticeNo(mLCGrpContSchema.getGrpContNo());
            tLJSPayGrpSchema.setSumDuePayMoney(tLCGrpPolSchema.getPrem());
            tLJSPayGrpSchema.setSumActuPayMoney(tLCGrpPolSchema.getPrem());
            tLJSPayGrpSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
            tLJSPayGrpSchema.setPayDate(PubFun.getCurrentDate());
            tLJSPayGrpSchema.setPayType("ZC");
            tLJSPayGrpSchema.setLastPayToDate(tLCGrpPolSchema.getCValiDate());
            tLJSPayGrpSchema.setCurPayToDate(tLCGrpPolSchema.getPaytoDate());
            tLJSPayGrpSchema.setApproveCode(tLCGrpPolSchema.getApproveCode());
            tLJSPayGrpSchema.setApproveDate(tLCGrpPolSchema.getApproveDate());
            tLJSPayGrpSchema.setApproveTime(tLCGrpPolSchema.getApproveTime());
//            tLJSPayGrpSchema.setSerialNo();
            tLJSPayGrpSchema.setInputFlag("1");
            tLJSPayGrpSchema.setOperator(mGlobalInput.Operator);
            tLJSPayGrpSchema.setMakeDate(PubFun.getCurrentDate());
            tLJSPayGrpSchema.setMakeTime(PubFun.getCurrentTime());
            tLJSPayGrpSchema.setModifyDate(PubFun.getCurrentDate());
            tLJSPayGrpSchema.setModifyTime(PubFun.getCurrentTime());
            this.mLJSPayGrpSet.add(tLJSPayGrpSchema);

            LJSPayGrpBSchema tLJSPayGrpBSchema = new LJSPayGrpBSchema();
            tLJSPayGrpBSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
            tLJSPayGrpBSchema.setPayCount(1);
            tLJSPayGrpBSchema.setGrpContNo(tLCGrpPolSchema.getGrpContNo());
            tLJSPayGrpBSchema.setManageCom(tLCGrpPolSchema.getManageCom());
            tLJSPayGrpBSchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
            tLJSPayGrpBSchema.setAgentType(tLCGrpPolSchema.getAgentType());
            tLJSPayGrpBSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
            tLJSPayGrpBSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
            tLJSPayGrpBSchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());
//            tLJSPayGrpBSchema.setPayTypeFlag();
            tLJSPayGrpBSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
            tLJSPayGrpBSchema.setGetNoticeNo(mLCGrpContSchema.getGrpContNo());
            tLJSPayGrpBSchema.setSumDuePayMoney(tLCGrpPolSchema.getPrem());
            tLJSPayGrpBSchema.setSumActuPayMoney(tLCGrpPolSchema.getPrem());
            tLJSPayGrpBSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
            tLJSPayGrpBSchema.setPayDate(PubFun.getCurrentDate());
            tLJSPayGrpBSchema.setPayType("ZC");
            tLJSPayGrpBSchema.setLastPayToDate(tLCGrpPolSchema.getCValiDate());
            tLJSPayGrpBSchema.setCurPayToDate(tLCGrpPolSchema.getPaytoDate());
            tLJSPayGrpBSchema.setApproveCode(tLCGrpPolSchema.getApproveCode());
            tLJSPayGrpBSchema.setApproveDate(tLCGrpPolSchema.getApproveDate());
            tLJSPayGrpBSchema.setApproveTime(tLCGrpPolSchema.getApproveTime());
//            tLJSPayGrpBSchema.setSerialNo();
            tLJSPayGrpBSchema.setInputFlag("1");
            tLJSPayGrpBSchema.setOperator(mGlobalInput.Operator);
            tLJSPayGrpBSchema.setMakeDate(PubFun.getCurrentDate());
            tLJSPayGrpBSchema.setMakeTime(PubFun.getCurrentTime());
            tLJSPayGrpBSchema.setModifyDate(PubFun.getCurrentDate());
            tLJSPayGrpBSchema.setModifyTime(PubFun.getCurrentTime());
            tLJSPayGrpBSchema.setDealState("0"); //0 催收状态为不成功
            this.mLJSPayGrpBSet.add(tLJSPayGrpBSchema);

            LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();
            tLJAPayGrpSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
            tLJAPayGrpSchema.setPayCount(1);
            tLJAPayGrpSchema.setGrpContNo(tLCGrpPolSchema.getGrpContNo());
            tLJAPayGrpSchema.setManageCom(tLCGrpPolSchema.getManageCom());
            tLJAPayGrpSchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
            tLJAPayGrpSchema.setAgentType(tLCGrpPolSchema.getAgentType());
            tLJAPayGrpSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
            tLJAPayGrpSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
            tLJAPayGrpSchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());
//            tLJAPayGrpSchema.setPayTypeFlag();
            tLJAPayGrpSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
            tLJAPayGrpSchema.setGetNoticeNo(mLCGrpContSchema.getGrpContNo());
            tLJAPayGrpSchema.setSumDuePayMoney(tLCGrpPolSchema.getPrem());
            tLJAPayGrpSchema.setSumActuPayMoney(tLCGrpPolSchema.getPrem());
            tLJAPayGrpSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
            tLJAPayGrpSchema.setPayDate(PubFun.getCurrentDate());
            tLJAPayGrpSchema.setPayType("ZC");
            tLJAPayGrpSchema.setLastPayToDate(tLCGrpPolSchema.getCValiDate());
            tLJAPayGrpSchema.setCurPayToDate(tLCGrpPolSchema.getPaytoDate());
            tLJAPayGrpSchema.setApproveCode(tLCGrpPolSchema.getApproveCode());
            tLJAPayGrpSchema.setApproveDate(tLCGrpPolSchema.getApproveDate());
            tLJAPayGrpSchema.setApproveTime(tLCGrpPolSchema.getApproveTime());
//            tLJAPayGrpSchema.setSerialNo();
            tLJAPayGrpSchema.setPayNo(mPayNo);
            tLJAPayGrpSchema.setOperator(mGlobalInput.Operator);
            tLJAPayGrpSchema.setMakeDate(PubFun.getCurrentDate());
            tLJAPayGrpSchema.setMakeTime(PubFun.getCurrentTime());
            tLJAPayGrpSchema.setModifyDate(PubFun.getCurrentDate());
            tLJAPayGrpSchema.setModifyTime(PubFun.getCurrentTime());
            this.mLJAPayGrpSet.add(tLJAPayGrpSchema);

        }
        return true;
    }

    /**
     * dealJSPayPerson
     *
     * @return boolean
     */
    private boolean dealJSPayPerson() {
        System.out.println("开始处理LJSPayPerson 和 LJSPayPersonB");
        LCPremDB tLCPremDB = new LCPremDB();
        tLCPremDB.setGrpContNo(this.mLCGrpContSchema.getGrpContNo());
        LCPremSet tLCPremSet = tLCPremDB.query();

        if (tLCPremSet == null || tLCPremSet.size() <= 0) {
            buildError("dealJSPayPerson", "签单后,查询LCPrem失败！");
            return false;
        }

        for (int i = 1; i <= tLCPremSet.size(); i++) {
            LCPremSchema tLCPremSchema = tLCPremSet.get(i);
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(tLCPremSchema.getPolNo());
            if (!tLCPolDB.getInfo()) {
                buildError("dealJSPayPerson", "签单后,生成应收过程中,查询险种失败！");
                return false;
            }
            LJSPayPersonSchema tLJSPayPersonSchema = new LJSPayPersonSchema();
            tLJSPayPersonSchema.setPolNo(tLCPremSchema.getPolNo());
            tLJSPayPersonSchema.setPayCount(1);
            tLJSPayPersonSchema.setGrpContNo(tLCPremSchema.getGrpContNo());
            tLJSPayPersonSchema.setGrpPolNo(tLCPolDB.getGrpPolNo());
            tLJSPayPersonSchema.setContNo(tLCPremSchema.getContNo());
            tLJSPayPersonSchema.setManageCom(tLCPremSchema.getManageCom());
            tLJSPayPersonSchema.setAgentCom(tLCPolDB.getAgentCom());
            tLJSPayPersonSchema.setAgentType(tLCPolDB.getAgentType());
            tLJSPayPersonSchema.setRiskCode(tLCPolDB.getRiskCode());
            tLJSPayPersonSchema.setAgentCode(tLCPolDB.getAgentCode());
            tLJSPayPersonSchema.setAgentGroup(tLCPolDB.getAgentGroup());
//            tLJSPayPersonSchema.setPayTypeFlag();
            tLJSPayPersonSchema.setAppntNo(tLCPolDB.getAppntNo());
            tLJSPayPersonSchema.setGetNoticeNo(tLCPremSchema.getGrpContNo());
            tLJSPayPersonSchema.setPayAimClass("2");
            tLJSPayPersonSchema.setDutyCode(tLCPremSchema.getDutyCode());
            tLJSPayPersonSchema.setPayPlanCode(tLCPremSchema.getPayPlanCode());
            tLJSPayPersonSchema.setSumDuePayMoney(tLCPremSchema.getPrem());
            tLJSPayPersonSchema.setSumActuPayMoney(tLCPremSchema.getPrem());
            tLJSPayPersonSchema.setPayIntv(tLCPremSchema.getPayIntv());
            tLJSPayPersonSchema.setPayDate(PubFun.getCurrentDate());
            tLJSPayPersonSchema.setPayType("ZC");
            tLJSPayPersonSchema.setLastPayToDate(tLCPolDB.getCValiDate());
            tLJSPayPersonSchema.setCurPayToDate(tLCPolDB.getPaytoDate());
//            tLJSPayPersonSchema.setInInsuAccState();
//            tLJSPayPersonSchema.setBankCode(tLCPolDB.get);
//            tLJSPayPersonSchema.setBankAccNo();
//            tLJSPayPersonSchema.setBankOnTheWayFlag();
//            tLJSPayPersonSchema.setBankSuccFlag();
            tLJSPayPersonSchema.setApproveCode(tLCPolDB.getApproveCode());
            tLJSPayPersonSchema.setApproveDate(tLCPolDB.getApproveDate());
            tLJSPayPersonSchema.setApproveTime(tLCPolDB.getApproveTime());
//            tLJSPayPersonSchema.setSerialNo();
            tLJSPayPersonSchema.setInputFlag("1");
            tLJSPayPersonSchema.setOperator(this.mGlobalInput.Operator);
            tLJSPayPersonSchema.setMakeDate(PubFun.getCurrentDate());
            tLJSPayPersonSchema.setMakeTime(PubFun.getCurrentTime());
            tLJSPayPersonSchema.setModifyDate(PubFun.getCurrentDate());
            tLJSPayPersonSchema.setModifyTime(PubFun.getCurrentTime());
            this.mLJSPayPersonSet.add(tLJSPayPersonSchema);

            LJSPayPersonBSchema tLJSPayPersonBSchema = new LJSPayPersonBSchema();
            tLJSPayPersonBSchema.setPolNo(tLCPremSchema.getPolNo());
            tLJSPayPersonBSchema.setPayCount(1);
            tLJSPayPersonBSchema.setGrpContNo(tLCPremSchema.getGrpContNo());
            tLJSPayPersonBSchema.setGrpPolNo(tLCPolDB.getGrpPolNo());
            tLJSPayPersonBSchema.setContNo(tLCPremSchema.getContNo());
            tLJSPayPersonBSchema.setManageCom(tLCPremSchema.getManageCom());
            tLJSPayPersonBSchema.setAgentCom(tLCPolDB.getAgentCom());
            tLJSPayPersonBSchema.setAgentType(tLCPolDB.getAgentType());
            tLJSPayPersonBSchema.setRiskCode(tLCPolDB.getRiskCode());
            tLJSPayPersonBSchema.setAgentCode(tLCPolDB.getAgentCode());
            tLJSPayPersonBSchema.setAgentGroup(tLCPolDB.getAgentGroup());
//            tLJSPayPersonBSchema.setPayTypeFlag();
            tLJSPayPersonBSchema.setAppntNo(tLCPolDB.getAppntNo());
            tLJSPayPersonBSchema.setGetNoticeNo(tLCPremSchema.getGrpContNo());
            tLJSPayPersonBSchema.setPayAimClass("2");
            tLJSPayPersonBSchema.setDutyCode(tLCPremSchema.getDutyCode());
            tLJSPayPersonBSchema.setPayPlanCode(tLCPremSchema.getPayPlanCode());
            tLJSPayPersonBSchema.setSumDuePayMoney(tLCPremSchema.getPrem());
            tLJSPayPersonBSchema.setSumActuPayMoney(tLCPremSchema.getPrem());
            tLJSPayPersonBSchema.setPayIntv(tLCPremSchema.getPayIntv());
            tLJSPayPersonBSchema.setPayDate(PubFun.getCurrentDate());
            tLJSPayPersonBSchema.setPayType("ZC");
            tLJSPayPersonBSchema.setLastPayToDate(tLCPolDB.getCValiDate());
            tLJSPayPersonBSchema.setCurPayToDate(tLCPolDB.getPaytoDate());
//            tLJSPayPersonBSchema.setInInsuAccState();
//            tLJSPayPersonBSchema.setBankCode(tLCPolDB.get);
//            tLJSPayPersonBSchema.setBankAccNo();
//            tLJSPayPersonBSchema.setBankOnTheWayFlag();
//            tLJSPayPersonBSchema.setBankSuccFlag();
            tLJSPayPersonBSchema.setApproveCode(tLCPolDB.getApproveCode());
            tLJSPayPersonBSchema.setApproveDate(tLCPolDB.getApproveDate());
            tLJSPayPersonBSchema.setApproveTime(tLCPolDB.getApproveTime());
//            tLJSPayPersonBSchema.setSerialNo();
            tLJSPayPersonBSchema.setInputFlag("1");
            tLJSPayPersonBSchema.setOperator(this.mGlobalInput.Operator);
            tLJSPayPersonBSchema.setMakeDate(PubFun.getCurrentDate());
            tLJSPayPersonBSchema.setMakeTime(PubFun.getCurrentTime());
            tLJSPayPersonBSchema.setModifyDate(PubFun.getCurrentDate());
            tLJSPayPersonBSchema.setModifyTime(PubFun.getCurrentTime());
            tLJSPayPersonBSchema.setDealState("0");
            this.mLJSPayPersonBSet.add(tLJSPayPersonBSchema);

            LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
            tLJAPayPersonSchema.setPolNo(tLCPremSchema.getPolNo());
            tLJAPayPersonSchema.setPayCount(1);
            tLJAPayPersonSchema.setGrpContNo(tLCPremSchema.getGrpContNo());
            tLJAPayPersonSchema.setGrpPolNo(tLCPolDB.getGrpPolNo());
            tLJAPayPersonSchema.setContNo(tLCPremSchema.getContNo());
            tLJAPayPersonSchema.setManageCom(tLCPremSchema.getManageCom());
            tLJAPayPersonSchema.setAgentCom(tLCPolDB.getAgentCom());
            tLJAPayPersonSchema.setAgentType(tLCPolDB.getAgentType());
            tLJAPayPersonSchema.setRiskCode(tLCPolDB.getRiskCode());
            tLJAPayPersonSchema.setAgentCode(tLCPolDB.getAgentCode());
            tLJAPayPersonSchema.setAgentGroup(tLCPolDB.getAgentGroup());
//            tLJAPayPersonSchema.setPayTypeFlag();
            tLJAPayPersonSchema.setAppntNo(tLCPolDB.getAppntNo());
            tLJAPayPersonSchema.setGetNoticeNo(tLCPremSchema.getGrpContNo());
            tLJAPayPersonSchema.setPayAimClass("2");
            tLJAPayPersonSchema.setDutyCode(tLCPremSchema.getDutyCode());
            tLJAPayPersonSchema.setPayPlanCode(tLCPremSchema.getPayPlanCode());
            tLJAPayPersonSchema.setSumDuePayMoney(tLCPremSchema.getPrem());
            tLJAPayPersonSchema.setSumActuPayMoney(tLCPremSchema.getPrem());
            tLJAPayPersonSchema.setPayIntv(tLCPremSchema.getPayIntv());
            tLJAPayPersonSchema.setPayDate(PubFun.getCurrentDate());
            tLJAPayPersonSchema.setPayType("ZC");
            tLJAPayPersonSchema.setLastPayToDate(tLCPolDB.getCValiDate());
            tLJAPayPersonSchema.setCurPayToDate(tLCPolDB.getPaytoDate());
//            tLJAPayPersonSchema.setInInsuAccState();
//            tLJAPayPersonSchema.setBankCode(tLCPolDB.get);
//            tLJAPayPersonSchema.setBankAccNo();
//            tLJAPayPersonSchema.setBankOnTheWayFlag();
//            tLJAPayPersonSchema.setBankSuccFlag();
            tLJAPayPersonSchema.setApproveCode(tLCPolDB.getApproveCode());
            tLJAPayPersonSchema.setApproveDate(tLCPolDB.getApproveDate());
            tLJAPayPersonSchema.setApproveTime(tLCPolDB.getApproveTime());
//            tLJAPayPersonSchema.setSerialNo();
            tLJAPayPersonSchema.setPayNo(mPayNo);
            tLJAPayPersonSchema.setOperator(this.mGlobalInput.Operator);
            tLJAPayPersonSchema.setMakeDate(PubFun.getCurrentDate());
            tLJAPayPersonSchema.setMakeTime(PubFun.getCurrentTime());
            tLJAPayPersonSchema.setModifyDate(PubFun.getCurrentDate());
            tLJAPayPersonSchema.setModifyTime(PubFun.getCurrentTime());
            this.mLJAPayPersonSet.add(tLJAPayPersonSchema);
        }
        return true;
    }

    /**
     * signGrpCont
     *
     * @return boolean
     */
    private boolean signGrpCont() {

        if (mLCGrpContSchema.getAppFlag().equals("9") ||
            mLCGrpContSchema.getAppFlag().equals("1")) {
            System.out.println("已经完成签单动作，不需要再次签单");
            return true;
        }

        System.out.println("开始进行签单动作.....");
        this.mLCGrpContSchema.setAppFlag("9");
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        tLCGrpContSet.add(mLCGrpContSchema);
        VData tInputData = new VData();
        tInputData.add(tLCGrpContSet);
        tInputData.add(this.mGlobalInput);
        LCGrpContSignBL tLCGrpContSignBL = new LCGrpContSignBL();
        try {
            tLCGrpContSignBL.submitData(tInputData, "");
        } catch (Exception ex) {
            buildError("signGrpCont", "签单发生异常错误！");
            return false;
        }
        if (tLCGrpContSignBL.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tLCGrpContSignBL.mErrors);
            return false;
        }
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {

        System.out.println("校验合同信息.....");
        if (mLCGrpContSchema == null) {
            buildError("checkData", "传入合同信息为null！");
            return false;
        }

        if (StrTool.cTrim(mLCGrpContSchema.getProposalGrpContNo()).equals("")) {
            buildError("checkData", "团体合同号码传入为null！");
            return false;
        }

        LCGrpContDB tLCGrpContDB = mLCGrpContSchema.getDB();
        LCGrpContSet tLCGrpContSet = tLCGrpContDB.query();
        if (tLCGrpContSet == null || tLCGrpContSet.size() <= 0 ||
            tLCGrpContSet.size() > 1) {
            buildError("checkData", "查询合同信息失败！");
            return false;
        }

        this.mLCGrpContSchema.setSchema(tLCGrpContSet.get(1));

        System.out.println("开始校验保费,人数信息是否一直");

        if (!checkContInfo()) {
            return false;
        }

        if (mTransferData == null) {
            buildError("checkData", "传入工作流表数据描述为null！");
            return false;
        }

        this.mMissionID = (String) mTransferData.getValueByName("MissionID");

        if (mMissionID == null || mMissionID.equals("")) {
            buildError("checkData", "传入工作流信息为null！");
            return false;
        }
        return true;
    }

    /**
     * checkContInfo
     *
     * @return boolean
     */
    private boolean checkContInfo() {
        double tGrpContPrem = this.mLCGrpContSchema.getPrem();

        double tGrpPolSumPrem = getGrpPolSumPrem();
        double tContSumPrem = getContSumPrem();
        double tPolSumPrem = getPolSumPrem();
        System.out.println("Group Prem" + tGrpContPrem);
        System.out.println("GrpPol Prem" + tGrpPolSumPrem);
        System.out.println("Cont Prem" + tContSumPrem);
        System.out.println("Pol Prem" + tPolSumPrem);
        if (tGrpContPrem != tGrpPolSumPrem) {
            buildError("checkContInfo", "整单合同保费与团体险种总保费不符！");
            return false;
        }
        if (tGrpContPrem != tContSumPrem) {
            buildError("checkContInfo", "整单合同保费与被保人合同总保费不符！");
            return false;
        }
        if (tGrpContPrem != tPolSumPrem) {
            buildError("checkContInfo", "整单合同保费与被保人合同总保费不符！");
            return false;
        }

        int tGrpPeople = mLCGrpContSchema.getPeoples2();
        int tContPeople = getContPeople();
        int tListPeople = getListPeople();
        System.out.println("团单下被保人数 " + tGrpPeople);
        System.out.println("合同下被保人数 " + tContPeople);
        System.out.println("清单被保人人数 " + tListPeople);

        if (tGrpPeople == 0) {
            buildError("checkContInfo", "团单记录被保人人数为0！");
            return false;
        }
        if (tGrpPeople != tContPeople) {
            buildError("checkContInfo", "团单下被保人人数不等于导入系统的人数！");
            return false;
        }
        if (tGrpPeople != tListPeople) {
            buildError("checkContInfo", "团单下被保人人数不等于录入被保人人数！");
            return false;
        }

        return true;
    }

    /**
     * getListPeople
     *
     * @return int
     */
    private int getListPeople() {
        LCInsuredListDB tLCInsuredListDB = new LCInsuredListDB();
        tLCInsuredListDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());

        LBInsuredListDB tLBInsuredListDB = new LBInsuredListDB();
        tLBInsuredListDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());

        return tLCInsuredListDB.getCount() + tLBInsuredListDB.getCount();
    }

    /**
     * getContPeople
     *
     * @return int
     */
    private int getContPeople() {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        return tLCContDB.getCount();
    }

    /**
     * getPolSumPrem
     *
     * @return double
     */
    private double getPolSumPrem() {
        String sql = "select sum(prem) from lcpol where grpcontno='" +
                     this.mLCGrpContSchema.getGrpContNo() + "'";
        return Double.parseDouble((new ExeSQL()).getOneValue(sql));

    }

    /**
     * getContSumPrem
     *
     * @return double
     */
    private double getContSumPrem() {
        String sql = "select sum(prem) from lccont where grpcontno='" +
                     this.mLCGrpContSchema.getGrpContNo() + "'";
        return Double.parseDouble((new ExeSQL()).getOneValue(sql));

    }

    /**
     * getGrpPolSumPrem
     *
     * @return double
     */
    private double getGrpPolSumPrem() {
        String sql = "select sum(prem) from lcgrppol where grpcontno='" +
                     this.mLCGrpContSchema.getGrpContNo() + "'";
        return Double.parseDouble((new ExeSQL()).getOneValue(sql));
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        mLCGrpContSchema = (LCGrpContSchema) mInputData.
                           getObjectByObjectName("LCGrpContSchema", 0);
        mTransferData = (TransferData) mInputData.
                        getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "BriefAgentSignBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

    /**
     * 调试主函数
     * @param args String[]
     */
    public static void main(String[] args) {
        String tGrpContNo = "1400002625";
        String tMissionID = "00000000000000004399";
        BriefAgentSignBL tBriefAgentSignBL = new BriefAgentSignBL();
        VData v = new VData();
        GlobalInput g = new GlobalInput();
        g.ManageCom = "86110000";
        g.ComCode = "86110000";
        g.Operator = "group";

        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema.setProposalGrpContNo(tGrpContNo);
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("MissionID",
                                      tMissionID);
        v.add(g);
        v.add(tLCGrpContSchema);
        v.add(tTransferData);
        try {
            tBriefAgentSignBL.submitData(v, "");
        } catch (Exception ex) {
            System.out.println("程序发生异常错误：" + ex);
        }
        CErrors Error = tBriefAgentSignBL.mErrors;
        if (Error.needDealError()) {
            System.out.println("程序发生错误，原因是：" + Error.getErrContent());
        }
    }
}
