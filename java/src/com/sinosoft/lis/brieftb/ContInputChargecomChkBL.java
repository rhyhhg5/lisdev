package com.sinosoft.lis.brieftb;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;

import javax.xml.namespace.QName;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import com.sinosoft.lis.db.LAChargeDB;
import com.sinosoft.lis.schema.LAChargeSchema;
import com.sinosoft.lis.vschema.LAChargeSet;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
public class ContInputChargecomChkBL {
   private Logger cLogger = Logger.getLogger(getClass());
   
	/** 往工作流引擎中传输数据的容器 */

    private LAChargeSchema tLAChargeSchema=new LAChargeSchema();
    private VData mInputData=new VData();
	//传输数据类
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /**
     * 发送报文的保单数据封装类
     */
    private TransferData mOutxml=new TransferData();
    /**
     * 前台传来的LAChargeSchema
     */
    private String Chargetype="";
    private String contsql="";
    private ExeSQL mExeSQL=new ExeSQL();
    private LAChargeSet mLAChargeSet = new LAChargeSet();
	public boolean submitData(VData cInputData, String cOperate) {
	        //首先将数据在本类中做一个备份
	        mInputData = (VData) cInputData.clone();
	        System.out.println("Start ContInputChargecomChkBL Submit...");
	        if (!getInputData(mInputData, cOperate))
	        {
	            return false;
	        }

	        if (!checkData())
	        {
	            return false;
	        }

	        try {
				if (!dealData())
				{
				    return false;
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
	       
	        System.out.println("End ContInputChargecomChkBL Submit...");

	        mInputData = null;
	        return true;
	    }
     private boolean checkData(){
		
		return true;
	}
     private boolean dealData() throws FileNotFoundException{
 		String tresult=queryChargeInfo();	
 		if(!"".equals(tresult) && tresult!=null){
 			Document tresDocument=getDocument(tresult.toString());
 			if(tresDocument!=null){
 				Element tall=tresDocument.getRootElement();
 				Element thead=tall.getChild("Head");
 				System.out.println("tal:"+tall.getChild("Head").getChildText("ErrorCode"));
 				Element terrorcode=thead.getChild("ErrorCode");
 				Element tresponsecode=thead.getChild("ResponseCode");
 				Element tbody=tall.getChild("Body");
 				System.out.println("errorcode:"+terrorcode.getText());
 				if(tresponsecode!=null && !"".equals(tresponsecode.getText()) && "1".equals(tresponsecode.getText()) ){
 					System.out.print("江苏中介接口返回成功！");
 					return true;
 				}else if(tresponsecode!=null && !"".equals(tresponsecode.getText()) && !"1".equals(tresponsecode.getText()) && "9999".equals(thead.getChildText("ErrorCode"))){
 				     CError tError = new CError();
 					tError.moduleName = "ContInputChargecomChkBL";
 		            tError.functionName = "dealData";
 		            tError.errorMessage = "江苏中介机构查询失败！错误信息为："+tbody.getChild("Error").getChildText("ErrorCode")+"-"+tbody.getChild("Error").getChildText("ErrorMessage");
 		            this.mErrors.addOneError(tError);
 		            return false;
 				}else if(tresponsecode!=null && !"".equals(tresponsecode.getText()) && !"1".equals(tresponsecode.getText())){
 				     CError tError = new CError();
 					tError.moduleName = "ContInputChargecomChkBL";
 		            tError.functionName = "dealData";
 		            tError.errorMessage = "江苏中介机构查询失败！错误信息为："+thead.getChildText("ErrorCode")+"-"+thead.getChildText("ErrorMessage");
 		            this.mErrors.addOneError(tError);
 		            return false;
 				}else{
 					CError tError = new CError();
 		            tError.moduleName = "ContInputChargecomChkBL";
 		            tError.functionName = "dealData";
 		            tError.errorMessage = "查询返回报文获取失败！";
 		            this.mErrors.addOneError(tError);
 		            return false;
 				}
 			}else{
 				    CError tError = new CError();
 		            tError.moduleName = "ContInputChargecomChkBL";
 		            tError.functionName = "dealData";
 		            tError.errorMessage = "查询返回报文获取失败！";
 		            this.mErrors.addOneError(tError);
 		            return false;
 			}
 		}else{
 			System.out.println("获取返回报文失败！");
 			CError tError = new CError();
             tError.moduleName = "ContInputChargecomChkBL";
             tError.functionName = "dealData";
             tError.errorMessage = "查询返回报文获取失败！";
             this.mErrors.addOneError(tError);
 			return false;
 		}
 		
 	}
 	
 	private Document getDocument(String tstr){
 		java.io.Reader in = new StringReader(tstr);  
         Document doc=null;
 		try {
 			doc = (new SAXBuilder()).build(in);
 		} catch (JDOMException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}         
         return doc; 
 	}
 	private boolean getInputData(VData cInputData,String cOperate){
 		System.out.println("+==============");
 		//从输入数据中得到所有对象
//         mTransferData = (TransferData) cInputData.getObjectByObjectName(
//                 "TransferData", 0);
 		tLAChargeSchema=(LAChargeSchema)cInputData.getObjectByObjectName("LAChargeSchema",0);
         mInputData = cInputData;
         Chargetype=(String)cInputData.get(1);
         System.out.println("*************"+Chargetype);
         String tContno=tLAChargeSchema.getContNo();
         System.out.println(tLAChargeSchema.getContNo());
         String tgrpcontno=tLAChargeSchema.getGrpContNo();
         System.out.println(tLAChargeSchema.getGrpContNo());
         mOutxml.setNameAndValue("AgencyCode",tLAChargeSchema.getAgentCom());
         System.out.println(tLAChargeSchema.getAgentCom());
      	 mOutxml.setNameAndValue("StatementsComCode",tLAChargeSchema.getManageCom());
      	 mOutxml.setNameAndValue("PayComCode",tLAChargeSchema.getManageCom().substring(0, 4));
      	mOutxml.setNameAndValue("CostType",tLAChargeSchema.getChargeType());
     	mOutxml.setNameAndValue("PayType",tLAChargeSchema.getPayIntv());
     	mOutxml.setNameAndValue("CoverageCode",tLAChargeSchema.getRiskCode());
     	mOutxml.setNameAndValue("Premium",tLAChargeSchema.getTransMoney());
     	mOutxml.setNameAndValue("CommissionRate",tLAChargeSchema.getChargeRate());
     	mOutxml.setNameAndValue("CommissionCharge",tLAChargeSchema.getCharge());
         System.out.println("+++++++++++++++++++++++++");
         if("00000000000000000000".equals(tgrpcontno) ){
        	 System.out.println("--------------------");
        	 mOutxml.setNameAndValue("channel","1");
        	 mOutxml.setNameAndValue("PolicyEndorsementNo", tContno);
         }else if(!"00000000000000000000".equals(tgrpcontno)){
        	 mOutxml.setNameAndValue("channel","0");
        	 mOutxml.setNameAndValue("PolicyEndorsementNo", tgrpcontno);
      		//return true;
//        	 contsql="select a.agentcom,a.managecom,d.name,d.bankaccno,d.bankaccname,a.PolNo,a.ChargeType," 
//                 //+"(select codename from ldcode where codetype='payintv' and int(code)=b.payintv),b.payyears,"
//        		 +"a.payintv,b.payyears,"
//        		 +"c.risktype,a.riskcode,c.riskname,a.transmoney,a.chargerate,a.charge"
//                 +" from lacharge a ,lacommision b,lmriskapp c,lacom d "
//                 +"where a.managecom=b.managecom and c.riskcode=a.riskcode and b.riskcode=a.riskcode  and a.managecom='86320000' and a.PolNo='"++"'"
//                 +" fetch first 10 rows only with ur";
         }
         contsql="select d.name,d.bankaccno,d.bankaccname from lacom d where d.agentcom='"+tLAChargeSchema.getAgentCom()+"'";
         SSRS tres=mExeSQL.execSQL(contsql);
         if(tres!=null && tres.MaxRow > 0){
        	 mOutxml.setNameAndValue("BankName", tres.GetText(1, 1));
        	 System.out.println(tres.GetText(1, 1));
        	 mOutxml.setNameAndValue("AccountNumber", tres.GetText(1, 2));
        	 mOutxml.setNameAndValue("AccountName", tres.GetText(1, 3));
         }
         else{
        	 CError tError = new CError();
        	 tError.moduleName = "ContInputChargecomChkBL";
        	 tError.functionName = "dealData";
        	 tError.errorMessage = "保单查询失败！";
        	 this.mErrors.addOneError(tError);
        	 return false;
         }
         String contsql1="select b.payyears from lacommision b where b.commisionsn='"+tLAChargeSchema.getCommisionSN()+"'";
         SSRS tre=mExeSQL.execSQL(contsql1);
         if(tre!=null && tre.MaxRow > 0){
        	 mOutxml.setNameAndValue("CurryPayPeriod", tre.GetText(1, 1));
         }else{
        	 CError tError = new CError();
        	 tError.moduleName = "ContInputChargecomChkBL";
        	 tError.functionName = "dealData";
        	 tError.errorMessage = "保单查询失败！";
        	 this.mErrors.addOneError(tError);
        	 return false;
         }
         String sql1="select c.risktype,c.riskname from lmriskapp c where c.riskcode='"+tLAChargeSchema.getRiskCode()+"'";
         SSRS tr=mExeSQL.execSQL(sql1);
         if(tr!=null && tr.MaxRow > 0){
        	 mOutxml.setNameAndValue("CoverageType", tr.GetText(1, 1));
        	 mOutxml.setNameAndValue("CoverageName", tr.GetText(1, 2));
         }else{
        	 CError tError = new CError();
        	 tError.moduleName = "ContInputChargecomChkBL";
        	 tError.functionName = "dealData";
        	 tError.errorMessage = "保单查询失败！";
        	 this.mErrors.addOneError(tError);
        	 return false;
         }
         String sql="select max(b.endorsementno) from LJAGetEndorse b,lacommision  a" 
             +" where b.actugetno=a.receiptno and b.ManageCom=a.ManageCom and b.RiskCode=a.RiskCode "
             +"and b.DutyCode=a.DutyCode and b.PayPlanCode=a.PayPlanCode "
             +"group by endorsementno "
             +"fetch first rows only with ur";
             SSRS tendorsementno=mExeSQL.execSQL(sql);
             String endorsementno=tendorsementno.encode();
         if("".equals(endorsementno)){
        	 mOutxml.setNameAndValue("PolicyType","01");
         }else if(!"".equals(endorsementno)){
        	 mOutxml.setNameAndValue("PolicyType","02");
         }
         return true;
 	}

 	public String queryChargeInfo(){
 		//根据统一工号调用webservice查询业务员信息
 		String tResponseXml="";
 		long mOldTimeMillis = System.currentTimeMillis();
 		long mCurTimeMillis = mOldTimeMillis;
 	     try
 	        {
 	            RPCServiceClient tRPCServiceClient = new RPCServiceClient();
 	            String url = "http://10.252.4.69:8080/jszjpt/services/SXGX_WS_SERVER?wsdl";
 	            String qName = "http://ws.sinosoft.com";
 	            String method = "ecService";
 	            Options options = tRPCServiceClient.getOptions();
 	            EndpointReference targetEPR = new EndpointReference(url);
 	            options.setTo(targetEPR);
 	            options.setTimeOutInMilliSeconds(120000);//
 	            QName opAddEntry = new QName(qName, method);
 	            String inputXml = getinputXml(); //这里是中介机构校验接口请求报文
 	            //String inputXml ="<?xml version=\"1.0\" encoding=\"UTF-8\"?><Packet type=\"REQUEST\" version=\"1.0\" channel=\"0\"><Head><RequestType>C01</RequestType></Head><Body><Policy><SerialNo>00000000000000000007</SerialNo><prtno>18150206002</prtno><AgencyCode>PC3200000592</AgencyCode><AgencyType>01</AgencyType><ComCode>PICC</ComCode><Coverage><CoverageType>H</CoverageType></Coverage><Coverage><CoverageType>H</CoverageType></Coverage><PolicyApplyDate>2015-02-06 15:23:11</PolicyApplyDate><PolicyFlag>01</PolicyFlag></Policy></Body></Packet>";
 	            String serialNo = "PICCjs_86320000";//这里是外部交易流水号，可为空
 	            Object[] opAddEntryArgs = new Object[] { inputXml,Chargetype,"C07",serialNo };
 	           System.out.println("################"+Chargetype);
 	            Class[] classes = new Class[] { String.class,String.class,String.class,String.class };
 	            mCurTimeMillis = System.currentTimeMillis();
 	    		cLogger.info("客户端准备数据完毕！耗时：" + (mCurTimeMillis - mOldTimeMillis) / 1000.0 + "s");

 	    		cLogger.info("开始调用WebService服务！" + url);
 	            tResponseXml = 
 	            (String) tRPCServiceClient.invokeBlocking(opAddEntry, opAddEntryArgs, classes)[0];
 	            mCurTimeMillis = System.currentTimeMillis();
 	    		cLogger.info("调用WebService服务成功！耗时：" + (mCurTimeMillis - mOldTimeMillis) / 1000.0 + "s");
 	    		System.out.println("返回的报文："+tResponseXml);
 	        }
 	        catch (Exception e)
 	        {
 	            e.printStackTrace();
 	            CError tError = new CError();
 	            tError.moduleName = "ContInputChargecomChkBL";
 	            tError.functionName = "getInputData";
 	            tError.errorMessage = "发送校验报文失败!";
 	            this.mErrors.addOneError(e.getMessage());
 	        }
 		return tResponseXml;
 	}
 	
     private String getinputXml(){ 
     	Element tRootData = new Element("Packet");
         tRootData.addAttribute("type", "REQUEST");
         tRootData.addAttribute("version", "1.0");
         tRootData.addAttribute("channel",(String)mOutxml.getValueByName("channel"));
         Element tHeadData = new Element("Head");
         Element tRequestType = new Element("RequestType");
         tRequestType.setText("C07");
         tHeadData.addContent(tRequestType);
         tRootData.addContent(tHeadData);

         Element tbody=new Element("Body");
         Element tCommission = new Element("Commission");
         Element tAgencyCode = new Element("AgencyCode");
         tAgencyCode.setText((String)mOutxml.getValueByName("AgencyCode"));
         tCommission.addContent(tAgencyCode);
         Element tBatchNo = new Element("BatchNo");
         //tBatchNo.setText((String)mOutxml.getValueByName("BatchNo"));
         tBatchNo.setText("20150330863200000001");
         tCommission.addContent(tBatchNo);
         Element tStatementsComCode = new Element("StatementsComCode");
         tStatementsComCode.setText((String)mOutxml.getValueByName("StatementsComCode"));
         tCommission.addContent(tStatementsComCode);
         Element tPayComCode = new Element("PayComCode");
         tPayComCode.setText((String)mOutxml.getValueByName("PayComCode"));
         tCommission.addContent(tPayComCode);
         Element tCurrencyCode = new Element("CurrencyCode");
         tCurrencyCode.setText("CNY");
         tCommission.addContent(tCurrencyCode);
         Element tBankName = new Element("BankName");
        tBankName.setText((String)mOutxml.getValueByName("BankName"));
         tCommission.addContent(tBankName);
         Element tAccountNumber = new Element("AccountNumber");
         tAccountNumber.setText((String)mOutxml.getValueByName("AccountNumber"));
         tCommission.addContent(tAccountNumber);
         Element tAccountName = new Element("AccountName");
         tAccountName.setText((String)mOutxml.getValueByName("AccountName"));
         tCommission.addContent(tAccountName);
         
         Element tCommissionItem = new Element("CommissionItem");
         Element tPolicyType = new Element("PolicyType");
         tPolicyType.setText((String)mOutxml.getValueByName("PolicyType"));
         tCommissionItem.addContent(tPolicyType);
         Element tPolicyEndorsementNo = new Element("PolicyEndorsementNo");
         tPolicyEndorsementNo.setText((String)mOutxml.getValueByName("PolicyEndorsementNo"));
         tCommissionItem.addContent(tPolicyEndorsementNo);
         Element tCostType = new Element("CostType");
         tCostType.setText((String)mOutxml.getValueByName("CostType"));
         tCommissionItem.addContent(tCostType);
         Element tPayType = new Element("PayType");
         tPayType.setText((String)mOutxml.getValueByName("PayType").toString());
         tCommissionItem.addContent(tPayType);
         Element tCurryPayPeriod = new Element("CurryPayPeriod");
         tCurryPayPeriod.setText((String)mOutxml.getValueByName("CurryPayPeriod"));
         tCommissionItem.addContent(tCurryPayPeriod);
         
         Element tCoverage = new Element("Coverage");
         Element tCoverageType = new Element("CoverageType");
         tCoverageType.setText((String)mOutxml.getValueByName("CoverageType"));
         tCoverage.addContent(tCoverageType);
         Element tCoverageCode = new Element("CoverageCode");
         tCoverageCode.setText((String)mOutxml.getValueByName("CoverageCode"));
         tCoverage.addContent(tCoverageCode);
         Element tCoverageName = new Element("CoverageName");
         tCoverageName.setText((String)mOutxml.getValueByName("CoverageName"));
         tCoverage.addContent(tCoverageName);
         Element tPremium = new Element("Premium");
         tPremium.setText((String)mOutxml.getValueByName("Premium").toString());
         tCoverage.addContent(tPremium);
         Element tCommissionRate = new Element("CommissionRate");
         tCommissionRate.setText((String)mOutxml.getValueByName("CommissionRate").toString());
         tCoverage.addContent(tCommissionRate);
         Element tCommissionCharge = new Element("CommissionCharge");
         tCommissionCharge.setText((String)mOutxml.getValueByName("CommissionCharge").toString());
         tCoverage.addContent(tCommissionCharge);
         tCommissionItem.addContent(tCoverage);
         
         Element tPolicyCharge = new Element("PolicyCharge");
         tPolicyCharge.setText("10");
         tCommissionItem.addContent(tPolicyCharge);
         tCommission.addContent(tCommissionItem);
         
         tbody.addContent(tCommission);
         tRootData.addContent(tbody);
         Document tDocument = new Document(tRootData);
         JdomUtil.print(tDocument.getRootElement());
         return DomtoString(tDocument);   
     }  
	private String DomtoString(Document doc){
     	XMLOutputter xmlout = new XMLOutputter();
     	ByteArrayOutputStream bo = new ByteArrayOutputStream();
     	try {
     		xmlout.setEncoding("GBK");
 			xmlout.output(doc,bo);
 		} catch (IOException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
     	String xmlStr = bo.toString().replace("GBK", "UTF-8");
     	System.out.println("发送报文："+xmlStr);
     return xmlStr;	
     }
     public static void main(String[] args) {
    	 ContInputChargecomChkBL tDealQueryChargeInfoBL = new ContInputChargecomChkBL();
     	 LAChargeDB tLAChargeDB = new LAChargeDB();
     	String tStr="select * from lacharge where managecom='86320000' and agentcom='PC3200000384' fetch first  rows only with ur";
     	tDealQueryChargeInfoBL.mLAChargeSet=tLAChargeDB.executeQuery(tStr);
    	//System.out.println("aaaaaaaaaaaa:"+tStr);
     	VData tVData = new VData();
     	for (int i = 1; i <= tDealQueryChargeInfoBL.mLAChargeSet.size(); i++)
        { 
     		tDealQueryChargeInfoBL.tLAChargeSchema=tDealQueryChargeInfoBL.mLAChargeSet.get(i);
     		System.out.println("eeeeeeeeeee"+tDealQueryChargeInfoBL.mLAChargeSet.size());
     		//tTransferData.setNameAndValue("contno","2200682780");
       	  tVData.add(tDealQueryChargeInfoBL.tLAChargeSchema);
       	  System.out.println("ffffffffffffff"+tVData.size());
        }
     	 String chargetype="ZX";
     	tVData.add(chargetype);
     	System.out.println("wwwwwww");
    	 tDealQueryChargeInfoBL.submitData(tVData, "check");
     	//System.out.println("ggggggggggggggg:"+tVData.size());
 	}
 }
