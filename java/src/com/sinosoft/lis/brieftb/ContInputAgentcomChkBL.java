package com.sinosoft.lis.brieftb;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.namespace.QName;

import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.yibaotong.JdomUtil;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
/**
 * 
 * @author zjd
 * 江苏中介校验
 */
public class ContInputAgentcomChkBL {
	private Logger cLogger = Logger.getLogger(getClass());
	
	/** 往工作流引擎中传输数据的容器 */

    private TransferData mTransferData = new TransferData();
    private VData mInputData=new VData();
	//传输数据类
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /**
     * 发送报文的保单数据封装类
     */
    private TransferData mOutxml=new TransferData();
    /**
     * 前台传来的保单号
     */
    private String mContno="";
    private ExeSQL mExeSQL=new ExeSQL();
	public boolean submitData(VData cInputData, String cOperate) {
	        //首先将数据在本类中做一个备份
	        mInputData = (VData) cInputData.clone();
	        System.out.println("Start ContInputAgentcomChkBL Submit...");
	        if (!getInputData(mInputData, cOperate))
	        {
	            return false;
	        }

	        if (!checkData())
	        {
	            return false;
	        }

	        try {
				if (!dealData())
				{
				    return false;
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
	       
	        System.out.println("End ContInputAgentcomChkBL Submit...");

	        mInputData = null;
	        return true;
	    }
	private boolean checkData(){
		
		return true;
	}
	private boolean dealData() throws FileNotFoundException{
		String tresult=queryAgentInfo();	
		if(!"".equals(tresult) && tresult!=null){
			Document tresDocument=getDocument(tresult.toString());
			if(tresDocument!=null){
				Element tall=tresDocument.getRootElement();
				Element thead=tall.getChild("Head");
				System.out.println("tal:"+tall.getChild("Head").getChildText("ErrorCode"));
				Element terrorcode=thead.getChild("ErrorCode");
				Element tresponsecode=thead.getChild("ResponseCode");
				Element tbody=tall.getChild("Body");
				System.out.println("errorcode:"+terrorcode.getText());
				if(tresponsecode!=null && !"".equals(tresponsecode.getText()) && "1".equals(tresponsecode.getText()) ){
					System.out.print("江苏中介接口返回成功！");
					return true;
				}else if(tresponsecode!=null && !"".equals(tresponsecode.getText()) && !"1".equals(tresponsecode.getText()) && "9999".equals(thead.getChildText("ErrorCode"))){
				     CError tError = new CError();
					tError.moduleName = "ContInputAgentcomChkBL";
		            tError.functionName = "dealData";
		            tError.errorMessage = "江苏中介机构查询失败！错误信息为："+tbody.getChild("Error").getChildText("ErrorCode")+"-"+tbody.getChild("Error").getChildText("ErrorMessage");
		            this.mErrors.addOneError(tError);
		            return false;
				}else if(tresponsecode!=null && !"".equals(tresponsecode.getText()) && !"1".equals(tresponsecode.getText())){
				     CError tError = new CError();
					tError.moduleName = "ContInputAgentcomChkBL";
		            tError.functionName = "dealData";
		            tError.errorMessage = "江苏中介机构查询失败！错误信息为："+thead.getChildText("ErrorCode")+"-"+thead.getChildText("ErrorMessage");
		            this.mErrors.addOneError(tError);
		            return false;
				}else{
					CError tError = new CError();
		            tError.moduleName = "ContInputAgentcomChkBL";
		            tError.functionName = "dealData";
		            tError.errorMessage = "查询返回报文获取失败！";
		            this.mErrors.addOneError(tError);
		            return false;
				}
			}else{
				    CError tError = new CError();
		            tError.moduleName = "ContInputAgentcomChkBL";
		            tError.functionName = "dealData";
		            tError.errorMessage = "查询返回报文获取失败！";
		            this.mErrors.addOneError(tError);
		            return false;
			}
		}else{
			System.out.println("获取返回报文失败！");
			CError tError = new CError();
            tError.moduleName = "ContInputAgentcomChkBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询返回报文获取失败！";
            this.mErrors.addOneError(tError);
			return false;
		}
		
	}
	
	private Document getDocument(String tstr){
		java.io.Reader in = new StringReader(tstr);  
        Document doc=null;
		try {
			doc = (new SAXBuilder()).build(in);
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}         
        return doc; 
	}
	private boolean getInputData(VData cInputData,String cOperate){
		//从输入数据中得到所有对象
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        mInputData = cInputData;
        mContno=(String)mTransferData.getValueByName("ContNo");
        if (mContno == null || "".equals(mContno))
        {
            // @@错误处理
        	CError tError = new CError();
            tError.moduleName = "ContInputAgentcomChkBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "保单号获取失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        //处理发送基础数据封装
        //获取保单类型
        String tcontsql="select prtno,"
        	           +" (select AgentOrganCode from lacom where agentcom = lc.agentcom),"
        	           +" (select othersign from ldcode where codetype = 'agencytype' and code = (select agentorgancode from lacom where lc.agentcom = agentcom)) ,"
        	           +" polapplydate,maketime,managecom,'0' "
        	           +" from lcgrpcont lc where grpcontno='"+mContno+"' "
        		       +" union " 
        		       +" select prtno,"
        		       +" (select AgentOrganCode from lacom where agentcom = lc.agentcom),"
        		       +" (select othersign from ldcode where codetype = 'agencytype' and code = (select agentorgancode from lacom where lc.agentcom = agentcom)) ,"
        		       +" polapplydate,maketime,managecom,'1' "
        		       +" from lccont lc where contno='"+mContno+"' ";
        SSRS tres=mExeSQL.execSQL(tcontsql);
        if(tres!=null && tres.MaxRow > 0){
        	mOutxml.setNameAndValue("prtno", tres.GetText(1, 1));
        	mOutxml.setNameAndValue("AgencyCode", tres.GetText(1, 2));
        	mOutxml.setNameAndValue("AgencyType", tres.GetText(1, 3));
        	mOutxml.setNameAndValue("PolicyApplyDate", tres.GetText(1, 4)+" "+tres.GetText(1, 5));
        	mOutxml.setNameAndValue("ComCode", tres.GetText(1, 6));
        	mOutxml.setNameAndValue("channel", tres.GetText(1, 7));
        }else{
        	CError tError = new CError();
            tError.moduleName = "ContInputAgentcomChkBL";
            tError.functionName = "dealData";
            tError.errorMessage = "保单查询失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
		
		String trisksql="select (select risktype from lmriskapp lm where lm.riskcode=lp.riskcode) from lcgrppol lp where lp.grpcontno='"+mContno+"'" +
				        " union " +
				        " select  (select risktype from lmriskapp lm where lm.riskcode=lp.riskcode) from lcpol lp where lp.contno='"+mContno+"' ";
		SSRS triskres=mExeSQL.execSQL(trisksql);
        if(triskres!=null && triskres.MaxRow > 0){
        	String[] trisklist=new String[triskres.MaxRow];
        	for(int i=1;i<=triskres.MaxRow;i++){
        		trisklist[i-1]=triskres.GetText(i, 1);
        	}
        	mOutxml.setNameAndValue("Coverage", trisklist);
        }else{
			CError tError = new CError();
            tError.moduleName = "ContInputAgentcomChkBL";
            tError.functionName = "dealData";
            tError.errorMessage = "险种信息查询失败！";
            this.mErrors.addOneError(tError);
            return false;
		}
		return true;
	}
	public String queryAgentInfo(){
		//根据同意工号调用webservice查询业务员信息
		String tResponseXml="";
		long mOldTimeMillis = System.currentTimeMillis();
		long mCurTimeMillis = mOldTimeMillis;
	     try
	        {
	            RPCServiceClient tRPCServiceClient = new RPCServiceClient();
	            String url = new ExeSQL().getOneValue("select codename from ldcode where codetype='JSAgentComChk' and code='url'");
	            String qName = new ExeSQL().getOneValue("select codename from ldcode where codetype='JSAgentComChk' and code='qName'");
	            String method = new ExeSQL().getOneValue("select codename from ldcode where codetype='JSAgentComChk' and code='method'");
	            Options options = tRPCServiceClient.getOptions();
	            EndpointReference targetEPR = new EndpointReference(url);
	            options.setTo(targetEPR);
	            options.setTimeOutInMilliSeconds(120000);//
	            QName opAddEntry = new QName(qName, method);
	            String inputXml = getinputXml(); //这里是中介机构校验接口请求报文
	            //String inputXml ="<?xml version=\"1.0\" encoding=\"UTF-8\"?><Packet type=\"REQUEST\" version=\"1.0\" channel=\"0\"><Head><RequestType>C01</RequestType></Head><Body><Policy><SerialNo>00000000000000000007</SerialNo><prtno>18150206002</prtno><AgencyCode>PC3200000592</AgencyCode><AgencyType>01</AgencyType><ComCode>PICC</ComCode><Coverage><CoverageType>H</CoverageType></Coverage><Coverage><CoverageType>H</CoverageType></Coverage><PolicyApplyDate>2015-02-06 15:23:11</PolicyApplyDate><PolicyFlag>01</PolicyFlag></Policy></Body></Packet>";
	            String serialNo = "PICCjs_86320000";//这里是外部交易流水号，可为空
	            Object[] opAddEntryArgs = new Object[] { inputXml,"HX","C01",serialNo };
	            Class[] classes = new Class[] { String.class,String.class,String.class,String.class };
	            mCurTimeMillis = System.currentTimeMillis();
	    		cLogger.info("客户端准备数据完毕！耗时：" + (mCurTimeMillis - mOldTimeMillis) / 1000.0 + "s");

	    		cLogger.info("开始调用WebService服务！" + url);
	            tResponseXml = 
	            (String) tRPCServiceClient.invokeBlocking(opAddEntry, opAddEntryArgs, classes)[0];
	            mCurTimeMillis = System.currentTimeMillis();
	    		cLogger.info("调用WebService服务成功！耗时：" + (mCurTimeMillis - mOldTimeMillis) / 1000.0 + "s");
	    		System.out.println("返回的报文："+tResponseXml);
	        }
	        catch (Exception e)
	        {
	            e.printStackTrace();
	            CError tError = new CError();
	            tError.moduleName = "ContInputAgentcomChkBL";
	            tError.functionName = "getInputData";
	            tError.errorMessage = "发送校验报文失败!";
	            this.mErrors.addOneError(e.getMessage());
	        }
		return tResponseXml;
	}
	
    private String getinputXml(){ 
    	Element tRootData = new Element("Packet");
        tRootData.addAttribute("type", "REQUEST");
        tRootData.addAttribute("version", "1.0");
        tRootData.addAttribute("channel",(String)mOutxml.getValueByName("channel"));
        Element tHeadData = new Element("Head");
        Element tRequestType = new Element("RequestType");
        tRequestType.setText("C01");
        tHeadData.addContent(tRequestType);
        tRootData.addContent(tHeadData);

        Element tbody=new Element("Body");
        Element tPolicyData = new Element("Policy");
        Element tSerialNo = new Element("SerialNo");
        tSerialNo.setText(PubFun1.CreateMaxNo("JXZJSERNO", 10));
        tPolicyData.addContent(tSerialNo);
        Element tprtno = new Element("prtno");
        tprtno.setText((String)mOutxml.getValueByName("prtno"));
        tPolicyData.addContent(tprtno);
        Element tChannel = new Element("Channel");
        tChannel.setText((String)mOutxml.getValueByName("channel"));
        tPolicyData.addContent(tChannel);
        Element tAgencyCode = new Element("AgencyCode");
        tAgencyCode.setText((String)mOutxml.getValueByName("AgencyCode"));
        tPolicyData.addContent(tAgencyCode);
        Element tAgencyType = new Element("AgencyType");
        tAgencyType.setText((String)mOutxml.getValueByName("AgencyType"));
        tPolicyData.addContent(tAgencyType);
        Element tComCode = new Element("ComCode");
        tComCode.setText((String)mOutxml.getValueByName("ComCode"));
//        tComCode.setText("86320000");//临时测试使用
        tPolicyData.addContent(tComCode);
        String[] trisklist=(String[])mOutxml.getValueByName("Coverage");
        for(int i=0;i<trisklist.length;i++){
        	Element tCoverage = new Element("Coverage");
        	Element tCoverageType = new Element("CoverageType");
        	tCoverageType.setText(trisklist[i]);
        	tCoverage.addContent(tCoverageType);
        	tPolicyData.addContent(tCoverage);
        }
        Element tPolicyApplyDate = new Element("PolicyApplyDate");
        tPolicyApplyDate.setText((String)mOutxml.getValueByName("PolicyApplyDate").toString().trim());
        tPolicyData.addContent(tPolicyApplyDate);
        Element tPolicyFlag = new Element("PolicyFlag");
        tPolicyFlag.setText("01");//固定 契约部分全是新单
        tPolicyData.addContent(tPolicyFlag);
        tbody.addContent(tPolicyData);
        tRootData.addContent(tbody);
        Document tDocument = new Document(tRootData);
        JdomUtil.print(tDocument.getRootElement());
        return DomtoString(tDocument);   
    }  
    
    private String DomtoString(Document doc){
    	XMLOutputter xmlout = new XMLOutputter();
    	ByteArrayOutputStream bo = new ByteArrayOutputStream();
    	try {
			xmlout.output(doc,bo);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	String xmlStr = bo.toString();
    	System.out.println("发送报文："+xmlStr);
    return xmlStr;	
    }
    
    public static void main(String[] args) {
    	ContInputAgentcomChkBL tDealQueryAgentInfoBL = new ContInputAgentcomChkBL();
    	  TransferData tTransferData = new TransferData();
    	  tTransferData.setNameAndValue("ContNo","13032369645");
    	  VData tVData = new VData();
    	  tVData.add(tTransferData);
    	  tDealQueryAgentInfoBL.submitData(tVData, "check");
	}

}
