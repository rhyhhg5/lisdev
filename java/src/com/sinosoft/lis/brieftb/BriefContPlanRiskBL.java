package com.sinosoft.lis.brieftb;

import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class BriefContPlanRiskBL {

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 往前面传输数据的容器 */
    private VData mResult = new VData();
    private GlobalInput mGlobalInput = null;
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
    private LCContPlanRiskSet mLCContPlanRiskSet = new LCContPlanRiskSet();
    private LCContPlanSchema mLCContPlanSchema = new LCContPlanSchema();
    private TransferData tTransferData = new TransferData();
    /**传输到后台处理的map*/
    private MMap map = new MMap();
    /** 数据操作字符串 */
    private String mOperate = "";
    private String mGrpContNo = "";
    private String CValiDate = "";
    private String CInValiDate = "";
    /** 保险计划要素信息　*/
    private LCContPlanDutyParamSet mLCContPlanDutyParamSet = new LCContPlanDutyParamSet();
    public BriefContPlanRiskBL() {
    }

    public static void main(String[] args) {
        BriefContPlanRiskBL briefcontplanriskbl = new BriefContPlanRiskBL();
    }

    /**
     * ＵＩ接口
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        mResult = cInputData;
        mOperate = cOperate;
        if (!getInputData()) {
            return false;
        }
        if (!checkData()) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubsubmit = new PubSubmit();
        if (!tPubsubmit.submitData(this.mResult, "INSERT")) {
            this.mErrors.copyAllErrors(tPubsubmit.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 从前台获取数据
     * @return boolean
     */
    private boolean getInputData() {
        mLCContPlanDutyParamSet = (LCContPlanDutyParamSet) mResult.getObjectByObjectName(
                "LCContPlanDutyParamSet", 0);
        mGlobalInput = (GlobalInput) mResult.getObjectByObjectName("GlobalInput", 0);
        tTransferData = (TransferData) mResult.getObjectByObjectName("TransferData", 0);
        CValiDate = (String) tTransferData.getValueByName("CValiDate");
        CInValiDate = (String) tTransferData.getValueByName("CInValiDate");
        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData() {
        if (mLCContPlanDutyParamSet.size() <= 0) {
            System.out.println("程序第67行出错，请检查BriefContPlanRiskBL.java中的checkData方法！");
            CError tError = new CError();
            tError.moduleName = "BriefContPlanRiskBL.java";
            tError.functionName = "checkData";
            tError.errorMessage = "要素信息传入失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        mGrpContNo = mLCContPlanDutyParamSet.get(1).getGrpContNo();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        if (!tLCGrpContDB.getInfo()) {
            System.out.println("程序第84行出错，请检查BriefContPlanRiskBL.java中的checkData方法！");
            CError tError = new CError();
            tError.moduleName = "BriefContPlanRiskBL.java";
            tError.functionName = "checkData";
            tError.errorMessage = "查询团体合同信息失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCGrpContSchema.setSchema(tLCGrpContDB.getSchema());
        return true;
    }

    /**
     * 数据处理
     * @return boolean
     */
    private boolean dealData() {
        System.out.println("Operate : " + this.mOperate);
        if (this.mOperate.equals("INSERT||CONTPLAN")) {
            if (!insertData())
                return false;
        } else if (this.mOperate.equals("DELETE||CONTPLAN")) {
            if (!deleteData())
                return false;
        } else if (this.mOperate.equals("UPDATE||CONTPLAN")) {
            if (!updateData())
                return false;
        }
        return true;
    }

    /**
     * 准备向后台传输
     * @return boolean
     */
    private boolean prepareOutputData() {
        if (this.mOperate.equals("INSERT||CONTPLAN")) {
            this.map.put(this.mLCContPlanDutyParamSet, "INSERT");
            this.map.put(this.mLCContPlanRiskSet, "INSERT");
            this.map.put(this.mLCContPlanSchema, "INSERT");
            this.map.put(this.mLCGrpPolSet, "INSERT");
        }
        if (!this.mOperate.equals("DELETE||CONTPLAN")) {
            this.map.put("update lcgrpcont set CValiDate='" + CValiDate + "',CInValiDate='" +
                         CInValiDate + "'  where grpcontno='" + this.mGrpContNo + "'", "UPDATE");
        }
        System.out.println("map : " + map.size());
        if (map.size() > 0) {
            this.mResult.add(this.map);
        }
        return true;
    }

    /**
     * 创建险种信息
     * @return boolean
     * @param cRiskCode String
     */
    private String creatPolInfo(String cRiskCode) {
        LMRiskDB tLMRiskDB = new LMRiskDB();
        tLMRiskDB.setRiskCode(cRiskCode);
        if (!tLMRiskDB.getInfo()) {
            System.out.println("程序第137行出错，请检查BriefContPlanRiskBL.java中的creatPolInfo方法！");
            CError tError = new CError();
            tError.moduleName = "BriefContPlanRiskBL.java";
            tError.functionName = "creatPolInfo";
            tError.errorMessage = "查询险种失败！";
            this.mErrors.addOneError(tError);
            return null;
        }
        //首先险种表lcpol
        LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
        tLCGrpPolSchema.setRiskCode(cRiskCode);
        String tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
        String tNo = PubFun1.CreateMaxNo("GrpProposalNo", tLimit);
        tLCGrpPolSchema.setGrpPolNo(tNo); //如果是新增
        tLCGrpPolSchema.setGrpProposalNo(tNo);
        tLCGrpPolSchema.setPrtNo(mLCGrpContSchema.getPrtNo());
        tLCGrpPolSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        tLCGrpPolSchema.setSaleChnl(mLCGrpContSchema.getSaleChnl());
        tLCGrpPolSchema.setManageCom(mLCGrpContSchema.getManageCom());
        tLCGrpPolSchema.setAgentCom(mLCGrpContSchema.getAgentCom());
        tLCGrpPolSchema.setAgentType(mLCGrpContSchema.getAgentType());
        tLCGrpPolSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        tLCGrpPolSchema.setAgentGroup(mLCGrpContSchema.getAgentGroup());
        tLCGrpPolSchema.setCustomerNo(mLCGrpContSchema.getAppntNo());
        tLCGrpPolSchema.setAddressNo(mLCGrpContSchema.getAddressNo());
        tLCGrpPolSchema.setGrpName(mLCGrpContSchema.getGrpName());
        tLCGrpPolSchema.setCValiDate(this.CValiDate);
        tLCGrpPolSchema.setPayMode(mLCGrpContSchema.getPayMode());
        tLCGrpPolSchema.setAppFlag("0");
        tLCGrpPolSchema.setUWFlag("0");
        tLCGrpPolSchema.setApproveFlag("0");
        tLCGrpPolSchema.setModifyDate(PubFun.getCurrentDate());
        tLCGrpPolSchema.setModifyTime(PubFun.getCurrentTime());
        tLCGrpPolSchema.setOperator(mGlobalInput.Operator);
        tLCGrpPolSchema.setMakeDate(PubFun.getCurrentDate());
        tLCGrpPolSchema.setMakeTime(PubFun.getCurrentTime());
        tLCGrpPolSchema.setPayMode(this.mLCGrpContSchema.getPayMode());
        for (int i = 1; i <= this.mLCContPlanDutyParamSet.size(); i++) {
            if (tLCGrpPolSchema.getRiskCode().equals(mLCContPlanDutyParamSet.get(i).getRiskCode())) {
                if (mLCContPlanDutyParamSet.get(i).getCalFactor().equals("Mult")) {
                    tLCGrpPolSchema.setMult(mLCContPlanDutyParamSet.get(i).getCalFactorValue());
                }
            }
        }

        mLCGrpPolSet.add(tLCGrpPolSchema);
        /**
         * 处理完成险种信息,
         * 开始处理险种计划
         */
        LCContPlanRiskSchema tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setGrpContNo(this.mGrpContNo);
        tLCContPlanRiskSchema.setProposalGrpContNo(this.mGrpContNo);
        tLCContPlanRiskSchema.setMainRiskCode(cRiskCode);
        tLCContPlanRiskSchema.setMainRiskVersion(tLMRiskDB.getRiskVer());
        tLCContPlanRiskSchema.setRiskCode(cRiskCode);
        tLCContPlanRiskSchema.setRiskVersion(tLMRiskDB.getRiskVer());
        tLCContPlanRiskSchema.setContPlanCode("00");
        tLCContPlanRiskSchema.setContPlanName("默认计划");
        tLCContPlanRiskSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(tLCContPlanRiskSchema);
        tLCContPlanRiskSchema.setPlanType("0");
        mLCContPlanRiskSet.add(tLCContPlanRiskSchema);
        return tNo;
    }

    /**
     * 执行插入操作
     * @return boolean
     */
    private boolean insertData() {
        //首先确定几个险种并将所有的险种信息保存
        String RiskCode = mLCContPlanDutyParamSet.get(1).getRiskCode();
        String tPolNo = "";
        for (int i = 1; i <= this.mLCContPlanDutyParamSet.size(); i++) {
            //首先为第一个险种添加信息
            if (i == 1) {
                tPolNo = creatPolInfo(mLCContPlanDutyParamSet.get(i).getRiskCode());
                mLCContPlanDutyParamSet.get(i).setGrpPolNo(tPolNo);
            } else {
                if (!RiskCode.equals(mLCContPlanDutyParamSet.get(i).getRiskCode())) {
                    tPolNo = creatPolInfo(mLCContPlanDutyParamSet.get(i).getRiskCode());
                    mLCContPlanDutyParamSet.get(i).setGrpPolNo(tPolNo);
                    RiskCode = mLCContPlanDutyParamSet.get(i).getRiskCode();
                }
            }
            mLCContPlanDutyParamSet.get(i).setGrpPolNo(tPolNo);
        }
        PubFun.fillDefaultField(mLCContPlanDutyParamSet);
        /**
         * 以上各步骤完成计划信息,要素信息,险种信息操作
         * 开始准备处理保单计划
         */
        mLCContPlanSchema.setGrpContNo(this.mGrpContNo);
        mLCContPlanSchema.setProposalGrpContNo(this.mGrpContNo);
        mLCContPlanSchema.setContPlanCode("00");
        mLCContPlanSchema.setContPlanName("默认计划");
        mLCContPlanSchema.setPlanType("0");
        mLCContPlanSchema.setOperator(this.mGlobalInput.Operator);
        PubFun.fillDefaultField(mLCContPlanSchema);
        return true;
    }

    /**
     * 删除操作<br>
     * 1、先删除要素信息<br>
     * 2、删除险种计划<br>
     * 3、删除计划<br>
     * 4、删除险种
     * @return boolean
     */
    private boolean deleteData() {
        String deleteFactorInfoSql = "delete from LCContPlanDutyParam where grpcontno='" +
                                     this.mGrpContNo + "'";
        String deleteContPlanRiskSql = "delete from LCContPlanRisk where grpcontno='" +
                                       this.mGrpContNo + "'";
        String deleteContPlanSql = "delete from LCContPlan where grpcontno='" + this.mGrpContNo +
                                   "'";
        String deleteGrpPolSql = "delete from LCGrpPol where grpcontno='" + this.mGrpContNo + "'";
        this.map.put(deleteFactorInfoSql, "DELETE");
        this.map.put(deleteContPlanRiskSql, "DELETE");
        this.map.put(deleteContPlanSql, "DELETE");
        this.map.put(deleteGrpPolSql, "DELETE");
        return true;
    }

    private boolean updateData() {
        map.put("delete from LCContPlanDutyParam where grpcontno='" + this.mGrpContNo + "'",
                "DELETE");
        map.put(this.mLCContPlanDutyParamSet, "INSERT");
        return true;
    }
}
