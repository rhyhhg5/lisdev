package com.sinosoft.lis.brieftb;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class BriefSingleContInputUI {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    public BriefSingleContInputUI() {
    }

    public boolean submitData(VData nInputData, String cOperate) {
        BriefSingleContInputBL tBriefSingleContInputBL = new
                BriefSingleContInputBL();
        if (!tBriefSingleContInputBL.submitData(nInputData, cOperate)) {
            this.mErrors.copyAllErrors(tBriefSingleContInputBL.mErrors);
            return false;
        } else {
            mResult = tBriefSingleContInputBL.getResult();
        }
        return true;
    }

    /**
     * �������
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }
}
