package com.sinosoft.lis.brieftb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.workflow.tb.*;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class BriefCardSignBL {
    public BriefCardSignBL() {
    }

    /** 传入参数 */
    private VData mInputData;
    /** 传入操作符 */
    private String mOperate;
    /** 登陆信息 */
    private GlobalInput mGlobalInput;
    /** 报错存储对象 */
    public CErrors mErrors = new CErrors();
    /** 最后保存结果 */
    private VData mResult = new VData();
    /** 最后递交Map */
    private MMap map = new MMap();
    /** 记录传递参数 */
    private TransferData mTransferData;
    /** 合同信息 */
    private LCContSchema mLCContSchema;
    /** 实收表主键 */
    private String mPayNo;
    /** 实收个人表 */
    private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();
    /** 是否存在暂收数据 */
    private boolean isTempFee = true;
    /** 实收总表 */
    private LJAPaySchema mLJAPaySchema = new LJAPaySchema();

    /**
     * submitData
     *
     * @param nInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData nInputData, String cOperate) {
        System.out.println("into BriefCardSignBL...");
        this.mInputData = nInputData;
        this.mOperate = cOperate;

        if (!getInputData()) {
            return false;
        }
        if (!checkData()) {
            return false;
        }
        if (!dealData()) {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(this.mResult, "INSERT")) {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        System.out.println("BriefCardSignBL finished...");
        /** 成功后短信提醒 */
        dealMassage();

        return true;
    }

    /**
     * dealMassage
     *
     * @return boolean
     */
    private boolean dealMassage() {
        /**
         * add by yangming
         * 只在生产环境上发送短信，其他环境一律不发送短信
         */
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("Mobile");
        if (tLDSysVarDB.getInfo() == false) {
            return false;
        }
        if (tLDSysVarDB.getSysVarValue() != null && tLDSysVarDB.getSysVarValue().equals("1")) {
            LCContDB tLCContDB = new LCContDB();
            tLCContDB.setProposalContNo(this.mLCContSchema.getProposalContNo());
            LCContSet tLCContSet = tLCContDB.query();
            if (tLCContSet != null && tLCContSet.size() > 0) {
                mLCContSchema = tLCContSet.get(1);
            }
            LCAppntDB tLCAppntDB = new LCAppntDB();
            tLCAppntDB.setContNo(mLCContSchema.getContNo());
            if (!tLCAppntDB.getInfo()) {
                return false;
            }
            LCAddressDB tLCAddressDB = new LCAddressDB();
            tLCAddressDB.setAddressNo(tLCAppntDB.getAddressNo());
            tLCAddressDB.setCustomerNo(tLCAppntDB.getAppntNo());
            if (!tLCAddressDB.getInfo()) {
                return false;
            }
            if (tLCAddressDB.getMobile() == null || tLCAddressDB.getMobile().equals("")) {
                return false;
            }

            String tAppntName = tLCAppntDB.getAppntName();
            String tAppntSex = getSex(tLCAppntDB.getAppntSex());
            String tRiskName = getRiskName();
            String tInsuredName = mLCContSchema.getInsuredName();
            String tCValiDate = mLCContSchema.getCValiDate();
            String tInsuredIDNo = mLCContSchema.getInsuredIDNo();
            String tInsuredSex = getSex(mLCContSchema.getInsuredSex());

            StringBuffer msgAppnt = new StringBuffer(255);
            StringBuffer msgInsured = new StringBuffer(255);

            msgAppnt.append("您为 ")
                    .append(tInsuredName)
                    .append("（证件号")
                    .append(tInsuredIDNo)
                    .append("）投保的")
                    .append(tRiskName)
                    .append("将于")
                    .append(tCValiDate)
                    .append("正式生效。－中国人保健康");
            try {
                PubFun.sendMessage(tLCAddressDB.getMobile(), msgAppnt.toString());
            } catch (Exception ex) {
                ex.printStackTrace();
            }

//            if (this.mLCContSchema.getInsuredNo() != null && this.mLCContSchema.getAppntNo() != null
//                && !this.mLCContSchema.getInsuredNo().equals(this.mLCContSchema.getAppntNo())) {
//                LCInsuredDB tLCInsuredDB = new LCInsuredDB();
//                tLCInsuredDB.setContNo(this.mLCContSchema.getContNo());
//                tLCInsuredDB.setInsuredNo(this.mLCContSchema.getInsuredNo());
//                if (!tLCInsuredDB.getInfo()) {
//                    return false;
//                }
//
//                tLCAddressDB = new LCAddressDB();
//                tLCAddressDB.setCustomerNo(tLCInsuredDB.getInsuredNo());
//                tLCAddressDB.setAddressNo(tLCInsuredDB.getAddressNo());
//                if (!tLCAddressDB.getInfo()) {
//                    return false;
//                }
//                if (tLCAddressDB.getMobile() == null || tLCAddressDB.getMobile().equals("")) {
//                    return false;
//                }
//                msgInsured.append("尊敬的 ")
//                        .append(tInsuredName)
//                        .append(tInsuredSex)
//                        .append("，")
//                        .append(tAppntName)
//                        .append("已为您在我公司投保了")
//                        .append(tRiskName)
//                        .append("，您的证件号是")
//                        .append(tInsuredIDNo)
//                        .append("，保单生效日为")
//                        .append(tCValiDate)
//                        .append("，如有疑问可致电4006695518（中国人保健康）");
//                try {
//                    PubFun.sendMessage(tLCAddressDB.getMobile(), msgInsured.toString());
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//
//            }

        }
        return false;
    }


    public void testdealMassage() {
        mLCContSchema = new LCContSchema();
        mLCContSchema.setProposalContNo("13000496717");
        dealMassage();
    }

    /**
     * getSex
     *
     * @param tSex String
     * @return String
     */
    private String getSex(String tSex) {
        return tSex.equals("1") ? "女士" : "先生";
    }

    /**
     * getRiskName
     *
     * @return String
     */
    private String getRiskName() {
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(this.mLCContSchema.getContNo());
        LCPolSet tLCPolSet = tLCPolDB.query();
        String tRiskName = "";
        if (tLCPolSet != null && tLCPolSet.size() > 0) {
            for (int i = 1; i <= tLCPolSet.size(); i++) {
                String tRiskCode = tLCPolSet.get(i).getRiskCode();
                CachedRiskInfo tCRI = CachedRiskInfo.getInstance();
                tRiskName += tCRI.findRiskByRiskCode(tRiskCode).getRiskShortName();
                if (i < tLCPolSet.size()) {
                    tRiskName += ",";
                }
            }
        }
        return tRiskName;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        System.out.println("into BriefCardSignBL.getInputData()...");
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLCContSchema = (LCContSchema) mInputData.getObjectByObjectName(
                "LCContSchema", 0);
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        System.out.println("into BriefCardSignBL.checkData()...");
        if (this.mGlobalInput == null) {
            String str = "登陆信息为null，可能是页面超时，请重新登陆!";
            buildError("checkData", str);
            return false;
        }
        if (mLCContSchema == null || mLCContSchema.getContNo() == null ||
            mLCContSchema.getContNo().equals("")) {
            String str = "传入合同信息不完整!";
            buildError("checkData", str);
            System.out.println("在程序BriefCardSignBL.checkData - 104 :" + str);
            return false;
        }
        LCContDB tLCContDB = mLCContSchema.getDB();
        if (!tLCContDB.getInfo()) {
            String str = "查询要签单的投保信息失败!";
            buildError("checkData", str);
            System.out.println("在程序BriefCardSignBL.checkData() - 112 : " + str);
            return false;
        }

        this.mLCContSchema.setSchema(tLCContDB);

        if (this.mLCContSchema.getAppFlag() == null ||
            !this.mLCContSchema.getAppFlag().equals("0")) {
            String str = "此卡单状态不正确允许进行签单激活动作!";
            buildError("checkData", str);
            System.out.println("在程序BriefCardSignBL.checkData() - 122 : " + str);
            return false;
        }

        if (mTransferData == null || mTransferData.getValueByName("ContNo") == null) {
            String str = "传入必要参数为null!";
            buildError("checkData", str);
            System.out.println("在程序BriefCardSignBL.checkData() - 135 : " + str);
            return false;
        }
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        System.out.println("into BriefCardSignBL.dealData()...");

        PreviewPrintAfterInitService tPreviewPrintAfterInitService = new
                                                                     PreviewPrintAfterInitService();
        /** 思路上先进性预打保单动作，完成合同信息的生成，生成完成之后处理财务实收保费问题 */
        boolean result = false;
        try {
            result = tPreviewPrintAfterInitService.submitData(mInputData, "");
        } catch (Exception ex) {
            String str = "生成保单过程中发生意外错误!" + ex.getMessage();
            buildError("dealData", str);
            System.out.println("在程序BriefCardSignBL.dealData() - 159 : " + str);
            return false;
        }
        if (!result) {
            this.mErrors.copyAllErrors(tPreviewPrintAfterInitService.mErrors);
            return false;
        }
        this.mResult = tPreviewPrintAfterInitService.getResult();
//        /** 生成财务实收数据 */
//        if (!ceratFin()) {
//            return false;
//        }

        if (!dealCardInfo()) {
            return false;
        }
        return true;
    }

    /**
     * dealCardInfo
     *
     * @return boolean
     */
    private boolean dealCardInfo() {
        String strSql =
                "select a.* from lzcard a,lzcardnumber b where a.subcode=b.cardtype and a.startno=b.cardserno and cardno='" +
                mLCContSchema.getPrtNo() + "'";
        LZCardDB tLZCardDB = new LZCardDB();
        LZCardSet tLZCardSet = tLZCardDB.executeQuery(strSql);
        if (tLZCardSet == null || tLZCardSet.size() <= 0) {
            String str = "没有查出单证资料!";
            buildError("dealCardInfo", str);
            System.out.println("在程序BriefCardSignBL.dealCardInfo() - 211 : " +
                               str);
            return false;
        }
        if (tLZCardSet.size() != 1) {
            String str = "单证信息不唯一!";
            buildError("dealCardInfo", str);
            System.out.println("在程序BriefCardSignBL.dealCardInfo() - 218 : " +
                               str);
            return false;
        }
        LZCardSchema tLZCardSchema = tLZCardSet.get(1);
        tLZCardSchema.setActiveFlag("1");
        map.put(tLZCardSchema, "UPDATE");

        return true;
    }

    /**
     * ceratFin
     *
     * @return boolean
     */
    private boolean ceratFin() {

        mPayNo = PubFun1.CreateMaxNo("PAYNO",
                                     PubFun.getNoLimit(mLCContSchema.
                                                       getManageCom()));

        /** 首先校验暂收数据 */
        if (!dealTempFee()) {
            return false;
        }

        /** 生成LJAPayPerson */
        if (!dealJAPayPerson()) {
            return false;
        }

        /** 生成LJSPay */
        if (!dealJAPay()) {
            return false;
        }

        return true;
    }

    /**
     * dealTempFee
     *
     * @return boolean
     */
    private boolean dealTempFee() {
        String chkSql = "select count(1) from ljtempfee where otherno='" +
                        mLCContSchema.getPrtNo() +
                        "' and EnterAccDate is not null and confDate is null";
        if (Integer.parseInt((new ExeSQL()).getOneValue(chkSql)) <= 0) {
            isTempFee = false;
        }
        return true;
    }

    /**
     * dealJAPay
     *
     * @return boolean
     */
    private boolean dealJAPay() {

        mLJAPaySchema.setPayNo(mPayNo);
        mLJAPaySchema.setIncomeNo(mLCContSchema.getPrtNo());
        mLJAPaySchema.setIncomeType("15");
        mLJAPaySchema.setAppntNo(mLCContSchema.getAppntNo());
        mLJAPaySchema.setSumActuPayMoney(mLCContSchema.getPrem());

//
        mLJAPaySchema.setApproveCode(mLCContSchema.getApproveCode());
        mLJAPaySchema.setApproveDate(mLCContSchema.getApproveDate());
//        mLJAPaySchema.setSerialNo();
        mLJAPaySchema.setOperator(mGlobalInput.Operator);
        mLJAPaySchema.setMakeDate(PubFun.getCurrentDate());
        mLJAPaySchema.setMakeTime(PubFun.getCurrentTime());
        mLJAPaySchema.setGetNoticeNo(mLCContSchema.getPrtNo());
        mLJAPaySchema.setModifyDate(PubFun.getCurrentDate());
        mLJAPaySchema.setModifyTime(PubFun.getCurrentTime());
        mLJAPaySchema.setManageCom(mLCContSchema.getManageCom());
        mLJAPaySchema.setAgentCom(mLCContSchema.getAgentCom());
        mLJAPaySchema.setAgentType(mLCContSchema.getAgentType());
        mLJAPaySchema.setBankCode(mLCContSchema.getBankCode());
        mLJAPaySchema.setBankAccNo(mLCContSchema.getBankAccNo());
//        mLJAPaySchema.setRiskCode();
        mLJAPaySchema.setAgentCode(mLCContSchema.getAgentCode());
        mLJAPaySchema.setAgentGroup(mLCContSchema.getAgentGroup());
        mLJAPaySchema.setAccName(mLCContSchema.getAccName());
//        mLJAPaySchema.setStartPayDate();
//        mLJAPaySchema.setPayTypeFlag();
//        mLJAPaySchema.setPrtStateFlag();
//        mLJAPaySchema.setPrintTimes();
        if (this.isTempFee) {
            String payDate = getPayDate();
            if (payDate == null || payDate.equals("")) {
                String str = "查询到账日期失败!";
                buildError("dealJAPay", str);
                System.out.println("在程序BriefCardSignBL.dealJAPay() - 275 : " +
                                   str);
                return false;
            }
            mLJAPaySchema.setEnterAccDate(payDate);
            mLJAPaySchema.setConfDate(payDate);
            mLJAPaySchema.setPayDate(payDate);
            for (int i = 1; i <= this.mLJAPayPersonSet.size(); i++) {
                this.mLJAPayPersonSet.get(i).setEnterAccDate(payDate);
                this.mLJAPayPersonSet.get(i).setConfDate(payDate);
                this.mLJAPayPersonSet.get(i).setPayDate(payDate);
            }
        } else {
//            mLJAPaySchema.setEnterAccDate();
//            mLJAPaySchema.setConfDate();
//            mLJAPaySchema.setPayDate(PubFun.getCurrentDate());
        }
        return true;
    }

    /**
     * getPayDate
     *
     * @return String
     */
    private String getPayDate() {
        String payDateSql =
                "select EnterAccDate from LJTempFee where otherno = '" +
                this.mLCContSchema.getPrtNo() + "'";
        return (new ExeSQL()).getOneValue(payDateSql);
    }

    /**
     * dealJAPayPerson
     *
     * @return boolean
     */
    private boolean dealJAPayPerson() {
        System.out.println("开始处理LJAPayPerson ");
        LCPremDB tLCPremDB = new LCPremDB();
        tLCPremDB.setContNo(this.mLCContSchema.getContNo());
        LCPremSet tLCPremSet = tLCPremDB.query();

        if (tLCPremSet == null || tLCPremSet.size() <= 0) {
            buildError("dealJSPayPerson", "签单后,查询LCPrem失败！");
            return false;
        }

        for (int i = 1; i <= tLCPremSet.size(); i++) {
            LCPremSchema tLCPremSchema = tLCPremSet.get(i);
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(tLCPremSchema.getPolNo());
            if (!tLCPolDB.getInfo()) {
                buildError("dealJSPayPerson", "签单后,生成应收过程中,查询险种失败！");
                return false;
            }

            LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
            tLJAPayPersonSchema.setPolNo(tLCPremSchema.getPolNo());
            tLJAPayPersonSchema.setPayCount(1);
            tLJAPayPersonSchema.setGrpContNo(tLCPremSchema.getGrpContNo());
            tLJAPayPersonSchema.setGrpPolNo(tLCPolDB.getGrpPolNo());
            tLJAPayPersonSchema.setContNo(tLCPremSchema.getContNo());
            tLJAPayPersonSchema.setManageCom(tLCPremSchema.getManageCom());
            tLJAPayPersonSchema.setAgentCom(tLCPolDB.getAgentCom());
            tLJAPayPersonSchema.setAgentType(tLCPolDB.getAgentType());
            tLJAPayPersonSchema.setRiskCode(tLCPolDB.getRiskCode());
            tLJAPayPersonSchema.setAgentCode(tLCPolDB.getAgentCode());
            tLJAPayPersonSchema.setAgentGroup(tLCPolDB.getAgentGroup());
            tLJAPayPersonSchema.setAppntNo(tLCPolDB.getAppntNo());
            tLJAPayPersonSchema.setGetNoticeNo(tLCPremSchema.getGrpContNo());
            tLJAPayPersonSchema.setPayAimClass("2");
            tLJAPayPersonSchema.setDutyCode(tLCPremSchema.getDutyCode());
            tLJAPayPersonSchema.setPayPlanCode(tLCPremSchema.getPayPlanCode());
            tLJAPayPersonSchema.setSumDuePayMoney(tLCPremSchema.getPrem());
            tLJAPayPersonSchema.setSumActuPayMoney(tLCPremSchema.getPrem());
            tLJAPayPersonSchema.setPayIntv(tLCPremSchema.getPayIntv());
            tLJAPayPersonSchema.setPayDate(PubFun.getCurrentDate());
            tLJAPayPersonSchema.setPayType("ZC");
            tLJAPayPersonSchema.setLastPayToDate(tLCPolDB.getCValiDate());
            tLJAPayPersonSchema.setCurPayToDate(tLCPolDB.getPaytoDate());
            tLJAPayPersonSchema.setApproveCode(tLCPolDB.getApproveCode());
            tLJAPayPersonSchema.setApproveDate(tLCPolDB.getApproveDate());
            tLJAPayPersonSchema.setApproveTime(tLCPolDB.getApproveTime());
            tLJAPayPersonSchema.setPayNo(mPayNo);
            tLJAPayPersonSchema.setOperator(this.mGlobalInput.Operator);
            tLJAPayPersonSchema.setMakeDate(PubFun.getCurrentDate());
            tLJAPayPersonSchema.setMakeTime(PubFun.getCurrentTime());
            tLJAPayPersonSchema.setModifyDate(PubFun.getCurrentDate());
            tLJAPayPersonSchema.setModifyTime(PubFun.getCurrentTime());
            this.mLJAPayPersonSet.add(tLJAPayPersonSchema);
        }
        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        System.out.println("into BriefCardSignBL.prepareOutputData()...");

        MMap tMMap = (MMap) mResult.getObjectByObjectName("MMap", 0);
        if (tMMap != null && tMMap.keySet().size() != 0) {
            this.map.add(tMMap);
        }

//        map.put(this.mLJAPayPersonSet, "INSERT");
//        map.put(this.mLJAPaySchema, "INSERT");

        map.put("UPDATE LCCONT SET APPFLAG='1',STATEFLAG='1' , signdate='" + PubFun.getCurrentDate() + "' ,signtime='" +
                PubFun.getCurrentTime() + "' WHERE PROPOSALCONTNO='" +
                this.mLCContSchema.getProposalContNo() + "'", "UPDATE");
        map.put("UPDATE LCPOL SET APPFLAG='1' ,STATEFLAG='1', signdate='" + PubFun.getCurrentDate() + "' ,signtime='" +
                PubFun.getCurrentTime() + "' WHERE PROPOSALCONTNO='" +
                this.mLCContSchema.getProposalContNo() + "'", "UPDATE");

        this.mResult.clear();
        this.mResult.add(map);

        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "BriefCardSignBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args) {
        String tContNo = "13000083950";
        String managecom = "86";
        String comcode = "86";
        String operator = "001";
        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema.setContNo(tContNo);
        TransferData mTransferData = new TransferData();
        mTransferData.setNameAndValue("ContNo", tContNo);

        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.Operator = operator;
        mGlobalInput.ManageCom = managecom;
        mGlobalInput.ComCode = comcode;

        VData v = new VData();
        v.add(mTransferData);
        v.add(tLCContSchema);
        v.add(mGlobalInput);
        BriefCardSignBL briefcardsignbl = new BriefCardSignBL();
//        briefcardsignbl.submitData(v, null);
        briefcardsignbl.testdealMassage();
    }


}
