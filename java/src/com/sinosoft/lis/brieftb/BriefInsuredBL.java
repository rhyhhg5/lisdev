package com.sinosoft.lis.brieftb;

import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.db.LMRiskDutyFactorDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContPlanDutyParamSchema;
import com.sinosoft.lis.schema.LCContPlanRiskSchema;
import com.sinosoft.lis.schema.LCContPlanSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.tb.CachedRiskInfo;
import com.sinosoft.lis.tb.ParseGuideInUI;
import com.sinosoft.lis.vschema.LCContPlanDutyParamSet;
import com.sinosoft.lis.vschema.LCContPlanRiskSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LMRiskDutySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.tb.ContDeleteUI;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.tb.ParseGuideIn;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.tb.CheckRiskDutyFactor;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LCDutySet;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class BriefInsuredBL {

    public BriefInsuredBL() {
    }

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 往前面传输数据的容器 */
    private VData mInputData = new VData();
    /**传输到后台处理的map*/
    private MMap map = new MMap();
    /** 数据操作字符串 */
    private String mOperate = "";
    /** 保险计划要素信息　*/
    private LCContPlanDutyParamSet mLCContPlanDutyParamSet;
    /** 登陆信息 */
    private GlobalInput mGlobalInput;
    /** 团体合同信息 */
    private LCGrpContSchema mLCGrpContSchema;
    /** 团体险种信息 */
    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
    /** 团体险种计划信息 */
    private LCContPlanRiskSet mLCContPlanRiskSet = new LCContPlanRiskSet();
    /** 团体保障计划信息 */
    private LCContPlanSchema mLCContPlanSchema = new LCContPlanSchema();
    /** 险种缓存 */
    private CachedRiskInfo mCRI = CachedRiskInfo.getInstance();
    /**
     * ＵＩ接口
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        mInputData = cInputData;
        mOperate = cOperate;

        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubsubmit = new PubSubmit();
        if (!tPubsubmit.submitData(this.mInputData, "INSERT")) {
            this.mErrors.copyAllErrors(tPubsubmit.mErrors);
            return false;
        }

        /** 完成递交操作后进行被保人的保费计算 */
        if (!calInsured()) {
            return false;
        }

        VData vd = new VData();
        vd.add(map);
        tPubsubmit = new PubSubmit();
        if (!tPubsubmit.submitData(vd, null)) {
            buildError("delInsured", "递交失败！" + tPubsubmit.mErrors.getContent());
            return false;
        }

        map = new MMap();
        map.add(dealSumPeople());
        vd = new VData();
        vd.add(map);
        tPubsubmit = new PubSubmit();
        if (!tPubsubmit.submitData(vd, null)) {
            buildError("delInsured", "递交失败！" + tPubsubmit.mErrors.getContent());
            return false;
        }

        return true;
    }

    /**
     * calInsured
     *
     * @return boolean
     */
    private boolean calInsured() {
        VData tVData = new VData();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("GrpContNo",
                                      this.mLCGrpContSchema.getGrpContNo());
        tTransferData.setNameAndValue("Flag", "N");
        tVData.add(tTransferData);
        tVData.add(this.mGlobalInput);
        ParseGuideIn tPGI = new ParseGuideIn();
        boolean ress = true;
        try {
            ress = tPGI.submitData(tVData, "INSERT||DATABASE");
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("calInsured", "发生异常错误 原因是: " + ex + "！");
            return false;
        }

        if (!ress) {
            this.mErrors.copyAllErrors(tPGI.mErrors);
            return false;
        }
        map = new MMap();
        map.add(tPGI.getSubmitData());
        map.put("update lccont set UWDate='" + PubFun.getCurrentDate() +
                "',UWTime='" + PubFun.getCurrentTime() + "',ApproveTime='" +
                PubFun.getCurrentTime() + "',ApproveDate='" +
                PubFun.getCurrentDate() + "',UWFlag='9',UWOperator='" +
                mGlobalInput.Operator + "',ApproveFlag='9' where grpcontno='" +
                this.mLCGrpContSchema.getGrpContNo() + "'",
                "UPDATE");
        map.put("update lcpol set UWDate='" + PubFun.getCurrentDate() +
                "',UWTime='" + PubFun.getCurrentTime() + "',ApproveTime='" +
                PubFun.getCurrentTime() + "',ApproveDate='" +
                PubFun.getCurrentDate() + "',UWFlag='9',UWCode='" +
                mGlobalInput.Operator + "',ApproveFlag='9' where grpcontno='" +
                this.mLCGrpContSchema.getGrpContNo() + "'",
                "UPDATE");
        map.put("update lcgrpcont set UWDate='" + PubFun.getCurrentDate() +
                "',UWTime='" + PubFun.getCurrentTime() + "',ApproveTime='" +
                PubFun.getCurrentTime() + "',ApproveDate='" +
                PubFun.getCurrentDate() + "',UWFlag='9',UWOperator='" +
                mGlobalInput.Operator + "',ApproveFlag='9' where grpcontno='" +
                this.mLCGrpContSchema.getGrpContNo() + "'",
                "UPDATE");
        map.put("update lcgrppol set UWDate='" + PubFun.getCurrentDate() +
                "',UWTime='" + PubFun.getCurrentTime() + "',ApproveTime='" +
                PubFun.getCurrentTime() + "',ApproveDate='" +
                PubFun.getCurrentDate() + "',UWFlag='9',UWOperator='" +
                mGlobalInput.Operator + "',ApproveFlag='9' where grpcontno='" +
                this.mLCGrpContSchema.getGrpContNo() + "'",
                "UPDATE");
        LCPolSet tLCPolSet = tPGI.getEdorNiResult();
        LCContSet tLCContSet=tPGI.getEdorNiContResult();
        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema=tLCContSet.get(1);
        for (int i = 1 ;i <= tLCPolSet.size(); i++) {
            LCPolSchema tLCPolSchema = new LCPolSchema();
            tLCPolSchema=tLCPolSet.get(i);
            double FloatRate;
            if (StrTool.cTrim(tLCPolSchema.getContNo()).equals(tLCContSchema.
                    getContNo())) {
                FloatRate = tLCPolSchema.getPrem() / tLCPolSchema.getStandPrem();
                System.out.println("标保"+tLCPolSchema.getStandPrem());
                System.out.println("非标保"+tLCPolSchema.getPrem());
                System.out.println("浮动费率" + FloatRate);

                for (int j = 1; j <= mLCContPlanDutyParamSet.size(); j++) {
                    LCContPlanDutyParamSchema newLCContPlanDutyParamSchema = new
                           LCContPlanDutyParamSchema();
                   LCContPlanDutyParamSchema oldLCContPlanDutyParamSchema = new
                           LCContPlanDutyParamSchema();

                    oldLCContPlanDutyParamSchema = mLCContPlanDutyParamSet.get(
                            j);
                    newLCContPlanDutyParamSchema = mLCContPlanDutyParamSet.get(
                            j);
                    if (newLCContPlanDutyParamSchema.getRiskCode().equals(
                            tLCPolSchema.getRiskCode()
                        ) &&
                        newLCContPlanDutyParamSchema.getCalFactor().equals("FloatRate")) {
                        newLCContPlanDutyParamSchema.setCalFactorValue(String.
                                valueOf(FloatRate));
                        System.out.println("zhuzhu"+newLCContPlanDutyParamSchema.getCalFactorValue());
                        mLCContPlanDutyParamSet.removeRange(
                                j,j);
                        mLCContPlanDutyParamSet.add(
                                newLCContPlanDutyParamSchema);
                    }
                }
            }
            }
        //用于校验险种责任要素
        //@author Yangming
        VData tempVData = new VData();
        tempVData.add(mLCContPlanDutyParamSet);
        CheckRiskDutyFactor tCheckRiskDutyFactor = new CheckRiskDutyFactor();
        if (!tCheckRiskDutyFactor.submitData(tempVData, "")) {
            System.out.println("校验报错！");
        }
        VData Result = tCheckRiskDutyFactor.getResult();
        if (Result.size() > 0) {
            String Error = (String) Result.get(0);
            if (Error != null) {
                if (!Error.equals("")) {
                    buildError("insertData", Error);
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        map.put("delete from lcgrppol where grpcontno='" +
                this.mLCGrpContSchema.getGrpContNo() + "'", "DELETE");
        map.put("delete from lccontplan where grpcontno='" +
                this.mLCGrpContSchema.getGrpContNo() + "'", "DELETE");
        map.put("delete from lccontplanrisk where grpcontno='" +
                this.mLCGrpContSchema.getGrpContNo() + "'", "DELETE");
        map.put("delete from lccontplandutyparam where grpcontno='" +
                this.mLCGrpContSchema.getGrpContNo() + "'", "DELETE");
        map.put(this.mLCContPlanDutyParamSet, "INSERT");
        map.put(this.mLCContPlanRiskSet, "INSERT");
        map.put(this.mLCContPlanSchema, "INSERT");
        map.put(this.mLCGrpPolSet, "INSERT");
        this.mInputData.add(map);
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        /** 开始删除被保险人 */
        System.out.println("开始删除被保险人");
        if (!delInsured()) {
            return false;
        }

        System.out.println("保存险种信息和计划信息的保存...");
        if (!dealContPlan()) {
            return false;
        }
        return true;
    }

    /**
     * delInsured
     *
     * @return boolean
     */
    private boolean delInsured() {
        LCContDB tLCContDB = new LCContDB();
        if (mLCGrpContSchema.getGrpContNo() == null ||
            mLCGrpContSchema.getGrpContNo().equals("")) {
            buildError("delInsured", "没有传入团体合同信息！");
            return false;
        }
        tLCContDB.setGrpContNo(this.mLCGrpContSchema.getGrpContNo());
        LCContSet tLCContSet = tLCContDB.query();
        MMap map = new MMap();
        if (tLCContSet != null) {
            for (int i = 1; i <= tLCContSet.size(); i++) {
                VData cInputData = new VData();
                TransferData mTransferData = new TransferData();
                cInputData.add(mGlobalInput);
                cInputData.add(tLCContSet.get(i));
                cInputData.add(mTransferData);
                ContDeleteUI tContDeleteUI = new ContDeleteUI();
                if (tContDeleteUI.submitData(cInputData, "GETMAP") == false) {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tContDeleteUI.mErrors);
                    return false;
                }
                VData tResult = tContDeleteUI.getResult();
                MMap tMap = (MMap) tResult.getObjectByObjectName("MMap", 0);
                if (tMap == null || tMap.size() <= 0) {
                    buildError("updateData", "生成删除数据失败！");
                    return false;
                }
                map.add(tMap);
            }
            map.put("UPDATE LCINSUREDLIST SET STATE='0' WHERE GRPCONTNO='" +
                    this.mLCGrpContSchema.getGrpContNo() + "'", "UPDATE");

            VData vd = new VData();
            vd.add(map);
            PubSubmit ps = new PubSubmit();
            if (!ps.submitData(vd, null)) {
                buildError("delInsured", "递交失败！" + ps.mErrors.getContent());
                return false;
            }
        }
        return true;
    }


    private MMap dealSumPeople() {

        String tGrpContNo = this.mLCGrpContSchema.getGrpContNo();
        MMap tMap = new MMap();
        StringBuffer sql_LCGrpPol = new StringBuffer();
        sql_LCGrpPol.append("select a,sum(b) from ( ");
        sql_LCGrpPol.append("SELECT GRPPOLNO a,INSUREDPEOPLES b ");
        sql_LCGrpPol.append(" FROM LCPOL WHERE ");
        sql_LCGrpPol.append("GRPCONTNO='");
        sql_LCGrpPol.append(tGrpContNo);
        sql_LCGrpPol.append("' AND POLTYPEFLAG IN ('0','1')");
        sql_LCGrpPol.append(" union all ");
        sql_LCGrpPol.append(" SELECT GRPPOLNO a,0 b ");
        sql_LCGrpPol.append(" FROM LCPOL WHERE  ");
        sql_LCGrpPol.append("GRPCONTNO='");
        sql_LCGrpPol.append(tGrpContNo);
        sql_LCGrpPol.append("' AND POLTYPEFLAG IN ('2')");
        sql_LCGrpPol.append(" ) as x ");
        sql_LCGrpPol.append(" GROUP BY A ");

        SSRS ssrs = (new ExeSQL()).execSQL(sql_LCGrpPol.toString());

        for (int i = 1; i <= ssrs.getMaxRow(); i++) {
            String tGrpPolNo = ssrs.GetText(i, 1);
            String tSumPeople = ssrs.GetText(i, 2);
            StringBuffer update_LCGrpPol = new StringBuffer();
            update_LCGrpPol.append("UPDATE LCGRPPOL SET PEOPLES2=");
            update_LCGrpPol.append(tSumPeople);
            update_LCGrpPol.append(
                    ",PREM=(SELECT SUM(PREM) FROM LCPOL WHERE GRPPOLNO=LCGRPPOL.GRPPOLNO)");
            update_LCGrpPol.append(" WHERE GRPPOLNO ='");
            update_LCGrpPol.append(tGrpPolNo);
            update_LCGrpPol.append("'");
            tMap.put(update_LCGrpPol.toString(), "UPDATE");
        }

        StringBuffer sql_LCGrpCont = new StringBuffer();
        sql_LCGrpCont.append("SELECT SUM(PEOPLES) ");
        sql_LCGrpCont.append(" FROM LCCONT WHERE ");
        sql_LCGrpCont.append(" GRPCONTNO='");
        sql_LCGrpCont.append(tGrpContNo);
        sql_LCGrpCont.append("' AND POLTYPE IN ('0','1')");
        String sum_People = (new ExeSQL()).getOneValue(sql_LCGrpCont.
                toString());

        StringBuffer update_LCGrpCont = new StringBuffer();
        update_LCGrpCont.append("UPDATE LCGRPCONT SET PEOPLES2=");
        update_LCGrpCont.append(sum_People);
        update_LCGrpCont.append(
                ",PREM=(SELECT SUM(PREM) FROM LCCONT WHERE GRPCONTNO=LCGRPCONT.GRPCONTNO)");
        update_LCGrpCont.append(" WHERE GRPCONTNO='");
        update_LCGrpCont.append(tGrpContNo);
        update_LCGrpCont.append("'");
        tMap.put(update_LCGrpCont.toString(), "UPDATE");
        return tMap;
    }

    /**
     * dealContPlan
     *
     * @return boolean
     */
    private boolean dealContPlan() {
        //首先确定几个险种并将所有的险种信息保存
        String RiskCode = mLCContPlanDutyParamSet.get(1).getRiskCode();
        String tPolNo = "";
        for (int i = 1; i <= this.mLCContPlanDutyParamSet.size(); i++) {
            //首先为第一个险种添加信息
            if (i == 1) {
                tPolNo =
                        creatPolInfo(mLCContPlanDutyParamSet.get(i).getRiskCode());
                mLCContPlanDutyParamSet.get(i).setGrpPolNo(tPolNo);
            } else {
                if (!RiskCode.equals(mLCContPlanDutyParamSet.get(i).getRiskCode())) {
                    tPolNo = creatPolInfo(mLCContPlanDutyParamSet.get(i).
                                          getRiskCode());
                    mLCContPlanDutyParamSet.get(i).setGrpPolNo(tPolNo);
                    RiskCode = mLCContPlanDutyParamSet.get(i).getRiskCode();
                }
            }
            LMRiskDutySet tLMRiskDutySet = mCRI.findRiskDutyByRiskCode(
                    mLCContPlanDutyParamSet.get(i).getRiskCode());
            if (tLMRiskDutySet == null || tLMRiskDutySet.size() <= 0) {
                buildError("dealData", "查询险种: " + RiskCode + "的责任信息失败！");
                return false;
            }
            if (tLMRiskDutySet.size() > 1) {
                buildError("dealData", "境外救援改造不支持多责任录入！");
                return false;
            }
            mLCContPlanDutyParamSet.get(i).setDutyCode(tLMRiskDutySet.get(1).
                    getDutyCode());
            LMRiskDutyFactorDB tLMRiskDutyFactorDB = new LMRiskDutyFactorDB();
            tLMRiskDutyFactorDB.setRiskCode(RiskCode);
            tLMRiskDutyFactorDB.setDutyCode(tLMRiskDutySet.get(1).getDutyCode());
            tLMRiskDutyFactorDB.setCalFactor(mLCContPlanDutyParamSet.get(i).
                                             getCalFactor());
            tLMRiskDutyFactorDB.setPayPlanCode("000000");
            tLMRiskDutyFactorDB.setGetDutyCode("000000");
            tLMRiskDutyFactorDB.setInsuAccNo("000000");
            if (!tLMRiskDutyFactorDB.getInfo()) {
                buildError("dealData", "查询险种要素信息失败！");
                return false;
            }
            mLCContPlanDutyParamSet.get(i).setCalFactorType(tLMRiskDutyFactorDB.
                    getCalFactorType());
            mLCContPlanDutyParamSet.get(i).setPlanType("0");
            mLCContPlanDutyParamSet.get(i).setPayPlanCode("000000");
            mLCContPlanDutyParamSet.get(i).setGetDutyCode("000000");
            mLCContPlanDutyParamSet.get(i).setInsuAccNo("000000");
            mLCContPlanDutyParamSet.get(i).setGrpPolNo(tPolNo);
        }
        PubFun.fillDefaultField(mLCContPlanDutyParamSet);

        /**
         * 以上各步骤完成计划信息,要素信息,险种信息操作
         * 开始准备处理保单计划
         */
        mLCContPlanSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        mLCContPlanSchema.setProposalGrpContNo(mLCGrpContSchema.getGrpContNo());
        mLCContPlanSchema.setContPlanCode("00");
        mLCContPlanSchema.setContPlanName("默认计划");
        mLCContPlanSchema.setPlanType("0");
        mLCContPlanSchema.setOperator(this.mGlobalInput.Operator);
        PubFun.fillDefaultField(mLCContPlanSchema);

        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        /** 开始校验相关数据 */
        System.out.println("开始校验相关数据...");

        if (this.mLCContPlanDutyParamSet == null ||
            mLCContPlanDutyParamSet.size() <= 0) {
            buildError("checkData", "险种信息不完整！");
            return false;
        }

        if (this.mGlobalInput == null) {
            buildError("checkData", "传入登陆信息为null！请重新登陆");
            return false;
        }

        if (mLCGrpContSchema == null) {
            buildError("checkData", "团体合同信息为null！");
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        mLCContPlanDutyParamSet = (LCContPlanDutyParamSet)
                                  mInputData.getObjectByObjectName(
                                          "LCContPlanDutyParamSet", 0);
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mLCGrpContSchema = (LCGrpContSchema) mInputData.getObjectByObjectName(
                "LCGrpContSchema", 0);
        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "BriefInsuredBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.println("程序报错：" + cError.errorMessage);
    }

    /**
     * 创建险种信息
     * @return boolean
     * @param cRiskCode String
     */
    private String creatPolInfo(String cRiskCode) {
        LMRiskDB tLMRiskDB = new LMRiskDB();
        tLMRiskDB.setRiskCode(cRiskCode);
        if (!tLMRiskDB.getInfo()) {
            System.out.println(
                    "程序第137行出错，请检查BriefContPlanRiskBL.java中的creatPolInfo方法！");
            CError tError = new CError();
            tError.moduleName = "BriefContPlanRiskBL.java";
            tError.functionName = "creatPolInfo";
            tError.errorMessage = "查询险种失败！";
            this.mErrors.addOneError(tError);
            return null;
        }
        //首先险种表lcpol
        LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
        tLCGrpPolSchema.setRiskCode(cRiskCode);
        String tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
        String tNo = PubFun1.CreateMaxNo("GrpProposalNo", tLimit);
        tLCGrpPolSchema.setGrpPolNo(tNo); //如果是新增
        tLCGrpPolSchema.setGrpProposalNo(tNo);
        tLCGrpPolSchema.setPrtNo(mLCGrpContSchema.getPrtNo());
        tLCGrpPolSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        tLCGrpPolSchema.setSaleChnl(mLCGrpContSchema.getSaleChnl());
        tLCGrpPolSchema.setManageCom(mLCGrpContSchema.getManageCom());
        tLCGrpPolSchema.setAgentCom(mLCGrpContSchema.getAgentCom());
        tLCGrpPolSchema.setAgentType(mLCGrpContSchema.getAgentType());
        tLCGrpPolSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        tLCGrpPolSchema.setAgentGroup(mLCGrpContSchema.getAgentGroup());
        tLCGrpPolSchema.setCustomerNo(mLCGrpContSchema.getAppntNo());
        tLCGrpPolSchema.setAddressNo(mLCGrpContSchema.getAddressNo());
        tLCGrpPolSchema.setGrpName(mLCGrpContSchema.getGrpName());
        tLCGrpPolSchema.setCValiDate(mLCGrpContSchema.getCValiDate());
        tLCGrpPolSchema.setPayMode(mLCGrpContSchema.getPayMode());
        tLCGrpPolSchema.setAppFlag("0");
        tLCGrpPolSchema.setUWFlag("9");
        tLCGrpPolSchema.setUWOperator(mGlobalInput.Operator);
        tLCGrpPolSchema.setUWTime(PubFun.getCurrentTime());
        tLCGrpPolSchema.setUWDate(PubFun.getCurrentDate());
        tLCGrpPolSchema.setApproveDate(PubFun.getCurrentDate());
        tLCGrpPolSchema.setApproveTime(PubFun.getCurrentTime());
        tLCGrpPolSchema.setApproveFlag("9");
        tLCGrpPolSchema.setModifyDate(PubFun.getCurrentDate());
        tLCGrpPolSchema.setModifyTime(PubFun.getCurrentTime());
        tLCGrpPolSchema.setOperator(mGlobalInput.Operator);
        tLCGrpPolSchema.setMakeDate(PubFun.getCurrentDate());
        tLCGrpPolSchema.setMakeTime(PubFun.getCurrentTime());
        tLCGrpPolSchema.setPayMode(this.mLCGrpContSchema.getPayMode());
        for (int i = 1; i <= this.mLCContPlanDutyParamSet.size(); i++) {
            if (tLCGrpPolSchema.getRiskCode().equals(mLCContPlanDutyParamSet.
                    get(i).getRiskCode())) {
                if (mLCContPlanDutyParamSet.get(i).getCalFactor().equals("Mult")) {
                    tLCGrpPolSchema.setMult(mLCContPlanDutyParamSet.get(i).
                                            getCalFactorValue());
                }
            }
        }

        mLCGrpPolSet.add(tLCGrpPolSchema);
        /**
         * 处理完成险种信息,
         * 开始处理险种计划
         */
        LCContPlanRiskSchema tLCContPlanRiskSchema = new LCContPlanRiskSchema();
        tLCContPlanRiskSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        tLCContPlanRiskSchema.setProposalGrpContNo(mLCGrpContSchema.
                getGrpContNo());
        tLCContPlanRiskSchema.setMainRiskCode(cRiskCode);
        tLCContPlanRiskSchema.setMainRiskVersion(tLMRiskDB.getRiskVer());
        tLCContPlanRiskSchema.setRiskCode(cRiskCode);
        tLCContPlanRiskSchema.setRiskVersion(tLMRiskDB.getRiskVer());
        tLCContPlanRiskSchema.setContPlanCode("00");
        tLCContPlanRiskSchema.setContPlanName("默认计划");
        tLCContPlanRiskSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(tLCContPlanRiskSchema);
        tLCContPlanRiskSchema.setPlanType("0");
        mLCContPlanRiskSet.add(tLCContPlanRiskSchema);
        return tNo;
    }

    /**
     * 调试函数
     * @param args String[]
     */
    public static void main(String[] args) {

        String tGrpContNo = "0000163201";
        String tRiskCode = "1501";
        String tMultAndAmnt = "1";
        String tFloatRate = "";
        String Prem="400";
        String tDegreeType = "0";
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86";
        tG.ComCode = "86";
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(tGrpContNo);
        if (!tLCGrpContDB.getInfo()) {

        }

        LCContPlanDutyParamSchema tLCContPlanDutyParamSchema = null;
        LCContPlanDutyParamSet tLCContPlanDutyParamSet = new
                LCContPlanDutyParamSet();

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode);
        tLCContPlanDutyParamSchema.setContPlanCode("00"); //默认计划
        tLCContPlanDutyParamSchema.setContPlanName("默认计划");

        tLCContPlanDutyParamSchema.setCalFactor("Mult");
        tLCContPlanDutyParamSchema.setCalFactorValue(tMultAndAmnt);

//        tLCContPlanDutyParamSchema.setCalFactor("Mult");
//        tLCContPlanDutyParamSchema.setCalFactorValue(tMultAndAmnt[index]);


        tLCContPlanDutyParamSchema.setPayPlanCode("000000");
        tLCContPlanDutyParamSchema.setGetDutyCode("000000");
        tLCContPlanDutyParamSchema.setInsuAccNo("000000");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        //保存计算方向,全部默认为表定费率折扣
        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode);
        tLCContPlanDutyParamSchema.setContPlanCode("00"); //默认计划
        tLCContPlanDutyParamSchema.setContPlanName("默认计划");
        tLCContPlanDutyParamSchema.setCalFactor("CalRule");
        tLCContPlanDutyParamSchema.setCalFactorValue("3");
        tLCContPlanDutyParamSchema.setPayPlanCode("000000");
        tLCContPlanDutyParamSchema.setGetDutyCode("000000");
        tLCContPlanDutyParamSchema.setInsuAccNo("000000");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        //保存折扣问题
        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode);
        tLCContPlanDutyParamSchema.setContPlanCode("00"); //默认计划
        tLCContPlanDutyParamSchema.setContPlanName("默认计划");
        tLCContPlanDutyParamSchema.setCalFactor("FloatRate");
        tLCContPlanDutyParamSchema.setCalFactorValue(tFloatRate);
        tLCContPlanDutyParamSchema.setPayPlanCode("000000");
        tLCContPlanDutyParamSchema.setGetDutyCode("000000");
        tLCContPlanDutyParamSchema.setInsuAccNo("000000");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        //单次多次
        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode);
        tLCContPlanDutyParamSchema.setContPlanCode("00"); //默认计划
        tLCContPlanDutyParamSchema.setContPlanName("默认计划");
        tLCContPlanDutyParamSchema.setCalFactor("StandbyFlag1");
        tLCContPlanDutyParamSchema.setCalFactorValue(tDegreeType);
        tLCContPlanDutyParamSchema.setPayPlanCode("000000");
        tLCContPlanDutyParamSchema.setGetDutyCode("000000");
        tLCContPlanDutyParamSchema.setInsuAccNo("000000");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode);
        tLCContPlanDutyParamSchema.setContPlanCode("00"); //默认计划
        tLCContPlanDutyParamSchema.setContPlanName("默认计划");
        tLCContPlanDutyParamSchema.setCalFactor("Prem");
        tLCContPlanDutyParamSchema.setCalFactorValue(Prem);
        tLCContPlanDutyParamSchema.setPayPlanCode("000000");
        tLCContPlanDutyParamSchema.setGetDutyCode("000000");
        tLCContPlanDutyParamSchema.setInsuAccNo("000000");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

         tRiskCode = "5501";
         tMultAndAmnt = "300000";
         tFloatRate = "";
         Prem="400";
         tDegreeType = "0";
         tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86";
        tG.ComCode = "86";

         tLCContPlanDutyParamSchema = null;
         tLCContPlanDutyParamSet = new
                LCContPlanDutyParamSet();

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode);
        tLCContPlanDutyParamSchema.setContPlanCode("00"); //默认计划
        tLCContPlanDutyParamSchema.setContPlanName("默认计划");

        tLCContPlanDutyParamSchema.setCalFactor("Amnt");
        tLCContPlanDutyParamSchema.setCalFactorValue(tMultAndAmnt);

//        tLCContPlanDutyParamSchema.setCalFactor("Mult");
//        tLCContPlanDutyParamSchema.setCalFactorValue(tMultAndAmnt[index]);


        tLCContPlanDutyParamSchema.setPayPlanCode("000000");
        tLCContPlanDutyParamSchema.setGetDutyCode("000000");
        tLCContPlanDutyParamSchema.setInsuAccNo("000000");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        //保存计算方向,全部默认为表定费率折扣
        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode);
        tLCContPlanDutyParamSchema.setContPlanCode("00"); //默认计划
        tLCContPlanDutyParamSchema.setContPlanName("默认计划");
        tLCContPlanDutyParamSchema.setCalFactor("CalRule");
        tLCContPlanDutyParamSchema.setCalFactorValue("3");
        tLCContPlanDutyParamSchema.setPayPlanCode("000000");
        tLCContPlanDutyParamSchema.setGetDutyCode("000000");
        tLCContPlanDutyParamSchema.setInsuAccNo("000000");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        //保存折扣问题
        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode);
        tLCContPlanDutyParamSchema.setContPlanCode("00"); //默认计划
        tLCContPlanDutyParamSchema.setContPlanName("默认计划");
        tLCContPlanDutyParamSchema.setCalFactor("FloatRate");
        tLCContPlanDutyParamSchema.setCalFactorValue(tFloatRate);
        tLCContPlanDutyParamSchema.setPayPlanCode("000000");
        tLCContPlanDutyParamSchema.setGetDutyCode("000000");
        tLCContPlanDutyParamSchema.setInsuAccNo("000000");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        //单次多次
        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode);
        tLCContPlanDutyParamSchema.setContPlanCode("00"); //默认计划
        tLCContPlanDutyParamSchema.setContPlanName("默认计划");
        tLCContPlanDutyParamSchema.setCalFactor("StandbyFlag1");
        tLCContPlanDutyParamSchema.setCalFactorValue(tDegreeType);
        tLCContPlanDutyParamSchema.setPayPlanCode("000000");
        tLCContPlanDutyParamSchema.setGetDutyCode("000000");
        tLCContPlanDutyParamSchema.setInsuAccNo("000000");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        tLCContPlanDutyParamSchema = new LCContPlanDutyParamSchema();
        tLCContPlanDutyParamSchema.setGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setProposalGrpContNo(tGrpContNo);
        tLCContPlanDutyParamSchema.setMainRiskCode(tRiskCode);
        tLCContPlanDutyParamSchema.setRiskCode(tRiskCode);
        tLCContPlanDutyParamSchema.setContPlanCode("00"); //默认计划
        tLCContPlanDutyParamSchema.setContPlanName("默认计划");
        tLCContPlanDutyParamSchema.setCalFactor("Prem");
        tLCContPlanDutyParamSchema.setCalFactorValue(Prem);
        tLCContPlanDutyParamSchema.setPayPlanCode("000000");
        tLCContPlanDutyParamSchema.setGetDutyCode("000000");
        tLCContPlanDutyParamSchema.setInsuAccNo("000000");
        tLCContPlanDutyParamSet.add(tLCContPlanDutyParamSchema);

        BriefInsuredBL tBriefInsuredBL = new BriefInsuredBL();
        VData vd = new VData();
        vd.add(tLCContPlanDutyParamSet);
        vd.add(tLCGrpContDB.getSchema());
        vd.add(tG);

        if (!tBriefInsuredBL.submitData(vd, null)) {
            System.out.println("程序报错: " + tBriefInsuredBL.mErrors.getErrContent());
        }
    }
}
