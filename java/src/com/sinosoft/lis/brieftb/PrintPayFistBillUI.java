package com.sinosoft.lis.brieftb;

import com.sinosoft.lis.f1print.NewPrintBillRightBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * 打印先收费下载清单,没缴费的投保书才能下载
 * 
 * @author NicolE
 * @version 1.0
 */
public class PrintPayFistBillUI {

	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private VData mInputData = new VData();

	private String mOperate;

	public PrintPayFistBillUI() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println("Start PrintPayFistBillUI Submit ...");
		mOperate = cOperate;
		mInputData = (VData) cInputData.clone();
		try {
			if (!mOperate.equals("PRINT")) {
				buildError("submitData", "不支持的操作字符串");
				return false;
			}
			PrintPayFistBillBL tPrintPayFistBillBL = new PrintPayFistBillBL();
			if (!tPrintPayFistBillBL.submitData(mInputData, mOperate)) {
				if (tPrintPayFistBillBL.mErrors.needDealError()) {
					mErrors.copyAllErrors(tPrintPayFistBillBL.mErrors);
					return false;
				} else {
					buildError("submitData", "打印下载清单发生错误，但是没有提供详细的出错信息");
					return false;
				}
			} else {
				mResult = tPrintPayFistBillBL.getResult();
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			CError cError = new CError();
			cError.moduleName = "PrintPayFistBillUI";
			cError.functionName = "submitData";
			cError.errorMessage = e.toString();
			mErrors.addOneError(cError);
			return false;
		}
	}

	/**
	 * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
	 */
	public VData getResult() {
		return this.mResult;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();

		cError.moduleName = "PrintPayFistBillUI";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		mErrors.addOneError(cError);
	}
}
