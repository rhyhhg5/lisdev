package com.sinosoft.lis.brieftb;

import com.sinosoft.lis.certify.VerifyCertifyNo;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.encrypt.LisIDEA;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: 卡的激活</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class BriefCardActivity
    implements CardActivityInterface {

  public BriefCardActivity() {
  }

  public static void main(String[] args) {
    BriefCardActivity briefcardactivity = new BriefCardActivity();
    try {
      briefcardactivity.checkCardNo("17000291542", "822712");
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * 主要做三件事情 1、校验卡号是否符合校验码 2、判断卡号密码是否正确 3、判断卡单状态是否正确
   *
   * @param tCardNo String
   * @param tPassword String
   * @return String
   * @throws Exception
   */
  public String checkCardNo(String tCardNo, String tPassword) throws
      Exception {
    /** 判断卡号校验位 */
    if (!checkCRC(tCardNo)) {
      return CRC_ERROR;
    }
    /** 判断卡号密码是否正确 */
    if (!checkPassword(tCardNo, tPassword)) {
      return PASSWORD_ERROR;
    }
    /** 是否已下发此单证 */
    if (!isRelease(tCardNo)) {
      return NOT_RELEASE_ERROR;
    }
    /** 判断是否已激活 */
    if (!isActivity(tCardNo)) {
      return ACTIVITY_ERROR;
    }
    /** 判断是否已经挂失 */
    if (isLost(tCardNo)) {
      return LOST_ERROR;
    }
    return SUCC;
  }

  /**
   * isLost
   *
   * @return boolean
   * @param tCardNo String
   */
  private boolean isLost(String tCardNo) {
    String chkSql = "select count(1) from lzcard a,lzcardnumber b where a.subcode=b.cardtype and a.startno=b.cardserno and cardno='" +
        tCardNo + "' and a.state='4'";
    if (Integer.parseInt( (new ExeSQL()).getOneValue(chkSql)) > 0) {
      return true;
    }
    return false;
  }

  /**
   * isActivity
   *
   * @return boolean
   * @param tCardNo String
   */
  private boolean isActivity(String tCardNo) {
    String chkSql = "select count(1) from lzcard a,lzcardnumber b where a.subcode=b.cardtype and a.startno=b.cardserno and cardno='" +
        tCardNo + "' and a.ActiveFlag='1'";
    if (Integer.parseInt( (new ExeSQL()).getOneValue(chkSql)) == 1) {
      return false;
    }
    return true;
  }

  /**
   * isRelease
   *
   * @return boolean
   * @param tCardNo String
   */
  private boolean isRelease(String tCardNo) {
    String chkSql = "select count(1) from lzcard a,lzcardnumber b where a.subcode=b.cardtype and a.startno=b.cardserno and cardno='" +
        tCardNo + "' and a.state in('10','11','12','2')";
    if (Integer.parseInt( (new ExeSQL()).getOneValue(chkSql)) <= 0) {
      return false;
    }
    return true;
  }

  /**
   * checkPassword
   *
   * @return boolean
   * @param tCardNo String
   * @param tPassword String
   */
  private boolean checkPassword(String tCardNo, String tPassword) {
    String chkSql = "select count(1) from lzcardnumber where cardno='" +
        tCardNo + "' and cardpassword='" +
        (new LisIDEA()).encryptString(tPassword) + "'";
    if (Integer.parseInt( (new ExeSQL()).getOneValue(chkSql)) <= 0) {
      return false;
    }
    return true;
  }

  /**
   * checkCRC
   *
   * @param tCardNo String
   * @return boolean
   * @throws Exception
   */
  private boolean checkCRC(String tCardNo) throws Exception {
    return VerifyCertifyNo.verify(tCardNo, VerifyCertifyNo.PERSONAL_CARD);
  }

}
