package com.sinosoft.lis.brieftb;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>
 * Title: 打印先收费下载清单
 * </p>
 * <p>
 * Description: 根据条件查询出要打印保单信息
 * </p>
 * <p>
 * Copyright: Copyright (c) 2003-04-02
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author XUN
 * @version 1.0
 * 
 */
public class PrintPayFistBillBL {

	public CErrors mErrors = new CErrors();

	private GlobalInput mG = new GlobalInput();

	private VData mInputData = null;

	private VData mResult = new VData();

	private String mOperate = null;

	private String strStartDate = null;

	private String strEndDate = null;

	private String strBankCode = null;

	private String t_ComCode = null;

	public PrintPayFistBillBL() {
	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return: true or false
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		mInputData = (VData) cInputData.clone();
		mOperate = cOperate;

		if (!getInputData()) {
			return false;
		}
		if (!checkData()) {
			return false;
		}
		if (!queryData())
			return false;

		return true;
	}

	/**
	 * 获取页面传递信息
	 * 
	 * @param cInputData
	 * @return
	 */
	private boolean getInputData() {

		mG.setSchema((GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0));
		strStartDate = (String) mInputData.get(0);
		strEndDate = (String) mInputData.get(1);
		strBankCode = (String) mInputData.get(2);
		t_ComCode = mG.ManageCom;

		return true;
	}

	/**
	 * 校验数据
	 * 
	 * @return
	 */
	private boolean checkData() {
		if (strStartDate == null) {
			mErrors.addOneError("起始日期不能为空！");
			return false;
		}
		if (strEndDate == null) {
			mErrors.addOneError("结束日期不能为空！");
			return false;
		}
		if (strBankCode == null) {
			mErrors.addOneError("银行编码不能为空！");
			return false;
		}
		if (t_ComCode == null) {
			mErrors.addOneError("获取登录用户信息失败！");
			return false;
		}
		return true;
	}

	/**
	 * 查询未缴费的数据
	 * 
	 * @return
	 */
	private boolean queryData() {

		try {
			String sql = "select ContNo,TempFeeNo,BankAccNo,AccName,Prem from lccont ";
			sql += " where grpcontno = '00000000000000000000' ";// 简易个单
			sql += " and payLocation= '0' and TempFeeNo is not null and appFlag = '0' ";
			sql += " and bankcode = '" + strBankCode + "' ";
			sql += " and makedate >='" + strStartDate + "' ";
			sql += " and makedate <='" + strEndDate + "' ";
			System.out.println("------" + sql);
			TextTag texttag = new TextTag(); // 新建一个TextTag的实例
			XmlExport xmlexport = new XmlExport(); // 新建一个XmlExport的实例
			xmlexport.createDocument("PrintPayFistBill.vts", "printer");
			ListTable alistTable = new ListTable();
			alistTable.setName("INFO");

			// 总金额
			double SumDuePayMoney = 0.0;

			ExeSQL tExeSQL = new ExeSQL();
			SSRS main_ssrs = tExeSQL.execSQL(sql);
			int RowNum = main_ssrs.getMaxRow();
			if (RowNum > 0) {
				System.out.println("查询的结果是" + RowNum);
				for (int i = 1; i <= RowNum; i++) {
					// 对其明细信息进行赋值
					String[] cols = new String[5];
					cols[0] = main_ssrs.GetText(i, 1); // 合同号
					cols[1] = main_ssrs.GetText(i, 2).trim(); // 缴费凭证号
					cols[2] = main_ssrs.GetText(i, 3).trim(); // 银行帐号
					cols[3] = main_ssrs.GetText(i, 4).trim(); // 帐户名
					cols[4] = main_ssrs.GetText(i, 5).trim(); // 金额
					// 计算总金额
					SumDuePayMoney += Double.parseDouble(main_ssrs
							.GetText(i, 5));

					alistTable.add(cols);
				}

				String[] b_col = new String[5];
				xmlexport.addDisplayControl("displayinfo");
				xmlexport.addListTable(alistTable, b_col);

				// 获取银行名称
				String sql2 = "select BankName from ldbank where bankcode = '"
						+ strBankCode + "'";
				ExeSQL tExeSQL2 = new ExeSQL();
				String tBankName = tExeSQL2.getOneValue(sql2);

				texttag.add("Date", PubFun.getCurrentDate());
				texttag.add("BankCode", strBankCode);
				texttag.add("BankName", tBankName);
				texttag.add("SumDuePayMoney", SumDuePayMoney);
				texttag.add("SumCount", RowNum);
				if (texttag.size() > 0) {
					xmlexport.addTextTag(texttag);
				}
				xmlexport.outputDocumentToFile("e:\\", "NewPayFirstPrintBill"); // 输出xml文档到文件
				mResult.clear();
				mResult.addElement(xmlexport);
			}
		} catch (Exception e) {
			e.printStackTrace();
			mErrors.addOneError("查询数据失败，具体原因是：" + e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * 供前台调用
	 * 
	 * @return
	 */
	public VData getResult() {
		return mResult;
	}

	public static void main(String[] args) {

	}
}
