package com.sinosoft.lis.brieftb;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class BriefCardSignUI {
    public BriefCardSignUI() {
    }

    public static void main(String[] args) {
        BriefCardSignUI briefcardsignui = new BriefCardSignUI();
    }

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /**
     * 向BL传递的接口
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        BriefCardSignBL tBriefCardSignBL = new BriefCardSignBL();
        if (!tBriefCardSignBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tBriefCardSignBL.mErrors);
            return false;
        }
        return true;
    }


}
