package com.sinosoft.lis.brieftb;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import java.io.FileOutputStream;
import com.sinosoft.utility.CError;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author YangMing
 * @version 1.0
 */
public class BriefGrpContToExcelUI {
  public BriefGrpContToExcelUI() {
  }

  /**
   * 全局数据变量
   */
  public CErrors mErrors = new CErrors();
  private VData mResult = new VData();
  private FileOutputStream out = null;

  public boolean submitData(VData nInputData, String cOperate) {
    BriefGrpContToExcelBL tBriefGrpContToExcelBL = new BriefGrpContToExcelBL();
    if (!tBriefGrpContToExcelBL.submitData(nInputData, cOperate)) {
      this.mErrors.copyAllErrors(tBriefGrpContToExcelBL.mErrors);
      return false;
    } else {
      mResult = tBriefGrpContToExcelBL.getResult();
    }
    return true;
  }

  /**
   * 操作结果
   * @return VData
   */
  public VData getResult() {
    return mResult;
  }
  /**
   * getFileOutputStream
   *
   * @return FileOutputStream
   */
  public FileOutputStream getFileOutputStream(){
    return out;
  }
}
