package com.sinosoft.lis.brieftb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.lis.pubfun.GlobalInput;
import java.io.FileOutputStream;
import java.io.OutputStream;
import com.sinosoft.lis.pubfun.MMap;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author YangMing
 * @version 1.0
 */
public class BriefGrpContToExcelBL {
  /** 记录错误对象 */
  public CErrors mErrors = new CErrors();
  /** 前台传入封装数据 */
  private VData mInputData;
  /** 操作符 */
  private String mOperate;
  /** 后台结果 */
  private VData mResult = new VData();
  /** LCGrpCont对应数据封装对象 */
  private LCGrpContSchema mLCGrpContSchema;
  /** 生成Excel对象 */
  private WriteToExcel tWriteToExcel;
  /** 前台传入的参数 */
  private TransferData mTransferData;
  /** Excel输出的路径  */
  private String mOutPutPath = null;
  /** ExcelUrl路径 */
  private String ExcelUrl = null;
  /** FileName */
  private String FileName = null;
  /** 生成Excel文件的Sheet名字，Sheet数目由数组长度决定 */
  private static final String[] SHEET_NAME = {
      "24_Hour_Policy","24_Hour_Policy_Withdrawl"};
  /** Excel文件的标题 */
  private static final String[] TITLE = {
      //1           2                 3
      "Policy No", "Undersign Date", "Undersign Branch",
      //4
      "Valid from"
      //5             6                 7
      , "Valid to", "Product code", "Product Level",
      //8
      "Sum of Inpatient Treatment",
      //9
      "Sum of  Emergecy Outpatient Treatment",
      //10
      "Sum of  Emergecy Dental Treatment"
      //11                12             13
      , "Insured Name", "Insured No.", "Insured  Chinese Alphabetics",
      //14
      "Birth date"
      //15                    16         17
      , "Passport or ID No.", "address", "Postal code",
      //18
      "Name of Contact Person In China",
      //19
      "Tel. No. of Contact Person In China"};

  public BriefGrpContToExcelBL() {
  }

  /**
   * submitData
   *
   * @param nInputData VData
   * @param cOperate String
   * @return boolean
   */
  public boolean submitData(VData nInputData, String cOperate) {
    this.mInputData = nInputData;
    this.mOperate = cOperate;

    if (!getInputData())
      return false;
    System.out.println("完成 getInputData ");
    if (!checkData())
      return false;
    System.out.println("完成 checkData ");
    if (!dealData())
      return false;
    System.out.println("完成 dealData ");
    if (!prepareOutputData())
      return false;
    System.out.println("完成 prepareOutputData ");
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(this.mResult, "INSERT")) {
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      return false;
    }

    return true;
  }

  /**
   * prepareOutputData
   *
   * @return boolean
   */
  private boolean prepareOutputData() {
    try {
      System.out.println("磁盘文件保存目录为 ： " + this.mOutPutPath );
      this.tWriteToExcel.write(this.mOutPutPath);
    }
    catch (Exception ex) {
      ex.printStackTrace();
      System.out.println(
          "程序第113行出错，请检查BriefGrpContToExcelBL.java中的prepareOutputData方法！");
      CError tError = new CError();
      tError.moduleName = "BriefGrpContToExcelBL.java";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "生成Excel文件错误！原因是" + ex.getMessage();
      this.mErrors.addOneError(tError);
      return false;
    }
    this.mTransferData.removeByName("OutPutPath");
    this.mTransferData.setNameAndValue("ExcelUrl",ExcelUrl);
    this.mTransferData.setNameAndValue("ExcelFileName",FileName);
    this.mResult.add(mTransferData);
    /**@author:Yangming 生成完成数据，将合同表中StandByFlag值为1，表示已生成Excel数据 */
    /** 由LCCont下的State字段存储保单已下载状态，00010000 */
    String strSql = "update LCCont set state='00010000' where cardflag='1' and appflag='1'";
    /** 由LBCont 下00010000状态更新为 03040000 */
    String strSql_ZT = "update LCCont set state='03040000' where state='00010000' and cardflag='1' and appflag='1'";
    MMap map = new MMap();
    map.put(strSql,"UPDATE");
    map.put(strSql_ZT,"UPDATE");
    this.mResult.add(map);
    return true;
  }

  public FileOutputStream getFileOutputStream() throws Exception {
    FileOutputStream out = null;
    try {
      out = tWriteToExcel.getFileOutputStream(mOutPutPath);
    }
    catch (Exception ex) {
      ex.printStackTrace();
      return null;
    }
    return out;
  }
  /**
   * dealData
   *
   * @return boolean
   */
  private boolean dealData() {
    //mOperate 是 CREAT||SQL 根据传入的Sql生成数据。
    if (this.mOperate.equals("CREAT||SQL")) {
      //前台传入的Sql语句，根据此语句生成文件
      String Sql = (String) mTransferData.getValueByName("Sql");
      String Sql_ZT = (String) mTransferData.getValueByName("Sql_ZT");
      //生成的文件名
      String fileName = "24_Hour_Policy" + PubFun.getCurrentDate() + "_" +
          PubFun.getCurrentTime().replace(':', '-') + ".xls";
      this.ExcelUrl += fileName;
      this.FileName = fileName;
      tWriteToExcel = new WriteToExcel(fileName);
      tWriteToExcel.createExcelFile();
      tWriteToExcel.setWrapText(true);
      tWriteToExcel.addSheet(SHEET_NAME);
      tWriteToExcel.setTitle(0, TITLE);
      tWriteToExcel.setTitle(1, TITLE);
      tWriteToExcel.setData(Sql);
      tWriteToExcel.setData(1,Sql_ZT);
    }
    return true;
  }

  /**
   * checkData
   *
   * @return boolean
   */
  private boolean checkData() {
    if (mTransferData == null) {
      System.out.println(
          "程序第159行出错，请检查BriefGrpContToExcelBL.java中的checkData方法！");
      CError tError = new CError();
      tError.moduleName = "BriefGrpContToExcelBL.java";
      tError.functionName = "checkData";
      tError.errorMessage = "传入数据为空！";
      this.mErrors.addOneError(tError);
      return false;
    }
    this.mOutPutPath = (String)this.mTransferData.getValueByName("OutPutPath");
    if (this.mOutPutPath == null) {
      System.out.println(
          "程序第169行出错，请检查BriefGrpContToExcelBL.java中的checkData方法！");
      CError tError = new CError();
      tError.moduleName = "BriefGrpContToExcelBL.java";
      tError.functionName = "checkData";
      tError.errorMessage = "缺少数据 ！";
      this.mErrors.addOneError(tError);
      return false;
    }
    this.mOutPutPath +=  "/ExcelFile/";
    this.ExcelUrl = "../ExcelFile/";
    return true;
  }

  /**
   * getInputData
   *
   * @return boolean
   */
  private boolean getInputData() {
    mTransferData = (TransferData) mInputData.getObjectByObjectName(
        "TransferData", 0);
    return true;
  }

  /**
   * getResult
   *
   * @return VData
   */
  public VData getResult() {
    return mResult;
  }

  public static void main(String[] args) {
    TransferData mTransferData = new TransferData();
    String value = "1212321";
    mTransferData.setNameAndValue("value",value);
    String sss = (String) mTransferData.getValueByName("value");
    mTransferData.removeByName("value");
    System.out.println(" mTransferData " +mTransferData.getValueByName("value"));
    String Sql = " select a.grpcontno,a.signdate,a.signcom,a.cvalidate,a.CInValiDate,b.riskcode,b.mult," +
        " ( select d1.standmoney from lcinsured a1, lcgrpcont b1, lcpol c1, lcget d1 where a1.grpcontNo=a.grpcontNo and a1.insuredno=e.insuredno and b1.grpcontNo=a1.grpcontNo and c1.grpcontNo=a1.grpcontNo and c1.insuredno=a1.insuredno and c1.riskcode='1502' and d1.grpcontNo=a1.grpcontNo and d1.polno=c1.polno and d1.getdutycode='613207' )," +
        " (select d2.standmoney from lcinsured a2, lcgrpcont b2, lcpol c2, lcget d2 where a2.grpcontNo=a.grpcontNo and a2.insuredno=e.insuredno and b2.grpcontNo=a2.grpcontNo and c2.grpcontNo=a2.grpcontNo and c2.insuredno=a2.insuredno and c2.riskcode='1502' and d2.grpcontNo=a2.grpcontNo and d2.polno=c2.polno and d2.getdutycode='613208')," +
        " (select d3.standmoney from lcinsured a3, lcgrpcont b3, lcpol c3, lcget d3 where a3.grpcontNo=a.grpcontNo and a3.insuredno=e.insuredno and b3.grpcontNo=a3.grpcontNo and c3.grpcontNo=a3.grpcontNo and c3.insuredno=a3.insuredno and c3.riskcode='1502'and d3.grpcontNo=a3.grpcontNo and d3.polno=c3.polno and d3.getdutycode='613209' )," +
        " b.insuredname,b.insuredno,e.EnglishName,e.Birthday,e.OthIDNo," +
        " d.linkmanaddress1,c.zipcode,d.phone1 from lcgrpcont a, lcpol b,lcgrpappnt c, lcgrpaddress d, lcinsured e " +
        " where a.grpcontno=b.grpcontno and a.grpcontno=c.grpcontno and c.CustomerNo=d.CustomerNo and c.AddressNo=d.AddressNo" +
        " and a.grpcontno=e.grpcontno and a.cardflag='0' and b.riskcode ='1502' and b.insuredno=e.insuredno and a.appflag='1' and a.grpcontno=e.grpcontno and b.contno=e.contno" +
        " union all " +
        " select a.grpcontno,a.signdate,a.signcom,a.cvalidate,a.CInValiDate,b.riskcode,b.mult," +
        " ( select d1.standmoney from lcinsured a1, lcgrpcont b1, lcpol c1, lcget d1 where a1.grpcontNo=a.grpcontNo and a1.insuredno=e.insuredno and b1.grpcontNo=a1.grpcontNo and c1.grpcontNo=a1.grpcontNo and c1.insuredno=a1.insuredno and c1.riskcode='1501' and d1.grpcontNo=a1.grpcontNo and d1.polno=c1.polno and d1.getdutycode='611207' )," +
        " 0," +
        " 0," +
        " b.insuredname,b.insuredno,e.EnglishName,e.Birthday,e.OthIDNo," +
        " d.linkmanaddress1,c.zipcode,d.phone1 from lcgrpcont a, lcpol b,lcgrpappnt c, lcgrpaddress d, lcinsured e " +
        " where a.grpcontno=b.grpcontno and a.grpcontno=c.grpcontno and c.CustomerNo=d.CustomerNo and c.AddressNo=d.AddressNo" +
        " and a.grpcontno=e.grpcontno and a.cardflag='0' and b.riskcode='1501' and b.insuredno=e.insuredno and a.appflag='1' and a.grpcontno=e.grpcontno and b.contno=e.contno";

    GlobalInput tG = new GlobalInput();
    BriefGrpContToExcelUI tBriefGrpContToExcelUI = new BriefGrpContToExcelUI();

    VData tVData = new VData();
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("Sql", Sql);
    tTransferData.setNameAndValue("OutPutPath","E:\\v1.1\\UI\\");
    tVData.add(tG);
    tVData.add(tTransferData);
    tBriefGrpContToExcelUI.submitData(tVData, "CREAT||SQL");
  }
}
