package com.sinosoft.lis.brieftb;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class BriefAgentSignUI {
    public BriefAgentSignUI() {
    }

    /** �������� */
    public CErrors mErrors = new CErrors();

    public boolean submitData(VData cInputData, String cOperate) {
        BriefAgentSignBL tBriefAgentSignBL = new BriefAgentSignBL();
        if (!tBriefAgentSignBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tBriefAgentSignBL.mErrors);
            return false;
        }
        return true;
    }

}
