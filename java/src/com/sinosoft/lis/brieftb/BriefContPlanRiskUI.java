package com.sinosoft.lis.brieftb;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class BriefContPlanRiskUI {
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    public BriefContPlanRiskUI() {
    }

    /**
     * 调试主函数
     * @param args String[]
     */
    public static void main(String[] args) {
        BriefContPlanRiskUI briefcontplanriskui = new BriefContPlanRiskUI();
        briefcontplanriskui.mErrors.getContent();
    }

    public boolean submitData(VData cInputData, String cOperate) {
        BriefContPlanRiskBL tBriefContPlanRiskBL = new BriefContPlanRiskBL();
        if (!tBriefContPlanRiskBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tBriefContPlanRiskBL.mErrors);
        }
        return true;
    }
}
