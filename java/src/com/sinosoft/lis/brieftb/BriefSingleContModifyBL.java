package com.sinosoft.lis.brieftb;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * <p>Title: LIS</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Yangming
 * @version 6.0
 */
public class BriefSingleContModifyBL {
    /** 报错对象 */
    public CErrors mErrors = new CErrors();
    /** 操作完成保存结果 */
    private VData mResult = new VData();
    /** 前台传入的封装对象 */
    private VData mInputData;
    /** 操作符 */
    private String mOperate;
    /** 个人合同信息 */
    private LCContSchema mLCContSchema;
    /** 客户登陆信息 */
    private GlobalInput mGlobalInput;
    /** 投保客户信息 */
    private LCAppntSchema mLCAppntSchema;
    /** 被保险人信息 */
    private LCInsuredSchema mLCInsuredSchema;
    /** 受益人信息 */
    private LCBnfSchema mLCBnfSchema;
    /** 客户信息（受益人不算客户） */
    private LDPersonSet mLDPersonSet = new LDPersonSet();
    /** 地址信息 */
    private LCAddressSet mLCAddressSet = new LCAddressSet();
    /** 险种信息 */
    private LCPolSet mLCPolSet;
    /** 责任信息 */
    private LCDutySet mLCDutySet;
    /** 创建客户标志 */
    private boolean needCreatAppnt = false;
    /** 创建被保客户标志 */
    private boolean needCreatInsured = false;
    /** 投保人客户号 */
    private String mAppntNo;
    private String moldAppntNo;
    /** 被保人客户号 */
    private String mInsuredNo;
    private String moldInsuredNo;
    /** 团体合同号 */
    private String mContNo;
    /** 险种号码 */
    private String mPolNo;
    /** 险种信息 */
//    private LMRiskAppSet mLMRiskAppSet;
    /** 换号标志 */
    private String appntchangecontnoFlag;
    private String insuredchangecontnoFlag;

    /** 当前时间 */
    private static String mCurrentData = PubFun.getCurrentDate();
    private static String mCurrentTime = PubFun.getCurrentTime();
    /** 缴费信息 */
    private LCPremSet mLCPremSet;
    /** 给付 */
    private LCGetSet mLCGetSet;
    /**标记*/
    private String mappntflag;
    private String insuredflag;
    /** 递交数据 */
    private MMap map = new MMap();
    /** 保单级的保费，保额，档次 */
    private double sumPrem, sumAmnt, sumMult;
    /** 封装地址信息的 */
    private TransferData mTransferData = new TransferData();
    /** 投保人地址信息 */
    private LCAddressSchema mAppntAddressSchema;
    /** 被保人信息 */
    private LCAddressSchema mInsuredAddressSchema;
    /** 生成地址编码标志 */
    private boolean needCreatAppntAddressNo = false;
    private boolean needCreatInsuredAddressNo=false;
    /** 抵达国家 */
    private LCNationSet mLCNationSet;
    /** 工作流信息 */
    private LWMissionSet mLWMissionSet;
    /** 打印管理表 */
    private LOPRTManagerSchema mLOPRTManagerSchema;
    public BriefSingleContModifyBL() {
    }

    /**
     * submitData
     *
     * @param nInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData nInputData, String cOperate) {
        this.mInputData = nInputData;
        this.mOperate = cOperate;
        System.out.println("@@完成程序submitData第41行");

        if (!getInputData()) {
            return false;
        }

        if (!checkData()) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit ps = new PubSubmit();
        if (!ps.submitData(this.mResult, "INSERT")) {
            this.mErrors.copyAllErrors(ps.mErrors);
            return false;
        }
        return true;
    }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        /* mappntflag     --1.投保人不换号,修改英文名或地址
           mappntflag     --2.投保人换号不是本人
           mappntflag     --3.修改投保人,投保人与被保人是本人且换号
           mappntflag     --4.投保人不换号,信息没有变化
           insuredflag    --4.修改被保人,投保人与被保人是本人且换号
           insuredflag    --5.被保人不变号
           insuredflag    --6.被保人变号
         */
        if (this.mOperate.equals("UPDATE||MAIN")) {
            mappntflag="4";insuredflag="6";
            System.out.println("mappntflag" + mappntflag);
            System.out.println("insuredflag" + insuredflag);
            String SQl1 = " delete from LDPerson where CustomerNo='" +
                          moldAppntNo + "' ";
            String SQl2 = " delete from LCAddress where CustomerNo='" +
                          moldAppntNo + "' ";
            String SQl3 = " delete from LCInsured where InsuredNo='" +
                          moldAppntNo + "' and prtno='" +
                          mLCContSchema.getPrtNo() + "' ";
            String SQl4 = " delete from LCAppnt where AppntNo='" + moldAppntNo +
                          "' and prtno='" + mLCContSchema.getPrtNo() + "' ";
            String SQl5 = " delete from LDPerson where CustomerNo='" +
                          moldInsuredNo + "' ";
            String SQl6 = " delete from LCAddress where CustomerNo='" +
                          moldInsuredNo + "' ";
            String SQl7 = " delete from LCInsured where InsuredNo='" +
                          moldInsuredNo + "' and prtno='" +
                          mLCContSchema.getPrtNo() + "' ";
            String SQl8 = " delete from LCAppnt where AppntNo='" +
                          moldInsuredNo + "' and prtno='" +
                          mLCContSchema.getPrtNo() + "' ";

            if (StrTool.cTrim(mappntflag).equals("1")) {
                if (this.mLCAddressSet.size() > 0) {
                    map.put(SQl2, "DELETE");
                    map.put(this.mAppntAddressSchema, "INSERT");
                }
                if (this.mLCNationSet.size() > 0) {
                    map.put(this.mLCNationSet, "DELETE&INSERT");
                }
                    String Sql = "update ldperson set englishname='" +
                                 this.mLCAppntSchema.getEnglishName() +
                                 "' where customerno='" + this.mAppntNo + "' ";
                    System.out.println("SQL" + Sql);
                    map.put(Sql, "UPDATE");
                    String Sql1 = "update lcappnt set englishname='" +
                                  this.mLCAppntSchema.getEnglishName() +
                                  "' where appntno='" + this.mAppntNo + "' ";
                    System.out.println("SQL" + Sql1);
                    map.put(Sql1, "UPDATE");
                    if (StrTool.cTrim(this.mLCInsuredSchema.getRelationToAppnt()).
                        equals("00")) {
                        String Sql11 = "update lcinsured set englishname='" +
                                       this.mLCAppntSchema.getEnglishName() +
                                       "' where insuredno='" + this.mAppntNo +
                                       "' ";
                        System.out.println("SQL" + Sql11);
                        map.put(Sql11, "UPDATE");
                    }
            } else if (StrTool.cTrim(mappntflag).equals("2")) {
                //map.put(SQl1, "DELETE");
                map.put(SQl4, "DELETE");
                map.put(this.mLCAppntSchema, "INSERT");
                map.put(this.mLDPersonSet, "INSERT");
                if (this.mLCAddressSet.size() > 0) {
                    map.put(SQl2, "DELETE");
                    map.put(this.mAppntAddressSchema, "INSERT");
                }
                if (this.mLCNationSet.size() > 0) {
                    map.put(this.mLCNationSet, "DELETE&INSERT");
                }
            } else if (StrTool.cTrim(mappntflag).equals("3")) {
               // map.put(SQl1, "DELETE");
                map.put(SQl3, "DELETE");
                map.put(SQl4, "DELETE");
                map.put(this.mLCAppntSchema, "INSERT");
                map.put(this.mLDPersonSet, "INSERT");
                map.put(this.mLCInsuredSchema, "INSERT");
                if (this.mLCAddressSet.size() > 0) {
                    map.put(SQl2, "DELETE");
                    map.put(this.mLCAddressSet, "INSERT");
                }
                if (this.mLCNationSet.size() > 0) {
                    map.put(this.mLCNationSet, "DELETE&INSERT");
                }
            }
            if (StrTool.cTrim(insuredflag).equals("4")&StrTool.cTrim(mappntflag).equals("4")) {
                //map.put(SQl5, "DELETE");
                map.put(SQl7, "DELETE");
                map.put(SQl8, "DELETE");
                map.put(this.mLCAppntSchema, "INSERT");
                map.put(this.mLDPersonSet, "INSERT");
                map.put(this.mLCInsuredSchema, "INSERT");
                if (this.mLCAddressSet.size() > 0) {
                    map.put(SQl2, "DELETE");
                    map.put(SQl6, "DELETE");
                    map.put(this.mLCAddressSet, "INSERT");
                }
                if (this.mLCNationSet.size() > 0) {
                    map.put(this.mLCNationSet, "DELETE&INSERT");
                }
                LCBnfDB tLCBnfDB = new LCBnfDB();
                LCBnfSet tLCBnfSet = new LCBnfSet();
                tLCBnfDB.setContNo(mLCContSchema.getContNo());
                tLCBnfSet = tLCBnfDB.query();
                if (tLCBnfSet.size() > 0) {
                    String SQl = "update LCBnf set " + "InsuredNo='" +
                                 mLCInsuredSchema.getInsuredNo() + "' "
                                 + "where contno='" + mLCContSchema.getContNo() +
                                 "' ";
                    map.put(SQl, "UPDATE");
                }
            } else if (StrTool.cTrim(insuredflag).equals("5")&StrTool.cTrim(mappntflag).equals("4")) {
                LDPersonDB tLDPersonDB = new LDPersonDB();
                LDPersonSet tLDPersonSet = new LDPersonSet();
                tLDPersonDB.setCustomerNo(mInsuredNo);
                tLDPersonSet = tLDPersonDB.query();
                if (this.mLCAddressSet.size() > 0) {
                    map.put(SQl2, "DELETE");
                    map.put(SQl6, "DELETE");
                    map.put(this.mLCAddressSet, "INSERT");
                }
                if (this.mLCNationSet.size() > 0) {
                    map.put(this.mLCNationSet, "DELETE&INSERT");
                }
                if (!StrTool.cTrim(tLDPersonSet.get(1).getEnglishName()).equals(this.
                        mLCInsuredSchema.
                        getEnglishName())) {
                    String Sql = "update ldperson set englishname='" +
                                 this.mLCInsuredSchema.getEnglishName() +
                                 "' where customerno='" + this.mInsuredNo +
                                 "' ";
                    System.out.println("SQL" + Sql);
                    map.put(Sql, "UPDATE");
                    String Sql1 = "update lcinsured set englishname='" +
                                  this.mLCInsuredSchema.getEnglishName() +
                                  "' where insuredno='" + this.mInsuredNo +
                                  "' ";
                    System.out.println("SQL" + Sql1);
                    map.put(Sql1, "UPDATE");
                    if (StrTool.cTrim(this.mLCInsuredSchema.getRelationToAppnt()).
                        equals("00")) {
                        String Sql11 = "update lcappnt set englishname='" +
                                       this.mLCInsuredSchema.getEnglishName() +
                                       "' where appntno='" + this.mInsuredNo +
                                       "' ";
                        System.out.println("SQL" + Sql11);
                        map.put(Sql11, "UPDATE");
                    }
                }
                if (!StrTool.cTrim(tLDPersonDB.getOthIDNo()).equals(this.
                        mLCInsuredSchema.
                        getOthIDNo())) {
                    String Sql = "update ldperson set OthIDNo='" +
                                 this.mLCInsuredSchema.getOthIDNo() +
                                 "' where customerno='" + this.mInsuredNo +
                                 "' ";
                    map.put(Sql, "UPDATE");
                    String Sq1l = "update lcinsured set OthIDNo='" +
                                  this.mLCInsuredSchema.getOthIDNo() +
                                  "' where insuredno='" + this.mInsuredNo +
                                  "' ";
                    map.put(Sq1l, "UPDATE");
                    if (StrTool.cTrim(this.mLCInsuredSchema.getRelationToAppnt()).
                        equals("00")) {
                        String Sq1l1 = "update lcappnt set OthIDNo='" +
                                       this.mLCInsuredSchema.getOthIDNo() +
                                       "' where appntno='" + this.mInsuredNo +
                                       "' ";
                        map.put(Sq1l1, "UPDATE");
                    }
                }
            } else if(StrTool.cTrim(insuredflag).equals("6")&StrTool.cTrim(mappntflag).equals("4")){
                map.put(SQl7, "DELETE");
                if(!moldInsuredNo.equals(moldAppntNo)){
                    //map.put(SQl5, "DELETE");
                }
                map.put(this.mLCInsuredSchema, "INSERT");
                map.put(this.mLDPersonSet, "INSERT");
                /*
                if (this.mLCAddressSet.size() > 0) {
                    map.put(SQl2, "DELETE");
                    map.put(SQl6, "DELETE");
                    map.put(this.mLCAddressSet, "INSERT");
                }
                if (this.mLCNationSet.size() > 0) {
                    map.put(this.mLCNationSet, "DELETE&INSERT");
                }
*/
                LCBnfDB tLCBnfDB = new LCBnfDB();
                LCBnfSet tLCBnfSet = new LCBnfSet();
                tLCBnfDB.setContNo(mLCContSchema.getContNo());
                tLCBnfSet = tLCBnfDB.query();
                if (tLCBnfSet.size() > 0) {
                    String SQl = "update LCBnf set " + "InsuredNo='" +
                                 mLCInsuredSchema.getInsuredNo() + "' "
                                 + "where contno='" + mLCContSchema.getContNo() +
                                 "' ";
                    map.put(SQl, "UPDATE");
                }

            }
            System.out.println("insuredchangecontnoFlag"+insuredchangecontnoFlag);
            System.out.println("appntchangecontnoFlag"+appntchangecontnoFlag);
            /*判断是否合同换号
             1.lccont的state=88888888不换号
             2.lccont的state=88888808换号
             3.lccont的state=88888008换号
             */
            if(StrTool.cTrim(insuredchangecontnoFlag).equals("false")&StrTool.cTrim(appntchangecontnoFlag).equals("false")){
               String tsql1="update lccont set state='88888888' where prtno='"+mLCContSchema.getPrtNo()+"'" ;
               map.put(tsql1, "UPDATE");
           }else if(StrTool.cTrim(insuredchangecontnoFlag).equals("true")&StrTool.cTrim(appntchangecontnoFlag).equals("true")){
               String tsql1="update lccont set state='88888808' where prtno='"+mLCContSchema.getPrtNo()+"'" ;
               map.put(tsql1, "UPDATE");
           }else{
               String tsql1="update lccont set state='88888008' where prtno='"+mLCContSchema.getPrtNo()+"'" ;
               map.put(tsql1, "UPDATE");
           }
            mResult.add(map);
        }
        /*
                mResult.add(this.mLCContSchema);
                mResult.add(this.mLCPolSet);
                mResult.add(this.mLCPremSet);
                mResult.add(this.mLCGetSet);
                mResult.add(this.mLCAppntSchema);
                mResult.add(this.mLDPersonSet);

                if (this.mOperate.equals("INSERT||MAIN") ||
                    this.mOperate.equals("UPDATE||MAIN")) {
                    if (map == null) {
                        map = new MMap();
                    }
                    map.put(this.mLCContSchema, "INSERT");
                    map.put(this.mLCPolSet, "INSERT");
                    map.put(this.mLCDutySet, "INSERT");
                    map.put(this.mLCPremSet, "INSERT");
                    map.put(this.mLCGetSet, "INSERT");
                    map.put(this.mLCAppntSchema, "INSERT");
                    map.put(this.mLCInsuredSchema, "INSERT");
                    map.put(this.mLCBnfSchema, "INSERT");
                    map.put(this.mLDPersonSet, "INSERT");
                    map.put(this.mLWMissionSet, "UPDATE");
                    map.put(this.mLOPRTManagerSchema, "UPDATE");
                    if (this.mLCAddressSet.size() > 0) {
                        map.put(this.mLCAddressSet, "DELETE&INSERT");
                    }
                    if (this.mLCNationSet.size() > 0) {
                        map.put(this.mLCNationSet, "DELETE&INSERT");
                    }
                    mResult.add(map);
                }
                if (this.mOperate.equals("DELETE||MAIN")) {
                    if (this.map.size() > 0) {
                        this.mResult.add(map);
                    } else {
                        buildError("prepareOutputData", "进行删除操作,但没有删除数据！");
                        return false;
                    }
                }
         */
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData() {
        /** 执行新单录入过程 */
        if (this.mOperate.equals("UPDATE||MAIN")) {
            if (!updateData()) {
                return false;
            }
        } else {
            buildError("dealData", "目前只支持保存修改,删除功能不支持其它操作！");
            return false;
        }
        return true;
    }

    /**
     * updateData
     *
     * @return boolean
     */
    private boolean updateData() {
        /** 执行删除和执行插入 */
        if (!UpData()) {
            return false;
        }

        /** 解决合同修改后合同号码重新生成，因此工作流也应该添加相应的更新 */
        /*
                 if (!updateMission()) {
            return false;
                 }
                 if (!updatePrtManager()) {
            return false;
                 }
         */
        return true;
    }

    /**
     * updatePrtManager
     *
     * @return boolean
     */
    private boolean updatePrtManager() {
        /** 处理首期缴费通知书丢失问题 */
        String oldContNo = (new ExeSQL()).getOneValue(
                "select contno from lccont where prtno='" +
                this.mLCContSchema.getPrtNo() + "'");
        if (StrTool.cTrim(oldContNo).equals("")) {
            buildError("updatePrtManager", "查询合同号码失败！");
            return false;
        }
        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setCode("07");
        tLOPRTManagerDB.setOtherNo(oldContNo);
        LOPRTManagerSet tLOPRTManagerSet = tLOPRTManagerDB.query();
        if (tLOPRTManagerSet.size() > 1) {
            buildError("updatePrtManager", "查询出缴费通知书有多条！");
            return false;
        }
        if (tLOPRTManagerSet.size() <= 0) {
            return true;
        }
        mLOPRTManagerSchema = tLOPRTManagerSet.get(1);
        mLOPRTManagerSchema.setOtherNo(this.mContNo);
        return true;
    }

    /**
     * updateMission
     *
     * @return boolean
     */
    private boolean updateMission() {
//        String strSql = "select * from lwmission where missionprop2='" +
//                        this.mLCContSchema.getPrtNo() + "'";
        LWMissionDB tLWMissionDB = new LWMissionDB();
        tLWMissionDB.setMissionProp2(this.mLCContSchema.getPrtNo());
        tLWMissionDB.setActivityID("0000007002");
        mLWMissionSet = tLWMissionDB.query();
        if (mLWMissionSet.size() > 0) {
            for (int i = 1; i <= mLWMissionSet.size(); i++) {
                mLWMissionSet.get(i).setMissionProp1(this.mContNo);
            }
        }
        return true;
    }

    /**
     * insertData
     *
     * @return boolean
     */
    private boolean UpData() {
        /** 先处理客户信息，被保人投保人 */
        if (!dealAppnt()) {
            return false;
        }
        /** 处理投保人地址信息 */
        System.out.println("处理投保人地址信息！");
       /*
        if (!dealAppntAddress()) {
            return false;
        }
        */
        /** 处理完成投保人开始处理被保人信息 */
        System.out.println("开始处理被保险人");
        if (!dealInsured()) {
            return false;
        }
        System.out.println("完成被保人信息录入！");
        /** 处理被保人地址信息 */
        /*
        if (!dealInsuredAddress()) {
            return false;
        }
*/
        /*
               if (!dealCont()) {
                   return false;
               }
               if (!dealRisk()) {
                   return false;
               }
         */
        //modify by zhangxing
        /*
        if (!dealNation()) {
            return false;
        }
*/
        //如果修改生效日期,险种的相关日期都需要修改,此处用于合同维护
        if (!dealCvailDate()) {
            return false;
        }

        return true;
    }

    /**
     * dealBnf
     *
     * @return boolean
     */
    private boolean dealBnf(LCPolSchema tLCPolSchema) {
        /** 意外 */
        this.mLCBnfSchema.setContNo(tLCPolSchema.getContNo());
        this.mLCBnfSchema.setPolNo(tLCPolSchema.getPolNo());
        this.mLCBnfSchema.setInsuredNo(tLCPolSchema.getInsuredNo());
        this.mLCBnfSchema.setBnfType("1");
        this.mLCBnfSchema.setBnfNo(1);
        this.mLCBnfSchema.setBnfGrade("1");
        PubFun.fillDefaultField(mLCBnfSchema);
        mLCBnfSchema.setOperator(this.mGlobalInput.Operator);
        System.out.println("受益人姓名 : " + mLCBnfSchema.getName());
        System.out.println("受益人性别 : " + mLCBnfSchema.getSex());
        System.out.println("受益人证件号码 : " + mLCBnfSchema.getIDNo());
        return true;
    }

    /**
     * dealNation
     *
     * @return boolean
     */
    private boolean dealNation() {
        for (int i = 1; i <= mLCNationSet.size(); i++) {
            System.out.println("国家代码　：");
            System.out.println(mLCNationSet.get(i).getNationNo());
        }

        for (int i = 1; i <= this.mLCNationSet.size(); i++) {
            if (StrTool.cTrim(mLCNationSet.get(i).getNationNo()).equals("")) {
                buildError("dealNation", "没有国家代码！");
                return false;
            }
            LDNationDB tLDNationDB = new LDNationDB();
            tLDNationDB.setNationNo(mLCNationSet.get(i).getNationNo());
            if (!tLDNationDB.getInfo()) {
                buildError("dealNation", "没有找到国家代码对应的国家名称！");
                return false;
            }
            tLDNationDB.setNationNo(mLCNationSet.get(i).getNationNo());
            mLCNationSet.get(i).setContNo(this.mLCContSchema.getContNo());
            mLCNationSet.get(i).setGrpContNo(SysConst.ZERONO);
            mLCNationSet.get(i).setEnglishName(tLDNationDB.getEnglishName());
            mLCNationSet.get(i).setOperator(this.mGlobalInput.Operator);
        }
        PubFun.fillDefaultField(mLCNationSet);
        System.out.println("完成抵达国家处理");
        return true;
    }

    /**
     * dealAddress
     *
     * @return boolean
     */
    private boolean dealAddress() {

        /** 处理被保人地址信息 */
        if (!dealInsuredAddress()) {
            return false;
        }
        return true;
    }

    /**
     * dealAppntAddress
     *
     * @return boolean
     */
    private boolean dealAppntAddress() {
        if (this.mAppntAddressSchema == null) {
            buildError("dealAppntAddress", "没有传入投保人地址信息！");
            return false;
        }
        if (!checkAppntAddress()) {
            return false;
        }
        return true;
    }

    /**
     * checkAppntAddress
     *
     * @return boolean
     */
    private boolean checkAppntAddress() {
        String mAddressNo = "";
        String tneedCreatAddressNo="0";
        System.out.println("投保人" + "地址代码 : " +
                           this.mAppntAddressSchema.getAddressNo());
        System.out.println("是否需要创建客户信息 :　" + (needCreatAppnt ? "是" : "否"));
        if (!StrTool.cTrim(this.mAppntAddressSchema.getAddressNo()).equals("") &&
            !this.needCreatAppnt) {
            System.out.println("需要生成地址代码！");
            LCAddressDB tLCAddressDB = new LCAddressDB();
            tLCAddressDB.setCustomerNo(this.mAppntNo);
            tLCAddressDB.setAddressNo(mAppntAddressSchema.getAddressNo());
            if (!tLCAddressDB.getInfo()) {
                buildError("checkAppntAddress", "录入客户号码与地址编码，但没有在系统中查询到该地址信息！");
                return false;
            }
            /** 前台之会录入 ：
             * 1、联系地址 PostalAddress
             * 2、邮政编码 ZipCode
             * 3、家庭电话 HomePhone
             * 4、移动电话 Mobile
             * 5、办公电话 CompanyPhone
             * 6、电子邮箱 EMail
             * 只校验上述信息如果不同则生成新的地址编码
             */
            if (!StrTool.unicodeToGBK(StrTool.cTrim(mAppntAddressSchema.
                    getPostalAddress())).equals(
                            StrTool.cTrim(tLCAddressDB.getPostalAddress()))) {
                needCreatAppntAddressNo = true;
            }
            if (!StrTool.cTrim(mAppntAddressSchema.getZipCode()).equals(
                    StrTool.cTrim(tLCAddressDB.getZipCode()))) {
                needCreatAppntAddressNo = true;
            }
            if (!StrTool.cTrim(mAppntAddressSchema.getHomePhone()).
                equals(StrTool.cTrim(tLCAddressDB.getHomePhone()))) {
                needCreatAppntAddressNo = true;
            }
            if (!StrTool.cTrim(mAppntAddressSchema.getMobile()).equals(
                    StrTool.cTrim(tLCAddressDB.getMobile()))) {
                needCreatAppntAddressNo = true;
            }
            if (!StrTool.cTrim(mAppntAddressSchema.getCompanyPhone()).
                equals(StrTool.cTrim(tLCAddressDB.getCompanyPhone()))) {
                needCreatAppntAddressNo = true;
            }
            System.out.println(needCreatAppntAddressNo ? "需要生成地址信息" : "不需要生成地址信息");
            if (needCreatAppntAddressNo) {
                ExeSQL tExeSQL = new ExeSQL();
                mAddressNo = tExeSQL.getOneValue("Select Case When max(int(AddressNo)) Is Null Then '0' Else max(int(AddressNo)) End from LCAddress where CustomerNo='"
                                                 + mAppntNo + "'");
                System.out.println("生成的地址代码为：　" + mAddressNo);
                if (StrTool.cTrim(mAddressNo).equals("")) {
                    buildError("checkAppntAddress", "生成地址代码错误！");
                    return false;
                }
                mAppntAddressSchema.setAddressNo(mAddressNo);
                mAppntAddressSchema.setCustomerNo(this.mAppntNo);
            }else{

                tneedCreatAddressNo="1";
            }
            mAppntAddressSchema.setCustomerNo(this.mAppntNo);
        } else {
            mAppntAddressSchema.setCustomerNo(this.mAppntNo);
            mAppntAddressSchema.setAddressNo("1");
        }
        this.mAppntAddressSchema.setOperator(this.mGlobalInput.Operator);
        PubFun.fillDefaultField(this.mAppntAddressSchema);
        mLCAddressSet.add(mAppntAddressSchema);
        System.out.println("完成地址信息 \n地址代码：" +
                           mAppntAddressSchema.getAddressNo() +
                           "\n客户号码" + mAppntAddressSchema.getCustomerNo());
        this.mLCAppntSchema.setAddressNo(mAppntAddressSchema.getAddressNo());
        LDPersonDB tLDPersonDB = new LDPersonDB();
        LDPersonSet tLDPersonSet = new LDPersonSet();
        tLDPersonDB.setCustomerNo(mAppntNo);
        tLDPersonSet = tLDPersonDB.query();
        if(tLDPersonSet.size()>0){
            if (StrTool.cTrim(mappntflag).equals("1") &
                StrTool.cTrim(tneedCreatAddressNo).equals("1") &
                StrTool.cTrim(tLDPersonSet.get(1).getEnglishName()).equals(this.
                    mLCAppntSchema.
                    getEnglishName())) {
                mappntflag = "4";
            }
        }
        return true;
    }

    /**
     * dealInsuredAddress
     *
     * @return boolean
     */
    private boolean dealInsuredAddress() {
    if(!StrTool.cTrim(mappntflag).equals("1")|!StrTool.cTrim(mappntflag).equals("2")){
        if (StrTool.cTrim(this.mLCInsuredSchema.getRelationToAppnt()).
            equals(
                    "00")) {
            /** 投保人和被保人是本人,不需要生成地址编码 */
            mLCInsuredSchema.setAddressNo(this.mLCAppntSchema.
                                          getAddressNo());
            return true;
        } else {
            if (!checkInsuredAddress()) {
                return false;
            }
        }
    }
                      return true;
    }

    /**
     * checkInsuredAddress
     *
     * @return boolean
     */
    private boolean checkInsuredAddress() {
        String mAddressNo = "";
        System.out.println("投保人" + "地址代码 : " +
                           this.mInsuredAddressSchema.getAddressNo());
        System.out.println("是否需要创建客户信息 :　" + (needCreatInsured ? "是" : "否"));
        if (!StrTool.cTrim(this.mInsuredAddressSchema.getAddressNo()).equals("") &&
            !this.needCreatInsured) {
            LCAddressDB tLCAddressDB = new LCAddressDB();
            if(StrTool.cTrim(mappntflag).equals("4")){
                tLCAddressDB.setCustomerNo(this.mInsuredNo);
                tLCAddressDB.setAddressNo(mInsuredAddressSchema.getAddressNo());
            }else{
                tLCAddressDB.setCustomerNo(this.moldInsuredNo);
                tLCAddressDB.setAddressNo(mInsuredAddressSchema.getAddressNo());
            }
            if (!tLCAddressDB.getInfo()) {
                buildError("checkInsuredAddress",
                           "录入客户号码与地址编码，但没有在系统中查询到该地址信息！");
                return false;
            }
            /** 前台之会录入 ：
             * 1、联系地址 PostalAddress
             * 2、邮政编码 ZipCode
             * 3、家庭电话 HomePhone
             * 4、移动电话 Mobile
             * 5、办公电话 CompanyPhone
             * 6、电子邮箱 EMail
             * 只校验上述信息如果不同则生成新的地址编码
             */
            if (!StrTool.cTrim(mInsuredAddressSchema.getPostalAddress()).equals(
                    StrTool.cTrim(tLCAddressDB.getPostalAddress()))) {
                needCreatInsuredAddressNo = true;
            } else if (!StrTool.cTrim(mInsuredAddressSchema.getZipCode()).
                       equals(StrTool.cTrim(tLCAddressDB.getZipCode()))) {
                needCreatInsuredAddressNo = true;
            } else if (!StrTool.cTrim(mInsuredAddressSchema.getHomePhone()).
                       equals(StrTool.cTrim(tLCAddressDB.getHomePhone()))) {
                needCreatInsuredAddressNo = true;
            } else if (!StrTool.cTrim(mInsuredAddressSchema.getMobile()).equals(
                    StrTool.cTrim(tLCAddressDB.getMobile()))) {
                needCreatInsuredAddressNo = true;
            } else if (!StrTool.cTrim(mInsuredAddressSchema.getCompanyPhone()).
                       equals(StrTool.cTrim(tLCAddressDB.getCompanyPhone()))) {
                needCreatInsuredAddressNo = true;
            }
            if (needCreatInsuredAddressNo) {
                ExeSQL tExeSQL = new ExeSQL();
                mAddressNo = tExeSQL.getOneValue("Select Case When max(int(AddressNo)) Is Null Then '0' Else max(int(AddressNo)) End from LCAddress where CustomerNo='"
                                                 + mInsuredNo + "'");
                if (StrTool.cTrim(mAddressNo).equals("")) {
                    buildError("checkAppntAddress", "生成地址代码错误！");
                    return false;
                }
                mInsuredAddressSchema.setCustomerNo(mLCInsuredSchema.
                        getInsuredNo());
                mInsuredAddressSchema.setAddressNo(mAddressNo);
            }
            mInsuredAddressSchema.setCustomerNo(mLCInsuredSchema.getInsuredNo());
        } else {
            mInsuredAddressSchema.setCustomerNo(mLCInsuredSchema.getInsuredNo());
            mInsuredAddressSchema.setAddressNo("1");
        }
        this.mInsuredAddressSchema.setOperator(this.mGlobalInput.Operator);
        PubFun.fillDefaultField(this.mInsuredAddressSchema);
        this.mLCAddressSet.add(mInsuredAddressSchema);
        System.out.println("完成地址信息 \n地址代码：" +
                           mInsuredAddressSchema.getAddressNo() +
                           "\n客户号码" + mInsuredAddressSchema.getCustomerNo());
        this.mLCInsuredSchema.setAddressNo(this.mInsuredAddressSchema.
                                           getAddressNo());
        return true;
    }

    /**
     * dealCont
     *
     * @return boolean
     */
    private boolean dealCont() {
        if (!StrTool.cTrim(this.mOperate).equals("DELETE||MAIN")) {
            /** 生成号码 */
            System.out.println("babab" + mLCContSchema.getContNo());
            LCContDB tLCContDB = new LCContDB();
            LCContSet tLCContSet = new LCContSet();
            tLCContDB.setPrtNo(mLCContSchema.getPrtNo());
            tLCContSet = tLCContDB.query();
            System.out.println("zhuzhuzhzu++++" + tLCContSet.size());
            if (tLCContSet.size() == 0) {
                String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
                this.mContNo = PubFun1.CreateMaxNo("ProposalContNo", tLimit);
            } else {
                mContNo = tLCContSet.get(1).getContNo();
            }

            if (StrTool.cTrim(mContNo).equals("")) {
                buildError("dealCont", "生成合同号码错误！");
                return false;
            }
            System.out.println("合同号码 " + this.mContNo);
            /**  保存合同号码 */
            this.mLCContSchema.setContNo(this.mContNo);
            this.mLCContSchema.setProposalContNo(this.mContNo);
            this.mLCContSchema.setGrpContNo(SysConst.ZERONO);
            /** 添加管理机构 */
            if (!dealAgentAndMangeCom()) {
                return false;
            }
            this.mLCContSchema.setContType("1"); //个单
            this.mLCContSchema.setInputOperator(this.mGlobalInput.Operator);
            this.mLCContSchema.setInputDate(this.mCurrentData);
            this.mLCContSchema.setInputTime(this.mCurrentTime);
            this.mLCContSchema.setApproveCode(this.mGlobalInput.Operator);
            this.mLCContSchema.setApproveDate(this.mCurrentData);
            this.mLCContSchema.setApproveFlag("9"); //符合通过
            this.mLCContSchema.setApproveTime(this.mCurrentTime);
            this.mLCContSchema.setUWFlag("9");
            this.mLCContSchema.setUWOperator(this.mGlobalInput.Operator);
            this.mLCContSchema.setUWDate(this.mCurrentData);
            this.mLCContSchema.setUWTime(this.mCurrentTime);
            this.mLCContSchema.setAppFlag("0");
            this.mLCContSchema.setPolApplyDate(this.mCurrentData);
            this.mLCContSchema.setOperator(this.mGlobalInput.Operator);
            mLCContSchema.setSignCom(this.mGlobalInput.ManageCom);
            PubFun.fillDefaultField(this.mLCContSchema);
            this.mLCContSchema.setProposalType("01");
            /** 保险终止日期需要处理一下,要加一天 */
            System.out.println(mLCContSchema.getCInValiDate());
            mLCContSchema.setCInValiDate(
                    PubFun.calDate(
                            this.mLCContSchema.getCInValiDate(), 1, "D", null));
            System.out.println("连锁店缴费连锁店缴费连锁店缴费三大件福连锁店缴费连锁店就弗里敦缴费连锁店缴费连锁店飞机 " +
                               mLCContSchema.getCInValiDate());
            if (!fillOtherTable()) {
                return false;
            }
            System.out.println("");
            /** 完成合同信息 */
        }
        return true;
    }

    /**
     * fillOtherTable
     *
     * @return boolean
     */
    private boolean fillOtherTable() {
        this.mLCInsuredSchema.setContNo(this.mContNo);
        this.mLCInsuredSchema.setPrtNo(this.mLCContSchema.getPrtNo());
        this.mLCInsuredSchema.setGrpContNo(SysConst.ZERONO);
        this.mLCInsuredSchema.setManageCom(this.mLCContSchema.getManageCom());
        this.mLCInsuredSchema.setExecuteCom(this.mGlobalInput.ManageCom);
        this.mLCAppntSchema.setContNo(this.mContNo);
        this.mLCAppntSchema.setPrtNo(this.mLCContSchema.getPrtNo());
        this.mLCAppntSchema.setGrpContNo(SysConst.ZERONO);
        return true;
    }

    /**
     * dealAgentAndMangeCom
     *
     * @return boolean
     */
    private boolean dealAgentAndMangeCom() {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(this.mGlobalInput.ManageCom);
        if (!tLDComDB.getInfo()) {
            buildError("dealAgentAndMangeCom", "查询管理机构失败请确认管理机构是否录入正确！");
            return false;
        }
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(this.mLCContSchema.getAgentCode());
        if (!tLAAgentDB.getInfo()) {
            buildError("dealAgentAndMangeCom", "查询代理人失败！");
            return false;
        }
        if (!StrTool.cTrim(tLAAgentDB.getManageCom()).equals(this.mLCContSchema.
                getManageCom())) {
            buildError("dealAgentAndMangeCom",
                       "代理人与所录入的管理机构不对应,请确认代理人或管理机构是否录入正确！");
            return false;
        }
        /** 校验代理人组别,如果传入代理人组别则校验代理组别是否对应,如果没有传入,就填充 */
        if (this.mLCContSchema.getAgentGroup() == null) {
            this.mLCContSchema.setAgentGroup(tLAAgentDB.getAgentGroup());
        } else {
            if (!this.mLCContSchema.getAgentGroup().equals(tLAAgentDB.
                    getAgentGroup())) {
                buildError("dealAgentAndMangeCom", "代理人组别录入错误,请检查该代理人是否录入错误！");
                return false;
            }
        }
//        if (this.mLCContSchema.getAgentType() == null) {
//            this.mLCContSchema.setAgentType(tLAAgentDB.getagent);
//        }
        if (this.mLCContSchema.getPayIntv() != 0) {
            buildError("dealAgentAndMangeCom",
                       "境外救援不不会有其他缴费频次,只有趸缴,请录入缴费频次代码 0 ！");
            return false;
        }
        if (this.mLCContSchema.getPayMode() == null) {
            buildError("dealAgentAndMangeCom", "没有录入缴费方式！");
            return false;
        }

        return true;
    }

    /**
     * dealRisk
     *
     * @return boolean
     */
    private boolean dealRisk() {
        /** 处理险种信息，主要是取出LMRiskApp中的险种信息，放到LCPol表中 */
        System.out.println("开始处理险种信息");
        System.out.println("本保单有" + this.mLCPolSet.size() + "个险种");
        mLCDutySet = new LCDutySet();
        mLCPremSet = new LCPremSet();
        mLCGetSet = new LCGetSet();
        CalBL tCalBL;
        for (int i = 1; i <= this.mLCPolSet.size(); i++) {
            /**
             * 针对每一个险种的信息进行的操作 :
             * 1、首先生成PolNo
             * 2、将一般险种信息封装。
             * 3、处理责任信息，把责任信息封装后传入CalBL中进行保费计算。
             * 4、获取封装信息，回填LCPol，LCCont的保费保额，缴至日期，生效日期，失效日期，责任终止日期
             **/

            LCPolSchema tLCPolSchema = this.mLCPolSet.get(i);
            if (!dealLCPol(tLCPolSchema, i)) {
                return false;
            }
            String mRiskCode = mLCPolSet.get(i).getRiskCode();
            /** 封装责任信息 */
            LMRiskDutyDB tLMRiskDutyDB = new LMRiskDutyDB();
            tLMRiskDutyDB.setRiskCode(mRiskCode);
            LMRiskDutySet tLMRiskDutySet = tLMRiskDutyDB.query();
            LCDutySet tLCDutySet = getDuty(tLMRiskDutySet, mLCPolSet.get(i));
            System.out.println("测试档次才决定；送风机司法苏联法扫雷；冬季" +
                               tLCDutySet.get(1).getMult());
            System.out.println("责任准备完毕");
            /** 责任信息封装完毕 */
            tCalBL = new CalBL(tLCPolSchema, tLCDutySet, null, null);
            if (!tCalBL.calPol()) {
                this.mErrors.copyAllErrors(tCalBL.mErrors);
                return false;
            }
            if (tCalBL.mErrors.needDealError()) {
                this.mErrors.copyAllErrors(tCalBL.mErrors);
                return false;
            }
            tLCPolSchema.setSchema(tCalBL.getLCPol());
            tLCDutySet = tCalBL.getLCDuty();
            LCPremSet tLCPremSet = tCalBL.getLCPrem();
            LCGetSet tLCGetSet = tCalBL.getLCGet();
            for (int m = 1; m <= tLCDutySet.size(); m++) {
                System.out.println("验证档次 :" + tLCDutySet.get(m).getMult());
                tLCDutySet.get(m).setPolNo(this.mPolNo);
                tLCPolSchema.setGetYear(tLCDutySet.get(m).getGetYear());
                tLCPolSchema.setGetYearFlag(tLCDutySet.get(m).getGetYearFlag());
                tLCPolSchema.setPayEndYearFlag(tLCDutySet.get(m).
                                               getPayEndYearFlag());
                tLCPolSchema.setPayEndYear(tLCDutySet.get(m).getPayEndYear());
                tLCPolSchema.setInsuYearFlag(tLCDutySet.get(m).getInsuYearFlag());
                tLCPolSchema.setInsuYear(tLCDutySet.get(m).getInsuYear());
                tLCPolSchema.setMult(tLCDutySet.get(m).getMult());
            }
            for (int m = 1; m <= tLCPremSet.size(); m++) {
                tLCPremSet.get(m).setPolNo(this.mPolNo);
                tLCPremSet.get(m).setGrpContNo(SysConst.ZERONO);
                tLCPremSet.get(m).setContNo(this.mContNo);
                tLCPremSet.get(m).setOperator(this.mGlobalInput.Operator);
            }
            for (int m = 1; m <= tLCGetSet.size(); m++) {
                tLCGetSet.get(m).setPolNo(this.mPolNo);
                tLCGetSet.get(m).setGrpContNo(SysConst.ZERONO);
                tLCGetSet.get(m).setContNo(this.mContNo);
                tLCGetSet.get(m).setOperator(this.mGlobalInput.Operator);
            }
            PubFun.fillDefaultField(tLCPremSet);
            PubFun.fillDefaultField(tLCGetSet);
            PubFun.fillDefaultField(tLCDutySet);
            this.mLCDutySet.add(tLCDutySet);
            this.mLCPremSet.add(tLCPremSet);
            this.mLCGetSet.add(tLCGetSet);
        }
        /** 将保费维护到LCCont表中 */
        for (int i = 1; i <= this.mLCPolSet.size(); i++) {
            sumPrem += this.mLCPolSet.get(i).getPrem();
            sumAmnt += this.mLCPolSet.get(i).getAmnt();
            sumMult += this.mLCPolSet.get(i).getMult();
        }
        this.mLCContSchema.setPrem(sumPrem);
        this.mLCContSchema.setAmnt(sumAmnt);
        this.mLCContSchema.setMult(sumMult);
        return true;
    }

    /**
     * dealLCPol
     *
     * @return boolean
     * @param tLCPolSchema LCPolSchema
     * @param seq int
     */
    private boolean dealLCPol(LCPolSchema tLCPolSchema, int seq) {
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        LMRiskDB tLMRiskDB = new LMRiskDB();

        mPolNo = PubFun1.CreateMaxNo("ProposalNo",
                                     PubFun.getNoLimit(mLCContSchema.
                getManageCom()));
        if (StrTool.cTrim(this.mPolNo).equals("")) {
            buildError("dealRisk", "生成险种号码错误！");
            return false;
        }
        System.out.println("生成的险种号码是 ：" + this.mPolNo);
        System.out.println("险种是" + tLCPolSchema.getRiskCode());
        tLMRiskAppDB.setRiskCode(tLCPolSchema.getRiskCode());
        tLMRiskDB.setRiskCode(tLCPolSchema.getRiskCode());
        if (!tLMRiskAppDB.getInfo() || !tLMRiskDB.getInfo()) {
            buildError("dealRisk", "查询险种信息失败！");
            return false;
        }
        tLCPolSchema.setPolNo(this.mPolNo);
        tLCPolSchema.setProposalNo(this.mPolNo);
        tLCPolSchema.setContNo(this.mContNo);
        tLCPolSchema.setPrtNo(this.mLCContSchema.getPrtNo());
        tLCPolSchema.setGrpContNo(SysConst.ZERONO);
        tLCPolSchema.setGrpPolNo(SysConst.ZERONO);
        tLCPolSchema.setContType("1"); //个单
        tLCPolSchema.setPolTypeFlag("0");
        tLCPolSchema.setMainPolNo(this.mPolNo);
        tLCPolSchema.setProposalContNo(this.mContNo);
        tLCPolSchema.setKindCode(tLMRiskAppDB.getKindCode());
        tLCPolSchema.setRiskVersion(tLMRiskAppDB.getRiskVer());
        tLCPolSchema.setManageCom(this.mLCContSchema.getManageCom());
        tLCPolSchema.setAgentCom(this.mLCContSchema.getAgentCom());
        tLCPolSchema.setAgentType(this.mLCContSchema.getAgentType());
        tLCPolSchema.setAgentCode(this.mLCContSchema.getAgentCode());
        tLCPolSchema.setAgentGroup(this.mLCContSchema.getAgentCode());
        tLCPolSchema.setSaleChnl(this.mLCContSchema.getSaleChnl());
        tLCPolSchema.setSaleChnlDetail(this.mLCContSchema.getSaleChnlDetail());
        tLCPolSchema.setCValiDate(this.mLCContSchema.getCValiDate());
        tLCPolSchema.setApproveCode(this.mGlobalInput.Operator);
        tLCPolSchema.setInsuredAppAge(
                PubFun.getInsuredAppAge(this.mLCContSchema.getCValiDate(),
                                        this.mLCInsuredSchema.getBirthday()));
        tLCPolSchema.setInsuredPeoples(1);
        tLCPolSchema.setAutoPayFlag(tLMRiskAppDB.getAutoPayFlag());
        if (StrTool.cTrim(tLMRiskDB.getRnewFlag()).equals("N")) {
            tLCPolSchema.setRnewFlag( -2);
        }
        tLCPolSchema.setComFeeRate(tLMRiskAppDB.getAppInterest());
        tLCPolSchema.setBranchFeeRate(tLMRiskAppDB.getAppPremRate());
        /** 不指定生效日期 */
        tLCPolSchema.setSpecifyValiDate("1");
        tLCPolSchema.setPayMode(this.mLCContSchema.getPayMode());
        tLCPolSchema.setPayIntv(this.mLCContSchema.getPayIntv());
        tLCPolSchema.setApproveDate(this.mCurrentData);
        tLCPolSchema.setApproveFlag("9"); //符合通过
        tLCPolSchema.setApproveTime(this.mCurrentTime);
        tLCPolSchema.setUWFlag("9");
        tLCPolSchema.setUWCode(this.mGlobalInput.Operator);
        tLCPolSchema.setUWDate(this.mCurrentData);
        tLCPolSchema.setUWTime(this.mCurrentTime);
        tLCPolSchema.setAppFlag("0");
        tLCPolSchema.setPolApplyDate(this.mCurrentData);
        tLCPolSchema.setOperator(this.mGlobalInput.Operator);
//        tLCPolSchema.setProposalContNo(mLCContSchema.getProposalContNo());
        PubFun.fillDefaultField(tLCPolSchema);
        tLCPolSchema.setRiskSeqNo(String.valueOf(seq).length() <= 1 ?
                                  "0" + String.valueOf(seq) :
                                  String.valueOf(seq));
        System.out.println(mLCBnfSchema);
        System.out.println(StrTool.cTrim(tLMRiskAppDB.getBnfFlag()));
        if (this.mLCBnfSchema != null &&
            StrTool.cTrim(tLMRiskAppDB.getBnfFlag()).equals("N")) {
            System.out.println("开始处理受益人信息");
            if (!dealBnf(tLCPolSchema)) {
                return false;
            }
        }
        return true;
    }

    /**
     * getDuty
     *
     * @param tLMRiskDutySet LMRiskDutySet
     * @param tLCPolSchema LCPolSchema
     * @return LCDutySet
     */
    private LCDutySet getDuty(LMRiskDutySet tLMRiskDutySet,
                              LCPolSchema tLCPolSchema) {
        LCDutySet tLCDutySet = new LCDutySet();
        for (int i = 1; i <= tLMRiskDutySet.size(); i++) {
            LCDutySchema tLCDutySchema = new LCDutySchema();
            /** 查询责任描述 */
            LMDutyDB tLMDutyDB = new LMDutyDB();
            tLMDutyDB.setDutyCode(tLMRiskDutySet.get(i).getDutyCode());
            if (!tLMDutyDB.getInfo()) {
                buildError("getDuty", "查询险种责任出错！");
                return null;
            }
            tLCDutySchema.setDutyCode(tLMRiskDutySet.get(i).getDutyCode());
            /** 境外救援是旅行次数 */
            tLCDutySchema.setStandbyFlag1(this.mLCContSchema.getDegreeType());
            /** 保额 */
            tLCDutySchema.setAmnt(tLCPolSchema.getAmnt());
            /** 保费 */
            tLCDutySchema.setMult(tLCPolSchema.getMult());
            /** 保费 */
            tLCDutySchema.setCopys(tLCPolSchema.getCopys());
            /** 保险期间 */
            int insuYear = getInsuYear(mLCContSchema.getCValiDate(),
                                       mLCContSchema.getCInValiDate(),
                                       tLMDutyDB.getInsuYearFlag());
            tLCDutySchema.setInsuYear(insuYear);
            tLCDutySchema.setInsuYearFlag(tLMDutyDB.getInsuYearFlag());
            tLCDutySet.add(tLCDutySchema);
        }
        return tLCDutySet;
    }

    /**
     * getInsuYear
     *
     * @param tCValiDate String
     * @param tCInValiDate String
     * @param tInsuYearFlag String
     * @return int
     */
    private int getInsuYear(String tCValiDate, String tCInValiDate,
                            String tInsuYearFlag) {
        return PubFun.calInterval2(tCValiDate, tCInValiDate, tInsuYearFlag);
    }

    /**
     * <h1>处理被保险人总体思路</h1>
     * <li>1.首先判断投保人和被保人是否是本人.</li>
     * <li>2.如果有客户号校验客户信息是否匹配,如果没有客户号,根据客户信息查询出客户号码</li>
     * <li>3.生成的被保客户信息填充到险种表和合同表.</li>
     *
     * @return boolean
     */
    private boolean dealInsured() {
        //if (StrTool.cTrim(mappntflag).equals("4")) {
            /** 新建被保险客户 */
            if (!checkInsured()) {
                return false;
            }
            if (this.needCreatInsured) {
                /** 检查完被保人，根据创建标志，判断是否新建客户 */
                this.mInsuredNo = PubFun1.CreateMaxNo("CUSTOMERNO", "SN");
                if (StrTool.cTrim(this.mInsuredNo).equals("")) {
                    // @@错误处理
                    System.out.println("BriefSingleContInputBL中"
                                       + "insertData方法报错，"
                                       + "在程序202行，Author:Yangming");
                    CError tError = new CError();
                    tError.moduleName = "BriefSingleContModifyBL";
                    tError.functionName = "insertData";
                    tError.errorMessage = "生成客户号码错误！";
                    this.mErrors.addOneError(tError);
                    return false;
                } else {
                    this.mLCInsuredSchema.setAppntNo(this.mAppntNo);
                    this.mLCInsuredSchema.setInsuredNo(this.mInsuredNo);
                    /** 生成客户 */
                    LDPersonSchema tLDPersonSchema = new LDPersonSchema();
                    tLDPersonSchema.setCustomerNo(this.mInsuredNo);
                    tLDPersonSchema.setName(this.mLCInsuredSchema.getName());
                    tLDPersonSchema.setSex(this.mLCInsuredSchema.getSex());
                    tLDPersonSchema.setBirthday(this.mLCInsuredSchema.
                                                getBirthday());
                    tLDPersonSchema.setIDType(this.mLCInsuredSchema.
                                              getIDType());
                    tLDPersonSchema.setIDNo(this.mLCInsuredSchema.getIDNo());
                    tLDPersonSchema.setOperator(this.mGlobalInput.Operator);
                    PubFun.fillDefaultField(tLDPersonSchema);
                    this.mLDPersonSet.add(tLDPersonSchema);

                }
                /** 封装Makedate等信息 */
                PubFun.fillDefaultField(mLCInsuredSchema);
                this.mLCInsuredSchema.setOperator(this.mGlobalInput.Operator);
                this.mLCInsuredSchema.setManageCom(this.mGlobalInput.
                        ManageCom);
                System.out.println("moldInsuredNo" + moldInsuredNo);
                System.out.println("mLCInsuredSchema.getInsuredNo()" +
                                   mLCInsuredSchema.getInsuredNo());
                if (!moldInsuredNo.equals(mLCInsuredSchema.getInsuredNo()) &&
                    StrTool.cTrim(this.mLCInsuredSchema.getRelationToAppnt()).
                    equals(
                            "00")) {
                    insuredflag = "4";
                    if (!dealInsuredToAppnt()) {
                    }
                }
                System.out.println("处理完成被保人");
            }
            if (!dealInsuredToCont()) {
                return false;
            }
      //  }
        return true;
    }

    /**
     * dealInsuredToCont
     *
     * @return boolean
     */
    private boolean dealInsuredToCont() {
        String Sql1 = "update lccont set InsuredBirthday='" +
                      this.mLCInsuredSchema.getBirthday() +
                      "',InsuredIDNo='" + this.mLCInsuredSchema.getIDNo() +
                      "',InsuredIDType='" + this.mLCInsuredSchema.getIDType() +
                      "',InsuredName='" + this.mLCInsuredSchema.getName() +
                      "',InsuredNo='" + this.mLCInsuredSchema.getInsuredNo() +
                      "',InsuredSex='" + this.mLCInsuredSchema.getSex() +
                      "' where prtno='" + mLCContSchema.getPrtNo() + "' ";
        map.put(Sql1, "UPDATE");
        int tInsuredAge = 0;
        tInsuredAge = PubFun.calInterval(mLCInsuredSchema.getBirthday(),
                                         mLCPolSet.get(1).getCValiDate(), "Y");
        if(tInsuredAge>70||tInsuredAge<3){
          buildError("dealInsuredToCont", "被保险人年龄超出范围！");
           return false;
      }
        System.out.println("tInsuredAge" + tInsuredAge);
        String Sql2 = "update lcpol set InsuredBirthday='" +
                      this.mLCInsuredSchema.getBirthday() +
                      "',InsuredName='" + this.mLCInsuredSchema.getName() +
                      "',InsuredNo='" + this.mLCInsuredSchema.getInsuredNo() +
                      "',InsuredSex='" + this.mLCInsuredSchema.getSex() +
                      "',insuredappage=" + tInsuredAge +
                      " where prtno='" + mLCContSchema.getPrtNo() + "' ";
        map.put(Sql2, "UPDATE");
        /*
        if (StrTool.cTrim(insuredflag).equals("4") &
            StrTool.cTrim(this.mLCInsuredSchema.getRelationToAppnt()).equals(
                "00")&StrTool.cTrim(insuredflag).equals("4")) {
            String Sql11 = "update lccont set appntBirthday='" +
                           this.mLCInsuredSchema.getBirthday() +
                           "',appntIDNo='" + this.mLCInsuredSchema.getIDNo() +
                           "',appntIDType='" + this.mLCInsuredSchema.getIDType() +
                           "',appntName='" + this.mLCInsuredSchema.getName() +
                           "',appntNo='" + this.mLCInsuredSchema.getInsuredNo() +
                           "',appntSex='" + this.mLCInsuredSchema.getSex() +
                           "' where prtno='" + mLCContSchema.getPrtNo() + "' ";
            map.put(Sql11, "UPDATE");
            String Sql111 = "update lcpol set AppntNo='" +
                            this.mLCInsuredSchema.getInsuredNo() +
                            "',AppntName='" + this.mLCInsuredSchema.getName() +
                            "' where prtno='" + mLCContSchema.getPrtNo() + "' ";
            map.put(Sql111, "UPDATE");
        }
*/
        return true;
    }

    /**
     * <h1>处理投保人的总体思路:</h1><br>
     * 1,首先判断是否录入客户号码,如果录入客户号码,校验姓名性别等基本信息是否相同,如果不同提示需要重新生成客户号<br>
     * 2.如果没有录入客户号码,首先根据客户基本信息查询系统中可客户,如果找到填充客户号码.如果没有找到生成新客户.<br>
     * 3.生成投保人信息后,将投报人信息填充到合同表中.
     * @return boolean
     */
    private boolean dealAppnt() {
        System.out.println("处理投保人!");
        /*        if (this.checkLCAppnt() == false) {
                    return false;
                }
         */
        System.out.println("开始处理投保客户");
        LDPersonDB tLDPersonDB = new LDPersonDB();
        if (!StrTool.cTrim(this.mLCAppntSchema.getAppntNo()).equals("")) {
            this.mAppntNo = this.mLCAppntSchema.getAppntNo();
            tLDPersonDB.setCustomerNo(mLCAppntSchema.getAppntNo());
            if (!tLDPersonDB.getInfo()) {
                buildError("dealAppnt", "录入投保客户号码但是系统中没有此刻户信息，请确认客户号码是否录入错误！！");
                return false;
            } else { //查询到客户信息
                /** 如果是老客户校验客户信息与投保信息是否相同 */
                this.mAppntNo = mLCAppntSchema.getAppntNo();
                if (!checkAppnt(tLDPersonDB.getSchema())) {
                    return false;
                }
                mappntflag = "1";

            }
        } else { //没有录入客户号码
            /** 如果没有客户号码信息,根据五个基本信息教研是否是同一个客户 */
            if (!queryAppnt()) {
                return false;
            }
        }
        /** 完成上面的操作,如果判断是新客户则生成客户号 */
        if (this.needCreatAppnt) {
            mAppntNo = PubFun1.CreateMaxNo("CUSTOMERNO", "SN");
            mLDPersonSet = new LDPersonSet();
            if (StrTool.cTrim(this.mAppntNo).equals("")) {
                CError tError = new CError();
                tError.moduleName = "BriefSingleContModifyBL";
                tError.functionName = "insertData";
                tError.errorMessage = "生成团体客户号错误！";
                System.out.println("程序第157行出错，"
                                   + "请检查BriefSingleContInputBL中的"
                                   + "insertData方法！" + tError.errorMessage);
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mLCAppntSchema.setAppntNo(this.mAppntNo);
            /** 生成客户 */
            LDPersonSchema tLDPersonSchema = new LDPersonSchema();
            tLDPersonSchema.setCustomerNo(this.mAppntNo);
            tLDPersonSchema.setName(this.mLCAppntSchema.getAppntName());
            tLDPersonSchema.setSex(this.mLCAppntSchema.getAppntSex());
            tLDPersonSchema.setBirthday(this.mLCAppntSchema.getAppntBirthday());
            tLDPersonSchema.setIDType(this.mLCAppntSchema.getIDType());
            tLDPersonSchema.setIDNo(this.mLCAppntSchema.getIDNo());
            tLDPersonSchema.setEnglishName(this.mLCAppntSchema.getEnglishName());
            tLDPersonSchema.setOperator(this.mGlobalInput.Operator);
            PubFun.fillDefaultField(tLDPersonSchema);
            this.mLDPersonSet.add(tLDPersonSchema);
        }
        /** 封装Makedate等信息 */
        PubFun.fillDefaultField(mLCAppntSchema);
        this.mLCAppntSchema.setOperator(this.mGlobalInput.Operator);
        this.mLCAppntSchema.setManageCom(this.mGlobalInput.ManageCom);
        System.out.println("处理完成投保人");
        System.out.println("moldAppntNo" + moldAppntNo);
        System.out.println("mLCAppntSchema.getAppntNo()" +
                           mLCAppntSchema.getAppntNo());
        /** 首先判断被保人与投保人的关系,如果是投保人被人,则直接付值 */
        if (!moldAppntNo.equals(mLCAppntSchema.getAppntNo()) &&
            StrTool.cTrim(this.mLCInsuredSchema.getRelationToAppnt()).equals(
                    "00")) {
            mappntflag = "3";
            if (!dealAppntToInsured()) {
                return false;
            }
            if (!dealInsuredToCont()) {
                return false;
            }
        }

        /** 处理完成投保人,将投保人信息填充到合同信息中 */
        if (!dealAppntToCont()) {
            return false;
        }

        return true;
    }

    /**
     * checkInsured
     *
     * @return boolean
     */
    private boolean checkInsured() {
        System.out.println("系统同录入的投保人客户号码，开始验证客户信息");
        System.out.println("录入_" + "被保人" + "姓名 : " +
                           StrTool.unicodeToGBK(mLCInsuredSchema.getName()));
        System.out.println("录入_" + "被保人" + "性别 : " +
                           StrTool.unicodeToGBK(mLCInsuredSchema.getSex()));
        System.out.println("录入_" + "被保人" + "生日 :　" +
                           StrTool.unicodeToGBK(mLCInsuredSchema.getBirthday()));
        System.out.println("录入_" + "被保人" + "证件类型 :　" +
                           StrTool.unicodeToGBK(mLCInsuredSchema.getIDType()));
        System.out.println("录入_" + "被保人" + "证件号码 :　" +
                           StrTool.unicodeToGBK(mLCInsuredSchema.getIDNo()));

        LDPersonDB tLDPersonDB = new LDPersonDB();
        if (!StrTool.unicodeToGBK(StrTool.cTrim(this.mLCInsuredSchema.
                                                getInsuredNo())).equals("")) {
            tLDPersonDB.setCustomerNo(mLCInsuredSchema.getInsuredNo());
            if (!tLDPersonDB.getInfo()) {
                // @@错误处理
                buildError("checkInsured", "录入被保人客户号，但是没有找到此客户号对应的客户信息！！");
                return false;
            } else {
                this.mInsuredNo = mLCInsuredSchema.getInsuredNo();
                if (!StrTool.unicodeToGBK(StrTool.cTrim(this.mLCInsuredSchema.
                        getName())).
                    equals(StrTool.cTrim(tLDPersonDB.getName()))) {
                    buildError("checkInsured",
                               "被保客户姓名与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
                    return false;
                }
                if (!StrTool.cTrim(this.mLCInsuredSchema.getBirthday()).
                    equals(StrTool.cTrim(tLDPersonDB.getBirthday()))) {
                    buildError("checkInsured",
                               "被保客户生日与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
                    return false;
                }
                /** 性别 */
                if (!StrTool.cTrim(this.mLCInsuredSchema.getSex()).
                    equals(StrTool.cTrim(tLDPersonDB.getSex()))) {
                    buildError("checkInsured",
                               "被保客户性别与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
                    return false;
                }
                /** 证件类型 */
                if (!StrTool.cTrim(this.mLCInsuredSchema.getIDType()).
                    equals(StrTool.cTrim(tLDPersonDB.getIDType()))) {
                    buildError("checkInsured",
                               "被保客户证件类型与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
                    return false;
                }

                /** 证件号码 */
                if (!StrTool.cTrim(this.mLCInsuredSchema.getIDNo()).
                    equals(StrTool.cTrim(tLDPersonDB.getIDNo()))) {
                    buildError("checkInsured",
                               "被保客户证件号码与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
                    return false;
                }
                insuredflag = "5";
            }
        } else {
            /** 如果没有录入客户号码，根据基本信息校验是否是老客户 */
            if (!queryInsuerd()) {
                return false;
            }
        }
        System.out.println("校验完成，无论是否应该生成客户都应该把投保人号码存入" + this.mAppntNo);
        mLCInsuredSchema.setAppntNo(this.mAppntNo);
        mLCInsuredSchema.setOperator(this.mGlobalInput.Operator);
        PubFun.fillDefaultField(mLCInsuredSchema);
        return true;
    }

    /**
     * queryInsuerd
     *
     * @return boolean
     */
    private boolean queryInsuerd() {
        LDPersonDB tLDPersonDB = new LDPersonDB();
        if (!StrTool.cTrim(this.mLCInsuredSchema.getName()).equals(
                "")) {
            tLDPersonDB.setName(this.mLCInsuredSchema.getName());
        } else {
            buildError("queryInsuerd", "没有录入投保人姓名！");
            return false;
        }
        if (!StrTool.cTrim(this.mLCInsuredSchema.getSex()).equals("")) {
            tLDPersonDB.setSex(this.mLCInsuredSchema.getSex());
        } else {
            buildError("queryInsuerd", "没有录入投保人性别！");
            return false;
        }
        if (!StrTool.cTrim(this.mLCInsuredSchema.getBirthday()).
            equals("")) {
            tLDPersonDB.setBirthday(this.mLCInsuredSchema.getBirthday());
        } else {
            buildError("queryInsuerd", "没有录入投保人生日！");
            return false;
        }
        if (!StrTool.cTrim(this.mLCInsuredSchema.getIDType()).equals("")) {
            tLDPersonDB.setIDType(this.mLCInsuredSchema.getIDType());
        } else {
            buildError("queryInsuerd", "没有录入投保人证件类型！！");
            return false;
        }
        if (!StrTool.cTrim(this.mLCInsuredSchema.getIDNo()).equals("")) {
            tLDPersonDB.setIDNo(this.mLCInsuredSchema.getIDNo());
        } else if (!StrTool.cTrim(this.mLCInsuredSchema.getIDType()).
                   equals("4")) {
            buildError("queryInsuerd", "投保人证件号码录入错误！！");
            return false;
        }
        LDPersonSet tLDPersonSet = tLDPersonDB.query();
        if (tLDPersonSet.size() == 1) {
            /** 找到为一个客户信息 */
            /** 取出当前客户的客户号码 */
            this.mLCInsuredSchema.setInsuredNo(tLDPersonSet.get(1).
                                               getCustomerNo());
            this.mInsuredNo = tLDPersonSet.get(1).
                              getCustomerNo();
            insuredflag = "6";

        } else if (tLDPersonSet.size() == 0) {
            this.needCreatInsured = true;
            insuredflag = "6";
        } else {
            buildError("queryInsuerd", "不会这么巧吧,存在相近客户,请检查客户信息！");
            return false;
        }
        return true;
    }

    /**
     * dealAppntToInsured
     *
     * @return boolean
     */
    private boolean dealAppntToInsured() {
        System.out.println("投保人和被保人是本人处理");
        mLCInsuredSchema.setInsuredNo(mLCAppntSchema.getAppntNo());
        mLCInsuredSchema.setAccName(mLCAppntSchema.getAccName());
        mLCInsuredSchema.setAddressNo(mLCAppntSchema.getAddressNo());
        mLCInsuredSchema.setAppntNo(mLCAppntSchema.getAppntNo());
//        mLCInsuredSchema.setAvoirdupois(mLCAppntSchema);
//        mLCInsuredSchema.setAvoirdupois();
//        mLCInsuredSchema.setBankAccNo(mLCAppntSchema);
//        mLCInsuredSchema.setBankCode();
        mLCInsuredSchema.setBirthday(mLCAppntSchema.getAppntBirthday());
//        mLCInsuredSchema.setBMI();
        mLCInsuredSchema.setEnglishName(mLCAppntSchema.getEnglishName());
        mLCInsuredSchema.setName(mLCAppntSchema.getAppntName());
        mLCInsuredSchema.setSex(mLCAppntSchema.getAppntSex());
        mLCInsuredSchema.setIDType(mLCAppntSchema.getIDType());
        mLCInsuredSchema.setIDNo(mLCAppntSchema.getIDNo());
        mLCInsuredSchema.setOperator(mGlobalInput.Operator);
        mLCInsuredSchema.setManageCom(mLCContSchema.getManageCom());
        mLCInsuredSchema.setExecuteCom(mGlobalInput.ManageCom);
        PubFun.fillDefaultField(mLCInsuredSchema);
        return true;
    }

    /**
     * dealAppntToInsured
     *
     * @return boolean
     */
    private boolean dealInsuredToAppnt() {
        System.out.println("投保人和被保人是本人处理");
        mLCAppntSchema.setAppntNo(mLCInsuredSchema.getInsuredNo());
        mLCAppntSchema.setAccName(mLCInsuredSchema.getAccName());
        mLCAppntSchema.setAddressNo(mLCInsuredSchema.getAddressNo());
//        mLCAppntSchema.setAppntNo(mLCInsuredSchema.getInsuredNo());
        mLCAppntSchema.setAppntBirthday(mLCInsuredSchema.getBirthday());
        mLCAppntSchema.setEnglishName(mLCInsuredSchema.getEnglishName());
        mLCAppntSchema.setAppntName(mLCInsuredSchema.getName());
        mLCAppntSchema.setIDType(mLCInsuredSchema.getIDType());
        mLCAppntSchema.setIDNo(mLCInsuredSchema.getIDNo());
        mLCAppntSchema.setAppntSex(mLCInsuredSchema.getSex());
        mLCAppntSchema.setOperator(mGlobalInput.Operator);
        PubFun.fillDefaultField(mLCAppntSchema);
        return true;
    }

    /**
     * dealAppntToCont
     *
     * @return boolean
     */
    private boolean dealAppntToCont() {
        String Sql1 = "update lccont set AppntBirthday='" +
                      this.mLCAppntSchema.getAppntBirthday() +
                      "',AppntIDNo='" + this.mLCAppntSchema.getIDNo() +
                      "',AppntIDType='" + this.mLCAppntSchema.getIDType() +
                      "',AppntName='" + this.mLCAppntSchema.getAppntName() +
                      "',AppntNo='" + this.mLCAppntSchema.getAppntNo() +
                      "',AppntSex='" + this.mLCAppntSchema.getAppntSex() +
                      "' where prtno='" + mLCContSchema.getPrtNo() + "' ";
        map.put(Sql1, "UPDATE");
        String Sql2 = "update lcpol set AppntNo='" +
                      this.mLCAppntSchema.getAppntNo() + "',AppntName='" +
                      this.mLCAppntSchema.getAppntName() + "' where prtno='" +
                      mLCContSchema.getPrtNo() + "' ";
        map.put(Sql2, "UPDATE");
        return true;
    }

    /**
     * queryAppnt
     *
     * @return boolean
     */
    private boolean queryAppnt() {
        LDPersonDB tLDPersonDB = new LDPersonDB();
        if (!StrTool.cTrim(this.mLCAppntSchema.getAppntName()).equals(
                "")) {
            tLDPersonDB.setName(this.mLCAppntSchema.getAppntName());
        } else {
            CError tError = new CError();
            tError.moduleName = "BriefSingleContModifyBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有录入投保人姓名！";
            System.out.println("程序第138行出错，"
                               + "请检查BriefSingleContInputBL中的"
                               + "dealData方法！" + tError.errorMessage);
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!StrTool.cTrim(this.mLCAppntSchema.getAppntSex()).equals(
                "")) {
            tLDPersonDB.setSex(this.mLCAppntSchema.getAppntSex());
        } else {
            CError tError = new CError();
            tError.moduleName = "BriefSingleContModifyBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有录入投保人性别！";
            System.out.println("程序第152行出错，"
                               + "请检查BriefSingleContInputBL中的"
                               + "dealData方法！" + tError.errorMessage);
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!StrTool.cTrim(this.mLCAppntSchema.getAppntBirthday()).
            equals("")) {
            tLDPersonDB.setBirthday(this.mLCAppntSchema.
                                    getAppntBirthday());
        } else {
            CError tError = new CError();
            tError.moduleName = "BriefSingleContModifyBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有录入投保人生日！";
            System.out.println("程序第167行出错，"
                               + "请检查BriefSingleContInputBL中的"
                               + "dealData方法！" + tError.errorMessage);
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!StrTool.cTrim(this.mLCAppntSchema.getIDType()).equals("")) {
            tLDPersonDB.setIDType(this.mLCAppntSchema.getIDType());
        } else {
            CError tError = new CError();
            tError.moduleName = "BriefSingleContModifyBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有录入投保人证件类型！";
            System.out.println("程序第181行出错，"
                               + "请检查BriefSingleContInputBL中的"
                               + "dealData方法！" + tError.errorMessage);
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!StrTool.cTrim(this.mLCAppntSchema.getIDNo()).equals("")) {
            tLDPersonDB.setIDNo(this.mLCAppntSchema.getIDNo());
        } else if (!StrTool.cTrim(this.mLCAppntSchema.getIDType()).
                   equals("4")) {
            CError tError = new CError();
            tError.moduleName = "BriefSingleContModifyBL";
            tError.functionName = "dealData";
            tError.errorMessage = "投保人证件号码录入错误！";
            System.out.println("程序第194行出错，"
                               + "请检查BriefSingleContInputBL中的"
                               + "dealData方法！" + tError.errorMessage);
            this.mErrors.addOneError(tError);
            return false;
        }
        LDPersonSet tLDPersonSet = tLDPersonDB.query();
        if (tLDPersonSet.size() == 1) {
            /** 找到为一个客户信息 */
            /** 取出当前客户的客户号码 */
            this.mLCAppntSchema.setAppntNo(tLDPersonSet.get(1).
                                           getCustomerNo());
            this.mAppntNo = tLDPersonSet.get(1).
                            getCustomerNo();
            mappntflag = "2";
        } else if (tLDPersonSet.size() == 0) {
            this.needCreatAppnt = true;
            mappntflag = "2";
        } else {
            CError tError = new CError();
            tError.moduleName = "BriefSingleContModifyBL";
            tError.functionName = "dealData";
            tError.errorMessage = "不会这么巧吧,存在相近客户,请检查客户信息！";
            System.out.println("程序第214行出错，"
                               + "请检查BriefSingleContInputBL中的"
                               + "dealData方法！" + tError.errorMessage);
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * checkAppnt
     *
     * @return boolean
     * @param tLDPersonSchema LDPersonSchema
     */
    private boolean checkAppnt(LDPersonSchema tLDPersonSchema) {
        System.out.println("系统同录入的投保人客户号码，开始验证客户信息");
        System.out.println("录入_投保人姓名 : " +
                           StrTool.unicodeToGBK(mLCAppntSchema.getAppntName()));
        System.out.println("录入_投保人性别 : " +
                           StrTool.unicodeToGBK(mLCAppntSchema.getAppntSex()));
        System.out.println("录入_投保人生日 :　" +
                           StrTool.unicodeToGBK(mLCAppntSchema.getAppntBirthday()));
        System.out.println("录入_投保人证件类型 :　" +
                           StrTool.unicodeToGBK(mLCAppntSchema.getIDType()));
        System.out.println("录入_投保人证件号码 :　" +
                           StrTool.unicodeToGBK(mLCAppntSchema.getIDNo()));
        if (!StrTool.unicodeToGBK(StrTool.cTrim(this.mLCAppntSchema.
                                                getAppntName())).
            equals(StrTool.cTrim(tLDPersonSchema.getName()))) {
            System.out.println("系统中的客户姓名为 : " +
                               StrTool.cTrim(tLDPersonSchema.getName()));
            buildError("checkAppnt",
                       "投保客户姓名与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
            return false;
        }
        if (!StrTool.cTrim(this.mLCAppntSchema.getAppntBirthday()).
            equals(StrTool.cTrim(tLDPersonSchema.getBirthday()))) {
            buildError("checkAppnt",
                       "投保客户生日与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
            return false;
        }
        /** 性别 */
        if (!StrTool.cTrim(this.mLCAppntSchema.getAppntSex()).
            equals(StrTool.cTrim(tLDPersonSchema.getSex()))) {
            buildError("checkAppnt",
                       "投保客户性别与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
            return false;
        }
        /** 证件类型 */
        if (!StrTool.cTrim(this.mLCAppntSchema.getIDType()).
            equals(StrTool.cTrim(tLDPersonSchema.getIDType()))) {
            buildError("checkAppnt",
                       "投保客户证件类型与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
            return false;
        }

        /** 证件号码 */
        if (!StrTool.cTrim(this.mLCAppntSchema.getIDNo()).
            equals(StrTool.cTrim(tLDPersonSchema.getIDNo()))) {
            buildError("checkAppnt",
                       "投保客户证件号码与系统中的客户信息不同，请确认是否录入错误？如果确认客户信息没有错误，请删除客户号码从新得到客户号！！");
            return false;
        }
        return true;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData() {
        /** 开始校验前台传入的数据 */
//        if (mLCContSchema == null || mLCAppntSchema == null ||
//            mLCInsuredSchema == null ||
//            mLCBnfSchema == null || mLCAddressSchema == null || mLCPolSet == null ||
//            mLCDutySet == null || mGlobalInput == null) {
//            CError tError = new CError();
//            tError.moduleName = "BriefSingleContModifyBL";
//            tError.functionName = "checkData";
//            tError.errorMessage = "前台传入数据不完整！";
//            System.out.println("程序第119行出错，"
//                               + "请检查BriefSingleContInputBL中的"
//                               + "checkData方法！" + tError.errorMessage);
//            this.mErrors.addOneError(tError);
//            return false;
//        }
        System.out.println("@@完成程序checkData第130行");
        /** 被保人与投保人是 */
       /*
        for (int i = 1; i <= mLCNationSet.size(); i++) {
            System.out.println("国家代码　：");
            System.out.println(mLCNationSet.get(i).getNationNo());
        }
      */
        /** 校验险种信息 */
        if (this.mLCPolSet == null || this.mLCPolSet.size() <= 0) {
            buildError("checkData", "录入险种信息不完整，请确认是否选择险种信息！");
            return false;
        }
        return true;
    }

    /**
     * riskAutoCheck
     *
     * @return boolean
     * @param tLCPolSchema LCPolSchema
     */
    private boolean riskAutoCheck(LCPolSchema tLCPolSchema) {
        Calculator mCalculator = new Calculator();
        LMUWDB tLMUWDB = new LMUWDB();
        tLMUWDB.setRiskCode(tLCPolSchema.getRiskCode());
        LMUWSet tLMUWSet = tLMUWDB.query();
        if (tLMUWSet.size() <= 0) {
            /** 没有自核信息,不需要教研,直接返回 */
            return true;
        }
        for (int m = 1; m <= tLCPolSchema.getFieldCount(); m++) {
            mCalculator.addBasicFactor(tLCPolSchema.getFieldName(m),
                                       tLCPolSchema.getV(m));
        }
//        for (int m = 1; m <= this.mLCContSchema.getFieldCount(); m++) {
//            mCalculator.addBasicFactor("LCCont_" +
//                                       mLCContSchema.getFieldName(m),
//                                       mLCContSchema.getV(m));
//        }
        VData errorVData = new VData();
        for (int i = 1; i <= tLMUWSet.size(); i++) {
            mCalculator.setCalCode(tLMUWSet.get(i).getCalCode());
            String tStr = mCalculator.calculate();
            if (!StrTool.cTrim(tStr).equals("")) {
                /** 解析报错要素 */
                errorVData.add(parseUWResultFactor(tLMUWSet.get(i).getRemark()));
            }
        }
        if (errorVData.size() > 0) {
            String errorMessage = "";
            for (int i = 0; i < errorVData.size(); i++) {
                errorMessage += (String) errorVData.get(i);
                errorMessage += "<br>";
            }
            buildError("riskAutoCheck", errorMessage);
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @return boolean
     */
    private boolean getInputData() {
        if (this.mInputData == null) {
            CError tError = new CError();
            tError.moduleName = "BriefSingleContModifyBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的封装对象为空！";
            System.out.println("程序第97行出错，"
                               + "请检查BriefSingleContInputBL中的"
                               + "getInputData方法！" + tError.errorMessage);
            this.mErrors.addOneError(tError);
            return false;
        }
        if (this.mOperate == null) {
            CError tError = new CError();
            tError.moduleName = "BriefSingleContModifyBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有传入操作符！";
            System.out.println("程序第169行出错，"
                               + "请检查BriefSingleContInputBL中的"
                               + "getInputData方法！" + tError.errorMessage);
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCContSchema = (LCContSchema) mInputData.getObjectByObjectName(
                "LCContSchema", 0);
        mLCAppntSchema = (LCAppntSchema) mInputData.getObjectByObjectName(
                "LCAppntSchema", 0);
        mLCInsuredSchema = (LCInsuredSchema) mInputData.
                           getObjectByObjectName(
                                   "LCInsuredSchema", 0);
        System.out.println("insured_RelationToAppnt"+mLCInsuredSchema.getRelationToAppnt());
        //       mLCBnfSchema = (LCBnfSchema) mInputData.getObjectByObjectName(
        //               "LCBnfSchema", 0);
//        mLDPersonSet = (LDPersonSet) mInputData.getObjectByObjectName(
//                "LDPersonSet", 0);
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
            this.mAppntAddressSchema=null;
            this.mInsuredAddressSchema =null;
            appntchangecontnoFlag = "true";
            insuredchangecontnoFlag = "true";
            mLCNationSet=null;
/*
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mTransferData == null) {
            buildError("getInputData", "传入的地址信息为空！");
            return false;
        }
        this.mAppntAddressSchema = (LCAddressSchema) mTransferData.
                                   getValueByName("AppntAddress");
        this.mInsuredAddressSchema = (LCAddressSchema) mTransferData.
                                     getValueByName("InsuredAddress");
        appntchangecontnoFlag=(String)mTransferData.
                                   getValueByName("appntchangecontnoFlag");
        insuredchangecontnoFlag=(String)mTransferData.
                                   getValueByName("insuredchangecontnoFlag");
 */
        /** 获取前台传入的国家代码 */
/*        this.mLCNationSet = (LCNationSet) mInputData.getObjectByObjectName(
                "LCNationSet", 0);
        for (int i = 1; i <= mLCNationSet.size(); i++) {
            System.out.println("国家代码　：");
            System.out.println(mLCNationSet.get(i).getNationNo());
        }
 */
//        this.mLCBnfSchema = (LCBnfSchema) mInputData.getObjectByObjectName(
//               "LCBnfSchema", 0);
        LCPolDB mLCPolDB = new LCPolDB();
        mLCPolDB.setPrtNo(mLCContSchema.getPrtNo());
        mLCPolSet = mLCPolDB.query();
        //防止换号，预先保留旧号码
        moldAppntNo = mLCPolSet.get(1).getAppntNo();
        moldInsuredNo = mLCPolSet.get(1).getInsuredNo();
        System.out.println("moldInsuredNo"+moldInsuredNo);
        return true;

    }

    /**
     * getResult
     *
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "BriefSingleContModifyBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.err.println("程序报错：" + cError.errorMessage);
    }

    /**
     * 调试函数
     * @param args String[]
     */
    public static void main(String[] args) {
        String mContNo = "00159272901";
        String mPrtNo = "13000001055";
        String mManageCom = "86110000";
        String mAgentCode = "1101000063";

        LCContSchema tLCContSchema = new LCContSchema();
        LCAppntSchema tLCAppntSchema = new LCAppntSchema();
        GlobalInput tGlobalInput = new GlobalInput();
        LCPolSet tLCPolSet = new LCPolSet();
        LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
        LCPolSchema tLCPolSchema;
        tLCContSchema.setPrtNo(mPrtNo);
        tLCContSchema.setContNo(mContNo);
        tLCContSchema.setManageCom(mManageCom);
        tLCContSchema.setAgentCode(mAgentCode);
        tLCContSchema.setPayMode("1");
        tLCContSchema.setAppFlag("1");
        tLCContSchema.setCValiDate("2006-12-22");
        tLCContSchema.setCInValiDate("2007-12-22");
        tLCContSchema.setDegreeType("0");
        tLCAppntSchema.setGrpContNo("00000000000000000000");
        tLCAppntSchema.setPrtNo(mPrtNo);
        tLCAppntSchema.setAppntNo("001592729");
        tLCAppntSchema.setContNo(mContNo);
        tLCAppntSchema.setAppntName("孙群义");
        tLCAppntSchema.setAppntSex("0");
        tLCAppntSchema.setAppntBirthday("1953-3-19");
        tLCAppntSchema.setIDType("0");
        tLCAppntSchema.setIDNo("11010219530319003X");
        tLCAppntSchema.setEnglishName("");
        /** 处理险种信息 */
        tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setRiskCode("5103");
        tLCPolSchema.setMult(8);
        tLCPolSet.add(tLCPolSchema);
        /*        tLCPolSchema = new LCPolSchema();
                tLCPolSchema.setRiskCode("5102");
                tLCPolSchema.setMult(1);
                tLCPolSet.add(tLCPolSchema);
         */
        /*tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setRiskCode("5102");
        tLCPolSchema.setAmnt(100000);
*/
        tLCPolSet.add(tLCPolSchema);
        tLCInsuredSchema.setGrpContNo("00000000000000000000");
         tLCInsuredSchema.setExecuteCom(mManageCom);
        tLCInsuredSchema.setPrtNo("13000009565");
        tLCInsuredSchema.setContNo(mContNo);
        tLCInsuredSchema.setInsuredNo("");
        tLCInsuredSchema.setRelationToAppnt("29");
        tLCInsuredSchema.setName("张姗姗");
        tLCInsuredSchema.setSex("1");
        tLCInsuredSchema.setBirthday("1976-2-19");
        tLCInsuredSchema.setIDType("0");
        tLCInsuredSchema.setIDNo("110105197602197328");
        tLCInsuredSchema.setEnglishName("");
/*
        LCAddressSchema tInsuredAddressSchema = new LCAddressSchema();
        tInsuredAddressSchema.setCustomerNo("");
        tInsuredAddressSchema.setAddressNo("");
        tInsuredAddressSchema.setPostalAddress("");
        tInsuredAddressSchema.setZipCode("");
        tInsuredAddressSchema.setHomePhone("");
        tInsuredAddressSchema.setMobile("");
        tInsuredAddressSchema.setCompanyPhone("");
        LCAddressSchema tAppntAddressSchema = new LCAddressSchema();
        tAppntAddressSchema.setCustomerNo("");
        tAppntAddressSchema.setAddressNo("");
        tAppntAddressSchema.setPostalAddress("");
        tAppntAddressSchema.setZipCode("");
        tAppntAddressSchema.setHomePhone("");
        tAppntAddressSchema.setMobile("");
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("AppntAddress", tAppntAddressSchema);
        tTransferData.setNameAndValue("InsuredAddress", tInsuredAddressSchema);
 */
        LCNationDB tLCNationDB = new LCNationDB();
        LCNationSet tLCNationSet = new LCNationSet();
        /*
        tLCNationDB.setGrpContNo("00000000000000000000");
        tLCNationDB.setContNo("00002918008");
        tLCNationSet = tLCNationDB.query();
*/
        tGlobalInput.ManageCom = mManageCom;
        tGlobalInput.Operator = "CP1106";
        tGlobalInput.ComCode = mManageCom;
        VData tVData = new VData();
        tVData.add(tGlobalInput);
        tVData.add(tLCContSchema);
        tVData.add(tLCAppntSchema);
        tVData.add(tLCPolSet);
//        tVData.add(tTransferData);
        tVData.add(tLCInsuredSchema);
//        tVData.add(tLCNationSet);
        BriefSingleContModifyBL tBriefSingleContModifyBL = new
                BriefSingleContModifyBL();
        if (!tBriefSingleContModifyBL.submitData(tVData, "UPDATE||MAIN")) {
            System.out.println("错误" +
                               tBriefSingleContModifyBL.mErrors.getFirstError());
        }
    }

    private String parseUWResultFactor(String tSql) {
        PubCalculator tPubCalculator = new PubCalculator();
        tPubCalculator.addBasicFactor("ContNo", this.mContNo);
        tPubCalculator.addBasicFactor("AppntNo", this.mAppntNo);
        tPubCalculator.addBasicFactor("InsuredNo", this.mInsuredNo);
        tPubCalculator.addBasicFactor("PolNo", this.mPolNo);
        String tStr = "";
        String tRemark = tSql;
        String tStr1 = "";
        try {
            while (true) {
                tStr = PubFun.getStr(tSql, 2, "$");
                if (tStr.equals("")) {
                    break;
                }
                tPubCalculator.setCalSql(tStr);
                tStr1 = "$" + tStr.trim() + "$";
                //替换变量

                tSql = StrTool.replaceEx(tSql, tStr1, tPubCalculator.calculate());
            }
        } catch (Exception ex) {
            // @@错误处理
            buildError("parseUWResultFactor", "解析要素失败！");
            return null;
        }
        if (tSql.trim().equals("")) {
            return tRemark;
        }
        return tSql;

    }

    private boolean checkLCAppnt() {
        System.out.println("处理投保人校验！");
        // 投保人-个人客户--如果是集体下个人，该投保人为空,所以个人时校验个人投保人
        if (mLCAppntSchema != null) {
            System.out.println("处理投保人校验1！");
            if (mLCAppntSchema.getAppntName() != null) { //去空格
                mLCAppntSchema.setAppntName(mLCAppntSchema.getAppntName().
                                            trim());
            }
            if (mLCAppntSchema.getAppntSex() != null) {
                mLCAppntSchema.setAppntSex(mLCAppntSchema.getAppntSex().
                                           trim());
            }
            if (mLCAppntSchema.getIDNo() != null) {
                mLCAppntSchema.setIDNo(mLCAppntSchema.getIDNo().trim());
            }
            if (mLCAppntSchema.getIDType() != null) {
                mLCAppntSchema.setIDType(mLCAppntSchema.getIDType().trim());
            }
            if (mLCAppntSchema.getAppntBirthday() != null) {
                mLCAppntSchema.setAppntBirthday(mLCAppntSchema.
                                                getAppntBirthday().
                                                trim());
            }

            if (mLCAppntSchema.getAppntNo() != null) {
                System.out.println("处理投保人校验，如果有客户号！");
                //如果有客户号
                if (!mLCAppntSchema.getAppntNo().equals("")) {
                    LDPersonDB tLDPersonDB = new LDPersonDB();

                    tLDPersonDB.setCustomerNo(mLCAppntSchema.getAppntNo());
                    if (tLDPersonDB.getInfo() == false) {
                        CError tError = new CError();
                        tError.moduleName = "ProposalBL";
                        tError.functionName = "checkPerson";
                        tError.errorMessage = "数据库查询失败!";
                        this.mErrors.addOneError(tError);

                        return false;
                    }
                    if (mLCAppntSchema.getAppntName() != null) {
                        String Name = StrTool.GBKToUnicode(tLDPersonDB.
                                getName()
                                .trim());
                        String NewName = StrTool.GBKToUnicode(
                                mLCAppntSchema.
                                getAppntName()
                                .trim());

                        if (!Name.equals(NewName)) {
                            CError tError = new CError();
                            tError.moduleName = "ProposalBL";
                            tError.functionName = "checkPerson";
                            tError.errorMessage = "您输入的投保人客户号对应在数据库中的客户姓名("
                                                  + Name + ")与您录入的客户姓名("
                                                  + NewName + ")不匹配！";
                            this.mErrors.addOneError(tError);

                            return false;
                        }
                    }
                    if (mLCAppntSchema.getAppntSex() != null) {
                        if (!tLDPersonDB.getSex().equals(mLCAppntSchema
                                .getAppntSex())) {
                            CError tError = new CError();
                            tError.moduleName = "ProposalBL";
                            tError.functionName = "checkPerson";
                            tError.errorMessage = "您输入的投保人客户号对应在数据库中的客户性别("
                                                  + tLDPersonDB.getSex()
                                                  + ")与您录入的客户性别("
                                                  + mLCAppntSchema.getAppntSex()
                                                  + ")不匹配！";
                            this.mErrors.addOneError(tError);

                            return false;
                        }
                    }
                    if (mLCAppntSchema.getAppntBirthday() != null) {
                        if (!tLDPersonDB.getBirthday().equals(
                                mLCAppntSchema
                                .getAppntBirthday())) {
                            System.out.println("jiaoyan");
                            CError tError = new CError();
                            tError.moduleName = "ProposalBL";
                            tError.functionName = "checkPerson";
                            tError.errorMessage = "您输入的投保人客户号对应在数据库中的客户生日("
                                                  + tLDPersonDB.getBirthday()
                                                  + ")与您录入的客户生日("
                                                  +
                                                  mLCAppntSchema.getAppntBirthday()
                                                  + ")不匹配！";
                            this.mErrors.addOneError(tError);
                            System.out.println("jiaoyan1");
                            return false;
                        }
                    }
                    if (mLCAppntSchema.getIDNo() != null
                            //&&!StrTool.cTrim(mLCAppntSchema.getIDType()).equals("4")
                            ) {
                        if (!StrTool.cTrim(tLDPersonDB.getIDNo()).equals(
                                StrTool.cTrim(mLCAppntSchema
                                              .getIDNo()))) {
                            CError tError = new CError();
                            tError.moduleName = "ProposalBL";
                            tError.functionName = "checkPerson";
                            tError.errorMessage =
                                    "您输入的投保人客户号对应在数据库中的客户证件号码("
                                    + tLDPersonDB.getIDNo()
                                    + ")与您录入的客户证件号码("
                                    + mLCAppntSchema.getIDNo()
                                    + ")不匹配！";
                            this.mErrors.addOneError(tError);

                            return false;
                        }
                    }
                    //校验证件类型
                    if (mLCAppntSchema.getIDType() != null) {
                        if (!StrTool.cTrim(tLDPersonDB.getIDType()).equals(
                                StrTool.cTrim(mLCAppntSchema
                                              .getIDType()))) {
                            CError tError = new CError();
                            tError.moduleName = "ProposalBL";
                            tError.functionName = "checkPerson";
                            tError.errorMessage =
                                    "您输入的投保人客户号对应在数据库中的客户证件类型("
                                    + tLDPersonDB.getIDType()
                                    + ")与您录入的客户证件类型("
                                    + mLCAppntSchema.getIDType()
                                    + ")不匹配！";
                            this.mErrors.addOneError(tError);

                            return false;
                        }
                    }

                } else { //如果没有客户号,查找客户信息表是否有相同名字，性别，出生年月，身份证号的纪录，若有，取客户号
                    if ((mLCAppntSchema.getAppntName() != null)
                        && (mLCAppntSchema.getAppntSex() != null)
                        && (mLCAppntSchema.getIDNo() != null)) {
                        LDPersonDB tLDPersonDB = new LDPersonDB();
                        tLDPersonDB.setName(mLCAppntSchema.getAppntName());
                        tLDPersonDB.setSex(mLCAppntSchema.getAppntSex());
                        tLDPersonDB.setBirthday(mLCAppntSchema.
                                                getAppntBirthday());
                        tLDPersonDB.setIDType(mLCAppntSchema.getIDType());
                        tLDPersonDB.setIDNo(mLCAppntSchema.getIDNo());

                        LDPersonSet tLDPersonSet = tLDPersonDB.query();
                        if (tLDPersonSet != null) {
                            if (tLDPersonSet.size() > 0) {
                                mLCAppntSchema.setAppntNo(tLDPersonSet.get(
                                        1)
                                        .getCustomerNo());
                            }
                        }
                    }
                }
            }
        }

        return true;
    }

    /**
     * 主要要处理LCCont LCPol LCGet LCDuty
     * 中的生效失效日期
     *
     * @return boolean
     */
    private boolean dealCvailDate() {
        System.out.println("AppFlag " + mLCContSchema.getAppFlag());
        if (StrTool.cTrim(mLCContSchema.getAppFlag()).equals("1") ||
            StrTool.cTrim(mLCContSchema.getAppFlag()).equals("9")) {
            //首先校验生效日期是否发生改变
            LCContDB tLCcontDB = new LCContDB();
            LCContSet tLCContSet = new LCContSet();
            tLCcontDB.setContNo(mLCContSchema.getContNo());
            tLCContSet = tLCcontDB.query();
            System.out.println("baba" + tLCContSet.get(1).getCValiDate());
            System.out.println("baba1" + mLCContSchema.getCValiDate());
            if (StrTool.cTrim(tLCContSet.get(1).getCValiDate()).equals(
                    StrTool.cTrim(mLCContSchema.getCValiDate()))) {
                //没有发生变化
                System.out.println("dsddddddddddddddddd");
            } else {
                System.out.println("ffffffffffffffffffff");
                //发生变化,需要更新生效日期,失效日期
                String CVailDate = this.mLCContSchema.getCValiDate();
                String CInVailDate = this.mLCContSchema.getCInValiDate();
                GregorianCalendar tInValiDate = new GregorianCalendar();
                Date tdate = (new FDate()).getDate(CInVailDate);
                tInValiDate.setTime(tdate);
                tInValiDate.add(tInValiDate.DATE, 1);
                System.out.println("sb"+(new FDate()).getString(tInValiDate.getTime()));
                StringBuffer sql = new StringBuffer();
                sql = new StringBuffer();
                sql.append("update LCPol set CValiDate='");
                sql.append(CVailDate);
                sql.append("',Enddate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("',getstartdate='");
                sql.append(CVailDate);
                sql.append("',PayToDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("',PayEndDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("' where prtno='");
                sql.append(this.mLCContSchema.getPrtNo());
                sql.append("'");
                this.map.put(sql.toString(), "UPDATE");

                sql = new StringBuffer();
                sql.append("update LCCont set CValiDate='");
                sql.append(CVailDate);
                sql.append("',CInValiDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("' where prtno='");
                sql.append(this.mLCContSchema.getPrtNo());
                sql.append("'");
                this.map.put(sql.toString(), "UPDATE");

                sql = new StringBuffer();
                sql.append("update LCGet set GetStartDate='");
                sql.append(CVailDate);
                sql.append("',GetEndDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("',GetToDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("' where contno='");
                sql.append(this.mLCContSchema.getContNo());
                sql.append("'");
                this.map.put(sql.toString(), "UPDATE");

                sql = new StringBuffer();
                sql.append("update LCprem set ");
                sql.append("PayToDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("',PayEndDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("' where contno='");
                sql.append(this.mLCContSchema.getContNo());
                sql.append("'");
                this.map.put(sql.toString(), "UPDATE");

                sql = new StringBuffer();
                sql.append("update LCDuty set PayToDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("',PayEndDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("',EndDate='");
                sql.append((new FDate()).getString(tInValiDate.getTime()));
                sql.append("',getstartdate='");
                sql.append(CVailDate);
                sql.append(
                        "' where contno in (select contno from lccont where contno='");
                sql.append(this.mLCContSchema.getContNo());
                sql.append("')");
                this.map.put(sql.toString(), "UPDATE");
            }
        }
        return true;
    }
}
