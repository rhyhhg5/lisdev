package com.sinosoft.lis.brieftb;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

/**
 *
 * <p>Title: 简易保单客户接口</p>
 *
 * <p>Description: 不做操作,直接传入BL操作</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author YangMing
 * @version 1.0
 */
public class BriefGroupContInputUI {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    public BriefGroupContInputUI() {
    }

    public static void main(String[] args) {
        BriefGroupContInputUI briefgroupcontinput = new BriefGroupContInputUI();
    }

    public boolean submitData(VData nInputData, String cOperate) {
        BriefGroupContInputBL tBriefGroupContInputBL = new BriefGroupContInputBL();
        if (!tBriefGroupContInputBL.submitData(nInputData, cOperate)) {
            this.mErrors.copyAllErrors(tBriefGroupContInputBL.mErrors);
            return false;
        } else {
            mResult = tBriefGroupContInputBL.getResult();
        }
        return true;
    }

    /**
     * 操作结果
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }
}
