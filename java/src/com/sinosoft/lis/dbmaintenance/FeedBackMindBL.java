package com.sinosoft.lis.dbmaintenance;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.feedbackmindSet;

import com.sinosoft.utility.*;
public class FeedBackMindBL
{
    /**错误的容器，错程序报错，可通过该变量获取出错信息*/
    public CErrors mErrors = new CErrors();
    private String mTransact = null;
    private GlobalInput mGI = null;
    //private  feedbackmindSchema mfeedbackmindSchema1 = null;
    //private  feedbackmindSchema mfeedbackmindSchema2 = null;
   // private  feedbackmindSchema mfeedbackmindSchema3 = null;
   // private  feedbackmindSchema mfeedbackmindSchema4 = null;
    //private  feedbackmindSchema mfeedbackmindSchema5 = null;
   // private  feedbackmindSchema mfeedbackmindSchema6 = null;
   // private  feedbackmindSchema mfeedbackmindSchema7 = null;
    
    private  feedbackmindSet mfeedbackmindSet = null;
    
    private MMap map = new MMap();  //存储处理后的数据，提交操作时使用
   
    
    public FeedBackMindBL()
    {
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData：存储外部数据集合
     * @param cOperate String：数据的操作方式
     * @return boolean：操作成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if(getSubmitMap(cInputData, cOperate) == null)
        {//System.out.println("到这里了没BL().getsubmitMap");
            return false;
        }

        VData data = new VData(); //操作提交接口的数据容器
        
        data.add(map);

        PubSubmit tPubSubmit = new PubSubmit();  //公共数据库提交类
        if(!tPubSubmit.submitData(data, ""))
        {//System.out.println("到这里失败了");
        
            System.out.println(tPubSubmit.mErrors.getErrContent());
            mErrors.addOneError("更新或者保存失败啦！！！");
            return false;
        }
        //System.out.println("到这里了没BL()chenggongla");
        return true;
    }

    /**
     * 方法参数同submitData
     * @param cInputData VData
     * @param cOperate String
     * @return MMap
     */
    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        mTransact = cOperate;

        if(!getInputData(cInputData))
        {      System.out.println("!getInputData(cInputData)");
            return null;
        }
        
        if(!checkData())
        {System.out.println("!checkData()");
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * checkData
     * 校验传入的数据是否符合业务规则要求
     * @return boolean：符合：true，否则false
     */
    private boolean checkData()
    {
        return true;
    }

    /**
    * 进行业务处理
    * @return boolean
    */
   public boolean dealData()
   {
       //保存
	   
       if(SysConst.INSERT.equals(mTransact))
       {
    	   //mfeedbackmindSchema.set("2011"+PubFun1.CreateMaxNo("DM", 10));//PubFun1.CreateMaxNo("DM", 15)
    	   
    	   //mDataMaintenanceSchema.setMakeTime(PubFun.getCurrentTime());
    	   //mDataMaintenanceSchema.setModifyDate(PubFun.getCurrentDate());
    	   //mDataMaintenanceSchema.setModifyTime(PubFun.getCurrentTime());
    	   //mfeedbackmindSchema.setState("N");
    	 // feedbackmindDB db = mfeedbackmindSchema.getDB();
    	// DataMaintenanceDB db2 = mDataMaintenanceSchema.getDB();
    	   
    	   map.put(mfeedbackmindSet, mTransact);
    	  
           //map.put(mfeedbackmindSchema1, mTransact);
           //map.put(mfeedbackmindSchema2, mTransact);
          // map.put(mfeedbackmindSchema3, mTransact);
           //map.put(mfeedbackmindSchema4, mTransact);
          // map.put(mfeedbackmindSchema5, mTransact);
          // map.put(mfeedbackmindSchema6, mTransact);
          // map.put(mfeedbackmindSchema7, mTransact);
       }
       //修改
       return true;
   }

   /**
        * 接收传递的参数
        * @param cInputData VData
        * @return boolean
        */
      public boolean getInputData(VData cInputData)
       {//System.out.println("已经进入到了getInputData");
           mGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        mfeedbackmindSet = (feedbackmindSet) cInputData.getObjectByObjectName("feedbackmindSet", 0);
           
          // mfeedbackmindSchema1=(feedbackmindSchema) cInputData.getObjectByObjectName("feedbackmindSet", 0);
           //mfeedbackmindSchema2=(feedbackmindSchema) cInputData.getObjectByObjectName("feedbackmindSet", 1);
          // mfeedbackmindSchema3=(feedbackmindSchema) cInputData.getObjectByObjectName("feedbackmindSet", 2);
          // mfeedbackmindSchema4=(feedbackmindSchema) cInputData.getObjectByObjectName("feedbackmindSet", 3);
          // mfeedbackmindSchema5=(feedbackmindSchema) cInputData.getObjectByObjectName("feedbackmindSet", 4);
          // mfeedbackmindSchema6=(feedbackmindSchema) cInputData.getObjectByObjectName("feedbackmindSet", 5);
          // mfeedbackmindSchema7=(feedbackmindSchema) cInputData.getObjectByObjectName("feedbackmindSet", 6);
           
           if(mGI == null)
           { //System.out.println("mgi==null");
               mErrors.addOneError("请传入操作员信息");
               return false;
           }
           if(mfeedbackmindSet == null)
           { //System.out.println("mLCSignLogSchema == null");
               mErrors.addOneError("请传入账户信息");
               return false;
           }

           return true;
    }

    public static void main(String[] args)
    {
    	FeedBackMindBL proposalsignlogbl = new FeedBackMindBL();
    }
}
