package com.sinosoft.lis.dbmaintenance;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
//import com.sinosoft.lis.dbmaintenance.FeedBackMindBL;

public class FeedBackMindUI
{
    public CErrors mErrors = new CErrors();

    public FeedBackMindUI()
    {
    }

   
    public boolean submitData(VData cInputData, String cOperate)
    {
    	FeedBackMindBL bl = new FeedBackMindBL();
        if(!bl.submitData(cInputData, cOperate))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }
       
        return true;
    }


    public static void main(String[] args)
    {
    	FeedBackMindUI bl = new FeedBackMindUI();
    }
}