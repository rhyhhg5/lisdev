package com.sinosoft.lis.dbmaintenance;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;

public class DataMaintenanceBL
{
    /**错误的容器，错程序报错，可通过该变量获取出错信息*/
    public CErrors mErrors = new CErrors();
    private String mTransact = null;
    private GlobalInput mGI = null;
    private  DataMaintenanceSchema mDataMaintenanceSchema = null;
    private  feedbackmindSchema mfeedbackmindSchema = null;
    
    private MMap map = new MMap();  //存储处理后的数据，提交操作时使用

    public DataMaintenanceBL()
    {
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData：存储外部数据集合
     * @param cOperate String：数据的操作方式
     * @return boolean：操作成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if(getSubmitMap(cInputData, cOperate) == null)
        {//System.out.println("到这里了没BL().getsubmitMap");
            return false;
        }

        VData data = new VData(); //操作提交接口的数据容器
        data.add(map);

        PubSubmit tPubSubmit = new PubSubmit();  //公共数据库提交类
        if(!tPubSubmit.submitData(data, ""))
        {//System.out.println("到这里失败了");
        
            System.out.println(tPubSubmit.mErrors.getErrContent());
            mErrors.addOneError("保存失败啦！！！");
            return false;
        }
        //System.out.println("到这里了没BL()chenggongla");
        return true;
    }

    /**
     * 方法参数同submitData
     * @param cInputData VData
     * @param cOperate String
     * @return MMap
     */
    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        mTransact = cOperate;

        if(!getInputData(cInputData))
        {      System.out.println("!getInputData(cInputData)");
            return null;
        }
        
        if(!checkData())
        {System.out.println("!checkData()");
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * checkData
     * 校验传入的数据是否符合业务规则要求
     * @return boolean：符合：true，否则false
     */
    private boolean checkData()
    {
        return true;
    }

    /**
    * 进行业务处理
    * @return boolean
    */
   public boolean dealData()
   {
       //保存
       if(SysConst.INSERT.equals(mTransact))
       {
    	   mDataMaintenanceSchema.setSerialNo(PubFun.getCurrentDate2()+PubFun1.CreateMaxNo("DM", 4));//PubFun1.CreateMaxNo("DM", 15)
    	   mDataMaintenanceSchema.setMakeDate(PubFun.getCurrentDate());
    	   //mDataMaintenanceSchema.setMakeTime(PubFun.getCurrentTime());
    	   //mDataMaintenanceSchema.setModifyDate(PubFun.getCurrentDate());
    	   //mDataMaintenanceSchema.setModifyTime(PubFun.getCurrentTime());
    	   mDataMaintenanceSchema.setState("N");
           map.put(mDataMaintenanceSchema, mTransact);
       }
       //修改
 
       else if(SysConst.UPDATE.equals(mTransact))
       {      //System.out.println("到这里了没BL()修改");
    	   DataMaintenanceDB db = mDataMaintenanceSchema.getDB();
    	   feedbackmindDB db2 = mfeedbackmindSchema.getDB();
           if(!db.getInfo())
           {
               mErrors.addOneError("请输入数值");
               return false;
           }

           db.setSerialNo(mDataMaintenanceSchema.getSerialNo());
           db.setTableName(mDataMaintenanceSchema.getTableName());
           db.setFieldName(mDataMaintenanceSchema.getFieldName());
           db.setCondition(mDataMaintenanceSchema.getCondition());
           db.setDataSize(mDataMaintenanceSchema.getDataSize());
           db.setReason(mDataMaintenanceSchema.getReason());
           db.setRoute(mDataMaintenanceSchema.getRoute());
           db.setMakeDate(PubFun.getCurrentDate());
           db.setMaintenanceMan(mDataMaintenanceSchema.getMaintenanceMan());
           if(db2.getState()!=null)
           {
        	   db.setState(db2.getState());
        	}
           else
           {
           db.setState("N");
           }
           db.setcq(mDataMaintenanceSchema.getcq());
           map.put(db.getSchema(), mTransact);
       }
       //删除
       else if(SysConst.DELETE.equals(mTransact))
       {
           if((mDataMaintenanceSchema.getSerialNo() == null)||( mDataMaintenanceSchema.getSerialNo().equals("")))
           {
               mErrors.addOneError("请选择流水号");
               return false;
           }
           //目前不需要增加其他处理

           
           
           map.put(mDataMaintenanceSchema, mTransact);
       }

       return true;
   }

   /**
        * 接收传递的参数
        * @param cInputData VData
        * @return boolean
        */
      public boolean getInputData(VData cInputData)
       {//System.out.println("已经进入到了getInputData");
           mGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
           mDataMaintenanceSchema = (DataMaintenanceSchema) cInputData.getObjectByObjectName("DataMaintenanceSchema", 0);
           if(mGI == null)
           { //System.out.println("mgi==null");
               mErrors.addOneError("请传入操作员信息");
               return false;
           }
           if(mDataMaintenanceSchema == null)
           { //System.out.println("mLCSignLogSchema == null");
               mErrors.addOneError("请传入账户信息");
               return false;
           }

           return true;
    }

    public static void main(String[] args)
    {
    	DataMaintenanceBL proposalsignlogbl = new DataMaintenanceBL();
    }
}
