package com.sinosoft.lis.agentquery;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.*;
import java.text.DecimalFormat;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class LAAMWageQueryBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mManageCom = null;
    private String mAgentCom = null;
    private String mWageNo = null;

    private String mOutXmlPath = null;
    //private String mtype = null;

    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();
    SSRS tSSRS=new SSRS();
    SSRS tSSRS1=new SSRS();
    public LAAMWageQueryBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
    	System.out.println("BL->dealDate()");    	

        
        
    	mSql="select distinct agentcom,name from lacom where branchtype='1' and branchtype2 in  ('02','04') "
		+" and endflag='N' and managecom like '"+mManageCom+"%' ";
        if (mAgentCom != null && !mAgentCom.equals("")) {
        	mSql=mSql+" and agentcom='"+mAgentCom+"'";		 
    		 }
        mSql=mSql+" order by agentcom ";
	    ExeSQL tExeSQL = new ExeSQL();
	    tSSRS = tExeSQL.execSQL(mSql);
	    
	    String tsql1="SELECT code,codename,othersign FROM LDCODE WHERE codetype='amadd' ";
        ExeSQL tExeSQL1 = new ExeSQL();
        tSSRS1 = tExeSQL1.execSQL(tsql1);
        String[][] mToExcel = new String[tSSRS.getMaxRow() + 4][30];
        mToExcel[0][0] = "个险中介机构佣金参考报表";

        mToExcel[1][0] = "管理机构";
        mToExcel[1][1] = "所属年月";
        mToExcel[1][2] = "机构代码";
        mToExcel[1][3] = "机构名称";        
        mToExcel[1][4] = "提奖";
        
        for(int rrow = 1; rrow<=tSSRS1.getMaxRow();rrow++)
        {        	
                mToExcel[1][rrow + 4] = tSSRS1.GetText(rrow, 2);
        }
        mToExcel[1][tSSRS1.getMaxRow()+5] = "汇总";       
	    
        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            	mToExcel[row+1][0] = mManageCom;
            	mToExcel[row+1][1] = mWageNo;
            	System.out.println(tSSRS.GetText(row, 1)+" "+tSSRS.GetText(row, 2));
                mToExcel[row+1][2] = tSSRS.GetText(row, 1);
                mToExcel[row+1][3] = tSSRS.GetText(row, 2);
                String magentcom=tSSRS.GetText(row, 1);                
                mToExcel[row+1][4] = getSumFyc(magentcom,mWageNo);
                double msum= Double.parseDouble(mToExcel[row+1][4]);
                for(int rrow = 1; rrow<=tSSRS1.getMaxRow();rrow++)
                {        	
                        mToExcel[row+1][rrow + 4] = getAMAdd(magentcom,tSSRS1.GetText(rrow, 1),mWageNo);
                        msum=msum+Double.parseDouble(mToExcel[row+1][rrow + 4]);                        
                }
                mToExcel[row+1][tSSRS1.getMaxRow() + 5]=String.valueOf(msum);
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    private String getSumFyc(String pmAgentCom,String pmWageNo)
    {
       String tSQL = "";
       String tRtValue="";
       tSQL = " select value(sum(fyc),0) from lacommision "
             +" where  agentcom='"+pmAgentCom+"' and wageno='"+pmWageNo+"'";
       SSRS tSSRS = new SSRS();
       ExeSQL tExeSQL = new ExeSQL();
       tSSRS = tExeSQL.execSQL(tSQL);
       if(tSSRS.getMaxRow()==0)
        {
          tRtValue="0";
        }
        else
       tRtValue=tSSRS.GetText(1, 1);
       return tRtValue;

    }
    private String getAMAdd(String pmAgentCom,String pmCodeType,String pmWageNo)
    {
       String tSQL = "";
       String tRtValue="";
       if(pmCodeType.compareTo("20")<0){
    	   tSQL = " select value(sum(money),0) from larewardpunish "
               +" where  agentcom='"+pmAgentCom+"' and wageno='"+pmWageNo+"'"
               +" and doneflag='"+ pmCodeType+"'";
       }
       else
       tSQL = " select value(sum(money)*(-1),0) from larewardpunish "
             +" where  agentcom='"+pmAgentCom+"' and wageno='"+pmWageNo+"'"
             +" and doneflag='"+ pmCodeType+"'";
       SSRS tSSRS = new SSRS();
       ExeSQL tExeSQL = new ExeSQL();
       tSSRS = tExeSQL.execSQL(tSQL);
       if(tSSRS.getMaxRow()==0)
        {
          tRtValue="0";
        }
        else
       tRtValue=tSSRS.GetText(1, 1);
       return tRtValue;

    }
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAAMWageQueryBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        mManageCom = (String) tf.getValueByName("ManageCom");
        mAgentCom = (String) tf.getValueByName("AgentCom");
        mWageNo = (String) tf.getValueByName("WageNo");
        if(mManageCom == null || mWageNo == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAAMWageQueryBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

  

    /**
    * 执行SQL文查询结果
    * @param sql String
    * @return double
    */
   private double execQuery(String sql)

   {
      Connection conn;
      conn = null;
      conn = DBConnPool.getConnection();

      System.out.println(sql);

      PreparedStatement st = null;
      ResultSet rs = null;
      try {
          if (conn == null)return 0.00;
          st = conn.prepareStatement(sql);
          if (st == null)return 0.00;
          rs = st.executeQuery();
          if (rs.next()) {
              return rs.getDouble(1);
          }
          return 0.00;
      } catch (Exception ex) {
          ex.printStackTrace();
          return -1;
      } finally {
          try {
             if (!conn.isClosed()) {
                 conn.close();
             }
             try {
                 st.close();
                 rs.close();
             } catch (Exception ex2) {
                 ex2.printStackTrace();
             }
             st = null;
             rs = null;
             conn = null;
           } catch (Exception e) {}

      }
     }

    public static void main(String[] args)
    {
        LAAMWageQueryBL LAAMWageQueryBL = new
            LAAMWageQueryBL();
    System.out.println("11111111");
    }
}
