package com.sinosoft.lis.agentquery;

import com.sinosoft.lis.bl.LAAgentBL;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LATreeAccessoryDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.schema.LATreeAccessorySchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vbl.LAAgentBLSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 代理人序列树逻辑处理</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class LAAgentTreeBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private VData mAgentGradeTree = new VData();
    private VData mBranchGradeTree = new VData();
    private VData mAgentRearTree = new VData();
    private int mIndex = 0;
    /* 信息分隔符 */
    private static final String PACKAGESPILTER = "|";
    private static final String RECORDSPLITER = "^";
    /**  */
    private LAAgentBL mLAAgentBL = new LAAgentBL();
    private LAAgentBLSet mLAAgentBLSet = new LAAgentBLSet();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    public LAAgentTreeBL()
    {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        //数据查询业务处理

        if (!queryData())
        {
            return false;
        }

        // 数据操作业务处理
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData()
    {

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                                 getObjectByObjectName("LAAgentSchema", 0));
        return true;

    }

    /**
     * 上下级机构隶属关系树
     * @param tLATreeSchema
     * @param tIndex
     */
    private void getBranchGradeTree(LABranchGroupSchema tLABranchGroupSchema,
                                    int tIndex)
    {
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupDB.setUpBranch(tLABranchGroupSchema.getAgentGroup());
        tLABranchGroupSet = tLABranchGroupDB.query();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("ParentCode",
                                      tLABranchGroupSchema.getUpBranch());
        tTransferData.setNameAndValue("ChildCount", tLABranchGroupSet.size());
        tTransferData.setNameAndValue("ChildCode",
                                      tLABranchGroupSchema.getAgentGroup());
        tTransferData.setNameAndValue("ChildName",
                                      tLABranchGroupSchema.getName().trim() +
                                      "(" + tLABranchGroupSchema.getBranchAttr() +
                                      ")");
        mBranchGradeTree.addElement(tTransferData);
//    mAgentGradeTree[tIndex][0]=tLABranchGroupSchema.getUpAgent();
//    mAgentGradeTree[tIndex][1] = String.valueOf(tLABranchGroupSet.size());
//    mAgentGradeTree[tIndex][3] = tLABranchGroupSchema.getAgentCode();
        mIndex++;
        for (int i = 1; i <= tLABranchGroupSet.size(); i++)
        {
            this.getBranchGradeTree(tLABranchGroupSet.get(i), mIndex);
        }
        System.out.println(mBranchGradeTree);
    }

    /**
     * 上下级代理人关系树生成
     * @param tLATreeSchema
     * @param tIndex
     */
    private void getAgentGradeTree(LATreeSchema tLATreeSchema, int tIndex)
    {

        LATreeSet tLATreeSet = new LATreeSet();
        LATreeDB tLATreeDB = new LATreeDB();
        tLATreeDB.setUpAgent(tLATreeSchema.getAgentCode());
        tLATreeSet = tLATreeDB.query();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("ParentCode", tLATreeSchema.getUpAgent());
        tTransferData.setNameAndValue("ChildCount", tLATreeSet.size());
        tTransferData.setNameAndValue("ChildCode", tLATreeSchema.getAgentCode());
        mAgentGradeTree.addElement(tTransferData);
//    mAgentGradeTree[tIndex][0]=tLATreeSchema.getUpAgent();
//    mAgentGradeTree[tIndex][1] = String.valueOf(tLATreeSet.size());
//    mAgentGradeTree[tIndex][3] = tLATreeSchema.getAgentCode();
        mIndex++;
        for (int i = 1; i <= tLATreeSet.size(); i++)
        {
            this.getAgentGradeTree(tLATreeSet.get(i), mIndex);
        }
        System.out.println(mAgentGradeTree);
    }


    /**
     * 育成关系树生成
     * @param tLATreeSchema
     * @param tIndex
     */
    private void getAgentRearTree(LATreeAccessorySchema tLATreeAccessorySchema,
                                  int tIndex)
    {

        LATreeAccessorySet tLATreeAccessorySet = new LATreeAccessorySet();
        LATreeAccessoryDB tLATreeAccessoryDB = new LATreeAccessoryDB();
        tLATreeAccessoryDB.setRearAgentCode(tLATreeAccessorySchema.getAgentCode());
        tLATreeAccessorySet = tLATreeAccessoryDB.query();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("ParentCode",
                                      tLATreeAccessorySchema.getRearAgentCode());
        tTransferData.setNameAndValue("ChildCount", tLATreeAccessorySet.size());
        tTransferData.setNameAndValue("ChildCode",
                                      tLATreeAccessorySchema.getAgentCode());
        mAgentRearTree.addElement(tTransferData);
//    mAgentRearTreeAccessory[tIndex][0]=tLATreeAccessorySchema.getUpAgent();
//    mAgentRearTreeAccessory[tIndex][1] = String.valueOf(tLATreeAccessorySet.size());
//    mAgentRearTreeAccessory[tIndex][3] = tLATreeAccessorySchema.getAgentCode();
        mIndex++;
        for (int i = 1; i <= tLATreeAccessorySet.size(); i++)
        {
            this.getAgentRearTree(tLATreeAccessorySet.get(i), mIndex);
        }
        System.out.println(mAgentRearTree);
    }

    /**
     * 查询符合条件的暂交费信息
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean queryData()
    {
        String tDataStr = "";
        String tAgentCode = mLAAgentSchema.getAgentCode();
        String tAgentName = mLAAgentSchema.getName();
        String tAgentGroup = mLAAgentSchema.getAgentGroup();

        if (mOperate.equals("query"))
        {
            //上下级关系树生成
            LATreeSchema tLATreeSchema = new LATreeSchema();
            tLATreeSchema.setUpAgent("0");
            tLATreeSchema.setAgentCode(tAgentCode);
            this.getAgentGradeTree(tLATreeSchema, mIndex);
            for (int i = 0; i < mAgentGradeTree.size(); i++)
            {
                LAAgentDB tLAAgentDB = new LAAgentDB();
                TransferData tTransferData = new TransferData();
                tTransferData = (TransferData) mAgentGradeTree.get(i);
                tLAAgentDB.setAgentCode((String) ((TransferData)
                                                  mAgentGradeTree.get(i)).
                                        getValueByName("ChildCode"));
                if (tLAAgentDB.getInfo())
                {
                    ((TransferData) mAgentGradeTree.get(i)).setNameAndValue(
                            "ChildName",
                            tLAAgentDB.getName().trim() + "(" +
                            tLAAgentDB.getAgentCode().trim() + ")");
                }
                tDataStr = tDataStr.trim() + RECORDSPLITER +
                           ((TransferData) mAgentGradeTree.get(i)).
                           getValueByName("ParentCode") + PACKAGESPILTER +
                           ((TransferData) mAgentGradeTree.get(i)).
                           getValueByName("ChildCount") + PACKAGESPILTER +
                           ((TransferData) mAgentGradeTree.get(i)).
                           getValueByName("ChildName") + PACKAGESPILTER +
                           ((TransferData) mAgentGradeTree.get(i)).
                           getValueByName("ChildCode");
            }

            String tHeader1 = "0|" + mAgentGradeTree.size();
            String tResultTree1 = tHeader1 + tDataStr;

            //育成关系树生成
            mIndex = 0;
            tDataStr = "";
            LATreeAccessorySchema tLATreeAccessorySchema = new
                    LATreeAccessorySchema();
            tLATreeAccessorySchema.setRearAgentCode("0");
            tLATreeAccessorySchema.setAgentCode(tAgentCode);
            this.getAgentRearTree(tLATreeAccessorySchema, mIndex);
            for (int i = 0; i < mAgentRearTree.size(); i++)
            {
                LAAgentDB tLAAgentDB = new LAAgentDB();
                TransferData tTransferData = new TransferData();
                tTransferData = (TransferData) mAgentRearTree.get(i);
                tLAAgentDB.setAgentCode((String) ((TransferData) mAgentRearTree.
                                                  get(i)).getValueByName(
                        "ChildCode"));
                if (tLAAgentDB.getInfo())
                {
                    ((TransferData) mAgentRearTree.get(i)).setNameAndValue(
                            "ChildName",
                            tLAAgentDB.getName().trim() + "(" +
                            tLAAgentDB.getAgentCode().trim() + ")");
                }
                tDataStr = tDataStr.trim() + RECORDSPLITER +
                           ((TransferData) mAgentRearTree.get(i)).
                           getValueByName("ParentCode") + PACKAGESPILTER +
                           ((TransferData) mAgentRearTree.get(i)).
                           getValueByName("ChildCount") + PACKAGESPILTER +
                           ((TransferData) mAgentRearTree.get(i)).
                           getValueByName("ChildName") + PACKAGESPILTER +
                           ((TransferData) mAgentRearTree.get(i)).
                           getValueByName("ChildCode");
            }

            String tHeader2 = "0|" + mAgentGradeTree.size();
            String tResultTree2 = tHeader2 + tDataStr;

            mResult.clear();
            mResult.add(tResultTree1);
            mResult.add(tResultTree2);
        }
        else if (mOperate.equals("querybranch"))
        {
            //上下级机构关系树生成
            LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
            tLABranchGroupSchema.setUpBranch("0");
            tLABranchGroupSchema.setAgentGroup(tAgentGroup);
            LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
            tLABranchGroupDB.setAgentGroup(tAgentGroup);
            if (tLABranchGroupDB.getInfo())
            {
                tLABranchGroupSchema.setName(tLABranchGroupDB.getName());
                tLABranchGroupSchema.setBranchAttr(tLABranchGroupDB.
                        getBranchAttr());
            }
            this.getBranchGradeTree(tLABranchGroupSchema, mIndex);
            for (int i = 0; i < mBranchGradeTree.size(); i++)
            {
                tDataStr = tDataStr.trim() + RECORDSPLITER +
                           ((TransferData) mBranchGradeTree.get(i)).
                           getValueByName("ParentCode") + PACKAGESPILTER +
                           ((TransferData) mBranchGradeTree.get(i)).
                           getValueByName("ChildCount") + PACKAGESPILTER +
                           ((TransferData) mBranchGradeTree.get(i)).
                           getValueByName("ChildName") + PACKAGESPILTER +
                           ((TransferData) mBranchGradeTree.get(i)).
                           getValueByName("ChildCode");
            }

            String tHeader1 = "0|" + mBranchGradeTree.size();
            String tResultTree = tHeader1 + tDataStr;
            mResult.clear();
            mResult.add(tResultTree);
        }
        return true;

    }

    /**
     * 查询符合条件的信息
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean queryPol()
    {
        return true;
    }

    /**
     * 校验传入的是否合法
     * 输出：如果发生错误则返回false,否则返回true
     */
    private boolean checkData()
    {

        boolean flag = true;
        return flag;

    }

    /**
     * 准备需要保存的数据
     */
    private void prepareOutputData()
    {
    }

    public static void main(String args[])
    {
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        LAAgentTreeBL tLAAgentTreeBL = new LAAgentTreeBL();
        tLAAgentSchema.setAgentCode("8611000130");
        tLAAgentSchema.setName("aa");
        VData tVData = new VData();
        tVData.add(tLAAgentSchema);
        tLAAgentTreeBL.submitData(tVData, "query");
    }
}
