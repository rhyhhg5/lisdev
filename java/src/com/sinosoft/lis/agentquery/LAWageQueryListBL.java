package com.sinosoft.lis.agentquery;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.FileWriter;
import java.io.*;

public class LAWageQueryListBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    //private String mtype = null;

    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();
    private String tName="";
    private String MakeDate="";

    public LAWageQueryListBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        SSRS tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LAStandardRCDownloadBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 7][26];

        mToExcel[0][0] = "中国人民健康保险股份有限公司"+tName+"分公司";
        mToExcel[1][0] =MakeDate+"份营销员佣金明细查询";
        mToExcel[2][0] ="编制部门:个险销售部 ";
        mToExcel[3][0] = "所属年月";
        mToExcel[3][1] = "展业机构号码";
        mToExcel[3][2] = "展业机构名称";
        mToExcel[3][3] = "业务员代码";
        mToExcel[3][4] = "业务员姓名";
        mToExcel[3][5] = "保单号";
        mToExcel[3][6] = "投保人";
        mToExcel[3][7] = "被保人";
        mToExcel[3][8] = "险种";
        mToExcel[3][9] = "险种名称";
        mToExcel[3][10] = "保费(元)";
        mToExcel[3][11] = "佣金比例";
        mToExcel[3][12] = "佣金(元)";
        mToExcel[3][13] = "缴费方式";
        mToExcel[3][14] = "缴费期";
        mToExcel[3][15] = "缴费次数";
        mToExcel[3][16] = "缴费时间";
        mToExcel[3][17] = "客户签字日期";
        mToExcel[3][18] = "保单回执回销日期";
        mToExcel[3][19] = "交单时间";
        mToExcel[3][20] = "续期/续保标记";
        mToExcel[3][21] = "人员状态";
        mToExcel[3][22] = "签单日期";
        mToExcel[3][23] = "回访成功日期";
        mToExcel[3][24] = "回访成功标志";
        mToExcel[3][25] = "是否孤儿单";
        
        

        int num=tSSRS.getMaxRow();
        int mcol=tSSRS.getMaxCol();
        for(int row = 4; row <= num+3; row++)
        {
            for(int col = 1; col <= mcol; col++)
            {
                mToExcel[row][col - 1] = tSSRS.GetText(row-3, col);
            }
        }
        mToExcel[num+6][0] = "总经理：                        复核 ：              制表：";
        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAStandardRCDownloadBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        tName = (String) tf.getValueByName("tName");
        MakeDate = (String) tf.getValueByName("MakeDate");
       //mtype = (String) tf.getValueByName("Type");
        if(mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAStandardRCDownloadBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }


    public static void main(String[] args)
    {
        //RateCommisionBL RateCommisionBL = new
           //RateCommisionBL();
    }
}
