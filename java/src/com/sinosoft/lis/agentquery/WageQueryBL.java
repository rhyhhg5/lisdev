package com.sinosoft.lis.agentquery;

import com.sinosoft.lis.schema.LAWageSchema;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class WageQueryBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private String mMonth[] = null;
    private String ManageCom = null;
    private LAWageSchema mLAWageSchema = new LAWageSchema();
    private String AgentCode = "";
    private String BranchAttr = "";
    private String StartMonth = "";
    private String EndMonth = "";

    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    //private GlobalInput mGlobalInput =new GlobalInput() ;
    public WageQueryBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("Start WageQueryBL Submit ...");
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        System.out.println("Start getInputData Submit ...");
        mMonth = (String[]) cInputData.get(0);
        System.out.println(mMonth[0]);
        System.out.println(ManageCom);
        ManageCom = (String) cInputData.get(2);
        System.out.println(ManageCom);
        mLAWageSchema.setSchema((LAWageSchema) cInputData.getObjectByObjectName(
                "LAWageSchema", 0));
        AgentCode = mLAWageSchema.getAgentCode();
        BranchAttr = mLAWageSchema.getState();
        System.out.println(BranchAttr);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCPolBillF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        System.out.println("Start queryData Submit ...");
        SSRS t_ssrs = new SSRS();
        LAWageSet tLAWageSet = new LAWageSet();
        String sql = "";
        if ((mLAWageSchema.getAgentCode() != null) &&
            (!mLAWageSchema.getAgentCode().equals("")))
        {
            AgentCode = " and LAWage.AgentCode='" + mLAWageSchema.getAgentCode() +
                        "' ";
        }
        if ((mLAWageSchema.getState() != null) &&
            (!mLAWageSchema.getState().equals("")))
        {
            BranchAttr = " and BranchAttr like '" + mLAWageSchema.getState() +
                         "%'";
        }
        if ((mMonth[0] != null) && (mMonth[1] != null) && (!mMonth[0].equals("")) &&
            (!mMonth[1].equals("")))
        {
            StartMonth = "and IndexCalNo>='" + mMonth[0] + "'";
            EndMonth = "and IndexCalNo<='" + mMonth[1] + "'";
        }
        sql = "select LAWage.AgentCode,LAAgent.Name,BranchAttr,IndexCalNo,SumMoney,F01,F02,F03,F04,F05,"
              + " F06,F07,F08,F09,F10,F11,F12,F13,F14,F15,F16,F17,F18,F19,F20,F21,F22,F23,F24,F25,K11,F30,F22,F23,F24,K12,K01+K02,K03,K04,K05,AgentGrade,W10"
              + " from LAWage,LAAgent where LAWage.AgentCode=LAAgent.AgentCode and LAAgent.managecom like '" +
              ManageCom + "%' and state='1'"
              + AgentCode + BranchAttr + StartMonth + EndMonth
              + " order by LAWage.AgentCode,IndexCalNo";

        ExeSQL tExeSQL = new ExeSQL();
        t_ssrs = tExeSQL.execSQL(sql);
        for (int i = 1; i <= t_ssrs.getMaxRow(); i++)
        {
            LAWageSchema tLAWageSchema = new LAWageSchema();
            tLAWageSchema.setAgentCode(t_ssrs.GetText(i, 1));
            tLAWageSchema.setManageCom(t_ssrs.GetText(i, 2)); //其中被赋值LAAgent.Name
            tLAWageSchema.setOperator2(t_ssrs.GetText(i, 3)); //其中被赋值labranchgroup.BranchAttr
            System.out.println(tLAWageSchema.getOperator2());
            tLAWageSchema.setIndexCalNo(t_ssrs.GetText(i, 4));
            tLAWageSchema.setSumMoney(t_ssrs.GetText(i, 5));
            tLAWageSchema.setF01(t_ssrs.GetText(i, 6));
            tLAWageSchema.setF02(t_ssrs.GetText(i, 7));
            tLAWageSchema.setF03(t_ssrs.GetText(i, 8));
            tLAWageSchema.setF04(t_ssrs.GetText(i, 9));
            tLAWageSchema.setF05(t_ssrs.GetText(i, 10));
            tLAWageSchema.setF06(t_ssrs.GetText(i, 11));
            tLAWageSchema.setF07(t_ssrs.GetText(i, 12));
            tLAWageSchema.setF08(t_ssrs.GetText(i, 13));
            tLAWageSchema.setF09(t_ssrs.GetText(i, 14));
            tLAWageSchema.setF10(t_ssrs.GetText(i, 15));
            tLAWageSchema.setF11(t_ssrs.GetText(i, 16));
            tLAWageSchema.setF12(t_ssrs.GetText(i, 17));
            tLAWageSchema.setF13(t_ssrs.GetText(i, 18));
            tLAWageSchema.setF14(t_ssrs.GetText(i, 19));
            tLAWageSchema.setF15(t_ssrs.GetText(i, 20));
            tLAWageSchema.setF16(t_ssrs.GetText(i, 21));
            tLAWageSchema.setF17(t_ssrs.GetText(i, 22));
            tLAWageSchema.setF18(t_ssrs.GetText(i, 23));
            tLAWageSchema.setF19(t_ssrs.GetText(i, 24));
            tLAWageSchema.setF20(t_ssrs.GetText(i, 25));
            tLAWageSchema.setF21(t_ssrs.GetText(i, 26));
            tLAWageSchema.setF22(t_ssrs.GetText(i, 27));
            tLAWageSchema.setF23(t_ssrs.GetText(i, 28));
            tLAWageSchema.setF24(t_ssrs.GetText(i, 29));
            tLAWageSchema.setF25(t_ssrs.GetText(i, 30));
            tLAWageSchema.setK11(t_ssrs.GetText(i, 31));
            tLAWageSchema.setF30(t_ssrs.GetText(i, 32));
            tLAWageSchema.setF22(t_ssrs.GetText(i, 33));
            tLAWageSchema.setF23(t_ssrs.GetText(i, 34));
            tLAWageSchema.setF24(t_ssrs.GetText(i, 35));
            tLAWageSchema.setK12(t_ssrs.GetText(i, 36));
            tLAWageSchema.setK01(t_ssrs.GetText(i, 37));
            tLAWageSchema.setK03(t_ssrs.GetText(i, 38));
            tLAWageSchema.setK04(t_ssrs.GetText(i, 39));
            tLAWageSchema.setK05(t_ssrs.GetText(i, 40));
            tLAWageSchema.setW10(t_ssrs.GetText(i, 42));
            //判断代理人级别
            String tAgentGrade = t_ssrs.GetText(i, 41);

            if (tAgentGrade.equals("A01") || tAgentGrade.equals("A02") ||
                tAgentGrade.equals("A03"))
            {
                tLAWageSchema.setModifyTime("");
                tLAWageSchema.setBranchType("");
                tLAWageSchema.setOperator("");
            }
            if (tAgentGrade.equals("A04") || tAgentGrade.equals("A05"))
            {
                //营业组佣金总额
                String asql =
                        "select T64 from laindexinfo where indextype='01' and agentcode='" +
                        tLAWageSchema.getAgentCode() + "'  and indexcalno='" +
                        t_ssrs.GetText(i, 4) + "'" + BranchAttr;
                System.out.println(asql);
                ExeSQL aExeSQL = new ExeSQL();
                tLAWageSchema.setModifyTime(aExeSQL.getOneValue(asql)); //暂放营业组佣金
                tLAWageSchema.setBranchType("");
                tLAWageSchema.setOperator("");
            }
            if (tAgentGrade.equals("A06") | tAgentGrade.equals("A07"))
            {
//          //营业组佣金总额
                String asql =
                        "select T64 from laindexinfo where indextype='01' and  agentcode='" +
                        tLAWageSchema.getAgentCode() + "'  and indexcalno='" +
                        t_ssrs.GetText(i, 4) + "'" + BranchAttr;
                System.out.println(asql);
                ExeSQL aExeSQL = new ExeSQL();
                tLAWageSchema.setModifyTime(aExeSQL.getOneValue(asql)); //暂放营业组佣金
                //营业部佣金总额
                String bsql =
                        "select T65 from laindexinfo where indextype='01' and agentcode='" +
                        tLAWageSchema.getAgentCode() + "'  and indexcalno='" +
                        t_ssrs.GetText(i, 4) + "'" + BranchAttr;
                ExeSQL bExeSQL = new ExeSQL();
                tLAWageSchema.setBranchType(bExeSQL.getOneValue(bsql)); //暂放营业部佣金
                tLAWageSchema.setOperator("");
            }
            if (tAgentGrade.equals("A08") || tAgentGrade.equals("A09"))
            {
//          //营业组佣金总额
                String asql =
                        "select T64 from laindexinfo where indextype='01' and agentcode='" +
                        tLAWageSchema.getAgentCode() + "'  and indexcalno='" +
                        t_ssrs.GetText(i, 4) + "'" + BranchAttr;
                System.out.println(asql);
                ExeSQL aExeSQL = new ExeSQL();
                tLAWageSchema.setModifyTime(aExeSQL.getOneValue(asql)); //暂放营业组佣金
                //营业部佣金总额
                String bsql =
                        "select T65 from laindexinfo where indextype='01' and agentcode='" +
                        tLAWageSchema.getAgentCode() + "'  and indexcalno='" +
                        t_ssrs.GetText(i, 4) + "'" + BranchAttr;
                ExeSQL bExeSQL = new ExeSQL();
                tLAWageSchema.setBranchType(bExeSQL.getOneValue(bsql)); //暂放营业部佣金
                System.out.println(tLAWageSchema.getCurrMoney());
                //督导部佣金总额
                String csql =
                        "select T66 from laindexinfo where indextype='01' and agentcode='" +
                        tLAWageSchema.getAgentCode() + "'  and indexcalno='" +
                        t_ssrs.GetText(i, 4) + "'" + BranchAttr;
                ExeSQL cExeSQL = new ExeSQL();
                tLAWageSchema.setOperator(cExeSQL.getOneValue(csql)); //暂放督导部佣金
                System.out.println(tLAWageSchema.getOperator());
            }

            tLAWageSet.add(tLAWageSchema);
        }
        System.out.println(tLAWageSet.size());
        mResult.clear();
        mResult.add(tLAWageSet);
        return true;
    }

    public static void main(String[] args)
    {
        WageQueryBL tWageQueryBL = new WageQueryBL();
    }
}
