/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.agentquery;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.vschema.LAChargeToNxsLNSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.lis.schema.LAChargeToNxsLNSchema;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.vschema.LAWageSet;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: </p>
 * @author Alse
 * @version 1.0
 */
public class LAChargeToNxsLNTBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();


    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String currentDate = PubFun.getCurrentDate();
    private TransferData mGetCessData = new TransferData();
    private String mStartDate = "";
    private String mEndDate = "";
    private String mIndexCalNo = "";
    private String mManageCom = "";
    private String mCheck1 = "";

    private LAChargeToNxsLNSet mLAChargeToNxsLNSet = new LAChargeToNxsLNSet();
    private LAChargeToNxsLNSet tLAChargeToNxsLNSet = new LAChargeToNxsLNSet();

    private MMap mMap = new MMap();
    private VData mInputData= new VData();
    private String mOperate="";
    public LAChargeToNxsLNTBL()
    {
    }

    public static void main(String[] args)
    {
    }


    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData)
    {

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        if (!prepareOutputData())
        {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start LAChargeToNxsLNTBL Submit...");
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAChargeToNxsLNTBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData=null;

        return true;
    }

    private boolean dealData()
    {

      for (int i = 1; i <= mLAChargeToNxsLNSet.size(); i++) {
            LAChargeToNxsLNSchema tLAChargeToNxsLNSchema = new LAChargeToNxsLNSchema();
            tLAChargeToNxsLNSchema = mLAChargeToNxsLNSet.get(i);
            
            String  tsql = "select * from LAChargeToNxsLN "
            	+"  where agentcom='"+tLAChargeToNxsLNSchema.getAgentCom()
            	+"' and tmakedate>='"+mStartDate+"' and tmakedate<='"+mEndDate+"' "
            	+"  and agentcom in (select agentcom from lacom where managecom like '"+mManageCom+"%')"
            	+"  and state='0' ";

System.out.println(tsql);
            LAChargeToNxsLNDB tLAChargeToNxsLNDB = new LAChargeToNxsLNDB();
            LAChargeToNxsLNSet ttLAChargeToNxsLNSet=new LAChargeToNxsLNSet();
            ttLAChargeToNxsLNSet=tLAChargeToNxsLNDB.executeQuery(tsql);
            
            if(ttLAChargeToNxsLNSet.size()<=0)
            {
            CError tError = new CError();
            tError.moduleName = "LAChargeToNxsLNTBL";
            tError.functionName = "dealData";
            tError.errorMessage = "机构:"+tLAChargeToNxsLNSchema.getAgentCom()+"月份:"+mIndexCalNo+"没有手续费信息!";
            this.mErrors.addOneError(tError);
            return false;
            }
            for(int j = 1; j <= ttLAChargeToNxsLNSet.size(); j++){
            	LAChargeToNxsLNSchema ttLAChargeToNxsLNSchema = new LAChargeToNxsLNSchema();
            	ttLAChargeToNxsLNSchema = ttLAChargeToNxsLNSet.get(j);
            	ttLAChargeToNxsLNSchema.setState("1");
            	ttLAChargeToNxsLNSchema.setOperator(mCheck1);
            	ttLAChargeToNxsLNSchema.setbak1(this.currentDate);
                tLAChargeToNxsLNSet.add(ttLAChargeToNxsLNSchema);
            }    
      }
      return true;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
         this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                     getObjectByObjectName("GlobalInput", 0));
         mGetCessData = (TransferData) cInputData.getObjectByObjectName(
	                "TransferData", 0);
         this.mLAChargeToNxsLNSet.set((LAChargeToNxsLNSet) cInputData.
                                            getObjectByObjectName(
                 "LAChargeToNxsLNSet", 0));
         ExeSQL tExeSQL = new ExeSQL(); 
     	 try{
             mIndexCalNo = (String) mGetCessData.getValueByName("IndexCalNo");
             mManageCom = (String) mGetCessData.getValueByName("ManageCom");
             System.out.println(mIndexCalNo);
             System.out.println(mManageCom);
	            String startSql= "select startdate from lastatsegment where stattype='5' and yearmonth = int ('"+mIndexCalNo+"') with ur ";
     	    SSRS tSSRS1 = tExeSQL.execSQL(startSql);
	            mStartDate = tSSRS1.GetText(1, 1);
	            String endSql= "select enddate from lastatsegment where stattype='5' and yearmonth = int ('"+mIndexCalNo+"') with ur ";
     	    SSRS tSSRS2 = tExeSQL.execSQL(endSql);
	            mEndDate = tSSRS2.GetText(1, 1);
     	}catch(Exception e){
     		buildError("LAChargeToNxsLNBL.java","获取输入信息失败，日期错误");
     		return false;
     	}
         this.mCheck1=mGlobalInput.Operator;
        return true;
    }

    private boolean prepareOutputData()
    {

        mMap.put(this.tLAChargeToNxsLNSet, "UPDATE");
        mInputData.add(mMap);
        return true;

    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LAChargeToNxsLNTBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

}
