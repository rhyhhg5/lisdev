package com.sinosoft.lis.agentquery;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: </p>
 * @author Alse
 * @version 1.0
 */
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class LAChargeToNxsLNTUI
{

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();


    public LAChargeToNxsLNTUI()
    {
    }

    public boolean submitData(VData cInputData)
    {
        mInputData = (VData) cInputData.clone();
        LAChargeToNxsLNTBL tLAChargeToNxsLNTBL = new LAChargeToNxsLNTBL();
        if (!tLAChargeToNxsLNTBL.submitData(mInputData))
        {
            this.mErrors.copyAllErrors(tLAChargeToNxsLNTBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "tLAChargeToNxsLNTUI";
            tError.functionName = "submitData";
            tError.errorMessage = tLAChargeToNxsLNTBL.mErrors.getFirstError();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
