package com.sinosoft.lis.agentquery;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentprint.LISComparator;
import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GZWageQueryListBL {
    public GZWageQueryListBL() {
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
   public CErrors mErrors = new CErrors();
   private VData mResult = new VData();
   /** 全局变量 */
   private GlobalInput mGlobalInput = new GlobalInput() ;
   private String mManageCom="";
   private String mWageNo= "";
   private String mContNo = "";
   private String mRiskCode= "";
   private String mAgentCode = "";
   private String mAgentName= "";
   private String mManageName = "";

   private VData mInputData = new VData();
   private String mOperate = "";
   private SSRS mSSRS1 = new SSRS();
   private XmlExport mXmlExport = null;
   private PubFun mPubFun = new PubFun();
   private ListTable mListTable = new ListTable();
   private TransferData mTransferData = new TransferData();


   /**
  * 传输数据的公共方法
  */
 public boolean submitData(VData cInputData, String cOperate)
 {

   mOperate = cOperate;
   mInputData = (VData) cInputData;
   if (mOperate.equals("")) {
       this.bulidError("submitData", "数据不完整");
       return false;
   }

   if (!mOperate.equals("PRINT")) {
       this.bulidError("submitData", "数据不完整");
       return false;
   }

   // 得到外部传入的数据，将数据备份到本类中
     if (!getInputData(mInputData)) {
         return false;
     }

     // 进行数据查询
  if (!dealdate()) {
      return false;
  }
  System.out.println("dayin");

  //进行数据打印
  if (!getPrintData()) {
      this.bulidError("getPrintData", "查询数据失败！");
      return false;
  }
System.out.println("dayinchenggong1232121212121");

     return true;
 }

 /**
   * 取得传入的数据
   * @return boolean
   */
  private boolean getInputData(VData cInputData)
  {

      try
      {
          mGlobalInput.setSchema((GlobalInput) cInputData.
                                 getObjectByObjectName("GlobalInput", 0));
          mTransferData = (TransferData) cInputData.getObjectByObjectName(
                  "TransferData", 0);
           //页面传入的数据 三个
           this.mManageCom = (String) mTransferData.getValueByName("tManageCom");
           this.mWageNo = (String) mTransferData.getValueByName("tWageNo");
           this.mContNo = (String) mTransferData.getValueByName("tContNo");
           this.mRiskCode = (String) mTransferData.getValueByName("tRiskCode");
           this.mAgentCode = (String) mTransferData.getValueByName("tAgentCode");
           this.mAgentName = (String) mTransferData.getValueByName("tAgentName");
           System.out.println(mManageCom);

      } catch (Exception ex) {
          this.mErrors.addOneError("");
          return false;
      }

      return true;

}

/**
  * 获取打印所需要的数据
  * @param cFunction String
  * @param cErrorMsg String
  */
 private void bulidError(String cFunction, String cErrorMsg) {

     CError tCError = new CError();

     tCError.moduleName = "GZWageQueryListBL";
     tCError.functionName = cFunction;
     tCError.errorMessage = cErrorMsg;

     this.mErrors.addOneError(tCError);

}


/**
  * 业务处理方法
  * @return boolean
  */

 private boolean dealdate()
 {
    System.out.println("chuli1");
    //查询数据
    if (!getAgentNow())
    {
       return false;
    }

    return true;
}

/**
  * 查询数据
  * @return boolean
 */
 private boolean getAgentNow() {
     String strSQL1="";
     String strSQL2="";
     String strSQL3="";

     ExeSQL tExeSQL = new ExeSQL();
     String strSQL = "select a.branchattr aa,(select name from labranchgroup where branchattr=a.branchattr and branchtype='2' and branchtype2='01'),a.agentcode ab,b.name,(select agentgrade from latree bb where bb.agentcode=a.agentcode),a.contno ac,c.appntname,a.riskcode,(select riskname from lmrisk where lmrisk.riskcode=a.riskcode),a.transmoney,a.FYCRate,a.fyc,(select codename from ldcode where codetype='payintv' and int(code)=a.payintv),a.payyears,a.paycount,a.caldate,a.commisionsn,(select InDueFormDate from latree where a.agentcode=agentcode),(select nvl(sum(feevalue),0) from lcgrpfee where feecode='000002' and grppolno='a.grppolno'),p7 from LACommision a,LAAgent b,LCpol c  where  a.agentcode=b.agentcode and a.polno=c.polno  and a.grpcontno='00000000000000000000' and a.grpcontno=c.grpcontno     "
                  +" and a.managecom ='"+mManageCom+"' "
                  +" and  a.wageno='"+mWageNo+"'  and a.branchtype='2' and a.branchtype2='01'"
                  +" and b.branchtype='2' and b.branchtype2='01'"
                  + strSQL1
                  + strSQL2
                  + strSQL3

          +" union all select a.branchattr aa,(select name from labranchgroup where branchattr=a.branchattr and branchtype='2' and branchtype2='01'),a.agentcode ab,b.name,(select agentgrade from latree bb where bb.agentcode=a.agentcode),a.contno ac,c.appntname,a.riskcode,(select riskname from lmrisk where lmrisk.riskcode=a.riskcode),a.transmoney,a.FYCRate,a.fyc,(select codename from ldcode where codetype='payintv' and int(code)=a.payintv),a.payyears,a.paycount,a.caldate,a.commisionsn,(select InDueFormDate from latree where a.agentcode=agentcode),(select nvl(sum(feevalue),0) from lcgrpfee where feecode='000002' and grppolno='a.grppolno'),p7  from LACommision a,LAAgent b,LbPol c   where  a.agentcode=b.agentcode and a.polno=c.polno  and a.grpcontno='00000000000000000000' and a.grpcontno=c.grpcontno      "
          +" and a.managecom ='"+mManageCom+"' "
          +" and  a.wageno='"+mWageNo+"'  and a.branchtype='2' and a.branchtype2='01'"
          +" and b.branchtype='2' and b.branchtype2='01'"
          + strSQL1
          + strSQL2
          + strSQL3
          + " order by aa,ab,ac";

  if (mAgentCode != null && !mAgentCode.equals("")) {
      strSQL1= " and a.AgentCode = '" + mAgentCode + "%'";
            }
   if (mAgentName != null && !mAgentName.equals("")) {
      strSQL2= " and b.Name = '" + mAgentName + "%' ";
     }
   if (mRiskCode != null && !mRiskCode.equals("")) {
      strSQL3= " and a.RiskCode = '" + mRiskCode + "%'";
     }


     mSSRS1 = tExeSQL.execSQL(strSQL);
     System.out.println(strSQL);
     if (tExeSQL.mErrors.needDealError()) {
         CError tCError = new CError();
         tCError.moduleName = "MakeXMLBL";
         tCError.functionName = "creatFile";
         tCError.errorMessage = "查询XML数据出错！";
         this.mErrors.addOneError(tCError);
         return false;

     }
     if (mSSRS1.getMaxRow() <= 0) {
         CError tCError = new CError();
         tCError.moduleName = "MakeXMLBL";
         tCError.functionName = "creatFile";
         tCError.errorMessage = "没有符合条件的信息！";
         this.mErrors.addOneError(tCError);
         return false;
     }

     return true;
 }

  /**
   *
   * @return boolean
   */
  private boolean getPrintData() {
      TextTag tTextTag = new TextTag();
      mXmlExport = new XmlExport();
      //设置模版名称
      mXmlExport.createDocument("GZLAGRPWageQueryListB.vts", "printer");

      String tMakeDate = "";
      String tMakeTime = "";

      tMakeDate = mPubFun.getCurrentDate();
      tMakeTime = mPubFun.getCurrentTime();

      System.out.print("dayin252");
     if (!getManageName()) {
          return false;
      }
      tTextTag.add("MakeDate", tMakeDate);
      tTextTag.add("MakeTime", tMakeTime);
      tTextTag.add("tName", mManageName);
      tTextTag.add("StartDate",mWageNo);
      tTextTag.add("EndDate",mWageNo);

      System.out.println("1212121" + tMakeDate);
      if (tTextTag.size() < 1) {
          return false;
      }

      mXmlExport.addTextTag(tTextTag);

      String[] title = {"", "", "", "", "" ,"","" ,"","","","","","","","","","","",""};

      if (!getListTable()) {
          return false;
      }
      System.out.println("111");
      mXmlExport.addListTable(mListTable, title);
      System.out.println("121");
      mXmlExport.outputDocumentToFile("c:\\", "new1");
      this.mResult.clear();

      mResult.addElement(mXmlExport);

      return true;
  }

  /**
   * 查询列表显示数据
   * @return boolean
   */
  private boolean getListTable() {
      System.out.println("dayimboiap288");
      if (mSSRS1.getMaxRow() > 0) {
          for (int i = 1; i <= mSSRS1.getMaxRow(); i++) {
              String Info[] = new String[19];
              Info[0] = mSSRS1.GetText(i, 1);
              Info[1] = mSSRS1.GetText(i, 2);
              Info[2] = mSSRS1.GetText(i, 3);
              Info[3] = mSSRS1.GetText(i, 4);
              Info[4] = mSSRS1.GetText(i, 5);
              Info[5] = mSSRS1.GetText(i, 6);
              Info[6] = mSSRS1.GetText(i, 7);
              Info[7] = mSSRS1.GetText(i, 8);
              Info[8] = mSSRS1.GetText(i, 9);
              Info[9] = mSSRS1.GetText(i, 10);
              Info[10] = mSSRS1.GetText(i, 11);
              Info[11] = mSSRS1.GetText(i, 12);
              Info[12] = mSSRS1.GetText(i, 13);
              Info[13] = mSSRS1.GetText(i, 14);
              Info[14] = mSSRS1.GetText(i, 15);
              Info[15] = mSSRS1.GetText(i, 16);
              Info[16] = mSSRS1.GetText(i, 17);
              Info[17] = mSSRS1.GetText(i, 18);
              Info[18] = mSSRS1.GetText(i, 19);



              mListTable.add(Info);

              System.out.println(Info[3]);
              System.out.println("dayin305");
          }
           mListTable.setName("Order");
      } else {
          CError tCError = new CError();
          tCError.moduleName = "CreateXml";
          tCError.functionName = "creatFile";
          tCError.errorMessage = "没有符合条件的信息！";
          this.mErrors.addOneError(tCError);
          return false;
      }
      return true;
  }

  private boolean getManageName() {

      String sql = "select name from ldcom where comcode='" + mManageCom +
                   "'";

      SSRS tSSRS = new SSRS();

      ExeSQL tExeSQL = new ExeSQL();

      tSSRS = tExeSQL.execSQL(sql);

      if (tExeSQL.mErrors.needDealError()) {

          this.mErrors.addOneError("销售单位不存在！");

          return false;

      }

      if (mManageCom.equals("86")) {
          this.mManageName = "";
      } else {
          if(mManageCom.length()>4)
          {this.mManageName = tSSRS.GetText(1, 1) + "分公司";}
          else
          {this.mManageName = tSSRS.GetText(1, 1);}
      }

      return true;
  }


  /**
   * 获取打印所需要的数据
   * @param cFunction String
   * @param cErrorMsg String
   */


  /**
   *
   * @return VData
   */
  public VData getResult() {
      return mResult;
}

}
