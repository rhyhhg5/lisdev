package com.sinosoft.lis.agentquery;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.task.*;
import com.sinosoft.lis.operfee.IndiCancelAndDueFeeBL;
import java.util.Date;

public class HRDevelopFinishBL {
	
	public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    private GlobalInput mGlobalInput = null;

    private String mMessage = null; 
    
    public HRDevelopFinishBL(){}
    
    public boolean submitData(){
    	if(!dealDate()){
    		return false;
    	}
    	if(!submit()){
    		return false;
    	}
    	return true;
    }
    
    /**
     * 处理业务数据
     * @return boolean
     */
    public boolean dealDate(){
    	String insertSql = "";
    	String month = PubFun.getCurrentDate().substring(5, 7);
    	String date = PubFun.getCurrentDate().substring(8, 10);
    	//三级人力到2017-12-15截止
    	String tValidate ="12-16";
    	if("01".equals(month))
    	{
    		return false;
    	}
    	//不是月初不进行批处理
    	if(!date.equals("01")&&!(month+"-"+date).equals(tValidate)){
    		return false;
    	}
    	String querySql = "select a.ManageCom,a.Agentcode from laagent a,LADimission b "
				   		+ "where a.agentcode=b.agentcode and a.BranchType='1' and a.branchtype2='01' and  a.AgentState='02' and a.employdate >='2016-12-16' "
				   		+ "group by a.agentcode,a.ManageCom "
				   		+ "having  min(days(a.employdate)-days(b.DepartDate)) >= '181' "
				   		+ "union "
				   		+"select a.ManageCom,a.Agentcode from laagent a "
				   		+ "where  a.BranchType='1' and a.branchtype2='01' and  a.AgentState='02' and a.employdate <'2016-12-16' "
				   		+ "union "
				   		+ "select a.ManageCom,a.Agentcode from laagent a "
				   		+ "where a.BranchType='1' and  a.AgentState='01' and a.branchtype2='01' "
				   		+ "with ur";
    	ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL. execSQL(querySql);
		System.out.println("三级人力本次插入数据开始" + querySql);
		for(int i=1;i<=tSSRS.getMaxRow();i++){
			String Mngcom = tSSRS.GetText(i, 1);
			String Agentcode = tSSRS.GetText(i, 2);
			insertSql = "insert into LAhumanDesc(Mngcom,Year,Month,Agentcode,Operator,Makedate,Maketime,Modifydate,Modifytime) "
					  + "values('"+Mngcom+"',year(current date),substr(current date - 1 day,6,2),'"+Agentcode+"', " 
					  + "'001',current date,current time,current date,current time)";
			mMap.put(insertSql,"INSERT");
		}
		System.out.println("本次插入数据结束");
    	return true;
    }
    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit(){
    	System.out.println("本次提交数据开始");
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")){
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        System.out.println("本次提交数据结束");
        return true;
    }
    
    public static void main(String[] args) {
    	HRDevelopFinishBL hrbl = new HRDevelopFinishBL();
		hrbl.submitData();
	}
}
