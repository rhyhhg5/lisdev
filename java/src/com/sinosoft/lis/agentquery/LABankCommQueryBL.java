package com.sinosoft.lis.agentquery;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.FileWriter;
import java.io.*;

public class LABankCommQueryBL {
	/** 错误信息容器 */
	public CErrors mErrors = new CErrors();
	private GlobalInput mGI = null;
	private String mSql = null;
	private String mOutXmlPath = null;
	// private String mtype = null;
        private String mLevel = null;

	private String mCurDate = PubFun.getCurrentDate();
	private String mCurTime = PubFun.getCurrentTime();

	public LABankCommQueryBL() {
	}

	public boolean submitData(VData cInputData, String operate) {

		if (!getInputData(cInputData)) {
			return false;
		}
		if (!checkData()) {
			return false;
		}
		if (!dealData()) {
			return false;
		}
		return true;
	}

	/**
	 * 校验操作是否合法
	 *
	 * @return boolean
	 */
	private boolean checkData() {
		return true;
	}

	/**
	 * dealData 处理业务数据
	 *
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean dealData() {
		ExeSQL tExeSQL = new ExeSQL();
		System.out.println("BL->dealDate()");
		System.out.println(mSql);
		System.out.println(mOutXmlPath);
		SSRS tSSRS = tExeSQL.execSQL(mSql);

		if (tExeSQL.mErrors.needDealError()) {
			System.out.println(tExeSQL.mErrors.getErrContent());
			CError tError = new CError();
			tError.moduleName = "AgentWageXLSBL";
			tError.functionName = "dealData";
			tError.errorMessage = "没有查询到需要下载的数据";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
                String[][] mToExcel = new String[tSSRS.getMaxRow() + 3][70];
                if(mLevel.equals("0")){ // 分公司
		mToExcel[0][0] = "竞赛业绩统计报表（已过犹豫期）";
		mToExcel[1][0] = "统计日期：";
		mToExcel[1][1] = mCurDate;
		mToExcel[1][6] = "统计层级：分公司";
		mToExcel[1][8] = "单位：元";

		mToExcel[2][0] = "机构代码";
		mToExcel[2][1] = "分公司";
		mToExcel[2][2] = "所辖网点数量";
		mToExcel[2][3] = "出单网点数量";
		mToExcel[2][4] = "万能B保费";
		mToExcel[2][5] = "万能C基本保费";
		mToExcel[2][6] = "万能C追加保费";
		mToExcel[2][7] = "万能D保费";
		mToExcel[2][8] = "万能E保费";
		mToExcel[2][9] = "万能F保费";
		mToExcel[2][10] = "万能G保费";
		mToExcel[2][11] = "健康专家保费";
        mToExcel[2][12] = "附加健康专家保费";
        mToExcel[2][13] = "少儿险成长无忧保费";
        mToExcel[2][14] = "附加少儿险成长无忧保费";
        mToExcel[2][15] = "金利宝个人护理保险（万能型）";
        mToExcel[2][16] = "安心宝个人终身重疾";
        mToExcel[2][17] = "附加安心宝个人护理";
        mToExcel[2][18] = "康利人生两全保险（5年趸交）";
        mToExcel[2][19] = "康利人生两全保险（6年趸交）";
        mToExcel[2][20] = "康利人生两全保险（3年期交）";
        mToExcel[2][21] = "康利人生两全保险（5年期交）";
        mToExcel[2][22] = "附加康利人生护理保险(5年趸交)";
        mToExcel[2][23] = "附加康利人生护理保险(6年趸交)";
        mToExcel[2][24] = "附加康利人生护理保险(3年期交)";
        mToExcel[2][25] = "附加康利人生护理保险(5年期交)";
        mToExcel[2][26] = "健康人生个人护理保险（万能型，H款）";
        mToExcel[2][27] = "附加健康人生个人意外伤害保险（H款）";
        mToExcel[2][28] = "福满人生两全保险（趸交）";// add new 
        mToExcel[2][29] = "福满人生两全保险（3年期交）";
        mToExcel[2][30] = "福满人生两全保险（5年期交）";
        mToExcel[2][31] = "福满人生两全保险（10年期交）";
        mToExcel[2][32] = "福满人生两全保险（20年期交）";
        mToExcel[2][33] = "附加福满人生护理保险（趸交）";// add new 
        mToExcel[2][34] = "附加福满人生护理保险（3年期交）";
        mToExcel[2][35] = "附加福满人生护理保险（5年期交）";
        mToExcel[2][36] = "附加福满人生护理保险（10年期交）";
        mToExcel[2][37] = "附加福满人生护理保险（20年期交）";
        mToExcel[2][38] = "百万安行个人护理（5年期交）";
        mToExcel[2][39] = "百万安行个人护理（10年期交）";
        mToExcel[2][40] = "附加百万安行个人意外伤害（5年期交）";
        mToExcel[2][41] = "附加百万安行个人意外伤害（10年期交）";
        mToExcel[2][42] = "附加百万安行个人交通意外伤害（5年期交）";
        mToExcel[2][43] = "附加百万安行个人交通意外伤害（10年期交）";
        mToExcel[2][44] = "惠享连年个人护理保险（万能型.趸交）";
        mToExcel[2][45] = "附加惠享连年个人意外伤害保险";
        mToExcel[2][46] = "福利双全个人护理保险";
        mToExcel[2][47] = "附加福利双全个人意外伤害保险";
        mToExcel[2][48] = "康乐人生个人重大疾病保险(A款)";
        mToExcel[2][49] = "附加康乐人生个人护理保险（A款）";
        mToExcel[2][50] = "北肿防癌管家个人疾病保险（A款）";
        mToExcel[2][51] = "附加北肿防癌管家个人护理保险（A款）";
        mToExcel[2][52] = "守护天使少儿特定疾病保险";
        mToExcel[2][53] = "天使之翼少儿重大疾病保险";
        mToExcel[2][54] = "附加天使之翼少儿护理保险";
        mToExcel[2][55] = "福惠双全个人护理保险";
        mToExcel[2][56] = "附加福惠双全个人意外伤害保险 ";
        mToExcel[2][57] = "保费合计";
		

		for (int row = 1; row <= tSSRS.getMaxRow(); row++) {
			for (int col = 1; col <= tSSRS.getMaxCol(); col++) {
				mToExcel[row + 2][col - 1] = tSSRS.GetText(row, col);
			}
		  }
                }
                if(mLevel.equals("1")){ // 支公司
                mToExcel[0][0] = "竞赛业绩统计报表（已过犹豫期）";
                mToExcel[1][0] = "统计日期：";
                mToExcel[1][1] = mCurDate;
                mToExcel[1][6] = "统计层级：支公司";
                mToExcel[1][8] = "单位：元";

                mToExcel[2][0] = "机构代码";
                mToExcel[2][1] = "分公司";
                mToExcel[2][2] = "中支代码";
                mToExcel[2][3] = "中支名称";
                mToExcel[2][4] = "所辖网点数量";
                mToExcel[2][5] = "出单网点数量";
                mToExcel[2][6] = "万能B保费";
                mToExcel[2][7] = "万能C基本保费";
                mToExcel[2][8] = "万能C追加保费";
                mToExcel[2][9] = "万能D保费";
                mToExcel[2][10] = "万能E保费";
                mToExcel[2][11] = "万能F保费";
                mToExcel[2][12] = "万能G保费";
                mToExcel[2][13] = "健康专家保费";
                mToExcel[2][14] = "附加健康专家保费";
                mToExcel[2][15] = "少儿险成长无忧保费";
                mToExcel[2][16] = "附加少儿险成长无忧保费";
                mToExcel[2][17] = "金利宝个人护理保险（万能型）";
                mToExcel[2][18] = "安心宝个人终身重疾";
                mToExcel[2][19] = "附加安心宝个人护理";
                mToExcel[2][20] = "康利人生两全保险（5年趸交）";
                mToExcel[2][21] = "康利人生两全保险（6年趸交）";
                mToExcel[2][22] = "康利人生两全保险（3年期交）";
                mToExcel[2][23] = "康利人生两全保险（5年期交）";
                mToExcel[2][24] = "附加康利人生护理保险(5年趸交)";
                mToExcel[2][25] = "附加康利人生护理保险(6年趸交)";
                mToExcel[2][26] = "附加康利人生护理保险(3年期交)";
                mToExcel[2][27] = "附加康利人生护理保险(5年期交)";
                mToExcel[2][28] = "健康人生个人护理保险（万能型，H款）";
                mToExcel[2][29] = "附加健康人生个人意外伤害保险（H款）";
                mToExcel[2][30] = "福满人生两全保险（趸交）";// add new 
                mToExcel[2][31] = "福满人生两全保险（3年期交）";
                mToExcel[2][32] = "福满人生两全保险（5年期交）";
                mToExcel[2][33] = "福满人生两全保险（10年期交）";
                mToExcel[2][34] = "福满人生两全保险（20年期交）";
                mToExcel[2][35] = "附加福满人生护理保险（趸交）";// add new 
                mToExcel[2][36] = "附加福满人生护理保险（3年期交）";
                mToExcel[2][37] = "附加福满人生护理保险（5年期交）";
                mToExcel[2][38] = "附加福满人生护理保险（10年期交）";
                mToExcel[2][39] = "附加福满人生护理保险（20年期交）";
                mToExcel[2][40] = "百万安行个人护理（5年期交）";
                mToExcel[2][41] = "百万安行个人护理（10年期交）";
                mToExcel[2][42] = "附加百万安行个人意外伤害（5年期交）";
                mToExcel[2][43] = "附加百万安行个人意外伤害（10年期交）";
                mToExcel[2][44] = "附加百万安行个人交通意外伤害（5年期交）";
                mToExcel[2][45] = "附加百万安行个人交通意外伤害（10年期交）";
                mToExcel[2][46] = "惠享连年个人护理保险（万能型.趸交）";
                mToExcel[2][47] = "附加惠享连年个人意外伤害保险";
                mToExcel[2][48] = "福利双全个人护理保险";
                mToExcel[2][49] = "附加福利双全个人意外伤害保险";
                mToExcel[2][50] = "康乐人生个人重大疾病保险(A款)";
                mToExcel[2][51] = "附加康乐人生个人护理保险（A款）";
                mToExcel[2][52] = "北肿防癌管家个人疾病保险（A款）";
                mToExcel[2][53] = "附加北肿防癌管家个人护理保险（A款）";
                mToExcel[2][54] = "守护天使少儿特定疾病保险";
                mToExcel[2][55] = "天使之翼少儿重大疾病保险";
                mToExcel[2][56] = "附加天使之翼少儿护理保险";
                mToExcel[2][57] = "福惠双全个人护理保险";
                mToExcel[2][58] = "附加福惠双全个人意外伤害保险 ";
                mToExcel[2][59] = "保费合计";

                for (int row = 1; row <= tSSRS.getMaxRow(); row++) {
                        for (int col = 1; col <= tSSRS.getMaxCol(); col++) {
                                mToExcel[row + 2][col - 1] = tSSRS.GetText(row, col);
                        }
                    }
                }
                
                if(mLevel.equals("2")){
                mToExcel[0][0] = "竞赛业绩统计报表（已过犹豫期）";
                mToExcel[1][0] = "统计日期：";
                mToExcel[1][1] = mCurDate;
                mToExcel[1][6] = "统计层级：营销组";
                mToExcel[1][8] = "单位：元";

                mToExcel[2][0] = "机构代码";
                mToExcel[2][1] = "分公司";
                mToExcel[2][2] = "中支代码";
                mToExcel[2][3] = "中支名称";
                mToExcel[2][4] = "团队代码";
                mToExcel[2][5] = "团队名称";
                mToExcel[2][6] = "所辖网点数量";
                mToExcel[2][7] = "出单网点数量";
                mToExcel[2][8] = "万能B保费";
                mToExcel[2][9] = "万能C基本保费";
                mToExcel[2][10] = "万能C追加保费";
                mToExcel[2][11] = "万能D保费";
                mToExcel[2][12] = "万能E保费";
                mToExcel[2][13] = "万能F保费";
                mToExcel[2][14] = "万能G保费";
                mToExcel[2][15] = "健康专家保费";
                mToExcel[2][16] = "附加健康专家保费";
                mToExcel[2][17] = "少儿险成长无忧保费";
                mToExcel[2][18] = "附加少儿险成长无忧保费";
                mToExcel[2][19] = "金利宝个人护理保险（万能型）";
                mToExcel[2][20] = "安心宝个人终身重疾";
                mToExcel[2][21] = "附加安心宝个人护理";
                mToExcel[2][22] = "康利人生两全保险（5年趸交）";
                mToExcel[2][23] = "康利人生两全保险（6年趸交）";
                mToExcel[2][24] = "康利人生两全保险（3年期交）";
                mToExcel[2][25] = "康利人生两全保险（5年期交）";
                mToExcel[2][26] = "附加康利人生护理保险(5年趸交)";
                mToExcel[2][27] = "附加康利人生护理保险(6年趸交)";
                mToExcel[2][28] = "附加康利人生护理保险(3年期交)";
                mToExcel[2][29] = "附加康利人生护理保险(5年期交)";
                mToExcel[2][30] = "健康人生个人护理保险（万能型，H款）";
                mToExcel[2][31] = "附加健康人生个人意外伤害保险（H款）";
                mToExcel[2][32] = "福满人生两全保险（趸交）";// add new 
                mToExcel[2][33] = "福满人生两全保险（3年期交）";
                mToExcel[2][34] = "福满人生两全保险（5年期交）";
                mToExcel[2][35] = "福满人生两全保险（10年期交）";
                mToExcel[2][36] = "福满人生两全保险（20年期交）";
                mToExcel[2][37] = "附加福满人生护理保险（趸交）";// add new 
                mToExcel[2][38] = "附加福满人生护理保险（3年期交）";
                mToExcel[2][39] = "附加福满人生护理保险（5年期交）";
                mToExcel[2][40] = "附加福满人生护理保险（10年期交）";
                mToExcel[2][41] = "附加福满人生护理保险（20年期交）";
                mToExcel[2][42] = "百万安行个人护理（5年期交）";
                mToExcel[2][43] = "百万安行个人护理（10年期交）";
                mToExcel[2][44] = "附加百万安行个人意外伤害（5年期交）";
                mToExcel[2][45] = "附加百万安行个人意外伤害（10年期交）";
                mToExcel[2][46] = "附加百万安行个人交通意外伤害（5年期交）";
                mToExcel[2][47] = "附加百万安行个人交通意外伤害（10年期交）";
                mToExcel[2][48] = "惠享连年个人护理保险（万能型.趸交）";
                mToExcel[2][49] = "附加惠享连年个人意外伤害保险";
                mToExcel[2][50] = "福利双全个人护理保险";
                mToExcel[2][51] = "附加福利双全个人意外伤害保险";
                mToExcel[2][52] = "康乐人生个人重大疾病保险(A款)";
                mToExcel[2][53] = "附加康乐人生个人护理保险（A款）";
                mToExcel[2][54] = "北肿防癌管家个人疾病保险（A款）";
                mToExcel[2][55] = "附加北肿防癌管家个人护理保险（A款）";
                mToExcel[2][56] = "守护天使少儿特定疾病保险";
                mToExcel[2][57] = "天使之翼少儿重大疾病保险";
                mToExcel[2][58] = "附加天使之翼少儿护理保险";
                mToExcel[2][59] = "福惠双全个人护理保险";
                mToExcel[2][60] = "附加福惠双全个人意外伤害保险 ";
                mToExcel[2][61] = "保费合计";

                for (int row = 1; row <= tSSRS.getMaxRow(); row++) {
                        for (int col = 1; col <= tSSRS.getMaxCol(); col++) {
                                mToExcel[row + 2][col - 1] = tSSRS.GetText(row, col);
                        }
                   }
                }

                if(mLevel.equals("3")){
                mToExcel[0][0] = "竞赛业绩统计报表（已过犹豫期）";
                mToExcel[1][0] = "统计日期：";
                mToExcel[1][1] = mCurDate;
                mToExcel[1][6] = "统计层级：业务经理";
                mToExcel[1][20] = "单位：元";

                mToExcel[2][0] = "机构代码";
                mToExcel[2][1] = "分公司";
                mToExcel[2][2] = "中支代码";
                mToExcel[2][3] = "中支名称";
                mToExcel[2][4] = "团队代码";
                mToExcel[2][5] = "团队名称";
                mToExcel[2][6] = "业务员代码";
                mToExcel[2][7] = "业务员姓名";
                mToExcel[2][8] = "职级名称";
                mToExcel[2][9] = "入司时间";
                mToExcel[2][10] = "在职状态";
                mToExcel[2][11] = "离职日期";
                mToExcel[2][12] = "季度考核保费标准";

                mToExcel[2][13] = "所辖网点数量";
                mToExcel[2][14] = "出单网点数量";
                mToExcel[2][15] = "万能B保费";
                mToExcel[2][16] = "万能C基本保费";
                mToExcel[2][17] = "万能C追加保费";
                mToExcel[2][18] = "万能D保费";
                mToExcel[2][19] = "万能E保费";
                mToExcel[2][20] = "万能F保费";
                mToExcel[2][21] = "万能G保费";
                mToExcel[2][22] = "健康专家保费";
                mToExcel[2][23] = "附加健康专家保费";
                mToExcel[2][24] = "少儿险成长无忧保费";
                mToExcel[2][25] = "附加少儿险成长无忧保费";
                mToExcel[2][26] = "金利宝个人护理保险（万能型）";
                mToExcel[2][27] = "安心宝个人终身重疾";
                mToExcel[2][28] = "附加安心宝个人护理保险";
                mToExcel[2][29] = "康利人生两全保险（5年趸交）";
                mToExcel[2][30] = "康利人生两全保险（6年趸交）";
                mToExcel[2][31] = "康利人生两全保险（3年期交）";
                mToExcel[2][32] = "康利人生两全保险（5年期交）";
                mToExcel[2][33] = "附加康利人生护理保险(5年趸交)";
                mToExcel[2][34] = "附加康利人生护理保险(6年趸交)";
                mToExcel[2][35] = "附加康利人生护理保险(3年期交)";
                mToExcel[2][36] = "附加康利人生护理保险(5年期交)";
                mToExcel[2][37] = "健康人生个人护理保险（万能型，H款）";
                mToExcel[2][38] = "附加健康人生个人意外伤害保险（H款）";
                mToExcel[2][39] = "福满人生两全保险（趸交）";// add new 
                mToExcel[2][40] = "福满人生两全保险（3年期交）";
                mToExcel[2][41] = "福满人生两全保险（5年期交）";
                mToExcel[2][42] = "福满人生两全保险（10年期交）";
                mToExcel[2][43] = "福满人生两全保险（20年期交）";
                mToExcel[2][44] = "附加福满人生护理保险（趸交）";// add new 
                mToExcel[2][45] = "附加福满人生护理保险（3年期交）";
                mToExcel[2][46] = "附加福满人生护理保险（5年期交）";
                mToExcel[2][47] = "附加福满人生护理保险（10年期交）";
                mToExcel[2][48] = "附加福满人生护理保险（20年期交）";
                mToExcel[2][49] = "百万安行个人护理（5年期交）";
                mToExcel[2][50] = "百万安行个人护理（10年期交）";
                mToExcel[2][51] = "附加百万安行个人意外伤害（5年期交）";
                mToExcel[2][52] = "附加百万安行个人意外伤害（10年期交）";
                mToExcel[2][53] = "附加百万安行个人交通意外伤害（5年期交）";
                mToExcel[2][54] = "附加百万安行个人交通意外伤害（10年期交）";
                mToExcel[2][55] = "惠享连年个人护理保险（万能型.趸交）";
                mToExcel[2][56] = "附加惠享连年个人意外伤害保险";
                mToExcel[2][57] = "福利双全个人护理保险";
                mToExcel[2][58] = "附加福利双全个人意外伤害保险";
                mToExcel[2][59] = "康乐人生个人重大疾病保险(A款)";
                mToExcel[2][60] = "附加康乐人生个人护理保险（A款）";
                mToExcel[2][61] = "北肿防癌管家个人疾病保险（A款）";
                mToExcel[2][62] = "附加北肿防癌管家个人护理保险（A款）";
                mToExcel[2][63] = "守护天使少儿特定疾病保险";
                mToExcel[2][64] = "天使之翼少儿重大疾病保险";
                mToExcel[2][65] = "附加天使之翼少儿护理保险";
                mToExcel[2][66] = "福惠双全个人护理保险";
                mToExcel[2][67] = "附加福惠双全个人意外伤害保险 ";
                mToExcel[2][68] = "保费合计";

                for (int row = 1; row <= tSSRS.getMaxRow(); row++) {
                   for (int col = 1; col <= tSSRS.getMaxCol(); col++) {
                      mToExcel[row + 2][col - 1] = tSSRS.GetText(row, col);
                      }
                  }
                }
		try {
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = { PubFun.getCurrentDate() };
			t.addSheet(sheetName);
			t.setData(0, mToExcel);
			t.write(mOutXmlPath);
		} catch (Exception ex) {
			ex.toString();
			ex.printStackTrace();
		}
		return true;
	}

	/**
	 * getInputData 将外部传入的数据分解到本类的属性中
	 *
	 * @param cInputData
	 *            VData：submitData中传入的VData对象
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean getInputData(VData data) {
		mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);
		if (mGI == null || tf == null) {
			CError tError = new CError();
			tError.moduleName = "LABankCommQueryBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}

		mSql = (String) tf.getValueByName("querySql");
		mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        mLevel = (String) tf.getValueByName("tLevel");
		if (mSql == null || mOutXmlPath == null) {
			CError tError = new CError();
			tError.moduleName = "LABankCommQueryBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整2";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		LABankCommQueryBL LABankCommQueryBL = new LABankCommQueryBL();
	}
}
