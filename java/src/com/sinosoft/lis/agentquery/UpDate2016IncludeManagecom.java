package com.sinosoft.lis.agentquery;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class UpDate2016IncludeManagecom {
	
	public CErrors mErrors = new CErrors();

    private MMap mMap = new MMap();

    public UpDate2016IncludeManagecom(){}
    
    public boolean submitData(){
    	if(!dealDate()){
    		return false;
    	}
    	if(!submit()){
    		return false;
    	}
    	return true;
    }
    
    /**
     * 处理业务数据
     * @return boolean
     */
    public boolean dealDate(){
    	String insertSql = "";
    	String month = PubFun.getCurrentDate().substring(5, 7);
    	String date = PubFun.getCurrentDate().substring(8, 10);
    	//三级人力到2016-12-15截止
    	String tValidate ="12-16";
//    	if("01".equals(month))
//    	{
//    		return false;
//    	}
    	//不是月初不进行批处理
    	if(!date.equals("01")&&!(month+"-"+date).equals(tValidate)){
    		return false;
    	}
    	String querySql = "select   substr(comcode,1,6)||'00',comcode,(select name from ldcom where comcode =substr(a.comcode,1,6)||'00'),name" +
    			" from ldcom a where 1=1 " +
    			" and substr(a.comcode,1,6) in (select substr(code,1,6) from ldcode where codetype =year(current date)||'humandeveidesc'  and int(substr(code,7,2))=0)" +
    			" and sign='1' and int(substr(trim(a.comcode),7,2))>0 and length(trim(a.comcode))=8 " +
    			" and not exists(select 1 from ldcode1 b where codetype =year(current date)||'includemanagecom' and b.code =substr(a.comcode,1,6)||'00' and a.comcode = b.code1 and  length(trim(a.comcode))=8)" +
    			" and exists(select 1 from laagent b where branchtype='1' and branchtype2='01' and a.comcode = b.managecom and b.agentstate in ('01','02'))"+
    			" with ur";
    	ExeSQL tExeSQL = new ExeSQL();
    	System.out.println("本次准备数据开始"+querySql);
		SSRS tSSRS = tExeSQL. execSQL(querySql);
		
		if(null!=tSSRS&&0<tSSRS.getMaxRow())
		{
			for(int i=1;i<=tSSRS.getMaxRow();i++){
				String Mngcom1 = tSSRS.GetText(i, 1);
				String Mngcom2 = tSSRS.GetText(i, 2);
				String Mngcom1Name = tSSRS.GetText(i, 3);
				String Mngcom2Name = tSSRS.GetText(i, 4);
				insertSql = "insert into ldcode1(codetype,code,code1,codename,codealias,othersign,RiskWrapPlanName) "
					+ "values(year(current date)||'includemanagecom','"+Mngcom1+"','"+Mngcom2+"','"+Mngcom1Name+"','"+Mngcom2Name+"',year(current date),to_char(current date || current time))";
				mMap.put(insertSql,"INSERT");
				System.out.println(insertSql);
				String tJudgeSQL=" select 1 from ldcode1 b where b.codetype =year(current date)||'includemanagecom' and b.code ='"+Mngcom1+"' and b.code1='"+Mngcom1+"' with ur";
				String tJudgeFlag = tExeSQL.getOneValue(tJudgeSQL);
				if("".equals(tJudgeFlag)||null ==tJudgeFlag)
				{
					insertSql = "insert into ldcode1(codetype,code,code1,codename,codealias,othersign,RiskWrapPlanName) "
						+ "values(year(current date)||'includemanagecom','"+Mngcom1+"','"+Mngcom1+"','"+Mngcom1Name+"','"+Mngcom1Name+"',year(current date),to_char(current date || current time))";
					mMap.put(insertSql,"INSERT");
				}
			}
			System.out.println("UpDate2016IncludeManagecom：本次准备数据结束，满足条件的数据量为："+tSSRS.getMaxRow());
		}
		else
		{
			System.out.println("UpDate2016IncludeManagecom：本次准备数据结束，没有满足条件的数据");
		}
    	return true;
    }
    /**
     * 提交数据到数据库
     * @return boolean
     */
    private boolean submit(){
    	System.out.println("本次提交数据开始");
        VData data = new VData();
        data.add(mMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, "")){
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        System.out.println("本次提交数据结束");
        return true;
    }
    
    public static void main(String[] args) {
    	UpDate2016IncludeManagecom hrbl = new UpDate2016IncludeManagecom();
		hrbl.submitData();
    	
	}
}
