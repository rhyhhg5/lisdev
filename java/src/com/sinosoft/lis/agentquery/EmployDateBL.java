/*
 * <p>ClassName: LABranchGroupBuildBL </p>
 * <p>Description: LABranchGroupBuildBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2013-05-17
 */
package com.sinosoft.lis.agentquery;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LARearRelationDB;
import com.sinosoft.lis.db.LARecomRelationDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LARearRelationSchema;
import com.sinosoft.lis.schema.LARecomRelationSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LARearRelationSet;
import com.sinosoft.lis.vschema.LARecomRelationSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Company: Sinosoft</p>
 * @author yangjian
 * @version 1.0
 * 
 *
 */
public class EmployDateBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap mMMap = new MMap();
    
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局变量 */
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAAgentSet mLAAgentSet = new LAAgentSet(); 
    /** 当前日期*/
	private String tCurrentDate = PubFun.getCurrentDate();
	private String tCurrentTime = PubFun.getCurrentTime();
    
    public EmployDateBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
    	mOperate = cOperate;
	    //得到外部传入的数据,将数据备份到本类中
	    if (!getInputData(cInputData))
	    {
	        return false;
	    }
	    //进行业务处理
	    if (!dealData())
	    {
	        // @@错误处理
	        CError tError = new CError();
	        tError.moduleName = "EmployDateBL";
	        tError.functionName = "DealData";
	        tError.errorMessage = "数据处理失败EmployDateBL-->dealData!";
	        this.mErrors.addOneError(tError);
	        return false;
	    }
	    try {
	    	PubSubmit tPubSubmit = new PubSubmit();
	    	if(!tPubSubmit.submitData(this.mInputData, "UPDATE")){
	    		CError tError = new CError();
	    		tError.moduleName = "OutWorkDateBL";
	 	        tError.functionName = "submitData";
	    		tError.errorMessage = "数据更新失败";
	    		this.mErrors.addOneError(tError);
		        return false;
	    	}
			
		} catch (Exception e) {
			CError tError = new CError();
	        tError.moduleName = "EmployDateBL";
	        tError.functionName = "submitData";
	        tError.errorMessage = "数据处理失败EmployDateBL-->dealData!";
	        this.mErrors.addOneError(tError);
	        return false;
		}
	    return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {   
    	LAAgentSet tUpdateLAAgentSet = new LAAgentSet();
    	LATreeSet tUpdateLATreeSet = new LATreeSet();
    	LARecomRelationSet tUpdateLARecomRelationSet = new LARecomRelationSet();
    	LARearRelationSet tUpdateLARearRelationSet = new LARearRelationSet();
        //设置基础数据
        for (int i = 1; i <= this.mLAAgentSet.size(); i++)
        {   
        	mLAAgentSchema = mLAAgentSet.get(i);
        	String tAgentCode = mLAAgentSchema.getAgentCode();
        	String tEmployDate = mLAAgentSchema.getEmployDate();
          /*String tManageCom = mLAAgentSchema.getManageCom();
        	String tGroupAgentCode = mLAAgentSchema.getGroupAgentCode();
        	String tName = mLAAgentSchema.getName();
        	String tBranchType2 = mLAAgentSchema.getBranchType2();
        	String tBranchType = mLAAgentSchema.getBranchType();*/
        	
        	/**LAAgent人员基础信息表中EmployDate、Operator、ModifyDate、ModifyTime字段*/
        	LAAgentDB tLAAgentDB = new LAAgentDB();
        	tLAAgentDB.setAgentCode(tAgentCode);
          /*tLAAgentDB.setBranchType(tBranchType);
        	tLAAgentDB.setBranchType2(tBranchType2);
        	tLAAgentDB.setManageCom(tManageCom);
        	tLAAgentDB.setGroupAgentCode(tGroupAgentCode);
        	tLAAgentDB.setName(tName);*/

        	LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        	LAAgentSet tLAAgentSet = new LAAgentSet();
        	
        	tLAAgentSet = tLAAgentDB.query();
        	if(tLAAgentSet != null && tLAAgentSet.size() != 0){
        		tLAAgentSchema = tLAAgentSet.get(1);
            	
            	tLAAgentSchema.setEmployDate(tEmployDate);
            	tLAAgentSchema.setOperator("EmployDate");
            	tLAAgentSchema.setModifyDate(tCurrentDate);
            	tLAAgentSchema.setModifyTime(tCurrentTime);
            	tUpdateLAAgentSet.add(tLAAgentSchema);
        	}
        	
        	
        	
        	
        	
        	/**LATree职级表中IntroCommStart、AstartDate、StartDate字段*/
        	LATreeDB tLATreeDB = new LATreeDB();
        	tLATreeDB.setAgentCode(tAgentCode);
        	
        	LATreeSchema tLATreeSchema = new LATreeSchema();
        	LATreeSet tLATreeSet = new LATreeSet();
        	
        	tLATreeSet = tLATreeDB.query();
        	if(tLATreeSet != null && tLATreeSet.size() != 0){
        		tLATreeSchema = tLATreeSet.get(1);
            	
            	tLATreeSchema.setIntroCommStart(tEmployDate);
          	    tLATreeSchema.setAstartDate(tEmployDate);
          	    tLATreeSchema.setStartDate(tEmployDate);
          	    tUpdateLATreeSet.add(tLATreeSchema);
        	}
        	
        	
        	
        	
        	
        	/**LARecomRelation推荐关系表中StartDate字段*/
        	LARecomRelationDB tLARecomRelationDB = new LARecomRelationDB();
        	tLARecomRelationDB.setAgentCode(tAgentCode);
        	
        	LARecomRelationSchema tLARecomRelationSchema = new LARecomRelationSchema();
        	LARecomRelationSet tLARecomRelationSet = new LARecomRelationSet();
        	
        	tLARecomRelationSet = tLARecomRelationDB.query();
        	if(tLARecomRelationSet != null && tLARecomRelationSet.size() != 0){
        		tLARecomRelationSchema = tLARecomRelationSet.get(1);
            	
            	tLARecomRelationSchema.setStartDate(tEmployDate);
            	tUpdateLARecomRelationSet.add(tLARecomRelationSchema);
        	}
        	     	
        	
        	
        	
        	/**LARearRelation推荐关系表中startDate字段*/
        	LARearRelationDB tLARearRelationDB = new LARearRelationDB();
        	tLARearRelationDB.setAgentCode(tAgentCode);
        	
        	LARearRelationSchema tLARearRelationSchema = new LARearRelationSchema();
        	LARearRelationSet tLARearRelationSet = new LARearRelationSet();
        	
        	tLARearRelationSet = tLARearRelationDB.query();
        	if(tLARearRelationSet != null && tLARearRelationSet.size() != 0){
        		tLARearRelationSchema = tLARearRelationSet.get(1);
            	tLARearRelationSchema.setstartDate(tEmployDate);
            	tUpdateLARearRelationSet.add(tLARearRelationSchema);  
        	}
        	
        	
        	
        }
        
        
        mMMap.put(tUpdateLAAgentSet,"UPDATE");
        mMMap.put(tUpdateLATreeSet,"UPDATE");
        mMMap.put(tUpdateLARecomRelationSet,"UPDATE");
        mMMap.put(tUpdateLARearRelationSet,"UPDATE");
        this.mInputData.add(mMMap);
        return true;
    	
    } 
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAAgentSet.set((LAAgentSet)cInputData.getObjectByObjectName("LAAgentSet", 0));
        this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput", 0));
        if (this.mGlobalInput == null)
        {
            CError tError = new CError();
            tError.moduleName = "EmployDateBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "mGlobalInput为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (null == this.mLAAgentSet||0==this.mLAAgentSet.size())
        {
            CError tError = new CError();
            tError.moduleName = "EmployDateBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "mLAAgentSchema为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

  
    public VData getResult()
    {
        return this.mResult;
    }
}

