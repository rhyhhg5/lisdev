package com.sinosoft.lis.bq;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;

import java.util.HashMap;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.1
 */
public class GEdorMJDetailBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private LPGrpPolSchema mLPGrpPolSchema = null;

    private LPGrpContSchema mLPGrpContSchema = null;

    private LPPolSet mLPPolSet = null;

    private LPInsuredSet mLPInsuredSet = null;
    private LPDiskImportSet mLPDiskImportSet = null;

    private Reflections mReflections = new Reflections();
    private TransferData returnTransferData = null;
    private EdorItemSpecialData mEdorItemSpecialData = null;
    private String mCInValidate = null;

    private MMap map = new MMap();

    public GEdorMJDetailBL()
    {
    }

    /**
     * 公共的数据提交方法
     * @param cInputData VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String operate)
    {
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        prepareOutputData();

        if (!submit())
        {
            return false;
        }

        return true;
    }

    private boolean checkData()
    {
        LPEdorEspecialDataDB tLPEdorEspecialDataDB = new LPEdorEspecialDataDB();
        tLPEdorEspecialDataDB.setEdorAcceptNo(mLPGrpPolSchema.getEdorNo());
        tLPEdorEspecialDataDB.setEdorNo(mLPGrpPolSchema.getEdorNo());
        tLPEdorEspecialDataDB.setEdorType(mLPGrpPolSchema.getEdorType());
        tLPEdorEspecialDataDB.setDetailType(BQ.DETAILTYPE_MJSTATE);
        tLPEdorEspecialDataDB.setPolNo(mLPGrpPolSchema.getGrpPolNo());
        if(tLPEdorEspecialDataDB.getInfo())
        {
            if(!tLPEdorEspecialDataDB.getEdorValue().equals(BQ.MJSTATE_INIT))
            {
                mErrors.addOneError("任务不是未处理，不能进行满期理算，若您需要重复理算，"
                                    + "请先进行满期理算回退操作。");
                return false;
            }

            return true;
        }

        tLPEdorEspecialDataDB.setPolNo(BQ.FILLDATA);
        if (!tLPEdorEspecialDataDB.getInfo())
        {
            mErrors.addOneError("满期处理状态有问题。");
            return false;
        }

        if (!tLPEdorEspecialDataDB.getEdorValue().equals(BQ.MJSTATE_INIT))
        {
            mErrors.addOneError("任务不是未处理，不能进行满期理算，若您需要重复理算，"
                                + "请先进行满期理算回退操作。");
            return false;
        }

        //若前面还有保全项目为完成，则不可做满期理算
        if (hasEdorItemDoing())
        {
            return false;
        }

        //若正在理赔，则不可做满期理算
        if (checkClaiming())
        {
            return false;
        }

        return true;
    }

    /**
     * 校验保单险种是否正在理赔 qulq modify 强校验
     * @return boolean,正在理赔: true
     */
    private boolean checkClaiming()
    {
        LGWorkDB tLGWorkDB = new LGWorkDB();
        tLGWorkDB.setWorkNo(mLPGrpPolSchema.getEdorNo());
        tLGWorkDB.getInfo();
/*
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpContNo(tLGWorkDB.getContNo());
       LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();

        TransferData tTransferData = new TransferData();
        EdorConnectToClaim tEdorConnectToClaim =
               new EdorConnectToClaim(tTransferData, new GlobalInput());

       tTransferData.setNameAndValue("calFlag", "1");

        for (int i = 1; i <= tLCGrpPolSet.size(); i++)
        {
            tTransferData.setNameAndValue("grpPolNo",
                                          tLCGrpPolSet.get(i).getGrpPolNo());
            tEdorConnectToClaim.getGrpPolClaimState();
        }
        LCPolSet claimedLCPolSet = tEdorConnectToClaim.getClaimedLCPolSet();
        if (claimedLCPolSet.size() > 0)
        {
            //若被保人有多个险种发生理赔，则只显示一个
            HashMap claimingInsuredNoHashMap = new HashMap();
            String insuredNames = "";
            for (int i = 1; i <= claimedLCPolSet.size(); i++)
            {
                String name = (String) claimingInsuredNoHashMap
                              .get(claimedLCPolSet.get(i).getInsuredNo());
                if (name == null)
                {
                    name = claimedLCPolSet.get(i).getInsuredName();
                    insuredNames += name + ", ";
                    claimingInsuredNoHashMap.put(
                            claimedLCPolSet.get(i).getInsuredNo(), name);
                }
            }

            mErrors.addOneError("被保人 " + insuredNames + "正在理赔。");
            return true;
        }
*/
        String sql = "select rgtno from llregister where rgtobjno='"+tLGWorkDB.getContNo()
                     +"' and rgtstate not in ('04','05') and declineflag is null ";
	ExeSQL temp = new ExeSQL();

        SSRS rs = temp.execSQL(sql);
        if(rs==null||rs.getMaxRow()==0)
        {
            LCInsureAccTraceDB db = new LCInsureAccTraceDB();
            db.setGrpContNo(tLGWorkDB.getContNo());
            db.setState("temp");
            LCInsureAccTraceSet tempSet = db.query();
            if(tempSet ==null || tempSet.size()==0)
            {
                return false;
            }
            else
            {
                mErrors.addOneError("帐户轨迹表中有理赔临时记录，不能操作");
                return true;
            }
        }
        else
        {
            StringBuffer tem = new StringBuffer();
            tem.append("该单正在理赔或有未撤件的理赔申请，不能操作,批次号");
            for(int i = 1;i<=rs.getMaxRow();i++)
            {
                tem.append(rs.GetText(i,1));
                tem.append("  ");
            }
            mErrors.addOneError(tem.toString());
            return true;
        }

    }

    /**
     * 校验是否有未结案的保全项目
     * @return boolean：有未结案的保全项目
     */
    private boolean hasEdorItemDoing()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select * ")
                .append("from LPEdorApp a, LPGrpEdorItem b ")
                .append("where a.edorAcceptNo = b.edorAcceptNo ")
                .append("   and b.grpContNo = '")
                .append(mLPGrpPolSchema.getGrpContNo())
                .append("'  and a.edorState != '")
                .append(BQ.EDORSTATE_CONFIRM)
                .append("' ");

        LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
        LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.executeQuery(
                sql.toString());
        if (tLPGrpEdorItemSet.size() > 0)
        {
            String errMsg = "";
            for (int i = 1; i <= tLPGrpEdorItemSet.size(); i++)
            {
                errMsg += tLPGrpEdorItemSet.get(i).getEdorAcceptNo() + ", ";
            }
            mErrors.addOneError("该保单还有受理号为"
                                + errMsg.substring(
                                        0, errMsg.lastIndexOf(",") - 1)
                                + "的保全受理未处理完毕，请先处理以上受理。");
            return true;
        }

        return false;
    }

    private void prepareOutputData()
    {
        map.put(mEdorItemSpecialData.getSpecialDataSet(), "DELETE&INSERT");

        setMJState();
    }

    /**
     * 存储数据
     * @return boolean
     */
    private boolean submit()
    {
        VData tVData = new VData();
        tVData.add(map);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(tVData, ""))
        {
            mErrors.copyAllErrors(p.mErrors);
            return false;
        }

        return true;
    }

    /**
     * 处理数据，为存入后台做准备
     * @return boolean
     */
    private boolean dealData()
    {
        if (!getLPGrpPolInfo())
        {
            return false;
        }

        if (!getLPGrpContInfo())
        {
            return false;
        }

        if (!getLPPolInfo())
        {
            return false;
        }

        if (!getLPInsuredInfo())
        {
            return false;
        }

        if (!getLPInsureAccInfo())
        {
            return false;
        }

        if (!calAccInterest())
        {
            return false;
        }

        updateLPDiskMoney();

        return true;
    }

    private void updateLPDiskMoney()
    {
        String sql = "update LPDiskImport a "
                     + "set Money = "
                     + "      (select Abs(Sum(GetMoney)) from LJSGetEndorse "
                     + "      where EndorsementNo = '"
                     + mLPGrpPolSchema.getEdorNo() + "' "
                     + "         and FeeOperationType = '"
                     + mLPGrpPolSchema.getEdorType() + "' "
                     + "         and FeeFinaType != 'LX' "
                     + "         and GrpContNo = a.GrpContNo "
                     + "         and InsuredNo = a.InsuredNo "
                     + "      ), "
                     + "   GetMoney = "
                     + "      (select Abs(Sum(GetMoney)) from LJSGetEndorse "
                     + "      where EndorsementNo = '"
                     + mLPGrpPolSchema.getEdorNo() + "' "
                     + "         and FeeOperationType = '"
                     + mLPGrpPolSchema.getEdorType() + "' "
                     + "         and GrpContNo = a.GrpContNo "
                     + "         and InsuredNo = a.InsuredNo "
                     + "      ) "
                     + "where EdorNo = '" + mLPGrpPolSchema.getEdorNo() + "' "
                     + "   and EdorType = '"
                     + mLPGrpPolSchema.getEdorType() + "' "
                     + "   and GrpContNo = '"
                     + mLPGrpPolSchema.getGrpContNo() + "' ";
        map.put(sql, SysConst.UPDATE);

    }

    /**
     * 备份账户保全信息
     * @return boolean
     */
    private boolean getLPInsureAccInfo()
    {
        String sql = "delete from LPInsureAcc "
                     + "where EdorNo = '" + mLPGrpPolSchema.getEdorNo() + "' "
                     + "   and EdorType = '"
                     + mLPGrpPolSchema.getEdorType() + "' "
                     + "   and GrpPolNo = '"
                     + mLPGrpPolSchema.getGrpPolNo() + "' ";
        map.put(sql, "INSERT");

        sql = "insert into LPInsureAcc (select '"
              + mLPGrpPolSchema.getEdorNo()
              + "', '" + mLPGrpPolSchema.getEdorType()
              + "', LCInsureAcc" + ".* from LCInsureAcc "
              + "where GrpPolNo = '" + mLPGrpPolSchema.getGrpPolNo() + "')";
        map.put(sql, "INSERT");

        return true;
    }

    /**
     * 结算账户利息
     * @return boolean
     */
    private boolean calAccInterest()
    {
        String rateType
            = mEdorItemSpecialData.getEdorValue(BQ.DETAILTYPE_ENDTIME_RATETYPE);
        String rate = mEdorItemSpecialData.getEdorValue(BQ.DETAILTYPE_ACCRATE);
        if(rateType == null || rateType.equals("")
           || rate == null || rate.equals(""))
        {
            mErrors.addOneError("请录入利息/利率。");
            return false;
        }
        double accRate = Double.parseDouble(rate);  //利息/利率


        String feeType = getFeeType();  //财务类型

        //若录入了利息，则不用条用利息计算公式计算利息
        if(rateType.equals(BQ.DEALTYPE_INTERESTINPUT))
        {
            setPublicAccLX(accRate);
        }


        //按每个账户分类分别计算账户余额和利息，为续保生成账户做准备
        String grpInsureAccNo
            = CommonBL.getInsuAccNo("001", mLPGrpPolSchema.getRiskCode());
        String grpFixInsureAccNo
            = CommonBL.getInsuAccNo("004", mLPGrpPolSchema.getRiskCode());

        double sumInsuredInterest = 0;  //个人账户总利息
        double sumInsuredBala = 0;  //个人账户总余额
        String publicAccContNo = getPublicAccContNo(); //公共账户保单号

        AccountManage tAccountManage = new AccountManage();

        LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
        LCInsureAccClassSet tLCInsureAccClassSet = null;
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            tLCInsureAccClassDB.setGrpContNo(mLPDiskImportSet.get(i).getGrpContNo());
            tLCInsureAccClassDB.setInsuredNo(mLPDiskImportSet.get(i).getInsuredNo());
            tLCInsureAccClassDB.setGrpPolNo(mLPGrpPolSchema.getGrpPolNo());
            tLCInsureAccClassSet = tLCInsureAccClassDB.query();
            if(tLCInsureAccClassSet.size() == 0)
            {
                continue;
            }

            for(int j = 1; j <= tLCInsureAccClassSet.size(); j++)
            {
                LCInsureAccClassSchema schema = tLCInsureAccClassSet.get(j);

                String sql = "select sum(Money) from LCInsureAccTrace "
                             + "where polNo = '" + schema.getPolNo() + "' "
                             + "   and InsuAccNo = '"
                             + schema.getInsuAccNo() + "' "
                             + " and state = '0' ";
//                             + "   and PayPlanCode = '"
//                             + schema.getPayPlanCode() + "' ";
                String accBala = new ExeSQL().getOneValue(sql);
                schema.setInsuAccBala(accBala);

                this.setLJSGetEndorse(schema, feeType, -CommonBL.carry(accBala));  //余额
                //团体理赔账户
                if(schema.getContNo().equals(publicAccContNo)
                   && schema.getInsuAccNo().equals(grpInsureAccNo))
                {
                    mEdorItemSpecialData.add(BQ.ACCBALA_GROUP,
                                             CommonBL.bigDoubleToCommonString(
                                                 schema.getInsuAccBala(), "0.00"));
                }
                //团体固定账户
                else if(schema.getContNo().equals(publicAccContNo)
                        && schema.getInsuAccNo().equals(grpFixInsureAccNo))
                {
                    mEdorItemSpecialData.add(BQ.ACCBALA_FIEXD,
                                             CommonBL.bigDoubleToCommonString(
                                                 schema.getInsuAccBala(), "0.00"));
                }
                //个人账户
                else
                {
                    sumInsuredBala += schema.getInsuAccBala();
                }

                //若录入利息或默认利息，则需要计算利息
                if(!rateType.equals(BQ.DEALTYPE_INTERESTINPUT))
                {
                    String tdata = mCInValidate;
                    double interest = tAccountManage.getOneAccClassInterest(
                        schema, accRate, tdata, 365);
                    interest = CommonBL.carry(interest);
                    setLJSGetEndorse(schema, "LX", -interest); //余额

                    //团体理赔账户
                    if(schema.getContNo().equals(publicAccContNo)
                       && schema.getInsuAccNo().equals(grpInsureAccNo))
                    {
                        mEdorItemSpecialData.add(BQ.DETAILTYPE_INTEREST_GROUP,
                                                 CommonBL.bigDoubleToCommonString(
                                                     interest, "0.00"));
                    }
                    //团体固定账户
                    else if(schema.getContNo().equals(publicAccContNo)
                            && schema.getInsuAccNo().equals(grpFixInsureAccNo))
                    {
                        mEdorItemSpecialData.add(BQ.DETAILTYPE_INTEREST_FIXED,
                                                 CommonBL.bigDoubleToCommonString(
                                                     interest, "0.00"));
                    }
                    //个人账户
                    else
                    {
                        sumInsuredInterest += interest;
                    }
                } //if(!rateType.equals(BQ.DEALTYPE_INTERESTINPUT))
            } //for(int j = 1; j <= tLCInsureAccClassSet.size(); j++)
        }

        mEdorItemSpecialData.add(
               BQ.ACCBALA_INSURED,
               CommonBL.bigDoubleToCommonString(sumInsuredBala, "0.00"));
        if(!rateType.equals(BQ.DEALTYPE_INTERESTINPUT))
        {
            mEdorItemSpecialData.add(
                BQ.DETAILTYPE_INTEREST_INSURED,
                CommonBL.bigDoubleToCommonString(sumInsuredInterest, "0.00"));
        }

        return true;
    }

    /**
     * 生成公共账户利息
     * @param accRate double
     * @param publicAccContNo String
     * @param getAccTraceSql String
     */
    private boolean setPublicAccLX(double accRate)
    {
        String publicAccContNo = getPublicAccContNo();

        String getAccTraceSql = "select * "
                                + "from LCInsureAccClass "
                                + "where GrpPolNo = '"
                                + mLPGrpPolSchema.getGrpPolNo() + "' ";

        //有公共账户，则利息放入公共账户余额
        if(publicAccContNo != null && !publicAccContNo.equals(""))
        {
            String sql = getAccTraceSql
                         + "   and contNo = '" + publicAccContNo + "' ";
            LCInsureAccClassSet grpAccClassSet
                = new LCInsureAccClassDB().executeQuery(sql);

            //有团体理赔账户，否则利息放入理赔账户余额
            for(int i = 1; i <= grpAccClassSet.size(); i++)
            {
                LCInsureAccClassSchema schema = grpAccClassSet.get(i);
                if(schema.getInsuAccNo().equals(CommonBL.getInsuAccNo("001",
                    mLPGrpPolSchema.getRiskCode())))
                {
                    this.setLJSGetEndorse(schema, "LX", -CommonBL.carry(accRate));
                    mEdorItemSpecialData.add(BQ.DETAILTYPE_INTEREST_GROUP,
                                             CommonBL.bigDoubleToCommonString(
                                                 accRate, "0.00"));

                    return true;
                }
            }

            //有团体固定账户，则放入团体固定账户余额
            for(int i = 1; i <= grpAccClassSet.size(); i++)
            {
                LCInsureAccClassSchema schema = grpAccClassSet.get(i);
                if(schema.getInsuAccNo().equals(CommonBL.getInsuAccNo("004",
                    mLPGrpPolSchema.getRiskCode())))
                {
                    this.setLJSGetEndorse(schema, "LX", -CommonBL.carry(accRate));
                    mEdorItemSpecialData.add(BQ.DETAILTYPE_INTEREST_FIXED,
                                             CommonBL.bigDoubleToCommonString(
                                                 accRate, "0.00"));

                    return true;
                }
            }
        }

        //否则按当前个人账户余额放入账户
        else
        {
            double accBalaUsed = 0;

            //求总余额
            String sql = "select sum(Money) from LCInsureAccTrace "
                         + "where GrpPolNo = '" + mLPGrpPolSchema.getGrpPolNo()
                         + "' and State = '0' ";
            String sumAccBala = new ExeSQL().getOneValue(sql);

            sql = getAccTraceSql
                  + "   and contNo != '" + publicAccContNo + "' ";
            LCInsureAccClassSet grpAccClassSet
                = new LCInsureAccClassDB().executeQuery(sql);
            if(grpAccClassSet.size() == 0)
            {
                CError tError = new CError();
                tError.moduleName = "GEdorMJDetailBL";
                tError.functionName = "setPublicAccLX";
                tError.errorMessage = "没有查询到账户信息";
                mErrors.addOneError(tError);
                System.out.println(tError.errorMessage);
                return false;
            }

            for(int i = 1; i < grpAccClassSet.size(); i++)
            {
                LCInsureAccClassSchema schema = grpAccClassSet.get(i);
                sql = "select sum(Money) from LCInsureAccTrace "
                      + "where polNo = '" + schema.getPolNo() + "' "
                      + "   and InsuAccNo = '"
                      + schema.getInsuAccNo() + "' "
//                      + "   and PayPlanCode = '"
//                      + schema.getPayPlanCode() + "' "
                      + " and State = '0' ";
                String oneClassAccBala = new ExeSQL().getOneValue(sql);

                double temp = accRate * (Double.parseDouble(oneClassAccBala)
                                         / Double.parseDouble(sumAccBala));
                setLJSGetEndorse(schema, "LX", -CommonBL.carry(temp));

                accBalaUsed += temp;
            }
            setLJSGetEndorse(grpAccClassSet.get(grpAccClassSet.size()), "LX",
                             -CommonBL.carry(accRate - accBalaUsed));
            mEdorItemSpecialData.add(
                BQ.DETAILTYPE_INTEREST_INSURED,
                CommonBL.bigDoubleToCommonString(CommonBL.carry(accRate), "0.00"));
        }

        return true;
    }

    //得到公共账户个单号
    private String getPublicAccContNo()
    {
        String publicAccContNo = "";
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setGrpContNo(mLPGrpContSchema.getGrpContNo());
        tLCPolDB.setPolTypeFlag(BQ.POLTYPEFLAG_PUBLIC);
        LCPolSet tLCPolSet = tLCPolDB.query();
        if(tLCPolSet.size() != 0)
        {
            publicAccContNo = tLCPolSet.get(1).getContNo();
        }
        return publicAccContNo;
    }

    private String getFeeType()
    {
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode(mLPGrpPolSchema.getRiskCode());
        if(!tLMRiskAppDB.getInfo())
        {
            return "";
        }

        //一年期及以内险种冲退保费 TF。
        if(tLMRiskAppDB.getRiskPeriod().equals("M")
           || tLMRiskAppDB.getRiskPeriod().equals("S"))
        {
            return BQ.FEEFINATYPE_TF;
        }
        //一年期以上险种冲退保金 TB
        else if(tLMRiskAppDB.getRiskPeriod().equals("L"))
        {
            return BQ.FEEFINATYPE_TB;
        }

        return "";
    }

    /**
     * 生成财务数据
     * @param aLPGrpEdorItemSchema LPGrpEdorItemSchema
     * @param aLCGrpPolSchema LCGrpPolSchema
     * @param aOperationType String
     * @param aFeeType String
     * @param aGetMoney double
     * @param aGlobalInput GlobalInput
     * @return LJSGetEndorseSchema
     */
    private void setLJSGetEndorse(LCInsureAccClassSchema tAccClassSchema,
                                  String aFeeType, double aGetMoney)
    {
        LJSGetEndorseSchema tLJSGetEndorseSchema = new LJSGetEndorseSchema();

        //生成批改交退费表
        tLJSGetEndorseSchema.setGetNoticeNo(mLPGrpPolSchema.getEdorNo()); //给付通知书号码
        tLJSGetEndorseSchema.setEndorsementNo(mLPGrpPolSchema.getEdorNo());
        tLJSGetEndorseSchema.setGrpContNo(mLPGrpPolSchema.getGrpContNo());
        tLJSGetEndorseSchema.setGrpPolNo(mLPGrpPolSchema.getGrpPolNo());
        tLJSGetEndorseSchema.setContNo(tAccClassSchema.getContNo());
        tLJSGetEndorseSchema.setInsuredNo(tAccClassSchema.getInsuredNo());
        tLJSGetEndorseSchema.setPolNo(tAccClassSchema.getPolNo());
        tLJSGetEndorseSchema.setFeeOperationType(mLPGrpPolSchema.getEdorType());
        tLJSGetEndorseSchema.setGetDate(mLPGrpContSchema.getCValiDate());
        tLJSGetEndorseSchema.setGetMoney(aGetMoney);
        tLJSGetEndorseSchema.setFeeOperationType(mLPGrpPolSchema.getEdorType()); //补退费业务类型
        tLJSGetEndorseSchema.setFeeFinaType(aFeeType); //补退费财务类型
        tLJSGetEndorseSchema.setPayPlanCode(tAccClassSchema.getPayPlanCode()); //无作用
        tLJSGetEndorseSchema.setDutyCode(tAccClassSchema.getInsuAccNo()); //无作用，但一定要，转ljagetendorse时非空,qulq modify 修改为特需帐户类型，否则无法支持多个帐户
        tLJSGetEndorseSchema.setOtherNo(mLPGrpPolSchema.getEdorNo()); //其他号码置为保全批单号
        tLJSGetEndorseSchema.setOtherNoType("3"); //保全给付
        tLJSGetEndorseSchema.setGetFlag("0");
        tLJSGetEndorseSchema.setGrpName(mLPGrpPolSchema.getGrpName());
        tLJSGetEndorseSchema.setManageCom(mLPGrpPolSchema.getManageCom());
        tLJSGetEndorseSchema.setAgentCode(mLPGrpPolSchema.getAgentCode());
        tLJSGetEndorseSchema.setAgentCom(mLPGrpPolSchema.getAgentCom());
        tLJSGetEndorseSchema.setAgentGroup(mLPGrpPolSchema.getAgentGroup());
        tLJSGetEndorseSchema.setAgentType(mLPGrpPolSchema.getAgentType());
        tLJSGetEndorseSchema.setKindCode(mLPGrpPolSchema.getKindCode());
        tLJSGetEndorseSchema.setAppntNo(mLPGrpPolSchema.getCustomerNo());
        tLJSGetEndorseSchema.setRiskCode(mLPGrpPolSchema.getRiskCode());
        tLJSGetEndorseSchema.setRiskVersion(mLPGrpPolSchema.getRiskVersion());
        tLJSGetEndorseSchema.setApproveCode(mLPGrpPolSchema.getApproveCode());
        tLJSGetEndorseSchema.setApproveDate(mLPGrpPolSchema.getApproveDate());
        tLJSGetEndorseSchema.setApproveTime(mLPGrpPolSchema.getApproveTime());
        tLJSGetEndorseSchema.setOperator(mGlobalInput.Operator);
        tLJSGetEndorseSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSGetEndorseSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSGetEndorseSchema.setModifyTime(PubFun.getCurrentTime());

        map.put(tLJSGetEndorseSchema, SysConst.DELETE_AND_INSERT);
    }


    /**
     * 得到计算后的利息
     * @return TransferData
     */
    public TransferData getInterest()
    {
        return returnTransferData;
    }

    /**
     * 更改满期结算状态为已理算
     * @return boolean
     */
    private boolean setMJState()
    {
        StringBuffer sql = new StringBuffer();
        sql.append("update LPEdorEspecialData ")
                .append("set edorValue = '").append(BQ.MJSTATE_CAL).append("' ")
                .append("where edorNo = '")
                .append(mLPGrpPolSchema.getEdorNo())
                .append("'  and edorType = '").append(BQ.EDORTYPE_MJ)
                .append("'  and detailType = '").append(BQ.DETAILTYPE_MJSTATE)
                .append("'  and (polNo = '")
                .append(mLPGrpPolSchema.getGrpPolNo()).append("' or polNo = '000000') ");
        map.put(sql.toString(), "UPDATE");

        return true;
    }

    /**
     * 得到需要退保的团单信息
     * @return boolean
     */
    private boolean getLPGrpContInfo()
    {
        mLPGrpContSchema = new LPGrpContSchema();
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLPGrpPolSchema.getGrpContNo());
        if (!tLCGrpContDB.getInfo())
        {
            mErrors.addOneError("没有查询到保单号为" + tLCGrpContDB.getGrpContNo()
                                + "的保单");
            return false;
        }

        mReflections.transFields(mLPGrpContSchema, tLCGrpContDB.getSchema());
        mLPGrpContSchema.setEdorNo(mLPGrpPolSchema.getEdorNo());
        mLPGrpContSchema.setEdorType(mLPGrpPolSchema.getEdorType());
        mLPGrpContSchema.setOperator(mGlobalInput.Operator);
        mLPGrpContSchema.setModifyDate(PubFun.getCurrentDate());
        mLPGrpContSchema.setModifyTime(PubFun.getCurrentTime());
        mLPGrpContSchema.setMakeDate(PubFun.getCurrentDate());
        mLPGrpContSchema.setMakeTime(PubFun.getCurrentTime());

        map.put("delete from LPGrpCont where edorNo = '"
                        + mLPGrpPolSchema.getEdorNo()
                        + "'  and edorType = '" + mLPGrpPolSchema.getEdorType()
                        + "'  and grpContNo = '" + mLPGrpPolSchema.getGrpContNo() +
                        "' ",
                        "DELETE");
        map.put(mLPGrpContSchema, "INSERT");
        mCInValidate = getCInValidate();
        return true;
    }

    /**
     * 得到需要处理的个人险种信息
     * @return boolean
     */
    private boolean getLPPolInfo()
    {
    	RSWrapper rsWrapper = new RSWrapper();
    	mLPPolSet = new LPPolSet();
    	LCPolSet tLCPolSet = new LCPolSet();
        String sql = "select * from LCPol "
                   + "where appFlag = '" + BQ.APPFLAG_SIGN
                   + "'  and grpPolNo = '"
                   + mLPGrpPolSchema.getGrpPolNo()
                   + "' order by insuredNo";
        System.out.println(sql);

        if (!rsWrapper.prepareData(tLCPolSet, sql)) 
        {
            System.out.println("处理数据准备失败! ");
            return false;
        }
        do 
        {
            rsWrapper.getData();
            if (tLCPolSet != null || tLCPolSet.size() > 0) 
            {
            	for (int i = 1; i <= tLCPolSet.size(); i++)
                {
                    LPPolSchema tLPPolSchema = new LPPolSchema();
                    mReflections.transFields(tLPPolSchema, tLCPolSet.get(i));
                    tLPPolSchema.setEdorNo(mLPGrpPolSchema.getEdorNo());
                    tLPPolSchema.setEdorType(mLPGrpPolSchema.getEdorType());
                    tLPPolSchema.setOperator(mGlobalInput.Operator);
                    tLPPolSchema.setModifyDate(PubFun.getCurrentDate());
                    tLPPolSchema.setModifyTime(PubFun.getCurrentTime());
                    tLPPolSchema.setMakeDate(PubFun.getCurrentDate());
                    tLPPolSchema.setMakeTime(PubFun.getCurrentTime());
                    mLPPolSet.add(tLPPolSchema);
                    MMap tMMap = new MMap();
                    tMMap.put(tLPPolSchema, SysConst.DELETE_AND_INSERT);
                    if(!submitMap(tMMap))
                    {
                    	System.out.println("GEdorMJDetailBL->插入LPPol出错，请检查程序L828\n");
                    	return false;
                    }
                }
            }
        }
        while (tLCPolSet != null && tLCPolSet.size() > 0);
        rsWrapper.close();
        return true;
    }

    /**
     * 得到需要处理的被保人信息
     * @return boolean
     */
    private boolean getLPInsuredInfo()
    {
        mLPInsuredSet = new LPInsuredSet();
        mLPDiskImportSet = new LPDiskImportSet();
        HashMap tHashMap = new HashMap();
        int count = 0;
        for (int i = 1; i <= mLPPolSet.size(); i++)
        {
        	String tInsuredNo = mLPPolSet.get(i).getInsuredNo();
        	if (tHashMap.containsKey(tInsuredNo))
            {
        		continue;
            }
            //去掉重复的被保人
            tHashMap.put(tInsuredNo, "");
            LCInsuredDB tLCInsuredDB = new LCInsuredDB();
            tLCInsuredDB.setContNo(mLPPolSet.get(i).getContNo());
            tLCInsuredDB.setInsuredNo(mLPPolSet.get(i).getInsuredNo());
            if (!tLCInsuredDB.getInfo())
            {
                mErrors.addOneError("没有查询到险种"
                                    + mLPPolSet.get(i).getPolNo()
                                    + "的被保人信息。");
                return false;
            }
            LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
            mReflections.transFields(tLPInsuredSchema, tLCInsuredDB.getSchema());
            tLPInsuredSchema.setEdorNo(mLPGrpPolSchema.getEdorNo());
            tLPInsuredSchema.setEdorType(mLPGrpPolSchema.getEdorType());
            tLPInsuredSchema.setOperator(mGlobalInput.Operator);
            tLPInsuredSchema.setModifyDate(PubFun.getCurrentDate());
            tLPInsuredSchema.setModifyTime(PubFun.getCurrentTime());
            tLPInsuredSchema.setMakeDate(PubFun.getCurrentDate());
            tLPInsuredSchema.setMakeTime(PubFun.getCurrentTime());
            mLPInsuredSet.add(tLPInsuredSchema);

            //将被保人信息放在此表中，生成页面显示被保人清单所需要信息
            LPDiskImportSchema tLPDiskImportSchema = new LPDiskImportSchema();
            mReflections.transFields(tLPDiskImportSchema, tLPInsuredSchema);
            tLPDiskImportSchema.setSerialNo(String.valueOf(++count));
            tLPDiskImportSchema.setState("1");
            tLPDiskImportSchema = setEmployeeInfo(tLPDiskImportSchema,
                                                  tLPInsuredSchema);
            tLPDiskImportSchema.setInsuredName(tLPInsuredSchema.getName());
            PubFun.fillDefaultField(tLPDiskImportSchema);
            this.mLPDiskImportSet.add(tLPDiskImportSchema);
            MMap tMMap = new MMap();
            tMMap.put(tLPInsuredSchema, SysConst.DELETE_AND_INSERT);
            tMMap.put(tLPDiskImportSchema, SysConst.DELETE_AND_INSERT);
            if(!submitMap(tMMap))
            {
            	System.out.println("GEdorMJDetailBL->插入LPDiskImport出错，请检查程序L889\n");
            	return false;
            }
        }
        return true;
    }

    private LPDiskImportSchema setEmployeeInfo(LPDiskImportSchema schema,
                                               LPInsuredSchema tLPInsuredSchema)
    {
        LPDiskImportSchema tLPDiskImportSchema = schema.getSchema();
        schema = null;
        tLPDiskImportSchema.setRelation(
                tLPInsuredSchema.getRelationToMainInsured());

        if (tLPDiskImportSchema.getRelation() == null
            || tLPDiskImportSchema.getRelation().equals(""))
        {
            tLPDiskImportSchema.setRelation(BQ.MAININSURED); //本人
        }

        if (tLPDiskImportSchema.getRelation().equals(BQ.MAININSURED)
            || tLPDiskImportSchema.getRelation().equals("1"))
        {
            tLPDiskImportSchema.setEmployeeName(tLPInsuredSchema.getName());
            return tLPDiskImportSchema;
        }

        String sql = "  select * "
                     + "from LCInsured "
                     + "where (relationToMainInsured = '" + BQ.MAININSURED
                     + "'    or relationToMainInsured is null "
                     + "     or relationToMainInsured = '1') "
                     + "  and contNo = '"
                     + tLPInsuredSchema.getContNo() + "' ";
        System.out.println(sql);
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        LCInsuredSet set = tLCInsuredDB.executeQuery(sql);
        if (set.size() == 0)
        {
            tLPDiskImportSchema.setEmployeeName(tLPInsuredSchema.getName());
            //mErrors.addOneError("没有得到与被保人对应的的员工信息。");
            //return null;
        } else
        {
            tLPDiskImportSchema.setEmployeeName(set.get(1).getName());
        }

        return tLPDiskImportSchema;
    }

    /**
     * 得到需要处理的团体险种信息
     * @return boolean
     */
    private boolean getLPGrpPolInfo()
    {
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tLCGrpPolDB.setGrpPolNo(mLPGrpPolSchema.getGrpPolNo());
        if (!tLCGrpPolDB.getInfo())
        {
            mErrors.addOneError("没有查询到险种号为"
                                + mLPGrpPolSchema.getGrpPolNo() + "的险种。");
            return false;
        }
        mReflections.transFields(mLPGrpPolSchema, tLCGrpPolDB.getSchema());
        mLPGrpPolSchema.setOperator(mGlobalInput.Operator);
        mLPGrpPolSchema.setModifyDate(PubFun.getCurrentDate());
        mLPGrpPolSchema.setModifyTime(PubFun.getCurrentTime());
        mLPGrpPolSchema.setMakeDate(PubFun.getCurrentDate());
        mLPGrpPolSchema.setMakeTime(PubFun.getCurrentTime());

        map.put("delete from LPGrpPol where edorNo = '"
                + mLPGrpPolSchema.getEdorNo()
                + "'  and edorType = '" + mLPGrpPolSchema.getEdorType()
                + "'  and grpPolNo = '" + mLPGrpPolSchema.getGrpPolNo() + "' ",
                "DELETE");
        map.put(mLPGrpPolSchema, "INSERT");

        return true;
    }

    /**
     * 得到页面传入的数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData
                       .getObjectByObjectName("GlobalInput", 0);
        mLPGrpPolSchema = (LPGrpPolSchema) cInputData
                          .getObjectByObjectName("LPGrpPolSchema", 0);
        mEdorItemSpecialData = (EdorItemSpecialData) cInputData
                        .getObjectByObjectName("EdorItemSpecialData", 0);
        if (mGlobalInput == null
            || mLPGrpPolSchema == null
            || mEdorItemSpecialData == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }

        System.out.println("时间:" + PubFun.getCurrentDate() + " "
            + PubFun.getCurrentTime() + ", 操作人:" + mGlobalInput.Operator);

        return true;
    }
    
    /**
     * 存储数据一条数据 20090401 zhanggm
     * @return boolean
     */
    private boolean submitMap(MMap aMMap)
    {
        VData tVData = new VData();
        tVData.add(aMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(tVData, ""))
        {
            mErrors.copyAllErrors(p.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String args[])
    {
        GlobalInput gi = new GlobalInput();
        gi.Operator = "endor";
        gi.ComCode = "86";

        String edorNo = "20061227000010";
        String edorType = "MJ";
        String grpPolNo = "2200000422";

        EdorItemSpecialData tEdorItemSpecialData
            = new EdorItemSpecialData(edorNo, edorType);
          tEdorItemSpecialData.setPolNo(grpPolNo);
//          tEdorItemSpecialData.query();
          tEdorItemSpecialData.add(BQ.DETAILTYPE_ACCRATE, "1000");
          tEdorItemSpecialData.add(BQ.DETAILTYPE_ENDTIME_RATETYPE, "1");

        LPGrpPolSchema tLPGrpPolSchema = new LPGrpPolSchema();
        tLPGrpPolSchema.setEdorNo(edorNo);
        tLPGrpPolSchema.setEdorType(BQ.EDORTYPE_MJ);
        tLPGrpPolSchema.setGrpPolNo(grpPolNo);

        VData data = new VData();
        data.add(gi);
        data.add(tLPGrpPolSchema);
        data.add(tEdorItemSpecialData);

        GEdorMJDetailBL bl = new GEdorMJDetailBL();
        long a = System.currentTimeMillis();
        if (!bl.submitData(data, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        System.out.println((System.currentTimeMillis() - a) / 1000);
    }
    
    /**
     * 20090427 zhanggm 得到特需利息计算截至日期 line467
     * 如果做过修改特需账户利息操作，则取第一次修改前的保单终止日期进行计算
     * @return String
     */
    String getCInValidate()
    {
    	FDate tFDate = new FDate();
    	String tCInValidate = null;
    	String tGrpContNo = mLPGrpPolSchema.getGrpContNo();
    	StringBuffer sql = new StringBuffer();
    	sql.append("select a.CInValiDate from LPGrpCont a, lgwork b ")
    	   .append("where a.edorno = b.workno and a.edortype = b.typeno ")
    	   .append("and b.typeno = '").append(BQ.CINVALIDATE_CHANGE).append("' and innersource = '1' ")
    	   .append("and b.contno = '").append(tGrpContNo).append("' with ur");
    	System.out.println(sql.toString());
    	SSRS tSSRS = new SSRS();
    	tSSRS = new ExeSQL().execSQL(sql.toString());
    	if(tSSRS.MaxRow==1)
    	{
    		StringBuffer sqlCount = new StringBuffer();
    		sqlCount.append("select max(int(innersource)) from lgwork ")
    		   .append("where contno = '").append(tGrpContNo).append("' ")
    		   .append("and typeno = '").append(BQ.CINVALIDATE_CHANGE).append("' with ur");
         	String tCount = new ExeSQL().getOneValue(sqlCount.toString());
    		System.out.println("保单"+tGrpContNo+"变更过"+tCount+"次终止日期");
    		tCInValidate = tSSRS.GetText(1, 1);
    	}
    	else
    	{
    		System.out.println("保单"+tGrpContNo+"没有变更过终止日期");
    		tCInValidate = mLPGrpContSchema.getCInValiDate();
    	}
    	return tFDate.getString(CommonBL.changeDate(tCInValidate, 1));
    }
    
}
