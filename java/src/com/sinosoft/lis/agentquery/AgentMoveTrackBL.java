/*
 * <p>ClassName: AgentMoveTrackBL </p>
 * <p>Description: BL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2003-06-21
 */
package com.sinosoft.lis.agentquery;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LATreeBDB;
import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LATreeBSchema;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LATreeBSet;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class AgentMoveTrackBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String mTableFlag;
    private String mManageCom;
    private String mBranchType;
    private String mDepartDate; //离职日期
//    private String mBranchAttr;
    private String mEdorNo = "00000000000000000000";
    private String mTreeBEdorNo = "0";
    /** 业务处理相关变量 */
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LATreeSet mLATreeSet = new LATreeSet();

    public AgentMoveTrackBL()
    {
    }

    public static void main(String[] args)
    {
        LAAgentSchema mLAAgentSchema = new LAAgentSchema();
        String tTableFlag = "1";
        mLAAgentSchema.setManageCom("86110000");
        mLAAgentSchema.setAgentCode("8611000011");
        mLAAgentSchema.setName(null);
        mLAAgentSchema.setBranchType("1");
        GlobalInput tG = new GlobalInput();
        tG.Operator = "jojo";
        tG.ManageCom = "86110000";
        VData tVData = new VData();
        tVData.add(tG);
        tVData.add(tTableFlag);
        tVData.addElement(mLAAgentSchema);
        AgentMoveTrackBL tAMTBL = new AgentMoveTrackBL();
        if (!tAMTBL.submitData(tVData, "QUERY||MAIN"))
        {
            System.out.println(tAMTBL.mErrors.getFirstError());
        }
        else
        {
            System.out.println("---------ok--------------");
        }
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentMoveTrackBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败AgentMoveTrackBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
//    if (!prepareOutputData())
//      return false;
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        //   this.mLAAttendSchema.setModifyDate(getDate());
        //  this.mLAAttendSchema.setModifyTime();

        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setSchema(mLAAgentSchema);
        if (tLAAgentDB.getInfo())
        {
            mLAAgentSchema.setSchema(tLAAgentDB.getSchema());
        }
        else
        {
            return false;
        }

        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                                      getObjectByObjectName("LAAgentSchema", 2));
        this.mTableFlag = (String) cInputData.get(1);
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));

        this.mManageCom = this.mLAAgentSchema.getManageCom().trim();
        this.mBranchType = this.mLAAgentSchema.getBranchType().trim();
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        String tCurEdorNo = "", tLastEdorNo = "", tTreeBEdorNo = "";
        String tBranchAttr = "", tBranchCode = "";
        ExeSQL tExeSQL;
        String tAgentCode = this.mLAAgentSchema.getAgentCode();
        String tName = this.mLAAgentSchema.getName();
        LATreeBDB tLATreeBDB = new LATreeBDB();
        LATreeBSet tLATreeBSet = new LATreeBSet();
        //1------------------查询历史行政信息
        //String tSQL = "SELECT * FROM LATreeB WHERE AStartDate IS NOT NULL ";
        //排除初始行政信息备份类型05
        String tSQL = "SELECT * FROM LATreeB WHERE 1=1 And RemoveType <> '05' ";
        tSQL += (tAgentCode == null || tAgentCode.equals(""))
                ? "" : "AND AgentCode = '" + tAgentCode + "' ";
        if (this.mTableFlag.equals("1")) //从正式表中判断
        {
            tSQL += (tName == null || tName.equals("")) ? "" :
                    "AND AgentCode in (SELECT AgentCode FROM LAAgent "
                    + "WHERE Name = '" + tName + "' AND BranchType = '" +
                    mBranchType + "') ";
        }
        tSQL += "ORDER BY EdorNo,AStartDate"; //从过去到现在排序
        System.out.println("tSQL:" + tSQL);
        tLATreeBSet = tLATreeBDB.executeQuery(tSQL);
        LATreeSchema tLATreeSchema;
        LATreeBSchema tLATreeBSchema;
        String strSql = "";
        for (int i = 1; i <= tLATreeBSet.size(); i++)
        {
            tLATreeSchema = new LATreeSchema();
            tLATreeBSchema = tLATreeBSet.get(i);
            if (i == 1)
            {
                tLastEdorNo = "00000000000000000000";
            }
            //代理人代码
            tAgentCode = tLATreeBSchema.getAgentCode().trim();
            tLATreeSchema.setAgentCode(tAgentCode); //-------------
            //代理人组别
            //1--从LAAgent及LAAgentB表中得到BranchCode
            tTreeBEdorNo = tLATreeBSchema.getEdorNO();
            tCurEdorNo = tTreeBEdorNo;
            String tIndexCalNo = tLATreeBSchema.getIndexCalNo();
            strSql = "SELECT NVL((SELECT TRIM(BranchCode) FROM LAAgentB "
                     + "WHERE (EdorNo >= '" + tCurEdorNo +
                     "' OR IndexCalNo = '" + tIndexCalNo + "') "
                     + "AND AgentCode = '" + tAgentCode + "' AND ROWNUM = 1),"
                     +
                     "(SELECT TRIM(BranchCode) FROM LAAgent WHERE AgentCode ='" +
                     tAgentCode + "')) "
                     + "FROM LDSysvar WHERE sysvar = 'onerow'";
            tExeSQL = new ExeSQL();
            tBranchCode = tExeSQL.getOneValue(strSql);
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentMoveTrackBL";
                tError.functionName = "submitquery";
                tError.errorMessage = "查询代理人的所在组BranchCode出错!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //2--从LABranchGroup及LABranchGroupB表中得到BranchAttr
            strSql = "SELECT NVL((SELECT TRIM(BranchAttr) FROM LABranchGroupB "
                     + "WHERE EdorNo >= '" + tTreeBEdorNo +
                     "' AND EndDate is NOT NULL "
                     + " AND AgentGroup = '" + tBranchCode +
                     // "'  and founddate >='"+mLAAgentSchema.getEmployDate()+
                     "' AND ROWNUM = 1),"
                     +
                     "(SELECT TRIM(BranchAttr) FROM LABranchGroup WHERE AgentGroup ='" +
                     tBranchCode + "')) "
                     + "FROM LDSysvar WHERE sysvar = 'onerow'";
            tExeSQL = new ExeSQL();
            tBranchAttr = tExeSQL.getOneValue(strSql);
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentMoveTrackBL";
                tError.functionName = "submitquery";
                tError.errorMessage = "查询代理人的所在组BranchAttr出错!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLATreeSchema.setAgentGroup(tBranchAttr); //-----------------
            //代理人职级
            tLATreeSchema.setManageCom(tLATreeBSchema.getAgentGrade()); //----------
            //上级代理人名称 推荐人名称
            String upAgent = tLATreeBSchema.getUpAgent();
            String introAgent = tLATreeBSchema.getIntroAgency();

            tLATreeSchema.setAgentSeries(upAgent == null ? "" : upAgent); //-------------
            tLATreeSchema.setAgentGrade(introAgent == null ? "" : introAgent); //-------------
            //////////////////////////////////////////////////////
            //不需要显示名称了
            //if (!getAgentName(upAgent,introAgent,tLATreeSchema))
            //  return false;
            //////////////////////////////////////////////////////
            //组育成人 部育成人 督导育成人
            String tAscriptCode = tLATreeBSchema.getAscriptSeries();
            if (!getAsciptCode(tAscriptCode, tLATreeSchema))
            {
                return false;
            }
            if (tAscriptCode == null || tAscriptCode.trim().equals(""))
            {
                tAscriptCode = "";
                tLATreeSchema.setAgentLastSeries(tAscriptCode); //---------
                tLATreeSchema.setAgentLastGrade(tAscriptCode); //---------
                tLATreeSchema.setIntroAgency(tAscriptCode); //---------
                tLATreeSchema.setUpAgent(tAscriptCode); //---------
            }
            System.out.println("----------jojo:" + tLastEdorNo);
            //调整日期
            tLATreeSchema.setAstartDate(tLATreeBSchema.getAstartDate()); //---------
            tLATreeSchema.setOperator(tLATreeBSchema.getRemoveType());
            /*---------------------查询组别变化--------------------------*/
            if (!addTreeOfBranchChange(tLATreeSchema, tBranchCode, tBranchAttr,
                                       tLastEdorNo, tCurEdorNo))
            {
                return false;
            }

            tLastEdorNo = tCurEdorNo;
            this.mLATreeSet.add(tLATreeSchema); //LATreeB表中的
        }
        //2--------------添加当前的行政信息
        tLATreeSchema = queryLATreeInfo(tAgentCode, tName, mBranchType,
                                        tLastEdorNo);
        if (tLATreeSchema == null)
        {
            return false;
        }
        //组信息
        tBranchCode = tLATreeSchema.getBranchCode();
        tBranchAttr = tLATreeSchema.getAgentGroup();
        if (!tLastEdorNo.equals(""))
        {
            tCurEdorNo = "100000000000000000000";
        }
        //--------查询组别变化
        if (!addTreeOfBranchChange(tLATreeSchema, tBranchCode, tBranchAttr,
                                   tLastEdorNo, tCurEdorNo))
        {
            return false;
        }
        this.mLATreeSet.add(tLATreeSchema); //LATree表中的
        //起期排序置于mLATreeSet集中
        if (!setOrder())
        {
            return false;
        }
        this.mResult.clear();
        this.mResult.add(this.mLATreeSet);
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
//      this.mInputData.add(this.mLAAttendSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentMoveTrackBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private LATreeSchema queryLATreeInfo(String cAgentCode, String cName,
                                         String cBranchType,
                                         String cLastAdjustDate)
    {
        String tSql = "", tAgentCode = "";
        String tUpAgent = "", tIntroAgent = "";
        String tLastAdjustDate = cLastAdjustDate;
        ExeSQL tExeSQL;
        LATreeSchema tLATreeSchema = new LATreeSchema();
        LATreeDB tLATreeDB = new LATreeDB();

        tSql = "SELECT * FROM LATree WHERE 1=1 ";
        tSql += (cAgentCode == null || cAgentCode.equals(""))
                ? "" : "AND AgentCode = '" + cAgentCode + "' ";
        if (this.mTableFlag.equals("1")) //从正式表中判断
        {
            tSql += (cName == null || cName.equals("")) ? "" :
                    "AND AgentCode in (SELECT AgentCode FROM LAAgent "
                    + "WHERE Name = '" + cName + "' AND BranchType = '" +
                    cBranchType + "') ";
        }
        System.out.println("sql:" + tSql);
        LATreeSet tLATreeSet = tLATreeDB.executeQuery(tSql);
        if (tLATreeDB.mErrors.needDealError() || tLATreeSet.size() <= 0)
        {
            this.mErrors.copyAllErrors(tLATreeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentMoveTrackBL";
            tError.functionName = "queryLATreeInfo";
            tError.errorMessage = "查询代理人的正式表中行政信息出错!";
            this.mErrors.addOneError(tError);
            return null;
        }
        tLATreeSchema = tLATreeSet.get(1);
        tAgentCode = tLATreeSchema.getAgentCode(); //--------
        tLATreeSchema.setManageCom(tLATreeSchema.getAgentGrade()); //-???-------
        //查询该代理人的离职时间
        tSql = "SELECT to_char(DepartDate,'YYYY-MM-DD') FROM LADimission "
               + "WHERE BranchType = '" + mBranchType + "' "
               + "And AgentCode = '" + tAgentCode +
               "' Order by DepartTimes desc";
        tExeSQL = new ExeSQL();
        this.mDepartDate = tExeSQL.getOneValue(tSql);
        if (tExeSQL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentMoveTrackBL";
            tError.functionName = "queryLATreeInfo";
            tError.errorMessage = "查询代理人" + tAgentCode + "的离职日期出错！";
            this.mErrors.addOneError(tError);
            return null;
        }
        //查询BranchAttr
        if (this.mDepartDate == null || this.mDepartDate.equals(""))
        {
            tSql = "SELECT TRIM(BranchAttr),AgentGroup FROM LABranchGroup "
                   + "WHERE AgentGroup = (SELECT BranchCode FROM LAAgent "
                   + "WHERE AgentCode = '" + tAgentCode + "')";
        }
        else
        {
            tSql =
                    "SELECT NVL((SELECT TRIM(a.BranchAttr) FROM LABranchGroupB a "
                    + "WHERE a.EndDate > '" + mDepartDate +
                    "' AND a.EndDate is NOT NULL "
                    + "AND a.AgentGroup = (SELECT BranchCode FROM LAAgent "
                    + "WHERE AgentCode = '" + tAgentCode +
                    "') AND EdorNo = (Select Max(EdorNo) "
                    + "From LABranchGroupB Where AgentGroup = a.AgentGroup)),"
                    + "(SELECT TRIM(BranchAttr) FROM LABranchGroup "
                    + "WHERE AgentGroup = (SELECT BranchCode FROM LAAgent "
                    + "WHERE AgentCode = '" + tAgentCode +
                    "'))),(SELECT BranchCode FROM LAAgent "
                    + "WHERE AgentCode = '" + tAgentCode + "') "
                    + "FROM LDSysvar WHERE sysvar = 'onerow'";
        }
        tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(tSql);
        if (tSSRS.getMaxRow() <= 0 || tExeSQL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentMoveTrackBL";
            tError.functionName = "queryLATreeInfo";
            tError.errorMessage = "查询代理人当前的BranchAttr出错！";
            this.mErrors.addOneError(tError);
            return null;
        }
        String tResult[][] = tSSRS.getAllData();
        tLATreeSchema.setBranchCode(tResult[0][1]);
        tLATreeSchema.setAgentGroup(tResult[0][0]); //-------
        tUpAgent = tLATreeSchema.getUpAgent();
        System.out.println("UpAgent:" + tUpAgent);
        tIntroAgent = tLATreeSchema.getIntroAgency();
        tLATreeSchema.setAgentGrade(tIntroAgent == null ? "" : tIntroAgent);
        tLATreeSchema.setAgentSeries(tUpAgent == null ? "" : tUpAgent);
        tLATreeSchema.setAgentLastSeries("");
        tLATreeSchema.setAgentLastGrade("");
        tLATreeSchema.setIntroAgency("");
        tLATreeSchema.setUpAgent("");
/////////////////////////////////////////////////////////////////////////
//-----------------不需要显示名称------------
//    //上级代理人 推荐人名称
//    if (!getAgentName(tLATreeSchema.getUpAgent(),tLATreeSchema.getIntroAgency(),tLATreeSchema))
//      return null;
/////////////////////////////////////////////////////////////////////////
        //tLATreeSchema.setAgentGrade(tLATreeSchema.getIntroAgency());
        //tLATreeSchema.setAgentSeries(tLATreeSchema.getUpAgent());
        //组 部 督导 育成人名称
        if (!getAsciptCode(tLATreeSchema.getAscriptSeries(), tLATreeSchema))
        {
            return null;
        }
        //tLATreeSchema.setAstartDate(tLATreeSchema.getAstartDate());
        return tLATreeSchema;
    }

    /**
     *根据育成链，得到组、部和督导育成人的名称
     */
    private boolean getAsciptCode(String cAscriptCode,
                                  LATreeSchema cLATreeSchema)
    {
        String strSql = "";
        ExeSQL tExeSQL;
        if (cAscriptCode == null || cAscriptCode.trim().equals(""))
        {
            return true;
        }
        String tAsGroup = "", tAsDep = "", tAsDudao = "", tAsAreaDudao = "";
        //组育成人 部育成人 督导育成人
        //String tAscriptCode = tLATreeBSchema.getAscriptSeries().trim();
        tAsGroup = cAscriptCode.indexOf(":") == -1 ? cAscriptCode
                   : cAscriptCode.substring(0, cAscriptCode.indexOf(":")); //组育成人
        cAscriptCode = cAscriptCode.indexOf(":") == -1 ? ""
                       : cAscriptCode.substring(cAscriptCode.indexOf(":") + 1);
        tAsDep = cAscriptCode.indexOf(":") == -1 ? cAscriptCode
                 : cAscriptCode.substring(0, cAscriptCode.indexOf(":")); //部育成人
        cAscriptCode = cAscriptCode.indexOf(":") == -1 ? ""
                       : cAscriptCode.substring(cAscriptCode.indexOf(":") + 1);
        tAsDudao = (cAscriptCode.indexOf(":") == -1) ? cAscriptCode //督导育成人
                   : cAscriptCode.substring(0, cAscriptCode.indexOf(":"));
        cAscriptCode = cAscriptCode.indexOf(":") == -1 ? ""
                       : cAscriptCode.substring(cAscriptCode.indexOf(":") + 1);
        tAsAreaDudao = (cAscriptCode.indexOf(":") == -1) ? cAscriptCode //区域督导育成人
                       : cAscriptCode.substring(0, cAscriptCode.indexOf(":"));

        cLATreeSchema.setAgentLastSeries(tAsGroup); //---------
        cLATreeSchema.setAgentLastGrade(tAsDep); //---------
        cLATreeSchema.setIntroAgency(tAsDudao); //---------
        cLATreeSchema.setUpAgent(tAsAreaDudao); //---------
/////////////////////////////////////////////////////////////////////////////
//    不需要显示名称了
//    if (!tAsGroup.equals("")||!tAsDep.equals("")||!tAsDudao.equals(""))
//    {
//      strSql = "SELECT AgentCode,Name FROM LAAgent "
//             +"WHERE AgentCode IN ('"+tAsGroup+"','"+tAsDep+"','"+tAsDudao+"')";
//      tExeSQL = new ExeSQL();
//      SSRS tSSRS = tExeSQL.execSQL(strSql);
//      if (tExeSQL.mErrors.needDealError())
//      {
//        this.mErrors.copyAllErrors(tExeSQL.mErrors);
//        CError tError = new CError();
//        tError.moduleName = "AgentMoveTrackBL";
//        tError.functionName = "getAscriptCode";
//        tError.errorMessage = "查询组育成人 部育成人 督导育成人出错!";
//        this.mErrors.addOneError(tError);
//        return false;
//      }
//      if (tSSRS.getMaxRow()>0)
//      {
//        String aName[][] = tSSRS.getAllData();
//        for(int j=0;j<aName.length;j++)
//        {
//          if (aName[j][0].equals(tAsGroup))
//            cLATreeSchema.setAgentLastSeries(aName[j][1]);     //---------
//          else if (aName[j][0].equals(tAsDep))
//            cLATreeSchema.setAgentLastGrade(aName[j][1]);      //---------
//          else if (aName[j][0].equals(tAsDudao))
//            cLATreeSchema.setIntroAgency(aName[j][1]);         //---------
//        }
//      }
//    }
///////////////////////////////////////////////////////////////////////////////
        return true;
    }

    /**
     *根据上级代理人及推荐人代码查询名称
     */
    private boolean getAgentName(String cUpAgent, String cIntroAgency,
                                 LATreeSchema cLATreeSchema)
    {
        String strSql = "";
        ExeSQL tExeSQL;
        String upAgent = cUpAgent == null ? "" : cUpAgent.trim();
        String introAgent = cIntroAgency == null ? "" : cIntroAgency.trim();
        if (!upAgent.equals("") || !introAgent.equals(""))
        {
            strSql = "SELECT AgentCode,Name FROM LAAgent "
                     + "WHERE AgentCode IN ('" + upAgent + "','" + introAgent +
                     "')";
            tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(strSql);
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentMoveTrackBL";
                tError.functionName = "getAgentName";
                tError.errorMessage = "查询上级代理人名称,推荐人名称出错!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tSSRS.getMaxRow() > 0)
            {
                String aName[][] = tSSRS.getAllData();
                for (int j = 0; j < aName.length; j++)
                {
                    try
                    {
                        if (aName[j][0].equals(upAgent))
                        {
                            cLATreeSchema.setAgentSeries(aName[j][1]); //-------------
                        }
                        else if (aName[j][0].equals(introAgent))
                        {
                            cLATreeSchema.setAgentGrade(aName[j][1]); //-------------
                        }
                    }
                    catch (Exception ex)
                    {
                        System.out.println("数组出界：" + ex.getMessage());
                    }
                }
            }
        }
        return true;
    }

    /**
     *查询组别变化，相应生成一条异动记录
     */
    private boolean addTreeOfBranchChange(LATreeSchema cLATreeSchema,
                                          String cBranchCode,
                                          String cBranchAttr,
                                          String cLastEdorNo, String cCurEdorNo)
    {
        //String tBeginDate = cLastAdjustDate;
        String tEdorNo = "";
        String tEdorType = "02"; //转储类型为机构调动
        String tIndexCalNo = "";
        String tOldAgentGroup = cLATreeSchema.getAgentGroup();
        int arrSize = 0;
        //查询此调整日期后的组别
        String strSql =
                "SELECT TRIM(BranchAttr),EndDate,EdorNo,IndexCalNo FROM LABranchGroupB "
                + "WHERE AgentGroup = '" + cBranchCode +
                "' AND EndDate IS NOT NULL ";
//        "and founddate >='"+mLAAgentSchema.getEmployDate()+"' ";
        if (!cLastEdorNo.equals(""))
        {
            strSql += "AND EdorNo < '" + cCurEdorNo + "' AND EdorNo > '" +
                    cLastEdorNo + "' ";
        }
        if (this.mDepartDate != null && !this.mDepartDate.equals(""))
        {
            strSql += "AND EndDate < '" + mDepartDate + "' ";
        }
        strSql += "Order by EdorNo";

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(strSql);
        if (tExeSQL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentMoveTrackBL";
            tError.functionName = "addTreeOfBranchChange";
            tError.errorMessage = "查询组别变化出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        arrSize = tSSRS.getMaxRow();
        System.out.println("LABranchGroup size:" + arrSize);
        if (arrSize > 0)
        {
            String aBranchAttr[][] = tSSRS.getAllData();
            LATreeSchema vLATreeSchema;
//            LATreeBDB tLATreeBDB;
//            LATreeBSet tLATreeBSet;
            //预先在set集中预定位置
            for (int k = 0; k < arrSize; k++)
            {
//                if (tOldAgentGroup.equals(aBranchAttr[k][0]))
//                    continue;
                vLATreeSchema = new LATreeSchema();
                vLATreeSchema.setSchema(cLATreeSchema);
                vLATreeSchema.setAstartDate(aBranchAttr[k][1]);
                vLATreeSchema.setAgentGroup(aBranchAttr[k][0]);
                tOldAgentGroup = aBranchAttr[k][0];
//                //查询人员备份表是否存在与机构转储号码一致的记录
//                tEdorNo = aBranchAttr[k][2];
//                tIndexCalNo = aBranchAttr[k][3];
//                tLATreeBDB = new LATreeBDB();
//                tLATreeBDB.setAgentCode(cLATreeSchema.getAgentCode());
//                if (tIndexCalNo == null || tIndexCalNo.equals(""))
//                {
//                    tLATreeBDB.setEdorNO(tEdorNo);
//                    tLATreeBDB.setRemoveType(tEdorType);
//                }else
//                    tLATreeBDB.setIndexCalNo(tIndexCalNo);
//                tLATreeBSet = tLATreeBDB.query();
//                if (tLATreeBDB.mErrors.needDealError())
//                {
//                    this.mErrors.copyAllErrors(tLATreeBDB.mErrors);
//                    CError tError = new CError();
//                    tError.moduleName = "AgentMoveTrackBL";
//                    tError.functionName = "addTreeOfBranchChange";
//                    tError.errorMessage = "查询行政信息备份出错!";
//                    this.mErrors.addOneError(tError);
//                    return false;
//                }
//                if (tLATreeBSet.size() > 0)
//                {
//                    //起期
//                    vLATreeSchema.setAstartDate(tLATreeBSet.get(1).getAstartDate());
//                    //上级代理人
//                    vLATreeSchema.setAgentSeries(tLATreeBSet.get(1).getUpAgent());
//                    //职级
//                    vLATreeSchema.setManageCom(tLATreeBSet.get(1).getAgentGrade());
//                    //推荐人
//                    vLATreeSchema.setAgentGrade(tLATreeBSet.get(1).getIntroAgency());
//                    //组育成人 部育成人 督导育成人
//                    String tAscriptCode = tLATreeBSet.get(1).getAscriptSeries();
//                    if (!getAsciptCode(tAscriptCode,vLATreeSchema))
//                        return false;
//                    if (tAscriptCode == null || tAscriptCode.trim().equals(""))
//                    {
//                        tAscriptCode = "";
//                        vLATreeSchema.setAgentLastSeries(tAscriptCode);
//                        vLATreeSchema.setAgentLastGrade(tAscriptCode);
//                        vLATreeSchema.setIntroAgency(tAscriptCode);
//                    }
//                }
                this.mLATreeSet.add(vLATreeSchema);
            }
        }
        return true;
    }

    //将mLATreeSet中的起期排序
    private boolean setOrder()
    {
        int iSize = this.mLATreeSet.size();
        if (iSize == 1)
        {
            return true;
        }
        LATreeSet tLATreeSet = new LATreeSet();
        LATreeSchema aLATreeSchema = new LATreeSchema();
        Reflections tRef = new Reflections();

        for (int i = 1; i <= mLATreeSet.size(); i++)
        {
            LATreeSchema tLATreeSchema = new LATreeSchema();
            tLATreeSchema = this.mLATreeSet.get(i).getSchema();
            if (i == 1)
            {
                aLATreeSchema.setSchema(tLATreeSchema);
            }
//    tRef.printFields(tLATreeSchema);
//    tRef.printFields(aLATreeSchema);
            if (i > 1)
            {
                if (tLATreeSchema.getAgentCode().equals(aLATreeSchema.
                        getAgentCode())
                    &&
                    tLATreeSchema.getAgentGrade().equals(aLATreeSchema.
                        getAgentGrade())
                    &&
                    tLATreeSchema.getAgentGroup().equals(aLATreeSchema.
                        getAgentGroup())
                    &&
                    tLATreeSchema.getManageCom().equals(aLATreeSchema.
                        getManageCom())
                    &&
                    tLATreeSchema.getAgentSeries().equals(aLATreeSchema.
                        getAgentSeries())
                    &&
                    tLATreeSchema.getAgentLastSeries().equals(aLATreeSchema.
                        getAgentLastSeries())
                    &&
                    tLATreeSchema.getAgentLastGrade().equals(aLATreeSchema.
                        getAgentLastGrade())
                    &&
                    tLATreeSchema.getIntroAgency().equals(aLATreeSchema.
                        getIntroAgency())
                    &&
                    tLATreeSchema.getUpAgent().equals(aLATreeSchema.getUpAgent())
                    &&
                    tLATreeSchema.getAstartDate().equals(aLATreeSchema.
                        getAstartDate())
                        )
                {
                    mLATreeSet.removeRange(i, i);
                }
                else
                {
                    aLATreeSchema.setSchema(tLATreeSchema);
                }
            }
        }
        //取得变动日期
        String aAstartDate[] = new String[iSize];
        for (int i = 1; i <= mLATreeSet.size(); i++)
        {
            aAstartDate[i - 1] = this.mLATreeSet.get(i).getAstartDate();
        }
        //数组排序
        String tTempDate = "";
        for (int j = 0; j < mLATreeSet.size(); j++)
        {
            for (int k = j + 1; k < mLATreeSet.size(); k++)
            {
                if (aAstartDate[j].compareTo(aAstartDate[k]) > 0)
                {
                    tTempDate = aAstartDate[k];
                    aAstartDate[k] = aAstartDate[j];
                    aAstartDate[j] = tTempDate;
                }
            }
        }
        for (int l = 1; l <= mLATreeSet.size(); l++)
        {
            this.mLATreeSet.get(l).setAstartDate(aAstartDate[l - 1]);
            System.out.println(l + ":" + aAstartDate[l - 1]);
        }
        return true;
    }
    /**
     *  显示值        位置字段
     * -----------------------
     * 代理人代码     --  AgentCode
     * 代理人组别     --  AgentGroup
     * 代理人职级     --  ManageCom
     * 上级代理人名称  --  AgentSeries
     * 推荐人名称     --  AgentGrade
     * 组育成人       --  AgentLastSeries
     * 部育成人       --  AgentLastGrade
     * 督导育成人     --  IntroAgency
     * 区域督导育成人  --  UpAgent
     * 调整日期       --  AStartDate
     * BranchCode   --  BranchCode   //界面不显示，用于查询组别变化
     * 转储类型      --  Operator
     */
}