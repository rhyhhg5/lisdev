package com.sinosoft.lis.agentquery;
/**
 * <p>Title: 个险中介</p>
 *
 * <p>Description: 个险中介业务保单明细查询</p>
 *
 * <p>Copyright: Copyright (c) 2011</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.FileWriter;
import java.io.*;
import java.text.DecimalFormat;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class LAAMQueryBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;

    private String mOutXmlPath = null;
    //private String mtype = null;

    SSRS tSSRS=new SSRS();
    public LAAMQueryBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LAZJFycQueryBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 4][30];
        mToExcel[0][0] = "中介业务专员提奖信息明细表";

        mToExcel[1][0] = "保单号";
        mToExcel[1][1] = "展业机构编码";   
        mToExcel[1][2] = "中介代理机构编码";
        mToExcel[1][3] = "中介机构名称";
        mToExcel[1][4] = "投保人";
        mToExcel[1][5] = "险种编码";
        mToExcel[1][6] = "险种名称";  
        mToExcel[1][7] = "保费";
        mToExcel[1][8] = "提奖比例";
        mToExcel[1][9] = "提奖";
        mToExcel[1][10] = "缴费频次";
        mToExcel[1][11] = "缴费期";
        mToExcel[1][12] = "缴费次数";
        mToExcel[1][13] = "签单日期";
        mToExcel[1][14] = "回执回销日期";
        mToExcel[1][15] = "交单日期";
        mToExcel[1][16] = "专员代码";
        mToExcel[1][17] = "专员姓名";     
        mToExcel[1][18] = "人员状态";


        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row +1][col - 1] = tSSRS.GetText(row, col);
            }
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAZJFycQueryBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        if(mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAZJFycQueryBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

  

   
    public static void main(String[] args)
    {
        LAZJFycQueryBL LAZJFycQueryBL = new
            LAZJFycQueryBL();
    System.out.println("11111111");
    }
}
