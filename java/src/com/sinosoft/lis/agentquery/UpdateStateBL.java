/*
 * <p>ClassName: LABranchGroupBuildBL </p>
 * <p>Description: LABranchGroupBuildBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2013-05-17
 */
package com.sinosoft.lis.agentquery;

import com.sinosoft.lis.db.LAWageHistoryDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAWageHistorySchema;
import com.sinosoft.lis.vschema.LAWageHistorySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Company: Sinosoft</p>
 * @author yangjian
 * @version 1.0
 * 
 *
 */
public class UpdateStateBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap mMMap = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局变量 */
    private LAWageHistorySchema mLAWageHistorySchema = new LAWageHistorySchema();
    private LAWageHistorySet mLAWageHistorySet = new LAWageHistorySet(); 
    
    public UpdateStateBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
    	mOperate = cOperate;
	    //得到外部传入的数据,将数据备份到本类中
	    if (!getInputData(cInputData))
	    {
	        return false;
	    }
	    //进行业务处理
	    if (!dealData())
	    {
	        // @@错误处理
	        CError tError = new CError();
	        tError.moduleName = "UpdateStateBL";
	        tError.functionName = "DealData";
	        tError.errorMessage = "数据处理失败UpdateStateBL-->dealData!";
	        this.mErrors.addOneError(tError);
	        return false;
	    }
	    try {
	    	PubSubmit tPubSubmit = new PubSubmit();
	    	tPubSubmit.submitData(this.mInputData, "UPDATE");
			
		} catch (Exception e) {
			CError tError = new CError();
	        tError.moduleName = "UpdateStateBL";
	        tError.functionName = "submitData";
	        tError.errorMessage = "数据处理失败UpdateStateBL-->dealData!";
	        this.mErrors.addOneError(tError);
	        return false;
		}
	    return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	LAWageHistorySet tUpdateLAWageHistorySet = new LAWageHistorySet();
        //设置基础数据
        for (int i = 1; i <= this.mLAWageHistorySet.size(); i++)
        {
        	mLAWageHistorySchema = mLAWageHistorySet.get(i);
        	String tManageCom = mLAWageHistorySchema.getManageCom();
        	String tWageNo = mLAWageHistorySchema.getWageNo();
        	String tBranchtype = mLAWageHistorySchema.getBranchType();
        	String tBranchtype2 = mLAWageHistorySchema.getBranchType2();
        	String tNewState = mLAWageHistorySchema.getState();
        	
        	LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();
        	tLAWageHistoryDB.setBranchType(tBranchtype);
        	tLAWageHistoryDB.setBranchType2(tBranchtype2);
        	tLAWageHistoryDB.setManageCom(tManageCom);
        	tLAWageHistoryDB.setWageNo(tWageNo);
        	tLAWageHistoryDB.setAClass("03");
        	LAWageHistorySchema tLAWageHistorySchema = new LAWageHistorySchema();
        	LAWageHistorySet tLAWageHistorySet = new LAWageHistorySet();
        	
        	
        	tLAWageHistorySet = tLAWageHistoryDB.query();
        	if(null == tLAWageHistorySet||0==tLAWageHistorySet.size()){
        		CError tError = new CError();
    	        tError.moduleName = "UpdateStateBL";
    	        tError.functionName = "submitData";
    	        tError.errorMessage = "该数据已不存在!";
    	        this.mErrors.addOneError(tError);
        		return false;
        	}
        	tLAWageHistorySchema = tLAWageHistorySet.get(1);
        	
        	tLAWageHistorySchema.setState(tNewState);
        	tUpdateLAWageHistorySet.add(tLAWageHistorySchema);
        	
        }
        mMMap.put(tUpdateLAWageHistorySet,"UPDATE");
        this.mInputData.add(mMMap);
        return true;
    	
    } 
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAWageHistorySet.set((LAWageHistorySet) cInputData.getObjectByObjectName("LAWageHistorySet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        
        if (this.mGlobalInput == null)
        {
            CError tError = new CError();
            tError.moduleName = "UpdateStateBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "mGlobalInput为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (null == this.mLAWageHistorySet||0==this.mLAWageHistorySet.size())
        {
            CError tError = new CError();
            tError.moduleName = "UpdateStateBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "mLAWageHistorySchema为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
        	mInputData.clear();
        	mInputData.add(mMMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UpdateStateBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
  
    public VData getResult()
    {
        return this.mResult;
    }
}

