package com.sinosoft.lis.agentquery;



/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class WageQueryListReport
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //输入的查询sql语句
    private String msql = "";

//取得的机构外部代码
    private String mAgentCom = "";

//取得的代理人离职日期
    private String mAgentComName = "";

//取得的代理人姓名
    private String mBankType = "";

//取得的保单号
    private String mSellFlag = "";
    private String mEndFlag = "";
    private String mBusiLicenseCode = "";
    //取
        private String mAgentCode = "";

//取
        private String mAgentName = "";


//取得的管理机构代码
    private String mManageCom = "";
//取得当前日期
    private String mdate = "";
 //取得当期时间
    private String mtime = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
//    private LJAGetSchema mLJAGetSchema = new LJAGetSchema();
//    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
//    private LDComSchema mLDComSchema = new LDComSchema();
 //取得所属年月起期
    private String mStartMonth = "";
 //取得所属年月止期
    private String mEndMonth  = "";
 //取得所属年月
    private String mMonth  = "";
 //取得保单号
    private String mContNo = "";
 //取得险种号
    private String mRiskCode = "";
 //取得套餐编码
    private String mWrapCode = "";
  //取得时间
    private String YYMMDD="";

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
    	System.out.println("...................lacomqueryreport:here begin submit");
    	if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
    	 mGlobalInput.setSchema((GlobalInput) cInputData.
                 getObjectByObjectName("GlobalInput", 0));
    	mManageCom = (String)cInputData.get(0);
    	mStartMonth = (String)cInputData.get(1);
    	mEndMonth = (String)cInputData.get(2);
    	mMonth = (String)cInputData.get(3);
    	mContNo = (String)cInputData.get(4);
    	mRiskCode = (String)cInputData.get(5);
    	mAgentCode = (String)cInputData.get(6);
    	mAgentName= (String)cInputData.get(7);
    	mWrapCode= (String)cInputData.get(8);
    	
    	System.out.println("...................WageQueryList:here begin getinputdata");
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
    	System.out.println("...................WageQueryList:here begin getPtintData");
    	TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport txmlexport = new XmlExport(); //新建一个XmlExport的实例
        txmlexport.createDocument("LAGRPWageQueryList4.vts", "printer"); //最好紧接着就初始化xml文档
        System.out.println("得到文件"+txmlexport.getDocument());
        ListTable tListTable = new ListTable();
        tListTable.setName("LAGRPWageQueryList4");
        String[] title = {"", "", "", "", "" , "", "", "","","" ,"","","","","","","","","","","","",""};

       //查询所有的记录
        /*
        msql = "select a.branchattr aa,a.agentcode ab,b.name,"
        	  +"(select agentgrade from latree bb where bb.agentcode=a.agentcode),a.grpcontno ac,a.p11,a.riskcode,"
        	  +"(select riskname from lmrisk where lmrisk.riskcode=a.riskcode),a.transmoney,a.FYCRate,a.fyc,"
        	  +" a.fyc*(1-d.comcode) ,"
        	  +"(select codename from ldcode where codetype='payintv' and int(code)=a.payintv),a.payyears,a.paycount,"
        	  +"a.caldate,a.commisionsn,(select InDueFormDate from latree where a.agentcode=agentcode),"
        	  +"(select nvl(sum(feevalue),0) from lcgrpfee where feecode='000002' and grppolno='a.grppolno'),"
        	  +"p7 "
        	  +" from LACommision a,LAAgent b ,ldcode d"
        	  +" where  a.agentcode=b.agentcode  and a.riskcode=d.riskcode  and d.CODETYPE='GrpRiskCodeRate'"
        	  +" and a.grpcontno<>'00000000000000000000'  ";	
        	  if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
              {
           	   msql += " and a.AgentCode='"+this.mAgentCode+"'";
              }
             if(this.mAgentName!=null&&!this.mAgentName.equals(""))
             {
     	       msql += " and b.Name='"+this.mAgentName+"'";
              }
             if(this.mContNo!=null&&!this.mContNo.equals(""))
             {
          	   msql += " and a.GRPContNo='"+this.mContNo+"'";
             }
             if(this.mMonth!=null&&!this.mMonth.equals(""))
             {
          	   msql += " and a.WageNo='"+this.mMonth+"'";
             }
             if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
             {
          	   msql += " and a.RiskCode='"+this.mRiskCode+"'";
             }
             if(this.mManageCom!=null&&!this.mManageCom.equals(""))
             {
          	   msql += " and a.managecom like '"+this.mManageCom+"%'";
             }
             if(this.mBranchType!=null&&!this.mBranchType.equals(""))
             {
          	   msql += " and b.branchType='"+this.mBranchType+"'";
             }
             if(this.mBranchType2!=null&&!this.mBranchType2.equals(""))
             {
          	   msql += " and b.branchType2='"+this.mBranchType2+"'";
             }
        	msql+=" union  select a.branchattr aa,a.agentcode ab,b.name,"
        	  +"(select agentgrade from latree bb where bb.agentcode=a.agentcode),a.grpcontno ac,a.p11,a.riskcode,"
        	  +"(select riskname from lmrisk where lmrisk.riskcode=a.riskcode),a.transmoney,a.FYCRate,a.fyc,"
        	  +" a.fyc "
        	  +"(select codename from ldcode where codetype='payintv' and int(code)=a.payintv),a.payyears,a.paycount,"
        	  +"a.caldate,a.commisionsn,(select InDueFormDate from latree where a.agentcode=agentcode),"
        	  +"(select nvl(sum(feevalue),0) from lcgrpfee where feecode='000002' and grppolno='a.grppolno'),"
        	  +"p7 ,a.F3"
        	  +" from LACommision a,LAAgent b "
        	  +" where  a.agentcode=b.agentcode  and a.riskcode not in (select code from LDCODE where CODETYPE='GrpRiskCodeRate')  and a.grpcontno<>'00000000000000000000' ";	
        	  if(this.mManageCom!=null&&!this.mManageCom.equals(""))
              {
           	   msql += " and a.managecom like '"+this.mManageCom+"%'";
              }
              if(this.mAgentCom!=null&&!this.mAgentCom.equals(""))
              {
     	      msql += " and a.AgentCom='"+this.mAgentCom+"'";
              }
              if(this.mAgentName!=null&&!this.mAgentName.equals(""))
              {
           	   msql += " and b.Name='"+this.mAgentName+"'";
              }
              if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
              {
           	   msql += " and a.AgentCode='"+this.mAgentCode+"'";
              }
              if(this.mContNo!=null&&!this.mContNo.equals(""))
              {
           	   msql += " and a.GRPContNo='"+this.mContNo+"'";
              }
              if(this.mMonth!=null&&!this.mMonth.equals(""))
              {
           	   msql += " and a.WageNo='"+this.mMonth+"'";
              }
              if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
              {
           	   msql += " and a.RiskCode='"+this.mRiskCode+"'";
              }
              if(this.mWrapCode!=null&&!this.mWrapCode.equals(""))
              {
           	   msql += " and a.F3='"+this.mWrapCode+"'";
              }
              if(this.mBranchType!=null&&!this.mBranchType.equals(""))
              {
           	   msql += " and b.branchType='"+this.mBranchType+"'";
              }
              if(this.mBranchType2!=null&&!this.mBranchType2.equals(""))
              {
           	   msql += " and b.branchType2='"+this.mBranchType2+"'";
              }
        		          msql+= " order by aa,ab,ac";
        	
        
        msql = "select a.agentgroup aa,a.branchattr,"     
        +"(select ff.name from labranchgroup ff where ff.branchType='2' and ff.branchtype2='01' and ff.branchattr=a.branchattr),"      
        +"a.agentcode ab,b.name,"
        +"(select agentgrade from latree aa where aa.agentcode=a.agentcode),"      
        +"a.Grpcontno ac,a.p11, a.riskcode,"     
        +"(select riskname from lmrisk where lmrisk.riskcode=a.riskcode),"
        +"a.transmoney,a.FYCRate,a.fyc," 
        +"a.fyc*(1-(value((select value(double(comcode),0) from ldcode where codetype='GrpRiskCodeRate' and code=a.riskcode),0))),"    
        +"(select codename from ldcode where codetype='payintv' and int(code)=a.payintv),a.payyears, "
        +"a.paycount,a.caldate,(select InDueFormDate from latree where a.agentcode=agentcode),"      
        +"(select nvl(sum(feevalue),0) from lcgrpfee where feecode='000002' and  grppolno='a.grppolno'),"      
        +"p7,a.F3  from LACommision a,LAAgent b"      
        +"where a.agentcode=b.agentcode  and a.grpcontno<>'00000000000000000000' and "
        +"b.branchType='2' and b.branchType2='01' and a.branchType='2'  and  a.branchType2='01'";
       if(this.mManageCom!=null&&!this.mManageCom.equals(""))
        {
         	msql += " and a.managecom like '"+this.mManageCom+"%'";
        }
        if(this.mMonth!=null&&!this.mMonth.equals(""))
        {
     	   msql += " and a.WageNo='"+this.mMonth+"'";
        }      
       if(this.mContNo!=null&&!this.mContNo.equals(""))
        {
          	   msql += " and a.GRPContNo='"+this.mContNo+"'";
        } 
       if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
       {
    	   msql += " and a.RiskCode='"+this.mRiskCode+"'";
       }
       if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
       {
    	   msql += " and a.AgentCode='"+this.mAgentCode+"'";
       } 
       if(this.mAgentName!=null&&!this.mAgentName.equals(""))
       {
    	   msql += " and b.Name='"+this.mAgentName+"'";
       }
       msql+= " order by aa,ab,ac";
       */
        /*
        msql =" select  a.wageno, a.branchattr, " 
            +" (select ff.name from labranchgroup ff where ff.branchtype='1'  and ff.branchtype2='01' and ff.branchattr=a.branchattr),  "   
            +" a.agentcode ab, b.name, a.contno ac,  a.p11,a.p13,  a.riskcode," 
            +" (select riskname from lmrisk where  lmrisk.riskcode=a.riskcode),a.transmoney,  "    
            +" a.FYCRate,   a.fyc,  (select codename from ldcode where codetype='payintv'  and int(code)=a.payintv),"  
            +" a.payyears,   a.paycount,    a.tconfdate,  "               
            +" a.customgetpoldate, a.getpoldate,a.makepoldate,  "   
            +" (case when (a.payyear>0 and a.renewcount=0) then '续期' when a.renewcount>0    then '续保' else '新单' end), "  
            +"  case  when b.agentstate<='02'  then '在职'  when b.agentstate>'02'  and  b.agentstate<'06' then '离职未确认'  else '离职' end ,a.F3"                   
            +" from LACommision a,LAAgent b    where     a.agentcode=b.agentcode       "        
            +" and  b.branchType='1'      and b.branchType2='01' "     
            +" and  a.branchType='1'      and a.branchType2='01' " ;
            if(this.mManageCom!=null&&!this.mManageCom.equals(""))
            {
             	msql += " and a.managecom like '"+this.mManageCom+"%'";
            }
            if(this.mStartMonth!=null&&!this.mStartMonth.equals(""))
            {
   	      msql += " and a.wageno>='"+this.mStartMonth+"'";
            }
            if(this.mEndMonth!=null&&!this.mEndMonth.equals(""))
            {
   	      msql += " and a.wageno<='"+this.mEndMonth+"'";
            }
            if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
            {
         	   msql += " and a.AgentCode='"+this.mAgentCode+"'";
            }
            if(this.mAgentName!=null&&!this.mAgentName.equals(""))
            {
         	   msql += " and b.Name='"+this.mAgentName+"'";
            }
            if(this.mContNo!=null&&!this.mContNo.equals(""))
            {
              	   msql += " and a.ContNo='"+this.mContNo+"'";
            } 
            if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
            {
         	   msql += " and a.RiskCode='"+this.mRiskCode+"'";
            }                                                                                
            if(this.mManageCom!=null&&!this.mManageCom.equals(""))
            {
             	msql += " and a.managecom = '"+this.mManageCom+"'";
            }
            if(this.mWrapCode!=null&&!this.mWrapCode.equals(""))
            {
         	   msql += " and a.F3='"+this.mWrapCode+"'";
            }
           msql+= " union all select  a.wageno, a.branchattr, " 
            +" (select ff.name from labranchgroup ff where ff.branchtype='1'  and ff.branchtype2='01' and ff.branchattr=a.branchattr),  "   
            +" a.agentcode ab, b.name, a.contno ac,  a.p11,a.p13,  a.riskcode," 
            +" (select riskname from lmrisk where  lmrisk.riskcode=a.riskcode),a.transmoney,  "    
            +" a.FYCRate,   a.fyc,  (select codename from ldcode where codetype='payintv'  and int(code)=a.payintv),"  
            +" a.payyears,   a.paycount,    a.tconfdate,  "               
            +" a.customgetpoldate, a.getpoldate,a.makepoldate,  "   
            +" (case when (a.payyear>0 and a.renewcount=0) then '续期' when a.renewcount>0    then '续保' else '新单' end), "  
            +"  case  when b.agentstate<='02'  then '在职'  when b.agentstate>'02'  and  b.agentstate<'06' then '离职未确认'  else '离职' end ,a.F3"                   
            +" from LACommision a,LAAgent b    where     a.agentcode=b.agentcode       "         
            +" and  a.branchType='1'      and a.branchType2='03' " ;
            if(this.mManageCom!=null&&!this.mManageCom.equals(""))
            {
             	msql += " and a.managecom like '"+this.mManageCom+"%'";
            }
            if(this.mStartMonth!=null&&!this.mStartMonth.equals(""))
            {
   	      msql += " and a.wageno>='"+this.mStartMonth+"'";
            }
            if(this.mEndMonth!=null&&!this.mEndMonth.equals(""))
            {
   	      msql += " and a.wageno<='"+this.mEndMonth+"'";
            }
            if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
            {
         	   msql += " and a.AgentCode='"+this.mAgentCode+"'";
            }
            if(this.mAgentName!=null&&!this.mAgentName.equals(""))
            {
         	   msql += " and b.Name='"+this.mAgentName+"'";
            }
            if(this.mContNo!=null&&!this.mContNo.equals(""))
            {
              	   msql += " and a.ContNo='"+this.mContNo+"'";
            } 
            if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
            {
         	   msql += " and a.RiskCode='"+this.mRiskCode+"'";
            }                                                                                
            if(this.mManageCom!=null&&!this.mManageCom.equals(""))
            {
             	msql += " and a.managecom = '"+this.mManageCom+"'";
            }
            if(this.mWrapCode!=null&&!this.mWrapCode.equals(""))
            {
         	   msql += " and a.F3='"+this.mWrapCode+"'";
            }
            
            */
             msql =" select  a.wageno, a.branchattr, " 
            +" (select ff.name from labranchgroup ff where (ff.branchtype='1'  and ff.branchtype2='01')  and ff.branchattr=a.branchattr),  "   
            +" a.agentcode ab, b.name, a.contno ac,  a.p11,a.p13,  a.riskcode," 
            +" (select riskname from lmrisk where  lmrisk.riskcode=a.riskcode),a.transmoney,  "    
            +" a.FYCRate,   a.fyc,  (select codename from ldcode where codetype='payintv'  and int(code)=a.payintv),"  
            +" a.payyears,   a.paycount,    a.tconfdate,  "               
            +" a.customgetpoldate, a.getpoldate,a.makepoldate,  "   
            +" (case when (a.payyear>0 and a.renewcount=0) then '续期' when a.renewcount>0    then '续保' else '新单' end), "  
            +"  case  when b.agentstate<='02'  then '在职'  when b.agentstate>'02'  and  b.agentstate<'06' then '离职未确认'  else '离职' end ,a.F3"                   
            +" from LACommision a,LAAgent b    where     a.agentcode=b.agentcode       "        
            +" and  ( (b.branchType='1'      and b.branchType2='01' "     
            +" and  a.branchType='1'      and a.branchType2='01')  or (a.branchtype = '1' and a.branchtype2 = '03'))"   ;
            if(this.mManageCom!=null&&!this.mManageCom.equals(""))
            {
             	msql += " and a.managecom like '"+this.mManageCom+"%'";
            }
            if(this.mStartMonth!=null&&!this.mStartMonth.equals(""))
            {
   	      msql += " and a.wageno>='"+this.mStartMonth+"'";
            }
            if(this.mEndMonth!=null&&!this.mEndMonth.equals(""))
            {
   	      msql += " and a.wageno<='"+this.mEndMonth+"'";
            }
            if(this.mAgentCode!=null&&!this.mAgentCode.equals(""))
            {
         	   msql += " and a.AgentCode='"+this.mAgentCode+"'";
            }
            if(this.mAgentName!=null&&!this.mAgentName.equals(""))
            {
         	   msql += " and b.Name='"+this.mAgentName+"'";
            }
            if(this.mContNo!=null&&!this.mContNo.equals(""))
            {
              	   msql += " and a.ContNo='"+this.mContNo+"'";
            } 
            if(this.mRiskCode!=null&&!this.mRiskCode.equals(""))
            {
         	   msql += " and a.RiskCode='"+this.mRiskCode+"'";
            }                                                                                
            if(this.mManageCom!=null&&!this.mManageCom.equals(""))
            {
             	msql += " and a.managecom = '"+this.mManageCom+"'";
            }
            if(this.mWrapCode!=null&&!this.mWrapCode.equals(""))
            {
         	   msql += " and a.F3='"+this.mWrapCode+"'";
            }
           msql+=" order by  ab,ac  " ;  
           
       
      ExeSQL mExeSQL = new ExeSQL();
      SSRS mSSRS = new SSRS();
      mSSRS = mExeSQL.execSQL(msql);
      System.out.println(msql);
      if (mSSRS.mErrors.needDealError()) {
          CError tCError = new CError();
          tCError.moduleName = "WageQueryList.java";
          tCError.functionName = "getPrintData";
          tCError.errorMessage = "查询XML数据出错！";

          this.mErrors.addOneError(tCError);

          return false;

      }
      if (mSSRS.getMaxRow() <= 0) {
          CError tCError = new CError();
          tCError.moduleName = "WageQueryList.java";
          tCError.functionName = "getPrintData";
          tCError.errorMessage = "没有符合条件的信息！";

          this.mErrors.addOneError(tCError);

          return false;
      }
      if(mSSRS.getMaxRow()>=1)
      {
    	 for(int i=1;i<=mSSRS.getMaxRow();i++)
    	 {
    	  String Info[] = new String[30];
    	  //System.out.println(mSSRS.GetText(i, 1));
    	  Info[0] = mSSRS.GetText(i, 1);
    	  //System.out.println(mSSRS.GetText(i, 2));
    	  Info[1] = mSSRS.GetText(i, 2);
    	  Info[2] = mSSRS.GetText(i, 3);
    	  Info[3] = mSSRS.GetText(i, 4);
    	  Info[4] = mSSRS.GetText(i, 5);
    	  Info[5] = mSSRS.GetText(i, 6);
    	  Info[6] = mSSRS.GetText(i, 7);
    	  Info[7] = mSSRS.GetText(i, 8);
          Info[8] = mSSRS.GetText(i, 9);
          Info[9] = mSSRS.GetText(i, 10);
          Info[10] = mSSRS.GetText(i, 11);
          Info[11] = mSSRS.GetText(i, 12);
          Info[12] = mSSRS.GetText(i, 13);
          Info[13] = mSSRS.GetText(i, 14);
          Info[14] = mSSRS.GetText(i, 15);
          Info[15] = mSSRS.GetText(i, 16);
          Info[16] = mSSRS.GetText(i, 17);
          Info[17] = mSSRS.GetText(i, 18);
          Info[18] = mSSRS.GetText(i, 19);
          Info[19] = mSSRS.GetText(i, 20);
          Info[20] = mSSRS.GetText(i, 21);
          Info[21] = mSSRS.GetText(i, 22);
          Info[22] = mSSRS.GetText(i, 23);
          
    	  tListTable.add(Info);
    	 }
      }
      else {
          CError tCError = new CError();
          tCError.moduleName = "CreateXml";
          tCError.functionName = "creatFile";
          tCError.errorMessage = "没有符合条件的信息！";
          this.mErrors.addOneError(tCError);
          return false;
      }
      ExeSQL tExeSQL = new ExeSQL();
      String sql="select name from ldcom where comcode='"+mManageCom+"'";
      String tName = tExeSQL.getOneValue(sql);
      
      YYMMDD = mStartMonth.substring(0,4) + "年"+mStartMonth.substring(4,6) + "月";
        //mdate = PubFun.getCurrentDate();
        //mtime = PubFun.getCurrentTime();

         texttag.add("MakeDate", YYMMDD); //日期
         texttag.add("Name", tName);
        if (texttag.size() > 0)
        {
            txmlexport.addTextTag(texttag);
        }
        //System.out.println(".............tListTable.size()"+texttag.size());
        //System.out.println(".............tListTable.size()"+tListTable.size());
        txmlexport.addListTable(tListTable,title);
        mResult.clear();
        mResult.addElement(txmlexport);
        System.out.println("...................exit java file here ");
        return true;
    }

    private String getComName(String strComCode)
    {
        LDCodeDB tLDCodeDB = new LDCodeDB();

        tLDCodeDB.setCode(strComCode);
        tLDCodeDB.setCodeType("station");

        if (!tLDCodeDB.getInfo())
        {
            mErrors.copyAllErrors(tLDCodeDB.mErrors);
            return "";
        }
        return tLDCodeDB.getCodeName();
    }
}



