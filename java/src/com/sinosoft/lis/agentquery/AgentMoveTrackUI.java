/*
 * <p>ClassName: AgentMoveTrackUI </p>
 * <p>Description: AgentMoveTrackUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2003-07-16
 */
package com.sinosoft.lis.agentquery;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class AgentMoveTrackUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private String mTableFlag;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    //private LAAttendSet mLAAttendSet=new LAAttendSet();
    public AgentMoveTrackUI()
    {
    }

    /**
     *传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        AgentMoveTrackBL tAgentMoveTrackBL = new AgentMoveTrackBL();
        System.out.println("Start AgentMoveTrack UI Submit...");
        tAgentMoveTrackBL.submitData(mInputData, mOperate);
        System.out.println("End AgentMoveTrack UI Submit...");
        //如果有需要处理的错误，则返回
        if (tAgentMoveTrackBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tAgentMoveTrackBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "AgentMoveTrackUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mOperate.equals("QUERY||MAIN"))
        {
            //this.mResult.clear();
            this.mResult = tAgentMoveTrackBL.getResult();
        }
        mInputData = null;
        return true;
    }

    public static void main(String[] args)
    {
        LAAgentSchema mLAAgentSchema = new LAAgentSchema();
        String tTableFlag = "1";
        mLAAgentSchema.setManageCom("86110000");
        mLAAgentSchema.setAgentCode("8611000014");
        mLAAgentSchema.setName(null);
        mLAAgentSchema.setBranchType("1");
        GlobalInput tG = new GlobalInput();
        tG.Operator = "jojo";
        tG.ManageCom = "86110000";
        VData tVData = new VData();
        tVData.add(tG);
        tVData.add(tTableFlag);
        tVData.addElement(mLAAgentSchema);
        AgentMoveTrackUI tAMTBL = new AgentMoveTrackUI();
        if (!tAMTBL.submitData(tVData, "QUERY||MAIN"))
        {
            System.out.println(tAMTBL.mErrors.getFirstError());
        }
        else
        {
            System.out.println("---------ok--------------");
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(this.mGlobalInput);
            mInputData.add(this.mTableFlag);
            mInputData.add(this.mLAAgentSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentMoveTrackUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mTableFlag = (String) cInputData.get(1);
        this.mLAAgentSchema.setSchema((LAAgentSchema) cInputData.
                                      getObjectByObjectName("LAAgentSchema", 2));
        if (mGlobalInput == null || this.mTableFlag == null ||
            this.mTableFlag.equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentMoveTrackUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}