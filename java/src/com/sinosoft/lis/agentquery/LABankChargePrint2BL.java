package com.sinosoft.lis.agentquery;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.*;
import java.text.DecimalFormat;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class LABankChargePrint2BL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;

    private String mSql = null;

    private String AgentCom = null;

    private String ManageCom = null;

    private String mOutXmlPath = null;

    private String WageNo = null;

    private String ChargeState = null;
    private String mCurTime = PubFun.getCurrentTime();
    private String mCurDate = PubFun.getCurrentDate();

    SSRS tSSRS = new SSRS();

    public LABankChargePrint2BL()
    {
    }

    public boolean submitData(VData cInputData, String operate)
    {

        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        tSSRS = tExeSQL.execSQL(mSql);

        if (tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LABankChargePrint2BL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        String[][] mToExcel = new String[tSSRS.getMaxRow() + 15][30];
        mToExcel[0][0] = "保险代理中介业务手续费 结算表（"+ WageNo.substring(0, 4)+"年 " + WageNo.substring(4, 6) + "月）";
        mToExcel[1][0] = "被代理单位名称:中国人民健康保险股份有限公司广东公司";
        mToExcel[2][0] = "中介费用项目：中介业务手续费";
        mToExcel[3][0] = "银行代理网点名称："+ getName(AgentCom);
        mToExcel[3][8] = "NO:"+mCurDate+" "+mCurTime;
        
        //mToExcel[6][0] = "机构";
        mToExcel[4][0] = "投保人";
        mToExcel[4][1] = "保单编号/批单号";
        mToExcel[4][2] = "投保日期";
        mToExcel[4][3] = "生效日期";
        mToExcel[4][4] = "险种代码";        
        mToExcel[4][5] = "保费";
        mToExcel[4][6] = "提取比例";
        mToExcel[4][7] = "手续费";
        mToExcel[4][8] = "保险公司支付日期";
        
        float sum =0;
        float sumCharge=0;
        
        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row +4][col - 1] = tSSRS.GetText(row, col);
                if (col == 3 && (tSSRS.GetText(row, col) == null || tSSRS.GetText(row, col).equals(""))) {
                    String poldateSql = "select polapplydate from lbcont where contno='" + tSSRS.GetText(row, 10) + "' " +
                            "union all " +
                            "select polapplydate from lcgrpcont where grpcontno='" + tSSRS.GetText(row, 10) + "' " +
                            "union all " +
                            "select polapplydate from lbgrpcont where grpcontno='" + tSSRS.GetText(row, 10) + "'";
                    System.out.println(poldateSql);
                    SSRS tSSRS = tExeSQL.execSQL(poldateSql);
                    for (int i = 1; i <= tSSRS.getMaxRow() ; i++) {
                        if (tSSRS.GetText(i,1) == null || tSSRS.GetText(i,1).equals("")) {
                            continue;
                        }
                        mToExcel[row +4][col - 1] = tSSRS.GetText(1,1);
                    }
                    
                }
                if (col == 6) {
                    sum = sum + Float.parseFloat(tSSRS.GetText(row, col));
                }
                if (col == 8) {
                    sumCharge = sumCharge + Float.parseFloat(tSSRS.GetText(row, col));
                }
            }
        }
        
        String sql = "select name,bankaccno from lacom where AgentCom='" + AgentCom + "' with ur";
        System.out.println(sql);
        SSRS tSSRS2 = tExeSQL.execSQL(sql);
        String agentName = tSSRS2.GetText(1,1);
        String bankAccNo = tSSRS2.GetText(1,2);
        
        mToExcel[tSSRS.getMaxRow()+5][0] = "合计:";
        mToExcel[tSSRS.getMaxRow()+5][5] = sum + "";
        mToExcel[tSSRS.getMaxRow()+5][7] = sumCharge + "";
        
        mToExcel[tSSRS.getMaxRow()+7][0] = agentName + "银行代理网点手续费结算账号信息：";
        mToExcel[tSSRS.getMaxRow()+8][0] = "户名：";
        mToExcel[tSSRS.getMaxRow()+9][0] = "帐号：" + bankAccNo;        
        mToExcel[tSSRS.getMaxRow()+10][0] = "开户行：";
        mToExcel[tSSRS.getMaxRow()+12][1] = "银行网点制表：";

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName(
                "TransferData", 0);

        if (mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LABankChargePrint2BL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        ManageCom = (String) tf.getValueByName("ManageCom");
        AgentCom = (String) tf.getValueByName("AgentCom");
        WageNo = (String) tf.getValueByName("WageNo");
        
        String confDate = WageNo.substring(0, 4) + "-" + WageNo.substring(4, 6) + "-01";
        String startDate = PubFun.calFLDate(confDate)[0];
        String endDate = PubFun.calFLDate(confDate)[1];
        mSql = "select b.p11,a.contno," +
               "(select polapplydate from lccont where contno=a.contno),b.cvalidate, " +
                "a.riskcode,a.transmoney,a.chargerate,a.charge,c.confdate,a.contno " +
                "from lacharge a,lacommision b,LJAGet c " +
                "where a.commisionsn=b.commisionsn and a.receiptno=c.otherno and a.managecom like '" + ManageCom + "%' " +
                "and a.agentcom like '" + AgentCom + "%' and c.confdate between '" + startDate.trim() + "' and '" + endDate.trim() +
                "' and (c.finstate!='FC' or c.finstate is null) and c.confdate is not null  and c.sumgetmoney<>0  and c.OtherNoType in ('AC','BC') ";

       mSql = mSql + "with ur";
       System.out.println("mSql----->" + mSql);
        if (mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "LABankChargePrint2BL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }
    private String getName(String pmAgentCom)
    {
      String tSQL = "";
      String tRtValue="";
      tSQL = "select  name from lacom where agentcom='" + pmAgentCom +"'";
      SSRS tSSRS = new SSRS();
      ExeSQL tExeSQL = new ExeSQL();
      tSSRS = tExeSQL.execSQL(tSQL);
      if(tSSRS.getMaxRow()==0)
      {
      tRtValue="";
      }
      else
      tRtValue=tSSRS.GetText(1, 1);
      return tRtValue;
  }

    public static void main(String[] args)
    {
    }
}
