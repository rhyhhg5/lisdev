/*
 * <p>ClassName: LAIndexTempQueryBL </p>
 * <p>Description: LAIndexTempQueryBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2003-07-16
 */
package com.sinosoft.lis.agentquery;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.db.LAStatSegmentDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAIndexInfoSchema;
import com.sinosoft.lis.vschema.LAIndexInfoSet;
import com.sinosoft.lis.vschema.LAStatSegmentSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LAIndexTempQueryBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    private String mManageCom;
    private String mAgentCode;
    private String mAgentGrade;
    private String mStartDate;
    private String mEndDate;
    //增加两个变量，用来记录统计期间
    private String mStartNo;
    private String mEndNo;
    //统计间隔月数
    private String mIntvl;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAIndexInfoSet mLAIndexInfoSet = new LAIndexInfoSet();
    public LAIndexTempQueryBL()
    {
    }

    /**
     *传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        if (mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        mInputData = null;
        return true;
    }

    public static void main(String[] args)
    {
        LAIndexTempQueryBL tLAIndexTempQueryBL = new LAIndexTempQueryBL();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "aa";
        tGlobalInput.ManageCom = "86110000";

        String tManageCom = "86110000";
        String tAgentCode = "8611000001";
        String tAgentGrade = "A08";
        String tStartDate = "2004-02-26";
        String tEndDate = "2004-03-01";
        VData tInputData = new VData();
        tInputData.add(tGlobalInput);
        tInputData.add(tManageCom);
        tInputData.add(tAgentCode);
        tInputData.add(tAgentGrade);
        tInputData.add(tStartDate);
        tInputData.add(tEndDate);
        tLAIndexTempQueryBL.submitData(tInputData, "QUERY||MAIN");

    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(this.mGlobalInput);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = false;
        //此处增加一些校验代码
        //修改：2004-04-14 LL
        //修改内容：对考核预警中起期进行校验
        if (!checkStartDate())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "dealData";
            tError.errorMessage = "校验起期出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        this.mManageCom = (String) cInputData.getObject(1);
        this.mAgentCode = (String) cInputData.getObject(2);
        this.mAgentGrade = (String) cInputData.getObject(3);
        this.mStartDate = (String) cInputData.getObject(4);
        this.mEndDate = (String) cInputData.getObject(5);

        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        /*
                 //校验日期
         this.mStartDate = getFormatDate(this.mStartDate,"yyyy-MM-dd",null);
         this.mEndDate = getFormatDate(this.mEndDate,"yyyy-MM-dd",null);
                 String today = getFormatDate("","yyyy-MM-dd",new Date());
                 if (this.mEndDate.compareTo(today)>0)
                 {
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "计算止期不能超过今天！";
            this.mErrors.addOneError(tError);
            return false;
                 }
         */
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        String tSql = "Select a.AgentCode,"
                      + "a.AgentGroup,"
                      +
                      "(Select BranchCode From LAAgent Where AgentCode = a.AgentCode) "
                      + "From LATree a Where ManageCom = '" + this.mManageCom +
                      "' "
                      + "And AgentGrade = '" + mAgentGrade + "'";
        if (this.mAgentCode != null && !this.mAgentCode.equals(""))
        {
            tSql += " And a.AgentCode = '" + this.mAgentCode + "'";
        }
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(tSql);
        if (tExeSQL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "submitquery";
            tError.errorMessage = "查询失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tSSRS.getMaxRow() < 1)
        {
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "submitquery";
            tError.errorMessage = "不存在符合条件的代理人！";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            String[][] aAgentInfo = tSSRS.getAllData();
            for (int i = 0; i < aAgentInfo.length; i++)
            {
                if (!queryIndexValue(aAgentInfo[i]))
                {
                    return false;
                }
            }
        }
        this.mResult.add(this.mLAIndexInfoSet);
        return true;
    }

    /**
     * 查询每个代理人的指标值
     * 职级不同则对应的指标也不同
     */
    private boolean queryIndexValue(String[] cAgentInfo)
    {
        String tSql = "", tIndexValue = "";
        ExeSQL tExeSQL;
        String tAgentCode = cAgentInfo[0];
        String tAgentGroup = cAgentInfo[1];
        String tBranchCode = cAgentInfo[2];
        LAIndexInfoSchema tLAIndexSchema = new LAIndexInfoSchema();
        tLAIndexSchema.setAgentCode(tAgentCode);

        //修改：2004-04-14 LL
        //修改了统计方法，采用WageNo进行统计
        //获得统计期间止期
        if (!getEndNo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "queryIndexValue";
            tError.errorMessage = "处理止期出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        // 指标：个人客户数 个人合计FYC
        if (this.mAgentGrade.compareTo("A08") < 0)
        {
            //--客户数
//            tSql = "Select IndCustSum('"+tAgentCode+"','"+this.mStartDate+"','"+this.mEndDate+"') "
//                  +"from ldsysvar where sysvar = 'onerow'";
            //原来的sql
//            tSql = "select trunc(nvl(sum(CalCount),0)) from lacommision where agentcode = '"
//                  +tAgentCode+"' and payyear < 1  and tmakedate >= to_date('"
//                  +mStartDate+"','YYYY-MM-DD') and tmakedate <= to_date('"+mEndDate+"','YYYY-MM-DD')";
            tSql =
                    "select trunc(nvl(sum(CalCount),0)) from lacommision where agentcode = '"
                    + tAgentCode + "' and payyear < 1  and wageno >= '" +
                    this.mStartNo
                    + "' and wageno <= '" + this.mEndNo +
                    "' and GetPolDate < '" + this.mEndDate + "'";
            System.out.println("统计个人客户数SQL" + tSql);
            tExeSQL = new ExeSQL();
            tIndexValue = tExeSQL.getOneValue(tSql);
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                return false;
            }
            tLAIndexSchema.setIndCustSum(tIndexValue);
            System.out.println("--个人客户数cust:" + tIndexValue);
            //--FYC
            //原来的SQL
//            tSql = "Select nvl(sum(fyc),0) From LACommision Where payYear < 1 "
//                  +"And AgentCode = '"+tAgentCode+"' "
//                  +"And tmakedate >= to_date('"+this.mStartDate+"','YYYY-MM-DD') "
//                  +"And tmakedate <= to_date('"+this.mEndDate+"','YYYY-MM-DD')";
            tSql = "Select nvl(sum(fyc),0) From LACommision Where payYear < 1 "
                   + " And AgentCode = '" + tAgentCode + "'"
                   + " and wageno >= '" + this.mStartNo + "'"
                   + " and wageno <= '" + this.mEndNo + "'"
                   + " and GetPolDate < '" + this.mEndDate + "'";
            System.out.println("统计个人合计FYC SQL" + tSql);
            tExeSQL = new ExeSQL();
            tIndexValue = tExeSQL.getOneValue(tSql);
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                return false;
            }
            tLAIndexSchema.setIndFYCSum(tIndexValue);
            System.out.println("--个人合计FYCFyc:" + tIndexValue);
        }
        //指标：直辖组FYC
        if (this.mAgentGrade.compareTo("A03") > 0)
        {
            //--GroupFYC
            //原来的sql
//            tSql = "Select nvl(sum(fyc),0) From LACommision Where payYear < 1 "
//                  +"And BranchCode = '"+tBranchCode+"' "
//                  +"And tmakedate >= to_date('"+this.mStartDate+"','YYYY-MM-DD') "
//                  +"And tmakedate <= to_date('"+this.mEndDate+"','YYYY-MM-DD')";
            tSql = "Select nvl(sum(fyc),0) From LACommision Where payYear < 1 "
                   + "And BranchCode = '" + tBranchCode + "'"
                   + " and wageno >= '" + this.mStartNo + "'"
                   + " and wageno <= '" + this.mEndNo + "'"
                   + " and GetPolDate < '" + this.mEndDate + "'";
            System.out.println("统计直辖组FYC SQL" + tSql);
            tExeSQL = new ExeSQL();
            tIndexValue = tExeSQL.getOneValue(tSql);
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                return false;
            }
            tLAIndexSchema.setDirTeamFYCSum(tIndexValue);
            System.out.println("--直辖组FYC GroupFyc:" + tIndexValue);
        }
        //指标：直辖部FYC
        if (this.mAgentGrade.compareTo("A05") > 0)
        {
            //--DepFYC
            //原来的SQL
//            tSql = "Select nvl(sum(fyc),0) From LACommision Where payYear < 1 "
//                  +"And tmakedate >= to_date('"+this.mStartDate+"','YYYY-MM-DD') "
//                 +"And tmakedate <= to_date('"+this.mEndDate+"','YYYY-MM-DD') "
//                 +"And (trim(Branchattr) like nvl((Select trim(b.Branchattr) "
//                 +"From Labranchgroup b where b.agentgroup = '"+tAgentGroup+"' "
//                 +"And b.BranchLevel = '02' and b.endflag = 'N'),'N') || '%' "
//                 +"Or AgentCode = '"+tAgentCode+"')";
            tSql = "Select nvl(sum(fyc),0) From LACommision Where payYear < 1 "
                   + " and wageno >= '" + this.mStartNo + "'"
                   + " and wageno <= '" + this.mEndNo + "'"
                   + " and GetPolDate < '" + this.mEndDate + "'"
                   +
                   " And trim(Branchattr) like nvl((Select trim(b.Branchattr) "
                   + " From Labranchgroup b where b.branchmanager = '" +
                   tAgentCode + "' "
                   +
                   " And b.BranchLevel = '02' and b.endflag = 'N'),'N') || '%' ";
            System.out.println("统计直辖部FYC SQL" + tSql);
            tExeSQL = new ExeSQL();
            tIndexValue = tExeSQL.getOneValue(tSql);
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                return false;
            }
            tLAIndexSchema.setDepFYCSum(tIndexValue);
            System.out.println("--直辖部FYC DepFyc:" + tIndexValue);
        }
        //指标：直辖部月均FYC
        if (this.mAgentGrade.compareTo("A07") > 0)
        {
            //--AvgDepFYC
            if (!tIndexValue.equals("0"))
            {
                //原来的SQL
//                tSql = "Select round("+tIndexValue+"/decode(months_between(to_date('"+this.mEndDate+"','YYYY-MM-DD')+1,"
//                      +"to_date('"+this.mStartDate+"','YYYY-MM-DD')),0,1,months_between(to_date('"+this.mEndDate+"','YYYY-MM-DD')+1,"
//                      +"to_date('"+this.mStartDate+"','YYYY-MM-DD'))),3) "
//                      +"From Ldsysvar Where sysvar = 'onerow'";
                if (!this.getInterval())
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "LAIndexTempQueryBL";
                    tError.functionName = "queryIndexValue";
                    tError.errorMessage = "获得统计期间间隔出错！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
//                tSql = "Select "+tIndexValue+"/" +this.mIntvl
//                      +" From Ldsysvar Where sysvar = 'onerow'";
//                System.out.println("统计直辖部月均FYC SQL"+tSql);
//                tExeSQL = new ExeSQL();
//                tIndexValue = tExeSQL.getOneValue(tSql);
//                if (tExeSQL.mErrors.needDealError())
//                {
//                    this.mErrors.copyAllErrors(tExeSQL.mErrors);
//                    return false;
//                }
                tIndexValue = String.valueOf(Double.parseDouble(tIndexValue) /
                                             Double.parseDouble(this.mIntvl));
            }
            tLAIndexSchema.setDirDepMonAvgFYC(tIndexValue);
            System.out.println("--直辖部月均FYC AvgDepFyc:" + tIndexValue);
        }
        this.mLAIndexInfoSet.add(tLAIndexSchema);
        return true;
    }

    /**
     * format date
     */
    private String getFormatDate(String cDate, String cFormat, Date today)
    {
        String FormatDate = "";
        FDate fDate = new FDate();
        SimpleDateFormat sfd = new SimpleDateFormat(cFormat);
        if (today == null)
        {
            FormatDate = sfd.format(fDate.getDate(cDate));
        }
        else
        {
            FormatDate = sfd.format(today);
        }
        return FormatDate;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    /**
     * 修改：新增功能
     * 功能：对录入起期进行判断
     * 规则：对1到11月每月只允许录入26日或1月1日
     */
    private boolean checkStartDate()
    {
        System.out.println("Into checkStartDate()函数：");
        System.out.println("统计起期：" + this.mStartDate);
        System.out.println("统计止期：" + this.mEndDate);
        //通过查找lastatsegment表来实现校验功能,如果此日期在表中有记录，
        //则说明日期合法（前提是数据库中数据正确），如果不存在则说明不合法
        String tSql = "select * from lastatsegment where stattype = '5'"
                      + " and startdate = '" + this.mStartDate + "'";
        LAStatSegmentSet tLAStatSegmentSet = new LAStatSegmentSet();
        LAStatSegmentDB tLAStatSegmentDB = new LAStatSegmentDB();

        tLAStatSegmentSet = tLAStatSegmentDB.executeQuery(tSql);
        if (tLAStatSegmentDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAStatSegmentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "checkStartDate";
            tError.errorMessage = "查询LAStatSegment表时出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //如果查询出的数据条目不为1条，则说明有错（为0或大于1都不对）
        if (tLAStatSegmentSet.size() != 1)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAStatSegmentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "checkStartDate";
            tError.errorMessage = "此起期录入不合法，请重新录入!";
            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("统计起期为：" + this.mStartDate);
        System.out.println("统计止期为：" + this.mEndDate);
        //判断 统计止期 是否大于 统计起期
        if (this.mStartDate.compareTo(this.mEndDate) > 0)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAStatSegmentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "checkStartDate";
            tError.errorMessage = "起期大于止期不合法!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //保存统计开始期间
        this.mStartNo = String.valueOf(tLAStatSegmentSet.get(1).getYearMonth());

        return true;
    }

    /**
     * 修改：新增功能
     * 功能：通过录入止期，获得结束统计期间
     * 规则：
     */
    private boolean getEndNo()
    {
        //转化 录入止期 格式 例如：2004-02-21 -> 200402
        String str = AgentPubFun.formatDate(this.mEndDate, "yyyyMM");
        System.out.println("统计止期转化后格式为：" + str);
        //查询LAStatSegment表
        String tSql = "select * from lastatsegment where stattype = '5'"
                      + " and yearmonth = '" + str + "'";
        System.out.println("查询SQL为：" + tSql);
        LAStatSegmentSet tLAStatSegmentSet = new LAStatSegmentSet();
        LAStatSegmentDB tLAStatSegmentDB = new LAStatSegmentDB();

        tLAStatSegmentSet = tLAStatSegmentDB.executeQuery(tSql);
        if (tLAStatSegmentDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAStatSegmentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "getEndNo";
            tError.errorMessage = "查询LAStatSegment表时出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //如果查询出的数据条目为0，则有错误
        if (tLAStatSegmentSet.size() == 0)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAStatSegmentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "getEndNo";
            tError.errorMessage = "此止期录入不合法，请重新录入!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //获得该考核年月止期
        String tEndDate = tLAStatSegmentSet.get(1).getEndDate();
        //比较 录入止期 和 考核年月 止期
        // 大于    ：取下个月
        // 小于等于 ：取本月
        if (this.mEndDate.compareTo(tEndDate) > 0) //大于，取下个月
        {
            //往后推一个月
            String temp = PubFun.calDate(this.mEndDate, 1, "M", null);
            this.mEndNo = AgentPubFun.formatDate(temp, "yyyyMM");
        }
        else
        {
            this.mEndNo = AgentPubFun.formatDate(this.mEndDate, "yyyyMM");
        }

        System.out.println("最终获得的起止统计期间为：");
        System.out.println("统计起期：" + this.mStartDate);
        System.out.println("统计止期：" + this.mEndDate);
        System.out.println("起始期间：" + this.mStartNo);
        System.out.println("终止期间：" + this.mEndNo);

        return true;
    }

    /**
     * 修改：新增功能
     * 功能：计算统计起期与统计止期之间的间隔月数
     * 规则：
     */
    private boolean getInterval()
    {
        String str1 = this.mStartNo.substring(0, 4) + "-"
                      + this.mStartNo.substring(4) + "-01";
        String str2 = this.mEndNo.substring(0, 4) + "-"
                      + this.mEndNo.substring(4) + "-01";
        System.out.println("统计期间转化后的起止期为：");
        System.out.println("起：" + str1);
        System.out.println("止：" + str2);
        int interval = PubFun.calInterval(str1, str2, "M");
        if (interval == 0)
        {
            this.mIntvl = "1";
        }
        else
        {
            this.mIntvl = String.valueOf(interval + 1);
        }
        System.out.println("时间间隔为：" + this.mIntvl);

        return true;
    }
}
