package com.sinosoft.lis.agentquery;

import java.util.Date;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAChargeToNxsLNSchema;
import com.sinosoft.lis.vschema.LAChargeToNxsLNSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/*
 * <p>ClassName: LAChargeToNxsLNBL </p>
 * <p>Description: 提取理赔数据 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: sinosoft </p>
 * @Database: 龙程彬
 * @CreateDate：2008-10-30
 */
public class LAChargeToNxsLNBL {
	


		    /** 错误处理类，每个需要错误处理的类中都放置该类 */
		    public CErrors mErrors = new CErrors();

		    /** 前台传入的公共变量 */
		    private GlobalInput globalInput = new GlobalInput();


		    /** 往后面传输数据的容器 */
//		    private VData mInputData = new VData();

		    /** 数据操作字符串 */
		    private String strOperate = "";
		    private String mToday = "";
		    private String mStartDate = "";
		    private String mEndDate = "";
		    private String mIndexCalNo = "";
		    private TransferData mGetCessData = new TransferData();
		    private MMap tmap = new MMap();
		    private VData mOutputData = new VData();
		    private LAChargeToNxsLNSet mLAChargeToNxsLNSet = new LAChargeToNxsLNSet();
		    private String mBatchNo = "";
		    private String mManageCom = "";
		    //业务处理相关变量
		    /** 全局数据 */

		    public LAChargeToNxsLNBL() {
		    }

		    /**
		     * 提交数据处理方法
		     * @param cInputData 传入的数据,VData对象
		     * @param cOperate 数据操作字符串
		     * @return 布尔值（true--提交成功, false--提交失败）
		     */
		    public boolean submitData(VData cInputData, String cOperate) {
		        System.out.println("Begin LAChargeToNxsLNBL.Submit..............");
		        this.strOperate = cOperate;
		        if (!getInputData(cInputData)) {
		            return false;
		        }
		        if (!dealData()) {
		            return false;
		        }
		        //准备往后台的数据
		        if (!prepareOutputData())
		        {
		            return false;
		        }

		        PubSubmit tPubSubmit = new PubSubmit();
		        tPubSubmit.submitData(mOutputData, "");
		        //如果有需要处理的错误，则返回
		        if (tPubSubmit.mErrors.needDealError())
		        {
		            // @@错误处理
		            //this.mErrors.copyAllErrors(tPubSubmit.mErrors);
		            CError tError = new CError();
		            tError.moduleName = "LAChargeToNxsLNBL";
		            tError.functionName = "submitDat";
		            tError.errorMessage = "数据提交失败!";
		            this.mErrors.addOneError(tError);		      
		            return false;
		        }
		        //System.out.println("LAChargeToNxsLNBL.submitData 执行完啦!准备返回true了!");
		        return true;
		    }


		    
		    
                private boolean getInputData(VData cInputData) {
		    	
		        globalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
		                "GlobalInput", 0));
		        mGetCessData = (TransferData) cInputData.getObjectByObjectName(
		                "TransferData", 0);
	        	ExeSQL tExeSQL = new ExeSQL(); 
	        	try{
		            mIndexCalNo = (String) mGetCessData.getValueByName("IndexCalNo");
		            mManageCom = (String) mGetCessData.getValueByName("ManageCom");
		            String startSql= "select startdate from lastatsegment where stattype='5' and yearmonth = int ('"+mIndexCalNo+"') with ur ";
	        	    SSRS tSSRS1 = tExeSQL.execSQL(startSql);
		            mStartDate = tSSRS1.GetText(1, 1);
		            String endSql= "select enddate from lastatsegment where stattype='5' and yearmonth = int ('"+mIndexCalNo+"') with ur ";
	        	    SSRS tSSRS2 = tExeSQL.execSQL(endSql);
		            mEndDate = tSSRS2.GetText(1, 1);
	        	}catch(Exception e){
	        		buildError("LAChargeToNxsLNBL.java","获取输入信息失败，日期错误");
	        		return false;
	        	}
		        System.out.println("mStartDate:"+mStartDate+" mEndDate:"+mEndDate);
		        
		        //获取批次号
		        if(!this.addMaxBatchNo()){
		        	return false;
		        }		   
		        
		        return true;
		    }
		    /**
		     * 根据前面的输入数据，进行UI逻辑处理
		     * 如果在处理过程中出错，则返回false,否则返回true
		     */
		    private boolean dealData() {
		    	System.out.println(mManageCom);
		    	String currentdate=PubFun.getCurrentDate();
		    	String currenttime=PubFun.getCurrentTime();
		    	String sql = "";
	    		sql=" select distinct a.polno,b.agentcom,a.reportno,b.tmakedate,sum(b.charge) "
                   +" from lktransstatus a,lacharge b "
                   +" where a.polno=b.contno "
                   +" and b.tmakedate=a.transdate "
                   +" and b.chargestate='1' "
                   +" and  b.tmakedate>='"+mStartDate+"' and b.tmakedate<='"+mEndDate+"' "
                   +" and  a.transdate>='"+mStartDate+"' and a.transdate<='"+mEndDate+"' "
                   +" and  a.managecom like '"+mManageCom+"%'" 
                   +" and a.funcflag='01' and a.Status='1' "
                   +" and a.bankcode=(select codealias from ldcode where codetype = 'xinbaotong' and code = '"+mManageCom+"')"
                   +" and not exists (select 1 from LAChargeToNxsLN where contno=b.contno) "
                   +" group by contno,a.polno,b.agentcom,a.reportno,b.tmakedate  "
                   +" with ur ";
		    	try{

		        	ExeSQL tExeSQL = new ExeSQL(); 
		        	SSRS tSSRS = tExeSQL.execSQL(sql);
		        	
		        	if (tSSRS.getMaxRow()==0)
			        {
			            // @@错误处理
			            CError tError = new CError();
			            tError.moduleName = "LAChargeToNxsLNBL";
			            tError.functionName = "submitData";
			            tError.errorMessage = "该月没有未提取的数据!";
			            this.mErrors.addOneError(tError);		      
			            return false;
			        }
		        	
		        	for(int i=1;i<=tSSRS.MaxRow;i++){ 
		        		LAChargeToNxsLNSchema tLAChargeToNxsLNSchema = new LAChargeToNxsLNSchema();
		        		tLAChargeToNxsLNSchema.setBatchNo(mBatchNo);
		        		tLAChargeToNxsLNSchema.setContNo(tSSRS.GetText(i, 1)) ;
		        		tLAChargeToNxsLNSchema.setAgentCom(tSSRS.GetText(i, 2));
		        		tLAChargeToNxsLNSchema.setOutCom(tSSRS.GetText(i, 3));
		        		tLAChargeToNxsLNSchema.setTMakeDate(tSSRS.GetText(i, 4));
		        		tLAChargeToNxsLNSchema.setCharge(tSSRS.GetText(i, 5));
		        		tLAChargeToNxsLNSchema.setMakeDate(currentdate); 
		        		tLAChargeToNxsLNSchema.setMakeTime(currenttime);  
//		        		tLAChargeToNxsLNSchema.setSendDate(currentdate); 
//		        		tLAChargeToNxsLNSchema.setSendTime(currenttime);
		        		tLAChargeToNxsLNSchema.setOperator(globalInput.Operator); 
		        		tLAChargeToNxsLNSchema.setState("0");
		        		mLAChargeToNxsLNSet.add(tLAChargeToNxsLNSchema);
		        	}        	
		    	}catch(Exception ex){
		    		ex.printStackTrace();
		    		return false;
		    	}
		    	return true;
		    }

	
		    
		     /**
		     * 准备后台的数据
		     * @return boolean
		     */
		    private boolean prepareOutputData()
		    {
		        try
		        {
		        	tmap.put(this.mLAChargeToNxsLNSet, "INSERT");
		            this.mOutputData.add(tmap);
		        }
		        catch (Exception ex)
		        {
		            // @@错误处理
		            CError.buildErr(this,"在准备往后层处理所需要的数据时出错。");
		            return false;
		        }
		        return true;
		    }
	
		    
		    //错误信息
		    private void buildError(String szFunc, String szErrMsg) {
		        CError cError = new CError();
		        cError.moduleName = "LAChargeToNxsLNSBL";
		        cError.functionName = szFunc;
		        cError.errorMessage = szErrMsg;
		        this.mErrors.addOneError(cError);
		    }

		    
		    
		    /**
		     * 从输入数据中得到所有对象
		     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
		     */

		    public String getResult() {
		        return "没有传数据";
		    }
		    private boolean addMaxBatchNo(){
		    	try{
		    		String sql=" select Maxno from LDMaxNo where Notype='CHARGETONXS' with ur ";
		        	ExeSQL tExeSQL = new ExeSQL(); 
		        	SSRS tSSRS = tExeSQL.execSQL(sql);
		        	if(tSSRS.getMaxRow()==0){
		        		return false;
		        	}else{
		        		mBatchNo="L"+tSSRS.GetText(1, 1);
		        	}
		            PubSubmit tPubsubmit= new PubSubmit();
		            MMap tmap = new MMap();
		            VData cInputData = new VData();
		            String upSql =" update ldmaxno set Maxno=Maxno+1 where Notype='CHARGETONXS' ";
		            tmap.put(upSql, "UPDATE");
		            cInputData.add(tmap);
		            tPubsubmit.submitData(cInputData,"");
		    	}catch(Exception e){
		    		return false;
		    	}
		    	return true;
		    }		    
		    public static void main(String[] args) {

		    }
}
