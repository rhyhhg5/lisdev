package com.sinosoft.lis.agentquery;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.*;
import java.text.DecimalFormat;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class LAZJReport1BL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql1 = null;
    private String mSql2 = null;
    private String mSql3 = null;
    private String mSql4 = null;
    private String mSql5 = null;
    private String mAssessYear = null;
    private String mManageCom = null;
    private String mAssessMonth = null;
    private String mOutXmlPath = null;

    SSRS tSSRS1=new SSRS();
    SSRS tSSRS2=new SSRS();
    SSRS tSSRS3=new SSRS();
    SSRS tSSRS4=new SSRS();
    SSRS tSSRS5=new SSRS();
    public LAZJReport1BL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS1 = tExeSQL.execSQL(mSql1);
        tSSRS2 = tExeSQL.execSQL(mSql2);
        tSSRS3 = tExeSQL.execSQL(mSql3);
        tSSRS4 = tExeSQL.execSQL(mSql4);
        tSSRS5 = tExeSQL.execSQL(mSql5);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LAZJReport1BL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        String[][] mToExcel = new String[30][30];
        mToExcel[0][0] = "人身保险公司中介业务基本情况";

        mToExcel[1][0] = "报表单位：PICCH保险公司"+getManageComName(mManageCom)+"    "+mAssessYear+"年"+mAssessMonth+"月";
        mToExcel[3][0] = "类别"; 
        mToExcel[3][2] = "数量";
        mToExcel[4][0] = "1、中介机构（家）";        
        mToExcel[4][1] = "专业代理机构";
        mToExcel[4][2] = tSSRS1.GetText(1, 1);
        mToExcel[5][1] = "兼业代理机构";
        mToExcel[5][2] = tSSRS2.GetText(1, 1);
        mToExcel[6][1] = "其中：银邮代理机构";
        mToExcel[6][2] = tSSRS3.GetText(1, 1);
        mToExcel[7][1] = "经纪公司";
        mToExcel[7][2] = tSSRS4.GetText(1, 1);
        mToExcel[8][0] = "2、签订委托代理协议的个人（人）";
        mToExcel[8][2] = tSSRS5.GetText(1, 1);
        mToExcel[9][0] = "3、交叉销售";
        mToExcel[9][2] = "";
        mToExcel[10][0] = "制表：";
        mToExcel[10][1] = "复核：";
        mToExcel[10][2] = "总经理：";
     
        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAZJReport1BL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql1 = (String) tf.getValueByName("querySql1");
        mSql2 = (String) tf.getValueByName("querySql2");
        mSql3 = (String) tf.getValueByName("querySql3");
        mSql4 = (String) tf.getValueByName("querySql4");
        mSql5 = (String) tf.getValueByName("querySql5");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");       
        mManageCom = (String) tf.getValueByName("ManageCom");
        mAssessYear = (String) tf.getValueByName("AssessYear");
        mAssessMonth = (String) tf.getValueByName("AssessMonth");
        if(mSql1 == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAZJReport1BL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    private String getManageComName(String pmManageCom)
    {
    	String tSQL = "";
        String tRtValue="";
    	if(pmManageCom.equals("86")){
        	tRtValue="总公司";	
        }
        else
        {
        	tSQL = "select name from ldcom where comcode='" + pmManageCom.substring(0,4) +"'";
            SSRS tSSRS = new SSRS();
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(tSQL);
            if (tExeSQL.mErrors.needDealError()) {
            this.mErrors.addOneError("管理机构不存在！");
                  }
            tRtValue=tSSRS.GetText(1, 1);
        }   	
        
        return tRtValue;
    }

    /**
    * 执行SQL文查询结果
    * @param sql String
    * @return double
    */
   private double execQuery(String sql)

   {
      Connection conn;
      conn = null;
      conn = DBConnPool.getConnection();

      System.out.println(sql);

      PreparedStatement st = null;
      ResultSet rs = null;
      try {
          if (conn == null)return 0.00;
          st = conn.prepareStatement(sql);
          if (st == null)return 0.00;
          rs = st.executeQuery();
          if (rs.next()) {
              return rs.getDouble(1);
          }
          return 0.00;
      } catch (Exception ex) {
          ex.printStackTrace();
          return -1;
      } finally {
          try {
             if (!conn.isClosed()) {
                 conn.close();
             }
             try {
                 st.close();
                 rs.close();
             } catch (Exception ex2) {
                 ex2.printStackTrace();
             }
             st = null;
             rs = null;
             conn = null;
           } catch (Exception e) {}

      }
     }

    public static void main(String[] args)
    {
        LAZJReport1BL LAZJReport1BL = new
            LAZJReport1BL();
    System.out.println("11111111");
    }
}
