package com.sinosoft.lis.agentquery;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.*;
import java.text.DecimalFormat;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class LABankChargePrint1BL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;

    private String mSql = null;

    private String AgentCom = null;

    private String ManageCom = null;

    private String mOutXmlPath = null;

    private String WageNo = null;

    private String ChargeState = null;
    private String mCurTime = PubFun.getCurrentTime();
    private String mCurDate = PubFun.getCurrentDate();

    SSRS tSSRS = new SSRS();

    public LABankChargePrint1BL()
    {
    }

    public boolean submitData(VData cInputData, String operate)
    {

        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!checkData())
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }

        return true;
    }

    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        tSSRS = tExeSQL.execSQL(mSql);

        if (tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LABankChargePrint1BL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        /*
        String[][] mToExcel = new String[tSSRS.getMaxRow() + 4][30];
        mToExcel[0][0] = "保险代理中介业务手续费计提表 (" + WageNo.substring(0, 4) + "年" + WageNo.substring(4, 6) + "月)";
        mToExcel[0][6] = "编制单位：" + ManageCom;
        mToExcel[1][0] = "机构";
        mToExcel[1][1] = "投保人";
        mToExcel[1][2] = "保单编号/批单号";
        mToExcel[1][3] = "险种代码";
        mToExcel[1][4] = "保费";
        mToExcel[1][5] = "提取比例";
        mToExcel[1][6] = "手续费";
        mToExcel[1][7] = "投保日期";
        mToExcel[1][8] = "生效日期";

        for (int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for (int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row + 1][col - 1] = tSSRS.GetText(row, col);
            }
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName = { PubFun.getCurrentDate() };
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch (Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
        */
        String[][] mToExcel = new String[tSSRS.getMaxRow() + 15][30];
        mToExcel[0][0] = "银保业务手续费计提表一";
        mToExcel[1][0] = "保险代理中介业务手续费计提汇总表（"+WageNo+"）";
        mToExcel[2][0] = "";
        mToExcel[3][0] = "编制单位：中国人民健康保险股份有限公司广东分公司";
        mToExcel[4][0] = "银行代理网点名称："+AgentCom+" "+getName(AgentCom);
        mToExcel[5][5] = "NO:"+mCurDate+" "+mCurTime;
        
        mToExcel[6][0] = "机构";
        mToExcel[6][1] = "投保人";
        mToExcel[6][2] = "保单编号/批单号";
        mToExcel[6][3] = "险种代码";        
        mToExcel[6][4] = "保费";
        mToExcel[6][5] = "提取比例";
        mToExcel[6][6] = "手续费";
        mToExcel[6][7] = "投保日期";
        mToExcel[6][8] = "生效日期";
        
        float sum =0;
        float sumCharge=0;
        
 

        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row +6][col - 1] = tSSRS.GetText(row, col);
                System.out.println(tSSRS.GetText(row, col) + "--" + col);
                if (col==5) {
                    sum = sum + Float.parseFloat(tSSRS.GetText(row, col));
                }
                if (col==7) {
                    sumCharge = sumCharge + Float.parseFloat(tSSRS.GetText(row, col));
                }
            }
        }
        mToExcel[tSSRS.getMaxRow()+7][0] = "合计:";
        mToExcel[tSSRS.getMaxRow()+7][4] = sum + "";
        mToExcel[tSSRS.getMaxRow()+7][6] = sumCharge + "";
        mToExcel[tSSRS.getMaxRow()+9][0] = "总经理:";
        mToExcel[tSSRS.getMaxRow()+9][3] = "分管总经理:";
        mToExcel[tSSRS.getMaxRow()+9][5] = "分公司计财部:";
        mToExcel[tSSRS.getMaxRow()+9][7] = "分公司银保部";        
        mToExcel[tSSRS.getMaxRow()+10][1] = "制表";
        mToExcel[tSSRS.getMaxRow()+10][4] = "日期："+mCurDate;
        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName(
                "TransferData", 0);

        if (mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LABankChargePrint1BL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        ManageCom = (String) tf.getValueByName("ManageCom");
        AgentCom = (String) tf.getValueByName("AgentCom");
        WageNo = (String) tf.getValueByName("WageNo");
        ChargeState = (String) tf.getValueByName("ChargeState");

    /*    mSql = "select a.agentcom,b.p11,a.contno,a.riskcode,a.transmoney,a.chargerate,a.charge,"
                + "(select polapplydate from lccont where contno=a.contno),b.cvalidate "
                + "from lacharge a,lacommision b "
                + "where a.commisionsn=b.commisionsn "
                + "and a.wageno ='200901' "
                + "and a.chargestate='0' fetch first 10 rows only with ur"; */
        
        mSql = "select a.agentcom,b.p11,a.contno,a.riskcode,a.transmoney,a.chargerate,a.charge," +
                "(select polapplydate from lccont where contno=a.contno),b.cvalidate " +
                "from lacharge a,lacommision b " +
                "where a.commisionsn=b.commisionsn and a.managecom like '" + ManageCom + "%' " +
                "and a.agentcom like '" + AgentCom + "%' and a.wageno ='" + WageNo + "' ";
        if(!(ChargeState ==null || ChargeState.equals(""))){
            mSql = mSql + "and a.chargestate='" + ChargeState + "' ";
       }
       mSql = mSql + "with ur";
       System.out.println("mSql----->" + mSql);
        if (mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "LABankChargePrint1BL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }
    private String getName(String pmAgentCom)
    {
      String tSQL = "";
      String tRtValue="";
      tSQL = "select  name from lacom where agentcom='" + pmAgentCom +"'";
      SSRS tSSRS = new SSRS();
      ExeSQL tExeSQL = new ExeSQL();
      tSSRS = tExeSQL.execSQL(tSQL);
      if(tSSRS.getMaxRow()==0)
      {
      tRtValue="";
      }
      else
      tRtValue=tSSRS.GetText(1, 1);
      return tRtValue;
  }

    public static void main(String[] args)
    {
    }
}
