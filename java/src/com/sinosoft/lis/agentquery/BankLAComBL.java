/*
 * <p>ClassName: LAWelfareInfoBL </p>
 * <p>Description: LAWelfareInfoBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 代理机构
 * @CreateDate：2003-01-09
 */
package com.sinosoft.lis.agentquery;

import com.sinosoft.lis.db.LACommisionDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LACommisionBSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

public class BankLAComBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LACommisionSchema mLACommisionSchema = new LACommisionSchema();
    private LACommisionBSchema mLACommisionBSchema = new LACommisionBSchema();
    public BankLAComBL()
    {}

    public static void main(String[] args)
    {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankLAComBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败BankLAComBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("Start BankLAComBL Submit...");
        BankLAComBLS tBankLAComBLS = new BankLAComBLS();
        tBankLAComBLS.submitData(mInputData, cOperate);
        System.out.println("End BankLAComBL Submit...");
        //如果有需要处理的错误，则返回
        if (tBankLAComBLS.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tBankLAComBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "BankLAComBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        LACommisionDB tLACommisionDB = new LACommisionDB();
        tLACommisionDB.setCommisionSN(this.mLACommisionSchema.getCommisionSN());
        tLACommisionDB.getInfo();
        LACommisionSchema tLACommisionSchema = new LACommisionSchema();
        tLACommisionSchema = tLACommisionDB.getSchema();

        Reflections tReflections = new Reflections();
        tReflections.transFields(mLACommisionBSchema, tLACommisionSchema);
        mLACommisionBSchema.setEdorType("11");
        mLACommisionBSchema.setMakeDate(currentDate);
        mLACommisionBSchema.setMakeTime(currentTime);
        String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
        mLACommisionBSchema.setEdorNo(tEdorNo);
        mLACommisionBSchema.setModifyDate(currentDate);
        mLACommisionBSchema.setModifyTime(currentTime);

        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLACommisionSchema.setSchema((LACommisionSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LACommisionSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLACommisionSchema);
            this.mInputData.add(this.mLACommisionBSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankLAComBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}