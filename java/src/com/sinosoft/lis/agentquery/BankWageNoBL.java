package com.sinosoft.lis.agentquery;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.FileWriter;
import java.io.*;

public class BankWageNoBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    //private String mtype = null;

    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    public BankWageNoBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        SSRS tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "AgentWageXLSBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 2][30];
        mToExcel[0][0] = "银保提奖查询明细表";
        mToExcel[0][10] = "银保提奖查询明细表";
        mToExcel[1][0] = "薪资年月";
        mToExcel[1][1] = "管理机构";
        mToExcel[1][2] = "所属团队名称";
        mToExcel[1][3] = "业务员代码";
        mToExcel[1][4] = "业务员姓名";
        mToExcel[1][5] = "员工类型";
        mToExcel[1][6] = "银代机构编码";
        mToExcel[1][7] = "银行机构名称";
        mToExcel[1][8] = "险种编码";
        mToExcel[1][9] = "险种名称";
        mToExcel[1][10] = "保单号";
        mToExcel[1][11] = "投保人姓名";
        mToExcel[1][12] = "被保人姓名";
        mToExcel[1][13] = "保费";
        mToExcel[1][14] = "提奖比例";
        mToExcel[1][15] = "提奖金额";
        mToExcel[1][16] = "主管间接提奖比例";
        mToExcel[1][17] = "主管提奖金额";
        mToExcel[1][18] = "投保日期";
        mToExcel[1][19] = "签单日期";
        mToExcel[1][20] = "回执回销日期";
        mToExcel[1][21] = "保费类型";
        mToExcel[1][22] = "保单年度";
        mToExcel[1][23] = "犹豫期撤保日期";
        mToExcel[1][24] = "是否银代直销";
        mToExcel[1][25] = "缴费年限";
        mToExcel[1][26] = "保险期间";
        mToExcel[1][27] = "保险期间标记";
        mToExcel[1][28] = "回访结果";
        mToExcel[1][29] = "回访成功时间";



        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row+1][col - 1] = tSSRS.GetText(row, col);
            }
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "BankWageNoBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
       //mtype = (String) tf.getValueByName("Type");
        if(mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "BankWageNoBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }


    public static void main(String[] args)
    {
        BankWageNoBL BankWageNoBL = new
            BankWageNoBL();
    }
}
