package com.sinosoft.lis.agentquery;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class BankWageNoUI
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    public BankWageNoUI()
    {
        System.out.println("BankWageNoUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
       BankWageNoBL bl = new BankWageNoBL();
        if(!bl.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        BankWageNoUI tBankWageNoUI = new   BankWageNoUI();
    }
}
