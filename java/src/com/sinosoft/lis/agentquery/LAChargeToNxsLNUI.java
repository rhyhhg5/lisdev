package com.sinosoft.lis.agentquery;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.*;
import com.sinosoft.lis.reinsure.*;

/*
 * <p>ClassName: LAChargeToNxsLNUI </p>
 * <p>Description: LAChargeToNxsLNUI类文件 </p>
 * <p>Copyright: Copyright (c) 2010</p>
 * <p>Company: sinosoft </p>
 * @Database: 龙程彬
 * @CreateDate：2008-10-30
 */

public class LAChargeToNxsLNUI {
		    /** 错误处理类，每个需要错误处理的类中都放置该类 */
		    public CErrors mErrors = new CErrors();

		    private String mResult = new String();

		    public LAChargeToNxsLNUI()
		    {
		    }

		    public boolean submitData(VData cInputData, String cOperate)
		    {
		        System.out.println("Begin LAChargeToNxsLNUI........");
		        LAChargeToNxsLNBL tLAChargeToNxsLNBL = new LAChargeToNxsLNBL();
		        if (!tLAChargeToNxsLNBL.submitData(cInputData, cOperate))
		        {
		            if (tLAChargeToNxsLNBL.mErrors.needDealError())
		            {
		                this.mErrors.copyAllErrors(tLAChargeToNxsLNBL.mErrors);
		                CError tError = new CError();
		                tError.moduleName = "tLAChargeToNxsLNBL";
		                tError.functionName = "submitData";
		                tError.errorMessage = tLAChargeToNxsLNBL.mErrors.getFirstError();
		                this.mErrors.addOneError(tError);
		                return false;
		            }
		            return false;
		        }
		         mResult = tLAChargeToNxsLNBL.getResult();
		         System.out.println("返回文字mResult:"+mResult);
		        return true;
		    }

		    public static void main(String[] args)
		    {
		        GlobalInput globalInput = new GlobalInput();
		        globalInput.ComCode = "8611";
		        globalInput.Operator = "001";

		        // 准备传输数据 VData
		        VData vData = new VData();
		    }

		    /**
		     * 准备往后层输出所需要的数据
		     * 输出：如果准备数据时发生错误则返回false,否则返回true
		     */
		    private boolean prepareOutputData()
		    {
		        return true;
		    }

		    /**
		     * 根据前面的输入数据，进行UI逻辑处理
		     * 如果在处理过程中出错，则返回false,否则返回true
		     */
		    private boolean dealData()
		    {
		        return true;
		    }

		    /**
		     * 从输入数据中得到所有对象
		     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
		     */
		    private boolean getInputData(VData cInputData)
		    {
		        return true;
		    }

		    public String getResult()
		    {
		        return this.mResult;
		    }

		    /*
		     * add by kevin
		     */
		    private void buildError(String szFunc, String szErrMsg)
		    {
		        CError cError = new CError();

		        cError.moduleName = "LAChargeToNxsLNUI";
		        cError.functionName = szFunc;
		        cError.errorMessage = szErrMsg;
		        this.mErrors.addOneError(cError);
		    }
}
