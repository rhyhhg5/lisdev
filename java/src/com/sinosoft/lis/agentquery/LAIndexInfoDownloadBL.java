package com.sinosoft.lis.agentquery;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.FileWriter;
import java.io.*;

public class LAIndexInfoDownloadBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    private String mAgentSeries=null;
    //private String mtype = null;

    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    public LAIndexInfoDownloadBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        SSRS tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LAIndexInfoDownloadBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        String[][] mToExcel=null;
        if(this.mAgentSeries.equals("0")){
        	mToExcel = new String[tSSRS.getMaxRow() + 1][14];

            mToExcel[0][0] = "代理人编码";
            mToExcel[0][1] = "代理人姓名";
            mToExcel[0][2] = "代理人职级";
            mToExcel[0][3] = "管理机构";
            mToExcel[0][4] = "销售机构代码";
            mToExcel[0][5] = "销售机构名称";
            mToExcel[0][6] = "指标类型";
            mToExcel[0][7] = "个人月均FYC";
            mToExcel[0][8] = "个人月均新标准客户";
            mToExcel[0][9] = "个人季继续率";
            mToExcel[0][10] = "FYC挂零月数";
            mToExcel[0][11] = "直接推荐人数";
            mToExcel[0][12] = "累计推荐人数";
            mToExcel[0][13] = "推荐客户顾问数";
        }else if(this.mAgentSeries.equals("1")){
        	mToExcel = new String[tSSRS.getMaxRow() + 1][11];

            mToExcel[0][0] = "代理人编码";
            mToExcel[0][1] = "代理人姓名";
            mToExcel[0][2] = "代理人职级";
            mToExcel[0][3] = "管理机构";
            mToExcel[0][4] = "销售机构代码";
            mToExcel[0][5] = "销售机构名称";
            mToExcel[0][6] = "指标类型";
            mToExcel[0][7] = "营业处累计FYC";
            mToExcel[0][8] = "营业处管客户顾问数";
            mToExcel[0][9] = "营业处季平均继续率";
            mToExcel[0][10] = "直接培养营业处个数";
        }else if(this.mAgentSeries.equals("2")){
        	mToExcel = new String[tSSRS.getMaxRow() + 1][12];

            mToExcel[0][0] = "代理人编码";
            mToExcel[0][1] = "代理人姓名";
            mToExcel[0][2] = "代理人职级";
            mToExcel[0][3] = "管理机构";
            mToExcel[0][4] = "销售机构代码";
            mToExcel[0][5] = "销售机构名称";
            mToExcel[0][6] = "指标类型";
            mToExcel[0][7] = "营业区累计FYC";
            mToExcel[0][8] = "营业区管客户顾问数";
            mToExcel[0][9] = "营业区季平均继续率";
            mToExcel[0][10] = "营业区管营业处个数";
            mToExcel[0][11] = "直接培养营业区个数";
        }
       
 

        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row][col - 1] = tSSRS.GetText(row, col);
            }
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAStandardRCDownloadBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        this.mAgentSeries=(String) tf.getValueByName("AgentSeries");
       //mtype = (String) tf.getValueByName("Type");
        if(mSql == null || mOutXmlPath == null || mAgentSeries==null)
        {
            CError tError = new CError();
            tError.moduleName = "LAStandardRCDownloadBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }


    public static void main(String[] args)
    {
//        RateCommisionBL RateCommisionBL = new
//            RateCommisionBL();
    }
}
