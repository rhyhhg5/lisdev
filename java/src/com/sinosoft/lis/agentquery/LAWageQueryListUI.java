package com.sinosoft.lis.agentquery;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAWageQueryListUI
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    public LAWageQueryListUI()
    {
        System.out.println("LAStandardRCDownloadUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
    	LAWageQueryListBL tLAWageQueryListBL = new LAWageQueryListBL();
        if(!tLAWageQueryListBL.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(tLAWageQueryListBL.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
    }
}
