package com.sinosoft.lis.agentquery;

import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:代理人序列树
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class LAAgentTreeUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    public LAAgentTreeUI()
    {}

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        LAAgentTreeBL tLAAgentTreeBL = new LAAgentTreeBL();

        System.out.println("---UI BEGIN---" + cOperate);
        if (tLAAgentTreeBL.submitData(cInputData, mOperate) == false)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAAgentTreeBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentTreeUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据查询失败!";
            this.mErrors.addOneError(tError);
            mResult.clear();
            return false;
        }
        else
        {
            mResult = tLAAgentTreeBL.getResult();
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        LAAgentTreeUI tLAAgentTreeUI = new LAAgentTreeUI();
        VData tVData = new VData();
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        tVData.addElement(tLAAgentSchema);
        tLAAgentTreeUI.submitData(tVData, "query");
    }
}