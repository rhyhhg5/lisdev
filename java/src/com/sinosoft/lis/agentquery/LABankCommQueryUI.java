package com.sinosoft.lis.agentquery;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author miaoxz
 * @version 1.0
 */

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LABankCommQueryUI {
	/** 错误信息容器 */
	public CErrors mErrors = new CErrors();

	public LABankCommQueryUI() {
		System.out.println("LABankCommQueryUI");
	}

	public boolean submitData(VData cInputData, String cOperator) {
		LABankCommQueryBL bl = new LABankCommQueryBL();
		if (!bl.submitData(cInputData, cOperator)) {
			mErrors.copyAllErrors(bl.mErrors);
			return false;
		}

		return true;
	}

	public static void main(String[] args) {
		LABankCommQueryUI tLABankCommQueryUI = new LABankCommQueryUI();
	}
}
