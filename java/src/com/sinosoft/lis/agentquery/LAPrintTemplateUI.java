package com.sinosoft.lis.agentquery;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAPrintTemplateUI
{
    /**������Ϣ����*/
    public CErrors mErrors = new CErrors();

    public LAPrintTemplateUI()
    {
        System.out.println("LAPrintTemplateUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
       LAPrintTemplateBL bl = new LAPrintTemplateBL();
        if(!bl.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        LAPrintTemplateUI tLAPrintTemplateUI = new   LAPrintTemplateUI();
         System.out.println("LAPrintTemplateUI");
    }
}
