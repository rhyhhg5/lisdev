package com.sinosoft.lis.agentquery;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.*;
import java.text.DecimalFormat;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class LATaxReportBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    //private String mtype = null;
    SSRS tSSRS=new SSRS();
    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    public LATaxReportBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LATaxReportBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 4][30];
        mToExcel[0][0] = "序号";
        mToExcel[0][1] = "保险公司名称";
        mToExcel[0][2] = "本期保险营销员税前收入总额(万元)"; 
        mToExcel[0][3] = "本期代扣代缴项目";
        mToExcel[0][7] =  "本期保险营销员可支配收入总额(万元)";
        
        mToExcel[1][3] ="本期代扣代缴保险营销员营业税及附加总额(万元)";  
        mToExcel[1][4] ="本期代扣代缴保险营销员个人所得税总额(万元)";    
        mToExcel[1][5] ="本期代扣代缴的其他项目总额(万元)";              
        mToExcel[1][6] ="本期代扣代缴的项目总额(万元)";           

       

        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
            	mToExcel[row +1][0] = String.valueOf(row); 
            	if(col!=1) {
            		 mToExcel[row +1][col] = getAct(tSSRS.GetText(row, col),"10000");  	
            	}
            	else {
            		 mToExcel[row +1][col] = tSSRS.GetText(row, col);
                }            
            }
        }
        mToExcel[tSSRS.getMaxRow()+2][1]="汇总";
        mToExcel[tSSRS.getMaxRow()+2][2]=getAct(dealsum(tSSRS.getMaxRow(),1),"10000");  	
        mToExcel[tSSRS.getMaxRow()+2][3]=getAct(dealsum(tSSRS.getMaxRow(),2),"10000");  	
        mToExcel[tSSRS.getMaxRow()+2][4]=getAct(dealsum(tSSRS.getMaxRow(),3),"10000");  	
        mToExcel[tSSRS.getMaxRow()+2][5]=getAct(dealsum(tSSRS.getMaxRow(),4),"10000");  	
        mToExcel[tSSRS.getMaxRow()+2][6]=getAct(dealsum(tSSRS.getMaxRow(),5),"10000");  	
        mToExcel[tSSRS.getMaxRow()+2][7]=getAct(dealsum(tSSRS.getMaxRow(),6),"10000");  	

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LATaxReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        if(mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "LATaxReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }
    private String dealsum(int pmLength,int pmArrNum)
           {
             String tReturnValue = "";
             DecimalFormat tDF = new DecimalFormat("0.######");
             String tSQL = "select 0";

             for(int i=1;i<pmLength+1;i++)
             {
                 tSQL += " + " + tSSRS.GetText(i, pmArrNum+1);
             }

             tSQL += " + 0 from dual";

             tReturnValue = "" + tDF.format(execQuery(tSQL));

             return tReturnValue;
        }
   
    
    private String getAct(String pmValue1,String pmValue2)
    {
        String tSQL = "";
        String tRtValue = "";
        tSQL = "select DECIMAL(DECIMAL(" + pmValue1 + ",12,2) / DECIMAL(" +pmValue2 + ",12,2),12,6) from dual";
        try{
            tRtValue = String.valueOf(execQuery(tSQL));
        }catch(Exception ex)
        {
            System.out.println("getAct 出错！");
        }

        return tRtValue;
    }
        /**
         * 执行SQL文查询结果
         * @param sql String
         * @return double
         */
        private double execQuery(String sql)

        {
           Connection conn;
           conn = null;
           conn = DBConnPool.getConnection();

           System.out.println(sql);

           PreparedStatement st = null;
           ResultSet rs = null;
           try {
               if (conn == null)return 0.00;
               st = conn.prepareStatement(sql);
               if (st == null)return 0.00;
               rs = st.executeQuery();
               if (rs.next()) {
                   return rs.getDouble(1);
               }
               return 0.00;
           } catch (Exception ex) {
               ex.printStackTrace();
               return -1;
           } finally {
               try {
                  if (!conn.isClosed()) {
                      conn.close();
                  }
                  try {
                      st.close();
                      rs.close();
                  } catch (Exception ex2) {
                      ex2.printStackTrace();
                  }
                  st = null;
                  rs = null;
                  conn = null;
                } catch (Exception e) {}

           }
     }

    public static void main(String[] args)
    {
        LATaxReportBL LATaxReportBL = new            LATaxReportBL();
    System.out.println("11111111");
    }
}
