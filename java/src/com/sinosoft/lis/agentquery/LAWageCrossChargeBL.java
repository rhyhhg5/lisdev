package com.sinosoft.lis.agentquery;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2016</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */


import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LAWageCrossChargeBL
{ /**错误信息容器*/
	public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    //private String mtype = null;

    SSRS tSSRS=new SSRS();
    public LAWageCrossChargeBL()
    {
      }
   
	public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }
        
        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
    	ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        tSSRS = tExeSQL.execSQL(mSql);
        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());
            
            CError tError = new CError();
            tError.moduleName = "LAWageCrossChargeBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        System.out.println("========================================"+tSSRS.getMaxRow());
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 4][30];
        mToExcel[0][0] = "交叉销售手续费查询报表";
        mToExcel[1][0] = "保单号";
        mToExcel[1][1] = "子公司代码";
        mToExcel[1][2] = "子公司名称";
        mToExcel[1][3] = "业务活动日期";
        mToExcel[1][4] = "签单日期";        
        mToExcel[1][5] = "险种ID";
        mToExcel[1][6] = "险种名称";
        mToExcel[1][7] = "业务单证ID";
        mToExcel[1][8] = "佣金生成日期";
        mToExcel[1][9] = "佣金类型代码";
        mToExcel[1][10] = "佣金类型名称";
        mToExcel[1][11] = "应付业务佣金";
        mToExcel[1][12] = "应付业务佣金比例";
        mToExcel[1][13] = "业务活动代码";
        mToExcel[1][14] = "业务活动名称";
        mToExcel[1][15] = "委托方机构代码";
        mToExcel[1][16] = "委托方机构名称";
        mToExcel[1][17] = "代理方机构代码";
        mToExcel[1][18] = "代理方机构名称";
        mToExcel[1][19] = "代理方子公司代码";
        mToExcel[1][20] = "委托方人员代码";
        mToExcel[1][21] = "委托方人员姓名";
        mToExcel[1][22] = "代理方人员代码";
        mToExcel[1][23] = "代理方人员姓名";
        mToExcel[1][24] = "客户名称";
        mToExcel[1][25] = "承保标的";
        mToExcel[1][26] = "保费收入";
        mToExcel[1][27] = "报送时间";
        
        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            { 
                mToExcel[row +1][col - 1] = tSSRS.GetText(row, col);
                //System.out.println(row +1);
                //System.out.println("---------"+mToExcel[row +1][col - 1]);
                //System.out.println( tSSRS.GetText(row, col));
            }
            //System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"+tSSRS.getMaxCol());
        }
        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

   
   
    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData  cInputData)
    {
    	mGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) cInputData
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAWageCrossChargeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
       //mtype = (String) tf.getValueByName("Type");
        if(mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAWageCrossChargeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }


}



