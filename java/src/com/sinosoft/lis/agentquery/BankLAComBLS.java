package com.sinosoft.lis.agentquery;

import java.sql.Connection;

import com.sinosoft.lis.db.LACommisionBDB;
import com.sinosoft.lis.schema.LACommisionBSchema;
import com.sinosoft.lis.schema.LACommisionSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */

public class BankLAComBLS
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    public BankLAComBLS()
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Start BankLAComBLS Submit...");
        tReturn = saveLAPresence(cInputData);
        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End BankLAComBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean saveLAPresence(VData mInputData)
    {
        //校验时间
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PoundCalBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LACommisionBDB tLACommisionBDB = new LACommisionBDB(conn);
            tLACommisionBDB.setSchema((LACommisionBSchema) mInputData.
                                      getObjectByObjectName(
                                              "LACommisionBSchema", 1));
            if (!tLACommisionBDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLACommisionBDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "PoundCalBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            tLACommisionSchema.setSchema((LACommisionSchema) mInputData.
                                         getObjectByObjectName(
                                                 "LACommisionSchema", 0));

            ExeSQL aExeSQL = new ExeSQL(conn);
            String SQL = "update lacommision set agentcode='" +
                         tLACommisionSchema.getAgentCode() + "',FYC="
                         + tLACommisionSchema.getFYC() + ",GrpFYC=" +
                         tLACommisionSchema.getGrpFYC() + ",Depfyc="
                         + tLACommisionSchema.getDepFYC() +
                         " where commisionsn='" +
                         tLACommisionSchema.getCommisionSN() + "' ";
            tReturn = aExeSQL.execUpdateSQL(SQL);
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PoundCalBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    public static void main(String[] args)
    {
    }
}