package com.sinosoft.lis.agentquery;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAHealthPrintTemplateUI
{
    /**������Ϣ����*/
    public CErrors mErrors = new CErrors();

    public LAHealthPrintTemplateUI()
    {
        System.out.println("LAHealthPrintTemplateUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
       LAHealthPrintTemplateBL bl = new LAHealthPrintTemplateBL();
        if(!bl.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        LAHealthPrintTemplateUI tLAHealthPrintTemplateUI = new   LAHealthPrintTemplateUI();
         System.out.println("LAHealthPrintTemplateUI");
    }
}
