package com.sinosoft.lis.agentquery;

import com.sinosoft.lis.schema.LAWageSchema;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class WageQueryUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String mMonth[] = null;
    private String ManageCom = null;
    //业务处理相关变量
    /** 全局数据 */
    private LAWageSchema mLAWageSchema = new LAWageSchema();
    public WageQueryUI()
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        // 进行业务处理
        if (!dealData())
        {
            return false;
        }
        // 准备传往后台的数据
        VData vData = new VData();

        if (!prepareOutputData(vData))
        {
            return false;
        }
        WageQueryBL tWageQueryBL = new WageQueryBL();
        System.out.println("Start WageQuery UI Submit ...");
        if (!tWageQueryBL.submitData(vData, cOperate))
        {
            if (tWageQueryBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tWageQueryBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "WageQueryBL发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            mResult = tWageQueryBL.getResult();
            return true;
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.addElement(mMonth);
            vData.addElement(mLAWageSchema);
            vData.add(ManageCom);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mMonth = (String[]) cInputData.get(0);
        ManageCom = (String) cInputData.get(2);
        mLAWageSchema.setSchema((LAWageSchema) cInputData.getObjectByObjectName(
                "LAWageSchema", 0));
        if (mLAWageSchema.getAgentCode() == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "MeetF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    public static void main(String[] args)
    {
        LAWageSchema mLAWageSchema = new LAWageSchema();
        mLAWageSchema.setAgentCode("8611000001");
        String mMonth[] = new String[2];
        mMonth[0] = "200308";
        mMonth[1] = "200308";
        VData tVData = new VData();
        tVData.addElement(mMonth);
        tVData.addElement(mLAWageSchema);
        tVData.addElement("86110000");
        WageQueryUI UI = new WageQueryUI();
        if (!UI.submitData(tVData, ""))
        {
            if (UI.mErrors.needDealError())
            {
                System.out.println(UI.mErrors.getFirstError());
            }
            else
            {
                System.out.println("UI发生错误，但是没有提供详细的出错信息");
            }
        }
        else
        {
            VData vData = UI.getResult();
            LAWageSet tLAWageSet = new LAWageSet();
            tLAWageSet = ((LAWageSet) vData.getObjectByObjectName("LAWageSet",
                    0));
            System.out.println("已经接收了数据!!!");
            int n = tLAWageSet.size();
            System.out.println("get Data " + n);
        }
    }
}
