package com.sinosoft.lis.agentquery; 

import java.text.DecimalFormat;

import org.apache.axis2.mex.MexException;

import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.lis.schema.LAhumandeveiSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;


public class LAMonthlyDownloadBL {
    public CErrors mErrors = new CErrors();

    /** 全局变量 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private String mManageCom = "";
    
    private VData mInputData = new VData();

    private ExeSQL tExeSQL = new ExeSQL();
    
    private GlobalInput mGI = null;
    
    private String mOutXmlPath = null;
    
    private String mType = null;
    
    private String mSubLength = null;
    
    private String mManageComHierarchy = null;
    
    public static void main(String[] args) {
    	LAMonthlyDownloadBL tLAMonthlyDownloadB = new LAMonthlyDownloadBL();
    	TransferData tTransferData= new TransferData();
//    	传参
    	tTransferData.setNameAndValue("tManageCom","86110000");
    	tTransferData.setNameAndValue("tCaseNo","C1100141020000011");
    	tTransferData.setNameAndValue("tOperator","cm001");
    	tTransferData.setNameAndValue("tFileNameB","Test.cvs");
    	tTransferData.setNameAndValue("OutXmlPath","000");
    	tTransferData.setNameAndValue("ManageCom","86");
    	tTransferData.setNameAndValue("type","2");
    	tTransferData.setNameAndValue("subLength","2");
    	GlobalInput tG = new GlobalInput();
    	VData tVData = new VData();
    	tVData.addElement(tG);
    	tVData.addElement(tTransferData);
    	
    	tLAMonthlyDownloadB.submitData(tVData, "PRINT");
	}
    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
    	System.out.println("<-Go Into LAMonthlyDownloadBL->");
        mInputData = (VData) cInputData;

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(mInputData)) {
            return false;
        }
        if(!dealData()){
        	return false;
        }           
        // 进行数据查询
        return true;
    }


    /**
     * 获取前台传入的数据
     * @return boolean
     */
    private boolean getInputData(VData data) {

    	System.out.println("<-Go Into getInputData()->");

        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAMonthlyDownloadBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        mManageCom = (String) tf.getValueByName("ManageCom");
        mType = (String) tf.getValueByName("type");
        mSubLength = (String) tf.getValueByName("subLength");
        if("2".equals(mType))
        {
        	mManageComHierarchy ="总公司";
        }
        else  if("4".equals(mType))
        {
        	mManageComHierarchy ="省公司";
        }
        else  if("8".equals(mType))
        {
        	mManageComHierarchy ="三级机构";
        }
        if(mOutXmlPath == null||mManageCom ==null)
        {
            CError tError = new CError();
            tError.moduleName = "AgentActiveWageXLSBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    
    }

    private boolean dealData() {
    	
    	System.out.println(mManageCom);
    	ExeSQL tExeSQL = new ExeSQL();
//    	String tManageComSQL ="select name from ldcom where comcode = "+mManageCom+"' with ur ";
//    	SSRS tManageComSSRS = new SSRS();
//    	tManageComSSRS = tExeSQL.execSQL(tManageComSQL);
//    	String tManageComName = tManageComSSRS.GetText(1, 1);
    	
	    String csql1 = "select distinct substr(code,1,"+mSubLength+") from ldcode a where codetype='2016humandeveidesc' and substr(code,1,"+mType+")= '"+mManageCom+"' ";	
		SSRS tCaseTraceSSRS = new SSRS();
		tCaseTraceSSRS = tExeSQL.execSQL(csql1);
		 
		 try {
			
//             String tContent[][] = new String[tCaseTraceSSRS.getMaxRow()][];
    		
	         if (tCaseTraceSSRS != null && tCaseTraceSSRS.getMaxRow() > 0) {
	        	 WriteToExcel t = new WriteToExcel("");
	             t.createExcelFile();
	        	 String[][] mToExcel = new String[tCaseTraceSSRS.getMaxRow()+3][16];
	     		mToExcel[0][0] = mManageComHierarchy+"预提汇总报表 ";
	     		mToExcel[1][0] = "序号";
	     		mToExcel[1][1] =  "分公司代码";
	     		mToExcel[1][2] =  "分公司名称";
	     		mToExcel[1][3] =  "管理机构代码";
	     		mToExcel[1][4] =  "管理机构名称";
	     		mToExcel[1][5] =  "基础人力";
	     		mToExcel[1][6] =  "期初在职人力";
	     		mToExcel[1][7] =  "期末在职人力";
	     		mToExcel[1][8] =  "当月平均人力";
	     		mToExcel[1][9] =  "月合格人力";
	     		mToExcel[1][10] = "2015-12-16（含）之后截至本月新增有效累计人力";
	     		mToExcel[1][11] = "基础人力中的离职人力";
	     		String tCurrntDate = PubFun.getCurrentDate2();
 				String mAssessYearMonth = tCurrntDate.substring(0, 6);
	     		if(mAssessYearMonth.compareTo("201607")<0){
	     			mToExcel[1][12] = "截至本月净增有效人力";
		     		mToExcel[1][13] = "净增有效人力标准";
		     		mToExcel[1][14] = "截至本月累计有效人力达成率";
		     		mToExcel[1][15] = "当月销售人力合格率";
	     		}else{
		     		mToExcel[1][12] = "截至本月年度净增有效人力";
		     		mToExcel[1][13] = "年度净增有效人力标准";
		     		mToExcel[1][14] = "截至本月年度累计有效人力达成率";
		     		mToExcel[1][15] = "截至当月年度销售人力合格率";
	     		}
	     		int tSumAllBaseHuman = 0;
     			int tSumAllTMonthHuW = 0;
     			int tSumAllLMonthHuW = 0;            
     			int tSumAllAveHu = 0;
     			int tSumAllQCHu = 0;
     			int tSumAllTMonthHuAdd =0;
     			int tSumAllTMonthHuDL =0;
     			int tSumAllTMonthHu = 0;
     			int tSumAllPlanHu = 0;
     			int tSumAllLastQChu=0;
     			int tSumAllLastAveHu=0;
     			DecimalFormat df = new DecimalFormat("#.00"); 
	     		for(int index=1;index<=tCaseTraceSSRS.getMaxRow();index++)
	     		{
	     			//查询本次汇总中涉及到的参加三级人力的8位机构
	     			String csql2 = "select  code from ldcode a where codetype='2016humandeveidesc' and code like  '"+tCaseTraceSSRS.GetText(index, 1)+"%' ";	
	     			SSRS tManageComs = new SSRS();
	     			tManageComs = tExeSQL.execSQL(csql2);
	     			int tSumBaseHuman = 0;
	     			int tSumTMonthHuW = 0;
	     			int tSumLMonthHuW = 0;            
	     			int tSumAveHu = 0;
	     			int tSumQCHu = 0;
	     			int tSumTMonthHuAdd =0;
	     			int tSumTMonthHuDL =0;
	     			int tSumTMonthHu = 0;
	     			int tSumPlanHu = 0;
	     			int tSumLastQChu=0;
	     			int tSumLastAveHu=0;
	     			String tDate2 = "2015-12-15";
	     			String tWageno = "201512";
	     			String tDate3 = "2017-1-1";
	     			for (int i = 1; i <= tManageComs.getMaxRow(); i++) {
	     				String tManagecom =tManageComs.GetText(i, 1);
	     				
	     				String tIncludeManagecom ="";
	     				
	     				String mSQL = "select code1 from ldcode1 where codetype = '2016includemanagecom' and code  =  '"+tManagecom+"'";
	     				SSRS mSSRS = tExeSQL.execSQL(mSQL);
	     				if(mSSRS!=null && mSSRS.MaxRow>1){
	     					for(int o = 1;o<=mSSRS.MaxRow;o++){
	     						//如果为合并机构将机构代码拼起来
	     						tIncludeManagecom += "'"+mSSRS.GetText(o,1 )+"',"; 
	     					}         	
	     					tIncludeManagecom = tIncludeManagecom.substring(0, tIncludeManagecom.length()-1);
	     					
	     				}else {
	     					tIncludeManagecom = "'"+tManagecom+"'";
	     				}
	     				String tWagenoH = "";
	     				String tYear1 = "";
	     				String tMonth1 = "";
	     				String tYear = mAssessYearMonth.substring(0,4);
	     				String tMonth = mAssessYearMonth.substring(4, 6);
	     				
	     				//如果大于等于201607,则需要统计本月之前月份的合格人力之和，平均人力之和，累计fyc
	     				//本月之前合格人力之和
	     				String  sumQChu ="0";
	     				//本月之前平均人力之和
	     				String  sumAveHu="0";
	     				//本月之前fyc之和
	     				String sumFYC ="0";
	     				//本月之前实发金额之和
	     				String sumRealPay ="0";
	     				
	     				//已经计算过三级人力的最大月份
	     				String maxMonth = tYear+"00";
	     				if(mAssessYearMonth.compareTo("201607")>=0)
	     				{
	     					String sumSQL="select sum(QCHu),sum(AveHu),sum(mfyc),sum(RealPay),max(remark1) from LAhumandevei where year ='"+tYear+"' and month<'"+tMonth+"' and mngcom ='"+tManagecom+"' with ur";
	     					SSRS tSSRS = new SSRS();
	     					tSSRS =  tExeSQL.execSQL(sumSQL);
	     					if(tSSRS!=null&&tSSRS.getMaxRow()>0)
	     					{
	     						sumQChu = tSSRS.GetText(1, 1);
	     						sumAveHu = tSSRS.GetText(1, 2);
	     						sumFYC = tSSRS.GetText(1, 3);
	     						sumRealPay = tSSRS.GetText(1, 4);
	     						maxMonth = tSSRS.GetText(1, 5);
	     						if(null==sumQChu||"".equals(sumQChu))
	     						{
	     							sumQChu="0";
	     						}
	     						if(null==sumAveHu||"".equals(sumAveHu))
	     						{
	     							sumAveHu="0";
	     						}
	     						if(null==sumFYC||"".equals(sumFYC))
	     						{
	     							sumFYC="0";
	     						}
	     						if(null==sumRealPay||"".equals(sumRealPay))
	     						{
	     							sumRealPay="0";
	     						}
	     						if(null==maxMonth||"".equals(maxMonth))
	     						{
	     							maxMonth="201600";
	     						}
	     					}
	     					tSumLastQChu += Integer.parseInt(sumQChu);
	     					tSumLastAveHu += Integer.parseInt(sumAveHu);
	     					int countMonth  = Integer.parseInt(mAssessYearMonth)-Integer.parseInt(maxMonth);
	     					for(int k=1;k<countMonth ;k++)
	     					{
	     						String lastMonth = String.valueOf(Integer.parseInt(maxMonth)+k);
	     						LAhumandeveiSchema tLAhumandeveiSchema = new LAhumandeveiSchema();
	     						tLAhumandeveiSchema = getLastMontData(tIncludeManagecom,tManagecom, lastMonth);
	     						tSumLastQChu += tLAhumandeveiSchema.getQCHu() ;
	     						tSumLastAveHu += tLAhumandeveiSchema.getAveHu() ;
	     						sumFYC=String.valueOf( Double.parseDouble(sumFYC)+tLAhumandeveiSchema.getMFYC() );
	     						sumRealPay=String.valueOf( Double.parseDouble(sumRealPay)+tLAhumandeveiSchema.getRealPay() );
	     					}
	     				}
	     				String  tWorkSQL ="select  nvl(count,0)  from lahumandesc where year ='"+tYear+"' and month ='"+tMonth+"' and mngcom in ("+tIncludeManagecom+") with ur";
	     				String  tCheckFlag = tExeSQL.getOneValue(tWorkSQL);
	     				if("0".equals(tCheckFlag))
	     				{
	     					tWorkSQL ="(select e.Agentcode from laagent e,LADimission f "
	     						+ "where e.agentcode=f.agentcode and e.BranchType='1' and e.branchtype2='01'  " 
	     						+" and e.managecom in ("+tIncludeManagecom+") and e.AgentState='02' and e.employdate >='2015-12-16' "
	     						+ "group by e.agentcode "
	     						+ "having  min(days(e.employdate)-days(f.DepartDate)) >= '181' "
	     						+ "union "
	     						+"select e.Agentcode from laagent e "
	     						+ "where  e.BranchType='1' and e.branchtype2='01' and e.managecom in ("+tIncludeManagecom+") and  e.AgentState='02' and e.employdate <'2015-12-16' "
	     						+ "union "
	     						+ "select e.Agentcode from laagent e "
	     						+ "where e.BranchType='1' and e.managecom in ("+tIncludeManagecom+") and  e.AgentState='01' and e.branchtype2='01') ";
	     				}
	     				else
	     				{
	     					tWorkSQL ="(select  agentcode  from lahumandesc where year ='"+tYear+"' and month ='"+tMonth+"' and mngcom in ("+tIncludeManagecom+") )";
	     				}
//	        	    	考核年，考核月，考核月区间
	     				String tSQL = "select codealias,comcode,code,code1,riskwrapplanname,othersign " +
	     				"from ldcode1  " +
	     				"where 1=1 and codetype='humandeveimonth' and codename = '"+mAssessYearMonth+"' ";
	     				System.out.println("tSQL:"+tSQL);
	     				
	     				
	     				//求上个月的 wageno 一些类型转换
	     				String rDate  = tYear+"-"+tMonth+"-"+"01";
	     				String rDate1 = PubFun.calDate(rDate, -1, "D", "2010-12-31");
	     				String rDate3 = AgentPubFun.formatDate(rDate1, "yyyyMMdd");
	     				
	     				tYear1 = rDate3.substring(0,4);
	     				tMonth1 = rDate3.substring(4, 6);
	     				tWagenoH = rDate3.substring(0, 6);
	     				
	     				// 机构 ，方案编码
	     				String tSQL1 = " select code,codename from ldcode where codetype='2016humandeveidesc' " +
	     				" and code = '"+tManagecom+"' with ur ";
	     				System.out.println("tSQL1:"+tSQL1);
	     				
	     				SSRS tSSRS1 = tExeSQL.execSQL(tSQL1);
	     				
	     				String tMngcom = tSSRS1.GetText(1, 1);//考核机构
	     				String tPlanCode = tSSRS1.GetText(1, 2);//考核机构方案编码
	     				
	     				// 方案，档次
	     				String tSQL2 = " select codealias,comcode,riskwrapplanname,othersign from ldcode1 where codetype='2016humandeveiplan' and code='"+tPlanCode+"' ";
	     				System.out.println("tSQL2:"+tSQL2);
	     				
	     				SSRS tSSRS2 = tExeSQL.execSQL(tSQL2);
	     				
	     				String tPlan = tSSRS2.GetText(1, 1);//方案
	     				String tLevel = tSSRS2.GetText(1, 2);//档次
	     				String tSumFYCStand = tSSRS2.GetText(1, 3);//合格人力标准
	     				String tWin = tSSRS2.GetText(1, 4);//合格比例
	     				
	     				
	     				// 计划净增人力
	     				String tSQL3 = " select code1 from ldcode1 where codetype='2016humandeveistand' " +
	     				"and  codealias='"+tPlan+"' and comcode='"+tLevel+"' and othersign='"+tMonth+"' ";
	     				System.out.println("tSQL3:"+tSQL3);
	     				
	     				SSRS tSSRS3 = tExeSQL.execSQL(tSQL3);
	     				
	     				String tPlanHu = tSSRS3.GetText(1, 1);//计划净增人力
	     				
	     				
	     				//累计本月新增有效人力
	     				String tSQL4 = "";
	     				
	     				
	     				tSQL4 = "select count(1) from " +
	     				" (select  a.agentcode,sum(a.fyc) " +
	     				" from lacommision a,laagent b	" +
	     				" where 1=1 and a.agentcode=b.agentcode and a.BranchType='1' and a.branchtype2='01' and Payyear = 0 and a.renewcount ='0' and " +
//	        			   " exists(select 1 from LAhumanDesc c where c.mngcom in ("+tIncludeManagecom+") and c.year='"+tYear+"' and c.month='"+tMonth+"' and c.agentcode=b.agentcode) " +
	     				" b.agentcode in "+tWorkSQL+
	     				" and b.EmployDate>'"+ tDate2 +"' and a.managecom in ("+tIncludeManagecom+") " +
	     				" and not exists(select 1 from lacommision  d where d.TRANSTYPE='WT'  and d.polno=a.polno and d.wageno>'201511' and d.managecom in("+tIncludeManagecom+")) " +
	     				" and Tmakedate >'"+ tDate2+"'"+
	     				" and (wageno between'"+ tWageno +"' and '"+ mAssessYearMonth +"')" +
	     				" and Tmakedate <'"+tDate3+"'"+
	     				" group by a.agentcode " +
	     				" having sum(a.fyc)>0)";
	     				
	     				SSRS tSSRS4 = tExeSQL.execSQL(tSQL4);
	     				String tTMonthHuAdd = "";//累计有效人力
	     				tTMonthHuAdd = tSSRS4.GetText(1, 1);
	     				
	     				//基础人力中离职的人数
	     				String tSQL41 ="";	        	            	
	     				tSQL41 = "select count(1) from  LAhumanDesc a where a.mngcom in ("+tIncludeManagecom+")  and a.year='2016' and a.month='00' and a.calflag <> 'Y'"  +
//	        	    			   " and not exists(select 1 from LAhumanDesc b where b.mngcom in ("+tIncludeManagecom+")  and b.year='"+tYear+"' and b.month='"+tMonth+"' and a.agentcode=b.agentcode  ) " ;
	     				" and agentcode not in "+tWorkSQL;
	     				
	     				SSRS tSSRS41 = tExeSQL.execSQL(tSQL41);
	     				String tTMonthHuDL = "";//基础人力中离职的人数
	     				tTMonthHuDL = tSSRS41.GetText(1, 1);
	     				
	     				//月度净增累计有效人力
	     				int tTMonthHu =0;
	     				tTMonthHu = (Integer.parseInt(tTMonthHuAdd)-Integer.parseInt(tTMonthHuDL));
	     				
//	        	        截至上月新增有效人力
	     				String tSQL5 = "select coalesce((select TMonthHu from lahumandevei where mngcom in ("+tIncludeManagecom+") and remark1='"+tWagenoH+"'),0) from dual where 1=1 ";
	     				
	     				SSRS tSSRS5 = tExeSQL.execSQL(tSQL5);
	     				
	     				String tLMonthHu = tSSRS5.GetText(1, 1);//截至上月新增有效人力
	     				
//	        	        当月净增有效人力
	     				String tSQL6 = "select "+tTMonthHu+"-("+tLMonthHu+") from dual where 1= 1 ";
	     				
	     				SSRS tSSRS6 = tExeSQL.execSQL(tSQL6);
	     				
	     				String RealHu = tSSRS6.GetText(1, 1);//当月净增有效人力
	     				
//	        	        当月合格人力
	     				String tSQL7 = "";
	     				tSQL7  = "select count(1) from " +
	     				" (select  a.agentcode,sum(a.fyc) " +
	     				" from lacommision a	" +
	     				" where 1=1 and a.BranchType='1' and a.branchtype2='01'  and Payyear = 0 and a.renewcount ='0' and " +
//	        			   " exists(select 1 from LAhumanDesc c where c.mngcom in("+tIncludeManagecom+") and c.year='"+tYear+"' and c.month='"+tMonth+"' and c.agentcode=a.agentcode) " +
	     				"  a.agentcode in "+tWorkSQL+
	     				" and a.managecom in ("+tIncludeManagecom+") " +
	     				" and not exists(select 1 from lacommision  d where d.TRANSTYPE='WT'  and d.polno=a.polno and d.wageno>'201511' and d.managecom in ("+tIncludeManagecom+")) " ;
	     				if(tMonth.equals("01")){
	     					tSQL7 +="and wageno between '"+tWagenoH+"' and '"+mAssessYearMonth+"' and Tmakedate > '"+tDate2+"'";
	     				}else if(tMonth.equals("12")){
	     					tSQL7 +="and wageno = '"+mAssessYearMonth+"' and Tmakedate < '"+tDate3+"'";
	     				}else{
	     					tSQL7 +="and wageno = '"+mAssessYearMonth+"'";
	     				}
	     				tSQL7 +=" group by a.agentcode  having sum(a.fyc)>='"+tSumFYCStand+"')";
	     				SSRS tSSRS7 = tExeSQL.execSQL(tSQL7);
	     				String QCHu = tSSRS7.GetText(1, 1);
	     				if(null==QCHu||"".equals(QCHu))
	     				{
	     					QCHu="0";
	     				}
//	        	        上月在职人数,期初人力
	     				String tSQL8 = "";
	     				if(tMonth.equals("01")){
	     		        	tSQL8 = "select count(1) from LAhumanDesc where mngcom in("+tIncludeManagecom+") and year='2016' and month='00' ";
	     		        }else{
	     		         tSQL8 = "select count(1) from LAhumanDesc where mngcom in("+tIncludeManagecom+") and year='"+tYear1+"' and month='"+tMonth1+"' ";
	     		        }
	     				SSRS tSSRS8 = tExeSQL.execSQL(tSQL8);
	     				String tTMonthHuW = tSSRS8.GetText(1, 1);
	     				if(null==tTMonthHuW||"".equals(tTMonthHuW))
	     				{
	     					tTMonthHuW="0";
	     				}
//	        	        当月在职人数，期末人力
	     				String tSQL9 = "";
	     				tSQL9 = "select count from "+tWorkSQL;
	     				SSRS tSSRS9 = tExeSQL.execSQL(tSQL9);
	     				String tLMonthHuW = tSSRS9.GetText(1, 1);
	     				if(null==tLMonthHuW||"".equals(tLMonthHuW))
	     				{
	     					tLMonthHuW="0";
	     				}
	     				
//	        	        月均人力
	     				String tSQL10 = "select round(cast("+tTMonthHuW+"+"+tLMonthHuW+" as decimal(12,1)) /2,0) from dual where 1 = 1 ";
	     				SSRS tSSRS10 = tExeSQL.execSQL(tSQL10);
	     				String tAveHu = tSSRS10.GetText(1, 1);
	     				double aveNum = Double.parseDouble(tAveHu);
	     				if(0==aveNum)
	     				{
	     					tAveHu = "0";
	     				}
	     				
//	        			获奖标准
	     				String tSQL11 = "select round(cast("+tTMonthHuW+"+"+tLMonthHuW+"+"+sumAveHu+"*2 as decimal(12,1)) /(2*"+tWin+"),0) from dual where 1 = 1 ";
	     				SSRS tSSRS11 = tExeSQL.execSQL(tSQL11);
	     				String tPreWin = tSSRS11.GetText(1, 1);
	     				double PreWinNum = Double.parseDouble(tPreWin);
	     				if(0==PreWinNum)
	     				{
	     					tPreWin = "0";
	     				}
//	        	        月度FYC
	     				String tSQL12 = "";
	     				tSQL12 = " select coalesce(sum(FYC),0) from lacommision where BranchType='1' and branchtype2='01' and managecom in("+tIncludeManagecom+") and wageno='"+mAssessYearMonth+"' " +
	     				" and ((Payyear = 0 and renewcount =0) or  renewcount >0) ";
	     				//       }
	     				SSRS tSSRS12 = tExeSQL.execSQL(tSQL12);
	     				String tMFYC = tSSRS12.GetText(1, 1);
	     				
//	        		     达成率，获得费用   
	     				String tSQL15 = "select cast((round(CAST("+tTMonthHu+" AS DOUBLE)/"+tPlanHu+",4))*100 as decimal(10,2)), " +
	     				" cast((case when "+QCHu+"+"+sumQChu+">="+tPreWin+"  then    " +
	     				"case when '"+mAssessYearMonth+"'<='201606' then round(("+tMFYC+"+"+sumFYC+")*min(1,cast(round(CAST("+tTMonthHu+" AS decimal(12,5))/"+tPlanHu+",4) as decimal(12,4))),2) " +
	     				" else round(("+tMFYC+"+"+sumFYC+")*min(1,cast(round(CAST("+tTMonthHu+" AS decimal(12,5))/"+tPlanHu+",4) as decimal(12,4))),2) end" +
	     				" else 0   end) as decimal(10,2)) " +
	     				" from dual   where 1=1 ";
	     				SSRS tSSRS15 = tExeSQL.execSQL(tSQL15);
	     				String tDaCL = tSSRS15.GetText(1, 1);
	     				String tfzFY = tSSRS15.GetText(1, 2);
	     				if("".equals(tfzFY)||null==tfzFY||"null".equals(tfzFY))
	     				{
	     					tfzFY ="0";
	     				}
	     				if(Double.parseDouble(tfzFY)<0){
	     					tfzFY = "0";
	     				}
	     				//基础人力
	     				String tBaseHumanSQL ="select count(1) from  LAhumanDesc a where a.mngcom in ("+tIncludeManagecom+")  and a.year='2016' and a.month='00' ";
	     				String tBaseHumanNum =  tExeSQL.getOneValue(tBaseHumanSQL);
	     				
	     				tSumBaseHuman +=Integer.parseInt(tBaseHumanNum);
	     				tSumTMonthHuW += Integer.parseInt(tTMonthHuW);
	     				tSumLMonthHuW += Integer.parseInt(tLMonthHuW);           
	     				tSumAveHu     += Integer.parseInt(tAveHu);
	     				tSumQCHu      += Integer.parseInt(QCHu);
	     				tSumTMonthHuAdd +=Integer.parseInt(tTMonthHuAdd);
	     				tSumTMonthHuDL	+=Integer.parseInt(tTMonthHuDL);
	     				tSumTMonthHu  += tTMonthHu;
	     				tSumPlanHu    += Integer.parseInt(tPlanHu);
//		            	
	     		}
	     			int tNum = tCaseTraceSSRS.GetText(index, 1).length()>4?4:tCaseTraceSSRS.GetText(index, 1).length();
	     			String managecomSQL = "select substr('"+tCaseTraceSSRS.GetText(index, 1)+"',1,"+tNum+"),(select name from ldcom where comcode =substr('"+tCaseTraceSSRS.GetText(index, 1)+"',1,"+tNum+")),comcode,name from ldcom where substr(comcode,1,"+mSubLength+") ='"+tCaseTraceSSRS.GetText(index, 1)+"'";
	     			SSRS tManagecomSSRS = new SSRS();
	     			tManagecomSSRS = tExeSQL.execSQL(managecomSQL);
	     			mToExcel[index+1][0] = String.valueOf(index);  
	     			
	     			mToExcel[index+1][1] = tManagecomSSRS.GetText(1,1);            
	     			
	     			mToExcel[index+1][2] = tManagecomSSRS.GetText(1,2);
	     			
	     			mToExcel[index+1][3] = tManagecomSSRS.GetText(1,3);
	     			
	     			mToExcel[index+1][4] = tManagecomSSRS.GetText(1,4);
	     			
	     			mToExcel[index+1][5] = tSumBaseHuman+"";
	     			
	     			mToExcel[index+1][6] = tSumTMonthHuW+"";
	     			
	     			mToExcel[index+1][7] = tSumLMonthHuW+"";            
	     			
	     			mToExcel[index+1][8] = tSumAveHu+"";
	     			
	     			mToExcel[index+1][9] = tSumQCHu+"";
	     			
	     			mToExcel[index+1][10] = tSumTMonthHuAdd+"";
	     			
	     			mToExcel[index+1][11] = tSumTMonthHuDL+"";
	     			
	     			mToExcel[index+1][12] = tSumTMonthHu+"";
	     			
	     			mToExcel[index+1][13] = tSumPlanHu+"";
	     			
	     			mToExcel[index+1][14] = df.format((double)tSumTMonthHu*100/(double)(tSumPlanHu))+"%";
	                 
	                mToExcel[index+1][15] = df.format(((double)(tSumQCHu)+(double)(tSumLastQChu))*100/((double)(tSumAveHu)+(double)(tSumLastAveHu)))+"%";
	                
	                tSumAllBaseHuman+=tSumBaseHuman;               
	                tSumAllTMonthHuW+=tSumTMonthHuW;               
	                tSumAllLMonthHuW+=tSumLMonthHuW;                   
	                tSumAllAveHu+=tSumAveHu;              
	                tSumAllQCHu+=tSumQCHu;             
	                tSumAllTMonthHuAdd+=tSumTMonthHuAdd;              
	                tSumAllTMonthHuDL+=tSumTMonthHuDL;             
	                tSumAllTMonthHu+=tSumTMonthHu;              
	                tSumAllPlanHu+=tSumPlanHu;               
	                tSumAllLastQChu+=tSumLastQChu;              
	                tSumAllLastAveHu+=tSumLastAveHu;               
	                System.out.println(tSumLastQChu+"+"+tSumAllLastAveHu);
	     			//求和 放入汇总行          
	     		}
	     		 System.out.println(tSumAllQCHu+"+"+tSumAllLastQChu);
	     		System.out.println(tSumAllAveHu+"+"+tSumAllLastAveHu);
             	 mToExcel[tCaseTraceSSRS.getMaxRow()+2][0] = String.valueOf(tCaseTraceSSRS.getMaxRow()+2-1);
             	 mToExcel[tCaseTraceSSRS.getMaxRow()+2][1] = "汇总";
             	 mToExcel[tCaseTraceSSRS.getMaxRow()+2][5] = tSumAllBaseHuman+"";
             	 mToExcel[tCaseTraceSSRS.getMaxRow()+2][6] = tSumAllTMonthHuW+"";
             	 mToExcel[tCaseTraceSSRS.getMaxRow()+2][7] = tSumAllLMonthHuW+"";
             	 mToExcel[tCaseTraceSSRS.getMaxRow()+2][8] = tSumAllAveHu+"";
             	 mToExcel[tCaseTraceSSRS.getMaxRow()+2][9] = tSumAllQCHu+"";
             	 mToExcel[tCaseTraceSSRS.getMaxRow()+2][10]= tSumAllTMonthHuAdd+"";
             	 mToExcel[tCaseTraceSSRS.getMaxRow()+2][11]= tSumAllTMonthHuDL+"";
             	 mToExcel[tCaseTraceSSRS.getMaxRow()+2][12]= tSumAllTMonthHu+"";
             	 mToExcel[tCaseTraceSSRS.getMaxRow()+2][13]= tSumAllPlanHu+"";
             	 mToExcel[tCaseTraceSSRS.getMaxRow()+2][14] = df.format((double)tSumAllTMonthHu*100/(double)(tSumAllPlanHu))+"%";
                 mToExcel[tCaseTraceSSRS.getMaxRow()+2][15] = df.format(((double)(tSumAllQCHu)+(double)(tSumAllLastQChu))*100/((double)(tSumAllAveHu)+(double)(tSumAllLastAveHu)))+"%";
	             String[] sheetName ={PubFun.getCurrentDate()};
	             System.out.println(sheetName+"------"+mOutXmlPath);
	             t.addSheet(sheetName);
	             t.setData(0, mToExcel);
	             t.write(mOutXmlPath);
	         }else
	         {
	        	 CError tError = new CError();
	             tError.moduleName = "AgentActiveWageXLSBL";
	             tError.functionName = "getInputData";
	             tError.errorMessage = mManageCom+"该机构没有参加三级人力考核";
	             mErrors.addOneError(tError);
	             System.out.println(tError.errorMessage);
	             return false;
	         }
		 } catch (Exception ex) {
	            ex.toString();
	            ex.printStackTrace();
		 } finally {
			 
		 }
		 return true;
	}
    /**
     * 根据传入的管理机构及对应的月份，计算当月的指标
     * @return
     */
    private LAhumandeveiSchema getLastMontData(String cManagecom,String kManagecom,String cMonth)
    {
    	String tWagenoH = "";
        String tYear1 = "";
        String tMonth1 = "";
        String tYear = cMonth.substring(0,4);
        String tMonth = cMonth.substring(4, 6);
        String tDate2 = "2015-12-15";
        String tWageno = "201512";
        String tDate3 = "2017-1-1";
        //求上个月的 wageno 一些类型转换
        String rDate  = tYear+"-"+tMonth+"-"+"01";
        String rDate1 = PubFun.calDate(rDate, -1, "D", "2010-12-31");
        String rDate3 = AgentPubFun.formatDate(rDate1, "yyyyMMdd");
        
        tYear1 = rDate3.substring(0,4);
        tMonth1 = rDate3.substring(4, 6);
        tWagenoH = rDate3.substring(0, 6);
        //从201607月份开始，进行年度通算，合格率=（201601至本月合格人力之和）÷（201601至本月平均人力之和）
        
        //如果大于等于201607,则需要统计本月之前月份的合格人力之和，平均人力之和，累计fyc
        //本月之前合格人力之和
        String  sumQChu ="0";
      //本月之前平均人力之和
        String  sumAveHu="0";
      //本月之前fyc之和
        String sumFYC ="0";
      //本月之前实发金额之和
        String sumRealPay ="0";
        System.out.println("201606".compareTo("201607"));
        System.out.println("201608".compareTo("201607"));
        if(cMonth.compareTo("201607")>=0)
        {
        	String sumSQL="select sum(QCHu),sum(AveHu),sum(mfyc),sum(RealPay) from LAhumandevei where year ='"+tYear+"' and month<'"+tMonth+"' and mngcom ="+kManagecom+" with ur";
        	SSRS tSSRS = new SSRS();
        	tSSRS =  tExeSQL.execSQL(sumSQL);
        	if(tSSRS!=null&&tSSRS.getMaxRow()>0)
        	{
        		sumQChu = tSSRS.GetText(1, 1);
        		sumAveHu = tSSRS.GetText(1, 2);
        		sumFYC = tSSRS.GetText(1, 3);
        		sumRealPay = tSSRS.GetText(1, 4);
        	}
        }
//        
// 机构 ，方案编码
        String tSQL1 = " select code,codename from ldcode where codetype='2016humandeveidesc' " +
        		" and code="+kManagecom+" with ur ";
        System.out.println("tSQL1:"+tSQL1);
        
        SSRS tSSRS1 = tExeSQL.execSQL(tSQL1);
        
        String tMngcom = tSSRS1.GetText(1, 1);//考核机构
        String tPlanCode = tSSRS1.GetText(1, 2);//考核机构方案编码

     // 方案，档次
        String tSQL2 = " select codealias,comcode,riskwrapplanname,othersign from ldcode1 where codetype='2016humandeveiplan' and code='"+tPlanCode+"' ";
        System.out.println("tSQL2:"+tSQL2);
        
        SSRS tSSRS2 = tExeSQL.execSQL(tSQL2);
        
        String tPlan = tSSRS2.GetText(1, 1);//方案
        String tLevel = tSSRS2.GetText(1, 2);//档次
        String tSumFYCStand = tSSRS2.GetText(1, 3);//合格人力标准
        String tWin = tSSRS2.GetText(1, 4);//合格比例
        

        // 计划净增人力
        String tSQL3 = " select code1 from ldcode1 where codetype='2016humandeveistand' " +
        		"and  codealias='"+tPlan+"' and comcode='"+tLevel+"' and othersign='"+tMonth+"' ";
        System.out.println("tSQL3:"+tSQL3);
        
        SSRS tSSRS3 = tExeSQL.execSQL(tSQL3);
        
        String tPlanHu = tSSRS3.GetText(1, 1);//计划净增人力


        //累计本月新增有效人力
        String tSQL4 = "";
 //       if("86120000".equals(mManageCom)){
        	
//         tSQL4 = "select count(1) from " +
//			   " (select  a.agentcode,sum(a.fyc) " +
//			   " from lacommision a,laagent b	" +
//			   " where 1=1 and a.agentcode=b.agentcode and a.BranchType='1' and a.branchtype2='01' and Payyear = 0 and a.renewcount ='0' and " +
//			   " exists(select 1 from LAhumanDesc c where c.mngcom in ('86120000','86120100') and c.year='"+tYear+"' and c.month='"+tMonth+"' and c.agentcode=b.agentcode) " +
//			   " and b.EmployDate>'"+ tDate2 +"' and a.managecom in ('86120000','86120100') " +
//			   " and not exists(select 1 from lacommision  d where d.TRANSTYPE='WT'  and d.polno=a.polno and d.wageno>'201411' and d.managecom='"+mManageCom+"') " +
////			   " and (( wageno='"+ tWageno +"' and Tmakedate >'"+ tDate2 +"' ) " +
////			   " or (wageno>'"+ tWageno +"' and wageno<'"+ cMonth +"' ) " +
////			   " or(wageno='"+ cMonth +"' and Tmakedate <='"+tDate1+"')) " +
//			   " and (wageno<='"+ cMonth +"' and Tmakedate <='"+tDate1+"') " +
//			   " group by a.agentcode " +
//			   " having sum(a.fyc)>0)";
//        }else{
        	
         tSQL4 = "select count(1) from " +
        			   " (select  a.agentcode,sum(a.fyc) " +
        			   " from lacommision a,laagent b	" +
        			   " where 1=1 and a.agentcode=b.agentcode and a.BranchType='1' and a.branchtype2='01' and Payyear = 0 and a.renewcount ='0' and " +
        			   " exists(select 1 from LAhumanDesc c where c.mngcom in ("+cManagecom+") and c.year='"+tYear+"' and c.month='"+tMonth+"' and c.agentcode=b.agentcode) " +
        			   " and b.EmployDate>'"+ tDate2 +"' and a.managecom in ("+cManagecom+") " +
        			   " and not exists(select 1 from lacommision  d where d.TRANSTYPE='WT'  and d.polno=a.polno and d.wageno>'201511' and d.managecom in("+cManagecom+")) " +
//        			   " and (( wageno='"+ tWageno +"' and Tmakedate >'"+ tDate2 +"' ) " +
//        			   " or (wageno>'"+ tWageno +"' and wageno<'"+ cMonth +"' ) " +
//        			   " or(wageno='"+ cMonth +"' and Tmakedate <='"+tDate1+"')) " +
        			   " and Tmakedate >'"+ tDate2+"'"+
        			   " and (wageno between'"+ tWageno +"' and '"+ cMonth +"')" +
        			   " and Tmakedate <'"+tDate3+"'"+
        			   " group by a.agentcode " +
        			   " having sum(a.fyc)>0)";
 //       }
        SSRS tSSRS4 = tExeSQL.execSQL(tSQL4);
        String tTMonthHuAdd = "";//累计有效人力
        tTMonthHuAdd = tSSRS4.GetText(1, 1);
        
       //基础人力中离职的人数
        String tSQL41 ="";
//        if("86120000".equals(mManageCom)){
//        	
//        	tSQL41 = "select count(1) from  LAhumanDesc a where a.mngcom in ('86120000','86120100') and a.year='2014' and a.month='12' " +
//   			   " and not exists(select 1 from LAhumanDesc b where b.mngcom in ('86120000','86120100') and b.year='"+tYear+"' and b.month='"+tMonth+"' and a.agentcode=b.agentcode) " ;
//   			  
//           }else{
//           	
       	   tSQL41 = "select count(1) from  LAhumanDesc a where a.mngcom in ("+cManagecom+")  and a.year='2016' and a.month='00' and a.calflag <> 'Y'"  +
   			   " and not exists(select 1 from LAhumanDesc b where b.mngcom in ("+cManagecom+")  and b.year='"+tYear+"' and b.month='"+tMonth+"' and a.agentcode=b.agentcode  ) " ;
//          }
        SSRS tSSRS41 = tExeSQL.execSQL(tSQL41);
        String tTMonthHuDL = "";//基础人力中离职的人数
        tTMonthHuDL = tSSRS41.GetText(1, 1);
        
        //月度净增累计有效人力
        int tTMonthHu =0;
        tTMonthHu = (Integer.parseInt(tTMonthHuAdd)-Integer.parseInt(tTMonthHuDL));
        
//        截至上月新增有效人力
        String tSQL5 = "select coalesce((select sum(TMonthHu) from lahumandevei where mngcom in ("+cManagecom+") and remark1='"+tWagenoH+"'),0) from dual where 1=1 ";
       
        SSRS tSSRS5 = tExeSQL.execSQL(tSQL5);
        
        String tLMonthHu = tSSRS5.GetText(1, 1);//截至上月新增有效人力
        
//        当月净增有效人力
//        String tSQL6 = "select "+tTMonthHu+"-"+tLMonthHu+" from dual where 1= 1 ";
//        
//        SSRS tSSRS6 = tExeSQL.execSQL(tSQL6);
        
//        String RealHu = tSSRS6.GetText(1, 1);//当月净增有效人力
    	int RealHu =tTMonthHu-Integer.parseInt(tLMonthHu);//当月净增有效人力
        
//        当月合格人力
        String tSQL7 = "";
//        if("86120000".equals(mManageCom)){
//        	
//        	tSQL7  = "select count(1) from " +
// 		   " (select  a.agentcode,sum(a.fyc) " +
// 		   " from lacommision a	" +
// 		   " where 1=1 and a.BranchType='1' and a.branchtype2='01' and Payyear = 0 and a.renewcount ='0' and " +
// 		   " exists(select 1 from LAhumanDesc c where c.mngcom in ('86120000','86120100') and c.year='"+tYear+"' and c.month='"+tMonth+"' and c.agentcode=a.agentcode) " +
// 		   " and a.managecom in ('86120000','86120100') " +
// 		   " and not exists(select 1 from lacommision  d where d.TRANSTYPE='WT'  and d.polno=a.polno and d.wageno>'201411' and d.managecom in ("+cManagecom+")) " +
// //		   " and (( wageno='"+ tWagenoH +"' and Tmakedate >'"+ tDate +"' ) " +
// //		   " or(wageno='"+ cMonth +"' and Tmakedate <='"+tDate1+"')) " +
// 		   " and (wageno between '"+ tWageno +"' and '"+ cMonth +"')"+
// 		   " group by a.agentcode " +
// 		   " having sum(a.fyc)>='"+tSumFYCStand+"')";
//        }else{
        
         tSQL7  = "select count(1) from " +
		   " (select  a.agentcode,sum(a.fyc) " +
		   " from lacommision a	" +
		   " where 1=1 and a.BranchType='1' and a.branchtype2='01'  and Payyear = 0 and a.renewcount ='0' and " +
		   " exists(select 1 from LAhumanDesc c where c.mngcom in("+cManagecom+") and c.year='"+tYear+"' and c.month='"+tMonth+"' and c.agentcode=a.agentcode) " +
		   " and a.managecom in ("+cManagecom+") " +
		   " and not exists(select 1 from lacommision  d where d.TRANSTYPE='WT'  and d.polno=a.polno and d.wageno>'201511' and d.managecom in ("+cManagecom+")) " ;
//		   " and Tmakedate >'"+ tDate2+"'"+
//		   " and (case when "+tMonth +"=01 then wageno between'"+ tWagenoH +"' and '"+ cMonth +
//		   " else  wageno = "+cMonth	+"')" +
//		   " and Tmakedate <'"+tDate3+"'"+
		   if(tMonth.equals("01")){
			   tSQL7 +="and wageno between '"+tWagenoH+"' and '"+cMonth+"' and Tmakedate > '"+tDate2+"'";
		   }else if(tMonth.equals("12")){
			   tSQL7 +="and wageno = '"+cMonth+"' and Tmakedate < '"+tDate3+"'";
		   }else{
			   tSQL7 +="and wageno = '"+cMonth+"'";
		   }
		   tSQL7 +=" group by a.agentcode  having sum(a.fyc)>='"+tSumFYCStand+"')";
 //       }
        SSRS tSSRS7 = tExeSQL.execSQL(tSQL7);
        String QCHu = tSSRS7.GetText(1, 1);
        
//        上月在职人数,期初人力
        String tSQL8 = "";
//        if("86120000".equals(mManageCom)){
//        	 tSQL8 = "select count(1) from LAhumanDesc where mngcom  in ('86120000','86120100') and year='"+tYear1+"' and month='"+tMonth1+"' ";
//        }else{
        if(tMonth.equals("01")){
        	tSQL8 = "select count(1) from LAhumanDesc where mngcom in("+cManagecom+") and year='2016' and month='00' ";
        }else{
         tSQL8 = "select count(1) from LAhumanDesc where mngcom in("+cManagecom+") and year='"+tYear1+"' and month='"+tMonth1+"' ";
        }
        //}
        SSRS tSSRS8 = tExeSQL.execSQL(tSQL8);
        String tTMonthHuW = tSSRS8.GetText(1, 1);
        
//        当月在职人数，期末人力
        String tSQL9 = "";
//        if("86120000".equals(mManageCom)){
//        	tSQL9 = "select count(1) from LAhumanDesc where mngcom in ('86120000','86120100') and year='"+tYear+"' and month='"+tMonth+"' ";
//        }else{
         tSQL9 = "select count(1) from LAhumanDesc where mngcom in("+cManagecom+") and year='"+tYear+"' and month='"+tMonth+"' ";
//        }
        SSRS tSSRS9 = tExeSQL.execSQL(tSQL9);
        String tLMonthHuW = tSSRS9.GetText(1, 1);

//        月均人力
        String tSQL10 = "select round(cast("+tTMonthHuW+"+"+tLMonthHuW+" as decimal(12,1)) /2,0) from dual where 1 = 1 ";
        SSRS tSSRS10 = tExeSQL.execSQL(tSQL10);
        String tAveHu = tSSRS10.GetText(1, 1);
        double aveNum = Double.parseDouble(tAveHu);
        if(0==aveNum)
        {
        	tAveHu = "0";
        }

//		获奖标准
        String tSQL11 = "select round(cast("+tTMonthHuW+"+"+tLMonthHuW+"+"+sumAveHu+"*2 as decimal(12,1)) /(2*"+tWin+"),0) from dual where 1 = 1 ";
        SSRS tSSRS11 = tExeSQL.execSQL(tSQL11);
        String tPreWin = tSSRS11.GetText(1, 1);
        double PreWinNum = Double.parseDouble(tPreWin);
        if(0==PreWinNum)
        {
        	tPreWin = "0";
        }
        
//        月度FYC
        String tSQL12 = "";
//        if("86120000".equals(mManageCom)){
//         tSQL12 = " select coalesce(sum(FYC),0) from lacommision where BranchType='1' and branchtype2='01' and managecom in ('86120000','86120100') and wageno='"+cMonth+"'" +
//         		" and ((Payyear = 0 and renewcount =0) or  renewcount >0) ";
//        }else{
        	tSQL12 = " select coalesce(sum(FYC),0) from lacommision where BranchType='1' and branchtype2='01' and managecom in("+cManagecom+") and wageno='"+cMonth+"' " +
        	" and ((Payyear = 0 and renewcount =0) or  renewcount >0) ";
 //       }
        SSRS tSSRS12 = tExeSQL.execSQL(tSQL12);
        String tMFYC = tSSRS12.GetText(1, 1);

//当月新增 但是 当月未出单人数
        String tSQL13 = "";
//        if("86120000".equals(mManageCom)){
//         tSQL13 = "select count(1) from laagent a where a.BranchType='1' and a.branchtype2='01' and a.managecom in ('86120000','86120100') " +
//         		" and exists(select 1 from LAhumanDesc c where c.mngcom in ('86120000','86120100') and c.year='"+tYear+"' and c.month='"+tMonth+"' and c.agentcode=a.agentcode) " +
//        		" and a.EmployDate >'"+tDate+"' and a.EmployDate<='"+tDate1+"' " +
//        		" and not exists(select 1 from lacommision " +
//        		" where  (( wageno='"+ tWagenoH +"' and Tmakedate >'"+ tDate +"' )  " +
//        		" or(wageno='"+ cMonth +"' and Tmakedate <='"+tDate1+"')) " +
//        		" and managecom in ('86120000','86120100') and BranchType='1' and branchtype2='01' and agentcode=a.agentcode) ";
//        }else{
        	tSQL13 = "select count(1) from laagent a where a.BranchType='1' and a.branchtype2='01' and a.managecom in ("+cManagecom+") " +
    		" and not exists(select 1 from lacommision " +
			" where  Tmakedate >'"+ tDate2+"'"+
			" and (wageno between'"+ tWageno +"' and '"+ cMonth +"')" +
			" and Tmakedate <'"+tDate3+"'"+
    		" and managecom in ("+cManagecom+") and BranchType='1' and branchtype2='01' and agentcode=a.agentcode) ";
  //      }
        SSRS tSSRS13 = tExeSQL.execSQL(tSQL13);
        String tRemark2 = tSSRS13.GetText(1, 1);
//	     达成率，获得费用   
        String tSQL15 = "select cast((round(CAST("+tTMonthHu+" AS DOUBLE)/"+tPlanHu+",4))*100 as decimal(10,2)), " +
        		" cast((case when "+QCHu+"+"+sumQChu+">="+tPreWin+"  then    " +
        					"case when '"+cMonth+"'<='201606' then round(("+tMFYC+"+"+sumFYC+")*min(1,cast(round(CAST("+tTMonthHu+" AS decimal(12,5))/"+tPlanHu+",4) as decimal(12,4))),2) " +
							" else round(("+tMFYC+"+"+sumFYC+")*min(1,cast(round(CAST("+tTMonthHu+" AS decimal(12,5))/"+tPlanHu+",4) as decimal(12,4))),2) end" +
						" else 0   end) as decimal(10,2)) " +
        		" from dual   where 1=1 ";
        SSRS tSSRS15 = tExeSQL.execSQL(tSQL15);
        String tDaCL = tSSRS15.GetText(1, 1);
        String tfzFY = tSSRS15.GetText(1, 2);
        if("".equals(tfzFY)||null==tfzFY||"null".equals(tfzFY))
    	{
        	tfzFY ="0";
    	}
        if(Double.parseDouble(tfzFY)<0){
        	tfzFY = "0";
        }
        double tRealPay = Double.parseDouble(tfzFY)-Double.parseDouble(sumRealPay);
        /*if(Integer.parseInt(tMonth)>=4&&Integer.parseInt(tMonth)<=6){
        	String tSQL16 = "";
        	tSQL16="select sum(cast(remark3 as decimal(10,2))),sum(realpay) from lahumandevei where mngcom in ("+cManagecom+") and year = '"+tYear+"' and month < '"+tMonth+"'";
        	SSRS tSSRS16 = tExeSQL.execSQL(tSQL16);
        	String sRemark3 = tSSRS16.GetText(1, 1);
        	String sRealPay = tSSRS16.GetText(1, 2);
        	System.out.println(sRemark3+"==="+sRealPay);
        	if("".equals(sRemark3)||null==sRemark3||"null".equals(sRemark3))
        	{
        		sRemark3 ="0";
        	}
        	if("".equals(sRealPay)||null==sRealPay||"null".equals(sRealPay))
        	{
        		sRealPay ="0";
        	}
        	System.out.println(sRemark3+"==="+sRealPay);
        	tRealPay = Double.parseDouble(tfzFY)+Double.parseDouble(sRemark3)-Double.parseDouble(sRealPay) ;
        }*/
        if(tRealPay<0){
        	tRealPay = 0;
        }
        
        System.out.println("dachenglv:"+tDaCL);
        System.out.println("jine:"+tfzFY);
        
		LAhumandeveiSchema tLAhumandeveiSchema = new LAhumandeveiSchema();
		tLAhumandeveiSchema.setMngcom(tMngcom);
		tLAhumandeveiSchema.setYear(tYear);
		tLAhumandeveiSchema.setMonth(tMonth);
		tLAhumandeveiSchema.setOperator(mGlobalInput.Operator);
		tLAhumandeveiSchema.setPlanCode(tPlanCode);
		tLAhumandeveiSchema.setPlan(tPlan);
		tLAhumandeveiSchema.setLevel(tLevel);
		tLAhumandeveiSchema.setTMonthHu(tTMonthHu);
		tLAhumandeveiSchema.setLMonthHu(tLMonthHu);
		tLAhumandeveiSchema.setAveHu(tAveHu);
		tLAhumandeveiSchema.setPreWin(tPreWin);
		tLAhumandeveiSchema.setPlanHu(tPlanHu);
		tLAhumandeveiSchema.setRealHu(RealHu);
		tLAhumandeveiSchema.setQCHu(QCHu);
		tLAhumandeveiSchema.setMFYC(tMFYC);
		tLAhumandeveiSchema.setTMonthHuW(tTMonthHuW);
		tLAhumandeveiSchema.setLMonthHuW(tLMonthHuW);
		tLAhumandeveiSchema.setRemark1(cMonth);
		tLAhumandeveiSchema.setRemark2(tRemark2);
		tLAhumandeveiSchema.setRemark3(tfzFY);
		tLAhumandeveiSchema.setRealPay(tRealPay);
		tLAhumandeveiSchema.setSumHu(tTMonthHuAdd);
		tLAhumandeveiSchema.setOutWorkHu(tTMonthHuDL);
		tLAhumandeveiSchema.setMakeDate(PubFun.getCurrentDate());
		tLAhumandeveiSchema.setMakeTime(PubFun.getCurrentTime());
		tLAhumandeveiSchema.setModifyDate(PubFun.getCurrentDate());
		tLAhumandeveiSchema.setModifyTime(PubFun.getCurrentTime());
    	return tLAhumandeveiSchema;
    }

}
