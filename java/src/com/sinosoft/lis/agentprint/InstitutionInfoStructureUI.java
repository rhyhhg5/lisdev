package com.sinosoft.lis.agentprint;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class InstitutionInfoStructureUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    /**取得查询条件中管理机构的值*/
    private String mManageCom;
    public InstitutionInfoStructureUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        /**得到从外部传来的数据，并备份到本类中*/
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!dealData())
        {
            return false;
        }
        VData vData = new VData();
        /**得到从外部传来的数据，并备份到本类中*/
        if (!prepareOutputData(vData))
        {
            return false;
        }
        /**产生一个BL类，开始调用BL操作*/
        InstitutionInfoStructureBL tInstitutionInfoStructureBL = new
                InstitutionInfoStructureBL();
        System.out.println("Start AgentInfoPrintUI Submit ...");
        /**将准备好的数据传入给BL类，开始查找数据库*/
        if (!tInstitutionInfoStructureBL.submitData(vData, cOperate))
        {
            if (tInstitutionInfoStructureBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tInstitutionInfoStructureBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData",
                           "tInstitutionInfoStructureBL发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            /**从BL类中返回查询结果*/
            mResult = tInstitutionInfoStructureBL.getResult();
            return true;
        }

    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "InstitutionInfoStructureUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getInputData(VData cInputData)
    {
        mManageCom = (String) cInputData.get(0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        return true;
    }

    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.addElement(mManageCom);
            vData.add(mGlobalInput);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    private boolean dealData()
    {
        return true;
    }

    /**将从BL类中返回到数据传递到上一层*/
    public VData getResult()
    {
        return this.mResult;
    }

    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        String tManageCom = "8611";
        VData tVData = new VData();
        tVData.addElement(tManageCom);
        tVData.add(tGlobalInput);
        InstitutionInfoStructureUI tInstitutionInfoStructureUI = new
                InstitutionInfoStructureUI();
        if (!tInstitutionInfoStructureUI.submitData(tVData, ""))
        {
            if (tInstitutionInfoStructureUI.mErrors.needDealError())
            {
                System.out.println(tInstitutionInfoStructureUI.mErrors.
                                   getFirstError());
            }
            else
            {
                System.out.println("UI发生错误，但是没有提供详细的出错信息");
            }
        }
        else
        {
            VData vData = tInstitutionInfoStructureUI.getResult();
            System.out.println("已经接收了数据!!!");
        }

    }

}