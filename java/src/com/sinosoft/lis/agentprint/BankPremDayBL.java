package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class BankPremDayBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //时间,险种，管理机构
    private GlobalInput mGlobalInput = new GlobalInput();
    private String Day = "";
    private String RiskCode = "";
    private String ManageCom = "";

    //全局变量
    private String strArr[] = null;
    private double data[] = null;
    private double data1[] = null;
    private ListTable tlistTable = new ListTable();
    //业务处理相关变量
    /** 全局数据 */
    public BankPremDayBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        Day = (String) cInputData.get(0);
        RiskCode = (String) cInputData.get(1);
        ManageCom = (String) cInputData.get(2);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "DepartOrderBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    //定义dealDate函数
    private boolean dealData(SSRS aSSRS)
    {
        String mDay[] = PubFun.calFLDate(Day); //取界面录入的日期的所在月的第一天
        String Year = Day.substring(0, 4) + "-01-01"; //取界面录入的日期的所在年的第一天
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);
            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                for (int b = 1; b <= aSSRS.getMaxCol(); b++)
                {
                    if (b == 1)
                    {
                        //银行名称
                        strArr[0] = aSSRS.GetText(a, 1);
                    }
                    if (b == 2)
                    {
                        //险种名称
                        if (RiskCode.equals("") || RiskCode == null)
                        {
                            String dsql = "select trim(riskname),riskcode from lmriskapp where riskcode in (select riskcode from lacommision where tmakeDate<='" +
                                          Day + "' and AgentCom like '" +
                                          aSSRS.GetText(a, 2) + "%')";
                            SSRS dSSRS = new SSRS();
                            dSSRS = aExeSQL.execSQL(dsql);
                            if (dSSRS.getMaxRow() == 0)
                            {
                                continue;
                            }
                            for (int c = 1; c <= dSSRS.getMaxRow(); c++)
                            {
                                for (int d = 1; d <= dSSRS.getMaxCol(); d++)
                                {
                                    if (d == 1)
                                    {
                                        strArr[1] = dSSRS.GetText(c, 1);
                                    }
                                    if (d == 2)
                                    {
                                        //年计划任务
                                        String esql =
                                                "select nvl(sum(planvalue),0) from laplan where planperiod='" +
                                                Day.substring(0, 4) +
                                                "' and plantype='2' and planperiodunit='12'  and planobject='" +
                                                aSSRS.GetText(a, 2) + "'";
                                        strArr[2] = aExeSQL.getOneValue(esql);
                                        //预收保费本日小计
                                        String fsql =
                                                "select nvl(sum(prem),0) from Lcpol where MakeDate='" +
                                                Day +
                                                "' and uwflag<>'a' and AgentCom like '" +
                                                aSSRS.GetText(a, 2) +
                                                "%' and riskcode='" +
                                                dSSRS.GetText(c, 2) + "'";
                                        strArr[3] = aExeSQL.getOneValue(fsql);
                                        data[3] += Double.parseDouble(strArr[3]);
                                        //本月合计
                                        String gsql =
                                                "select nvl(sum(prem),0) from Lcpol where  MakeDate>='" +
                                                mDay[0] + "' and MakeDate<='" +
                                                Day +
                                                "' and uwflag<>'a' and AgentCom like'" +
                                                aSSRS.GetText(a, 2) +
                                                "%' and riskcode='" +
                                                dSSRS.GetText(c, 2) + "'";
                                        strArr[4] = aExeSQL.getOneValue(gsql);
                                        data[4] += Double.parseDouble(strArr[4]);
                                        //本年累计
                                        String hsql =
                                                "select nvl(sum(prem),0)  from Lcpol where MakeDate>='" +
                                                Year + "' and MakeDate<='" +
                                                Day +
                                                "' and uwflag<>'a' and AgentCom like '" +
                                                aSSRS.GetText(a, 2) +
                                                "%' and riskcode='" +
                                                dSSRS.GetText(c, 2) + "'";
                                        strArr[5] = aExeSQL.getOneValue(hsql);
                                        data[5] += Double.parseDouble(strArr[5]);
                                        //实收保费本日小计
                                        String isql =
                                                "select nvl(sum(transmoney),0) from Lacommision where tmakeDate='" +
                                                Day + "' and AgentCom like '" +
                                                aSSRS.GetText(a, 2) +
                                                "%' and riskcode='" +
                                                dSSRS.GetText(c, 2) + "'";
                                        strArr[6] = aExeSQL.getOneValue(isql);
                                        data[6] += Double.parseDouble(strArr[6]);
                                        //本月合计
                                        String jsql =
                                                "select nvl(sum(transmoney),0) from Lacommision where  tmakeDate>='" +
                                                mDay[0] + "' and tmakeDate<='" +
                                                Day + "' and AgentCom like '" +
                                                aSSRS.GetText(a, 2) +
                                                "%' and riskcode='" +
                                                dSSRS.GetText(c, 2) + "'";
                                        strArr[7] = aExeSQL.getOneValue(jsql);
                                        data[7] += Double.parseDouble(strArr[7]);
                                        //本年累计
                                        String ksql =
                                                "select nvl(sum(transmoney),0) from Lacommision where tmakeDate>='" +
                                                Year + "' and tmakeDate<='" +
                                                Day + "' and AgentCom like '" +
                                                aSSRS.GetText(a, 2) +
                                                "%' and riskcode='" +
                                                dSSRS.GetText(c, 2) + "'";
                                        strArr[8] = aExeSQL.getOneValue(ksql);
                                        data[8] += Double.parseDouble(strArr[8]);
                                        //年达成率
                                        if (strArr[2].equals("0"))
                                        {
                                            strArr[9] = "0";
                                        }
                                        else
                                        {
                                            strArr[9] = String.valueOf(Double.
                                                    parseDouble(strArr[8]) /
                                                    Double.parseDouble(strArr[2]));
                                        }
                                        tlistTable.add(strArr);
                                        strArr = new String[10];
                                    }
                                }
                            }
                        }
                        else
                        {
                            String lsql =
                                    "select trim(riskname) from lmriskapp where riskcode='" +
                                    RiskCode + "'";
                            strArr[1] = aExeSQL.getOneValue(lsql);
                            //年计划任务
                            String msql =
                                    "select nvl(sum(planvalue),0) from laplan where planperiod='" +
                                    Day.substring(0, 4) +
                                    "' and  plantype='2' and planperiodunit='12' and planobject='" +
                                    aSSRS.GetText(a, 2) + "'";
                            strArr[2] = aExeSQL.getOneValue(msql);
                            //预收保费本日小计
                            String nsql =
                                    "select nvl(sum(prem),0) from Lcpol where MakeDate='" +
                                    Day +
                                    "'  and uwflag<>'a' and AgentCom like '" +
                                    aSSRS.GetText(a, 2) + "%' and riskcode='" +
                                    RiskCode + "'";
                            strArr[3] = aExeSQL.getOneValue(nsql);
                            data[3] += Double.parseDouble(strArr[3]);
                            //本月合计
                            String osql =
                                    "select nvl(sum(prem),0) from Lcpol where  MakeDate>='" +
                                    mDay[0] + "' and MakeDate<='" + Day +
                                    "' and uwflag<>'a' and AgentCom like'" +
                                    aSSRS.GetText(a, 2) + "%' and riskcode='" +
                                    RiskCode + "'";
                            strArr[4] = aExeSQL.getOneValue(osql);
                            data[4] += Double.parseDouble(strArr[4]);
                            //本年累计
                            String psql =
                                    "select nvl(sum(prem),0)  from Lcpol where MakeDate>='" +
                                    Year + "' and MakeDate<='" + Day +
                                    "' and uwflag<>'a' and AgentCom like '" +
                                    aSSRS.GetText(a, 2) + "%' and riskcode='" +
                                    RiskCode + "'";
                            strArr[5] = aExeSQL.getOneValue(psql);
                            data[5] += Double.parseDouble(strArr[5]);
                            //实收保费本日小计
                            String qsql =
                                    "select nvl(sum(transmoney),0) from Lacommision where tmakeDate='" +
                                    Day + "' and AgentCom like '" +
                                    aSSRS.GetText(a, 2) + "%' and riskcode='" +
                                    RiskCode + "'";
                            strArr[6] = aExeSQL.getOneValue(qsql);
                            data[6] += Double.parseDouble(strArr[6]);
                            //本月合计
                            String rsql =
                                    "select nvl(sum(transmoney),0) from Lacommision where  tmakeDate>='" +
                                    mDay[0] + "' and tmakeDate<='" + Day +
                                    "' and AgentCom like '" +
                                    aSSRS.GetText(a, 2) + "%' and riskcode='" +
                                    RiskCode + "'";
                            strArr[7] = aExeSQL.getOneValue(rsql);
                            data[7] += Double.parseDouble(strArr[7]);
                            //本年累计
                            String ssql =
                                    "select nvl(sum(transmoney),0) from Lacommision where tmakeDate>='" +
                                    Year + "' and tmakeDate<='" + Day +
                                    "' and AgentCom like '" +
                                    aSSRS.GetText(a, 2) + "%' and riskcode='" +
                                    RiskCode + "'";
                            strArr[8] = aExeSQL.getOneValue(ssql);
                            data[8] += Double.parseDouble(strArr[8]);
                            //年达成率
                            if (strArr[2].equals("0"))
                            {
                                strArr[9] = "0";
                            }
                            else
                            {
                                strArr[9] = String.valueOf(Double.parseDouble(
                                        strArr[8]) /
                                        Double.parseDouble(strArr[2]));
                            }
                            tlistTable.add(strArr);
                            strArr = new String[10];

                        }
                    }
                }
            }

            //小合计
            strArr[0] = "合计";
            for (int i = 2; i <= 8; i++)
            {
                strArr[i] = String.valueOf(data[i]);
                data1[i] += data[i];
            }
            if (strArr[2].equals("0.0"))
            {
                strArr[9] = "0";
            }
            else
            {
                strArr[9] = String.valueOf(data[8] / data[2]);
            }
            tlistTable.add(strArr);
            strArr = new String[10];
            data = new double[10];
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankPremDayBL";
            tError.functionName = "DealData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }

        return true;
    }


    //主函数
    private boolean queryData()
    {
        tlistTable.setName("BankPremDay");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("Day", Day); //输入制表时间
        texttag.add("RiskCode", RiskCode); //险种
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);
            String aasql = "select shortname from ldcom where comcode='" +
                           ManageCom + "'";
            texttag.add("ManageCom", aExeSQL.getOneValue(aasql)); //管理机构
            strArr = new String[10];
            data = new double[10];
            data1 = new double[10];
            //  四家国家银行
            String asql = "select name,agentcom from lacom where banktype='01' and upagentcom in ('1101','1102','1103','1104') and managecom like '" +
                          ManageCom + "%'";
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(asql);
            dealData(aSSRS);
            //其他银行
            String bsql = "select name,agentcom from lacom where banktype='01' and upagentcom not in ('1101','1102','1103','1104') and managecom like '" +
                          ManageCom + "%'";
            SSRS bSSRS = new SSRS();
            bSSRS = aExeSQL.execSQL(bsql);
            dealData(bSSRS);
            //代理机构
            String csql =
                    "select name,agentcom from lacom where banktype is null and managecom like '" +
                    ManageCom + "%'";
            SSRS cSSRS = new SSRS();
            cSSRS = aExeSQL.execSQL(csql);
            dealData(cSSRS);

            //总计
            strArr = new String[10];
            strArr[0] = "总计";
            for (int i = 2; i <= 8; i++)
            {
                strArr[i] = String.valueOf(data1[i]);
            }
            if (strArr[2].equals("0.0"))
            {
                strArr[9] = "0";
            }
            else
            {
                strArr[9] = String.valueOf(data1[8] / data1[2]);
            }
            tlistTable.add(strArr);
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("BankPremDay.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankPremDayBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    public static void main(String[] args)
    {
    }
}