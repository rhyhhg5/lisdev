package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class AgentWagePF1PUI
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    //private VData mInputData = new VData();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String strTemplatePath = null;
    private String BranchAttr;
    private String AgentGroup;
    private String IndexCalNo;

    public AgentWagePF1PUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }
            // 准备传往后台的数据
            VData vData = new VData();

            if (!prepareOutputData(vData))
            {
                return false;
            }
            if (cOperate.equals("WAGE"))
            {
                LAEmployeeBL tLAEmployeeBL = new LAEmployeeBL();
                System.out.println("Start LAEmployeeBL UI Submit ...");

                if (!tLAEmployeeBL.submitData(vData, cOperate))
                {
                    if (tLAEmployeeBL.mErrors.needDealError())
                    {
                        mErrors.copyAllErrors(tLAEmployeeBL.mErrors);
                        return false;
                    }
                    else
                    {
                        buildError("submitData",
                                   "tLAEmployeeBL发生错误，但是没有提供详细的出错信息");
                        return false;
                    }
                }
                else
                {
                    mResult = tLAEmployeeBL.getResult();
                    System.out.println("end");
                    return true;
                }
            }
            else
            {
                AgentWagePF1PBL tAgentWagePF1PBL = new AgentWagePF1PBL();
                System.out.println("Start AgentWagePF1P UI Submit ...");

                if (!tAgentWagePF1PBL.submitData(vData, cOperate))
                {
                    if (tAgentWagePF1PBL.mErrors.needDealError())
                    {
                        mErrors.copyAllErrors(tAgentWagePF1PBL.mErrors);
                        return false;
                    }
                    else
                    {
                        buildError("submitData",
                                   "AgentWagePF1PBL发生错误，但是没有提供详细的出错信息");
                        return false;
                    }
                }
                else
                {
                    mResult = tAgentWagePF1PBL.getResult();
                    System.out.println("end");
                    return true;
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CError cError = new CError();
            cError.moduleName = "AgentWagePF1PUI";
            cError.functionName = "submit";
            cError.errorMessage = e.toString();
            mErrors.addOneError(cError);
            return false;
        }
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.add(AgentGroup);
            vData.add(BranchAttr);
            vData.add(IndexCalNo);
            vData.add(strTemplatePath);
            vData.add(mGlobalInput);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        AgentGroup = ((String) cInputData.getObject(0));
        BranchAttr = ((String) cInputData.getObject(1));
        IndexCalNo = ((String) cInputData.getObject(2));
        strTemplatePath = ((String) cInputData.getObject(3));
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 4));

        System.out.println("start");
        System.out.println(mGlobalInput.Operator);
        System.out.println(mGlobalInput.ManageCom);

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "AgentWagePF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {

        String strTemplatePath = null;
        String BranchAttr = "861100000101";
        String IndexCalNo = "200307";
        String AgentGroup = "";
        VData tVData = new VData();
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86";
        tG.ComCode = "86";
        tVData.addElement(AgentGroup);
        tVData.addElement(BranchAttr);
        tVData.addElement(IndexCalNo);
        tVData.add(strTemplatePath);
        tVData.add(tG);
        AgentWagePF1PUI tUI = new AgentWagePF1PUI();
        tUI.submitData(tVData, "PRINT");

    }
}