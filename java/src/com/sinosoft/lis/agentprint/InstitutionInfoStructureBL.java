package com.sinosoft.lis.agentprint;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class InstitutionInfoStructureBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mManageCom;

    public InstitutionInfoStructureBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        /**得到从外部传来的数据，并备份到本类中*/
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();

        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        /**保存从外部传来的管理机构的信息，作为查询条件*/
        mManageCom = (String) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 1));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "InstitutionInfoStructrue";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String asql = "";
        String bsql = "";
        String csql = "";
        String dsql = "";
        String esql = "";
        String fsql = "";
        String aasql = "";

        String[] strArr = new String[6];
        ListTable tListTable = new ListTable();
        tListTable.setName("InstitutionInfoStructure");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        aasql = "select shortname from ldcom where comcode='" + mManageCom
                + "'";
        ExeSQL aaExeSQL = new ExeSQL();
        texttag.add("ManageCom", aaExeSQL.getOneValue(aasql));

        SSRS aSSRS = new SSRS();
        SSRS bSSRS = new SSRS();
        SSRS cSSRS = new SSRS();
        SSRS dSSRS = new SSRS();
        SSRS eSSRS = new SSRS();
        SSRS fSSRS = new SSRS();

        /**查询第一级机构，然后查询每个下级机构的下级机构 */
        asql = "select agentgroup,Name,BranchManagerName from labranchgroup where managecom like '"
               + mManageCom
               +
               "%' and branchlevel='1' and branchtype='3' and (state<>'1' or state is null)";
        ExeSQL aExeSQL = new ExeSQL();
        aSSRS = aExeSQL.execSQL(asql);
        for (int a = 1; a <= aSSRS.getMaxRow(); a++)
        {
            for (int b = 1; b <= aSSRS.getMaxCol(); b++)
            {
                if (b == 1)
                {
                    strArr[0] = aSSRS.GetText(a, 3);
                }
                if (b == 2)
                {
                    /** 查询第二级机构*/
                    bsql =
                            "select Agentgroup,Name,BranchManagerName from labranchgroup where Upbranch='"
                            + aSSRS.GetText(a, 1)
                            +
                            "' and BranchLevel='2' and (state<>'1' or state is null)";
                    ExeSQL bExeSQL = new ExeSQL();
                    bSSRS = bExeSQL.execSQL(bsql);
                    for (int c = 1; c <= bSSRS.getMaxRow(); c++)
                    {
                        for (int d = 1; d <= bSSRS.getMaxCol(); d++)
                        {
                            if (d == 1)
                            {
                                strArr[1] = bSSRS.GetText(c, 3);
                            }
                            if (d == 2)
                            {
                                /** 查询第三级机构*/
                                csql =
                                        "select AgentGroup,Name from labranchgroup where Upbranch='"
                                        + bSSRS.GetText(c, 1)
                                        +
                                        "'and BranchLevel='3' and (state<>'1' or state is null) and EndFlag='N'";
                                ExeSQL cExeSQL = new ExeSQL();
                                cSSRS = cExeSQL.execSQL(csql);
                                for (int e = 1; e <= cSSRS.getMaxRow(); e++)
                                {
                                    for (int f = 1; f <= cSSRS.getMaxCol();
                                                 f++)
                                    {
                                        if (f == 1)
                                        {
                                            strArr[2] = cSSRS.GetText(e, 2);
                                        }
                                        if (f == 2)
                                        {
                                            /** 查询第三级机构的组长姓名 */
                                            dsql =
                                                    "select Branchmanagername from labranchgroup where Agentgroup='"
                                                    + cSSRS.GetText(e, 1) + "'";
                                            ExeSQL dExeSQL = new ExeSQL();
                                            strArr[3] = dExeSQL.getOneValue(
                                                    dsql);
                                            /** 查询第三级机构的所辖组员人数 */
                                            esql =
                                                    "select count(*) from laagent where Agentgroup='"
                                                    + cSSRS.GetText(e, 1)
                                                    +
                                                    "' and AgentState in ('01','02')";
                                            ExeSQL eExeSQL = new ExeSQL();
                                            strArr[4] = eExeSQL.getOneValue(
                                                    esql);
                                            /** 查询第三级机构的所辖网点总数 */
                                            fsql = "select count(distinct(LAComToAgent.AgentCom)) from LAComToAgent,LACom where LAComToAgent.AgentCom=LACom.AgentCom and LAComToAgent.RelaType='0' and LAComToAgent.agentgroup='"
                                                    + cSSRS.GetText(e, 1)
                                                    +
                                                    "' and LACom.calFlag='Y' and LACom.ACType='01' and LACom.sellFlag='Y'";
                                            ExeSQL fExeSQL = new ExeSQL();
                                            strArr[5] = fExeSQL.getOneValue(
                                                    fsql);
                                            tListTable.add(strArr);
                                            strArr = new String[6];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        tListTable.add(strArr);
        /**将所有记录都保存到tListTable中之后，开始输出XML文件 */
        XmlExport xmlexport = new XmlExport(); //产生一个新的XmlExport对象
        /**产生vts文件 */
        xmlexport.createDocument("InstitutionInfoStructure.vts", "printer");
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        /**将tListTable的信息输入到xmlexport 中*/
        xmlexport.addListTable(tListTable, strArr);
        /**xmlexport将所得数据流入下面目录下的一XML文件中*/
        //     xmlexport.outputDocumentToFile("e:\\","test") ;
        /**将xmlexport添加到mResult中，以供以后调用*/
        mResult.addElement(xmlexport);
        System.out.println("end");
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        InstitutionInfoStructureBL tInstitutionInfoStructureBL = new
                InstitutionInfoStructureBL();
        String tManageCom = "8611";
        VData tVData = new VData();
        tVData.addElement(tManageCom);
        GlobalInput tGlobalInput = new GlobalInput();
        tVData.add(tGlobalInput);
        tInstitutionInfoStructureBL.submitData(tVData, "");
    }
}
