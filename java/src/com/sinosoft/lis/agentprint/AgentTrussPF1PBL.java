package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LATreeAccessoryDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LATreeAccessorySchema;
import com.sinosoft.lis.vschema.LATreeAccessorySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.F1PrintVTS;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;

public class AgentTrussPF1PBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private F1PrintVTS tF1PrintVTS;

    //取得的时间
    private String BranchAttr = null;
    private String strTemplatePath = null;

    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public AgentTrussPF1PBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        BranchAttr = ((String) cInputData.getObjectByObjectName("String", 0));
        strTemplatePath = ((String) cInputData.getObject(1));
        strTemplatePath = strTemplatePath + "AgentTrussDetail.vts";
        tF1PrintVTS = new F1PrintVTS(strTemplatePath, 11);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }


    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCPolBillF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        LAAgentDB tLAAgentDB1;
        LATreeAccessoryDB tLATreeAccessoryDB1;
        LATreeAccessorySet tLATreeAccessorySet1;
        LATreeAccessorySchema tLATreeAccessorySchema;
        SSRS tSSRS = new SSRS();
        msql = "select c.Name,a.Name,a.AgentCode,d.Codename,decode(b.AgentGrade,'A04','营业组主管','A05','营业组主管','A06','营业部主管','A07','营业部主管','A08','督导长','A09','区域督导长'),"
               + "a.EmployDate,c.branchattr,b.IntroAgency from LAAgent a,LATree b,LABranchGroup c,LDCode d,LDCode e "
               + "where a.agentstate <='02' and BranchAttr like '"
               + BranchAttr + "%' and BranchLevel='01'"
               + "and a.AgentCode = b.AgentCode and a.BranchCode=c.AgentGroup and d.Codetype='agentgrade' "
               + "and b.AgentGrade=d.Code and e.Codetype = 'agentstate' and a.AgentState=e.Code  "
               + "and (c.state<>'1' or c.state is null) order by BranchAttr";
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);
        String strArr[] = null;
        strArr = new String[11];
        strArr[0] = "营业组";
        strArr[1] = "姓名";
        strArr[2] = "代码";
        strArr[3] = "职级";
        strArr[4] = "主管属性";
        strArr[5] = "入司时间";
        strArr[6] = "代理人组代码";
        strArr[7] = "增员人姓名";
        strArr[8] = "组育成人姓名";
        strArr[9] = "部育成人姓名";
        strArr[10] = "督导长育成人姓名";
        tF1PrintVTS.addRow(strArr);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            strArr = new String[11];
            for (int j = 1; j <= tSSRS.getMaxCol(); j++)
            {
                strArr[j - 1] = tSSRS.GetText(i, j);
            }
            tLAAgentDB1 = new LAAgentDB();
            tLAAgentDB1.setAgentCode(strArr[7]);
            tLAAgentDB1.getInfo();
            strArr[7] = tLAAgentDB1.getName();
            tLATreeAccessoryDB1 = new LATreeAccessoryDB();
            String tsql = "select * from LATreeAccessory where (AgentGrade='A04' or AgentGrade='A05' or AgentGrade='A06' or AgentGrade='A07' or AgentGrade='A08' or AgentGrade='A09') and AgentCode=" +
                          strArr[2];
            tLATreeAccessorySet1 = tLATreeAccessoryDB1.executeQuery(tsql);
            for (int j = 1; j <= tLATreeAccessorySet1.size(); j++)
            {
                tLATreeAccessorySchema = (tLATreeAccessorySet1.get(j));
                if (StrTool.cTrim(tLATreeAccessorySchema.getAgentGrade()).
                    equals("A04") ||
                    StrTool.cTrim(tLATreeAccessorySchema.getAgentGrade()).
                    equals("A05"))
                {
                    tLAAgentDB1 = new LAAgentDB();
                    tLAAgentDB1.setAgentCode(tLATreeAccessorySchema.
                                             getRearAgentCode());
                    tLAAgentDB1.getInfo();
                    strArr[8] = tLAAgentDB1.getName();
                }
                else if (StrTool.cTrim(tLATreeAccessorySchema.getAgentGrade()).
                         equals("A06") ||
                         StrTool.cTrim(tLATreeAccessorySchema.getAgentGrade()).
                         equals("A07"))
                {
                    tLAAgentDB1 = new LAAgentDB();
                    tLAAgentDB1.setAgentCode(tLATreeAccessorySchema.
                                             getRearAgentCode());
                    tLAAgentDB1.getInfo();
                    strArr[9] = tLAAgentDB1.getName();
                }
                else if (StrTool.cTrim(tLATreeAccessorySchema.getAgentGrade()).
                         equals("A08") ||
                         StrTool.cTrim(tLATreeAccessorySchema.getAgentGrade()).
                         equals("A09"))
                {
                    tLAAgentDB1 = new LAAgentDB();
                    tLAAgentDB1.setAgentCode(tLATreeAccessorySchema.
                                             getRearAgentCode());
                    tLAAgentDB1.getInfo();
                    strArr[10] = tLAAgentDB1.getName();
                }
            }
            tF1PrintVTS.addRow(strArr);
        }
        mResult.addElement(tF1PrintVTS);
        System.out.println("end");
        return true;

    }

    public static void main(String[] args)
    {
        String BranchAttr = "8611";
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "8611";
        tG.ComCode = "8611";
        tG.Operator = "001";
        VData tVData = new VData();
        tVData.addElement(BranchAttr);
        tVData.addElement(tG);
        AgentTrussPF1PBL tAgentTrussPF1PUI = new AgentTrussPF1PBL();
        tAgentTrussPF1PUI.submitData(tVData, "PRINT");
    }
}