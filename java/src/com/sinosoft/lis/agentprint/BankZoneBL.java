package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class BankZoneBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private GlobalInput mGlobalInput = new GlobalInput();
    private String StartDay = "";
    private String EndDay = "";

    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";

    //业务处理相关变量

    /** 全局数据 */
    public BankZoneBL()
    {
    }

    /**
        传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        StartDay = (String) cInputData.get(0);
        EndDay = (String) cInputData.get(1);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 2));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "BankWeekBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String[] strArr = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("Bank");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDay", StartDay); //输入制表时间
        texttag.add("EndDay", EndDay); //输入制表时间
        strArr = new String[5];
        double[] data = null;
        data = new double[5];
        double[] dataall = null;
        dataall = new double[5];
        //统计银行
        String asql = "select name,agentcom from lacom where actype='01' and banktype='00' order by agentcom";

        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);

            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(asql);
            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                for (int b = 1; b <= aSSRS.getMaxCol(); b++)
                {
                    if (b == 1)
                    {
                        strArr[0] = aSSRS.GetText(a, 1); //银行名称
                    }
                    if (b == 2)
                    {
                        //地区名称省级
                        String fsql =
                                "select agentcom,name,ManageCom from LACom where ManageCom like '" +
                                mGlobalInput.ManageCom
                                +
                                "%' and ACType='01' and length(trim(agentcom))=6 and agentcom like '" +
                                aSSRS.GetText(a, 2)
                                + "%' order by agentcom";
                        SSRS tSSRS = new SSRS();
                        tSSRS = aExeSQL.execSQL(fsql);
                        for (int c = 1; c <= tSSRS.getMaxRow(); c++)
                        {
                            //地区名称8位统计
                            String bsql = "select distinct managecom from LACom where ACType='01' and length(trim(managecom))=8 and agentcom like '"
                                          + tSSRS.GetText(c, 1) +
                                          "%' order by managecom";
                            SSRS bSSRS = new SSRS();
                            bSSRS = aExeSQL.execSQL(bsql);
                            //取地区名称
                            for (int d = 1; d <= bSSRS.getMaxRow(); d++)
                            {
                                String hsql =
                                        "select trim(shortname) From ldcom where comcode='" +
                                        bSSRS.GetText(d, 1) + "'";
                                strArr[1] = aExeSQL.getOneValue(hsql); //地区名称
                                String dsql = "select distinct a.riskcode,riskname from lacommision a,lmriskapp b where a.riskcode=b.riskcode and"
                                              +
                                              " branchtype='3' and agentcom like '" +
                                              tSSRS.GetText(c, 1) +
                                              "%' and managecom='" +
                                              bSSRS.GetText(d, 1) +
                                              "' and tmakedate>='"
                                              + EndDay.substring(0, 4) +
                                              "-01-01' and tmakedate<='" +
                                              EndDay + "'";
                                SSRS cSSRS = new SSRS();
                                cSSRS = aExeSQL.execSQL(dsql);
                                for (int e = 1; e <= cSSRS.getMaxRow(); e++)
                                {
                                    strArr[2] = cSSRS.GetText(e, 2); //险种名称
                                    //期内保费
                                    String esql = "select nvl(sum(transmoney),0)/10000 from lacommision where branchtype='3' and tmakedate>='" +
                                                  StartDay
                                                  + "' and tmakedate<='" +
                                                  EndDay + "' and managecom='" +
                                                  bSSRS.GetText(d, 1) +
                                                  "' and agentcom like '" +
                                                  tSSRS.GetText(c, 1)
                                                  + "%' and riskcode='" +
                                                  cSSRS.GetText(e, 1) + "'";
                                    strArr[3] = aExeSQL.getOneValue(esql);
                                    data[3] += Double.parseDouble(strArr[3]);
                                    //累计保费
                                    String gsql = "select sum(transmoney)/10000 from lacommision where branchtype='3' and tmakedate>='"
                                                  + EndDay.substring(0, 4) +
                                                  "-01-01' and tmakedate<='" +
                                                  EndDay +
                                                  "' and agentcom like '" +
                                                  tSSRS.GetText(c, 1)
                                                  + "%' and managecom='" +
                                                  bSSRS.GetText(d, 1) +
                                                  "' and riskcode='" +
                                                  cSSRS.GetText(e, 1) + "'";
                                    strArr[4] = aExeSQL.getOneValue(gsql);
                                    data[4] += Double.parseDouble(strArr[4]);
                                    tlistTable.add(strArr);
                                    strArr = new String[5];
                                }
                            }
                            strArr[1] = tSSRS.GetText(c, 2) + "小计";
                            strArr[3] = String.valueOf(data[3]);
                            strArr[4] = String.valueOf(data[4]);
                            dataall[3] += Double.parseDouble(strArr[3]);
                            dataall[4] += Double.parseDouble(strArr[4]);
                            tlistTable.add(strArr);
                            strArr = new String[5];
                            data = new double[5];
                        }
                        if (tSSRS.getMaxRow() != 0)
                        {
                            strArr[0] = aSSRS.GetText(a, 1) + "总计";
                            strArr[3] = String.valueOf(dataall[3]);
                            strArr[4] = String.valueOf(dataall[4]);
                            tlistTable.add(strArr);
                            strArr = new String[5];
                            dataall = new double[5];
                        }
                    }
                }
            }
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("BankZone.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
//      xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
            mResult.addElement(xmlexport);

            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankAllStatBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    public static void main(String[] args)
    {
    }
}