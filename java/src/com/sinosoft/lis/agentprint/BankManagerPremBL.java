package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class BankManagerPremBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间,险种
    private GlobalInput mGlobalInput = new GlobalInput();
    private String StartDay = "";
    private String EndDay = "";
    private String RiskCode = "";

    //全局变量
    private String strArr[] = null;
    private double data[] = null;
    private double data1[] = null;
    private ListTable tlistTable = new ListTable();
    //业务处理相关变量
    /** 全局数据 */
    public BankManagerPremBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        StartDay = (String) cInputData.get(0);
        EndDay = (String) cInputData.get(1);
        RiskCode = (String) cInputData.get(2);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 3));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "DepartOrderBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String strArr[] = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("BankManagerPrem");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDay", StartDay + "---" + EndDay); //输入制表时间
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);
            String aasql =
                    "select trim(riskname) from lmriskapp where riskcode='" +
                    RiskCode + "'";
            texttag.add("RiskCode", aExeSQL.getOneValue(aasql)); //险种名称
            strArr = new String[9];
            double data[] = null;
            data = new double[9];
            double data1[] = null;
            data1 = new double[9];
            //经理姓名
            String asql = "select name,agentcode from laagent where InsideFlag='1' and AgentState in ('01','02') and branchtype='3' and managecom like '" +
                          mGlobalInput.ManageCom + "%'";
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(asql);
            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                for (int b = 1; b <= aSSRS.getMaxCol(); b++)
                {
                    if (b == 1)
                    {
                        strArr[0] = aSSRS.GetText(a, 1);
                    }
                    if (b == 2)
                    {
                        if (RiskCode.equals("") || RiskCode == null)
                        {
                            //预收保费
                            String bsql =
                                    "select nvl(sum(prem),0) from lcpol where makeDate>='" +
                                    StartDay + "' and makeDate<='" + EndDay +
                                    "' and uwflag<>'a' and AgentCode = '" +
                                    aSSRS.GetText(a, 2) + "'";
                            strArr[1] = aExeSQL.getOneValue(bsql);
                            data[1] += Double.parseDouble(strArr[1]);
                            //活动网点数
                            String csql =
                                    "select count(distinct agentcom) from lcpol where  makeDate>='" +
                                    StartDay + "' and makeDate<='" + EndDay +
                                    "' and uwflag<>'a' and AgentCode='" +
                                    aSSRS.GetText(a, 2) + "'";
                            strArr[2] = aExeSQL.getOneValue(csql);
                            data[2] += Double.parseDouble(strArr[2]);
                            //保单件数
                            String dsql =
                                    "select count(distinct polno) from lcpol where makeDate>='" +
                                    StartDay + "' and makeDate<='" + EndDay +
                                    "'   and uwflag<>'a' and AgentCode = '" +
                                    aSSRS.GetText(a, 2) + "'";
                            strArr[3] = aExeSQL.getOneValue(dsql);
                            data[3] += Double.parseDouble(strArr[3]);
                            //实收保费
                            String esql =
                                    "select nvl(sum(transmoney),0) from lacommision where  tmakeDate>='" +
                                    StartDay + "' and tmakeDate<='" + EndDay +
                                    "' and AgentCode = '" + aSSRS.GetText(a, 2) +
                                    "'";
                            strArr[4] = aExeSQL.getOneValue(esql);
                            data[4] += Double.parseDouble(strArr[4]);
                            //活动网点数
                            String fsql =
                                    "select count(distinct agentcom) from lacommision where tmakeDate>='" +
                                    StartDay + "' and tmakeDate<='" + EndDay +
                                    "'  and AgentCode='" + aSSRS.GetText(a, 2) +
                                    "'";
                            strArr[5] = aExeSQL.getOneValue(fsql);
                            data[5] += Double.parseDouble(strArr[5]);
                            //保单件数
                            String gsql =
                                    "select nvl(sum(calcount),0) from lacommision where tmakeDate>='" +
                                    StartDay + "' and tmakeDate<='" + EndDay +
                                    "' and AgentCode = '" + aSSRS.GetText(a, 2) +
                                    "'";
                            strArr[6] = aExeSQL.getOneValue(gsql);
                            data[6] += Double.parseDouble(strArr[6]);
                            //撤单件数
                            String hsql =
                                    "select nvl(sum(calcount),0) from lacommision where tmakeDate>='" +
                                    StartDay + "' and tmakeDate<='" + EndDay +
                                    "' and transtype='WT' and AgentCode = '" +
                                    aSSRS.GetText(a, 2) + "'";
                            strArr[7] = aExeSQL.getOneValue(hsql);
                            data[7] += Double.parseDouble(strArr[7]);
                            //件数小计
                            if (strArr[6].equals("0"))
                            {
                                strArr[8] = "0";
                            }
                            else
                            {
                                strArr[8] = String.valueOf(Double.parseDouble(
                                        strArr[6]) -
                                        Double.parseDouble(strArr[7]));
                            }
                            tlistTable.add(strArr);
                            strArr = new String[9];
                        }
                        else
                        {
                            //预收保费
                            String isql =
                                    "select nvl(sum(prem),0) from lcpol where makeDate>='" +
                                    StartDay + "' and makeDate<='" + EndDay +
                                    "'  and uwflag<>'a' and AgentCode = '" +
                                    aSSRS.GetText(a, 2) + "' and riskcode='" +
                                    RiskCode + "'";
                            strArr[1] = aExeSQL.getOneValue(isql);
                            data[1] += Double.parseDouble(strArr[1]);
                            //活动网点数
                            String jsql =
                                    "select count(distinct agentcom) from lcpol where  makeDate>='" +
                                    StartDay + "' and makeDate<='" + EndDay +
                                    "'   and uwflag<>'a' and AgentCode='" +
                                    aSSRS.GetText(a, 2) + "' and riskcode='" +
                                    RiskCode + "'";
                            strArr[2] = aExeSQL.getOneValue(jsql);
                            data[2] += Double.parseDouble(strArr[2]);
                            //保单件数
                            String ksql =
                                    "select count(distinct polno) from lcpol where makeDate>='" +
                                    StartDay + "' and makeDate<='" + EndDay +
                                    "'   and uwflag<>'a' and AgentCode = '" +
                                    aSSRS.GetText(a, 2) + "' and riskcode='" +
                                    RiskCode + "'";
                            strArr[3] = aExeSQL.getOneValue(ksql);
                            data[3] += Double.parseDouble(strArr[3]);
                            //实收保费
                            String lsql =
                                    "select nvl(sum(transmoney),0) from lacommision where  tmakeDate>='" +
                                    StartDay + "' and tmakeDate<='" + EndDay +
                                    "' and AgentCode = '" + aSSRS.GetText(a, 2) +
                                    "' and riskcode='" + RiskCode + "'";
                            strArr[4] = aExeSQL.getOneValue(lsql);
                            data[4] += Double.parseDouble(strArr[4]);
                            //活动网点数
                            String msql =
                                    "select count(distinct agentcom) from lacommision where tmakeDate>='" +
                                    StartDay + "' and tmakeDate<='" + EndDay +
                                    "'  and AgentCode='" + aSSRS.GetText(a, 2) +
                                    "'and riskcode='" + RiskCode + "'";
                            strArr[5] = aExeSQL.getOneValue(msql);
                            data[5] += Double.parseDouble(strArr[5]);
                            //保单件数
                            String nsql =
                                    "select nvl(sum(calcount),0) from lacommision where tmakeDate>='" +
                                    StartDay + "' and tmakeDate<='" + EndDay +
                                    "' and AgentCode = '" + aSSRS.GetText(a, 2) +
                                    "' and riskcode='" + RiskCode + "'";
                            strArr[6] = aExeSQL.getOneValue(nsql);
                            data[6] += Double.parseDouble(strArr[6]);
                            //撤单件数
                            String osql =
                                    "select nvl(sum(calcount),0) from lacommision where tmakeDate>='" +
                                    StartDay + "' and tmakeDate<='" + EndDay +
                                    "'  and transtype='WT' and AgentCode = '" +
                                    aSSRS.GetText(a, 2) + "' and riskcode='" +
                                    RiskCode + "'";
                            strArr[7] = aExeSQL.getOneValue(osql);
                            data[7] += Double.parseDouble(strArr[7]);
                            //件数小计
                            if (strArr[6].equals("0"))
                            {
                                strArr[8] = "0";
                            }
                            else
                            {
                                strArr[8] = String.valueOf(Double.parseDouble(
                                        strArr[6]) -
                                        Double.parseDouble(strArr[7]));
                            }
                            tlistTable.add(strArr);
                            strArr = new String[9];
                        }
                    }
                }
            }

//汇总
            strArr = new String[9];
            strArr[0] = "汇总";
            data1 = new double[9];
            for (int i = 1; i <= 8; i++)
            {
                strArr[i] = String.valueOf(data[i]);
                data1[i] += data[i];
            }
            tlistTable.add(strArr);

            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("BankManagerPrem.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankManagerPremBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    /*
       如果一个字符串数字中小数点后有值，则去掉小数点后面数值
     */
    public static String getInt(String Value)
    {
        String result = "";
        boolean mflag = true;
        int m = 0;
        m = Value.lastIndexOf(".");
        if (m == -1)
        {
            result = Value;
        }
        else
        {
            result = Value.substring(0, m);
        }
        return result;
    }

    public static void main(String[] args)
    {
    }
}