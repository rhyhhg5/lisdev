/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */

package com.sinosoft.lis.agentprint;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;


public class AgentInfoPrintBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();

    /**取得查询条件中管理机构的值*/
    private String mManageCom;
    public AgentInfoPrintBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        /**得到从外部传来的数据，并备份到本类中*/
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        /**保存从外部传来的管理机构的信息，作为查询条件*/
        mManageCom = (String) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    private boolean queryData()
    {
        String strArr[] = null;
        ListTable tListTable = new ListTable();
        tListTable.setName("AGENT");
        SSRS tSSRS = new SSRS();

        /**书写SQL语句，这一个语句只能查出符合条件得前12个字段得值*/
        String SQL = "select getUniteCode(LAAgent.AgentCode),LAAgent.Name,b.codename,LAAgent.IDNo,LAAgent.employdate,c.codename,"
                     + "LAAgent.Mobile,e.name,a.codename,d.codename,LAAgent.channelname,(select count(*) from LAComToAgent where RelaType='1' and AgentCode=LAAgent.AgentCode ) "
                     + "from LAAgent,latree,labranchgroup,ldcode a,ldcode b,ldcode c,ldcode d,labranchgroup e "
                     + "where  a.codetype='agentkind' and a.code = LAAgent.agentkind and LAAgent.agentgroup = labranchgroup.agentgroup "
                     + "and b.code = LAAgent.Sex and b.codetype='sex' and c.code = LAAgent.Degree and c.codetype='degree' "
                     + "and d.code = latree.agentgrade and d.codetype='agentgrade' and e.branchattr=labranchgroup.branchattr "
                     + "and LAAgent.agentcode = latree.agentcode and LAAgent.AgentState in ('01','02') and LAAgent.branchtype='3' "
                     + "and LAAgent.ManageCom like '" + mManageCom +
                     "%' order by LAAgent.AgentCode";
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(SQL);
        int i = 1;
        for (int a = 1; a <= tSSRS.getMaxRow(); a++)
        {
            strArr = new String[13];
            strArr[0] = String.valueOf(i);
            /**将所得到得符合条件得记录逐条加入到strArr中相应得位置中*/
            for (int b = 1; b <= tSSRS.getMaxCol(); b++)
            {
                strArr[b] = tSSRS.GetText(a, b);
            }
            i++;
            tListTable.add(strArr);
        }

//    String asql ="select rownum,LAAgent.AgentCode,LAAgent.Name,LAAgent.Sex,LAAgent.IDNo,LAAgent.employdate,LAAgent.Degree,LAAgent.Mobile,labranchgroup.branchattr,ldcode.codename,latree.agentgrade,LAAgent.channelname from LAAgent,latree,labranchgroup,ldcode where  ldcode.codetype='agentkind' and ldcode.code = LAAgent.agentkind and LAAgent.agentgroup = labranchgroup.agentgroup and LAAgent.agentcode = latree.agentcode and LAAgent.ManageCom='"+mManageCom+"'  and LAAgent.AgentState in ('01','02') and LAAgent.branchtype='3'";
//    ExeSQL aExeSQL =new ExeSQL();
//    aSSRS=aExeSQL.execSQL(asql);
//    if (!(aSSRS.getMaxRow()==0))
//    for (int a=1;a<=aSSRS.getMaxRow();a++)
//    {
//      strArr=new String[13];
//      /**将所得到得符合条件得记录逐条加入到strArr中相应得位置中*/
//       for (int b=1;b<=aSSRS.getMaxCol();b++)
//       {
//         /**容错处理：加入得到的记录某个字段为空，则strArr相应的位置为空*/
//         if ((aSSRS.GetText(a,b)==null) ||(aSSRS.GetText(a,b).equals("")))
//           strArr[b-1]="";
//           else strArr[b-1]=aSSRS.GetText(a,b) ;
//         }
//       SSRS bSSRS =new SSRS();
//       String bsql="";
//       ExeSQL bExeSQL=new ExeSQL();
//       /**在ldcode表中，根据sex字段的code值查出Codename并赋值到strArr[3]字段中，覆盖code*/
//       if (!((strArr[3]==null)||(strArr[3].equals(""))))
//           {
//             bsql="select Codename from ldcode where CodeType='sex' and Code='"+strArr[3]+"'";
//             bSSRS=bExeSQL.execSQL(bsql);
//             if (!(bSSRS.getMaxRow()==0))
//             strArr[3]=bSSRS.GetText(1,1);
//             else strArr[3]="";
//           };
//       /**在ldcode表中，根据degree字段的code值查出Codename并赋值到strArr[6]字段中，覆盖code*/
//       if (!((strArr[6]==null)||(strArr[6].equals(""))))
//         {
//           bsql = "select Codename from ldcode where CodeType='degree' and Code='" +
//               strArr[6] + "'";
//           bSSRS = bExeSQL.execSQL(bsql);
//           if (!(bSSRS.getMaxRow()==0))
//           strArr[6] = bSSRS.GetText(1, 1);
//           else strArr[6]="";
//         }
//
//
//
//         if (!((strArr[10]==null)||(strArr[10].equals(""))))
//         {
//           bsql = "select Codename from ldcode where CodeType='agentgrade' and Code='" + strArr[10] + "'";
//           bSSRS = bExeSQL.execSQL(bsql);
//           if (!(bSSRS.getMaxRow()==0))
//             strArr[10] = bSSRS.GetText(1, 1);
//           else strArr[10]="";
//         }
//
//
//         if (!((strArr[8]==null)||(strArr[8].equals(""))))
//         {
//           bsql = "select name from labranchgroup where branchattr='" + strArr[8] + "'";
//           bSSRS = bExeSQL.execSQL(bsql);
//           if (!(bSSRS.getMaxRow()==0))
//             strArr[8] = bSSRS.GetText(1, 1);
//           else strArr[8]="";
//         }
//
//
//       /**在LAComToAgent表中，查询管理网点数  */
//       bsql="select count(*) from LAComToAgent where RelaType='1' and agentcode='"+strArr[1]+"'";
//       bSSRS=bExeSQL.execSQL(bsql);
//       strArr[12]=bSSRS.GetText(1,1);
//       tListTable.add(strArr);
//    }
        /**将所有记录都保存到tListTable中之后，开始输出XML文件 */
        XmlExport xmlexport = new XmlExport(); //产生一个新的XmlExport对象
        /**产生vts文件 */
        xmlexport.createDocument("AgentInfoPrint.vts", "printer");
        /**将tListTable的信息输入到xmlexport 中*/
        xmlexport.addListTable(tListTable, strArr);
        /**xmlexport将所得数据流入下面目录下的一XML文件中*/
//    xmlexport.outputDocumentToFile("e:\\","test") ;
        /**将xmlexport添加到mResult中，以供以后调用*/
        mResult.addElement(xmlexport);
        System.out.println("end");
        return true;
    }

    /**为外部构造一个返回mResult的接口*/
    public VData getResult()
    {
        return mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "AgentInfoPrintBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
        AgentInfoPrintBL tAgentInfoPrintBL = new AgentInfoPrintBL();
        VData tVData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        tVData.add(tGlobalInput);
        tVData.add(tLAAgentSchema);
        tAgentInfoPrintBL.submitData(tVData, "");
    }


}
