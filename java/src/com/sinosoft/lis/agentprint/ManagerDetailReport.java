package com.sinosoft.lis.agentprint;

import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class ManagerDetailReport
{
    public GlobalInput mGI = new GlobalInput();
    public String mStartDate = "";
    public String mEndDate = "";
    public VData mResult = new VData();
    public CErrors mErrors = new CErrors();

    public ManagerDetailReport()
    {
    }

    public static void main(String[] args)
    {
        ManagerDetailReport managerDetailReport1 = new ManagerDetailReport();
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86110000";
        tGI.ManageCom = "86";
        tGI.Operator = "aa";
        VData tInputData = new VData();
        tInputData.clear();
        tInputData.addElement(tGI);
        tInputData.add(0, "2003-12-8");
        tInputData.add(1, "2003-12-8");

        if (!managerDetailReport1.getInputData(tInputData))
        {
            System.out.println("-----getinputdata failed!------");
        }
        if (!managerDetailReport1.prepareData())
        {
            System.out.println("-------prepareData failed!------");
        }

    }

    public boolean getInputData(VData cInputData)
    {
        //全局变量
        try
        {
            mStartDate = (String) cInputData.getObjectByObjectName("String", 0);
            mEndDate = (String) cInputData.getObjectByObjectName("String", 1);
            mGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput",
                    0);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("getInputData", ex.toString());
            return false;
        }
        System.out.println("---StartDate : " + this.mStartDate);
        System.out.println("---EndDate : " + this.mEndDate);
        return true;
    }

    public boolean prepareData()
    {
        ListTable tListTable = new ListTable();
        tListTable.setName("ManagerDetail");

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        String tStaticDate = "";
        tStaticDate = this.mStartDate.trim() + " 至 " + this.mEndDate.trim();
        texttag.add("StaticDate", tStaticDate); //输入制表时间

        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);

            //查询渠道经理
            String Sql1 =
                    "select agentcode,name,getbranchattr(agentgroup) from laagent "
                    + " where branchtype='3' "
                    + " and managecom like '" + this.mGI.ManageCom.trim() +
                    "%'";
            int tAgentCount = 0;
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(Sql1);
            tAgentCount = aSSRS.getMaxRow();
            if (tAgentCount == 0)
            {
                dealError("prepareData", "没有代理人信息！");
                return false;
            }
            String strArr[] = null;
            for (int i = 1; i <= tAgentCount; i++)
            {
                SSRS bSSRS = new SSRS();
                String tAgentCode = aSSRS.GetText(i, 1);
                String tAgentName = aSSRS.GetText(i, 2);

                String Sql2 =
                        "select b.name,b.channeltype,c.riskname,a.p11,a.polno,"
                        +
                        "a.transmoney,a.signdate,a.cvalidate,a.getpoldate,a.tmakedate,"
                        + "a.fyc,a.p15 from lacommision a,lacom b,lmriskapp c "
                        + "where a.agentcom = b.agentcom and a.riskcode=c.riskcode and trim(a.agentcode) = '"
                        + tAgentCode.trim() + "' and getpoldate >= '"
                        + this.mStartDate.trim() + "' and getpoldate <= '"
                        + this.mEndDate.trim() + "'";
                bSSRS = aExeSQL.execSQL(Sql2);
                int tNetCount = 0;
                tNetCount = bSSRS.getMaxRow();
                if (tNetCount == 0)
                {
                    continue;
                }

                for (int j = 1; j <= tNetCount; j++)
                {
                    strArr = new String[15];
                    if (j == 1)
                    {
                        strArr[0] = tAgentName;
                    }

                    strArr[1] = bSSRS.GetText(j, 1);
                    strArr[2] = bSSRS.GetText(j, 2);
                    strArr[3] = bSSRS.GetText(j, 3); //险种名称
                    strArr[5] = bSSRS.GetText(j, 4);
                    strArr[6] = bSSRS.GetText(j, 5);
                    //投保单号
                    strArr[4] = bSSRS.GetText(j, 12);
                    strArr[7] = bSSRS.GetText(j, 6).trim();
                    strArr[8] = bSSRS.GetText(j, 7).trim();
                    strArr[9] = bSSRS.GetText(j, 8).trim();
                    strArr[10] = bSSRS.GetText(j, 9).trim();
                    //判断是否置撤单日期
                    if (Float.parseFloat(strArr[7].trim()) < 0)
                    {
                        strArr[11] = bSSRS.GetText(j, 10).trim();
                    }
                    strArr[12] = bSSRS.GetText(j, 11).trim();

                    if (j == 1)
                    {
                        //判断是否显示组绩效、部绩效
                        Sql2 =
                                "select agentkind from laagent where trim(agentcode) = '"
                                + tAgentCode.trim() + "'";
                        String tAgentKind = "";
                        tAgentKind = aExeSQL.getOneValue(Sql2);
                        if (tAgentKind.trim().equals("") || tAgentKind == null)
                        {
                            dealError("prepareData",
                                      "代理人表中没有" + tAgentCode.trim() + "的信息！");
                            return false;
                        }

                        if (tAgentKind.trim().compareToIgnoreCase("03") <= 0)
                        {
                            String tBranchAttr = aSSRS.GetText(i, 3);
                            Sql2 =
                                    "select nvl(sum(grpfyc),0) from lacommision where getpoldate >= '"
                                    + this.mStartDate.trim() +
                                    "' and getpoldate <= '"
                                    + this.mEndDate.trim() + "'"
                                    + " and branchattr like '" +
                                    tBranchAttr.trim() + "%'";
                            strArr[13] = aExeSQL.getOneValue(Sql2).trim();

                            if (tAgentKind.trim().compareToIgnoreCase("02") <=
                                0)
                            {
                                Sql2 =
                                        "select nvl(sum(DepFyc),0) from lacommision where getpoldate >= '"
                                        + this.mStartDate.trim() +
                                        "' and getpoldate <= '"
                                        + this.mEndDate.trim() + "'"
                                        + " and managecom like '" +
                                        this.mGI.ManageCom.trim() + "%'";
                                strArr[14] = aExeSQL.getOneValue(Sql2).trim();
                            }
                        }
                    }
                    tListTable.add(strArr);
                }
            }

            XmlExport xmlexport = new XmlExport();
            xmlexport.createDocument("ManagerDetailReport.vts", "printer");
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tListTable, strArr);
//   xmlexport.outputDocumentToFile("e:\\","test");
            this.mResult.add(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ManagerDetailReport";
            tError.functionName = "preparedata";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("------prepareData end-----");

        return true;
    }

    private void dealError(String tFuncName, String tErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "ChannelBusiReport";
        cError.functionName = tFuncName;
        cError.errorMessage = tErrMsg;
        this.mErrors.addOneError(cError);
    }

    public VData getResult()
    {
        return this.mResult;
    }
}