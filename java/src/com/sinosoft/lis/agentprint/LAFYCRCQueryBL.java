package com.sinosoft.lis.agentprint;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class LAFYCRCQueryBL {
	/** 错误信息容器 */
	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = null;

	private String mManageCom = null;

	private String mOutXmlPath = null;

	private String mUnity = "";

	private String mStartDate = "";

	private String mEndDate = "";

	private String mBranchAttr = "";

	public LAFYCRCQueryBL() {
	}

	public boolean submitData(VData cInputData, String operate) {

		if (!getInputData(cInputData)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		return true;
	}

	/**
	 * 校验操作是否合法
	 * 
	 * @return boolean
	 */
	private boolean checkData() {

		return true;
	}

	private String getSQL() {
		String tSQL = "select '0','1','2','3','4','5','6','7','8' from dual where 1=1";
		String[] sqlArray = new String[3];
		if (null!=mUnity && !"".equals(mUnity) && "01".equals(mUnity)) {
			sqlArray[0] = "	   substr(temp.managecom,1, 4),(select name from ldcom where comcode = substr(temp.managecom, 1, 4)),";
			sqlArray[1] = " substr(temp.managecom, 1, 4)";
		} else if (null!=mUnity && !"".equals(mUnity) && "02".equals(mUnity)) {
			sqlArray[0] = "	  temp.managecom,(select name from ldcom where comcode = temp.managecom),";
			sqlArray[1] = " temp.managecom";
		} else if (null!=mUnity && !"".equals(mUnity) && "03".equals(mUnity)) {
			sqlArray[0] = "'" + mManageCom
					+ "',(select name from ldcom where comcode ='" + mManageCom
					+ "'),";
			sqlArray[0] += " (select branchattr from labranchgroup where branchtype = '1' and branchtype2 = '01' and agentgroup = substr(temp.branchseries, 1, 12)), (select name from labranchgroup where branchtype = '1' and branchtype2 = '01' and agentgroup = substr(temp.branchseries, 1, 12)),";
			sqlArray[1] = " substr(temp.branchseries,1,12)";
		} else if (null!=mUnity && !"".equals(mUnity) && "04".equals(mUnity)) {
			sqlArray[0] = "'" + mManageCom
					+ "',(select name from ldcom where comcode ='" + mManageCom
					+ "'),";
			sqlArray[0] += " (select branchattr from labranchgroup where branchtype = '1' and branchtype2 = '01' and agentgroup = substr(temp.branchseries, 14, 12)), (select name from labranchgroup where branchtype = '1' and branchtype2 = '01' and agentgroup = substr(temp.branchseries, 14, 12)),";
			sqlArray[1] = " substr(temp.branchseries,14,12)";
		} else if (null!=mUnity && !"".equals(mUnity) && "05".equals(mUnity)) {
			sqlArray[0] = "'" + mManageCom
					+ "',(select name from ldcom where comcode ='" + mManageCom
					+ "'),";
			sqlArray[0] += " (select branchattr from labranchgroup where branchtype = '1' and branchtype2 = '01' and agentgroup = substr(temp.branchseries, 27, 12)), (select name from labranchgroup where branchtype = '1' and branchtype2 = '01' and agentgroup = substr(temp.branchseries, 27, 12)),";
			sqlArray[1] = " substr(temp.branchseries,27,12)";
		} else if (null!=mUnity && !"".equals(mUnity) && "06".equals(mUnity)) {
			sqlArray[0] = "'" + mManageCom
					+ "',(select name from ldcom where comcode ='" + mManageCom
					+ "'),";
			sqlArray[0] += "'"
					+ mBranchAttr
					+ "',"
					+ " (select name from labranchgroup where branchtype = '1' and branchtype2 = '01' and branchattr='"
					+ mBranchAttr + "'),";
			sqlArray[0] += " temp.agentcode,(select name from laagent where agentcode = temp.agentcode), ";
			sqlArray[1] = " temp.agentcode";
		}
		
		
		
		tSQL="select "+
		sqlArray[0]+
		"       sum(case"+
		"             when temp.renewcount = 0 then"+
		"              temp.fyc"+
		"             else"+
		"              0"+
		"           end),"+
		"       sum(case"+
		"             when temp.renewcount > 0 then"+
		"              temp.fyc"+
		"             else"+
		"              0"+
		"           end),"+
		"       sum(fyc)"+
		"  from (select managecom, branchattr,branchseries,agentcode, fyc, renewcount"+
		"          from lacommision"+
		"         where managecom like '"+mManageCom+"%'"+
		"           and branchtype = '1'"+
		"           and (branchtype2 = '01' or"+
		"               (branchtype2 = '03' and f3 is not null))"+
		"           and CommDire = '1' ";
		if(mUnity.compareTo("04")!=-1&& mBranchAttr!=null && mBranchAttr!=""){
			tSQL = tSQL  + " and branchattr like '" + mBranchAttr+"%'";
		}
		tSQL =tSQL+
		"           and (RENEWCOUNT>=1 or (payyear = 0 and RENEWCOUNT = 0))"+
		"           and tmakedate >= '"+mStartDate+"'"+
		"           and tmakedate <= '"+mEndDate+"') as temp"+
		" where 1 = 1"+
		" group by "+
		sqlArray[1]
		; 
		return tSQL;
	}

	/**
	 * dealData 处理业务数据
	 * 
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean dealData() {
		ExeSQL tExeSQL = new ExeSQL();
		System.out.println("BL->dealDate()");
		System.out.println(mOutXmlPath);
		String tSQL = this.getSQL();
		SSRS tSSRS = tExeSQL.execSQL(tSQL);

		if (tExeSQL.mErrors.needDealError()) {
			System.out.println(tExeSQL.mErrors.getErrContent());

			CError tError = new CError();
			tError.moduleName = "LAFYCRCQueryBL";
			tError.functionName = "dealData";
			tError.errorMessage = "没有查询到需要下载的数据";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		String[][] mToExcel = new String[tSSRS.getMaxRow() + 3][20];
		mToExcel[0][0] = "FYC(首年度佣金)和续保佣金报表：统计时间为"+mStartDate+"~"+mEndDate;
		mToExcel[1][0] = "此报表显示的是非审核发放的即时动态数据。";
		if (null!=mUnity && !"".equals(mUnity) && ("01".equals(mUnity)||"02".equals(mUnity))) {
			mToExcel[2][0] = "管理机构";
			mToExcel[2][1] = "管理机构名称";
			mToExcel[2][2] = "FYC(首年度佣金)";
			mToExcel[2][3] = "续保佣金";
			mToExcel[2][4] = "合计";
		}else if(null!=mUnity && !"".equals(mUnity) && ("03".equals(mUnity)||"04".equals(mUnity)||"05".equals(mUnity))){
			mToExcel[2][0] = "管理机构";
			mToExcel[2][1] = "管理机构名称";
			mToExcel[2][2] = "销售团队代码";
			mToExcel[2][3] = "销售团队代码名称";
			mToExcel[2][4] = "FYC(首年度佣金)";
			mToExcel[2][5] = "续保佣金";
			mToExcel[2][6] = "合计";
		}else if(null!=mUnity && !"".equals(mUnity) && "06".equals(mUnity)){
			mToExcel[2][0] = "管理机构";
			mToExcel[2][1] = "管理机构名称";
			mToExcel[2][2] = "销售团队代码";
			mToExcel[2][3] = "销售团队代码名称";
			mToExcel[2][4] = "营销员编码";
			mToExcel[2][5] = "营销员姓名";
			mToExcel[2][6] = "FYC(首年度佣金)";
			mToExcel[2][7] = "续保佣金";
			mToExcel[2][8] = "合计";
		}

		for (int row = 1; row <= tSSRS.getMaxRow(); row++) {
			for (int col = 1; col <= tSSRS.getMaxCol(); col++) {
				mToExcel[row+2][col - 1] = tSSRS.GetText(row, col);
				System.out.println(mToExcel[row+2][col - 1]);
			}
		}

		try {
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = { PubFun.getCurrentDate() };
			t.addSheet(sheetName);
			t.setData(0, mToExcel);
			t.write(mOutXmlPath);
		} catch (Exception ex) {
			ex.toString();
			ex.printStackTrace();
		}

		return true;
	}

	/**
	 * getInputData 将外部传入的数据分解到本类的属性中
	 * 
	 * @param cInputData
	 *            VData：submitData中传入的VData对象
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean getInputData(VData data) {
		mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		TransferData tf = (TransferData) data.getObjectByObjectName(
				"TransferData", 0);

		if (mGI == null || tf == null) {
			CError tError = new CError();
			tError.moduleName = "LAFYCRCQueryBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}

		mUnity = (String) tf.getValueByName("Unity");
		mManageCom = (String) tf.getValueByName("ManageCom");
		mStartDate = (String) tf.getValueByName("StartDate");
		mEndDate = (String) tf.getValueByName("EndDate");
		mBranchAttr = (String) tf.getValueByName("BranchAttr");
		mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
		if (mUnity == null || mOutXmlPath == null || mManageCom == null
				|| mStartDate == null || mEndDate == null) {
			CError tError = new CError();
			tError.moduleName = "LAFYCRCQueryBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整2";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}

		return true;
	}
}
