package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class AgentPeopleMonthBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private String StartMonth = "";
    private String EndMonth = "";
    private String ManageCom = "";

    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    private String currentYear = PubFun.getCurrentDate().substring(0, 4);

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    public AgentPeopleMonthBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!queryDataOther())
        {
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        ManageCom = (String) cInputData.get(0);
        StartMonth = (String) cInputData.get(1);
        EndMonth = (String) cInputData.get(2);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "AgentPeopleMonthBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryDataOther()
    {
        String strArr[] = null;
//    double data[]= new double[22];//总公司
        ListTable tlistTable = new ListTable();
        tlistTable.setName("Month");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        String currentYear = PubFun.getCurrentDate().substring(0, 4);
        String smDay[] = PubFun.calFLDate(StartMonth.substring(0, 4) + "-" +
                                          StartMonth.substring(4, 6) + "-01");
        String emDay[] = PubFun.calFLDate(EndMonth.substring(0, 4) + "-" +
                                          EndMonth.substring(4, 6) + "-01");
        texttag.add("StartMonth",
                    StartMonth.substring(0, 4) + "年" +
                    StartMonth.substring(4, 6) + "月"); //输入制表时间
        texttag.add("EndMonth",
                    EndMonth.substring(0, 4) + "年" + EndMonth.substring(4, 6) +
                    "月"); //输入制表时间
        //查询
        strArr = new String[22];
        //营业单位
        String asql =
                "select comcode from ldcom where length(trim(comcode))=length(trim(" +
                ManageCom + "))+2 or comcode=" + ManageCom + " union select branchattr From labranchgroup where branchlevel='03' and managecom=" +
                ManageCom + " order by comcode";

        ExeSQL aExeSQL = new ExeSQL();
        SSRS aSSRS = new SSRS();
        aSSRS = aExeSQL.execSQL(asql);
        for (int a = 1; a <= aSSRS.getMaxRow(); a++)
        {
            //营业单位
            strArr[0] = aSSRS.GetText(1, a);
            //合计2
            String bsql =
                    "select nvl(count(*),0) from labranchgroup b,laagent a where "
                    + "OutWorkDate<='" + emDay[1] + "' and OutWorkDate>='" +
                    smDay[0] + "'  and AgentState>='03' and a.branchtype = '1'"
                    + " and a.branchcode=b.agentgroup and branchattr like '" +
                    aSSRS.GetText(1, a) + "%'";
            ExeSQL bExeSQL = new ExeSQL();
            strArr[21] = bExeSQL.getOneValue(bsql);
//      data[21]+=Double.parseDouble(strArr[21]);
            //性别占比
            String csql = "select count(*) from LAAgent a where a.branchtype = '1' and AgentState>='03' and sex='0'"
                          + " and OutWorkDate<='" + emDay[1] +
                          "' and OutWorkDate>='" + smDay[0] + "' and exists (select 'X' from LABranchGroup where BranchType='1' and branchattr like '" +
                          aSSRS.GetText(1, a)
                          + "%' and a.branchcode=agentgroup)";
            ExeSQL cExeSQL = new ExeSQL();
            String tuonan = cExeSQL.getOneValue(csql);
//      data[1]+=Double.parseDouble(tuonan);
            if (strArr[21].equals("0"))
            {
                strArr[1] = "0";
            }
            else
            {
                strArr[1] = String.valueOf(Double.parseDouble(tuonan) /
                                           Double.parseDouble(strArr[21]));
            }
            //女
            String dsql = "select count(*) from LAAgent a where a.branchtype = '1' and AgentState>='03' and sex='1'"
                          + " and OutWorkDate<='" + emDay[1] +
                          "' and OutWorkDate>='" + smDay[0] + "' and exists (select 'X' from LABranchGroup where BranchType='1' and branchattr like '" +
                          aSSRS.GetText(1, a)
                          + "%' and a.branchcode=agentgroup)";
            ExeSQL dExeSQL = new ExeSQL();
            String tuonv = dExeSQL.getOneValue(dsql);
//      data[2]+=Double.parseDouble(tuonv);
            if (strArr[21].equals("0"))
            {
                strArr[2] = "0";
            }
            else
            {
                strArr[2] = String.valueOf(Double.parseDouble(tuonv) /
                                           Double.parseDouble(strArr[21]));
            }
            //年龄占比


//      data[3]+=birthdaytuo(aSSRS.GetText(1,a),emDay[1],"0","20",smDay[0]);
            if (strArr[21].equals("0"))
            {
                strArr[3] = "0";
            }
            else
            {
                strArr[3] = String.valueOf(birthdaytuo(aSSRS.GetText(1, a),
                        emDay[1], "0", "20", smDay[0]) /
                                           Double.parseDouble(strArr[21]));
            }
            //20

//      data[4]+=birthdaytuo(aSSRS.GetText(1,a),emDay[1],"20","30",smDay[0]);
            if (strArr[21].equals("0"))
            {
                strArr[4] = "0";
            }
            else
            {
                strArr[4] = String.valueOf(birthdaytuo(aSSRS.GetText(1, a),
                        emDay[1], "20", "30", smDay[0]) /
                                           Double.parseDouble(strArr[21]));
            }
            //30

//      data[5]+=birthdaytuo(aSSRS.GetText(1,a),emDay[1],"30","35",smDay[0]);
            if (strArr[21].equals("0"))
            {
                strArr[5] = "0";
            }
            else
            {
                strArr[5] = String.valueOf(birthdaytuo(aSSRS.GetText(1, a),
                        emDay[1], "30", "35", smDay[0]) /
                                           Double.parseDouble(strArr[21]));
            }
            //35
//      data[6]+=birthdaytuo(aSSRS.GetText(1,a),emDay[1],"35","40",smDay[0]);
            if (strArr[21].equals("0"))
            {
                strArr[6] = "0";
            }
            else
            {
                strArr[6] = String.valueOf(birthdaytuo(aSSRS.GetText(1, a),
                        emDay[1], "35", "40", smDay[0]) /
                                           Double.parseDouble(strArr[21]));
            }
            //40

//      data[7]+=birthdaytuo(aSSRS.GetText(1,a),emDay[1],"40","200",smDay[0]);
            if (strArr[21].equals("0"))
            {
                strArr[7] = "0";
            }
            else
            {
                strArr[7] = String.valueOf(birthdaytuo(aSSRS.GetText(1, a),
                        emDay[1], "40", "200", smDay[0]) /
                                           Double.parseDouble(strArr[21]));
            }
            //原行业
            double occu1 = oldoccupationtuo(aSSRS.GetText(1, a), emDay[1], "01",
                                            smDay[0]);
//      data[8]+=occu1;
            if (strArr[21].equals("0"))
            {
                strArr[8] = "0";
            }
            else
            {
                strArr[8] = String.valueOf(occu1 / Double.parseDouble(strArr[21]));
            }
            //行业2
            double occu2 = oldoccupationtuo(aSSRS.GetText(1, a), emDay[1], "02",
                                            smDay[0]);
//      data[9]+=occu2;
            if (strArr[21].equals("0"))
            {
                strArr[9] = "0";
            }
            else
            {
                strArr[9] = String.valueOf(occu2 / Double.parseDouble(strArr[21]));
            }
            //行业3
            double occu3 = oldoccupationtuo(aSSRS.GetText(1, a), emDay[1], "03",
                                            smDay[0]);
//      data[10]+=occu3;
            if (strArr[21].equals("0"))
            {
                strArr[10] = "0";
            }
            else
            {
                strArr[10] = String.valueOf(occu3 /
                                            Double.parseDouble(strArr[21]));
            }
            //行业4
            double occu4 = oldoccupationtuo(aSSRS.GetText(1, a), emDay[1], "04",
                                            smDay[0]);
//      data[11]+=occu4;
            if (strArr[21].equals("0"))
            {
                strArr[11] = "0";
            }
            else
            {
                strArr[11] = String.valueOf(occu4 /
                                            Double.parseDouble(strArr[21]));
            }
            //行业5
            double occu5 = oldoccupationtuo(aSSRS.GetText(1, a), emDay[1], "05",
                                            smDay[0]);
//      data[12]+=occu5;
            if (strArr[21].equals("0"))
            {
                strArr[12] = "0";
            }
            else
            {
                strArr[12] = String.valueOf(occu5 /
                                            Double.parseDouble(strArr[21]));
            }
            //行业6
            double occu6 = oldoccupationtuo(aSSRS.GetText(1, a), emDay[1], "06",
                                            smDay[0]);
//      data[13]+=occu6;
            if (strArr[21].equals("0"))
            {
                strArr[13] = "0";
            }
            else
            {
                strArr[13] = String.valueOf(occu6 /
                                            Double.parseDouble(strArr[21]));
            }
            //行业7
            double occu7 = oldoccupationtuo(aSSRS.GetText(1, a), emDay[1], "07",
                                            smDay[0]);
//      data[14]+=occu7;
            if (strArr[21].equals("0"))
            {
                strArr[14] = "0";
            }
            else
            {
                strArr[14] = String.valueOf(occu7 /
                                            Double.parseDouble(strArr[21]));
            }
            //学历
            double degree1 = degreetuo(aSSRS.GetText(1, a), emDay[1], "6,7",
                                       smDay[0]);
//      data[15]+=degree1;
            if (strArr[21].equals("0"))
            {
                strArr[15] = "0";
            }
            else
            {
                strArr[15] = String.valueOf(degree1 /
                                            Double.parseDouble(strArr[21]));
            }
            //学历2
            double degree2 = degreetuo(aSSRS.GetText(1, a), emDay[1], "5",
                                       smDay[0]);
//      data[16]+=degree2;
            if (strArr[21].equals("0"))
            {
                strArr[16] = "0";
            }
            else
            {
                strArr[16] = String.valueOf(degree2 /
                                            Double.parseDouble(strArr[21]));
            }
            //学历3
            double degree3 = degreetuo(aSSRS.GetText(1, a), emDay[1], "4",
                                       smDay[0]);
//      data[17]+=degree3;
            if (strArr[21].equals("0"))
            {
                strArr[17] = "0";
            }
            else
            {
                strArr[17] = String.valueOf(degree3 /
                                            Double.parseDouble(strArr[21]));
            }
            //学历4
            double degree4 = degreetuo(aSSRS.GetText(1, a), emDay[1], "3",
                                       smDay[0]);
//      data[18]+=degree4;
            if (strArr[21].equals("0"))
            {
                strArr[18] = "0";
            }
            else
            {
                strArr[18] = String.valueOf(degree4 /
                                            Double.parseDouble(strArr[21]));
            }
            //学历5
            double degree5 = degreetuo(aSSRS.GetText(1, a), emDay[1], "2",
                                       smDay[0]);
//      data[19]+=degree5;
            if (strArr[21].equals("0"))
            {
                strArr[19] = "0";
            }
            else
            {
                strArr[19] = String.valueOf(degree5 /
                                            Double.parseDouble(strArr[21]));
            }
            //学历6
            double degree6 = degreetuo(aSSRS.GetText(1, a), emDay[1], "0,1",
                                       smDay[0]);
//      data[20]+=degree6;
            if (strArr[21].equals("0"))
            {
                strArr[20] = "0";
            }
            else
            {
                strArr[20] = String.valueOf(degree6 /
                                            Double.parseDouble(strArr[21]));
            }
            tlistTable.add(strArr);
            strArr = new String[22];
        }

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("AgentPeopleMonth.vts", "printer"); //最好紧接着就初始化xml文档
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
        mResult.addElement(xmlexport);
        System.out.println("end");
        return true;
    }

    //原行业
    public double oldoccupationtuo(String a, String b, String c, String d)
    {
        String sql = "select count(*) from LAAgent a where a.branchtype = '1' and AgentState>='03' and oldoccupation='" +
                     c
                     + "' and OutWorkDate<='" + b + "' and OutWorkDate>='" + d + "' and exists (select 'X' from LABranchGroup where BranchType='1' and branchattr like '" +
                     a
                     + "%' and a.branchcode=agentgroup)";
        ExeSQL zExeSQL = new ExeSQL();
        double x = Double.parseDouble(zExeSQL.getOneValue(sql));
        return x;
    }

    //原行业
    public double birthdaytuo(String a, String b, String c, String d, String e)
    {
        String sql = "select count(*) from LAAgent a where a.branchtype = '1' and AgentState>='03' and '" +
                     currentYear + "'-substr(birthday,0,4)>'" + c + "' and '" +
                     currentYear + "'-substr(birthday,0,4)<='" + d
                     + "' and OutWorkDate<='" + b + "' and OutWorkDate>='" + e + "' and exists (select 'X' from LABranchGroup where BranchType='1' and branchattr like '" +
                     a
                     + "%' and a.branchcode=agentgroup)";
        ExeSQL zExeSQL = new ExeSQL();
        double x = Double.parseDouble(zExeSQL.getOneValue(sql));
        return x;
    }

//学历
    public double degreetuo(String a, String b, String c, String d)
    {
        String sql = "select count(*) from LAAgent a where a.branchtype = '1' and AgentState>='03' and degree in (" +
                     c
                     + ") and OutWorkDate<='" + b + "' and OutWorkDate>='" + d + "' and exists (select 'X' from LABranchGroup where BranchType='1' and branchattr like '" +
                     a
                     + "%' and a.branchcode=agentgroup)";
        ExeSQL zExeSQL = new ExeSQL();
        double x = Double.parseDouble(zExeSQL.getOneValue(sql));
        return x;
    }

    public static void main(String[] args)
    {
        PolNBDepartF1PBL tPolNBDepartF1PBL = new PolNBDepartF1PBL();
    }
}