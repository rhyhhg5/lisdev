package com.sinosoft.lis.agentprint;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.*;
import java.text.DecimalFormat;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class LAAgentFinSumBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    private String mLevel = null;
    private String mStartMonth = null;
    private String mEndMonth = null;
    SSRS tSSRS=new SSRS();
    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    public LAAgentFinSumBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LAAgentFinSumBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 4][52];

       mToExcel[0][2] = "收入汇总表" + "统计时间："+mStartMonth+"——"+mEndMonth+"";
       
       mToExcel[1][0]="管理机构 ";
       if(mLevel.equals("0")){
       	mToExcel[1][1]="团队编码";
       	mToExcel[1][2]="团队名称";
       	}
       else{
       	mToExcel[1][1]="人员编码";
       	mToExcel[1][2]="人员名称";
       }
       mToExcel[1][3]="综拓首佣汇总";
       mToExcel[1][4]="综拓续佣汇总"; 
       mToExcel[1][5]="首佣汇总";     
       mToExcel[1][6]="续佣汇总";     
       mToExcel[1][7]="间佣汇总";    
       mToExcel[1][8]="收入合计";    

        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row +1][col - 1] = tSSRS.GetText(row, col);
            }
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentFinSumBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        mLevel = (String) tf.getValueByName("Level");
        mStartMonth = (String) tf.getValueByName("StartMonth");
        mEndMonth = (String) tf.getValueByName("EndMonth");
        if(mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentFinSumBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }
    private String dealsum(int pmLength,int pmArrNum)
           {
             String tReturnValue = "";
             DecimalFormat tDF = new DecimalFormat("0.######");
             String tSQL = "select 0";

             for(int i=1;i<pmLength+1;i++)
             {
                 tSQL += " + " + tSSRS.GetText(i, pmArrNum+1);
             }

             tSQL += " + 0 from dual";

             tReturnValue = "" + tDF.format(execQuery(tSQL));

             return tReturnValue;
        }
   
    
    private String getAct(String pmValue1,String pmValue2)
    {
        String tSQL = "";
        String tRtValue = "";
        tSQL = "select DECIMAL(DECIMAL(" + pmValue1 + ",12,2) / DECIMAL(" +pmValue2 + ",12,2),12,6) from dual";
        try{
            tRtValue = String.valueOf(execQuery(tSQL));
        }catch(Exception ex)
        {
            System.out.println("getAct 出错！");
        }

        return tRtValue;
    }
        /**
         * 执行SQL文查询结果
         * @param sql String
         * @return double
         */
        private double execQuery(String sql)

        {
           Connection conn;
           conn = null;
           conn = DBConnPool.getConnection();

           System.out.println(sql);

           PreparedStatement st = null;
           ResultSet rs = null;
           try {
               if (conn == null)return 0.00;
               st = conn.prepareStatement(sql);
               if (st == null)return 0.00;
               rs = st.executeQuery();
               if (rs.next()) {
                   return rs.getDouble(1);
               }
               return 0.00;
           } catch (Exception ex) {
               ex.printStackTrace();
               return -1;
           } finally {
               try {
                  if (!conn.isClosed()) {
                      conn.close();
                  }
                  try {
                      st.close();
                      rs.close();
                  } catch (Exception ex2) {
                      ex2.printStackTrace();
                  }
                  st = null;
                  rs = null;
                  conn = null;
                } catch (Exception e) {}

           }
     }

    public static void main(String[] args)
    {
        LAAgentFinSumBL LAAgentFinSumBL = new            LAAgentFinSumBL();
    System.out.println("11111111");
    }
}
