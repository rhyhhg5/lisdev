package com.sinosoft.lis.agentprint;

import java.sql.Connection;
import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class AgentRiskStaticPrt
{
    public GlobalInput mGI = new GlobalInput();
    private String mStartDate = "";
    private String mEndDate = "";
    private String mStaticOrg = "";
    public VData mResult = new VData();
    public CErrors mErrors = new CErrors();

    public AgentRiskStaticPrt()
    {
    }

    public static void main(String[] args)
    {
        AgentRiskStaticPrt agentRiskStaticPrt1 = new AgentRiskStaticPrt();
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86110000";
        tGI.ManageCom = "86";
        tGI.Operator = "aa";
        VData tInputData = new VData();
        tInputData.clear();
        tInputData.addElement(tGI);
        tInputData.add(0, "2003-12-1");
        tInputData.add(1, "2003-12-18");
        tInputData.add(2, "861100000101");

        if (!agentRiskStaticPrt1.getInputData(tInputData))
        {
            System.out.println("-----getinputdata failed!------");
        }
        if (!agentRiskStaticPrt1.prepareData())
        {
            System.out.println("-------prepareData failed!------");
        }

    }

    public boolean getInputData(VData tInputData)
    {
        try
        {
            this.mStartDate = (String) tInputData.getObjectByObjectName(
                    "String", 0);
            this.mEndDate = (String) tInputData.getObjectByObjectName("String",
                    1);
            this.mStaticOrg = (String) tInputData.getObjectByObjectName(
                    "String", 2);
            this.mGI = (GlobalInput) tInputData.getObjectByObjectName(
                    "GlobalInput", 0);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("getInputData", ex.toString());
            return false;
        }

        System.out.println("------------getInputData------------");
        System.out.println("统计起期：" + this.mStartDate);
        System.out.println("统计止期：" + this.mEndDate);
        System.out.println("统计机构：" + this.mStaticOrg);
        System.out.println("------------------------------------");
        return true;
    }

    public boolean prepareData()
    {
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);
            String tSql = "";
            SSRS ssrs = new SSRS();

            //取长险传统险的riskcode
            tSql =
                    "select distinct x.riskcode,y.riskname from lmriskapp y ,lacommision x where"
                    + " x.tmakedate >= '" + this.mStartDate.trim()
                    + "' and x.tmakedate <= '" + this.mEndDate.trim()
                    + "' and x.branchtype = '1' and x.riskcode = y.riskcode "
                    + "and y.riskperiod = 'L' "
                    + "and y.bonusflag = 'N' "
                    + " and trim(x.branchattr) like '" + this.mStaticOrg.trim() +
                    "%'";
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(tSql);
            int tCount1 = 0;
            tCount1 = aSSRS.getMaxRow();

            //取长险分红险的riskcode
            tSql =
                    "select distinct x.riskcode,y.riskname from lmriskapp y,lacommision x where"
                    + " x.tmakedate >= '" + this.mStartDate.trim()
                    + "' and x.tmakedate <= '" + this.mEndDate.trim()
                    +
                    "' and x.branchtype = '1' and x.riskcode = y.riskcode and y.riskperiod = 'L' "
                    + "and y.bonusflag = 'Y'"
                    + " and trim(x.branchattr) like '" + this.mStaticOrg.trim() +
                    "%'";
            SSRS bSSRS = new SSRS();
            bSSRS = aExeSQL.execSQL(tSql);
            int tCount2 = 0;
            tCount2 = bSSRS.getMaxRow();

            //取短险意外险的riskcode
            tSql =
                    "select distinct x.riskcode,y.riskname from lacommision x,lmriskapp y where"
                    + " x.tmakedate >= '" + this.mStartDate.trim()
                    + "' and x.tmakedate <= '" + this.mEndDate.trim()
                    +
                    "' and branchtype = '1' and x.riskcode = y.riskcode and y.riskperiod <> 'L' "
                    + "and y.risktype = 'A'"
                    + " and trim(x.branchattr) like '" + this.mStaticOrg.trim() +
                    "%'";
            SSRS cSSRS = new SSRS();
            cSSRS = aExeSQL.execSQL(tSql);
            int tCount3 = 0;
            tCount3 = cSSRS.getMaxRow();

            //取短险健康险的riskcode
            tSql =
                    "select distinct x.riskcode,y.riskname from lacommision x,lmriskapp y where "
                    + " x.tmakedate >= '" + this.mStartDate.trim()
                    + "' and x.tmakedate <= '" + this.mEndDate.trim()
                    +
                    "' and branchtype = '1' and x.riskcode = y.riskcode and y.riskperiod <> 'L' "
                    + "and y.risktype = 'H'"
                    + " and trim(x.branchattr) like '" + this.mStaticOrg.trim() +
                    "%'";
            SSRS dSSRS = new SSRS();
            dSSRS = aExeSQL.execSQL(tSql);
            int tCount4 = 0;
            tCount4 = dSSRS.getMaxRow();

            //取短险寿险的riskcode
            tSql =
                    "select distinct x.riskcode,y.riskname from lacommision x,lmriskapp y where "
                    + " x.tmakedate >= '" + this.mStartDate.trim()
                    + "' and x.tmakedate <= '" + this.mEndDate.trim()
                    +
                    "' and branchtype = '1' and x.riskcode = y.riskcode and y.riskperiod <> 'L' "
                    + "and y.risktype = 'L'"
                    + " and trim(x.branchattr) like '" + this.mStaticOrg.trim() +
                    "%'";
            SSRS eSSRS = new SSRS();
            eSSRS = aExeSQL.execSQL(tSql);
            int tCount5 = 0;
            tCount5 = eSSRS.getMaxRow();
            int tCount = 0;

            //取总规模保费
            tSql = "select nvl(sum(transmoney)/10000,0) from lacommision where"
                   + " tmakedate >= '" + this.mStartDate.trim()
                   + "' and tmakedate <= '" + this.mEndDate
                   + "' and payyear = 0 and branchtype = '1'"
                   + " and trim(branchattr) like '" + this.mStaticOrg.trim() +
                   "%'";
            float tSumTransMoney = 0;
            tSumTransMoney = Float.parseFloat(aExeSQL.getOneValue(tSql));

            String[][] strArr = null;
            String tSql2 = "";

            ListTable tListTable = new ListTable();
            tListTable.setName("AgentRisk");

            TextTag texttag = new TextTag(); //新建一个TextTag的实例
            String tStaticDate = "";
            tStaticDate = this.mStartDate.trim() + " 至 " + this.mEndDate.trim();
            texttag.add("StaticDate", tStaticDate); //输入制表时间
            tSql = "select trim(shortname) from ldcom where comcode = '" +
                   this.mStaticOrg.trim() + "'";
            String tName = aExeSQL.getOneValue(tSql).trim();
            texttag.add("StaticOrg", tName);

            //传统险小计：
            float tSumTransMoney1 = 0;
            float tSumStandPrem1 = 0;
            float tSumCalCount1 = 0;
            float tSumIntvMoney1 = 0;
            float tSumContinueMoney1 = 0;
            float tSumMoney1 = 0;
            //分红险小计：
            float tSumTransMoney2 = 0;
            float tSumStandPrem2 = 0;
            float tSumCalCount2 = 0;
            float tSumIntvMoney2 = 0;
            float tSumContinueMoney2 = 0;
            float tSumMoney2 = 0;
            //意外险小计：
            float tSumTransMoney3 = 0;
            float tSumStandPrem3 = 0;
            float tSumCalCount3 = 0;
            float tSumIntvMoney3 = 0;
            float tSumContinueMoney3 = 0;
            float tSumMoney3 = 0;
            //健康险小计：
            float tSumTransMoney4 = 0;
            float tSumStandPrem4 = 0;
            float tSumCalCount4 = 0;
            float tSumIntvMoney4 = 0;
            float tSumContinueMoney4 = 0;
            float tSumMoney4 = 0;

            //短险寿险小计：
            float tSumTransMoney5 = 0;
            float tSumStandPrem5 = 0;
            float tSumCalCount5 = 0;
            float tSumIntvMoney5 = 0;
            float tSumContinueMoney5 = 0;
            float tSumMoney5 = 0;

            // float tSumTransMoney = 0;
            String[] tEachList = null;
            String[] tRow = null;

            if (tCount1 + tCount2 > 0)
            {
                if (tCount1 != 0)
                {
                    strArr = new String[tCount1][8];
                    for (int i = 1; i <= tCount1; i++)
                    {
                        String tRiskCode = aSSRS.GetText(i, 1);
                        strArr[i - 1][0] = aSSRS.GetText(i, 2).trim();

                        tRow = new String[10];
                        tRow = getValue(tRiskCode);
                        strArr[i - 1][1] = tRow[3];
                        strArr[i - 1][3] = tRow[5];
                        strArr[i - 1][4] = tRow[6];
                        strArr[i - 1][5] = tRow[7];
                        strArr[i - 1][6] = tRow[8];
                        strArr[i - 1][7] = tRow[9];

                        //规模保费
                        tSumTransMoney1 +=
                                Float.parseFloat(strArr[i - 1][1].trim());
                        //标准保费
                        tSumStandPrem1 +=
                                Float.parseFloat(strArr[i - 1][3].trim());
                        //件数
                        tSumCalCount1 += Float.parseFloat(strArr[i - 1][4].trim());
                        //期交保费
                        tSumIntvMoney1 +=
                                Float.parseFloat(strArr[i - 1][5].trim());
                        //续期保费
                        tSumContinueMoney1 +=
                                Float.parseFloat(strArr[i - 1][6].trim());
                        //保费合计
                        tSumMoney1 += Float.parseFloat(strArr[i - 1][7].trim());
                    }
                    //保费占比,期交占比
                    if (tSumTransMoney != 0)
                    {
                        for (int i = 0; i < tCount1; i++)
                        {
                            strArr[i][2] = new DecimalFormat("0.00%").format(
                                    Double.parseDouble(strArr[i][1]) /
                                    tSumTransMoney);
                            strArr[i][5] = new DecimalFormat("0.00%").format(
                                    Double.parseDouble(strArr[i][5]) /
                                    Float.parseFloat(strArr[i][1]));
                        }
                    }
                    else
                    {
                        for (int i = 0; i < tCount1; i++)
                        {
                            strArr[i][2] = "0%";
                            strArr[i][5] = "0%";
                        }
                    }

                    for (int i = 0; i < tCount1; i++)
                    {
                        tEachList = new String[8];
                        for (int j = 0; j < 8; j++)
                        {
                            tEachList[j] = strArr[i][j];
                        }
                        tListTable.add(tEachList);
                    }

                    //传统险小计
                    tEachList = new String[8];
                    tEachList[0] = "传统险小计";
                    tEachList[1] = Float.toString(tSumTransMoney1).trim();
                    tEachList[2] = new DecimalFormat("0.00%").format(
                            tSumTransMoney1 / tSumTransMoney);
                    tEachList[3] = Float.toString(tSumStandPrem1).trim();
                    tEachList[4] = Float.toString(tSumCalCount1).trim();
                    if (tSumTransMoney1 == 0)
                    {
                        tEachList[5] = "0";
                    }
                    else
                    {
                        tEachList[5] = new DecimalFormat("0.00%").format(
                                tSumIntvMoney1 / tSumTransMoney1);
                    }
                    tEachList[6] = Float.toString(tSumContinueMoney1).trim();
                    tEachList[7] = Float.toString(tSumMoney1).trim();
                    tListTable.add(tEachList);
                }

                //分红险
                if (tCount2 != 0)
                {
                    strArr = new String[tCount2][8];
                    for (int i = 1; i <= tCount2; i++)
                    {
                        String tRiskCode = bSSRS.GetText(i, 1);
                        strArr[i - 1][0] = bSSRS.GetText(i, 2).trim();

                        tRow = new String[10];
                        tRow = getValue(tRiskCode);
                        strArr[i - 1][1] = tRow[3];
                        strArr[i - 1][3] = tRow[5];
                        strArr[i - 1][4] = tRow[6];
                        strArr[i - 1][5] = tRow[7];
                        strArr[i - 1][6] = tRow[8];
                        strArr[i - 1][7] = tRow[9];

                        //规模保费
                        tSumTransMoney2 +=
                                Float.parseFloat(strArr[i - 1][1].trim());
                        //标准保费
                        tSumStandPrem2 +=
                                Float.parseFloat(strArr[i - 1][3].trim());
                        //件数
                        tSumCalCount2 += Float.parseFloat(strArr[i - 1][4].trim());
                        //期交保费
                        tSumIntvMoney2 +=
                                Float.parseFloat(strArr[i - 1][5].trim());
                        //续期保费
                        tSumContinueMoney2 +=
                                Float.parseFloat(strArr[i - 1][6].trim());
                        //保费合计
                        tSumMoney2 += Float.parseFloat(strArr[i - 1][7].trim());
                    }
                    //保费占比，期交占比
                    if (tSumTransMoney != 0)
                    {
                        for (int i = 0; i < tCount2; i++)
                        {
                            strArr[i][2] = new DecimalFormat("0.00%").format(
                                    Float.parseFloat(strArr[i][1].trim()) /
                                    tSumTransMoney).trim();
                            strArr[i][5] = new DecimalFormat("0.00%").format(
                                    Float.parseFloat(strArr[i][5].trim()) /
                                    Float.parseFloat(strArr[i][1].trim()));
                        }
                    }
                    else
                    {
                        for (int i = 0; i < tCount2; i++)
                        {
                            strArr[i][2] = "0%";
                            strArr[i][5] = "0%";
                        }
                    }

                    for (int i = 0; i < tCount2; i++)
                    {
                        tEachList = new String[8];
                        for (int j = 0; j < 8; j++)
                        {
                            tEachList[j] = strArr[i][j];
                        }
                        tListTable.add(tEachList);
                    }

                    //分红险小计
                    tEachList = new String[8];
                    tEachList[0] = "分红险小计";
                    tEachList[1] = Float.toString(tSumTransMoney2).trim();
                    tEachList[2] = new DecimalFormat("0.00%").format(
                            tSumTransMoney2 / tSumTransMoney).trim(); ;
                    tEachList[3] = Float.toString(tSumStandPrem2).trim();
                    tEachList[4] = Float.toString(tSumCalCount2).trim();
                    if (tSumTransMoney2 == 0)
                    {
                        tEachList[5] = "0%";
                    }
                    else
                    {
                        tEachList[5] = new DecimalFormat("0.00%").format(
                                tSumIntvMoney2 / tSumTransMoney2).trim();
                    }
                    tEachList[6] = Float.toString(tSumContinueMoney2).trim();
                    tEachList[7] = Float.toString(tSumMoney2).trim();
                    tListTable.add(tEachList);
                }
            } //长险 if

            //长险合计：
            tEachList = new String[8];
            //规模保费
            tEachList[0] = "长期险合计";
            tEachList[1] = Float.toString(tSumTransMoney1 + tSumTransMoney2).
                           trim();
            tEachList[2] = new DecimalFormat("0.00%").format((tSumTransMoney1 +
                    tSumTransMoney2) / tSumTransMoney).trim(); ;
            tEachList[3] = Float.toString(tSumStandPrem1 + tSumStandPrem2).trim();
            tEachList[4] = Float.toString(tSumCalCount1 + tSumCalCount2).trim();
            if (tSumTransMoney1 + tSumTransMoney2 == 0)
            {
                tEachList[5] = "0%";
            }
            else
            {
                tEachList[5] = new DecimalFormat("0.00%").format((
                        tSumIntvMoney1 + tSumIntvMoney2) /
                        (tSumTransMoney1 + tSumTransMoney2)).trim();
            }

            tEachList[6] = Float.toString(tSumContinueMoney1 +
                                          tSumContinueMoney2).trim();
            tEachList[7] = Float.toString(tSumMoney1 + tSumMoney2).trim();
            tListTable.add(tEachList);

            //短险
            if (tCount3 + tCount4 + tCount5 > 0)
            {
                if (tCount3 != 0)
                {
                    strArr = new String[tCount3][8];
                    for (int i = 1; i <= tCount3; i++)
                    {
                        String tRiskCode = cSSRS.GetText(i, 1);
                        strArr[i - 1][0] = cSSRS.GetText(i, 2).trim();

                        tRow = new String[8];
                        tRow = getValue(tRiskCode);
                        strArr[i - 1][1] = tRow[3];
                        strArr[i - 1][3] = tRow[5];
                        strArr[i - 1][4] = tRow[6];
                        strArr[i - 1][5] = tRow[7];
                        strArr[i - 1][6] = tRow[8];
                        strArr[i - 1][7] = tRow[9];

                        //规模保费
                        tSumTransMoney3 +=
                                Float.parseFloat(strArr[i - 1][1].trim());
                        //标准保费
                        tSumStandPrem3 +=
                                Float.parseFloat(strArr[i - 1][3].trim());
                        //件数
                        tSumCalCount3 += Float.parseFloat(strArr[i - 1][4].trim());
                        //期交保费
                        tSumIntvMoney3 +=
                                Float.parseFloat(strArr[i - 1][5].trim());
                        //续期保费
                        tSumContinueMoney3 +=
                                Float.parseFloat(strArr[i - 1][6].trim());
                        //保费合计
                        tSumMoney3 += Float.parseFloat(strArr[i - 1][7].trim());
                    }
                    //保费占比,期交占比
                    if (tSumTransMoney != 0)
                    {
                        for (int i = 0; i < tCount3; i++)
                        {
                            strArr[i][2] = new DecimalFormat("0.00%").format(
                                    Float.parseFloat(strArr[i][1].trim()) /
                                    tSumTransMoney).trim();
                            strArr[i][5] = new DecimalFormat("0.00%").format(
                                    Float.parseFloat(strArr[i][5].trim()) /
                                    Float.parseFloat(strArr[i][1].trim()));
                        }
                    }
                    else
                    {
                        for (int i = 0; i < tCount3; i++)
                        {
                            strArr[i][2] = "0%";
                            strArr[i][5] = "0%";
                        }
                    }

                    for (int i = 0; i < tCount3; i++)
                    {
                        tEachList = new String[8];
                        for (int j = 0; j < 8; j++)
                        {
                            tEachList[j] = strArr[i][j];
                        }
                        tListTable.add(tEachList);
                    }

                    //意外险小计
                    tEachList = new String[8];
                    tEachList[0] = "意外险小计";
                    tEachList[1] = Float.toString(tSumTransMoney3).trim();
                    tEachList[2] = new DecimalFormat("0.00%").format(
                            tSumTransMoney3 / tSumTransMoney).trim();
                    tEachList[3] = Float.toString(tSumStandPrem3).trim();
                    tEachList[4] = Float.toString(tSumCalCount3).trim();
                    if (tSumTransMoney3 == 0)
                    {
                        tEachList[5] = "0%";
                    }
                    else
                    {
                        tEachList[5] = new DecimalFormat("0.00%").format(
                                tSumIntvMoney3 / tSumTransMoney3).trim();
                    }
                    tEachList[6] = Float.toString(tSumContinueMoney3).trim();
                    tEachList[7] = Float.toString(tSumMoney3).trim();
                    tListTable.add(tEachList);
                }

                //健康险
                if (tCount4 != 0)
                {
                    strArr = new String[tCount4][8];
                    for (int i = 1; i <= tCount4; i++)
                    {
                        String tRiskCode = dSSRS.GetText(i, 1);
                        strArr[i - 1][0] = dSSRS.GetText(i, 2).trim();

                        tRow = new String[8];
                        tRow = getValue(tRiskCode);
                        strArr[i - 1][1] = tRow[3];
                        strArr[i - 1][3] = tRow[5];
                        strArr[i - 1][4] = tRow[6];
                        strArr[i - 1][5] = tRow[7];
                        strArr[i - 1][6] = tRow[8];
                        strArr[i - 1][7] = tRow[9];

                        //规模保费
                        tSumTransMoney4 +=
                                Float.parseFloat(strArr[i - 1][1].trim());
                        //标准保费
                        tSumStandPrem4 +=
                                Float.parseFloat(strArr[i - 1][3].trim());
                        //件数
                        tSumCalCount4 += Float.parseFloat(strArr[i - 1][4].trim());
                        //期交保费
                        tSumIntvMoney4 +=
                                Float.parseFloat(strArr[i - 1][5].trim());
                        //续期保费
                        tSumContinueMoney4 +=
                                Float.parseFloat(strArr[i - 1][6].trim());
                        //保费合计
                        tSumMoney4 += Float.parseFloat(strArr[i - 1][7].trim());
                    }
                    //保费占比
                    if (tSumTransMoney != 0)
                    {
                        for (int i = 0; i < tCount4; i++)
                        {
                            strArr[i][2] = new DecimalFormat("0.00%").format(
                                    Float.parseFloat(strArr[i][1].trim()) /
                                    tSumTransMoney).trim();
                            strArr[i][5] = new DecimalFormat("0.00%").format(
                                    Float.parseFloat(strArr[i][5].trim()) /
                                    Float.parseFloat(strArr[i][1].trim()));
                        }
                    }
                    else
                    {
                        for (int i = 0; i < tCount4; i++)
                        {
                            strArr[i][2] = "0%";
                            strArr[i][5] = "0%";
                        }
                    }

                    for (int i = 0; i < tCount4; i++)
                    {
                        tEachList = new String[8];
                        for (int j = 0; j < 8; j++)
                        {
                            tEachList[j] = strArr[i][j];
                        }
                        tListTable.add(tEachList);
                    }

                    //健康险小计
                    tEachList = new String[8];
                    tEachList[0] = "健康险小计";
                    tEachList[1] = Float.toString(tSumTransMoney4).trim();
                    tEachList[2] = new DecimalFormat("0.00%").format(
                            tSumTransMoney4 / tSumTransMoney).trim();
                    tEachList[3] = Float.toString(tSumStandPrem4).trim();
                    tEachList[4] = Float.toString(tSumCalCount4).trim();
                    if (tSumTransMoney4 == 0)
                    {
                        tEachList[5] = "0%";
                    }
                    else
                    {
                        tEachList[5] = new DecimalFormat("0.00%").format(
                                tSumIntvMoney4 / tSumTransMoney4).trim();
                    }
                    tEachList[6] = Float.toString(tSumContinueMoney4).trim();
                    tEachList[7] = Float.toString(tSumMoney4).trim();
                    tListTable.add(tEachList);
                }

                //短险寿险
                if (tCount5 != 0)
                {
                    strArr = new String[tCount5][8];
                    for (int i = 1; i <= tCount5; i++)
                    {
                        String tRiskCode = eSSRS.GetText(i, 1);
                        strArr[i - 1][0] = eSSRS.GetText(i, 2).trim();

                        tRow = new String[8];
                        tRow = getValue(tRiskCode);
                        strArr[i - 1][1] = tRow[3];
                        strArr[i - 1][3] = tRow[5];
                        strArr[i - 1][4] = tRow[6];
                        strArr[i - 1][5] = tRow[7];
                        strArr[i - 1][6] = tRow[8];
                        strArr[i - 1][7] = tRow[9];

                        //规模保费
                        tSumTransMoney5 +=
                                Float.parseFloat(strArr[i - 1][1].trim());
                        //标准保费
                        tSumStandPrem5 +=
                                Float.parseFloat(strArr[i - 1][3].trim());
                        //件数
                        tSumCalCount5 += Float.parseFloat(strArr[i - 1][4].trim());
                        //期交保费
                        tSumIntvMoney5 +=
                                Float.parseFloat(strArr[i - 1][5].trim());
                        //续期保费
                        tSumContinueMoney5 +=
                                Float.parseFloat(strArr[i - 1][6].trim());
                        //保费合计
                        tSumMoney5 += Float.parseFloat(strArr[i - 1][7].trim());
                    }
                    //保费占比
                    if (tSumTransMoney != 0)
                    {
                        for (int i = 0; i < tCount5; i++)
                        {
                            strArr[i][2] = new DecimalFormat("0.00%").format(
                                    Float.parseFloat(strArr[i][1].trim()) /
                                    tSumTransMoney).trim();
                            strArr[i][5] = new DecimalFormat("0.00%").format(
                                    Float.parseFloat(strArr[i][5].trim()) /
                                    Float.parseFloat(strArr[i][1].trim()));
                        }
                    }
                    else
                    {
                        for (int i = 0; i < tCount5; i++)
                        {
                            strArr[i][2] = "0%";
                            strArr[i][5] = "0%";
                        }
                    }

                    for (int i = 0; i < tCount5; i++)
                    {
                        tEachList = new String[8];
                        for (int j = 0; j < 8; j++)
                        {
                            tEachList[j] = strArr[i][j];
                        }
                        tListTable.add(tEachList);
                    }

                    //健康险小计
                    tEachList = new String[8];
                    tEachList[0] = "一年期定期寿险小计";
                    tEachList[1] = Float.toString(tSumTransMoney5).trim();
                    tEachList[2] = new DecimalFormat("0.00%").format(
                            tSumTransMoney5 / tSumTransMoney).trim();
                    tEachList[3] = Float.toString(tSumStandPrem5).trim();
                    tEachList[4] = Float.toString(tSumCalCount5).trim();
                    if (tSumTransMoney5 == 0)
                    {
                        tEachList[5] = "0%";
                    }
                    else
                    {
                        tEachList[5] = new DecimalFormat("0.00%").format(
                                tSumIntvMoney5 / tSumTransMoney5).trim();
                    }
                    tEachList[6] = Float.toString(tSumContinueMoney5).trim();
                    tEachList[7] = Float.toString(tSumMoney5).trim();
                    tListTable.add(tEachList);
                }

            } //短险if

            //短险合计：
            tEachList = new String[8];
            tEachList[0] = "短险合计";
            tEachList[1] = Float.toString(tSumTransMoney3 + tSumTransMoney4 +
                                          tSumTransMoney5).trim();
            tEachList[2] = new DecimalFormat("0.00%").format((tSumTransMoney3 +
                    tSumTransMoney4 + tSumTransMoney5) /
                    tSumTransMoney).trim();
            tEachList[3] = Float.toString(tSumStandPrem3 + tSumStandPrem4 +
                                          tSumStandPrem5).trim();
            tEachList[4] = Float.toString(tSumCalCount3 + tSumCalCount4 +
                                          tSumCalCount5).trim();
            if (tSumTransMoney3 + tSumTransMoney4 + tSumTransMoney5 == 0)
            {
                tEachList[5] = "0%";
            }
            else
            {
                tEachList[5] = new DecimalFormat("0.00%").format((
                        tSumIntvMoney3 + tSumIntvMoney4 + tSumIntvMoney5) /
                        (tSumTransMoney3 + tSumTransMoney4 + tSumTransMoney5)).
                               trim();
            }
            tEachList[6] = Float.toString(tSumContinueMoney3 +
                                          tSumContinueMoney4 +
                                          tSumContinueMoney5).trim();
            tEachList[7] = Float.toString(tSumMoney3 + tSumMoney4 + tSumMoney5).
                           trim();
            tListTable.add(tEachList);

            //总计
            tEachList = new String[8];
            tEachList[0] = "总计";
            //tSumTransMoney = tSumTransMoney1+tSumTransMoney2+tSumTransMoney3+tSumTransMoney4;
            tEachList[1] = Float.toString(tSumTransMoney);
            tEachList[2] = "";
            tEachList[3] = Float.toString(tSumStandPrem1 + tSumStandPrem2 +
                                          tSumStandPrem3 + tSumStandPrem4 +
                                          tSumStandPrem5).trim();
            tEachList[4] = Float.toString(tSumCalCount1 + tSumCalCount2 +
                                          tSumCalCount3 + tSumCalCount4 +
                                          tSumCalCount5).trim();
            if (tSumTransMoney == 0)
            {
                tEachList[5] = "0%";
            }
            else
            {
                tEachList[5] = new DecimalFormat("0.00%").format((
                        tSumIntvMoney1 + tSumIntvMoney2 + tSumIntvMoney3 +
                        tSumIntvMoney4 + tSumIntvMoney5) /
                        tSumTransMoney).trim();
            }
            tEachList[6] = Float.toString(tSumContinueMoney1 +
                                          tSumContinueMoney2 +
                                          tSumContinueMoney3 +
                                          tSumContinueMoney4 +
                                          tSumContinueMoney5).trim();
            tEachList[7] = Float.toString(tSumMoney1 + tSumMoney2 + tSumMoney3 +
                                          tSumMoney4 + tSumMoney5).trim();
            tListTable.add(tEachList);

            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("AgentRiskStatic.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tListTable, tEachList);
//    xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
            this.mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentRiskStaticPrt";
            tError.functionName = "preparedata";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("------prepareData end-----");

        return true;
    }


    private String[] getValue(String tRiskCode)
    {
        String[] tArr = new String[10];
        String Sql = "";
        ExeSQL tExe = new ExeSQL();
        SSRS ssrs = new SSRS();

        Sql =
                "select nvl(sum(transmoney)/10000,0),nvl(sum(standprem)/10000,0),"
                + "nvl(sum(calcount),0) from lacommision "
                + "where trim(branchattr) like '" + this.mStaticOrg.trim()
                + "%' and tmakedate >= '" + this.mStartDate.trim()
                + "' and tmakedate <= '" + this.mEndDate
                + "' and riskcode = '" + tRiskCode.trim()
                + "' and payyear = 0 and branchtype = '1'";
        ssrs = tExe.execSQL(Sql);
        //规模保费
        tArr[3] = ssrs.GetText(1, 1).trim();

        //标准保费
        tArr[5] = ssrs.GetText(1, 2).trim();

        //件数
        tArr[6] = ssrs.GetText(1, 3).trim();

        //期交保费
        Sql = "select nvl(sum(transmoney)/10000,0) from lacommision "
              + "where trim(branchattr) like '" + this.mStaticOrg.trim()
              + "%' and tmakedate >= '" + this.mStartDate.trim()
              + "' and tmakedate <= '" + this.mEndDate
              + "' and riskcode = '" + tRiskCode.trim()
              + "' and payyear = 0 and payintv <> 0 and branchtype = '1'";
        tArr[7] = tExe.getOneValue(Sql).trim();

        //续期保费
        Sql = "select nvl(sum(transmoney)/10000,0) from lacommision "
              + "where trim(branchattr) like '" + this.mStaticOrg.trim()
              + "%' and tmakedate >= '" + this.mStartDate.trim()
              + "' and tmakedate <= '" + this.mEndDate
              + "' and riskcode = '" + tRiskCode.trim()
              + "' and payyear > 0 and branchtype = '1'";
        tArr[8] = tExe.getOneValue(Sql).trim();

        //保费合计
        tArr[9] = Float.toString(Float.parseFloat(tArr[3].trim())
                                 + Float.parseFloat(tArr[8].trim()));

        return tArr;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void dealError(String FuncName, String ErrMsg)
    {
        CError error = new CError();

        error.moduleName = "AgentRiskStaticPrt";
        error.errorMessage = ErrMsg.trim();
        error.functionName = FuncName.trim();

        this.mErrors.addOneError(error);
    }

}