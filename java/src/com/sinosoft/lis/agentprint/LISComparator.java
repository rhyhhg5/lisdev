package com.sinosoft.lis.agentprint;

import java.util.Comparator;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */

public class LISComparator implements Comparator
{

    private int num;

    public LISComparator()
    {
    }

    public void setNum(int aNum)
    {
        num = aNum;
    }

    public int compare(Object o1, Object o2)
    {
        String[] a = (String[]) o1;
        String[] b = (String[]) o2;

        double Da = Double.parseDouble(a[num]);
        double Db = Double.parseDouble(b[num]);

        if (Da >= Db)
        {
            return -1;
        }
        else
        {
            return 1;
        }
//    return a[0].compareTo(b[0]);
    }

    public boolean equals(Object o)
    {
        return false;
    }

    public static void main(String[] args)
    {
        LISComparator LISComparator1 = new LISComparator();
    }
}