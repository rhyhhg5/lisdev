package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import java.sql.Connection;
import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class BankBranchattrBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private GlobalInput mGlobalInput = new GlobalInput();
    private String StartDay = "";
    private String EndDay = "";

    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";

    //业务处理相关变量

    /** 全局数据 */
    public BankBranchattrBL()
    {
    }

    /**
        传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        StartDay = (String) cInputData.get(0);
        EndDay = (String) cInputData.get(1);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 2));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "BankWeekBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String[] strArr = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("Bank");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDay", StartDay); //输入制表时间
        texttag.add("EndDay", EndDay); //输入制表时间
        strArr = new String[9];
        double[] data = null; //小计
        data = new double[9];
        double[] data1 = null; //总计
        data1 = new double[9];
        double BranchMoney = 0;

        //统计分公司
        String asql =
                "select trim(shortname),comcode from ldcom where length(trim(comcode))=4 and comcode like '"
                + mGlobalInput.ManageCom + "%' order by comcode";

        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(asql);
            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                for (int b = 1; b <= aSSRS.getMaxCol(); b++)
                {
                    if (b == 1)
                    {
                        strArr[0] = aSSRS.GetText(a, 1); //分公司名称
                    }
                    if (b == 2)
                    {
                        //渠道组名称
                        String fsql =
                                "select trim(name),agentgroup,branchattr from labranchgroup where BranchJobType='1' and branchtype='3' and managecom like '"
                                + aSSRS.GetText(a, 2)
                                +
                                "%' and length(trim(branchattr))=12 and endflag='N'";
                        SSRS bSSRS = new SSRS();
                        bSSRS = aExeSQL.execSQL(fsql);
                        for (int c = 1; c <= bSSRS.getMaxRow(); c++)
                        {
                            for (int d = 1; d <= bSSRS.getMaxCol(); d++)
                            {
                                if (d == 1)
                                {
                                    strArr[1] = bSSRS.GetText(c, 1); //渠道名称
                                }
                                if (d == 2)
                                {
                                    //渠道人力
                                    String bsql =
                                            "select count(*) From laagent where agentstate<'03' and agentgroup='"
                                            + bSSRS.GetText(c, 2) + "'";
                                    strArr[2] = aExeSQL.getOneValue(bsql);
                                    data[2] += Double.parseDouble(strArr[2]);
                                    //查询险种
                                    String ksql =
                                            "select distinct a.riskcode,b.riskname from lacommision a,LMRiskApp b where branchtype='3' "
                                            +
                                            "and a.riskcode=b.riskcode  and tmakeDate<='"
                                            + EndDay +
                                            "' and branchattr like '"
                                            + bSSRS.GetText(c, 3) + "%'";
                                    SSRS dSSRS = new SSRS();
                                    dSSRS = aExeSQL.execSQL(ksql);
                                    for (int g = 1; g <= dSSRS.getMaxRow(); g++)
                                    {
                                        strArr[3] = dSSRS.GetText(g, 2); //险种名称
                                        //本期承保
                                        String jsql =
                                                "select nvl(sum(transmoney),0)/10000 from Lacommision where branchtype='3' and tmakeDate>='"
                                                + StartDay +
                                                "' and tmakeDate<='"
                                                + EndDay +
                                                "' and branchattr like '"
                                                + bSSRS.GetText(c, 3)
                                                + "%' and riskcode='"
                                                + dSSRS.GetText(g, 1) + "'";
                                        strArr[4] = aExeSQL.getOneValue(jsql);

                                        //                                    BranchMoney+=Double.parseDouble(strArr[4]);//组承保合计
                                        data[4] += Double.parseDouble(strArr[4]);
                                        //期内组承保合计
                                        String gsql =
                                                "select nvl(sum(transmoney),0)/10000 from Lacommision where branchtype='3' and tmakeDate>='"
                                                + StartDay +
                                                "' and tmakeDate<='"
                                                + EndDay +
                                                "' and branchattr like '"
                                                + bSSRS.GetText(c, 3) + "%' ";
                                        strArr[5] = aExeSQL.getOneValue(gsql);
                                        //累计承保
                                        String dsql =
                                                "select nvl(sum(transmoney),0)/10000 from Lacommision where branchtype='3' and tmakeDate>='"
                                                + EndDay.substring(0, 4)
                                                + "-01-01' and tmakeDate<='" +
                                                EndDay
                                                + "' and branchattr like '"
                                                + bSSRS.GetText(c, 3)
                                                + "%' and riskcode='"
                                                + dSSRS.GetText(g, 1) + "'";
                                        strArr[6] = aExeSQL.getOneValue(dsql);
                                        data[6] += Double.parseDouble(strArr[6]);
                                        //累计预收
                                        String csql =
                                                "select nvl(sum(a),0)/10000 From (select sum(prem) a From lcpol where makedate>='"
                                                + EndDay.substring(0, 4)
                                                + "-01-01' and makedate<='" +
                                                EndDay
                                                + "' and agentgroup ='"
                                                + bSSRS.GetText(c, 2)
                                                + "' and riskcode='"
                                                + dSSRS.GetText(g, 1)
                                                +
                                                "' union select sum(prem) a From lbpol where makedate>='"
                                                + EndDay.substring(0, 4)
                                                + "-01-01' and makedate<='" +
                                                EndDay
                                                + "' and agentgroup ='"
                                                + bSSRS.GetText(c, 2)
                                                + "' and riskcode='"
                                                + dSSRS.GetText(g, 1) + "')";
                                        strArr[7] = aExeSQL.getOneValue(csql);
                                        data[7] += Double.parseDouble(strArr[7]);
                                        //累计撤单
                                        String esql =
                                                "select nvl(sum(transmoney),0)/10000 from Lacommision where branchtype='3' and tmakeDate>='"
                                                + EndDay.substring(0, 4)
                                                + "-01-01' and tmakeDate<='" +
                                                EndDay
                                                + "' and branchattr like '"
                                                + bSSRS.GetText(c, 3)
                                                + "%' and riskcode='"
                                                + dSSRS.GetText(g, 1)
                                                + "' and transtype='WT'";
                                        strArr[8] = aExeSQL.getOneValue(esql);
                                        data[8] += Double.parseDouble(strArr[8]);

                                        if (g != 1)
                                        {
                                            strArr[5] = ""; //期内组承保
                                        }
                                        else
                                        {
                                            data[5] +=
                                                    Double.parseDouble(strArr[5]); //组承保合计
                                        }
                                        tlistTable.add(strArr);
                                        strArr = new String[9];
                                    }
                                }
                            }
                        }
                    }
                }
                //分公司合计
                strArr[0] = "分公司小计";
                strArr[1] = "";
                System.out.println(111);
                System.out.println(data[2]);
                strArr[2] = new DecimalFormat("0").format(data[2]);
                System.out.println(strArr[2]);
                data1[2] += data[2];
                for (int i = 4; i <= 8; i++)
                {
                    strArr[i] = String.valueOf(data[i]);
                    data1[i] += data[i];
                }
                strArr[3] = "";
                tlistTable.add(strArr);
                data = new double[9];
                strArr = new String[9];
            }

            strArr = new String[9];
            strArr[0] = "总计";
            strArr[1] = "";
            System.out.println(222);
            strArr[2] = new DecimalFormat("0").format(data1[2]);
            System.out.println(strArr[2]);
            for (int i = 4; i <= 8; i++)
            {
                strArr[i] = String.valueOf(data1[i]);
            }
            strArr[3] = "";
            tlistTable.add(strArr);
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("BankBranchattr.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
            //        xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankBranchattrBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    public static void main(String[] args)
    {
    }
}