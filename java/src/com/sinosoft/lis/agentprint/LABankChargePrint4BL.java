package com.sinosoft.lis.agentprint;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.FileWriter;
import java.io.*;
import java.text.DecimalFormat;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class LABankChargePrint4BL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mManageCom = null;
    private String mAgentCom = null;
    private String mWageNo = null;

    private String mOutXmlPath = null;
    //private String mtype = null;

    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();
    SSRS tSSRS=new SSRS();
    public LABankChargePrint4BL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        mSql = "select distinct a.AgentCom,  c.AppntNo,b.contno,"
        	 +"(select polapplydate from lcpol where contno=b.contno and riskcode=b.riskcode),c.CValiDate,"
        	 +"b.riskcode,b.transmoney,b.chargerate,b.charge,current date  "
        	  + " from LJAGet a,lacharge b,lacommision c "
        	  + " where 1=1 and a.OtherNo=b.receiptno  and b.commisionsn=c.commisionsn "
        	  + " and (a.finstate != 'FC' or a.finstate is null) "
        	  + " and a.ConfDate is null and a.EnterAccDate is null and (a.bankonthewayflag = '0' or a.bankonthewayflag is null) "
        	  + " and b.charge<>0 and a.Paymode <> '4' "
        	  +" and a.ManageCom like '"+mManageCom+"%' ";
        if (mAgentCom != null && !mAgentCom.equals("")) {
        mSql +=" and a.AgentCom like '"+mAgentCom+"%'";
        }
        mSql +=" and a.makedate>=(select startdate from lastatsegment where yearmonth="+mWageNo+" and stattype='5') ";
        mSql +=" and a.makedate<=(select enddate from lastatsegment where yearmonth="+mWageNo+" and stattype='5') ";
        mSql +=" and a.OtherNoType in ('AC','BC') ";
        mSql +=" order by a.AgentCom,  c.AppntNo,b.contno with ur ";
        
        System.out.println(mOutXmlPath);
        tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LABankChargePrint4BL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        if(tSSRS.getMaxRow()==0)
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LABankChargePrint4BL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有符合条件的数据，请重新输入条件";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 7][30];
        mToExcel[0][0] = "银保中介业务应付手续费余额明细表";
        mToExcel[1][0] = "统计年月："+mWageNo+"";
        
        mToExcel[2][0] = "银行代理网点名称";
        mToExcel[2][1] = "投保人";
        mToExcel[2][2] = "保单编号/批单号";
        mToExcel[2][3] = "投保日期";        
        mToExcel[2][4] = "生效日期";
        mToExcel[2][5] = "险种代码";
        mToExcel[2][6] = "保费";
        mToExcel[2][7] = "提取比例";
        mToExcel[2][8] = "手续费";
        mToExcel[2][9] = "生成应付记录日期";


        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row +2][col - 1] = tSSRS.GetText(row, col);
            }
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LABankChargePrint4BL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }        
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        mManageCom = (String) tf.getValueByName("ManageCom");
        mAgentCom = (String) tf.getValueByName("AgentCom");
        mWageNo = (String) tf.getValueByName("WageNo");
        if(mManageCom == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "LABankChargePrint4BL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        return true;
    }

  

    /**
    * 执行SQL文查询结果
    * @param sql String
    * @return double
    */
   private double execQuery(String sql)

   {
      Connection conn;
      conn = null;
      conn = DBConnPool.getConnection();

      System.out.println(sql);

      PreparedStatement st = null;
      ResultSet rs = null;
      try {
          if (conn == null)return 0.00;
          st = conn.prepareStatement(sql);
          if (st == null)return 0.00;
          rs = st.executeQuery();
          if (rs.next()) {
              return rs.getDouble(1);
          }
          return 0.00;
      } catch (Exception ex) {
          ex.printStackTrace();
          return -1;
      } finally {
          try {
             if (!conn.isClosed()) {
                 conn.close();
             }
             try {
                 st.close();
                 rs.close();
             } catch (Exception ex2) {
                 ex2.printStackTrace();
             }
             st = null;
             rs = null;
             conn = null;
           } catch (Exception e) {}

      }
     }

    public static void main(String[] args)
    {
        LABankChargePrint4BL LABankChargePrint4BL = new
            LABankChargePrint4BL();
    System.out.println("11111111");
    }
}
