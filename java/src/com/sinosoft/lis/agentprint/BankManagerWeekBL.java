package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class BankManagerWeekBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间,险种
    private GlobalInput mGlobalInput = new GlobalInput();
    private String StartDay = "";
    private String EndDay = "";

    //全局变量
    private String strArr[] = null;
    private double data[] = null;
    private double data1[] = null;
    private ListTable tlistTable = new ListTable();
    //业务处理相关变量
    /** 全局数据 */
    public BankManagerWeekBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        StartDay = (String) cInputData.get(0);
        EndDay = (String) cInputData.get(1);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 2));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "DepartOrderBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String strArr[] = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("BankManagerWeek");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDay", StartDay); //输入制表时间
        texttag.add("EndDay", EndDay);
        strArr = new String[9];
        double data[] = null;
        data = new double[9];
        double data1[] = null;
        data1 = new double[9];
        //经理姓名
        String asql =
                "select name,agentcode from laagent where branchtype='3' and managecom like '" +
                mGlobalInput.ManageCom + "%'";
        ExeSQL aExeSQL = new ExeSQL();
        SSRS aSSRS = new SSRS();
        aSSRS = aExeSQL.execSQL(asql);
        for (int a = 1; a <= aSSRS.getMaxRow(); a++)
        {
            for (int b = 1; b <= aSSRS.getMaxCol(); b++)
            {
                if (b == 1)
                {
                    strArr[0] = aSSRS.GetText(a, 1);
                }
                if (b == 2)
                {
                    //预收保费
                    String bsql =
                            "select nvl(sum(prem),0) from lcpol where makeDate>='" +
                            StartDay + "' and makeDate<='" + EndDay +
                            "' and AgentCode = '" + aSSRS.GetText(a, 2) + "'";
                    ExeSQL bExeSQL = new ExeSQL();
                    strArr[1] = bExeSQL.getOneValue(bsql);
                    data[1] += Double.parseDouble(strArr[1]);
                    //实收保费
                    String csql =
                            "select nvl(sum(transmoney),0) from lacommision where  tmakeDate>='" +
                            StartDay + "' and tmakeDate<='" + EndDay +
                            "' and AgentCode = '" + aSSRS.GetText(a, 2) + "'";
                    ExeSQL cExeSQL = new ExeSQL();
                    strArr[2] = cExeSQL.getOneValue(csql);
                    data[2] += Double.parseDouble(strArr[2]);
                    //累计实收保费
                    //活动网点数
                    //保单件数
                    String dsql =
                            "select count(distinct polno) from lacommision where tmakeDate>='" +
                            StartDay + "' and tmakeDate<='" + EndDay +
                            "' and AgentCode = '" + aSSRS.GetText(a, 2) + "'";
                    ExeSQL dExeSQL = new ExeSQL();
                    strArr[5] = dExeSQL.getOneValue(dsql);
                    data[5] += Double.parseDouble(strArr[5]);
                    //网点数
                    //网均产量
                    //网点活动率
                    tlistTable.add(strArr);
                    strArr = new String[9];
                }
            }
        }

//汇总
        strArr = new String[9];
        strArr[0] = "汇总";
        data1 = new double[9];
        for (int i = 2; i <= 6; i++)
        {
            strArr[i] = String.valueOf(data[i]);
            data1[i] += data[i];
        }
        tlistTable.add(strArr);

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("BankManagerWeek.vts", "printer"); //最好紧接着就初始化xml文档
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
        mResult.addElement(xmlexport);
        System.out.println("end");
        return true;
    }

    /*
       如果一个字符串数字中小数点后有值，则去掉小数点后面数值
     */
    public static String getInt(String Value)
    {
        String result = "";
        boolean mflag = true;
        int m = 0;
        m = Value.lastIndexOf(".");
        if (m == -1)
        {
            result = Value;
        }
        else
        {
            result = Value.substring(0, m);
        }
        return result;
    }

    public static void main(String[] args)
    {
    }
}