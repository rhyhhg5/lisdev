package com.sinosoft.lis.agentprint;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.*;
import java.text.DecimalFormat;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class LAAgentFinListBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    private String mLevel = null;
    private String mStartMonth = null;
    private String mEndMonth = null;
    SSRS tSSRS=new SSRS();
    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    public LAAgentFinListBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LAAgentFinListBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 4][50];

        mToExcel[0][2] = "个人收入明细汇总表" + "   统计时间："+mStartMonth+"——"+mEndMonth+"";
        
        mToExcel[1][0]="管理机构 ";        
        mToExcel[1][1]="人员编码";
        mToExcel[1][2]="人员姓名";
        mToExcel[1][3]="综合拓展佣金";
        mToExcel[1][4]="首佣";                   
        mToExcel[1][5]="续佣";                   
        mToExcel[1][6]="续期服务奖 ";               
        mToExcel[1][7]="孤儿单服务奖金";              
        mToExcel[1][8]="绩优达标奖";                
        mToExcel[1][9]="健代产佣金";
        mToExcel[1][10]="健代寿佣金";
        mToExcel[1][11]="岗位津贴";
        mToExcel[1][12]="季度奖";         
        mToExcel[1][13]="增员奖";         
        mToExcel[1][14]="晋升奖";         
        mToExcel[1][15]="职务津贴";        
        mToExcel[1][16]="管理津贴";        
        mToExcel[1][17]="直接养成津贴";      
        mToExcel[1][18]="间接养成津贴";      
        mToExcel[1][19]="培训津贴（筹）";          
        mToExcel[1][20]="财务支持费用（筹）";   
        //mToExcel[1][18]="财补加款";
        mToExcel[1][21]="加款（含财补加款）"; 
        mToExcel[1][22]="差勤扣款";                 
        mToExcel[1][23]="养老金";                  
        mToExcel[1][24]="个人所得税";                
        mToExcel[1][25]="个人增值税";                
        mToExcel[1][26]="个人城建税";                
        mToExcel[1][27]="教育费附加税";               
        mToExcel[1][28]="地方教育费附加税";             
        mToExcel[1][29]="其它附加税";                
        mToExcel[1][30]="扣款 ";                  
        mToExcel[1][31]="上次佣金余额";               
        mToExcel[1][32]="实际收入";                 
                                                
            

        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row +1][col - 1] = tSSRS.GetText(row, col);
            }
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentFinListBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        //mLevel = (String) tf.getValueByName("Level");
        mStartMonth = (String) tf.getValueByName("StartMonth");
        mEndMonth = (String) tf.getValueByName("EndMonth");
        if(mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentFinListBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }
    private String dealsum(int pmLength,int pmArrNum)
           {
             String tReturnValue = "";
             DecimalFormat tDF = new DecimalFormat("0.######");
             String tSQL = "select 0";

             for(int i=1;i<pmLength+1;i++)
             {
                 tSQL += " + " + tSSRS.GetText(i, pmArrNum+1);
             }

             tSQL += " + 0 from dual";

             tReturnValue = "" + tDF.format(execQuery(tSQL));

             return tReturnValue;
        }
   
    
  
        /**
         * 执行SQL文查询结果
         * @param sql String
         * @return double
         */
        private double execQuery(String sql)

        {
           Connection conn;
           conn = null;
           conn = DBConnPool.getConnection();

           System.out.println(sql);

           PreparedStatement st = null;
           ResultSet rs = null;
           try {
               if (conn == null)return 0.00;
               st = conn.prepareStatement(sql);
               if (st == null)return 0.00;
               rs = st.executeQuery();
               if (rs.next()) {
                   return rs.getDouble(1);
               }
               return 0.00;
           } catch (Exception ex) {
               ex.printStackTrace();
               return -1;
           } finally {
               try {
                  if (!conn.isClosed()) {
                      conn.close();
                  }
                  try {
                      st.close();
                      rs.close();
                  } catch (Exception ex2) {
                      ex2.printStackTrace();
                  }
                  st = null;
                  rs = null;
                  conn = null;
                } catch (Exception e) {}

           }
     }

    public static void main(String[] args)
    {
        LAAgentFinListBL LAAgentFinListBL = new            LAAgentFinListBL();
    System.out.println("11111111");
    }
}
