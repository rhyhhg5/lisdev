package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PolMonthBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private String Month = "";

    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";

    //业务处理相关变量

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public PolMonthBL()
    {
    }

    /**
            传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT") && !cOperate.equals("CONFIRM"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (cOperate.equals("PRINT"))
        {
            if (!queryData())
            {
                return false;
            }
        }
        if (cOperate.equals("CONFIRM"))
        {
            if (!queryDataOther())
            {
                return false;
            }
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        Month = (String) cInputData.get(0);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "PolMonthBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String[] strArr = null;
        double[] data = new double[21]; //总公司
        String[] mmDay = PubFun.calFLDate(Month.substring(0, 4) + "-"
                                          + Month.substring(4, 6) + "-01");
        String FirDay = Month.substring(0, 4) + "-01-01";
        FDate fDate = new FDate();
        java.util.Date First = fDate.getDate(FirDay);
        java.util.Date End = fDate.getDate(mmDay[1]);
        ListTable tlistTable = new ListTable();
        tlistTable.setName("PolMonth");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("Day",
                    Month.substring(0, 4) + "年" + Month.substring(4, 6) + "月"); //输入制表时间
        double fenziadd = 0;
        double fenmuadd = 0;

        //查询
        strArr = new String[21];
        //营业单位
        String asql = "select ComCode, Name from ldcom where length(trim(comcode))=8  and comcode like '86%' order by comcode ";
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);

            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(asql);
            for (int b = 1; b <= aSSRS.getMaxRow(); b++)
            {
                for (int a = 1; a <= aSSRS.getMaxCol(); a++)
                {
                    if (a == 2)
                    { //营业机构
                        strArr[0] = aSSRS.GetText(b, 2);
                    }
                    if (a == 1)
                    {
                        //月末累计人力
                        String bsql =
                                "select count(*) from LAAgent where (OutWorkDate is null or OutWorkDate>'"
                                + mmDay[1] + "') and EmployDate<='" + mmDay[1]
                                + "' and agentgroup in (select agentGroup from LABranchGroup where BranchType='1' and branchattr like '"
                                + aSSRS.GetText(b, 1) + "%')";
                        double mo = Double.parseDouble(aExeSQL.getOneValue(bsql));
                        strArr[1] = String.valueOf(mo);
                        data[1] += mo;
                        //月末累计总保费
                        String csql =
                                "select NVL(sum(transmoney),0)/10000,NVL(sum(standprem),0)/10000,NVL(sum(calcount),0)"
                                + " from lacommision where tmakedate>='" +
                                FirDay
                                + "' and tmakedate<='" + mmDay[1]
                                + "' and BranchType='1' and branchattr like '"
                                + aSSRS.GetText(b, 1) + "%'";
                        SSRS tSSRS = new SSRS();
                        tSSRS = aExeSQL.execSQL(csql);
                        strArr[2] = tSSRS.GetText(1, 1);
                        data[2] += Double.parseDouble(strArr[2]);
                        //规模保费累计
                        String dsql =
                                "select NVL(sum(transmoney),0)/10000 from lacommision where tmakedate>='"
                                + FirDay + "' and tmakedate<='" + mmDay[1]
                                + "' and BranchType='1' and branchattr like '"
                                + aSSRS.GetText(b, 1) + "%' and paycount<='1'";

                        double scopemoney = Double.parseDouble(aExeSQL.
                                getOneValue(
                                        dsql));
                        strArr[3] = String.valueOf(scopemoney);
                        data[3] += scopemoney;
                        //期交比例
                        String esql =
                                "select NVL(sum(transmoney),0)/10000 from lacommision where tmakedate>='"
                                + FirDay + "' and tmakedate<='" + mmDay[1]
                                + "' and branchattr like '" +
                                aSSRS.GetText(b, 1)
                                +
                                "%' and BranchType='1' and payintv <> '0' and paycount<='1'";
                        double oncemoney = Double.parseDouble(aExeSQL.
                                getOneValue(
                                        esql));
                        if (strArr[3].equals("0.0"))
                        {
                            strArr[4] = "0";
                            data[4] += 0;
                        }
                        else
                        {
                            strArr[4] = String.valueOf(oncemoney / scopemoney);
                            data[4] += oncemoney; //期交保费
                        }

                        //标准保费
                        strArr[5] = tSSRS.GetText(1, 2);
                        data[5] += Double.parseDouble(strArr[5]);
                        //件数
                        strArr[6] = tSSRS.GetText(1, 3);
                        data[6] += Double.parseDouble(strArr[6]);
                        //退保率
                        String fsql =
                                "select nvl(sum(prem),0)/10000 from lbpol a where makedate>='"
                                + FirDay + "' and makedate<='" + mmDay[1]
                                +
                                "' and exists (select 'X' from LABranchGroup where branchattr like '"
                                + aSSRS.GetText(b, 1)
                                + "%' and BranchType='1' and agentGroup=a.agentGroup) and exists (select 'X' from lpedormain where EdorType<>'WT' and polno=a.polno)";
                        double cancel = Double.parseDouble(aExeSQL.getOneValue(
                                fsql));
                        if (strArr[3].equals("0.0"))
                        {
                            strArr[7] = "0";
                            data[7] += 0;
                        }
                        else
                        {
                            strArr[7] = String.valueOf(cancel / scopemoney); //退保保费/规模承保保费
                            data[7] += cancel;
                        }

                        //续期保费
                        String gsql =
                                "select NVL(sum(transmoney),0)/10000 from lacommision where tmakedate>='"
                                + FirDay + "' and tmakedate<='" + mmDay[1]
                                + "' and BranchType='1' and branchattr like '"
                                + aSSRS.GetText(b, 1) + "%' and paycount>'1'";
                        strArr[8] = aExeSQL.getOneValue(gsql);
                        data[8] += Double.parseDouble(strArr[8]);
                        //续期率
                        String msql = "SELECT BranchCONTINUE_RATE('" + FirDay
                                      + "','" + mmDay[1] + "','" +
                                      aSSRS.GetText(b, 1)
                                      +
                                      "') from ldsysvar where sysvar = 'onerow'";
                        strArr[9] = aExeSQL.getOneValue(msql);
                        //人均标保
                        if (strArr[1].equals("0") || strArr[1].equals("0.0"))
                        {
                            strArr[10] = "0";
                        }
                        else
                        {
                            strArr[10] = String.valueOf(Double.parseDouble(
                                    strArr[5]) / mo);
                        }

                        //人均件数
                        if (strArr[1].equals("0") || strArr[1].equals("0.0"))
                        {
                            strArr[11] = "0";
                        }
                        else
                        {
                            strArr[11] = String.valueOf(Double.parseDouble(
                                    strArr[6]) / mo);
                        }

                        //件均保费
                        if (Double.parseDouble(strArr[6]) == 0)
                        {
                            strArr[12] = "0";
                        }
                        else
                        {
                            strArr[12] = String.valueOf(Double.parseDouble(
                                    strArr[5]) / Double.parseDouble(strArr[6]));
                        }

                        //活动率
                        int num1 = 0;
                        String rsql =
                                "select count(distinct agentcode) from lacommision where branchattr like '"
                                + aSSRS.GetText(b, 1)
                                + "%' and BranchType='1' and tmakedate>='" +
                                FirDay
                                + "' and tmakedate<='" + mmDay[1] + "'";

                        num1 = Integer.parseInt(aExeSQL.getOneValue(rsql));
                        if (strArr[1].equals("0") || strArr[1].equals("0.0"))
                        {
                            strArr[13] = "0";
                        }
                        else
                        {
                            strArr[13] = String.valueOf(num1 / mo);
                        }
                        data[13] += num1; //有业绩的人数
                        //人均产能
                        if (num1 == 0)
                        {
                            strArr[14] = "0";
                        }
                        else
                        {
                            strArr[14] = String.valueOf(Double.parseDouble(
                                    strArr[5]) / num1); //标准保费、有业绩人力
                        }

                        //持证率
                        int num2 = 0;
                        String hsql =
                                "select count(*) from LAAgent where QuafNo is not null and agentstate in ('01','02') and EmployDate>='"
                                + FirDay + "' and EmployDate <='" + mmDay[1]
                                + "' and agentgroup in (select agentGroup from LABranchGroup where BranchType='1' and branchattr like '"
                                + aSSRS.GetText(b, 1) + "%')";
                        num2 = Integer.parseInt(aExeSQL.getOneValue(hsql));
                        if (strArr[1].equals("0") || strArr[1].equals("0.0"))
                        {
                            strArr[15] = "0";
                        }
                        else
                        {
                            strArr[15] = String.valueOf(num2 / mo);
                        }
                        data[15] += num2; //持证人数
                        //净增率
                        int num3 = 0;
                        String isql =
                                "select count(*) from LAAgent where EmployDate>='"
                                + FirDay + "' and EmployDate <='" + mmDay[1]
                                + "' and agentgroup in (select agentGroup from LABranchGroup where BranchType='1' and branchattr like '"
                                + aSSRS.GetText(b, 1) + "%')";
                        String zsql =
                                "select count(*) from laagent where OutWorkDate>='"
                                + FirDay + "' and OutWorkDate<='" + mmDay[1]
                                + "' and AgentState>='03' and agentgroup in (select agentGroup from LABranchGroup where BranchType='1' and branchattr like '"
                                + aSSRS.GetText(b, 1) + "%')";

                        num3 = Integer.parseInt(aExeSQL.getOneValue(isql))
                               - Integer.parseInt(aExeSQL.getOneValue(zsql)); //增员人数
                        if (strArr[1].equals("0") || strArr[1].equals("0.0"))
                        {
                            strArr[16] = "0";
                        }
                        else
                        {
                            strArr[16] = String.valueOf(num3 / mo);
                        }
                        data[16] += num3; //增员人数
                        //脱落率
                        int num4 = 0;
                        num4 = Integer.parseInt(aExeSQL.getOneValue(zsql)); //离职人数
                        if (strArr[1].equals("0") || strArr[1].equals("0.0"))
                        {
                            strArr[17] = "0";
                        }
                        else
                        {
                            strArr[17] = String.valueOf(num4 / mo);
                        }
                        data[17] += num4; //离职人数
                        //有效人力
                        String ssql =
                                "select count(count(*)) from lacommision where tmakedate>='"
                                + FirDay + "' and tmakedate<='" + mmDay[1]
                                + "' and BranchType='1' and branchattr like '"
                                + aSSRS.GetText(b, 1)
                                +
                                "%' group by agentcode having sum(transmoney)>=2000";
                        strArr[18] = aExeSQL.getOneValue(ssql);
                        data[18] += Double.parseDouble(strArr[18]);
                        //任务计划
                        String usql =
                                "select nvl(sum(planvalue),0)/10000 from laplan where plantype=2 and PlanPeriodUnit=1 and PlanPeriod='"
                                + Month + "' and planobject = '" +
                                aSSRS.GetText(b, 1)
                                + "' and BranchType='1'";
                        String zan = aExeSQL.getOneValue(usql);
                        if ((zan != null) && !zan.equals(""))
                        {
                            strArr[19] = zan;
                            data[19] += Double.parseDouble(zan);
                        }
                        else
                        {
                            strArr[19] = "0";
                            data[19] += 0;
                        }

                        //达成率
                        if (strArr[19].equals("0") || strArr[19].equals("0.0"))
                        {
                            strArr[20] = "0";
                            data[20] += 0;
                        }
                        else
                        { //标准保费/计划任务
                            strArr[20] = String.valueOf(Double.parseDouble(
                                    strArr[5]) / (Double.parseDouble(zan)));
                            data[20] +=
                                    (Double.parseDouble(strArr[5]) / (Double
                                    .parseDouble(zan)));
                        }
                    }
                }
                tlistTable.add(strArr);
                strArr = new String[21];
            }

            //展业机构：总计
            strArr = new String[21];
            strArr[0] = "全系统";
            for (int u = 1; u <= 3; u++)
            {
                strArr[u] = String.valueOf(data[u]);
            }

            //趸交比例
            strArr[4] = String.valueOf(data[4] / data[3]);
            strArr[5] = String.valueOf(data[5]);
            strArr[6] = String.valueOf(data[6]);
            //退保率
            strArr[7] = String.valueOf(data[4] / data[3]);
            strArr[8] = String.valueOf(data[8]);
            //续期率
            String ysql = "SELECT BranchCONTINUE_RATE('" + FirDay + "','"
                          + mmDay[1] +
                          "','86') from ldsysvar where sysvar = 'onerow'";
            strArr[9] = aExeSQL.getOneValue(ysql);

            //人均标保
            if (data[1] == 0)
            {
                strArr[10] = "0";
            }
            else
            {
                strArr[10] = String.valueOf(data[3] / data[1]);
            }

            //人均件数
            if (data[1] == 0)
            {
                strArr[11] = "0";
            }
            else
            {
                strArr[11] = String.valueOf(data[6] / data[1]);
            }

            //件均标保
            if (data[6] == 0)
            {
                strArr[12] = "0";
            }
            else
            {
                strArr[12] = String.valueOf(data[3] / data[6]);
            }

            //活动率
            if (data[1] == 0)
            {
                strArr[13] = "0";
            }
            else
            {
                strArr[13] = String.valueOf(data[13] / data[1]);
            }

            //人均产能
            if (data[13] == 0)
            {
                strArr[14] = "0";
            }
            else
            {
                strArr[14] = String.valueOf(data[3] / data[13]);
            }

            //持证率
            if (data[1] == 0)
            {
                strArr[15] = "0";
            }
            else
            {
                strArr[15] = String.valueOf(data[15] / data[1]);
            }

            //净增率
            if (data[1] == 0)
            {
                strArr[16] = "0";
            }
            else
            {
                strArr[16] = String.valueOf(data[16] / data[1]);
            }

            //脱落率
            if (data[1] == 0)
            {
                strArr[17] = "0";
            }
            else
            {
                strArr[17] = String.valueOf(data[17] / data[1]);
            }

            //有效人力
            strArr[18] = String.valueOf(data[18]);

            //任务计划
            strArr[19] = String.valueOf(data[19]);
            //达成率
            if (data[19] == 0)
            {
                strArr[20] = "0";
            }
            else
            {
                strArr[20] = String.valueOf(data[3] / data[19]);
            }

            tlistTable.add(strArr);
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("PolMonth.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
            //     xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);

            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PolMonthBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    private boolean queryDataOther()
    {
        String[] strArr = null;
        double[] data = new double[21]; //总公司
        String[] mmDay = PubFun.calFLDate(Month.substring(0, 4) + "-"
                                          + Month.substring(4, 6) + "-01");
        String FirDay = Month.substring(0, 4) + "-01-01";
        FDate fDate = new FDate();
        java.util.Date First = fDate.getDate(FirDay);
        java.util.Date End = fDate.getDate(mmDay[1]);
        ListTable tlistTable = new ListTable();
        tlistTable.setName("PolMonth");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("Day",
                    Month.substring(0, 4) + "年" + Month.substring(4, 6) + "月"); //输入制表时间
        double fenziadd = 0;
        double fenmuadd = 0;

        //查询
        strArr = new String[21];
        //营业单位
        String asql = "select Code,trim(Name) from view_ldcom_labranchgroup2";
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        ExeSQL aExeSQL = new ExeSQL(conn);

        SSRS aSSRS = new SSRS();
        aSSRS = aExeSQL.execSQL(asql);
        for (int b = 1; b <= aSSRS.getMaxRow(); b++)
        {
            for (int a = 1; a <= aSSRS.getMaxCol(); a++)
            {
                if (a == 2)
                { //营业机构
                    strArr[0] = aSSRS.GetText(b, 2);
                }
                if (a == 1)
                {
                    //月末累计人力
                    String bsql =
                            "select count(*) from LAAgent where (OutWorkDate is null or OutWorkDate>'"
                            + mmDay[1] + "') and EmployDate<='" + mmDay[1]
                            + "' and agentgroup in (select agentGroup from LABranchGroup where BranchType='1' and branchattr like '"
                            + aSSRS.GetText(b, 1) + "%')";
                    double mo = Double.parseDouble(aExeSQL.getOneValue(bsql));
                    strArr[1] = String.valueOf(mo);
                    data[1] += mo;
                    //月末累计总保费
                    String csql =
                            "select NVL(sum(transmoney),0)/10000,NVL(sum(standprem),0)/10000,NVL(sum(calcount),0)"
                            + " from lacommision where tmakedate>='" + FirDay
                            + "' and tmakedate<='" + mmDay[1]
                            + "' and BranchType='1' and branchattr like '"
                            + aSSRS.GetText(b, 1) + "%'";
                    SSRS tSSRS = new SSRS();
                    tSSRS = aExeSQL.execSQL(csql);
                    strArr[2] = tSSRS.GetText(1, 1);
                    data[2] += Double.parseDouble(strArr[2]);
                    //规模保费累计
                    String dsql =
                            "select NVL(sum(transmoney),0)/10000 from lacommision where tmakedate>='"
                            + FirDay + "' and tmakedate<='" + mmDay[1]
                            + "' and BranchType='1' and branchattr like '"
                            + aSSRS.GetText(b, 1) + "%' and paycount<='1'";

                    double scopemoney = Double.parseDouble(aExeSQL.getOneValue(
                            dsql));
                    strArr[3] = String.valueOf(scopemoney);
                    data[3] += scopemoney;
                    //期交比例
                    String esql =
                            "select NVL(sum(transmoney),0)/10000 from lacommision where tmakedate>='"
                            + FirDay + "' and tmakedate<='" + mmDay[1]
                            + "' and branchattr like '" + aSSRS.GetText(b, 1)
                            +
                            "%' and BranchType='1' and payintv <> '0' and paycount<='1'";
                    double oncemoney = Double.parseDouble(aExeSQL.getOneValue(
                            esql));
                    if (strArr[3].equals("0.0"))
                    {
                        strArr[4] = "0";
                        data[4] += 0;
                    }
                    else
                    {
                        strArr[4] = String.valueOf(oncemoney / scopemoney);
                        data[4] += oncemoney; //期交保费
                    }

                    //标准保费
                    strArr[5] = tSSRS.GetText(1, 2);
                    data[5] += Double.parseDouble(strArr[5]);
                    //件数
                    strArr[6] = tSSRS.GetText(1, 3);
                    data[6] += Double.parseDouble(strArr[6]);
                    //退保率
                    String fsql =
                            "select nvl(sum(prem),0)/10000 from lbpol a where makedate>='"
                            + FirDay + "' and makedate<='" + mmDay[1]
                            +
                            "' and exists (select 'X' from LABranchGroup where branchattr like '"
                            + aSSRS.GetText(b, 1)
                            + "%' and BranchType='1' and agentGroup=a.agentGroup) and exists (select 'X' from lpedormain where EdorType<>'WT' and polno=a.polno)";
                    double cancel = Double.parseDouble(aExeSQL.getOneValue(fsql));
                    if (strArr[3].equals("0.0"))
                    {
                        strArr[7] = "0";
                        data[7] += 0;
                    }
                    else
                    {
                        strArr[7] = String.valueOf(cancel / scopemoney);
                        data[7] += cancel;
                    }

                    //续期保费
                    String gsql =
                            "select NVL(sum(transmoney),0)/10000 from lacommision where tmakedate>='"
                            + FirDay + "' and tmakedate<='" + mmDay[1]
                            + "' and BranchType='1' and branchattr like '"
                            + aSSRS.GetText(b, 1) + "%' and paycount>'1'";
                    strArr[8] = aExeSQL.getOneValue(gsql);
                    data[8] += Double.parseDouble(strArr[8]);
                    //续期率
                    String msql = "SELECT BranchCONTINUE_RATE('" + FirDay
                                  + "','" + mmDay[1] + "','" +
                                  aSSRS.GetText(b, 1)
                                  + "') from ldsysvar where sysvar = 'onerow'";
                    strArr[9] = aExeSQL.getOneValue(msql);
                    //人均标保
                    if (strArr[1].equals("0") || strArr[1].equals("0.0"))
                    {
                        strArr[10] = "0";
                    }
                    else
                    {
                        strArr[10] = String.valueOf(Double.parseDouble(
                                strArr[5]) / mo);
                    }

                    //人均件数
                    if (strArr[1].equals("0") || strArr[1].equals("0.0"))
                    {
                        strArr[11] = "0";
                    }
                    else
                    {
                        strArr[11] = String.valueOf(Double.parseDouble(
                                strArr[6]) / mo);
                    }

                    //件均保费
                    if (Double.parseDouble(strArr[6]) == 0)
                    {
                        strArr[12] = "0";
                    }
                    else
                    {
                        strArr[12] = String.valueOf(Double.parseDouble(
                                strArr[5]) / Double.parseDouble(strArr[6]));
                    }

                    //活动率
                    int num1 = 0;
                    String rsql =
                            "select count(distinct agentcode) from lacommision where branchattr like '"
                            + aSSRS.GetText(b, 1)
                            + "%' and BranchType='1' and tmakedate>='" + FirDay
                            + "' and tmakedate<='" + mmDay[1] + "'";

                    num1 = Integer.parseInt(aExeSQL.getOneValue(rsql));
                    if (strArr[1].equals("0") || strArr[1].equals("0.0"))
                    {
                        strArr[13] = "0";
                    }
                    else
                    {
                        strArr[13] = String.valueOf(num1 / mo);
                    }
                    data[13] += num1; //有业绩的人数
                    //人均产能
                    if (num1 == 0)
                    {
                        strArr[14] = "0";
                    }
                    else
                    {
                        strArr[14] = String.valueOf(Double.parseDouble(
                                strArr[5]) / num1); //标准保费、有业绩人力
                    }

                    //持证率
                    int num2 = 0;
                    String hsql =
                            "select count(*) from LAAgent where QuafNo is not null and agentstate in ('01','02') and EmployDate>='"
                            + FirDay + "' and EmployDate <='" + mmDay[1]
                            + "' and agentgroup in (select agentGroup from LABranchGroup where BranchType='1' and branchattr like '"
                            + aSSRS.GetText(b, 1) + "%')";
                    num2 = Integer.parseInt(aExeSQL.getOneValue(hsql));
                    if (strArr[1].equals("0") || strArr[1].equals("0.0"))
                    {
                        strArr[15] = "0";
                    }
                    else
                    {
                        strArr[15] = String.valueOf(num2 / mo);
                    }
                    data[15] += num2; //持证人数
                    //净增率
                    int num3 = 0;
                    String isql =
                            "select count(*) from LAAgent where EmployDate>='"
                            + FirDay + "' and EmployDate <='" + mmDay[1]
                            + "' and agentgroup in (select agentGroup from LABranchGroup where BranchType='1' and branchattr like '"
                            + aSSRS.GetText(b, 1) + "%')";
                    String zsql =
                            "select count(*) from laagent where OutWorkDate>='"
                            + FirDay + "' and OutWorkDate<='" + mmDay[1]
                            + "' and AgentState>='03' and agentgroup in (select agentGroup from LABranchGroup where BranchType='1' and branchattr like '"
                            + aSSRS.GetText(b, 1) + "%')";

                    num3 = Integer.parseInt(aExeSQL.getOneValue(isql))
                           - Integer.parseInt(aExeSQL.getOneValue(zsql)); //增员人数
                    if (strArr[1].equals("0") || strArr[1].equals("0.0"))
                    {
                        strArr[16] = "0";
                    }
                    else
                    {
                        strArr[16] = String.valueOf(num3 / mo);
                    }
                    data[16] += num3; //增员人数
                    //脱落率
                    int num4 = 0;
                    num4 = Integer.parseInt(aExeSQL.getOneValue(zsql)); //离职人数
                    if (strArr[1].equals("0") || strArr[1].equals("0.0"))
                    {
                        strArr[17] = "0";
                    }
                    else
                    {
                        strArr[17] = String.valueOf(num4 / mo);
                    }
                    data[17] += num4; //离职人数
                    //有效人力
                    String ssql =
                            "select count(count(*)) from lacommision where tmakedate>='"
                            + FirDay + "' and tmakedate<='" + mmDay[1]
                            + "' and BranchType='1' and branchattr like '"
                            + aSSRS.GetText(b, 1)
                            +
                            "%' group by agentcode having sum(transmoney)>=2000";
                    strArr[18] = aExeSQL.getOneValue(ssql);
                    data[18] += Double.parseDouble(strArr[18]);
                    //任务计划
                    String usql =
                            "select nvl(sum(planvalue),0)/10000 from laplan where plantype=2 and PlanPeriodUnit=1 and PlanPeriod='"
                            + Month + "' and planobject = '" +
                            aSSRS.GetText(b, 1)
                            + "' and BranchType='1'";
                    String zan = aExeSQL.getOneValue(usql);
                    if ((zan != null) && !zan.equals(""))
                    {
                        strArr[19] = zan;
                        data[19] += Double.parseDouble(zan);
                    }
                    else
                    {
                        strArr[19] = "0";
                        data[19] += 0;
                    }

                    //达成率
                    if (strArr[19].equals("0") || strArr[19].equals("0.0"))
                    {
                        strArr[20] = "0";
                        data[20] += 0;
                    }
                    else
                    { //标准保费/计划任务
                        strArr[20] = String.valueOf(Double.parseDouble(
                                strArr[5]) / (Double.parseDouble(zan)));
                        data[20] += (Double.parseDouble(strArr[5]) / (Double
                                .parseDouble(zan)));
                    }
                }
            }
            tlistTable.add(strArr);
            strArr = new String[21];
        }

        try
        {
            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        //展业机构：总计
        strArr = new String[21];
        strArr[0] = "全系统";
        for (int u = 1; u <= 3; u++)
        {
            strArr[u] = String.valueOf(data[u]);
        }

        //趸交比例
        strArr[4] = String.valueOf(data[4] / data[3]);
        strArr[5] = String.valueOf(data[5]);
        strArr[6] = String.valueOf(data[6]);
        //退保率
        strArr[7] = String.valueOf(data[4] / data[3]);
        strArr[8] = String.valueOf(data[8]);
        //续期率
        String ysql = "SELECT BranchCONTINUE_RATE('" + FirDay + "','"
                      + mmDay[1] +
                      "','86') from ldsysvar where sysvar = 'onerow'";
        strArr[9] = aExeSQL.getOneValue(ysql);

        //人均标保
        if (data[1] == 0)
        {
            strArr[10] = "0";
        }
        else
        {
            strArr[10] = String.valueOf(data[3] / data[1]);
        }

        //人均件数
        if (data[1] == 0)
        {
            strArr[11] = "0";
        }
        else
        {
            strArr[11] = String.valueOf(data[6] / data[1]);
        }

        //件均标保
        if (data[6] == 0)
        {
            strArr[12] = "0";
        }
        else
        {
            strArr[12] = String.valueOf(data[3] / data[6]);
        }

        //活动率
        if (data[1] == 0)
        {
            strArr[13] = "0";
        }
        else
        {
            strArr[13] = String.valueOf(data[13] / data[1]);
        }

        //人均产能
        if (data[13] == 0)
        {
            strArr[14] = "0";
        }
        else
        {
            strArr[14] = String.valueOf(data[3] / data[13]);
        }

        //持证率
        if (data[1] == 0)
        {
            strArr[15] = "0";
        }
        else
        {
            strArr[15] = String.valueOf(data[15] / data[1]);
        }

        //净增率
        if (data[1] == 0)
        {
            strArr[16] = "0";
        }
        else
        {
            strArr[16] = String.valueOf(data[16] / data[1]);
        }

        //脱落率
        if (data[1] == 0)
        {
            strArr[17] = "0";
        }
        else
        {
            strArr[17] = String.valueOf(data[17] / data[1]);
        }

        //有效人力
        strArr[18] = String.valueOf(data[18]);

        //任务计划
        strArr[19] = String.valueOf(data[19]);
        //达成率
        if (data[19] == 0)
        {
            strArr[20] = "0";
        }
        else
        {
            strArr[20] = String.valueOf(data[3] / data[19]);
        }

        tlistTable.add(strArr);
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PolMonth.vts", "printer"); //最好紧接着就初始化xml文档
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        //    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
        mResult.addElement(xmlexport);
        System.out.println("end");
        return true;
    }

    /*
         如果一个字符串数字中小数点后有值，则去掉小数点后面数值
     */
    public static String getInt(String Value)
    {
        String result = "";
        boolean mflag = true;
        int m = 0;
        m = Value.lastIndexOf(".");
        if (m == -1)
        {
            result = Value;
        }
        else
        {
            result = Value.substring(0, m);
        }
        return result;
    }

    public static void main(String[] args)
    {
        PolNBDepartF1PBL tPolNBDepartF1PBL = new PolNBDepartF1PBL();
    }
}