package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author ZY
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PolListBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private String mDay = "";
    private String mManageCom = "";
    double date2[] = new double[12]; //定义总计的数组

    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    public PolListBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        mDay = (String) cInputData.get(0);
        mManageCom = (String) cInputData.get(1);
//    mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "PolNBBillF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String strArr[] = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("POLLIST");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        String xsql = "select name from LABranchGroup where agentgroup= '" +
                      mManageCom + "' ";

        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);

            texttag.add("Name", aExeSQL.getOneValue(xsql));
            texttag.add("Day", mDay); //输入制表时间
            String mmDay[] = PubFun1.calFLDate(mDay);
            SSRS aSSRS = new SSRS();
            msql = "select branchattr,name,agentgroup from LABranchGroup where EndFlag<>'Y' and BranchType = '1' and BranchLevel = '01' and (state<>'1' or state is null) and upbranch = '" +
                   mManageCom + "' order by agentgroup";
            aSSRS = aExeSQL.execSQL(msql);

            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                double date1[] = new double[6];
                for (int c = 1; c <= aSSRS.getMaxCol(); c++)
                {
                    if (c == 1)
                    {
                        SSRS bSSRS = new SSRS();
                        nsql = "select a.name,a.agentcode from LATree b,LAAgent a where a.agentcode=b.agentcode "
                               + "and a.agentstate<'03' and a.branchcode in (select agentgroup from LABranchGroup where BranchAttr like '"
                               + aSSRS.GetText(a, 1) +
                               "%') order by a.branchcode asc,b.agentgrade desc";
                        bSSRS = aExeSQL.execSQL(nsql);
                        for (int b = 1; b <= bSSRS.getMaxRow(); b++)
                        {
                            for (int d = 1; d <= bSSRS.getMaxCol(); d++)
                            {
                                if (d == 1)
                                { //展业机构：个人名称
                                    strArr = new String[6];
                                    strArr[0] = bSSRS.GetText(b, 1);
                                }
                                if (d == 2)
                                {
                                    //月初人力
                                    strArr[1] = "";
                                    //日预收保费
                                    String bsql =
                                            "select NVL(sum(paymoney),0) from ljtempfee_lmriskapp4 where makedate ='" +
                                            mDay + "' and agentcode= '" +
                                            bSSRS.GetText(b, 2) + "'";
                                    strArr[2] = aExeSQL.getOneValue(bsql);
                                    date1[2] += Double.parseDouble(strArr[2]);
                                    String csql = "select CALPIECEGT('" + mDay +
                                                  "','" + mDay + "','" +
                                                  bSSRS.GetText(b, 2) +
                                                  "') from ldsysvar where sysvar='onerow'";

                                    //件数
                                    strArr[3] = aExeSQL.getOneValue(csql);
                                    date1[3] += Double.parseDouble(strArr[3]);
                                    //月预收保费
                                    String fsql =
                                            "select NVL(sum(paymoney),0) from ljtempfee_lmriskapp4 where makedate >='" +
                                            mmDay[0] + "' and makedate <='" +
                                            mDay + "' and agentcode= '" +
                                            bSSRS.GetText(b, 2) + "'";
                                    strArr[4] = aExeSQL.getOneValue(fsql);
                                    date1[4] += Double.parseDouble(strArr[4]);
                                    String gsql = "select CALPIECEGT('" +
                                                  mmDay[0] + "','" + mDay +
                                                  "','" + bSSRS.GetText(b, 2) +
                                                  "') from ldsysvar where sysvar='onerow'";

                                    //月件数
                                    strArr[5] = aExeSQL.getOneValue(gsql);
                                    date1[5] += Double.parseDouble(strArr[5]);
                                    tlistTable.add(strArr);
                                }
                            }
                        }
                    }
                    if (c == 2)
                    {
                        //展业机构：组一级名
                        strArr = new String[6];
                        strArr[0] = aSSRS.GetText(a, 2);
                    }
                    if (c == 3)
                    {
                        //组级人力资源
                        String aasql =
                                "select count(*) from LAAgent where EmployDate < '" +
                                mmDay[0] + "' and BranchCode = '" +
                                aSSRS.GetText(a, 3) + "'";
                        strArr[1] = aExeSQL.getOneValue(aasql);
                        strArr[2] = String.valueOf(date1[2]);
                        strArr[3] = String.valueOf(date1[3]);
                        strArr[4] = String.valueOf(date1[4]);
                        strArr[5] = String.valueOf(date1[5]);
                        tlistTable.add(strArr);
                        date2[1] += Double.parseDouble(strArr[1]);
                        date2[2] += Double.parseDouble(strArr[2]);
                        date2[3] += Double.parseDouble(strArr[3]);
                        date2[4] += Double.parseDouble(strArr[4]);
                        date2[5] += Double.parseDouble(strArr[5]);
                        strArr = new String[12];
                        strArr[0] = "";
                        strArr[1] = "";
                        strArr[2] = "";
                        strArr[3] = "";
                        strArr[4] = "";
                        strArr[5] = "";
                        tlistTable.add(strArr);
                    }
                }
            }

            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PolListBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }

        strArr = new String[6];
        strArr[0] = "总计";
        strArr[1] = String.valueOf(date2[1]);
        strArr[2] = String.valueOf(date2[2]);
        strArr[3] = String.valueOf(date2[3]);
        strArr[4] = String.valueOf(date2[4]);
        strArr[5] = String.valueOf(date2[5]);
        tlistTable.add(strArr);
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PolList.vts", "printer"); //最好紧接着就初始化xml文档
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
        mResult.addElement(xmlexport);
        System.out.println("end");
        return true;
    }

    public static void main(String[] args)
    {
        PolNBBillF1PBL tWageQueryBL = new PolNBBillF1PBL();
    }
}