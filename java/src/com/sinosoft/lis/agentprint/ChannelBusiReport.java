package com.sinosoft.lis.agentprint;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class ChannelBusiReport
{
    public GlobalInput mGI = new GlobalInput();
    public String mStartDate = "";
    public String mEndDate = "";
    public VData mResult = new VData();
    public CErrors mErrors = new CErrors();

    public ChannelBusiReport()
    {
    }

    public static void main(String[] args)
    {
        ChannelBusiReport ChannelBusiReport1 = new ChannelBusiReport();
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86110000";
        tGI.ManageCom = "86110000";
        tGI.Operator = "aa";
        VData tInputData = new VData();
        tInputData.clear();
        tInputData.addElement(tGI);
        tInputData.add(0, "2003-11-8");
        tInputData.add(1, "2003-12-8");
        if (!ChannelBusiReport1.getInputData(tInputData))
        {
            System.out.println("-----getinputdata failed!------");
        }
        if (!ChannelBusiReport1.prepareData())
        {
            System.out.println("-------prepareData failed!------");
        }

    }

    public boolean getInputData(VData cInputData)
    {
        //全局变量
        try
        {
            mStartDate = (String) cInputData.getObjectByObjectName("String", 0);
            mEndDate = (String) cInputData.getObjectByObjectName("String", 1);
            mGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput",
                    0);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("getInputData", ex.toString());
            return false;
        }
        System.out.println("---StartDate : " + this.mStartDate);
        System.out.println("---EndDate : " + this.mEndDate);
        return true;
    }

    public boolean prepareData()
    {
        ListTable tListTable = new ListTable();
        tListTable.setName("ChannelBusi");

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        String tStaticDate = "";
        tStaticDate = this.mStartDate.trim() + " 至 " + this.mEndDate.trim();
        texttag.add("StaticDate", tStaticDate); //输入制表时间
        PubFun tPF = new PubFun();

        //取本年度的第一天
        String tYearStart = this.mStartDate.trim().substring(0, 4) + "-1-1";
        System.out.println("---YearStartDate : " + tYearStart);

        //取录入时间的年月代码
        String pattern = "yyyyMMdd";
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        Date tDate = new Date();
        Date tDate1 = new Date();
        try
        {
            tDate = df.parse(this.mStartDate);
            tDate1 = df.parse(this.mEndDate);
        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ChannelBusiReport";
            tError.functionName = "PrepareDate";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);
        }

        String tStartMon = df.format(tDate).trim().substring(0, 6);
        String tEndMon = df.format(tDate1).trim().substring(0, 6);

        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);

            //查询出各个渠道组
            SSRS aSSRS = new SSRS();
            String Sql1 = "select branchattr,name from Labranchgroup "
                          +
                          "where branchtype='3' and branchlevel='3' and (state<>'1' or state is null)"
                          + " and managecom like '" + this.mGI.ManageCom.trim() +
                          "%'";
            int tChannelCount = 0;

            aSSRS = aExeSQL.execSQL(Sql1);
            tChannelCount = aSSRS.getMaxRow();
            if (tChannelCount == 0)
            {
                dealError("prepareData", "渠道组信息查询失败！");
                return false;
            }

            String strArr[][] = new String[tChannelCount][16];
            String eachList[] = null;

            String[] tChannel = new String[tChannelCount];
            String[] tChannelName = new String[tChannelCount];
            float tSum = 0;
            float tSumPlanValue = 0;
            float tSumYearPlan = 0;
            float tTransMoneySum = 0;

            for (int i = 1; i <= tChannelCount; i++)
            {
                System.out.println("--------" + i + "---------");

                tChannel[i - 1] = new String();
                tChannel[i - 1] = aSSRS.GetText(i, 1);
                tChannelName[i - 1] = new String();
                tChannelName[i - 1] = aSSRS.GetText(i, 2);
                System.out.println("------Channel: " + tChannel[i - 1]);
                System.out.println("------ChannelName: " + tChannelName[i - 1]);
                strArr[i - 1][0] = tChannelName[i - 1];

                //统计实收保费,件数，标保
                String Sql2 =
                        "select nvl(sum(transmoney),0),nvl(sum(calcount),0),nvl(sum(standprem),0) from lacommision "
                        + "where  tmakedate >= '" + this.mStartDate.trim()
                        + "' and tmakedate <= '" + this.mEndDate.trim() + "'"
                        + " and trim(branchattr) like '" + tChannel[i - 1].trim() +
                        "%'";
                SSRS bSSRS = new SSRS();
                bSSRS = aExeSQL.execSQL(Sql2);
                strArr[i - 1][1] = bSSRS.GetText(1, 1);
                strArr[i - 1][4] = bSSRS.GetText(1, 2);
                System.out.println("---" + tChannelName[i - 1].trim() +
                                   "--保单件数 ＝ " + strArr[i - 1][4].trim());
                strArr[i - 1][10] = bSSRS.GetText(1, 3);

                //累加实收保费
                tSum += Float.parseFloat(strArr[i - 1][1].trim());

                //达成率
                Sql2 = "select nvl(sum(planvalue),0) from laplan "
                       + "where planperiod >= '" + tStartMon.trim() +
                       "' and planperiod <= '" + tEndMon.trim() + "'"
                       + " and planperiodunit = '1' and plantype = '2'"
                       + " and planobject = '" + tChannel[i - 1].trim() + "'";
                float tPlanValue = 0;
                tPlanValue = Float.parseFloat(aExeSQL.getOneValue(Sql2).trim());
                if (tPlanValue == 0)
                {
                    strArr[i - 1][3] = "0";
                }
                else
                {
                    strArr[i -
                            1][3] = Float.toString(Float.parseFloat(strArr[i -
                            1][1].trim()) / tPlanValue).trim();
                }
                tSumPlanValue += tPlanValue;

                //统计活动网点数
                Sql2 =
                        "select nvl(count(distinct agentcom),0) from lacommision "
                        + "where trim(branchattr) like '" + tChannel[i -
                        1].trim()
                        + "%' and signdate >= '" + this.mStartDate.trim()
                        + "' and signdate <= '" + this.mEndDate.trim()
                        + "' and transmoney <> 0";
                strArr[i - 1][5] = aExeSQL.getOneValue(Sql2).trim();

                //统计客户经理数
                Sql2 =
                        "select nvl(count(distinct agentcode),0) from lacomtoagent "
                        +
                        "where agentcom in ( select distinct agentcom from lacommision "
                        + "where tmakedate >= '" + this.mStartDate.trim()
                        + "' and tmakedate <= '" + this.mEndDate.trim() + "'"
                        + " and trim(branchattr) like '" + tChannel[i - 1].trim() +
                        "%')";
                strArr[i - 1][6] = aExeSQL.getOneValue(Sql2).trim();

                //统计网点总数
                Sql2 = "select nvl(count(distinct agentcom),0) from lacom "
                       +
                       "where agentcom in ( select distinct agentcom from lacommision "
                       + "where tmakedate >= '" + this.mStartDate.trim()
                       + "' and tmakedate <= '" + this.mEndDate.trim()
                       + "' and transmoney <> 0"
                       + " and trim(branchattr) like '" + tChannel[i - 1].trim() +
                       "%')";
                strArr[i - 1][7] = aExeSQL.getOneValue(Sql2).trim();

                //累计实收保费,标准保费
                Sql2 =
                        "select nvl(sum(transmoney),0),nvl(sum(standprem),0) from lacommision "
                        + "where tmakedate >= '" + tYearStart.trim()
                        + "' and tmakedate <= '" + this.mEndDate.trim() + "'"
                        + " and trim(branchattr) like '" + tChannel[i - 1].trim() +
                        "%'";
                SSRS zSSRS = new SSRS();
                zSSRS = aExeSQL.execSQL(Sql2);
                strArr[i - 1][8] = zSSRS.GetText(1, 1);
                tTransMoneySum += Float.parseFloat(strArr[i - 1][8]);
                strArr[i - 1][11] = zSSRS.GetText(1, 2);

                //累计完成进度
                Sql2 = "select nvl(planvalue,0) from laplan "
                       + "where planperiodunit = '12'"
                       + " and planperiod = '" +
                       this.mStartDate.trim().substring(0, 4).trim() + "'"
                       + " and plantype = '2' and planobject = '" + tChannel[i -
                       1].trim() + "'";
                String tYearPlan = "";
                tYearPlan = aExeSQL.getOneValue(Sql2).trim();
                if (tYearPlan.trim().equals("") || tYearPlan == null)
                {
                    strArr[i - 1][9] = "0";
                }
                else
                {
                    strArr[i -
                            1][9] = Float.toString(Float.parseFloat(strArr[i -
                            1][8].trim())
                            / Float.parseFloat(tYearPlan.trim()));
                    tSumYearPlan += Float.parseFloat(tYearPlan.trim());
                }

                //网均产能
                if (strArr[i - 1][7].trim().equalsIgnoreCase("0"))
                {
                    strArr[i - 1][12] = "0";
                }
                else
                {
                    strArr[i -
                            1][12] = Float.toString(Float.parseFloat(strArr[i -
                            1][1].trim()) /
                            Float.parseFloat(strArr[i - 1][7].trim()));
                }

                //件均保费
                if (strArr[i - 1][4].trim().equalsIgnoreCase("0"))
                {
                    strArr[i - 1][13] = "0";
                }
                else
                {
                    strArr[i -
                            1][13] = Float.toString(Float.parseFloat(strArr[i -
                            1][1].trim()) /
                            Float.parseFloat(strArr[i - 1][4].trim()));
                }

                //人均保费
                if (strArr[i - 1][6].trim().equalsIgnoreCase("0"))
                {
                    strArr[i - 1][14] = "0";
                }
                else
                {
                    strArr[i -
                            1][14] = Float.toString(Float.parseFloat(strArr[i -
                            1][1].trim()) /
                            Float.parseFloat(strArr[i - 1][6].trim()));
                }
                //网点活动率
                if (strArr[i - 1][7].trim().equalsIgnoreCase("0"))
                {
                    strArr[i - 1][15] = "0";
                }
                else
                {
                    strArr[i -
                            1][15] = Float.toString(Float.parseFloat(strArr[i -
                            1][5].trim()) /
                            Float.parseFloat(strArr[i - 1][7].trim()));
                }
            }

            //计算业务占比
            if (tSum == 0)
            {
                for (int j = 0; j < tChannelCount; j++)
                {
                    strArr[j][2] = "0";
                }
            }
            else
            {
                for (int j = 0; j < tChannelCount; j++)
                {
                    strArr[j][2] = Float.toString(Float.parseFloat(strArr[j][1].
                            trim()) / tSum);
                }
            }

            for (int m = 0; m < tChannelCount; m++)
            {
                eachList = new String[16];
                for (int n = 0; n < 16; n++)
                {
                    eachList[n] = strArr[m][n];
                }
                tListTable.add(eachList);
            }

            //部门汇总
            eachList = new String[16];
            eachList[0] = "部门汇总";
            eachList[1] = Float.toString(tSum).trim();
            eachList[2] = "0";

            if (tSumYearPlan == 0)
            {
                eachList[3] = "0";
            }
            else
            {
                eachList[3] = Float.toString(tSum / tSumPlanValue).trim();
            }

            float tPolCount = 0;
            int tAllNetCount = 0;
            int tManagerCount = 0;
            int tActiveNetCount = 0;
            float tSumTransMoney = 0;
            float tSumStandPrem = 0;
            float tYearStandPrem = 0;
            for (int k = 0; k < tChannelCount; k++)
            {
                tPolCount += Float.parseFloat(strArr[k][4].trim());
                tAllNetCount += Integer.parseInt(strArr[k][7].trim());
                tManagerCount += Integer.parseInt(strArr[k][6].trim());
                tActiveNetCount += Integer.parseInt(strArr[k][5].trim());
                tSumTransMoney += Float.parseFloat(strArr[k][8].trim());
                tSumStandPrem += Float.parseFloat(strArr[k][10].trim());
                tYearStandPrem += Float.parseFloat(strArr[k][11].trim());
            }
            eachList[4] = Float.toString(tPolCount).trim();
            eachList[5] = Integer.toString(tActiveNetCount).trim();
            eachList[6] = Integer.toString(tManagerCount).trim();
            eachList[7] = Integer.toString(tAllNetCount).trim();
            eachList[8] = Float.toString(tSumTransMoney).trim();

            if (tSumYearPlan == 0)
            {
                eachList[9] = "0";
            }
            else
            {
                eachList[9] = Float.toString(tTransMoneySum / tSumYearPlan).
                              trim();
            }

            eachList[10] = Float.toString(tSumStandPrem).trim();
            eachList[11] = Float.toString(tYearStandPrem).trim();
            if (tAllNetCount == 0)
            {
                eachList[12] = "0";
            }
            else
            {
                eachList[12] = Float.toString(tSum / tAllNetCount).trim();
            }

            if (tPolCount == 0)
            {
                eachList[13] = "0";
            }
            else
            {
                eachList[13] = Float.toString(tSum / tPolCount).trim();
            }

            if (tManagerCount == 0)
            {
                eachList[14] = "0";
            }
            else
            {
                eachList[14] = Float.toString(tSum / tManagerCount).trim();
            }

            if (tAllNetCount == 0)
            {
                eachList[15] = "0";
            }
            else
            {
                eachList[15] = Float.toString(tActiveNetCount / tAllNetCount).
                               trim();
            }

            tListTable.add(eachList);

            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("ChannelBusiStatics.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tListTable, eachList);
//    xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
            this.mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ChannelBusiReport";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("------prepareData end-----");

        return true;
    }


    private void dealError(String tFuncName, String tErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "ChannelBusiReport";
        cError.functionName = tFuncName;
        cError.errorMessage = tErrMsg;
        this.mErrors.addOneError(cError);
    }

    public VData getResult()
    {
        return this.mResult;
    }

}