/*
 * <p>ClassName: LAChnlActiveReportBL </p>
 * <p>Description: LAChnlActiveReportBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2009-04-20 18:03:36
 */
package com.sinosoft.lis.agentprint;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;


import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.lis.pubfun.AgentPubFun;

public class LAChnlActiveReportBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */

    //统计层级
    private String mLevel = "";
    //统计管理机构
    private String mManageCom = "";
    //统计止期
    private String  mEndMonth= "";
    //统计方式
    private String  mMode= "";
    //统计代理机构编码
    private String  mAgentCom = "";
    //需要调用的模版
    private String mFlag = "";

    //time
    private String mMonthBegin = "";
    private String mMonthEnd = "";
    private String mSeaSonBegin = "";


    //零时存放的数组

    //当前时间
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private  ListTable tListTable = new ListTable();
    public LAChnlActiveReportBL() {
    }

    public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //将操作数据拷贝到本类中
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        mOperate=cOperate;
        System.out.println("dealData:" + mOperate);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }


        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAChnlActiveReportBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAChnlActiveReportBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        tListTable = null;

        return true;
    }
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LAChnlActiveReportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("dealData:" + mOperate);
        //调用模板
        mFlag = getflag(mLevel);
        ExeSQL tExeSQL = new ExeSQL();
        int j=1;

        if(this.mLevel.equals("0"))//分公司
        {
           String
           manageSQL = " select comcode,name from ldcom "
                     + " where comgrade='02' and sign='1' and comcode like '"+mManageCom+"%' order  by comcode ";
           SSRS managSSRS = new SSRS();
           managSSRS = tExeSQL.execSQL(manageSQL);
           System.out.println();
           for(int i = 1;i<=managSSRS.getMaxRow();i++)
           {//进行机构循环
               getDataList(managSSRS.GetText(i, 1),managSSRS.GetText(i, 2),mMode,j);
               j++;
           }
       System.out.println(manageSQL);
        }

        if(this.mLevel.equals("1"))//中支公司
        {
            String
            manageSQL =" select comcode,name from ldcom "
                      +" where comgrade='03' and sign='1' and comcode<>'86000000'  "//and substr(comcode,5)<>'0000'
                      +" and comcode like '"+mManageCom+"%' order  by comcode ";
            SSRS managSSRS = new SSRS();
            managSSRS = tExeSQL.execSQL(manageSQL);
            for(int i = 1;i<=managSSRS.getMaxRow();i++)
            {//进行机构循环
               getDataList(managSSRS.GetText(i, 1),managSSRS.GetText(i, 2),mMode,j);
               j++;
            }
          System.out.println(manageSQL);
        }
        if(this.mLevel.equals("2"))//营业部,统计区间内为营业
        {
            String
            branchSQL =" select branchattr,agentgroup,name,managecom from labranchgroup "
                      +" where branchtype='3' and branchtype2='01' "
                      +" and founddate<='"+this.mMonthEnd+"' and (enddate is null or enddate>='"+this.mMonthEnd+"')  "
                      +" and branchlevel='42' and managecom like '"+mManageCom+"%' order by managecom,branchattr ";
            System.out.println(branchSQL);
            SSRS tSSRS = new SSRS();
            tSSRS = tExeSQL.execSQL(branchSQL);
            for(int i = 1;i<=tSSRS.getMaxRow();i++)
            {//进行营业部循环
                getDataList(tSSRS.GetText(i, 1),tSSRS.GetText(i, 2),tSSRS.GetText(i, 3),tSSRS.GetText(i, 4),mMode,j);
                j++;
            }

        }
        if(this.mLevel.equals("3"))//营业组
        {
            String
            agentSQL =" select branchattr,agentgroup,name,managecom from labranchgroup "
                     +" where branchtype='3' and branchtype2='01' "
                     +" and founddate<='"+this.mMonthEnd+"' and (enddate is null or enddate>='"+this.mMonthEnd+"') "
                     +" and branchlevel='41' and managecom like '"+mManageCom+"%' order by managecom,branchattr ";

            System.out.println(agentSQL);
            SSRS tSSRS = new SSRS();
            tSSRS = tExeSQL.execSQL(agentSQL);
            for(int i = 1;i<=tSSRS.getMaxRow();i++)
            {//进行人员循环
                getDataList(tSSRS.GetText(i, 1),tSSRS.GetText(i, 2),tSSRS.GetText(i, 3),tSSRS.GetText(i, 4),mMode,j);
                j++;
            }

        }
         if(this.mLevel.equals("4"))//所有人员  稍等
        {
            String
            agentSQL =" select agentcode,name,agentgroup,managecom from laagent "
                     +" where branchtype='3' and branchtype2='01' "
                     +" and employdate<='"+this.mMonthEnd+"' and (outworkdate is null or outworkdate>='"+this.mMonthEnd+"')"
                     +" and managecom like '"+mManageCom+"%'  order by managecom ";
            System.out.println(agentSQL);
            SSRS tSSRS = new SSRS();
            tSSRS = tExeSQL.execSQL(agentSQL);
            for(int i = 1;i<=tSSRS.getMaxRow();i++)
            {//进行人员循环

                getDataList(tSSRS.GetText(i, 1),tSSRS.GetText(i, 2),tSSRS.GetText(i, 3),tSSRS.GetText(i, 4),j);
                j++;
            }

        }

        if(tListTable==null||tListTable.equals(""))
        {
            CError tCError = new CError();
            tCError.moduleName = "MakeXMLBL";
            tCError.functionName = "creatFile";
            tCError.errorMessage = "没有符合条件的信息！";
            this.mErrors.addOneError(tCError);
            return false;
        }
        //进行模版的区分
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        XmlExport txmlexport = new XmlExport(); //新建一个XmlExport的实例
        if(mFlag.equals("1"))
        {
            txmlexport.createDocument("LAChnlActive1.vts","printer");
        }
        if(mFlag.equals("2"))
        {
            txmlexport.createDocument("LAChnlActive2.vts","printer");
        }
        if(mFlag.equals("3"))
        {
            txmlexport.createDocument("LAChnlActive3.vts","printer");
        }
        if(mFlag.equals("4"))
        {
            txmlexport.createDocument("LAChnlActive4.vts","printer");
        }
        tListTable.setName("LAChnlActive");
        String[] title = {"", "", "", "", "" ,"","","","","","","","","","",""};
        //放入一些基本值
        texttag.add("MakeDate", currentDate);
        texttag.add("MakeTime", currentTime);
        texttag.add("ManageCom", mManageCom);
        texttag.add("Level", mLevel);
        texttag.add("LevelName", getLevelName());
        texttag.add("Mode", mMode);
        texttag.add("ModeName", getModeName());
        texttag.add("EndMonth",mEndMonth);
        texttag.add("AgentCom",mAgentCom);
        if (texttag.size() > 0)
        {
            txmlexport.addTextTag(texttag);
        }
//       将数据放到LISTTABLE中
        txmlexport.addListTable(tListTable, title);
        txmlexport.outputDocumentToFile("c:\\", "lach");
        mResult.clear();
        mResult.addElement(txmlexport);
        return true;
    }
    /**
     * @string
     * 模版调用标记
     */
    private String getflag(String level)
    {
        String tflag = "";
        if(level.equals("0"))
        {
            tflag = "1";
        }
        if(level.equals("1"))
        {
            tflag = "1";
        }
        if(level.equals("2"))
        {
            tflag = "2";
        }
        if(level.equals("3"))
        {
            tflag = "3";
        }
        if(level.equals("4"))
        {
            tflag = "4";
        }
        return tflag;
    }


    /**
   * 分公司查询\支公司查询
   *cCode-管理机构编码；Name1名称、mMode查询方式
   */
  private boolean getDataList(String Managecom,String Name,String mMode,int j)
  {

      try
      {
          DecimalFormat tDF = new DecimalFormat("0.######");
          String data[] = new String[8];
          data[0] = String.valueOf(j);
          data[1] = Managecom;
          data[2] = Name;
          data[3] = String.valueOf(getCurrentCount(Managecom,mMonthEnd,"Z"));//计算止期网点数量
          data[4] = String.valueOf(getMonSeaYearAcc(Managecom,mEndMonth,mMode,"Z"));//出单数量
          data[5] = String.valueOf(getMonSeaYearCount(Managecom,mMonthEnd,mMode,"Z"));//平均网点数量
          data[6] = getRate(data[4],data[5]);
          tListTable.add(data);
      }
      catch(Exception ex)
      {
          CError.buildErr("getDataList", "准备数据时出错！");
          System.out.println(ex.toString());
          return false;
      }
      return true;
  }


  /**
   * 营业部查询\营业组查询
   *cCode-管理机构编码；Name1名称、mMode查询方式
   */
  private boolean getDataList(String BranchAttr,String AgentGroup,String Name,String ManageCom,String Mode,int j)
  {

      try
      {
          DecimalFormat tDF = new DecimalFormat("0.######");
          String data[] = new String[8];
          data[0] = String.valueOf(j);
          data[1] = ManageCom;
          data[2] = BranchAttr;
          data[3] = Name;
          data[4] = String.valueOf(getCurrentCount(AgentGroup,mMonthEnd,"Y"));//计算止期网点数量
          data[5] = String.valueOf(getMonSeaYearAcc(AgentGroup,mEndMonth,mMode,"Y"));
          data[6] = String.valueOf(getMonSeaYearCount(AgentGroup,mMonthEnd,mMode,"Y"));
          data[7] = getRate(data[5],data[6]);
          tListTable.add(data);
      }
      catch(Exception ex)
      {
          CError.buildErr("getDataList", "准备数据时出错！");
          System.out.println(ex.toString());
          return false;
      }
      return true;
  }


  /**
   * 营业部查询\营业组查询
   *cCode-管理机构编码；Name1名称、mMode查询方式
   */
  private boolean getDataList(String AgentCode,String Name,String AgentGroup,String ManageCom,int j)
  {

      try
      {
          DecimalFormat tDF = new DecimalFormat("0.######");
          String data[] = new String[15];
          data[0] = String.valueOf(j);
          data[1] = ManageCom;
          ExeSQL tExeSQL=new ExeSQL();
          String
           manageSQL = " select branchlevel,branchattr,name,upbranch from labranchgroup  "
                     + " where agentgroup='"+AgentGroup+"' ";
           SSRS managSSRS = new SSRS();
           managSSRS = tExeSQL.execSQL(manageSQL);
           if(managSSRS.GetText(1,1).equals("41"))
           {
           String
           SQL = " select branchattr,name from labranchgroup  "
                      + " where agentgroup='"+managSSRS.GetText(1,4)+"' ";
          ExeSQL ttExeSQL=new ExeSQL();
          SSRS mgSSRS = new SSRS();
          mgSSRS = ttExeSQL.execSQL(SQL);
          data[2] = mgSSRS.GetText(1,1);
          data[3] = mgSSRS.GetText(1,2);
          data[4] = managSSRS.GetText(1,2);
          data[5] = managSSRS.GetText(1,3);
           }
          else
          {
            data[2] = managSSRS.GetText(1,2);
            data[3] = managSSRS.GetText(1,3);
            data[4] = "";
            data[5] = "";
          }
          data[6] = AgentCode;
          data[7] = Name;
          data[8] = String.valueOf(getCurrentCount(AgentCode,mMonthEnd,"X"));//计算止期网点数量
          data[9] = String.valueOf(getMonSeaYearAcc(AgentCode,mEndMonth,mMode,"X"));
          data[10] = String.valueOf(getMonSeaYearCount(AgentCode,mMonthEnd,mMode,"X"));
          data[11] = getRate(data[9],data[10]);
          tListTable.add(data);
      }
      catch(Exception ex)
      {
          CError.buildErr("getDataList", "准备数据时出错！");
          System.out.println(ex.toString());
          return false;
      }
      return true;
  }


  /**
   * 网点数量
   * mtype 代表所传入的mCode类型，z-管理机构编码 y-营业部营业组编码 x-人员代码
   */
  private int getCurrentCount(String mCode,String mDate,String mType)
  {
      int AgentcomNum = 0;
      String tSQL ="";
      if(mType.equals("Z"))
      {
      tSQL=" select count(b.agentcom) from lacom b "
          +" where b.branchtype='3' and b.branchtype2='01' "
          +" and b.sellflag='Y' "
          +" and b.makedate<='"+mDate+"'"
          +" and (b.enddate is null or b.enddate>'"+mDate+"')"
          +" and b.managecom like '"+mCode+"%'";

      }
      if(mType.equals("Y"))
        {
        tSQL=" select count(b.agentcom) from lacom b "
            +" where b.branchtype='3' and b.branchtype2='01' "
            +" and b.sellflag='Y' "
            +" and b.makedate<='"+mDate+"'"
            +" and (b.enddate is null or b.enddate>'"+mDate+"')"
            +" and b.agentcom in (select agentcom from lacomtoagent "
            +" where agentgroup in "
            +" (select agentgroup from labranchgroup where agentgroup='"+mCode+"' or upbranch='"+mCode+"'))";
        }
        if(mType.equals("X"))
       {
        tSQL=" select count(b.agentcom) from lacom b "
            +" where b.branchtype='3' and b.branchtype2='01' "
            +" and b.sellflag='Y' "
            +" and b.makedate<='"+mDate+"'"
            +" and (b.enddate is null or b.enddate>'"+mDate+"')"
            +" and b.agentcom in (select distinct agentcom from lacomtoagent "
            +" where agentcode='"+mCode+"' )";
        }
     if(mAgentCom!=null&&!mAgentCom.equals("")){
       tSQL=tSQL+" and b.agentcom like '"+mAgentCom+"%'";
     }
       tSQL=tSQL+" with ur ";
     try{
         AgentcomNum = (int) execQuery(tSQL);
     }catch(Exception ex)
     {
         System.out.println("getCurrentCount 出错！");
     }
      return AgentcomNum;
  }



  /**
    * 出单数量
    * mtype 代表所传入的mCode类型，z-管理机构编码 y-营业部营业组编码 x-人员代码
    */
   private int getMonSeaYearAcc(String mCode,String mDate,String mMode,String mType)
   {
       int AgentcomNum = 0;
       String tSQL ="";
       if(mType.equals("Z"))
       {
       tSQL=" select count(distinct b.agentcom) from lacommision b "
           +" where b.branchtype='3' and b.branchtype2='01' "
           +" and b.managecom like '"+mCode+"%'";
      }
      else if(mType.equals("Y"))
         {
         tSQL=" select count(distinct b.agentcom) from lacommision b "
             +" where b.branchtype='3' and b.branchtype2='01' "
             +" and b.agentcom in (select agentcom from lacomtoagent "
             +" where agentgroup in "
             +" (select agentgroup from labranchgroup where agentgroup='"+mCode+"' or upbranch='"+mCode+"'))";
         }
        else if(mType.equals("X"))
        {
         tSQL=" select count(distinct b.agentcom) from lacommision b "
             +" where b.branchtype='3' and b.branchtype2='01' "
             +" and b.agentcom in (select distinct agentcom from lacomtoagent "
             +" where agentcode='"+mCode+"' )";
         }

      if(mMode.equals("0")){
         tSQL=tSQL+" and b.wageno='"+mDate+"'";
      }
      else if(mMode.equals("1")){
      String  tNow = AgentPubFun.formatDate(mSeaSonBegin, "yyyyMM");
      tSQL=tSQL+" and b.wageno>='"+tNow+"' and b.wageno<='"+mDate+"'";
      }
      else if(mMode.equals("2")){
        String  tNow=mDate.substring(0,4)+"01";
        tSQL=tSQL+" and b.wageno>='"+tNow+"' and b.wageno<='"+mDate+"'";
     }

      if(mAgentCom!=null&&!mAgentCom.equals("")){
        tSQL=tSQL+" and b.agentcom like '"+mAgentCom+"%'";
      }
        tSQL=tSQL+" with ur ";
      try{
          AgentcomNum = (int) execQuery(tSQL);
      }catch(Exception ex)
      {
          System.out.println("getMonSeaYearAcc 出错！");
      }
       return AgentcomNum;
   }


  /**
  * 月均网点数量、季度平均网点数量、年度网点数量
  */
 private String getMonSeaYearCount(String mCode,String mDate,String mMode,String mType)
 {
     String tValue ="";
     int AgentcomNum = 0;
     try{
     if(mMode.equals("0"))
     {
       int value1=getMonSeaYearCountOne(mCode,mMonthBegin,mMode,mType);
       int value2=getMonSeaYearCountOne(mCode,mMonthEnd,mMode,mType);
       AgentcomNum=value1+value2;
       tValue=getRate1(String.valueOf(AgentcomNum),"2");
     }
    else if(mMode.equals("1"))
    {
     String sea1=mSeaSonBegin;
     String sea2=PubFun.calDate(mSeaSonBegin, 1,"M", null);
     String sea3=PubFun.calDate(mSeaSonBegin, 2,"M", null);
     String end1=mMonthEnd;
     String end2=PubFun.calDate(sea2, -1,"D", null);
     String end3=PubFun.calDate(sea3, -1,"D", null);
     int value1=getMonSeaYearCountOne(mCode,sea1,mMode,mType);
     int value2=getMonSeaYearCountOne(mCode,sea2,mMode,mType);
     int value3=getMonSeaYearCountOne(mCode,sea3,mMode,mType);
     int value4=getMonSeaYearCountOne(mCode,end1,mMode,mType);
     int value5=getMonSeaYearCountOne(mCode,end2,mMode,mType);
     int value6=getMonSeaYearCountOne(mCode,end3,mMode,mType);
     AgentcomNum=value1+value2+value3+value4+value5+value6;
     tValue=getRate1(String.valueOf(AgentcomNum),"6");
    }

    else if(mMode.equals("2"))
    {
       String sea1=mEndMonth.substring(0,4)+"-01-01";
       String sea2=mEndMonth.substring(0,4)+"-04-01";
       String sea3=mEndMonth.substring(0,4)+"-07-01";
       String sea4=mEndMonth.substring(0,4)+"-10-01";
       String end1=mEndMonth.substring(0,4)+"-03-31";
       String end2=mEndMonth.substring(0,4)+"-06-30";
       String end3=mEndMonth.substring(0,4)+"-09-30";
       String end4=mEndMonth.substring(0,4)+"-12-31";
       int value1=getMonSeaYearCountOne(mCode,sea1,mMode,mType);
       int value2=getMonSeaYearCountOne(mCode,sea2,mMode,mType);
       int value3=getMonSeaYearCountOne(mCode,sea3,mMode,mType);
       int value4=getMonSeaYearCountOne(mCode,sea4,mMode,mType);
       int value5=getMonSeaYearCountOne(mCode,end1,mMode,mType);
       int value6=getMonSeaYearCountOne(mCode,end2,mMode,mType);
       int value7=getMonSeaYearCountOne(mCode,end3,mMode,mType);
       int value8=getMonSeaYearCountOne(mCode,end4,mMode,mType);
       AgentcomNum=value1+value2+value3+value4+value5+value6+value7+value8;
       tValue=getRate1(String.valueOf(AgentcomNum),"8");
    }

    }catch(Exception ex)
    {
        System.out.println("getMonSeaYearCount 出错！");
    }
     return tValue;
 }


 private int getMonSeaYearCountOne(String mCode,String mDate,String mMode,String mType)
 {
     int AgentcomNum = 0;
     String tSQL ="";
     //yuechu
     if(mType.equals("Z"))
     {
     tSQL=" select count(b.agentcom) from lacom b "
         +" where b.branchtype='3' and b.branchtype2='01' "
         +" and b.sellflag='Y' "
         +" and b.makedate<='"+mDate+"'"
         +" and (b.enddate is null or b.enddate>'"+mDate+"')"
         +" and b.managecom like '"+mCode+"%'";
     }
     else if(mType.equals("Y"))
       {
       tSQL=" select count(b.agentcom) from lacom b "
           +" where b.branchtype='3' and b.branchtype2='01' "
           +" and b.sellflag='Y' "
           +" and b.makedate<='"+mDate+"'"
           +" and (b.enddate is null or b.enddate>'"+mDate+"')"
           +" and b.agentcom in (select distinct agentcom from lacomtoagent "
           +" where agentgroup in "
           +" (select agentgroup from labranchgroup where agentgroup='"+mCode+"' or upbranch='"+mCode+"'))";
       }
   else if(mType.equals("X"))
   {
       tSQL=" select count(b.agentcom) from lacom b "
           +" where b.branchtype='3' and b.branchtype2='01' "
           +" and b.sellflag='Y' "
           +" and b.makedate<='"+mDate+"'"
           +" and (b.enddate is null or b.enddate>'"+mDate+"')"
           +" and b.agentcom in (select distinct  agentcom from lacomtoagent "
           +" where agentcode='"+mCode+"' ) ";

    }
    if(mAgentCom!=null&&!mAgentCom.equals("")){
          tSQL=tSQL+" and b.agentcom like '"+mAgentCom+"%'";
        }
       tSQL=tSQL+" with ur ";
    try{
        AgentcomNum = (int) execQuery(tSQL);
    }catch(Exception ex)
    {
        System.out.println("getMonSeaYearCountOne 出错！");
    }
     return AgentcomNum;
 }



    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {

        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        mLevel = (String)cInputData.get(0);
        System.out.println("统计层级mLevel:"+mLevel);
        mManageCom = (String)cInputData.get(1);
        mEndMonth = (String)cInputData.get(2);
        mMode = (String)cInputData.get(3);
        mAgentCom = (String)cInputData.get(4);
         System.out.println("统计条件为：managecom"+mManageCom+" mEndMonth:"+mEndMonth+" mMode:"+mMode);
        if(mEndMonth!=null&&!mEndMonth.equals("")){
            //月初 月末
        String SQL = "select startdate,enddate from lastatsegment where stattype='1' and yearmonth="+mEndMonth+" with ur";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(SQL);
        mMonthBegin=tSSRS.GetText(1, 1);
        mMonthEnd=tSSRS.GetText(1, 2);
       if(mMode.equals("1")){
           String tSQL =
                   "select startdate from lastatsegment where stattype='2' and yearmonth=" +
                   mEndMonth + " with ur";
           ExeSQL ttExeSQL = new ExeSQL();
           SSRS ttSSRS = new SSRS();
           ttSSRS = ttExeSQL.execSQL(tSQL);
           mSeaSonBegin = ttSSRS.GetText(1, 1);
       }
       }
       System.out.println(mMonthBegin+" "+mMonthEnd+" "+mSeaSonBegin);
        return true;
    }


    /**
     * bili
     */


    private String getRate(String pmValue1,String pmValue2)
   {
       String tSQL = "";
       String tRtValue = "";

       // 判断被除数是0
       if("0.0".equals(pmValue2)||"0".equals(pmValue2))
       {
           return "0";
       }
        tSQL = "select DECIMAL(DECIMAL("+pmValue1+"*100,12,2)/DECIMAL("+pmValue2+",12,2),12,2) from dual ";
        try{
           tRtValue = String.valueOf(execQuery(tSQL));
       }catch(Exception ex)
       {
           System.out.println("getRate 出错！");
       }

       return tRtValue;
   }

   private String getRate1(String pmValue1,String pmValue2)
   {
       String tSQL = "";
       String tRtValue = "";

       // 判断被除数是0
       if("0".equals(pmValue2))
       {
           return "0";
       }
        tSQL = "select DECIMAL(DECIMAL("+pmValue1+",12,2)/DECIMAL("+pmValue2+",12,2),12,2) from dual ";
        try{
           tRtValue = String.valueOf(execQuery(tSQL));
       }catch(Exception ex)
       {
           System.out.println("getRate 出错！");
       }

       return tRtValue;
   }


    /**
    * 得到层级名称
    */
   private String getLevelName()
   {
       String name = "";
       if(mLevel.equals("0"))
       {
         name="分公司层级";
       }
       else  if(mLevel.equals("1"))
       {
         name="支公司层级";
       }
       else  if(mLevel.equals("2"))
      {
        name="营业部层级";
      }
      else  if(mLevel.equals("3"))
      {
        name="营业组层级";
      }
      else  if(mLevel.equals("4"))
      {
        name="业务经理层级";
      }

       return name;
   }

   /**
     * 得到方式名称
     */
    private String getModeName()
    {
        String name = "";
        if(mMode.equals("0"))
        {
          name="月";
        }
        else  if(mMode.equals("1"))
        {
          name="季度";
        }
        else  if(mMode.equals("2"))
       {
         name="年";
       }

        return name;
    }


    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return double
     */
    private double execQuery(String sql)

    {

        Connection conn;

        conn = null;

        conn = DBConnPool.getConnection();



        System.out.println(sql);



        PreparedStatement st = null;

        ResultSet rs = null;

        try {

            if (conn == null)return 0.00;

            st = conn.prepareStatement(sql);

            if (st == null)return 0.00;

            rs = st.executeQuery();

            if (rs.next()) {

                return rs.getDouble(1);

            }

            return 0.00;

        } catch (Exception ex) {

            ex.printStackTrace();

            return -1;

        } finally {

            try {

              if (!conn.isClosed()) {

                  conn.close();

              }

              try {

                  st.close();

                  rs.close();

              } catch (Exception ex2) {

                  ex2.printStackTrace();

              }

              st = null;

                rs = null;

                conn = null;

            } catch (Exception e) {}

        }

      }
    public VData getResult()
    {
        return mResult;
    }
}
