package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: sinosoft</p>
 * @author zouxh
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class BankPoliDetailBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mManagecom = "";
    private String mAgentcom = "";
    private String mRiskcode = "";
    private String mTjtype = "";
    private String mTypestr = "";
    private String StartDay = "";
    private String EndDay = "";
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    public BankPoliDetailBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        mManagecom = (String) cInputData.get(0);
        System.out.println("managecom" + mManagecom);
        mAgentcom = (String) cInputData.get(1);
        System.out.println("mAgentcom" + mAgentcom);
        mRiskcode = (String) cInputData.get(2);
        System.out.println("mRiskcode" + mRiskcode);
        mTjtype = (String) cInputData.get(3);
        System.out.println("mTjtype" + mTjtype);
        StartDay = (String) cInputData.get(4);
        System.out.println("StartDay" + StartDay);
        EndDay = (String) cInputData.get(5);
        System.out.println("EndDay" + EndDay);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 6);
        System.out.println("mGlobalInput end");
        //mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",6));

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "BankPoliDetailBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String strArr[] = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("BANK");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDay", StartDay); //输入制表时间
        texttag.add("EndDay", EndDay); //输入制表时间
        strArr = new String[26];
        double data[] = null;
        data = new double[26];
        double data1[] = null;
        data1 = new double[26];

        if (mTjtype.equals("") || mTjtype == null)
        {
            mTypestr = "tmakedate";
        }
        else
        {
            switch (Integer.parseInt(mTjtype))
            {
                case 1:
                {
                    mTypestr = "tmakedate"; //统计日期
                    break;
                }
                case 2:
                {
                    mTypestr = "signdate"; //签单日期
                    break;
                }
                case 3:
                {
                    mTypestr = "getpoldate"; //回单日期
                    break;
                }
            }
        }

        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);
            //执行统计
            String asql = "select a.polno,a.years,a.p11,a.signdate,a.makepoldate,a.getpoldate,b.riskname,a.transmoney,"
                          + " a.payintv,a.payyears,a.tmakedate,a.agentcode,c.name,d.name,a.fyc,a.grpfyc,a.depfyc,a.managecom,a.appntno,a.p12,a.agentcom,a.branchcode"
                          + " from lacommision a,lmrisk b,laagent c,lacom d"
                          + " where a.managecom like '" +
                          mGlobalInput.ManageCom.trim() + "%'"
                          + " and a.agentcode=c.agentcode"
                          + " and a.riskcode=b.riskcode"
                          + " and a.agentcom=d.agentcom"
                          + " and a.branchtype='3'"
                          + " and " + mTypestr + ">='" + StartDay + "' and " +
                          mTypestr + "<='" + EndDay + "'"
                          + " and a.AgentCom like '" + mAgentcom + "%'"
                          + " and a.riskcode like '" + mRiskcode +
                          "%' and a.managecom like '" + this.mManagecom + "%'" +
                          " order by a.polno";
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(asql);
            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                for (int b = 1; b <= 18; b++)
                {

                    strArr[b - 1] = aSSRS.GetText(a, b);

                }
                //机构名称
                String bsql = "select shortname from ldcom where comcode='" +
                              aSSRS.GetText(a, 18) + "'";
                strArr[18] = aExeSQL.getOneValue(bsql);
                //投保人年龄，性别
                SSRS bSSRS = new SSRS();
                int Years = Integer.parseInt(EndDay.substring(0, 4)); //得到当前年
                String csql =
                        "select birthday,decode(sex,'0','男','女') from ldperson where customerno='" +
                        aSSRS.GetText(a, 19) + "'";
                bSSRS = aExeSQL.execSQL(csql);
                int n = bSSRS.getMaxRow();
                if (bSSRS.getMaxRow() == 0)
                {
                    strArr[19] = "";
                    strArr[20] = "";
                }
                else
                {
                    String Birth = bSSRS.GetText(n, 1);
                    int BirthYear = Integer.parseInt(Birth.substring(0, 4)); //得到出生年
                    strArr[19] = String.valueOf(Years - BirthYear);
                    strArr[20] = bSSRS.GetText(n, 2);
                }
                String dsql =
                        "select birthday,decode(sex,'0','男','女') from ldperson where customerno='" +
                        aSSRS.GetText(a, 20) + "'";
                SSRS cSSRS = new SSRS();
                cSSRS = aExeSQL.execSQL(dsql);
                if (cSSRS.getMaxRow() == 0)
                {
                    strArr[21] = "";
                    strArr[22] = "";
                }
                else
                {
                    String Birth1 = cSSRS.GetText(1, 1);
                    int BirthYear1 = Integer.parseInt(Birth1.substring(0, 4)); //得到出生年
                    strArr[21] = String.valueOf(Years - BirthYear1);
                    strArr[22] = bSSRS.GetText(1, 2);
                }
                //网点代码
                strArr[23] = aSSRS.GetText(a, 21);
                //客户经理所属渠道组
                String esql =
                        "select name from labranchgroup where agentgroup='" +
                        aSSRS.GetText(a, 22) + "'";
                strArr[24] = aExeSQL.getOneValue(esql);
                //网点所属机构
                if (strArr[23].substring(0, 1).equals("2"))
                {
                    strArr[25] = "中介";
                }
                else
                {
                    String fsql = "select name from lacom where agentcom='" +
                                  strArr[23].substring(0, 4) + "'";
                    strArr[25] = aExeSQL.getOneValue(fsql);
                }

                tlistTable.add(strArr);
                strArr = new String[26];
            }

            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("BankPoliDetail.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
            //xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankPoliDetailBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    public static void main(String[] args)
    {

        String mmanagecom = "86";
        String AgentCom = "";
        String RiskCode = "111502";
        String tjtype = "1";
        String StartDay = "2004-1-1";
        String EndDay = "2004-4-5";
        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.ManageCom = "86";
        VData tVData = new VData();

        //VData.addElement(tG);
        tVData.addElement(mmanagecom);
        tVData.addElement(AgentCom);
        tVData.addElement(RiskCode);
        tVData.addElement(tjtype);
        tVData.addElement(StartDay);
        tVData.addElement(EndDay);
        tVData.addElement(mGlobalInput);
        BankPoliDetailBL UI = new BankPoliDetailBL();
        if (!UI.submitData(tVData, ""))
        {
            if (UI.mErrors.needDealError())
            {
                System.out.println(UI.mErrors.getFirstError());
            }
            else
            {
                System.out.println("UI发生错误，但是没有提供详细的出错信息");
            }
        }
        else
        {
            VData vData = UI.getResult();
            System.out.println("已经接收了数据!!!");
        }

    }
}