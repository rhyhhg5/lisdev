package com.sinosoft.lis.agentprint;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.*;
import java.text.DecimalFormat;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class LAAgentReportBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    //private String mtype = null;
    SSRS tSSRS=new SSRS();
    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    public LAAgentReportBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LAAgentReportBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 4][50];

        mToExcel[0][2] = "中国人民健康保险股份有限公司"+"tName"+"分公司业务员基础信息表";
        
        mToExcel[1][0]="营销员代码 ";
        mToExcel[1][1]="姓     名";
        mToExcel[1][2]="性 别";
        mToExcel[1][3]="出生日期";
        mToExcel[1][4]="证件号码";
        mToExcel[1][5]="民族";
        mToExcel[1][6]="籍贯";
        mToExcel[1][7]="政治面貌";
        mToExcel[1][8]="户口所在地";
        mToExcel[1][9]="学历";
        mToExcel[1][10]="毕业院校";
        mToExcel[1][11]="专业";
        mToExcel[1][12]="家庭住址";
        mToExcel[1][13]="邮政编码";
        mToExcel[1][14]="电话";
        mToExcel[1][15]="手机";
        mToExcel[1][16]="电子邮件";
        mToExcel[1][17]="原工作单位";
        mToExcel[1][18]="入司时间";
        mToExcel[1][19]="营销员职级";
        mToExcel[1][20]="职级名称";
        mToExcel[1][21]="推荐人代码";
        mToExcel[1][22]="推荐人姓名";
        mToExcel[1][23]="销售单位";
        mToExcel[1][24]="销售单位名称";
        mToExcel[1][25]="营销部名称";
        mToExcel[1][26]="处经理代码";
        mToExcel[1][27]="处经理姓名";
        mToExcel[1][28]="区经理代码";
        mToExcel[1][29]="区经理姓名";
        mToExcel[1][30]="部经理代码";
        mToExcel[1][31]="部经理姓名";
        mToExcel[1][32]="入职日期";
        mToExcel[1][33]="离职日期";
        mToExcel[1][34]="代理人状态";
        mToExcel[1][35]="展业证号";
        mToExcel[1][36]="资格证号";
       

        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row +1][col - 1] = tSSRS.GetText(row, col);
            }
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        if(mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }
    private String dealsum(int pmLength,int pmArrNum)
           {
             String tReturnValue = "";
             DecimalFormat tDF = new DecimalFormat("0.######");
             String tSQL = "select 0";

             for(int i=1;i<pmLength+1;i++)
             {
                 tSQL += " + " + tSSRS.GetText(i, pmArrNum+1);
             }

             tSQL += " + 0 from dual";

             tReturnValue = "" + tDF.format(execQuery(tSQL));

             return tReturnValue;
        }
   
    
    private String getAct(String pmValue1,String pmValue2)
    {
        String tSQL = "";
        String tRtValue = "";
        tSQL = "select DECIMAL(DECIMAL(" + pmValue1 + ",12,2) / DECIMAL(" +pmValue2 + ",12,2),12,6) from dual";
        try{
            tRtValue = String.valueOf(execQuery(tSQL));
        }catch(Exception ex)
        {
            System.out.println("getAct 出错！");
        }

        return tRtValue;
    }
        /**
         * 执行SQL文查询结果
         * @param sql String
         * @return double
         */
        private double execQuery(String sql)

        {
           Connection conn;
           conn = null;
           conn = DBConnPool.getConnection();

           System.out.println(sql);

           PreparedStatement st = null;
           ResultSet rs = null;
           try {
               if (conn == null)return 0.00;
               st = conn.prepareStatement(sql);
               if (st == null)return 0.00;
               rs = st.executeQuery();
               if (rs.next()) {
                   return rs.getDouble(1);
               }
               return 0.00;
           } catch (Exception ex) {
               ex.printStackTrace();
               return -1;
           } finally {
               try {
                  if (!conn.isClosed()) {
                      conn.close();
                  }
                  try {
                      st.close();
                      rs.close();
                  } catch (Exception ex2) {
                      ex2.printStackTrace();
                  }
                  st = null;
                  rs = null;
                  conn = null;
                } catch (Exception e) {}

           }
     }

    public static void main(String[] args)
    {
        LAAgentReportBL LAAgentReportBL = new            LAAgentReportBL();
    System.out.println("11111111");
    }
}
