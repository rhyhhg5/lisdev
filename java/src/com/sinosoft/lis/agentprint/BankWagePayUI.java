package com.sinosoft.lis.agentprint;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.agentprint.BankWagePayBL;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: </p>
 *
 * @author XX
 * @version 1.0
 */
public class BankWagePayUI{

    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();


    public BankWagePayUI() {
    }

    public boolean submitData(VData cInputData, String cOperate) {

        BankWagePayBL tBankWagePayBL = new BankWagePayBL();

        if (!tBankWagePayBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tBankWagePayBL.mErrors);

            return false;
        } else {
            this.mResult = tBankWagePayBL.getResult();
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }


    public static void main(String[] args) {

    }
}
