package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class ComStruInfoBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mManageCom = "";

    public ComStruInfoBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        /**得到从外部传来的数据，并备份到本类中*/
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        //准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "ComStruInfo";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        ListTable tListTable = new ListTable();
        tListTable.setName("ComAgInfo");
        String strArr[] = new String[7];
        String mAgentCom = "";

        SSRS aSSRS = new SSRS();
        SSRS bSSRS = new SSRS();
        SSRS cSSRS = new SSRS();
        SSRS dSSRS = new SSRS();

        //分公司名称
        String SQL = "select comcode From ldcom where comcode like '" +
                     mManageCom +
                     "%' and length(trim(comcode))=8 order by comcode";
        SSRS zSSRS = new SSRS();

        ExeSQL zExeSQL = new ExeSQL();
        zSSRS = zExeSQL.execSQL(SQL);
        for (int a = 1; a <= zSSRS.getMaxRow(); a++)
        {
            String asql = "select shortname from ldcom where comcode = '" +
                          zSSRS.GetText(a, 1) + "'";
            ExeSQL aExeSQL = new ExeSQL();
            strArr[0] = aExeSQL.getOneValue(asql);
            //机构代理分类
            String bsql = "select distinct a.codename,a.code,b.actype from ldcode a,lacom b where managecom like '" +
                          zSSRS.GetText(a, 1) + "%' and a.codetype='actype' and b.actype in ('02','03','04') and a.code=b.actype order by actype";
            ExeSQL bExeSQL = new ExeSQL();
            aSSRS = bExeSQL.execSQL(bsql);
            for (int i = 1; i <= aSSRS.getMaxRow(); i++)
            {
                strArr[1] = aSSRS.GetText(i, 1);
                //代理机构代码
                String csql =
                        "select agentcom,name from lacom where managecom = '" +
                        zSSRS.GetText(a, 1) +
                        "' and length(trim(agentcom))=14 and actype='" +
                        aSSRS.GetText(i, 3) + "'";
                ExeSQL cExeSQL = new ExeSQL();
                bSSRS = cExeSQL.execSQL(csql);
                for (int j = 1; j <= bSSRS.getMaxRow(); j++)
                {
                    for (int m = 1; m <= bSSRS.getMaxCol(); m++)
                    {
                        if (m == 1)
                        {
                            strArr[2] = bSSRS.GetText(j, 1);
                        }
                        if (m == 2)
                        {
                            //代理机构名称
                            strArr[3] = bSSRS.GetText(j, 2);
                            //客户经理名称
                            String esql = "select trim(name) from laagent where agentcode in (select agentcode from lacomtoagent where agentcom='" +
                                          bSSRS.GetText(j, 1) +
                                          "' and relatype='1')";
                            ExeSQL eExeSQL = new ExeSQL();
                            strArr[4] = eExeSQL.getOneValue(esql);
                            //所属渠道组
                            String fsql = "select trim(name),trim(branchmanagername) from labranchgroup where  managecom like '" +
                                          zSSRS.GetText(a, 1) +
                                          "' and agentgroup in (select agentgroup from lacomtoagent where agentcom='" +
                                          bSSRS.GetText(j, 1) +
                                          "' and relatype='0') and (state<>'1' or state is null)";
                            ExeSQL fExeSQL = new ExeSQL();
                            cSSRS = fExeSQL.execSQL(fsql);
                            for (int n = 1; n <= cSSRS.getMaxRow(); n++)
                            {
                                strArr[5] = cSSRS.GetText(n, 1);
                                //渠道组组长名
                                strArr[6] = cSSRS.GetText(n, 2);
                                tListTable.add(strArr);
                                strArr = new String[7];
                            }
                        }
                    }
                }
            }
        }

        /**将所有记录都保存到tListTable中之后，开始输出XML文件 */
        XmlExport xmlexport = new XmlExport(); //产生一个新的XmlExport对象
        /**产生vts文件 */
        xmlexport.createDocument("ComAgInfo.vts", "printer");
        /**将tListTable的信息输入到xmlexport 中*/
        xmlexport.addListTable(tListTable, strArr);
        /**xmlexport将所得数据流入下面目录下的一XML文件中*/
//xmlexport.outputDocumentToFile("e:\\","test") ;
        /**将xmlexport添加到mResult中，以供以后调用*/
        mResult.addElement(xmlexport);
        System.out.println("end");
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    private boolean getInputData(VData cInputData)
    {
        /** 保存从外部传来的管理机构的信息，作为查询条件  */
        mManageCom = (String) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 1));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public static void main(String[] args)
    {

        ComStruInfoBL tComStruInfoBL = new ComStruInfoBL();
        String tManageCom = "86110000";
        VData tVData = new VData();
        tVData.addElement(tManageCom);
        GlobalInput tGlobalInput = new GlobalInput();
        tVData.add(tGlobalInput);
        tComStruInfoBL.submitData(tVData, "");
    }

    private String tranStr(String aCodeType, String aCode)
    {
        String tCodeName = null;
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType(aCodeType);
        tLDCodeDB.setCode(aCode);
        tLDCodeDB.getInfo();
        tCodeName = tLDCodeDB.getCodeName();
        return tCodeName;
    }
}