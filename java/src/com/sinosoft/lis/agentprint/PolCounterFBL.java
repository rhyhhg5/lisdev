package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PolCounterFBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private String mDay = "";
    private String mManageCom = "";
    double date3[] = new double[18]; //定义总计的数组
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    public PolCounterFBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mDay = (String) cInputData.get(0);
        mManageCom = (String) cInputData.get(1);
//    mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "PolFilialeBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String strArr[] = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("Counter");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCode(mManageCom);
        tLDCodeDB.setCodeType("station");
        tLDCodeDB.getInfo();
        texttag.add("Name", tLDCodeDB.getCodeName()); //输入制表机构
        texttag.add("Day", mDay); //输入制表时间
        double date[] = new double[6]; //分公司
        double date4[] = new double[6]; //定义支公司的数组
        double date3[] = new double[6]; //定义督导区公司的数组
        double date2[] = new double[6]; //定义区的数组
        double date1[] = new double[6]; //计算组里的数据值,得到部数据

        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);

            //查询支公司
            nsql = "select comcode,name from ldcom where length(trim(comcode))=8 and Comcode like '" +
                   mManageCom + "%' order by comcode";
            SSRS ySSRS = new SSRS();
            ySSRS = aExeSQL.execSQL(nsql);
            for (int z = 1; z <= ySSRS.getMaxRow(); z++)
            {
                for (int y = 1; y <= ySSRS.getMaxCol(); y++)
                {
                    if (y == 1)
                    {
                        //查询出各个区级
                        String mmDay[] = PubFun1.calFLDate(mDay);
                        SSRS aSSRS = new SSRS();
                        msql = "select Branchattr,name from LABranchGroup where EndFlag<>'Y' and BranchType = '1' "
                               +
                               "and BranchLevel = '03' and (state<>'1' or state is null) and branchattr like '"
                               + ySSRS.GetText(z, 1) + "%' order by branchattr";
                        aSSRS = aExeSQL.execSQL(msql);
                        int heigh = 0;
                        double people = 0;
                        for (int a = 1; a <= aSSRS.getMaxRow(); a++)
                        {
                            for (int c = 1; c <= aSSRS.getMaxCol(); c++)
                            {

                                if (c == 1)
                                {
                                    strArr = new String[6];
                                    //计算每个部门下的每个组人力资源,并加和
                                    String asql =
                                            "select count(*) from LAAgent where EmployDate < '" +
                                            mmDay[0] + "' and agentstate in ('01','02') and branchcode in (select agentgroup from labranchgroup where branchattr like '" +
                                            aSSRS.GetText(a, 1) + "%')"; ;
                                    strArr[1] = aExeSQL.getOneValue(asql);
                                    //日预收保费
                                    String bsql =
                                            "select NVL(sum(paymoney),0) from ljtempfee where MakeDate ='" +
                                            mDay
                                            +
                                            "' and (Tempfeetype='1' or Tempfeetype='5' or Tempfeetype='6')"
                                            + " and agentgroup in (select agentgroup from labranchgroup where branchattr like '"
                                            + aSSRS.GetText(a, 1) + "%')";
                                    strArr[2] = aExeSQL.getOneValue(bsql);
                                    //日柜台交单件数
                                    String csql = "select CALPIECEGTBranch('" +
                                                  mDay + "','" + mDay + "','" +
                                                  aSSRS.GetText(a, 1) +
                                                  "') from ldsysvar where sysvar='onerow'";
                                    strArr[3] = aExeSQL.getOneValue(csql);
                                    //月预收保费
                                    String fsql =
                                            "select NVL(sum(paymoney),0) from ljtempfee where MakeDate >='" +
                                            mmDay[0]
                                            +
                                            "' and (Tempfeetype='1' or Tempfeetype='5' or Tempfeetype='6')"
                                            + " and MakeDate <='" + mDay + "' and agentgroup in (select agentgroup from labranchgroup where branchattr like '"
                                            + aSSRS.GetText(a, 1) + "%')";
                                    strArr[4] = aExeSQL.getOneValue(fsql);
                                    //月交单件数
                                    String gsql = "select CALPIECEGTBranch('" +
                                                  mmDay[0] + "','" + mDay +
                                                  "','" + aSSRS.GetText(a, 1) +
                                                  "') from ldsysvar where sysvar='onerow'";
                                    strArr[5] = aExeSQL.getOneValue(gsql);
                                }
                                if (c == 2)
                                {
                                    //插入区值
                                    strArr[0] = aSSRS.GetText(a, 2); //督导区名
                                    tlistTable.add(strArr);
                                }
                            }
                            for (int m = 1; m <= 5; m++)
                            {
                                //计算展业机构：区值
                                date3[m] += Double.parseDouble(strArr[m]);
                                //计算展业机构：督导区值
                            }
                        }
                    }
                    strArr = new String[6];
                    //插入支公司值
                    if (y == 2)
                    {
                        strArr[0] = ySSRS.GetText(z, 2); //支公司名
                        for (int p = 1; p <= 5; p++)
                        {
                            strArr[p] = String.valueOf(date3[p]);
                        }
                        tlistTable.add(strArr);
                        strArr = new String[6];
                        strArr[0] = "";
                        strArr[1] = "";
                        strArr[2] = "";
                        strArr[3] = "";
                        strArr[4] = "";
                        strArr[5] = "";
                        tlistTable.add(strArr);
                    }
                }
                for (int m = 1; m <= 5; m++)
                {
                    //计算展业机构：分公司值
                    date[m] += date3[m];
                }
                date3 = new double[6]; //清空
            }
            //展业机构：总计
            strArr = new String[6];
            strArr[0] = "分公司总计";
            for (int u = 1; u <= 5; u++)
            {
                strArr[u] = String.valueOf(date[u]);
            }
            tlistTable.add(strArr);

            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("PolCounterF.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PolCounterFBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    public static void main(String[] args)
    {
        PolNBDepartF1PBL tPolNBDepartF1PBL = new PolNBDepartF1PBL();
    }
}
