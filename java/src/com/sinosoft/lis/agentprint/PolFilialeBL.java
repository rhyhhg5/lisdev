package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import java.sql.Connection;
import java.util.Arrays;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PolFilialeBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    //取得的时间
    private String mDay = "";
    private String mManageCom = "";
    double date3[] = new double[18]; //定义总计的数组
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    public PolFilialeBL()
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mDay = (String) cInputData.get(0);
        mManageCom = (String) cInputData.get(1);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "PolFilialeBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String strArr[] = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("POLFI");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCode(mManageCom);
        tLDCodeDB.setCodeType("station");
        tLDCodeDB.getInfo();
        texttag.add("Name", tLDCodeDB.getCodeName()); //输入制表机构
        texttag.add("Day", mDay); //输入制表时间

        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);

            //计算区和支公司共有多少
            String vsql = "select count(name) from LABranchGroup where EndFlag<>'Y' and BranchType = '1' and BranchLevel = '03' or BranchLevel = '05'  and (state<>'1' or state is null) and ManageCom like '" +
                          mManageCom + "%'";
            String aa = aExeSQL.getOneValue(vsql);
            int sum = Integer.parseInt(aa);
            String Date[][] = new String[sum + 1][18];
            int n = 0;
            double people2 = 0;
            double date[] = new double[18]; //分公司
            double date4[] = new double[18]; //定义支公司的数组
            double date3[] = new double[18]; //定义督导区公司的数组
            double date2[] = new double[18]; //定义区的数组
            double date1[] = new double[18]; //计算组里的数据值,得到部数据
            //查询支公司
            nsql = "select comcode,shortname from ldcom where length(trim(comcode))=8 and Comcode like '" +
                   mManageCom + "%'  order by comcode";
            SSRS ySSRS = new SSRS();
            double people1 = 0;
            ySSRS = aExeSQL.execSQL(nsql);
            for (int z = 1; z <= ySSRS.getMaxRow(); z++)
            {
                for (int y = 1; y <= ySSRS.getMaxCol(); y++)
                {
                    if (y == 1)
                    {
                        //查询出各个区级
                        String mmDay[] = PubFun1.calFLDate(mDay);
                        SSRS aSSRS = new SSRS();
                        msql = "select branchattr,name from LABranchGroup where EndFlag<>'Y' and BranchType = '1' and BranchLevel = '03' and (state<>'1' or state is null) and branchattr like  '" +
                               ySSRS.GetText(z, 1) + "%'";
                        aSSRS = aExeSQL.execSQL(msql);
                        int heigh = 0;
                        double people = 0;
                        for (int a = 1; a <= aSSRS.getMaxRow(); a++)
                        {
                            for (int c = 1; c <= aSSRS.getMaxCol(); c++)
                            {
                                if (c == 1)
                                {
                                    strArr = new String[15];
                                    //计算各个区人力资源,并加和
                                    String asql =
                                            "select count(*) from LAAgent where EmployDate < '" +
                                            mmDay[0] + "' and agentstate in ('01','02') and branchcode in (select agentgroup from labranchgroup where branchattr like '" +
                                            aSSRS.GetText(a, 1) + "%')"; ;
                                    strArr[1] = aExeSQL.getOneValue(asql);
                                    date2[1] = Double.parseDouble(strArr[1]);
                                    //日预收规模保费
                                    String bsql =
                                            "select NVL(sum(prem),0) from ljtempfee_lmriskapp where MakeDate ='" +
                                            mDay + "' and agentgroup in (select agentgroup from labranchgroup where branchattr like '" +
                                            aSSRS.GetText(a, 1) + "%')";
                                    date2[2] = Double.parseDouble(aExeSQL.
                                            getOneValue(bsql));
                                    //月预收规模保费
                                    String csql = null;
                                    try
                                    {
                                        csql =
                                                "select NVL(sum(prem),0) from ljtempfee_lmriskapp where MakeDate >='" +
                                                mmDay[0] + "' and MakeDate <='" +
                                                mDay + "' and agentgroup in (select agentgroup from labranchgroup where branchattr like '" +
                                                aSSRS.GetText(a, 1) + "%')";
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                    date2[3] = Double.parseDouble(aExeSQL.
                                            getOneValue(csql));
                                    //日规模承保
                                    String dsql = "select NVL(sum(SumActuPayMoney),0) from ljapayperson_lmriskapp2 where makedate ='" +
                                                  mDay + "' and agentgroup in (select agentgroup from labranchgroup where branchattr like '" +
                                                  aSSRS.GetText(a, 1) + "%')";
                                    date2[4] = Double.parseDouble(aExeSQL.
                                            getOneValue(dsql));
                                    //月规模保费
                                    String fsql = "select NVL(sum(SumActuPayMoney),0) from ljapayperson_lmriskapp2 where makedate >='" +
                                                  mmDay[0] +
                                                  "' and makedate <='" + mDay + "' and agentgroup in (select agentgroup from labranchgroup where branchattr like '" +
                                                  aSSRS.GetText(a, 1) + "%')";
                                    date2[5] = Double.parseDouble(aExeSQL.
                                            getOneValue(fsql));
                                    //日承保标保
                                    String gsql = "select StandpremCBBranch('" +
                                                  mDay + "','" + mDay + "','" +
                                                  aSSRS.GetText(a, 1) +
                                                  "') from ldsysvar where sysvar='onerow'";
                                    date2[6] = Double.parseDouble(aExeSQL.
                                            getOneValue(gsql));
                                    //月承保标保
                                    String hsql = "select StandpremCBBranch('" +
                                                  mmDay[0] + "','" + mDay +
                                                  "','" + aSSRS.GetText(a, 1) +
                                                  "') from ldsysvar where sysvar='onerow'";
                                    date2[7] = Double.parseDouble(aExeSQL.
                                            getOneValue(hsql));
                                    //月承保件数
                                    String jsql = "select calpieceCBBranch('" +
                                                  mmDay[0] + "','" + mDay +
                                                  "','" + aSSRS.GetText(a, 1) +
                                                  "') from ldsysvar where sysvar='onerow'";
                                    date2[8] = Double.parseDouble(aExeSQL.
                                            getOneValue(jsql));
                                    //人均标保
                                    if (strArr[1].equals("0"))
                                    {
                                        date2[9] = 0;
                                    }
                                    else
                                    {
                                        date2[9] = (date2[7]) /
                                                Integer.parseInt(strArr[1]);
                                    }
                                    //人均件数
                                    if (date2[1] == 0)
                                    {
                                        date2[10] = 0;
                                    }
                                    else
                                    {
                                        date2[10] = date2[8] / date2[1];
                                    }
                                    //件均标保
                                    if (date2[8] == 0)
                                    {
                                        date2[11] = 0;
                                    }
                                    else
                                    {
                                        date2[11] = (date2[7]) / date2[8];
                                    }
                                    //活动率
                                    int num1 = 0;
                                    String rsql =
                                            "select count( distinct agentcode) from ljapayperson where makeDate >='" +
                                            mmDay[0] + "' and makeDate <='" +
                                            mDay + "' and agentgroup in (select agentgroup from labranchgroup where branchattr like '" +
                                            aSSRS.GetText(a, 1) + "%')";
                                    num1 = Integer.parseInt(aExeSQL.getOneValue(
                                            rsql));
                                    people = num1; //区 有业绩的人数
                                    if (date2[1] == 0)
                                    {
                                        date2[12] = 0;
                                    }
                                    else
                                    {
                                        date2[12] = people / date2[1]; //区 活动率
                                    }
                                    //人均产能
                                    if (num1 == 0)
                                    {
                                        date2[13] = 0;
                                    }
                                    else
                                    {
                                        date2[13] = date2[7] / num1;
                                    }
                                    //有效人力
                                    int num2 = 0;
                                    SSRS gSSRS = new SSRS();
                                    String ssql = "select count(count(*)) from ljapayperson a,laagent b where a.agentcode=b.agentcode and a.makedate >='" +
                                                  mmDay[0] +
                                                  "' and a.makedate <='" + mDay + "' and branchcode in (select agentgroup from labranchgroup where branchattr like '" +
                                                  aSSRS.GetText(a, 1) +
                                                  "%') group by a.agentcode having NVL(sum(SumActuPayMoney),0)>'2000'";
                                    date2[14] = Double.parseDouble(aExeSQL.
                                            getOneValue(ssql));
                                }
                                if (c == 2)
                                {
                                    //插入区值
                                    Date[n][0] = aSSRS.GetText(a, 2); //督导区名
                                    for (int t = 1; t <= 14; t++)
                                    {
                                        Date[n][t] = String.valueOf(date2[t]);
                                    }
                                    //计算区计划信息
                                    //月任务计划
                                    String Month = "";
                                    if (mDay.substring(6, 7).equals("-"))
                                    {
                                        Month = mDay.substring(0, 4) + "0" +
                                                mDay.substring(5, 6);
                                    }
                                    else
                                    {
                                        Month = mDay.substring(0, 4) +
                                                mDay.substring(5, 7);
                                    }
                                    String usql = "select planvalue from laplan where plantype=2 and PlanPeriodUnit=1 and PlanPeriod='" +
                                                  Month + "' and planobject='" +
                                                  aSSRS.GetText(a, 1) + "' ";
                                    String zan = aExeSQL.getOneValue(usql);
                                    if (zan != null && !zan.equals(""))
                                    {
                                        Date[n][15] = zan;
                                    }
                                    else
                                    {
                                        Date[n][15] = "0";
                                    }
                                    //月计划达成率
                                    if (Date[n][15].equals("0"))
                                    {
                                        Date[n][16] = "0";
                                    }
                                    else
                                    {
                                        Date[n][16] = String.valueOf(date1[5] /
                                                (Double.parseDouble(Date[n][15])));
                                    }
                                    n++;
                                }

                            }
                            for (int m = 1; m <= 8; m++)
                            {
                                //计算展业机构 区 加和
                                date3[m] += date2[m];
                            }
                            date3[12] += people; //督导区有业绩人
                            date3[14] += date2[14]; //有效人力
                            people = 0; //清空
                            date2 = new double[15]; //清空
                        }
                        for (int m = 1; m <= 8; m++)
                        {
                            //计算展业机构：支公司值
                            date4[m] += date3[m];
                        }
                        people1 += date3[12]; //支公司 有业绩的人
                        //支 人均标保
                        if (date4[1] == 0)
                        {
                            date4[9] = 0;
                        }
                        else
                        {
                            date4[9] = date4[7] / date4[1];
                        }
                        //支 人均件数
                        if (date4[1] == 0)
                        {
                            date4[10] = 0;
                        }
                        else
                        {
                            date4[10] = date4[8] / date4[1];
                        }
                        //支 件均标保
                        if (date4[8] == 0)
                        {
                            date4[11] = 0;
                        }
                        else
                        {
                            date4[11] = date4[7] / date4[8];
                        }
                        //支 活动率
                        if (date4[1] == 0)
                        {
                            date4[12] = 0;
                        }
                        else
                        {
                            date4[12] = people1 / date4[1];
                        }
                        //支 人均产能
                        if (people1 == 0)
                        {
                            date4[13] = 0;
                        }
                        else
                        {
                            date4[13] = date4[7] / people1;
                        }
                        //有效人力
                        date4[14] += date3[14];
                        date3 = new double[15]; //清空
                        people2 += people1; //支公司 有业绩的人
                        people1 = 0;
                    }
                    //插入支公司值
                    if (y == 2)
                    {
                        Date[n][0] = ySSRS.GetText(z, 2); //支公司名
                        for (int p = 1; p <= 14; p++)
                        {
                            Date[n][p] = String.valueOf(date4[p]);
                        }
                        Date[n][15] = "";
                        Date[n][16] = "";
                        n++;
                    }
                }
                for (int m = 1; m <= 8; m++)
                {
                    //计算展业机构：分公司值
                    date[m] += date4[m];
                }

                //分 人均标保
                if (date[1] == 0)
                {
                    date[9] = 0;
                }
                else
                {
                    date[9] = date[7] / date[1];
                }
                //分 人均件数
                if (date[1] == 0)
                {
                    date[10] = 0;
                }
                else
                {
                    date[10] = date[8] / date[1];
                }
                //分 件均标保
                if (date[8] == 0)
                {
                    date[11] = 0;
                }
                else
                {
                    date[11] = date[7] / date[8];
                }
                //分 活动率
                if (date[1] == 0)
                {
                    date[12] = 0;
                }
                else
                {
                    date[12] = people2 / date[1]; //区 活动率
                }
                //支 人均产能
                if (people2 == 0)
                {
                    date[13] = 0;
                }
                else
                {
                    date[13] = date[7] / people2;
                }
                //有效人力
                date[14] += date4[14];
                date4 = new double[15]; //清空
            }

            //END 支公司循环
            double compare[] = new double[n];
            for (int q = 0; q < n; q++)
            {
                if (!Date[q][16].equals("") && Date[q][16] != null)
                {
                    compare[q] = Double.parseDouble(Date[q][16]);
                }
                else
                {
                    compare[q] = Double.parseDouble("0");
                }
            }
            //将月计划达成率排序
            Arrays.sort(compare);
            for (int r = 0; r < n; r++)
            {
                for (int s = 0; s < n; s++)
                {
                    if (!String.valueOf(compare[s]).equals("") &&
                        String.valueOf(compare[s]) != null &&
                        !Date[r][16].equals("") && Date[r][16] != null)
                    {
                        if (Double.parseDouble(Date[r][16]) == compare[s])
                        {
                            Date[r][17] = String.valueOf(n - s - 1);
                            break;
                        }
                    }
                    else
                    {
                        Date[r][17] = "";
                    }
                }
            }

//将数组中的值插入模板
            for (int t = 0; t < n; t++)
            {
                strArr = new String[18];
                for (int v = 0; v < 18; v++)
                {
                    strArr[v] = Date[t][v];
                }
                tlistTable.add(strArr);
            }
            //展业机构：总计
            strArr = new String[18];
            strArr[0] = "分公司总计";
            for (int u = 1; u <= 14; u++)
            {
                strArr[u] = String.valueOf(date[u]);
            }
            strArr[15] = "";
            strArr[16] = "";
            strArr[17] = "";
            tlistTable.add(strArr);
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("PolFiliale.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
//  xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PolFilialeBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    /*
       如果一个字符串数字中小数点后有值，则去掉小数点后面数值
     */

    public static void main(String[] args)
    {
        PolNBDepartF1PBL tPolNBDepartF1PBL = new PolNBDepartF1PBL();
    }
}