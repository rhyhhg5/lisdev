package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAIndexInfoQueryUI
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String tManageCom;
    private String tIndexCalNo;
    private String tAgentGrade;
    private String tIndexType;
    private String tAgentCode;
    public LAIndexInfoQueryUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }

            // 准备传往后台的数据
            VData vData = new VData();

            if (!prepareOutputData(vData))
            {
                return false;
            }

            LAIndexInfoQueryBL tLAIndexInfoQueryBL = new LAIndexInfoQueryBL();
            System.out.println("Start LAIndexInfoQueryUI Submit ...");

            if (!tLAIndexInfoQueryBL.submitData(vData, cOperate))
            {
                if (tLAIndexInfoQueryBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tLAIndexInfoQueryBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData",
                               "LAIndexInfoQueryBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tLAIndexInfoQueryBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CError cError = new CError();
            cError.moduleName = "LAIndexInfoQueryUI";
            cError.functionName = "submit";
            cError.errorMessage = e.toString();
            mErrors.addOneError(cError);
            return false;
        }
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.add(tManageCom);
            vData.add(tIndexCalNo);
            vData.add(tAgentGrade);
            vData.add(tIndexType);
            vData.add(tAgentCode);
            //vData.add(mGlobalInput);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        this.tManageCom = (String) cInputData.get(0);
        this.tIndexCalNo = (String) cInputData.get(1);
        this.tAgentGrade = (String) cInputData.get(2);
        this.tIndexType = (String) cInputData.get(3);
        this.tAgentCode = (String) cInputData.get(4);
        //mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",5));
        //System.out.println("start");
        //System.out.println(mGlobalInput.Operator);
        //System.out.println(mGlobalInput.ManageCom);

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LAIndexInfoQueryUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
        String tManageCom = "86320000";
        String tIndexCalNo = "200311";
        String tAgentGrade = "A01";
        String tIndexType = "维持";
        String tAgentCode = "8632000026";
        VData tVData = new VData();
        tVData.addElement(tManageCom);
        tVData.addElement(tIndexCalNo);
        tVData.addElement(tAgentGrade);
        tVData.addElement(tIndexType);
        tVData.addElement(tAgentCode);
        LAIndexInfoQueryUI tLAIndexInfoQueryUI = new LAIndexInfoQueryUI();
        tLAIndexInfoQueryUI.submitData(tVData, "PRINT");
    }
}