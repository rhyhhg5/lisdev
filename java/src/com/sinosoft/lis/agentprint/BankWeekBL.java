package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class BankWeekBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private GlobalInput mGlobalInput = new GlobalInput();
    private String StartDay = "";
    private String EndDay = "";
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    public BankWeekBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        StartDay = (String) cInputData.get(0);
        EndDay = (String) cInputData.get(1);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "BankWeekBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String strArr[] = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("BankWeek");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDay", StartDay); //输入制表时间
        texttag.add("EndDay", EndDay); //输入制表时间
        strArr = new String[11];
        double data[] = null;
        data = new double[11];
        double data1[] = null;
        data1 = new double[11];
        String asql =
                "select trim(shortname),comcode from ldcom where length(trim(comcode))=4";
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(asql);
            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                for (int b = 1; b <= aSSRS.getMaxCol(); b++)
                {
                    if (b == 1)
                    {
                        strArr[0] = aSSRS.GetText(a, 1); //分公司名称
                    }
                    if (b == 2)
                    {
                        //银代部分
                        String bsql =
                                "select trim(name),agentcom from lacom where banktype='01' and managecom like '" +
                                aSSRS.GetText(a, 2) + "%' order by agentcom";
                        SSRS bSSRS = new SSRS();
                        bSSRS = aExeSQL.execSQL(bsql);
                        for (int c = 1; c <= bSSRS.getMaxRow(); c++)
                        {
                            for (int d = 1; d <= bSSRS.getMaxCol(); d++)
                            {
                                if (d == 1)
                                {
                                    strArr[1] = bSSRS.GetText(c, 1); //银行名称
                                }
                                if (d == 2)
                                {
                                    //渠道网点数
                                    String fsql = "select count(agentcom) from Lacom where sellflag='Y' and CalFlag='Y' and AgentCom like '" +
                                                  bSSRS.GetText(c, 2) + "%'";
                                    strArr[2] = aExeSQL.getOneValue(fsql);
                                    if (strArr[2].equals("-1"))
                                    {
                                        strArr[2] = "0";
                                    }
                                    data[2] += Double.parseDouble(strArr[2]);
                                    //渠道人力
                                    String hsql = "select count(distinct agentcode) from lacomtoagent where RelaType='1' and AgentCom like '" +
                                                  bSSRS.GetText(c, 2) + "%'";
                                    strArr[3] = aExeSQL.getOneValue(hsql);
                                    data[3] += Double.parseDouble(strArr[3]);
                                    //规模保费
                                    String csql = "select nvl(sum(transmoney),0)/10000 from Lacommision where branchtype='3' and tmakeDate>='" +
                                                  StartDay +
                                                  "' and tmakeDate<='" + EndDay +
                                                  "' and AgentCom like '" +
                                                  bSSRS.GetText(c, 2) + "%'";
                                    strArr[4] = aExeSQL.getOneValue(csql);
                                    //本期网均产能
                                    if (strArr[2].equals("0"))
                                    {
                                        strArr[5] = "0";
                                    }
                                    else
                                    {
                                        strArr[5] = String.valueOf(Double.
                                                parseDouble(strArr[4]) /
                                                Double.parseDouble(strArr[2]));
                                    }
                                    //查询险种
                                    String ksql = "select distinct riskcode from lacommision where branchtype='3' and tmakeDate<='" +
                                                  EndDay +
                                                  "' and AgentCom like '" +
                                                  bSSRS.GetText(c, 2) + "%'";
                                    SSRS dSSRS = new SSRS();
                                    dSSRS = aExeSQL.execSQL(ksql);
                                    for (int g = 1; g <= dSSRS.getMaxRow(); g++)
                                    {
                                        //本周保费
                                        String jsql = "select nvl(sum(transmoney),0)/10000 from Lacommision where branchtype='3' and tmakeDate>='" +
                                                StartDay + "' and tmakeDate<='" +
                                                EndDay +
                                                "' and AgentCom like '" +
                                                bSSRS.GetText(c, 2) +
                                                "%' and riskcode='" +
                                                dSSRS.GetText(1, g) + "'";
                                        strArr[7] = aExeSQL.getOneValue(jsql);
                                        data[7] += Double.parseDouble(strArr[7]);
                                        //险种名称，累计保费
                                        String dsql = "select Riskname,nvl(sum(transmoney),0)/10000 from Lacommision a,LMRiskApp b where a.branchtype='3' and a.riskcode=b.riskcode and tmakeDate>='" +
                                                EndDay.substring(0, 4) +
                                                "-01-01" + "' and tmakeDate<='" +
                                                EndDay +
                                                "' and AgentCom like '" +
                                                bSSRS.GetText(c, 2) +
                                                "%' and a.riskcode='" +
                                                dSSRS.GetText(1, g) +
                                                "' group by riskname";
                                        SSRS cSSRS = new SSRS();
                                        cSSRS = aExeSQL.execSQL(dsql);
                                        if (cSSRS.getMaxRow() != 0)
                                        {
                                            for (int f = 1;
                                                    f <= cSSRS.getMaxCol(); f++)
                                            {
                                                if (f == 1)
                                                {
                                                    strArr[6] = cSSRS.GetText(1,
                                                            1);
                                                }
                                                if (f == 2)
                                                {
                                                    strArr[8] = cSSRS.GetText(1,
                                                            2);
                                                    data[8] += Double.
                                                            parseDouble(strArr[
                                                            8]);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            strArr[6] = "";
                                            strArr[8] = "0";
                                        }
                                        //预收保费
                                        String esql = "select sum(a) from (select nvl(sum(prem),0)/10000 a from Lcpol where uwflag<>'a' and makeDate>='"
                                                + EndDay.substring(0, 4) +
                                                "-01-01" + "' and makeDate<='" +
                                                EndDay +
                                                "' and AgentCom like '"
                                                + bSSRS.GetText(c, 2) +
                                                "%' and riskcode='" +
                                                dSSRS.GetText(1, g) +
                                                "' and salechnl='03' union "
                                                +
                                                " select nvl(sum(prem),0)/10000 a from Lbpol where uwflag<>'a' and makeDate>='"
                                                + EndDay.substring(0, 4) +
                                                "-01-01" + "' and makeDate<='" +
                                                EndDay +
                                                "' and AgentCom like '"
                                                + bSSRS.GetText(c, 2) +
                                                "%' and riskcode='" +
                                                dSSRS.GetText(1, g) +
                                                "' and salechnl='03')";
                                        strArr[9] = aExeSQL.getOneValue(esql);
                                        data[9] += Double.parseDouble(strArr[9]);
                                        //累计退保
                                        String isql = "select nvl(sum(transmoney),0)/10000 from lacommision where branchtype='3' and tmakeDate>='" +
                                                EndDay.substring(0, 4) +
                                                "-01-01" + "' and tmakeDate<='" +
                                                EndDay +
                                                "' and AgentCom like '" +
                                                bSSRS.GetText(c, 2) +
                                                "%' and riskcode='" +
                                                dSSRS.GetText(1, g) +
                                                "' and transmoney<'0'";
                                        strArr[10] = aExeSQL.getOneValue(isql);
                                        data[10] +=
                                                Double.parseDouble(strArr[10]);
                                        if (g != 1)
                                        {
                                            strArr[4] = "";
                                            strArr[5] = "";
                                        }
                                        tlistTable.add(strArr);
                                        strArr = new String[11];
                                    }
                                }
                            }
                        }
                        for (int z = 0; z < 2; z++)
                        {
                            if (z == 0)
                            {
                                strArr = new String[11];
                                strArr[1] = "专业机构";
                                //渠道网点数:有销售资格、并统计
                                String fsql = "select count(agentcom) from Lacom where banktype is null and sellflag='Y' and managecom like '" +
                                              aSSRS.GetText(a, 2) +
                                              "%' and AgentCom like '20%'";
                                strArr[2] = aExeSQL.getOneValue(fsql);
                                data[2] += Double.parseDouble(strArr[2]);
                                //渠道组人力
                                String hsql = "select count(distinct agentcode) from lacomtoagent where RelaType='1' and AgentCom like '20" +
                                              aSSRS.GetText(a, 2).substring(2,
                                        4) + "%' ";
                                strArr[3] = aExeSQL.getOneValue(hsql);
                                data[3] += Double.parseDouble(strArr[3]);
                                //规模保费
                                String csql = "select nvl(sum(transmoney),0)/10000 from Lacommision where branchtype='3' and tmakeDate>='" +
                                              StartDay + "' and tmakeDate<='" +
                                              EndDay + "' and managecom like '" +
                                              aSSRS.GetText(a, 2) +
                                              "%' and AgentCom like '20%'";
                                strArr[4] = aExeSQL.getOneValue(csql);
                                //本期网均产能
                                if (strArr[2].equals("0"))
                                {
                                    strArr[5] = "0";
                                }
                                else
                                {
                                    strArr[5] = String.valueOf(Double.
                                            parseDouble(strArr[4]) /
                                            Double.parseDouble(strArr[2]));
                                }
                                //查询险种
                                String ksql = "select distinct riskcode from lacommision where branchtype='3' and tmakeDate<='" +
                                              EndDay + "' and managecom like '" +
                                              aSSRS.GetText(a, 2) +
                                              "%' and AgentCom like '20%'";
                                SSRS dSSRS = new SSRS();
                                dSSRS = aExeSQL.execSQL(ksql);
                                for (int g = 1; g <= dSSRS.getMaxRow(); g++)
                                {
                                    //查询对应银行下的不同险种和本周保费
                                    String jsql = "select nvl(sum(transmoney),0)/10000 from Lacommision where branchtype='3' and tmakeDate>='" +
                                                  StartDay +
                                                  "' and tmakeDate<='" + EndDay +
                                                  "' and AgentCom like '20%'  and managecom like '" +
                                                  aSSRS.GetText(a, 2) +
                                                  "%' and riskcode='" +
                                                  dSSRS.GetText(1, g) + "'";
                                    strArr[7] = aExeSQL.getOneValue(jsql);
                                    data[7] += Double.parseDouble(strArr[7]);
                                    //险种名称，累计保费
                                    String dsql = "select Riskname,nvl(sum(transmoney),0)/10000 from Lacommision a,LMRiskApp b where a.branchtype='3' and a.riskcode=b.riskcode and tmakeDate>='" +
                                                  EndDay.substring(0, 4) +
                                                  "-01-01" +
                                                  "' and tmakeDate<='" + EndDay +
                                                  "' and AgentCom like '20%'  and managecom like '" +
                                                  aSSRS.GetText(a, 2) +
                                                  "%' and a.riskcode='" +
                                                  dSSRS.GetText(1, g) +
                                                  "' group by riskname";
                                    SSRS cSSRS = new SSRS();
                                    cSSRS = aExeSQL.execSQL(dsql);

                                    if (cSSRS.getMaxRow() != 0)
                                    {
                                        for (int f = 1; f <= cSSRS.getMaxCol();
                                                f++)
                                        {
                                            if (f == 1)
                                            {
                                                strArr[6] = cSSRS.GetText(1, 1);
                                            }
                                            if (f == 2)
                                            {
                                                strArr[8] = cSSRS.GetText(1, 2);
                                                data[8] +=
                                                        Double.parseDouble(
                                                        strArr[
                                                        8]);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        strArr[6] = "";
                                        strArr[8] = "0";
                                    }
                                    //预收保费
                                    String esql = "select sum(a) from (select nvl(sum(prem),0)/10000 a from Lcpol where uwflag<>'a' and makeDate>='"
                                                  + EndDay.substring(0, 4) +
                                                  "-01-01" +
                                                  "' and makeDate<='" + EndDay +
                                                  "' and managecom like '"
                                                  + aSSRS.GetText(a, 2) +
                                                  "%' and AgentCom like '20%' and riskcode='" +
                                                  dSSRS.GetText(1, g) +
                                                  "' and salechnl='03' union "
                                                  +
                                                  " select nvl(sum(prem),0)/10000 a from Lbpol where uwflag<>'a' and makeDate>='"
                                                  + EndDay.substring(0, 4) +
                                                  "-01-01" +
                                                  "' and makeDate<='" + EndDay +
                                                  "' and managecom like '"
                                                  + aSSRS.GetText(a, 2) +
                                                  "%' and AgentCom like '20%' and riskcode='" +
                                                  dSSRS.GetText(1, g) +
                                                  "' and salechnl='03')";

                                    strArr[9] = aExeSQL.getOneValue(esql);
                                    data[9] += Double.parseDouble(strArr[9]);
                                    //累计退保
                                    String isql = "select nvl(sum(transmoney),0)/10000 from lacommision where branchtype='3' and tmakeDate>='" +
                                                  EndDay.substring(0, 4) +
                                                  "-01-01" +
                                                  "' and tmakeDate<='" + EndDay +
                                                  "' and managecom like '" +
                                                  aSSRS.GetText(a, 2) +
                                                  "%' and AgentCom like '20%' and riskcode='" +
                                                  dSSRS.GetText(1, g) +
                                                  "' and transmoney<'0'";
                                    strArr[10] = aExeSQL.getOneValue(isql);
                                    data[10] += Double.parseDouble(strArr[10]);
                                    if (g != 1)
                                    {
                                        strArr[4] = "";
                                        strArr[5] = "";
                                    }
                                    tlistTable.add(strArr);
                                    strArr = new String[11];
                                }
                            }
                            if (z == 1)
                            {
                                strArr = new String[11];
                                strArr[1] = "兼业机构";
                                //渠道网点数:有销售资格、并统计
                                String fsql = "select count(agentcom) from Lacom where banktype is null and sellflag='Y' and managecom like '" +
                                              aSSRS.GetText(a, 2) +
                                              "%' and AgentCom like '21%' ";
                                strArr[2] = aExeSQL.getOneValue(fsql);
                                data[2] += Double.parseDouble(strArr[2]);
                                //渠道组人力
                                String hsql = "select count(distinct agentcode) from lacomtoagent where RelaType='1' and AgentCom like '21" +
                                              aSSRS.GetText(a, 2).substring(2,
                                        4) + "%' ";
                                strArr[3] = aExeSQL.getOneValue(hsql);
                                data[3] += Double.parseDouble(strArr[3]);
                                //规模保费
                                String csql = "select nvl(sum(transmoney),0)/10000 from Lacommision where branchtype='3' and tmakeDate>='" +
                                              StartDay + "' and tmakeDate<='" +
                                              EndDay + "' and managecom like '" +
                                              aSSRS.GetText(a, 2) +
                                              "%' and AgentCom like '21%'";
                                strArr[4] = aExeSQL.getOneValue(csql);
                                //本期网均产能
                                if (strArr[2].equals("0"))
                                {
                                    strArr[5] = "0";
                                }
                                else
                                {
                                    strArr[5] = String.valueOf(Double.
                                            parseDouble(strArr[4]) /
                                            Double.parseDouble(strArr[2]));
                                }
                                //查询险种
                                String ksql = "select distinct riskcode from lacommision where branchtype='3' and tmakeDate<='" +
                                              EndDay +
                                              "'  and managecom like '" +
                                              aSSRS.GetText(a, 2) +
                                              "%' and AgentCom like '21%'";
                                SSRS dSSRS = new SSRS();
                                dSSRS = aExeSQL.execSQL(ksql);
                                for (int g = 1; g <= dSSRS.getMaxRow(); g++)
                                {
                                    //查询对应银行下的不同险种和本周保费
                                    String jsql = "select nvl(sum(transmoney),0)/10000 from Lacommision where branchtype='3' and tmakeDate>='" +
                                                  StartDay +
                                                  "' and tmakeDate<='" + EndDay +
                                                  "' and managecom like '" +
                                                  aSSRS.GetText(a, 2) +
                                                  "%' and AgentCom like '21%' and riskcode='" +
                                                  dSSRS.GetText(1, g) + "'";
                                    strArr[7] = aExeSQL.getOneValue(jsql);
                                    data[7] += Double.parseDouble(strArr[7]);
                                    //险种名称，累计保费
                                    String dsql = "select Riskname,nvl(sum(transmoney),0)/10000 from Lacommision a,LMRiskApp b where a.branchtype='3' and a.riskcode=b.riskcode and tmakeDate>='" +
                                                  EndDay.substring(0, 4) +
                                                  "-01-01" +
                                                  "' and tmakeDate<='" + EndDay +
                                                  "'  and managecom like '" +
                                                  aSSRS.GetText(a, 2) +
                                                  "%' and AgentCom like '21%' and a.riskcode='" +
                                                  dSSRS.GetText(1, g) +
                                                  "' group by riskname";
                                    SSRS cSSRS = new SSRS();
                                    cSSRS = aExeSQL.execSQL(dsql);
                                    if (cSSRS.getMaxRow() != 0)
                                    {
                                        for (int f = 1; f <= cSSRS.getMaxCol();
                                                f++)
                                        {
                                            if (f == 1)
                                            {
                                                strArr[6] = cSSRS.GetText(1, 1);
                                            }
                                            if (f == 2)
                                            {
                                                strArr[8] = cSSRS.GetText(1, 2);
                                                data[8] +=
                                                        Double.parseDouble(
                                                        strArr[
                                                        8]);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        strArr[6] = "";
                                        strArr[8] = "0";
                                    }
                                    //预收保费
                                    String esql = "select sum(a) from (select nvl(sum(prem),0)/10000 a from Lcpol where uwflag<>'a' and makeDate>='"
                                                  + EndDay.substring(0, 4) +
                                                  "-01-01" +
                                                  "' and makeDate<='" + EndDay +
                                                  "' and managecom like '"
                                                  + aSSRS.GetText(a, 2) +
                                                  "%' and AgentCom like '21%' and riskcode='" +
                                                  dSSRS.GetText(1, g) +
                                                  "' and salechnl='03' union "
                                                  +
                                                  " select nvl(sum(prem),0)/10000 a from Lbpol where uwflag<>'a' and makeDate>='"
                                                  + EndDay.substring(0, 4) +
                                                  "-01-01" +
                                                  "' and makeDate<='" + EndDay +
                                                  "' and managecom like '"
                                                  + aSSRS.GetText(a, 2) +
                                                  "%' and AgentCom like '21%' and riskcode='" +
                                                  dSSRS.GetText(1, g) +
                                                  "' and salechnl='03')";
                                    strArr[9] = aExeSQL.getOneValue(esql);
                                    data[9] += Double.parseDouble(strArr[9]);
                                    //累计退保
                                    String isql = "select nvl(sum(transmoney),0)/10000 from lacommision where branchtype='3' and tmakeDate>='" +
                                                  EndDay.substring(0, 4) +
                                                  "-01-01" +
                                                  "' and tmakeDate<='" + EndDay +
                                                  "' and managecom like '" +
                                                  aSSRS.GetText(a, 2) +
                                                  "%' and AgentCom like '21%' and riskcode='" +
                                                  dSSRS.GetText(1, g) +
                                                  "' and transmoney<'0'";
                                    strArr[10] = aExeSQL.getOneValue(isql);
                                    data[10] += Double.parseDouble(strArr[10]);
                                    if (g != 1)
                                    {
                                        strArr[4] = "";
                                        strArr[5] = "";
                                    }
                                    tlistTable.add(strArr);
                                    strArr = new String[11];

                                }
                            }
                        }
                        //小合计
                        strArr = new String[11];
                        strArr[0] = aSSRS.GetText(a, 1) + "合计";
                        //网点数
                        strArr[2] = getInt(String.valueOf(data[2]));
                        data1[2] += data[2];
                        //人力
                        strArr[3] = getInt(String.valueOf(data[3]));
                        data1[3] += data[3];
                        //保费
                        strArr[4] = String.valueOf(data[4]);
                        data1[4] += data[4];
                        for (int i = 7; i <= 10; i++)
                        {
                            strArr[i] = String.valueOf(data[i]);
                            data1[i] += data[i];
                        }
                        //本期网均产能
                        if (strArr[2].equals("0"))
                        {
                            strArr[5] = "0";
                        }
                        else
                        {
                            strArr[5] = String.valueOf(Double.parseDouble(
                                    strArr[4]) / Double.parseDouble(strArr[2]));
                        }
                        tlistTable.add(strArr);
                        strArr = new String[11];
                        data = new double[11];
                    }
                }
            }

            strArr = new String[11];
            strArr[0] = "总计";
            //网点数
            strArr[2] = getInt(String.valueOf(data1[2]));
            //人力
            strArr[3] = getInt(String.valueOf(data1[3]));
            //保费
            strArr[4] = String.valueOf(data1[4]);
            for (int i = 7; i <= 10; i++)
            {
                strArr[i] = String.valueOf(data1[i]);
            }
            //本期网均产能
            if (strArr[2].equals("0"))
            {
                strArr[5] = "0";
            }
            else
            {
                strArr[5] = String.valueOf(Double.parseDouble(strArr[4]) /
                                           Double.parseDouble(strArr[2]));
            }
            tlistTable.add(strArr);
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("BankWeek.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankWeekBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    /*
       如果一个字符串数字中小数点后有值，则去掉小数点后面数值
     */
    public static String getInt(String Value)
    {
        String result = "";
        boolean mflag = true;
        int m = 0;
        m = Value.lastIndexOf(".");
        if (m == -1)
        {
            result = Value;
        }
        else
        {
            result = Value.substring(0, m);
        }
        return result;
    }

    public static void main(String[] args)
    {
    }
}