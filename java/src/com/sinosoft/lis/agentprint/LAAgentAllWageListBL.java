package com.sinosoft.lis.agentprint;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.*;
import java.text.DecimalFormat;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class LAAgentAllWageListBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    private String mType = null;
    private String mStartMonth = null;
    private String mEndMonth = null;
    SSRS tSSRS=new SSRS();
    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    public LAAgentAllWageListBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LAAgentAllWageListBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 2][50];

       mToExcel[0][2] = "机构收入明细汇总表" + "   统计时间："+mStartMonth+"——"+mEndMonth+" 说明： 本期应发佣金=税前薪资-差勤扣款-个人所得税-个人营业税 +扣款- 养老金计提   本期实发佣金=本期应发佣金+上次佣金余额";
       
       if(mType.equals("A")||mType.equals("B"))
       {
       mToExcel[1][0]="机构编码 ";        
       mToExcel[1][1]="机构名称";
       }
       else
       {
    	   mToExcel[1][0]="营业部编码 ";        
           mToExcel[1][1]="营业部名称";   
       }
       mToExcel[1][2]="首佣";
       mToExcel[1][3]="续期佣金";
       mToExcel[1][4]="续保佣金";
       mToExcel[1][5]="晋升奖 ";        
       mToExcel[1][6]="续期服务奖";
       mToExcel[1][7]="岗位津贴";
       mToExcel[1][8]="岗位津贴补发";
       mToExcel[1][9]="季度奖";      
       mToExcel[1][10]="增员奖";
       mToExcel[1][11]="晋升奖";
       mToExcel[1][12]="责任津贴";
       mToExcel[1][13]="职务津贴";
       mToExcel[1][14]="职务津贴补发";
       mToExcel[1][15]="管理津贴";
       mToExcel[1][16]="直接养成津贴";        
       mToExcel[1][17]="间接养成津贴";
       mToExcel[1][18]="新人津贴（筹）";
       mToExcel[1][19]="财务支持费用（筹）";
       mToExcel[1][20]="加款（含财补）";
       mToExcel[1][21]="税前薪资";
       mToExcel[1][22]="差勤扣款";        
       mToExcel[1][23]="个人所得税";
       mToExcel[1][24]="个人营业税";
       mToExcel[1][25]="扣款";
       mToExcel[1][26]="养老金计提";
       mToExcel[1][27]="本期应发佣金";
       mToExcel[1][28]="上次佣金余额";
       mToExcel[1][29]="本期实发佣金";


      
       
       
        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row +1][col - 1] = tSSRS.GetText(row, col);
            }
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentAllWageListBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        mType = (String) tf.getValueByName("Type");
        mStartMonth = (String) tf.getValueByName("StartMonth");
        mEndMonth = (String) tf.getValueByName("EndMonth");
        System.out.println("msql:"+mSql);
        System.out.println("path:"+mOutXmlPath);
        if(mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentAllWageListBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }
   
    

       
    public static void main(String[] args)
    {
        LAAgentAllWageListBL LAAgentAllWageListBL = new            LAAgentAllWageListBL();
    System.out.println("11111111");
    }
}
