package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LaChargeBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private String StartDay = "";
    private String EndDay = "";
    private String AgentCom = "";
    private String BankType = "";
    private String ChargeType = "";
    private String RiskCode = "";

    private String strArr[] = new String[8];
    private double data[] = new double[8];
    private ListTable tlistTable = new ListTable();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    public LaChargeBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!queryDataOther())
        {
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        StartDay = (String) cInputData.get(0);
        EndDay = (String) cInputData.get(1);
        AgentCom = (String) cInputData.get(2);
        BankType = (String) cInputData.get(3);
        ChargeType = (String) cInputData.get(4);
        RiskCode = (String) cInputData.get(5);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 6));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "AgentChargeBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryDataOther()
    {
        String strArr[] = null;
        strArr = new String[8];
        double data[] = new double[8];
        ListTable tlistTable = new ListTable();
        tlistTable.setName("LaCharge");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDay", StartDay + "---" + EndDay);
        String aasql = "select trim(name) from lacom where agentcom='" +
                       AgentCom + "'";
        ExeSQL aaExeSQL = new ExeSQL();
        texttag.add("AgentCom", aaExeSQL.getOneValue(aasql));

        //支行名称
        String asql = "select distinct trim(a.name),a.agentcom from lacom a,lacommision b where b.agentcom like '" +
                      AgentCom + "%' and a.banktype='" + BankType +
                      "' and a.agentcom=b.agentcom";
        ExeSQL aExeSQL = new ExeSQL();
        SSRS aSSRS = new SSRS();
        aSSRS = aExeSQL.execSQL(asql);
        for (int a = 1; a <= aSSRS.getMaxRow(); a++)
        {
            for (int b = 1; b <= aSSRS.getMaxCol(); b++)
            {
                if (b == 1)
                {
                    strArr[0] = aSSRS.GetText(a, 1);
                }
                if (b == 2)
                {
                    if (RiskCode.equals("") || RiskCode == null)
                    {
                        //险种名称
                        String bsql = " select trim(riskname),riskcode from lmriskapp where riskcode in (select riskcode from lacommision where  SignDate>='" +
                                      StartDay + "' and SignDate<='" + EndDay +
                                      "' and agentcom='" + AgentCom + "')";
                        ExeSQL bExeSQL = new ExeSQL();
                        SSRS bSSRS = new SSRS();
                        bSSRS = bExeSQL.execSQL(bsql);
                        for (int c = 1; c <= bSSRS.getMaxRow(); c++)
                        {
                            for (int d = 1; d <= bSSRS.getMaxCol(); d++)
                            {
                                if (c == 1)
                                {
                                    strArr[1] = bSSRS.GetText(c, 1);
                                }
                                if (c == 2)
                                {
                                    //保费收入
                                    String csql = "select nvl(sum(transmoney),0) from lacommision where transmoney>0 and  SignDate>='" +
                                                  StartDay +
                                                  "' and SignDate<='" + EndDay +
                                                  "' and riskcode='" +
                                                  bSSRS.GetText(b, 2) +
                                                  "' and agentcom='" +
                                                  aSSRS.GetText(a, 2) + "'";
                                    ExeSQL cExeSQL = new ExeSQL();
                                    strArr[2] = cExeSQL.getOneValue(csql);
                                    data[2] +=
                                            Double.parseDouble(strArr[2].trim());
                                    //撤保收入
                                    String dsql = " select nvl(sum(transmoney),0) from lacommision where transmoney<0  and  SignDate>='" +
                                                  StartDay +
                                                  "' and SignDate<='" + EndDay +
                                                  "' and riskcode='" +
                                                  bSSRS.GetText(b, 2) +
                                                  "' and agentcom='" +
                                                  aSSRS.GetText(a, 2) + "'";
                                    ExeSQL dExeSQL = new ExeSQL();
                                    strArr[3] = dExeSQL.getOneValue(dsql);
                                    data[3] +=
                                            Double.parseDouble(strArr[3].trim());
                                    //结算保费小计
                                    String esql =
                                            "select nvl(sum(transmoney),0) from lacommision where  SignDate>='" +
                                            StartDay + "' and SignDate<='" +
                                            EndDay + "' and riskcode='" +
                                            bSSRS.GetText(b, 2) +
                                            "' and agentcom='" +
                                            aSSRS.GetText(a, 2) + "'";
                                    ExeSQL eExeSQL = new ExeSQL();
                                    strArr[4] = eExeSQL.getOneValue(esql);
                                    data[4] +=
                                            Double.parseDouble(strArr[4].trim());
                                    //比例
                                    String fsql = "select trim(nvl(sum(chargerate),0)),trim(nvl(sum(charge),0)) from lacharge a,lacommision b where a.chargetype='" +
                                                  ChargeType +
                                                  "' and a.caldate>='" +
                                                  StartDay +
                                                  "' and a.caldate<='" + EndDay +
                                                  "' and a.riskcode='" +
                                                  bSSRS.GetText(b, 2) +
                                                  "' and a.agentcom='" +
                                                  aSSRS.GetText(a, 2) +
                                                  "' and a.commisionsn=b.commisionsn";
                                    ExeSQL fExeSQL = new ExeSQL();
                                    SSRS cSSRS = new SSRS();
                                    cSSRS = fExeSQL.execSQL(fsql);
                                    for (int e = 1; e <= cSSRS.getMaxRow(); e++)
                                    {
                                        strArr[5] = cSSRS.GetText(e, 1);
                                        //金额
                                        strArr[6] = cSSRS.GetText(e, 2);
                                        data[6] +=
                                                Double.parseDouble(strArr[6].
                                                trim());
                                    }
                                    //备注
                                }
                            }
                        }
                    }

                    else
                    {
                        String hsql =
                                "select trim(riskname) from lmriskapp where riskcode='" +
                                RiskCode + "'";
                        ExeSQL hExeSQL = new ExeSQL();
                        strArr[1] = hExeSQL.getOneValue(hsql);
                        //保费收入
                        String isql = "select nvl(sum(transmoney),0) from lacommision where transmoney>0 and  SignDate>='" +
                                      StartDay + "' and SignDate<='" + EndDay +
                                      "' and riskcode='" + RiskCode +
                                      "' and agentcom='" + aSSRS.GetText(a, 2) +
                                      "'";
                        ExeSQL iExeSQL = new ExeSQL();
                        strArr[2] = iExeSQL.getOneValue(isql);
                        data[2] += Double.parseDouble(strArr[2].trim());
                        //撤保收入
                        String jsql = " select nvl(sum(transmoney),0) from lacommision where transmoney<0  and  SignDate>='" +
                                      StartDay + "' and SignDate<='" + EndDay +
                                      "' and riskcode='" + RiskCode +
                                      "' and agentcom='" + aSSRS.GetText(a, 2) +
                                      "'";
                        ExeSQL jExeSQL = new ExeSQL();
                        strArr[3] = jExeSQL.getOneValue(jsql);
                        data[3] += Double.parseDouble(strArr[3].trim());
                        //结算保费小计
                        String ksql =
                                "select nvl(sum(transmoney),0) from lacommision where  SignDate>='" +
                                StartDay + "' and SignDate<='" + EndDay +
                                "' and riskcode='" + RiskCode +
                                "' and agentcom='" + aSSRS.GetText(a, 2) + "'";
                        ExeSQL kExeSQL = new ExeSQL();
                        strArr[4] = kExeSQL.getOneValue(ksql);
                        data[4] += Double.parseDouble(strArr[4].trim());
                        //比例
                        String lsql = "select trim(nvl(sum(chargerate),0)),trim(nvl(sum(charge),0)) from lacharge a,lacommision b where a.chargetype='" +
                                      ChargeType + "' and a.caldate>='" +
                                      StartDay + "' and a.caldate<='" + EndDay +
                                      "' and a.riskcode='" + RiskCode +
                                      "' and a.agentcom='" + aSSRS.GetText(a, 2) +
                                      "' and a.commisionsn=b.commisionsn";
                        ExeSQL lExeSQL = new ExeSQL();
                        SSRS dSSRS = new SSRS();
                        dSSRS = lExeSQL.execSQL(lsql);
                        for (int f = 1; f <= dSSRS.getMaxRow(); f++)
                        {
                            strArr[5] = dSSRS.GetText(f, 1);
                            //金额
                            strArr[6] = dSSRS.GetText(f, 2);
                            data[6] += Double.parseDouble(strArr[6].trim());
                        }
                        //备注
                    }
                    tlistTable.add(strArr);
                    strArr = new String[8];

                }
            }
        }
        //展业机构：总计
        strArr = new String[8];
        strArr[0] = "合计";
        for (int i = 2; i <= 4; i++)
        {
            strArr[i] = String.valueOf(data[i]);
        }
        strArr[5] = String.valueOf("0");
        strArr[6] = String.valueOf(data[6]);
        tlistTable.add(strArr);
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("LaCharge.vts", "printer"); //最好紧接着就初始化xml文档
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
        mResult.addElement(xmlexport);
        System.out.println("end");
        return true;
    }


    public static void main(String[] args)
    {
        PolNBDepartF1PBL tPolNBDepartF1PBL = new PolNBDepartF1PBL();
    }
}