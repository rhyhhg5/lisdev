package com.sinosoft.lis.agentprint;

// Imported Serializer classes
//import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Vector;

// Imported JAVA API for XML Parsing 1.0 classes
import javax.xml.parsers.DocumentBuilderFactory;

import com.f1j.ss.BookModelImpl;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.tb.GrpPolImpInfo;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import org.apache.xpath.XPathAPI;
import org.jdom.output.XMLOutputter;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 投保业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author zy
 * @version 1.0
 */
public class ParseXML
{
//  @Fields
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往前面传输数据的容器 */
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /**内存文件暂存*/
    private org.jdom.Document myDocument;
    private org.jdom.Element myElement;
    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private String mDutyFlag = "0";
    private InputStream DataIn;

    private String FileName;
    private String XmlFileName;

    private String BatchNo;
    private String GrpPolNo;
    private String ID;
    private String InsuredName;


    //节点是否存在标志
    private boolean HaveNodeFlag = true;
//数据Xml解析节点描述
    private String FilePath = "C:/Lis/";
    private String ParseRootPath = "/DATASET/BATCHID";
    private static String PATH_GRPPOLNO =
            "/DATASET/TABLE/ROW/FIELD/GrpPolNo[position()=1]";
    private String ParsePath = "/DATASET/TABLE/ROW";

    //配置文件Xml节点描述
    private String ImportFileName;
    private String ConfigFileName;
    private String ConfigRootPath = "/CONFIG";

    private org.w3c.dom.Document m_doc = null; // 临时的Document对象

    private GrpPolImpInfo m_GrpPolImpInfo = new GrpPolImpInfo();

    private String[] m_strDataFiles = null;

    private String[] mHead = null;
    private String[][] mData = null;
    private String mTable = null;

    private int nRow;
    private int nCol;

    //     @Constructors
    public ParseXML()
    {
//    bulidDocument();
    }

    public ParseXML(String fileName)
    {
        bulidDocument();
        FileName = fileName;
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        mInputData = (VData) cInputData.clone();
        this.getInputData();
        //取上传文件名
        if (!this.checkData())
        {
            return false;
        }
        //开始准备解析xls为xml
        try
        {
            if (!this.parseVts())
            {
                return false;
            }
            System.out.println("生成Xml成功:" + PubFun.getCurrentTime());

            for (int nIndex = 0; nIndex < m_strDataFiles.length; nIndex++)
            {
                XmlFileName = m_strDataFiles[nIndex];
                if (!this.resolveXml()) //解析xml
                {
                    return false;
                }
                if (!insertData())
                {
                    return false;
                }
            }

//      mLCGrpImportLogSet = m_GrpPolImpInfo.getErrors();

            // 显示错误日志
//      if( mLCGrpImportLogSet.size()>0 ) {
//        String tReturn = mLCGrpImportLogSet.encode();
//        tReturn = "0|"+mLCGrpImportLogSet.size()+"^"+tReturn;
//        mResult.clear();
//        mResult.addElement(tReturn);
//      }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "导入文件格式有误!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("结束时间:" + PubFun.getCurrentTime());
        return true;
    }

    /**
     * 得到传入数据
     */
    private void getInputData()
    {
        mGlobalInput = (GlobalInput) mInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
    }

    /**
     * 校验传输数据
     * @return
     */
    private boolean checkData()
    {
        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "无操作员信息，请重新登录!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mTransferData == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "无导入文件信息，请重新导入!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            FileName = (String) mTransferData.getValueByName("FileName");
        }
        return true;
    }

    /**
     * 得到生成文件路径
     * @return
     */
    private boolean getFilePath()
    {
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("TranDataPath");
        if (!tLDSysVarDB.getInfo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseXML";
            tError.functionName = "checkData";
            tError.errorMessage = "缺少文件导入路径!";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            FilePath = tLDSysVarDB.getSysVarValue();
        }

        return true;
    }

    /**
     * 检验文件是否存在
     * @return
     */
    private boolean checkXmlFileName()
    {
//    XmlFileName = (String)mTransferData.getValueByName("FileName");
        File tFile = new File(XmlFileName);
        if (!tFile.exists())
        {
            LDSysVarDB tLDSysVarDB = new LDSysVarDB();
            tLDSysVarDB.setSysVar("TranDataPath");
            if (!tLDSysVarDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ParseXML";
                tError.functionName = "checkData";
                tError.errorMessage = "缺少文件导入路径!";
                this.mErrors.addOneError(tError);
                return false;
            }
            else
            {
                FilePath = tLDSysVarDB.getSysVarValue();
            }

            File tFile1 = new File(FilePath);
            if (!tFile1.exists())
            {
                tFile1.mkdirs();
            }
            XmlFileName = FilePath + XmlFileName;
            File tFile2 = new File(XmlFileName);
            if (!tFile2.exists())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ParseXML";
                tError.functionName = "checkData";
                tError.errorMessage = "请上传相应的数据文件到指定路径" + FilePath + "!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }

    /**
     * 检查导入配置文件是否存在
     * @return
     */
    private boolean checkImportConfig()
    {
        this.getFilePath();
//    FilePath="E:/";
        File tFile1 = new File(FilePath);
        if (!tFile1.exists())
        {
            tFile1.mkdirs();
        }

        ConfigFileName = FilePath + "Import.xml";
        File tFile2 = new File(ConfigFileName);
        if (!tFile2.exists())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseGuideIn";
            tError.functionName = "checkData";
            tError.errorMessage = "请上传配置文件到指定路径" + FilePath + "!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("检查导入配置文件是否存在");
        return true;
    }

    /**
     * 初始化上传文件
     * @return
     */
    private boolean initImportFile()
    {
        this.getFilePath();
        ImportFileName = FilePath + FileName;
        File tFile = new File(ImportFileName);
        if (!tFile.exists())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseXML";
            tError.functionName = "checkData";
            tError.errorMessage = "未上传文件到指定路径" + FilePath + "!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("-----导入文件");
        return true;
    }

    /**
     * 解析操作
     * @return
     */
    private boolean parseVts() throws Exception
    {
        //初始化导入文件
        this.initImportFile(); //确定文件存在
//    this.checkImportConfig();//确定配置文件Import.xml存在

        XlsParserXml gpvp = new XlsParserXml(); //周平的解析xls文件

        gpvp.setFileName(ImportFileName); //设置文件名
//    gpvp.setConfigFileName(ConfigFileName);//设指配置文件名

        gpvp.transform(); // 转换操作，将磁盘投保文件转换成指定格式的XML文件。

        // 得到生成的XML数据文件名
        m_strDataFiles = gpvp.getDataFiles();

        return true;
    }

    /**
     *  输出xml文件，参数为路径，文件名
     * @param pathname
     * @param filename
     */
    private void outputDocumentToFile(String pathname, String filename)
    {
        //setup this like outputDocument
        try
        {
            XMLOutputter outputter = new XMLOutputter("  ", true, "GB2312");
            //output to a file
            String str = pathname + filename + ".xml";
            XmlFileName = str;
            FileWriter writer = new FileWriter(str);
            outputter.output(myDocument, writer);
            writer.close();
        }
        catch (java.io.IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * 增加元素
     * @param aElt
     */
    private void addElement(org.jdom.Element aElt)
    {
        this.myDocument.getRootElement().addContent(aElt);
    }

    /**
     *初始化内存文件
     */
    private void initElement()
    {
        myElement = new org.jdom.Element("DATASET");
        myDocument = new org.jdom.Document(myElement);

        org.jdom.Element tE1 = new org.jdom.Element("BATCHID");
        tE1.setText(this.getBatchID());
        myElement.addContent(tE1);
        myElement.addContent(new org.jdom.Element("TABLE"));
        Vector tV = new Vector();
        tV.addElement("TABLE");
    }

    //提交第一层内的循环体到第一层循环内的指定结点下
    public void AddMuliElement(Vector aVT, org.jdom.Element aElt)
    {
        int NodeCount = aVT.size();
        //org.jdom.Element tElt1 = (org.jdom.Element)myElement.clone();
        org.jdom.Element tElt = myElement;
        int i = 0;
        while (NodeCount > 0)
        {
            i++;
            String NodeName = (String) aVT.get(i - 1);
//      tElt = tElt.getChild((String)aVT.get(i-1));
            tElt = (org.jdom.Element) tElt.getChildren((String) aVT.get(i - 1)).
                   get(tElt.getChildren((String) aVT.get(i - 1)).size() - 1);
            //myElement.addContent(t.Elt);
            NodeCount--;
        }
        tElt.addContent(aElt);
        //myDocument = new org.jdom.Document(tElt1);
    }

    /**
     * 得到Xml开始标签
     * @return
     */
    private String getMarkStart()
    {
        String tStr = "";
        return tStr;
    }

    /**
     * 生成批次号
     * @return
     */
    private String getBatchID()
    {
        String tBatchID = "";
        File tFile = new File(FileName);
        String name = tFile.getName();
        for (int i = name.length() - 1; i >= 0; i--)
        {
            if (name.substring(i - 1, i).equals("."))
            {
                tBatchID = name.substring(0, i - 1);
                //System.out.println("BatchID:"+tBatchID);
                return tBatchID;
            }
        }
        return tBatchID;
    }

    /**
     * 解析xml
     * @return
     */
    private boolean resolveXml()
    {
        System.out.println("-----开始读取Xml文件");

        if (!checkXmlFileName())
        {
            return false;
        }
        File tFile = new File(XmlFileName);
        try
        {
            InputSource in = new InputSource(new FileInputStream(XmlFileName));
            DocumentBuilderFactory dfactory = DocumentBuilderFactory.
                                              newInstance();
            dfactory.setNamespaceAware(true);
            org.w3c.dom.Document doc = dfactory.newDocumentBuilder().parse(in);
            org.w3c.dom.Element ele = doc.getDocumentElement();
            Node table = XPathAPI.selectSingleNode(ele, "/TABLE");
            NamedNodeMap tMap = table.getAttributes();
            Node tAttr = tMap.getNamedItem("name");
            mTable = tAttr.getNodeValue();

            NodeList tHead = XPathAPI.selectNodeList(ele, "/TABLE/HEAD/COL");
            nCol = tHead.getLength();
            mHead = new String[nCol];
            for (int i = 0; i < nCol; i++)
            {
                Node tNode = tHead.item(i);
//        org.w3c.dom.Document m_doc = dfactory.newDocumentBuilder().newDocument();
//        Node NewNode = m_doc.importNode(tNode,true);
//        String strValue = NewNode.getFirstChild().getNodeValue();
                String strValue = tNode.getFirstChild() == null ? "" :
                                  tNode.getFirstChild().getNodeValue();
                mHead[i] = strValue;
            }
            NodeList tData = XPathAPI.selectNodeList(ele, "/TABLE/ROW");
            nRow = tData.getLength();
            mData = new String[nRow][nCol];
            for (int m = 0; m < nRow; m++)
            {
                Node aNode = tData.item(m);
                NodeList aData = XPathAPI.selectNodeList(aNode, "COL");
                for (int n = 0; n < nCol; n++)
                {
                    Node bNode = aData.item(n);
//          Node a = bNode.getFirstChild();
//          String ab = a.getNodeValue();
                    String strValue = bNode.getFirstChild() == null ? "" :
                                      bNode.getFirstChild().getNodeValue();
                    mData[m][n] = strValue;
                }
            }
            System.out.println("111111111111111111111");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseXML";
            tError.functionName = "resolveXml";
            tError.errorMessage = "解析文件出错!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (tFile.exists())
        {
//      boolean a = tFile.delete();
//      System.out.println(a);
            if (!tFile.delete())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ParseGuideIn";
                tError.functionName = "checkData";
                tError.errorMessage = "Xml文件删除失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        System.out.println(this.mErrors.getErrorCount() + "error:" +
                           this.mErrors.getFirstError());
        return true;
    }

    private boolean insertData()
    {
        boolean flag = false;
        try
        {
            String aSQL = "select * from " + mTable;
            Connection tConn = DBConnPool.getConnection();
            if (tConn == null)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ParseXML";
                tError.functionName = "insertData";
                tError.errorMessage = "数据库连接失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            Statement tStat = tConn.createStatement();
            ResultSet rs = tStat.executeQuery(aSQL);
            ResultSetMetaData rsmd = rs.getMetaData();
            for (int m = 0; m < rsmd.getColumnCount(); m++)
            {
                String aName = rsmd.getColumnName(m + 1);
                if (aName.trim().toUpperCase().equals("OPERATOR"))
                {
                    flag = true;
                    break;
                }
            }
            rs.close();
            tStat.close();
            tConn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseXML";
            tError.functionName = "insertData";
            tError.errorMessage = "数据库查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        String tSQL = null;
        String tName = "";
        String tValue = null;
        String curDate = PubFun.getCurrentDate();
        String curTime = PubFun.getCurrentTime();
        Connection conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseXML";
            tError.functionName = "insertData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        try
        {
            conn.setAutoCommit(false);
            for (int i = 0; i < nRow; i++)
            {
                for (int j = 0; j < nCol; j++)
                {
                    if (j == 0)
                    {
                        tName = mHead[j];
                        tValue = "'" + mData[i][j] + "'";
                    }
                    else
                    {
                        tName = tName + "," + mHead[j];
                        tValue = tValue + "," + "'" + mData[i][j] + "'";
                    }
                }
                if (flag == true)
                {
                    tName = tName +
                            ",Operator,MakeDate,MakeTime,ModifyDate,ModifyTime";
                    tValue = tValue + ",'" + mGlobalInput.Operator + "','" +
                             curDate + "','" + curTime + "','" + curDate +
                             "','" + curTime + "'";
                }
                tSQL = "insert into " + mTable + "(" + tName + ") values(" +
                       tValue + ")";
                ExeSQL tExeSQL = new ExeSQL(conn);
                if (!tExeSQL.execUpdateSQL(tSQL))
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "ParseXML";
                    tError.functionName = "insertData";
                    tError.errorMessage = "插入数据失败，请查询导入的Excel文件是否符合要求！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ParseXML";
            tError.functionName = "insertData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }


    public String getExtendFileName(String aFileName)
    {
        File tFile = new File(aFileName);
        String aExtendFileName = "";
        String name = tFile.getName();
        for (int i = name.length() - 1; i >= 0; i--)
        {
            if (i < 1)
            {
                i = 1;
            }
            if (name.substring(i - 1, i).equals("."))
            {
                aExtendFileName = name.substring(i, name.length());
                System.out.println("ExtendFileName;" + aExtendFileName);
                return aExtendFileName;
            }
        }
        return aExtendFileName;
    }

    /**
     * 字符串替换
     * @param s1
     * @param OriginStr
     * @param DestStr
     * @return
     */
    public static String replace(String s1, String OriginStr, String DestStr)
    {
        s1 = s1.trim();
        int mLenOriginStr = OriginStr.length();
        for (int j = 0; j < s1.length(); j++)
        {
            int befLen = s1.length();
            if (s1.substring(j, j + 1) == null ||
                s1.substring(j, j + 1).trim().equals(""))
            {
                continue;
            }
            else
            {
                if (OriginStr != null && DestStr != null)
                {
                    if (j + mLenOriginStr <= s1.length())
                    {

                        if (s1.substring(j, j + mLenOriginStr).equals(OriginStr))
                        {

                            OriginStr = s1.substring(j, j + mLenOriginStr);

                            String startStr = s1.substring(0, j);
                            String endStr = s1.substring(j + mLenOriginStr,
                                    s1.length());

                            s1 = startStr + DestStr + endStr;

                            j = j + s1.length() - befLen;
                            if (j < 0)
                            {
                                j = 0;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        return s1;
    }

    private String parseNode(Node aNode, String aParsePath)
    {

        String aNodeValue = "";
        HaveNodeFlag = true;
        try
        {
//       System.out.println("try:"+aNodeValue+" path:"+aParsePath);
            if (XPathAPI.selectSingleNode(aNode, aParsePath) == null)
            {
                HaveNodeFlag = false;
                return null;
            }
            if (XPathAPI.selectSingleNode(aNode, aParsePath).getFirstChild() == null)
            {
                HaveNodeFlag = false;
                return null;
            }
            aNodeValue = XPathAPI.selectSingleNode(aNode, aParsePath).
                         getFirstChild().getNodeValue();

        }
        catch (Exception ex)
        {
            HaveNodeFlag = false;
            System.out.println("catch:" + ex);
        }
//     System.out.println("parseNode:"+aNodeValue);
        return aNodeValue;
    }

    private boolean getHaveNodeFlag()
    {
        return HaveNodeFlag;
    }

    private String parseNodeName(Node aNode, String aParsePath)
    {
        String aNodeName = "";
        try
        {
            aNodeName = XPathAPI.selectSingleNode(aNode, aParsePath).
                        getNodeName();
        }
        catch (Exception ex)
        {
        }
        return aNodeName;
    }

    /**
     * 得到日志显示结果
     * @return
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
        try
        {
            ParseXML tPGI = new ParseXML();
//      tPGI.parseVts();
            VData tVData = new VData();
            TransferData tTransferData = new TransferData();
            tTransferData.setNameAndValue("FileName", "lacom.xls");
            GlobalInput tG = new GlobalInput();
            tG.Operator = "001";
            tG.ManageCom = "86110000";
            tVData.add(tTransferData);
            tVData.add(tG);
            tPGI.submitData(tVData, "");
            tPGI.outputDocumentToFile("E:/temp/", "ProcedureXml");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Kevin 2003-06-21
     * 取得VTS文件中有数据的范围。取得最大行。
     * @param book
     */
    private int getMaxRow(BookModelImpl book) throws Exception
    {
        int nRow = 0;

        for (nRow = 0; nRow < book.getMaxRow(); nRow++)
        {
            String str = book.getText(nRow, 0);
            if (str == null || str.trim().equals(""))
            {
                break;
            }
        }

        return nRow;
    }

    /**
     * Kevin 2003-06-21
     * 取得VTS文件中有数据的范围。取得最大列。
     * @param book
     */
    private int getMaxCol(BookModelImpl book) throws Exception
    {
        int nCol = 0;

        for (nCol = 0; nCol < book.getMaxCol(); nCol++)
        {
            String str = book.getText(0, nCol);
            if (str == null || str.trim().equals(""))
            {
                break;
            }
        }

        return nCol - 1;
    }

    /**
     * Build a instance document object for function transfromNode()
     */
    private void bulidDocument()
    {
        DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
        dfactory.setNamespaceAware(true);

        try
        {
            m_doc = dfactory.newDocumentBuilder().newDocument();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * Detach node from original document, fast XPathAPI process.
     * @param node
     * @return
     * @throws Exception
     */
    private org.w3c.dom.Node transformNode(org.w3c.dom.Node node) throws
            Exception
    {
        Node nodeNew = m_doc.importNode(node, true);

        return nodeNew;
    }


    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "ParseGuideIn";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}