package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author
 * @version 1.0
 */
import java.sql.Connection;
import java.util.Arrays;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class BankChannelBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private GlobalInput mGlobalInput = new GlobalInput();
    private String Day = "";
    private String StartYearDay = "";
    private String mOperate = "";

    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    public BankChannelBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();

        if (cOperate.equals("Ch"))
        {
            if (!queryDataCh())
            {
                return false;
            }
        }
        if (cOperate.equals("Br"))
        {
            if (!queryDataBr())
            {
                return false;
            }
        }
        if (cOperate.equals("Ma"))
        {
            if (!queryDataMa())
            {
                return false;
            }
        }
        if (cOperate.equals("Bi"))
        {
            if (!queryDataBi())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        Day = (String) cInputData.get(0);
        mOperate = (String) cInputData.get(1);
        StartYearDay = Day.substring(0, 4) + "01-01";
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "PolYSOrderBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryDataCh()
    {
        //确定数组长度
        String asql = "select shortname,a.name,agentcom from ldcom b,lacom a where a.managecom=b.comcode and banktype='01' and actype='01'";
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(asql);
            int n = aSSRS.getMaxRow(); //最大行数
            String data[][] = new String[n + 1][6];
            String data1[][] = new String[n + 1][6]; //新顺序
            int sum = 0; //数组行数
            String strArr[] = null;
            ListTable tlistTable = new ListTable();
            tlistTable.setName("Channel");
            TextTag texttag = new TextTag(); //新建一个TextTag的实例
            texttag.add("Day", Day); //输入制表时间
            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                data[sum][1] = aSSRS.GetText(a, 1); //分公司名称
                data[sum][3] = aSSRS.GetText(a, 2); //渠道名称
                String bsql = "select sum(standprem)/10000,sum(transmoney)/10000 from lacommision where tmakedate<='" +
                              Day + "' and tmakedate>='" + StartYearDay +
                              "' and agentcom like '" + aSSRS.GetText(a, 3) +
                              "%'";
                SSRS bSSRS = new SSRS();
                bSSRS = aExeSQL.execSQL(bsql);
                for (int b = 1; b <= bSSRS.getMaxRow(); b++)
                {
                    data[sum][4] = bSSRS.GetText(b, 1); //标准保费
                    data[sum][5] = bSSRS.GetText(b, 2); //实收保费
                }
                String csql = "select a.name from laagent a,lacomtoagent b where a.agentcode=b.agentcode and agentcom='" +
                              aSSRS.GetText(a, 3) + "'";
                String name = aExeSQL.getOneValue(csql);
                if (name.equals("") || name == null)
                {
                    data[sum][2] = "";
                }
                else
                {
                    data[sum][2] = name;
                }
                sum++;
            }

            //END 组循环
            double compare[] = new double[sum];
            for (int q = 0; q < sum; q++)
            {
                if (!data[q][4].equals("") && data[q][4] != null)
                {
                    compare[q] = Double.parseDouble(data[q][4]);
                }
                else
                {
                    compare[q] = Double.parseDouble("0");
                }
            }
            //将标保排序,将序号插入第一列,然后排序
            Arrays.sort(compare);
            for (int s = 0; s < sum; s++)
            {
                int m = 0;
                for (int a = s; a < sum; a++)
                {
                    if (compare[s] == compare[a])
                    {
                        m++;
                    }
                }
                int b = 0;
                for (int r = 0; r < sum; r++)
                {
                    if (!String.valueOf(compare[s]).equals("") &&
                        String.valueOf(compare[s]) != null &&
                        !data[r][4].equals("") && data[r][4] != null)
                    {
                        int rr = 0;
                        if (Double.parseDouble(data[r][4]) == compare[s])
                        {
                            data[r][0] = String.valueOf(sum - s);
                            for (int z = 0; z <= 5; z++)
                            {
                                data1[sum - s - 1][z] = data[r][z];
                            }
                            b++;
                            if (b == m)
                            {
                                break;
                            }
                        }
                    }
                }
            }
//将数组中的值插入模板
            for (int t = 0; t < sum; t++)
            {
                strArr = new String[6];
                for (int v = 0; v <= 5; v++)
                {
                    strArr[v] = data1[t][v];
                }
                tlistTable.add(strArr);
            }
            strArr = new String[6];
            strArr[0] = "排名";
            strArr[1] = "分公司";
            strArr[2] = "渠道经理";
            strArr[3] = "渠道";
            strArr[4] = "标保";
            strArr[5] = "实收保费";
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("ChannelOrder.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankChannelBL";
            tError.functionName = "queryDatach";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    private boolean queryDataMa()
    {
        //确定数组长度
        String asql = "select a.name,b.shortname,c.agentcode,sum(transmoney)/10000,sum(standprem)/10000 from laagent a,ldcom b,lacommision c where a.branchtype='3' and a.managecom=b.comcode and a.agentcode=c.agentcode and tmakedate<='" +
                      Day + "' and tmakedate>='" + StartYearDay +
                      "' group by a.name,b.shortname,c.agentcode";
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(asql);
            int n = aSSRS.getMaxRow(); //最大行数
            String data[][] = new String[n + 1][6];
            String data1[][] = new String[n + 1][6]; //新顺序
            int sum = 0; //数组行数
            String strArr[] = null;
            ListTable tlistTable = new ListTable();
            tlistTable.setName("Manager");
            TextTag texttag = new TextTag(); //新建一个TextTag的实例
            texttag.add("Day", Day); //输入制表时间
            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                data[sum][1] = aSSRS.GetText(a, 1); //分公司名称
                data[sum][2] = aSSRS.GetText(a, 2); //分公司名称
                data[sum][5] = aSSRS.GetText(a, 4); //分公司名称
                data[sum][4] = aSSRS.GetText(a, 5); //标准保费
                //增加渠道名称
                String name = "select a.name from labranchgroup a,laagent b where a.agentgroup=b.branchcode and (a.state<>'1' or a.state is null) and b.agentcode='" +
                              aSSRS.GetText(a, 3) + "'";
                data[sum][3] = aExeSQL.getOneValue(name);
                sum++;
            }

            //END 组循环
            double compare[] = new double[sum];
            for (int q = 0; q < sum; q++)
            {
                if (!data[q][4].equals("") && data[q][4] != null)
                {
                    compare[q] = Double.parseDouble(data[q][4]);
                }
                else
                {
                    compare[q] = Double.parseDouble("0");
                }
            }
            //将标保排序,将序号插入第一列,然后排序
            Arrays.sort(compare);
            for (int s = 0; s < sum; s++)
            {
                int m = 0;
                for (int a = s; a < sum; a++)
                {
                    if (compare[s] == compare[a])
                    {
                        m++;
                    }
                }
                int b = 0;
                for (int r = 0; r < sum; r++)
                {
                    if (!String.valueOf(compare[s]).equals("") &&
                        String.valueOf(compare[s]) != null &&
                        !data[r][4].equals("") && data[r][4] != null)
                    {
                        int rr = 0;
                        if (Double.parseDouble(data[r][4]) == compare[s])
                        {
                            data[r][0] = String.valueOf(sum - s);
                            for (int z = 0; z <= 5; z++)
                            {
                                data1[sum - s - 1][z] = data[r][z];
                            }
                            b++;
                            if (b == m)
                            {
                                break;
                            }
                        }
                    }
                }
            }
//将数组中的值插入模板
            for (int t = 0; t < sum; t++)
            {
                strArr = new String[6];
                for (int v = 0; v <= 5; v++)
                {
                    strArr[v] = data1[t][v];
                }
                tlistTable.add(strArr);
            }
            strArr = new String[6];
            strArr[0] = "排名";
            strArr[1] = "分公司";
            strArr[2] = "渠道经理";
            strArr[3] = "渠道";
            strArr[4] = "标保";
            strArr[5] = "实收保费";
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("ManagerOrder.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
//   xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankChannelBL";
            tError.functionName = "queryDatama";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }


    private boolean queryDataBi()
    {
        //确定数组长度
        String asql = "select a.name,b.shortname,standprem/10000,transmoney/10000,signdate,d.riskname,e.name from ldcom b,lacommision c,lmrisk d,lacom e,laagent a where a.managecom=b.comcode and a.agentcode=c.agentcode and c.riskcode=d.riskcode and c.agentcom=e.agentcom and tmakedate<='" +
                      Day + "'  and tmakedate>='" + StartYearDay +
                      "' and a.branchtype='3' and transmoney>'50000'";
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(asql);
            int n = aSSRS.getMaxRow(); //最大行数
            String data[][] = new String[n + 1][8];
            String data1[][] = new String[n + 1][8]; //新顺序
            int sum = 0; //数组行数
            String strArr[] = null;
            ListTable tlistTable = new ListTable();
            tlistTable.setName("Big");
            TextTag texttag = new TextTag(); //新建一个TextTag的实例
            texttag.add("Day", Day); //输入制表时间
            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                data[sum][1] = aSSRS.GetText(a, 1); //分公司名称
                data[sum][2] = aSSRS.GetText(a, 2); //分公司名称
                data[sum][3] = aSSRS.GetText(a, 3); //分公司名称
                data[sum][4] = aSSRS.GetText(a, 4); //分公司名称
                data[sum][5] = aSSRS.GetText(a, 5); //分公司名称
                data[sum][6] = aSSRS.GetText(a, 6); //分公司名称
                data[sum][7] = aSSRS.GetText(a, 7); //渠道名称
                sum++;
            }

            //END 组循环
            double compare[] = new double[sum];
            for (int q = 0; q < sum; q++)
            {
                if (!data[q][3].equals("") && data[q][3] != null)
                {
                    compare[q] = Double.parseDouble(data[q][3]);
                }
                else
                {
                    compare[q] = Double.parseDouble("0");
                }
            }
            //将标保排序,将序号插入第一列,然后排序
            Arrays.sort(compare);
            for (int s = 0; s < sum; s++)
            {
                int m = 0;
                for (int a = s; a < sum; a++)
                {
                    if (compare[s] == compare[a])
                    {
                        m++;
                    }
                }
                int b = 0;
                for (int r = 0; r < sum; r++)
                {
                    if (!String.valueOf(compare[s]).equals("") &&
                        String.valueOf(compare[s]) != null &&
                        !data[r][3].equals("") && data[r][3] != null)
                    {
                        int rr = 0;
                        if (Double.parseDouble(data[r][3]) == compare[s])
                        {
                            data[r][0] = String.valueOf(sum - s);
                            for (int z = 0; z <= 7; z++)
                            {
                                data1[sum - s - 1][z] = data[r][z];
                            }
                            b++;
                            if (b == m)
                            {
                                break;
                            }
                        }
                    }
                }
            }
//将数组中的值插入模板
            for (int t = 0; t < sum; t++)
            {
                strArr = new String[8];
                for (int v = 0; v <= 7; v++)
                {
                    strArr[v] = data1[t][v];
                }
                tlistTable.add(strArr);
            }
            strArr = new String[8];
            strArr[0] = "排名";
            strArr[1] = "分公司";
            strArr[2] = "渠道经理";
            strArr[3] = "渠道";
            strArr[4] = "标保";
            strArr[5] = "实收保费";
            strArr[6] = "标保";
            strArr[7] = "实收保费";
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("BigMoney.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
//  xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankChannelBL";
            tError.functionName = "queryDatabi";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }


    private boolean queryDataBr()
    {
        //确定数组长度
        String asql = "select shortname,a.name,agentcom from lacom a,ldcom b where a.managecom=b.comcode and ACType<>'01' and actype<>'05'";
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(asql);
            int n = aSSRS.getMaxRow(); //最大行数
            String data[][] = new String[n + 1][6];
            String data1[][] = new String[n + 1][6]; //新顺序
            int sum = 0; //数组行数
            String strArr[] = null;
            ListTable tlistTable = new ListTable();
            tlistTable.setName("Channel");
            TextTag texttag = new TextTag(); //新建一个TextTag的实例
            texttag.add("Day", Day); //输入制表时间
            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                data[sum][1] = aSSRS.GetText(a, 1); //分公司名称
                data[sum][3] = aSSRS.GetText(a, 2); //渠道名称
                String bsql = "select sum(standprem)/10000,sum(transmoney)/10000 from lacommision where tmakedate<='" +
                              Day + "'  and tmakedate>='" + StartYearDay +
                              "' and agentcom ='" + aSSRS.GetText(a, 3) + "'";
                SSRS bSSRS = new SSRS();
                bSSRS = aExeSQL.execSQL(bsql);
                for (int b = 1; b <= bSSRS.getMaxRow(); b++)
                {
                    data[sum][4] = bSSRS.GetText(b, 1); //标准保费
                    data[sum][5] = bSSRS.GetText(b, 2); //实收保费
                }
                String csql = "select a.name from laagent a,lacomtoagent b where a.agentcode=b.agentcode and agentcom='" +
                              aSSRS.GetText(a, 3) + "'";
                String name = aExeSQL.getOneValue(csql);
                if (name.equals("") || name == null)
                {
                    data[sum][2] = "";
                }
                else
                {
                    data[sum][2] = name;
                }
                sum++;
            }

            //END 组循环
            double compare[] = new double[sum];
            for (int q = 0; q < sum; q++)
            {
                if (!data[q][4].equals("") && data[q][4] != null)
                {
                    compare[q] = Double.parseDouble(data[q][4]);
                }
                else
                {
                    compare[q] = Double.parseDouble("0");
                }
            }
            //将标保排序,将序号插入第一列,然后排序
            Arrays.sort(compare);
            for (int s = 0; s < sum; s++)
            {
                int m = 0;
                for (int a = s; a < sum; a++)
                {
                    if (compare[s] == compare[a])
                    {
                        m++;
                    }
                }
                int b = 0;
                for (int r = 0; r < sum; r++)
                {
                    if (!String.valueOf(compare[s]).equals("") &&
                        String.valueOf(compare[s]) != null &&
                        !data[r][4].equals("") && data[r][4] != null)
                    {
                        int rr = 0;
                        if (Double.parseDouble(data[r][4]) == compare[s])
                        {
                            data[r][0] = String.valueOf(sum - s);
                            for (int z = 0; z <= 5; z++)
                            {
                                data1[sum - s - 1][z] = data[r][z];
                            }
                            b++;
                            if (b == m)
                            {
                                break;
                            }
                        }
                    }
                }
            }
//将数组中的值插入模板
            for (int t = 0; t < sum; t++)
            {
                strArr = new String[6];
                for (int v = 0; v <= 5; v++)
                {
                    strArr[v] = data1[t][v];
                }
                tlistTable.add(strArr);
            }
            strArr = new String[6];
            strArr[0] = "排名";
            strArr[1] = "分公司";
            strArr[2] = "渠道经理";
            strArr[3] = "渠道";
            strArr[4] = "标保";
            strArr[5] = "实收保费";
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("BranchOrder.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
//   xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankChannelBL";
            tError.functionName = "queryDatabr";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }


    public static void main(String[] args)
    {
    }
}