package com.sinosoft.lis.agentprint;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;

import com.sinosoft.lis.db.LATreeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.lis.schema.LATreeSchema;
import com.sinosoft.lis.vschema.LATreeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class BankPremPayBL {

    public CErrors mErrors = new CErrors();

    private VData mInputData = new VData();

    private VData mResult = new VData();

    private String mOperate = "";

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();
    private String mBranchAttr = "";
    private String mIndexCalNo = "";
    private String mEndDate = "";
    private String mAgentState = "";
    private String mAgentGroup ="";
    private String mAgentCode ="";
    private String mAgentName ="";
    private String mManageCom = "";
    private String[] mDataList = null;
    private XmlExport mXmlExport = null;
    private SSRS mSSRS1 = new SSRS();    
    
    private SSRS m_SSRS1 = new SSRS();// add new 
    private SSRS m_SSRS2 = new SSRS();// add new 
    
    private SSRS R_SSRS = new SSRS();// add new 
    private String mOutXmlPath="";
    
    private ListTable mListTable = new ListTable();
    private PubFun mPubFun = new PubFun();
    private String mManageName = "";

    public BankPremPayBL() {
    }
    public static void main(String[] args)
    {

        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";
        BankPremPayBL tBankPremPayBL = new BankPremPayBL();
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return 	boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        mOperate = cOperate;
        mInputData = (VData) cInputData;
        if (mOperate.equals("")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!mOperate.equals("PRINT")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!this.getInputData(mInputData)) {
            return false;
        }

        if (!dealdate()) {
            return false;
        }

        if (!getPrintData()) {
            this.bulidError("getPrintData", "查询数据失败！");
            return false;
        }

        return true;
    }

    /**
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

        try
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);

        } catch (Exception ex) {
            this.mErrors.addOneError("");
            return false;
        }
        this.mManageCom = (String) mTransferData.getValueByName("tManageCom");
        this.mBranchAttr = (String) mTransferData.getValueByName("tBranchAttr");
        this.mAgentGroup = (String) mTransferData.getValueByName("tAgentGroup");
        this.mAgentCode = (String) mTransferData.getValueByName("tAgentCode");
        this.mIndexCalNo = (String) mTransferData.getValueByName("tIndexCalNo");
        this.mAgentName = (String) mTransferData.getValueByName("tAgentName");
        this.mOutXmlPath = (String) mTransferData.getValueByName("OutXmlPath");//得到路径
        System.out.println(mAgentName);
        return true;
    }
    /**
    * 业务处理方法
    * @return boolean
    */

   private boolean dealdate()
   {
      if (!getAgentNow())
      {
         return false;
      }

      return true;
   }
   /**
    * 查询数据
    * @return boolean
   */
   private boolean getAgentNow() {

       ExeSQL tExeSQL = new ExeSQL();
       String tSql =
                " select '"+this.mIndexCalNo+"',c.branchattr,c.name,getUniteCode(a.agentcode),b.name ,a.agentgrade "
               +" ,b.employdate "
               +" ,case when b.agentstate<='02' then '在职' when b.agentstate='03' then '离职登记' else '离职确认' end"
               +" ,b.outworkdate "
               +" from lawage  a,laagent b,labranchgroup  c  "
               +" where a.branchtype='3' and a.branchtype2='01' "
               +" and b.branchtype='3' and b.branchtype2='01' "
               +" and c.branchtype='3' and c.branchtype2='01' "
               +" and a.agentcode=b.agentcode and a.agentgroup=c.agentgroup "
               +" and  a.managecom like '" + mManageCom + "%' "
               +" and  a.indexcalno='"+mIndexCalNo+"'";

       if (mAgentCode != null && !mAgentCode.equals("")) {
           tSql += " and a.agentcode = '" + mAgentCode + "' ";
       }
       if (mAgentName != null && !mAgentName.equals("")) {
           tSql += " and b.name = '" + mAgentName+ "' ";
       }
       if (mBranchAttr != null && !mBranchAttr.equals("")) {
           tSql += " and c.branchattr like '%" + mBranchAttr + "%' ";
       }
       tSql += " order by a.managecom,c.branchattr,a.agentgrade,a.agentcode,b.name  with ur ";

       mSSRS1 = tExeSQL.execSQL(tSql);
       System.out.println(tSql);
       if (tExeSQL.mErrors.needDealError()) {
           CError tCError = new CError();
           tCError.moduleName = "MakeXMLBL";
           tCError.functionName = "creatFile";
           tCError.errorMessage = "查询XML数据出错！";

           this.mErrors.addOneError(tCError);

           return false;

       }
       if (mSSRS1.getMaxRow() <= 0) {
           CError tCError = new CError();
           tCError.moduleName = "MakeXMLBL";
           tCError.functionName = "creatFile";
           tCError.errorMessage = "没有符合条件的信息！";

           this.mErrors.addOneError(tCError);

           return false;
       }

       return true;
   }

    /**
     *
     * @return boolean
     */
    private boolean getPrintData() {
//        TextTag tTextTag = new TextTag();
//
//        mXmlExport = new XmlExport();
//        //设置模版名称
//        mXmlExport.createDocument("BankPremPay.vts", "printer");
//
//        String tMakeDate = "";
//        String tMakeTime = "";
//
//        tMakeDate = mPubFun.getCurrentDate();
//        tMakeTime = mPubFun.getCurrentTime();
//
//        if (!getManageName()) {
//            return false;
//        }
//        tTextTag.add("MakeDate", tMakeDate);
//        tTextTag.add("MakeTime", tMakeTime);
//        tTextTag.add("tName", mManageName);
//
//
//        System.out.println("12121212" + tMakeDate);
//        if (tTextTag.size() < 1) {
//            return false;
//        }
//
//        mXmlExport.addTextTag(tTextTag);
////需要修改
//        String[] title = {"","","","","","","","","","","","",   //12
//        		          "","","","","","","","","","","","",
//        		          "","","","","","","","","","","","",
//        		          "","","","","","","","","","","","",
//                          "","","","","","","","","","","","",
//                          "","","","","","","","","","","","",
//                          "","","","","","","","","","","","",
//                          "","","","","","","","","","","","",
//                          "","","","","","","","","","","","",
//                          "","","","","","","","",""}; // new

//        if (!getListTable()) {
//            return false;
//        }
//         this.mDataList = new String[80]; // new 
//        // 3、追加 合计 行
////        if(!setAddRow(mSSRS1.getMaxRow() ))
////        {
////           buildError("queryData", "进行合计计算时出错！");
////           return false;
////        }
//         mListTable.setName("Order");
//         mXmlExport.addListTable(mListTable, title);
//         mXmlExport.outputDocumentToFile("c:\\", "new1");
//         this.mResult.clear();
// 
//        mResult.addElement(mXmlExport);
    	

    	
    	//表头部分
    	// 前续表头
    	ExeSQL tExeSQL = new ExeSQL();
    	String SQL ="select '薪资年月','团队代码','团队名称','业务员代码','业务员姓名','职级名称','入司日期','在职状态','离职日期','季度业绩考核标准' from dual where 1=1";
     	m_SSRS1 = tExeSQL.execSQL(SQL);
   	    if(m_SSRS1.getMaxRow()==0)
        {
   		 buildError("queryData", "险种描述表查询出错！");
   		 return false;
          
        }
        //险种明细
        SQL = "select code,codename,comcode,othersign,codealias from LDCODE1 where CODETYPE ='SQBF'  order by int(code1) ";    	
    	m_SSRS2 = tExeSQL.execSQL(SQL);
    	System.out.println("列值为"+m_SSRS2.getMaxRow());
    	 if(m_SSRS2.getMaxRow()==0)
         {
    		 buildError("queryData", "险种描述表查询出错！");
    		 return false;
           
         }
       
        String[][] mToExcel = new String[mSSRS1.getMaxRow()+2][m_SSRS1.getMaxCol()+m_SSRS2.getMaxRow()*5+1];//这样定义一个二维数组
    	mToExcel[0][0]="银保销售序列月度业绩达成率查询表";
    	
        //第一轮赋值
    	for(int i =1;i<=m_SSRS1.getMaxRow();i++)
    	{
    	  for(int j = 1;j<=m_SSRS1.getMaxCol();j++)
    	  {   		  
    		  mToExcel[1][j-1]=m_SSRS1.GetText(i, j);  		  
    	  }    		
    	}
        //查询险种表描述
    	for(int r_i = 1;r_i<=m_SSRS2.getMaxRow();r_i++)
    	{   
    		 mToExcel[1][m_SSRS1.getMaxCol()-1+r_i]=m_SSRS2.GetText(r_i, 5)+"(个人银代业务)"; 
    		 mToExcel[1][m_SSRS1.getMaxCol()+m_SSRS2.getMaxRow()-1+r_i]=m_SSRS2.GetText(r_i, 5)+"(个人直销业务)";
    		 mToExcel[1][m_SSRS1.getMaxCol()+m_SSRS2.getMaxRow()*2-1+r_i]=m_SSRS2.GetText(r_i, 5)+"(团队银代业务*不含主管)";
    		 mToExcel[1][m_SSRS1.getMaxCol()+m_SSRS2.getMaxRow()*3-1+r_i]=m_SSRS2.GetText(r_i, 5)+"(团队直销业务*不含主管)";
    		 mToExcel[1][m_SSRS1.getMaxCol()+m_SSRS2.getMaxRow()*4-1+r_i]=m_SSRS2.GetText(r_i, 5)+"(折算保费：保费*缴费年期)";
    	}
    	mToExcel[1][m_SSRS1.getMaxCol()+m_SSRS2.getMaxRow()*5]="月度达成率合计(单位：%)";

    	

    	//开始填充数据
    	//先填充查询sql表数据

    	for(int data_i =1;data_i<=mSSRS1.getMaxRow();data_i++)
    	{
    	  for(int data_j= 1;data_j<=mSSRS1.getMaxCol();data_j++)
    	  {   		  

    		  mToExcel[data_i+1][data_j-1]=mSSRS1.GetText(data_i, data_j); // 
    		  
    			
    		    if(data_j==mSSRS1.getMaxCol())
    		    {  
    		    LATreeSchema tLATreeSchema = new LATreeSchema();
    		    //查询统一工号： 2014-11-30  解青青
				   ExeSQL tttExeSQL = new ExeSQL();
				  String tttAC = "select agentcode from laagent where groupagentcode='"+mSSRS1.GetText(data_i,4)+"'";
				  String tttAgentCode = tttExeSQL.getOneValue(tttAC);
            	tLATreeSchema=getInfo(tttAgentCode);
    		     //季度业绩考核标准 
        		  mToExcel[data_i+1][data_j]=getAssessStand(tLATreeSchema.getManageCom(),tLATreeSchema.getAgentGrade(),"RRR");//季度业绩考核标准
                 
        		  String zbTransmoney = "";
        		  for(int d_i =1;d_i<=m_SSRS2.getMaxRow();d_i++)
        		  {
        			  
                      //个人银代业务
        			  mToExcel[data_i+1][m_SSRS1.getMaxCol()-1+d_i]= getPrem(tLATreeSchema.getAgentCode(),tLATreeSchema.getAgentGrade(),tLATreeSchema.getAgentGroup(),mIndexCalNo,m_SSRS2.GetText(d_i, 1),m_SSRS2.GetText(d_i, 2),m_SSRS2.GetText(d_i, 3),m_SSRS2.GetText(d_i, 4),"1","Y");   
        			  //个人直销业务
        			  mToExcel[data_i+1][m_SSRS1.getMaxCol()+m_SSRS2.getMaxRow()-1+d_i]= getPrem(tLATreeSchema.getAgentCode(),tLATreeSchema.getAgentGrade(),tLATreeSchema.getAgentGroup(),mIndexCalNo,m_SSRS2.GetText(d_i, 1),m_SSRS2.GetText(d_i, 2),m_SSRS2.GetText(d_i, 3),m_SSRS2.GetText(d_i, 4),"0","Y");  
        			  //团队银代业务
        			  mToExcel[data_i+1][m_SSRS1.getMaxCol()+m_SSRS2.getMaxRow()*2-1+d_i]= getPrem(tLATreeSchema.getAgentCode(),tLATreeSchema.getAgentGrade(),tLATreeSchema.getAgentGroup(),mIndexCalNo,m_SSRS2.GetText(d_i, 1),m_SSRS2.GetText(d_i, 2),m_SSRS2.GetText(d_i, 3),m_SSRS2.GetText(d_i, 4),"1","Z");
        			  //团队直销业务
        			  mToExcel[data_i+1][m_SSRS1.getMaxCol()+m_SSRS2.getMaxRow()*3-1+d_i]= getPrem(tLATreeSchema.getAgentCode(),tLATreeSchema.getAgentGrade(),tLATreeSchema.getAgentGroup(),mIndexCalNo,m_SSRS2.GetText(d_i, 1),m_SSRS2.GetText(d_i, 2),m_SSRS2.GetText(d_i, 3),m_SSRS2.GetText(d_i, 4),"0","Z");
        			  // 算折标
        			  mToExcel[data_i+1][m_SSRS1.getMaxCol()+m_SSRS2.getMaxRow()*4-1+d_i] =getAddZB(mToExcel[data_i+1][m_SSRS1.getMaxCol()-1+d_i],mToExcel[data_i+1][m_SSRS1.getMaxCol()+m_SSRS2.getMaxRow()-1+d_i],mToExcel[data_i+1][m_SSRS1.getMaxCol()+m_SSRS2.getMaxRow()*2-1+d_i],mToExcel[data_i+1][m_SSRS1.getMaxCol()+m_SSRS2.getMaxRow()*3-1+d_i],m_SSRS2.GetText(d_i, 4),m_SSRS2.GetText(d_i, 1),m_SSRS2.GetText(d_i, 2));
        			  
        			  System.out.println("折标计算结果："+mToExcel[data_i+1][m_SSRS1.getMaxCol()+m_SSRS2.getMaxRow()*4-1+d_i]);
        			  zbTransmoney = getAdd1(mToExcel[data_i+1][m_SSRS1.getMaxCol()+m_SSRS2.getMaxRow()*4-1+d_i],zbTransmoney,"0","0");
        			  System.out.println("折算总保费zbTransmoney--------->>:"+zbTransmoney);
        			  if(d_i==m_SSRS2.getMaxRow())
        			  {
        				  // 算达成率
        				  mToExcel[data_i+1][m_SSRS1.getMaxCol()+m_SSRS2.getMaxRow()*4-1+d_i+1]=getRate(zbTransmoney,mToExcel[data_i+1][data_j]);  
        				  
        			  }
        			  
        		  }

    		    }
    		  
    	  }    		
    	}
    	 try
         {
             WriteToExcel t = new WriteToExcel("");
             t.createExcelFile();
             String[] sheetName ={PubFun.getCurrentDate()};
             t.addSheet(sheetName);//生成sheetname 类型是一维数组类型
             t.setData(0, mToExcel);//生成sheet数据 类型是二维数组类型
             t.write(mOutXmlPath);//获得文件读取路径
         }
         catch(Exception ex)
         {
             ex.toString();
             ex.printStackTrace();
         }
        return true;
    }

    /**
     * 查询列表显示数据
     * @return boolean
     */
    private boolean getListTable() {

//        if (mSSRS1.getMaxRow() > 0) {
//            for (int i = 1; i <= mSSRS1.getMaxRow(); i++) {
//                String  temp="";
//                String  temp1="";
//                String  temp2="";
//                String  temp3="";
//                String  temp4="";
//                String Info[] = new String[120];
//                Info[0] = Integer.toString(i);
//                Info[1] = mIndexCalNo;
//                Info[2] = mSSRS1.GetText(i, 1);
//                Info[3] = mSSRS1.GetText(i, 2);
//                Info[4] = mSSRS1.GetText(i, 3);//agentcode
//                Info[5] = mSSRS1.GetText(i, 4);
//                Info[6] = getGradeName(mSSRS1.GetText(i, 5));//agentgrade
//                Info[7] = mSSRS1.GetText(i, 6);
//                Info[8] = mSSRS1.GetText(i, 7);
//                Info[9] = mSSRS1.GetText(i, 8);
//                Info[10] = getAssessStand(mSSRS1.GetText(i, 10),mSSRS1.GetText(i, 5),"RRR");// 考核业绩标准
//                 
//                // 个人银代业务
//                Info[11] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331201","00","0","A","1","Y");
//                Info[12] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331301","00","12","A","1","Y");
//                Info[13] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331301","03","12","A","1","Y");
//                Info[14] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331601","00","0","A","1","Y");
//                Info[15] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"230701","00","12","A","1","Y");
//                Info[16] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331701","00","12","A","1","Y");
//                Info[17] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"240501","00","12","3","1","Y");
//                Info[18] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"240501","00","12","5","1","Y");
//                Info[19] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331901","00","0","A","1","Y");
//                Info[20] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730101","00","0","B","1","Y");
//                Info[21] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730101","00","12","3","1","Y");
//                Info[22] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730101","00","12","5","1","Y");
//                Info[23] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"332501","00","0","A","1","Y");
//                Info[24] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"332601","00","0","A","1","Y");
//                Info[25] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"332901","00","0","A","1","Y");
//                Info[26] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"333001","00","0","A","1","Y");
//                Info[27] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730201","00","0","B","1","Y"); 
//                Info[28] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730201","00","12","3","1","Y");
//                Info[29] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730201","00","12","5","1","Y");
//                Info[30] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730201","00","12","10","1","Y");
//                Info[31] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730201","00","12","20","1","Y");
//                Info[32] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"333501","00","12","5","1","Y");
//                Info[33] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"333501","00","12","10","1","Y");
//                Info[34] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"333201","00","0","A","1","Y");// add new
//                
//                // 个人直销业务
//                Info[35] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331201","00","0","A","0","Y");
//                Info[36] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331301","00","12","A","0","Y");
//                Info[37] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331301","03","12","A","0","Y");
//                Info[38] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331601","00","0","A","0","Y");
//                Info[39] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"230701","00","12","A","0","Y");
//                Info[40] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331701","00","12","A","0","Y");
//                Info[41] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"240501","00","12","3","0","Y");
//                Info[42] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"240501","00","12","5","0","Y");
//                Info[43] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331901","00","0","A","0","Y");
//                Info[44] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730101","00","0","B","0","Y");
//                Info[45] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730101","00","12","3","0","Y");
//                Info[46] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730101","00","12","5","0","Y");
//                Info[47] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"332501","00","0","A","0","Y");
//                Info[48] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"332601","00","0","A","0","Y");
//                Info[49] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"332901","00","0","A","0","Y");
//                Info[50] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"333001","00","0","A","0","Y");
//                Info[51] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730201","00","0","B","0","Y");
//                Info[52] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730201","00","12","3","0","Y");
//                Info[53] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730201","00","12","5","0","Y");
//                Info[54] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730201","00","12","10","0","Y");
//                Info[55] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730201","00","12","20","0","Y");
//                Info[56] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"333501","00","12","5","0","Y");
//                Info[57] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"333501","00","12","10","0","Y");
//                Info[58] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"333201","00","0","A","0","Y");// add new
//                
//                
//                // 主管 银代业务
//                Info[59] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331201","00","0","A","1","Z");
//                Info[60] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331301","00","12","A","1","Z");
//                Info[61] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331301","03","12","A","1","Z");
//                Info[62] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331601","00","0","A","1","Z");
//                Info[63] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"230701","00","12","A","1","Z");
//                Info[64] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331701","00","12","A","1","Z");
//                Info[65] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"240501","00","12","3","1","Z");
//                Info[66] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"240501","00","12","5","1","Z");
//                Info[67] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331901","00","0","A","1","Z");
//                Info[68] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730101","00","0","B","1","Z");
//                Info[69] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730101","00","12","3","1","Z");
//                Info[70] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730101","00","12","5","1","Z");
//                Info[71] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"332501","00","0","A","1","Z");
//                Info[72] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"332601","00","0","A","1","Z");
//                Info[73] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"332901","00","0","A","1","Z");
//                Info[74] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"333001","00","0","A","1","Z");
//                Info[75] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730201","00","0","B","1","Z");
//                Info[76] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730201","00","12","3","1","Z");
//                Info[77] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730201","00","12","5","1","Z");
//                Info[78] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730201","00","12","10","1","Z");
//                Info[79] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730201","00","12","20","1","Z");
//                Info[80] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"333501","00","12","5","1","Z");
//                Info[81] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"333201","00","12","10","1","Z");// add new
//                
//                // 主管 直销业务
//                Info[82] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331201","00","0","A","0","Z");
//                Info[83] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331301","00","12","A","0","Z");
//                Info[84] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331301","03","12","A","0","Z");
//                Info[85] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331601","00","0","A","0","Z");
//                Info[86] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"230701","00","12","A","0","Z");
//                Info[87] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331701","00","12","A","0","Z");
//                Info[88] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"240501","00","12","3","0","Z");
//                Info[89] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"240501","00","12","5","0","Z");
//                Info[90] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"331901","00","0","A","0","Z");
//                Info[91] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730101","00","0","B","0","Z");
//                Info[92] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730101","00","12","3","0","Z");
//                Info[93] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730101","00","12","5","0","Z");
//                Info[94] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"332501","00","0","A","0","Z");
//                Info[95] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"332601","00","0","A","0","Z");
//                Info[96] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"332901","00","0","A","0","Z");
//                Info[97] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"333001","00","0","A","0","Z");
//                Info[98] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730201","00","0","B","0","Z");
//                Info[99] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730201","00","12","3","0","Z");
//                Info[100] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730201","00","12","5","0","Z");               
//                Info[101] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730201","00","12","10","0","Z");
//                Info[102] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"730201","00","12","20","0","Z");
//                Info[103] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"333501","00","12","5","0","Z");
//                Info[104] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"333501","00","12","10","0","Z");
//                Info[105] = getPrem(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 5),mSSRS1.GetText(i, 9),mIndexCalNo,"333201","00","0","A","0","Z");
//                
//                Info[106] = getAdd4(Info[11],Info[13],Info[35],Info[37],Info[57],Info[59],Info[80],Info[82]);//月度万能B折及万能C追加折算保费（=万能B+万能C追加）
//                Info[107] =getAdd3( Info[12],Info[36],Info[58],Info[81],5);//月度万能C折算保费（=万能C*5）
//                Info[108] =getAdd3( Info[14],Info[38],Info[60],Info[83],1);//月度万能D折算保费（=万能D）
//                Info[109] =getAdd3( Info[15],Info[39],Info[61],Info[84],5);//月度健康专家折算保费（=健专*5）
//                Info[110] =getAdd3( Info[16],Info[40],Info[62],Info[85],5);//金利宝折算保费（=金利宝*5）
//                //===
//                temp = getAdd3( Info[17],Info[41],Info[63],Info[86],3);//安心保3年*3
//                temp1 = getAdd3( Info[18],Info[42],Info[64],Info[87],5);//安心保5年*5
//                temp2="0";
//                temp3="0";
//                Info[108]=getAdd1(temp,temp1,temp2,temp3);//安心宝折算保费（=安心保3年*3+安心保5年*5）
//     
//                Info[109]=getAdd1(Info[19],Info[43],Info[65],Info[88]);//康利相伴折算保费（=康利相伴*1）
//                
//                temp=getAdd1(Info[20],Info[44],Info[66],Info[89]); // 康利趸*1
//                temp1 =getAdd3( Info[21],Info[45],Info[67],Info[90],3); // 康利3年*3
//                temp2 =getAdd3( Info[22],Info[46],Info[68],Info[91],5); //康利5年*5
//                temp3="0";
//                Info[110]=getAdd1(temp,temp1,temp2,temp3);//康利人生折算保费（=康利趸*1+康利3年*3+康利5年*5）
//                //===新增加险种
//                Info[111] =getAdd3( Info[23],Info[47],Info[69],Info[92],1);// 月度万能E折算保费（=万能E）
//                Info[112] =getAdd3( Info[24],Info[48],Info[70],Info[93],1); // 月度万能F折算保费（=万能F）
//                Info[113] =getAdd3( Info[25],Info[49],Info[71],Info[94],1); // 月度万能G折算保费（=万能G）
//                Info[114] =getAdd3( Info[26],Info[50],Info[72],Info[95],1); // 月度万能G折算保费（=万能H）
//                
////                // add new 
//                temp=getAdd1(Info[27],Info[51],Info[73],Info[96]); // 福满趸*1
//                temp1 =getAdd3( Info[28],Info[52],Info[74],Info[97],3); // 福满3年*3
//                temp2 =getAdd3( Info[29],Info[53],Info[75],Info[98],5); //福满5年*5
//                temp3=getAdd3( Info[30],Info[54],Info[76],Info[99],10); //福满10年*10;
//                temp4=getAdd3( Info[31],Info[55],Info[77],Info[100],20); //福满20年*20;
//                Info[115] =getAdd2(temp,temp1,temp2,temp3,temp4,"0","0");
//                
//                
//                temp = getAdd3( Info[32],Info[56],Info[78],Info[101],5);//百万安行5年*5
//                temp1 = getAdd3( Info[33],Info[57],Info[79],Info[102],10);//百万安行10年*10
//                temp2="0";
//                temp3="0";
//                Info[116]=getAdd1(temp,temp1,temp2,temp3);//百万安行折算保费（=百万安行5年*5+百万安行10年*10）
//                //
//                
//                temp =getAdd( Info[103],Info[104],Info[105],Info[106],Info[107],Info[108],Info[109],Info[110],Info[111],Info[112],Info[113],Info[114],Info[115],Info[116]);
//                Info[117]=getRate(temp,Info[10]);
//                mListTable.add(Info);
//            }
//
//        } else {
//            CError tCError = new CError();
//            tCError.moduleName = "CreateXml";
//            tCError.functionName = "creatFile";
//            tCError.errorMessage = "没有符合条件的信息！";
//            this.mErrors.addOneError(tCError);
//            return false;
//        }
        return true;
    }


    /**
   * 追加一条合计行
   * @return boolean
   */
  private boolean setAddRow(int pmRow)
  {
      System.out.println("合计行的行数是："+pmRow);

      mDataList[0] = "合  计";
      mDataList[1] = mIndexCalNo;
      mDataList[2] = "";
      mDataList[3] = "";
      mDataList[4] = "";
      mDataList[5] = "";
      mDataList[6] = "";
      mDataList[7] = "";
      mDataList[8] = "";
      mDataList[9] = "";
      mDataList[10] = "";
      mDataList[11] = "";
      mDataList[12] = "";
      mDataList[13] = "";
      mDataList[14] = "";
      mDataList[15] = "";
      mDataList[16] = "";
      mDataList[17] = "";

      mListTable.add(mDataList);



      return true;
  }

  private String dealSum(int pmArrNum)
{
   String tReturnValue = "";
   DecimalFormat tDF = new DecimalFormat("0.##");
   String tSQL = "select 0";

   for(int i=1;i<=mSSRS1.getMaxRow();i++)
   {
       tSQL += " + " + mSSRS1.GetText(i,pmArrNum);
   }

   tSQL += " + 0 from dual";

   tReturnValue = "" + tDF.format(execQuery(tSQL));

   return tReturnValue;
}


    private String getAssessStand(String pmManageCom,String pmAgentGrade,String pmRiskCode)
  {
      String tSQL = "";
     String tRtValue="";
     tSQL = " select decimal(standprem,12,2) from LADiscount "
           +" where discounttype='05' and managecom='"+pmManageCom+"' and agentgrade='"+pmAgentGrade+"'"
           +" and riskcode='"+pmRiskCode+"'";
     SSRS tSSRS = new SSRS();
     ExeSQL tExeSQL = new ExeSQL();
     tSSRS = tExeSQL.execSQL(tSQL);
     if(tSSRS.getMaxRow()==0)
      {
        tRtValue="0";
      }
      else
     tRtValue=tSSRS.GetText(1, 1);
     return tRtValue;
}

 /*
*pmType 代表 1-正常银代业务（agentcom不为空） 0-银代直销（agentcom为空）
*pmLevel 代表 Y  - 业务职级   Z-主管职级
  *只计算首年保费
  */
 private String getPrem(String pmAgentCode,String pmAgentGrade,String pmAgentGroup,String pmIndexCalNo,String pmRiskCode,String pmTransState,String pmType,String pmLevel)
{
  String tSQL = "";
   String tRtValue="";
   tSQL = " select value(sum(transmoney),0) from lacommision "
         +" where branchtype='3' and branchtype2='01' "
         +" and payyear=0 and renewcount=0 "
         +" and wageno='"+pmIndexCalNo+"'"
         +" and transstate='"+pmTransState+"'";

  if(pmRiskCode.equals("230701")){
      tSQL =tSQL +" and riskcode in ('230701','331401')";
  }else if(pmRiskCode.equals("320106")){
	  tSQL =tSQL +" and riskcode in ('320106','120706')";
  }else if(pmRiskCode.equals("240501")){
              tSQL =tSQL +" and riskcode in ('340201','240501')";
  }else
  {
      tSQL =tSQL  +" and riskcode='"+pmRiskCode+"'";
  }

  if(pmLevel.equals("Y")){

       tSQL =tSQL +" and agentcode='"+pmAgentCode+"' ";

  }
  else if(pmLevel.equals("Z")){
  if(pmAgentGrade.compareTo("F33")<=0 )
       {
        tSQL =tSQL +" and 1=2 ";
       }
    else if(pmAgentGrade.compareTo("G53")<=0){
      tSQL =tSQL +" and agentgroup='"+pmAgentGroup+"' and agentcode<>'"+pmAgentCode+"' ";
    }
    else if(pmAgentGrade.compareTo("G73")<=0){
      tSQL =tSQL +" and substr(branchseries,1,12)='"+pmAgentGroup+"'  and agentcode<>'"+pmAgentCode+"' ";
    }

  }

  if(pmType.equals("1")){

    tSQL= tSQL+" and (agentcom is not null and agentcom <> '')  with ur";
  }
  else if(pmType.equals("0")){
     tSQL= tSQL+" and (agentcom is  null or agentcom = '') with ur";
  }
  System.out.println(tSQL);
   SSRS tSSRS = new SSRS();
   ExeSQL tExeSQL = new ExeSQL();
   tSSRS = tExeSQL.execSQL(tSQL);
   if(tSSRS.getMaxRow()==0)
    {
      tRtValue="0";
    }
    else
   tRtValue=tSSRS.GetText(1, 1);
   return tRtValue;
}

 
 /*
 *pmType 代表 1-正常银代业务（agentcom不为空） 0-银代直销（agentcom为空）
 *pmLevel 代表 Y  - 业务职级   Z-主管职级
   *只计算首年保费
   */
  private String getPrem(String pmAgentCode,String pmAgentGrade,String pmAgentGroup,
		  String pmIndexCalNo,String pmRiskCode,String pmTransState,String pmPayIntv,
		  String pmPayYears,String pmType,String pmLevel)
 {
   String tSQL = "";
   String tRtValue="";
    tSQL = " select value(sum(transmoney),0) from lacommision "
          +" where branchtype='3' and branchtype2='01' "
          +" and payyear=0 and renewcount=0 "
          +" and wageno='"+pmIndexCalNo+"'"
          +" and transstate='"+pmTransState+"'";

   if(pmRiskCode.equals("230701")){
       tSQL =tSQL +" and riskcode in ('230701','331401')";
   }else if(pmRiskCode.equals("240501")){
               tSQL =tSQL +" and riskcode in ('340201','240501')";
   }else if(pmRiskCode.equals("730101")){
               tSQL =tSQL +" and riskcode in ('730101','332401')";
   }else if(pmRiskCode.equals("730201")){
	   // add new 
       tSQL =tSQL +" and riskcode in ('730201','332801')";
   }else if(pmRiskCode.equals("333501")){
	   // add new 
       tSQL =tSQL +" and riskcode in ('333501','532401','532501')";
   }else if(pmRiskCode.equals("333201")){
	   // add new 
       tSQL =tSQL +" and riskcode in ('333201','532301')";
   }else if(pmRiskCode.equals("333701")){
	   // add new 
       tSQL =tSQL +" and riskcode in ('333701','532601')";
   }else if(pmRiskCode.equals("231701")){
	   // add new 
       tSQL =tSQL +" and riskcode in ('231701','333801')";
   }
   else if(pmRiskCode.equals("232101")){
	   // add new 
       tSQL =tSQL +" and riskcode in ('232101','334301')";
   }
   else if(pmRiskCode.equals("231601")){
	   // add new 
       tSQL =tSQL +" and riskcode in ('231601','333601','231401')";
   }
   else {
       tSQL =tSQL  +" and riskcode='"+pmRiskCode+"'";
   }
   
   if(pmPayIntv.equals("12")&&pmPayYears.equals("3")){
	   tSQL =tSQL  +" and payyears=3  and payintv=12 "; 
   }
   if(pmPayIntv.equals("12")&&pmPayYears.equals("5")){
	   tSQL =tSQL  +" and payyears=5  and payintv=12 "; 
   }
   if(pmPayIntv.equals("0")&&pmPayYears.equals("B")){
	   tSQL =tSQL  +" and payintv=0 "; 
   }
   if(pmPayIntv.equals("12")&&pmPayYears.equals("10")){
	   tSQL =tSQL  +" and payyears=10  and payintv=12 "; 
   }
   if(pmPayIntv.equals("12")&&pmPayYears.equals("20")){
	   tSQL =tSQL  +" and payyears=20  and payintv=12 "; 
   }
   if(pmLevel.equals("Y")){

        tSQL =tSQL +" and agentcode='"+pmAgentCode+"' ";

   }
   else if(pmLevel.equals("Z")){
   if(pmAgentGrade.compareTo("F33")<=0 )
        {
         tSQL =tSQL +" and 1=2 ";
        }
     else if(pmAgentGrade.compareTo("G53")<=0){
       tSQL =tSQL +" and agentgroup='"+pmAgentGroup+"' and agentcode<>'"+pmAgentCode+"' ";
     }
     else if(pmAgentGrade.compareTo("G73")<=0){
       tSQL =tSQL +" and substr(branchseries,1,12)='"+pmAgentGroup+"'  and agentcode<>'"+pmAgentCode+"' ";
     }
   }

   if(pmType.equals("1")){

     tSQL= tSQL+" and (agentcom is not null and agentcom <> '') with ur";
   }
   else if(pmType.equals("0")){
      tSQL= tSQL+" and (agentcom is  null or agentcom = '') with ur";
   }
   System.out.println(tSQL);
    SSRS tSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    tSSRS = tExeSQL.execSQL(tSQL);
    if(tSSRS.getMaxRow()==0)
     {
       tRtValue="0";
     }
     else
    tRtValue=tSSRS.GetText(1, 1);
    return tRtValue;
 }
 
 

    private String getRate(String pmPrem,String pmStand)
     {
         String tSQL = "";
         String tRtValue = "";
         double tDbValue = 0.00;

         if(0 == Integer.parseInt(pmStand))
         {
             return "0.0";
         }

         tSQL = "select DECIMAL(DECIMAL(" + pmPrem + ",12,2) / DECIMAL(" +
                pmStand + ",12,2)*100*3,12,2) from dual";

         try{
             tDbValue = execQuery(tSQL);
         }catch(Exception ex)
         {
             System.out.println("getRate 出错！");
         }
         tRtValue = String.valueOf(tDbValue);
         return tRtValue;
     }

     private String getAdd(String pmValue1,String pmValue2,String pmValue3,String pmValue4,String pmValue5,String pmValue6,String pmValue7,String pmValue8,String pmValue9,String pmValue10,String pmValue11,String pmValue12,String pmValue13,String pmValue14  )
       {
           String tSQL = "";
           String tRtValue = "";

           tSQL = "select DECIMAL(" + pmValue1 + "+" + pmValue2 +"+"+ pmValue3 +"+" + pmValue4+ "+" + pmValue5
                  + "+" + pmValue6
                  + "+" + pmValue7 + "+" + pmValue8 + "+"+ pmValue9 + "+"+ pmValue10 +"+"+pmValue11 +"+" + pmValue12 + "+"+pmValue13+"+"+pmValue14+",12,2) from dual";

           try{
               tRtValue = String.valueOf(execQuery(tSQL));
           }catch(Exception ex)
           {
               System.out.println("getAverageAgentCount 出错！");
           }

           return tRtValue;
  }
     private String getAdd4(String pmValue1,String pmValue2,String pmValue3,String pmValue4,String pmValue5,String pmValue6,String pmValue7,String pmValue8)
     {
         String tSQL = "";
         String tRtValue = "";

         tSQL = "select DECIMAL(" + pmValue1 + "+" + pmValue2 +"+"+ pmValue3 +"+" + pmValue4+ "+" + pmValue5
                + "+" + pmValue6
                + "+" + pmValue7 + "+" + pmValue8 + ",12,2) from dual";

         try{
             tRtValue = String.valueOf(execQuery(tSQL));
         }catch(Exception ex)
         {
             System.out.println("getAverageAgentCount 出错！");
         }

         return tRtValue;
}
  private String getAdd1(String pmValue1,String pmValue2,String pmValue3,String pmValue4)
        {
            String tSQL = "";
            String tRtValue = "";

            tSQL = "select DECIMAL(" + pmValue1 + "+" + pmValue2 +"+"+ pmValue3 +"+" + pmValue4+ ",12,2) from dual";

            try{
                tRtValue = String.valueOf(execQuery(tSQL));
            }catch(Exception ex)
            {
                System.out.println("getAdd1 出错！");
            }

            return tRtValue;
  }


  private String getAdd2(String pmValue1,String pmValue2,String pmValue3,String pmValue4,String pmValue5,String pmValue6,String pmValue7)
        {
            String tSQL = "";
            String tRtValue = "";

            tSQL = "select value(DECIMAL(" + pmValue1 + "+" + pmValue2+"+"+ pmValue3 +"+"+ pmValue4 +"+"+ pmValue5+"+"+ pmValue6 +"+" + pmValue7 + ",12,2),0) from dual";

            try{
                tRtValue = String.valueOf(execQuery(tSQL));
            }catch(Exception ex)
            {
                System.out.println("getAdd1 出错！");
            }

            return tRtValue;
  }

  private String getAdd3(String pmValue1,String pmValue2,String pmValue3,String pmValue4,int pmValue5)
  {
      String tSQL = "";
      String tRtValue = "";

      tSQL = "select value(DECIMAL((" + pmValue1 + "+" + pmValue2 +"+"+ pmValue3 +"+" + pmValue4+ ")*"+pmValue5+",12,2),0) from dual";

      try{
          tRtValue = String.valueOf(execQuery(tSQL));
      }catch(Exception ex)
      {
          System.out.println("getAdd1 出错！");
      }

      return tRtValue;
}
  

  
    private String getGradeName(String pmAgentGrade)
    {
      String tSQL = "";
      String tRtValue="";
      tSQL = "select  gradename from laagentgrade where gradecode='" + pmAgentGrade +"'";
      SSRS tSSRS = new SSRS();
      ExeSQL tExeSQL = new ExeSQL();
      tSSRS = tExeSQL.execSQL(tSQL);
      if(tSSRS.getMaxRow()==0)
      {
      tRtValue="";
      }
      else
      tRtValue=tSSRS.GetText(1, 1);
      return tRtValue;
    }

    /**
      * 追加错误信息
      * @param szFunc String
      * @param szErrMsg String
      */
     private void buildError(String szFunc, String szErrMsg)
     {
         CError cError = new CError();
         cError.moduleName = "LAStatisticReportBL";
         cError.functionName = szFunc;
         cError.errorMessage = szErrMsg;
         System.out.println(szFunc + "--" + szErrMsg);
         this.mErrors.addOneError(cError);
     }

    private boolean getManageName() {

        String sql = "select name from ldcom where comcode='" + mManageCom +
                     "'";

        SSRS tSSRS = new SSRS();

        ExeSQL tExeSQL = new ExeSQL();

        tSSRS = tExeSQL.execSQL(sql);

        if (tExeSQL.mErrors.needDealError()) {

            this.mErrors.addOneError("销售单位不存在！");

            return false;

        }

        if (mManageCom.equals("86")) {
            this.mManageName = "";
        } else {
            this.mManageName = tSSRS.GetText(1, 1) + "分公司";
        }

        return true;
    }

    /**
     * 获取打印所需要的数据
     * @param cFunction String
     * @param cErrorMsg String
     */
    private void bulidError(String cFunction, String cErrorMsg) {

        CError tCError = new CError();

        tCError.moduleName = "BankPremPayBL";
        tCError.functionName = cFunction;
        tCError.errorMessage = cErrorMsg;

        this.mErrors.addOneError(tCError);

    }
    /**
      * 执行SQL文查询结果
      * @param sql String
      * @return double
      */
     private double execQuery(String sql)
     {
         Connection conn;
         conn = null;
         conn = DBConnPool.getConnection();

         System.out.println(sql);

         PreparedStatement st = null;
         ResultSet rs = null;
         try {
             if (conn == null)return 0.00;
             st = conn.prepareStatement(sql);
             if (st == null)return 0.00;
             rs = st.executeQuery();
             if (rs.next()) {
                 return rs.getDouble(1);
             }
             return 0.00;
         } catch (Exception ex) {
             ex.printStackTrace();
             return -1;
         } finally {
             try {
               if (!conn.isClosed()) {
                   conn.close();
               }
               try {
                   st.close();
                   rs.close();
               } catch (Exception ex2) {
                   ex2.printStackTrace();
               }
               st = null;
               rs = null;
               conn = null;
             } catch (Exception e) {}

         }
    }
    /**
     *
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }
    
    private LATreeSchema getInfo(String AgentCode)
    {
    	LATreeSchema tLATreeSchema = new LATreeSchema();
    	LATreeSet tLATreeSet = new LATreeSet();
    	LATreeDB tLATreeDB = new LATreeDB();
    	tLATreeDB.setAgentCode(AgentCode);
    	
    	tLATreeSet=tLATreeDB.query();
    	tLATreeSchema =tLATreeSet.get(1);
    	return tLATreeSchema;
    }
    
    private String getAddZB(String pmValue1,String pmValue2,String pmValue3,String pmValue4,String pmValue5,String RiskCode,String TransState)
    {
    	String tSQL = "";
        String tRtValue = "";
        if("A".equals(pmValue5)||"B".equals(pmValue5))
        {
        	pmValue5 ="1";
        }
        if("331301".equals(RiskCode)&&"00".equals(TransState)&&"A".equals(pmValue5))
        {
        	pmValue5 ="5";
        }
        if("230701".equals(RiskCode)&&"00".equals(TransState)&&"A".equals(pmValue5))
        {
        	pmValue5 ="5";
        }
        if("331701".equals(RiskCode)&&"00".equals(TransState)&&"A".equals(pmValue5))
        {
        	pmValue5 ="5";
        }
        //银保渠道福惠双全护理险（334001、532801）  折标比例0.01
        if("334001".equals(RiskCode))
        {
        	pmValue5 ="0.01";
        }
        if("532801".equals(RiskCode))
        {
        	pmValue5 ="0.01";
        }
        tSQL = "select value(DECIMAL((" + pmValue1 + "+" + pmValue2 +"+"+ pmValue3 +"+" + pmValue4+ ")*"+pmValue5+",12,2),0) from dual";

        try{
            //tRtValue =  String.valueOf(execQuery(tSQL));
        	DecimalFormat df = new DecimalFormat("###0.00#");
        	tRtValue =  df.format(execQuery(tSQL));
        	System.out.println(df.format(execQuery(tSQL)));
        }catch(Exception ex)
        {
            System.out.println("getAdd1 出错！");
        }

        return tRtValue;
    }
}
