package com.sinosoft.lis.agentprint;

import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class HeadPolCounterF
{
    public GlobalInput mGI = new GlobalInput();
    private String mDay = "";
    private String mStaticOrg = "";
    public VData mResult = new VData();
    public CErrors mErrors = new CErrors();

    public HeadPolCounterF()
    {
    }

    public static void main(String[] args)
    {
        HeadPolCounterF headPolCounterF1 = new HeadPolCounterF();

        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86110000";
        tGI.ManageCom = "86";
        tGI.Operator = "aa";
        VData tInputData = new VData();
        tInputData.clear();
        tInputData.addElement(tGI);
        tInputData.add(0, "2003-12-1");
        tInputData.add(2, "8611");

        if (!headPolCounterF1.getInputData(tInputData))
        {
            System.out.println("-----getinputdata failed!------");
        }
        if (!headPolCounterF1.prepareData())
        {
            System.out.println("-------prepareData failed!------");
        }

    }

    public boolean getInputData(VData tInputData)
    {
        try
        {
            this.mDay = (String) tInputData.getObjectByObjectName("String", 0);
            this.mStaticOrg = (String) tInputData.getObjectByObjectName(
                    "String", 1);
            this.mGI = (GlobalInput) tInputData.getObjectByObjectName(
                    "GlobalInput",
                    0);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("getInputData", ex.toString());
            return false;
        }
        System.out.println("------------getInputData------------");
        System.out.println("统计时间：" + this.mDay);
        System.out.println("统计机构：" + this.mStaticOrg);
        System.out.println("------------------------------------");
        return true;
    }

    public boolean prepareData()
    {

        double data[] = null;
        data = new double[6];

        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);

            String Sql = "";
            SSRS ssrs = new SSRS();
            Sql = "select trim(shortName) from ldcom where trim(comcode) = '"
                  + this.mStaticOrg.trim() + "'";
            String tName = aExeSQL.getOneValue(Sql).trim();

            ListTable tListTab = new ListTable();
            tListTab.setName("Counter");

            TextTag texttag = new TextTag(); //新建一个TextTag的实例
            String tStaticDate = "";
            tStaticDate = this.mDay.trim();
            texttag.add("Day", tStaticDate); //输入制表时间
            texttag.add("Name", "制表:" + tName.trim());
            int tLength = 0;
            if (this.mStaticOrg.trim().length() != 8)
            {
                tLength = this.mStaticOrg.trim().length() + 2;
            }
            else
            {
                tLength = this.mStaticOrg.trim().length();
            }
            Sql = "select trim(comcode),trim(shortname) from ldcom where "
                  + " length(trim(comcode)) = " + tLength
                  + " and trim(comcode) like '" + this.mStaticOrg.trim() +
                  "%' ";
            ssrs = aExeSQL.execSQL(Sql);
            int tCount = 0;
            tCount = ssrs.getMaxRow();
            System.out.println("-----tCount = " + tCount);
            String[] strArr = null;
            String tSql = "";
            PubFun1 tPF = new PubFun1();
            String tDay[] = tPF.calFLDate(this.mDay.trim());
            for (int i = 1; i <= tCount; i++)
            {
                strArr = new String[6];
                strArr[0] = ssrs.GetText(i, 2).trim();
                String tComCode = ssrs.GetText(i, 1).trim();
                System.out.println("----tComCode = " + tComCode);
                //月初人力
                tSql = "select nvl(count(*),0) from LAAgent where"
                       + " EmployDate < '" + tDay[0].trim() +
                       "' and agentstate in ('01','02')"
                       +
                       " and branchcode in (select agentgroup from labranchgroup where"
                       +
                       " branchtype = '1' and (state<>'1' or state is null) and branchattr like '" +
                       tComCode.trim() + "%')";
                strArr[1] = aExeSQL.getOneValue(tSql);
                data[1] += Double.parseDouble(strArr[1]);

                //日交单规模保费
                tSql =
                        "select NVL(sum(paymoney),0) from ljtempfee_lmriskapp3 where MakeDate ='" +
                        mDay + "' and agentgroup in (select agentgroup from labranchgroup where branchtype = '1' and (state<>'1' or state is null) and branchattr like '" +
                        tComCode.trim() + "%')";

                System.out.println("----日交单规模保费tSql = " + tSql.trim());
                strArr[2] = aExeSQL.getOneValue(tSql);
                data[2] += Double.parseDouble(strArr[2]);

                //日件数
                tSql = "select CALPIECEGTBranch('" + this.mDay.trim()
                       + "','" + this.mDay.trim() + "','" + tComCode.trim()
                       + "') from ldsysvar where sysvar='onerow'";
                System.out.println("----日件数tSql = " + tSql.trim());
                strArr[3] = aExeSQL.getOneValue(tSql).trim();
                data[3] += Double.parseDouble(strArr[3]);

                //月交单规模保费
                tSql =
                        "select NVL(sum(paymoney),0) from ljtempfee_lmriskapp3 where MakeDate >='" +
                        tDay[0].trim() + "' and MakeDate <='" + mDay + "' and agentgroup in (select agentgroup from labranchgroup where branchtype = '1' and (state<>'1' or state is null) and branchattr like '" +
                        tComCode.trim() + "%')";

                System.out.println("----月交单规模保费tSql = " + tSql.trim());
                strArr[4] = aExeSQL.getOneValue(tSql).trim();
                data[4] += Double.parseDouble(strArr[4]);

                //月件数
                tSql = "select CALPIECEGTBranch('" + tDay[0].trim()
                       + "','" + this.mDay.trim() + "','"
                       + tComCode.trim()
                       + "') from ldsysvar where sysvar='onerow'";
                System.out.println("----月件数tSql = " + tSql.trim());
                strArr[5] = aExeSQL.getOneValue(tSql).trim();
                data[5] += Double.parseDouble(strArr[5]);

                tListTab.add(strArr);
            }

            strArr = new String[6];
            strArr[0] = "总计";
            //网点数
            for (int i = 1; i <= 5; i++)
            {
                strArr[i] = String.valueOf(data[i]);
            }
            tListTab.add(strArr);

            XmlExport xmlexport = new XmlExport();
            xmlexport.createDocument("HeadPolCounterF.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tListTab, strArr);
//    xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
            this.mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "HeadPolCounterF";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("------prepareData end-----");

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void dealError(String FuncName, String ErrMsg)
    {
        CError error = new CError();

        error.moduleName = "HeadPolCounterF";
        error.errorMessage = ErrMsg.trim();
        error.functionName = FuncName.trim();

        this.mErrors.addOneError(error);
    }


}