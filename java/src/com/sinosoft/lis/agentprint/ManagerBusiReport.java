package com.sinosoft.lis.agentprint;

import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class ManagerBusiReport
{
    public GlobalInput mGI = new GlobalInput();
    public String mStartDate = "";
    public String mEndDate = "";
    public VData mResult = new VData();
    public CErrors mErrors = new CErrors();

    public ManagerBusiReport()
    {
    }

    public static void main(String[] args)
    {
        ManagerBusiReport ManagerBusiReport1 = new ManagerBusiReport();
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86110000";
        tGI.ManageCom = "86110000";
        tGI.Operator = "aa";
        VData tInputData = new VData();
        tInputData.clear();
        tInputData.addElement(tGI);
        tInputData.add(0, "2003-11-8");
        tInputData.add(1, "2003-12-8");
        if (!ManagerBusiReport1.getInputData(tInputData))
        {
            System.out.println("-----getinputdata failed!------");
        }
        if (!ManagerBusiReport1.prepareData())
        {
            System.out.println("-------prepareData failed!------");
        }

    }

    public boolean getInputData(VData cInputData)
    {
        //全局变量
        try
        {
            mStartDate = (String) cInputData.getObjectByObjectName("String", 0);
            mEndDate = (String) cInputData.getObjectByObjectName("String", 1);
            mGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput",
                    0);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("getInputData", ex.toString());
            return false;
        }
        System.out.println("---StartDate : " + this.mStartDate);
        System.out.println("---EndDate : " + this.mEndDate);
        return true;
    }

    public boolean prepareData()
    {
        ListTable tListTable = new ListTable();
        tListTable.setName("ManagerBusi");

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        String tStaticDate = "";
        tStaticDate = this.mStartDate.trim() + " 至 " + this.mEndDate.trim();
        texttag.add("StaticDate", tStaticDate); //输入制表时间

        //取本年度的第一天
        String tYearStart = this.mStartDate.trim().substring(0, 4) + "-1-1";
        System.out.println("---YearStartDate : " + tYearStart);

        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);

            //查询出各个区级
            SSRS aSSRS = new SSRS();
            String Sql1 = "select agentcode,sum(transmoney),sum(standprem) "
                          + "from lacommision where tmakedate >= '" +
                          this.mStartDate.trim()
                          + "' and tmakedate <= '" + this.mEndDate.trim()
                          + "' and agentcode in ("
                          + "select agentcode from laagent "
                          +
                          "where branchtype='3' and insideflag = '1' and managecom like '"
                          + this.mGI.ManageCom.trim() +
                          "%') group by agentcode "
                          + "order by sum(transmoney) DESC";

            System.out.println("---查询客户经理：Sql1 = " + Sql1);
            int tManagerCount = 0;

            aSSRS = aExeSQL.execSQL(Sql1);
            tManagerCount = aSSRS.getMaxRow();
            if (tManagerCount == 0)
            {
                dealError("prepareData", "客户经理信息查询失败！");
                return false;
            }
            String strArr[] = null;

            String[] tAgentCode = new String[tManagerCount];
            String[] tTransMoney = new String[tManagerCount];
            String[] tStandPrem = new String[tManagerCount];
            float tSum = 0;
            float tSumAdd = 0;
            float tSumStand = 0;
            float tSumStandAdd = 0;

            for (int i = 1; i <= tManagerCount; i++)
            {
                strArr = new String[8];
                System.out.println("--------" + i + "---------");

                tAgentCode[i - 1] = new String();
                tAgentCode[i - 1] = aSSRS.GetText(i, 1);
                tTransMoney[i - 1] = new String();
                tTransMoney[i - 1] = aSSRS.GetText(i, 2);
                tStandPrem[i - 1] = new String();
                tStandPrem[i - 1] = aSSRS.GetText(i, 3);

                System.out.println("------AgentCode: " + tAgentCode[i - 1]);
                System.out.println("------TransMoney: " + tTransMoney[i - 1]);
                System.out.println("------StandPrem: " + tStandPrem[i - 1]);

                String tAgentName = "";
                String tAgentGrade = "";
                String Sql2 =
                        "select x.name,y.agentgrade from laagent x,latree y "
                        + "where x.agentcode = y.agentcode and x.agentcode = '"
                        + tAgentCode[i - 1].trim() + "'";
                SSRS bSSRS = new SSRS();
                bSSRS = aExeSQL.execSQL(Sql2);
                tAgentName = bSSRS.GetText(1, 1);
                tAgentGrade = bSSRS.GetText(1, 2);

                strArr[0] = tAgentName;
                strArr[1] = Integer.toString(i);
                strArr[2] = tTransMoney[i - 1];
                strArr[4] = tStandPrem[i - 1];
                strArr[6] = tAgentGrade;

                //累计实收保费、累计标准保费
                Sql2 =
                        "select nvl(sum(transmoney),0),nvl(sum(standprem),0) from lacommision "
                        + "where tmakedate >= '" + tYearStart.trim()
                        + "' and tmakedate <= '" + this.mEndDate.trim() + "'"
                        + " and trim(agentcode) = '" + tAgentCode[i - 1].trim() +
                        "'";
                SSRS cSSRS = new SSRS();
                cSSRS = aExeSQL.execSQL(Sql2);
                strArr[3] = cSSRS.GetText(1, 1);
                strArr[5] = cSSRS.GetText(1, 2);
                tListTable.add(strArr);

                //累计
                tSum += Float.parseFloat(strArr[2].trim());
                tSumAdd += Float.parseFloat(strArr[3].trim());
                tSumStand += Float.parseFloat(strArr[4].trim());
                tSumStandAdd += Float.parseFloat(strArr[5].trim());
            }

            try
            {
                conn.close();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }

            //合计
            strArr = new String[8];
            strArr[0] = "合计";
            strArr[2] = Float.toString(tSum).trim();
            strArr[3] = Float.toString(tSumAdd).trim();
            strArr[4] = Float.toString(tSumStand);
            strArr[5] = Float.toString(tSumStandAdd).trim();
            tListTable.add(strArr);

            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("ManagerBusiOrder.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tListTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
            this.mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ManagerBusiReport";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("------prepareData end-----");

        return true;
    }

    private void dealError(String tFuncName, String tErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "ManagerBusiReport";
        cError.functionName = tFuncName;
        cError.errorMessage = tErrMsg;
        this.mErrors.addOneError(cError);
    }

    public VData getResult()
    {
        return this.mResult;
    }

}