package com.sinosoft.lis.agentprint;

import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.ReportPubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title:AgentGroupInfoBL </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */

public class AgentGroupInfoBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    /**取得查询条件中管理机构和日期的值*/
    private String mManageCom = "";
    private String mStartDate = "";
    private String mEndDate = "";

    public AgentGroupInfoBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        /**得到从外部传来的数据，并备份到本类中*/
        if (!getInputData(cInputData))
        {
            return false;
        }
        /**业务处理*/
        if (!dealData())
        {
            return false;
        }
        mResult.clear();
        /**准备所有要打印的数据*/
        if (!getPrintData())
        {
            return false;
        }
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        /**保存从外部传来的管理机构的信息，作为查询条件*/
        System.out.println("getInputData");
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mManageCom = (String) cInputData.get(1);
        System.out.println(mManageCom);
        mStartDate = (String) cInputData.get(2);
        System.out.println(mStartDate);
        mEndDate = (String) cInputData.get(3);
        System.out.println(mEndDate);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    private boolean dealData()
    {
        System.out.println("dealData");
        return true;
    }

    private boolean getPrintData()
    {
        System.out.println("getPrintData0");
        String[] strArr = null;
        //String displayname = "";
        XmlExport xmlexport = new XmlExport();
        System.out.println("getPrintData0.1");
        xmlexport.createDocument("AgentGroupInfo.vts", "printer");
        //xmlexport.addDisplayControl(displayname);
        System.out.println("getPrintData0.2");
        ListTable tlistTable = new ListTable();
        tlistTable.setName("AgentGroupInfo");

        String StrSQL = "";
        String SQL1 = "select comcode from ldcom where comcode like '" +
                      mManageCom + "%' order by comcode asc";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        tSSRS = tExeSQL.execSQL(SQL1);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String tManageCom = tSSRS.GetText(i, 1).trim();
            StrSQL =
                    "select comcode,name,"
                    //计算期初人力
                    +
                    "(select count(agentcode) from laagent where laagent.managecom like '" +
                    tManageCom + "%' and "
                    + "EmployDate < '" + mStartDate + "' and "
                    + "(OutWorkDate is null  or "
                    + "OutWorkDate >=  '" + mStartDate + "' ) ),"
                    //计算新增人力
                    +
                    "(select count(agentcode) from laagent where laagent.managecom like '" +
                    tManageCom + "%' and "
                    + "EmployDate >= '" + mStartDate + "' and "
                    + "EmployDate <= '" + mEndDate + "' and "
                    + "(OutWorkDate is null  or "
                    + "OutWorkDate >  '" + mEndDate + "' ) ),"
                    //计算脱落人力
                    +
                    "(select count(agentcode) from laagent where laagent.managecom like '" +
                    tManageCom + "%' and "
                    + "EmployDate < '" + mStartDate + "' and "
                    + "OutWorkDate >= '" + mStartDate + "' and "
                    + "OutWorkDate <  '" + mEndDate + "' ),"
                    //填充，为计算脱落率预留空间
                    + " outcomcode,"
                    //计算期末人力
                    +
                    "(select count(agentcode) from laagent where laagent.managecom like '" +
                    tManageCom + "%' and "
                    + "EmployDate <= '" + mEndDate + "' and "
                    + "(OutWorkDate is null  or "
                    + "OutWorkDate >  '" + mEndDate + "' ) )"

                    + " from ldcom where comcode = '" + tManageCom +
                    "' order by comcode asc";
            System.out.println("-----------分 " + i + "  begin------------");
            System.out.println(StrSQL);
            System.out.println("------------分 " + i + "  end-----------");
            ExeSQL tExeSQL1 = new ExeSQL();
            SSRS tSSRS1 = new SSRS();
            tSSRS1 = tExeSQL1.execSQL(StrSQL);
            System.out.println("getPrintData " + i);
            for (int k = 1; k <= tSSRS1.getMaxRow(); k++)
            {
                strArr = new String[12];
                strArr[0] = "";
                strArr[1] = "";
                strArr[2] = "";
                strArr[3] = "";
                strArr[4] = "";
                strArr[5] = "";
                strArr[6] = "";
                strArr[7] = "";
                strArr[8] = "";
                strArr[9] = "";
                strArr[10] = "";
                strArr[11] = "";
                for (int j = 1; j <= tSSRS1.getMaxCol(); j++)
                {
                    System.out.println(j);
                    if (j != 6)
                    {
                        strArr[j - 1] = tSSRS1.GetText(k, j);
                    }
                    else
                    {
                        double a = Integer.parseInt(strArr[4]);
                        int b = Integer.parseInt(strArr[2]);
                        if (b != 0)
                        {
                            strArr[5] = String.valueOf(a / b);
                        }
                        else
                        {
                            strArr[5] = "0.0";
                        }
                    }
                }
                tlistTable.add(strArr);
            }

        }

        strArr = new String[12];
        strArr[0] = "总计";
        strArr[1] = "";
        //统计总期初人力
        System.out.println("-----------统计总期初人力 begin------------");
        StrSQL = new String();
        StrSQL = "select count(agentcode) from laagent where managecom like '"
                 + mManageCom + "%'and "
                 + "EmployDate <= '" + mStartDate
                 + "' and (OutWorkDate is null  or OutWorkDate > '" +
                 mStartDate + "')";
        tSSRS = new SSRS();
        tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(StrSQL);
        System.out.println(tSSRS.GetText(1, 1));
        strArr[2] = tSSRS.GetText(1, 1);
        //统计总新增人力
        System.out.println("-----------统计总新增人力 begin------------");
        StrSQL = new String();
        StrSQL = "select count(agentcode) from laagent where managecom like '"
                 + mManageCom + "%'and "
                 + "EmployDate >= '" + mStartDate + "' and "
                 + "EmployDate <= '" + mEndDate + "' and "
                 + "(OutWorkDate is null  or "
                 + "OutWorkDate >  '" + mEndDate + "' )";
        tSSRS = new SSRS();
        tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(StrSQL);
        System.out.println(tSSRS.GetText(1, 1));
        strArr[3] = tSSRS.GetText(1, 1);
        //统计总脱落人力
        System.out.println("-----------统计总脱落人力 begin------------");
        StrSQL = new String();
        StrSQL = "select count(agentcode) from laagent where managecom like '"
                 + mManageCom + "%'and "
                 + "EmployDate < '" + mStartDate + "' and "
                 + "OutWorkDate >= '" + mStartDate + "' and "
                 + "OutWorkDate <  '" + mEndDate + "'";
        tSSRS = new SSRS();
        tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(StrSQL);
        System.out.println(tSSRS.GetText(1, 1));
        strArr[4] = tSSRS.GetText(1, 1);
        //统计总脱落率
        System.out.println("-----------统计总脱落率 begin------------");
        double a = Integer.parseInt(strArr[4]);
        int b = Integer.parseInt(strArr[2]);
        if (b != 0)
        {
            strArr[5] = String.valueOf(a / b);
        }
        else
        {
            strArr[5] = "0.0";
        }
        //统计总期末人力
        System.out.println("-----------统计总期末人力 begin------------");
        StrSQL = new String();
        StrSQL = "select count(agentcode) from laagent where managecom like '"
                 + mManageCom + "%'and "
                 + "EmployDate <= '" + mEndDate + "' and "
                 + "(OutWorkDate is null  or "
                 + "OutWorkDate >  '" + mEndDate + "' ) ";
        tSSRS = new SSRS();
        tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(StrSQL);
        System.out.println(tSSRS.GetText(1, 1));
        strArr[6] = tSSRS.GetText(1, 1);
        strArr[7] = "";
        strArr[8] = "";
        strArr[9] = "";
        strArr[10] = "";
        strArr[11] = "";
        tlistTable.add(strArr);
        System.out.println("getPrintData2");

        strArr = new String[12];
//                strArr[0] = "代码";
//                strArr[1] = "名称";
//                strArr[2] = "期初人力";
//                strArr[3] = "新增人力";
//                strArr[4] = "脱落人力";
//                strArr[5] = "脱落率";
//                strArr[6] = "期末人力";
//                strArr[7] = "职级上升";
//                strArr[8] = "维持原有职级";
//                strArr[9] = "职级下降";
//                strArr[10] = "活动率";
//                strArr[11] = "人均保费(元)";

        xmlexport.addListTable(tlistTable, strArr);

        AgentPubFun.formatDate(mStartDate, "yyyyMMdd");
        AgentPubFun.formatDate(mEndDate, "yyyyMMdd");
        String CurrentDate = PubFun.getCurrentDate();
        AgentPubFun.formatDate(CurrentDate, "yyyyMMdd");

        TextTag texttag = new TextTag();
        String YYMMDD = "";
        YYMMDD = mStartDate.substring(0, mStartDate.indexOf("-")) + "年"
                 +
                 mStartDate.substring(mStartDate.indexOf("-") + 1,
                                      mEndDate.lastIndexOf("-")) + "月"
                 + mStartDate.substring(mStartDate.lastIndexOf("-") + 1) + "日";
        texttag.add("StartDate", YYMMDD);
        YYMMDD = "";
        YYMMDD = mEndDate.substring(0, mEndDate.indexOf("-")) + "年"
                 +
                 mEndDate.substring(mEndDate.indexOf("-") + 1,
                                    mEndDate.lastIndexOf("-")) + "月"
                 + mEndDate.substring(mEndDate.lastIndexOf("-") + 1) + "日";
        texttag.add("EndDate", YYMMDD);
        texttag.add("ManageCom", ReportPubFun.getMngName(mManageCom));
        YYMMDD = "";
        YYMMDD = CurrentDate.substring(0, CurrentDate.indexOf("-")) + "年"
                 +
                 CurrentDate.substring(CurrentDate.indexOf("-") + 1,
                                       CurrentDate.lastIndexOf("-")) + "月"
                 + CurrentDate.substring(CurrentDate.lastIndexOf("-") + 1) +
                 "日";
        texttag.add("MakeDate", YYMMDD);
        xmlexport.addTextTag(texttag);
        System.out.println("getPrintData3");
        mResult.clear();
        mResult.addElement(xmlexport);

        System.out.println("end");
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "AgentGroupInfoBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**将从BL类中返回到数据传递到上一层*/
    public VData getResult()
    {
        return this.mResult;
    }

    public static void main(String[] args)
    {
        AgentGroupInfoBL agentgroupinfobl = new AgentGroupInfoBL();
    }
}
