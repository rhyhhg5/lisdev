package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PolCalcountOrderBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private GlobalInput mGlobalInput = new GlobalInput();
    private String StartDay = "";
    private String EndDay = "";
    private String ManageCom = "";

    //业务处理相关变量
    /** 全局数据 */
    public PolCalcountOrderBL()
    {
    }

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("------come in BL----");
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!queryData())
        {
            System.out.println("---Error: " +
                               this.mErrors.getFirstError().toString());
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量

        ManageCom = (String) cInputData.get(0);
        StartDay = (String) cInputData.get(1);
        EndDay = (String) cInputData.get(2);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 3));

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "PolYSOrderBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        ListTable tlistTable = new ListTable();
        tlistTable.setName("PolCalcountOrder");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDay", StartDay + "---" + EndDay); //输入制表时间
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);
            String aasql =
                    "select trim(name) from labranchgroup where branchattr='" +
                    ManageCom + "'";
            System.out.println("----aasql= " + aasql.trim());
            String managename = aExeSQL.getOneValue(aasql);
            System.out.println("----managename = " + managename.trim());
            texttag.add("ManageCom", managename);
            String strArr[] = new String[7];
            //相关数据
            String msql =
                    "select b.name,a.name,a.agentcode,agentgrade,sum(calcount),sum(standprem) "
                    + "from labranchgroup b,latree c,laagent a,lacommision d "
                    + "where branchlevel='01' and a.agentcode=c.agentcode and a.branchcode=b.agentgroup "
                    +
                    "and a.agentcode=d.agentcode and d.branchtype='1' and tMakeDate >='" +
                    StartDay + "' and tMakeDate <='" + EndDay
                    + "' and b.branchattr like '" + ManageCom +
                    "%' and b.branchattr like '" + mGlobalInput.ManageCom +
                    "%' group by b.name,a.name,a.agentcode,agentgrade "
                    + " order by sum(calcount) desc";

            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(msql);
            int Row = 3000;
            if (aSSRS.getMaxRow() < 3000)
            {
                Row = aSSRS.getMaxRow();
            }
            for (int z = 1; z <= Row; z++)
            {
                strArr = new String[7];
                strArr[0] = String.valueOf(z);
                for (int u = 1; u <= 6; u++)
                {
                    strArr[u] = aSSRS.GetText(z, u);
                }
                tlistTable.add(strArr);
            }
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("PolCalcountOrder.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PolCalcountOrderBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    public static void main(String[] args)
    {
    }
}