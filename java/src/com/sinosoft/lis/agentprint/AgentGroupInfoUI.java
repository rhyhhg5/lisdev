package com.sinosoft.lis.agentprint;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: AgentGroupInfoUI</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */

public class AgentGroupInfoUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    /**取得查询条件中管理机构和日期的值*/
    private String mManageCom = "";
    private String mStartDate = "";
    private String mEndDate = "";
    public AgentGroupInfoUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        /**得到从外部传来的数据，并备份到本类中*/
        if (!getInputData(cInputData))
        {
            return false;
        }
        /**业务处理*/
        if (!dealData())
        {
            return false;
        }
        VData vData = new VData();
        /**准备传往后台的数据*/
        if (!prepareOutputData(vData))
        {
            return false;
        }
        /**产生一个BL类，开始调用BL操作*/
        AgentGroupInfoBL tAgentGroupInfoBL = new AgentGroupInfoBL();
        System.out.println("Start AgentGroupInfoUI Submit ...");
        /**将准备好的数据传入给BL类，开始查找数据库*/
        if (!tAgentGroupInfoBL.submitData(vData, cOperate))
        {
            if (tAgentGroupInfoBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tAgentGroupInfoBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "tAgentGroupInfoBL发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            /**从BL类中返回查询结果*/
            mResult = tAgentGroupInfoBL.getResult();
            return true;
        }

    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "AgentGroupInfoUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        mManageCom = (String) cInputData.get(1);
        mStartDate = (String) cInputData.get(2);
        mEndDate = (String) cInputData.get(3);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    /**将数据添加到vData中，以便调用BL类*/
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.add(mGlobalInput);
            vData.addElement(mManageCom);
            vData.add(mStartDate);
            vData.add(mEndDate);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    private boolean dealData()
    {
        return true;
    }

    /**将从BL类中返回到数据传递到上一层*/
    public VData getResult()
    {
        return this.mResult;
    }

    public static void main(String[] args)
    {

    }
}
