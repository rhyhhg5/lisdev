package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class MonthWageBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    //取得的时间
    private String mDay = "";
    private String mManageCom = "";


    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    public MonthWageBL()
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mDay = (String) cInputData.get(0);
        mManageCom = (String) cInputData.get(1);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "PolFilialeBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String strArr[] = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("WAGE");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCode(mManageCom);
        tLDCodeDB.setCodeType("station");
        tLDCodeDB.getInfo();
        texttag.add("Name", tLDCodeDB.getCodeName()); //输入制表机构
        texttag.add("Day", mDay); //输入制表时间
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);

            //计算代理人编码、职级、支助薪资
//2004-7-21 根据杨桦需求 只打印发放佣金人员信息
//2004-8-26 根据杨桦需求 增加管理机构、同业招募、创业奖、加扣2
//2004-11-03 根据杨桦需求 增加个人期交保费,小组期交保费,部门期交保费,区期交保费
            String vsql =
                    "select a.agentcode,c.agentgrade,c.branchattr,a.name,a.managecom,"
                    + "nvl(sum(f22),0),nvl(sum(f23),0),nvl(sum(f24),0),nvl(sum(K12),0) from laagent a,lawage c "
                    +
                    "where a.branchtype='1' and a.agentcode=c.agentcode and c.indexcalno='" +
                    mDay
                    + "' and a.managecom like '" + mManageCom +
                    "%' and c.state='1' "
                    + "group by a.agentcode,c.agentgrade,c.branchattr,a.name,a.managecom order by a.agentcode";
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(vsql);
            for (int z = 1; z <= aSSRS.getMaxRow(); z++)
            {
                strArr = new String[20];
                //代理人姓名、组别
                strArr[0] = aSSRS.GetText(z, 4);
                strArr[2] = aSSRS.GetText(z, 5);
                strArr[1] = aSSRS.GetText(z, 1);
                strArr[3] = aSSRS.GetText(z, 2);
                strArr[12] = aSSRS.GetText(z, 6);
                strArr[13] = aSSRS.GetText(z, 7);
                strArr[14] = aSSRS.GetText(z, 8);
                strArr[15] = aSSRS.GetText(z, 9);
                //个人保费、个人佣金
                String bSQL = "select nvl(sum(standprem),0),nvl(sum(FYC),0) from lacommision where CommDire=1 and wageno='" +
                              mDay + "' and agentcode='" + aSSRS.GetText(z, 1) +
                              "'";
                SSRS fSSRS = new SSRS();
                fSSRS = aExeSQL.execSQL(bSQL);
                String bsql =
                        "select nvl(sum(transmoney),0) from lacommision where CommDire=1 and wageno='" +
                        mDay + "' and agentcode='" + aSSRS.GetText(z, 1) +
                        "' and payintv<>0";
                SSRS mSSRS = new SSRS();
                mSSRS = aExeSQL.execSQL(bsql);
                strArr[4] = fSSRS.getMaxRow() > 0 ? fSSRS.GetText(1, 1) : "0";
                strArr[5] = fSSRS.getMaxRow() > 0 ? fSSRS.GetText(1, 2) : "0";
                strArr[16] = mSSRS.getMaxRow() > 0 ? mSSRS.GetText(1, 1) : "0";
                if (aSSRS.GetText(z, 2).equals("A01") ||
                    aSSRS.GetText(z, 2).equals("A02") ||
                    aSSRS.GetText(z, 2).equals("A03"))
                {
                    strArr[11] = "0";
                    strArr[6] = "0";
                    strArr[7] = "0";
                    strArr[8] = "0";
                    strArr[10] = "0";
                    strArr[9] = "0";
                    strArr[16] = "0";
                    strArr[17] = "0";
                    strArr[18] = "0";
                    strArr[19] = "0";
                }
                else if (aSSRS.GetText(z, 2).compareTo("A04") >= 0 &&
                         aSSRS.GetText(z, 2).compareTo("A05") <= 0)
                {
                    String dSQL = "select nvl(sum(standprem),0),nvl(sum(FYC),0) from LACommision where CommDire=1 and WageNo='"
                                  + mDay + "' and branchattr='" +
                                  aSSRS.GetText(z, 3) + "'";
                    SSRS cSSRS = new SSRS();
                    cSSRS = aExeSQL.execSQL(dSQL);
                    String bsql1 =
                            "select nvl(sum(transmoney),0) from lacommision where CommDire=1 and wageno='" +
                            mDay + "' and agentcode='" + aSSRS.GetText(z, 1) +
                            "' and payintv<>0";
                    SSRS mSSRS1 = new SSRS();
                    mSSRS1 = aExeSQL.execSQL(bsql1);
                    String dsql =
                            "select nvl(sum(transmoney),0) from lacommision where CommDire=1 and WageNo='"
                            + mDay + "' and branchattr='" + aSSRS.GetText(z, 3) +
                            "' and payintv<>0";
                    SSRS nSSRS = new SSRS();
                    nSSRS = aExeSQL.execSQL(dsql);
                    strArr[6] = cSSRS.getMaxRow() > 0 ? cSSRS.GetText(1, 1) :
                                "0";
                    strArr[7] = cSSRS.getMaxRow() > 0 ? cSSRS.GetText(1, 2) :
                                "0";
                    strArr[16] = mSSRS1.getMaxRow() > 0 ? mSSRS1.GetText(1, 1) :
                                 "0";
                    strArr[17] = nSSRS.getMaxRow() > 0 ? nSSRS.GetText(1, 1) :
                                 "0";
                    strArr[11] = "0";
                    strArr[8] = "0";
                    strArr[10] = "0";
                    strArr[9] = "0";
                    strArr[18] = "0";
                    strArr[19] = "0";
                }
                else if (aSSRS.GetText(z, 2).compareTo("A06") >= 0 &&
                         aSSRS.GetText(z, 2).compareTo("A07") <= 0)
                {
                    String dSQL = "select nvl(sum(standprem),0),nvl(sum(FYC),0) from LACommision where CommDire=1 and WageNo='"
                                  + mDay + "' and branchattr='" +
                                  aSSRS.GetText(z, 3) + "'";
                    SSRS cSSRS = new SSRS();
                    cSSRS = aExeSQL.execSQL(dSQL);
                    strArr[6] = cSSRS.getMaxRow() > 0 ? cSSRS.GetText(1, 1) :
                                "0";
                    strArr[7] = cSSRS.getMaxRow() > 0 ? cSSRS.GetText(1, 2) :
                                "0";
                    String a = aSSRS.GetText(z, 3).substring(0, 15);
                    String eSQL = "select nvl(sum(standprem),0),nvl(sum(FYC),0) from LACommision where CommDire=1 and WageNo='"
                                  + mDay + "' and BranchAttr like '" + a + "%'";
                    SSRS dSSRS = new SSRS();
                    dSSRS = aExeSQL.execSQL(eSQL);
                    String bsql1 =
                            "select nvl(sum(transmoney),0) from lacommision where CommDire=1 and wageno='" +
                            mDay + "' and agentcode='" + aSSRS.GetText(z, 1) +
                            "' and payintv<>0";
                    SSRS mSSRS1 = new SSRS();
                    mSSRS1 = aExeSQL.execSQL(bsql1);
                    String dsql =
                            "select nvl(sum(transmoney),0) from lacommision where CommDire=1 and WageNo='"
                            + mDay + "' and branchattr='" + aSSRS.GetText(z, 3) +
                            "' and payintv<>0";
                    SSRS nSSRS = new SSRS();
                    nSSRS = aExeSQL.execSQL(dsql);
                    String esql =
                            "select nvl(sum(transmoney),0) from lacommision where CommDire=1 and WageNo='"
                            + mDay + "' and BranchAttr like '" + a +
                            "%' and payintv<>0";
                    SSRS sSSRS = new SSRS();
                    sSSRS = aExeSQL.execSQL(esql);
                    strArr[8] = dSSRS.getMaxRow() > 0 ? dSSRS.GetText(1, 1) :
                                "0";
                    strArr[9] = dSSRS.getMaxRow() > 0 ? dSSRS.GetText(1, 2) :
                                "0";
                    strArr[16] = mSSRS.getMaxRow() > 0 ? mSSRS1.GetText(1, 1) :
                                 "0";
                    strArr[17] = nSSRS.getMaxRow() > 0 ? nSSRS.GetText(1, 1) :
                                 "0";
                    strArr[18] = sSSRS.getMaxRow() > 0 ? sSSRS.GetText(1, 1) :
                                 "0";
                    strArr[10] = "0";
                    strArr[11] = "0";
                    strArr[19] = "0";
                }
                else if (aSSRS.GetText(z, 2).compareTo("A08") >= 0 &&
                         aSSRS.GetText(z, 2).compareTo("A09") <= 0)
                {
                    String dSQL = "select nvl(sum(standprem),0),nvl(sum(FYC),0) from LACommision where CommDire=1 and WageNo='"
                                  + mDay + "' and branchattr='" +
                                  aSSRS.GetText(z, 3) + "'";
                    SSRS cSSRS = new SSRS();
                    cSSRS = aExeSQL.execSQL(dSQL);
                    strArr[6] = cSSRS.getMaxRow() > 0 ? cSSRS.GetText(1, 1) :
                                "0";
                    strArr[7] = cSSRS.getMaxRow() > 0 ? cSSRS.GetText(1, 2) :
                                "0";
                    String a = aSSRS.GetText(z, 3).substring(0, 15);
                    String eSQL = "select nvl(sum(standprem),0),nvl(sum(FYC),0) from LACommision where CommDire=1 and WageNo='"
                                  + mDay + "' and BranchAttr  like '" + a +
                                  "%'";
                    SSRS dSSRS = new SSRS();
                    dSSRS = aExeSQL.execSQL(eSQL);
                    strArr[8] = dSSRS.getMaxRow() > 0 ? dSSRS.GetText(1, 1) :
                                "0";
                    strArr[9] = dSSRS.getMaxRow() > 0 ? dSSRS.GetText(1, 2) :
                                "0";
                    String b = aSSRS.GetText(z, 3).substring(0, 12);
                    String fsql = "select nvl(sum(standprem),0),nvl(sum(FYC),0) from LACommision where CommDire=1 and WageNo='"
                                  + mDay + "' and BranchAttr like '" + b + "%'";
                    SSRS eSSRS = new SSRS();
                    eSSRS = aExeSQL.execSQL(fsql);
                    String bsql1 =
                            "select nvl(sum(transmoney),0) from lacommision where CommDire=1 and wageno='" +
                            mDay + "' and agentcode='" + aSSRS.GetText(z, 1) +
                            "' and payintv<>0";
                    SSRS mSSRS1 = new SSRS();
                    mSSRS1 = aExeSQL.execSQL(bsql1);
                    String dsql =
                            "select nvl(sum(transmoney),0) from lacommision where CommDire=1 and WageNo='"
                            + mDay + "' and branchattr='" + aSSRS.GetText(z, 3) +
                            "' and payintv<>0";
                    SSRS nSSRS = new SSRS();
                    nSSRS = aExeSQL.execSQL(dsql);
                    String esql =
                            "select nvl(sum(transmoney),0) from lacommision where CommDire=1 and WageNo='"
                            + mDay + "' and BranchAttr like '" + a +
                            "%' and payintv<>0";
                    SSRS sSSRS = new SSRS();
                    sSSRS = aExeSQL.execSQL(esql);
                    String fSQL =
                            "select nvl(sum(transmoney),0) from lacommision where CommDire=1 and WageNo='"
                            + mDay + "' and BranchAttr like '" + b +
                            "%' and payintv<>0";
                    SSRS rSSRS = new SSRS();
                    rSSRS = aExeSQL.execSQL(fSQL);

                    strArr[10] = eSSRS.getMaxRow() > 0 ? eSSRS.GetText(1, 1) :
                                 "0";
                    strArr[11] = eSSRS.getMaxRow() > 0 ? eSSRS.GetText(1, 2) :
                                 "0";
                    strArr[16] = mSSRS1.getMaxRow() > 0 ? mSSRS1.GetText(1, 1) :
                                 "0";
                    strArr[17] = nSSRS.getMaxRow() > 0 ? nSSRS.GetText(1, 1) :
                                 "0";
                    strArr[18] = sSSRS.getMaxRow() > 0 ? sSSRS.GetText(1, 1) :
                                 "0";
                    strArr[19] = rSSRS.getMaxRow() > 0 ? rSSRS.GetText(1, 1) :
                                 "0";
                }
                tlistTable.add(strArr);
            }
            conn.close();
        }
        catch (Exception ex)
        { // @@错误处理
            CError tError = new CError();
            tError.moduleName = "MonthWageBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        //END 支公司循环

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("MonthWage.vts", "printer"); //最好紧接着就初始化xml文档
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
        mResult.addElement(xmlexport);
        System.out.println("end");
        return true;
    }

    /*
       如果一个字符串数字中小数点后有值，则去掉小数点后面数值
     */

    public static void main(String[] args)
    {
        MonthWageBL tBL = new MonthWageBL();
        VData tVData = new VData();
        tVData.add("200409");
        tVData.add("86350000");
        tBL.submitData(tVData, "");
    }
}