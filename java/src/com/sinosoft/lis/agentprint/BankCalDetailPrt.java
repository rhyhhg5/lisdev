package com.sinosoft.lis.agentprint;

import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class BankCalDetailPrt
{
    public GlobalInput mGI = new GlobalInput();
    private String mStartDate = "";
    private String mEndDate = "";
    private String mAgentCom = "";
    private String mRiskCode = "";
    private String mCalType = "";
    public VData mResult = new VData();
    public CErrors mErrors = new CErrors();

    public BankCalDetailPrt()
    {
    }

    public static void main(String[] args)
    {
        BankCalDetailPrt bankCalDetailPrt1 = new BankCalDetailPrt();
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86110000";
        tGI.ManageCom = "86";
        tGI.Operator = "aa";
        VData tInputData = new VData();
        tInputData.clear();
        tInputData.addElement(tGI);
        tInputData.add(0, "2003-8-1");
        tInputData.add(1, "2003-12-1");
        tInputData.add(2, "180011");
        tInputData.add(3, "47");
        tInputData.add(4, "111302");

        if (!bankCalDetailPrt1.getInputData(tInputData))
        {
            System.out.println("-----getinputdata failed!------");
        }
        if (!bankCalDetailPrt1.prepareData())
        {
            System.out.println("-------prepareData failed!------");
        }

    }

    public boolean getInputData(VData cInputData)
    {
        //全局变量
        try
        {
            mStartDate = (String) cInputData.getObjectByObjectName("String", 0);
            mEndDate = (String) cInputData.getObjectByObjectName("String", 1);
            mAgentCom = (String) cInputData.getObjectByObjectName("String", 2);
            mRiskCode = (String) cInputData.getObjectByObjectName("String", 4);
            mCalType = (String) cInputData.getObjectByObjectName("String", 3);
            mGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput",
                    0);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("getInputData", ex.toString());
            return false;
        }
        System.out.println("---StartDate : " + this.mStartDate);
        System.out.println("---EndDate : " + this.mEndDate);
        System.out.println("---AgentCom : " + this.mAgentCom);
        System.out.println("---RiskCode : " + this.mRiskCode);
        System.out.println("---mcaltype : " + this.mCalType);
        return true;
    }

    public boolean prepareData()
    {
        String tSql = "";

        ListTable tListTable = new ListTable();
        tListTable.setName("CalDetail");

        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);

            TextTag texttag = new TextTag();
            tSql = "select trim(name) from lacom where trim(agentcom) = '"
                   + this.mAgentCom.trim() + "'";
            String tStaticOrg = "";
            tStaticOrg = aExeSQL.getOneValue(tSql).trim();
            texttag.add("StaticOrg", tStaticOrg);

            String tStaticDate = "";
            tStaticDate = this.mStartDate.trim() + " 至 " + this.mEndDate.trim();
            texttag.add("StaticDate", tStaticDate); //输入制表时间

            if (this.mRiskCode.trim().equals("") || this.mRiskCode == null)
            {
                tSql = "select a.agentcom,b.name,c.name,d.riskname,a.p11,"
                       + "a.p14,a.polno,a.signdate,a.cvalidate,"
                       + "a.makepoldate,a.customgetpoldate,a.transmoney,"
                       + "e.chargerate,e.charge "
                       + "from lacom b,laagent c,"
                       + "lmriskapp d,lacharge e,lacommision a "
                       + "where a.tmakedate >= '" + this.mStartDate.trim()
                       + "' and a.tmakedate <= '" + this.mEndDate.trim()
                       +
                       "' and a.agentcode = c.agentcode and a.agentcom = b.agentcom "
                       +
                       " and a.riskcode = d.riskcode and a.commisionsn = e.commisionsn "
                       + " and e.chargetype = '" + this.mCalType.trim()
                       + "' and a.branchtype = '3'"
                       + " and a.agentcom like '" + this.mAgentCom.trim() +
                       "%'"
                       + " order by a.agentcom,a.riskcode";
            }
            else
            {
                tSql = "select a.agentcom,b.name,c.name,d.riskname,a.p11,"
                       + "a.p14,a.polno,a.signdate,a.cvalidate,"
                       + "a.makepoldate,a.customgetpoldate,a.transmoney,"
                       + "e.chargerate,e.charge "
                       + "from lacom b,laagent c,"
                       + "lmriskapp d,lacharge e,lacommision a "
                       + "where a.tmakedate >= '" + this.mStartDate.trim()
                       + "' and a.tmakedate <= '" + this.mEndDate.trim()
                       + "' and a.riskcode = '" + this.mRiskCode.trim()
                       +
                       "' and a.agentcode = c.agentcode and a.agentcom = b.agentcom "
                       + " and a.riskcode = d.riskcode "
                       + " and a.commisionsn = e.commisionsn"
                       + " and a.branchtype = '3' "
                       + " and e.chargetype = '" + this.mCalType.trim()
                       + "' and a.agentcom like '" + this.mAgentCom.trim()
                       + "%' order by a.agentcom,a.riskcode";
            }
            SSRS ssrs = new SSRS();
            ssrs = aExeSQL.execSQL(tSql);
            int tCount = 0;
            tCount = ssrs.getMaxRow();

            if (tCount == 0)
            {

                CError tError = new CError();
                tError.moduleName = "ALAAgentBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "没有数据!";
                this.mErrors.addOneError(tError);
                return false;
            }

            String[] strArr = null;
            String tLastAgentCom = "";
            String tAgentCom = "";
            for (int i = 1; i <= tCount; i++)
            {
                tAgentCom = ssrs.GetText(i, 1);
                strArr = new String[15];
                if (i != 1)
                {
                    if (!tAgentCom.trim().equals(tLastAgentCom.trim()))
                    {
                        for (int j = 0; j < 15; j++)
                        {
                            strArr[j] = "";
                        }
                        tListTable.add(strArr);
                        strArr = new String[15];
                    }
                }

                strArr[0] = ssrs.GetText(i, 1).trim();
                strArr[1] = ssrs.GetText(i, 2).trim();
                strArr[2] = ssrs.GetText(i, 3).trim();
                strArr[3] = ssrs.GetText(i, 4).trim();
                strArr[4] = ssrs.GetText(i, 5).trim();
                strArr[5] = ssrs.GetText(i, 6).trim();
                strArr[6] = ssrs.GetText(i, 7).trim();
                strArr[7] = ssrs.GetText(i, 8).trim();
                strArr[8] = ssrs.GetText(i, 9).trim();
                strArr[9] = ssrs.GetText(i, 10).trim();
                strArr[10] = ssrs.GetText(i, 11).trim();
                strArr[11] = ssrs.GetText(i, 12).trim();
                strArr[12] = ssrs.GetText(i, 13).trim();
                strArr[13] = ssrs.GetText(i, 14).trim();

                tListTable.add(strArr);
                tLastAgentCom = tAgentCom.trim();
            }

            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("BankCalDetail.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tListTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
            this.mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankCalDetailPrt";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("------prepareData end-----");

        return true;
    }

    private void dealError(String tFuncName, String tErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "BankCalDetailPrt";
        cError.functionName = tFuncName;
        cError.errorMessage = tErrMsg;
        this.mErrors.addOneError(cError);
    }

    public VData getResult()
    {
        return this.mResult;
    }

}