/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.agentprint;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class AgentInfoPrintUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();

    /**取得查询条件中管理机构的值*/
    private String mManageCom;
    public AgentInfoPrintUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        /**得到从外部传来的数据，并备份到本类中*/
        if (!getInputData(cInputData))
        {
            return false;
        }

        if (!dealData())
        {
            return false;
        }
        VData vData = new VData();
        /**得到从外部传来的数据，并备份到本类中*/
        if (!prepareOutputData(vData))
        {
            return false;
        }
        /**产生一个BL类，开始调用BL操作*/
        AgentInfoPrintBL tAgentInfoPrintBL = new AgentInfoPrintBL();
        System.out.println("Start AgentInfoPrintUI Submit ...");
        /**将准备好的数据传入给BL类，开始查找数据库*/
        if (!tAgentInfoPrintBL.submitData(vData, cOperate))
        {
            if (tAgentInfoPrintBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tAgentInfoPrintBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "tAgentInfoPrintBL发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            /**从BL类中返回查询结果*/
            mResult = tAgentInfoPrintBL.getResult();
            return true;
        }

    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "AgentInfoPrintUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getInputData(VData cInputData)
    {
        mManageCom = (String) cInputData.get(0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        return true;
    }

    /**将数据添加到vData中，以便调用BL类*/
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.addElement(mManageCom);
            vData.add(mGlobalInput);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    private boolean dealData()
    {
        return true;
    }

    /**将从BL类中返回到数据传递到上一层*/
    public VData getResult()
    {
        return this.mResult;
    }

    public static void main(String[] args)
    {

        GlobalInput tGlobalInput = new GlobalInput();
        String tManageCom = "86110000";
        VData tVData = new VData();
        tVData.addElement(tManageCom);
        tVData.add(tGlobalInput);
        AgentInfoPrintUI tAgentInfoPrintUI = new AgentInfoPrintUI();
        if (!tAgentInfoPrintUI.submitData(tVData, ""))
        {
            if (tAgentInfoPrintUI.mErrors.needDealError())
            {
                System.out.println(tAgentInfoPrintUI.mErrors.getFirstError());
            }
            else
            {
                System.out.println("UI发生错误，但是没有提供详细的出错信息");
            }
        }
        else
        {
            VData vData = tAgentInfoPrintUI.getResult();
            System.out.println("已经接收了数据!!!");
        }

    }

}
