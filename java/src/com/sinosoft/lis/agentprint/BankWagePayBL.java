package com.sinosoft.lis.agentprint;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.AgentPubFun;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class BankWagePayBL {

    public CErrors mErrors = new CErrors();

    private VData mInputData = new VData();

    private VData mResult = new VData();

    private String mOperate = "";

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();
    private String mBranchAttr = "";
    private String mIndexCalNo = "";
    private String mEndDate = "";
    private String mAgentState = "";
    private String mAgentGroup ="";
    private String mAgentCode ="";
    private String mAgentName ="";
    private String mManageCom = "";
    private String[] mDataList = null;
    private XmlExport mXmlExport = null;

    private SSRS mSSRS1 = new SSRS();


    private ListTable mListTable = new ListTable();

    private PubFun mPubFun = new PubFun();

    private String mManageName = "";

    public BankWagePayBL() {
    }
    public static void main(String[] args)
    {

        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";
        BankWagePayBL tBankWagePayBL = new BankWagePayBL();
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        mOperate = cOperate;
        mInputData = (VData) cInputData;
        if (mOperate.equals("")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!mOperate.equals("PRINT")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!this.getInputData(mInputData)) {
            return false;
        }

        if (!dealdate()) {
            return false;
        }

        if (!getPrintData()) {
            this.bulidError("getPrintData", "查询数据失败！");
            return false;
        }

        return true;
    }

    /**
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

        try
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);

        } catch (Exception ex) {
            this.mErrors.addOneError("");
            return false;
        }
        this.mManageCom = (String) mTransferData.getValueByName("tManageCom");
        this.mBranchAttr = (String) mTransferData.getValueByName("tBranchAttr");
        this.mAgentGroup = (String) mTransferData.getValueByName("tAgentGroup");
        this.mAgentCode = (String) mTransferData.getValueByName("tAgentCode");
        this.mIndexCalNo = (String) mTransferData.getValueByName("tIndexCalNo");
        this.mAgentName = (String) mTransferData.getValueByName("tAgentName");

        return true;
    }
    /**
    * 业务处理方法
    * @return boolean
    */

   private boolean dealdate()
   {
      if (!getAgentNow())
      {
         return false;
      }

      return true;
   }
   /**
    * 查询数据
    * @return boolean
   */
   private boolean getAgentNow() {
System.out.println("你好无聊！");
       ExeSQL tExeSQL = new ExeSQL();
       String tSql =
                " select c.branchattr,c.name,getUniteCode(a.agentcode),b.name ,a.agentgrade "
               +" ,b.employdate "
               +" ,case when b.agentstate<='02' then '在职' when b.agentstate='03' then '离职登记' else '离职确认' end"
               +" ,b.outworkdate,a.F07,A.F08,A.F10 "
               +" ,A.F11,A.F12,A.F13,A.F09,(a.F14+a.F15),(a.F30+a.F16+a.F18),(a.k12+a.F17+a.F19),a.currmoney "
               +" ,b.agentstate,a.managecom"
               +" from lawage  a,laagent b,labranchgroup  c  "
               +" where a.branchtype='3' and a.branchtype2='01' "
               +" and b.branchtype='3' and b.branchtype2='01' "
               +" and c.branchtype='3' and c.branchtype2='01' "
               +" and a.agentcode=b.agentcode and a.agentgroup=c.agentgroup "
               +" and  a.managecom like '" + mManageCom + "%' "
               +" and  a.indexcalno='"+mIndexCalNo+"'";

       if (mAgentCode != null && !mAgentCode.equals("")) {
           tSql += " and a.agentcode = '" + mAgentCode + "' ";
       }
       if (mAgentName != null && !mAgentName.equals("")) {
           tSql += " and b.name = '" + mAgentName+ "' ";
       }
       if (mBranchAttr != null && !mBranchAttr.equals("")) {
           tSql += " and c.branchattr like '%" + mBranchAttr + "%' ";
       }
       tSql += " order by a.managecom,c.branchattr,a.agentgrade,a.agentcode,b.name  with ur ";

       mSSRS1 = tExeSQL.execSQL(tSql);
       System.out.println(tSql);
       if (tExeSQL.mErrors.needDealError()) {
           CError tCError = new CError();
           tCError.moduleName = "MakeXMLBL";
           tCError.functionName = "creatFile";
           tCError.errorMessage = "查询XML数据出错！";

           this.mErrors.addOneError(tCError);

           return false;

       }
       if (mSSRS1.getMaxRow() <= 0) {
           CError tCError = new CError();
           tCError.moduleName = "MakeXMLBL";
           tCError.functionName = "creatFile";
           tCError.errorMessage = "没有符合条件的信息！";

           this.mErrors.addOneError(tCError);

           return false;
       }

       return true;
   }

    /**
     *
     * @return boolean
     */
    private boolean getPrintData() {
        TextTag tTextTag = new TextTag();

        mXmlExport = new XmlExport();
        //设置模版名称
        mXmlExport.createDocument("BankWagePay.vts", "printer");

        String tMakeDate = "";
        String tMakeTime = "";

        tMakeDate = mPubFun.getCurrentDate();
        tMakeTime = mPubFun.getCurrentTime();

        if (!getManageName()) {
            return false;
        }
        tTextTag.add("MakeDate", tMakeDate);
        tTextTag.add("MakeTime", tMakeTime);
        tTextTag.add("tName", mManageName);


        System.out.println("12121212" + tMakeDate);
        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);

        String[] title = {"","","","","","","","","","","","",
        	                "","","","","","","","","","","","",""  };

        if (!getListTable()) {
            return false;
        }
         this.mDataList = new String[25];
        // 3、追加 合计 行
        if(!setAddRow(mSSRS1.getMaxRow() ))
        {
           buildError("queryData", "进行合计计算时出错！");
           return false;
        }
         mListTable.setName("Order");
        System.out.print("111");
        mXmlExport.addListTable(mListTable, title);
        System.out.print("121");
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 查询列表显示数据
     * @return boolean
     */
    private boolean getListTable() {

        if (mSSRS1.getMaxRow() > 0) {
            for (int i = 1; i <= mSSRS1.getMaxRow(); i++) {
                String Info[] = new String[25];
                Info[0] = Integer.toString(i);
                Info[1] = mIndexCalNo;
                Info[2] = mSSRS1.GetText(i, 1);//团队编码
                Info[3] = mSSRS1.GetText(i, 2);//团队名称
                Info[4] = mSSRS1.GetText(i, 3);//人员编码
                Info[5] = mSSRS1.GetText(i, 4);//人员姓名
                Info[6] = getGradeName(mSSRS1.GetText(i, 5));//职级名称
                Info[7] = getBaseStand(mSSRS1.GetText(i, 21),mSSRS1.GetText(i, 5));//基本工资标准
                Info[8] = getPosiStand(mSSRS1.GetText(i, 21),mSSRS1.GetText(i, 5));//岗位工资标准
                Info[9] = mSSRS1.GetText(i, 6);//入司时间
                Info[10] = mSSRS1.GetText(i, 7);//在职状态
              //Info[11] = mSSRS1.GetText(i, 8);
                Info[11] = getOutDate(mSSRS1.GetText(i, 3),mSSRS1.GetText(i, 20),mSSRS1.GetText(i, 8));//离职日期
                Info[12] = mSSRS1.GetText(i, 9);//基本工资
                Info[13] = mSSRS1.GetText(i, 10);//岗位津贴
                //Info[14] = mSSRS1.GetText(i, 11);//长期服务津贴
                Info[14] = mSSRS1.GetText(i, 12);//绩效提奖
                Info[15] = mSSRS1.GetText(i, 13);//年终奖
                Info[16] = mSSRS1.GetText(i, 14);//管理津贴
                Info[17] = mSSRS1.GetText(i, 15);//季度岗位补发
                Info[18] = mSSRS1.GetText(i, 16);// 奖励方案
                Info[19] = mSSRS1.GetText(i, 17);//加款
                Info[20] = mSSRS1.GetText(i, 18);//扣款
                Info[21] = mSSRS1.GetText(i, 19);//实发工资合计
                Info[22] = getRewardRsn(mSSRS1.GetText(i, 3),mIndexCalNo);
                Info[23] = getPunishRsn(mSSRS1.GetText(i, 3),mIndexCalNo);
                mListTable.add(Info);
            }

        } else {
            CError tCError = new CError();
            tCError.moduleName = "CreateXml";
            tCError.functionName = "creatFile";
            tCError.errorMessage = "没有符合条件的信息！";
            this.mErrors.addOneError(tCError);
            return false;
        }
        return true;
    }


    /**
   * 追加一条合计行
   * @return boolean
   */
  private boolean setAddRow(int pmRow)
  {
      System.out.println("合计行的行数是："+pmRow);

      mDataList[0] = "合  计";
      mDataList[1] = mIndexCalNo;
      mDataList[2] = "";
              mDataList[3] ="";
              mDataList[4] ="";
              mDataList[5] ="";
              mDataList[6] = "";
              mDataList[7] ="";
              mDataList[8] ="";
              mDataList[9] = "";
              mDataList[10] = "";
              mDataList[11] ="";
              mDataList[12] = dealSum(9);
              mDataList[13] = dealSum(10);
              //mDataList[14] = dealSum(11);
              mDataList[14] = dealSum(12);
              mDataList[15] = dealSum(13);
              mDataList[16] = dealSum(14);
              mDataList[17] = dealSum(15);
              mDataList[18] = dealSum(16);
              mDataList[19] = dealSum(17);
              mDataList[20] = dealSum(18);
              mDataList[21] = dealSum(19);
              mDataList[22] = "";
              mDataList[23] = "";
              mListTable.add(mDataList);



      return true;
  }

  private String dealSum(int pmArrNum)
{
   String tReturnValue = "";
   DecimalFormat tDF = new DecimalFormat("0.##");
   String tSQL = "select 0";

   for(int i=1;i<=mSSRS1.getMaxRow();i++)
   {
       tSQL += " + " + mSSRS1.GetText(i,pmArrNum);
   }

   tSQL += " + 0 from dual";

   tReturnValue = "" + tDF.format(execQuery(tSQL));

   return tReturnValue;
}

    private String getGradeName(String pmAgentGrade)
    {
      String tSQL = "";
      String tRtValue="";
      tSQL = "select  gradename from laagentgrade where gradecode='" + pmAgentGrade +"'";
      SSRS tSSRS = new SSRS();
      ExeSQL tExeSQL = new ExeSQL();
      tSSRS = tExeSQL.execSQL(tSQL);
      if(tSSRS.getMaxRow()==0)
      {
      tRtValue="";
      }
      else
      tRtValue=tSSRS.GetText(1, 1);
      return tRtValue;
}

  private String getBaseStand(String pmManageCom,String pmAgentGrade)
{
    String tSQL = "";
   String tRtValue="";
   tSQL = " select decimal(basewage,12,2) from labasewage "
         +" where type='11' and managecom='"+pmManageCom+"' and agentgrade='"+pmAgentGrade+"'";
   SSRS tSSRS = new SSRS();
   ExeSQL tExeSQL = new ExeSQL();
   tSSRS = tExeSQL.execSQL(tSQL);
   if(tSSRS.getMaxRow()==0)
    {
      tRtValue="0";
    }
    else
   tRtValue=tSSRS.GetText(1, 1);
   return tRtValue;

}

    /**
      * 追加错误信息
      * @param szFunc String
      * @param szErrMsg String
      */
     private void buildError(String szFunc, String szErrMsg)
     {
         CError cError = new CError();
         cError.moduleName = "LAStatisticReportBL";
         cError.functionName = szFunc;
         cError.errorMessage = szErrMsg;
         System.out.println(szFunc + "--" + szErrMsg);
         this.mErrors.addOneError(cError);
     }


    private String getPosiStand(String pmManageCom,String pmAgentGrade)
  {
      String tSQL = "";
     String tRtValue="";
     tSQL = " select decimal(monthrate,12,2) from labasewage "
           +" where type='11' and managecom='"+pmManageCom+"' and agentgrade='"+pmAgentGrade+"'";
     SSRS tSSRS = new SSRS();
     ExeSQL tExeSQL = new ExeSQL();
     tSSRS = tExeSQL.execSQL(tSQL);
     if(tSSRS.getMaxRow()==0)
      {
        tRtValue="0";
      }
      else
     tRtValue=tSSRS.GetText(1, 1);
     return tRtValue;

}


 private String getRewardRsn(String pmAgentCode,String pmIndexCalNo)
     {
       String tSQL = "";
       String tRtValue="";
       tSQL = "select  distinct awardtitle from larewardpunish where branchtype='3'"
              +" and branchtype2='01' and doneflag<'10'  and agentcode='"+pmAgentCode+"'"
              +" and wageno='"+pmIndexCalNo+"'";
       SSRS tSSRS = new SSRS();
       ExeSQL tExeSQL = new ExeSQL();
       tSSRS = tExeSQL.execSQL(tSQL);
       if(tSSRS.getMaxRow()==0)
       {
       tRtValue="";
       }
       else
       for(int i=1;i<=tSSRS.getMaxRow();i++)
       {
           tRtValue=tRtValue+tSSRS.GetText(i, 1)+";";
       }
       return tRtValue;
}

   /**
      * 日期
      * @param  String
      * @return String
      */
     private String getOutDate(String pmAgentCode,String pmAgentState,String pmOutWorkDate)
     {
         String tSQL = "";
         String tRtValue="";
         if(pmAgentState.equals("03"))
         {
             tSQL =" select applydate from ladimission  where agentcode='" + pmAgentCode +"'"
                  +" order by departtimes desc fetch first 1 rows only";
            SSRS tSSRS = new SSRS();
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(tSQL);
            if(tSSRS.getMaxRow()==0)
            {
             tRtValue="";
            }
            else
            tRtValue=tSSRS.GetText(1, 1);
         }
         else
         { tRtValue=pmOutWorkDate;}


         return tRtValue;
     }

   private String getPunishRsn(String pmAgentCode,String pmIndexCalNo)
       {
         String tSQL = "";
         String tRtValue="";
         tSQL = "select  distinct punishrsn from larewardpunish where branchtype='3'"
                +" and branchtype2='01' and doneflag>'10'  and agentcode='"+pmAgentCode+"'"
                +" and wageno='"+pmIndexCalNo+"'";
         SSRS tSSRS = new SSRS();
         ExeSQL tExeSQL = new ExeSQL();
         tSSRS = tExeSQL.execSQL(tSQL);
         if(tSSRS.getMaxRow()==0)
         {
         tRtValue="";
         }
         else
         for(int i=1;i<=tSSRS.getMaxRow();i++)
         {
             tRtValue=tRtValue+tSSRS.GetText(i, 1)+";";
         }
         return tRtValue;
}


    private boolean getManageName() {

        String sql = "select name from ldcom where comcode='" + mManageCom +
                     "'";

        SSRS tSSRS = new SSRS();

        ExeSQL tExeSQL = new ExeSQL();

        tSSRS = tExeSQL.execSQL(sql);

        if (tExeSQL.mErrors.needDealError()) {

            this.mErrors.addOneError("销售单位不存在！");

            return false;

        }

        if (mManageCom.equals("86")) {
            this.mManageName = "";
        } else {
            this.mManageName = tSSRS.GetText(1, 1);
        }

        return true;
    }


    /**
     * 获取打印所需要的数据
     * @param cFunction String
     * @param cErrorMsg String
     */
    private void bulidError(String cFunction, String cErrorMsg) {

        CError tCError = new CError();

        tCError.moduleName = "BankWagePayBL";
        tCError.functionName = cFunction;
        tCError.errorMessage = cErrorMsg;

        this.mErrors.addOneError(tCError);

    }
    /**
      * 执行SQL文查询结果
      * @param sql String
      * @return double
      */
     private double execQuery(String sql)
     {
         Connection conn;
         conn = null;
         conn = DBConnPool.getConnection();

         System.out.println(sql);

         PreparedStatement st = null;
         ResultSet rs = null;
         try {
             if (conn == null)return 0.00;
             st = conn.prepareStatement(sql);
             if (st == null)return 0.00;
             rs = st.executeQuery();
             if (rs.next()) {
                 return rs.getDouble(1);
             }
             return 0.00;
         } catch (Exception ex) {
             ex.printStackTrace();
             return -1;
         } finally {
             try {
               if (!conn.isClosed()) {
                   conn.close();
               }
               try {
                   st.close();
                   rs.close();
               } catch (Exception ex2) {
                   ex2.printStackTrace();
               }
               st = null;
               rs = null;
               conn = null;
             } catch (Exception e) {}

         }
    }
    /**
     *
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


}
