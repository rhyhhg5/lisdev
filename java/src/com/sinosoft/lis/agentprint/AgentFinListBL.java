package com.sinosoft.lis.agentprint;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author Alse
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.FileWriter;
import java.io.*;

public class AgentFinListBL {
	/** 错误信息容器 */
	public CErrors mErrors = new CErrors();

	private GlobalInput mGI = null;
	private String mSql = null;
	private String mOutXmlPath = null;
	private String mAgentType = null;
	// private String mtype = null;

	private String mCurDate = PubFun.getCurrentDate();
	private String mCurTime = PubFun.getCurrentTime();

	public AgentFinListBL() {
	}

	public boolean submitData(VData cInputData, String operate) {

		if (!getInputData(cInputData)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		if (!dealData()) {
			return false;
		}

		return true;
	}

	/**
	 * 校验操作是否合法
	 * 
	 * @return boolean
	 */
	private boolean checkData() {

		return true;
	}

	/**
	 * dealData 处理业务数据
	 * 
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean dealData() {
		ExeSQL tExeSQL = new ExeSQL();
		System.out.println("BL->dealDate()");
		System.out.println(mSql);
		System.out.println(mOutXmlPath);
		SSRS tSSRS = tExeSQL.execSQL(mSql);

		if (tExeSQL.mErrors.needDealError()) {
			System.out.println(tExeSQL.mErrors.getErrContent());

			CError tError = new CError();
			tError.moduleName = "AgentFinListBL";
			tError.functionName = "dealData";
			tError.errorMessage = "没有查询到需要下载的数据";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}
		String[][] mToExcel = new String[tSSRS.getMaxRow() + 2][30];
		if (mAgentType.equals("1")) {
			mToExcel[0][0] = "银代薪资计算结果";
			mToExcel[0][10] = "银代薪资计算结果（营销人员）";
			mToExcel[1][0] = "管理机构";
			mToExcel[1][1] = "薪资年月";
			mToExcel[1][2] = "团队代码";
			mToExcel[1][3] = "业务员代码";
			mToExcel[1][4] = "业务员名称";
			mToExcel[1][5] = "职级名称";
			mToExcel[1][6] = "员工类型";
			mToExcel[1][7] = "入司时间";
			mToExcel[1][8] = "在职状态";
			mToExcel[1][9] = "离职(登记/确认)时间";
			mToExcel[1][10] = "基本报酬标准";			
			mToExcel[1][11] = "月度业绩达成率";
			mToExcel[1][12] = "基本报酬";
			mToExcel[1][13] = "基本报酬补发";
			mToExcel[1][14] = "首期绩效提奖";
			mToExcel[1][15] = "续期提奖";
			mToExcel[1][16] = "主管间接佣金";
			mToExcel[1][17] = "基本报酬加款";
			mToExcel[1][18] = "基本报酬扣款";
			mToExcel[1][19] = "绩效提奖加款";
			mToExcel[1][20] = "绩效提奖扣款";
			mToExcel[1][21] = "奖励方案加款";
			mToExcel[1][22] = "奖励方案扣款";
			mToExcel[1][23] = "其它加款";
			mToExcel[1][24] = "其它扣款";
		    mToExcel[1][25] = "交叉销售佣金";
		    mToExcel[1][26] = "应发工资合计";
		    mToExcel[1][27] = "实发金额";
		    mToExcel[1][28] = "确认状态";
		    
		    
	}
		else {
				mToExcel[0][0] = "银代薪资计算结果";
				mToExcel[0][10] = "银代薪资计算结果（直销人员）";
				mToExcel[1][0] = "管理机构";
				mToExcel[1][1] = "薪资年月";
				mToExcel[1][2] = "团队代码";
				mToExcel[1][3] = "业务员代码";
				mToExcel[1][4] = "业务员名称";
				mToExcel[1][5] = "职级名称";
				mToExcel[1][6] = "员工类型";
				mToExcel[1][7] = "入司时间";
				mToExcel[1][8] = "在职状态";
				mToExcel[1][9] = "离职(登记/确认)时间";
				mToExcel[1][10] = "基本报酬标准";					
				mToExcel[1][11] = "月度业绩达成率";
				mToExcel[1][12] = "基本工资";
				mToExcel[1][13] = "基本工资补发";
				mToExcel[1][14] = "绩效工资";
				mToExcel[1][15] = "绩效工资补发";
				mToExcel[1][16] = "基本工资加款";
				mToExcel[1][17] = "基本工资扣款";
				mToExcel[1][18] = "绩效工资加款";
				mToExcel[1][19] = "绩效工资扣款";
				mToExcel[1][20] = "奖励方案加款";
				mToExcel[1][21] = "奖励方案扣款";
				mToExcel[1][22] = "其它加款";
				mToExcel[1][23] = "其它扣款";
				mToExcel[1][24] = "交叉销售佣金";
				mToExcel[1][25] = "应发工资合计";
				mToExcel[1][26] = "实发金额";
				mToExcel[1][27] = "确认状态";
		}

		for (int row = 1; row <= tSSRS.getMaxRow(); row++) {
			for (int col = 1; col <= tSSRS.getMaxCol(); col++) {
				mToExcel[row + 1][col - 1] = tSSRS.GetText(row, col);
			}
		}

		try {
			WriteToExcel t = new WriteToExcel("");
			t.createExcelFile();
			String[] sheetName = { PubFun.getCurrentDate() };
			t.addSheet(sheetName);
			t.setData(0, mToExcel);
			t.write(mOutXmlPath);
		} catch (Exception ex) {
			ex.toString();
			ex.printStackTrace();
		}

		return true;
	}

	/**
	 * getInputData 将外部传入的数据分解到本类的属性中
	 * 
	 * @param cInputData
	 *            VData：submitData中传入的VData对象
	 * @return boolean：true提交成功, false提交失败
	 */
	private boolean getInputData(VData data) {
		mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
		TransferData tf = (TransferData) data.getObjectByObjectName(
				"TransferData", 0);

		if (mGI == null || tf == null) {
			CError tError = new CError();
			tError.moduleName = "AgentFinListBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}

		mSql = (String) tf.getValueByName("querySql");
		mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
		mAgentType = (String) tf.getValueByName("AgentType");
		// mtype = (String) tf.getValueByName("Type");
		if (mSql == null || mOutXmlPath == null || mAgentType == null) {
			CError tError = new CError();
			tError.moduleName = "AgentFinListBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的信息不完整2";
			mErrors.addOneError(tError);
			System.out.println(tError.errorMessage);
			return false;
		}

		return true;
	}

	public static void main(String[] args) {
		AgentFinListBL AgentFinListBL = new AgentFinListBL();
	}
}
