package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LAAssessInfoBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    private String IndexCalNo = "";
    private String AssessType = "";
    private String AgentGrade = "";
    private String ManageCom = "";
    private String BranchType = "";
    private String StandAssessFlag = "";

    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public LAAssessInfoBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        IndexCalNo = ((String) cInputData.getObjectByObjectName("String", 0));
        AssessType = ((String) cInputData.getObjectByObjectName("String", 1));
        AgentGrade = ((String) cInputData.getObjectByObjectName("String", 2));
        ManageCom = ((String) cInputData.getObjectByObjectName("String", 3));
        BranchType = ((String) cInputData.getObjectByObjectName("String", 4));
        StandAssessFlag = ((String) cInputData.getObjectByObjectName("String",
                5));
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 6));
        System.out.println("start");
        System.out.println(mGlobalInput.Operator);
        System.out.println(mGlobalInput.ManageCom);

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }


    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCPolBillF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        if (AssessType.equals("") || AssessType == null)
        {
            AssessType = "";
        }
        else if (AssessType.equals("01"))
        {
            AssessType = " AND a.AgentGrade > a.AgentGrade1 ";
        }
        else if (AssessType.equals("02"))
        {
            AssessType = " AND a.AgentGrade = a.AgentGrade1 ";
        }
        else if (AssessType.equals("03"))
        {
            AssessType = " AND a.AgentGrade < a.AgentGrade1 ";
        }

        if (ManageCom.equals("") || ManageCom == null)
        {
            ManageCom = " and a.ManageCom like '" + mGlobalInput.ManageCom +
                        "%'";
        }
        else
        {
            ManageCom = " and a.ManageCom like '" + ManageCom + "%'";
        }

        if (StandAssessFlag.equals("N"))
        {
            StandAssessFlag = " and StandAssessFlag='0'";
        }
        else
        {
            StandAssessFlag = " and StandAssessFlag='1'";
        }

        SSRS tSSRS = new SSRS();
        msql = "SELECT a.AgentGrade,a.BranchAttr,b.name,getUniteCode(a.AgentCode),(SELECT Name FROM LAAgent WHERE AgentCode = a.AgentCode),a.AgentGrade1 from LAbranchgroup b,LAAssess a " +
               "WHERE indexCalNo='" + IndexCalNo + "' and AgentGrade='" +
               AgentGrade + "' and a.branchtype='" + BranchType +
               "' and a.branchattr=b.branchattr"
               + AssessType + ManageCom + StandAssessFlag;
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);

        ListTable tlistTable = new ListTable();
        String strArr[] = null;
        tlistTable.setName("LAAssess");
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            strArr = new String[6];
            for (int j = 1; j <= tSSRS.getMaxCol(); j++)
            {
                strArr[j - 1] = tSSRS.GetText(i, j);
            }
            tlistTable.add(strArr);
        }
        strArr = new String[6];
        //strArr[0] = "营业组"; strArr[1] ="姓名";strArr[2] = "代码"; strArr[3] ="职级";strArr[4] ="主管属性";strArr[5] ="入司时间";strArr[6] ="代理人状态";strArr[7] = "增员人姓名"; strArr[8] ="组育成人姓名";strArr[9] = "部育成人姓名"; strArr[10] ="督导长育成人姓名";
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        String YearMonth = IndexCalNo.substring(0, 4) + "年" +
                           IndexCalNo.substring(4, 6) + "月";
        texttag.add("YearMonth", YearMonth); //输入制表时间
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("LAAssess.vts", "printer"); //最好紧接着就初始化xml文档
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
        mResult.addElement(xmlexport);
        return true;

    }

    public static void main(String[] args)
    {
        String BranchAttr = "8611";
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "8611";
        tG.ComCode = "8611";
        tG.Operator = "001";
        VData tVData = new VData();
        tVData.addElement(BranchAttr);
        tVData.addElement(tG);
        AgentTrussPF1PBL tAgentTrussPF1PUI = new AgentTrussPF1PBL();
        tAgentTrussPF1PUI.submitData(tVData, "PRINT");
    }
}