package com.sinosoft.lis.agentprint;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class BankStruInfoBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mManageCom = "";
    public BankStruInfoBL()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        /**得到从外部传来的数据，并备份到本类中*/
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        //准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "BankStruInfo";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);

    }

    private boolean queryData()
    {
        int a = 1;
        String companyname = ""; //companyname用来保存分公司的全称

        String asql = "";
        String aasql = "";
        String bsql = "";
        String csql = "";
        String dsql = "";
        String esql = "";
        String strArr[] = new String[7];

        TextTag texttag = new TextTag(); //新建一个TextTag的实例

        ListTable tListTable = new ListTable();
        tListTable.setName("BankAgInfo");
        SSRS aSSRS = new SSRS();
        SSRS bSSRS = new SSRS();
        SSRS cSSRS = new SSRS();
        SSRS dSSRS = new SSRS();
        SSRS eSSRS = new SSRS();

        /**查询分公司的名称 */
        aasql = "select ShortName from ldcom where Comcode='" + mManageCom +
                "'";

        ExeSQL aaExeSQL = new ExeSQL();
        companyname = aaExeSQL.getOneValue(aasql);
        /**如果存在与输入的managecom相符的分公司名字，则查询其他数据，否则，这一个记录置空 */
        if (!(companyname == ""))
        {
            texttag.add("filiale", companyname);
            /**查询与输入的managecom相符的渠道名称 */
            asql = "select AgentCom,Name from LACom where ManageCom like '" +
                   mManageCom + "%' and banktype='01' order by agentcom";

            ExeSQL bExeSQL = new ExeSQL();

            aSSRS = bExeSQL.execSQL(asql);
            if (!(aSSRS.getMaxRow() == 0))
            {
                for (int i = 1; i <= aSSRS.getMaxRow(); i++)
                {
                    a = 1;
                    /**查询这个渠道的总网点数 */
                    bsql = "select count(*) from lacom where upAgentCom like '" +
                           aSSRS.GetText(i, 1) + "%' and SellFlag='Y'";
                    bSSRS = bExeSQL.execSQL(bsql);

                    csql =
                            "select AgentCom,Name,areatype,channeltype from lacom where upAgentCom like '" +
                            aSSRS.GetText(i, 1) + "%' and SellFlag='Y'";
                    ExeSQL cExeSQL = new ExeSQL();

                    cSSRS = cExeSQL.execSQL(csql);
                    /**如果渠道下面存在支行，则查询支行的相关信息，否则，查询该渠道的相关信息 */
                    if (!(cSSRS.getMaxRow() == 0))
                    {
                        for (int j = 1; j <= cSSRS.getMaxRow(); j++)
                        {
                            strArr = new String[7];
                            strArr[2] = cSSRS.GetText(j, 2);
                            strArr[4] = cSSRS.GetText(j, 3);
                            strArr[5] = cSSRS.GetText(j, 4);
                            dsql =
                                    "select count(*) from lacom where upAgentCom like '" +
                                    cSSRS.GetText(j, 1) + "%' and SellFlag='Y'";
                            ExeSQL dExeSQL = new ExeSQL();

                            dSSRS = dExeSQL.execSQL(dsql);
                            if (!(dSSRS.getMaxRow() == 0))
                            {
                                strArr[3] = dSSRS.GetText(1, 1);
                            }
                            esql = "select name from laagent where agentcode in (select agentcode from lacomtoagent where agentcom='" +
                                   cSSRS.GetText(j, 1) + "' and relatype='1')";
                            ExeSQL eExeSQL = new ExeSQL();
                            eSSRS = eExeSQL.execSQL(esql);
                            if (!(eSSRS.getMaxRow() == 0))
                            {
                                strArr[6] = eSSRS.GetText(1, 1);
                            }
                            else
                            {
                                strArr[6] = "";
                            }
                            if (a == 1)
                            {
                                strArr[0] = aSSRS.GetText(i, 2);
                                strArr[1] = bSSRS.GetText(1, 1);
                                a++;
                            }
                            else
                            {
                                strArr[0] = strArr[1] = "";
                            }
                            tListTable.add(strArr);
                        }
                    }
                }
            }
        }
        /**将所有记录都保存到tListTable中之后，开始输出XML文件 */
        XmlExport xmlexport = new XmlExport(); //产生一个新的XmlExport对象
        /**产生vts文件 */
        xmlexport.createDocument("BankAgInfo.vts", "printer");
        /**将tListTable的信息输入到xmlexport 中*/
        xmlexport.addTextTag(texttag);
        xmlexport.addListTable(tListTable, strArr);
        /**xmlexport将所得数据流入下面目录下的一XML文件中*/
//    xmlexport.outputDocumentToFile("e:\\","test") ;
        /**将xmlexport添加到mResult中，以供以后调用*/
        mResult.addElement(xmlexport);
        System.out.println("end");
        return true;

    }

    public VData getResult()
    {
        return mResult;
    }

    private boolean getInputData(VData cInputData)
    {
        /** 保存从外部传来的管理机构的信息，作为查询条件  */
        mManageCom = (String) cInputData.get(0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public static void main(String[] args)
    {

        BankStruInfoBL tBankStruInfoBL = new BankStruInfoBL();
        String tManageCom = "8611";
        VData tVData = new VData();
        tVData.addElement(tManageCom);
        GlobalInput tGlobalInput = new GlobalInput();
        //LAAgentSchema tLAAgentSchema=new LAAgentSchema();
        tVData.add(tGlobalInput);
        // tVData.add(tLAAgentSchema);
        tBankStruInfoBL.submitData(tVData, "");

    }

}