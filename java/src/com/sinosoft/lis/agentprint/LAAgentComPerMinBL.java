package com.sinosoft.lis.agentprint;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.*;
import java.text.DecimalFormat;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class LAAgentComPerMinBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    SSRS tSSRS=new SSRS();
    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    public LAAgentComPerMinBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LAAgentComPerMinBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 3][20];

       mToExcel[0][2] = "机构增员奖报表(此表为动态数据，最终数据以文件规定日期及指标为准)" ;
       
       mToExcel[1][0]="分公司机构编码 ";        
       mToExcel[1][1]="分公司机构名称";
       mToExcel[1][2]="机构编码 ";        
       mToExcel[1][3]="机构名称";
       mToExcel[1][4]="业务员代码";
       mToExcel[1][5]="业务员名称";
       mToExcel[1][6]="入司日期";
       mToExcel[1][7]="离职日期";        
       mToExcel[1][8]="总保费";
       mToExcel[1][9]="期缴保费";
       mToExcel[1][10]="201112佣金";
       mToExcel[1][11]="201201佣金";
       mToExcel[1][12]="201202佣金";
       mToExcel[1][13]="201203佣金";
       mToExcel[1][14]="201204佣金";
       mToExcel[1][15]="201205佣金";
       mToExcel[1][16]="201206佣金";
       mToExcel[1][17]="挂0月";
      
        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
        	mToExcel[row +1][0]=tSSRS.GetText(row, 1);
        	mToExcel[row +1][1]=tSSRS.GetText(row, 2);
        	mToExcel[row +1][2]=tSSRS.GetText(row, 3);
        	mToExcel[row +1][3]=tSSRS.GetText(row, 4);
        	mToExcel[row +1][4]=tSSRS.GetText(row, 5);
        	mToExcel[row +1][5]=tSSRS.GetText(row, 6);
        	mToExcel[row +1][6]=tSSRS.GetText(row, 7);
        	mToExcel[row +1][7]=tSSRS.GetText(row, 8);
        	mToExcel[row +1][8]=getSTransmoney(mToExcel[row +1][4],"Z");  
        	mToExcel[row +1][9]=getSTransmoney(mToExcel[row +1][4],"Q"); 
        	String month=getYearMonth(mToExcel[row +1][6]);
        	mToExcel[row +1][10]=getSFyc(mToExcel[row +1][4],"201112",month);   
        	mToExcel[row +1][11]=getSFyc(mToExcel[row +1][4],"201201",month);    
        	mToExcel[row +1][12]=getSFyc(mToExcel[row +1][4],"201202",month);  
        	mToExcel[row +1][13]=getSFyc(mToExcel[row +1][4],"201203",month); 
        	mToExcel[row +1][14]=getSFyc(mToExcel[row +1][4],"201204",month); 
        	mToExcel[row +1][15]=getSFyc(mToExcel[row +1][4],"201205",month); 
        	mToExcel[row +1][16]=getSFyc(mToExcel[row +1][4],"201206",month); 
        	mToExcel[row +1][17]=getSZero(mToExcel[row +1][10],mToExcel[row +1][11],mToExcel[row +1][12],mToExcel[row +1][13],mToExcel[row +1][14],mToExcel[row +1][15],mToExcel[row +1][16]); 

        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentComPerMinBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        System.out.println("msql:"+mSql);
        System.out.println("path:"+mOutXmlPath);
        if(mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentComPerMinBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }
   
    
    
    
    private String getSFyc(String pmAgentCode,String pmWageNo,String pmYearMonth)
   {
     String tSQL = "";
     String tRtValue="";
     if(pmYearMonth.compareTo(pmWageNo)<=0)
     {
    	 tSQL = " select value(sum(a.fyc),0) "
       	  +" from lacommision  a where a.wageno ='"+pmWageNo+"' "
             +" and a.agentcode='"+pmAgentCode+"'";
        System.out.println(tSQL);
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        if(tSSRS.getMaxRow()==0)
         {
           tRtValue="0";
         }
        else
        tRtValue=tSSRS.GetText(1, 1);
     }
     else 
    	 tRtValue="-";
     
     return tRtValue;
   }
   
    private String getSZero(String pmFyc1,String pmFyc2,String pmFyc3,String pmFyc4,String pmFyc5,String pmFyc6,String pmFyc7)
    {
      String tRtValue="";
      int num=0;
      if(pmFyc1.equals("0"))
      {
    	  num=num+1;
      }
      if(pmFyc2.equals("0"))
      {
    	  num=num+1;
      }
      if(pmFyc3.equals("0"))
      {
    	  num=num+1;
      }
      if(pmFyc4.equals("0"))
      {
    	  num=num+1;
      }
      if(pmFyc5.equals("0"))
      {
    	  num=num+1;
      }
      if(pmFyc6.equals("0"))
      {
    	  num=num+1;
      }
      if(pmFyc7.equals("0"))
      {
    	  num=num+1;
      }
      tRtValue=String.valueOf(num);
      return tRtValue;
    }
    private String getSTransmoney(String pmAgentCode,String pmType)
    {
      String tSQL = "";
      String tRtValue="";
      tSQL = " select value(sum(a.transmoney),0) "
     	  +" from lacommision  a where a.wageno between '201112' and '201206'  "
           +" and a.agentcode='"+pmAgentCode+"'";

      if(pmType.equals("Q")){
           tSQL =tSQL +" and payintv=12 ";
      }   
      System.out.println(tSQL);
      SSRS tSSRS = new SSRS();
      ExeSQL tExeSQL = new ExeSQL();
      tSSRS = tExeSQL.execSQL(tSQL);
      if(tSSRS.getMaxRow()==0)
       {
         tRtValue="0";
       }
      else
      tRtValue=tSSRS.GetText(1, 1);
      return tRtValue;
    }
    
 
    private String getYearMonth(String pmEmployDate)
    {
      String tSQL = "";
      String tRtValue="";
      tSQL = " select yearmonth from lastatsegment where stattype='1'  "
           +" and startdate<='"+pmEmployDate+"' and '"+pmEmployDate+"'<= enddate ";

      System.out.println(tSQL);
      SSRS tSSRS = new SSRS();
      ExeSQL tExeSQL = new ExeSQL();
      tSSRS = tExeSQL.execSQL(tSQL);
      if(tSSRS.getMaxRow()==0)
       {
         tRtValue="0";
       }
      else
      tRtValue=tSSRS.GetText(1, 1);
      return tRtValue;
    }
   
  
    public static void main(String[] args)
    {
    System.out.println("11111111");
    }
}
