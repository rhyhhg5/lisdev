package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class AgentChargeBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private String StartDay = "";
    private String EndDay = "";
    private String AgentCom = "";
    private String RiskCode = "";

    private String strArr[] = new String[12];
    private double data[] = new double[12];
    private ListTable tlistTable = new ListTable();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    public AgentChargeBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!queryDataOther())
        {
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        StartDay = (String) cInputData.get(0);
        EndDay = (String) cInputData.get(1);
        AgentCom = (String) cInputData.get(2);
        RiskCode = (String) cInputData.get(3);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "AgentChargeBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryDataOther()
    {
        String strArr[] = null;
        strArr = new String[12];
        double data[] = new double[12];
        ListTable tlistTable = new ListTable();
        tlistTable.setName("AgentCharge");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDay", StartDay + "---" + EndDay);
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);

            String aasql = "select name from lacom where agentcom='" + AgentCom +
                           "'";
            texttag.add("AgentCom", aExeSQL.getOneValue(aasql));
            //险种名称
            if (RiskCode.equals("") || RiskCode == null)
            {
                String asql = "select trim(riskname),riskcode from lmriskapp where riskcode in (select riskcode from lacommision where  SignDate>='" +
                              StartDay + "' and SignDate<='" + EndDay +
                              "' and agentcom='" + AgentCom + "')";
                SSRS aSSRS = new SSRS();
                aSSRS = aExeSQL.execSQL(asql);
                for (int a = 1; a <= aSSRS.getMaxRow(); a++)
                {
                    for (int b = 1; b <= aSSRS.getMaxCol(); b++)
                    {
                        if (b == 1)
                        {
                            strArr[0] = aSSRS.GetText(a, 1);
                        }
                        if (b == 2)
                        {
                            //保费收入
                            String csql =
                                    " select nvl(sum(transmoney),0) from lacommision where  tmakeDate>='" +
                                    StartDay + "' and tmakeDate<='" + EndDay +
                                    "' and agentcom='" + AgentCom +
                                    "' and riskcode='" + aSSRS.GetText(a, 2) +
                                    "'";
                            strArr[1] = aExeSQL.getOneValue(csql);
                            data[1] += Double.parseDouble(strArr[1].trim());
                            //撤保收入
                            String dsql = "select nvl(sum(transmoney),0) from lacommision where transtype='WT' and tmakeDate>='" +
                                          StartDay + "' and tmakeDate<='" +
                                          EndDay + "' and agentcom='" +
                                          AgentCom + "' and riskcode='" +
                                          aSSRS.GetText(a, 2) + "'";
                            strArr[2] = aExeSQL.getOneValue(dsql);
                            data[2] += Double.parseDouble(strArr[2].trim());
                            //结算保费小计
                            String esql =
                                    " select nvl(sum(transmoney),0) from lacommision where  tmakeDate>='" +
                                    StartDay + "' and tmakeDate<='" + EndDay +
                                    "'  and agentcom='" + AgentCom +
                                    "' and riskcode='" + aSSRS.GetText(a, 2) +
                                    "'";
                            strArr[3] = aExeSQL.getOneValue(esql);
                            data[3] += Double.parseDouble(strArr[3].trim());
                            //手续费比例
                            String fsql = "select nvl(sum(chargerate),0) from lacharge where chargetype='51' and caldate>='" +
                                          StartDay + "' and caldate<='" +
                                          EndDay + "' and agentcom='" +
                                          AgentCom + "' and riskcode='" +
                                          aSSRS.GetText(a, 2) + "'";
                            strArr[4] = aExeSQL.getOneValue(fsql);
                            data[4] += Double.parseDouble(strArr[4].trim());
                            //手续费金额
                            String gsql =
                                    "select nvl(sum(charge),0) from lacharge where chargetype='51' and caldate>='" +
                                    StartDay + "' and caldate<='" + EndDay +
                                    "'  and agentcom='" + AgentCom +
                                    "' and riskcode='" + aSSRS.GetText(a, 2) +
                                    "'";
                            strArr[5] = aExeSQL.getOneValue(gsql);
                            data[5] += Double.parseDouble(strArr[5].trim());
                            //劳务费比例
                            String hsql = "select nvl(sum(chargerate),0) from lacharge where chargetype='52' and caldate>='" +
                                          StartDay + "' and caldate<='" +
                                          EndDay + "'  and agentcom='" +
                                          AgentCom + "' and riskcode='" +
                                          aSSRS.GetText(a, 2) + "'";
                            strArr[6] = aExeSQL.getOneValue(hsql);
                            data[6] += Double.parseDouble(strArr[6].trim());
                            //劳务费金额
                            String isql =
                                    "select nvl(sum(charge),0) from lacharge where chargetype='52' and caldate>='" +
                                    StartDay + "' and caldate<='" + EndDay +
                                    "'  and agentcom='" + AgentCom +
                                    "' and riskcode='" + aSSRS.GetText(a, 2) +
                                    "'";
                            strArr[7] = aExeSQL.getOneValue(isql);
                            data[7] += Double.parseDouble(strArr[7].trim());
                            //业务费比例
                            String jsql = "select nvl(sum(chargerate),0) from lacharge where chargetype='53' and caldate>='" +
                                          StartDay + "' and caldate<='" +
                                          EndDay + "' and agentcom='" +
                                          AgentCom + "' and riskcode='" +
                                          aSSRS.GetText(a, 2) + "'";
                            strArr[8] = aExeSQL.getOneValue(jsql);
                            data[8] += Double.parseDouble(strArr[8].trim());
                            //业务费金额
                            String ksql =
                                    "select nvl(sum(charge),0) from lacharge where chargetype='53' and caldate>='" +
                                    StartDay + "' and caldate<='" + EndDay +
                                    "'  and agentcom='" + AgentCom +
                                    "' and riskcode='" + aSSRS.GetText(a, 2) +
                                    "'";
                            strArr[9] = aExeSQL.getOneValue(ksql);
                            data[9] += Double.parseDouble(strArr[9].trim());
                            //结余费比例
                            String lsql = "select nvl(sum(chargerate),0) from lacharge where chargetype='54' and caldate>='" +
                                          StartDay + "' and caldate<='" +
                                          EndDay + "'  and agentcom='" +
                                          AgentCom + "' and riskcode='" +
                                          aSSRS.GetText(a, 2) + "'";
                            strArr[10] = aExeSQL.getOneValue(lsql);
                            data[10] += Double.parseDouble(strArr[10].trim());
                            //结余费金额
                            String msql =
                                    "select nvl(sum(charge),0) from lacharge where chargetype='54' and caldate>='" +
                                    StartDay + "' and caldate<='" + EndDay +
                                    "'  and agentcom='" + AgentCom +
                                    "' and riskcode='" + aSSRS.GetText(a, 2) +
                                    "'";
                            strArr[11] = aExeSQL.getOneValue(msql);
                            data[11] += Double.parseDouble(strArr[11].trim());
                            tlistTable.add(strArr);
                            strArr = new String[12];

                        }
                    }
                }
            }
            else
            {
                String bsql =
                        "select trim(riskname) from lmriskapp where riskcode='" +
                        RiskCode + "'";
                strArr[0] = aExeSQL.getOneValue(bsql);
                //保费收入
                String csql =
                        " select nvl(sum(transmoney),0) from lacommision where  tmakeDate>='" +
                        StartDay + "' and tmakeDate<='" + EndDay +
                        "' and agentcom='" + AgentCom + "' and riskcode='" +
                        RiskCode + "'";
                strArr[1] = aExeSQL.getOneValue(csql);
                data[1] += Double.parseDouble(strArr[1].trim());
                //撤保收入
                String dsql = "select nvl(sum(transmoney),0) from lacommision where transtype='WT' and tmakeDate>='" +
                              StartDay + "' and tmakeDate<='" + EndDay +
                              "'  and agentcom='" + AgentCom +
                              "' and riskcode='" + RiskCode + "'";
                strArr[2] = aExeSQL.getOneValue(dsql);
                data[2] += Double.parseDouble(strArr[2].trim());
                //结算保费小计
                String esql =
                        " select nvl(sum(transmoney),0) from lacommision where  tmakeDate>='" +
                        StartDay + "' and tmakeDate<='" + EndDay +
                        "'  and agentcom='" + AgentCom + "' and riskcode='" +
                        RiskCode + "'";
                strArr[3] = aExeSQL.getOneValue(esql);
                data[3] += Double.parseDouble(strArr[3].trim());
                //手续费比例
                String fsql = "select nvl(sum(chargerate),0) from lacharge where chargetype='51' and caldate>='" +
                              StartDay + "' and caldate<='" + EndDay +
                              "'  and agentcom='" + AgentCom +
                              "' and riskcode='" + RiskCode + "'";
                strArr[4] = aExeSQL.getOneValue(fsql);
                data[4] += Double.parseDouble(strArr[4].trim());
                //手续费金额
                String gsql =
                        "select nvl(sum(charge),0) from lacharge where chargetype='51' and caldate>='" +
                        StartDay + "' and caldate<='" + EndDay +
                        "'  and agentcom='" + AgentCom + "' and riskcode='" +
                        RiskCode + "'";
                strArr[5] = aExeSQL.getOneValue(gsql);
                data[5] += Double.parseDouble(strArr[5].trim());
                //劳务费比例
                String hsql = "select nvl(sum(chargerate),0) from lacharge where chargetype='52' and caldate>='" +
                              StartDay + "' and caldate<='" + EndDay +
                              "'  and agentcom='" + AgentCom +
                              "' and riskcode='" + RiskCode + "'";
                strArr[6] = aExeSQL.getOneValue(hsql);
                data[6] += Double.parseDouble(strArr[6].trim());
                //劳务费金额
                String isql =
                        "select nvl(sum(charge),0) from lacharge where chargetype='52' and caldate>='" +
                        StartDay + "' and caldate<='" + EndDay +
                        "'  and agentcom='" + AgentCom + "' and riskcode='" +
                        RiskCode + "'";
                strArr[7] = aExeSQL.getOneValue(isql);
                data[7] += Double.parseDouble(strArr[7].trim());
                //业务费比例
                String jsql = "select nvl(sum(chargerate),0) from lacharge where chargetype='53' and caldate>='" +
                              StartDay + "' and caldate<='" + EndDay +
                              "'  and agentcom='" + AgentCom +
                              "' and riskcode='" + RiskCode + "' ";
                strArr[8] = aExeSQL.getOneValue(jsql);
                data[8] += Double.parseDouble(strArr[8].trim());
                //业务费金额
                String ksql =
                        "select nvl(sum(charge),0) from lacharge where chargetype='53' and caldate>='" +
                        StartDay + "' and caldate<='" + EndDay +
                        "'  and agentcom='" + AgentCom + "' and riskcode='" +
                        RiskCode + "'";
                strArr[9] = aExeSQL.getOneValue(ksql);
                data[9] += Double.parseDouble(strArr[9].trim());
                //结余费比例
                String lsql = "select nvl(sum(chargerate),0) from lacharge where chargetype='54' and caldate>='" +
                              StartDay + "' and caldate<='" + EndDay +
                              "'  and agentcom='" + AgentCom +
                              "' and riskcode='" + RiskCode + "'";
                strArr[10] = aExeSQL.getOneValue(lsql);
                data[10] += Double.parseDouble(strArr[10].trim());
                //结余费金额
                String msql =
                        "select nvl(sum(charge),0) from lacharge where chargetype='54' and caldate>='" +
                        StartDay + "' and caldate<='" + EndDay +
                        "'  and agentcom='" + AgentCom + "' and riskcode='" +
                        RiskCode + "'";
                strArr[11] = aExeSQL.getOneValue(msql);
                data[11] += Double.parseDouble(strArr[11].trim());
                tlistTable.add(strArr);
                strArr = new String[12];
            }
            //展业机构：总计
            strArr = new String[12];
            strArr[0] = "合计";
            for (int i = 1; i <= 3; i++)
            {
                strArr[i] = String.valueOf(data[i]);
            }
            strArr[4] = "";
            strArr[8] = "";
            strArr[6] = "";
            strArr[10] = "";
            strArr[5] = String.valueOf(data[5]);
            strArr[7] = String.valueOf(data[7]);
            strArr[9] = String.valueOf(data[9]);
            strArr[11] = String.valueOf(data[11]);
            tlistTable.add(strArr);
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("AgentCharge.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
            //   xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentChargeBL";
            tError.functionName = "queryDataother";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }


    public static void main(String[] args)
    {
        PolNBDepartF1PBL tPolNBDepartF1PBL = new PolNBDepartF1PBL();
    }
}