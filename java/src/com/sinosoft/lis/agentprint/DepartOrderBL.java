package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import java.sql.Connection;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class DepartOrderBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private GlobalInput mGlobalInput = new GlobalInput();
    private String StartDay = "";
    private String EndDay = "";
    private String mManageCom = "";
    private String FYC = "";
    private String tManagecomtype = "";

    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    private String qqsql = "";
    //业务处理相关变量
    /** 全局数据 */
    public DepartOrderBL()
    {
    }

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        mManageCom = (String) cInputData.get(0);
        FYC = (String) cInputData.get(1);
        StartDay = (String) cInputData.get(2);
        EndDay = (String) cInputData.get(3);
        tManagecomtype = (String) cInputData.get(4);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "DepartOrderBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String CurrentDate = PubFun.getCurrentDate(); //得到当天日期
        FDate fDate = new FDate();
        java.util.Date CurDate = fDate.getDate(CurrentDate);
        GregorianCalendar mCalendar = new GregorianCalendar();
        mCalendar.setTime(CurDate);
        int Months = mCalendar.get(Calendar.MONTH) + 1; //因为从0开始的，得到当前月
        int Years = Integer.parseInt(EndDay.substring(0, 4)); //得到当前年
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);

            int num = 1;
            int n = 0;
            String strArr[] = null;
            double date[] = new double[8];
            String Date[][] = null;
            String Date1[][] = null;
            ListTable tlistTable = new ListTable();
            tlistTable.setName("Order");
            TextTag texttag = new TextTag(); //新建一个TextTag的实例
            texttag.add("StartDay", StartDay); //输入制表时间
            texttag.add("EndDay", EndDay); //输入制表时间
            texttag.add("FYC", FYC); //入围标准
            String aasql = "select Name from ldcom where comcode = '" +
                           mManageCom + "' ";
            texttag.add("City", aExeSQL.getOneValue(aasql)); //城市

            SSRS bSSRS = new SSRS();
            if (mManageCom.equals("86"))
            {
                if (tManagecomtype == null || tManagecomtype.trim().equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "query";
                    tError.functionName = "prepareData";
                    tError.errorMessage = "机构类型 不能 为空。";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                String SQL =
                        "select trim(param) From LDParam where paramtype='managecomtype' "
                        + "and param like '86%' and param1='" +
                        this.tManagecomtype + "'";
                SSRS tSSRS = new SSRS();
                tSSRS = aExeSQL.execSQL(SQL);
                for (int t = 1; t <= tSSRS.getMaxRow(); t++)
                {
                    if (t == 1)
                    {
                        qqsql += " c.branchattr like '" + tSSRS.GetText(t, 1) +
                                "%'";
                    }
                    else
                    {
                        qqsql += " or c.branchattr like '" + tSSRS.GetText(t, 1) +
                                "%'";
                    }
                }
                nsql = "select a.name,b.shortname,a.branchattr,a.branchmanagername,SUM(c.standprem),sum(c.calcount) "
                       + "from ldcom b,LABranchGroup a,LACommision c "
                       + "where BranchLevel = '02' and substr(c.branchattr,0,15)= trim(a.BranchAttr) and (TRIM(A.STATE)<>'1' OR A.STATE IS NULL)" +
                       " and trim(b.comcode)=substr(c.managecom,0,4) and c.SignDate>='" +
                       StartDay + "' and c.SignDate<='" + Years
                       +
                       "-12-31' and payintv<>0 and (paycount=0 or paycount =1) and (" +
                       qqsql + ") and c.makepoldate>='"
                       + StartDay + "' and c.makepoldate <='" + EndDay
                       + "' group by a.name,a.branchattr,a.branchmanagername,b.shortname having sum(c.Standprem)>'" +
                       FYC + "'";
            }
            else
            {
                nsql = "select a.name,b.shortname,a.branchattr,a.branchmanagername,SUM(c.standprem),sum(c.calcount)"
                       + "from ldcom b,LABranchGroup a,LACommision c " +
                       "where BranchLevel = '02' and substr(c.branchattr,0,15)= trim(a.BranchAttr) and (TRIM(A.STATE)<>'1' OR A.STATE IS NULL)" +
                       " and trim(b.comcode)=substr(c.managecom,0,4) and c.SignDate>='" +
                       StartDay + "' and c.SignDate<='" + Years
                       +
                       "-12-31' and payintv<>0 and (paycount=0 or paycount =1) and c.branchattr like '" +
                       mManageCom + "%' and c.makepoldate>='"
                       + StartDay + "' and c.makepoldate <='" + EndDay
                       + "' group by a.name,a.branchattr,a.branchmanagername,b.shortname having sum(c.Standprem)>'" +
                       FYC + "'";
            }
            bSSRS = aExeSQL.execSQL(nsql);
            Date = new String[bSSRS.getMaxRow()][8];
            n = bSSRS.getMaxRow();
            String SQL = "";
            for (int b = 1; b <= bSSRS.getMaxRow(); b++)
            {
                Date[b - 1][1] = bSSRS.GetText(b, 1);
                Date[b - 1][2] = bSSRS.GetText(b, 2);
                Date[b - 1][3] = bSSRS.GetText(b, 3);
                Date[b - 1][4] = bSSRS.GetText(b, 4);
                Date[b - 1][5] = bSSRS.GetText(b, 5);
                Date[b - 1][6] = bSSRS.GetText(b, 6);
                SQL = "select sum(FYC)/code3 from lacommision a,ldcoderela b where trim(b.code1)=substr(managecom,0,4)"
                      + "and b.relatype='comtoareatype' and  branchattr like '" +
                      Date[b - 1][3]
                      + "%' and branchtype='1' and SignDate>='"
                      + StartDay + "' and SignDate<='" + Years +
                      "-12-31' and (paycount=0 or paycount =1) "
                      + " and makepoldate>='" + StartDay +
                      "' and makepoldate <='" + EndDay +
                      "' and CommDire='1' group by code3";

                Date[b - 1][7] = aExeSQL.getOneValue(SQL);
            }

            LISComparator tLISComparator = new LISComparator();
            tLISComparator.setNum(7);
            Arrays.sort(Date, tLISComparator);
            for (int i = 1; i <= n; i++)
            {
                strArr = new String[8];
                strArr[0] = String.valueOf(i);
                for (int j = 1; j <= 7; j++)
                {
                    strArr[j] = Date[i - 1][j];
                }
                tlistTable.add(strArr);
            }

            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("DepartOrder.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
//      xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "DepartOrderBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    public static void main(String[] args)
    {
    }
}