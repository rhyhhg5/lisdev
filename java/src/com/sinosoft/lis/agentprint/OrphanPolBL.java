package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */
import java.text.SimpleDateFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;


public class OrphanPolBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //取得的时间
    private String Polno = null;
    private String AscriptionCount = null;

    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public OrphanPolBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        Polno = ((String) cInputData.getObjectByObjectName("String", 0));
        AscriptionCount = ((String) cInputData.getObjectByObjectName("String",
                1));
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }


    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LCPolBillF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        SSRS tSSRS = new SSRS();
        msql = "select agentold,agentnew from LAAscription where polno='"
               + this.Polno + "' and AscriptionCount='" + this.AscriptionCount +
               "'";
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(msql);
        TextTag texttag = new TextTag();
        texttag.add("AgentOld", tSSRS.GetText(1, 1));
        texttag.add("PolNo", Polno);
        texttag.add("AgentNew", tSSRS.GetText(1, 2));

        String cSQL = "select managecom,AppntName from lcpol where polno='" +
                      Polno + "'";
        ExeSQL cExeSQL = new ExeSQL();
        SSRS zSSRS = new SSRS();
        zSSRS = cExeSQL.execSQL(cSQL);

        texttag.add("AppntName", zSSRS.GetText(1, 2));

        nsql = "select phone,name from ldcom where comcode='" +
               zSSRS.GetText(1, 1) + "' ";
        ExeSQL aExeSQL = new ExeSQL();
        SSRS bSSRS = new SSRS();
        bSSRS = aExeSQL.execSQL(nsql);
        texttag.add("Phone", bSSRS.GetText(1, 1));
        texttag.add("ManageComName", bSSRS.GetText(1, 2));

        sql =
                "select a.name,b.name from laagent a,laagent b where a.agentcode='" +
                tSSRS.GetText(1, 1)
                + "' and b.agentcode ='" + tSSRS.GetText(1, 2) + "'";

        SSRS aSSRS = new SSRS();
        ExeSQL bExeSQL = new ExeSQL();
        aSSRS = bExeSQL.execSQL(sql);
        texttag.add("AgentOldName", aSSRS.GetText(1, 1));
        texttag.add("AgentNewName", aSSRS.GetText(1, 2));

        SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日");
        texttag.add("Today", df.format(new java.util.Date()));

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("OrphanPol.vts", "printer"); //最好紧接着就初始化xml文档
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
        //xmlexport.createDocument("OrphanPol.vts","printer");//最好紧接着就初始化xml文档

        mResult.addElement(xmlexport);
        return true;

    }

    public static void main(String[] args)
    {
        String Polno = "86110020030210018031";
        String AscriptionCount = "1";
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "8611";
        tG.ComCode = "8611";
        tG.Operator = "001";
        VData tVData = new VData();
        tVData.addElement(Polno);
        tVData.addElement(AscriptionCount);
        tVData.addElement(tG);
        OrphanPolBL tUI = new OrphanPolBL();
        tUI.submitData(tVData, "");
    }
}