package com.sinosoft.lis.agentprint;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class ChannelReport
{
    public GlobalInput mGI = new GlobalInput();
    private String mStartDate = "";
    private String mEndDate = "";
    private String mBankType1 = "";
    private String mBankType2 = "";
    public VData mResult = new VData();
    public CErrors mErrors = new CErrors();

    public ChannelReport()
    {
    }

    public static void main(String[] args)
    {
        ChannelReport ChannelReport1 = new ChannelReport();
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86110000";
        tGI.ManageCom = "86";
        tGI.Operator = "aa";
        VData tInputData = new VData();
        tInputData.clear();
        tInputData.addElement(tGI);
        tInputData.add(0, "2003-12-1");
        tInputData.add(1, "2003-12-10");
        tInputData.add(2, "02");
        tInputData.add(3, "03");
        if (!ChannelReport1.getInputData(tInputData))
        {
            System.out.println("-----getinputdata failed!------");
        }
        if (!ChannelReport1.prepareData())
        {
            System.out.println("-------prepareData failed!------");
        }

    }

    public boolean getInputData(VData cInputData)
    {
        //全局变量
        try
        {
            mStartDate = (String) cInputData.getObjectByObjectName("String", 0);
            mEndDate = (String) cInputData.getObjectByObjectName("String", 1);
            mBankType1 = (String) cInputData.getObjectByObjectName("String", 2);
            mBankType2 = (String) cInputData.getObjectByObjectName("String", 3);
            mGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput",
                    0);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("getInputData", ex.toString());
            return false;
        }
        System.out.println("---StartDate : " + this.mStartDate);
        System.out.println("---EndDate : " + this.mEndDate);
        System.out.println("---BankType1 : " + this.mBankType1);
        System.out.println("---BankType2 : " + this.mBankType2);
        return true;
    }

    public boolean prepareData()
    {
        ListTable tListTable = new ListTable();
        tListTable.setName("ChannelBusi");

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        String tStaticDate = "";
        tStaticDate = this.mStartDate.trim() + " 至 " + this.mEndDate.trim();
        texttag.add("StaticDate", tStaticDate); //输入制表时间
        PubFun tPF = new PubFun();

        //取本年度的第一天
        String tYearStart = this.mStartDate.trim().substring(0, 4) + "-1-1";
        System.out.println("---YearStartDate : " + tYearStart);

        //取录入时间的年月代码
        String pattern = "yyyy-mm-dd";
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        Date tDate = new Date();
        Date tDate1 = new Date();
        try
        {
            tDate = df.parse(this.mStartDate);
            tDate1 = df.parse(this.mEndDate);
        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ChannelReport";
            tError.functionName = "PrepareDate";
            tError.errorMessage = e.toString();
            this.mErrors.addOneError(tError);
        }
        String tStartMon = df.format(tDate).trim().substring(0, 4).trim()
                           + df.format(tDate).trim().substring(5, 7).trim();
        String tEndMon = df.format(tDate1).trim().substring(0, 4).trim()
                         + df.format(tDate1).trim().substring(5, 7).trim();

        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);

            //查询出各个银行
            SSRS aSSRS = new SSRS();
            String Sql1 = "select agentcom,name from Lacom "
                          + "where trim(banktype) = '" + this.mBankType1.trim() +
                          "'"
                          + " and trim(managecom) like '" +
                          this.mGI.ManageCom.trim() + "%'";
            int tBankCount = 0;

            aSSRS = aExeSQL.execSQL(Sql1);
            tBankCount = aSSRS.getMaxRow();
            System.out.println("---tBankCount = " + tBankCount);
            if (tBankCount == 0)
            {
                dealError("prepareData", "银行信息查询失败！");
                return false;
            }

            String strArr[][] = null;
            String eachList[] = null;
            String[] tBank = new String[tBankCount];
            String[] tBankName = new String[tBankCount];

            float tSum = 0;
            float tSumPlanValue = 0;
            float tSumYearPlan = 0;
            float tTransMoneySum = 0;

            //实收保费合计
            float tAllTransMoneySum = 0;
            //达成率标准和
            float tAllPlanValueSum = 0;
            //保单件数和
            float tAllPolCount = 0;
            //活动网点和
            int tAllActiveNetCount = 0;
            //客户经理和
            int tAllManagerCount = 0;
            //网点总数和
            int tAllNetCountSum = 0;
            //累计实收保费和
            float tAddSum = 0;
            //当期标准保费和
            float tAllStandPremSum = 0;
            //累计标保和
            float tAddStandSum = 0;
            //累计完成进度
            float tAllSumYearPlan = 0;

            for (int l = 1; l <= tBankCount; l++)
            {
                tBank[l - 1] = new String();
                tBank[l - 1] = aSSRS.GetText(l, 1);
                tBankName[l - 1] = new String();
                tBankName[l - 1] = aSSRS.GetText(l, 2);
                System.out.println("------Bank: " + tBank[l - 1]);
                System.out.println("------BankName: " + tBankName[l - 1]);

                SSRS bSSRS = new SSRS();
                Sql1 = "select agentcom,name from Lacom "
                       + "where trim(banktype) = '" + this.mBankType2.trim() +
                       "'"
                       + " and trim(managecom) like '" +
                       this.mGI.ManageCom.trim() + "%'"
                       + " and trim(agentcom) like '" + tBank[l - 1].trim() +
                       "%'";

                int tChannelCount = 0;
                bSSRS = aExeSQL.execSQL(Sql1);
                tChannelCount = bSSRS.getMaxRow();
                if (tChannelCount == 0)
                {
                    continue;
                }

                strArr = new String[tChannelCount][17];
                tSum = 0;
                tSumPlanValue = 0;
                tTransMoneySum = 0;
                tSumYearPlan = 0;
                for (int i = 1; i <= tChannelCount; i++)
                {
                    String[] tChannel = new String[tChannelCount];
                    String[] tChannelName = new String[tChannelCount];

                    System.out.println("--------" + i + "---------");

                    tChannel[i - 1] = new String();
                    tChannel[i - 1] = bSSRS.GetText(i, 1);
                    tChannelName[i - 1] = new String();
                    tChannelName[i - 1] = bSSRS.GetText(i, 2);
                    System.out.println("------Channel: " + tChannel[i - 1]);
                    System.out.println("------ChannelName: " + tChannelName[i -
                                       1]);
                    if (i == 1)
                    {
                        strArr[i - 1][0] = tBankName[l - 1];
                    }

                    strArr[i - 1][1] = tChannelName[i - 1];

                    //统计实收保费,件数，标保
                    String Sql2 =
                            "select nvl(sum(transmoney),0),nvl(sum(calcount),0),nvl(sum(standprem),0) from lacommision "
                            + " where tmakedate >= '" + this.mStartDate.trim()
                            + "' and tmakedate <= '" + this.mEndDate.trim() +
                            "'"
                            + " and trim(agentcom) like '" + tChannel[i -
                            1].trim() + "%'";
                    SSRS cSSRS = new SSRS();
                    cSSRS = aExeSQL.execSQL(Sql2);
                    strArr[i - 1][2] = cSSRS.GetText(1, 1);
                    strArr[i - 1][5] = cSSRS.GetText(1, 2);
                    System.out.println("---" + tChannelName[i - 1].trim() +
                                       "--保单件数 ＝ " +
                                       strArr[i - 1][5].trim());
                    strArr[i - 1][11] = cSSRS.GetText(1, 3);

                    //累加实收保费
                    tSum += Float.parseFloat(strArr[i - 1][2].trim());

                    //达成率
                    Sql2 = "select nvl(sum(planvalue),0) from laplan "
                           + "where planperiod >= '" + tStartMon.trim()
                           + "' and planperiod <= '" + tEndMon.trim() + "'"
                           + " and plantype = '2' and planperiodunit = '1'"
                           + " and planobject = '" + tChannel[i - 1].trim() +
                           "'";

                    float tPlanValue = 0;
                    tPlanValue = Float.parseFloat(aExeSQL.getOneValue(Sql2).
                                                  trim());
                    if (tPlanValue == 0)
                    {
                        strArr[i - 1][4] = "0";
                    }
                    else
                    {
                        strArr[i -
                                1][4] = Float.toString(Float.parseFloat(strArr[
                                i - 1][2].trim()) /
                                tPlanValue).trim();
                    }
                    tSumPlanValue += tPlanValue;

                    //统计活动网点数
                    Sql2 =
                            "select nvl(count(distinct agentcom),0) from lacommision "
                            + "where tmakedate >= '" + this.mStartDate.trim()
                            + "' and tmakedate <= '" + this.mEndDate.trim()
                            + "' and transmoney <> 0"
                            + " and trim(agentcom) like '" + tChannel[i -
                            1].trim() + "%'";
                    strArr[i - 1][6] = aExeSQL.getOneValue(Sql2).trim();

                    //统计客户经理数
                    Sql2 =
                            "select nvl(count(distinct agentcode),0) from lacomtoagent "
                            +
                            " where agentcom in ( select distinct agentcom from lacommision "
                            + " where trim(agentcom) like '" + tChannel[i -
                            1].trim()
                            + "%' and tmakedate >= '" + this.mStartDate.trim()
                            + "' and tmakedate <= '" + this.mEndDate.trim()
                            + "' )";
                    strArr[i - 1][7] = aExeSQL.getOneValue(Sql2).trim();

                    //统计网点总数
                    Sql2 = "select nvl(count(distinct agentcom),0) from lacom "
                           +
                           "where agentcom in ( select distinct agentcom from lacommision "
                           + "where tmakedate >= '" + this.mStartDate.trim()
                           + "' and tmakedate <= '" + this.mEndDate.trim()
                           + "' and transmoney <> 0)"
                           + " and trim(agentcom) like '" + tChannel[i -
                           1].trim() + "%'";
                    strArr[i - 1][8] = aExeSQL.getOneValue(Sql2).trim();

                    //累计实收保费,标准保费
                    Sql2 =
                            "select nvl(sum(transmoney),0),nvl(sum(standprem),0) from lacommision "
                            + "where tmakedate >= '" + tYearStart.trim()
                            + "' and tmakedate <= '" + this.mEndDate.trim() +
                            "'"
                            + " and trim(agentcom) like '" + tChannel[i -
                            1].trim() + "%'";
                    SSRS zSSRS = new SSRS();
                    zSSRS = aExeSQL.execSQL(Sql2);
                    strArr[i - 1][9] = zSSRS.GetText(1, 1);
                    tTransMoneySum += Float.parseFloat(strArr[i - 1][9]);
                    strArr[i - 1][12] = zSSRS.GetText(1, 2);

                    //累计完成进度
                    Sql2 = "select nvl(planvalue,0) from laplan "
                           + "where plantype = '2' and planperiodunit = '12'"
                           + " and planperiod = '" +
                           this.mStartDate.trim().substring(0, 4).trim() + "'"
                           + " and planobject = '" + tChannel[i - 1].trim() +
                           "'";
                    String tYearPlan = "";
                    tYearPlan = aExeSQL.getOneValue(Sql2).trim();
                    if (tYearPlan.trim().equals("") || tYearPlan == null)
                    {
                        strArr[i - 1][10] = "0";
                    }
                    else
                    {
                        strArr[i -
                                1][10] = Float.toString(Float.parseFloat(strArr[
                                i - 1][9].trim())
                                / Float.parseFloat(tYearPlan.trim()));
                        tSumYearPlan += Float.parseFloat(tYearPlan.trim());
                    }

                    //网均产能
                    if (strArr[i - 1][8].trim().equalsIgnoreCase("0"))
                    {
                        strArr[i - 1][13] = "0";
                    }
                    else
                    {
                        strArr[i -
                                1][13] = Float.toString(Float.parseFloat(strArr[
                                i - 1][2].trim()) /
                                Float.parseFloat(strArr[i - 1][8].trim()));
                    }

                    //件均保费
                    if (strArr[i - 1][5].trim().equalsIgnoreCase("0"))
                    {
                        strArr[i - 1][14] = "0";
                    }
                    else
                    {
                        strArr[i -
                                1][14] = Float.toString(Float.parseFloat(strArr[
                                i - 1][2].trim()) /
                                Float.parseFloat(strArr[i - 1][5].trim()));
                    }

                    //人均保费
                    if (strArr[i - 1][7].trim().equalsIgnoreCase("0"))
                    {
                        strArr[i - 1][15] = "0";
                    }
                    else
                    {
                        strArr[i -
                                1][15] = Float.toString(Float.parseFloat(strArr[
                                i - 1][2].trim()) /
                                Float.parseFloat(strArr[i - 1][7].trim()));
                    }
                    //网点活动率
                    if (strArr[i - 1][8].trim().equalsIgnoreCase("0"))
                    {
                        strArr[i - 1][16] = "0";
                    }
                    else
                    {
                        strArr[i -
                                1][16] = Float.toString(Float.parseFloat(strArr[
                                i - 1][6].trim()) /
                                Float.parseFloat(strArr[i - 1][8].trim()));
                    }
                }

                //计算业务占比
                if (tSum == 0)
                {
                    for (int j = 0; j < tChannelCount; j++)
                    {
                        strArr[j][3] = "0";
                    }
                }
                else
                {
                    for (int j = 0; j < tChannelCount; j++)
                    {
                        strArr[j][3] = Float.toString(Float.parseFloat(strArr[j][
                                2].trim()) /
                                tSum);
                    }
                }

                for (int m = 0; m < tChannelCount; m++)
                {
                    eachList = new String[17];
                    for (int n = 0; n < 17; n++)
                    {
                        eachList[n] = strArr[m][n];
                    }
                    tListTable.add(eachList);
                }

                //小计
                eachList = new String[17];
                eachList[0] = tBankName[l - 1].trim() + "小计";
                eachList[2] = Float.toString(tSum).trim();
                eachList[3] = "0";

                if (tSumYearPlan == 0)
                {
                    eachList[4] = "0";
                }
                else
                {
                    eachList[4] = Float.toString(tSum / tSumPlanValue).trim();
                }

                float tPolCount = 0;
                int tAllNetCount = 0;
                int tManagerCount = 0;
                int tActiveNetCount = 0;
                float tSumTransMoney = 0;
                float tSumStandPrem = 0;
                float tYearStandPrem = 0;
                for (int k = 0; k < tChannelCount; k++)
                {
                    tPolCount += Float.parseFloat(strArr[k][5].trim());
                    tAllNetCount += Integer.parseInt(strArr[k][6].trim());
                    tManagerCount += Integer.parseInt(strArr[k][7].trim());
                    tActiveNetCount += Integer.parseInt(strArr[k][8].trim());
                    tSumTransMoney += Float.parseFloat(strArr[k][9].trim());
                    tSumStandPrem += Float.parseFloat(strArr[k][11].trim());
                    tYearStandPrem += Float.parseFloat(strArr[k][12].trim());
                }
                eachList[5] = Float.toString(tPolCount).trim();
                eachList[6] = Integer.toString(tActiveNetCount).trim();
                eachList[7] = Integer.toString(tManagerCount).trim();
                eachList[8] = Integer.toString(tAllNetCount).trim();
                eachList[9] = Float.toString(tSumTransMoney).trim();

                if (tSumYearPlan == 0)
                {
                    eachList[10] = "0";
                }
                else
                {
                    eachList[10] = Float.toString(tTransMoneySum / tSumYearPlan).
                                   trim();
                }

                eachList[11] = Float.toString(tSumStandPrem).trim();
                eachList[12] = Float.toString(tYearStandPrem).trim();
                if (tAllNetCount == 0)
                {
                    eachList[13] = "0";
                }
                else
                {
                    eachList[13] = Float.toString(tSum / tAllNetCount).trim();
                }

                if (tPolCount == 0)
                {
                    eachList[14] = "0";
                }
                else
                {
                    eachList[14] = Float.toString(tSum / tPolCount).trim();
                }

                if (tManagerCount == 0)
                {
                    eachList[15] = "0";
                }
                else
                {
                    eachList[15] = Float.toString(tSum / tManagerCount).trim();
                }

                if (tAllNetCount == 0)
                {
                    eachList[16] = "0";
                }
                else
                {
                    eachList[16] = Float.toString(tActiveNetCount /
                                                  tAllNetCount).trim();
                }

                tListTable.add(eachList);

                //小计求和
                tAllTransMoneySum += tSum;
                tAllPlanValueSum += tSumPlanValue;
                tAllPolCount += tPolCount;
                tAllActiveNetCount += tActiveNetCount;
                tAllManagerCount += tManagerCount;
                tAllNetCountSum += tAllNetCount;
                tAddSum += tSumTransMoney;
                tAllStandPremSum += tSumStandPrem;
                tAddStandSum += tYearStandPrem;
                tAllSumYearPlan += tSumYearPlan;
            }

            //合计
            eachList = new String[17];
            eachList[0] = "合计";
            eachList[2] = Float.toString(tAllTransMoneySum).trim();
            eachList[3] = "0";

            if (tAllPlanValueSum == 0)
            {
                eachList[4] = "0";
            }
            else
            {
                eachList[4] = Float.toString(tAllTransMoneySum /
                                             tAllPlanValueSum).trim();
            }

            eachList[5] = Float.toString(tAllPolCount).trim();
            eachList[6] = Integer.toString(tAllActiveNetCount).trim();
            eachList[7] = Integer.toString(tAllManagerCount).trim();
            eachList[8] = Integer.toString(tAllNetCountSum).trim();
            eachList[9] = Float.toString(tAddSum).trim();

            if (tAllSumYearPlan == 0)
            {
                eachList[10] = "0";
            }
            else
            {
                eachList[10] = Float.toString(tAddSum / tAllSumYearPlan).trim();
            }

            eachList[11] = Float.toString(tAllStandPremSum).trim();
            eachList[12] = Float.toString(tAddStandSum).trim();

            if (tAllNetCountSum == 0)
            {
                eachList[13] = "0";
            }
            else
            {
                eachList[13] = Float.toString(tAllTransMoneySum /
                                              tAllNetCountSum);
            }

            if (tAllPolCount == 0)
            {
                eachList[14] = "0";
            }
            else
            {
                eachList[14] = Float.toString(tAllTransMoneySum / tAllPolCount).
                               trim();
            }

            if (tAllManagerCount == 0)
            {
                eachList[15] = "0";
            }
            else
            {
                eachList[15] = Float.toString(tAllTransMoneySum /
                                              tAllManagerCount).trim();
            }

            if (tAllNetCountSum == 0)
            {
                eachList[16] = "0";
            }
            else
            {
                eachList[16] = Float.toString(tAllActiveNetCount /
                                              tAllNetCountSum).trim();
            }

            tListTable.add(eachList);

            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("ChannelBusiReport.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tListTable, eachList);
//    xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
            this.mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ChannelReport";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("------prepareData end-----");

        return true;
    }


    private void dealError(String tFuncName, String tErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "ChannelReport";
        cError.functionName = tFuncName;
        cError.errorMessage = tErrMsg;
        this.mErrors.addOneError(cError);
    }

    public VData getResult()
    {
        return this.mResult;
    }

}