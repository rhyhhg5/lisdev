package com.sinosoft.lis.agentprint;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author
 * @version 1.0
 */

public class LaChargeUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String StartDay = "";
    private String EndDay = "";
    private String AgentCom = "";
    private String BankType = "";
    private String ChargeType = "";
    private String RiskCode = "";

    public LaChargeUI()
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        // 进行业务处理
        if (!dealData())
        {
            return false;
        }

        // 准备传往后台的数据
        VData vData = new VData();

        if (!prepareOutputData(vData))
        {
            return false;
        }

        LaChargeBL tLaChargeBL = new LaChargeBL();
        System.out.println("Start LaChargeUI Submit ...");

        if (!tLaChargeBL.submitData(vData, cOperate))
        {
            if (tLaChargeBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tLaChargeBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "LaChargeBL发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            mResult = tLaChargeBL.getResult();
            return true;
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.addElement(StartDay);
            vData.addElement(EndDay);
            vData.addElement(AgentCom);
            vData.addElement(BankType);
            vData.addElement(ChargeType);
            vData.addElement(RiskCode);
            vData.add(mGlobalInput);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        StartDay = (String) cInputData.get(0);
        EndDay = (String) cInputData.get(1);
        AgentCom = (String) cInputData.get(2);
        BankType = (String) cInputData.get(3);
        ChargeType = (String) cInputData.get(4);
        RiskCode = (String) cInputData.get(5);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 6));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "PolAssessBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    public static void main(String[] args)
    {
        String StartDay = "20030601";
        String EndDay = "20031225";
        String AgentCom = "180011";
        String BankType = "02";
        String ChargeType = "22";
        String RiskCode = "312201";
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        VData tVData = new VData();
        tVData.addElement(StartDay);
        tVData.addElement(EndDay);
        tVData.addElement(AgentCom);
        tVData.addElement(BankType);
        tVData.addElement(ChargeType);
        tVData.addElement(RiskCode);
        tVData.addElement(tG);
        LaChargeUI UI = new LaChargeUI();
        if (!UI.submitData(tVData, "PRINT"))
        {
            if (UI.mErrors.needDealError())
            {
                System.out.println(UI.mErrors.getFirstError());
            }
            else
            {
                System.out.println("UI发生错误，但是没有提供详细的出错信息");
            }
        }
        else
        {
            VData vData = UI.getResult();
            System.out.println("已经接收了数据!!!");
        }
    }
}