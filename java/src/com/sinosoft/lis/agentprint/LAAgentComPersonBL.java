package com.sinosoft.lis.agentprint;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.*;
import java.text.DecimalFormat;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class LAAgentComPersonBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    SSRS tSSRS=new SSRS();
    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();

    public LAAgentComPersonBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {

        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LAAgentComPersonBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 3][15];

       mToExcel[0][2] = "机构增员奖报表（此表为动态数据，最终数据以文件规定日期及指标为准）" ;
       
       mToExcel[1][0]="分公司机构编码 ";        
       mToExcel[1][1]="分公司机构名称";
       mToExcel[1][2]="新增有效新人";
       mToExcel[1][3]="留存人数";
       mToExcel[1][4]="新人留存率（%）";
       mToExcel[1][5]="新人人均产能达标人数 ";        
//       mToExcel[1][6]="奖励标准";
//       mToExcel[1][7]="奖励金额";
              
        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
        	mToExcel[row +1][0]=tSSRS.GetText(row, 1);
        	mToExcel[row +1][1]=tSSRS.GetText(row, 2);
        	mToExcel[row +1][2]=tSSRS.GetText(row, 3);
        	mToExcel[row +1][3]=getRetention(tSSRS.GetText(row, 1));
        	mToExcel[row +1][4]=getRetentionRate(mToExcel[row +1][3],mToExcel[row +1][2]);
        	mToExcel[row +1][5]=getStandardCount(tSSRS.GetText(row, 1));
//        	mToExcel[row +1][6]=getStandard(mToExcel[row +1][3]);  
//        	mToExcel[row +1][7]=getSMoney(mToExcel[row +1][5],mToExcel[row +1][6]);        	
        }

        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data
                          .getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentComPersonBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        mSql = (String) tf.getValueByName("querySql");
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        System.out.println("msql:"+mSql);
        System.out.println("path:"+mOutXmlPath);
        if(mSql == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAAgentComPersonBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }
   
    
    
    private String getRetention(String pmManageCom)
    {
        String tSQL = "";
        String tRtValue = "";
        double tDbValue = 0.00;
       tSQL = " select count(agentcode) from laagent "
             +" where agentstate='01' and managecom like '"+pmManageCom+"%' "
             +" and branchtype='1' and branchtype2='01' "
             +" and employdate between '2011-12-01' and '2012-03-31' "
             +" and (outworkdate is null or outworkdate>'2012-04-30') "
             ;
       try{
           tDbValue = execQuery(tSQL);
       }
       catch(Exception ex)
       {
           System.out.println("getRetention 出错！");
       }
       int trs = (int)(tDbValue);
       tRtValue = String.valueOf(trs);
       return tRtValue;
  }
    
  
    private String getRetentionRate(String pmPerson,String pmStand)
    {
        String tSQL = "";
        String tRtValue = "";
        double tDbValue = 0.00;

        if(0 == Integer.parseInt(pmStand))
        {
            return "0.0";
        }

        tSQL = "select DECIMAL(DECIMAL(" + pmPerson + "*100,12,2) / DECIMAL(" +
               pmStand + ",12,2),12,2) from dual";

        try{
            tDbValue = execQuery(tSQL);
        }catch(Exception ex)
        {
            System.out.println("getRetentionRate 出错！");
        }
        tRtValue = String.valueOf(tDbValue);
        return tRtValue;
    }
    
    
    private String getStandardCount(String pmManageCom)
    {
        String tSQL = "";
        String tRtValue = "";
        double tDbValue = 0.00;
       tSQL ="select count(distinct agentcode) from ("
    	     + "select distinct agentcode  from lacommision  a where a.wageno between '201112' and '201204'"
    	     +" and exists (select '1' from laagent "
             +" where  agentstate='01' and managecom like '"+pmManageCom+"%' "
             +" and branchtype='1' and branchtype2='01' and agentcode=a.agentcode "
             +" and employdate between '2011-12-01' and '2012-03-31' "
             +" and (outworkdate is null or outworkdate>'2012-04-30') )"
             +" group by agentcode having sum(transmoney)>=20000 "
             +" union "
             +"select distinct agentcode  from lacommision  a where a.wageno between '201112' and '201204'"
    	     +" and exists (select '1' from laagent "
             +" where  agentstate='01' and managecom like '"+pmManageCom+"%' "
             +" and branchtype='1' and branchtype2='01' and agentcode=a.agentcode "
             +" and employdate between '2011-12-01' and '2012-03-31' "
             +" and (outworkdate is null or outworkdate>'2012-04-30') )"
             +" and payintv=12 "
             +" group by agentcode having sum(transmoney)>=6000 "
             +" ) as ff "
             ;
       try{
           tDbValue = execQuery(tSQL);
       }catch(Exception ex)
       {
           System.out.println("getStandardCount 出错！");
       }
       int trs = (int)(tDbValue);
       tRtValue = String.valueOf(trs);
       return tRtValue;
  }
    
    private String getStandard(String pmPerson)
    {
        String tSQL = "";
       String tRtValue="";
       double  pmPerson1= Double.parseDouble(pmPerson);
       tSQL = " select case when "+pmPerson1+">=80 and "+pmPerson1+"<=149 then '300'  "
       +" when "+pmPerson1+">=150 and "+pmPerson1+"<=299 then '320'  "
       +" when "+pmPerson1+">=300 and "+pmPerson1+"<=449 then '340' "
       +" when "+pmPerson1+">=450   then '360' "
       +" else '0' end from dual where 1=1  "  ;
       SSRS tSSRS = new SSRS();
       ExeSQL tExeSQL = new ExeSQL();
       tSSRS = tExeSQL.execSQL(tSQL);
       if(tSSRS.getMaxRow()==0)
        {
          tRtValue="0";
        }
        else
       tRtValue=tSSRS.GetText(1, 1);
       return tRtValue;
  }
    
  
    private String getSMoney(String pmPerson,String pmStand)
    {
        String tSQL = "";
        String tRtValue = "";
        double tDbValue = 0.00;
   

        tSQL = "select DECIMAL(DECIMAL(" + pmPerson + "*100,12,2) * DECIMAL(" +
        pmStand + ",12,2),12,2) from dual";

        try{
            tDbValue = execQuery(tSQL);
        }catch(Exception ex)
        {
            System.out.println("getRetentionRate 出错！");
        }
        tRtValue = String.valueOf(tDbValue);
        return tRtValue;
    }
    
    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return double
     */
    private double execQuery(String sql)
    {
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        System.out.println(sql);

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null)return 0.00;
            st = conn.prepareStatement(sql);
            if (st == null)return 0.00;
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getDouble(1);
            }
            return 0.00;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
              if (!conn.isClosed()) {
                  conn.close();
              }
              try {
                  st.close();
                  rs.close();
              } catch (Exception ex2) {
                  ex2.printStackTrace();
              }
              st = null;
              rs = null;
              conn = null;
            } catch (Exception e) {}

        }
   }
    public static void main(String[] args)
    {
    System.out.println("11111111");
    }
}
