package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PolNBDDayF1PBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    //取得的时间
    private String mDay = "";
    private String mManageCom = "";
    double date3[] = new double[18]; //定义总计的数组
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    public PolNBDDayF1PBL()
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mDay = (String) cInputData.get(0);
        mManageCom = (String) cInputData.get(1);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "PolNBBillF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String strArr[] = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("NBDDay");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCode(mManageCom);
        tLDCodeDB.setCodeType("station");
        tLDCodeDB.getInfo();
        texttag.add("Name", tLDCodeDB.getCodeName()); //输入制表机构
        texttag.add("Day", mDay); //输入制表时间

        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);

            //查询出各个区级
            String mmDay[] = PubFun1.calFLDate(mDay);
            SSRS aSSRS = new SSRS();
            msql = "select AgentGroup,name from LABranchGroup where EndFlag<>'Y' and BranchType = '1' and BranchLevel = '03' and (state<>'1' or state is null) and ManageCom like '" +
                   mManageCom + "%'";
            aSSRS = aExeSQL.execSQL(msql);
            int heigh = 0;
            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                double date2[] = new double[15]; //定义区的数组
                for (int c = 1; c <= aSSRS.getMaxCol(); c++)
                {
                    if (c == 1)
                    {
                        SSRS bSSRS = new SSRS();
                        //查询出各个区以下的部级
                        nsql = "select name,branchattr from LABranchGroup where EndFlag<>'Y' and (state<>'1' or state is null) and UpBranch = '" +
                               aSSRS.GetText(a, 1) + "' order by agentgroup";
                        bSSRS = aExeSQL.execSQL(nsql);
                        for (int b = 1; b <= bSSRS.getMaxRow(); b++)
                        {
                            for (int d = 1; d <= bSSRS.getMaxCol(); d++)
                            {
                                if (d == 1)
                                {
                                    //展业机构：部一级名
                                    strArr = new String[15];
                                    strArr[0] = bSSRS.GetText(b, 1);
                                }
                                if (d == 2)
                                {
                                    SSRS zSSRS = new SSRS();
                                    //查询部以下组级
                                    //计算每个部门下的每个组人力资源,并加和
                                    String asql =
                                            "select count(*) from LAAgent where EmployDate < '" +
                                            mmDay[0] + "' and agentstate in ('01','02') and branchcode in (select agentgroup from labranchgroup where branchattr like '" +
                                            bSSRS.GetText(b, 2) + "%')";
                                    strArr[1] = aExeSQL.getOneValue(asql);
                                    //日预收规模保费
                                    String bsql =
                                            "select NVL(sum(prem),0) from ljtempfee_lmriskapp where MakeDate ='" +
                                            mDay + "' and agentgroup in (select agentgroup from labranchgroup where branchattr like '" +
                                            bSSRS.GetText(b, 2) + "%')";
                                    strArr[2] = aExeSQL.getOneValue(bsql);
                                    //月预收规模保费
                                    String csql =
                                            "select NVL(sum(prem),0) from ljtempfee_lmriskapp where MakeDate >='" +
                                            mmDay[0] + "' and MakeDate <='" +
                                            mDay + "' and agentgroup in (select agentgroup from labranchgroup where branchattr like '" +
                                            bSSRS.GetText(b, 2) + "%')";
                                    strArr[3] = aExeSQL.getOneValue(csql);
                                    //日规模承保
                                    String dsql = "select NVL(sum(SumActuPayMoney),0) from ljapayperson_lmriskapp2 where makedate ='" +
                                                  mDay + "' and agentgroup in (select agentgroup from labranchgroup where branchattr like '" +
                                                  bSSRS.GetText(b, 2) + "%')";
                                    strArr[4] = aExeSQL.getOneValue(dsql);
                                    //月规模保费
                                    String fsql = "select NVL(sum(SumActuPayMoney),0) from ljapayperson_lmriskapp2 where makedate >='" +
                                                  mmDay[0] +
                                                  "' and makedate <='" + mDay + "' and agentgroup in (select agentgroup from labranchgroup where branchattr like '" +
                                                  bSSRS.GetText(b, 2) + "%')";
                                    strArr[5] = aExeSQL.getOneValue(fsql);
                                    //日承保标保
                                    String gsql = "select StandpremCBBranch('" +
                                                  mDay + "','" + mDay + "','" +
                                                  bSSRS.GetText(b, 2) +
                                                  "') from ldsysvar where sysvar='onerow'";
                                    strArr[6] = aExeSQL.getOneValue(gsql);
                                    //月承保标保
                                    String hsql = "select StandpremCBBranch('" +
                                                  mmDay[0] + "','" + mDay +
                                                  "','" + bSSRS.GetText(b, 2) +
                                                  "') from ldsysvar where sysvar='onerow'";
                                    strArr[7] = aExeSQL.getOneValue(hsql);
                                    //月承保件数
                                    //首先按照不同险种查出保单份数
                                    String jsql = "select calpieceCBBranch('" +
                                                  mmDay[0] + "','" + mDay +
                                                  "','" + bSSRS.GetText(b, 2) +
                                                  "') from ldsysvar where sysvar='onerow'";
                                    strArr[8] = aExeSQL.getOneValue(jsql);
                                    //人均标保
                                    if (strArr[1].equals("0"))
                                    {
                                        strArr[9] = "0";
                                    }
                                    else
                                    {
                                        strArr[9] = String.valueOf(Double.
                                                parseDouble(strArr[7]) /
                                                Integer.parseInt(strArr[1]));
                                    }
                                    //人均件数
                                    if (strArr[1].equals("0"))
                                    {
                                        strArr[10] = "0";
                                    }
                                    else
                                    {
                                        strArr[10] = String.valueOf(Double.
                                                parseDouble(strArr[8]) /
                                                Integer.parseInt(strArr[1]));
                                    }
                                    //件均标保
                                    if (Double.parseDouble(strArr[8]) == 0)
                                    {
                                        strArr[11] = "0";
                                    }
                                    else
                                    {
                                        strArr[11] = String.valueOf(Double.
                                                parseDouble(strArr[7]) /
                                                Double.parseDouble(strArr[8]));
                                    }
                                    //活动率
                                    double num1 = 0;
                                    String ssql =
                                            "select count( distinct agentcode) from ljapayperson where makeDate >='" +
                                            mmDay[0] + "' and makeDate <='" +
                                            mDay + "' and agentgroup in (select agentgroup from labranchgroup where branchattr like '" +
                                            bSSRS.GetText(b, 2) + "%')";
                                    num1 = Double.parseDouble(aExeSQL.
                                            getOneValue(ssql));
                                    if (strArr[1].equals("0"))
                                    {
                                        strArr[12] = "0";
                                    }
                                    else
                                    {
                                        strArr[12] = String.valueOf(num1 /
                                                Double.parseDouble(strArr[1]));
                                    }
                                    //人均产能
                                    if (num1 == 0)
                                    {
                                        strArr[13] = "0";
                                    }
                                    else
                                    {
                                        strArr[13] = String.valueOf(Double.
                                                parseDouble(strArr[7]) / num1);
                                    }
                                    //有效人力
                                    int num2 = 0;
                                    SSRS gSSRS = new SSRS();
                                    String rsql = "select count(count(*)) from ljapayperson a,laagent b where a.agentcode=b.agentcode and a.paycount=1 and a.makedate >='" +
                                                  mmDay[0] +
                                                  "' and a.makedate <='" + mDay + "' and branchcode in (select agentgroup from labranchgroup where branchattr like '" +
                                                  bSSRS.GetText(b, 2) +
                                                  "%') group by a.agentcode having NVL(sum(SumActuPayMoney),0)>'2000'";
                                    strArr[14] = aExeSQL.getOneValue(rsql);
                                    tlistTable.add(strArr);
                                    //计算展业机构：区值
                                    for (int p = 1; p <= 8; p++)
                                    {
                                        date2[p] += Double.parseDouble(strArr[p]);
                                    }
                                    date2[12] += num1;
                                    date2[14] += Double.parseDouble(strArr[14]);
                                }
                            }
                            //输入展业机构 部 信息
                        }
                    }
                    if (c == 2)
                    {
                        //展业机构：区一级名
                        strArr = new String[15];
                        strArr[0] = aSSRS.GetText(a, 2);
                        for (int p = 1; p <= 8; p++)
                        {
                            strArr[p] = String.valueOf(date2[p]);
                            //计算展业机构：支公司值
                            date3[p] += date2[p];
                        }
                        //区 人均标保
                        if (strArr[1].equals("0.0"))
                        {
                            strArr[9] = String.valueOf("0");
                        }
                        else
                        {
                            strArr[9] = String.valueOf(Double.parseDouble(
                                    strArr[7]) / Double.parseDouble(strArr[1]));
                        }
                        //区 人均件数
                        if (strArr[1].equals("0.0"))
                        {
                            strArr[10] = String.valueOf("0");
                        }
                        else
                        {
                            strArr[10] = String.valueOf(Double.parseDouble(
                                    strArr[8]) / Double.parseDouble(strArr[1]));
                        }
                        //区 件均标保
                        if (strArr[8].equals("0.0"))
                        {
                            strArr[11] = String.valueOf("0");
                        }
                        else
                        {
                            strArr[11] = String.valueOf(Double.parseDouble(
                                    strArr[7]) / Double.parseDouble(strArr[8]));
                        }
                        //区 活动率
                        if (strArr[1].equals("0.0"))
                        {
                            strArr[12] = String.valueOf("0");
                        }
                        else
                        {
                            strArr[12] = String.valueOf(date2[12] /
                                    Double.parseDouble(strArr[1]));
                        }
                        date3[12] += date2[12];
                        //区 人均产能
                        if (date2[12] == 0)
                        {
                            strArr[13] = String.valueOf("0");
                        }
                        else
                        {
                            strArr[13] = String.valueOf(Double.parseDouble(
                                    strArr[7]) / date2[12]);
                        }
                        //有效人力
                        strArr[14] = String.valueOf(date2[14]);
                        date3[14] += date2[14];
                        tlistTable.add(strArr);
                        strArr = new String[15];
                        strArr[0] = "";
                        strArr[1] = "";
                        strArr[2] = "";
                        strArr[3] = "";
                        strArr[4] = "";
                        strArr[5] = "";
                        strArr[6] = "";
                        strArr[7] = "";
                        strArr[8] = "";
                        strArr[9] = "";
                        strArr[10] = "";
                        strArr[11] = "";
                        strArr[12] = "";
                        strArr[13] = "";
                        strArr[14] = "";
                        tlistTable.add(strArr);
                    }
                }
            }

            //展业机构：总计
            strArr = new String[15];
            strArr[0] = "支公司总计";
            for (int u = 1; u <= 8; u++)
            {
                strArr[u] = String.valueOf(date3[u]);
            }
            //支公司 人均标保
            if (strArr[1].equals("0"))
            {
                strArr[9] = String.valueOf("0");
            }
            else
            {
                strArr[9] = String.valueOf(Double.parseDouble(strArr[7]) /
                                           Double.parseDouble(strArr[1]));
            }
            //支公司 人均件数
            if (strArr[1].equals("0"))
            {
                strArr[10] = String.valueOf("0");
            }
            else
            {
                strArr[10] = String.valueOf(Double.parseDouble(strArr[8]) /
                                            Double.parseDouble(strArr[1]));
            }
            //支公司 件均标保
            if (strArr[8].equals("0"))
            {
                strArr[11] = String.valueOf("0");
            }
            else
            {
                strArr[11] = String.valueOf(Double.parseDouble(strArr[7]) /
                                            Double.parseDouble(strArr[8]));
            }
            //支公司 活动率
            if (strArr[1].equals("0"))
            {
                strArr[12] = String.valueOf("0");
            }
            else
            {
                strArr[12] = String.valueOf(date3[12] /
                                            Double.parseDouble(strArr[1]));
            }
            //支公司 人均产能
            if (date3[12] == 0)
            {
                strArr[13] = String.valueOf("0");
            }
            else
            {
                strArr[13] = String.valueOf(Double.parseDouble(strArr[7]) /
                                            date3[12]);
            }
            //有效人力
            strArr[14] = String.valueOf(date3[14]);
            tlistTable.add(strArr);
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("PolNBDDay.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PolNBDDayF1PBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    public static void main(String[] args)
    {
        PolNBDepartF1PBL tPolNBDepartF1PBL = new PolNBDepartF1PBL();
    }
}