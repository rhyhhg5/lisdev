package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class AgentAnnualQuickBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private GlobalInput mGlobalInput = new GlobalInput();
    private String StartDay = "";
    private String Day = "";
    private String mManageComCount = "";

    //业务处理相关变量
    /** 全局数据 */
    public AgentAnnualQuickBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        StartDay = (String) cInputData.get(0);
        Day = (String) cInputData.get(1);
        mManageComCount = (String) cInputData.get(2);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "AgentAnnualQuickBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            System.out.println("start query----");
            ExeSQL aExeSQL = new ExeSQL(conn);
            int n = 0;
            String strArr[] = null;
            strArr = new String[9];
            String mDay[] = PubFun.calFLDate(Day);
            Day = AgentPubFun.formatDate(Day, "yyyy-MM-dd");
            String YearDay = Day.substring(0, 4) + "-01-01";
//      String startDay="";
            ListTable tlistTable = new ListTable();
            tlistTable.setName("AgentAnnualQuick");
            TextTag texttag = new TextTag(); //新建一个TextTag的实例
            texttag.add("Day", Day); //输入制表时间

            SSRS aSSRS = new SSRS();
            String asql = "select a.comcode,a.shortname,b.shortname from ldcom a,ldcom b where length(trim(a.comcode))='"
                          + mManageComCount +
                          "' and trim(b.comcode)=substr(a.comcode,0,4) order by a.comcode";
            aSSRS = aExeSQL.execSQL(asql);

            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                for (int b = 1; b <= aSSRS.getMaxCol(); b++)
                {
                    if (b == 1)
                    {
                        strArr[0] = "";
                        strArr[3] = aSSRS.GetText(a, 2);
                        strArr[4] = aSSRS.GetText(a, 3);
                    }
                    if (b == 2)
                    {
//            if(Day.compareTo("2004-08-29")<0)
//            startDay=Day;
//           if(Day.compareTo("2004-08-29")>=0&&Day.compareTo("2004-09-28")<=0)//判断月初起期
//            startDay="2004-08-29";
//           if(Day.compareTo("2004-09-29")>=0&&Day.compareTo("2004-10-28")<=0)
//            startDay="2004-09-29";
//           if(Day.compareTo("2004-10-29")>=0&&Day.compareTo("2004-11-28")<=0)
//             startDay="2004-10-29";
//           if(Day.compareTo("2004-11-29")>=0&&Day.compareTo("2004-12-31")<=0)
//             startDay="2004-11-29";


                        //月初人力
                        String bsql =
                                "select nvl(count(*),0) from laagent  where managecom like '" +
                                aSSRS.GetText(a, 1) + "%'"
                                + " and employdate <= '" + StartDay +
                                "' and branchtype = '1'"
                                + " and (OutWorkDate is null or OutWorkDate>'" +
                                StartDay + "')";
                        strArr[1] = aExeSQL.getOneValue(bsql);
                        //本日人力
                        String csql =
                                "select nvl(count(*),0) from laagent  where managecom like '" +
                                aSSRS.GetText(a, 1) + "%'"
                                + " and employdate<='" + Day +
                                "' and branchtype = '1'"
                                + " and (OutWorkDate is null or OutWorkDate>'" +
                                Day + "')";
                        strArr[2] = aExeSQL.getOneValue(csql);
                        //本日期交保费
                        String dsql =
                                "select nvl(sum(transmoney),0)/10000 from lacommision where managecom like '" +
                                aSSRS.GetText(a, 1) + "%'"
                                + " and tmakedate='" + Day +
                                "' and branchtype = '1'"
                                + " and payintv<>'0' and paycount<'2'";
                        strArr[5] = aExeSQL.getOneValue(dsql);
                        //本月期交保费
                        String esql =
                                "select nvl(sum(transmoney),0)/10000 from lacommision where managecom like '" +
                                aSSRS.GetText(a, 1) + "%'"
                                + " and tmakedate>='" + StartDay +
                                "' and tmakedate<='" + Day +
                                "' and branchtype='1'"
                                + " and payintv<>'0' and paycount<'2'";
                        strArr[6] = aExeSQL.getOneValue(esql);
                        //本年期交保费
                        String fsql =
                                "select nvl(sum(transmoney),0)/10000 from lacommision where managecom like '" +
                                aSSRS.GetText(a, 1) + "%'"
                                + " and tmakedate>='" + YearDay +
                                "' and tmakedate<='" + Day +
                                "' and branchtype='1'"
                                + " and payintv<>'0' and paycount<'2'";
                        strArr[7] = aExeSQL.getOneValue(fsql);
                        //百舸争流
                        String gsql =
                                "select nvl(sum(transmoney),0)/10000 from lacommision where managecom like '" +
                                aSSRS.GetText(a, 1) + "%'"
                                +
                                " and tmakedate>='2004-10-12' and tmakedate<='" +
                                Day + "' and branchtype='1'"
                                + " and payintv<>'0' and paycount<'2'";
                        strArr[8] = aExeSQL.getOneValue(gsql);
                        strArr[0] = String.valueOf(a);
                        tlistTable.add(strArr);
                        strArr = new String[9];
                    }
                }
            }

            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("AgentAnnualQuick.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
            //xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AgentAnnualQuickBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    public static void main(String[] args)
    {
    }
}