package com.sinosoft.lis.agentprint;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import com.f1j.ss.BookModelImpl;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

/**
 * <p>Title: Web业务系统 </p>
 * <p>Description: 将xls文件解析成指定格式的XML文件 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft </p>
 * @author zy
 * @version 1.0
 * @date 2003-12-04
 */
public class XlsParserXml
{
    public CErrors mErrors = new CErrors();

    private String m_strBatchNo = "";

    // 保存磁盘投保文件的文件名
    private String m_strFileName = "";
    // 保存磁盘投保文件的路径名
    private String m_strPathName = "";

    // 保存列名元素名映射文件的文件名
    private String m_strConfigFileName = ""; //？？？？？？

    // 保存Sheet相关的信息，如最大行，最大列，当前处理到的行等
    private Hashtable[] m_hashInfo = null;

    // 常量定义
    private static String PROP_MAX_ROW = "max_row";
    private static String PROP_MAX_COL = "max_col";
    private static String PROP_CUR_ROW = "cur_row";
    private static String PROP_COL_NAME = "col_name";

//  private static int SHEET_COUNT = 5;//sheet的个数

    private static int ROW_LIMIT = 1000; // 一次处理的行数,提高效率

    // 用来保存每一个小部分生成的XML文件名
    private List m_listFiles = new ArrayList(); //

    //定义BookModelImpl公共处理类
    private BookModelImpl m_book = new BookModelImpl();


    public XlsParserXml()
    {
        m_hashInfo = new Hashtable[1];

        for (int nIndex = 0; nIndex < 1; nIndex++)
        {
            m_hashInfo[nIndex] = new Hashtable();
            m_hashInfo[nIndex].put(PROP_CUR_ROW, "1"); // 从第一行开始解析
        }
    }

    // 设置要处理的文件名
    public boolean setFileName(String strFileName)
    {
        File file = new File(strFileName);
        if (!file.exists())
        {
            buildError("setFileName", "指定的文件不存在！");
            return false;
        }

        m_strFileName = strFileName;
        m_strPathName = file.getParent();

        int nPos = strFileName.indexOf('.');

        if (nPos == -1)
        {
            nPos = strFileName.length();
        }

        m_strBatchNo = strFileName.substring(0, nPos); //得到文件名

        nPos = m_strBatchNo.lastIndexOf('\\');

        if (nPos != -1)
        {
            m_strBatchNo = m_strBatchNo.substring(nPos + 1);
        }

        nPos = m_strBatchNo.lastIndexOf('/');

        if (nPos != -1)
        {
            m_strBatchNo = m_strBatchNo.substring(nPos + 1);
        }

        return true;
    }

    public String getFileName()
    {
        return m_strFileName;
    }

    // 设置映射文件名
//  public boolean setConfigFileName(String strConfigFileName) {
//    File file = new File(strConfigFileName);
//    if( !file.exists() ) {
//      buildError("setFileName", "指定的文件不存在！");
//      return false;
//    }
//
//    m_strConfigFileName = strConfigFileName;
//    return true;
//  }

//  public String getConfigFileName() {
//    return m_strConfigFileName;
//  }

    //？？？？？
    public String[] getDataFiles()
    {
        String[] files = new String[m_listFiles.size()];
        for (int nIndex = 0; nIndex < m_listFiles.size(); nIndex++)
        {
            files[nIndex] = (String) m_listFiles.get(nIndex);
        }

        return files;
    }

    // 转换操作，将磁盘投保文件转换成指定格式的XML文件。※※※※
    public boolean transform()
    {

        String strFileName = "";
        int nCount = 0;

        try
        {
            verify(); //校验

//      while( hasMoreData() ) {
            strFileName = m_strPathName + File.separator
                          + m_strBatchNo
                          + ".xml";

            genPart(strFileName);

            m_listFiles.add(strFileName);

//      }



        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("transfrom", ex.getMessage());
            return false;
        }
        return true;
    }

    // 下面是私有函数
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "GrpPolVTSParser";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 校验格式
     * 要求磁盘投保文件中的数据按照唯一标识，从小到大排列
     */
    private void verify() throws Exception
    {
        if (m_strFileName.equals(""))
        {
            throw new Exception("请先调用setFileName函数设置要处理的文件名。");
        }

        m_book.read(m_strFileName, new com.f1j.ss.ReadParams());
        if (m_book.getNumSheets() < 1)
        {
            throw new Exception("磁盘投保文件不完整，缺少Sheet。");
        }
        getMaxRow(0);
        getMaxCol(0);

//    int nMaxID = -1;
//    int nID = -1;
//    String strID = "";

        // 检查数据是否是按“唯一号”排序的
//    for(int nIndex = 0; nIndex < SHEET_COUNT; nIndex ++) {
//
//      nMaxID = -1;
//
//      for(int nRow = 1; nRow < getMaxRow(nIndex); nRow ++) {
//        strID = m_book.getText(nIndex, nRow, 0);
//        nID = Integer.parseInt(strID);
//        if( nID > nMaxID ) {
//          nMaxID = nID;
//        } else if( nID == nMaxID ) {
//          // do nothing
//        } else {
//          throw new Exception("投保文件中的数据不是按照唯一号从小到大排序的");
//        }
//      }
//    }

        // 将第一行中元素的值设为对应的XML元素的名字
        // 如在Sheet0中，每行的第一列对应的XML元素名字为ID。
//    DOMBuilder db = new DOMBuilder();
//    Document doc = db.build(new FileInputStream(m_strConfigFileName));
//    Element eleRoot = doc.getRootElement();
//    Element ele = null;
//    String strColName = "";

//    for(int nIndex = 0; nIndex < 1; nIndex ++) {
//
//      ele = eleRoot.getChild("Sheet" + String.valueOf(nIndex+1));
//      int nMaxCol = getMaxCol(nIndex);
//      String[] strArr = new String[nMaxCol];
//
//      for(int nCol = 0; nCol < nMaxCol; nCol ++) {
//        strColName = ele.getChildText("COL" + String.valueOf(nCol));
//        if( strColName == null || strColName.equals("") ) {
//          throw new Exception("找不到对应的配置信息，Sheet" + String.valueOf(nIndex+1)
//                              + ":COL" + String.valueOf(nCol));
//        }
//
//        strArr[nCol] = strColName;
//      }
//
//      setPropArray(nIndex, PROP_COL_NAME, strArr);
//    }
    }

    private int getMaxRow(int nSheetIndex) throws Exception
    {
        String str = "";
        int nMaxRow = getPropNumber(nSheetIndex, PROP_MAX_ROW);

        if (nMaxRow == -1)
        {
            for (nMaxRow = 0; nMaxRow < m_book.getMaxRow(); nMaxRow++)
            {
                str = m_book.getText(nSheetIndex, nMaxRow, 0);
                if (str == null || str.trim().equals(""))
                {
                    break;
                }
            }
            setPropNumber(nSheetIndex, PROP_MAX_ROW, nMaxRow);
        }

        return nMaxRow;
    }

    private int getMaxCol(int nSheetIndex) throws Exception
    {
        String str = "";
        int nMaxCol = getPropNumber(nSheetIndex, PROP_MAX_COL);

        if (nMaxCol == -1)
        {
            for (nMaxCol = 0; nMaxCol < m_book.getMaxRow(); nMaxCol++)
            {
                str = m_book.getText(nSheetIndex, 0, nMaxCol);
                if (str == null || str.trim().equals(""))
                {
                    break;
                }
            }
            setPropNumber(nSheetIndex, PROP_MAX_COL, nMaxCol);
        }
        return nMaxCol;
    }

    private int getPropNumber(int nSheetIndex, String strPropName)
    {
        Hashtable hash = m_hashInfo[nSheetIndex];
        String str = (String) hash.get(strPropName);
        if (str != null && !str.equals(""))
        {
            return Integer.parseInt(str);
        }
        else
        {
            return -1;
        }
    }

    private void setPropNumber(int nSheetIndex, String strPropName, int nValue)
    {
        Hashtable hash = m_hashInfo[nSheetIndex];
        hash.put(strPropName, String.valueOf(nValue));
    }

    private String[] getPropArray(int nSheetIndex, String strPropName)
    {
        Hashtable hash = m_hashInfo[nSheetIndex];
        String[] strArr = (String[]) hash.get(strPropName);
        return strArr;
    }

    private void setPropArray(int nSheetIndex, String strPropName,
                              String[] strArr)
    {
        Hashtable hash = m_hashInfo[nSheetIndex];
        hash.put(strPropName, strArr);
    }

    // 执行一次生成，将ROW_LIMIT行数的VTS数据生成一个Document
    private void genPart(String strFileName) throws Exception
    {
        Element root = new Element("TABLE");
        Attribute name = new Attribute("name", m_strBatchNo);
        LinkedList tList = new LinkedList();
        tList.add(name);
        root.setAttributes(tList);

//    Attribute name = new Attribute("name",m_strBatchNo);
//    root.setAttribute(name);
//    .setAttribute(name);
//    List nn = new ArrayList();
//    nn.
//    root.setText(m_strBatchNo);
        Document doc = new Document(root);
//    Element tColName = new Element("NAME");
        int c = 0;

        for (int b = 1; b <= this.getMaxRow(0) - 1; b++)
        {
            c = b - 1;
            Element ele = null;
            if (b == 1)
            {
                ele = new Element("HEAD");
            }
            else
            {
                ele = new Element("ROW");
            }
            root.addContent(ele);
            for (int a = 1; a <= this.getMaxCol(0) - 1; a++)
            {
                Element info = new Element("COL");
                info.setText(m_book.getText(b, a));
                ele.addContent(info);
            }
        }
//    ele = new Element("TABLE");
//    root.addContent(ele);
//    genPartLCPol(ele);



        XMLOutputter xo = new XMLOutputter("  ", true, "GB2312");
        xo.output(doc, new FileOutputStream(strFileName));
    }

    /**
     * 判断是否还有数据没有处理
     * 如果在存放保单信息的Sheet中，已经处理到了最大行，返回false；
     * 否则，返回true;
     * @return
     */
//  private boolean hasMoreData() {
//    int nCurRow = getPropNumber(0, PROP_CUR_ROW);
//    int nMaxRow = getPropNumber(0, PROP_MAX_ROW);
//
//    if( nCurRow >= nMaxRow ) {
//      return false;
//    } else {
//      return true;
//    }
//  }

//  private void genPartLCPol(Element eleParent)
//      throws Exception
//  {
//    // 保单信息存放在第一个Sheet中。
//    int nCurRow = getPropNumber(0, PROP_CUR_ROW);
//    int nMaxRow = getMaxRow(0);
//    int nMaxCol = getMaxCol(0);
//
//    String[] strColName = getPropArray(0, PROP_COL_NAME);
//    String strID = "";
//
//    int nRow = 0;
//    int nCount = 0;
//
//    // 对于有主附险的磁盘导入文件，只是前面的保单信息不同，
//    // 后面的保费、责任、被保险人及受益人等信息都是相同的，不需要每次都扫描。
//    // 主附险的唯一号是一样的。
//    String strIDCached = "";
//    Element eleCached = new Element("CACHED");
//
//    nCount = 0;
//    for(nRow = nCurRow, nCount = 0; nRow < nMaxRow; nRow ++) {
//
//      // 取得唯一号信息
//      strID = m_book.getText(0, nRow, 0);
//
//      // 一次处理ROW_LIMIT行，第二个条件保证主附险在同一个数据文件中
//      if( nCount ++ > ROW_LIMIT && !strID.equals(strIDCached) ) {
//        break;
//      }
//
//      System.out.println("Current row is : " + String.valueOf(nRow));
//
//      Element eleRow = new Element("ROW");
//      eleParent.addContent(eleRow);
//
//      // 产生保单信息
//      Element eleField = new Element("FIELD");
//      eleRow.addContent(eleField);
//      eleCached.removeChild("FIELD");  // 每一个保单信息都要重新生成Field
//      eleCached.addContent(eleField);
//
//      for(int nCol = 0; nCol < nMaxCol; nCol ++) {
//        Element ele = new Element( strColName[nCol] );
//        ele.setText( m_book.getText(0, nRow, nCol) );
//
//        eleField.addContent(ele);
//      }
//
//      // 如果是同一张保单，后面的信息不用重新生成
//      // BTW：现在也没法重新生成，因为现在只做一次扫描
//      if( !strID.equals(strIDCached) ) {
//        strIDCached = strID;  // 缓存上一次操作的保单唯一号
//
//        // 保费信息
//        Element elePrem = new Element("SUBPREMTABLE");
//      eleRow.addContent(elePrem);
//        eleCached.removeChild("SUBPREMTABLE");
//        eleCached.addContent(elePrem);
//
//        genPartSubTable(elePrem, 1, strID);
//
//        // 责任信息
//        Element eleDuty = new Element("SUBDUTYTABLE");
//      eleRow.addContent(eleDuty);
//        eleCached.removeChild("SUBDUTYTABLE");
//        eleCached.addContent(eleDuty);
//
//        genPartSubTable(eleDuty, 2, strID);
//
//        // 被保人信息
//        Element eleInsured = new Element("SUBINSUREDTABLE");
//      eleRow.addContent(eleInsured);
//        eleCached.removeChild("SUBINSUREDTABLE");
//        eleCached.addContent(eleInsured);
//
//        genPartSubTable(eleInsured, 3, strID);
//
//        // 受益人信息
//        Element eleBnf = new Element("SUBBNFTABLE");
//      eleRow.addContent(eleBnf);
//        eleCached.removeChild("SUBBNFTABLE");
//        eleCached.addContent(eleBnf);
//
//        genPartSubTable(eleBnf, 4, strID);
//      }
//
//      eleRow.setMixedContent(eleCached.getMixedContent());
//    }
//
//    setPropNumber(0, PROP_CUR_ROW, nRow);
//  }

    /**
     * 处理子表
     * 因为在Sheet的数据中，是按照“唯一号”ID排序的，所以对子表的数据，
     * 我们也只需要扫描一次就可以了。
     * @param eleParent
     * @param nSheetIndex
     * @param strID
     * @throws Exception
     */
//  private void genPartSubTable(Element eleParent, int nSheetIndex, String strID)
//      throws Exception
//  {
//    int nCurRow = getPropNumber(nSheetIndex, PROP_CUR_ROW);
//    int nMaxRow = getMaxRow(nSheetIndex);
//    int nMaxCol = getMaxCol(nSheetIndex);
//
//    String[] strColName = getPropArray(nSheetIndex, PROP_COL_NAME);
//
//    int nRow = 0;
//
//    for(nRow = nCurRow; nRow < nMaxRow; nRow ++) {
//      // 如果唯一号不等，就是新的保单记录了，跳出循环
//      if( !strID.equals( m_book.getText(nSheetIndex, nRow, 0) ) ) {
//        break;
//      }
//
//      Element eleRow = new Element("ROW");
//      eleParent.addContent(eleRow);
//
//      for(int nCol = 0; nCol < nMaxCol; nCol ++) {
//        Element ele = new Element( strColName[nCol] );
//        ele.setText( m_book.getText(nSheetIndex, nRow, nCol) );
//
//        eleRow.addContent(ele);
//      }
//    }
//
//    setPropNumber(nSheetIndex, PROP_CUR_ROW, nRow);
//  }
}