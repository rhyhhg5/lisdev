package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import java.sql.Connection;
import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class BankAllStatBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private GlobalInput mGlobalInput = new GlobalInput();
    private String StartDay = "";
    private String EndDay = "";

    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";

    //业务处理相关变量

    /** 全局数据 */
    public BankAllStatBL()
    {
    }

    /**
        传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        StartDay = (String) cInputData.get(0);
        EndDay = (String) cInputData.get(1);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 2));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "BankWeekBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String[] strArr = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("Bank");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDay", StartDay); //输入制表时间
        texttag.add("EndDay", EndDay); //输入制表时间
        strArr = new String[9];
        double[] data = null;
        data = new double[9];
        //统计银行
        String asql = "select name,agentcom from lacom where actype='01' and banktype='00' order by agentcom";

        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);

            //统计民生银代本期总保费
            String AllNewSQL =
                    "select nvl(sum(transmoney),0)/10000 from lacommision where branchtype='3' and tmakeDate>='"
                    + StartDay + "' and tmakeDate<='" + EndDay
                    + "' and (agentcom like '1%' or agentcom like '4%')";

            //统计民生银代累计总保费
            String AllSQL =
                    "select nvl(sum(transmoney),0)/10000 from lacommision where branchtype='3' and tmakeDate>='"
                    + EndDay.substring(0, 4) + "-01-01' and tmakeDate<='" +
                    EndDay
                    + "' and (agentcom like '1%' or agentcom like '4%')";

            double AllNewmoney = Double.parseDouble(aExeSQL.getOneValue(
                    AllNewSQL));
            double Allmoney = Double.parseDouble(aExeSQL.getOneValue(AllSQL));
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(asql);
            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                for (int b = 1; b <= aSSRS.getMaxCol(); b++)
                {
                    if (b == 1)
                    {
                        strArr[0] = aSSRS.GetText(a, 1); //银行名称
                    }
                    if (b == 2)
                    {
                        //签约网点数
                        String fsql =
                                "select count(agentcom) from Lacom where sellflag='Y' and CalFlag='Y' and AgentCom like '"
                                + aSSRS.GetText(a, 2) + "%'";
                        strArr[6] = aExeSQL.getOneValue(fsql);
                        data[6] += Double.parseDouble(strArr[6]);
                        //活动网点数
                        String gsql =
                                "select count(distinct agentcom),nvl(sum(transmoney),0)/10000 from Lacommision where branchtype='3' and AgentCom like '"
                                + aSSRS.GetText(a, 2) + "%' and tmakeDate>='"
                                + StartDay + "' and tmakeDate<='" + EndDay +
                                "'";
                        SSRS tSSRS = new SSRS();
                        tSSRS = aExeSQL.execSQL(gsql);
                        strArr[7] = tSSRS.GetText(1, 1);
                        data[7] += Double.parseDouble(strArr[7]);
                        data[8] += Double.parseDouble(tSSRS.GetText(1, 2)); //银行本期承保
                        //本期网均产能
                        if (strArr[6].equals("0"))
                        {
                            strArr[8] = "0";
                        }
                        else
                        {
                            strArr[8] = new DecimalFormat("0.00").format(Double
                                    .parseDouble(tSSRS.GetText(1, 2)) / Double
                                    .parseDouble(strArr[6]));
                        }

                        //查询险种
                        String ksql =
                                "select distinct a.riskcode,b.riskname from lacommision a,LMRiskApp b where branchtype='3' "
                                + "and a.riskcode=b.riskcode  and tmakeDate<='"
                                + EndDay + "' and AgentCom like '"
                                + aSSRS.GetText(a, 2) + "%'";
                        SSRS dSSRS = new SSRS();
                        dSSRS = aExeSQL.execSQL(ksql);
                        for (int g = 1; g <= dSSRS.getMaxRow(); g++)
                        {
                            strArr[1] = dSSRS.GetText(g, 2);
                            //本期承保
                            String jsql =
                                    "select nvl(sum(transmoney),0)/10000 from Lacommision where branchtype='3' and tmakeDate>='"
                                    + StartDay + "' and tmakeDate<='" + EndDay
                                    + "' and AgentCom like '" +
                                    aSSRS.GetText(a, 2)
                                    + "%' and riskcode='" + dSSRS.GetText(g, 1) +
                                    "'";
                            strArr[2] = aExeSQL.getOneValue(jsql);
                            strArr[3] = new DecimalFormat("0.00%").format(
                                    Double
                                    .parseDouble(strArr[2]) / AllNewmoney);

                            //累计承保
                            String dsql =
                                    "select nvl(sum(transmoney),0)/10000 from Lacommision where branchtype='3' and tmakeDate>='"
                                    + EndDay.substring(0, 4)
                                    + "-01-01' and tmakeDate<='" + EndDay
                                    + "' and AgentCom like '" +
                                    aSSRS.GetText(a, 2)
                                    + "%' and riskcode='" + dSSRS.GetText(g, 1) +
                                    "'";
                            strArr[4] = aExeSQL.getOneValue(dsql);
                            strArr[5] = new DecimalFormat("0.00%").format(
                                    Double
                                    .parseDouble(strArr[4]) / Allmoney);
                            if (g != 1)
                            {
                                strArr[6] = "";
                                strArr[7] = "";
                                strArr[8] = "";
                            }
                            tlistTable.add(strArr);
                            strArr = new String[9];
                        }
                    }
                }
            }

            strArr = new String[9];
            strArr[0] = "总计";
            //本期承保
            strArr[2] = String.valueOf(AllNewmoney);
            //累计承保
            strArr[4] = String.valueOf(Allmoney);
            //签约网点数
            strArr[6] = String.valueOf(data[6]);
            //活动网点数
            strArr[7] = String.valueOf(data[7]);
            //网均产能
            if (strArr[6].equals("0.0"))
            {
                strArr[8] = "0";
            }
            else
            {
                strArr[8] = new DecimalFormat("0.00").format(data[8] / Double
                        .parseDouble(strArr[6]));
            }
            tlistTable.add(strArr);
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("BankAllStat.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
//        xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
            mResult.addElement(xmlexport);

            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankAllStatBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    public static void main(String[] args)
    {
    }
}