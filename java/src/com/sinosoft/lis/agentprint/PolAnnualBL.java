package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import java.sql.Connection;
import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PolAnnualBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private String StartDay = "";
    private String EndDay = "";
    private String mManageCom = "";
//  double date3[]= new double[20];//定义总计的数组
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    public PolAnnualBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        this.StartDay = (String) cInputData.get(0);
        this.EndDay = (String) cInputData.get(1);
        mManageCom = (String) cInputData.get(2);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "PolNewBDayBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String strArr[] = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("Ann");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDay", StartDay); //输入制表时间
        texttag.add("EndDay", EndDay); //输入制表时间

        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //计算区和支公司共有多少
        double date[] = new double[8]; //合计
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);

            //查询支公司
            String aasql =
                    "select b from (select trim(comcode) a,shortname b from ldcom union"
                    +
                    " select trim(branchattr) a,name b From labranchgroup where branchtype='1'"
                    + " and (state<>1 or state is null))"
                    + " where a ='" + this.mManageCom + "'";
            texttag.add("ManageCom", aExeSQL.getOneValue(aasql)); //输入制表时间

            int n = this.mManageCom.trim().length();
            System.out.println("录入机构－－－" + mManageCom);
            System.out.println("录入机构长度－－－" + n);
            String MM = "";
            switch (n)
            {
                case 2:
                {
                    MM = " and length(a)=4 ";
                    break;
                }
                case 4:
                {
                    MM = " and length(a)=6 ";
                    break;
                }
                case 6:
                {
                    MM = " and length(a)=8 ";
                    break;
                }
                case 8:
                {
                    MM = " and length(a)=12 ";
                    break;
                }
                case 12:
                {
                    MM = " and length(a)=15 ";
                    break;
                }
                case 15:
                {
                    MM = " and length(a)=18 ";
                    break;
                }
            }

            nsql =
                    "select a,b from (select trim(comcode) a,shortname b from ldcom union"
                    +
                    " select trim(branchattr) a,name b From labranchgroup where branchtype='1'"
                    +
                    " and (state<>1 or state is null) and length(trim(branchattr))<>8)"
                    + " where a like '" + this.mManageCom + "%' and a like '" +
                    mGlobalInput.ManageCom + "%' "
                    + MM + "order by a";
            SSRS ySSRS = new SSRS();
            ySSRS = aExeSQL.execSQL(nsql);
            for (int z = 1; z <= ySSRS.getMaxRow(); z++)
            {
                for (int y = 1; y <= ySSRS.getMaxCol(); y++)
                {
                    if (y == 1)
                    {
                        strArr = new String[8];
                        strArr[0] = ySSRS.GetText(z, 2);
                        //月初规模人力
                        String asql =
                                "select nvl(count(*),0) from laagent a,labranchgroup b where "
                                + " employdate <= '" + this.StartDay +
                                "' and a.branchtype = '1'"
                                + " and (OutWorkDate is null or OutWorkDate>'" +
                                StartDay + "')"
                                +
                                " and a.branchcode=b.agentgroup and branchattr like '" +
                                ySSRS.GetText(z, 1) + "%'";
                        strArr[1] = aExeSQL.getOneValue(asql);
                        date[1] += Double.parseDouble(strArr[1]);
                        //月末规模人力
                        String bsql =
                                "select nvl(count(*),0) from laagent a,labranchgroup b where "
                                + " employdate <= '" + EndDay +
                                "' and a.branchtype = '1'"
                                + " and (OutWorkDate is null or OutWorkDate>'" +
                                EndDay + "')"
                                +
                                " and a.branchcode=b.agentgroup and branchattr like '" +
                                ySSRS.GetText(z, 1) + "%'";
                        strArr[2] = aExeSQL.getOneValue(bsql);
                        date[2] += Double.parseDouble(strArr[2]);
                        //期交保费、件数
                        String dsql = "select NVL(sum(transmoney),0)/10000,nvl(sum(calcount),0) from lacommision where tmakedate >='" +
                                      StartDay
                                      + "' and tmakedate <='" + EndDay +
                                      "'  and branchtype='1' and branchattr like '" +
                                      ySSRS.GetText(z, 1)
                                      +
                                      "%' and payintv<>0 and (paycount=1 or paycount=0)";
                        SSRS aSSRS = new SSRS();
                        aSSRS = aExeSQL.execSQL(dsql);
                        strArr[3] = aSSRS.GetText(1, 1);
                        date[3] += Double.parseDouble(strArr[3]);
                        strArr[4] = aSSRS.GetText(1, 2);
                        date[4] += Double.parseDouble(strArr[4]);
                        //规模保费、件数
                        String csql = "select NVL(sum(transmoney),0)/10000,nvl(sum(calcount),0) from lacommision where tmakedate >='" +
                                      StartDay
                                      + "' and tmakedate <='" + EndDay +
                                      "' and (paycount=1 or paycount=0) and branchtype='1' and branchattr like '" +
                                      ySSRS.GetText(z, 1)
                                      + "%'";
                        SSRS bSSRS = new SSRS();
                        bSSRS = aExeSQL.execSQL(csql);
                        strArr[5] = bSSRS.GetText(1, 1);
                        date[5] += Double.parseDouble(strArr[5]);
                        strArr[6] = bSSRS.GetText(1, 2);
                        date[6] += Double.parseDouble(strArr[6]);
                        //期交占比
                        if (!strArr[5].equals("0"))
                        {
                            strArr[7] = new DecimalFormat("0.00%").format(
                                    Double.parseDouble(strArr[3]) /
                                    Double.parseDouble(strArr[5]));
                        }
                        else
                        {
                            strArr[7] = "0";
                        }
                        tlistTable.add(strArr);
                    }
                }
            }

            //展业机构：总计
            strArr = new String[8];
            strArr[0] = "合计";
            for (int u = 1; u <= 6; u++)
            {
                strArr[u] = String.valueOf(date[u]);
            }
            if (!strArr[5].equals("0"))
            {
                strArr[7] = new DecimalFormat("0.00%").format(Double.
                        parseDouble(strArr[3]) / Double.parseDouble(strArr[5]));
            }
            else
            {
                strArr[7] = "0";
            }

            tlistTable.add(strArr);
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("PolAnnual.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PolSYOrderBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }

        System.out.println("end");
        return true;
    }

    public static void main(String[] args)
    {
        PolNBDepartF1PBL tPolNBDepartF1PBL = new PolNBDepartF1PBL();
    }
}