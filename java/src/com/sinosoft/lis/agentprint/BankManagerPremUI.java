package com.sinosoft.lis.agentprint;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author
 * @version 1.0
 */

public class BankManagerPremUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String StartDay = "";
    private String EndDay = "";
    private String RiskCode = "";
    public BankManagerPremUI()
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        // 进行业务处理
        if (!dealData())
        {
            return false;
        }

        // 准备传往后台的数据
        VData vData = new VData();

        if (!prepareOutputData(vData))
        {
            return false;
        }

        BankManagerPremBL tBankManagerPremBL = new BankManagerPremBL();
        System.out.println("Start PolDayListUI Submit ...");

        if (!tBankManagerPremBL.submitData(vData, cOperate))
        {
            if (tBankManagerPremBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tBankManagerPremBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "tBankManagerPremBL发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            mResult = tBankManagerPremBL.getResult();
            return true;
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.addElement(StartDay);
            vData.addElement(EndDay);
            vData.addElement(RiskCode);
            vData.add(mGlobalInput);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        StartDay = (String) cInputData.get(0);
        EndDay = (String) cInputData.get(1);
        RiskCode = (String) cInputData.get(2);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 3));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "tBankManagerPremUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    public static void main(String[] args)
    {
        String StartDay = "2003-06-01";
        String EndDay = "2003-07-01";
        String RiskCode = "312201";
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "8611";
        VData tVData = new VData();
        tVData.addElement(StartDay);
        tVData.addElement(EndDay);
        tVData.addElement(RiskCode);
        tVData.addElement(tG);
        BankManagerPremUI UI = new BankManagerPremUI();
        if (!UI.submitData(tVData, ""))
        {
            if (UI.mErrors.needDealError())
            {
                System.out.println(UI.mErrors.getFirstError());
            }
            else
            {
                System.out.println("UI发生错误，但是没有提供详细的出错信息");
            }
        }
        else
        {
            VData vData = UI.getResult();
            System.out.println("已经接收了数据!!!");
        }
    }
}