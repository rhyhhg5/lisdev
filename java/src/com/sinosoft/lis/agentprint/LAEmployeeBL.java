package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import java.text.DecimalFormat;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.F1PrintVTS;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LAEmployeeBL
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private String strArr[] = null;
    private F1PrintVTS tF1PrintVTS;
    //取得的时间
    private String AgentGroup = null;
    private String BranchAttr = null;
    private String IndexCalNo = null;
    private String strTemplatePath = null;
    private String mOperate = "";
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    public LAEmployeeBL()
    {
    }

    public static void main(String[] args)
    {
        try
        {
            String strTemplatePath = "E:\\lis\\ui\\f1print\\template\\";
            String BranchAttr = "861100000101001017";
            String IndexCalNo = "200307";
            String AgentGroup = "";
            VData tVData = new VData();
            GlobalInput tG = new GlobalInput();
            tG.Operator = "001";
            tG.ManageCom = "86";
            tG.ComCode = "86";
            tVData.addElement(AgentGroup);
            tVData.addElement(BranchAttr);
            tVData.addElement(IndexCalNo);
            tVData.add(strTemplatePath);
            tVData.add(tG);
            AgentWagePF1PBL agentWagePF1PBL1 = new AgentWagePF1PBL();
            agentWagePF1PBL1.submitData(tVData, "PRINT");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        AgentGroup = ((String) cInputData.getObject(0));
        BranchAttr = ((String) cInputData.getObject(1));
        IndexCalNo = ((String) cInputData.getObject(2));
        strTemplatePath = ((String) cInputData.getObject(3));
        strTemplatePath = strTemplatePath + "AgentWage1.vts";
        tF1PrintVTS = new F1PrintVTS(strTemplatePath, 9);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LCPolBillF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean dealDatabu(double[] wage1)
    {
        strArr[0] = "部薪资合计";
        strArr[1] = "年月:" + IndexCalNo + "";
        tF1PrintVTS.addRow(strArr);
        strArr = new String[9];
        strArr[0] = "创业扶持金:" + new DecimalFormat("0.00").format(wage1[1]);
        strArr[1] = "创业金:" + new DecimalFormat("0.00").format(wage1[2]);
        strArr[2] = "转正奖:" + new DecimalFormat("0.00").format(wage1[3]);
        strArr[3] = "初年度佣金:" + new DecimalFormat("0.00").format(wage1[4]);
        strArr[4] = "续年度佣金:" + new DecimalFormat("0.00").format(wage1[5]);
        strArr[5] = "伯乐奖:" + new DecimalFormat("0.00").format(wage1[6]);
        strArr[6] = "育才奖金:" + new DecimalFormat("0.00").format(wage1[7]);
        strArr[7] = "再次转正补佣:" + new DecimalFormat("0.00").format(wage1[8]);
        strArr[8] = "高级理财服务主任特别津贴:" + new DecimalFormat("0.00").format(wage1[9]);
        tF1PrintVTS.addRow(strArr);
        strArr = new String[9];
        strArr[0] = "职务津贴:" + new DecimalFormat("0.00").format(wage1[10]);
        strArr[1] = "直辖组管理津贴:" + new DecimalFormat("0.00").format(wage1[11]);
        strArr[2] = "组育成津贴:" + new DecimalFormat("0.00").format(wage1[12]);
        strArr[3] = "部直辖管理津贴:" + new DecimalFormat("0.00").format(wage1[13]);
        strArr[4] = "增部津贴:" + new DecimalFormat("0.00").format(wage1[14]);
        strArr[5] = "总监职务底薪:" + new DecimalFormat("0.00").format(wage1[15]);
        strArr[6] = "总监育成津贴:" + new DecimalFormat("0.00").format(wage1[16]);
        strArr[7] = "业务拓展津贴:" + new DecimalFormat("0.00").format(wage1[17]);
        strArr[8] = "资深总监职务底薪:" + new DecimalFormat("0.00").format(wage1[18]);
        tF1PrintVTS.addRow(strArr);
        strArr = new String[9];
        strArr[0] = "个人年终奖:" + new DecimalFormat("0.00").format(wage1[19]);
        strArr[1] = "奖惩款:" + new DecimalFormat("0.00").format(wage1[20]);
        strArr[2] = "加/扣款:" + new DecimalFormat("0.00").format(wage1[21]);
        strArr[3] = "支助薪资:" + new DecimalFormat("0.00").format(wage1[22]);
        strArr[4] = "招募资金(衔):" + new DecimalFormat("0.00").format(wage1[23]);
        strArr[5] = "创业奖(衔):" + new DecimalFormat("0.00").format(wage1[24]);
        strArr[6] = "衔接资金加扣款:" + new DecimalFormat("0.00").format(wage1[25]);
        strArr[7] = "底薪:" + new DecimalFormat("0.00").format(wage1[26]);
        strArr[8] = "社保费用支持金:" + new DecimalFormat("0.00").format(wage1[27]);
        tF1PrintVTS.addRow(strArr);
        strArr = new String[9];
        strArr[0] = "费用补贴:" + new DecimalFormat("0.00").format(wage1[28]);
        strArr[1] = "养老保险:" + new DecimalFormat("0.00").format(wage1[29]);
        strArr[2] = "失业保险:" + new DecimalFormat("0.00").format(wage1[30]);
        strArr[3] = "医疗保险:" + new DecimalFormat("0.00").format(wage1[31]);
        strArr[4] = "工伤保险:" + new DecimalFormat("0.00").format(wage1[32]);
        strArr[5] = "住房公积金:" + new DecimalFormat("0.00").format(wage1[33]);
        strArr[6] = "加扣款3:" + new DecimalFormat("0.00").format(wage1[34]);
        strArr[7] = "本期应发金额:" + new DecimalFormat("0.00").format(wage1[35]);
        strArr[8] = "上次佣金余额:" + new DecimalFormat("0.00").format(wage1[36]);
        tF1PrintVTS.addRow(strArr);
        strArr = new String[9];
        strArr[0] = "扣税:" + new DecimalFormat("0.00").format(wage1[37]);
        strArr[1] = "差勤扣款:" + new DecimalFormat("0.00").format(wage1[38]);
        strArr[2] = "本期实发金额:" + new DecimalFormat("0.00").format(wage1[39]);
        tF1PrintVTS.addRow(strArr);
        strArr = new String[9];
        strArr[0] = "";
        strArr[1] = "";
        strArr[2] = "";
        strArr[3] = "";
        strArr[4] = "";
        strArr[5] = "";
        strArr[6] = "";
        strArr[7] = "";
        strArr[8] = "";
        tF1PrintVTS.addRow(strArr);
        strArr = new String[9];
        return true;
    }

    private boolean dealDataqu(double[] wage2)
    {
        strArr[0] = "区薪资合计";
        strArr[1] = "年月:" + IndexCalNo + "";
        tF1PrintVTS.addRow(strArr);
        strArr = new String[9];
        strArr[0] = "创业扶持金:" + new DecimalFormat("0.00").format(wage2[1]);
        strArr[1] = "创业金:" + new DecimalFormat("0.00").format(wage2[2]);
        strArr[2] = "转正奖:" + new DecimalFormat("0.00").format(wage2[3]);
        strArr[3] = "初年度佣金:" + new DecimalFormat("0.00").format(wage2[4]);
        strArr[4] = "续年度佣金:" + new DecimalFormat("0.00").format(wage2[5]);
        strArr[5] = "伯乐奖:" + new DecimalFormat("0.00").format(wage2[6]);
        strArr[6] = "育才奖金:" + new DecimalFormat("0.00").format(wage2[7]);
        strArr[7] = "再次转正补佣:" + new DecimalFormat("0.00").format(wage2[8]);
        strArr[8] = "高级理财服务主任特别津贴:" + new DecimalFormat("0.00").format(wage2[9]);
        tF1PrintVTS.addRow(strArr);
        strArr = new String[9];
        strArr[0] = "职务津贴:" + new DecimalFormat("0.00").format(wage2[10]);
        strArr[1] = "直辖组管理津贴:" + new DecimalFormat("0.00").format(wage2[11]);
        strArr[2] = "组育成津贴:" + new DecimalFormat("0.00").format(wage2[12]);
        strArr[3] = "部直辖管理津贴:" + new DecimalFormat("0.00").format(wage2[13]);
        strArr[4] = "增部津贴:" + new DecimalFormat("0.00").format(wage2[14]);
        strArr[5] = "总监职务底薪:" + new DecimalFormat("0.00").format(wage2[15]);
        strArr[6] = "总监育成津贴:" + new DecimalFormat("0.00").format(wage2[16]);
        strArr[7] = "业务拓展津贴:" + new DecimalFormat("0.00").format(wage2[17]);
        strArr[8] = "资深总监职务底薪:" + new DecimalFormat("0.00").format(wage2[18]);
        tF1PrintVTS.addRow(strArr);
        strArr = new String[9];
        strArr[0] = "个人年终奖:" + new DecimalFormat("0.00").format(wage2[19]);
        strArr[1] = "奖惩款:" + new DecimalFormat("0.00").format(wage2[20]);
        strArr[2] = "加/扣款:" + new DecimalFormat("0.00").format(wage2[21]);
        strArr[3] = "支助薪资:" + new DecimalFormat("0.00").format(wage2[22]);
        strArr[4] = "招募资金(衔):" + new DecimalFormat("0.00").format(wage2[23]);
        strArr[5] = "创业奖(衔):" + new DecimalFormat("0.00").format(wage2[24]);
        strArr[6] = "衔接资金加扣款:" + new DecimalFormat("0.00").format(wage2[25]);
        strArr[7] = "底薪:" + new DecimalFormat("0.00").format(wage2[26]);
        strArr[8] = "社保费用支持金:" + new DecimalFormat("0.00").format(wage2[27]);
        tF1PrintVTS.addRow(strArr);
        strArr = new String[9];
        strArr[0] = "费用补贴:" + new DecimalFormat("0.00").format(wage2[28]);
        strArr[1] = "养老保险:" + new DecimalFormat("0.00").format(wage2[29]);
        strArr[2] = "失业保险:" + new DecimalFormat("0.00").format(wage2[30]);
        strArr[3] = "医疗保险:" + new DecimalFormat("0.00").format(wage2[31]);
        strArr[4] = "工伤保险:" + new DecimalFormat("0.00").format(wage2[32]);
        strArr[5] = "住房公积金:" + new DecimalFormat("0.00").format(wage2[33]);
        strArr[6] = "加扣款3:" + new DecimalFormat("0.00").format(wage2[34]);
        strArr[7] = "本期应发金额:" + new DecimalFormat("0.00").format(wage2[35]);
        strArr[8] = "上次佣金余额:" + new DecimalFormat("0.00").format(wage2[36]);
        tF1PrintVTS.addRow(strArr);
        strArr = new String[9];
        strArr[0] = "扣税:" + new DecimalFormat("0.00").format(wage2[37]);
        strArr[1] = "差勤扣款:" + new DecimalFormat("0.00").format(wage2[38]);
        strArr[2] = "本期实发金额:" + new DecimalFormat("0.00").format(wage2[39]);
        tF1PrintVTS.addRow(strArr);
        strArr = new String[9];
        return true;
    }

    private boolean dealDataduqu(double[] wage3)
    {
        strArr[0] = "督导区薪资合计";
        strArr[1] = "年月:" + IndexCalNo + "";
        tF1PrintVTS.addRow(strArr);
        strArr = new String[9];
        strArr[0] = "创业扶持金:" + new DecimalFormat("0.00").format(wage3[1]);
        strArr[1] = "创业金:" + new DecimalFormat("0.00").format(wage3[2]);
        strArr[2] = "转正奖:" + new DecimalFormat("0.00").format(wage3[3]);
        strArr[3] = "初年度佣金:" + new DecimalFormat("0.00").format(wage3[4]);
        strArr[4] = "续年度佣金:" + new DecimalFormat("0.00").format(wage3[5]);
        strArr[5] = "伯乐奖:" + new DecimalFormat("0.00").format(wage3[6]);
        strArr[6] = "育才奖金:" + new DecimalFormat("0.00").format(wage3[7]);
        strArr[7] = "再次转正补佣:" + new DecimalFormat("0.00").format(wage3[8]);
        strArr[8] = "高级理财服务主任特别津贴:" + new DecimalFormat("0.00").format(wage3[9]);
        tF1PrintVTS.addRow(strArr);
        strArr = new String[9];
        strArr[0] = "职务津贴:" + new DecimalFormat("0.00").format(wage3[10]);
        strArr[1] = "直辖组管理津贴:" + new DecimalFormat("0.00").format(wage3[11]);
        strArr[2] = "组育成津贴:" + new DecimalFormat("0.00").format(wage3[12]);
        strArr[3] = "部直辖管理津贴:" + new DecimalFormat("0.00").format(wage3[13]);
        strArr[4] = "增部津贴:" + new DecimalFormat("0.00").format(wage3[14]);
        strArr[5] = "总监职务底薪:" + new DecimalFormat("0.00").format(wage3[15]);
        strArr[6] = "总监育成津贴:" + new DecimalFormat("0.00").format(wage3[16]);
        strArr[7] = "业务拓展津贴:" + new DecimalFormat("0.00").format(wage3[17]);
        strArr[8] = "资深总监职务底薪:" + new DecimalFormat("0.00").format(wage3[18]);
        tF1PrintVTS.addRow(strArr);
        strArr = new String[9];
        strArr[0] = "个人年终奖:" + new DecimalFormat("0.00").format(wage3[19]);
        strArr[1] = "奖惩款:" + new DecimalFormat("0.00").format(wage3[20]);
        strArr[2] = "加/扣款:" + new DecimalFormat("0.00").format(wage3[21]);
        strArr[3] = "支助薪资:" + new DecimalFormat("0.00").format(wage3[22]);
        strArr[4] = "招募资金(衔):" + new DecimalFormat("0.00").format(wage3[23]);
        strArr[5] = "创业奖(衔):" + new DecimalFormat("0.00").format(wage3[24]);
        strArr[6] = "衔接资金加扣款:" + new DecimalFormat("0.00").format(wage3[25]);
        strArr[7] = "底薪:" + new DecimalFormat("0.00").format(wage3[26]);
        strArr[8] = "社保费用支持金:" + new DecimalFormat("0.00").format(wage3[27]);
        tF1PrintVTS.addRow(strArr);
        strArr = new String[9];
        strArr[0] = "费用补贴:" + new DecimalFormat("0.00").format(wage3[28]);
        strArr[1] = "养老保险:" + new DecimalFormat("0.00").format(wage3[29]);
        strArr[2] = "失业保险:" + new DecimalFormat("0.00").format(wage3[30]);
        strArr[3] = "医疗保险:" + new DecimalFormat("0.00").format(wage3[31]);
        strArr[4] = "工伤保险:" + new DecimalFormat("0.00").format(wage3[32]);
        strArr[5] = "住房公积金:" + new DecimalFormat("0.00").format(wage3[33]);
        strArr[6] = "加扣款3:" + new DecimalFormat("0.00").format(wage3[34]);
        strArr[7] = "本期应发金额:" + new DecimalFormat("0.00").format(wage3[35]);
        strArr[8] = "上次佣金余额:" + new DecimalFormat("0.00").format(wage3[36]);
        tF1PrintVTS.addRow(strArr);
        strArr = new String[9];
        strArr[0] = "扣税:" + new DecimalFormat("0.00").format(wage3[37]);
        strArr[1] = "差勤扣款:" + new DecimalFormat("0.00").format(wage3[38]);
        strArr[2] = "本期实发金额:" + new DecimalFormat("0.00").format(wage3[39]);
        tF1PrintVTS.addRow(strArr);
        strArr = new String[9];
        strArr[0] = "";
        strArr[1] = "";
        strArr[2] = "";
        strArr[3] = "";
        strArr[4] = "";
        strArr[5] = "";
        strArr[6] = "";
        strArr[7] = "";
        strArr[8] = "";
        tF1PrintVTS.addRow(strArr);
        strArr = new String[9];
        return true;
    }

    private boolean getPrintData()
    {
        String tDate = IndexCalNo.substring(0, 4) + "-" +
                       IndexCalNo.substring(4) + "-" + "12";
        String mDate[] = PubFun.calFLDate(tDate);
        String BranchLevel1 = "";
        String BranchLevel2 = "";
        String BranchLevel3 = "";
        String BranchLevel4 = "";
        //按照部、组排序
        String xsql = "select BranchLevel,branchattr,name from labranchgroup a where branchattr like '" +
                      BranchAttr
                      + "%' and (state<>'1' or state is null) and branchtype='1' and exists (select 'X' from lawage "
                      + "where agentgroup=a.agentgroup and indexcalno='"
                      + IndexCalNo + "' and state='1') order by branchattr";
        SSRS xSSRS = new SSRS();
        ExeSQL xExeSQL = new ExeSQL();
        xSSRS = xExeSQL.execSQL(xsql);
        double wage1[] = new double[40]; //部一级
        double wage2[] = new double[40]; //区一级
        double wage3[] = new double[40]; //督导部一级
        double wage4[] = new double[40]; //支公司一级
        String branchattr1 = "";
        for (int p = 1; p <= xSSRS.getMaxRow(); p++)
        {
            double wage[] = new double[40];
            String BranchLevel = "";
            for (int q = 1; q <= xSSRS.getMaxCol(); q++)
            {
                branchattr1 = xSSRS.GetText(xSSRS.getMaxRow(), 2); //最后一条纪录
                if (q == 1)
                {
                    BranchLevel = xSSRS.GetText(p, q);
                    BranchLevel1 = xSSRS.GetText(1, 1);
                    if (p == 2)
                    {
                        BranchLevel2 = xSSRS.GetText(2, 1);
                    }
                    if (p == 3)
                    {
                        BranchLevel3 = xSSRS.GetText(3, 1);
                    }
                    if (p == 4)
                    {
                        BranchLevel4 = xSSRS.GetText(4, 1);
                    }
                }
                if (q == 2)
                {
                    strArr = new String[9];
                    strArr[1] = xSSRS.GetText(p, q);
                    strArr[2] = "年月:" + IndexCalNo + "";
                    BranchAttr = xSSRS.GetText(p, q);
                }
                if (q == 3)
                {
                    if (BranchLevel1.equals("05") && BranchLevel.equals("05") ||
                        BranchLevel1.equals("04") && BranchLevel.equals("04") ||
                        BranchLevel1.equals("03") && BranchLevel.equals("03") ||
                        BranchLevel1.equals("02") && BranchLevel.equals("02"))
                    {
                        strArr[0] = xSSRS.GetText(p, q);
                        tF1PrintVTS.addRow(strArr);
                    }
                    else if (BranchLevel2.equals("04") &&
                             BranchLevel.equals("04") ||
                             BranchLevel2.equals("03") &&
                             BranchLevel.equals("03") ||
                             BranchLevel2.equals("02") &&
                             BranchLevel.equals("02"))
                    {
                        strArr[0] = xSSRS.GetText(p, q);
                        tF1PrintVTS.addRow(strArr);
                        BranchLevel2 = "";
                    }
                    else if (BranchLevel3.equals("03") &&
                             BranchLevel.equals("03") ||
                             BranchLevel3.equals("02") &&
                             BranchLevel.equals("02"))
                    {
                        strArr[0] = xSSRS.GetText(p, q);
                        tF1PrintVTS.addRow(strArr);
                        BranchLevel3 = "";
                    }
                    else if (BranchLevel4.equals("02") &&
                             BranchLevel.equals("02"))
                    {
                        strArr[0] = xSSRS.GetText(p, q);
                        tF1PrintVTS.addRow(strArr);
                        BranchLevel4 = "";
                    }
                    else
                    {
                        if (BranchLevel.equals("01"))
                        {
                            strArr[0] = xSSRS.GetText(p, q);
                            strArr[3] = "";
                            strArr[4] = "";
                            strArr[5] = "";
                            strArr[6] = "";
                            strArr[7] = "";
                            strArr[8] = "";
                            tF1PrintVTS.addRow(strArr);
                            //查组里的每个人工资信息
                            SSRS tSSRS = new SSRS();
                            //按照组里人员的级别排序
                            msql = "select a.name,a.agentcode,b.agentgrade from LAAgent a,LATree b where a.EmployDate<='" +
                                   mDate[1]
                                   + "' and a.agentcode=b.agentcode and exists (select 'X' from lawage where agentcode=a.agentcode and indexcalno='"
                                   + IndexCalNo + "' and state='1' and shouldmoney<>0) and exists (select 'X' from LABranchGroup where agentgroup=a.branchcode and BranchAttr='"
                                   + BranchAttr +
                                   "') order by a.branchcode asc,b.agentgrade desc ";
                            ExeSQL tExeSQL = new ExeSQL();
                            tSSRS = tExeSQL.execSQL(msql);
                            System.out.println("tSSRS.getMaxRow():" +
                                               tSSRS.getMaxRow());
                            for (int i = 1; i <= tSSRS.getMaxRow(); i++)
                            {
                                strArr = new String[9];
                                for (int j = 1; j <= tSSRS.getMaxCol(); j++)
                                {
                                    //各组员信息
                                    if (j == 1)
                                    {
                                        strArr[0] = "姓名:" + tSSRS.GetText(i, j);
                                    }
                                    //取姓名、代理人编码、职级
                                    if (j == 2)
                                    {
                                        String Agentcode = "";
                                        Agentcode = tSSRS.GetText(i, j);
                                        strArr[1] = tSSRS.GetText(i, j);
                                        String Agentgrade = tSSRS.GetText(i, 3);
                                        LDCodeDB tLDCodeDB = new LDCodeDB();
                                        tLDCodeDB.setCodeType("agentgrade");
                                        tLDCodeDB.setCode(Agentgrade);
                                        tLDCodeDB.getInfo();
                                        strArr[2] = "职级:" +
                                                tLDCodeDB.getCodeName();
                                        strArr[3] = "年月:" + IndexCalNo + "";
                                        tF1PrintVTS.addRow(strArr);
                                        strArr = new String[9];
                                        //计算工资
                                        SSRS aSSRS = new SSRS();
                                        String asql =
                                                "select varvalue from LAsysvar where vartype = 'wage'";
                                        ExeSQL zExeSQL = new ExeSQL();
                                        String aasql = zExeSQL.getOneValue(asql);
                                        String bsql =
                                                "select '创业扶持金:'||nvl(F01,0),'创业金:'||nvl(F02,0),'转正奖:'||nvl(F03,0)," +
                                                "'初年度佣金:'||nvl(F04,0),'续年度佣金:'||nvl(F05,0),'伯乐奖:'||nvl(F06,0)," +
                                                "'增员奖金:'||nvl(F08,0),'再次转正补佣:'||nvl(F21,0),'高级理财服务主任特别津贴:'||nvl(F10,0)," +
                                                "'职务津贴:'||nvl(F11,0),'直辖组管理津贴:'||nvl(F12,0),'组育成津贴:'||nvl(F13,0)," +
                                                "'部直辖管理津贴:'||nvl(F15,0),'增部津贴:'||nvl(F16,0),'总监职务底薪:'||nvl(F18,0)," +
                                                "'总监育成津贴:'||nvl(F19,0),'业务拓展津贴:'||nvl(W10,0),'资深总监职务底薪:'||nvl(F20,0)," +
                                                "'个人年终奖金:'||nvl(F07,0)," +
                                                "'奖惩款:'||nvl(k11,0),'加/扣款:'||nvl(F30,0)," +
                                                "'支助薪资(衔):'||nvl(F22,0),'招募资金(衔):'||nvl(F23,0),'创业奖(衔):'||nvl(F24,0)," +
                                                "'衔接资金加扣款:'||nvl(K12,0)," +
                                                "'底薪:'||nvl(K13,0),'社保费用支持金:'||nvl(K14,0),'费用补贴:'||nvl(W08,0)," +
                                                "'养老保险:'||nvl(K15,0),'失业保险:'||nvl(K19,0),'医疗保险:'||nvl(K17,0)," +
                                                "'工伤保险:'||nvl(W02,0),'住房公积金:'||nvl(W04,0),'加扣款3:'||nvl(W09,0)," +
                                                "'本期应发金额:'||nvl(shouldmoney,0),'上次佣金余额:'||nvl(lastmoney,0)," +
                                                "'扣税:'||nvl(K01+K02,0),'差勤扣款:'||nvl(K03,0),'本期实发金额:'||nvl(summoney,0)" +
                                                " from lawage where AgentCode='" +
                                                Agentcode +
                                                "' and IndexCalNo='" +
                                                IndexCalNo + "'";

                                        ExeSQL aExeSQL = new ExeSQL();
                                        aSSRS = aExeSQL.execSQL(bsql);
                                        //插入具体工资
                                        for (int a = 1; a <= aSSRS.getMaxRow();
                                                a++)
                                        {
                                            for (int m = 1;
                                                    m <= aSSRS.getMaxCol(); m++)
                                            {
                                                String Money = aSSRS.GetText(a,
                                                        m).substring(aSSRS.
                                                        GetText(a,
                                                        m).indexOf(":") + 1);
                                                Money = new DecimalFormat(
                                                        "0.00").format(Double.
                                                        valueOf(String.valueOf(
                                                        Money)));
                                                Money = PubFun.getInt(Money);
                                                String Money1 = aSSRS.GetText(a,
                                                        m).substring(0,
                                                        aSSRS.GetText(a, m).
                                                        indexOf(":")) + ":" +
                                                        Money;
                                                //将各项工资加和
                                                if (m != 4 && m != 5)
                                                {
                                                    wage[m] += Double.
                                                            parseDouble(Money);
                                                }
                                                if (m >= 1 && m <= 9)
                                                {
                                                    strArr[m - 1] = Money1;
                                                }
                                                if (m >= 10 && m <= 18)
                                                {
                                                    strArr[m - 10] = Money1;
                                                }
                                                if (m >= 19 && m <= 27)
                                                {
                                                    strArr[m - 19] = Money1;
                                                }
                                                if (m >= 28 && m <= 36)
                                                {
                                                    strArr[m - 28] = Money1;
                                                }
                                                if (m >= 37 && m <= 45)
                                                {
                                                    strArr[m - 37] = Money1;
                                                }
                                                if (m == 9 || m == 18 ||
                                                        m == 27 || m == 36 ||
                                                        m == aSSRS.getMaxCol())
                                                {
                                                    tF1PrintVTS.addRow(strArr);
                                                    strArr = new String[9];
                                                }
                                            }
                                        }
                                        System.out.println("保单信息");
                                        //保单信息
                                        String mDay[] = PubFun1.calFLDate(
                                                IndexCalNo.substring(0, 4) +
                                                "-" + IndexCalNo.substring(4, 6) +
                                                "-12");
                                        SSRS cSSRS = new SSRS();
                                        String csql = "select polno,p11,p13,riskcode,transmoney,decode(PayIntv,0,'1',payyears),paycount,FYCRate,fyc from LACommision where agentcode='" +
                                                Agentcode + "' and WageNo='" +
                                                IndexCalNo + "'";
                                        ExeSQL cExeSQL = new ExeSQL();
                                        cSSRS = cExeSQL.execSQL(csql);
                                        strArr[0] = "保单号";
                                        strArr[1] = "投保人";
                                        strArr[2] = "被保人";
                                        strArr[3] = "险种编码";
                                        strArr[4] = "保费";
                                        strArr[5] = "缴费期";
                                        strArr[6] = "缴费次数";
                                        strArr[7] = "佣金比例";
                                        strArr[8] = "佣金";
                                        tF1PrintVTS.addRow(strArr);
                                        strArr = new String[9];
                                        for (int b = 1; b <= cSSRS.getMaxRow();
                                                b++)
                                        {
                                            for (int n = 1;
                                                    n <= cSSRS.getMaxCol(); n++)
                                            {
                                                strArr[n -
                                                        1] = cSSRS.GetText(b, n);
                                            }
                                            tF1PrintVTS.addRow(strArr);
                                            strArr = new String[9];
                                        }
                                        strArr[0] = "";
                                        strArr[1] = "";
                                        strArr[2] = "";
                                        strArr[3] = "";
                                        strArr[4] = "";
                                        strArr[5] = "";
                                        strArr[6] = "";
                                        strArr[7] = "";
                                        strArr[8] = "";
                                        tF1PrintVTS.addRow(strArr);
                                        strArr = new String[9];
                                    }
                                }
                            }
                            String csql =
                                    "select nvl(sum(f04),0),nvl(sum(f05),0) from lawage where indexcalno='" +
                                    IndexCalNo
                                    + "' and state='1' and branchattr ='" +
                                    BranchAttr + "'";
                            ExeSQL cExeSQL = new ExeSQL();
                            SSRS nSSRS = new SSRS();
                            nSSRS = cExeSQL.execSQL(csql);
                            wage[4] = Double.parseDouble(nSSRS.GetText(1, 1));
                            wage[5] = Double.parseDouble(nSSRS.GetText(1, 2));
                            strArr = new String[9];
                            for (int i = 1; i <= 39; i++)
                            {
                                wage1[i] += wage[i]; //计算部薪资
                            }
                            strArr[0] = "组薪资合计";
                            strArr[1] = "年月:" + IndexCalNo + "";
                            tF1PrintVTS.addRow(strArr);
                            strArr = new String[9];
                            strArr[0] = "创业扶持金:" +
                                        new DecimalFormat("0.00").format(wage[1]);
                            strArr[1] = "创业金:" +
                                        new DecimalFormat("0.00").format(wage[2]);
                            strArr[2] = "转正奖:" +
                                        new DecimalFormat("0.00").format(wage[3]);
                            strArr[3] = "初年度佣金:" +
                                        new DecimalFormat("0.00").format(wage[4]);
                            strArr[4] = "续年度佣金:" +
                                        new DecimalFormat("0.00").format(wage[5]);
                            strArr[5] = "伯乐奖:" +
                                        new DecimalFormat("0.00").format(wage[6]);
                            strArr[6] = "育才奖金:" +
                                        new DecimalFormat("0.00").format(wage[7]);
                            strArr[7] = "再次转正补佣:" +
                                        new DecimalFormat("0.00").format(wage[8]);
                            strArr[8] = "高级理财服务主任特别津贴:" +
                                        new DecimalFormat("0.00").format(wage[9]);
                            tF1PrintVTS.addRow(strArr);
                            strArr = new String[9];
                            strArr[0] = "职务津贴:" +
                                        new DecimalFormat("0.00").
                                        format(wage[10]);
                            strArr[1] = "直辖组管理津贴:" +
                                        new DecimalFormat("0.00").
                                        format(wage[11]);
                            strArr[2] = "组育成津贴:" +
                                        new DecimalFormat("0.00").
                                        format(wage[12]);
                            strArr[3] = "部直辖管理津贴:" +
                                        new DecimalFormat("0.00").
                                        format(wage[13]);
                            strArr[4] = "增部津贴:" +
                                        new DecimalFormat("0.00").
                                        format(wage[14]);
                            strArr[5] = "总监职务底薪:" +
                                        new DecimalFormat("0.00").
                                        format(wage[15]);
                            strArr[6] = "总监育成津贴:" +
                                        new DecimalFormat("0.00").
                                        format(wage[16]);
                            strArr[7] = "业务拓展津贴:" +
                                        new DecimalFormat("0.00").
                                        format(wage[17]);
                            strArr[8] = "资深总监职务底薪:" +
                                        new DecimalFormat("0.00").
                                        format(wage[18]);
                            tF1PrintVTS.addRow(strArr);
                            strArr = new String[9];
                            strArr[0] = "个人年终奖:" +
                                        new DecimalFormat("0.00").
                                        format(wage[19]);
                            strArr[1] = "奖惩款:" +
                                        new DecimalFormat("0.00").
                                        format(wage[20]);
                            strArr[2] = "加/扣款:" +
                                        new DecimalFormat("0.00").
                                        format(wage[21]);
                            strArr[3] = "支助薪资:" +
                                        new DecimalFormat("0.00").
                                        format(wage[22]);
                            strArr[4] = "招募资金(衔):" +
                                        new DecimalFormat("0.00").
                                        format(wage[23]);
                            strArr[5] = "创业奖(衔):" +
                                        new DecimalFormat("0.00").
                                        format(wage[24]);
                            strArr[6] = "衔接资金加扣款:" +
                                        new DecimalFormat("0.00").
                                        format(wage[25]);
                            strArr[7] = "底薪:" +
                                        new DecimalFormat("0.00").
                                        format(wage[26]);
                            strArr[8] = "社保费用支持金:" +
                                        new DecimalFormat("0.00").
                                        format(wage[27]);
                            tF1PrintVTS.addRow(strArr);
                            strArr = new String[9];
                            strArr[0] = "费用补贴:" +
                                        new DecimalFormat("0.00").
                                        format(wage[28]);
                            strArr[1] = "养老保险:" +
                                        new DecimalFormat("0.00").
                                        format(wage[29]);
                            strArr[2] = "失业保险:" +
                                        new DecimalFormat("0.00").
                                        format(wage[30]);
                            strArr[3] = "医疗保险:" +
                                        new DecimalFormat("0.00").
                                        format(wage[31]);
                            strArr[4] = "工伤保险:" +
                                        new DecimalFormat("0.00").
                                        format(wage[32]);
                            strArr[5] = "住房公积金:" +
                                        new DecimalFormat("0.00").
                                        format(wage[33]);
                            strArr[6] = "加扣款3:" +
                                        new DecimalFormat("0.00").
                                        format(wage[34]);
                            strArr[7] = "本期应发金额:" +
                                        new DecimalFormat("0.00").
                                        format(wage[35]);
                            strArr[8] = "上次佣金余额:" +
                                        new DecimalFormat("0.00").
                                        format(wage[36]);
                            tF1PrintVTS.addRow(strArr);
                            strArr = new String[9];
                            strArr[0] = "扣税:" +
                                        new DecimalFormat("0.00").
                                        format(wage[37]);
                            strArr[1] = "差勤扣款:" +
                                        new DecimalFormat("0.00").
                                        format(wage[38]);
                            strArr[2] = "本期实发金额:" +
                                        new DecimalFormat("0.00").
                                        format(wage[39]);
                            tF1PrintVTS.addRow(strArr);
                            strArr = new String[9];
                            strArr[0] = "";
                            strArr[1] = "";
                            strArr[2] = "";
                            strArr[3] = "";
                            strArr[4] = "";
                            strArr[5] = "";
                            strArr[6] = "";
                            strArr[7] = "";
                            strArr[8] = "";
                            tF1PrintVTS.addRow(strArr);
                            strArr = new String[9];
                            wage = new double[40];
                        }
                        if (BranchLevel.equals("02"))
                        {
                            strArr = new String[9];
                            for (int i = 1; i <= 39; i++)
                            {
                                wage2[i] += wage1[i]; //计算区薪资
                            }
                            if (!dealDatabu(wage1))
                            {
                                return false;
                            }

                            wage1 = new double[40]; //清空
                            strArr[0] = xSSRS.GetText(p, q);
                            tF1PrintVTS.addRow(strArr); //插入部一级名
                        }
                        if (BranchLevel.equals("03"))
                        {
                            strArr = new String[9];
                            for (int i = 1; i <= 39; i++)
                            {
                                wage2[i] += wage1[i]; //计算区薪资
                            }
                            for (int i = 1; i <= 39; i++)
                            {
                                wage3[i] += wage2[i]; //计算督导区薪资
                            }
                            if (!dealDatabu(wage1))
                            {
                                return false;
                            }
                            wage1 = new double[40]; //清空
                            if (!dealDataqu(wage2))
                            {
                                return false;
                            }
                            wage2 = new double[40]; //清空
                            strArr[0] = xSSRS.GetText(p, q);
                            tF1PrintVTS.addRow(strArr); //插入区一级名
                        }
                        if (BranchLevel.equals("04"))
                        {
                            strArr = new String[9];
                            for (int i = 1; i <= 39; i++)
                            {
                                wage2[i] += wage1[i]; //计算区薪资
                            }
                            for (int i = 1; i <= 39; i++)
                            {
                                wage3[i] += wage2[i]; //计算督导区薪资
                            }
                            for (int i = 1; i <= 39; i++)
                            {
                                wage4[i] += wage3[i]; //计算支公司薪资
                            }
                            if (!dealDatabu(wage1))
                            {
                                return false;
                            }
                            wage1 = new double[40]; //清空

                            if (!dealDataqu(wage2))
                            {
                                return false;
                            }
                            wage2 = new double[40]; //清空

                            if (!dealDataduqu(wage3))
                            {
                                return false;
                            }
                            wage3 = new double[40]; //清空
                            strArr[0] = xSSRS.GetText(p, q);
                            tF1PrintVTS.addRow(strArr); //插入督导区一级名
                        }
                        if (branchattr1 == BranchAttr) //判断是否结束
                        {
                            if (BranchLevel1.equals("02") &&
                                BranchLevel.equals("01"))
                            {
                                for (int i = 1; i <= 39; i++)
                                {
                                    wage1[i] += wage[i]; //计算部薪资
                                }
                                for (int i = 1; i <= 39; i++)
                                {
                                    wage2[i] += wage1[i]; //计算区薪资
                                }
                                if (!dealDatabu(wage1))
                                {
                                    return false;
                                }
                            }
                            if (BranchLevel1.equals("03"))
                            {
                                if (BranchLevel.equals("01"))
                                {
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage1[i] += wage[i]; //计算部薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage2[i] += wage1[i]; //计算区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage3[i] += wage2[i]; //计算督导区薪资
                                    }
                                    if (!dealDatabu(wage1))
                                    {
                                        return false;
                                    }
                                    wage1 = new double[40]; //清空
                                    if (!dealDataqu(wage2))
                                    {
                                        return false;
                                    }
                                }
                                if (BranchLevel.equals("02"))
                                {
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage2[i] += wage1[i]; //计算区薪资
                                    }
                                    if (!dealDataqu(wage2))
                                    {
                                        return false;
                                    }
                                }
                            }
                            if (BranchLevel1.equals("04"))
                            {
                                if (BranchLevel.equals("01"))
                                {
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage1[i] += wage[i]; //计算区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage2[i] += wage1[i]; //计算区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage3[i] += wage2[i]; //计算区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage4[i] += wage3[i]; //计算支公司薪资
                                    }
                                    if (!dealDatabu(wage1))
                                    {
                                        return false;
                                    }
                                    wage1 = new double[40]; //清空

                                    if (!dealDataqu(wage2))
                                    {
                                        return false;
                                    }
                                    wage2 = new double[40]; //清空

                                    if (!dealDataduqu(wage3))
                                    {
                                        return false;
                                    }
                                }
                                if (BranchLevel.equals("02"))
                                {
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage1[i] += wage[i]; //计算部薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage2[i] += wage1[i]; //计算区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage3[i] += wage2[i]; //计算督导区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage4[i] += wage3[i]; //计算支公司薪资
                                    }
                                    if (!dealDataqu(wage2))
                                    {
                                        return false;
                                    }
                                    wage2 = new double[40]; //清空

                                    if (!dealDataduqu(wage3))
                                    {
                                        return false;
                                    }
                                }
                                if (BranchLevel.equals("03"))
                                {
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage1[i] += wage[i]; //计算区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage2[i] += wage1[i]; //计算区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage3[i] += wage2[i]; //计算区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage4[i] += wage3[i]; //计算支公司薪资
                                    }
                                    if (!dealDataduqu(wage3))
                                    {
                                        return false;
                                    }
                                }
                            }
                            if (BranchLevel1.equals("05"))
                            {
                                if (BranchLevel.equals("01"))
                                {
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage1[i] += wage[i]; //计算区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage2[i] += wage1[i]; //计算区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage3[i] += wage2[i]; //计算区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage4[i] += wage3[i]; //计算支公司薪资
                                    }
                                    if (!dealDatabu(wage1))
                                    {
                                        return false;
                                    }
                                    wage1 = new double[40]; //清空

                                    if (!dealDataqu(wage2))
                                    {
                                        return false;
                                    }
                                    wage2 = new double[40]; //清空

                                    if (!dealDataduqu(wage3))
                                    {
                                        return false;
                                    }

                                    strArr[0] = "支公司薪资合计";
                                    strArr[1] = "年月:" + IndexCalNo + "";
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "创业扶持金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[1]);
                                    strArr[1] = "创业金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[2]);
                                    strArr[2] = "转正奖:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[3]);
                                    strArr[3] = "初年度佣金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[4]);
                                    strArr[4] = "续年度佣金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[5]);
                                    strArr[5] = "伯乐奖:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[6]);
                                    strArr[6] = "育才奖金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[7]);
                                    strArr[7] = "再次转正补佣:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[8]);
                                    strArr[8] = "高级理财服务主任特别津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[9]);
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "职务津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[10]);
                                    strArr[1] = "直辖组管理津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[11]);
                                    strArr[2] = "组育成津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[12]);
                                    strArr[3] = "部直辖管理津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[13]);
                                    strArr[4] = "增部津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[14]);
                                    strArr[5] = "总监职务底薪:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[15]);
                                    strArr[6] = "总监育成津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[16]);
                                    strArr[7] = "业务拓展津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[17]);
                                    strArr[8] = "资深总监职务底薪:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[18]);
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "个人年终奖:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[19]);
                                    strArr[1] = "奖惩款:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[20]);
                                    strArr[2] = "加/扣款:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[21]);
                                    strArr[3] = "支助薪资:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[22]);
                                    strArr[4] = "招募资金(衔):" +
                                                new DecimalFormat("0.00").
                                                format(wage4[23]);
                                    strArr[5] = "创业奖(衔):" +
                                                new DecimalFormat("0.00").
                                                format(wage4[24]);
                                    strArr[6] = "衔接资金加扣款:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[25]);
                                    strArr[7] = "底薪:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[26]);
                                    strArr[8] = "社保费用支持金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[27]);
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "费用补贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[28]);
                                    strArr[1] = "养老保险:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[29]);
                                    strArr[2] = "失业保险:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[30]);
                                    strArr[3] = "医疗保险:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[31]);
                                    strArr[4] = "工伤保险:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[32]);
                                    strArr[5] = "住房公积金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[33]);
                                    strArr[6] = "加扣款3:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[34]);
                                    strArr[7] = "本期应发金额:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[35]);
                                    strArr[8] = "上次佣金余额:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[36]);
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "扣税:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[37]);
                                    strArr[1] = "差勤扣款:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[38]);
                                    strArr[2] = "本期实发金额:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[39]);
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "";
                                    strArr[1] = "";
                                    strArr[2] = "";
                                    strArr[3] = "";
                                    strArr[4] = "";
                                    strArr[5] = "";
                                    strArr[6] = "";
                                    strArr[7] = "";
                                    strArr[8] = "";
                                    tF1PrintVTS.addRow(strArr);
                                }
                                if (BranchLevel.equals("02"))
                                {
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage1[i] += wage[i]; //计算区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage2[i] += wage1[i]; //计算区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage3[i] += wage2[i]; //计算区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage4[i] += wage3[i]; //计算支公司薪资
                                    }
                                    if (!dealDataqu(wage2))
                                    {
                                        return false;
                                    }
                                    wage2 = new double[40]; //清空

                                    if (!dealDataduqu(wage3))
                                    {
                                        return false;
                                    }
                                    strArr[0] = "支公司薪资合计";
                                    strArr[1] = "年月:" + IndexCalNo + "";
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "创业扶持金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[1]);
                                    strArr[1] = "创业金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[2]);
                                    strArr[2] = "转正奖:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[3]);
                                    strArr[3] = "初年度佣金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[4]);
                                    strArr[4] = "续年度佣金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[5]);
                                    strArr[5] = "伯乐奖:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[6]);
                                    strArr[6] = "育才奖金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[7]);
                                    strArr[7] = "再次转正补佣:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[8]);
                                    strArr[8] = "高级理财服务主任特别津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[9]);
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "职务津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[10]);
                                    strArr[1] = "直辖组管理津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[11]);
                                    strArr[2] = "组育成津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[12]);
                                    strArr[3] = "部直辖管理津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[13]);
                                    strArr[4] = "增部津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[14]);
                                    strArr[5] = "总监职务底薪:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[15]);
                                    strArr[6] = "总监育成津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[16]);
                                    strArr[7] = "业务拓展津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[17]);
                                    strArr[8] = "资深总监职务底薪:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[18]);
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "个人年终奖:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[19]);
                                    strArr[1] = "奖惩款:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[20]);
                                    strArr[2] = "加/扣款:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[21]);
                                    strArr[3] = "支助薪资:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[22]);
                                    strArr[4] = "招募资金(衔):" +
                                                new DecimalFormat("0.00").
                                                format(wage4[23]);
                                    strArr[5] = "创业奖(衔):" +
                                                new DecimalFormat("0.00").
                                                format(wage4[24]);
                                    strArr[6] = "衔接资金加扣款:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[25]);
                                    strArr[7] = "底薪:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[26]);
                                    strArr[8] = "社保费用支持金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[27]);
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "费用补贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[28]);
                                    strArr[1] = "养老保险:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[29]);
                                    strArr[2] = "失业保险:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[30]);
                                    strArr[3] = "医疗保险:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[31]);
                                    strArr[4] = "工伤保险:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[32]);
                                    strArr[5] = "住房公积金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[33]);
                                    strArr[6] = "加扣款3:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[34]);
                                    strArr[7] = "本期应发金额:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[35]);
                                    strArr[8] = "上次佣金余额:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[36]);
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "扣税:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[37]);
                                    strArr[1] = "差勤扣款:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[38]);
                                    strArr[2] = "本期实发金额:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[39]);
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "";
                                    strArr[1] = "";
                                    strArr[2] = "";
                                    strArr[3] = "";
                                    strArr[4] = "";
                                    strArr[5] = "";
                                    strArr[6] = "";
                                    strArr[7] = "";
                                    strArr[8] = "";
                                    tF1PrintVTS.addRow(strArr);
                                }
                                if (BranchLevel.equals("03"))
                                {
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage1[i] += wage[i]; //计算区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage2[i] += wage1[i]; //计算区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage3[i] += wage2[i]; //计算区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage4[i] += wage3[i]; //计算支公司薪资
                                    }
                                    if (!dealDataduqu(wage3))
                                    {
                                        return false;
                                    }

                                    strArr[0] = "支公司薪资合计";
                                    strArr[1] = "年月:" + IndexCalNo + "";
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "创业扶持金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[1]);
                                    strArr[1] = "创业金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[2]);
                                    strArr[2] = "转正奖:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[3]);
                                    strArr[3] = "初年度佣金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[4]);
                                    strArr[4] = "续年度佣金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[5]);
                                    strArr[5] = "伯乐奖:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[6]);
                                    strArr[6] = "育才奖金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[7]);
                                    strArr[7] = "再次转正补佣:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[8]);
                                    strArr[8] = "高级理财服务主任特别津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[9]);
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "职务津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[10]);
                                    strArr[1] = "直辖组管理津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[11]);
                                    strArr[2] = "组育成津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[12]);
                                    strArr[3] = "部直辖管理津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[13]);
                                    strArr[4] = "增部津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[14]);
                                    strArr[5] = "总监职务底薪:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[15]);
                                    strArr[6] = "总监育成津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[16]);
                                    strArr[7] = "业务拓展津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[17]);
                                    strArr[8] = "资深总监职务底薪:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[18]);
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "个人年终奖:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[19]);
                                    strArr[1] = "奖惩款:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[20]);
                                    strArr[2] = "加/扣款:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[21]);
                                    strArr[3] = "支助薪资:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[22]);
                                    strArr[4] = "招募资金(衔):" +
                                                new DecimalFormat("0.00").
                                                format(wage4[23]);
                                    strArr[5] = "创业奖(衔):" +
                                                new DecimalFormat("0.00").
                                                format(wage4[24]);
                                    strArr[6] = "衔接资金加扣款:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[25]);
                                    strArr[7] = "底薪:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[26]);
                                    strArr[8] = "社保费用支持金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[27]);
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "费用补贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[28]);
                                    strArr[1] = "养老保险:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[29]);
                                    strArr[2] = "失业保险:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[30]);
                                    strArr[3] = "医疗保险:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[31]);
                                    strArr[4] = "工伤保险:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[32]);
                                    strArr[5] = "住房公积金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[33]);
                                    strArr[6] = "加扣款3:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[34]);
                                    strArr[7] = "本期应发金额:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[35]);
                                    strArr[8] = "上次佣金余额:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[36]);
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "扣税:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[37]);
                                    strArr[1] = "差勤扣款:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[38]);
                                    strArr[2] = "本期实发金额:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[39]);
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "";
                                    strArr[1] = "";
                                    strArr[2] = "";
                                    strArr[3] = "";
                                    strArr[4] = "";
                                    strArr[5] = "";
                                    strArr[6] = "";
                                    strArr[7] = "";
                                    strArr[8] = "";
                                    tF1PrintVTS.addRow(strArr);
                                }
                                if (BranchLevel.equals("04") ||
                                    BranchLevel.equals("05"))
                                {
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage1[i] += wage[i]; //计算区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage2[i] += wage1[i]; //计算区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage3[i] += wage2[i]; //计算区薪资
                                    }
                                    for (int i = 1; i <= 39; i++)
                                    {
                                        wage4[i] += wage3[i]; //计算支公司薪资
                                    }
                                    strArr[0] = "支公司薪资合计";
                                    strArr[1] = "年月:" + IndexCalNo + "";
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "创业扶持金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[1]);
                                    strArr[1] = "创业金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[2]);
                                    strArr[2] = "转正奖:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[3]);
                                    strArr[3] = "初年度佣金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[4]);
                                    strArr[4] = "续年度佣金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[5]);
                                    strArr[5] = "伯乐奖:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[6]);
                                    strArr[6] = "育才奖金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[7]);
                                    strArr[7] = "再次转正补佣:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[8]);
                                    strArr[8] = "高级理财服务主任特别津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[9]);
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "职务津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[10]);
                                    strArr[1] = "直辖组管理津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[11]);
                                    strArr[2] = "组育成津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[12]);
                                    strArr[3] = "部直辖管理津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[13]);
                                    strArr[4] = "增部津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[14]);
                                    strArr[5] = "总监职务底薪:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[15]);
                                    strArr[6] = "总监育成津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[16]);
                                    strArr[7] = "业务拓展津贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[17]);
                                    strArr[8] = "资深总监职务底薪:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[18]);
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "个人年终奖:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[19]);
                                    strArr[1] = "奖惩款:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[20]);
                                    strArr[2] = "加/扣款:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[21]);
                                    strArr[3] = "支助薪资:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[22]);
                                    strArr[4] = "招募资金(衔):" +
                                                new DecimalFormat("0.00").
                                                format(wage4[23]);
                                    strArr[5] = "创业奖(衔):" +
                                                new DecimalFormat("0.00").
                                                format(wage4[24]);
                                    strArr[6] = "衔接资金加扣款:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[25]);
                                    strArr[7] = "底薪:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[26]);
                                    strArr[8] = "社保费用支持金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[27]);
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "费用补贴:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[28]);
                                    strArr[1] = "养老保险:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[29]);
                                    strArr[2] = "失业保险:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[30]);
                                    strArr[3] = "医疗保险:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[31]);
                                    strArr[4] = "工伤保险:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[32]);
                                    strArr[5] = "住房公积金:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[33]);
                                    strArr[6] = "加扣款3:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[34]);
                                    strArr[7] = "本期应发金额:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[35]);
                                    strArr[8] = "上次佣金余额:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[36]);
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "扣税:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[37]);
                                    strArr[1] = "差勤扣款:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[38]);
                                    strArr[2] = "本期实发金额:" +
                                                new DecimalFormat("0.00").
                                                format(wage4[39]);
                                    tF1PrintVTS.addRow(strArr);
                                    strArr = new String[9];
                                    strArr[0] = "";
                                    strArr[1] = "";
                                    strArr[2] = "";
                                    strArr[3] = "";
                                    strArr[4] = "";
                                    strArr[5] = "";
                                    strArr[6] = "";
                                    strArr[7] = "";
                                    strArr[8] = "";
                                    tF1PrintVTS.addRow(strArr);
                                }
                            }
                        }
                    }
                }
            }
        }

        mResult.addElement(tF1PrintVTS);
        System.out.println("end");
        return true;
    }
}