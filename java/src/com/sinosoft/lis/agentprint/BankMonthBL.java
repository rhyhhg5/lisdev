package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class BankMonthBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private GlobalInput mGlobalInput = new GlobalInput();
    private String Month = "";
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    public BankMonthBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        Month = (String) cInputData.get(0);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "DepartOrderBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String strArr[] = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("BankMonth");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("Day", Month); //输入制表时间
        String mmDay[] = PubFun.calFLDate(Month.substring(0, 4) + "-" +
                                          Month.substring(4, 6) + "-01");
        String lastFDay = PubFun.calDate(mmDay[0], -1, "M", null);
        String mFDay[] = PubFun.calFLDate(lastFDay);
        strArr = new String[15];
        double data[] = null;
        data = new double[15];
        double data1[] = null;
        data1 = new double[15];
        String asql =
                "select trim(shortname),comcode from ldcom where length(trim(comcode))=4";
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(asql);
            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                for (int b = 1; b <= aSSRS.getMaxCol(); b++)
                {
                    if (b == 1)
                    { //分公司名
                        strArr[0] = aSSRS.GetText(a, 1);
                    }
                    if (b == 2)
                    {
                        //银行部分
                        String bsql =
                                "select trim(name),agentcom from lacom where banktype='01' and managecom like '" +
                                aSSRS.GetText(a, 2) + "%' order by agentcom";
                        SSRS bSSRS = new SSRS();
                        bSSRS = aExeSQL.execSQL(bsql);
                        for (int c = 1; c <= bSSRS.getMaxRow(); c++)
                        {
                            for (int d = 1; d <= bSSRS.getMaxCol(); d++)
                            {
                                if (d == 1)
                                {
                                    strArr[1] = bSSRS.GetText(c, 1);
                                }
                                if (d == 2)
                                {
                                    //渠道网点数:有销售资格、并统计网点合格率
                                    String fsql = "select count(agentcom) from Lacom where sellflag='Y' and CalFlag='Y' and AgentCom like '" +
                                                  bSSRS.GetText(c, 2) + "%'";
                                    strArr[2] = aExeSQL.getOneValue(fsql);
                                    if (strArr[2].equals("-1"))
                                    {
                                        strArr[2] = "0";
                                    }
                                    data[2] += Double.parseDouble(strArr[2]);
                                    //渠道组人力
                                    String hsql = "select count(distinct agentcode) from lacomtoagent where  RelaType='1' and AgentCom like '" +
                                                  bSSRS.GetText(c, 2) + "%'";
                                    strArr[3] = aExeSQL.getOneValue(hsql);
                                    data[3] += Double.parseDouble(strArr[3]);
                                    //规模保费
                                    String csql = "select nvl(sum(transmoney),0)/10000,nvl(sum(standprem),0)/10000 from Lacommision where tmakedate>='" +
                                                  mmDay[0] +
                                                  "' and tmakedate<='" +
                                                  mmDay[1] +
                                                  "' and AgentCom like '" +
                                                  bSSRS.GetText(c, 2) + "%'";
                                    SSRS cSSRS = new SSRS();
                                    cSSRS = aExeSQL.execSQL(csql);
                                    strArr[10] = cSSRS.GetText(1, 1);
                                    data[10] += Double.parseDouble(strArr[10]);
                                    //标准保费
                                    strArr[11] = cSSRS.GetText(1, 2);
                                    data[11] += Double.parseDouble(strArr[11]);
                                    //本月网均产能
                                    if (strArr[2].equals("0"))
                                    {
                                        strArr[4] = "0";
                                    }
                                    else
                                    {
                                        strArr[4] = String.valueOf(Double.
                                                parseDouble(strArr[10]) /
                                                Double.parseDouble(strArr[2]));
                                    }
                                    //月增长率,先取上月日期
                                    String dsql =
                                            "select nvl(sum(transmoney),0)/10000 from Lacommision where tmakedate>='" +
                                            mFDay[0] + "' and tmakedate<='" +
                                            mFDay[1] + "' and AgentCom like '" +
                                            bSSRS.GetText(c, 2) + "%'";
                                    String zeng = aExeSQL.getOneValue(dsql);
                                    if (zeng.equals("0"))
                                    {
                                        strArr[12] = "0";
                                    }
                                    else
                                    {
                                        strArr[12] = String.valueOf(Double.
                                                parseDouble(strArr[10]) /
                                                Double.parseDouble(zeng));
                                    }
                                    //计划指标
                                    String msql = "select nvl(sum(planvalue),0)/10000 from laplan where plantype='2' and branchtype='3' and planobject='" +
                                                  bSSRS.GetText(c, 2) +
                                                  "' and planperiod='" + Month +
                                                  "'";
                                    strArr[13] = aExeSQL.getOneValue(msql);
                                    //达成率
                                    if (strArr[13].equals("0"))
                                    {
                                        strArr[14] = "0";
                                    }
                                    else
                                    {
                                        strArr[14] = String.valueOf(Double.
                                                parseDouble(strArr[11]) /
                                                Double.parseDouble(strArr[13]));
                                    }
                                    //查询险种
                                    String ksql =
                                            "select distinct riskcode from lacommision where tmakedate<='" +
                                            mmDay[1] + "' and AgentCom like '" +
                                            bSSRS.GetText(c, 2) + "%'";
                                    SSRS dSSRS = new SSRS();
                                    dSSRS = aExeSQL.execSQL(ksql);
                                    for (int g = 1; g <= dSSRS.getMaxRow(); g++)
                                    {
                                        //查询对应银行下的不同险种和本月承保保费
                                        String jsql =
                                                "select nvl(sum(transmoney),0)/10000 from lacommision where tmakedate>='" +
                                                mmDay[0] + "' and tmakedate<='" +
                                                mmDay[1] +
                                                "' and AgentCom like '" +
                                                bSSRS.GetText(c, 2) +
                                                "%' and riskcode='" +
                                                dSSRS.GetText(1, g) + "'";
                                        strArr[6] = aExeSQL.getOneValue(jsql);
                                        data[6] += Double.parseDouble(strArr[6]);
                                        //险种名称，累计预收保费
                                        String aasql =
                                                "select riskname from LMRiskApp where riskcode='" +
                                                dSSRS.GetText(1, g) + "'";
                                        strArr[5] = aExeSQL.getOneValue(aasql);
                                        String esql =
                                                "select nvl(sum(prem),0)/10000 from Lcpol where uwflag<>'a' and makeDate>='" +
                                                Month.substring(0, 4) +
                                                "-01-01" + "' and makeDate<='" +
                                                mmDay[1] +
                                                "' and managecom like '" +
                                                aSSRS.GetText(a, 2) +
                                                "%'  and AgentCom like '" +
                                                bSSRS.GetText(c, 2) +
                                                "%' and riskcode='" +
                                                dSSRS.GetText(1, g) + "'";
                                        strArr[8] = aExeSQL.getOneValue(esql);
                                        data[8] += Double.parseDouble(strArr[8]);
                                        //累计承保保费
                                        String gsql =
                                                "select nvl(sum(transmoney),0)/10000 from lacommision where tmakedate>='" +
                                                Month.substring(0, 4) +
                                                "-01-01" + "' and tmakedate<='" +
                                                mmDay[1] +
                                                "' and AgentCom like '" +
                                                bSSRS.GetText(c, 2) +
                                                "%' and riskcode='" +
                                                dSSRS.GetText(1, g) + "'";
                                        strArr[7] = aExeSQL.getOneValue(gsql);
                                        data[7] += Double.parseDouble(strArr[7]);
                                        //累计退保保费
                                        String isql =
                                                "select nvl(sum(transmoney),0)/10000 from lacommision where tmakeDate>='" +
                                                Month.substring(0, 4) +
                                                "-01-01" + "' and tmakeDate<='" +
                                                mmDay[1] +
                                                "' and AgentCom like '" +
                                                bSSRS.GetText(c, 2) +
                                                "%' and riskcode='" +
                                                dSSRS.GetText(1, g) +
                                                "' and transmoney<'0'";
                                        strArr[9] = aExeSQL.getOneValue(isql);
                                        data[9] += Double.parseDouble(strArr[9]);
                                        if (g != 1)
                                        {
                                            strArr[10] = "";
                                            strArr[11] = "";
                                            strArr[12] = "";
                                            strArr[13] = "";
                                            strArr[14] = "";
                                            strArr[4] = "";
                                        }
                                        tlistTable.add(strArr);
                                        strArr = new String[15];
                                    }
                                }
                            }
                        }
                        //兼业、专业代理部分
                        for (int z = 0; z < 2; z++)
                        {
                            if (z == 0)
                            {
                                strArr = new String[15];
                                strArr[1] = "专业机构";
                                //渠道网点数:有销售资格、并统计
                                String fsql = "select count(agentcom) from Lacom where banktype is null and sellflag='Y' and managecom like '" +
                                              aSSRS.GetText(a, 2) +
                                              "%' and AgentCom like '20%'";
                                strArr[2] = aExeSQL.getOneValue(fsql);
                                data[2] += Double.parseDouble(strArr[2]);
                                //渠道组人力
                                String hsql = "select count(distinct agentcode) from lacomtoagent where RelaType='1' and AgentCom like '20" +
                                              aSSRS.GetText(a, 2).substring(2,
                                        4) + "%' ";
                                strArr[3] = aExeSQL.getOneValue(hsql);
                                data[3] += Double.parseDouble(strArr[3]);
                                //规模保费
                                String csql = "select nvl(sum(transmoney),0)/10000,nvl(sum(standprem),0)/10000 from Lacommision where tmakedate>='" +
                                              mmDay[0] + "' and tmakedate<='" +
                                              mmDay[1] +
                                              "' and managecom like '" +
                                              aSSRS.GetText(a, 2) +
                                              "%' and AgentCom like '20%'";
                                SSRS cSSRS = new SSRS();
                                cSSRS = aExeSQL.execSQL(csql);
                                strArr[10] = cSSRS.GetText(1, 1);
                                data[10] += Double.parseDouble(strArr[10]);
                                //标准保费
                                strArr[11] = cSSRS.GetText(1, 2);
                                data[11] += Double.parseDouble(strArr[11]);
                                //本月网均产能
                                if (strArr[2].equals("0"))
                                {
                                    strArr[4] = "0";
                                }
                                else
                                {
                                    strArr[4] = String.valueOf(Double.
                                            parseDouble(strArr[10]) /
                                            Double.parseDouble(strArr[2]));
                                }
                                //月增长率,先取上月日期
                                String dsql =
                                        "select nvl(sum(transmoney),0)/10000 from Lacommision where tmakedate>='" +
                                        mFDay[0] + "' and tmakedate<='" +
                                        mFDay[1] + "' and managecom like '" +
                                        aSSRS.GetText(a, 2) +
                                        "%' and AgentCom like '20%'";
                                String zeng = aExeSQL.getOneValue(dsql);
                                if (zeng.equals("0"))
                                {
                                    strArr[12] = "0";
                                }
                                else
                                {
                                    strArr[12] = String.valueOf(Double.
                                            parseDouble(strArr[10]) /
                                            Double.parseDouble(zeng));
                                }
                                //计划指标
                                String msql =
                                        "select nvl(sum(planvalue),0)/10000 from laplan where planobject like '20" +
                                        aSSRS.GetText(a, 2).substring(2, 4) +
                                        "%' and plantype='2' and branchtype='3' and planperiod='" +
                                        Month + "'";
                                strArr[13] = aExeSQL.getOneValue(msql);
                                //达成率
                                if (strArr[13].equals("0"))
                                {
                                    strArr[14] = "0";
                                }
                                else
                                {
                                    strArr[14] = String.valueOf(Double.
                                            parseDouble(strArr[11]) /
                                            Double.parseDouble(strArr[13]));
                                }
                                //查询险种
                                String ksql =
                                        "select distinct riskcode from lacommision where tmakedate<='" +
                                        mmDay[1] + "' and managecom like '" +
                                        aSSRS.GetText(a, 2) +
                                        "%' and AgentCom like '20%'";
                                SSRS dSSRS = new SSRS();
                                dSSRS = aExeSQL.execSQL(ksql);
                                for (int g = 1; g <= dSSRS.getMaxRow(); g++)
                                {
                                    //查询对应银行下的不同险种和本月承保保费
                                    String jsql =
                                            "select nvl(sum(transmoney),0)/10000 from lacommision where tmakedate>='" +
                                            mmDay[0] + "' and tmakedate<='" +
                                            mmDay[1] +
                                            "'  and managecom like '" +
                                            aSSRS.GetText(a, 2) +
                                            "%' and AgentCom like '20%' and riskcode='" +
                                            dSSRS.GetText(1, g) + "'";
                                    strArr[6] = aExeSQL.getOneValue(jsql);
                                    data[6] += Double.parseDouble(strArr[6]);
                                    //险种名称，累计预收保费
                                    String aasql =
                                            "select riskname from LMRiskApp where riskcode='" +
                                            dSSRS.GetText(1, g) + "'";
                                    strArr[5] = aExeSQL.getOneValue(aasql);
                                    String esql =
                                            "select nvl(sum(prem),0)/10000 from Lcpol where uwflag<>'a' and makeDate>='" +
                                            Month.substring(0, 4) + "-01-01" +
                                            "' and makeDate<='" + mmDay[1] +
                                            "' and managecom like '" +
                                            aSSRS.GetText(a, 2) +
                                            "%' and riskcode='" +
                                            dSSRS.GetText(1, g) +
                                            "' and AgentCom like '20%'";
                                    strArr[8] = aExeSQL.getOneValue(esql);
                                    data[8] += Double.parseDouble(strArr[8]);
                                    //累计承保保费
                                    String gsql =
                                            "select nvl(sum(transmoney),0)/10000 from lacommision where tmakedate>='" +
                                            Month.substring(0, 4) + "-01-01" +
                                            "' and tmakedate<='" + mmDay[1] +
                                            "' and managecom like '" +
                                            aSSRS.GetText(a, 2) +
                                            "%' and AgentCom like '20%' and riskcode='" +
                                            dSSRS.GetText(1, g) + "'";
                                    strArr[7] = aExeSQL.getOneValue(gsql);
                                    data[7] += Double.parseDouble(strArr[7]);
                                    //累计退保保费
                                    String isql =
                                            "select nvl(sum(transmoney),0)/10000 from lacommision where tmakeDate>='" +
                                            Month.substring(0, 4) + "-01-01" +
                                            "' and tmakeDate<='" + mmDay[1] +
                                            "' and managecom like '" +
                                            aSSRS.GetText(a, 2) +
                                            "%' and AgentCom like '20%' and riskcode='" +
                                            dSSRS.GetText(1, g) +
                                            "' and transmoney<'0'";
                                    strArr[9] = aExeSQL.getOneValue(gsql);
                                    data[9] += Double.parseDouble(strArr[9]);
                                    if (g != 1)
                                    {
                                        strArr[10] = "";
                                        strArr[11] = "";
                                        strArr[12] = "";
                                        strArr[13] = "";
                                        strArr[14] = "";
                                        strArr[4] = "";
                                    }
                                    tlistTable.add(strArr);
                                    strArr = new String[15];
                                }
                            }
                            else
                            {
                                strArr = new String[15];
                                strArr[1] = "兼业机构";
                                //渠道网点数:有销售资格、并统计
                                String fsql = "select count(agentcom) from Lacom where banktype is null and sellflag='Y' and managecom like '" +
                                              aSSRS.GetText(a, 2) +
                                              "%' and AgentCom like '21%'";
                                strArr[2] = aExeSQL.getOneValue(fsql);
                                data[2] += Double.parseDouble(strArr[2]);
                                //渠道组人力
                                String hsql = "select count(distinct agentcode) from lacomtoagent where RelaType='1' and AgentCom like '21" +
                                              aSSRS.GetText(a, 2).substring(2,
                                        4) + "%'";
                                strArr[3] = aExeSQL.getOneValue(hsql);
                                data[3] += Double.parseDouble(strArr[3]);
                                //规模保费
                                String csql = "select nvl(sum(transmoney),0)/10000,nvl(sum(standprem),0)/10000 from Lacommision where tmakedate>='" +
                                              mmDay[0] + "' and tmakedate<='" +
                                              mmDay[1] +
                                              "' and managecom like '" +
                                              aSSRS.GetText(a, 2) +
                                              "%' and AgentCom like '21%'";
                                SSRS cSSRS = new SSRS();
                                cSSRS = aExeSQL.execSQL(csql);
                                strArr[10] = cSSRS.GetText(1, 1);
                                data[10] += Double.parseDouble(strArr[10]);
                                //标准保费
                                strArr[11] = cSSRS.GetText(1, 2);
                                data[11] += Double.parseDouble(strArr[11]);
                                //本月网均产能
                                if (strArr[2].equals("0"))
                                {
                                    strArr[4] = "0";
                                }
                                else
                                {
                                    strArr[4] = String.valueOf(Double.
                                            parseDouble(strArr[10]) /
                                            Double.parseDouble(strArr[2]));
                                }
                                //月增长率,先取上月日期
                                String dsql =
                                        "select nvl(sum(transmoney),0)/10000 from Lacommision where tmakedate>='" +
                                        mFDay[0] + "' and tmakedate<='" +
                                        mFDay[1] + "' and managecom like '" +
                                        aSSRS.GetText(a, 2) +
                                        "%' and AgentCom like '21%'";
                                String zeng = aExeSQL.getOneValue(dsql);
                                if (zeng.equals("0"))
                                {
                                    strArr[12] = "0";
                                }
                                else
                                {
                                    strArr[12] = String.valueOf(Double.
                                            parseDouble(strArr[10]) /
                                            Double.parseDouble(zeng));
                                }
                                //计划指标
                                String msql =
                                        "select nvl(sum(planvalue),0)/10000 from laplan where planobject like '21" +
                                        aSSRS.GetText(a, 2).substring(2, 4) +
                                        "%' and plantype='2' and branchtype='3' and planperiod='" +
                                        Month + "'";
                                strArr[13] = aExeSQL.getOneValue(msql);
                                //达成率
                                if (strArr[13].equals("0"))
                                {
                                    strArr[14] = "0";
                                }
                                else
                                {
                                    strArr[14] = String.valueOf(Double.
                                            parseDouble(strArr[11]) /
                                            Double.parseDouble(strArr[13]));
                                }
                                //查询险种
                                String ksql =
                                        "select distinct riskcode from lacommision where tmakedate<='" +
                                        mmDay[1] + "' and managecom like '" +
                                        aSSRS.GetText(a, 2) +
                                        "%' and AgentCom like '21%'";
                                SSRS dSSRS = new SSRS();
                                dSSRS = aExeSQL.execSQL(ksql);
                                for (int g = 1; g <= dSSRS.getMaxRow(); g++)
                                {
                                    //查询对应银行下的不同险种和本月承保保费
                                    String jsql =
                                            "select nvl(sum(transmoney),0)/10000 from lacommision where tmakedate>='" +
                                            mmDay[0] + "' and tmakedate<='" +
                                            mmDay[1] + "' and managecom like '" +
                                            aSSRS.GetText(a, 2) +
                                            "%' and AgentCom like '21%' and riskcode='" +
                                            dSSRS.GetText(1, g) + "'";
                                    strArr[6] = aExeSQL.getOneValue(jsql);
                                    data[6] += Double.parseDouble(strArr[6]);
                                    //险种名称，累计预收保费
                                    String aasql =
                                            "select riskname from LMRiskApp where riskcode='" +
                                            dSSRS.GetText(1, g) + "'";
                                    strArr[5] = aExeSQL.getOneValue(aasql);
                                    String esql =
                                            "select nvl(sum(prem),0)/10000 from Lcpol where uwflag<>'a' and makeDate>='" +
                                            Month.substring(0, 4) + "-01-01" +
                                            "' and makeDate<='" + mmDay[1] +
                                            "' and managecom like '" +
                                            aSSRS.GetText(a, 2) +
                                            "%' and AgentCom like '21%' and riskcode='" +
                                            dSSRS.GetText(1, g) + "'";
                                    strArr[8] = aExeSQL.getOneValue(esql);
                                    data[8] += Double.parseDouble(strArr[8]);
                                    //累计承保保费
                                    String gsql =
                                            "select nvl(sum(transmoney),0)/10000 from lacommision where tmakedate>='" +
                                            Month.substring(0, 4) + "-01-01" +
                                            "' and tmakedate<='" + mmDay[1] +
                                            "'  and managecom like '" +
                                            aSSRS.GetText(a, 2) +
                                            "%' and AgentCom like '21%' and riskcode='" +
                                            dSSRS.GetText(1, g) + "'";
                                    strArr[7] = aExeSQL.getOneValue(gsql);
                                    data[7] += Double.parseDouble(strArr[7]);
                                    //累计退保保费
                                    String isql =
                                            "select nvl(sum(transmoney),0)/10000 from lacommision where tmakeDate>='" +
                                            Month.substring(0, 4) + "-01-01" +
                                            "' and tmakeDate<='" + mmDay[1] +
                                            "' and managecom like '" +
                                            aSSRS.GetText(a, 2) +
                                            "%' and AgentCom like '21%' and riskcode='" +
                                            dSSRS.GetText(1, g) +
                                            "' and transmoney<'0'";
                                    strArr[9] = aExeSQL.getOneValue(gsql);
                                    data[9] += Double.parseDouble(strArr[9]);
                                    if (g != 1)
                                    {
                                        strArr[10] = "";
                                        strArr[11] = "";
                                        strArr[12] = "";
                                        strArr[13] = "";
                                        strArr[14] = "";
                                        strArr[4] = "";
                                    }
                                    tlistTable.add(strArr);
                                    strArr = new String[15];
                                }
                            }
                        }
                        //小合计
                        strArr = new String[15];
                        strArr[0] = aSSRS.GetText(a, 1) + "合计";
                        for (int i = 2; i <= 3; i++)
                        {
                            strArr[i] = getInt(String.valueOf(data[i]));
                            data1[i] += data[i];
                        }
                        for (int i = 6; i <= 11; i++)
                        {
                            strArr[i] = String.valueOf(data[i]);
                            data1[i] += data[i];
                        }
                        strArr[13] = String.valueOf(data[13]);
                        data1[13] += data[13];
                        //本月网均产能
                        if (data[2] == 0)
                        {
                            strArr[4] = "0";
                        }
                        else
                        {
                            strArr[4] = String.valueOf(data[10] / data[2]);
                        }
                        //月增长率
                        String dsql =
                                "select nvl(sum(transmoney),0)/10000 from Lacommision where tmakedate>='" +
                                mFDay[0] + "' and tmakedate<='" + mFDay[1] +
                                "' and managecom like '" + aSSRS.GetText(a, 2) +
                                "%'";
                        String zeng = aExeSQL.getOneValue(dsql);
                        if (zeng.equals("0"))
                        {
                            strArr[12] = "0";
                        }
                        else
                        {
                            strArr[12] = String.valueOf(Double.parseDouble(
                                    strArr[10]) / Double.parseDouble(zeng));
                        }
                        //达成率
                        strArr[13] = String.valueOf(data[13]);
                        data1[13] += data[13];
                        if (data[13] == 0)
                        {
                            strArr[14] = "0";
                        }
                        else
                        {
                            strArr[14] = String.valueOf(data[11] / data[13]);
                        }
                        tlistTable.add(strArr);
                        strArr = new String[15];
                        data = new double[15];
                    }
                }
            }

            strArr = new String[15];
            strArr[0] = "总计";
            for (int i = 2; i <= 3; i++)
            {
                strArr[i] = getInt(String.valueOf(data1[i]));
            }
            for (int i = 6; i <= 11; i++)
            {
                strArr[i] = String.valueOf(data1[i]);
            }
            //本月网均产能
            if (data1[2] == 0)
            {
                strArr[4] = "0";
            }
            else
            {
                strArr[4] = String.valueOf(data1[10] / data1[2]);
            }
            //月增长率
            String dsql =
                    "select nvl(sum(transmoney),0)/10000 from Lacommision where tmakedate>='" +
                    mFDay[0] + "' and tmakedate<='" + mFDay[1] + "' ";
            String zeng = aExeSQL.getOneValue(dsql);
            if (zeng.equals("0"))
            {
                strArr[12] = "0";
            }
            else
            {
                strArr[12] = String.valueOf(Double.parseDouble(strArr[10]) /
                                            Double.parseDouble(zeng));
            }
            //达成率
            strArr[13] = String.valueOf(data1[13]);
            if (data1[13] == 0)
            {
                strArr[14] = "0";
            }
            else
            {
                strArr[14] = String.valueOf(data1[11] / data1[13]);
            }

            tlistTable.add(strArr);

            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("BankMonth.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankMonthBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    /*
       如果一个字符串数字中小数点后有值，则去掉小数点后面数值
     */
    public static String getInt(String Value)
    {
        String result = "";
        boolean mflag = true;
        int m = 0;
        m = Value.lastIndexOf(".");
        if (m == -1)
        {
            result = Value;
        }
        else
        {
            result = Value.substring(0, m);
        }
        return result;
    }

    public static void main(String[] args)
    {
    }
}