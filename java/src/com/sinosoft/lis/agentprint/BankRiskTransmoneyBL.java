package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class BankRiskTransmoneyBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private GlobalInput mGlobalInput = new GlobalInput();
    private String StartDay = "";
    private String EndDay = "";
    private String RiskCode = "";
    private String BankType = "";
    private String NetType = "";

    //全局变量
    private String strArr[] = null;
    private double data[] = null;
    private double data1[] = null;
    private ListTable tlistTable = new ListTable();


    //业务处理相关变量
    /** 全局数据 */
    public BankRiskTransmoneyBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        this.mResult.clear();
        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //取数据
        StartDay = (String) cInputData.get(0);
        EndDay = (String) cInputData.get(1);
        RiskCode = (String) cInputData.get(2);
        BankType = (String) cInputData.get(3);
        NetType = (String) cInputData.get(4);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 5));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "DepartOrderBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        ListTable tlistTable = new ListTable();
        tlistTable.setName("BankRiskTransmoney");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDay", StartDay + "---" + EndDay); //输入制表时间
        strArr = new String[8];
        double data[] = new double[8];
        double data1[] = new double[8];
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);

            //银行名称
            String asql = "select name,agentcom from lacom where banktype='" +
                          BankType + "' and managecom like '" +
                          mGlobalInput.ManageCom + "%'";
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(asql);
            int tCount = 0;
            tCount = aSSRS.getMaxRow();
            if (tCount == 0)
            {
                buildError("queryData", "没有银行信息！");
                return false;
            }
            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                for (int b = 1; b <= aSSRS.getMaxCol(); b++)
                {
                    if (b == 1)
                    {
                        strArr[0] = aSSRS.GetText(a, 1);
                    }
                    if (b == 2)
                    {
                        //网点名称
                        String bsql =
                                "select name,agentcom from lacom where banktype='" +
                                NetType + "' and upagentcom = '" +
                                aSSRS.GetText(a, 2) + "' order by agentcom";
                        SSRS bSSRS = new SSRS();
                        bSSRS = aExeSQL.execSQL(bsql);
                        if (bSSRS.getMaxRow() == 0)
                        {
                            continue;
                        }
                        for (int e = 1; e <= bSSRS.getMaxRow(); e++)
                        {
                            for (int f = 1; f <= bSSRS.getMaxCol(); f++)
                            {
                                if (f == 1)
                                {
                                    strArr[1] = bSSRS.GetText(e, 1);
                                }
                                if (f == 2)
                                {
                                    //险种名称
                                    if (RiskCode.equals("") || RiskCode == null)
                                    {
                                        String csql = "select trim(riskname),riskcode from lmriskapp where riskcode in (select riskcode from lacommision where  tmakeDate>='" +
                                                StartDay + "' and tmakeDate<='" +
                                                EndDay +
                                                "' and agentcom like '" +
                                                bSSRS.GetText(e, 2) + "%')";
                                        SSRS cSSRS = new SSRS();
                                        cSSRS = aExeSQL.execSQL(csql);
                                        for (int c = 1; c <= cSSRS.getMaxRow();
                                                c++)
                                        {
                                            for (int d = 1;
                                                    d <= cSSRS.getMaxCol(); d++)
                                            {
                                                if (d == 1)
                                                {
                                                    strArr[2] = cSSRS.GetText(c,
                                                            1);
                                                }
                                                if (d == 2)
                                                {
                                                    //预收保费
                                                    String dsql =
                                                            "select nvl(sum(prem),0) from lcpol where  makeDate>='" +
                                                            StartDay +
                                                            "' and makeDate<='" +
                                                            EndDay +
                                                            "' and uwflag<>'a' and agentcom like '" +
                                                            bSSRS.GetText(e, 2) +
                                                            "%' and riskcode='" +
                                                            cSSRS.GetText(c, 2) +
                                                            "'";
                                                    strArr[3] = aExeSQL.
                                                            getOneValue(dsql);
                                                    data[3] += Double.
                                                            parseDouble(strArr[
                                                            3].trim());
                                                    //实收保费
                                                    String esql =
                                                            "select nvl(sum(transmoney),0) from lacommision where  tmakeDate>='" +
                                                            StartDay +
                                                            "' and tmakeDate<='" +
                                                            EndDay +
                                                            "' and agentcom like '" +
                                                            bSSRS.GetText(e, 2) +
                                                            "%' and riskcode='" +
                                                            cSSRS.GetText(c, 2) +
                                                            "'";
                                                    strArr[4] = aExeSQL.
                                                            getOneValue(esql);
                                                    data[4] += Double.
                                                            parseDouble(strArr[
                                                            4].trim());
                                                    //活动网点数
                                                    String fsql =
                                                            "select count(distinct agentcom) from lacommision where tmakeDate>='" +
                                                            StartDay +
                                                            "' and tmakeDate<='" +
                                                            EndDay +
                                                            "'  and agentcom like '" +
                                                            bSSRS.GetText(e, 2) +
                                                            "%' and riskcode='" +
                                                            cSSRS.GetText(c, 2) +
                                                            "'";
                                                    strArr[5] = aExeSQL.
                                                            getOneValue(fsql);
                                                    data[5] += Double.
                                                            parseDouble(strArr[
                                                            5].trim());
                                                    //保单件数
                                                    String gsql =
                                                            "select nvl(sum(calcount),0) from lacommision where tmakeDate>='" +
                                                            StartDay +
                                                            "' and tmakeDate<='" +
                                                            EndDay +
                                                            "' and agentcom like '" +
                                                            bSSRS.GetText(e, 2) +
                                                            "%' and riskcode='" +
                                                            cSSRS.GetText(c, 2) +
                                                            "'";
                                                    strArr[6] = aExeSQL.
                                                            getOneValue(gsql);
                                                    data[6] += Double.
                                                            parseDouble(strArr[
                                                            6].trim());
                                                    //撤单件数
                                                    String hsql =
                                                            "select nvl(sum(calcount),0) from lacommision where tmakeDate>='" +
                                                            StartDay +
                                                            "' and tmakeDate<='" +
                                                            EndDay +
                                                            "' and transmoney<0 and agentcom like '" +
                                                            bSSRS.GetText(e, 2) +
                                                            "%' and riskcode='" +
                                                            cSSRS.GetText(c, 2) +
                                                            "'";
                                                    strArr[7] = aExeSQL.
                                                            getOneValue(hsql);
                                                    data[7] += Double.
                                                            parseDouble(strArr[
                                                            7].trim());
                                                    tlistTable.add(strArr);
                                                    strArr = new String[8];
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        String nsql = "select trim(riskname) from lmriskapp a,lacommision b where a.riskcode=b.riskcode and tmakeDate>='" +
                                                StartDay + "' and tmakeDate<='" +
                                                EndDay +
                                                "' and agentcom like '" +
                                                bSSRS.GetText(e, 2) +
                                                "%' and a.riskcode='" +
                                                RiskCode + "'";
                                        strArr[2] = aExeSQL.getOneValue(nsql);
                                        //预收保费
                                        String osql =
                                                "select nvl(sum(prem),0) from lcpol where  makeDate>='" +
                                                StartDay + "' and makeDate<='" +
                                                EndDay +
                                                "'  and uwflag<>'a' and agentcom like '" +
                                                bSSRS.GetText(e, 2) +
                                                "%' and riskcode='" + RiskCode +
                                                "'";
                                        strArr[3] = aExeSQL.getOneValue(osql);
                                        data[3] +=
                                                Double.parseDouble(strArr[3].
                                                trim());
                                        //实收保费
                                        String psql =
                                                "select nvl(sum(transmoney),0) from lacommision where  tmakeDate>='" +
                                                StartDay + "' and tmakeDate<='" +
                                                EndDay +
                                                "' and agentcom like '" +
                                                bSSRS.GetText(e, 2) +
                                                "%' and riskcode='" + RiskCode +
                                                "'";
                                        strArr[4] = aExeSQL.getOneValue(psql);
                                        data[4] +=
                                                Double.parseDouble(strArr[4].
                                                trim());
                                        //活动网点数
                                        String qsql =
                                                "select nvl(count(distinct agentcom),0) from lacommision where tmakeDate>='" +
                                                StartDay + "' and tmakeDate<='" +
                                                EndDay +
                                                "'  and agentcom like '" +
                                                bSSRS.GetText(e, 2) +
                                                "%' and riskcode='" + RiskCode +
                                                "'";
                                        strArr[5] = aExeSQL.getOneValue(qsql);
                                        data[5] +=
                                                Double.parseDouble(strArr[5].
                                                trim());
                                        //保单件数
                                        String rsql =
                                                "select nvl(sum(calcount),0) from lacommision where tmakeDate>='" +
                                                StartDay + "' and tmakeDate<='" +
                                                EndDay +
                                                "' and agentcom like '" +
                                                bSSRS.GetText(e, 2) +
                                                "%' and riskcode='" + RiskCode +
                                                "'";
                                        strArr[6] = aExeSQL.getOneValue(rsql);
                                        data[6] +=
                                                Double.parseDouble(strArr[6].
                                                trim());
                                        //撤单件数
                                        String tsql = "select nvl(sum(calcount),0) from lacommision where transmoney<0 and tmakeDate>='" +
                                                StartDay + "' and tmakeDate<='" +
                                                EndDay +
                                                "' and agentcom like '" +
                                                bSSRS.GetText(e, 2) +
                                                "%' and riskcode='" + RiskCode +
                                                "'";
                                        strArr[7] = aExeSQL.getOneValue(tsql);
                                        data[7] +=
                                                Double.parseDouble(strArr[7].
                                                trim());
                                        tlistTable.add(strArr);
                                        strArr = new String[8];
                                    }
                                }
                            }
                        }

                        strArr = new String[8];
                        strArr[0] = "小计";
                        for (int i = 3; i <= 7; i++)
                        {
                            strArr[i] = String.valueOf(data[i]);
                            data1[i] += data[i];
                        }
                        data = new double[8];
                        tlistTable.add(strArr);
                        strArr = new String[8];
                    }
                }
            }

            //大总计
            strArr = new String[8];
            strArr[0] = "汇总";
            for (int i = 3; i <= 7; i++)
            {
                strArr[i] = String.valueOf(data1[i]);
            }
            tlistTable.add(strArr);

            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("BankRiskTransmoney.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
            xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "HeadPolCounterF";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    /*
       如果一个字符串数字中小数点后有值，则去掉小数点后面数值
     */
    public static String getInt(String Value)
    {
        String result = "";
        boolean mflag = true;
        int m = 0;
        m = Value.lastIndexOf(".");
        if (m == -1)
        {
            result = Value;
        }
        else
        {
            result = Value.substring(0, m);
        }
        return result;
    }

    public static void main(String[] args)
    {
    }
}