package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAIndexTempUI
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mManageCom;
    private String mAgentCode;
    private String mAgentGrade;
    private String mStartDate;
    private String mEndDate;
    private String mBranchType;
    public LAIndexTempUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }

            // 准备传往后台的数据
            VData vData = new VData();

            if (!prepareOutputData(vData))
            {
                return false;
            }

            LAIndexTempBL tLAIndexTempBL = new LAIndexTempBL();
            System.out.println("Start LAIndexTempUI Submit ...");

            if (!tLAIndexTempBL.submitData(vData, cOperate))
            {
                if (tLAIndexTempBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tLAIndexTempBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData", "LAIndexTempBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tLAIndexTempBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CError cError = new CError();
            cError.moduleName = "LAIndexTempUI";
            cError.functionName = "submit";
            cError.errorMessage = e.toString();
            mErrors.addOneError(cError);
            return false;
        }
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.add(mManageCom);
            vData.add(mAgentCode);
            vData.add(mAgentGrade);
            vData.add(mStartDate);
            vData.add(mEndDate);
            vData.add(this.mBranchType);
            vData.add(mGlobalInput);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        this.mManageCom = (String) cInputData.get(0);
        this.mAgentCode = (String) cInputData.get(1);
        this.mAgentGrade = (String) cInputData.get(2);
        this.mStartDate = (String) cInputData.get(3);
        this.mEndDate = (String) cInputData.get(4);
        this.mBranchType = (String) cInputData.get(5);

        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 6));
        System.out.println("start");
        System.out.println(mGlobalInput.Operator);
        System.out.println(mGlobalInput.ManageCom);

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LAIndexTempUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
        AgentTrussPF1PUI agentTrussPF1PUI1 = new AgentTrussPF1PUI();
    }
}