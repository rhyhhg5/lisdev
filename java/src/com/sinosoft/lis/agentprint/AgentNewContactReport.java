package com.sinosoft.lis.agentprint;

import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */

public class AgentNewContactReport
{
    public GlobalInput mGI = new GlobalInput();
    private String mStartDate = "";
    private String mEndDate = "";
    private String mStaticOrg = "";
    public VData mResult = new VData();
    public CErrors mErrors = new CErrors();

    public AgentNewContactReport()
    {
    }

    public static void main(String[] args)
    {
        AgentNewContactReport agentNewContactReport1 = new
                AgentNewContactReport();

        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86110000";
        tGI.ManageCom = "86";
        tGI.Operator = "aa";
        VData tInputData = new VData();
        tInputData.clear();
        tInputData.addElement(tGI);
        tInputData.add(0, "2003-12-1");
        tInputData.add(1, "2003-12-1");
        tInputData.add(2, "861100000101");

        if (!agentNewContactReport1.getInputData(tInputData))
        {
            System.out.println("-----getinputdata failed!------");
        }
        if (!agentNewContactReport1.prepareData())
        {
            System.out.println("-------prepareData failed!------");
        }

    }

    public boolean getInputData(VData tInputData)
    {
        try
        {
            this.mStartDate = (String) tInputData.getObjectByObjectName(
                    "String", 0);
            this.mEndDate = (String) tInputData.getObjectByObjectName("String",
                    1);
            this.mStaticOrg = (String) tInputData.getObjectByObjectName(
                    "String", 2);
            this.mGI = (GlobalInput) tInputData.getObjectByObjectName(
                    "GlobalInput", 0);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("getInputData", ex.toString());
            return false;
        }
        System.out.println("------------getInputData------------");
        System.out.println("统计起期：" + this.mStartDate);
        System.out.println("统计止期：" + this.mEndDate);
        System.out.println("统计机构：" + this.mStaticOrg);
        System.out.println("------------------------------------");
        return true;
    }

    public boolean prepareData()
    {
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        ExeSQL aExeSQL = new ExeSQL(conn);

        int tLength = this.mStaticOrg.trim().length();
        String tSql = "";
        System.out.println("---tLength = " + tLength);
//判断选取得打印出的展业机构
        switch (tLength)
        {
            case 2:
                tSql = "select comcode,shortname from ldcom where "
                       + "length(trim(comcode)) = 4 and comcode like '"
                       + this.mStaticOrg.trim() + "%'";
                break;
            case 4:
                tSql = "select comcode,shortname from ldcom where "
                       + "length(trim(comcode)) = 6 and comcode like '"
                       + this.mStaticOrg.trim() + "%'";
                break;
            case 6:
                tSql = "select comcode,shortname from ldcom where "
                       + "length(trim(comcode)) = 8 and comcode like '"
                       + this.mStaticOrg.trim() + "%'";
                break;
            case 8:
                tSql = "select branchattr,name from labranchgroup where "
                       +
                       " branchlevel='03' and branchtype = '1' and (state<>'1' or state is null)"
                       + " and trim(branchattr) like '" + this.mStaticOrg.trim() +
                       "%'";
                break;
            case 12:
                tSql = "select branchattr,name from labranchgroup where "
                       +
                       " branchlevel='02' and branchtype = '1' and (state<>'1' or state is null)"
                       + " and branchattr like '" + this.mStaticOrg.trim() +
                       "%'";
                break;
        }
        System.out.println("----tSql = " + tSql.trim());

        SSRS aSSRS = new SSRS();
        aSSRS = aExeSQL.execSQL(tSql);
        int tCount = aSSRS.getMaxRow();
        String[] strArr = null;
        ListTable tListTab = new ListTable();
        tListTab.setName("AgentNewContact");

        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        String tStaticDate = "";
        tStaticDate = this.mStartDate.trim() + " 至 " + this.mEndDate.trim();
        texttag.add("StaticDate", tStaticDate); //输入制表时间
        String sql = "select shortname from ldcom where comcode='" + mStaticOrg +
                     "'";
        texttag.add("StaticOrg", aExeSQL.getOneValue(sql));

        float tSumStartLabor = 0;
        float tSumEndLabor = 0;
        float tSumTransMoney = 0;
        float tSumStandPrem = 0;
        float tSumIntvMoney = 0;
        float tSumCalCount = 0;
        float tSumNotNull = 0;
        float tSumMinusMoney = 0;
        float tSumMinusCount = 0;

        System.out.println("---展业机构个数 tCount = " + tCount);
        for (int i = 1; i <= tCount; i++)
        {
            strArr = new String[14];
            SSRS bSSRS = new SSRS();
            String tBranchAttr = "";
            String tBranchName = "";
            tBranchAttr = aSSRS.GetText(i, 1);
            tBranchName = aSSRS.GetText(i, 2);
            //展业机构名
            strArr[0] = tBranchName.trim();

            //期初人力
            String tSql2 =
                    "select nvl(count(*),0) from labranchgroup b,laagent a where "
                    + "employdate <= '" + mStartDate +
                    "' and a.branchtype = '1'"
                    + " and (OutWorkDate is null or OutWorkDate>'" + mStartDate +
                    "')"
                    + " and a.branchcode=b.agentgroup and branchattr like '" +
                    tBranchAttr + "%'";
            //System.out.println("---tSql2 = " + tSql2);
            strArr[1] = aExeSQL.getOneValue(tSql2).trim();
            tSumStartLabor += Float.parseFloat(strArr[1].trim());

            //期末人力
            tSql2 =
                    "select nvl(count(*),0) from laagent a,labranchgroup b where "
                    + " employdate <= '" + mEndDate +
                    "' and a.branchtype = '1'"
                    + " and (OutWorkDate is null or OutWorkDate>'" + mEndDate +
                    "')"
                    + " and a.branchcode=b.agentgroup and branchattr like '" +
                    tBranchAttr + "%'";
            //System.out.println("---tSql2 = " + tSql2);
            strArr[2] = aExeSQL.getOneValue(tSql2).trim();
            tSumEndLabor += Float.parseFloat(strArr[2].trim());

            //规模保费、标准保费、件数
            tSql2 = "select nvl(sum(transmoney)/10000,0),nvl(sum(standprem)/10000,0),nvl(sum(calcount),0) from lacommision where "
                    + " branchtype='1' and (paycount=0 or paycount=1)"
                    + " and tmakedate >= '" + this.mStartDate.trim()
                    + "' and tmakedate <= '" + this.mEndDate.trim() + "'"
                    + " and trim(branchattr) like '" + tBranchAttr.trim() +
                    "%'";
            System.out.println("ttt--operator--time:" + mGI.Operator +
                               PubFun.getCurrentDate() + PubFun.getCurrentTime());
            System.out.println("ttt--tSql2 = " + tSql2);
            bSSRS = aExeSQL.execSQL(tSql2);
            strArr[3] = bSSRS.GetText(1, 1).trim();
            strArr[4] = bSSRS.GetText(1, 2).trim();
            strArr[6] = bSSRS.GetText(1, 3).trim();
            tSumTransMoney += Float.parseFloat(strArr[3].trim());
            tSumStandPrem += Float.parseFloat(strArr[4].trim());
            tSumCalCount += Float.parseFloat(strArr[6].trim());

            //期交占比
            tSql2 =
                    "select nvl(sum(transmoney)/10000,0) from lacommision where branchtype='1' and "
                    + " tmakedate >= '" + this.mStartDate.trim()
                    + "' and tmakedate <= '" + this.mEndDate.trim()
                    + "' and payintv <> 0 and (paycount=0 or paycount=1) and "
                    + " trim(branchattr) like '" + tBranchAttr.trim() + "%'";
            float tIntvlMoney = Float.parseFloat(aExeSQL.getOneValue(tSql2).
                                                 trim());
            if (strArr[3].trim().equals("") || strArr[3] == null ||
                strArr[3].trim().equals("0"))
            {
                strArr[5] = "0";
            }
            else
            {
                strArr[5] = Float.toString(tIntvlMoney /
                                           Float.parseFloat(strArr[3].trim()));
            }
            tSumIntvMoney += tIntvlMoney;

            //人均标保
            float tAvg = 0;
            tAvg = (Float.parseFloat(strArr[1]) +
                    Float.parseFloat(strArr[2].trim())) / 2;
            if (tAvg == 0)
            {
                strArr[7] = "0";
                strArr[8] = "0";
            }
            else
            {
                strArr[7] = Float.toString(Float.parseFloat(strArr[4].trim()) *
                                           10000 / tAvg);
                //人均件数
                strArr[8] = Float.toString(Float.parseFloat(strArr[6].trim()) /
                                           tAvg);
            }

            //件均标保
            if (strArr[6].trim().equals("") || strArr[6] == null ||
                strArr[6].trim().equals("0"))
            {
                strArr[9] = "0";
            }
            else
            {
                strArr[9] = Float.toString(Float.parseFloat(strArr[4].trim()) /
                                           Float.parseFloat(strArr[6].trim()));
            }

            //活动率
            tSql2 = "select nvl(sum(count(distinct agentcode)),0) from lacommision where branchtype='1' and (paycount=0 or paycount=1) and "
                    + "tmakedate >= '" + this.mStartDate.trim() +
                    "' and tmakedate <= '"
                    + this.mEndDate.trim() + "' and branchattr like '"
                    + tBranchAttr.trim()
                    + "%' group by agentcode having sum(transmoney)>0";
            //有业绩的人数
            float tNotNull = 0;
            String notnull = aExeSQL.getOneValue(tSql2);
            if (notnull.trim().equals("") || notnull == null)
            {
                tNotNull = 0;
            }
            else
            {
                tNotNull = Float.parseFloat(notnull.trim());
            }
            if (tAvg == 0)
            {
                strArr[10] = "0";
            }
            else
            {
                strArr[10] = Float.toString(tNotNull / tAvg).trim();
            }
            tSumNotNull += tNotNull;

            //人均产能
            if (tNotNull == 0)
            {
                strArr[11] = "0";
            }
            else
            {
                strArr[11] = Float.toString(Float.parseFloat(strArr[4].trim()) *
                                            10000 / tNotNull).trim();
            }

            //撤单保费、件数
            tSql2 = "select nvl(sum(transmoney)/10000,0),nvl(sum(calcount),0) from lacommision where "
                    + " branchtype='1'"
                    + " and tmakedate >= '" + this.mStartDate.trim()
                    + "' and tmakedate <= '" + this.mEndDate.trim()
                    + "' and transmoney < 0 "
                    + " and trim(branchattr) like '" + tBranchAttr.trim() +
                    "%'";
//      System.out.println("ttt---tSql2 = " + tSql2);
            bSSRS = aExeSQL.execSQL(tSql2);
            strArr[12] = bSSRS.GetText(1, 1).trim();
            strArr[13] = bSSRS.GetText(1, 2).trim();
            tSumMinusMoney += Float.parseFloat(strArr[12].trim());
            tSumMinusCount += Float.parseFloat(strArr[13].trim());

            tListTab.add(strArr);
        }

        try
        {
            conn.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        //合计
        strArr = new String[14];
        strArr[0] = "合计";
        strArr[1] = Float.toString(tSumStartLabor).trim();
        strArr[2] = Float.toString(tSumEndLabor).trim();
        strArr[3] = Float.toString(tSumTransMoney).trim();
        strArr[4] = Float.toString(tSumStandPrem).trim();
        if (tSumTransMoney == 0)
        {
            strArr[5] = "0";
        }
        else
        {
            strArr[5] = Float.toString(tSumIntvMoney / tSumTransMoney).trim();
        }
        strArr[6] = Float.toString(tSumCalCount).trim();

        float tSumAvg = (tSumStartLabor + tSumEndLabor) / 2;

        if (tSumAvg == 0)
        {
            strArr[7] = "0";
            strArr[8] = "0";
            strArr[10] = "0";
        }
        else
        {
            strArr[7] = Float.toString(tSumStandPrem * 10000 / tSumAvg).trim();
            strArr[8] = Float.toString(tSumCalCount / tSumAvg).trim();
            strArr[10] = Float.toString(tSumNotNull / tSumAvg).trim();
        }
        if (tSumCalCount == 0)
        {
            strArr[9] = "0";
        }
        else
        {
            strArr[9] = Float.toString(tSumStandPrem / tSumCalCount).trim();
        }

        if (tSumNotNull == 0)
        {
            strArr[11] = "0";
        }
        else
        {
            strArr[11] = Float.toString(tSumStandPrem * 10000 / tSumNotNull).
                         trim();
        }
        strArr[12] = Float.toString(tSumMinusMoney).trim();
        strArr[13] = Float.toString(tSumMinusCount).trim();
        tListTab.add(strArr);

        XmlExport xmlexport = new XmlExport();
        xmlexport.createDocument("AgentNewContact.vts", "printer"); //最好紧接着就初始化xml文档
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tListTab, strArr);
//    xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
        this.mResult.addElement(xmlexport);
        System.out.println("------prepareData end-----");

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void dealError(String FuncName, String ErrMsg)
    {
        CError error = new CError();

        error.moduleName = "AgentNewContactReport";
        error.errorMessage = ErrMsg.trim();
        error.functionName = FuncName.trim();

        this.mErrors.addOneError(error);
    }
}