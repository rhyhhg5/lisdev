package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author  zy
 * @version 1.0
 */
import java.sql.Connection;
import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PolDayListBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private String mDay = "";
    private String mManageCom = "";
    double date2[] = new double[11]; //定义总计的数组

    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    public PolDayListBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        mDay = (String) cInputData.get(0);
        mManageCom = (String) cInputData.get(1);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "PolDayListBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String strArr[] = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("POLDAYLIST");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例

        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);

            String xsql = "select name from LABranchGroup where agentgroup= '" +
                          mManageCom + "' ";
            texttag.add("Name", aExeSQL.getOneValue(xsql));
            texttag.add("Day", mDay); //输入制表时间
            String mmDay[] = PubFun1.calFLDate(mDay);
            SSRS aSSRS = new SSRS();
            msql = "select branchattr,name from LABranchGroup where EndFlag<>'Y' and (state<>'1' or state is null) and BranchType = '1' and BranchLevel = '01' and  upbranch= '" +
                   mManageCom + "' order by agentgroup";
            aSSRS = aExeSQL.execSQL(msql);

            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                double date1[] = new double[11];
                for (int c = 1; c <= aSSRS.getMaxCol(); c++)
                {
                    if (c == 1)
                    {
                        SSRS bSSRS = new SSRS();
                        nsql = "select a.name,b.agentgrade,a.agentcode from LATree b,LAAgent a where a.agentcode=b.agentcode "
                               + "and a.agentstate <'03' and a.branchtype='1' and a.branchcode in (select agentgroup "
                               + "from LABranchGroup where BranchAttr like '" +
                               aSSRS.GetText(a, 1) +
                               "%') order by a.branchcode asc,b.agentgrade desc";
                        bSSRS = aExeSQL.execSQL(nsql);
                        for (int b = 1; b <= bSSRS.getMaxRow(); b++)
                        {
                            for (int d = 1; d <= bSSRS.getMaxCol(); d++)
                            {
                                if (d == 1)
                                { //展业机构：个人名称
                                    strArr = new String[13];
                                    strArr[0] = bSSRS.GetText(b, 1);
                                }
                                if (d == 2)
                                {
                                    //代理人职级
                                    strArr[2] = bSSRS.GetText(b, 2);
                                }
                                if (d == 3)
                                {
                                    //代理业务代码
                                    strArr[1] = bSSRS.GetText(b, 3);
                                    //日预收保费
                                    String bsql =
                                            "select NVL(sum(prem),0) from ljtempfee_lmriskapp2 where MakeDate ='" +
                                            mDay + "' and agentcode= '" +
                                            bSSRS.GetText(b, 3) + "'";
                                    strArr[3] = aExeSQL.getOneValue(bsql);
                                    date1[1] += Double.parseDouble(strArr[3]);
                                    String csql = "select calpieceYSpol('" +
                                                  mDay + "','" + mDay + "','" +
                                                  bSSRS.GetText(b, 3) +
                                                  "') from ldsysvar where sysvar='onerow'";
                                    //计算件数
                                    strArr[5] = aExeSQL.getOneValue(csql);
                                    date1[3] += Double.parseDouble(strArr[5]);
                                    //日预收标保
                                    String dsql = "select StandpremYSPol('" +
                                                  mDay + "','" + mDay + "','" +
                                                  bSSRS.GetText(b, 3) +
                                                  "') from ldsysvar where sysvar='onerow'";
                                    strArr[4] = aExeSQL.getOneValue(dsql);
                                    date1[2] += Double.parseDouble(strArr[4]);
                                    //日承保标保
                                    String esql = "select StandpremCBPol('" +
                                                  mDay + "','" + mDay + "','" +
                                                  bSSRS.GetText(b, 3) +
                                                  "') from ldsysvar where sysvar='onerow'";
                                    strArr[6] = aExeSQL.getOneValue(esql);
                                    date1[4] += Double.parseDouble(strArr[6]);
                                    //日承保件数
                                    String ksql = "select calpieceCBpol('" +
                                                  mDay + "','" + mDay + "','" +
                                                  bSSRS.GetText(b, 3) +
                                                  "') from ldsysvar where sysvar='onerow'";
                                    //计算承保件数
                                    strArr[7] = aExeSQL.getOneValue(ksql);
                                    date1[5] += Double.parseDouble(strArr[7]);
                                    //月预收保费
                                    String fsql =
                                            "select NVL(sum(prem),0) from ljtempfee_lmriskapp2 where MakeDate >='" +
                                            mmDay[0] + "' and MakeDate <='" +
                                            mDay + "' and agentcode= '" +
                                            bSSRS.GetText(b, 3) + "'";
                                    strArr[8] = aExeSQL.getOneValue(fsql);
                                    date1[6] += Double.parseDouble(strArr[8]);
                                    String gsql = "select calpieceYSpol('" +
                                                  mmDay[0] + "','" + mDay +
                                                  "','" + bSSRS.GetText(b, 3) +
                                                  "') from ldsysvar where sysvar='onerow'";
                                    //月预收保费件数
                                    strArr[10] = aExeSQL.getOneValue(gsql);
                                    date1[8] += Double.parseDouble(strArr[10]);
                                    //月预收标保
                                    String hsql = "select StandpremYSPol('" +
                                                  mmDay[0] + "','" + mDay +
                                                  "','" + bSSRS.GetText(b, 3) +
                                                  "') from ldsysvar where sysvar='onerow'";
                                    strArr[9] = aExeSQL.getOneValue(hsql);
                                    date1[7] += Double.parseDouble(strArr[9]);
                                    //月承保标保
                                    String isql = "select StandpremCBPol('" +
                                                  mmDay[0] + "','" + mDay +
                                                  "','" + bSSRS.GetText(b, 3) +
                                                  "') from ldsysvar where sysvar='onerow'";
                                    strArr[11] = aExeSQL.getOneValue(isql);
                                    date1[9] += Double.parseDouble(strArr[11]);
                                    //月承保件数
                                    String rsql = "select calpieceCBpol('" +
                                                  mmDay[0] + "','" + mDay +
                                                  "','" + bSSRS.GetText(b, 3) +
                                                  "') from ldsysvar where sysvar='onerow'";
                                    strArr[12] = aExeSQL.getOneValue(rsql);
                                    date1[10] += Double.parseDouble(strArr[12]);
                                    tlistTable.add(strArr);
                                }
                            }
                        }
                        //tjj add clear memory;
                        bSSRS = null;
                    }
                    if (c == 2)
                    {
                        //展业机构：组一级名
                        strArr = new String[13];
                        strArr[0] = aSSRS.GetText(a, 2);
                        strArr[1] = "";
                        strArr[2] = "";
                        strArr[3] = String.valueOf(date1[1]);
                        strArr[4] = String.valueOf(date1[2]);
                        strArr[5] = String.valueOf(date1[3]);
                        strArr[6] = String.valueOf(date1[4]);
                        strArr[7] = String.valueOf(date1[5]);
                        strArr[8] = String.valueOf(date1[6]);
                        strArr[9] = new DecimalFormat("0.00").format(Double.
                                valueOf(String.valueOf(date1[7])));
                        strArr[10] = String.valueOf(date1[8]);
                        strArr[11] = String.valueOf(date1[9]);
                        strArr[12] = String.valueOf(date1[10]);
                        tlistTable.add(strArr);
                        date2[1] += Double.parseDouble(strArr[3]);
                        date2[2] += Double.parseDouble(strArr[4]);
                        date2[3] += Double.parseDouble(strArr[5]);
                        date2[4] += Double.parseDouble(strArr[6]);
                        date2[5] += Double.parseDouble(strArr[7]);
                        date2[6] += Double.parseDouble(strArr[8]);
                        date2[7] += Double.parseDouble(strArr[9]);
                        date2[8] += Double.parseDouble(strArr[10]);
                        date2[9] += Double.parseDouble(strArr[11]);
                        date2[10] += Double.parseDouble(strArr[12]);
                        strArr = new String[13];
                        strArr[0] = "";
                        strArr[1] = "";
                        strArr[2] = "";
                        strArr[3] = "";
                        strArr[4] = "";
                        strArr[5] = "";
                        strArr[6] = "";
                        strArr[7] = "";
                        strArr[8] = "";
                        strArr[9] = "";
                        strArr[10] = "";
                        strArr[11] = "";
                        strArr[12] = "";
                        tlistTable.add(strArr);
                    }
                }
            }

            strArr = new String[13];
            strArr[0] = "营业部总计";
            strArr[1] = "";
            strArr[2] = "";
            strArr[3] = String.valueOf(date2[1]);
            strArr[4] = String.valueOf(date2[2]);
            strArr[5] = String.valueOf(date2[3]);
            strArr[6] = String.valueOf(date2[4]);
            strArr[7] = String.valueOf(date2[5]);
            strArr[8] = String.valueOf(date2[6]);
            strArr[9] = new DecimalFormat("0.00").format(Double.valueOf(String.
                    valueOf(date2[7])));
            strArr[10] = String.valueOf(date2[8]);
            strArr[11] = String.valueOf(date2[9]);
            strArr[12] = String.valueOf(date2[10]);
            tlistTable.add(strArr);
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("PolDayList.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PolDayListBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    public static void main(String[] args)
    {
    }
}