package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class BankChannelDayBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private GlobalInput mGlobalInput = new GlobalInput();
    private String StartDay = "";
    private String EndDay = "";
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    public BankChannelDayBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        StartDay = (String) cInputData.get(0);
        EndDay = (String) cInputData.get(1);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 2));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "BankChannelDayBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String strArr[] = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("BankChannelDay");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDay", StartDay); //输入制表时间
        texttag.add("EndDay", EndDay); //输入制表时间
        strArr = new String[12];
        double data[] = null;
        data = new double[12];
        double data1[] = null;
        data1 = new double[12];
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);
            //各个分公司
            String asql =
                    "select trim(shortname),comcode from ldcom where comcode like '" +
                    mGlobalInput.ManageCom.trim() +
                    "%' and length(trim(comcode))=4";
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(asql);
            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                for (int b = 1; b <= aSSRS.getMaxCol(); b++)
                {
                    if (b == 1)
                    {
                        strArr[0] = aSSRS.GetText(a, 1);
                    }
                    if (b == 2)
                    {
                        //银行、渠道
                        String bsql = "select trim(name),agentcom from lacom where (banktype='01' or banktype is null) and managecom like '" +
                                      aSSRS.GetText(a, 2) +
                                      "%' order by agentcom";
                        SSRS bSSRS = new SSRS();
                        bSSRS = aExeSQL.execSQL(bsql);
                        for (int c = 1; c <= bSSRS.getMaxRow(); c++)
                        {
                            for (int d = 1; d <= bSSRS.getMaxCol(); d++)
                            {
                                if (d == 1)
                                {
                                    strArr[1] = bSSRS.GetText(c, 1);
                                }
                                if (d == 2)
                                {
                                    //承保规模
                                    String fsql = "select nvl(sum(transmoney),0)/10000,nvl(sum(standprem),0)/10000 from lacommision where tmakeDate>='" +
                                                  StartDay +
                                                  "' and tmakeDate<='" + EndDay +
                                                  "' and AgentCom like '" +
                                                  bSSRS.GetText(c, 2) + "%'";
                                    SSRS zSSRS = new SSRS();
                                    zSSRS = aExeSQL.execSQL(fsql);
                                    strArr[8] = zSSRS.GetText(1, 1);
                                    strArr[9] = zSSRS.GetText(1, 2);
                                    data[8] += Double.parseDouble(strArr[8]);
                                    data[9] += Double.parseDouble(strArr[9]);
                                    //年度计划指标
                                    String gsql =
                                            "select nvl(sum(planvalue),0)/10000 from laplan where planobject='" +
                                            bSSRS.GetText(c, 2) +
                                            "' and plantype='2' and branchtype='3' and planperiod='" +
                                            EndDay.substring(0, 4) + "'";
                                    strArr[10] = aExeSQL.getOneValue(gsql);
                                    data[10] += Double.parseDouble(strArr[10]);
                                    //总达成
                                    if (strArr[10].equals("0"))
                                    {
                                        strArr[11] = "0";
                                    }
                                    else
                                    {
                                        strArr[11] = String.valueOf(Double.
                                                parseDouble(strArr[8]) /
                                                Double.parseDouble(strArr[10]));
                                    }
                                    data[11] += Double.parseDouble(strArr[11]);
                                    //查询险种
                                    String ksql = "select distinct riskcode from Lacommision where  branchtype='3' and tmakeDate<='" +
                                                  EndDay +
                                                  "' and AgentCom like '" +
                                                  bSSRS.GetText(c, 2) + "%'";
                                    SSRS dSSRS = new SSRS();
                                    dSSRS = aExeSQL.execSQL(ksql);
                                    for (int g = 1; g <= dSSRS.getMaxRow(); g++)
                                    {
                                        //查询对应银行下的不同险种,险种名称，预收保费
                                        String SQL =
                                                "select riskname from LMRiskApp where riskcode='" +
                                                dSSRS.GetText(1, g) + "'";
                                        strArr[2] = aExeSQL.getOneValue(SQL);
                                        String dsql = "select nvl(sum(prem),0)/10000,sum(m) from (select nvl(sum(prem),0) prem,count(*) m from Lcpol "
                                                +
                                                "where uwflag<>'a' and AgentCom like '" +
                                                bSSRS.GetText(c, 2) +
                                                "%' and makeDate>='" + StartDay +
                                                "' and makeDate<='" + EndDay +
                                                "' "
                                                + "and riskcode='" +
                                                dSSRS.GetText(1, g) +
                                                "' union select nvl(sum(prem),0) prem,count(*) m "
                                                +
                                                "from Lbpol where AgentCom like '" +
                                                bSSRS.GetText(c, 2) +
                                                "%' and makeDate>='" +
                                                StartDay +
                                                "' and makeDate<='" + EndDay +
                                                "' "
                                                + "and riskcode='" +
                                                dSSRS.GetText(1, g) + "')";
                                        SSRS mSSRS = new SSRS();
                                        mSSRS = aExeSQL.execSQL(dsql);
                                        strArr[3] = mSSRS.GetText(1, 1);
                                        data[3] += Double.parseDouble(strArr[3]);
                                        strArr[4] = mSSRS.GetText(1, 2);
                                        data[4] += Double.parseDouble(strArr[4]);
                                        //承保保费、承保件数
                                        String csql =
                                                "select nvl(sum(transmoney),0)/10000 from Lacommision where tmakeDate>='" +
                                                StartDay + "' and tmakeDate<='" +
                                                EndDay +
                                                "' and AgentCom like '" +
                                                bSSRS.GetText(c, 2) +
                                                "%' and riskcode='" +
                                                dSSRS.GetText(1, g) +
                                                "' and branchtype='3'";
                                        strArr[5] = aExeSQL.getOneValue(csql);
                                        data[5] += Double.parseDouble(strArr[5]);
                                        if (dSSRS.GetText(1, g).equals("312201"))
                                        {
                                            String esql =
                                                    "select count(distinct polno) from lacommision where tmakeDate>='" +
                                                    StartDay +
                                                    "' and tmakeDate<='" +
                                                    EndDay +
                                                    "' and AgentCom like '" +
                                                    bSSRS.GetText(c, 2) +
                                                    "%' and riskcode='" +
                                                    dSSRS.GetText(1, g) +
                                                    "' and branchtype='3'";
                                            strArr[6] = aExeSQL.getOneValue(
                                                    esql);
                                            data[6] +=
                                                    Double.parseDouble(strArr[6]);
                                        }
                                        else
                                        {
                                            strArr[6] = "";
                                            data[6] += 0;
                                        }
                                        //退保保费
                                        String isql =
                                                "select nvl(sum(transmoney),0)/10000 from lacommision where tmakeDate>='" +
                                                StartDay + "' and tmakeDate<='" +
                                                EndDay +
                                                "' and AgentCom like '" +
                                                bSSRS.GetText(c, 2) +
                                                "%' and riskcode='" +
                                                dSSRS.GetText(1, g) +
                                                "' and branchtype='3' and transtype='WT'";
                                        strArr[7] = aExeSQL.getOneValue(isql);
                                        data[7] += Double.parseDouble(strArr[7]);
                                        if (g != 1)
                                        {
                                            strArr[11] = "";
                                            strArr[8] = "";
                                            strArr[9] = "";
                                            strArr[10] = "";
                                        }
                                        tlistTable.add(strArr);
                                        strArr = new String[12];
                                    }
                                }
                            }
                        }
                        //小合计
                        strArr[0] = aSSRS.GetText(a, 1) + "合计";
                        for (int i = 3; i <= 10; i++)
                        {
                            strArr[i] = String.valueOf(data[i]);
                            data1[i] += data[i];
                        }
                        //总达成
                        if (strArr[10].equals("0.0"))
                        {
                            strArr[11] = "0";
                        }
                        else
                        {
                            strArr[11] = String.valueOf(Double.parseDouble(
                                    strArr[7]) / Double.parseDouble(strArr[9]));
                        }
                        tlistTable.add(strArr);
                        strArr = new String[12];
                        data = new double[12];
                    }
                }
            }

            strArr = new String[12];
            strArr[0] = "总计";
            for (int i = 3; i <= 10; i++)
            {
                strArr[i] = String.valueOf(data1[i]);
            }
            //总达成
            if (strArr[10].equals("0.0"))
            {
                strArr[11] = "0";
            }
            else
            {
                strArr[11] = String.valueOf(Double.parseDouble(strArr[8]) /
                                            Double.parseDouble(strArr[10]));
            }
            tlistTable.add(strArr);
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("BankChannelDay.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankChannelDayBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    public static void main(String[] args)
    {
    }
}