package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class BankRiskPremBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private GlobalInput mGlobalInput = new GlobalInput();
    private String StartDay = "";
    private String EndDay = "";
    private String RiskCode = "";
    //输入的查询sql语句

    //业务处理相关变量
    /** 全局数据 */
    public BankRiskPremBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        StartDay = (String) cInputData.get(0);
        EndDay = (String) cInputData.get(1);
        RiskCode = (String) cInputData.get(2);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 3));

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "DepartOrderBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String strArr[] = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("BankRiskPrem");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDay", StartDay); //输入制表时间
        texttag.add("EndDay", EndDay);
        texttag.add("RiskCode", RiskCode);
        strArr = new String[7];
        double data[] = null;
        data = new double[7];
        double data1[] = null;
        data1 = new double[7];
        //总行名称
        String asql =
                "select name,agentcom from lacom where banktype='00' and managecom like '" +
                mGlobalInput.ManageCom + "%'";
        ExeSQL aExeSQL = new ExeSQL();
        SSRS aSSRS = new SSRS();
        aSSRS = aExeSQL.execSQL(asql);
        for (int a = 1; a <= aSSRS.getMaxRow(); a++)
        {
            for (int b = 1; b <= aSSRS.getMaxCol(); b++)
            {
                if (b == 1)
                {
                    strArr[0] = aSSRS.GetText(a, 1);
                }
                if (b == 2)
                {
                    //支行名称
                    String bsql =
                            "select name,agentcom from lacom where banktype='02' and agentcom like '" +
                            aSSRS.GetText(a, 2) + "%'";
                    ExeSQL bExeSQL = new ExeSQL();
                    SSRS bSSRS = new SSRS();
                    bSSRS = bExeSQL.execSQL(bsql);
                    for (int c = 1; c <= bSSRS.getMaxRow(); c++)
                    {
                        for (int d = 1; d <= bSSRS.getMaxCol(); d++)
                        {
                            if (d == 1)
                            {
                                strArr[1] = bSSRS.GetText(c, 1);
                            }
                            if (d == 2)
                            {
                                //网点名称
                                String csql =
                                        "select name,agentcom from lacom where banktype='04' and AgentCom like '" +
                                        bSSRS.GetText(c, 2) + "%'";
                                ExeSQL cExeSQL = new ExeSQL();
                                strArr[2] = cExeSQL.getOneValue(csql);
                                //险种名称
                                String dsql =
                                        "select riskname from lmriskapp where  and AgentCom like '" +
                                        bSSRS.GetText(c, 2) + "%'";
                                ExeSQL dExeSQL = new ExeSQL();
                                strArr[3] = dExeSQL.getOneValue(dsql);
                                //预收保费
                                String fsql =
                                        "select nvl(sum(prem),0) from lcpol where  tmakeDate>='" +
                                        StartDay + "' and tmakeDate<='" +
                                        EndDay + "' and AgentCom like '" +
                                        bSSRS.GetText(c, 2) +
                                        "%' and sellflag='Y'";
                                ExeSQL fExeSQL = new ExeSQL();
                                strArr[4] = fExeSQL.getOneValue(fsql);
                                data[4] += Double.parseDouble(strArr[4]);
                                //活动网点数
                                //保单件数
                                String gsql =
                                        "select count(distinct polno) from lcpol where tmakeDate>='" +
                                        StartDay + "' and tmakeDate<='" +
                                        EndDay + "' and AgentCom like '" +
                                        bSSRS.GetText(c, 2) +
                                        "%' and sellflag='Y'";
                                ExeSQL gExeSQL = new ExeSQL();
                                strArr[6] = gExeSQL.getOneValue(gsql);
                                data[6] += Double.parseDouble(strArr[6]);
                                tlistTable.add(strArr);
                                strArr = new String[7];
                            }
                        }
                    }
                }
            }
            //各总行合计
            strArr[0] = "总行汇总";
            strArr[4] = String.valueOf(data[4]);
            data1[4] += data[4];
            strArr[6] = String.valueOf(data[6]);
            data1[6] += data[6];
            tlistTable.add(strArr);
            strArr = new String[7];
            data = new double[7];
            return true;
        }
        //大总计
        strArr = new String[7];
        strArr[0] = "汇总";
        strArr[4] = String.valueOf(data1[4]);
        strArr[6] = String.valueOf(data1[6]);
        tlistTable.add(strArr);

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("BankRiskPrem.vts", "printer"); //最好紧接着就初始化xml文档
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
        mResult.addElement(xmlexport);
        System.out.println("end");
        return true;
    }

    /*
       如果一个字符串数字中小数点后有值，则去掉小数点后面数值
     */
    public static String getInt(String Value)
    {
        String result = "";
        boolean mflag = true;
        int m = 0;
        m = Value.lastIndexOf(".");
        if (m == -1)
        {
            result = Value;
        }
        else
        {
            result = Value.substring(0, m);
        }
        return result;
    }

    public static void main(String[] args)
    {
    }
}