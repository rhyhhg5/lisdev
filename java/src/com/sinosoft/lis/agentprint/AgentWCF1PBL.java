package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class AgentWCF1PBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    //取得的时间
    private String YearMonth;
    private String ManageCom;

    //输入的查询sql语句
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public AgentWCF1PBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        YearMonth = (String) cInputData.get(0);
        ManageCom = (String) cInputData.get(1);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }


    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCPolBillF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        SSRS tSSRS = new SSRS();
        sql = "select a.AgentCode,c.Name,b.branchattr,a.ManageCom,a.f22,a.f23,a.LastMoney,a.k04,a.k05,a.k06,a.k07,a.k08,a.k09,a.f26,a.f27,a.f29,a.ShouldMoney,a.k02,a.f28,a.CurrMoney,a.summoney from LAWage a,labranchgroup b,laagent c where a.agentcode = c.agentcode and b.agentgroup = c.branchcode and c.BranchType='2' and IndexCalNo = '" +
              YearMonth + "' and a.ManageCom like '" + ManageCom +
              "%' order by a.AgentCode";

        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql);

        ListTable tlistTable = new ListTable();
        String strArr[] = null;
        tlistTable.setName("AGENT");
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            strArr = new String[21];
            for (int j = 1; j <= tSSRS.getMaxCol(); j++)
            {
                strArr[j - 1] = tSSRS.GetText(i, j);
            }
            tlistTable.add(strArr);
        }
        strArr = new String[21];
//    strArr[0] = "营业组"; strArr[1] ="姓名";strArr[2] = "代码"; strArr[3] ="职级";strArr[4] ="主管属性";strArr[5] ="入司时间";strArr[6] ="代理人状态";strArr[7] = "增员人姓名"; strArr[8] ="组育成人姓名";strArr[9] = "部育成人姓名"; strArr[10] ="督导长育成人姓名";
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("AgentWC.vts", "printer"); //最好紧接着就初始化xml文档
        xmlexport.addListTable(tlistTable, strArr);
        xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
        mResult.addElement(xmlexport);
        return true;

    }

    public static void main(String[] args)
    {
        String BranchAttr = "8611";
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "8611";
        tG.ComCode = "8611";
        tG.Operator = "001";
        VData tVData = new VData();
        tVData.addElement(BranchAttr);
        tVData.addElement(tG);
        AgentWCF1PBL tAgentWCF1PUI = new AgentWCF1PBL();
        tAgentWCF1PUI.submitData(tVData, "PRINT");
    }
}