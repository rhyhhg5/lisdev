package com.sinosoft.lis.agentprint;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.AgentPubFun;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class YearBonusPrintBL {

    public CErrors mErrors = new CErrors();

    private VData mInputData = new VData();

    private VData mResult = new VData();

    private String mOperate = "";

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();
    private String mBranchAttr = "";
    private String mStartCalNo = "";
    private String mEndCalNo = "";
    private String mAgentGroup = "";
    private String mAgentCode ="";
    private String mAgentName ="";
    private String mType ="";
    private String mWageNo ="";
    private String mManageCom = "";
    private String[] mDataList = null;
    private XmlExport mXmlExport = null;
//xqq 2014-12-2
    public String tttAgentCode = "";
    
    private SSRS mSSRS1 = new SSRS();


    private ListTable mListTable = new ListTable();

    private PubFun mPubFun = new PubFun();

    private String mManageName = "";

    public YearBonusPrintBL() {
    }
    public static void main(String[] args)
    {

        GlobalInput tG = new GlobalInput();
        tG.Operator = "xxx";
        tG.ManageCom = "86";
        YearBonusPrintBL tYearBonusPrintBL = new YearBonusPrintBL();
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        mOperate = cOperate;
        mInputData = (VData) cInputData;
        if (mOperate.equals("")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!mOperate.equals("PRINT")) {
            this.bulidError("submitData", "数据不完整");
            return false;
        }

        if (!this.getInputData(mInputData)) {
            return false;
        }

        if (!dealdate()) {
            return false;
        }

        if (!getPrintData()) {
            this.bulidError("getPrintData", "查询数据失败！");
            return false;
        }

        return true;
    }

    /**
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

        try
        {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);

        } catch (Exception ex) {
            this.mErrors.addOneError("");
            return false;
        }
        this.mManageCom = (String) mTransferData.getValueByName("tManageCom");
        this.mBranchAttr = (String) mTransferData.getValueByName("tBranchAttr");
        this.mAgentGroup = (String) mTransferData.getValueByName("tAgentGroup");
        this.mAgentCode = (String) mTransferData.getValueByName("tAgentCode");
        this.mType = (String) mTransferData.getValueByName("tType");
        this.mWageNo = (String) mTransferData.getValueByName("tWageNo");
        this.mStartCalNo = (String) mTransferData.getValueByName("tStartCalNo");
        this.mEndCalNo = (String) mTransferData.getValueByName("tEndCalNo");
        this.mAgentName = (String) mTransferData.getValueByName("tAgentName");

        return true;
    }
    /**
    * 业务处理方法
    * @return boolean
    */

   private boolean dealdate()
   {
      if (!getAgentNow())
      {
         return false;
      }

      return true;
   }
   /**
    * 查询数据
    * @return boolean
   */
   private boolean getAgentNow() {

       ExeSQL tExeSQL = new ExeSQL();
        if(mType.equals("0")){
            String tStart=mWageNo+"01";
            String tEnd=mWageNo+"12";
            String tSql =
                " select min(getUniteCode(a.agentcode)),value(sum(a.T70),0) "
               +" from laindexinfo a,laagent b "
               +" where a.branchtype='2' and a.branchtype2='01'  and a.indextype='00' and a.agentcode=b.agentcode  "
              // +" and a.agentgroup=c.agentgroup and c.branchtype='2' and c.branchtype2='01' "
               +" and a.indexcalno  between '"+tStart+"'  and '"+tEnd+"' "
               +" and a.managecom like '"+mManageCom+"%' ";
       if (mAgentCode != null && !mAgentCode.equals("")) {
           tSql += " and a.agentcode = '" + mAgentCode + "' ";
       }
       if (mAgentName != null && !mAgentName.equals("")) {
           tSql += " and b.name = '" + mAgentName+ "' ";
       }
       if (mBranchAttr != null && !mBranchAttr.equals("")) {
           tSql += " and a.agentgroup= '" + mAgentGroup + "' ";
       }
       tSql +=  " group by a.agentcode  "
               +" order by a.agentcode ";
       mSSRS1 = tExeSQL.execSQL(tSql);
       System.out.println(tSql);

        }
       if(mType.equals("1")){
       String tSql =
                " select min(getUniteCode(a.agentcode)),a.indexcalno,value(sum(a.T70),0) "
               +" from laindexinfo a,laagent b "
               +" where a.branchtype='2' and a.branchtype2='01' and a.indextype='00'  and a.agentcode=b.agentcode  "
              // +" and a.agentgroup=c.agentgroup and c.branchtype='2' and c.branchtype2='01' "
               +" and a.indexcalno  between '"+mStartCalNo+"'  and '"+mEndCalNo+"' "
               +" and a.managecom like '"+mManageCom+"%' ";
       if (mAgentCode != null && !mAgentCode.equals("")) {
           tSql += " and a.agentcode = '" + mAgentCode + "' ";
       }
       if (mAgentName != null && !mAgentName.equals("")) {
           tSql += " and b.name = '" + mAgentName+ "' ";
       }
       if (mBranchAttr != null && !mBranchAttr.equals("")) {
           tSql += " and a.agentgroup= '" + mAgentGroup + "' ";
       }
       tSql +=  " group by GROUPING SETS ((a.agentcode,a.indexcalno),a.agentcode)  "
               +" order by a.agentcode,a.indexcalno";
       mSSRS1 = tExeSQL.execSQL(tSql);
       System.out.println(tSql);
       }
       if (tExeSQL.mErrors.needDealError()) {
           CError tCError = new CError();
           tCError.moduleName = "MakeXMLBL";
           tCError.functionName = "creatFile";
           tCError.errorMessage = "查询XML数据出错！";
           this.mErrors.addOneError(tCError);
           return false;
       }
       if (mSSRS1.getMaxRow() <= 0) {
           CError tCError = new CError();
           tCError.moduleName = "MakeXMLBL";
           tCError.functionName = "creatFile";
           tCError.errorMessage = "没有符合条件的信息！";
           this.mErrors.addOneError(tCError);
           return false;
       }

       return true;
   }

    /**
     *
     * @return boolean
     */
    private boolean getPrintData() {
        TextTag tTextTag = new TextTag();
        System.out.println("121212123");
        mXmlExport = new XmlExport();
        //设置模版名称
        mXmlExport.createDocument("YearBonusPrint.vts", "printer");

        String tMakeDate = "";
        String tMakeTime = "";

        tMakeDate = mPubFun.getCurrentDate();
        tMakeTime = mPubFun.getCurrentTime();

        if (!getManageName()) {
            return false;
        }
        tTextTag.add("MakeDate", tMakeDate);
        tTextTag.add("MakeTime", tMakeTime);
        tTextTag.add("tName", mManageName);


        System.out.println("12121212" + tMakeDate);
        if (tTextTag.size() < 1) {
            return false;
        }

        mXmlExport.addTextTag(tTextTag);
//需要修改
        String[] title = {"","","","","","","","",""};

        if (!getListTable()) {
            return false;
        }

        mListTable.setName("Order");
        mXmlExport.addListTable(mListTable, title);
        mXmlExport.outputDocumentToFile("c:\\", "new1");
        this.mResult.clear();

        mResult.addElement(mXmlExport);

        return true;
    }

    /**
     * 查询列表显示数据
     * @return boolean
     */
    private boolean getListTable() {

        if (mSSRS1.getMaxRow() > 0) {
            for (int i = 1; i <= mSSRS1.getMaxRow(); i++) {
                String Info[] = new String[36];
                if(mType.equals("0")){
                	 //查询统一工号： 2014-11-21  解青青
					 ExeSQL tttExeSQL = new ExeSQL();
					 String tttGAC = "select agentcode from laagent where groupagentcode='"+mSSRS1.GetText(i, 1)+"'";
					 String tttAgentCode = tttExeSQL.getOneValue(tttGAC);
					 //xqq 2014-12-2把mSSRS1.GetText(i, 1) 换成了 tttAgentCode
                     Info[0] = Integer.toString(i);
                     Info[1] = mSSRS1.GetText(i, 1);
                     Info[2] = getAgentName(tttAgentCode);
                     Info[3] = mManageCom;
                     Info[4] = getBranchAttr(tttAgentCode);
                     Info[5] = getBranchName(tttAgentCode);
                     Info[6] = mWageNo;
                     Info[7] = getFyc(Info[1],Info[6],"Y");
                     Info[8] = mSSRS1.GetText(i, 2);
                }
                if(mType.equals("1")){
//                	查询统一工号： 2014-11-21  解青青
					 ExeSQL tttExeSQL = new ExeSQL();
					 String tttGAC = "select agentcode from laagent where groupagentcode='"+mSSRS1.GetText(i, 1)+"'";
					 String tttAgentCode = tttExeSQL.getOneValue(tttGAC);
					 //xqq 2014-12-2把mSSRS1.GetText(i, 1) 换成了 tttAgentCode
                    Info[0] = Integer.toString(i);
                   Info[1] = mSSRS1.GetText(i, 1);
                   Info[2] = getAgentName(tttAgentCode);
                   Info[3] = mManageCom;
                   Info[4] = getBranchAttr(tttAgentCode);
                   Info[5] = getBranchName(tttAgentCode);
                   Info[6] = mSSRS1.GetText(i, 2);
                   Info[7] = getFyc(Info[1],Info[6],"M");
                   Info[8] = mSSRS1.GetText(i, 3);
               }
                mListTable.add(Info);
            }

        } else {
            CError tCError = new CError();
            tCError.moduleName = "CreateXml";
            tCError.functionName = "creatFile";
            tCError.errorMessage = "没有符合条件的信息！";
            this.mErrors.addOneError(tCError);
            return false;
        }
        return true;
    }





    private String getFyc(String pmAgentCode,String pmWageNo,String pmType)
     {
         String tSQL = "";
         String tRtValue = "";
         double tDbValue = 0.00;
        if(pmType.equals("Y")){
        	tSQL = "select value(sum(k20),0) from lawage where agentcode='"+pmAgentCode+"'"
        	+" and indexcalno>='"+pmWageNo+"01' and indexcalno<='"+pmWageNo+"12'";
        }
        else {
        
        if(!pmWageNo.equals("")){
        	tSQL = "select sum(k20) from lawage where agentcode='"+pmAgentCode
            +"' and indexcalno='"+pmWageNo+"' ";
        	}
        else{
        	tSQL = "select sum(k20) from lawage where agentcode='"+pmAgentCode
            +"' and indexcalno>='"+mStartCalNo+"'  and indexcalno<='"+mEndCalNo+"' ";
        }
        }
         try{
             tDbValue = execQuery(tSQL);
         }catch(Exception ex)
         {
             System.out.println("getFyc 出错！");
         }
         tRtValue = String.valueOf(tDbValue);

         return tRtValue;
     }

  
  private String getAgentName(String pmAgentCode)
  {
    String tSQL = "";
    String tRtValue="";
    tSQL = "select  name from laagent where agentcode='" + pmAgentCode +"'";
    SSRS tSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    tSSRS = tExeSQL.execSQL(tSQL);
    if(tSSRS.getMaxRow()==0)
    {
    tRtValue="";
    }
    else
    tRtValue=tSSRS.GetText(1, 1);
    return tRtValue;
}

private String getBranchAttr(String pmAgentCode)
 {
   String tSQL = "";
   String tRtValue="";
   tSQL = "select  branchattr from labranchgroup where agentgroup=(select agentgroup from laagent where agentcode='" + pmAgentCode +"')";
   SSRS tSSRS = new SSRS();
   ExeSQL tExeSQL = new ExeSQL();
   tSSRS = tExeSQL.execSQL(tSQL);
   if(tSSRS.getMaxRow()==0)
   {
   tRtValue="";
   }
   else
   tRtValue=tSSRS.GetText(1, 1);
   return tRtValue;
}


    private String getBranchName(String pmAgentCode)
     {
       String tSQL = "";
       String tRtValue="";
       tSQL = "select  name from labranchgroup where agentgroup=(select agentgroup from laagent where agentcode='" + pmAgentCode +"')";
       SSRS tSSRS = new SSRS();
       ExeSQL tExeSQL = new ExeSQL();
       tSSRS = tExeSQL.execSQL(tSQL);
       if(tSSRS.getMaxRow()==0)
       {
       tRtValue="";
       }
       else
       tRtValue=tSSRS.GetText(1, 1);
       return tRtValue;
}
    private boolean getManageName() {

        String sql = "select name from ldcom where comcode='" + mManageCom +
                     "'";

        SSRS tSSRS = new SSRS();

        ExeSQL tExeSQL = new ExeSQL();

        tSSRS = tExeSQL.execSQL(sql);

        if (tExeSQL.mErrors.needDealError()) {

            this.mErrors.addOneError("销售单位不存在！");

            return false;

        }

        if (mManageCom.equals("86")) {
            this.mManageName = "";
        } else {
            this.mManageName = tSSRS.GetText(1, 1) + "分公司";
        }

        return true;
    }


    /**
     * 获取打印所需要的数据
     * @param cFunction String
     * @param cErrorMsg String
     */
    private void bulidError(String cFunction, String cErrorMsg) {

        CError tCError = new CError();

        tCError.moduleName = "YearBonusPrintBL";
        tCError.functionName = cFunction;
        tCError.errorMessage = cErrorMsg;

        this.mErrors.addOneError(tCError);

    }
    /**
      * 执行SQL文查询结果
      * @param sql String
      * @return double
      */
     private double execQuery(String sql)
     {
         Connection conn;
         conn = null;
         conn = DBConnPool.getConnection();

         System.out.println(sql);

         PreparedStatement st = null;
         ResultSet rs = null;
         try {
             if (conn == null)return 0.00;
             st = conn.prepareStatement(sql);
             if (st == null)return 0.00;
             rs = st.executeQuery();
             if (rs.next()) {
                 return rs.getDouble(1);
             }
             return 0.00;
         } catch (Exception ex) {
             ex.printStackTrace();
             return -1;
         } finally {
             try {
               if (!conn.isClosed()) {
                   conn.close();
               }
               try {
                   st.close();
                   rs.close();
               } catch (Exception ex2) {
                   ex2.printStackTrace();
               }
               st = null;
               rs = null;
               conn = null;
             } catch (Exception e) {}

         }
    }
    /**
     *
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


}
