package com.sinosoft.lis.agentprint;

import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class BankDayReportPrt
{
    public GlobalInput mGI = new GlobalInput();
    public String mStaticDate = "";
    public VData mResult = new VData();
    public CErrors mErrors = new CErrors();

    public BankDayReportPrt()
    {
    }

    public static void main(String[] args)
    {
        BankDayReportPrt bankDayReportPrt1 = new BankDayReportPrt();
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86110000";
        tGI.ManageCom = "86110000";
        tGI.Operator = "aa";
        VData tInputData = new VData();
        tInputData.clear();
        tInputData.addElement(tGI);
        tInputData.add("2003-12-28");
        if (!bankDayReportPrt1.getInputData(tInputData))
        {
            System.out.println("-----getinputdata failed!------");
        }
        if (!bankDayReportPrt1.prepareData())
        {
            System.out.println("-------prepareData failed!------");
        }

    }

    public boolean getInputData(VData cInputData)
    {
        //全局变量
        try
        {
            mStaticDate = (String) cInputData.getObjectByObjectName("String", 0);
            mGI = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput",
                    0);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("getInputData", ex.toString());
            return false;
        }

        return true;
    }

    public boolean prepareData()
    {
        String strArr[] = null;

        ListTable tListTable = new ListTable();
        tListTable.setName("BankDay");

        TextTag texttag = new TextTag(); //新建一个TextTag的实例

        texttag.add("StaticDate", this.mStaticDate); //输入制表时间
        PubFun tPF = new PubFun();
        //本旬
        String tTenDay = tPF.calDate(this.mStaticDate, -10, "D", "");
        //本季
        String tQuarter = tPF.calDate(this.mStaticDate, -3, "M", "");
        //本月
        String tMon = tPF.calDate(this.mStaticDate, -1, "M", "");
        //本年
        String tYear = tPF.calDate(this.mStaticDate, -1, "Y", "");

        //查询出各个区级
        SSRS aSSRS = new SSRS();
        String Sql1 =
                "select comcode,trim(shortname) from Ldcom where length(trim(comcode))=4";
        String Sql2 = "";
        String Sql3 = "";
        int tChanelCount = 0;
        double[] sum2 = new double[20];

        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);
            aSSRS = aExeSQL.execSQL(Sql1);
            //各个分公司
            int tBranchCount = aSSRS.getMaxRow();
            if (tBranchCount == 0)
            {
                dealError("prepareData", "分支公司信息查询失败！");
                return false;
            }

            String[] tBranch = new String[tBranchCount];
            String[] tBranchName = new String[tBranchCount];
            for (int i = 1; i <= tBranchCount; i++)
            {
                System.out.println("--------" + i + "---------");
                double sum[] = new double[20];

                tBranch[i - 1] = new String();
                tBranch[i - 1] = aSSRS.GetText(i, 1);
                tBranchName[i - 1] = new String();
                tBranchName[i - 1] = aSSRS.GetText(i, 2);
                System.out.println("------Branch: " + tBranch[i - 1]);
                System.out.println("------BranchName: " + tBranchName[i - 1]);

                Sql2 = " select agentcom,trim(name) from lacom where "
                       + " (banktype = '01' or banktype is null) and "
                       + " managecom like '" + tBranch[i - 1].trim() + "%'";
                SSRS bSSRS = new SSRS();
                bSSRS = aExeSQL.execSQL(Sql2);
                //查询分行或者代理机构
                tChanelCount = bSSRS.getMaxRow();
                if (tChanelCount == 0)
                {
                    continue;
                }

                String[] tChanel = new String[tChanelCount];
                String[] tChanelName = new String[tChanelCount];
                for (int j = 1; j <= tChanelCount; j++)
                {
                    strArr = new String[22];
                    //分公司名
                    strArr[0] = tBranchName[i - 1].trim();

                    tChanel[j - 1] = new String();
                    tChanelName[j - 1] = new String();
                    tChanel[j - 1] = bSSRS.GetText(j, 1);
                    tChanelName[j - 1] = bSSRS.GetText(j, 2);
                    //渠道名
                    strArr[1] = tChanelName[j - 1].trim();

                    //当日承保保费--承保数据--保费,件数
                    String aSql3 =
                            "select nvl(sum(transmoney),0),nvl(sum(calcount),0) from lacommision where "
                            + " tmakedate = '" + this.mStaticDate.trim() + "'"
                            + " and  trim(agentcom) like '" + tChanel[j -
                            1].trim() + "%'";
                    SSRS zSSRS = new SSRS();
                    zSSRS = aExeSQL.execSQL(aSql3);
                    strArr[2] = zSSRS.GetText(1, 1);
                    strArr[3] = zSSRS.GetText(1, 2);

                    //当日撤保保费--承保前撤保--保费
                    String cSql3 =
                            "select nvl(sum(transmoney),0),nvl(sum(calcount),0) from lacommision where "
                            + " tmakedate = '" + this.mStaticDate.trim()
                            + "' and transtype = 'WT'"
                            + " and trim(agentcom) like '" + tChanel[j -
                            1].trim() + "%'";
                    SSRS ySSRS = new SSRS();
                    ySSRS = aExeSQL.execSQL(cSql3);
                    strArr[4] = ySSRS.GetText(1, 1);
                    strArr[5] = ySSRS.GetText(1, 2);

                    //本旬累计承保保费--承保数据--保费
                    String eSql3 =
                            "select nvl(sum(transmoney),0),nvl(sum(calcount),0) from lacommision where "
                            + " tmakedate <= '" + this.mStaticDate.trim()
                            + "' and tmakedate > '" + tTenDay.trim() + "'"
                            + " and trim(agentcom) like '" + tChanel[j -
                            1].trim() + "%'";
                    SSRS xSSRS = new SSRS();
                    xSSRS = aExeSQL.execSQL(eSql3);
                    strArr[6] = xSSRS.GetText(1, 1);
                    strArr[7] = xSSRS.GetText(1, 2);
                    //本旬累计承保保费--承保前撤保--保费
                    String gSql3 =
                            "select nvl(sum(transmoney),0),nvl(sum(calcount),0) from lacommision where "
                            + " tmakedate <= '" + this.mStaticDate.trim()
                            + "' and tmakedate > '" + tTenDay.trim() + "'"
                            + " and transtype = 'WT'"
                            + " and trim(agentcom) like '" + tChanel[j -
                            1].trim() + "%'";
                    SSRS vSSRS = new SSRS();
                    vSSRS = aExeSQL.execSQL(gSql3);
                    strArr[8] = vSSRS.GetText(1, 1);
                    strArr[9] = vSSRS.GetText(1, 2);

                    //本月累计承保保费--承保数据--保费
                    String iSql3 =
                            "select nvl(sum(transmoney),0),nvl(sum(calcount),0) from lacommision where "
                            + " tmakedate <= '" + this.mStaticDate.trim()
                            + "' and tmakedate > '" + tMon.trim() + "'"
                            + " and trim(agentcom) like '" + tChanel[j -
                            1].trim() + "%'";
                    SSRS uSSRS = new SSRS();
                    uSSRS = aExeSQL.execSQL(iSql3);
                    strArr[10] = uSSRS.GetText(1, 1);
                    strArr[11] = uSSRS.GetText(1, 2);

                    //本月累计承保保费--承保前撤保--保费
                    String kSql3 =
                            "select nvl(sum(transmoney),0),nvl(sum(calcount),0) from lacommision where "
                            + " tmakedate <= '" + this.mStaticDate.trim()
                            + "' and tmakedate > '" + tMon.trim() + "'"
                            + " and transtype = 'WT'"
                            + " and trim(agentcom) like '" + tChanel[j -
                            1].trim() + "%'";
                    SSRS tSSRS = new SSRS();
                    tSSRS = aExeSQL.execSQL(kSql3);
                    strArr[12] = tSSRS.GetText(1, 1);
                    strArr[13] = tSSRS.GetText(1, 2);

                    //本季累计承保保费--承保数据--保费
                    String mSql3 =
                            "select nvl(sum(transmoney),0),nvl(sum(calcount),0) from lacommision where "
                            + " tmakedate <= '" + this.mStaticDate.trim()
                            + "' and tmakedate > '" + tQuarter.trim() + "'"
                            + " and trim(agentcom) like '" + tChanel[j -
                            1].trim() + "%'";
                    SSRS sSSRS = new SSRS();
                    sSSRS = aExeSQL.execSQL(mSql3);
                    strArr[14] = sSSRS.GetText(1, 1);
                    strArr[15] = sSSRS.GetText(1, 2);

                    //本季累计承保保费--承保前撤保--保费
                    String oSql3 =
                            "select nvl(sum(transmoney),0),nvl(sum(calcount),0) from lacommision where "
                            + " tmakedate <= '" + this.mStaticDate.trim()
                            + "' and tmakedate > '" + tQuarter.trim() + "'"
                            + " and transtype = 'WT'"
                            + " and trim(agentcom) like '" + tChanel[j -
                            1].trim() + "%'";
                    SSRS rSSRS = new SSRS();
                    rSSRS = aExeSQL.execSQL(oSql3);
                    strArr[16] = rSSRS.GetText(1, 1);
                    strArr[17] = rSSRS.GetText(1, 2);

                    //本年累计承保保费--承保数据--保费
                    String qSql3 =
                            "select nvl(sum(transmoney),0),nvl(sum(calcount),0) from lacommision where "
                            + " tmakedate <= '" + this.mStaticDate.trim()
                            + "' and tmakedate > '" + tYear.trim() + "'"
                            + " and trim(agentcom) like '" + tChanel[j -
                            1].trim() + "%'";
                    SSRS qSSRS = new SSRS();
                    qSSRS = aExeSQL.execSQL(qSql3);
                    strArr[18] = qSSRS.GetText(1, 1);
                    strArr[19] = qSSRS.GetText(1, 2);

                    //本年累计承保保费--承保前撤保--保费
                    String sSql3 =
                            "select nvl(sum(transmoney),0),nvl(sum(calcount),0) from lacommision where "
                            + " tmakedate <= '" + this.mStaticDate.trim()
                            + "' and tmakedate > '" + tYear.trim() + "'"
                            + " and transtype = 'WT'"
                            + " and trim(agentcom) like '" + tChanel[j -
                            1].trim() + "%'";
                    SSRS pSSRS = new SSRS();
                    pSSRS = aExeSQL.execSQL(sSql3);
                    strArr[20] = pSSRS.GetText(1, 1);
                    strArr[21] = pSSRS.GetText(1, 2);
                    tListTable.add(strArr);

                    //合计

                    sum[0] += Double.parseDouble(strArr[2]);
                    sum[1] += Double.parseDouble(strArr[3]);
                    sum[2] += Double.parseDouble(strArr[4]);
                    sum[3] += Double.parseDouble(strArr[5]);
                    sum[4] += Double.parseDouble(strArr[6]);
                    sum[5] += Double.parseDouble(strArr[7]);
                    sum[6] += Double.parseDouble(strArr[8]);
                    sum[7] += Double.parseDouble(strArr[9]);
                    sum[8] += Double.parseDouble(strArr[10]);
                    sum[9] += Double.parseDouble(strArr[11]);
                    sum[10] += Double.parseDouble(strArr[12]);
                    sum[11] += Double.parseDouble(strArr[13]);
                    sum[12] += Double.parseDouble(strArr[14]);
                    sum[13] += Double.parseDouble(strArr[15]);
                    sum[14] += Double.parseDouble(strArr[16]);
                    sum[15] += Double.parseDouble(strArr[17]);
                    sum[16] += Double.parseDouble(strArr[18]);
                    sum[17] += Double.parseDouble(strArr[19]);
                    sum[18] += Double.parseDouble(strArr[20]);
                    sum[19] += Double.parseDouble(strArr[21]);
                }

                strArr = new String[22];
                strArr[0] = "合计";
                strArr[1] = "";
                for (int n = 2; n <= 21; n++)
                {
                    strArr[n] = String.valueOf(sum[n - 2]);
                }
                tListTable.add(strArr);

                //共计
                sum2[0] += sum[0];
                sum2[1] += sum[1];
                sum2[2] += sum[2];
                sum2[3] += sum[3];
                sum2[4] += sum[4];
                sum2[5] += sum[5];
                sum2[6] += sum[6];
                sum2[7] += sum[7];
                sum2[8] += sum[8];
                sum2[9] += sum[9];
                sum2[10] += sum[10];
                sum2[11] += sum[11];
                sum2[12] += sum[12];
                sum2[13] += sum[13];
                sum2[14] += sum[14];
                sum2[15] += sum[15];
                sum2[16] += sum[16];
                sum2[17] += sum[17];
                sum2[18] += sum[18];
                sum2[19] += sum[19];
            }

            strArr = new String[22];
            strArr[0] = "共计";
            strArr[1] = "";
            for (int m = 0; m < 20; m++)
            {
                strArr[m + 2] = String.valueOf(sum2[m]);
            }
            tListTable.add(strArr);

            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("BankDayReport.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tListTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankDayReportPrt";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("------prepareData end-----");
        return true;
    }

    private void dealError(String tFuncName, String tErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "BankDayReportPrt";
        cError.functionName = tFuncName;
        cError.errorMessage = tErrMsg;
        this.mErrors.addOneError(cError);
    }

    public VData getResult()
    {
        return this.mResult;
    }

}