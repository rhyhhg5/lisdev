package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author
 * @version 1.0
 */
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PolAssessBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private String Month = "";
    private String quarter = "";
    private String ManageCom = "";

    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    public PolAssessBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("PRINT") && !cOperate.equals("CONFIRM"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (cOperate.equals("PRINT"))
        {
            if (!queryData())
            {
                return false;
            }
        }
        if (cOperate.equals("CONFIRM"))
        {
            if (!queryDataOther())
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        Month = (String) cInputData.get(0);
        quarter = (String) cInputData.get(1);
        ManageCom = (String) cInputData.get(2);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "PolAssessBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String strArr[] = null;
        double data[] = new double[9]; //总公司
        FDate bDate = new FDate();
        Date date1 = bDate.getDate(Month.substring(0, 4) + "-" +
                                   Month.substring(4, 6) + "-01");
        Date xyc = PubFun.calDate(date1, 1, "M", null); //下月初
        Date q9yc = PubFun.calDate(date1, -8, "M", null); //前推9个月初
        Date q2yc = PubFun.calDate(date1, -1, "M", null); //前推2个月初
        Date q3yc = PubFun.calDate(date1, -2, "M", null); //前推3个月初
        String xgyc = bDate.getString(xyc);
        String q9gyc = bDate.getString(q9yc).substring(0, 7) + "-10";
        String q2gyc = bDate.getString(q2yc).substring(0, 7) + "-10";
        String q3gyc = bDate.getString(q3yc).substring(0, 7) + "-10";
        ListTable tlistTable = new ListTable();
        tlistTable.setName("PolAss");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("Day",
                    Month.substring(0, 4) + "年" + Month.substring(4, 6) + "月"); //输入制表时间
        //查询
        strArr = new String[9];
        //营业单位
        String asql =
                "select distinct agentgrade from latree where agentgrade in ('A01','A02','A03')";
        ExeSQL aExeSQL = new ExeSQL();
        SSRS aSSRS = new SSRS();
        aSSRS = aExeSQL.execSQL(asql);

        for (int a = 1; a <= aSSRS.getMaxRow(); a++)
        {
            //营业机构
            strArr[0] = aSSRS.GetText(1, a);
            //考核前人力
            String bsql = "select count(*) from laassess where agentgrade='" +
                          aSSRS.GetText(1, a)
                          + "' and Indexcalno='" + Month +
                          "' and branchtype='1' and ManageCom like '" +
                          ManageCom + "%'";
            ExeSQL bExeSQL = new ExeSQL();
            strArr[1] = bExeSQL.getOneValue(bsql);
            data[1] += Double.parseDouble(strArr[1]);
            //业绩为零的人数和占比
            if (aSSRS.GetText(1, a).equals("A01"))
            {
                String csql =
                        "select count(*) from LAIndexInfo a where indexcalno='" +
                        Month
                        + "' and managecom like '" + ManageCom +
                        "%' and t1='10' and IndexType='02' and agentgrade='"
                        + aSSRS.GetText(1, a) + "' and exists (select 'X' from laagent b where a.agentcode=b.agentcode and EmployDate<='"
                        + q2gyc + "' )";
                ExeSQL cExeSQL = new ExeSQL();
                strArr[2] = cExeSQL.getOneValue(csql);
                data[2] += Double.parseDouble(strArr[2]);
                //九月未转正
                String fsql = "select count(*) from laassess a where branchtype='1' and agentgrade='A01' and agentgrade1='00'and Indexcalno='" +
                              Month
                              + "' and exists (select 'X' from laagent b where a.agentcode=b.agentcode and EmployDate<='"
                              + q9gyc + "' )"; //清退人力
                ExeSQL fExeSQL = new ExeSQL();
                strArr[4] = fExeSQL.getOneValue(fsql);
                data[4] += Double.parseDouble(strArr[4]);
                //占比
                if (strArr[1].equals("0"))
                {
                    strArr[5] = "0";
                }
                else
                {
                    strArr[5] = String.valueOf(Double.parseDouble(strArr[4]) /
                                               Double.parseDouble(strArr[1]));
                }
                //转正人数
                String dsql =
                        "select nvl(count(*),0) from laagent a where InDueFormDate='" +
                        xgyc
                        + "' and managecom like '" + ManageCom +
                        "%' and exists (select 'X' from laassess b "
                        + "where a.agentcode=b.agentcode and Indexcalno='" +
                        Month + "')";
                ExeSQL dExeSQL = new ExeSQL();
                strArr[6] = dExeSQL.getOneValue(dsql);
                data[6] += Double.parseDouble(strArr[6]);
                //占比
                if (strArr[1].equals("0"))
                {
                    strArr[7] = "0";
                }
                else
                {
                    strArr[7] = String.valueOf(Double.parseDouble(strArr[6]) /
                                               Double.parseDouble(strArr[1]));
                }
            }
            else
            {
                String csql =
                        "select count(*) from LAIndexInfo a where indexcalno='" +
                        Month
                        + "' and managecom like '" + ManageCom +
                        "%' and t2='10' and IndexType='02' and agentgrade='" +
                        aSSRS.GetText(1, a)
                        + "' and exists (select 'X' from laagent b where a.agentcode=b.agentcode and EmployDate<='"
                        + q3gyc + "' )";
                ExeSQL cExeSQL = new ExeSQL();
                strArr[2] = cExeSQL.getOneValue(csql);
                data[2] += Double.parseDouble(strArr[2]);
                strArr[4] = "";
                strArr[5] = "";
                strArr[6] = "";
                strArr[7] = "";
            }
            //业绩为零占比
            if (strArr[1].equals("0"))
            {
                strArr[3] = "0";
            }
            else
            {
                strArr[3] = String.valueOf(Double.parseDouble(strArr[2]) /
                                           Double.parseDouble(strArr[1]));
            }
            //考核后人力
            String esql = "select count(*) from laassess where agentgrade1='" +
                          aSSRS.GetText(1, a)
                          + "' and Indexcalno='" + Month +
                          "' and branchtype='1' and ManageCom like '" +
                          ManageCom + "%'";
            ExeSQL eExeSQL = new ExeSQL();
            strArr[8] = eExeSQL.getOneValue(esql);
            data[8] += Double.parseDouble(strArr[8]);
            tlistTable.add(strArr);
            strArr = new String[9];
        }

        //展业机构：总计
        strArr = new String[9];
        strArr[0] = "合计";
        for (int u = 1; u <= 2; u++)
        {
            strArr[u] = getInt(String.valueOf(data[u]));
        }
        if (data[1] == 0)
        {
            strArr[3] = "0";
        }
        else
        {
            strArr[3] = String.valueOf(data[2] / data[1]);
        }
        strArr[4] = "";
        strArr[5] = "";
        strArr[6] = "";
        strArr[7] = "";
        strArr[8] = getInt(String.valueOf(data[8]));
        tlistTable.add(strArr);
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PolAssess.vts", "printer"); //最好紧接着就初始化xml文档
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
//     xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
        mResult.addElement(xmlexport);
        System.out.println("end");
        return true;
    }

    private boolean queryDataOther()
    {
        String strArr[] = null;
        double data[] = new double[9]; //总公司
        FDate bDate = new FDate();
        FDate aDate = new FDate();
        String CurrentDate = PubFun.getCurrentDate();
        Date CurDate = aDate.getDate(CurrentDate);
        GregorianCalendar mCalendar = new GregorianCalendar();
        mCalendar.setTime(CurDate);
        int Months = mCalendar.get(Calendar.MONTH) + 1; //因为从0开始的
        int Years = mCalendar.get(Calendar.YEAR);
        String endmonth = String.valueOf(Integer.parseInt(quarter) * 3);
        String firstmonth = String.valueOf(Integer.parseInt(endmonth) - 2);
        String firstday = String.valueOf(Years) + "-" + firstmonth + "-01";
        String lastday = "";
        if (quarter.equals("1") || quarter.equals("4"))
        {
            lastday = String.valueOf(Years) + "-" + firstmonth + "-31";
        }
        else
        {
            lastday = String.valueOf(Years) + "-" + endmonth + "-30";
        }
        String OneMonth = "";
        String TwoMonth = "";
        String ThreeMOnth = "";
        if (quarter.equals("4"))
        {
            OneMonth = String.valueOf(Years) + firstmonth;
            TwoMonth = String.valueOf(Years) +
                       String.valueOf(Integer.parseInt(endmonth) - 1);
            ThreeMOnth = String.valueOf(Years) + endmonth;
        }
        else
        {
            OneMonth = String.valueOf(Years) + "0" + firstmonth;
            TwoMonth = String.valueOf(Years) + "0" +
                       String.valueOf(Integer.parseInt(endmonth) - 1);
            ThreeMOnth = String.valueOf(Years) + "0" + endmonth;
        }
        ListTable tlistTable = new ListTable();
        tlistTable.setName("PolAss");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("Day", quarter + "季度"); //输入制表时间
        //查询
        strArr = new String[9];
        //营业单位
        String asql =
                "select distinct agentgrade from latree where agentgrade in ('A01','A02','A03')";
        ExeSQL aExeSQL = new ExeSQL();
        SSRS aSSRS = new SSRS();
        aSSRS = aExeSQL.execSQL(asql);

        for (int a = 1; a <= aSSRS.getMaxRow(); a++)
        {
            //营业机构
            strArr[0] = aSSRS.GetText(1, a);
            //考核前人力
            String bsql = "select count(*) from laassess where agentgrade='" +
                          aSSRS.GetText(1, a)
                          + "' and Indexcalno in ('" + OneMonth + "','" +
                          TwoMonth + "','" + ThreeMOnth
                          + "') and branchtype='1' and ManageCom like '" +
                          ManageCom + "%'";
            ExeSQL bExeSQL = new ExeSQL();
            strArr[1] = bExeSQL.getOneValue(bsql);
            data[1] += Double.parseDouble(strArr[1]);
            //业绩为零的人数和占比
            if (aSSRS.GetText(1, a).equals("A01"))
            {
                String csql = "select sum(a) from (select count(*) a from LAIndexInfo a where agentgrade='A01' "
                              + "and t1='10' and IndexType='02'"
                              + "and Indexcalno='" + OneMonth +
                              "'and exists (select 'X' from "
                              +
                              "laagent b where a.agentcode=b.agentcode and EmployDate<=add_months('"
                              + OneMonth.substring(0, 4) + "-" +
                              OneMonth.substring(4, 6) + "-10',-1) ) union "
                              +
                              "select count(*) a from LAIndexInfo a where agentgrade='A01' "
                              + "and t1='10' and IndexType='02'"
                              + "and Indexcalno='" + TwoMonth +
                              "'and  exists (select 'X' from "
                              +
                              "laagent b where a.agentcode=b.agentcode and EmployDate<=add_months('"
                              + TwoMonth.substring(0, 4) + "-" +
                              TwoMonth.substring(4, 6) + "-10',-1) ) union "
                              +
                              "select count(*) a from LAIndexInfo a where agentgrade='A01' "
                              + "and t1='10' and IndexType='02'"
                              + "and Indexcalno='" + ThreeMOnth +
                              "'and exists (select 'X' from "
                              +
                              "laagent b where a.agentcode=b.agentcode and EmployDate<=add_months('"
                              + ThreeMOnth.substring(0, 4) + "-" +
                              ThreeMOnth.substring(4, 6) + "-10',-1))  )";

                ExeSQL cExeSQL = new ExeSQL();
                strArr[2] = cExeSQL.getOneValue(csql);
                data[2] += Double.parseDouble(strArr[2]);
                //九月未转正
                String fsql = "select sum(a) from (select count(*) a from laassess a where agentgrade='A01' and agentgrade1='00'"
                              + "and Indexcalno='" + OneMonth +
                              "'and branchtype='1' and exists (select 'X' from "
                              +
                              "laagent b where a.agentcode=b.agentcode and EmployDate<=add_months('"
                              + OneMonth.substring(0, 4) + "-" +
                              OneMonth.substring(4, 6) + "-10',-8) ) union "
                              +
                              "select count(*) a from laassess a where agentgrade='A01' and agentgrade1='00'"
                              + "and Indexcalno='" + TwoMonth +
                              "'and branchtype='1' and exists (select 'X' from "
                              +
                              "laagent b where a.agentcode=b.agentcode and EmployDate<=add_months('"
                              + TwoMonth.substring(0, 4) + "-" +
                              TwoMonth.substring(4, 6) + "-10',-8) ) union "
                              +
                              "select count(*) a from laassess a where agentgrade='A01' and agentgrade1='00'"
                              + "and Indexcalno='" + ThreeMOnth +
                              "'and branchtype='1' and exists (select 'X' from "
                              +
                              "laagent b where a.agentcode=b.agentcode and EmployDate<=add_months('"
                              + ThreeMOnth.substring(0, 4) + "-" +
                              ThreeMOnth.substring(4, 6) + "-10',-8))  )";

                ExeSQL fExeSQL = new ExeSQL();
                strArr[4] = fExeSQL.getOneValue(fsql);
                data[4] += Double.parseDouble(strArr[4]);
                //占比
                if (strArr[1].equals("0"))
                {
                    strArr[5] = "0";
                }
                else
                {
                    strArr[5] = String.valueOf(Double.parseDouble(strArr[4]) /
                                               Double.parseDouble(strArr[1]));
                }
                //转正人数
                String dsql =
                        "select sum(a) from (select count(*) a from laagent a where InDueFormDate='" +
                        TwoMonth.substring(0, 4) + "-" +
                        TwoMonth.substring(4, 6) +
                        "-01' and exists (select 'X' from laassess b "
                        + "where a.agentcode=b.agentcode and Indexcalno ='" +
                        OneMonth
                        + "') and managecom like '" + ManageCom + "%'  union "
                        +
                        "select count(*) a from laagent a where InDueFormDate='" +
                        ThreeMOnth.substring(0, 4) + "-" +
                        ThreeMOnth.substring(4, 6) +
                        "-01' and exists (select 'X' from laassess b "
                        + "where a.agentcode=b.agentcode and Indexcalno ='" +
                        TwoMonth
                        + "') and managecom like '" + ManageCom + "%'  union "
                        +
                        "select count(*) a from laagent a where InDueFormDate=add_months('" +
                        ThreeMOnth.substring(0, 4) + "-" +
                        ThreeMOnth.substring(4, 6) +
                        "-01',1) and exists (select 'X' from laassess b "
                        + "where a.agentcode=b.agentcode and Indexcalno ='" +
                        ThreeMOnth + "') and managecom like '"
                        + ManageCom + "%'  )";
                ExeSQL dExeSQL = new ExeSQL();
                strArr[6] = dExeSQL.getOneValue(dsql);
                data[6] += Double.parseDouble(strArr[6]);
                //占比
                if (strArr[1].equals("0"))
                {
                    strArr[7] = "0";
                }
                else
                {
                    strArr[7] = String.valueOf(Double.parseDouble(strArr[6]) /
                                               Double.parseDouble(strArr[1]));
                }
            }
            else
            {
                String csql =
                        "select sum(a) from (select count(*) a from LAIndexInfo a where agentgrade='" +
                        aSSRS.GetText(1, a) + "' "
                        + "and t2='10' and IndexType='02'"
                        + "and Indexcalno='" + OneMonth +
                        "'and exists (select 'X' from "
                        +
                        "laagent b where a.agentcode=b.agentcode and EmployDate<=add_months('"
                        + OneMonth.substring(0, 4) + "-" +
                        OneMonth.substring(4, 6) + "-10',-2) ) union "
                        +
                        "select count(*) a from LAIndexInfo a where agentgrade='" +
                        aSSRS.GetText(1, a) + "' "
                        + "and t2='10' and IndexType='02'"
                        + "and Indexcalno='" + TwoMonth +
                        "'and  exists (select 'X' from "
                        +
                        "laagent b where a.agentcode=b.agentcode and EmployDate<=add_months('"
                        + TwoMonth.substring(0, 4) + "-" +
                        TwoMonth.substring(4, 6) + "-10',-2) ) union "
                        +
                        "select count(*) a from LAIndexInfo a where agentgrade='" +
                        aSSRS.GetText(1, a) + "' "
                        + "and t2='10' and IndexType='02'"
                        + "and Indexcalno='" + ThreeMOnth +
                        "'and exists (select 'X' from "
                        +
                        "laagent b where a.agentcode=b.agentcode and EmployDate<=add_months('"
                        + ThreeMOnth.substring(0, 4) + "-" +
                        ThreeMOnth.substring(4, 6) + "-10',-2))  )";
                ExeSQL cExeSQL = new ExeSQL();
                strArr[2] = cExeSQL.getOneValue(csql);
                data[2] += Double.parseDouble(strArr[2]);
                strArr[4] = "";
                strArr[5] = "";
                strArr[6] = "";
                strArr[7] = "";
            }
            //业绩为零占比
            if (strArr[1].equals("0"))
            {
                strArr[3] = "0";
            }
            else
            {
                strArr[3] = String.valueOf(Double.parseDouble(strArr[2]) /
                                           Double.parseDouble(strArr[1]));
            }
            //考核后人力
            String esql = "select count(*) from laassess where agentgrade1='" +
                          aSSRS.GetText(1, a)
                          + "' and Indexcalno in ('" + OneMonth + "','" +
                          TwoMonth + "','" + ThreeMOnth
                          + "') and branchtype='1' and ManageCom like '" +
                          ManageCom + "%'";
            ExeSQL eExeSQL = new ExeSQL();
            strArr[8] = eExeSQL.getOneValue(esql);
            data[8] += Double.parseDouble(strArr[8]);
            tlistTable.add(strArr);
            strArr = new String[9];
        }
        //展业机构：总计
        strArr = new String[9];
        strArr[0] = "合计";
        for (int u = 1; u <= 2; u++)
        {
            strArr[u] = getInt(String.valueOf(data[u]));
        }
        if (data[1] == 0)
        {
            strArr[3] = "0";
        }
        else
        {
            strArr[3] = String.valueOf(data[2] / data[1]);
        }
        strArr[4] = "";
        strArr[5] = "";
        strArr[6] = "";
        strArr[7] = "";
        strArr[8] = getInt(String.valueOf(data[8]));
        tlistTable.add(strArr);
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PolAssess.vts", "printer"); //最好紧接着就初始化xml文档
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
//     xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
        mResult.addElement(xmlexport);
        System.out.println("end");
        return true;
    }

    /*
       如果一个字符串数字中小数点后有值，则去掉小数点后面数值
     */
    public static String getInt(String Value)
    {
        String result = "";
        boolean mflag = true;
        int m = 0;
        m = Value.lastIndexOf(".");
        if (m == -1)
        {
            result = Value;
        }
        else
        {
            result = Value.substring(0, m);
        }
        return result;
    }

    public static void main(String[] args)
    {
        PolNBDepartF1PBL tPolNBDepartF1PBL = new PolNBDepartF1PBL();
    }
}