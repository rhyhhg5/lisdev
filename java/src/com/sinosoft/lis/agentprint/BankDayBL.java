package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author　ｚｙ
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class BankDayBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private GlobalInput mGlobalInput = new GlobalInput();
    private String StartDay = "";
    private String EndDay = "";
    //业务处理相关变量
    /** 全局数据 */
    public BankDayBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        StartDay = (String) cInputData.get(0);
        EndDay = (String) cInputData.get(1);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "DepartOrderBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String strArr[] = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("BankDay");
        String Year = StartDay.substring(0, 4) + "-01-01";
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDay", StartDay); //输入制表时间
        texttag.add("EndDay", EndDay);
        strArr = new String[12];
        double data[] = null;
        data = new double[12];
        double data1[] = null;
        data1 = new double[12];
        //区分公司名称、代码
        String asql =
                "select trim(shortname),comcode from ldcom where length(trim(comcode))=4";
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(asql);
            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                for (int b = 1; b <= aSSRS.getMaxCol(); b++)
                {
                    if (b == 1)
                    {
                        //分公司名称
                        strArr[0] = aSSRS.GetText(a, 1);
                    }
                    if (b == 2)
                    {

                        String bsql = "select trim(name),agentcom from lacom where (banktype='01' or banktype is null) and managecom like '" +
                                      aSSRS.GetText(a, 2) +
                                      "%' order by agentcom";
                        SSRS bSSRS = new SSRS();
                        bSSRS = aExeSQL.execSQL(bsql);
                        for (int c = 1; c <= bSSRS.getMaxRow(); c++)
                        {
                            for (int d = 1; d <= bSSRS.getMaxCol(); d++)
                            {
                                if (d == 1)
                                {
                                    //银行名称
                                    strArr[1] = bSSRS.GetText(c, 1);
                                }
                                if (d == 2)
                                {
                                    //全年累计承保保费
                                    String csql =
                                            "select nvl(sum(prem),0) from Lcpol where makeDate>='" +
                                            Year + "' and makeDate<='" + EndDay +
                                            "' and AgentCom like '" +
                                            bSSRS.GetText(c, 2) + "%'";
                                    strArr[2] = aExeSQL.getOneValue(csql);
                                    data[2] += Double.parseDouble(strArr[2]);
                                    //本旬承保保费
                                    String dsql =
                                            "select nvl(sum(prem),0) from Lcpol where makeDate>='" +
                                            StartDay + "' and makeDate<='" +
                                            EndDay + "' and AgentCom like '" +
                                            bSSRS.GetText(c, 2) + "%'";
                                    strArr[3] = aExeSQL.getOneValue(dsql);
                                    data[3] += Double.parseDouble(strArr[3]);
                                    //网点数
                                    String fsql = "select count(distinct agentcom) from lacom where sellflag='Y' and CalFlag='Y' and agentcom like '" +
                                                  bSSRS.GetText(c, 2) + "%' ";
                                    strArr[4] = aExeSQL.getOneValue(fsql);
                                    data[4] += Double.parseDouble(strArr[4]);
                                    //本旬网均产能
                                    if (strArr[4].equals("0"))
                                    {
                                        strArr[5] = "0";
                                    }
                                    else
                                    {
                                        strArr[5] = String.valueOf(Double.
                                                parseDouble(strArr[3]) /
                                                Double.parseDouble(strArr[4]));
                                    }
                                    //出单网点数
                                    String gsql =
                                            "select count(distinct agentcom) from Lacommision where tmakeDate>='" +
                                            StartDay + "' and tmakeDate<='" +
                                            EndDay +
                                            "' and transmoney is not null and AgentCom like '" +
                                            bSSRS.GetText(c, 2) + "%'";
                                    strArr[6] = aExeSQL.getOneValue(gsql);
                                    data[6] += Double.parseDouble(strArr[6]);
                                    //网点合格率
                                    if (strArr[4].equals("0"))
                                    {
                                        strArr[7] = "0";
                                    }
                                    else
                                    {
                                        strArr[7] = String.valueOf(Double.
                                                parseDouble(strArr[6]) /
                                                Double.parseDouble(strArr[4]));
                                    }
                                    //渠道人力
                                    String hsql = "select count(distinct agentcode) from lacomtoagent where RelaType='1' and AgentCom like '" +
                                                  bSSRS.GetText(c, 2) + "%'";
                                    strArr[8] = aExeSQL.getOneValue(hsql);
                                    data[8] += Double.parseDouble(strArr[8]);
                                    //人均网点数
                                    if (strArr[8].equals("0"))
                                    {
                                        strArr[9] = "0";
                                    }
                                    else
                                    {
                                        strArr[9] = String.valueOf(Double.
                                                parseDouble(strArr[4]) /
                                                Double.parseDouble(strArr[8]));
                                    }
                                    //人均产能
                                    if (strArr[8].equals("0"))
                                    {
                                        strArr[10] = "0";
                                    }
                                    else
                                    {
                                        strArr[10] = String.valueOf(Double.
                                                parseDouble(strArr[3]) /
                                                Double.parseDouble(strArr[8]));
                                    }
                                    //问题件
                                    tlistTable.add(strArr);
                                    strArr = new String[12];
                                }
                            }
                        }

                        //小合计
                        strArr[0] = "合计";
                        for (int i = 2; i <= 4; i++)
                        {
                            strArr[i] = String.valueOf(data[i]);
                            data1[i] += data[i];
                        }
                        //本旬网均产能
                        if (data[4] == 0)
                        {
                            strArr[5] = "0";
                        }
                        else
                        {
                            strArr[5] = String.valueOf(data[3] / data[4]);
                        }
                        strArr[6] = String.valueOf(data[6]);
                        data1[6] += data[6];
                        //网点合格率
                        if (data[4] == 0)
                        {
                            strArr[7] = "0";
                        }
                        else
                        {
                            strArr[7] = String.valueOf(data[6] / data[4]);
                        }
                        strArr[8] = String.valueOf(data[8]);
                        data1[8] += data[8];
                        //人均网点数
                        if (data[8] == 0)
                        {
                            strArr[9] = "0";
                        }
                        else
                        {
                            strArr[9] = String.valueOf(data[4] / data[8]);
                        }
                        //人均产能
                        if (data[8] == 0)
                        {
                            strArr[10] = "0";
                        }
                        else
                        {
                            strArr[10] = String.valueOf(data[3] / data[8]);
                        }
                        tlistTable.add(strArr);
                        strArr = new String[12];
                        data = new double[12];
                    }
                }
            }
            strArr = new String[12];
            strArr[0] = "总计";
            for (int i = 2; i <= 4; i++)
            {
                strArr[i] = String.valueOf(data1[i]);
            }
            //本旬网均产能
            if (data1[4] == 0)
            {
                strArr[5] = "0";
            }
            else
            {
                strArr[5] = String.valueOf(data1[3] / data1[4]);
            }
            strArr[6] = String.valueOf(data1[6]);
            //网点合格率
            if (data1[4] == 0)
            {
                strArr[7] = "0";
            }
            else
            {
                strArr[7] = String.valueOf(data1[6] / data1[4]);
            }
            strArr[8] = String.valueOf(data1[8]);
            //人均网点数
            if (data1[8] == 0)
            {
                strArr[9] = "0";
            }
            else
            {
                strArr[9] = String.valueOf(data1[4] / data1[8]);
            }
            //人均产能
            if (data1[8] == 0)
            {
                strArr[10] = "0";
            }
            else
            {
                strArr[10] = String.valueOf(data1[3] / data1[8]);
            }
            tlistTable.add(strArr);
            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("BankDay.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankDayBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    public static void main(String[] args)
    {
    }
}