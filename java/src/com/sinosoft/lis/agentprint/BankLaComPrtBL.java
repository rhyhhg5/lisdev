package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class BankLaComPrtBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mAgentCode = "";
    private String mBranchAttr = "";
    private String mAgentCom = "";
    private String mStartDate = "";
    private String mEndDate = "";
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    public BankLaComPrtBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        this.mAgentCode = (String) cInputData.get(0);
        this.mBranchAttr = (String) cInputData.get(1);
        this.mAgentCom = (String) cInputData.get(2);
        this.mStartDate = (String) cInputData.get(3);
        this.mEndDate = (String) cInputData.get(4);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 5);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "DepartOrderBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String strArr[] = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("BankMonth");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDay", this.mStartDate); //输入制表时间
        texttag.add("EndDay", this.mEndDate); //输入制表时间
        strArr = new String[10];
        String tAgentcode = "";
        String tBranchAttr = "";
        String tAgentCom = "";
        if (!this.mAgentCode.equals("") && this.mAgentCode != null)
        {
            tAgentcode = " and a.agentcode='" + this.mAgentCode + "'";
        }
        if (!this.mBranchAttr.equals("") && this.mBranchAttr != null)
        {
            tBranchAttr = " and a.branchattr='" + this.mBranchAttr + "'";
        }
        if (!this.mAgentCom.equals("") && this.mAgentCom != null)
        {
            tAgentCom = " and a.agentcom='" + this.mAgentCom + "'";
        }

        String asql = "select a.polno,c.riskname,a.transmoney,a.tmakedate,a.getpoldate,b.name,a.agentcode,a.Fyc,a.Grpfyc,a.Depfyc "
                      + "from lmriskapp c,lacom b,lacommision a where "
                      +
                      "a.agentcom= b.agentcom and a.branchtype='3' and a.riskcode=c.riskcode "
                      + " and not exists (select 'X' from lacomtoagent where agentcode=a.agentcode and agentcom=a.agentcom and relatype='1')"
                      + " and a.managecom like '" + this.mGlobalInput.ManageCom +
                      "%'"
                      + " and tmakedate>='" + this.mStartDate +
                      "' and tmakedate<='" + this.mEndDate + "'"
                      + tAgentcode + tBranchAttr + tAgentCom
                      + " order by a.polno";
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(asql);
            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                for (int b = 1; b <= aSSRS.getMaxCol(); b++)
                {
                    strArr[b - 1] = aSSRS.GetText(a, b);
                }
                tlistTable.add(strArr);
                strArr = new String[10];
            }

            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("BankLACom.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
            xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankLaComBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    /*
       如果一个字符串数字中小数点后有值，则去掉小数点后面数值
     */
    public static String getInt(String Value)
    {
        String result = "";
        boolean mflag = true;
        int m = 0;
        m = Value.lastIndexOf(".");
        if (m == -1)
        {
            result = Value;
        }
        else
        {
            result = Value.substring(0, m);
        }
        return result;
    }

    public static void main(String[] args)
    {
    }
}