package com.sinosoft.lis.agentprint;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.agentprint.YearBonusPrintBL;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: </p>
 *
 * @author XX
 * @version 1.0
 */
public class YearBonusPrintUI{

    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();


    public YearBonusPrintUI() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("cont boe");
        YearBonusPrintBL tYearBonusPrintBL = new YearBonusPrintBL();

        if (!tYearBonusPrintBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tYearBonusPrintBL.mErrors);

            return false;
        } else {
            this.mResult = tYearBonusPrintBL.getResult();
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }


    public static void main(String[] args) {

    }
}
