package com.sinosoft.lis.agentprint;

import java.sql.Connection;
import java.util.Arrays;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class HeadPolFilialePrt
{
    public GlobalInput mGI = new GlobalInput();
    private String mDay = "";
    private String mStaticOrg = "";
    public VData mResult = new VData();
    public CErrors mErrors = new CErrors();

    public HeadPolFilialePrt()
    {
    }

    public static void main(String[] args)
    {
        HeadPolFilialePrt headPolFilialePrt1 = new HeadPolFilialePrt();
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86110000";
        tGI.ManageCom = "86";
        tGI.Operator = "aa";
        VData tInputData = new VData();
        tInputData.clear();
        tInputData.addElement(tGI);
        tInputData.add(0, "2003-12-1");
        tInputData.add(2, "8611");

        if (!headPolFilialePrt1.getInputData(tInputData))
        {
            System.out.println("-----getinputdata failed!------");
        }
        if (!headPolFilialePrt1.prepareData())
        {
            System.out.println("-------prepareData failed!------");
        }

    }

    public boolean getInputData(VData tInputData)
    {
        try
        {
            this.mDay = (String) tInputData.getObjectByObjectName("String", 0);
            this.mStaticOrg = (String) tInputData.getObjectByObjectName(
                    "String", 1);
            this.mGI = (GlobalInput) tInputData.getObjectByObjectName(
                    "GlobalInput",
                    0);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            dealError("getInputData", ex.toString());
            return false;
        }
        System.out.println("------------getInputData------------");
        System.out.println("统计时间：" + this.mDay);
        System.out.println("统计机构：" + this.mStaticOrg);
        System.out.println("------------------------------------");
        return true;
    }

    public boolean prepareData()
    {

        double data[] = null;
        data = new double[20];

        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);

            String Sql = "";
            SSRS ssrs = new SSRS();
            Sql = "select trim(shortName) from ldcom where trim(comcode) = '"
                  + this.mStaticOrg.trim() + "'";
            String tName = aExeSQL.getOneValue(Sql).trim();

            ListTable tListTab = new ListTable();
            tListTab.setName("POLFI");

            TextTag texttag = new TextTag(); //新建一个TextTag的实例
            String tStaticDate = "";
            tStaticDate = this.mDay.trim();
            texttag.add("Day", tStaticDate); //输入制表时间
            texttag.add("Name", "制表:" + tName.trim());
            int tLength = 0;
            if (this.mStaticOrg.trim().length() != 8)
            {
                tLength = this.mStaticOrg.trim().length() + 2;
            }
            else
            {
                tLength = this.mStaticOrg.trim().length();
            }
            Sql = "select trim(comcode),trim(shortname) from ldcom where "
                  + " length(trim(comcode)) = " + tLength
                  + " and trim(comcode) like '" + this.mStaticOrg.trim() +
                  "%' "
                  + " order by comcode";
            ssrs = aExeSQL.execSQL(Sql);
            int tCount = 0;
            tCount = ssrs.getMaxRow();
            System.out.println("-----tCount = " + tCount);
            String[][] strArr = null;
            String tSql = "";
            PubFun1 tPF = new PubFun1();

            String tDay[] = tPF.calFLDate(this.mDay.trim());

            strArr = new String[tCount][20];

            for (int i = 1; i <= tCount; i++)
            {

                strArr[i - 1][0] = ssrs.GetText(i, 2).trim();
                String tComCode = ssrs.GetText(i, 1).trim();
                System.out.println("----tComCode = " + tComCode);

                //月初人力
                tSql = "select nvl(count(*),0) from LAAgent where "
                       + " EmployDate < '" + tDay[0].trim() +
                       "' and agentstate in ('01','02')"
                       + " and branchcode in (select agentgroup from labranchgroup where branchtype = '1' and (state<>'1' or state is null)"
                       + " and branchattr like '" + tComCode.trim() + "%')";
                strArr[i - 1][1] = aExeSQL.getOneValue(tSql).trim();
                data[1] += Double.parseDouble(strArr[i - 1][1]);

                //预收规模本日
                tSql =
                        "select NVL(sum(prem),0) from ljtempfee_lmriskapp where "
                        + "  MakeDate ='" + mDay.trim()
                        + "' and agentgroup in "
                        + "(select agentgroup from labranchgroup where branchtype='1' and (state<>'1' or state is null)"
                        + " and branchattr like '" + tComCode.trim() + "%')";
                strArr[i - 1][2] = aExeSQL.getOneValue(tSql).trim();
                data[2] += Double.parseDouble(strArr[i - 1][2]);

                //预收规模本月
                tSql =
                        "select NVL(sum(prem),0) from ljtempfee_lmriskapp where "
                        + " MakeDate >='" + tDay[0].trim() +
                        "' and MakeDate <='" + mDay.trim() + "'"
                        +
                        " and agentgroup in (select agentgroup from labranchgroup where "
                        + " branchtype='1' and (state<>'1' or state is null)"
                        + " and branchattr like '" + tComCode.trim() + "%')";
                strArr[i - 1][3] = aExeSQL.getOneValue(tSql).trim();
                data[3] += Double.parseDouble(strArr[i - 1][3]);

                //预收规模本日
                tSql =
                        "select NVL(sum(prem),0) from ljtempfee_lmriskapp where "
                        + "  MakeDate ='" + mDay.trim()
                        + "' and payintv<>0 and agentgroup in "
                        + "(select agentgroup from labranchgroup where branchtype='1' and (state<>'1' or state is null)"
                        + " and branchattr like '" + tComCode.trim() + "%')";
                strArr[i - 1][4] = aExeSQL.getOneValue(tSql).trim();
                data[4] += Double.parseDouble(strArr[i - 1][4]);

                //预收规模本月
                tSql =
                        "select NVL(sum(prem),0) from ljtempfee_lmriskapp where "
                        + " MakeDate >='" + tDay[0].trim() +
                        "' and MakeDate <='" + mDay.trim() + "'"
                        +
                        " and payintv<>0 and agentgroup in (select agentgroup from labranchgroup where "
                        + " branchtype='1' and (state<>'1' or state is null)"
                        + " and branchattr like '" + tComCode.trim() + "%')";
                strArr[i - 1][5] = aExeSQL.getOneValue(tSql).trim();
                data[5] += Double.parseDouble(strArr[i - 1][5]);

                //承保规模本日
                tSql =
                        "select NVL(sum(SumActuPayMoney),0) from ljapayperson_lmriskapp2 "
                        + "where makedate ='" + mDay.trim()
                        +
                        "' and agentgroup in (select agentgroup from labranchgroup where "
                        + " branchtype='1' and (state<>'1' or state is null)"
                        + " and branchattr like '" + tComCode.trim() + "%')";
                strArr[i - 1][6] = aExeSQL.getOneValue(tSql).trim();
                data[6] += Double.parseDouble(strArr[i - 1][6]);

                //承保规模本月
                tSql =
                        "select NVL(sum(SumActuPayMoney),0) from ljapayperson_lmriskapp2 where"
                        + " makedate >='" + tDay[0].trim() +
                        "' and makedate <='" + mDay.trim()
                        +
                        "' and agentgroup in (select agentgroup from labranchgroup "
                        +
                        "where branchtype='1' and (state<>'1' or state is null) and branchattr like '" +
                        tComCode.trim() + "%')";
                strArr[i - 1][7] = aExeSQL.getOneValue(tSql).trim();
                data[7] += Double.parseDouble(strArr[i - 1][7]);

                //承保标保本日
                tSql = "select StandpremCBBranch('" + mDay.trim() + "','"
                       + mDay.trim() + "','" + tComCode.trim()
                       + "') from ldsysvar where sysvar='onerow'";
                strArr[i - 1][8] = aExeSQL.getOneValue(tSql).trim();
                data[8] += Double.parseDouble(strArr[i - 1][8]);

                //承保标保本月
                tSql = "select StandpremCBBranch('" + tDay[0].trim()
                       + "','" + mDay.trim() + "','" + tComCode.trim()
                       + "') from ldsysvar where sysvar='onerow'";
                strArr[i - 1][9] = aExeSQL.getOneValue(tSql).trim();
                data[9] += Double.parseDouble(strArr[i - 1][9]);

                //月承保件数
                tSql = "select calpieceCBBranch('" + tDay[0].trim() + "','"
                       + mDay.trim() + "','" + tComCode.trim()
                       + "') from ldsysvar where sysvar='onerow'";
                strArr[i - 1][10] = aExeSQL.getOneValue(tSql).trim();
                data[10] += Double.parseDouble(strArr[i - 1][10]);

                //人均标保
                if (strArr[i - 1][1].trim().equals("0"))
                {
                    strArr[i - 1][11] = "0";
                }
                else
                {
                    strArr[i -
                            1][11] = Float.toString(Float.parseFloat(strArr[i -
                            1][9].trim()) /
                            Integer.parseInt(strArr[i - 1][1].trim())).trim();
                }

                //人均件数
                if (strArr[i - 1][1].trim().equals("0"))
                {
                    strArr[i - 1][12] = "0";
                }
                else
                {
                    strArr[i -
                            1][12] = Float.toString(Float.parseFloat(strArr[i -
                            1][10].trim()) /
                            Integer.parseInt(strArr[i - 1][1].trim())).trim();
                }

                //件均标保
                if (strArr[i - 1][10].trim().equals("0"))
                {
                    strArr[i - 1][13] = "0";
                }
                else
                {
                    strArr[i -
                            1][13] = Float.toString(Float.parseFloat(strArr[i -
                            1][9].trim())
                            / Float.parseFloat(strArr[i - 1][10].trim())).trim();
                }

                //活动率
                int num = 0;
                tSql =
                        "select nvl(count( distinct agentcode),0) from ljapayperson where "
                        + " makeDate >='" + tDay[0].trim() +
                        "' and makeDate <='" + mDay.trim() + "'"
                        +
                        " and agentgroup in (select agentgroup from labranchgroup where "
                        + " branchtype='1' and (state<>'1' or state is null)"
                        + " and branchattr like '" + tComCode.trim() + "%' )";
                num = Integer.parseInt(aExeSQL.getOneValue(tSql).trim());
                if (strArr[i - 1][1].trim().equals("0"))
                {
                    strArr[i - 1][14] = "0";
                }
                else
                {
                    strArr[i -
                            1][14] = Float.toString(num /
                            Float.parseFloat(strArr[i - 1][1].trim())).trim();
                }
                data[14] += num;

                //人均产能
                if (num == 0)
                {
                    strArr[i - 1][15] = "0";
                }
                else
                {
                    strArr[i -
                            1][15] = Float.toString(Float.parseFloat(strArr[i -
                            1][9].trim()) / num).trim();
                }

                //有效人力
                tSql =
                        "select nvl(count(count(*)),0) from ljapayperson a,laagent b where "
                        + " a.makedate >='" + tDay[0].trim() +
                        "' and a.makedate <='" + mDay.trim() + "'"
                        + " and a.agentcode=b.agentcode and branchcode in "
                        + "(select agentgroup from labranchgroup where "
                        + " branchtype='1' and (state<>'1' or state is null)"
                        + " and branchattr like '" + tComCode.trim() + "%')"
                        +
                        " group by a.agentcode having NVL(sum(SumActuPayMoney),0)>'2000'";
                strArr[i - 1][16] = aExeSQL.getOneValue(tSql).trim();
                data[16] += Double.parseDouble(strArr[i - 1][16]);

                //月任务计划
                String Month = "";
                if (mDay.substring(6, 7).equals("-"))
                {
                    Month = mDay.substring(0, 4) + "0" + mDay.substring(5, 6);
                }
                else
                {
                    Month = mDay.substring(0, 4) + mDay.substring(5, 7);
                }
                tSql = "select planvalue from laplan where trim(plantype)='2' and PlanPeriodUnit=1 and PlanPeriod='" +
                       Month + "' and trim(planobject)='"
                       + tComCode.trim() + "'";
                String zan = aExeSQL.getOneValue(tSql).trim();
                if (zan != null && !zan.equals(""))
                {
                    strArr[i - 1][17] = zan;
                }
                else
                {
                    strArr[i - 1][17] = "0";
                }
                data[17] += Double.parseDouble(strArr[i - 1][17]);

                //月计划达成率
                if (strArr[i - 1][17].trim().equals("0"))
                {
                    strArr[i - 1][18] = "0";
                }
                else
                {
                    strArr[i -
                            1][18] = Float.toString(Float.parseFloat(strArr[i -
                            1][7].trim())
                            / Float.parseFloat(zan.trim()));
                }
            }

            //月达成率排名
            double sort[] = new double[tCount];
            for (int i = 0; i < tCount; i++)
            {
                if (!strArr[i][18].equals("") && strArr[i][18] != null)
                {
                    sort[i] = Double.parseDouble(strArr[i][18].trim());
                }
                else
                {
                    sort[i] = Double.parseDouble("0");
                }
            }
            Arrays.sort(sort);
            for (int i = 0; i < tCount; i++)
            {
                for (int j = 0; j < tCount; j++)
                {
                    if (!String.valueOf(sort[j]).equals("") &&
                        String.valueOf(sort[j]) != null &&
                        !strArr[i][18].equals("") && strArr[i][18] != null)
                    {
                        if (Double.parseDouble(strArr[i][18]) == sort[j])
                        {
                            strArr[i][19] = String.valueOf(tCount - j);
                            break;
                        }
                    }
                    else
                    {
                        strArr[i][19] = "";
                    }
                }
            }

            String[] eachList = null;
            for (int i = 0; i < tCount; i++)
            {
                eachList = new String[20];
                for (int j = 0; j < 20; j++)
                {
                    eachList[j] = strArr[i][j];
                }
                tListTab.add(eachList);
            }

            eachList = new String[20];
            eachList[0] = "总计";
            //网点数
            for (int i = 1; i <= 10; i++)
            {
                eachList[i] = String.valueOf(data[i]);
            }
            eachList[16] = String.valueOf(data[16]);
            eachList[17] = String.valueOf(data[17]);

            //人均标保
            if (data[1] == 0)
            {
                eachList[11] = "0";
            }
            else
            {
                eachList[11] = String.valueOf(data[9] / data[1]);
            }

            //人均件数
            if (data[1] == 0)
            {
                eachList[12] = "0";
            }
            else
            {
                eachList[12] = String.valueOf(data[10] / data[1]);
            }

            //件均标保
            if (data[10] == 0)
            {
                eachList[13] = "0";
            }
            else
            {
                eachList[13] = String.valueOf(data[9] / data[10]);
            }
            //活动率
            if (data[1] == 0)
            {
                eachList[14] = "0";
            }
            else
            {
                eachList[14] = String.valueOf(data[14] / data[1]);
            }

            //人均产能
            if (data[14] == 0)
            {
                eachList[15] = "0";
            }
            else
            {
                eachList[15] = String.valueOf(data[9] / data[14]);
            }

            //月计划达成率
            if (data[17] == 0)
            {
                eachList[18] = "0";
            }
            else
            {
                eachList[18] = String.valueOf(data[7] / data[17]);
            }

            //总公司不参加排名
            strArr[0][19] = "";

            tListTab.add(eachList);

            XmlExport xmlexport = new XmlExport();
            xmlexport.createDocument("HeadPolFiliale.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tListTab, eachList);
            xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
            this.mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "HeadPolFilialePrt";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("------prepareData end-----");

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void dealError(String FuncName, String ErrMsg)
    {
        CError error = new CError();

        error.moduleName = "HeadPolFilialePrt";
        error.errorMessage = ErrMsg.trim();
        error.functionName = FuncName.trim();

        this.mErrors.addOneError(error);
    }

}