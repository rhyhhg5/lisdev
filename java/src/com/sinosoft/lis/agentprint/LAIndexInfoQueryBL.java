package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LAIndexInfoQueryBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    private String tManageCom;
    private String tIndexCalNo;
    private String tAgentGrade;
    private String tIndexType;
    private String tAgentCode;

    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    private String tsql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public LAIndexInfoQueryBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        this.tManageCom = (String) cInputData.get(0);
        this.tIndexCalNo = (String) cInputData.get(1);
        this.tAgentGrade = (String) cInputData.get(2);
        this.tIndexType = (String) cInputData.get(3);
        this.tAgentCode = (String) cInputData.get(4);
        //mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",5));
        System.out.println("start");
        System.out.println(mGlobalInput.Operator);
        System.out.println(mGlobalInput.ManageCom);

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }


    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCPolBillF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getPrintData()
    {
        String strArr[] = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("LAIndexInfoQuery");
        strArr = new String[51];
        tsql = "select a.AgentCode,c.name,b.AgentGrade,decode(a.IndexType,'02','维持','03','晋升'),"
               + "a.BranchAttr,d.name,"
               + "a.StartDate,a.StartEnd,a.IndCustSum,a.IndFYCSum,a.MonAvgFYC,a.MonAvgCust,a.IndRate,"
               +
               "decode(a.T1,'10','是','11','否'),decode(a.FYCZeroMon,'0','无','1','有'),"
               + "a.DirAddCount,a.AddCount,a.IncFinaMngCount,decode(a.T2,'10','是','11','否'),a.IndAddFYCSum,"
               + "a.DirMngCount,a.DirMngFinaCount,a.TeamAvgRate,a.DRTeamCount,a.RearTeamSum,a.DirTeamFYCSum,"
               + "a.T4,a.T5,decode(a.IndexType,'02',(a.T4 - (-a.DirTeamFYCSum)),'03',(a.T5 - (-a.DirTeamFYCSum))) a1,"
               +
               "a.DRTeamAvgRate,a.DRFYCSum,a.DRTeamMonLabor,a.DepFYCSum,a.T13,a.T5,"
               + "decode(a.IndexType,'02',(a.T13 - (-a.DepFYCSum)),'03',(a.T5 - (-a.DepFYCSum))) a2,"
               + "a.DRearDepCount,a.RearDepCount,a.DirDepMonAvgFYC,a.T19,a.DepAvgRate,a.DRDepMonLabor,"
               + "a.DRDepAvgRate,a.DepFinaCount,a.T16,a.T12,a.RearAdmCount,a.BranComFinaCount,a.T20,"
               + "a.AreaRate,a.T3 "
               + "from LAIndexInfo a,LAAssess b,LAAgent c,LABranchGroup d "
               + "where a.AgentCode = b.AgentCode and a.agentcode=c.agentcode and a.branchattr=d.branchattr and a.IndexCalNo = b.IndexCalNo "
               + "and a.ManageCom = b.ManageCom  and IndexType in ('02','03')";
        if (this.tManageCom != null && !this.tManageCom.equals(""))
        {
            tsql += " And a.ManageCom like '" + this.tManageCom + "'";
        }
        if (this.tIndexCalNo != null && !this.tIndexCalNo.equals(""))
        {
            tsql += " And a.IndexCalNo = '" + this.tIndexCalNo + "'";
        }
        if (this.tAgentGrade != null && !this.tAgentGrade.equals(""))
        {
            tsql += " And a.AgentGrade = '" + this.tAgentGrade + "'";
        }
        if (this.tIndexType != null && !this.tIndexType.equals(""))
        {
            tsql += " And a.IndexType = '" + this.tIndexType + "'";
        }
        if (this.tAgentCode != null && !this.tAgentCode.equals(""))
        {
            tsql += " And a.AgentCode = '" + this.tAgentCode + "'";
        }
        tsql += " ORDER BY a.IndexType";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(tsql);
        for (int a = 1; a <= tSSRS.getMaxRow(); a++)
        {
            for (int b = 1; b <= tSSRS.getMaxCol(); b++)
            {
                strArr[b - 1] = tSSRS.GetText(a, b);
            }
            tlistTable.add(strArr);
            strArr = new String[51];
        }
        //strArr = new String[51];
        //strArr[0] = "营业组"; strArr[1] ="姓名";strArr[2] = "代码"; strArr[3] ="职级";strArr[4] ="主管属性";strArr[5] ="入司时间";strArr[6] ="代理人状态";strArr[7] ="入司时间";strArr[8] ="代理人状态";
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        //texttag.add("StartDay",mStartDate);//输入制表时间
        //texttag.add("EndDay",mEndDate);//输入制表时间
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("LAIndexInfoQuery.vts", "printer"); //最好紧接着就初始化xml文档
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        xmlexport.outputDocumentToFile("e:\\", "test"); //输出xml文档到文件
        mResult.addElement(xmlexport);
        return true;

    }

//  public static void main(String[] args) {
//    String  tManageCom= "86320000";
//    String  tIndexCalNo="200311";
//    String  tAgentGrade="A01";
//    String  tIndexType="维持";
//    String  tAgentCode="8632000026";
//    VData tVData = new VData();
//    tVData.addElement(tManageCom);
//    tVData.addElement(tIndexCalNo);
//    tVData.addElement(tAgentGrade);
//    tVData.addElement(tIndexType);
//    tVData.addElement(tAgentCode);
//    LAIndexInfoQueryBL tLAIndexInfoQueryUI = new LAIndexInfoQueryBL();
//    tLAIndexInfoQueryUI.submitData(tVData,"PRINT");
//  }
}