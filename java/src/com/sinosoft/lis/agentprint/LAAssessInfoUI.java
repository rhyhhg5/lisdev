package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAAssessInfoUI
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String IndexCalNo;
    private String AssessType;
    private String AgentGrade;
    private String ManageCom;
    private String BranchType;
    private String StandAssessFlag;
    public LAAssessInfoUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }

            // 准备传往后台的数据
            VData vData = new VData();

            if (!prepareOutputData(vData))
            {
                return false;
            }

            LAAssessInfoBL tLAAssessInfoBL = new LAAssessInfoBL();
            System.out.println("Start LAAssessInfoUI Submit ...");

            if (!tLAAssessInfoBL.submitData(vData, cOperate))
            {
                if (tLAAssessInfoBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tLAAssessInfoBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData",
                               "AgentTrussPF1PBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tLAAssessInfoBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CError cError = new CError();
            cError.moduleName = "LAAssessInfoUI";
            cError.functionName = "submit";
            cError.errorMessage = e.toString();
            mErrors.addOneError(cError);
            return false;
        }
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.add(IndexCalNo);
            vData.add(AssessType);
            vData.add(AgentGrade);
            vData.add(ManageCom);
            vData.add(BranchType);
            vData.add(StandAssessFlag);
            vData.add(mGlobalInput);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        IndexCalNo = ((String) cInputData.getObjectByObjectName("String", 0));
        AssessType = ((String) cInputData.getObjectByObjectName("String", 1));
        AgentGrade = ((String) cInputData.getObjectByObjectName("String", 2));
        ManageCom = ((String) cInputData.getObjectByObjectName("String", 3));
        BranchType = ((String) cInputData.getObjectByObjectName("String", 4));
        StandAssessFlag = ((String) cInputData.getObjectByObjectName("String",
                5));
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 6));
        System.out.println("start");
        System.out.println(mGlobalInput.Operator);
        System.out.println(mGlobalInput.ManageCom);

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LAAssessInfoUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args)
    {
        AgentTrussPF1PUI agentTrussPF1PUI1 = new AgentTrussPF1PUI();
    }
}