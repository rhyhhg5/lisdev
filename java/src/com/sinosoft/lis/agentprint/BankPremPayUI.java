package com.sinosoft.lis.agentprint;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.agentprint.BankPremPayBL;

/**
 *
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: </p>
 *
 * @author XX
 * @version 1.0
 */
public class BankPremPayUI{

    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();


    public BankPremPayUI() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("cont boe");
        BankPremPayBL tBankPremPayBL = new BankPremPayBL();

        if (!tBankPremPayBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tBankPremPayBL.mErrors);

            return false;
        } else {
            this.mResult = tBankPremPayBL.getResult();
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }


    public static void main(String[] args) {

    }
}
