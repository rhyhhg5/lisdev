package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class PolAnnualOrderBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private String StartDay = "";
    private String EndDay = "";
    private String mManageCom = "";
//  double date3[]= new double[20];//定义总计的数组
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    public PolAnnualOrderBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        this.StartDay = (String) cInputData.get(0);
        this.EndDay = (String) cInputData.get(1);
        mManageCom = (String) cInputData.get(2);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "PolNewBDayBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String strArr[] = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("Ann");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDay", StartDay); //输入制表时间
        texttag.add("EndDay", EndDay); //输入制表时间

        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        ExeSQL aExeSQL = new ExeSQL(conn);
        try
        {
            //计算区和支公司共有多少
            double date[] = new double[6]; //合计
            //查询支公司
            String aasql =
                    "select b from (select trim(comcode) a,shortname b from ldcom union"
                    +
                    " select trim(branchattr) a,name b From labranchgroup where branchtype='1'"
                    + " and (state<>1 or state is null))"
                    + " where a ='" + this.mManageCom + "'";
            texttag.add("ManageCom", aExeSQL.getOneValue(aasql)); //输入制表时间
            nsql =
                    "select a.name,b.name,b.agentcode,sum(transmoney),agentgrade "
                    + "from labranchgroup a,laagent b,lacommision c,latree d where b.agentcode=c.agentcode and "
                    + "b.agentcode=d.agentcode and b.branchtype='1' and b.branchcode=a.agentgroup and branchlevel='01' "
                    +
                    "and payintv<>0 and (paycount=1 or paycount=0) and c.branchattr like '" +
                    mManageCom + "%' and c.branchattr like '" +
                    mGlobalInput.ManageCom
                    + "%' and tmakedate>='" + StartDay + "' and tmakedate<='" +
                    EndDay + "' group by a.name,b.name,b.agentcode,agentgrade "
                    + "having sum(transmoney)<>0 order by sum(transmoney) desc";

            SSRS ySSRS = new SSRS();
            ySSRS = aExeSQL.execSQL(nsql);
            int Row = 3000;
            if (ySSRS.getMaxRow() < 3000)
            {
                Row = ySSRS.getMaxRow();
            }
            for (int z = 1; z <= Row; z++)
            {
                strArr = new String[6];
                strArr[0] = String.valueOf(z);
                for (int u = 1; u <= 5; u++)
                {
                    strArr[u] = ySSRS.GetText(z, u);
                }
                tlistTable.add(strArr);
            }

            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PolAnnualOrderBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("PolAnnualOrder.vts", "printer"); //最好紧接着就初始化xml文档
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
        mResult.addElement(xmlexport);
        System.out.println("end");
        return true;
    }

    public static void main(String[] args)
    {
        PolNBDepartF1PBL tPolNBDepartF1PBL = new PolNBDepartF1PBL();
    }
}