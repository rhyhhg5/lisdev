package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import com.sinosoft.lis.agentwages.AgentWageCalSetWageNoReBL;
import com.sinosoft.lis.db.LAStatSegmentDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LAStatSegmentSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class LAIndexTempBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    private String mManageCom;
    private String mAgentCode;
    private String mAgentGrade;
    private String mStartDate;
    private String mEndDate;
    private String mBranchType;

    //增加两个变量，用来记录统计期间
    private String mStartNo;
    private String mEndNo;
    //统计间隔月数
    private String mIntvl;

    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public LAIndexTempBL()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        this.mManageCom = (String) cInputData.get(0);
        this.mAgentCode = (String) cInputData.get(1);
        this.mAgentGrade = (String) cInputData.get(2);
        this.mStartDate = (String) cInputData.get(3);
        this.mEndDate = (String) cInputData.get(4);
        this.mBranchType = (String) cInputData.get(5);

        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 6));
        System.out.println("start");
        System.out.println(mGlobalInput.Operator);
        System.out.println(mGlobalInput.ManageCom);

        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        return true;
    }


    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LCPolBillF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 修改：新增功能
     * 功能：对录入起期进行判断
     * 规则：对1到11月每月只允许录入26日或1月1日
     */
    private boolean checkStartDate()
    {
        System.out.println("Into checkStartDate()函数：");
        System.out.println("统计起期：" + this.mStartDate);
        System.out.println("统计止期：" + this.mEndDate);
        //通过查找lastatsegment表来实现校验功能,如果此日期在表中有记录，
        //则说明日期合法（前提是数据库中数据正确），如果不存在则说明不合法
        String tSql = "select * from lastatsegment where stattype = '5'"
                      + " and startdate = '" + this.mStartDate + "'";
        LAStatSegmentSet tLAStatSegmentSet = new LAStatSegmentSet();
        LAStatSegmentDB tLAStatSegmentDB = new LAStatSegmentDB();

        tLAStatSegmentSet = tLAStatSegmentDB.executeQuery(tSql);
        if (tLAStatSegmentDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAStatSegmentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "checkStartDate";
            tError.errorMessage = "查询LAStatSegment表时出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //如果查询出的数据条目不为1条，则说明有错（为0或大于1都不对）
        if (tLAStatSegmentSet.size() != 1)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAStatSegmentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "checkStartDate";
            tError.errorMessage = "此起期录入不合法，请重新录入!";
            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("统计起期为：" + this.mStartDate);
        System.out.println("统计止期为：" + this.mEndDate);
        //判断 统计止期 是否大于 统计起期
        if (this.mStartDate.compareTo(this.mEndDate) > 0)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAStatSegmentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "checkStartDate";
            tError.errorMessage = "起期大于止期不合法!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //保存统计开始期间
        this.mStartNo = String.valueOf(tLAStatSegmentSet.get(1).getYearMonth());
        return true;
    }

    private boolean checkdate()
    {
        boolean tReturn = false;
        //此处增加一些校验代码
        //修改：2004-04-14 LL
        //修改内容：对考核预警中起期进行校验
        if (!checkStartDate())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "dealData";
            tError.errorMessage = "校验起期出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        //修改：2004-04-14 LL
        //修改了统计方法，采用WageNo进行统计
        //获得统计期间止期
        if (!getEndNo())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "queryIndexValue";
            tError.errorMessage = "处理止期出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        //修改：2004-09-24 LL
        //调用重置日期程序，把如下条件的单子重置
        //1 - 25号之前扫描 ; 2 - 28号之前承保 ; 3 - 下个月5号之前回单的
        //把满足这些条件的单子重置为上个月的单子
        AgentWageCalSetWageNoReBL tAgentWageCalSetWageNoReBL = new
                AgentWageCalSetWageNoReBL();
        VData tInputData = new VData();
        tInputData.add(this.mManageCom);
        tInputData.add(this.mEndNo);
        tInputData.add(this.mBranchType);
        tInputData.add(this.mGlobalInput);
        tAgentWageCalSetWageNoReBL.submitData(tInputData, "");
        //如果有需要处理的错误，则返回
        if (tAgentWageCalSetWageNoReBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tAgentWageCalSetWageNoReBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "queryIndexValue";
            tError.errorMessage = "重置保单处理出错!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!this.getInterval())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "queryIndexValue";
            tError.errorMessage = "获得统计期间间隔出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        tReturn = true;
        return tReturn;
    }


    /**
     * 修改：新增功能
     * 功能：通过录入止期，获得结束统计期间
     * 规则：
     */
    private boolean getEndNo()
    {
        //转化 录入止期 格式 例如：2004-02-21 -> 200402
        String str = AgentPubFun.formatDate(this.mEndDate, "yyyyMM");
        System.out.println("统计止期转化后格式为：" + str);
        //查询LAStatSegment表
        String tSql = "select * from lastatsegment where stattype = '5'"
                      + " and yearmonth = '" + str + "'";
        System.out.println("查询SQL为：" + tSql);
        LAStatSegmentSet tLAStatSegmentSet = new LAStatSegmentSet();
        LAStatSegmentDB tLAStatSegmentDB = new LAStatSegmentDB();

        tLAStatSegmentSet = tLAStatSegmentDB.executeQuery(tSql);
        if (tLAStatSegmentDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAStatSegmentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "getEndNo";
            tError.errorMessage = "查询LAStatSegment表时出错!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //如果查询出的数据条目为0，则有错误
        if (tLAStatSegmentSet.size() == 0)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAStatSegmentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "getEndNo";
            tError.errorMessage = "此止期录入不合法，请重新录入!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //获得该考核年月止期
        String tEndDate = tLAStatSegmentSet.get(1).getEndDate();
        //比较 录入止期 和 考核年月 止期
        // 大于    ：取下个月
        // 小于等于 ：取本月
        if (this.mEndDate.compareTo(tEndDate) > 0) //大于，取下个月
        {
            //往后推一个月
            String temp = PubFun.calDate(this.mEndDate, 1, "M", null);
            this.mEndNo = AgentPubFun.formatDate(temp, "yyyyMM");
        }
        else
        {
            this.mEndNo = AgentPubFun.formatDate(this.mEndDate, "yyyyMM");
        }

        System.out.println("最终获得的起止统计期间为：");
        System.out.println("统计起期：" + this.mStartDate);
        System.out.println("统计止期：" + this.mEndDate);
        System.out.println("起始期间：" + this.mStartNo);
        System.out.println("终止期间：" + this.mEndNo);

        return true;
    }


    private boolean getPrintData()
    {
        if (!this.checkdate())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAIndexTempQueryBL";
            tError.functionName = "queryIndexValue";
            tError.errorMessage = "考核预警校验出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        String strArr[] = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("Temp");
        strArr = new String[9];
        String tSql =
                "Select b.branchattr,b.name,a.AgentCode,a.name,a.agentgroup,a.branchcode "
                +
                "From LAagent a,labranchgroup b,latree c Where a.ManageCom like '" +
                this.mManageCom + "%' and a.agentcode=c.agentcode "
                +
                " and a.agentgroup=b.agentgroup and c.state <> '3' And AgentGrade = '" +
                mAgentGrade + "' order by b.branchattr";
        if (this.mAgentCode != null && !this.mAgentCode.equals(""))
        {
            tSql =
                    "Select b.branchattr,b.name,a.AgentCode,a.name,a.agentgroup,a.branchcode "
                    +
                    "From LAagent a,labranchgroup b,latree c Where a.ManageCom like '" +
                    this.mManageCom + "%' and a.agentcode=c.agentcode "
                    +
                    " and a.agentgroup=b.agentgroup and c.state <> '3' And AgentGrade = '" +
                    mAgentGrade + "' And a.AgentCode = '" + this.mAgentCode
                    + "' order by b.branchattr";
        }
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(tSql);
        for (int a = 1; a <= tSSRS.getMaxRow(); a++)
        {

            for (int b = 1; b <= tSSRS.getMaxCol(); b++)
            {
                if (b >= 1 || b <= 4)
                {
                    strArr[b - 1] = tSSRS.GetText(a, b);
                }
            }
            if (this.mAgentGrade.compareTo("A08") < 0)
            {
                //--客户数
                tSql =
                        "select trunc(nvl(sum(CalCount),0)) from lacommision where agentcode = '"
                        + tSSRS.GetText(a, 3) +
                        "' and payyear < 1  and wageno >= '" + this.mStartNo
                        + "' and wageno <= '" + this.mEndNo +
                        "' and GetPolDate < '" + this.mEndDate + "'";
                tExeSQL = new ExeSQL();
                strArr[4] = tExeSQL.getOneValue(tSql);
                //--FYC
                tSql =
                        "Select nvl(sum(fyc),0) From LACommision Where payYear < 1 "
                        + " And AgentCode = '" + tSSRS.GetText(a, 3) + "'"
                        + " and wageno >= '" + this.mStartNo + "'"
                        + " and wageno <= '" + this.mEndNo + "'"
                        + " and GetPolDate < '" + this.mEndDate + "'";
                tExeSQL = new ExeSQL();
                strArr[5] = tExeSQL.getOneValue(tSql);
            }
            if (this.mAgentGrade.compareTo("A03") > 0)
            {
                //--GroupFYC
                tSql =
                        "Select nvl(sum(fyc),0) From LACommision Where payYear < 1 "
                        + "And BranchCode = '" + tSSRS.GetText(a, 4) + "'"
                        + " and wageno >= '" + this.mStartNo + "'"
                        + " and wageno <= '" + this.mEndNo + "'"
                        + " and GetPolDate < '" + this.mEndDate + "'";
                tExeSQL = new ExeSQL();
                strArr[6] = tExeSQL.getOneValue(tSql);
            }
            //指标：直辖部FYC
            if (this.mAgentGrade.compareTo("A05") > 0)
            {
                //--DepFYC
                tSql =
                        "Select nvl(sum(fyc),0) From LACommision Where payYear < 1 "
                        + " and wageno >= '" + this.mStartNo + "'"
                        + " and wageno <= '" + this.mEndNo + "'"
                        + " and GetPolDate < '" + this.mEndDate + "'"
                        +
                        " And trim(Branchattr) like nvl((Select trim(b.Branchattr) "
                        + " From Labranchgroup b where b.branchmanager = '" +
                        tSSRS.GetText(a, 3) + "' "
                        +
                        " And b.BranchLevel = '02' and b.endflag = 'N'),'N') || '%' ";
                tExeSQL = new ExeSQL();
                strArr[7] = tExeSQL.getOneValue(tSql);
            }
            //指标：直辖部月均FYC
            if (this.mAgentGrade.compareTo("A07") > 0)
            {
                //--AvgDepFYC
                if (!strArr[7].equals("0"))
                {
                    strArr[8] = String.valueOf(Double.parseDouble(strArr[7]) /
                                               Double.parseDouble(this.mIntvl));

                }
            }
            tlistTable.add(strArr);
            strArr = new String[9];
        }
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        texttag.add("StartDay", mStartDate); //输入制表时间
        texttag.add("EndDay", mEndDate); //输入制表时间
        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("IndexTemp.vts", "printer"); //最好紧接着就初始化xml文档
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
        mResult.addElement(xmlexport);
        return true;

    }

    /**
     * 修改：新增功能
     * 功能：计算统计起期与统计止期之间的间隔月数
     * 规则：
     */
    private boolean getInterval()
    {
        String str1 = this.mStartNo.substring(0, 4) + "-"
                      + this.mStartNo.substring(4) + "-01";
        String str2 = this.mEndNo.substring(0, 4) + "-"
                      + this.mEndNo.substring(4) + "-01";
        System.out.println("统计期间转化后的起止期为：");
        System.out.println("起：" + str1);
        System.out.println("止：" + str2);
        int interval = PubFun.calInterval(str1, str2, "M");
        if (interval == 0)
        {
            this.mIntvl = "1";
        }
        else
        {
            this.mIntvl = String.valueOf(interval + 1);
        }
        System.out.println("时间间隔为：" + this.mIntvl);

        return true;
    }

    public static void main(String[] args)
    {
        String mManageCom = "86110000";
        String mAgentCode = "";
        String mAgentGrade = "A01";
        String mStartDate = "2004-3-26";
        String mEndDate = "2004-6-25";
        String mBranchType = "1";

        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "8611";
        tG.ComCode = "8611";
        tG.Operator = "001";
        VData tVData = new VData();
        tVData.addElement(mManageCom);
        tVData.addElement(mAgentCode);
        tVData.addElement(mAgentGrade);
        tVData.addElement(mStartDate);
        tVData.addElement(mEndDate);
        tVData.addElement(mBranchType);
        tVData.addElement(tG);
        LAIndexTempBL tAgentTrussPF1PUI = new LAIndexTempBL();
        tAgentTrussPF1PUI.submitData(tVData, "");
    }
}
