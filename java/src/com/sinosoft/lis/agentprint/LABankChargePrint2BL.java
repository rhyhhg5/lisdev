package com.sinosoft.lis.agentprint;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import java.io.*;
import java.text.DecimalFormat;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class LABankChargePrint2BL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mManageCom = null;
    private String mAgentCom = null;
    private String mWageNo = null;
    private String mChargeState = null;
    private String mOutXmlPath = null;
    //private String mtype = null;

    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();
    SSRS tSSRS=new SSRS();
    public LABankChargePrint2BL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        mSql="select a.riskcode,b.p11,sum(a.transmoney),value(a.chargerate,0),sum(a.charge)  "
        	+" from lacharge a,lacommision b "
        	+" where a.commisionsn=b.commisionsn "
        	+" and a.managecom like '"+mManageCom+"%' and a.agentcom like '"+mAgentCom+"%' "
        	+" and a.wageno ='"+mWageNo+"' ";
        	if (mChargeState != null && !mChargeState.equals("")) {
        		mSql += " and a.chargestate= '" + mChargeState + "' ";
            }

        	mSql +=" group by GROUPING SETS  (( a.riskcode,b.p11,a.chargerate),(a.riskcode))"
        	+" order by a.riskcode,b.p11 ";
        
        System.out.println(mOutXmlPath);
        tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "LABankChargePrint2BL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        if(tSSRS.getMaxRow()==0)
        {
           
            CError tError = new CError();
            tError.moduleName = "LABankChargePrint2BL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有符合条件的数据，请重新输入条件";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 15][30];
        mToExcel[0][0] = "银保业务手续费计提表二";
        mToExcel[1][0] = "保险代理中介业务手续费计提汇总表（"+mWageNo+"）";
        mToExcel[2][0] = "";
        mToExcel[3][0] = "编制单位：中国人民健康保险股份有限公司广东分公司";
        mToExcel[4][0] = "银行代理网点名称："+mAgentCom+" "+getName(mAgentCom);
        mToExcel[5][5] = "NO:"+mCurDate+" "+mCurTime;
        
        mToExcel[6][0] = "险种代码";
        mToExcel[6][1] = "投保人";
        mToExcel[6][2] = "保险费";
        mToExcel[6][3] = "提取比例";        
        mToExcel[6][4] = "手续费";


        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row +6][col - 1] = tSSRS.GetText(row, col);
            }
        }
        
        mToExcel[tSSRS.getMaxRow()+9][0] = "总经理:";
        mToExcel[tSSRS.getMaxRow()+9][3] = "分管总经理:";
        mToExcel[tSSRS.getMaxRow()+9][5] = "分公司计财部:";
        mToExcel[tSSRS.getMaxRow()+9][7] = "分公司银保部";        
        mToExcel[tSSRS.getMaxRow()+10][1] = "制表";
        mToExcel[tSSRS.getMaxRow()+10][4] = "日期："+mCurDate;
        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "LABankChargePrint2BL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }        
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        mManageCom = (String) tf.getValueByName("ManageCom");
        mAgentCom = (String) tf.getValueByName("AgentCom");
        mWageNo = (String) tf.getValueByName("WageNo");
        mChargeState = (String) tf.getValueByName("ChargeState");
        if(mManageCom == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "LABankChargePrint2BL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        return true;
    }

    private String getName(String pmAgentCom)
    {
      String tSQL = "";
      String tRtValue="";
      tSQL = "select  name from lacom where agentcom='" + pmAgentCom +"'";
      SSRS tSSRS = new SSRS();
      ExeSQL tExeSQL = new ExeSQL();
      tSSRS = tExeSQL.execSQL(tSQL);
      if(tSSRS.getMaxRow()==0)
      {
      tRtValue="";
      }
      else
      tRtValue=tSSRS.GetText(1, 1);
      return tRtValue;
  }

    /**
    * 执行SQL文查询结果
    * @param sql String
    * @return double
    */
   private double execQuery(String sql)

   {
      Connection conn;
      conn = null;
      conn = DBConnPool.getConnection();

      System.out.println(sql);

      PreparedStatement st = null;
      ResultSet rs = null;
      try {
          if (conn == null)return 0.00;
          st = conn.prepareStatement(sql);
          if (st == null)return 0.00;
          rs = st.executeQuery();
          if (rs.next()) {
              return rs.getDouble(1);
          }
          return 0.00;
      } catch (Exception ex) {
          ex.printStackTrace();
          return -1;
      } finally {
          try {
             if (!conn.isClosed()) {
                 conn.close();
             }
             try {
                 st.close();
                 rs.close();
             } catch (Exception ex2) {
                 ex2.printStackTrace();
             }
             st = null;
             rs = null;
             conn = null;
           } catch (Exception e) {}

      }
     }

    public static void main(String[] args)
    {
        LABankChargePrint2BL LABankChargePrint2BL = new
            LABankChargePrint2BL();
    System.out.println("11111111");
    }
}
