package com.sinosoft.lis.agentprint;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author zy
 * @version 1.0
 */
import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class BankOperMonthBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    //取得的时间
    private GlobalInput mGlobalInput = new GlobalInput();
    private String Month = "";
    //输入的查询sql语句
    private String msql = "";
    private String nsql = "";
    private String sql = "";
    //业务处理相关变量
    /** 全局数据 */
    public BankOperMonthBL()
    {
    }

    /**
          传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        mResult.clear();
        // 准备所有要打印的数据
        if (!queryData())
        {
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
//全局变量
        Month = (String) cInputData.get(0);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "DepartOrderBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean queryData()
    {
        String strArr[] = null;
        ListTable tlistTable = new ListTable();
        tlistTable.setName("BankMonth");
        TextTag texttag = new TextTag(); //新建一个TextTag的实例
        String ToDay = PubFun.getCurrentDate();
        texttag.add("ToDay", ToDay); //输入制表时间
        String mmDay[] = PubFun.calFLDate(Month.substring(0, 4) + "-" +
                                          Month.substring(4, 6) + "-01");
        String lastFDay = PubFun.calDate(mmDay[0], -1, "M", null);
        String mFDay[] = PubFun.calFLDate(lastFDay);
        texttag.add("StartDay", mmDay[0]); //输入制表时间
        texttag.add("EndDay", mmDay[1]); //输入制表时间
        strArr = new String[16];
        double data[] = null; //分公司合计
        data = new double[16];
        double data1[] = null; //总计
        data1 = new double[16];
        String asql =
                "select shortname,comcode from ldcom where length(trim(comcode))=8";
        //tjj chg 0206 为了提高效率改善性能，减少数据库的连接次数。
        Connection conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            ExeSQL aExeSQL = new ExeSQL(conn);
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(asql);
            for (int a = 1; a <= aSSRS.getMaxRow(); a++)
            {
                for (int b = 1; b <= aSSRS.getMaxCol(); b++)
                {
                    if (b == 1)
                    {
                        strArr[0] = aSSRS.GetText(a, 1); //赋分公司的名称
                    }
                    if (b == 2)
                    {
                        //员工代码、员工姓名、负责渠道、负责网点数
                        String bsql = "select a.name,a.channelname,count(b.agentcom),a.agentcode from lacomtoagent b,lacom c ,laagent a where a.agentcode=b.agentcode and b.agentcom=c.agentcom and c.SellFlag='Y' and a.branchtype='3' and a.managecom='" +
                                      aSSRS.GetText(a, 2) +
                                      "' group by a.agentcode,a.name,a.channelname order by a.agentcode";
                        SSRS bSSRS = new SSRS();
                        bSSRS = aExeSQL.execSQL(bsql);
                        for (int c = 1; c <= bSSRS.getMaxRow(); c++)
                        {
                            for (int d = 1; d <= bSSRS.getMaxCol(); d++)
                            {
                                if (d == 1 || d == 2 || d == 3)
                                {
                                    strArr[d + 1] = bSSRS.GetText(c, d);
                                }
                                if (d == 4)
                                {
                                    strArr[1] = bSSRS.GetText(c, 4);
                                    //本月标准保费
                                    String dsql =
                                            "select nvl(sum(standprem),0)/10000 from lacommision where tmakedate>='" +
                                            mmDay[0] + "' and tmakedate<='" +
                                            mmDay[1] + "' and agentcode='" +
                                            bSSRS.GetText(c, 4) + "'";
                                    SSRS dSSRS = new SSRS();
                                    strArr[7] = aExeSQL.getOneValue(dsql);
                                    //本月指标
                                    String zsql = "select nvl(sum(planvalue),0)/10000 from laplan where planobject=(select agentgrade from latree where agentcode='" +
                                                  bSSRS.GetText(c, 4) +
                                                  "') and plantype='1' and branchtype='3' and planperiod='" +
                                                  Month + "'";
                                    strArr[8] = aExeSQL.getOneValue(zsql); //本月指标
                                    //本月达成率
                                    if (strArr[8].equals("0"))
                                    {
                                        strArr[9] = "0";
                                    }
                                    else
                                    {
                                        strArr[9] = String.valueOf(Double.
                                                parseDouble(strArr[7]) /
                                                Double.parseDouble(strArr[8]));
                                    }
                                    //月增长率
                                    String ysql =
                                            "select nvl(sum(Standprem),0)/10000 from Lacommision where tmakeDate>='" +
                                            mFDay[0] + "' and tmakeDate<='" +
                                            mFDay[1] + "' and agentcode='" +
                                            bSSRS.GetText(c, 4) + "'";
                                    String zeng = aExeSQL.getOneValue(ysql);
                                    if (zeng.equals("0"))
                                    {
                                        strArr[10] = "0";
                                    }
                                    else
                                    {
                                        strArr[10] = String.valueOf(Double.
                                                parseDouble(strArr[7]) /
                                                Double.parseDouble(zeng));
                                    }
                                    //本月退保
                                    String xsql =
                                            "select nvl(sum(transmoney),0)/10000 from lacommision where tmakedate>='" +
                                            mmDay[0] + "' and tmakedate<='" +
                                            mmDay[1] +
                                            "' and transtype='WT' and agentcode='" +
                                            bSSRS.GetText(c, 4) + "'";
                                    strArr[11] = aExeSQL.getOneValue(xsql);
                                    //总退保
                                    String esql =
                                            "select nvl(sum(transmoney),0)/10000 from lacommision where tmakedate>='" +
                                            Month.substring(0, 4) +
                                            "-01-01' and tmakedate<='" +
                                            mmDay[1] +
                                            "' and transtype='WT' and agentcode='" +
                                            bSSRS.GetText(c, 4) + "'";
                                    strArr[12] = aExeSQL.getOneValue(esql);
                                    //总业绩
                                    String fsql =
                                            "select nvl(sum(transmoney),0)/10000 from lacommision where tmakedate>='" +
                                            Month.substring(0, 4) +
                                            "-01-01' and tmakedate<='" +
                                            mmDay[1] + "' and agentcode='" +
                                            bSSRS.GetText(c, 4) + "'";
                                    strArr[13] = aExeSQL.getOneValue(fsql);
                                    //年度计划指标
                                    String gsql = "select nvl(sum(planvalue),0)/10000 from laplan where planobject=(select agentgrade from latree where plantype='1' and branchtype='3' and planperiod='" +
                                                  Month.substring(0, 4) +
                                                  "' and agentcode='" +
                                                  bSSRS.GetText(c, 4) + "')";
                                    strArr[14] = aExeSQL.getOneValue(gsql); //本月指标
                                    //总达成
                                    if (strArr[14].equals("0"))
                                    {
                                        strArr[15] = "0";
                                    }
                                    else
                                    {
                                        strArr[15] = String.valueOf(Double.
                                                parseDouble(strArr[13]) /
                                                Double.parseDouble(strArr[14]));
                                    }
                                    //求合计
                                    data[4] += Double.parseDouble(strArr[4]);
                                    data[7] += Double.parseDouble(strArr[7]);
                                    data[8] += Double.parseDouble(strArr[8]);
                                    data[11] += Double.parseDouble(strArr[11]);
                                    data[12] += Double.parseDouble(strArr[12]);
                                    data[13] += Double.parseDouble(strArr[13]);
                                    data[14] += Double.parseDouble(strArr[14]);
                                    //险种保费
                                    String csql = "select Riskname,nvl(sum(prem),0)/10000 from LMRiskApp b, Lcpol a where a.uwflag<>'a' and a.riskcode=b.riskcode and makeDate>='" +
                                                  mmDay[0] +
                                                  "' and makeDate<='" + mmDay[1] +
                                                  "' and Agentcode='" +
                                                  bSSRS.GetText(c, 4) +
                                                  "' group by riskname";
                                    SSRS cSSRS = new SSRS();
                                    cSSRS = aExeSQL.execSQL(csql);
                                    for (int e = 1; e <= cSSRS.getMaxRow(); e++)
                                    {
                                        strArr[5] = cSSRS.GetText(e, 1);
                                        strArr[6] = cSSRS.GetText(e, 2);
                                        data[6] += Double.parseDouble(strArr[6]);
                                        if (e >= 2)
                                        {
                                            for (int i = 7; i <= 15; i++)
                                            {
                                                strArr[i] = "";
                                            }
                                        }
                                        tlistTable.add(strArr);
                                        strArr = new String[16];
                                    }
                                }
                            }
                        }
                        if (bSSRS.getMaxRow() != 0)
                        {
                            //分公司合计
                            strArr = new String[16];
                            strArr[0] = "合计";
                            strArr[1] = "";
                            strArr[2] = "";
                            strArr[3] = "";
                            strArr[5] = "";
                            strArr[10] = "";
                            strArr[4] = getInt(String.valueOf(data[4]));
                            strArr[6] = String.valueOf(data[6]);
                            strArr[7] = String.valueOf(data[7]);
                            strArr[8] = String.valueOf(data[8]);
                            strArr[11] = String.valueOf(data[11]);
                            strArr[12] = String.valueOf(data[12]);
                            strArr[13] = String.valueOf(data[13]);
                            strArr[14] = String.valueOf(data[14]);
                            //本月达成率
                            if (strArr[8].equals("0.0"))
                            {
                                strArr[9] = "0";
                            }
                            else
                            {
                                strArr[9] = String.valueOf(Double.parseDouble(
                                        strArr[7]) /
                                        Double.parseDouble(strArr[8]));
                            }
                            //总达成
                            if (strArr[14].equals("0.0"))
                            {
                                strArr[15] = "0";
                            }
                            else
                            {
                                strArr[15] = String.valueOf(Double.parseDouble(
                                        strArr[13]) /
                                        Double.parseDouble(strArr[14]));
                            }
                            //计算总计
                            data1[4] += data[4];
                            data1[6] += data[6];
                            data1[7] += data[7];
                            data1[8] += data[8];
                            data1[11] += data[11];
                            data1[12] += data[12];
                            data1[13] += data[13];
                            data1[14] += data[14];
                            data = new double[16];
                            tlistTable.add(strArr);
                            strArr = new String[16];
                        }
                    }
                }
            }

            //分公司合计
            strArr = new String[16];
            strArr[0] = "总计";
            strArr[1] = "";
            strArr[2] = "";
            strArr[3] = "";
            strArr[5] = "";
            strArr[10] = "";
            strArr[4] = getInt(String.valueOf(data1[4]));
            strArr[6] = String.valueOf(data1[6]);
            strArr[7] = String.valueOf(data1[7]);
            strArr[8] = String.valueOf(data1[8]);
            strArr[11] = String.valueOf(data1[11]);
            strArr[12] = String.valueOf(data1[12]);
            strArr[13] = String.valueOf(data1[13]);
            strArr[14] = String.valueOf(data1[14]);
            //本月达成率
            if (strArr[8].equals("0.0"))
            {
                strArr[9] = "0";
            }
            else
            {
                strArr[9] = String.valueOf(Double.parseDouble(strArr[7]) /
                                           Double.parseDouble(strArr[8]));
            }
            //总达成
            if (strArr[14].equals("0.0"))
            {
                strArr[15] = "0";
            }
            else
            {
                strArr[15] = String.valueOf(Double.parseDouble(strArr[13]) /
                                            Double.parseDouble(strArr[14]));
            }

            tlistTable.add(strArr);

            XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
            xmlexport.createDocument("BankOperMonth.vts", "printer"); //最好紧接着就初始化xml文档
            if (texttag.size() > 0)
            {
                xmlexport.addTextTag(texttag);
            }
            xmlexport.addListTable(tlistTable, strArr);
//    xmlexport.outputDocumentToFile("e:\\","test");//输出xml文档到文件
            mResult.addElement(xmlexport);
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BankOperMonthBL";
            tError.functionName = "queryData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.close();
            }
            catch (Exception e)
            {
            }
            return false;
        }
        System.out.println("end");
        return true;
    }

    public static String getInt(String Value)
    {
        String result = "";
        boolean mflag = true;
        int m = 0;
        m = Value.lastIndexOf(".");
        if (m == -1)
        {
            result = Value;
        }
        else
        {
            result = Value.substring(0, m);
        }
        return result;
    }

    public static void main(String[] args)
    {
    }
}