package com.sinosoft.lis.otof;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p>Title: Web业务系统投保暂交费功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author YT
 * @version 1.0
 */

public class LITranLogBLS
{
  //传输数据类
  private VData mInputData ;
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();
  private LITranLogSchema mLITranLogSchema = new LITranLogSchema();
  public LITranLogBLS() {}

  public static void main(String[] args)
  {
  }

  public boolean submitData(VData cInputData,String cOperate)
  {
  	boolean tReturn = false;
    mInputData=(VData)cInputData.clone() ;
    System.out.println("开始执行saveData");
    if (!this.saveData())
      return false;

    mInputData=null;
    return true;
  }

  private boolean saveData()
  {
    mLITranLogSchema =(LITranLogSchema)mInputData.getObjectByObjectName("LITranLogSchema",0);
    Connection conn = DBConnPool.getConnection();
    if (conn==null)
    {
      CError tError = new CError();
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        tError.moduleName = "LITranLogBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    try
    {
      conn.setAutoCommit(false);
			LITranLogDB mLITranLogDB = new LITranLogDB( conn );
      mLITranLogDB.setSchema(mLITranLogSchema);
			if (mLITranLogDB.insert() == false)
			{
        this.mErrors.copyAllErrors(mLITranLogDB.mErrors);
				CError tError = new CError();
				tError.moduleName = "LITranLogBLS";
				tError.functionName = "saveData";
				tError.errorMessage = "保存数据失败!";
				this.mErrors .addOneError(tError) ;
	       			conn.rollback() ;
	       			conn.close();
				return false;
			}
      conn.commit();
      conn.close();
		} // end of try
	catch (Exception ex)
		{
			CError tError = new CError();
			tError.moduleName = "LITranLogBLS";
			tError.functionName = "submitData";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);
			try
      {
        conn.rollback() ;
        conn.close();
			}
      catch(Exception e){}
		return false;
    }
    return true;
  }
}


