package com.sinosoft.lis.otof;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import java.util.*;
import java.lang.String;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:计算累计生息账户当期利息
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class CalAccInterestUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mStrLJAGetSet = new String();
  private String mOperate;
  private String mResultQueryString = "";

  public CalAccInterestUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
	//将操作数据拷贝到本类中
	this.mOperate = cOperate;
	CalAccInterestBL tCalAccInterestBL = new CalAccInterestBL();
	System.out.println("---UI BEGIN---"+mOperate);
	if (tCalAccInterestBL.submitData(cInputData,mOperate) == false)
	{
	  // @@错误处理
	  this.mErrors.copyAllErrors(tCalAccInterestBL.mErrors);
	  return false;
	}

	return true;
  }

  public VData getQueryResult()
  {
	return mResult;
  }

  public String getLJAGetSet()
  {
	 return mStrLJAGetSet;
  }
  public static void main(String[] args)
  {

  GlobalInput tG = new GlobalInput();
  tG.Operator = "001";
  tG.ManageCom = "86";
  tG.ComCode = "86";
 TransferData tTransferData= new TransferData();
  tTransferData.setNameAndValue("BalaDate","2004-06-22");
	VData tVData = new VData();

  tVData.add(tTransferData) ;
  tVData.add( tG );
  CalAccInterestUI ui = new CalAccInterestUI();
  if( ui.submitData( tVData, "INSERT" ) == true )
	  System.out.println("---ok---");
  else
	  System.out.println("---NO---");
	System.out.println("-------test...");
  }
}