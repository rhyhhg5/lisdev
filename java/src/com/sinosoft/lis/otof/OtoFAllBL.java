package com.sinosoft.lis.otof;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
import com.sinosoft.lis.otof.*;
import java.io.*;
import org.xml.sax.InputSource;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import java.net.*;
import org.apache.xml.serialize.*;
import java.util.*;
import org.xml.sax.InputSource;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.otof.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.xpath.*;
import com.sinosoft.lis.xpath.UniteXML;

public class OtoFAllBL {
   /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public String bdate = ""; //request.getParameter("Bdate");
    public String edate = ""; //request.getParameter("Edate");
    public String mStartDate = "";
    public GlobalInput tG = new GlobalInput();
    private String flag = "true";
    public String tFlag = "0"; //手工提取
    private CErrors tError = null;
    private String Content = "";
    private String FlagStr = "";
    private String tdate = "";
    private String tname = "";
    private String pname = "";
    private String allpath = "";
    public String Path1 = "";
    public String Path = "";
    private BillXmlOutput tBillXmlOutput = new BillXmlOutput();
    private String transportmessage = "";
    public String danzhenghao = "";
    public String yearmonth = "";
    private String URLpath = "";
    private String tURL = ""; //request.getRequestURL().toString();
    private String ttURL = ""; //HttpUtils.getRequestURL(request).toString();
    private String ttAddress = ""; //request.getRemoteAddr();
    private int num;
    GlobalInput mGlobalInput = new GlobalInput();
    public OtoFAllBL() {
    }

    private boolean getInputData(VData cInputData)
    {
      mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
            "GlobalInput", 0));
      mStartDate = (String) cInputData.get(1);
      num = Integer.parseInt((String) cInputData.get(2));
      bdate = (String) cInputData.get(3);
      edate = (String) cInputData.get(4);
      if (mGlobalInput == null)
      {
        buildError("getInputData", "没有得到足够的信息！");
        return false;
      }
      return true;
    }

    public boolean submitData(VData cInputData,String cOperator)
    {
        try
        {
            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }
//            System.out.println("getinputdata END...");
            // 进行业务处理
            if (cOperator.equals("DELETE"))
            {
                if (!DelLITranInfo(bdate,edate,mGlobalInput.ManageCom,num)){
                    return false;
                }
            }
            else if (cOperator.equals("INSERT"))
            {
                if (!dealData()) {
                    return false;
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submit", "发生异常");
            return false;
        }
        return true;
    }

    private boolean dealData()
    {
      OtoFAllBL mOtoFAllBL = new OtoFAllBL();
      for (int i = 1; i <= 26; i++) {
          System.out.println("财务提数项目" + i);
          if (i==10 || i==11) continue;
          if (mOtoFAllBL.initDeal(mStartDate)) {
              Integer I = new Integer(i);
              String danzheng = I.toString();
              mOtoFAllBL.Setdanzhenghao(danzheng);
              mOtoFAllBL.GetPath();
          }
          try {
              mOtoFAllBL.bussDeal(mGlobalInput.ManageCom);
          } catch (FileNotFoundException ex) {
          } catch (Exception ex) {
          }

      }
      return true;
     }

  private boolean initDeal(String strDate) {
      OtoFAllBL tOtoFAllBL = new OtoFAllBL();
      LDSysVarDB tLDSysVarDB = new LDSysVarDB();
      String sql = "Select * from LDSysVar where SysVar = 'ServerRoot'";
      LDSysVarSet tLDSysVarSet = tLDSysVarDB.executeQuery(sql);
      LDSysVarSchema tLDSysVarSchema = tLDSysVarSet.get(1);
      Path = tLDSysVarSchema.getSysVarValue() + "/temp";
      tOtoFAllBL.SetPath(Path);
      String getDate = strDate;
      bdate = getDate;
      edate = getDate;
      System.out.println(Path);
      if (Path.endsWith("\\") || Path.endsWith("/")) {
          Path = Path.substring(0, Path.length() - 1);
      }
      System.out.println(Path);

      Path1 = tLDSysVarSchema.getSysVarValue() + "/";
      System.out.println(Path1);
      System.out.println(
              "------------------------------------------------------");
      if (!(Path1.endsWith("\\") || Path1.endsWith("/"))) {
          Path1 = Path1 + "/";
      }
      System.out.println(Path1);
      String urlSql = "Select * from LDSysVar where SysVar = 'ServerURL'";
      tLDSysVarSet = tLDSysVarDB.executeQuery(urlSql);
      tLDSysVarSchema = tLDSysVarSet.get(1);
      String tempURL = tLDSysVarSchema.getSysVarValue();
      pname = Path + "/";
      URLpath = "ufin/addVoucherReturnXML.jsp";
      ttURL = tempURL + "otof/OtoF.jsp";
      tURL = ttURL.toString();
      System.out.println("aaa==" + ttURL);
      System.out.println("bbb==" + tURL);
      System.out.println("ccc==" + ttAddress);
      int tBeginidx = tURL.indexOf("otof/OtoF.jsp");
      String addVoucerUrl = tURL.substring(0, tBeginidx);
      if (addVoucerUrl.endsWith("/")) {
          addVoucerUrl = addVoucerUrl + URLpath;
      } else {
          addVoucerUrl = addVoucerUrl + "/" + URLpath;
      }
      System.out.println("aaa==" + addVoucerUrl);
      FDate chgdate = new FDate();
      Date dbdate = chgdate.getDate(bdate);
      Date dedate = chgdate.getDate(edate);

      if (dbdate.compareTo(dedate) > 0) {
          flag = "false";
          Content = "日期录入错误1!";
          System.out.println(Content);

      }
      System.out.println("开始测试");
      return true;
  }

  public boolean bussDeal(String tManageCom) throws FileNotFoundException, Exception {
      String strLDComSQL="";

      if (flag.equals("true")) {
         LDComSchema mLDComSchema = new LDComSchema();
         LDComDB mLDComDB = new LDComDB();
         LDComSet mLDComSet = new LDComSet();
         if (tManageCom.equals("86"))
            strLDComSQL = "Select * from LDCom where sign = '1' and comcode not in ('86','86000000') and length(trim(comcode)) <> 4 order by comcode";
         else
            strLDComSQL = "Select * from LDCom where sign = '1' and comcode not in ('86','86000000') and length(trim(comcode)) <> 4 and comcode like '"+tManageCom+"%' order by comcode";
         mLDComSet = mLDComDB.executeQuery(strLDComSQL);
         for (int j=1;j<=mLDComSet.size();j++)
         {
             mLDComSchema = mLDComSet.get(j);
             VData vData = new VData();
             tG.Operator = "cwad";
             tG.ManageCom = mLDComSchema.getComCode();
             System.out.println("tG.ManageCom = " +  mLDComSchema.getComCode());
             vData.addElement(tG);
             vData.addElement(bdate);
             vData.addElement(edate);
             Integer itemp = new Integer(danzhenghao);
             vData.addElement(itemp);
             vData.addElement(tFlag);
             String realPath = Path + "/";
//             String realPath = "D:\\";
             vData.addElement(realPath);
             vData.addElement(yearmonth);
             System.out.println("-----------------------------------------");
             System.out.println("realPath : " + realPath);
             System.out.println("-----------------------------------------");
             System.out.println("加载完成");
             OtoFUI tOtoFUI = new OtoFUI();
             if (!tOtoFUI.submitData(vData, "PRINT")) {
                 tError = tOtoFUI.mErrors;
                 System.out.println("fail to get print data");
                 flag = "false";
                 Content = "提取数据出错，原因是：" + tError.getFirstError();
                 FlagStr = "Fail";

             } else {
                 System.out.println("nook");
//                 int i = 0;
                 int entrynumber = 0;
//                 num = tOtoFUI.getFilenumUI();
                 vData = tOtoFUI.getResult();
                 CErrors mErrors = new CErrors();
                 System.out.println("--------------ok!------------------");
                 OtoFExport tOtoFExport = (OtoFExport) vData.
                                          getObjectByObjectName("OtoFExport",
                         0);
                 tdate = PubFun.getCurrentDate();
                 tname = "bill" + itemp + "-" + tdate;
                 System.out.println(
                         "------Before do outputDocumentToFileOtoF------");
//                 pname = "D:\\";
                 tOtoFExport.outputDocumentToFileOtoF(pname, tname);
                 System.out.println("outputtoaddVoucherUrl start");
//      		System.out.println("addVoucerUrl:"+addVoucerUrl);
//      		tOtoFExport.outputDocumentToFileOtoF(addVoucerUrl);
//      		System.out.println("outputtoaddVoucherUrl end");


                 allpath = Path1 + "temp/";
//                 allpath = "D:\\";
//                 Path1 = "D:\\";
                 tname = tname + ".xml";
                 System.out.println(allpath + tname);
                 System.out.println("------Before do BillXmlOutput------");
                 if (!tBillXmlOutput.findfile(allpath, tname)) {
                     Content = "生成第一个xml出错";
                     flag = "false";
                     FlagStr = "Fail";
                 } else {
                     System.out.println("toFinxml中的文件已经生成完毕");
                     FileInputStream fis1 = new FileInputStream(allpath +
                             tname);

                     UFFecade uff = new UFFecade(fis1);
                     uff.read();
                     uff.convert(Path1);
                     uff.writeReturnXML(Path1);
                     mErrors = uff.geterrors();
                     entrynumber = uff.getentrycount();
                     transportmessage = PubFun.changForHTML(uff.
                             gettransportmessage());
                 }

                 if (flag.equals("true")) {
                     if (mErrors.getErrorCount() > 0) {
                         Content = transportmessage + " 有部分错误，请查看错误日志";
                         FlagStr = "Succ";
                     } else if (entrynumber == 0) {
                         Content = " 没有需要提交的数据";
                         FlagStr = "Succ";
                     } else {
                         Content = " 保存成功" + transportmessage;
                         FlagStr = "Succ";
                     }
                     System.out.println("开始执行日志表的操作");
                     LITranInfoSchema yLITranInfoSchema = new
                             LITranInfoSchema();
                     LITranInfoSet yLITranInfoSet = new LITranInfoSet();
                     yLITranInfoSet = (LITranInfoSet) vData.
                                      getObjectByObjectName("LITranInfoSet",
                             0);
                     LITranLogSchema tLITranLogSchema = new LITranLogSchema();
                     if (yLITranInfoSet.get(1) != null) {
                         yLITranInfoSchema.setSchema(yLITranInfoSet.get(1));
                         System.out.println("批次号是:" +
                                            yLITranInfoSchema.getBatchNo());
                         tLITranLogSchema.setBatchNo(yLITranInfoSchema.
                                 getBatchNo());
                         tLITranLogSchema.setFileName(tname);
                         tLITranLogSchema.setFilePath(pname);
                         tLITranLogSchema.setStartDate(bdate);
                         tLITranLogSchema.setEndDate(edate);
                         tLITranLogSchema.setFlag(FlagStr);
                         LITranLogUI tLITranLogUI = new LITranLogUI();
                         vData.clear();
                         vData.addElement(tLITranLogSchema);
                         vData.addElement(tG);
                         if (!tLITranLogUI.submitData(vData, "INSERT")) {
                             Content = " 日志表保存失败";
                             FlagStr = "Fail";
                         }
                     }
                     System.out.println("执行日志表的操作结束");

                 }

             }
             try {} catch (Exception e) {
                 e.printStackTrace();
             }
         }
      }

      return true;
  }

  public boolean Setdanzhenghao(String tDanzhanghao) {
      danzhenghao = tDanzhanghao;
      return true;
  }
  public boolean SetPath(String nPath) {
      Path = nPath;
      return true;
  }

  public String GetPath() {
      return Path;
  }

  private boolean DelLITranInfo(String tSBussDate,String tEBussDate,String tManageCom,int tVoucherID)
  {
      String strLITranInfoSQL = "";
      if (tVoucherID == 0)
      {
          strLITranInfoSQL ="Delete From LITranInfo Where BussDate >= '"+tSBussDate+"' and BussDate <= '"+tEBussDate+"' and ManageCom like '"+tManageCom+"%' with ur";
      }
      else
      {
          strLITranInfoSQL ="Delete From LITranInfo Where BussDate >= '"+tSBussDate+"' and BussDate <= '"+tEBussDate+"' and ManageCom like '"+tManageCom+"%' and VoucherID = "+tVoucherID+" with ur";
      }
      ExeSQL mExeSQL = new ExeSQL();
      if (!mExeSQL.execUpdateSQL(strLITranInfoSQL))
          return false;
      return true;

  }
  private void buildError(String szFunc, String szErrMsg)
{
    CError cError = new CError();
    cError.moduleName = "OtoFUI";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
}
public static void main(String[] args) throws Exception
{
    GlobalInput tG = new GlobalInput();
    tG.ManageCom = "8612";
    tG.Operator = "cwad";
    String strDate = "2008-04-08";
    String no = "1";
    VData tVData = new VData();
    tVData.addElement(tG);
    tVData.addElement(strDate);
    tVData.addElement(no);
    OtoFAllBL tOtoFAllBL = new OtoFAllBL();
    tOtoFAllBL.submitData(tVData,"DELETE");
    tOtoFAllBL.submitData(tVData,"INSERT");

}

}
