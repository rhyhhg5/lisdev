package com.sinosoft.lis.otof;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.xpath.UFFecade;
import java.io.FileInputStream;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.xpath.HandleXML;

public class OtoFAllUI {
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
        public CErrors mErrors = new CErrors();

        private VData mResult = new VData();
        private int filenumUI = 0 ;
        /** 全局数据 */
        private GlobalInput mGlobalInput = new GlobalInput();
    public OtoFAllUI() {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("DELETE") && !cOperate.equals("INSERT") )
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }
            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }

            if (!dealData())
            {
                return false;
            }

            OtoFAllBL tOtoFAllBL = new OtoFAllBL();

            if (!tOtoFAllBL.submitData(cInputData, cOperate))
            {
                if (tOtoFAllBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tOtoFAllBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("sbumitData", "OtoFBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
//                mResult = tOtoFAllBL.getResult();
//                filenumUI = tOtoFAllBL.getFilenum();
                return true;
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submit", "发生异常");
            return false;
        }
    }
    public int getFilenumUI()
    {
        return filenumUI;
    }
    public static void main(String[] args) throws Exception
    {
        OtoFAllUI tOtofAllUI = new OtoFAllUI();
        VData vData = new VData();
        String mToday = "2008-04-08";
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "8612";
        String no = "0";

        vData.addElement(tG);
        vData.addElement(mToday);
        vData.addElement(no);
        tOtofAllUI.submitData(vData, "INSERT");


    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealData()
    {
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData Vdata
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {

        //全局变量
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "OtoFUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
