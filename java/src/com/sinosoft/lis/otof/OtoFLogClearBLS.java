package com.sinosoft.lis.otof;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title:  </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 </p>
 * <p>Company: Sinosoft< /p>
 * @author GUOXIANG
 * @version 1.0
 */

public class OtoFLogClearBLS{
     //传输数据类
      private VData mInputData ;
      /** 往界面传输数据的容器 */
      private VData mResult = new VData();
      /** 数据操作字符串 */
      private String mOperate;
      //错误处理类，每个需要错误处理的类中都放置该类
      public  CErrors mErrors = new CErrors();
      /** 删除mutileline的数据set */
      private LITranInfoSet tLITranInfoSet = new LITranInfoSet();
      /** 删除mutileline的数据对应的DBset */
      public OtoFLogClearBLS() {}
      public static void main(String[] args){
      }
      public boolean submitData(VData cInputData,String cOperate){
            boolean tReturn = false;
            mInputData=(VData)cInputData.clone() ;
            this.mOperate = cOperate;
                   //得到外部传入的数据,将数据备份到本类中
            if (!getInputData(cInputData))
                 return false;
            if(cOperate.equals("Delete")){
                 if(!deleteOtoFLogClear())
                    return false;
                 System.out.println("--开始处理deleteOtoFLogClear---");
            }
            return true;
      }
      /**
       * 从输入数据中得到所有对象
       * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
       * @param: cInputData 输入的数据
      */
      private boolean getInputData(VData cInputData){
            // 检验查询条件
            if(mOperate.equals("Delete")){
              System.out.println("----------开始处理Delete的getInputData------");
              tLITranInfoSet.set((LITranInfoSet)mInputData.getObjectByObjectName("LITranInfoSet",0));
              //  mGlobalInput.setSchema((GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0));
              System.out.println("----------结束处理Delete的getInputData--------------");
            }
            return true;
     }
     private boolean deleteOtoFLogClear(){
          Connection conn = DBConnPool.getConnection();
          if (conn==null){
                CError tError = new CError();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                tError.moduleName = "LITranLogBLS";
                tError.functionName = "deleteData";
                tError.errorMessage = "数据库连接失败!";
                this.mErrors .addOneError(tError) ;
                return false;
          }
          try{
                conn.setAutoCommit(false);

                LITranInfoDBSet tLITranInfoDBSet=new LITranInfoDBSet(conn);
                tLITranInfoDBSet.add(tLITranInfoSet);
                System.out.println("gxtest1:"+tLITranInfoSet.size());
                if (tLITranInfoDBSet.size() == 0){
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "OtoFLogClearBLS";
                    tError.functionName = "deleteData";
                    tError.errorMessage = "未找到相关数据!";
                    this.mErrors.addOneError(tError);
                    tLITranInfoDBSet.clear();
                    return false;
                }
                if (tLITranInfoDBSet.delete() == false){
                    this.mErrors.copyAllErrors(tLITranInfoDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "OtoFLogClearBLS";
                    tError.functionName = "deleteData";
                    tError.errorMessage = "删除数据失败!";
                    this.mErrors .addOneError(tError) ;
                    conn.rollback() ;
                    conn.close();
                    return false;
              }
              conn.commit();
              conn.close();
        } // end of try
        catch (Exception ex){
              CError tError = new CError();
              tError.moduleName = "OtoFLogClearBLS";
              tError.functionName = "deleteData";
              tError.errorMessage = ex.toString();
              this.mErrors .addOneError(tError);
              try{
                    conn.rollback() ;
                    conn.close();
              }
              catch(Exception e){ }
              return false;
        }
        System.out.println("删除成功");
        return true;
   }
}


