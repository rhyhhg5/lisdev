//程序名称：ReportCalculate
//程序功能：在服务器端财务数据接口
//创建日期：2003-7-11
//创建人  ：GUOXIANG
//更新记录：  更新人    更新日期     更新原因/内容
package com.sinosoft.lis.otof;
import com.sinosoft.lis.xpath.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.otof.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.xreport.util.*;
import java.lang.*;
import java.text.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import org.apache.xml.serialize.*;
import org.xml.sax.InputSource ;
import org.jdom.*;
import org.jdom.output.*;
import org.w3c.dom.*;
import org.apache.xml.serialize.*;
import org.apache.log4j.*;
import com.sinosoft.lis.xpath.*;




public class OtoF
{



  public  static void  main(String[] args){


    GlobalInput tG = new GlobalInput();
    String dirSys=System.getProperty("user.dir");
    String pname=dirSys+ File.separator+ "temp"+File.separator;
    File tempfile = new File(pname);
    if (!tempfile.exists())
    {
      tempfile.mkdirs();
    }

    String flag = "true";
    String tname = "";
    String addVoucerUrl="";
    tG.Operator="001";
    tG.ManageCom="86";

    String tPath = dirSys;
    String tXreportPath=tPath+File.separator+"applications"+File.separator+"xreport_data";
    SysConfig.FILEPATH=tXreportPath+File.separator;



    Logger log = Logger.getLogger("com.sinosoft.lis.xpath");


//----GET  AddVoucerURL from database----------------------------

    LDSysVarDB urlSysVarDB = new LDSysVarDB();
    urlSysVarDB.setSysVar("AddVoucerURL");
    if (!urlSysVarDB.getInfo())
    {
      log.error("AddVoucerURL 查询失败");
      System.exit(0);
    }
    addVoucerUrl=urlSysVarDB.getSysVarValue();

    String tdate = "";
    OtoFUI tOtoFUI = new OtoFUI();
    String FlagStr = "";
    String tString="";
    if(args.length==0)
     {
      String pattern="yyyy-MM-dd";
      SimpleDateFormat df = new SimpleDateFormat(pattern);
      Date today = new Date();
      Date finday=PubFun.calDate(today,-1,"D",new Date());
      tString = df.format(finday);
      System.out.println("set yesterday as bussdate");
     }
     else
     {
       tString = args[0];
       System.out.println("set inputdate as bussdate");
     }
    String bdate = tString;
    String edate = bdate;
    String tFlag = "1"; //自动提取
    log.info("************开始提取"+tString+"的数据************");

    if (flag.equals("true"))
    {
      for (int i=1;i<=7;i++)
      {
        System.out.println("次数-------------------------"+i);
        Integer itemp = new Integer(i) ;
        VData vData = new VData();
        vData.addElement(tG);
        vData.addElement(bdate);
        vData.addElement(edate);
        vData.addElement(itemp);
        vData.addElement(tFlag);
        log.info("凭证"+i+"加载完成");


        if( !tOtoFUI.submitData(vData, "PRINT") )
        {
          CErrors cerr=tOtoFUI.mErrors;
          log.error("凭证"+i+"提取失败，原因："+cerr.toString());
          flag = "false";
        }
        else
        {

          vData = tOtoFUI.getResult();
          System.out.println("ok!");
          OtoFExport tOtoFExport = (OtoFExport)vData.getObjectByObjectName("OtoFExport", 0);
          tdate = PubFun.getCurrentDate();
          tname = "bill"+i+"-"+tdate;
          try
          {
            tOtoFExport.outputDocumentToFileOtoF(pname,tname);
          }
          catch (Exception ex)
          {
            log.error("写备份文件失败："+pname+tname);
          }
          String tReturn="";
          try {
            tReturn = tOtoFExport.outputDocumentToFileOtoF(addVoucerUrl);
            //log.info(tReturn);
          }
          catch(Exception e)
          {
            //log.info(e.printStackTrace());
            log.error("发送到xpath时发生异常："+e.getMessage());
          }

          log.info("outputtoaddVoucherUrl end");

          if (flag=="true")
          {
            FlagStr = "Succ";
            log.info("开始执行日志表的操作");
            LITranInfoSchema yLITranInfoSchema   = new LITranInfoSchema();
            LITranInfoSet yLITranInfoSet = new LITranInfoSet();
            yLITranInfoSet = (LITranInfoSet)vData.getObjectByObjectName("LITranInfoSet",0);
            LITranLogSchema tLITranLogSchema = new LITranLogSchema();
            if (yLITranInfoSet.get(1)!=null)
            {
              yLITranInfoSchema.setSchema(yLITranInfoSet.get(1));
              log.info("批次号是:"+yLITranInfoSchema.getBatchNo());
              tLITranLogSchema.setBatchNo(yLITranInfoSchema.getBatchNo());
              tLITranLogSchema.setFileName(tname);
              tLITranLogSchema.setFilePath(pname);
              tLITranLogSchema.setStartDate(bdate);
              tLITranLogSchema.setEndDate(edate);
              tLITranLogSchema.setFlag(FlagStr);
              LITranLogUI tLITranLogUI = new LITranLogUI();
              vData.clear();
              vData.addElement(tLITranLogSchema);
              vData.addElement(tG);
              if (!tLITranLogUI.submitData(vData,"INSERT"))
              {
                log.error("执行日志表插入失败");
                FlagStr = "Fail";
                break;
              }
            }
            log.info("执行日志表的操作结束");
            log.info("----------------------");
          }
        }
      } //end for
    }  //end if
    log.info("**********提取"+tString+"数据结束*************");
    System.exit(0);
  }
}