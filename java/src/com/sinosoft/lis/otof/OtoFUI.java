/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.otof;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.xpath.UFFecade;
import java.io.FileInputStream;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.xpath.HandleXML;

/*
 * <p>ClassName: LCPolF1PUI </p>
 * <p>Description: LCPolF1PUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: LIS
 * @CreateDate：2002-11-04
 */
public class OtoFUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
//    private String mToday = "";
//    private String mBdate = "";
//    private String mEdate = "";
//    private int mTime = 0;
    private int filenumUI = 0 ;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
//    private LCPolSchema mLCPolSchema = new LCPolSchema();

    public OtoFUI()
    {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("PRINT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }
//            System.out.println("UI BEGIN...");
            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }
//            System.out.println("getinputdata END...");
            // 进行业务处理
            if (!dealData())
            {
                return false;
            }
//            System.out.println("dealData END...");
            // 准备传往后台的数据
//            System.out.println("OK");
            OtoFBL tOtoFBL = new OtoFBL();
//            System.out.println("end OK");
//            System.out.println("Start OtoF UI Submit ...");
            if (!tOtoFBL.submitData(cInputData, cOperate))
            {
                if (tOtoFBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tOtoFBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("sbumitData", "OtoFBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tOtoFBL.getResult();
                filenumUI = tOtoFBL.getFilenum();
                return true;
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submit", "发生异常");
            return false;
        }
    }
    public int getFilenumUI()
    {
        return filenumUI;
    }
    public static void main(String[] args) throws Exception
    {
        OtoFUI tOtoFUI = new OtoFUI();
        VData vData = new VData();
        String mToday = "2007-04-25";
        String bdate = mToday;
        String edate = mToday;
        String yearmonth = "";
        GlobalInput tG = new GlobalInput();
        tG.Operator = "cwad";
        tG.ManageCom = "8632";
        String no = "20";

        vData.addElement(tG);
        vData.addElement(bdate);
        vData.addElement(edate);
        Integer itemp = new Integer(no);
        vData.addElement(itemp);
        vData.addElement("0");
        String realPath = "D:\\";
        vData.addElement(realPath);
        vData.addElement(yearmonth);
        if (tOtoFUI.submitData(vData, "PRINT"))
        {
            vData = tOtoFUI.getResult();
            OtoFExport tOtoFExport = (OtoFExport) vData.getObjectByObjectName(
                    "OtoFExport", 0);
            System.out.println("ok");
            tOtoFExport.outputDocumentToFileOtoF("D:\\", "bill"+no+"-"+PubFun.getCurrentDate()+"-"+"0");
        }
        System.out.println("nook");
        int num = 0;
        num = tOtoFUI.getFilenumUI();
        System.out.println("num : "+num);

            FileInputStream fis = new FileInputStream(
                    "D:\\bill"+no+"-"+PubFun.getCurrentDate()+"-"+"0.xml");
//        FileInputStream fis = new FileInputStream(
//                    "D:\\bill10-2007-03-22-0.xml");

            UFFecade uff = new UFFecade(fis);
            uff.read();
            uff.convert("D:\\");
            uff.writeReturnXML("D:\\");


    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealData()
    {
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData Vdata
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "OtoFUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private void jbInit() throws Exception {
    }
}
