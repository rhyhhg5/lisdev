package com.sinosoft.lis.otof;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p>Title: Web业务系统投保暂交费功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author YT
 * @version 1.0
 */

public class OtoFBLS
{
  //传输数据类
  private VData mInputData ;
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();

  public OtoFBLS() {}

  public static void main(String[] args)
  {
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    boolean tReturn = false;
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;
    System.out.println("Start OtoF BLS Submit...");

    if (!this.saveData())
      return false;
    if (!this.wnsaveData())
        return false;
    System.out.println("End OtoF BLS Submit...");
    mInputData=null;
    return true;
  }

  private boolean saveData()
  {
    LITranInfoSet mLITranInfoSet = new LITranInfoSet();
    mLITranInfoSet = (LITranInfoSet)mInputData.getObjectByObjectName("LITranInfoSet",0);
    LITranErrSet mLITranErrSet = new LITranErrSet();
    mLITranErrSet = (LITranErrSet)mInputData.getObjectByObjectName("LITranErrSet",0);
    OmnipotenceIntfDataSet mOmnipotenceIntfDataSet= new OmnipotenceIntfDataSet();
    mOmnipotenceIntfDataSet = (OmnipotenceIntfDataSet)mInputData.getObjectByObjectName("OmnipotenceIntfDataSet",0);
    
    Connection conn = DBConnPool.getConnection();

    if (conn==null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OtoFBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    try
    {
      conn.setAutoCommit(false);
      
      OmnipotenceIntfDataDBSet mOmnipotenceIntfDataDBSet = new OmnipotenceIntfDataDBSet(conn);
      mOmnipotenceIntfDataDBSet.set(mOmnipotenceIntfDataSet);
      if (mOmnipotenceIntfDataDBSet.delete() == false)
      {
        // @@错误处理
        this.mErrors.copyAllErrors(mOmnipotenceIntfDataDBSet.mErrors);
        CError tError = new CError();
        tError.moduleName = "OtoFBLS";
        tError.functionName = "saveData";
        tError.errorMessage = "删除数据失败!";
        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
        System.out.println(tError.errorMessage);
        return false;
      }
      if (mOmnipotenceIntfDataDBSet.insert() == false)
      {
        // @@错误处理
        this.mErrors.copyAllErrors(mOmnipotenceIntfDataDBSet.mErrors);
        CError tError = new CError();
        tError.moduleName = "OtoFBLS";
        tError.functionName = "saveData";
        tError.errorMessage = "保存数据失败!";
        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
        System.out.println(tError.errorMessage);
        return false;
      }
      
      LITranInfoDBSet mLITranInfoDBSet = new LITranInfoDBSet(conn);
      mLITranInfoDBSet.set(mLITranInfoSet);
      if (mLITranInfoDBSet.insert() == false)
      {
        // @@错误处理
        this.mErrors.copyAllErrors(mLITranInfoDBSet.mErrors);
        CError tError = new CError();
        tError.moduleName = "OtoFBLS";
        tError.functionName = "saveData";
        tError.errorMessage = "保存数据失败!";
        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
        return false;
      }

      LITranErrDBSet mLITranErrDBSet = new LITranErrDBSet(conn);
      mLITranErrDBSet.set(mLITranErrSet);
      if (mLITranErrDBSet.insert() == false)
      {
        // @@错误处理
        this.mErrors.copyAllErrors(mLITranErrDBSet.mErrors);
        CError tError = new CError();
        tError.moduleName = "OtoFBLS";
        tError.functionName = "saveData";
        tError.errorMessage = "保存数据失败!";
        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
        return false;
      }

      conn.commit() ;
      conn.close();
    } // end of try
    catch (Exception ex)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ReportBLS";
      tError.functionName = "submitData";
      tError.errorMessage = ex.toString();
      this.mErrors .addOneError(tError);
      try{conn.rollback() ;
      conn.close();
      }

      catch(Exception e){}
      return false;
    }
    return true;
  }
  private boolean wnsaveData()
  {
    OmnipotenceIntfDataSet mOmnipotenceIntfDataSet= new OmnipotenceIntfDataSet();
    mOmnipotenceIntfDataSet = (OmnipotenceIntfDataSet)mInputData.getObjectByObjectName("OmnipotenceIntfDataSet",0);
    Connection conn =null;
    try{
        Class.forName("com.ibm.db2.jcc.DB2Driver").newInstance();
        String url = "jdbc:db2://10.252.1.123:50000/ACCOUNT"; // ACCOUNT8为你的数据库名,ACCOUNT8是财务的正式数据库
        String user = "db2inst1";
        String password = "cwdb123";
        conn = DriverManager.getConnection(url, user, password);
    }catch (Exception e)
    {
        CError tError = new CError();
        tError.moduleName = "OtoFBLS";
        tError.functionName = "saveData";
        tError.errorMessage = "财务数据库连接失败!";
        this.mErrors .addOneError(tError) ;
        return false;
    }
    try
    {
      conn.setAutoCommit(false);
      OmnipotenceIntfDataDBSet mOmnipotenceIntfDataDBSet = new OmnipotenceIntfDataDBSet(conn);
      mOmnipotenceIntfDataDBSet.set(mOmnipotenceIntfDataSet);
//      if (mOmnipotenceIntfDataDBSet.delete() == false)
//      {
//        // @@错误处理
//        this.mErrors.copyAllErrors(mOmnipotenceIntfDataDBSet.mErrors);
//        CError tError = new CError();
//        tError.moduleName = "OtoFBLS";
//        tError.functionName = "saveData";
//        tError.errorMessage = "删除数据失败!";
//        this.mErrors .addOneError(tError) ;
//        conn.rollback() ;
//        conn.close();
//        System.out.println(tError.errorMessage);
//        return false;
//      }
      if (mOmnipotenceIntfDataDBSet.insert() == false)
      {
        // @@错误处理
        this.mErrors.copyAllErrors(mOmnipotenceIntfDataDBSet.mErrors);
        CError tError = new CError();
        tError.moduleName = "OtoFBLS";
        tError.functionName = "saveData";
        tError.errorMessage = "保存数据失败!";
        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
        System.out.println(tError.errorMessage);
        return false;
      }
      conn.commit() ;
      conn.close();
    } // end of try
    catch (Exception ex)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ReportBLS";
      tError.functionName = "submitData";
      tError.errorMessage = ex.toString();
      this.mErrors .addOneError(tError);
      try{conn.rollback() ;
      conn.close();
      }

      catch(Exception e){}
      return false;
    }
    System.out.println("保存成功");
    return true;
  }
}
