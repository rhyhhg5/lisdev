package com.sinosoft.lis.otof;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.tb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import java.util.*;
import java.lang.String;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:计算累计生息账户当期利息
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class CalAccInterestOtherUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mStrLJAGetSet = new String();
  private String mOperate;
  private String mResultQueryString = "";

  public CalAccInterestOtherUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    System.out.println("---CalAccInterestOtherUI BEGIN---"+cOperate);
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    CalAccInterestOtherBL tCalAccInterestOtherBL = new CalAccInterestOtherBL();
    if (tCalAccInterestOtherBL.submitData(cInputData,mOperate) == false)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tCalAccInterestOtherBL.mErrors);
      System.out.println("---CalAccInterestOtherUI fail---"+mOperate);
      return false;
    }

    System.out.println("---CalAccInterestOtherUI success---"+mOperate);
    return true;
  }

  public VData getQueryResult()
  {
    return mResult;
  }

  public String getLJAGetSet()
  {
    return mStrLJAGetSet;
  }
  public static void main(String[] args)
  {
    GlobalInput tG = new GlobalInput();
    tG.Operator = "001";
    tG.ManageCom = "86";
    tG.ComCode = "86";
    TransferData tTransferData= new TransferData();
    tTransferData.setNameAndValue("BalaDate","2005-10-10");
    LCInsureAccSchema tLCInsureAccSchema= new LCInsureAccSchema();
    tLCInsureAccSchema.setPolNo("86110020040210000142");
    tLCInsureAccSchema.setInsuAccNo("000005");
    //2005.1.20杨明注释掉问题代码tLCInsureAccSchema.setOtherNo("86110020040210000142");

    VData tVData = new VData();
    tVData.add(tLCInsureAccSchema);
    tVData.add(tTransferData) ;
    tVData.add( tG );
    CalAccInterestOtherUI ui = new CalAccInterestOtherUI();
    if( ui.submitData( tVData, "INSERT" ) == true )
      System.out.println("---ok---");
    else
      System.out.println("---NO---");
  }
}
