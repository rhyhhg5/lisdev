package com.sinosoft.lis.otof;

import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 承保暂交费业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class LITranLogBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  private LITranLogSchema mLITranLogSchema = new LITranLogSchema();

  private GlobalInput mG = new GlobalInput();

  public LITranLogBL() {}

  public boolean submitData(VData cInputData,String cOperate)
  {
    mInputData = (VData)cInputData.clone() ;
    this.mOperate = cOperate;
    System.out.println("开始处理getInputData");

    mLITranLogSchema=(LITranLogSchema)mInputData.getObjectByObjectName("LITranLogSchema",0);
    mG.setSchema((GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0));
    System.out.println("结束处理getInputData");
    if (cOperate.equals("INSERT"))
    {
      if (!dealData())
        return false;
      System.out.println("---dealData---");
    }
    System.out.println("结束处理dealdata");
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealData()
  {
    //准备给后台的数据
    prepareOutputData();
    //数据提交
    LITranLogBLS tLITranLogBLS = new LITranLogBLS();
    System.out.println("Start Report BL Submit...");

    if (!tLITranLogBLS.submitData(mInputData,mOperate))
    {
      // @@错误处理
      System.out.println("处理出错");
      this.mErrors.copyAllErrors(tLITranLogBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "LITranLogBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */

  private boolean checkData()
  {
    boolean flag = true;
    return flag;
  }
  /**
   * 准备需要保存的数据
   */
  private void prepareOutputData()
  {
    mLITranLogSchema.setManageCom(mG.ManageCom);
    mLITranLogSchema.setOperator(mG.Operator);
    mLITranLogSchema.setMakeDate(PubFun.getCurrentDate());
    mLITranLogSchema.setMakeTime(PubFun.getCurrentTime());
    mLITranLogSchema.setNumBuss("7");
    mInputData.clear();
    mInputData.add(mLITranLogSchema);
    mResult.clear();
    mResult.add(mLITranLogSchema);
  }
}