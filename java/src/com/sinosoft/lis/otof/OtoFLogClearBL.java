  package com.sinosoft.lis.otof;
  import com.sinosoft.lis.sys.*;
  import com.sinosoft.lis.db.*;
  import com.sinosoft.lis.schema.*;
  import com.sinosoft.lis.vdb.*;
  import com.sinosoft.lis.vschema.*;
  import com.sinosoft.utility.*;
  import com.sinosoft.lis.bl.*;
  import com.sinosoft.lis.vbl.*;
  import com.sinosoft.lis.pubfun.*;
  /**
   * <p>Title: Web业务系统</p>
   * <p>Description: 日志表查询业务逻辑处理类</p>
   * <p>Copyright: Copyright (c) 2003</p>
   * <p>Company: Sinosoft</p>
   * @author GUOXIANG
   * @version 1.0
   */
  public class OtoFLogClearBL{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
      public  CErrors mErrors = new CErrors();
      /** 往后面传输数据的容器 */
      private VData mInputData ;
      /** 往界面传输数据的容器 */
      private VData mResult = new VData();
      /** 数据操作字符串 */
      private String mOperate;
      /** 数据查询Schema */
      private LITranInfoSchema mLITranInfoSchema = new LITranInfoSchema();
      /** 数据查询set */
      private LITranInfoSet mLITranInfoSet = new LITranInfoSet();
      /** 删除mutileline的数据set */
      private LITranInfoSet tLITranInfoSet = new LITranInfoSet();
      private GlobalInput mGlobalInput = new GlobalInput();
      /**构造函数*/
      public OtoFLogClearBL() {}
      public static void main(String[] args) {
      }
      /**
      * 传输数据的公共方法
      * @param: cInputData 输入的数据
      *         cOperate 数据操作
      * @return:
      */

      public boolean submitData(VData cInputData,String cOperate){
           //将操作数据拷贝到本类中
           mInputData = (VData)cInputData.clone() ;
           this.mOperate = cOperate;

           //进行业务处理:查询在本类，删除等在BLS类里
           if(cOperate.equals("Query")){
              //得到外部传入的数据,将数据备份到本类中
               if (!getInputData(cInputData))
                     return false;

               if (!queryOtoFLogClear())
                    return false;
               System.out.println("---开始处理queryOtoFLogClear---");
           }
           if(cOperate.equals("Delete")){
               if (!dealData())
                    return false;
               System.out.println("---dealData---");
           }
           return true;
      }
      /**
       * 结果集
       */
      public VData getResult(){
          return mResult;
      }

      /**
       * 从输入数据中得到所有对象
       * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
       * @param: cInputData 输入的数据
       */
      private boolean getInputData(VData cInputData){
          // 检验查询条件
          if(mOperate.equals("Query")){
               System.out.println("----------开始处理Query的getInputData------");
               mGlobalInput.setSchema((GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0));
               mLITranInfoSchema=(LITranInfoSchema)mInputData.getObjectByObjectName("LITranInfoSchema",0);
               System.out.println("----------结束处理Query的getInputData--------------");
          }
          return true;
     }

     /**
     * 查询日志表信息
      * 输出：如果发生错误则返回false,否则返回true
     */
     private boolean queryOtoFLogClear(){
           LITranInfoDB tLITranInfoDB = new LITranInfoDB();
           tLITranInfoDB.setSchema(mLITranInfoSchema);
           mLITranInfoSet=tLITranInfoDB.query();
           if (tLITranInfoDB.mErrors.needDealError() == true)
             {
             // @@错误处理
             this.mErrors.copyAllErrors(tLITranInfoDB.mErrors);
             CError tError = new CError();
             tError.moduleName = "OtoFLogClearBL";
             tError.functionName = "queryData";
             tError.errorMessage = "接口日志表查询失败!";
             this.mErrors.addOneError(tError);
             mLITranInfoSet.clear();
             return false;
           }
           if (mLITranInfoSet.size() == 0)
           {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "OtoFLogClearBL";
             tError.functionName = "queryData";
             tError.errorMessage = "未找到相关数据!";
             this.mErrors.addOneError(tError);
             mLITranInfoSet.clear();
             return false;
           }

           mResult.clear();
           if(!mResult.add(mLITranInfoSet))
             return false;
           return true;
   }
   private boolean dealData(){
         //准备给后台的数据
         //prepareOutputData();
         //数据提交
         OtoFLogClearBLS tOtoFLogClearBLS = new OtoFLogClearBLS();
         System.out.println("Start  BL Submit...");

         if (!tOtoFLogClearBLS.submitData(mInputData,mOperate)){
           // @@错误处理
           System.out.println("处理出错");
           this.mErrors.copyAllErrors(tOtoFLogClearBLS.mErrors);
           CError tError = new CError();
           tError.moduleName = "OtoFLogClearBL";
           tError.functionName = "submitData";
           tError.errorMessage = "数据提交失败!";
           this.mErrors .addOneError(tError) ;
           return false;
         }
         return true;
    }
}
