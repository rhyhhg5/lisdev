package com.sinosoft.lis.otof;


/**类的名称：XmlExport
 *类的描述：生成打印模板所对应的xml文件
 *
 *
 *
 *
 *
 */
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;


public class OtoFExport
{

    //public static  String UFINURL="http://10.0.0.2:8080/ui/ufin/sendVoucer.jsp";

    private Document myDocument;
    private Element myElement; //第一层循环临时变量
    private Element myElement2; //第一层内再循环临时变量
    private int col;


    //初始化myElement
    public Element initElement(String tname)
    {
        this.myElement = new Element(tname);
        return myElement;
    }


    //初始化myElement2
    public Element initElement2(String tname)
    {
        this.myElement2 = new Element(tname);
        return myElement2;
    }


    // @Method
    //初始化文件
    public Document createDocumentOtoF()
    {
        // Create the root element

        //ufinterface
        Element Level1 = new Element("ufinterface");

        Level1.addAttribute("roottag", "add-bill");
        Level1.addAttribute("docid", "989898989898");
        Level1.addAttribute("receiver", "1003");
        Level1.addAttribute("sender", "1101");
        Level1.addAttribute("proc", "ReqParam");
        Level1.addAttribute("codeexchange", "y");

        //create the document
        this.myDocument = new Document(Level1);

        return myDocument;
    }


    //输出xml文件，参数为路径，文件名
    public void outputDocumentToFileOtoF(String pathname, String filename)
    {
        //setup this like outputDocument
        try
        {
            XMLOutputter outputter = new XMLOutputter("  ", true, "UTF-8");
            //output to a file
            String str = pathname + filename + ".xml";
            FileOutputStream fos = new FileOutputStream(str);
            outputter.output(myDocument, fos);
            fos.close();
//      FileWriter writer = new FileWriter(str);
//      writer.close();
        }
        catch (java.io.IOException e)
        {
            e.printStackTrace();
        }
    }


    //输出xml文件，参数为路径，文件名
    public String outputDocumentToFileOtoF(String url) throws Exception
    {
        //setup this like outputDocument
        System.out.println("url:" + url);
        URL jspUrl = new URL(url);
        URLConnection uc = jspUrl.openConnection();

        //uc.connect();
        uc.setDoOutput(true);
        OutputStream os = uc.getOutputStream();

        try
        {
            XMLOutputter outputter = new XMLOutputter("  ", true, "GB2312");
            outputter.output(myDocument, os);
        }
        catch (java.io.IOException e)
        {
            e.printStackTrace();
        }
        os.flush();
        os.close();

        //uc.connect();
        InputStream is = uc.getInputStream();

        //os.close();

        int count = -1;
        byte[] bt = new byte[4096];
        StringBuffer sb = new StringBuffer();

        while ((count = is.read(bt)) > -1)
        {
            String s = new String(bt, 0, count);
            sb.append(s);
        }
        System.out.println("ttt:" + sb.toString());
        return sb.toString();

    }


    //向根结点中非循环元素添加属性
    //levelname 根结点下非循环结点元素名称 attr 元素属性 value 属性值
    public Document AddAttribute(String levelname, String attr, String value)
    {

        //get rootelement
        Element root = myDocument.getRootElement();

        Element level = myDocument.getRootElement().getChild(levelname);
        level.addAttribute(attr, value);

        return myDocument;
    }


    //向非循环指定结点添加元素,只支持四级
    //flag为0,1,2,3 分别对应结点级数，value为要加入结点值，
    public Document Addcontent(String name0, String name1, String name2,
                               String name3, String value, String flag)
    {
        if (flag.equals("0"))
        {
            Element level = new Element(name0);
            level.setText(value);
            myDocument.getRootElement().addContent(level);
        }

        if (flag.equals("1"))
        {
            Element level = new Element(name1);
            level.setText(value);
            myDocument.getRootElement().getChild(name0).addContent(level);
        }

        if (flag.equals("2"))
        {
            Element level = new Element(name2);
            level.setText(value);
            myDocument.getRootElement().getChild(name0).getChild(name1).
                    addContent(level);
        }

        if (flag.equals("3"))
        {
            Element level = new Element(name3);
            level.setText(value);
            myDocument.getRootElement().getChild(name0).getChild(name1).
                    getChild(name2).addContent(level);
        }
        return myDocument;
    }


    //提交第一层循环到指定结点下
    public void AddAllEle(String name0, String name1, String name2,
                          String name3, String value, String flag)
    {
        if (flag.equals("0"))
        {
            myDocument.getRootElement().addContent(myElement);
        }

        if (flag.equals("1"))
        {
            myDocument.getRootElement().getChild(name0).addContent(myElement);
        }

        if (flag.equals("2"))
        {
            myDocument.getRootElement().getChild(name0).getChild(name1).
                    addContent(myElement);
        }

        if (flag.equals("3"))
        {
            myDocument.getRootElement().getChild(name0).getChild(name1).
                    getChild(name2).addContent(myElement);
        }

        if (flag.equals("4"))
        {
            myDocument.getRootElement().getChild(name0).getChild(name1).
                    getChild(name2).getChild(name3).addContent(myElement);
        }
    }


    //提交第一层内的循环体到第一层循环内的指定结点下
    public void AddAllEle2(String name0, String name1, String name2,
                           String name3, String value, String flag)
    {
        if (flag.equals("0"))
        {
            myElement.addContent(myElement2);
        }

        if (flag.equals("1"))
        {
            myElement.getChild(name0).addContent(myElement2);
        }

        if (flag.equals("2"))
        {
            myElement.getChild(name0).getChild(name1).addContent(myElement);
        }

        if (flag.equals("3"))
        {
            myElement.getChild(name0).getChild(name1).getChild(name2).
                    addContent(myElement2);
        }

        if (flag.equals("4"))
        {
            myElement.getChild(name0).getChild(name1).getChild(name2).getChild(
                    name3).addContent(myElement2);
        }
    }


    //向element添加内容,name不为空添加元素，为空直接添加值
    public Element AddElement(String name, String value, String flag)
    {
        if (flag.equals("1"))
        {
            Element level = new Element(name);
            level.setText(value);
            this.myElement.addContent(level);
        }
        if (flag.equals("0"))
        {
            this.myElement.setText(value);
        }

        return myElement;
    }


    //向element2添加内容,name不为空添加元素，为空直接添加值
    public Element AddElement2(String name, String value, String flag)
    {
        if (flag.equals("1"))
        {
            Element level = new Element(name);
            level.setText(value);
            this.myElement2.addContent(name);
        }
        if (flag.equals("0"))
        {
            this.myElement2.setText(value);
        }

        return myElement2;
    }


    //向element下指定元素添加属性
    public Element AddEleAttr(String name, String attr, String value,
                              String flag)
    {
        if (flag.equals("0"))
        {
            myElement.addAttribute(attr, value);
        }

        if (flag.equals("1"))
        {
            myElement.getChild(name).addAttribute(attr, value);
        }
        return myElement;
    }


    //向element2下指定元素添加属性
    public Element AddEleAttr2(String name, String attr, String value,
                               String flag)
    {
        if (flag.equals("0"))
        {
            myElement2.addAttribute(attr, value);
        }

        if (flag.equals("1"))
        {
            myElement2.getChild(name).addAttribute(attr, value);
        }
        return myElement;
    }

    public static void main(String[] args)
    {
        try
        {
            URL turl = new URL(
                    "http://10.0.5.6:7001/ufin/addVoucherReturnXML.jsp");
            URLConnection uc = turl.openConnection();
            uc.setDoOutput(true);
            OutputStream os = uc.getOutputStream();
        }
        catch (MalformedURLException ex)
        {
            ex.printStackTrace();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }

        //uc.connect();
    }
}
