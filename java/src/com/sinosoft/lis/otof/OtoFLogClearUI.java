package com.sinosoft.lis.otof;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:接口日志查询
 * </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: Sinosoft</p>
 * @author guoxiang
 * @version 1.0
 */
public class OtoFLogClearUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public OtoFLogClearUI() {}

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    System.out.println("UI BEGIN");
    this.mOperate = cOperate;

    OtoFLogClearBL tOtoFLogClearBL=new OtoFLogClearBL();

    if (tOtoFLogClearBL.submitData(cInputData,mOperate) == false)
	{
  		// @@错误处理
      this.mErrors.copyAllErrors(tOtoFLogClearBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "OtoFLogClearUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors .addOneError(tError) ;
      mInputData.clear();
      return false;
	}
	else
		mInputData = tOtoFLogClearBL.getResult();
    return true;
  }

  public VData getResult()
  {
  	return mInputData;
  }
  public static void main(String[] args) {

    // LITranInfoSchema  tLITranInfoSchema = new LITranInfoSchema();
     //tLITranInfoSchema.setBatchNo("200002041");
//    tLITranInfoSchema.setVoucherID("0");
//     tLITranInfoSchema.setMakeDate("2003-6-4");
//     tLITranInfoSchema.setBussDate("2003-5-15");

     // 准备传输数据 VData
//     VData tVData = new VData();
//     OtoFLogClearUI tOtoFLogClearUI = new OtoFLogClearUI();
//     GlobalInput tG = new GlobalInput();
//     tVData.addElement(tG);
//     tVData.add(tLITranInfoSchema);
//     tOtoFLogClearUI.submitData(tVData,"Delete");
  }

}