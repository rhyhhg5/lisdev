package com.sinosoft.lis.otof;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 投保业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author TJJ
 * @version 1.0
 */
public class CalAccInterestBL  {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData;
  /** 数据操作字符串 */
  private Reflections mReflections = new Reflections();
  private String mOperate;
  private String mOperater;
  private String mManageCom;
  private String mPolNo;
  private String mOtherNo;
  private String mInsuAccNo;
  private String mBalanceDate;
  private String mRateType="D";
  private String mIntervalType="D";
  private String CurrentDate=PubFun.getCurrentDate();
  private String CurrentTime=PubFun.getCurrentTime();
  private String mLimit ;
  private String mActuGetNo ;//产生实付号码
  private String mGetNoticeNo;//产生即付通知书号
  private String mBalaDate;
  private MMap map = new MMap();

  /** 全局数据 */
  private GlobalInput mGlobalInput =new GlobalInput() ;
  /** 业务处理相关变量 */
  private LCInsureAccSet mLCInsureAccSet = new LCInsureAccSet();


  private LJABonusGetSchema mLJABonusGetSchema = new LJABonusGetSchema();
  private String mDrawer = "";
  private String mDrawerID = "";
  private VData mResult = new VData();
  public CalAccInterestBL() {
  }


  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData,String cOperate)
  {
	//将操作数据拷贝到本类中
	this.mOperate =cOperate;
	//得到外部传入的数据,将数据备份到本类中
	if (!getInputData(cInputData))
	  return false;

	//进行业务处理
	if (!dealData())
	{
	  return false;
	}

	if(mOperate.equals("INSERT") )
	{

	}


	 return true;
  }



  public String getActuGetNo()
  {
	return this.mActuGetNo;
  }




  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData()
  {
	if(mOperate.equals("INSERT") )
	{
	  mResult.clear();

	  //查询出所有的红利累计生息账户
       String tStringSQL=" select * from LCInsureAcc where InsuAccNo='000001' and AccType='004'  and BalaDate <'"+mBalaDate+"' order by PolNo ";
	  LCInsureAccDB tLCInsureAccDB = new LCInsureAccDB();
	  mLCInsureAccSet=tLCInsureAccDB.executeQuery(tStringSQL) ;
	  if(mLCInsureAccSet==null)
	  {
		CError tError = new CError();
		tError.moduleName = "CalAccInterestBL";
		tError.functionName = "getInputData";
		tError.errorMessage = "查询红利累计生息账户失败!";
		this.mErrors .addOneError(tError) ;
		return false;
	  }

	  int tSetCount =0;
      for(int i=1;i<=mLCInsureAccSet.size() ;i++)
	  {
		tSetCount++;
		LCInsureAccSchema mLCInsureAccSchema = new LCInsureAccSchema();
		mLCInsureAccSchema=mLCInsureAccSet.get(i);
		LCPolSchema mLCPolSchema  = new LCPolSchema ();
		mLCPolSchema.setPolNo(mLCInsureAccSchema.getPolNo() ) ;

		if(!checkData(mLCPolSchema))
	      return false;

		double tCurrentMoney=getCurrentMoney(mLCInsureAccSet.get(i));
		if(!prepareGet(mLCInsureAccSchema,mLCPolSchema,tCurrentMoney))
			return false;

        if(tSetCount==100||i==mLCInsureAccSet.size())//每一百条数据提交一次
		 {
		  tSetCount=0;
		  if(!prepareOutputData())
	        return false;
          //公共提交
	     PubSubmit tPubSubmit =new PubSubmit();
	     if(tPubSubmit.submitData(mResult,"INSERT")==false)
            return false;

	     mResult.clear() ;
		}
	  }
	}
	return true;
  }
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
	//从输入数据中得到所有对象
	//获得全局公共数据
	 mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
	 mInputData = cInputData ;
	 if ( mGlobalInput == null  )
	 {
	   // @@错误处理
	   //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
	   CError tError = new CError();
	   tError.moduleName = "CalAccInterestBL";
	   tError.functionName = "getInputData";
	   tError.errorMessage = "前台传输全局公共数据失败!";
	   this.mErrors .addOneError(tError) ;
	   return false;
	 }

	 //获得操作员编码
	 mOperater = mGlobalInput.Operator;
	 if ( mOperater == null || mOperater.trim().equals("") )
	 {
	   // @@错误处理
	   //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
	   CError tError = new CError();
	   tError.moduleName = "CalAccInterestBL";
	   tError.functionName = "getInputData";
	   tError.errorMessage = "前台传输全局公共数据Operater失败!";
	   this.mErrors .addOneError(tError) ;
	   return false;
	 }

	 //获得登陆机构编码
	 mManageCom = mGlobalInput.ManageCom;
	 if ( mManageCom == null || mManageCom.trim().equals("") )
	 {
	   // @@错误处理
	   //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
	   CError tError = new CError();
	   tError.moduleName = "CalAccInterestBL";
	   tError.functionName = "getInputData";
	   tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
	   this.mErrors .addOneError(tError) ;
	   return false;
	 }

	 TransferData mTransferData= new TransferData();
	 mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData",0);
	 //获得业务数据
	  if ( mTransferData == null  )
	  {
		// @@错误处理
		//this.mErrors.copyAllErrors( tLCPolDB.mErrors );
		CError tError = new CError();
		tError.moduleName = "CalAccInterestBL";
		tError.functionName = "getInputData";
		tError.errorMessage = "前台传输业务数据失败!";
		this.mErrors .addOneError(tError) ;
		return false;
	  }

	  if(mOperate.equals("INSERT") )
	  {
		mBalaDate = (String)mTransferData.getValueByName("BalaDate");
		if ( mBalaDate == null || mBalaDate.trim().equals("") )
		{
		  // @@错误处理
		  //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
		  CError tError = new CError();
		  tError.moduleName = "CalAccInterestBL";
		  tError.functionName = "getInputData";
		  tError.errorMessage = "前台传输业务数据中BalaDate失败!";
		  this.mErrors .addOneError(tError) ;
		  return false;
		}
	  }
	 return true;
  }
  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData()
  {
   mResult.clear() ;
   mResult.add(map) ;
   map = new MMap();
   return true;
  }


  private boolean checkData(LCPolSchema mLCPolSchema)
  {
	//校验保单信息
	LCPolDB tLCPolDB = new LCPolDB();
	tLCPolDB.setPolNo(mLCPolSchema.getPolNo()) ;
	if(!tLCPolDB.getInfo())
	{
	  CError tError = new CError();
	  tError.moduleName = "BalaDate";
	  tError.functionName = "checkData";
	  tError.errorMessage = "保单"+mPolNo+"信息查询失败!";
	  this.mErrors .addOneError(tError) ;
	  return false;
	}
	mLCPolSchema.setSchema(tLCPolDB) ;



	return true;
  }



 public VData getResult()
{
	 return mResult;
 }
  private boolean prepareGet(LCInsureAccSchema mLCInsureAccSchema,LCPolSchema mLCPolSchema,double mCurrentMoney)
  {
    LCInsureAccTraceSchema mLXLCInsureAccTraceSchema = new LCInsureAccTraceSchema();

	String mLimit = PubFun.getNoLimit( mLCPolSchema.getManageCom() );
	String tLXSerialNo = PubFun1.CreateMaxNo( "SERIALNO", mLimit );//产生利息流水号码

	String tDrawerID="";
	LCAppntIndDB tLCAppntIndDB = new LCAppntIndDB();
	tLCAppntIndDB.setPolNo(mLCPolSchema.getPolNo());
	tLCAppntIndDB.setCustomerNo(mLCPolSchema.getAppntNo());
	if(tLCAppntIndDB.getInfo()==false)
	{
	  tDrawerID="";
	}
	else
	{
	  tDrawerID = tLCAppntIndDB.getIDNo();
	}


	//准备账户轨迹表信息
	//填充保险帐户表记价履历表一条利息记录
	if(mCurrentMoney-mLCInsureAccSchema.getInsuAccBala()>0)
	{
	mLXLCInsureAccTraceSchema.setSerialNo(tLXSerialNo);
	//2005.1.20杨明注释掉问题代码mLXLCInsureAccTraceSchema.setInsuredNo(mLCInsureAccSchema.getInsuredNo());
	mLXLCInsureAccTraceSchema.setPolNo(mLCInsureAccSchema.getPolNo());
	mLXLCInsureAccTraceSchema.setMoneyType("LX");
	mLXLCInsureAccTraceSchema.setRiskCode(mLCInsureAccSchema.getRiskCode());
	mLXLCInsureAccTraceSchema.setOtherNo(mLCInsureAccSchema.getPolNo());
	mLXLCInsureAccTraceSchema.setOtherType("1");
	mLXLCInsureAccTraceSchema.setMoney(mCurrentMoney-mLCInsureAccSchema.getInsuAccBala());
	mLXLCInsureAccTraceSchema.setContNo(mLCInsureAccSchema.getContNo());
	mLXLCInsureAccTraceSchema.setGrpPolNo(mLCInsureAccSchema.getGrpPolNo());
	mLXLCInsureAccTraceSchema.setInsuAccNo(mLCInsureAccSchema.getInsuAccNo());
/*Lis5.3 upgrade set
	mLXLCInsureAccTraceSchema.setAppntName(mLCInsureAccSchema.getAppntName());
*/
  mLXLCInsureAccTraceSchema.setState(mLCInsureAccSchema.getState());
	mLXLCInsureAccTraceSchema.setManageCom(mLCInsureAccSchema.getManageCom());
	mLXLCInsureAccTraceSchema.setOperator(mLCInsureAccSchema.getOperator());
	mLXLCInsureAccTraceSchema.setMakeDate(CurrentDate);
	mLXLCInsureAccTraceSchema.setMakeTime(CurrentTime);
	mLXLCInsureAccTraceSchema.setModifyDate(CurrentDate);
	mLXLCInsureAccTraceSchema.setModifyTime(CurrentTime);
	mLXLCInsureAccTraceSchema.setPayDate(mBalaDate) ;

	//准备账户信息
	mLCInsureAccSchema.setBalaDate(mBalaDate);
	mLCInsureAccSchema.setBalaTime(CurrentTime);
	mLCInsureAccSchema.setInsuAccBala(mCurrentMoney);
	mLCInsureAccSchema.setModifyDate(CurrentDate) ;
	mLCInsureAccSchema.setModifyTime(CurrentTime);
	mLCInsureAccSchema.setOperator(mOperater) ;

	//添加账户表数据
	if(mLCInsureAccSchema != null && mLCInsureAccSchema.getPolNo()!= null && !mLCInsureAccSchema.getPolNo().trim().equals("") )
   map.put(mLCInsureAccSchema, "UPDATE");




//添加账户轨迹表利息数据
if(mLXLCInsureAccTraceSchema != null && mLXLCInsureAccTraceSchema.getPolNo()!= null && !mLXLCInsureAccTraceSchema.getPolNo().trim().equals("") )
	 map.put(mLXLCInsureAccTraceSchema, "INSERT");
	}


	return true ;
  }


  private double getCurrentMoney( LCInsureAccSchema mLCInsureAccSchema)
  {
	AccountManage mAccountManage = new AccountManage();

	mInsuAccNo=mLCInsureAccSchema.getInsuAccNo();
	mPolNo =mLCInsureAccSchema.getPolNo() ;
	mBalanceDate = mBalaDate;//前台传入的结息日期
	mRateType="Y";
	mIntervalType="D";
	double tCurrentMoney = mAccountManage.getAccBalance(mInsuAccNo,mPolNo,mBalanceDate,mRateType,mIntervalType) ;
	tCurrentMoney= PubFun.setPrecision(tCurrentMoney,"0.00") ;
	return tCurrentMoney;
  }




  public static void main(String[] args) {

  }
}
