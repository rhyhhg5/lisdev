package com.sinosoft.lis.wpleace;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PlApprovalUI 
{

	 /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();

  private VData mResult = new VData();
  //业务处理相关变量
  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();
  private String mDay[] = null;


  public PlApprovalUI()
  {}

  /**
   传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate,String flag)
  {
      try
      {
          if (!cOperate.equals("APPROVE"))
          {
              buildError("submitData", "不支持的操作字符串");
              return false;
          }

          // 得到外部传入的数据，将数据备份到本类中
          if (!getInputData(cInputData))
          {
              return false;
          }

          // 进行业务处理
          if (!dealData())
          {
              return false;
          }

          if (!prepareOutputData(cInputData))
          {
              return false;
          }

          PlApprovalBL mPlApprovalBL = new PlApprovalBL(); 
          System.out.println("Start PlApprovalUI Submit ...");

          if (!mPlApprovalBL.submitData(cInputData, cOperate,flag))
          {
              if (mPlApprovalBL.mErrors.needDealError())
              {
                  mErrors.copyAllErrors(mPlApprovalBL.mErrors);
                  return false;
              }
              else
              {
                  buildError("submitData","PlApproveBL，但是没有提供详细的出错信息");
                  return false;
              }
          }
          else
          {
              mResult = mPlApprovalBL.getResult();
              return true;
          }
      }
      catch (Exception e)
      {
          e.printStackTrace();
          buildError("submitData", "报错信息："+e.toString());
          return false;
      }
  }


  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData(VData vData)
  {
      return true;
  }

  /**
   * 根据前面的输入数据，进行UI逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData()
  {
      return true;
  }

  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
      return true;
  }

  public VData getResult()
  {
      return this.mResult;
  }

  private void buildError(String szFunc, String szErrMsg)
  {
      CError cError = new CError();

      cError.moduleName = "PlApprovalUI";
      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      this.mErrors.addOneError(cError);
  }





}
