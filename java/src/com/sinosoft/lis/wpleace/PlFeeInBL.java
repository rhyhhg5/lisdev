package com.sinosoft.lis.wpleace;

import utils.system;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.midplat.*;

import java.io.File;
import jxl.*;
import jxl.write.*;
import jxl.write.biff.RowsExceededException;
import jxl.format.UnderlineStyle;

//import com.sinosoft.lis.vschema.LIPliceRentInfoSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PlFeeInBL 
{


	  /** 错误处理类，每个需要错误处理的类中都放置该类 */
	  public CErrors mErrors = new CErrors();
	  private VData mResult = new VData();

	  /** 往后面传输数据的容器 */
	  private VData mInputData;

	  /** 全局数据 */
	  private GlobalInput mGlobalInput = new GlobalInput();

	  /** 数据操作字符串 */
	  private String mOperate;

	  /** 业务处理相关变量 */
	  private String placeNo;
	  private PlaceFeeImportSet  tPlaceFeeImportSet=new PlaceFeeImportSet();

	  public PlFeeInBL(String tplaceNo,GlobalInput tg)
	  {
		  placeNo=tplaceNo;
		  mGlobalInput=tg;
	  }

	  private void buildError(String szFunc, String szErrMsg) {
	     CError cError = new CError();
	     cError.moduleName = "PlFeeInBL";
	     cError.functionName = szFunc;
	     cError.errorMessage = szErrMsg;
	     this.mErrors.addOneError(cError);
	  }


	  /**
	   * 传输数据的公共方法
	   * @param: cInputData 输入的数据
	   *         cOperate 数据操作
	   * @return:
	   */
	  public boolean doUp(String path,String fileName)
	  {
		  System.out.println("文件路径及名称："+path+fileName);
		  try {
			  Workbook book=Workbook.getWorkbook(new File(path.trim()+fileName.trim()));
			  //获取第一个工作表sheet
			  Sheet sheet1=book.getSheet(0);  //租金费用
			  Sheet sheet2=book.getSheet(1);  //物业费用
			  Sheet sheet3=book.getSheet(2);  //装修费用
			  
			  //归档
			  if (!makePlaceFeeImport(sheet1,"01")) {
				return false;
			  }
			  if (!makePlaceFeeImport(sheet2,"02")) {
					return false;
				}
			  if (!makePlaceFeeImport(sheet3,"03")) {
					return false;
				}
			  
			  PubSubmit ps = new PubSubmit();
	    	  MMap map=new MMap();
	    	  String delSQL="delete from PlaceFeeImport where placeno='"+placeNo+"'";  //删除历史数据
	    	  map.put(delSQL,"DELETE");
	          map.put(tPlaceFeeImportSet,"INSERT");
	          mResult.add(map);
		      if (!ps.submitData(mResult, null)) {
		    	  if (ps.mErrors.needDealError())
		          {
		              this.mErrors.copyAllErrors(ps.mErrors);
		              return false;
		          }
		          else
		          {
		              buildError("doUp","PubSubmit，但是没有提供详细的出错信息!");
		              return false;
		          }
		      }
		      return true;
			  
		} catch (Exception e) {
			e.printStackTrace();
			return false;
			// TODO: handle exception
		}
	  }
	  
	  //拼装插入sql
	  public boolean makePlaceFeeImport(Sheet tSheet,String feeType)
	  {
		  PlaceFeeImportSchema tPlaceFeeImportSchema=null;
		  Cell tCell=null;
		  System.out.println("费用类型--"+feeType+";行数--"+tSheet.getRows()+";列数--"+tSheet.getColumns());
		  for (int i = 1; i < tSheet.getRows(); i++) {
			  String tCellContents = "";
			  tPlaceFeeImportSchema=new PlaceFeeImportSchema();
			  for (int j = 0; j < tSheet.getColumns(); j++) {
				tCell=tSheet.getCell(j,i);
				tCellContents = tCell.getContents();
				if((!tCellContents.equals("")) && (!tCellContents.equals("null")) && (tCellContents!=null)){
					if (j==0) {
						tPlaceFeeImportSchema.setPayDate(tCell.getContents());
					} else if(j==1){
						tPlaceFeeImportSchema.setMoney(tCell.getContents());
					}else if(j==2){
						tPlaceFeeImportSchema.setPayBeginDate(tCell.getContents().trim()+"-1");
					}else if(j==3){
						tPlaceFeeImportSchema.setPayEndDate(tCell.getContents().trim()+"-1");
					}else{
						buildError("makePlaceFeeImport","模板格式错误！");
						return false;
					}
					System.out.println("cell:"+tCell.getContents());
				}
			}
			  if((!tCellContents.equals("")) && (!tCellContents.equals("null")) && (tCellContents!=null)){
				  System.out.println("费用类型："+feeType);
				  tPlaceFeeImportSchema.setPlaceNo(placeNo);
				  tPlaceFeeImportSchema.setFeeType(feeType);
				  tPlaceFeeImportSchema.setSOrder(Integer.toString(i));
				  tPlaceFeeImportSchema.setOperator(mGlobalInput.Operator);
				  tPlaceFeeImportSchema.setMakeDate(PubFun.getCurrentDate());
				  tPlaceFeeImportSchema.setMakeTime(PubFun.getCurrentTime());
				  tPlaceFeeImportSchema.setModifyDate(PubFun.getCurrentDate());
				  tPlaceFeeImportSchema.setModifyTime(PubFun.getCurrentTime());
				  tPlaceFeeImportSet.add(tPlaceFeeImportSchema);
				}
			  System.out.println("============");
			  
		}  
		  return true;
	  }
	  /**
	   * 从输入数据中得到所有对象
	   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	   */
	 public VData getResult()
	  {
	    return this.mResult;
	  }

}








