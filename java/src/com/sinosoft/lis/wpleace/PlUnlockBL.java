/*
* <p>ClassName: PlUnlockBL </p>
* <p>Description: PlUnlockBL类文件 </p>
* <p>Copyright: Copyright (c) 2002</p>
* <p>Company: sinosoft </p>
* @Database: 职场管理
* @CreateDate：2012-09-14
 */
package com.sinosoft.lis.wpleace;

import com.sinosoft.lis.pubfun.*;

import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;

public class PlUnlockBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();
  private VData mResult = new VData();

  /** 往后面传输数据的容器 */
  private VData mInputData;

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();

  /** 数据操作字符串 */
  private String mOperate;

  /** 业务处理相关变量 */
  private LIPlaceRentInfoSchema mLIPlaceRentInfoSchema = new LIPlaceRentInfoSchema();
  private LIPlaceRentInfoSet mLIPlaceRentInfoSet= new LIPlaceRentInfoSet();
  public PlUnlockBL()
  {
  }

  private void buildError(String szFunc, String szErrMsg) {
     CError cError = new CError();
     cError.moduleName = "PlUnlockBL";
     cError.functionName = szFunc;
     cError.errorMessage = szErrMsg;
     this.mErrors.addOneError(cError);
  }


  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate)
  {
     mInputData = cInputData;
     mOperate = cOperate;
     if (!getInputData(cInputData)) {
        return false;
     }
     if (!checkData()) {
        return false;
     }
    //进行业务处理
    if (!dealData())
    {
      // @@错误处理
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
    {
      return false;
    }
    PubSubmit ps = new PubSubmit();
    System.out.println("submitdata===============");
    if (!ps.submitData(mResult, null)) {
        this.mErrors.copyAllErrors(ps.mErrors);
        return false;
    }
    System.out.println("PlUnlockBL======submitdata");
    return true;
  }
 private boolean checkData()
 {
    return true;

 }


  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData()
  {
	  return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    this.mGlobalInput=(GlobalInput) cInputData.getObjectByObjectName("GlobalInput",0);
    this.mLIPlaceRentInfoSet = (LIPlaceRentInfoSet)cInputData.getObjectByObjectName ("LIPlaceRentInfoSet",0);
    System.out.println("getinputdata===============");
    return true;
  }

  private boolean prepareOutputData()
  {
    try
    {
    	if(mOperate.equals("ADD")){
    		MMap map=new MMap();
    		// 01 非锁定  02 锁定
    		mLIPlaceRentInfoSchema = mLIPlaceRentInfoSet.get(1);
    		String tPlaceno = mLIPlaceRentInfoSchema.getPlaceno();
    		System.out.println("tPlaceno====="+tPlaceno);
    		String strSql = "update LIPlaceRentInfo set Islock = '01',Modifydate = '"+PubFun.getCurrentDate()+"',Modifytime = '"+PubFun.getCurrentTime()+"' where Placeno='"+tPlaceno+"' ";
        	map.put(strSql,"UPDATE");
        	mResult.add(map);
        	System.out.println("prepareOutputData==========");
    	}else {
    		buildError("prepareOutputData", "PlUnlockBL报错：不支持的操作类型！");
		}  	
    }
    catch (Exception ex)
    {
      // @@错误处理
      ex.printStackTrace();
      buildError("prepareOutputData", "PlUnlockBL报错："+ex.toString());
      System.out.println("prepareOutputData==========false");
      return false;
    }

    return true;
  }

  public VData getResult()
  {
    return this.mResult;
  }
}