/*
* <p>ClassName: LAAssessInputUI </p>
* <p>Description: LAAssessUI类文件 </p>
* <p>Copyright: Copyright (c) 2002</p>
* <p>Company: sinosoft </p>
* @Database: 销售管理
* @CreateDate：2003-06-21
 */
package com.sinosoft.lis.wpleace;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class PlNextUI {
	 /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mDay[] = null;


    public PlNextUI()
    {}

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("NEXT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }

            // 进行业务处理
            if (!dealData())
            {
                return false;
            }

            if (!prepareOutputData(cInputData))
            {
                return false;
            }

            PlNextBL mPlNextBL = new PlNextBL(); 
            System.out.println("Start PlNextBL UI Submit ...");

            if (!mPlNextBL.submitData(cInputData, cOperate))
            {
                if (mPlNextBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(mPlNextBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData","PlNextBL，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = mPlNextBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("submitData", "错误信息"+e.toString());
            return false;
        }
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "PlNextUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}