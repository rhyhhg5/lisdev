package com.sinosoft.lis.wpleace;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LIPlaceChangeTraceSchema;
import com.sinosoft.lis.vschema.LIPlaceChangeTraceSet;
import com.sinosoft.lis.vschema.LIPlaceRentFeeBSet;
import com.sinosoft.lis.vschema.LIPlaceRentInfoBSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PlModifyBL 
{

	  /** 错误处理类，每个需要错误处理的类中都放置该类 */
	  public CErrors mErrors = new CErrors();
	  private VData mResult = new VData();

	  /** 业务处理相关变量 */

	  //private String mUpdateLAAgentSQL = "";        //更新考核清退的人的状态为离职
	 
	  private LIPlaceRentInfoBSet mLIPlaceRentInfoBSet = new LIPlaceRentInfoBSet();
	  private LIPlaceRentFeeBSet mLIPlaceRentFeeBSet = new LIPlaceRentFeeBSet();
	  private LIPlaceChangeTraceSet mLIPlaceChangeTraceSet = new LIPlaceChangeTraceSet();
	  
	  private String codeno="";
	  private String updateMainSql="";
	  public PlModifyBL()
	  {
	  }

	  private void buildError(String szFunc, String szErrMsg) {
	     CError cError = new CError();
	     cError.moduleName = "PlModifyBL";
	     cError.functionName = szFunc;
	     cError.errorMessage = szErrMsg;
	     this.mErrors.addOneError(cError);
	  }


	  /**
	   * 传输数据的公共方法
	   * @param: cInputData 输入的数据
	   *         cOperate 数据操作
	   * @return:
	   */
	  public boolean submitData(VData cInputData, String cOperate)
	  {
	     if (!getInputData(cInputData)) {
	        return false;
	     }
	     if (!checkData()) {
	        return false;
	     }
	    //进行业务处理
	    if (!dealData())
	    {
	      return false;
	    }
	    //准备往后台的数据
	    if (!prepareOutputData())
	    {
	      return false;
	    }
	    PubSubmit ps = new PubSubmit();
	    System.out.println("submitdata===============");
	    if (!ps.submitData(mResult, null)) {
	        this.mErrors.copyAllErrors(ps.mErrors);
	        return false;
	    }
	    System.out.println("PlModifyBL======submitdata");
	    return true;
	  }
	 private boolean checkData()
	 {
	    return true;

	 }


	  /**
	   * 根据前面的输入数据，进行BL逻辑处理
	   * 如果在处理过程中出错，则返回false,否则返回true
	   */
	  private boolean dealData()
	  {
		  return true;
	  }

	  /**
	   * 从输入数据中得到所有对象
	   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	   */
	  private boolean getInputData(VData cInputData)
	  {
	    try{
	    	this.mLIPlaceRentInfoBSet=(LIPlaceRentInfoBSet) cInputData.getObjectByObjectName("LIPlaceRentInfoBSet",0);
		    this.mLIPlaceRentFeeBSet=(LIPlaceRentFeeBSet) cInputData.getObjectByObjectName("LIPlaceRentFeeBSet",0);
		    this.mLIPlaceChangeTraceSet=(LIPlaceChangeTraceSet) cInputData.getObjectByObjectName("LIPlaceChangeTraceSet",0);
		    this.codeno=(String) cInputData.getObjectByObjectName("String",0);
		    System.out.println("获取到的职场编号为："+codeno);
		    this.updateMainSql="update LIPlaceRentInfo set State='02',Islock='01',Modifydate='"+PubFun.getCurrentDate()+"',Modifytime='"+PubFun.getCurrentTime()+"' where placeno='"+codeno+"'";
		    System.out.println("getinputdata===============");
	    	
	    }catch(Exception e){
	    	e.printStackTrace();
		    buildError("prepareData","报错原因：获取业务数据失败！");
		    System.out.println("getInputData==========false");
		    return false;
	    }
	  //获取流水号
		  try {
			  //获取信息流水号
			  String []infoSerialno=PlCreateSerialNO.getSerialNoByInfo(mLIPlaceRentInfoBSet.size());
			  //获取费用流水号
			  String []feeSerialno=PlCreateSerialNO.getSerialNoByFee(mLIPlaceRentFeeBSet.size());
			  //获取变更轨迹流水号
			  String []traceSerialno=PlCreateSerialNO.getSerialNoByTrace(mLIPlaceChangeTraceSet.size());
			  //职场只可能有一条数据
			  mLIPlaceRentInfoBSet.get(1).setSerialno(infoSerialno[0]);
			  mLIPlaceRentInfoBSet.get(1).setStandbystring1(traceSerialno[0]);
			  //职场变更
			  mLIPlaceChangeTraceSet.get(1).setSerialno(traceSerialno[0]);
			  mLIPlaceChangeTraceSet.get(1).setStandbystring1(infoSerialno[0]);

			  for (int i = 1; i <= mLIPlaceRentFeeBSet.size(); i++) {
				  mLIPlaceRentFeeBSet.get(i).setSerialno(feeSerialno[i-1]);
				  mLIPlaceRentFeeBSet.get(i).setStandbystring1(traceSerialno[0]);
			}	
			} catch (Exception e) {
			      e.printStackTrace();
			      buildError("prepareData","报错原因：生成流水号失败！");
			      System.out.println("getInputData==========false");
			      return false;
			}	  
		System.out.println("getinputdata===============");
	    return true;
	  }

	  private boolean prepareOutputData()
	  {
	    try
	    {
	    	MMap map=new MMap();
	    	map.put(mLIPlaceRentInfoBSet,"INSERT");
	    	map.put(mLIPlaceRentFeeBSet,"INSERT");
	    	map.put(mLIPlaceChangeTraceSet,"INSERT");
	    	map.put(updateMainSql, "UPDATE");
	    	mResult.add(map);
	    	System.out.println("prepareOutputData==========");
	    }
	    catch (Exception ex)
	    {
	      // @@错误处理
	      ex.printStackTrace();
	      buildError("prepareOutputData", "执行错误，原因是："+ex.toString());
	      System.out.println("prepareOutputData==========false");
	      return false;
	    }

	    return true;
	  }

	  public VData getResult()
	  {
	    return this.mResult;
	  }
}
