/*
* <p>ClassName: PlInUI </p>
* <p>Description: PlInUI类文件 </p>
* <p>Copyright: Copyright (c) 2002</p>
* <p>Company: sinosoft </p>
* @Database: 职场管理
* @CreateDate：2012-06-11
 */
package com.sinosoft.lis.wpleace;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class PlInUI {
	 /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mDay[] = null;

    public PlInUI()
    {}
    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("ADD"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }

            // 进行业务处理
            if (!dealData())
            {
                return false;
            }
            //准备往后台传输的数据
            if (!prepareOutputData(cInputData))
            {
                return false;
            }

            PlInBL mPlInBL = new PlInBL(); 
            System.out.println("Start PlInUI UI Submit ...");

            if (!mPlInBL.submitData(cInputData, cOperate))
            {
                if (mPlInBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(mPlInBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData","mPlInBL，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = mPlInBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("submitData",e.toString());
            return false;
        }
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "PlInUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}