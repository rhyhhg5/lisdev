package com.sinosoft.lis.wpleace;

import java.io.File;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.PlaceFeeImportSchema;
import com.sinosoft.lis.vschema.PlaceFeeImportSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PlFeeUploadBL {

	  /** 错误处理类，每个需要错误处理的类中都放置该类 */
	  public CErrors mErrors = new CErrors();
	  private VData mResult = new VData();

	  /** 往后面传输数据的容器 */
	  private VData mInputData;

	  /** 全局数据 */
	  private GlobalInput mGlobalInput = new GlobalInput();

	  /** 数据操作字符串 */
	  private String mOperate;

	  /** 业务处理相关变量 */
	  /*private String placeNo;
	  private PlaceFeeImportSet  tPlaceFeeImportSet=new PlaceFeeImportSet();*/

	  public PlFeeUploadBL(GlobalInput tg)
	  {
		 
		  mGlobalInput=tg;
	  }

	  private void buildError(String szFunc, String szErrMsg) {
	     CError cError = new CError();
	     cError.moduleName = "PlFeeUploadBL";
	     cError.functionName = szFunc;
	     cError.errorMessage = szErrMsg;
	     this.mErrors.addOneError(cError);
	  }


	  /**
	   * 传输数据的公共方法
	   * @param: cInputData 输入的数据
	   *         cOperate 数据操作
	   * @return:
	   */
	  public boolean doUp(String path,String fileName)
	  {
		  System.out.println("文件路径及名称："+path+fileName);
		  return true;
	  }
	  /**
	   * 从输入数据中得到所有对象
	   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	   */
	 public VData getResult()
	  {
	    return this.mResult;
	  }
}
