/*
* <p>ClassName: PlChangeBL </p>
* <p>Description: PlChangeBL类文件 </p>
* <p>Copyright: Copyright (c) 2002</p>
* <p>Company: sinosoft </p>
* @Database: 职场管理
* @CreateDate：2012-06-11
 */
package com.sinosoft.lis.wpleace;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class PlChangeBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors = new CErrors();
  private VData mResult = new VData();

  /** 往后面传输数据的容器 */
  private VData mInputData;

  /** 全局数据 */
  private GlobalInput mGlobalInput = new GlobalInput();


  /** 业务处理相关变量 */

  //private String mUpdateLAAgentSQL = "";        //更新考核清退的人的状态为离职
  private LIPlaceRentInfoBSet mLIPlaceRentInfoBSet = new LIPlaceRentInfoBSet();
  private LIPlaceRentFeeBSet mLIPlaceRentFeeBSet = new LIPlaceRentFeeBSet();
  private LIPlaceChangeTraceSet mLIPlaceChangeTraceSet=new LIPlaceChangeTraceSet();
//  private LIPlaceChangeInfoSet mLIPlaceChangeInfoSet = new LIPlaceChangeInfoSet();
  
  private String codeno="";
  private String sql="";

  public PlChangeBL()
  {
  }

  private void buildError(String szFunc, String szErrMsg) {
     CError cError = new CError();
     cError.moduleName = "PlChangeBL";
     cError.functionName = szFunc;
     cError.errorMessage = szErrMsg;
     this.mErrors.addOneError(cError);
  }


  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData, String cOperate)
  {
	  if (!cOperate.equals("CHANGE"))
      {
          buildError("submitData", "不支持的操作字符串");
          return false;
      }
     if (!getInputData(cInputData)) {
        return false;
     }
     if (!checkData()) {
        return false;
     }
    //进行业务处理
    if (!dealData())
    {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
    {
      return false;
    }
    PubSubmit ps = new PubSubmit();
    System.out.println("submitdata===============");
    if (!ps.submitData(mResult, null)) {
        this.mErrors.copyAllErrors(ps.mErrors);
        return false;
    }
    System.out.println("wpleacebl======submitdata");
    return true;
  }
 private boolean checkData()
 {
    return true;

 }


  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData()
  {
	  return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
	  try {
		this.mLIPlaceRentInfoBSet=(LIPlaceRentInfoBSet) cInputData.getObjectByObjectName("LIPlaceRentInfoBSet",0);
		this.mLIPlaceRentFeeBSet=(LIPlaceRentFeeBSet)cInputData.getObjectByObjectName("LIPlaceRentFeeBSet",0);
	    this.mLIPlaceChangeTraceSet=(LIPlaceChangeTraceSet)cInputData.getObjectByObjectName("LIPlaceChangeTraceSet",0);
	    this.mGlobalInput=(GlobalInput) cInputData.getObjectByObjectName("GlobalInput",0);
	    this.codeno=(String) cInputData.getObjectByObjectName("String",0);
	    
	    LIPlaceChangeTraceSchema s=mLIPlaceChangeTraceSet.get(1);
	    System.out.println("s.getPlacenorefer()===="+s.getPlacenorefer());
		this.sql="update LIPlaceRentInfo set State='02',Modifydate='"+s.getModifyDate()+"',Modifytime='"+s.getModifyTime()+"' where Placeno='"+s.getPlacenorefer()+"'";
	    System.out.println("getinputdata===============");
	} catch (Exception e) {
		 // @@错误处理
        e.printStackTrace();
        buildError("prepareOutputData", "报错信息："+e.toString());
        System.out.println("getInputData==========false");
      return false;
	}
		
	  //获取流水号
	  try {
		  //获取信息流水号
		  String []infoSerialno=PlCreateSerialNO.getSerialNoByInfo(mLIPlaceRentInfoBSet.size());
		  //获取费用流水号
		  String []feeSerialno=PlCreateSerialNO.getSerialNoByFee(mLIPlaceRentFeeBSet.size());
		  //获取变更轨迹流水号
		  String []traceSerialno=PlCreateSerialNO.getSerialNoByTrace(mLIPlaceChangeTraceSet.size());
		  //职场只可能有一条数据
		  mLIPlaceRentInfoBSet.get(1).setSerialno(infoSerialno[0]);
		  mLIPlaceRentInfoBSet.get(1).setStandbystring1(traceSerialno[0]);
		  //职场变更
		  mLIPlaceChangeTraceSet.get(1).setSerialno(traceSerialno[0]);
		  mLIPlaceChangeTraceSet.get(1).setStandbystring1(infoSerialno[0]);
		  
		  for (int i = 1; i <= mLIPlaceRentFeeBSet.size(); i++) {
			  mLIPlaceRentFeeBSet.get(i).setSerialno(feeSerialno[i-1]);
			  mLIPlaceRentFeeBSet.get(i).setStandbystring1(traceSerialno[0]);
		}	
		} catch (Exception e) {
		      e.printStackTrace();
		      buildError("prepareData","报错原因：生成流水号失败！");
		      System.out.println("getInputData==========false");
		      return false;
		}
		  
		  
		  System.out.println("getinputdata===============");

    return true;
  }

  private boolean prepareOutputData()
  {
    try
    {
    	MMap map=new MMap();
    	String delSQL="delete from PlaceFeeImport where placeno='"+mLIPlaceChangeTraceSet.get(1).getPlaceno()+"'";
    	map.put(mLIPlaceRentInfoBSet,"INSERT");
    	map.put(mLIPlaceRentFeeBSet,"INSERT");
    	map.put(mLIPlaceChangeTraceSet,"INSERT");
    	map.put(sql,"UPDATE");
    	map.put(delSQL,"DELETE");
    	mResult.add(map);
    	System.out.println("prepareOutputData==========");
    }
    catch (Exception ex)
    {
      // @@错误处理
      ex.printStackTrace();
      buildError("prepareOutputData", "报错信息："+ex.toString());
      System.out.println("prepareOutputData==========false");
      return false;
    }

    return true;
  }

  public VData getResult()
  {
    return this.mResult;
  }
}