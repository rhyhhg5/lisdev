package com.sinosoft.lis.wpleace;

import utils.system;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LIPlaceChangeTraceSchema;
import com.sinosoft.lis.schema.LIPlaceRentFeeBSchema;
import com.sinosoft.lis.schema.LIPlaceRentFeeSchema;
import com.sinosoft.lis.schema.LIPlaceRentInfoBSchema;
import com.sinosoft.lis.schema.LIPlaceRentInfoSchema;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.LIPlaceChangeInfoSet;
import com.sinosoft.lis.vschema.LIPlaceChangeTraceSet;
import com.sinosoft.lis.vschema.LIPlaceEndInfoSet;
import com.sinosoft.lis.vschema.LIPlaceNextInfoSet;
import com.sinosoft.lis.vschema.LIPlaceRentFeeBSet;
import com.sinosoft.lis.vschema.LIPlaceRentFeeSet;
import com.sinosoft.lis.vschema.LIPlaceRentInfoBSet;
import com.sinosoft.lis.vschema.LIPlaceRentInfoSet;
//import com.sinosoft.lis.vschema.LIPliceRentInfoSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PlApprovalBL 
{


	  /** 错误处理类，每个需要错误处理的类中都放置该类 */
	  public CErrors mErrors = new CErrors();
	  private VData mResult = new VData();

	  /** 往后面传输数据的容器 */
	  private VData mInputData;

	  /** 全局数据 */
	  private GlobalInput mGlobalInput = new GlobalInput();

	  /** 数据操作字符串 */
	  private String mOperate;

	  /** 业务处理相关变量 */

	  //private String mUpdateLAAgentSQL = "";        //更新考核清退的人的状态为离职
	  	//租赁信息类 
	  	private LIPlaceRentInfoSchema mLIPlaceRentInfoSchema = new LIPlaceRentInfoSchema();
	  	private LIPlaceRentInfoSet mLIPlaceRentInfoSet = new LIPlaceRentInfoSet();
	  	LIPlaceRentInfoDB mLIPlaceRentInfoDB=new LIPlaceRentInfoDB(); 
	  
	  	//租赁信息备份类
	  	private LIPlaceRentInfoBSchema mLIPlaceRentInfoBSchema = new LIPlaceRentInfoBSchema();
	  	private LIPlaceRentInfoBSet mLIPlaceRentInfoBSet = new LIPlaceRentInfoBSet();
	  	private LIPlaceRentInfoBSet mLIPlaceRentInfoBSet1 = new LIPlaceRentInfoBSet();
	  	LIPlaceRentInfoBDB mLIPlaceRentInfoBDB=new LIPlaceRentInfoBDB();
	  	
	  	private LIPlaceRentInfoBSet mLIPlaceRentInfoBSetN = new LIPlaceRentInfoBSet(); //审批不通过使用
	  	private LIPlaceRentInfoBSchema mLIPlaceRentInfoBSchemaN = new LIPlaceRentInfoBSchema();  //审批不通过使用
	  	
	  	//租期内租金费用类
	  	private LIPlaceRentFeeSchema mLIPlaceRentFeeSchema1;
	  	private LIPlaceRentFeeSchema mLIPlaceRentFeeSchema2;
	  	private LIPlaceRentFeeSchema mLIPlaceRentFeeSchema3;
	  	private LIPlaceRentFeeSet mLIPlaceRentFeeSet=new LIPlaceRentFeeSet();
	  	private LIPlaceRentFeeSet mLIPlaceRentFeeSet3 = new LIPlaceRentFeeSet();
	  	LIPlaceRentFeeDB mLIPlaceRentFeeDB1=new LIPlaceRentFeeDB();
	  	LIPlaceRentFeeDB mLIPlaceRentFeeDB2=new LIPlaceRentFeeDB();
	  	LIPlaceRentFeeDB mLIPlaceRentFeeDB3=new LIPlaceRentFeeDB();
	  	LIPlaceRentFeeDB mLIPlaceRentFeeDB=new LIPlaceRentFeeDB();
	  	//租期内租金费用备份类
	  	private LIPlaceRentFeeBSchema mLIPlaceRentFeeBSchema= new LIPlaceRentFeeBSchema();
	  	private LIPlaceRentFeeBSet mLIPlaceRentFeeBSet=new LIPlaceRentFeeBSet();
	  	private LIPlaceRentFeeBSet mLIPlaceRentFeeBSet1=new LIPlaceRentFeeBSet();
	  	
	  	private LIPlaceRentFeeBSchema mLIPlaceRentFeeBSchemaN;  //审批不通过使用
	  	private LIPlaceRentFeeBSet mLIPlaceRentFeeBSetN=new LIPlaceRentFeeBSet();  //审批不通过使用
	  	
	  	private LIPlaceRentFeeBSchema mLIPlaceRentFeeBSchema1;
	  	private LIPlaceRentFeeBSchema mLIPlaceRentFeeBSchema2;
	  	private LIPlaceRentFeeBSchema mLIPlaceRentFeeBSchema3;
	  	LIPlaceRentFeeBDB mLIPlaceRentFeeBDB1=new LIPlaceRentFeeBDB();
	  	LIPlaceRentFeeBDB mLIPlaceRentFeeBDB2=new LIPlaceRentFeeBDB();
	  	LIPlaceRentFeeBDB mLIPlaceRentFeeBDB3=new LIPlaceRentFeeBDB();
		
		//轨迹变更类
		private LIPlaceChangeTraceSet mLIPlaceChangeTraceSet=new LIPlaceChangeTraceSet();
		private LIPlaceChangeTraceSchema mLIPlaceChangeTraceSchema =new LIPlaceChangeTraceSchema();
		LIPlaceChangeTraceDB mLIPlaceChangeTraceDB=new LIPlaceChangeTraceDB();
		
		//职场换租
		private LIPlaceChangeInfoSet mLIPlaceChangeInfoSet = new LIPlaceChangeInfoSet();
		//职场续租
		private LIPlaceNextInfoSet mLIPlaceNextInfoSet = new LIPlaceNextInfoSet();
		//职场撤租
		private LIPlaceEndInfoSet mLIPlaceEndInfoSet =new LIPlaceEndInfoSet();
		private String flag="";

	  public PlApprovalBL()
	  {
	  }

	  private void buildError(String szFunc, String szErrMsg) {
	     CError cError = new CError();
	     cError.moduleName = "PlApprovalBL";
	     cError.functionName = szFunc;
	     cError.errorMessage = szErrMsg;
	     this.mErrors.addOneError(cError);
	  }


	  /**
	   * 传输数据的公共方法
	   * @param: cInputData 输入的数据
	   *         cOperate 数据操作
	   * @return:
	   */
	  public boolean submitData(VData cInputData, String cOperate,String tflag)
	  {
	     mInputData = cInputData;
	     mOperate = cOperate;
	     flag=tflag;
	     if (!getInputData(cInputData)) {
	        return false;
	     }
	     if (!checkData()) {
	        return false;
	     }
	    //进行业务处理
	    if (!dealData())
	    {
	      return false;
	    }
	    //准备往后台的数据
	    if (!prepareOutputData())
	    {
	      return false;
	    }
	    PubSubmit ps = new PubSubmit();
	    if (!ps.submitData(mInputData, null)) {
	    	
	    	if (ps.mErrors.needDealError())
            {
                mErrors.copyAllErrors(ps.mErrors);
                return false;
            }
            else
            {
                buildError("submitData","PubSubmit，但是没有提供详细的出错信息!");
                return false;
            }
	    }
	    System.out.println("PlApprovalBL======submitdata");
	    return true;
	  }
	 private boolean checkData()
	 {
	    return true;

	 }


	  /**
	   * 根据前面的输入数据，进行BL逻辑处理
	   * 如果在处理过程中出错，则返回false,否则返回true
	   */
	  private boolean dealData()
	  {
		  return true;
	  }

	  /**
	   * 从输入数据中得到所有对象
	   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	   */
	  private boolean getInputData(VData cInputData)
	  {
	    this.mLIPlaceChangeTraceSet=(LIPlaceChangeTraceSet) cInputData.getObjectByObjectName("LIPlaceChangeTraceSet",0);
	    this.mLIPlaceRentInfoBSetN=(LIPlaceRentInfoBSet)cInputData.getObjectByObjectName("LIPlaceRentInfoBSet",0);
	    this.mLIPlaceRentFeeBSetN=(LIPlaceRentFeeBSet)cInputData.getObjectByObjectName("LIPlaceRentFeeBSet",0);
		this.mLIPlaceNextInfoSet=(LIPlaceNextInfoSet)cInputData.getObjectByObjectName("LIPlaceNextInfoSet",0);
	    this.mLIPlaceChangeInfoSet=(LIPlaceChangeInfoSet)cInputData.getObjectByObjectName("LIPlaceChangeInfoSet",0);
		this.mLIPlaceEndInfoSet= (LIPlaceEndInfoSet)cInputData.getObjectByObjectName("LIPlaceEndInfoSet",0);
	    this.mGlobalInput=(GlobalInput) cInputData.getObjectByObjectName("GlobalInput",0);
	    System.out.println("getinputdata===============");

	    return true;
	  }

	  private boolean prepareOutputData()
	  {
	      mLIPlaceChangeTraceSchema =mLIPlaceChangeTraceSet.get(1);
		  if ("2".equals(flag) || "3".equals(flag)) {
				if(!dealApproval()){
	                buildError("prepareOutputData","PlApprovalBL,分公司审批或总公司审批操作失败！");
	                return false;
				}
			} else {
				if(!dealApprovalN()){
	                buildError("prepareOutputData","PlApprovalBL,审批不通过操作失败！");
	                return false;
				}
			}
	      return true;
	  }
	  
   //分公司审批和总公司审批
   private boolean dealApproval() {
	   try
	    {
	    	MMap map=new MMap();
	    	//轨迹变更
	    	String CTSerialno = mLIPlaceChangeTraceSchema.getSerialno();//获取职场变更流水号
	    	String Placenorefer=mLIPlaceChangeTraceSchema.getPlacenorefer();//获取关联职场编码
	    	String Changetype=mLIPlaceChangeTraceSchema.getChangetype();//获取变更类型
	    	String Changestate=mLIPlaceChangeTraceSchema.getChangestate();//获取审批状态
	    	String Placeno=mLIPlaceChangeTraceSchema.getPlaceno();//获取职场号码
	    	String Fcheckdate=mLIPlaceChangeTraceSchema.getFcheckdate();//获取分公司审批日期
	    	String ModifyDate=mLIPlaceChangeTraceSchema.getModifyDate();//获取修改日期
	    	String ModifyTime=mLIPlaceChangeTraceSchema.getModifyTime();//获取修改时间
	    	String Fremark=mLIPlaceChangeTraceSchema.getFremark();//分公司审批不通过，须添加原因
	    	String Zcheckdate=mLIPlaceChangeTraceSchema.getZcheckdate();//获取总公司审批日期
	    	String Zremark=mLIPlaceChangeTraceSchema.getZremark();//获取总公司审批不确认备注信息
	    	String Enddate=mLIPlaceChangeTraceSchema.getEnddate();//获取职场租赁实际停用信息
	    	
	    	System.out.println("flag ====" +flag);
	    	System.out.println("Changetype======"+Changetype);
	    	System.out.println("Changestate ====" +Changestate);
	    	System.out.println("变更轨迹流水号CTSerialno："+CTSerialno);
	    	
   		//更新审批轨迹表sql---审批不通过
   		String lctraceF="update LIPlaceChangeTrace set Changestate='"+Changestate+"',Fcheckdate='"+Fcheckdate+"',ModifyDate='"+ModifyDate+"'," +
           "ModifyTime='"+ModifyTime+"',Fremark='"+Fremark+"' where serialno='"+CTSerialno+"'";
   		String lctraceZ="update LIPlaceChangeTrace set Changestate='"+Changestate+"',Zcheckdate='"+Zcheckdate+"',ModifyDate='"+ModifyDate+"'," +
           "ModifyTime='"+ModifyTime+"',Zremark='"+Zremark+"' where serialno='"+CTSerialno+"'";
	    	
	    	//--------审批不通过处理sql逻辑  begin
   		
   		//更新职场租赁信息备份表状态
	        String lrinfo="update LIPlaceRentInfoB set Dealstate='03',Modifydate='"+ModifyDate+"',Modifytime='"+ModifyTime+"' where standbystring1='"+CTSerialno+"'";
	        //更新租期内租金费用备份表状态
	        String lrfee="update LIPlaceRentFeeB set Dealstate='03',Modifydate='"+ModifyDate+"',Modifytime='"+ModifyTime+"' where Standbystring1='"+CTSerialno+"'";
	        //职场租赁信息表   撤租/装修费补录
	        String tSql="update LIPlaceRentInfo set State='01',Modifydate='"+ModifyDate+"',Modifytime='"+ModifyTime+"' where Placeno='"+Placeno+"'";
	        //职场租赁信息表  职场修改
	        String tSql1="update LIPlaceRentInfo set State='01',Islock='02',Modifydate='"+ModifyDate+"',Modifytime='"+ModifyTime+"' where Placeno='"+Placeno+"'";
	        //职场租赁信息表  续租/换租
	        String tSql2="update LIPlaceRentInfo set State='01',Modifydate='"+ModifyDate+"',Modifytime='"+ModifyTime+"' where Placeno='"+Placenorefer+"'";
	        
	        if (Changestate=="04") {
	        	
	        	if("2".equals(flag))
		    	{   
					map.put(lctraceF,"UPDATE");
		    	}else{
			        map.put(lctraceZ,"UPDATE");
		    	}
   			System.out.println("---审批不通过---");
   			if("01".equals(Changetype)){//新增职场
	    			map.put(lrinfo,"UPDATE");
	    			map.put(lrfee,"UPDATE");
	    		}
		    	
		    	if("02".equals(Changetype) || "03".equals(Changetype)){//续租和换租
		    		map.put(lrinfo, "UPDATE");
		    		map.put(lrfee,"UPDATE");
		    		map.put(tSql2,"UPDATE");
		    	}
		    	if("04".equals(Changetype) ){//撤租
		    		map.put(tSql, "UPDATE");
		    	}
		    	if("05".equals(Changetype)){//职场修改
		    		map.put(lrinfo, "UPDATE");
		    		map.put(lrfee,"UPDATE");
		    		map.put(tSql1,"UPDATE");
		    	}
		    	if("06".equals(Changetype)){//装修费补录
		    		map.put(tSql, "UPDATE");
		    		map.put(lrfee, "UPDATE");
		    	}	
			}
	        
	    	//--------审批不通过处理sql逻辑  end
	        
	        //--------审批通过处理sql逻辑  begin
   		//更新审批轨迹表sql---审批通过
   		String lmtraceF="update LIPlaceChangeTrace set Changestate='"+Changestate+"',Fcheckdate='"+Fcheckdate+"',ModifyDate='"+ModifyDate+"'," +
           "ModifyTime='"+ModifyTime+"' where serialno='"+CTSerialno+"'";
   		String lmtraceZ="update LIPlaceChangeTrace set Changestate='"+Changestate+"',Zcheckdate='"+Zcheckdate+"',ModifyDate='"+ModifyDate+"'," +
           "ModifyTime='"+ModifyTime+"' where serialno='"+CTSerialno+"'";
	        if (Changestate!="04") {
	        	System.out.println("审批通过！");
	        	//分公司审批
		    	if("2".equals(flag))
		    	{   
					System.out.println("分公司审批-》审批通过");
					map.put(lmtraceF,"UPDATE");
		    	}
		    	//总公司审批
		    	if ("3".equals(flag)) {
		    		System.out.println("总公司审批-》审批通过");
		    		map.put(lmtraceZ,"UPDATE");  //更新审批轨迹表
		    		//新增职场/职场续租/职场换租
		    		if("01".equals(Changetype) || "02".equals(Changetype) || "03".equals(Changetype)){
		    			System.out.println("------->新增职场/职场续租/职场换租");
			    		//把备份表数据复制到主表
			    		System.out.println("===开始从备份表向主表中复制数据！===");
				    	mLIPlaceRentInfoSet=CopyToMainTable(CTSerialno);//职场租赁信息
				    	mLIPlaceRentFeeSet=CopyToMainTableFee(CTSerialno);//租期内费用信息 	
			    	
				    	//删除租赁备份表信息
			    		String delinfoB=" delete from LIPlaceRentInfoB where standbystring1='"+CTSerialno+"'";
			    		String delfeeB=" delete from LIPlaceRentFeeB where standbystring1='"+CTSerialno+"'";
			    		
			    		//续租更新主表信息
			    		String upinfo02="update LIPlaceRentInfo set State='01',Islock = '02',Modifydate='"+ModifyDate+"',Modifytime='"+ModifyTime+"' where Placeno='"+Placenorefer+"'";
			    		//换租更新主表信息
			    		String upinfo03="update LIPlaceRentInfo set State='01',Islock = '02',Modifydate='"+ModifyDate+"',Modifytime='"+ModifyTime+"',Actualenddate='"+Enddate+"' where Placeno='"+Placenorefer+"'";
			    		
			    		map.put(delinfoB,"UPDATE"); 
			    		map.put(delfeeB,"UPDATE");  
			    		map.put(mLIPlaceRentInfoSet, "INSERT");
			    		map.put(mLIPlaceRentFeeSet, "INSERT");
			    		if("02".equals(Changetype)){//职场续租
				    		map.put(mLIPlaceNextInfoSet, "INSERT");//向职场续租信息表中插入数据
				    		map.put(upinfo02,"UPDATE");
				    	}
				    	if("03".equals(Changetype)){//职场换租
				    		map.put(mLIPlaceChangeInfoSet, "INSERT");//向职场换租信息表中插入数据
				    		map.put(upinfo03,"UPDATE");
				    	}
			    	}
		    		if("04".equals(Changetype)){//职场撤租
		    			System.out.println("------->职场撤租");
			    		String upinfo04="update LIPlaceRentInfo set State='00',Islock = '02',Modifydate='"+ModifyDate+"',Modifytime='"+ModifyTime+"',Actualenddate='"+Enddate+"' where Placeno='"+Placeno+"'";
			    		map.put(mLIPlaceEndInfoSet, "INSERT");//向撤租信息表中插入数据
			    		map.put(upinfo04, "UPDATE");//更新主表状态为停用
			    	}
		    		if("05".equals(Changetype)){//职场修改
		    			System.out.println("------->职场修改");
			    		//先把主表信息插入到备份表（有流水号作为区别），然后把备份表信息更新到主表
			    		System.out.println("===主表信息插入到备份表开始===");
			    		mLIPlaceRentInfoBSet = CopyToBTable(Placeno,Changetype);
			    		System.out.println("(主->备)获取到的备份表的流水号为："+mLIPlaceRentInfoBSet.get(1).getSerialno());
			    		mLIPlaceRentFeeBSet1 = CopyToBTableFee(Placeno,"BF",Changetype);
			    		System.out.println("===插入到备份表的主表信息获取完成===把备份表信息更新到主表开始===");
			    		
			    		mLIPlaceRentInfoSet = UpdateToMainTable(CTSerialno);
			    		mLIPlaceRentFeeSet = UpdateToMainTableFee(CTSerialno);
			    		System.out.println("===更新到主表的备份表信息获取完成===");
			    		//删除主表中的数据
			    		String delinfo05="delete from LIPlaceRentFee where Placeno='"+Placeno+"'";
			    		String delfee05="delete from LIPlaceRentInfo where Placeno='"+Placeno+"'";	
			    		//将备份表中与主表中重复的那条数据删除
			    		String delinfoB05="delete from LIPlaceRentInfoB where standbystring1='"+CTSerialno+"'";
			    		String delfeeB05="delete from LIPlaceRentFeeB where Standbystring1='"+CTSerialno+"'";
			    		
			    		map.put(mLIPlaceRentInfoBSet, "INSERT");
			    		map.put(mLIPlaceRentFeeBSet1, "INSERT");
			    		map.put(delinfo05, "DELETE");
			    		map.put(delfee05, "DELETE");
			    		map.put(mLIPlaceRentInfoSet, "INSERT");
			    		map.put(mLIPlaceRentFeeSet, "INSERT");
			    		map.put(delinfoB05, "DELETE");
			    		map.put(delfeeB05, "DELETE");
			    		
			    	}
		    		if("06".equals(Changetype)){//装修费补录
		    			System.out.println("------->装修费补录");
			    		//将费用备份表中的该条信息的流水号通过CTStandbystring1传递到方法中
			    		System.out.println("=====装修费补录=====");
			    		mLIPlaceRentFeeSet3 = CopyToMainTableDecFee(CTSerialno,Placeno);
			    		//
			    		String upinfo06="update LIPlaceRentInfo set State='01',Islock = '02',Modifydate='"+ModifyDate+"',Modifytime='"+ModifyTime+"' where Placeno='"+Placeno+"'";
			    		String delfeeB06 = "delete from LIPlaceRentFeeB where standbystring1='"+CTSerialno+"'";
			    		String delfee06="delete from LIPlaceRentFee where placeno='"+Placeno+"' and feetype='03'";
			    		
			    		map.put(delfee06, "DELETE");//删除主表费用
			    		map.put(mLIPlaceRentFeeSet3, "INSERT");//把费用备份表中的信息插入到费用主表
			    		map.put(upinfo06, "UPDATE");//更新租赁信息表的状态
			    		map.put(delfeeB06, "DELETE");//删除费用备份表中的对应数据
			    	}	
			    	
				}
			}
	        
	      //----------审批通过处理sql逻辑 end
	    		   	
	    	mInputData.add(map);
	    	System.out.println("dealApproval==========");
	    }
	    catch (Exception ex)
	    {
	      // @@错误处理
	      ex.printStackTrace();
	      buildError("dealApproval", "PlApprovalBL,在准备往后层处理所需要的数据时出错!");
	      return false;
	    }
	    return true;
}
   
   //审批不通过
   private boolean dealApprovalN() {
	  try {
	    	MMap map=new MMap();
	    	String CTSerialno = mLIPlaceChangeTraceSchema.getSerialno();//获取职场变更流水号
	    	String Changetype=mLIPlaceChangeTraceSchema.getChangetype();//获取变更类型
	    	String Enddate=mLIPlaceChangeTraceSchema.getEnddate();
	    	double Shouldgetfee=mLIPlaceChangeTraceSchema.getShouldgetfee();
	    	String Shouldgetdate=mLIPlaceChangeTraceSchema.getShouldgetdate();
	    	double Apenaltyfee=mLIPlaceChangeTraceSchema.getApenaltyfee();
	    	String Apenaltyfeegetdate=mLIPlaceChangeTraceSchema.getApenaltyfeegetdate();
	    	double Marginfee=mLIPlaceChangeTraceSchema.getMarginfee();
	    	String Marginfeegetdate=mLIPlaceChangeTraceSchema.getMarginfeegetdate();
	    	
	    	//获取流水号（新增/续租/换租/修改）
	    	if ("01".equals(Changetype) || "05".equals(Changetype) || "02".equals(Changetype) || "03".equals(Changetype)) {
	    		try {
	    			  //获取信息流水号
	    			  String []infoSerialno=PlCreateSerialNO.getSerialNoByInfo(mLIPlaceRentInfoBSetN.size());
	    			  //获取费用流水号
	    			  String []feeSerialno=PlCreateSerialNO.getSerialNoByFee(mLIPlaceRentFeeBSetN.size());
	    			  //职场只可能有一条数据
	    			  mLIPlaceRentInfoBSetN.get(1).setSerialno(infoSerialno[0]);

	    			  for (int i = 1; i <= mLIPlaceRentFeeBSetN.size(); i++) {
	    				  mLIPlaceRentFeeBSetN.get(i).setSerialno(feeSerialno[i-1]);
	    			}	
	    			} catch (Exception e) {
	    			      e.printStackTrace();
	    			      buildError("prepareData","报错原因：生成流水号失败！");
	    			      System.out.println("getInputData==========false");
	    			      return false;
	    			}
			}
	    	//获取流水号（装修费补录）
	    	if ("06".equals(Changetype)) {
	    		try {
	    			//获取费用流水号
	    			String []feeSerialno=PlCreateSerialNO.getSerialNoByFee(mLIPlaceRentFeeBSetN.size());
	    			for (int i = 1; i <= mLIPlaceRentFeeBSetN.size(); i++) {
	    				  mLIPlaceRentFeeBSetN.get(i).setSerialno(feeSerialno[i-1]);
	    			}	
	    			} catch (Exception e) {
	    			      e.printStackTrace();
	    			      buildError("prepareData","报错原因：生成流水号失败！");
	    			      System.out.println("getInputData==========false");
	    			      return false;
	    			}
			}
            //新增、修改
	    	if ("01".equals(Changetype) || "05".equals(Changetype)) {
		    	String uptrace="update LIPlaceChangeTrace set Changestate='01',Fcheckdate=null,Fremark=null,Zcheckdate=null,Zremark=null,modifydate='"+PubFun.getCurrentDate()+"',ModifyTime='"+PubFun.getCurrentTime()+"' where serialno='"+CTSerialno+"'";	
	    		String delinfoB="delete from LIPlaceRentInfoB where standbystring1='"+CTSerialno+"'";
	    		String delfeeB="delete from LIPlaceRentFeeB where standbystring1='"+CTSerialno+"'";
	    		
	    		map.put(delinfoB, "DELETE");//把费用备份表中的信息插入到费用主表
	    		map.put(delfeeB, "DELETE");//把费用备份表中的信息插入到费用主表
	    		map.put(mLIPlaceRentInfoBSetN, "INSERT");
	    		map.put(mLIPlaceRentFeeBSetN, "INSERT");
	    		map.put(uptrace, "UPDATE");
			}
	    	//续租
	    	if ("02".equals(Changetype)) {
		    	String uptrace="update LIPlaceChangeTrace set Changestate='01',Fcheckdate=null,Fremark=null,Zcheckdate=null,Zremark=null,Marginfee="+Marginfee+",Marginfeegetdate='"+Marginfeegetdate+"',modifydate='"+PubFun.getCurrentDate()+"',ModifyTime='"+PubFun.getCurrentTime()+"' where serialno='"+CTSerialno+"'";	
	    		String delinfoB="delete from LIPlaceRentInfoB where standbystring1='"+CTSerialno+"'";
	    		String delfeeB="delete from LIPlaceRentFeeB where standbystring1='"+CTSerialno+"'";
	    		
	    		map.put(delinfoB, "DELETE");//把费用备份表中的信息插入到费用主表
	    		map.put(delfeeB, "DELETE");//把费用备份表中的信息插入到费用主表
	    		map.put(mLIPlaceRentInfoBSetN, "INSERT");
	    		map.put(mLIPlaceRentFeeBSetN, "INSERT");
	    		map.put(uptrace, "UPDATE");
			}
	    	//续租
	    	if ("03".equals(Changetype)) {
		    	String uptrace="update LIPlaceChangeTrace set Changestate='01',Fcheckdate=null,Fremark=null,Zcheckdate=null,Zremark=null,Enddate='"+Enddate+"',Shouldgetfee="+Shouldgetfee+",Shouldgetdate='"+Shouldgetdate+"',Apenaltyfee="+Apenaltyfee+",Apenaltyfeegetdate='"+Apenaltyfeegetdate+"',Marginfee="+Marginfee+",Marginfeegetdate='"+Marginfeegetdate+"',modifydate='"+PubFun.getCurrentDate()+"',ModifyTime='"+PubFun.getCurrentTime()+"' where serialno='"+CTSerialno+"'";	
	    		String delinfoB="delete from LIPlaceRentInfoB where standbystring1='"+CTSerialno+"'";
	    		String delfeeB="delete from LIPlaceRentFeeB where standbystring1='"+CTSerialno+"'";
	    		
	    		map.put(delinfoB, "DELETE");
	    		map.put(delfeeB, "DELETE");
	    		map.put(mLIPlaceRentInfoBSetN, "INSERT");
	    		map.put(mLIPlaceRentFeeBSetN, "INSERT");
	    		map.put(uptrace, "UPDATE");
			}
	    	//撤租
	    	if ("04".equals(Changetype)) {
		    	String uptrace="update LIPlaceChangeTrace set Changestate='01',Fcheckdate=null,Fremark=null,Zcheckdate=null,Zremark=null,Enddate='"+Enddate+"',Shouldgetfee="+Shouldgetfee+",Shouldgetdate='"+Shouldgetdate+"',Apenaltyfee="+Apenaltyfee+",Apenaltyfeegetdate='"+Apenaltyfeegetdate+"',Marginfee="+Marginfee+",Marginfeegetdate='"+Marginfeegetdate+"',modifydate='"+PubFun.getCurrentDate()+"',ModifyTime='"+PubFun.getCurrentTime()+"' where serialno='"+CTSerialno+"'";	

	    		map.put(uptrace, "UPDATE");
			}
	    	//装修费补录
	    	if ("06".equals(Changetype)) {
		    	String uptrace="update LIPlaceChangeTrace set Changestate='01',Fcheckdate=null,Fremark=null,Zcheckdate=null,Zremark=null,modifydate='"+PubFun.getCurrentDate()+"',ModifyTime='"+PubFun.getCurrentTime()+"' where serialno='"+CTSerialno+"'";	
	    		String delfeeB="delete from LIPlaceRentFeeB where standbystring1='"+CTSerialno+"'";
	    		
		    	map.put(delfeeB, "DELETE");//把费用备份表中的信息插入到费用主表
		    	map.put(mLIPlaceRentFeeBSetN, "INSERT");
	    		map.put(uptrace, "UPDATE");
			}
	    	mInputData.add(map);
	    	System.out.println("dealApprovalN==========");
		
	} catch (Exception em) {
	      em.printStackTrace();
	      buildError("dealApprovalN", "PlApprovalBL,在准备往后层处理所需要的数据时出错!"+em.toString());
	      return false;
	}    
	  return true;
}
	
	//装修费补录
	private LIPlaceRentFeeSet CopyToMainTableDecFee(String Standbystring1,String placeno) {
		
		mLIPlaceRentFeeBDB3.setFeetype("03");
		mLIPlaceRentFeeBDB3.setDealstate("01");
		mLIPlaceRentFeeBDB3.setDealtype("06");
		mLIPlaceRentFeeBDB3.setStandbystring1(Standbystring1);
		LIPlaceRentFeeBSet mLIPlaceRentFeeBSet3 = mLIPlaceRentFeeBDB3.query();

		System.out.println("===mLIPlaceRentFeeBSet3.size()==="+mLIPlaceRentFeeBSet3.size());

		//从主表信息order的值的下一个开始插入
		for(int i=1; i<=mLIPlaceRentFeeBSet3.size(); i++ ){
			//此处Schema类必须在每一次循环的时更新一下，否则循环执行过后最后一条信息会覆盖之前的数据
			mLIPlaceRentFeeSchema3= new LIPlaceRentFeeSchema();
			mLIPlaceRentFeeSchema3.setPlaceno(mLIPlaceRentFeeBSet3.get(i).getPlaceno());
			mLIPlaceRentFeeSchema3.setFeetype(mLIPlaceRentFeeBSet3.get(i).getFeetype());
			mLIPlaceRentFeeSchema3.setOrder(String.valueOf(i));
			mLIPlaceRentFeeSchema3.setPayDate(mLIPlaceRentFeeBSet3.get(i).getPayDate());
			mLIPlaceRentFeeSchema3.setMoney(mLIPlaceRentFeeBSet3.get(i).getMoney());
			mLIPlaceRentFeeSchema3.setPaybegindate(mLIPlaceRentFeeBSet3.get(i).getPaybegindate());
			mLIPlaceRentFeeSchema3.setPayenddate(mLIPlaceRentFeeBSet3.get(i).getPayenddate());
			mLIPlaceRentFeeSchema3.setOperator(mLIPlaceRentFeeBSet3.get(i).getOperator());
			mLIPlaceRentFeeSchema3.setMakedate(PubFun.getCurrentDate());
			mLIPlaceRentFeeSchema3.setMaketime(PubFun.getCurrentTime());
			mLIPlaceRentFeeSchema3.setModifyDate(PubFun.getCurrentDate());
			mLIPlaceRentFeeSchema3.setModifyTime(PubFun.getCurrentTime());
			//mLIPlaceRentFeeSchema3.setStandbystring1(tCTStandbystring1);
			mLIPlaceRentFeeSet3.add(mLIPlaceRentFeeSchema3);
		}
		return mLIPlaceRentFeeSet3;
	}

	private LIPlaceRentFeeSet UpdateToMainTableFee(String standbystring1) {
		// TODO Auto-generated method stub
		//获取租金信息条数
		mLIPlaceRentFeeBDB1.setFeetype("01");
		mLIPlaceRentFeeBDB1.setDealtype("05");
		mLIPlaceRentFeeBDB1.setStandbystring1(standbystring1);
		LIPlaceRentFeeBSet mLIPlaceRentFeeBSet1 = mLIPlaceRentFeeBDB1.query();
		//获取物业信息条数
		mLIPlaceRentFeeBDB2.setFeetype("02");
		mLIPlaceRentFeeBDB2.setDealtype("05");
		mLIPlaceRentFeeBDB2.setStandbystring1(standbystring1);
		LIPlaceRentFeeBSet mLIPlaceRentFeeBSet2 = mLIPlaceRentFeeBDB2.query();
		//获取装修信息条数
		mLIPlaceRentFeeBDB3.setFeetype("03");
		mLIPlaceRentFeeBDB3.setDealtype("05");
		mLIPlaceRentFeeBDB3.setStandbystring1(standbystring1);
		LIPlaceRentFeeBSet mLIPlaceRentFeeBSet3 = mLIPlaceRentFeeBDB3.query();
		
		System.out.println("获取租金信息条数mLIPlaceRentFeeBSet1====="+mLIPlaceRentFeeBSet1.size());
		System.out.println("获取物业信息条数mLIPlaceRentFeeBSet2====="+mLIPlaceRentFeeBSet2.size());
		System.out.println("获取装修信息条数mLIPlaceRentFeeBSet3====="+mLIPlaceRentFeeBSet3.size());
		
		//向费用表中添加租金信息
		for(int i=1; i<=mLIPlaceRentFeeBSet1.size(); i++ ){
			//此处Schema类必须在每一次循环的时更新一下，否则循环执行过后最后一条信息会覆盖之前的数据
			mLIPlaceRentFeeSchema1= new LIPlaceRentFeeSchema();
			mLIPlaceRentFeeSchema1.setPlaceno(mLIPlaceRentFeeBSet1.get(i).getPlaceno());
			mLIPlaceRentFeeSchema1.setFeetype(mLIPlaceRentFeeBSet1.get(i).getFeetype());
			mLIPlaceRentFeeSchema1.setOrder(mLIPlaceRentFeeBSet1.get(i).getOrder());
			mLIPlaceRentFeeSchema1.setPayDate(mLIPlaceRentFeeBSet1.get(i).getPayDate());
			mLIPlaceRentFeeSchema1.setMoney(mLIPlaceRentFeeBSet1.get(i).getMoney());
			mLIPlaceRentFeeSchema1.setPaybegindate(mLIPlaceRentFeeBSet1.get(i).getPaybegindate());
			mLIPlaceRentFeeSchema1.setPayenddate(mLIPlaceRentFeeBSet1.get(i).getPayenddate());
			mLIPlaceRentFeeSchema1.setOperator(mLIPlaceRentFeeBSet1.get(i).getOperator());
			mLIPlaceRentFeeSchema1.setMakedate(PubFun.getCurrentDate());
			mLIPlaceRentFeeSchema1.setMaketime(PubFun.getCurrentTime());
			mLIPlaceRentFeeSchema1.setModifyDate(PubFun.getCurrentDate());
			mLIPlaceRentFeeSchema1.setModifyTime(PubFun.getCurrentTime());
			System.out.println("序列号Order第"+i+"个====="+mLIPlaceRentFeeBSet1.get(i).getOrder());
			mLIPlaceRentFeeSet.add(mLIPlaceRentFeeSchema1);
		}
		//向费用表中添加物业信息
		for(int i=1; i<=mLIPlaceRentFeeBSet2.size(); i++ ){
			//此处Schema类必须在每一次循环的时更新一下，否则循环执行过后最后一条信息会覆盖之前的数据
			mLIPlaceRentFeeSchema2= new LIPlaceRentFeeSchema();
			mLIPlaceRentFeeSchema2.setPlaceno(mLIPlaceRentFeeBSet2.get(i).getPlaceno());
			mLIPlaceRentFeeSchema2.setFeetype(mLIPlaceRentFeeBSet2.get(i).getFeetype());
			mLIPlaceRentFeeSchema2.setOrder(mLIPlaceRentFeeBSet2.get(i).getOrder());
			mLIPlaceRentFeeSchema2.setPayDate(mLIPlaceRentFeeBSet2.get(i).getPayDate());
			mLIPlaceRentFeeSchema2.setMoney(mLIPlaceRentFeeBSet2.get(i).getMoney());
			mLIPlaceRentFeeSchema2.setPaybegindate(mLIPlaceRentFeeBSet2.get(i).getPaybegindate());
			mLIPlaceRentFeeSchema2.setPayenddate(mLIPlaceRentFeeBSet2.get(i).getPayenddate());
			mLIPlaceRentFeeSchema2.setOperator(mLIPlaceRentFeeBSet2.get(i).getOperator());
			mLIPlaceRentFeeSchema2.setMakedate(PubFun.getCurrentDate());
			mLIPlaceRentFeeSchema2.setMaketime(PubFun.getCurrentTime());
			mLIPlaceRentFeeSchema2.setModifyDate(PubFun.getCurrentDate());
			mLIPlaceRentFeeSchema2.setModifyTime(PubFun.getCurrentTime());
			System.out.println("序列号Order第"+i+"个====="+mLIPlaceRentFeeBSet2.get(i).getOrder());
			mLIPlaceRentFeeSet.add(mLIPlaceRentFeeSchema2);
		}
		//向费用表中添加装修信息
		for(int i=1; i<=mLIPlaceRentFeeBSet3.size(); i++ ){
			//此处Schema类必须在每一次循环的时更新一下，否则循环执行过后最后一条信息会覆盖之前的数据
			mLIPlaceRentFeeSchema3= new LIPlaceRentFeeSchema();
			mLIPlaceRentFeeSchema3.setPlaceno(mLIPlaceRentFeeBSet3.get(i).getPlaceno());
			mLIPlaceRentFeeSchema3.setFeetype(mLIPlaceRentFeeBSet3.get(i).getFeetype());
			mLIPlaceRentFeeSchema3.setOrder(mLIPlaceRentFeeBSet3.get(i).getOrder());
			mLIPlaceRentFeeSchema3.setPayDate(mLIPlaceRentFeeBSet3.get(i).getPayDate());
			mLIPlaceRentFeeSchema3.setMoney(mLIPlaceRentFeeBSet3.get(i).getMoney());
			mLIPlaceRentFeeSchema3.setPaybegindate(mLIPlaceRentFeeBSet3.get(i).getPaybegindate());
			mLIPlaceRentFeeSchema3.setPayenddate(mLIPlaceRentFeeBSet3.get(i).getPayenddate());
			mLIPlaceRentFeeSchema3.setOperator(mLIPlaceRentFeeBSet3.get(i).getOperator());
			mLIPlaceRentFeeSchema3.setMakedate(PubFun.getCurrentDate());
			mLIPlaceRentFeeSchema3.setMaketime(PubFun.getCurrentTime());
			mLIPlaceRentFeeSchema3.setModifyDate(PubFun.getCurrentDate());
			mLIPlaceRentFeeSchema3.setModifyTime(PubFun.getCurrentTime());
			System.out.println("序列号Order第"+i+"个====="+mLIPlaceRentFeeBSet3.get(i).getOrder());
			mLIPlaceRentFeeSet.add(mLIPlaceRentFeeSchema3);
		}
		return mLIPlaceRentFeeSet;
	}

	private LIPlaceRentInfoSet UpdateToMainTable(String Standbystring1) {
		// TODO Auto-generated method stub
		System.out.println("开始查询。。。。。。。。。。。。。。。。。。。");
		mLIPlaceRentInfoBDB.setStandbystring1(Standbystring1);
		mLIPlaceRentInfoBSchema  = mLIPlaceRentInfoBDB.query().get(1);
		
		mLIPlaceRentInfoSchema.setPlaceno(mLIPlaceRentInfoBSchema.getPlaceno());
		mLIPlaceRentInfoSchema.setManagecom(mLIPlaceRentInfoBSchema.getManagecom());
		mLIPlaceRentInfoSchema.setAppno(mLIPlaceRentInfoBSchema.getAppno());
		mLIPlaceRentInfoSchema.setState("01");//00--停用、01--在用 、02-审批状态
		mLIPlaceRentInfoSchema.setIslock("02"); // 01 非锁定  02 锁定 
		mLIPlaceRentInfoSchema.setComlevel(mLIPlaceRentInfoBSchema.getComlevel());
		mLIPlaceRentInfoSchema.setComname(mLIPlaceRentInfoBSchema.getComname());
		mLIPlaceRentInfoSchema.setBegindate(mLIPlaceRentInfoBSchema.getBegindate());
		mLIPlaceRentInfoSchema.setEnddate(mLIPlaceRentInfoBSchema.getEnddate());
		mLIPlaceRentInfoSchema.setActualenddate(mLIPlaceRentInfoBSchema.getActualenddate());
		mLIPlaceRentInfoSchema.setAddress(mLIPlaceRentInfoBSchema.getAddress());
		mLIPlaceRentInfoSchema.setLessor(mLIPlaceRentInfoBSchema.getLessor());
		mLIPlaceRentInfoSchema.setHouseno(mLIPlaceRentInfoBSchema.getHouseno());
		mLIPlaceRentInfoSchema.setProvince(mLIPlaceRentInfoBSchema.getProvince());
		mLIPlaceRentInfoSchema.setCity(mLIPlaceRentInfoBSchema.getCity());
		mLIPlaceRentInfoSchema.setIsrentregister(mLIPlaceRentInfoBSchema.getIsrentregister());
		mLIPlaceRentInfoSchema.setIslessorassociate(mLIPlaceRentInfoBSchema.getIslessorassociate());
		mLIPlaceRentInfoSchema.setIslessorright(mLIPlaceRentInfoBSchema.getIslessorright());
		mLIPlaceRentInfoSchema.setIshouserent(mLIPlaceRentInfoBSchema.getIshouserent());
		mLIPlaceRentInfoSchema.setIsplaceoffice(mLIPlaceRentInfoBSchema.getIsplaceoffice());
		mLIPlaceRentInfoSchema.setIshousegroup(mLIPlaceRentInfoBSchema.getIshousegroup());
		mLIPlaceRentInfoSchema.setIshousefarm(mLIPlaceRentInfoBSchema.getIshousefarm());
		mLIPlaceRentInfoSchema.setSigarea(mLIPlaceRentInfoBSchema.getSigarea());
		mLIPlaceRentInfoSchema.setGrparea(mLIPlaceRentInfoBSchema.getGrparea());
		mLIPlaceRentInfoSchema.setBankarea(mLIPlaceRentInfoBSchema.getBankarea());
		mLIPlaceRentInfoSchema.setHealtharea(mLIPlaceRentInfoBSchema.getHealtharea());
		mLIPlaceRentInfoSchema.setServicearea(mLIPlaceRentInfoBSchema.getServicearea());
		mLIPlaceRentInfoSchema.setJointarea(mLIPlaceRentInfoBSchema.getJointarea());
		mLIPlaceRentInfoSchema.setManageroffice(mLIPlaceRentInfoBSchema.getManageroffice());
		mLIPlaceRentInfoSchema.setDepartpersno(mLIPlaceRentInfoBSchema.getDepartpersno());
		mLIPlaceRentInfoSchema.setPlanfinance(mLIPlaceRentInfoBSchema.getPlanfinance());
		mLIPlaceRentInfoSchema.setPublicarea(mLIPlaceRentInfoBSchema.getPublicarea());
		mLIPlaceRentInfoSchema.setOtherarea(mLIPlaceRentInfoBSchema.getOtherarea());
		mLIPlaceRentInfoSchema.setExplanation(mLIPlaceRentInfoBSchema.getExplanation());
		mLIPlaceRentInfoSchema.setMarginfee(mLIPlaceRentInfoBSchema.getMarginfee());
		mLIPlaceRentInfoSchema.setMarginpaydate(mLIPlaceRentInfoBSchema.getMarginpaydate());
		mLIPlaceRentInfoSchema.setMarginbackdate(mLIPlaceRentInfoBSchema.getMarginbackdate());
		mLIPlaceRentInfoSchema.setPenaltyfee(mLIPlaceRentInfoBSchema.getPenaltyfee());
		mLIPlaceRentInfoSchema.setRemake(mLIPlaceRentInfoBSchema.getRemake());
		mLIPlaceRentInfoSchema.setFilename(mLIPlaceRentInfoBSchema.getFilename());
		mLIPlaceRentInfoSchema.setFilepath(mLIPlaceRentInfoBSchema.getFilepath());
		mLIPlaceRentInfoSchema.setOperator(mLIPlaceRentInfoBSchema.getOperator());
		mLIPlaceRentInfoSchema.setMakedate(PubFun.getCurrentDate());
		mLIPlaceRentInfoSchema.setMaketime(PubFun.getCurrentTime());
		mLIPlaceRentInfoSchema.setModifydate(PubFun.getCurrentDate());
		mLIPlaceRentInfoSchema.setModifytime(PubFun.getCurrentTime());
		mLIPlaceRentInfoSet.add(mLIPlaceRentInfoSchema);
		return mLIPlaceRentInfoSet;
	}

	private LIPlaceRentFeeBSet CopyToBTableFee(String placeno,String serialno,String changetype) {
		// TODO Auto-generated method stub
		//获取租金信息条数
		mLIPlaceRentFeeDB1.setFeetype("01");
		mLIPlaceRentFeeDB1.setPlaceno(placeno);
		LIPlaceRentFeeSet mLIPlaceRentFeeSet1 = mLIPlaceRentFeeDB1.query();
		//获取物业信息条数
		mLIPlaceRentFeeDB2.setFeetype("02");
		mLIPlaceRentFeeDB2.setPlaceno(placeno);
		LIPlaceRentFeeSet mLIPlaceRentFeeSet2 = mLIPlaceRentFeeDB2.query();
		//获取装修信息条数
		mLIPlaceRentFeeDB3.setFeetype("03");
		mLIPlaceRentFeeDB3.setPlaceno(placeno);
		LIPlaceRentFeeSet mLIPlaceRentFeeSet3 = mLIPlaceRentFeeDB3.query();
					
		System.out.println("获取租金信息条数mLIPlaceRentFeeSet1====="+mLIPlaceRentFeeSet1.size());
		System.out.println("获取物业信息条数mLIPlaceRentFeeSet2====="+mLIPlaceRentFeeSet2.size());
		System.out.println("获取装修信息条数mLIPlaceRentFeeSet3====="+mLIPlaceRentFeeSet3.size());
		System.out.println("初始值："+mLIPlaceRentFeeBSet1.size());
		//向费用表中添加租金信息
		for(int i=1; i<=mLIPlaceRentFeeSet1.size(); i++ ){
			//此处Schema类必须在每一次循环的时更新一下，否则循环执行过后最后一条信息会覆盖之前的数据
			mLIPlaceRentFeeBSchema1= new LIPlaceRentFeeBSchema();
			mLIPlaceRentFeeBSchema1.setPlaceno(mLIPlaceRentFeeSet1.get(i).getPlaceno());
			mLIPlaceRentFeeBSchema1.setFeetype(mLIPlaceRentFeeSet1.get(i).getFeetype());
			mLIPlaceRentFeeBSchema1.setOrder(mLIPlaceRentFeeSet1.get(i).getOrder());
			mLIPlaceRentFeeBSchema1.setPayDate(mLIPlaceRentFeeSet1.get(i).getPayDate());
			mLIPlaceRentFeeBSchema1.setMoney(mLIPlaceRentFeeSet1.get(i).getMoney());
			mLIPlaceRentFeeBSchema1.setPaybegindate(mLIPlaceRentFeeSet1.get(i).getPaybegindate());
			mLIPlaceRentFeeBSchema1.setPayenddate(mLIPlaceRentFeeSet1.get(i).getPayenddate());
			mLIPlaceRentFeeBSchema1.setDealtype(changetype);
			mLIPlaceRentFeeBSchema1.setDealstate("02");
			mLIPlaceRentFeeBSchema1.setOperator(mLIPlaceRentFeeSet1.get(i).getOperator());
			mLIPlaceRentFeeBSchema1.setMakedate(PubFun.getCurrentDate());
			mLIPlaceRentFeeBSchema1.setMaketime(PubFun.getCurrentTime());
			mLIPlaceRentFeeBSchema1.setModifyDate(PubFun.getCurrentDate());
			mLIPlaceRentFeeBSchema1.setModifyTime(PubFun.getCurrentTime());
			mLIPlaceRentFeeBSchema1.setStandbystring1(serialno);
			System.out.println("序列号Order第"+i+"个====="+mLIPlaceRentFeeSet1.get(i).getOrder());
			mLIPlaceRentFeeBSet1.add(mLIPlaceRentFeeBSchema1);
			System.out.println("租金："+mLIPlaceRentFeeBSet1.size());
		}
			//向费用表中添加物业信息
		for(int i=1; i<=mLIPlaceRentFeeSet2.size(); i++ ){
		//此处Schema类必须在每一次循环的时更新一下，否则循环执行过后最后一条信息会覆盖之前的数据
		mLIPlaceRentFeeBSchema2= new LIPlaceRentFeeBSchema();
		mLIPlaceRentFeeBSchema2.setPlaceno(mLIPlaceRentFeeSet2.get(i).getPlaceno());
		mLIPlaceRentFeeBSchema2.setFeetype(mLIPlaceRentFeeSet2.get(i).getFeetype());
		mLIPlaceRentFeeBSchema2.setOrder(mLIPlaceRentFeeSet2.get(i).getOrder());
		mLIPlaceRentFeeBSchema2.setPayDate(mLIPlaceRentFeeSet2.get(i).getPayDate());
		mLIPlaceRentFeeBSchema2.setMoney(mLIPlaceRentFeeSet2.get(i).getMoney());
		mLIPlaceRentFeeBSchema2.setPaybegindate(mLIPlaceRentFeeSet2.get(i).getPaybegindate());
		mLIPlaceRentFeeBSchema2.setPayenddate(mLIPlaceRentFeeSet2.get(i).getPayenddate());
		mLIPlaceRentFeeBSchema2.setDealtype(changetype);
		mLIPlaceRentFeeBSchema2.setDealstate("02");
		mLIPlaceRentFeeBSchema2.setOperator(mLIPlaceRentFeeSet2.get(i).getOperator());
		mLIPlaceRentFeeBSchema2.setMakedate(PubFun.getCurrentDate());
		mLIPlaceRentFeeBSchema2.setMaketime(PubFun.getCurrentTime());
		mLIPlaceRentFeeBSchema2.setModifyDate(PubFun.getCurrentDate());
		mLIPlaceRentFeeBSchema2.setModifyTime(PubFun.getCurrentTime());
		mLIPlaceRentFeeBSchema2.setStandbystring1(serialno);
		System.out.println("序列号Order第"+i+"个====="+mLIPlaceRentFeeSet2.get(i).getOrder());
		mLIPlaceRentFeeBSet1.add(mLIPlaceRentFeeBSchema2);
		System.out.println("物业："+mLIPlaceRentFeeBSet1.size());
	}
	//向费用表中添加装修信息
	for(int i=1; i<=mLIPlaceRentFeeSet3.size(); i++ ){
		//此处Schema类必须在每一次循环的时更新一下，否则循环执行过后最后一条信息会覆盖之前的数据
		mLIPlaceRentFeeBSchema3= new LIPlaceRentFeeBSchema();
		mLIPlaceRentFeeBSchema3.setPlaceno(mLIPlaceRentFeeSet3.get(i).getPlaceno());
		mLIPlaceRentFeeBSchema3.setFeetype(mLIPlaceRentFeeSet3.get(i).getFeetype());
		mLIPlaceRentFeeBSchema3.setOrder(mLIPlaceRentFeeSet3.get(i).getOrder());
		mLIPlaceRentFeeBSchema3.setPayDate(mLIPlaceRentFeeSet3.get(i).getPayDate());
		mLIPlaceRentFeeBSchema3.setMoney(mLIPlaceRentFeeSet3.get(i).getMoney());
		mLIPlaceRentFeeBSchema3.setPaybegindate(mLIPlaceRentFeeSet3.get(i).getPaybegindate());
		mLIPlaceRentFeeBSchema3.setPayenddate(mLIPlaceRentFeeSet3.get(i).getPayenddate());
		mLIPlaceRentFeeBSchema3.setDealtype(changetype);
		mLIPlaceRentFeeBSchema3.setDealstate("02");
		mLIPlaceRentFeeBSchema3.setOperator(mLIPlaceRentFeeSet3.get(i).getOperator());
		mLIPlaceRentFeeBSchema3.setMakedate(PubFun.getCurrentDate());
		mLIPlaceRentFeeBSchema3.setMaketime(PubFun.getCurrentTime());
		mLIPlaceRentFeeBSchema3.setModifyDate(PubFun.getCurrentDate());
		mLIPlaceRentFeeBSchema3.setModifyTime(PubFun.getCurrentTime());
		mLIPlaceRentFeeBSchema3.setStandbystring1(serialno);
		System.out.println("序列号Order第"+i+"个====="+mLIPlaceRentFeeSet3.get(i).getOrder());
		mLIPlaceRentFeeBSet1.add(mLIPlaceRentFeeBSchema3);
		System.out.println("装修："+mLIPlaceRentFeeBSet1.size());
	}
		//获取费用流水号
		try {
			System.out.println("mLIPlaceRentFeeBSet1.size()======"+mLIPlaceRentFeeBSet1.size());
			String []feeSerialno=PlCreateSerialNO.getSerialNoByFee(mLIPlaceRentFeeBSet1.size());
			for (int i = 1; i <= mLIPlaceRentFeeBSet1.size(); i++) {
					mLIPlaceRentFeeBSet1.get(i).setSerialno(feeSerialno[i-1]);
					System.out.println("生成的费用的流水号为"+i+"："+mLIPlaceRentFeeBSet1.get(i).getSerialno());
			}	
		} catch (Exception e) {
			e.printStackTrace();
			buildError("prepareData","报错原因：生成流水号失败！");
			System.out.println("getInputData==========false");
		}	
				
		return mLIPlaceRentFeeBSet1;
	}

	private LIPlaceRentInfoBSet CopyToBTable(String placeno,String changetype) {
		// TODO Auto-generated method stub
		//查询出主表信息
		System.out.println("===开始获取主表信息！===");
		mLIPlaceRentInfoDB.setPlaceno(placeno);
		mLIPlaceRentInfoSchema= mLIPlaceRentInfoDB.query().get(1);
		
		mLIPlaceRentInfoBSchema.setPlaceno(mLIPlaceRentInfoSchema.getPlaceno());
		mLIPlaceRentInfoBSchema.setManagecom(mLIPlaceRentInfoSchema.getManagecom());
		mLIPlaceRentInfoBSchema.setAppno(mLIPlaceRentInfoSchema.getAppno());
		mLIPlaceRentInfoBSchema.setDealtype(changetype);
		mLIPlaceRentInfoBSchema.setDealstate("02");
		mLIPlaceRentInfoBSchema.setPlacenorefer("");
		mLIPlaceRentInfoBSchema.setState("00");//00--停用、01--在用 、02-审批状态
		mLIPlaceRentInfoBSchema.setIslock("02"); // 01 非锁定  02 锁定 
		mLIPlaceRentInfoBSchema.setComlevel(mLIPlaceRentInfoSchema.getComlevel());
		mLIPlaceRentInfoBSchema.setComname(mLIPlaceRentInfoSchema.getComname());
		mLIPlaceRentInfoBSchema.setBegindate(mLIPlaceRentInfoSchema.getBegindate());
		mLIPlaceRentInfoBSchema.setEnddate(mLIPlaceRentInfoSchema.getEnddate());
		mLIPlaceRentInfoBSchema.setActualenddate(PubFun.getCurrentDate());
		mLIPlaceRentInfoBSchema.setAddress(mLIPlaceRentInfoSchema.getAddress());
		mLIPlaceRentInfoBSchema.setLessor(mLIPlaceRentInfoSchema.getLessor());
		mLIPlaceRentInfoBSchema.setHouseno(mLIPlaceRentInfoSchema.getHouseno());
		mLIPlaceRentInfoBSchema.setProvince(mLIPlaceRentInfoSchema.getProvince());
		mLIPlaceRentInfoBSchema.setCity(mLIPlaceRentInfoSchema.getCity());
		mLIPlaceRentInfoBSchema.setIsrentregister(mLIPlaceRentInfoSchema.getIsrentregister());
		mLIPlaceRentInfoBSchema.setIslessorassociate(mLIPlaceRentInfoSchema.getIslessorassociate());
		mLIPlaceRentInfoBSchema.setIslessorright(mLIPlaceRentInfoSchema.getIslessorright());
		mLIPlaceRentInfoBSchema.setIshouserent(mLIPlaceRentInfoSchema.getIshouserent());
		mLIPlaceRentInfoBSchema.setIsplaceoffice(mLIPlaceRentInfoSchema.getIsplaceoffice());
		mLIPlaceRentInfoBSchema.setIshousegroup(mLIPlaceRentInfoSchema.getIshousegroup());
		mLIPlaceRentInfoBSchema.setIshousefarm(mLIPlaceRentInfoSchema.getIshousefarm());
		mLIPlaceRentInfoBSchema.setSigarea(mLIPlaceRentInfoSchema.getSigarea());
		mLIPlaceRentInfoBSchema.setGrparea(mLIPlaceRentInfoSchema.getGrparea());
		mLIPlaceRentInfoBSchema.setBankarea(mLIPlaceRentInfoSchema.getBankarea());
		mLIPlaceRentInfoBSchema.setHealtharea(mLIPlaceRentInfoSchema.getHealtharea());
		mLIPlaceRentInfoBSchema.setServicearea(mLIPlaceRentInfoSchema.getServicearea());
		mLIPlaceRentInfoBSchema.setJointarea(mLIPlaceRentInfoSchema.getJointarea());
		mLIPlaceRentInfoBSchema.setManageroffice(mLIPlaceRentInfoSchema.getManageroffice());
		mLIPlaceRentInfoBSchema.setDepartpersno(mLIPlaceRentInfoSchema.getDepartpersno());
		mLIPlaceRentInfoBSchema.setPlanfinance(mLIPlaceRentInfoSchema.getPlanfinance());
		mLIPlaceRentInfoBSchema.setPublicarea(mLIPlaceRentInfoSchema.getPublicarea());
		mLIPlaceRentInfoBSchema.setOtherarea(mLIPlaceRentInfoSchema.getOtherarea());
		mLIPlaceRentInfoBSchema.setExplanation(mLIPlaceRentInfoSchema.getExplanation());
		mLIPlaceRentInfoBSchema.setMarginfee(mLIPlaceRentInfoSchema.getMarginfee());
		mLIPlaceRentInfoBSchema.setMarginpaydate(mLIPlaceRentInfoSchema.getMarginpaydate());
		mLIPlaceRentInfoBSchema.setMarginbackdate(mLIPlaceRentInfoSchema.getMarginbackdate());
		mLIPlaceRentInfoBSchema.setPenaltyfee(mLIPlaceRentInfoSchema.getPenaltyfee());
		mLIPlaceRentInfoBSchema.setRemake(mLIPlaceRentInfoSchema.getRemake());
		mLIPlaceRentInfoBSchema.setFilename(mLIPlaceRentInfoSchema.getFilename());
		mLIPlaceRentInfoBSchema.setFilepath(mLIPlaceRentInfoSchema.getFilepath());
		mLIPlaceRentInfoBSchema.setOperator(mLIPlaceRentInfoSchema.getOperator());
		mLIPlaceRentInfoBSchema.setMakedate(PubFun.getCurrentDate());
		mLIPlaceRentInfoBSchema.setMaketime(PubFun.getCurrentTime());
		mLIPlaceRentInfoBSchema.setModifydate(PubFun.getCurrentDate());
		mLIPlaceRentInfoBSchema.setModifytime(PubFun.getCurrentTime());
		mLIPlaceRentInfoBSet1.add(mLIPlaceRentInfoBSchema);
		//生成流水号
		try{
			//获取信息流水号
			System.out.println("======mLIPlaceRentInfoBSet.size()======"+mLIPlaceRentInfoBSet1.size());
			 String []infoSerialno=PlCreateSerialNO.getSerialNoByInfo(mLIPlaceRentInfoBSet1.size());
			//职场只可能有一条数据
			 mLIPlaceRentInfoBSet1.get(1).setSerialno(infoSerialno[0]);
			 System.out.println("生成的流水号为："+mLIPlaceRentInfoBSet1.get(1).getSerialno());
		}catch(Exception e){
			e.printStackTrace();
		      buildError("prepareData","报错原因：生成流水号失败！");
		      System.out.println("getInputData==========false");
		}
		
		System.out.println("====主表信息获取完毕！=====");
		return mLIPlaceRentInfoBSet1;
	}

	private LIPlaceRentFeeSet CopyToMainTableFee(String Standbystring1) {
		
		//获取租金信息条数
		mLIPlaceRentFeeBDB1.setFeetype("01");
		mLIPlaceRentFeeBDB1.setStandbystring1(Standbystring1);
		LIPlaceRentFeeBSet mLIPlaceRentFeeBSet1 = mLIPlaceRentFeeBDB1.query();
		//获取物业信息条数
		mLIPlaceRentFeeBDB2.setFeetype("02");
		mLIPlaceRentFeeBDB2.setStandbystring1(Standbystring1);
		LIPlaceRentFeeBSet mLIPlaceRentFeeBSet2 = mLIPlaceRentFeeBDB2.query();
		//获取装修信息条数
		mLIPlaceRentFeeBDB3.setFeetype("03");
		mLIPlaceRentFeeBDB3.setStandbystring1(Standbystring1);
		LIPlaceRentFeeBSet mLIPlaceRentFeeBSet3 = mLIPlaceRentFeeBDB3.query();
		
		System.out.println("获取租金信息条数mLIPlaceRentFeeBSet1====="+mLIPlaceRentFeeBSet1.size());
		System.out.println("获取物业信息条数mLIPlaceRentFeeBSet2====="+mLIPlaceRentFeeBSet2.size());
		System.out.println("获取装修信息条数mLIPlaceRentFeeBSet3====="+mLIPlaceRentFeeBSet3.size());
		//向费用表中添加租金信息
		for(int i=1; i<=mLIPlaceRentFeeBSet1.size(); i++ ){
			//此处Schema类必须在每一次循环的时更新一下，否则循环执行过后最后一条信息会覆盖之前的数据
			mLIPlaceRentFeeSchema1= new LIPlaceRentFeeSchema();
			mLIPlaceRentFeeSchema1.setPlaceno(mLIPlaceRentFeeBSet1.get(i).getPlaceno());
			mLIPlaceRentFeeSchema1.setFeetype(mLIPlaceRentFeeBSet1.get(i).getFeetype());
			mLIPlaceRentFeeSchema1.setOrder(mLIPlaceRentFeeBSet1.get(i).getOrder());
			mLIPlaceRentFeeSchema1.setPayDate(mLIPlaceRentFeeBSet1.get(i).getPayDate());
			mLIPlaceRentFeeSchema1.setMoney(mLIPlaceRentFeeBSet1.get(i).getMoney());
			mLIPlaceRentFeeSchema1.setPaybegindate(mLIPlaceRentFeeBSet1.get(i).getPaybegindate());
			mLIPlaceRentFeeSchema1.setPayenddate(mLIPlaceRentFeeBSet1.get(i).getPayenddate());
			mLIPlaceRentFeeSchema1.setOperator(mLIPlaceRentFeeBSet1.get(i).getOperator());
			mLIPlaceRentFeeSchema1.setMakedate(mLIPlaceRentFeeBSet1.get(i).getMakedate());
			mLIPlaceRentFeeSchema1.setMaketime(mLIPlaceRentFeeBSet1.get(i).getMaketime());
			mLIPlaceRentFeeSchema1.setModifyDate(mLIPlaceRentFeeBSet1.get(i).getModifyDate());
			mLIPlaceRentFeeSchema1.setModifyTime(mLIPlaceRentFeeBSet1.get(i).getModifyTime());
			System.out.println("序列号Order第"+i+"个====="+mLIPlaceRentFeeBSet1.get(i).getOrder());
			mLIPlaceRentFeeSet.add(mLIPlaceRentFeeSchema1);
		}
		//向费用表中添加物业信息
		for(int i=1; i<=mLIPlaceRentFeeBSet2.size(); i++ ){
			//此处Schema类必须在每一次循环的时更新一下，否则循环执行过后最后一条信息会覆盖之前的数据
			mLIPlaceRentFeeSchema2= new LIPlaceRentFeeSchema();
			mLIPlaceRentFeeSchema2.setPlaceno(mLIPlaceRentFeeBSet2.get(i).getPlaceno());
			mLIPlaceRentFeeSchema2.setFeetype(mLIPlaceRentFeeBSet2.get(i).getFeetype());
			mLIPlaceRentFeeSchema2.setOrder(mLIPlaceRentFeeBSet2.get(i).getOrder());
			mLIPlaceRentFeeSchema2.setPayDate(mLIPlaceRentFeeBSet2.get(i).getPayDate());
			mLIPlaceRentFeeSchema2.setMoney(mLIPlaceRentFeeBSet2.get(i).getMoney());
			mLIPlaceRentFeeSchema2.setPaybegindate(mLIPlaceRentFeeBSet2.get(i).getPaybegindate());
			mLIPlaceRentFeeSchema2.setPayenddate(mLIPlaceRentFeeBSet2.get(i).getPayenddate());
			mLIPlaceRentFeeSchema2.setOperator(mLIPlaceRentFeeBSet2.get(i).getOperator());
			mLIPlaceRentFeeSchema2.setMakedate(mLIPlaceRentFeeBSet2.get(i).getMakedate());
			mLIPlaceRentFeeSchema2.setMaketime(mLIPlaceRentFeeBSet2.get(i).getMaketime());
			mLIPlaceRentFeeSchema2.setModifyDate(mLIPlaceRentFeeBSet2.get(i).getModifyDate());
			mLIPlaceRentFeeSchema2.setModifyTime(mLIPlaceRentFeeBSet2.get(i).getModifyTime());
			System.out.println("序列号Order第"+i+"个====="+mLIPlaceRentFeeBSet2.get(i).getOrder());
			mLIPlaceRentFeeSet.add(mLIPlaceRentFeeSchema2);
		}
		//向费用表中添加装修信息
		for(int i=1; i<=mLIPlaceRentFeeBSet3.size(); i++ ){
			//此处Schema类必须在每一次循环的时更新一下，否则循环执行过后最后一条信息会覆盖之前的数据
			mLIPlaceRentFeeSchema3= new LIPlaceRentFeeSchema();
			mLIPlaceRentFeeSchema3.setPlaceno(mLIPlaceRentFeeBSet3.get(i).getPlaceno());
			mLIPlaceRentFeeSchema3.setFeetype(mLIPlaceRentFeeBSet3.get(i).getFeetype());
			mLIPlaceRentFeeSchema3.setOrder(mLIPlaceRentFeeBSet3.get(i).getOrder());
			mLIPlaceRentFeeSchema3.setPayDate(mLIPlaceRentFeeBSet3.get(i).getPayDate());
			mLIPlaceRentFeeSchema3.setMoney(mLIPlaceRentFeeBSet3.get(i).getMoney());
			mLIPlaceRentFeeSchema3.setPaybegindate(mLIPlaceRentFeeBSet3.get(i).getPaybegindate());
			mLIPlaceRentFeeSchema3.setPayenddate(mLIPlaceRentFeeBSet3.get(i).getPayenddate());
			mLIPlaceRentFeeSchema3.setOperator(mLIPlaceRentFeeBSet3.get(i).getOperator());
			mLIPlaceRentFeeSchema3.setMakedate(mLIPlaceRentFeeBSet3.get(i).getMakedate());
			mLIPlaceRentFeeSchema3.setMaketime(mLIPlaceRentFeeBSet3.get(i).getMaketime());
			mLIPlaceRentFeeSchema3.setModifyDate(mLIPlaceRentFeeBSet3.get(i).getModifyDate());
			mLIPlaceRentFeeSchema3.setModifyTime(mLIPlaceRentFeeBSet3.get(i).getModifyTime());
			System.out.println("序列号Order第"+i+"个====="+mLIPlaceRentFeeBSet3.get(i).getOrder());
			mLIPlaceRentFeeSet.add(mLIPlaceRentFeeSchema3);
		}
		return mLIPlaceRentFeeSet;
	}

	private LIPlaceRentInfoSet CopyToMainTable(String Standbystring1) {
		// 获取职场租赁基本信息信息
		System.out.println("开始查询。。。。。。。。。。。。。。。。。。。");
		mLIPlaceRentInfoBDB.setStandbystring1(Standbystring1);
		mLIPlaceRentInfoBSchema  = mLIPlaceRentInfoBDB.query().get(1);
		
		mLIPlaceRentInfoSchema.setPlaceno(mLIPlaceRentInfoBSchema.getPlaceno());
		mLIPlaceRentInfoSchema.setManagecom(mLIPlaceRentInfoBSchema.getManagecom());
		mLIPlaceRentInfoSchema.setAppno(mLIPlaceRentInfoBSchema.getAppno());
		mLIPlaceRentInfoSchema.setState("01");//00--停用、01--在用 、02-审批状态
		mLIPlaceRentInfoSchema.setIslock("02"); // 01 非锁定  02 锁定 
		mLIPlaceRentInfoSchema.setComlevel(mLIPlaceRentInfoBSchema.getComlevel());
		mLIPlaceRentInfoSchema.setComname(mLIPlaceRentInfoBSchema.getComname());
		mLIPlaceRentInfoSchema.setBegindate(mLIPlaceRentInfoBSchema.getBegindate());
		mLIPlaceRentInfoSchema.setEnddate(mLIPlaceRentInfoBSchema.getEnddate());
		mLIPlaceRentInfoSchema.setActualenddate(mLIPlaceRentInfoBSchema.getActualenddate());
		mLIPlaceRentInfoSchema.setAddress(mLIPlaceRentInfoBSchema.getAddress());
		mLIPlaceRentInfoSchema.setLessor(mLIPlaceRentInfoBSchema.getLessor());
		mLIPlaceRentInfoSchema.setHouseno(mLIPlaceRentInfoBSchema.getHouseno());
		mLIPlaceRentInfoSchema.setProvince(mLIPlaceRentInfoBSchema.getProvince());
		mLIPlaceRentInfoSchema.setCity(mLIPlaceRentInfoBSchema.getCity());
		mLIPlaceRentInfoSchema.setIsrentregister(mLIPlaceRentInfoBSchema.getIsrentregister());
		mLIPlaceRentInfoSchema.setIslessorassociate(mLIPlaceRentInfoBSchema.getIslessorassociate());
		mLIPlaceRentInfoSchema.setIslessorright(mLIPlaceRentInfoBSchema.getIslessorright());
		mLIPlaceRentInfoSchema.setIshouserent(mLIPlaceRentInfoBSchema.getIshouserent());
		mLIPlaceRentInfoSchema.setIsplaceoffice(mLIPlaceRentInfoBSchema.getIsplaceoffice());
		mLIPlaceRentInfoSchema.setIshousegroup(mLIPlaceRentInfoBSchema.getIshousegroup());
		mLIPlaceRentInfoSchema.setIshousefarm(mLIPlaceRentInfoBSchema.getIshousefarm());
		mLIPlaceRentInfoSchema.setSigarea(mLIPlaceRentInfoBSchema.getSigarea());
		mLIPlaceRentInfoSchema.setGrparea(mLIPlaceRentInfoBSchema.getGrparea());
		mLIPlaceRentInfoSchema.setBankarea(mLIPlaceRentInfoBSchema.getBankarea());
		mLIPlaceRentInfoSchema.setHealtharea(mLIPlaceRentInfoBSchema.getHealtharea());
		mLIPlaceRentInfoSchema.setServicearea(mLIPlaceRentInfoBSchema.getServicearea());
		mLIPlaceRentInfoSchema.setJointarea(mLIPlaceRentInfoBSchema.getJointarea());
		mLIPlaceRentInfoSchema.setManageroffice(mLIPlaceRentInfoBSchema.getManageroffice());
		mLIPlaceRentInfoSchema.setDepartpersno(mLIPlaceRentInfoBSchema.getDepartpersno());
		mLIPlaceRentInfoSchema.setPlanfinance(mLIPlaceRentInfoBSchema.getPlanfinance());
		mLIPlaceRentInfoSchema.setPublicarea(mLIPlaceRentInfoBSchema.getPublicarea());
		mLIPlaceRentInfoSchema.setOtherarea(mLIPlaceRentInfoBSchema.getOtherarea());
		mLIPlaceRentInfoSchema.setExplanation(mLIPlaceRentInfoBSchema.getExplanation());
		mLIPlaceRentInfoSchema.setMarginfee(mLIPlaceRentInfoBSchema.getMarginfee());
		mLIPlaceRentInfoSchema.setMarginpaydate(mLIPlaceRentInfoBSchema.getMarginpaydate());
		mLIPlaceRentInfoSchema.setMarginbackdate(mLIPlaceRentInfoBSchema.getMarginbackdate());
		mLIPlaceRentInfoSchema.setPenaltyfee(mLIPlaceRentInfoBSchema.getPenaltyfee());
		mLIPlaceRentInfoSchema.setRemake(mLIPlaceRentInfoBSchema.getRemake());
		mLIPlaceRentInfoSchema.setFilename(mLIPlaceRentInfoBSchema.getFilename());
		mLIPlaceRentInfoSchema.setFilepath(mLIPlaceRentInfoBSchema.getFilepath());
		mLIPlaceRentInfoSchema.setOperator(mLIPlaceRentInfoBSchema.getOperator());
		mLIPlaceRentInfoSchema.setMakedate(mLIPlaceRentInfoBSchema.getMakedate());
		mLIPlaceRentInfoSchema.setMaketime(mLIPlaceRentInfoBSchema.getMaketime());
		mLIPlaceRentInfoSchema.setModifydate(mLIPlaceRentInfoBSchema.getModifydate());
		mLIPlaceRentInfoSchema.setModifytime(mLIPlaceRentInfoBSchema.getModifytime());
		System.out.println("传入的serialno====="+Standbystring1);
		System.out.println("查询出的serialno====="+mLIPlaceRentInfoBSchema.getSerialno());
		System.out.println("查询出的Placeno====="+mLIPlaceRentInfoBSchema.getPlaceno());
		System.out.println("查询出的Explanation====="+mLIPlaceRentInfoBSchema.getExplanation());
		System.out.println("查询出的Filename====="+mLIPlaceRentInfoBSchema.getFilename());
		System.out.println("查询出的Filepath====="+mLIPlaceRentInfoBSchema.getFilepath());
		mLIPlaceRentInfoSet.add(mLIPlaceRentInfoSchema);
		return mLIPlaceRentInfoSet;
	}

	public VData getResult()
	  {
	    return this.mResult;
	  }

}








