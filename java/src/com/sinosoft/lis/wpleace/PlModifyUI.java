package com.sinosoft.lis.wpleace;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PlModifyUI 
{

	 /** 错误处理类，每个需要错误处理的类中都放置该类 */
   public CErrors mErrors = new CErrors();

   private VData mResult = new VData();
   //业务处理相关变量
   /** 全局数据 */
   private GlobalInput mGlobalInput = new GlobalInput();
   private String mDay[] = null;


   public PlModifyUI()
   {}

   /**
    传输数据的公共方法
    */
   public boolean submitData(VData cInputData, String cOperate)
   {
       try
       {
           if (!cOperate.equals("MODIFY"))
           {
               buildError("submitData", "不支持的操作字符串");
               return false;
           }
           PlModifyBL mPlModifyBL = new PlModifyBL(); 
           System.out.println("Start PlModifyUI Submit ...");
           if (!mPlModifyBL.submitData(cInputData, cOperate))
           {
               if (mPlModifyBL.mErrors.needDealError())
               {
                   mErrors.copyAllErrors(mPlModifyBL.mErrors);
                   return false;
               }
               else
               {
                   buildError("submitData","mPlModifyBL，但是没有提供详细的出错信息");
                   return false;
               }
           }
           else
           {
               mResult = mPlModifyBL.getResult();
               return true;
           }
       }
       catch (Exception e)
       {
           e.printStackTrace();
           buildError("submitData","报错原因是："+e.toString());
           return false;
       }
   }


   /**
    * 准备往后层输出所需要的数据
    * 输出：如果准备数据时发生错误则返回false,否则返回true
    */

   public VData getResult()
   {
       return this.mResult;
   }

   private void buildError(String szFunc, String szErrMsg)
   {
       CError cError = new CError();

       cError.moduleName = "PlModifyUI";
       cError.functionName = szFunc;
       cError.errorMessage = szErrMsg;
       this.mErrors.addOneError(cError);
   }



}
