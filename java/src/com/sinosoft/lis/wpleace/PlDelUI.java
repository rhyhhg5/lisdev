/*
* <p>ClassName: LAAssessInputUI </p>
* <p>Description: LAAssessUI类文件 </p>
* <p>Copyright: Copyright (c) 2002</p>
* <p>Company: sinosoft </p>
* @Database: 销售管理
* @CreateDate：2003-06-21
 */
package com.sinosoft.lis.wpleace;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class PlDelUI {
	 /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public PlDelUI()
    {}

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate,String placeno)
    {
        try
        {
            if (!cOperate.equals("Del"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }
            PlDelBL mPlDelBL = new PlDelBL(); 
            System.out.println("Start PlDelUI UI Submit ...");

            if (!mPlDelBL.submitData(cInputData, cOperate,placeno))
            {
                if (mPlDelBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(mPlDelBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData","PlDelBL，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = mPlDelBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("submitData","PlDelBL，操作错误："+e.getMessage());
            return false;
        }
    }
    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "PlUnlockUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}