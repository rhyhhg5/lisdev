/*
* <p>ClassName: LAAssessInputUI </p>
* <p>Description: LAAssessUI类文件 </p>
* <p>Copyright: Copyright (c) 2002</p>
* <p>Company: sinosoft </p>
* @Database: 销售管理
* @CreateDate：2003-06-21
 */
package com.sinosoft.lis.wpleace;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LIPlaceRentFeeDB;
import com.sinosoft.lis.pubfun.*;

public class PlDecAddUI {
	 /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mDay[] = null;
    private LIPlaceRentFeeBSchema mLIPlaceRentFeeBSchema;
//    private LIPlaceRentFeeSet mLIPlaceRentFeeSet=new LIPlaceRentFeeSet();
 //   private LIPlaceRentFeeDB mLIPlaceRentFeeDB= new LIPlaceRentFeeDB();
    private LIPlaceRentFeeBSet mLIPlaceRentFeeBSet = new LIPlaceRentFeeBSet();

    public PlDecAddUI()
    {}

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate,String placeno)
    {
        try
        {
            if (!cOperate.equals("ADD"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }

            // 进行业务处理
/*            if (!dealData(placeno))
            {
                return false;
            }*/

            // 准备传往后台的数据
            VData vData = new VData();

            if (!prepareOutputData(vData))
            {
                return false;
            }

            PlDecAddBL mPlDecAddBL = new PlDecAddBL(); 
            System.out.println("Start PlDecAddUI UI Submit ...");

            if (!mPlDecAddBL.submitData(cInputData, cOperate))
            {
                if (mPlDecAddBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(mPlDecAddBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData","mPlDecAddBL，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = mPlDecAddBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CError cError = new CError();
            cError.moduleName = "PlDecAddUI";
            cError.functionName = "submit";
            cError.errorMessage = e.toString();
            mErrors.addOneError(cError);
            return false;
        }
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData(String placeno)
    {
//    	mLIPlaceRentFeeDB.setPlaceno(placeno);
//    	mLIPlaceRentFeeDB.setFeetype("03");
//    	mLIPlaceRentFeeSet = mLIPlaceRentFeeDB.query();
    	
    	String sql="select paybegindate,payenddate from LIPlaceRentFee where placeno='"+placeno+"' and feetype='03' ";
    	System.out.println("sql:"+sql);
    	System.out.println("mLIPlaceRentFeeBSet.size()"+mLIPlaceRentFeeBSet.size());
    	ExeSQL exesql = new ExeSQL();
    	SSRS tssrs = exesql.execSQL(sql);
    	for(int i=1;i<=tssrs.MaxRow;i++)
		{
			String begindate=tssrs.GetText(i, 1).substring(0,tssrs.GetText(i, 1).lastIndexOf("-"));
    		String enddate=tssrs.GetText(i, 2).substring(0,tssrs.GetText(i, 2).lastIndexOf("-"));

			for(int y=1;y<=mLIPlaceRentFeeBSet.size();y++){
				mLIPlaceRentFeeBSchema=mLIPlaceRentFeeBSet.get(y);
				String tbegindate=mLIPlaceRentFeeBSchema.getPaybegindate().substring(0,mLIPlaceRentFeeBSchema.getPaybegindate().lastIndexOf("-"));
	    		String tenddate=mLIPlaceRentFeeBSchema.getPayenddate().substring(0,mLIPlaceRentFeeBSchema.getPayenddate().lastIndexOf("-"));
	    		if(compareDate(tenddate,begindate)==-1 || compareDate(tbegindate,enddate)==1){
	    	    	continue;
	    	    }else{
	    	    	buildError("dealData","职场编号："+placeno+",装修费摊销日期与现有摊销日期重复，请重新输入！");
	    	    	return false;
	    	    }
			}
		}
    	
        return true;
    }

    /*
     *作用：比较日期m和n的大小
     *返回值：如果m>n 返回1； 如果m=n 返回0 ；如果m<n 返回-1
    */
    private int compareDate(String m,String n){
    	int mYear=Integer.parseInt(m.substring(0,4));
    	int nYear=Integer.parseInt(n.substring(0,4));
    	int mMonth=Integer.parseInt(m.substring(m.indexOf("-")+1,m.length()));
    	int nMonth=Integer.parseInt(n.substring(n.indexOf("-")+1,n.length()));
    	System.out.println("=============");
    	System.out.println("mYear:"+mYear);
    	System.out.println("nYear:"+nYear);
    	System.out.println("mMonth:"+mMonth);
    	System.out.println("nMonth:"+nMonth);
    	System.out.println("=================");
    	if(mYear>nYear){
    		return 1;
    	}else if(mYear==nYear){
    		if(mMonth>nMonth){
    			return 1;
    		}else if(mMonth==nMonth){
    			return 0;
    		}else{
    			return -1;
    		}
    		
    	}else{
    		return -1;
    	}
    }
    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
    	//mLIPlaceRentFeeSet=(LIPlaceRentFeeSet)cInputData.get(1);
    	this.mLIPlaceRentFeeBSet=(LIPlaceRentFeeBSet)cInputData.getObjectByObjectName("LIPlaceRentFeeBSet",0);
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "PlDecAddUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}