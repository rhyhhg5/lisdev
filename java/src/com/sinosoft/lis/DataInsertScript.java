package com.sinosoft.lis;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.io.*;
import java.sql.Time;
import java.sql.Date;
import com.sinosoft.workflow.tb.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.lis.schema.LWMissionSchema;
import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.lis.vschema.LWMissionSet;


/**
 * <p>Title: </p>
 * @PICCH压力测试数据准备-新单复核 数据插入脚本
 * <p>Description: </p>
 * @
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author Liuliang
 * @version 1.0
 */
public class DataInsertScript
{

	private static Connection conn = null; //建立连接
	/**
   * flag = true: 传入Connection
   * flag = false: 不传入Connection
   **/
   private static boolean mflag = false;


		//LDPerson的插入语句
	 private static final String SQL_LDPerson =
		"insert into LDPerson" +
	   //	 "    1     2    3      4      5     6      7        8        9         10         11          12         13      14      15          16
		 " (CUSTOMERNO,Name,Sex,Birthday,Idtype,Idno,Marriage,Stature,Avoirdupois,Salary,OccupationCode,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime)"+
		 " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

  //LCAddress的插入语句
     private static final String SQL_LCAddress =
		"insert into LCAddress" +
	   //	 "     1       2           3           4      5          6        7            8
		 " (CUSTOMERNO,ADDRESSNO,PostalAddress,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime)"+
		 " values (?,?,?,?,?,?,?,?)";
   //LCAppnt的插入语句
	 private static final String SQL_LCAppnt =
		 "insert into LCAppnt" +
//	 "        1           2    3      4      5           6         7             8      9     10    11       12        13         14       15                16        17        18        19      20        21        22      23    " +
		 " (Grpcontno,Contno,Prtno,Appntno,Appntname,Appntsex,Appntbirthday,AddressNo,Idtype,Idno,Marriage,Stature,Avoirdupois,Salary,Occupationtype,Occupationcode,Operator,Managecom,MakeDate,MakeTime,ModifyDate,ModifyTime,Bmi)" +
	  " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

   //LCInsured的插入语句
     private static final String SQL_LCInsured =
	  "insert into LCInsured" +
//	  "        1      2        3      4      5        6        7           8                 9            10              11       12        13  14     15    16     17       18      19       20       21            22           23          24       25      26         27        28      29         30 "+
	  " (Grpcontno,Contno,INSUREDNO,Prtno,Appntno,Managecom,Executecom,RELATIONTOMAININSURED,RELATIONTOAPPNT,ADDRESSNO,SEQUENCENO,Name,Sex,Birthday,IDTYPE,IDNo,Marriage,Stature,Avoirdupois,Salary,Occupationtype,Occupationcode,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime,Bmi,INSUREDPEOPLES,FamilyID)" +
	  " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

   //LCCont的插入语句
	 private static final String SQL_LCCont =
		 "insert into LCCont" +
//	  "        1       2        3           4      5          6        7       8        9          10       11          12        13      14       15       16        17            18           19       20          21        22            23             24            25       26      27        28        29 sInt     30    31   32   33     34    35      36           37         38        39        40     41       42      43       44         45        46           47        48               " +
		 " (Grpcontno,Contno,ProposalContNo,Prtno,ContType,FamilyType,PolType,CardFlag,Managecom,Executecom,Agentcode,Agentgroup,Salechnl,Appntno,Appntname,Appntsex,Appntbirthday,Appntidtype,Appntidno,Insuredno,Insuredname,Insuredsex,Insuredbirthday,Insuredidtype,Insuredidno,PayIntv,Signcom,PrintCount,LostTimes,PEOPLES,MULT,PREM,AMNT,SUMPREM,DIF,INPUTOPERATOR,INPUTDATE,INPUTTIME,APPROVEFLAG,UWFLAG,APPFLAG,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime,FamilyID)" +
	  " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	//LCPol的插入语句
  private static final String SQL_LCPol =
	  "insert into LCPol" +
	 // "      1       2        3       4      5        6       7         8          9         10       11        12        13        14       15         16        17         18         19         20          21             22            23             24            25      26       27        28          29          30       31         32       33          34          35      36           37            38       39        40     41       42          43      44      45     46      47    48     49      50              51          52        53         54        55         56       57         58            59        60        61        62         63      64           65      66        67      68           69        70           71 " +
	  " (GRPCONTNO,GRPPOLNO,CONTNO,POLNO,PROPOSALNO,PRTNO,CONTTYPE,POLTYPEFLAG,MAINPOLNO,KINDCODE,RISKCODE,RISKVERSION,MANAGECOM,AGENTCOM,AGENTCODE,AGENTGROUP,SALECHNL,INSUREDNO,INSUREDNAME,INSUREDSEX,INSUREDBIRTHDAY,INSUREDAPPAGE,INSUREDPEOPLES,OCCUPATIONTYPE,APPNTNO,APPNTNAME,CVALIDATE,PAYENDDATE,GETSTARTDATE,ENDDATE,GETYEARFLAG,GETYEAR,PAYENDYEAR,INSUYEARFLAG,INSUYEAR,ACCIYEAR,SPECIFYVALIDATE,PAYLOCATION,PAYINTV,PAYYEARS,YEARS,MANAGEFEERATE,FLOATRATE,MULT,STANDPREM,PREM,SUMPREM,AMNT,RISKAMNT,LEAVINGMONEY,ENDORSETIMES,CLAIMTIMES,LIVETIMES,RENEWCOUNT,RNEWFLAG,AUTOPAYFLAG,BNFFLAG,HEALTHCHECKFLAG,IMPARTFLAG,ApproveFlag,UWFlag,PolApplyDate,AppFlag,StandbyFlag1,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime,WaitPeriod,PayEndYearFlag)" +
	  " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";



//  //LJAGetEndorse的插入语句
//  private static final String SQL_LJAGetEndorse =
//	  "insert into LJAGetEndorse" +
//	  " (PolNo,EndorsementNo,GrpPolNo,ContNo,ActuGetNo,DutyCode,PayPlanCOde,FeeOperationType,FeeFinaType,RiskCode,GetMoney,Operator,MakeDate,MakeTime,ModifyDate,ModifyTime,AgentCode,AgentGroup,GetDate,OtherNo,OtherNoType)" +
//	  " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

  private static String Contno ="130110000014495";     //保单合同号码
  private static String Prtno ="86110020001";          //印刷号
  private static String AppntNo="0000498904" ;         //投保人客户号码
  private static String AppntName ="test";             //投保人的姓名
  private String AppntSex ="0";                 //投保人性别

  private Date AppntBirthday =new Date(1979-07-19);         //投保人出生日期



  //被保人信息
  private String Insuredno = "0000498901";      //被保人客户号
  private static String InsuredName = "test-2005-A";   //被保人姓名   第一被保人
                                                //被保人命名规则:	被保人-[投保人姓名流水号]-A
                                                //如:	被保人-2005-A
  private static String SubInsuredName = "test-2005-B";//第二被保人  如:	被保人-2005-B
  private String InsuredSex = "0";              //被保人性别
  private Date InsuredBirthday =null;        //被保人出生日期
  private String InsuredIDType = "4";           //被保人证件类别
  private String InsuredIDNo ="12345";          //第一被保人证件号
  private String SubInsuredIDNo="456789";       //第二被保人证件号
  private String InsuredAppAge = "54";          //被保人投保年龄
  private String  Insuredno1="";
  private String  Insuredno2="";
  private String InsuredNameA = "";
  private String InsuredNameB = "";


  //个人险种信息
  private static String PolNo = "110110000020883";     //保单险种号码
  private Date CValiDate =new Date(2005-04-18); //险种生效日期
  private Date PayEndDate = new Date(2006-04-18);

  private double Prem =2438;
  private double Amnt = 88300;
  private double SumPrem=0;

  private static String Customerno="0";

  Time time = new Time(System.currentTimeMillis()); //当前时间
  Date date = new Date(System.currentTimeMillis()); //当前日期



    public DataInsertScript()
    {

    }

    public static void main(String[] args)
    {


        DataInsertScript datainsertscript = new DataInsertScript();

	if (!mflag)
    {
     conn = DBConnPool.getConnection();
    }
	if(conn!=null)
	{
		System.out.println("数据库连接成功!");

		//开始插入数据
        try
        {


			for(int i=1;i<=9999;i++)
			{

	            //生成印刷号的[86110020001]  20001---29999部分
				int temp = 20000 +i;
				Prtno = "861100"+ Integer.toString(temp);

				//个单生成工作流
				datainsertscript.CreatWorkFlow();

				//先生成Customerno,Contno,Insuredno,Appntno
                Customerno = PubFun1.CreateMaxNo("CUSTOMERNO", "SN");
                System.out.println(Customerno);
                AppntNo = Customerno;
                String tLimit = PubFun.getNoLimit("86110000");
                Contno = PubFun1.CreateMaxNo("ProposalContNo", tLimit);
                System.out.println(Contno);


				AppntName = "投保人-" +Integer.toString(temp);
				InsuredName="被保人"+AppntName.subSequence(AppntName.lastIndexOf("-"),AppntName.length())+"-A";

				System.out.println(AppntName);
				//向各个表中插入数据
				datainsertscript.InsertData();

	            //工作流业务逻辑转换
				datainsertscript.WorkFlowOperate();
			}


        }
        catch (Exception ex)
        {
			System.out.println(ex.getMessage());

			System.out.println("Error " + ex.toString());

        }
	}


    }

    private void CreatWorkFlow()throws Exception
	{
		//输出参数
        CErrors tError = null;
        String FlagStr = "Fail";


        GlobalInput tG = new GlobalInput();
		tG.Operator = "001";
		tG.ComCode  = "86";
        tG.ManageCom = "86";
        if (tG == null)
        {
            System.out.println("tG is null");
            return;
        }
       //prepare data for workflow
        VData tVData = new VData();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("PrtNo", Prtno);
        tTransferData.setNameAndValue("ManageCom","86110000");
        tTransferData.setNameAndValue("InputDate", PubFun.getCurrentDate());
        tTransferData.setNameAndValue("Operator", "001");
        tTransferData.setNameAndValue("SubType", "03");
        tVData.add(tTransferData);
        tVData.add(tG);

		try
		{
			TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
			if (tTbWorkFlowUI.submitData(tVData,"7799999999") == false)
				{
					int n = tTbWorkFlowUI.mErrors.getErrorCount();
					System.out.println("n=="+n);
					for (int j = 0; j < n; j++)
					System.out.println("Error: "+tTbWorkFlowUI.mErrors.getError(j).errorMessage);
					System.out.println( " 投保单申请失败，原因是: " + tTbWorkFlowUI.mErrors.getError(0).errorMessage);
					FlagStr = "Fail";
				}
						//如果在Catch中发现异常，则不从错误类中提取错误信息
				if (FlagStr != "Fail")
				{
					tError = tTbWorkFlowUI.mErrors;
					//tErrors = tTbWorkFlowUI.mErrors;
					System.out.println(" 投保单申请成功! ");
					if (!tError.needDealError())
					{
						int n = tError.getErrorCount();
						if (n > 0)
						{
						  for(int j = 0;j < n;j++)
						  {
							//tError = tErrors.getError(j);
							System.out.println(  +j+". "+ tError.getError(j).errorMessage.trim()+".");
						  }
						}

						FlagStr = "Succ";
					}
					else
					{
						int n = tError.getErrorCount();
						if (n > 0)
						{
						  for(int j = 0;j < n;j++)
						  {
							//tError = tErrors.getError(j);
							System.out.println( +j+". "+ tError.getError(j).errorMessage.trim()+".");
						  }
							}
						FlagStr = "Fail";
					}
				}

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
    }

	private void InsertDataToLDPerson()throws Exception
	{
		PreparedStatement pLDPerson = conn.prepareStatement(SQL_LDPerson);
		try
		{
			System.out.println("========开始导入LDPerson表数据================");

			pLDPerson.setString(1,Customerno);                    //客户号码
			pLDPerson.setString(2,AppntName);                     //客户姓名
			pLDPerson.setString(3,AppntSex);                      //客户性别
			pLDPerson.setDate(4,AppntBirthday.valueOf("1980-07-19"));  //客户出生日期
			pLDPerson.setString(5,"4");                           //客户证件类别
			pLDPerson.setString(6,"123456789");                   //Idno
			pLDPerson.setString(7, "1"); //婚姻状况      婚姻:	1
			pLDPerson.setString(8, "0"); //身高    0
			pLDPerson.setString(9, "0"); //体重    0
			pLDPerson.setString(10, "0"); //薪水    0
			//pLDPerson.setString(15, "1"); //职业类别
			pLDPerson.setString(11, "06405"); //职业代码


		   pLDPerson.setString(12, "001"); //操作员

		   pLDPerson.setDate(13, date); //MakeDate
		   pLDPerson.setString(14, time.toString()); //MakeTime
		   pLDPerson.setDate(15, date); //ModifyDate
		   pLDPerson.setString(16, time.toString()); //ModifyTime

			pLDPerson.executeUpdate();

		   System.out.println("========LDPerson表数据导入成功!=================");

		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
			conn.rollback();
			System.out.println("Error " + ex.toString());

		}

	}

    private void InsertDataToLCAddress()throws Exception
	{
		PreparedStatement pLCAddress = conn.prepareStatement(SQL_LCAddress);
		try
		{
			System.out.println("========开始导入LCAddress表数据================");

			pLCAddress.setString(1,Customerno);                    //客户号码
			pLCAddress.setString(2,"1");                           //地址号码
			pLCAddress.setString(3,"北京市海淀区中关村大街"+AppntName.subSequence(AppntName.lastIndexOf("-"),AppntName.length())+"号");   //PostalAddress

		   pLCAddress.setString(4, "001"); //操作员

		   pLCAddress.setDate(5, date); //MakeDate
		   pLCAddress.setString(6, time.toString()); //MakeTime
		   pLCAddress.setDate(7, date); //ModifyDate
		   pLCAddress.setString(8, time.toString()); //ModifyTime

			pLCAddress.executeUpdate();

		   System.out.println("========LCAddress表数据导入成功!=================");

		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
			conn.rollback();
			System.out.println("Error " + ex.toString());

		}
	}

	private void InsertDataToLCAppnt()throws Exception
	{

		PreparedStatement pLCAppnt = conn.prepareStatement(SQL_LCAppnt);
		try
		{
			System.out.println("========开始导入LCAppnt表数据================");

			pLCAppnt.setString(1,"00000000000000000000");
            pLCAppnt.setString(2, Contno);
            pLCAppnt.setString(3, Prtno);
            pLCAppnt.setString(4, AppntNo);
            pLCAppnt.setString(5, AppntName);
            pLCAppnt.setString(6, AppntSex);
            pLCAppnt.setDate(7, AppntBirthday.valueOf("1980-07-19"));
            pLCAppnt.setString(8, "1");
            pLCAppnt.setString(9, "4"); //证件类型
            pLCAppnt.setString(10, "123456789"); //证件号码
            pLCAppnt.setString(11, "1"); //婚姻状况      婚姻:	1
            pLCAppnt.setString(12, "0"); //身高    0
            pLCAppnt.setString(13, "0"); //体重    0
            pLCAppnt.setString(14, "0"); //薪水    0
            pLCAppnt.setString(15, "1"); //职业类别
            pLCAppnt.setString(16, "06405"); //职业代码
            pLCAppnt.setString(17, "001"); //操作员
            pLCAppnt.setString(18, "86110000"); //管理机构:	86110000
            pLCAppnt.setDate(19, date); //MakeDate
            pLCAppnt.setString(20, time.toString()); //MakeTime
            pLCAppnt.setDate(21, date); //ModifyDate
            pLCAppnt.setString(22, time.toString()); //ModifyTime
            pLCAppnt.setString(23, "0"); //Bmi

            pLCAppnt.executeUpdate();

			System.out.println("========LCAppnt表数据导入成功!=================");

		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
			conn.rollback();
			System.out.println("Error " + ex.toString());

		}
	}

	private void InsertDataToLCInsured(int InsuredCount) throws Exception
	{

        PreparedStatement pLCInsured = conn.prepareStatement(SQL_LCInsured);
		//由于现在有两个被保人,在插入LCInsured之前,应先判断第一被保人和第二被保人不同的字段
		if(InsuredCount==1)
		{
			//第一被保人
			Insuredno = PubFun1.CreateMaxNo("CUSTOMERNO","SN");   //每创建一个被保人,将重新创建一个Insuredno
			Insuredno1 = Insuredno;    //储存第一被保人的标号 后面LCPol要用
			pLCInsured.setString(8,"00");                              //与主被保人关系  与第一被保险人关系:	00
			pLCInsured.setString(9,"04");                              //与投保人关系    与投保人关系:	04
			pLCInsured.setString(11,"1");                              //客户内部号码    第一被保人 1 第二被保人 2
			InsuredName="被保人"+AppntName.subSequence(AppntName.lastIndexOf("-"),AppntName.length())+"-A";
			InsuredNameA =InsuredName;
			pLCInsured.setString(13,"0");                              //被保人性别
			pLCInsured.setString(16,"12345");                           //证件号码
			pLCInsured.setString(22,"05001");                          //职业代码

			pLCInsured.setDate(14,InsuredBirthday.valueOf("1949-07-19"));     //被保人出生日期

		}
		if(InsuredCount==2)
		{
			//第二被保人
			Insuredno = PubFun1.CreateMaxNo("CUSTOMERNO","SN");
			Insuredno2 = Insuredno;   //储存第二被保人的标号
			pLCInsured.setString(8,"01");                              //与主被保人关系  与第一被保险人关系:	00
			pLCInsured.setString(9,"05");                              //与投保人关系    与投保人关系:	04
			pLCInsured.setString(11,"2");                              //客户内部号码    第一被保人 1 第二被保人 2
			InsuredName="被保人"+AppntName.subSequence(AppntName.lastIndexOf("-"),AppntName.length())+"-B";
			InsuredNameB =InsuredName;
			pLCInsured.setString(13,"1");                               //被保人性别
			pLCInsured.setString(16,"456789");                          //证件号码
			pLCInsured.setString(22,"05902");                           //职业代码
			pLCInsured.setDate(14,InsuredBirthday.valueOf("1950-07-19"));                 //被保人出生日期    先使用当前日期


		}
		try
		{

			System.out.println("========开始导入LCInsured表数据================");

			pLCInsured.setString(1,"00000000000000000000");
			pLCInsured.setString(2,Contno);
			pLCInsured.setString(3,Insuredno);
			pLCInsured.setString(4,Prtno);
			pLCInsured.setString(5,AppntNo);
			pLCInsured.setString(6,"86110000");                        //管理机构
			pLCInsured.setString(7,"86110000");                        //处理机构


			pLCInsured.setString(10,"1");                              //客户地址号码

			pLCInsured.setString(12,InsuredName);                      //


			pLCInsured.setString(15,"4");                              //证件类型

			pLCInsured.setString(17,"1");                              //Marriage
			pLCInsured.setString(18,"0");                              //身高    0
			pLCInsured.setString(19,"0");                              //体重    0
			pLCInsured.setString(20,"0");                              //薪水    0
			pLCInsured.setString(21,"1");                              //职业类别   金融管理人员-金融业

			pLCInsured.setString(23,"001");                            //操作员
			pLCInsured.setDate(24, date);                              //MakeDate
			pLCInsured.setString(25, time.toString());                 //MakeTime
			pLCInsured.setDate(26, date);                              //ModifyDate
			pLCInsured.setString(27, time.toString());                 //ModifyTime
			pLCInsured.setString(28, "0");                             //Bmi
            pLCInsured.setString(29,"1");                              //INSUREDPEOPLES

			pLCInsured.setString(30,"0000000130");                     //FamilyID  2005-4-21新增加的

			pLCInsured.executeUpdate();
			System.out.println("=======LCInsured表数据导入成功!=================");


		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
			conn.rollback();
			System.out.println("Error " + ex.toString());

		}
	}

	private void InsertDataToLCCont()throws Exception
	{
		PreparedStatement pLCCont = conn.prepareStatement(SQL_LCCont);
		try
		{

			System.out.println("========开始导入LCCont表数据================");

			pLCCont.setString(1,"00000000000000000000");
			pLCCont.setString(2,Contno);
			pLCCont.setString(3,Contno);                               //总单投保单号码 和Contno 相同
			pLCCont.setString(4,Prtno);
			pLCCont.setString(5,"1");                                  //ContType
			pLCCont.setString(6,"1");                                  //FamilyType
			pLCCont.setString(7,"0");                                  //PolType
			pLCCont.setString(8,"0");                                  //CardFlag
			pLCCont.setString(9,"86110000");                           //管理机构
			pLCCont.setString(10,"86110000");                          //处理机构
			pLCCont.setString(11,"8611000152");                        //代理人编码  8611000152
			pLCCont.setString(12,"000000000160");                      //代理人组别:	000000000160
			pLCCont.setString(13,"02");                                //销售渠道:	02
			pLCCont.setString(14,AppntNo);                             //投保人客户号码
			pLCCont.setString(15,AppntName);
			pLCCont.setString(16,AppntSex);
			pLCCont.setDate(17,AppntBirthday.valueOf("1980-07-19"));
			pLCCont.setString(18,"4");                                  //投保人证件类型
			pLCCont.setString(19,"123456789");                          //投保人证件号码
			pLCCont.setString(20,Insuredno);
			pLCCont.setString(21,InsuredName);
			pLCCont.setString(22,InsuredSex);
			pLCCont.setDate(23,InsuredBirthday.valueOf("1949-07-19"));
			pLCCont.setString(24,InsuredIDType);
			pLCCont.setString(25,InsuredIDNo);
			pLCCont.setString(26,"0");                                   //PayIntv
			pLCCont.setString(27,"86110000");                            //签单机构
			pLCCont.setInt(28,0);                                        //PrintCount
			pLCCont.setInt(29,0);                                        //LostTimes
			pLCCont.setInt(30,2);                                        //Peoples
			pLCCont.setDouble(31,3);                                     //MULT
			pLCCont.setDouble(32,Prem);                                   //Prem
			pLCCont.setDouble(33,Amnt);                                 //Amnt
			pLCCont.setDouble(34,SumPrem);                                     //SumPrem
			pLCCont.setDouble(35,0);                                     //dif
			pLCCont.setString(36,"001");                                 //Input操作员
			pLCCont.setDate(37,date);                                    //INPUTDATE
			pLCCont.setString(38,time.toString());                       //INPUTTIME
			pLCCont.setString(39,"0");                                   //APPROVEFLAg
			pLCCont.setString(40,"0");                                   //UWFLAG
			pLCCont.setString(41,"0");                                   //APPFLAG
			pLCCont.setString(42,"001");                                 //操作员
			pLCCont.setDate(43, date);                                   //MakeDate
			pLCCont.setString(44, time.toString());                      //MakeTime
			pLCCont.setDate(45, date);                                   //ModifyDate
			pLCCont.setString(46, time.toString());                      //ModifyTime

			//pLCCont.setString(47,"03");                                  //PROPOSALTYPE
			pLCCont.setString(47,"0000000130");                          //FamilyID


			pLCCont.executeUpdate();
			System.out.println("=======LCCont表数据导入成功!=================");

		}
		catch (Exception ex)
		{
			System.out.println(ex.getMessage());
			conn.rollback();
			System.out.println("Error " + ex.toString());

		}
	}

	private void InsertDataToLCPol(int PolCount)throws Exception
	{
		PreparedStatement pLCPol = conn.prepareStatement(SQL_LCPol);
		try
		{
			//险种一
			if(PolCount==1)
			{
				//重新生成Polno
				String tLimit = PubFun.getNoLimit("86110000");
				PolNo=PubFun1.CreateMaxNo("ProposalNo", tLimit);

			    System.out.println(PolNo);
				pLCPol.setString(11,"IC11001");                                //险种编码:	IC11001
				Insuredno = Insuredno1;
				InsuredName =InsuredNameA;
				InsuredSex ="0";
				InsuredBirthday =InsuredBirthday.valueOf("1949-07-19");
				InsuredAppAge="55";
				pLCPol.setDouble(43,1);                                         //FloatRate
				pLCPol.setInt(33,0);                                            //PayEndYear
				pLCPol.setString(37,"N");                                       //SpecifyValiDate
				pLCPol.setDouble(44,1);                                         //MULT
				Prem=357;
				Amnt=55600;
				pLCPol.setString(71,null);                                   //PayEndYearFlag 2005-4-21增加


			}
			//险种二
			if(PolCount==2)
			{
				//重新生成Polno
				String tLimit = PubFun.getNoLimit("86110000");
				PolNo=PubFun1.CreateMaxNo("ProposalNo", tLimit);

				System.out.println(PolNo);
				pLCPol.setString(11,"IC11002");                                //险种编码:	IC11002
				Insuredno=Insuredno2;
				InsuredName =InsuredNameB;
				InsuredSex ="1";
				InsuredBirthday =InsuredBirthday.valueOf("1950-07-19");
				InsuredAppAge="54";
				pLCPol.setDouble(43,0);                                         //FloatRate
				pLCPol.setInt(33,1);                                            //PayEndYear
				pLCPol.setString(37,"Y");                                       //SpecifyValiDate
				pLCPol.setDouble(44,2);                                         //MULT
				Prem=2081;
				Amnt=32700;
				pLCPol.setString(71,"Y");                                   //PayEndYearFlag 2005-4-21增加


			}
			System.out.println("========开始导入LCPol表数据================");

			pLCPol.setString(1,"00000000000000000000");
			pLCPol.setString(2,"00000000000000000000");                   //GRPPOLNO
			pLCPol.setString(3,Contno);
			pLCPol.setString(4,PolNo);
			pLCPol.setString(5,PolNo);                                     //ProposalNo 和PolNo相同
			pLCPol.setString(6,Prtno);
			pLCPol.setString(7,"1");                                       //CONTTYPE
			pLCPol.setString(8,"0");                                       //POLTYPEFLAG
			pLCPol.setString(9,PolNo);                                     //MainPolNo
			pLCPol.setString(10,"DX");                                      //KINDCODE

			pLCPol.setString(12,"2002");                                   //RISKVERSION [需对应险种编码]
			pLCPol.setString(13,"86110000");                               //管理机构
			pLCPol.setString(14,"861100");                                 //代理机构
			pLCPol.setString(15,"8611000152");                             //代理人编码  8611000152
			pLCPol.setString(16,"000000000160");                           //代理人组别:	000000000160
			pLCPol.setString(17,"02");                                     //销售渠道:	02
			pLCPol.setString(18,Insuredno);
			pLCPol.setString(19,InsuredName);
			pLCPol.setString(20,InsuredSex);
			pLCPol.setDate(21,InsuredBirthday);
			pLCPol.setString(22,InsuredAppAge);
			pLCPol.setString(23,"1");                                       //INSUREDPEOPLES
			pLCPol.setString(24,"1");                                       //被保人职业类别/工种编码 OccupationType
			pLCPol.setString(25,AppntNo);
			pLCPol.setString(26,AppntName);
			pLCPol.setDate(27,CValiDate.valueOf("2005-04-18"));                                   //险种生效日期
			pLCPol.setDate(28,PayEndDate.valueOf("2006-04-18"));
			pLCPol.setDate(29,date.valueOf("2005-04-18"));                                        //起领日期
			pLCPol.setDate(30,date.valueOf("2006-04-18"));                                        //保险责任终止日期
			pLCPol.setString(31,"Y");                                       //GetYearFlag
			pLCPol.setInt(32,0);                                            //GetYear

			pLCPol.setString(34,"Y");                                       //InsuYearFlag
			pLCPol.setInt(35,1);                                            //InsuYear
			pLCPol.setInt(36,0);                                            //AcciYear

			pLCPol.setString(38,"1");                                       //PayLocation
			pLCPol.setInt(39,0);                                            //PayIntv
			pLCPol.setInt(40,1);                                            //PayYears
			pLCPol.setInt(41,1);                                            //Years
			pLCPol.setFloat(42,0);                                          //ManageFeeRate


			pLCPol.setDouble(45,Prem);                                      //StandPrem 和Prem 相同
			pLCPol.setDouble(46,Prem);                                      //Prem
			pLCPol.setDouble(47,SumPrem);                                   //SumPrem
			pLCPol.setDouble(48,Amnt);                                      //Amnt
			pLCPol.setDouble(49,Amnt);                                      //RiskAmnt
			pLCPol.setInt(50,0);                                            //LeavingMoney
			pLCPol.setInt(51,0);                                            //EndorseTimes
			pLCPol.setInt(52,0);                                            //ClaimTimes
			pLCPol.setInt(53,0);                                            //LiveTimes
			pLCPol.setInt(54,0);                                            //RenewCount
			pLCPol.setInt(55,-1);                                           //RnewFlag
			pLCPol.setString(56,"0");                                       //AutoPayFlag
			pLCPol.setString(57,"0");                                       //BnfFlag
			pLCPol.setString(58,"N");                                       //HealthCheckFlag
			pLCPol.setString(59,"0");                                       //ImpartFlag
			pLCPol.setString(60,"0");                                       //ApproveFlag
			pLCPol.setString(61,"0");                                       //UWFlag
			pLCPol.setDate(62,date);                                        //PolApplyDate
			pLCPol.setString(63,"0");                                       //AppFlag
			pLCPol.setString(64,"1");                                       //StandbyFlag1
			pLCPol.setString(65,"001");                                 //操作员
			pLCPol.setDate(66, date);                                   //MakeDate
			pLCPol.setString(67, time.toString());                      //MakeTime
			pLCPol.setDate(68, date);                                   //ModifyDate
			pLCPol.setString(69, time.toString());                      //ModifyTime

			pLCPol.setInt(70,0);                                        //WaitPeriod




			pLCPol.executeUpdate();
			System.out.println("=======LCPol表数据导入成功!=================");

		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
			conn.rollback();
			System.out.println("Error " + ex.toString());

		}
	}


    private void WorkFlowOperate()throws Exception
	{
		try
		{
			//输出参数
            CErrors tError = null;

            String FlagStr = "";

            String wFlag = "";
            String CloseBase = "NO";

            GlobalInput tG = new GlobalInput();

            tG.Operator = "001";
            tG.ComCode = "86";
            tG.ManageCom = "86";

            //查找相应的工作流MissionID和SubMissionID
            LWMissionSet tLWMissionSet = new LWMissionSet();
            LWMissionSchema tLWMissionSchema = new LWMissionSchema();
            LWMissionDB tLWMissionDB = new LWMissionDB();
            tLWMissionDB.setActivityID("0000001098");
            tLWMissionDB.setProcessID("0000000003");
            tLWMissionDB.setMissionProp1(Prtno);
            tLWMissionSet = tLWMissionDB.query();

			if (tLWMissionSet.size() == 0)
			{
				System.out.print("没有查询到数据,请检查传入的参数!");
            }

			 tLWMissionSchema = tLWMissionSet.get(1);






            VData tVData = new VData();
            //工作流操作型别，根据此值检索活动ID，取出服务类执行具体业务逻辑
            wFlag = "0000001098";
            TransferData mTransferData = new TransferData();
            mTransferData.setNameAndValue("ContNo", Contno);
            mTransferData.setNameAndValue("PrtNo", Prtno);
            mTransferData.setNameAndValue("AppntNo", AppntNo);
            mTransferData.setNameAndValue("AppntName", AppntName);
            mTransferData.setNameAndValue("AgentCode", "8611000152");
            mTransferData.setNameAndValue("ManageCom", "86110000");
            mTransferData.setNameAndValue("Operator", tG.Operator);
            mTransferData.setNameAndValue("MakeDate", PubFun.getCurrentDate());
            mTransferData.setNameAndValue("MissionID", tLWMissionSchema.getMissionID());
            mTransferData.setNameAndValue("SubMissionID", tLWMissionSchema.getSubMissionID());

            System.out.println(Prtno);
            tVData.add(mTransferData);
            tVData.add(tG);
            System.out.println("wFlag=" + wFlag);
            System.out.println("-------------------start workflow---------------------");
            TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
            if (tTbWorkFlowUI.submitData(tVData, wFlag) == false)
            {
                System.out.println( " 录入确认失败，原因是: " + tTbWorkFlowUI.mErrors.getError(0).errorMessage);
                FlagStr = "Fail";
            }
            else
            {
                CloseBase = "YES";
                System.out.println( " 录入成功！");
                FlagStr = "Succ";
            }
            System.out.println("-------------------end workflow---------------------");

		}
		catch(Exception ex)
		{
			ex.getStackTrace();
		}
    }
	private  void InsertData() throws Exception
    {
		try
		{
			InsertDataToLCAppnt();
			InsertDataToLCAddress();
			InsertDataToLDPerson();
			InsertDataToLCCont();
			for(int i=1;i<=2;i++)
			{
				InsertDataToLCInsured(i);
				InsertDataToLCPol(i);
			}




		}
		catch(SQLException ex)
			{

			System.out.println(ex.getNextException());
	        conn.rollback();
	        System.out.println("Error " + ex.toString());

		}

    }

}
