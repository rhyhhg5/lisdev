/*
 * <p>ClassName: OLFDBConfigBLS </p>
 * <p>Description: LFDBConfigBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2004-06-29
 */
package com.sinosoft.lis.onetable;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;

 public class OLFDBConfigBLS {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
 /** 数据操作字符串 */
private String mOperate;
public OLFDBConfigBLS() {
	}
public static void main(String[] args) {
}
 /**
 传输数据的公共方法
*/
public boolean submitData(VData cInputData,String cOperate)
{
     boolean tReturn =false;
     //将操作数据拷贝到本类中
     this.mOperate =cOperate;

    System.out.println("Start LFDBConfigBLS Submit...");
    if(this.mOperate.equals("INSERT||MAIN"))
    {tReturn=saveLFDBConfig(cInputData);}
    if (this.mOperate.equals("DELETE||MAIN"))
    {tReturn=deleteLFDBConfig(cInputData);}
    if (this.mOperate.equals("UPDATE||MAIN"))
    {tReturn=updateLFDBConfig(cInputData);}
    if (tReturn)
        System.out.println(" sucessful");
    else
        System.out.println("Save failed") ;
        System.out.println("End LFDBConfigBLS Submit...");
  return tReturn;
}
 /**
* 保存函数
*/
private boolean saveLFDBConfig(VData mInputData)
{
  boolean tReturn =true;
  System.out.println("Start Save...");
  Connection conn;
  conn=null;
  conn=DBConnPool.getConnection();
  if (conn==null)
  {
  		// @@错误处理
 		CError tError = new CError();
           tError.moduleName = "LFDBConfigBLS";
           tError.functionName = "saveData";
           tError.errorMessage = "数据库连接失败!";
           this.mErrors .addOneError(tError) ;
	        return false;
   }
	try{
 		conn.setAutoCommit(false);
		System.out.println("Start 保存...");
           LFDBConfigDB tLFDBConfigDB=new LFDBConfigDB(conn);
           tLFDBConfigDB.setSchema((LFDBConfigSchema)mInputData.getObjectByObjectName("LFDBConfigSchema",0));
           if (!tLFDBConfigDB.insert())
           {
		// @@错误处理
	   	this.mErrors.copyAllErrors(tLFDBConfigDB.mErrors);
		CError tError = new CError();
           tError.moduleName = "LFDBConfigBLS";
           tError.functionName = "saveData";
           tError.errorMessage = "数据保存失败!";
           this.mErrors .addOneError(tError) ;
           conn.rollback();
           return false;
}
           conn.commit() ;
}
           catch (Exception ex)
           {
           // @@错误处理
               CError tError =new CError();
               tError.moduleName="LFDBConfigBLS";
               tError.functionName="submitData";
               tError.errorMessage=ex.toString();
               this.mErrors .addOneError(tError);
               tReturn=false;
               try{conn.rollback() ;} catch(Exception e){}
	       }
               return tReturn;
   }
    /**
    * 保存函数
    */
    private boolean deleteLFDBConfig(VData mInputData)
    {
        boolean tReturn =true;
        System.out.println("Start Save...");
        Connection conn;
        conn=null;
        conn=DBConnPool.getConnection();
        if (conn==null)
        {
		// @@错误处理
		CError tError = new CError();
           tError.moduleName = "LFDBConfigBLS";
           tError.functionName = "saveData";
           tError.errorMessage = "数据库连接失败!";
           this.mErrors .addOneError(tError) ;
           return false;
        }
        try{
           conn.setAutoCommit(false);
           System.out.println("Start 保存...");
           LFDBConfigDB tLFDBConfigDB=new LFDBConfigDB(conn);
           tLFDBConfigDB.setSchema((LFDBConfigSchema)mInputData.getObjectByObjectName("LFDBConfigSchema",0));
           if (!tLFDBConfigDB.delete())
           {
		// @@错误处理
		    this.mErrors.copyAllErrors(tLFDBConfigDB.mErrors);
 		    CError tError = new CError();
		    tError.moduleName = "LFDBConfigBLS";
		    tError.functionName = "saveData";
		    tError.errorMessage = "数据删除失败!";
		    this.mErrors .addOneError(tError) ;
               conn.rollback();
               return false;
           }
               conn.commit() ;
         }
       catch (Exception ex)
       {
      // @@错误处理
          CError tError =new CError();
          tError.moduleName="LFDBConfigBLS";
          tError.functionName="submitData";
          tError.errorMessage=ex.toString();
          this.mErrors .addOneError(tError);
          tReturn=false;
          try{conn.rollback() ;} catch(Exception e){}
         }
         return tReturn;
}
/**
  * 保存函数
*/
private boolean updateLFDBConfig(VData mInputData)
{
     boolean tReturn =true;
     System.out.println("Start Save...");
     Connection conn;
     conn=null;
     conn=DBConnPool.getConnection();
     if (conn==null)
     {
	     CError tError = new CError();
        tError.moduleName = "LFDBConfigBLS";
        tError.functionName = "updateData";
        tError.errorMessage = "数据库连接失败!";
        this.mErrors .addOneError(tError) ;
        return false;
     }
     try{
           conn.setAutoCommit(false);
           System.out.println("Start 保存...");
           LFDBConfigDB tLFDBConfigDB=new LFDBConfigDB(conn);
		tLFDBConfigDB.setSchema((LFDBConfigSchema)mInputData.getObjectByObjectName("LFDBConfigSchema",0));
           if (!tLFDBConfigDB.update())
           {
	          // @@错误处理
	         this.mErrors.copyAllErrors(tLFDBConfigDB.mErrors);
	         CError tError = new CError();
	         tError.moduleName = "LFDBConfigBLS";
	         tError.functionName = "saveData";
            tError.errorMessage = "数据保存失败!";
            this.mErrors .addOneError(tError) ;
            conn.rollback();
            return false;
            }
            conn.commit() ;
       }
       catch (Exception ex)
       {
       // @@错误处理
               CError tError =new CError();
               tError.moduleName="LFDBConfigBLS";
               tError.functionName="submitData";
               tError.errorMessage=ex.toString();
               this.mErrors .addOneError(tError);
               tReturn=false;
               try{conn.rollback() ;} catch(Exception e){}
     }
               return tReturn;
     }
}
