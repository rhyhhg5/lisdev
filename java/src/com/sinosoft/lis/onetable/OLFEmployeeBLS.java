/*
 * <p>ClassName: OLFEmployeeBLS </p>
 * <p>Description: LFEmployeeBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: RENLI
 * @CreateDate：2004-06-08
 */
package com.sinosoft.lis.onetable;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;

 public class OLFEmployeeBLS {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
 /** 数据操作字符串 */
private String mOperate;
public OLFEmployeeBLS() {
	}
public static void main(String[] args) {
}
 /**
 传输数据的公共方法
*/
public boolean submitData(VData cInputData,String cOperate)
{
     boolean tReturn =false;
     //将操作数据拷贝到本类中
     this.mOperate =cOperate;

    System.out.println("Start LFEmployeeBLS Submit...");
    if(this.mOperate.equals("INSERT||MAIN"))
    {tReturn=saveLFEmployee(cInputData);}
    if (this.mOperate.equals("DELETE||MAIN"))
    {tReturn=deleteLFEmployee(cInputData);}
    if (this.mOperate.equals("UPDATE||MAIN"))
    {tReturn=updateLFEmployee(cInputData);}
    if (tReturn)
        System.out.println(" sucessful");
    else
        System.out.println("Save failed") ;
        System.out.println("End LFEmployeeBLS Submit...");
  return tReturn;
}
 /**
* 保存函数
*/
private boolean saveLFEmployee(VData mInputData)
{
  boolean tReturn =true;
  System.out.println("Start Save...");
  Connection conn;
  conn=null;
  conn=DBConnPool.getConnection();
  if (conn==null)
  {
  		// @@错误处理
 		CError tError = new CError();
           tError.moduleName = "LFEmployeeBLS";
           tError.functionName = "saveData";
           tError.errorMessage = "数据库连接失败!";
           this.mErrors .addOneError(tError) ;
	        return false;
   }
	try{
 		conn.setAutoCommit(false);
		System.out.println("Start 保存...");
           LFEmployeeDB tLFEmployeeDB=new LFEmployeeDB(conn);
           tLFEmployeeDB.setSchema((LFEmployeeSchema)mInputData.getObjectByObjectName("LFEmployeeSchema",0));
           if (!tLFEmployeeDB.insert())
           {
		// @@错误处理
	   	this.mErrors.copyAllErrors(tLFEmployeeDB.mErrors);
		CError tError = new CError();
           tError.moduleName = "LFEmployeeBLS";
           tError.functionName = "saveData";
           tError.errorMessage = "数据保存失败!";
           this.mErrors .addOneError(tError) ;
           conn.rollback();
           return false;
}
           conn.commit() ;
}
           catch (Exception ex)
           {
           // @@错误处理
               CError tError =new CError();
               tError.moduleName="LFEmployeeBLS";
               tError.functionName="submitData";
               tError.errorMessage=ex.toString();
               this.mErrors .addOneError(tError);
               tReturn=false;
               try{conn.rollback() ;} catch(Exception e){}
	       }
               return tReturn;
   }
    /**
    * 保存函数
    */
    private boolean deleteLFEmployee(VData mInputData)
    {
        boolean tReturn =true;
        System.out.println("Start Save...");
        Connection conn;
        conn=null;
        conn=DBConnPool.getConnection();
        if (conn==null)
        {
		// @@错误处理
		CError tError = new CError();
           tError.moduleName = "LFEmployeeBLS";
           tError.functionName = "saveData";
           tError.errorMessage = "数据库连接失败!";
           this.mErrors .addOneError(tError) ;
           return false;
        }
        try{
           conn.setAutoCommit(false);
           System.out.println("Start 保存...");
           LFEmployeeDB tLFEmployeeDB=new LFEmployeeDB(conn);
           tLFEmployeeDB.setSchema((LFEmployeeSchema)mInputData.getObjectByObjectName("LFEmployeeSchema",0));
           if (!tLFEmployeeDB.delete())
           {
		// @@错误处理
		    this.mErrors.copyAllErrors(tLFEmployeeDB.mErrors);
 		    CError tError = new CError();
		    tError.moduleName = "LFEmployeeBLS";
		    tError.functionName = "saveData";
		    tError.errorMessage = "数据删除失败!";
		    this.mErrors .addOneError(tError) ;
               conn.rollback();
               return false;
           }
               conn.commit() ;
         }
       catch (Exception ex)
       {
      // @@错误处理
          CError tError =new CError();
          tError.moduleName="LFEmployeeBLS";
          tError.functionName="submitData";
          tError.errorMessage=ex.toString();
          this.mErrors .addOneError(tError);
          tReturn=false;
          try{conn.rollback() ;} catch(Exception e){}
         }
         return tReturn;
}
/**
  * 保存函数
*/
private boolean updateLFEmployee(VData mInputData)
{
     boolean tReturn =true;
     System.out.println("Start Save...");
     Connection conn;
     conn=null;
     conn=DBConnPool.getConnection();
     if (conn==null)
     {
	     CError tError = new CError();
        tError.moduleName = "LFEmployeeBLS";
        tError.functionName = "updateData";
        tError.errorMessage = "数据库连接失败!";
        this.mErrors .addOneError(tError) ;
        return false;
     }
     try{
           conn.setAutoCommit(false);
           System.out.println("Start 保存...");
           LFEmployeeDB tLFEmployeeDB=new LFEmployeeDB(conn);
		tLFEmployeeDB.setSchema((LFEmployeeSchema)mInputData.getObjectByObjectName("LFEmployeeSchema",0));
           if (!tLFEmployeeDB.update())
           {
	          // @@错误处理
	         this.mErrors.copyAllErrors(tLFEmployeeDB.mErrors);
	         CError tError = new CError();
	         tError.moduleName = "LFEmployeeBLS";
	         tError.functionName = "saveData";
            tError.errorMessage = "数据保存失败!";
            this.mErrors .addOneError(tError) ;
            conn.rollback();
            return false;
            }
            conn.commit() ;
       }
       catch (Exception ex)
       {
       // @@错误处理
               CError tError =new CError();
               tError.moduleName="LFEmployeeBLS";
               tError.functionName="submitData";
               tError.errorMessage=ex.toString();
               this.mErrors .addOneError(tError);
               tReturn=false;
               try{conn.rollback() ;} catch(Exception e){}
     }
               return tReturn;
     }
}
