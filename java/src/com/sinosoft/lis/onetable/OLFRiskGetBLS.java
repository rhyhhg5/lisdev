/*
 * <p>ClassName: OLFRiskGetBLS </p>
 * <p>Description: LFRiskGetBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: RENLI
 * @CreateDate：2004-06-08
 */
package com.sinosoft.lis.onetable;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;

 public class OLFRiskGetBLS {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
 /** 数据操作字符串 */
private String mOperate;
public OLFRiskGetBLS() {
	}
public static void main(String[] args) {
}
 /**
 传输数据的公共方法
*/
public boolean submitData(VData cInputData,String cOperate)
{
     boolean tReturn =false;
     //将操作数据拷贝到本类中
     this.mOperate =cOperate;

    System.out.println("Start LFRiskGetBLS Submit...");
    if(this.mOperate.equals("INSERT||MAIN"))
    {tReturn=saveLFRiskGet(cInputData);}
    if (this.mOperate.equals("DELETE||MAIN"))
    {tReturn=deleteLFRiskGet(cInputData);}
    if (this.mOperate.equals("UPDATE||MAIN"))
    {tReturn=updateLFRiskGet(cInputData);}
    if (tReturn)
        System.out.println(" sucessful");
    else
        System.out.println("Save failed") ;
        System.out.println("End LFRiskGetBLS Submit...");
  return tReturn;
}
 /**
* 保存函数
*/
private boolean saveLFRiskGet(VData mInputData)
{
  boolean tReturn =true;
  System.out.println("Start Save...");
  Connection conn;
  conn=null;
  conn=DBConnPool.getConnection();
  if (conn==null)
  {
  		// @@错误处理
 		CError tError = new CError();
           tError.moduleName = "LFRiskGetBLS";
           tError.functionName = "saveData";
           tError.errorMessage = "数据库连接失败!";
           this.mErrors .addOneError(tError) ;
	        return false;
   }
	try{
 		conn.setAutoCommit(false);
		System.out.println("Start 保存...");
           LFRiskGetDB tLFRiskGetDB=new LFRiskGetDB(conn);
           tLFRiskGetDB.setSchema((LFRiskGetSchema)mInputData.getObjectByObjectName("LFRiskGetSchema",0));
           if (!tLFRiskGetDB.insert())
           {
		// @@错误处理
	   	this.mErrors.copyAllErrors(tLFRiskGetDB.mErrors);
		CError tError = new CError();
           tError.moduleName = "LFRiskGetBLS";
           tError.functionName = "saveData";
           tError.errorMessage = "数据保存失败!";
           this.mErrors .addOneError(tError) ;
           conn.rollback();
           return false;
}
           conn.commit() ;
}
           catch (Exception ex)
           {
           // @@错误处理
               CError tError =new CError();
               tError.moduleName="LFRiskGetBLS";
               tError.functionName="submitData";
               tError.errorMessage=ex.toString();
               this.mErrors .addOneError(tError);
               tReturn=false;
               try{conn.rollback() ;} catch(Exception e){}
	       }
               return tReturn;
   }
    /**
    * 保存函数
    */
    private boolean deleteLFRiskGet(VData mInputData)
    {
        boolean tReturn =true;
        System.out.println("Start Save...");
        Connection conn;
        conn=null;
        conn=DBConnPool.getConnection();
        if (conn==null)
        {
		// @@错误处理
		CError tError = new CError();
           tError.moduleName = "LFRiskGetBLS";
           tError.functionName = "saveData";
           tError.errorMessage = "数据库连接失败!";
           this.mErrors .addOneError(tError) ;
           return false;
        }
        try{
           conn.setAutoCommit(false);
           System.out.println("Start 保存...");
           LFRiskGetDB tLFRiskGetDB=new LFRiskGetDB(conn);
           tLFRiskGetDB.setSchema((LFRiskGetSchema)mInputData.getObjectByObjectName("LFRiskGetSchema",0));
           if (!tLFRiskGetDB.delete())
           {
		// @@错误处理
		    this.mErrors.copyAllErrors(tLFRiskGetDB.mErrors);
 		    CError tError = new CError();
		    tError.moduleName = "LFRiskGetBLS";
		    tError.functionName = "saveData";
		    tError.errorMessage = "数据删除失败!";
		    this.mErrors .addOneError(tError) ;
               conn.rollback();
               return false;
           }
               conn.commit() ;
         }
       catch (Exception ex)
       {
      // @@错误处理
          CError tError =new CError();
          tError.moduleName="LFRiskGetBLS";
          tError.functionName="submitData";
          tError.errorMessage=ex.toString();
          this.mErrors .addOneError(tError);
          tReturn=false;
          try{conn.rollback() ;} catch(Exception e){}
         }
         return tReturn;
}
/**
  * 保存函数
*/
private boolean updateLFRiskGet(VData mInputData)
{
     boolean tReturn =true;
     System.out.println("Start Save...");
     Connection conn;
     conn=null;
     conn=DBConnPool.getConnection();
     if (conn==null)
     {
	     CError tError = new CError();
        tError.moduleName = "LFRiskGetBLS";
        tError.functionName = "updateData";
        tError.errorMessage = "数据库连接失败!";
        this.mErrors .addOneError(tError) ;
        return false;
     }
     try{
           conn.setAutoCommit(false);
           System.out.println("Start 保存...");
           LFRiskGetDB tLFRiskGetDB=new LFRiskGetDB(conn);
		tLFRiskGetDB.setSchema((LFRiskGetSchema)mInputData.getObjectByObjectName("LFRiskGetSchema",0));
           if (!tLFRiskGetDB.update())
           {
	          // @@错误处理
	         this.mErrors.copyAllErrors(tLFRiskGetDB.mErrors);
	         CError tError = new CError();
	         tError.moduleName = "LFRiskGetBLS";
	         tError.functionName = "saveData";
            tError.errorMessage = "数据保存失败!";
            this.mErrors .addOneError(tError) ;
            conn.rollback();
            return false;
            }
            conn.commit() ;
       }
       catch (Exception ex)
       {
       // @@错误处理
               CError tError =new CError();
               tError.moduleName="LFRiskGetBLS";
               tError.functionName="submitData";
               tError.errorMessage=ex.toString();
               this.mErrors .addOneError(tError);
               tReturn=false;
               try{conn.rollback() ;} catch(Exception e){}
     }
               return tReturn;
     }
}
