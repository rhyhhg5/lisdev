/*
 * <p>ClassName: OLFChargeModeUI </p>
 * <p>Description: OLFChargeModeUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2004-09-13 14:21:34
 */
package com.sinosoft.lis.onetable;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

public class OLFChargeModeUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    //业务处理相关变量

    /** 全局数据 */
    private LFChargeModeSchema mLFChargeModeSchema = new LFChargeModeSchema();

    public OLFChargeModeUI()
    {
    }

    /**
    传输数据的公共方法
    */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        OLFChargeModeBL tOLFChargeModeBL = new OLFChargeModeBL();

        //System.out.println("Start OLFChargeMode UI Submit...");
        tOLFChargeModeBL.submitData(mInputData, mOperate);

        //System.out.println("End OLFChargeMode UI Submit...");
        //如果有需要处理的错误，则返回
        if (tOLFChargeModeBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tOLFChargeModeBL.mErrors);

            CError tError = new CError();
            tError.moduleName = "OLFChargeModeUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        if (mOperate.equals("INSERT||MAIN"))
        {
            this.mResult.clear();
            this.mResult = tOLFChargeModeBL.getResult();
        }
        mInputData = null;

        return true;
    }

    public static void main(String[] args)
    {
        //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
        String transact = "UPDATE||MAIN";
        LFChargeModeSchema tLFChargeModeSchema = new LFChargeModeSchema();
        tLFChargeModeSchema.setFinItemCode("4431010204");
        tLFChargeModeSchema.setRiskCode("112202");
        tLFChargeModeSchema.setRiskName("民生长顺两全保险（分红型）11");

        GlobalInput tG = new GlobalInput();
        tG.Operator = "Admin";
        tG.ComCode = "asd";
        tG.ManageCom = "8611";

        VData tVData = new VData();
        tVData.add(tLFChargeModeSchema);
        tVData.add(tG);

        OLFChargeModeUI tOLFChargeModeUI = new OLFChargeModeUI();
        tOLFChargeModeUI.submitData(tVData, transact);
    }

    /**
    * 准备往后层输出所需要的数据
    * 输出：如果准备数据时发生错误则返回false,否则返回true
    */
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(this.mLFChargeModeSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LFChargeModeUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
      * 如果在处理过程中出错，则返回false,否则返回true
      */
    private boolean dealData()
    {
        boolean tReturn = false;

        //此处增加一些校验代码
        tReturn = true;

        return tReturn;
    }

    /**
    * 从输入数据中得到所有对象
    *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        this.mLFChargeModeSchema.setSchema((LFChargeModeSchema) cInputData
                                           .getObjectByObjectName("LFChargeModeSchema",
                                                                  0));

        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
