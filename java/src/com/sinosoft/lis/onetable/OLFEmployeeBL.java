/*
 * <p>ClassName: OLFEmployeeBL </p>
 * <p>Description: LFEmployeeBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: RENLI
 * @CreateDate：2004-06-08
 */
package com.sinosoft.lis.onetable;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.onetable.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLFEmployeeBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
 /** 往后面传输数据的容器 */
 private VData mInputData;
 /** 全局数据 */
 private GlobalInput mGlobalInput =new GlobalInput() ;
 /** 数据操作字符串 */
 private String mOperate;
/** 业务处理相关变量 */
 private LFEmployeeSchema mLFEmployeeSchema=new LFEmployeeSchema();
 private LFEmployeeSet mLFEmployeeSet=new LFEmployeeSet();
 public OLFEmployeeBL() {
 }
public static void main(String[] args) {
}
/**
* 传输数据的公共方法
	* @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
       //将操作数据拷贝到本类中
       this.mOperate =cOperate;
       //得到外部传入的数据,将数据备份到本类中
       if (!getInputData(cInputData))
            return false;
       //进行业务处理
       if (!dealData())
       {
	       // @@错误处理
          CError tError = new CError();
          tError.moduleName = "LFEmployeeBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败LFEmployeeBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
       }
     //准备往后台的数据
     if (!prepareOutputData())
        return false;
     if (this.mOperate.equals("QUERY||MAIN"))
     {
            this.submitquery();
     }
     else
    {
        System.out.println("Start OLFEmployeeBL Submit...");
        OLFEmployeeBLS tOLFEmployeeBLS=new OLFEmployeeBLS();
        tOLFEmployeeBLS.submitData(mInputData,cOperate);
        System.out.println("End OLFEmployeeBL Submit...");
        //如果有需要处理的错误，则返回
        if (tOLFEmployeeBLS.mErrors.needDealError())
        {
		// @@错误处理
		this.mErrors.copyAllErrors(tOLFEmployeeBLS.mErrors);
		CError tError = new CError();
		tError.moduleName = "OLFEmployeeBL";
		tError.functionName = "submitDat";
		tError.errorMessage ="数据提交失败!";
		this.mErrors .addOneError(tError) ;
		return false;
         }
    }
        mInputData=null;
        return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
         boolean tReturn =true;
      //   this.mLFEmployeeSchema.setModifyDate(getDate());
     //  this.mLFEmployeeSchema.setModifyTime();
         tReturn=true;
         return tReturn ;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
         this.mLFEmployeeSchema.setSchema((LFEmployeeSchema)cInputData.getObjectByObjectName("LFEmployeeSchema",0));
         this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
           this.mResult.clear();
           System.out.println("Start LFEmployeeBLQuery Submit...");
 	        LFEmployeeDB tLFEmployeeDB=new LFEmployeeDB();
	        tLFEmployeeDB.setSchema(this.mLFEmployeeSchema);
	        this.mLFEmployeeSet=tLFEmployeeDB.query();
	        this.mResult.add(this.mLFEmployeeSet);
		System.out.println("End LFEmployeeBLQuery Submit...");
		//如果有需要处理的错误，则返回
		if (tLFEmployeeDB.mErrors.needDealError())
 		{
		// @@错误处理
  			this.mErrors.copyAllErrors(tLFEmployeeDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "LFEmployeeBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors .addOneError(tError) ;
 			return false;
}
                   mInputData=null;
                   return true;
}
 private boolean prepareOutputData()
 {
   try
	{
		this.mInputData=new VData();
		this.mInputData.add(this.mGlobalInput);
		this.mInputData.add(this.mLFEmployeeSchema);
	}
	catch(Exception ex)
	{
 		 // @@错误处理
		CError tError =new CError();
 		tError.moduleName="LFEmployeeBL";
 		tError.functionName="prepareData";
 		tError.errorMessage="在准备往后层处理所需要的数据时出错。";
 		this.mErrors .addOneError(tError) ;
		return false;
	}
	return true;
	}
	public VData getResult()
	{
  		return this.mResult;
	}
}
