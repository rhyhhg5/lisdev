/*
 * <p>ClassName: OLFFinXmlBL </p>
 * <p>Description: LFFinXmlBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: MSREPORT
 * @CreateDate：2004-07-02
 */
package com.sinosoft.lis.onetable;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.onetable.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLFFinXmlBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
 /** 往后面传输数据的容器 */
 private VData mInputData;
 /** 全局数据 */
 private GlobalInput mGlobalInput =new GlobalInput() ;
 /** 数据操作字符串 */
 private String mOperate;
/** 业务处理相关变量 */
 private LFFinXmlSchema mLFFinXmlSchema=new LFFinXmlSchema();
 private LFFinXmlSet mLFFinXmlSet=new LFFinXmlSet();
 public OLFFinXmlBL() {
 }
public static void main(String[] args) {
}
/**
* 传输数据的公共方法
	* @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
       //将操作数据拷贝到本类中
       this.mOperate =cOperate;
       //得到外部传入的数据,将数据备份到本类中
       if (!getInputData(cInputData))
            return false;
       //进行业务处理
       if (!dealData())
       {
	       // @@错误处理
          CError tError = new CError();
          tError.moduleName = "LFFinXmlBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败LFFinXmlBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
       }
     //准备往后台的数据
     if (!prepareOutputData())
        return false;
     if (this.mOperate.equals("QUERY||MAIN"))
     {
            this.submitquery();
     }
     else
    {
        System.out.println("Start OLFFinXmlBL Submit...");
        OLFFinXmlBLS tOLFFinXmlBLS=new OLFFinXmlBLS();
        tOLFFinXmlBLS.submitData(mInputData,cOperate);
        System.out.println("End OLFFinXmlBL Submit...");
        //如果有需要处理的错误，则返回
        if (tOLFFinXmlBLS.mErrors.needDealError())
        {
		// @@错误处理
		this.mErrors.copyAllErrors(tOLFFinXmlBLS.mErrors);
		CError tError = new CError();
		tError.moduleName = "OLFFinXmlBL";
		tError.functionName = "submitDat";
		tError.errorMessage ="数据提交失败!";
		this.mErrors .addOneError(tError) ;
		return false;
         }
    }
        mInputData=null;
        return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
         boolean tReturn =true;
      //   this.mLFFinXmlSchema.setModifyDate(getDate());
     //  this.mLFFinXmlSchema.setModifyTime();
         tReturn=true;
         return tReturn ;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
         this.mLFFinXmlSchema.setSchema((LFFinXmlSchema)cInputData.getObjectByObjectName("LFFinXmlSchema",0));
         this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
           this.mResult.clear();
           System.out.println("Start LFFinXmlBLQuery Submit...");
 	        LFFinXmlDB tLFFinXmlDB=new LFFinXmlDB();
	        tLFFinXmlDB.setSchema(this.mLFFinXmlSchema);
	        this.mLFFinXmlSet=tLFFinXmlDB.query();
	        this.mResult.add(this.mLFFinXmlSet);
		System.out.println("End LFFinXmlBLQuery Submit...");
		//如果有需要处理的错误，则返回
		if (tLFFinXmlDB.mErrors.needDealError())
 		{
		// @@错误处理
  			this.mErrors.copyAllErrors(tLFFinXmlDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "LFFinXmlBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors .addOneError(tError) ;
 			return false;
}
                   mInputData=null;
                   return true;
}
 private boolean prepareOutputData()
 {
   try
	{
		this.mInputData=new VData();
		this.mInputData.add(this.mGlobalInput);
		this.mInputData.add(this.mLFFinXmlSchema);
	}
	catch(Exception ex)
	{
 		 // @@错误处理
		CError tError =new CError();
 		tError.moduleName="LFFinXmlBL";
 		tError.functionName="prepareData";
 		tError.errorMessage="在准备往后层处理所需要的数据时出错。";
 		this.mErrors .addOneError(tError) ;
		return false;
	}
	return true;
	}
	public VData getResult()
	{
  		return this.mResult;
	}
}
