/*
 * <p>ClassName: OLDOperatorInsideBL </p>
 * <p>Description: OLDOperatorInsideBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-08-23 14:52:31
 */
package com.sinosoft.lis.config;

import com.sinosoft.lis.db.LDOperatorInsideDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LDOperatorInsideSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;

public class OLDOperatorInsideBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LDOperatorInsideSchema mLDOperatorInsideSchema = new
            LDOperatorInsideSchema();
    /** 数据递交map */
    private MMap map = new MMap();
//private LDOperatorInsideSet mLDOperatorInsideSet=new LDOperatorInsideSet();
    public OLDOperatorInsideBL() {
    }

    public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start OLDOperatorInsideBL Submit...");
            PubSubmit ps = new PubSubmit();
            if (!ps.submitData(this.mResult, null)) {
                this.mErrors.copyAllErrors(ps.mErrors);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        if (this.mOperate.equals("INSERT||MAIN")) {
            if (!insertData()) {
                return false;
            }
        }
        if (this.mOperate.equals("UPDATE||MAIN")) {
            if (!updateData()) {
                return false;
            }
        }
        if (this.mOperate.equals("DELETE||MAIN")) {
            if (!deleteData()) {
                return false;
            }
        }

        return true;
    }

    /**
     * insertData
     *
     * @return boolean
     */
    private boolean insertData() {
        if (mLDOperatorInsideSchema.getUserCode() == null ||
            mLDOperatorInsideSchema.getUserCode().equals("")) {
            mLDOperatorInsideSchema.setUserCode(PubFun1.CreateMaxNo("USERCODE",
                    10));
        }
        this.map.put(mLDOperatorInsideSchema, "INSERT");
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        this.map.put(mLDOperatorInsideSchema, "DELETE&INSERT");
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        this.map.put(mLDOperatorInsideSchema, "DELETE");
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLDOperatorInsideSchema.setSchema((LDOperatorInsideSchema)
                                               cInputData.getObjectByObjectName(
                "LDOperatorInsideSchema", 0));
        //this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LDOperatorInsideDB tLDOperatorInsideDB = new LDOperatorInsideDB();
        tLDOperatorInsideDB.setSchema(this.mLDOperatorInsideSchema);
        //如果有需要处理的错误，则返回
        if (tLDOperatorInsideDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLDOperatorInsideDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDOperatorInsideBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLDOperatorInsideSchema);
            mResult.clear();
            mResult.add(this.mLDOperatorInsideSchema);
            mResult.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDOperatorInsideBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
