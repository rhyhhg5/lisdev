/*
 * <p>ClassName: OLDOperatorInsideBLS </p>
 * <p>Description: OLDOperatorInsideBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-08-23 14:52:31
 */
package com.sinosoft.lis.config;

import java.sql.Connection;

import com.sinosoft.lis.db.LDOperatorInsideDB;
import com.sinosoft.lis.schema.LDOperatorInsideSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

public class OLDOperatorInsideBLS {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
//传输数据类
    private VData mInputData;
    /** 数据操作字符串 */
    private String mOperate;
    public OLDOperatorInsideBLS() {
    }

    public static void main(String[] args) {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        mInputData = (VData) cInputData.clone();
        if (this.mOperate.equals("INSERT||MAIN")) {
            if (!saveLDOperatorInside()) {
                return false;
            }
        }
        if (this.mOperate.equals("DELETE||MAIN")) {
            if (!deleteLDOperatorInside()) {
                return false;
            }
        }
        if (this.mOperate.equals("UPDATE||MAIN")) {
            if (!updateLDOperatorInside()) {
                return false;
            }
        }
        return true;
    }

    /**
     * 保存函数
     */
    private boolean saveLDOperatorInside() {
        LDOperatorInsideSchema tLDOperatorInsideSchema = new
                LDOperatorInsideSchema();
        tLDOperatorInsideSchema = (LDOperatorInsideSchema) mInputData.
                                  getObjectByObjectName(
                                          "LDOperatorInsideSchema", 0);
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLDOperatorInsideBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            conn.setAutoCommit(false);
            LDOperatorInsideDB tLDOperatorInsideDB = new LDOperatorInsideDB(
                    conn);
            tLDOperatorInsideDB.setSchema(tLDOperatorInsideSchema);
            if (!tLDOperatorInsideDB.insert()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLDOperatorInsideDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLDOperatorInsideBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLDOperatorInsideBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                conn.rollback();
                conn.close();
            } catch (Exception e) {}
            return false;
        }
        return true;
    }

    /**
     * 保存函数
     */
    private boolean deleteLDOperatorInside() {
        LDOperatorInsideSchema tLDOperatorInsideSchema = new
                LDOperatorInsideSchema();
        tLDOperatorInsideSchema = (LDOperatorInsideSchema) mInputData.
                                  getObjectByObjectName(
                                          "LDOperatorInsideSchema", 0);
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLDOperatorInsideBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LDOperatorInsideDB tLDOperatorInsideDB = new LDOperatorInsideDB(
                    conn);
            tLDOperatorInsideDB.setSchema(tLDOperatorInsideSchema);
            if (!tLDOperatorInsideDB.delete()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLDOperatorInsideDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLDOperatorInsideBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLDOperatorInsideBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                conn.rollback();
                conn.close();
            } catch (Exception e) {}
            return false;
        }
        return true;
    }

    /**
     * 保存函数
     */
    private boolean updateLDOperatorInside() {
        LDOperatorInsideSchema tLDOperatorInsideSchema = new
                LDOperatorInsideSchema();
        tLDOperatorInsideSchema = (LDOperatorInsideSchema) mInputData.
                                  getObjectByObjectName(
                                          "LDOperatorInsideSchema", 0);
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null) {
            CError tError = new CError();
            tError.moduleName = "OLDOperatorInsideBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LDOperatorInsideDB tLDOperatorInsideDB = new LDOperatorInsideDB(
                    conn);
            tLDOperatorInsideDB.setSchema(tLDOperatorInsideSchema);
            if (!tLDOperatorInsideDB.update()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLDOperatorInsideDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLDOperatorInsideBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLDOperatorInsideBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                conn.rollback();
                conn.close();
            } catch (Exception e) {}
            return false;
        }
        return true;
    }
}
