/*
 * <p>ClassName: OLDContReceiveUI </p>
 * <p>Description: OLDContReceiveUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-08-25 10:42:49
 */
package com.sinosoft.lis.config;

import com.sinosoft.lis.schema.LDContReceiveSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class OLDContReceiveUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    // 业务处理相关变量
    /** 全局数据 */
    private LDContReceiveSchema mLDContReceiveSchema = new LDContReceiveSchema();
    /** 添加日志对象 */
    private static Log log = LogFactory.getLog(OLDContReceiveUI.class);
    public OLDContReceiveUI() {
    }

    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        // 将操作数据拷贝到本类中
        this.mOperate = cOperate;

        OLDContReceiveBL tOLDContReceiveBL = new OLDContReceiveBL();

        log.debug("Start tOLDContReceiveBL UI Submit...");
        tOLDContReceiveBL.submitData(cInputData, mOperate);
        // System.out.println("End OLDContReceive UI Submit...");
        // 如果有需要处理的错误，则返回
        if (tOLDContReceiveBL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tOLDContReceiveBL.mErrors);
            return false;
        }
        if (mOperate.equals("INSERT||MAIN")) {
            this.mResult.clear();
            this.mResult = tOLDContReceiveBL.getResult();
        }
        mInputData = null;
        return true;
    }

    public static void main(String[] args) {
    }

    /**
     * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(this.mLDContReceiveSchema);
        } catch (Exception ex) {
            // @@错误处理
            String str = "在准备往后层处理所需要的数据时出错。!";
            buildError("prepareOutputData", str);
            log.debug(str);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        boolean tReturn = false;
        // 此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        // 全局变量
        this.mLDContReceiveSchema.setSchema((LDContReceiveSchema) cInputData
                                            .getObjectByObjectName(
                "LDContReceiveSchema", 0));
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "OLDContReceiveUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}
