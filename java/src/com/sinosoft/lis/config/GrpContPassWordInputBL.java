package com.sinosoft.lis.config;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class GrpContPassWordInputBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 往后面传输的数据库操作 */
    private MMap map = new MMap();
    /** 执行删除的数据库操作，放在最后 */
    private MMap mapDel = new MMap();
    /**用户登陆信息 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    private String mGrpContNo = "";

    private String mPassWord = "";

    private String mOperator = "";

    private String mCurrentDate = "";

    private String mCurrentTime = "";

    public GrpContPassWordInputBL() {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     * @param: cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        mInputData = (VData) cInputData.clone();
        mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        System.out.println("after getInputData");

        //数据操作业务处理
        if (!dealData()) {
            return false;
        }

        System.out.println("after dealData");

        //准备给后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        System.out.println("after prepareOutputData");

        //数据提交
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpContPassWordInputBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;

        return true;
    }

    private boolean dealData() {
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        if (!tLCGrpContDB.getInfo()) {
            buildError("dealData", "团体保单查询失败");
            return false;
        }

        LCGrpContSchema tLCGrpContSchema = tLCGrpContDB.getSchema();
        tLCGrpContSchema.setPassword(mPassWord);
        tLCGrpContSchema.setOperator(mOperator);
        tLCGrpContSchema.setModifyDate(mCurrentDate);
        tLCGrpContSchema.setModifyTime(mCurrentTime);

        map.put(tLCGrpContSchema, "UPDATE");
        return true;
    }


    private boolean getInputData(VData cInputData) {
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));

        mLCGrpContSchema.setSchema((LCGrpContSchema) cInputData.
                                   getObjectByObjectName("LCGrpContSchema", 0));

        if (mLCGrpContSchema == null) {
            buildError("getInputData", "获取保单信息失败");
            return false;
        }

        mGrpContNo = mLCGrpContSchema.getGrpContNo();
        if ("".equals(mGrpContNo) || mGrpContNo == null) {
            buildError("getInputData", "获取保单信息失败");
            return false;
        }

        mPassWord = mLCGrpContSchema.getPassword();
        if (mPassWord == null || "".equals(mPassWord)) {
            buildError("getInputData", "获取保单信息失败");
            return false;
        }

        mOperator = mGlobalInput.Operator;
        mCurrentDate = PubFun.getCurrentDate();
        mCurrentTime = PubFun.getCurrentTime();

        return true;
    }

    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            buildError("prepareOutputData", "在准备往后层处理所需要的数据时出错:" +
                       ex.toString());
            return false;
        }
        return true;
    }

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "GrpContPassWordInputBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }


    public VData getResult() {
        return mResult;
    }

    public static void main(String[] args) {
    }
}
