/*
 * <p>ClassName: OLDContReceiveBL </p>
 * <p>Description: OLDContReceiveBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-08-25 10:42:49
 */
package com.sinosoft.lis.config;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LDContReceiveSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class OLDContReceiveBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    // private LDContReceiveSchema mLDContReceiveSchema = new
    // LDContReceiveSchema();
    /** 添加日志 */
    private static Log log = LogFactory.getLog(OLDContReceiveBL.class);
    /** 数据递交对象 */
    private MMap map = new MMap();
    private LDContReceiveSet mLDContReceiveSet = new LDContReceiveSet();

    public OLDContReceiveBL() {
    }

    public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     *
     * @param: cInputData 输入的数据 cOperate 数据操作
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        // 将操作数据拷贝到本类中
        this.mOperate = cOperate;
        // 得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        // 进行业务处理
        if (!dealData()) {
            // @@错误处理
            return false;
        }
        // 准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
        } else {
            log.debug("Start OLDContReceiveBL Submit...");
            PubSubmit ps = new PubSubmit();
            if (!ps.submitData(this.mInputData, null)) {
                this.mErrors.copyAllErrors(ps.mErrors);
                log.debug("数据递交失败");
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean dealData() {
        if (this.mOperate.equals("INSERT||MAIN")) {
            if (!insertData()) {
                return false;
            }
        }
        if (this.mOperate.equals("UPDATE||MAIN")) {
            if (!updateData()) {
                return false;
            }
        }
        if (this.mOperate.equals("DELETE||MAIN")) {
            if (!deleteData()) {
                return false;
            }
        }

        return true;
    }

    /**
     * 执行插入方法
     *
     * @return boolean
     */
    private boolean insertData() {
        if (this.mLDContReceiveSet != null) {
            for (int i = 1; i <= this.mLDContReceiveSet.size(); i++) {
                mLDContReceiveSet.get(i).setOperator(mGlobalInput.Operator);
                PubFun.fillDefaultField(mLDContReceiveSet);
            }
            map.put(mLDContReceiveSet, "INSERT");
        } else {
            String str = "前台没有传入配置信息!";
            buildError("insertData", str);
            log.debug(str);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean updateData() {
        if (this.mLDContReceiveSet != null) {
            for (int i = 1; i <= this.mLDContReceiveSet.size(); i++) {
                mLDContReceiveSet.get(i).setOperator(mGlobalInput.Operator);
                mLDContReceiveSet.get(i).setModifyDate(PubFun.getCurrentDate());
                mLDContReceiveSet.get(i).setModifyTime(PubFun.getCurrentTime());
            }
            log.debug("执行删除插入操作");
            String strSql = "delete from LDContReceive where ManageCom='" +
                            this.mLDContReceiveSet.get(1).getManageCom() + "'";
            this.map.put(strSql, "DELETE");
            this.map.put(mLDContReceiveSet, "INSERT");
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     *
     * @return boolean
     */
    private boolean deleteData() {
        if (mLDContReceiveSet != null && mLDContReceiveSet.size() > 0) {
            String strSql = "delete from LDContReceive where ManageCom='" +
                            this.mLDContReceiveSet.get(1).getManageCom() + "'";
            this.map.put(strSql, "DELETE");
        } else {
            String str = "前台没有传入需要删除的数据!";
            buildError("deleteData", str);
            log.debug(str);
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        this.mLDContReceiveSet.set((LDContReceiveSet) cInputData
                                   .getObjectByObjectName("LDContReceiveSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData
                                    .getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLDContReceiveSet);
            this.mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLDContReceiveSet);
        } catch (Exception ex) {
            // @@错误处理
            String str = "在准备往后层处理所需要的数据时出错。!";
            buildError("prepareOutputData", str);
            log.debug(str);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "OLDContReceiveBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


}
