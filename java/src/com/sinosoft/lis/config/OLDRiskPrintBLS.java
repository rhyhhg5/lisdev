/*
 * <p>ClassName: OLDRiskPrintBLS </p>
 * <p>Description: OLDRiskPrintBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-16 15:29:18
 */
package com.sinosoft.lis.config;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;
 public class OLDRiskPrintBLS {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
//传输数据类
  private VData mInputData ;
 /** 数据操作字符串 */
private String mOperate;
public OLDRiskPrintBLS() {
	}
public static void main(String[] args) {
}
 /**
 传输数据的公共方法
*/
public boolean submitData(VData cInputData,String cOperate)
{
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
	  mInputData=(VData)cInputData.clone();
    if(this.mOperate.equals("INSERT||MAIN"))
    {if (!saveLDRiskPrint())
        return false;
    }
    if (this.mOperate.equals("DELETE||MAIN"))
    {if (!deleteLDRiskPrint())
        return false;
    }
    if (this.mOperate.equals("UPDATE||MAIN"))
    {if (!updateLDRiskPrint())
        return false;
    }
  return true;
}
 /**
* 保存函数
*/
private boolean saveLDRiskPrint()
{
  LDRiskPrintSchema tLDRiskPrintSchema = new LDRiskPrintSchema();
  tLDRiskPrintSchema = (LDRiskPrintSchema)mInputData.getObjectByObjectName("LDRiskPrintSchema",0);
  Connection conn;
  conn=null;
  conn=DBConnPool.getConnection();
  if (conn==null)
  {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OLDRiskPrintBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
   }
	try{
 	  conn.setAutoCommit(false);
    LDRiskPrintDB tLDRiskPrintDB=new LDRiskPrintDB(conn);
    tLDRiskPrintDB.setSchema(tLDRiskPrintSchema);
    if (!tLDRiskPrintDB.insert())
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLDRiskPrintDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "OLDRiskPrintBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据保存失败!";
      this.mErrors .addOneError(tError) ;
      conn.rollback();
      conn.close();
      return false;
    }
    conn.commit() ;
    conn.close();
  }
  catch (Exception ex)
  {
    // @@错误处理
    CError tError =new CError();
    tError.moduleName="OLDRiskPrintBLS";
    tError.functionName="submitData";
    tError.errorMessage=ex.toString();
    this.mErrors .addOneError(tError);
      try{
      conn.rollback() ;
      conn.close();
      }
      catch(Exception e){}
    return false;
	}
  return true;
}
    /**
    * 保存函数
    */
    private boolean deleteLDRiskPrint()
    {
        LDRiskPrintSchema tLDRiskPrintSchema = new LDRiskPrintSchema();
        tLDRiskPrintSchema = (LDRiskPrintSchema)mInputData.getObjectByObjectName("LDRiskPrintSchema",0);
        System.out.println("Start Save...");
        Connection conn;
        conn=null;
        conn=DBConnPool.getConnection();
        if (conn==null)
        {
		// @@错误处理
		CError tError = new CError();
           tError.moduleName = "OLDRiskPrintBLS";
           tError.functionName = "saveData";
           tError.errorMessage = "数据库连接失败!";
           this.mErrors .addOneError(tError) ;
           return false;
        }
        try{
           conn.setAutoCommit(false);
           System.out.println("Start 保存...");
           LDRiskPrintDB tLDRiskPrintDB=new LDRiskPrintDB(conn);
           tLDRiskPrintDB.setSchema(tLDRiskPrintSchema);
           if (!tLDRiskPrintDB.delete())
           {
		// @@错误处理
		    this.mErrors.copyAllErrors(tLDRiskPrintDB.mErrors);
 		    CError tError = new CError();
		    tError.moduleName = "OLDRiskPrintBLS";
		    tError.functionName = "saveData";
		    tError.errorMessage = "数据删除失败!";
		    this.mErrors .addOneError(tError) ;
               conn.rollback();
               conn.close();
               return false;
           }
               conn.commit() ;
               conn.close();
         }
       catch (Exception ex)
       {
      // @@错误处理
          CError tError =new CError();
          tError.moduleName="OLDRiskPrintBLS";
          tError.functionName="submitData";
          tError.errorMessage=ex.toString();
          this.mErrors .addOneError(tError);
          try{conn.rollback() ;
          conn.close();} catch(Exception e){}
         return false;
         }
         return true;
}
/**
  * 保存函数
*/
private boolean updateLDRiskPrint()
{
     LDRiskPrintSchema tLDRiskPrintSchema = new LDRiskPrintSchema();
     tLDRiskPrintSchema = (LDRiskPrintSchema)mInputData.getObjectByObjectName("LDRiskPrintSchema",0);
     System.out.println("Start Save...");
     Connection conn;
     conn=null;
     conn=DBConnPool.getConnection();
     if (conn==null)
     {
	     CError tError = new CError();
        tError.moduleName = "OLDRiskPrintBLS";
        tError.functionName = "updateData";
        tError.errorMessage = "数据库连接失败!";
        this.mErrors .addOneError(tError) ;
        return false;
     }
     try{
           conn.setAutoCommit(false);
           System.out.println("Start 保存...");
           LDRiskPrintDB tLDRiskPrintDB=new LDRiskPrintDB(conn);
	tLDRiskPrintDB.setSchema(tLDRiskPrintSchema);
           if (!tLDRiskPrintDB.update())
           {
	          // @@错误处理
	         this.mErrors.copyAllErrors(tLDRiskPrintDB.mErrors);
	         CError tError = new CError();
	         tError.moduleName = "OLDRiskPrintBLS";
	         tError.functionName = "saveData";
            tError.errorMessage = "数据保存失败!";
            this.mErrors .addOneError(tError) ;
            conn.rollback();
            conn.close();
            return false;
            }
            conn.commit() ;
            conn.close();
       }
       catch (Exception ex)
       {
       // @@错误处理
               CError tError =new CError();
               tError.moduleName="OLDRiskPrintBLS";
               tError.functionName="submitData";
               tError.errorMessage=ex.toString();
               this.mErrors .addOneError(tError);
               try{conn.rollback() ;
               conn.close();} catch(Exception e){}
               return false;
     }
               return true;
     }
}
