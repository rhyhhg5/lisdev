package com.sinosoft.lis.config;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:被保险人资料变更功能类
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Tjj
 * @version 1.0
 */
public class E_RenewalConfUI
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 往界面传输数据的容器 */
	private VData mResult = new VData();
	/** 数据操作字符串 */
	private String mOperate;

	public E_RenewalConfUI() {}

	/**
  	传输数据的公共方法
	 */
	public boolean submitData(VData cInputData,String cOperate){
		//将操作数据拷贝到本类中
		this.mOperate = cOperate;
		E_RenewalConfBL confBL = new E_RenewalConfBL();
		System.out.println("---BB UI BEGIN---"+mOperate);
		if (confBL.submitData(cInputData,mOperate) == false)
		{
		  // @@错误处理
		  this.mErrors.copyAllErrors(confBL.mErrors);
		  CError tError = new CError();
		  tError.moduleName = "PEdorADDetailUI";
		  tError.functionName = "submitData";
		  tError.errorMessage = "数据查询失败!";
		  this.mErrors .addOneError(tError) ;
		  mResult.clear();
		  return false;
		}
		else
		  mResult = confBL.getResult();
		return true;
	}

	public VData getResult(){
		return mResult;
	}

	public static void main(String[] args){
		E_RenewalConfUI confUI = new E_RenewalConfUI();
		VData tVData = new VData();
		LDCodeSchema codeSchema = new LDCodeSchema();
		codeSchema.setCodeType("DSXQ");
		codeSchema.setCode("2137014748");
		codeSchema.setCodeName("王保升");
		codeSchema.setComCode("1");
		codeSchema.setCodeAlias("电商保单续期配置");
		tVData.addElement(codeSchema);
		if(!confUI.submitData(tVData, "DELETE||MAIN")){
			System.out.println(confUI.mErrors.getErrContent());
		}
		else{
			System.out.println("OK");
		}
	}
}

