/*
 * <p>ClassName: OLDCodeSetBL </p>
 * <p>Description: OLDCodeSetBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-26 13:18:17
 */
package com.sinosoft.lis.config;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLDCodeSetBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LDCodeSet mLDCodeSet = new LDCodeSet();
    private String mCodeType = "";
    private String zdywcode = "";
    private MMap map = new MMap();

    public OLDCodeSetBL() {
    }

    public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLDCodeSetBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLDCodeSetBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("Start OLDCodeSetBL Submit...");

        PubSubmit tPubSubmit = new PubSubmit();

        if (!tPubSubmit.submitData(mInputData, cOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            CError tError = new CError();
            tError.moduleName = "LDPersonBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }

        System.out.println("End OLDCodeSetBL Submit...");

        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
    	int insertIndex=-1;//要插入的索引
        boolean tReturn = false;
        System.out.println("After getInputData");
        System.out.println(mOperate);
        //处理个人信息数据
        //添加纪录
        if (mOperate.equals("INSERT")) {
        	ExeSQL tExeSQL=new ExeSQL();
        	for(int i=1;i<=mLDCodeSet.size();i++)
        	{
        		String num=tExeSQL.getOneValue("select count(*) from ldcode where codetype='llgrptype' and codename='"+mLDCodeSet.get(i).getCodeName()+"'");
        		if("0".equals(num))
        		{
        			insertIndex=i;
        			break;
        		}
        	}
        	if(insertIndex==-1)
        	{
        		buildError("dealData", "新增失败！");
                return false;
        	}
        	String maxno=tExeSQL.getOneValue("select max(code) from ldcode where codetype='llgrptype'");
        	if(maxno==null || "".equals(maxno))
        		maxno="0";
        	mLDCodeSet.get(insertIndex).setCode(String.valueOf(Integer.parseInt(maxno)+1));
            map.put(mLDCodeSet.get(insertIndex), "INSERT");
            tReturn = true;
        }

        if (mOperate.equals("UPDATE")) {
        	if(this.zdywcode==null||"".equals(this.zdywcode))
        	{
        		buildError("dealData", "请先选择重点业务，然后再修改并提交");
                return false;
        	}
        	String tUpdateSQL="update ldcode set codename='"+mLDCodeSet.get(1).getCodeName()+"' where codetype='llgrptype' and code='"+this.zdywcode+"'";
//            map.put(mLDCodeSet, "UPDATE");
            map.put(tUpdateSQL, "UPDATE");
            tReturn = true;

        }

        if (mOperate.equals("DELETE")) {
            map.put(mLDCodeSet, "DELETE");
            tReturn = true;
        }

        if (mOperate.equals("DELETE&INSERT")) {
            String tDELSQL = "delete from ldcode where codetype='" + mCodeType +
                             "'";
            map.put(tDELSQL, "DELETE");
            map.put(mLDCodeSet, "INSERT");
            tReturn = true;
        }

        return tReturn;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLDCodeSet.set((LDCodeSet) cInputData.
                            getObjectByObjectName("LDCodeSet", 0));
        //this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
        this.zdywcode=(String)((TransferData) cInputData.getObjectByObjectName("TransferData", 0)).getValueByName("zdywcode");
        if (mLDCodeSet == null) {
            buildError("getInputData", "获取分类业务信息失败");
            return false;
        }

        if (mLDCodeSet.size() <= 0) {
            buildError("getInputData", "获取分类业务信息失败");
            return false;
        }

        //默认只处理一中类型的
        mCodeType = mLDCodeSet.get(1).getCodeType();

        if (mCodeType == null || "".equals(mCodeType)) {
            buildError("getInputData", "获取分类业务信息失败");
            return false;
        }

        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLDCodeSet);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDCodeSetBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    /**
     * 追加错误信息
     *
     * @param szFunc
     *            String
     * @param szErrMsg
     *            String
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "OLDCodeSetBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }

}
