/*
 * <p>ClassName: OLDUWUserBL </p>
 * <p>Description: OLDUWUserBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-24 18:15:01
 */
package com.sinosoft.lis.config;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLDUWUserBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LDUWUserSchema mLDUWUserSchema = new LDUWUserSchema();
    LCUWReponsComSet mLCUWReponsComSet = new LCUWReponsComSet();
    private LDSpotUWRateSet mLDSpotUWRateSet = new LDSpotUWRateSet();
//private LDUWUserSet mLDUWUserSet=new LDUWUserSet();
    public OLDUWUserBL() {
    }

    public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLDUWUserBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLDUWUserBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start OLDUWUserBL Submit...");
            /*      OLDUWUserBLS tOLDUWUserBLS=new OLDUWUserBLS();
                  tOLDUWUserBLS.submitData(mInputData,mOperate);
                  System.out.println("End OLDUWUserBL Submit...");
                  //如果有需要处理的错误，则返回
                  if (tOLDUWUserBLS.mErrors.needDealError())
                  {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tOLDUWUserBLS.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "OLDUWUserBL";
                    tError.functionName = "submitDat";
                    tError.errorMessage ="数据提交失败!";
                    this.mErrors .addOneError(tError) ;
                    return false;
                  }*/
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                CError tError = new CError();
                tError.moduleName = "OLDUWUserBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }

            System.out.println("---commitData---");

        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        LDUserDB tLDUsertDB = new LDUserDB();
        tLDUsertDB.setUserCode(mLDUWUserSchema.getUserCode());
        String strSQL = "";

        if (!tLDUsertDB.getInfo()) { //验证LCGrpCont表中是否存在该合同项记录
            CError tError = new CError();
            tError.moduleName = "dealData";
            tError.functionName = "dealData";
            tError.errorMessage = "核保师不存在";
            this.mErrors.addOneError(tError);

        }
        if (this.mOperate.equals("INSERT||MAIN")) {
            mLDUWUserSchema.setManageCom(tLDUsertDB.getComCode());
            mLDUWUserSchema.setOperator(mGlobalInput.Operator);
            mLDUWUserSchema.setMakeDate(PubFun.getCurrentDate());
            mLDUWUserSchema.setMakeTime(PubFun.getCurrentTime());
            mLDUWUserSchema.setModifyDate(PubFun.getCurrentDate());
            mLDUWUserSchema.setModifyTime(PubFun.getCurrentTime());

            map.put(mLDUWUserSchema, "INSERT");
            map.put(mLDSpotUWRateSet, "INSERT");
        }

        if (this.mOperate.equals("UPDATE||MAIN")) {
            mLDUWUserSchema.setManageCom(tLDUsertDB.getComCode());
            mLDUWUserSchema.setOperator(mGlobalInput.Operator);
            mLDUWUserSchema.setMakeDate(PubFun.getCurrentDate());
            mLDUWUserSchema.setMakeTime(PubFun.getCurrentTime());
            mLDUWUserSchema.setModifyDate(PubFun.getCurrentDate());
            mLDUWUserSchema.setModifyTime(PubFun.getCurrentTime());
            /**
             * yangming:原来没有对抽检比例的修改操作，现在添加
             */
            strSQL = "delete from LDSpotUWRate where UserCode='"
                     + mLDSpotUWRateSet.get(1).getUserCode() + "' and UWType='" +
                     mLDSpotUWRateSet.get(1).getUWType()+"'";
            map.put(strSQL, "DELETE");
            map.put(mLDSpotUWRateSet, "INSERT");
            map.put(mLDUWUserSchema, "UPDATE");
        }

        if (this.mOperate.equals("DELETE||MAIN")) {
            map.put(mLDUWUserSchema, "DELETE");
            String strSQL_re = "delete from LDSpotUWRate where UserCode='"
                               + mLDUWUserSchema.getUserCode() + "' and UWType='" +
                     mLDSpotUWRateSet.get(1).getUWType()+"'";
            map.put(strSQL_re, "DELETE");
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLDUWUserSchema.setSchema((LDUWUserSchema) cInputData.
                                       getObjectByObjectName("LDUWUserSchema",
                0));
//        mLCUWReponsComSet.set((LCUWReponsComSet) cInputData.
//                              getObjectByObjectName(
//                                      "LCUWReponsComSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        mLDSpotUWRateSet.set((LDSpotUWRateSet) cInputData.
                             getObjectByObjectName(
                                     "LDSpotUWRateSet", 0)
                );
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LDUWUserDB tLDUWUserDB = new LDUWUserDB();
        tLDUWUserDB.setSchema(this.mLDUWUserSchema);
        //如果有需要处理的错误，则返回
        if (tLDUWUserDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLDUWUserDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDUWUserBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLDUWUserSchema);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLDUWUserSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDUWUserBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
