/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.config;

import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:单证回收处理功能模块
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author kevin
 * @version 1.0
 */
public class RiskMSUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    /** 数据操作字符串 */
    private String mOperate;


    //业务处理相关变量
    private VData mResult = null;

    public RiskMSUI()
    {
    }

    public static void main(String[] args)
    {
    	
    }


    /*
     * public function used to send data
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            mOperate = cOperate;

            RiskMSBL tRiskMSBL = new RiskMSBL();

            boolean bReturn = tRiskMSBL.submitData(cInputData, mOperate);

            mResult = tRiskMSBL.getResult();

            if (!bReturn)
            {
                if (tRiskMSBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tRiskMSBL.mErrors);
                }
                else
                {
                    buildError("submitData", "CertTakeBackBL出错，但是没有提供详细的错误信息");
                }
            }

            return bReturn;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submitData", "发生异常");
            return false;
        }
    }

    public VData getResult()
    {
        return mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CertTakeBackUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
