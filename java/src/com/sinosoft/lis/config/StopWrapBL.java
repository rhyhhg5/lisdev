/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.config;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LDRiskWrapSchema;
import com.sinosoft.lis.schema.LICertifyImportLogSchema;
import com.sinosoft.lis.vschema.LDRiskWrapSet;
import com.sinosoft.utility.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description:单证管理回收操作</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author kevin
 * @version 1.0
 */

public class StopWrapBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /* 业务相关的数据 */
    private LDRiskWrapSet mLDRiskWrapSet = new LDRiskWrapSet();
    private VData mResult = new VData();
    private GlobalInput globalInput = new GlobalInput();
    private String mszOperate = "";


    // 记录下当前操作到哪一条记录，如果操作没有成功完成，给用户返回所有未能成功处理的数据。

    public StopWrapBL()
    {
    }

    public static void main(String[] args)
    {
    }


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        mszOperate = cOperate;
        if (mszOperate.equals(""))
        {
            buildError("verifyOperate", "不支持的操作字符串");
            return false;
        }

        if (!getInputData(cInputData))
        {
        	return false;
        }
        if (!dealData())
        {
            return false;
        }

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }


    //根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData()
    {
        boolean flag = true;
        MMap tMMap = new MMap();
        String tErrorInfo = "";
        for(int i=1;i<=mLDRiskWrapSet.size();i++){
        	LDRiskWrapSchema tLDRiskWrapSchema = mLDRiskWrapSet.get(i);
        	String tSQL = "";
        	String tWSQL = "";
    		LICertifyImportLogSchema tLICertifyImportLogSchema = new LICertifyImportLogSchema();
    		String aBatchNo = PubFun1.CreateMaxNo("STOPWRAP", "stop");
    		tLICertifyImportLogSchema.setBatchNo(aBatchNo);
    		tLICertifyImportLogSchema.setCardNo(tLDRiskWrapSchema.getRiskWrapCode());
    		if("Stop".equals(mszOperate)){
            	tErrorInfo = "套餐停售："+tLDRiskWrapSchema.getRiskWrapCode();
            	tLICertifyImportLogSchema.setErrorState("2");
            	tLICertifyImportLogSchema.setErrorType("");
            	tSQL = "update ldriskwrap set enddate = '"+tLDRiskWrapSchema.getEndDate()+"' where riskwrapcode='"+tLDRiskWrapSchema.getRiskWrapCode()+"' ";
            	tWSQL = "update ldwrap set wraptype='8S' where riskwrapcode='"+tLDRiskWrapSchema.getRiskWrapCode()+"' and wraptype='8' ";
            	
            }else if("QStop".equals(mszOperate)){
            	tErrorInfo = "取消停售套餐："+tLDRiskWrapSchema.getRiskWrapCode();
            	tLICertifyImportLogSchema.setErrorState("3");
            	tLICertifyImportLogSchema.setErrorType("");
            	tSQL = "update ldriskwrap set enddate = null where riskwrapcode='"+tLDRiskWrapSchema.getRiskWrapCode()+"' ";
            	tWSQL = "update ldwrap set wraptype='8' where riskwrapcode='"+tLDRiskWrapSchema.getRiskWrapCode()+"' and wraptype='8S' ";
            }
    		tLICertifyImportLogSchema.setErrorInfo(tErrorInfo);
    		tLICertifyImportLogSchema.setOperator(globalInput.Operator);
    		tLICertifyImportLogSchema.setMakeDate(PubFun.getCurrentDate());
    		tLICertifyImportLogSchema.setMakeTime(PubFun.getCurrentTime());
    		tLICertifyImportLogSchema.setModifyDate(PubFun.getCurrentDate());
    		tLICertifyImportLogSchema.setModifyTime(PubFun.getCurrentTime());
    		
    		tMMap.put(tSQL, SysConst.UPDATE);
    		String tWNSql = "select 1 from ldwrap where riskwrapcode = '"+tLDRiskWrapSchema.getRiskWrapCode()+"' and wraptype in ('8','8S') ";
    		String tWNFlag = new ExeSQL().getOneValue(tWNSql);
    		if("1".equals(tWNFlag)){
    			tMMap.put(tWSQL, SysConst.UPDATE);
    		}
    		tMMap.put(tLICertifyImportLogSchema, SysConst.INSERT);
    	}
//		保存数据
        VData data = new VData();
        data.add(tMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }
        tMMap = null;
    	return flag;
    }


    //从输入数据中得到所有对象
    //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    private boolean getInputData(VData vData)
    {
    	
    	globalInput.setSchema((GlobalInput) vData.getObjectByObjectName(
                "GlobalInput", 0));
    	mLDRiskWrapSet.set((LDRiskWrapSet) vData.getObjectByObjectName("LDRiskWrapSet",0));
        if(mLDRiskWrapSet == null || mLDRiskWrapSet.size()<=0){
        	buildError("getInputData","获取套餐数据失败！");
        	return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "CertifyTakeBackUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
