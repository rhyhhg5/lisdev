/*
 * <p>ClassName: OLAArchieveBLS </p>
 * <p>Description: OLAArchieveBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:05:58
 */
package com.sinosoft.lis.agentdaily;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.LAArchieveDBSet;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;
 public class OLAArchieveBLS {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
//传输数据类
  private VData mInputData ;
 /** 数据操作字符串 */
private String mOperate;
 private LAArchieveSet mInsertLAArchieveSet = new LAArchieveSet();
 private LAArchieveSet mUpdateLAArchieveSet = new LAArchieveSet();
 private String ArchieveNo="";
public OLAArchieveBLS() {
	}
public static void main(String[] args) {
}
 /**
 传输数据的公共方法
*/
public boolean submitData(VData cInputData,String cOperate)
{
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
	  mInputData=(VData)cInputData.clone();
    if(this.mOperate.equals("INSERT||MAIN") || this.mOperate .equals("UPDATE||MAIN") )
    {if (!saveLAArchieve())
        return false;
    }
    if (this.mOperate.equals("DELETE||MAIN"))
    {if (!deleteLAArchieve())
        return false;
    }
//    if (this.mOperate.equals("UPDATE||MAIN"))
//    {if (!updateLAArchieve())
//        return false;
//    }
  return true;
}
 /**
* 保存函数
*/
private boolean saveLAArchieve()
{

       this.mUpdateLAArchieveSet = (LAArchieveSet) mInputData.getObject(0);
       this.mInsertLAArchieveSet = (LAArchieveSet) mInputData.getObject(1);

  Connection conn;
  conn=null;
  conn=DBConnPool.getConnection();
  if (conn==null)
  {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OLAArchieveBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
   }
	try{
 	  conn.setAutoCommit(false);
           if (mInsertLAArchieveSet.size() >0)
           {
               LAArchieveDBSet tLAArchieveDBSet = new LAArchieveDBSet(conn);
               tLAArchieveDBSet.add(mInsertLAArchieveSet);


               this.ArchieveNo = createArchieveNo(mInsertLAArchieveSet.get(1).
                       getAgentCode());
               for (int i=1;i<=mInsertLAArchieveSet.size() ;i++)
               {
                   mInsertLAArchieveSet.get(i).setArchieveNo(ArchieveNo);
               }


               try
               {
                   if (!tLAArchieveDBSet.insert())
                   {
                       // @@错误处理
                       this.mErrors.copyAllErrors(tLAArchieveDBSet.mErrors);
                       CError tError = new CError();
                       tError.moduleName = "OLAArchieveBLS";
                       tError.functionName = "saveData";
                       tError.errorMessage = "数据保存失败!";
                       this.mErrors.addOneError(tError);
                       conn.rollback();
                       conn.close();
                       return false;
                   }
               }catch(Exception eex)
               {
                   eex.printStackTrace() ;
               }
           }
           if (mUpdateLAArchieveSet.size() >0)
          {
              LAArchieveDBSet tLAArchieveDBSet = new LAArchieveDBSet(conn);
              tLAArchieveDBSet.add(mUpdateLAArchieveSet);
              try
              {
                  if (!tLAArchieveDBSet.update())
                  {
                      // @@错误处理
                      this.mErrors.copyAllErrors(tLAArchieveDBSet.mErrors);
                      CError tError = new CError();
                      tError.moduleName = "OLAArchieveBLS";
                      tError.functionName = "saveData";
                      tError.errorMessage = "数据保存失败!";
                      this.mErrors.addOneError(tError);
                      conn.rollback();
                      conn.close();
                      return false;
                  }
              }catch(Exception eex)
              {
                  eex.printStackTrace() ;
              }
          }

    conn.commit() ;
    conn.close();
  }
  catch (Exception ex)
  {
    // @@错误处理
    ex.printStackTrace() ;
    CError tError =new CError();
    tError.moduleName="OLAArchieveBLS";
    tError.functionName="submitData";
    tError.errorMessage=ex.toString();
    this.mErrors .addOneError(tError);
      try{
      conn.rollback() ;
      conn.close();
      }
      catch(Exception e){}
    return false;
	}
  return true;
}

     private String createArchieveNo(String cAgentCode)
   {
       String sql1="select distinct archieveno from laarchieve where agentcode='"+cAgentCode+"'";
       ExeSQL tExeSQL=new ExeSQL();
       String ss= tExeSQL.getOneValue(sql1) ;
       if (ss!=null && !ss.equals("")  )
       {
           return ss;
       }
       String sql = "select substr(branchattr,1,10) from labranchgroup where branchtype='1' and branchtype2='01' "
                    +
               " and agentgroup=(select branchcode from laagent where agentcode='" +
                    cAgentCode + "')";
       String result = (new ExeSQL()).getOneValue(sql);
       String seriesNo = PubFun1.CreateMaxNo("ArcNo" + result, 5);
       String ArchieveNo = result + seriesNo;
       return ArchieveNo;
   }

    /**
    * 保存函数
    */
    private boolean deleteLAArchieve()
    {
        LAArchieveSchema tLAArchieveSchema = new LAArchieveSchema();
        tLAArchieveSchema = (LAArchieveSchema)mInputData.getObjectByObjectName("LAArchieveSchema",0);
        System.out.println("Start Save...");
        Connection conn;
        conn=null;
        conn=DBConnPool.getConnection();
        if (conn==null)
        {
		// @@错误处理
		CError tError = new CError();
           tError.moduleName = "OLAArchieveBLS";
           tError.functionName = "saveData";
           tError.errorMessage = "数据库连接失败!";
           this.mErrors .addOneError(tError) ;
           return false;
        }
        try{
           conn.setAutoCommit(false);
           System.out.println("Start 保存...");
           LAArchieveDB tLAArchieveDB=new LAArchieveDB(conn);
           tLAArchieveDB.setSchema(tLAArchieveSchema);
           if (!tLAArchieveDB.delete())
           {
		// @@错误处理
		    this.mErrors.copyAllErrors(tLAArchieveDB.mErrors);
 		    CError tError = new CError();
		    tError.moduleName = "OLAArchieveBLS";
		    tError.functionName = "saveData";
		    tError.errorMessage = "数据删除失败!";
		    this.mErrors .addOneError(tError) ;
               conn.rollback();
               conn.close();
               return false;
           }
               conn.commit() ;
               conn.close();
         }
       catch (Exception ex)
       {
      // @@错误处理
          CError tError =new CError();
          tError.moduleName="OLAArchieveBLS";
          tError.functionName="submitData";
          tError.errorMessage=ex.toString();
          this.mErrors .addOneError(tError);
          try{conn.rollback() ;
          conn.close();} catch(Exception e){}
         return false;
         }
         return true;
}
/**
  * 保存函数
*/
private boolean updateLAArchieve()
{

     this.mUpdateLAArchieveSet = (LAArchieveSet) mInputData.getObjectByObjectName(
             "LAArchieveSet", 0);

 Connection conn;
 conn=null;
 conn=DBConnPool.getConnection();
 if (conn==null)
 {
     // @@错误处理
     CError tError = new CError();
     tError.moduleName = "OLAArchieveBLS";
     tError.functionName = "saveData";
     tError.errorMessage = "数据库连接失败!";
     this.mErrors .addOneError(tError) ;
     return false;
  }
       try{
         conn.setAutoCommit(false);
          if (mUpdateLAArchieveSet.size() >0)
          {
              LAArchieveDBSet tLAArchieveDBSet = new LAArchieveDBSet(conn);
              tLAArchieveDBSet.add(mUpdateLAArchieveSet);
              if (!tLAArchieveDBSet.update())
              {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLAArchieveDBSet.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "OLAArchieveBLS";
                  tError.functionName = "saveData";
                  tError.errorMessage = "数据保存失败!";
                  this.mErrors.addOneError(tError);
                  conn.rollback();
                  conn.close();
                  return false;
              }
          }

            conn.commit() ;
            conn.close();
       }
       catch (Exception ex)
       {
       // @@错误处理
               CError tError =new CError();
               tError.moduleName="OLAArchieveBLS";
               tError.functionName="submitData";
               tError.errorMessage=ex.toString();
               this.mErrors .addOneError(tError);
               try{conn.rollback() ;
               conn.close();} catch(Exception e){}
               return false;
     }
               return true;
     }

     public VData getResult()
     {
         VData tResult=new VData();
         tResult.add(ArchieveNo) ;
         return tResult;
     }

}


