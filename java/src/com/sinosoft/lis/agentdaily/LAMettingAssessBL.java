package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.db.LAMettingDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAMettingSchema;
import com.sinosoft.lis.vschema.LAMettingSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;


public class LAMettingAssessBL {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
   public CErrors mErrors = new CErrors();
   private VData mResult = new VData();
   /** 往后面传输数据的容器 */
   private VData mInputData;
   /** 全局数据 */
   private GlobalInput mGlobalInput = new GlobalInput();
   /** 数据操作字符串 */
   private String mOperate;
   /** 业务处理相关变量 */
   private LAMettingSchema mLAMettingSchema = new LAMettingSchema();
   private LAMettingSet mLAMettingSet = new LAMettingSet();

  public LAMettingAssessBL() {
  }
  public static void main(String[] args)
   {
       //tLAPlanSchema.setPlanCode(request.getParameter("PlanCode"));




   }
   public boolean submitData(VData cInputData, String cOperate)
      {
          //将操作数据拷贝到本类中
          this.mOperate = cOperate;
          //得到外部传入的数据,将数据备份到本类中
          if (!getInputData(cInputData))
          {
              return false;
          }
          //进行业务处理
          if (!dealData())
          {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "LAPlanBL";
              tError.functionName = "submitData";
              tError.errorMessage = "数据处理失败LAPlanBL-->dealData!";
              this.mErrors.addOneError(tError);
              return false;
          }
          //准备往后台的数据
          if (!prepareOutputData())
          {
              return false;
          }
          if (this.mOperate.equals("QUERY||MAIN"))
          {
              this.submitquery();
          }
          else
          {
              System.out.println("Start ALAPlanBL Submit...");
              LAMettingAssessBLS tLAMettingAssessBLS = new LAMettingAssessBLS();
              tLAMettingAssessBLS.submitData(mInputData, cOperate);
              System.out.println("End ALAPlanBL Submit...");
              //如果有需要处理的错误，则返回
              if (tLAMettingAssessBLS.mErrors.needDealError())
              {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLAMettingAssessBLS.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "ALAPlanBL";
                  tError.functionName = "submitDat";
                  tError.errorMessage = "数据提交失败!";
                  this.mErrors.addOneError(tError);
                  return false;
              }
          }
          mInputData = null;
          return true;
      }
      private boolean dealData()
       {
           boolean tReturn = true;
           String currentDate = PubFun.getCurrentDate();
           String currentTime = PubFun.getCurrentTime();
           String tSeriesNo = "";



           if (this.mOperate.equals("INSERT||MAIN"))
           {


               //得到最大编号+1
              tSeriesNo = com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo("minino", 10);


               System.out.println("tSeriesNo:" + tSeriesNo);
               this.mLAMettingSchema.setSeriesNo(tSeriesNo);
               this.mLAMettingSchema.setOperator(this.mGlobalInput.Operator);
               this.mLAMettingSchema.setMakeDate(currentDate);
               this.mLAMettingSchema.setMakeTime(currentTime);
               this.mLAMettingSchema.setModifyDate(currentDate);
               this.mLAMettingSchema.setModifyTime(currentTime);
           }
           else if (this.mOperate.equals("UPDATE||MAIN"))
           {

           //校验计划起期与计划止期是否与计划时间单位相符 add by liuyy
           // if(!checkTimePeriod())
           // {
           //     return false;
          //  }

               tSeriesNo = this.mLAMettingSchema.getSeriesNo();
               LAMettingDB tLAMettingDB = new LAMettingDB();
               tLAMettingDB.setSeriesNo(tSeriesNo);
               if (!tLAMettingDB.getInfo())
               {
                   // @@错误处理
                   this.mErrors.copyAllErrors(tLAMettingDB.mErrors);
                   CError tError = new CError();
                   tError.moduleName = "ALAPlanBL";
                   tError.functionName = "dealData";
                   tError.errorMessage = "查询原计划任务记录失败!";
                   this.mErrors.addOneError(tError);
                   return false;
               }
//       tLAPlanDB.setPlanType(this.mLAPlanSchema.getPlanType());
//       tLAPlanDB.setBranchType(this.mLAPlanSchema.getBranchType());
//       tLAPlanDB.setPlanCond1(this.mLAPlanSchema.getPlanCond1());
//       tLAPlanDB.setPlanPeriod(this.mLAPlanSchema.getPlanPeriod());
//       tLAPlanDB.setPlanPeriodUnit(this.mLAPlanSchema.getPlanPeriodUnit());
//       tLAPlanDB.setPlanValue(this.mLAPlanSchema.getPlanValue());
//       tLAPlanDB.setPlanObject(this.mLAPlanSchema.getPlanObject());
//       this.mLAPlanSchema.setSchema(tLAPlanDB.getSchema());
               this.mLAMettingSchema.setOperator(this.mGlobalInput.Operator);
               this.mLAMettingSchema.setMakeDate(tLAMettingDB.getMakeDate());
               this.mLAMettingSchema.setMakeTime(tLAMettingDB.getMakeTime());
               this.mLAMettingSchema.setModifyDate(currentDate);
               this.mLAMettingSchema.setModifyTime(currentTime);
           }
           tReturn = true;
           return tReturn;
       }
       private boolean getInputData(VData cInputData)
           {
               this.mLAMettingSchema.setSchema((LAMettingSchema) cInputData.
                                            getObjectByObjectName("LAMettingSchema", 0));
               this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                           getObjectByObjectName("GlobalInput", 0));
               return true;
           }
           private boolean submitquery()
           {
                   this.mResult.clear();
                   System.out.println("Start LAPlanBLQuery Submit...");
                   LAMettingDB tLAMettingDB = new LAMettingDB();
                   tLAMettingDB.setSchema(this.mLAMettingSchema);
                   this.mLAMettingSet = tLAMettingDB.query();
                   this.mResult.add(this.mLAMettingSet);
                   System.out.println("End LAPlanBLQuery Submit...");
                   mInputData = null;
        return true;
           }
           private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLAMettingSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "mLAMettingbl";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    public VData getResult()
       {
           return this.mResult;
       }

}
