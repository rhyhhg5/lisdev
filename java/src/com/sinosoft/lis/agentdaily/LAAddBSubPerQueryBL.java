package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.db.LARewardPunishDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LARewardPunishSchema;
import com.sinosoft.lis.vschema.LARewardPunishSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class LAAddBSubPerQueryBL {

	  //错误处理类
	  public  CErrors mErrors = new CErrors();
	  //业务处理相关变量
	  /** 全局数据 */
	  private VData mInputData = new VData();
	  private String mOperate = "";
	  private String CurrentDate = PubFun.getCurrentDate();
	  private String CurrentTime = PubFun.getCurrentTime();
	  private MMap map = new MMap();
	  public GlobalInput mGlobalInput = new GlobalInput();
	  private LARewardPunishSet mLARewardPunishSet = new LARewardPunishSet();
	  private Reflections ref = new Reflections();
	  private String mSQL = "";
	  private String mAgentCode="";
	  private String mManageCom="";
	  private String mAgentGroup="";
	  private String mGroupAgentCode="";
	  private String mDoneFlag="";
	  private String mDoneFlag1="";
	  private String mWageNo = "";
	  private String mSendGrp = "";
	  private String mAgentType = "";
	  private String mSendGrp1 = "";

//		public static void main(String[] args)
//		{
//			LABRCSetBL LABRCSetBL = new LABRCSetBL();
//		}
	  /**
	    传输数据的公共方法
	   */
	  public boolean submitData(VData cInputData, String cOperate) {
	    System.out.println("Begin LABRCSetAwardMoneyBL.submitData.........");
	    //将操作数据拷贝到本类中
	    this.mOperate = cOperate;
	    //得到外部传入的数据,将数据备份到本类中
	    if (!getInputData(cInputData)) {
	      return false;
	    }
	    if(!check())
	    {
	    	return false;
	    }
	    //进行业务处理
	    if (!dealData()) {
	      return false;
	    }
	    //准备往后台的数据
	    if (!prepareOutputData()) {
	      return false;
	    }
	    PubSubmit tPubSubmit = new PubSubmit();
	    if (!tPubSubmit.submitData(mInputData, mOperate)) {
	      // @@错误处理
	      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
	      CError tError = new CError();
	      tError.moduleName = "LABRCSetAwardMoneyBL";
	      tError.functionName = "submitData";
	      tError.errorMessage = "数据提交失败!";
	      this.mErrors.addOneError(tError);
	      return false;
	    }
	    mInputData = null;
	    return true;
	  }
	/**
	 * 
	 */
	  private boolean check()
	  {
		     String tSql1="";
	    	 ExeSQL tExeSQL=new ExeSQL();
	         if(mOperate.equals("selSubmit")||mOperate.equals("selBack")) {
		    	 for(int i = 1;i<=mLARewardPunishSet.size();i++){
		    	   tSql1 ="select '1' from larewardpunish where idx='"+mLARewardPunishSet.get(i).getIdx()+"' and agentcode="
		    	   	+"(select agentcode from laagent where groupagentcode = '"
		    	   +mLARewardPunishSet.get(i).getAgentCode()+"') and sendgrp='02'";
		    	    	if(tExeSQL.getOneValue(tSql1).equals("1")){
		    	        	mErrors.addOneError("数据中有已审核通过的加扣款无法提交和收回");
		    	    		return false;
		    	    	}
		       }
		    	 
	         }
		     if(mOperate.equals("allSubmit")||mOperate.equals("allBack")){
			   LARewardPunishDB tLARewardPunishDB =new  LARewardPunishDB(); 
		    	 String tSQL ="select * from larewardpunish  where branchtype ='3' and branchtype2 = '01' "
		                     +" and managecom = '"+this.mManageCom+"' " ;
	    		       if(this.mAgentGroup!=""&&!this.mAgentGroup.equals("")){
	    		    	   tSQL+="and agentcode in (select agentcode from laagent where agentgroup in (" 
	    		    	  + "select agentgroup from labranchgroup where BranchAttr ='"+this.mAgentGroup+"'))";
	    		       }
	    		       if(this.mGroupAgentCode!=""&&!this.mGroupAgentCode.equals("")){
	    		    	   tSQL+=" and agentcode = (select agentcode from laagent where groupagentcode ='"+this.mGroupAgentCode+"')";
	    		       }
	    		       if((this.mDoneFlag!=""&&!this.mDoneFlag.equals(""))||(this.mDoneFlag1==null&&!this.mDoneFlag1.equals(""))){
	    		    	   tSQL+=" and (DoneFlag ='"+this.mDoneFlag+"' or DoneFlag ='"+this.mDoneFlag1+"')"; 
	    		       }
	    		       if(this.mWageNo!=""&&!this.mWageNo.equals("")){
	    		    	   tSQL+=" and wageno = '"+this.mWageNo+"'";
	    		       }
	    		       if(this.mSendGrp!=""&&!this.mSendGrp.equals("")){
	    		    	   tSQL+=" and SendGrp = '"+this.mSendGrp+"'";
	    		       }
	    		       if(this.mAgentType!=""&&!this.mAgentType.equals("")){
	    		    	   tSQL+=" and agentcode in (select agentcode from laagent where agenttype = '"+this.mAgentType+"')";
	    		       }
	    		       tSQL+=" with ur";
	    		       System.out.println("提交SQL为："+tSQL);
	      		       mLARewardPunishSet=tLARewardPunishDB.executeQuery(tSQL);
	      		       for(int i = 1;i<=mLARewardPunishSet.size();i++){
			    		 tSql1 ="select '1' from larewardpunish where idx='"+mLARewardPunishSet.get(i).getIdx()+"' and agentcode='"
			    		 +mLARewardPunishSet.get(i).getAgentCode()+"' and sendgrp='02'";
			    	    	if(tExeSQL.getOneValue(tSql1).equals("1")){
			    	        	mErrors.addOneError("数据中有已审核通过的加扣款无法提交和收回");
			    	    		return false;
			    	    	}
	      		       }
	      		       
		         }
		  return true;
	  }
	  /**
	   * 从输入数据中得到所有对象
	   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	   */
	  private boolean getInputData(VData cInputData) {
	    //全局变量
	    try {
	      System.out.println("Begin LABRCSetAwardMoneyBL.getInputData.........");
	      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
	                                  getObjectByObjectName("GlobalInput", 0));
	      if(mOperate.equals("selSubmit")||mOperate.equals("selBack")){
	      		this.mLARewardPunishSet.set( (LARewardPunishSet) cInputData.getObjectByObjectName("LARewardPunishSet",0));
	      		System.out.println("LARewardPunishSet get"+mLARewardPunishSet.size());
	      }
	      if(mOperate.equals("allSubmit")||mOperate.equals("allBack")){
		    	 this.mManageCom=(String)cInputData.get(1);
		    	 this.mAgentGroup=(String)cInputData.get(2);
		    	 this.mGroupAgentCode=(String)cInputData.get(3);
		    	 this.mDoneFlag=(String)cInputData.get(4);
		    	 this.mDoneFlag1=(String)cInputData.get(5);
		    	 this.mWageNo=(String)cInputData.get(6);
		    	 this.mSendGrp=(String)cInputData.get(7);
		    	 this.mAgentType=(String)cInputData.get(8);
		    	 this.mSendGrp1=(String)cInputData.get(9);
	      }

	    }
	    catch (Exception ex) {
	      // @@错误处理
	      CError tError = new CError();
	      tError.moduleName = "LABRCSetAwardMoneyBL";
	      tError.functionName = "getInputData";
	      tError.errorMessage = "在读取处理所需要的数据时出错。";
	      this.mErrors.addOneError(tError);
	      return false;
	    }
	    System.out.println("getInputData end ");
	    return true;
	  }

	  /**
	   * 业务处理主函数
	   */
	  private boolean dealData() {
	    System.out.println("Begin LABRCSetAwardMoneyBL.dealData........."+mOperate);
	    ExeSQL tExe1 = new ExeSQL();
	    
	    try {
	        LARewardPunishSchema  tLARewardPunishSchemaNew = new  LARewardPunishSchema();
	        LARewardPunishSchema  tLARewardPunishSchemaOld = new  LARewardPunishSchema();
	        LARewardPunishDB tLARewardPunishDB =new  LARewardPunishDB();
	      if(mOperate.equals("selSubmit")||mOperate.equals("selBack")) {
	    	 LARewardPunishSet tLARewardPunishSet = new LARewardPunishSet();
	    	 for(int i = 1;i<=mLARewardPunishSet.size();i++){
				  mSQL = "select agentcode from laagent where groupagentcode = '"+mLARewardPunishSet.get(i).getAgentCode()+"'";
			     mAgentCode=tExe1.getOneValue(mSQL);
	    		 tLARewardPunishDB.setIdx(mLARewardPunishSet.get(i).getIdx());
	    		 tLARewardPunishDB.setAgentCode(mAgentCode);
	    		 tLARewardPunishSchemaOld = tLARewardPunishDB.query().get(1); 
	    		 tLARewardPunishSchemaNew = mLARewardPunishSet.get(i);
	    		 tLARewardPunishSchemaOld.setModifyDate(CurrentDate);
	    		 tLARewardPunishSchemaOld.setModifyTime(CurrentTime);
	    		 tLARewardPunishSchemaOld.setSendGrp(tLARewardPunishSchemaNew.getSendGrp());
	    		 tLARewardPunishSchemaOld.setOperator(mGlobalInput.Operator); 
	    		 tLARewardPunishSet.add(tLARewardPunishSchemaOld);
	    	 }
	    	 map.put(tLARewardPunishSet, "UPDATE");
	      }
	     if(mOperate.equals("allSubmit")||mOperate.equals("allBack")){
	    	 LARewardPunishSet tLARewardPunishSet = new LARewardPunishSet();   
//	    	 String tSQL ="select * from larewardpunish  where branchtype ='3' and branchtype2 = '01' "
//	                     +" and managecom = '"+this.mManageCom+"' " ;
//    		       if(this.mAgentGroup!=""&&!this.mAgentGroup.equals("")){
//    		    	   tSQL+="and agentcode in (select agentcode from laagent where agentgroup in (" 
//    		    	  + "select agentgroup from labranchgroup where BranchAttr ='"+this.mAgentGroup+"'))";
//    		       }
//    		       if(this.mGroupAgentCode!=""&&!this.mGroupAgentCode.equals("")){
//    		    	   tSQL+=" and agentcode = (select agentcode from laagent where groupagentcode ='"+this.mGroupAgentCode+"'";
//    		       }
//    		       if((this.mDoneFlag!=""&&!this.mDoneFlag.equals(""))||(this.mDoneFlag1==null&&!this.mDoneFlag1.equals(""))){
//    		    	   tSQL+=" and (DoneFlag ='"+this.mDoneFlag+"' or DoneFlag ='"+this.mDoneFlag1+"')"; 
//    		       }
//    		       if(this.mWageNo!=""&&!this.mWageNo.equals("")){
//    		    	   tSQL+=" and wageno = '"+this.mWageNo+"'";
//    		       }
//    		       if(this.mSendGrp!=""&&!this.mSendGrp.equals("")){
//    		    	   tSQL+=" and SendGrp = '"+this.mSendGrp+"'";
//    		       }
//    		       if(this.mAgentType!=""&&!this.mAgentType.equals("")){
//    		    	   tSQL+=" and agentcode in (select agentcode from laagent where agenttype = '"+this.mAgentType+"')";
//    		       }
//    		       tSQL+=" with ur";
//    		       mLARewardPunishSet=tLARewardPunishDB.executeQuery(tSQL);
    		       for(int j=1;j<=mLARewardPunishSet.size();j++){
    		    	   tLARewardPunishSchemaOld=mLARewardPunishSet.get(j).getSchema();
    		    	   tLARewardPunishSchemaOld.setSendGrp(mSendGrp1);
    		    	   tLARewardPunishSchemaOld.setModifyDate(CurrentDate);
    		    	   tLARewardPunishSchemaOld.setModifyTime(CurrentTime);
    		    	   tLARewardPunishSchemaOld.setOperator(mGlobalInput.Operator);
    		    		 tLARewardPunishSet.add(tLARewardPunishSchemaOld);
    		       }
    		    	 map.put(tLARewardPunishSet, "UPDATE");
	         }
	    }
	    catch (Exception ex) {
	      // @@错误处理
	      CError tError = new CError();
	      tError.moduleName = "LABRCSetAwardMoneyBL";
	      tError.functionName = "dealData";
	      tError.errorMessage = "在处理所数据时出错。";
	      this.mErrors.addOneError(tError);
	      return false;
	    }
	    return true;
	  }

	  /**
	   * 准备往后层输出所需要的数据
	   * 输出：如果准备数据时发生错误则返回false,否则返回true
	   */
	  private boolean prepareOutputData() {
	    try {
	      mInputData.clear();
	      mInputData.add(map);
	    }
	    catch (Exception ex) {
	      // @@错误处理
	      CError tError = new CError();
	      tError.moduleName = "LABRCSetAwardMoneyBL";
	      tError.functionName = "prepareOutputData";
	      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
	      this.mErrors.addOneError(tError);
	      return false;
	    }
	    return true;
	  }
	  
	  private void BuildError(String tFun,String tMess)
	  {
	      CError tError = new CError();
	      tError.moduleName = "LABRCSetAwardMoneyBL";
	      tError.functionName = tFun;
	      tError.errorMessage = tMess;
	      this.mErrors.addOneError(tError);

	} 

}
