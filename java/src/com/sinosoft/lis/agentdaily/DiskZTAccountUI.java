package com.sinosoft.lis.agentdaily;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author luomin
 * @version 1.1
 */
public class DiskZTAccountUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private DiskZTAccountBL mDiskZTAccountBL = null;

    public DiskZTAccountUI()
    {
        mDiskZTAccountBL = new DiskZTAccountBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
        if(!mDiskZTAccountBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mDiskZTAccountBL.mErrors);
            return false;
        }
        //
        if(mDiskZTAccountBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mDiskZTAccountBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mDiskZTAccountBL.getImportPersons();
    }



    public static void main(String[] args)
    {
        DiskZTAccountUI zDiskZTAccountUI = new DiskZTAccountUI();
    }
}
