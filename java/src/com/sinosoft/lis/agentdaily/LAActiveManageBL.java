/*
 * <p>ClassName: ALARewardPunishBL </p>
 * <p>Description: ALARewardPunishBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentdaily;

//import com.sinosoft.lis.db.LAAgentDB;

import com.sinosoft.lis.db.LAAgentCompreScoreDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentCompreScoreSchema;
import com.sinosoft.lis.vschema.LAAgentCompreScoreSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class LAActiveManageBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap mMap = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LAAgentCompreScoreSet tLAAgentCompreScoreSet = new LAAgentCompreScoreSet();


    public LAActiveManageBL() {}

    public static void main(String[] args)
    { }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据 cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 将操作数据拷贝到本类中
        this.mOperate = cOperate;

        // 得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if(check())
        {
        	return false;
        }
        // 进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAActivManageBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALAActivManageBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        PubSubmit t = new PubSubmit();
         t.submitData(mInputData, this.mOperate);
        // 准备往后台的数据
        mInputData = null;
        return true;
    }



    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
       // String tRewardPunishType = ""; //奖励类型
        LAAgentCompreScoreSchema tLAAgentCompreScoreSchema ;
        LAAgentCompreScoreDB tLAAgentCompreScoreDB;
        LAAgentCompreScoreSet mLAAgentCompreScoreSet = new LAAgentCompreScoreSet();
    	for(int i=1;i<=this.tLAAgentCompreScoreSet.size();i++)
    	{
    		tLAAgentCompreScoreSchema = new LAAgentCompreScoreSchema();
    		tLAAgentCompreScoreSchema = tLAAgentCompreScoreSet.get(i);
    		tLAAgentCompreScoreDB = new LAAgentCompreScoreDB();
    		tLAAgentCompreScoreDB.setAgentCode(tLAAgentCompreScoreSchema.getAgentCode());
    		tLAAgentCompreScoreDB.setWageNo(tLAAgentCompreScoreSchema.getWageNo());
    		tLAAgentCompreScoreDB.setProjectCode(tLAAgentCompreScoreSchema.getProjectCode());
    		boolean existFlag = tLAAgentCompreScoreDB.getInfo();
    		if (existFlag)
    		{
    			if(this.mOperate.equals("INSERT"))
    			{
	    			CError tError = new CError();
	                tError.moduleName = "ALAActivManageBL";
	                tError.functionName = "check";
	                tError.errorMessage = "该业务员对应的综合评分已经录入，请查询后修改!";
	                this.mErrors.addOneError(tError);
	                return false;
    			}
	    		if(this.mOperate.equals("UPDATE"))
	    		{
	    			tLAAgentCompreScoreDB.setLifeScore(tLAAgentCompreScoreSchema.getLifeScore());
	    			tLAAgentCompreScoreDB.setPropertyScore(tLAAgentCompreScoreSchema.getPropertyScore());
	    			tLAAgentCompreScoreDB.setSumScore(tLAAgentCompreScoreSchema.getSumScore());
	    			tLAAgentCompreScoreDB.setLifeScore(tLAAgentCompreScoreSchema.getLifeScore());
	    			tLAAgentCompreScoreDB.setModifyDate(currentDate);
	    			tLAAgentCompreScoreDB.setModifyTime(currentTime);
	    			mLAAgentCompreScoreSet.add(tLAAgentCompreScoreDB);
	    		}
	    		if(this.mOperate.equals("DELETE"))
	    		{
	    			mLAAgentCompreScoreSet.add(tLAAgentCompreScoreDB);
	    		}
        	}
    		else 
    		{
    			if(this.mOperate.equals("INSERT"))
    			{
    				tLAAgentCompreScoreSchema.setOperator(this.mGlobalInput.Operator);
    				tLAAgentCompreScoreSchema.setMakeDate(currentDate);
    				tLAAgentCompreScoreSchema.setMakeTime(currentTime);
    				tLAAgentCompreScoreSchema.setModifyDate(currentDate);
    				tLAAgentCompreScoreSchema.setModifyTime(currentTime);
    				mLAAgentCompreScoreSet.add(tLAAgentCompreScoreSchema);
    			}
    		}
    	}
    	mMap.put(mLAAgentCompreScoreSet, this.mOperate);
    	mInputData.clear();
    	mInputData.add(mMap);
        return true;
    }

    /**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.tLAAgentCompreScoreSet.set((LAAgentCompreScoreSet) cInputData.getObject(0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObject(1));
        System.out.println("aaa");
        return true;
    }
 
    private boolean check()
    {
    	String agentcode = tLAAgentCompreScoreSet.get(1).getAgentCode();
        String wageno= tLAAgentCompreScoreSet.get(1).getWageNo();
        String message ="";
        if("01".equals(wageno.substring(4)))
        {
        	message = "上半年";
        }
        if("07".equals(wageno.substring(4)))
        {
        	message = "下半年";
        }
        String sql ="select 1 from lawage where agentcode ='"+agentcode+"' and indexcalno ='"+wageno+"' and state='1' with ur";
        String num = new ExeSQL().getOneValue(sql);
        if(!"".equals(num)&&null!=num)
        {
        	CError tError = new CError();
            tError.moduleName = "ALAActivManageBL";
            tError.functionName = "check";
            tError.errorMessage = "该业务员对应的薪资在"+message+"已经确认！不能录入";
            this.mErrors.addOneError(tError);
            return false;
        }
    	return false;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LAActiveManageBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}
