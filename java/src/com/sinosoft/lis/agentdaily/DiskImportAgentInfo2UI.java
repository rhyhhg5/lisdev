package com.sinosoft.lis.agentdaily;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author luomin
 * @version 1.1
 */
public class DiskImportAgentInfo2UI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private DiskImportAgentInfo2BL mDiskImportAgentInfo2BL = null;

    public DiskImportAgentInfo2UI()
    {
        mDiskImportAgentInfo2BL = new DiskImportAgentInfo2BL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
        if(!mDiskImportAgentInfo2BL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mDiskImportAgentInfo2BL.mErrors);
            return false;
        }
        //
        if(mDiskImportAgentInfo2BL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mDiskImportAgentInfo2BL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mDiskImportAgentInfo2BL.getImportPersons();
    }



    public static void main(String[] args)
    {
        DiskImportAgentInfo2UI zDiskImportAgentInfoUI = new DiskImportAgentInfo2UI();
    }
}
