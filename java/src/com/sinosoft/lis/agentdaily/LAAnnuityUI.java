package com.sinosoft.lis.agentdaily;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;

public class LAAnnuityUI
{
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    public static void main(String[] args){
        VData cInputData = new VData();
        String cOperate = "UPDATE||MAIN";
        String Priview = "PREVIEW";
        boolean expectedReturn = true;
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "001";
        tGlobalInput.ManageCom = "861100";
        tGlobalInput.ComCode = "001";
        cInputData.add(tGlobalInput);
        String[] testContNo = {"1400000296"};
        LAAnnuitySchema tLAAnnuitySchema = new LAAnnuitySchema();
        LAAnnuitySet tLAAnnuitySet = new LAAnnuitySet();

        tLAAnnuitySchema.setAgentCode("1101000008");
        tLAAnnuitySchema.setOutWorkDate("2005-04-30");
        tLAAnnuitySchema.setPayDate("2005-05-01");
        tLAAnnuitySchema.setDrawMarker("1");
        tLAAnnuitySchema.setSumAnnutity(10000);
        tLAAnnuitySchema.setDealAnnuity(10000);
        tLAAnnuitySchema.setDrawAnnuity(5000);
        tLAAnnuitySchema.setBaseAnnuity(300);
        tLAAnnuitySchema.setScaleAnnuity(0.03);

        tLAAnnuitySet.add(tLAAnnuitySchema);

        cInputData.add(tLAAnnuitySchema);
        cInputData.add(Priview);
        LAAnnuityUI tLAAnnuityUI = new LAAnnuityUI();
        boolean actualReturn = tLAAnnuityUI.submitData(cInputData, cOperate);
    }

    public boolean submitData(VData cInputData,String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        mInputData=(VData)cInputData.clone();

        LAAnnuityBL tLAAnnuityBL= new LAAnnuityBL();
        tLAAnnuityBL.submitData(mInputData,mOperate);
        //如果有需要处理的错误，则返回
        if (tLAAnnuityBL.mErrors.needDealError())
        {
          // @@错误处理
          this.mErrors.copyAllErrors(tLAAnnuityBL.mErrors);
          CError tError = new CError();
          tError.moduleName = "LAAnnuityUI";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
          this.mErrors.addOneError(tError);
          return false;
        }
        System.out.println("成功了!");
        return true;
    }
}
