package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LAPlanUnitSchema;
import com.sinosoft.lis.vschema.LAPlanUnitSet;
import com.sinosoft.utility.*;

/**
 * <p>Title: 人员考核指标计算流程类</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class LAPlanUnitUI {
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    public static void main(String[] agrs)
    {
        String cOperate = "UPDATE||MAIN";
        LAPlanUnitSchema tLAPlanUnitSchema = new LAPlanUnitSchema();
        LAPlanUnitSet tLAPlanUnitSet = new LAPlanUnitSet();

        // 基础信息
        tLAPlanUnitSchema.setPlanType("2");
        tLAPlanUnitSchema.setPlanObject("861100009988");
        tLAPlanUnitSchema.setPlanPeriodUnit("12");
        tLAPlanUnitSchema.setPlanStartYM("200501");
        tLAPlanUnitSchema.setManageCom("8611");
        tLAPlanUnitSchema.setBranchType("1");
        tLAPlanUnitSchema.setBranchType2("01");

        // 计划项目及  计划数据
        LAPlanUnitSchema tLAPlanUnitSchema1 = new LAPlanUnitSchema();
        tLAPlanUnitSchema1.setPlanItemType("01");  // 保费计划
        tLAPlanUnitSchema1.setPlanValue("100000000");
        tLAPlanUnitSchema1.setPlanValue2("");
        tLAPlanUnitSchema1.setPlanMark("");
        tLAPlanUnitSet.add(tLAPlanUnitSchema1);

        LAPlanUnitSchema tLAPlanUnitSchema2 = new LAPlanUnitSchema();
        tLAPlanUnitSchema2.setPlanItemType("02");  // 标保计划
        tLAPlanUnitSchema2.setPlanValue("200000000");
        tLAPlanUnitSchema2.setPlanValue2("");
        tLAPlanUnitSchema2.setPlanMark("");
        tLAPlanUnitSet.add(tLAPlanUnitSchema2);

        LAPlanUnitSchema tLAPlanUnitSchema3 = new LAPlanUnitSchema();
        tLAPlanUnitSchema3.setPlanItemType("03");  // 人力计划
        tLAPlanUnitSchema3.setPlanValue("12");
        tLAPlanUnitSchema3.setPlanValue2("15");
        tLAPlanUnitSchema3.setPlanMark("");
        tLAPlanUnitSet.add(tLAPlanUnitSchema3);

        LAPlanUnitSchema tLAPlanUnitSchema4 = new LAPlanUnitSchema();
        tLAPlanUnitSchema4.setPlanItemType("04");  // 客户数计划
        tLAPlanUnitSchema4.setPlanValue("600");
        tLAPlanUnitSchema4.setPlanValue2("1000");
        tLAPlanUnitSchema4.setPlanMark("");
        tLAPlanUnitSet.add(tLAPlanUnitSchema4);

        LAPlanUnitSchema tLAPlanUnitSchema5 = new LAPlanUnitSchema();
        tLAPlanUnitSchema5.setPlanItemType("05");  // 活动率
        tLAPlanUnitSchema5.setPlanValue("85");
        tLAPlanUnitSchema5.setPlanValue2("");
        tLAPlanUnitSchema5.setPlanMark("");
        tLAPlanUnitSet.add(tLAPlanUnitSchema5);

        LAPlanUnitSchema tLAPlanUnitSchema6 = new LAPlanUnitSchema();
        tLAPlanUnitSchema6.setPlanItemType("06");  // 年继续率
        tLAPlanUnitSchema6.setPlanValue("75");
        tLAPlanUnitSchema6.setPlanValue2("");
        tLAPlanUnitSchema6.setPlanMark("");
        tLAPlanUnitSet.add(tLAPlanUnitSchema6);

        VData cInputData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "mak002";
        tGlobalInput.ManageCom = "861100";
        tGlobalInput.ComCode = "mak002";

        cInputData.add(tGlobalInput);
        cInputData.add(tLAPlanUnitSchema);
        cInputData.add(tLAPlanUnitSet);

        LAPlanUnitUI tLAPlanUnitUI = new LAPlanUnitUI();
        boolean actualReturn = tLAPlanUnitUI.submitData(cInputData, cOperate);
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        mInputData = (VData) cInputData.clone();

        LAPlanUnitBL tLAPlanUnitBL = new LAPlanUnitBL();
        tLAPlanUnitBL.submitData(mInputData, mOperate);
        //如果有需要处理的错误，则返回
        if (tLAPlanUnitBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAPlanUnitBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAGroupUniteUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
