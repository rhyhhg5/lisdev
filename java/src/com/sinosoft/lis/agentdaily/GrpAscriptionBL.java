/*
 * <p>ClassName: GrpAscriptionBL </p>
 * <p>Description: GrpAscriptionBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAscriptionSchema;
import com.sinosoft.lis.vschema.LAAscriptionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.db.LAOrphanPolicyDB;
import com.sinosoft.lis.schema.LAOrphanPolicySchema;
import com.sinosoft.lis.schema.LAOrphanPolicyBSchema;
import com.sinosoft.lis.vschema.LAOrphanPolicyBSet;
import com.sinosoft.lis.vschema.LAOrphanPolicySet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.vschema.LJAPaySet;
import com.sinosoft.lis.db.LJAPayGrpDB;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.db.LJAGetClaimDB;
import com.sinosoft.lis.vschema.LJAGetClaimSet;
import com.sinosoft.lis.db.LJAGetEndorseDB;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.schema.LJAPayGrpSchema;
import com.sinosoft.lis.schema.LJAGetClaimSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;

public class GrpAscriptionBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private MMap mMap = new MMap();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAAscriptionSchema mLAAscriptionSchema = new LAAscriptionSchema();
    private LAAscriptionSet mLAAscriptionSet = new LAAscriptionSet();
    private String mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);

    public GrpAscriptionBL()
    {

    }

    public static void main(String[] args)
    {

    }

    /**
    * 传输数据的公共方法
    * @param: cInputData 输入的数据
    *         cOperate 数据操作
    * @return:
    */
   public boolean submitData(VData cInputData, String cOperate)
   {
       //将操作数据拷贝到本类中
       this.mOperate = cOperate;
       //得到外部传入的数据,将数据备份到本类中
       if (!getInputData(cInputData))
       {
           return false;
       }
       if (!checkData())
       {
           return false;
       }
       //进行业务处理
       if (!dealData())
       {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "GrpAscriptionBL";
           tError.functionName = "submitData";
           tError.errorMessage = "数据处理失败GrpAscriptionBL-->dealData!";
           this.mErrors.addOneError(tError);
           return false;
       }
       //准备往后台的数据
       if (!prepareOutputData())
       {
           return false;
       }
       if (this.mOperate.equals("QUERY||MAIN"))
       {
           this.submitquery();
       }
       else
       {
           System.out.println("Start GrpAscriptionBL Submit...");
           PubSubmit tPubSubmit = new PubSubmit();

           //如果有需要处理的错误，则返回
           if (!tPubSubmit.submitData(this.mInputData,""))
           {
               // @@错误处理
               this.mErrors.copyAllErrors(tPubSubmit.mErrors);
               CError tError = new CError();
               tError.moduleName = "GrpAscriptionBL";
               tError.functionName = "submitDat";
               tError.errorMessage = "数据提交失败!";
               this.mErrors.addOneError(tError);
               return false;
           }
       }
       mInputData = null;
       return true;
   }

   private boolean dealData()
  {

      String AgentNew =  this.mLAAscriptionSchema.getAgentNew();//新的代理人
      String AgentOld = this.mLAAscriptionSchema.getAgentOld();//原代理人
      String GrpContNo = this.mLAAscriptionSchema.getGrpContNo();
      String currentDate = PubFun.getCurrentDate();//当前日期
      String currentTime = PubFun.getCurrentTime();//当期时间
      String strsql= "select count(ascripno)+1 from laascription where 1=1 and " +
            "ascripstate='3' and grpcontno='"+mLAAscriptionSchema.getContNo()+"'";

      ExeSQL tExeSQL1 = new ExeSQL();
      String mCount = tExeSQL1.getOneValue(strsql);
      if (tExeSQL1.mErrors.needDealError())
       {
           CError tError = new CError();
         tError.moduleName = "GrpAscriptionBL";
         tError.functionName = "dealData";
         tError.errorMessage = "查询归属表信息出错";
         this.mErrors.addOneError(tError);
          return false;
       }
      int mCountNO=Integer.parseInt(mCount);//得到归属次数
      if(this.mOperate.equals("INSERT||MAIN"))
      {
      String mAscripNo = com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo(
              "PerAscripNo", 20);
      System.out.println("mAscripNo:"+mAscripNo);
      //插入归属表
      LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
      tLAAscriptionSchema.setSchema(mLAAscriptionSchema);
      tLAAscriptionSchema.setAscripNo(mAscripNo);
      tLAAscriptionSchema.setValidFlag("N");
      tLAAscriptionSchema.setAClass("01");
      tLAAscriptionSchema.setGrpContNo(GrpContNo);
      tLAAscriptionSchema.setOperator(mGlobalInput.Operator);
      tLAAscriptionSchema.setMakeDate(currentDate);
      tLAAscriptionSchema.setMakeTime(currentTime);
      tLAAscriptionSchema.setModifyDate(currentDate);
      tLAAscriptionSchema.setModifyTime(currentTime);
      tLAAscriptionSchema.setAscriptionCount(mCountNO);
      mMap.put(tLAAscriptionSchema, "INSERT");

      String agentSQL = "select a.agentgroup,(select branchattr from labranchgroup where agentgroup=a.agentgroup)"
             +",(select branchseries from labranchgroup where agentgroup=a.agentgroup),a.branchcode,a.name "
             +" from laagent a "
             +" where agentcode='"+AgentNew+"'"
             +" and branchtype='"+this.mLAAscriptionSchema.getBranchType()+"'"
             +" and branchtype2='"+this.mLAAscriptionSchema.getBranchType2()+"'";
      SSRS tSSRS = new SSRS();
      ExeSQL tExeSQL = new ExeSQL();
      tSSRS = tExeSQL.execSQL(agentSQL);
      if(tSSRS.getMaxRow()<0)
      {
              // @@错误处理
      this.mErrors.copyAllErrors(tSSRS.mErrors);
      CError tError = new CError();
      tError.moduleName = "GrpAscriptionBL";
      tError.functionName = "submitDat";
      tError.errorMessage = "人员新政信息查询出错!";
      this.mErrors.addOneError(tError);
      return false;
      }
      String AgentGroup = tSSRS.GetText(1, 1);
      String BranchAttr = tSSRS.GetText(1, 2);
      String BranchSeries = tSSRS.GetText(1, 3);
      String BranchCode = tSSRS.GetText(1, 4);
      String Name =  tSSRS.GetText(1, 5);

      boolean mFlag=false;
      String LAcommisionSQL="";
      String oldBranchSeries="";
      String newBranchSeries="";
      String oldAgentgroup="";
      String newAgentgroup="";
      String oldBranchSeriesSQL="select branchseries from labranchgroup where branchtype='"+this.mLAAscriptionSchema.getBranchType()+"' and agentgroup=(select agentgroup from laagent where agentcode='"+AgentOld+"')";
      String newBranchSeriesSQL="select branchseries from labranchgroup where branchtype='"+this.mLAAscriptionSchema.getBranchType()+"' and agentgroup=(select agentgroup from laagent where agentcode='"+AgentNew+"')";
      String oldAgentgroupSQL="select agentgroup from laagent where agentcode='"+AgentOld+"'";
      String newAgentgroupSQL="select agentgroup from laagent where agentcode='"+AgentNew+"'";

      SSRS mSSRS00 = new SSRS();
      ExeSQL mExeSQL00 = new ExeSQL();
      mSSRS00 = mExeSQL00.execSQL(oldBranchSeriesSQL);
      oldBranchSeries=mSSRS00.GetText(1, 1);

      SSRS mSSRS01 = new SSRS();
      ExeSQL mExeSQL01 = new ExeSQL();
      mSSRS01 = mExeSQL01.execSQL(newBranchSeriesSQL);
      newBranchSeries=mSSRS01.GetText(1, 1);

      SSRS mSSRS02 = new SSRS();
      ExeSQL mExeSQL02 = new ExeSQL();
      mSSRS02 = mExeSQL02.execSQL(oldAgentgroupSQL);
      oldAgentgroup=mSSRS02.GetText(1, 1);

      SSRS mSSRS03 = new SSRS();
      ExeSQL mExeSQL03 = new ExeSQL();
      mSSRS03 = mExeSQL03.execSQL(newAgentgroupSQL);
      newAgentgroup=mSSRS03.GetText(1, 1);
      System.out.println("~~~~~~~~~~"+oldBranchSeries.substring(0,11));
      if(!oldBranchSeries.substring(0,12).equals(newBranchSeries.substring(0,12)))
      {
        String ljapaySQL="select * from ljapay "
                         +"where agentcode='"+AgentOld+"'  and  incomeno in (select distinct grpcontno from LJAPayGrp where GrpContNo='"+GrpContNo+"'  and agentcode='"+AgentOld+"'"
                         +" union "
                         +" select distinct endorsementno from LJAPayGrp where GrpContNo='"+GrpContNo+"'  and agentcode='"+AgentOld+"' and endorsementno is not null )";
        String ljapaygrpSQL="select * from ljapaygrp where grpcontno='"+GrpContNo+"' and agentcode='"+AgentOld+"'";
        String ljagetclaimSQL="select * from ljagetclaim where grpcontno='"+GrpContNo+"' and agentcode='"+AgentOld+"'";
        String ljagetendoreSQL="select * from ljagetendorse where grpcontno='"+GrpContNo+"' and agentcode='"+AgentOld+"'";
        String ljtempfeeSQL="select * from ljtempfee where otherno='"+GrpContNo+"' and agentcode='"+AgentOld+"'";
        String tManageComSQL="select managecom from laagent where agentcode='"+AgentOld+"'";
        String tManageCom="";

        SSRS mSSRS0 = new SSRS();
        ExeSQL mExeSQL0 = new ExeSQL();
        mSSRS0 = mExeSQL0.execSQL(tManageComSQL);
        tManageCom=mSSRS0.GetText(1, 1);

        LJAPayDB tLJAPayDB=new LJAPayDB();
        LJAPaySet tLJAPaySet=tLJAPayDB.executeQuery(ljapaySQL);
//	      LJAPayGrpDB tLJAPayGrpDB=new LJAPayGrpDB();
//	      LJAPayGrpSet tLJAPayGrpSet=tLJAPayGrpDB.executeQuery(ljapaygrpSQL);
	      LJAGetClaimDB tLJAGetClaimDB=new LJAGetClaimDB();
	      LJAGetClaimSet tLJAGetClaimSet=tLJAGetClaimDB.executeQuery(ljagetclaimSQL);
	      LJAGetEndorseDB tLJAGetEndorseDB=new LJAGetEndorseDB();
	      LJAGetEndorseSet tLJAGetEndorseSet=tLJAGetEndorseDB.executeQuery(ljagetendoreSQL);
	      LJTempFeeDB tLJTempFeeDB=new LJTempFeeDB();
	      LJTempFeeSet tLJTempFeeSet=tLJTempFeeDB.executeQuery(ljtempfeeSQL);

        if(tLJAPaySet.size()>0)
        {
        mFlag=true;
        LJAPaySet qLJAPaySet=new  LJAPaySet();
        LJAPayGrpSet qLJAPayGrpSet=new  LJAPayGrpSet();
        String tLimit = PubFun.getNoLimit(tManageCom);
        String tPaySerialNo1 = PubFun1.CreateMaxNo("PAYNO", tLimit);
        long  TmP1=Long.parseLong(tPaySerialNo1);
        for(int i=1;i<=tLJAPaySet.size();i++)
        {
          String tPaySerialNo = PubFun1.CreateMaxNo("PAYNO", tLimit);
          long  TmP=Long.parseLong(tPaySerialNo);
          String mp=String.valueOf(TmP);
	  LJAPaySchema tLJAPaySchema  = new LJAPaySchema();
          LJAPaySchema sLJAPaySchema1  = new LJAPaySchema();
	  tLJAPaySchema = tLJAPaySet.get(i);
          sLJAPaySchema1=tLJAPaySchema.getSchema();
          sLJAPaySchema1.setPayNo(mp);
          sLJAPaySchema1.setMakeDate(currentDate);
          sLJAPaySchema1.setMakeTime(currentTime);
          sLJAPaySchema1.setModifyDate(currentDate);
          sLJAPaySchema1.setModifyTime(currentTime);
          sLJAPaySchema1.setSumActuPayMoney(0 -(tLJAPaySchema.getSumActuPayMoney()));
          qLJAPaySet.add(sLJAPaySchema1);

          LJAPayGrpSet sLJAPayGrpSet=new  LJAPayGrpSet();
          LJAPayGrpDB  sLJAPayGrpDB=new  LJAPayGrpDB();
          sLJAPayGrpDB.setAgentCode(tLJAPaySchema.getAgentCode());
          sLJAPayGrpDB.setGrpContNo(tLJAPaySchema.getIncomeNo());
          sLJAPayGrpDB.setPayNo(tLJAPaySchema.getPayNo()); //b kanqingkuang
          sLJAPayGrpSet=sLJAPayGrpDB.query();
          for( int j=1;j<=sLJAPayGrpSet.size();j++){
              LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();
              LJAPayGrpSchema sLJAPayGrpSchema1 = new LJAPayGrpSchema();
              tLJAPayGrpSchema = sLJAPayGrpSet.get(j);
              sLJAPayGrpSchema1=tLJAPayGrpSchema.getSchema();
              sLJAPayGrpSchema1.setPayNo(mp);
              sLJAPayGrpSchema1.setSumActuPayMoney(0 -(tLJAPayGrpSchema.getSumActuPayMoney()));
              sLJAPayGrpSchema1.setMakeDate(currentDate);
              sLJAPayGrpSchema1.setMakeTime(currentTime);
              sLJAPayGrpSchema1.setModifyDate(currentDate);
              sLJAPayGrpSchema1.setModifyTime(currentTime);
              qLJAPayGrpSet.add(sLJAPayGrpSchema1);
          }

          //fanchognzhegnshu
          //TmP=TmP+1;
          String sPaySerialNo = PubFun1.CreateMaxNo("PAYNO", tLimit);
          long  sTmP=Long.parseLong(sPaySerialNo);
          String smp=String.valueOf(sTmP);
          LJAPaySchema sLJAPaySchema2  = new LJAPaySchema();
          sLJAPaySchema2=tLJAPaySchema.getSchema();
          sLJAPaySchema2.setPayNo(smp);
          sLJAPaySchema2.setAgentCode(AgentNew);
          sLJAPaySchema2.setAgentGroup(AgentGroup);
          sLJAPaySchema2.setMakeDate(currentDate);
          sLJAPaySchema2.setMakeTime(currentTime);
          sLJAPaySchema2.setModifyDate(currentDate);
          sLJAPaySchema2.setModifyTime(currentTime);
          qLJAPaySet.add(sLJAPaySchema2);

          for(int k=1;k<=sLJAPayGrpSet.size();k++){
             LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();
             LJAPayGrpSchema sLJAPayGrpSchema2 = new LJAPayGrpSchema();
             tLJAPayGrpSchema = sLJAPayGrpSet.get(k);
             sLJAPayGrpSchema2=tLJAPayGrpSchema.getSchema();
             sLJAPayGrpSchema2.setPayNo(smp);
             sLJAPayGrpSchema2.setAgentCode(AgentNew);
             sLJAPayGrpSchema2.setAgentGroup(AgentGroup);
             sLJAPayGrpSchema2.setMakeDate(currentDate);
             sLJAPayGrpSchema2.setMakeTime(currentTime);
             sLJAPayGrpSchema2.setModifyDate(currentDate);
             sLJAPayGrpSchema2.setModifyTime(currentTime);
             qLJAPayGrpSet.add(sLJAPayGrpSchema2);
         }
         TmP=TmP+1;
        }
          mMap.put(qLJAPaySet,"INSERT");
          mMap.put(qLJAPayGrpSet,"INSERT");
      }

      if(tLJAGetClaimSet.size()>0)
        {
        mFlag=true;
        LJAGetClaimSet qLJAGetClaimSet=new  LJAGetClaimSet();
        String tLimit = PubFun.getNoLimit(tManageCom);
        String tACTUGETNONo = PubFun1.CreateMaxNo("ACTUGETNO", tLimit);
        long  TmP=Long.parseLong(tACTUGETNONo);
        String sql="select distinct ActuGetNo from LJAGetClaim  where grpcontno='"+GrpContNo+"' and agentcode='"+AgentOld+"'";
        SSRS   sSSRS = new SSRS();
        ExeSQL mSQL = new ExeSQL();
        sSSRS = mSQL.execSQL(sql);
        for(int i=1;i<=sSSRS.getMaxRow();i++){
          String mp=String.valueOf(tACTUGETNONo);
          String arr[]=  sSSRS.getRowData(i);
          LJAGetClaimDB mLJAGetClaimDB=new LJAGetClaimDB();
          LJAGetClaimSet mLJAGetClaimSet=new LJAGetClaimSet();
          mLJAGetClaimDB.setActuGetNo(arr[0]);
          mLJAGetClaimDB.setAgentCode(AgentOld);
          mLJAGetClaimDB.setGrpContNo(GrpContNo);
          mLJAGetClaimSet=mLJAGetClaimDB.query();
          for(int  j=1;j<=mLJAGetClaimSet.size();j++){
             String cmp=String.valueOf(TmP);
             LJAGetClaimSchema tLJAGetClaimSchema  = new LJAGetClaimSchema();
             LJAGetClaimSchema sLJAGetClaimSchema1  = new LJAGetClaimSchema();
             tLJAGetClaimSchema = mLJAGetClaimSet.get(j);
             sLJAGetClaimSchema1=tLJAGetClaimSchema.getSchema();
             sLJAGetClaimSchema1.setActuGetNo(cmp);
             sLJAGetClaimSchema1.setEnterAccDate(currentDate);
             sLJAGetClaimSchema1.setConfDate(currentDate);
             sLJAGetClaimSchema1.setPay(0 - (tLJAGetClaimSchema.getPay()));
             sLJAGetClaimSchema1.setMakeDate(currentDate);
             sLJAGetClaimSchema1.setMakeTime(currentTime);
             sLJAGetClaimSchema1.setModifyDate(currentDate);
             sLJAGetClaimSchema1.setModifyTime(currentTime);
             qLJAGetClaimSet.add(sLJAGetClaimSchema1);

             TmP=TmP+1;
             String smp=String.valueOf(TmP);
             LJAGetClaimSchema sLJAGetClaimSchema2  = new LJAGetClaimSchema();
             sLJAGetClaimSchema2=tLJAGetClaimSchema.getSchema();
             sLJAGetClaimSchema2.setActuGetNo(smp);
             sLJAGetClaimSchema2.setAgentCode(AgentNew);
             sLJAGetClaimSchema2.setAgentGroup(AgentGroup);
             sLJAGetClaimSchema2.setEnterAccDate(currentDate);
             sLJAGetClaimSchema2.setConfDate(currentDate);
             sLJAGetClaimSchema2.setMakeDate(currentDate);
             sLJAGetClaimSchema2.setMakeTime(currentTime);
             sLJAGetClaimSchema2.setModifyDate(currentDate);
             sLJAGetClaimSchema2.setModifyTime(currentTime);
             qLJAGetClaimSet.add(sLJAGetClaimSchema2);
            }
           TmP=TmP+1;
          }
         mMap.put(qLJAGetClaimSet,"INSERT");
        }


        if(tLJAGetEndorseSet.size()>0)
       {
       mFlag=true;
       LJAGetEndorseSet qLJAGetEndorseSet=new  LJAGetEndorseSet();
       String tLimit = PubFun.getNoLimit(tManageCom);
       String tACTUGETNONo = PubFun1.CreateMaxNo("ACTUGETNO", tLimit);
       long  TmP=Long.parseLong(tACTUGETNONo);
       for(int i=1;i<=tLJAGetEndorseSet.size();i++){
            String mp=String.valueOf(tACTUGETNONo);
            LJAGetEndorseSchema tLJAGetEndorseSchema  = new LJAGetEndorseSchema();
            LJAGetEndorseSchema sLJAGetEndorseSchema1  = new LJAGetEndorseSchema();
            tLJAGetEndorseSchema = tLJAGetEndorseSet.get(i);
            sLJAGetEndorseSchema1=tLJAGetEndorseSchema.getSchema();
            sLJAGetEndorseSchema1.setActuGetNo(mp);
            sLJAGetEndorseSchema1.setEnterAccDate(currentDate);
            sLJAGetEndorseSchema1.setGetConfirmDate(currentDate);

            sLJAGetEndorseSchema1.setGetMoney(0 -(tLJAGetEndorseSchema.getGetMoney()));
            sLJAGetEndorseSchema1.setMakeDate(currentDate);
            sLJAGetEndorseSchema1.setMakeTime(currentTime);
            sLJAGetEndorseSchema1.setModifyDate(currentDate);
            sLJAGetEndorseSchema1.setModifyTime(currentTime);
            qLJAGetEndorseSet.add(sLJAGetEndorseSchema1);

            TmP=TmP+1;
            String smp=String.valueOf(TmP);
            LJAGetEndorseSchema sLJAGetEndorseSchema2  = new LJAGetEndorseSchema();
            sLJAGetEndorseSchema2=tLJAGetEndorseSchema.getSchema();
            sLJAGetEndorseSchema2.setActuGetNo(smp);
            sLJAGetEndorseSchema2.setAgentCode(AgentNew);
            sLJAGetEndorseSchema2.setAgentGroup(AgentGroup);
            sLJAGetEndorseSchema2.setEnterAccDate(currentDate);
            sLJAGetEndorseSchema2.setGetConfirmDate(currentDate);
            sLJAGetEndorseSchema2.setMakeDate(currentDate);
            sLJAGetEndorseSchema2.setMakeTime(currentTime);
            sLJAGetEndorseSchema2.setModifyDate(currentDate);
            sLJAGetEndorseSchema2.setModifyTime(currentTime);
            qLJAGetEndorseSet.add(sLJAGetEndorseSchema2);
       }
        mMap.put(qLJAGetEndorseSet,"INSERT");
       }



       if(tLJTempFeeSet.size()>0)
            {
            mFlag=true;
            //'FCO'||tempfeeno   'FCN'||tempfeeno
            LJTempFeeSet qLJTempFeeSet=new  LJTempFeeSet();
            for(int i=1;i<=tLJTempFeeSet.size();i++){
                 LJTempFeeSchema tLJTempFeeSchema  = new LJTempFeeSchema();
                 LJTempFeeSchema sLJTempFeeSchema1  = new LJTempFeeSchema();
                 tLJTempFeeSchema = tLJTempFeeSet.get(i);
                 sLJTempFeeSchema1=tLJTempFeeSchema.getSchema();
                 sLJTempFeeSchema1.setTempFeeNo("FCO"+tLJTempFeeSchema.getTempFeeNo());
                 sLJTempFeeSchema1.setPayMoney(0 -(tLJTempFeeSchema.getPayMoney()));
                 sLJTempFeeSchema1.setMakeDate(currentDate);
                 sLJTempFeeSchema1.setMakeTime(currentTime);
                 sLJTempFeeSchema1.setModifyDate(currentDate);
                 sLJTempFeeSchema1.setModifyTime(currentTime);
                 qLJTempFeeSet.add(sLJTempFeeSchema1);

                 LJTempFeeSchema sLJTempFeeSchema2  = new LJTempFeeSchema();
                 sLJTempFeeSchema2=tLJTempFeeSchema.getSchema();
                 sLJTempFeeSchema2.setTempFeeNo("FCN"+tLJTempFeeSchema.getTempFeeNo());
                 sLJTempFeeSchema2.setAgentCode(AgentNew);
                 sLJTempFeeSchema2.setAgentGroup(AgentGroup);
                 sLJTempFeeSchema2.setMakeDate(currentDate);
                 sLJTempFeeSchema2.setMakeTime(currentTime);
                 sLJTempFeeSchema2.setModifyDate(currentDate);
                 sLJTempFeeSchema2.setModifyTime(currentTime);
                 qLJTempFeeSet.add(sLJTempFeeSchema2);

            }
             mMap.put(qLJTempFeeSet,"INSERT");
       }
//        SSRS mSSRS1 = new SSRS();
//        ExeSQL mExeSQL1 = new ExeSQL();
//        mSSRS1 = mExeSQL1.execSQL(ljapaySQL);
//
//        SSRS mSSRS2 = new SSRS();
//        ExeSQL mExeSQL2 = new ExeSQL();
//        mSSRS2 = mExeSQL2.execSQL(ljapaygrpSQL);
//
//        SSRS mSSRS3 = new SSRS();
//        ExeSQL mExeSQL3 = new ExeSQL();
//        mSSRS3 = mExeSQL3.execSQL(ljagetclaimSQL);
//
//        SSRS mSSRS4 = new SSRS();
//        ExeSQL mExeSQL4 = new ExeSQL();
//        mSSRS4 = mExeSQL4.execSQL(ljagetendoreSQL);
//
//        if(mSSRS1.getMaxRow()>0)
//        {
//          mFlag=true;
//				  String ljapaysql02="";
//				  String ljapaysql01="select PAYNO, INCOMENO, INCOMETYPE, APPNTNO, -1*SUMACTUPAYMONEY, PAYDATE,"
//			       + "'"+currentDate+"', '"+currentDate+"', APPROVECODE, APPROVEDATE, SERIALNO, OPERATOR,"
//			       + "'"+currentDate+"', '"+currentTime+"', GETNOTICENO, '"+currentDate+"', '"+currentTime
//			       +"', MANAGECOM, AGENTCOM"
//			       + ", AGENTTYPE, BANKCODE, BANKACCNO, RISKCODE, AGENTCODE, AGENTGROUP, ACCNAME,"
//			       + "STARTPAYDATE, PAYTYPEFLAG, PRTSTATEFLAG, PRINTTIMES, FINSTATE, DUEFEETYPE"
//			       + " from LJAPAY where agentcode='"+AgentOld+"' and incomeno='"+GrpContNo+"'";
//				  SSRS mSSRS001 = new SSRS();
//	        ExeSQL mExeSQL001 = new ExeSQL();
//	        mSSRS001 = mExeSQL001.execSQL(ljapaysql01);
//	        for(int i=1;i<=mSSRS001.getMaxRow();i++)
//	        {
//	        String tLimit = PubFun.getNoLimit(tManageCom);
//					String tPaySerialNo = PubFun1.CreateMaxNo("PAYNO", tLimit);
//	        String arr[]=  mSSRS001.getRowData(i);
//	        arr[0]=tPaySerialNo;
//
//	        ljapaysql02="insert into ljapay values('"+arr[0]+"','"+arr[1]+"','"+arr[2]+"','"+arr[3]+"',"+Double.parseDouble(arr[4])+",'"+arr[5]+"','"+arr[6]+"','"+arr[7]+"','"+arr[8]+"','"+arr[9]+"','"+arr[10]+"','"+arr[11]+"','"+arr[12]+"','"+arr[13]+"','"+arr[14]+"','"+arr[15]+"','"+arr[16]+"','"+arr[17]+"','"+arr[18]+"','"+arr[19]+"','"+arr[20]+"','"+arr[21]+"','"+arr[22]+"','"+arr[23]+"','"+arr[24]+"','"+arr[25]+"','"+arr[26]+"','"+arr[27]+"','"+arr[28]+"',"+Integer.parseInt(arr[29])+",'"+arr[30]+"','"+arr[31]+"')";
//	        mMap.put(ljapaysql02, "INSERT");
//	        }
//
//
//	        String ljapaysql04="";
//				  String ljapaysql03="select PAYNO, INCOMENO, INCOMETYPE, APPNTNO, SUMACTUPAYMONEY, PAYDATE,"
//		             + "'"+currentDate+"', '"+currentDate+"', APPROVECODE,APPROVEDATE, SERIALNO, OPERATOR,"
//		             + "'"+currentDate+"', '"+currentTime+"', GETNOTICENO, '"+currentDate+"', '"+currentTime+"', MANAGECOM, '"+mLAAscriptionSchema.getAgentComNew()+"'"
//		             + ", AGENTTYPE, BANKCODE, BANKACCNO, RISKCODE, '"+AgentNew+"', '"+newAgentgroup+"', ACCNAME,"
//		             + "STARTPAYDATE, PAYTYPEFLAG, PRTSTATEFLAG, PRINTTIMES, FINSTATE, DUEFEETYPE"
//		             + " from LJAPAY where agentcode='"+AgentOld+"'and incomeno='"+GrpContNo+"'";
//				  SSRS mSSRS003 = new SSRS();
//	        ExeSQL mExeSQL003 = new ExeSQL();
//	        mSSRS003 = mExeSQL003.execSQL(ljapaysql03);
//	        for(int i=1;i<=mSSRS003.getMaxRow();i++)
//	        {
//	        String tLimit = PubFun.getNoLimit(tManageCom);
//					String tPaySerialNo = PubFun1.CreateMaxNo("PAYNO", tLimit);
//	        String arr[]=  mSSRS003.getRowData(i);
//	        arr[0]=tPaySerialNo;
//	        ljapaysql04="insert into ljapay values('"+arr[0]+"','"+arr[1]+"','"+arr[2]+"','"+arr[3]+"',"+Double.parseDouble(arr[4])+",'"+arr[5]+"','"+arr[6]+"','"+arr[7]+"','"+arr[8]+"','"+arr[9]+"','"+arr[10]+"','"+arr[11]+"','"+arr[12]+"','"+arr[13]+"','"+arr[14]+"','"+arr[15]+"','"+arr[16]+"','"+arr[17]+"','"+arr[18]+"','"+arr[19]+"','"+arr[20]+"','"+arr[21]+"','"+arr[22]+"','"+arr[23]+"','"+arr[24]+"','"+arr[25]+"','"+arr[26]+"','"+arr[27]+"','"+arr[28]+"',"+Integer.parseInt(arr[29])+",'"+arr[30]+"','"+arr[31]+"')";
//	        mMap.put(ljapaysql04, "INSERT");
//	        }
//
//				}
//				//
//				if (mSSRS2.getMaxRow()>0)
//				{
//					  mFlag=true;
//					  String ljapaygrpsql06="";
//					  String ljapaygrpsql05="select GRPPOLNO, PAYCOUNT, GRPCONTNO, MANAGECOM, AGENTCOM,"
//					  + " AGENTTYPE, RISKCODE, AGENTCODE, AGENTGROUP, PAYTYPEFLAG, APPNTNO, PAYNO, "
//					  + " ENDORSEMENTNO, SUMDUEPAYMONEY, (-1)*SUMACTUPAYMONEY, PAYINTV "
//					  + " , PAYDATE, PAYTYPE, '"+currentDate+"', '"+currentDate+"', LASTPAYTODATE, CURPAYTODATE, "
//					  + "  APPROVECODE, APPROVEDATE, APPROVETIME, SERIALNO,GETNOTICENO, OPERATOR, "
//					  + "'"+currentDate+"', '"+currentTime+"',  '"+currentDate+"', '"+currentTime
//					  +"', FINSTATE from LJAPAYGRP where agentcode='"+AgentOld+"' and grpcontno='"+GrpContNo+"'";
//					  SSRS mSSRS001 = new SSRS();
//		        ExeSQL mExeSQL001 = new ExeSQL();
//		        mSSRS001 = mExeSQL001.execSQL(ljapaygrpsql05);
//		        for(int i=1;i<=mSSRS001.getMaxRow();i++)
//		        {
//		        String tLimit = PubFun.getNoLimit(tManageCom);
//						String tPaySerialNo = PubFun1.CreateMaxNo("PAYNO", tLimit);
//		        String arr[]=  mSSRS001.getRowData(i);
//		        arr[11]=tPaySerialNo;
//		        ljapaygrpsql06="insert into ljapaygrp values('"+arr[0]+"',"+Integer.parseInt(arr[1])+",'"+arr[2]
//		        +"','"+arr[3]+"','"+arr[4]+"','"+arr[5]+"','"+arr[6]+"','"+arr[7]+"','"+arr[8]+"','"+arr[9]+"','"
//		        +arr[10]+"','"+arr[11]+"','"+arr[12]+"',"
//		        +Double.parseDouble(arr[13])+","+Double.parseDouble(arr[14])+","+Double.parseDouble(arr[15])+",'"
//		        +arr[16]+"','"+arr[17]+"','"+arr[18]+"','"+arr[19]+"','"+arr[20]+"','"+arr[21]+"','"+arr[22]+"','"
//		        +arr[23]+"','"+arr[24]+"','"+arr[25]+"','"+arr[26]+"','"+arr[27]+"','"+arr[28]+"','"+arr[29]+"','"
//		        +arr[30]+"','"+arr[31]+"','"+arr[32]+"')";
//		        mMap.put(ljapaygrpsql06, "INSERT");
//		        }
//
//
//		        String ljapaygrpsql08="";
//					  String ljapaygrpsql07="select GRPPOLNO, PAYCOUNT, GRPCONTNO, MANAGECOM, AGENTCOM,"
//						  + " AGENTTYPE, RISKCODE, AGENTCODE, AGENTGROUP, PAYTYPEFLAG, APPNTNO, PAYNO, "
//						  + " ENDORSEMENTNO, SUMDUEPAYMONEY, SUMACTUPAYMONEY, PAYINTV"
//						  + " , PAYDATE, PAYTYPE, '"+currentDate+"', '"+currentDate+"', LASTPAYTODATE, CURPAYTODATE, "
//						  + "  APPROVECODE, APPROVEDATE, APPROVETIME, SERIALNO,GETNOTICENO, OPERATOR, "
//						  + "'"+currentDate+"', '"+currentTime+"',  '"+currentDate+"', '"+currentTime
//						  + "', FINSTATE from ljapaygrp where agentcode='"+AgentOld+"'and grpcontno='"+GrpContNo+"'";
//					  SSRS mSSRS003 = new SSRS();
//		        ExeSQL mExeSQL003 = new ExeSQL();
//		        mSSRS003 = mExeSQL003.execSQL(ljapaygrpsql07);
//		        for(int i=1;i<=mSSRS003.getMaxRow();i++)
//		        {
//		        String tLimit = PubFun.getNoLimit(tManageCom);
//						String tPaySerialNo = PubFun1.CreateMaxNo("PAYNO", tLimit);
//		        String arr[]=  mSSRS003.getRowData(i);
//		        arr[11]=tPaySerialNo;
//		        ljapaygrpsql08="insert into ljapaygrp values('"+arr[0]+"',"+Integer.parseInt(arr[1])+",'"+arr[2]
//		        +"','"+arr[3]+"','"+arr[4]+"','"+arr[5]+"','"+arr[6]+"','"+arr[7]+"','"+arr[8]+"','"+arr[9]+"','"
//		        +arr[10]+"','"+arr[11]+"','"+arr[12]+"',"
//		        +Double.parseDouble(arr[13])+","+Double.parseDouble(arr[14])+","+Double.parseDouble(arr[15])+",'"
//		        +arr[16]+"','"+arr[17]+"','"+arr[18]+"','"+arr[19]+"','"+arr[20]+"','"+arr[21]+"','"+arr[22]+"','"
//		        +arr[23]+"','"+arr[24]+"','"+arr[25]+"','"+arr[26]+"','"+arr[27]+"','"+arr[28]+"','"+arr[29]+"','"
//		        +arr[30]+"','"+arr[31]+"','"+arr[32]+"')";
//		        mMap.put(ljapaygrpsql08, "INSERT");
//		        }
//
//
//                      }
//				if (mSSRS3.getMaxRow()>0)
//				{
//					 mFlag=true;
//
//					  String ljagetclaimsql010="";
//					  String ljagetclaimsql09=" select ACTUGETNO, FEEFINATYPE, FEEOPERATIONTYPE, OTHERNO, OTHERNOTYPE,"
//					                         +" GETDUTYCODE, GETDUTYKIND, GRPCONTNO, CONTNO, GRPPOLNO, POLNO, KINDCODE,"
//	                                 +" RISKCODE, RISKVERSION, SALECHNL, AGENTCODE, AGENTGROUP, GETDATE,"
//	                                 +" '"+currentDate+"', '"+currentDate+"', (-1)*PAY, MANAGECOM, AGENTCOM, AGENTTYPE, GETNOTICENO,"
//	                                 +" OPCONFIRMCODE, OPCONFIRMDATE,  OPCONFIRMTIME, SERIALNO, OPERATOR,"
//	                                 +" '"+currentDate+"', '"+currentTime+"', '"+currentDate+"', '"+currentTime+"' "
//	                                 +" from LJAGETCLAIM where agentcode='"+AgentOld+"' and grpcontno='"+GrpContNo+"'";
//					  SSRS mSSRS001 = new SSRS();
//		        ExeSQL mExeSQL001 = new ExeSQL();
//		        mSSRS001 = mExeSQL001.execSQL(ljagetclaimsql09);
//		        for(int i=1;i<=mSSRS001.getMaxRow();i++)
//		        {
//		        String tLimit = PubFun.getNoLimit(tManageCom);
//		         String tACTUGETNONo = PubFun1.CreateMaxNo("ACTUGETNO", tLimit);
//		        String arr[]=  mSSRS001.getRowData(i);
//		        arr[0]=tACTUGETNONo;
//		        ljagetclaimsql010="insert into ljagetclaim values('"+arr[0]+"','"+arr[1]+"','"+arr[2]+"','"+arr[3]+"','"
//                        +arr[4]+"','"+arr[5]+"','"+arr[6]+"','"+arr[7]+"','"+arr[8]+"','"+arr[9]+"','"+arr[10]+"','"
//		        +arr[11]+"','"+arr[12]+"','"+arr[13]+"','"+arr[14]+"','"+arr[15]+"','"+arr[16]+"','"+arr[17]+"','"
//		        +arr[18]+"','"+arr[19]+"',"+Double.parseDouble(arr[20])+",'"+arr[21]+"','"+arr[22]+"','"
//		        +arr[23]+"','"+arr[24]+"','"+arr[25]+"','"+arr[26]+"','"+arr[27]+"','"+arr[28]+"','"+arr[29]+"','"
//		        +arr[30]+"','"+arr[31]+"','"+arr[32]+"','"+arr[33]+"')";
//		        mMap.put(ljagetclaimsql010, "INSERT");
//
//		        }
//
//
//		        String ljagetclaimsql012="";
//					  String ljagetclaimsql011="select ACTUGETNO, FEEFINATYPE, FEEOPERATIONTYPE, OTHERNO, OTHERNOTYPE,"
//	                                  +"GETDUTYCODE, GETDUTYKIND, GRPCONTNO, CONTNO, GRPPOLNO, POLNO, KINDCODE,"
//	                                  +"RISKCODE, RISKVERSION, SALECHNL, '"+AgentNew+"', '"+newAgentgroup+"', GETDATE,"
//	                                  +"'"+currentDate+"', '"+currentDate+"', PAY, MANAGECOM, '"+mLAAscriptionSchema.getAgentComNew()+"', AGENTTYPE, GETNOTICENO,"
//	                                  +"OPCONFIRMCODE, OPCONFIRMDATE,  OPCONFIRMTIME, SERIALNO, OPERATOR,"
//	                                  +"'"+currentDate+"', '"+currentTime+"', '"+currentDate+"', '"+currentTime+"'"
//	                                  +" from LJAGETCLAIM where agentcode='"+AgentOld+"' and grpcontno='"+GrpContNo+"'";
//					  SSRS mSSRS003 = new SSRS();
//		        ExeSQL mExeSQL003 = new ExeSQL();
//		        mSSRS003 = mExeSQL003.execSQL(ljagetclaimsql011);
//		        for(int i=1;i<=mSSRS003.getMaxRow();i++)
//		        {
//		        String tLimit = PubFun.getNoLimit(tManageCom);
//		         String tACTUGETNONo = PubFun1.CreateMaxNo("ACTUGETNO", tLimit);
//		        String arr[]=  mSSRS003.getRowData(i);
//		        arr[0]=tACTUGETNONo;
//		        ljagetclaimsql012="insert into ljagetclaim values('"+arr[0]+"','"+arr[1]+"','"+arr[2]+"','"+arr[3]
//		        +"','"+arr[4]+"','"+arr[5]+"','"+arr[6]+"','"+arr[7]+"','"+arr[8]+"','"+arr[9]+"','"+arr[10]+"','"
//		        +arr[11]+"','"+arr[12]+"','"+arr[13]+"','"+arr[14]+"','"+arr[15]+"','"+arr[16]+"','"+arr[17]+"','"
//		        +arr[18]+"','"+arr[19]+"',"+Double.parseDouble(arr[20])+",'"+arr[21]+"','"+arr[22]+"','"
//		        +arr[23]+"','"+arr[24]+"','"+arr[25]+"','"+arr[26]+"','"+arr[27]+"','"+arr[28]+"','"+arr[29]+"','"
//		        +arr[30]+"','"+arr[31]+"','"+arr[32]+"','"+arr[33]+"')";
//		        mMap.put(ljagetclaimsql012, "INSERT");
//		        }
//				}
//
//				if (mSSRS4.getMaxRow()>0)
//				{
//					 mFlag=true;
//
//					  String ljagetendorsesql010="";
//					  String ljagetendorsesql09="select ACTUGETNO, ENDORSEMENTNO, FEEOPERATIONTYPE, FEEFINATYPE, GRPCONTNO,"
//	                                   +"CONTNO, GRPPOLNO, POLNO, OTHERNO, OTHERNOTYPE, DUTYCODE, PAYPLANCODE,"
//	                                   +"APPNTNO, INSUREDNO, GETNOTICENO, GETDATE, '"+currentDate+"', '"+currentDate+"',"
//	                                   +"(-1)*GETMONEY, KINDCODE, RISKCODE, RISKVERSION, MANAGECOM, AGENTCOM, AGENTTYPE,"
//	                                   +"AGENTCODE, AGENTGROUP, GRPNAME, HANDLER, APPROVEDATE, APPROVETIME, POLTYPE,"
//	                                   +"APPROVECODE, OPERATOR, SERIALNO, '"+currentDate+"', '"+currentDate+"', '"+currentTime+"', GETFLAG, "
//	                                   +"'"+currentTime+"', FINSTATE "
//	                                   +" from LJAGETENDORSE where agentcode='"+AgentOld+"' and grpcontno='"+GrpContNo+"'";
//					  SSRS mSSRS001 = new SSRS();
//		        ExeSQL mExeSQL001 = new ExeSQL();
//		        mSSRS001 = mExeSQL001.execSQL(ljagetendorsesql09);
//		        for(int i=1;i<=mSSRS001.getMaxRow();i++)
//		        {
//		        String tLimit = PubFun.getNoLimit(tManageCom);
//		        String tACTUGETNONo = PubFun1.CreateMaxNo("ACTUGETNO", tLimit);
//		        String arr[]=  mSSRS001.getRowData(i);
//		        arr[0]=tACTUGETNONo;
//		        ljagetendorsesql010="insert into ljagetendorse values('"+arr[0]+"','"+arr[1]+"','"+arr[2]+"','"
//		        +arr[3]+"','"+arr[4]+"','"+arr[5]+"','"+arr[6]+"','"+arr[7]+"','"+arr[8]+"','"+arr[9]+"','"
//		        +arr[10]+"','"+arr[11]+"','"+arr[12]+"','"+arr[13]+"','"+arr[14]+"','"+arr[15]+"','"+arr[16]+"','"
//		        +arr[17]+"',"+Double.parseDouble(arr[18])+",'"+arr[19]+"','"+arr[20]+"','"+arr[21]+"','"
//		        +arr[22]+"','"+arr[23]+"','"+arr[24]+"','"+arr[25]+"','"+arr[26]+"','"+arr[27]+"','"+arr[28]+"','"
//		        +arr[29]+"','"+arr[30]+"','"+arr[31]+"','"+arr[32]+"','"+arr[33]+"','"+arr[34]+"','"+arr[35]+"','"
//		        +arr[36]+"','"+arr[37]+"','"+arr[38]+"','"+arr[39]+"','"+arr[40]+"')";
//		        mMap.put(ljagetendorsesql010, "INSERT");
//		        }
//
//
//		        String ljagetendorsesql012="";
//					  String ljagetendorsesql011="select ACTUGETNO, ENDORSEMENTNO, FEEOPERATIONTYPE, FEEFINATYPE, GRPCONTNO,"
//	                                    +"CONTNO, GRPPOLNO, POLNO, OTHERNO, OTHERNOTYPE, DUTYCODE, PAYPLANCODE,"
//	                                    +"APPNTNO, INSUREDNO, GETNOTICENO, GETDATE, '"+currentDate+"', '"+currentDate+"',"
//	                                    +"GETMONEY, KINDCODE, RISKCODE, RISKVERSION, MANAGECOM, '"+mLAAscriptionSchema.getAgentComNew()+"', AGENTTYPE,"
//	                                    +"'"+AgentNew+"', '"+newAgentgroup+"', AGENTGROUP, GRPNAME, HANDLER, APPROVEDATE, APPROVETIME, POLTYPE,"
//	                                    +"APPROVECODE, OPERATOR, SERIALNO, '"+currentDate+"', '"+currentDate+"', '"+currentTime+"', GETFLAG, "
//	                                    +"'"+currentTime+"', FINSTATE "
//	                                    +" from LJAGETENDORSE where agentcode='"+AgentOld+"' and grpcontno='"+GrpContNo+"'";
//					  SSRS mSSRS003 = new SSRS();
//		        ExeSQL mExeSQL003 = new ExeSQL();
//		        mSSRS003 = mExeSQL003.execSQL(ljagetendorsesql011);
//		        for(int i=1;i<=mSSRS003.getMaxRow();i++)
//		        {
//		        String tLimit = PubFun.getNoLimit(tManageCom);
//		        String tACTUGETNONo = PubFun1.CreateMaxNo("ACTUGETNO", tLimit);
//		        String arr[]=  mSSRS003.getRowData(i);
//		        arr[0]=tACTUGETNONo;
//		        ljagetendorsesql012="insert into ljagetendorse values('"+arr[0]+"','"+arr[1]+"','"+arr[2]+"','"+arr[3]+"','"+arr[4]+"','"+arr[5]+"','"+arr[6]+"','"+arr[7]+"','"+arr[8]+"','"+arr[9]+"','"+arr[10]+"','"+arr[11]+"','"+arr[12]+"','"+arr[13]+"','"+arr[14]+"','"+arr[15]+"','"+arr[16]+"','"+arr[17]+"',"+Double.parseDouble(arr[18])+",'"+arr[19]+"','"+arr[20]+"','"+arr[21]+"','"+arr[22]+"','"+arr[23]+"','"+arr[24]+"','"+arr[25]+"','"+arr[26]+"','"+arr[27]+"','"+arr[28]+"','"+arr[29]+"','"+arr[30]+"','"+arr[31]+"','"+arr[32]+"','"+arr[33]+"','"+arr[34]+"','"+arr[35]+"','"+arr[36]+"','"+arr[37]+"','"+arr[38]+"','"+arr[39]+"','"+arr[40]+"')";
//		        mMap.put(ljagetendorsesql012, "INSERT");
//		        }
//				}
      }
      else
      {
      String LJAPaySQL   = "update LJAPay set " +
                     "agentcode = '"+AgentNew+"'," +
                     "agentgroup='"+AgentGroup+"', " +
                     "modifydate = '"+currentDate+"'," +
                     "modifytime = '"+currentTime+"'," +
                     "operator = '"+this.mGlobalInput.Operator+"' " +
                     "where incomeno='"+GrpContNo+"' ";
      String LJAPayGrpSQL = "update LCPol  set " +
                     "agentcode = '"+AgentNew+"'," +
                     "agentgroup = '"+AgentGroup+"', " +
                     "modifydate = '"+currentDate+"'," +
                     "modifytime = '"+currentTime+"'," +
                     "operator = '"+this.mGlobalInput.Operator+"' " +
                     "where grpcontno='"+GrpContNo+"'";
      String LJAGETCLAIMSQL= "update LJAGETCLAIM set " +
                      "agentcode = '"+AgentNew+"'," +
                      "agentgroup='"+AgentGroup+"' ," +
                      "modifydate = '"+currentDate+"'," +
                      "modifytime = '"+currentTime+"'," +
                      "operator = '"+this.mGlobalInput.Operator+"' " +
                      "where grpcontno='"+GrpContNo+"'";
      String LJAGETENDORSESQL= "update LJAGETENDORSE set " +
                        "agentcode = '"+AgentNew+"'," +
                        "agentgroup='"+AgentGroup+"', " +
                        "modifydate = '"+currentDate+"'," +
                        "modifytime = '"+currentTime+"'," +
                        "operator = '"+this.mGlobalInput.Operator+"' " +
                        "where grpcontno='"+GrpContNo+"'";
      String LJTEMPFEESQL   = "update LJTEMPFEE   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"' ," +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             " where otherno='"+GrpContNo+"' and othernotype='7'";

      mMap.put(LJAPaySQL, "UPDATE");
      mMap.put(LJAPayGrpSQL, "UPDATE");
      mMap.put(LJAGETCLAIMSQL, "UPDATE");
      mMap.put(LJAGETENDORSESQL, "UPDATE");
      mMap.put(LJTEMPFEESQL, "UPDATE");
      }
      if(!mFlag)//所查表中无数据时才update lacommision表
      {
          //修改销售表
           LAcommisionSQL = "update LACOMMISION SET " +
                        "AGENTCODE  = '"+AgentNew+"'," +
                        "agentgroup = '"+AgentGroup+"'," +
                        "branchattr = '"+BranchAttr+"'," +
                        "branchcode = '"+BranchCode+"'," +
                        "branchseries = '"+BranchSeries+"'," +
                        "modifydate = '"+currentDate+"'," +
                        "modifytime = '"+currentTime+"'," +
                        "operator = '"+this.mGlobalInput.Operator+"' " +
                        "where grpcontno='"+GrpContNo+"' " +
                        "and agentcode='"+AgentOld+"'";
           mMap.put(LAcommisionSQL, "UPDATE");
      }



          //修改业务表
      String LCPolSQL = "update LCPol  set " +
                     "agentcode = '"+AgentNew+"'," +
                     "agentgroup = '"+AgentGroup+"', " +
                     "modifydate = '"+currentDate+"'," +
                     "modifytime = '"+currentTime+"'," +
                     "operator = '"+this.mGlobalInput.Operator+"' " +
                     "where grpcontno='"+GrpContNo+"'";

      String LCCUWSubSQL = "update LCCUWSub set " +
                     "agentcode='"+AgentNew+"'," +
                     "agentgroup='"+AgentGroup+"', " +
                     "modifydate = '"+currentDate+"'," +
                     "modifytime = '"+currentTime+"'," +
                     "operator = '"+this.mGlobalInput.Operator+"' " +
                     "where grpcontno='"+GrpContNo+"'";

      String LCCUWMasterSQL = "update LCCUWMaster set " +
                     "agentcode = '"+AgentNew+"'," +
                     "agentgroup = '"+AgentGroup+"', " +
                     "modifydate = '"+currentDate+"'," +
                     "modifytime = '"+currentTime+"'," +
                     "operator = '"+this.mGlobalInput.Operator+"' " +
                     " where grpcontno='"+GrpContNo+"'";
      String LCContSQL  = "update LCCont set " +
                     "agentcode = '"+AgentNew+"'," +
                     "agentgroup='"+AgentGroup+"', " +
                     "modifydate = '"+currentDate+"'," +
                     "modifytime = '"+currentTime+"'," +
                     "operator = '"+this.mGlobalInput.Operator+"' " +
                     "where grpcontno='"+GrpContNo+"'";

      String LJAPayPersonSQL   = "update LJAPayPerson set " +
                     "agentcode = '"+AgentNew+"'," +
                     "agentgroup='"+AgentGroup+"'," +
                     "modifydate = '"+currentDate+"'," +
                     "modifytime = '"+currentTime+"'," +
                     "operator = '"+this.mGlobalInput.Operator+"' " +
                     "where grpcontno='"+GrpContNo+"'";


//      String LCContReceiveSQL   = "update LCContReceive set " +
//                     "agentcode = '"+AgentNew+"'," +
//                     "agentname='"+Name+"', " +
//                     "modifydate = '"+currentDate+"'," +
//                     "modifytime = '"+currentTime+"'" +
//                     "where contno='"+ContNo+"' ";
//      String LCContGetPolSQL   = "update LCContGetPol set " +
//                     "agentcode = '"+AgentNew+"'," +
//                     "agentname='"+Name+"' ," +
//                     "modifydate = '"+currentDate+"'," +
//                     "modifytime = '"+currentTime+"'" +
//                     "where contno='"+ContNo+"' ";

      String LJSGETCLAIMSQL   = "update LJSGETCLAIM set " +
                     "agentcode = '"+AgentNew+"'," +
                     "agentgroup='"+AgentGroup+"' ," +
                     "modifydate = '"+currentDate+"'," +
                     "modifytime = '"+currentTime+"'," +
                     "operator = '"+this.mGlobalInput.Operator+"' " +
                     "where grpcontno='"+GrpContNo+"'";

      String LJSPAYBSQL   = "update LJSPAYB set " +
                     "agentcode = '"+AgentNew+"'," +
                     "agentgroup='"+AgentGroup+"' ," +
                     "modifydate = '"+currentDate+"'," +
                     "modifytime = '"+currentTime+"'," +
                     "operator = '"+this.mGlobalInput.Operator+"' " +
                     "where otherno='"+GrpContNo+"' ";


      String LJSPAYSQL   = "update LJSPAY  set " +
                     "agentcode = '"+AgentNew+"'," +
                     "agentgroup='"+AgentGroup+"' ," +
                     "modifydate = '"+currentDate+"'," +
                     "modifytime = '"+currentTime+"'," +
                     "operator = '"+this.mGlobalInput.Operator+"' " +
                     "where otherno='"+GrpContNo+"' ";

      String LJAGETOTHERSQL   = "update LJAGETOTHER   set " +
                     "agentcode = '"+AgentNew+"'," +
                     "agentgroup='"+AgentGroup+"' ," +
                     "modifydate = '"+currentDate+"'," +
                     "modifytime = '"+currentTime+"'," +
                     "operator = '"+this.mGlobalInput.Operator+"' " +
                     " where othernotype='8' and otherno='"+GrpContNo+"' ";

//      String LPUWSUBMAINSQL   = "update LPUWSUBMAIN set " +
//                     "agentcode = '"+AgentNew+"'," +
//                     "agentgroup='"+AgentGroup+"', " +
//                     "modifydate = '"+currentDate+"'," +
//                     "modifytime = '"+currentTime+"'," +
//                     "operator = '"+this.mGlobalInput.Operator+"' " +
//                     "where contno='"+ContNo+"' ";

//      String LPUWSUBSQL   = "update LPUWSUB  set " +
//                     "agentcode = '"+AgentNew+"'," +
//                     "agentgroup='"+AgentGroup+"', " +
//                     "modifydate = '"+currentDate+"'," +
//                     "modifytime = '"+currentTime+"'," +
//                     "operator = '"+this.mGlobalInput.Operator+"' " +
//                     "where contno='"+ContNo+"' ";
//      String LPUWMASTERMAINSQL   = "update LPUWMASTERMAIN  set " +
//                     "agentcode = '"+AgentNew+"'," +
//                     "agentgroup='"+AgentGroup+"'," +
//                     "modifydate = '"+currentDate+"'," +
//                     "modifytime = '"+currentTime+"'," +
//                     "operator = '"+this.mGlobalInput.Operator+"' " +
//                     "where contno='"+ContNo+"' ";
//      String LPUWMASTERSQL   = "update LPUWMASTER  set " +
//             "agentcode = '"+AgentNew+"'," +
//                             "agentgroup='"+AgentGroup+"' ," +
//                             "modifydate = '"+currentDate+"'," +
//                             "modifytime = '"+currentTime+"'," +
//                             "operator = '"+this.mGlobalInput.Operator+"' " +
//                                             "where contno='"+ContNo+"' ";

      String LPPOLSQL   = "update LPPOL  set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";

      String LPCUWMASTERSQL   = "update LPCUWMASTER  set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"'," +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";

      String LPCONTSQL   = "update LPCONT set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";

      String LLCLAIMUWDETAILSQL   = "update LLCLAIMUWDETAIL  set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"' ," +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where contno in (select contno from lccont where grpcontno='"+GrpContNo+"') ";

      String LLCLAIMUNDERWRITESQL   = "update LLCLAIMUNDERWRITE   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where contno in (select contno from lccont where grpcontno='"+GrpContNo+"') ";


      String LLCLAIMPOLICYSQL   = "update LLCLAIMPOLICY   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";

      String LLCLAIMDETAILSQL   = "update LLCLAIMDETAIL   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";

      String LLCASEPOLICYLSQL   = "update LLCASEPOLICY   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";




      String LJSPAYPERSONBSQL   = "update LJSPAYPERSONB   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where contno in (select contno from lccont where grpcontno='"+GrpContNo+"') ";

      String LJSPAYPERSONSQL   = "update LJSPAYPERSON   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"'," +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where contno in (select contno from lccont where grpcontno='"+GrpContNo+"') ";

      String LJSGETENDORSESQL   = "update LJSGETENDORSE   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";

      String LJSPAYGRPSQL   = "update LJSPAYGRP   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";

      String LJSPAYGRPBSQL   = "update LJSPAYGRPB   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";

      String LPGRPCONTSQL   = "update LPGRPCONT   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";
      String LBGRPCONTSQL   = "update LBGRPCONT   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";
      String LCGRPCONTSQL   = "update LCGRPCONT   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";
      String LCGRPpolSQL   = "update LCGRPpol   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";

      String LCGCUWSubSQL   = "update LCGCUWSub   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"'," +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno=(select proposalgrpcontno from lcgrpcont where grpcontno='"+GrpContNo+"')";
     String LCGCUWMASTERSQL   = "update LCGCUWMASTER   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"'," +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno=(select proposalgrpcontno from lcgrpcont where grpcontno='"+GrpContNo+"')";

      String LPGUWSUBMAINSQL   = "update LPGUWSUBMAIN   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";

      String LPGUWSUBSQL   = "update LPGUWSUB   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";

      String LPGUWMASTERMAINSQL   = "update LPGUWMASTERMAIN   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";

     String LPGUWMASTERSQL   = "update LPGUWMASTER   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";

    String LPGRPPOLSQL   = "update LPGRPPOL   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";

   String LPGCUWSUBSQL   = "update LPGCUWSUB   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";

   String LPGCUWMASTERSQL   = "update LPGCUWMASTER   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";

   String LPCUWSUBSQL   = "update LPCUWSUB   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentgroup='"+AgentGroup+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"'," +
             "operator = '"+this.mGlobalInput.Operator+"' " +
             "where grpcontno='"+GrpContNo+"' ";

   String LCContReceiveSQL   = "update LCContReceive   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentname='"+Name+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"' " +
             "where contno ='"+GrpContNo+"' ";
   String LCContGetPolSQL   = "update LCContGetPol   set " +
             "agentcode = '"+AgentNew+"'," +
             "agentname='"+Name+"', " +
             "modifydate = '"+currentDate+"'," +
             "modifytime = '"+currentTime+"' " +
             "where contno ='"+GrpContNo+"' ";

         mMap.put(LCPolSQL, "UPDATE");
         mMap.put(LCCUWSubSQL, "UPDATE");
         mMap.put(LCCUWMasterSQL, "UPDATE");
         mMap.put(LCContSQL, "UPDATE");
         mMap.put(LJAPayPersonSQL, "UPDATE");
         mMap.put(LJSGETCLAIMSQL, "UPDATE");
         mMap.put(LJSPAYBSQL, "UPDATE");
         mMap.put(LJSPAYSQL, "UPDATE");
         mMap.put(LJAGETOTHERSQL, "UPDATE");
         mMap.put(LPPOLSQL, "UPDATE");
         mMap.put(LPCUWMASTERSQL, "UPDATE");

         mMap.put(LPCONTSQL, "UPDATE");
         mMap.put(LLCLAIMUWDETAILSQL, "UPDATE");
         mMap.put(LLCLAIMUNDERWRITESQL, "UPDATE");
         mMap.put(LLCLAIMPOLICYSQL, "UPDATE");
         mMap.put(LLCLAIMDETAILSQL, "UPDATE");

         mMap.put(LLCASEPOLICYLSQL	, "UPDATE");

         mMap.put(LJSPAYPERSONBSQL, "UPDATE");
         mMap.put(LJSPAYPERSONSQL, "UPDATE");
         mMap.put(LJSGETENDORSESQL, "UPDATE");


         mMap.put(LJSPAYGRPSQL, "UPDATE");
         mMap.put(LJSPAYGRPBSQL, "UPDATE");
         mMap.put(LPGRPCONTSQL	, "UPDATE");
         mMap.put(LBGRPCONTSQL	, "UPDATE");
         mMap.put(LCGRPCONTSQL	, "UPDATE");

         mMap.put(LCGRPpolSQL	, "UPDATE");
         mMap.put(LCGCUWSubSQL		, "UPDATE");
         mMap.put(LCGCUWMASTERSQL	, "UPDATE");
         mMap.put(LPGUWSUBMAINSQL	, "UPDATE");
         mMap.put(LPGUWSUBSQL	, "UPDATE");

         mMap.put(LPGUWMASTERMAINSQL	, "UPDATE");
         mMap.put(LPGUWMASTERSQL	, "UPDATE");
         mMap.put(LPGRPPOLSQL	, "UPDATE");
         mMap.put(LPGCUWSUBSQL	, "UPDATE");
         mMap.put(LPGCUWMASTERSQL	, "UPDATE");

         mMap.put(LPCUWSUBSQL	, "UPDATE");
         mMap.put(LCContReceiveSQL	, "UPDATE");
         mMap.put(LCContGetPolSQL	, "UPDATE");

    }
        //修改核心表
    return true;
  }

   private boolean getInputData(VData cInputData)
     {
         this.mLAAscriptionSchema.setSchema((LAAscriptionSchema) cInputData.
                                            getObjectByObjectName(
                 "LAAscriptionSchema", 0));
           System.out.println("Cont="+mLAAscriptionSchema.getContNo());
         this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                     getObjectByObjectName("GlobalInput", 0));
         return true;
     }
     //验证原代理人与保单号是否符合
     private boolean checkData()
    {
       if(mLAAscriptionSchema.getGrpContNo()!=null&&!(mLAAscriptionSchema.getGrpContNo().equals("")))
       {
        String strsql="select 'Y' from lcgrpcont  where agentcode='"+mLAAscriptionSchema.getAgentOld()+"' and grpcontno='"+mLAAscriptionSchema.getGrpContNo()+"'";
        ExeSQL tstrExeSQL = new ExeSQL();
        String tCommisionSN = tstrExeSQL.getOneValue(strsql);
        if (tCommisionSN==null || tCommisionSN.equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpAscriptionBL";
            tError.functionName = "checkData";
            tError.errorMessage = "原代理人"+mLAAscriptionSchema.getAgentOld()+"没有保单号为:"+mLAAscriptionSchema.getGrpContNo()+"的保单";
            this.mErrors.addOneError(tError);
            return false;
         }
//         strsql = "select count(1) from lacomtoagent where agentcode ='"+mLAAscriptionSchema.getAgentNew()+"'and agentcom='"+mLAAscriptionSchema.getAgentComNew()+"'";
//         tstrExeSQL = new ExeSQL();
//         String tAgentToComCNT = tstrExeSQL.getOneValue(strsql);
//         if(tAgentToComCNT == null || tAgentToComCNT.equals("0") )
//         {
//                 // @@错误处理
//                CError tError = new CError();
//                tError.moduleName = "GrpAscriptionBL";
//                tError.functionName = "checkData";
//                tError.errorMessage = "新代理人"+mLAAscriptionSchema.getAgentNew()+"不是"+mLAAscriptionSchema.getAgentComNew()+"的负责人";
//                this.mErrors.addOneError(tError);
//                return false;
//         }
       }
       else
       {
         String strsql="select grpcontno from lcgrpcont  where agentcode='"+mLAAscriptionSchema.getAgentOld()+"' and uwflag<>'a'";
         ExeSQL tstrExeSQL = new ExeSQL();
         String tCommisionSN = tstrExeSQL.getOneValue(strsql);
         if (tCommisionSN==null || tCommisionSN.equals(""))
         {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "GrpAscriptionBL";
             tError.functionName = "checkData";
             tError.errorMessage = "原代理人"+mLAAscriptionSchema.getAgentOld()+"名下没有保单，不用做归属！";
             this.mErrors.addOneError(tError);
             return false;
          }

       }
     //本功能只实现

        return true;
    }
     /**
      * 准备往后层输出所需要的数据
      * 输出：如果准备数据时发生错误则返回false,否则返回true
      */
     private boolean submitquery()
    {
        return true;
    }

    private boolean prepareOutputData()
   {
       try
       {
           this.mInputData = new VData();
           this.mInputData.add(this.mGlobalInput);
           System.out.println("|||||||||||||||"+mLAAscriptionSchema.getBranchAttr());
           this.mInputData.add(mMap);

       }
       catch (Exception ex)
       {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "GrpAscriptionBL";
           tError.functionName = "prepareData";
           tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
           this.mErrors.addOneError(tError);
           return false;
       }
       return true;
   }

   public VData getResult()
   {
       return this.mResult;
   }

}
