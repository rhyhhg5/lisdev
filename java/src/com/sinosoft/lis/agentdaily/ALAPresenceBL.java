/*
 * <p>ClassName: ALAPresenceBL </p>
 * <p>Description: ALAPresenceBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.db.LAPresenceDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAPresenceSchema;
import com.sinosoft.lis.vschema.LAPresenceSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class ALAPresenceBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAPresenceSchema mLAPresenceSchema = new LAPresenceSchema();
    private LAPresenceSet mLAPresenceSet = new LAPresenceSet();
    public ALAPresenceBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAPresenceBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALAPresenceBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start ALAPresenceBL Submit...");
            ALAPresenceBLS tALAPresenceBLS = new ALAPresenceBLS();
            tALAPresenceBLS.submitData(mInputData, cOperate);
            System.out.println("End ALAPresenceBL Submit...");
            //如果有需要处理的错误，则返回
            if (tALAPresenceBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tALAPresenceBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAPresenceBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        if (mOperate.equals("INSERT||MAIN"))
        {
            //确定记录顺序号
            String tAgentCode = this.mLAPresenceSchema.getAgentCode().trim();
            String tCount;
            int i;

            String tSQL = "select MAX(Idx) from LAPresence where AgentCode = '"
                          + tAgentCode + "'";
            ExeSQL tExeSQL = new ExeSQL();
            tCount = tExeSQL.getOneValue(tSQL);
            if (tCount != null && !tCount.equals(""))
            {

                Integer tInteger = new Integer(tCount);
                i = tInteger.intValue();
                i = i + 1;
            }
            else
            {
                i = 1;
            }
            this.mLAPresenceSchema.setIdx(i);
            this.mLAPresenceSchema.setMakeDate(currentDate);
            this.mLAPresenceSchema.setMakeTime(currentTime);
            this.mLAPresenceSchema.setModifyDate(currentDate);
            this.mLAPresenceSchema.setModifyTime(currentTime);
            mLAPresenceSchema.setOperator(mGlobalInput.Operator);
        }
        if (mOperate.equals("UPDATE||MAIN"))
        {
            LAPresenceDB tLAPresenceDB = new LAPresenceDB();
            tLAPresenceDB.setAgentCode(mLAPresenceSchema.getAgentCode());
            tLAPresenceDB.setIdx(mLAPresenceSchema.getIdx());
            if (!tLAPresenceDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ALAPresenceBL";
                tError.functionName = "dealData";
                tError.errorMessage = "原差勤信息查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mLAPresenceSchema.setMakeDate(tLAPresenceDB.getMakeDate());
            this.mLAPresenceSchema.setMakeTime(tLAPresenceDB.getMakeTime());
            this.mLAPresenceSchema.setModifyDate(currentDate);
            this.mLAPresenceSchema.setModifyTime(currentTime);
            mLAPresenceSchema.setOperator(mGlobalInput.Operator);
        }
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAPresenceSchema.setSchema((LAPresenceSchema) cInputData.
                                         getObjectByObjectName(
                                                 "LAPresenceSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start ALAPresenceBLQuery Submit...");
        LAPresenceDB tLAPresenceDB = new LAPresenceDB();
        tLAPresenceDB.setSchema(this.mLAPresenceSchema);
        this.mLAPresenceSet = tLAPresenceDB.query();
        this.mResult.add(this.mLAPresenceSet);
        System.out.println("End ALAPresenceBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLAPresenceDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAPresenceDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAPresenceBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLAPresenceSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAPresenceBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
