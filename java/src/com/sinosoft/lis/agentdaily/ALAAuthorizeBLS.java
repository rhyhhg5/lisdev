/*
 * <p>ClassName: ALAAuthorizeBLS </p>
 * <p>Description: ALAAuthorizeBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-15
 */
package com.sinosoft.lis.agentdaily;


import java.sql.Connection;

import com.sinosoft.lis.db.LAAuthorizeDB;
import com.sinosoft.lis.schema.LAAuthorizeSchema;
import com.sinosoft.lis.vdb.LAAuthorizeDBSet;
import com.sinosoft.lis.vschema.LAAuthorizeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

public class ALAAuthorizeBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    public ALAAuthorizeBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        System.out.println("Start ALAAuthorizeBLS Submit...");
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            tReturn = saveLAAuthorize(cInputData);
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            tReturn = deleteLAAuthorize(cInputData);
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            tReturn = updateLAAuthorize(cInputData);
        }
        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End ALAAuthorizeBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean saveLAAuthorize(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAuthorizeBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAAuthorizeDBSet tLAAuthorizeDBSet = new LAAuthorizeDBSet(conn);
            tLAAuthorizeDBSet.set((LAAuthorizeSet) mInputData.
                                  getObjectByObjectName("LAAuthorizeSet", 0));

            LAAuthorizeDB tLAAuthorizeDB = new LAAuthorizeDB(conn);
            tLAAuthorizeDB.setAuthorObj(tLAAuthorizeDBSet.get(1).getAuthorObj());
            if (!tLAAuthorizeDB.deleteSQL())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAuthorizeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAAuthorizeBLS";
                tError.functionName = "deleteData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            System.out.println("删除完成！");
            if (!tLAAuthorizeDBSet.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAuthorizeDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAAuthorizeBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAuthorizeBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean deleteLAAuthorize(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAuthorizeBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAAuthorizeDB tLAAuthorizeDB = new LAAuthorizeDB(conn);
            tLAAuthorizeDB.setSchema((LAAuthorizeSchema) mInputData.
                                     getObjectByObjectName("LAAuthorizeSchema",
                    0));
            if (!tLAAuthorizeDB.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAuthorizeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAAuthorizeBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAuthorizeBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean updateLAAuthorize(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "ALAAuthorizeBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAAuthorizeDB tLAAuthorizeDB = new LAAuthorizeDB(conn);
            tLAAuthorizeDB.setSchema((LAAuthorizeSchema) mInputData.
                                     getObjectByObjectName("LAAuthorizeSchema",
                    0));
            if (!tLAAuthorizeDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAuthorizeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAAuthorizeBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAuthorizeBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }
}
