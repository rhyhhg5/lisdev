/*
 * <p>ClassName: AbsenceBL </p>
 * <p>Description: AbsenceBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-07-08
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class LAAddressBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

      private LAAddressSchema mLAAddressSchema = new LAAddressSchema();

    public LAAddressBL() {
    }

    public static void main(String[] args)
    {

    }

    public boolean submitData(VData cInputData, String cOperate)
  {
      //将操作数据拷贝到本类中
      this.mOperate = cOperate;
      //得到外部传入的数据,将数据备份到本类中
      if (!getInputData(cInputData))
      {
          return false;
      }
      //进行业务处理
      if (!dealData())
      {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "LAAddressBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败ALAHolsBL-->dealData!";
          this.mErrors.addOneError(tError);
          return false;
      }
      //准备往后台的数据
      if (!prepareOutputData())
      {
          return false;
      }
      if (this.mOperate.equals("QUERY||MAIN"))
      {
          this.submitquery();
      }
      else
      {
          System.out.println("Start LAAddressBL Submit...");
          LAAddressBLS tLAAddressBLS = new LAAddressBLS();
          tLAAddressBLS.submitData(mInputData, cOperate);
          System.out.println("End LAAddressBL Submit...");
          //如果有需要处理的错误，则返回
          if (tLAAddressBLS.mErrors.needDealError())
          {
              // @@错误处理
              this.mErrors.copyAllErrors(tLAAddressBLS.mErrors);
              CError tError = new CError();
              tError.moduleName = "LAAddressBL";
              tError.functionName = "submitDat";
              tError.errorMessage = "数据提交失败!";
              this.mErrors.addOneError(tError);
              return false;
          }
      }
      mInputData = null;
      return true;
  }
  private boolean getInputData(VData cInputData)
   {
       this.mLAAddressSchema.setSchema((LAAddressSchema) cInputData.
                                    getObjectByObjectName(
                                            "LAAddressSchema", 0));
     this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
  //     this.mContType=(String)cInputData.getObjectByObjectName("String",0);

       return true;
   }
   private boolean dealData()
   {
       if (mOperate.equals("INSERT||MAIN"))
       {
           String tAddressSN = com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo(
               "AGrpAddressSN", 20);
           mLAAddressSchema.setAddressSN(tAddressSN);

       }
       if (mOperate.equals("UPDATE||MAIN"))
     {
//         String tManageCom=mLAAddressSchema.getManageCom();
//         String tAddress=mLAAddressSchema.getAddress() ;
//         String tAgentGroup=mLAAddressSchema.getAgentCode() ;
//         String tZipCode=mLAAddressSchema.getZipCode();
//         String tSQL =
//                         "select AddressSN from LAAddress where Address= '"
//                         + tAddress + "' and ManageCom='"+tManageCom+"' and AgentGroup='"
//                         +tAgentGroup+"' and ZipCode='"+tZipCode+"' ";
//                 ExeSQL tExeSQL = new ExeSQL();
//                 String mAddressSN = tExeSQL.getOneValue(tSQL);
//               mLAAddressSchema.setAddressSN(mAddressSN);

     }
     if (mOperate.equals("UPDATE||MAIN"))
     {

     }

       return true;
   }
   private boolean submitquery()
   {
       return true;
   }
   private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLAAddressSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAddressBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
       {
           this.mResult .clear() ;
        //   this.mResult .add(mProtocalNo) ;
           return mResult;
    }

}
