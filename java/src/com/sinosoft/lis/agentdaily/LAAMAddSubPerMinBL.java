/*
 * <p>ClassName: LAAMAddSubPerMinBL </p>
 * <p>Description: LAAMAddSubPerMinBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2006-07-13
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LARewardPunishDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LARewardPunishSchema;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;


import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;

public class LAAMAddSubPerMinBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private int mIdx;
    private MMap mMap=new MMap();
    //tBigInteger = tBigInteger.add(tOneBigInteger);
    /** 业务处理相关变量 */
    private LARewardPunishSchema mLARewardPunishSchema = new
            LARewardPunishSchema();
    private LARewardPunishSet mLARewardPunishSet = new LARewardPunishSet();
    private LARewardPunishSet minLARewardPunishSet = new LARewardPunishSet();
    private LARewardPunishSet mupLARewardPunishSet = new LARewardPunishSet();
    private LARewardPunishSet mdeLARewardPunishSet = new LARewardPunishSet();
    private String mDonedate = "";
    private String donedate = "";
    public LAAMAddSubPerMinBL()
    {
    }

    public static void main(String[] args)
    {

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
            if (!check()) {
                return false;
            }
            //进行业务处理
            if (!dealData()) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAAMAddSubPerMinBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败LAAMAddSubPerMinBL-->dealData!";
                this.mErrors.addOneError(tError);
                return false;
            }
       
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("Start AgentWageCalSaveNewBL Submit...");
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentWageCalSaveNewBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }


        }
        mInputData = null;
        return true;
    }


    private boolean check() //已经计算过佣金的日期不能操作
    {   
    	 for(int i=1;i<=this.mLARewardPunishSet.size();i++)
       	{
    	mLARewardPunishSchema = mLARewardPunishSet.get(i);	 
        mDonedate=mLARewardPunishSchema.getWageNo().substring(0,4)+"-"+mLARewardPunishSchema.getWageNo().substring(4,6)+"-01";
        donedate = AgentPubFun.formatDate(mDonedate, "yyyy-MM-dd");

        String tAgentCom = this.mLARewardPunishSchema.getAgentCom();
        LAComDB tLAComDB = new LAComDB();
        tLAComDB.setAgentCom(tAgentCom);
        if (!tLAComDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLAComDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAMAddSubPerMinBL";
            tError.functionName = "check";
            tError.errorMessage = "查询不到代理机构" + tAgentCom + "的信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
          if(this.mOperate.equals("INSERT||MAIN")){
        	 
        	  String sql = "select count(1) from larewardpunish where branchtype='"+mLARewardPunishSet.get(i).getBranchType()+"'" +
        		  		" and branchtype2='"+mLARewardPunishSet.get(i).getBranchType2()+"' " +
        		  		" and agentcom = '"+mLARewardPunishSet.get(i).getAgentCom()+"'"+
        		  	    " and wageno = '"+mLARewardPunishSet.get(i).getWageNo()+"'"+
        		  		" and managecom = '"+mLARewardPunishSet.get(i).getManageCom()+"'"+
        		  		" and doneflag ='"+mLARewardPunishSet.get(i).getDoneFlag()+"'"+
        		  		" and aclass = '"+mLARewardPunishSet.get(i).getAClass()+"'";
        		  		
      		      ExeSQL tExeSQL = new ExeSQL();
        		  String count = tExeSQL.getOneValue(sql);
        		  System.out.println("count: "+count);
        		  if(count.compareTo("1")>=0)
        		  {
        			  CError tError = new CError();
        		      tError.moduleName = "LAWrapRateSetBL";
        		      tError.functionName = "check";
        		      tError.errorMessage = "数据库已经存在同样情况的加款或扣款,请执行修改或者删除!";
        		      this.mErrors.addOneError(tError);
        		      return false;
        		  }
        	 
          	}
          }
          
          return true;
    }
    private int getMaxNO() //计算 idx
    {
        String tAgentCode = this.mLARewardPunishSet.get(1).getAgentCode().trim();
        int i;
        String tSQL =
                "select MAX(Idx) from LARewardPunish where AgentCode = '"
                + tAgentCode + "'";
        ExeSQL tExeSQL = new ExeSQL();
        String tCount = tExeSQL.getOneValue(tSQL);
        if (tCount != null && !tCount.equals(""))
        {

            Integer tInteger = new Integer(tCount);
            i = tInteger.intValue();
            i = i + 1;
        }
        else
        {
            i = 1;
        }
        return i;

    }
    
    
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        mIdx=getMaxNO();
        for(int i=1;i<=this.mLARewardPunishSet.size();i++)
    	{
        	mLARewardPunishSchema = mLARewardPunishSet.get(i);
         if (this.mOperate.equals("INSERT||MAIN"))
          {
            
            this.mLARewardPunishSchema.setIdx(mIdx);
            this.mLARewardPunishSchema.setDoneDate(donedate);
            this.mLARewardPunishSchema.setMakeDate(currentDate);
            this.mLARewardPunishSchema.setMakeTime(currentTime);
            this.mLARewardPunishSchema.setModifyDate(currentDate);
            this.mLARewardPunishSchema.setModifyTime(currentTime);
            minLARewardPunishSet.add(mLARewardPunishSchema);
            
          
        }
        else if (this.mOperate.equals("UPDATE||MAIN"))
        {
            LARewardPunishDB tLARewardPunishDB = new LARewardPunishDB();
            LARewardPunishSet tLARewardPunishSet = new LARewardPunishSet();
            tLARewardPunishDB.setAgentCode(mLARewardPunishSchema.getAgentCode());
            int idx = mLARewardPunishSchema.getIdx();
            String sql = "select * from larewardpunish where idx ='"+idx +"' and agentcode ='0000000000'";
            tLARewardPunishSet = (LARewardPunishSet)tLARewardPunishDB.executeQuery(sql);
            if(tLARewardPunishSet.size()> 0){
            	for (int j = 1; j <= tLARewardPunishSet.size(); j++) {
				   mLARewardPunishSchema.setDoneFlag(tLARewardPunishSet.get(j).getDoneFlag());
				   mLARewardPunishSchema.setMakeDate(tLARewardPunishSet.get(j).getMakeDate());
				   mLARewardPunishSchema.setMakeTime(tLARewardPunishSet.get(j).getMakeTime());
				}
            }
            
            tLARewardPunishDB.setIdx(idx);
            if (!tLARewardPunishDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ALARewardPunish";
                tError.functionName = "dealData";
                tError.errorMessage = "原加扣款信息查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mLARewardPunishSchema.setModifyDate(currentDate);
            this.mLARewardPunishSchema.setModifyTime(currentTime);
            this.mLARewardPunishSchema.setOperator(this.mGlobalInput.Operator);
            mupLARewardPunishSet.add(mLARewardPunishSchema);
            
        }
        else if (this.mOperate.equals("DELETE||MAIN"))
        {   
            LARewardPunishDB tLARewardPunishDB = new LARewardPunishDB();
            tLARewardPunishDB.setAgentCode(mLARewardPunishSchema.getAgentCode());
            tLARewardPunishDB.setIdx(mLARewardPunishSchema.getIdx());
            if (!tLARewardPunishDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ALARewardPunish";
                tError.functionName = "dealData";
                tError.errorMessage = "原加扣款信息查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            LARewardPunishSchema tLARewardPunishSchema = new LARewardPunishSchema();
            tLARewardPunishSchema=tLARewardPunishDB.getSchema();
            mdeLARewardPunishSet.add(tLARewardPunishSchema);
          }
         mIdx=mIdx+1;
    	}  
        tReturn = true;
        return tReturn;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLARewardPunishSet.set((LARewardPunishSet) cInputData.
                                             getObjectByObjectName(
                "LARewardPunishSet", 0));

        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }
  
  

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start LAAMAddSubPerMinBLQuery Submit...");
        LARewardPunishDB tLARewardPunishDB = new LARewardPunishDB();
        tLARewardPunishDB.setSchema(this.mLARewardPunishSchema);
        this.mLARewardPunishSet = 							tLARewardPunishDB.query();
        this.mResult.add(this.mLARewardPunishSet);
        System.out.println("End LAAMAddSubPerMinBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLARewardPunishDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLARewardPunishDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAMAddSubPerMinBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }


    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            mMap.put(this.minLARewardPunishSet, "INSERT");
            mMap.put(this.mupLARewardPunishSet, "UPDATE");
            mMap.put(this.mdeLARewardPunishSet, "DELETE");
            this.mInputData.add(this.mGlobalInput);

            this.mInputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAMAddSubPerMinBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    public VData getResult()
    {
        return this.mResult;
    }


}

