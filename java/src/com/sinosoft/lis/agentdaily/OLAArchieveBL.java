/*
 * <p>ClassName: OLAArchieveBL </p>
 * <p>Description: OLAArchieveBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:05:58
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;


public class OLAArchieveBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAArchieveSchema mLAArchieveSchema = new LAArchieveSchema();
    private LAArchieveSet mLAArchieveSet = new LAArchieveSet();
    private LAArchieveSet mUpdLAArchieveSet = new LAArchieveSet();
    private LAArchieveSet mInsLAArchieveSet = new LAArchieveSet();
    private String ArchieveNo = "";
//private LAArchieveSet mLAArchieveSet=new LAArchieveSet();
    public OLAArchieveBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
//       System.out.println("transact"+transact);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAArchieveBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLAArchieveBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start OLAArchieveBL Submit...");
            OLAArchieveBLS tOLAArchieveBLS = new OLAArchieveBLS();
            tOLAArchieveBLS.submitData(mInputData, mOperate);
            System.out.println("End OLAArchieveBL Submit...");
            //如果有需要处理的错误，则返回
            if (tOLAArchieveBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tOLAArchieveBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLAArchieveBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.ArchieveNo=(String)tOLAArchieveBLS.getResult().firstElement();
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {

        if (this.mOperate.equals("INSERT||MAIN") || this.mOperate.equals("UPDATE||MAIN"))
        {

            if (!saveData())
            {
                return false;
            }
        }
//        if (this.mOperate.equals("UPDATE||MAIN"))
//        {
//            if (!updateData())
//            {
//                return false;
//            }
//        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {

        }

        return true;
    }



    private boolean saveData()
    {
        int maxItemIndex = 1;
        if (mLAArchieveSet != null && mLAArchieveSet.size() > 0)
        {
            LAArchieveSchema tLAArchieveSchema = new LAArchieveSchema();
            tLAArchieveSchema = mLAArchieveSet.get(1);
            ExeSQL tExeSQL = new ExeSQL();


            String sql =
                    "select max(ItemIdx) from laarchieve where agentcode='" +
                    tLAArchieveSchema.getAgentCode() + "' ";
            String maxItemCode = tExeSQL.getOneValue(sql);
            if (maxItemCode == null || maxItemCode.equals(""))
            {
                maxItemIndex = 1;
            }
            else
            {
                maxItemIndex = Integer.parseInt(maxItemCode) + 1;
            }
        }
        for (int i = 1; i <= mLAArchieveSet.size(); i++)
        {
            LAArchieveSchema tLAArchieveSchema = new LAArchieveSchema();
            tLAArchieveSchema = mLAArchieveSet.get(i);
            System.out.println("tLAArchieveSchema.getArchieveNo():"+tLAArchieveSchema.getArchieveNo()) ;
            if (tLAArchieveSchema.getArchieveNo()!=null && !tLAArchieveSchema.getArchieveNo().trim()  .equals("") )
            {
                System.out.println("dealData:" + mOperate);
                 String currentDate = PubFun.getCurrentDate();
                 String currentTime = PubFun.getCurrentTime();
                 tLAArchieveSchema.setMakeDate(currentDate);
                 tLAArchieveSchema.setMakeTime(currentTime);
                 tLAArchieveSchema.setModifyDate(currentDate);
                 tLAArchieveSchema.setModifyTime(currentTime);
                 tLAArchieveSchema.setOperator(mGlobalInput.Operator);
                 tLAArchieveSchema.setItemIdx(tLAArchieveSchema.getItemIdx());
                 tLAArchieveSchema.setArchieveNo(tLAArchieveSchema.getArchieveNo());
                 mUpdLAArchieveSet.add(tLAArchieveSchema) ;

            }
            else
            {

                System.out.println("dealData:" + mOperate);
                String currentDate = PubFun.getCurrentDate();
                String currentTime = PubFun.getCurrentTime();
                tLAArchieveSchema.setMakeDate(currentDate);
                tLAArchieveSchema.setMakeTime(currentTime);
                tLAArchieveSchema.setModifyDate(currentDate);
                tLAArchieveSchema.setModifyTime(currentTime);
                tLAArchieveSchema.setOperator(mGlobalInput.Operator);
                tLAArchieveSchema.setItemIdx(maxItemIndex);
                tLAArchieveSchema.setArchieveNo(ArchieveNo);
                maxItemIndex++;
                 mInsLAArchieveSet.add(tLAArchieveSchema);
            }


        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData()
    {
        for (int i = 0; i < mLAArchieveSet.size(); i++)
        {
            LAArchieveSchema tLAArchieveSchema = new LAArchieveSchema();
            tLAArchieveSchema = mLAArchieveSet.get(1);
            String sql = "select * from laarchieve where agentcode ='" +
                         mLAArchieveSet.get(i).getAgentCode() +
                         "' and ItemIdx='"
                         + mLAArchieveSet.get(i).getItemIdx() + "'"
                         + " and archieveno='" +
                         tLAArchieveSchema.getArchieveNo() + "'";
            LAArchieveDB tLAArchieveDB = new LAArchieveDB();
            LAArchieveSet tLAArchieveSet = new LAArchieveSet();
            tLAArchieveSet = tLAArchieveDB.executeQuery(sql);
            if (tLAArchieveSet == null || tLAArchieveSet.size() == 0)
            {
                CError tError = new CError();
                tError.moduleName = "LAArchieveBL";
                tError.functionName = "updataData";
                tError.errorMessage = "该档案记录不存在，不能进行修改!";
                this.mErrors.addOneError(tError);
                return false;
            }
            String currentDate = PubFun.getCurrentDate();
            String currentTime = PubFun.getCurrentTime();
            tLAArchieveSchema.setModifyDate(currentDate);
            tLAArchieveSchema.setModifyTime(currentTime);
            tLAArchieveSchema.setOperator(mGlobalInput.Operator);
            mUpdLAArchieveSet.add(tLAArchieveSchema);
//        String agentCode = mLAArchieveSet.get(i).getAgentCode();
//        tLAArchieveDB = new LAArchieveDB();
//        tLAArchieveDB.setAgentCode(agentCode);
//        if (!tLAArchieveDB.getInfo())
//        {
//            this.mErrors.copyAllErrors(tLAArchieveDB.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "LAArchieveBL";
//            tError.functionName = "submitData";
//            tError.errorMessage = "查找业务员基本信息时失败!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
//        tLAArchieveDB.setArchieveNo(mLAArchieveSet.get(i).getArchieveNo());
//        tLAArchieveDB.setItemIdx(mLAArchieveSet.get(i).getItemIdx());
//        tLAArchieveDB.setArchieveDate(mLAArchieveSet.get(i).getArchieveDate());
//        tLAArchieveDB.setPigeOnHoleDate(mLAArchieveSet.get(i).getPigeOnHoleDate());
//        tLAArchieveDB.setArchType(mLAArchieveSet.get(i).getArchType());
//        tLAArchieveDB.setArchItem(mLAArchieveSet.get(i).getArchItem());
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        return true;
    }

    public VData getResult()
    {
        this.mResult.clear();
        this.mResult.add(this.ArchieveNo);
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAArchieveSet.set((LAArchieveSet) cInputData.
                                getObjectByObjectName("LAArchieveSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        LAArchieveDB tLAArchieveDB = new LAArchieveDB();
        int i;
        for (i = 0; i < mLAArchieveSet.size(); i++)
        {

            tLAArchieveDB.setSchema(this.mLAArchieveSet.get(i));
            //如果有需要处理的错误，则返回
            if (tLAArchieveDB.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAArchieveDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAArchieveBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mInputData = null;
        }
        return true;
    }

    private boolean prepareOutputData()
    {

        try
        {
            this.mInputData.clear();
            this.mInputData.add(this.mUpdLAArchieveSet);
            this.mInputData.add(this.mInsLAArchieveSet) ;

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAArchieveBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

}
