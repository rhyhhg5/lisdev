/*
 * <p>ClassName: HDLAContinueAscriptionBL </p>
 * <p>Description: HDLAContinueAscriptionBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2016-02-25
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAscriptionSchema;
import com.sinosoft.lis.vschema.LAAscriptionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.db.LAOrphanPolicyDB;
import com.sinosoft.lis.schema.LAOrphanPolicySchema;
import com.sinosoft.lis.schema.LAOrphanPolicyBSchema;
import com.sinosoft.lis.vschema.LAOrphanPolicyBSet;
import com.sinosoft.lis.vschema.LAOrphanPolicySet;
import com.sinosoft.lis.pubfun.MMap;

public class HDLAContinueAscriptionBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private MMap mMap = new MMap();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LAAscriptionSchema mLAAscriptionSchema = new LAAscriptionSchema();

    private LAAscriptionSet mLAAscriptionSet = new LAAscriptionSet();

    private String message="";
    private String AgentNew1 = "";

    private String tContNo1 = "";
    private String tGrpContNo1 = "";

    private String updateSQL = "";

    private LAAscriptionSchema preLAAscriptionSchema = new LAAscriptionSchema();

    private LAOrphanPolicyBSet tmLAOrphanPolicyBSet = new LAOrphanPolicyBSet();

    private LAOrphanPolicySet delLAOrphanPolicyBSet = new LAOrphanPolicySet();

    private String mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);

    public HDLAContinueAscriptionBL()
    {

    }

    public static void main(String[] args)
    {

    }
    
    public String getMessage()
    {
    	return this.message;
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "HDLAContinueAscriptionBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败HDLAContinueAscriptionBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start HDLAContinueAscriptionBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();

            //如果有需要处理的错误，则返回
            if (!tPubSubmit.submitData(this.mInputData, ""))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "HDLAContinueAscriptionBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    private boolean dealData()
    {
        boolean tReturn = true;
        int tCount = 0;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        String tSQL = "select a.agentgroup,a.branchattr from labranchgroup a,laagent b where a.agentgroup=b.agentgroup and b.agentcode='"
                + mLAAscriptionSchema.getAgentOld() + "'";
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        if (tExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "HDLAContinueAscriptionBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询原业务员相应的信息出错";
            this.mErrors.addOneError(tError);
            return false;
        }

        int a = tSSRS.getMaxRow();
        if (a != 1)
        {
            CError tError = new CError();
            tError.moduleName = "HDLAContinueAscriptionBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查到原业务员相应的信息";
            this.mErrors.addOneError(tError);
            return false;
        }
        String mAgentGroup = tSSRS.GetText(1, 1);
        String mBranchAttr = tSSRS.GetText(1, 2);
        String strsql = "select count(ascripno)+1 from laascription where 1=1 and "
                + "ascripstate='3' and contno='"
                + mLAAscriptionSchema.getContNo() + "'";

        ExeSQL tExeSQL1 = new ExeSQL();
        String mCount = tExeSQL1.getOneValue(strsql);
        if (tExeSQL1.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "HDLAContinueAscriptionBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询归属表信息出错";
            this.mErrors.addOneError(tError);
            return false;
        }
        int mCountNO = Integer.parseInt(mCount);//得到归属次数

        if (mOperate.equals("INSERT||MAIN"))
        {
            String tAgentOld = mLAAscriptionSchema.getAgentOld();
            String tContNo = mLAAscriptionSchema.getContNo();
            String tGrpContNo = mLAAscriptionSchema.getGrpContNo();
            System.out.println("………………开始进行插入操作…………………");
            System.out.println("获取的个单号为：" + tContNo);
            System.out.println("获取的团单号为：" + tGrpContNo);
            // 判断是人对人的归属还是单个保单对人的归属
            if ((tContNo == null || tContNo.equals(""))&&(tGrpContNo == null || tGrpContNo.equals("")))
            {
                //可实现批量归属
                System.out.println("人对人进行批量归属！");
                String allSQL = "select distinct(contno) from lccont where agentcode='"
                        + tAgentOld + "' and uwflag<>'a' and grpcontno='00000000000000000000'";
                ExeSQL allExeSQL = new ExeSQL();
                SSRS allSSRS = new SSRS();
                allSSRS = allExeSQL.execSQL(allSQL);
                System.out.println("大小：" + allSSRS.getMaxRow());
                if (allSSRS.getMaxRow() > 0)
                {
                    for (int i = 1; i <= allSSRS.getMaxRow(); i++)
                    {
                        //对每个保单进行循环处理
                        //判断是否已经进行过一次手工分配，但是还未进行归属确认（重复校验，由于操作原因，在JS中没有校验到的）
                        String Ascrip = "select getUniteCode(agentnew) from LAAscription where agentold='"
                            + tAgentOld
                            + "' and ascripstate='2' and contno='"
                            + allSSRS.GetText(i, 1) + "' and maketype='01' and validflag='N'";
                        ExeSQL aExeSQL = new ExeSQL();
                        String flag = aExeSQL.getOneValue(Ascrip);
                        if(flag!=null&&!flag.equals(""))
                        {
                            CError tError = new CError();
                            tError.moduleName = "HDLAContinueAscriptionBL";
                            tError.functionName = "dealData";
                            tError.errorMessage = ""+allSSRS.GetText(i, 1)+" 保单已经归属给代理人"+flag+"，并未确认，请先执行修改、删除或者归属确认操作！";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                        //判断是否为孤儿单
                        tmLAOrphanPolicyBSet = new LAOrphanPolicyBSet();
                        delLAOrphanPolicyBSet = new LAOrphanPolicySet();
                        LAOrphanPolicyDB tmLAOrphanPolicyDB = new LAOrphanPolicyDB();
                        LAOrphanPolicySchema tmLAOrphanPolicySchema = new LAOrphanPolicySchema();
                        //tmLAOrphanPolicyDB.setAgentCode(tAgentOld);
                        tmLAOrphanPolicyDB.setContNo(allSSRS.GetText(i, 1));
                        if (!tmLAOrphanPolicyDB.getInfo())
                        {

                        }
                        else
                        {
                            tmLAOrphanPolicySchema = tmLAOrphanPolicyDB.getSchema();
                            //备份
                            LAOrphanPolicyBSchema tmLAOrphanPolicyBSchema = new LAOrphanPolicyBSchema();
                            Reflections tReflections = new Reflections();
                            tReflections.transFields(tmLAOrphanPolicyBSchema,tmLAOrphanPolicySchema);
                            tmLAOrphanPolicyBSchema.setEdorType("01");
                            tmLAOrphanPolicyBSchema.setEdorNo(mEdorNo);
                            tmLAOrphanPolicyBSchema.setMakeDate(currentDate);
                            tmLAOrphanPolicyBSchema.setMakeTime(currentTime);
                            tmLAOrphanPolicyBSchema.setModifyDate(currentDate);
                            tmLAOrphanPolicyBSchema.setModifyTime(currentTime);
                            tmLAOrphanPolicyBSchema.setOperator(this.mGlobalInput.Operator);
                            mMap.put(tmLAOrphanPolicyBSchema, "INSERT");
                            mMap.put(tmLAOrphanPolicySchema, "DELETE");
                        }
                        //判断是否为归属未确认的保单
                        String Ascripfail = "select 'Y' from LAAscription where agentold='"
                                + tAgentOld
                                + "' and ascripstate<>'3' and contno='"
                                + allSSRS.GetText(i, 1) + "' and maketype='02' and validflag='N'";
                        ExeSQL failExeSQL = new ExeSQL();
                        String failflag = failExeSQL.getOneValue(Ascripfail);
                        if (failflag != null && !failflag.equals(""))
                        {
                           updateSQL= "update LAAscription "
                                    +" set ValidFlag='Y'"
                                    +",modifydate='"+ currentDate+ "'"
                                    +",modifytime='"+ currentTime + "'"
                                    +",operator='"+ this.mGlobalInput.Operator+ "' "
                                    +" where agentold='"+ mLAAscriptionSchema.getAgentOld() + "'"
                                    +" and ValidFlag='N'"
                                    +" and AscripState<>'3' "
                                    +" and  contno='"+ allSSRS.GetText(i, 1) + "' ";
                            mMap.put(updateSQL, "UPDATE");
                        }
                        //进行数据插入LAAscription的准备
                        String mAscripNo = com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo("PerAscripNo", 20);
                        System.out.println("mAscripNo:" + mAscripNo);
                        System.out.println("Contno:" + allSSRS.GetText(i, 1));
                        LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
                        tLAAscriptionSchema.setSchema(mLAAscriptionSchema);
                        tLAAscriptionSchema.setAscripNo(mAscripNo);
                        tLAAscriptionSchema.setValidFlag("N");
                        tLAAscriptionSchema.setAClass("01");
                        tLAAscriptionSchema.setContNo(allSSRS.GetText(i, 1));
                        tLAAscriptionSchema.setAgentGroup(mAgentGroup);
                        tLAAscriptionSchema.setBranchAttr(mBranchAttr);
                        tLAAscriptionSchema.setAscriptionDate(mLAAscriptionSchema.getAscriptionDate());
                        tLAAscriptionSchema.setOperator(mGlobalInput.Operator);
                        tLAAscriptionSchema.setMakeDate(currentDate);
                        tLAAscriptionSchema.setMakeTime(currentTime);
                        tLAAscriptionSchema.setModifyDate(currentDate);
                        tLAAscriptionSchema.setModifyTime(currentTime);
                        tLAAscriptionSchema.setAscriptionCount(mCountNO);
                        tLAAscriptionSchema.setAscripState("2");
                        mMap.put(tLAAscriptionSchema, "INSERT");

                    }
                }
                else
                {
                	message = "没有要归属的保单!";
                }
                
                
                String allGrpSQL = "select distinct(grpcontno) from lcgrpcont where agentcode='"
                        + tAgentOld + "' and uwflag<>'a'";
                ExeSQL allGrpExeSQL = new ExeSQL();
                SSRS allGrpSSRS = new SSRS();
                allGrpSSRS = allGrpExeSQL.execSQL(allGrpSQL);
                System.out.println("大小：" + allGrpSSRS.getMaxRow());
                if (allGrpSSRS.getMaxRow() > 0)
                {
                    for (int i = 1; i <= allGrpSSRS.getMaxRow(); i++)
                    {
                        //对每个保单进行循环处理
                        //判断是否已经进行过一次手工分配，但是还未进行归属确认（重复校验，由于操作原因，在JS中没有校验到的）
                        String Ascrip = "select getUniteCode(agentnew) from LAAscription where agentold='"
                            + tAgentOld
                            + "' and ascripstate='2' and grpcontno='"
                            + allGrpSSRS.GetText(i, 1) + "' and maketype='01' and validflag='N'";
                        ExeSQL aExeSQL = new ExeSQL();
                        String flag = aExeSQL.getOneValue(Ascrip);
                        if(flag!=null&&!flag.equals(""))
                        {
                            CError tError = new CError();
                            tError.moduleName = "HDLAContinueAscriptionBL";
                            tError.functionName = "dealData";
                            tError.errorMessage = ""+allGrpSSRS.GetText(i, 1)+" 保单已经归属给代理人"+flag+"，并未确认，请先执行修改、删除或者归属确认操作！";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                        //判断是否为孤儿单
                        tmLAOrphanPolicyBSet = new LAOrphanPolicyBSet();
                        delLAOrphanPolicyBSet = new LAOrphanPolicySet();
                        LAOrphanPolicyDB tmLAOrphanPolicyDB = new LAOrphanPolicyDB();
                        LAOrphanPolicySchema tmLAOrphanPolicySchema = new LAOrphanPolicySchema();
                        //tmLAOrphanPolicyDB.setAgentCode(tAgentOld);
                        tmLAOrphanPolicyDB.setContNo(allGrpSSRS.GetText(i, 1));
                        if (!tmLAOrphanPolicyDB.getInfo())
                        {                
                        }
                        else
                        {
                            tmLAOrphanPolicySchema = tmLAOrphanPolicyDB.getSchema();
                            //备份
                            LAOrphanPolicyBSchema tmLAOrphanPolicyBSchema = new LAOrphanPolicyBSchema();
                            Reflections tReflections = new Reflections();
                            tReflections.transFields(tmLAOrphanPolicyBSchema,tmLAOrphanPolicySchema);
                            tmLAOrphanPolicyBSchema.setEdorType("01");
                            tmLAOrphanPolicyBSchema.setEdorNo(mEdorNo);
                            tmLAOrphanPolicyBSchema.setMakeDate(currentDate);
                            tmLAOrphanPolicyBSchema.setMakeTime(currentTime);
                            tmLAOrphanPolicyBSchema.setModifyDate(currentDate);
                            tmLAOrphanPolicyBSchema.setModifyTime(currentTime);
                            tmLAOrphanPolicyBSchema.setOperator(this.mGlobalInput.Operator);
                            mMap.put(tmLAOrphanPolicyBSchema, "INSERT");
                            mMap.put(tmLAOrphanPolicySchema, "DELETE");
                        }
                        //判断是否为归属未确认的保单
                        String Ascripfail = "select 'Y' from LAAscription where agentold='"
                                + tAgentOld
                                + "' and ascripstate<>'3' and grpcontno='"
                                + allGrpSSRS.GetText(i, 1) + "' and maketype='02' and validflag='N'";
                        ExeSQL failExeSQL = new ExeSQL();
                        String failflag = failExeSQL.getOneValue(Ascripfail);
                        if (failflag != null && !failflag.equals(""))
                        {
                           updateSQL= "update LAAscription "
                                    +" set ValidFlag='Y'"
                                    +",modifydate='"+ currentDate+ "'"
                                    +",modifytime='"+ currentTime + "'"
                                    +",operator='"+ this.mGlobalInput.Operator+ "' "
                                    +" where agentold='"+ mLAAscriptionSchema.getAgentOld() + "'"
                                    +" and ValidFlag='N'"
                                    +" and AscripState<>'3' "
                                    +" and  grpcontno='"+ allGrpSSRS.GetText(i, 1) + "' ";
                            mMap.put(updateSQL, "UPDATE");
                        }
                        //进行数据插入LAAscription的准备
                        String mAscripNo = com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo("PerAscripNo", 20);
                        System.out.println("mAscripNo:" + mAscripNo);
                        System.out.println("Contno:" + allGrpSSRS.GetText(i, 1));
                        LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
                        tLAAscriptionSchema.setSchema(mLAAscriptionSchema);
                        tLAAscriptionSchema.setAscripNo(mAscripNo);
                        tLAAscriptionSchema.setValidFlag("N");
                        tLAAscriptionSchema.setAClass("01");
                        tLAAscriptionSchema.setGrpContNo(allGrpSSRS.GetText(i, 1));
                        tLAAscriptionSchema.setAgentGroup(mAgentGroup);
                        tLAAscriptionSchema.setBranchAttr(mBranchAttr);
                        tLAAscriptionSchema.setAscriptionDate(mLAAscriptionSchema.getAscriptionDate());
                        tLAAscriptionSchema.setOperator(mGlobalInput.Operator);
                        tLAAscriptionSchema.setMakeDate(currentDate);
                        tLAAscriptionSchema.setMakeTime(currentTime);
                        tLAAscriptionSchema.setModifyDate(currentDate);
                        tLAAscriptionSchema.setModifyTime(currentTime);
                        tLAAscriptionSchema.setAscriptionCount(mCountNO);
                        tLAAscriptionSchema.setAscripState("2");
                        mMap.put(tLAAscriptionSchema, "INSERT");
                    }
                }
                
            }
            else
            //单张保单归属
            {
                if (tContNo != null && !tContNo.equals("")){
                //判断是否为已撤销的保单
                String ttsql = "select 'X' from lccont where contno='"+ tContNo + "' and Agentcode='"
                               + tAgentOld+ "' and uwflag='a'";
                ExeSQL ttExeSQL = new ExeSQL();
                String ttFlag = ttExeSQL.getOneValue(ttsql);
                if (ttExeSQL.mErrors.needDealError())
                {
                    CError tError = new CError();
                    tError.moduleName = "HDLAContinueAscriptionBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "查询保单表信息出错";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (ttFlag != null && !ttFlag.equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "HDLAContinueAscriptionBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = " 已做撤销的保单不做归属！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                //判断是否已经进行过一次手工分配，但是还未进行归属确认（重复校验，由于操作原因，在JS中没有校验到的）
                String Ascrip = "select getUniteCode(agentnew) from LAAscription "
                                +" where agentold='"+ tAgentOld
                                +"' and ascripstate='2' and contno='"
                                + tContNo + "' and maketype='01' and validflag='N'";
                ExeSQL aExeSQL = new ExeSQL();
                String flag = aExeSQL.getOneValue(Ascrip);
                if(flag!=null&&!flag.equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "HDLAContinueAscriptionBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = ""+tContNo+" 保单已经归属给代理人"+flag+",并未确认，请先执行修改、删除或者进行归属确认操作！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                //判断是否为孤儿单
                LAOrphanPolicyDB tmLAOrphanPolicyDB = new LAOrphanPolicyDB();
                LAOrphanPolicySchema tmLAOrphanPolicySchema = new LAOrphanPolicySchema();
                //tmLAOrphanPolicyDB.setAgentCode(tAgentOld);
                tmLAOrphanPolicyDB.setContNo(tContNo);
                if (!tmLAOrphanPolicyDB.getInfo())
                {
                }
                else
                {
                    tmLAOrphanPolicySchema = tmLAOrphanPolicyDB.getSchema();
                    //备份
                    LAOrphanPolicyBSchema tmLAOrphanPolicyBSchema = new LAOrphanPolicyBSchema();
                    Reflections tReflections = new Reflections();
                    tReflections.transFields(tmLAOrphanPolicyBSchema,
                            tmLAOrphanPolicySchema);
                    tmLAOrphanPolicyBSchema.setEdorType("02");
                    tmLAOrphanPolicyBSchema.setEdorNo(mEdorNo);
                    tmLAOrphanPolicyBSchema.setMakeDate(currentDate);
                    tmLAOrphanPolicyBSchema.setMakeTime(currentTime);
                    tmLAOrphanPolicyBSchema.setModifyDate(currentDate);
                    tmLAOrphanPolicyBSchema.setModifyTime(currentTime);
                    tmLAOrphanPolicyBSchema
                            .setOperator(this.mGlobalInput.Operator);
                    mMap.put(tmLAOrphanPolicyBSchema, "INSERT");
                    mMap.put(tmLAOrphanPolicySchema, "DELETE");
                }
                //判断是否为自动归属失败的保单
                String Ascripfail = "select 'Y' from LAAscription where agentold='"
                        + tAgentOld
                        + "'and ascripstate<>'3' and contno='"
                        + tContNo + "' and maketype='02' and validflag='N'";
                ExeSQL failExeSQL = new ExeSQL();
                String failflag = failExeSQL.getOneValue(Ascripfail);
                if (failflag != null && !failflag.equals(""))
                {
                    updateSQL = "update LAAscription set ValidFlag='Y',modifydate='"
                            + currentDate
                            + "',modifytime='"
                            + currentTime
                            + "',operator='"
                            + this.mGlobalInput.Operator
                            + "' where agentold='"
                            + mLAAscriptionSchema.getAgentOld()
                            + "'  and ValidFlag='N'"
                            + " and MakeType='02' and ascripstate<>'3' and contno='"
                            + tContNo + "'";
                    mMap.put(updateSQL, "UPDATE");
                }
                String mAscripNo = com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo(
                        "PerAscripNo", 20);
                mLAAscriptionSchema.setAscripNo(mAscripNo);
                mLAAscriptionSchema.setValidFlag("N");
                mLAAscriptionSchema.setAClass("01");
                mLAAscriptionSchema.setAgentGroup(mAgentGroup);
                mLAAscriptionSchema.setBranchAttr(mBranchAttr);
                mLAAscriptionSchema.setAscriptionDate(mLAAscriptionSchema.getAscriptionDate());
                mLAAscriptionSchema.setOperator(mGlobalInput.Operator);
                mLAAscriptionSchema.setMakeDate(currentDate);
                mLAAscriptionSchema.setMakeTime(currentTime);
                mLAAscriptionSchema.setModifyDate(currentDate);
                mLAAscriptionSchema.setModifyTime(currentTime);
                mLAAscriptionSchema.setAscriptionCount(mCountNO);
                mLAAscriptionSchema.setAscripState("2");
                mMap.put(mLAAscriptionSchema, "INSERT");
                }
                else{
                //判断是否为已撤销的保单
                String ttsql = "select 'X' from lcgrpcont where grpcontno='"+ tGrpContNo + "' and Agentcode='"
                               + tAgentOld+ "' and uwflag='a'";
                ExeSQL ttExeSQL = new ExeSQL();
                String ttFlag = ttExeSQL.getOneValue(ttsql);
                if (ttExeSQL.mErrors.needDealError())
                {
                    CError tError = new CError();
                    tError.moduleName = "HDLAContinueAscriptionBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "查询保单表信息出错";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (ttFlag != null && !ttFlag.equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "HDLAContinueAscriptionBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = " 已做撤销的保单不做归属！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                //判断是否已经进行过一次手工分配，但是还未进行归属确认（重复校验，由于操作原因，在JS中没有校验到的）
                String Ascrip = "select getUniteCode(agentnew) from LAAscription "
                                +" where agentold='"+ tAgentOld
                                +"' and ascripstate='2' and grpcontno='"
                                + tContNo + "' and maketype='01' and validflag='N'";
                ExeSQL aExeSQL = new ExeSQL();
                String flag = aExeSQL.getOneValue(Ascrip);
                if(flag!=null&&!flag.equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "HDLAContinueAscriptionBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = ""+tContNo+" 保单已经归属给代理人"+flag+",并未确认，请先执行修改、删除或者进行归属确认操作！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                //判断是否为孤儿单
                LAOrphanPolicyDB tmLAOrphanPolicyDB = new LAOrphanPolicyDB();
                LAOrphanPolicySchema tmLAOrphanPolicySchema = new LAOrphanPolicySchema();
                //tmLAOrphanPolicyDB.setAgentCode(tAgentOld);
                tmLAOrphanPolicyDB.setContNo(tContNo);
                if (!tmLAOrphanPolicyDB.getInfo())
                {
                }
                else
                {
                    tmLAOrphanPolicySchema = tmLAOrphanPolicyDB.getSchema();
                    //备份
                    LAOrphanPolicyBSchema tmLAOrphanPolicyBSchema = new LAOrphanPolicyBSchema();
                    Reflections tReflections = new Reflections();
                    tReflections.transFields(tmLAOrphanPolicyBSchema,
                            tmLAOrphanPolicySchema);
                    tmLAOrphanPolicyBSchema.setEdorType("01");
                    tmLAOrphanPolicyBSchema.setEdorNo(mEdorNo);
                    tmLAOrphanPolicyBSchema.setMakeDate(currentDate);
                    tmLAOrphanPolicyBSchema.setMakeTime(currentTime);
                    tmLAOrphanPolicyBSchema.setModifyDate(currentDate);
                    tmLAOrphanPolicyBSchema.setModifyTime(currentTime);
                    tmLAOrphanPolicyBSchema
                            .setOperator(this.mGlobalInput.Operator);
                    mMap.put(tmLAOrphanPolicyBSchema, "INSERT");
                    mMap.put(tmLAOrphanPolicySchema, "DELETE");
                }
                //判断是否为自动归属失败的保单
                String Ascripfail = "select 'Y' from LAAscription where agentold='"
                        + tAgentOld
                        + "'and ascripstate<>'3' and grpcontno='"
                        + tContNo + "' and maketype='02' and validflag='N'";
                ExeSQL failExeSQL = new ExeSQL();
                String failflag = failExeSQL.getOneValue(Ascripfail);
                if (failflag != null && !failflag.equals(""))
                {
                    updateSQL = "update LAAscription set ValidFlag='Y',modifydate='"
                            + currentDate
                            + "',modifytime='"
                            + currentTime
                            + "',operator='"
                            + this.mGlobalInput.Operator
                            + "' where agentold='"
                            + mLAAscriptionSchema.getAgentOld()
                            + "'  and ValidFlag='N'"
                            + " and MakeType='02' and ascripstate<>'3' and grpcontno='"
                            + tContNo + "'";
                    mMap.put(updateSQL, "UPDATE");
                }
                String mAscripNo = com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo(
                        "PerAscripNo", 20);
                mLAAscriptionSchema.setAscripNo(mAscripNo);
                mLAAscriptionSchema.setValidFlag("N");
                mLAAscriptionSchema.setAClass("01");
                mLAAscriptionSchema.setAgentGroup(mAgentGroup);
                mLAAscriptionSchema.setBranchAttr(mBranchAttr);
                mLAAscriptionSchema.setAscriptionDate(mLAAscriptionSchema.getAscriptionDate());
                mLAAscriptionSchema.setOperator(mGlobalInput.Operator);
                mLAAscriptionSchema.setMakeDate(currentDate);
                mLAAscriptionSchema.setMakeTime(currentTime);
                mLAAscriptionSchema.setModifyDate(currentDate);
                mLAAscriptionSchema.setModifyTime(currentTime);
                mLAAscriptionSchema.setAscriptionCount(mCountNO);
                mLAAscriptionSchema.setAscripState("2");
                mMap.put(mLAAscriptionSchema, "INSERT");
            }
            }
        }
        if (mOperate.equals("UPDATE||MAIN"))
        {
            String tAgentOld = mLAAscriptionSchema.getAgentOld();
            String tContNo = mLAAscriptionSchema.getContNo();
            String tGrpContNo = mLAAscriptionSchema.getGrpContNo();
            String tsql = "select AscripNo from LAAscription where agentnew='"
                    + AgentNew1 + "' and AgentOld='" + tAgentOld
                    + "' and AscripState='3' ";
            ExeSQL tstrExeSQL = new ExeSQL();
            String strAscripNo = tstrExeSQL.getOneValue(tsql);
            if (strAscripNo != null && !strAscripNo.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "HDLAContinueAscriptionBL";
                tError.functionName = "dealData";
                tError.errorMessage = "此代理人的保单已归属处理成功,不能再修改此条数据";
                this.mErrors.addOneError(tError);
                return false;
            }

            //如果修改数据,则要记录轨迹,把原来的数据修改为无效的历史记录,并插入一条新的数据(重新生成AscripNo)
            else
            {
                //判断是否为单张保单归属
                if ((tContNo == null || tContNo.equals("")) && (tGrpContNo == null || tGrpContNo.equals("")))
                {

                    updateSQL = "update LAAscription set ValidFlag='Y',modifydate='"
                            + currentDate
                            + "',modifytime='"
                            + currentTime
                            + "',"
                            + "operator='"
                            + this.mGlobalInput.Operator
                            + "' where agentold='"
                            + mLAAscriptionSchema.getAgentOld()
                            + "' and agentnew='"
                            + AgentNew1
                            + "' and ValidFlag='N'"
                            + " and AscripState='2' and MakeType='01'";
                    mMap.put(updateSQL, "UPDATE");
                    
                    String allSQL = "select distinct(contno) from LAAscription where agentold='"
                            + tAgentOld
                            + "' and agentnew='"
                            + AgentNew1
                            + "' and ValidFlag='N'"
                            + " and AscripState='2' and MakeType='01' and contno is not null ";
                    ExeSQL allExeSQL = new ExeSQL();
                    SSRS allSSRS = new SSRS();
                    allSSRS = allExeSQL.execSQL(allSQL);
                    System.out.println("大小：" + allSSRS.getMaxRow());
                    if (allSSRS.getMaxRow() > 0)
                    {
                        for (int i = 1; i <= allSSRS.getMaxRow(); i++)
                        {
                            //进行数据插入LAAscription的准备
                            String mAscripNo = com.sinosoft.lis.pubfun.PubFun1
                                    .CreateMaxNo("PerAscripNo", 20);
                            System.out.println("mAscripNo:" + mAscripNo);
                            System.out.println("Contno:"
                                    + allSSRS.GetText(i, 1));
                            LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
                            tLAAscriptionSchema.setSchema(mLAAscriptionSchema);
                            tLAAscriptionSchema.setAscripNo(mAscripNo);
                            tLAAscriptionSchema.setValidFlag("N");
                            tLAAscriptionSchema.setAClass("01");
                            tLAAscriptionSchema
                                    .setContNo(allSSRS.GetText(i, 1));
                            tLAAscriptionSchema.setAgentGroup(mAgentGroup);
                            tLAAscriptionSchema.setBranchAttr(mBranchAttr);
                            tLAAscriptionSchema.setAscriptionDate(mLAAscriptionSchema.getAscriptionDate());
                            tLAAscriptionSchema
                                    .setOperator(mGlobalInput.Operator);
                            tLAAscriptionSchema.setMakeDate(currentDate);
                            tLAAscriptionSchema.setMakeTime(currentTime);
                            tLAAscriptionSchema.setModifyDate(currentDate);
                            tLAAscriptionSchema.setModifyTime(currentTime);
                            tLAAscriptionSchema.setAscriptionCount(mCountNO);
                            tLAAscriptionSchema.setAscripState("2");
                            mMap.put(tLAAscriptionSchema, "INSERT");

                        }
                    }
                    
                    
                    String aSQL = "select distinct(grpcontno) from LAAscription where agentold='"
                            + tAgentOld
                            + "' and agentnew='"
                            + AgentNew1
                            + "' and ValidFlag='N'"
                            + " and AscripState='2' and MakeType='01' and grpcontno is not null ";
                    ExeSQL aExeSQL = new ExeSQL();
                    SSRS aSSRS = new SSRS();
                    aSSRS = aExeSQL.execSQL(aSQL);
                    System.out.println("大小：" + aSSRS.getMaxRow());
                    if (aSSRS.getMaxRow() > 0)
                    {
                        for (int i = 1; i <= aSSRS.getMaxRow(); i++)
                        {
                            //进行数据插入LAAscription的准备
                            String mAscripNo = com.sinosoft.lis.pubfun.PubFun1
                                    .CreateMaxNo("PerAscripNo", 20);
                            System.out.println("mAscripNo:" + mAscripNo);
                            System.out.println("Contno:"
                                    + aSSRS.GetText(i, 1));
                            LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
                            tLAAscriptionSchema.setSchema(mLAAscriptionSchema);
                            tLAAscriptionSchema.setAscripNo(mAscripNo);
                            tLAAscriptionSchema.setValidFlag("N");
                            tLAAscriptionSchema.setAClass("01");
                            tLAAscriptionSchema
                                    .setGrpContNo(aSSRS.GetText(i, 1));
                            tLAAscriptionSchema.setAgentGroup(mAgentGroup);
                            tLAAscriptionSchema.setBranchAttr(mBranchAttr);
                            tLAAscriptionSchema.setAscriptionDate(mLAAscriptionSchema.getAscriptionDate());
                            tLAAscriptionSchema
                                    .setOperator(mGlobalInput.Operator);
                            tLAAscriptionSchema.setMakeDate(currentDate);
                            tLAAscriptionSchema.setMakeTime(currentTime);
                            tLAAscriptionSchema.setModifyDate(currentDate);
                            tLAAscriptionSchema.setModifyTime(currentTime);
                            tLAAscriptionSchema.setAscriptionCount(mCountNO);
                            tLAAscriptionSchema.setAscripState("2");
                            mMap.put(tLAAscriptionSchema, "INSERT");
                        }
                    }
                    
                }
                else
                {

                    //判断是否为已撤销的保单
                     if (tContNo != null && !tContNo.equals("")){
                    String ttsql = "select 'X' from lccont where contno='"
                            + tContNo + "' and Agentcode='" + tAgentOld
                            + "' and uwflag='a'";
                    ExeSQL ttExeSQL = new ExeSQL();
                    String ttFlag = ttExeSQL.getOneValue(ttsql);
                    if (ttExeSQL.mErrors.needDealError())
                    {
                        CError tError = new CError();
                        tError.moduleName = "HDLAContinueAscriptionBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = "查询保单表信息出错";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    if (ttFlag != null && !ttFlag.equals(""))
                    {
                        CError tError = new CError();
                        tError.moduleName = "HDLAContinueAscriptionBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = " 已做撤销的保单不做归属！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    updateSQL = "update LAAscription set ValidFlag='Y',modifydate='"
                        + currentDate
                        + "',modifytime='"
                        + currentTime
                        + "',operator='"
                        + this.mGlobalInput.Operator
                        + "' where agentold='"
                        + mLAAscriptionSchema.getAgentOld()
                        + "' and agentnew='"
                        + AgentNew1
                        + "' and ValidFlag='N'"
                        + " and AscripState='2' and MakeType='01' and contno='"
                        + tContNo1 + "'";
                   mMap.put(updateSQL, "UPDATE");
                    //判断是否为孤儿单
                    LAOrphanPolicyDB tmLAOrphanPolicyDB = new LAOrphanPolicyDB();
                    LAOrphanPolicySchema tmLAOrphanPolicySchema = new LAOrphanPolicySchema();
                    tmLAOrphanPolicyDB.setAgentCode(tAgentOld);
                    tmLAOrphanPolicyDB.setContNo(tContNo);
                    if (!tmLAOrphanPolicyDB.getInfo())
                    {
                    }
                    else
                    {
                        tmLAOrphanPolicySchema = tmLAOrphanPolicyDB.getSchema();
                        //备份
                        LAOrphanPolicyBSchema tmLAOrphanPolicyBSchema = new LAOrphanPolicyBSchema();
                        Reflections tReflections = new Reflections();
                        tReflections.transFields(tmLAOrphanPolicyBSchema,
                                tmLAOrphanPolicySchema);
                        tmLAOrphanPolicyBSchema.setEdorType("01");
                        tmLAOrphanPolicyBSchema.setEdorNo(mEdorNo);
                        tmLAOrphanPolicyBSchema.setMakeDate(currentDate);
                        tmLAOrphanPolicyBSchema.setMakeTime(currentTime);
                        tmLAOrphanPolicyBSchema.setModifyDate(currentDate);
                        tmLAOrphanPolicyBSchema.setModifyTime(currentTime);
                        tmLAOrphanPolicyBSchema
                                .setOperator(this.mGlobalInput.Operator);
                        mMap.put(tmLAOrphanPolicyBSchema, "INSERT");
                        mMap.put(tmLAOrphanPolicySchema, "DELETE");
                    }

                    String mAscripNo = com.sinosoft.lis.pubfun.PubFun1
                            .CreateMaxNo("PerAscripNo", 20);
                    mLAAscriptionSchema.setAscripNo(mAscripNo);
                    mLAAscriptionSchema.setValidFlag("N");
                    mLAAscriptionSchema.setAClass("01");
                    mLAAscriptionSchema.setAgentGroup(mAgentGroup);
                    mLAAscriptionSchema.setBranchAttr(mBranchAttr);
                    mLAAscriptionSchema.setAscriptionDate(mLAAscriptionSchema.getAscriptionDate());
                    mLAAscriptionSchema.setOperator(mGlobalInput.Operator);
                    mLAAscriptionSchema.setMakeDate(currentDate);
                    mLAAscriptionSchema.setMakeTime(currentTime);
                    mLAAscriptionSchema.setModifyDate(currentDate);
                    mLAAscriptionSchema.setModifyTime(currentTime);
                    mLAAscriptionSchema.setAscriptionCount(mCountNO);
                    mLAAscriptionSchema.setAscripState("2");
                    mMap.put(mLAAscriptionSchema, "INSERT");
                     }
                     else{
                    String ttsql = "select 'X' from lcgrpcont where grpcontno='"
                            + tContNo + "' and Agentcode='" + tAgentOld
                            + "' and uwflag='a'";
                    ExeSQL ttExeSQL = new ExeSQL();
                    String ttFlag = ttExeSQL.getOneValue(ttsql);
                    if (ttExeSQL.mErrors.needDealError())
                    {
                        CError tError = new CError();
                        tError.moduleName = "HDLAContinueAscriptionBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = "查询保单表信息出错";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    if (ttFlag != null && !ttFlag.equals(""))
                    {
                        CError tError = new CError();
                        tError.moduleName = "HDLAContinueAscriptionBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = " 已做撤销的保单不做归属！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    updateSQL = "update LAAscription set ValidFlag='Y',modifydate='"
                        + currentDate
                        + "',modifytime='"
                        + currentTime
                        + "',operator='"
                        + this.mGlobalInput.Operator
                        + "' where agentold='"
                        + mLAAscriptionSchema.getAgentOld()
                        + "' and agentnew='"
                        + AgentNew1
                        + "' and ValidFlag='N'"
                        + " and AscripState='2' and MakeType='01' and grpcontno='"
                        + tContNo1 + "'";
                   mMap.put(updateSQL, "UPDATE");
                    //判断是否为孤儿单
                    LAOrphanPolicyDB tmLAOrphanPolicyDB = new LAOrphanPolicyDB();
                    LAOrphanPolicySchema tmLAOrphanPolicySchema = new LAOrphanPolicySchema();
                    //tmLAOrphanPolicyDB.setAgentCode(tAgentOld);
                    tmLAOrphanPolicyDB.setContNo(tContNo);
                    if (!tmLAOrphanPolicyDB.getInfo())
                    {
                    }
                    else
                    {
                        tmLAOrphanPolicySchema = tmLAOrphanPolicyDB.getSchema();
                        //备份
                        LAOrphanPolicyBSchema tmLAOrphanPolicyBSchema = new LAOrphanPolicyBSchema();
                        Reflections tReflections = new Reflections();
                        tReflections.transFields(tmLAOrphanPolicyBSchema,
                                tmLAOrphanPolicySchema);
                        tmLAOrphanPolicyBSchema.setEdorType("01");
                        tmLAOrphanPolicyBSchema.setEdorNo(mEdorNo);
                        tmLAOrphanPolicyBSchema.setMakeDate(currentDate);
                        tmLAOrphanPolicyBSchema.setMakeTime(currentTime);
                        tmLAOrphanPolicyBSchema.setModifyDate(currentDate);
                        tmLAOrphanPolicyBSchema.setModifyTime(currentTime);
                        tmLAOrphanPolicyBSchema
                                .setOperator(this.mGlobalInput.Operator);
                        mMap.put(tmLAOrphanPolicyBSchema, "INSERT");
                        mMap.put(tmLAOrphanPolicySchema, "DELETE");
                    }

                    String mAscripNo = com.sinosoft.lis.pubfun.PubFun1
                            .CreateMaxNo("PerAscripNo", 20);
                    mLAAscriptionSchema.setAscripNo(mAscripNo);
                    mLAAscriptionSchema.setValidFlag("N");
                    mLAAscriptionSchema.setAClass("01");
                    mLAAscriptionSchema.setAgentGroup(mAgentGroup);
                    mLAAscriptionSchema.setBranchAttr(mBranchAttr);
                    mLAAscriptionSchema.setAscriptionDate(mLAAscriptionSchema.getAscriptionDate());
                    mLAAscriptionSchema.setOperator(mGlobalInput.Operator);
                    mLAAscriptionSchema.setMakeDate(currentDate);
                    mLAAscriptionSchema.setMakeTime(currentTime);
                    mLAAscriptionSchema.setModifyDate(currentDate);
                    mLAAscriptionSchema.setModifyTime(currentTime);
                    mLAAscriptionSchema.setAscriptionCount(mCountNO);
                    mLAAscriptionSchema.setAscripState("2");
                    mMap.put(mLAAscriptionSchema, "INSERT");
                     }
                }

            }
        }

        if (mOperate.equals("DELETE||MAIN"))
        {
            //删除数据只需要将ValidFlag字段置为无效即可

            if ((this.mLAAscriptionSchema.getContNo() == null
                    || this.mLAAscriptionSchema.getContNo().equals(""))
                    &&( this.mLAAscriptionSchema.getGrpContNo() == null
                    || this.mLAAscriptionSchema.getGrpContNo().equals("")))
            {
                updateSQL = "update LAAscription set ValidFlag='Y',modifydate='"
                        + currentDate
                        + "',modifytime='"
                        + currentTime
                        + "',operator='"
                        + this.mGlobalInput.Operator
                        + "' where agentold='"
                        + mLAAscriptionSchema.getAgentOld()
                        + "' and agentnew='"
                        + this.mLAAscriptionSchema.getAgentNew()
                        + "' and ValidFlag='N'"
                        + " and AscripState='2' and MakeType='01'";
            }
            else
            {
            	String tContNo = mLAAscriptionSchema.getContNo();
                String tGrpContNo = mLAAscriptionSchema.getGrpContNo();  
            	if (tContNo != null && !tContNo.equals("")){
                updateSQL = "UPDATE LAASCRIPTION set ValidFlag='Y',modifydate='"
                        + currentDate
                        + "',modifytime='"
                        + currentTime
                        + "',operator='"
                        + this.mGlobalInput.Operator
                        + "' where agentold='"
                        + mLAAscriptionSchema.getAgentOld()
                        + "' and agentnew='"
                        + this.mLAAscriptionSchema.getAgentNew()
                        + "' and ValidFlag='N'"
                        + " and AscripState='2' and MakeType='01' and contno='"
                        + this.mLAAscriptionSchema.getContNo() + "'";
                      }
                 else{
                 	updateSQL = "UPDATE LAASCRIPTION set ValidFlag='Y',modifydate='"
                        + currentDate
                        + "',modifytime='"
                        + currentTime
                        + "',operator='"
                        + this.mGlobalInput.Operator
                        + "' where agentold='"
                        + mLAAscriptionSchema.getAgentOld()
                        + "' and agentnew='"
                        + this.mLAAscriptionSchema.getAgentNew()
                        + "' and ValidFlag='N'"
                        + " and AscripState='2' and MakeType='01' and grpcontno='"
                        + this.mLAAscriptionSchema.getGrpContNo() + "'";
                 	}     
            }
            mMap.put(updateSQL, "UPDATE");
        }
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        this.mLAAscriptionSchema.setSchema((LAAscriptionSchema) cInputData
                .getObjectByObjectName("LAAscriptionSchema", 0));
        System.out.println("Cont=" + mLAAscriptionSchema.getContNo());
        this.mGlobalInput.setSchema((GlobalInput) cInputData
                .getObjectByObjectName("GlobalInput", 0));
        AgentNew1 = (String) cInputData.get(2);
        tContNo1 = (String) cInputData.get(3);
        tGrpContNo1 = (String) cInputData.get(4);
        System.out.println("AgentNew1:" + AgentNew1);
        System.out.println("tContNo1:" + tContNo1);
        System.out.println("????????????????");
        return true;
    }

    //验证原代理人与保单号是否符合
    private boolean checkData()
    {
        if (mLAAscriptionSchema.getContNo() != null
                && !(mLAAscriptionSchema.getContNo().equals("")))
        {
            String strsql1 = "select prtno from lccont  where agentcode='"
                    + mLAAscriptionSchema.getAgentOld() + "' and contno='"
                    + mLAAscriptionSchema.getContNo() + "'";
            ExeSQL tstrExeSQL = new ExeSQL();
            String tPrtno = tstrExeSQL.getOneValue(strsql1);
            if (tPrtno == null || tPrtno.equals(""))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAContinueAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "原代理人"
                        + mLAAscriptionSchema.getAgentOld() + "没有保单号为:"
                        + mLAAscriptionSchema.getContNo() + "的保单";
                this.mErrors.addOneError(tError);
                return false;
            }
            String   tSQL="select '1' from lccont b "
                + "where b.uwflag = 'a'  and b.PrtNo='"
                + tPrtno+"'";
            ExeSQL tExeSQL = new ExeSQL();
            String flag = tExeSQL.getOneValue(tSQL);
            if ("1".equals(flag))
            {
                //           	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAContinueAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "" 
                        + "印刷号为"+tPrtno
                        +"的保单已撤单，所以不能对该保单进行保单分配处理！";
                this.mErrors.addOneError(tError);
                return false;
            }
            tSQL="select '1' from lccont b "
                + "where b.uwflag = '1'  and b.PrtNo='"
                + tPrtno+"'";
            tExeSQL = new ExeSQL();
            flag = tExeSQL.getOneValue(tSQL);
            if ("1".equals(flag))
            {
                //           	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAContinueAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "" 
                        + "印刷号为"+tPrtno
                        +"的保单已拒保，所以不能对该保单进行保单分配处理！";
                this.mErrors.addOneError(tError);
                return false;
            }
            tSQL="select '1' from lccont b "
                + "where (b.uwflag = '8' or b.uwflag='2')  and b.PrtNo='"
                + tPrtno+"'";
            //tExeSQL = new ExeSQL();
            flag = tExeSQL.getOneValue(tSQL);
            if ("1".equals(flag))
            {
                //           	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAContinueAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "" 
                        + "印刷号为"+tPrtno
                        +"的保单为延期承保，所以不能对该保单进行保单分配处理！";
                this.mErrors.addOneError(tError);
                return false;
            }
            tSQL="select '1' from lccont b "
                + "where b.uwflag <> '8' and b.uwflag<>'2' and b.uwflag<>'a' and b.uwflag<>'1'  and b.PrtNo='"
                + tPrtno+"' and b.signdate is null and b.prtno not in (" +
                       		"select prtno from lcrnewstatelog where state!='6' and contno='" +
                       		mLAAscriptionSchema.getContNo()+"' )";
            //tExeSQL = new ExeSQL();
            flag = tExeSQL.getOneValue(tSQL);
            if ("1".equals(flag))
            {
                //           	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAContinueAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "" 
                        + "印刷号为"+tPrtno
                        +"的保单没有签单，所以不能对该保单进行保单分配处理！";
                this.mErrors.addOneError(tError);
                return false;
            }
            tSQL="select '1' from lccont b "
                + "where b.uwflag <> '8' and b.uwflag<>'2' and b.uwflag<>'a' and b.uwflag<>'1'  and b.PrtNo='"
                + tPrtno+"' and b.signdate is not null "
                +" and (b.customgetpoldate is  null or b.getpoldate is null) with ur";
            //tExeSQL = new ExeSQL();
            flag = tExeSQL.getOneValue(tSQL);
            if ("1".equals(flag))
            {
                //           	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAContinueAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "" 
                        + "印刷号为"+tPrtno
                        +"的保单未回执回销，所以不能对该保单进行保单分配处理！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }   
            if (mLAAscriptionSchema.getGrpContNo() != null
                && !(mLAAscriptionSchema.getGrpContNo().equals("")))
          {
            String strsql2 = "select prtno from lcgrpcont  where agentcode='"
                    + mLAAscriptionSchema.getAgentOld() + "' and grpcontno='"
                    + mLAAscriptionSchema.getGrpContNo() + "'";
            ExeSQL tsExeSQL = new ExeSQL();
            String tsPrtno = tsExeSQL.getOneValue(strsql2);
            if (tsPrtno == null || tsPrtno.equals(""))
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAContinueAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "原代理人"
                        + mLAAscriptionSchema.getAgentOld() + "没有保单号为:"
                        + mLAAscriptionSchema.getGrpContNo() + "的保单";
                this.mErrors.addOneError(tError);
                return false;
            }
            String   tSQL1="select '1' from lcgrpcont b "
                + "where b.uwflag = 'a'  and b.PrtNo='"
                + tsPrtno+"'";
            ExeSQL tExeSQL1 = new ExeSQL();
            String flag1 = tExeSQL1.getOneValue(tSQL1);
            if ("1".equals(flag1))
            {
                //           	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAContinueAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "" 
                        + "印刷号为"+tsPrtno
                        +"的保单已撤单，所以不能对该保单进行保单分配处理！";
                this.mErrors.addOneError(tError);
                return false;
            }
            tSQL1="select '1' from lcgrpcont b "
                + "where b.uwflag = '1'  and b.PrtNo='"
                + tsPrtno+"'";
            tExeSQL1 = new ExeSQL();
            flag1 = tExeSQL1.getOneValue(tSQL1);
            if ("1".equals(flag1))
            {
                //           	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAContinueAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "" 
                        + "印刷号为"+tsPrtno
                        +"的保单已拒保，所以不能对该保单进行保单分配处理！";
                this.mErrors.addOneError(tError);
                return false;
            }
            tSQL1="select '1' from lcgrpcont b "
                + "where (b.uwflag = '8' or b.uwflag='2')  and b.PrtNo='"
                + tsPrtno+"'";
            //tExeSQL = new ExeSQL();
            flag1 = tExeSQL1.getOneValue(tSQL1);
            if ("1".equals(flag1))
            {
                //           	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAContinueAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "" 
                        + "印刷号为"+tsPrtno
                        +"的保单为延期承保，所以不能对该保单进行保单分配处理！";
                this.mErrors.addOneError(tError);
                return false;
            }
            tSQL1="select '1' from lcgrpcont b "
                + "where b.uwflag <> '8' and b.uwflag<>'2' and b.uwflag<>'a' and b.uwflag<>'1'  and b.PrtNo='"
                + tsPrtno+"' and b.signdate is null and b.prtno not in (" +
                       		"select prtno from lcrnewstatelog where state!='6' and grpcontno='" +
                       		mLAAscriptionSchema.getGrpContNo()+"' )";
            //tExeSQL = new ExeSQL();
            flag1 = tExeSQL1.getOneValue(tSQL1);
            if ("1".equals(flag1))
            {
                //           	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAContinueAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "" 
                        + "印刷号为"+tsPrtno
                        +"的保单没有签单，所以不能对该保单进行保单分配处理！";
                this.mErrors.addOneError(tError);
                return false;
            }
            tSQL1="select '1' from lcgrpcont b "
                + "where b.uwflag <> '8' and b.uwflag<>'2' and b.uwflag<>'a' and b.uwflag<>'1'  and b.PrtNo='"
                + tsPrtno+"' and b.signdate is not null "
                +" and (b.customgetpoldate is  null or b.getpoldate is null) with ur";
            //tExeSQL = new ExeSQL();
            flag1 = tExeSQL1.getOneValue(tSQL1);
            if ("1".equals(flag1))
            {
                //           	 @@错误处理
                CError tError = new CError();
                tError.moduleName = "HDLAContinueAscriptionBL";
                tError.functionName = "checkData";
                tError.errorMessage = "" 
                        + "印刷号为"+tsPrtno
                        +"的保单未回执回销，所以不能对该保单进行保单分配处理！";
                this.mErrors.addOneError(tError);
                return false;
            }
          }
        //校验是否跨机构分配
        String sql = "select managecom from laagent where agentcode='"
                + this.mLAAscriptionSchema.getAgentNew()
                + "' and branchtype='1'";
        ExeSQL tExeSQL = new ExeSQL();
        String managecom = tExeSQL.getOneValue(sql);
        if (!this.mLAAscriptionSchema.getManageCom().equals(managecom))
        {
            CError tError = new CError();
            tError.moduleName = "HDLAContinueAscriptionBL";
            tError.functionName = "checkData";
            tError.errorMessage = "现代理人与原代理人机构不一致！请在同一机构下做保单分配！";
            this.mErrors.addOneError(tError);
            return false;
        }
  

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            System.out.println("|||||||||||||||"+ mLAAscriptionSchema.getBranchAttr());
           this.mInputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "HDLAContinueAscriptionBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

}
