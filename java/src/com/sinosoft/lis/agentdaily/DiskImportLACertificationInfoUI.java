package com.sinosoft.lis.agentdaily;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author luomin
 * @version 1.1
 */
public class DiskImportLACertificationInfoUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private DiskImportLACertificationInfoBL mDiskImportLACertificationInfoBL = null;

    public DiskImportLACertificationInfoUI()
    {
        mDiskImportLACertificationInfoBL = new DiskImportLACertificationInfoBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
        if(!mDiskImportLACertificationInfoBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mDiskImportLACertificationInfoBL.mErrors);
            return false;
        }
        //
        if(mDiskImportLACertificationInfoBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mDiskImportLACertificationInfoBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mDiskImportLACertificationInfoBL.getImportPersons();
    }



    public static void main(String[] args)
    {
        DiskImportLACertificationInfoUI zDiskImportAgentInfoUI = new DiskImportLACertificationInfoUI();
    }
}
