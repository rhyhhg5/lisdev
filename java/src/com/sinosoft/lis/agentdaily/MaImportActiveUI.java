package com.sinosoft.lis.agentdaily;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author zhangxinjing
 * @version 1.1
 */
public class MaImportActiveUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private MaImportActiveBL mMaImportActiveBL = null;

    public MaImportActiveUI()
    {
    	mMaImportActiveBL = new MaImportActiveBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	System.out.println("UI submited11111111111111111111111111111111111111");
    	if(!mMaImportActiveBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mMaImportActiveBL.mErrors);
            return false;
        }
        //
        if(mMaImportActiveBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mMaImportActiveBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mMaImportActiveBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mMaImportActiveBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
    	MaImportActiveUI zMaImportBRCDSUI = new MaImportActiveUI();
    }
}
