package com.sinosoft.lis.agentdaily;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAMettingSchema;
import com.sinosoft.lis.vschema.LAMettingSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class LAMettingAssessUI {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
   public CErrors mErrors = new CErrors();
   private VData mResult = new VData();
   /** 往后面传输数据的容器 */
   private VData mInputData = new VData();
   /** 数据操作字符串 */
   private String mOperate;
//业务处理相关变量
   /** 全局数据 */
   private GlobalInput mGlobalInput = new GlobalInput();
   private LAMettingSchema mLAMettingSchema = new LAMettingSchema();
   private LAMettingSet mLAMettingSet = new LAMettingSet();

  public LAMettingAssessUI() {
  }
  /**
   传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate)
  {
      //将操作数据拷贝到本类中
      this.mOperate = cOperate;
      //得到外部传入的数据,将数据备份到本类中
      if (!getInputData(cInputData))
      {
          return false;
      }
      //进行业务处理
      if (!dealData())
      {
          return false;
      }
      //准备往后台的数据
      if (!prepareOutputData())
      {
          return false;
      }
      LAMettingAssessBL tLAMettingAssessBL = new LAMettingAssessBL();
      System.out.println("Start LAMettingAssessBL UI Submit...");
      tLAMettingAssessBL.submitData(mInputData, mOperate);
      System.out.println("End ALAPlan UI Submit...");
      //如果有需要处理的错误，则返回
      if (tLAMettingAssessBL.mErrors.needDealError())
      {
          // @@错误处理
          this.mErrors.copyAllErrors(tLAMettingAssessBL.mErrors);
          CError tError = new CError();
          tError.moduleName = "ALAPlanUI";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
          this.mErrors.addOneError(tError);
          return false;
      }
      if (mOperate.equals("QUERY||MAIN"))
      {
          this.mResult.clear();
          this.mResult = tLAMettingAssessBL.getResult();
      }
      mInputData = null;
      return true;
  }
  private boolean prepareOutputData()
  {
      try
      {
          mInputData.clear();
          mInputData.add(this.mGlobalInput);
          mInputData.add(this.mLAMettingSchema);
      }
      catch (Exception ex)
      {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "LAPlanUI";
          tError.functionName = "prepareData";
          tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
          this.mErrors.addOneError(tError);
          return false;
      }
      return true;
  }
  private boolean dealData()
     {
         boolean tReturn = false;
         //此处增加一些校验代码
         tReturn = true;
         return tReturn;
     }
     private boolean getInputData(VData cInputData)
        {
            //全局变量
            mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0));
            this.mLAMettingSchema.setSchema((LAMettingSchema) cInputData.
                                         getObjectByObjectName("LAMettingSchema", 0));
            if (mGlobalInput == null)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAPlanUI";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有得到足够的信息！";
                this.mErrors.addOneError(tError);
                return false;
            }
            return true;
        }
        public VData getResult()
           {
               return this.mResult;
           }


}
