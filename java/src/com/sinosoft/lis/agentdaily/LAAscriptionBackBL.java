package com.sinosoft.lis.agentdaily;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.Reflections;

public class LAAscriptionBackBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private int mLength=5000;//得到一次提交的长度

    /** 业务处理相关变量 */
    private LAAscriptionSchema mLAAscriptionSchema = new LAAscriptionSchema();
    private LAAscriptionSet    mLAAscriptionSet = new LAAscriptionSet();
    private LJAPayPersonSet     mLJAPayPersonSet=new LJAPayPersonSet();
    private LACommisionSet     mLACommisionSet=new LACommisionSet();
    private LCPolSet           mLCPolSet=new LCPolSet();
    private LCContSet          mLCContSet=new LCContSet();
    private LJSPayPersonSet    mLJSPayPersonSet=new LJSPayPersonSet();
    private LJSPayPersonBSet    mLJSPayPersonBSet=new LJSPayPersonBSet();
    private LCPENoticeSet      mLCPENoticeSet=new LCPENoticeSet();
    private LCContGetPolSet    mLCContGetPolSet = new LCContGetPolSet();
    private String mAgentNew = "";
    private String mFlag = "";
    
    private LAAscriptionSchema preLAAscriptionSchema= new LAAscriptionSchema();
    public LAAscriptionBackBL()
    {

    }

    public static void main(String[] args)
    {
        LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
        tLAAscriptionSchema.setAgentOld("");
        tLAAscriptionSchema.setNoti("");
        tLAAscriptionSchema.setBranchType("1");
        tLAAscriptionSchema.setBranchType2("01");
        tLAAscriptionSchema.setAgentNew("86110000");
        String tFlag="ALL";
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.ComCode = "86";
        tGlobalInput.Operator = "mak001";
  //  tVData.add(tLAAscriptionSchema);
        LAAscriptionSet tLAAscriptionSet = new LAAscriptionSet();
       tLAAscriptionSet.add(tLAAscriptionSchema);
       VData tVData = new VData();
       tVData.addElement(tLAAscriptionSet);
       tVData.add(tGlobalInput);
        tVData.add(tFlag);
        LAAscriptionBackBL bl =new LAAscriptionBackBL();
        bl.submitData(tVData,"UPDATE||MAIN");
    }

    /**
    * 传输数据的公共方法
    * @param: cInputData 输入的数据
    *         cOperate 数据操作
    * @return:
    */
   public boolean submitData(VData cInputData, String cOperate)
   {
       //将操作数据拷贝到本类中
       this.mOperate = cOperate;
       //得到外部传入的数据,将数据备份到本类中
       if (!getInputData(cInputData))
       {
           return false;
       }
//       if (!checkData())
//       {
//           return false;
//       }
       //进行业务处理
       if (!dealData())
       {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "LAAscriptionBackBL";
           tError.functionName = "submitData";
           tError.errorMessage = "数据处理失败LAAscriptionBackBL-->dealData!";
           this.mErrors.addOneError(tError);
           return false;
       }
       //准备往后台的数据
       if (!prepareOutputData())
       {
           return false;
       }
       if (this.mOperate.equals("QUERY||MAIN"))
       {
           this.submitquery();
       }
       else
       {
           System.out.println("Start ALAAscriptionEnsureBL Submit...");
           LAAscriptionBackBLS tLAAscriptionBackBLS= new LAAscriptionBackBLS();
           tLAAscriptionBackBLS.submitData(mInputData, cOperate);
           System.out.println("End ALAAscriptionEnsureBL Submit...");
           //如果有需要处理的错误，则返回
           if (tLAAscriptionBackBLS.mErrors.needDealError())
           {
               // @@错误处理
               this.mErrors.copyAllErrors(tLAAscriptionBackBLS.mErrors);
               CError tError = new CError();
               tError.moduleName = "LAAscriptionBackBL";
               tError.functionName = "submitDat";
               tError.errorMessage = "数据提交失败!";
               this.mErrors.addOneError(tError);
               return false;
           }
       }
       mInputData = null;
       return true;
   }

   private boolean dealData()
  {
      if(mFlag==null || mFlag.equals(""))
      {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "LAAscriptionBackBL";
              tError.functionName = "submitData";
              tError.errorMessage = "参数传递错误!";
              this.mErrors.addOneError(tError);
           return false;
      }
      boolean tReturn = true;
      int tCount = 0;
      String currentDate = PubFun.getCurrentDate();
      String currentTime = PubFun.getCurrentTime();
      if (mOperate.equals("UPDATE||MAIN"))
      {
    	  
          if(mFlag.equals("ALL"))
          {//全部回退
              LAAscriptionDB tLAAscriptionDB = new LAAscriptionDB();
              LAAscriptionSet tLAAscriptionSet = new LAAscriptionSet();
              LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
              tLAAscriptionSchema = mLAAscriptionSet.get(1);
              mLAAscriptionSet.clear();
              String tagentold = tLAAscriptionSchema.getAgentOld();
              String tmanagecom = tLAAscriptionSchema.getAgentNew(); //传值 时用AgentNew传 managecom
              String tname = tLAAscriptionSchema.getNoti(); //传值 时用Noti传 业务员姓名
              String sql =
                      "select * from ";
              sql +=
                      " laascription a where   a.ascripstate='3'";
              sql += "  and a.validflag='N' and a.branchtype='" +
                      tLAAscriptionSchema.getBranchType() +
                      "' and a.branchtype2='" +
                      tLAAscriptionSchema.getBranchType2() + "'";
              if (tagentold != null && !tagentold.equals("")) {
                  sql += " and a.agentold='" + tagentold + "'  ";
              }
              sql += " and exists (select c.agentcode from laagent c where c.ManageCom='" + tmanagecom +
                          "' and a.AgentOld=c.agentcode   ";
              if (tname != null && !tname.equals("")) {
                  sql += " and c.name='" + tname + "'  ";
              }
              else
              {
                   sql += " )";
              }
              System.out.println(sql);
              int tStart = 1;
              do {
                   tLAAscriptionSet = tLAAscriptionDB.executeQuery(sql, tStart,
                          mLength);
                     if (!dealCont(tLAAscriptionSet))
                     {
                          return false;
                     }
                   if(mLAAscriptionSet.size()>=mLength)
                   {//进行事物拆分,set 中大于mLength时事物就执行,完成以后使 set 为空,原来的数据不再执行
                       if (!prepareOutputData())
                       {
                           return false;
                       }
                       else
                       {
                           System.out.println("Start LAAscriptionBackBL Submit...");
                           LAAscriptionBackBLS tLAAscriptionBackBLS= new LAAscriptionBackBLS();
                           tLAAscriptionBackBLS.submitData(mInputData, mOperate);
                           System.out.println("End LAAscriptionBackBL Submit...");
                           //如果有需要处理的错误，则返回
                           if (tLAAscriptionBackBLS.mErrors.needDealError())
                           {
                               // @@错误处理
                               this.mErrors.copyAllErrors(tLAAscriptionBackBLS.mErrors);
                               CError tError = new CError();
                               tError.moduleName = "LAAscriptionBackBL";
                               tError.functionName = "submitDat";
                               tError.errorMessage = "数据提交失败!";
                               this.mErrors.addOneError(tError);
                               return false;
                           }
                        }
//                       mLAAscriptionSet = new LAAscriptionSet();
//                       mLACommisionSet=new LACommisionSet();
//                       mLCPolSet=new LCPolSet();
//                       mLCContSet=new LCContSet();
//                       mLJSPayPersonSet=new LJSPayPersonSet();
//                       mLCPENoticeSet=new LCPENoticeSet();
                        mLAAscriptionSet.clear();
                        mLACommisionSet.clear();
                        mLCPolSet.clear();
                        mLCContSet.clear();
                        mLJSPayPersonSet.clear();
                        mLCPENoticeSet.clear();
                  }
                  tStart = tStart + mLength;
              } while (tLAAscriptionSet.size() > 0);
          }
          else
          {//选择回退
              LAAscriptionSet tLAAscriptionSet = new LAAscriptionSet();
              tLAAscriptionSet.set(mLAAscriptionSet);
              mLAAscriptionSet.clear();
              if (!dealCont(tLAAscriptionSet)) {
                  return false;
              }
          }
      }
          return true;

  }

   private boolean getInputData(VData cInputData)
     {
         this.mLAAscriptionSet.set((LAAscriptionSet) cInputData.
                                            getObjectByObjectName(
                 "LAAscriptionSet", 0));
         this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                     getObjectByObjectName("GlobalInput", 0));
         this.mFlag=(String)cInputData.getObjectByObjectName("String",0);
         
         System.out.println("mFlag:"+mFlag);
         
         return true;
     }
    private boolean dealCont(LAAscriptionSet tLAAscriptionSet)
    {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        for (int i = 1; i <= tLAAscriptionSet.size(); i++) {

            LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
            tLAAscriptionSchema = tLAAscriptionSet.get(i);

            String tAscripNo = tLAAscriptionSchema.getAscripNo();
            String tContNo = tLAAscriptionSchema.getContNo();
            String tAgentNew = tLAAscriptionSchema.getAgentNew();
            String tAgentOld = tLAAscriptionSchema.getAgentOld();
            String sql1 = "select a.agentgroup,a.branchcode,b.branchattr,b.branchseries,a.managecom,a.Name from laagent a ,";
            sql1 +=
                    "labranchgroup b where a.agentgroup=b.agentgroup and a.agentcode='" +
                    tAgentOld + "'";
            SSRS tSSRS = new SSRS();
            ExeSQL tExeSQL2 = new ExeSQL();
            tSSRS = tExeSQL2.execSQL(sql1);
            int tCount2 = tSSRS.getMaxRow();
            if (tExeSQL2.mErrors.needDealError()) {
                this.mErrors.copyAllErrors(tExeSQL2.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAscriptionBackBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询新代理人信息出错！";
                this.mErrors.addOneError(tError);
                return false;
            }
            String tAgentGroup = tSSRS.GetText(1, 1);
            String tBranchCode = tSSRS.GetText(1, 2);
            String tBranchAttr = tSSRS.GetText(1, 3);
            String tBranchSeries = tSSRS.GetText(1, 4);
            String tManageCom = tSSRS.GetText(1, 5);
            String tName = tSSRS.GetText(1, 6);

            LAAscriptionDB tLAAscriptionDB = new LAAscriptionDB();
            tLAAscriptionDB.setAscripNo(tAscripNo);
            if (!tLAAscriptionDB.getInfo()) {
                this.mErrors.copyAllErrors(tLAAscriptionDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAscriptionBackBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查找归属表基本信息时失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLAAscriptionSchema = tLAAscriptionDB.getSchema();

            //tLAAscriptionSchema.setAscriptionDate(currentDate);
            tLAAscriptionSchema.setModifyDate(currentDate);
            tLAAscriptionSchema.setModifyTime(currentTime);
            tLAAscriptionSchema.setAscripState("1");
//            mLAAscriptionSet.add(tLAAscriptionSchema);

            //修改LCCont表中的AgentGroup,AgentCode,manageCom
            String tAppFlag = ""; //如果为1,则为已签单
            LCContSet tLCContSet = new LCContSet();
            LCContDB tLCContDB = new LCContDB();
            String tSQL="select * from lccont where prtno in(select prtno from lccont where contno='"+tContNo+"' ) ";
            tLCContSet = tLCContDB.executeQuery(tSQL);
            if (tLCContSet == null || tLCContSet.size() == 0)
            {
                  tLAAscriptionSchema.setValidFlag("Y");  //如果没找到保单,则归属成功,但是已经失效
            }
            mLAAscriptionSet.add(tLAAscriptionSchema);
            for (int k = 1; k <= tLCContSet.size(); k++)
            {
                LCContSchema tLCContSchema = new LCContSchema();
                tLCContSchema = tLCContSet.get(k);
                tLCContSchema.setAgentCode(tAgentOld);
                tLCContSchema.setAgentGroup(tAgentGroup);
                tLCContSchema.setManageCom(tManageCom);
                mLCContSet.add(tLCContSchema);
                tAppFlag = tLCContSchema.getAppFlag(); //是否签单
            }
            MMap mMap = new MMap();
            mMap.put(mLCContSet, "");
            //续保:没有签单,续期:已经签单,所以不能判断是否签单
            //修改LJSPayPerson表中的AgentGroup,AgentCode,manageCom
            LJSPayPersonSet tLJSPayPersonSet = new LJSPayPersonSet();
            LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
            tLJSPayPersonDB.setContNo(tContNo);
            tLJSPayPersonSet = tLJSPayPersonDB.query();
            String tGetNoticeNo = "";
            //可以没有应收
            for (int k = 1; k <= tLJSPayPersonSet.size(); k++) {
                LJSPayPersonSchema tLJSPayPersonSchema = new
                        LJSPayPersonSchema();
                tGetNoticeNo = tLJSPayPersonSchema.getGetNoticeNo(); //用于备份表的查询
                tLJSPayPersonSchema = tLJSPayPersonSet.get(k);

                tLJSPayPersonSchema.setAgentCode(tAgentOld);
                tLJSPayPersonSchema.setAgentGroup(tAgentGroup);
                tLJSPayPersonSchema.setManageCom(tManageCom);
                mLJSPayPersonSet.add(tLJSPayPersonSchema);
            }
            //按照归属时间来更改
            LACommisionSet tLACommisionSet = new LACommisionSet();
            LACommisionDB tLACommisionDB = new LACommisionDB();
            String wageno=tLAAscriptionSchema.getAscriptionDate().substring(0,4)+tLAAscriptionSchema.getAscriptionDate().substring(5,7);
            String mSQL="select * from lacommision where contno='"+tContNo+"'  and ( wageno='"+wageno+"' or wageno is null)  ";
            tLACommisionSet = tLACommisionDB.executeQuery(mSQL);
            for (int k = 1; k <= tLACommisionSet.size(); k++) {
                   LACommisionSchema tLACommisionSchema = new LACommisionSchema();
                   tLACommisionSchema = tLACommisionSet.get(k);
                   tLACommisionSchema.setAgentCode(tAgentOld);
                   tLACommisionSchema.setAgentGroup(tAgentGroup);
                   tLACommisionSchema.setManageCom(tManageCom);
                   tLACommisionSchema.setBranchAttr(tBranchAttr);
                   tLACommisionSchema.setBranchCode(tBranchCode);
                   tLACommisionSchema.setBranchSeries(tBranchSeries);
                   mLACommisionSet.add(tLACommisionSchema);
               }
            //按照归属时间来更改实收表
            //修改LJSPayPerson表中的AgentGroup,AgentCode,manageCom
            LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
            LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
            String makedate=tLAAscriptionSchema.getAscriptionDate().substring(0,4)+tLAAscriptionSchema.getAscriptionDate().substring(5,7)+"-01";
            String mSQL1="select * from ljapayperson where contno='"+tContNo+"'  and makedate>='"+makedate+"' ";
            tLJAPayPersonSet = tLJAPayPersonDB.executeQuery(mSQL1);
            //可以没有应收
            for (int k = 1; k <= tLJAPayPersonSet.size(); k++) {
            	LJAPayPersonSchema tLJAPayPersonSchema = new
                     LJAPayPersonSchema();

             tLJAPayPersonSchema = tLJAPayPersonSet.get(k);

             tLJAPayPersonSchema.setAgentCode(tAgentOld);
             tLJAPayPersonSchema.setAgentGroup(tAgentGroup);
             tLJAPayPersonSchema.setManageCom(tManageCom);
             mLJAPayPersonSet.add(tLJAPayPersonSchema);
            }
            //应收备份表
            if (tGetNoticeNo != null && !tGetNoticeNo.equals("")) {
                LJSPayPersonBSet tLJSPayPersonBSet = new LJSPayPersonBSet();
                LJSPayPersonBDB tLJSPayPersonBDB = new LJSPayPersonBDB();
                tLJSPayPersonBDB.setGetNoticeNo(tGetNoticeNo);
                tLJSPayPersonBSet = tLJSPayPersonBDB.query();
                for (int k = 1; k <= tLJSPayPersonBSet.size(); k++) {
                    LJSPayPersonBSchema tLJSPayPersonBSchema = new
                            LJSPayPersonBSchema();
                    tLJSPayPersonBSchema = tLJSPayPersonBSet.get(k);
                    tLJSPayPersonBSchema.setAgentCode(tAgentOld);
                    tLJSPayPersonBSchema.setAgentGroup(tAgentGroup);
                    tLJSPayPersonBSchema.setManageCom(tManageCom);
                    mLJSPayPersonBSet.add(tLJSPayPersonBSchema);
                }
            }
       //修改LCPENotice表的AgentGroup,AgentCode,manageCom
                LCPENoticeSet tLCPENoticeSet = new LCPENoticeSet();
                LCPENoticeDB tLCPENoticeDB = new LCPENoticeDB();
                tLCPENoticeDB.setContNo(tContNo);
                tLCPENoticeSet = tLCPENoticeDB.query();
               //可以没有体检数据             
                for (int k = 1; k <= tLCPENoticeSet.size(); k++) {
                    LCPENoticeSchema tLCPENoticeSchema = new LCPENoticeSchema();
                    tLCPENoticeSchema = tLCPENoticeSet.get(k);
                    tLCPENoticeSchema.setAgentCode(tAgentOld);
                    tLCPENoticeSchema.setManageCom(tManageCom);
                    tLCPENoticeSchema.setAgentName(tName);
                    mLCPENoticeSet.add(tLCPENoticeSchema);
                }
 //           }
            //修改LCPol表中的AgentGroup,AgentCode,manageCom
            LCPolSet tLCPolSet = new LCPolSet();
            LCPolDB tLCPolDB = new LCPolDB();
            String tSQL1="select * from lcpol where prtno in(select prtno from lccont where contno='"+tContNo+"' )";
            tLCPolSet = tLCPolDB.executeQuery(tSQL1);
            for (int k = 1; k <= tLCPolSet.size(); k++) {
                LCPolSchema tLCPolSchema = new LCPolSchema();
                tLCPolSchema = tLCPolSet.get(k);
                tLCPolSchema.setAgentCode(tAgentOld);
                tLCPolSchema.setAgentGroup(tAgentGroup);
                tLCPolSchema.setManageCom(tManageCom);
                mLCPolSet.add(tLCPolSchema);
            }
            //LCContgetpol
            LCContGetPolSet tLCContGetPolSet = new LCContGetPolSet();
            LCContGetPolDB tLCContGetPolDB = new LCContGetPolDB();
            tLCContGetPolDB.setContNo(tContNo);
            tLCContGetPolSet = tLCContGetPolDB.query();
            for (int k = 1; k <= tLCContGetPolSet.size(); k++) {
                LCContGetPolSchema tLCContGetPolSchema = new LCContGetPolSchema();
                tLCContGetPolSchema = tLCContGetPolSet.get(k);
                tLCContGetPolSchema.setAgentCode(tAgentOld);
                //tLCContGetPolSchema.setAgentGroup(tAgentGroup);
                tLCContGetPolSchema.setManageCom(tManageCom);
                mLCContGetPolSet.add(tLCContGetPolSchema);
            }


        }
        
          return true;
     }

     /**
      * 准备往后层输出所需要的数据
      * 输出：如果准备数据时发生错误则返回false,否则返回true
      */
     private boolean submitquery()
    {
        return true;
    }
    private boolean prepareOutputData()
   {
       try
       {
           this.mInputData = new VData();
           this.mInputData.add(this.mGlobalInput);
           this.mInputData.add(this.mLAAscriptionSet);
           this.mInputData.add(this.mLCPolSet);
           this.mInputData.add(this.mLCContSet);
           this.mInputData.add(this.mLJSPayPersonSet);
           this.mInputData.add(this.mLJSPayPersonBSet);
           this.mInputData.add(this.mLCPENoticeSet);
           this.mInputData.add(this.mLACommisionSet);
           this.mInputData.add(this.mLJAPayPersonSet);
           //lccontgetpol不修改
           //this.mInputData.add(this.mLCContGetPolSet);
        //   System.out.println("Set有多大？？？？" + mLAAscriptionSet.size());
       }
       catch (Exception ex)
       {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "HDLAAscriptionBL";
           tError.functionName = "prepareData";
           tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
           this.mErrors.addOneError(tError);
           return false;
       }
       return true;
   }
   public VData getResult()
   {
       return this.mResult;
   }

}



