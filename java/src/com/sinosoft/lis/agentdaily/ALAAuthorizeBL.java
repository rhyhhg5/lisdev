/*
 * <p>ClassName: ALAAuthorizeBL </p>
 * <p>Description: ALAAuthorizeBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-15
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAAuthorizeDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAuthorizeSchema;
import com.sinosoft.lis.vschema.LAAuthorizeSet;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ALAAuthorizeBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAAuthorizeSchema mLAAuthorizeSchema = new LAAuthorizeSchema();
    private LAAuthorizeSet mLAAuthorizeSet = new LAAuthorizeSet();
    public ALAAuthorizeBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!checkData())
        {
            return false;
        }
        System.out.println("checkdata out");
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAuthorizeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALAAuthorizeBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start ALAAuthorizeBL Submit...");
            ALAAuthorizeBLS tALAAuthorizeBLS = new ALAAuthorizeBLS();
            tALAAuthorizeBLS.submitData(mInputData, cOperate);
            System.out.println("End ALAAuthorizeBL Submit...");
            //如果有需要处理的错误，则返回
            if (tALAAuthorizeBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tALAAuthorizeBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAAuthorizeBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            //this.mLAAuthorizeSet.set((LAAuthorizeSet)mInputData.getObjectByObjectName("LAAuthorizeSet",0));
            LAAuthorizeSet tLAAuthorizeSet = new LAAuthorizeSet();
            tLAAuthorizeSet.set(mLAAuthorizeSet);
            mLAAuthorizeSet.clear();
            for (int i = 1; i <= tLAAuthorizeSet.size(); i++)
            {
                LMRiskAppSet tLMRiskAppSet = new LMRiskAppSet();
                LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
                tLMRiskAppDB.setRiskCode(tLAAuthorizeSet.get(i).getRiskCode());
                tLMRiskAppSet = tLMRiskAppDB.query();
                LAAuthorizeSchema tLAAuthorizeSchema = null;
                if (tLMRiskAppSet.size() > 0)
                {

                        tLAAuthorizeSchema = new LAAuthorizeSchema();
                        tLAAuthorizeSchema.setAuthorObj(tLAAuthorizeSet.get(i).getAuthorObj());
                        tLAAuthorizeSchema.setAuthorType(tLAAuthorizeSet.get(i).getAuthorType());
                        tLAAuthorizeSchema.setRiskCode(tLAAuthorizeSet.get(i).
                                getRiskCode());
                        tLAAuthorizeSchema.setBranchType(tLAAuthorizeSet.get(i).
                                getBranchType());
                        tLAAuthorizeSchema.setBranchType2(tLAAuthorizeSet.get(i).
                                getBranchType2());
                        tLAAuthorizeSchema.setRiskType(tLMRiskAppSet.get(1).
                                getRiskType());
                        tLAAuthorizeSchema.setAuthorStartDate(tLAAuthorizeSet.
                                get(i).getAuthorStartDate());
                        tLAAuthorizeSchema.setAuthorEndDate(tLAAuthorizeSet.get(
                                i).getAuthorEndDate());
                        if(tLAAuthorizeSet.get(i).getMakeDate()==null || tLAAuthorizeSet.get(i).getMakeDate().equals(""))
                        {
                             tLAAuthorizeSchema.setMakeDate(currentDate);
                        }
                        else
                        {
                             tLAAuthorizeSchema.setMakeDate(tLAAuthorizeSet.get(i).getMakeDate());
                        }
                        if(tLAAuthorizeSet.get(i).getMakeTime()==null || tLAAuthorizeSet.get(i).getMakeTime().equals(""))
                        {
                             tLAAuthorizeSchema.setMakeTime(currentTime);
                        }
                        else
                        {
                             tLAAuthorizeSchema.setMakeTime(tLAAuthorizeSet.get(i).getMakeTime());
                        }
                        tLAAuthorizeSchema.setModifyDate(currentDate);
                        tLAAuthorizeSchema.setModifyTime(currentTime);
                        tLAAuthorizeSchema.setOperator(this.mGlobalInput.
                                Operator);
                        mLAAuthorizeSet.add(tLAAuthorizeSchema);

                }
            }
        }
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAAuthorizeSchema.setSchema((LAAuthorizeSchema) cInputData.
                                          getObjectByObjectName(
                                                  "LAAuthorizeSchema", 0));
        this.mLAAuthorizeSet.set((LAAuthorizeSet) cInputData.
                                 getObjectByObjectName("LAAuthorizeSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start ALAAuthorizeBLQuery Submit...");
        LAAuthorizeDB tLAAuthorizeDB = new LAAuthorizeDB();
        tLAAuthorizeDB.setSchema(this.mLAAuthorizeSchema);
        this.mLAAuthorizeSet = tLAAuthorizeDB.query();
        this.mResult.add(this.mLAAuthorizeSet);
        System.out.println("End ALAAuthorizeBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLAAuthorizeDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAAuthorizeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAAuthorizeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLAAuthorizeSet);
            this.mInputData.add(this.mLAAuthorizeSchema);
            System.out.println("prepareOutputData");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAuthorizeBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private boolean checkData()
    {
        if (!this.mOperate.equals("QUERY||MAIN"))
        {
            return true;
        }
        int tCount;
        String tAgentCode;
        tAgentCode = mLAAuthorizeSchema.getAgentCode();
        if (tAgentCode == null)
        {
            return true;
        }
        else
        {
            LAAgentDB tLAAgentDB = new LAAgentDB();
            tLAAgentDB.setAgentCode(tAgentCode);
            tCount = tLAAgentDB.getCount();
            System.out.println("tCount:" + Integer.toString(tCount));
            if (tCount == -1)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAAuthorizeBL";
                tError.functionName = "checkData()";
                tError.errorMessage = "操作失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            else if (tCount == 0)
            {
                CError tError = new CError();
                tError.moduleName = "ALAAuthorizeBL";
                tError.functionName = "checkData()";
                tError.errorMessage = "没有这个代理人!";
                this.mErrors.addOneError(tError);
                return false;
            }
            return true;
        }
    }
}
