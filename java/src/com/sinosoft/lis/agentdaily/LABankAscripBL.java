package com.sinosoft.lis.agentdaily;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LAAscriptionSet;
import com.sinosoft.lis.db.LAAscriptionDB;
import com.sinosoft.utility.SSRS;
import java.math.BigInteger;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.vdb.LAAgentDBSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.PubFun1;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LABankAscripBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData=new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mBranchType = "";
    private String mBranchType2= "";
    private int mLength=10000;//得到一次提交的长度
    private String updateSQL = "";
    private LAAscriptionSchema cLAAscriptionSchema = new LAAscriptionSchema();
    private LAAscriptionSet cLAAscriptionSet = new LAAscriptionSet();
    private LAAscriptionSet mLAAscriptionSet = new LAAscriptionSet();
    private LAAscriptionSet preLAAscriptionSet = new LAAscriptionSet();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private MMap mMap = new MMap();
    private String mConttype="";////保单类型：1--个单，2--团单

    public LABankAscripBL()
    {
    }

    public static void main(String[] args)
    {
        LABankAscripBL BL = new LABankAscripBL();
    }
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        System.out.println("Operate:"+cOperate);
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABankAscripBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LABankAscripBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start LABankAscripBL Submit...");
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LABankAscripBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("End LABankAscripBL Submit...");

        mInputData = null;
        return true;
    }
    private boolean dealData()
    {
        System.out.println("come here ");
       if (mOperate.equals("INSERT||MAIN"))
        {
        	for (int i = 1; i <= cLAAscriptionSet.size(); i++) {

        		LAAscriptionSchema cLAAscriptionSchema = new LAAscriptionSchema();
        		cLAAscriptionSchema = cLAAscriptionSet.get(i);

        		boolean tReturn = true;
            int tCount = 0;
//          防止保单归属确认后重复分配保单 modify by zhuxt 20140915
//            String aSQL = "select distinct agentold from laascription where agentold = '"+cLAAscriptionSchema.getAgentOld()
//                          +"' and contno = '" + cLAAscriptionSchema.getContNo()+"' and ascripstate = '3'";
//            ExeSQL aaExeSQL = new ExeSQL();
//            String aAgentold = aaExeSQL.getOneValue(aSQL);
//            if (aaExeSQL.mErrors.needDealError())
//            {
//               CError tError = new CError();
//               tError.moduleName = "LABankAscripBL";
//               tError.functionName = "dealData";
//               tError.errorMessage = "查询原业务员相应的信息出错";
//               this.mErrors.addOneError(tError);
//               return false;
//            }
//            if(!aAgentold.equals("") && null != aAgentold)
//            {
//            	CError tError = new CError();
//                tError.moduleName = "LABankAscripBL";
//                tError.functionName = "dealData";
//                tError.errorMessage = "业务员" + aAgentold + "的保单"+cLAAscriptionSchema.getContNo()+"已经做过归属确认，不能再进行保单分配！";
//                this.mErrors.addOneError(tError);
//                return false;
//            }
           String tSQL = "select a.agentgroup,a.branchattr,b.managecom from labranchgroup a,laagent b "
           +" where a.agentgroup=b.agentgroup and b.agentcode='"
           + cLAAscriptionSchema.getAgentOld() + "'";
            SSRS tSSRS = new SSRS();
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(tSQL);
            if (tExeSQL.mErrors.needDealError())
            {
               CError tError = new CError();
               tError.moduleName = "HDLAAscriptionBL";
               tError.functionName = "dealData";
               tError.errorMessage = "查询原业务员相应的信息出错";
               this.mErrors.addOneError(tError);
               return false;
            }

            int a = tSSRS.getMaxRow();
            if (a != 1)
            {
               CError tError = new CError();
               tError.moduleName = "HDLAAscriptionBL";
               tError.functionName = "dealData";
               tError.errorMessage = "没有查到原业务员相应的信息";
               this.mErrors.addOneError(tError);
               return false;
             }
            String mAgentGroup = tSSRS.GetText(1, 1);
            String mBranchAttr = tSSRS.GetText(1, 2);
            String mManageCom=tSSRS.GetText(1, 3);
            String strsql="";
            if(this.mConttype.equals("1"))
            {
            	strsql = "select count(ascripno)+1 from laascription where 1=1 and "
                    + " contno='"+ cLAAscriptionSchema.getContNo() + "'";
            }
            else if(this.mConttype.equals("2"))
            {
            	strsql = "select count(ascripno)+1 from laascription where 1=1 and "
                    + " grpcontno='"+ cLAAscriptionSchema.getGrpContNo() + "'";
            }
            ExeSQL tExeSQL1 = new ExeSQL();
            String mCount = tExeSQL1.getOneValue(strsql);
            if (tExeSQL1.mErrors.needDealError())
            {
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询归属表信息出错";
                this.mErrors.addOneError(tError);
                return false;
            }
            int mCountNO = Integer.parseInt(mCount);//得到归属次数

            String tAgentOld = cLAAscriptionSchema.getAgentOld();
            String tContNo = cLAAscriptionSchema.getContNo();
            String tGrpcontno=cLAAscriptionSchema.getGrpContNo();
            System.out.println("………………开始进行插入操作…………………"+ tContNo+"/"+tGrpcontno);
            //判断是否为已撤销的保单
            String ttsql = "select 'X' from lccont where contno='"+ tContNo + "' and Agentcode='"
                           + tAgentOld+ "' and uwflag='a'";
            ttsql+="union select 'X' from lcgrpcont where grpcontno='"+tGrpcontno+"' and agentcode='"+tAgentOld+"' and uwflag='a'";
            ExeSQL ttExeSQL = new ExeSQL();
            String ttFlag = ttExeSQL.getOneValue(ttsql);
            if (ttExeSQL.mErrors.needDealError())
            {
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询保单表信息出错";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (ttFlag != null && !ttFlag.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBL";
                tError.functionName = "dealData";
                tError.errorMessage = " 已做撤销的保单不做归属！";
                this.mErrors.addOneError(tError);
                return false;
            }
            //将之前进行过的个单分配记录变成无效
            if(this.mConttype.equals("1"))
            {
             String Ascrip = "select agentnew from LAAscription "
                          +" where agentold='"+ tAgentOld
                          +"' and ascripstate='2' and contno='"
                          + tContNo + "' and maketype='01' and validflag='N'";
             ExeSQL aExeSQL = new ExeSQL();
             String flag = aExeSQL.getOneValue(Ascrip);
             if(flag!=null&&!flag.equals(""))
             {
            	 updateSQL = "update LAAscription set ValidFlag='Y',modifydate='"+ currentDate
                         + "',modifytime='"+ currentTime
                         + "',operator='"+ this.mGlobalInput.Operator
                         + "' where agentold='"+ cLAAscriptionSchema.getAgentOld()+ "'  and ValidFlag='N'"
                         + " and MakeType='01' and ascripstate<>'3' and contno='"+ tContNo + "'";
               mMap.put(updateSQL, "UPDATE");
              }
            }else if(this.mConttype.equals("2"))
            {//将之前进行过的团单分配记录变成无效
            	String Ascrip = "select agentnew from LAAscription "
                    +" where agentold='"+ tAgentOld
                    +"' and ascripstate='2' and grpcontno='"
                    + tGrpcontno + "' and maketype='01' and validflag='N'";
            	ExeSQL aExeSQL = new ExeSQL();
            	String flag = aExeSQL.getOneValue(Ascrip);
            	if(flag!=null&&!flag.equals(""))
            	{
            		updateSQL = "update LAAscription set ValidFlag='Y',modifydate='"+ currentDate
                   + "',modifytime='"+ currentTime
                   + "',operator='"+ this.mGlobalInput.Operator
                   + "' where agentold='"+ cLAAscriptionSchema.getAgentOld()+ "'  and ValidFlag='N'"
                   + " and MakeType='01' and ascripstate<>'3' and grpcontno='"+ tGrpcontno + "'";
            		mMap.put(updateSQL, "UPDATE");
            	}
            }
          
//插入炒作
            String mAscripNo = com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo("PerAscripNo", 20);
            cLAAscriptionSchema.setAscripNo(mAscripNo);
            cLAAscriptionSchema.setValidFlag("N");
            cLAAscriptionSchema.setAClass("01");
            cLAAscriptionSchema.setAgentGroup(mAgentGroup);
            cLAAscriptionSchema.setBranchAttr(mBranchAttr);
            cLAAscriptionSchema.setManageCom(mManageCom);
            cLAAscriptionSchema.setMakeType("01");
            cLAAscriptionSchema.setAscriptionDate(currentDate);
            cLAAscriptionSchema.setOperator(mGlobalInput.Operator);
            cLAAscriptionSchema.setMakeDate(currentDate);
            cLAAscriptionSchema.setMakeTime(currentTime);
            cLAAscriptionSchema.setModifyDate(currentDate);
            cLAAscriptionSchema.setModifyTime(currentTime);
            cLAAscriptionSchema.setAscriptionCount(mCountNO);
            cLAAscriptionSchema.setAscripState("2");
            mLAAscriptionSet.add(cLAAscriptionSchema);
       }
        mMap.put(mLAAscriptionSet, "INSERT");
        }

//        if (mOperate.equals("UPDATE||MAIN"))
//        {
//            String tAgentOld = mLAAscriptionSchema.getAgentOld();
//            String tContNo = mLAAscriptionSchema.getContNo();
//            String tsql = "select AscripNo from LAAscription where agentnew='"
//                    + AgentNew1 + "' and AgentOld='" + tAgentOld
//                    + "' and AscripState='3' ";
//            ExeSQL tstrExeSQL = new ExeSQL();
//            String strAscripNo = tstrExeSQL.getOneValue(tsql);
//            if (strAscripNo != null && !strAscripNo.equals(""))
//            {
//                CError tError = new CError();
//                tError.moduleName = "HDLAAscriptionBL";
//                tError.functionName = "dealData";
//                tError.errorMessage = "此代理人的保单已归属处理成功,不能再修改此条数据";
//                this.mErrors.addOneError(tError);
//                return false;
//            }
//
//            //如果修改数据,则要记录轨迹,把原来的数据修改为无效的历史记录,并插入一条新的数据(重新生成AscripNo)
//            else
//            {
//                //判断是否为单张保单归属
//                if (tContNo == null || tContNo.equals(""))
//                {
//                    updateSQL = "update LAAscription set ValidFlag='Y',modifydate='"+ currentDate
//                            + "',modifytime='"+ currentTime+ "',"+ "operator='"+ this.mGlobalInput.Operator
//                            + "' where agentold='"+ mLAAscriptionSchema.getAgentOld()+ "' and agentnew='"
//                            + AgentNew1+ "' and ValidFlag='N'"+ " and AscripState='2' and MakeType='01'";
//                    mMap.put(updateSQL, "UPDATE");
//                    String allSQL = "select distinct(contno) from LAAscription where agentcode='"
//                            + tAgentOld+ "' and agentnew='"+ AgentNew1+ "' and ValidFlag='N'"
//                            + " and AscripState='2' and MakeType='01'";
//                    ExeSQL allExeSQL = new ExeSQL();
//                    SSRS allSSRS = new SSRS();
//                    allSSRS = allExeSQL.execSQL(allSQL);
//                    System.out.println("大小：" + allSSRS.getMaxRow());
//                    if (allSSRS.getMaxRow() > 0)
//                    {
//                        for (int i = 1; i <= allSSRS.getMaxRow(); i++)
//                        {
//                            //进行数据插入LAAscription的准备
//                            String mAscripNo = com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo("PerAscripNo", 20);
//                            System.out.println("mAscripNo:" + mAscripNo);
//                            System.out.println("Contno:"+ allSSRS.GetText(i, 1));
//                            LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
//                            tLAAscriptionSchema.setSchema(mLAAscriptionSchema);
//                            tLAAscriptionSchema.setAscripNo(mAscripNo);
//                            tLAAscriptionSchema.setValidFlag("N");
//                            tLAAscriptionSchema.setAClass("01");
//                            tLAAscriptionSchema.setContNo(allSSRS.GetText(i, 1));
//                            tLAAscriptionSchema.setAgentGroup(mAgentGroup);
//                            tLAAscriptionSchema.setBranchAttr(mBranchAttr);
//                            tLAAscriptionSchema.setAscriptionDate(mLAAscriptionSchema.getAscriptionDate());
//                            tLAAscriptionSchema.setOperator(mGlobalInput.Operator);
//                            tLAAscriptionSchema.setMakeDate(currentDate);
//                            tLAAscriptionSchema.setMakeTime(currentTime);
//                            tLAAscriptionSchema.setModifyDate(currentDate);
//                            tLAAscriptionSchema.setModifyTime(currentTime);
//                            tLAAscriptionSchema.setAscriptionCount(mCountNO);
//                            tLAAscriptionSchema.setAscripState("2");
//                            mLAAscriptionSet.add(tLAAscriptionSchema);
//                        }
//                    }
//                }
//                else
//                {
//
//                    //判断是否为已撤销的保单
//                    String ttsql = "select 'X' from lccont where contno='"+ tContNo + "' and Agentcode='" + tAgentOld+ "' and uwflag='a'";
//                    ExeSQL ttExeSQL = new ExeSQL();
//                    String ttFlag = ttExeSQL.getOneValue(ttsql);
//                    if (ttExeSQL.mErrors.needDealError())
//                    {
//                        CError tError = new CError();
//                        tError.moduleName = "HDLAAscriptionBL";
//                        tError.functionName = "dealData";
//                        tError.errorMessage = "查询保单表信息出错";
//                        this.mErrors.addOneError(tError);
//                        return false;
//                    }
//                    if (ttFlag != null && !ttFlag.equals(""))
//                    {
//                        CError tError = new CError();
//                        tError.moduleName = "HDLAAscriptionBL";
//                        tError.functionName = "dealData";
//                        tError.errorMessage = " 已做撤销的保单不做归属！";
//                        this.mErrors.addOneError(tError);
//                        return false;
//                    }
//                    updateSQL = "update LAAscription set ValidFlag='Y',modifydate='"+ currentDate
//                        + "',modifytime='"+ currentTime+ "',operator='"+ this.mGlobalInput.Operator
//                        + "' where agentold='"+ mLAAscriptionSchema.getAgentOld()+ "' and agentnew='"
//                        + AgentNew1 + "' and ValidFlag='N'"+ " and AscripState='2' and MakeType='01' and contno='"+ tContNo1 + "'";
//                   mMap.put(updateSQL, "UPDATE");
//                    //判断是否为孤儿单
//                    LAOrphanPolicyDB tmLAOrphanPolicyDB = new LAOrphanPolicyDB();
//                    LAOrphanPolicySchema tmLAOrphanPolicySchema = new LAOrphanPolicySchema();
//                    tmLAOrphanPolicyDB.setAgentCode(tAgentOld);
//                    tmLAOrphanPolicyDB.setContNo(tContNo);
//                    if (!tmLAOrphanPolicyDB.getInfo())
//                    {
//                    }
//                    else
//                    {
//                        tmLAOrphanPolicySchema = tmLAOrphanPolicyDB.getSchema();
//                        //备份
//                        LAOrphanPolicyBSchema tmLAOrphanPolicyBSchema = new LAOrphanPolicyBSchema();
//                        Reflections tReflections = new Reflections();
//                        tReflections.transFields(tmLAOrphanPolicyBSchema,
//                                tmLAOrphanPolicySchema);
//                        tmLAOrphanPolicyBSchema.setEdorType("01");
//                        tmLAOrphanPolicyBSchema.setEdorNo(mEdorNo);
//                        tmLAOrphanPolicyBSchema.setMakeDate(currentDate);
//                        tmLAOrphanPolicyBSchema.setMakeTime(currentTime);
//                        tmLAOrphanPolicyBSchema.setModifyDate(currentDate);
//                        tmLAOrphanPolicyBSchema.setModifyTime(currentTime);
//                        tmLAOrphanPolicyBSchema
//                                .setOperator(this.mGlobalInput.Operator);
//                        mMap.put(tmLAOrphanPolicyBSchema, "INSERT");
//                        mMap.put(tmLAOrphanPolicySchema, "DELETE");
//                    }
//
//                    String mAscripNo = com.sinosoft.lis.pubfun.PubFun1
//                            .CreateMaxNo("PerAscripNo", 20);
//                    mLAAscriptionSchema.setAscripNo(mAscripNo);
//                    mLAAscriptionSchema.setValidFlag("N");
//                    mLAAscriptionSchema.setAClass("01");
//                    mLAAscriptionSchema.setAgentGroup(mAgentGroup);
//                    mLAAscriptionSchema.setBranchAttr(mBranchAttr);
//                    mLAAscriptionSchema.setAscriptionDate(mLAAscriptionSchema.getAscriptionDate());
//                    mLAAscriptionSchema.setOperator(mGlobalInput.Operator);
//                    mLAAscriptionSchema.setMakeDate(currentDate);
//                    mLAAscriptionSchema.setMakeTime(currentTime);
//                    mLAAscriptionSchema.setModifyDate(currentDate);
//                    mLAAscriptionSchema.setModifyTime(currentTime);
//                    mLAAscriptionSchema.setAscriptionCount(mCountNO);
//                    mLAAscriptionSchema.setAscripState("2");
//                    mMap.put(mLAAscriptionSchema, "INSERT");
//                }
//
//            }
//        }

//        if (mOperate.equals("DELETE||MAIN"))
//        {
//            //删除数据只需要将ValidFlag字段置为无效即可
//
//            if (this.mLAAscriptionSchema.getContNo() == null|| this.mLAAscriptionSchema.getContNo().equals(""))
//            {
//                updateSQL = "update LAAscription set ValidFlag='Y',modifydate='"+ currentDate
//                        + "',modifytime='"+ currentTime+ "',operator='"+ this.mGlobalInput.Operator
//                        + "' where agentold='"+ mLAAscriptionSchema.getAgentOld()
//                        + "' and agentnew='"+ this.mLAAscriptionSchema.getAgentNew()
//                        + "' and ValidFlag='N'"+ " and AscripState='2' and MakeType='01'";
//            }
//            else
//            {
//                updateSQL = "UPDATE LAASCRIPTION set ValidFlag='Y',modifydate='"
//                        + currentDate+ "',modifytime='"+ currentTime+ "',operator='"
//                        + this.mGlobalInput.Operator+ "' where agentold='"
//                        + mLAAscriptionSchema.getAgentOld()+ "' and agentnew='"
//                        + this.mLAAscriptionSchema.getAgentNew()+ "' and ValidFlag='N'"
//                        + " and AscripState='2' and MakeType='01' and contno='"
//                        + this.mLAAscriptionSchema.getContNo() + "'";
//            }
//            mMap.put(updateSQL, "UPDATE");
//        }
        return true;

    }


    private boolean getInputData(VData cInputData)
    {
        this.cLAAscriptionSet.set( (LAAscriptionSet) cInputData.getObjectByObjectName("LAAscriptionSet",0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mConttype=(String) cInputData.getObject(2);
        System.out.println("保单类型:" + this.mConttype );
        return true;
      }
      private boolean prepareOutputData()
      {
          this.mInputData.add(mMap);
          return true;
    }
}
