package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.DBConnPool;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Connection;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.utility.ExeSQL;

public class LADataGatherUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAWageLogSchema mLAWageLogSchema = new LAWageLogSchema();

    private LACommisionSet mLACommisionSet = new LACommisionSet();
    private LACommisionBSet mLACommisionBSet = new LACommisionBSet();
    private LAWageLogSet mLAWageLogSet = new LAWageLogSet();
    private LAWageHistorySet mLAWageHistorySet = new LAWageHistorySet();
    private LARollBackTraceSet mLARollBackTraceSet = new LARollBackTraceSet();

    private VData mInputData = new VData();
    private String mType = "";

    private String mDayNum = "";
    private String mEndDate = "";
    private String mEndDate2 = "";
    private String mNewEdorNo = "";

    private String strDate = PubFun.getCurrentDate2();

    public static void main(String args[]) {
        LAWageLogSchema tLAWageLogSchema = new LAWageLogSchema();
         tLAWageLogSchema.setManageCom("86110000");
        tLAWageLogSchema.setWageNo("5");
        tLAWageLogSchema.setBranchType("1");
        tLAWageLogSchema.setBranchType2("01");

        GlobalInput tG = new GlobalInput();
        tG.ComCode = "86110000";
        tG.Operator = "001";
        tG.ManageCom = "86110000";

        VData tVData = new VData();
        tVData.addElement(tLAWageLogSchema);
        tVData.add(tG);

        LADataGatherUI tLADataGatherUI = new LADataGatherUI();
        boolean tB = tLADataGatherUI.submitData(tVData, "");
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //得到外部传入的数据,将数据备份到本类中
        if (this.getInputData(cInputData) == false) {
            return false;
        }

        //进行业务处理
        System.out.println("dealdata");
        if (!dealData()) {
            return false;
        }

        LADataGatherBL tLADataGatherBL = new LADataGatherBL();
        if (!tLADataGatherBL.submitData(mInputData, mType)) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LADataGatherUI";
            tError.functionName = "submitData";
            tError.errorMessage = "向后台提交数据时 处理失败！";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 业务处理
     * @return boolean
     */
    private boolean dealData() {
        //得到最大计算止期、和回退后的日期
        getMaxEndDate();

        //判断最大计算止期和回退日期获取是否成功
        if (mEndDate.length() != 10 || mEndDate2.length() != 10) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LADataGatherUI";
            tError.functionName = "dealData";
            tError.errorMessage = "最大计算止期、回退日期获取失败！";
            this.mErrors.addOneError(tError);

            return false;
        }
        String tSql  =" select yearmonth from lastatsegment where startdate<='"+mEndDate
                      +"' and endDate>='"+mEndDate+"' and stattype='5'" ;
        ExeSQL tExeSQL = new ExeSQL();
        String tEndNo = tExeSQL.getOneValue(tSql) ;
        tSql  =" select yearmonth from lastatsegment where startdate<='"+mEndDate2
                      +"' and endDate>='"+mEndDate2+"' and stattype='5'" ;
        tExeSQL = new ExeSQL();
        String tStartNo = tExeSQL.getOneValue(tSql) ;
//        if (!tEndNo.equals(tStartNo)) {
//                // @@错误处理
//                CError tError = new CError();
//                tError.moduleName = "LADataGatherUI";
//                tError.functionName = "dealData";
//                tError.errorMessage = "最大计算止期、回退日期必需为当前月！";
//                this.mErrors.addOneError(tError);
//
//                return false;
//
//        }

        //进行业务验证、如果佣金已经算过则不允许回退
        if (checkWageLog(tStartNo,tEndNo) == false) {
            return false;
        }

        //准备数据
        //直接佣金明细表（佣金扎账表）/直接佣金明细备份表（佣金扎账表）
        if (!dealLACommision(tStartNo,tEndNo)) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LADataGatherUI";
            tError.functionName = "dealData";
            tError.errorMessage = "明晰数据准备失败！";
            this.mErrors.addOneError(tError);

            return false;
        }

        //如果回退操作跨月，做一下操作
        //佣金提数日志表/佣金计算历史
        String tType = dealLAWage();

        //如果没有找到需要处理的提数日志或佣金计算历史记录时 作如下操作
        if ("0".equals(tType)) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LADataGatherUI";
            tError.functionName = "dealData";
            tError.errorMessage = "没有需要处理的佣金提数日志/佣金计算历史的数据！";
            this.mErrors.addOneError(tError);

            return false;
        }

        //处理回退记录信息
        dealRollBackTrace();

        //缓存处理状态
        mType = tType;

        //准备后台处理的数据
        if (!prepareOutputData(tType)) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LADataGatherUI";
            tError.functionName = "dealData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错！";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 进行回退记录处理
     */
    private void dealRollBackTrace() {
        //条件划分符号
        String tSign = "&";
        //回退序号
        String tIdx = PubFun1.CreateMaxNo("IDX", 20);
        //回退条件
        String tTrem = "";
        tTrem += tSign + "管理机构:" + mLAWageLogSchema.getManageCom();
        tTrem += tSign + "展业机构:" + mLAWageLogSchema.getBranchType();
        tTrem += tSign + "展业渠道:" + mLAWageLogSchema.getBranchType2();
        tTrem += tSign + "回退天数:" + mDayNum;

        LARollBackTraceSchema tLARollBackTraceSchema = new
                LARollBackTraceSchema();
        tLARollBackTraceSchema.setIdx(tIdx);
        tLARollBackTraceSchema.setEdorNo(mNewEdorNo);
        tLARollBackTraceSchema.setoperator(mGlobalInput.Operator);
        tLARollBackTraceSchema.setstate("0");
        tLARollBackTraceSchema.setConditions(tTrem);
        tLARollBackTraceSchema.setMakeDate(PubFun.getCurrentDate());
        tLARollBackTraceSchema.setMakeTime(PubFun.getCurrentTime());
        tLARollBackTraceSchema.setRollBackType("09");

        //缓存回退记录
        mLARollBackTraceSet.add(tLARollBackTraceSchema);
    }

    /**
     * 准备后台处理的数据
     * @param pmType String
     * @return boolean
     */
    private boolean prepareOutputData(String pmType) {
        try {
            mInputData.clear();
            mInputData.add(this.mGlobalInput);
            mInputData.add(this.mLACommisionSet);
            mInputData.add(this.mLACommisionBSet);
            mInputData.add(this.mLARollBackTraceSet);

            if (pmType.equals("1")) {
                //多余了 删掉
                //mInputData.add(this.mLAWageLogSet);
            } else if (pmType.equals("2")) {
                mInputData.add(this.mLAWageLogSet);
                mInputData.add(this.mLAWageHistorySet);
            }

            //修改佣金提数日志表中的计算止期
            LAWageLogDB tLAWageLogDB = new LAWageLogDB();
            //设置查询条件
            String tWageNo = mEndDate2.substring(0, 4) +
                             mEndDate2.subSequence(5, 7);
            tLAWageLogDB.setWageNo(tWageNo);
            tLAWageLogDB.setManageCom(mLAWageLogSchema.getManageCom());
            tLAWageLogDB.setBranchType(mLAWageLogSchema.getBranchType());
            tLAWageLogDB.setBranchType2(mLAWageLogSchema.getBranchType2());
            LAWageLogSet tLAWageLogSet = tLAWageLogDB.query();

            if (tLAWageLogSet.size() > 0) {
                LAWageLogSchema tLAWageLogSchema = tLAWageLogSet.get(1).
                        getSchema();
                //设置要修改的项
                String mAscriptDate = PubFun.calDate(mEndDate2, -1, "D", null);
                tLAWageLogSchema.setEndDate(mAscriptDate);
                tLAWageLogSchema.setModifyDate(PubFun.getCurrentDate());
                tLAWageLogSchema.setModifyTime(PubFun.getCurrentTime());
                //准备要修改的数据
                mInputData.add(tLAWageLogSchema);
            }
        } catch (Exception ex) {
            return false;
        }

        return true;
    }

    /**
     * 处理佣金提数计算信息
     * @return String
     */
    private String dealLAWage() {
        //判断回退操作跨月
        String tStartMonth = mEndDate.substring(5, 7);
        String tEndMonth = mEndDate2.substring(5, 7);

        LAWageLogDB tLAWageLogDB = new LAWageLogDB();
        LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();

        //如果回退操作处在同月当中，需要修改佣金提数日志表的计算止期
        if (tStartMonth.equals(tEndMonth)) {
            String tEndDate = mEndDate.substring(0, 4) +
                              mEndDate.substring(5, 7);
            //设置条件
            tLAWageLogDB.setWageNo(tEndDate);
            tLAWageLogDB.setManageCom(mLAWageLogSchema.getManageCom());
            tLAWageLogDB.setBranchType(mLAWageLogSchema.getBranchType());
            tLAWageLogDB.setBranchType2(mLAWageLogSchema.getBranchType2());

            //缓存待处理的数据
            mLAWageLogSet = tLAWageLogDB.query();

            if (mLAWageLogSet.size() > 0) {
                String tEndDateI = mEndDate2.substring(0, 4) +
                                   mEndDate2.substring(5, 7);
                mLAWageLogSet.get(1).setEndDate(tEndDateI);
                return "1";
            }

            return "0";
        }

        //回退后的年月
        String tEndYM = mEndDate2.substring(0, 4) + mEndDate2.substring(5, 7);
        //查询用SQL 查询准备删除的数据  佣金提数日志表
        String tSql = "SELECT *";
        tSql += "  FROM LAWageLog";
        tSql += " WHERE WageNo > '" + tEndYM + "'";
        tSql += "   AND ManageCom = '" + mLAWageLogSchema.getManageCom() + "'";
        tSql += "   AND BranchType = '" + mLAWageLogSchema.getBranchType() +
                "'";
        tSql += "   AND BranchType2 = '" + mLAWageLogSchema.getBranchType2() +
                "'";

        mLAWageLogSet = tLAWageLogDB.executeQuery(tSql);
        if (mLAWageLogSet.size() == 0) {
            return "0";
        }

        //查询用SQL 查询准备删除的数据  佣金计算历史
        tSql = "SELECT *";
        tSql += "  FROM LAWageHistory";
        tSql += " WHERE WageNo > '" + tEndYM + "'";
        tSql += "   AND AClass = '03'";
        tSql += "   AND ManageCom = '" + mLAWageLogSchema.getManageCom() + "'";
        tSql += "   AND BranchType = '" + mLAWageLogSchema.getBranchType() +
                "'";
        tSql += "   AND BranchType2 = '" + mLAWageLogSchema.getBranchType2() +
                "'";

        mLAWageHistorySet = tLAWageHistoryDB.executeQuery(tSql);
        if (mLAWageHistorySet.size() == 0) {
            return "0";
        }

        return "2";
    }

    /**
     * 明晰数据准备
     * 直接佣金明细表（佣金扎账表）/直接佣金明细备份表（佣金扎账表）
     * @return boolean
     */
    private boolean dealLACommision(String tStartNo,String tEndNo) {
        //查询直接佣金明细表（佣金扎账表）的数据
        LACommisionDB tLACommisionDB = new LACommisionDB();
        //设置查询条件
        String tSql = "SELECT *";
        tSql += "  FROM LACommision";
        tSql += " WHERE";
        tSql += "       ManageCom like '" + mLAWageLogSchema.getManageCom() +
                "%' AND";
        tSql += "       BranchType = '" + mLAWageLogSchema.getBranchType() +
                "' AND";
        tSql += "       BranchType2 = '" + mLAWageLogSchema.getBranchType2() +
                "' AND";
        tSql += "       TMakeDate >= '" + mEndDate2 + "' AND";
        tSql += "       TMakeDate <= '" + mEndDate + "' and wageno>='" +
                tStartNo+"' and wageno <= '"+tEndNo+"' ";
        System.out.println("SQL[1]:" + tSql);

        //执行查询获得结果
        LACommisionSet tLACommisionSet = tLACommisionDB.executeQuery(tSql);
        //判断查询是否失败
        if (tLACommisionSet == null) {
            return false;
        }

        //如果查询结果大于零，进行数据备份
        if (tLACommisionSet.size() > 0) {
            if (!dealLACommisionB(tLACommisionSet)) {
                return false;
            }
        }

        //缓存待处理的数据
        mLACommisionSet = tLACommisionSet;

        return true;
    }

    /**
     * 进行数据备份处理
     * 直接佣金明细备份表（佣金扎账表）
     * @param pmLACommisionSet LACommisionSet
     * @return boolean
     */
    private boolean dealLACommisionB(LACommisionSet pmLACommisionSet) {
        //取得直接佣金明细表（佣金扎账表）的数据数
        int iMax = pmLACommisionSet.size();
        LACommisionBSet tLACommisionBSet = new LACommisionBSet();
        //创建转储号码
        String tNewEdorNo = PubFun1.CreateMaxNo("EDORNO", 20);
        //缓存转储号码
        mNewEdorNo = tNewEdorNo;

        for (int i = 1; i <= iMax; i++) {
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            tLACommisionSchema.setSchema(pmLACommisionSet.get(i).getSchema());

            if (tLACommisionSchema != null) {
                LACommisionBSchema tLACommisionBSchema =
                        setLACommisionB(tLACommisionSchema, tNewEdorNo);
                tLACommisionBSet.add(tLACommisionBSchema);
            }
        }

        //缓存待处理的数据
        mLACommisionBSet = tLACommisionBSet;

        return true;
    }

    /**
     * 设置直接佣金明细备份表（佣金扎账表）
     * @param pmLACommisionSchema LACommisionSchema
     */
    private LACommisionBSchema setLACommisionB(LACommisionSchema
                                               pmLACommisionSchema,
                                               String pmNewEdorNo) {
        Reflections tReflections = new Reflections();
        LACommisionBSchema tLACommisionBSchema = new LACommisionBSchema();

        tReflections.transFields(tLACommisionBSchema, pmLACommisionSchema);

        //tLACommisionBSchema.setEdorNo(PubFun1.CreateMaxNo("EDORNO",20));
        tLACommisionBSchema.setEdorNo(pmNewEdorNo);
        tLACommisionBSchema.setEdorType("08");
        //System.out.println(tLACommisionBSchema.getManageCom());
        //System.out.println(tLACommisionBSchema.getBranchType());
        //System.out.println(tLACommisionBSchema.getBranchType2());

        return tLACommisionBSchema;
    }

    /**
     * 校验佣金计算时间、如果佣金计算完毕则返回false
     * @return boolean
     */
    private boolean checkWageLog(String tStartNo,String tEndNo) {
        String tWageNo = tStartNo;
        String tSql = "";
        int tCount = 0;
        PreparedStatement ps = null;
        ResultSet rs = null;

        //校验回滚期间内是否有佣金计算完毕的情况
        tSql += "SELECT";
        tSql += "    count(WageNo)";
        tSql += "  FROM";
        tSql += "    LAWageHistory";
        tSql += " WHERE";
        tSql += "    WageNo >= '" + tWageNo + "' AND";
        tSql += "    ManageCom like '" + mLAWageLogSchema.getManageCom() +
                "%' AND";
        tSql += "    BranchType = '" + mLAWageLogSchema.getBranchType() +
                "' AND";
        tSql += "    BranchType2 = '" + mLAWageLogSchema.getBranchType2() +
                "' AND";
        tSql += "    AClass = '03' AND";
        tSql += "    State = '14'";

        try {
            Connection conn = DBConnPool.getConnection();
            ps = conn.prepareStatement(tSql);
            rs = ps.executeQuery();
            if (rs.next()) {
                tCount = rs.getInt(1);
                rs.close();
                ps.close();
            }
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LADataGatherUI";
            tError.functionName = "checkWageLog";
            tError.errorMessage = "读取数据库失败！";
            this.mErrors.addOneError(tError);
            return false;
        }

        //佣金计算完毕,不允许回退
        if (tCount > 0) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LADataGatherUI";
            tError.functionName = "checkWageLog";
            tError.errorMessage = "要求回退的期间内,佣金计算已经完毕,不允许回退!";
            System.out.println("要求回退的期间内,佣金计算已经完毕,不允许回退!");
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 取得制定条件下的最大计算止期
     * @return String
     */
    private void getMaxEndDate() {
        String tSql = "";
        String tEndDate = "";
        String tEndDate2 = "";
        PreparedStatement ps = null;
        ResultSet rs = null;

        //Sql文
//        tSql = "SELECT";
//        tSql += "    MAX(EndDate),";
//        tSql += "    MAX(EndDate) - " + mDayNum + " days";
//        tSql += "  FROM";
//        tSql += "    LAWageLog";
//        tSql += " WHERE";
//        tSql += "    ManageCom = '" + mLAWageLogSchema.getManageCom() + "' AND";
//        tSql += "    BranchType = '" + mLAWageLogSchema.getBranchType() +
//                "' AND";
//        tSql += "    BranchType2 = '" + mLAWageLogSchema.getBranchType2() + "'";

        tSql = "select current date,current date - " + this.mDayNum +
               " days from dual";
        System.out.println("SQL:" + tSql);

        try {
            Connection conn = DBConnPool.getConnection();
            ps = conn.prepareStatement(tSql);
            rs = ps.executeQuery();
            if (rs.next()) {
                tEndDate = rs.getString(1);
                tEndDate2 = rs.getString(2);
                rs.close();
                ps.close();
            }
            conn.close();
            mEndDate = AgentPubFun.formatDate(tEndDate,"yyyy-MM-dd");
            mEndDate2 = AgentPubFun.formatDate(tEndDate2,"yyyy-MM-dd");;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * 从传入参数中得到全部数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //得到全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LADataGatherUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }

        //得到该处理的数据
        mLAWageLogSchema.setSchema((LAWageLogSchema) cInputData.
                                   getObjectByObjectName(
                                           "LAWageLogSchema", 0));

        if (mLAWageLogSchema == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LADataGatherUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入数据失败！";
            this.mErrors.addOneError(tError);

            return false;
        }

        //取得回退天数
        mDayNum = mLAWageLogSchema.getWageNo();
        System.out.println("回退天数：" + mDayNum);
        System.out.println("管理机构：" + mLAWageLogSchema.getManageCom());

        return true;
    }

}
