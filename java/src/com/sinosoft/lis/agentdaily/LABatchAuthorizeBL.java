/*
 * <p>ClassName: LABatchAuthorizeBL </p>
 * <p>Description: ALAAuthorizeUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-15
 */
package com.sinosoft.lis.agentdaily;

import java.sql.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class LABatchAuthorizeBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private String CurrentDate=PubFun.getCurrentDate();
    private String CurrentTime=PubFun.getCurrentTime();
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAAgentSet mLAAgentSet = new LAAgentSet();   // 被授权人员
    private LAAuthorizeSet mLAAuthorizeSet = new LAAuthorizeSet();      // 被授于的权限
    private LAAuthorizeSet mINLAAuthorizeSet = new LAAuthorizeSet();      // 被授于的权限
    private LAAuthorizeBSet mUPLAAuthorizeBSet = new LAAuthorizeBSet();      // 备份的信息
    private LAAuthorizeSet mDELELAAuthorizeSet = new LAAuthorizeSet();      // 原来的权限

    private String mAgentCode   = "";                    // 授权业务员代码
    private String mAgentName   = "";                    // 授权业务员姓名
    private String mManageCom   = "";                    // 授权管理机构
    private String mBranchAttr  = "";                    // 授权团队
    private String mAgentGrade  = "";                    // 授权职级
    private String mWorkYears   = "";                    // 授权工龄
    private String mBatchMark   = "";                    // 是否按条件批量授权标记
    private String mBranchType  = "";                    // 展业类型
    private String mBranchType2 = "";                    // 渠道
    private String mEdorNO = "";
    private String mRiskCode="";
    private MMap mMap = new MMap();

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        mEdorNO=PubFun1.CreateMaxNo("AthorEdorNO", 20);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行有关验证,只有插入(执行限制授权)时需要
        if(mOperate.equals("INSERT||MAIN"))
        {
            if (!check()) {
                return false;
            }
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {

        } else {

            System.out.println("Start LABatchAuthorizeBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, "")) {
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LABatchAuthorizeBL";
                tError.functionName = "submitData";
                tError.errorMessage = "PubSubmit保存数据失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }

    /**
     * 准备往后台的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {

        try {
            mMap.put(mDELELAAuthorizeSet, "DELETE");//删除原来的重新插入后便是修改
            mMap.put(mINLAAuthorizeSet, "INSERT");//插入
            mMap.put(mUPLAAuthorizeBSet, "INSERT");//备份
             mInputData = new VData();
             mInputData.add(mMap);
         } catch (Exception ex) {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "LABatchAuthorizeBL";
             tError.functionName = "prepareData";
             tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
             this.mErrors.addOneError(tError);

             return false;
         }

        return true;
    }

    /**
     * 进行业务处理
     * @return boolean
     */
    private boolean dealData()
    {
        System.out.println("Start LABatchAuthorizeBL Submit..."+mOperate);
        // 判断如果是增加限制权限
        if("INSERT||MAIN".equals(this.mOperate))
        {//增加限制权限
            if(!dealIntegerData())
            {
                return false;
            }
        }else if("DELETE||MAIN".equals(this.mOperate))
        {//消除限制权限(暂时没实现)
            if(!dealDeleteData())
            {
                return false;
            }
        }

        return true;
    }

    /**
     * 消除限制权限处理( )
     * @return boolean
     */
    private boolean dealDeleteData()
    {
            if ("TRUE".equals(this.mBatchMark)) { //按条件范围授权
                // 先选择出范围内的全体人员
                String tSQL = "SELECT";
                tSQL += "     a.*";
                tSQL += " FROM    laAuthorize a,laagent b,latree  c,LABranchGroup d";
                tSQL += "    WHERE  a.authortype ='0' and  a.authorobj = b.AgentCode ";
                tSQL += " and a.authorobj = c.AgentCode and   d.AgentGroup = b.AgentGroup";
                tSQL += " AND a.BranchType='" + this.mBranchType + "'";
                tSQL += " AND a.BranchType2='" + this.mBranchType2 + "'";
                tSQL += " AND b.AgentState in ('01','02')";
                if (!"".equals(this.mWorkYears)) { // 加入工龄条件
                    tSQL += " AND b.EmployDate + " + this.mWorkYears +
                            " year <= current date";
                }
                if (!"".equals(this.mAgentCode)) { // 加入业务员编码条件
                    tSQL += " AND b.AgentCode = '" + this.mAgentCode + "'";
                }
                if (!"".equals(this.mAgentName)) { // 加入业务员姓名条件
                    tSQL += " AND b.Name LIKE '" + this.mAgentName + "%'";
                }
                if (!"".equals(this.mManageCom)) { // 加入管理机构条件
                    tSQL += " AND b.ManageCom LIKE '" + this.mManageCom + "%'";
                }
                if (!"".equals(this.mBranchAttr)) { // 加入销售机构条件
                    tSQL += " AND d.BranchAttr LIKE '" + this.mBranchAttr +
                            "%'";
                }
                if (!"".equals(this.mAgentGrade)) { // 加入职级条件
                    tSQL += " AND c.AgentGrade >= '" + this.mAgentGrade + "'";
                }
                if (!"".equals(this.mRiskCode)) { // 加入险种条件
                     tSQL += " AND a.riskcode = '" + this.mRiskCode + "'";
                }
                tSQL += " ORDER BY d.BranchAttr,b.AgentCode";

                LAAuthorizeDB tLAAuthorizeDB = new LAAuthorizeDB();
                LAAuthorizeSet tLAAuthorizeSet = new LAAuthorizeSet();
                tLAAuthorizeSet = tLAAuthorizeDB.executeQuery(tSQL);
                System.out.println(tSQL);
                if(tLAAuthorizeSet.size()<=0)
                {
                    CError tError = new CError();
                    tError.moduleName = "LABatchAuthorizeBL";
                    tError.functionName = "dealDeleteData";
                    tError.errorMessage = "没有符合条件的授权信息存在,不需要进行取消限制授权！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                mLAAuthorizeSet.clear();
                mLAAuthorizeSet.set(tLAAuthorizeSet);
            }
            else if("FALSE".equals(this.mBatchMark))
            {//按被选择的人员授权
                if(mLAAuthorizeSet.size()<=0)
                {
                    CError tError = new CError();
                    tError.moduleName = "LABatchAuthorizeBL";
                    tError.functionName = "dealDeleteData";
                    tError.errorMessage = "没有选择要取消的限制授权信息！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            if(!dealDeleteAuthorize())
            {
                System.out.println("删除失败！");
                return false;
            }
           System.out.println("删除失败！");


         return true;
    }
    /**
     * 增加限制权限处理
     * @return boolean
     */
    private boolean dealIntegerData()
    {
        // 如果标记需要进行指定条件人员授权  和直接对选择出来的人员授权分开处理
        if("TRUE".equals(this.mBatchMark))
        {//按条件范围授权
            // 先选择出范围内的全体人员
            String tSQL = "SELECT";
            tSQL += "     a.*";
            tSQL += " FROM";
            tSQL += "     LAAgent a,";
            tSQL += "     LABranchGroup b,";
            tSQL += "     LATree c";
            tSQL += " WHERE";
            tSQL += "     a.AgentGroup = b.AgentGroup";
            tSQL += " AND a.AgentCode = c.AgentCode";
            tSQL += " AND a.BranchType='" + this.mBranchType + "'";
            tSQL += " AND a.BranchType2='" + this.mBranchType2 + "'";
            tSQL += " AND a.AgentState in ('01','02')";
            if(!"".equals(this.mWorkYears))
            {// 加入工龄条件
                tSQL += " AND a.EmployDate + " + this.mWorkYears +
                        " year > current date";
            }
            if(!"".equals(this.mAgentCode))
            {// 加入业务员编码条件
                tSQL += " AND a.AgentCode LIKE '" + this.mAgentCode + "%'";
            }
            if(!"".equals(this.mAgentName))
            {// 加入业务员姓名条件
                tSQL += " AND a.Name LIKE '" + this.mAgentName + "%'";
            }
            if(!"".equals(this.mManageCom))
            {// 加入管理机构条件
                tSQL += " AND a.ManageCom LIKE '" + this.mManageCom + "%'";
            }
            if(!"".equals(this.mBranchAttr))
            {// 加入销售机构条件
                tSQL += " AND b.BranchAttr LIKE '" + this.mBranchAttr + "%'";
            }
            if(!"".equals(this.mAgentGrade))
            {// 加入职级条件
                tSQL += " AND c.AgentGrade < '" + this.mAgentGrade + "%'";
            }
            tSQL += " ORDER BY a.ManageCom,b.BranchAttr,a.AgentCode";
            System.out.println("进行符合条件的业务员查询SQL：[ "+tSQL+" ]");

            LAAgentDB tLAAgentDB = new LAAgentDB();
            LAAgentSet tLAAgentSet = new LAAgentSet();
            tLAAgentSet = tLAAgentDB.executeQuery(tSQL);
            System.out.println("符合条件的人数：[ "+tLAAgentSet.size()+" ]");
            // 判断是否查到符合条件的人员
            if(tLAAgentSet.size()==0)
            {// @@错误处理
                CError tError = new CError();
                tError.moduleName = "LABatchAuthorizeBL";
                tError.functionName = "dealIntegerData";
                tError.errorMessage = "没有符合条件的人员存在！";
                this.mErrors.addOneError(tError);
                System.out.println("根据条件查询业务员失败！");
                return false;
            }
            // 给全局变量赋值
            mLAAgentSet.clear();
            mLAAgentSet.set(tLAAgentSet);
            // 授权人员已经确定了，授权险种也已经确定了，调用dealPeopleAuthorize执行授权
            if(!dealPeopleAuthorize())
            {
                System.out.println("对选择的人群进行授权失败！");
                return false;
            }
        }else if("FALSE".equals(this.mBatchMark))
        {//按被选择的人员授权
            if(!dealPeopleAuthorize())
            {
                System.out.println("对选择的人群进行授权失败！");
                return false;
            }
        }

        return true;
    }
  //处理取消限制授权
    private boolean     dealDeleteAuthorize()
    {
        for(int  i=1;i<=mLAAuthorizeSet.size();i++)
        {
            LAAuthorizeSchema tLAAuthorizeSchema = new LAAuthorizeSchema();
            tLAAuthorizeSchema=mLAAuthorizeSet.get(i);
            LAAuthorizeDB tLAAuthorizeDB = new LAAuthorizeDB();
            tLAAuthorizeDB.setAuthorType("0");
            tLAAuthorizeDB.setAuthorObj(tLAAuthorizeSchema.getAuthorObj());
            tLAAuthorizeDB.setRiskCode(tLAAuthorizeSchema.getRiskCode());
            tLAAuthorizeDB.getInfo();
            tLAAuthorizeSchema=tLAAuthorizeDB.getSchema();
            //备份原来的信息
            LAAuthorizeBSchema upLAAuthorizeBSchema = new LAAuthorizeBSchema();
            Reflections tReflections = new Reflections();
           // System.out.println("12222222222222222222222222"+mupLAContFYCRateSet.size());
            tReflections.transFields(upLAAuthorizeBSchema, tLAAuthorizeSchema);
            upLAAuthorizeBSchema.setEdorNo(mEdorNO);
            upLAAuthorizeBSchema.setEdorType("02");
            mUPLAAuthorizeBSet.add(upLAAuthorizeBSchema);
        }
        mDELELAAuthorizeSet=mLAAuthorizeSet;//删除信息
       System.out.println("12222222222222222222222222delete"+mDELELAAuthorizeSet.size());
        return true;
    }
    /**
     * 按被选择的人员授权
     * @return boolean
     */
    private boolean dealPeopleAuthorize()
    {
        int tAgentCount = 0;           // 参加考核的人数
        tAgentCount = mLAAgentSet.size();
        for(int i=1;i<=tAgentCount;i++)
        {
            String tRiskCodeNew = "'";
            LAAgentSchema tLAAgentSchema = new LAAgentSchema();
            tLAAgentSchema.setSchema(mLAAgentSet.get(i));
            System.out.println("对["+tLAAgentSchema.getAgentCode()+"]进行处理！");

            // 准备授权数据
            LAAuthorizeSet tLAAuthorizeSet = new LAAuthorizeSet();
            // a、加入新增限制权限
            for(int j=1;j<=mLAAuthorizeSet.size();j++)
            {
                LMRiskAppSet tLMRiskAppSet = new LMRiskAppSet();
                LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
                tLMRiskAppDB.setRiskCode(mLAAuthorizeSet.get(j).getRiskCode());
                tLMRiskAppSet = tLMRiskAppDB.query();
                LAAuthorizeSchema tLAAuthorizeSchema = new LAAuthorizeSchema();
                // 设置险种、授权起期、授权止期
                tLAAuthorizeSchema.setSchema(mLAAuthorizeSet.get(j));
                tLAAuthorizeSchema.setRiskType(tLMRiskAppSet.get(1).
                                getRiskType());
                // 设置授权对象编码
                tLAAuthorizeSchema.setAuthorObj(tLAAgentSchema.getAgentCode());
                // 设置授权对象类别（业务员）
                tLAAuthorizeSchema.setAuthorType("0");
                // 设置展业类型
                tLAAuthorizeSchema.setBranchType(this.mBranchType);
                // 设置渠道
                tLAAuthorizeSchema.setBranchType2(this.mBranchType2);

                LAAuthorizeDB tLAAuthorizeDB = new LAAuthorizeDB();
                tLAAuthorizeDB.setAuthorType("0");
                tLAAuthorizeDB.setAuthorObj(tLAAgentSchema.getAgentCode());
                tLAAuthorizeDB.setRiskCode(tLAAuthorizeSchema.getRiskCode());
                if(!tLAAuthorizeDB.getInfo())
                {
                tLAAuthorizeSchema.setOperator(mGlobalInput.Operator);
                tLAAuthorizeSchema.setMakeDate(CurrentDate);
                tLAAuthorizeSchema.setMakeTime(CurrentTime);
                }
                else
                {
                    //如果原来的信息存在需要备份并修改
                 LAAuthorizeSchema deLAAuthorizeSchema = new LAAuthorizeSchema();
                 deLAAuthorizeSchema=tLAAuthorizeDB.getSchema();
                 tLAAuthorizeSchema.setOperator(deLAAuthorizeSchema.getOperator());
                 tLAAuthorizeSchema.setMakeDate(deLAAuthorizeSchema.getMakeDate());
                 tLAAuthorizeSchema.setMakeTime(deLAAuthorizeSchema.getMakeTime());
                 //删除原来的信息
                 mDELELAAuthorizeSet.add(tLAAuthorizeSchema);
                 //备份原来的信息
                 LAAuthorizeBSchema upLAAuthorizeBSchema = new LAAuthorizeBSchema();
                 Reflections tReflections = new Reflections();
                // System.out.println("12222222222222222222222222"+mupLAContFYCRateSet.size());
                 tReflections.transFields(upLAAuthorizeBSchema, deLAAuthorizeSchema);
                 upLAAuthorizeBSchema.setEdorNo(mEdorNO);
                 upLAAuthorizeBSchema.setEdorType("01");
                 mUPLAAuthorizeBSet.add(upLAAuthorizeBSchema);
                }
                tLAAuthorizeSchema.setModifyDate(CurrentDate);
                tLAAuthorizeSchema.setModifyTime(CurrentTime);
                // 加入授权数据集
                tLAAuthorizeSet.add(tLAAuthorizeSchema);

                tRiskCodeNew += tLAAuthorizeSchema.getRiskCode() + "','";
            }

//           tRiskCodeNew += "'";

            // b、加入原有限制权限（不含新设置的险种）
//            LAAuthorizeDB tLAAuthorizeInhereDB = new LAAuthorizeDB();
//            LAAuthorizeSet tLAAuthorizeInhereSet = new LAAuthorizeSet();
//            String tSQL = "SELECT * FROM LAAuthorize WHERE AuthorType='0'";
//            tSQL += " AND AuthorObj='"+tLAAgentSchema.getAgentCode()+"'";
//            tSQL += " AND RiskCode NOT IN ("+tRiskCodeNew+")";
//            System.out.println("查询原来有没有分配过被限制的权限[ "+tSQL+" ]");
//            tLAAuthorizeInhereSet = tLAAuthorizeInhereDB.executeQuery(tSQL);
            // 如果存在原先设置的限制权限  重新加入
//            tLAAuthorizeSet.add(tLAAuthorizeInhereSet);
            /*for(int k=1;k<=tLAAuthorizeInhereSet.size();k++)
            {
                LAAuthorizeSchema tLAAuthorizeSchema = new LAAuthorizeSchema();
                // 设置险种、授权起期、授权止期
                tLAAuthorizeSchema.setSchema(tLAAuthorizeInhereSet.get(k));
                // 设置授权对象编码
                tLAAuthorizeSchema.setAuthorObj(tLAAgentSchema.getAgentCode());
                // 设置授权对象类别（业务员）
                tLAAuthorizeSchema.setAuthorType("0");
                // 设置展业类型
                tLAAuthorizeSchema.setBranchType(this.mBranchType);
                // 设置渠道
                tLAAuthorizeSchema.setBranchType2(this.mBranchType2);
                // 加入授权数据集
                tLAAuthorizeSet.add(tLAAuthorizeSchema);
            }*/

            // 传到ALAAuthorizeUI类中进行授权处理
//            if(!dealAuthorize(tLAAuthorizeSet))
//            {
//                return false;
//            }
            mINLAAuthorizeSet.add(tLAAuthorizeSet);
        }

        return true;
    }

    /**
     * 把传进来的授权数据 经过ALAAuthorizeUI类来处理  进行授权
     * @param pmLAAuthorizeSet LAAuthorizeSet
     * @return boolean
     */
    private boolean dealAuthorize(LAAuthorizeSet pmLAAuthorizeSet)
    {
        // 判断是否存在授权数据
        if(pmLAAuthorizeSet.size()==0)
            return true;
        // 个人授权类
        ALAAuthorizeUI tALAAuthorizeUI  = new ALAAuthorizeUI();
        // 传递操作符
        String tOperate="INSERT||MAIN";
        // 准备传输数据 VData
        VData tVData = new VData();
        LAAuthorizeSchema tLAAuthorizeSchema = new LAAuthorizeSchema();
        tVData.add(mGlobalInput);
        tVData.addElement(pmLAAuthorizeSet);
        tVData.add(tLAAuthorizeSchema);

        // 进行授权处理
        if(!tALAAuthorizeUI.submitData(tVData,tOperate))
        {// @@错误处理
            this.mErrors.copyAllErrors(tALAAuthorizeUI.mErrors);
            CError tError = new CError();
            tError.moduleName = "LABatchAuthorizeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            System.out.println("进行个人授权处理失败！业务员代码：[ " +
                               pmLAAuthorizeSet.get(1).getAgentCode() + " ]");
            return false;
        }

        return true;
    }

    /**
     * 进行有关验证
     * @return boolean
     */
    private boolean check()
    {
        // 有没有人  有没有险种
        if(mLAAgentSet.size()==0 && "FALSE".equals(mBatchMark))
        {// @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABatchAuthorizeBL";
            tError.functionName = "check";
            tError.errorMessage = "没有被授权人员，请选择被授权人员！";
            this.mErrors.addOneError(tError);

            return false;
        }
        if(mLAAuthorizeSet.size()==0)
        {// @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABatchAuthorizeBL";
            tError.functionName = "check";
            tError.errorMessage = "没有选择险种，请选择被授予的限制权限！";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return int
     */
    private int execQuery(String sql)
    {
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null)return 0;
            st = conn.prepareStatement(sql);
            if (st == null)return 0;
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
                st.close();
                rs.close();
                conn.close();
                st = null;
                rs = null;
                conn = null;
            } catch (Exception e) {}
        }
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData pmInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) pmInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLAAgentSet.set((LAAgentSet) pmInputData.getObjectByObjectName(
                "LAAgentSet", 0));
        mLAAuthorizeSet.set((LAAuthorizeSet) pmInputData.getObjectByObjectName(
                "LAAuthorizeSet", 0));
        this.mAgentCode = (String) pmInputData.getObject(0);
        this.mAgentName = (String) pmInputData.getObject(1);
        this.mManageCom = (String) pmInputData.getObject(2);
        this.mBranchAttr = (String) pmInputData.getObject(3);
        this.mAgentGrade = (String) pmInputData.getObject(4);
        this.mWorkYears = (String) pmInputData.getObject(5);
        this.mBatchMark = (String) pmInputData.getObject(6);
        this.mBranchType = (String) pmInputData.getObject(7);
        this.mBranchType2 = (String) pmInputData.getObject(8);
        this.mRiskCode = (String) pmInputData.getObject(9);


        System.out.println("人员列表中人数:[" + mLAAgentSet.size() + "]");
        System.out.println("险种列表中个数:[" + mLAAuthorizeSet.size() + "]");
        System.out.println("[" + mBranchAttr + "] [" + mAgentGrade + "] [" +
                           mWorkYears + "] [" + mBatchMark + "] [" +
                           mBranchType +
                           "] [" + mBranchType2 + "]");
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAuthorizeUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
