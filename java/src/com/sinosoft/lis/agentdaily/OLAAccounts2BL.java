/*
 * <p>ClassName: OLAAccountsBL </p>
 * <p>Description: OLAAccountsBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:03:36
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.db.LAAccountsDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAAccountsBSchema;
import com.sinosoft.lis.schema.LAAccountsSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LAAccountsSet;
import com.sinosoft.lis.vschema.LAAccountsBSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;

public class OLAAccounts2BL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAAccountsSchema mLAAccountsSchema = new LAAccountsSchema();
    private LAAgentSchema mLAAgentSchema = new LAAgentSchema();
    private LAAccountsBSchema mLAAccountsBSchema = new LAAccountsBSchema();
    private LAAccountsBSet mLAAccountsBSet = new LAAccountsBSet();
    private TransferData mTransferData = new TransferData();
    private LJAGetSet mLJAGetSet = new LJAGetSet();

    private String mAgentCodeOld = "";

    private String mAccountOld = "";
    public OLAAccounts2BL() {
    }

    public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("dealData:" + mOperate);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        System.out.println("dealData:" + mOperate);
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAAccountsBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLAAccountsBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start OLAAccountsBL Submit...");
            OLAAccountsBLS tOLAAccounts2BLS = new OLAAccountsBLS();
            tOLAAccounts2BLS.submitData(mInputData, mOperate);
            System.out.println("End OLAAccountsBL Submit...");
            //如果有需要处理的错误，则返回
            if (tOLAAccounts2BLS.mErrors.needDealError()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tOLAAccounts2BLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLAAccountsBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("dealData:" + mOperate);
        if (this.mOperate.equals("INSERT||MAIN")) {
            if (!insertData()) {
                return false;
            }
        }
        if (this.mOperate.equals("UPDATE||MAIN")) {
            if (!updateData()) {
                return false;
            }
        }
        if (this.mOperate.equals("DELETE||MAIN")) {

        }
        return true;
    }

    private boolean insertData() {
        System.out.println("mLAAccountsSchema.getAgentCode():" +
                           mLAAccountsSchema.getAgentCode());
        System.out.println("mLAAccountsSchema.getAccount():" +
                           mLAAccountsSchema.getAccount());
        if (mLAAccountsSchema.getState().equals("1")) {
            CError tError = new CError();
            tError.moduleName = "LAAccountsBL";
            tError.functionName = "insertData";
            tError.errorMessage = "请增加有效帐号!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mLAAccountsSchema.getDestoryDate() != null) {
            CError tError = new CError();
            tError.moduleName = "LAAccountsBL";
            tError.functionName = "insertData";
            tError.errorMessage = "增加帐号时不能有销户日期信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        String sql = "select * from laaccounts where agentcode ='" +
                     mLAAccountsSchema.getAgentCode() + "' and account='" +
                     mLAAccountsSchema.getAccount() + "'";

        System.out.println(sql);
        LAAccountsDB tLAAccountsDB = new LAAccountsDB();
        LAAccountsSet tLAAccountsSet = new LAAccountsSet();
        tLAAccountsSet = tLAAccountsDB.executeQuery(sql);

        if (tLAAccountsSet != null && tLAAccountsSet.size() > 0) {
            CError tError = new CError();
            tError.moduleName = "LAAccountsBL";
            tError.functionName = "insertData";
            tError.errorMessage = "该业务员有账号" + mLAAccountsSchema.getAccount() +
                                  "!";
            this.mErrors.addOneError(tError);
            return false;
        }
        sql = "select * from laaccounts where agentcode ='" +
              mLAAccountsSchema.getAgentCode() + "' and state<>'1'";
        tLAAccountsDB = new LAAccountsDB();
        tLAAccountsSet = new LAAccountsSet();
        tLAAccountsSet = tLAAccountsDB.executeQuery(sql);
        if (tLAAccountsSet != null && tLAAccountsSet.size() > 0) {
            CError tError = new CError();
            tError.moduleName = "LAAccountsBL";
            tError.functionName = "insertData";
            tError.errorMessage = "该业务员有账号" + tLAAccountsSet.get(1).getAccount() +
                                  "处于有效状态，不能新增账号！";
            this.mErrors.addOneError(tError);
            return false;
        }
        //
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        this.mLAAccountsSchema.setMakeDate(currentDate);
        this.mLAAccountsSchema.setMakeTime(currentTime);
        this.mLAAccountsSchema.setModifyDate(currentDate);
        this.mLAAccountsSchema.setModifyTime(currentTime);
        this.mLAAccountsSchema.setOperator(mGlobalInput.Operator);
        String agentCode = mLAAccountsSchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(agentCode);
        if (!tLAAgentDB.getInfo()) {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAccountsBL";
            tError.functionName = "insertData";
            tError.errorMessage = "查找业务员基本信息时失败!";
            this.mErrors.addOneError(tError);
            return false;

        }
        tLAAgentDB.setBankAccNo(mLAAccountsSchema.getAccount());
        tLAAgentDB.setBankCode(mLAAccountsSchema.getBank());
        mLAAgentSchema = tLAAgentDB.getSchema();
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        if (mLAAccountsSchema.getState().equals("1") &&
            mLAAccountsSchema.getDestoryDate() == null) {
            CError tError = new CError();
            tError.moduleName = "LAAccountsBL";
            tError.functionName = "updateData";
            tError.errorMessage = "账号" + mLAAccountsSchema.getAccount() +
                                  "设置为无效，销户日期不能为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mLAAccountsSchema.getState().equals("0") &&
            mLAAccountsSchema.getDestoryDate() != null) {
            CError tError = new CError();
            tError.moduleName = "LAAccountsBL";
            tError.functionName = "updateData";
            tError.errorMessage = "账号" + mLAAccountsSchema.getAccount() +
                                  "设置为有效，不能填写销户日期!";
            this.mErrors.addOneError(tError);
            return false;
        }
//        String sql = "select * from laaccounts where agentcode ='" +
//                     mLAAccountsSchema.getAgentCode() + "' and account='" +
//                     mLAAccountsSchema.getAccount() + "'";
//        String sql = "select * from laaccounts where agentcode ='" +
//                     mAgentCodeOld + "' and account='" +
//                     mAccountOld + "'";
//
//        System.out.println(sql);
        LAAccountsDB tLAAccountsDB = new LAAccountsDB();
        LAAccountsSet tLAAccountsSet = new LAAccountsSet();
        LJAGetSet tLJAGetSet = new LJAGetSet();
        LJAGetDB tLJAGetDB = new LJAGetDB();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        tLAAgentDB.setAgentCode(mAgentCodeOld);
        tLAAccountsDB.getInfo();
        tLAAgentSchema = tLAAgentDB.getSchema();
        String tBranchType = tLAAgentSchema.getBranchType();
        String tBranchType2 = tLAAgentSchema.getBranchType2();
        if("1".equals(tBranchType) && "01".equals(tBranchType2))
        {
        	//查询未发盘成功的数据
            String tjudgeSQL = " select * from ljaget where GetNoticeNo = '"+mAgentCodeOld+"' and BankAccNo = '"+mAccountOld+"'" +
            		           " and not exists (select 1 from ljfiget where actugetno=ljaget.actugetno) with ur ";
            tLJAGetSet = tLJAGetDB.executeQuery(tjudgeSQL);
            if(tLJAGetSet.size()>0)
            {
            	if(!queryLjaget(mAgentCodeOld, mAccountOld))
                {
            		CError tError = new CError();
                    tError.moduleName = "LAAccountsBL";
                    tError.functionName = "updateData";
                    tError.errorMessage = "薪资审核发放之后，且在发盘途中，不能修改银行帐号相关信息！";
                    this.mErrors.addOneError(tError);
                    return false;
            		
//            			for (int i = 1; i <= tLJAGetSet.size(); i++) {
//                    		LJAGetSchema tLJAGetSchema = new LJAGetSchema();
//                    		tLJAGetSchema = tLJAGetSet.get(i);
//                    		tLJAGetSchema.setAccName(mLAAccountsSchema.getAccountName());
//                    		tLJAGetSchema.setBankCode(mLAAccountsSchema.getBankCode());
//                    		tLJAGetSchema.setBankAccNo(mLAAccountsSchema.getAccount());
//                    		tLJAGetSchema.setOperator(this.mGlobalInput.Operator);
//            	            tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
//            	            tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
//            	            this.mLJAGetSet.add(tLJAGetSchema);
//            			}
            
            		
                }
            	else
            	{
            		for (int i = 1; i <= tLJAGetSet.size(); i++) {
                		LJAGetSchema tLJAGetSchema = new LJAGetSchema();
                		tLJAGetSchema = tLJAGetSet.get(i);
                		tLJAGetSchema.setAccName(mLAAccountsSchema.getAccountName());
                		tLJAGetSchema.setBankCode(mLAAccountsSchema.getBankCode());
                		tLJAGetSchema.setBankAccNo(mLAAccountsSchema.getAccount());
                		tLJAGetSchema.setOperator(this.mGlobalInput.Operator);
        	            tLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
        	            tLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
        	            this.mLJAGetSet.add(tLJAGetSchema);
            	}
            	
            	
            }
            } 
            //未发盘成功时，不能将银行帐号置为无效
            if(mLAAccountsSchema.getState().equals("1"))
            {
            	if(tLJAGetSet.size()>0)
            	{
            		CError tError = new CError();
                  tError.moduleName = "LAAccountsBL";
                  tError.functionName = "updataData";
                  tError.errorMessage = "薪资审核发放之后，未进行发盘操作或发盘失败，不能修改银行帐号状态为无效！";
                  this.mErrors.addOneError(tError);
                  return false;
            	}
            	
            }
        	
        }
        
      
        
//        tLAAccountsSet = tLAAccountsDB.executeQuery(sql);
//        if (tLAAccountsSet == null || tLAAccountsSet.size() == 0) {
//            CError tError = new CError();
//            tError.moduleName = "LAAccountsBL";
//            tError.functionName = "updataData";
//            tError.errorMessage = "该业务员没有账号" + mAccountOld +
//                                  "，不能进行修改!";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
//        sql = "select * from laaccounts where agentcode ='" +
//              mLAAccountsSchema.getAgentCode() + "' and state<>'1'";
//        sql = "select * from laaccounts where agentcode ='" +
//              mAgentCodeOld + "' and state<>'1'";
//
//        System.out.println(sql);
//
//        tLAAccountsDB = new LAAccountsDB();
//        tLAAccountsSet = new LAAccountsSet();
//        tLAAccountsSet = tLAAccountsDB.executeQuery(sql);
//        if (tLAAccountsSet != null && tLAAccountsSet.size() > 0) {
////            if (!mLAAccountsSchema.getAccount().trim().equals(
//            System.out.println("222222222222323232333333333");
//            if (mAgentCodeOld.equals(tLAAccountsSet.get(1).getAccount().trim())) {
//                CError tError = new CError();
//                tError.moduleName = "LAAccountsBL";
//                tError.functionName = "insertData";
//                tError.errorMessage = "该业务员有账号" +
//                                      tLAAccountsSet.get(1).getAccount() +
//                                      "处于有效状态，不能修改账号" +
//                                      mLAAccountsSchema.getAccount() + "!";
//                this.mErrors.addOneError(tError);
//                return false;
//            }
//        }
//        if(this.mLAAccountsSchema.getState().equals("0")){
//        	sql="select * from laaccounts where agentcode='"
//            	+this.mLAAccountsSchema.getAgentCode()+"'  and state='0'";
//            tLAAccountsDB = new LAAccountsDB();
//            tLAAccountsSet = new LAAccountsSet();
//            tLAAccountsSet = tLAAccountsDB.executeQuery(sql);
//            if (tLAAccountsSet != null && tLAAccountsSet.size() > 0) {
////                if (!mLAAccountsSchema.getAccount().trim().equals(
//               // System.out.println("222222222222323232333333333");
//                //if (mAgentCodeOld.equals(tLAAccountsSet.get(1).getAccount().trim())) {
//                    CError tError = new CError();
//                    tError.moduleName = "LAAccountsBL";
//                    tError.functionName = "insertData";
//                    tError.errorMessage = "该业务员有账号" +
//                                          tLAAccountsSet.get(1).getAccount() +
//                                          "处于有效状态，不能修改账号" +
//                                          mLAAccountsSchema.getAccount() + "!";
//                    this.mErrors.addOneError(tError);
//                    return false;
//               // }
//            }
//        }
        
        System.out.println("7878777777777777777778888");
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        this.mLAAccountsSchema.setModifyDate(currentDate);
        this.mLAAccountsSchema.setModifyTime(currentTime);
        this.mLAAccountsSchema.setOperator(mGlobalInput.Operator);

        String agentCode = mLAAccountsSchema.getAgentCode();
        tLAAgentDB.setAgentCode(agentCode);
        System.out.println("7878777777777777777778888");
        if (!tLAAgentDB.getInfo()) {

            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAccountsBL";
            tError.functionName = "submitData";
            tError.errorMessage = "查找业务员基本信息时失败!";
            this.mErrors.addOneError(tError);
            return false;

        }
        System.out.println("7878777777777777777778888");
        tLAAgentDB.setBankAccNo(mLAAccountsSchema.getAccount());
        tLAAgentDB.setBankCode(mLAAccountsSchema.getBank());
        mLAAgentSchema = tLAAgentDB.getSchema();
        System.out.println("7878777777777777777778888");
        //添加帐户信息表的轨迹备份
        
        String LAaccountsSql = " select * from laaccounts where agentcode = '"+mLAAccountsSchema.getAgentCode()+"' and " +
        		" Account = '"+mAccountOld +"' with ur ";
        tLAAccountsSet = tLAAccountsDB.executeQuery(LAaccountsSql);
        if(tLAAccountsSet.size() > 0){
        	LAAccountsSchema tLAAccountsSchema = new LAAccountsSchema();
        	tLAAccountsSchema = tLAAccountsSet.get(1);
        	LAAccountsBSchema tLAAccountsBSchema = new LAAccountsBSchema();
        Reflections tReflections = new Reflections();
        String tLAAccountsEdorNo = PubFun1.CreateMaxNo("AccountsEdorNo", 20);
        tReflections.transFields(tLAAccountsBSchema,
        		tLAAccountsSchema);
        tLAAccountsBSchema.setEdorNo(tLAAccountsEdorNo);
        tLAAccountsBSchema.setEdorType("1");
        mLAAccountsBSchema = tLAAccountsBSchema.getSchema();
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLAAccountsSchema.setSchema((LAAccountsSchema) cInputData.
                                         getObjectByObjectName(
                                                 "LAAccountsSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));

        this.mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);

        mAgentCodeOld = (String) mTransferData.getValueByName("AgentCodeOld");

        mAccountOld = (String) mTransferData.getValueByName("AccountOld");

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LAAccountsDB tLAAccountsDB = new LAAccountsDB();
        tLAAccountsDB.setSchema(this.mLAAccountsSchema);
        //如果有需要处理的错误，则返回
        if (tLAAccountsDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAAccountsDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAccountsBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLAAccountsSchema);
            this.mInputData.add(this.mLAAgentSchema);
            this.mInputData.add(this.mLAAccountsBSchema);
            this.mInputData.add(this.mTransferData);
            this.mInputData.add(this.mLJAGetSet);
            mResult.clear();
            mResult.add(this.mLAAccountsSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAccountsBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
    //查询在途数据
    public boolean queryLjaget(String cAgentCode,String cAccountNo)
    {  
    	String tjudgeSQL =" select actugetno from ljaget where getnoticeno ='"+cAgentCode+"' and " +
		          " bankaccno ='"+cAccountNo+"' and  BankOnTheWayFlag ='1'  with ur";
    	ExeSQL tExeSQL = new ExeSQL();
    	if(tExeSQL.getOneValue(tjudgeSQL) != null && !"".equals(tExeSQL.getOneValue(tjudgeSQL)))
    	{
    		return false;
    	}
    	
    	return true;
    }
}
