package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAConRateSchema;
import com.sinosoft.lis.vschema.LAConRateSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAECNonStandRateSetUI
{
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    public static void main(String[] args){
        VData cInputData = new VData();
        String cOperate = "UPDATE||MAIN";
        String Priview = "PREVIEW";
        boolean expectedReturn = true;
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "001";
        tGlobalInput.ManageCom = "861100";
        tGlobalInput.ComCode = "001";
        cInputData.add(tGlobalInput);
        String[] testContNo = {"1400000296"};
        LAConRateSchema tLAConRateSchema = new LAConRateSchema();
        LAConRateSet tLAConRateSet = new LAConRateSet();

     //   tLAConRateSchema.setAgentCode("1101000008");
     //   tLAConRateSchema.setOutWorkDate("2005-04-30");
     //   tLAConRateSchema.setPayDate("2005-05-01");


        tLAConRateSet.add(tLAConRateSchema);

        cInputData.add(tLAConRateSchema);
        cInputData.add(Priview);
        LAECNonStandRateSetUI tLAECNonStandRateSetUI = new LAECNonStandRateSetUI();
        boolean actualReturn = tLAECNonStandRateSetUI.submitData(cInputData, cOperate);
    }

    public boolean submitData(VData cInputData,String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        mInputData=(VData)cInputData.clone();

        LAECNonStandRateSetBL tLAECNonStandRateSetBL= new LAECNonStandRateSetBL();
        tLAECNonStandRateSetBL.submitData(mInputData,mOperate);
        //如果有需要处理的错误，则返回
        if (tLAECNonStandRateSetBL.mErrors.needDealError())
        {
          // @@错误处理
          this.mErrors.copyAllErrors(tLAECNonStandRateSetBL.mErrors);
          CError tError = new CError();
          tError.moduleName = "LAECNonStandRateSetUI";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
          this.mErrors.addOneError(tError);
          return false;
        }
        System.out.println("成功了!");
        return true;
    }
}
