package com.sinosoft.lis.agentdaily;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public class LAAMContUI
{
    public CErrors mErrors=new CErrors();
    public VData mResult =new VData();
    private VData mInputData=new VData();
    private String mOperate="";
    public LAAMContUI()
    {
    }
    public boolean submitData(VData cInputData,String cOperate)
    {
        mOperate=cOperate;
        this.mInputData =(VData)cInputData.clone() ;
        LAAMContBL tLAAMContBL=new LAAMContBL();
        try
        {
            if (!tLAAMContBL.submitData(mInputData, mOperate))
            {
                this.mErrors.copyAllErrors(tLAAMContBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAMContUI";
                tError.functionName = "submitDat";
                tError.errorMessage = "BL类处理失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        mResult=tLAAMContBL.getResult();
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

}
