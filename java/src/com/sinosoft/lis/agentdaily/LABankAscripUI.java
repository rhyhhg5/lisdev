package com.sinosoft.lis.agentdaily;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 20060406</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xiangchun
 * @version 1.0
 */
public class LABankAscripUI
{
    public CErrors mErrors=new CErrors();
    public VData mResult =new VData();
    private VData mInputData=new VData();
    private String mOperate="";
    public LABankAscripUI()
    {
    }
    public boolean submitData(VData cInputData,String cOperate)
    {
        mOperate=cOperate;
        this.mInputData =(VData)cInputData.clone() ;
        LABankAscripBL tLABankAscripBL=new LABankAscripBL();
        try
        {
            if (!tLABankAscripBL.submitData(mInputData, mOperate))
            {
                this.mErrors.copyAllErrors(tLABankAscripBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "LABankAscripUI";
                tError.functionName = "submitDat";
                tError.errorMessage = "BL类处理失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
 //       mResult=tLABankAscripBL.getResult();
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

}
