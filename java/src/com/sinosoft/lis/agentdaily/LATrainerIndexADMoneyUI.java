package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LATrainerIndexSet;
/**
 * <p>Title: LATrainerIndexADMoneyUI</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author WangQingmin
 * @version 1.0
 */
public class LATrainerIndexADMoneyUI
{
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
        public CErrors mErrors = new CErrors();
        /** 往后面传输数据的容器 */
        private VData mInputData = new VData();
        /** 数据操作字符串 */
        private String mOperate = "";
        //业务处理相关变量
        /** 全局数据 */
        private GlobalInput mGlobalInput = new GlobalInput();
        private LATrainerIndexSet mLATrainerIndexSet = new LATrainerIndexSet();
        private LATrainerIndexADMoneyBL mLATrainerIndexADMoneyBL = new LATrainerIndexADMoneyBL();
        private String mWageNo = "";
        private String mManageCom = "";
        public LATrainerIndexADMoneyUI()
        {
        }

        public static void main(String[] args)
        {

        }

        /**
         传输数据的公共方法
         */
        public boolean submitData(VData cInputData, String cOperate)
        {
                System.out.println("Begin LATrainerIndexADMoneyUI.submitData.........");
                //将操作数据拷贝到本类中
                this.mOperate = cOperate;
                //得到外部传入的数据,将数据备份到本类中
                if (!getInputData(cInputData))
                {
                        return false;
                }
                //进行业务处理
                if (!dealData())
                {
                        return false;
                }
                //准备往后台的数据
                if (!prepareOutputData())
                {
                        return false;
                }
                System.out.println("Start LATrainerIndexADMoneyUI Submit...");
                try
                {
                	mLATrainerIndexADMoneyBL.submitData(mInputData, mOperate);
                }
                catch(Exception ex){
                  ex.toString();
                }
                System.out.println("End LATrainerIndexADMoneyUI Submit...");
                //如果有需要处理的错误，则返回
                if (mLATrainerIndexADMoneyBL.mErrors.needDealError())
                {
                        // @@错误处理
                       this.mErrors.copyAllErrors(mLATrainerIndexADMoneyBL.mErrors);
                       CError tError = new CError();
                       tError.moduleName = "LATrainerIndexADMoneyUI";
                       tError.functionName = "submitData";
                       tError.errorMessage = "数据提交失败!";
                       this.mErrors.addOneError(tError);
                       return false;
                }
               mInputData = null;
                return true;
        }

        /**
         * 从输入数据中得到所有对象
         *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
         */
        private boolean getInputData(VData cInputData)
        {
            //全局变量
            System.out.println("getInputData");
            mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0));
            System.out.println("hhhhr");
            mWageNo = (String) cInputData.get(1);
            mManageCom = (String) cInputData.get(2);
            this.mLATrainerIndexSet.set((LATrainerIndexSet) cInputData.
                                                 getObjectByObjectName(
                    "LATrainerIndexSet", 0));
            if (mGlobalInput == null)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAAddBSubPerUI";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有得到足够的信息！";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("LATrainerIndexSet.size():"+mLATrainerIndexSet.size());
            return true;
        }

        /**
         * 根据前面的输入数据，进行UI逻辑处理
         * 如果在处理过程中出错，则返回false,否则返回true
         */
        private boolean dealData()
        {
                System.out.println("Begin LATrainerIndexADMoneyUI.dealData.........");
                return true;
        }


        /**
         * 准备往后层输出所需要的数据
         * 输出：如果准备数据时发生错误则返回false,否则返回true
         */
        private boolean prepareOutputData()
        {
            try
            {
                mInputData.clear();
                mInputData.add(this.mGlobalInput);
                mInputData.add(this.mWageNo);
                mInputData.add(this.mManageCom);
                mInputData.add(this.mLATrainerIndexSet);
            }
            catch (Exception ex)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAAddBSubPerUI";
                tError.functionName = "prepareData";
                tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
                this.mErrors.addOneError(tError);
                return false;
            }
            return true;
        }
}
