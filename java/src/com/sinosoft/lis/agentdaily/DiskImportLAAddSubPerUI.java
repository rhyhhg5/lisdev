package com.sinosoft.lis.agentdaily;
import com.sinosoft.lis.agentconfig.DiskImportRateCommisionUI;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class DiskImportLAAddSubPerUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private DiskImportLAAddSubPerBL mDiskImportLAAddSubPerBL = null;

    public DiskImportLAAddSubPerUI()
    {
    	mDiskImportLAAddSubPerBL = new DiskImportLAAddSubPerBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mDiskImportLAAddSubPerBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mDiskImportLAAddSubPerBL.mErrors);
            return false;
        }
        //
        if(mDiskImportLAAddSubPerBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mDiskImportLAAddSubPerBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mDiskImportLAAddSubPerBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mDiskImportLAAddSubPerBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
        DiskImportRateCommisionUI zDiskImportRateCommisionUI = new DiskImportRateCommisionUI();
    }
}
