/*
 * <p>ClassName: ALAAscriptionEnsureBLS </p>
 * <p>Description: LAAscriptionBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentdaily;


import java.sql.Connection;

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LAAscriptionDB;
import com.sinosoft.lis.schema.LAAscriptionSchema;
import com.sinosoft.lis.db.LCContGetPolDB;

public class ALAAscriptionEnsureBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    public ALAAscriptionEnsureBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        System.out.println("Start ALAAscriptionEnsureBLS Submit...");
        if (this.mOperate.equals("INSERT||MAIN"))
        {
          //  tReturn = saveLAAscription(cInputData);
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
          //  tReturn = deleteLAAscription(cInputData);
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            tReturn = updateLAAscription(cInputData);
        }
        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End ALAAscriptionEnsureBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean updateLAAscription(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "ALAAscriptionEnsureBLS";
            tError.functionName = "updateLAAscription";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAAscriptionDBSet tLAAscriptionDBSet = new LAAscriptionDBSet(conn);
            LAAscriptionSet tLAAscriptionSet = (LAAscriptionSet) mInputData.
                                               getObjectByObjectName(
                    "LAAscriptionSet", 0);
           tLAAscriptionDBSet.set(tLAAscriptionSet);

            if (!tLAAscriptionDBSet.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAscriptionDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAAscriptionEnsureBLS";
                tError.functionName = "updateLAAscription";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LCContDBSet tLCContDBSet = new LCContDBSet(conn);
            LCContSet tLCContSet = (LCContSet) mInputData.
                                                   getObjectByObjectName(
                          "LCContSet", 0);
            tLCContDBSet.set(tLCContSet);
            if (!tLCContDBSet.update())
          {
              // @@错误处理
              this.mErrors.copyAllErrors(tLCContDBSet.mErrors);
              CError tError = new CError();
              tError.moduleName = "ALAAscriptionEnsureBLS";
              tError.functionName = "updateLAAscription";
              tError.errorMessage = "数据保存失败!";
              this.mErrors.addOneError(tError);
              conn.rollback();
              conn.close();
              return false;
          }
          LCPolDBSet tLCPolDBSet = new LCPolDBSet(conn);
          LCPolSet tLCPolSet = (LCPolSet) mInputData.
                                                 getObjectByObjectName(
                        "LCPolSet", 0);
          tLCPolDBSet.set(tLCPolSet);
          if (!tLCPolDBSet.update())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCPolDBSet.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAAscriptionEnsureBLS";
            tError.functionName = "updateLAAscription";
            tError.errorMessage = "数据保存失败!";
            this.mErrors.addOneError(tError);
            conn.rollback();
            conn.close();
            return false;
          }
          LJSPayPersonDBSet tLJSPayPersonDBSet = new LJSPayPersonDBSet(conn);
          LJSPayPersonSet tLJSPayPersonSet = (LJSPayPersonSet) mInputData.
                                                 getObjectByObjectName(
                        "LJSPayPersonSet", 0);
          tLJSPayPersonDBSet.set(tLJSPayPersonSet);
          if (!tLJSPayPersonDBSet.update())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLJSPayPersonDBSet.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAAscriptionEnsureBLS";
            tError.functionName = "updateLAAscription";
            tError.errorMessage = "数据保存失败!";
            this.mErrors.addOneError(tError);
            conn.rollback();
            conn.close();
            return false;
          }
          LJAPayPersonDBSet tLJAPayPersonDBSet = new LJAPayPersonDBSet(conn);
          LJAPayPersonSet tLJAPayPersonSet = (LJAPayPersonSet) mInputData.
                                                 getObjectByObjectName(
                        "LJAPayPersonSet", 0);
          tLJAPayPersonDBSet.set(tLJAPayPersonSet);
          if (!tLJAPayPersonDBSet.update())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLJAPayPersonDBSet.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAAscriptionEnsureBLS";
            tError.functionName = "updateLAAscription";
            tError.errorMessage = "数据保存失败!";
            this.mErrors.addOneError(tError);
            conn.rollback();
            conn.close();
            return false;
          }

          LACommisionDBSet tLACommisionDBSet = new LACommisionDBSet(conn);
          LACommisionSet tLACommisionSet = (LACommisionSet) mInputData.
                                                           getObjectByObjectName(
                                  "LACommisionSet", 0);
                    tLACommisionDBSet.set(tLACommisionSet);
                    if (!tLACommisionDBSet.update())
                  {
                      // @@错误处理
                      this.mErrors.copyAllErrors(tLACommisionDBSet.mErrors);
                      CError tError = new CError();
                      tError.moduleName = "ALAAscriptionEnsureBLS";
                      tError.functionName = "updateLAAscription";
                      tError.errorMessage = "数据保存失败!";
                      this.mErrors.addOneError(tError);
                      conn.rollback();
                      conn.close();
                      return false;
          }

          LJSPayDBSet tLJSPayDBSet = new LJSPayDBSet(conn);
          LJSPaySet tLJSPaySet = (LJSPaySet) mInputData.
                                                 getObjectByObjectName(
                        "LJSPaySet", 0);
          tLJSPayDBSet.set(tLJSPaySet);
          if (!tLJSPayDBSet.update())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLJSPayPersonDBSet.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAAscriptionEnsureBLS";
            tError.functionName = "updateLAAscription";
            tError.errorMessage = "数据保存失败!";
            this.mErrors.addOneError(tError);
            conn.rollback();
            conn.close();
            return false;
          }
          LPPolDBSet  tLPPolDBSet = new LPPolDBSet();
          LPPolSet tLPPolSet = (LPPolSet)mInputData.getObjectByObjectName("LPPolSet", 0);
          tLPPolDBSet.set(tLPPolSet);
          if(!tLPPolDBSet.update())
          {
              // @@错误处理
              this.mErrors.copyAllErrors(tLPPolDBSet.mErrors);
              CError tError = new CError();
              tError.moduleName = "ALAAscriptionEnsureBLS";
              tError.functionName = "updateLAAscription";
              tError.errorMessage = "数据保存失败!";
              this.mErrors.addOneError(tError);
              conn.rollback();
              conn.close();
              return false;
          }
          LPContDBSet tLPContDBSet = new LPContDBSet();
          LPContSet tLPContSet = (LPContSet)mInputData.getObjectByObjectName("LPContSet", 0);
          tLPContDBSet.set(tLPContSet);
          if(!tLPContDBSet.update())
          {
              // @@错误处理
              this.mErrors.copyAllErrors(tLPContDBSet.mErrors);
              CError tError = new CError();
              tError.moduleName = "ALAAscriptionEnsureBLS";
              tError.functionName = "updateLAAscription";
              tError.errorMessage = "数据保存失败!";
              this.mErrors.addOneError(tError);
              conn.rollback();
              conn.close();
              return false;
          }

          LBPolDBSet  tLBPolDBSet = new LBPolDBSet();
          LBPolSet tLBPolSet = (LBPolSet)mInputData.getObjectByObjectName("LBPPolSet", 0);
          tLBPolDBSet.set(tLBPolSet);
          if(!tLBPolDBSet.update())
          {
              // @@错误处理
              this.mErrors.copyAllErrors(tLPPolDBSet.mErrors);
              CError tError = new CError();
              tError.moduleName = "ALAAscriptionEnsureBLS";
              tError.functionName = "updateLAAscription";
              tError.errorMessage = "数据保存失败!";
              this.mErrors.addOneError(tError);
              conn.rollback();
              conn.close();
              return false;
          }
          LBContDBSet tLBContDBSet = new LBContDBSet();
          LBContSet tLBContSet = (LBContSet)mInputData.getObjectByObjectName("LBContSet", 0);
          tLBContDBSet.set(tLBContSet);
          if(!tLBContDBSet.update())
          {
              // @@错误处理
              this.mErrors.copyAllErrors(tLPContDBSet.mErrors);
              CError tError = new CError();
              tError.moduleName = "ALAAscriptionEnsureBLS";
              tError.functionName = "updateLAAscription";
              tError.errorMessage = "数据保存失败!";
              this.mErrors.addOneError(tError);
              conn.rollback();
              conn.close();
              return false;
          }

          LCPENoticeDBSet tLCPENoticeDBSet = new LCPENoticeDBSet(conn);
          LCPENoticeSet tLCPENoticeSet = (LCPENoticeSet) mInputData.
                                                 getObjectByObjectName(
                        "LCPENoticeSet", 0);
          tLCPENoticeDBSet.set(tLCPENoticeSet);
          if (!tLCPENoticeDBSet.update())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCPENoticeDBSet.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAAscriptionEnsureBLS";
            tError.functionName = "updateLAAscription";
            tError.errorMessage = "数据保存失败!";
            this.mErrors.addOneError(tError);
            conn.rollback();
            conn.close();
            return false;
          }
          LCContGetPolDBSet tLCContGetPolDBSet = new LCContGetPolDBSet();
          LCContGetPolSet tLCContGetPolSet = (LCContGetPolSet) mInputData.
                                                 getObjectByObjectName(
                        "LCContGetPolSet", 0);
          tLCContGetPolDBSet.set(tLCContGetPolSet);
          if (!tLCContGetPolDBSet.update())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCContGetPolDBSet.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAAscriptionEnsureBLS";
            tError.functionName = "updateLAAscription";
            tError.errorMessage = "数据保存失败!";
            this.mErrors.addOneError(tError);
            conn.rollback();
            conn.close();
            return false;
          }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAscriptionEnsureBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }


        return tReturn;
    }

}
