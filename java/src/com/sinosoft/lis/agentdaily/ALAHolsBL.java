/*
 * <p>ClassName: ALAHolsBL </p>
 * <p>Description: ALAHolsBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.db.LAHolsDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAHolsSchema;
import com.sinosoft.lis.vschema.LAHolsSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class ALAHolsBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAHolsSchema mLAHolsSchema = new LAHolsSchema();
    private LAHolsSet mLAHolsSet = new LAHolsSet();
    public ALAHolsBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAHolsBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALAHolsBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start ALAHolsBL Submit...");
            ALAHolsBLS tALAHolsBLS = new ALAHolsBLS();
            tALAHolsBLS.submitData(mInputData, cOperate);
            System.out.println("End ALAHolsBL Submit...");
            //如果有需要处理的错误，则返回
            if (tALAHolsBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tALAHolsBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAHolsBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        if (mOperate.equals("INSERT||MAIN"))
        {
            //确定记录顺序号
            String tAgentCode = this.mLAHolsSchema.getAgentCode().trim();
            System.out.println(tAgentCode);
            String tCount;
            int i;

            String tSQL = "select MAX(Idx) from LAHols where AgentCode = '"
                          + tAgentCode + "'";
            ExeSQL tExeSQL = new ExeSQL();
            tCount = tExeSQL.getOneValue(tSQL);
            if (tCount != null && !tCount.equals(""))
            {

                Integer tInteger = new Integer(tCount);
                i = tInteger.intValue();
                i = i + 1;
            }
            else
            {
                i = 1;
            }
            mLAHolsSchema.setIdx(i);
            this.mLAHolsSchema.setLeaveState("0");
            this.mLAHolsSchema.setMakeDate(currentDate);
            this.mLAHolsSchema.setMakeTime(currentTime);
            this.mLAHolsSchema.setModifyDate(currentDate);
            this.mLAHolsSchema.setModifyTime(currentTime);
            mLAHolsSchema.setOperator(mGlobalInput.Operator);
        }
        if (mOperate.equals("UPDATE||MAIN"))
        {
            LAHolsDB tLAHolsDB = new LAHolsDB();
            tLAHolsDB.setAgentCode(mLAHolsSchema.getAgentCode());
            tLAHolsDB.setIdx(mLAHolsSchema.getIdx());
            if (!tLAHolsDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAHolsBL";
                tError.functionName = "dealData";
                tError.errorMessage = "原请假信息查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mLAHolsSchema.setMakeDate(tLAHolsDB.getMakeDate());
            this.mLAHolsSchema.setMakeTime(tLAHolsDB.getMakeTime());
            this.mLAHolsSchema.setModifyDate(currentDate);
            this.mLAHolsSchema.setModifyTime(currentTime);
            this.mLAHolsSchema.setOperator(mGlobalInput.Operator);
        }
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAHolsSchema.setSchema((LAHolsSchema) cInputData.
                                     getObjectByObjectName("LAHolsSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start ALAHolsBLQuery Submit...");
        LAHolsDB tLAHolsDB = new LAHolsDB();
        tLAHolsDB.setSchema(this.mLAHolsSchema);
        this.mLAHolsSet = tLAHolsDB.query();
        this.mResult.add(this.mLAHolsSet);
        System.out.println("End ALAHolsBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLAHolsDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAHolsDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAHolsBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLAHolsSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAHolsBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
