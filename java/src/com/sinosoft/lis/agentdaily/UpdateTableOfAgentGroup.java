package com.sinosoft.lis.agentdaily;

import java.sql.Connection;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;

/**
 * 当机构调整后，更新所有相关业务表中的AgentGroup
 * 更新调整日期的记录，对于LACommision表中更新回单日期为空以及在调整日期后回单的记录
 * @version 1.0
 */

public class UpdateTableOfAgentGroup
{
    //错误类处理
    public CErrors mErrors = new CErrors();

    //传入数据
    private String mAgentCode = "";
    private String mOldAgentGroup = "";
    private String mNewAgentGroup = "";
    private String mNewBranchAttr = "";
    private String mAdjustDate = "";
    private String mEndDate = "";
    private String mFlag = ""; //0:只更新扎帐表中的BranchAttr 1:更新数组中的所有表
    private Connection mConn = null;

//    private String[] mUpdateTable = {"LACommision","LAHols","LAIndexInfo",
//        "LAIndexInfoTemp","LAPresence","LAQualityAssess","LARewardPunish",
//        "LATrain","LBCont","LBGrpPol","LBPol","LCCont","LCGrpPol","LCPol",
//        "LCUWMaster","LJABonusGet","LJAGet","LJAGetClaim","LJAGetDraw",
//        "LJAGetEndorse","LJAGetOther","LJAGetTempFee","LJAPay","LJAPayCont",
//        "LJAPayGrp","LJAPayPerson","LJFIGet","LJSGet","LJSGetClaim","LJSGetDraw",
//        "LJSGetEndorse","LJSGetOther","LJSGetTempFee","LJSPay","LJSPayCont",
//        "LJSPayGrp","LJSPayPerson","LJTempFee","LPCont","LPGrpPol","LPPol",
//        "LPUWMaster"};
    private String[] mUpdateTable =
            {
            "LACommision", "LCCont", "LCGrpPol",
            "LCPol"};

    public UpdateTableOfAgentGroup()
    {
    }

    public static void main(String[] args)
    {
    }

    public void setAgentCode(String cAgentCode)
    {
        this.mAgentCode = cAgentCode;
    }

    public void setOldAgentGroup(String cOldAgentGroup)
    {
        this.mOldAgentGroup = cOldAgentGroup;
    }

    public void setNewAgentGroup(String cNewAgentGroup)
    {
        this.mNewAgentGroup = cNewAgentGroup;
    }

    public void setNewBranchAttr(String cNewBranchAttr)
    {
        this.mNewBranchAttr = cNewBranchAttr;
    }

    public void setAdjustDate(String cAdjustDate)
    {
        this.mAdjustDate = cAdjustDate;
    }

    public void setEndDate(String cEndDate)
    {
        this.mEndDate = cEndDate;
    }

    public void setFlag(String cFlag)
    {
        this.mFlag = cFlag;
    }

    public boolean submitData(Connection conn)
    {
        this.mConn = conn;

        if (!getInputData())
        {
            return false;
        }

        //更新所有表的组别
        if (!updateAgentGroup())
        {
            return false;
        }

        return true;
    }

    public boolean getInputData()
    {
//        this.mOldAgentGroup = (String)this.mInputData.getObject(0);
//        this.mNewAgentGroup = (String)this.mInputData.getObject(1);
//        this.mNewBranchAttr = (String)this.mInputData.getObject(2);
//        this.mAdjustDate = (String)this.mInputData.getObject(3);
        if (this.mOldAgentGroup == null || this.mOldAgentGroup.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "UpdateTableOfAgentGroup";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入原组别参数OldAgentGroup不能为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (this.mNewAgentGroup == null || this.mNewAgentGroup.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "UpdateTableOfAgentGroup";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入新组别参数NewAgentGroup不能为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (this.mNewBranchAttr == null || this.mNewBranchAttr.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "UpdateTableOfAgentGroup";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入BranchAttr参数不能为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (this.mAdjustDate == null || this.mAdjustDate.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "UpdateTableOfAgentGroup";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入调整日期参数不能为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mConn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "UpdateTableOfAgentGroup";
            tError.functionName = "getInputData";
            tError.errorMessage = "数据库连接为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean updateAgentGroup()
    {
        ExeSQL tExeSQL;
        StringBuffer tSql = new StringBuffer("");
        String tTableName = "";
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        if (this.mFlag.equals("0"))
        {
            tSql.append("UPDATE LACommision SET ");
            tSql.append("BranchAttr = '" + this.mNewBranchAttr + "',");
            tSql.append("ModifyDate = '" + currentDate + "',");
            tSql.append("ModifyTime = '" + currentTime + "' ");
            if (mEndDate == null || mEndDate.equals(""))
            {
                tSql.append("WHERE (CalDate IS NULL  OR CalDate >= '" +
                            this.mAdjustDate + "') ");
            }
            else
            {
                tSql.append("WHERE CalDate >= '" + this.mAdjustDate +
                            "') And CalDate <= '" + mEndDate + "' ");
            }

            if (mAgentCode != null && !mAgentCode.equals(""))
            {
                tSql.append("And AgentCode = '" + mAgentCode + "' ");
            }
            tSql.append("AND BranchCode = '" + this.mOldAgentGroup + "' ");
            System.out.println("SQL:" + tSql.toString());
            tExeSQL = new ExeSQL(this.mConn);
            if (!tExeSQL.execUpdateSQL(tSql.toString()))
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "UpdateTableOfAgentGroup";
                tError.functionName = "updateAgentGroup";
                tError.errorMessage = "执行ExeSQL只更新LACommision表的BranchAttr出错！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        //更新某个代理人跨两个不同的组的情况 （一般为经过初始代理人信息维护的修改组别的情况）
        else if (this.mFlag.equals("1"))
        {
            int tLength = this.mUpdateTable.length;
            for (int i = 0; i < tLength; i++)
            {
                tSql = new StringBuffer("");
                tTableName = this.mUpdateTable[i];
                if (tTableName.equals("LACommision"))
                {
                    tSql.append("UPDATE LACommision SET BranchCode = '" +
                                this.mNewAgentGroup + "',");
                    tSql.append(
                            "AgentGroup = (Select AgentGroup From LAAgent Where AgentCode = '" +
                            mAgentCode + "'),");
                    tSql.append("BranchAttr = '" + this.mNewBranchAttr + "',");
                    tSql.append("ModifyDate = '" + currentDate + "',");
                    tSql.append("ModifyTime = '" + currentTime + "' ");
                    if (mEndDate == null || mEndDate.equals(""))
                    {
                        tSql.append("WHERE (CalDate IS NULL  OR CalDate >= '" +
                                    this.mAdjustDate + "') ");
                    }
                    else
                    {
                        tSql.append("WHERE CalDate >= '" + this.mAdjustDate +
                                    "') And CalDate <= '" + mEndDate + "' ");
                    }
                    if (mAgentCode != null && !mAgentCode.equals(""))
                    {
                        tSql.append("And AgentCode = '" + mAgentCode + "' ");
                    }
                    tSql.append("AND BranchCode = '" + this.mOldAgentGroup +
                                "' ");
                }
                else
                {
                    tSql.append("UPDATE " + tTableName + " SET AgentGroup = '" +
                                this.mNewAgentGroup + "',");
                    tSql.append("ModifyDate = '" + currentDate + "',");
                    tSql.append("ModifyTime = '" + currentTime + "' ");
                    if (mEndDate == null || mEndDate.equals(""))
                    {
                        tSql.append("WHERE MakeDate >= '" + this.mAdjustDate +
                                    "' ");
                    }
                    else
                    {
                        tSql.append("WHERE MakeDate >= '" + this.mAdjustDate +
                                    "' And Makedate <= '" + mEndDate + "' ");
                    }
                    if (mAgentCode != null && !mAgentCode.equals(""))
                    {
                        tSql.append("And AgentCode = '" + mAgentCode + "' ");
                    }
                    tSql.append("AND AgentGroup = '" + this.mOldAgentGroup +
                                "' ");
                }
                System.out.println("SQL:" + tSql.toString());
                tExeSQL = new ExeSQL(this.mConn);
                if (!tExeSQL.execUpdateSQL(tSql.toString()))
                {
                    this.mErrors.copyAllErrors(tExeSQL.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "UpdateTableOfAgentGroup";
                    tError.functionName = "updateAgentGroup";
                    tError.errorMessage = "执行ExeSQL更新表" + tTableName + "的组别出错！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
        }
        return true;
    }
}
