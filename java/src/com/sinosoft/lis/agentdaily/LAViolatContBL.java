/*
 * <p>ClassName: AbsenceBL </p>
 * <p>Description: AbsenceBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-07-08
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import java.math.BigInteger;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.MMap;

public class LAViolatContBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mViolatNo="";
    private MMap mMap = new MMap();
    /** 业务处理相关变量 */
    private LAViolatContSchema mLAViolatContSchema = new LAViolatContSchema();
    private LAViolatContSet mLAViolatContSet = new LAViolatContSet();
    public LAViolatContBL()
    {
    }

    public static void main(String[] args)
    {
        LAViolatContBL BL = new LAViolatContBL();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.ComCode = "86";
        tGlobalInput.Operator = "mak001";
        VData tVData = new VData();
        LAViolatContSchema tLAViolatContSchema=new LAViolatContSchema();
        tLAViolatContSchema.setBranchType("1");
        tLAViolatContSchema.setViolatDate("2005-05-01");
        tLAViolatContSchema.setViolatDate("2005-05-01");
        tLAViolatContSchema.setViolatNo("1");
        tVData.add(tLAViolatContSchema);
        tVData.add(tGlobalInput);
        BL.submitData(tVData, "INSERT||MAIN");
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!check())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAContBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAContBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("Start LAViolatContBL Submit...");
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start LARateStandPremBL Submit...");
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LARateStandPremBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        System.out.println("123233");
        return true;
    }


    private String createViolatNo()
    {
        String tViolatNo="";
        tViolatNo=com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo("LAViolatContNo",20);
        return tViolatNo;
    }

    private boolean check()
    {

        return true;
    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        if (mOperate.equals("INSERT||MAIN"))
        {

            String sql="select * from LAViolatCont  where contno='"+
                   mLAViolatContSchema.getContNo()+"'  and prtNo='"+
                   mLAViolatContSchema.getPrtNo()+"' ";
            LAViolatContDB tLAViolatContDB=new LAViolatContDB();
            LAViolatContSet tLAViolatContSet=new LAViolatContSet();
            tLAViolatContSet=tLAViolatContDB.executeQuery(sql);
            if(tLAViolatContSet.size()>0)
            {
                CError tError = new CError();
                tError.moduleName = "LAViolatContBL";
                tError.functionName = "dealData";
                tError.errorMessage = "保险单号为"+mLAViolatContSchema.getContNo()+"投保单号为"+mLAViolatContSchema.getPrtNo()+"的信息已存在，不能再进行录入。";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mLAViolatContSchema.setMakeTime(currentTime);
            this.mLAViolatContSchema.setMakeDate(currentDate);
            this.mLAViolatContSchema.setModifyTime(currentTime);
            this.mLAViolatContSchema.setModifyDate(currentDate);
            this.mLAViolatContSchema.setOperator(mGlobalInput.Operator);

            mViolatNo =createViolatNo();
            if (mViolatNo==null || mViolatNo.equals("") )
            {
                CError tError = new CError();
                tError.moduleName = "LAContBL";
                tError.functionName = "dealData";
                tError.errorMessage = "生成新的案件号失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mLAViolatContSchema.setViolatNo(mViolatNo);
            mMap.put(this.mLAViolatContSchema, "INSERT");
        }
        if (mOperate.equals("UPDATE||MAIN"))
        {
            String tViolatNo=this.mLAViolatContSchema.getViolatNo();
            LAViolatContSchema tLAViolatContSchema=new LAViolatContSchema();
            LAViolatContDB tLAViolatContDB=new LAViolatContDB();
            tLAViolatContDB.setViolatNo(tViolatNo);
            if (!tLAViolatContDB.getInfo() )
            {

                this.mErrors .copyAllErrors(tLAViolatContDB.mErrors ) ;
                CError tError = new CError();
                tError.moduleName = "LAViolatConttBL";
                tError.functionName = "dealData";
                tError.errorMessage = "没有编码为"+tViolatNo+"的案件信息!";
                this.mErrors.addOneError(tError);
                return false;
            }
            tLAViolatContSchema=tLAViolatContDB.getSchema();
            mLAViolatContSchema.setMakeDate(tLAViolatContSchema.getMakeDate());
            mLAViolatContSchema.setMakeTime(tLAViolatContSchema.getMakeTime());
            mLAViolatContSchema.setModifyDate(currentDate);
            mLAViolatContSchema.setModifyTime(currentTime);
            mMap.put(this.mLAViolatContSchema, "UPDATE");

         }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAViolatContSchema.setSchema((LAViolatContSchema) cInputData.
                                     getObjectByObjectName(
                                             "LAViolatContSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));

        return true;
    }


    public VData getResult()
    {
        this.mResult .clear() ;
        this.mResult .add(mViolatNo) ;
        return mResult;
    }

    public String getViolatNo()
    {
        return mViolatNo;
    }


    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAViolatContBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


}
