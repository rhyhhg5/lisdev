/*
 * <p>ClassName: LABankContAgentBLS </p>
 * <p>Description: LABankContAgentBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentdaily;


import java.sql.Connection;

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.vdb.LAAscriptionDBSet;
import com.sinosoft.lis.vschema.LAAscriptionSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class LABankContAgentBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    public LABankContAgentBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        System.out.println("Start LABankContAgentBLS Submit...");
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            tReturn = saveLAAscription(cInputData);
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            //tReturn = deleteLAAscription(cInputData);
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
           // tReturn = updateLAAscription(cInputData);
        }
        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End LABankContAgentBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean saveLAAscription(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABankContAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAAscriptionDBSet tLAAscriptionDBSet = new LAAscriptionDBSet(conn);
          //  LAAscriptionSet tLAAscriptionSet = new  LAAscriptionSet ();
            LAAscriptionSet tLAAscriptionSet = (LAAscriptionSet) mInputData.
                                                 getObjectByObjectName(
                      "LAAscriptionSet", 0);


          //  tLAAscriptionSet=(LAAscriptionSet) mInputData.get(1);
             tLAAscriptionDBSet.set(tLAAscriptionSet);
            if (!tLAAscriptionDBSet.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAscriptionDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "LABankContAgentBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
             LAAscriptionDBSet mLAAscriptionDBSet = new LAAscriptionDBSet(conn);
          //   LAAscriptionSet mLAAscriptionSet = new  LAAscriptionSet ();
         //    mLAAscriptionSet=(LAAscriptionSet) mInputData.get(2);
         LAAscriptionSet mLAAscriptionSet = (LAAscriptionSet) mInputData.
                                               getObjectByObjectName(
                    "LAAscriptionSet", 2);

               mLAAscriptionDBSet.set(mLAAscriptionSet);
             if(mLAAscriptionSet.size()>0)
             {
                 if (!mLAAscriptionDBSet.update()) {
                     // @@错误处理
                     this.mErrors.copyAllErrors(mLAAscriptionDBSet.mErrors);
                     CError tError = new CError();
                     tError.moduleName = "LABankContAgentBLS";
                     tError.functionName = "saveData";
                     tError.errorMessage = "数据保存失败!";
                     this.mErrors.addOneError(tError);
                     conn.rollback();
                     conn.close();
                     return false;
                 }
             }
             conn.commit();
           conn.close();
            //循环更新保单表中的代理人编码
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALABankContAgentBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    /**
     * 保存函数
     */
/*    private boolean deleteLAAscription(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALABankContAgentBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAAscriptionDB tLAAscriptionDB = new LAAscriptionDB(conn);
            if (!tLAAscriptionDB.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAscriptionDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALABankContAgentBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALABankContAgentBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }
 */
    /**
     * 保存函数
     */

   /*
    private boolean updateLAAscription(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "LABankContAgentBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAAscriptionDB tLAAscriptionDB = new LAAscriptionDB(conn);
            if (!tLAAscriptionDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAscriptionDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LABankContAgentBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABankContAgentBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }
   */
}
