package com.sinosoft.lis.agentdaily;


import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.agentdaily.AgentInfoDiskImporter;
import java.io.File;

/**
 * <p>Title: </p>
 *
 * <p>Description:</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author luomin
 * @version 1.1
 */
public class DiskImportAgentInfo2BL {
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private MMap mmap = new MMap();

    private int importPersons = 0; //记录导入成功的记录数


    private String diskimporttype = "";
    private String path = "";
    private String fileName = "";
    private String configName = "AgentInfoDiskImport2.xml";


    private LAAgentSet mLAAgentSet = new LAAgentSet();

    private LACertificationSet mLACertificationSet = null;
    private LAQualificationSet mLAQualificationSet = null;
    private LAAccountsSet mLAAccountsSet = null;



    public DiskImportAgentInfo2BL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        if (!getInputData(cInputData)) {
            return false;
        }
        mOperate = cOperate;
        String path = (String) mTransferData.getValueByName("path");
        String fileName = path + File.separator +
                          (String) mTransferData.getValueByName("fileName");

        String configFileName = path + File.separator + configName;

        //从磁盘导入数据
        AgentInfoDiskImporter importer = new AgentInfoDiskImporter(fileName,configFileName,diskimporttype);
        importer.setTableName(diskimporttype);
        if (!importer.doImport()) {
            mErrors.copyAllErrors(importer.mErrrors);
            return false;
        }

        if (diskimporttype.equals("LAAccounts")) {
             mLAAccountsSet = (LAAccountsSet) importer
                                           .getSchemaSet();
        }
        
        //若被保人在保单中没有记录，则剔出
        if (!checkData()) {
            return false;
        }
        //数据准备操作
        if (mOperate.equals("INSERT")) {
            if (!prepareData()) {
                return false;
            }
            System.out.println("---End prepareData---");
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mResult, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            System.out.println("DiskImportAgentInfoBL数据提交失败");
            this.mErrors.addOneError("数据提交失败!");
            return false;
        }
        System.out.println("--- ->submitData--- 数据提交成功。");

        return true;
    }

    private boolean getInputData(VData cInputData) {
        mTransferData = (TransferData) cInputData.
                        getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.
                       getObjectByObjectName("GlobalInput", 0);

        if (mTransferData == null || mGlobalInput == null) {
            mErrors.addOneError("传入的的数据不完整: mTransferData == null "
                                + "|| mGlobalInput == null");
            return false;
        }

        diskimporttype = (String) mTransferData.getValueByName("diskimporttype");
        return true;
    }

    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkData() {

        //银行账号校验
        if (diskimporttype.equals("LAAccounts")) {
        	
            if (mLAAccountsSet.size() == 0) {
                mErrors.addOneError("导入存折信息数据不存在失败或业务员代码不存在！");
                return false;
            } else {
            	//System.out.println("..........mLAAccountsSet.size(final)"+mLAAccountsSet.get(mLAAccountsSet.size()).getAgentCode());
            	//System.out.println("..........mLAAccountsSet.size(0)"+mLAAccountsSet.get(0).getAgentCode());
            	LAAccountsSet tmLAAccountsSet = new LAAccountsSet();
            	LAAgentSet tLAAgentSet = new LAAgentSet();
            	for (int i = 1; i <= mLAAccountsSet.size(); i++) {
                    if (mLAAccountsSet.get(i).getAgentCode() == null ||
                        mLAAccountsSet.get(i).getAgentCode().equals("")) {
                        mErrors.addOneError("导入代理人代码不能为空！");
                        return false;
                    } else if (mLAAccountsSet.get(i).getAccount() == null ||
                               mLAAccountsSet.get(i).getAccount().equals(
                                       "")) {
                        mErrors.addOneError("导入存折帐号不能为空！");
                        return false;
                    } else if (mLAAccountsSet.get(i).getBank() == null ||
                               mLAAccountsSet.get(i).getBank().
                               equals(
                                       "")) {
                        mErrors.addOneError("导入开户银行名称不能为空！");
                        return false;
                    } else if (mLAAccountsSet.get(i).getOpenDate() == null ||
                               mLAAccountsSet.get(i).getOpenDate().
                               equals(
                                       "")) {
                        mErrors.addOneError("导入开户日期不能为空或不为有效日期格式！");
                        return false;
                    } else if (mLAAccountsSet.get(i).getAgentCode()!= null ||
                            !mLAAccountsSet.get(i).getAgentCode().equals("")) {
                     LAAccountsDB tmLAAccountsDB = new LAAccountsDB();
                     LAAgentDB tLAAgentDB = new  LAAgentDB();
                     //将集团工号转为内部编码
                     String tSQL1 ="select * from Laagent where groupagentcode ='"+mLAAccountsSet.get(i).getAgentCode()+"'";
                     tLAAgentSet = tLAAgentDB.executeQuery(tSQL1);
                     if(tLAAgentSet.size()>0){
                     	LAAgentSchema tLAAgentSchema = new LAAgentSchema();
                     	tLAAgentSchema = tLAAgentSet.get(1);
                     	tSQL1 = "select *  from LAAccounts where agentcode='"+tLAAgentSchema.getAgentCode()
                                 +"'  and state='0'";
                          tmLAAccountsSet = tmLAAccountsDB.executeQuery(tSQL1);
                          if (tmLAAccountsSet.size() != 0) {
                          mErrors.addOneError("第"+i+"行"+mLAAccountsSet.get(i).getAgentCode()+"导入业务员已经存在有效的帐户！");
                          return false;
                          }
                     	
                     }else{
                     	  mErrors.addOneError("第"+i+"行"+mLAAccountsSet.get(i).getAgentCode()+"导入业务员不存在代理人信息！");
                           return false;
                     }
                     
//                     else{
//                     	////add by wyd 添加银行行号的较验
//                     	tSQL = "select * from laagent where agentcode = '"+mLAAccountsSet.get(i).getAgentCode()+"'   ";
//                     	LAAgentDB tLAAgentDB = new LAAgentDB();
//                     	LDBankDB tLDBankDB = new LDBankDB();
//                     	LAAgentSet tLAAgentSet = new LAAgentSet();
//                     	LDBankSet tLDBankSet = new LDBankSet();
//                     	tLAAgentSet = tLAAgentDB.executeQuery(tSQL);
//                     	if(tLAAgentSet.size()>0){
//                     		LAAgentSchema tLAAgent = tLAAgentSet.get(1).getSchema();
//                     		String tBankSql = " select * from ldbank where comcode = '"+tLAAgent.getManageCom()+"' and bankcode ='"+mLAAccountsSet.get(i).getBank()+"'  ";
//                     		tLDBankSet = tLDBankDB.executeQuery(tBankSql);
//                     		if(tLDBankSet.size() == 0){
//                     			mErrors.addOneError("第"+i+"行开户银行行号与代理人应为同一个管理机构下！。");
//                                 return false;	
//                     		}
//                     		
//                     	}
//                     }
                 }
                    
                    else if (mLAAccountsSet.get(i).getOpenDate() != null ||
                            !mLAAccountsSet.get(i).getOpenDate().
                            equals(
                                    "")) {
                    	if(!PubFun.checkDateForm(mLAAccountsSet.get(i).getOpenDate())){
                    		 String tStrErr = "上传数据中报文发送日期格式与约定格式不符，请核查！";
                    		return false;
                    	}else{
                    		System.out.println("上传数据中报文发送日期格式与约定格式相符");
                    	}
                 }
                   
                    if(i<mLAAccountsSet.size()){
                    	for(int j=i+1;j<=mLAAccountsSet.size();j++){
                    		String agentcode1=mLAAccountsSet.get(i).getAgentCode();
                    		String agentcode2=mLAAccountsSet.get(j).getAgentCode();
                        	if(agentcode1.equals(agentcode2)){
                        		mErrors.addOneError("第"+i+"行代理人编码与第"
                        				+j+"行代理人编码重复！一个代理人只能有一个有效的帐号。");
                                return false;
                        	}
                        }
                    }
                    

                }
            }
        }

        checkAgent();

        return true;
    }

    /**
     * 检查每个客户是否是本公司的代理人，若不是则剔除
     * @param
     * @return boolean，成功校验 trun
     */

    private void checkAgent() {
        //是否出错标志
        boolean flag = false;
        StringBuffer errMsg = new StringBuffer();
        if (diskimporttype.equals("LAAccounts")) {
        	
            for (int i = 1; i <= mLAAccountsSet.size(); i++) {
                LAAgentDB tLAAgentDB = new LAAgentDB();
                LAAgentSet tLAAgentSet = new LAAgentSet();
                String tSQL ="select * from laagent where 1=1 and  groupagentcode ='"+mLAAccountsSet.get(i).getAgentCode()+"'";
                tLAAgentSet =tLAAgentDB.executeQuery(tSQL);
//                tLAAgentDB.setAgentCode(mLAAccountsSet.get(i).getAgentCode());
//                tLAAgentSet = tLAAgentDB.query();
                if (tLAAgentSet.size() == 0) {
                    errMsg.append("代理人编码" +
                                  mLAAccountsSet.get(i).getAgentCode() +
                                  ",");
                    mLAAccountsSet.removeRange(i, i);
                    i--;
                    flag = true;
                }
            }
            if (flag) {
                mErrors.addOneError(errMsg.toString() + "未录入系统;");
            }

        }

    }


    private boolean prepareData() {

        if (diskimporttype.equals("LAAccounts")) {
        	LAAgentSet tLAAgentSet = new LAAgentSet();
            importPersons = mLAAccountsSet.size();
            for (int i = 1; i <= mLAAccountsSet.size(); i++) {
            	//集团工号转为内部编码
            	String tsql ="select * from laagent where groupagentcode='"+ mLAAccountsSet.get(i).getAgentCode()+"'";
            	LAAgentSchema tLAAgentSchema = new LAAgentSchema();
            	LAAgentDB tLAAgentDB = new LAAgentDB(); 
            	tLAAgentSchema =tLAAgentDB.executeQuery(tsql).get(1); 
                mLAAccountsSet.get(i).setState("0");
                String sql = "select * from LAAccounts where agentcode='" +
                		tLAAgentSchema.getAgentCode() +
                             "' and Account <> '" +
                             mLAAccountsSet.get(i).getAccount() + "'";
                LAAccountsSet tLAAccountsSet = new LAAccountsSet();
                LAAccountsDB tLAAccountsDB = new LAAccountsDB();
                tLAAccountsSet = tLAAccountsDB.executeQuery(sql);
                if (tLAAccountsSet.size() > 0 ) {
                    tLAAccountsSet.get(1).setState("1");
                    tLAAccountsSet.get(1).setAgentCode(tLAAgentSchema.getAgentCode());
                    mmap.put(tLAAccountsSet.get(1), "DELETE&INSERT");
                }
                if(mLAAccountsSet.get(i).getDestoryDate()!=null && !mLAAccountsSet.get(i).getDestoryDate().equals("")) {
                	mLAAccountsSet.get(i).setState("0");
                	mLAAccountsSet.get(i).setAgentCode(tLAAgentSchema.getAgentCode());
                }
                mLAAccountsSet.get(i).setAgentCode(tLAAgentSchema.getAgentCode());
                mLAAccountsSet.get(i).setMakeDate(PubFun.getCurrentDate());
                mLAAccountsSet.get(i).setMakeTime(PubFun.getCurrentTime());
                mLAAccountsSet.get(i).setModifyDate(PubFun.getCurrentDate());
                mLAAccountsSet.get(i).setModifyTime(PubFun.getCurrentTime());
                mmap.put(mLAAccountsSet.get(i), "DELETE&INSERT");
            }
            this.mResult.add(mmap);

        }

        return true;

    }

    /**
     *成功导入的记录数
     * @return int
     */
    public int getImportPersons() {
        return importPersons;
    }


    public static void main(String[] args) {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        TransferData t = new TransferData();

        t.setNameAndValue("diskimporttype", "LACertification");
        t.setNameAndValue("path", "D:\\picch\\ui\\temp");
        t.setNameAndValue("fileName", "展业证清单.xls");

        VData v = new VData();
        v.add(g);
        v.add(t);

        DiskImportAgentInfo2BL d = new DiskImportAgentInfo2BL();
        d.submitData(v, "INSERT");

        if (d.mErrors.needDealError()) {
            System.out.println(d.mErrors.getErrContent());
        }
    }
    // 去除批量录入信息中全角空格
   private String getSwitchData(String temp)
   {
	   if(temp == null)
	   {
		   return null;
	   }
	   String Result = "";
	   String regStartSpace = "^[　 ]*";  
       String regEndSpace = "[　 ]*$";  
         
       // 连续两个 replaceAll   
       Result = temp.replaceAll(regStartSpace, "").replaceAll(regEndSpace, "").trim();
	   return Result;
   }
}
