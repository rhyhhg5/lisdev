/*
 * <p>ClassName: LAAddBSubPerBL </p>
 * <p>Description: LAAddBSubPerBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2006-07-13
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LARewardPunishDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LARewardPunishSchema;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LAWageDB;
import java.math.BigInteger;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;

public class LAActiveAddBSubPerBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private int mIdx;
    private MMap mMap=new MMap();
    //tBigInteger = tBigInteger.add(tOneBigInteger);
    /** 业务处理相关变量 */
    private LARewardPunishSchema mLARewardPunishSchema = new
            LARewardPunishSchema();
    private LARewardPunishSet mLARewardPunishSet = new LARewardPunishSet();
    private LARewardPunishSet minLARewardPunishSet = new LARewardPunishSet();
    private LARewardPunishSet mupLARewardPunishSet = new LARewardPunishSet();
    private LARewardPunishSet mdeLARewardPunishSet = new LARewardPunishSet();
    private String mWageNo = "";
    private String mDonedate = "";
    private String donedate = "";
    private String mManageCom = "";
    public LAActiveAddBSubPerBL()
    {
    }

    public static void main(String[] args)
    {

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
         mIdx=getMaxNO();
        //循环处理每一条数据
        for(int i=1;i<=mLARewardPunishSet.size();i++)
        {
            mLARewardPunishSchema = new LARewardPunishSchema();
            System.out.println(mLARewardPunishSet.get(i).getAgentCode());
            mLARewardPunishSchema=mLARewardPunishSet.get(i);
            if (!check()) {
                return false;
            }
            //进行业务处理
            if (!dealData()) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAAddBSubPerBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败LAAddBSubPerBL-->dealData!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mIdx=mIdx+1;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("Start AgentWageCalSaveNewBL Submit...");
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentWageCalSaveNewBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }


        }
        mInputData = null;
        return true;
    }


    private boolean check() //已经计算过佣金的日期不能操作
    {
        mDonedate=mWageNo.substring(0,4)+"-"+mWageNo.substring(4,6)+"-01";
        donedate = AgentPubFun.formatDate(mDonedate, "yyyy-MM-dd");
        String day = donedate.substring(8); //从日期位取起
        /*if (!day.equals("01"))
        {
            CError tError = new CError();
            tError.moduleName = "LAAddBSubPerBL";
            tError.functionName = "check";
            tError.errorMessage = "执行日期必须从一号开始!";
            this.mErrors.addOneError(tError);
            return false;
        }*/

        String tAgentCode = this.mLARewardPunishSchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAddBSubPerBL";
            tError.functionName = "check";
            tError.errorMessage = "查询不到代理人" + tAgentCode + "的信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tAgentState = tLAAgentDB.getAgentState();
        if (tAgentState != null && tAgentState.compareTo("06") >= 0) //离职人员
        {
            String OutWorkDate = tLAAgentDB.getOutWorkDate();
            String yearmonth = AgentPubFun.formatDate(mDonedate, "yyyyMM");
            String sql =
                    "select startdate,enddate from lastatsegment where stattype='5' and yearmonth=" +
                    yearmonth ;
            ExeSQL aExeSQL = new ExeSQL();
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(sql);
            String startdate = aSSRS.GetText(1, 1);
            String enddate = aSSRS.GetText(1, 2);
            if (OutWorkDate.compareTo(startdate) < 0)
            {
                CError tError = new CError();
                tError.moduleName = "LAAddBSubPerBL";
                tError.functionName = "check";
                tError.errorMessage = "代理人" + tAgentCode + "在" + OutWorkDate +
                                      "已经离职，不能做" + yearmonth + "的加扣款!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        String sql = "select yearmonth from lastatsegment where stattype='1' and startdate<='" +
                     donedate+"'  and enddate>='"+donedate+"'  ";
        ExeSQL aExeSQL = new ExeSQL();
        String  tWageNO=aExeSQL.getOneValue(sql);
        String  tsql = "select * from lawage  where indexcalno='"+tWageNO+"' and managecom='"+
                       mManageCom+"' and BranchType='5' and BranchType2='01' fetch first 5 rows only";
        LAWageDB tLAWageDB = new LAWageDB();
        LAWageSet tLAWageSet = new LAWageSet();
        tLAWageSet=tLAWageDB.executeQuery(tsql);
        if(tLAWageSet.size()>0)
        {
            CError tError = new CError();
            tError.moduleName = "LAAddBSubPerBL";
            tError.functionName = "check";
            tError.errorMessage = "机构:"+mManageCom+"月份:"+tWageNO+"已经计算过佣金,不能进行下一步操作!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private int getMaxNO() //计算 idx
    {
        String tAgentCode = this.mLARewardPunishSet.get(1).getAgentCode().trim();
        int i;
        String tSQL =
                "select MAX(Idx) from LARewardPunish where AgentCode = '"
                + tAgentCode + "'";
        ExeSQL tExeSQL = new ExeSQL();
        String tCount = tExeSQL.getOneValue(tSQL);
        if (tCount != null && !tCount.equals(""))
        {

            Integer tInteger = new Integer(tCount);
            i = tInteger.intValue();
            i = i + 1;
        }
        else
        {
            i = 1;
        }
        return i;

    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();

        if (this.mOperate.equals("INSERT||MAIN"))
        {
            //确定记录顺序号
            String tAgentCode = this.mLARewardPunishSchema.getAgentCode().trim();
            String tCount;
            this.mLARewardPunishSchema.setIdx(mIdx);
            this.mLARewardPunishSchema.setDoneDate(donedate);
            this.mLARewardPunishSchema.setMakeDate(currentDate);
            this.mLARewardPunishSchema.setMakeTime(currentTime);
            this.mLARewardPunishSchema.setModifyDate(currentDate);
            this.mLARewardPunishSchema.setModifyTime(currentTime);
            minLARewardPunishSet.add(mLARewardPunishSchema);
        }
        else if (this.mOperate.equals("UPDATE||MAIN"))
        {


            LARewardPunishDB tLARewardPunishDB = new LARewardPunishDB();
            tLARewardPunishDB.setAgentCode(mLARewardPunishSchema.getAgentCode());
            tLARewardPunishDB.setIdx(mLARewardPunishSchema.getIdx());
            if (!tLARewardPunishDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ALARewardPunish";
                tError.functionName = "dealData";
                tError.errorMessage = "原加扣款信息查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
//            String doneDate = tLARewardPunishDB.getDoneDate();
//            String sql = "select yearmonth from lastatsegment where stattype='1' and startdate<='" +
//                         doneDate+"'  and enddate>='"+doneDate+"'  ";
//            ExeSQL aExeSQL = new ExeSQL();
//            String  tWageNO=aExeSQL.getOneValue(sql);
            String  tWageNO= tLARewardPunishDB.getWageNo();
            String  tsql = "select * from lawage  where indexcalno='"+tWageNO+"' and managecom='"+
                           mManageCom+"' and BranchType='5' and BranchType2='01' fetch first 5 rows only";
            LAWageDB tLAWageDB = new LAWageDB();
            LAWageSet tLAWageSet = new LAWageSet();
            tLAWageSet=tLAWageDB.executeQuery(tsql);
            if(tLAWageSet.size()>0)
            {
                CError tError = new CError();
                tError.moduleName = "LAAddBSubPerBL";
                tError.functionName = "check";
                tError.errorMessage = "此加扣款信息已经就算过佣金，不能修改 !";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mLARewardPunishSchema.setMakeDate(tLARewardPunishDB.
                    getMakeDate());
            this.mLARewardPunishSchema.setMakeTime(tLARewardPunishDB.
                    getMakeTime());
            this.mLARewardPunishSchema.setModifyDate(currentDate);
            this.mLARewardPunishSchema.setModifyTime(currentTime);
            this.mLARewardPunishSchema.setOperator(this.mGlobalInput.Operator);
            mupLARewardPunishSet.add(mLARewardPunishSchema);
        }
        else if (this.mOperate.equals("DELETE||MAIN"))
        {
            LARewardPunishDB tLARewardPunishDB = new LARewardPunishDB();
            tLARewardPunishDB.setAgentCode(mLARewardPunishSchema.getAgentCode());
            tLARewardPunishDB.setIdx(mLARewardPunishSchema.getIdx());
            if (!tLARewardPunishDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ALARewardPunish";
                tError.functionName = "dealData";
                tError.errorMessage = "原加扣款信息查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
//            String doneDate = tLARewardPunishDB.getDoneDate();
//            String sql = "select yearmonth from lastatsegment where stattype='1' and startdate<='" +
//                         doneDate+"'  and enddate>='"+doneDate+"'  ";
//            ExeSQL aExeSQL = new ExeSQL();
//            String  tWageNO=aExeSQL.getOneValue(sql);
            String  ttWageNO= tLARewardPunishDB.getWageNo();
            String  tsql = "select * from lawage  where indexcalno='"+ttWageNO+"' and managecom='"+
                           mManageCom+"' and BranchType='5' and BranchType2='01' fetch first 5 rows only";
            LAWageDB tLAWageDB = new LAWageDB();
            LAWageSet tLAWageSet = new LAWageSet();
            tLAWageSet=tLAWageDB.executeQuery(tsql);
            if(tLAWageSet.size()>0)
            {
                CError tError = new CError();
                tError.moduleName = "LAAddBSubPerBL";
                tError.functionName = "check";
                tError.errorMessage = "此加扣款信息已经就算过佣金，不能删除 !";
                this.mErrors.addOneError(tError);
                return false;
            }
            LARewardPunishSchema tLARewardPunishSchema = new LARewardPunishSchema();
            tLARewardPunishSchema=tLARewardPunishDB.getSchema();
            mdeLARewardPunishSet.add(tLARewardPunishSchema);
        }
        tReturn = true;
        return tReturn;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLARewardPunishSet.set((LARewardPunishSet) cInputData.
                                             getObjectByObjectName(
                "LARewardPunishSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mWageNo = (String) cInputData.get(1);
        this.mManageCom = (String) cInputData.get(2);
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start LAAddBSubPerBLQuery Submit...");
        LARewardPunishDB tLARewardPunishDB = new LARewardPunishDB();
        tLARewardPunishDB.setSchema(this.mLARewardPunishSchema);
        this.mLARewardPunishSet = tLARewardPunishDB.query();
        this.mResult.add(this.mLARewardPunishSet);
        System.out.println("End LAAddBSubPerBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLARewardPunishDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLARewardPunishDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAddBSubPerBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }


    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            mMap.put(this.minLARewardPunishSet, "INSERT");
            mMap.put(this.mupLARewardPunishSet, "UPDATE");
            mMap.put(this.mdeLARewardPunishSet, "DELETE");
            this.mInputData.add(this.mGlobalInput);

            this.mInputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAddBSubPerBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LAAddBSubPerBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}

