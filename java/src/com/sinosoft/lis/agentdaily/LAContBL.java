/*
 * <p>ClassName: AbsenceBL </p>
 * <p>Description: AbsenceBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-07-08
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAuthorizeSchema;
import com.sinosoft.lis.schema.LAContSchema;
import com.sinosoft.lis.schema.LARateChargeSchema;
import com.sinosoft.lis.vschema.LAAuthorizeSet;
import com.sinosoft.lis.vschema.LAContSet;
import com.sinosoft.lis.vschema.LARateChargeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import java.math.BigInteger;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.db.LARateChargeDB;

public class LAContBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mProtocalNo="";
    private String mContType="";
    /** 业务处理相关变量 */
    private LAContSchema mLAContSchema = new LAContSchema();
    private LAAuthorizeSet mLAAuthorizeSet = new LAAuthorizeSet();
    private LARateChargeSet mLARateChargeSet = new LARateChargeSet();
    private LARateChargeSet deleteLARateChargeSet = new LARateChargeSet();
    private LAAuthorizeSet deleteLAAuthorizeSet = new LAAuthorizeSet();
    public LAContBL()
    {
    }

    public static void main(String[] args)
    {
        LAContBL BL = new LAContBL();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.ComCode = "86";
        tGlobalInput.Operator = "mak001";
        VData tVData = new VData();
        LAAuthorizeSet tLAAuthorizeSet = new LAAuthorizeSet();
        LARateChargeSet tLARateChargeSet = new LARateChargeSet();
        LAContSchema tLAContSchema = new LAContSchema();
        tLAContSchema.setProtocolNo("PI110000000320050005");
        tLAAuthorizeSet.add(new LAAuthorizeSchema());
        tLAAuthorizeSet.add(new LAAuthorizeSchema());
        tLARateChargeSet.add(new LARateChargeSchema());
        tLARateChargeSet.add(new LARateChargeSchema());
        tVData.add(tLAAuthorizeSet);
        tVData.add(tLARateChargeSet);
        tVData.add(tLAContSchema);
        tVData.add(tGlobalInput);
        BL.submitData(tVData, "UPDATE||MAIN");
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!check())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAContBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAContBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("Start LAContBL Submit...");
        LAContBLS tLAContBLS = new LAContBLS();
        tLAContBLS.submitData(mInputData, cOperate);
        System.out.println("End LAContBL Submit...");
        //如果有需要处理的错误，则返回
        if (tLAContBLS.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAContBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAContBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 中介协议编码格式: 中介机构代码 + 年份后两位 + 二位流水号 共20位
     * @return String
     */
    private String createProtocalNo(String tAcType)
    {
        String protocolNo = "";
        if(tAcType.equals("01") )
        {
            protocolNo = this.mContType;

            String validDate = mLAContSchema.getStartDate();
            validDate = com.sinosoft.lis.pubfun.AgentPubFun.formatDate(
                    validDate,
                    "yyyy");
            //validDate = validDate.substring(2,4);
            String tAgentCom = mLAContSchema.getAgentCom();
            tAgentCom = tAgentCom.substring(0, 2);
            String tManageCom = mLAContSchema.getManageCom();
            tManageCom=tManageCom.substring(2,4);
            tAgentCom = tAgentCom+tManageCom+"0000";


            BigInteger tBigInteger = new BigInteger(tAgentCom.substring(2));
            BigInteger tOneBigInteger = new BigInteger("1");
            String sql =
                    "select Max(protocolno) from lacont where protocolno like '" +
                    tAgentCom + validDate + "%' ";
            ExeSQL tExeSQL = new ExeSQL();
            String maxProtocolNo = tExeSQL.getOneValue(sql);
            if (maxProtocolNo == null || maxProtocolNo.equals(""))
            {
                protocolNo = tAgentCom + validDate + "0001";
                System.out.println("protocolNo:{" + protocolNo + "}");
            }
            else
            {
                tBigInteger = new BigInteger(maxProtocolNo.substring(2));
                tBigInteger = tBigInteger.add(tOneBigInteger);
                protocolNo = tAgentCom.substring(0, 2) + tBigInteger.toString();
            }
        }
        else
        {
            protocolNo = this.mContType;

            String validDate = mLAContSchema.getStartDate();
            validDate = com.sinosoft.lis.pubfun.AgentPubFun.formatDate(
                    validDate,
                    "yyyy");
            //validDate = validDate.substring(2,4);
            String tAgentCom = mLAContSchema.getAgentCom();
            tAgentCom = tAgentCom.substring(0, tAgentCom.length() - 4);

            BigInteger tBigInteger = new BigInteger(tAgentCom.substring(2));
            BigInteger tOneBigInteger = new BigInteger("1");
            String sql =
                    "select Max(protocolno) from lacont where protocolno like '" +
                    tAgentCom + validDate + "%' ";
            ExeSQL tExeSQL = new ExeSQL();
            String maxProtocolNo = tExeSQL.getOneValue(sql);
            if (maxProtocolNo == null || maxProtocolNo.equals("")) {
                protocolNo = tAgentCom + validDate + "0001";
                System.out.println("protocolNo:{" + protocolNo + "}");
            } else {
                tBigInteger = new BigInteger(maxProtocolNo.substring(2));
                tBigInteger = tBigInteger.add(tOneBigInteger);
                protocolNo = tAgentCom.substring(0, 2) + tBigInteger.toString();
            }
        }
        return protocolNo;
    }

    private boolean check()
    {
        if (mOperate.equals("UPDATE||MAIN"))
        {
            if (mLAContSchema.getAgentCom() == null ||
                mLAContSchema.getAgentCom().equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "LAContBL";
                tError.functionName = "check";
                tError.errorMessage = "需要被修改的合同对应的中介机构代码不能为空!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (mLAContSchema.getProtocolNo() == null ||
                mLAContSchema.getProtocolNo().equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "LAContBL";
                tError.functionName = "check";
                tError.errorMessage = "需要被修改的合同对应的合同号码不能为空!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        String sql="select * from lacont where agentCom='"+mLAContSchema.getAgentCom()+"' and EndDate>='"+mLAContSchema.getStartDate() +"'";
        LAContDB tLAContDB=new LAContDB();
        LAContSet tLAContSet=new LAContSet();
        tLAContSet=tLAContDB.executeQuery(sql) ;
        if (tLAContSet!=null && tLAContSet.size() >0)
        {
            if (mOperate.equals("INSERT||MAIN") )
            {
                CError tError = new CError();
                tError.moduleName = "LAContBL";
                tError.functionName = "check";
                tError.errorMessage = "当前录入的合同起止期已经和上一份合同" +
                                      tLAContSet.get(1).getProtocolNo() +
                                      "的起止期冲突了，请调整起止期!";
                this.mErrors.addOneError(tError);
                return false;
            }
            else
            {
                if (mOperate.equals("UPDATE||MAIN") )
                {
                    if (!tLAContSet.get(1).getProtocolNo().equals(mLAContSchema.getProtocolNo() )  )
                    {
                        CError tError = new CError();
                        tError.moduleName = "LAContBL";
                        tError.functionName = "check";
                        tError.errorMessage = "当前录入的合同起止期已经和上一份合同" +
                                              tLAContSet.get(1).getProtocolNo() +
                                              "的起止期冲突了，请调整起止期!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
            }
        }
        return true;
    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        String tAgentCom=mLAContSchema.getAgentCom();
        LAComDB tLAComDB=new LAComDB();
        tLAComDB.setAgentCom(tAgentCom);
        if (!tLAComDB.getInfo() )
        {
            this.mErrors .copyAllErrors(tLAComDB.mErrors ) ;
            CError tError = new CError();
            tError.moduleName = "LAContBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有编码为"+tAgentCom+"的中介机构!";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tAcType=tLAComDB.getSchema().getACType();
        if (mOperate.equals("INSERT||MAIN"))
        {
         //   this.mLAContSchema.setSignDate(currentDate);
            this.mLAContSchema.setMakeTime(currentTime);
            this.mLAContSchema.setMakeDate(currentDate);
            this.mLAContSchema.setModifyTime(currentTime);
            this.mLAContSchema.setModifyDate(currentDate);
            this.mLAContSchema.setProtocolType("0");
            this.mLAContSchema.setOperator(mGlobalInput.Operator);
            mProtocalNo =createProtocalNo(tAcType);
System.out.println("mProtocalNo = ["+mProtocalNo+"]");
            if (mProtocalNo==null || mProtocalNo.equals("") )
            {
                CError tError = new CError();
                tError.moduleName = "LAContBL";
                tError.functionName = "dealData";
                tError.errorMessage = "生成新的合同号失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mLAContSchema.setProtocolNo(mProtocalNo);
            LARateChargeSet tLARateChargeSet = new LARateChargeSet();
            tLARateChargeSet.set(mLARateChargeSet);
            mLARateChargeSet.clear();
            for (int i = 1; i <= tLARateChargeSet.size(); i++)
            {
                LARateChargeSchema tLARateChargeSchema = new LARateChargeSchema();
                tLARateChargeSchema = tLARateChargeSet.get(i);
                tLARateChargeSchema.setOtherNo((mProtocalNo));
                tLARateChargeSchema.setOtherNoType("3");
                tLARateChargeSchema.setMakeTime(currentTime);
                tLARateChargeSchema.setMakeDate(currentDate);
                tLARateChargeSchema.setModifyTime(currentTime);
                tLARateChargeSchema.setModifyDate(currentDate);
                tLARateChargeSchema.setStartDate(mLAContSchema.getStartDate() ) ;
                tLARateChargeSchema.setEndDate(mLAContSchema.getEndDate() ) ;
                tLARateChargeSchema.setOperator(mGlobalInput.Operator);
                mLARateChargeSet.set(tLARateChargeSet);
            }

            LAAuthorizeSet tLAAuthorizeSet = new LAAuthorizeSet();
            tLAAuthorizeSet.set(mLAAuthorizeSet);
            mLAAuthorizeSet.clear();
            for (int i = 1; i <= tLAAuthorizeSet.size(); i++)
            {
                LAAuthorizeSchema tLAAuthorizeSchema = tLAAuthorizeSet.get(i);
                tLAAuthorizeSchema.setAuthorObj(mProtocalNo);
                if(tLAAuthorizeSchema.getRiskType() != null && 
                		!tLAAuthorizeSchema.getRiskType().equals("")&&
                		tLAAuthorizeSchema.getRiskType().equals("T")){                	
                	tLAAuthorizeSchema.setRiskType("T");	
                }
                else {
                String tRiskCode = tLAAuthorizeSchema.getRiskCode();
                String tSQL =
                        "select RiskType3 from LMRiskAPP where RiskCode= '"
                        + tRiskCode + "'";
                ExeSQL tExeSQL = new ExeSQL();
                String RiskType3 = tExeSQL.getOneValue(tSQL);                
                tLAAuthorizeSchema.setRiskType(RiskType3);
                }
                tLAAuthorizeSchema.setMakeDate(currentDate) ;
                tLAAuthorizeSchema.setMakeTime(currentTime);
                tLAAuthorizeSchema.setModifyDate(currentDate);
                tLAAuthorizeSchema.setModifyTime(currentTime);
                tLAAuthorizeSchema.setAuthorStartDate(mLAContSchema.getStartDate() );
                tLAAuthorizeSchema.setAuthorEndDate(mLAContSchema.getEndDate());
                tLAAuthorizeSchema.setOperator(mGlobalInput.Operator);
                mLAAuthorizeSet.add(tLAAuthorizeSchema);
            }
        }
        if (mOperate.equals("UPDATE||MAIN"))
        {
            LAContDB tLAContDB = new LAContDB();
            tLAContDB.setProtocolNo(mLAContSchema.getProtocolNo());
            if (!tLAContDB.getInfo()) {
                CError tError = new CError();
                tError.moduleName = "LAContBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "查询不到" + mLAContSchema.getProtocolNo() +
                                      "的机构信息!";
                this.mErrors.addOneError(tError);
                return false;

            }
            mProtocalNo = mLAContSchema.getProtocolNo();
            LAContSchema tLAContSchema=new LAContSchema();
            tLAContSchema=tLAContDB.getSchema();

           // this.mLAContSchema.setSignDate(tLAContSchema.getSignDate());
            this.mLAContSchema.setMakeTime(tLAContSchema.getMakeTime());
            this.mLAContSchema.setMakeDate(tLAContSchema.getMakeDate());
            this.mLAContSchema.setModifyTime(currentTime);
            this.mLAContSchema.setModifyDate(currentDate);
            this.mLAContSchema.setProtocolType("0");
            this.mLAContSchema.setOperator(mGlobalInput.Operator);

            LARateChargeSet tLARateChargeSet = new LARateChargeSet();
            tLARateChargeSet.set(mLARateChargeSet);
            mLARateChargeSet.clear();
            for (int i = 1; i <= tLARateChargeSet.size(); i++)
            {
                LARateChargeSchema tLARateChargeSchema = new LARateChargeSchema();
                tLARateChargeSchema = tLARateChargeSet.get(i);
                tLARateChargeSchema.setOtherNo(this.mLAContSchema.getProtocolNo());
                tLARateChargeSchema.setOtherNoType("3");
                tLARateChargeSchema.setMakeTime(currentTime);
                tLARateChargeSchema.setMakeDate(currentDate);
                tLARateChargeSchema.setModifyTime(currentTime);
                tLARateChargeSchema.setModifyDate(currentDate);
                tLARateChargeSchema.setStartDate(mLAContSchema.getStartDate() ) ;
                tLARateChargeSchema.setEndDate(mLAContSchema.getEndDate() ) ;
                tLARateChargeSchema.setOperator(mGlobalInput.Operator);
                mLARateChargeSet.set(tLARateChargeSet);
            }
            LAAuthorizeSet tLAAuthorizeSet = new LAAuthorizeSet();
           tLAAuthorizeSet.set(mLAAuthorizeSet);
           mLAAuthorizeSet.clear();
           for (int i = 1; i <= tLAAuthorizeSet.size(); i++)
           {
               LAAuthorizeSchema tLAAuthorizeSchema = tLAAuthorizeSet.get(i);
               tLAAuthorizeSchema.setAuthorObj(this.mLAContSchema.getProtocolNo());
               
               if(tLAAuthorizeSchema.getRiskType() != null && 
               		!tLAAuthorizeSchema.getRiskType().equals("")&&
            		tLAAuthorizeSchema.getRiskType().equals("T")){                	
               	tLAAuthorizeSchema.setRiskType("T");	
               }
               else {
               String tRiskCode = tLAAuthorizeSchema.getRiskCode();
               String tSQL =
                       "select RiskType3 from LMRiskAPP where RiskCode= '"
                       + tRiskCode + "'";
               ExeSQL tExeSQL = new ExeSQL();
               String RiskType3 = tExeSQL.getOneValue(tSQL);                
               tLAAuthorizeSchema.setRiskType(RiskType3);
               }
               tLAAuthorizeSchema.setMakeDate(currentDate) ;
               tLAAuthorizeSchema.setMakeTime(currentTime);
               tLAAuthorizeSchema.setModifyDate(currentDate);
               tLAAuthorizeSchema.setModifyTime(currentTime);
               tLAAuthorizeSchema.setAuthorStartDate(mLAContSchema.getStartDate() );
               tLAAuthorizeSchema.setAuthorEndDate(mLAContSchema.getEndDate());
               tLAAuthorizeSchema.setOperator(mGlobalInput.Operator);
               mLAAuthorizeSet.add(tLAAuthorizeSchema);
           }



         LARateChargeDB tLARateChargeDB = new LARateChargeDB();
         String sql="select * from laratecharge where OtherNo='"+mLAContSchema.getProtocolNo()+"'"
                  +"  and PayIntv=0 and StartYear=0 and EndYear=999 ";
         deleteLARateChargeSet=tLARateChargeDB.executeQuery(sql);

         LAAuthorizeDB tLAAuthorizeDB=new LAAuthorizeDB();
          String strsql="select * from LAAuthorize where AuthorObj='"+mLAContSchema.getProtocolNo()+"'"
                     +" and AuthorType in ('0','1','2')";
         deleteLAAuthorizeSet=tLAAuthorizeDB.executeQuery(strsql);
//       LARateChargeSet tLARateChargeSet = new LARateChargeSet();
//       LARateChargeSchema tLARateChargeSchema = new LARateChargeSchema();
//         if(tLARateChargeSet.size()!=0)
//         {
//         for(int i=1;i<=tLARateChargeSet.size();i++)
//         {
//             tLARateChargeSchema = tLARateChargeSet.get(i);
//
//         }

         }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAContSchema.setSchema((LAContSchema) cInputData.
                                     getObjectByObjectName(
                                             "LAContSchema", 0));
        this.mLARateChargeSet.set((LARateChargeSet) cInputData.
                                  getObjectByObjectName(
                                          "LARateChargeSet", 0));
        this.mLAAuthorizeSet.set((LAAuthorizeSet) cInputData.
                                 getObjectByObjectName(
                                         "LAAuthorizeSet", 0));
        for(int i = 1; i<=mLAAuthorizeSet.size();i++)
        {
            System.out.println("[["+mLAAuthorizeSet.get(i).getRiskCode()+"]]");
        }

        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mContType=(String)cInputData.getObjectByObjectName("String",0);

        return true;
    }


    public VData getResult()
    {
        this.mResult .clear() ;
        this.mResult .add(mProtocalNo) ;
        return mResult;
    }
    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    /* private boolean submitquery()
     {
         this.mResult.clear();
         System.out.println("Start AbsenceBLQuery Submit...");
         LAPresenceDB tLAPresenceDB = new LAPresenceDB();
         tLAContDB.setSchema(this.mLAPresenceSchema);
         this.mLAPresenceSet = tLAPresenceDB.query();
         this.mResult.add(this.mLAPresenceSet);
         System.out.println("End AbsenceBLQuery Submit...");
         //如果有需要处理的错误，则返回
         if (tLAPresenceDB.mErrors.needDealError())
         {
             // @@错误处理
             this.mErrors.copyAllErrors(tLAPresenceDB.mErrors);
             CError tError = new CError();
             tError.moduleName = "AbsenceBL";
             tError.functionName = "submitData";
             tError.errorMessage = "数据提交失败!";
             this.mErrors.addOneError(tError);
             return false;
         }
         mInputData = null;
         return true;
     }*/

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLAAuthorizeSet);
            this.mInputData.add(this.mLARateChargeSet);
            this.mInputData.add(this.mLAContSchema);
            this.mInputData.add(this.deleteLAAuthorizeSet);
            this.mInputData.add(this.deleteLARateChargeSet);
//            System.out.println(deleteLAAuthorizeSet.size());
//            System.out.println(deleteLAAuthorizeSet.get(1).getAuthorObj());
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAContBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


}
