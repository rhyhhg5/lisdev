package com.sinosoft.lis.agentdaily;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: </p>
 * @author Alse
 * @version 1.0
 */
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class LAAddSeManageUI
{

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();


    public LAAddSeManageUI()
    {
    }

    public boolean submitData(VData cInputData)
    {
        mInputData = (VData) cInputData.clone();
        LAAddSeManageBL tLAAddSeManageBL = new LAAddSeManageBL();
        if (!tLAAddSeManageBL.submitData(mInputData))
        {
            this.mErrors.copyAllErrors(tLAAddSeManageBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "tLAAddSeManageUI";
            tError.functionName = "submitData";
            tError.errorMessage = tLAAddSeManageBL.mErrors.getFirstError();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
