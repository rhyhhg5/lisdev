/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.vschema.LARewardPunishSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.lis.schema.LARewardPunishSchema;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.vschema.LAWageSet;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: </p>
 * @author Alse
 * @version 1.0
 */
public class LAAddSeManageBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();


    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mManageCom = "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String mIndexCalNo = "";
    private String mCheck1 = "";
    private String mFlag = "";
    private LARewardPunishSet mLARewardPunishSet = new LARewardPunishSet();
    private LARewardPunishSchema mLARewardPunishSchema= new LARewardPunishSchema();
    private LARewardPunishSet tLARewardPunishSet = new LARewardPunishSet();
    private LARewardPunishSchema tLARewardPunishSchema= new LARewardPunishSchema();
    private MMap mMap = new MMap();
    private VData mInputData= new VData();
    private String mOperate="";
    public LAAddSeManageBL()
    {
    }

    public static void main(String[] args)
    {

    }


    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData)
    {

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
//        if (!check())
//        {
//            return false;
//       }
        // 进行业务处理
        if (!dealData())
        {
            return false;
        }
        if (!prepareOutputData())
        {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start LAAddSeManageBL Submit...");
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAddSeManageBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData=null;

        return true;
    }
/**
    private boolean check()
    {
        if (this.mManageCom.length() != 8)
        {
            buildError("check", "机构代码长度必须为8位");
            return false;

        }
        else
        {
            String sql="select EndDate+5 days from LAStatSegment where CHAR(yearmonth)='" + mIndexCalNo +
            "' and stattype='5' ";
            ExeSQL tExeSQL = new ExeSQL();
            String tEndDate = tExeSQL.getOneValue(sql);
            System.out.println("not error2");
            String tSQL1 = "select * from lawagelog where managecom like '" +
                          mManageCom + "%' and (char(int(wageno)-1)='" + mIndexCalNo +
                          "' or (char(int(wageno)-89)='" + mIndexCalNo +
                          "'  and  char(int(wageyear)-1)='" +mIndexCalNo.substring(0,4)+
                          "'  and substr(wageno,5,6)='01'))  and branchtype='" +
                      mBranchType + "' and BranchType2='"+mBranchType2+
                      "'    ";
             System.out.println("not error1");
            LAWageLogSet tLAWageLogSet = new LAWageLogSet();
            LAWageLogDB tLAWageLogDB = new LAWageLogDB();
            tLAWageLogSet = tLAWageLogDB.executeQuery(tSQL1);
            System.out.println("not error3"+tSQL1);

            if (tLAWageLogDB.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tLAWageLogDB.mErrors);
                return false;
             }
             else if(tLAWageLogSet.size()<=0)
             {
                 buildError("dealData", "本月没有进行提数计算未完成"+tEndDate+"还未提数，请提数后进行佣金试算，再确认佣金！");
                  return false;
             }
             else
             {
                 System.out.println(tSQL1+tLAWageLogSet.get(1).getEndDate());
                 String mEndDate= tLAWageLogSet.get(1).getEndDate();
                 if (mEndDate.compareTo(tEndDate)<0)
                 {
                     buildError("dealData", "本月没有进行提数计算未完成"+tEndDate+"还未提数，请提数后进行佣金试算，再确认佣金！");
                     return false;
                 }

            }
            return true;
        }
    }
*/
    private boolean dealData()
    {
      if(mFlag==null || mFlag.equals(""))
      {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "LAAddSeManageBL";
              tError.functionName = "dealData";
              tError.errorMessage = "参数传递错误!";
              this.mErrors.addOneError(tError);
           return false;
      }
      boolean tReturn = true;
      int tCount = 0;

      for (int i = 1; i <= mLARewardPunishSet.size(); i++) {
            System.out.println("update LAWage");

            LARewardPunishDB tLARewardPunishDB = new LARewardPunishDB();
            LARewardPunishSchema tLARewardPunishSchema = new LARewardPunishSchema();
            tLARewardPunishSchema = mLARewardPunishSet.get(i);
            tLARewardPunishDB.setAgentCode(tLARewardPunishSchema.getAgentCode());
            tLARewardPunishDB.setIdx(tLARewardPunishSchema.getIdx());
            System.out.println(tLARewardPunishSchema.getAgentCode() + "/" +tLARewardPunishSchema.getIdx());
            //tLARewardPunishDB.setBranchType(mLARewardPunishSchema.getBranchType());
            //tLARewardPunishDB.setBranchType2(mLARewardPunishSchema.getBranchType2());
            // tLARateCommisionSchemaold = tLARateCommisionDB.query().get(1);
            tLARewardPunishSchema = tLARewardPunishDB.query().get(1);
            //在这里添加校验语句
            if ((tLARewardPunishSchema.getSendGrp().trim().equals("21")) ||
                (tLARewardPunishSchema.getSendGrp().trim().equals("22"))) {
                CError tError = new CError();
                tError.moduleName = "LAAddSeManageBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "销售员代码为：" + tLARewardPunishSchema.getAgentCode() +
                                      "的此加扣款项目已经审核完毕，不能再次审核";
                this.mErrors.addOneError(tError);
                return false;
            }
/**
            String tSql = "select max(indexcalno) from lawage where agentcode='" +
                          tLARewardPunishSchema.getAgentCode() + "' with ur";
            ExeSQL exeSQL = new ExeSQL();
            String strNo = exeSQL.getOneValue(tSql);
            if ((strNo != null) && (!strNo.trim().equals(""))) {
                {
                    String tindexcalno = AgentPubFun.formatDate(tLARewardPunishSchema.getDoneDate(), "yyyyMM");
                    if (tindexcalno.compareTo(strNo)<=0) {
                        CError tError = new CError();
                        tError.moduleName = "LAAddSeManageBL";
                        tError.functionName = "dealdata";
                        tError.errorMessage = "销售员代码为：" +
                                              tLARewardPunishSchema.getAgentCode() +
                                              "的薪资已经计算，不能进行审批";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                } */
       String doneDate=tLARewardPunishSchema.getDoneDate();
       String sql = "select yearmonth from lastatsegment where stattype='1' and startdate<='" +
                     doneDate+"'  and enddate>='"+doneDate+"'  ";
        ExeSQL aExeSQL = new ExeSQL();
        String  tWageNO=aExeSQL.getOneValue(sql);
        String  tsql = "select * from lawage  where indexcalno='"+tWageNO+"' and agentcode='"+
                       tLARewardPunishSchema.getAgentCode()+"' and BranchType='1' and BranchType2='01' fetch first 5 rows only";
        LAWageDB tLAWageDB = new LAWageDB();
        LAWageSet tLAWageSet = new LAWageSet();
        tLAWageSet=tLAWageDB.executeQuery(tsql);
        if(tLAWageSet.size()>0)
        {
            CError tError = new CError();
            tError.moduleName = "LAAddSubPerBL";
            tError.functionName = "check";
            tError.errorMessage = "人员:"+tLARewardPunishSchema.getAgentCode()+"月份:"+tWageNO+"已经计算过佣金,不能进行审批!";
            this.mErrors.addOneError(tError);
            return false;
        }
      if (mFlag.equals("Agree"))
      {
          tLARewardPunishSchema.setSendGrp("21"); //复审同意
      }
      else if(mFlag.equals("DisAgree"))
      {
          tLARewardPunishSchema.setSendGrp("22");//复审不同意
      }
      tLARewardPunishSchema.setCheck2(mCheck1);
      tLARewardPunishSchema.setOperator(mCheck1);
      tLARewardPunishSchema.setModifyDate(this.currentDate);
      tLARewardPunishSchema.setModifyTime(this.currentTime);
      tLARewardPunishSet.add(tLARewardPunishSchema);
      }

        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量


         this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                     getObjectByObjectName("GlobalInput", 0));
         this.mLARewardPunishSet.set((LARewardPunishSet) cInputData.
                                            getObjectByObjectName(
                 "LARewardPunishSet", 0));
         this.mFlag=(String)cInputData.getObjectByObjectName("String",0);
         this.mCheck1=mGlobalInput.Operator;
        return true;
    }
    // 个险佣金开始试算，插入state = '12'，表示正在试算，其他人不能再算
    private boolean prepareOutputData()
    {

        //mMap.put(this.mLAWageSet, "INSERT");
        mMap.put(this.tLARewardPunishSet, "UPDATE");
        mInputData.add(mMap);


        return true;

    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LAAddSeManageBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

}
