package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.DBConnPool;
import java.sql.Connection;

public class LADataGatherBL
{

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("Start LADataGatherBL submit...");

        //开始后台操作
        if(!dealData(cInputData,cOperate))
        {
            return false;
        }

        System.out.println("End LADataGatherBL submit...");

        return true;
    }

    /**
     * 进行后台数据处理
     * @param pmVData VData
     * @return boolean
     */
    private boolean dealData(VData pmVData,String pmOperate)
    {
        Connection conn = DBConnPool.getConnection();

        try
        {
            conn.setAutoCommit(false);
            //直接佣金明细备份表（佣金扎账表）
            System.out.println("进行数据备份！");
            LACommisionBDBSet tLACommisionBDBSe = new LACommisionBDBSet(conn);
            tLACommisionBDBSe.set((LACommisionBSet) pmVData.
                                  getObjectByObjectName(
                                          "LACommisionBSet", 0));
            if (!tLACommisionBDBSe.insert()) {
                conn.rollback();
                conn.close();
                System.out.println(tLACommisionBDBSe.mErrors.getFirstError());
                System.out.println("直接佣金明细备份表（佣金扎账表） 处理失败！");
                return false;
            }

            //直接佣金明细表（佣金扎账表）
            System.out.println("删除已经备份的正式数据！");
            LACommisionDBSet tLACommisionDBSet = new LACommisionDBSet(conn);
            tLACommisionDBSet.set((LACommisionSet) pmVData.
                                  getObjectByObjectName(
                                  "LACommisionSet", 0));
            if (!tLACommisionDBSet.delete()) {
                conn.rollback();
                conn.close();
                System.out.println(tLACommisionDBSet.mErrors.getFirstError());
                System.out.println("直接佣金明细表（佣金扎账表） 处理失败！");
                return false;
            }

            //回退操作跨月作如下操作
            if(pmOperate.equals("2"))
            {
                //佣金提数日志表
                System.out.println("删除需要回退掉的日志记录！");
                LAWageLogDBSet tLAWageLogDBSet = new LAWageLogDBSet(conn);
                tLAWageLogDBSet.set((LAWageLogSet) pmVData.
                                      getObjectByObjectName(
                                              "LAWageLogSet", 0));
                if (!tLAWageLogDBSet.delete()) {
                    conn.rollback();
                    conn.close();
                    System.out.println(tLAWageLogDBSet.mErrors.getFirstError());
                    System.out.println("佣金提数日志表 处理失败！");
                    return false;
                }

                //佣金计算历史
                System.out.println("删除需要回退掉的历史记录！");
                LAWageHistoryDBSet tLAWageHistoryDBSet = new LAWageHistoryDBSet(conn);
                tLAWageHistoryDBSet.set((LAWageHistorySet) pmVData.
                                      getObjectByObjectName(
                                              "LAWageHistorySet", 0));
                if (!tLAWageHistoryDBSet.delete()) {
                    conn.rollback();
                    conn.close();
                    System.out.println(tLAWageHistoryDBSet.mErrors.getFirstError());
                    System.out.println("佣金计算历史 处理失败！");
                    return false;
                }

            }

            //佣金提数日志表
            System.out.println("修改计算止日！");
            LAWageLogDBSet tLAWageLogDBSet = new LAWageLogDBSet(conn);
            LAWageLogSchema tLAWageLogSchema = ((LAWageLogSchema) pmVData.
                                  getObjectByObjectName(
                                  "LAWageLogSchema", 0));
            tLAWageLogDBSet.add(tLAWageLogSchema);

            if (!tLAWageLogDBSet.update()) {
                conn.rollback();
                conn.close();
                System.out.println(tLAWageLogDBSet.mErrors.getFirstError());
                System.out.println("佣金提数日志表 处理失败！");
                return false;
            }

            //回退轨迹表
            System.out.println("添加回退轨迹");
            LARollBackTraceDBSet tLARollBackTraceDBSet = new LARollBackTraceDBSet(conn);
            tLARollBackTraceDBSet.set((LARollBackTraceSet) pmVData.
                              getObjectByObjectName("LARollBackTraceSet", 0));
            if (!tLARollBackTraceDBSet.insert()) {
                conn.rollback();
                conn.close();
                System.out.println(tLARollBackTraceDBSet.mErrors.getFirstError());
                System.out.println("回退轨迹表 处理失败！");
                return false;
            }
            conn.commit();
            conn.close();
        }catch(Exception ex)
        {
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
            return false;
        }

        return true;
    }
}
