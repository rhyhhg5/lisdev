package com.sinosoft.lis.agentdaily;


import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.StrTool;
import java.text.DecimalFormat;

public class AgentCancelContRateBL{
      /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private VData mResult = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    private String mOperate="";
    private String mManageCom="";
    private String mAgentCom="";
    private String mWageNo="";
    private String mBranchType="";
    private String mBranchType2="";
    private String mAgentComName="";
    private String mAgentName="";
    private String mAgentCode="";

    private MMap mMap=new MMap();
    public static void main(String[] args)
    {
    }
    public AgentCancelContRateBL()
    {
    }

    private boolean getInputData(VData cInputData) {
        try {
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);

            if (mGlobalInput == null || mTransferData==null) {
                CError tError = new CError();
                tError.moduleName = "AgentGroupActiveRateBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有得到足够的信息！";
                this.mErrors.addOneError(tError);
                return false;
            }

            this.mManageCom = (String) mTransferData.getValueByName("ManageCom");
            this.mAgentCom = (String) mTransferData.getValueByName("AgentCom");
            this.mWageNo = (String) mTransferData.getValueByName("WageNo");
            this.mBranchType= (String) mTransferData.getValueByName("BranchType");
            this.mBranchType2 = (String) mTransferData.getValueByName("BranchType2");
            this.mAgentComName= (String) mTransferData.getValueByName("AgentComName");
            this.mAgentCode = (String) mTransferData.getValueByName("AgentCode");
            this.mAgentName= (String) mTransferData.getValueByName("AgentName");

            return true;
        }
        catch (Exception e) {
          // @@错误处理
          CError.buildErr(this, "接收数据失败");
          return false;
        }
    }
    public boolean checkData()
    {
        return true;
    }
    public boolean submitData(VData cInputData, String cOperate) {
        this.mOperate=cOperate;
        if(!getInputData(cInputData))
        {
            return false;
        }
        if(!checkData())
        {
            return false;
        }
        if(!dealDate())
        {
              CError tError = new CError();
              tError.moduleName = "AgentCancelContRateBL";
              tError.functionName = "submitData";
              tError.errorMessage = "数据处理失败-->dealData!";
              this.mErrors.addOneError(tError);
              return false;
        }
        return true;
    }

    public boolean dealDate()
    {
        String condition="";
        if(!StrTool.cTrim(this.mAgentCode).equals("")){
            condition+=" and agentcode='"+this.mAgentCode+"' ";
        }
        if(!StrTool.cTrim(this.mAgentName).equals("")){
            condition+=" and name like '%"+this.mAgentName+"%' ";
        }
        if(!StrTool.cTrim(this.mAgentCom).equals("")){
            condition+=" and agentgroup in (select agentgroup from labranchgroup where branchtype='"+this.mBranchType+"' and branchtype2='"+this.mBranchType2+"' and branchattr='"+this.mAgentCom+"') ";
        }
        if(!StrTool.cTrim(this.mAgentComName).equals("")){
            condition+=" and agentgroup in (select agentgroup from labranchgroup where branchtype='"+this.mBranchType+"' and branchtype2='"+this.mBranchType2+"' and name like '%"+this.mAgentComName+"%') ";
        }
        String sql="";
        sql="select managecom,agentcode,name,agentgroup from LAAgent "
						+"where ManageCom like '"+this.mManageCom+"%' and BranchType = '"+this.mBranchType+"' and BranchType2 ='"+this.mBranchType2+"' "
 						+"and EmployDate <= (select enddate from lastatsegment where stattype='2' and yearmonth="+this.mWageNo+" ) "
 						+"and (outworkdate is null or outworkdate>=(select startdate from lastatsegment where stattype='2' and yearmonth="+this.mWageNo+" )) "
 						+"and exists (select 'X' from latree where agentcode=laagent.agentcode "
						+"and (SpeciFlag is null or SpeciFlag<>'01')) "
                        +condition
                        +" order by AgentCode asc with ur";
        String strArr[] = null;
        XmlExport xmlexport = new XmlExport();
        xmlexport.createDocument("AgentCancelContRate.vts", "printer");
        SSRS tSSRS=new ExeSQL().execSQL(sql);
        System.out.println("SQL:"+sql);
        ListTable tlistTable = new ListTable();
        tlistTable.setName("Get");
        if(tSSRS!=null){
            for (int i = 1; i <= tSSRS.MaxRow; i++)
            {
                strArr = new String[8];
                String tAgentCode=tSSRS.GetText(i, 2);
                String tAgentGroup=tSSRS.GetText(i, 4);
                strArr[0] = tSSRS.GetText(i, 1);
                strArr[1] = tAgentCode;
                strArr[2] = tSSRS.GetText(i, 3);
                String[] tDepartment=getDepartment(tAgentGroup);
                strArr[3] = tDepartment[0];
                strArr[4] = tDepartment[1];
                String[] tGroup=getGroup(tAgentGroup);
                strArr[5] = tGroup[0];
                strArr[6] = tGroup[1];
                strArr[7] = getActiveRate(tAgentCode);

                tlistTable.add(strArr);
            }
        }
        TextTag texttag = new TextTag();
        texttag.add("ManageCom", this.mManageCom);
        strArr = new String[8];
        strArr[0] = "managecom";
        strArr[1] = "agentcode";
        strArr[2] = "name";
        strArr[3] = "bu";
        strArr[4] = "buname";
        strArr[5] = "zu";
        strArr[6] = "zuname";
        strArr[7] = "rate";
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }

    public boolean prepareOutputData()
    {
         try
         {
             mInputData = new VData();
             mInputData.add(mMap);
         }
         catch(Exception e)
         {
             CError.buildErr(this,"提交数据失败！");
             return false;
         }
         return true;
    }
    /** 获得综合撤销率 */
    public String getActiveRate(String agentCode){
        String tRate="0";
        String num1="";
        String num2="";
        String sql="";
        double tempRate =0.00;
        sql="select (select abs(sum(prem)) from lbcont where appflag='3' and agentcode='"+agentCode+"' "
            +"and signdate>=(select startdate from lastatsegment where stattype='2' and yearmonth="+this.mWageNo+" ) "
            +"and signdate<=(select enddate from lastatsegment where stattype='2' and yearmonth="+this.mWageNo+" ))*1.00/ (select sum(prem) from ( "
            +"select abs(prem) from lccont where appflag='1' and agentcode='"+agentCode+"' "
            +"and signdate>=(select startdate from lastatsegment where stattype='2' and yearmonth="+this.mWageNo+" ) "
            +"and signdate<=(select enddate from lastatsegment where stattype='2' and yearmonth="+this.mWageNo+" )  "
            +"union all "
            +"select abs(prem) from lbcont where appflag='3' and agentcode='"+agentCode+"' "
            +"and signdate>=(select startdate from lastatsegment where stattype='2' and yearmonth="+this.mWageNo+" ) "
            +"and signdate<=(select enddate from lastatsegment where stattype='2' and yearmonth="+this.mWageNo+" )) cont ) "
            +"from dual with ur";

        SSRS tSSRS=new ExeSQL().execSQL(sql);
        System.out.println(sql);
        if(tSSRS==null  || StrTool.cTrim(tSSRS.GetText(1, 1)).equals("null")){
            num1="0";
        }else{
            num1=tSSRS.GetText(1, 1);
        }
        sql="select (select count(1) from lbcont where appflag='3' and agentcode='"+agentCode+"' "
        +"and signdate>=(select startdate from lastatsegment where stattype='2' and yearmonth="+this.mWageNo+" ) "
        +"and signdate<=(select enddate from lastatsegment where stattype='2' and yearmonth="+this.mWageNo+" ))*1.00/(select count(1) from lccont where appflag='1' and agentcode='"+agentCode+"' "
        +"and signdate>=(select startdate from lastatsegment where stattype='2' and yearmonth="+this.mWageNo+" ) "
        +"and signdate<=(select enddate from lastatsegment where stattype='2' and yearmonth="+this.mWageNo+" )) "
        +"from dual with ur";
        tSSRS=new ExeSQL().execSQL(sql);
        System.out.println(sql);
        try{
            if(tSSRS==null  || StrTool.cTrim(tSSRS.GetText(1, 1)).equals("null")){
                num2="0";
            }else{
                num2=tSSRS.GetText(1, 1);
            }
            tempRate = Double.parseDouble(num1)+Double.parseDouble(num2)/2;
            DecimalFormat df = new DecimalFormat("0.00");
            tRate=String.valueOf(df.format(tempRate));


        }catch(Exception e){
            return "0";
        }
        return tRate;
    }
    /** 获得所在组 */
    public String[] getGroup(String agentGroup){
        String[] str=new String[2];
        str[0]="";
        str[1]="";
        String sql="select name,branchattr,upbranch from labranchgroup where  branchtype='"+this.mBranchType+"' and branchtype2='"+this.mBranchType2+"' and branchlevel='41' and agentGroup='"+agentGroup+"'";
        SSRS tSSRS=new ExeSQL().execSQL(sql);
        if(tSSRS.MaxRow>0){
            str[1]=tSSRS.GetText(1, 1);
            str[0]=tSSRS.GetText(1, 2);
        }
        return str;
    }
    /** 获得所在部 */
    public String[] getDepartment(String agentGroup){
        String[] str=new String[2];
        str[0]="";
        str[1]="";
        String sql="select name,branchattr,upbranch from labranchgroup where  branchtype='"+this.mBranchType+"' and branchtype2='"+this.mBranchType2+"' and branchlevel='41' and agentGroup='"+agentGroup+"'";
        SSRS tSSRS=new ExeSQL().execSQL(sql);
        if(tSSRS!=null && tSSRS.MaxRow>0){
            String upBranch=tSSRS.GetText(1, 3);
            tSSRS=new ExeSQL().execSQL("select name,branchattr from labranchgroup where agentgroup='"+upBranch+"'");
            if(tSSRS!=null && tSSRS.MaxRow>0){
                str[1]=tSSRS.GetText(1, 1);
                str[0]=tSSRS.GetText(1, 2);
            }
        }else{
            tSSRS=new ExeSQL().execSQL("select name,branchattr from labranchgroup where branchtype='"+this.mBranchType+"' and branchtype2='"+this.mBranchType2+"' and branchlevel='42' and agentGroup='"+agentGroup+"'");
            if(tSSRS!=null && tSSRS.MaxRow>0){
                str[1]=tSSRS.GetText(1, 1);
                str[0]=tSSRS.GetText(1, 2);
            }
        }
        return str;
    }
    public VData getResult()
    {
        return mResult;
    }
}

