/*
 * <p>ClassName:  </p>
 * <p>Description: 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAscriptionSchema;
import com.sinosoft.lis.vschema.LAAscriptionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.db.LAAscriptionDB;

public class LABankAscripEnsureBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private MMap mMap = new MMap();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAAscriptionSchema mLAAscriptionSchema = new LAAscriptionSchema();
    private LAAscriptionSchema eLAAscriptionSchema = new LAAscriptionSchema();
    private LAAscriptionSet mLAAscriptionSet = new LAAscriptionSet();
    private LAAscriptionSet eLAAscriptionSet = new LAAscriptionSet();
    private String mMakeType="";
    private String mFlag="";
    private String currentDate = PubFun.getCurrentDate();//当前日期
    private String currentTime = PubFun.getCurrentTime();//当期时间
    private String mConttype="";//保单类型：1--个单  2--团单
    public LABankAscripEnsureBL()
    {
    }

    public static void main(String[] args)
    {
    }

     /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if(mFlag.equals("ALL")){
        	if(!this.getAllSet()){
        		return false;
        	}
        }
        if (!checkData())
        {
            return false;
        }
        if(this.mConttype.equals("1"))
        {
        //进行个单业务处理
          if (!dealData())
          {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABankAscripEnsureBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALABankAscripEnsureBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
           }
        }else if(this.mConttype.equals("2"))
        {
        	//进行团单业务处理
        	if(!dealGrpData())
        	{
        		// @@错误处理
                CError tError = new CError();
                tError.moduleName = "LABankAscripEnsureBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败ALABankAscripEnsureBL-->dealData!";
                this.mErrors.addOneError(tError);
                return false;	
        	}
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start LABankAscripEnsureBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();

            //如果有需要处理的错误，则返回
            if (!tPubSubmit.submitData(this.mInputData,""))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LABankAscripEnsureBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("End LABankAscripEnsureBL Submit...");
        }
        mInputData = null;
        return true;
    }

	private boolean dealData()
    {
     for (int i = 1; i <= mLAAscriptionSet.size(); i++) {

       LAAscriptionSchema mLAAscriptionSchema = new LAAscriptionSchema();
       mLAAscriptionSchema = mLAAscriptionSet.get(i);

       LAAscriptionDB tLAAscriptionDB = new LAAscriptionDB();
       tLAAscriptionDB.setAscripNo(mLAAscriptionSchema.getAscripNo());
       LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
       if (!tLAAscriptionDB.getInfo()) {
             this.mErrors.copyAllErrors(tLAAscriptionDB.mErrors);
             CError tError = new CError();
             tError.moduleName = "ALAAscriptionEnsureBL";
             tError.functionName = "dealData";
             tError.errorMessage = "查找归属表基本信息时失败!";
             this.mErrors.addOneError(tError);
             return false;
       }
       tLAAscriptionSchema = tLAAscriptionDB.getSchema();
       tLAAscriptionSchema.setAscriptionDate(currentDate);

       tLAAscriptionSchema.setModifyDate(currentDate);
       tLAAscriptionSchema.setModifyTime(currentTime);
       tLAAscriptionSchema.setAscripState("3");
       eLAAscriptionSet.add(tLAAscriptionSchema);

       String AgentNew =  mLAAscriptionSchema.getAgentNew();//新的代理人
       String AgentOld = mLAAscriptionSchema.getAgentOld();//原代理人
       String AgentComNew =  mLAAscriptionSchema.getAgentComNew();//新的代理机构
       if("".equals(AgentComNew)||AgentComNew==null){
    	   AgentComNew="";
       }
       String AgentComOld = mLAAscriptionSchema.getAgentComOld();//原代理机构
       String ContNo = mLAAscriptionSchema.getContNo();


       String agentSQL =" select a.agentgroup,(select branchattr from labranchgroup where agentgroup=a.agentgroup),"
                       +" (select branchseries from labranchgroup where agentgroup=a.agentgroup),a.branchcode,a.name,a.managecom from laagent a "
                       +" where agentcode='"+AgentNew+"' and branchtype='"+mLAAscriptionSchema.getBranchType()+"'"
                       +" and branchtype2='"+mLAAscriptionSchema.getBranchType2()+"'";
       SSRS tSSRS = new SSRS();
       ExeSQL tExeSQL = new ExeSQL();
       tSSRS = tExeSQL.execSQL(agentSQL);
       if(tSSRS.getMaxRow()<0)
       {
                // @@错误处理
         this.mErrors.copyAllErrors(tSSRS.mErrors);
         CError tError = new CError();
         tError.moduleName = "LABankAscripEnsureBL";
         tError.functionName = "submitDat";
         tError.errorMessage = "人员新政信息查询出错!";
         this.mErrors.addOneError(tError);
         return false;
       }
       String AgentGroup = tSSRS.GetText(1, 1);
       String Name =  tSSRS.GetText(1, 5);
       String aManagecom = tSSRS.GetText(1, 6);

           //修改业务表
       String LCPolSQL = "update LCPol  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup = '"+AgentGroup+"',managecom='"+aManagecom+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"'";
       String LCContSQL  = "update LCCont set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"',managecom='"+aManagecom+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
       String LCContGetPolSQL   = "update LCContGetPol set agentcode = '"+AgentNew+"',agentname='"+Name+"' , managecom='"+aManagecom+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"'where contno='"+ContNo+"' ";
       String LJSPAYSQL   = "update LJSPAY  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' , managecom='"+aManagecom+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where otherno='"+ContNo+"' ";
//       String LJTEMPFEESQL   = "update LJTEMPFEE   set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"'  where otherno='"+ContNo+"' and othernotype='2'";
       String LCUWSubSQL = "update LCUWSub set agentcode='"+AgentNew+"',agentgroup = '"+AgentGroup+"', managecom='"+aManagecom+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
       String LCUWMasterSQL = "update LCUWMaster set agentcode='"+AgentNew+"',agentgroup='"+AgentGroup+"',managecom='"+aManagecom+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno = '"+ContNo+"' ";
       String LCCUWSubSQL = "update LCCUWSub set agentcode='"+AgentNew+"',agentgroup='"+AgentGroup+"', managecom='"+aManagecom+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"'";
       String LCCUWMasterSQL = "update LCCUWMaster set agentcode = '"+AgentNew+"',agentgroup = '"+AgentGroup+"',managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"'  where contno='"+ContNo+"'";
       String LCContReceiveSQL   = "update LCContReceive set agentcode = '"+AgentNew+"',agentname='"+Name+"', managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"' where contno='"+ContNo+"' ";

       String LJSGETCLAIMSQL   = "update LJSGETCLAIM set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' , managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
       String LJSGETENDORSESQL   = "update LJSGETENDORSE set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"',  managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
       String LJSPAYBSQL   = "update LJSPAYB set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' , managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where otherno='"+ContNo+"' ";
       String LJAGETOTHERSQL   = "update LJAGETOTHER   set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' , managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"'  where othernotype='6' and otherno in (select polno from lcpol where contno='"+ContNo+"')";

       String LPUWSUBMAINSQL   = "update LPUWSUBMAIN set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"',  managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
       String LPUWSUBSQL   = "update LPUWSUB  set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"',  managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
       String LPUWMASTERMAINSQL   = "update LPUWMASTERMAIN  set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
       String LPUWMASTERSQL   = "update LPUWMASTER  set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"' , managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";

       String LPPOLSQL   = "update LPPOL  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"',  managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
       String LPCUWMASTERSQL   = "update LPCUWMASTER  set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
       String LPCONTSQL   = "update LPCONT set agentcom = '"+AgentComNew+"',agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', managecom='"+aManagecom+"',  modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
       String LLCLAIMUWDETAILSQL   = "update LLCLAIMUWDETAIL  set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";

       String LLCLAIMUNDERWRITESQL   = "update LLCLAIMUNDERWRITE   set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
       String LLCLAIMPOLICYSQL   = "update LLCLAIMPOLICY   set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
       String LLCLAIMDETAILSQL   = "update LLCLAIMDETAIL   set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
       String LLCASEPOLICYLSQL   = "update LLCASEPOLICY   set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";
       String LJSPAYPERSONBSQL   = "update LJSPAYPERSON   set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"', managecom='"+aManagecom+"',  modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' ";



       mMap.put(LCPolSQL, "UPDATE");
       mMap.put(LCContSQL, "UPDATE");
       mMap.put(LCContGetPolSQL, "UPDATE");
       mMap.put(LJSPAYSQL, "UPDATE");
//       mMap.put(LJTEMPFEESQL, "UPDATE");
       mMap.put(LCUWSubSQL, "UPDATE");
       mMap.put(LCUWMasterSQL, "UPDATE");
       mMap.put(LCCUWSubSQL, "UPDATE");
       mMap.put(LCCUWMasterSQL	, "UPDATE");
       mMap.put(LCContReceiveSQL, "UPDATE");

       mMap.put(LJSGETCLAIMSQL, "UPDATE");
       mMap.put(LJSGETENDORSESQL, "UPDATE");
       mMap.put(LJSPAYBSQL, "UPDATE");
       mMap.put(LJAGETOTHERSQL, "UPDATE");

       mMap.put(LPUWSUBMAINSQL, "UPDATE");
       mMap.put(LPUWSUBSQL, "UPDATE");
       mMap.put(LPUWMASTERMAINSQL, "UPDATE");
       mMap.put(LPUWMASTERSQL	, "UPDATE");

       mMap.put(LPPOLSQL, "UPDATE");
       mMap.put(LPCUWMASTERSQL, "UPDATE");
       mMap.put(LPCONTSQL, "UPDATE");
       mMap.put(LLCLAIMUWDETAILSQL, "UPDATE");

       mMap.put(LLCLAIMUNDERWRITESQL, "UPDATE");
       mMap.put(LLCLAIMPOLICYSQL, "UPDATE");
       mMap.put(LLCLAIMDETAILSQL, "UPDATE");
       mMap.put(LLCASEPOLICYLSQL, "UPDATE");
       mMap.put(LJSPAYPERSONBSQL, "UPDATE");
     }
     mMap.put(eLAAscriptionSet, "UPDATE");
     return true;
    }
    private boolean dealGrpData()
    {  
    	for (int i = 1; i <= mLAAscriptionSet.size(); i++)
    	{
    	       LAAscriptionSchema mLAAscriptionSchema = new LAAscriptionSchema();
    	       mLAAscriptionSchema = mLAAscriptionSet.get(i);

    	       LAAscriptionDB tLAAscriptionDB = new LAAscriptionDB();
    	       tLAAscriptionDB.setAscripNo(mLAAscriptionSchema.getAscripNo());
    	       LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
    	       if (!tLAAscriptionDB.getInfo()) {
    	             this.mErrors.copyAllErrors(tLAAscriptionDB.mErrors);
    	             CError tError = new CError();
    	             tError.moduleName = "ALAAscriptionEnsureBL";
    	             tError.functionName = "dealData";
    	             tError.errorMessage = "查找归属表基本信息时失败!";
    	             this.mErrors.addOneError(tError);
    	             return false;
    	       }
    	       tLAAscriptionSchema = tLAAscriptionDB.getSchema();
    	       tLAAscriptionSchema.setAscriptionDate(currentDate);

    	       tLAAscriptionSchema.setModifyDate(currentDate);
    	       tLAAscriptionSchema.setModifyTime(currentTime);
    	       tLAAscriptionSchema.setAscripState("3");
    	       eLAAscriptionSet.add(tLAAscriptionSchema);

    	       String AgentNew =  mLAAscriptionSchema.getAgentNew();//新的代理人
    	       String AgentOld = mLAAscriptionSchema.getAgentOld();//原代理人
    	       String AgentComNew =  mLAAscriptionSchema.getAgentComNew();//新的代理机构
    	       if("".equals(AgentComNew)||AgentComNew==null){
    	    	   AgentComNew="";
    	       }
    	       String AgentComOld = mLAAscriptionSchema.getAgentComOld();//原代理机构
    	       String GrpContNo = mLAAscriptionSchema.getGrpContNo();


    	       String agentSQL =" select a.agentgroup,(select branchattr from labranchgroup where agentgroup=a.agentgroup),"
    	                       +" (select branchseries from labranchgroup where agentgroup=a.agentgroup),a.branchcode,a.name,a.managecom from laagent a "
    	                       +" where agentcode='"+AgentNew+"' and branchtype='"+mLAAscriptionSchema.getBranchType()+"'"
    	                       +" and branchtype2='"+mLAAscriptionSchema.getBranchType2()+"'";
    	       SSRS tSSRS = new SSRS();
    	       ExeSQL tExeSQL = new ExeSQL();
    	       tSSRS = tExeSQL.execSQL(agentSQL);
    	       if(tSSRS.getMaxRow()<0)
    	       {
    	                // @@错误处理
    	         this.mErrors.copyAllErrors(tSSRS.mErrors);
    	         CError tError = new CError();
    	         tError.moduleName = "LABankAscripEnsureBL";
    	         tError.functionName = "submitDat";
    	         tError.errorMessage = "人员新政信息查询出错!";
    	         this.mErrors.addOneError(tError);
    	         return false;
    	       }
    	       String AgentGroup = tSSRS.GetText(1, 1);
    	       String Name =  tSSRS.GetText(1, 5);
    	       String aManagecom = tSSRS.GetText(1, 6);
    	       //修改业务表
    	       String LCPolSQL = "update LCPol  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup = '"+AgentGroup+"',managecom='"+aManagecom+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"'";
    	       String LCContSQL  = "update LCCont set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"',managecom='"+aManagecom+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
    	       String LCContGetPolSQL   = "update LCContGetPol set agentcode = '"+AgentNew+"',agentname='"+Name+"' , managecom='"+aManagecom+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"'where contno='"+GrpContNo+"' ";
    	       String LJSPAYSQL   = "update LJSPAY  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' , managecom='"+aManagecom+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where otherno='"+GrpContNo+"' ";
//    	       String LJTEMPFEESQL   = "update LJTEMPFEE   set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"'  where otherno='"+ContNo+"' and othernotype='2'";
    	       String LCUWSubSQL = "update LCUWSub set agentcode='"+AgentNew+"',agentgroup = '"+AgentGroup+"', managecom='"+aManagecom+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
    	       String LCUWMasterSQL = "update LCUWMaster set agentcode='"+AgentNew+"',agentgroup='"+AgentGroup+"',managecom='"+aManagecom+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno = '"+GrpContNo+"' ";
    	       String LCCUWSubSQL = "update LCCUWSub set agentcode='"+AgentNew+"',agentgroup='"+AgentGroup+"', managecom='"+aManagecom+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"'";
    	       String LCCUWMasterSQL = "update LCCUWMaster set agentcode = '"+AgentNew+"',agentgroup = '"+AgentGroup+"',managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"'  where grpcontno='"+GrpContNo+"'";
    	       String LCContReceiveSQL   = "update LCContReceive set agentcode = '"+AgentNew+"',agentname='"+Name+"', managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"' where contno='"+GrpContNo+"' ";

    	       String LJSGETCLAIMSQL   = "update LJSGETCLAIM set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' , managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
    	       String LJSGETENDORSESQL   = "update LJSGETENDORSE set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"',  managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
    	       String LJSPAYBSQL   = "update LJSPAYB set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' , managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where otherno='"+GrpContNo+"' ";
    	       String LJAGETOTHERSQL   = "update LJAGETOTHER   set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"' , managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"'  where othernotype='6' and otherno in (select polno from lcpol where grpcontno='"+GrpContNo+"')";

    	       String LPUWSUBMAINSQL   = "update LPUWSUBMAIN set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"',  managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
    	       String LPUWSUBSQL   = "update LPUWSUB  set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"',  managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
    	       String LPUWMASTERMAINSQL   = "update LPUWMASTERMAIN  set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
    	       String LPUWMASTERSQL   = "update LPUWMASTER  set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"' , managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";

    	       String LPPOLSQL   = "update LPPOL  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"',  managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
    	       String LPCUWMASTERSQL   = "update LPCUWMASTER  set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', managecom='"+aManagecom+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
    	       String LPCONTSQL   = "update LPCONT set agentcom = '"+AgentComNew+"',agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', managecom='"+aManagecom+"',  modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
    	       String LLCLAIMUWDETAILSQL   = "update LLCLAIMUWDETAIL  set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";

    	       String LLCLAIMUNDERWRITESQL   = "update LLCLAIMUNDERWRITE   set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
    	       String LLCLAIMPOLICYSQL   = "update LLCLAIMPOLICY   set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
    	       String LLCLAIMDETAILSQL   = "update LLCLAIMDETAIL   set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
    	       String LLCASEPOLICYLSQL   = "update LLCASEPOLICY   set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
    	       String LJSPAYPERSONBSQL   = "update LJSPAYPERSON   set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"', managecom='"+aManagecom+"',  modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
               //增加团单
    	       String LCGRPPolSQL = "update LCGRPPol  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup = '"+AgentGroup+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"'";
    	       String LCGRPContSQL  = "update LCGRPCont set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
    	       String LPGRPPOLSQL   = "update LPGRPPOL  set agentcode = '"+AgentNew+"',AGENTCOM = '"+AgentComNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
    	       String LPGRPCONTSQL   = "update LPGRPCONT set agentcom = '"+AgentComNew+"',agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where grpcontno='"+GrpContNo+"' ";
    	       String LBPOLSQL = "update lbpol set AgentCode='"+AgentNew+"' ,AgentGroup='"+AgentGroup+"' ,agentcom='"+AgentComNew +"' ,modifydate=current date ,modifytime=current time  where  grpcontno='"+GrpContNo+"'";
               String LBGRPPOLSQL = "update lbgrppol set AgentCode='"+AgentNew+"' ,AgentGroup='"+AgentGroup+"' ,agentcom='"+AgentComNew +"' ,modifydate=current date ,modifytime=current time  where  grpcontno='"+GrpContNo+"'";
               String LBCONTSQL = "update lbcont set AgentCode='"+AgentNew+"' ,AgentGroup='"+AgentGroup+"' ,agentcom='"+AgentComNew +"' ,modifydate=current date,modifytime=current time  where  grpcontno='"+GrpContNo+"'";
               String LBGRPCONTSQL = "update lbgrpcont set AgentCode='"+AgentNew+"' ,AgentGroup='"+AgentGroup+"' ,agentcom='"+AgentComNew +"' ,modifydate=current date,modifytime=current time  where  grpcontno='"+GrpContNo+"'";
               String LJSPAYGRPSQL= "update LJSPayGrp set AgentCode='"+AgentNew+"' ,AgentGroup='"+AgentGroup+"' ,agentcom='"+AgentComNew +"' ,modifydate=current date ,modifytime=current time  where  grpcontno='"+GrpContNo+"'";
               
               mMap.put(LCGRPPolSQL, "UPDATE");
               mMap.put(LCGRPContSQL, "UPDATE");
               mMap.put(LPGRPPOLSQL, "UPDATE");
               mMap.put(LPGRPCONTSQL, "UPDATE");
               mMap.put(LBPOLSQL, "UPDATE");
               mMap.put(LBGRPPOLSQL, "UPDATE");
               mMap.put(LBCONTSQL, "UPDATE");
               mMap.put(LBGRPCONTSQL, "UPDATE");
               mMap.put(LJSPAYGRPSQL, "UPDATE");
    	       mMap.put(LCPolSQL, "UPDATE");
    	       mMap.put(LCContSQL, "UPDATE");
    	       mMap.put(LCContGetPolSQL, "UPDATE");
    	       mMap.put(LJSPAYSQL, "UPDATE");
//    	       mMap.put(LJTEMPFEESQL, "UPDATE");
    	       mMap.put(LCUWSubSQL, "UPDATE");
    	       mMap.put(LCUWMasterSQL, "UPDATE");
    	       mMap.put(LCCUWSubSQL, "UPDATE");
    	       mMap.put(LCCUWMasterSQL	, "UPDATE");
    	       mMap.put(LCContReceiveSQL, "UPDATE");

    	       mMap.put(LJSGETCLAIMSQL, "UPDATE");
    	       mMap.put(LJSGETENDORSESQL, "UPDATE");
    	       mMap.put(LJSPAYBSQL, "UPDATE");
    	       mMap.put(LJAGETOTHERSQL, "UPDATE");

    	       mMap.put(LPUWSUBMAINSQL, "UPDATE");
    	       mMap.put(LPUWSUBSQL, "UPDATE");
    	       mMap.put(LPUWMASTERMAINSQL, "UPDATE");
    	       mMap.put(LPUWMASTERSQL	, "UPDATE");

    	       mMap.put(LPPOLSQL, "UPDATE");
    	       mMap.put(LPCUWMASTERSQL, "UPDATE");
    	       mMap.put(LPCONTSQL, "UPDATE");
    	       mMap.put(LLCLAIMUWDETAILSQL, "UPDATE");

    	       mMap.put(LLCLAIMUNDERWRITESQL, "UPDATE");
    	       mMap.put(LLCLAIMPOLICYSQL, "UPDATE");
    	       mMap.put(LLCLAIMDETAILSQL, "UPDATE");
    	       mMap.put(LLCASEPOLICYLSQL, "UPDATE");
    	       mMap.put(LJSPAYPERSONBSQL, "UPDATE");
    	 }
    	 mMap.put(eLAAscriptionSet, "UPDATE");
     return true;	
    }
    private boolean getInputData(VData cInputData)
    {
    	
      this.mFlag=(String)cInputData.getObjectByObjectName("String",0);
      if(null!=mFlag && !mFlag.equals("") ){
    	  if("SELECT".equals(mFlag)){
    		  this.mLAAscriptionSet.set((LAAscriptionSet) cInputData.getObjectByObjectName("LAAscriptionSet", 0));
    	  }else if("ALL".equals(mFlag)){
    		  this.eLAAscriptionSchema.setSchema((LAAscriptionSchema) cInputData.getObjectByObjectName("LAAscriptionSchema", 0));
    		  System.out.println("管理机构:"+this.eLAAscriptionSchema.getManageCom());
    	  }
      }
     
      this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
      
      this.mMakeType = (String)cInputData.get(3);
      System.out.println("mFlag:"+mFlag);
      System.out.println("mMakeType:"+mMakeType);
//      if(this.mFlag.equals("SELECT")){
    	  if(!cInputData.get(4).equals("")&&cInputData.get(4)!=null)
          {
        	  this.mConttype=(String)cInputData.get(4);
              System.out.println("保单类型：["+this.mConttype+"]");  
          }
     // }
     
      return true;
    }
     //验证原代理人与保单号是否符合
    private boolean checkData()
    {
     for (int i = 1; i <= mLAAscriptionSet.size(); i++) {
    	 LAAscriptionSchema cLAAscriptionSchema = new LAAscriptionSchema();
       cLAAscriptionSchema = mLAAscriptionSet.get(i);
       String groupAgentCode = new ExeSQL().getOneValue("select groupagentcode from laagent where agentcode='"+cLAAscriptionSchema.getAgentOld()+"' with ur ");
       if(cLAAscriptionSchema.getContNo()!=null&&!(cLAAscriptionSchema.getContNo().equals(""))||(cLAAscriptionSchema.getGrpContNo()!=null&&!cLAAscriptionSchema.getGrpContNo().equals("")))
       {
        String strsql="select 'Y' from lccont  where agentcode='"+cLAAscriptionSchema.getAgentOld()+"' and contno='"+cLAAscriptionSchema.getContNo()+"' and grpcontno='00000000000000000000'";
         strsql+=" union select 'Y' from lcgrpcont  where agentcode='"+cLAAscriptionSchema.getAgentOld()+"' and grpcontno='"+cLAAscriptionSchema.getGrpContNo()+"' and grpcontno<>'00000000000000000000'";
        ExeSQL tstrExeSQL = new ExeSQL();
        String tCommisionSN = tstrExeSQL.getOneValue(strsql);
        
        
        if (tCommisionSN==null || tCommisionSN.equals(""))
        {
        	//modify by zhuxt 20141029
        	//已经分配确认过，会出现再次分配的情况，这时候保单的代理人已经变成了agentnew，这种情况就不再提示错误了。
        	if(this.mConttype.equals("1"))
        	{
        	 String asql = "select 'Y' from laascription where validflag = 'N' and ascripstate = '3' and agentold='"+cLAAscriptionSchema.getAgentOld()
        	                +"' and agentnew = '"+cLAAscriptionSchema.getAgentNew()+"' and contno='"+cLAAscriptionSchema.getContNo()+"'";
        	 String temp = tstrExeSQL.getOneValue(asql);
        	 if(temp == null || temp.equals("")) {
//        		 @@错误处理
                CError tError = new CError();
                tError.moduleName = "LABankAscripEnsureBL";
                tError.functionName = "checkData";
                tError.errorMessage = "原代理人"+groupAgentCode+"没有个单号为:"+cLAAscriptionSchema.getContNo()+"的保单";
                this.mErrors.addOneError(tError);
                return false;
        	   }
        	 }else if(this.mConttype.equals("2"))
        	 {
        	    String asql = "select 'Y' from laascription where validflag = 'N' and ascripstate = '3' and agentold='"+cLAAscriptionSchema.getAgentOld()
	                +"' and agentnew = '"+cLAAscriptionSchema.getAgentNew()+"' and grpcontno='"+cLAAscriptionSchema.getGrpContNo()+"'";
        	    	String temp = tstrExeSQL.getOneValue(asql);
        	    	if(temp == null || temp.equals("")) {
        	    		//		 @@错误处理
        	    		CError tError = new CError();
        	    		tError.moduleName = "LABankAscripEnsureBL";
        	    		tError.functionName = "checkData";
        	    		tError.errorMessage = "原代理人"+groupAgentCode+"没有团单号为:"+cLAAscriptionSchema.getGrpContNo()+"的保单";
        	    		this.mErrors.addOneError(tError);
        	    		return false;
        	    	} 
        	 }
             
        }
//        if(cLAAscriptionSchema.getAgentComOld()!=null&&(!cLAAscriptionSchema.getAgentComOld().equals(""))){
//             strsql = "select distinct agentcode from lacomtoagent where  agentcom='" +
//                      cLAAscriptionSchema.getAgentComNew() + "' and relatype='1'  ";
//             tstrExeSQL = new ExeSQL();
//             String tAgentToComCNT = tstrExeSQL.getOneValue(strsql);
//             System.out.println("aaaaaaaaaaa:"+tAgentToComCNT);
//             if(tAgentToComCNT!=null &&! tAgentToComCNT.equals("")){
//                 System.out.println("comhe  heewer  s");
//                 if (!tAgentToComCNT.equals(cLAAscriptionSchema.getAgentNew())) {
//                     // @@错误处理
//                     CError tError = new CError();
//                     tError.moduleName = "LABankAscripEnsureBL";
//                     tError.functionName = "checkData";
//                     tError.errorMessage = "新代理人" +
//                                           cLAAscriptionSchema.getAgentNew() +
//                                           "不是" +
//                                           cLAAscriptionSchema.getAgentComNew() +
//                                           "的负责人";
//                     this.mErrors.addOneError(tError);
//                     return false;
//                 }
//             }
//        }
//        else{
//             System.out.println("come here");
//              if (cLAAscriptionSchema.getAgentComNew() != null &&! cLAAscriptionSchema.getAgentComNew().equals("")) {
//
//                   CError tError = new CError();
//                   tError.moduleName = "LABankAscripEnsureBL";
//                   tError.functionName = "checkData";
//                   tError.errorMessage = "此单为银代直销业务，不需要录入新网点机构代码 ！";
//                   this.mErrors.addOneError(tError);
//                   return false;
//
//              }
//        }
       }
       else
       {
         String strsql="select contno from lccont  where agentcode='"+cLAAscriptionSchema.getAgentOld()+"' and uwflag<>'a'";
         strsql+="union select grpcontno from lcgrpcont  where agentcode='"+cLAAscriptionSchema.getAgentOld()+"' and uwflag<>'a'";
         ExeSQL tstrExeSQL = new ExeSQL();
         String tCommisionSN = tstrExeSQL.getOneValue(strsql);
         if (tCommisionSN==null || tCommisionSN.equals(""))
         {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "LABankAscripEnsureBL";
             tError.functionName = "checkData";
             tError.errorMessage = "原代理人"+groupAgentCode+"名下没有保单，不用做归属！";
             this.mErrors.addOneError(tError);
             return false;
         }
       }
     }
       return true;
    }
     /**
      * 准备往后层输出所需要的数据
      * 输出：如果准备数据时发生错误则返回false,否则返回true
      */
    private boolean submitquery()
    {
        return true;
    }

    private boolean prepareOutputData()
    {
       try
       {
           this.mInputData = new VData();
           this.mInputData.add(this.mGlobalInput);
           System.out.println("|||||||||||||||"+mLAAscriptionSchema.getBranchAttr());
           this.mInputData.add(mMap);

       }
       catch (Exception ex)
       {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "LABankAscripEnsureBL";
           tError.functionName = "prepareData";
           tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
           this.mErrors.addOneError(tError);
           return false;
       }
       return true;
    }

    public VData getResult()
    {
       return this.mResult;
    }


    private boolean getAllSet() {
      
      System.out.println("Save ALL");
      String oldagentcode = this.eLAAscriptionSchema.getAgentOld();
      String tmanagecom = this.eLAAscriptionSchema.getManageCom();
      String tcontno  = this.eLAAscriptionSchema.getContNo();
      System.out.println(tcontno);
      System.out.println(tmanagecom);
      System.out.println(oldagentcode);
      String tSQL="select * from laascription where AscripState='2' "
      	  +" and validflag='N' and branchtype='3' and branchtype2='01' and MakeType='"+mMakeType
      	  +"' and managecom like '"+this.eLAAscriptionSchema.getManageCom()+"%'";
  	  if(this.eLAAscriptionSchema.getAgentOld()!=null&&!this.eLAAscriptionSchema.getAgentOld().equals(""))
  	  {
  		  tSQL+=" and AgentOld = '"+this.eLAAscriptionSchema.getAgentOld()+"'";
  	  }
  	    if((this.eLAAscriptionSchema.getContNo()!=null&&!this.eLAAscriptionSchema.getContNo().equals(""))||(this.eLAAscriptionSchema.getGrpContNo()!=null&&!this.eLAAscriptionSchema.getGrpContNo().equals("")))
  	    {
  	     if(!this.mConttype.equals("")&&this.mConttype!=null)
  	      {
  	    	if(this.mConttype.equals("1"))
  	    	{
  		      tSQL+=" and contno = '"+this.eLAAscriptionSchema.getContNo()+"'";
  	    	}else if(this.mConttype.equals("2"))
  	    	{
  	    		tSQL+=" and grpcontno = '"+this.eLAAscriptionSchema.getGrpContNo()+"'"; 
  	    	  }
  	      }
  	    }
  	   tSQL+=" with ur";
  	  System.out.println(tSQL);
  	  LAAscriptionDB tLAAscriptionDB = new LAAscriptionDB();
  	  this.mLAAscriptionSet=tLAAscriptionDB.executeQuery(tSQL);
  	  if(mLAAscriptionSet.size()<=0){
  		CError tError = new CError();
        tError.moduleName = "LABankAscripEnsureBL";
        tError.functionName = "getAllSet()";
        tError.errorMessage = "未查询到要分配的保单数据";
        this.mErrors.addOneError(tError);
        return false;
  	  }
  	  System.out.println("cindy");
  	  if(this.eLAAscriptionSchema.getContNo().equals("")||this.eLAAscriptionSchema.getContNo()==null)
  	  {   System.out.println("apple");
  	    for(int j=1;j<=this.mLAAscriptionSet.size();j++){
  		  String tsql="select conttype from lccont where grpcontno='00000000000000000000' and contno='"+this.mLAAscriptionSet.get(j).getContNo()+"'";
  		  tsql+="union select conttype from lccont where grpcontno<>'00000000000000000000' and grpcontno='"+this.mLAAscriptionSet.get(j).getGrpContNo()+"'";
  		 ExeSQL tstrExeSQL = new ExeSQL();
         this.mConttype = tstrExeSQL.getOneValue(tsql);
         System.out.println("保单类型apple:"+this.mConttype);
  	     }
  	  }
  	  return true;
   }
}
