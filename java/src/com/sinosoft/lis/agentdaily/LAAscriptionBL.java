/*
 * <p>ClassName: ALAAscriptionBL </p>
 * <p>Description: ALAAscriptionBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAscriptionSchema;
import com.sinosoft.lis.vschema.LAAscriptionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.db.LAOrphanPolicyDB;
import com.sinosoft.lis.db.LJAGetClaimDB;
import com.sinosoft.lis.db.LJAGetEndorseDB;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.schema.LAOrphanPolicySchema;
import com.sinosoft.lis.schema.LAOrphanPolicyBSchema;
import com.sinosoft.lis.schema.LJAGetClaimSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LAOrphanPolicyBSet;
import com.sinosoft.lis.vschema.LAOrphanPolicySet;
import com.sinosoft.lis.vschema.LJAGetClaimSet;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LJAPaySet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.pubfun.MMap;

public class LAAscriptionBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private MMap mMap = new MMap();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAAscriptionSchema mLAAscriptionSchema = new LAAscriptionSchema();
    private LAAscriptionSet mLAAscriptionSet = new LAAscriptionSet();
    private String AgentNew1 = "";
    private String tContNo1 = "";
    private String updateSQL="";
    private LAAscriptionSchema preLAAscriptionSchema= new LAAscriptionSchema();
    private LAOrphanPolicyBSet tmLAOrphanPolicyBSet = new LAOrphanPolicyBSet();
    private LAOrphanPolicySet  delLAOrphanPolicyBSet = new LAOrphanPolicySet();
    private String mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
    public LAAscriptionBL()
    {

    }

    public static void main(String[] args)
    {

    }

    /**
    * 传输数据的公共方法
    * @param: cInputData 输入的数据
    *         cOperate 数据操作
    * @return:
    */
   public boolean submitData(VData cInputData, String cOperate)
   {
       //将操作数据拷贝到本类中
       this.mOperate = cOperate;
       //得到外部传入的数据,将数据备份到本类中
       if (!getInputData(cInputData))
       {
           return false;
       }
       if (!checkData())
       {
           return false;
       }
       //进行业务处理
       if (!dealData())
       {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "LAAscriptionBL";
           tError.functionName = "submitData";
           tError.errorMessage = "数据处理失败ALAAscriptionBL-->dealData!";
           this.mErrors.addOneError(tError);
           return false;
       }
       //准备往后台的数据
       if (!prepareOutputData())
       {
           return false;
       }
       if (this.mOperate.equals("QUERY||MAIN"))
       {
           this.submitquery();
       }
       else
       {
           System.out.println("Start LAAscriptionBL Submit...");
           PubSubmit tPubSubmit = new PubSubmit();

           //如果有需要处理的错误，则返回
           if (!tPubSubmit.submitData(this.mInputData,""))
           {
               // @@错误处理
               this.mErrors.copyAllErrors(tPubSubmit.mErrors);
               CError tError = new CError();
               tError.moduleName = "LAAscriptionBL";
               tError.functionName = "submitDat";
               tError.errorMessage = "数据提交失败!";
               this.mErrors.addOneError(tError);
               return false;
           }
       }
       mInputData = null;
       return true;
   }

   private boolean dealData()
  {

	  String AgentNew =  this.mLAAscriptionSchema.getAgentNew();//新的代理人
	  String AgentOld = this.mLAAscriptionSchema.getAgentOld();//原代理人
	  String ContNo = this.mLAAscriptionSchema.getContNo();
	  String currentDate = PubFun.getCurrentDate();//当前日期
	  String currentTime = PubFun.getCurrentTime();//当期时间
	  String strsql= "select count(ascripno)+1 from laascription where 1=1 and " +
		"ascripstate='3' and contno='"+mLAAscriptionSchema.getContNo()+"'";

			ExeSQL tExeSQL1 = new ExeSQL();
			String mCount = tExeSQL1.getOneValue(strsql);
			if (tExeSQL1.mErrors.needDealError())
			 {
			     CError tError = new CError();
			   tError.moduleName = "HDLAAscriptionBL";
			   tError.functionName = "dealData";
			   tError.errorMessage = "查询归属表信息出错";
			   this.mErrors.addOneError(tError);
			    return false;
			 }
      int mCountNO=Integer.parseInt(mCount);//得到归属次数

      if(this.mOperate.equals("INSERT||MAIN"))
      {

      String mAscripNo = com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo(
              "PerAscripNo", 20);
      System.out.println("mAscripNo:"+mAscripNo);
      //插入归属表
      LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
      tLAAscriptionSchema.setSchema(mLAAscriptionSchema);
      tLAAscriptionSchema.setAscripNo(mAscripNo);
      tLAAscriptionSchema.setValidFlag("N");
      tLAAscriptionSchema.setAClass("01");
      tLAAscriptionSchema.setContNo(ContNo);

      tLAAscriptionSchema.setOperator(mGlobalInput.Operator);
      tLAAscriptionSchema.setMakeDate(currentDate);
      tLAAscriptionSchema.setMakeTime(currentTime);
      tLAAscriptionSchema.setModifyDate(currentDate);
      tLAAscriptionSchema.setModifyTime(currentTime);
      tLAAscriptionSchema.setAscriptionCount(mCountNO);
      mMap.put(tLAAscriptionSchema, "INSERT");


    String agentSQL =" select a.agentgroup,(select branchattr from labranchgroup where agentgroup=a.agentgroup),"
                    +" (select branchseries from labranchgroup where agentgroup=a.agentgroup),a.branchcode,a.name from laagent a "
                    +" where agentcode='"+AgentNew+"' and branchtype='"+this.mLAAscriptionSchema.getBranchType()+"'"
                    +" and branchtype2='"+this.mLAAscriptionSchema.getBranchType2()+"'";
    SSRS tSSRS = new SSRS();
    ExeSQL tExeSQL = new ExeSQL();
    tSSRS = tExeSQL.execSQL(agentSQL);
    if(tSSRS.getMaxRow()<0)
    {
             // @@错误处理
      this.mErrors.copyAllErrors(tSSRS.mErrors);
      CError tError = new CError();
      tError.moduleName = "LABankContAgentBL";
      tError.functionName = "submitDat";
      tError.errorMessage = "人员新政信息查询出错!";
      this.mErrors.addOneError(tError);
      return false;
    }
    String AgentGroup = tSSRS.GetText(1, 1);
    String Name =  tSSRS.GetText(1, 5);

    String oldBranchSeries="";
    String newBranchSeries="";
    String oldAgentgroup="";
    String newAgentgroup="";

    String oldBranchSeriesSQL="select branchseries from labranchgroup where branchtype='"+this.mLAAscriptionSchema.getBranchType()+"' and agentgroup=(select agentgroup from laagent where agentcode='"+AgentOld+"')";
    SSRS mSSRS00 = new SSRS();
    ExeSQL mExeSQL00 = new ExeSQL();
    mSSRS00 = mExeSQL00.execSQL(oldBranchSeriesSQL);
    oldBranchSeries=mSSRS00.GetText(1, 1);

    String newBranchSeriesSQL="select branchseries from labranchgroup where branchtype='"+this.mLAAscriptionSchema.getBranchType()+"' and agentgroup=(select agentgroup from laagent where agentcode='"+AgentNew+"')";
    SSRS mSSRS01 = new SSRS();
    ExeSQL mExeSQL01 = new ExeSQL();
    mSSRS01 = mExeSQL01.execSQL(newBranchSeriesSQL);
    newBranchSeries=mSSRS01.GetText(1, 1);

    String oldAgentgroupSQL="select agentgroup from laagent where agentcode='"+AgentOld+"'";
    SSRS mSSRS02 = new SSRS();
    ExeSQL mExeSQL02 = new ExeSQL();
    mSSRS02 = mExeSQL02.execSQL(oldAgentgroupSQL);
    oldAgentgroup=mSSRS02.GetText(1, 1);

    String newAgentgroupSQL="select agentgroup from laagent where agentcode='"+AgentNew+"'";
    SSRS mSSRS03 = new SSRS();
    ExeSQL mExeSQL03 = new ExeSQL();
    mSSRS03 = mExeSQL03.execSQL(newAgentgroupSQL);
    newAgentgroup=mSSRS03.GetText(1, 1);
    System.out.println("~~~~~~~~~~"+oldBranchSeries.substring(0,11));
    //需要反冲
    if(!oldBranchSeries.substring(0,12).equals(newBranchSeries.substring(0,12)))
    {
        LJAPayDB tLJAPayDB=new LJAPayDB();
        LJAPayPersonDB tLJAPayPersonDB=new LJAPayPersonDB();

        String tManageComSQL="select managecom from laagent where agentcode='"+AgentOld+"'";
        String ManageCom="";
        SSRS mSSRS0 = new SSRS();
        ExeSQL mExeSQL0 = new ExeSQL();
        mSSRS0 = mExeSQL0.execSQL(tManageComSQL);
        ManageCom=mSSRS0.GetText(1, 1);

        String CH1SQL=" select * from ljapay where payno in (select payno from ljapayperson where contno='"+ContNo +"'"
                     +" and agentcode='"+AgentOld+"' and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"')";
        LJAPaySet tLJAPaySet=tLJAPayDB.executeQuery(CH1SQL);
        if(tLJAPaySet.size()>0)
        {
           System.out.println("~~~~~~~"+tLJAPaySet.size());
           String tLimit = PubFun.getNoLimit(ManageCom);

           for(int i=1;i<=tLJAPaySet.size();i++)
           {
               LJAPaySchema tLJAPaySchema  = new LJAPaySchema();
               tLJAPaySchema = tLJAPaySet.get(i);
               String tPayNo=PubFun1.CreateMaxNo("PAYNO", tLimit);
               String sql1 = "insert into LJAPay (select '"+tPayNo+"', IncomeNo, IncomeType, AppntNo, SumActuPayMoney,"
                           +"PAYDATE, ENTERACCDATE, '"+currentDate+"', ApproveCode, APPROVEDATE, SerialNo, Operator,"
                           +" '"+currentDate+"','"+currentTime+"', "
                           +"GetNoticeNo, '"+currentDate+"', '"+currentTime+"', ManageCom, '"+mLAAscriptionSchema.getAgentComNew()+"', AgentType, BankCode,"
                           +"BankAccNo, RiskCode, '"+AgentNew+"', '"+newAgentgroup+"', AccName, STARTPAYDATE, PayTypeFlag,"
                           +"PrtStateFlag, PrintTimes,FinState,duefeetype,markettype,salechnl  "
                           +" from LJAPay where IncomeNo='"+ContNo+"' and payno = '"+tLJAPaySchema.getPayNo()+"')";
               String sql2 = "insert into LJAPayPerson select POLNO, PAYCOUNT, GRPCONTNO, GRPPOLNO, CONTNO, MANAGECOM, '"+mLAAscriptionSchema.getAgentComNew()+"',"
                           +"AGENTTYPE, RISKCODE, '"+AgentNew+"', '"+newAgentgroup+"', PAYTYPEFLAG, APPNTNO, '"+tPayNo+"', "
                           +"PAYAIMCLASS, DUTYCODE, PAYPLANCODE, SUMDUEPAYMONEY, SUMACTUPAYMONEY, PAYINTV"
                           +", PAYDATE, PAYTYPE, ENTERACCDATE, '"+currentDate+"', LASTPAYTODATE, CURPAYTODATE,"
                           +"ININSUACCSTATE, APPROVECODE, APPROVEDATE, APPROVETIME, SERIALNO, OPERATOR,"
                           +"'"+currentDate+"', '"+currentTime+"', GETNOTICENO, '"+currentDate+"', '"+currentTime+"', FINSTATE from"
                           +" LJAPAYPERSON where  CONTNO='"+ContNo+"' and payno = '"+tLJAPaySchema.getPayNo()+"'";

              tPayNo=PubFun1.CreateMaxNo("PAYNO", tLimit);
              String sql3 = "insert into LJAPay (select '"+tPayNo+"', IncomeNo, IncomeType, AppntNo, (-1)*SumActuPayMoney,"
                          +"PAYDATE, ENTERACCDATE, '"+currentDate+"', ApproveCode, APPROVEDATE, SerialNo, Operator,"
                          +" '"+currentDate+"','"+currentTime+"', "
                          +"GetNoticeNo, '"+currentDate+"', '"+currentTime+"', ManageCom, AgentCom, AgentType, BankCode,"
                          +"BankAccNo, RiskCode, AgentCode, AgentGroup, AccName, STARTPAYDATE, PayTypeFlag,"
                          +"PrtStateFlag, PrintTimes,FinState,duefeetype,markettype,salechnl "
                          +" from LJAPay where IncomeNo='"+ContNo+"' and payno = '"+tLJAPaySchema.getPayNo()+"')";

              String sql4 = "insert into LJAPayPerson select POLNO, PAYCOUNT, GRPCONTNO, GRPPOLNO, CONTNO, MANAGECOM, AGENTCOM,"
                          +"AGENTTYPE, RISKCODE, AGENTCODE, AGENTGROUP, PAYTYPEFLAG, APPNTNO, '"+tPayNo+"', "
                          +"PAYAIMCLASS, DUTYCODE, PAYPLANCODE, (-1)*SUMDUEPAYMONEY, (-1)*SUMACTUPAYMONEY, PAYINTV"
                          +", PAYDATE, PAYTYPE, ENTERACCDATE, '"+currentDate+"', LASTPAYTODATE, CURPAYTODATE,"
                          +"ININSUACCSTATE, APPROVECODE, APPROVEDATE, APPROVETIME, SERIALNO, OPERATOR,"
                          +"'"+currentDate+"', '"+currentTime+"', GETNOTICENO, '"+currentDate+"', '"+currentTime+"', FINSTATE from"
                          +" LJAPAYPERSON where CONTNO='"+ContNo+"' and payno = '"+tLJAPaySchema.getPayNo()+"'";

              mMap.put(sql1, "INSERT");
              mMap.put(sql2, "INSERT");
              mMap.put(sql3, "INSERT");
              mMap.put(sql4, "INSERT");
              }
            }

          String CH3SQL="select * from ljagetclaim where contno='"+ContNo+"' and agentcode='"+AgentOld+"' and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"'";
          LJAGetClaimDB tLJAGetClaimDB=new LJAGetClaimDB();
          LJAGetClaimSet tLJAGetClaimSet=tLJAGetClaimDB.executeQuery(CH3SQL);
          if(tLJAGetClaimSet.size()>0)
          {
              String tLimit = PubFun.getNoLimit(ManageCom);
              for(int i=1;i<=tLJAGetClaimSet.size();i++)
              {
                   String tACTUGETNONo = PubFun1.CreateMaxNo("ACTUGETNO", tLimit);
                   LJAGetClaimSchema tLJAGetClaimSchema  = new LJAGetClaimSchema();
                   tLJAGetClaimSchema = tLJAGetClaimSet.get(i);
                   String sql1 = "insert into LJAGetClaim (select '"+tACTUGETNONo+"', FEEFINATYPE, FEEOPERATIONTYPE, OTHERNO, OTHERNOTYPE,"
                                        +"GETDUTYCODE, GETDUTYKIND, GRPCONTNO, CONTNO, GRPPOLNO, POLNO, KINDCODE,"
                                        +"RISKCODE, RISKVERSION, SALECHNL, AGENTCODE, AGENTGROUP, GETDATE,"
                                        +"ENTERACCDATE, CONFDATE,(-1)*PAY, MANAGECOM, AGENTCOM, AGENTTYPE, GETNOTICENO,"
                                        +"OPCONFIRMCODE, OPCONFIRMDATE, OPCONFIRMTIME, SERIALNO, OPERATOR, '"+currentDate+"',"
                                        +"'"+currentTime+"', '"+currentDate+"', '"+currentTime+"' from"
                                        +" LJAGetClaim where agentcode='"+AgentOld+"' and CONTNO='"+ContNo+"' and ActuGetNo = '"+tLJAGetClaimSchema.getActuGetNo()+"')";
                    tACTUGETNONo = PubFun1.CreateMaxNo("ACTUGETNO", tLimit);
                    String sql2 = "insert into LJAGetClaim (select '"+tACTUGETNONo+"', FEEFINATYPE, FEEOPERATIONTYPE, OTHERNO, OTHERNOTYPE,"
                          +"GETDUTYCODE, GETDUTYKIND, GRPCONTNO, CONTNO, GRPPOLNO, POLNO, KINDCODE,"
                          +"RISKCODE, RISKVERSION, SALECHNL, '"+AgentNew+"', '"+AgentGroup+"', GETDATE,"
                          +"ENTERACCDATE, CONFDATE,PAY, MANAGECOM, AGENTCOM, AGENTTYPE, GETNOTICENO,"
                          +"OPCONFIRMCODE, OPCONFIRMDATE, OPCONFIRMTIME, SERIALNO, OPERATOR, '"+currentDate+"',"
                          +"'"+currentTime+"', '"+currentDate+"', '"+currentTime+"' from"
                          +" LJAGetClaim where agentcode='"+AgentOld+"' and CONTNO='"+ContNo+"' and ActuGetNo = '"+tLJAGetClaimSchema.getActuGetNo()+"')";
                     mMap.put(sql1, "INSERT");
                     mMap.put(sql2, "INSERT");
                    }

          }


          String CH4SQL="select * from ljagetendorse where contno='"+ContNo+"' and agentcode='"+AgentOld+"'  and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"'";
          LJAGetEndorseDB tLJAGetEndorseDB=new LJAGetEndorseDB();
          LJAGetEndorseSet tLJAGetEndorseSet=tLJAGetEndorseDB.executeQuery(CH4SQL);
          if(tLJAGetEndorseSet.size()>0)
          {
             String tLimit = PubFun.getNoLimit(ManageCom);
             for(int i=1;i<=tLJAGetEndorseSet.size();i++)
             {
                 String tACTUGETNONo = PubFun1.CreateMaxNo("ACTUGETNO", tLimit);
                 LJAGetEndorseSchema tLJAGetEndorseSchema   = new LJAGetEndorseSchema ();
                 tLJAGetEndorseSchema  = tLJAGetEndorseSet.get(i);
                 String sql1 = "insert into LJAGetEndorse (select '"+tACTUGETNONo+"', ENDORSEMENTNO, FEEOPERATIONTYPE, FEEFINATYPE, GRPCONTNO,"
                                       +"CONTNO, GRPPOLNO, POLNO, OTHERNO, OTHERNOTYPE, DUTYCODE, PAYPLANCODE,"
                                       +"APPNTNO, INSUREDNO, GETNOTICENO, GETDATE, ENTERACCDATE,GETCONFIRMDATE,"
                                       +"(-1)*GETMONEY, KINDCODE, RISKCODE, RISKVERSION, MANAGECOM, AGENTCOM, AGENTTYPE,"
                                       +"AGENTCODE, AGENTGROUP, GRPNAME, HANDLER, APPROVEDATE, APPROVETIME, POLTYPE,"
                                       +"APPROVECODE, OPERATOR, SERIALNO, '"+currentDate+"', '"+currentDate+"', '"+currentTime+"', GETFLAG,"
                                       +"'"+currentTime+"', FINSTATE "
                                       +"from LJAGetEndorse where agentcode='"+AgentOld+"' and CONTNO='"+ContNo+"' and ActuGetNo = '"+tLJAGetEndorseSchema.getActuGetNo()+"')";
                 tACTUGETNONo = PubFun1.CreateMaxNo("ACTUGETNO", tLimit);
                 String sql2 = "insert into LJAGetEndorse (select '"+tACTUGETNONo+"', ENDORSEMENTNO, FEEOPERATIONTYPE, FEEFINATYPE, GRPCONTNO,"
                                       +"CONTNO, GRPPOLNO, POLNO, OTHERNO, OTHERNOTYPE, DUTYCODE, PAYPLANCODE,"
                                       +"APPNTNO, INSUREDNO, GETNOTICENO, GETDATE, ENTERACCDATE,GETCONFIRMDATE,"
                                       +"GETMONEY, KINDCODE, RISKCODE, RISKVERSION, MANAGECOM, AGENTCOM, AGENTTYPE,"
                                       +"'"+AgentNew+"', '"+AgentGroup+"', GRPNAME, HANDLER, APPROVEDATE, APPROVETIME, POLTYPE,"
                                       +"APPROVECODE, OPERATOR, SERIALNO, '"+currentDate+"', '"+currentDate+"', '"+currentTime+"', GETFLAG,"
                                       +"'"+currentTime+"', FINSTATE "
                                       +"from LJAGetEndorse where agentcode='"+AgentOld+"' and CONTNO='"+ContNo+"' and ActuGetNo = '"+tLJAGetEndorseSchema.getActuGetNo()+"')";
                  mMap.put(sql1, "INSERT");
                  mMap.put(sql2, "INSERT");
                }
          }
         }
        //不需要反冲
        else{
          String LJAPaySQL   = "update LJAPay set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"'  where incomeno='"+ContNo+"' and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"'";
          String LJAPayPersonSQL   = "update LJAPayPerson set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"',modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"'";
          String LJAGETCLAIMSQL   = "update LJAGETCLAIM set agentcom = '',agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"'";
          String LJAGETENDORSESQL   = "update LJAGETENDORSE set agentcom = '',agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"', modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"'";
          //修改业务表
          String LAcommisionSQL = "update LACOMMISION SET AGENTCODE  = '"+AgentNew+"',agentgroup = '"+AgentGroup+"',branchattr =(select branchattr from labranchgroup where agentgroup='"+AgentGroup+"'),branchcode = (select branchcode from laagent where agentcode='"+AgentNew+"'),branchseries = (select branchseries from labranchgroup where agentgroup='"+AgentGroup+"'),modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"' where contno='"+ContNo+"' and agentcode='"+AgentOld+"' and tmakedate>='"+tLAAscriptionSchema.getAscriptionDate()+"'";

          mMap.put(LJAPaySQL, "UPDATE");
          mMap.put(LJAPayPersonSQL, "UPDATE");
          mMap.put(LJAGETENDORSESQL, "UPDATE");
          mMap.put(LJAGETCLAIMSQL, "UPDATE");

          mMap.put(LAcommisionSQL, "UPDATE");
        }
        String LJTEMPFEESQL   = "update LJTEMPFEE   set agentcode = '"+AgentNew+"',agentgroup='"+AgentGroup+"' ,modifydate = '"+currentDate+"',modifytime = '"+currentTime+"',operator = '"+this.mGlobalInput.Operator+"'  where otherno='"+ContNo+"' and othernotype='2'";
	 String LCUWSubSQL = "update LCUWSub set " +
	 		     "agentcode='"+AgentNew+"'," +
                             "agentgroup = '"+AgentGroup+"'," +
                             "modifydate = '"+currentDate+"'," +
                             "modifytime = '"+currentTime+"'," +
                             "operator = '"+this.mGlobalInput.Operator+"' " +
                             "where contno='"+ContNo+"' ";
	 String LCUWMasterSQL = "update LCUWMaster set " +
	 		        "agentcode='"+AgentNew+"'," +
                                "agentgroup='"+AgentGroup+"'," +
                                "modifydate = '"+currentDate+"'," +
                                "modifytime = '"+currentTime+"'," +
                                "operator = '"+this.mGlobalInput.Operator+"' " +
                                "where contno = '"+ContNo+"' ";
	 String LCPolSQL = "update LCPol  set " +
	 		   "agentcode = '"+AgentNew+"'," +
                           "agentgroup = '"+AgentGroup+"', " +
                           "modifydate = '"+currentDate+"'," +
                           "modifytime = '"+currentTime+"'," +
                           "operator = '"+this.mGlobalInput.Operator+"' " +
                           "where contno='"+ContNo+"'";
	 String LCCUWSubSQL = "update LCCUWSub set " +
	 		      "agentcode='"+AgentNew+"'," +
                              "agentgroup='"+AgentGroup+"', " +
                              "modifydate = '"+currentDate+"'," +
                              "modifytime = '"+currentTime+"'," +
                              "operator = '"+this.mGlobalInput.Operator+"' " +
                              "where contno='"+ContNo+"'";
	 String LCCUWMasterSQL = "update LCCUWMaster set " +
	 		         "agentcode = '"+AgentNew+"'," +
                                 "agentgroup = '"+AgentGroup+"', " +
                                 "modifydate = '"+currentDate+"'," +
                                 "modifytime = '"+currentTime+"'," +
                                 "operator = '"+this.mGlobalInput.Operator+"' " +
                                 " where contno='"+ContNo+"'";
	 String LCContSQL  = "update LCCont set " +
	 		     "agentcode = '"+AgentNew+"'," +
                             "agentgroup='"+AgentGroup+"', " +
                             "modifydate = '"+currentDate+"'," +
                             "modifytime = '"+currentTime+"'," +
                             "operator = '"+this.mGlobalInput.Operator+"' " +
                             "where contno='"+ContNo+"' ";
	 String LCContReceiveSQL   = "update LCContReceive set " +
		                     "agentcode = '"+AgentNew+"'," +
                                     "agentname='"+Name+"', " +
				     "modifydate = '"+currentDate+"'," +
		 		     "modifytime = '"+currentTime+"'" +
                                     "where contno='"+ContNo+"' ";
	 String LCContGetPolSQL   = "update LCContGetPol set " +
		                    "agentcode = '"+AgentNew+"'," +
				    "agentname='"+Name+"' ," +
				    "modifydate = '"+currentDate+"'," +
		 		    "modifytime = '"+currentTime+"'" +
                                    "where contno='"+ContNo+"' ";
	 String LJSGETCLAIMSQL   = "update LJSGETCLAIM set " +
                                "agentcode = '"+AgentNew+"'," +
				"agentgroup='"+AgentGroup+"' ," +
				"modifydate = '"+currentDate+"'," +
		 		"modifytime = '"+currentTime+"'," +
		 		"operator = '"+this.mGlobalInput.Operator+"' " +
                                "where contno='"+ContNo+"' ";
	 String LJSGETENDORSESQL   = "update LJSGETENDORSE set " +
		                "agentcode = '"+AgentNew+"'," +
				"agentgroup='"+AgentGroup+"', " +
				"modifydate = '"+currentDate+"'," +
		 		"modifytime = '"+currentTime+"'," +
		 		"operator = '"+this.mGlobalInput.Operator+"' " +
                                "where contno='"+ContNo+"' ";
	 String LJSPAYBSQL   = "update LJSPAYB set " +
		                "agentcode = '"+AgentNew+"'," +
				"agentgroup='"+AgentGroup+"' ," +
				"modifydate = '"+currentDate+"'," +
		 		"modifytime = '"+currentTime+"'," +
		 		"operator = '"+this.mGlobalInput.Operator+"' " +
                                "where otherno='"+ContNo+"' ";
	 String LJSPAYSQL   = "update LJSPAY  set " +
		              "agentcode = '"+AgentNew+"'," +
				"agentgroup='"+AgentGroup+"' ," +
				"modifydate = '"+currentDate+"'," +
		 		"modifytime = '"+currentTime+"'," +
		 		"operator = '"+this.mGlobalInput.Operator+"' " +
                                "where otherno='"+ContNo+"' ";
	 String LJAGETOTHERSQL   = "update LJAGETOTHER   set " +
		                "agentcode = '"+AgentNew+"'," +
				"agentgroup='"+AgentGroup+"' ," +
				"modifydate = '"+currentDate+"'," +
		 		"modifytime = '"+currentTime+"'," +
		 		"operator = '"+this.mGlobalInput.Operator+"' " +
                                " where othernotype='6' and otherno in (select polno from lcpol where contno='"+ContNo+"')";


	 String LPUWSUBMAINSQL   = "update LPUWSUBMAIN set " +
		                "agentcode = '"+AgentNew+"'," +
				"agentgroup='"+AgentGroup+"', " +
				"modifydate = '"+currentDate+"'," +
		 		"modifytime = '"+currentTime+"'," +
		 		"operator = '"+this.mGlobalInput.Operator+"' " +
                                "where contno='"+ContNo+"' ";

	 String LPUWSUBSQL   = "update LPUWSUB  set " +
		                "agentcode = '"+AgentNew+"'," +
				"agentgroup='"+AgentGroup+"', " +
				"modifydate = '"+currentDate+"'," +
		 		"modifytime = '"+currentTime+"'," +
		 		"operator = '"+this.mGlobalInput.Operator+"' " +
                                "where contno='"+ContNo+"' ";
	 String LPUWMASTERMAINSQL   = "update LPUWMASTERMAIN  set " +
		                 "agentcode = '"+AgentNew+"'," +
				"agentgroup='"+AgentGroup+"'," +
				"modifydate = '"+currentDate+"'," +
		 		"modifytime = '"+currentTime+"'," +
		 		"operator = '"+this.mGlobalInput.Operator+"' " +
                                "where contno='"+ContNo+"' ";
	 String LPUWMASTERSQL   = "update LPUWMASTER  set " +
		                "agentcode = '"+AgentNew+"'," +
				"agentgroup='"+AgentGroup+"' ," +
				"modifydate = '"+currentDate+"'," +
		 		"modifytime = '"+currentTime+"'," +
		 		"operator = '"+this.mGlobalInput.Operator+"' " +
                                "where contno='"+ContNo+"' ";
	 String LPPOLSQL   = "update LPPOL  set " +
		              "agentcode = '"+AgentNew+"'," +
				"agentgroup='"+AgentGroup+"', " +
				"modifydate = '"+currentDate+"'," +
		 		"modifytime = '"+currentTime+"'," +
		 		"operator = '"+this.mGlobalInput.Operator+"' " +
                                "where contno='"+ContNo+"' ";
	 String LPCUWMASTERSQL   = "update LPCUWMASTER  set " +
		"agentcode = '"+AgentNew+"'," +
				"agentgroup='"+AgentGroup+"'," +
				"modifydate = '"+currentDate+"'," +
		 		"modifytime = '"+currentTime+"'," +
		 		"operator = '"+this.mGlobalInput.Operator+"' " +
                                "where contno='"+ContNo+"' ";
	 String LPCONTSQL   = "update LPCONT set " +
	 	"agentcom = ''," +
		"agentcode = '"+AgentNew+"'," +
				"agentgroup='"+AgentGroup+"', " +
				"modifydate = '"+currentDate+"'," +
		 		"modifytime = '"+currentTime+"'," +
		 		"operator = '"+this.mGlobalInput.Operator+"' " +
                                "where contno='"+ContNo+"' ";
	 String LLCLAIMUWDETAILSQL   = "update LLCLAIMUWDETAIL  set " +
		"agentcode = '"+AgentNew+"'," +
				"agentgroup='"+AgentGroup+"' ," +
				"modifydate = '"+currentDate+"'," +
		 		"modifytime = '"+currentTime+"'," +
		 		"operator = '"+this.mGlobalInput.Operator+"' " +
                                "where contno='"+ContNo+"' ";
	 String LLCLAIMUNDERWRITESQL   = "update LLCLAIMUNDERWRITE   set " +
		"agentcode = '"+AgentNew+"'," +
				"agentgroup='"+AgentGroup+"', " +
				"modifydate = '"+currentDate+"'," +
		 		"modifytime = '"+currentTime+"'," +
		 		"operator = '"+this.mGlobalInput.Operator+"' " +
                                "where contno='"+ContNo+"' ";
	 String LLCLAIMPOLICYSQL   = "update LLCLAIMPOLICY   set " +
		"agentcode = '"+AgentNew+"'," +
				"agentgroup='"+AgentGroup+"', " +
				"modifydate = '"+currentDate+"'," +
		 		"modifytime = '"+currentTime+"'," +
		 		"operator = '"+this.mGlobalInput.Operator+"' " +
                                "where contno='"+ContNo+"' ";
	 String LLCLAIMDETAILSQL   = "update LLCLAIMDETAIL   set " +
		"agentcode = '"+AgentNew+"'," +
				"agentgroup='"+AgentGroup+"', " +
				"modifydate = '"+currentDate+"'," +
		 		"modifytime = '"+currentTime+"'," +
		 		"operator = '"+this.mGlobalInput.Operator+"' " +
                                "where contno='"+ContNo+"' ";
	 String LLCASEPOLICYLSQL   = "update LLCASEPOLICY   set " +
		"agentcode = '"+AgentNew+"'," +
				"agentgroup='"+AgentGroup+"', " +
				"modifydate = '"+currentDate+"'," +
		 		"modifytime = '"+currentTime+"'," +
		 		"operator = '"+this.mGlobalInput.Operator+"' " +
                                "where contno='"+ContNo+"' ";
	 String LJSPAYPERSONBSQL   = "update LJSPAYPERSONB   set " +
		"agentcode = '"+AgentNew+"'," +
				"agentgroup='"+AgentGroup+"', " +
				"modifydate = '"+currentDate+"'," +
		 		"modifytime = '"+currentTime+"'," +
		 		"operator = '"+this.mGlobalInput.Operator+"' " +
                                "where contno='"+ContNo+"' ";
	 String LJSPAYPERSONSQL   = "update LJSPAYPERSON   set " +
		"agentcode = '"+AgentNew+"'," +
				"agentgroup='"+AgentGroup+"'," +
				"modifydate = '"+currentDate+"'," +
		 		"modifytime = '"+currentTime+"'," +
		 		"operator = '"+this.mGlobalInput.Operator+"' " +
                                "where contno='"+ContNo+"' ";


	 mMap.put(LCUWSubSQL, "UPDATE");
	 mMap.put(LCUWMasterSQL, "UPDATE");
	 mMap.put(LCPolSQL, "UPDATE");
	 mMap.put(LCCUWSubSQL, "UPDATE");
	 mMap.put(LCCUWMasterSQL, "UPDATE");
	 mMap.put(LCContSQL, "UPDATE");
	 mMap.put(LCContReceiveSQL, "UPDATE");
	 mMap.put(LCContGetPolSQL, "UPDATE");
         mMap.put(LJTEMPFEESQL	, "UPDATE");
	 mMap.put(LJSGETCLAIMSQL, "UPDATE");
	 mMap.put(LJSGETENDORSESQL, "UPDATE");
	 mMap.put(LJSPAYBSQL, "UPDATE");
	 mMap.put(LJSPAYSQL, "UPDATE");
	 mMap.put(LJAGETOTHERSQL, "UPDATE");
	 mMap.put(LPUWSUBMAINSQL, "UPDATE");
	 mMap.put(LPUWSUBSQL, "UPDATE");
	 mMap.put(LPUWMASTERMAINSQL, "UPDATE");
	 mMap.put(LPUWMASTERSQL	, "UPDATE");
	 mMap.put(LPPOLSQL, "UPDATE");
	 mMap.put(LPCUWMASTERSQL, "UPDATE");
	 mMap.put(LPCONTSQL, "UPDATE");
	 mMap.put(LLCLAIMUWDETAILSQL, "UPDATE");
	 mMap.put(LLCLAIMUNDERWRITESQL, "UPDATE");
	 mMap.put(LLCLAIMPOLICYSQL, "UPDATE");
	 mMap.put(LLCLAIMDETAILSQL, "UPDATE");
	 mMap.put(LLCASEPOLICYLSQL, "UPDATE");
         mMap.put(LJSPAYPERSONBSQL, "UPDATE");
	 mMap.put(LJSPAYPERSONSQL, "UPDATE");

//	修改核心表
	}


    return true;
  }

   private boolean getInputData(VData cInputData)
     {
         this.mLAAscriptionSchema.setSchema((LAAscriptionSchema) cInputData.getObjectByObjectName("LAAscriptionSchema", 0));
         System.out.println("Cont="+mLAAscriptionSchema.getContNo());
         this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
         AgentNew1 = (String)cInputData.get(2);
         System.out.println("AgentNew1:"+AgentNew1);
         System.out.println("????????????????");
         return true;
     }
     //验证原代理人与保单号是否符合
     private boolean checkData()
    {
       if(mLAAscriptionSchema.getContNo()!=null&&!(mLAAscriptionSchema.getContNo().equals("")))
       {
        String strsql="select 'Y' from lccont  where agentcode='"+mLAAscriptionSchema.getAgentOld()+"' and contno='"+mLAAscriptionSchema.getContNo()+"'";
        ExeSQL tstrExeSQL = new ExeSQL();
        String tCommisionSN = tstrExeSQL.getOneValue(strsql);
        if (tCommisionSN==null || tCommisionSN.equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAscriptionBL";
            tError.functionName = "checkData";
            tError.errorMessage = "原代理人"+mLAAscriptionSchema.getAgentOld()+"没有保单号为:"+mLAAscriptionSchema.getContNo()+"的保单";
            this.mErrors.addOneError(tError);
            return false;
         }

       }
       else
       {
         String strsql="select contno from lccont  where agentcode='"+mLAAscriptionSchema.getAgentOld()+"' and uwflag<>'a'";
         ExeSQL tstrExeSQL = new ExeSQL();
         String tCommisionSN = tstrExeSQL.getOneValue(strsql);
         if (tCommisionSN==null || tCommisionSN.equals(""))
         {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "LAAscriptionBL";
             tError.functionName = "checkData";
             tError.errorMessage = "原代理人"+mLAAscriptionSchema.getAgentOld()+"名下没有保单，不用做归属！";
             this.mErrors.addOneError(tError);
             return false;
          }

       }
     //本功能只实现

        return true;
    }
     /**
      * 准备往后层输出所需要的数据
      * 输出：如果准备数据时发生错误则返回false,否则返回true
      */
     private boolean submitquery()
    {
        return true;
    }

    private boolean prepareOutputData()
   {
       try
       {
           this.mInputData = new VData();
           this.mInputData.add(this.mGlobalInput);
           System.out.println("|||||||||||||||"+mLAAscriptionSchema.getBranchAttr());
           this.mInputData.add(mMap);

       }
       catch (Exception ex)
       {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "LAAscriptionBL";
           tError.functionName = "prepareData";
           tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
           this.mErrors.addOneError(tError);
           return false;
       }
       return true;
   }

   public VData getResult()
   {
       return this.mResult;
   }

}
