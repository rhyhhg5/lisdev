/*
 * <p>ClassName: ALACrossBL </p>
 * <p>Description: ALACrossBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-07-08
 */
package com.sinosoft.lis.agentdaily;
import com.sinosoft.lis.pubfun.*;//GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;
//import com.sinosoft.lis.schema.LAContSchema;
import com.sinosoft.lis.schema.LARateChargeSchema;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.vschema.LAContSet;
import com.sinosoft.lis.vschema.LARateChargeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.utility.SSRS;
import java.util.Date;
import  java.lang.Integer;

public class ALACrossBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 数据操作字符串 */
    private String mOperate;
    private String mValidStart;
    private String mValidEnd;
    String mSQL = "";
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mProtocalNo = "";
    /** 业务处理相关变量 */
    LAAcrossCommSchema mLAAcrossCommSchema   = new LAAcrossCommSchema();
    private LACommisionSchema mLACommisionSchema = new LACommisionSchema();
  //  private LAAcrossCommSchema mLAAcrossCommSchema=new LAAcrossCommSchema();
  //   private LAAcrossCommSet mLAAcrossCommSet=new LAAcrossCommSet();
    //private LAAuthorizeSet mLAAuthorizeSet = new LAAuthorizeSet();
    //  private LARateChargeSet mLARateChargeSet = new LARateChargeSet();
    public ALACrossBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public static void main(String[] args)
    {

       ALACrossBL BL = new ALACrossBL();
       GlobalInput tGlobalInput = new GlobalInput();
       tGlobalInput.ManageCom = "86";
      tGlobalInput.ComCode = "86";
        tGlobalInput.Operator = "001";
        VData tVData = new VData();
      LACommisionSchema aLACommisionSchema = new LACommisionSchema();
       String tValidStart="2005-02-11";
       String tValidEnd="2005-05-11";
        aLACommisionSchema.setManageCom("8611");
        aLACommisionSchema.setBranchType2("01");
       aLACommisionSchema.setBranchType("1");
        tVData.addElement(aLACommisionSchema);
        tVData.add(tValidStart);
        tVData.add(tValidEnd);
       tVData.add(tGlobalInput);
        BL.submitData(tVData, "QUERY||MAIN");
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALACrossBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAContBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("1111"+mOperate);
        if (this.mOperate.equals("QUERY||MAIN"))
       {
           this.submitquery();
       }
       else
         {
             System.out.println("Start ALACrossBL Submit...1111");
            ALACrossBLS tALACrossBLS = new ALACrossBLS();
            tALACrossBLS.submitData(mInputData, cOperate);
             System.out.println("End ALACrossBL Submit...");
             //如果有需要处理的错误，则返回
           if (tALACrossBLS.mErrors.needDealError())
           {
                // @@错误处理
                this.mErrors.copyAllErrors(tALACrossBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALACrossBL";
               tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
               return false;
           }
         }


       mInputData = null;
       return true;



    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();

        if (mOperate.equals("INSERT||MAIN"))
      {
          int i;

           String tAcrossSN=com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo("AcrossSN" ,20);
           this.mLAAcrossCommSchema.setoperator(mGlobalInput.Operator );
          this.mLAAcrossCommSchema.setAcrossSN(tAcrossSN);
          this.mLAAcrossCommSchema.setMakeDate(currentDate);
          this.mLAAcrossCommSchema.setMakeTime(currentTime);
          this.mLAAcrossCommSchema.setModifyDate(currentDate);
          this.mLAAcrossCommSchema.setModifyTime(currentTime);
      }
      if (mOperate.equals("UPDATE||MAIN"))
    {
        String sql="select AcrossSN from LAAcrossComm where preAgentCode='"+mLAAcrossCommSchema.getpreAgentCode()+"' and preContNo='"+mLAAcrossCommSchema.getpreContNo()+"' and AgentCode='"+mLAAcrossCommSchema.getAgentCode()+"' and ContNo='"+mLAAcrossCommSchema.getContNo()+"' and AppntIdNo='"+mLAAcrossCommSchema.getAppntIdNo()+"'";
         ExeSQL tExeSQL = new ExeSQL();
               String tAcrossSN = tExeSQL.getOneValue(sql);
               LAAcrossCommDB tLAAcrossCommDB = new LAAcrossCommDB();
           tLAAcrossCommDB.setAcrossSN(tAcrossSN);
           if (!tLAAcrossCommDB.getInfo())
           {
               // @@错误处理
               CError tError = new CError();
               tError.moduleName = "ALACrossBL";
               tError.functionName = "dealData";
               tError.errorMessage = "原交叉信息查询失败!";
               this.mErrors.addOneError(tError);
               return false;
           }

         this.mLAAcrossCommSchema.setAcrossSN(tAcrossSN);
         this.mLAAcrossCommSchema.setoperator(mGlobalInput.Operator );
        //this.mLAAcrossCommSchema.setMakeDate(currentDate);
        //this.mLAAcrossCommSchema.setMakeTime(currentTime);
        this.mLAAcrossCommSchema.setModifyDate(currentDate);
        this.mLAAcrossCommSchema.setModifyTime(currentTime);
    }
    if (mOperate.equals("DELETE||MAIN"))
 {
     String sql = "select AcrossSN from LAAcrossComm where preAgentCode='" +
                  mLAAcrossCommSchema.getpreAgentCode() +
                  "' and preContNo='" + mLAAcrossCommSchema.getpreContNo() +
                  "' and AgentCode='" + mLAAcrossCommSchema.getAgentCode() +
                  "' and ContNo='" + mLAAcrossCommSchema.getContNo() +
                  "' and AppntIdNo='" + mLAAcrossCommSchema.getAppntIdNo() +
                  "'";
     ExeSQL tExeSQL = new ExeSQL();
     String tAcrossSN = tExeSQL.getOneValue(sql);
     LAAcrossCommDB tLAAcrossCommDB = new LAAcrossCommDB();
     tLAAcrossCommDB.setAcrossSN(tAcrossSN);
     if (!tLAAcrossCommDB.getInfo()) {
         // @@错误处理
         CError tError = new CError();
         tError.moduleName = "ALACrossBL";
         tError.functionName = "dealData";
         tError.errorMessage = "原交叉信息查询失败!";
         this.mErrors.addOneError(tError);
         return false;
     }

     this.mLAAcrossCommSchema.setAcrossSN(tAcrossSN);
 }


          return true;
    }

    private void jbInit() throws Exception {
   }
   private boolean getInputData(VData cInputData)
   {
       this.mLACommisionSchema.setSchema((LACommisionSchema) cInputData.
                                           getObjectByObjectName(
               "LACommisionSchema", 0));
       this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName(
                                           "GlobalInput", 0));
       this.mLAAcrossCommSchema.setSchema((LAAcrossCommSchema) cInputData.
                                           getObjectByObjectName(
               "LAAcrossCommSchema", 0));

       mValidStart=(String) cInputData.get(1);
       mValidEnd=(String) cInputData.get(2);



       if (this.mGlobalInput == null)
       {
           CError tError = new CError();
           tError.moduleName = "ALACrossBL";
           tError.functionName = "getInputData";
           tError.errorMessage = "mGlobalInput为空！";
           this.mErrors.addOneError(tError);
           return false;
       }
       if (this.mLACommisionSchema == null)
       {
           CError tError = new CError();
           tError.moduleName = "ALACrossBL";
           tError.functionName = "getInputData";
           tError.errorMessage = "mLACommisionSchema为空！";
           this.mErrors.addOneError(tError);
           return false;
       }
       return true;
   }
   private boolean prepareOutputData()
   {
          try
         {
             this.mInputData = new VData();
             this.mInputData.add(this.mGlobalInput);
             this.mInputData.add(this.mLAAcrossCommSchema);
//             this.mInputData.add(this.mLARateChargeSet);
//             this.mInputData.add(this.mLAContSchema);
//             this.mInputData.add(this.mSQL);
         }
         catch (Exception ex)
         {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "ALACrossBL";
             tError.functionName = "prepareData";
             tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
             this.mErrors.addOneError(tError);
             return false;
         }

   return true;
   }

   private String submitquery()
   {
       String tReturn="";
       String tValidEnd="" ;
        String tValidStart="" ;
        String tSQL="";
        String tSQL1="";
        String tIDNo ="";
         String tSignDate="";
        String tRiskCode ="";
        SSRS tSSRS3 = new SSRS();
       LACommisionSchema tLACommisionSchema = new LACommisionSchema();
       LACommisionSet tLACommisionSet = new LACommisionSet();
       // LAAcrossCommSet tLAAcrossCommSet=new LAAcrossCommSet();
       tLACommisionSchema = this.mLACommisionSchema;
       tSQL = "select c.IDNo,a.SignDate,a.RiskCode,a.CommisionSN from LACommision a,LDPerson c where 1=1 and a.P12=c.CustomerNo";
          System.out.println("qqqqqqq"+tSQL);
       if(!mValidStart.trim().equals("") && mValidStart.trim() !=null)
       {
           System.out.println("qqqqqqq" + mValidStart);
           tValidStart = AgentPubFun.formatDate(mValidStart,
                   "yyyy-MM-dd");
            tSQL+=" and a.SignDate >= '"+ tValidStart + "'";
       }
       if(!mValidEnd.trim().equals("") && mValidEnd.trim() !=null)
       {
           System.out.println("qqqqqqq" + mValidEnd);
            tValidEnd = AgentPubFun.formatDate(mValidEnd, "yyyy-MM-dd");
            System.out.println("qqqqqqq"+tValidEnd);
            tSQL+=" and  a.SignDate<='" + tValidEnd + "'";
        }
         System.out.println("qqqqqq11122222"+tLACommisionSchema.getManageCom().trim());
       if(!tLACommisionSchema.getManageCom().trim().equals("")  && tLACommisionSchema.getManageCom().trim() !=null)
       {
           tSQL += " and a.ManageCom='" + tLACommisionSchema.getManageCom() +
                   "'";
       }
    SSRS tSSRS = new SSRS();
       ExeSQL tExeSQL = new ExeSQL();
       tSSRS = tExeSQL.execSQL(tSQL);
       int a= tSSRS.getMaxRow() ;
       if (tExeSQL.mErrors.needDealError())
       {
           this.mErrors.copyAllErrors(tExeSQL.mErrors);
           CError tError = new CError();
           tError.moduleName = "ALACrossBL";
           tError.functionName = "submitquery";
           tError.errorMessage = "查询直接佣金明细表的信息出错！";
           this.mErrors.addOneError(tError);
           return null;
       }
       if(a<1)
       {
           CError tError = new CError();
            tError.moduleName = "ALACrossBL";
            tError.functionName = "submitquery";
            tError.errorMessage = "查询直接佣金明细表的信息为空！";
            this.mErrors.addOneError(tError);
            return null;

       }
       System.out.println("tSQL:" + tSSRS.getMaxRow() + "|");

        if (tSSRS.getMaxRow() >0)
        {
            System.out.println("tSQL:1234556" + tSSRS.getMaxRow() + "|");

         for(int i=1;i<=tSSRS.getMaxRow();i++)
         {
           tIDNo = tSSRS.GetText(i, 1);
           tSignDate = tSSRS.GetText(i, 2);
           tRiskCode = tSSRS.GetText(i, 3);
           String tCommisionSN=tSSRS.GetText(i, 4);
             System.out.println("tSignDate:" + tSignDate + "|");
            System.out.println("tRiskCode:" + tRiskCode + "|");
            System.out.println("tCommisionSN:" + tCommisionSN + "|");
          tSQL ="";
            tSQL = "select a.SignDate,a.ModifyDate,a.ContNo from LBCont a,LBPol c where 1=1 and a.ContNo=c.ContNo";
          System.out.println("qqqqqqq"+tSQL);
           tSQL+=" and c.RiskCode='"+tRiskCode+"' and a.InsuredIDNo='"+tIDNo+"' ";
//           if(!tLACommisionSchema.getManageCom().trim().equals("")  && tLACommisionSchema.getManageCom().trim() !=null)
//       {
//           tSQL += " and a.ManageCom='" + tLACommisionSchema.getManageCom() +
//                   "'";
//       }

           SSRS tSSRS1 = new SSRS();
         ExeSQL tExeSQL1 = new ExeSQL();
               tSSRS1 = tExeSQL1.execSQL(tSQL);
                System.out.println("tSSRS1:" + tSSRS1.getMaxRow() + "|");
                int b=tSSRS1.getMaxRow();
       if (tExeSQL.mErrors.needDealError())
      {
          this.mErrors.copyAllErrors(tExeSQL.mErrors);
          CError tError = new CError();
          tError.moduleName = "ALACrossBL";
          tError.functionName = "submitquery";
          tError.errorMessage = "查询个人保单备份表的信息出错！";
          this.mErrors.addOneError(tError);
          return null;
      }
      if(b<1)
      {
          CError tError = new CError();
           tError.moduleName = "ALACrossBL";
           tError.functionName = "submitquery";
           tError.errorMessage = "查询个人保单备份表的信息为空！";
           this.mErrors.addOneError(tError);
           return null;

      }

                if (tSSRS1.getMaxRow() >0)
                {
                  for(int k=1;k<=tSSRS1.getMaxRow();k++)
                 {
                    String tSignDate1 = tSSRS1.GetText(k, 1);
                    String tModifyDate1 = tSSRS1.GetText(k, 2);
                    String tContNo  =tSSRS1.GetText(k, 3);
               String unit="D";
            //tDataNo为判断是否为自客户承保之日起180天内出现投保人退保
            // tSignDataNo为现保单签约时间减去原保单间约时间
            //tSignDateModify为判断是否为退
            int tSignDateModify=PubFun.calInterval(tModifyDate1,tSignDate,unit);
          int tSignDataNo=PubFun.calInterval(tSignDate1,tSignDate,unit);
          int tDataNo= PubFun.calInterval(tSignDate1,tModifyDate1,unit);
                    if (tDataNo<=180 && tSignDataNo<=180 && tSignDataNo>=0 && tSignDateModify>=0)
                    {
                     System.out.println("存在交叉业务员, 退保天数为"+tDataNo);

  String tSQL2="select a.PolNo,a.P11,a.AgentCode,(select b.Name from LAAgent b where b.AgentCode=a.AgentCode)";
             tSQL2+=",a.TransMoney,c.IDNo,c.Name,e.PolNo,d.Prem,e.AppntName,d.AgentCode";
               tSQL2+=",(select f.Name from LAAgent f where f.AgentCode=d.AgentCode),a.PayYears,a.RiskCode";
               tSQL2+=" from LACommision a,LDPerson c,LBCont d,LBPol e  where 1=1 and a.P12=c.CustomerNo";
               tSQL2+=" and a.CommisionSN='"+tCommisionSN+"' and d.ContNo=e.ContNo and d.ContNo='"+tContNo+"'";
    System.out.println("存在交叉业务员, 退保天数为tSQL2"+tSQL2);
               SSRS tSSRS2 = new SSRS();

        ExeSQL tExeSQL2 = new ExeSQL();
        tSSRS2 = tExeSQL2.execSQL(tSQL2);
        int d=tSSRS2.getMaxRow();
   if (tExeSQL.mErrors.needDealError())
   {
    this.mErrors.copyAllErrors(tExeSQL.mErrors);
    CError tError = new CError();
    tError.moduleName = "ALACrossBL";
    tError.functionName = "submitquery";
    tError.errorMessage = "查询个人保单备份表的信息出错！";
    this.mErrors.addOneError(tError);
    return null;
   }
   if(d<1)
    {
    CError tError = new CError();
    tError.moduleName = "ALACrossBL";
    tError.functionName = "submitquery";
    tError.errorMessage = "查询个人保单备份表的信息为空！";
    this.mErrors.addOneError(tError);
    return null;

   }
        if(k==1&&i==1)  //k=1&&i=1 表示第一次给tSSRS3付值
        {
        tSSRS3=tExeSQL2.execSQL(tSQL2);
        int c=tSSRS2.getMaxRow();
        }
          else //tSSRS3的值为加上tSSRS2的值
          {

              System.out.println("tSSRS2:" + tSSRS2.getMaxRow() + "|");


              tSSRS3.addRow(tSSRS2);
              int e = tSSRS3.getMaxRow();

              System.out.println("tSSRS3:" + tSSRS3.getMaxRow() + "|");
              System.out.println("存在交叉业务员, 退保天数为" + tSQL2);
          }
                    }
                }
              }
         }
        }
       this.mResult .clear() ;

       this.mResult .add(tSSRS3) ;
       int d=tSSRS3.getMaxRow();
     System.out.println("tSSRS3:" + tSSRS3.getMaxRow() + "|");
//       this.mResult .add(tLAAcrossCommSet) ;

return tReturn;

}
   public VData getResult()
      {
           System.out.println("tSSRS3:得到结果|");
          return mResult;
    }


}



