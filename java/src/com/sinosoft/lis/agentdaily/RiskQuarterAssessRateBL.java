package com.sinosoft.lis.agentdaily;


import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LADiscountBSchema;
import com.sinosoft.lis.schema.LADiscountSchema;
import com.sinosoft.lis.db.LADiscountDB;
import com.sinosoft.lis.vschema.LADiscountBSet;
import com.sinosoft.lis.vschema.LADiscountSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.StrTool;


public class RiskQuarterAssessRateBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    
    private String currentDate = PubFun.getCurrentDate();//当天日期
    
    private String currentTime = PubFun.getCurrentTime();//当时时间
    
    private LADiscountSet mLADiscountSet=null;
    
    private LADiscountBSet mLADiscountBSet=null;
    
    private String mOperate="";
    
    private MMap mMap=new MMap();
    
    public static void main(String[] args)
    {
    }
    public RiskQuarterAssessRateBL()
    {
    }

    private boolean getInputData(VData cInputData)
    {
        this.mErrors.clearErrors();
        try {
            mLADiscountSet = (LADiscountSet) cInputData.getObjectByObjectName("LADiscountSet", 0);
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
            if (mLADiscountSet == null || mGlobalInput == null)
            {
                CError tError = new CError();
                tError.moduleName = "RiskQuarterAssessRateBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有得到足够的信息！";
                this.mErrors.addOneError(tError);
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
          // @@错误处理
          CError.buildErr(this, "接收数据失败");
          return false;
        }
    }
    public boolean checkData()
    {
        if(!this.mOperate.equals("DELETE")){
            LADiscountSchema tempLADiscountSchema1;
            LADiscountSchema tempLADiscountSchema2;

            for (int i = 1; i <= this.mLADiscountSet.size(); i++) {
                tempLADiscountSchema1 = this.mLADiscountSet.get(i);
                for (int j = i + 1; j <= this.mLADiscountSet.size(); j++) {
                    tempLADiscountSchema2 = this.mLADiscountSet.get(j);
                    if ((tempLADiscountSchema1.getAgentGrade().equals(tempLADiscountSchema2.getAgentGrade()))
                        && (tempLADiscountSchema1.getRiskCode().equals(tempLADiscountSchema2.getRiskCode()))
                        && (tempLADiscountSchema1.getInsureYear().equals(tempLADiscountSchema2.getInsureYear()))
                        && (tempLADiscountSchema1.getBankType().equals(tempLADiscountSchema2.getBankType()))
                        && (tempLADiscountSchema1.getCode1()==(tempLADiscountSchema2.getCode1()))
                        && (tempLADiscountSchema1.getCode2()==(tempLADiscountSchema2.getCode2()))
                        && (tempLADiscountSchema1.getPayIntv().equals(tempLADiscountSchema2.getPayIntv()))
                        && (tempLADiscountSchema1.getCode4().equals(tempLADiscountSchema2.getCode4()))
                        && (tempLADiscountSchema1.getCode5().equals(tempLADiscountSchema2.getCode5()))) 
                    {
                        System.out.println("...........erro1");
                        CError tError = new CError();
                        tError.moduleName = "RiskQuarterAssessRateBL";
                        tError.functionName = "check()";
                        tError.errorMessage = "在验证操作数据时出错。在选中的数据中第" + i + "行与第" +
                                              j + "行数据重复。";
                        this.mErrors.clearErrors();
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
            }
        }
        return true;
    }
    public boolean submitData(VData cInputData, String cOperate) {
        this.mOperate=cOperate;
        if(!getInputData(cInputData))
        {
            return false;
        }
        if(!checkData())
        {
            return false;
        }
        if(!dealDate())
        {
              CError tError = new CError();
              tError.moduleName = "RiskQuarterAssessRateBL";
              tError.functionName = "submitData";
              tError.errorMessage = "数据处理失败-->dealData!";
              this.mErrors.addOneError(tError);
              return false;
        }
        if(!prepareOutputData())
        {
            return false;
        }
         System.out.println("Start RiskQuarterAssessRateBL Submit...");
         PubSubmit tPubSubmit = new PubSubmit();
         if (!tPubSubmit.submitData(mInputData, ""))
         {
             this.mErrors.copyAllErrors(tPubSubmit.mErrors);
             CError tError = new CError();
             tError.moduleName = "RiskQuarterAssessRateBL";
             tError.functionName = "submitData";
             tError.errorMessage = "PubSubmit保存数据失败!";
             this.mErrors.addOneError(tError);
             return false;
         }
        return true;
    }
    /**
     * 验证录入的信息
     * @param cLAWageLogSchema
     * @return
     */
    public boolean checkDiscount(LADiscountSchema tLADiscountSchema)
    {
        if(tLADiscountSchema==null)
        {
            return false;
        }
        if(this.mOperate.equals("INSERT"))
        {
        	String sql = " select count(1) from LADiscount where discounttype = '" + tLADiscountSchema.getDiscountType()+ "' "
 		            +" and managecom = '" + tLADiscountSchema.getManageCom() + "' "
 	             	+" and agentgrade = '"+ tLADiscountSchema.getAgentGrade() + "' "
                    +" and insureyear = '"+ tLADiscountSchema.getInsureYear() + "' "
                    +" and banktype = '"+ tLADiscountSchema.getBankType() + "' "
 		            +" and riskcode = '" + tLADiscountSchema.getRiskCode() + "'"
 		            +" and code1 = " + tLADiscountSchema.getCode1() + ""
 		            +" and code2 = " + tLADiscountSchema.getCode2() + ""
 		            +" and payintv = '"+tLADiscountSchema.getPayIntv()+"'"
 		            +" and code4 = '"+tLADiscountSchema.getCode4()+"'"
 		            +" and code5 = '"+tLADiscountSchema.getCode5()+"'"
 		            +" with ur";
           SSRS tSSRS=new ExeSQL().execSQL(sql);
           if(!tSSRS.GetText(1,1).equals("0"))
           {
             String info = "录入的职级：" + tLADiscountSchema.getAgentGrade() + "，"
 	                     + "险种：" + tLADiscountSchema.getRiskCode() + "，信息在系统中已经存在，请重新录入！";
             CError.buildErr(this,info);
             return false;
           }
        }
        else if(this.mOperate.equals("UPDATE"))
        {
        	 String msql = "select count(1) from LADiscount where discounttype = '" + tLADiscountSchema.getDiscountType()+ "' "
             +" and managecom = '" + tLADiscountSchema.getManageCom() + "' "
             +" and agentgrade = '"+ tLADiscountSchema.getAgentGrade() + "' "
             +" and insureyear = '"+ tLADiscountSchema.getInsureYear() + "' "
             +" and banktype = '"+ tLADiscountSchema.getBankType() + "' "
             +" and riskcode = '" + tLADiscountSchema.getRiskCode() + "' "
             +" and code1 = " + tLADiscountSchema.getCode1() + ""
             +" and code2 = " + tLADiscountSchema.getCode2() + ""
		     +" and payintv = '"+tLADiscountSchema.getPayIntv()+"'"
 		     +" and code4 = '"+tLADiscountSchema.getCode4()+"'"
 		     +" and code5 = '"+tLADiscountSchema.getCode5()+"'"
             +" and idx<>"+tLADiscountSchema.getIdx()+" with ur";
        SSRS mtSSRS=new ExeSQL().execSQL(msql);
        if(!mtSSRS.GetText(1,1).equals("0"))
        {
          String info = "修改后主管间接绩效提奖信息系统已存在！";
          CError.buildErr(this,info);
          return false;
        }
//         String sql = "select count(1) from LADiscount where discounttype = '" + tLADiscountSchema.getDiscountType()+ "' "
// 		            +" and managecom = '" + tLADiscountSchema.getManageCom() + "' "
// 	             	+" and agentgrade = '"+ tLADiscountSchema.getAgentGrade() + "' "
//                    +" and insureyear = '"+ tLADiscountSchema.getInsureYear() + "' "
//                    +" and banktype = '"+ tLADiscountSchema.getBankType() + "' "
// 		            +" and riskcode = '" + tLADiscountSchema.getRiskCode() + "'"
// 		            +" and code1 = " + tLADiscountSchema.getCode1() + ""
// 		            +" and code2 = " + tLADiscountSchema.getCode2() + ""
// 				    +" and payintv = '"+tLADiscountSchema.getPayIntv()+"'"
// 		 		    +" and code4 = '"+tLADiscountSchema.getCode4()+"'"
// 		 		    +" and code5 = '"+tLADiscountSchema.getCode5()+"'"
// 		            +"  with ur";
//           SSRS tSSRS=new ExeSQL().execSQL(sql);
//           if(tSSRS.GetText(1,1).equals("0"))
//           {
//             String info = "修改的职级：" + tLADiscountSchema.getAgentGrade() + "，"
// 	                     + "险种：" + tLADiscountSchema.getRiskCode() + "，信息在系统中不存在，请先录入！";
//             CError.buildErr(this,info);
//             return false;
//           }
  

        }
        return true;
    }
    /**
     * 获得主键信息
     * @param cLAWageLogSchema
     * @return
     */
    public int getIDX()
    {
        String sql="select max(idx)+1 from LADiscount where 1=1 ";
        SSRS tSSRS=new ExeSQL().execSQL(sql);  
        String idx=tSSRS.GetText(1,1);
        System.out.println(idx);
        if(idx!=null && !idx.equals("null"))
        {
            if(StrTool.cTrim(idx).equals("0"))
            {
                return 1;
            }
            return Integer.parseInt(idx);
        }
        else
        {
            return 1;
        }
    }

    public boolean dealDate()
    {
        LADiscountSchema tLADiscountBSchema = new LADiscountSchema();
        LADiscountSet tLADiscountSet = new LADiscountSet();
        LADiscountSet tLADiscountBSet = new LADiscountSet();
        if(this.mOperate.equals("INSERT"))
        {
            int tIDX=getIDX();
            for(int i=1;i<=this.mLADiscountSet.size();i++)
            {
                LADiscountSchema tLADiscountSchema=mLADiscountSet.get(i);
                if(tLADiscountSchema.getIdx()==-1)
                {
                    tLADiscountSchema.setMakeDate(this.currentDate);
                    tLADiscountSchema.setMakeTime(this.currentTime);
                    tLADiscountSchema.setModifyDate(this.currentDate);
                    tLADiscountSchema.setModifyTime(this.currentTime);
                    tLADiscountSchema.setOperator(mGlobalInput.Operator);
                    tLADiscountSchema.setDiscountType("06");
                    if(!checkDiscount(tLADiscountSchema))
                    {
                        return false;
                    }
                    tLADiscountSchema.setIdx(tIDX);
                    tIDX++;
                    tLADiscountSet.add(tLADiscountSchema);
                }
            }
            this.mMap.put(tLADiscountSet, this.mOperate);
        }
        else if(this.mOperate.equals("UPDATE"))
        {
            for(int i=1;i<=this.mLADiscountSet.size();i++)
            {
                LADiscountSchema tLADiscountSchema=mLADiscountSet.get(i);
                if(tLADiscountSchema.getIdx()!=-1)
                {
                    LADiscountDB tLADiscountDB=new LADiscountDB();
                    tLADiscountDB.setIdx(tLADiscountSchema.getIdx());
                    tLADiscountBSchema=tLADiscountDB.query().get(1);
                    tLADiscountBSet.add(tLADiscountBSchema);

                    tLADiscountSchema.setMakeDate(this.currentDate);
                    tLADiscountSchema.setMakeTime(this.currentTime);
                    tLADiscountSchema.setModifyDate(this.currentDate);
                    tLADiscountSchema.setModifyTime(this.currentTime);
                    tLADiscountSchema.setOperator(mGlobalInput.Operator);
                    tLADiscountSchema.setDiscountType("06");
                    tLADiscountSchema.setIdx(tLADiscountSchema.getIdx());
                    if(!checkDiscount(tLADiscountSchema))
                    {
                        return false;
                    }
                    tLADiscountSet.add(tLADiscountSchema);
                }
            }
//          数据备份
            if(!backupLADiscount(tLADiscountBSet))
            {
                return false;
            }
            this.mMap.put(tLADiscountSet, this.mOperate);
        }
        else if(this.mOperate.equals("DELETE"))
        {
            for(int i=1;i<=this.mLADiscountSet.size();i++)
            {
                LADiscountSchema tLADiscountSchema=mLADiscountSet.get(i);
                if(tLADiscountSchema.getIdx()!=-1)
                {
                    LADiscountDB tLADiscountDB=new LADiscountDB();
                    tLADiscountDB.setIdx(tLADiscountSchema.getIdx());
                    tLADiscountBSchema=tLADiscountDB.query().get(1);
                    tLADiscountBSet.add(tLADiscountBSchema);
                    
                    tLADiscountSchema.setMakeDate(this.currentDate);
                    tLADiscountSchema.setMakeTime(this.currentTime);
                    tLADiscountSchema.setModifyDate(this.currentDate);
                    tLADiscountSchema.setModifyTime(this.currentTime);
                    tLADiscountSchema.setOperator(mGlobalInput.Operator);
                    tLADiscountSchema.setDiscountType("06");
                    tLADiscountSchema.setIdx(tLADiscountSchema.getIdx());
                    tLADiscountSet.add(tLADiscountSchema);
                }
            }
//          数据备份
            if(!backupLADiscount(tLADiscountBSet))
            {
                return false;
            }
            this.mMap.put(tLADiscountSet, this.mOperate);
        }
        return true;
    }
    /**
     * 提数日志表备份
     * @param cLAWageLogSchema
     * @return
     */
    public boolean backupLADiscount(LADiscountSet cLADiscountSet)
    {
        if(cLADiscountSet==null)
        {
            CError.buildErr(this,"备份信息失败！");
            return false;
        }
        //循环备份
        mLADiscountBSet=new LADiscountBSet();
        for(int i=1;i<=cLADiscountSet.size();i++)
        {
            LADiscountSchema tLADiscountSchema = new LADiscountSchema();
            LADiscountBSchema tLADiscountBSchema = new LADiscountBSchema();
            Reflections tReflections=new Reflections();
            tLADiscountSchema = cLADiscountSet.get(i);
            tReflections.transFields(tLADiscountBSchema,tLADiscountSchema);
            tLADiscountBSchema.setMakeDate(currentDate);
            tLADiscountBSchema.setMakeTime(currentTime);
            tLADiscountBSchema.setOperator(this.mGlobalInput.Operator);
            tLADiscountBSchema.setEdorType("22");
            tLADiscountBSchema.setEdorNo(PubFun1.CreateMaxNo("EdorNo", 20));
            this.mLADiscountBSet.add(tLADiscountBSchema);
        }
        mMap.put(mLADiscountBSet, "INSERT");
        return true;
    }

    public boolean prepareOutputData()
    {
         try
         {
             mInputData = new VData();
             mInputData.add(mMap);
         }
         catch(Exception e)
         {
             CError.buildErr(this,"提交数据失败！");
             return false;
         }
         return true;
    }
}

