package com.sinosoft.lis.agentdaily;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Alsa
 * @version 1.1
 */
public class DiskImportAddPerUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private DiskImportAddPerBL mDiskImportAddPerBL = null;

    public DiskImportAddPerUI()
    {
        mDiskImportAddPerBL = new DiskImportAddPerBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mDiskImportAddPerBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mDiskImportAddPerBL.mErrors);
            return false;
        }
        //
        if(mDiskImportAddPerBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mDiskImportAddPerBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mDiskImportAddPerBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mDiskImportAddPerBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
        DiskImportAddPerUI zDiskImportAddPerUI = new DiskImportAddPerUI();
    }
}
