/*
 * <p>ClassName: OLAQualificationBLS </p>
 * <p>Description: OLAQualificationBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-21 15:48:13
 */
package com.sinosoft.lis.agentdaily;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;
 public class OLAQualificationBLS {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
//传输数据类
  private VData mInputData ;
 /** 数据操作字符串 */
private String mOperate;
public OLAQualificationBLS() {
	}
public static void main(String[] args) {
}
 /**
 传输数据的公共方法
*/
public boolean submitData(VData cInputData,String cOperate)
{
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
	  mInputData=(VData)cInputData.clone();
    if(this.mOperate.equals("INSERT||MAIN"))
    {if (!saveLAQualification())
        return false;
    }
    if (this.mOperate.equals("DELETE||MAIN"))
    {if (!deleteLAQualification())
        return false;
    }
    if (this.mOperate.equals("UPDATE||MAIN"))
    {if (!updateLAQualification())
        return false;
    }
  return true;
}
 /**
* 保存函数
*/
private boolean saveLAQualification()
{
        LAQualificationSchema tLAQualificationSchema = new LAQualificationSchema();
        tLAQualificationSchema = (LAQualificationSchema) mInputData.getObjectByObjectName("LAQualificationSchema",1);
        
       // LAQualificationSet tLAQualificationSet = new LAQualificationSet();
       // tLAQualificationSet = (LAQualificationSet) mInputData.getObjectByObjectName("LAQualificationSet", 0);
        
        LAAgentSet tLAAgentSet = new LAAgentSet();
        tLAAgentSet = (LAAgentSet) mInputData.getObjectByObjectName("LAAgentSet", 0);
        
        // tianjia  2012-6-5 将多余数据存到B表中
        LAQualificationBSet tLAQualificationBSet = new LAQualificationBSet();
        tLAQualificationBSet = (LAQualificationBSet)mInputData.getObjectByObjectName("LAQualificationBSet", 0);
        
        LAQualificationSet delLAQualificationSet = new LAQualificationSet();
        delLAQualificationSet = (LAQualificationSet) mInputData.getObjectByObjectName("LAQualificationSet", 1);
        
        
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAQualificationBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            LAQualificationDB tLAQualificationDB = new LAQualificationDB(conn);
            tLAQualificationDB.setSchema(tLAQualificationSchema);
            if (!tLAQualificationDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAQualificationDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLAQualificationBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
             if (tLAAgentSet!=null && tLAAgentSet.size() >0)
          {
              LAAgentDBSet tLAAgentDBSet = new LAAgentDBSet(conn);
              tLAAgentDBSet.add(tLAAgentSet);

              if (!tLAAgentDBSet.update())
              {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLAAgentDBSet.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "OLAQualificationBLS";
                  tError.functionName = "saveData";
                  tError.errorMessage = "数据保存失败!";
                  this.mErrors.addOneError(tError);
                  conn.rollback();
                  conn.close();
                  return false;
              }
          }
//            if (tLAQualificationSet!=null && tLAQualificationSet.size() >0)
//            {
//                LAQualificationDBSet tLAQualificationDBSet = new
//                        LAQualificationDBSet(conn);
//                tLAQualificationDBSet.add(tLAQualificationSet);
//                if (!tLAQualificationDBSet.update())
//                {
//                    // @@错误处理
//                    this.mErrors.copyAllErrors(tLAQualificationDBSet.mErrors);
//                    CError tError = new CError();
//                    tError.moduleName = "OLAQualificationBLS";
//                    tError.functionName = "saveData";
//                    tError.errorMessage = "数据保存失败!";
//                    this.mErrors.addOneError(tError);
//                    conn.rollback();
//                    conn.close();
//                    return false;
//                }
//            }
            // tianjia 2012-6-5 
            if(tLAQualificationBSet!=null&& tLAQualificationBSet.size()>0)
            {
            	LAQualificationBDBSet tLAQualificationBDBSet = new LAQualificationBDBSet(conn);
            	tLAQualificationBDBSet.add(tLAQualificationBSet);
            	 if (!tLAQualificationBDBSet.insert())
                 {
                     // @@错误处理
                     this.mErrors.copyAllErrors(tLAQualificationBDBSet.mErrors);
                     CError tError = new CError();
                     tError.moduleName = "OLAQualificationBLS";
                     tError.functionName = "saveData";
                     tError.errorMessage = "数据保存失败!";
                     this.mErrors.addOneError(tError);
                     conn.rollback();
                     conn.close();
                     return false;
                 }
            }
            if (delLAQualificationSet!=null && delLAQualificationSet.size() >0)
            {
                LAQualificationDBSet del_LAQualificationDBSet = new LAQualificationDBSet(conn);
                del_LAQualificationDBSet.add(delLAQualificationSet);
                
                if (!del_LAQualificationDBSet.delete())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(del_LAQualificationDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "OLAQualificationBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            conn.commit();
            conn.close();
    }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAQualificationBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
}
    /**
    * 保存函数
    */
    private boolean deleteLAQualification()
    {
        LAQualificationSchema tLAQualificationSchema = new LAQualificationSchema();
        tLAQualificationSchema = (LAQualificationSchema)mInputData.getObjectByObjectName("LAQualificationSchema",1);
        System.out.println("Start Save...");
        Connection conn;
        conn=null;
        conn=DBConnPool.getConnection();
        if (conn==null)
        {
		// @@错误处理
		CError tError = new CError();
           tError.moduleName = "OLAQualificationBLS";
           tError.functionName = "saveData";
           tError.errorMessage = "数据库连接失败!";
           this.mErrors .addOneError(tError) ;
           return false;
        }
        try{
           conn.setAutoCommit(false);
           System.out.println("Start 保存...");
           LAQualificationDB tLAQualificationDB=new LAQualificationDB(conn);
           tLAQualificationDB.setSchema(tLAQualificationSchema);
           if (!tLAQualificationDB.delete())
           {
		// @@错误处理
		    this.mErrors.copyAllErrors(tLAQualificationDB.mErrors);
 		    CError tError = new CError();
		    tError.moduleName = "OLAQualificationBLS";
		    tError.functionName = "saveData";
		    tError.errorMessage = "数据删除失败!";
		    this.mErrors .addOneError(tError) ;
               conn.rollback();
               conn.close();
               return false;
           }
               conn.commit() ;
               conn.close();
         }
       catch (Exception ex)
       {
      // @@错误处理
          CError tError =new CError();
          tError.moduleName="OLAQualificationBLS";
          tError.functionName="submitData";
          tError.errorMessage=ex.toString();
          this.mErrors .addOneError(tError);
          try{conn.rollback() ;
          conn.close();} catch(Exception e){}
         return false;
         }
         return true;
}
/**
  * 保存函数
*/
private boolean updateLAQualification()
{
      LAQualificationSchema tupLAQualificationSchema = new LAQualificationSchema();
      LAQualificationSchema tLAQualificationSchema = new LAQualificationSchema();
      
      tupLAQualificationSchema = (LAQualificationSchema) mInputData.getObjectByObjectName("LAQualificationSchema",0);//删除
      tLAQualificationSchema = (LAQualificationSchema) mInputData.getObjectByObjectName("LAQualificationSchema",1); //修改的数据
      
      LAQualificationSet tLAQualificationSet = new LAQualificationSet();
      tLAQualificationSet = (LAQualificationSet) mInputData.getObjectByObjectName("LAQualificationSet",0);
      LAAgentSet tLAAgentSet = new LAAgentSet();
      tLAAgentSet = (LAAgentSet) mInputData.getObjectByObjectName(
               "LAAgentSet", 0);
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "OLAQualificationBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");

            LAQualificationDB tupLAQualificationDB=new LAQualificationDB(conn);
            tupLAQualificationDB.setSchema(tupLAQualificationSchema);
            if (!tupLAQualificationDB.delete())
            {
                 // @@错误处理
                     this.mErrors.copyAllErrors(tupLAQualificationDB.mErrors);
                     CError tError = new CError();
                     tError.moduleName = "OLAQualificationBLS";
                     tError.functionName = "saveData";
                     tError.errorMessage = "数据删除失败!";
                     this.mErrors .addOneError(tError) ;
                conn.rollback();
                conn.close();
                return false;
           }
           LAQualificationDB tLAQualificationDB = new LAQualificationDB(conn);
           tLAQualificationDB.setSchema(tLAQualificationSchema);
            if (!tLAQualificationDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAQualificationDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLAQualificationBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            if (tLAQualificationSet!=null && tLAQualificationSet.size() >0)
           {
               LAQualificationDBSet tLAQualificationDBSet = new
                       LAQualificationDBSet(conn);
               tLAQualificationDBSet.add(tLAQualificationSet);
               if (!tLAQualificationDBSet.update())
               {
                   // @@错误处理
                   this.mErrors.copyAllErrors(tLAQualificationDBSet.mErrors);
                   CError tError = new CError();
                   tError.moduleName = "OLAQualificationBLS";
                   tError.functionName = "saveData";
                   tError.errorMessage = "数据保存失败!";
                   this.mErrors.addOneError(tError);
                   conn.rollback();
                   conn.close();
                   return false;
               }
           }
           if (tLAAgentSet!=null && tLAAgentSet.size() >0)
         {
             LAAgentDBSet tLAAgentDBSet = new LAAgentDBSet(conn);
             tLAAgentDBSet.add(tLAAgentSet);

             if (!tLAAgentDBSet.update())
             {
                 // @@错误处理
                 this.mErrors.copyAllErrors(tLAAgentDBSet.mErrors);
                 CError tError = new CError();
                 tError.moduleName = "OLACertificationBLS";
                 tError.functionName = "saveData";
                 tError.errorMessage = "数据保存失败!";
                 this.mErrors.addOneError(tError);
                 conn.rollback();
                 conn.close();
                 return false;
             }
         }


            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAQualificationBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;

     }
}
