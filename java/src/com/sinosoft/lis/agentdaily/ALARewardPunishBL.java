/*
 * <p>ClassName: ALARewardPunishBL </p>
 * <p>Description: ALARewardPunishBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LARewardPunishDB;
import com.sinosoft.lis.db.LMCheckFieldDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubCheckField;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LARewardPunishSchema;
import com.sinosoft.lis.vschema.LARewardPunishSet;
import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ALARewardPunishBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LARewardPunishSchema mLARewardPunishSchema = new
            LARewardPunishSchema();
    private LARewardPunishSet mLARewardPunishSet = new LARewardPunishSet();
    private String mDoneDate = "";
    private String mManageCom = "";
    public ALARewardPunishBL()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!check())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALARewardPunishBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALARewardPunishBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start ALARewardPunishBL Submit...");
            ALARewardPunishBLS tALARewardPunishBLS = new ALARewardPunishBLS();
            tALARewardPunishBLS.submitData(mInputData, cOperate);
            System.out.println("End ALARewardPunishBL Submit...");
            //如果有需要处理的错误，则返回
            if (tALARewardPunishBLS.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tALARewardPunishBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALARewardPunishBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }


    private boolean check() //已经审核过的佣金不能删除
    {
        String doneDate = AgentPubFun.formatDate(this.mDoneDate, "yyyy-MM-dd");
        String day = doneDate.substring(8); //从日期位取起
        /*if (!day.equals("01"))
        {
            CError tError = new CError();
            tError.moduleName = "ALARewardPunishBL";
            tError.functionName = "check";
            tError.errorMessage = "执行日期必须从一号开始!";
            this.mErrors.addOneError(tError);
            return false;
        }*/
        String tAgentCode = this.mLARewardPunishSchema.getAgentCode();
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(tAgentCode);
        if (!tLAAgentDB.getInfo())
        {
            this.mErrors.copyAllErrors(tLAAgentDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALARewardPunishBL";
            tError.functionName = "check";
            tError.errorMessage = "查询不到代理人" + tAgentCode + "的信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tAgentState = tLAAgentDB.getAgentState();
        if (tAgentState != null && tAgentState.compareTo("06") > 0) //离职人员
        {
            String OutWorkDate = tLAAgentDB.getOutWorkDate();
            String yearmonth = AgentPubFun.formatDate(mDoneDate, "yyyyMM");
            String sql =
                    "select startdate,enddate from lastatsegment where stattype='5' and yearmonth='" +
                    yearmonth + "'";
            ExeSQL aExeSQL = new ExeSQL();
            SSRS aSSRS = new SSRS();
            aSSRS = aExeSQL.execSQL(sql);
            String startdate = aSSRS.GetText(1, 1);
            String enddate = aSSRS.GetText(1, 2);
            if (OutWorkDate.compareTo(startdate) < 0)
            {
                CError tError = new CError();
                tError.moduleName = "ALARewardPunishBL";
                tError.functionName = "check";
                tError.errorMessage = "代理人" + tAgentCode + "在" + OutWorkDate +
                                      "已经离职，不能做" + yearmonth + "的加扣款!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        PubCheckField checkField1 = new PubCheckField();
        VData cInputData = new VData();
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("IndexCalNo",
                                      AgentPubFun.formatDate(mDoneDate,
                "yyyyMM"));
        tTransferData.setNameAndValue("AgentCode", tAgentCode);
        //通过 CKBYSET 方式校验
        LMCheckFieldDB tLMCheckFieldDB = new LMCheckFieldDB();
        LMCheckFieldSet tLMCheckFieldSet = new LMCheckFieldSet();
        String tSql = "select * from lmcheckfield where riskcode = '000000'"
                      +
                      " and fieldname = 'ALARewardPunishBL' order by serialno asc";
        tLMCheckFieldSet = tLMCheckFieldDB.executeQuery(tSql);
        cInputData.add(tTransferData);
        cInputData.add(tLMCheckFieldSet);
        if (!checkField1.submitData(cInputData, "CKBYSET"))
        {
            System.out.println("Enter Error Field!");
            //此判断是用来区分是程序处理过程中报的错误，还是校验时报的错误

            if (checkField1.mErrors.needDealError()) //程序错误处理
            {

                this.mErrors.copyAllErrors(checkField1.mErrors);
                return false;
            }
            else
            {
                VData t = checkField1.getResultMess(); //校验错误处理
                buildError("dealData", t.get(0).toString());
                return false;
            }
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();

        if (this.mOperate.equals("INSERT||MAIN"))
        {
            //确定记录顺序号
            String tAgentCode = this.mLARewardPunishSchema.getAgentCode().trim();
            String tCount;
            int i;

            String tSQL =
                    "select MAX(Idx) from LARewardPunish where AgentCode = '"
                    + tAgentCode + "'";
            ExeSQL tExeSQL = new ExeSQL();
            tCount = tExeSQL.getOneValue(tSQL);
            if (tCount != null && !tCount.equals(""))
            {

                Integer tInteger = new Integer(tCount);
                i = tInteger.intValue();
                i = i + 1;
            }
            else
            {
                i = 1;
            }
            this.mLARewardPunishSchema.setIdx(i);
            this.mLARewardPunishSchema.setMakeDate(currentDate);
            this.mLARewardPunishSchema.setMakeTime(currentTime);
            this.mLARewardPunishSchema.setModifyDate(currentDate);
            this.mLARewardPunishSchema.setModifyTime(currentTime);
        }
        else
        if (this.mOperate.equals("UPDATE||MAIN"))
        {

            LARewardPunishDB tLARewardPunishDB = new LARewardPunishDB();
            tLARewardPunishDB.setAgentCode(mLARewardPunishSchema.getAgentCode());
            tLARewardPunishDB.setIdx(mLARewardPunishSchema.getIdx());
            if (!tLARewardPunishDB.getInfo())
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ALARewardPunish";
                tError.functionName = "dealData";
                tError.errorMessage = "原奖惩信息查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            this.mLARewardPunishSchema.setMakeDate(tLARewardPunishDB.
                    getMakeDate());
            this.mLARewardPunishSchema.setMakeTime(tLARewardPunishDB.
                    getMakeTime());
            this.mLARewardPunishSchema.setModifyDate(currentDate);
            this.mLARewardPunishSchema.setModifyTime(currentTime);
            this.mLARewardPunishSchema.setOperator(this.mGlobalInput.Operator);
        }
        tReturn = true;
        return tReturn;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLARewardPunishSchema.setSchema((LARewardPunishSchema) cInputData.
                                             getObjectByObjectName(
                "LARewardPunishSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mDoneDate = (String) cInputData.get(1);
        this.mManageCom = (String) cInputData.get(2);
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        System.out.println("Start ALARewardPunishBLQuery Submit...");
        LARewardPunishDB tLARewardPunishDB = new LARewardPunishDB();
        tLARewardPunishDB.setSchema(this.mLARewardPunishSchema);
        this.mLARewardPunishSet = tLARewardPunishDB.query();
        this.mResult.add(this.mLARewardPunishSet);
        System.out.println("End ALARewardPunishBLQuery Submit...");
        //如果有需要处理的错误，则返回
        if (tLARewardPunishDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLARewardPunishDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALARewardPunishBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }


    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLARewardPunishSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALARewardPunishBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "ALARewardPunishBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}
