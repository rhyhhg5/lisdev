package com.sinosoft.lis.agentdaily;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 契约系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author zhousp
 * @version 1.0
 */

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAAddReManageDownUI
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    public LAAddReManageDownUI()
    {
        System.out.println("LAAddReManageDownUI");
    }

    public boolean submitData(VData cInputData, String cOperator)
    {
       LAAddReManageDownBL bl = new LAAddReManageDownBL();
        if(!bl.submitData(cInputData, cOperator))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        LAAddReManageDownUI tLAAddReManageDownUI = new   LAAddReManageDownUI();
    }
}
