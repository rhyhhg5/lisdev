/*
 * <p>ClassName: ALARewardPunishBL </p>
 * <p>Description: ALARewardPunishBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentdaily;

//import com.sinosoft.lis.db.LAAgentDB;

import com.sinosoft.lis.db.LAActiveMarkDB;
import com.sinosoft.lis.vdb.LAActiveMarkDBSet;
import com.sinosoft.lis.pubfun.*;

import com.sinosoft.lis.schema.LAActiveMarkSchema;
import com.sinosoft.lis.vschema.LAActiveMarkSet;
//import com.sinosoft.lis.vschema.LMCheckFieldSet;
import com.sinosoft.utility.*;
//import com.sinosoft.lis.vschema.LAAgentSet;
//import java.util.ArrayList;

public class ALAActivManageBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap mMap = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    private LAActiveMarkSchema mLAActiveMarkSchema = new LAActiveMarkSchema();

    private LAActiveMarkSchema tLAActiveMarkSchema = new LAActiveMarkSchema();

    private LAActiveMarkSet mLAActiveMarkSet = new LAActiveMarkSet();

    private LAActiveMarkSet tLAActiveMarkSet = new LAActiveMarkSet();

    private String mName = "";
    private String mBranchAttr = "";
    private String mAgentGroup = "";
    private String mManageCom = "";


    public ALAActivManageBL() {}

    public static void main(String[] args)
    { }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据 cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 将操作数据拷贝到本类中
        this.mOperate = cOperate;

        // 得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        // 进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAActivManageBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败ALAActivManageBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        PubSubmit t = new PubSubmit();
         t.submitData(mInputData, "");
        // 准备往后台的数据
        if(!prepareOutputData()) {
                return false;
        }


            if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        } else
        {}
        mInputData = null;
        return true;
    }



    /**
     * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
       // String tRewardPunishType = ""; //奖励类型

       if (this.mOperate.equals("INSERT||MAIN"))
        {
         String tAgentCode = this.mLAActiveMarkSchema.getAgentCode();
         String tAssessYM = this.mLAActiveMarkSchema.getAssessYM();

         // 判断设定日期是否大于当日
         if (this.mLAActiveMarkSchema.getDoneDate().compareTo(currentDate) > 0)
        {
            CError tError = new CError();
             tError.moduleName = "ALABKPresenceBL";
             tError.functionName = "dealData";
             tError.errorMessage = "所设置的执行日期不能大于今日！";
             this.mErrors.addOneError(tError);
             return false;
        }

         // 确定记录顺序号
          String tSQL = "select count(Idx) from LAActiveMark where AgentCode = '" + tAgentCode + "' and AssessYM='"+tAssessYM+"'";
           ExeSQL tExeSQL = new ExeSQL();
           String tCount;
            int i;
            tCount = tExeSQL.getOneValue(tSQL);
            if (tCount != null && !tCount.equals(""))
             {

              Integer tInteger = new Integer(tCount);
              i = tInteger.intValue();
               i = i + 1;
              }
                else
                {
                i = 1;
                }
              System.out.println(i);
               this.mLAActiveMarkSchema.setIdx(i+"");


               this.mLAActiveMarkSchema.setMakeDate(currentDate);
               this.mLAActiveMarkSchema.setMakeTime(currentTime);
               this.mLAActiveMarkSchema.setModifyDate(currentDate);
               this.mLAActiveMarkSchema.setModifyTime(currentTime);
               this.mLAActiveMarkSchema.setOperator(this.mGlobalInput.
                            Operator);
               System.out.println("nnnnnnnnnnnnnnnn"+this.mGlobalInput.
                            Operator);



            mMap.put(mLAActiveMarkSchema, "INSERT");

            mInputData.add(mMap);
        }



        if(this.mOperate.equals("UPDATE||MAIN")) {
            LAActiveMarkDB tLAActiveMarkDB = new LAActiveMarkDB();
            //LARewardPInfoSet tLARewardPInfoSet = new LARewardPInfoSet();

             tLAActiveMarkDB.setAgentCode(this.mLAActiveMarkSchema.getAgentCode());
             tLAActiveMarkDB.setBranchType(this.mLAActiveMarkSchema.getBranchType());
             tLAActiveMarkDB.setBranchType2(this.mLAActiveMarkSchema.getBranchType2());
             tLAActiveMarkDB.setAssessYM(this.mLAActiveMarkSchema.getAssessYM());
             tLAActiveMarkDB.setIdx(this.mLAActiveMarkSchema.getIdx());
            // System.out.println(this.mLAActiveMarkSchema.getAgentCode());
            // System.out.println("ooooooooooo"+this.mLAActiveMarkSchema.getIdx());

              if(!tLAActiveMarkDB.getInfo()) {
//        	 @@错误处理
                  this.mErrors.copyAllErrors(tLAActiveMarkDB.mErrors);
                  CError tError = new CError();
                   tError.moduleName = "LAActivManageBL";
                   tError.functionName = "submitData";
                   tError.errorMessage = "数据提交失败!查询后的业务员代码不可修改!";
                   this.mErrors.addOneError(tError);
                   return false;
              }
           tLAActiveMarkSchema = tLAActiveMarkDB.getSchema();
//           tLAActiveMarkSchema.setAgentCode(this.mLAActiveMarkSchema.getAgentCode());
           tLAActiveMarkSchema.setMarkType(this.mLAActiveMarkSchema.getMarkType());
           tLAActiveMarkSchema.setAssessMark(this.mLAActiveMarkSchema.getAssessMark());
           tLAActiveMarkSchema.setNoti(this.mLAActiveMarkSchema.getNoti());
           tLAActiveMarkSchema.setAssessMarkType("11");
//           tLAActiveMarkSchema.setDoneDate(this.mLAActiveMarkSchema.getDoneDate());
           tLAActiveMarkSchema.setOperator(this.mGlobalInput.
                       Operator);
           tLAActiveMarkSchema.setModifyDate(currentDate);
           tLAActiveMarkSchema.setModifyTime(currentTime);

          mMap.put(this.tLAActiveMarkSchema, "UPDATE");
          System.out.println(mMap);
          mInputData.clear();
          mInputData.add(mMap);
        }


        return true;
    }

    /**
     * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        System.out.println("oooooooooooooooo");

        this.mName = (String) cInputData.get(0);
        //this.mBranchAttr = (String) cInputData.get(1);
        this.mAgentGroup = (String) cInputData.get(1);
        this.mManageCom = (String) cInputData.get(2);

        this.mLAActiveMarkSchema.setSchema((LAActiveMarkSchema) cInputData.
                getObjectByObjectName(
                        "LAActiveMarkSchema",
                        0));


        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput",
                0));



        if (mGlobalInput == null || mManageCom == null) {
            CError tError = new CError();
            tError.moduleName = "ALAActivManageBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传入参数失败!";
            this.mErrors.addOneError(tError);
            return false;

        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        System.out.println("Start ALABKRewardPunishBLQuery Submit...");
        LAActiveMarkDB tLAActiveMarkDB = new LAActiveMarkDB();
        tLAActiveMarkDB.setSchema(this.mLAActiveMarkSchema);
        this.mLAActiveMarkSet = tLAActiveMarkDB.query();
        this.mResult.add(this.mLAActiveMarkSet);
        System.out.println("End ALAActivManageBLQuery Submit...");
        // 如果有需要处理的错误，则返回
        if (tLAActiveMarkDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAActiveMarkDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAActivManageBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData = new VData();
            this.mInputData.add(mName);
             //this.mInputData.add(mBranchAttr);
             this.mInputData.add(mAgentGroup);
             this.mInputData.add(mManageCom);
            this.mInputData.add(this.mLAActiveMarkSchema);
            this.mInputData.add(this.mGlobalInput);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAActivManageBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ALAActivManageBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

}
