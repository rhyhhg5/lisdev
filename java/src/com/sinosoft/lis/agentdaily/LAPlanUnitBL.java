package com.sinosoft.lis.agentdaily;

import java.sql.*;
import com.sinosoft.lis.db.LAPlanUnitDB;
import com.sinosoft.lis.db.LAStatSegmentDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LAPlanUnitSchema;
import com.sinosoft.lis.vschema.LAPlanUnitSet;
import com.sinosoft.utility.*;

/**
 * <p>Title: 人员考核指标计算流程类</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class LAPlanUnitBL
{
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap mMap=new MMap();
    private VData mOutputDate=new VData();
    private LAPlanUnitSchema mLAPlanUnitSchema = new LAPlanUnitSchema();
    private LAPlanUnitSet mLAPlanUnitSet = new LAPlanUnitSet();
    private LAPlanUnitSet mLAPlanUnitDealSet = new LAPlanUnitSet();
    private String mPlanStartDate = "";
    private String mPlanEndDate = "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit=new PubSubmit();
        tPubSubmit.submitData(mOutputDate,"") ;
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError())
        {
            buildError("submitData","向数据库提交保存时出错！");
            return false;
        }

        return true;
    }

    /**
     * 进行后台验证
     * @param pmCheckType String  验证类型  0：制定计划验证 1：修改已有计划验证
     * @return boolean
     */
    private boolean check(String pmCheckType)
    {
        if("0".equals(pmCheckType))
        {// 制定新计划验证
            // 对时间进行验证 判断是否已经做过相同条件的计划了
            LAPlanUnitDB tLAPlanUnitDB = new LAPlanUnitDB();
            LAPlanUnitSet tLAPlanUnitSet = new LAPlanUnitSet();

            tLAPlanUnitDB.setPlanType(this.mLAPlanUnitSchema.getPlanType());
            tLAPlanUnitDB.setPlanObject(this.mLAPlanUnitSchema.getPlanObject());
            tLAPlanUnitDB.setPlanPeriodUnit(this.mLAPlanUnitSchema.
                                            getPlanPeriodUnit());
            tLAPlanUnitDB.setPlanStartYM(this.mLAPlanUnitSchema.getPlanStartYM());
            tLAPlanUnitDB.setBranchType(this.mLAPlanUnitSchema.getBranchType());
            tLAPlanUnitDB.setBranchType2(this.mLAPlanUnitSchema.getBranchType2());

            tLAPlanUnitSet = tLAPlanUnitDB.query();
            if(tLAPlanUnitDB.mErrors.needDealError())
            {
                buildError("check", "作后台验证访问数据库时出错！");
                return false;
            }
            if(tLAPlanUnitSet.size() != 0)
            {
                buildError("check", "当前类型的计划已经设定过了,清确认！");
                return false;
            }
        }else if("1".equals(pmCheckType))
        {// 修改已有计划验证
            // 计划一旦设定  时间不允许再修改
        }

        return true;
    }

    /**
     * 进行业务处理
     * @return boolean
     */
    private boolean dealData()
    {
        // 判断操作符分别进行不同操作
        if("INSERT||MAIN".equals(this.mOperate))
        {
            // 制定新计划
            if(!dealInsertPlan())
                return false;
        }else if("UPDATE||MAIN".equals(this.mOperate))
        {
            // 修改已有计划
            if(!dealUpdataPlan())
                return false;
        }

        return true;
    }

    /**
     * 进行制定新计划的操作
     * @return boolean
     */
    private boolean dealInsertPlan()
    {
        //处理前进行 制定新计划验证
        if (!check("0"))
        {
            return false;
        }

        // 首先根据[时间类别]进行计划起期止期的确定
        if (!getStartEndDate())
        {
            buildError("dealData", "获取计划起止期时出错！");
            return false;
        }

        // 准备需要写入的数据
        if(!dealPlanSaveData())
            return false;

        return true;
    }

    /**
     * 准备需要写入的数据
     * @return boolean
     */
    private boolean dealPlanSaveData()
    {
        // 循环处理各种计划数据
        for(int i=1;i<=this.mLAPlanUnitSet.size();i++)
        {
            LAPlanUnitSchema tLAPlanUnitSchema = new LAPlanUnitSchema();
            LAPlanUnitSchema ttLAPlanUnitSchema = new LAPlanUnitSchema();
            // 复制计划对象的基本信息
            tLAPlanUnitSchema.setSchema(this.mLAPlanUnitSchema);
            // 取得画面传入的计划数据
            ttLAPlanUnitSchema = this.mLAPlanUnitSet.get(i);

            // 把画面录入的数据保存起来
            tLAPlanUnitSchema.setPlanItemType(ttLAPlanUnitSchema.getPlanItemType());
            tLAPlanUnitSchema.setPlanValue(ttLAPlanUnitSchema.getPlanValue());
            tLAPlanUnitSchema.setPlanValue2(ttLAPlanUnitSchema.getPlanValue2());
            tLAPlanUnitSchema.setPlanStartDate(this.mPlanStartDate);
            tLAPlanUnitSchema.setPlanEndDate(this.mPlanEndDate);
            tLAPlanUnitSchema.setPlanMark(ttLAPlanUnitSchema.getPlanMark());
            tLAPlanUnitSchema.setOperator(this.mGlobalInput.Operator);
            tLAPlanUnitSchema.setMakeDate(this.currentDate);
            tLAPlanUnitSchema.setMakeTime(this.currentTime);
            tLAPlanUnitSchema.setModifyDate(this.currentDate);
            tLAPlanUnitSchema.setModifyTime(this.currentTime);

            this.mLAPlanUnitDealSet.add(tLAPlanUnitSchema);
        }

        return true;
    }

    /**
     * 取得计划起止期
     * @return boolean
     */
    private boolean getStartEndDate()
    {
        String tStartYearMonth = this.mLAPlanUnitSchema.getPlanStartYM();
        String tEndYearMonth = "";

        // 1：月计划  3：季计划  12：年计划
        if("1".equals(this.mLAPlanUnitSchema.getPlanPeriodUnit()))
        {
            tEndYearMonth = tStartYearMonth;
        }else if("3".equals(this.mLAPlanUnitSchema.getPlanPeriodUnit()))
        {
            String tYear = tStartYearMonth.substring(0,4);
            String tMonth = tStartYearMonth.substring(4,6);
            if("01".equals(tMonth))
            {
                tEndYearMonth = tYear + "03";
            }else if("04".equals(tMonth))
            {
                tEndYearMonth = tYear + "06";
            }else if("07".equals(tMonth))
            {
                tEndYearMonth = tYear + "09";
            }else if("10".equals(tMonth))
            {
                tEndYearMonth = tYear + "12";
            }else
            {
                return false;
            }
        }else if("12".equals(this.mLAPlanUnitSchema.getPlanPeriodUnit()))
        {
            String tYear = tStartYearMonth.substring(0,4);
            tEndYearMonth = tYear + "12";
        }

        // 取得计划起期
        LAStatSegmentDB tLAStatSegmentDB = new LAStatSegmentDB();
        tLAStatSegmentDB.setStatType("5");
        tLAStatSegmentDB.setYearMonth(tStartYearMonth);
        if(!tLAStatSegmentDB.getInfo())
        {
            return false;
        }
        this.mPlanStartDate = tLAStatSegmentDB.getStartDate();
        // 取得计划止期
        tLAStatSegmentDB = new LAStatSegmentDB();
        tLAStatSegmentDB.setStatType("5");
        tLAStatSegmentDB.setYearMonth(tEndYearMonth);
        if(!tLAStatSegmentDB.getInfo())
        {
            return false;
        }
        this.mPlanEndDate = tLAStatSegmentDB.getEndDate();

        return true;
    }

    /**
     * 进行修改已定计划的操作
     * @return boolean
     */
    private boolean dealUpdataPlan()
    {
        // 处理前进行 修改计划验证
        if (!check("1")) {
            return false;
        }

        // 准备修改数据
        if(!dealPlanUpdate())
            return false;

        return true;
    }

    /**
     * 准备修改的数据
     * @return boolean
     */
    private boolean dealPlanUpdate()
    {
        LAPlanUnitDB tLAPlanUnitDB = new LAPlanUnitDB();
        LAPlanUnitSet tLAPlanUnitSet = new LAPlanUnitSet();

        tLAPlanUnitDB.setPlanType(this.mLAPlanUnitSchema.getPlanType());
        tLAPlanUnitDB.setPlanObject(this.mLAPlanUnitSchema.getPlanObject());
        tLAPlanUnitDB.setPlanPeriodUnit(this.mLAPlanUnitSchema.getPlanPeriodUnit());
        tLAPlanUnitDB.setPlanStartYM(this.mLAPlanUnitSchema.getPlanStartYM());
        tLAPlanUnitDB.setBranchType(this.mLAPlanUnitSchema.getBranchType());
        tLAPlanUnitDB.setBranchType2(this.mLAPlanUnitSchema.getBranchType2());

        tLAPlanUnitSet = tLAPlanUnitDB.query();

        if(tLAPlanUnitDB.mErrors.needDealError())
        {
            buildError("dealPlanUpdate", "查询指定计划信息时出错！");
            return false;
        }
        // 如果没有查到数据  或  数据库里的数据数不对  报错
        if(tLAPlanUnitSet.size() == 0 ||
           tLAPlanUnitSet.size() != this.mLAPlanUnitSet.size())
        {
            buildError("dealPlanUpdate", "查询指定计划信息失败！");
            return false;
        }

        for(int i=1;i<=tLAPlanUnitSet.size();i++)
        {
            LAPlanUnitSchema tLAPlanUnitSchema = new LAPlanUnitSchema();
            LAPlanUnitSchema ttLAPlanUnitSchema = new LAPlanUnitSchema();
            tLAPlanUnitSchema = tLAPlanUnitSet.get(i);
            ttLAPlanUnitSchema = this.mLAPlanUnitSet.get(i);

            // 修改变更的数据
            tLAPlanUnitSchema.setPlanValue(ttLAPlanUnitSchema.getPlanValue());
            tLAPlanUnitSchema.setPlanValue2(ttLAPlanUnitSchema.getPlanValue2());
            tLAPlanUnitSchema.setPlanMark(ttLAPlanUnitSchema.getPlanMark());
            tLAPlanUnitSchema.setOperator(this.mGlobalInput.Operator);
            tLAPlanUnitSchema.setModifyDate(this.currentDate);
            tLAPlanUnitSchema.setModifyTime(this.currentTime);

            mLAPlanUnitDealSet.add(tLAPlanUnitSchema);
        }

        return true;
    }

    /**
     * 准备后台数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        String tOperate = "";

        // 根据不同的处理要求  设置操作符
        if("INSERT||MAIN".equals(this.mOperate))
        {
            tOperate = "INSERT";
        }else if("UPDATE||MAIN".equals(this.mOperate))
        {
            tOperate = "UPDATE";
        }

        for(int i=1;i<=this.mLAPlanUnitDealSet.size();i++)
        {
            System.out.print(mLAPlanUnitDealSet.get(i).getPlanItemType());
            System.out.println();
        }

        try{
            mMap.put(this.mLAPlanUnitDealSet, tOperate);
            this.mOutputDate.add(mMap);
        }catch (Exception ex)
        {
            buildError("prepareOutputData","准备提交后台的数据时出错！");
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("Begin LAAgentEvaluateBL.getInputData.........");
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLAPlanUnitSchema.setSchema((LAPlanUnitSchema) cInputData.
                                         getObjectByObjectName(
                                                 "LAPlanUnitSchema", 0));
        this.mLAPlanUnitSet.set((LAPlanUnitSet) cInputData.getObjectByObjectName(
                "LAPlanUnitSet", 0));

        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAgentEvaluateUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return int
     */
    private int execQuery(String sql)
    {
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null)return 0;
            st = conn.prepareStatement(sql);
            if (st == null)return 0;
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
                st.close();
                rs.close();
                conn.close();
                st = null;
                rs = null;
                conn = null;
            } catch (Exception e) {}
        }
    }

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LAPlanUnitBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
}
