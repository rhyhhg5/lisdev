package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.DBConnPool;
import java.sql.Connection;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;

public class LAWageGatherBL
{
    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public  CErrors mErrors=new CErrors();
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("Start LAWageGatherBL submit...");

        //开始后台操作
        if (!dealData(cInputData, cOperate)) {
            return false;
        }

        System.out.println("End LAWageGatherBL submit...");

        return true;
    }

    /**
     * 进行数据库操作
     * @param pmVData VData
     * @param pmOperate String
     * @return boolean
     */
    private boolean dealData(VData pmVData,String pmOperate)
    {
        Connection conn ;
        conn=null;
        conn=DBConnPool.getConnection();
         if (conn==null)
         {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "LAWageGatherBL";
             tError.functionName = "saveData";
             tError.errorMessage = "数据库连接失败!";
             this.mErrors .addOneError(tError) ;
             return false;
        }
        try{
            conn.setAutoCommit(false);


            //代理人每月最终佣金备份
            System.out.println("进行佣金备份！");
            LAWageBDBSet tLAWageBDBSet = new LAWageBDBSet(conn);
            tLAWageBDBSet.set((LAWageBSet) pmVData.
                              getObjectByObjectName("LAWageBSet", 0));
            if (!tLAWageBDBSet.insert()) {
                conn.rollback();
                conn.close();
                System.out.println(tLAWageBDBSet.mErrors.getFirstError());
                System.out.println("代理人每月最终佣金备份 处理失败！");
                return false;
            }

            //代理人每月最终佣金表
            System.out.println("删除已经备份的佣金信息");
            LAWageDBSet tLAWageDBSet = new LAWageDBSet(conn);
            tLAWageDBSet.set((LAWageSet) pmVData.
                              getObjectByObjectName("LAWageSet", 0));
            if (!tLAWageDBSet.delete()) {
                conn.rollback();
                conn.close();
                System.out.println(tLAWageDBSet.mErrors.getFirstError());
                System.out.println("代理人每月最终佣金表 处理失败！");
                return false;
            }
//          代理人每月试算佣金表
            System.out.println("删除代理人薪资试算表信息");
            LAWageTempDBSet tLAWageTempDBSet = new LAWageTempDBSet(conn);
            tLAWageTempDBSet.set((LAWageTempSet) pmVData.
                              getObjectByObjectName("LAWageTempSet", 0));
            if (!tLAWageTempDBSet.delete()) {
                conn.rollback();
                conn.close();
                System.out.println(tLAWageTempDBSet.mErrors.getFirstError());
                System.out.println("代理人每月试算佣金表 处理失败！");
                return false;
            }
            //指标信息备份表
//            System.out.println("备份指标信息备份表");
//            LAIndexInfoBDBSet tLAIndexInfoBDBSet = new LAIndexInfoBDBSet(conn);
//            tLAIndexInfoBDBSet.set((LAIndexInfoBSet) pmVData.
//                              getObjectByObjectName("LAIndexInfoBSet", 0));
//            if (!tLAIndexInfoBDBSet.insert()) {
//                conn.rollback();
//                conn.close();
//                System.out.println(tLAIndexInfoBDBSet.mErrors.getFirstError());
//                System.out.println("指标信息备份表 处理失败！");
//                return false;
//            }
            
//          指标信息表
            System.out.println("指标信息备份");
            LAIndexInfoBDBSet tLAIndexInfoBDBSet = new LAIndexInfoBDBSet(conn);
            tLAIndexInfoBDBSet.set((LAIndexInfoBSet) pmVData.
                              getObjectByObjectName("LAIndexInfoBSet", 0));
            if (!tLAIndexInfoBDBSet.insert()) {
                conn.rollback();
                conn.close();
                System.out.println(tLAIndexInfoBDBSet.mErrors.getFirstError());
                System.out.println("指标信息备份表 处理失败！");
                return false;
            }
            
            //指标信息表
            System.out.println("删除指标信息表");
            LAIndexInfoDBSet tLAIndexInfoDBSet = new LAIndexInfoDBSet(conn);
            tLAIndexInfoDBSet.set((LAIndexInfoSet) pmVData.
                              getObjectByObjectName("LAIndexInfoSet", 0));
            if (!tLAIndexInfoDBSet.delete()) {
                conn.rollback();
                conn.close();
                System.out.println(tLAIndexInfoDBSet.mErrors.getFirstError());
                System.out.println("指标信息表 处理失败！");
                return false;
            }

            //回退佣金历史表
            System.out.println("回退佣金历史表");
            LAWageHistoryDBSet tLAWageHistoryDBSet = new LAWageHistoryDBSet(conn);
            tLAWageHistoryDBSet.set((LAWageHistorySet) pmVData.
                              getObjectByObjectName("LAWageHistorySet", 0));
            if (!tLAWageHistoryDBSet.update()) {
                conn.rollback();
                conn.close();
                System.out.println(tLAWageHistoryDBSet.mErrors.getFirstError());
                System.out.println("回退轨迹表 处理失败！");
                return false;
            }

            //回退轨迹表
            System.out.println("添加回退轨迹");
            LARollBackTraceDBSet tLARollBackTraceDBSet = new LARollBackTraceDBSet(conn);
            tLARollBackTraceDBSet.set((LARollBackTraceSet) pmVData.
                              getObjectByObjectName("LARollBackTraceSet", 0));
            if (!tLARollBackTraceDBSet.insert())
            {
                conn.rollback();
                conn.close();
                System.out.println(tLARollBackTraceDBSet.mErrors.getFirstError());
                System.out.println("回退轨迹表 处理失败！");
                return false;
            }
            conn.commit() ;
            conn.close();
        }
        catch(Exception ex)
        {

          // @@错误处理
            ex.printStackTrace() ;
            CError tError =new CError();
            tError.moduleName="LAWageGatherBL";
            tError.functionName="dealData";
            tError.errorMessage=ex.toString();
            this.mErrors .addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }

        return true;
    }
}
