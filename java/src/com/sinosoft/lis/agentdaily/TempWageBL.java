package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.schema.LAWageSchema;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAWageDB;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: TempWageBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author songgh
 * @version 1.0
 */
public class TempWageBL {
  //错误处理类
  public static CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LAWageSet mLAWageSet = new LAWageSet();
  private String mAreaType = "";
  private String mManageCom = "";
  private String sWageno = "";//开始年月
  private String eWageno = "";//结束年月
//  private Reflections ref = new Reflections();
  //private LARateCommisionBSet mLARateCommisionBSet = new LARateCommisionBSet();

  public TempWageBL() {
 
  }
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin TempWageBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start LABankRateCommSetBL Submit...");
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LABankRateCommSetBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LABankRateCommSetBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     System.out.println("GlobalInput get");
     this.mLAWageSet.set( (LAWageSet) cInputData.getObjectByObjectName("LAWageSet",0));
     this.mManageCom = (String)cInputData.get(1);
     this.sWageno =(String)cInputData.get(2);
     this.eWageno =(String)cInputData.get(3);
      System.out.println("mLAWageSet get"+mLAWageSet.size());
      System.out.println("mManageCom get"+mManageCom);

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LABankRateCommSetBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin TempWageBL.dealData........."+mOperate);
    try {
    	 mAreaType = AgentPubFun.getAreaType(mManageCom.substring(0, 4),"02");
         
    	 String SQL = "select * from lawage where branchtype='2'" +
         		" and branchtype2='01' " +
         		" and agentcode in (select agentcode from laagent where agentstate<='02')" +
         		" and managecom like '"+mManageCom+"%'"+         		
         		" and indexcalno between '"+this.sWageno+"' and '"+this.eWageno+"' order by indexcalno,managecom,agentcode";
       // ExeSQL tExeSQL = new ExeSQL();
        LAWageDB tLAWageDB = new LAWageDB();      
        this.mLAWageSet = null;
        this.mLAWageSet =  tLAWageDB.executeQuery(SQL);       
         if(mLAWageSet.size()>0)
        {
        	for(int i = 1;i<=mLAWageSet.size();i++)
        	{
        		LAWageSchema tLAWageSchema = new LAWageSchema();
        		tLAWageSchema = mLAWageSet.get(i);
        	    String tAgentCode = tLAWageSchema.getAgentCode();
        	    String tAgentGrade = tLAWageSchema.getAgentGrade();
        	    String tWageNo = tLAWageSchema.getIndexCalNo();
        	    String tBranchType = "2";
        	    String tBranchType2 = "01";
        	    //先进行T41计算,然后进行T42计算,主要是得到最终结果,所以中间结果先不考虑
        	    String T41 = calT41(tBranchType,tBranchType2,tAgentCode,tAgentGrade,tWageNo,mAreaType);
        	    System.out.println("T41=:"+T41);
        	    String T42 = calT42(tBranchType,tBranchType2,tAgentCode,tAgentGrade,tWageNo,mAreaType);
        	    System.out.println("T42=:"+T42);
        	    double W10 = Double.parseDouble(T41) - Double.parseDouble(T42);
        	    if(!updateLAWage(tAgentCode,tWageNo,W10))
        	    {
        	    	return false;
        	    }
        	}       	
        }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "TempWageBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
/**
 * 效益提奖正值
 * @param cBranchType
 * @param cBranchType2
 * @param cAgentCode
 * @param cAgentGrade
 * @param cWageNo
 * @param cAreaType
 * @return
 */
  private String calT41(String cBranchType, String cBranchType2,
          String cAgentCode, String cAgentGrade,
          String cWageNo,String cAreaType)
  {
		
		String tAgentCode = cAgentCode == null ? "" : cAgentCode;
		String tBranchType = cBranchType == null ? "" : cBranchType;
		String tBranchType2 = cBranchType2 == null ? "" : cBranchType2;
		String tAgentGrade = cAgentGrade == null ? "" : cAgentGrade;
		String tWageNo = cWageNo ==null? "": cWageNo;
		String tAreaType = cAreaType ==null? "": cAreaType;
		if (tBranchType == "") {
		System.out.println("------||||||||+++++++展业类型不能为空");
		return "";
		}
		if (tBranchType2 == "") {
		System.out.println("------||||||||+++++++渠道类型不能为空");
		return "";
		}
		Calculator tCalculator = new Calculator();
		tCalculator.setCalCode("AP0071");
		tCalculator.addBasicFactor("AgentCode", tAgentCode);
		tCalculator.addBasicFactor("AgentGrade", tAgentGrade);
		tCalculator.addBasicFactor("WageNo", tWageNo);
		tCalculator.addBasicFactor("AreaType", tAreaType);
		tCalculator.addBasicFactor("BranchType", tBranchType);
		tCalculator.addBasicFactor("BranchType2", tBranchType2);		
		String tResult = tCalculator.calculate();
		return tResult == null ? "" : tResult;

}
  /**
   * 效益提奖负值
   * @param cBranchType
   * @param cBranchType2
   * @param cAgentCode
   * @param cAgentGrade
   * @param cWageNo
   * @param cAreaType
   * @return
   */
  private String calT42(String cBranchType, String cBranchType2,
          String cAgentCode, String cAgentGrade,
          String cWageNo,String cAreaType)
  {
		
		String tAgentCode = cAgentCode == null ? "" : cAgentCode;
		String tBranchType = cBranchType == null ? "" : cBranchType;
		String tBranchType2 = cBranchType2 == null ? "" : cBranchType2;
		String tAgentGrade = cAgentGrade == null ? "" : cAgentGrade;
		String tWageNo = cWageNo ==null? "": cWageNo;
		String tAreaType = cAreaType ==null? "": cAreaType;
		if (tBranchType == "") {
		System.out.println("------||||||||+++++++展业类型不能为空");
		return "";
		}
		if (tBranchType2 == "") {
		System.out.println("------||||||||+++++++渠道类型不能为空");
		return "";
		}
		Calculator tCalculator = new Calculator();
		tCalculator.setCalCode("AP0072");
		tCalculator.addBasicFactor("AgentCode", tAgentCode);
		tCalculator.addBasicFactor("AgentGrade", tAgentGrade);
		tCalculator.addBasicFactor("WageNo", tWageNo);
		tCalculator.addBasicFactor("AreaType", tAreaType);
		tCalculator.addBasicFactor("BranchType", tBranchType);
		tCalculator.addBasicFactor("BranchType2", tBranchType2);		
		String tResult = tCalculator.calculate();
		return tResult == null ? "" : tResult;

}
  /**
   * 
   * @param cAgentCode
   * @param cWageNo
   * @param W10
   * @return
   */
  private boolean updateLAWage(String cAgentCode,String cWageNo,double cW10)
  {
	  
	  String updateSQL = "update lawage set W10= "+cW10+" where agentcode='"+cAgentCode+"' and " +
	  		" indexcalno= '"+cWageNo+"' and branchtype='2' and branchtype2='01'";
	  System.out.println(updateSQL	);
	  this.map.put(updateSQL, "UPDATE");
	  return true;
  }
  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      System.out.println(
          "Begin LABankRateCommSetBL.prepareOutputData.........");
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LABankRateCommSetBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
}
