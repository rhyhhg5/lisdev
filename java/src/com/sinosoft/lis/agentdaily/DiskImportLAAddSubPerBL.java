package com.sinosoft.lis.agentdaily;


import java.io.File;

import com.sinosoft.lis.db.LARewardPunishDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LARateCommisionSchema;
import com.sinosoft.lis.schema.LARewardPunishSchema;
import com.sinosoft.lis.schema.LCGrpImportLogSchema;
import com.sinosoft.lis.vschema.LARateCommisionSet;
import com.sinosoft.lis.vschema.LARewardPunishSet;
import com.sinosoft.lis.vschema.LCGrpImportLogSet;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class DiskImportLAAddSubPerBL {
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private MMap mmap = new MMap();
    
    private int importPersons = 0; //记录导入成功的记录数
    private int unImportRecords = 0; //记录导入不成功的记录数
    
    private String diskimporttype = "";
    private String path = "";
    private String fileName = "";
    private String configName = "LARewardPunishDiskImport.xml";
    private LARewardPunishSet mLARewardPunishSetFinal = new LARewardPunishSet();
    private LARewardPunishSet mLARewardPunishSet = null;
    //存放日志的
    private LCGrpImportLogSet mLCGrpImportLogSet=new LCGrpImportLogSet();
    private int merrorNum=0;
    private StringBuffer merrorInfo=new StringBuffer();
    public DiskImportLAAddSubPerBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
    	//System.out.println("BL submited");
        if (!getInputData(cInputData)) {
            return false;
        }
        if(!checkBatchno()){
        	return false;
        }
        mOperate = cOperate;
        String path = (String) mTransferData.getValueByName("path");
        String fileName = path + File.separator +
                          (String) mTransferData.getValueByName("fileName");
        diskimporttype=(String)mTransferData.getValueByName("diskimporttype");
        String configFileName = path + File.separator + configName;
        System.out.println("configFileName========"+configFileName);
        System.out.println("begin import");
        //开始从磁盘导入数据
        LARewardPunishDiskImporter importer = new LARewardPunishDiskImporter(fileName,
                configFileName,
                diskimporttype);
        System.out.println("DiskImportLAAddSubPerBL.java:diskimporttype"+diskimporttype);
        importer.setTableName(diskimporttype);
       if (!importer.doImport()) {
           mErrors.copyAllErrors(importer.mErrrors);
           return false;
        }
        System.out.println("doimported");
       if (diskimporttype.equals("LARewardPunish")) {
    	   mLARewardPunishSet = (LARewardPunishSet) importer
                                 .getSchemaSet();
        }
       System.out.println(mLARewardPunishSet.size());
       System.out.println("=====开始校验数据======");
        if (!checkData()) {
            return false;
        }
        System.out.println("data checked");
        //数据准备操作
        if (mOperate.equals("INSERT")) {
        	System.out.println("开始准备数据");
            if (!prepareData()) {
                return false;
            }
            System.out.println("---End prepareData---");
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mResult, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            System.out.println("DiskImportRateCommisionBL数据提交失败");
            this.mErrors.addOneError("数据提交失败!");
            return false;
        }
        System.out.println("--- ->submitData--- 数据提交成功。");

        return true;
    }

    private boolean getInputData(VData cInputData) {
        mTransferData = (TransferData) cInputData.
                        getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.
                       getObjectByObjectName("GlobalInput", 0);
        this.fileName=(String)mTransferData.getValueByIndex(4);
        
        System.out.println("...........mTransferData.getValueByIndex(4)"+mTransferData.getValueByIndex(4));
        if (mTransferData == null || mGlobalInput == null) {
            mErrors.addOneError("传入的的数据不完整: mTransferData == null "
                                + "|| mGlobalInput == null");
            return false;
        }
        diskimporttype = (String) mTransferData.getValueByName("diskimporttype");
        System.out.println("...............mTransferData.getValueByName(branchtype)"
        		+mTransferData.getValueByName("branchtype"));
        return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkBatchno(){
    	String tSQL="select 1 from lcgrpimportlog where batchno='"+this.fileName+"' with ur";
    	ExeSQL tExeSQL=new ExeSQL();
    	if(tExeSQL.getOneValue(tSQL).equals("1")){
        	mErrors.addOneError("该文件已被导入过，不能再次被导入.请核实！");
    		return false;
    	}
    	return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkData() {
        //展业证信息校验
        if (diskimporttype.equals("LARewardPunish")) {
            if (mLARewardPunishSet == null) {
                mErrors.addOneError("导入加扣款信息失败！没有得到要导入的数据。");
                return false;
            } else {
            	System.out.println("mLARewardPunishSet======"+mLARewardPunishSet);
                LARewardPunishSchema  tempLARewardPunishSchema = new LARewardPunishSchema();
                LARewardPunishSchema  tempLARewardPunishSchema2 = new LARewardPunishSchema();
                LARewardPunishDB  mLARewardPunishDB = new LARewardPunishDB();
                LCGrpImportLogSchema tLCGrpImportLogSchema;
            	String importInfo;
            	System.out.println("导入数据的个数======="+mLARewardPunishSet.size());
                for (int i = 1; i <= mLARewardPunishSet.size(); i++) {
                	this.merrorNum=0;
                	this.merrorInfo.delete(0,merrorInfo.length());
                	System.out.println("..............checkdata here merrorInfo"
                			+this.merrorInfo);
                	System.out.println("merrorNum--------"+merrorNum);
                	tempLARewardPunishSchema=mLARewardPunishSet.get(i);
                	System.out.println("tempLARewardPunishSchema"+tempLARewardPunishSchema.getAgentCode());
                    if (tempLARewardPunishSchema.getAgentCode() == null ||
                    		tempLARewardPunishSchema.getAgentCode() .equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("营销员不能为空/");
                    }
                    System.out.println("错误可能产生1--------"+merrorNum);
                    if (tempLARewardPunishSchema.getAgentGroup() == null ||
                    		tempLARewardPunishSchema.getAgentGroup().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("销售单位不能为空/");
                    } 
                    System.out.println("错误可能产生2--------"+merrorNum);
                    if (tempLARewardPunishSchema.getManageCom() == null ||
                    		tempLARewardPunishSchema.getManageCom().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("管理机构不能为空/");
                    } 
                    System.out.println("错误可能产生3--------"+merrorNum);
                    
                    if (tempLARewardPunishSchema.getDoneDate() == null ||
                    		tempLARewardPunishSchema.getDoneDate().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("执行日期不能为空/");
                    } 
                    
                    System.out.println("错误可能产生4--------"+merrorNum);
                    System.out.println("merrorNum+++++++"+merrorNum);
                    if(this.merrorNum==0){
                    	for(int j = i+1; j <= mLARewardPunishSet.size(); j++){
                    		tempLARewardPunishSchema2=mLARewardPunishSet.get(j);
                        	if(tempLARewardPunishSchema.getManageCom().equals(tempLARewardPunishSchema2.getManageCom())
                        			&&tempLARewardPunishSchema.getAgentCode().equals(tempLARewardPunishSchema2.getAgentCode())
                        			&&tempLARewardPunishSchema.getAgentGroup().equals(tempLARewardPunishSchema2.getAgentGroup())
                        			&&tempLARewardPunishSchema.getNoti().equals(tempLARewardPunishSchema2.getNoti())
                        			&&tempLARewardPunishSchema.getMoney()==tempLARewardPunishSchema2.getMoney()
                        			&&tempLARewardPunishSchema.getDoneFlag().equals(tempLARewardPunishSchema2.getDoneFlag())
                        			&&tempLARewardPunishSchema.getPunishRsn().equals(tempLARewardPunishSchema2.getPunishRsn())
                        			&&tempLARewardPunishSchema.getAwardTitle().equals(tempLARewardPunishSchema2.getAwardTitle())
                        	){
                        		this.merrorNum++;
                                this.merrorInfo.append("与第"+j+"行数据重复/");
                        	}
                        }
                    }
                    System.out.println("没有数据重复");
                    System.out.println("merrorNum==========="+merrorNum);
                    if(this.merrorNum==0){
                    	System.out.println("开始跟数据库的信息进行校验");
                       checkExist(tempLARewardPunishSchema,mLARewardPunishDB);
                    }
                    if(this.merrorNum>0){
                    	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                    	tLCGrpImportLogSchema.setErrorInfo("导入错误！具体错误有："
                    			+this.merrorInfo.toString());
                    	tLCGrpImportLogSchema.setErrorState("1");
                    	tLCGrpImportLogSchema.setErrorType("1");
                    	tLCGrpImportLogSchema.setContID(i+"");
                    	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                    	this.unImportRecords++;
                    }else{
                    	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                    	tLCGrpImportLogSchema.setErrorInfo("导入成功！");
                    	tLCGrpImportLogSchema.setErrorState("0");
                    	tLCGrpImportLogSchema.setErrorType("0");
                    	tLCGrpImportLogSchema.setContID(i+"");
                    	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                    }
                }
            }
        }
        return true;
    }
private boolean checkExist(LARewardPunishSchema mLARewardPunishSchema,LARewardPunishDB mLARewardPunishDB){

	LARateCommisionSet tempLARateCommisionSet=new LARateCommisionSet();
	LARewardPunishSet  tempmLARewardPunishSet = new LARewardPunishSet();
	String tempSql="select * from LARewardPunish where managecom='"
		+mLARewardPunishSchema.getManageCom()+"' and agentcode='"
		+mLARewardPunishSchema.getAgentCode()+"' and agentgroup='"
		+mLARewardPunishSchema.getAgentGroup()+"' and doneflag="
		+mLARewardPunishSchema.getDoneFlag()+" and money="
		+mLARewardPunishSchema.getMoney()+" and awardtitle="
		+mLARewardPunishSchema.getAwardTitle()+" and PunishRsn= "
		+mLARewardPunishSchema.getPunishRsn()+ "and branchtype='"
		+mTransferData.getValueByName("branchtype")+"' and branchtype2='"
		+mTransferData.getValueByName("branchtype2")+"'";
	System.out.println("checkExist:tempsql"+tempSql);
	tempmLARewardPunishSet=mLARewardPunishDB.executeQuery(tempSql);
	if(tempLARateCommisionSet.size()>0){
		System.out.println("unImportRecords appending one");
		this.merrorNum++;
        this.merrorInfo.append("在数据库中已经存在/");
	}else{
		LARewardPunishSchema tempLARewardPunishSchema=new LARewardPunishSchema();
		tempLARewardPunishSchema.setAgentCode(mLARewardPunishSchema.getAgentCode());
		tempLARewardPunishSchema.setAgentGroup(mLARewardPunishSchema.getAgentGroup());
		tempLARewardPunishSchema.setManageCom(mLARewardPunishSchema.getManageCom());
		tempLARewardPunishSchema.setDoneFlag(mLARewardPunishSchema.getDoneFlag());
		tempLARewardPunishSchema.setMoney(mLARewardPunishSchema.getMoney());
		tempLARewardPunishSchema.setAwardTitle(mLARewardPunishSchema.getAwardTitle());
		tempLARewardPunishSchema.setPunishRsn(mLARewardPunishSchema.getPunishRsn());
		tempLARewardPunishSchema.setDoneDate(mLARewardPunishSchema.getDoneDate());
		mLARewardPunishSetFinal.add(tempLARewardPunishSchema);
		System.out.println("mLARewardPunishSetFinal--------"+mLARewardPunishSetFinal.size());
	}
	return true;
}
    /**
     * 检查每个客户是否是本公司的代理人，若不是则剔除
     * @param
     * @return boolean，成功校验 trun
     */
    private boolean prepareData() {
    	String idxTemp = (String) mTransferData.getValueByName("idx");
    	int idx=Integer.parseInt(idxTemp)+1;
    	System.out.println("prepareData:idxTempCompare======"+idx);
        if (diskimporttype.equals("LARewardPunish")) {
        	System.out.println("prepareData:doing");
            importPersons=mLARewardPunishSetFinal.size();
            System.out.println("导入数据的个数为"+importPersons);
            //System.out.println("prepareData:unImportPersons"+unImportPersons);
            if(mLARewardPunishSetFinal!=null&&mLARewardPunishSetFinal.size()>0){
             	//importPersons = mLARateCommisionSetFinal.size();
            	System.out.println("prepareData:importPersons"+importPersons);
            	for (int i = 1; i <= mLARewardPunishSetFinal.size(); i++) {
              	String branchtype=(String) mTransferData.getValueByName("branchtype");
              	System.out.println("BL:branchtype  "+branchtype);
              	String branchtype2=(String) mTransferData.getValueByName("branchtype2");
              	System.out.println("BL:branchtype2  "+branchtype2);
              	mLARewardPunishSetFinal.get(i).setOperator(mGlobalInput.Operator);
              	mLARewardPunishSetFinal.get(i).setIdx(idx++);
              	mLARewardPunishSetFinal.get(i).setBranchType(branchtype);
              	mLARewardPunishSetFinal.get(i).setBranchType2(branchtype2); 
              	mLARewardPunishSetFinal.get(i).setMakeDate(PubFun.getCurrentDate());
              	mLARewardPunishSetFinal.get(i).setMakeTime(PubFun.getCurrentTime());
              	mLARewardPunishSetFinal.get(i).setModifyDate(PubFun.getCurrentDate());
              	mLARewardPunishSetFinal.get(i).setModifyTime(PubFun.getCurrentTime());
              	mmap.put(mLARewardPunishSetFinal.get(i), "INSERT");
              }
            }
            if(this.mLCGrpImportLogSet!=null&&this.mLCGrpImportLogSet.size()>0){
            	//importPersons = mLARateCommisionSetFinal.size();
            	for(int i=1;i<=this.mLCGrpImportLogSet.size();i++){
            		mLCGrpImportLogSet.get(i).setGrpContNo("00000000000000000000");
            		mLCGrpImportLogSet.get(i).setOperator(this.mGlobalInput.Operator);
            		mLCGrpImportLogSet.get(i).setMakeDate(PubFun.getCurrentDate());
            		mLCGrpImportLogSet.get(i).setMakeTime(PubFun.getCurrentTime());
            		mLCGrpImportLogSet.get(i).setBatchNo(this.fileName);
            		mmap.put(mLCGrpImportLogSet.get(i), "INSERT");
            	}
            }
            System.out.println("prepareData:adding map");
            this.mResult.add(mmap);
            System.out.println("prepareData:did");
        }
        return true;
    }

    /**
     *成功导入的记录数
     * @return int
     */
    public int getImportPersons() {
        return importPersons;
    }
    public int getUnImportRecords() {
        return unImportRecords;
    }

    public static void main(String[] args) {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        TransferData t = new TransferData();

        t.setNameAndValue("diskimporttype", "LACertification");
        t.setNameAndValue("path", "D:\\picch\\ui\\temp");
        t.setNameAndValue("fileName", "展业证清单.xls");

        VData v = new VData();
        v.add(g);
        v.add(t);

        DiskImportLAAddSubPerBL d = new DiskImportLAAddSubPerBL();
        d.submitData(v, "INSERT");

        if (d.mErrors.needDealError()) {
            System.out.println(d.mErrors.getErrContent());
        }
    }

}
