package com.sinosoft.lis.agentdaily;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.db.LAComToAgentDB;
import com.sinosoft.lis.db.LAComToAgentBDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LAComToAgentBSchema;
import com.sinosoft.lis.schema.LAComToAgentSchema;
import com.sinosoft.lis.vschema.LAComToAgentBSet;
import com.sinosoft.lis.vschema.LAComToAgentSet;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;
import java.math.BigInteger;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import sun.reflect.Reflection;

public class ContChangeComBL {
        //错误处理类
       public CErrors mErrors = new CErrors(); //错误处理类
        //业务处理相关变量
        /** 全局数据 */
        private VData mInputData = new VData();
        private String mOperate = "";
        private String CurrentDate = PubFun.getCurrentDate();
        private String CurrentTime = PubFun.getCurrentTime();


        private MMap map = new MMap();
        public  GlobalInput mGlobalInput = new GlobalInput();

        private LCContSchema mLCContSchema= new LCContSchema();
        private LCContSchema mNewLCContSchema=  new LCContSchema();


        private Reflections ref = new Reflections();
        private VData mResult = new VData();


        /**
          传输数据的公共方法
         */
        public boolean submitData(VData cInputData, String cOperate) {
          System.out.println("Begin ContChangeComBL.submitData.........");
          //将操作数据拷贝到本类中
          this.mOperate = cOperate;
          //得到外部传入的数据,将数据备份到本类中
          if (!getInputData(cInputData)) {
            return false;
          }
          //进行业务处理
          if (!dealData()) {
            return false;
          }
          //准备往后台的数据
          if (!prepareOutputData()) {
            return false;
          }
          PubSubmit tPubSubmit = new PubSubmit();
          System.out.println("Start ContChangeComBL Submit...");
          if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "ContChangeComBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
          }
          return true;
        }

        /**
         * 从输入数据中得到所有对象
         *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
         */
        private boolean getInputData(VData cInputData) {
          //全局变量
          try {
            System.out.println("Begin ContChangeComBL.getInputData.........");
            this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                        getObjectByObjectName("GlobalInput", 0));
           System.out.println("GlobalInput get");

          this.mLCContSchema.setSchema((LCContSchema) cInputData.getObjectByObjectName(
                    "LCContSchema", 1));
          this.mNewLCContSchema.setSchema((LCContSchema) cInputData.getObjectByObjectName(
                    "LCContSchema", 2));

           System.out.println("LAComToAgentSchema get"+mLCContSchema.getAgentCom());
           System.out.println("LAComToAgentSchema 111get"+mNewLCContSchema.getAgentCom());
          }
          catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ContChangeComBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "在读取处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
          }
          System.out.println("getInputData end ");
          return true;
        }

        /**
         * 业务处理主函数
         */
        private boolean dealData() {
          System.out.println("Begin ContChangeComBL.dealData........."+mOperate);

        String ContNo = this.mLCContSchema.getContNo();
        String AgentComNew = this.mNewLCContSchema.getAgentCom();//
        String AgentNew = this.mNewLCContSchema.getAgentCode();//
        String AgentNewGroup = this.mNewLCContSchema.getAgentGroup();
        String AgentOld = this.mLCContSchema.getAgentCode();//
        String AgentComOld = this.mLCContSchema.getAgentCom();//


          try {
            if (mOperate.equals("UPDATE||INSERT") )
            {
             if(AgentComOld.equals("")||AgentComOld==null){
                        //更新
             String LCContSQL = "update lccont  set agentcom='"+AgentComNew+"'";
             if(!AgentNew.equals(AgentOld))
             { LCContSQL+=",agentcode='"+AgentNew+"'  ,agentgroup='"+AgentNewGroup+"'"; }
             LCContSQL+=",ModifyDate='"+CurrentDate+"',ModifyTime='"+CurrentTime+"'  where contno='"+ContNo+"'";

             String LBContSQL = "update LBCont  set agentcom='"+AgentComNew+"'";
                         if(!AgentNew.equals(AgentOld))
                         { LBContSQL+=",agentcode='"+AgentNew+"'  ,agentgroup='"+AgentNewGroup+"'"; }
             LBContSQL+=",ModifyDate='"+CurrentDate+"',ModifyTime='"+CurrentTime+"'  where contno='"+ContNo+"'";

             String LCPolSQL = "update LCPol  set agentcom='"+AgentComNew+"'";
             if(!AgentNew.equals(AgentOld))
             { LCPolSQL+=",agentcode='"+AgentNew+"'  ,agentgroup='"+AgentNewGroup+"'"; }
              LCPolSQL+=",ModifyDate='"+CurrentDate+"',ModifyTime='"+CurrentTime+"'  where contno='"+ContNo+"'";

              String LBPolSQL = "update LBPol  set agentcom='"+AgentComNew+"'";
                           if(!AgentNew.equals(AgentOld))
                           { LBPolSQL+=",agentcode='"+AgentNew+"'  ,agentgroup='"+AgentNewGroup+"'"; }
              LBPolSQL+=",ModifyDate='"+CurrentDate+"',ModifyTime='"+CurrentTime+"'  where contno='"+ContNo+"'";

             String ljapaySQL = "update ljapay  set agentcom='"+AgentComNew+"'";
              if(!AgentNew.equals(AgentOld))
             { ljapaySQL+=",agentcode='"+AgentNew+"'  ,agentgroup='"+AgentNewGroup+"'"; }
              ljapaySQL+=",ModifyDate='"+CurrentDate+"',ModifyTime='"+CurrentTime+"'  where InComeNo='"+ContNo+"'";

              String ljapaypersonSQL = "update ljapayperson  set agentcom='"+AgentComNew+"'";
              if(!AgentNew.equals(AgentOld))
              { ljapaypersonSQL+=",agentcode='"+AgentNew+"'  ,agentgroup='"+AgentNewGroup+"'"; }
              ljapaypersonSQL+=",ModifyDate='"+CurrentDate+"',ModifyTime='"+CurrentTime+"'  where contno='"+ContNo+"'";

              String ljtempfeeSQL = "update ljtempfee  set agentcom='"+AgentComNew+"'";
              if(!AgentNew.equals(AgentOld))
              { ljtempfeeSQL+=",agentcode='"+AgentNew+"'  ,agentgroup='"+AgentNewGroup+"'"; }
              ljtempfeeSQL+=",ModifyDate='"+CurrentDate+"',ModifyTime='"+CurrentTime+"'  where OtherNo='"+ContNo+"'";

              String LJSGETCLAIMSQL = "update LJSGETCLAIM  set agentcom='"+AgentComNew+"'";
              if(!AgentNew.equals(AgentOld))
              { LJSGETCLAIMSQL+=",agentcode='"+AgentNew+"'  ,agentgroup='"+AgentNewGroup+"'"; }
              LJSGETCLAIMSQL+=",ModifyDate='"+CurrentDate+"',ModifyTime='"+CurrentTime+"'  where contno='"+ContNo+"'";

              String LJSGETENDORSESQL = "update LJSGETENDORSE  set agentcom='"+AgentComNew+"'";
              if(!AgentNew.equals(AgentOld))
              { LJSGETENDORSESQL+=",agentcode='"+AgentNew+"'  ,agentgroup='"+AgentNewGroup+"'"; }
              LJSGETENDORSESQL+=",ModifyDate='"+CurrentDate+"',ModifyTime='"+CurrentTime+"'  where contno='"+ContNo+"'";

              String LJSPAYBSQL = "update LJSPAYB  set agentcom='"+AgentComNew+"'";
              if(!AgentNew.equals(AgentOld))
              { LJSPAYBSQL+=",agentcode='"+AgentNew+"'  ,agentgroup='"+AgentNewGroup+"'"; }
              LJSPAYBSQL+=",ModifyDate='"+CurrentDate+"',ModifyTime='"+CurrentTime+"'  where OtherNo='"+ContNo+"'";

              String LJSPAYSQL = "update LJSPAY  set agentcom='"+AgentComNew+"'";
              if(!AgentNew.equals(AgentOld))
              { LJSPAYSQL+=",agentcode='"+AgentNew+"'  ,agentgroup='"+AgentNewGroup+"'"; }
              LJSPAYSQL+=",ModifyDate='"+CurrentDate+"',ModifyTime='"+CurrentTime+"'  where OtherNo='"+ContNo+"'";

              String LJAGETOTHERSQL = "update LJAGETOTHER   set agentcom='"+AgentComNew+"'";
              if(!AgentNew.equals(AgentOld))
              { LJAGETOTHERSQL+=",agentcode='"+AgentNew+"'  ,agentgroup='"+AgentNewGroup+"'"; }
              LJAGETOTHERSQL+=",ModifyDate='"+CurrentDate+"',ModifyTime='"+CurrentTime+"'  where  otherno in (select polno from lcpol where contno='"+ContNo+"')";

              String LJAGETENDORSESQL = "update LJAGETENDORSE  set agentcom='"+AgentComNew+"'";
              if(!AgentNew.equals(AgentOld))
              { LJAGETENDORSESQL+=",agentcode='"+AgentNew+"'  ,agentgroup='"+AgentNewGroup+"'"; }
              LJAGETENDORSESQL+=",ModifyDate='"+CurrentDate+"',ModifyTime='"+CurrentTime+"'  where contno='"+ContNo+"'";

              String LJAGETCLAIMSQL = "update LJAGETCLAIM  set agentcom='"+AgentComNew+"'";
              if(!AgentNew.equals(AgentOld))
              { LJAGETCLAIMSQL+=",agentcode='"+AgentNew+"'  ,agentgroup='"+AgentNewGroup+"'"; }
              LJAGETCLAIMSQL+=",ModifyDate='"+CurrentDate+"',ModifyTime='"+CurrentTime+"'  where contno='"+ContNo+"'";

              String LPPOLSQL = "update LPPOL  set agentcom='"+AgentComNew+"'";
              if(!AgentNew.equals(AgentOld))
              { LPPOLSQL+=",agentcode='"+AgentNew+"'  ,agentgroup='"+AgentNewGroup+"'"; }
              LPPOLSQL+=",ModifyDate='"+CurrentDate+"',ModifyTime='"+CurrentTime+"'  where contno='"+ContNo+"'";

              String LPCONTSQL = "update LPCONT  set agentcom='"+AgentComNew+"'";
              if(!AgentNew.equals(AgentOld))
              { LPCONTSQL+=",agentcode='"+AgentNew+"'  ,agentgroup='"+AgentNewGroup+"'"; }
              LPCONTSQL+=",ModifyDate='"+CurrentDate+"',ModifyTime='"+CurrentTime+"'  where contno='"+ContNo+"'";

              String LJTEMPFEESQL = "update LJTEMPFEE  set agentcom='"+AgentComNew+"'";
              if(!AgentNew.equals(AgentOld))
              { LJTEMPFEESQL+=",agentcode='"+AgentNew+"'  ,agentgroup='"+AgentNewGroup+"'"; }
              LJTEMPFEESQL+=",ModifyDate='"+CurrentDate+"',ModifyTime='"+CurrentTime+"'  where otherno='"+ContNo+"'";

              String LJSPAYPERSONBSQL = "update LJSPAYPERSONB  set agentcom='"+AgentComNew+"'";
              if(!AgentNew.equals(AgentOld))
               { LJSPAYPERSONBSQL+=",agentcode='"+AgentNew+"'  ,agentgroup='"+AgentNewGroup+"'"; }
              LJSPAYPERSONBSQL+=",ModifyDate='"+CurrentDate+"',ModifyTime='"+CurrentTime+"'  where otherno='"+ContNo+"'";

              String LJSPAYPERSONSQL = "update LJTEMPFEE  set agentcom='"+AgentComNew+"'";
              if(!AgentNew.equals(AgentOld))
              { LJSPAYPERSONSQL+=",agentcode='"+AgentNew+"'  ,agentgroup='"+AgentNewGroup+"'"; }
              LJSPAYPERSONSQL+=",ModifyDate='"+CurrentDate+"',ModifyTime='"+CurrentTime+"'  where otherno='"+ContNo+"'";

              String lacommisionSQL = "update lacommision  set agentcom='"+AgentComNew+"'";
              if(!AgentNew.equals(AgentOld))
              { lacommisionSQL+=",agentcode='"+AgentNew+"'  ,agentgroup='"+AgentNewGroup+"',branchattr=(select branchattr from labranchgroup where agentgroup='"+AgentNewGroup+"'),branchseries=(select branchseries from labranchgroup where agentgroup='"+AgentNewGroup+"'),branchcode=(select branchcode from laagent where agentcode='"+AgentNew+"')"; }
              lacommisionSQL+=",ModifyDate='"+CurrentDate+"',ModifyTime='"+CurrentTime+"'  where contno='"+ContNo+"'";

               map.put(LCContSQL, "UPDATE");
               map.put(LBContSQL, "UPDATE");
               map.put(LCPolSQL, "UPDATE");
               map.put(LBPolSQL, "UPDATE");
               map.put(LPPOLSQL, "UPDATE");
               map.put(LPCONTSQL, "UPDATE");
               map.put(ljapaySQL, "UPDATE");
               map.put(ljapaypersonSQL, "UPDATE");
               map.put(LJSGETCLAIMSQL, "UPDATE");
               map.put(LJSGETENDORSESQL, "UPDATE");
               map.put(LJSPAYBSQL, "UPDATE");
               map.put(LJSPAYSQL, "UPDATE");
               map.put(LJAGETOTHERSQL, "UPDATE");
               map.put(LJAGETENDORSESQL, "UPDATE");
               map.put(LJAGETCLAIMSQL, "UPDATE");
               map.put(LJTEMPFEESQL	, "UPDATE");
               map.put(LJSPAYPERSONBSQL, "UPDATE");
               map.put(LJSPAYPERSONSQL, "UPDATE");
               map.put(lacommisionSQL, "UPDATE");
               }
            }
          }

          catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ContChangeComBL";
            tError.functionName = "dealData";
            tError.errorMessage = "在处理所数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
          }
          return true;
        }

        /**
         * 准备往后层输出所需要的数据
         * 输出：如果准备数据时发生错误则返回false,否则返回true
         */
        private boolean prepareOutputData() {
          try {
            System.out.println(
                "Begin ContChangeComBL.prepareOutputData.........");
            mInputData.clear();
            mInputData.add(map);
          }
          catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ContChangeComBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
          }
          return true;
        }
 public ContChangeComBL() {
    }
    public VData getResult()
      {
          return this.mResult;
      }



}
