/*
 * <p>ClassName: OLACertificationBLS </p>
 * <p>Description: OLACertificationBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:07:04
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;

public class OLACertificationBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
//传输数据类
    private VData mInputData;
    /** 数据操作字符串 */
    private String mOperate;
    public OLACertificationBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        mInputData = (VData) cInputData.clone();
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            if (!saveLACertification())
                return false;
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            if (!deleteLACertification())
                return false;
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            if (!updateLACertification())
                return false;
        }
        return true;
    }

    /**
     * 保存函数
     */
    private boolean saveLACertification()
    {
        LACertificationSchema tLACertificationSchema = new
                LACertificationSchema();
        tLACertificationSchema = (LACertificationSchema) mInputData.
                                 getObjectByObjectName("LACertificationSchema",
                1);
        LACertificationSet tLACertificationSet = new LACertificationSet();
        tLACertificationSet = (LACertificationSet) mInputData.
                              getObjectByObjectName("LACertificationSet", 0);
        LAAgentSet tLAAgentSet = new LAAgentSet();
        tLAAgentSet = (LAAgentSet) mInputData.getObjectByObjectName(
                "LAAgentSet", 0);
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLACertificationBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            LACertificationDB tLACertificationDB = new LACertificationDB(conn);
            tLACertificationDB.setSchema(tLACertificationSchema);
            if (!tLACertificationDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLACertificationDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLACertificationBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
             if (tLAAgentSet!=null && tLAAgentSet.size() >0)
          {
              LAAgentDBSet tLAAgentDBSet = new LAAgentDBSet(conn);
              tLAAgentDBSet.add(tLAAgentSet);

              if (!tLAAgentDBSet.update())
              {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLAAgentDBSet.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "OLACertificationBLS";
                  tError.functionName = "saveData";
                  tError.errorMessage = "数据保存失败!";
                  this.mErrors.addOneError(tError);
                  conn.rollback();
                  conn.close();
                  return false;
              }
          }
            if (tLACertificationSet!=null && tLACertificationSet.size() >0)
            {
                LACertificationDBSet tLACertificationDBSet = new
                        LACertificationDBSet(conn);
                tLACertificationDBSet.add(tLACertificationSet);
                if (!tLACertificationDBSet.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLACertificationDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "OLACertificationBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            conn.commit();
            conn.close();
    }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLACertificationBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }

    /**
     * 保存函数
     */
    private boolean deleteLACertification()
    {
        LACertificationSchema tLACertificationSchema = new
                LACertificationSchema();
        tLACertificationSchema = (LACertificationSchema) mInputData.
                                 getObjectByObjectName("LACertificationSchema",
                1);
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLACertificationBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LACertificationDB tLACertificationDB = new LACertificationDB(conn);
            tLACertificationDB.setSchema(tLACertificationSchema);
            if (!tLACertificationDB.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLACertificationDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLACertificationBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLACertificationBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }

    /**
     * 保存函数
     */
    private boolean updateLACertification()
    {
        LACertificationSchema tupLACertificationSchema = new
                LACertificationSchema();
        tupLACertificationSchema = (LACertificationSchema) mInputData.
                                 getObjectByObjectName("LACertificationSchema",0);
        LACertificationSchema tLACertificationSchema = new
                LACertificationSchema();
        tLACertificationSchema = (LACertificationSchema) mInputData.
                                 getObjectByObjectName("LACertificationSchema",1);
        LACertificationSet tLACertificationSet = new
               LACertificationSet();
       tLACertificationSet = (LACertificationSet) mInputData.
                                getObjectByObjectName("LACertificationSet",0);
          LAAgentSet tLAAgentSet = new LAAgentSet();
       tLAAgentSet = (LAAgentSet) mInputData.getObjectByObjectName(
               "LAAgentSet", 0);



        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "OLACertificationBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LACertificationDB tupLACertificationDB = new LACertificationDB(conn);
            tupLACertificationDB.setSchema(tupLACertificationSchema);
            if (!tupLACertificationDB.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tupLACertificationDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLACertificationBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LACertificationDB tLACertificationDB = new LACertificationDB(conn);
            tLACertificationDB.setSchema(tLACertificationSchema);
            if (!tLACertificationDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLACertificationDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLACertificationBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            if (tLACertificationSet!=null && tLACertificationSet.size() >0)
           {
               LACertificationDBSet tLACertificationDBSet = new
                       LACertificationDBSet(conn);
               tLACertificationDBSet.add(tLACertificationSet);
               if (!tLACertificationDBSet.update())
               {
                   // @@错误处理
                   this.mErrors.copyAllErrors(tLACertificationDBSet.mErrors);
                   CError tError = new CError();
                   tError.moduleName = "OLACertificationBLS";
                   tError.functionName = "saveData";
                   tError.errorMessage = "数据保存失败!";
                   this.mErrors.addOneError(tError);
                   conn.rollback();
                   conn.close();
                   return false;
               }
           }
           if (tLAAgentSet!=null && tLAAgentSet.size() >0)
         {
             LAAgentDBSet tLAAgentDBSet = new LAAgentDBSet(conn);
             tLAAgentDBSet.add(tLAAgentSet);

             if (!tLAAgentDBSet.update())
             {
                 // @@错误处理
                 this.mErrors.copyAllErrors(tLAAgentDBSet.mErrors);
                 CError tError = new CError();
                 tError.moduleName = "OLACertificationBLS";
                 tError.functionName = "saveData";
                 tError.errorMessage = "数据保存失败!";
                 this.mErrors.addOneError(tError);
                 conn.rollback();
                 conn.close();
                 return false;
             }
         }


            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLACertificationBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        }
        return true;
    }
}
