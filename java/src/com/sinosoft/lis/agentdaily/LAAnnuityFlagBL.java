package com.sinosoft.lis.agentdaily;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAAnnuityFlagSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LAAnnuityFlagBSchema;
import com.sinosoft.lis.db.LAAnnuityFlagDB;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.Reflections;
import java.util.Map;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LAAnnuityFlagBL {
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
        public CErrors mErrors = new CErrors();
        private VData mResult = new VData();
        /** 往后面传输数据的容器 */
        private VData mInputData;
        /** 数据操作字符串 */
        private String mOperate;
        /** 全局数据 */
        private GlobalInput mGlobalInput = new GlobalInput();
        private String CurrentDate = PubFun.getCurrentDate();
        private String CurrentTime = PubFun.getCurrentTime();
        //录入
        private LAAnnuityFlagSchema mLAAnnuityFlagSchema = new LAAnnuityFlagSchema();
        //修改
        private LAAnnuityFlagSchema mupLAAnnuityFlagSchema = new LAAnnuityFlagSchema();
        //备份
        private LAAnnuityFlagBSchema mLAAnnuityFlagBSchema = new LAAnnuityFlagBSchema();
        private MMap mMap=new MMap();
        public LAAnnuityFlagBL() {
        }

        public static void main(String[] args)
        {
            LAAnnuityFlagBL tLAAnnuityFlagBL   = new LAAnnuityFlagBL();
            LAAnnuityFlagSchema tLAAnnuityFlagSchema = new LAAnnuityFlagSchema();
            tLAAnnuityFlagSchema.setManageCom("86110000");
            tLAAnnuityFlagSchema.setAgentCode("1101000001");
            tLAAnnuityFlagSchema.setAgentGroup("1");
            tLAAnnuityFlagSchema.setBranchCode("1");
            tLAAnnuityFlagSchema.setFlag("N");
            tLAAnnuityFlagSchema.setBranchType("1");
            tLAAnnuityFlagSchema.setBranchType2("01");

            VData tVData = new VData();
            tVData.add(tLAAnnuityFlagSchema);
            GlobalInput tG = new GlobalInput();
            tG.Operator = "001";
            tG.ManageCom = "86";
            tVData.addElement(tG);
            System.out.println("111");
            tLAAnnuityFlagBL.submitData(tVData,"INSERT||MAIN");

        }

        public boolean submitData(VData cInputData, String cOperate)
      {
          //将操作数据拷贝到本类中
          this.mOperate = cOperate;
          //得到外部传入的数据,将数据备份到本类中
          if (!getInputData(cInputData))
          {
              return false;
          }
          //进行业务处理
          if (!dealData())
          {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "LAAnnuityFlagBL";
              tError.functionName = "submitData";
              tError.errorMessage = "数据处理失败LAAnnuityFlagBL-->dealData!";
              this.mErrors.addOneError(tError);
              return false;
          }
          //准备往后台的数据
          if (!prepareOutputData())
          {
              return false;
          }
          if (this.mOperate.equals("QUERY||MAIN"))
          {
              this.submitquery();
          }
          else
          {
              PubSubmit tPubSubmit = new PubSubmit();
              System.out.println("Start AgentWageCalSaveNewBL Submit...");
              if (!tPubSubmit.submitData(mInputData, mOperate))
              {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "AgentWageCalSaveNewBL";
                  tError.functionName = "submitData";
                  tError.errorMessage = "数据提交失败!";
                  this.mErrors.addOneError(tError);
                  return false;
                 }
          }
          mInputData = null;
          return true;
      }
      private boolean getInputData(VData cInputData)
      {
           this.mLAAnnuityFlagSchema.setSchema((LAAnnuityFlagSchema) cInputData.
                                        getObjectByObjectName(
                                                "LAAnnuityFlagSchema", 0));
         this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                       getObjectByObjectName("GlobalInput", 0));
      //     this.mContType=(String)cInputData.getObjectByObjectName("String",0);

           return true;
       }
       private boolean dealData()
       {
           if (mOperate.equals("INSERT||MAIN"))
           {
               LAAnnuityFlagDB tLAAnnuityFlagDB = new LAAnnuityFlagDB();
               tLAAnnuityFlagDB.setAgentCode(mLAAnnuityFlagSchema.getAgentCode());
               if(tLAAnnuityFlagDB.getInfo())
               {
                   this.mErrors.copyAllErrors(tLAAnnuityFlagDB.mErrors);
                   CError tError = new CError();
                   tError.moduleName = "LAAnnuityFlagBL";
                   tError.functionName = "dealData";
                   tError.errorMessage = "代理人" + mLAAnnuityFlagSchema.getAgentCode() + "养老金缴纳信息已经存在，不能保存，但可以修改！";
                   this.mErrors.addOneError(tError);
                         return false;
              }
                mLAAnnuityFlagSchema.setOperator(mGlobalInput.Operator);
                mLAAnnuityFlagSchema.setMakeDate(CurrentDate);
                mLAAnnuityFlagSchema.setMakeTime(CurrentTime);
                mLAAnnuityFlagSchema.setModifyDate(CurrentDate);
                mLAAnnuityFlagSchema.setModifyTime(CurrentTime);
                mMap.put(this.mLAAnnuityFlagSchema,"INSERT") ;
           }
          if (mOperate.equals("UPDATE||MAIN"))
          {

              LAAnnuityFlagDB tLAAnnuityFlagDB = new LAAnnuityFlagDB();
              tLAAnnuityFlagDB.setAgentCode(mLAAnnuityFlagSchema.getAgentCode());
              if(!tLAAnnuityFlagDB.getInfo())
              {
                  this.mErrors.copyAllErrors(tLAAnnuityFlagDB.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "LAAnnuityFlagBL";
                  tError.functionName = "dealData";
                  tError.errorMessage = "代理人" + mLAAnnuityFlagSchema.getAgentCode() + "查询养老金缴纳信息出错！";
                  this.mErrors.addOneError(tError);
                        return false;
              }
              mupLAAnnuityFlagSchema=tLAAnnuityFlagDB.getSchema();
              String tEdorNo = PubFun1.CreateMaxNo("AAnnuityEdorNo", 20);

              Reflections tReflections=new Reflections();
              //备份原来的信息
              tReflections.transFields(mLAAnnuityFlagBSchema,mupLAAnnuityFlagSchema);
              mLAAnnuityFlagBSchema.setAnnuityEdorNo(tEdorNo);
              //缴纳标记修改：AnnuityEdorType= 02 ，缴纳标记没有修改，即原始的修改 AnnuityEdorType=01
              if(mLAAnnuityFlagSchema.getFlag().equals(mupLAAnnuityFlagSchema.getFlag()))
              {
               mLAAnnuityFlagBSchema.setAnnuityEdorType("01");
              }
              else
              {
                  mLAAnnuityFlagBSchema.setAnnuityEdorType("02");
              }
              //修改
              mupLAAnnuityFlagSchema.setFlag(mLAAnnuityFlagSchema.getFlag());
              mupLAAnnuityFlagSchema.setOperator(mGlobalInput.Operator);
              mupLAAnnuityFlagSchema.setModifyDate(CurrentDate);
              mupLAAnnuityFlagSchema.setModifyTime(CurrentTime);
              mLAAnnuityFlagSchema=null;
              mMap.put(this.mupLAAnnuityFlagSchema,"UPDATE") ;
              mMap.put(this.mLAAnnuityFlagBSchema,"INSERT") ;
          }

           return true;
       }
       private boolean submitquery()
       {
           return true;
       }
       private boolean prepareOutputData()
        {
            try
            {
                mInputData = new VData();
                this.mInputData.add(mMap);
            }
            catch (Exception ex)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAAnnuityFlagBL";
                tError.functionName = "prepareData";
                tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
                this.mErrors.addOneError(tError);
                return false;
            }
            return true;
        }

        public VData getResult()
           {
               this.mResult .clear() ;
               return mResult;
        }

}
