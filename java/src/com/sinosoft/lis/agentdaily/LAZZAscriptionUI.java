package com.sinosoft.lis.agentdaily;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.agentdaily.LAZZAscriptionBL;
/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public class LAZZAscriptionUI
{
    public CErrors mErrors=new CErrors();
    public VData mResult =new VData();
    private VData mInputData=new VData();
    private String mOperate="";
    public LAZZAscriptionUI()
    {
    }
    public boolean submitData(VData cInputData,String cOperate)
    {
        System.out.println("11111111113244444444444444444444444444444");
        System.out.println();
        mOperate=cOperate;
        this.mInputData =(VData)cInputData.clone() ;
        System.out.println("??------------?????");
        LAZZAscriptionBL tLAZZAscriptionBL = new LAZZAscriptionBL();
        System.out.println("-------------------"+tLAZZAscriptionBL.hashCode());
        try
        {   
            if (!tLAZZAscriptionBL.submitData(mInputData, mOperate))
            {
                this.mErrors.copyAllErrors(tLAZZAscriptionBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAZZAscriptionUI";
                tError.functionName = "submitDat";
                tError.errorMessage = "BL类处理失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        mResult=tLAZZAscriptionBL.getResult();
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

}
