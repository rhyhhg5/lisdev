package com.sinosoft.lis.agentdaily;


import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author luomin
 * @version 1.1
 */
public class DiskImportAgentInfoUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private DiskImportAgentInfoBL mDiskImportAgentInfoBL = null;

    public DiskImportAgentInfoUI()
    {
        mDiskImportAgentInfoBL = new DiskImportAgentInfoBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
        if(!mDiskImportAgentInfoBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mDiskImportAgentInfoBL.mErrors);
            return false;
        }
        //
        if(mDiskImportAgentInfoBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mDiskImportAgentInfoBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mDiskImportAgentInfoBL.getImportPersons();
    }



    public static void main(String[] args)
    {
        DiskImportAgentInfoUI zDiskImportAgentInfoUI = new DiskImportAgentInfoUI();
    }
}
