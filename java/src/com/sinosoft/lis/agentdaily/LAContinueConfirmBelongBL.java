/*
 * <p>ClassName: ALAAscriptionBL </p>
 * <p>Description: ALAAscriptionBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentdaily;


import com.sinosoft.lis.db.LAAscriptionDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAscriptionSchema;
import com.sinosoft.lis.vschema.LAAscriptionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;


public class LAContinueConfirmBelongBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
  //  private int mLength=1000;//得到一次提交的长度
    /** 业务处理相关变量 */

    private LAAscriptionSet    mLAAscriptionSet = new LAAscriptionSet();
    private String mFlag = "";
    private String mMakeType = "";
    private  String currentDate = PubFun.getCurrentDate();//当前日期
    private  String currentTime = PubFun.getCurrentTime();//当期时间
    
    private MMap map = new MMap();
    
    public LAContinueConfirmBelongBL()
    {

    }

    public static void main(String[] args)
    {
        LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
        tLAAscriptionSchema.setAgentOld("");
        tLAAscriptionSchema.setNoti("");
        tLAAscriptionSchema.setBranchType("1");
        tLAAscriptionSchema.setBranchType2("01");
        tLAAscriptionSchema.setAgentNew("86110000");
        String tFlag="ALL";
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.ComCode = "86";
        tGlobalInput.Operator = "mak001";
        //  tVData.add(tLAAscriptionSchema);
        LAAscriptionSet tLAAscriptionSet = new LAAscriptionSet();
        tLAAscriptionSet.add(tLAAscriptionSchema);
        VData tVData = new VData();
        tVData.addElement(tLAAscriptionSet);
        tVData.add(tGlobalInput);
        tVData.add(tFlag);
        LAContinueConfirmBelongBL bl =new LAContinueConfirmBelongBL();
        bl.submitData(tVData,"UPDATE||MAIN");
    }

    /**
    * 传输数据的公共方法
    * @param: cInputData 输入的数据
    *         cOperate 数据操作
    * @return:
    */
   public boolean submitData(VData cInputData, String cOperate)
   {
       //将操作数据拷贝到本类中
       this.mOperate = cOperate;
       //得到外部传入的数据,将数据备份到本类中
       if (!getInputData(cInputData))
       {
           return false;
       }
       //进行业务处理
       if (!dealData())
       {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "LAZGAscriptionEnsureBL";
           tError.functionName = "submitData";
           tError.errorMessage = "数据处理失败LAContinueConfirmBelongBL-->dealData!";
           this.mErrors.addOneError(tError);
           return false;
       }
       
       return true;
   }

   private boolean dealData()
  {
      if(mFlag==null || mFlag.equals("")){
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "LAContinueConfirmBelongBL";
           tError.functionName = "submitData";
           tError.errorMessage = "参数传递错误!";
           this.mErrors.addOneError(tError);
           return false;
      }

      if (mOperate.equals("UPDATE||MAIN"))
      {
          if(mFlag.equals("ALL"))
          {//全部确认
              LAAscriptionDB tLAAscriptionDB = new LAAscriptionDB();
              LAAscriptionSet tLAAscriptionSet = new LAAscriptionSet();
              LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
              tLAAscriptionSchema = mLAAscriptionSet.get(1);
              mLAAscriptionSet.clear();
              String tagentold = tLAAscriptionSchema.getAgentOld();
              String tmanagecom = tLAAscriptionSchema.getAgentNew(); //传值 时用AgentNew传 managecom
              String tname = tLAAscriptionSchema.getNoti(); //传值 时用Noti传 业务员姓名
              if ((tagentold != null && !tagentold.equals(""))
            		  ||(tname != null && !tname.equals(""))) {
                  //sql += " and a.agentold='" + tagentold + "'  ";
            	  String sql ="select * from ";
                  sql +=" laascription a where   a.ascripstate='2'";
                  sql +=" and a.validflag='N' and a.branchtype='" +tLAAscriptionSchema.getBranchType() 
                      + "' and a.branchtype2='" +tLAAscriptionSchema.getBranchType2() 
                      + "' and MakeType in ('03','04') ";
                  if (tagentold != null && !tagentold.equals("")) {
                      sql += " and a.agentold='" + tagentold + "'  ";
                  }
                  sql += " and exists (select c.agentcode from laagent c where c.ManageCom='" 
                      + tmanagecom +"' and a.AgentOld=c.agentcode   ";
                  if (tname != null && !tname.equals("")) {
                      sql += " and c.name='" + tname + "') ";
                  }
                  else
                  {
                       sql += " )";
                  }
                  tLAAscriptionSet = tLAAscriptionDB.executeQuery(sql);
                  if (!dealCont(tLAAscriptionSet))
                  {
                     return false;
                  }
              }else{
            	  String tSQL="select distinct agentold from laascription"
            		  +" where ascripstate='2' and validflag='N' and branchtype='" 
            		  +tLAAscriptionSchema.getBranchType() 
                      + "' and branchtype2='" +tLAAscriptionSchema.getBranchType2() 
                      + "' and MakeType in ('03','04') and managecom like '"
                      +tmanagecom+"%' with ur";
            	  ExeSQL tExeSQL=new ExeSQL();
            	  SSRS tSSRS=new SSRS();
            	  tSSRS=tExeSQL.execSQL(tSQL);
            	  for(int i=1;i<=tSSRS.getMaxRow();i++){
            		  String sql ="select * from ";
                      sql +=" laascription a where   a.ascripstate='2'";
                      sql +=" and a.validflag='N' and a.branchtype='" +tLAAscriptionSchema.getBranchType() 
                          + "' and a.branchtype2='" +tLAAscriptionSchema.getBranchType2() 
                          + "' and MakeType in ('03','04')";
                      sql += " and a.agentold='" + tSSRS.GetText(i,1) + "'  ";
                      sql += " and exists (select c.agentcode from laagent c where c.ManageCom='" 
                          + tmanagecom +"' and a.AgentOld=c.agentcode )";
                      tLAAscriptionSet = tLAAscriptionDB.executeQuery(sql);
                      if (!dealCont(tLAAscriptionSet))
                      {
                         return false;
                      }
            	  }
              }
          }
          else
          {//选择确认
              LAAscriptionSet tLAAscriptionSet = new LAAscriptionSet();
              tLAAscriptionSet.set(mLAAscriptionSet);
              mLAAscriptionSet.clear();
              if (!dealCont(tLAAscriptionSet)) {
                  return false;
              }
          }
      }
          return true;

  }

   private boolean getInputData(VData cInputData)
     {
         this.mLAAscriptionSet.set((LAAscriptionSet) cInputData.
                                            getObjectByObjectName(
                 "LAAscriptionSet", 0));
         this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                     getObjectByObjectName("GlobalInput", 0));
         this.mFlag=(String)cInputData.getObjectByObjectName("String",0);
         this.mMakeType = (String)cInputData.get(3);
         System.out.println("mFlag:"+mFlag);
         System.out.println("mMakeType:"+mMakeType);
         return true;
     }
    private boolean dealCont(LAAscriptionSet tLAAscriptionSet)
    {
      for (int i = 1; i <= tLAAscriptionSet.size(); i++) {
      
      LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
      tLAAscriptionSchema = tLAAscriptionSet.get(i);

      String tAscripNo = tLAAscriptionSchema.getAscripNo();
      
      LAAscriptionDB tLAAscriptionDB = new LAAscriptionDB();
      tLAAscriptionDB.setAscripNo(tAscripNo);
      if (!tLAAscriptionDB.getInfo()) {
          this.mErrors.copyAllErrors(tLAAscriptionDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "LAContinueConfirmBelongBL";
          tError.functionName = "dealData";
          tError.errorMessage = "查找归属表基本信息时失败!";
          this.mErrors.addOneError(tError);
          return false;
      }
      tLAAscriptionSchema = tLAAscriptionDB.getSchema();
      //
      tLAAscriptionSchema.setAscriptionDate(currentDate);
      
      String tGrpContNo = tLAAscriptionSchema.getGrpContNo();
      String tContNo = tLAAscriptionSchema.getContNo();
      String tAgentNew = tLAAscriptionSchema.getAgentNew();
      String tAgentOld = tLAAscriptionSchema.getAgentOld();
      System.out.println("++++"+tLAAscriptionSchema.getAscripNo());
      //校验新人员是否在职
      String pcheck="select * from laagent  where agentcode='"+tAgentNew+"' "
      +" and agentstate<='02' ";
      SSRS tcheck = new SSRS();
      ExeSQL tcheck5 = new ExeSQL();
      tcheck = tcheck5.execSQL(pcheck);
      int tch5 = tcheck.getMaxRow();
      if (tch5<1) {
    	  CError tError = new CError();
          tError.moduleName = "LAContinueConfirmBelongBL";
          tError.functionName = "dealData";
          tError.errorMessage = "保单"+tLAAscriptionSchema.getContNo()+"/"+tLAAscriptionSchema.getGrpContNo()+"新负责人"+tAgentNew+"已经离职，请重新进行分配及归属！";
          this.mErrors.addOneError(tError);
          return false;  
      }      

      String sql1 = "select a.agentgroup,a.branchcode,b.branchattr,b.branchseries,a.managecom,a.Name "
      	 +" from laagent a , labranchgroup b "
      	 +" where a.agentgroup=b.agentgroup and a.agentcode='" +tAgentNew + "'";
      SSRS tSSRS = new SSRS();
      ExeSQL tExeSQL2 = new ExeSQL();
      tSSRS = tExeSQL2.execSQL(sql1);
      if (tExeSQL2.mErrors.needDealError()) {
          this.mErrors.copyAllErrors(tExeSQL2.mErrors);
          CError tError = new CError();
          tError.moduleName = "LAContinueConfirmBelongBL";
          tError.functionName = "dealData";
          tError.errorMessage = "查询新代理人信息出错！";
          this.mErrors.addOneError(tError);
          return false;
      }
      String tAgentGroup = tSSRS.GetText(1, 1);
      String tBranchCode = tSSRS.GetText(1, 2);
      String tBranchAttr = tSSRS.GetText(1, 3);
      String tBranchSeries = tSSRS.GetText(1, 4);
      String tManageCom = tSSRS.GetText(1, 5);
      String tName = tSSRS.GetText(1, 6);

   

        //团单
      if(tLAAscriptionSchema.getGrpContNo()!= null&&!tLAAscriptionSchema.getGrpContNo().trim().equals("")){
        String tSQL="select count('1') from lcgrpcont where  grpcontno='"+tGrpContNo+"'";      
        SSRS tSSRS1 = new SSRS();
        ExeSQL tExeSQL1 = new ExeSQL();
        tSSRS1 = tExeSQL1.execSQL(tSQL);
        int tCount1 = tSSRS1.getMaxRow();
        if (tCount1>=1) {
        	 String updateSQL="update lcgrpcont set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
         	+" ,modifytime=current time  where grpcontno='"+tGrpContNo+"' ";
        	 this.map.put(updateSQL, "UPDATE");
        	 updateSQL="update lccont set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
          	+" ,modifytime=current time  where  grpcontno='"+tGrpContNo+"'";
         	 this.map.put(updateSQL, "UPDATE");
         	updateSQL="update lcgrppol set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
         	+" ,modifytime=current time  where  grpcontno='"+tGrpContNo+"'";
        	 this.map.put(updateSQL, "UPDATE");
        	 updateSQL="update lcpol set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
          	+" ,modifytime=current time  where  grpcontno='"+tGrpContNo+"'";
         	 this.map.put(updateSQL, "UPDATE");
        }
        
        String tSQL1="select count('1') from lbgrpcont where  grpcontno='"+tGrpContNo+"'";      
        SSRS tSSRS2 = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS2 = tExeSQL.execSQL(tSQL1);
        int tCount = tSSRS2.getMaxRow();
        if (tCount>=1) {
        	 String updateSQL="update lbgrpcont set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
         	+" ,modifytime=current time  where  grpcontno='"+tGrpContNo+"'";
        	 this.map.put(updateSQL, "UPDATE");
        	 updateSQL="update lbcont set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
          	+" ,modifytime=current time  where  grpcontno='"+tGrpContNo+"'";
         	 this.map.put(updateSQL, "UPDATE");
         	updateSQL="update lbgrppol set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
         	+" ,modifytime=current time  where  grpcontno='"+tGrpContNo+"'";
        	 this.map.put(updateSQL, "UPDATE");
        	 updateSQL="update lbpol set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
          	+" ,modifytime=current time  where  grpcontno='"+tGrpContNo+"'";
         	 this.map.put(updateSQL, "UPDATE");
        }
      
        String tSQL3="select count('1') from lpgrpcont where  grpcontno='"+tGrpContNo+"'";      
        SSRS tSSRS3 = new SSRS();
        ExeSQL tExeSQL3 = new ExeSQL();
        tSSRS3 = tExeSQL3.execSQL(tSQL3);
        int tCount3 = tSSRS3.getMaxRow();
        if (tCount3>=1) {
        	 String updateSQL="update lpgrpcont set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
         	+" ,modifytime=current time  where  grpcontno='"+tGrpContNo+"'";
        	 this.map.put(updateSQL, "UPDATE");
        	 updateSQL="update lpcont set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
          	+" ,modifytime=current time  where  grpcontno='"+tGrpContNo+"'";
         	 this.map.put(updateSQL, "UPDATE");
         	updateSQL="update lpgrppol set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
         	+" ,modifytime=current time  where  grpcontno='"+tGrpContNo+"'";
        	 this.map.put(updateSQL, "UPDATE");
        	 updateSQL="update lppol set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
          	+" ,modifytime=current time  where  grpcontno='"+tGrpContNo+"'";
         	 this.map.put(updateSQL, "UPDATE");
        }
        
        String tSQL4="select count('1') from LJSPayGrp where  grpcontno='"+tGrpContNo+"' ";      
        SSRS tSSRS4 = new SSRS();
        ExeSQL tExeSQL4 = new ExeSQL();
        tSSRS3 = tExeSQL4.execSQL(tSQL4);
        int tCount4 = tSSRS4.getMaxRow();
        if (tCount4>=1) {
        	 String updateSQL="update LJSPayGrp set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
         	+" ,modifytime=current time  where  grpcontno='"+tGrpContNo+"'";
        	 this.map.put(updateSQL, "UPDATE");
        	 updateSQL="update ljspayperson set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
             +" ,managecom='"+tManageCom +"' ,modifydate=current date"
         	+" ,modifytime=current time  where grpcontno='"+ tGrpContNo+"'";
         	 this.map.put(updateSQL, "UPDATE");
         	updateSQL="update LJSPay set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
            +" ,managecom='"+tManageCom +"' ,modifydate=current date"
        	+" ,modifytime=current time  where OtherNo='"+ tGrpContNo+"'";
        	 this.map.put(updateSQL, "UPDATE");
        	
        }
        
        String upSQL="update LJSGetEndorse set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
        +" ,managecom='"+tManageCom +"' ,modifydate=current date"
    	+" ,modifytime=current time  where OtherNo='"+ tGrpContNo+"'";
    	 this.map.put(upSQL, "UPDATE");
      
        String pSQLl="select * from ljapaygrp where grpcontno='"+tGrpContNo+"' "
	      +" and makedate='"+currentDate+"' "
	      +" and agentcode='"+tAgentOld+"'";
        SSRS tS5 = new SSRS();
        ExeSQL tExe5 = new ExeSQL();
        tS5 = tExe5.execSQL(pSQLl);
        int tCou5 = tS5.getMaxRow();
        //恰好今天收费
        if (tCou5>=1) {
      	  String
          	ttSQL="update Ljapay set agentcode='"+tAgentNew+"',agentgroup='"+tAgentGroup
          	+"',modifydate=current date,modifytime=current time "
          		+" where incomeno='"+tGrpContNo+"'  and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"' ";
          	this.map.put(ttSQL, "UPDATE");  
          	ttSQL="update Ljapaygrp set agentcode='"+tAgentNew+"',agentgroup='"+tAgentGroup
          	+"',modifydate=current date,modifytime=current time "
          		+" where grpcontno='"+tGrpContNo+"'  and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"' ";
          	this.map.put(ttSQL, "UPDATE");  
          	ttSQL="update Ljapayperson set agentcode='"+tAgentNew+"',agentgroup='"+tAgentGroup
          	+"',modifydate=current date,modifytime=current time "
          		+" where grpcontno='"+tGrpContNo+"'  and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"' ";
          	this.map.put(ttSQL, "UPDATE");  
          	ttSQL="update ljagetendorse set agentcode='"+tAgentNew+"',agentgroup='"+tAgentGroup
          	+"',modifydate=current date,modifytime=current time "
          		+" where grpcontno='"+tGrpContNo+"'  and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"' ";
          	this.map.put(ttSQL, "UPDATE");   
        }
        
        
      }

      //个单
      if(tLAAscriptionSchema.getContNo()!= null&&!tLAAscriptionSchema.getContNo().trim().equals("")){
      //修改LCCont表中的AgentGroup,AgentCode,manageCom
      	String tSQL1="select count('1') from LCCont where  prtno in (select prtno from lccont where contno='"+tContNo+"' ) ";
          SSRS tSSRS1 = new SSRS();
          ExeSQL tExeSQL1 = new ExeSQL();
          tSSRS1 = tExeSQL1.execSQL(tSQL1);
          int tCount1 = tSSRS1.getMaxRow();
          if (tCount1>=1) {
          	 String updateSQL="update LCCont set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
           	 +" ,modifytime=current time  where   prtno in (select prtno from lccont where contno='"+tContNo+"' ) ";
          	 this.map.put(updateSQL, "UPDATE");
          	 updateSQL="update LCPol set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
               +" ,managecom='"+tManageCom +"' ,modifydate=current date"
           	+" ,modifytime=current time  where  prtno in (select prtno from lccont where contno='"+tContNo+"' ) ";
           	 this.map.put(updateSQL, "UPDATE");
          }
          
          String tSQL2="select count('1') from LBCont where  prtno in (select prtno from lbcont where contno='"+tContNo+"' ) ";
          SSRS tSSRS2 = new SSRS();
          ExeSQL tExeSQL21 = new ExeSQL();
          tSSRS2 = tExeSQL21.execSQL(tSQL2);
          int tCount2 = tSSRS2.getMaxRow();
          if (tCount2>=1) {
          	 String updateSQL="update LBCont set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
           	 +" ,modifytime=current time  where   prtno in (select prtno from lbcont where contno='"+tContNo+"' ) ";
          	 this.map.put(updateSQL, "UPDATE");
          	 updateSQL="update LBPol set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
               +" ,managecom='"+tManageCom +"' ,modifydate=current date"
           	+" ,modifytime=current time  where  prtno in (select prtno from lbcont where contno='"+tContNo+"' ) ";
           	 this.map.put(updateSQL, "UPDATE");
          }
      
          
          String tSQL3="select count('1') from lpcont where prtno  in  (select prtno from lccont where contno = '"+tContNo+"'" +
                  " union" +
                  " select prtno from lbcont where contno = '"+tContNo+"')";
          SSRS tSSRS3 = new SSRS();
          ExeSQL tExeSQL3 = new ExeSQL();
          tSSRS3 = tExeSQL3.execSQL(tSQL3);
          int tCount3 = tSSRS3.getMaxRow();
          if (tCount3>=1) {
          	 String updateSQL="update lpcont set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
           	 +" ,modifytime=current time  where  prtno in (select prtno from lccont where contno = '"+tContNo+"'" +
                  " union" +
                  " select prtno from lbcont where contno = '"+tContNo+"')";
          	 this.map.put(updateSQL, "UPDATE");
          	 updateSQL="update LPPol set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
               +" ,managecom='"+tManageCom +"' ,modifydate=current date"
           	+" ,modifytime=current time  where  prtno in (select prtno from lccont where contno='"+tContNo+"' )";
           	 this.map.put(updateSQL, "UPDATE");
          }
          
          String tSQL4="select count('1') from LJSPayPerson  where contno = '"+tContNo+"' ";
          SSRS tSSRS4 = new SSRS();
          ExeSQL tExeSQL4 = new ExeSQL();
          tSSRS4 = tExeSQL4.execSQL(tSQL4);
          int tCount4 = tSSRS4.getMaxRow();
          if (tCount4>=1) {
    	      String updateSQL="update LJSPayPerson set AgentCode='"+tAgentNew+"' ,AgentGroup='"+tAgentGroup+"' ,managecom='"+tManageCom +"' ,modifydate=current date"
     	      +" ,modifytime=current time  where contno = '"+tContNo+"' ";
    	      this.map.put(updateSQL, "UPDATE");
    	      updateSQL="update LJSPay set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
              +" ,managecom='"+tManageCom +"' ,modifydate=current date"
     	      +" ,modifytime=current time  where OtherNo = '"+tContNo+"' ";
     	      this.map.put(updateSQL, "UPDATE");
     	      
           }
  
          String updSQL="update LJSGetEndorse set agentcode='"+tAgentNew+"' ,agentgroup='"+tAgentGroup +"'"
          +" ,managecom='"+tManageCom +"' ,modifydate=current date"
	      +" ,modifytime=current time  where OtherNo = '"+tContNo+"' ";
	      this.map.put(updSQL, "UPDATE");
          String update="update LCContGetPol set agentcode='"+tAgentNew+"' ,managecom='"+tManageCom +"' ,agentname='"+tName+"' ,modifydate=current date"
	      +" ,modifytime=current time  where ContNo = '"+tContNo+"' ";
	      this.map.put(update, "UPDATE");
          
	      
	     
          String pSQLl="select * from ljapayperson where contno='"+tContNo+"' "
	      +" and makedate='"+currentDate+"' "
	      +" and agentcode='"+tAgentOld+"'";
          SSRS tS5 = new SSRS();
          ExeSQL tExe5 = new ExeSQL();
          tS5 = tExe5.execSQL(pSQLl);
          int tCou5 = tS5.getMaxRow();
          //恰好今天收费
          if (tCou5>=1) {
        	  String 
            	ttSQL="update Ljapay set agentcode='"+tAgentNew+"',agentgroup='"+tAgentGroup
            	+"',modifydate=current date,modifytime=current time "
            		+" where incomeno='"+tContNo+"'  and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"' ";
            	this.map.put(ttSQL, "UPDATE");  
            	ttSQL="update Ljapayperson set agentcode='"+tAgentNew+"',agentgroup='"+tAgentGroup
            	+"',modifydate=current date,modifytime=current time "
            		+" where contno='"+tContNo+"'  and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"' ";
            	this.map.put(ttSQL, "UPDATE");  
            	ttSQL="update ljagetendorse set agentcode='"+tAgentNew+"',agentgroup='"+tAgentGroup
            	+"',modifydate=current date,modifytime=current time "
            		+" where contno='"+tContNo+"'  and makedate>='"+tLAAscriptionSchema.getAscriptionDate()+"' ";
            	this.map.put(ttSQL, "UPDATE");   
          }
 
      }
      
      String endSQL="update  LAAscription set AscripState='3' ,AscriptionDate=current date  ,modifydate=current date,modifytime =current time"
      +" where AscripNo='"+tAscripNo+"'";
      this.map.put(endSQL, "UPDATE");
      
//    准备往后台的数据
      if (!prepareOutputData())
      {
          return false;
      }
      
      System.out.println("Start LAContinueConfirmBelongBL Submit...");
      

      PubSubmit tPubSubmit = new PubSubmit();
      if (!tPubSubmit.submitData(mInputData, ""))
      {
          // @@错误处理
          this.mErrors.copyAllErrors(tPubSubmit.mErrors);
          CError tError = new CError();
          tError.moduleName = "LAContinueConfirmBelongBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
          this.mErrors.addOneError(tError);
          return false;
      }
      mInputData = null;
      }    
     return true;
    }


    private boolean prepareOutputData()
   {
       try
       {    	     
           this.mInputData = new VData();
           this.mInputData.add(map);
       }
       catch (Exception ex)
       {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "LAContinueConfirmBelongBL";
           tError.functionName = "prepareData";
           tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
           this.mErrors.addOneError(tError);
           return false;
       }
       return true;
   }
    
   
   public VData getResult()
   {
       return this.mResult;
   }

}
