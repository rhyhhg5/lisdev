package com.sinosoft.lis.agentdaily;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class DiskImportGrpYearBonusUI
{
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private DiskImportGrpYearBonusBL mDiskImportGrpYearBonusBL = null;

    public DiskImportGrpYearBonusUI()
    {
        mDiskImportGrpYearBonusBL = new DiskImportGrpYearBonusBL();
    }

    public boolean submitData(VData cInputData, String operate)
    {
    	//System.out.println("UI submited");
    	if(!mDiskImportGrpYearBonusBL.submitData(cInputData, operate))
        {
            mErrors.copyAllErrors(mDiskImportGrpYearBonusBL.mErrors);
            return false;
        }
        //
        if(mDiskImportGrpYearBonusBL.mErrors.needDealError())
        {
            mErrors.copyAllErrors(mDiskImportGrpYearBonusBL.mErrors);
        }

        return true;
    }

    /**
     * 得到成功导入的记录数
     * @return int
     */
    public int getImportPersons()
    {
        return mDiskImportGrpYearBonusBL.getImportPersons();
    }
    public int getUnImportRecords()
    {
        return mDiskImportGrpYearBonusBL.getUnImportRecords();
    }


    public static void main(String[] args)
    {
        DiskImportGrpYearBonusUI zDiskImportGrpYearBonusUI = new DiskImportGrpYearBonusUI();
    }
}
