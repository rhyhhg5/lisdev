/*
 * <p>ClassName: LAAscriptionBLS </p>
 * <p>Description: LAAscriptionBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentdaily;


import java.sql.Connection;

import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.vdb.LAAscriptionDBSet;
import com.sinosoft.lis.vschema.LAAscriptionSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LAAscriptionDB;
import com.sinosoft.lis.schema.LAAscriptionSchema;

public class HDLAAscriptionBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    public HDLAAscriptionBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        System.out.println("Start HDLAAscriptionBLS Submit...");
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            tReturn = saveLAAscription(cInputData);
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            tReturn = deleteLAAscription(cInputData);
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            tReturn = updateLAAscription(cInputData);
        }
        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End HDLAAscriptionBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean saveLAAscription(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "HDLAAscriptionBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAAscriptionDB tLAAscriptionDB= new LAAscriptionDB(conn);
          //  LAAscriptionSet tLAAscriptionSet = new  LAAscriptionSet ();
            LAAscriptionSchema tLAAscriptionSchema = (LAAscriptionSchema) mInputData.
                                                 getObjectByObjectName(
                      "LAAscriptionSchema", 0);


          //  tLAAscriptionSet=(LAAscriptionSet) mInputData.get(1);
             tLAAscriptionDB.setSchema(tLAAscriptionSchema);
            if (!tLAAscriptionDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAscriptionDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
             LAAscriptionDB mLAAscriptionDB = new LAAscriptionDB(conn);
          //   LAAscriptionSet mLAAscriptionSet = new  LAAscriptionSet ();
         //    mLAAscriptionSet=(LAAscriptionSet) mInputData.get(2);
         LAAscriptionSchema mLAAscriptionSchema = (LAAscriptionSchema) mInputData.
                                               getObjectByObjectName(
                    "LAAscriptionSchema", 2);
            if(mLAAscriptionSchema.getAscripNo()!=null && mLAAscriptionSchema.getAscripNo()!="")
            {
                mLAAscriptionDB.setSchema(mLAAscriptionSchema);
                if (!mLAAscriptionDB.update()) {
                    // @@错误处理
                    this.mErrors.copyAllErrors(mLAAscriptionDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAAscriptionBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            conn.commit();
            conn.close();
            //循环更新保单表中的代理人编码
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "HDLAAscriptionBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    /**
     * 保存函数
     */
     private boolean deleteLAAscription(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAscriptionBLS";
            tError.functionName = "deleteLAAscription";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAAscriptionDB tLAAscriptionDB = new LAAscriptionDB(conn);
            LAAscriptionSchema tLAAscriptionSchema = (LAAscriptionSchema) mInputData.
                                                getObjectByObjectName(
                     "LAAscriptionSchema", 0);
            tLAAscriptionDB.setSchema(tLAAscriptionSchema);

            if (!tLAAscriptionDB.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAscriptionDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "HDLAAscriptionBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    /**
     * 保存函数
     */


    private boolean updateLAAscription(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "HDLAAscriptionBLS";
            tError.functionName = "updateLAAscription";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAAscriptionDB tLAAscriptionDB = new LAAscriptionDB(conn);
            LAAscriptionSchema tLAAscriptionSchema = (LAAscriptionSchema) mInputData.
                                               getObjectByObjectName(
                    "LAAscriptionSchema", 0);
           tLAAscriptionDB.setSchema(tLAAscriptionSchema);

            if (!tLAAscriptionDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAscriptionDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "HDLAAscriptionBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

}
