package com.sinosoft.lis.agentdaily;

import java.io.File;

import com.sinosoft.lis.db.LATaxAdjustDB;
import com.sinosoft.lis.db.LAWageHistoryDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LATaxAdjustSchema;
import com.sinosoft.lis.schema.LAWageHistorySchema;
import com.sinosoft.lis.schema.LCGrpImportLogSchema;
import com.sinosoft.lis.vschema.LATaxAdjustSet;
import com.sinosoft.lis.vschema.LAWageHistorySet;
import com.sinosoft.lis.vschema.LCGrpImportLogSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description:</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author zhangxinjing
 * @version 1.1
 */
public class MaImportActiveBL {
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private MMap mmap = new MMap();
    
    private int importPersons = 0; //记录导入成功的记录数
    private int unImportRecords = 0; //记录导入不成功的记录数
    
    private String mBranchType;
    private String mBranchType2;
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String diskimporttype = "";
    private String fileName = "";
    private String configName = "LATaxAdjustMaImport.xml";
    LDComDB mLDComDB=new LDComDB();
	LDComSet mLDComSet=new LDComSet();
	
    private LATaxAdjustSet mLATaxAdjustSetFinal=new LATaxAdjustSet();
    
    private LATaxAdjustSet mLATaxAdjustSet = null;
    private LCGrpImportLogSet mLCGrpImportLogSet=new LCGrpImportLogSet();
    private int merrorNum=0;
    private StringBuffer merrorInfo=new StringBuffer();
    public MaImportActiveBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
    	///System.out.println("BL submited+++++++++++++++++++++++++++++++++++++++++++++++++++++");
    	System.out.println("data checked"+this.mErrors.getErrContent());
        if (!getInputData(cInputData)) {
            return false;
        }
        if(!checkBatchno()){
        	return false;
        }
        //System.out.println("geted inputdata+++++++++++++++++++++++++++++++++++++++++++++++++++++");
        mOperate = cOperate;
        String path = (String) mTransferData.getValueByName("FilePath");
        String fileName = path + File.separator +
                          (String) mTransferData.getValueByName("FileName");
        diskimporttype=(String)mTransferData.getValueByName("diskimporttype");
        String configFileName = path + File.separator + configName;
        System.out.println("begin import");
        //从磁盘导入数据
        LATaxAdjustDiskImporter importer = new LATaxAdjustDiskImporter(fileName,configFileName,diskimporttype);
        importer.setTableName(diskimporttype);
        if (!importer.doImport()) {
            mErrors.copyAllErrors(importer.mErrrors);
            return false;
        }
                      
        //System.out.println("doimported+++++++++++++++++++++++++++++");
        if (diskimporttype.equals("LATaxAdjust")) {
            mLATaxAdjustSet = (LATaxAdjustSet) importer
                                  .getSchemaSet();
        }
        if (!checkData()) {
            return false;
        }
        System.out.println("data checked"+this.mErrors.getErrContent());
        //数据准备操作
        if (mOperate.equals("INSERT")) {
            if (!prepareData()) {
                return false;
            }
            System.out.println("---End prepareData---");
        }
        this.mResult.add(mmap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mResult, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            System.out.println("MaImportActiveBL数据提交失败");
            this.mErrors.addOneError("数据提交失败!");
            return false;
        }
        System.out.println("--- ->submitData--- 数据提交成功。");

        return true;
    }

    private boolean getInputData(VData cInputData) {
        mTransferData = (TransferData) cInputData.
                        getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.
                       getObjectByObjectName("GlobalInput", 0);
        this.fileName=(String)mTransferData.getValueByIndex(0);
        
        if (mTransferData == null || mGlobalInput == null) {
            mErrors.addOneError("传入的的数据不完整: mTransferData == null "
                                + "|| mGlobalInput == null");
            return false;
        }
        diskimporttype = (String) mTransferData.getValueByName("diskimporttype");
       // mManageCom  = (String) mTransferData.getValueByName("ManageCom");
        mBranchType  = (String) mTransferData.getValueByName("BranchType");
        mBranchType2  = (String) mTransferData.getValueByName("BranchType2");
        return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkBatchno(){
    	String tSQL="select 1 from lcgrpimportlog where batchno='"+this.fileName+"' with ur";
    	ExeSQL tExeSQL=new ExeSQL();
    	if(tExeSQL.getOneValue(tSQL).equals("1")){
        	mErrors.addOneError("该文件批次已经被使用过，请使用新的文件批次！");
    		return false;
    	}
    	return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkData() {
        //展业证信息校验
        if (diskimporttype.equals("LATaxAdjust")) {
            if (mLATaxAdjustSet == null||mLATaxAdjustSet.size()==0) {
                mErrors.addOneError("导入的文件不能为空！");
                return false;
            } else {
            	LATaxAdjustDB mTempLATaxAdjustDB=new LATaxAdjustDB();
            	LATaxAdjustSchema mTempLATaxAdjustSchema=new LATaxAdjustSchema();
            	LATaxAdjustSchema mTempLATaxAdjustSchema2;
            	LCGrpImportLogSchema tLCGrpImportLogSchema;
                for (int i = 1; i <= mLATaxAdjustSet.size(); i++) {
                	this.merrorNum=0;
                	this.merrorInfo.delete(0,merrorInfo.length());
                	System.out.println("..............checkdata here merrorInfo"
                			+this.merrorInfo);
                	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                	mTempLATaxAdjustSchema=mLATaxAdjustSet.get(i);
                	String tAgentCode= new ExeSQL().getOneValue("select * from LAAgent where GroupAgentCode='"+mTempLATaxAdjustSchema.getAgentCode()+"' and managecom= '"+mTempLATaxAdjustSchema.getManageCom()+"'  ");
                	if(tAgentCode==null||tAgentCode.equals("")){
                		this.merrorNum++;
                        this.merrorInfo.append("系统中没"+mTempLATaxAdjustSchema.getAgentCode()+"营销员或其工号、管理机构输入有误/");
                	}
                	//薪资状态不能为13,14               	
                	String tState= new ExeSQL().getOneValue(" select state from LAWageHistory where 1=1 and branchtype ='1' " +
                			" and branchtype2 = '01' and ManageCom='"+mTempLATaxAdjustSchema.getManageCom()+"'  and WageNo= '"+mTempLATaxAdjustSchema.getWageNo()+"' ");
        	        if(tState.equals("13")||tState.equals("14")){
        	        	 this.merrorNum++;
                         this.merrorInfo.append("营销员"+mTempLATaxAdjustSchema.getAgentCode()+"所属机构的薪资月已经薪资确认或者薪资审核发放，不能导入/");
        	        }
                    if (mTempLATaxAdjustSchema.getAgentCode() == null ||
                    		mTempLATaxAdjustSchema.getAgentCode().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("营销员代码不能为空/");
                    }
                    if (mTempLATaxAdjustSchema.getManageCom() == null ||
                    		mTempLATaxAdjustSchema.getManageCom().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("营销员"+mTempLATaxAdjustSchema.getAgentCode()+"所属管理机构不能为空/");
                    }
                    if (mTempLATaxAdjustSchema.getWageNo() == null ||
                    		mTempLATaxAdjustSchema.getWageNo().equals(
                                       "")) {
                        this.merrorNum++;
                        this.merrorInfo.append("营销员"+mTempLATaxAdjustSchema.getAgentCode()+"的薪资月不能为空/");
                    }                                     
                                        
                    if(this.merrorNum==0){
                    	for(int j = i+1; j <= mLATaxAdjustSet.size(); j++){
                        	mTempLATaxAdjustSchema2=mLATaxAdjustSet.get(j);
                        	if(mTempLATaxAdjustSchema.getAgentCode().equals(mTempLATaxAdjustSchema2.getAgentCode())
                        	&&mTempLATaxAdjustSchema.getCBPTaxes()==mTempLATaxAdjustSchema2.getCBPTaxes()
                        	&&mTempLATaxAdjustSchema.getPCBPTaxes()==mTempLATaxAdjustSchema2.getPCBPTaxes()
                        	&&mTempLATaxAdjustSchema.getITBPTaxes()==mTempLATaxAdjustSchema2.getITBPTaxes()
                        	&&mTempLATaxAdjustSchema.getWageNo().equals(mTempLATaxAdjustSchema2.getWageNo()))                       
                        	{
                        		this.merrorNum++;
                                this.merrorInfo.append("与第"+j+"行数据重复/");
                        	}
                        }
                    }
                    if(this.merrorNum==0){
                       checkExist(mTempLATaxAdjustSchema,mTempLATaxAdjustDB);
                    }
                    if(this.merrorNum>0){
                    	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                    	tLCGrpImportLogSchema.setErrorInfo("导入错误！具体错误有："
                    			+this.merrorInfo.toString());
                    	tLCGrpImportLogSchema.setErrorState("1");
                    	tLCGrpImportLogSchema.setErrorType("1");
                    	tLCGrpImportLogSchema.setContID(i+"");
                    	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                    	this.unImportRecords++;
                    	mErrors.addOneError(merrorInfo.toString());
                    }else{
                    	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                    	tLCGrpImportLogSchema.setErrorInfo("导入成功！");
                    	tLCGrpImportLogSchema.setErrorState("0");
                    	tLCGrpImportLogSchema.setErrorType("0");
                    	tLCGrpImportLogSchema.setContID(i+"");
                    	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                    }
                }
            }
        }
        return true;
    }
private boolean checkExist(LATaxAdjustSchema mLATaxAdjustSchema,LATaxAdjustDB mLATaxAdjustDB){
	
	String tAgentCode= new ExeSQL().getOneValue("select AgentCode from LAAgent where GroupAgentCode='"+mLATaxAdjustSchema.getAgentCode()+"' ");
	LATaxAdjustSet tempLATaxAdjustSet=new LATaxAdjustSet();
	String tempSql="select * from LATaxAdjust where AgentCode='"+tAgentCode+"' and WageNo='"+mLATaxAdjustSchema.getWageNo()+"' ";
	System.out.println("checkExist:tempsql"+tempSql);
	tempLATaxAdjustSet=mLATaxAdjustDB.executeQuery(tempSql);
	if(tempLATaxAdjustSet.size()>0){
		System.out.println("unImportRecords appending one");
		this.merrorNum++;
        this.merrorInfo.append("在数据库中已经存在/");
	}else{
		LATaxAdjustSchema tempLATaxAdjustSchema=new LATaxAdjustSchema();
		tempLATaxAdjustSchema.setAgentCode(tAgentCode);
		tempLATaxAdjustSchema.setManageCom(mLATaxAdjustSchema.getManageCom());
		tempLATaxAdjustSchema.setCBPTaxes(mLATaxAdjustSchema.getCBPTaxes());
		tempLATaxAdjustSchema.setPCBPTaxes(mLATaxAdjustSchema.getPCBPTaxes());
		tempLATaxAdjustSchema.setITBPTaxes(mLATaxAdjustSchema.getITBPTaxes());
		tempLATaxAdjustSchema.setWageNo(mLATaxAdjustSchema.getWageNo());
		tempLATaxAdjustSchema.setOperator(this.mGlobalInput.Operator);
		tempLATaxAdjustSchema.setBranchType(this.mBranchType);
		tempLATaxAdjustSchema.setBranchType2(this.mBranchType2);
		tempLATaxAdjustSchema.setMakeDate(this.currentDate);
		tempLATaxAdjustSchema.setMakeTime(this.currentTime);
		tempLATaxAdjustSchema.setModifyDate(this.currentDate);
		tempLATaxAdjustSchema.setModifyTime(this.currentTime);
		mLATaxAdjustSetFinal.add(tempLATaxAdjustSchema);
	}
	return true;
}
    /**
     * 检查每个客户是否是本公司的代理人，若不是则剔除
     * @param
     * @return boolean，成功校验 trun
     */
    private boolean prepareData() {
        if (diskimporttype.equals("LATaxAdjust")) {
        	System.out.println("prepareData:doing");
            importPersons=mLATaxAdjustSetFinal.size();
            if(mLATaxAdjustSetFinal!=null&&mLATaxAdjustSetFinal.size()>0){
            	System.out.println("prepareData:importPersons"+importPersons);
            	for (int i = 1; i <= mLATaxAdjustSetFinal.size(); i++) {
              	mLATaxAdjustSetFinal.get(i).setOperator(mGlobalInput.Operator);
              	mmap.put(mLATaxAdjustSetFinal.get(i), "INSERT");
              }
            }
            
            if(this.mLCGrpImportLogSet!=null&&this.mLCGrpImportLogSet.size()>0){
            	//importPersons = mLARateCommisionSetFinal.size();
            	for(int i=1;i<=this.mLCGrpImportLogSet.size();i++){
            		mLCGrpImportLogSet.get(i).setGrpContNo("00000000000000000000");
            		mLCGrpImportLogSet.get(i).setOperator(this.mGlobalInput.Operator);
            		mLCGrpImportLogSet.get(i).setMakeDate(PubFun.getCurrentDate());
            		mLCGrpImportLogSet.get(i).setMakeTime(PubFun.getCurrentTime());
            		mLCGrpImportLogSet.get(i).setBatchNo(this.fileName);
            		mLCGrpImportLogSet.get(i).setContNo("Active");
            		mmap.put(mLCGrpImportLogSet.get(i), "INSERT");
            	}
            }
        }
        return true;
    }

    /**
     *成功导入的记录数
     * @return int
     */
    public int getImportPersons() {
        return importPersons;
    }
    public int getUnImportRecords() {
        return unImportRecords;
    }

    public static void main(String[] args) {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        TransferData t = new TransferData();

        t.setNameAndValue("diskimporttype", "LATaxAdjust");
        t.setNameAndValue("path", "D:\\picch\\ui\\temp");
        t.setNameAndValue("fileName", "应纳税项.xls");
        
        VData v = new VData();
        v.add(g);
        v.add(t);

        MaImportActiveBL d = new MaImportActiveBL();
        d.submitData(v, "INSERT");

        if (d.mErrors.needDealError()) {
            System.out.println(d.mErrors.getErrContent());
        }
    }

}
