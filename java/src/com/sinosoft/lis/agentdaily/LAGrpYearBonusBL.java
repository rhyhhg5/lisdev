package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.vschema.LAAttendSet;
import com.sinosoft.lis.vschema.LARateCommisionBSet;
import com.sinosoft.lis.schema.LAAttendSchema;
import com.sinosoft.lis.schema.LARateCommisionBSchema;
import com.sinosoft.lis.db.LAAttendDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>
 * Title: LAGrpYearBonusBL
 * </p>
 *
 * <p>
 * Description: LIS - 销售管理
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 *
 * <p>
 * Company: SinoSoft
 * </p>
 *
 * @author luomin
 * @version 1.0
 */
public class LAGrpYearBonusBL {
	// 错误处理类
	public CErrors mErrors = new CErrors();

	// 业务处理相关变量
	/** 全局数据 */
	private VData mInputData = new VData();

	private String mOperate = "";

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();

	private MMap map = new MMap();

	public GlobalInput mGlobalInput = new GlobalInput();
 
	private LAAttendSet mLAAttendSet = new LAAttendSet();

	private Reflections ref = new Reflections();

	public LAGrpYearBonusBL() {

	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println("Begin LAGrpYearBonusBL.submitData.........");
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		if (!check()) {
			return false;
		}
		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		PubSubmit tPubSubmit = new PubSubmit();
		System.out.println("Start LAGrpYearBonusBL Submit...");
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "LAGrpYearBonusBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	/**
	 *
	 */
	private boolean check() {
		// 进行重复数据校验
		System.out.println("Begin LAGrpYearBonusBL.check.........1" + mLAAttendSet.size());
		ExeSQL tExeSQL=new ExeSQL();
		String tResult=null;
		String tSQL=null;
		LAAttendSchema tLAAttendSchema ;
		LAAttendSchema tLAAttendSchema2 ;
		for (int i = 1; i <= this.mLAAttendSet.size(); i++) {
                    tLAAttendSchema = mLAAttendSet.get(i);   
                    // 校验业务员
                     tSQL="select groupagentcode from laagent where agentcode='"+tLAAttendSchema.getAgentCode()+"'";
                     tResult=tExeSQL.getOneValue(tSQL);
                     String tGroupAgentCode = tResult;
                     if(tResult==null || "".equals(tResult)){
                     	CError tError = new CError();
                      	tError.moduleName = "LAGrpYearBonusBL";
                      	tError.functionName = "check";
                      	tError.errorMessage = "录入的数据中业务员代码不符条件，请修改!";
                      	this.mErrors.addOneError(tError);
                      	return false;
                      }
                    // 校验团队
                      tSQL="select 1 from labranchgroup where agentgroup='"+tLAAttendSchema.getAgentGroup()
                          +"' and branchtype='2' and branchtype2='01'";
                      tResult=tExeSQL.getOneValue(tSQL);
                      if(tResult==null || !tResult.equals("1")){
                      	CError tError = new CError();
                      	tError.moduleName = "LAGrpYearBonusBL";
                      	tError.functionName = "check";
                      	tError.errorMessage = "录入的数据中销售机构编码不符条件，请修改!";
                      	this.mErrors.addOneError(tError);
                      	return false;
                      }
                     // 校验是否计算过薪资
                      tSQL= "select  '1' from lawagehistory where managecom = '"+tLAAttendSchema.getManagecom()+"' and branchtype = '"+tLAAttendSchema.getBranchType()+"' and branchtype2 = '"+tLAAttendSchema.getBranchType2()+"' and wageno = '"+tLAAttendSchema.getWageNo()+"' and state >='13'";
                      tResult=tExeSQL.getOneValue(tSQL);
                      if(tResult!=null &&tResult.equals("1")){
                    	  CError tError = new CError();
                          tError.moduleName = "LAGrpYearBonusBL";
                          tError.functionName = "check";
                          tError.errorMessage = "业务员"+tGroupAgentCode+"所属机构"+tLAAttendSchema.getManagecom()+","+tLAAttendSchema.getWageNo()+"月份薪资已确认，不能于此月再进行年效益奖录入、修改或删除操作!";
                          this.mErrors.addOneError(tError);
                          return false;
                      }
                      // 校验处理数据是否有重复.同一年份不能对同一业务员多次进行年效益奖录入操作
                      for(int j=i+1;j<=this.mLAAttendSet.size();j++){
                      	tLAAttendSchema2 = mLAAttendSet.get(j);
                      	if(tLAAttendSchema.getAgentCode().equals(tLAAttendSchema2.getAgentCode()))
                      	{
                             CError tError = new CError();
                             tError.moduleName = "LAGrpYearBonusBL";
                             tError.functionName = "check";
                             tError.errorMessage = "不能对业务员"+tGroupAgentCode+"，进行多次年效益奖录入操作，请修改!";
                             this.mErrors.addOneError(tError);
                             return false;
                        }
                       // 分公司只能统一进行年效益奖发放操作(分公司不能跨薪资月进行业务员年效益奖发放)
                      	if(!tLAAttendSchema.getWageNo().equals(tLAAttendSchema2.getWageNo()))
                      	{
                             CError tError = new CError();
                             tError.moduleName = "LAGrpYearBonusBL";
                             tError.functionName = "check";
                             tError.errorMessage = "分公司只能统一进行年效益奖发放操作(分公司不能跨薪资月进行业务员年效益奖发放)，请修改!";
                             this.mErrors.addOneError(tError);
                             return false;
                        }
			           }   
		          }
		System.out.println("mOperate..............."+mOperate);
		if (mOperate.equals("INSERT")) {
			for (int i = 1; i <= this.mLAAttendSet.size(); i++) {
			   tLAAttendSchema = mLAAttendSet.get(i);
			   String tGroupAgentCodeSQL = "select groupagentcode from laagent where agentcode = '"+tLAAttendSchema.getAgentCode()+"' ";
			   String tGroupAgentCode = new ExeSQL().getOneValue(tGroupAgentCodeSQL);
			     // 校验业务员每年只能进行一次年效益奖录入操作
	             String tYear = tLAAttendSchema.getWageNo().substring(0, 4);
	             String tcheck = "select wageno from LAAttend where agentcode = '"+tLAAttendSchema.getAgentCode()+"' and wageno like '"+tYear+"%'  fetch first 1 rows only ";
	             System.out.println("校验SQL如下："+tcheck);
	             tResult=tExeSQL.getOneValue(tcheck);
	             if(tResult!=null && !tResult.equals(""))
	             {
	            	CError tError = new CError();
	               	tError.moduleName = "LAGrpYearBonusBL";
	               	tError.functionName = "check";
	               	tError.errorMessage = "业务员"+tGroupAgentCode+"已于"+tResult+"录入了年效益奖，本年度内不能再次录入！";
	               	this.mErrors.addOneError(tError);
	               	return false;
	             }    
	             tcheck = "select * from laattend where managecom like '"+tLAAttendSchema.getManagecom().substring(0, 4)+"%' and branchtype ='2' and branchtype2 = '01' and wageno like '"+tYear+"%' and wageno <>'"+tLAAttendSchema.getWageNo()+"' fetch first 1 rows only ";
	             System.out.println("新增校验sql"+tcheck);
	             LAAttendDB tLAAttendDB = new LAAttendDB();
	             LAAttendSet tLAAttendSet = new LAAttendSet();
	             tLAAttendSet =tLAAttendDB.executeQuery(tcheck);
	             if(tLAAttendSet.size()>=1)
	             {
	            	 String tGroupAgentCodeSQL1 = "select groupagentcode from laagent where agentcode = '"+tLAAttendSet.get(1).getAgentCode()+"' ";
	  			   	 String tGroupAgentCode1 = new ExeSQL().getOneValue(tGroupAgentCodeSQL1);
	            	 CError tError = new CError();
		             tError.moduleName = "LAGrpYearBonusBL";
		             tError.functionName = "check";
		             tError.errorMessage = "分公司"+tLAAttendSet.get(1).getManagecom().substring(0, 4)+"已存有业务员"+tGroupAgentCode1+"于"+tLAAttendSet.get(1).getWageNo()+"录入了年效益奖。依据团险部规定(分公司只能统一进行年效益奖发放操作！),分公司不能跨薪资月进行年效益奖录入，请进行修改！";
		             this.mErrors.addOneError(tError);
		             return false;
	             }
	             
			}
		}
		if (mOperate.equals("UPDATE")) {
			for (int i = 1; i <= this.mLAAttendSet.size(); i++) {
                  tLAAttendSchema = mLAAttendSet.get(i);
                  // 校验是否修改后与数据库数据相同
                  String sql = "select idx from LAAttend where branchtype='"
                            		+ tLAAttendSchema.getBranchType()
                              		+ "' and branchtype2='"
                              		+ tLAAttendSchema.getBranchType2()
                              		+ "' and agentcode='"
                              		+ tLAAttendSchema.getAgentCode()
                              		+ "' and wageno='"
                              		+ tLAAttendSchema.getWageNo()+ "'";
                              String tidx = tExeSQL.getOneValue(sql);
                              System.out.println(tLAAttendSchema.getIdx()+":::"+tidx);
              if (tidx!=null && Integer.parseInt(tidx)!=tLAAttendSchema.getIdx()) {
            	String tGroupAgentCodeSQL = "select groupagentcode from laagent where agentcode = '"+tLAAttendSchema.getAgentCode()+"' ";
   			    String tGroupAgentCode = new ExeSQL().getOneValue(tGroupAgentCodeSQL);
				CError tError = new CError();
				tError.moduleName = "LAGrpYearBonusBL";
				tError.functionName = "check";
				tError.errorMessage = "数据库已经存在该代理人:"+tGroupAgentCode+"同样年月的年效益奖设置,修改失败!";
				this.mErrors.addOneError(tError);
				return false;
              }
			}
		}
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		// 全局变量
		try {
			System.out.println("Begin LAGrpYearBonusBL.getInputData.........");
			this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
            this.mLAAttendSet.set((LAAttendSet) cInputData.getObjectByObjectName("LAAttendSet", 0));
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAGrpYearBonusBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "在读取处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("getInputData end ");
		return true;
	}

	/**
	 * 业务处理主函数
	 */
	/**
	 * @return
	 */
	private boolean dealData() {
		System.out.println("Begin LAGrpYearBonusBL.dealData........."+ mOperate);
		try {
			ExeSQL tExeSQL=new ExeSQL();
			SSRS tSSRS =new SSRS();
			String tSQL=null;
			if (mOperate.equals("UPDATE")) {
				LAAttendSet tLAAttendSet = new LAAttendSet();
				for (int i = 1; i <= mLAAttendSet.size(); i++) {
					LAAttendSchema tLAAttendSchemaold = new LAAttendSchema();
					LAAttendSchema tLAAttendSchemanew = new LAAttendSchema();
					LAAttendDB tLAAttendDB = new LAAttendDB();
					tLAAttendDB.setIdx(mLAAttendSet.get(i).getIdx());
					tLAAttendSchemaold = tLAAttendDB.query().get(1);
					tLAAttendSchemanew = mLAAttendSet.get(i);
					tSQL="select managecom,agentgroup from labranchgroup where branchtype='2' and branchtype2='01' " +
							"and agentgroup='"+tLAAttendSchemanew.getAgentGroup()+"'";
					tSSRS=tExeSQL.execSQL(tSQL);
					tLAAttendSchemanew.setAgentGroup(tSSRS.GetText(1,2));
					tLAAttendSchemanew.setManagecom(tSSRS.GetText(1,1));
					tLAAttendSchemanew.setOperator(mGlobalInput.Operator);
					tLAAttendSchemanew.setMakeDate(tLAAttendSchemaold.getMakeDate());
					tLAAttendSchemanew.setMakeTime(tLAAttendSchemaold.getMakeTime());
					tLAAttendSchemanew.setModifyDate(CurrentDate);
					tLAAttendSchemanew.setModifyTime(CurrentTime);
					tLAAttendSet.add(tLAAttendSchemanew);
				}
				System.out.println("进行插入操作！");
				map.put(tLAAttendSet, mOperate);
			} else if(mOperate.equals("INSERT")){
				LAAttendSet tLAAttendSet = new LAAttendSet();
				for (int i = 1; i <= mLAAttendSet.size(); i++) {
					LAAttendSchema tLAAttendSchemanew = new LAAttendSchema();
					tLAAttendSchemanew = mLAAttendSet.get(i);
					tSQL="select managecom,agentgroup from labranchgroup where branchtype='2' and branchtype2='01' " +
					" and agentgroup='"+tLAAttendSchemanew.getAgentGroup()+"'";
					tSSRS=tExeSQL.execSQL(tSQL);
					tLAAttendSchemanew.setManagecom(tSSRS.GetText(1,1));
					tLAAttendSchemanew.setAgentGroup(tSSRS.GetText(1,2));
					tSQL="select value((select max(idx) from LAAttend),0) from dual with ur";
					String tTemp=tExeSQL.getOneValue(tSQL);
					tLAAttendSchemanew.setIdx(Integer.parseInt(tTemp)+1);
					tLAAttendSchemanew.setOperator(mGlobalInput.Operator);
					tLAAttendSchemanew.setMakeDate(CurrentDate);
					tLAAttendSchemanew.setMakeTime(CurrentTime);
					tLAAttendSchemanew.setModifyDate(CurrentDate);
					tLAAttendSchemanew.setModifyTime(CurrentTime);
					tLAAttendSet.add(tLAAttendSchemanew);
					
				}
				System.out.println("进行修改操作！");
				map.put(tLAAttendSet, mOperate);
			}else{
				LAAttendSet tLAAttendSet = new LAAttendSet();
				for(int i=1;i<=mLAAttendSet.size();i++)
				{
					LAAttendSchema tLAAttendSchemadel = new LAAttendSchema();
					tLAAttendSchemadel =mLAAttendSet.get(i);
					tLAAttendSet.add(tLAAttendSchemadel);					
				}
				System.out.println("进行删除操作！");
				map.put(tLAAttendSet, mOperate);
			}
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAGrpYearBonusBL";
			tError.functionName = "dealData";
			tError.errorMessage = "在处理所数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
	 */
	private boolean prepareOutputData() {
		try {
			System.out.println("Begin LAGrpYearBonusBL.prepareOutputData.........");
			mInputData.clear();
			mInputData.add(map);
		} catch (Exception ex) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAGrpYearBonusBL";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
}
