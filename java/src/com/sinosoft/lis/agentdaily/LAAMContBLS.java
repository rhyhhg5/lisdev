/*
 * <p>ClassName: AbsenceBLS </p>
 * <p>Description: AbsenceBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-07-08
 */
package com.sinosoft.lis.agentdaily;


import java.sql.Connection;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LARateChargeSet;
import com.sinosoft.lis.vschema.LAAuthorizeSet;

public class LAAMContBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    public LAAMContBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        System.out.println("Start LAAMContBLS Submit...");
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            tReturn = saveLACont(cInputData);
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            tReturn = deleteLACont(cInputData);
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            tReturn = updateLACont(cInputData);
        }
        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End LACoContBLS Submit...");
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean saveLACont(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAMContBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAContDB tLAContDB= new LAContDB(conn);
            LAAuthorizeDBSet tLAAuthorizeDBSet=new LAAuthorizeDBSet(conn);
            LARateChargeDBSet tLARateChargeDBSet=new LARateChargeDBSet(conn);

          tLAContDB.setSchema((LAContSchema) mInputData.
                                    getObjectByObjectName("LAContSchema", 0));
          tLAAuthorizeDBSet.set((LAAuthorizeSet) mInputData.
                                   getObjectByObjectName("LAAuthorizeSet", 0));
          tLARateChargeDBSet.set((LARateChargeSet) mInputData.
                                   getObjectByObjectName("LARateChargeSet", 0));
            System.out.println("before save");
            if (!tLAContDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAContDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAMContBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }


        if (tLAAuthorizeDBSet.size() >0 &&!tLAAuthorizeDBSet.insert())
           {
               // @@错误处理
               this.mErrors.copyAllErrors(tLAAuthorizeDBSet.mErrors);
               CError tError = new CError();
               tError.moduleName = "LAAMContBLS";
               tError.functionName = "saveData";
               tError.errorMessage = "数据保存失败!";
               this.mErrors.addOneError(tError);
               conn.rollback();
               conn.close();
               return false;
           }
           if (tLARateChargeDBSet.size() >0 && !tLARateChargeDBSet.insert())
          {
              // @@错误处理
              this.mErrors.copyAllErrors(tLARateChargeDBSet.mErrors);
              CError tError = new CError();
              tError.moduleName = "LAAMContBLS";
              tError.functionName = "saveData";
              tError.errorMessage = "数据保存失败!";
              this.mErrors.addOneError(tError);
              conn.rollback();
              conn.close();
              return false;
          }
           conn.commit();
           conn.close();
       }

        catch (Exception ex)
        {
            // @@错误处理
            ex.printStackTrace() ;
            CError tError = new CError();
            tError.moduleName = "LAAMContBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean deleteLACont(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAMContBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAContDB tLAContDB = new LAContDB(conn);
            tLAContDB.setSchema((LAContSchema) mInputData.
                                    getObjectByObjectName("LAContSchema", 0));
            if (!tLAContDB.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAContDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAMContBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAMContBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    /**
     * 保存函数
     */
    private boolean updateLACont(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "LAAMContBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAContDB tLAContDB = new LAContDB(conn);
            LAAuthorizeDBSet tLAAuthorizeDBSet=new LAAuthorizeDBSet(conn);
            LARateChargeDBSet tLARateChargeDBSet=new LARateChargeDBSet(conn);
            LAAuthorizeDBSet deleteLAAuthorizeDBSet=new LAAuthorizeDBSet(conn);
            LARateChargeDBSet deleteLARateChargeDBSet=new LARateChargeDBSet(conn);


            tLAContDB.setSchema((LAContSchema) mInputData.
                                    getObjectByObjectName("LAContSchema", 0));
            tLAAuthorizeDBSet.set((LAAuthorizeSet) mInputData.
                                      getObjectByObjectName("LAAuthorizeSet", 0));
            tLARateChargeDBSet.set((LARateChargeSet) mInputData.
                                      getObjectByObjectName("LARateChargeSet", 0));
            deleteLAAuthorizeDBSet.set((LAAuthorizeSet) mInputData.
                                        getObjectByObjectName("LAAuthorizeSet", 3));
            deleteLARateChargeDBSet.set((LARateChargeSet) mInputData.
                                        getObjectByObjectName("LARateChargeSet", 3));




            if (!tLAContDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAContDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAMContBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            if (deleteLAAuthorizeDBSet.size() >0 &&!deleteLAAuthorizeDBSet.delete())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAAuthorizeDBSet.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAMContBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据保存失败!";
            this.mErrors.addOneError(tError);
            conn.rollback();
            conn.close();
            return false;
        }
        if (deleteLARateChargeDBSet.size() >0 && !deleteLARateChargeDBSet.delete())
       {
           // @@错误处理
           this.mErrors.copyAllErrors(tLARateChargeDBSet.mErrors);
           CError tError = new CError();
           tError.moduleName = "LAAMContBLS";
           tError.functionName = "saveData";
           tError.errorMessage = "数据保存失败!";
           this.mErrors.addOneError(tError);
           conn.rollback();
           conn.close();
           return false;
       }

            if (tLAAuthorizeDBSet.size() >0 &&!tLAAuthorizeDBSet.insert())
          {
              // @@错误处理
              this.mErrors.copyAllErrors(tLAAuthorizeDBSet.mErrors);
              CError tError = new CError();
              tError.moduleName = "LAAMContBLS";
              tError.functionName = "saveData";
              tError.errorMessage = "数据保存失败!";
              this.mErrors.addOneError(tError);
              conn.rollback();
              conn.close();
              return false;
          }
          if (tLARateChargeDBSet.size() >0 && !tLARateChargeDBSet.insert())
         {
             // @@错误处理
             this.mErrors.copyAllErrors(tLARateChargeDBSet.mErrors);
             CError tError = new CError();
             tError.moduleName = "LAAMContBLS";
             tError.functionName = "saveData";
             tError.errorMessage = "数据保存失败!";
             this.mErrors.addOneError(tError);
             conn.rollback();
             conn.close();
             return false;
         }

            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAMContBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }

        return tReturn;
    }
}
