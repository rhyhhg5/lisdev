package com.sinosoft.lis.agentdaily;

import java.sql.Connection;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.vdb.*;

public class LAAnnuityBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    //传输数据类
    private VData mInputData;
    /** 数据操作字符串 */
    private String mOperate;

    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("Start LAAnnuityBLS submitData...");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        this.mInputData = (VData) cInputData.clone();
        System.out.println("操作符:"+mOperate);

        if("INSERT||MAIN".equals(mOperate))
        {
            if(!saveAnnuity())
                return false;
        }
        if("UPDATE||MAIN".equals(mOperate))
        {
            System.out.println("修改！");
            if(!updateAnnuity())
                return false;
            System.out.println("修改完毕！");
        }
        if("DELETE||MAIN".equals(mOperate))
        {
            if(!deleteAnnuity())
                return false;
        }

        return true;
    }

    /**
     * 修改
     * @return boolean
     */
    public boolean deleteAnnuity()
    {
        LAAnnuitySchema tLAAnnuitySchema = new LAAnnuitySchema();
        tLAAnnuitySchema = (LAAnnuitySchema)mInputData.getObjectByObjectName(
                           "LAAnnuitySchema",0);

        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAAccountsBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            LAAnnuityDB tLAAnnuityDB = new LAAnnuityDB(conn);
            tLAAnnuityDB.setSchema(tLAAnnuitySchema);
            if (!tLAAnnuityDB.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAnnuityDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLAAnnuityBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAAnnuityBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        } finally {
            try {
                conn.close();
                conn = null;
            } catch (Exception e) {}
        }

        return true;
    }

    /**
     * 修改
     * @return boolean
     */
    public boolean updateAnnuity()
    {
        LAAnnuitySchema tLAAnnuitySchema = new LAAnnuitySchema();
        tLAAnnuitySchema = (LAAnnuitySchema)mInputData.getObjectByObjectName(
                           "LAAnnuitySchema",0);
System.out.println("养老金序号:"+tLAAnnuitySchema.getSeriesNo());
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAAccountsBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            LAAnnuityDB tLAAnnuityDB = new LAAnnuityDB(conn);
            tLAAnnuityDB.setSchema(tLAAnnuitySchema);
            if (!tLAAnnuityDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAnnuityDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAnnuityBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAnnuityBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
            return false;
        } finally {
            try {
                conn.close();
                conn = null;
            } catch (Exception e) {}
        }
        return true;
    }

    /**
     * 保存
     * @return boolean
     */
    public boolean saveAnnuity()
    {
        LAAnnuitySchema tLAAnnuitySchema = new LAAnnuitySchema();
        tLAAnnuitySchema = (LAAnnuitySchema)mInputData.getObjectByObjectName(
                           "LAAnnuitySchema",0);

        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAAccountsBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            LAAnnuityDB tLAAnnuityDB = new LAAnnuityDB(conn);
            tLAAnnuityDB.setSchema(tLAAnnuitySchema);
            if (!tLAAnnuityDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAnnuityDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLAAnnuityBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAAnnuityBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}

            return false;
        } finally {
            try {
                conn.close();
                conn = null;
            } catch (Exception e) {}
        }

        return true;
    }
}
