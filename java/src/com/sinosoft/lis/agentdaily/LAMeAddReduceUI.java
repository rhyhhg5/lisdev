/*
 * <p>ClassName: LAAddSubPerUI </p>
 * <p>Description: LAAddSubPerUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 个险加扣款
 * @CreateDate：2006-07-13
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LARewardPunishSchema;
import com.sinosoft.lis.vschema.LARewardPunishSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAMeAddReduceUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 存放查询结果得容器 */
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    
    private LAMeAddReduceBL mLAMeAddReduceBL = new LAMeAddReduceBL();
    //业务处理相关变量
    public LAMeAddReduceUI()
    {
    }

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        this.mInputData=cInputData;
        //得到外部传入的数据,将数据备份到本类中
        mLAMeAddReduceBL.submitData(mInputData, mOperate);
  
        	 if (mLAMeAddReduceBL.mErrors.needDealError())
             {
                 // @@错误处理
                 this.mErrors.copyAllErrors(mLAMeAddReduceBL.mErrors);
                 CError tError = new CError();
                 tError.moduleName = "LAAddBSubPerUI";
                 tError.functionName = "submitData";
                 tError.errorMessage = "数据提交失败!";
                 this.mErrors.addOneError(tError);
                 return false;
             }


        return true;
    }


    public static void main(String[] args)
    {
     }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
      
        return true;
    }


    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	return true;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
       
        return true;
    }


    public VData getResult()
    {
        return this.mResult;
    }
}
