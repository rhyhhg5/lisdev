package com.sinosoft.lis.agentdaily;


import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.agentdaily.AgentInfoDiskImporter;
import java.io.File;

/**
 * <p>Title: </p>
 *
 * <p>Description:</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author luomin
 * @version 1.1
 */
public class DiskImportLACertificationInfoBL {
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private MMap mmap = new MMap();

    private int importPersons = 0; //记录导入成功的记录数

    private String mBranchType;
    private String mBranchType2;
    private String diskimporttype = "";
    private String configName = "LACertificationInfoDiskImport.xml";

    private LACertificationSet mLACertificationSet = null;

    public DiskImportLACertificationInfoBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
        if (!getInputData(cInputData)) {
            return false;
        }
        mOperate = cOperate;
        String path = (String) mTransferData.getValueByName("FilePath");
        String fileName = path + File.separator +
                          (String) mTransferData.getValueByName("FileName");

        String configFileName = path + File.separator + configName;

        //从磁盘导入数据
        AgentInfoDiskImporter importer = new AgentInfoDiskImporter(fileName,configFileName,diskimporttype);
        importer.setTableName(diskimporttype);
        if (!importer.doImport()) {
            mErrors.copyAllErrors(importer.mErrrors);
            return false;
        }
        if (diskimporttype.equals("LACertification")) {
            mLACertificationSet = (LACertificationSet) importer
                                  .getSchemaSet();

        }
        
        //若被保人在保单中没有记录，则剔出
        if (!checkData()) {
            return false;
        }
        //数据准备操作
        if (mOperate.equals("INSERT")) {
            if (!prepareData()) {
                return false;
            }
            System.out.println("---End prepareData---");
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mResult, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            System.out.println("DiskImportAgentInfoBL数据提交失败");
            this.mErrors.addOneError("数据提交失败!");
            return false;
        }
        System.out.println("--- ->submitData--- 数据提交成功。");

        return true;
    }

    private boolean getInputData(VData cInputData) {
        mTransferData = (TransferData) cInputData.
                        getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.
                       getObjectByObjectName("GlobalInput", 0);

        if (mTransferData == null || mGlobalInput == null) {
            mErrors.addOneError("传入的的数据不完整: mTransferData == null "
                                + "|| mGlobalInput == null");
            return false;
        }

        diskimporttype = (String) mTransferData.getValueByName("diskimporttype");
        mBranchType  = (String) mTransferData.getValueByName("BranchType");
        mBranchType2  = (String) mTransferData.getValueByName("BranchType2");
        return true;
    }

    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkData() {
        //展业证信息校验
        if (diskimporttype.equals("LACertification")) {
            if (mLACertificationSet == null) {
                mErrors.addOneError("导入展业证信息失败！");
                return false;
            } else {
            	StringBuffer errMsg = new StringBuffer();
                for (int i = 1; i <= mLACertificationSet.size(); i++) {
                	
                	System.out.println("----->>"+mLACertificationSet.get(i).getGrantDate());
                    if (mLACertificationSet.get(i).getAgentCode() == null ||
                        mLACertificationSet.get(i).getAgentCode().equals("")) {
                        mErrors.addOneError("导入代理人代码不能为空！");
                        return false;
                    } 
                    else
                    {
                    	LAAgentDB tLAAgentDB = new LAAgentDB();
                        LAAgentSet tLAAgentSet = new LAAgentSet();
                        tLAAgentDB.setGroupAgentCode(mLACertificationSet.get(i).getAgentCode());
                        tLAAgentDB.setBranchType(mBranchType);
                        tLAAgentDB.setBranchType2(mBranchType2);
                        tLAAgentSet = tLAAgentDB.query();
                        if (tLAAgentSet.size() == 0) {
                            errMsg.append("销售人员代码：" +
                                          mLACertificationSet.get(i).getAgentCode() +
                                          ",");
                            mErrors.addOneError(errMsg.toString() + "在系统中不存在；");
    	                }
                    }
                	if (mLACertificationSet.get(i).getCertifNo() == null ||
                           mLACertificationSet.get(i).getCertifNo().equals(
                                   "")) {
	                    mErrors.addOneError("导入展业证书号不能为空！");
	                    return false;
                    }
                	if (mLACertificationSet.get(i).getAuthorUnit() == null ||
                               mLACertificationSet.get(i).getAuthorUnit().
                               equals(
                                       "")) {
                        mErrors.addOneError("导入批准单位不能为空！");
                        return false;
                    }
                	if (mLACertificationSet.get(i).getGrantDate() == null ||
                               mLACertificationSet.get(i).getGrantDate().
                               equals(
                                       "")) {
                        mErrors.addOneError("导入发放日期不能为空！");
                        return false;
                    }
                	if (mLACertificationSet.get(i).getValidStart() == null ||
                               mLACertificationSet.get(i).getValidStart().equals("")) {
                        mErrors.addOneError("导入有效起期不能为空！");
                        return false;
                    }
                	if (mLACertificationSet.get(i).getValidEnd() == null ||
                               mLACertificationSet.get(i).getValidEnd().equals("")) {
                        mErrors.addOneError("导入有效止期不能为空！");
                        return false;
                    }
                    if (mLACertificationSet.get(i).getValidStart().indexOf("-") != -1&&mLACertificationSet.get(i).getValidEnd().indexOf("-") != -1) {
                        String bir[] = mLACertificationSet.get(i).getValidStart().
                                       split("-");
                        String tStartDate = bir[0] + bir[1] + bir[2];
                        int intStartDate = new Integer(tStartDate).intValue();
                        String bir1[] = mLACertificationSet.get(i).getValidEnd().
                                        split("-");
                        String tEndDate = bir1[0] + bir1[1] + bir1[2];
                        int intEndDate = new Integer(tEndDate).intValue();
                        if (intStartDate > intEndDate) {
                            mErrors.addOneError("导入有效起期不能晚于有效止期！");
                            return false;
                        }
                    }
//                    if(mLACertificationSet.get(i).getState() == null ||
//                            mLACertificationSet.get(i).getState().equals(""))
//                    {
//                    	 mErrors.addOneError("导入有效状态不能为空！");
//                         return false;
//                    }
//                    else if(mLACertificationSet.get(i).getState().equals("0"))
//                    {
//                    	mLACertificationSet.get(i).setInvalidDate("");
//                    	mLACertificationSet.get(i).setInvalidRsn("");
//                    }
//                    else if(mLACertificationSet.get(i).getState().equals("1"))
//                    {
//                    	if(mLACertificationSet.get(i).getInvalidDate() == null ||
//                                mLACertificationSet.get(i).getInvalidDate().equals(""))
//                        {
//                        	 mErrors.addOneError("当展业证有效状态 为失效时，失效日期不能为空！");
//                             return false;
//                        }
//                    	if (mLACertificationSet.get(i).getValidStart().indexOf("-") != -1&&mLACertificationSet.get(i).getInvalidDate().indexOf("-") != -1) {
//                            String bir[] = mLACertificationSet.get(i).getValidStart().
//                                           split("-");
//                            String tStartDate = bir[0] + bir[1] + bir[2];
//                            int intStartDate = new Integer(tStartDate).intValue();
//                            String bir1[] = mLACertificationSet.get(i).getInvalidDate().
//                                            split("-");
//                            String tEndDate = bir1[0] + bir1[1] + bir1[2];
//                            int intEndDate = new Integer(tEndDate).intValue();
//                            if (intStartDate > intEndDate) {
//                                mErrors.addOneError("导入失效日期不能晚于有效起期！");
//                                return false;
//                            }
//                        }
//                    	if(mLACertificationSet.get(i).getInvalidRsn() == null ||
//                                mLACertificationSet.get(i).getInvalidRsn().equals(""))
//                        {
//                        	 mErrors.addOneError("当展业证有效状态 为失效时，失效原因不能为空");
//                             return false;
//                        }
//                    }
                  // add new 校验系统批量信息中是否存有业务员已存有有效展业证书号
                    if (mLACertificationSet.get(i).getAgentCode()!= null ||
                            !mLACertificationSet.get(i).getAgentCode().equals("")) {
                    	
                    	LACertificationDB tLACertificationDB = new LACertificationDB();
                    	LACertificationSet tLACertificationSet = new LACertificationSet();
                    	String tSQL = "";
                    	tSQL = "select *  from LACertification where agentcode=db2inst1.getagentcode('"
                    		+mLACertificationSet.get(i).getAgentCode()
                            	+"')  and state='0'";
                    	tLACertificationSet = tLACertificationDB.executeQuery(tSQL);
                    	if (tLACertificationSet.size() != 0) {
                    		mErrors.addOneError("第"+i+"行"+mLACertificationSet.get(i).getAgentCode()
                    				+"导入业务员已经存在有效的展业证号！");
                    		return false;
                    	}
                    }
                    // 校验数据是否重复
                    if(i<mLACertificationSet.size()){
                    	for(int j=i+1;j<=mLACertificationSet.size();j++){
                    		String agentcode1=mLACertificationSet.get(i).getAgentCode();
                    		String agentcode2=mLACertificationSet.get(j).getAgentCode();
                        	if(agentcode1.equals(agentcode2)){
                        		mErrors.addOneError("第"+i+"行代理人编码与第"
                        				+j+"行代理人编码重复！一个代理人只能有一个有效的展业证号。");
                                return false;
                        	}
                        }
                    }
                    // 校验批量导入展业证号系统是否已存在
                    if (mLACertificationSet.get(i).getCertifNo() == null ||
                            mLACertificationSet.get(i).getCertifNo().equals(
                                    "")) {
                    String check_sql = "select certifno from LACertification where CertifNo='"+mLACertificationSet.get(i).getCertifNo()+"' " +
                    		"union select certifno from LACertificationb where CertifNo='"+mLACertificationSet.get(i).getCertifNo()+"' fetch first 1 rows only ";
                    ExeSQL tExeSQL = new ExeSQL();
                    String tcheck = tExeSQL.getOneValue(check_sql);
                    if(tcheck!=null&&!tcheck.equals(""))
                    {
                		mErrors.addOneError("第"+i+"行"+mLACertificationSet.get(i).getAgentCode()
                    				+"业务员导入展业证号"+tcheck+"系统已存在,烦请进行调整！");
                        return false;
                    }
                     return false;
                 }

                }
            }
        }
        return true;
    }

    private boolean prepareData() {
        if (diskimporttype.equals("LACertification")) {
            importPersons = mLACertificationSet.size();
            for (int i = 1; i <= mLACertificationSet.size(); i++) {
            	
            	LACertificationSchema tempLACertification = new LACertificationSchema();
            	tempLACertification = mLACertificationSet.get(i);
            	System.out.println("--------->>"+tempLACertification.getCertifNo()+"m");
            	LAAgentDB tLAAgentDB = new LAAgentDB();
                LAAgentSet tLAAgentSet = new LAAgentSet();
                tLAAgentDB.setGroupAgentCode(tempLACertification.getAgentCode());
                tLAAgentDB.setBranchType(mBranchType);
                tLAAgentDB.setBranchType2(mBranchType2);
                tLAAgentSet = tLAAgentDB.query();
                if (tLAAgentSet.size() == 0) {
                    continue;
                }
            	// 细节处理
                tempLACertification.setAgentCode(tLAAgentSet.get(1).getAgentCode());
            	tempLACertification.setCertifNo(getSwitchData(tempLACertification.getCertifNo()));
            	tempLACertification.setState("0");
            	tempLACertification.setInvalidRsn("");
                tempLACertification.setMakeDate(PubFun.getCurrentDate());
                tempLACertification.setMakeTime(PubFun.getCurrentTime());
                tempLACertification.setModifyDate(PubFun.getCurrentDate());
                tempLACertification.setModifyTime(PubFun.getCurrentTime());
                tempLACertification.setOperator(this.mGlobalInput.Operator);
                tempLACertification.setreissueDate(PubFun.getCurrentDate());
                
                String sql = "select * from LACertification where agentcode='" +tempLACertification.getAgentCode() +"' and CertifNo <> '" +tempLACertification.getCertifNo() + "'";
                System.out.println("打印sql如下："+sql);
                LACertificationSet tLACertificationSet = new LACertificationSet();
                LACertificationDB tLACertificationDB = new LACertificationDB();
                tLACertificationSet = tLACertificationDB.executeQuery(sql);
          	    LACertificationBSet tLACertificationBSet = new LACertificationBSet();
        	  
                if (tLACertificationSet.size() > 0) 
                {
                    String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 19);// 获取变更批次号
                	for(int j = 1;j<=tLACertificationSet.size();j++)
                	{
                	  LACertificationSchema tLACertificationSchema = new LACertificationSchema();
                	  tLACertificationSchema =tLACertificationSet.get(j).getSchema();// 获取原信息
                	  LACertificationBSchema tLACertificationBSchema = new LACertificationBSchema();
               	      Reflections tReflections = new Reflections();
            	      tReflections.transFields(tLACertificationBSchema, tLACertificationSchema);
               	      tLACertificationBSchema.setEdorNo(tEdorNo);
            	      tLACertificationBSchema.setOldMakeDate(tLACertificationSchema.getMakeDate());
            	      tLACertificationBSchema.setOldMakeTime(tLACertificationSchema.getMakeTime());
            	      tLACertificationBSchema.setOldModifyDate(tLACertificationSchema.getModifyDate());
            	      tLACertificationBSchema.setOldModifyTime(tLACertificationSchema.getModifyTime());
            	      tLACertificationBSchema.setOldOperator(tLACertificationSchema.getOperator());
            	      tLACertificationBSchema.setMakeDate(PubFun.getCurrentDate());
            	      tLACertificationBSchema.setMakeTime(PubFun.getCurrentTime());
            	      tLACertificationBSchema.setModifyDate(PubFun.getCurrentDate());
            	      tLACertificationBSchema.setModifyTime(PubFun.getCurrentTime());
            	      tLACertificationBSchema.setOperator(this.mGlobalInput.Operator);
            	      tLACertificationBSet.add(tLACertificationBSchema);
                	}
                }
//                // 插入操作
                mmap.put(tLACertificationSet, "DELETE");
                mmap.put(tLACertificationBSet, "INSERT");
                mmap.put(tempLACertification, "DELETE&INSERT");
                // 新增一条 修改laagent 表 DevNo1 字段
//                LAAgentDB tLAAgentDB = new LAAgentDB();
//                LAAgentSet tLAAgentSet = new LAAgentSet();
//                tLAAgentDB.setAgentCode(tempLACertification.getAgentCode());
//                tLAAgentSet=tLAAgentDB.query();
//                for(int k = 1;k<=tLAAgentSet.size();k++)
//                {
//                	LAAgentSchema tLAAgentSchema = new LAAgentSchema();
//                	tLAAgentSchema=tLAAgentSet.get(k).getSchema();
//                	tLAAgentSchema.setDevNo1(tempLACertification.getCertifNo());
//                	this.mLAAgentSet.add(tLAAgentSchema);
//                }
//                mmap.put(mLAAgentSet, "UPDATE");
                
            }
            this.mResult.add(mmap);

        }
        return true;

    }

    /**
     *成功导入的记录数
     * @return int
     */
    public int getImportPersons() {
        return importPersons;
    }


    public static void main(String[] args) {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        TransferData t = new TransferData();

        t.setNameAndValue("diskimporttype", "LACertification");
        t.setNameAndValue("path", "D:\\picch\\ui\\temp");
        t.setNameAndValue("fileName", "展业证清单.xls");

        VData v = new VData();
        v.add(g);
        v.add(t);

        DiskImportLACertificationInfoBL d = new DiskImportLACertificationInfoBL();
        d.submitData(v, "INSERT");

        if (d.mErrors.needDealError()) {
            System.out.println(d.mErrors.getErrContent());
        }
    }
    // 去除批量录入信息中全角空格
   private String getSwitchData(String temp)
   {
	   if(temp == null)
	   {
		   return null;
	   }
	   String Result = "";
	   String regStartSpace = "^[　 ]*";  
       String regEndSpace = "[　 ]*$";  
         
       // 连续两个 replaceAll   
       Result = temp.replaceAll(regStartSpace, "").replaceAll(regEndSpace, "").trim();
	   return Result;
   }
}
