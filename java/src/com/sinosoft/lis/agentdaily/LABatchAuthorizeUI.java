/*
 * <p>ClassName: LABatchAuthorizeUI </p>
 * <p>Description: ALAAuthorizeUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-15
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LABatchAuthorizeUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "mak001";
        tG.ComCode = "mak001";
        tG.ManageCom = "8611";

        String tOperate = "INSERT||MAIN"; // 操作类型
        String tAgentCode  = "";          // 授权业务员代码
        String tAgentName  = "";          // 授权业务员姓名
        String tManageCom  = "";          // 管理机构
        String tBranchAttr = "86110000030304";          // 授权团队
        String tAgentGrade = "";          // 授权职级
        String tWorkYears  = "";          // 授权工龄
        String tBatchMark  = "False";     // 是否按条件批量授权
        String tBranchType = "1";         // 展业类型
        String tBranchType2 = "01";       // 渠道

        // 被授权人员列表
        LAAgentSet tLAAgentSet = new LAAgentSet();
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        tLAAgentSchema.setAgentCode("1101000111");
        tLAAgentSet.add(tLAAgentSchema);

        // 被授于的权限列表
        LAAuthorizeSet tLAAuthorizeSet = new LAAuthorizeSet();
        LAAuthorizeSchema tLAAuthorizeSchema = new LAAuthorizeSchema();
        tLAAuthorizeSchema.setRiskCode("1101");
        tLAAuthorizeSchema.setAuthorType("0");
        tLAAuthorizeSchema.setAuthorObj("110100019");
 //       tLAAuthorizeSchema.setAuthorStartDate("2005-01-01");
 //       tLAAuthorizeSchema.setAuthorEndDate("2005-12-31");
        tLAAuthorizeSet.add(tLAAuthorizeSchema);

        VData tVData = new VData();
        tVData.add(tAgentCode);
        tVData.add(tAgentName);
        tVData.add(tManageCom);
        tVData.add(tBranchAttr);
        tVData.add(tAgentGrade);
        tVData.add(tWorkYears);
        tVData.add(tBatchMark);
        tVData.add(tBranchType);
        tVData.add(tBranchType2);
        tVData.add(tLAAgentSet);
        tVData.add(tLAAuthorizeSet);
        tVData.add(tG);

        LABatchAuthorizeUI tLABatchAuthorizeUI = new LABatchAuthorizeUI();
        if (tLABatchAuthorizeUI.submitData(tVData, tOperate)) {
            System.out.println("成功了！");
        } else {
            System.out.println("失败了！");
        }
    }

    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        this.mInputData=(VData)cInputData.clone();
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        LABatchAuthorizeBL tLABatchAuthorizeBL = new LABatchAuthorizeBL();
        System.out.println("Start LABatchAuthorizeBL Submit...");
        tLABatchAuthorizeBL.submitData(mInputData, mOperate);
        System.out.println("End LABatchAuthorizeBL Submit...");

        //如果有需要处理的错误，则返回
        if (tLABatchAuthorizeBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLABatchAuthorizeBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LABatchAuthorizeUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 准备往后台的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        return true;
    }

    /**
     * 进行业务处理
     * @return boolean
     */
    private boolean dealData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        return true;
    }
}
