/*
 * <p>ClassName: OLAArchieveBL </p>
 * <p>Description: OLAArchieveBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:05:58
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.db.LAConRateDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAConRateBSchema;
import com.sinosoft.lis.schema.LAConRateSchema;
import com.sinosoft.lis.vschema.LAConRateBSet;
import com.sinosoft.lis.vschema.LAConRateSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;


public class LAECNonStandRateSetBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String currentDate;
    private String currentTime;
    /** 业务处理相关变量 */
    private LAConRateSchema mLAConRateSchema = new LAConRateSchema();
    private LAConRateSet mLAConRateSet = new LAConRateSet();
    private LAConRateSet mupLAConRateSet = new LAConRateSet();
    private LAConRateSet mdelLAConRateSet = new LAConRateSet();
    private LAConRateSet mInsLAConRateSet= new LAConRateSet();
    private LAConRateBSet mInsLAConRateBSet= new LAConRateBSet();
    private MMap mMap = new MMap();
//private LAArchieveSet mLAArchieveSet=new LAArchieveSet();
    public LAECNonStandRateSetBL()
    {
    }

    public static void main(String[] args)
    {
        LAConRateSchema tLAConRateSchema = new LAConRateSchema();
        LAConRateSet tupLAConRateSet = new LAConRateSet();
        tLAConRateSchema.setGrpContNo("0000031701");
        tLAConRateSchema.setGrpPolNo("0000031701");
        tLAConRateSchema.setRiskCode("1605");
        tLAConRateSchema.setRate("0.8");
        tLAConRateSchema.setBranchType("03");
        tupLAConRateSet.add(tLAConRateSchema);
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86";
        VData tVData = new VData();
        tVData.addElement(tG);

        tVData.addElement(tupLAConRateSet);
        LAECNonStandRateSetBL tLAECNonStandRateSetBL = new LAECNonStandRateSetBL();
        tLAECNonStandRateSetBL.submitData(tVData, "INSERT||MAIN");

    }
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
//       System.out.println("transact"+transact);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAECNonStandRateSetBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAECNonStandRateSetBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start LARateStandPremBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("Start LARateStandPremBL Submit...");
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LARateStandPremBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        currentDate = PubFun.getCurrentDate();
        currentTime = PubFun.getCurrentTime();
        System.out.print(mOperate+"|||||||||||||||||12122");
        if (this.mOperate.equals("INSERT||MAIN") )
        {

            if (!saveData())
            {
                return false;
            }
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            if (!upDateData())
            {
                return false;
            }
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            if(!deleteData())
            {
                return false;
            }
        }

        return true;
    }
    private boolean saveData()
    {
        for (int i = 1; i <= mLAConRateSet.size(); i++)
        {
            LAConRateSchema tLAConRateSchema = new LAConRateSchema();
            tLAConRateSchema = mLAConRateSet.get(i);
            String tGrpContNo=tLAConRateSchema.getGrpContNo();
            String tRiskCode=tLAConRateSchema.getRiskCode();
            String tGrpPolNo=tLAConRateSchema.getGrpPolNo();
            LAConRateDB tLAConRateDB=new LAConRateDB();
            tLAConRateDB.setChargeType(tLAConRateSchema.getChargeType());
            tLAConRateDB.setGrpPolNo(tGrpPolNo);
            if(tLAConRateDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "LAECNonStandRateSetBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "保单号:"+tGrpContNo+ "险种号:"+tRiskCode+" 的信息已存在，如要修改它的信息，请先查询再修改!";
                this.mErrors.addOneError(tError);
                return false;
            }
            //转化BranchType,BranchType2,传如的参数为 salechnl ,salechnldetail
            String[] tBranchTypes = AgentPubFun.switchSalChnl(tLAConRateSchema.getBranchType(),
                    tLAConRateSchema.getBranchType2(),""  );
            if(tBranchTypes[0]==null || tBranchTypes[0].equals("") || tBranchTypes[1]==null || tBranchTypes[1].equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "LAECNonStandRateSetBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "渠道类型转化错误!";
                this.mErrors.addOneError(tError);
                return false;

            }
            tLAConRateSchema.setBranchType(tBranchTypes[0]);
            tLAConRateSchema.setBranchType2(tBranchTypes[1]);
          //  System.out.println("transact||||||||||||||||"+tBranchTypes[0]+tBranchTypes[1]);
            tLAConRateSchema.setMakeDate(currentDate);
            tLAConRateSchema.setMakeTime(currentTime);
            tLAConRateSchema.setModifyDate(currentDate);
            tLAConRateSchema.setModifyTime(currentTime);
            tLAConRateSchema.setOperator(mGlobalInput.Operator);
            mInsLAConRateSet.add(tLAConRateSchema) ;
         }
         mMap.put(this.mInsLAConRateSet, "INSERT");
         return true;
    }
    private boolean upDateData()
    {
        String tEdorNo = PubFun1.CreateMaxNo("ContEdorNo", 20);
        for (int i = 1; i <= mLAConRateSet.size(); i++)
        {
            LAConRateSchema tLAConRateSchema = new LAConRateSchema();
            tLAConRateSchema = mLAConRateSet.get(i);
            String tGrpContNo=tLAConRateSchema.getGrpContNo();
            String tRiskCode=tLAConRateSchema.getRiskCode();
            String tGrpPolNo=tLAConRateSchema.getGrpPolNo();
            LAConRateDB tLAConRateDB=new LAConRateDB();
            tLAConRateDB.setChargeType(tLAConRateSchema.getChargeType());
            tLAConRateDB.setGrpPolNo(tGrpPolNo);
            if(!tLAConRateDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "LAECNonStandRateSetBL";
                tError.functionName = "dealdata";
                tError.errorMessage = "保单号:"+tGrpContNo+ "险种号:"+tRiskCode+" 的信息不存在,不能修改!";
                this.mErrors.addOneError(tError);
                return false;
            }
             /*交验是否已经计算过手续费  此处现在还没有
              *
              *
            */
            tLAConRateSchema=tLAConRateDB.getSchema();
            tLAConRateSchema.setRate(mLAConRateSet.get(i).getRate());
            tLAConRateSchema.setMakeDate(tLAConRateDB.getMakeDate());
            tLAConRateSchema.setMakeTime(tLAConRateDB.getMakeTime());
            tLAConRateSchema.setModifyDate(currentDate);
            tLAConRateSchema.setModifyTime(currentTime);
            tLAConRateSchema.setOperator(mGlobalInput.Operator);
            mupLAConRateSet.add(tLAConRateSchema) ;
            //备份
            LAConRateBSchema tLAConRateBSchema = new LAConRateBSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLAConRateBSchema, tLAConRateDB.getSchema());
            tLAConRateBSchema.setContEdorNo(tEdorNo);
            tLAConRateBSchema.setContEdorNoType("01");
            mInsLAConRateBSet.add(tLAConRateBSchema);
         }
         mMap.put(this.mupLAConRateSet, "UPDATE");
         mMap.put(this.mInsLAConRateBSet, "INSERT");
         return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        String tEdorNo = PubFun1.CreateMaxNo("ContEdorNo", 20);
        //判断 该保单是否已计算佣金
        for(int i=1;i<=mLAConRateSet.size();i++)
        {
            LAConRateSchema tLAConRateSchema = new LAConRateSchema();
            tLAConRateSchema = mLAConRateSet.get(i);
            String tGrpContNo=tLAConRateSchema.getGrpContNo();
            String tRiskCode=tLAConRateSchema.getRiskCode();
            String tGrpPolNo=tLAConRateSchema.getGrpPolNo();
            LAConRateDB tLAConRateDB=new LAConRateDB();
            tLAConRateDB.setChargeType(tLAConRateSchema.getChargeType());
            tLAConRateDB.setGrpPolNo(tGrpPolNo);
           if(!tLAConRateDB.getInfo())
            {
                CError tError = new CError();
                tError.moduleName = "LAECNonStandRateSetBL";
                tError.functionName = "dealdata";
                tError.errorMessage ="保单号:"+tGrpContNo+ "险种号:"+tRiskCode+" 的信息不存在,不能修改!";
                this.mErrors.addOneError(tError);
                return false;
            }
            /*交验是否已经计算过手续费  此处现在还没有
             *
             *
            */
            mdelLAConRateSet.add(tLAConRateSchema) ;
            //备份
            LAConRateBSchema tLAConRateBSchema = new LAConRateBSchema();
            Reflections tReflections = new Reflections();
            tReflections.transFields(tLAConRateBSchema, tLAConRateDB.getSchema());
            tLAConRateBSchema.setContEdorNo(tEdorNo);
            tLAConRateBSchema.setContEdorNoType("02");
            mInsLAConRateBSet.add(tLAConRateBSchema);
        }
        // 备份数据
        mMap.put(this.mInsLAConRateBSet, "INSERT");
        mMap.put(this.mdelLAConRateSet, "DELETE");
        return true;
    }

    public VData getResult()
    {
        this.mResult.clear();
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAConRateSet.set((LAConRateSet) cInputData.
                                getObjectByObjectName("LAConRateSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        LAConRateDB tLAConRateDB = new LAConRateDB();
        int i;
        for (i = 0; i < mLAConRateSet.size(); i++)
        {

            tLAConRateDB.setSchema(this.mLAConRateSet.get(i));
            //如果有需要处理的错误，则返回
            if (tLAConRateDB.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAConRateDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAConRateDB";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mInputData = null;
        }
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAECNonStandRateSetBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    }

