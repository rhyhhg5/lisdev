package com.sinosoft.lis.agentdaily;

import java.sql.Connection;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: lis</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: sinosoft</p>
 * @author lh
 * @version 1.0
 */

public class AdjustCommDataBLS
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    private String mManageCom = "";
    private String mAgentCode = "";
    private String mOriginBranchAttr = "";
    private String mNewBranchAttr = "";
    private String mOriginBranchCode = "";
    private String mNewBranchCode = "";
    private String mStartDate = "";
    private String mEndDate = "";
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */

    public AdjustCommDataBLS()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("submitData");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!updateData())
        {
            return false;
        }

        mInputData = null;
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        this.mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        this.mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        this.mAgentCode = (String) mTransferData.getValueByName("AgentCode");
        this.mManageCom = (String) mTransferData.getValueByName("ManageCom");
        this.mOriginBranchAttr = (String) mTransferData.getValueByName(
                "OriginBranchAttr");
        this.mNewBranchAttr = (String) mTransferData.getValueByName(
                "NewBranchAttr");
        this.mOriginBranchCode = (String) mTransferData.getValueByName(
                "OriginBranchCode");
        this.mNewBranchCode = (String) mTransferData.getValueByName(
                "NewBranchCode");
        this.mStartDate = (String) mTransferData.getValueByName("StartDate");
        this.mEndDate = (String) mTransferData.getValueByName("EndDate");
        if (this.mGlobalInput == null || this.mAgentCode == null ||
            this.mOriginBranchAttr == null)
        {
            CError tError = new CError();
            tError.moduleName = "ALAssessBL";
            tError.functionName = "getInputData()";
            tError.errorMessage = "前台传入数据未初始化";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     *业务逻辑处理
     */
    private boolean dealData()
    {

        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
//            this.mInputData=new VData();
//            this.mInputData.add(this.mGlobalInput);
//            this.mInputData.add(this.mUpdateLAAssessSet);
//            this.mInputData.add(this.mLATreeSet);
//            this.mInputData.add(this.mLADimissionSet);
//            this.mInputData.add(this.mUpdateLAAgentSQL);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustCommDataBLS";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData()
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustCommDataBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 业务数据更新...");
            //只更新LACommision表中的BranchAttr
            String tFlag = "";
            if (this.mOriginBranchCode.equals(mNewBranchCode))
            {
                tFlag = "0";
            }
            else
            {
                tFlag = "1";
            }
            System.out.println("111");
            UpdateTableOfAgentGroup tUpdate = new UpdateTableOfAgentGroup();
            tUpdate.setAgentCode(mAgentCode);
            tUpdate.setOldAgentGroup(mOriginBranchCode);
            tUpdate.setNewAgentGroup(mNewBranchCode);
            tUpdate.setAdjustDate(mStartDate);
            tUpdate.setEndDate(mEndDate);
            tUpdate.setFlag(tFlag);
            tUpdate.setNewBranchAttr(mNewBranchAttr);
            if (!tUpdate.submitData(conn))
            {
                this.mErrors.copyAllErrors(tUpdate.mErrors);
                CError tError = new CError();
                tError.moduleName = "AdjustCommDataBLS";
                tError.functionName = "updateData";
                tError.errorMessage = "更新业务数据出错！";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustCommDataBLS";
            tError.functionName = "updateData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }

    public static void main(String[] args)
    {
        AdjustCommDataBLS tdd = new AdjustCommDataBLS();
        VData tVData = new VData();
        GlobalInput tg = new GlobalInput();
        tg.Operator = "bb";
        tVData.add(tg);
        tVData.add("86130000");
        tVData.add("200312");
        tVData.add("3");
        tdd.submitData(tVData, "");
    }
}
