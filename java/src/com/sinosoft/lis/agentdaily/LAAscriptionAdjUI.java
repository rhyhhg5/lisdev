package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAAscriptionAdjUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        LAAscriptionAdjBL tLAAscriptionAdjBL = new LAAscriptionAdjBL();
        if(!tLAAscriptionAdjBL.submitData(cInputData,cOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAAscriptionAdjBL.mErrors);
            return false;
        }
        return true;
    }
}
