package com.sinosoft.lis.agentdaily;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.pubfun.*;

public class LAAnnuityBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    private String mNowDate;
    private String mNowTime;
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 业务处理对象 */
    LAAnnuitySchema mLAAnnuitySchema = new LAAnnuitySchema();
    LAAgentSchema mLAAgentSchema = new LAAgentSchema();

    /**
     *传输数据的公共方法
     */
    public boolean submitData(VData cInputData,String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData,cOperate)) {
            return false;
        }

        //进行数据验证
        if(!check())
        {
            System.out.println("对目标业务员的验证没有通过!");
            return false;
        }

        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAnnuityBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAAnnuityBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        // 准备传往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("Start LAAnnuityBL Submit...");
        System.out.println("进入LAAnnuityBLS类进行后台处理！");
        LAAnnuityBLS tLAAnnuityBLS = new LAAnnuityBLS();
        tLAAnnuityBLS.submitData(mInputData,mOperate);
        //如果有需要处理的错误，则返回
        if (tLAAnnuityBLS.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAAnnuityBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAnnuityBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("End LAAnnuityBL Submit...");

        mInputData = null;
        return true;
    }

    /**
     * 准备向后台的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            this.mInputData.add(this.mLAAnnuitySchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAnnuityBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 进行数据验证
     * @return boolean
     */
    private boolean check()
    {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        LAAgentSet tLAAgentSet = new LAAgentDBSet();
        LAAnnuityDB ttLAAnnuityDB = new LAAnnuityDB();
        LAAnnuitySet ttLAAnnuitySet = new LAAnnuitySet();

        //检查用户信息
        tLAAgentDB.setAgentCode(mLAAnnuitySchema.getAgentCode());
        tLAAgentSet = tLAAgentDB.query();
        if (tLAAgentDB.mErrors.needDealError())
        {
            CError.buildErr(this, "queryAgent", tLAAgentDB.mErrors);
            System.out.println("查询["+mLAAnnuitySchema.getAgentCode()+"]用户失败");

            return false;
        }
        if (tLAAgentSet == null || tLAAgentSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAnnuityBL";
            tError.functionName = "submitData";
            tError.errorMessage = "查询["+mLAAnnuitySchema.getAgentCode()+"]用户失败";
            this.mErrors.addOneError(tError);

            return false;
        }
        //缓存业务员信息
        mLAAgentSchema.setSchema(tLAAgentSet.get(1));

        if("INSERT||MAIN".equals(mOperate))
        {
            String tOutWorkDate = tLAAgentSet.get(1).getOutWorkDate();
            System.out.println(mLAAnnuitySchema.getAgentCode() + "的离职时间是：" +
                               tOutWorkDate);
            //业务员离职验证
            if (!"".equals(tOutWorkDate) &&
                !mLAAnnuitySchema.getOutWorkDate().equals(tOutWorkDate)) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAAnnuityBL";
                tError.functionName = "submitData";
                tError.errorMessage = "[" + mLAAnnuitySchema.getAgentCode() +
                                      "]的离职时间不正确！";
                this.mErrors.addOneError(tError);

                return false;
            }

            //同一人不能有两条养老金记录
            ttLAAnnuityDB = new LAAnnuityDB();
            ttLAAnnuitySet = new LAAnnuitySet();
            ttLAAnnuityDB.setAgentCode(mLAAnnuitySchema.getAgentCode());
            ttLAAnnuitySet = ttLAAnnuityDB.query();
            if(ttLAAnnuitySet.size() > 0)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAAnnuityBL";
                tError.functionName = "submitData";
                tError.errorMessage = "[" + mLAAnnuitySchema.getAgentCode() +
                                      "]的养老金信息已经存在！";
                System.out.println("[" + mLAAnnuitySchema.getAgentCode() +
                                      "]的养老金信息已经存在！");
                this.mErrors.addOneError(tError);

                return false;
            }
        }

        //修改/删除时
        if(!"INSERT||MAIN".equals(mOperate))
        {
            //验证有无养老金领取记录
            LAAnnuityDB tLAAnnuityDB = new LAAnnuityDB();
            tLAAnnuityDB.setAgentCode(mLAAnnuitySchema.getAgentCode());
            LAAnnuitySet tLAAnnuitySet = new LAAnnuitySet();
            tLAAnnuitySet = tLAAnnuityDB.query();
            if (tLAAnnuityDB.mErrors.needDealError()) {
                CError.buildErr(this, "queryAnnuity", tLAAnnuityDB.mErrors);
                System.out.println("查询[" + mLAAnnuitySchema.getAgentCode() +
                                   "]用户失败");

                return false;
            }
            if (tLAAnnuitySet == null || tLAAnnuitySet.size() == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAAnnuityBL";
                tError.functionName = "submitData";
                tError.errorMessage = "用户[" + mLAAnnuitySchema.getAgentCode() +
                                      "]没有养老金记录！";
                this.mErrors.addOneError(tError);

                return false;
            }
            // *** Dumy
            mLAAnnuitySchema.setScaleAnnuity(
                             tLAAnnuitySet.get(1).getScaleAnnuity());
        }

        return true;
    }

    /**
     * 进行数据处理
     * @return boolean
     */
    private boolean dealData()
    {
        mNowDate = PubFun.getCurrentDate();
        mNowTime = PubFun.getCurrentTime();

        if("INSERT||MAIN".equals(mOperate))
        {
            if(!insertDeal())
                return false;
        }
        if("UPDATE||MAIN".equals(mOperate))
        {
            if(!updateDeal())
                return false;
        }
        if("DELETE||MAIN".equals(mOperate))
        {
            if(!deleteDeal())
                return false;
        }

        return true;
    }

    /**
     * 删除处理
     * @return boolean
     */
    private boolean deleteDeal()
    {
        // ↓ Dumy 得到养老金ID号码
        if("".equals(mLAAnnuitySchema.getSeriesNo()) ||
           null == mLAAnnuitySchema.getSeriesNo())
        {
            LAAnnuityDB tLAAnnuityDB = new LAAnnuityDB();
            LAAnnuitySet tLAAnnuitySet = new LAAnnuityDBSet();
            tLAAnnuitySet = tLAAnnuityDB.query();
            mLAAnnuitySchema.setSeriesNo(tLAAnnuitySet.get(1).getSeriesNo());
        }
        // ↑ Dumy

        return true;
    }

    /**
     * 修改处理
     * @return boolean
     */
    private boolean updateDeal()
    {
        // ↓ Dumy 得到养老金ID号码
//        if("".equals(mLAAnnuitySchema.getSeriesNo()) ||
//           null == mLAAnnuitySchema.getSeriesNo())
//        {
//            LAAnnuityDB tLAAnnuityDB = new LAAnnuityDB();
//            LAAnnuitySet tLAAnnuitySet = new LAAnnuityDBSet();
//            tLAAnnuitySet = tLAAnnuityDB.query();
//            mLAAnnuitySchema.setSeriesNo(tLAAnnuitySet.get(1).getSeriesNo());
//        }
        // ↑ Dumy
        LAAnnuityDB tLAAnnuityDB = new LAAnnuityDB();
        LAAnnuitySet tLAAnnuitySet = new LAAnnuityDBSet();
        tLAAnnuityDB.setAgentCode(mLAAnnuitySchema.getAgentCode());
        tLAAnnuitySet = tLAAnnuityDB.query();
        System.out.println("人：" + tLAAnnuitySet.get(1).getAgentCode() + " 领取状态：" +
                           tLAAnnuitySet.get(1).getDrawMarker());
        //如果已经领取  不允许修改
        if("1".equals(tLAAnnuitySet.get(1).getDrawMarker()))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAnnuityBL";
            tError.functionName = "updateDeal";
            tError.errorMessage = "养老金已经领取,不允许修改!";
            System.out.println("养老金已经领取,不允许修改!");
            this.mErrors.addOneError(tError);
            return false;
        }

        mLAAnnuitySchema.setAgentGroup(mLAAgentSchema.getAgentGroup());
        mLAAnnuitySchema.setManageCom(mLAAgentSchema.getManageCom());
        mLAAnnuitySchema.setOperator(mGlobalInput.Operator);
        mLAAnnuitySchema.setModifyDate(mNowDate);
        mLAAnnuitySchema.setModifyTime(mNowTime);

        return true;
    }

    /**
     * 新建处理
     * @return boolean
     */
    private boolean insertDeal()
    {
        String newAnnuityNo = "";
        System.out.println("dealData:" + mOperate);
        System.out.println("AgentCode:" + mLAAnnuitySchema.getAgentCode());

        //获得养老金流水号
        newAnnuityNo = PubFun1.CreateMaxNo("ANNUITYTNO",20);
        mLAAnnuitySchema.setAgentGroup(mLAAgentSchema.getAgentGroup());
        mLAAnnuitySchema.setManageCom(mLAAgentSchema.getManageCom());
        mLAAnnuitySchema.setSeriesNo(newAnnuityNo);
        mLAAnnuitySchema.setOperator(mGlobalInput.Operator);
        mLAAnnuitySchema.setModifyDate(mNowDate);
        mLAAnnuitySchema.setModifyTime(mNowTime);
        mLAAnnuitySchema.setMakeDate(mNowDate);
        mLAAnnuitySchema.setMakeTime(mNowTime);

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData,String cOperate)
    {
        mLAAnnuitySchema.setSchema((LAAnnuitySchema)
                                     cInputData.getObjectByObjectName(
                                     "LAAnnuitySchema",0));
        this.mGlobalInput.setSchema((GlobalInput)
                                     cInputData.getObjectByObjectName(
                                     "GlobalInput",0));

        return true;
    }
}
