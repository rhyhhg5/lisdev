package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.vschema.LATrainerIndexSet;
import com.sinosoft.lis.vschema.LATrainerWageHistorySet;
import com.sinosoft.lis.schema.LATrainerIndexSchema;
import com.sinosoft.lis.db.LATrainerIndexDB;
import com.sinosoft.lis.db.LATrainerWageHistoryDB;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: LATrainerIndexADMoneyUI</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2009</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author WangQingMin 2018-6-7
 * @version 1.0
 */
public class LATrainerIndexADMoneyBL {
  //错误处理类
  public  CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  //UPDATE标识
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LATrainerIndexSet mLATrainerIndexSet = new LATrainerIndexSet();
  private LATrainerIndexSchema mLaTrainerIndexSchema=new LATrainerIndexSchema();
  private String mWageNo = "";
  private String mDonedate = "";
  private String donedate = "";
  private String mManageCom = "";
  public LATrainerIndexADMoneyBL() {

  }

  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LATrainerIndexADMoneyBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    for(int i=1;i<=mLATrainerIndexSet.size();i++)
    {
        mLaTrainerIndexSchema = new LATrainerIndexSchema();
        System.out.println(mLATrainerIndexSet.get(i).getTrainerCode());
        mLaTrainerIndexSchema=mLATrainerIndexSet.get(i);
        if(!check())
        {
        	return false;
        }
    }
        
  
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LATrainerIndexADMoneyBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("保存成功");
    mInputData = null;
    return true;
  }
/**
 * 
 */
  private boolean check()
  {
	//已经计算过佣金的日期不能操作
      mDonedate=mWageNo.substring(0,4)+"-"+mWageNo.substring(4,6)+"-01";
      donedate = AgentPubFun.formatDate(mDonedate, "yyyy-MM-dd");
      //校验调整月为最大薪资月次月
  	String tManageCom = mLaTrainerIndexSchema.getPManageCom();
  	String twageno1 = mLaTrainerIndexSchema.getWageNo();
  	String check_wage = "select int(max(wageno)) from laTrainerWageHistory where state='13' and managecom = '"+tManageCom+"' and branchtype = '1' " +
				"and branchtype2 = '01' ";
			ExeSQL tExeSQL = new ExeSQL();
			String tmonth=twageno1.substring(4, 6);
			if(Integer.parseInt(tmonth)>12){
				 CError tError = new CError();
			     tError.moduleName = "LATrainerIndexADMoneyBL";
			     tError.functionName = "check";
			     tError.errorMessage = "录入薪资月为"+tmonth+"不对！";
			     this.mErrors.addOneError(tError);
			     return false;
			}
			String tMaxWage =tExeSQL.getOneValue(check_wage); 
			if(!(tMaxWage.equals("")||tMaxWage==null)){
			int i =Integer.parseInt(twageno1)-Integer.parseInt(tMaxWage);
			System.out.println(tmonth+"月--薪资月差"+i);
			if(i!=1&&i!=89)
			{
				 CError tError = new CError();
			     tError.moduleName = "LATrainerIndexADMoneyBL";
			     tError.functionName = "check";
			     tError.errorMessage = "支公司薪资月为"+tMaxWage+"业务规则加扣款可操作执行年月必须为最大计算薪资月次月！";
			     this.mErrors.addOneError(tError);
			     return false;
			}
			}
			//校验
			System.out.println(mOperate+"----加扣款验证");
	        if(mOperate.equals("INSERT"))
	        {
	      	  String check_count = "select (select codename from ldcode where codetype = 'bankmoney' and code = latrainerIndex.ADState)" +
	      	  		"from latrainerIndex where wageCode='GX0003' and  trainerCode = '"+mLaTrainerIndexSchema.getTrainerCode()+"'" +
	      	  		" and wageno = '"+mLaTrainerIndexSchema.getWageNo()+"' and ADState = '"+mLaTrainerIndexSchema.getADState()+"'";
	      	  
	      	  System.out.println("同一个薪资月不能重复录入相同类型加扣款检验SQL如下:"+check_count);
	          ExeSQL check_ExeSQL = new ExeSQL();
	          String error_Mess = check_ExeSQL.getOneValue(check_count);
	      	  if(error_Mess!=null&&error_Mess!="")
	    	   {
	    		 CError tError = new CError();
	             tError.moduleName = "LATrainerIndexADMoneyBL";
	             tError.functionName = "check";
	             tError.errorMessage = "同一个执行年月,每个业务员只能录入一条加扣款类型为"+error_Mess+"的数据";
	             this.mErrors.addOneError(tError);
	             return false;
	    	   }     	
	        }

      String sql = "select yearmonth from lastatsegment where stattype='1' and startdate<='" +
                   donedate+"'  and enddate>='"+donedate+"'  ";
      ExeSQL aExeSQL = new ExeSQL();
      String  tWageNO=aExeSQL.getOneValue(sql);
      String  tsql = "select * from laTrainerWageHistory  where state='13' and  wageno='"+tWageNO+"' and managecom='"+
                     mManageCom+"' and BranchType='1' and BranchType2='01' fetch first 5 rows only";
      LATrainerWageHistoryDB tLATrainerWageHistoryDB = new LATrainerWageHistoryDB();
      LATrainerWageHistorySet tLATrainerWageHistorySet = new LATrainerWageHistorySet();
      tLATrainerWageHistorySet=tLATrainerWageHistoryDB.executeQuery(tsql);
      if(tLATrainerWageHistorySet.size()>0)
      {
          CError tError = new CError();
          tError.moduleName = "LAAddBSubPerBL";
          tError.functionName = "check";
          tError.errorMessage = "机构:"+mManageCom+"月份:"+tWageNO+"已经确认过佣金,不能进行下一步操作!";
          this.mErrors.addOneError(tError);
          return false;
      }
	  return true;
  }
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LATrainerIndexADMoneyBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     this.mLATrainerIndexSet.set( (LATrainerIndexSet) cInputData.getObjectByObjectName("LATrainerIndexSet",0));
      System.out.println("LATrainerIndexSet get"+mLATrainerIndexSet.size());
      this.mWageNo = (String) cInputData.get(1);
      this.mManageCom = (String) cInputData.get(2);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LATrainerIndexADMoneyBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LATrainerIndexADMoneyBL.dealData........."+mOperate);
    try {
    	LATrainerIndexSchema  tLATrainerIndexSchemaNew = new  LATrainerIndexSchema();
    	LATrainerIndexSchema  tLATrainerIndexSchemaOld = new  LATrainerIndexSchema();
    	LATrainerIndexDB tLATrainerIndexDB =new  LATrainerIndexDB();
    	
        if(mOperate.equals("INSERT")) {
	        System.out.println("Begin LATrainerIndexADMoneyBL.dealData.........INSERT"+mLATrainerIndexSet.size());
	        LATrainerIndexSet tLATrainerIndexSet = new LATrainerIndexSet();
	        for (int i = 1; i <= mLATrainerIndexSet.size(); i++) {
	        	
	        	tLATrainerIndexSchemaNew = mLATrainerIndexSet.get(i);	
		          ExeSQL tExe = new ExeSQL();
		          String tSql=
		              "select max(int(idx)) from LATrainerIndex order by 1 desc";
		          String strIdx = "";
		          int tMaxIdx = 0;
		          strIdx = tExe.getOneValue(tSql);
		          if (strIdx == null || strIdx.trim().equals("")) {
		            tMaxIdx = 0;
		          }
		          else {
		            tMaxIdx = Integer.parseInt(strIdx);
		          }
		      tMaxIdx += i;
		      String sMaxIdx =tMaxIdx+"";
		      tLATrainerIndexSchemaNew.setIdx(sMaxIdx);
		      tLATrainerIndexSchemaNew.setMakeDate(CurrentDate);
		      tLATrainerIndexSchemaNew.setMakeTime(CurrentTime);
		      tLATrainerIndexSchemaNew.setModifyDate(CurrentDate);
		      tLATrainerIndexSchemaNew.setModifyTime(CurrentTime);
		      tLATrainerIndexSet.add(tLATrainerIndexSchemaNew);
		      
      }
	        System.out.println("进来了，是保存："+tLATrainerIndexSchemaNew.toString());
        	map.put(tLATrainerIndexSet, mOperate);
      }
      if(mOperate.equals("UPDATE")) {
    	  LATrainerIndexSet tLATrainerIndexSet = new LATrainerIndexSet();
    	 for(int i = 1;i<=mLATrainerIndexSet.size();i++){
    		 tLATrainerIndexDB.setIdx(mLATrainerIndexSet.get(i).getIdx());
    		 tLATrainerIndexSchemaOld = tLATrainerIndexDB.query().get(1); 
    		 String  tWageNO= tLATrainerIndexSchemaOld.getWageNo();
	            String  tsql = "select 1 from laTrainerWageHistory  where state='13' and wageno='"+tWageNO+"' and managecom='"+
	                           mManageCom+"' and BranchType='1' and BranchType2='01' fetch first 5 rows only";
	            
		          ExeSQL check_ExeSQL = new ExeSQL();
		          String error_Mess = check_ExeSQL.getOneValue(tsql);
		          if(error_Mess!=null&&error_Mess!="")
		    	   {
		                CError tError = new CError();
		                tError.moduleName = "LAAddBSubPerBL";
		                tError.functionName = "check";
		                tError.errorMessage = "此加扣款信息已经就算过佣金，不能修改 !";
		                this.mErrors.addOneError(tError);
		                return false;
		            }
	            
    		 tLATrainerIndexSchemaNew = mLATrainerIndexSet.get(i);
    		 tLATrainerIndexSchemaNew.setMakeDate(tLATrainerIndexSchemaOld.getMakeDate());
    		 tLATrainerIndexSchemaNew.setMakeTime(tLATrainerIndexSchemaOld.getMakeTime());
    		 tLATrainerIndexSchemaNew.setModifyDate(CurrentDate);
    		 tLATrainerIndexSchemaNew.setModifyTime(CurrentTime);
    		 tLATrainerIndexSet.add(tLATrainerIndexSchemaNew);
    		 }
    	 map.put(tLATrainerIndexSet, mOperate);
      }
      if(mOperate.equals("DELETE")){
    	  LATrainerIndexSet tLATrainerIndexSet = new LATrainerIndexSet();
    	 for(int i = 1;i<=mLATrainerIndexSet.size();i++){	
    		 tLATrainerIndexDB.setIdx(mLATrainerIndexSet.get(i).getIdx());
    		 tLATrainerIndexSchemaOld = tLATrainerIndexDB.query().get(1); 
    		 String  tWageNO= tLATrainerIndexSchemaOld.getWageNo();
	            String  tsql = "select 1 from laTrainerWagehistory  where state='13' and wageno='"+tWageNO+"' and managecom='"+
	                           mManageCom+"' and BranchType='1' and BranchType2='01' fetch first 5 rows only";
	            
		          ExeSQL check_ExeSQL = new ExeSQL();
		          String error_Mess = check_ExeSQL.getOneValue(tsql);
		          if(error_Mess!=null&&error_Mess!="")
		    	   {
		                CError tError = new CError();
		                tError.moduleName = "LAAddBSubPerBL";
		                tError.functionName = "check";
		                tError.errorMessage = "此加扣款信息已经就算过佣金，不能删除!";
		                this.mErrors.addOneError(tError);
		                return false;
		            }
    		 tLATrainerIndexSchemaNew = mLATrainerIndexSet.get(i);
    		 tLATrainerIndexSchemaNew.setMakeDate(tLATrainerIndexSchemaOld.getMakeDate());
    		 tLATrainerIndexSchemaNew.setMakeTime(tLATrainerIndexSchemaOld.getMakeTime());
    		 tLATrainerIndexSchemaNew.setModifyDate(CurrentDate);
    		 tLATrainerIndexSchemaNew.setModifyTime(CurrentTime);
    		 tLATrainerIndexSet.add(tLATrainerIndexSchemaNew);
		      
    	 }
    	 map.put(tLATrainerIndexSet, mOperate);
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LATrainerIndexADMoneyBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      mInputData.clear();
	  mInputData.add(map);      
      }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LATrainerIndexADMoneyBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
 
}
