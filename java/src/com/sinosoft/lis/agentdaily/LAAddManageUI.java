package com.sinosoft.lis.agentdaily;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: </p>
 * @author Alse
 * @version 1.0
 */
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class LAAddManageUI
{

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();


    public LAAddManageUI()
    {
    }

    public boolean submitData(VData cInputData)
    {
        mInputData = (VData) cInputData.clone();
        LAAddManageBL tLAAddManageBL = new LAAddManageBL();
        if (!tLAAddManageBL.submitData(mInputData))
        {
            this.mErrors.copyAllErrors(tLAAddManageBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "tLAAddManageUI";
            tError.functionName = "submitData";
            tError.errorMessage = tLAAddManageBL.mErrors.getFirstError();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
