package com.sinosoft.lis.agentdaily;


import java.io.File;

import com.sinosoft.lis.db.LAAttendDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LAAttendSchema;
import com.sinosoft.lis.schema.LCGrpImportLogSchema;
import com.sinosoft.lis.vschema.LAAttendSet;
import com.sinosoft.lis.vschema.LCGrpImportLogSet;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.agentdaily.GrpYearBonusDiskImporter;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.schema.LAAgentSchema;

/**
 * <p>Title: </p>
 *
 * <p>Description:</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author 苗祥征
 * @version 1.1
 */
public class DiskImportGrpYearBonusBL {
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private MMap mmap = new MMap();

    private int importPersons = 0; //记录导入成功的记录数
    private int unImportRecords = 0; //记录导入不成功的记录数

    private String diskimporttype = "";
    private String fileName = "";
    private String configName = "GrpYearBonusDiskImport.xml";
    LDComDB mLDComDB=new LDComDB();
	LDComSet mLDComSet=new LDComSet();

    private LAAttendSet mLAAttendSetFinal=new LAAttendSet();

    private LAAttendSet mLAAttendSet = null;
    private LCGrpImportLogSet mLCGrpImportLogSet=new LCGrpImportLogSet();
    private int merrorNum=0;
    private StringBuffer merrorInfo=new StringBuffer();
    public DiskImportGrpYearBonusBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {

        if (!getInputData(cInputData)) {
            return false;
        }
        if(!checkBatchno()){
        	return false;
        }

        mOperate = cOperate;
        String path = (String) mTransferData.getValueByName("path");
        String fileName = path + File.separator +(String) mTransferData.getValueByName("fileName");
        diskimporttype=(String)mTransferData.getValueByName("diskimporttype");
        String configFileName = path + File.separator + configName;

        //从磁盘导入数据
        GrpYearBonusDiskImporter importer = new GrpYearBonusDiskImporter(fileName,configFileName,diskimporttype);
        importer.setTableName(diskimporttype);
        if (!importer.doImport()) {
            mErrors.copyAllErrors(importer.mErrrors);
            return false;
        }
        if (diskimporttype.equals("LAAttend")) {
            mLAAttendSet = (LAAttendSet) importer.getSchemaSet();
        }
        if (!checkData()) {
            return false;
        }
        System.out.println("data checked");
        //数据准备操作
        if (mOperate.equals("INSERT")) {
            if (!prepareData()) {
                return false;
            }
            System.out.println("---End prepareData---");
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mResult, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            System.out.println("DiskImportGrpYearBonusBL数据提交失败");
            this.mErrors.addOneError("数据提交失败!");
            return false;
        }
        System.out.println("--- ->submitData--- 数据提交成功。");

        return true;
    }

    private boolean getInputData(VData cInputData) {
        mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        this.fileName=(String)mTransferData.getValueByIndex(4);
        System.out.println("mTransferData.getValueByIndex(4)："+mTransferData.getValueByIndex(4));

        if (mTransferData == null || mGlobalInput == null) {
            mErrors.addOneError("传入的的数据不完整: mTransferData == null "+ "|| mGlobalInput == null");
            return false;
        }
        diskimporttype = (String) mTransferData.getValueByName("diskimporttype");
        System.out.println("mTransferData.getValueByName(branchtype)："+mTransferData.getValueByName("branchtype"));
        return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkBatchno(){
    	String tSQL="select 1 from lcgrpimportlog where batchno='"+this.fileName+"' with ur";
    	ExeSQL tExeSQL=new ExeSQL();
    	if(tExeSQL.getOneValue(tSQL).equals("1")){
        	mErrors.addOneError("该文件批次已经被使用过，请使用新的文件批次！");
    		return false;
    	}
    	return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkData() {
        //展业证信息校验
        if (diskimporttype.equals("LAAttend")) {
            if (mLAAttendSet == null) {
                mErrors.addOneError("导入信息失败！没有得到要导入的数据。");
                return false;
            } else {
            	LAAttendDB mTempLAAttendDB=new LAAttendDB();
            	LAAttendSchema mTempLAAttendSchema=new LAAttendSchema();
            	LAAttendSchema mTempLAAttendSchema2;
            	LCGrpImportLogSchema tLCGrpImportLogSchema;

                for (int i = 1; i <= mLAAttendSet.size(); i++) {
                	//this.merrorNum=0;
                	this.merrorInfo.delete(0,merrorInfo.length());
                	System.out.println("checkdata here merrorInfo"+this.merrorInfo);
                	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                	mTempLAAttendSchema=mLAAttendSet.get(i);
                    if (mTempLAAttendSchema.getAgentCode() == null ||mTempLAAttendSchema.getAgentCode().equals("")) 
                    {
                        this.merrorNum++;
                        this.merrorInfo.append("业务员代码不能为空/");
                    }
                    if (mTempLAAttendSchema.getSummoney() < 0 )
                    {
                        this.merrorNum++;
                        this.merrorInfo.append("效益奖不能为空/");
                    }
                    if (mTempLAAttendSchema.getWageNo() == null ||mTempLAAttendSchema.getWageNo().equals("")) 
                    {
                        this.merrorNum++;
                        this.merrorInfo.append("薪资月不能为空/");
                    }
                    if(this.merrorNum==0)
                    {
                    	// 校验是否数据重复
                    	for(int j = i+1; j <= mLAAttendSet.size(); j++)
                    	{
                        	mTempLAAttendSchema2=mLAAttendSet.get(j);
                        	if(mTempLAAttendSchema.getAgentCode().equals(mTempLAAttendSchema2.getAgentCode()))
                        	{
                        		this.merrorNum++;
                                this.merrorInfo.append("与第"+j+"行人员编码重复/");
                        	}
                        	
                        	if(!mTempLAAttendSchema.getWageNo().equals(mTempLAAttendSchema2.getWageNo()))
                        	{
                        		this.merrorNum++;
                                this.merrorInfo.append("与第"+j+"行薪资月数据不相同/");
                                
                        	}
                        }
                    }
                    if(this.merrorNum==0){
                       checkExist(mTempLAAttendSchema,mTempLAAttendDB,i);
                    }
                    if(this.merrorNum>0){
                    	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                    	tLCGrpImportLogSchema.setErrorInfo("导入错误！具体错误有："
                    			+this.merrorInfo.toString());
                    	tLCGrpImportLogSchema.setErrorState("1");
                    	tLCGrpImportLogSchema.setErrorType("1");
                    	tLCGrpImportLogSchema.setContID(i+"");
                    	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                    	this.unImportRecords++;
                    }else{
                    	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                    	tLCGrpImportLogSchema.setErrorInfo("导入成功！");
                    	tLCGrpImportLogSchema.setErrorState("0");
                    	tLCGrpImportLogSchema.setErrorType("0");
                    	tLCGrpImportLogSchema.setContID(i+"");
                    	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                    }
                }
            }
        }
        return true;
    }
private boolean checkExist(LAAttendSchema mLAAttendSchema,LAAttendDB mLAAttendDB,int i){


	LAAttendSet tempLAAttendSet=new LAAttendSet();
	String tempSql="select * from laattend  where agentcode='"
		+mLAAttendSchema.getAgentCode()+"' and wageno like '"
		+mLAAttendSchema.getWageNo().substring(0, 4)+"%' ";
	System.out.println("checkExist:tempsql"+tempSql);
	tempLAAttendSet=mLAAttendDB.executeQuery(tempSql);
	if(tempLAAttendSet.size()>0)
	{
		System.out.println("unImportRecords appending one");
		this.merrorNum++;
        this.merrorInfo.append("在数据库中已经存在业务员"+mLAAttendSchema.getAgentCode()+","+mLAAttendSchema.getWageNo().substring(0, 4)+"年度年效益奖信息，无法再次录入/");
	}else{
		LAAttendSchema tempLAAttendSchema=new LAAttendSchema();

		LAAgentDB tLAAgentDB = new LAAgentDB();
        LAAgentSchema  mLAAgentSchema=new LAAgentSchema();
        tLAAgentDB.setAgentCode(mLAAttendSchema.getAgentCode());
        tLAAgentDB.getInfo();
        mLAAgentSchema = tLAAgentDB.getSchema();
        //  增加薪资校验 
        String checksql = "select '1' from lawagehistory where branchtype = '"+mLAAgentSchema.getBranchType()+"' and branchtype2 = '"+mLAAgentSchema.getBranchType2()+"' and managecom = '"+mLAAgentSchema.getManageCom()+"' and state >='13' and wageno = '"+mLAAttendSchema.getWageNo()+"'";
        System.out.println("校验薪资算sql 如下："+checksql);
        ExeSQL tExeSQL = new ExeSQL();
        String tResult = tExeSQL.getOneValue(checksql);
        if(tResult!=null&&"1".equals(tResult))
        {
        	this.unImportRecords++;
        	//  mErrors.addOneError("第"+i+"条数据导入失败原因：机构"+mLAAgentSchema.getManageCom()+","+mLAAttendSchema.getWageNo()+"薪资已计算，无法录入年效益奖");
        }else{
        tempLAAttendSchema.setAgentCode(mLAAttendSchema.getAgentCode());
		tempLAAttendSchema.setManagecom(mLAAgentSchema.getManageCom());// 控制从agent表中得出相应managecom信息
		tempLAAttendSchema.setAgentGroup(mLAAgentSchema.getAgentGroup());//控制从agent表中得出相应agentgroup信息 
		tempLAAttendSchema.setTimes("0");
		tempLAAttendSchema.setSummoney(mLAAttendSchema.getSummoney());
		tempLAAttendSchema.setBranchType(mLAAgentSchema.getBranchType());
		tempLAAttendSchema.setWageNo(mLAAttendSchema.getWageNo());
		tempLAAttendSchema.setBranchType2(mLAAgentSchema.getBranchType2());
		mLAAttendSetFinal.add(tempLAAttendSchema);
        }
	}
	return true;
}
    /**
     * 检查每个客户是否是本公司的代理人，若不是则剔除
     * @param
     * @return boolean，成功校验 trun
     */
    private boolean prepareData() {
    	String idxTemp = (String) mTransferData.getValueByName("idx");
    	int idx=Integer.parseInt(idxTemp)+1;

      if (diskimporttype.equals("LAAttend")) {
            importPersons=mLAAttendSetFinal.size();
            if(mLAAttendSetFinal==null)
            {
            	importPersons=0;
            }
            if(mLAAttendSetFinal!=null&&mLAAttendSetFinal.size()>0){
            	for (int i = 1; i <= mLAAttendSetFinal.size(); i++) {
              	System.out.println("preparedata : managecom"+mLAAttendSetFinal.get(i).getManagecom());
              	mLAAttendSetFinal.get(i).setOperator(mGlobalInput.Operator);
              	mLAAttendSetFinal.get(i).setIdx(idx++);
              	mLAAttendSetFinal.get(i).setMakeDate(PubFun.getCurrentDate());
              	mLAAttendSetFinal.get(i).setMakeTime(PubFun.getCurrentTime());
              	mLAAttendSetFinal.get(i).setModifyDate(PubFun.getCurrentDate());
              	mLAAttendSetFinal.get(i).setModifyTime(PubFun.getCurrentTime());
              	mmap.put(mLAAttendSetFinal.get(i), "INSERT");
              }
            }

            if(this.mLCGrpImportLogSet!=null&&this.mLCGrpImportLogSet.size()>0){
            	for(int i=1;i<=this.mLCGrpImportLogSet.size();i++){
            		mLCGrpImportLogSet.get(i).setGrpContNo("00000000000000000000");
            		mLCGrpImportLogSet.get(i).setOperator(this.mGlobalInput.Operator);
            		mLCGrpImportLogSet.get(i).setMakeDate(PubFun.getCurrentDate());
            		mLCGrpImportLogSet.get(i).setMakeTime(PubFun.getCurrentTime());
            		mLCGrpImportLogSet.get(i).setBatchNo(this.fileName);
            		mmap.put(mLCGrpImportLogSet.get(i), "INSERT");
            	}
            }
            this.mResult.add(mmap);
            System.out.println("prepareData:did");
        }
        return true;
    }

    /**
     *成功导入的记录数
     * @return int
     */
    public int getImportPersons() {
        return importPersons;
    }
    public int getUnImportRecords() {
        return unImportRecords;
    }

    public static void main(String[] args) {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        TransferData t = new TransferData();

        t.setNameAndValue("diskimporttype", "LACertification");
        t.setNameAndValue("path", "D:\\picch\\ui\\temp");
        t.setNameAndValue("fileName", "展业证清单.xls");

        VData v = new VData();
        v.add(g);
        v.add(t);

        DiskImportGrpYearBonusBL d = new DiskImportGrpYearBonusBL();
        d.submitData(v, "INSERT");

        if (d.mErrors.needDealError()) {
            System.out.println(d.mErrors.getErrContent());
        }
    }

}
