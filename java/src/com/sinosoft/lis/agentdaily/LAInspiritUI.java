package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LAInspiritSchema;
import com.sinosoft.utility.*;

/**
 * <p>Title: 人员考核指标计算流程类</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class LAInspiritUI {
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    public static void main(String[] agrs)
    {
        String cOperate = "INSERT||MAIN";
        LAInspiritSchema tLAInspiritSchema = new LAInspiritSchema();

        // 基础信息
        tLAInspiritSchema.setStartDate("2006-01-01");
        tLAInspiritSchema.setEndDate("2006-01-10");
        tLAInspiritSchema.setStatDate("2006-01-15");
        tLAInspiritSchema.setOrganizer("总公司本部");
        tLAInspiritSchema.setPloyName("春节联欢晚会");
        tLAInspiritSchema.setAgentCode("1101000002");
        tLAInspiritSchema.setAgentName("郑杰");
        tLAInspiritSchema.setInspiritName("最佳造型和最佳舞台效果奖");
        tLAInspiritSchema.setInspiritContent("奖杯一个,奖状一张,FANS若干。");

        VData cInputData = new VData();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "mak001";
        tGlobalInput.ManageCom = "86110000";
        tGlobalInput.ComCode = "mak001";

        cInputData.add(tGlobalInput);
        cInputData.add(tLAInspiritSchema);

        LAInspiritUI tLAInspiritUI = new LAInspiritUI();
        boolean actualReturn = tLAInspiritUI.submitData(cInputData, cOperate);
        if(actualReturn)
        {
            System.out.println("成功了！");
        }else
        {
            System.out.println("失败了！");
        }
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        mInputData = (VData) cInputData.clone();

        LAInspiritBL tLAInspiritBL = new LAInspiritBL();
        tLAInspiritBL.submitData(mInputData, mOperate);
        //如果有需要处理的错误，则返回
        if (tLAInspiritBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAInspiritBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAInspiritUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
