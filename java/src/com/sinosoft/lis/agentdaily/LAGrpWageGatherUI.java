package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.DBConnPool;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Connection;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;

public class LAGrpWageGatherUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private VData mInputData = new VData();
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAWageSchema mLAWageTerm = new LAWageSchema();

    private String mWageYM = "";               //薪资年月
    //获得转储号码
    private String mNewEdorNo = "";

    private LAWageSet mLAWageSet = null;
    private LAWageBSet mLAWageBSet = null;
    private LAWageHistorySet mLAWageHistorySet = null;
    private LAIndexInfoSet mLAIndexInfoSet = null;
    private LAIndexInfoBSet mLAIndexInfoBSet = null;
    private LARollBackTraceSet mLARollBackTraceSet = new LARollBackTraceSet();
    private MMap mMap=new MMap();
    private VData mOutputDate=new VData();

    public static void main(String args[])
    {
        LAWageSchema tLAWageSchema = new LAWageSchema();
        tLAWageSchema.setManageCom("8611");
        tLAWageSchema.setBranchType("1");
        tLAWageSchema.setBranchType2("01");
        tLAWageSchema.setIndexCalNo("200505");

        GlobalInput tG = new GlobalInput();
        tG.ComCode = "86940000";
        tG.Operator = "001";
        tG.ManageCom = "86940000";

        VData tVData = new VData();
        tVData.addElement(tLAWageSchema);
        tVData.add(tG);

        LAWageGatherUI tLAWageGatherUI = new LAWageGatherUI();
        boolean tB = tLAWageGatherUI.submitData(tVData,"");
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!this.getInputData(cInputData))
            return false;

        //验证薪资是否已发放，如果已经发放则不允许回退
        if(!this.checkWageType())
            return false;

        //进行业务处理
        if (!dealData())
            return false;

        //进行后台处理
        

        PubSubmit tPubSubmit=new PubSubmit();
        tPubSubmit.submitData(mOutputDate,"") ;
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError())
        {
            buildError("submitData","向数据库提交保存时出错！");
            return false;
        }

        return true;
    }

    /**
     * 业务处理
     * @return boolean
     */
    private boolean dealData()
    {
        //获得转储号码
        mNewEdorNo = PubFun1.CreateMaxNo("EDORNO", 20);

        //处理代理人薪资表和薪资备份表
        if(this.dealWage() == false)
            return false;

        //处理指标信息表
        //if(this.dealIndexInfo() == false)
           // return false;

        //处理薪资历史表
        if(this.dealWageHistory() == false)
            return false;

        //处理回退记录信息
        dealRollBackTrace();

        //准备后台处理的数据
        if(!prepareOutputData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageGatherUI";
            tError.functionName = "dealData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错！";
            System.out.println("在准备往后层处理所需要的数据时出错！");
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 处理薪资历史表
     * @return boolean
     */
    private boolean dealWageHistory()
    {
        String tSQL = "";
        LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();
        LAWageHistorySet tLAWageHistorySet = new LAWageHistorySet();

        tSQL  = "SELECT * ";
        tSQL += "  FROM LAWageHistory";
        tSQL += " WHERE ManageCom LIKE '"+mLAWageTerm.getManageCom()+"%'";
        tSQL += "   AND AClass = '03' AND WageNo = '"+this.mWageYM+"'";
        tSQL += "   AND BranchType = '"+mLAWageTerm.getBranchType()+"'";
        tSQL += "   AND BranchType2 = '"+mLAWageTerm.getBranchType2()+"'";


        tLAWageHistorySet = tLAWageHistoryDB.executeQuery(tSQL);
        if(tLAWageHistoryDB.mErrors.needDealError())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageGatherUI";
            tError.functionName = "dealWageHistory";
            tError.errorMessage = "检索不到薪资历史纪录！";
            System.out.println("检索不到薪资历史纪录！");
            this.mErrors.addOneError(tError);

            return false;
        }
        for(int i=1;i<= tLAWageHistorySet.size();i++)
        {
            tLAWageHistorySet.get(i).setState("11");
        }

        mLAWageHistorySet = tLAWageHistorySet;

        return true;
    }

    /**
     * 准备后台处理的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        try
        {
        	 mMap.put(this.mLAWageSet, "DELETE");
        	 mMap.put(this.mLAWageBSet, "INSERT");
        	 mMap.put(this.mLAWageHistorySet, "UPDATE");
        	 mMap.put(this.mLAIndexInfoSet, "DELETE");
        	 mMap.put(this.mLAIndexInfoBSet, "INSERT");
        	 mMap.put(this.mLARollBackTraceSet, "INSERT");
        	 this.mOutputDate.add(mMap);
        }
        catch (Exception ex)
        {
            return false;
        }

        return true;
    }

    /**
     * 进行回退记录处理
     */
    private void dealRollBackTrace()
    {
        //条件划分符号
        String tSign = "&";
        //回退序号
        String tIdx = PubFun1.CreateMaxNo("IDX", 20);
        //回退条件
        String tTrem = "薪资回退年月:" + mWageYM;
        tTrem  += tSign + "管理机构:" + mLAWageTerm.getManageCom();
        tTrem  += tSign + "展业机构:" + mLAWageTerm.getBranchType();
        tTrem  += tSign + "展业渠道:" + mLAWageTerm.getBranchType2();

        LARollBackTraceSchema tLARollBackTraceSchema = new LARollBackTraceSchema();
        tLARollBackTraceSchema.setIdx(tIdx);
        tLARollBackTraceSchema.setEdorNo(mNewEdorNo);
        tLARollBackTraceSchema.setoperator(mGlobalInput.Operator);
        tLARollBackTraceSchema.setstate("0");
        tLARollBackTraceSchema.setConditions(tTrem);
        tLARollBackTraceSchema.setMakeDate(PubFun.getCurrentDate());
        tLARollBackTraceSchema.setMakeTime(PubFun.getCurrentTime());
        tLARollBackTraceSchema.setRollBackType("09");

        //缓存回退记录
        mLARollBackTraceSet.add(tLARollBackTraceSchema);
    }

    /**
     * 处理指标信息表和备份信息
     * @return boolean
     */
    private boolean dealIndexInfo()
    {
        LAIndexInfoDB tLAIndexInfoDB = new LAIndexInfoDB();
        //设置查询条件
        String tSql = "SELECT *";
        tSql       += "  FROM LAIndexInfo";
        tSql       += " WHERE";
        tSql       += "    IndexCalNo = '" + mWageYM + "' AND";
        tSql       += "    IndexType in ('01','00') AND";
        tSql       += "    ManageCom LIKE '" + mLAWageTerm.getManageCom() + "%' AND";
        tSql       += "    BranchType = '" + mLAWageTerm.getBranchType() + "' AND";
        tSql       += "    BranchType2 = '" + mLAWageTerm.getBranchType2() + "'";
        System.out.println("指标信息查询：" + tSql);

        //查询取到结果
        LAIndexInfoSet tLAIndexInfoSet = tLAIndexInfoDB.executeQuery(tSql);

        if(tLAIndexInfoSet == null || tLAIndexInfoSet.size() < 1)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageGatherUI";
            tError.functionName = "dealIndexInfo";
            tError.errorMessage = "检索指标信息失败！";
            System.out.println("检索指标信息失败！");
            this.mErrors.addOneError(tError);

            return false;
        }

        //缓存指标信息
        mLAIndexInfoSet = tLAIndexInfoSet;
        System.out.println("找到 " + mLAIndexInfoSet.size() + " 条指标信息数据！" );

        //取得并处理需要备份的数据
        if(setLAIndexInfoBSet() == false)
            return false;

        return true;
    }

    /**
     * 处理指标信息备份数据
     * @return boolean
     */
    private boolean setLAIndexInfoBSet()
    {
        if(mLAIndexInfoSet == null || mLAIndexInfoSet.size() < 1)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageGatherUI";
            tError.functionName = "setLAIndexInfoBSet";
            tError.errorMessage = "没有指标信息数据！";
            System.out.println("没有指标信息数据！");
            this.mErrors.addOneError(tError);

            return false;
        }

        //得到数据数
        int iMax = mLAIndexInfoSet.size();
        LAIndexInfoBSet tLAIndexInfoBSet = new LAIndexInfoBSet();
        //获得转储号码
        //String tNewEdorNo = PubFun1.CreateMaxNo("EDORNO",20);
        String tNewEdorNo = mNewEdorNo;

        //准备备份数据
        for(int i=1;i<=iMax;i++)
        {
            LAIndexInfoSchema tLAIndexInfoSchema = new LAIndexInfoSchema();
            tLAIndexInfoSchema = mLAIndexInfoSet.get(i).getSchema();
            if(tLAIndexInfoSchema!=null)
            {
                LAIndexInfoBSchema tLAIndexInfoBSchema =
                        setLAIndexInfoBSchema(tLAIndexInfoSchema,tNewEdorNo);
                if(tLAIndexInfoBSchema!=null)
                    tLAIndexInfoBSet.add(tLAIndexInfoBSchema);
            }
        }

        //缓存备份数据
        mLAIndexInfoBSet = tLAIndexInfoBSet;

        return true;
    }

    /**
     * 设置备份指标信息
     * @param pmLAIndexInfoSchema LAIndexInfoSchema
     * @param pmEdorNo String
     * @return LAIndexInfoBSchema
     */
    private LAIndexInfoBSchema setLAIndexInfoBSchema(LAIndexInfoSchema pmLAIndexInfoSchema,
                                                     String pmEdorNo)
    {
        LAIndexInfoBSchema tLAIndexInfoBSchema = new LAIndexInfoBSchema();
        Reflections tReflections = new Reflections();

        //设置备份表中和薪资表中相同的项
        tReflections.transFields(tLAIndexInfoBSchema,pmLAIndexInfoSchema);
        //设置转储号码
        tLAIndexInfoBSchema.setEdorNo(pmEdorNo);
        //设置转储类型“薪资回退备份”
        tLAIndexInfoBSchema.setEdorType("09");

        return tLAIndexInfoBSchema;
    }

    /**
     * 处理薪资信息到备份表的数据准备
     * @return boolean
     */
    private boolean dealWage()
    {
        LAWageDB tLAWageDB = new LAWageDB();
        //设置查询条件
        String tSql = "SELECT *";
        tSql       += "  FROM LAWage";
        tSql       += " WHERE";
        tSql       += "    IndexCalNo = '" + mWageYM + "' AND";
        tSql       += "    ManageCom LIKE '" + mLAWageTerm.getManageCom() + "%' AND";
        tSql       += "    BranchType = '" + mLAWageTerm.getBranchType() + "' AND";
        tSql       += "    BranchType2 = '" + mLAWageTerm.getBranchType2() + "'";
        System.out.println("薪资查询：" + tSql);

        //查询数据库 得到结果
        mLAWageSet = tLAWageDB.executeQuery(tSql);

        if (mLAWageSet == null || mLAWageSet.size() < 1)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageGatherUI";
            tError.functionName = "checkWageType";
            tError.errorMessage = "检索薪资信息失败！";
            System.out.println("检索薪资信息失败！");
            this.mErrors.addOneError(tError);

            return false;
        }
        System.out.println("找到 " + mLAWageSet.size() + " 条薪资数据！" );

        //查询得到需要回退的薪资备份记录
        if(setLAWageBSet() == false)
            return false;

        return true;
    }

    /**
     * 设置薪资备份的数据
     * @return boolean
     */
    private boolean setLAWageBSet()
    {
        //判断待删除的薪资数据是否存在
        if(mLAWageSet==null || mLAWageSet.size()<1)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageGatherUI";
            tError.functionName = "setLAWageBSet";
            tError.errorMessage = "没有薪资备份务数据！";
            System.out.println("没有薪资备份务数据！");
            this.mErrors.addOneError(tError);

            return false;
        }

        //取得直接薪资明细表（薪资扎账表）的数据数
        int iMax = mLAWageSet.size();
        LAWageBSet tLAWageBSet = new LAWageBSet();
        //获得转储号码
        String tNewEdorNo = mNewEdorNo;

        //准备备份数据
        for(int i=1;i<=iMax;i++)
        {
            LAWageSchema tLAWageSchema = new LAWageSchema();
            tLAWageSchema = mLAWageSet.get(i).getSchema();
            if(tLAWageSchema!=null)
            {
                LAWageBSchema tLAWageBSchema = setLAWageBSchema(tLAWageSchema,tNewEdorNo);
                if(tLAWageBSchema!=null)
                    tLAWageBSet.add(tLAWageBSchema);
            }
        }

        //缓存备份数据
        mLAWageBSet = tLAWageBSet;

        return true;
    }

    /**
     * 设置备份数据
     * @param pmLAWageSchema LAWageSchema
     * @param pmEdorNo String
     * @return LAWageBSchema
     */
    private LAWageBSchema setLAWageBSchema(LAWageSchema pmLAWageSchema,
                                           String pmEdorNo)
    {
        LAWageBSchema tLAWageBSchema = new LAWageBSchema();
        Reflections tReflections = new Reflections();

        //设置备份表中和薪资表中相同的项
        tReflections.transFields(tLAWageBSchema,pmLAWageSchema);
        //设置转储号码
        tLAWageBSchema.setEdorNo(pmEdorNo);
        //设置转储类型“薪资回退备份”
        tLAWageBSchema.setEdorType("09");

        return tLAWageBSchema;
    }

    /**
     * 验证薪资是否未被发放，如果已经发放则不允许被回退
     * @return boolean
     * 已发放 返回：false 未发放 返回：true
     */
    private boolean checkWageType()
    {
        String tWageYM = "";
        String tSql = "";
        int dataCount = 0;

        try {
            //判断制定条件的数据是否存在
            tSql = "SELECT *";
            tSql += "  FROM LAWage";
            tSql += " WHERE";
            tSql += "    ManageCom like '" + mLAWageTerm.getManageCom() + "%' AND";
            tSql += "    BranchType = '" + mLAWageTerm.getBranchType() + "' AND";
            tSql += "    BranchType2 = '" + mLAWageTerm.getBranchType2() + "' AND";
            tSql += "    IndexCalNo = '" + mLAWageTerm.getIndexCalNo() + "'";


            LAWageDB tLAWageDB = new LAWageDB();
            LAWageSet tLAWageSet = new LAWageSet();

            tLAWageSet = tLAWageDB.executeQuery(tSql);
            dataCount = tLAWageSet.size();

            if(dataCount == 0)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAWageGatherUI";
                tError.functionName = "checkWageType";
                tError.errorMessage = "分公司编码为"+mLAWageTerm.getManageCom()+",薪资月为"+ mLAWageTerm.getIndexCalNo()+"薪资未进行确认！";
                System.out.println("指定薪资不存在！");
                this.mErrors.addOneError(tError);

                return false;
            }

           // 取得薪资回退年月
           tWageYM = mLAWageTerm.getIndexCalNo();
            //判断当前薪资发放状态，是否是已经发放
            tSql = "SELECT";
            tSql += "    COUNT(*)";
            tSql += "  FROM";
            tSql += "    LAWage";
            tSql += " WHERE";
            tSql += "    State = '1' AND";
            tSql += "    IndexCalNo = '" + tWageYM + "' AND";
            tSql += "     ManageCom like '" + mLAWageTerm.getManageCom() + "%' AND";
            tSql += "    BranchType = '" + mLAWageTerm.getBranchType() + "' AND";
            tSql += "    BranchType2 = '" + mLAWageTerm.getBranchType2() + "'";
            ExeSQL aExeSQL = new ExeSQL();
            String tNum=aExeSQL.getOneValue(tSql);
            System.out.println(tSql);
            System.out.println(tNum);
            if(!"0".equals(tNum))
            {
                  // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LAWageGatherUI";
                tError.functionName = "checkWageType";
                tError.errorMessage = tWageYM + " 的薪资已经发放，烦请先进行薪资审核发放回退操作！";
                this.mErrors.addOneError(tError);
                return false;
             }
        } catch (Exception ex) {
            ex.printStackTrace();
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageGatherUI";
            tError.functionName = "checkWageType";
            tError.errorMessage = "访问数据库失败！";
            System.out.println("访问数据库失败！checkWageType");
            this.mErrors.addOneError(tError);

            return false;
        }

        //缓存薪资年月
        mWageYM = tWageYM;

        return true;
    }

    /**
     * 返回查询结果
     * @param pmSql String
     * @param pmConn Connection
     * @return String
     */
    private String getSqlRs(String pmSql,Connection pmConn)
    {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String tRsValue = "";

        try {
            ps = pmConn.prepareStatement(pmSql);
            rs = ps.executeQuery();
            if (rs.next()) {
                tRsValue = rs.getString(1);
                rs.close();
                ps.close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return tRsValue;
    }

    /**
     * 从传入参数中得到全部数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //得到全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageGatherUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            System.out.println("没有得到足够的信息！");
            this.mErrors.addOneError(tError);
            return false;
        }

        //取得需要处理的数据的条件
        mLAWageTerm.setSchema((LAWageSchema)cInputData.getObjectByObjectName(
                              "LAWageSchema",0));
        if (mLAWageTerm == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LADataGatherUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收传入数据失败！";
            System.out.println("接收传入数据失败！");
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }
    
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LAPlanUnitBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
}
