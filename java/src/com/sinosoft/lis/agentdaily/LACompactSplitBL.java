/*
 * <p>ClassName: LABatchAuthorizeBL </p>
 * <p>Description: ALAAuthorizeUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-15
 */
package com.sinosoft.lis.agentdaily;

import java.sql.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class LACompactSplitBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAGrpCommisionDetailSet mLAGrpCommisionDetailSet = new
            LAGrpCommisionDetailSet(); // 接收用保单发佣分配数据
    private LAGrpCommisionDetailSet mLAGrpDealDetailSet = new
            LAGrpCommisionDetailSet(); // 保存用保单发佣分配数据
    private Reflections ref = new Reflections();
    private MMap mMap = new MMap();
    private VData mOutputDate = new VData();
    private String mGrpContNo = "";
    private String mMakeDate = "";
    private String mMakeTime = "";
    private String mCurrDate = PubFun.getCurrentDate();
    private String mModifyDate = PubFun.getCurrentDate();
    private String mModifyTime = PubFun.getCurrentTime();
    private String mMakeContNum="";
    private double mSumScale = 1; // 参加保费分配比例总合
    private int mBusiCount = 4; // 参加保费分配的最大人数
    private double mSumApp = 1; // 参加客户数分配比例总和
    private int mAppAllotCount = 2; // 参加客户数分配的最大人数

    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行有关验证
        if (!check()) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mOutputDate, "");
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LACompactSplitBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 准备往后台的数据
     * @return boolean
     */
    private boolean prepareOutputData() {
        mMakeContNum = PubFun1.CreateMaxNo("EdorNo", 20);
        LAGrpCommisionDetailSet tLAGrpCommisionDetailSet = new
                LAGrpCommisionDetailSet();
        LAGrpCommisionDetailDB tLAGrpCommisionDetailDB = new
                LAGrpCommisionDetailDB();
        LAGrpCommisionDetailBSet tmLAGrpCommisionDetailBSet = new  LAGrpCommisionDetailBSet();
        tLAGrpCommisionDetailDB.setGrpContNo(this.mGrpContNo);
        tLAGrpCommisionDetailDB.setRiskCode("0000000000");
        tLAGrpCommisionDetailSet = tLAGrpCommisionDetailDB.query();
        System.out.println("1111111111111"+tLAGrpCommisionDetailSet.size());
        if(tLAGrpCommisionDetailSet.size()>0)
        {
         System.out.println(tLAGrpCommisionDetailSet.size());
                for (int k = 1; k <= tLAGrpCommisionDetailSet.size(); k++) {
                    System.out.println("33333333333333");
                    LAGrpCommisionDetailBSchema  tmLAGrpCommisionDetailBSchema=new LAGrpCommisionDetailBSchema();
                    ref.transFields(tmLAGrpCommisionDetailBSchema,
                                    tLAGrpCommisionDetailSet.get(k));
                    tmLAGrpCommisionDetailBSchema.setEdorNO(mMakeContNum);
                    tmLAGrpCommisionDetailBSchema.setEdorType("01"); //保单分配
                    tmLAGrpCommisionDetailBSchema.setOperator2(mGlobalInput.
                            Operator);
                    tmLAGrpCommisionDetailBSchema.setMakeDate2(mCurrDate);
                    tmLAGrpCommisionDetailBSchema.setMakeTime2(mModifyTime);
                    tmLAGrpCommisionDetailBSchema.setModifyDate2(
                            mModifyDate);
                    tmLAGrpCommisionDetailBSchema.setModifyTime2(
                            mModifyTime);
                   tmLAGrpCommisionDetailBSet.add(tmLAGrpCommisionDetailBSchema);
                    System.out.println("1111111111111"+tmLAGrpCommisionDetailBSet.get(k).getAgentCode());
            }
            this.mMap.put(tmLAGrpCommisionDetailBSet, "INSERT");
        }
        if (!tLAGrpCommisionDetailSet.mErrors.needDealError()) {

                this.mMap.put(tLAGrpCommisionDetailSet, "DELETE");
            }


        this.mMap.put(this.mLAGrpDealDetailSet, "INSERT");

        mOutputDate.add(mMap);

        return true;
    }

    /**
     * 进行业务处理
     * @return boolean
     */
    private boolean dealData() {
        int tCount = mLAGrpCommisionDetailSet.size();

        for (int i = 1; i <= tCount; i++) {
            LAGrpCommisionDetailSchema tLAGrpCommisionDetailSchema = new
                    LAGrpCommisionDetailSchema();
            tLAGrpCommisionDetailSchema = mLAGrpCommisionDetailSet.get(i);
            // 处理数据,保存画面传进来的数据的有关完整数据
            if (!dealLAGrpCommisionDetail(tLAGrpCommisionDetailSchema)) {
                return false;
            }
        }

        return true;
    }

    /**
     * 准备数据,把传进来的数据处理后，保存在全局变量中 mLAGrpDealDetailSet
     * @param pmLAGrpCommisionDetailSchema LAGrpCommisionDetailSchema
     * @return boolean
     */
    private boolean dealLAGrpCommisionDetail(LAGrpCommisionDetailSchema
                                             pmLAGrpCommisionDetailSchema) {
        String tSQL = "";
        LAGrpCommisionDetailSchema tLAGrpCommisionDetailSchema = new
                LAGrpCommisionDetailSchema();

        // 验证本条记录是否曾经存在
        LAGrpCommisionDetailDB tLAGrpCommisionDetailDB = new
                LAGrpCommisionDetailDB();
        tLAGrpCommisionDetailDB.setGrpContNo(this.mGrpContNo);
        tLAGrpCommisionDetailDB.setRiskCode("0000000000");
        tLAGrpCommisionDetailDB.setAgentCode(pmLAGrpCommisionDetailSchema.
                                             getAgentCode());
        LAGrpCommisionDetailSet tLAGrpCommisionDetailSet = new
                LAGrpCommisionDetailSet();
        tLAGrpCommisionDetailSet = tLAGrpCommisionDetailDB.query();
        if (tLAGrpCommisionDetailDB.mErrors.needDealError()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LACompactSplitBL";
            tError.functionName = "dealLAGrpCommisionDetail";
            tError.errorMessage = "准备数据时出现错误！";
            this.mErrors.addOneError(tError);

            return false;
        } else {
            if (tLAGrpCommisionDetailSet.size() > 0) {
                mMakeDate = tLAGrpCommisionDetailSet.get(1).getMakeDate();
                mMakeTime = tLAGrpCommisionDetailSet.get(1).getMakeTime();
            } else {
                mMakeDate = mModifyDate;
                mMakeTime = mModifyTime;
            }
        }

        tLAGrpCommisionDetailSchema.setGrpContNo(this.mGrpContNo);
        tLAGrpCommisionDetailSchema.setRiskCode("0000000000");
        tLAGrpCommisionDetailSchema.setAgentCode(pmLAGrpCommisionDetailSchema.
                                                 getAgentCode());
        tLAGrpCommisionDetailSchema.setServFlag(getAgentServFlag(
                pmLAGrpCommisionDetailSchema.getAgentCode()));
        tLAGrpCommisionDetailSchema.setBusiRate(pmLAGrpCommisionDetailSchema.
                                                getBusiRate());
        tLAGrpCommisionDetailSchema.setAppAllotMark(
                pmLAGrpCommisionDetailSchema.
                getAppAllotMark());
        tLAGrpCommisionDetailSchema.setMngCom(pmLAGrpCommisionDetailSchema.
                                              getMngCom());
        tLAGrpCommisionDetailSchema.setOperator(this.mGlobalInput.Operator);
        tLAGrpCommisionDetailSchema.setMakeDate(mMakeDate);
        tLAGrpCommisionDetailSchema.setMakeTime(mMakeTime);
        tLAGrpCommisionDetailSchema.setModifyDate(mModifyDate);
        tLAGrpCommisionDetailSchema.setModifyTime(mModifyTime);
        tLAGrpCommisionDetailSchema.setAppntRate(pmLAGrpCommisionDetailSchema.
                                                 getAppntRate());

        mLAGrpDealDetailSet.add(tLAGrpCommisionDetailSchema);

        return true;
    }

    /**
     * 取得是否是服务人的标记
     * @param pmAgentCode String
     * @return String
     */
    private String getAgentServFlag(String pmAgentCode) {
        String tRTServFlag = "0";
        String tSQL = "";

        // 判断如果是LACommision表里面提过来的原设定负责人  设置标记 ‘1’
        tSQL = "select count(*) from lcgrpcont where GrpContNo = '" +
               this.mGrpContNo + "' and AgentCode='" + pmAgentCode + "'";
        if (0 < execQuery(tSQL)) {
            tRTServFlag = "1";
        }

        return tRTServFlag;
    }

    /**
     * 进行有关验证
     *    1、验证人员是否重复
     *    2、验证业务比例之和必须等于 100%
     *    3、参加分配保费的最多只能是4个人
     *    4、参加分配客户数的最多只能是2个人
     * @return boolean
     */
    private boolean check() {
        float tSumScale = 0; // 保费分配比例总合
        double tSumApp = 0; // 保费分配比例总合
        int tBusiCount = 0; // 参加保费分配人数
        int tAppAllotCount = 0; // 参加客户数分配的人数
        int k = 0;
        String tsql = "";
        String tAgentCon = ""; //代理人集合
        tsql = "select count(*) from LAWageHistory where managecom like '" +
               mLAGrpCommisionDetailSet.get(1).getMngCom() + "%' and wageno='"
               + mCurrDate.substring(0, 4) + mCurrDate.substring(5, 7) +
               "'   and branchtype='2' and BranchType2='01' and state='14'   ";

        ExeSQL tExeSQL = new ExeSQL();
        String strWage = tExeSQL.getOneValue(tsql);
        if (tExeSQL.mErrors.needDealError()) {

            CError tError = new CError();
            tError.moduleName = "LACompactSplitBL";
            tError.functionName = "check";
            tError.errorMessage = "查询数据 LAWageHistory 出错！";
            this.mErrors.addOneError(tError);

            return false;
        }
        if (strWage != null && !strWage.equals("") && !strWage.equals("0")) {
            CError tError = new CError();
            tError.moduleName = "LACompactSplitBL";
            tError.functionName = "check";
            tError.errorMessage = mGrpContNo + "本月已经进行过薪资计算，不能拆分保单！";
            this.mErrors.addOneError(tError);
            System.out.println("本月已经进行过薪资计算，不能拆分保单！");

            return false;

        }

        // 1、验证人员是否重复
        for (int i = 1; i <= mLAGrpCommisionDetailSet.size(); i++) {
            LAGrpCommisionDetailSchema tLAGrpCommisionDetailSchema = new
                    LAGrpCommisionDetailSchema();
            tLAGrpCommisionDetailSchema.setSchema(mLAGrpCommisionDetailSet.get(
                    i));
            String tAgentCode = tLAGrpCommisionDetailSchema.getAgentCode();
            k = 0;
            if (tAgentCon.equals("")) {
                tAgentCon = "'" + tAgentCode + "'";
            }else {
                tAgentCon = tAgentCon + ",'" + tAgentCode + "'";
            }

            for (int j = 1; j <= mLAGrpCommisionDetailSet.size(); j++) {
                if (tAgentCode.equals(mLAGrpCommisionDetailSet.get(j).
                                      getAgentCode())) { // 如果业务员代码相同,报错返回

                    if (k > 0) { // 如果出现了两次重复的人
                        CError tError = new CError();
                        tError.moduleName = "LACompactSplitBL";
                        tError.functionName = "check";
                        tError.errorMessage = "分配业务员不能重复！";
                        this.mErrors.addOneError(tError);
                        System.out.println("分配业务员不能重复！");

                        return false;
                    }
                    k++;
                }
            }
            // 2、验证业务比例之和必须等于 100%
            if (tLAGrpCommisionDetailSchema.getBusiRate() > 1 ||
                tLAGrpCommisionDetailSchema.getBusiRate() < 0 ||
                !(tLAGrpCommisionDetailSchema.getBusiRate() >= 0 &&
                  tLAGrpCommisionDetailSchema.getBusiRate() <= 1)) {
                CError tError = new CError();
                tError.moduleName = "LACompactSplitBL";
                tError.functionName = "check";
                tError.errorMessage = "请输入正确的保费分配比例！";
                this.mErrors.addOneError(tError);
                System.out.println("请输入正确的保费分配比例！");

                return false;
            }

            // 求保费分配比例总合
            tSumScale += tLAGrpCommisionDetailSchema.getBusiRate();
            tSumApp += tLAGrpCommisionDetailSchema.getAppntRate();
            // 求分配保费的人数

            if (0 < tLAGrpCommisionDetailSchema.getBusiRate()) {
                tBusiCount++;
            }
            // 求参加分配客户数的人数
            if (0 < tLAGrpCommisionDetailSchema.getAppntRate()) {
                tAppAllotCount++;
            }
        }
        //拆分的业务员中必须包含原保单业务员
        tsql = "select grpcontno from lcgrpcont   where grpcontno='" +
               mGrpContNo + "' and agentcode in (" + tAgentCon + ")"
               + " union "
               + "select grpcontno from lBgrpcont   where grpcontno='" +
               mGrpContNo + "' and agentcode in (" + tAgentCon + ")";

        tExeSQL = new ExeSQL();
        String strMainAgent = tExeSQL.getOneValue(tsql);
        if (tExeSQL.mErrors.needDealError()) {

            CError tError = new CError();
            tError.moduleName = "LACompactSplitBL";
            tError.functionName = "check";
            tError.errorMessage = "查询数据 LPGrpEdorMain 出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (strMainAgent == null || strMainAgent.equals("") ||
            strMainAgent.equals("0")) {
            CError tError = new CError();
            tError.moduleName = "LACompactSplitBL";
            tError.functionName = "check";
            tError.errorMessage = mGrpContNo + "拆分的业务员中必须包含原保单业务员！";
            this.mErrors.addOneError(tError);
            System.out.println(mGrpContNo + "拆分的业务员中必须包含原保单业务员！");
            return false;

        }
        // 2、验证业务比例之和必须等于 100%
        if (tSumApp != mSumApp) {
            CError tError = new CError();
            tError.moduleName = "LACompactSplitBL";
            tError.functionName = "check";
            tError.errorMessage = "客户数分配比例之合必须等于‘1’！";
            this.mErrors.addOneError(tError);
            System.out.println("客户数分配比例之合必须等于‘1’！"+tSumApp);

            return false;
        }

        // 2、验证业务比例之和必须等于 100%
        if (tSumScale != mSumScale) {
            CError tError = new CError();
            tError.moduleName = "LACompactSplitBL";
            tError.functionName = "check";
            tError.errorMessage = "保费分配比例之合必须等于‘1’！";
            this.mErrors.addOneError(tError);
            System.out.println("保费分配比例之合必须等于‘1’！"+tSumScale);

            return false;
        }

        // 3、参加分配保费的最多只能是4个人
        if (tBusiCount > mBusiCount) {
            CError tError = new CError();
            tError.moduleName = "LACompactSplitBL";
            tError.functionName = "check";
            tError.errorMessage = "参加保费分配的最多只能" + mBusiCount + "个人！";
            this.mErrors.addOneError(tError);
            System.out.println("参加保费分配的最多只能" + mBusiCount + "个人！");

            return false;
        }

        // 4、参加分配客户数的最多只能是2个人
        if (tAppAllotCount > mAppAllotCount) {
            CError tError = new CError();
            tError.moduleName = "LACompactSplitBL";
            tError.functionName = "check";
            tError.errorMessage = "参加客户数分配的最多只能" + mAppAllotCount + "个人！";
            this.mErrors.addOneError(tError);
            System.out.println("参加客户数分配的最多只能" + mAppAllotCount + "个人！");

            return false;
        }

        // 5、不能没人参加分配客户数
        if (tAppAllotCount == 0) {
            CError tError = new CError();
            tError.moduleName = "LACompactSplitBL";
            tError.functionName = "check";
            tError.errorMessage = "不能没人参加客户数的分配！";
            this.mErrors.addOneError(tError);
            System.out.println("不能没人参加客户数的分配！");

            return false;
        }

        return true;
    }

    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return int
     */
    private int execQuery(String sql) {
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null) {
                return 0;
            }
            st = conn.prepareStatement(sql);
            if (st == null) {
                return 0;
            }
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
                st.close();
                rs.close();
                conn.close();
                st = null;
                rs = null;
                conn = null;
            } catch (Exception e) {}
        }
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData pmInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) pmInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLAGrpCommisionDetailSet.set((LAGrpCommisionDetailSet) pmInputData.
                                     getObjectByObjectName(
                                             "LAGrpCommisionDetailSet", 0));
        this.mGrpContNo = (String) pmInputData.getObject(0);
        System.out.println("保单号：[ " + this.mGrpContNo + " ]");
        if (mGlobalInput == null && mLAGrpCommisionDetailSet == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAuthorizeUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

}
