package com.sinosoft.lis.agentdaily;


import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.agentdaily.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.db.*;

/**
 * <p>Title: LAManageAllowRateBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author luomin
 * @version 1.0
 */
public class LAManageAllowRateBL {
  //错误处理类
  public static CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public  GlobalInput mGlobalInput = new GlobalInput();

  private LABaseWageSet mLABaseWageSet = new LABaseWageSet();

  private Reflections ref = new Reflections();

  public LAManageAllowRateBL() {

  }


  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LAManageAllowRateBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }

    if (!check()) {
     return false;
   }

    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start LAManageAllowRateBL Submit...");
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LAManageAllowRateBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      this.mErrors.clearErrors();
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     this.mLABaseWageSet.set( (LABaseWageSet) cInputData.getObjectByObjectName("LABaseWageSet",0));

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAManageAllowRateBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LAManageAllowRateBL.dealData........."+mOperate);
    try {
        if (mOperate.equals("UPDATE") || mOperate.equals("DELETE")) {
          System.out.println("Begin LAManageAllowRateBL.dealData.........1"+mLABaseWageSet.size());
          LABaseWageSet tLABaseWageSet = new LABaseWageSet();
          LABaseWageBSet tLABaseWageBSet = new LABaseWageBSet();
          ExeSQL tExe = new ExeSQL();
          String tSql =
              "select max(int(edorno)) from labasewageb  ";
          String strIdx = "";
          int tMaxIdx = 0;

          strIdx = tExe.getOneValue(tSql);
          if (strIdx == null || strIdx.trim().equals("")) {
            tMaxIdx = 0;
          }
          else {
            tMaxIdx = Integer.parseInt(strIdx);
          }
          for (int i = 1; i <= mLABaseWageSet.size(); i++) {
            System.out.println("Begin LAManageAllowRateBL.dealData.........2");
            LABaseWageSchema tLABaseWageSchemaold = new LABaseWageSchema();
            LABaseWageSchema tLABaseWageSchemanew = new  LABaseWageSchema();
            LABaseWageDB tLABaseWageDB = new LABaseWageDB();
            System.out.println("Begin LAManageAllowRateBL.dealData.........3");
            System.out.println("++++++++"+mLABaseWageSet.get(i).getYearRate());
            System.out.println("++++++++"+mLABaseWageSet.get(i).getIdx());


            tLABaseWageDB.setIdx(mLABaseWageSet.get(i).getIdx());
            tLABaseWageDB.setAgentGrade(mLABaseWageSet.get(i).getAgentGrade());
            tLABaseWageSchemaold = tLABaseWageDB.query().get(1);
            tLABaseWageSchemanew = mLABaseWageSet.get(i);
            //tLARateCommisionSchemanew.setManageCom(mGlobalInput.ManageCom);
            tLABaseWageSchemanew.setOperator(mGlobalInput.Operator);
            tLABaseWageSchemanew.setModifyDate(CurrentDate);
            tLABaseWageSchemanew.setModifyTime(CurrentTime);
            tLABaseWageSchemanew.setMakeDate(tLABaseWageSchemaold.getMakeDate());
            tLABaseWageSchemanew.setMakeTime(tLABaseWageSchemaold.getMakeTime());
            tLABaseWageSchemanew.setType(tLABaseWageSchemaold.getType());

            tLABaseWageSet.add(tLABaseWageSchemanew);
            LABaseWageBSchema tLABaseWageBSchema = new
                LABaseWageBSchema();
            ref.transFields(tLABaseWageBSchema, tLABaseWageSchemaold);
            //获取最大的ID号
            tMaxIdx++;
            tLABaseWageBSchema.setEdorNo(String.valueOf(tMaxIdx));
//            if(i>1){
//              tLABaseWageBSchema.setEdorNo(tLABaseWageBSet.get(i-1).getEdorNo() + 1);
//              System.out.println("BL:"+tLABaseWageBSet.get(i-1).getEdorNo());
//            }
            tLABaseWageBSchema.setEdorType("01");

            tLABaseWageBSet.add(tLABaseWageBSchema);

          }
          map.put(tLABaseWageSet, mOperate);
          map.put(tLABaseWageBSet, "INSERT");
        }

      else {
      LABaseWageSet tLABaseWageSet = new   LABaseWageSet();

      for (int i = 1; i <= mLABaseWageSet.size(); i++) {
       LABaseWageSchema tLABaseWageSchemanew = new    LABaseWageSchema();
       tLABaseWageSchemanew = mLABaseWageSet.get(i);

  /**
       //获取最大的ID号
      ExeSQL tExe = new ExeSQL();
      String tSql =
          "select int(max(idx)) from labasewage order by 1 desc ";
      String strIdx = "";
      int tMaxIdx = 0;

      strIdx = tExe.getOneValue(tSql);
      if (strIdx == null || strIdx.trim().equals("")) {
        tMaxIdx = 0;
      }
      else {
        tMaxIdx = Integer.parseInt(strIdx);
        System.out.println(tMaxIdx);
      }
      tMaxIdx++;
      tLABaseWageSchemanew.setIdx(tMaxIdx);
      if(i>1){
        tLABaseWageSchemanew.setIdx(mLABaseWageSet.get(i - 1).getIdx() + 1);
      }
*/
          tLABaseWageSchemanew.setOperator(mGlobalInput.Operator);
          tLABaseWageSchemanew.setMakeDate(CurrentDate);
          tLABaseWageSchemanew.setMakeTime(CurrentTime);
          tLABaseWageSchemanew.setModifyDate(CurrentDate);
          tLABaseWageSchemanew.setModifyTime(CurrentTime);
          tLABaseWageSchemanew.setType("12");
          tLABaseWageSet.add(tLABaseWageSchemanew);

        }
        map.put(tLABaseWageSet, mOperate);
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAManageAllowRateBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }


  private boolean check(){
             if(!this.mOperate.equals("DELETE")){
             LABaseWageSchema tempLABaseWageSchema1;
             LABaseWageSchema tempLABaseWageSchema2;
             ExeSQL tExeSQL=new ExeSQL();
             StringBuffer tSB;
             String tResult;
             for(int i=1;i<=this.mLABaseWageSet.size();i++){
                     tempLABaseWageSchema1=this.mLABaseWageSet.get(i);
                     for(int j=i+1;j<=this.mLABaseWageSet.size();j++){
                             tempLABaseWageSchema2=this.mLABaseWageSet.get(j);
                             if(tempLABaseWageSchema1.getAgentGrade()
                                             .equals(tempLABaseWageSchema2.getAgentGrade())
                                            ){
                                     System.out.println("...........erro1");
                                     CError tError = new CError();
                                 tError.moduleName = "LABankBaseWageBL";
                                 tError.functionName = "check()";
                                 tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行与第"+j+"行数据重复。";
                                 this.mErrors.clearErrors();
                                 this.mErrors.addOneError(tError);
                                 return false;
                             }
                     }
//                   if(this.mOperate.equals("INSERT")){
//                           tSB=new StringBuffer();
//                           tSB=tSB.append("select distinct 1 from laratecommision where agentgrade='")
//                           .append(tempLABaseWageSchema1.getRiskCode())
//                           .append("' and curyear=")
//                           .append(tempLARateCommisionSchema1.getCurYear())
//                           .append(" and payintv='")
//                           .append(tempLARateCommisionSchema1.getPayIntv())
//                           .append("' and f03=")
//                           .append(tempLARateCommisionSchema1.getF03())
//                           .append(" and f04=")
//                           .append(tempLARateCommisionSchema1.getF04())
//                           .append(" and managecom = '")
//                           .append(tempLARateCommisionSchema1.getManageCom())
//                           .append("'")
//                           .append(" and branchtype='")
//                           .append(tempLARateCommisionSchema1.getBranchType())
//                           .append("' and branchtype2='")
//                           .append(tempLARateCommisionSchema1.getBranchType2())
//                           .append("'");
//                           tResult=tExeSQL.getOneValue(tSB.toString());
//                           if(tResult!=null&&tResult.equals("1")){
//                                   System.out.println("...........erro2");
//                                   CError tError = new CError();
//                               tError.moduleName = "LaratecommisionSetBL";
//                               tError.functionName = "check()";
//                               tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行数据在数据库中已经存在。";
//                               this.mErrors.clearErrors();
//                               this.mErrors.addOneError(tError);
//                               return false;
//                           }
//                   }
//                   if(this.mOperate.equals("UPDATE")){
//                           SSRS tSSRS=new SSRS();
//                           tSB=new StringBuffer();
//                           tSB=tSB.append("select distinct idx from laratecommision where riskcode='")
//                           .append(tempLARateCommisionSchema1.getRiskCode())
//                           .append("' and curyear=")
//                           .append(tempLARateCommisionSchema1.getCurYear())
//                           .append(" and payintv='")
//                           .append(tempLARateCommisionSchema1.getPayIntv())
//                           .append("' and f03=")
//                           .append(tempLARateCommisionSchema1.getF03())
//                           .append(" and f04=")
//                           .append(tempLARateCommisionSchema1.getF04())
//                           .append(" and managecom = '")
//                           .append(tempLARateCommisionSchema1.getManageCom())
//                           .append("'")
//                           .append(" and branchtype='")
//                           .append(tempLARateCommisionSchema1.getBranchType())
//                           .append("' and branchtype2='")
//                           .append(tempLARateCommisionSchema1.getBranchType2())
//                           .append("'");
//                           tSSRS=tExeSQL.execSQL(tSB.toString());
//                           for(int m=1;m<=tSSRS.getMaxRow();m++){
//                                   System.out.println("...........tSSRS.GetText(m,1)"+tSSRS.GetText(m,1));
//                                   if(tempLARateCommisionSchema1.getIdx()!=Integer.parseInt(tSSRS.GetText(m,1))){
//                                           CError tError = new CError();
//                                       tError.moduleName = "LaratecommisionSetBL";
//                                       tError.functionName = "check()";
//                                       tError.errorMessage = "在验证操作数据时出错。在选中的数据中第"+i+"行数据在数据库中已经存在。";
//                                       this.mErrors.clearErrors();
//                                       this.mErrors.addOneError(tError);
//                                       return false;
//                                   }
//                           }
//                   }
             }
             }
             return true;
    }




  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      System.out.println(
          "Begin LAManageAllowRateBL.prepareOutputData.........");
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LAManageAllowRateBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
}
