package com.sinosoft.lis.agentdaily;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LAAscriptionSet;
import com.sinosoft.lis.db.LAAscriptionDB;
import com.sinosoft.utility.SSRS;
import java.math.BigInteger;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.vdb.LAAgentDBSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.vschema.LAOrphanPolicySet;
import com.sinosoft.lis.vschema.LAOrphanPolicyBSet;
import com.sinosoft.lis.pubfun.PubFun1;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LAAtoAscriptionBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData=new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mManageCom = "";
    private String mBranchAttr= "";
    private String mAgentCode = "";
    private String mOutWorkDate = "";
    private String tAgentNew = "";
    private String tAgentGoupNew = "";
    private String tContno="";
    private String tGrpContno="";
    private int mLength=10000;//得到一次提交的长度
    private LAAscriptionSchema cLAAscriptionSchema = new LAAscriptionSchema();
    private LAAscriptionSet mLAAscriptionSet = new LAAscriptionSet();
    private LAAscriptionSet preLAAscriptionSet = new LAAscriptionSet();
    private LAOrphanPolicySet  mLAOrphanPolicySet = new LAOrphanPolicySet();
    private LAOrphanPolicyBSet  mLAOrphanPolicyBSet = new LAOrphanPolicyBSet();
    private LAOrphanPolicyBSchema mLAOrphanPolicyBSchema = new LAOrphanPolicyBSchema ();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private MMap mMap = new MMap();
    public LAAtoAscriptionBL()
    {
    }
    public static void main(String[] args)
    {
        LAAtoAscriptionBL BL = new LAAtoAscriptionBL();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.ComCode = "86";
        tGlobalInput.Operator = "mak001";
        VData tVData = new VData();
        LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();

        tLAAscriptionSchema.setBranchType("1");
        tLAAscriptionSchema.setBranchType2("01");
        String ManageCom="86110000";
        tVData.add(ManageCom);
        tVData.add("");
        tVData.add("1101000012");
        tVData.add("");
        tVData.add(tGlobalInput);
  	    tVData.add(tLAAscriptionSchema);
        BL.submitData(tVData, "INSERT||MAIN");
    }
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        System.out.println("Operate:"+cOperate);
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAtoAscriptionBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAAtoAscriptionBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
       //     this.submitquery();
        }
        else
        {
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("Start LAAtoAscriptionBL Submit...");
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAtoAscriptionBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("End LAAtoAscriptionBL Submit...");
        }
        mInputData = null;
        return true;
    }
    private boolean dealData()
    {
        SSRS tSSRS_agent = new SSRS();
        //得到离职人员
        tSSRS_agent=getAgent();
        if(tSSRS_agent==null)
        {
             return false ;
        }

       int tCount_agent= tSSRS_agent.getMaxRow() ;
       for(int i=1;i<=tCount_agent;i++)
       {
        String tAgentCode = tSSRS_agent.GetText(i, 1);
        String tAgentGroup = tSSRS_agent.GetText(i, 2);
        String tsql= "select branchattr from labranchgroup where agentgroup='" + tAgentGroup + "'";
        ExeSQL tstrExeSQL = new ExeSQL();
        String tOldBranchAttr = tstrExeSQL.getOneValue(tsql);

        String tSql = "select * from LAOrphanPolicy where managecom='"
                      +mManageCom+"'   and agentcode='" + tAgentCode + "' ";

        LAOrphanPolicyDB tLAOrphanPolicyDB = new LAOrphanPolicyDB();
        LAOrphanPolicySet tLAOrphanPolicySet = new LAOrphanPolicySet();
        System.out.println("查询孤儿单信息："+tSql);
        int tStart=1;
        do {
          tLAOrphanPolicySet = tLAOrphanPolicyDB.executeQuery(tSql, tStart, mLength);


          for(int k=1;k<=tLAOrphanPolicySet.size();k++)
          {
               String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
               LAOrphanPolicySchema tLAOrphanPolicySchema = new LAOrphanPolicySchema();
               tLAOrphanPolicySchema=tLAOrphanPolicySet.get(k);
               mLAOrphanPolicySet.add(tLAOrphanPolicySchema);
               LAOrphanPolicyBSchema tLAOrphanPolicyBSchema = new LAOrphanPolicyBSchema();
               Reflections tReflections=new Reflections();
               tReflections.transFields(tLAOrphanPolicyBSchema,tLAOrphanPolicySchema);
               tLAOrphanPolicyBSchema.setEdorNo(tEdorNo);
               tLAOrphanPolicyBSchema.setEdorType("01");
               mLAOrphanPolicyBSet.add(tLAOrphanPolicyBSchema);
               tContno = tLAOrphanPolicySchema.getContNo();
               tGrpContno= tLAOrphanPolicySchema.getContNo();
               String tManageCom = tLAOrphanPolicySchema.getManageCom();
               //查询归属表,如果存在该保单,并且有效,则已经作了归属,返回,(保证归属表保单只有一条有效的纪录)
               String tsql0 =
                            "select AscripNo from LAAscription where ContNo='" +tContno+
                            "' and  validflag='N' and agentold ='" +tAgentCode+"'"
                            +" union  select AscripNo from LAAscription where grpContNo='" +tGrpContno+
                            "' and  validflag='N' and agentold ='" +tAgentCode+"'";
               ExeSQL tstrExeSQL0 = new ExeSQL();
//               String tAscripNo = tstrExeSQL0.getOneValue(tsql0);
//               if (tAscripNo != null && !tAscripNo.equals("")) {
//                        continue; //做了归属,并且没有成历史则纪录,还有效,说明可能作了手工归属  返回,处理下一保单
//               }
                    //得到新的代理人,先查询推荐人,没有或离职则查找主管,没有主管或离职则归属失败
               if (!getNewAgent(tAgentCode, tAgentGroup)) {
                     return false;
               }
               String mAscripNo = com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo(
                                      "PerAscripNo", 20);
               String strsql= "select count(ascripno)+1 from laascription where 1=1 and ascripstate='3' "
            	   +" and (contno='"+tContno+"' or grpcontno='"+tGrpContno+"')";
               ExeSQL tExeSQL1 = new ExeSQL();
               String mCount = tExeSQL1.getOneValue(strsql);
               LAAscriptionSchema mLAAscriptionSchema = new LAAscriptionSchema();
               mLAAscriptionSchema.setBranchType(cLAAscriptionSchema.getBranchType());
               mLAAscriptionSchema.setBranchType2(cLAAscriptionSchema.getBranchType2());
               mLAAscriptionSchema.setAscripNo(mAscripNo);
               mLAAscriptionSchema.setAscriptionCount(mCount);
               mLAAscriptionSchema.setAgentGroup(tAgentGoupNew);
               mLAAscriptionSchema.setBranchAttr(tOldBranchAttr); //原代理人的展业机构
               mLAAscriptionSchema.setAgentNew(tAgentNew);
               mLAAscriptionSchema.setAgentOld(tAgentCode);
               if(tLAOrphanPolicySchema.getReasonType().equals("0")){ mLAAscriptionSchema.setContNo(tContno); }
               else {mLAAscriptionSchema.setGrpContNo(tGrpContno); }
               
               mLAAscriptionSchema.setAClass("01"); //代理人离职
               mLAAscriptionSchema.setValidFlag("N");
               mLAAscriptionSchema.setMakeType("02");//系统自动归属
               mLAAscriptionSchema.setManageCom(tManageCom);
               mLAAscriptionSchema.setAscriptionDate(currentDate);
               mLAAscriptionSchema.setOperator(mGlobalInput.Operator);
               mLAAscriptionSchema.setMakeDate(currentDate);
               mLAAscriptionSchema.setMakeTime(currentTime);
               mLAAscriptionSchema.setModifyDate(currentDate);
               mLAAscriptionSchema.setModifyTime(currentTime);
               if(tAgentNew.equals(""))
               {
                   mLAAscriptionSchema.setAscripState("1");//没有找到新代理人,归属失败
               }
               else
               {
                   mLAAscriptionSchema.setAscripState("2");//找到新代理人,归属成功
                }
               this.mLAAscriptionSet.add(mLAAscriptionSchema);
               //修改保单的历史信息
               tsql0 = "select AscripNo from LAAscription where ContNo='" + tContno +"' and  AscripState<>'3'";
               //tsql0 = "select AscripNo from LAAscription where ContNo='" + tContno +"' and  ValidFlag='N' and  AscripState='3'";

               tstrExeSQL0 = new ExeSQL();
               String strAscripNo0 = tstrExeSQL0.getOneValue(tsql0);
               if (strAscripNo0 != null && !strAscripNo0.equals(""))
               {
                    //此保单已归属成功,并已有历史纪录并且没有成为无效的历史纪录，则将它修改为无效的历史纪录
                    //归属表只能有一条有效的历史纪录
                    LAAscriptionSchema    tLAAscriptionSchema= new LAAscriptionSchema();
                    LAAscriptionDB tLAAscriptionDB = new LAAscriptionDB();
                    tLAAscriptionDB.setAscripNo(strAscripNo0);
                    tLAAscriptionDB.getInfo();
                    tReflections=new Reflections();
                    tReflections.transFields(tLAAscriptionSchema, tLAAscriptionDB.getSchema());
                    tLAAscriptionSchema.setValidFlag("Y");//修改为无效的历史纪录
                    preLAAscriptionSet.add(tLAAscriptionSchema);
                 }
              }
                if(mLAAscriptionSet.size()>=mLength)
                {//进行事物拆分,set 中大于mLength时事物就执行,完成以后使 set 为空,原来的数据不再执行
                    if (!prepareOutputData())
                    {
                        return false;
                    }
                    PubSubmit tPubSubmit = new PubSubmit();
                   System.out.println("Start LAAtoAscriptionBL Submit...");
                   if (!tPubSubmit.submitData(mInputData, mOperate))
                   {
                       // @@错误处理
                       this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                       CError tError = new CError();
                       tError.moduleName = "LAAtoAscriptionBL";
                       tError.functionName = "submitData";
                       tError.errorMessage = "数据提交失败!";
                       this.mErrors.addOneError(tError);
                       return false;
                   }
                   preLAAscriptionSet.clear();
                   mLAAscriptionSet.clear();
                   mMap = new MMap();
                   mInputData.clear();
                }
                tStart=tStart+mLength;
            } while(tLAOrphanPolicySet.size()>0) ;
       }
        return true;
    }
   //得到离职人员
    private SSRS getAgent()
    {
        String tBranchType=cLAAscriptionSchema.getBranchType();
        String tBranchType2=cLAAscriptionSchema.getBranchType2();
        String sql_agent="select distinct a.AgentCode,a.AgentGroup ";
               sql_agent+=" from LAAgent a,LABranchgroup b ";
               sql_agent+=" where a.AgentGroup=b.AgentGroup and a.ManageCom like '"+mManageCom+"%'";
               sql_agent+=" and a.AgentState>='06' and a.BranchType='"+tBranchType+"'   ";
               sql_agent+=" and  a.BranchType2='"+tBranchType2+"' ";
               if("1".equals(tBranchType)&&"01".equals(tBranchType2))
               {
            	   sql_agent+=" and a.employdate<'2013-4-1' ";
               }
               if( mBranchAttr!=null && !mBranchAttr.equals("") )
             {
               sql_agent+=" and b.branchattr like '" + mBranchAttr +"%'";
             }
             if( mAgentCode !=null && !mAgentCode.equals("") )
             {
               sql_agent+=" and a.agentcode='" + mAgentCode + "' ";
             }
             if( mOutWorkDate !=null && !mOutWorkDate.equals("") )
             {
               sql_agent+=" and a.outworkdate='" + mOutWorkDate + "' ";
             }
               sql_agent+="order by a.agentgroup,a.agentcode ";

        System.out.println("查询离职人员信息："+sql_agent);
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql_agent);
        int tCount=tSSRS.MaxRow;
        if (tExeSQL.mErrors.needDealError())
        {
          this.mErrors.copyAllErrors(tExeSQL.mErrors);
          CError tError = new CError();
          tError.moduleName = "LAAscriptionBL";
          tError.functionName = "getAgent";
          tError.errorMessage = "查询离职信息出错！";
          this.mErrors.addOneError(tError);
          return null;
         }
        if(tCount<1)
        {
          CError tError = new CError();
          tError.moduleName = "LAAscriptionBL";
          tError.functionName = "getAgent";
          tError.errorMessage = "没有离职人员的信息！";
          this.mErrors.addOneError(tError);
          return null;
        }
        return tSSRS;
    }
    //得到新的代理人,先查询推荐人,没有或离职则查找主管,没有主管或离职则归属失败
    private boolean  getNewAgent(String tAgentCode,String tAgentGroup)
    {
        String tBranchType=cLAAscriptionSchema.getBranchType();
        String tBranchType2=cLAAscriptionSchema.getBranchType2();
        int tFlag=0;//为1标记  表示代理人离职
        String sql_agent="select a.IntroAgency from latree a  where a.agentcode='"
        	+tAgentCode+"' ";
        	//+"' and a.managecom = (select managecom from laagent where agentcode=a.introagency) with ur";
        System.out.println(sql_agent);
        SSRS tSSRS = new SSRS();
        ExeSQL tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(sql_agent);
        int tCount=tSSRS.MaxRow;
        String tIntroAgency=tSSRS.GetText(1, 1);
        if (tExeSQL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAscriptionBL";
            tError.functionName = "getAgent";
            tError.errorMessage = "查询离职信息出错！";
            this.mErrors.addOneError(tError);
            return false;
         }
        if(tCount<1)
        {
            CError tError = new CError();
            tError.moduleName = "LAAscriptionBL";
            tError.functionName = "getAgent";
            tError.errorMessage = "没有离职人员的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if(tIntroAgency!=null && !tIntroAgency.equals("") )
        {//如果推荐人存在
            LAAgentDB tLAAgentDB=new LAAgentDB();
            LAAgentSchema tLAAgentSchema=new LAAgentSchema();
            tLAAgentDB.setAgentCode(tIntroAgency);
            tLAAgentDB.getInfo();
            tLAAgentSchema=tLAAgentDB.getSchema();
            String tAgentSate=tLAAgentSchema.getAgentState();
            if (tAgentSate!=null && tAgentSate.compareTo("03")>=0)
            {//推荐人离职，使tFlag=1
                tFlag=1;
            }
            else
            {//推荐人存在且没有离职，使tFlag=0，并使新代理人为推荐人
                tFlag = 0;
                tAgentNew = tIntroAgency;
                tAgentGoupNew=tLAAgentSchema.getAgentGroup();
                return true ;
            }
        }
        if(tIntroAgency==null || tIntroAgency.equals("") || tFlag==1)
        {//如果没有推荐人，或者推荐人已离职
            sql_agent="select a.UpAgent from latree a where a.agentcode='"
            	+tAgentCode+"' ";
            	//+"' and a.managecom = (select managecom from laagent where agentcode=a.upagent) with ur";
            System.out.println(sql_agent);
            tSSRS = new SSRS();
            tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(sql_agent);
            tCount=tSSRS.MaxRow;
            if (tExeSQL.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAAscriptionBL";
                tError.functionName = "getAgent";
                tError.errorMessage = "查询离职人员的团队信息出错！";
                this.mErrors.addOneError(tError);
                return false;
           }
           if(tCount<1)
           {
               CError tError = new CError();
               tError.moduleName = "LAAscriptionBL";
               tError.functionName = "getAgent";
               tError.errorMessage = "没有离职人员的团队信息！";
               this.mErrors.addOneError(tError);
               return false;
           }
          String tBranchManager=tSSRS.GetText(1, 1);
          if(tBranchManager==null || tBranchManager.equals(""))
          {//如果没有主管，则返回，置为归属失败
              tAgentNew = "";
              tAgentGoupNew="";
              return true;
          }
          //如果有主管 ，则判断是否离职
          LAAgentDB tLAAgentDB=new LAAgentDB();
          LAAgentSchema tLAAgentSchema=new LAAgentSchema();
          tLAAgentDB.setAgentCode(tBranchManager);
          tLAAgentDB.getInfo();
          tLAAgentSchema=tLAAgentDB.getSchema();
          String tAgentSate=tLAAgentSchema.getAgentState();
          if (tAgentSate!=null && tAgentSate.compareTo("03")>0)
          {//主管离职
              tAgentNew = "";
              tAgentGoupNew="";
              return true;
          }
          else
          {//主管存在且没有离职，使新代理人为主管
              tAgentNew = tBranchManager;
              tAgentGoupNew=tLAAgentSchema.getAgentGroup();
          }
        }
        return true;
    }
    private boolean getInputData(VData cInputData)
    {

        this.cLAAscriptionSchema.setSchema((LAAscriptionSchema) cInputData.
                                           getObjectByObjectName(
                "LAAscriptionSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        mManageCom=(String) cInputData.get(0);
        mBranchAttr =(String) cInputData.get(1);
        mAgentCode =(String) cInputData.get(2);
        mOutWorkDate = (String) cInputData.get(3);
        return true;
      }
      private boolean prepareOutputData()
      {
          System.out.println("处理孤儿单信息个数："+mLAOrphanPolicySet.size());
          System.out.println("处理孤儿单备份信息个数："+mLAOrphanPolicyBSet.size());
          mMap.put(this.preLAAscriptionSet, "UPDATE");
          mMap.put(this.mLAAscriptionSet, "INSERT");
          mMap.put(this.mLAOrphanPolicySet, "DELETE");
          mMap.put(this.mLAOrphanPolicyBSet, "INSERT");
          this.mInputData.add(mMap);
          return true;
    }
}
