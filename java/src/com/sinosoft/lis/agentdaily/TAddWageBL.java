package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.LAWageTempSchema;
import com.sinosoft.lis.vschema.LAWageTempSet;
import com.sinosoft.lis.schema.LAIndexInfoSchema;
import com.sinosoft.lis.vschema.LAIndexInfoSet;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LAWageTempDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.agentcalculate.CalIndex;
import com.sinosoft.lis.agentcalculate.AgCalBase;

/**
 * <p>Title: TAddWageBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author Elsa
 * @version 1.0
 */
public class TAddWageBL {
  //错误处理类
  public static CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private CalIndex mCalIndex = new CalIndex(); //指标计算类
  private MMap map = new MMap();
  public GlobalInput mGlobalInput = new GlobalInput();
  private LAWageTempSet mLAWageTempSet = new LAWageTempSet();
  private LAIndexInfoSet mLAIndexInfoSet = new LAIndexInfoSet();
  private LAAssessIndexSchema mLAAssessIndexSchema=new LAAssessIndexSchema();
  private LAAssessIndexSet mLAAssessIndexSet = new LAAssessIndexSet();
  private LAIndexVsCommSchema mLAIndexVsCommSchema=new LAIndexVsCommSchema();

  private String mAreaType = "";//个险的地区类型都是 “A”
  private String mManageCom = "";//管理机构
  private String mWageno = "";//薪资月
  private String mWagecode = "";//薪资项目

  public TAddWageBL() {

  }
  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin TAddWageBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    this.mErrors.clearErrors();
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    //进行业务处理
    if (!dealData()) {

      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start TAddWageBL Submit...");
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "TAddWageBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin TAddWageBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     System.out.println("GlobalInput get");
     this.mManageCom = (String)cInputData.get(1);
     this.mWageno =(String)cInputData.get(2);
     this.mWagecode =(String)cInputData.get(3);
     System.out.println("mManageCom get"+mManageCom+" wageno is "+mWageno+" wagecode is "+mWagecode);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "TAddWageBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("End TAddWageBL.getInputData.........");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin TAddWageBL.dealData........."+mOperate);

    if (!insertState()) {
               return false;
    }
    String tReturn = new String();
    try {
      	 //设置起止日期
       if (!setBeginEnd())
       {
           return false;
       }
    	 //查询需要循环的人
       String SQL = "select * from lawagetemp where branchtype='1'"
    	 +" and branchtype2='01' "
    	 +" and managecom like '"+mManageCom+"%'"
    	 + " and indexcalno ='"+this.mWageno+"' order by indexcalno,managecom,agentcode";
       LAWageTempDB tLAWageTempDB = new LAWageTempDB();
       this.mLAWageTempSet = null;
       this.mLAWageTempSet =  tLAWageTempDB.executeQuery(SQL);
       if(mLAWageTempSet.size()>0)
       {
       	for(int i = 1;i<=mLAWageTempSet.size();i++)
       	{
       		LAWageTempSchema tLAWageTempSchema = new LAWageTempSchema();
       		tLAWageTempSchema = mLAWageTempSet.get(i);
     	    String tAgentCode = tLAWageTempSchema.getAgentCode();
     	    String tAgentGrade = tLAWageTempSchema.getAgentGrade();
     	    String tAgentGroup = tLAWageTempSchema.getAgentGroup();
     	    String tIndexCalNo = tLAWageTempSchema.getIndexCalNo();
     	    String tBranchType = "1";
        	String tBranchType2 = "01";
        	LAIndexInfoDB  tLAIndexInfoDB=new LAIndexInfoDB();
        	LAIndexInfoSchema  tLAIndexInfoSchema=new LAIndexInfoSchema();
        	tLAIndexInfoDB.setIndexCalNo(tIndexCalNo);
        	tLAIndexInfoDB.setIndexType("00");
        	tLAIndexInfoDB.setAgentCode(tAgentCode);
        	if (!tLAIndexInfoDB.getInfo())
            {
                this.mErrors.copyAllErrors(tLAIndexInfoDB.mErrors);
                buildError("dealData",
                           "未查到人员为" + tAgentCode + "的基础指标信息");
                return false;
            }
          tLAIndexInfoSchema = tLAIndexInfoDB.getSchema();
          String tBranchAttr = tLAIndexInfoSchema.getBranchAttr();
          String tBranchSeries = tLAIndexInfoSchema.getBranchSeries();
 
          LAAgentDB  tLAAgentDB=new LAAgentDB();
          LAAgentSchema  tLAAgentSchema=new LAAgentSchema();
          tLAAgentDB.setAgentCode(tAgentCode);
          tLAAgentDB.getInfo();
          tLAAgentSchema=tLAAgentDB.getSchema();
          String tWageVersion=tLAAgentSchema.getWageVersion();          
          
          //按照人循环基础指标的计算
        	if(mWagecode.equals("0"))
        	{
        	LAIndexVsCommDB 	tLAIndexVsCommDB=new LAIndexVsCommDB();
        	LAIndexVsCommSet  tLAIndexVsCommSet=new LAIndexVsCommSet();
        	String agentgradeSql=" select * from laindexvscomm where agentgrade='"+tAgentGrade+"'"
        	                    +" and wageversion='"+tWageVersion +"' "
        	                    +" and wageorder>=30  and wageorder<>36   and wageorder<>37  "
        	                    +" order by wageorder " ;
        	tLAIndexVsCommSet = tLAIndexVsCommDB.executeQuery(agentgradeSql);
        	System.out.println(agentgradeSql);
        	if(tLAIndexVsCommSet.size()<=0)
          {
              this.mErrors.copyAllErrors(tLAIndexVsCommDB.mErrors);
              buildError("dealData","查询AgentGrade为" + tAgentGrade +"的佣金项指标失败");
              return false;
          }
           //开始逐项指标的计算
          for (int j = 1; j <= tLAIndexVsCommSet.size(); j++)
          {
                mCalIndex.mErrors.clearErrors();
                mLAIndexVsCommSchema = tLAIndexVsCommSet.get(j);
                String tIndexCode = mLAIndexVsCommSchema.getIndexCode();
                String WageCode = mLAIndexVsCommSchema.getWageCode();
                String tTableName = mLAIndexVsCommSchema.getCTableName();
                String tColName = mLAIndexVsCommSchema.getCColName();
                if (!getLAAssessIndexSchema(j))
                {
                    return false;
                }
                //指标计算

                mCalIndex.setAssessIndex(mLAAssessIndexSchema);
                mCalIndex.setAgentCode(tAgentCode);
                mCalIndex.setIndexCalNo(tIndexCalNo);
                mCalIndex.setOperator(mGlobalInput.Operator);
                mCalIndex.clearIndexSet();

                if (!setBaseValue(mCalIndex.tAgCalBase, tAgentCode, WageCode,
                                  tAgentGrade, tAgentGroup,tBranchAttr,tBranchSeries))
                {
                    return false;
                }
                //将参数设置好后，开始调用指标计算类开始计算
                tReturn = mCalIndex.Calculate();

                if (StrTool.cTrim(tReturn).equals(""))
                {
                    tReturn = "0";
                }
                double tValue=Double.parseDouble(tReturn);
                String iTableName=mLAAssessIndexSchema.getITableName();
                String iColName=mLAAssessIndexSchema.getIColName();
                String iIndexType=mLAAssessIndexSchema.getIndexType();
//                if (!updateLAIndexInfo(tIndexCalNo, tAgentCode, iTableName, iColName,iIndexType,tValue))
//                {
//                   System.out.println("插入佣金项的值时出错！！");
//                   return false;
//                }
                if (!updateLAWageTemp(tIndexCalNo, tAgentCode, tTableName, tColName,tValue))
                {
                   System.out.println("插入佣金项的值时出错！！");
                   return false;
                }
          }
        	}
        }
       }
      }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "TAddWageBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    if (!returnState())
     {
             return false;
       }

    return true;
  }

 /**
     * 往计算基数类中设置基本信息
     * @param cAgCalBase
     * @param cAgentCode
     * @param cWageCode
     * @param cAgentGrade
     * @return
     */
    private boolean setBaseValue(AgCalBase cAgCalBase, String cAgentCode,
                                 String cWageCode, String cAgentGrade,
                                 String cAgentGroup,String cBranchAttr,
                                 String cBranchSeries)
    {
        mCalIndex.tAgCalBase.setAgentCode(cAgentCode);
        mCalIndex.tAgCalBase.setWageNo(mWageno);
        mCalIndex.tAgCalBase.setWageCode(cWageCode);
        mCalIndex.tAgCalBase.setAgentGrade(cAgentGrade);
        mCalIndex.tAgCalBase.setManageCom(this.mManageCom);
        mCalIndex.tAgCalBase.setAgentGroup(cAgentGroup);
        mCalIndex.tAgCalBase.setAssessType("00");
        mCalIndex.tAgCalBase.setZSGroupBranchAttr(cBranchAttr);
        mCalIndex.tAgCalBase.setAreaType("A");
        mCalIndex.tAgCalBase.setZSGroupBranchSeries(cBranchSeries);
        mCalIndex.tAgCalBase .setBranchType("1") ;
        mCalIndex.tAgCalBase .setBranchType2("01");
        return true;
    }



/**
     * 设置起止时间标志（仅仅设置标志，而不是设置具体时间）
     * @return
     */
    private boolean setBeginEnd()
    {
        //从数据库中查出时间区间
        LAStatSegmentSchema tLAStatSegmentSchema;
        LAStatSegmentSet tLAStatSegmentSet;
        LAStatSegmentDB tLAStatSegmentDB = new LAStatSegmentDB();
        String tSQL = "select * from LAStatSegment where YearMonth = " +
                      this.mWageno+ " order by stattype";
        tLAStatSegmentSet = tLAStatSegmentDB.executeQuery(tSQL);
        if (tLAStatSegmentDB.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tLAStatSegmentDB.mErrors);
            CError tCError = new CError();
            tCError.moduleName = "TAddWageBL";
            tCError.functionName = "setBeginEnd()";
            tCError.errorMessage = "查询时间区间出错！";
            this.mErrors.addOneError(tCError);
            return false;
        }
        int tCount = tLAStatSegmentSet.size();
        System.out.println("tCount:" + tCount + "," + this.mWageno);
        if (tCount < 1)
        {
            CError tCError = new CError();
            tCError.moduleName = "TAddWageBL";
            tCError.functionName = "setBeginEnd()";
            tCError.errorMessage = "所输年月有问题，无法查到时间区间";
            this.mErrors.addOneError(tCError);
            return false;
        }
        String tStatType = "";
        for (int i = 1; i <= tCount; i++)
        {
            tLAStatSegmentSchema = tLAStatSegmentSet.get(i);
            tStatType = tLAStatSegmentSchema.getStatType().trim();

            if (tStatType.equals("1"))
            {
                mCalIndex.tAgCalBase.setMonthMark("1");
                mCalIndex.tAgCalBase.setMonthBegin(tLAStatSegmentSchema.getStartDate());
                mCalIndex.tAgCalBase.setMonthEnd(tLAStatSegmentSchema.getEndDate());
                mCalIndex.tAgCalBase.setQuauterBegin(tLAStatSegmentSchema.getStartDate());
                mCalIndex.tAgCalBase.setQuauterEnd(tLAStatSegmentSchema.getEndDate());
                mCalIndex.tAgCalBase.setHalfYearBegin(tLAStatSegmentSchema.getStartDate());
                mCalIndex.tAgCalBase.setHalfYearEnd(tLAStatSegmentSchema.getEndDate());
                mCalIndex.tAgCalBase.setYearBegin(tLAStatSegmentSchema.getStartDate());
                mCalIndex.tAgCalBase.setYearEnd(tLAStatSegmentSchema.getEndDate());
                continue;
            }
            if (tStatType.equals("2"))
            {
                mCalIndex.tAgCalBase.setQuauterMark("1");
                mCalIndex.tAgCalBase.setQuauterBegin(tLAStatSegmentSchema.getStartDate());
                mCalIndex.tAgCalBase.setQuauterEnd(tLAStatSegmentSchema.getEndDate());
                mCalIndex.tAgCalBase.setHalfYearBegin(tLAStatSegmentSchema.getStartDate());
                mCalIndex.tAgCalBase.setHalfYearEnd(tLAStatSegmentSchema.getEndDate());
                mCalIndex.tAgCalBase.setYearBegin(tLAStatSegmentSchema.getStartDate());
                mCalIndex.tAgCalBase.setYearEnd(tLAStatSegmentSchema.getEndDate());
                continue;
            }
            if (tStatType.equals("3"))
            {
                mCalIndex.tAgCalBase.setHalfYearMark("1");
                mCalIndex.tAgCalBase.setHalfYearBegin(tLAStatSegmentSchema.getStartDate());
                mCalIndex.tAgCalBase.setHalfYearEnd(tLAStatSegmentSchema.getEndDate());
                mCalIndex.tAgCalBase.setYearBegin(tLAStatSegmentSchema.getStartDate());
                mCalIndex.tAgCalBase.setYearEnd(tLAStatSegmentSchema.getEndDate());
                continue;
            }
            if (tStatType.equals("4"))
            {
                mCalIndex.tAgCalBase.setYearMark("1");
                mCalIndex.tAgCalBase.setYearBegin(tLAStatSegmentSchema.getStartDate());
                mCalIndex.tAgCalBase.setYearEnd(tLAStatSegmentSchema.getEndDate());
                continue;
            }
        }
        mCalIndex.tAgCalBase.setTempBegin(mCalIndex.tAgCalBase.getMonthBegin());
        mCalIndex.tAgCalBase.setTempEnd(mCalIndex.tAgCalBase.getMonthEnd());
        return true;
    }


  /**
     * 根据佣金计算指标记录中的指标编码从LAIndexCode取出该编码对应的指标记录
     * @return
     */
    private boolean getLAAssessIndexSchema(int i)
    {
        if (mLAAssessIndexSet != null && mLAAssessIndexSet.size() > 0)
        {
            mLAAssessIndexSchema = mLAAssessIndexSet.get(i);
        }
        else
        {
            String tIndexCode = mLAIndexVsCommSchema.getIndexCode();
            LAAssessIndexDB tLAAssessIndexDB = new LAAssessIndexDB();
            tLAAssessIndexDB.setIndexCode(tIndexCode);
            if (!tLAAssessIndexDB.getInfo())
            {
                this.mErrors.copyAllErrors(tLAAssessIndexDB.mErrors);
                buildError("getLAAssessINdexSchema",
                           "未查到指标编码为" + tIndexCode + "的指标信息");
                return false;
            }
            mLAAssessIndexSchema = tLAAssessIndexDB.getSchema();
        }
        return true;
    }

/**
   *
   * @param cAgentCode
   * @param cWageNo
   * @param W10
   * @return
   */
  private boolean updateLAIndexInfo(String cIndexCalNo,String cAgentCode,String cTableName,String cColName,String cIndexType,double value)
  {

	  String updateSQL =" update LAIndexInfo set "+cColName+" = "+value+""
	                   +" where agentcode='"+cAgentCode+"' and "
	                   +" indexcalno= '"+cIndexCalNo+"' and indextype='"+cIndexType+"' and branchtype='1' and branchtype2='01'";
	  System.out.println(updateSQL	);
	  this.map.put(updateSQL, "UPDATE");
	  return true;
  }

  /**
    * 构造错误信息
    * @param szFunc
    * @param szErrMsg
    */
   private void buildError(String szFunc, String szErrMsg)
   {
       CError cError = new CError();
       cError.moduleName = "CalWageBaseBL";
       cError.functionName = szFunc;
       cError.errorMessage = szErrMsg;
       this.mErrors.addOneError(cError);
   }

  /**
   *
   * @param cAgentCode
   * @param cWageNo
   * @param W10
   * @return
   */
  private boolean updateLAWageTemp(String cIndexCalNo,String cAgentCode,String cTableName,String cColName,double value)
  {

	  String updateSQL =" update LAWageTemp set "+cColName+" = "+value+" "
                           +",modifydate='"+CurrentDate+"',modifytime='"+CurrentTime+"'"
	                   +" where agentcode='"+cAgentCode+"' and "
	                   +" indexcalno= '"+cIndexCalNo+"' and branchtype='1' and branchtype2='01'";
	  System.out.println(updateSQL	);
	  this.map.put(updateSQL, "UPDATE");
	  return true;
  }


  // 开始试算，插入state = '12'，表示正在试算，其他人不能再算
     private boolean insertState()
     {
         String tSQL1 = "update LAWageHistory set state = '12' where "
                        + " wageno = '" + mWageno +
                        "' and ManageCom like '" + mManageCom
                        + "%' and BranchType = '1' and BranchType2 ='01' and AClass = '03'";
         System.out.println("最后更新LAWageHistory表中state记录的SQL: " + tSQL1);
         ExeSQL tExeSQL = new ExeSQL();
         boolean tValue = tExeSQL.execUpdateSQL(tSQL1);
         if (tExeSQL.mErrors.needDealError()) {
             // @@错误处理
             this.mErrors.copyAllErrors(tExeSQL.mErrors);
             CError tError = new CError();
             tError.moduleName = "AgentWageGatherBL";
             tError.functionName = "insertState";
             tError.errorMessage = "执行SQL语句,更新LAWageHistory表记录失败!";
             this.mErrors.addOneError(tError);
             System.out.println("执行SQL语句,更新LAWageHistory表表中state记录失败!");
             return false;
         }
         return true;
     }
     // 试算结束，回到原来的状态，插入state = '11'，表示可以计算
     private boolean returnState()
     {
         String tSQL1 = "update LAWageHistory set state = '11' where "
                        + " wageno = '" + mWageno +
                        "' and ManageCom like '" + mManageCom
                        + "%' and BranchType = '1' and BranchType2 ='01' and AClass = '03'";
         System.out.println("最后更新LAWageHistory表中state记录的SQL: " + tSQL1);
         System.out.println(tSQL1);
         this.map.put(tSQL1, "UPDATE");

         return true;
    }


  /**
   *
   * @param cAgentCode
   * @param cWageNo
   * @param W10
   * @return
   */
  private boolean updateLAWage(String cAgentCode,String cWageNo,double cW10)
  {

	  String updateSQL = "update lawage set W10= "+cW10+" where agentcode='"+cAgentCode+"' and " +
	  		" indexcalno= '"+cWageNo+"' and branchtype='2' and branchtype2='01'";
	  System.out.println(updateSQL	);
	  this.map.put(updateSQL, "UPDATE");
	  return true;
  }
  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      System.out.println(
          "Begin LABankRateCommSetBL.prepareOutputData.........");
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LABankRateCommSetBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
}
