package com.sinosoft.lis.agentdaily;

import java.sql.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LAInspiritSchema;
import com.sinosoft.lis.vschema.LAInspiritSet;
import com.sinosoft.lis.db.LAInspiritDB;
import com.sinosoft.utility.*;

/**
 * <p>Title: 人员考核指标计算流程类</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class LAInspiritBL
{
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap mMap=new MMap();
    private VData mOutputDate=new VData();
    private LAInspiritSchema mLAInspiritSchema = new LAInspiritSchema();
    private LAInspiritSet mLAInspiritSet = new LAInspiritSet();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit=new PubSubmit();
        tPubSubmit.submitData(mOutputDate,"");
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError())
        {
            buildError("submitData","向数据库提交保存时出错！");
            return false;
        }

        return true;
    }

    /**
     * 进行后台验证
     * @param pmCheckType String  验证类型  0：新业务激励验证 1：修改已有业务激励验证
     * @return boolean
     */
    private boolean check(String pmCheckType)
    {
        LAInspiritDB tLAInspiritDB = new LAInspiritDB();

        if("0".equals(pmCheckType))
        {// 保存新业务激励信息前的验证

        }else if("1".equals(pmCheckType))
        {// 修改已有业务激励信息前的验证
            tLAInspiritDB.setSeqNo(this.mLAInspiritSchema.getSeqNo());
            if(!tLAInspiritDB.getInfo())
            {
                buildError("check","查询要修改的数据失败！");
                return false;
            }
        }

        return true;
    }

    /**
     * 进行业务处理
     * @return boolean
     */
    private boolean dealData()
    {
        // 判断操作符分别进行不同操作
        if("INSERT||MAIN".equals(this.mOperate))
        {
            // 保存新业务激励信息
            if(!dealInsertInspirit())
                return false;
        }else if("UPDATE||MAIN".equals(this.mOperate))
        {
            // 修改已有业务激励信息
            if(!dealUpdataInspirit())
                return false;
        }

        return true;
    }

    /**
     * 修改已有业务激励信息
     * @return boolean
     */
    private boolean dealUpdataInspirit()
    {
        LAInspiritSchema tLAInspiritSchema = new LAInspiritSchema();
        LAInspiritDB tLAInspiritDB = new LAInspiritDB();
        tLAInspiritDB.setSeqNo(this.mLAInspiritSchema.getSeqNo());
        if(!tLAInspiritDB.getInfo())
        {
            buildError("dealUpdataInspirit","检索指定数据失败！");
            return false;
        }

        // 进行新业务激励录入验证
        if (!check("1"))
        {
            return false;
        }

        tLAInspiritSchema = this.mLAInspiritSchema;
        tLAInspiritSchema.setOperator(this.mGlobalInput.Operator);
        tLAInspiritSchema.setMakeDate(tLAInspiritDB.getMakeDate());
        tLAInspiritSchema.setMakeTime(tLAInspiritDB.getMakeTime());
        tLAInspiritSchema.setModifyDate(this.currentDate);
        tLAInspiritSchema.setModifyTime(this.currentTime);

        // 保存提往后台的数据
        mLAInspiritSet.add(tLAInspiritSchema);

        return true;
    }

    /**
     * 保存新业务激励信息
     * @return boolean
     */
    private boolean dealInsertInspirit()
    {
        LAInspiritSchema tLAInspiritSchema = new LAInspiritSchema();
        // 进行新业务激励录入验证
        if(!check("0"))
        {
            return false;
        }

        // 准备将要写入后台的数据
        tLAInspiritSchema.setSchema(this.mLAInspiritSchema);
        tLAInspiritSchema.setSeqNo(PubFun1.CreateMaxNo("Inspirit", 20));
        tLAInspiritSchema.setOperator(this.mGlobalInput.Operator);
        tLAInspiritSchema.setMakeDate(this.currentDate);
        tLAInspiritSchema.setMakeTime(this.currentTime);
        tLAInspiritSchema.setModifyDate(this.currentDate);
        tLAInspiritSchema.setModifyTime(this.currentTime);

        // 保存提往后台的数据
        mLAInspiritSet.add(tLAInspiritSchema);

        return true;
    }

    /**
     * 准备后台数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        String tOperate = "";

        // 根据不同的处理要求  设置操作符
        if("INSERT||MAIN".equals(this.mOperate))
        {
            tOperate = "INSERT";
        }else if("UPDATE||MAIN".equals(this.mOperate))
        {
            tOperate = "UPDATE";
        }

        for(int i=1;i<=this.mLAInspiritSet.size();i++)
        {
            System.out.print(mLAInspiritSet.get(i).getSeqNo());
            System.out.println();
        }

        try{
            mMap.put(this.mLAInspiritSet, tOperate);
            this.mOutputDate.add(mMap);
        }catch (Exception ex)
        {
            buildError("prepareOutputData","准备提交后台的数据时出错！");
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("Begin LAInspiritBL.getInputData.........");
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        this.mLAInspiritSchema.setSchema((LAInspiritSchema) cInputData.
                getObjectByObjectName("LAInspiritSchema",0));

        if (mGlobalInput == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAInspiritBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 执行SQL文查询结果
     * @param sql String
     * @return int
     */
    private int execQuery(String sql)
    {
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();

        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (conn == null)return 0;
            st = conn.prepareStatement(sql);
            if (st == null)return 0;
            rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        } finally {
            try {
                st.close();
                rs.close();
                conn.close();
                st = null;
                rs = null;
                conn = null;
            } catch (Exception e) {}
        }
    }

    /**
     * 追加错误信息
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LAInspiritBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        System.out.println(szFunc + "--" + szErrMsg);
        this.mErrors.addOneError(cError);
    }
}
