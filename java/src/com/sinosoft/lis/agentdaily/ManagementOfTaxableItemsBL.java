/*
 * <p>ClassName: ManagementOfTaxableItemsBL </p>
 * <p>Description: ManagementOfTaxableItemsBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2016-04-07
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.db.LATaxAdjustDB;
import com.sinosoft.lis.db.LAWageHistoryDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LATaxAdjustSchema;
import com.sinosoft.lis.schema.LAWageHistorySchema;
import com.sinosoft.lis.vschema.LATaxAdjustSet;
import com.sinosoft.lis.vschema.LAWageHistorySet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ManagementOfTaxableItemsBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private MMap mMap=new MMap();
    /** 业务处理相关变量 */
    private LATaxAdjustSchema mLATaxAdjustSchema = new
    		LATaxAdjustSchema();
    private LATaxAdjustSet mLATaxAdjustSet = new LATaxAdjustSet();
    private LATaxAdjustSet minLATaxAdjustSet = new LATaxAdjustSet();
    private LATaxAdjustSet mupLATaxAdjustSet = new LATaxAdjustSet();
    private LATaxAdjustSet mdeLATaxAdjustSet = new LATaxAdjustSet();
    public ManagementOfTaxableItemsBL()
    {
    }

    public static void main(String[] args)
    {

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
    	 System.out.println("ssssssssssssssssss");
    	//将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //循环处理每一条数据
        for(int i=1;i<=mLATaxAdjustSet.size();i++)
        {
            mLATaxAdjustSchema = new LATaxAdjustSchema();
            mLATaxAdjustSchema = mLATaxAdjustSet.get(i);
            if (!check()) {
                return false;
            }
            //进行业务处理
            if (!dealData()) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "ManagementOfTaxableItemsBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据处理失败ManagementOfTaxableItemsBL-->dealData!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start ManagementOfTaxableItemsBL Submit...");
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "ManagementOfTaxableItemsBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean check() 
    {                      
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = true;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
        if (this.mOperate.equals("INSERT"))
        {
        	//薪资状态不能为13,14
        	LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();
        	LAWageHistorySet tLAWageHistorySet = new LAWageHistorySet();
        	
        	String sqls=" select * from LAWageHistory where 1=1 and branchtype ='"+this.mLATaxAdjustSchema.getBranchType()+"'" +
        			" and branchtype2 = '"+this.mLATaxAdjustSchema.getBranchType2()+"' and ManageCom='"+this.mLATaxAdjustSchema.getManageCom()+"'  and WageNo= '"+this.mLATaxAdjustSchema.getWageNo()+"' ";
        	tLAWageHistorySet =tLAWageHistoryDB.executeQuery(sqls);
        	for(int i = 1;i<=tLAWageHistorySet.size();i++)
        	{
        		LAWageHistorySchema tLAWageHistorySchema = new LAWageHistorySchema();
        		tLAWageHistorySchema =tLAWageHistorySet.get(i).getSchema();       		
  
	        	if((tLAWageHistorySchema.getState()).equals("13")||(tLAWageHistorySchema.getState()).equals("14")){
	        		CError tError = new CError();
	                tError.moduleName = "ManagementOfTaxableItemsBL";
	                tError.functionName = "dealData";
	                tError.errorMessage = "该业务员所属机构的薪资月已经薪资确认或者薪资审核发放，不能录入!";
	                this.mErrors.addOneError(tError);
	                return false;
	        	}
        	}
        	
        	
        	//业务员信息不能重复
        	LATaxAdjustDB tLATaxAdjustDB = new LATaxAdjustDB();
        	LATaxAdjustSet tLATaxAdjustSet = new LATaxAdjustSet();
        	String sql = "select * from LATaxAdjust where 1=1 and branchtype ='"+this.mLATaxAdjustSchema.getBranchType()+"'" +
        			" and branchtype2 = '"+this.mLATaxAdjustSchema.getBranchType2()+"' and ManageCom='"+this.mLATaxAdjustSchema.getManageCom()+"' and agentcode= '"+this.mLATaxAdjustSchema.getAgentCode()+"' and WageNo= '"+this.mLATaxAdjustSchema.getWageNo()+"' ";
        	tLATaxAdjustSet =tLATaxAdjustDB.executeQuery(sql);        	       	
        	
        	for(int i = 1;i<=tLATaxAdjustSet.size();i++)
        	{
        		LATaxAdjustSchema tLATaxAdjustSchema = new LATaxAdjustSchema();
        		tLATaxAdjustSchema =tLATaxAdjustSet.get(i).getSchema(); 
  
	        	if((tLATaxAdjustSchema.getAgentCode()).equals(this.mLATaxAdjustSchema.getAgentCode())
	        			&&(tLATaxAdjustSchema.getWageNo()).equals(this.mLATaxAdjustSchema.getWageNo())
	        			&&(tLATaxAdjustSchema.getManageCom()).equals(this.mLATaxAdjustSchema.getManageCom())
	        			&&(tLATaxAdjustSchema.getBranchType()).equals(this.mLATaxAdjustSchema.getBranchType())
	        			&&(tLATaxAdjustSchema.getBranchType2()).equals(this.mLATaxAdjustSchema.getBranchType2())
	        			){
	        		CError tError = new CError();
	                tError.moduleName = "ManagementOfTaxableItemsBL";
	                tError.functionName = "dealData";
	                tError.errorMessage = "该业务员信息已存在，不能重复录入!";
	                this.mErrors.addOneError(tError);
	                return false;
	        	}
        	}        	
        	this.mLATaxAdjustSchema.setOperator(this.mGlobalInput.Operator);
            this.mLATaxAdjustSchema.setMakeDate(currentDate);
            this.mLATaxAdjustSchema.setMakeTime(currentTime);
            this.mLATaxAdjustSchema.setModifyDate(currentDate);
            this.mLATaxAdjustSchema.setModifyTime(currentTime);
            minLATaxAdjustSet.add(mLATaxAdjustSchema);
        }
        else if (this.mOperate.equals("UPDATE"))
        {
        	//薪资状态不能为13,14
        	LAWageHistoryDB tLAWageHistoryDB = new LAWageHistoryDB();
        	LAWageHistorySet tLAWageHistorySet = new LAWageHistorySet();
        	
        	String sqls=" select * from LAWageHistory where 1=1 and branchtype ='"+this.mLATaxAdjustSchema.getBranchType()+"'" +
        			" and branchtype2 = '"+this.mLATaxAdjustSchema.getBranchType2()+"' and ManageCom='"+this.mLATaxAdjustSchema.getManageCom()+"'  and WageNo= '"+this.mLATaxAdjustSchema.getWageNo()+"' ";
        	tLAWageHistorySet =tLAWageHistoryDB.executeQuery(sqls);
        	for(int i = 1;i<=tLAWageHistorySet.size();i++)
        	{
        		LAWageHistorySchema tLAWageHistorySchema = new LAWageHistorySchema();
        		tLAWageHistorySchema =tLAWageHistorySet.get(i).getSchema();       		
  
	        	if((tLAWageHistorySchema.getState()).equals("13")||(tLAWageHistorySchema.getState()).equals("14")){
	        		CError tError = new CError();
	                tError.moduleName = "ManagementOfTaxableItemsBL";
	                tError.functionName = "dealData";
	                tError.errorMessage = "该业务员所属机构的薪资月已经薪资确认或者薪资审核发放，不能修改!";
	                this.mErrors.addOneError(tError);
	                return false;
	        	}
        	}
        	
        	
        	LATaxAdjustDB tLATaxAdjustDB = new LATaxAdjustDB();
        	LATaxAdjustSet tLATaxAdjustSet = new LATaxAdjustSet();
            // 信息修改
        	String sql = "select * from LATaxAdjust where 1=1 and branchtype ='"+this.mLATaxAdjustSchema.getBranchType()+"'" +
        			" and branchtype2 = '"+this.mLATaxAdjustSchema.getBranchType2()+"' and ManageCom='"+this.mLATaxAdjustSchema.getManageCom()+"' and agentcode= '"+this.mLATaxAdjustSchema.getAgentCode()+"' and WageNo= '"+this.mLATaxAdjustSchema.getWageNo()+"' ";
        	tLATaxAdjustSet =tLATaxAdjustDB.executeQuery(sql);
        	for(int i = 1;i<=tLATaxAdjustSet.size();i++)
        	{
        		LATaxAdjustSchema tLATaxAdjustSchema = new LATaxAdjustSchema();
        		tLATaxAdjustSchema =tLATaxAdjustSet.get(i).getSchema();       		
        		
        		tLATaxAdjustSchema.setAgentCode(this.mLATaxAdjustSchema.getAgentCode());
        		tLATaxAdjustSchema.setCBPTaxes(this.mLATaxAdjustSchema.getCBPTaxes());
        		tLATaxAdjustSchema.setPCBPTaxes(this.mLATaxAdjustSchema.getPCBPTaxes());
        		tLATaxAdjustSchema.setITBPTaxes(this.mLATaxAdjustSchema.getITBPTaxes());
        		tLATaxAdjustSchema.setWageNo(this.mLATaxAdjustSchema.getWageNo());
        		tLATaxAdjustSchema.setOperator(this.mGlobalInput.Operator); 
                tLATaxAdjustSchema.setModifyDate(currentDate);
                tLATaxAdjustSchema.setModifyTime(currentTime);
                
        		this.mupLATaxAdjustSet.add(tLATaxAdjustSchema);
        			
        	}
            System.out.println("222222222222222222222222");
        }
        else if (this.mOperate.equals("DELETE"))
        {        	
            mdeLATaxAdjustSet.add(mLATaxAdjustSchema);
        }
        tReturn = true;
        return tReturn;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLATaxAdjustSet.set((LATaxAdjustSet) cInputData.
                                             getObjectByObjectName(
                "LATaxAdjustSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            mMap.put(this.minLATaxAdjustSet, "INSERT");
            mMap.put(this.mupLATaxAdjustSet, "UPDATE");
            mMap.put(this.mdeLATaxAdjustSet, "DELETE");
            this.mInputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ManagementOfTaxableItemsBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    public VData getResult()
    {
        return this.mResult;
    }
}
