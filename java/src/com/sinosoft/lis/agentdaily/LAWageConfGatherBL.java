/*
 * <p>ClassName: LAWageBL </p>
 * <p>Description: LAWageBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-27
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAWageDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.PubFun1;

public class LAWageConfGatherBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /*时间变量*/
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LAWageSchema mLAWageSchema = new LAWageSchema();
    private LAWageSet mLAWageSet = new LAWageSet();
    private String  mNewEdorNo="";
    private LARollBackTraceSet mLARollBackTraceSet=new LARollBackTraceSet();
    private MMap mMap=new MMap();

    public LAWageConfGatherBL()
    {
    }
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if(!check())
        {
        	return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageConfGatherBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAWageConfGatherBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
            System.out.println("Start LAWageConfGatherBL Submit...");
            System.out.println("End LAWageConfGatherBL Submit...");
            //如果有需要处理的错误，则返回
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("Start LAWageConfGatherBL Submit...");
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAWageConfGatherBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
        }
        mInputData = null;
        return true;
    }

    /**************************************************************\
     * @author Administrator
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     *************************************************************/
    private boolean dealData()
    {
        mNewEdorNo = PubFun1.CreateMaxNo("EDORNO", 20);
            String sql="update lawage set state='0',operator='"+mGlobalInput.Operator+
                        "',modifydate='"+currentDate+"',modifytime='"+currentTime+
                        "' where state='1' and indexcalno='"+mLAWageSchema.getIndexCalNo() +
                         "' and managecom like '" + mLAWageSchema.getManageCom() +
                         "%' and managecom like '" + mGlobalInput.ManageCom + "%' and BranchType='" +
                         mLAWageSchema.getBranchType() + "' and BranchType2='" + mLAWageSchema.getBranchType2() +
                         "'";
            ExeSQL tExeSQL = new ExeSQL();
            if (!tExeSQL.execUpdateSQL(sql)) {
                this.mErrors.copyAllErrors(tExeSQL.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentWageCalDoBL";
                tError.functionName = "dealData";
                tError.errorMessage = "薪筹审核回退失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
           dealRollBackTrace(mLAWageSchema,0);
           dealwagehistory();// 修改lawagehistory 表state 状态 置为13 -- add fanting
            return true;
    }

    /************************************************************
     * 从输入数据中得到所有对象
     * @author Administrato
     * @ 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     ************************************************************/
    private boolean getInputData(VData cInputData)
    {
        this.mLAWageSchema.setSchema((LAWageSchema) cInputData.
                                     getObjectByObjectName("LAWageSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
        return true;
    }
    /************************************************************
     * 注:准备后台操作数据
     * @author Administrator
     * @param receipt 
     *************************************************************/
    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mMap.put(this.mLAWageSet,"UPDATE");
            this.mMap.put(this.mLARollBackTraceSet,"INSERT");
            this.mInputData.add(mMap);

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAWageConfGatherBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /************************************************************
     * 注:进行回退记录处理
     * @author Administrator
     * @return
     *************************************************************/
    private void dealRollBackTrace(LAWageSchema tLAWageSchema,int tLength)
    {
        //条件划分符号
        String tSign = "&";
        //回退序号
        String tIdx = PubFun1.CreateMaxNo("IDX", 20);
        //回退条件
        String tTrem = "薪资审核发放回退年月:" + tLAWageSchema.getIndexCalNo();
        tTrem  += tSign + "管理机构:" + tLAWageSchema.getManageCom();
        tTrem  += tSign + "展业机构:" + tLAWageSchema.getBranchType();
        tTrem  += tSign + "展业渠道:" + tLAWageSchema.getBranchType2();
        if(tLength!=0)
        {
            tTrem += tSign + "回退人数:" + tLength;
        }
        else
        {
            if (mLAWageSchema.getAgentGroup() != null && !mLAWageSchema.getAgentGroup().equals(""))
            {
                tTrem += tSign + "销售单位:" + tLAWageSchema.getAgentGroup();
            }
            if (mLAWageSchema.getAgentCode() != null && !mLAWageSchema.getAgentCode().trim().equals(""))
            {
                tTrem += tSign + "人员代码:" + tLAWageSchema.getAgentCode();
            }
            else
            {
                tTrem += tSign + "回退人数: 全部";
            }
        }
        LARollBackTraceSchema tLARollBackTraceSchema = new LARollBackTraceSchema();
        tLARollBackTraceSchema.setIdx(tIdx);
        tLARollBackTraceSchema.setEdorNo(mNewEdorNo);
        tLARollBackTraceSchema.setoperator(mGlobalInput.Operator);
        tLARollBackTraceSchema.setstate("0");
        tLARollBackTraceSchema.setConditions(tTrem);
        tLARollBackTraceSchema.setMakeDate(PubFun.getCurrentDate());
        tLARollBackTraceSchema.setMakeTime(PubFun.getCurrentTime());
        tLARollBackTraceSchema.setRollBackType("09");
        //缓存回退记录
        mLARollBackTraceSet.add(tLARollBackTraceSchema);
    }

    public VData getResult()
    {
        return this.mResult;
    }

/************************************************************
 *  注: 增加程序校验,系统只支持逐月进行薪资审核发放回退
 *  @author Administrator
 *  @return 
 *************************************************************/
    public boolean check()
    {
    	String check_sql = "select max(indexcalno) from lawage where branchtype = '2' " +
    			"and branchtype2 ='01' and managecom like '"+mLAWageSchema.getManageCom()+"%'" +
    					"and indexcalno >'"+mLAWageSchema.getIndexCalNo()+"' ";
    	 ExeSQL tExeSQL = new ExeSQL();
    	 String tMax = tExeSQL.getOneValue(check_sql);
    	 if(tMax!=null&&!tMax.equals(""))
    	 {
    		   CError tError = new CError();
               tError.moduleName = "LAWageConfGatherBL";
               tError.functionName = "checkname";
               tError.errorMessage = "分公司编码为" + mLAWageSchema.getManageCom()+
                                     "最大确认薪资月为"+tMax+",烦请逐月进行回退！";
               this.mErrors.addOneError(tError);
             return false;
    	 }
    	 //add 2012-09-05
    	String check_sql2 = "select '1' from lawagehistory where branchtype ='2' and branchtype2= '01' and " 
    			+" managecom like '"+mLAWageSchema.getManageCom()+"%' and wageno = '"+mLAWageSchema.getIndexCalNo()+"'" 
    			+" and state='14'";
    	String tCheck = tExeSQL.getOneValue(check_sql2);
   	    if(tCheck==null||tCheck.equals(""))
	    {
		   CError tError = new CError();
           tError.moduleName = "LAWageConfGatherBL";
           tError.functionName = "checkname";
           tError.errorMessage = "分公司编码为" + mLAWageSchema.getManageCom()+
                                 "薪资月为"+tMax+"薪资状态不是审核发放,请核实！";
           this.mErrors.addOneError(tError);
           return false;
	   }
    	return true;
    }
    
 /************************************************************
  *  注: 增加程序校验,系统只支持逐月进行薪资审核发放回退
  *      @author Administrator  
  *      @return
 *************************************************************/
    public void dealwagehistory()
    {
         String dealwagehistory = "update lawagehistory set state = '13',modifydate = current date," +
         		"modifytime = current time where branchtype = '2' " +
         		"and branchtype2 = '01' and managecom like '"+mLAWageSchema.getManageCom()+"%' " +
         	    "and wageno = '"+mLAWageSchema.getIndexCalNo()+"'";
         this.mMap.put(dealwagehistory, "UPDATE");
    }
}
