/*
 * <p>ClassName: LAAMAddSubPerUI </p>
 * <p>Description: LAAMAddSubPerUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 个险加扣款
 * @CreateDate：2006-07-13
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
public class LAAMAddSubPerUI
{

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    private String mOperate;


    public LAAMAddSubPerUI()
    {
    }

    public boolean submitData(VData cInputData,String cOperate)
    {
        mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        LAAMAddSubPerBL bl = new LAAMAddSubPerBL();
        if (!bl.submitData(mInputData,mOperate))
        {
            this.mErrors.copyAllErrors(bl.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAMAddSubPerUI";
            tError.functionName = "submitData";
            tError.errorMessage = bl.mErrors.getFirstError();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
