/*
 * <p>ClassName: OLAArchieveUI </p>
 * <p>Description: OLAArchieveUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:05:58
 */
package com.sinosoft.lis.agentdaily;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.pubfun.*;
public class LAZTContFYCRateUI {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData =new VData();
private LAContFYCRateSet mLAContFYCRateSet=new LAContFYCRateSet();

private GlobalInput mGlobalInput=new GlobalInput();
/** 数据操作字符串 */
private String mOperate;
private String mTest;
//业务处理相关变量
 /** 全局数据 */
private LAZTContFYCRateBL tLAZTContFYCRateBL=new LAZTContFYCRateBL();
public LAZTContFYCRateUI ()
{
}
 /**
传输数据的公共方法
*/
public boolean submitData(VData cInputData,String cOperate)
{
  //将操作数据拷贝到本类中




	tLAZTContFYCRateBL.submitData(cInputData,cOperate);
  System.out.println("End LAContFYCRate UI Submit...");
  //如果有需要处理的错误，则返回
  if (tLAZTContFYCRateBL.mErrors .needDealError() )
  {
     // @@错误处理
     this.mErrors.copyAllErrors(tLAZTContFYCRateBL.mErrors);
     CError tError = new CError();
     tError.moduleName = "LAContFYCRateUI";
     tError.functionName = "submitData";
     tError.errorMessage = "数据提交失败!";
     this.mErrors .addOneError(tError) ;
     return false;
  }
  return true;
  }
  public static void main(String[] args)
  {
     LAContFYCRateSchema tLAContFYCRateSchema   = new LAContFYCRateSchema();
     LAContFYCRateSet tLAContFYCRateSet = new LAContFYCRateSet();
     tLAContFYCRateSchema = new LAContFYCRateSchema();
     tLAContFYCRateSchema.setGrpContNo("1400000265");
     tLAContFYCRateSchema.setRiskCode("1601");
     tLAContFYCRateSchema.setFlag("N");
     tLAContFYCRateSchema.setRate(0.2);
     tLAContFYCRateSet.add(tLAContFYCRateSchema);
     VData tVData=new VData();
     tVData.add(tLAContFYCRateSet) ;
     tVData.add(new GlobalInput());
     LAContFYCRateUI tUI=new LAContFYCRateUI();
     tUI.submitData(tVData,"INSERT||MAIN" );
  }
  /**
  * 准备往后层输出所需要的数据
  * 输出：如果准备数据时发生错误则返回false,否则返回true
  */
/**
 * 根据前面的输入数据，进行UI逻辑处理
  * 如果在处理过程中出错，则返回false,否则返回true
  */

public VData getResult()
{
  return this.mResult;
}
}
