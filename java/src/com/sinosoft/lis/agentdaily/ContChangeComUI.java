package com.sinosoft.lis.agentdaily;
 /*
 * <p>ClassName: ChangeBranchUI </p>
 * <p>Description: ChangeBranchUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-27
 */

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LAComToAgentSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;
public class ContChangeComUI {
    public ContChangeComUI() {
    }

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors(); //错误处理类
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    LCContSchema mLCContSchema = new LCContSchema();
    LCContSchema mNewLCContSchema = new LCContSchema();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    ContChangeComBL tContChangeComBL = new ContChangeComBL();

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("Begin ContChangeComUI.submitData.........");
               //将操作数据拷贝到本类中
               this.mOperate = cOperate;
               //得到外部传入的数据,将数据备份到本类中
               if (!getInputData(cInputData))
               {
                       return false;
               }
               //进行业务处理
               if (!dealData())
               {
                       return false;
               }
               //准备往后台的数据
               if (!prepareOutputData())
               {
                       return false;
               }
               System.out.println("Start ContChangeComUI Submit...");
               try
               {
                 tContChangeComBL.submitData(mInputData, mOperate);
               }
               catch(Exception ex){
                 ex.toString();
               }
               System.out.println("End ContChangeComUI Submit...");
               //如果有需要处理的错误，则返回
               if (tContChangeComBL.mErrors.needDealError())
               {
                       // @@错误处理
                       this.mErrors.copyAllErrors(tContChangeComBL.mErrors);
                       CError tError = new CError();
                       tError.moduleName = "LAGroupStandPremUI";
                       tError.functionName = "submitData";
                       tError.errorMessage = "数据提交失败!";
                       this.mErrors.addOneError(tError);
                       return false;
               }
               mInputData = null;
               return true;

    }



    /**
        * 从输入数据中得到所有对象
        *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
        */
       private boolean getInputData(VData cInputData)
       {
               //全局变量
               try
                {
                   System.out.println("Begin ContChangeComUI.getInputData.........");
                        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                                "GlobalInput", 0));
                       //全局变量
                       //this.mLAWageRadixSet[0].set((LAWageRadixSet) cInputData.getObjectByObjectName("LAWageRadixSet", 1));
             this.mLCContSchema.setSchema((LCContSchema) cInputData.getObjectByObjectName(
                                "LCContSchema", 1));
             this.mNewLCContSchema.setSchema((LCContSchema) cInputData.getObjectByObjectName(
                                "LCContSchema", 2));

                 }
                 catch(Exception ex)
                 {
                         // @@错误处理
                         CError tError = new CError();
                         tError.moduleName = "ContChangeComUI";
                         tError.functionName = "getInputData";
                         tError.errorMessage = "在读取处理所需要的数据时出错。";
                         this.mErrors.addOneError(tError);
                         return false;
                 }
                 if (mGlobalInput == null)
          {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "ContChangeComUI";
              tError.functionName = "getInputData";
              tError.errorMessage = "没有得到足够的信息！";
              this.mErrors.addOneError(tError);
              return false;
          }

               return true;
       }

       /**
        * 根据前面的输入数据，进行UI逻辑处理
        * 如果在处理过程中出错，则返回false,否则返回true
        */
       private boolean dealData()
       {
               System.out.println("Begin ContChangeComUI.dealData.........");
               return true;
       }


       /**
        * 准备往后层输出所需要的数据
        * 输出：如果准备数据时发生错误则返回false,否则返回true
        */
       private boolean prepareOutputData()
       {
               try
               {
                       System.out.println("Begin ContChangeComUI.prepareOutputData.........");
                       mInputData.clear();
                       mInputData.add(mGlobalInput);
                       mInputData.add(mLCContSchema);
                       mInputData.add(mNewLCContSchema);

               }
               catch (Exception ex)
               {
                       // @@错误处理
                       CError tError = new CError();
                       tError.moduleName = "ContChangeComUI";
                       tError.functionName = "prepareOutputData";
                       tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
                       this.mErrors.addOneError(tError);
                       return false;
               }
               return true;
        }

    public VData getResult()
    {
        return this.mResult;
    }
}



