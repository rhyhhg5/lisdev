package com.sinosoft.lis.agentdaily;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public class LAContUI
{
    public CErrors mErrors=new CErrors();
    public VData mResult =new VData();
    private VData mInputData=new VData();
    private String mOperate="";
    public LAContUI()
    {
    }
    public boolean submitData(VData cInputData,String cOperate)
    {
        mOperate=cOperate;
        this.mInputData =(VData)cInputData.clone() ;
        LAContBL tLAContBL=new LAContBL();
        try
        {
            if (!tLAContBL.submitData(mInputData, mOperate))
            {
                this.mErrors.copyAllErrors(tLAContBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAContUI";
                tError.functionName = "submitDat";
                tError.errorMessage = "BL类处理失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        mResult=tLAContBL.getResult();
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

}
