package com.sinosoft.lis.agentdaily;


import java.io.File;

import com.sinosoft.lis.db.LARewardPunishDB;
import com.sinosoft.lis.schema.LARewardPunishSchema;
import com.sinosoft.lis.vschema.LARewardPunishSet;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpImportLogSchema;
import com.sinosoft.lis.vschema.LCGrpImportLogSet;
import com.sinosoft.lis.vschema.LDComSet;
import com.sinosoft.lis.vschema.LMRiskAppSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.db.LAWageDB;
import com.sinosoft.lis.vschema.LAWageSet;
import com.sinosoft.lis.vschema.LAAgentSet;

/**
 * <p>Title: </p>
 *
 * <p>Description:</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Alsa
 * @version 1.1
 */
public class DiskImportAddPerBL {
    /**
     * 错误的容器
     */
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = null;

    /** 数据操作字符串 */
    private String mOperate;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private MMap mmap = new MMap();
    private MMap mmapCompare = new MMap();

    private int importPersons = 0; //记录导入成功的记录数
    private int unImportRecords = 0; //记录导入不成功的记录数

    private String diskimporttype = "";
    private String path = "";
    private String fileName = "";
    private String configName = "LAAddPerDiskImport.xml";
    LDComDB mLDComDB=new LDComDB();
    LDComSet mLDComSet=new LDComSet();

    private LARewardPunishSet mLARewardPunishSetFinal=new LARewardPunishSet();
    private LARewardPunishSet mLARewardPunishSet = null;
    private LCGrpImportLogSet mLCGrpImportLogSet=new LCGrpImportLogSet();
    private int merrorNum=0;
    private int idx=0;
    private StringBuffer merrorInfo=new StringBuffer();
    public DiskImportAddPerBL() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
    	  System.out.println("BL submited");
        if (!getInputData(cInputData)) {
            return false;
        }
        if(!checkBatchno()){
        	return false;
        }
        //System.out.println("geted inputdata");
        mOperate = cOperate;
        String path = (String) mTransferData.getValueByName("path");
        String fileName = path + File.separator +
                          (String) mTransferData.getValueByName("fileName");
        diskimporttype=(String)mTransferData.getValueByName("diskimporttype");
        String configFileName = path + File.separator + configName;
        //System.out.println("begin import");
        //从磁盘导入数据
        LAAddPerDiskImporter importer = new LAAddPerDiskImporter(fileName,
                configFileName,
                diskimporttype);
        importer.setTableName(diskimporttype);
        if (!importer.doImport()) {
            mErrors.copyAllErrors(importer.mErrrors);
            return false;
        }
        System.out.println("doimported");
        if (diskimporttype.equals("LARewardPunish")) {
            mLARewardPunishSet = (LARewardPunishSet) importer
                                  .getSchemaSet();

        }
        if (!checkData()) {
            return false;
        }
        System.out.println("data checked");
        //数据准备操作
        if (mOperate.equals("INSERT")) {
            if (!prepareData()) {
                return false;
            }
            System.out.println("---End prepareData---");
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mResult, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);

            System.out.println("DiskImportAddPerBL数据提交失败");
            this.mErrors.addOneError("数据提交失败!");
            return false;
        }
        System.out.println("--- ->submitData--- 数据提交成功。");

        return true;
    }

    private boolean getInputData(VData cInputData) {
        mTransferData = (TransferData) cInputData.
                        getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.
                       getObjectByObjectName("GlobalInput", 0);
        this.fileName=(String)mTransferData.getValueByIndex(4);

        System.out.println("...........mTransferData.getValueByIndex(4)"+mTransferData.getValueByIndex(4));
        if (mTransferData == null || mGlobalInput == null) {
            mErrors.addOneError("传入的的数据不完整: mTransferData == null "
                                + "|| mGlobalInput == null");
            return false;
        }
        diskimporttype = (String) mTransferData.getValueByName("diskimporttype");
        System.out.println("...............mTransferData.getValueByName(branchtype)"
        		+mTransferData.getValueByName("branchtype"));
          String tSQL ="select value(MAX(Idx),0)  from LARewardPunish where 1=1 with ur";
           ExeSQL tExeSQL = new ExeSQL();
           String tCount = tExeSQL.getOneValue(tSQL);
           idx   =   Integer.parseInt(tCount);

        return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkBatchno(){
    	String tSQL="select 1 from lcgrpimportlog where batchno='"+this.fileName+"' with ur";
    	ExeSQL tExeSQL=new ExeSQL();
    	if(tExeSQL.getOneValue(tSQL).equals("1")){
        	mErrors.addOneError("该文件批次已经被使用过，请使用新的文件批次！");
    		return false;
    	}
    	return true;
    }
    /**
     * 校验导入数据的合法性
     * @return boolean, 执行成功：true， 否则：false
     */
    private boolean checkData() {
        //展业证信息校验
        if (diskimporttype.equals("LARewardPunish")) {
            if (mLARewardPunishSet == null) {
                mErrors.addOneError("导入加扣款信息失败！没有得到要导入的数据。");
                return false;
            }
            else
            {
            	LMRiskAppDB tLMRiskAppDB=new LMRiskAppDB();
            	LMRiskAppSet tLMRiskAppSet=new LMRiskAppSet();
            	String tempSQL="select * from lmriskapp where enddate is null";
            	tLMRiskAppSet=tLMRiskAppDB.executeQuery(tempSQL);
            	tempSQL="select * from ldcom where length(trim(comcode))=8";
            	mLDComSet=mLDComDB.executeQuery(tempSQL);
            	System.out.println("mLDComSet size:"+mLDComSet.size());
            	LARewardPunishDB mTempLARewardPunishDB=new LARewardPunishDB();
            	//LARewardPunishSet mLARewardPunishSet=new LARewardPunishSet();
            	LARewardPunishSchema mTempLARewardPunishSchema=new LARewardPunishSchema();
            	LARewardPunishSchema mTempLARewardPunishSchema2;
            	LCGrpImportLogSchema tLCGrpImportLogSchema;
            	String importInfo;
                for (int i = 1; i <= mLARewardPunishSet.size(); i++) {
                	this.merrorNum=0;
                	this.merrorInfo.delete(0,merrorInfo.length());
                	System.out.println("..............checkdata here merrorInfo"
                			+this.merrorInfo);
                	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                	mTempLARewardPunishSchema=mLARewardPunishSet.get(i);
                    if (mTempLARewardPunishSchema.getManageCom() == null ||
                    		mTempLARewardPunishSchema.getManageCom().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("管理机构不能为空/");
                    }
                    if (mTempLARewardPunishSchema.getAgentCode() == null ||
                    		mTempLARewardPunishSchema.getAgentCode().equals(
                                       "")) {
                        this.merrorNum++;
                        this.merrorInfo.append("营销员代码不能为空/");
                    }
                     if (mTempLARewardPunishSchema.getAClass() == null ||
                    		mTempLARewardPunishSchema.getAClass().equals(
                                       "")) {
                        this.merrorNum++;
                        this.merrorInfo.append("调整类型不能为空");
                    }
                    if (mTempLARewardPunishSchema.getDoneFlag() == null ||
                    		mTempLARewardPunishSchema.getDoneFlag().equals("")) {
                        this.merrorNum++;
                        this.merrorInfo.append("调整项目不能为空");
                    }
                    if (mTempLARewardPunishSchema.getWageNo() == null ||
                    		mTempLARewardPunishSchema.getWageNo().equals("")) {
                    	this.merrorNum++;
                        this.merrorInfo.append("调整年月不能为空");
                    }
                    if (mTempLARewardPunishSchema.getMoney()<=0) {
                        this.merrorNum++;
                        this.merrorInfo.append("调整金额不能为负数");
                    }

                    if(mTempLARewardPunishSchema.getManageCom()!=null
                    		&&!mTempLARewardPunishSchema.getManageCom().equals("")
                    		&&!mTempLARewardPunishSchema.getManageCom().equals("86")){
                    	int tempFlag1=0;
                        for(int m=1;m<=mLDComSet.size();m++){
                        	if(mLDComSet.get(m).getComCode()
                        			.equals(mTempLARewardPunishSchema.getManageCom())){
                        		tempFlag1=1;
                        		break;
                        	}
                        }
                        if(tempFlag1==0){
                        	this.merrorNum++;
                            this.merrorInfo.append("管理机构代码不存在/");
                        }
                    }

                    if(mTempLARewardPunishSchema.getAgentCode()!=null
                    		&&!mTempLARewardPunishSchema.getAgentCode().equals("")){
                     String tAgentCode = mTempLARewardPunishSchema.getAgentCode();
                     String tManageCom=mTempLARewardPunishSchema.getManageCom();
                     LAAgentSet mLAAgentSet=new LAAgentSet();
                     LAAgentDB mLAAgentDB=new LAAgentDB();
                     mLAAgentDB.setAgentCode(tAgentCode);
                     mLAAgentDB.setManageCom(tManageCom);
                     mLAAgentSet=mLAAgentDB.query();
                     if(mLAAgentSet.size()==0){
                        this.merrorNum++;
                        this.merrorInfo.append("代理人代码和管理机构信息不符合");
                     }
                     LAAgentDB tLAAgentDB = new LAAgentDB();
                     tLAAgentDB.setAgentCode(tAgentCode);
                     if (!tLAAgentDB.getInfo())
                     {
                     	this.merrorNum++;
                        this.merrorInfo.append("查询不到代理人的信息");
                     }
                     String tAgentState = tLAAgentDB.getAgentState();
                     if (tAgentState != null && tAgentState.compareTo("06") >=0) //离职人员
                     {
                       String OutWorkDate = tLAAgentDB.getOutWorkDate();
                       String yearmonth = mTempLARewardPunishSchema.getWageNo();
                       String sql ="select startdate,enddate from lastatsegment where stattype='5' and yearmonth=" +
                                    yearmonth + "";
                       ExeSQL aExeSQL = new ExeSQL();
                       SSRS aSSRS = new SSRS();
                       aSSRS = aExeSQL.execSQL(sql);
                       String startdate = aSSRS.GetText(1, 1);
                       String enddate = aSSRS.GetText(1, 2);
                       System.out.println(OutWorkDate+"/"+startdate);
                       if (OutWorkDate.compareTo(startdate) < 0)
                       {
                       	this.merrorNum++;
                        this.merrorInfo.append( "代理人" + mTempLARewardPunishSchema.getAgentCode()
                         + "在" + OutWorkDate +"已经离职，不能做" + yearmonth + "的加扣款!");
                       }
                      }
                      String  tsql = "select * from lawage  where indexcalno='"+mTempLARewardPunishSchema.getWageNo()+"' and managecom='"+
                       mTempLARewardPunishSchema.getManageCom()+"' and BranchType='1' and BranchType2='01' fetch first 5 rows only";
                      LAWageDB tLAWageDB = new LAWageDB();
                      LAWageSet tLAWageSet = new LAWageSet();
                      tLAWageSet=tLAWageDB.executeQuery(tsql);
                      if(tLAWageSet.size()>0)
                      {
                      	this.merrorNum++;
                        this.merrorInfo.append("机构:"+mTempLARewardPunishSchema.getManageCom()+"月份:"
                        +mTempLARewardPunishSchema.getWageNo()+"已经计算过佣金,不能进行加扣款的导入!");
                      }
                    }
                    if(this.merrorNum==0){
                    	for(int j = i+1; j <= mLARewardPunishSet.size(); j++){
                        	mTempLARewardPunishSchema2=mLARewardPunishSet.get(j);
                        	if(mTempLARewardPunishSchema.getManageCom()
                        			.equals(mTempLARewardPunishSchema2.getManageCom())
                        			&&mTempLARewardPunishSchema.getAgentCode()
                        			.equals(mTempLARewardPunishSchema2.getAgentCode())
                        			&&mTempLARewardPunishSchema.getAClass()
                        			==mTempLARewardPunishSchema2.getAClass()
                        			&&mTempLARewardPunishSchema.getDoneFlag()
                        			.equals(mTempLARewardPunishSchema2.getDoneFlag())
                        			&&mTempLARewardPunishSchema.getWageNo()
                        			==mTempLARewardPunishSchema2.getWageNo()
                        			&&mTempLARewardPunishSchema.getMoney()
                        			==mTempLARewardPunishSchema2.getMoney()){
                        		this.merrorNum++;
                                this.merrorInfo.append("与第"+j+"行数据重复/");
                        	}
                        }
                    }
                    if(this.merrorNum==0){
                       checkExist(mTempLARewardPunishSchema,mTempLARewardPunishDB);
                    }
                    if(this.merrorNum>0){
                    	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                    	tLCGrpImportLogSchema.setErrorInfo("导入错误！具体错误有："
                    			+this.merrorInfo.toString());
                    	tLCGrpImportLogSchema.setErrorState("1");
                    	tLCGrpImportLogSchema.setErrorType("1");
                    	tLCGrpImportLogSchema.setContID(i+"");
                    	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                    	this.unImportRecords++;
                    }
                    else{
                    	tLCGrpImportLogSchema=new LCGrpImportLogSchema();
                    	tLCGrpImportLogSchema.setErrorInfo("导入成功！");
                    	tLCGrpImportLogSchema.setErrorState("0");
                    	tLCGrpImportLogSchema.setErrorType("0");
                    	tLCGrpImportLogSchema.setContID(i+"");
                    	mLCGrpImportLogSet.add(tLCGrpImportLogSchema);
                    }
                }
            }
        }
        return true;
    }
private boolean checkExist(LARewardPunishSchema mLARewardPunishSchema,LARewardPunishDB mLARewardPunishDB){

	LARewardPunishSet tempLARewardPunishSet=new LARewardPunishSet();
	String tempSql="select * from larewardpunish where managecom='"
		+mLARewardPunishSchema.getManageCom()+"' and agentcode='"
		+mLARewardPunishSchema.getAgentCode()+"' and aclass='"
		+mLARewardPunishSchema.getAClass()+"' and doneflag='"
		+mLARewardPunishSchema.getDoneFlag()+"' and wageno='"
		+mLARewardPunishSchema.getWageNo()+"' and branchtype='"
		+mTransferData.getValueByName("branchtype")+"' and branchtype2='"
		+mTransferData.getValueByName("branchtype2")+"'";
	System.out.println("checkExist:tempsql"+tempSql);
	tempLARewardPunishSet=mLARewardPunishDB.executeQuery(tempSql);
	if(tempLARewardPunishSet.size()>0){
		System.out.println("unImportRecords appending one");
		this.merrorNum++;
        this.merrorInfo.append("在数据库中已经存在/");
	}else{
		LARewardPunishSchema tempLARewardPunishSchema=new LARewardPunishSchema();
		tempLARewardPunishSchema.setManageCom(mLARewardPunishSchema.getManageCom());
		tempLARewardPunishSchema.setAgentCode(mLARewardPunishSchema.getAgentCode());
		tempLARewardPunishSchema.setAClass(mLARewardPunishSchema.getAClass());
		tempLARewardPunishSchema.setDoneFlag(mLARewardPunishSchema.getDoneFlag());
		tempLARewardPunishSchema.setAwardTitle(mLARewardPunishSchema.getAwardTitle());
		tempLARewardPunishSchema.setWageNo(mLARewardPunishSchema.getWageNo());
		tempLARewardPunishSchema.setMoney(mLARewardPunishSchema.getMoney());
                tempLARewardPunishSchema.setDoneDate(mLARewardPunishSchema.getWageNo().substring(0,4)+"-"+mLARewardPunishSchema.getWageNo().substring(4)+"-01");
		//tempLARewardPunishSchema=mLARewardPunishSchema;
		mLARewardPunishSetFinal.add(tempLARewardPunishSchema);
	}
	return true;
}
    /**
     * 检查每个客户是否是本公司的代理人，若不是则剔除
     * @param
     * @return boolean，成功校验 trun
     */
    private boolean prepareData() {
        if (diskimporttype.equals("LARewardPunish")) {
        	System.out.println("prepareData:doing");
            importPersons=mLARewardPunishSetFinal.size();

            if(mLARewardPunishSetFinal!=null&&mLARewardPunishSetFinal.size()>0){
            	for (int i = 1; i <= mLARewardPunishSetFinal.size(); i++) {
              	String branchtype=(String) mTransferData.getValueByName("branchtype");
                String branchtype2=(String) mTransferData.getValueByName("branchtype2");
                String tAgentCode = this.mLARewardPunishSetFinal.get(i).getAgentCode().trim();

            	  String tAgentGroup ="";
                String tBranchAttr="";
            	  String ttSQL ="select agentgroup,branchattr from labranchgroup where agentgroup=(select agentgroup from laagent where agentcode='"+tAgentCode+"')";
            	ExeSQL  ttExeSQL = new ExeSQL();
                SSRS  tGroup = ttExeSQL.execSQL(ttSQL);
                if (tGroup != null) {
                tAgentGroup = tGroup.GetText(1, 1);
                tBranchAttr = tGroup.GetText(1, 2);
                }
            	  mLARewardPunishSetFinal.get(i).setAgentGroup(tAgentGroup);
            	  mLARewardPunishSetFinal.get(i).setBranchAttr(tBranchAttr);
              	mLARewardPunishSetFinal.get(i).setOperator(mGlobalInput.Operator);
                      idx+=1;
              	mLARewardPunishSetFinal.get(i).setIdx(idx);
          	mLARewardPunishSetFinal.get(i).setBranchType(branchtype);
              	mLARewardPunishSetFinal.get(i).setBranchType2(branchtype2);
              	mLARewardPunishSetFinal.get(i).setMakeDate(PubFun.getCurrentDate());
              	mLARewardPunishSetFinal.get(i).setMakeTime(PubFun.getCurrentTime());
              	mLARewardPunishSetFinal.get(i).setModifyDate(PubFun.getCurrentDate());
              	mLARewardPunishSetFinal.get(i).setModifyTime(PubFun.getCurrentTime());
              	mLARewardPunishSetFinal.get(i).setSendGrp("00");
              	mmap.put(mLARewardPunishSetFinal.get(i), "INSERT");
              }
            }

            if(this.mLCGrpImportLogSet!=null&&this.mLCGrpImportLogSet.size()>0){
            	//importPersons = mLARewardPunishSetFinal.size();
            	for(int i=1;i<=this.mLCGrpImportLogSet.size();i++){
            		mLCGrpImportLogSet.get(i).setGrpContNo("00000000000000000000");
            		mLCGrpImportLogSet.get(i).setOperator(this.mGlobalInput.Operator);
            		mLCGrpImportLogSet.get(i).setMakeDate(PubFun.getCurrentDate());
            		mLCGrpImportLogSet.get(i).setMakeTime(PubFun.getCurrentTime());
            		mLCGrpImportLogSet.get(i).setBatchNo(this.fileName);
            		mmap.put(mLCGrpImportLogSet.get(i), "INSERT");
            	}
            }
            System.out.println("prepareData:adding map");
            this.mResult.add(mmap);
            //this.mResult.add(mmapCompare);
            System.out.println("prepareData:did");
        }
        return true;
    }

    /**
     *成功导入的记录数
     * @return int
     */
    public int getImportPersons() {
        return importPersons;
    }
    public int getUnImportRecords() {
        return unImportRecords;
    }

    public static void main(String[] args) {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        TransferData t = new TransferData();

        t.setNameAndValue("diskimporttype", "LACertification");
        t.setNameAndValue("path", "D:\\picch\\ui\\temp");
        t.setNameAndValue("fileName", "展业证清单.xls");

        VData v = new VData();
        v.add(g);
        v.add(t);

        DiskImportAddPerBL d = new DiskImportAddPerBL();
        d.submitData(v, "INSERT");

        if (d.mErrors.needDealError()) {
            System.out.println(d.mErrors.getErrContent());
        }
    }

}
