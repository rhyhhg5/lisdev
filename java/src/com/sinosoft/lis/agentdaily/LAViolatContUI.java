package com.sinosoft.lis.agentdaily;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xiangchun
 * @version 1.2
 */
public class LAViolatContUI
{
    public CErrors mErrors=new CErrors();
    public VData mResult =new VData();
    private VData mInputData=new VData();
    private String mOperate="";
     private String ViolatNo="";
    public LAViolatContUI()
    {
    }
    public boolean submitData(VData cInputData,String cOperate)
    {
        mOperate=cOperate;
        this.mInputData =(VData)cInputData.clone() ;
        LAViolatContBL tLAViolatContBL=new LAViolatContBL();
        try
        {
            if (!tLAViolatContBL.submitData(mInputData, mOperate))
            {
                this.mErrors.copyAllErrors(tLAViolatContBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAViolatContUI";
                tError.functionName = "submitDat";
                tError.errorMessage = "BL类处理失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        mResult=tLAViolatContBL.getResult();
        ViolatNo=tLAViolatContBL.getViolatNo();
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }
    public String getViolatNo()
    {
        return ViolatNo;
    }

}
