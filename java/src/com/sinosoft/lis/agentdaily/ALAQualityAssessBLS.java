/*
 * <p>ClassName: ALAQualityAssessBLS </p>
 * <p>Description: ALAQualityAssessBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentdaily;


import java.sql.Connection;

import com.sinosoft.lis.db.LAQualityAssessDB;
import com.sinosoft.lis.schema.LAQualityAssessSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;

public class ALAQualityAssessBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    public ALAQualityAssessBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        System.out.println("Start ALAQualityAssessBLS Submit...");
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            tReturn = saveLAQualityAssess(cInputData);
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            tReturn = deleteLAQualityAssess(cInputData);
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            tReturn = updateLAQualityAssess(cInputData);
        }
        if (tReturn)
        {
            System.out.println(" sucessful");
        }
        else
        {
            System.out.println("Save failed");
        }
        System.out.println("End ALAQualityAssessBLS Submit...");
        return tReturn;
    }


    /**
     * 保存函数
     */
    private boolean saveLAQualityAssess(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAQualityAssessBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAQualityAssessDB tLAQualityAssessDB = new LAQualityAssessDB(conn);
            tLAQualityAssessDB.setSchema((LAQualityAssessSchema) mInputData.
                                        getObjectByObjectName(
                                                "LAQualityAssessSchema", 0));
            if (!tLAQualityAssessDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAQualityAssessDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAQualityAssessBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
        }
        try
        {
            conn.close();
        }
        catch (Exception e)
        {}
        return tReturn;
    }


    /**
     * 保存函数
     */
    private boolean deleteLAQualityAssess(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAQualityAssessBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAQualityAssessDB tLAQualityAssessDB = new LAQualityAssessDB(conn);
            tLAQualityAssessDB.setSchema((LAQualityAssessSchema) mInputData.
                                        getObjectByObjectName(
                                                "LAQualityAssessSchema", 0));
            if (!tLAQualityAssessDB.delete())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAQualityAssessDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAQualityAssessBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.close();
                return false;
            }
            conn.commit();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAQualityAssessBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
        }
        try
        {
            conn.close();
        }
        catch (Exception e)
        {}
        return tReturn;
    }


    /**
     * 保存函数
     */
    private boolean updateLAQualityAssess(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            CError tError = new CError();
            tError.moduleName = "ALAQualityAssessBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAQualityAssessDB tLAQualityAssessDB = new LAQualityAssessDB(conn);
            tLAQualityAssessDB.setSchema((LAQualityAssessSchema) mInputData.
                                        getObjectByObjectName(
                                                "LAQualityAssessSchema", 0));
            if (!tLAQualityAssessDB.update())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAQualityAssessDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALAQualityAssessBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAQualityAssessBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
        }
        try
        {
            conn.close();
        }
        catch (Exception e)
        {}
        return tReturn;
    }
}
