package com.sinosoft.lis.agentdaily;


import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.agentdaily.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.db.*;

/**
 * <p>Title: LABankBaseWageBL</p>
 *
 * <p>Description:  LIS - 销售管理</p>
 *
 * <p>Copyright: Copyright (c) 2004</p>
 *
 * <p>Company: SinoSoft</p>
 *
 * @author luomin
 * @version 1.0
 */
public class LABankBaseWageBL {
  //错误处理类
  public static CErrors mErrors = new CErrors();
  //业务处理相关变量
  /** 全局数据 */
  private VData mInputData = new VData();
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap map = new MMap();
  public  GlobalInput mGlobalInput = new GlobalInput();

  private LABaseWageSet mLABaseWageSet = new LABaseWageSet();

  private Reflections ref = new Reflections();

  public LABankBaseWageBL() {

  }


  /**
    传输数据的公共方法
   */
  public boolean submitData(VData cInputData, String cOperate) {
    System.out.println("Begin LABankBaseWageBL.submitData.........");
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    //进行业务处理
    if (!dealData()) {
      return false;
    }
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    PubSubmit tPubSubmit = new PubSubmit();
    System.out.println("Start LABankBaseWageBL Submit...");
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "LABankBaseWageBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData) {
    //全局变量
    try {
      System.out.println("Begin LABankBaseWageBL.getInputData.........");
      this.mGlobalInput.setSchema( (GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));
     System.out.println("GlobalInput get");
     this.mLABaseWageSet.set( (LABaseWageSet) cInputData.getObjectByObjectName("LABaseWageSet",0));
     System.out.println("LABaseWageSet get"+mLABaseWageSet.size());

    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LABankBaseWageBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "在读取处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    System.out.println("getInputData end ");
    return true;
  }

  /**
   * 业务处理主函数
   */
  private boolean dealData() {
    System.out.println("Begin LABankBaseWageBL.dealData........."+mOperate);
    try {
        if (mOperate.equals("UPDATE") || mOperate.equals("DELETE")) {
          System.out.println("Begin LABankBaseWageBL.dealData.........1"+mLABaseWageSet.size());
          LABaseWageSet tLABaseWageSet = new LABaseWageSet();
          LABaseWageBSet tLABaseWageBSet = new LABaseWageBSet();
          ExeSQL tExe = new ExeSQL();
          String tSql =
              "select max(int(edorno)) from labasewageb  ";
          String strIdx = "";
          int tMaxIdx = 0;

          strIdx = tExe.getOneValue(tSql);
          if (strIdx == null || strIdx.trim().equals("")) {
            tMaxIdx = 0;
          }
          else {
            tMaxIdx = Integer.parseInt(strIdx);
          }
          for (int i = 1; i <= mLABaseWageSet.size(); i++) {
            System.out.println("Begin LABankBaseWageBL.dealData.........2");
            LABaseWageSchema tLABaseWageSchemaold = new LABaseWageSchema();
            LABaseWageSchema tLABaseWageSchemanew = new  LABaseWageSchema();
            LABaseWageDB tLABaseWageDB = new LABaseWageDB();
            System.out.println("Begin LABankBaseWageBL.dealData.........3");
            System.out.println("++++++++"+mLABaseWageSet.get(i).getAgentGrade());
            System.out.println("++++++++"+mLABaseWageSet.get(i).getIdx());


            tLABaseWageDB.setIdx(mLABaseWageSet.get(i).getIdx());
            tLABaseWageDB.setAgentGrade(mLABaseWageSet.get(i).getAgentGrade());
            tLABaseWageSchemaold = tLABaseWageDB.query().get(1);
            tLABaseWageSchemanew = mLABaseWageSet.get(i);
            //tLARateCommisionSchemanew.setManageCom(mGlobalInput.ManageCom);
            tLABaseWageSchemanew.setOperator(mGlobalInput.Operator);
            tLABaseWageSchemanew.setModifyDate(CurrentDate);
            tLABaseWageSchemanew.setModifyTime(CurrentTime);
            tLABaseWageSchemanew.setMakeDate(tLABaseWageSchemaold.getMakeDate());
            tLABaseWageSchemanew.setMakeTime(tLABaseWageSchemaold.getMakeTime());
            tLABaseWageSchemanew.setType(tLABaseWageSchemaold.getType());

            tLABaseWageSet.add(tLABaseWageSchemanew);
            LABaseWageBSchema tLABaseWageBSchema = new
                LABaseWageBSchema();
            ref.transFields(tLABaseWageBSchema, tLABaseWageSchemaold);
            //获取最大的ID号
            tMaxIdx++;
            tLABaseWageBSchema.setEdorNo(String.valueOf(tMaxIdx));
//            if(i>1){
//              tLABaseWageBSchema.setEdorNo(tLABaseWageBSet.get(i-1).getEdorNo() + 1);
//              System.out.println("BL:"+tLABaseWageBSet.get(i-1).getEdorNo());
//            }
            tLABaseWageBSchema.setEdorType("11");

            tLABaseWageBSet.add(tLABaseWageBSchema);

          }
          map.put(tLABaseWageSet, mOperate);
          map.put(tLABaseWageBSet, "INSERT");
        }

      else {
      LABaseWageSet tLABaseWageSet = new   LABaseWageSet();

      for (int i = 1; i <= mLABaseWageSet.size(); i++) {
       LABaseWageSchema tLABaseWageSchemanew = new    LABaseWageSchema();
       tLABaseWageSchemanew = mLABaseWageSet.get(i);

  /**
       //获取最大的ID号
      ExeSQL tExe = new ExeSQL();
      String tSql =
          "select int(max(idx)) from labasewage order by 1 desc ";
      String strIdx = "";
      int tMaxIdx = 0;

      strIdx = tExe.getOneValue(tSql);
      if (strIdx == null || strIdx.trim().equals("")) {
        tMaxIdx = 0;
      }
      else {
        tMaxIdx = Integer.parseInt(strIdx);
        System.out.println(tMaxIdx);
      }
      tMaxIdx++;
      tLABaseWageSchemanew.setIdx(tMaxIdx);
      if(i>1){
        tLABaseWageSchemanew.setIdx(mLABaseWageSet.get(i - 1).getIdx() + 1);
      }
*/
          tLABaseWageSchemanew.setOperator(mGlobalInput.Operator);
          tLABaseWageSchemanew.setMakeDate(CurrentDate);
          tLABaseWageSchemanew.setMakeTime(CurrentTime);
          tLABaseWageSchemanew.setModifyDate(CurrentDate);
          tLABaseWageSchemanew.setModifyTime(CurrentTime);
          tLABaseWageSchemanew.setType("11");
          tLABaseWageSet.add(tLABaseWageSchemanew);

        }
        map.put(tLABaseWageSet, mOperate);
      }
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LABankBaseWageBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理所数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      System.out.println(
          "Begin LABankBaseWageBL.prepareOutputData.........");
      mInputData.clear();
      mInputData.add(map);
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LABankBaseWageBL";
      tError.functionName = "prepareOutputData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }
}
