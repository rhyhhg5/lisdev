package com.sinosoft.lis.agentdaily;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2008</p>
 * <p>Company: </p>
 * @author Alse
 * @version 1.0
 */
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;


public class LAAddReManageUI
{

    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();


    public LAAddReManageUI()
    {
    }

    public boolean submitData(VData cInputData)
    {
        mInputData = (VData) cInputData.clone();
        LAAddReManageBL tLAAddReManageBL = new LAAddReManageBL();
        if (!tLAAddReManageBL.submitData(mInputData))
        {
            this.mErrors.copyAllErrors(tLAAddReManageBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "tLAAddReManageUI";
            tError.functionName = "submitData";
            tError.errorMessage = tLAAddReManageBL.mErrors.getFirstError();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
