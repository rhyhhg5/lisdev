package com.sinosoft.lis.agentdaily;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.agentdaily.GrpAscriptionBL;
/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public class GrpAscriptionUI
{
    public CErrors mErrors=new CErrors();
    public VData mResult =new VData();
    private VData mInputData=new VData();
    private String mOperate="";
    public GrpAscriptionUI()
    {
    }
    public boolean submitData(VData cInputData,String cOperate)
    {

        mOperate=cOperate;
        this.mInputData =(VData)cInputData.clone() ;
        GrpAscriptionBL tGrpAscriptionBL = new GrpAscriptionBL();
        try
        {
            if (!tGrpAscriptionBL.submitData(mInputData, mOperate))
            {
                this.mErrors.copyAllErrors(tGrpAscriptionBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "GrpAscriptionUI";
                tError.functionName = "submitDat";
                tError.errorMessage = "BL类处理失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        mResult=tGrpAscriptionBL.getResult();
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

}
