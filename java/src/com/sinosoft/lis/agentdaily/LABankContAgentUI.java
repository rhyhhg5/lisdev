package com.sinosoft.lis.agentdaily;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.agentdaily.LABankContAgentBL;
/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public class LABankContAgentUI
{
    public CErrors mErrors=new CErrors();
    public VData mResult =new VData();
    private VData mInputData=new VData();
    private String mOperate="";
    public LABankContAgentUI()
    {
    }
    public boolean submitData(VData cInputData,String cOperate)
    {
        System.out.println("11111111113244444444444444444444444444444");
        System.out.println();
        mOperate=cOperate;
        this.mInputData =(VData)cInputData.clone() ;
        System.out.println("??------------?????");
        LABankContAgentBL tHDLABankContAgentBL = new LABankContAgentBL();
        System.out.println("-------------------"+tHDLABankContAgentBL.hashCode());
        try
        {
            if (!tHDLABankContAgentBL.submitData(mInputData, mOperate))
            {
                this.mErrors.copyAllErrors(tHDLABankContAgentBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "HDLABankContAgentUI";
                tError.functionName = "submitDat";
                tError.errorMessage = "BL类处理失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        mResult=tHDLABankContAgentBL.getResult();
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

}
