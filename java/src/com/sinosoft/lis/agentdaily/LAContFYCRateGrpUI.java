/*
 * <p>ClassName: OLAArchieveUI </p>
 * <p>Description: OLAArchieveUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:05:58
 */
package com.sinosoft.lis.agentdaily;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.pubfun.*;
public class LAContFYCRateGrpUI {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
//业务处理相关变量
 /** 全局数据 */
private LAContFYCRateGrpBL tLAContFYCRateGrpBL=new LAContFYCRateGrpBL();
public LAContFYCRateGrpUI ()
{
}
 /**
传输数据的公共方法
*/
public boolean submitData(VData cInputData,String cOperate)
{
  //将操作数据拷贝到本类中
   tLAContFYCRateGrpBL.submitData(cInputData,cOperate);
   System.out.println("End LAContFYCRateGrpUI UI Submit...");
  //如果有需要处理的错误，则返回
    if (tLAContFYCRateGrpBL.mErrors .needDealError() )
       {
     // @@错误处理
         this.mErrors.copyAllErrors(tLAContFYCRateGrpBL.mErrors);
         CError tError = new CError();
         tError.moduleName = "LAContFYCRateGrpUI";
         tError.functionName = "submitData";
         tError.errorMessage = "数据提交失败!";
         this.mErrors .addOneError(tError) ;
         return false;
       }
       return true;
  }

/**
 * 根据前面的输入数据，进行UI逻辑处理
  * 如果在处理过程中出错，则返回false,否则返回true
  */

public VData getResult()
{
  return this.mResult;
}
public static void main(String[] args)
{
   
}
}
