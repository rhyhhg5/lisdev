/*
 * <p>ClassName: OLAArchieveUI </p>
 * <p>Description: OLAArchieveUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:05:58
 */
package com.sinosoft.lis.agentdaily;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.pubfun.*;
public class OLAArchieveUI {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData =new VData();
private LAArchieveSet mLAArchieveSet=new LAArchieveSet();

private GlobalInput mGlobalInput=new GlobalInput();
/** 数据操作字符串 */
private String mOperate;
private String mTest;
//业务处理相关变量
 /** 全局数据 */
private LAArchieveSchema mLAArchieveSchema=new LAArchieveSchema();
public OLAArchieveUI ()
{
}
 /**
传输数据的公共方法
*/
public boolean submitData(VData cInputData,String cOperate)
{
  //将操作数据拷贝到本类中
  System.out.println("cOperate+++"+cOperate);
  this.mOperate =cOperate;

  //得到外部传入的数据,将数据备份到本类中
  if (!getInputData(cInputData))
    return false;

  //进行业务处理
  if (!dealData())
    return false;

  //准备往后台的数据
  if (!prepareOutputData())
    return false;

  OLAArchieveBL tOLAArchieveBL=new OLAArchieveBL();

  System.out.println("Start OLAArchieve UI Submit...");
  tOLAArchieveBL.submitData(mInputData,mOperate);
  System.out.println("End OLAArchieve UI Submit...");
  //如果有需要处理的错误，则返回
  if (tOLAArchieveBL.mErrors .needDealError() )
  {
     // @@错误处理
     this.mErrors.copyAllErrors(tOLAArchieveBL.mErrors);
     CError tError = new CError();
     tError.moduleName = "OLAArchieveUI";
     tError.functionName = "submitData";
     tError.errorMessage = "数据提交失败!";
     this.mErrors .addOneError(tError) ;
     return false;
  }
  if (mOperate.equals("INSERT||MAIN"))
  {
     this.mResult.clear();
     this.mResult=tOLAArchieveBL.getResult();
     System.out.println("mResult:"+mResult.size() ) ;
  }
  mInputData=null;
  return true;
  }
  public static void main(String[] args)
  {
     LAArchieveSchema tLAArchieveSchema   = new LAArchieveSchema();
     LAArchieveSet tLAArchieveSet = new LAArchieveSet();
     tLAArchieveSchema = new LAArchieveSchema();
     tLAArchieveSchema.setArchType("01");
     tLAArchieveSchema.setArchItem("fff");
     tLAArchieveSchema.setItemIdx(2);
     tLAArchieveSchema.setItemCode("02");
     tLAArchieveSchema.setAgentCode("8611000001");
     tLAArchieveSchema.setArchieveNo("8611000001");
     tLAArchieveSchema.setArchieveDate("2005-05-06");
     tLAArchieveSchema.setPigeOnHoleDate("2005-05-06");
     tLAArchieveSet.add(tLAArchieveSchema);
     VData tVData=new VData();
     tVData.add(tLAArchieveSet) ;
     tVData.add(new GlobalInput());
      OLAArchieveUI tUI=new OLAArchieveUI();
      tUI.submitData(tVData,"INSERT||MAIN" );
  }
  /**
  * 准备往后层输出所需要的数据
  * 输出：如果准备数据时发生错误则返回false,否则返回true
  */
 private boolean prepareOutputData()
 {
    try
    {
      mInputData.clear();
      mInputData.add(this.mLAArchieveSet);
      mInputData.add(this.mGlobalInput ) ;
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LAArchieveUI";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
}
/**
 * 根据前面的输入数据，进行UI逻辑处理
  * 如果在处理过程中出错，则返回false,否则返回true
  */
 private boolean dealData()
 {
      boolean tReturn =false;
      mTest=mLAArchieveSet.get(1).getAgentCode();
      System.out.println("mTest+++"+mTest);
      //此处增加一些校验代码
      tReturn=true;
      return tReturn ;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
  //全局变量
  this.mLAArchieveSet.set((LAArchieveSet)cInputData.getObjectByObjectName("LAArchieveSet",0));
  this.mGlobalInput .setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
  System.out.println("End getinputData");
  return true;
}
public VData getResult()
{
  return this.mResult;
}
}
