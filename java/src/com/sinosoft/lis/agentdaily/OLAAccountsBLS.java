/*
 * <p>ClassName: OLAAccountsBLS </p>
 * <p>Description: OLAAccountsBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:03:36
 */
package com.sinosoft.lis.agentdaily;

import java.sql.Connection;

import com.sinosoft.lis.db.LAAccountsBDB;
import com.sinosoft.lis.db.LAAccountsDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.schema.LAAccountsBSchema;
import com.sinosoft.lis.schema.LAAccountsSchema;
import com.sinosoft.lis.schema.LAAgentSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.TransferData;

public class OLAAccountsBLS {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
//传输数据类
    private VData mInputData;
    /** 数据操作字符串 */
    private String mOperate;
    public OLAAccountsBLS() {
    }

    public static void main(String[] args) {
    }

    /**
      传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        mInputData = (VData) cInputData.clone();
        System.out.println(mOperate);
        if (this.mOperate.equals("INSERT||MAIN")) {
            if (!saveLAAccounts()) {
                return false;
            }
        }
        if (this.mOperate.equals("DELETE||MAIN")) {
            if (!deleteLAAccounts()) {
                return false;
            }
        }
        if (this.mOperate.equals("UPDATE||MAIN")) {
            if (!updateLAAccounts()) {
                return false;
            }
        }
        return true;
    }

    /**
     * 保存函数
     */
    private boolean saveLAAccounts() {
        LAAccountsSchema tLAAccountsSchema = new LAAccountsSchema();
        tLAAccountsSchema = (LAAccountsSchema) mInputData.getObjectByObjectName(
                "LAAccountsSchema", 0);
        LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        tLAAgentSchema = (LAAgentSchema) mInputData.getObjectByObjectName(
                "LAAgentSchema", 0);
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAAccountsBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            conn.setAutoCommit(false);
            LAAccountsDB tLAAccountsDB = new LAAccountsDB(conn);
            tLAAccountsDB.setSchema(tLAAccountsSchema);
            System.out.println("before insert:" +
                               tLAAccountsSchema.getAgentCode());
            if (!tLAAccountsDB.insert()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAccountsDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLAAccountsBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            LAAgentDB tLAAgentDB = new LAAgentDB(conn);
            tLAAgentDB.setSchema(tLAAgentSchema);
            System.out.println("before insert:" +
                               tLAAccountsSchema.getAgentCode());
            if (!tLAAgentDB.update()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAccountsDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLAAccountsBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            conn.commit();
            conn.close();
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAAccountsBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                conn.rollback();
                conn.close();
            } catch (Exception e) {}
            return false;
        }
        return true;
    }

    /**
     * 保存函数
     */
    private boolean deleteLAAccounts() {
        LAAccountsSchema tLAAccountsSchema = new LAAccountsSchema();
        tLAAccountsSchema = (LAAccountsSchema) mInputData.getObjectByObjectName(
                "LAAccountsSchema", 0);
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAAccountsBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAAccountsDB tLAAccountsDB = new LAAccountsDB(conn);
            tLAAccountsDB.setSchema(tLAAccountsSchema);
            if (!tLAAccountsDB.delete()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAccountsDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLAAccountsBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据删除失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAAccountsBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                conn.rollback();
                conn.close();
            } catch (Exception e) {}
            return false;
        }
        return true;
    }

    /**
     * 保存函数
     */
    private boolean updateLAAccounts() {
        LAAccountsSchema oLAAccountsSchema = new LAAccountsSchema();

        LJAGetSet tLJAGetSet = new LJAGetSet();
        
        TransferData tTransferData = new TransferData();

        tTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        oLAAccountsSchema.setAccount((String) tTransferData.getValueByName(
                "AccountOld"));
        oLAAccountsSchema.setAgentCode((String) tTransferData.getValueByName(
                "AgentCodeOld"));

        LAAccountsSchema tLAAccountsSchema = new LAAccountsSchema();
        tLAAccountsSchema = (LAAccountsSchema) mInputData.getObjectByObjectName(
                "LAAccountsSchema", 0);
        LAAccountsBSchema tLAAccountsBSchema = new LAAccountsBSchema();
        tLAAccountsBSchema = (LAAccountsBSchema) mInputData.getObjectByObjectName(
                "LAAccountsBSchema", 0);
        tLJAGetSet = (LJAGetSet) mInputData.getObjectByObjectName("LJAGetSet", 0);
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null) {
            CError tError = new CError();
            tError.moduleName = "OLAAccountsBLS";
            tError.functionName = "updateData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAAccountsDB tLAAccountsDB = new LAAccountsDB(conn);
//            tLAAccountsDB.setSchema(tLAAccountsSchema);
            tLAAccountsDB.setSchema(oLAAccountsSchema);
//            if (!tLAAccountsDB.update()) {
            if (!tLAAccountsDB.delete()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAccountsDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLAAccountsBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            tLAAccountsDB.setSchema(tLAAccountsSchema);
            if (!tLAAccountsDB.insert()) {
                this.mErrors.copyAllErrors(tLAAccountsDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLAAccountsBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;

            }
            
            LAAccountsBDB tLAAccountsBDB = new LAAccountsBDB(conn);
            tLAAccountsBDB.setSchema(tLAAccountsBSchema);
            System.out.println("before insert:" +
                               tLAAccountsSchema.getAgentCode());
            if (!tLAAccountsBDB.insert()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAccountsDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLAAccountsBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            if(tLJAGetSet.size()>0){
            for (int i = 1; i <= tLJAGetSet.size(); i++) {
            	LJAGetSchema tLJAGetSchema = new LJAGetSchema();
            	tLJAGetSchema = tLJAGetSet.get(i);
            	LJAGetDB tLJAGetDB = new LJAGetDB(conn);
            	tLJAGetDB.setSchema(tLJAGetSchema);
            	System.out.println("before insert:" +
                        tLJAGetSchema.getGetNoticeNo());
            	if (!tLJAGetDB.update()) {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLJAGetDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "OLAAccountsBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
				
			}
        }  
            conn.commit();
            conn.close();
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAAccountsBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                conn.rollback();
                conn.close();
            } catch (Exception e) {}
            return false;
        }
        return true;
    }
}
