package com.sinosoft.lis.agentdaily;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.vschema.LAAscriptionSet;
import com.sinosoft.lis.db.LAAscriptionDB;
import com.sinosoft.utility.SSRS;
import java.math.BigInteger;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.vdb.LAAgentDBSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.PubFun1;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LABankAscripAllBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData=new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private String querySql;
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mBranchType = "";
    private String mBranchType2= "";
    private int mLength=10000;//得到一次提交的长度
    private String updateSQL = "";
    private LAAscriptionSchema cLAAscriptionSchema = new LAAscriptionSchema();
    private LAAscriptionSet cLAAscriptionSet = new LAAscriptionSet();
    private LAAscriptionSet mLAAscriptionSet = new LAAscriptionSet();
    private LAAscriptionSet preLAAscriptionSet = new LAAscriptionSet();
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();
    private String mconttype="";//保单类型：1--个单  2--团单
    private MMap mMap = new MMap();

    public LABankAscripAllBL()
    {
    }

    public static void main(String[] args)
    {
        LABankAscripAllBL BL = new LABankAscripAllBL();
    }
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        System.out.println("Operate:"+cOperate);
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LABankAscripBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LABankAscripBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start LABankAscripBL Submit...");
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LABankAscripBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("End LABankAscripBL Submit...");

        mInputData = null;
        return true;
    }
    private boolean dealData()
    {
        System.out.println("come here ");
        System.out.println(this.querySql);
        //获取要分配的所有保单数据
        SSRS mSSRS = new SSRS();
        ExeSQL mExeSQL = new ExeSQL();
       if(this.querySql!=null && !"".equals(this.querySql)){
    	   mSSRS = mExeSQL.execSQL(this.querySql);
	        if (mExeSQL.mErrors.needDealError())
	        {
	           CError tError = new CError();
	           tError.moduleName = "LABankAscripAllBL";
	           tError.functionName = "dealData";
	           tError.errorMessage = "查询原业务员相应的信息出错";
	           this.mErrors.addOneError(tError);
	           return false;
	       }
       }
       System.out.println("总数据量："+mSSRS.getMaxRow());
       if(mSSRS.getMaxRow()>0){
    	for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
    		LAAscriptionSchema tempLAAscriptionSchema = new LAAscriptionSchema();
    		String SQL="select conttype from lccont where grpcontno='00000000000000000000' and contno='"+mSSRS.GetText(i, 1)+"'";
    		       SQL+="union select conttype from lccont where grpcontno<>'00000000000000000000' and grpcontno='"+mSSRS.GetText(i, 1)+"'";
    		ExeSQL tExeSQL = new ExeSQL();
    	    this.mconttype = tExeSQL.getOneValue(SQL);//保单类型：1--个单  2--团单
    		if(this.mconttype.equals("1"))
    		{
    			 System.out.println("apple个单："+this.mconttype+"/个单号"+mSSRS.GetText(i, 1));
	      	tempLAAscriptionSchema.setContNo(mSSRS.GetText(i, 1));
    		}
    		else if(this.mconttype.equals("2"))
    		{
    		System.out.println("apple团单："+this.mconttype+"/团单号"+mSSRS.GetText(i, 1));
    		tempLAAscriptionSchema.setGrpContNo(mSSRS.GetText(i, 1));
    		}
	      	tempLAAscriptionSchema.setAgentOld(new ExeSQL().getOneValue("select agentcode from laagent where groupagentcode='"+mSSRS.GetText(i, 9)+"'"));
	      	tempLAAscriptionSchema.setAgentComOld(mSSRS.GetText(i, 11));
	      	tempLAAscriptionSchema.setAgentNew(this.cLAAscriptionSchema.getAgentNew());
	      	tempLAAscriptionSchema.setAgentComNew(mSSRS.GetText(i, 15));
	      	tempLAAscriptionSchema.setBranchType("3");
	      	tempLAAscriptionSchema.setBranchType2("01");
	      	cLAAscriptionSet.add(tempLAAscriptionSchema);
		}
       }else{
    	   CError tError = new CError();
           tError.moduleName = "HDLAAscriptionBL";
           tError.functionName = "dealData";
           tError.errorMessage = "没有查询对应的保单数据！";
           this.mErrors.addOneError(tError);
           return false;
       }
       
        
       if (mOperate.equals("INSERT||MAIN"))
        {
        	for (int i = 1; i <= cLAAscriptionSet.size(); i++) {

        		LAAscriptionSchema cLAAscriptionSchema = new LAAscriptionSchema();
        		cLAAscriptionSchema = cLAAscriptionSet.get(i);

        		boolean tReturn = true;
            int tCount = 0;

           String tSQL = "select a.agentgroup,a.branchattr,b.managecom from labranchgroup a,laagent b "
           +" where a.agentgroup=b.agentgroup and b.agentcode='"
           + cLAAscriptionSchema.getAgentOld() + "'";
            SSRS tSSRS = new SSRS();
            ExeSQL tExeSQL = new ExeSQL();
            tSSRS = tExeSQL.execSQL(tSQL);
            if (tExeSQL.mErrors.needDealError())
            {
               CError tError = new CError();
               tError.moduleName = "HDLAAscriptionBL";
               tError.functionName = "dealData";
               tError.errorMessage = "查询原业务员相应的信息出错";
               this.mErrors.addOneError(tError);
               return false;
            }

            int a = tSSRS.getMaxRow();
            if (a != 1)
            {
               CError tError = new CError();
               tError.moduleName = "HDLAAscriptionBL";
               tError.functionName = "dealData";
               tError.errorMessage = "没有查到原业务员相应的信息";
               this.mErrors.addOneError(tError);
               return false;
             }
            String mAgentGroup = tSSRS.GetText(1, 1);
            String mBranchAttr = tSSRS.GetText(1, 2);
            String mManageCom=tSSRS.GetText(1, 3);
            String strsql="";
           if(this.mconttype.equals("1"))
           {
        	   strsql = "select count(ascripno)+1 from laascription where 1=1 and "
                   + " contno='"+ cLAAscriptionSchema.getContNo() + "'";
           }
           else if(this.mconttype.equals("2"))
           {
        	   strsql = "select count(ascripno)+1 from laascription where 1=1 and "
                   + " grpcontno='"+ cLAAscriptionSchema.getGrpContNo() + "'";
           }

            ExeSQL tExeSQL1 = new ExeSQL();
            String mCount = tExeSQL1.getOneValue(strsql);
            if (tExeSQL1.mErrors.needDealError())
            {
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询归属表信息出错";
                this.mErrors.addOneError(tError);
                return false;
            }
            int mCountNO = Integer.parseInt(mCount);//得到归属次数

            String tAgentOld = cLAAscriptionSchema.getAgentOld();
            String tContNo = cLAAscriptionSchema.getContNo();
            String tGrpcontNo=cLAAscriptionSchema.getGrpContNo();
            System.out.println("………………开始进行插入操作…………………"+ tContNo+"/"+tGrpcontNo);
            //判断是否为已撤销的保单
            String ttsql = "select 'X' from lccont where contno='"+ tContNo + "' and Agentcode='"
                           + tAgentOld+ "' and uwflag='a'";
                    ttsql+="union select 'X' from lcgrpcont where grpcontno='"+tGrpcontNo+"' and agentcode='"+tAgentOld+"' and uwflag='a'";
            ExeSQL ttExeSQL = new ExeSQL();
            String ttFlag = ttExeSQL.getOneValue(ttsql);
            if (ttExeSQL.mErrors.needDealError())
            {
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBL";
                tError.functionName = "dealData";
                tError.errorMessage = "查询保单表信息出错";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (ttFlag != null && !ttFlag.equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "HDLAAscriptionBL";
                tError.functionName = "dealData";
                tError.errorMessage = " 已做撤销的保单不做归属！";
                this.mErrors.addOneError(tError);
                return false;
            }
            //将之前进行过的个单分配记录变成无效
            if(this.mconttype.equals("1")){
            	System.out.println("………………熊猫…………………");
            String Ascrip = "select agentnew from LAAscription "
                          +" where agentold='"+ tAgentOld
                          +"' and ascripstate='2' and contno='"
                          + tContNo + "' and maketype='01' and validflag='N'";
            ExeSQL aExeSQL = new ExeSQL();
            String flag = aExeSQL.getOneValue(Ascrip);
             if(flag!=null&&!flag.equals(""))
             {   
            	 updateSQL = "update LAAscription set ValidFlag='Y',modifydate='"+ currentDate
                         + "',modifytime='"+ currentTime
                         + "',operator='"+ this.mGlobalInput.Operator
                         + "' where agentold='"+ cLAAscriptionSchema.getAgentOld()+ "'  and ValidFlag='N'"
                         + " and MakeType='01' and ascripstate<>'3' and contno='"+ tContNo + "'";
               mMap.put(updateSQL, "UPDATE");
             }
            }
            else if(this.mconttype.equals("2")){
            	System.out.println("………………大白(●—●)…………………");
            	String tAsrip="select agentnew from laascription "
            		         +" where agentold='"+ tAgentOld
                             +"' and ascripstate='2' and grpcontno='"
                             + tGrpcontNo + "' and maketype='01' and validflag='N'";
            	ExeSQL aExeSQL = new ExeSQL();
                String tflag = aExeSQL.getOneValue(tAsrip);
                 if(tflag!=null&&!tflag.equals(""))
                 {
                	 updateSQL = "update LAAscription set ValidFlag='Y',modifydate='"+ currentDate
                             + "',modifytime='"+ currentTime
                             + "',operator='"+ this.mGlobalInput.Operator
                             + "' where agentold='"+ cLAAscriptionSchema.getAgentOld()+ "'  and ValidFlag='N'"
                             + " and MakeType='01' and ascripstate<>'3' and grpcontno='"+ tGrpcontNo + "'";
                   mMap.put(updateSQL, "UPDATE");
                 }
            }
            //插入
            String mAscripNo = com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo("PerAscripNo", 20);
            cLAAscriptionSchema.setAscripNo(mAscripNo);
            cLAAscriptionSchema.setValidFlag("N");
            cLAAscriptionSchema.setAClass("01");
            cLAAscriptionSchema.setAgentGroup(mAgentGroup);
            cLAAscriptionSchema.setBranchAttr(mBranchAttr);
            cLAAscriptionSchema.setManageCom(mManageCom);
            cLAAscriptionSchema.setMakeType("01");
            cLAAscriptionSchema.setAscriptionDate(currentDate);
            cLAAscriptionSchema.setOperator(mGlobalInput.Operator);
            cLAAscriptionSchema.setMakeDate(currentDate);
            cLAAscriptionSchema.setMakeTime(currentTime);
            cLAAscriptionSchema.setModifyDate(currentDate);
            cLAAscriptionSchema.setModifyTime(currentTime);
            cLAAscriptionSchema.setAscriptionCount(mCountNO);
            cLAAscriptionSchema.setAscripState("2");
            mLAAscriptionSet.add(cLAAscriptionSchema);
       }
        mMap.put(mLAAscriptionSet, "INSERT");
        }
        return true;

    }


    private boolean getInputData(VData cInputData)
    {
        this.cLAAscriptionSchema.setSchema((LAAscriptionSchema) cInputData.getObjectByObjectName("LAAscriptionSchema",0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.querySql = (String) cInputData.get(2);
        return true;
      }
      private boolean prepareOutputData()
      {
          this.mInputData.add(mMap);
          return true;
    }
}
