/*
 * <p>ClassName: AbsenceBL </p>
 * <p>Description: AbsenceBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-07-08
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;
import java.math.BigInteger;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.db.*;

public class LACaseInfoBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 数据操作字符串 */
    private String mOperate;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mCaseNo="";
    /** 业务处理相关变量 */
    private LACaseInfoSchema mLACaseInfoSchema = new LACaseInfoSchema();
    private LACaseInfoSet mLACaseInfoSet = new LACaseInfoSet();
    public LACaseInfoBL()
    {
    }

    public static void main(String[] args)
    {
        LACaseInfoBL BL = new LACaseInfoBL();
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.ComCode = "86";
        tGlobalInput.Operator = "mak001";
        VData tVData = new VData();
        LACaseInfoSchema tLACaseInfoSchema=new LACaseInfoSchema();
        tLACaseInfoSchema.setBranchType("1");
        tLACaseInfoSchema.setCaseDate("2005-05-01");
         tLACaseInfoSchema.setCaseHappenDate("2005-05-01");
         tLACaseInfoSchema.setAgentPutCase("1");
         tLACaseInfoSchema.setAppealNo("1");
         tLACaseInfoSchema.setCaseNo("1");
        tVData.add(tLACaseInfoSchema);
        tVData.add(tGlobalInput);
        BL.submitData(tVData, "INSERT||MAIN");
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if (!check())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAContBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAContBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        System.out.println("Start LACaseInfoBL Submit...");
        LACaseInfoBLS tLACaseInfoBLS = new LACaseInfoBLS();
        tLACaseInfoBLS.submitData(mInputData, cOperate);
        System.out.println("End LACaseInfoBL Submit...");
        //如果有需要处理的错误，则返回
        if (tLACaseInfoBLS.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLACaseInfoBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "LACaseInfoBLS";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;

        return true;
    }


    private String createCaseNo()
    {
        String tCaseNo="";
        tCaseNo=com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo("LACaseInfoNo",20);
        return tCaseNo;
    }

    private boolean check()
    {

        return true;
    }
    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();
      String tAppealNo=this.mLACaseInfoSchema.getAppealNo();
      String sql="select * from LGAppeal where AppealNo='"+tAppealNo+"' and AppealKind='11' ";
        LGAppealDB tLGAppealDB=new LGAppealDB();
        LGAppealSet tLGAppealSet=new LGAppealSet();
        tLGAppealSet=tLGAppealDB.executeQuery(sql);
      if (tLGAppealDB.mErrors.needDealError())
       {
           // @@错误处理
           this.mErrors.copyAllErrors(tLGAppealDB.mErrors);
           CError tError = new CError();
           tError.moduleName = "LACaseInfoBL";
           tError.functionName = "dealData";
           tError.errorMessage = "查询投诉信息失败!";
           this.mErrors.addOneError(tError);
           return false;
       }

        if (mOperate.equals("INSERT||MAIN"))
        {
         //   this.mLAContSchema.setSignDate(currentDate);
            this.mLACaseInfoSchema.setMakeTime(currentTime);
            this.mLACaseInfoSchema.setMakeDate(currentDate);
            this.mLACaseInfoSchema.setModifyTime(currentTime);
            this.mLACaseInfoSchema.setModifyDate(currentDate);
            this.mLACaseInfoSchema.setOperator(mGlobalInput.Operator);
            mCaseNo =createCaseNo();
            if (mCaseNo==null || mCaseNo.equals("") )
            {
                CError tError = new CError();
                tError.moduleName = "LAContBL";
                tError.functionName = "dealData";
                tError.errorMessage = "生成新的案件号失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
          this.mLACaseInfoSchema.setCaseNo(mCaseNo);

        }
        if (mOperate.equals("UPDATE||MAIN"))
        {
             String tCaseNo=this.mLACaseInfoSchema.getCaseNo();
             LACaseInfoSchema tLACaseInfoSchema=new LACaseInfoSchema();
             LACaseInfoDB tLACaseInfoDB=new LACaseInfoDB();
                     tLACaseInfoDB.setCaseNo(tCaseNo);
                 if (!tLACaseInfoDB.getInfo() )
                 {
                     this.mErrors .copyAllErrors(tLACaseInfoDB.mErrors ) ;
                     CError tError = new CError();
                     tError.moduleName = "LACaseInfotBL";
                     tError.functionName = "dealData";
                     tError.errorMessage = "没有编码为"+tCaseNo+"的案件信息!";
                     this.mErrors.addOneError(tError);
                     return false;
                 }
          tLACaseInfoSchema=tLACaseInfoDB.getSchema();
          mLACaseInfoSchema.setMakeDate(tLACaseInfoSchema.getMakeDate());
          mLACaseInfoSchema.setMakeTime(tLACaseInfoSchema.getMakeTime());
          mLACaseInfoSchema.setModifyDate(currentDate);
           mLACaseInfoSchema.setModifyTime(currentTime);

         }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLACaseInfoSchema.setSchema((LACaseInfoSchema) cInputData.
                                     getObjectByObjectName(
                                             "LACaseInfoSchema", 0));

        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));

        return true;
    }


    public VData getResult()
    {
        this.mResult .clear() ;
        this.mResult .add(mCaseNo) ;
        return mResult;
    }

    public String getCaseNo()
    {
        return mCaseNo;
    }


    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(this.mGlobalInput);
            this.mInputData.add(this.mLACaseInfoSchema);
//            System.out.println(deleteLAAuthorizeSet.size());
//            System.out.println(deleteLAAuthorizeSet.get(1).getAuthorObj());
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LACaseInfoBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


}
