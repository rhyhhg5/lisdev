package com.sinosoft.lis.agentdaily;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;
/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public class LAContinueZGAscriptionUI
{
    public CErrors mErrors=new CErrors();
    public VData mResult =new VData();
    private VData mInputData=new VData();
    private String mOperate="";
    public LAContinueZGAscriptionUI()
    {
    }
    public boolean submitData(VData cInputData,String cOperate)
    {
        System.out.println("11111111113244444444444444444444444444444");
        System.out.println();
        mOperate=cOperate;
        this.mInputData =(VData)cInputData.clone() ;
        System.out.println("??------------?????");
        LAContinueZGAscriptionBL tLAContinueZGAscriptionBL = new LAContinueZGAscriptionBL();
        System.out.println("-------------------"+tLAContinueZGAscriptionBL.hashCode());
        try
        {   
            if (!tLAContinueZGAscriptionBL.submitData(mInputData, mOperate))
            {
                this.mErrors.copyAllErrors(tLAContinueZGAscriptionBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAContinueZGAscriptionUI";
                tError.functionName = "submitDat";
                tError.errorMessage = "BL类处理失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        mResult=tLAContinueZGAscriptionBL.getResult();
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

}
