/*
 * <p>ClassName: LABatchAuthorizeBL </p>
 * <p>Description: ALAAuthorizeUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-15
 */
package com.sinosoft.lis.agentdaily;

import java.sql.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class LACompactSplitUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "mak001";
        tG.ComCode = "mak001";
        tG.ManageCom = "8611";

        String tOperate = "INSERT||MAIN"; // 操作类型
        String tGrpContNo = "0000000101"; // 团体保单号

        String[] tAgentCode = {"1102000006","1102000007"};
        String[] tBusiRate = {"60","40"};
        String[] tAppAllotMark = {"Y","Y"};

        LAGrpCommisionDetailSet tLAGrpCommisionDetailSet = new LAGrpCommisionDetailSet();
        for(int i=0;i<tAgentCode.length;i++)
        {
            LAGrpCommisionDetailSchema tLAGrpCommisionDetailSchema = new
                    LAGrpCommisionDetailSchema();
            tLAGrpCommisionDetailSchema.setAgentCode(tAgentCode[i]);
            tLAGrpCommisionDetailSchema.setMngCom("86110000");
            tLAGrpCommisionDetailSchema.setBusiRate(tBusiRate[i]);
            tLAGrpCommisionDetailSchema.setAppAllotMark(tAppAllotMark[i]);
            tLAGrpCommisionDetailSet.add(tLAGrpCommisionDetailSchema);
        }

        VData tVData = new VData();
        tVData.add(tGrpContNo);
        tVData.add(tLAGrpCommisionDetailSet);
        tVData.add(tG);

        LACompactSplitUI tLACompactSplitUI = new LACompactSplitUI();
        if (tLACompactSplitUI.submitData(tVData, tOperate)) {
            System.out.println("成功了！");
        } else {
            System.out.println("失败了！");
        }
    }

    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        this.mInputData = (VData) cInputData.clone();

        LACompactSplitBL tLACompactSplitBL = new LACompactSplitBL();
        System.out.println("Start LABatchAuthorizeBL Submit...");
        tLACompactSplitBL.submitData(mInputData, mOperate);
        System.out.println("End LABatchAuthorizeBL Submit...");

        //如果有需要处理的错误，则返回
        if (tLACompactSplitBL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLACompactSplitBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LABatchAuthorizeUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }
}
