/*
 * <p>ClassName: AdjustCommDataBL </p>
 * <p>Description: AdjustCommDataBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-02-27
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.bl.LABranchGroupBL;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.pubfun.AgentPubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LABranchGroupSchema;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class AdjustCommDataBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
//业务处理相关变量
    private String mOriginBranchAttr = "";
    private String mNewBranchAttr = "";
    private String mOriginBranchCode = "";
    private String mNewBranchCode = "";
    private TransferData mTransferData = new TransferData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    public AdjustCommDataBL()
    {
    }

    private boolean check()
    {

        String sql = "select * from labranchgroup where branchattr='" +
                     this.mOriginBranchAttr + "' and branchlevel='01'  ";
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(sql);
        if (tLABranchGroupSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "AdjustAgentBL";
            tError.functionName = "check";
            tError.errorMessage = "不存在机构号码为'" + mOriginBranchAttr + "'的营业组";
            this.mErrors.addOneError(tError);
            return false;
        }
        sql = "select * from labranchgroup where branchattr='" +
              this.mNewBranchAttr + "' and branchlevel='01'  ";
        tLABranchGroupDB = new LABranchGroupDB();
        tLABranchGroupSet = new LABranchGroupSet();
        tLABranchGroupSet = tLABranchGroupDB.executeQuery(sql);
        if (tLABranchGroupSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "AdjustAgentBL";
            tError.functionName = "check";
            tError.errorMessage = "不存在机构号码为'" + mNewBranchAttr + "'的营业组";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tLABranchGroupDB.getEndFlag() != null &&
            tLABranchGroupDB.getEndFlag().equals("Y"))
        {
            CError tError = new CError();
            tError.moduleName = "AdjustAgentBL";
            tError.functionName = "check";
            tError.errorMessage = "目表机构" + mNewBranchAttr + "已经停业";
            this.mErrors.addOneError(tError);
            return false;

        }

        String tAgentCode = (String)this.mTransferData.getValueByName(
                "AgentCode");
        String tStartDate = (String)this.mTransferData.getValueByName(
                "StartDate");
        System.out.println(tStartDate);
        tStartDate = AgentPubFun.formatDate(tStartDate, "yyyy-MM-dd");
        System.out.println(tStartDate);
        String tCheckResult = AgentPubFun.AdjustCommCheck(tAgentCode,
                tStartDate);
        if (tCheckResult.equals("00"))
        {
            return true;
        }
        else
        {
            CError tError = new CError();
            tError.moduleName = "AdjustAgentBL";
            tError.functionName = "check";
            tError.errorMessage = tCheckResult;
            this.mErrors.addOneError(tError);
            return false;
        }

//      if (!tStartDate.endsWith("01") )//如果开始时间不是从1号开始，则返回错误
//        return false ;
//      String sql="select max(indexcalno) from lawage where AgentCode='"+tAgentCode+"'";
//      ExeSQL tExeSQL=new ExeSQL();
//      String maxIndexCalNo=tExeSQL.getOneValue(sql)  ;
//      System.out.println("---最近发工资的日期---"+maxIndexCalNo) ;
//      if (maxIndexCalNo!=null&&!maxIndexCalNo.equals("") )
//      {
//        String lastDate=PubFun.calDate(tStartDate,-1,"M",null) ;
//        lastDate=AgentPubFun.formatDate(lastDate,"yyyyMM") ;
//        if (maxIndexCalNo.trim() .compareTo(lastDate.trim() )>=0 )
//        {
//          CError tError = new CError();
//          tError.moduleName = "AdjustAgentBL";
//          tError.functionName = "check";
//          tError.errorMessage = "上次发工资是"+maxIndexCalNo.substring(0,4) +"年"+maxIndexCalNo.substring(4).trim()  +"月，因次调整日期必须从这个月的下一个月1号";
//          this.mErrors .addOneError(tError) ;
//          return false;
//        }
//      }

    }

    /**
     传输数据的公共方法
     */

    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("end getInputData!");
        if (!check())
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        AdjustCommDataBLS tAdjustCommDataBLS = new AdjustCommDataBLS();
        System.out.println("Start AdjustCommData BL Submit...");
        tAdjustCommDataBLS.submitData(mInputData, mOperate);
        System.out.println("End AdjustCommData BL Submit...");
        //如果有需要处理的错误，则返回
        if (tAdjustCommDataBLS.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tAdjustCommDataBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "AdjustCommDataBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mInputData = null;
        return true;
    }

    public static void main(String[] args)
    {
        AdjustCommDataUI mAdjustData = new AdjustCommDataUI();
        TransferData mTransferData = new TransferData();
        //\u00CA\u00E4\u00B3\u00F6\u00B2\u00CE\u00CA\u00FD
        CErrors tError = null;
        String tOperate = "";
        String tRela = "";

        GlobalInput tG = new GlobalInput();

        tG.Operator = "aa";
        tG.ComCode = "86";

        String tAgentCode = "8611000001";
        String tManageCom = "86110000";
        String tOriginBranchAttr = "861100000202001001";
        String tNewBranchAttr = "861100000203001001";
//  String tOriginBranchCode = request.getParameter("OriginBranchCode");
//  String tNewBranchCode = request.getParameter("NewBranchCode");
        String tStartDate = "2004-06-01";
        String tEndDate = "2004-06-30";
        mTransferData.setNameAndValue("AgentCode", tAgentCode);
        mTransferData.setNameAndValue("ManageCom", tManageCom);
        mTransferData.setNameAndValue("OriginBranchAttr", tOriginBranchAttr);
        mTransferData.setNameAndValue("NewBranchAttr", tNewBranchAttr);
        mTransferData.setNameAndValue("StartDate", tStartDate);
        mTransferData.setNameAndValue("EndDate", tEndDate);
        VData tVData = new VData();
        tVData.add(tG);
        tVData.addElement(mTransferData);
        System.out.println("add over");
        try
        {
            mAdjustData.submitData(tVData, tOperate);
        }
        catch (Exception ex)
        {
        }

    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(this.mGlobalInput);
            mInputData.add(this.mTransferData);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustCommDataBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = false;
//        String temp = "";
        //查询OriginBranchAttr对应的AgentGroup
//        while(temp.equals(mNewBranchAttr))
//        {
//            temp = temp.equals("")?this.mOriginBranchAttr:mNewBranchAttr;
        LABranchGroupBL tLABranchGroupBL = new LABranchGroupBL();
        LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
        //            tLABranchGroupBL.setBranchAttr(temp);
        tLABranchGroupBL.setBranchAttr(mOriginBranchAttr);
        tLABranchGroupSchema = tLABranchGroupBL.getInfo();
        if (tLABranchGroupBL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "AdjustCommDataBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询原机构的内部编码出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        this.mOriginBranchCode = tLABranchGroupSchema.getAgentGroup();
        tLABranchGroupBL = new LABranchGroupBL();
        tLABranchGroupBL.setBranchAttr(this.mNewBranchAttr);
        tLABranchGroupSchema = tLABranchGroupBL.getInfo();
        if (tLABranchGroupBL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "AdjustCommDataBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询新机构的内部编码出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        this.mNewBranchCode = tLABranchGroupSchema.getAgentGroup();
        this.mTransferData.setNameAndValue("OriginBranchCode",
                                           mOriginBranchCode);
        this.mTransferData.setNameAndValue("NewBranchCode", mNewBranchCode);
        System.out.println("OriginBranchCode:" + mOriginBranchCode);
        System.out.println("NewBranchCode:" + mNewBranchCode);
//        }
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);

        this.mOriginBranchAttr = (String) mTransferData.getValueByName(
                "OriginBranchAttr");
        this.mNewBranchAttr = (String) mTransferData.getValueByName(
                "NewBranchAttr");
        System.out.println("originAttr:" + mOriginBranchAttr);
        System.out.println("newAttr:" + mNewBranchAttr);
        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "AdjustCommDataBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
