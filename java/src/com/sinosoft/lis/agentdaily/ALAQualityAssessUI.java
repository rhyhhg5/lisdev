/*
 * <p>ClassName: ALAQualityAssessUI </p>
 * <p>Description: ALAQualityAssessUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 奖惩管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LAQualityAssessSchema;
import com.sinosoft.lis.vschema.LAQualityAssessSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ALAQualityAssessUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 存放查询结果得容器 */
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAQualityAssessSchema mLAQualityAssessSchema = new
            LAQualityAssessSchema();
    private LAQualityAssessSet mLAQualityAssessSet = new LAQualityAssessSet();
    private String mDoneDate = "";
    private String mManageCom = "";
    public ALAQualityAssessUI()
    {
    }

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        System.out.println("hhhfrrrrrrffhr");
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        ALAQualityAssessBL tALAQualityAssessBL = new ALAQualityAssessBL();
        System.out.println("Start LAQualityAssess UI Submit...");
        tALAQualityAssessBL.submitData(mInputData, mOperate);
        System.out.println("End LAQualityAssess UI Submit...");
        //如果有需要处理的错误，则返回
        if (tALAQualityAssessBL.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tALAQualityAssessBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAQualityAssessUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mOperate.equals("QUERY||MAIN"))
        {
            this.mResult.clear();
            this.mResult = tALAQualityAssessBL.getResult();
        }
        mInputData = null;
        return true;
    }


    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "001";
        tGlobalInput.ManageCom = "861100";
        tGlobalInput.ComCode = "001";

        LAQualityAssessSchema tLAQualityAssessSchema   = new LAQualityAssessSchema();
        tLAQualityAssessSchema.setAgentCode("1101000001");
        tLAQualityAssessSchema.setIdx("");
        tLAQualityAssessSchema.setBranchAttr("8611000001");
        tLAQualityAssessSchema.setAgentGroup("000000000001");
        tLAQualityAssessSchema.setBranchType("1");
        tLAQualityAssessSchema.setBranchType2("01");
        tLAQualityAssessSchema.setManageCom("86110000");
        tLAQualityAssessSchema.setMarkType("1");
        tLAQualityAssessSchema.setMark(2);
        tLAQualityAssessSchema.setMarkRsn("Q00043");
        tLAQualityAssessSchema.setDoneDate("2005-06-18");
        tLAQualityAssessSchema.setOperator("mak001");
        tLAQualityAssessSchema.setNoti("");

        VData tVData = new VData();
        tVData.addElement(tLAQualityAssessSchema);
        tVData.add(tLAQualityAssessSchema.getDoneDate());
        tVData.add("86110000");
        tVData.add(tGlobalInput);

        ALAQualityAssessUI tALAQualityAssessUI = new ALAQualityAssessUI();
        tALAQualityAssessUI.submitData(tVData,"INSERT||MAIN");
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(this.mGlobalInput);
            mInputData.add(this.mDoneDate);
            mInputData.add(this.mManageCom);
            mInputData.add(this.mLAQualityAssessSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAQualityAssessUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("hhhfffhr");
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        System.out.println("hhhhr");
        mDoneDate = (String) cInputData.get(1);
        mManageCom = (String) cInputData.get(2);
        this.mLAQualityAssessSchema.setSchema((LAQualityAssessSchema) cInputData.
                                             getObjectByObjectName(
                "LAQualityAssessSchema", 0));
        if (mGlobalInput == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAQualityAssessUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("hhhfffhr");
        return true;
    }


    public VData getResult()
    {
        return this.mResult;
    }
}
