/*
 * <p>ClassName: OLAArchieveBL </p>
 * <p>Description: OLAArchieveBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-20 18:05:58
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.Reflections;


public class LAContFYCRateBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String currentDate;
    private String currentTime;
    /** 业务处理相关变量 */
    private LAContFYCRateSchema mLAContFYCRateSchema = new LAContFYCRateSchema();
    private LAContFYCRateSet mLAContFYCRateSet = new LAContFYCRateSet();
    private LAContFYCRateSet mupLAContFYCRateSet = new LAContFYCRateSet();
    private LAContFYCRateSet mInsLAContFYCRateSet= new LAContFYCRateSet();
    private LAContFYCRateBSet mInsLAContFYCRateBSet= new LAContFYCRateBSet();
    private LACommisionSet mInLACommisionSet = new LACommisionSet();
    private LACommisionBSet mInLACommisionBSet = new LACommisionBSet(); 
    private MMap mMap = new MMap();
//private LAArchieveSet mLAArchieveSet=new LAArchieveSet();
    public LAContFYCRateBL()
    {
    }

    public static void main(String[] args)
    {
        LAContFYCRateSchema tLAContFYCRateSchema = new LAContFYCRateSchema();
        LAContFYCRateSet tupLAContFYCRateSet = new LAContFYCRateSet();
        tLAContFYCRateSchema.setGrpContNo("0000031701");
        tLAContFYCRateSchema.setRiskCode("1605");
        tLAContFYCRateSchema.setRate("0.8");
        tupLAContFYCRateSet.add(tLAContFYCRateSchema);
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86";
        VData tVData = new VData();
        tVData.addElement(tG);

        tVData.addElement(tupLAContFYCRateSet);
        LAContFYCRateBL tLAContFYCRateBL = new LAContFYCRateBL();
        tLAContFYCRateBL.submitData(tVData, "DELETE||MAIN");

    }
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
//       System.out.println("transact"+transact);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAContFYCRateBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAContFYCRateBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start LARateStandPremBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("Start LARateStandPremBL Submit...");
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LARateStandPremBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        currentDate = PubFun.getCurrentDate();
        currentTime = PubFun.getCurrentTime();
        System.out.print(mOperate+"|||||||||||||||||12122");
        if (this.mOperate.equals("INSERT||MAIN") )
        {

            if (!saveData())
            {
                return false;
            }
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            if (!upDateData())
            {
                return false;
            }
        }
        if (this.mOperate.equals("DELETE||MAIN"))
        {
            if(!deleteData())
            {
                return false;
            }
        }

        return true;
    }
    private boolean saveData()
    {
        String tGrpContNo=mLAContFYCRateSet.get(1).getGrpContNo();
        LAContFYCRateDB tLAContFYCRateDB=new LAContFYCRateDB();
        tLAContFYCRateDB.setGrpContNo(tGrpContNo);
        mupLAContFYCRateSet=tLAContFYCRateDB.query();
         System.out.print(mLAContFYCRateSet.size()+"|||111");
         System.out.print(mupLAContFYCRateSet.size());

        if(mupLAContFYCRateSet.size()>0)
        {
            CError tError = new CError();
            tError.moduleName = "LAContFYCRateBL";
            tError.functionName = "dealdata";
            tError.errorMessage = tGrpContNo+ "的信息已存在，如要修改它的信息，请先查询并修点击改!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.print("|||"+mLAContFYCRateSet.size());
        for (int i = 1; i <= mLAContFYCRateSet.size(); i++)
        {
            System.out.print("|||||||"+mLAContFYCRateSet.size());
            LAContFYCRateSchema tLAContFYCRateSchema = new LAContFYCRateSchema();
            tLAContFYCRateSchema = mLAContFYCRateSet.get(i);

            //判断 该保单是否已计算佣金
            LACommisionDB tLACommisionDB = new LACommisionDB();
            LACommisionSet tLACommisionSet = new LACommisionSet();
            tLACommisionDB.setGrpContNo(tGrpContNo);
            tLACommisionDB.setRiskCode(tLAContFYCRateSchema.getRiskCode());
            tLACommisionSet=tLACommisionDB.query();
            if (tLACommisionSet.size()>0)
            {
                for(int k=1;k<=tLACommisionSet.size();k++)
                {
                    LACommisionSchema tLACommisionSchema = new LACommisionSchema();
                    tLACommisionSchema=tLACommisionSet.get(k);
                    String tSQL = "select MakeDate from lawage where "
                                  + " IndexCalNo = '" +
                                  tLACommisionSchema.getWageNo() + "'"
                                  + " and agentcode = '" +
                                  tLACommisionSchema.getAgentCode() + "'";
                    ExeSQL tExeSQL = new ExeSQL();
                    String tMakeDate = tExeSQL.getOneValue(tSQL);
                    if (tExeSQL.mErrors.needDealError())
                    {
                        this.mErrors.copyAllErrors(tExeSQL.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "LAContFYCRateBL";
                        tError.functionName = "upDateData";
                        tError.errorMessage = "查询指标信息表的佣金指标出错！";
                        this.mErrors.addOneError(tError);
                        return false;
                     }
                    if(tMakeDate!=null  && !tMakeDate.equals(""))
                    {
                        /**
                        CError tError = new CError();
                        tError.moduleName = "LAContFYCRateBL";
                        tError.functionName = "upDateData";
                        tError.errorMessage = "该保单信息已进行过薪资计算，无法录入或修改该笔保单提奖比例信息，如需调整，请上报总公司销售管理部审批！";
                        this.mErrors.addOneError(tError);
                         return false;
                         */
                      continue;
                    }
                    else
                   {
                    double tRate=tLAContFYCRateSchema.getRate();
                     LACommisionSchema tPerLACommisionSchema = new LACommisionSchema();
                     tPerLACommisionSchema = tLACommisionSchema.getSchema();
                     
                     LACommisionBSchema tLACommisionBSchema = new LACommisionBSchema();  
                	  Reflections tReflections = new Reflections();
                    tReflections.transFields(tLACommisionBSchema, tPerLACommisionSchema);
                    String mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
                    tLACommisionBSchema.setEdorNo(mEdorNo);
                    tLACommisionBSchema.setEdorType("03");
                    mInLACommisionBSet.add(tLACommisionBSchema);
                     
                     tPerLACommisionSchema.setFYCRate(tRate);
                     tPerLACommisionSchema.setStandFYCRate(tRate);
                     tPerLACommisionSchema.setFYC(tLACommisionSchema.getTransMoney()*tRate);
                       tPerLACommisionSchema.setDirectWage(tLACommisionSchema.getTransMoney()*tRate);
                       tPerLACommisionSchema.setModifyDate(currentDate);
                     tPerLACommisionSchema.setModifyTime(currentTime);
                     tPerLACommisionSchema.setOperator(mGlobalInput.Operator);
                     mInLACommisionSet.add(tPerLACommisionSchema);
                     

                     
                     tLAContFYCRateSchema.setFlag("Y");
                       }

                }
            }
            String currentDate = PubFun.getCurrentDate();
            String currentTime = PubFun.getCurrentTime();
            tLAContFYCRateSchema.setMakeDate(currentDate);
            tLAContFYCRateSchema.setMakeTime(currentTime);
            tLAContFYCRateSchema.setModifyDate(currentDate);
            tLAContFYCRateSchema.setModifyTime(currentTime);
            tLAContFYCRateSchema.setOperator(mGlobalInput.Operator);
            mInsLAContFYCRateSet.add(tLAContFYCRateSchema) ;
         }
         System.out.print("||eeee|"+mLAContFYCRateSet.size());
         mMap.put(this.mInsLAContFYCRateSet, "INSERT");
         this.mMap.put(this.mInLACommisionSet, "UPDATE");
         this.mMap.put(this.mInLACommisionBSet, "INSERT");
           return true;
    }
    private boolean upDateData()
    {
        String tEdorNo = PubFun1.CreateMaxNo("FYCRateEdorNo", 20);
        String tGrpContNo=mLAContFYCRateSet.get(1).getGrpContNo();
        LAContFYCRateDB tLAContFYCRateDB=new LAContFYCRateDB();
        tLAContFYCRateDB.setGrpContNo(tGrpContNo);
        mupLAContFYCRateSet=tLAContFYCRateDB.query();
        if(mupLAContFYCRateSet.size()<=0)
        {
            CError tError = new CError();
            tError.moduleName = "LAContFYCRateBL";
            tError.functionName = "dealdata";
            tError.errorMessage = "查询不到" +tGrpContNo+ "的机构信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        for (int i = 1; i <= mLAContFYCRateSet.size(); i++)
        {
            LAContFYCRateSchema tLAContFYCRateSchema = new LAContFYCRateSchema();
            tLAContFYCRateSchema = mLAContFYCRateSet.get(i);
            //判断 该保单是否已计算佣金
            LACommisionDB tLACommisionDB = new LACommisionDB();
            LACommisionSet tLACommisionSet = new LACommisionSet();
            tLACommisionDB.setGrpContNo(tGrpContNo);
            tLACommisionDB.setRiskCode(tLAContFYCRateSchema.getRiskCode());
            tLACommisionSet=tLACommisionDB.query();
            if (tLACommisionSet.size()>0)
            {
                for(int k=1;k<=tLACommisionSet.size();k++)
                {
                    LACommisionSchema tLACommisionSchema = new LACommisionSchema();
                    tLACommisionSchema=tLACommisionSet.get(k);
                    String tSQL = "select MakeDate from lawage where "
                                  + " IndexCalNo = '" +
                                  tLACommisionSchema.getWageNo() + "'"
                                  + " and agentcode = '" +
                                  tLACommisionSchema.getAgentCode() + "'";
                    ExeSQL tExeSQL = new ExeSQL();
                    String tMakeDate = tExeSQL.getOneValue(tSQL);
                    if (tExeSQL.mErrors.needDealError())
                    {
                        this.mErrors.copyAllErrors(tExeSQL.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "LAContFYCRateBL";
                        tError.functionName = "upDateData";
                        tError.errorMessage = "查询指标信息表的佣金指标出错！";
                        this.mErrors.addOneError(tError);
                        return false;
                     }
                    if(tMakeDate!=null  && !tMakeDate.equals(""))
                    {
                        /**
                        CError tError = new CError();
                        tError.moduleName = "LAContFYCRateBL";
                        tError.functionName = "upDateData";
                        tError.errorMessage = "该保单信息已进行过薪资计算，无法录入或修改该笔保单提奖比例信息，如需调整，请上报总公司销售管理部审批！";
                        this.mErrors.addOneError(tError);
                        return false;*/
                     continue;
                    }
                    else
                   {
                       double tRate=tLAContFYCRateSchema.getRate();
                     LACommisionSchema tPerLACommisionSchema = new LACommisionSchema();
                     tPerLACommisionSchema = tLACommisionSchema.getSchema();
                     
                     LACommisionBSchema tLACommisionBSchema = new LACommisionBSchema();  
                	  Reflections tReflections = new Reflections();
                    tReflections.transFields(tLACommisionBSchema, tPerLACommisionSchema);
                    String mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
                    tLACommisionBSchema.setEdorNo(mEdorNo);
                    tLACommisionBSchema.setEdorType("03");
                    mInLACommisionBSet.add(tLACommisionBSchema);
                     
                     tPerLACommisionSchema.setFYCRate(tRate);
                     tPerLACommisionSchema.setStandFYCRate(tRate);
                     tPerLACommisionSchema.setFYC(tLACommisionSchema.getTransMoney()*tRate);
                       tPerLACommisionSchema.setDirectWage(tLACommisionSchema.getTransMoney()*tRate);
                       tPerLACommisionSchema.setModifyDate(currentDate);
                     tPerLACommisionSchema.setModifyTime(currentTime);
                     tPerLACommisionSchema.setOperator(mGlobalInput.Operator);
                     mInLACommisionSet.add(tPerLACommisionSchema);
                     

                     
                     tLAContFYCRateSchema.setFlag("Y");
                       }

                }
            }
            tLAContFYCRateSchema.setMakeDate(currentDate);
            tLAContFYCRateSchema.setMakeTime(currentTime);
            tLAContFYCRateSchema.setModifyDate(currentDate);
            tLAContFYCRateSchema.setModifyTime(currentTime);
            tLAContFYCRateSchema.setOperator(mGlobalInput.Operator);
            mInsLAContFYCRateSet.add(tLAContFYCRateSchema) ;
         }
        // 备份数据
         for (int i = 1; i <= mupLAContFYCRateSet.size(); i++)
         {
             LAContFYCRateBSchema tLAContFYCRateBSchema = new
                     LAContFYCRateBSchema();
             Reflections tReflections = new Reflections();
             System.out.println("12222222222222222222222222"+mupLAContFYCRateSet.size());
             tReflections.transFields(tLAContFYCRateBSchema, mupLAContFYCRateSet.get(i));
             tLAContFYCRateBSchema.setEdorNo(tEdorNo);
             tLAContFYCRateBSchema.setOriOperator(tLAContFYCRateBSchema.getOperator());
             tLAContFYCRateBSchema.setOriMakeDate(tLAContFYCRateBSchema.
                                                  getMakeDate());
             tLAContFYCRateBSchema.setOriMakeTime(tLAContFYCRateBSchema.
                                                  getMakeTime());
             tLAContFYCRateBSchema.setOriModifyDate(tLAContFYCRateBSchema.
                                                  getModifyDate());
             tLAContFYCRateBSchema.setOriModifyTime(tLAContFYCRateBSchema.
                                                  getModifyTime());
             tLAContFYCRateBSchema.setOperator(mGlobalInput.Operator);
             tLAContFYCRateBSchema.setMakeDate(currentDate);
             tLAContFYCRateBSchema.setMakeTime(currentTime);
             tLAContFYCRateBSchema.setModifyDate(currentDate);
             tLAContFYCRateBSchema.setModifyTime(currentTime);
             mInsLAContFYCRateBSet.add(tLAContFYCRateBSchema) ;
         }
         mMap.put(this.mupLAContFYCRateSet, "DELETE");
         mMap.put(this.mInsLAContFYCRateBSet, "INSERT");
         mMap.put(this.mInsLAContFYCRateSet, "INSERT");
         mMap.put(this.mInLACommisionSet, "UPDATE");
         mMap.put(this.mInLACommisionBSet, "INSERT");
         return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        String tEdorNo = PubFun1.CreateMaxNo("FYCRateEdorNo", 20);
        String tGrpContNo=mLAContFYCRateSet.get(1).getGrpContNo();
        LAContFYCRateDB tLAContFYCRateDB=new LAContFYCRateDB();
        tLAContFYCRateDB.setGrpContNo(tGrpContNo);
        mupLAContFYCRateSet=tLAContFYCRateDB.query();
        if(mupLAContFYCRateSet.size()<=0)
        {
            CError tError = new CError();
            tError.moduleName = "LAContFYCRateBL";
            tError.functionName = "dealdata";
            tError.errorMessage = "查询不到" +tGrpContNo+ "的机构信息!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //判断 该保单是否已计算佣金
        for(int i=1;i<=mupLAContFYCRateSet.size();i++)
        {
        LAContFYCRateSchema tLAContFYCRateSchema = new LAContFYCRateSchema();
        tLAContFYCRateSchema=mupLAContFYCRateSet.get(i);
        LACommisionDB tLACommisionDB = new LACommisionDB();
        LACommisionSet tLACommisionSet = new LACommisionSet();
        tLACommisionDB.setGrpContNo(tGrpContNo);
        tLACommisionDB.setRiskCode(tLAContFYCRateSchema.getRiskCode());
        tLACommisionSet=tLACommisionDB.query();
        if (tLACommisionSet.size()>0)
        {
            for(int k=1;k<=tLACommisionSet.size();k++)
            {
                LACommisionSchema tLACommisionSchema = new LACommisionSchema();
                String tSQL = "select MakeDate from lawage where "
                              + " IndexCalNo = '" +
                              tLACommisionSchema.getWageNo() + "'"
                              + " and agentcode = '" +
                              tLACommisionSchema.getAgentCode() + "'";
                ExeSQL tExeSQL = new ExeSQL();
                String tMakeDate = tExeSQL.getOneValue(tSQL);
                if (tExeSQL.mErrors.needDealError())
                {
                    this.mErrors.copyAllErrors(tExeSQL.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "LAContFYCRateBL";
                    tError.functionName = "upDateData";
                    tError.errorMessage = "查询指标信息表的佣金指标出错！";
                    this.mErrors.addOneError(tError);
                    return false;
                 }
                if(tMakeDate!=null  && !tMakeDate.equals(""))
                {
                        /**
                          CError tError = new CError();
                          tError.moduleName = "LAContFYCRateBL";
                          tError.functionName = "upDateData";
                          tError.errorMessage = "该保单信息已进行过薪资计算，无法录入或修改该笔保单提奖比例信息，如需调整，请上报总公司销售管理部审批！";
                          this.mErrors.addOneError(tError);
                           return false;
                           */
                        continue;

                }
            }
            }
        }
        // 备份数据
        System.out.println("12222222222222222222222222");
 //        LAContFYCRateBSet tLAContFYCRateBSet = new  LAContFYCRateBSet();
         System.out.println("12222222222222222222222222");
         for (int i = 1; i <= mupLAContFYCRateSet.size(); i++)
         {

             LAContFYCRateBSchema tLAContFYCRateBSchema = new
                     LAContFYCRateBSchema();
             Reflections tReflections = new Reflections();
             System.out.println("12222222222222222222222222"+mupLAContFYCRateSet.size());
             tReflections.transFields(tLAContFYCRateBSchema, mupLAContFYCRateSet.get(i));
             tLAContFYCRateBSchema.setEdorNo(tEdorNo);
             tLAContFYCRateBSchema.setOriOperator(tLAContFYCRateBSchema.getOperator());
             tLAContFYCRateBSchema.setOriMakeDate(tLAContFYCRateBSchema.
                                                  getMakeDate());
             tLAContFYCRateBSchema.setOriMakeTime(tLAContFYCRateBSchema.
                                                  getMakeTime());
             tLAContFYCRateBSchema.setOriModifyDate(tLAContFYCRateBSchema.
                                                  getModifyDate());
             tLAContFYCRateBSchema.setOriModifyTime(tLAContFYCRateBSchema.
                                                  getModifyTime());
             tLAContFYCRateBSchema.setOperator(mGlobalInput.Operator);
             tLAContFYCRateBSchema.setMakeDate(currentDate);
             tLAContFYCRateBSchema.setMakeTime(currentTime);
             tLAContFYCRateBSchema.setModifyDate(currentDate);
             tLAContFYCRateBSchema.setModifyTime(currentTime);
             mInsLAContFYCRateBSet.add(tLAContFYCRateBSchema) ;
             System.out.println("12222222222222222222222222");
         }
        mMap.put(this.mInsLAContFYCRateBSet, "INSERT");
        mMap.put(this.mupLAContFYCRateSet, "DELETE");
        return true;
    }

    public VData getResult()
    {
        this.mResult.clear();
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLAContFYCRateSet.set((LAContFYCRateSet) cInputData.
                                getObjectByObjectName("LAContFYCRateSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        LAContFYCRateDB tLAContFYCRateDB = new LAContFYCRateDB();
        int i;
        for (i = 0; i < mLAContFYCRateSet.size(); i++)
        {

            tLAContFYCRateDB.setSchema(this.mLAContFYCRateSet.get(i));
            //如果有需要处理的错误，则返回
            if (tLAContFYCRateDB.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAContFYCRateDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAContFYCRateDB";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mInputData = null;
        }
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAContFYCRateBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    }

