/*
 * <p>ClassName: LAAddReduceBL </p>
 * <p>Description: LAAddReduceBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2006-07-13
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.db.LARewardPunishDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LARewardPunishBSchema;
import com.sinosoft.lis.schema.LARewardPunishSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;

public class LAMeAddReduceBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    
    private int mMaxID;
    
    // 管理机构
    private String mManageCom;
    
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();

    /** 数据封装集合 */
    private MMap mMap=new MMap();
    
    private LARewardPunishSchema mLARewardPunishSchema = new LARewardPunishSchema();
    private LARewardPunishSchema mupLARewardPunishSchema = new LARewardPunishSchema();//修改存储schema
    
    private LARewardPunishBSchema mLARewardPunishBSchema = new LARewardPunishBSchema();

    public LAMeAddReduceBL()
    {
    }

    public static void main(String[] args)
    {

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        if(!check())
        {
        	return false;
        }
        if(!dealData())
        {
        	return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start AgentWageCalSaveNewBL Submit...");
        if (!tPubSubmit.submitData(mInputData,""))
          {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "AgentWageCalSaveNewBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
          }
        mInputData = null;
        return true;
    }


    private boolean check() //已经计算过佣金的日期不能操作
    {
    	String tManageCom = mLARewardPunishSchema.getManageCom();
    	String twageno = mLARewardPunishSchema.getWageNo();
    	double tMoney = mLARewardPunishSchema.getMoney();
    	
    	// 注:校验金额不能小于0
    	if(tMoney<0)
    	{
   		  CError tError = new CError();
          tError.moduleName = "LAMeAddReduceBL";
          tError.functionName = "check";
          tError.errorMessage = "页面输入金额不能小于0";
          this.mErrors.addOneError(tError);
          return false;		
    	}
    	
    	// Check1 存的是所属年份 ---- 用于校验
    	this.mLARewardPunishSchema.setCheck1(twageno.substring(0,4));
    	
    	String check_wage = "select max(indexcalno) from lawage where " +
    			"managecom = '"+tManageCom+"' and branchtype = '2' " +
    					"and branchtype2 = '02' and indexcalno >='"+twageno+"'";
	//String check_wage="select state from lawagehistory where " +
		//"managecom = '"+tManageCom+"' and branchtype = '2' " +
		//"and branchtype2 = '01'  and wageno ='"+twageno+"'";

    	ExeSQL tExeSQL = new ExeSQL();
    	String tMaxWage =tExeSQL.getOneValue(check_wage); 
    	if(tMaxWage!=null&&tMaxWage!="")
    	{
    		 CError tError = new CError();
             tError.moduleName = "LAMeAddReduceBL";
             tError.functionName = "check";
             tError.errorMessage = "支公司薪资月为"+tMaxWage+"业务规则加扣款可操作执行年月必须大于最大计算薪资月！";
             this.mErrors.addOneError(tError);
             return false;
    	}
    	
    	/**********************************************************
    	 * 注:校验同一年内，所有的已经录入的加扣款项目和必须小于等于0，不能大于0
    	 *    对于删除操作 需特殊处理
    	 **********************************************************/
    	if(mOperate.equals("DELETE||MAIN"))
    	{
    		tMoney = 0;
    	}
    	System.out.println("前台传进来的money值为"+tMoney);
    	// 修改点
//    	String check_ADD = "select GETADDREDUCE("+tMoney+",'"
//        + mLARewardPunishSchema.getAClass() + "','"
//        + mLARewardPunishSchema.getCheck1() + "','"
//        + mLARewardPunishSchema.getAgentCode() + "','"
//        + mLARewardPunishSchema.getDoneFlag()+"','"
//        + mLARewardPunishSchema.getWageNo()+"') from dual ";
//    	
//    	System.out.println("校验加扣款项目总和SQL:"+check_ADD);
//    	ExeSQL checkADD_ExeSQL = new ExeSQL();
//    	String tResult = checkADD_ExeSQL.getOneValue(check_ADD);
//    	if(tResult.equals("1"))
//    	{
//   		  CError tError = new CError();
//          tError.moduleName = "LAAddReduceBL";
//          tError.functionName = "check";
//          tError.errorMessage = "一年之内，该人员所有已经录入的加扣款项目和必须小于等于0，不能大于0";
//          this.mErrors.addOneError(tError);
//          return false;	
//    	}
    	if("07".equals(mLARewardPunishSchema.getAClass())||"17".equals(mLARewardPunishSchema.getAClass()))
    	{
    		String check_OtherADD = "select GETFYCADDREDUCE("+tMoney+",'"
            + mLARewardPunishSchema.getAClass() + "','"
            + mLARewardPunishSchema.getCheck1() + "','"
            + mLARewardPunishSchema.getAgentCode() + "','"
            + mLARewardPunishSchema.getDoneFlag()+"','"
            + mLARewardPunishSchema.getWageNo()+"') from dual ";
        	
        	System.out.println("校验其他类型加扣款项目总和SQL:"+check_OtherADD);
        	String tResult = tExeSQL.getOneValue(check_OtherADD);
        	if(tResult.equals("1"))
        	{
       		  CError tError = new CError();
              tError.moduleName = "LAMeAddReduceBL";
              tError.functionName = "check";
              tError.errorMessage = "最近12个月内的绩效扣款和最近8个月内的绩效加款之和不能大于0";
              this.mErrors.addOneError(tError);
              return false;	
        	}     		
    	}
    	if("01".equals(mLARewardPunishSchema.getAClass())||"11".equals(mLARewardPunishSchema.getAClass()))
    	{
    		String check_FycADD = "select GETFYCADDREDUCE("+tMoney+",'"
            + mLARewardPunishSchema.getAClass() + "','"
            + mLARewardPunishSchema.getCheck1() + "','"
            + mLARewardPunishSchema.getAgentCode() + "','"
            + mLARewardPunishSchema.getDoneFlag()+"','"
            + mLARewardPunishSchema.getWageNo()+"') from dual ";
        	
        	System.out.println("校验提奖类型加扣款项目总和SQL:"+check_FycADD);
        	String tResult = tExeSQL.getOneValue(check_FycADD);
        	if(tResult.equals("1"))
        	{
       		  CError tError = new CError();
              tError.moduleName = "LAMeAddReduceBL";
              tError.functionName = "check";
              tError.errorMessage = "最近12个月内的提奖扣款和最近8个月内的提奖加款之和不能大于0 ";
              this.mErrors.addOneError(tError);
              return false;	
        	}     		
    	}
    	
    	/*****************************************************
    	 * 注: 同一个薪资月不能插入2条加扣款类型一样的数据 
    	 * 
    	 *****************************************************/
        if(mOperate.equals("INSERT||MAIN"))
        {
      	  String check_count = "select (select codename from ldcode where codetype = 'grpmoney' and code = LARewardPunish.aclass)" +
      	  		"from LARewardPunish where agentcode = '"+mLARewardPunishSchema.getAgentCode()+"'" +
      	  		" and wageno = '"+mLARewardPunishSchema.getWageNo()+"' and aclass = '"+mLARewardPunishSchema.getAClass()+"'";
      	  
      	  System.out.println("同一个薪资月不能重复录入相同类型加扣款检验SQL如下:"+check_count);
          ExeSQL check_ExeSQL = new ExeSQL();
          String error_Mess = check_ExeSQL.getOneValue(check_count);
      	  if(error_Mess!=null&&error_Mess!="")
    	   {
    		 CError tError = new CError();
             tError.moduleName = "LAMeAddReduceBL";
             tError.functionName = "check";
             tError.errorMessage = "同一个执行年月,每个业务员只能录入一条加扣款类型为"+error_Mess+"的数据";
             this.mErrors.addOneError(tError);
             return false;
    	   }     	
        }
        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
      if(mOperate.equals("INSERT||MAIN"))
      {
    	  getMaxID();//得到最大IDX号
    	  mLARewardPunishSchema.setIdx(mMaxID);
    	  mLARewardPunishSchema.setMakeDate(currentDate);
    	  mLARewardPunishSchema.setMakeTime(currentTime);
    	  mLARewardPunishSchema.setModifyDate(currentDate);
    	  mLARewardPunishSchema.setModifyTime(currentTime);
    	  mLARewardPunishSchema.setOperator(mGlobalInput.Operator);
    	  mMap.put(mLARewardPunishSchema, "INSERT");  
      }
      if(mOperate.equals("UPDATE||MAIN"))
      {
    	  LARewardPunishDB tLARewardPunishDB = new LARewardPunishDB();
    	  tLARewardPunishDB.setAgentCode(mLARewardPunishSchema.getAgentCode());
    	  tLARewardPunishDB.setIdx(mLARewardPunishSchema.getIdx());
    	  if(!tLARewardPunishDB.getInfo())
    	  {
    		  CError tError = new CError();
              tError.moduleName = "LAMeAddReduceBL";
              tError.functionName = "dealdate";
              tError.errorMessage = "未找到所需修改数据详细信息";
              this.mErrors.addOneError(tError);
              return false; 
    	  }
    	  mupLARewardPunishSchema = new LARewardPunishSchema();
    	  mupLARewardPunishSchema=tLARewardPunishDB.getSchema();//--获取备份数据
    	  
    	  // 备份B表数据
    	 mLARewardPunishBSchema = new LARewardPunishBSchema();
    	 Reflections tReflections1 = new Reflections();
    	 tReflections1.transFields(mLARewardPunishBSchema, mupLARewardPunishSchema);
    	 mLARewardPunishBSchema.setOperator1(mupLARewardPunishSchema.getOperator());
    	 mLARewardPunishBSchema.setModifyDate1(mupLARewardPunishSchema.getModifyDate());
    	 mLARewardPunishBSchema.setModifyTime1(mupLARewardPunishSchema.getModifyTime());
    	 mLARewardPunishBSchema.setMakeDate1(mupLARewardPunishSchema.getMakeDate());
    	 mLARewardPunishBSchema.setMakeTime1(mupLARewardPunishSchema.getMakeTime());
    	 
    	 mLARewardPunishBSchema.setOperator(mGlobalInput.Operator);//**
    	 mLARewardPunishBSchema.setMakeDate(currentDate);
    	 mLARewardPunishBSchema.setMakeTime(currentTime);
    	 mLARewardPunishBSchema.setModifyDate(currentDate);
    	 mLARewardPunishBSchema.setModifyTime(currentTime);//PubFun1.CreateMaxNo("EdorNo", 19)
    	 mLARewardPunishBSchema.setEdorNo(PubFun1.CreateMaxNo("EdorNo", 19));
    	 mLARewardPunishBSchema.setEdortype("01");//  01 表示修改操作
    	 mMap.put(mLARewardPunishBSchema, "INSERT");
  
    	 mLARewardPunishSchema.setModifyDate(currentDate);
    	 mLARewardPunishSchema.setModifyTime(currentTime);
    	 mLARewardPunishSchema.setOperator(mGlobalInput.Operator);
    	 mMap.put(mLARewardPunishSchema, "UPDATE"); 
      }
      if(mOperate.equals("DELETE||MAIN"))
      {
    	  LARewardPunishDB tLARewardPunishDB = new LARewardPunishDB();
    	  tLARewardPunishDB.setAgentCode(mLARewardPunishSchema.getAgentCode());
    	  tLARewardPunishDB.setIdx(mLARewardPunishSchema.getIdx());
    	  if(!tLARewardPunishDB.getInfo())
    	  {
    		  CError tError = new CError();
              tError.moduleName = "LAAddSubPerBL";
              tError.functionName = "dealdate";
              tError.errorMessage = "未找到所需删除数据详细信息";
              this.mErrors.addOneError(tError);
              return false; 
    	  }
    	  mLARewardPunishSchema=tLARewardPunishDB.getSchema();
    	  
    	// 备份B表数据
    	 mLARewardPunishBSchema = new LARewardPunishBSchema();
     	 Reflections tReflections1 = new Reflections();
     	 tReflections1.transFields(mLARewardPunishBSchema, mLARewardPunishSchema);
     	 mLARewardPunishBSchema.setOperator1(mLARewardPunishSchema.getOperator());
     	 mLARewardPunishBSchema.setModifyDate1(mLARewardPunishSchema.getModifyDate());
     	 mLARewardPunishBSchema.setModifyTime1(mLARewardPunishSchema.getModifyTime());
     	 mLARewardPunishBSchema.setMakeDate1(mLARewardPunishSchema.getMakeDate());
     	 mLARewardPunishBSchema.setMakeTime1(mLARewardPunishSchema.getMakeTime());
     	 
     	 mLARewardPunishBSchema.setOperator(mGlobalInput.Operator);//**
     	 mLARewardPunishBSchema.setMakeDate(currentDate);
     	 mLARewardPunishBSchema.setMakeTime(currentTime);
     	 mLARewardPunishBSchema.setModifyDate(currentDate);
     	 mLARewardPunishBSchema.setModifyTime(currentTime);//PubFun1.CreateMaxNo("EdorNo", 19)
     	 mLARewardPunishBSchema.setEdorNo(PubFun1.CreateMaxNo("EdorNo", 19));
     	 mLARewardPunishBSchema.setEdortype("02");//  02 表示删除操作
     	 
     	 mMap.put(mLARewardPunishBSchema, "INSERT");
    	 mMap.put(mLARewardPunishSchema, "DELETE");  
    	  
      }
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
    	
    	this.mLARewardPunishSchema.setSchema((LARewardPunishSchema)cInputData.getObjectByObjectName("LARewardPunishSchema", 0));
    	this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
    	
        return true;
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
      
        return true;
    }


    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(mMap);	
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAAddSubPerBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "LAAddSubPerBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    /**
     * 注:LARewardPunish 表主键为 ：agentcode 和 idx  
     * 避免插入过程中主键冲突,插入时候 生成idx
     */
    private int getMaxID()
    {
    	String getMaxID_SQL = "select max(idx) from LARewardPunish where agentcode = '"+mLARewardPunishSchema.getAgentCode()+"' ";
    	ExeSQL tExeSQL = new ExeSQL();
    	
    	System.out.println("最大号查询SQL"+getMaxID_SQL);
    	String idx =tExeSQL.getOneValue(getMaxID_SQL);
    	if(idx==null||idx.equals(""))
    	{
    		mMaxID=1;
    	}else{
    		mMaxID=Integer.parseInt(idx)+1;
    	}
    	System.out.println("生成最大号为："+mMaxID);
    	return mMaxID;
    	
    }
}
