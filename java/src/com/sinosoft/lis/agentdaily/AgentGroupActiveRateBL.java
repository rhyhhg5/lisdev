package com.sinosoft.lis.agentdaily;


import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.StrTool;
import java.text.DecimalFormat;

public class AgentGroupActiveRateBL{
      /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private VData mResult = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    private String mOperate="";
    private String mManageCom="";
    private String mAgentCom="";
    private String mWageNo="";
    private String mBranchType="";
    private String mBranchType2="";
    private String mAgentComName="";
    private String mAgentName="";
    private String mAgentCode="";
    private String mGroupAgentCode="";

    private MMap mMap=new MMap();
    public static void main(String[] args)
    {
    }
    public AgentGroupActiveRateBL()
    {
    }

    private boolean getInputData(VData cInputData) {
        try {
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);

            if (mGlobalInput == null || mTransferData==null) {
                CError tError = new CError();
                tError.moduleName = "AgentGroupActiveRateBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有得到足够的信息！";
                this.mErrors.addOneError(tError);
                return false;
            }

            this.mManageCom = (String) mTransferData.getValueByName("ManageCom");
            this.mAgentCom = (String) mTransferData.getValueByName("AgentCom");
            this.mWageNo = (String) mTransferData.getValueByName("WageNo");
            this.mBranchType= (String) mTransferData.getValueByName("BranchType");
            this.mBranchType2 = (String) mTransferData.getValueByName("BranchType2");
            this.mAgentComName= (String) mTransferData.getValueByName("AgentComName");
            this.mAgentCode = (String) mTransferData.getValueByName("AgentCode");
            this.mAgentName= (String) mTransferData.getValueByName("AgentName");
            this.mGroupAgentCode = (String) mTransferData.getValueByName("GroupAgentCode");

            return true;
        }
        catch (Exception e) {
          // @@错误处理
          CError.buildErr(this, "接收数据失败");
          return false;
        }
    }
    public boolean checkData()
    {
        return true;
    }
    public boolean submitData(VData cInputData, String cOperate) {
        this.mOperate=cOperate;
        if(!getInputData(cInputData))
        {
            return false;
        }
        if(!checkData())
        {
            return false;
        }
        if(!dealDate())
        {
              CError tError = new CError();
              tError.moduleName = "AgentGroupActiveRateBL";
              tError.functionName = "submitData";
              tError.errorMessage = "数据处理失败-->dealData!";
              this.mErrors.addOneError(tError);
              return false;
        }
        return true;
    }

    public boolean dealDate()
    {
        String condition="";
        if(!StrTool.cTrim(this.mAgentCode).equals("")){
            condition+=" and agentcode='"+this.mAgentCode+"' ";
        }
        if(!StrTool.cTrim(this.mAgentName).equals("")){
            condition+=" and name like '%"+this.mAgentName+"%' ";
        }
        if(!StrTool.cTrim(this.mAgentCom).equals("")){
            condition+=" and agentgroup in (select agentgroup from labranchgroup where branchtype='"+this.mBranchType+"' and branchtype2='"+this.mBranchType2+"' and branchattr='"+this.mAgentCom+"') ";
        }
        if(!StrTool.cTrim(this.mAgentComName).equals("")){
            condition+=" and agentgroup in (select agentgroup from labranchgroup where branchtype='"+this.mBranchType+"' and branchtype2='"+this.mBranchType2+"' and name like '%"+this.mAgentComName+"%') ";
        }
        String sql="";
        sql="select managecom,groupagentcode,name,agentgroup,agentcode from LAAgent "
						+"where ManageCom like '"+this.mManageCom+"%' and BranchType = '"+this.mBranchType+"' and BranchType2 ='"+this.mBranchType2+"' "
 						+"and EmployDate <= (select enddate from lastatsegment where stattype='2' and yearmonth="+this.mWageNo+" ) "
 						+"and (outworkdate is null or outworkdate>=(select startdate from lastatsegment where stattype='2' and yearmonth="+this.mWageNo+" )) "
 						+"and exists (select 'X' from latree where agentcode=laagent.agentcode "
						+"and (SpeciFlag is null or SpeciFlag<>'01')) "
                        +condition
                        +" order by GroupAgentCode asc with ur";
        String strArr[] = null;
        XmlExport xmlexport = new XmlExport();
        xmlexport.createDocument("ComActiviteRate.vts", "printer");
        SSRS tSSRS=new ExeSQL().execSQL(sql);
        System.out.println("SQL:"+sql);
        ListTable tlistTable = new ListTable();
        tlistTable.setName("Get");
        if(tSSRS!=null){
            for (int i = 1; i <= tSSRS.MaxRow; i++)
            {
                strArr = new String[8];
                String tGroupAgentCode=tSSRS.GetText(i, 2);
                String tAgentGroup=tSSRS.GetText(i, 4);
                String tAgentCode=tSSRS.GetText(i, 5);
                strArr[0] = tSSRS.GetText(i, 1);
                strArr[1] = tGroupAgentCode;
                strArr[2] = tSSRS.GetText(i, 3);
                String[] tDepartment=getDepartment(tAgentGroup);
                strArr[3] = tDepartment[0];
                strArr[4] = tDepartment[1];
                String[] tGroup=getGroup(tAgentGroup);
                strArr[5] = tGroup[0];
                strArr[6] = tGroup[1];
                strArr[7] = getActiveRate(tAgentCode);
//                strArr[8] = tAgentCode;

                tlistTable.add(strArr);
            }
        }
        TextTag texttag = new TextTag();
        texttag.add("ManageCom", this.mManageCom);
        strArr = new String[8];
        strArr[0] = "managecom";
        strArr[1] = "groupagentcode";
        strArr[2] = "name";
        strArr[3] = "bu";
        strArr[4] = "buname";
        strArr[5] = "zu";
        strArr[6] = "zuname";
        strArr[7] = "rate";
        if (texttag.size() > 0)
        {
            xmlexport.addTextTag(texttag);
        }
        xmlexport.addListTable(tlistTable, strArr);
        mResult.clear();
        mResult.addElement(xmlexport);
        return true;
    }

    public boolean prepareOutputData()
    {
         try
         {
             mInputData = new VData();
             mInputData.add(mMap);
         }
         catch(Exception e)
         {
             CError.buildErr(this,"提交数据失败！");
             return false;
         }
         return true;
    }
    /** 获得活动率 */
    public String getActiveRate(String agentCode){
        String tRate="0";
        String num1="";
        String num2="";
        double tempRate =0.00;
        String sql="select count(1) from lacomtoagent where relatype='1' and agentcode='"+agentCode+"' and "
                   +"agentcom in (select agentcom from lccont where salechnl='04' and appflag='1' "
                   +"and signdate>=(select startdate from lastatsegment where stattype='1' and yearmonth="+this.mWageNo+" ) "
                   +"and signdate<=(select enddate from lastatsegment where stattype='1' and yearmonth="+this.mWageNo+" ))  "
                   +"and NOT EXISTS (select '1' from lacom  where endflag='Y' and agentcom=lacomtoagent.agentcom) with ur ";
        SSRS tSSRS=new ExeSQL().execSQL(sql);
        num1=tSSRS.GetText(1, 1);
        sql="select count(1) from lacomtoagent where relatype='1' and agentcode='"+agentCode+"' "
            +"and  NOT EXISTS (select '1' from lacom  where endflag='Y' and agentcom=lacomtoagent.agentcom)";
        tSSRS=new ExeSQL().execSQL(sql);
        try{
            System.out.println("rate2:"+tSSRS.GetText(1, 1));
            num2=tSSRS.GetText(1, 1);
            if(StrTool.cTrim(num2).equals("0")){
                return "0";
            }
            DecimalFormat df = new DecimalFormat("0.00");
            tempRate = Double.parseDouble(num1)/Double.parseDouble(num2);
            tRate=String.valueOf(df.format(tempRate));

        }catch(Exception e){
            return "0";
        }
        return tRate;
    }
    /** 获得所在组 */
    public String[] getGroup(String agentGroup){
        String[] str=new String[2];
        str[0]="";
        str[1]="";
        String sql="select name,branchattr,upbranch from labranchgroup where  branchtype='"+this.mBranchType+"' and branchtype2='"+this.mBranchType2+"' and branchlevel='41' and agentGroup='"+agentGroup+"'";
        SSRS tSSRS=new ExeSQL().execSQL(sql);
        if(tSSRS.MaxRow>0){
            str[1]=tSSRS.GetText(1, 1);
            str[0]=tSSRS.GetText(1, 2);
        }
        return str;
    }
    /** 获得所在部 */
    public String[] getDepartment(String agentGroup){
        String[] str=new String[2];
        str[0]="";
        str[1]="";
        String sql="select name,branchattr,upbranch from labranchgroup where  branchtype='"+this.mBranchType+"' and branchtype2='"+this.mBranchType2+"' and branchlevel='41' and agentGroup='"+agentGroup+"'";
        SSRS tSSRS=new ExeSQL().execSQL(sql);
        if(tSSRS!=null && tSSRS.MaxRow>0){
            String upBranch=tSSRS.GetText(1, 3);
            tSSRS=new ExeSQL().execSQL("select name,branchattr from labranchgroup where branchtype='"+this.mBranchType+"' and branchtype2='"+this.mBranchType2+"'  and agentGroup='"+upBranch+"'");
            if(tSSRS!=null && tSSRS.MaxRow>0){
                str[1]=tSSRS.GetText(1, 1);
                str[0]=tSSRS.GetText(1, 2);
            }
        }else{
            tSSRS=new ExeSQL().execSQL("select name,branchattr from labranchgroup where branchtype='"+this.mBranchType+"' and branchtype2='"+this.mBranchType2+"' and branchlevel='42' and agentGroup='"+agentGroup+"'");
            if(tSSRS!=null && tSSRS.MaxRow>0){
                str[1]=tSSRS.GetText(1, 1);
                str[0]=tSSRS.GetText(1, 2);
            }
        }
        return str;
    }
    public VData getResult()
    {
        return mResult;
    }
}

