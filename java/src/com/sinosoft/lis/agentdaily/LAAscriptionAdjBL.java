package com.sinosoft.lis.agentdaily;

import java.sql.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;

public class LAAscriptionAdjBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private MMap mMap = new MMap();
    private VData mOutputData = new VData();
    private LAAscriptionSet mLAAscriptionSet = new LAAscriptionSet();
    private LAAscriptionSet mLAAscriptionOutSet = new LAAscriptionSet();
    private String mManageCom = "";
    private String mBranchType = "";
    private String mBranchType2 = "";
    private String mContNo = "";
    private String mAgentOld = "";
    private String mName = "";

    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mOutputData, "");
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError())
        {
            // @@错误处理
            //this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAAgentEvaluateBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }


    /**
     * 准备后台的数据
     * @return boolean
     */
    private boolean prepareOutputData()
    {
        try
        {
            mMap.put(this.mLAAscriptionOutSet , "UPDATE");
            this.mOutputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError.buildErr(this,"在准备往后层处理所需要的数据时出错。");
            return false;
        }
        return true;
    }

    /**
     * 业务处理方法
     * @return boolean
     */
    private boolean dealData()
    {
        LAAscriptionSet tLAAscriptionDealSet = new LAAscriptionSet();
        LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
        int tAgentCount = mLAAscriptionSet.size(); //需要处理的人数
        //循环处理每个孤儿单信息
        for (int i = 1; i <= tAgentCount; i++)
        {
//            tLAAscriptionSchema = dealAscriptionAgent(mLAAscriptionSet.get(i));
                tLAAscriptionSchema = mLAAscriptionSet.get(i);
            //如果更新信息成功
            if (tLAAscriptionSchema != null)
            {

                LAAscriptionDB tLAAscriptionDB = new LAAscriptionDB();
                tLAAscriptionDB.setAscripNo(tLAAscriptionSchema.getAscripNo());
                if(!tLAAscriptionDB.getInfo()){
                    CError.buildErr(this,"没有找到该保单的归属信息！");
                    return false;
                }
                String tContNo = tLAAscriptionDB.getContNo();
                String tAgentCode = tLAAscriptionSchema.getAgentNew();
                LAAgentDB tLAAgentDB = new LAAgentDB();
                tLAAgentDB.setAgentCode(tAgentCode);
                if(!tLAAgentDB.getInfo())
                {
                    CError.buildErr(this,"查询保单"+tContNo+"的新代理人"+tAgentCode+"出错！");
                    return false;
                }
                if(tLAAgentDB.getAgentState().compareTo("02")>0)
                {
                    CError.buildErr(this,"保单"+tContNo+"的新代理人"+tAgentCode+"已经离职，不能归属！");
                    return false;
                }
                if(!(tLAAgentDB.getBranchType().equals("1") && tLAAgentDB.getBranchType2().equals("01")))
                {
                    CError.buildErr(this,"保单"+tContNo+"的新代理人"+tAgentCode+"的渠道类型必需是个险渠道！");
                    return false;
                }

                tLAAscriptionDB.setAgentNew(tLAAscriptionSchema.getAgentNew());
                tLAAscriptionDB.setAgentGroup(tLAAgentDB.getAgentGroup());
                tLAAscriptionDB.setAscripState("2");
                tLAAscriptionDB.setAscriptionDate(tLAAscriptionSchema.getAscriptionDate());
                tLAAscriptionDB.setOperator(mGlobalInput.Operator);
                tLAAscriptionDB.setModifyDate(PubFun.getCurrentDate());
                tLAAscriptionDB.setModifyTime(PubFun.getCurrentTime());
                tLAAscriptionDealSet.add(tLAAscriptionDB.getSchema());
            }
            else
            {
                CError.buildErr(this,"更新保单信息失败！");
                return false;
            }
        }
        mLAAscriptionOutSet = tLAAscriptionDealSet;
        return true;
    }


       /**
     * 从输入数据中得到所有对象
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        System.out.println("Begin getInputData.........");
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName(
                                            "GlobalInput", 0));
        this.mLAAscriptionSet.set((LAAscriptionSet) cInputData.getObjectByObjectName(
                "LAAscriptionSet", 0));
        this.mManageCom = (String)cInputData.getObject(2);
        this.mBranchType = (String)cInputData.getObject(3);
        this.mBranchType2 = (String)cInputData.getObject(4);
        this.mAgentOld = (String)cInputData.getObject(5);

        if (mGlobalInput == null)
        {
            // @@错误处理
            CError.buildErr(this,"没有得到足够的信息！");
            return false;
        }
        return true;
    }
}
