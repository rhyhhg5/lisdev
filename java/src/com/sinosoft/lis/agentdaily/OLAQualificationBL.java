/*
 * <p>ClassName: OLAQualificationBL </p>
 * <p>Description: OLAQualificationBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-03-21 15:48:13
 */
package com.sinosoft.lis.agentdaily;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class OLAQualificationBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
private String mQualifNoQuery;
/** 业务处理相关变量 */
private LAQualificationSchema mLAQualificationSchema=new LAQualificationSchema();
private LAQualificationSet mLAQualificationSet=new LAQualificationSet();
private LAQualificationSchema mupLAQualificationSchema=new LAQualificationSchema();
private LAQualificationSet mupLAQualificationSet=new LAQualificationSet();
private LAAgentSet mLAAgentSet=new LAAgentSet();

// 备份B表数据  tianjia 2012-6-5
private LAQualificationSet mdelLAQualificationSet = new LAQualificationSet();
private LAQualificationBSet mLAQualificationBSet = new LAQualificationBSet();

private LAQualificationBSchema mLAQualificationBSchema = new LAQualificationBSchema();

private MMap map = new MMap();

public OLAQualificationBL() {
}
public static void main(String[] args) {
}
/**
* 传输数据的公共方法
	* @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "OLAQualificationBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败OLAQualificationBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      CError tError = new CError();
      tError.moduleName = "OLAQualificationBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mInputData=null;
    return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
   boolean tReturn=true;
   String currentDate = PubFun.getCurrentDate();
   String currentTime = PubFun.getCurrentTime();
   String tQualifNo = this.mLAQualificationSchema.getQualifNo();    
   String tAgentCode = this.mLAQualificationSchema.getAgentCode();
   
    LAQualificationDB tLAQualificationDB=new LAQualificationDB();
    LAQualificationSet tLAQualificationSet=new LAQualificationSet();
    LAAgentDB tLAAgentDB=new LAAgentDB();
    LAAgentSet tLAAgentSet=new LAAgentSet();
   if (mOperate.equals("INSERT||MAIN"))
   {
       // 校验页面录入资格证号在系统中是否已存在
       String tSQL="select qualifno from LAQualification where QualifNo='"+tQualifNo+"' and state = '0' ";
       ExeSQL tExeSQL = new ExeSQL();
       String tcheck = tExeSQL.getOneValue(tSQL);
       if (tcheck==null||("").equals(tcheck))
       {
//       if (tLAQualificationSet.size()==0)
//       {
    	   // 基本校验  资格证有效起期需小于资格证有效止期
          if (mLAQualificationSchema.getValidStart().compareTo(mLAQualificationSchema.getValidEnd())>0)
          {
               CError tError = new CError();
               tError.moduleName = "OLAQualificationBL";
               tError.functionName = "dealData";
               tError.errorMessage = "有效起始日期应小于有效截止日期!";
               this.mErrors.addOneError(tError);
               return false;
           }
           // 资格证状态中 ： 0 -有效 、1- 无效 校验页面录入业务员目前是否存有有效资格证
           String cSQL="select * from LAQualification where AgentCode ='"+tAgentCode+"' and State='0'";
           LAQualificationDB nLAQualificationDB=new LAQualificationDB();
           LAQualificationSet ntLAQualificationSet=new LAQualificationSet();
           ntLAQualificationSet=nLAQualificationDB.executeQuery(cSQL);
           if (ntLAQualificationSet!=null && ntLAQualificationSet.size()>0)
           {
               CError tError = new CError();
               tError.moduleName = "OLAQualificationBL";
               tError.functionName = "dealData";
               tError.errorMessage = "原资格证书号"+ntLAQualificationSet.get(1).getQualifNo()+"依然有效!";
               this.mErrors.addOneError(tError);
               return false;

           }
          this.mLAQualificationSchema.setIdx(1);
          this.mLAQualificationSchema.setMakeDate(currentDate);
          this.mLAQualificationSchema.setMakeTime(currentTime);
          this.mLAQualificationSchema.setModifyDate(currentDate);
          this.mLAQualificationSchema.setModifyTime(currentTime);
          this.mLAQualificationSchema.setOperator(mGlobalInput.Operator);// mLAQualificationSchema insert
          this.mLAQualificationSchema.setreissueDate(currentDate);// 存储资格证有效开始日期 add new 
          map.put(mLAQualificationSchema, "INSERT");
          
          // laagent 表 字段调整 存储quafno 字段
          String sql="select * from LAAgent where AgentCode='"+tAgentCode+"' and branchtype='1' ";
          tLAAgentSet=tLAAgentDB.executeQuery(sql);
          if (tLAAgentSet!=null && tLAAgentSet.size() >0)
          {
              LAAgentSchema mLAAgentSchema=new LAAgentSchema();
              mLAAgentSchema = tLAAgentSet.get(1);
              mLAAgentSchema.setQuafNo(tQualifNo);
              mLAAgentSet.add(mLAAgentSchema);  // mLAAgentSet update
          }
          else
          {
              CError tError = new CError();
              tError.moduleName = "OLAQualificationBL";
              tError.functionName = "submitDat";
              tError.errorMessage = "系统中没有营销员"+tAgentCode+"信息!";
              this.mErrors.addOneError(tError);
              return false;

          }
          map.put(mLAAgentSet, "UPDATE");
         String backup_sql = "select * from LAQualification where agentcode = '"+tAgentCode+"'";
         // 备份相关数据
         nLAQualificationDB=new LAQualificationDB();
         mdelLAQualificationSet= nLAQualificationDB.executeQuery(backup_sql);// mdelLAQualificationSet delete
         String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 19);
         for(int temp = 1;temp<=mdelLAQualificationSet.size();temp++)
         {
        	 LAQualificationSchema oldLAQualificationSchema = new LAQualificationSchema();
        	 LAQualificationBSchema t_LAQualificationBSchema = new LAQualificationBSchema();
        	 oldLAQualificationSchema= mdelLAQualificationSet.get(temp).getSchema();
        	 Reflections tReflections1 = new Reflections();
        	 tReflections1.transFields(t_LAQualificationBSchema, oldLAQualificationSchema);       	 
        	 t_LAQualificationBSchema.setEdorNo(tEdorNo);// 存储业务号
        	 t_LAQualificationBSchema.setMakeDate1(oldLAQualificationSchema.getMakeDate());
        	 t_LAQualificationBSchema.setMakeTime1(oldLAQualificationSchema.getMakeTime());
        	 t_LAQualificationBSchema.setOperator1(oldLAQualificationSchema.getOperator());
        	 t_LAQualificationBSchema.setOperator(mGlobalInput.Operator);
        	 t_LAQualificationBSchema.setMakeDate(currentDate);
        	 t_LAQualificationBSchema.setMakeTime(currentTime);
        	 t_LAQualificationBSchema.setModifyDate(currentDate);
        	 t_LAQualificationBSchema.setModifyTime(currentTime);
        	 mLAQualificationBSet.add(t_LAQualificationBSchema); // mLAQualificationBSet Insert 操作        	 
         }
         map.put(mdelLAQualificationSet, "DELETE");
         map.put(mLAQualificationBSet, "INSERT");
       }
       else
       {
               CError tError = new CError();
               tError.moduleName = "OLAQualificationBL";
               tError.functionName = "submitDat";
               tError.errorMessage = "系统已存在有效录入资格证书号!";
               this.mErrors.addOneError(tError);
               return false;
         }
   }
   if (this.mOperate.equals("UPDATE||MAIN"))
   {
       if(mQualifNoQuery==null || mQualifNoQuery.equals(""))
       {
           tLAQualificationDB.setQualifNo(mLAQualificationSchema.getQualifNo());
       }
       else
       {
           tLAQualificationDB.setQualifNo(mQualifNoQuery);
       }
       
        tLAQualificationDB.setAgentCode(mLAQualificationSchema.getAgentCode());
        if (!tLAQualificationDB.getInfo())
        {
               // @@错误处理
               CError tError = new CError();
               tError.moduleName = "OLAQualificationBL";
               tError.functionName = "dealData";
               tError.errorMessage = "原资格证书信息查询失败!";
               this.mErrors.addOneError(tError);
               return false;
        }
        mupLAQualificationSchema=tLAQualificationDB.getSchema();// 查询出修改资格证信息 
        
        // 基本校验 修改时，所属业务员是否存有有效展业证信息
        String cSQL="select qualifno from LAQualification where AgentCode ='"+tAgentCode+"' and State='0' and qualifno <>'"+mQualifNoQuery+"' fetch first 1 rows only";
        ExeSQL tExeSQL = new ExeSQL();
        String tResult = tExeSQL.getOneValue(cSQL);
        if(tResult==null||tResult.equals(""))
        {        
        	// 展业证有效起期应小于展业证有效止期
            if (mLAQualificationSchema.getValidStart().compareTo(mLAQualificationSchema.getValidEnd())>0)
            {
                 CError tError = new CError();
                 tError.moduleName = "OLACertificationBL";
                 tError.functionName = "dealData";
                 tError.errorMessage = "有效起始日期应小于有效截止日期!";
                 this.mErrors.addOneError(tError);
                 return false;
             }
             // 置展业证失效时，需录入展业证失效日期
            if("1".equals(mLAQualificationSchema.getState()))
            {
          	  if(mLAQualificationSchema.getInvalidDate()==null||mLAQualificationSchema.getInvalidDate().equals(""))
          	  {
          		  CError tError = new CError();
                    tError.moduleName = "OLACertificationBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "页面资格证状态为失效时，需录入对应失效日期!";
                    this.mErrors.addOneError(tError);
                    return false;
          	  }
            }
          	String tEdorNo = PubFun1.CreateMaxNo("EdorNo", 19);
        	// 先备份自己
        	Reflections t1 = new Reflections();
        	LAQualificationBSchema tLAQualificationBSchema = new LAQualificationBSchema();
        	t1.transFields(tLAQualificationBSchema, mupLAQualificationSchema);
        	tLAQualificationBSchema.setEdorNo(tEdorNo);// 存储业务号
        	tLAQualificationBSchema.setMakeDate1(mupLAQualificationSchema.getMakeDate());
        	tLAQualificationBSchema.setMakeTime1(mupLAQualificationSchema.getMakeTime());
        	tLAQualificationBSchema.setOperator1(mupLAQualificationSchema.getOperator());
        	tLAQualificationBSchema.setOperator(mGlobalInput.Operator);
    	    tLAQualificationBSchema.setMakeDate(currentDate);
    	    tLAQualificationBSchema.setMakeTime(currentTime);
    	    tLAQualificationBSchema.setModifyDate(currentDate);
    	    tLAQualificationBSchema.setModifyTime(currentTime);
    	    mLAQualificationBSet.add(tLAQualificationBSchema);// 备份自己信息
        	
        	// 有三个字段 agentcode qualifno 控制不能进行修改
        	if("0".equals(mLAQualificationSchema.getState())&&("1").equals(mupLAQualificationSchema.getState()))
            {
        		mupLAQualificationSchema.setreissueDate(currentDate);
            }
        	mupLAQualificationSchema.setGrantUnit(this.mLAQualificationSchema.getGrantUnit());// 批准单位
        	mupLAQualificationSchema.setGrantDate(this.mLAQualificationSchema.getGrantDate());//发放日期
        	mupLAQualificationSchema.setValidStart(this.mLAQualificationSchema.getValidStart());// 有效起期
        	mupLAQualificationSchema.setValidEnd(this.mLAQualificationSchema.getValidEnd()); // 有效止期
        	mupLAQualificationSchema.setState(this.mLAQualificationSchema.getState());// 资格证状态
        	if(this.mLAQualificationSchema.getState().equals("1"))
        	{
        		mupLAQualificationSchema.setInvalidDate(this.mLAQualificationSchema.getInvalidDate()); //失效日期
            	mupLAQualificationSchema.setInvalidRsn(this.mLAQualificationSchema.getInvalidRsn());// 失效原因
        	}else{
        		mupLAQualificationSchema.setInvalidDate("");
        		mupLAQualificationSchema.setInvalidRsn("");        		
        	}

        	mupLAQualificationSchema.setPasExamDate(this.mLAQualificationSchema.getPasExamDate()); // 通过考试日期
        	mupLAQualificationSchema.setExamYear(this.mLAQualificationSchema.getExamYear());// 考试年度
        	mupLAQualificationSchema.setExamTimes(this.mLAQualificationSchema.getExamTimes()); //考试次数
        	// mupLAQualificationSchema.setMakeDate(this.mLAQualificationSchema.getMakeDate()); // 原日期
        	// mupLAQualificationSchema.setMakeTime(this.mLAQualificationSchema.getMakeTime()); // 原时间
        	mupLAQualificationSchema.setModifyDate(currentDate); // 当前日期
        	mupLAQualificationSchema.setModifyTime(currentTime); // 当前时间
        	mupLAQualificationSchema.setOperator(this.mGlobalInput.Operator);// 当前操作员
        	map.put(mupLAQualificationSchema, "UPDATE");
        	
        	// 查询所有资格证信息  +++ 这个处理还有问题。
        	cSQL="select * from LAQualification where AgentCode ='"+tAgentCode+"' and qualifno <>'"+mQualifNoQuery+"' ";
        	this.mdelLAQualificationSet=tLAQualificationDB.executeQuery(cSQL);

        	for(int i = 1;i<=this.mdelLAQualificationSet.size();i++)
        	{
              	 LAQualificationSchema oldLAQualificationSchema = new LAQualificationSchema();
            	 LAQualificationBSchema t_LAQualificationBSchema = new LAQualificationBSchema();
            	 oldLAQualificationSchema= mdelLAQualificationSet.get(i).getSchema();
            	 Reflections tReflections1 = new Reflections();
            	 tReflections1.transFields(t_LAQualificationBSchema, oldLAQualificationSchema);       	 
            	 t_LAQualificationBSchema.setEdorNo(tEdorNo);// 存储业务号
            	 t_LAQualificationBSchema.setMakeDate1(oldLAQualificationSchema.getMakeDate());
            	 t_LAQualificationBSchema.setMakeTime1(oldLAQualificationSchema.getMakeTime());
            	 t_LAQualificationBSchema.setOperator1(oldLAQualificationSchema.getOperator());
            	 t_LAQualificationBSchema.setOperator(mGlobalInput.Operator);
            	 t_LAQualificationBSchema.setMakeDate(currentDate);
            	 t_LAQualificationBSchema.setMakeTime(currentTime);
            	 t_LAQualificationBSchema.setModifyDate(currentDate);
            	 t_LAQualificationBSchema.setModifyTime(currentTime);
            	 mLAQualificationBSet.add(t_LAQualificationBSchema); // mLAQualificationBSet Insert 操作   	
        	}
        map.put(mdelLAQualificationSet, "DELETE");
        map.put(mLAQualificationBSet, "INSERT");
        	
        }else{
        	 // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLAQualificationBL";
            tError.functionName = "dealData";
            tError.errorMessage = "系统中存有该人员有效资格证信息（对应资格证号为"+tResult+"）,烦请先进行有效资格证信息处理操作!";
            this.mErrors.addOneError(tError);
            return false;
        }
   }
   return tReturn;

}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
         this.mLAQualificationSchema.setSchema((LAQualificationSchema)cInputData.getObjectByObjectName("LAQualificationSchema",0));
         this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         this.mQualifNoQuery=String.valueOf(cInputData.get(0));
         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
    this.mResult.clear();
    LAQualificationDB tLAQualificationDB=new LAQualificationDB();
    tLAQualificationDB.setSchema(this.mLAQualificationSchema);
		//如果有需要处理的错误，则返回
		if (tLAQualificationDB.mErrors.needDealError())
 		{
		  // @@错误处理
 			this.mErrors.copyAllErrors(tLAQualificationDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "LAQualificationBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors .addOneError(tError) ;
 			return false;
    }
    mInputData=null;
    return true;
}
 private boolean prepareOutputData()
 {
   try
	{
	      mInputData.clear();
	      mInputData.add(map);
	}
	catch(Exception ex)
	{
 		// @@错误处理
		CError tError =new CError();
 		tError.moduleName="LAQualificationBL";
 		tError.functionName="prepareData";
 		tError.errorMessage="在准备往后层处理所需要的数据时出错。";
 		this.mErrors .addOneError(tError) ;
		return false;
	}
	return true;
	}
	public VData getResult()
	{
  	return this.mResult;
	}
}
