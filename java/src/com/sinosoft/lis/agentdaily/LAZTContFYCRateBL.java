/*
 * <p>ClassName: LAZTContFYCRateBL </p>
 * <p>Description: LAZTContFYCRateBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2008-07-08 18:05:58
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.Reflections;


public class LAZTContFYCRateBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String currentDate;
    private String currentTime;
    /** 业务处理相关变量 */
    private LAContFYCRateSchema mLAContFYCRateSchema = new LAContFYCRateSchema();
    private LAContFYCRateSet mLAContFYCRateSet = new LAContFYCRateSet();
    private LAContFYCRateSet mdelLAContFYCRateSet = new LAContFYCRateSet();
    private LAContFYCRateBSet mLAContFYCRateBSet = new LAContFYCRateBSet();
    
    private LACommisionSet mLACommisionSet = new LACommisionSet();
    private LACommisionBSet mLACommisionBSet = new LACommisionBSet();
    
    private TransferData mTransferData = new TransferData();
    private String mWageNo ;
    private String mContNo;
    private String mBranchType2;
    
    private MMap mMap = new MMap();
    
    public LAZTContFYCRateBL()
    {
    }

    public static void main(String[] args)
    {
        LAContFYCRateSchema tLAContFYCRateSchema = new LAContFYCRateSchema();
        LAContFYCRateSet tupLAContFYCRateSet = new LAContFYCRateSet();
        tLAContFYCRateSchema.setGrpContNo("0000031701");
        tLAContFYCRateSchema.setRiskCode("1605");
        tLAContFYCRateSchema.setRate("0.8");
        tupLAContFYCRateSet.add(tLAContFYCRateSchema);
        GlobalInput tG = new GlobalInput();
        tG.Operator = "001";
        tG.ManageCom = "86";
        VData tVData = new VData();
        tVData.addElement(tG);

        tVData.addElement(tupLAContFYCRateSet);
        LAZTContFYCRateBL tLAZTContFYCRateBL = new LAZTContFYCRateBL();
        //tLAZTContFYCRateBL.submitData(tVData, "DELETE||MAIN");

    }
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("操作："+mOperate);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if(!checkDate()) return false;
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAZTContFYCRateBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LAZTContFYCRateBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAZTContFYCRateBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        currentDate = PubFun.getCurrentDate();
        currentTime = PubFun.getCurrentTime();
        System.out.print("开始 dealdate():"+mOperate);
        if (this.mOperate.equals("INSERT||MAIN") )
        {

            if (!saveData())
            {
                return false;
            }

        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            if (!upDateData())
            {
                return false;
            }


        }

        return true;
    }
    private boolean saveData()
    {
    	for(int temp = 1;temp <= mLAContFYCRateSet.size();temp++)
    	{
          LAContFYCRateSchema tLAContFYCRateSchema = new LAContFYCRateSchema();
          LAContFYCRateSet tLAContFYCRateSet = new LAContFYCRateSet();
          tLAContFYCRateSchema = mLAContFYCRateSet.get(temp).getSchema();
          LAContFYCRateDB tLAContFYCRateDB = new LAContFYCRateDB();
      	  tLAContFYCRateDB.setGrpContNo(tLAContFYCRateSchema.getGrpContNo());
      	  tLAContFYCRateSet = tLAContFYCRateDB.query();
      	if(tLAContFYCRateSet.size()>0)
      	{
      		 CError tError = new CError();
               tError.moduleName = "LAContFYCRateBL";
               tError.functionName = "dealdata";
               tError.errorMessage = "该笔保单已录入非标比例,请通过修改功能进行非标修改!";
               this.mErrors.addOneError(tError);
               return false; 
      	}
          LACommisionDB tLACommisionDB = new LACommisionDB();
          LACommisionSet tLACommisionSet = new LACommisionSet();
       // 这个为了避免主键冲突特意写的 tLAContFYCRateSchema 中GrpContNo 存的是主键commisionsn
          tLACommisionDB.setCommisionSN(tLAContFYCRateSchema.getGrpContNo());
          tLACommisionSet=tLACommisionDB.query();
          if(tLACommisionSet.size()<=0)
          {
        	  CError tError = new CError();
              tError.moduleName = "LAContFYCRateBL";
              tError.functionName = "dealdata";
              tError.errorMessage = "查询不到" +tLAContFYCRateSchema.getGrpContNo()+ "的对应提奖信息!";
              this.mErrors.addOneError(tError);
              return false; 
          }
          if(tLACommisionSet.get(1).getTransMoney()<0)
          {
        	  CError tError = new CError();
              tError.moduleName = "LAContFYCRateBL";
              tError.functionName = "dealdata";
              tError.errorMessage = "保单号为"+mContNo+"险种编码为"+tLAContFYCRateSchema.getRiskCode()+"对应保费为负(保费金额为"+tLAContFYCRateSchema.getMoney()+"),不允许进行非标比例的调整!";
              this.mErrors.addOneError(tError);
              return false; 
          }
          for(int j = 1;j<=tLACommisionSet.size();j++)
          {
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            tLACommisionSchema=tLACommisionSet.get(j).getSchema();
            if(tLACommisionSchema.getFYCRate()<tLAContFYCRateSchema.getRate())
            {
            	 CError tError = new CError();
                 tError.moduleName = "LAContFYCRateBL";
                 tError.functionName = "dealdata";
                 tError.errorMessage = "保单号为"+mContNo+"险种编码为"+tLAContFYCRateSchema.getRiskCode()+"非标比例高于标准比例,烦请重新录入!";
                 this.mErrors.addOneError(tError);
                 return false; 
            }
            //备份B表数据
            LACommisionBSchema tLACommisionBSchema = new LACommisionBSchema();  
      	    Reflections tReflections = new Reflections();
            tReflections.transFields(tLACommisionBSchema, tLACommisionSchema);
            String mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
            tLACommisionBSchema.setEdorNo(mEdorNo);
            tLACommisionBSchema.setEdorType("03");
            this.mLACommisionBSet.add(tLACommisionBSchema);
            
            String del_sql = "select distinct 1 from lawagecalelement where branchtype ='2' and branchtype2 = '"+mBranchType2+"' and caltype = '10' and riskcode = '"+tLACommisionSchema.getRiskCode()+"' ";
            ExeSQL tExeSQL = new ExeSQL();
            String tResult = tExeSQL.getOneValue(del_sql);
            if(tResult!=null&&!tResult.equals(""))
            {
            	 tLACommisionSchema.setFYCRate(tLAContFYCRateSchema.getRate());
                 tLACommisionSchema.setStandFYCRate(tLAContFYCRateSchema.getRate());
                 tLACommisionSchema.setFYC(tLAContFYCRateSchema.getRate()*(tLACommisionSchema.getP7()));
                 tLACommisionSchema.setDirectWage(tLAContFYCRateSchema.getRate()*(tLACommisionSchema.getP7()));
                 tLACommisionSchema.setModifyDate(currentDate);
                 tLACommisionSchema.setModifyTime(currentTime);
                 tLACommisionSchema.setOperator(mGlobalInput.Operator);
                 this.mLACommisionSet.add(tLACommisionSchema);
            }else{
            tLACommisionSchema.setFYCRate(tLAContFYCRateSchema.getRate());
            tLACommisionSchema.setStandFYCRate(tLAContFYCRateSchema.getRate());
            tLACommisionSchema.setFYC(tLAContFYCRateSchema.getRate()*(tLACommisionSchema.getTransMoney()));
            tLACommisionSchema.setDirectWage(tLAContFYCRateSchema.getRate()*(tLACommisionSchema.getTransMoney()));
            tLACommisionSchema.setModifyDate(currentDate);
            tLACommisionSchema.setModifyTime(currentTime);
            tLACommisionSchema.setOperator(mGlobalInput.Operator);
            this.mLACommisionSet.add(tLACommisionSchema);
            }
          } 
           /**
            * 注: 针对团险非标改造 
            * 将 LAContFYCRate表 grpcontno 字段 用于存储lacommision表主键
            */
           tLAContFYCRateSchema.setMakeDate(currentDate);
           tLAContFYCRateSchema.setMakeTime(currentTime);
           tLAContFYCRateSchema.setModifyDate(currentDate);
           tLAContFYCRateSchema.setModifyTime(currentTime);
           tLAContFYCRateSchema.setOperator(mGlobalInput.Operator);
           mdelLAContFYCRateSet.add(tLAContFYCRateSchema);
    	}
    	
    	mMap.put(mLACommisionBSet, "INSERT");
    	mMap.put(mLACommisionSet, "UPDATE");
    	mMap.put(mdelLAContFYCRateSet, "INSERT");
        return true;
    }
    private boolean upDateData()
    {
    	
        for (int i = 1; i <= mLAContFYCRateSet.size(); i++)
        {
        	Reflections tReflections = new Reflections();
        	LAContFYCRateSchema tempLAContFYCRateSchema = new LAContFYCRateSchema();
        	LAContFYCRateSet tLAContFYCRateSet = new LAContFYCRateSet();
        	tempLAContFYCRateSchema = mLAContFYCRateSet.get(i).getSchema();
        	LAContFYCRateDB tLAContFYCRateDB = new LAContFYCRateDB();
        	tLAContFYCRateDB.setGrpContNo(tempLAContFYCRateSchema.getGrpContNo());
        	tLAContFYCRateSet = tLAContFYCRateDB.query();
        	if(tLAContFYCRateSet.size()<=0)
        	{
        		 CError tError = new CError();
                 tError.moduleName = "LAContFYCRateBL";
                 tError.functionName = "dealdata";
                 tError.errorMessage = "查询不到保单号为" +mContNo+ "险种号为"+tempLAContFYCRateSchema.getRiskCode()+"的对应非标信息!";
                 this.mErrors.addOneError(tError);
                 return false; 
        	}
        	for(int j =1;j<=tLAContFYCRateSet.size();j++)
        	{
        		LAContFYCRateSchema tLAContFYCRateSchema = new LAContFYCRateSchema();
        		LAContFYCRateBSchema tLAContFYCRateBSchema = new LAContFYCRateBSchema();
        		
        		tLAContFYCRateSchema = tLAContFYCRateSet.get(j).getSchema();
        		
        		tReflections.transFields(tLAContFYCRateBSchema, tLAContFYCRateSchema);
        		 String tEdorNo = PubFun1.CreateMaxNo("FYCRateEdorNo", 20);
        		tLAContFYCRateBSchema.setEdorNo(tEdorNo);
        		tLAContFYCRateBSchema.setOriOperator(tLAContFYCRateSchema.getOperator());
        		tLAContFYCRateBSchema.setOriMakeDate(tLAContFYCRateSchema.getMakeDate());
        		tLAContFYCRateBSchema.setOriMakeTime(tLAContFYCRateSchema.getMakeTime());
        		tLAContFYCRateBSchema.setOriModifyDate(tLAContFYCRateSchema.getModifyDate());
        		tLAContFYCRateBSchema.setOriModifyTime(tLAContFYCRateSchema.getModifyTime());
        		tLAContFYCRateBSchema.setMakeDate(currentDate);
        		tLAContFYCRateBSchema.setMakeTime(currentTime);
        		tLAContFYCRateBSchema.setModifyDate(currentDate);
        		tLAContFYCRateBSchema.setModifyTime(currentTime);
        		this.mLAContFYCRateBSet.add(tLAContFYCRateBSchema);
        		
        		tLAContFYCRateSchema.setRate(tempLAContFYCRateSchema.getRate());
        		tLAContFYCRateSchema.setModifyDate(currentDate);
        		tLAContFYCRateSchema.setModifyTime(currentTime);
        		tLAContFYCRateSchema.setOperator(mGlobalInput.Operator);
        		this.mdelLAContFYCRateSet.add(tLAContFYCRateSchema);
        	}
        	  LACommisionDB tLACommisionDB = new LACommisionDB();
              LACommisionSet tLACommisionSet = new LACommisionSet();
           // 这个为了避免主键冲突特意写的 tLAContFYCRateSchema 中GrpContNo 存的是主键commisionsn
              tLACommisionDB.setCommisionSN(tempLAContFYCRateSchema.getGrpContNo()); 
              tLACommisionSet=tLACommisionDB.query();
              if(tLACommisionSet.size()<1)
              {
            	  CError tError = new CError();
                  tError.moduleName = "LAContFYCRateBL";
                  tError.functionName = "dealdata";
                  tError.errorMessage = "查询不到" +tempLAContFYCRateSchema.getGrpContNo()+ "的对应提奖信息!";
                  this.mErrors.addOneError(tError);
                  return false; 
              }
              for(int j = 1;j<=tLACommisionSet.size();j++)
              {
                LACommisionSchema tLACommisionSchema = new LACommisionSchema();
                tLACommisionSchema=tLACommisionSet.get(j).getSchema();
                if(tLACommisionSchema.getFYCRate()<tempLAContFYCRateSchema.getRate())
                {
                	 CError tError = new CError();
                     tError.moduleName = "LAContFYCRateBL";
                     tError.functionName = "dealdata";
                     tError.errorMessage = "保单号为"+mContNo+"险种编码为"+tempLAContFYCRateSchema.getRiskCode()+"非标比例高于标准比例,烦请重新录入!";
                     this.mErrors.addOneError(tError);
                     return false; 
                }
                //备份B表数据
                LACommisionBSchema tLACommisionBSchema = new LACommisionBSchema();  
          	     tReflections = new Reflections();
                tReflections.transFields(tLACommisionBSchema, tLACommisionSchema);
                String mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20);
                tLACommisionBSchema.setEdorNo(mEdorNo);
                tLACommisionBSchema.setEdorType("03");
                this.mLACommisionBSet.add(tLACommisionBSchema);
                String del_sql = "select distinct 1 from lawagecalelement where branchtype ='2' and branchtype2 = '"+mBranchType2+"' and caltype = '10' and riskcode = '"+tLACommisionSchema.getRiskCode()+"' ";
                ExeSQL tExeSQL = new ExeSQL();
                String tResult = tExeSQL.getOneValue(del_sql);
                if(tResult!=null&&!tResult.equals(""))
                {
                	 tLACommisionSchema.setFYCRate(tempLAContFYCRateSchema.getRate());
                     tLACommisionSchema.setStandFYCRate(tempLAContFYCRateSchema.getRate());
                     tLACommisionSchema.setFYC(tempLAContFYCRateSchema.getRate()*(tLACommisionSchema.getP7()));
                     tLACommisionSchema.setDirectWage(tempLAContFYCRateSchema.getRate()*(tLACommisionSchema.getP7()));
                     tLACommisionSchema.setModifyDate(currentDate);
                     tLACommisionSchema.setModifyTime(currentTime);
                     tLACommisionSchema.setOperator(mGlobalInput.Operator);
                     this.mLACommisionSet.add(tLACommisionSchema);
                }else{
                tLACommisionSchema.setFYCRate(tempLAContFYCRateSchema.getRate());
                tLACommisionSchema.setStandFYCRate(tempLAContFYCRateSchema.getRate());
                tLACommisionSchema.setFYC(tempLAContFYCRateSchema.getRate()*(tLACommisionSchema.getTransMoney()));
                tLACommisionSchema.setDirectWage(tempLAContFYCRateSchema.getRate()*(tLACommisionSchema.getTransMoney()));
                tLACommisionSchema.setModifyDate(currentDate);
                tLACommisionSchema.setModifyTime(currentTime);
                tLACommisionSchema.setOperator(mGlobalInput.Operator);
                this.mLACommisionSet.add(tLACommisionSchema);
                }
              }  
        	
        }
        mMap.put(mLACommisionBSet, "INSERT");
    	mMap.put(mLACommisionSet, "UPDATE");
    	mMap.put(mdelLAContFYCRateSet, "UPDATE");
    	mMap.put(mLAContFYCRateBSet, "INSERT");
        return true;
    }


    public VData getResult()
    {
        this.mResult.clear();
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
    	try{
            this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                        getObjectByObjectName("GlobalInput", 0));
            
            this.mTransferData=(TransferData)cInputData.getObjectByObjectName("TransferData", 0);
            this.mWageNo=(String)mTransferData.getValueByName("WageNo");
            this.mContNo=(String)mTransferData.getValueByName("ContNo");
            this.mLAContFYCRateSet=(LAContFYCRateSet)mTransferData.getValueByName("LAContFYCRateSet");
            this.mBranchType2= (String)mTransferData.getValueByName("BranchType2");
            System.out.println("接受过来的薪资月为"+mWageNo);
        	}catch(Exception ex)
        	{
        		ex.printStackTrace();
        	}
            return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        LAContFYCRateDB tLAContFYCRateDB = new LAContFYCRateDB();
        int i;
        for (i = 0; i < mLAContFYCRateSet.size(); i++)
        {

            tLAContFYCRateDB.setSchema(this.mLAContFYCRateSet.get(i));
            //如果有需要处理的错误，则返回
            if (tLAContFYCRateDB.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAContFYCRateDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "LAZTContFYCRateBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mInputData = null;
        }
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData = new VData();
            this.mInputData.add(mMap);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAZTContFYCRateBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    /**************************************
     *  注 : 对数据进行相关校验
     *       修改数据是否符合条件
     **/
    private boolean checkDate()
    {
        LACommisionDB tLACommisionDB = new LACommisionDB();
        LACommisionSet tLACommisionSet = new LACommisionSet();
        tLACommisionDB.setContNo(mContNo);
        tLACommisionDB.setWageNo(mWageNo);
        if(mContNo==null||mContNo.equals(""))
        {
        	 System.out.println("11");
            CError tError = new CError();
            tError.moduleName = "LAContFYCRateDB";
            tError.functionName = "submitData";
            tError.errorMessage = "页面录入保单号信息不能为空,烦请重新进行录入!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if(mWageNo==null||mWageNo.equals(""))
        {
        	System.out.println("22");
            CError tError = new CError();
            tError.moduleName = "LAContFYCRateDB";
            tError.functionName = "submitData";
            tError.errorMessage = "页面录入薪资月信息为空,烦请重新进行录入!";
            this.mErrors.addOneError(tError);
            return false;
        }
        tLACommisionSet=tLACommisionDB.query();
        if(tLACommisionSet.size()<1)
        {
        	  // @@错误处理
            this.mErrors.copyAllErrors(tLACommisionDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAContFYCRateDB";
            tError.functionName = "submitData";
            tError.errorMessage = "查询修改保单信息失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        String check_sql = "select distinct '1' from lawage where branchtype = '2' and branchtype2 = '"+mBranchType2+"' and managecom= '"+tLACommisionSet.get(1).getManageCom()+"' and indexcalno = '"+mWageNo+"'";
        System.out.println("检验保单所属薪资月是否大于最大薪资月SQL如下"+check_sql);
        ExeSQL tExeSQL = new ExeSQL();
        String tResult = tExeSQL.getOneValue(check_sql);
        if (tExeSQL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tExeSQL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LAContFYCRateBL";
            tError.functionName = "upDateData";
            tError.errorMessage = "查询薪资表数据出错！";
            this.mErrors.addOneError(tError);
            return false;
         }
        if("1".equals(tResult))
        {
            CError tError = new CError();
            tError.moduleName = "LAContFYCRateBL";
            tError.functionName = "upDateData";
            tError.errorMessage = "该保单信息已进行过薪资计算，无法录入或修改该笔保单提奖比例信息，如需调整，请上报总公司销售管理部审批！";
            this.mErrors.addOneError(tError);
            return false;
        }
    	 return true;
    }
    }

