/*
 * <p>ClassName: ALACrossBLS </p>
 * <p>Description: ALACrossBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentdaily;


import java.sql.Connection;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.db.LAAcrossCommDB;

public class ALACrossBLS
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据操作字符串 */
    private String mOperate;
    public ALACrossBLS() {
    }

    public static void main(String[] args) {
    }

    /**
       传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        System.out.println("Start ALACrossBLS Submit...");
        if (this.mOperate.equals("INSERT||MAIN")) {
            tReturn = saveLAAcrossComm(cInputData);
        }
        if (this.mOperate.equals("DELETE||MAIN")) {
            tReturn = deleteLAAcrossComm(cInputData);
        }
        if (this.mOperate.equals("UPDATE||MAIN")) {
         tReturn = deleteLAAcrossComm(cInputData);
        }
        if (tReturn) {
            System.out.println(" sucessful");
        } else {
            System.out.println("Save failed");
        }
        System.out.println("End ALACrossBLS Submit...");
        return tReturn;
    }
    private boolean saveLAAcrossComm(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn;
        conn = null;
        conn = DBConnPool.getConnection();
        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAContBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            System.out.println("Start 保存...");
            LAAcrossCommDB tLAAcrossCommDB= new LAAcrossCommDB(conn);

          tLAAcrossCommDB.setSchema((LAAcrossCommSchema) mInputData.
                                    getObjectByObjectName("LAAcrossCommSchema", 0));
            System.out.println("before save");
            if (!tLAAcrossCommDB.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLAAcrossCommDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALACrossBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

           conn.commit();
           conn.close();
       }

        catch (Exception ex)
        {
            // @@错误处理
            ex.printStackTrace() ;
            CError tError = new CError();
            tError.moduleName = "ALACrossBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            tReturn = false;
            try
            {
                conn.rollback();
                conn.close();
            }
            catch (Exception e)
            {}
        }
        return tReturn;
    }
    private boolean deleteLAAcrossComm(VData mInputData)
       {
         boolean tReturn = true;
         System.out.println("Start Delete...");
         Connection conn;
         conn = null;
         conn = DBConnPool.getConnection();
         if (conn == null)
         {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "ALACrossBLS";
             tError.functionName = "deleteData";
             tError.errorMessage = "数据库连接失败!";
             this.mErrors.addOneError(tError);
             return false;
         }
         try
         {
             conn.setAutoCommit(false);
             System.out.println("Start 删除...");
             LAAcrossCommDB tLAAcrossCommDB= new LAAcrossCommDB(conn);

           tLAAcrossCommDB.setSchema((LAAcrossCommSchema) mInputData.
                                     getObjectByObjectName("LAAcrossCommSchema", 0));
             System.out.println("before delete");
             if (!tLAAcrossCommDB.delete())
             {
                 // @@错误处理
                 this.mErrors.copyAllErrors(tLAAcrossCommDB.mErrors);
                 CError tError = new CError();
                 tError.moduleName = "ALACrossBLS";
                 tError.functionName = "deleteData";
                 tError.errorMessage = "数据保存失败!";
                 this.mErrors.addOneError(tError);
                 conn.rollback();
                 conn.close();
                 return false;
             }

            conn.commit();
            conn.close();
        }

         catch (Exception ex)
         {
             // @@错误处理
             ex.printStackTrace() ;
             CError tError = new CError();
             tError.moduleName = "ALACrossBLS";
             tError.functionName = "submitData";
             tError.errorMessage = ex.toString();
             this.mErrors.addOneError(tError);
             tReturn = false;
             try
             {
                 conn.rollback();
                 conn.close();
             }
             catch (Exception e)
             {}
         }
         return tReturn;
     }
     private boolean updateLAAcrossComm(VData mInputData)
       {
         boolean tReturn = true;
         System.out.println("Start Update...");
         Connection conn;
         conn = null;
         conn = DBConnPool.getConnection();
         if (conn == null)
         {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "ALACrossBLS";
             tError.functionName = "updateData";
             tError.errorMessage = "数据库连接失败!";
             this.mErrors.addOneError(tError);
             return false;
         }
         try
         {
             conn.setAutoCommit(false);
             System.out.println("Start 删除...");
             LAAcrossCommDB tLAAcrossCommDB= new LAAcrossCommDB(conn);

           tLAAcrossCommDB.setSchema((LAAcrossCommSchema) mInputData.
                                     getObjectByObjectName("LAAcrossCommSchema", 0));
             System.out.println("before update");
             if (!tLAAcrossCommDB.update())
             {
                 // @@错误处理
                 this.mErrors.copyAllErrors(tLAAcrossCommDB.mErrors);
                 CError tError = new CError();
                 tError.moduleName = "ALACrossBLS";
                 tError.functionName = "updateData";
                 tError.errorMessage = "数据保存失败!";
                 this.mErrors.addOneError(tError);
                 conn.rollback();
                 conn.close();
                 return false;
             }

            conn.commit();
            conn.close();
        }

         catch (Exception ex)
         {
             // @@错误处理
             ex.printStackTrace() ;
             CError tError = new CError();
             tError.moduleName = "ALACrossBLS";
             tError.functionName = "submitData";
             tError.errorMessage = ex.toString();
             this.mErrors.addOneError(tError);
             tReturn = false;
             try
             {
                 conn.rollback();
                 conn.close();
             }
             catch (Exception e)
             {}
         }
         return tReturn;
     }


}


