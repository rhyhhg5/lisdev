package com.sinosoft.lis.agentdaily;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;

public class LAContRateUI
{
    public CErrors mErrors = new CErrors();
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    public static void main(String[] args){
        VData cInputData = new VData();
        String cOperate = "UPDATE||MAIN";
        String Priview = "PREVIEW";
        boolean expectedReturn = true;
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "001";
        tGlobalInput.ManageCom = "861100";
        tGlobalInput.ComCode = "001";
        cInputData.add(tGlobalInput);
        String[] testContNo = {"1400000296"};
        LAConRateSchema tLAConRateSchema = new LAConRateSchema();
        LAConRateSet tLAConRateSet = new LAConRateSet();

     //   tLAConRateSchema.setAgentCode("1101000008");
     //   tLAConRateSchema.setOutWorkDate("2005-04-30");
     //   tLAConRateSchema.setPayDate("2005-05-01");


        tLAConRateSet.add(tLAConRateSchema);

        cInputData.add(tLAConRateSchema);
        cInputData.add(Priview);
        LAContRateUI tLAContRateUI = new LAContRateUI();
        boolean actualReturn = tLAContRateUI.submitData(cInputData, cOperate);
    }

    public boolean submitData(VData cInputData,String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        mInputData=(VData)cInputData.clone();

        LAContRateBL tLAContRateBL= new LAContRateBL();
        tLAContRateBL.submitData(mInputData,mOperate);
        //如果有需要处理的错误，则返回
        if (tLAContRateBL.mErrors.needDealError())
        {
          // @@错误处理
          this.mErrors.copyAllErrors(tLAContRateBL.mErrors);
          CError tError = new CError();
          tError.moduleName = "LAContRateUI";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
          this.mErrors.addOneError(tError);
          return false;
        }
        System.out.println("成功了!");
        return true;
    }
}
