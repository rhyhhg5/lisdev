/*
* <p>ClassName: ALAAscriptionBL </p>
* <p>Description: ALAAscriptionBL类文件 </p>
* <p>Copyright: Copyright (c) 2002</p>
* <p>Company: sinosoft </p>
* @Database: 销售管理
* @CreateDate：2003-01-10
 */
package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;


public class ALAAscriptionBL  {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors=new CErrors();
  private VData mResult = new VData();
  /** 往后面传输数据的容器 */
  private VData mInputData;
  /** 全局数据 */
  private GlobalInput mGlobalInput =new GlobalInput() ;
  /** 数据操作字符串 */
  private String mOperate;
  /** 业务处理相关变量 */
  private LAAscriptionSchema mLAAscriptionSchema=new LAAscriptionSchema();
  private LAAscriptionSet mLAAscriptionSet=new LAAscriptionSet();
  private String mAgentNew ="";
  private String mBranchType="";
  private String currentDate = PubFun.getCurrentDate();
  private String currentTime = PubFun.getCurrentTime();
  public ALAAscriptionBL() {
  }
  public static void main(String[] args) {
  }
  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
    //进行业务处理
    if (!dealData())
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ALAAscriptionBL";
      tError.functionName = "submitData";
      tError.errorMessage = "数据处理失败ALAAscriptionBL-->dealData!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    //准备往后台的数据
    if (!prepareOutputData())
      return false;

    System.out.println("Start ALAAscriptionBL Submit...");
    ALAAscriptionBLS tALAAscriptionBLS=new ALAAscriptionBLS();
    tALAAscriptionBLS.submitData(mInputData,cOperate);
    System.out.println("End ALAAscriptionBL Submit...");
    //如果有需要处理的错误，则返回
    if (tALAAscriptionBLS.mErrors.needDealError())
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tALAAscriptionBLS.mErrors);
      CError tError = new CError();
      tError.moduleName = "ALAAscriptionBL";
      tError.functionName = "submitDat";
      tError.errorMessage ="数据提交失败!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    mInputData=null;
    return true;
  }
  /**
   * 根据前面的输入数据，进行BL逻辑处理
   * 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData()
  {
    boolean tReturn =true;
    int tCount=0;
    //循环查询出该保单的归属次数
    if (mOperate.equals("INSERT||MAIN"))
    {
      //非团险保单处理
      if(!this.mBranchType.equals("2"))
      {
        int n=mLAAscriptionSet.size();
        LAAscriptionDB tLAAscriptionDB = new LAAscriptionDB();
        for(int i=0;i<n;i++)
        {
          mLAAscriptionSchema=mLAAscriptionSet.get(i+1);
          //校验是否有附险保单
          String zSQL="select polno from lcpol where mainpolno='"
                     +mLAAscriptionSchema.getPolNo()+"' order by polno";
          SSRS aSSRS=new SSRS();
          ExeSQL cExeSQL =new ExeSQL();
          aSSRS=cExeSQL.execSQL(zSQL);

          for (int t=1;t<=aSSRS.getMaxRow();t++)
          {
            if(t==1)
            {
            tLAAscriptionDB.setPolNo(mLAAscriptionSchema.getPolNo());
            tCount=tLAAscriptionDB.getCount();
            if (tCount==-1)
            {
              // @@错误处理
              this.mErrors.copyAllErrors(tLAAscriptionDB.mErrors);
              CError tError = new CError();
              tError.moduleName = "ALAAscriptionBL";
              tError.functionName = "dealData()";
              tError.errorMessage = "归属次数查询操作失败!";
              this.mErrors .addOneError(tError);
              return false;
            }
            mAgentNew=mLAAscriptionSet.get(i+1).getAgentNew();
            String SQL="select agentcode,mainpolno from lcpol where polno='"
                      +mLAAscriptionSchema.getPolNo()+"'";
            ExeSQL aExeSQL =new ExeSQL();
            SSRS tSSRS = new SSRS();
            tSSRS =aExeSQL.execSQL(SQL);

            mLAAscriptionSet.get(i+1).setAgentOld(tSSRS.GetText(1,1));
            mLAAscriptionSet.get(i+1).setMainPolNo(tSSRS.GetText(1,2));
            mLAAscriptionSet.get(i+1).setValidFlag("1");

            String tSQL="select count(*) from laagent agentcode='"
                       +mLAAscriptionSchema.getAgentOld()+"' and agentstate>='03'";
            ExeSQL tExeSQL  = new ExeSQL ();
            if(tExeSQL.getOneValue(tSQL).equals("1"))
              mLAAscriptionSet.get(i+1).setAClass("01");//离职人员
            else
              mLAAscriptionSet.get(i+1).setAClass("02");//续收员、内勤

            mLAAscriptionSet.get(i+1).setAscriptionCount(tCount+1);
            mLAAscriptionSet.get(i+1).setBranchAttr(AgentPubFun.getAgentBranchAttr(mLAAscriptionSchema.getAgentNew()));
            mLAAscriptionSet.get(i+1).setMakeDate(currentDate);
            mLAAscriptionSet.get(i+1).setMakeTime(currentTime);
            mLAAscriptionSet.get(i+1).setModifyDate(currentDate);
            mLAAscriptionSet.get(i+1).setModifyTime(currentTime);
            mLAAscriptionSet.get(i+1).setAscriptionDate(currentDate);
            mLAAscriptionSet.get(i+1).setOperator(mGlobalInput.Operator);
          }
          else
          {
            LAAscriptionSchema tLAAscriptionSchema =new LAAscriptionSchema();
            tLAAscriptionSchema.setPolNo(aSSRS.GetText(t,1));
            tLAAscriptionSchema.setAgentNew(mAgentNew);
            tLAAscriptionDB.setPolNo(aSSRS.GetText(t,1));
            tCount=tLAAscriptionDB.getCount();
            if (tCount==-1)
            {
              // @@错误处理
              this.mErrors.copyAllErrors(tLAAscriptionDB.mErrors);
              CError tError = new CError();
              tError.moduleName = "ALAAscriptionBL";
              tError.functionName = "dealData()";
              tError.errorMessage = "归属次数查询操作失败!";
              this.mErrors .addOneError(tError);
              return false;
            }
            String SQL="select agentcode,mainpolno from lcpol where polno='"+aSSRS.GetText(t,1)+"'";
            ExeSQL aExeSQL =new ExeSQL();
            SSRS tSSRS = new SSRS();
            tSSRS =aExeSQL.execSQL(SQL);

            tLAAscriptionSchema.setAgentOld(tSSRS.GetText(1,1));
            tLAAscriptionSchema.setMainPolNo(tSSRS.GetText(1,2));
            tLAAscriptionSchema.setValidFlag("1");

            String tSQL="select count(*) from laagent where agentcode='"+tSSRS.GetText(1,1)+"' and agentstate>='03'";
            ExeSQL tExeSQL  = new ExeSQL ();
            if(tExeSQL.getOneValue(tSQL).equals("1"))
              tLAAscriptionSchema.setAClass("01");//离职人员
            else
              tLAAscriptionSchema.setAClass("02");//续收员、内勤

            tLAAscriptionSchema.setAscriptionCount(tCount+1);
            tLAAscriptionSchema.setBranchAttr(AgentPubFun.getAgentBranchAttr(mLAAscriptionSchema.getAgentNew()));
            tLAAscriptionSchema.setMakeDate(currentDate);
            tLAAscriptionSchema.setMakeTime(currentTime);
            tLAAscriptionSchema.setModifyDate(currentDate);
            tLAAscriptionSchema.setModifyTime(currentTime);
            tLAAscriptionSchema.setAscriptionDate(currentDate);
            tLAAscriptionSchema.setOperator(mGlobalInput.Operator);
            mLAAscriptionSet.add(tLAAscriptionSchema);
          }
          }
          //交费表、扎帐表处理
          for (int m=1;m<=aSSRS.getMaxRow();m++)
          {
            ExeSQL aExeSQL  = new ExeSQL ();
            String fSQL="select agentgroup,branchcode from laagent where agentcode='"
                       +mLAAscriptionSchema.getAgentNew()+"'";
            SSRS fSSRS = new SSRS();
            fSSRS=aExeSQL.execSQL(fSQL);

            //修改保单表信息
            String eSQL="update lcpol set agentcode='"+mLAAscriptionSchema.getAgentNew()+"',agentgroup ='"
                       +fSSRS.GetText(1,2)+"',modifydate='"+currentDate+"',modifytime='"+currentTime
                       +"' where polno= '"+aSSRS.GetText(m,1)+"'";
            aExeSQL.execUpdateSQL(eSQL);
            //1.维护交费表中有，但扎帐表中没有的数据
            String aSQL="update ljapayperson a set agentcode ='"+mLAAscriptionSchema.getAgentNew()
                       +"',agentgroup ='"+fSSRS.GetText(1,2)+"',modifydate='"+currentDate+"',modifytime='"
                       +currentTime+"' where polno= '"+aSSRS.GetText(m,1)
                       +"' and not exists (select 'X' from lacommision where polno='"
                       +aSSRS.GetText(m,1)+"' and receiptno=a.payno and dutycode=a.dutycode and payplancode=a.payplancode) ";

            aExeSQL.execUpdateSQL(aSQL);
            //2.维护未算过佣金的保单
            String bSQL="update lacommision a set agentcode ='"+mLAAscriptionSchema.getAgentNew()
                       +"',agentgroup ='"+fSSRS.GetText(1,1)+"',branchcode ='"+fSSRS.GetText(1,2)+"',branchattr='"
                       +AgentPubFun.getAgentBranchAttr(mLAAscriptionSchema.getAgentNew())
                       +"',modifydate='"+currentDate+"',modifytime='"+currentTime+"' where polno= '"+aSSRS.GetText(m,1)
                       +"' and not exists (select 'X' from lawagehistory where managecom =a.managecom and branchtype='"
                       +this.mBranchType+"' and state='14' and wageno=a.wageno) ";
            ExeSQL bExeSQL  = new ExeSQL ();
            bExeSQL.execUpdateSQL(bSQL);
            //3.维护扎帐表中存在的交费表信息
            String cSQL="select receiptno,dutycode,payplancode from lacommision where polno= '"+aSSRS.GetText(m,1)
                       +"' and not exists (select 'X' from lawagehistory where managecom =a.managecom and branchtype='"
                       +this.mBranchType+"' and state='14') ";
            SSRS cSSRS =new SSRS();
            ExeSQL dExeSQL  = new ExeSQL ();
            cSSRS=dExeSQL.execSQL(cSQL);
            for (int o=1;o<=cSSRS.getMaxRow();o++)
            {
              String dSQL="update ljapayperson a set agentcode ='"+mLAAscriptionSchema.getAgentNew()
                      +"',agentgroup ='"+fSSRS.GetText(1,2)+"',modifydate='"+currentDate+"',modifytime='"
                      +currentTime+"' where polno= '"+aSSRS.GetText(m,1)
                      +"' and receiptno='"+cSSRS.GetText(o,1)+"' and dutycode='"+cSSRS.GetText(o,2)
                      +" and payplancode='"+cSSRS.GetText(o,3)+" ";
              ExeSQL eExeSQL  = new ExeSQL ();
              eExeSQL.execUpdateSQL(dSQL);
            }
          }
        }
      }
      //团单处理
      else
      {
        int n=mLAAscriptionSet.size();
        LAAscriptionDB tLAAscriptionDB = new LAAscriptionDB();
        for(int i=0;i<n;i++)
        {
          mLAAscriptionSchema=mLAAscriptionSet.get(i+1);
          tLAAscriptionDB.setPolNo(mLAAscriptionSchema.getPolNo());
          tLAAscriptionDB.setMainPolNo("");
          tCount=tLAAscriptionDB.getCount();
          if (tCount==-1)
          {
            // @@错误处理
            this.mErrors.copyAllErrors(tLAAscriptionDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "ALAAscriptionBL";
            tError.functionName = "dealData()";
            tError.errorMessage = "归属次数查询操作失败!";
            this.mErrors .addOneError(tError);
            return false;
          }
          mAgentNew=mLAAscriptionSet.get(i+1).getAgentNew();
          mLAAscriptionSet.get(i+1).setMainPolNo(mLAAscriptionSchema.getPolNo());
          mLAAscriptionSet.get(i+1).setValidFlag("1");

          String tSQL="select count(*) from laagent where agentcode='"+mLAAscriptionSchema.getAgentOld()
                     +"' and agentstate>='03'";
          ExeSQL tExeSQL  = new ExeSQL ();
          if(tExeSQL.getOneValue(tSQL).equals("1"))
            mLAAscriptionSet.get(i+1).setAClass("01");//离职人员
          else
            mLAAscriptionSet.get(i+1).setAClass("02");//续收员、内勤

          mLAAscriptionSet.get(i+1).setAscriptionCount(tCount+1);
          mLAAscriptionSet.get(i+1).setBranchAttr(AgentPubFun.getAgentBranchAttr(mLAAscriptionSchema.getAgentNew()));
          mLAAscriptionSet.get(i+1).setMakeDate(currentDate);
          mLAAscriptionSet.get(i+1).setMakeTime(currentTime);
          mLAAscriptionSet.get(i+1).setModifyDate(currentDate);
          mLAAscriptionSet.get(i+1).setModifyTime(currentTime);
          mLAAscriptionSet.get(i+1).setAscriptionDate(currentDate);
          mLAAscriptionSet.get(i+1).setOperator(mGlobalInput.Operator);

          ExeSQL aExeSQL  = new ExeSQL ();
          String fSQL="select agentgroup,branchcode from laagent where agentcode='"
                     +mLAAscriptionSchema.getAgentNew()+"'";
          SSRS fSSRS = new SSRS();
          fSSRS=aExeSQL.execSQL(fSQL);
          //维护交费表中有，但扎帐表中没有的数据
          //修改保单表信息
          String eSQL="update lcgrppol set agentcode='"+mLAAscriptionSchema.getAgentNew()+"',agentgroup ='"
                     +fSSRS.GetText(1,2)+"',modifydate='"+currentDate+"',modifytime='"+currentTime
                     +"' where grppolno= '"+mLAAscriptionSchema.getPolNo()+"'";
          aExeSQL.execUpdateSQL(eSQL);
          String aSQL="update LJAPayGrp a set agentcode ='"+mLAAscriptionSchema.getAgentNew()
                     +"',agentgroup='"+fSSRS.GetText(1,2)+"',modifydate='"+currentDate+"',modifytime='"
                     +currentTime+"' where grppolno= '"+mLAAscriptionSchema.getPolNo()
                     +"' and not exists (select 'X' from lacommision where polno='"+mLAAscriptionSchema.getPolNo()
                     +"' and receiptno=a.payno) ";
          aExeSQL.execUpdateSQL(aSQL);
          //维护未算过佣金的保单
          String bSQL="update lacommision a set agentcode ='"+mLAAscriptionSchema.getAgentNew()
                     +"',agentgroup ='"+fSSRS.GetText(1,1)+"',branchcode ='"+fSSRS.GetText(1,2)+"',branchattr='"
                     +AgentPubFun.getAgentBranchAttr(mLAAscriptionSchema.getAgentNew())
                     +"',modifydate='"+currentDate+"',modifytime='"+currentTime+"' where polno= '"+mLAAscriptionSchema.getPolNo()
                     +"' and not exists (select 'X' from lawagehistory where managecom =a.managecom and branchtype='"
                     +this.mBranchType+"' and state='14' and wageno=a.wageno) ";
          ExeSQL bExeSQL  = new ExeSQL ();
          bExeSQL.execUpdateSQL(bSQL);
          //维护扎帐表中存在的交费表信息,同时未算过佣金
          String cSQL="select receiptno from lacommision a where polno= '"+mLAAscriptionSchema.getPolNo()
                     +"' and not exists (select 'X' from lawagehistory where managecom =a.managecom and branchtype='"
                     +this.mBranchType+"' and state='14' and wageno=a.wageno) ";
          SSRS cSSRS =new SSRS();
          ExeSQL dExeSQL  = new ExeSQL ();
          cSSRS=dExeSQL.execSQL(cSQL);
          for (int o=1;o<=cSSRS.getMaxRow();o++)
          {
            String dSQL="update LJAPayGrp a set agentcode ='"+mLAAscriptionSchema.getAgentNew()
                    +"',agentgroup='"+fSSRS.GetText(1,2)+"',modifydate='"+currentDate+"',modifytime='"
                    +currentTime+"' where grppolno= '"+mLAAscriptionSchema.getPolNo()
                    +"' and payno='"+cSSRS.GetText(o,1)+"'";
            ExeSQL eExeSQL  = new ExeSQL ();
            eExeSQL.execUpdateSQL(dSQL);
          }
        }
      }
    }
    tReturn=true;
    return tReturn ;
  }
  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    this.mLAAscriptionSet.set((LAAscriptionSet)cInputData.getObjectByObjectName("LAAscriptionSet",0));
    this.mBranchType=(String)cInputData.get(2);
    return true;
  }

  private boolean prepareOutputData()
  {
    try
    {
      this.mInputData=new VData();
      this.mInputData.add(this.mGlobalInput);
      this.mInputData.add(this.mLAAscriptionSchema);
      this.mInputData.add(this.mLAAscriptionSet);
      this.mInputData.add(this.mBranchType);
      System.out.println("Set有多大？？？？"+mLAAscriptionSet.size());
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="ALAAscriptionBL";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }
  public VData getResult()
  {
    return this.mResult;
  }
}
