package com.sinosoft.lis.agentdaily;

import com.sinosoft.lis.agentdaily.LAAddBSubPerQueryBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.LARateCommisionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class LAAddBSubReviewUI {
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	/** 数据操作字符串 */
	private String mOperate = "";
	//业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	private LARateCommisionSet mLARateCommisionSet = new LARateCommisionSet();
	private LAAddBSubReviewBL mLAAddBSubReviewBL = new LAAddBSubReviewBL();

	public static void main(String[] args)
	{

	}

	/**
	 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate)
	{
		System.out.println("Begin LAAddBSubReviewUI.submitData.........");
		//将操作数据拷贝到本类中
		this.mErrors.clearErrors();
		this.mOperate = cOperate;
		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData))
		{
			return false;
		}
		//进行业务处理
		if (!dealData())
		{
			return false;
		}
		//准备往后台的数据
		if (!prepareOutputData())
		{
			return false;
		}
		System.out.println("Start LAAddBSubReviewUI Submit...");
                try
                {
                	mLAAddBSubReviewBL.submitData(mInputData, mOperate);
                }
                catch(Exception ex){
                  ex.toString();
                }
		System.out.println("End LAAddBSubReviewUI Submit...");
		//如果有需要处理的错误，则返回
		if (mLAAddBSubReviewBL.mErrors.needDealError())
		{
			// @@错误处理
			this.mErrors.copyAllErrors(mLAAddBSubReviewBL.mErrors);
			CError tError = new CError();
			tError.moduleName = "LAAddBSubReviewUI";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

	/**
	 * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData)
	{
		//全局变量
		try
		 {
			 System.out.println("Begin LAAddBSubReviewUI.getInputData.........");
			 mInputData=(VData)cInputData.clone();
		  }
		  catch(Exception ex)
		  {
			  // @@错误处理
			  CError tError = new CError();
			  tError.moduleName = "LAAddBSubReviewUI";
			  tError.functionName = "getInputData";
			  tError.errorMessage = "在读取处理所需要的数据时出错。";
			  this.mErrors.addOneError(tError);
			  return false;
		  }
		return true;
	}

	/**
	 * 根据前面的输入数据，进行UI逻辑处理
	 * 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData()
	{
		System.out.println("Begin LAAddBSubReviewUI.dealData.........");
		return true;
	}


	/**
	 * 准备往后层输出所需要的数据
	 * 输出：如果准备数据时发生错误则返回false,否则返回true
	 */
	private boolean prepareOutputData()
	{
		try
		{
			System.out.println("Begin LAAddBSubReviewUI.prepareOutputData.........");
		}
		catch (Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "LAAddBSubReviewUI";
			tError.functionName = "prepareOutputData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
}
