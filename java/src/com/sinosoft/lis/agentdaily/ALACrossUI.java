package com.sinosoft.lis.agentdaily;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: 销售管理系统</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author xjh
 * @version 1.0
 */
public class ALACrossUI
{
    public CErrors mErrors=new CErrors();
    public VData mResult =new VData();
    private VData mInputData=new VData();
    private String mOperate="";
    public ALACrossUI()
    {
    }
    public boolean submitData(VData cInputData,String cOperate)
    {
        System.out.println("ALACrossUI:得到结果12324456|");
        mOperate=cOperate;
        this.mInputData =(VData)cInputData.clone() ;
        ALACrossBL tALACrossBL=new ALACrossBL();
        try
        {
            if (!tALACrossBL.submitData(mInputData, mOperate))
            {
                this.mErrors.copyAllErrors(tALACrossBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "ALACrossUI";
                tError.functionName = "submitDat";
                tError.errorMessage = "BL类处理失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        mResult=tALACrossBL.getResult();
        return true;
    }

    public VData getResult()
    {
         System.out.println("ALACrossUI:得到结果|");
        return mResult;
    }

}
