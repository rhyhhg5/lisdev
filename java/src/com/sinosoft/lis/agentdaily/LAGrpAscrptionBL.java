/*
 * <p>ClassName: LABatchAuthorizeBL </p>
 * <p>Description: ALAAuthorizeUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database: 销售管理
 * @CreateDate：2003-01-15
 */
package com.sinosoft.lis.agentdaily;

import java.sql.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.vdb.LAGrpCommisionDetailBDBSet;
                           //  LAGrpCommisionDetailBDBSet


public class LAGrpAscrptionBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LAGrpCommisionDetailSet mLAGrpCommisionDetailSet = new
            LAGrpCommisionDetailSet(); // 接收前台传入的数据

    private LAAscriptionSet mInLAAsctiptionSet = new LAAscriptionSet(); // 要插入的新数据
    private LAAscriptionSet mUpLAAsctiptionSet = new LAAscriptionSet(); //  修改的数据

    private LACommisionSet mupLACommisionSet = new LACommisionSet(); //删除的数据
    private LACommisionBSet mLACommisionBSet = new LACommisionBSet(); //备份的数据
    private LACommisionSet mLACommisionSet = new LACommisionSet(); //查询的数据
    private MMap mMap = new MMap();
    private VData mOutputDate = new VData();
    private String mGrpContNo = "";
    private String currentDate = PubFun.getCurrentDate();
    private String currentTime = PubFun.getCurrentTime();

    private int mCount; //归属的总人数
    private String mAscripDate;
    private String mOldAgentCode; //原主业务员
    private String mNewAgentGroup; //新的主业务员
    private String mContType;//保单类型：1--个单，2--团单

    private String mEdorNo = PubFun1.CreateMaxNo("EdorNo", 20); //一条记录生成一个转储号;
    private String mSql_lcgrpcont;
    private String mSql_lcgrppol;
    private String mSql_ljspaygrp;
    private String mSql_lccont;
    private String mSql_lcpol;
    private String mSql_ljspayperson;
    private String mSql_ljspay;
    private String mSql_ljsgetendorse;
    private String mSql_lbgrpcont;
    private String mSql_lbgrppol;
    private String mSql_lbcont;
    private String mSql_lbpol;
    private String mSql_lccontreceive;
    private String mSql_lpgrpcont;
    private String mSql_lpgrppol;
    private String mSql_lpcont;
    private String mSql_lppol;

    private String mSql_ljapaygrp;
    private String mSql_ljapayperson;
    private String mSql_ljagetendorse;
    private String mSql_ljapay;
    
    private String mSql_lccontgetpol;
    
    private int nCnt = 0;

    /**
     * 传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行有关验证
        if (!check()) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mOutputDate, "");
        //如果有需要处理的错误，则返回
        if (tPubSubmit.mErrors.needDealError()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LAGrpAscrptionBL";
            tError.functionName = "submitDat";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    /**
     * 准备往后台的数据
     * @return boolean mInLAAsctiptionSet
     */
    private boolean prepareOutputData() {

        //处理LAAsctiption
        this.mMap.put(this.mInLAAsctiptionSet, "INSERT");
        this.mMap.put(this.mUpLAAsctiptionSet, "UPDATE");

        //处理LACommision
        this.mMap.put(this.mupLACommisionSet, "UPDATE");
        this.mMap.put(this.mLACommisionBSet, "INSERT");

        //处理sql
        mMap.put(mSql_lcgrpcont, "UPDATE");
        mMap.put(mSql_lcgrppol, "UPDATE");
        mMap.put(mSql_lbgrpcont, "UPDATE");
        mMap.put(mSql_lbgrppol, "UPDATE");
        mMap.put(mSql_lpgrpcont, "UPDATE");
        mMap.put(mSql_lpgrppol, "UPDATE");
        
        mMap.put(mSql_ljspay, "UPDATE");
        mMap.put(mSql_lccontreceive, "UPDATE");
        mMap.put(mSql_ljspaygrp, "UPDATE");
        mMap.put(mSql_lccont, "UPDATE");
        mMap.put(mSql_lcpol, "UPDATE");
        mMap.put(mSql_lbcont, "UPDATE");
        mMap.put(mSql_lbpol, "UPDATE");
        mMap.put(mSql_lpcont, "UPDATE");
        mMap.put(mSql_lppol, "UPDATE");
        //modify by zhuxt
        mMap.put(mSql_lccontgetpol, "UPDATE");


        mMap.put(mSql_ljspayperson, "UPDATE");
        mMap.put(mSql_ljsgetendorse, "UPDATE");
        mMap.put(mSql_ljapaygrp, "UPDATE");
        mMap.put(mSql_ljapayperson, "UPDATE");
        mMap.put(mSql_ljagetendorse, "UPDATE");
//      modify by zhuxt
        mMap.put(mSql_ljapay, "UPDATE");

        mOutputDate.add(mMap);

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData pmInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) pmInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLAGrpCommisionDetailSet.set((LAGrpCommisionDetailSet) pmInputData.
                                     getObjectByObjectName(
                                             "LAGrpCommisionDetailSet", 0));
        this.mGrpContNo = (String) pmInputData.getObject(0);
        this.mAscripDate = (String) pmInputData.getObject(3);        
        this.mOldAgentCode = (String) pmInputData.getObject(4);
        this.mContType=(String) pmInputData.getObject(5);
        System.out.println("保单号：[ " + this.mGrpContNo + " ]");
        System.out.println("保单类型:" + this.mContType );
        if (mGlobalInput == null && mLAGrpCommisionDetailSet == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ALAAuthorizeUI";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的信息！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    /**
     * 进行有关验证
     *    1、归属日校验，只能为最大薪资确认月次月
     *    2、不能跨团队进行保单归属操作
     * @return boolean
     */
    private boolean check() {
        String tsql = "";
        //校验归属日期必须为某个月1日
        String tAscripDay =AgentPubFun.formatDate(mAscripDate, "yyyy-MM-dd");//转换成日期格式
        String tDay = tAscripDay.substring(tAscripDay.lastIndexOf("-") + 1);//获得天数
        if (!tDay.equals("01")) {
            CError tError = new CError();
            tError.moduleName = "LAGrpAscrptionBL";
            tError.functionName = "check";
            tError.errorMessage = "归属日期必须是从某个月的1号开始!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        //校验归属日期所属年月必须大于最大薪资月
        String tAscripMonth = AgentPubFun.formatDate(mAscripDate, "yyyyMM");
        tsql = "select max(WageNo) from LAWageHistory where managecom = '" +
               mLAGrpCommisionDetailSet.get(1).getMngCom()
               + "' and branchtype='2' and BranchType2='01' and state='14'";
        ExeSQL tExeSQL = new ExeSQL();
        String strWage = tExeSQL.getOneValue(tsql);
        if (tExeSQL.mErrors.needDealError()) {

            CError tError = new CError();
            tError.moduleName = "LAGrpAscrptionBL";
            tError.functionName = "check";
            tError.errorMessage = "查询数据 LAWageHistory 出错！";
            this.mErrors.addOneError(tError);

            return false;
        }
        if (strWage != null && !strWage.equals("") && !strWage.equals("0")) {
            if (strWage.compareTo(tAscripMonth) >= 0) {
                CError tError = new CError();
                tError.moduleName = "LAGrpAscrptionBL";
                tError.functionName = "check";
                tError.errorMessage = strWage + "已经薪资确认,归属日期必须在下个月！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        //业务员必须变更
        if(this.mContType.equals("2")){
        tsql = "select grpcontno from lcgrpcont   where grpcontno='" +
               mGrpContNo + "' and agentcode='" + mLAGrpCommisionDetailSet.get(1).getAgentCode() + "' "
               + " union "
               + "select grpcontno from lBgrpcont   where grpcontno='" +
               mGrpContNo + "' and agentcode='" + mLAGrpCommisionDetailSet.get(1).getAgentCode() + "' ";
        }
        else if(this.mContType.equals("1")){
        	tsql=" select contno from lccont where contno='"+mGrpContNo+"' and agentcode='"+ mLAGrpCommisionDetailSet.get(1).getAgentCode() + "' "
            +"union select contno from lbcont where contno='"+mGrpContNo+"' and agentcode='"+ mLAGrpCommisionDetailSet.get(1).getAgentCode() + "' ";

        }
        
        tExeSQL = new ExeSQL();
        String tResult = tExeSQL.getOneValue(tsql);
        if (tExeSQL.mErrors.needDealError()) {

            CError tError = new CError();
            tError.moduleName = "LAGrpAscrptionBL";
            tError.functionName = "check";
            tError.errorMessage = "查询数据 LPGrpEdorMain 出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tResult != null && !tResult.equals("") ||
        		tResult.equals("0")) {
            CError tError = new CError();
            tError.moduleName = "LAGrpAscrptionBL";
            tError.functionName = "check";
            tError.errorMessage = mGrpContNo + "保单业务员必须变更！";
            this.mErrors.addOneError(tError);
            System.out.println(mGrpContNo + "保单业务员必须变更！");
            return false;
        }
        //校验是否变更前后业务员是否同属一个团队
        tsql="select '1' from laagent where agentcode = '"+mLAGrpCommisionDetailSet.get(1).getAgentCode()+"'" +
        		"and agentgroup = (select agentgroup from laagent where agentcode = '"+this.mOldAgentCode+"')";
        tExeSQL = new ExeSQL();
        String tCheck = tExeSQL.getOneValue(tsql);
        if (tExeSQL.mErrors.needDealError()) {

            CError tError = new CError();
            tError.moduleName = "LAGrpAscrptionBL";
            tError.functionName = "check";
            tError.errorMessage = "查询数据 laagent 出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tCheck == null && tCheck.equals("")) {
            CError tError = new CError();
            tError.moduleName = "LAGrpAscrptionBL";
            tError.functionName = "check";
            tError.errorMessage =  "保单变更前后业务员不属于同一团队，烦请页面重新选择归属业务员！";
            this.mErrors.addOneError(tError);
            System.out.println(mGrpContNo + "保单变更前后业务员不属于同一团队！");
            return false;
        }
        return true;
    }

    /**
     * 进行业务处理
     * @return boolean
     */
    private boolean dealData() {
     if(this.mContType.equals("2")){
        //团单查询扎帐表
        if (!queryGrpCommision(mGrpContNo)) {
            return false;
        }
        mCount = mLAGrpCommisionDetailSet.size();
        for (int i = 1; i <= mCount; i++) {

            LAGrpCommisionDetailSchema tLAGrpCommisionDetailSchema = new LAGrpCommisionDetailSchema();
            tLAGrpCommisionDetailSchema = mLAGrpCommisionDetailSet.get(i);
            String tGrpContNo = tLAGrpCommisionDetailSchema.getGrpContNo();
            String tAgentCode = tLAGrpCommisionDetailSchema.getAgentCode();//归属后人员编码
                //查询团单归属后业务员的agentgroup
                if (!queryAgentGroup(tAgentCode)) {
                    return false;
                }
               // 团单层数据修改
                if (!dealGrpCont(tGrpContNo, tAgentCode)) {
                    return false;
                }
                //处理团单实收表
                if (!dealGrpLjaPay(tGrpContNo, tAgentCode)) {
                    return false;
                }
                // 处理团单单归属表
                if (!dealGrpAscript(tGrpContNo, tAgentCode)) {
                    return false;
                }
            //团单拆分扎帐表
            if (!dealCommision(tLAGrpCommisionDetailSchema)) {
                return false;
            }
         }
       }else if(this.mContType.equals("1")){
    	   //个单查询扎帐表
    	   if(!queryCommision(mGrpContNo)){
    		   return false;
    	   }
    	   int tcount=mLAGrpCommisionDetailSet.size();
    	   for(int j=1;j<=tcount;j++){
    		   LAGrpCommisionDetailSchema TLAGrpCommisionDetailSchema = new LAGrpCommisionDetailSchema();  
    		   TLAGrpCommisionDetailSchema= mLAGrpCommisionDetailSet.get(j);
    		   String tcontno=TLAGrpCommisionDetailSchema.getGrpContNo();
    		   String Tagentcode=TLAGrpCommisionDetailSchema.getAgentCode();
    		 //查询个单归属后业务员的agentgroup
               if (!queryAgentGroup(Tagentcode)) {
                   return false;
               } 
            // 个单层数据修改
               if (!dealCont(tcontno, Tagentcode)) {
                   return false;
               } 
           // 处理个单实收表
               if(!dealLjaPay(tcontno, Tagentcode)){
            	   return false;
               }
               // 处理团单单归属表
               if (!dealAscript(tcontno, Tagentcode)) {
                   return false;
               }
             //个单拆分扎帐表
               if (!dealCommision(TLAGrpCommisionDetailSchema)) {
                   return false;
               }
    	   }
       }
        return true;
    }
    /**
     * 团队信息查询
     * @param cAgentCode
     * @return
     */
    private boolean queryAgentGroup(String cAgentCode) {
        LAAgentDB tLAAgentDB = new LAAgentDB();
        tLAAgentDB.setAgentCode(cAgentCode);
        if (!tLAAgentDB.getInfo()) {
            CError tError = new CError();
            tError.moduleName = "LAGrpAscrptionBL";
            tError.functionName = "queryAgentCode";
            tError.errorMessage = "查询" + cAgentCode + "的团队信息出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        mNewAgentGroup = tLAAgentDB.getAgentGroup();
        return true;
    }

    /*处理团单单信息,修改
     *lcgrpcont、lcgrppol、ljspaygrp、lccont、lcpol、ljspayperson、ljsgetendorse
     *表的agentcode
     */
    private boolean dealGrpCont(String cGrpContNo, String cAgentCode) {
        mSql_lcgrpcont = " update lcgrpcont set agentcode='" + cAgentCode +
                         "',agentgroup='"
                         + mNewAgentGroup + "' ,modifydate='" + currentDate +
                         "' ,modifytime='" + currentTime + "' where GrpContNo='" +
                         cGrpContNo +
                         "'";
        mSql_lcgrppol = " update lcgrppol set agentcode='" + cAgentCode +
                        "',agentgroup='"
                        + mNewAgentGroup + "' ,modifydate='" + currentDate +
                        "' ,modifytime='" + currentTime + "' where GrpContNo='" +
                        cGrpContNo +
                        "'";
        mSql_ljspaygrp = " update ljspaygrp set agentcode='" + cAgentCode +
                         "',agentgroup='"
                         + mNewAgentGroup + "' ,modifydate='" + currentDate +
                         "' ,modifytime='" + currentTime + "' where GrpContNo='" +
                         cGrpContNo +
                         "'";
        mSql_lccont = " update lccont set agentcode='" + cAgentCode +
                      "',agentgroup='"
                      + mNewAgentGroup + "' ,modifydate='" + currentDate +
                      "' ,modifytime='" + currentTime + "' where GrpContNo='" +
                      cGrpContNo +
                      "'";
        mSql_lcpol = " update lcpol set agentcode='" + cAgentCode +
                     "',agentgroup='"
                     + mNewAgentGroup + "' ,modifydate='" + currentDate +
                     "' ,modifytime='" + currentTime + "' where GrpContNo='" +
                     cGrpContNo +
                     "'";
        mSql_ljspayperson = " update ljspayperson set agentcode='" + cAgentCode +
                            "',agentgroup='"
                            + mNewAgentGroup + "',modifydate='" + currentDate +
                            "' ,modifytime='" + currentTime + "' where GrpContNo='" +
                            cGrpContNo +
                            "'";
        mSql_ljsgetendorse = " update ljsgetendorse set agentcode='" +
                             cAgentCode +
                             "',agentgroup='"
                             + mNewAgentGroup + "',modifydate='" + currentDate +
                             "' ,modifytime='" + currentTime +
                             "' where GrpContNo='" +
                             cGrpContNo +
                             "'";
        
        //续保后可以能回退，把b表的数据复制到c表，所以也要被分续保回退的数据
        String tSql = "select prtno from  lcgrpcont where grpcontno='" +
                      cGrpContNo
                      +
                      "'  ";
        ExeSQL tExeSQL = new ExeSQL();
        String tPrtNo = tExeSQL.getOneValue(tSql);

        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LAGrpAscrptionBL";
            tError.functionName = "dealCont";
            tError.errorMessage = "查询保单原业务员出错";
            this.mErrors.addOneError(tError);
            return false;
        }
        mSql_lbgrpcont = " update lbgrpcont set agentcode='" +
                         cAgentCode +
                         "',agentgroup='"
                         + mNewAgentGroup + "',modifydate='" + currentDate +
                         "' ,modifytime='" + currentTime + "' where prtno='" +
                         tPrtNo +
                         "'";
        mSql_lbgrppol = " update lbgrppol set agentcode='" +
                        cAgentCode +
                        "',agentgroup='"
                        + mNewAgentGroup + "',modifydate='" + currentDate +
                        "' ,modifytime='" + currentTime + "' where prtno='" +
                        tPrtNo +
                        "'";
        mSql_lbcont = " update lbcont set agentcode='" +
                      cAgentCode +
                      "',agentgroup='"
                      + mNewAgentGroup + "',modifydate='" + currentDate +
                      "' ,modifytime='" + currentTime + "' where prtno='" +
                      tPrtNo +
                      "'";
        mSql_lbpol = " update lbpol set agentcode='" +
                     cAgentCode +
                     "',agentgroup='"
                     + mNewAgentGroup + "',modifydate='" + currentDate +
                     "' ,modifytime='" + currentTime + "' where prtno='" +
                     tPrtNo +
                     "'";
        //修改保全p表，p表可能会回写c表
        mSql_lpgrpcont = " update lpgrpcont set agentcode='" +
                         cAgentCode +
                         "',agentgroup='"
                         + mNewAgentGroup + "',modifydate='" + currentDate +
                         "' ,modifytime='" + currentTime + "' where prtno='" +
                         tPrtNo +
                         "'";
        mSql_lpgrppol = " update lpgrppol set agentcode='" +
                        cAgentCode +
                        "',agentgroup='"
                        + mNewAgentGroup + "',modifydate='" + currentDate +
                        "' ,modifytime='" + currentTime + "' where prtno='" +
                        tPrtNo +
                        "'";
        mSql_lpcont = " update lpcont set agentcode='" +
                      cAgentCode +
                      "',agentgroup='"
                      + mNewAgentGroup + "',modifydate='" + currentDate +
                      "' ,modifytime='" + currentTime + "' where prtno='" +
                      tPrtNo +
                      "'";
        mSql_lppol = " update lppol set agentcode='" +
                     cAgentCode +
                     "',agentgroup='"
                     + mNewAgentGroup + "',modifydate='" + currentDate +
                     "' ,modifytime='" + currentTime + "' where prtno='" +
                     tPrtNo +
                     "'";
      //modify by zhuxt  修改回执回销业务表lccontgetpol
        String tNameSQL = "select name from laagent where agentcode = '" + cAgentCode +"'";
        String tAgentname = tExeSQL.getOneValue(tNameSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LAGrpAscrptionBL";
            tError.functionName = "dealCont";
            tError.errorMessage = "查询保单原业务员姓名出错";
            this.mErrors.addOneError(tError);
            return false;
        }
        mSql_lccontgetpol = " update lccontgetpol set agentcode='" +
					        cAgentCode +
					        "',agentname='"
					        + tAgentname + "',modifydate='" + currentDate +
					        "' ,modifytime='" + currentTime + "' where contno='" +
					        cGrpContNo +
					        "'";


        return true;
    }
    /*处理个单单信息,修改
     *lccont、lcpol、ljspaygrp、ljspayperson、ljsgetendorse
     *表的agentcode
     */
    private boolean dealCont(String cContNo, String cAgentCode) {
        mSql_lccont = " update lccont set agentcode='" + cAgentCode +
                      "',agentgroup='"
                      + mNewAgentGroup + "' ,modifydate='" + currentDate +
                      "' ,modifytime='" + currentTime + "' where ContNo='" +
                      cContNo +
                      "'";
        mSql_lcpol = " update lcpol set agentcode='" + cAgentCode +
                     "',agentgroup='"
                     + mNewAgentGroup + "' ,modifydate='" + currentDate +
                     "' ,modifytime='" + currentTime + "' where ContNo='" +
                     cContNo +
                     "'";
        mSql_ljspayperson = " update ljspayperson set agentcode='" + cAgentCode +
                            "',agentgroup='"
                            + mNewAgentGroup + "',modifydate='" + currentDate +
                            "' ,modifytime='" + currentTime + "' where ContNo='" +
                            cContNo +
                            "'";
        mSql_ljsgetendorse = " update ljsgetendorse set agentcode='" +
                             cAgentCode +
                             "',agentgroup='"
                             + mNewAgentGroup + "',modifydate='" + currentDate +
                             "' ,modifytime='" + currentTime +
                             "' where ContNo='" +
                             cContNo +
                             "'";
        mSql_ljspay = " update ljspay set agentcode='" + cAgentCode +
        "',agentgroup='"
        + mNewAgentGroup + "',modifydate='" + currentDate +
        "' ,modifytime='" + currentTime + "' where OtherNo='" +
        cContNo +
        "'";
        //续保后可以能回退，把b表的数据复制到c表，所以也要被分续保回退的数据
        String tSql = "select prtno from  lccont where contno='" +cContNo+"'  ";
        ExeSQL tExeSQL = new ExeSQL();
        String tPrtNo = tExeSQL.getOneValue(tSql);

        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LAGrpAscrptionBL";
            tError.functionName = "dealCont";
            tError.errorMessage = "查询个单原业务员出错";
            this.mErrors.addOneError(tError);
            return false;
        }
        mSql_lbcont = " update lbcont set agentcode='" +
                      cAgentCode +
                      "',agentgroup='"
                      + mNewAgentGroup + "',modifydate='" + currentDate +
                      "' ,modifytime='" + currentTime + "' where prtno='" +
                      tPrtNo +
                      "'";
        mSql_lbpol = " update lbpol set agentcode='" +
                     cAgentCode +
                     "',agentgroup='"
                     + mNewAgentGroup + "',modifydate='" + currentDate +
                     "' ,modifytime='" + currentTime + "' where prtno='" +
                     tPrtNo +
                     "'";
        //修改保全p表，p表可能会回写c表
        mSql_lpcont = " update lpcont set agentcode='" +
                      cAgentCode +
                      "',agentgroup='"
                      + mNewAgentGroup + "',modifydate='" + currentDate +
                      "' ,modifytime='" + currentTime + "' where prtno='" +
                      tPrtNo +
                      "'";
        mSql_lppol = " update lppol set agentcode='" +
                     cAgentCode +
                     "',agentgroup='"
                     + mNewAgentGroup + "',modifydate='" + currentDate +
                     "' ,modifytime='" + currentTime + "' where prtno='" +
                     tPrtNo +
                     "'";
      //modify by zhuxt  修改回执回销业务表lccontgetpol
        String tNameSQL = "select name from laagent where agentcode = '" + cAgentCode +"'";
        String tAgentname = tExeSQL.getOneValue(tNameSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LAGrpAscrptionBL";
            tError.functionName = "dealCont";
            tError.errorMessage = "查询保单原业务员姓名出错";
            this.mErrors.addOneError(tError);
            return false;
        }
        mSql_lccontgetpol = " update lccontgetpol set agentcode='" +
					        cAgentCode +
					        "',agentname='"
					        + tAgentname + "',modifydate='" + currentDate +
					        "' ,modifytime='" + currentTime + "' where contno='" +
					        cContNo +
					        "'";
        mSql_lccontreceive = " update lccontreceive set agentcode='" +
        cAgentCode +
  		"',agentname='"
  		+ tAgentname + "',modifydate='" + PubFun.getCurrentDate() +
  		"' ,modifytime='" + PubFun.getCurrentTime() + "' where contno='" +
  		cContNo +
  		"'";
        return true;
    }
    /*处理团单信息,修改
     * ljagetendorse、ljapayperson、ljapaygrp
     *表的agentcode
     */
    private boolean dealGrpLjaPay(String cGrpContNo, String cAgentCode) {
    	
        mSql_ljagetendorse = " update ljagetendorse set agentcode='" +
                             cAgentCode +
                             "',agentgroup='"
                             + mNewAgentGroup + "' ,modifydate='"+currentDate+"' ,modifytime='"+currentTime+"' where GrpContNo='" +
                             cGrpContNo +
                             "'  and makedate>='" + mAscripDate + "' ";
        mSql_ljapayperson = " update ljapayperson set agentcode='" + cAgentCode +
                            "',agentgroup='"
                            + mNewAgentGroup + "',modifydate='"+currentDate+"' ,modifytime='"+currentTime+"' where GrpContNo='" +
                            cGrpContNo +
                            "'  and makedate>='" + mAscripDate + "' ";
        mSql_ljapaygrp = " update ljapaygrp set agentcode='" + cAgentCode +
                         "',agentgroup='"
                         + mNewAgentGroup + "',modifydate='"+currentDate+"' ,modifytime='"+currentTime+"' where GrpContNo='" + cGrpContNo +
                         "'  and makedate>='" + mAscripDate + "' ";
//      modify by zhuxt
        mSql_ljapay  = " update ljapay set agentcode='" +
				        cAgentCode +
				        "',agentgroup='"
				        + mNewAgentGroup + "',modifydate='" + currentDate +
				        "' ,modifytime='" + currentTime +
				        "' where Incomeno='" +
				        cGrpContNo +
				        "'  and makedate>='" + mAscripDate + "' ";
        return true;
    }
    /*处理个单信息,修改
     * ljagetendorse、ljapayperson、ljapaygrp
     *表的agentcode
     */
    private boolean dealLjaPay(String cContNo, String cAgentCode) {
    	
        mSql_ljagetendorse = " update ljagetendorse set agentcode='" +
                             cAgentCode +
                             "',agentgroup='"
                             + mNewAgentGroup + "' ,modifydate='"+currentDate+"' ,modifytime='"+currentTime+"' where ContNo='" +
                             cContNo +
                             "'  and makedate>='" + mAscripDate + "' ";
        mSql_ljapayperson = " update ljapayperson set agentcode='" + cAgentCode +
                            "',agentgroup='"
                            + mNewAgentGroup + "',modifydate='"+currentDate+"' ,modifytime='"+currentTime+"' where ContNo='" +
                            cContNo +
                            "'  and makedate>='" + mAscripDate + "' ";
//      modify by zhuxt
        mSql_ljapay  = " update ljapay set agentcode='" +
				        cAgentCode +
				        "',agentgroup='"
				        + mNewAgentGroup + "',modifydate='" + currentDate +
				        "' ,modifytime='" + currentTime +
				        "' where Incomeno='" +
				        cContNo +
				        "'  and makedate>='" + mAscripDate + "' ";
        return true;
    } 

    /**
     * 处理团单归属表
     * 如果该保单有validflag='N'(有效) 的数据,则置为无效Y,grpcontno在laascription表中只有一
     * 条有效的数据
     */
    private boolean dealGrpAscript(String cGrpContNo, String cMianAgentCode) {
        String tSQL = "select * from laascription where grpcontno='" +
                      cGrpContNo +
                      "' and validflag='N' ";
        System.out.println(tSQL);
        LAAscriptionDB tLAAscriptionDB = new LAAscriptionDB();
        LAAscriptionSet tLAAscriptionSet = new LAAscriptionSet();

        tLAAscriptionSet = tLAAscriptionDB.executeQuery(tSQL);
        if (tLAAscriptionDB.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LAGrpAscrptionBL";
            tError.functionName = "dealAscript";
            tError.errorMessage = "查询数据 laascrption 出错！";
            this.mErrors.addOneError(tError);
        }
        //如果 存在有效的数据，则修改为无效
        if (tLAAscriptionSet != null && tLAAscriptionSet.size() >= 1)
        {
            for (int i = 1; i <= tLAAscriptionSet.size(); i++) 
            {
                LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
                tLAAscriptionSchema = tLAAscriptionSet.get(i);
                tLAAscriptionSchema.setModifyDate(currentDate);
                tLAAscriptionSchema.setModifyTime(currentTime);
                tLAAscriptionSchema.setValidFlag("Y");
                mUpLAAsctiptionSet.add(tLAAscriptionSchema);
            }
        }
        //插入新的数据（只有主业务员）
        String mAscripNo = com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo("PerAscripNo", 20);
        
        mOldAgentCode = "";
        String tSql = "select AgentCode from  lcgrpcont where grpcontno='" +
                      cGrpContNo
                      +
                      "'  union select agentcode from lbgrpcont where grpcontno='" +
                      cGrpContNo + "'";
        ExeSQL tExeSQL = new ExeSQL();
        mOldAgentCode = tExeSQL.getOneValue(tSql);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LAGrpAscrptionBL";
            tError.functionName = "dealAscript";
            tError.errorMessage = "查询保单原业务员出错";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mOldAgentCode == null || mOldAgentCode.equals("") ||
            mOldAgentCode.equals("0")) {
            CError tError = new CError();
            tError.moduleName = "LAGrpAscrptionBL";
            tError.functionName = "dealAscript";
            tError.errorMessage = "查询保单信息出错！";
            this.mErrors.addOneError(tError);
            return false;

        }
        tSQL = "select a.agentgroup,a.branchattr from labranchgroup a,laagent b where a.agentgroup=b.agentgroup and b.agentcode='" +
               mOldAgentCode + "'";
        SSRS tSSRS = new SSRS();
        tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LAGrpAscrptionBL";
            tError.functionName = "dealAscript";
            tError.errorMessage = "查询原业务员相应的信息出错";
            this.mErrors.addOneError(tError);
            return false;
        }
        int a = tSSRS.getMaxRow();
        if (a != 1) {
            CError tError = new CError();
            tError.moduleName = "HDLAAscriptionBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查到原业务员相应的信息";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tOldAgentGroup = tSSRS.GetText(1, 1);
        String tOldBranchAttr = tSSRS.GetText(1, 2);
        String strsql =
                "select count(ascripno)+1 from laascription where 1=1 and " +
                "ascripstate='3' and grpcontno='" +
                cGrpContNo + "'";

        tExeSQL = new ExeSQL();
        String tCountNo = tExeSQL.getOneValue(strsql);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "HDLAAscriptionBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询归属表信息出错";
            this.mErrors.addOneError(tError);
            return false;
        }
        int tCount = Integer.parseInt(tCountNo); //得到归属次数

        LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();

        tLAAscriptionSchema.setAscripNo(mAscripNo);
        tLAAscriptionSchema.setGrpContNo(cGrpContNo);
        tLAAscriptionSchema.setValidFlag("N");
        tLAAscriptionSchema.setAgentOld(mOldAgentCode);
        tLAAscriptionSchema.setAgentNew(cMianAgentCode);
        tLAAscriptionSchema.setAClass("01");
        tLAAscriptionSchema.setAgentGroup(tOldAgentGroup);
        tLAAscriptionSchema.setBranchAttr(tOldBranchAttr);
        tLAAscriptionSchema.setAscriptionDate(mAscripDate);
        tLAAscriptionSchema.setOperator(mGlobalInput.Operator);
        tLAAscriptionSchema.setMakeDate(currentDate);
        tLAAscriptionSchema.setMakeTime(currentTime);
        tLAAscriptionSchema.setModifyDate(currentDate);
        tLAAscriptionSchema.setModifyTime(currentTime);
        tLAAscriptionSchema.setAscriptionCount(tCount);
        tLAAscriptionSchema.setAscripState("3");
        tLAAscriptionSchema.setMakeType("01");
        tLAAscriptionSchema.setBranchType("2");
        tLAAscriptionSchema.setBranchType2("01");
        mInLAAsctiptionSet.add(tLAAscriptionSchema);

        return true;
    }
    /**
     * 处理个单归属表
     * 如果该保单有validflag='N'(有效) 的数据,则置为无效Y,contno在laascription表中只有一
     * 条有效的数据
     */
    private boolean dealAscript(String cContNo, String cMianAgentCode) {
        String tSQL = "select * from laascription where contno='" +cContNo +"' and validflag='N' ";
        System.out.println(tSQL);
        LAAscriptionDB tLAAscriptionDB = new LAAscriptionDB();
        LAAscriptionSet tLAAscriptionSet = new LAAscriptionSet();

        tLAAscriptionSet = tLAAscriptionDB.executeQuery(tSQL);
        if (tLAAscriptionDB.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LAGrpAscrptionBL";
            tError.functionName = "dealAscript";
            tError.errorMessage = "查询数据 laascrption 出错！";
            this.mErrors.addOneError(tError);
        }
        //如果 存在有效的数据，则修改为无效
        if (tLAAscriptionSet != null && tLAAscriptionSet.size() >= 1)
        {
            for (int i = 1; i <= tLAAscriptionSet.size(); i++) 
            {
                LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();
                tLAAscriptionSchema = tLAAscriptionSet.get(i);
                tLAAscriptionSchema.setModifyDate(currentDate);
                tLAAscriptionSchema.setModifyTime(currentTime);
                tLAAscriptionSchema.setValidFlag("Y");
                mUpLAAsctiptionSet.add(tLAAscriptionSchema);
            }
        }
        //插入新的数据（只有主业务员）
        String mAscripNo = com.sinosoft.lis.pubfun.PubFun1.CreateMaxNo("PerAscripNo", 20);
        
        mOldAgentCode = "";
        String tSql = "select AgentCode from  lccont where contno='" +cContNo+ "'  union select agentcode from lbcont where contno='" 
                      +cContNo + "'";
        ExeSQL tExeSQL = new ExeSQL();
        mOldAgentCode = tExeSQL.getOneValue(tSql);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LAGrpAscrptionBL";
            tError.functionName = "dealAscript";
            tError.errorMessage = "查询保单原业务员出错";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mOldAgentCode == null || mOldAgentCode.equals("") ||
            mOldAgentCode.equals("0")) {
            CError tError = new CError();
            tError.moduleName = "LAGrpAscrptionBL";
            tError.functionName = "dealAscript";
            tError.errorMessage = "查询保单信息出错！";
            this.mErrors.addOneError(tError);
            return false;

        }
        tSQL = "select a.agentgroup,a.branchattr from labranchgroup a,laagent b where a.agentgroup=b.agentgroup and b.agentcode='" +
               mOldAgentCode + "'";
        SSRS tSSRS = new SSRS();
        tExeSQL = new ExeSQL();
        tSSRS = tExeSQL.execSQL(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "LAGrpAscrptionBL";
            tError.functionName = "dealAscript";
            tError.errorMessage = "查询原业务员相应的信息出错";
            this.mErrors.addOneError(tError);
            return false;
        }
        int a = tSSRS.getMaxRow();
        if (a != 1) {
            CError tError = new CError();
            tError.moduleName = "HDLAAscriptionBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查到原业务员相应的信息";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tOldAgentGroup = tSSRS.GetText(1, 1);
        String tOldBranchAttr = tSSRS.GetText(1, 2);
        String strsql =
                "select count(ascripno)+1 from laascription where 1=1 and " +
                "ascripstate='3' and contno='" +
                cContNo + "'";

        tExeSQL = new ExeSQL();
        String tCountNo = tExeSQL.getOneValue(strsql);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "HDLAAscriptionBL";
            tError.functionName = "dealData";
            tError.errorMessage = "查询归属表信息出错";
            this.mErrors.addOneError(tError);
            return false;
        }
        int tCount = Integer.parseInt(tCountNo); //得到归属次数

        LAAscriptionSchema tLAAscriptionSchema = new LAAscriptionSchema();

        tLAAscriptionSchema.setAscripNo(mAscripNo);
        tLAAscriptionSchema.setContNo(cContNo);
        tLAAscriptionSchema.setValidFlag("N");
        tLAAscriptionSchema.setAgentOld(mOldAgentCode);
        tLAAscriptionSchema.setAgentNew(cMianAgentCode);
        tLAAscriptionSchema.setAClass("01");
        tLAAscriptionSchema.setAgentGroup(tOldAgentGroup);
        tLAAscriptionSchema.setBranchAttr(tOldBranchAttr);
        tLAAscriptionSchema.setAscriptionDate(mAscripDate);
        tLAAscriptionSchema.setOperator(mGlobalInput.Operator);
        tLAAscriptionSchema.setMakeDate(currentDate);
        tLAAscriptionSchema.setMakeTime(currentTime);
        tLAAscriptionSchema.setModifyDate(currentDate);
        tLAAscriptionSchema.setModifyTime(currentTime);
        tLAAscriptionSchema.setAscriptionCount(tCount);
        tLAAscriptionSchema.setAscripState("3");
        tLAAscriptionSchema.setMakeType("01");
        tLAAscriptionSchema.setBranchType("2");
        tLAAscriptionSchema.setBranchType2("01");
        mInLAAsctiptionSet.add(tLAAscriptionSchema);

        return true;
    }
    /**
     * 处理扎帐表
     *  如果是多个代理人,则重新拆分保单
     */
    private boolean dealCommision(LAGrpCommisionDetailSchema cNewLAGrpCommisionDetailSchema) {
        LACommisionSet tLACommisionSet= new LACommisionSet();
        tLACommisionSet=mLACommisionSet;

        if (tLACommisionSet != null && tLACommisionSet.size() >= 1) {
            String tAgentCode = "";
            for (int i = 1; i <= tLACommisionSet.size(); i++) {
                if(tAgentCode.equals(""))
                {//第一条数据不需要交验
                    tAgentCode = tLACommisionSet.get(i).getAgentCode();
                    continue;
                }

                if (!tAgentCode.equals(tLACommisionSet.get(i).getAgentCode())) {
                    CError tError = new CError();
                    tError.moduleName = "LAGrpAscrptionBL";
                    tError.functionName = "dealCommision";
                    tError.errorMessage = "该保单已经做过拆分,请回退提数后再进行保单归属！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                tAgentCode = tLACommisionSet.get(i).getAgentCode();
            }
            StringBuffer tSb = new StringBuffer();
            tSb = new StringBuffer();
            tSb.append(
                    " SELECT a.agentgroup,a.branchcode,b.branchattr,b.branchseries FROM LAAGENT a ,LABRANCHGROUP b WHERE a.AGENTGROUP = b.AGENTGROUP")
                    .append(" and a.agentcode ='")
                    .append(cNewLAGrpCommisionDetailSchema.getAgentCode())
                    .append("'");
            String tSql = tSb.toString();
            System.out.println(tSql);
            ExeSQL exeSQL = new ExeSQL();
            SSRS ssrs = new SSRS();
            ssrs = exeSQL.execSQL(tSql);
            if (exeSQL.mErrors.needDealError()) {
                mErrors.copyAllErrors(exeSQL.mErrors);
                return false;
            }
            nCnt = 0; //标记清 0

            for (int i = 1; i <= tLACommisionSet.size(); i++) {
                nCnt = nCnt + 1;

                LACommisionSchema tLACommisionSchema = new LACommisionSchema();
                tLACommisionSchema = tLACommisionSet.get(i);
                tLACommisionSchema.setAgentCode(cNewLAGrpCommisionDetailSchema.getAgentCode());
                tLACommisionSchema.setAgentGroup(ssrs.GetText(1, 1));
                tLACommisionSchema.setBranchCode(ssrs.GetText(1, 2));
                tLACommisionSchema.setBranchAttr(ssrs.GetText(1, 3));
                tLACommisionSchema.setBranchSeries(ssrs.GetText(1, 4));
                tLACommisionSchema.setModifyDate(this.currentDate);
                tLACommisionSchema.setModifyTime(this.currentTime);
                tLACommisionSchema.setOperator(this.mGlobalInput.Operator);
                this.mupLACommisionSet.add(tLACommisionSchema);
            }
        }
        return true;
    }
    
    /**
     * 团单查询lacommision表数据
     * @param cGrpContNo
     * @return
     */
    private boolean queryGrpCommision(String cGrpContNo) {
        StringBuffer tSb = new StringBuffer();
        tSb = new StringBuffer();
        tSb.append(" SELECT * FROM LACOMMISION WHERE TMAKEDATE>='")
                .append(mAscripDate)
                .append("' AND BRANCHTYPE='2'")
                .append(" AND BRANCHTYPE2='01'")
                .append(" AND GrpContNo='")
                .append(cGrpContNo)
                .append("'");
        ;
        String tSql = tSb.toString();
        System.out.println(tSql);
        LACommisionDB tLACommisionDB = new LACommisionDB();
        mLACommisionSet = tLACommisionDB.executeQuery(tSql);
        for(int i=1;i<=mLACommisionSet.size();i++)
        {
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            tLACommisionSchema=mLACommisionSet.get(i);
            // 备份B表数据
            if (!copyLACommission(tLACommisionSchema)) {
                return false;
            }
        }

        return true;
    }
    /**
     * 个单查询lacommision表数据
     * @param cGrpContNo
     * @return
     */
    private boolean queryCommision(String tContNo) {
        StringBuffer tSb = new StringBuffer();
        tSb = new StringBuffer();
        tSb.append(" SELECT * FROM LACOMMISION WHERE TMAKEDATE>='")
                .append(mAscripDate)
                .append("' AND BRANCHTYPE='2'")
                .append(" AND BRANCHTYPE2='01'")
                .append(" AND ContNo='")
                .append(tContNo)
                .append("'");
        ;
        String tSql = tSb.toString();
        System.out.println(tSql);
        LACommisionDB tLACommisionDB = new LACommisionDB();
        mLACommisionSet = tLACommisionDB.executeQuery(tSql);
        for(int i=1;i<=mLACommisionSet.size();i++)
        {
            LACommisionSchema tLACommisionSchema = new LACommisionSchema();
            tLACommisionSchema=mLACommisionSet.get(i);
            // 备份B表数据
            if (!copyLACommission(tLACommisionSchema)) {
                return false;
            }
        }

        return true;
    }
    /**
     * 备份lacommision表数据
     * @return boolean
     */
    private boolean copyLACommission(LACommisionSchema cLACommisionSchema) {

        String commEdor = "";
        commEdor = PubFun1.CreateMaxNo("COMMISIONEDOR", "SN");
        LACommisionBSchema tLACommisionBSchema = new LACommisionBSchema();
        Reflections tReflections = new Reflections();
        tReflections.transFields(tLACommisionBSchema, cLACommisionSchema);
        tLACommisionBSchema.setEdorNo(commEdor);
        tLACommisionBSchema.setEdorType("12"); //备份保单数据
        mLACommisionBSet.add(tLACommisionBSchema);
        return true;
    }
}
