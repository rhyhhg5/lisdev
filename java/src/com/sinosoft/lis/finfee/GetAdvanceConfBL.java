package com.sinosoft.lis.finfee;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LBTempFeeSchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LBTempFeeSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 预收保费暂交费退费</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class GetAdvanceConfBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    private String mNotBLS = "";

    private GlobalInput tG = new GlobalInput();

    private LJTempFeeSet inLJTempFeeSet = new LJTempFeeSet();

    private LJTempFeeSet outLJTempFeeSet = new LJTempFeeSet();

    private LJTempFeeSet newLJTempFeeSet = new LJTempFeeSet();

    private LJTempFeeSet newInLJTempFeeSet = new LJTempFeeSet();

    private LJTempFeeClassSet outLJTempFeeClassSet = new LJTempFeeClassSet();

    private LJTempFeeClassSet newLJTempFeeClassSet = new LJTempFeeClassSet();
    
    private LBTempFeeSet inLBTempFeeSet = new LBTempFeeSet();

    private String ManCom = "";

    private double sumMoney = 0.0;

    private double tMoney = 0.0;

    private String tLimit = "";

    private String serNo = "";

    public GetAdvanceConfBL()
    {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("---End getInputData---");

        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        System.out.println("---End dealData---");
        if (!checkData())
        {
           return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        System.out.println("---End prepareOutputData---");

        //为外部调用提供接口，外部传入该参数后，程序将不自动调用BLS，外部可通过getResult方法获取准备好的数据VData
        if (!mNotBLS.equals(""))
        {
            mResult = mInputData;
            return true;
        }
        return true;
    }

    /**
     * 核销处理
     * @param tLJTempFeeSchema
     */
    private boolean confTempFee(LJTempFeeSchema tLJTempFeeSchema)
    {
        //核销暂交费分类表
        LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
        tLJTempFeeClassSchema.setTempFeeNo(tLJTempFeeSchema.getTempFeeNo());
        tLJTempFeeClassSchema = tLJTempFeeClassSchema.getDB().query().get(1);
        String numSql = "select count(*)+1 from ljtempfee where otherno='"
                + tLJTempFeeSchema.getOtherNo() + "'";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(numSql);
        String tempNo = tLJTempFeeSchema.getOtherNo() + "80" + tSSRS.GetText(1, 1);
        tLJTempFeeClassSchema.setTempFeeNo(tempNo);
        tLJTempFeeClassSchema.setPayMoney(-sumMoney);
        tLJTempFeeClassSchema.setPayMode("YS");
        System.out.println(tLJTempFeeClassSchema.getTempFeeNo());

        outLJTempFeeClassSet.add(tLJTempFeeClassSchema);

        //      核销暂交费表
        tLJTempFeeSchema.setTempFeeNo(tempNo);
        tLJTempFeeSchema.setPayMoney(-sumMoney);
        outLJTempFeeSet.add(tLJTempFeeSchema);

        return true;
    }

    private boolean setLJTempFee()
    {
        String agentCodet = newLJTempFeeSet.get(1).getAgentCode();
        String agSql = "select agentgroup from laagent where agentcode='"
                + agentCodet + "'";
        System.out.println(agSql);
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(agSql);
        if (tSSRS.MaxRow == 0)
        {
            System.out.println("setLJTempFee--agentgroup不存在");
            CError tError = new CError();
            tError.moduleName = "PayAdvanceBL";
            tError.functionName = "setLJTempFee";
            tError.errorMessage = "未找到业务员代码!";
            this.mErrors.addOneError(tError);
            return false;
        }

        String tempFeeNo = newLJTempFeeSet.get(1).getTempFeeNo();
        for (int j = 1; j <= newLJTempFeeSet.size(); j++)
        {
            System.out.println(j + "" + newLJTempFeeSet.size() + ""
                    + (j <= newLJTempFeeSet.size()));
            LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
            tLJTempFeeSchema = newLJTempFeeSet.get(j);
            tLJTempFeeSchema.setAgentGroup(tSSRS.GetText(1, 1));
            tLJTempFeeSchema.setPayDate(PubFun.getCurrentDate());
            tLJTempFeeSchema.setOtherNoType("5");
            tLJTempFeeSchema.setConfFlag("0");
            tLJTempFeeSchema.setEnterAccDate(PubFun.getCurrentDate());
            tLJTempFeeSchema.setManageCom(ManCom);
            tLJTempFeeSchema.setPolicyCom(ManCom);
            tLJTempFeeSchema.setOperator(tG.Operator);
            tLJTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
            tLJTempFeeSchema.setConfMakeTime(PubFun.getCurrentTime());
            tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
            tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
            tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
            tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
            tLJTempFeeSchema.setSerialNo(serNo);
            sumMoney = sumMoney + tLJTempFeeSchema.getPayMoney();
            newInLJTempFeeSet.add(tLJTempFeeSchema);
        }
        System.out.println("转保费金额：" + sumMoney);
        if (sumMoney == 0.0)
        {
            //          @@错误处理
            CError tError = new CError();
            tError.moduleName = "GetAdvanceConfBL";
            tError.functionName = "setNewTempfee";
            tError.errorMessage = "输入总金额为零！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (sumMoney > tMoney)
        {
            System.out.println("转保费金额：" + sumMoney + "--预收保费剩余金额" + tMoney);
            //          @@错误处理
            CError tError = new CError();
            tError.moduleName = "GetAdvanceConfBL";
            tError.functionName = "setNewTempfee";
            tError.errorMessage = "转保费总金额大于预收保费剩余金额！";
            this.mErrors.addOneError(tError);
            return false;
        }
        LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
        tLJTempFeeClassSchema.setTempFeeNo(tempFeeNo);
        tLJTempFeeClassSchema.setPayMode("YS");
        tLJTempFeeClassSchema.setPayMoney(sumMoney);
        tLJTempFeeClassSchema.setPayDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setEnterAccDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setManageCom(ManCom);
        tLJTempFeeClassSchema.setPolicyCom(ManCom);
        tLJTempFeeClassSchema.setConfFlag("0");
        tLJTempFeeClassSchema.setOperator(tG.Operator);
        tLJTempFeeClassSchema.setConfMakeDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setConfMakeTime(PubFun.getCurrentTime());
        tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
        tLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
        tLJTempFeeClassSchema.setSerialNo(serNo);
        newLJTempFeeClassSet.add(tLJTempFeeClassSchema);
        return true;
    }
    
    private boolean setLBTempFee()
    {
        LBTempFeeSchema tLBTempFeeSchema = new LBTempFeeSchema();
        tLBTempFeeSchema.setTempFeeNo(newInLJTempFeeSet.get(1).getTempFeeNo());
        tLBTempFeeSchema.setTempFeeType("1");
        tLBTempFeeSchema.setRiskCode("000000");
        tLBTempFeeSchema.setOtherNo(outLJTempFeeSet.get(1).getOtherNo());
        tLBTempFeeSchema.setOtherNoType("YS");
        tLBTempFeeSchema.setPayMoney(sumMoney);
        tLBTempFeeSchema.setPayDate(PubFun.getCurrentDate());
        tLBTempFeeSchema.setEnterAccDate(PubFun.getCurrentDate());
        tLBTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
        tLBTempFeeSchema.setConfDate(PubFun.getCurrentDate());
        tLBTempFeeSchema.setManageCom(ManCom);
        //2012-4-12 lbtempfee表APPntName字段太短 导致插入失败 故去掉
        //tLBTempFeeSchema.setAPPntName(outLJTempFeeSet.get(1).getAPPntName());
        tLBTempFeeSchema.setAgentCode(outLJTempFeeSet.get(1).getAgentCode());
        tLBTempFeeSchema.setConfFlag("1");
        tLBTempFeeSchema.setSerialNo(serNo);
        tLBTempFeeSchema.setOperator(tG.Operator);
        tLBTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
        tLBTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
        tLBTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
        tLBTempFeeSchema.setModifyTime(PubFun.getCurrentTime());    
        tLBTempFeeSchema.setBackUpSerialNo(PubFun1.CreateMaxNo("LBTempFee", 20));
        
        inLBTempFeeSet.add(tLBTempFeeSchema);
        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        try
        {
            System.out.println("--setLJTempFee-start--");
            if (!setLJTempFee())
            {
                return false;
            }

            for (int i = 1; i <= inLJTempFeeSet.size(); i++)
            { //SET是从1开始记数
                //取出一行，并查询出该行的全部信息
                LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
                tLJTempFeeSchema.setOtherNo(inLJTempFeeSet.get(i).getOtherNo());
                tLJTempFeeSchema = (tLJTempFeeSchema.getDB().query()).get(1);

                //反冲处理
                if (!confTempFee(tLJTempFeeSchema))
                {
                    //                  @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "GetAdvanceConfBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "核销预收保费错误！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (!setLBTempFee())
                {
                    //                  @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "GetAdvanceConfBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "生成预收保费关联数据出错！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
        }
        catch (Exception e)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GetAdvanceConfBL";
            tError.functionName = "dealData";
            tError.errorMessage = "数据处理错误!" + e.getMessage();
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData)
    {
        inLJTempFeeSet.set((LJTempFeeSet) mInputData.getObjectByObjectName(
                "LJTempFeeSet", 0));
        tG = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
        newLJTempFeeSet.set((LJTempFeeSet) mInputData.getObjectByObjectName(
                "LJTempFeeSet", 1));

        if (inLJTempFeeSet == null || tG == null || newLJTempFeeSet == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GetAdvanceConfBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        this.tMoney = inLJTempFeeSet.get(1).getPayMoney();

        this.ManCom = tG.ManageCom;
        for (int i = ManCom.length(); i < 8; i++)
        {
            ManCom = ManCom + "0";
        }
        System.out.println("收费机构：" + ManCom);
        tLimit = PubFun.getNoLimit(ManCom);
        serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        return true;
    }

    private boolean checkData()
    {
        String appnt = outLJTempFeeSet.get(1).getAPPntName();
        String prtno = newLJTempFeeSet.get(1).getOtherNo();

        ExeSQL tExeSQL = new ExeSQL();
        String sql = "select grpname from lcgrpcont where prtno='" + prtno
                + "' ";
        SSRS tSSRS = tExeSQL.execSQL(sql);
        System.out.println(sql);
        String newAppnt = tSSRS.GetText(1, 1);
        System.out.println(appnt + "==" + newAppnt);
        if (!appnt.equals(newAppnt))
        {
            System.out.println("交费单位名称不一致，不能进行预收保费转实收保费！");
            CError tError = new CError();
            tError.moduleName = "GetAdvanceConfBL";
            tError.functionName = "dealData";
            tError.errorMessage = "交费单位名称不一致，不能进行预收保费转实收保费！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData()
    {
        mInputData = new VData();
        MMap outMap = new MMap();
        try
        {
            System.out.println("----before submit----");
            outMap.put(outLJTempFeeSet, "INSERT");
            outMap.put(outLJTempFeeClassSet, "INSERT");
            outMap.put(newInLJTempFeeSet, "INSERT");
            outMap.put(newLJTempFeeClassSet, "INSERT");
            outMap.put(inLBTempFeeSet, "INSERT");
            mInputData.add(outMap);
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, ""))
            {
                CError.buildErr(this, "预收保费反冲数据插入失败", tPubSubmit.mErrors);
                return false;
            }
            System.out.println("----after submit----");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GetAdvanceConfBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
    }
}
