package com.sinosoft.lis.finfee;

import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;
import java.sql.*;
import java.lang.String;
import com.sinosoft.lis.f1print.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class PolFeeWithdrawBalanceBL  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 数据操作字符串 */
  private GlobalInput mGI=new GlobalInput();
  private String mOperate;
  private String tLimit="";
  private String serNo="";//流水号
  private String getNo="";//给付收据号码
  private String prtSeqNo="";//印刷流水号

  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private LJAGetSchema         mLJAGetSchema       = new LJAGetSchema();//实付总表
  private LJAGetSet            mLJAGetSet          = new LJAGetSet();
  private LOPRTManagerSchema   mLOPRTManagerSchema = new LOPRTManagerSchema();//打印管理表
  private LOPRTManagerSet      mLOPRTManagerSet    = new LOPRTManagerSet();
  private LCPolSchema          mLCPolSchema        = new LCPolSchema();//个人保单表
  private LCPolSet             mLCPolSet           = new LCPolSet();
  private LCPolSchema          mNewLCPolSchema     = new LCPolSchema();//个人保单表
  private LCPolSet             mNewLCPolSet        = new LCPolSet();

  public PolFeeWithdrawBalanceBL() {
  }
  public static void main(String[] args) {
    PolFeeWithdrawBalanceBL PolFeeWithdrawBalanceBL1 = new PolFeeWithdrawBalanceBL();
    VData tVData = new VData();
    GlobalInput tGI = new GlobalInput();
    tGI.ManageCom="8600";
    tGI.Operator="hzm";
    LCPolSchema tLCPolSchema = new LCPolSchema();
    tLCPolSchema.setPolNo("86110020030210000084");
    tVData.add(tLCPolSchema);
    tVData.add(tGI);
    PolFeeWithdrawBalanceBL1.submitData(tVData);
  }

  //传输数据的公共方法
  public boolean submitData(VData tVData)
  {
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(tVData))
      return false;
System.out.println("After getinputdata");

    //进行业务处理
    if (!dealData())
      return false;
System.out.println("After dealData！");
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
System.out.println("After prepareOutputData");

    System.out.println("Start PolFeeWithdrawBalanceBL BL Submit...");

    PolFeeWithdrawBalanceBLS tPolFeeWithdrawBalanceBLS=new PolFeeWithdrawBalanceBLS();
    tPolFeeWithdrawBalanceBLS.submitData(mInputData);

    System.out.println("End PolFeeWithdrawBalanceBL BL Submit...");

    //如果有需要处理的错误，则返回
    if (tPolFeeWithdrawBalanceBLS.mErrors .needDealError())
    {
        this.mErrors .copyAllErrors(tPolFeeWithdrawBalanceBLS.mErrors ) ;
    }

    mInputData=null;
    return true;
  }

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData()
  {
    //产生流水号
    tLimit=PubFun.getNoLimit(mGI.ManageCom);
    serNo=PubFun1.CreateMaxNo("SERIALNO",tLimit);
    LCPolSchema tLCPolSchema = new LCPolSchema();
    LCPolSet tLCPolSet = new LCPolSet();
        tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setPolNo(mLCPolSchema.getPolNo());
        //1-查询投保单号对应的已签单的保单，如果没有则跳过,否则保存查询到的纪录
        LCPolDB tLCPolDB=new LCPolDB();
        tLCPolDB.setSchema(tLCPolSchema);
        tLCPolSet=tLCPolDB.query();
        if(tLCPolSet==null)
        {
          // @@错误处理
         this.mErrors.copyAllErrors(tLCPolDB.mErrors);
         CError tError =new CError();
         tError.moduleName="PolFeeWithdrawBalanceBL";
         tError.functionName="dealData";
         tError.errorMessage="查询个人保单表失败!";
         this.mErrors .addOneError(tError) ;
         return false;
        }
        if(tLCPolSet.size()==0)
        {
          CError tError =new CError();
          tError.moduleName="PolFeeWithdrawBalanceBL";
          tError.functionName="dealData";
          tError.errorMessage="没有查询到符合条件的个人保单表!";
          this.mErrors .addOneError(tError) ;
          return false;
        }
        mNewLCPolSchema = tLCPolSet.get(1);
        if(!mNewLCPolSchema.getAppFlag().equals("1")) //如果没有签单返回
        {
          CError tError =new CError();
          tError.moduleName="PolFeeWithdrawBalanceBL";
          tError.functionName="dealData";
          tError.errorMessage="该保单尚未签单!";
          this.mErrors .addOneError(tError) ;
          return false;
        }
        if(!(mNewLCPolSchema.getLeavingMoney()>0))//如果没有余额,返回
        {
          CError tError =new CError();
          tError.moduleName="PolFeeWithdrawBalanceBL";
          tError.functionName="dealData";
          tError.errorMessage="该保单没有余额!";
          this.mErrors .addOneError(tError) ;
          return false;
        }
        //查询应收个人表-如果已经发催收，那么不容许退余额
        LJSPaySet tLJSPaySet = new LJSPaySet();
        LJSPaySchema tLJSPaySchema = new LJSPaySchema();
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPaySchema.setOtherNo(mNewLCPolSchema.getPolNo());//个人保单号
        tLJSPaySchema.setOtherNoType("2");//号码类型：个人保单
        tLJSPayDB.setSchema(tLJSPaySchema);
        tLJSPaySet=tLJSPayDB.query();
        if(tLJSPaySet==null)
        {
          // @@错误处理
         this.mErrors.copyAllErrors(tLJSPayDB.mErrors);
         CError tError =new CError();
         tError.moduleName="PolFeeWithdrawBalanceBL";
         tError.functionName="dealData";
         tError.errorMessage="查询应收个人总表失败!";
         this.mErrors .addOneError(tError) ;
         return false;
        }
        if(tLJSPaySet.size()>0)
        {
          // @@错误处理
         CError tError =new CError();
         tError.moduleName="PolFeeWithdrawBalanceBL";
         tError.functionName="dealData";
         tError.errorMessage="应收个人总表已经有该保单的催收纪录!";
         this.mErrors .addOneError(tError) ;
         return false;
        }
        //2-准备实付表纪录,产生实付号码
        tLimit=PubFun.getNoLimit(mGI.ManageCom);
        getNo=PubFun1.CreateMaxNo("GETNO",tLimit);
        mLJAGetSchema = new LJAGetSchema();
        mLJAGetSchema.setActuGetNo(getNo);
        mLJAGetSchema.setOtherNo(mNewLCPolSchema.getPolNo());
        mLJAGetSchema.setOtherNoType("6");//其它类型退费
        mLJAGetSchema.setAppntNo(mNewLCPolSchema.getAppntNo());
        mLJAGetSchema.setSumGetMoney(mNewLCPolSchema.getLeavingMoney());
        mLJAGetSchema.setSaleChnl(mNewLCPolSchema.getSaleChnl());
        mLJAGetSchema.setSerialNo(serNo);
        mLJAGetSchema.setOperator(mGI.Operator);
        mLJAGetSchema.setManageCom(mGI.ManageCom);
        mLJAGetSchema.setAgentCode(mNewLCPolSchema.getAgentCode());
        mLJAGetSchema.setAgentCom(mNewLCPolSchema.getAgentCom());
        mLJAGetSchema.setAgentGroup(mNewLCPolSchema.getAgentGroup());
        mLJAGetSchema.setMakeDate(CurrentDate);
        mLJAGetSchema.setMakeTime(CurrentTime);
        mLJAGetSchema.setModifyDate(CurrentDate);
        mLJAGetSchema.setModifyTime(CurrentTime);
        //mLJAGetSet.add(mLJAGetSchema);
        //3-准备打印数据,生成印刷流水号
        tLimit=PubFun.getNoLimit(mGI.ManageCom);
        prtSeqNo=PubFun1.CreateMaxNo("PRTSEQNO",tLimit);
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_INDPOL);
        mLOPRTManagerSchema.setOtherNo(mNewLCPolSchema.getPolNo());
        mLOPRTManagerSchema.setMakeDate(CurrentDate);
        mLOPRTManagerSchema.setMakeTime(CurrentTime);
        mLOPRTManagerSchema.setManageCom(mNewLCPolSchema.getManageCom());
        mLOPRTManagerSchema.setCode(PrintManagerBL.CODE_BALANCE);
        mLOPRTManagerSchema.setReqCom(mGI.ManageCom);
        mLOPRTManagerSchema.setReqOperator(mGI.Operator);
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        PrintManagerBL tPrintManagerBL=new PrintManagerBL();
        VData tempVData = new VData();
        tempVData.add(mLOPRTManagerSchema);
        tPrintManagerBL.submitData(tempVData,"REQ");//打印数据处理
        tempVData=tPrintManagerBL.getResult();
        mLOPRTManagerSchema=(LOPRTManagerSchema)tempVData.getObjectByObjectName("LOPRTManagerSchema",0);

        //mLOPRTManagerSet.add(mLOPRTManagerSchema);


        //4-更新个人保单数据
        mNewLCPolSchema.setLeavingMoney(0);
        mNewLCPolSchema.setModifyDate(CurrentDate);
        mNewLCPolSchema.setModifyTime(CurrentTime);
        //mNewLCPolSet.add(mNewLCPolSchema);
    return true ;
  }

 /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData tVData)
  {
    mLCPolSchema=(LCPolSchema)tVData.getObjectByObjectName("LCPolSchema",0);
    mGI=(GlobalInput)tVData.getObjectByObjectName("GlobalInput",0);
    if(mLCPolSchema==null||mGI==null)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="PolFeeWithdrawBalanceBL";
      tError.functionName="getInputData";
      tError.errorMessage="没有得到足够的数据，请您确认!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    if(mLCPolSchema.getPolNo()==null)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="PolFeeWithdrawBalanceBL";
      tError.functionName="getInputData";
      tError.errorMessage="没有得到保单号，请您确认!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData()
  {
    mInputData=new VData();
    try
    {
      mInputData.add(mNewLCPolSchema);
      mInputData.add(mLJAGetSchema);
      mInputData.add(mLOPRTManagerSchema);
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="PolFeeWithdrawBalanceBL";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }
}

