package com.sinosoft.lis.finfee;

import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.operfee.IndiLJSCancelBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 银保通续期反冲接口
 * 
 * @author zhangchengxuan
 */
public class YBTTempRecoil {

	/** 保单号 */
	private String contNo;

	/** 收费金额 */
	private String payMoney;

	/** 错误处理类 */
	private CErrors mErrors = new CErrors();

	/** 提交数据库VData集合 */
	private VData mInputData = new VData();

	/** 提交数据库MMap集合 */
	private MMap map = new MMap();

	/** GlobalInput信息对象 */
	private GlobalInput mGlobalInput;

	/** 暂收费号 */
	private String tempFeeNo;

	/**
	 * 程序调用入口
	 * 
	 * @param cInputData
	 * @return 反冲成功标记 true-续期反冲成功 false-续期反冲失败
	 */
	public boolean submitData(TransferData tTransferData) {

		System.out.println("---YBTTempRecoil--start---");

		if (!getInputData(tTransferData)) {
			return false;
		}

		System.out.println("---End getInputData---");

		if (!dealData()) {
			return false;
		}

		if (!prepareOutputData()) {
			return false;
		}
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, "")) {
			// 错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}

		if (!afterSubmit()) {
			return false;
		}

		System.out.println("---End dealData---");
		return true;
	}

	/**
	 * 获取查询失败原因
	 * 
	 * @return mErrors 错误信息集合
	 */
	public CErrors getErrorInf() {

		return this.mErrors;
	}

	/**
	 * 获取参数
	 * 
	 * @param tTransferData
	 *            payMoney-收费金额 contNo-保单号
	 * @return 传入信息成功标记
	 */
	private boolean getInputData(TransferData tTransferData) {

		try {
			this.contNo = (String) tTransferData.getValueByName("ContNo");
			this.payMoney = (String) tTransferData.getValueByName("PayMoney");
			this.mGlobalInput = (GlobalInput) tTransferData
					.getValueByName("GlobalInput");

			if (contNo == null || contNo.equals("")) {
				mErrors.addOneError("保单号为空");
				System.out.println("---传入的保单号为空---");
				return false;
			}

			if (payMoney == null || payMoney.equals("")) {
				mErrors.addOneError("金额为空");
				System.out.println("---传入的金额为空---");
				return false;
			}

			if (mGlobalInput == null) {
				mErrors.addOneError("传入的机构信息为空");
				System.out.println("---传入的机构信息为空---");
				return false;
			}
		} catch (Exception e) {
			mErrors.addOneError("获取传入的保单信息失败");
			System.out.println("---获取保单信息失败---");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * 程序处理入口
	 * 
	 * @return 反冲处理成功标记
	 */
	private boolean dealData() {

		String checkSql = "select sum(paymoney) from ljtempfee where otherno='"
				+ contNo + "' and tempfeetype='18' and confdate is null";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(checkSql);
		if (tSSRS.MaxRow == 0 || tSSRS.GetText(1, 1) == null
				|| tSSRS.GetText(1, 1).equals("null")
				|| tSSRS.GetText(1, 1).equals("")) {
			mErrors.addOneError("未查到保单收费相关信息");
			System.out.println("---未查到保单收费相关信息---");
			return false;
		} else if (Double.parseDouble(tSSRS.GetText(1, 1)) == 0) {
			mErrors.addOneError("该笔收费已进行反冲");
			System.out.println("---该笔收费已进行反冲---");
			return false;
		} else if (Double.parseDouble(tSSRS.GetText(1, 1)) != Double
				.parseDouble(payMoney)) {
			mErrors.addOneError("金额与收费金额不符");
			System.out.println("---金额与收费金额不符---");
			return false;
		}

		String querySql = "select * from ljtempfee where otherno='" + contNo
				+ "' and tempfeetype='18' and confdate is null";
		System.out.println(querySql);

		LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
		LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.executeQuery(querySql);

		if (tLJTempFeeSet.size() == 0) {
			mErrors.addOneError("未查到保单收费相关信息");
			System.out.println("---未查到保单收费相关信息---");
			return false;
		}

		tempFeeNo = tLJTempFeeSet.get(1).getTempFeeNo();

		LJTempFeeSet upLJTempFeeSet = new LJTempFeeSet();

		for (int row = 1; row <= tLJTempFeeSet.size(); row++) {
			LJTempFeeSchema tLJTempFeeSchema = tLJTempFeeSet.get(row);
			tLJTempFeeSchema
					.setTempFeeNo("F" + tLJTempFeeSchema.getTempFeeNo());
			tLJTempFeeSchema.setPayMoney(-tLJTempFeeSchema.getPayMoney());
			tLJTempFeeSchema.setConfMakeTime(PubFun.getCurrentTime());
			tLJTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
			tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
			tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
			tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
			upLJTempFeeSet.add(tLJTempFeeSchema);
		}

		querySql = "select * from ljtempfeeclass where tempfeeno='" + tempFeeNo
				+ "' and paymoney=" + payMoney;
		System.out.println(querySql);

		LJTempFeeClassDB tLJTempFeClasseDB = new LJTempFeeClassDB();
		LJTempFeeClassSet tLJTempFeeClassSet = tLJTempFeClasseDB
				.executeQuery(querySql);

		if (tLJTempFeeClassSet.size() == 0) {
			mErrors.addOneError("未查到保单收费相关信息");
			System.out.println("---未查到保单收费相关信息---");
			return false;
		}

		LJTempFeeClassSet upLJTempFeeClassSet = new LJTempFeeClassSet();

		for (int row = 1; row <= tLJTempFeeClassSet.size(); row++) {
			LJTempFeeClassSchema tLJTempFeeClassSchema = tLJTempFeeClassSet
					.get(row);
			tLJTempFeeClassSchema.setTempFeeNo("F"
					+ tLJTempFeeClassSchema.getTempFeeNo());
			tLJTempFeeClassSchema.setPayMoney(-tLJTempFeeClassSchema
					.getPayMoney());
			tLJTempFeeClassSchema.setConfMakeDate(PubFun.getCurrentDate());
			tLJTempFeeClassSchema.setConfMakeTime(PubFun.getCurrentTime());
			tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
			tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
			tLJTempFeeClassSchema.setOperator(mGlobalInput.Operator);
			upLJTempFeeClassSet.add(tLJTempFeeClassSchema);
		}

		this.map.put(upLJTempFeeSet, "INSERT");
		this.map.put(upLJTempFeeClassSet, "INSERT");

		LJSPayDB tLJSPayDB = new LJSPayDB();
		tLJSPayDB.setGetNoticeNo(tempFeeNo);
		if(!tLJSPayDB.getInfo()){
			mErrors.addOneError("保单应收信息获取失败");
			System.out.println("---保单应收信息获取失败---");
			return false;
		}
		LJSPaySchema tLJSPaySchema = tLJSPayDB.getSchema();
		tLJSPaySchema.setBankOnTheWayFlag("0");
		tLJSPaySchema.setModifyDate(PubFun.getCurrentDate());
		tLJSPaySchema.setModifyTime(PubFun.getCurrentTime());
		tLJSPaySchema.setOperator(mGlobalInput.Operator);
		this.map.put(tLJSPaySchema, "UPDATE");

		return true;
	}

	/**
	 * 设置更新数据
	 * 
	 * @return 成功标记
	 */
	private boolean prepareOutputData() {

		try {
			mInputData.clear();
			mInputData.add(map);
		} catch (Exception ex) {
			ex.printStackTrace();
			mErrors.addOneError("在准备往后层处理所需要的数据时出错");
			return false;
		}
		return true;
	}

	/**
	 * 调用续期作废接口
	 * 
	 * @return 暂全部为成功
	 */
	private boolean afterSubmit() {
		System.out.println("--调用续期作废接口--");

		LJSPaySchema tLJSPaySchema = new LJSPaySchema();
		tLJSPaySchema.setGetNoticeNo(tempFeeNo);

		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("CancelMode", "2");

		VData tVData = new VData();
		tVData.addElement(tLJSPaySchema);
		tVData.addElement(mGlobalInput);
		tVData.addElement(tTransferData);

		IndiLJSCancelBL bl = new IndiLJSCancelBL();
		if (!bl.submitData(tVData, "INSERT")) {
			System.out.println("--作废失败--");
			System.out.println(bl.mCErrors.getErrContent());
			return true;
		}
		System.out.println("--作废成功--");
		return true;
	}

	/**
	 * 调用方法举例
	 */
	public static void main(String[] arr) {

		CErrors cErrors = new CErrors();

		GlobalInput tGI = new GlobalInput();
		tGI.ManageCom = "86";
		tGI.Operator = "YBT";

		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("ContNo", "00000619801");
		tTransferData.setNameAndValue("PayMoney", "443.00");
		tTransferData.setNameAndValue("GlobalInput", tGI);

		YBTTempRecoil tYBTTempRecoil = new YBTTempRecoil();

		if (!tYBTTempRecoil.submitData(tTransferData)) {
			// 反冲失败，可获得返回信息
			cErrors = tYBTTempRecoil.getErrorInf();
			System.out.println(cErrors.getLastError());
		}
	}
}
