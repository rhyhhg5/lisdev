package com.sinosoft.lis.finfee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

import com.sinosoft.lis.bl.LJTempFeeBL;
import com.sinosoft.lis.bl.LJTempFeeClassBL;
import com.sinosoft.lis.certify.PubCertifyTakeBack;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpPolDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCPremDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LJABonusGetDB;
import com.sinosoft.lis.db.LJAGetClaimDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LJAGetDrawDB;
import com.sinosoft.lis.db.LJAGetEndorseDB;
import com.sinosoft.lis.db.LJAGetOtherDB;
import com.sinosoft.lis.db.LJAGetTempFeeDB;
import com.sinosoft.lis.db.LJFIGetDB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.db.LLCasePayAdvancedTraceDB;
import com.sinosoft.lis.db.LMCertifyDesDB;
import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.db.LZCardNumberDB;
import com.sinosoft.lis.db.LZCardPayDB;
import com.sinosoft.lis.encrypt.LisIDEA;
import com.sinosoft.lis.operfee.ChangeFinFeeStateBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCGrpPolSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCPremSchema;
import com.sinosoft.lis.schema.LJABonusGetSchema;
import com.sinosoft.lis.schema.LJAGetClaimSchema;
import com.sinosoft.lis.schema.LJAGetDrawSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.schema.LJAGetOtherSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJAGetTempFeeSchema;
import com.sinosoft.lis.schema.LJAPayGrpSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.schema.LJFIGetSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.schema.LLCasePayAdvancedTraceSchema;
import com.sinosoft.lis.schema.LZCardSchema;
import com.sinosoft.lis.schema.LZCardTrackSchema;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.lis.vschema.LCGrpPolSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.vschema.LJABonusGetSet;
import com.sinosoft.lis.vschema.LJAGetClaimSet;
import com.sinosoft.lis.vschema.LJAGetDrawSet;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.vschema.LJAGetOtherSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJAGetTempFeeSet;
import com.sinosoft.lis.vschema.LJAPayGrpSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LJAPaySet;
import com.sinosoft.lis.vschema.LJFIGetSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LMCertifyDesSet;
import com.sinosoft.lis.vschema.LZCardNumberSet;
import com.sinosoft.lis.vschema.LZCardPaySet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.lis.vschema.LZCardTrackSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CodeJudge;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class TempFeeBL {
  //错误处理类，每个需要错误处理的类中都放置该类
  public CErrors mErrors = new CErrors(
      );

  /** 往后面传输数据的容器 */
  private String[] existTempFeeNo;
  private VData mInputData;
  private VData mLastFinData = new VData();
  private GlobalInput tGI = new GlobalInput();
  private VData FinFeeVData = new VData(); //存放财务付费数据

  /** 数据操作字符串 */
  private String serNo = ""; //流水号
  private String tLimit = "";
  private String tNo = ""; //生成的暂交费号
  private String mOperate;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private String mEngagePrtNo = ""; // 记录预打报单交费收据号

  private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
  private LJTempFeeSet mLJTempFeeSetNew = new LJTempFeeSet();
  private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
  private LJTempFeeClassSet mLJTempFeeClassSetNew = new LJTempFeeClassSet();
  private LJTempFeeClassSet mLJTempFeeClassSetDel = new LJTempFeeClassSet();

  //存在预打保单时需要处理的数据信息
  //实收集体交费信息
  private LJAPayGrpSet mLJAPayGrpSet = new LJAPayGrpSet();
  //实收个人交费信息
  private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();
  //总实收信息
  private LJAPaySet mLJAPaySet = new LJAPaySet();

  private SchemaSet mSchemaSet = new SchemaSet();
  private LJFIGetSet mLJFIGetSet = new LJFIGetSet();
  private LJAGetSet mLJAGetSet = new LJAGetSet();

  //业务处理相关变量
  public TempFeeBL() {
  }

  public static void main(String[] args) {
    LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
    LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
    LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
    LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
    GlobalInput tGI = new GlobalInput();
    tGI.ComCode = "86110000";
    tGI.Operator = "001";
    tGI.ManageCom = "86110000";
    LisIDEA tLisIDEA = new LisIDEA();
    String strPass = tLisIDEA.decryptString("BECC67AEC310A897");
    System.out.println(strPass);
//1st record:
//    tLJTempFeeSchema = new LJTempFeeSchema();
//    tLJTempFeeSchema.setTempFeeNo("81000000913");
//    tLJTempFeeSchema.setTempFeeType("1");
//    tLJTempFeeSchema.setPayDate("2006-03-20");
//    tLJTempFeeSchema.setPayMoney(11168.00);
//    tLJTempFeeSchema.setEnterAccDate("2006-3-21");
//    tLJTempFeeSchema.setRiskCode("1501");
//    tLJTempFeeSchema.setAgentCode("1102000001");
//    tLJTempFeeSchema.setAgentGroup("000000000016");
//    tLJTempFeeSchema.setManageCom("86110000");
//    tLJTempFeeSchema.setOperator("001");
//    tLJTempFeeSchema.setOtherNo("18000929005");
//    tLJTempFeeSchema.setOtherNoType("4");
//    tLJTempFeeSet.add(tLJTempFeeSchema);

//    tLJTempFeeSchema = new LJTempFeeSchema();
//    tLJTempFeeSchema.setTempFeeNo("81000000913");
//    tLJTempFeeSchema.setTempFeeType("a");
//    tLJTempFeeSchema.setPayDate("2005-09-29");
//    tLJTempFeeSchema.setPayMoney(10190.00);
//    tLJTempFeeSchema.setEnterAccDate("2005-5-15");
//    tLJTempFeeSchema.setRiskCode("1502");
//    tLJTempFeeSchema.setAgentCode("1102000001");
//    tLJTempFeeSchema.setAgentGroup("000000000016");
//    tLJTempFeeSchema.setManageCom("86110000");
//    tLJTempFeeSchema.setOperator("001");
//    tLJTempFeeSchema.setOtherNo("18000909050");
//    tLJTempFeeSchema.setOtherNoType("4");
//    tLJTempFeeSet.add(tLJTempFeeSchema);

//    tLJTempFeeSchema = new LJTempFeeSchema();
//    tLJTempFeeSchema.setTempFeeNo("81000000913");
//    tLJTempFeeSchema.setTempFeeType("1");
//    tLJTempFeeSchema.setPayDate("2006-03-20");
//    tLJTempFeeSchema.setPayMoney(11520.00);
//    tLJTempFeeSchema.setEnterAccDate("2006-03-21");
//    tLJTempFeeSchema.setRiskCode("5501");
//    tLJTempFeeSchema.setAgentCode("1102000001");
//    tLJTempFeeSchema.setAgentGroup("000000000016");
//    tLJTempFeeSchema.setManageCom("86110000");
//    tLJTempFeeSchema.setOperator("001");
//    tLJTempFeeSchema.setOtherNo("18000929005");
//    tLJTempFeeSchema.setOtherNoType("4");
//    tLJTempFeeSet.add(tLJTempFeeSchema);

    /*
       tLJTempFeeClassSchema    = new LJTempFeeClassSchema();
       tLJTempFeeClassSchema.setTempFeeNo("12222222222222222223");
       tLJTempFeeClassSchema.setPayMode("1");
       tLJTempFeeClassSchema.setPayDate("2003-3-5");
       tLJTempFeeClassSchema.setPayMoney(10);
       tLJTempFeeClassSchema.setEnterAccDate("2003-3-5");
       tLJTempFeeClassSchema.setManageCom("86110000");
     tLJTempFeeClassSet.add(tLJTempFeeClassSchema);
//2nd record:
       tLJTempFeeSchema     = new LJTempFeeSchema();
       tLJTempFeeSchema.setTempFeeNo("12222222222222222224");
       tLJTempFeeSchema.setTempFeeType("1");
       tLJTempFeeSchema.setPayDate("2003-3-5");
       tLJTempFeeSchema.setPayMoney(10);
       tLJTempFeeSchema.setEnterAccDate("2003-3-5");
       tLJTempFeeSchema.setRiskCode("111301");
       tLJTempFeeSchema.setAgentCode("0000000001");
       tLJTempFeeSchema.setAgentGroup("010101");
       tLJTempFeeSchema.setManageCom("86110000");
       tLJTempFeeSchema.setOperator("001");
     tLJTempFeeSet.add(tLJTempFeeSchema);

       tLJTempFeeClassSchema    = new LJTempFeeClassSchema();
       tLJTempFeeClassSchema.setTempFeeNo("12222222222222222224");
       tLJTempFeeClassSchema.setPayMode("1");
       tLJTempFeeClassSchema.setPayDate("2003-3-5");
       tLJTempFeeClassSchema.setPayMoney(10);
       tLJTempFeeClassSchema.setEnterAccDate("2003-3-5");
       tLJTempFeeClassSchema.setManageCom("86110000");
     tLJTempFeeClassSet.add(tLJTempFeeClassSchema);
//3rd record:
       tLJTempFeeSchema     = new LJTempFeeSchema();
       tLJTempFeeSchema.setTempFeeNo("12222222222222222225");
       tLJTempFeeSchema.setTempFeeType("1");
       tLJTempFeeSchema.setPayDate("2003-3-5");
       tLJTempFeeSchema.setPayMoney(10);
       tLJTempFeeSchema.setEnterAccDate("2003-3-5");
       tLJTempFeeSchema.setRiskCode("111301");
       tLJTempFeeSchema.setAgentCode("0000000001");
       tLJTempFeeSchema.setAgentGroup("010101");
       tLJTempFeeSchema.setManageCom("86110000");
       tLJTempFeeSchema.setOperator("001");
     tLJTempFeeSet.add(tLJTempFeeSchema);

       tLJTempFeeSchema     = new LJTempFeeSchema();
       tLJTempFeeSchema.setTempFeeNo("12222222222222222225");
       tLJTempFeeSchema.setTempFeeType("1");
       tLJTempFeeSchema.setPayDate("2003-3-5");
       tLJTempFeeSchema.setPayMoney(10);
       tLJTempFeeSchema.setEnterAccDate("2003-3-5");
       tLJTempFeeSchema.setRiskCode("111501");
       tLJTempFeeSchema.setAgentCode("0000000001");
       tLJTempFeeSchema.setAgentGroup("010101");
       tLJTempFeeSchema.setManageCom("86110000");
       tLJTempFeeSchema.setOperator("001");
     tLJTempFeeSet.add(tLJTempFeeSchema);

       tLJTempFeeClassSchema    = new LJTempFeeClassSchema();
       tLJTempFeeClassSchema.setTempFeeNo("12222222222222222225");
       tLJTempFeeClassSchema.setPayMode("1");
       tLJTempFeeClassSchema.setPayDate("2003-3-5");
       tLJTempFeeClassSchema.setPayMoney(20);
       tLJTempFeeClassSchema.setEnterAccDate("2003-3-5");
       tLJTempFeeClassSchema.setManageCom("86110000");
     tLJTempFeeClassSet.add(tLJTempFeeClassSchema);
//5th record:
       tLJTempFeeSchema     = new LJTempFeeSchema();
       tLJTempFeeSchema.setTempFeeNo("12222222222222222226");
       tLJTempFeeSchema.setTempFeeType("1");
       tLJTempFeeSchema.setPayDate("2003-3-5");
       tLJTempFeeSchema.setPayMoney(10);
       tLJTempFeeSchema.setEnterAccDate("2003-3-5");
       tLJTempFeeSchema.setRiskCode("111301");
       tLJTempFeeSchema.setAgentCode("0000000001");
       tLJTempFeeSchema.setAgentGroup("010101");
       tLJTempFeeSchema.setManageCom("86110000");
       tLJTempFeeSchema.setOperator("001");
     tLJTempFeeSet.add(tLJTempFeeSchema);

       tLJTempFeeSchema     = new LJTempFeeSchema();
       tLJTempFeeSchema.setTempFeeNo("12222222222222222226");
       tLJTempFeeSchema.setTempFeeType("1");
       tLJTempFeeSchema.setPayDate("2003-3-5");
       tLJTempFeeSchema.setPayMoney(10);
       tLJTempFeeSchema.setEnterAccDate("2003-3-5");
       tLJTempFeeSchema.setRiskCode("111501");
       tLJTempFeeSchema.setAgentCode("0000000001");
       tLJTempFeeSchema.setAgentGroup("010101");
       tLJTempFeeSchema.setManageCom("86110000");
       tLJTempFeeSchema.setOperator("001");
     tLJTempFeeSet.add(tLJTempFeeSchema);

       tLJTempFeeClassSchema    = new LJTempFeeClassSchema();
       tLJTempFeeClassSchema.setTempFeeNo("12222222222222222226");
       tLJTempFeeClassSchema.setPayMode("1");
       tLJTempFeeClassSchema.setPayDate("2003-3-5");
       tLJTempFeeClassSchema.setPayMoney(20);
       tLJTempFeeClassSchema.setEnterAccDate("2003-3-5");
       tLJTempFeeClassSchema.setManageCom("86110000");
     tLJTempFeeClassSet.add(tLJTempFeeClassSchema);
//6th record:
       tLJTempFeeSchema     = new LJTempFeeSchema();
       tLJTempFeeSchema.setTempFeeNo("12222222222222222227");
       tLJTempFeeSchema.setTempFeeType("1");
       tLJTempFeeSchema.setPayDate("2003-3-5");
       tLJTempFeeSchema.setPayMoney(10);
       tLJTempFeeSchema.setEnterAccDate("2003-3-5");
       tLJTempFeeSchema.setRiskCode("111301");
       tLJTempFeeSchema.setAgentCode("0000000001");
       tLJTempFeeSchema.setAgentGroup("010101");
       tLJTempFeeSchema.setManageCom("86110000");
       tLJTempFeeSchema.setOperator("001");
     tLJTempFeeSet.add(tLJTempFeeSchema);

       tLJTempFeeClassSchema    = new LJTempFeeClassSchema();
       tLJTempFeeClassSchema.setTempFeeNo("12222222222222222227");
       tLJTempFeeClassSchema.setPayMode("1");
       tLJTempFeeClassSchema.setPayDate("2003-3-5");
       tLJTempFeeClassSchema.setPayMoney(10);
       tLJTempFeeClassSchema.setEnterAccDate("2003-3-5");
       tLJTempFeeClassSchema.setManageCom("86110000");
     tLJTempFeeClassSet.add(tLJTempFeeClassSchema);
//7th record:
       tLJTempFeeSchema     = new LJTempFeeSchema();
       tLJTempFeeSchema.setTempFeeNo("12222222222222222229");
       tLJTempFeeSchema.setTempFeeType("1");
       tLJTempFeeSchema.setPayDate("2003-3-5");
       tLJTempFeeSchema.setPayMoney(10);
       tLJTempFeeSchema.setEnterAccDate("2003-3-5");
       tLJTempFeeSchema.setRiskCode("111301");
       tLJTempFeeSchema.setAgentCode("0000000001");
       tLJTempFeeSchema.setAgentGroup("010101");
       tLJTempFeeSchema.setManageCom("86110000");
       tLJTempFeeSchema.setOperator("001");
     tLJTempFeeSet.add(tLJTempFeeSchema);

       tLJTempFeeSchema     = new LJTempFeeSchema();
       tLJTempFeeSchema.setTempFeeNo("12222222222222222229");
       tLJTempFeeSchema.setTempFeeType("1");
       tLJTempFeeSchema.setPayDate("2003-3-5");
       tLJTempFeeSchema.setPayMoney(10);
       tLJTempFeeSchema.setEnterAccDate("2003-3-5");
       tLJTempFeeSchema.setRiskCode("111501");
       tLJTempFeeSchema.setAgentCode("0000000001");
       tLJTempFeeSchema.setAgentGroup("010101");
       tLJTempFeeSchema.setManageCom("86110000");
       tLJTempFeeSchema.setOperator("001");
     tLJTempFeeSet.add(tLJTempFeeSchema); */

//    tLJTempFeeClassSchema = new LJTempFeeClassSchema();
//    tLJTempFeeClassSchema.setTempFeeNo("81000000913");
//    tLJTempFeeClassSchema.setPayMode("13");
//    tLJTempFeeClassSchema.setPayDate("2006-03-20");
//    tLJTempFeeClassSchema.setPayMoney(22688.00);
//    //tLJTempFeeClassSchema.setEnterAccDate(null);
//
//    tLJTempFeeClassSchema.setManageCom("86110000");
//    tLJTempFeeClassSchema.setOperator("001");
//    tLJTempFeeClassSet.add(tLJTempFeeClassSchema);


    //7th record:
    tLJTempFeeSchema = new LJTempFeeSchema();
    tLJTempFeeSchema.setTempFeeNo("31000001132");
    tLJTempFeeSchema.setTempFeeType("4");
    tLJTempFeeSchema.setPayDate("2006-10-18");
    tLJTempFeeSchema.setPayMoney(72.50);
    tLJTempFeeSchema.setEnterAccDate("");
    tLJTempFeeSchema.setRiskCode("000000");
    tLJTempFeeSchema.setAgentCode("1101000016");
    tLJTempFeeSchema.setAgentGroup("000000000014");
    tLJTempFeeSchema.setManageCom("86110000");
    tLJTempFeeSchema.setOperator("pa0001");
    tLJTempFeeSchema.setOtherNo("");
    tLJTempFeeSchema.setOtherNoType("13");
    tLJTempFeeSet.add(tLJTempFeeSchema);
    //    tLJTempFeeClassSchema = new LJTempFeeClassSchema();

    tLJTempFeeClassSchema = new LJTempFeeClassSchema();
    tLJTempFeeClassSchema.setTempFeeNo("31000001132");
    tLJTempFeeClassSchema.setPayMode("1");
    tLJTempFeeClassSchema.setPayDate("2006-10-18");
    tLJTempFeeClassSchema.setPayMoney(72.50);
    tLJTempFeeClassSchema.setChequeNo("");
    tLJTempFeeClassSchema.setChequeDate("");
    tLJTempFeeClassSchema.setEnterAccDate("");
    tLJTempFeeClassSchema.setManageCom("86110000");
    tLJTempFeeClassSchema.setBankCode("");
    tLJTempFeeClassSchema.setBankAccNo("");
    tLJTempFeeClassSchema.setAccName("");
    tLJTempFeeClassSchema.setInsBankCode("");
    tLJTempFeeClassSchema.setOperator("pa0001");
    tLJTempFeeClassSet.add(tLJTempFeeClassSchema);

    /*tLJTempFeeClassSchema   = new LJTempFeeClassSchema();
      tLJTempFeeClassSchema.setTempFeeNo("81000011844");
      tLJTempFeeClassSchema.setPayMode("10");
      tLJTempFeeClassSchema.setPayDate("2006-10-13");
      tLJTempFeeClassSchema.setPayMoney(5638548);
      tLJTempFeeClassSchema.setChequeNo("");
      tLJTempFeeClassSchema.setChequeDate("");
      tLJTempFeeClassSchema.setEnterAccDate("");
      tLJTempFeeClassSchema.setManageCom("8653000");
      tLJTempFeeClassSchema.setBankCode("");
      tLJTempFeeClassSchema.setBankAccNo("");
      tLJTempFeeClassSchema.setAccName("");
      tLJTempFeeClassSchema.setInsBankCode("");
      tLJTempFeeClassSchema.setOperator("group");
      tLJTempFeeClassSet.add(tLJTempFeeClassSchema);*/


    VData tVData = new VData();
    tVData.addElement(tLJTempFeeSet);
    tVData.addElement(tLJTempFeeClassSet);
    tVData.addElement(tGI);

    TempFeeBL tTempFeeBL = new TempFeeBL();

    tTempFeeBL.submitData(tVData, "INSERT");
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    System.out.println("Operate==" + cOperate);
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    System.out.println("After getinputdata");

    if (!checkData()) {
      return false;
    }

    //进行业务处理
    if (!dealData()) {
      return false;
    }
    System.out.println("After dealData！");
    //准备往后台的数据
    if (!prepareOutputData()) {
      return false;
    }
    System.out.println("After prepareOutputData");

    System.out.println("Start TempFee BL Submit...");

    TempFeeBLS tTempFeeBLS = new TempFeeBLS();
    tTempFeeBLS.submitData(mInputData, cOperate);

    System.out.println("End LJTempFee BL Submit...");

    //如果有需要处理的错误，则返回
    if (tTempFeeBLS.mErrors.needDealError()) {
      this.mErrors.copyAllErrors(tTempFeeBLS.mErrors);
      for (int m = 0; m < tTempFeeBLS.mErrors.getErrorCount(); m++) {
        System.out.println(tTempFeeBLS.mErrors.getError(1).functionName);
        System.out.println(tTempFeeBLS.mErrors.getError(1).errorMessage);
      }
      mInputData = null;
      return false;
    }
    System.out.println("TempFeeBL end");

    //单证回收
    if (this.mOperate.equals("INSERT")) {
      PubCertifyTakeBack tPubCertifyTakeBack = new PubCertifyTakeBack();
      if (tPubCertifyTakeBack.CheckNewType(tPubCertifyTakeBack.
                                           CERTIFY_CheckNo1)) { //如果需要单证回收
        String operator = tGI.Operator;
        LJTempFeeSchema tLJTempFee = new LJTempFeeSchema();
        for (int i = 1; i <= mLJTempFeeSetNew.size(); i++) {
          tLJTempFee = mLJTempFeeSetNew.get(i);
          //回收定额单
          if (tLJTempFee.getTempFeeType().equals("6")) {
            System.out.println("开始回收定额单");
            String CertifyCode = tLJTempFee.getState();
            if (CertifyCode == null) {
              continue;
            }
//            LDSysVarDB tLDSysVarDB = new LDSysVarDB();
//            tLDSysVarDB.setSysVar("NotVerifyCertifyCode");
//            if(tLDSysVarDB.getInfo()==false)
//            {
//                continue;
//            }
//            String arrValue[]=PubFun.split(tLDSysVarDB.getSysVarValue(),"/");
//            boolean returnflag=false;
//            for(int n=0;n<arrValue.length;n++)
//            {
//                System.out.println("单证查询到的不用回收的险种:"+arrValue[n]);
//
//                if(CertifyCode.equals(arrValue[n]))
//                   {
//                      returnflag=true;
//                      break;
//                   }
//            }
//            System.out.println("不用回收标记:"+returnflag);
//            if(returnflag) continue;

            if (!tPubCertifyTakeBack.CertifyTakeBack_A(tLJTempFee.
                                                       getOtherNo(), tLJTempFee.getOtherNo(),
                                                       CertifyCode, operator)) {
              System.out.println("单证回收错误（定额单" + i + "）:" +
                                 tLJTempFee.getOtherNo());
              System.out.println("错误原因：" +
                                 tPubCertifyTakeBack.mErrors.
                                 getFirstError().toString());
            }
          }
          //回收普通单证
          else {
            if (tLJTempFee.getTempFeeType().equals("5")) { //类型为5的不用回收单证，跳出
              continue;
            } else {
              System.out.println("开始回收普通单证");
              System.out.println("tLJTempFee.getTempFeeNo() is :" +
                                 tLJTempFee.getTempFeeNo());
              System.out.println(
                  "tPubCertifyTakeBack.CERTIFY_TempFee is :" +
                  tPubCertifyTakeBack.CERTIFY_TempFee);

              //民生程序写死了暂交费类型，实在是不好的方式
              //需要特殊处理一下，应该是动态选取
              LMCertifyDesDB tLMCertifyDesDB = new LMCertifyDesDB();
              tLMCertifyDesDB.setCertifyClass2("0");
              LMCertifyDesSet tLMCertifyDesSet = new
                  LMCertifyDesSet();
              //查选全部的暂交费单证信息
              tLMCertifyDesSet = tLMCertifyDesDB.query();

              LZCardDB tLZCardDB = new LZCardDB();
              LZCardSet tLZCardSet = new LZCardSet();
              String tSql = "";
              String tCertifyType = "";
              //循环查询此单证号码，属于那个暂交费单证
              for (int m = 1; m <= tLMCertifyDesSet.size(); m++) {
                tSql =
                    "select CertifyCode from lzcard where certifycode = '" +
                    tLMCertifyDesSet.get(m).getCertifyCode() +
                    "' and startno <= '" +
                    tLJTempFee.getTempFeeNo() +
                    "' and endno >= '" +
                    tLJTempFee.getTempFeeNo() + "'";
                System.out.println("查询不同单证类型的数据" + tSql);
                tLZCardSet = tLZCardDB.executeQuery(tSql);
                //如果根据当前的单证类型，查询道此单证号码
                if (tLZCardSet.size() != 0) {
                  tCertifyType = tLZCardSet.get(1).
                      getCertifyCode();
                  break;
                }
              }
              System.out.println("tCertifyType is" + tCertifyType);
              if (!tPubCertifyTakeBack.CertifyTakeBack_A(
                  tLJTempFee.getTempFeeNo(),
                  tLJTempFee.getTempFeeNo(),
                  tCertifyType,
                  operator)) {
                System.out.println("单证回收错误（暂交费" + i + "）:" +
                                   tLJTempFee.getTempFeeNo());
                System.out.println("错误原因：" +
                                   tPubCertifyTakeBack.mErrors.
                                   getFirstError().toString());
              }
            }
          }
        }
      }
      /** 进行卡单的单证回收 */
      if (!cardTakeBack()) {
        return false;
      }
    }
    //续期应收数据状态修改 2011-11-4
    if(!finUrgeVerify()){
        return false;
    }
    
    if(!afterSubmit())
    {
        return false;
    }

    System.out.println("单证回收完毕");
    mInputData = null;
    return true;
  }

  /**
 * CardTakeBack
 *
 * @return boolean
 */
private boolean cardTakeBack() {
    MMap map = new MMap();
    if (this.mLJTempFeeSet.get(1).getTempFeeType().equals("6")) {
        /** 进行卡单的单证回收和实收数据的生成 */
        LZCardPayDB tLZCardPayDB = new LZCardPayDB();
        tLZCardPayDB.setPayNo(mLJTempFeeSet.get(1).getOtherNo());
        tLZCardPayDB.setState("1");
        LZCardPaySet tLZCardPaySet = tLZCardPayDB.query();

        if (tLZCardPaySet == null || tLZCardPaySet.size() <= 0) {
            String str = "查询卡单费用表失败!";
            buildError("cardTakeBack", str);
            System.out.println("在程序TempFeeBL.cardTakeBack() - 491 : " + str);
            return false;
        }
        LZCardSet mLZCardSet = new LZCardSet();
        LZCardTrackSet mLZCardTrackSet = new LZCardTrackSet();
        mLJAPayPersonSet = new LJAPayPersonSet();
        mLJAPaySet = new LJAPaySet();
        for (int i = 1; i <= tLZCardPaySet.size(); i++) {
            LZCardDB tLZCardDB = new LZCardDB();
            String tPayNo = PubFun1.CreateMaxNo("PAYNO",
                    PubFun.getNoLimit(tLZCardPaySet.get(i).getManageCom()));

//        tLZCardDB.setCertifyCode(tLZCardPaySet.get(i).getCertifyCode());
            tLZCardDB.setSubCode(tLZCardPaySet.get(i).getCardType());
            tLZCardDB.setStartNo(tLZCardPaySet.get(i).getStartNo());
            tLZCardDB.setEndNo(tLZCardPaySet.get(i).getEndNo());
            LZCardSet tLZCardSet = tLZCardDB.query();
            if (tLZCardSet == null || tLZCardSet.size() <= 0) {
                String str = "查询卡单信息失败!";
                buildError("cardTakeBack", str);
                System.out.println("在程序TempFeeBL.cardTakeBack() - 504 : " +
                                   str);
                return false;
            }


            for (int m = 1; m <= tLZCardSet.size(); m++) {
                tLZCardSet.get(m).setState("2"); //正常回销
                LZCardNumberDB tLZCardNumberDB = new LZCardNumberDB();
                tLZCardNumberDB.setCardType(tLZCardSet.get(m).getSubCode());
                //tLZCardNumberDB.setOperateType("0");
                tLZCardNumberDB.setCardSerNo(tLZCardSet.get(m).getStartNo());

                LZCardNumberSet tLZCardNumberSet = tLZCardNumberDB.query();
                if (tLZCardNumberSet == null ||
                    tLZCardNumberSet.size() <= 0) {
                    String str = "查询单证号码表失败!";
                    buildError("cardTakeBack", str);
                    System.out.println(
                            "在程序TempFeeBL.cardTakeBack() - 593 : " + str);
                    return false;
                }
                if (tLZCardNumberSet.size() > 1) {
                    String str = "查询单证号码信息不唯一!";
                    buildError("cardTakeBack", str);
                    System.out.println(
                            "在程序TempFeeBL.cardTakeBack() - 600 : " + str);
                    return false;
                }
                LZCardSchema tLZCardSchema = new LZCardSchema();
                tLZCardSchema = tLZCardSet.get(m).getSchema();

                LZCardTrackSchema tLZCardTrackSchema = new
                        LZCardTrackSchema();
                tLZCardTrackSchema.setCertifyCode(tLZCardSchema.
                        getCertifyCode());

                tLZCardTrackSchema.setSubCode(tLZCardSchema.
                                              getSubCode());

                tLZCardTrackSchema.setRiskCode("0");
                tLZCardTrackSchema.setRiskVersion("0");

                tLZCardTrackSchema.setStartNo(tLZCardSchema.
                                              getStartNo());
                tLZCardTrackSchema.setEndNo(tLZCardSchema.getEndNo());
                tLZCardTrackSchema.setSendOutCom(tLZCardSchema.
                        getSendOutCom());
                tLZCardTrackSchema.setReceiveCom(tLZCardSchema.
                        getReceiveCom());
                tLZCardTrackSchema.setSumCount(tLZCardSchema.
                        getSumCount());
                tLZCardTrackSchema.setPrem("");
                tLZCardTrackSchema.setAmnt("");
                tLZCardTrackSchema.setHandler(tLZCardSchema.getHandler());
                tLZCardTrackSchema.setHandleDate(tLZCardSchema.
                        getHandleDate());
                tLZCardTrackSchema.setInvaliDate(tLZCardSchema.
                        getInvaliDate());
                tLZCardTrackSchema.setTakeBackNo(tLZCardSchema.
                        getTakeBackNo());
                tLZCardTrackSchema.setSaleChnl(tLZCardSchema.
                        getSaleChnl());
                tLZCardTrackSchema.setStateFlag(tLZCardSchema.
                        getStateFlag());
                tLZCardTrackSchema.setOperateFlag("0");
                tLZCardTrackSchema.setPayFlag(tLZCardSchema.getPayFlag());
                tLZCardTrackSchema.setEnterAccFlag("");
                tLZCardTrackSchema.setReason("");
                tLZCardTrackSchema.setState(tLZCardSet.get(m).getState());
                tLZCardTrackSchema.setOperator(tGI.Operator);
                tLZCardTrackSchema.setMakeDate(PubFun.getCurrentDate());
                tLZCardTrackSchema.setMakeTime(PubFun.getCurrentTime());
                tLZCardTrackSchema.setModifyDate(PubFun.getCurrentDate());
                tLZCardTrackSchema.setModifyTime(PubFun.getCurrentTime());

                mLZCardTrackSet.add(tLZCardTrackSchema);

                /** 生成实收数据 */
                LJAPayPersonSchema tLJAPayPersonSchema = new
                        LJAPayPersonSchema();
                tLJAPayPersonSchema.setPayNo(tPayNo);
                tLJAPayPersonSchema.setPolNo(SysConst.ZERONO);
                tLJAPayPersonSchema.setPayCount(1);
                tLJAPayPersonSchema.setGrpContNo(SysConst.ZERONO);
                tLJAPayPersonSchema.setGrpPolNo(SysConst.ZERONO);
                tLJAPayPersonSchema.setContNo(tLZCardNumberSet.get(1).
                                              getCardNo());
                tLJAPayPersonSchema.setManageCom(tLZCardPaySet.get(i).
                        getManageCom());
                tLJAPayPersonSchema.setAgentCom(tLZCardPaySet.get(i).
                        getAgentCom());
                tLJAPayPersonSchema.setRiskCode(tLZCardSet.get(m).
                        getRiskCode());
                tLJAPayPersonSchema.setAgentCode(tLZCardPaySet.get(i).
                        getHandlerCode()); //代理人
//          tLJAPayPersonSchema.setAgentGroup(tLCPolDB.getAgentGroup());
                tLJAPayPersonSchema.setGetNoticeNo(tLZCardPaySet.get(i).
                        getPayNo());
                tLJAPayPersonSchema.setPayAimClass("2");
                tLJAPayPersonSchema.setDutyCode("000000");
                tLJAPayPersonSchema.setPayPlanCode("000000");
                tLJAPayPersonSchema.setSumDuePayMoney(tLZCardPaySet.get(i).
                        getDuePayMoney());
                tLJAPayPersonSchema.setSumActuPayMoney(tLZCardPaySet.get(i).
                        getActPayMoney());
                tLJAPayPersonSchema.setPayIntv(0);
                tLJAPayPersonSchema.setPayDate(PubFun.getCurrentDate());
                tLJAPayPersonSchema.setPayType("ZC");
//          tLJAPayPersonSchema.setLastPayToDate(tLCPolDB.getCValiDate());
//          tLJAPayPersonSchema.setCurPayToDate(tLCPolDB.getPaytoDate());
//          tLJAPayPersonSchema.setApproveCode(tLCPolDB.getApproveCode());
//          tLJAPayPersonSchema.setApproveDate(tLCPolDB.getApproveDate());
//          tLJAPayPersonSchema.setApproveTime(tLCPolDB.getApproveTime());
//          tLJAPayPersonSchema.setPayNo(mPayNo);
                tLJAPayPersonSchema.setOperator(this.tGI.Operator);
                tLJAPayPersonSchema.setMakeDate(PubFun.getCurrentDate());
                tLJAPayPersonSchema.setMakeTime(PubFun.getCurrentTime());
                tLJAPayPersonSchema.setModifyDate(PubFun.getCurrentDate());
                tLJAPayPersonSchema.setModifyTime(PubFun.getCurrentTime());
                tLJAPayPersonSchema.setEnterAccDate(PubFun.getCurrentDate());
                tLJAPayPersonSchema.setConfDate(PubFun.getCurrentDate());
                tLJAPayPersonSchema.setPayDate(PubFun.getCurrentDate());

                mLJAPayPersonSet.add(tLJAPayPersonSchema);
                mLZCardSet.add(tLZCardSet);
            }

            /** 生成实收总表 */
            LJAPaySchema tLJAPaySchema = new LJAPaySchema();

            tLJAPaySchema.setPayNo(tPayNo);
            tLJAPaySchema.setIncomeNo(tLZCardPaySet.get(i).getPayNo());
            tLJAPaySchema.setIncomeType("15");
//        tLJAPaySchema.setAppntNo(mLCContSchema.getAppntNo());
            tLJAPaySchema.setSumActuPayMoney(tLZCardPaySet.get(i).getActPayMoney());
//
//        tLJAPaySchema.setApproveCode(mLCContSchema.getApproveCode());
//        tLJAPaySchema.setApproveDate(mLCContSchema.getApproveDate());
//        tLJAPaySchema.setSerialNo();
            tLJAPaySchema.setOperator(tGI.Operator);
            tLJAPaySchema.setMakeDate(PubFun.getCurrentDate());
            tLJAPaySchema.setMakeTime(PubFun.getCurrentTime());
            tLJAPaySchema.setGetNoticeNo(tLZCardPaySet.get(i).getPayNo());
            tLJAPaySchema.setModifyDate(PubFun.getCurrentDate());
            tLJAPaySchema.setModifyTime(PubFun.getCurrentTime());
            tLJAPaySchema.setManageCom(tLZCardPaySet.get(i).getManageCom());
            tLJAPaySchema.setAgentCom(tLZCardPaySet.get(i).getAgentCom());
//        tLJAPaySchema.setBankCode(mLCContSchema.getBankCode());
//        tLJAPaySchema.setBankAccNo(mLCContSchema.getBankAccNo());
            tLJAPaySchema.setAgentCode(tLZCardPaySet.get(i).getHandlerCode());
//        tLJAPaySchema.setAgentGroup(mLCContSchema.getAgentGroup());
//        tLJAPaySchema.setAccName(mLCContSchema.getAccName());
            tLJAPaySchema.setEnterAccDate(PubFun.getCurrentDate());
            tLJAPaySchema.setConfDate(PubFun.getCurrentDate());
            tLJAPaySchema.setPayDate(PubFun.getCurrentDate());
            mLJAPaySet.add(tLJAPaySchema);

            tLZCardPaySet.get(i).setConfDate(PubFun.getCurrentDate());
            tLZCardPaySet.get(i).setState("1"); //已核销
            map.put(mLZCardSet, "UPDATE"); //完成卡单回销
            map.put(tLZCardPaySet, "UPDATE"); //完成卡单回销
            map.put(mLJAPaySet, "INSERT"); //完成卡单回销
            map.put(mLJAPayPersonSet, "INSERT"); //完成卡单回销
            map.put(mLZCardTrackSet, "INSERT"); //完成卡单回销
        }
    }
    for (int i = 1; i <= map.size(); i++) {
        System.out.println("map : " + map);
    }
    VData vData = new VData();
    vData.add(map);
    PubSubmit ps = new PubSubmit();
    if (!ps.submitData(vData, null)) {
        this.mErrors.copyAllErrors(ps.mErrors);
        return false;
    }
    return true;
}

  /**
   * creatActuPay
   *
   * @return boolean
   */
  private boolean creatActuPay() {
    return true;
  }

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData() {
    int i, iMax;
    boolean tReturn = false;
    String tempFeeFlag = "";
    String tEngageTempFeeNo = ""; // 记录预打报单交费收据号
    //产生流水号
    LJTempFeeBL tempLJTempFeeBL = new LJTempFeeBL();
    tempLJTempFeeBL.setSchema(mLJTempFeeSet.get(1).getSchema());
    tLimit = PubFun.getNoLimit(tempLJTempFeeBL.getManageCom());

    if (this.mOperate.equals("INSERT")) {
      serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
    } else {
      serNo = tempLJTempFeeBL.getSerialNo();
    }
    boolean newFlag = false; //判断新单标记

    //添加纪录
    if (this.mOperate.equals("INSERT") || this.mOperate.equals("UPDATE")) {
      //1-处理暂交费表，记录集，循环处理
      LJTempFeeBL tLJTempFeeBL;
      iMax = mLJTempFeeSet.size();
      String saveNo[][] = new String[iMax][2];
      String PolNo = "";
      int num = 0;
      existTempFeeNo = new String[iMax]; //保存重复的数据库中已经存在的暂交费号码
      //定额单判断生成暂交费号
      HashMap cardTempFeeNoMap = new HashMap();
      boolean sameCardFlag = false;

      int NoIndex = 0; //保存号码的数目,作为existTempFeeNo[]的参数
      for (i = 1; i <= iMax; i++) {
        tLJTempFeeBL = new LJTempFeeBL();
        tLJTempFeeBL.setSchema(mLJTempFeeSet.get(i).getSchema());
        System.out.println(mLJTempFeeSet.get(i).getEnterAccDate());
        //System.out.println(tLJTempFeeBL.getOtherNo());
        //System.out.println(tLJTempFeeBL.getEnterAccDate());
        //* ↓ *** liuhao *** 2005-05-17 *** add *****
         //判断预打保单时缴费是否能一次交齐
         if (tLJTempFeeBL.getTempFeeType().equals("a")) {
           if (!checkLJAPay(tLJTempFeeBL)) {
             CError tError = new CError();
             tError.moduleName = "LJTempFeeBL";
             tError.functionName = "dealData";
             tError.errorMessage = "预打保单(" +
                 tLJTempFeeBL.getOtherNo() +
                 ")交费与应交金额不等。";
             this.mErrors.addOneError(tError);
             System.out.println("预打保单(" +
                                tLJTempFeeBL.getOtherNo() +
                                ")交费与应交金额不等。");
             continue;
           }
         }
         
         if (tLJTempFeeBL.getTempFeeType().equals("25")) {
        	LJSPaySchema mLJSPaySchema=new LJSPaySchema();
        	LJSPayDB mLJSPayDB=new LJSPayDB();
       	     mLJSPayDB.setGetNoticeNo(mLJTempFeeSet.get(1).getTempFeeNo());
           	mLJSPayDB.setOtherNo(mLJTempFeeSet.get(1).getOtherNo());

            LLCasePayAdvancedTraceSchema mLLCasePayAdvancedTraceSchema = new LLCasePayAdvancedTraceSchema();
        	LLCasePayAdvancedTraceDB mLLCasePayAdvancedTraceDB = new LLCasePayAdvancedTraceDB();
        	mLLCasePayAdvancedTraceDB.setGetNoticeNo(mLJTempFeeSet.get(1).getTempFeeNo());
        	mLLCasePayAdvancedTraceDB.setGrpContNo(mLJTempFeeSet.get(1).getOtherNo());
        	mLLCasePayAdvancedTraceSchema=mLLCasePayAdvancedTraceDB.query().get(1);
        	mLLCasePayAdvancedTraceSchema.setDealState("04");
        	mLLCasePayAdvancedTraceSchema.setModifyDate(PubFun.getCurrentDate());
        	mLLCasePayAdvancedTraceSchema.setModifyTime(PubFun.getCurrentTime());
         	System.out.println("LJTempFeeBL889行：mLLCasePayAdvancedTraceSchema中getnoticeno:"+mLLCasePayAdvancedTraceSchema.getGetNoticeNo()+".grpcontno:"+mLLCasePayAdvancedTraceSchema.getGrpContNo());
         	try {
         		//mLLCasePayAdvancedTraceDB=null;
         		mLJSPayDB.delete();
         		mLLCasePayAdvancedTraceDB.setSchema(mLLCasePayAdvancedTraceSchema);
         		mLLCasePayAdvancedTraceDB.update();
         	      } catch (Exception e) {
         	    	 CError tError = new CError();
                     tError.moduleName = "LJTempFeeBL";
                     tError.functionName = "dealData";
                     tError.errorMessage = "社保报销(" +
                         tLJTempFeeBL.getTempFeeNo() +
                         ")删除应收表或更改社保报销轨迹表失败。";
                     this.mErrors.addOneError(tError);
                     System.out.println("社保报销(" +
                             tLJTempFeeBL.getTempFeeNo() +
                             ")删除应收表或更改社保报销轨迹表失败。");
         	      }
           }
        //* ↑ *** liuhao *** 2005-05-17 *** add *****
         //后续添加：将到帐日期赋给财务确认操作日期
//        tLJTempFeeBL.setConfMakeDate(tLJTempFeeBL.getEnterAccDate());
         //Modify by Minim，判断到帐日期，不为空则设置操作日期为当天
         if (tLJTempFeeBL.getEnterAccDate() != null &&
             !tLJTempFeeBL.getEnterAccDate().equals("")) {
           tLJTempFeeBL.setConfMakeDate(PubFun.getCurrentDate());
           tLJTempFeeBL.setConfMakeTime(PubFun.getCurrentTime());
         }

        //首期交费时，管理机构和交费机构相同
        //预打保单交费和首期交费类似
        //* ↓ *** liuhao *** 2005-05-17 *** modify *****
         if (tLJTempFeeBL.getTempFeeType().equals("1") ||
             tLJTempFeeBL.getTempFeeType().equals("a")) {
           //* ↑ *** liuhao *** 2005-05-17 *** modify *****
            tLJTempFeeBL.setPolicyCom(tLJTempFeeBL.getManageCom());
           tempFeeFlag = "1"; //首期交费标志
           //保存投保人名称
          String tSql = "select * from LCGrpCont where p1rtno='" +
                  tLJTempFeeBL.getOtherNo() + "' with ur";
           System.out.println(tSql);
           LCGrpContDB tLCGrpContDB = new LCGrpContDB();
           LCGrpContSet tLCGrpContSet = new LCGrpContSet();
           tLCGrpContSet = tLCGrpContDB.executeQuery(tSql);
           if (tLCGrpContSet.size() > 0) {
               tLJTempFeeBL.setAPPntName(tLCGrpContSet.get(1).
                            getGrpName());
               tLJTempFeeBL.setOtherNoType("5");
           }else{
               tSql = "select * from LCCont where prtno='" +
                   tLJTempFeeBL.getOtherNo() + "' with ur";
               System.out.println(tSql);
               LCContDB tLCContDB = new LCContDB();
               LCContSet tLCContSet = new LCContSet();
               tLCContSet = tLCContDB.executeQuery(tSql);
               System.out.println("PrtNo = " + tLJTempFeeBL.getOtherNo());
               if (tLCContSet.size() > 0) {
                 tLJTempFeeBL.setAPPntName(tLCContSet.get(1).
                                           getAppntName());
               }
               if(tLCContSet.size() > 0 && tLCContSet.get(1).getContType().equals("2")){
            	   tLJTempFeeBL.setOtherNoType("5");
               } else {
            	   tLJTempFeeBL.setOtherNoType("4");
               }
           }
           
           //如果是预打保单需要处理首期交费日期,并且将otherno换为保单号,
           if (tLJTempFeeBL.getTempFeeType().equals("a")) {
             StringBuffer sql = new StringBuffer();
             sql.append("select grpcontno from lcgrpcont where prtno='");
             sql.append(tLJTempFeeBL.getOtherNo());
             sql.append("'");
             ExeSQL tExeSQL = new ExeSQL();
             tLJTempFeeBL.setOtherNo(tExeSQL.getOneValue(sql.toString()));
             tLJTempFeeBL.setOtherNoType("7"); //团体保单号
             tLJTempFeeBL.setConfDate(PubFun.getCurrentDate());
             tLJTempFeeBL.setConfDate(PubFun.getCurrentTime());
           }
         }
        //在数据库中查询该纪录是否已经存在,如果已存在，将号码保存在数组中
        if (!queryTempFee(tLJTempFeeBL.getSchema()) &&
            this.mOperate.equals("INSERT")) {
          existTempFeeNo[NoIndex] = tLJTempFeeBL.getTempFeeNo();
          NoIndex = NoIndex + 1;
          continue;
        }

        //判断该保单号码类型，填充OtherNoType字段
        PolNo = tLJTempFeeBL.getOtherNo();
        //当是续期收费时，判断该号码的类型
        //* ↑ *** zhangjun *** 2008-05-28 *** modify ***** 取掉续期续保特殊处理，修改保全OtherNotype取值
        if (tLJTempFeeBL.getTempFeeType().equals("3") ||
            tLJTempFeeBL.getTempFeeType().equals("4") ||
            tLJTempFeeBL.getTempFeeType().equals("9")) {
          System.out.println("PolNo:" + PolNo);
          String sql = "Select OthernoType From LJSPay Where OtherNo = '"+PolNo+"'";
          ExeSQL mExeSQL = new ExeSQL();
          tLJTempFeeBL.setOtherNoType(mExeSQL.getOneValue(sql));
//          if (CodeJudge.judgeCodeType(PolNo, "21")) { //个人保单号
//            tLJTempFeeBL.setOtherNoType("0");
//          }
//          if (CodeJudge.judgeCodeType(PolNo, "22")) { //集体保单号
//            tLJTempFeeBL.setOtherNoType("1");
//          }else{                                       //存储合同号
//            tLJTempFeeBL.setOtherNoType("2");
//          }
        }
        //repair:合同号
        //tLJTempFeeBL.setOtherNoType("4");
        tLJTempFeeBL.setSerialNo(serNo);

        if (this.mOperate.equals("INSERT")) {
          tLJTempFeeBL.setMakeDate(CurrentDate); //入机日期
          tLJTempFeeBL.setMakeTime(CurrentTime); //入机时间
        }

        tLJTempFeeBL.setModifyDate(CurrentDate); //最后一次修改日期
        tLJTempFeeBL.setModifyTime(CurrentTime); //最后一次修改时间
        tLJTempFeeBL.setConfFlag("0"); //核销标志置0
        //* ↓ *** liuhao *** 2005-05-17 *** add *****
         //如果是预打保单交费  设置核销标志为 1
         if (tLJTempFeeBL.getTempFeeType().equals("a")) {
           tLJTempFeeBL.setConfFlag("1"); //核销标志置1
         }
        //* ↑ *** liuhao *** 2005-05-17 *** add *****

         if (tLJTempFeeBL.getTempFeeType().equals("3") &&
             this.mOperate.equals("INSERT")) { //如果是不定期交费，要产生暂交费号
           tLimit = PubFun.getNoLimit(tLJTempFeeBL.getManageCom()); //产生通知书号即暂交费号
           tNo = PubFun1.CreateMaxNo("GETNOTICENO", tLimit);
           tLJTempFeeBL.setTempFeeNo(tNo);
           saveNo[num][0] = PolNo; //第一位存放保单号
           saveNo[num][1] = tNo; //第二位存放新生成的暂交费号
           num++; //后面循环用到;为了与对应的暂交费分类纪录的关联（保单号相同则赋予新生成的暂交费号）
         }

        if (tLJTempFeeBL.getTempFeeType().equals("5") &&
            this.mOperate.equals("INSERT")) { //如果是银行扣款，要产生暂交费号
          sameCardFlag = false;
          tLJTempFeeBL.setOtherNo(tLJTempFeeBL.getTempFeeNo()); //将保存在暂加费号处的印刷号重新放置在其它号码处
          tLJTempFeeBL.setOtherNoType("4"); //其它号码类型置印刷号

          for (int k = 0; k < cardTempFeeNoMap.size(); k++) {
            if (cardTempFeeNoMap.get(tLJTempFeeBL.getOtherNo()) != null) {
              tLJTempFeeBL.setTempFeeNo( (String) cardTempFeeNoMap.
                                        get(tLJTempFeeBL.getOtherNo()));
              sameCardFlag = true;
              continue;
            }
          }

          if (!sameCardFlag) {
            tLimit = PubFun.getNoLimit(tLJTempFeeBL.getManageCom()); //产生通知书号即暂交费号
            tNo = PubFun1.CreateMaxNo("GETNOTICENO", tLimit);
            tLJTempFeeBL.setTempFeeNo(tNo); //此时暂加费号存放新生成的暂加费号码
            cardTempFeeNoMap.put(tLJTempFeeBL.getOtherNo(), tNo);
          }

          saveNo[num][0] = tLJTempFeeBL.getOtherNo(); //第一位存放印刷号
          saveNo[num][1] = tNo; //第二位存放新生成的暂交费号
          num++; //后面循环用到;为了与对应的暂交费分类纪录的关联（印刷号相同则赋予新生成的暂交费号）
        }
        System.out.println("----------------heyq--------TempFeeType==" +
                           tLJTempFeeBL.getTempFeeType());
        if (tLJTempFeeBL.getTempFeeType().equals("6") &&
            this.mOperate.equals("INSERT")) { //如果是定额单交费，要产生暂交费号
//          sameCardFlag = false;
          //tLJTempFeeBL.setState(tLJTempFeeBL.getOtherNo()); //将保存在暂加费号处的单证编码重新放置在状态处
          tLJTempFeeBL.setOtherNoType("4"); //其它号码类型置印刷号
          System.out.println("tLJTempFeeBL:" + tLJTempFeeBL.encode());

//如果需要生成新的暂交费号，可以用以下这段程序
//          for (int k=0; k<cardTempFeeNoMap.size(); k++) {
//            if (cardTempFeeNoMap.get(tLJTempFeeBL.getOtherNo()) != null) {
//              tLJTempFeeBL.setTempFeeNo((String)cardTempFeeNoMap.get(tLJTempFeeBL.getOtherNo()));
//              sameCardFlag = true;
//              continue;
//            }
//          }
//
//          if (!sameCardFlag) {
//            tLimit=PubFun.getNoLimit(tLJTempFeeBL.getManageCom());//产生通知书号即暂交费号
//            tNo=PubFun1.CreateMaxNo("GETNOTICENO",tLimit);
//            tLJTempFeeBL.setTempFeeNo(tNo); //此时暂加费号存放新生成的暂加费号码
//            cardTempFeeNoMap.put(tLJTempFeeBL.getOtherNo(), tNo);
//          }

          saveNo[num][0] = tLJTempFeeBL.getOtherNo(); //第一位存放暂交费号
          saveNo[num][1] = tLJTempFeeBL.getTempFeeNo(); ; //第二位存放暂交费号
          num++; //后面循环用到;为了与对应的暂交费分类纪录的关联（印刷号相同则赋予新生成的暂交费号）
        }

        //* ↓ *** liuhao *** 2005-05-17 *** add *****
         //记录预打保单的打印号码
         if (tLJTempFeeBL.getTempFeeType().equals("a")) {
           tEngageTempFeeNo = tLJTempFeeBL.getTempFeeNo() + ",";
           mEngagePrtNo = tLJTempFeeBL.getOtherNo() + ",";
         }
        //* ↑ *** liuhao *** 2005-05-17 *** add *****

         mLJTempFeeSetNew.add(tLJTempFeeBL);
        tReturn = true;
      }

      //考虑到页面同时录入主附险，假设主险纪录已经存在，而附险没有录，那么必须将mLJTempFeeSetNew中保留的
      //附险纪录删除掉，因为与该暂交费号关联的所有暂交费分类纪录已经被删除.
      LJTempFeeSet tempLJTempFeeSet = new LJTempFeeSet();
      if (this.mOperate.equals("INSERT")) {
        for (int n = 1; n <= mLJTempFeeSetNew.size(); n++) {
          for (int m = 0; m < NoIndex; m++) {
            System.out.println("暂交费号" +
                               mLJTempFeeSetNew.get(n).getTempFeeNo());
            System.out.println("重复的号" + existTempFeeNo[m]);

            if (existTempFeeNo[m].equals(mLJTempFeeSetNew.get(n).
                                         getTempFeeNo())) {
              //如果重复的暂交费号码与集合中的纪录的号码相同，删掉该纪录
              mLJTempFeeSetNew.remove(mLJTempFeeSetNew.get(n));
              n = n - 1;
              break;
            }
          }
        }
      }

//2-处理暂交费分类表，记录集，循环处理
      VData tempVData = new VData();
      LJTempFeeClassBL tLJTempFeeClassBL;
      LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();
      iMax = mLJTempFeeClassSet.size();
      boolean existTempFeeClassFlag = false;

      for (i = 1; i <= iMax; i++) {
        existTempFeeClassFlag = false; //初始化为假
        tLJTempFeeClassBL = new LJTempFeeClassBL();
        tLJTempFeeClassBL.setSchema(mLJTempFeeClassSet.get(i).getSchema());

        //判断暂交费分类纪录是否也是重复的纪录，从数组中查找
        if (NoIndex > 0 && this.mOperate.equals("INSERT")) { //如果暂交费纪录有重复
          for (int n = 0; n < NoIndex; n++) {
            if (tLJTempFeeClassBL.getTempFeeNo().equals(
                existTempFeeNo[n])) {
              existTempFeeClassFlag = true; //找到重复的暂交费分类纪录
            }
          }
          if (existTempFeeClassFlag == true) {
            continue;
          }
        }

        tLJTempFeeClassBL.setSerialNo(serNo);
        if (this.mOperate.equals("INSERT")) {
          tLJTempFeeClassBL.setMakeDate(CurrentDate); //入机时间
          tLJTempFeeClassBL.setMakeTime(CurrentTime); //入机时间
        }

        //首期交费时，管理机构和交费机构相同
        if (tempFeeFlag.equals("1")) {
          tLJTempFeeClassBL.setPolicyCom(tLJTempFeeClassBL.
                                         getManageCom());
        }

        tLJTempFeeClassBL.setModifyDate(CurrentDate);
        tLJTempFeeClassBL.setModifyTime(CurrentTime);
        tLJTempFeeClassBL.setConfFlag("0"); //核销标志置0
        //* ↓ *** liuhao *** 2005-05-17 *** add *****
         //判断如果是预打保单交费帐单 设置核销标志为1
         if (tEngageTempFeeNo.indexOf(tLJTempFeeClassBL.getTempFeeNo()) != -1) {
           tLJTempFeeClassBL.setConfFlag("1"); //核销标志置1
         }
        //* ↑ *** liuhao *** 2005-05-17 *** add *****

         //如果号码类型不是交费收据号31，那么循环找到数组中对应的保单号，
         //(因为对于要产生暂交费号码的类型，其暂交费分类纪录的暂交费号存放的是保单号或印刷号)
         //所以和前面保存的保单号或者印刷号对比，如果匹配，将新生成的暂交费号赋予暂交费分类纪录的暂交费号
         if (! (tLJTempFeeClassBL.getTempFeeNo().length() == 20 &&
                CodeJudge.judgeCodeType(tLJTempFeeClassBL.getTempFeeNo(),
                                        "31")) &&
             this.mOperate.equals("INSERT")) {
           for (int n = 0; n < num; n++) { //循环处理：如果是相同的保单号
             if (tLJTempFeeClassBL.getTempFeeNo().equals(saveNo[n][0])) {
               tLJTempFeeClassBL.setTempFeeNo(saveNo[n][1]);
               break;
             }
           }
         }

//        if (tLJTempFeeClassBL.getPayMode().equals("2") || tLJTempFeeClassBL.getPayMode().equals("3")) {
//            tLJTempFeeClassBL.setEnterAccDate("");
//
//            //处理主表的到帐日期
//            for (int j=0; j<mLJTempFeeSetNew.size(); j++) {
//                if (mLJTempFeeSetNew.get(j+1).getSchema().getTempFeeNo().equals(tLJTempFeeClassBL.getTempFeeNo())) {
//                    mLJTempFeeSetNew.get(j+1).setEnterAccDate("");
//                    mLJTempFeeSetNew.get(j+1).setConfMakeDate("");
//                }
//            }
//        }
//        else
//        {
        //tLJTempFeeClassBL.setConfMakeDate(tLJTempFeeClassBL.getEnterAccDate()); //后续添加：将到帐日期赋给财务确认操作日期
        //Modify by Minim，判断到帐日期，不为空则设置操作日期为当天
        if (tLJTempFeeClassBL.getEnterAccDate() != null &&
            !tLJTempFeeClassBL.getEnterAccDate().equals("")) {
          tLJTempFeeClassBL.setConfMakeDate(PubFun.getCurrentDate());
          tLJTempFeeClassBL.setConfMakeTime(PubFun.getCurrentTime());
        }
//        }
        /*
         LJTempFeeClassSchema tLJTempFeeClassSchema=new LJTempFeeClassSchema();
         LJTempFeeClassSchema delLJTempFeeClassSchema=new LJTempFeeClassSchema();
         if(newFlag && this.mOperate.equals("INSERT"))//如果是新单交费，查询暂加费分类表中是否已经存在相同暂加费分类纪录
                {
         tLJTempFeeClassSchema=queryLJTempFeeClass(tLJTempFeeClassBL.getSchema());
                  if(tLJTempFeeClassSchema!=null)//如果有，将存在的数据的金额添加
                  {
         tLJTempFeeClassBL.setPayMoney(tLJTempFeeClassBL.getPayMoney()+tLJTempFeeClassSchema.getPayMoney());
         delLJTempFeeClassSchema.setTempFeeNo(tLJTempFeeClassBL.getTempFeeNo());
         delLJTempFeeClassSchema.setPayMode(tLJTempFeeClassBL.getPayMode());
                    mLJTempFeeClassSetDel.add(delLJTempFeeClassSchema);
                  }
                }
         */
        if (this.mOperate.equals("UPDATE")) {
          //查询上次暂收是否为内部转账，如果是，取给付通知书号
          LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
          tLJTempFeeClassDB.setTempFeeNo(tLJTempFeeClassBL.
                                         getTempFeeNo());
          tLJTempFeeClassDB.setPayMode("5");
          if (tLJTempFeeClassDB.getInfo()) {
            String tChequeNo = tLJTempFeeClassDB.getChequeNo();
            VData ttData = new VData();
            LJFIGetDB tLJFIGetDB = new LJFIGetDB();
            tLJFIGetDB.setActuGetNo(tChequeNo);
            tLJFIGetDB.setPayMode("5");
            LJFIGetSet tLJFIGetSet = new LJFIGetSet();
            tLJFIGetSet = tLJFIGetDB.query();
            LJAGetDB tLJAGetDB = new LJAGetDB();
            tLJAGetDB.setActuGetNo(tChequeNo);
            if (tLJAGetDB.getInfo()) {
              LJAGetSchema tLJAGetSchema = new LJAGetSchema();
              tLJAGetSchema.setSchema(tLJAGetDB.getSchema());
              tLJAGetSchema.setConfDate("");
              tLJAGetSchema.setEnterAccDate("");
              String tFlag = tLJAGetSchema.getOtherNoType();
              SchemaSet tSchemaSet = new SchemaSet();
              if (tFlag.equals("0") || tFlag.equals("1") ||
                  tFlag.equals("2")) {
                LJAGetDrawSet tLJAGetDrawSet = new
                    LJAGetDrawSet();
                LJAGetDrawDB tLJAGetDrawDB = new LJAGetDrawDB();
                tLJAGetDrawDB.setActuGetNo(tLJAGetSchema.
                                           getActuGetNo());
                tLJAGetDrawSet = tLJAGetDrawDB.query();
                for (int j = 1; j <= tLJAGetDrawSet.size(); j++) {
                  LJAGetDrawSchema tLJAGetDrawSchema =
                      tLJAGetDrawSet.get(j);
                  tLJAGetDrawSchema.setConfDate("");
                  tLJAGetDrawSchema.setEnterAccDate("");
                  tLJAGetDrawSet.set(j, tLJAGetDrawSchema);
                }
                tSchemaSet = tLJAGetDrawSet;

              }

              if (tFlag.equals("3")) {
                LJAGetEndorseSet tLJAGetEndorseSet = new
                    LJAGetEndorseSet();
                LJAGetEndorseDB tLJAGetEndorseDB = new
                    LJAGetEndorseDB();
                tLJAGetEndorseDB.setActuGetNo(tLJAGetSchema.
                                              getActuGetNo());
                tLJAGetEndorseSet = tLJAGetEndorseDB.query();
                for (int j = 1; j <= tLJAGetEndorseSet.size();
                     j++) {
                  LJAGetEndorseSchema tLJAGetEndorseSchema =
                      tLJAGetEndorseSet.get(j);
                  tLJAGetEndorseSchema.setGetConfirmDate("");
                  tLJAGetEndorseSchema.setEnterAccDate("");
                  tLJAGetEndorseSet.set(j,
                                        tLJAGetEndorseSchema);
                }
                tSchemaSet = tLJAGetEndorseSet;
              }

              if (tFlag.equals("4")) {
                LJAGetTempFeeSet tLJAGetTempFeeSet = new
                    LJAGetTempFeeSet();
                LJAGetTempFeeDB tLJAGetTempFeeDB = new
                    LJAGetTempFeeDB();
                tLJAGetTempFeeDB.setActuGetNo(tLJAGetSchema.
                                              getActuGetNo());
                tLJAGetTempFeeSet = tLJAGetTempFeeDB.query();
                for (int j = 1; j <= tLJAGetTempFeeSet.size();
                     j++) {
                  LJAGetTempFeeSchema tLJAGetTempFeeSchema =
                      tLJAGetTempFeeSet.get(j);
                  tLJAGetTempFeeSchema.setConfDate("");
                  tLJAGetTempFeeSchema.setEnterAccDate("");
                  tLJAGetTempFeeSet.set(j,
                                        tLJAGetTempFeeSchema);
                }
                tSchemaSet = tLJAGetTempFeeSet;

              }

              if (tFlag.equals("5")) {
                LJAGetClaimSet tLJAGetClaimSet = new
                    LJAGetClaimSet();
                LJAGetClaimDB tLJAGetClaimDB = new
                    LJAGetClaimDB();
                tLJAGetClaimDB.setActuGetNo(tLJAGetSchema.
                                            getActuGetNo());
                tLJAGetClaimSet = tLJAGetClaimDB.query();
                for (int j = 1; j <= tLJAGetClaimSet.size(); j++) {
                  LJAGetClaimSchema tLJAGetClaimSchema =
                      tLJAGetClaimSet.get(j);
                  tLJAGetClaimSchema.setConfDate("");
                  tLJAGetClaimSchema.setEnterAccDate("");
                  tLJAGetClaimSet.set(j, tLJAGetClaimSchema);
                }
                tSchemaSet = tLJAGetClaimSet;
              }

              if (tFlag.equals("6")) {
                LJAGetOtherSet tLJAGetOtherSet = new
                    LJAGetOtherSet();
                LJAGetOtherDB tLJAGetOtherDB = new
                    LJAGetOtherDB();
                tLJAGetOtherDB.setActuGetNo(tLJAGetSchema.
                                            getActuGetNo());
                tLJAGetOtherSet = tLJAGetOtherDB.query();
                for (int j = 1; j <= tLJAGetOtherSet.size(); j++) {
                  LJAGetOtherSchema tLJAGetOtherSchema =
                      tLJAGetOtherSet.get(j);
                  tLJAGetOtherSchema.setConfDate("");
                  tLJAGetOtherSchema.setEnterAccDate("");
                  tLJAGetOtherSet.set(j, tLJAGetOtherSchema);
                }
                tSchemaSet = tLJAGetOtherSet;
              }

              if (tFlag.equals("7")) {
                LJABonusGetSet tLJABonusGetSet = new
                    LJABonusGetSet();
                LJABonusGetDB tLJABonusGetDB = new
                    LJABonusGetDB();
                tLJABonusGetDB.setActuGetNo(tLJAGetSchema.
                                            getActuGetNo());
                tLJABonusGetSet = tLJABonusGetDB.query();
                for (int j = 1; j <= tLJABonusGetSet.size(); j++) {
                  LJABonusGetSchema tLJABonusGetSchema =
                      tLJABonusGetSet.get(j);
                  tLJABonusGetSchema.setConfDate("");
                  tLJABonusGetSchema.setEnterAccDate("");
                  tLJABonusGetSet.set(j, tLJABonusGetSchema);
                }
                tSchemaSet = tLJABonusGetSet;
              }
              ttData.add(tLJFIGetSet);
              ttData.add(tLJAGetSchema);
              ttData.add(tFlag);
              ttData.add(tSchemaSet);
              mLastFinData.add(ttData);
            }
          }

        }

        if (tLJTempFeeClassBL.getPayMode().equals("5")) { //如果暂交费分类纪录的交费方式是5-内部转账
          tLJFIGetSchema = new LJFIGetSchema();
          tLJFIGetSchema.setActuGetNo(tLJTempFeeClassBL.getChequeNo()); //将票据号赋予实付号
          tLJFIGetSchema.setPayMode(tLJTempFeeClassBL.getPayMode()); //交费方式
          tLJFIGetSchema.setGetMoney(tLJTempFeeClassBL.getPayMoney()); //交费金额
          tLJFIGetSchema.setEnterAccDate(tLJTempFeeClassBL.
                                         getEnterAccDate()); //到帐日期
          tLJFIGetSchema.setConfDate(CurrentDate); //确认日期为当天
          tLJFIGetSchema.setSerialNo(tLJTempFeeClassBL.getSerialNo());
          tempVData = new VData();
          tempVData = DoFinFeePay(tLJFIGetSchema); //做财务付费的流程
          if (tempVData == null) {
            return false;
          }
          FinFeeVData.add(tempVData);
        }
        mLJTempFeeClassSetNew.add(tLJTempFeeClassBL);
        tReturn = true;
      }
      
      for (int s = 1; s <= mLJTempFeeSetNew.size(); s++) {
          String otherNo=mLJTempFeeSetNew.get(s).getOtherNo();     
              if (otherNo == null || otherNo.equals("")) {
  	   	 	      CError tError = new CError();
  	   	 	      tError.moduleName = "TempFeeBL";
  	   	 	      tError.functionName = "dealData";
  	   	 	      tError.errorMessage = "数据发生异常，请您重新进行收费操作！";
  	   		      this.mErrors.addOneError(tError);
  	   		      System.out.println(tError.errorMessage);
  	   		      return false;
               }   
           }  
      }              
    return tReturn;
  }

  /**
   * 处理财务处理结束后的操作。
   * 本操作在支票确认和银行转帐回盘时都会调用，
   * 请注意实现AfterTempFeeBL接口时要保障实现后的类在这两个地方都能使用
   * @return boolean
   */
  private boolean afterSubmit()
  {
      System.out.println("暂收处理后的相关业务处理afterSubmit");
      String tempFeeType = null;
      MMap map = new MMap();

      //注意，一条LJSPay可能对应多个LJTempFee纪录，所以进行处理时要注意相关性
      for(int i = 1; i <= mLJTempFeeSetNew.size(); i++)
      {
          try
          {
              LJTempFeeSchema tLJTempFeeSchema
                  = mLJTempFeeSetNew.get(i).getSchema();
              //按TempFeeType编码调用不同的类
              tempFeeType = tLJTempFeeSchema.getTempFeeType();

              String className = "com.sinosoft.lis.finfee.AfterTempFee" +
                                 tempFeeType + "BL";
              Class confirmClass = Class.forName(className);
              AfterTempFeeBL tAfterTempFeeBL = (AfterTempFeeBL)
                                          confirmClass.newInstance();
              VData data = new VData();
              data.add(tGI);
              data.add(tLJTempFeeSchema);

              MMap tMMap = tAfterTempFeeBL.getSubmitMMap(data, "");
              if(tMMap == null)
              {
                  mErrors.copyAllErrors(tAfterTempFeeBL.mErrors);
                  return false;
              }
              map.add(tMMap);
          }
          catch(ClassNotFoundException ex)
          {
              System.out.println(tempFeeType + "没有暂收后处理类!");
          }
          catch(NullPointerException ex)
          {
              ex.printStackTrace();
          }
          catch(Exception ex)
          {
              ex.printStackTrace();
              return false;
          }
      }

      if(map == null || map.size() == 0)
      {
          return true;
      }

      VData data = new VData();
      data.add(map);
      PubSubmit p = new PubSubmit();
      if(!p.submitData(data, ""))
      {
          CError tError = new CError();
          tError.moduleName = "TempFeeBL";
          tError.functionName = "afterSubmit";
          tError.errorMessage = "收费成功，但后续处理失败";
          mErrors.addOneError(tError);
          System.out.println(tError.errorMessage);
          return false;
      }

      return true;
  }

  /**
   * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   *
   * @param mInputData VData
   * @return boolean
   */
  private boolean getInputData(VData mInputData) {
    // 暂交费表
      System.out.println("mLJTempFeeClassSet================");
    mLJTempFeeSet.set( (LJTempFeeSet) mInputData.getObjectByObjectName(
        "LJTempFeeSet", 0));
    System.out.println("mLJTempFeeSet" + mLJTempFeeSet.get(1).getEnterAccDate());

    // 暂交费分类表
    mLJTempFeeClassSet.set( (LJTempFeeClassSet) mInputData.
                           getObjectByObjectName("LJTempFeeClassSet", 0));
    System.out.println("mLJTempFeeClassSet" +
                       mLJTempFeeClassSet.get(1).getEnterAccDate());
    // 公用变量
    tGI = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);

    if (mLJTempFeeSet == null || mLJTempFeeClassSet == null || tGI == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "TempFeeBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "没有得到足够的数据，请您确认!";
      this.mErrors.addOneError(tError);
      return false;
    }
    /*
   LJTempFeeSchema mNewLJTempFeeSchema=new LJTempFeeSchema();
   LJTempFeeSchema mOldLJTempFeeSchema=new LJTempFeeSchema();
   String tContno;
    for(int i=1;i<mLJTempFeeSet.size();i++){
     if(StrTool.cTrim(mLJTempFeeSet.get(i).getTempFeeType()).equals("2")){
        for(int j=i+1;j<=mLJTempFeeSet.size();j++){
            if(StrTool.cTrim(mLJTempFeeSet.get(i).getTempFeeType()).equals(mLJTempFeeSet.get(j).getTempFeeType())
               &&StrTool.cTrim(mLJTempFeeSet.get(i).getRiskCode()).equals(mLJTempFeeSet.get(j).getRiskCode())
               &&StrTool.cTrim(mLJTempFeeSet.get(i).getTempFeeNo()).equals(mLJTempFeeSet.get(j).getTempFeeNo())){
                mNewLJTempFeeSchema=mLJTempFeeSet.get(i);
                mOldLJTempFeeSchema=mLJTempFeeSet.get(j);
                mLJTempFeeSet.remove(mNewLJTempFeeSchema);
                mLJTempFeeSet.remove(mOldLJTempFeeSchema);
                double Paymoney= mNewLJTempFeeSchema.getPayMoney()+mOldLJTempFeeSchema.getPayMoney();
               System.out.println("Paymoney"+Paymoney);
               mNewLJTempFeeSchema.setPayMoney(Paymoney);
               mNewLJTempFeeSchema.setOtherNoType("2");
               tContno=(new ExeSQL()).getOneValue("select distinct contno from lcpol where polno='"+mNewLJTempFeeSchema.getOtherNo()+"'");
               mNewLJTempFeeSchema.setOtherNo(tContno);
               mLJTempFeeSet.add(mNewLJTempFeeSchema);
               }
            }
        }
      }
     */
    return true;
  }

  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData() {
    String arrPrtNo[] = null;
    String strAllPrtNo = "";
    mInputData = new VData();
    MMap tempMap = new MMap();

    try {
      for (int n = 1; n <= mLJTempFeeSetNew.size(); n++) {
        LJTempFeeSchema tLJTempFeeSchema = mLJTempFeeSetNew.get(n);
        String tempFeeType = tLJTempFeeSchema.getTempFeeType();
        //健康服务管理代理人取值
        if(tempFeeType.equals("30")){
            LDCodeDB tLDCodeDB = new LDCodeDB();
            String agSql = "select * from ldcode where codetype='FeeAgent'";
            LDCodeSet tLDCodeSet = tLDCodeDB.executeQuery(agSql);
            if (tLDCodeSet.size() == 0) {
                CError tError = new CError();
                tError.moduleName = "TempFeeBL";
                tError.functionName = "prepareOutputData";
                tError.errorMessage = "没有找到代理人编码配置";
                this.mErrors.addOneError(tError);
                return false;
              }
            tLJTempFeeSchema.setAgentCode(tLDCodeSet.get(1).getCode());
        }
//      保全续期没有代理人
        if ( (!tempFeeType.equals("4")) && (!tempFeeType.equals("7"))
                && !tempFeeType.equals("2") && !tempFeeType.equals("6")) {
          String AgentCode = tLJTempFeeSchema.getAgentCode();
          LAAgentDB tLAAgentDB = new LAAgentDB();
          tLAAgentDB.setAgentCode(AgentCode);
          if (tLAAgentDB.getInfo() == false) {
            CError tError = new CError();
            tError.moduleName = "TempFeeBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "没有找到代理人编码：" + AgentCode +
                "对应的代理人组别";
            this.mErrors.addOneError(tError);
            return false;
          }
          tLJTempFeeSchema.setAgentGroup(tLAAgentDB.getBranchCode());
        }
        mLJTempFeeSetNew.set(n, tLJTempFeeSchema);
      }

//1-处理暂交费表，记录集，循环处理
      mInputData.add(mLJTempFeeSetNew); //位置是0
//2-处理暂交费分类表，记录集，循环处理
      mInputData.add(mLJTempFeeClassSetNew); //位置是1
//3-添加要删除的暂交费分类表
      mInputData.add(mLJTempFeeClassSetDel); //位置是2
//4-添加处理财务付费的数据
      mInputData.add(FinFeeVData); //位置是3
      mInputData.add(tGI); //位置是4
      mInputData.add(mLastFinData); //位置是5

      // * ↓ *** add *** liuhao *** 2005-05-17 *****
      //如果交费中有预打保单交费，使保单生效
      if (mEngagePrtNo.length() > 0) {
        LCGrpContSet tSignLCContSet = new LCGrpContSet();
        LCGrpPolSet tSignLCGrpPolSet = new LCGrpPolSet();
        //实收财务、保单生效处理
        LCGrpContSchema signSchema = null;
        LCGrpPolSchema tLCGrpPolSchema = null;
        arrPrtNo = mEngagePrtNo.split(",");
        for (int i = 0; i < arrPrtNo.length; i++) {
          LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
          tLCGrpContSchema.setGrpContNo(arrPrtNo[i]);
          LCGrpContDB tLCGrpContDB = new LCGrpContDB();
          tLCGrpContDB.setSchema(tLCGrpContSchema);
          LCGrpContSet rsLCGrpContSet = tLCGrpContDB.query();
          if (tLCGrpContDB.mErrors.needDealError() ||
              rsLCGrpContSet == null) {
            continue;
          }

          //取出查询结果
          signSchema = rsLCGrpContSet.get(1);
          //更改保单状态使其生效
          signSchema.setAppFlag("1");
          //数据缓存
          tSignLCContSet.add(signSchema);

          LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
          tLCGrpPolDB.setGrpContNo(arrPrtNo[i]);
          LCGrpPolSet tLCGrpPolSet = tLCGrpPolDB.query();
          if (tLCGrpPolDB.mErrors.needDealError() ||
              rsLCGrpContSet == null) {
            continue;
          }
          //取出查询结果
          tLCGrpPolSchema = tLCGrpPolSet.get(1);
          //更改保单状态使其生效
          tLCGrpPolSchema.setAppFlag("1");
          //数据缓存
          tSignLCGrpPolSet.add(tLCGrpPolSchema);

          //进行预打保单财务实收处理
          dealEngageAPay(signSchema);
        }

        //如果有预打保单交费，数据处理正确
        if (tSignLCContSet.size() > 0) {
          tempMap.put(tSignLCContSet, "UPDATE");
          mInputData.add(tSignLCContSet);
          mInputData.add(tSignLCGrpPolSet);
        }

        //添加实收集体交费信息
        mInputData.add(mLJAPayGrpSet); //位置是7
        //添加实收个人交费信息
        mInputData.add(mLJAPayPersonSet); //位置是8
        //添加实收总表信息
        mInputData.add(mLJAPaySet); //位置是9
      }
      // * ↑ *** add *** liuhao *** 2005-05-17 *****

      System.out.println("prepareOutputData:");
    } catch (Exception ex) {
      // @@错误处理
      ex.printStackTrace();
      CError tError = new CError();
      tError.moduleName = "TempFeeBL";
      tError.functionName = "prepareData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      System.out.println("在准备往后层处理所需要的数据时出错。");
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /*
   *  查询暂交费表
   *  返回布尔值
   */
  private boolean queryTempFee(LJTempFeeSchema pLJTempFeeSchema) {
    String TempFeeNo = pLJTempFeeSchema.getTempFeeNo();
    String RiskCode = pLJTempFeeSchema.getRiskCode();
    if (TempFeeNo == null) {
      CError tError = new CError();
      tError.moduleName = "TempFeeBL";
      tError.functionName = "queryTempFee";
      tError.errorMessage = "暂交费号不能为空!";
      this.mErrors.addOneError(tError);
      return false;
    }
    //查询凡是同一个收据号的纪录
    String sqlStr = "select * from LJTempFee where TempFeeNo='" + TempFeeNo +
        "'";
    //sqlStr=sqlStr+" and RiskCode='"+RiskCode+"'";
    System.out.println("查询暂交费表:" + sqlStr);
    LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
    LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
    LJTempFeeDB tLJTempFeeDB = tLJTempFeeSchema.getDB();
    tLJTempFeeSet = tLJTempFeeDB.executeQuery(sqlStr);
    if (tLJTempFeeDB.mErrors.needDealError() == true) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJTempFeeDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "TempFeeBL";
      tError.functionName = "queryLJTempFee";
      tError.errorMessage = "暂交费表查询失败!";
      this.mErrors.addOneError(tError);
      tLJTempFeeSet.clear();
      return false;
    }
    if (tLJTempFeeSet.size() > 0) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "TempFeeBL";
      tError.functionName = "queryLJTempFee";
      tError.errorMessage = "暂交费号为：" + TempFeeNo + " 的纪录已经存在！";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  /*
   *  查询暂交费分类表
   *  返回布尔值
   */
  private LJTempFeeClassSchema queryLJTempFeeClass(LJTempFeeClassSchema
                                                   nLJTempFeeClassSchema) {
    String strSql = "select * from LJTempFeeClass where TempFeeNo='" +
        nLJTempFeeClassSchema.getTempFeeNo() + "'";
    strSql = strSql + " and PayMode='" + nLJTempFeeClassSchema.getPayMode() +
        "'";
    System.out.println("查询暂交费分类表:" + strSql);
    LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
    LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
    LJTempFeeClassDB tLJTempFeeClassDB = tLJTempFeeClassSchema.getDB();
    tLJTempFeeClassSet = tLJTempFeeClassDB.executeQuery(strSql);
    if (tLJTempFeeClassDB.mErrors.needDealError() == true) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "TempFeeBL";
      tError.functionName = "queryLJTempFeeClass";
      tError.errorMessage = "暂交费分类表查询失败!";
      this.mErrors.addOneError(tError);
      tLJTempFeeClassSet.clear();
      return null;
    }
    if (tLJTempFeeClassSet.size() == 0) {
      return null;
    }
    LJTempFeeClassSchema pLJTempFeeClassSchema = new LJTempFeeClassSchema();
    pLJTempFeeClassSchema = tLJTempFeeClassSet.get(1);
    return pLJTempFeeClassSchema;
  }

  /**
   * 处理财务付费流程
   *
   * @param pLJFIGetSchema 实付号码(票据号码)
   * @return com.sinosoft.utility.VData
   */
  private VData DoFinFeePay(LJFIGetSchema pLJFIGetSchema) {
    if (pLJFIGetSchema == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "TempFeeBL";
      tError.functionName = "DoFinFeePay";
      tError.errorMessage = "传入参数不能为空!";
      this.mErrors.addOneError(tError);
      return null;
    }
    if (pLJFIGetSchema.getActuGetNo() == null || pLJFIGetSchema.getPayMode() == null ||
        pLJFIGetSchema.getEnterAccDate() == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "TempFeeBL";
      tError.functionName = "DoFinFeePay";
      tError.errorMessage = "实付号码（票据号码）,交费方式,到帐日期不能为空!";
      this.mErrors.addOneError(tError);
      return null;
    }
    VData tVData = new VData();
    //1-查询实付总表
    LJAGetSet tLJAGetSet = new LJAGetSet();
    LJAGetSchema tLJAGetSchema = new LJAGetSchema();
    tLJAGetSchema.setActuGetNo(pLJFIGetSchema.getActuGetNo());
    LJAGetDB tLJAGetDB = tLJAGetSchema.getDB();
    tLJAGetSet = tLJAGetDB.query();
    if (tLJAGetDB.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJAGetDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "TempFeeBL";
      tError.functionName = "DoFinFeePay";
      tError.errorMessage = "数据库查询失败!";
      this.mErrors.addOneError(tError);
      return null;
    }
    if (tLJAGetSet.size() == 0) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "TempFeeBL";
      tError.functionName = "DoFinFeePay";
      tError.errorMessage = "没有查询到票据号码对应的实付总表!票据号码:" +
          pLJFIGetSchema.getActuGetNo();
      this.mErrors.addOneError(tError);
      return null;
    }
    tLJAGetSchema = tLJAGetSet.get(1);
    //准备实付总表的更新纪录
    if (tLJAGetSchema.getSumGetMoney() != pLJFIGetSchema.getGetMoney()) {
      CError tError = new CError();
      tError.moduleName = "TempFeeBL";
      tError.functionName = "DoFinFeePay";
      tError.errorMessage = "作内部转账的退费金额与暂收金额不符";
      this.mErrors.addOneError(tError);
      return null;
    }
    if (!tLJAGetSchema.getPayMode().equals("5") &&
        tLJAGetSchema.getConfDate() != null) {
      CError tError = new CError();
      tError.moduleName = "TempFeeBL";
      tError.functionName = "DoFinFeePay";
      tError.errorMessage = "该退费已经给付确认";
      this.mErrors.addOneError(tError);
      return null;
    }
    //tLJAGetSchema.setSumGetMoney(tLJAGetSchema.getSumGetMoney()+pLJFIGetSchema.getGetMoney());
    tLJAGetSchema.setEnterAccDate(pLJFIGetSchema.getEnterAccDate());
    tLJAGetSchema.setConfDate(pLJFIGetSchema.getConfDate());
    tLJAGetSchema.setManageCom(tGI.ManageCom);
    tLJAGetSchema.setOperator(tGI.Operator);
    tLJAGetSchema.setModifyDate(CurrentDate);
    tLJAGetSchema.setModifyTime(CurrentTime);
    tLJAGetSchema.setSerialNo(pLJFIGetSchema.getSerialNo());
    tLJAGetSchema.setPayMode(pLJFIGetSchema.getPayMode());

    //2-查询财务给付表
    LJFIGetSet tLJFIGetSet = new LJFIGetSet();
    LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();
    tLJFIGetSchema.setActuGetNo(pLJFIGetSchema.getActuGetNo()); //注意是pLJFIGetSchema
    LJFIGetDB tLJFIGetDB = tLJFIGetSchema.getDB();
    tLJFIGetSet = tLJFIGetDB.query();
    if (tLJFIGetDB.mErrors.needDealError()) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJFIGetDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "TempFeeBL";
      tError.functionName = "DoFinFeePay";
      tError.errorMessage = "数据库查询失败!";
      this.mErrors.addOneError(tError);
      return null;
    }
    if (tLJFIGetSet.size() > 0 && this.mOperate.equals("INSERT")) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "TempFeeBL";
      tError.functionName = "DoFinFeePay";
      tError.errorMessage = "已经存在实付号码为:" + pLJFIGetSchema.getActuGetNo() +
          ",交费方式为:" + pLJFIGetSchema.getPayMode() +
          "的财务给付纪录!";
      this.mErrors.addOneError(tError);
      return null;
    }
    //准备财务给付表纪录数据
    tLJFIGetSchema.setActuGetNo(pLJFIGetSchema.getActuGetNo());
    tLJFIGetSchema.setPayMode(pLJFIGetSchema.getPayMode());
    tLJFIGetSchema.setOtherNo(tLJAGetSchema.getOtherNo());
    tLJFIGetSchema.setOtherNoType(tLJAGetSchema.getOtherNoType());
    tLJFIGetSchema.setEnterAccDate(pLJFIGetSchema.getEnterAccDate());
    tLJFIGetSchema.setConfDate(CurrentDate);
    tLJFIGetSchema.setSaleChnl(tLJAGetSchema.getSaleChnl());
    tLJFIGetSchema.setManageCom(tGI.ManageCom);
    tLJFIGetSchema.setAgentCode(tLJAGetSchema.getAgentCode());
    tLJFIGetSchema.setAgentGroup(tLJAGetSchema.getAgentGroup());
    tLJFIGetSchema.setAgentCom(tLJAGetSchema.getAgentCom());
    tLJFIGetSchema.setSerialNo(pLJFIGetSchema.getSerialNo());
    tLJFIGetSchema.setMakeDate(CurrentDate);
    tLJFIGetSchema.setMakeTime(CurrentTime);
    tLJFIGetSchema.setModifyDate(CurrentDate);
    tLJFIGetSchema.setModifyTime(CurrentTime);
    tLJFIGetSchema.setOperator(tGI.Operator);
    tLJFIGetSchema.setGetMoney(pLJFIGetSchema.getGetMoney());

    //3-准备子表更新数据
    String OtherNoType = tLJAGetSchema.getOtherNoType();
    String flag = "";
    LJABonusGetSet mLJABonusGetSet = new LJABonusGetSet();
    LJAGetDrawSet mLJAGetDrawSet = new LJAGetDrawSet();
    LJAGetEndorseSet mLJAGetEndorseSet = new LJAGetEndorseSet();
    LJAGetTempFeeSet mLJAGetTempFeeSet = new LJAGetTempFeeSet();
    LJAGetClaimSet mLJAGetClaimSet = new LJAGetClaimSet();
    LJAGetOtherSet mLJAGetOtherSet = new LJAGetOtherSet();

    //更新 给付表(生存领取_实付)   LJAGetDraw
    if (OtherNoType.equals("1") || OtherNoType.equals("2") ||
        OtherNoType.equals("0")) {
      mLJAGetDrawSet = updateLJAGetDraw(tLJAGetSchema);
      if (mLJAGetDrawSet == null) {
        return null;
      } else {
        flag = "1";
      }
    }
    if (OtherNoType.equals("3")) { //更新 批改补退费表(实收/实付) LJAGetEndorse
      mLJAGetEndorseSet = updateLJAGetEndorse(tLJAGetSchema);
      if (mLJAGetEndorseSet == null) {
        return null;
      } else {
        flag = "3";
      }
    }

    if (OtherNoType.equals("4")) { //更新 暂交费退费实付表
      mLJAGetTempFeeSet = updateLJAGetTempFee(tLJAGetSchema);
      if (mLJAGetTempFeeSet == null) {
        return null;
      } else {
        flag = "4";
      }
    }

    if (OtherNoType.equals("5")) { //更新 赔付实付表             LJAGetClaim
      mLJAGetClaimSet = updateLJAGetClaim(tLJAGetSchema);
      if (mLJAGetClaimSet == null) {
        return null;
      } else {
        flag = "5";
      }
    }

    if (OtherNoType.equals("6") || OtherNoType.equals("8")) { //更新 其他退费实付表         LJAGetOther
      mLJAGetOtherSet = updateLJAGetOther(tLJAGetSchema);
      if (mLJAGetOtherSet == null) {
        return null;
      } else {
        flag = "6";
      }
    }

    if (OtherNoType.equals("7")) { //更新 红利给付实付表         LJABonusGet
      mLJABonusGetSet = updateLJABonusGet(tLJAGetSchema);
      if (mLJABonusGetSet == null) {
        return null;
      } else {
        flag = "7";
      }
    }

    tVData.add(tLJFIGetSchema); //1-财务给付表
    tVData.add(tLJAGetSchema); //2-实付总表，单项纪录
    if (flag.equals("1")) {
      tVData.add(mLJAGetDrawSet); //3-检验标志位
    }
    if (flag.equals("3")) {
      tVData.add(mLJAGetEndorseSet);
    }
    if (flag.equals("4")) {
      tVData.add(mLJAGetTempFeeSet);
    }
    if (flag.equals("5")) {
      tVData.add(mLJAGetClaimSet);
    }
    if (flag.equals("6")) {
      tVData.add(mLJAGetOtherSet);
    }
    if (flag.equals("7")) {
      tVData.add(mLJABonusGetSet);
    }
    tVData.add(flag); //4-加上标志位
    return tVData;
  }

  /**
   * 查询给付表(生存领取_实付)
   *
   * @param mLJAGetSchema LJAGetSchema
   * @return com.sinosoft.lis.vschema.LJAGetDrawSet
   */
  private LJAGetDrawSet updateLJAGetDraw(LJAGetSchema mLJAGetSchema) {
    if (mLJAGetSchema == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetDraw";
      tError.errorMessage = "传入参数不能为空！";
      this.mErrors.addOneError(tError);
      return null;
    }
    String sqlStr = "select * from LJAGetDraw where ActuGetNo='" +
        mLJAGetSchema.getActuGetNo() + "'";
    System.out.println("查询给付表(生存领取_实付):" + sqlStr);
    LJAGetDrawSet tLJAGetDrawSet = new LJAGetDrawSet();
    LJAGetDrawSchema tLJAGetDrawSchema = new LJAGetDrawSchema();
    LJAGetDrawDB tLJAGetDrawDB = tLJAGetDrawSchema.getDB();
    tLJAGetDrawSet = tLJAGetDrawDB.executeQuery(sqlStr);
    if (tLJAGetDrawDB.mErrors.needDealError() == true) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJAGetDrawDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "queryLJAGetDraw";
      tError.errorMessage = "给付表(生存领取_实付)查询失败!";
      this.mErrors.addOneError(tError);
      tLJAGetDrawSet.clear();
      return null;
    }
    LJAGetDrawSet newLJAGetDrawSet = new LJAGetDrawSet();
    for (int n = 1; n <= tLJAGetDrawSet.size(); n++) {
      tLJAGetDrawSchema = new LJAGetDrawSchema();
      tLJAGetDrawSchema = tLJAGetDrawSet.get(n);
      tLJAGetDrawSchema.setConfDate(mLJAGetSchema.getConfDate());
      tLJAGetDrawSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
      tLJAGetDrawSchema.setModifyDate(CurrentDate);
      tLJAGetDrawSchema.setModifyTime(CurrentTime);
      tLJAGetDrawSchema.setSerialNo(mLJAGetSchema.getSerialNo());
      newLJAGetDrawSet.add(tLJAGetDrawSchema);
    }
    if (newLJAGetDrawSet.size() == 0) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetDraw";
      tError.errorMessage = "没有可以更新的给付表(生存领取_实付)！";
      this.mErrors.addOneError(tError);
      return null;
    }
    return newLJAGetDrawSet;
  }

  /**
   * 查询批改补退费表(实收/实付)
   *
   * @param mLJAGetSchema LJAGetSchema
   * @return com.sinosoft.lis.vschema.LJAGetEndorseSet
   */
  private LJAGetEndorseSet updateLJAGetEndorse(LJAGetSchema mLJAGetSchema) {
    if (mLJAGetSchema == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetEndorse";
      tError.errorMessage = "传入参数不能为空！";
      this.mErrors.addOneError(tError);
      return null;
    }
    String sqlStr = "select * from LJAGetEndorse where ActuGetNo='" +
        mLJAGetSchema.getActuGetNo() + "'";
    System.out.println("查询给付表(生存领取_实付):" + sqlStr);
    LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
    LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
    LJAGetEndorseDB tLJAGetEndorseDB = tLJAGetEndorseSchema.getDB();
    tLJAGetEndorseSet = tLJAGetEndorseDB.executeQuery(sqlStr);
    if (tLJAGetEndorseDB.mErrors.needDealError() == true) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJAGetEndorseDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetEndorse";
      tError.errorMessage = "批改补退费表(实收/实付)查询失败!";
      this.mErrors.addOneError(tError);
      tLJAGetEndorseSet.clear();
      return null;
    }
    LJAGetEndorseSet newLJAGetEndorseSet = new LJAGetEndorseSet();
    for (int n = 1; n <= tLJAGetEndorseSet.size(); n++) {
      tLJAGetEndorseSchema = new LJAGetEndorseSchema();
      tLJAGetEndorseSchema = tLJAGetEndorseSet.get(n);
      tLJAGetEndorseSchema.setGetConfirmDate(mLJAGetSchema.getConfDate());
      tLJAGetEndorseSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
      tLJAGetEndorseSchema.setModifyDate(CurrentDate);
      tLJAGetEndorseSchema.setModifyTime(CurrentTime);
      tLJAGetEndorseSchema.setSerialNo(mLJAGetSchema.getSerialNo());
      newLJAGetEndorseSet.add(tLJAGetEndorseSchema);
    }
    if (newLJAGetEndorseSet.size() == 0) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetEndorse";
      tError.errorMessage = "没有可以更新的批改补退费表(实收/实付)！";
      this.mErrors.addOneError(tError);
      return null;
    }
    return newLJAGetEndorseSet;

  }

  /**
   * 查询其他退费实付表
   *
   * @param mLJAGetSchema LJAGetSchema
   * @return com.sinosoft.lis.vschema.LJAGetOtherSet
   */
  private LJAGetOtherSet updateLJAGetOther(LJAGetSchema mLJAGetSchema) {
    if (mLJAGetSchema == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetOther";
      tError.errorMessage = "传入参数不能为空！";
      this.mErrors.addOneError(tError);
      return null;
    }
    String sqlStr = "select * from LJAGetOther where ActuGetNo='" +
        mLJAGetSchema.getActuGetNo() + "'";
    System.out.println("其他退费实付表:" + sqlStr);
    LJAGetOtherSet tLJAGetOtherSet = new LJAGetOtherSet();
    LJAGetOtherSchema tLJAGetOtherSchema = new LJAGetOtherSchema();
    LJAGetOtherDB tLJAGetOtherDB = tLJAGetOtherSchema.getDB();
    tLJAGetOtherSet = tLJAGetOtherDB.executeQuery(sqlStr);
    if (tLJAGetOtherDB.mErrors.needDealError() == true) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJAGetOtherDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetOther";
      tError.errorMessage = "其他退费实付表查询失败!";
      this.mErrors.addOneError(tError);
      tLJAGetOtherSet.clear();
      return null;
    }
    LJAGetOtherSet newLJAGetOtherSet = new LJAGetOtherSet();
    for (int n = 1; n <= tLJAGetOtherSet.size(); n++) {
      tLJAGetOtherSchema = new LJAGetOtherSchema();
      tLJAGetOtherSchema = tLJAGetOtherSet.get(n);
      tLJAGetOtherSchema.setConfDate(mLJAGetSchema.getConfDate());
      tLJAGetOtherSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
      tLJAGetOtherSchema.setModifyDate(CurrentDate);
      tLJAGetOtherSchema.setModifyTime(CurrentTime);
      tLJAGetOtherSchema.setSerialNo(mLJAGetSchema.getSerialNo());
      newLJAGetOtherSet.add(tLJAGetOtherSchema);
    }
    if (newLJAGetOtherSet.size() == 0) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetOther";
      tError.errorMessage = "没有可以更新的其他退费实付表！";
      this.mErrors.addOneError(tError);
      return null;
    }
    return newLJAGetOtherSet;

  }

  /**
   * 赔付实付表
   *
   * @param mLJAGetSchema LJAGetSchema
   * @return com.sinosoft.lis.vschema.LJAGetClaimSet
   */
  private LJAGetClaimSet updateLJAGetClaim(LJAGetSchema mLJAGetSchema) {
    if (mLJAGetSchema == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetClaim";
      tError.errorMessage = "传入参数不能为空！";
      this.mErrors.addOneError(tError);
      return null;
    }
    String sqlStr = "select * from LJAGetClaim where ActuGetNo='" +
        mLJAGetSchema.getActuGetNo() + "'";
    System.out.println("赔付实付表:" + sqlStr);
    LJAGetClaimSet tLJAGetClaimSet = new LJAGetClaimSet();
    LJAGetClaimSchema tLJAGetClaimSchema = new LJAGetClaimSchema();
    LJAGetClaimDB tLJAGetClaimDB = tLJAGetClaimSchema.getDB();
    tLJAGetClaimSet = tLJAGetClaimDB.executeQuery(sqlStr);
    if (tLJAGetClaimDB.mErrors.needDealError() == true) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJAGetClaimDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetClaim";
      tError.errorMessage = "赔付实付表查询失败!";
      this.mErrors.addOneError(tError);
      tLJAGetClaimSet.clear();
      return null;
    }
    LJAGetClaimSet newLJAGetClaimSet = new LJAGetClaimSet();
    for (int n = 1; n <= tLJAGetClaimSet.size(); n++) {
      tLJAGetClaimSchema = new LJAGetClaimSchema();
      tLJAGetClaimSchema = tLJAGetClaimSet.get(n);
      tLJAGetClaimSchema.setConfDate(mLJAGetSchema.getConfDate());
      tLJAGetClaimSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
      tLJAGetClaimSchema.setModifyDate(CurrentDate);
      tLJAGetClaimSchema.setModifyTime(CurrentTime);
      tLJAGetClaimSchema.setSerialNo(mLJAGetSchema.getSerialNo());
      newLJAGetClaimSet.add(tLJAGetClaimSchema);
    }
    if (newLJAGetClaimSet.size() == 0) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetClaim";
      tError.errorMessage = "没有可以更新的赔付实付表！";
      this.mErrors.addOneError(tError);
      return null;
    }
    return newLJAGetClaimSet;

  }

  /**
   * 暂交费退费实付表
   *
   * @param mLJAGetSchema LJAGetSchema
   * @return com.sinosoft.lis.vschema.LJAGetTempFeeSet
   */
  private LJAGetTempFeeSet updateLJAGetTempFee(LJAGetSchema mLJAGetSchema) {
    if (mLJAGetSchema == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetTempFee";
      tError.errorMessage = "传入参数不能为空！";
      this.mErrors.addOneError(tError);
      return null;
    }
    String sqlStr = "select * from LJAGetTempFee where ActuGetNo='" +
        mLJAGetSchema.getActuGetNo() + "'";
    System.out.println("暂交费退费实付表:" + sqlStr);
    LJAGetTempFeeSet tLJAGetTempFeeSet = new LJAGetTempFeeSet();
    LJAGetTempFeeSchema tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
    LJAGetTempFeeDB tLJAGetTempFeeDB = tLJAGetTempFeeSchema.getDB();
    tLJAGetTempFeeSet = tLJAGetTempFeeDB.executeQuery(sqlStr);
    if (tLJAGetTempFeeDB.mErrors.needDealError() == true) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJAGetTempFeeDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetTempFee";
      tError.errorMessage = "暂交费退费实付表查询失败!";
      this.mErrors.addOneError(tError);
      tLJAGetTempFeeSet.clear();
      return null;
    }
    LJAGetTempFeeSet newLJAGetTempFeeSet = new LJAGetTempFeeSet();
    for (int n = 1; n <= tLJAGetTempFeeSet.size(); n++) {
      tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
      tLJAGetTempFeeSchema = tLJAGetTempFeeSet.get(n);
      tLJAGetTempFeeSchema.setConfDate(mLJAGetSchema.getConfDate());
      tLJAGetTempFeeSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
      tLJAGetTempFeeSchema.setModifyDate(CurrentDate);
      tLJAGetTempFeeSchema.setModifyTime(CurrentTime);
      tLJAGetTempFeeSchema.setSerialNo(mLJAGetSchema.getSerialNo());
      newLJAGetTempFeeSet.add(tLJAGetTempFeeSchema);
    }
    if (newLJAGetTempFeeSet.size() == 0) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetTempFee";
      tError.errorMessage = "没有可以更新的暂交费退费实付表！";
      this.mErrors.addOneError(tError);
      return null;
    }
    return newLJAGetTempFeeSet;

  }

  /**
   * 红利给付实付表
   *
   * @param mLJAGetSchema LJAGetSchema
   * @return com.sinosoft.lis.vschema.LJABonusGetSet
   */
  private LJABonusGetSet updateLJABonusGet(LJAGetSchema mLJAGetSchema) {
    if (mLJAGetSchema == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJABonusGet";
      tError.errorMessage = "传入参数不能为空！";
      this.mErrors.addOneError(tError);
      return null;
    }
    String sqlStr = "select * from LJABonusGet where ActuGetNo='" +
        mLJAGetSchema.getActuGetNo() + "'";
    System.out.println("红利给付实付表:" + sqlStr);
    LJABonusGetSet tLJABonusGetSet = new LJABonusGetSet();
    LJABonusGetSchema tLJABonusGetSchema = new LJABonusGetSchema();
    LJABonusGetDB tLJABonusGetDB = tLJABonusGetSchema.getDB();
    tLJABonusGetSet = tLJABonusGetDB.executeQuery(sqlStr);
    if (tLJABonusGetDB.mErrors.needDealError() == true) {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJABonusGetDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJABonusGet";
      tError.errorMessage = "红利给付实付表查询失败!";
      this.mErrors.addOneError(tError);
      tLJABonusGetSet.clear();
      return null;
    }
    LJABonusGetSet newLJABonusGetSet = new LJABonusGetSet();
    for (int n = 1; n <= tLJABonusGetSet.size(); n++) {
      tLJABonusGetSchema = new LJABonusGetSchema();
      tLJABonusGetSchema = tLJABonusGetSet.get(n);
      tLJABonusGetSchema.setConfDate(mLJAGetSchema.getConfDate());
      tLJABonusGetSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
      tLJABonusGetSchema.setModifyDate(CurrentDate);
      tLJABonusGetSchema.setModifyTime(CurrentTime);
      tLJABonusGetSchema.setSerialNo(mLJAGetSchema.getSerialNo());
      newLJABonusGetSet.add(tLJABonusGetSchema);
    }
    if (newLJABonusGetSet.size() == 0) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJABonusGet";
      tError.errorMessage = "没有可以更新的红利给付实付表！";
      this.mErrors.addOneError(tError);
      return null;
    }
    return newLJABonusGetSet;

  }

  public String[] getResult() {
    return existTempFeeNo;
  }

  /**
   * 检验
   *
   * @return boolean
   */
  public boolean checkData() { //设置TempFeeClass中的到账日期
    /*
           LJTempFeeSchema tLJTempFeeSchema =new LJTempFeeSchema();
           for(int i=1;i<=mLJTempFeeSet.size();i++)
           {
        tLJTempFeeSchema =new LJTempFeeSchema();
        tLJTempFeeSchema=mLJTempFeeSet.get(i);
        LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
        tLMRiskAppDB.setRiskCode( tLJTempFeeSchema.getRiskCode() );
        if( tLMRiskAppDB.getInfo() == false )
        {
         // @@错误处理
            this.mErrors.copyAllErrors( tLMRiskAppDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "ProposalBL";
            tError.functionName = "checkData";
            tError.errorMessage = "LMRiskApp表查询失败!";
            this.mErrors.addOneError( tError ) ;
            return false;
        }
        System.out.println( "校验主险:");
        if( tLMRiskAppDB.getSubRiskFlag().equals( "M" ))
            break;
           }
         //代理人是否正确
           LAAgentBL tLAAgentBL = new LAAgentBL();
     if(!tLAAgentBL.validateAgent(tLJTempFeeSchema.getAgentCode(),tLJTempFeeSchema.getRiskCode()))
           {
        this.mErrors.copyAllErrors( tLAAgentBL.mErrors );
        CError tError = new CError();
        tError.moduleName = "TempFeeBL";
        tError.functionName = "checkData";
        tError.errorMessage = "代理人不符合要求!";
        this.mErrors.addOneError( tError ) ;
        return false;
           }
     */
    //到帐日期处理

//        LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
    String currentTempFeeNo = "";
    String tMaxEnterAccDate = "";
    /**zhangjun*****2006-3-21***modify**/
    //如果是现金交费或者银行代收-那么存入当天系统时间,如果是支票，则置空
    /*
         PayMode     Mean
       1	现金     *
       2	现金支票
       3	转账支票（支票）*
       4	银行转账
       5	内部转帐
       6	银行托收
       7	其他
       8        赠送保险  *
       10       应收保费  *
       11       银行代收  *
     */
    for (int k = 1; k <= mLJTempFeeClassSet.size(); k++) {
      /**zhangjun*****2006-3-21***modify**/
      if (mLJTempFeeClassSet.get(k).getPayMode().equals("2") ||
                 mLJTempFeeClassSet.get(k).getPayMode().equals("3")) {
        mLJTempFeeClassSet.get(k).setEnterAccDate("");
      }else if(mLJTempFeeClassSet.get(k).getPayMode().equals("11") || mLJTempFeeClassSet.get(k).getPayMode().equals("12") ){
      }else{
          mLJTempFeeClassSet.get(k).setEnterAccDate(PubFun.getCurrentDate());
      }
//      if (mLJTempFeeClassSet.get(k).getPayMode().equals("6") &&
//          (mLJTempFeeClassSet.get(k).getBankCode() == null ||
//           mLJTempFeeClassSet.get(k).getBankCode().length() == 0)) {
//        CError tError = new CError();
//        tError.moduleName = "ProposalBL";
//        tError.functionName = "checkData";
//        tError.errorMessage = "请录入托收银行代码!";
//        this.mErrors.addOneError(tError);
//        return false;
//      }
    }

    for (int n = 1; n <= mLJTempFeeClassSet.size(); n++) {
      tMaxEnterAccDate = "1900-1-1";
      currentTempFeeNo = mLJTempFeeClassSet.get(n).getTempFeeNo();

      //把分类表中同一暂交费的纪录查出，如果有一项到帐日期为空，则tMaxEnterAccDate置空。否则取最大的到帐日期
      for (int m = 1; m <= mLJTempFeeClassSet.size(); m++) {
        if (mLJTempFeeClassSet.get(m).getTempFeeNo().equals(
            currentTempFeeNo)) {
          if (mLJTempFeeClassSet.get(m).getEnterAccDate() == null ||
              mLJTempFeeClassSet.get(m).getEnterAccDate().equals("")) {
            tMaxEnterAccDate = "";
            break;
          } else {
            //  起始日期，终止日期 ,比较单位 (格式："YYYY-MM-DD")
            int t = PubFun.calInterval(tMaxEnterAccDate,
                                       mLJTempFeeClassSet.get(m).
                                       getEnterAccDate(),
                                       "D");
            if (t > 0) {
              tMaxEnterAccDate = mLJTempFeeClassSet.get(m).
                  getEnterAccDate();
            }
          }
        }
      }
      //把主表中同一暂交费的纪录查出，置到帐日期为tMaxEnterAccDate.
      for (int s = 1; s <= mLJTempFeeSet.size(); s++) {
    	  
        if (mLJTempFeeSet.get(s).getTempFeeNo().equals(currentTempFeeNo)) {
          mLJTempFeeSet.get(s).setEnterAccDate(tMaxEnterAccDate);
        }

      
      }

    }

    return true;
  }

  // * ↓ *** add *** liuhao *** 2005-05-17 *****
  //追加预打保单交费并使保单生效的功能
  /**
   * 验证预打保单交费必须一次交清
   * @param pmLJTempFeeBL LJTempFeeSchema
   * @return boolean 如果交费金额和应交一致返回true否则返回false
   */
  private boolean checkLJAPay(LJTempFeeSchema pmLJTempFeeBL) {
    //保单应交保费的查询语句
    StringBuffer sql = new StringBuffer();
    sql.append("select sum(prem) from lcpol where prtno='");
    sql.append(pmLJTempFeeBL.getOtherNo());
    sql.append("' and riskcode='");
    sql.append(pmLJTempFeeBL.getRiskCode());
    sql.append("'");
    ExeSQL tExeSQL = new ExeSQL();
    double riskPrem = Double.parseDouble(tExeSQL.getOneValue(sql.toString()));
    if (riskPrem != pmLJTempFeeBL.getPayMoney()) {
      return false;
    }
    return true;
  }

  /**
   * 执行
   *
   * @param conn Connection
   * @param sql String
   * @return boolean
   */
  private double execSumQuery(Connection conn, String sql) {
    PreparedStatement st = null;
    //   Connection conn = null;
    ResultSet rs = null;
    try {
      // conn = DBConnPool.getConnection();
      if (conn == null) {
        return 0;
      }
      st = conn.prepareStatement(sql);
      if (st == null) {
        return 0;
      }
      rs = st.executeQuery();
      if (rs.next()) {
        return rs.getDouble(1);
      }
      return 0;
    } catch (Exception ex) {
      ex.printStackTrace();
      return 0;
    } finally {
      try {
        st.close();
        rs.close();
      } catch (Exception e) {}

    }
  }

  /**
   * 处理预打保单实收财务
   * @param pmLCGrpContSchema LCGrpContSchema
   */
  private void dealEngageAPay(LCGrpContSchema pmLCGrpContSchema) {
    //集体保单印刷号
    String tGrpPrtNo = pmLCGrpContSchema.getPrtNo();
    //集体实交金额
    double tPayMoney = 0.00;
    //交费收据号
    String tPayNo = PubFun1.CreateMaxNo("PAYNO",
                                        PubFun.getNoLimit(pmLCGrpContSchema.getManageCom()));
    //到账日期
    String tEnterAccDate = null;
    //集体险种信息查询相关变量
    LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
    LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();

    for (int i = 1; i <= mLJTempFeeSetNew.size(); i++) {
      if (tGrpPrtNo.equals(mLJTempFeeSetNew.get(i).getOtherNo())) {
        tPayMoney = mLJTempFeeSetNew.get(i).getPayMoney();
//        tPayNo = mLJTempFeeSetNew.get(i).getTempFeeNo();
        tEnterAccDate = mLJTempFeeSetNew.get(i).getEnterAccDate();
        break;
      }
    }

    //根据集体保单信息查询集体险种信息
    tLCGrpPolDB.setPrtNo(pmLCGrpContSchema.getPrtNo());
    tLCGrpPolSet = tLCGrpPolDB.query();
    if (tLCGrpPolSet != null) {
      //处理预打保单财务实收信息
      dealGrpAPay(tLCGrpPolSet.get(1), tPayNo, tEnterAccDate, tPayMoney);
    }

    //实收总表
    //准备实收总表数据
    prepareLJAPAY(pmLCGrpContSchema, tPayMoney, tPayNo);
  }

  /**
   * 处理预打保单财务实交信息
   *
   * @param tLCGrpPolSchema LCGrpPolSchema
   * @param tPaySerialNo String
   * @param pmEnterAccDate String
   * @param pmPayMoney double
   */
  private void dealGrpAPay(LCGrpPolSchema tLCGrpPolSchema,
                           String tPaySerialNo,
                           String pmEnterAccDate,
                           double pmPayMoney) {
    LJAPayGrpSet PayGrpSet = new LJAPayGrpSet();
    MMap tmpMap = new MMap();
    String confDate = PubFun.getCurrentDate();
    String confTime = PubFun.getCurrentTime();
    //集体保单查询
    LCPremSet tPremSet = new LCPremSet();
    LCPolSet tPolSet = new LCPolSet();
    LCPolSchema tPolSchema = null;
    LCPolDB tPolDB = new LCPolDB();
    LCPremDB tPremDB = new LCPremDB();
    LJAPayGrpSchema PayGrpSchema = null;
    LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();

    tPolDB.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());

    tPolDB.setAppFlag("1"); //已签单

    tPolSet = tPolDB.query();
    if (tPolSet == null) {
      return;
    }

    for (int j = 1; j <= tPolSet.size(); j++) {
      tPolSchema = tPolSet.get(j);
      if (j == 1) {
        //缓存下一次交费日期
        //paytoDateMap.put( tPolSchema.getRiskCode() , tPolSchema.getPaytoDate());
        //在第一个节点处初始化
        PayGrpSchema = new LJAPayGrpSchema();
        PayGrpSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
        PayGrpSchema.setRiskCode(tPolSchema.getRiskCode());
        PayGrpSchema.setSumActuPayMoney(pmPayMoney);
        PayGrpSchema.setSumDuePayMoney(pmPayMoney);
        PayGrpSchema.setPayType("ZC");
        PayGrpSchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
        PayGrpSchema.setAgentType(tLCGrpPolSchema.getAgentType());
        PayGrpSchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());
        PayGrpSchema.setAppntNo(tLCGrpPolSchema.getCustomerNo());
        PayGrpSchema.setApproveCode(tGI.Operator);
        PayGrpSchema.setApproveDate(confDate);
        PayGrpSchema.setPayCount(1);
        PayGrpSchema.setPayNo(tPaySerialNo);
        PayGrpSchema.setPayDate(confDate);
        PayGrpSchema.setEnterAccDate(pmEnterAccDate);
        PayGrpSchema.setLastPayToDate(confDate);
        //下一次交费日期
        PayGrpSchema.setCurPayToDate(tPolSchema.getPaytoDate());
        PayGrpSchema.setApproveTime(confTime);
        PayGrpSchema.setConfDate(confDate);
        PayGrpSchema.setGrpContNo(tLCGrpPolSchema.getGrpContNo());
        PayGrpSchema.setMakeDate(confDate);
        PayGrpSchema.setMakeTime(confTime);
        PayGrpSchema.setModifyDate(PubFun.getCurrentDate());
        PayGrpSchema.setModifyTime(PubFun.getCurrentTime());
        PayGrpSchema.setOperator(tGI.Operator);
        PayGrpSchema.setManageCom(tLCGrpPolSchema.getManageCom());
        PayGrpSchema.setAgentCode(tPolSchema.getAgentCode());

      }
      //实收信息,初始化基本信息
      tPolSchema = tPolSet.get(j);
      //个人保单保费项查询
      tPremDB.setPolNo(tPolSchema.getPolNo());
      tPremSet = tPremDB.query();
      if (tPremSet == null) {
        continue;
      }
      //转换数据类型
      //tdate = fdate.getDate( tPolSchema.getPayEndDate());
      //if ( maxPayEndDate ==null || maxPayEndDate.before( tdate ) )
      //{
      //    maxPayEndDate= tdate ;
      //}
      int m = tPremSet.size();
      if (m > 0) {
        for (int k = 1; k <= m; k++) {

          LCPremSchema tLCPremSchema = tPremSet.get(k);
          LJAPayPersonSchema tLJAPayPersonSchema = new
              LJAPayPersonSchema();
          tLJAPayPersonSchema.setPolNo(tPolSchema.getPolNo());
          tLJAPayPersonSchema.setPayCount(1);
          tLJAPayPersonSchema.setGrpPolNo(tLCGrpPolSchema.getGrpPolNo());
          tLJAPayPersonSchema.setContNo(tPolSchema.getContNo());
          tLJAPayPersonSchema.setGrpContNo(tLCGrpPolSchema.getGrpContNo());
          tLJAPayPersonSchema.setAppntNo(tPolSchema.getAppntNo());
          tLJAPayPersonSchema.setPayNo(tPaySerialNo);
          tLJAPayPersonSchema.setPayAimClass("1");
          tLJAPayPersonSchema.setDutyCode(tLCPremSchema.getDutyCode());
          tLJAPayPersonSchema.setPayPlanCode(tLCPremSchema.
                                             getPayPlanCode());
          tLJAPayPersonSchema.setSumDuePayMoney(tLCPremSchema.getPrem());
          tLJAPayPersonSchema.setSumActuPayMoney(tLCPremSchema.
                                                 getPrem());
          tLJAPayPersonSchema.setPayIntv(tLCPremSchema.getPayIntv());
          tLJAPayPersonSchema.setPayDate(confDate);
          tLJAPayPersonSchema.setPayType("ZC");
          tLJAPayPersonSchema.setEnterAccDate(pmEnterAccDate);
          tLJAPayPersonSchema.setConfDate(confDate);
          tLJAPayPersonSchema.setLastPayToDate("1899-12-31");
          tLJAPayPersonSchema.setCurPayToDate(tLCPremSchema.
                                              getPaytoDate());
          tLJAPayPersonSchema.setOperator(tGI.Operator);
          tLJAPayPersonSchema.setApproveCode(tGI.Operator);
          tLJAPayPersonSchema.setApproveDate(confDate);
          tLJAPayPersonSchema.setApproveTime(confTime);

          tLJAPayPersonSchema.setMakeDate(confDate);
          tLJAPayPersonSchema.setMakeTime(confTime);
          tLJAPayPersonSchema.setModifyDate(PubFun.getCurrentDate());
          tLJAPayPersonSchema.setModifyTime(PubFun.getCurrentTime());
          tLJAPayPersonSchema.setManageCom(tPolSchema.getManageCom());
          tLJAPayPersonSchema.setAgentCom(tPolSchema.getAgentCom());
          tLJAPayPersonSchema.setAgentType(tPolSchema.getAgentType());
          tLJAPayPersonSchema.setRiskCode(tPolSchema.getRiskCode());
          tLJAPayPersonSchema.setAgentCode(tPolSchema.getAgentCode());
          tLJAPayPersonSchema.setAgentGroup(tPolSchema.getAgentGroup());

          tLJAPayPersonSet.add(tLJAPayPersonSchema);
        }

      }
      //tmpMap.put(tLJAPayPersonSet, "INSERT");
      //集体保单实交信息
      //fillLJAPayGrp(PayGrpSchema, tPremSet);
    }
    //缓存实收集体交费数据
    mLJAPayGrpSet.add(PayGrpSchema);
    //缓存实收个人交费表
    mLJAPayPersonSet.add(tLJAPayPersonSet);
  }

  /**
   * 根据传入保费项，求集体保单总应交和总实交
   *
   * @param tPayGrpSchema LJAPayGrpSchema
   * @param tPremSet LCPremSet
   */
  private void fillLJAPayGrp(LJAPayGrpSchema tPayGrpSchema,
                             LCPremSet tPremSet) {
    //求保费
    double sumPrem = 0.00;
    double sumStandPrem = 0.00;
    LCPremSchema tPremSchema = null;
    for (int t = 1; t <= tPremSet.size(); t++) {
      tPremSchema = tPremSet.get(t);
      sumPrem += tPremSchema.getPrem();
      sumStandPrem += tPremSchema.getStandPrem();
    }
    tPayGrpSchema.setSumActuPayMoney(PubFun.setPrecision(tPayGrpSchema.
                                                         getSumActuPayMoney() +
                                                         sumPrem, "0.00"));
    tPayGrpSchema.setSumDuePayMoney(PubFun.setPrecision(tPayGrpSchema.
                                                        getSumDuePayMoney() +
                                                        sumStandPrem, "0.00"));

  }

  /**
   * 总实收
   *
   * @param tLCGrpContSchema LCGrpContSchema
   * @param pmPayMoney double
   * @param pmPayNo String
   */
  private void prepareLJAPAY(LCGrpContSchema tLCGrpContSchema,
                             double pmPayMoney,
                             String pmPayNo) {
    String confDate = PubFun.getCurrentDate();
    String confTime = PubFun.getCurrentTime();
    MMap tmpMap = new MMap();
    // 生成流水号
    LJAPaySchema tLJAPaySchema = new LJAPaySchema();
    tLJAPaySchema.setPayNo(pmPayNo);
    tLJAPaySchema.setIncomeNo(tLCGrpContSchema.getGrpContNo());
    tLJAPaySchema.setIncomeType("1");
    tLJAPaySchema.setAppntNo(tLCGrpContSchema.getAppntNo());
    tLJAPaySchema.setSumActuPayMoney(pmPayMoney);
    tLJAPaySchema.setPayDate(confDate);
    tLJAPaySchema.setEnterAccDate(PubFun.getCurrentDate());
    tLJAPaySchema.setConfDate(PubFun.getCurrentDate());
    tLJAPaySchema.setApproveCode(tGI.Operator);
    tLJAPaySchema.setApproveDate(confDate);
    tLJAPaySchema.setSerialNo(pmPayNo);
    tLJAPaySchema.setOperator(tGI.Operator);
    tLJAPaySchema.setMakeDate(confDate);
    tLJAPaySchema.setMakeTime(confTime);
    tLJAPaySchema.setModifyDate(PubFun.getCurrentDate());
    tLJAPaySchema.setModifyTime(PubFun.getCurrentTime());
    tLJAPaySchema.setStartPayDate(confDate);
    tLJAPaySchema.setManageCom(tLCGrpContSchema.getManageCom());
    tLJAPaySchema.setAgentCom(tLCGrpContSchema.getAgentCom());
    tLJAPaySchema.setAgentType(tLCGrpContSchema.getAgentType());
    tLJAPaySchema.setAgentCode(tLCGrpContSchema.getAgentCode());
    tLJAPaySchema.setAgentGroup(tLCGrpContSchema.getAgentGroup());
    tLJAPaySchema.setAgentType(tLCGrpContSchema.getAgentType());
    tLJAPaySchema.setBankAccNo(tLCGrpContSchema.getBankAccNo());
    tLJAPaySchema.setBankCode(tLCGrpContSchema.getBankCode());
    tLJAPaySchema.setAccName(tLCGrpContSchema.getAccName());
    tLJAPaySchema.setPayTypeFlag(tLCGrpContSchema.getPayMode()); //?

    mLJAPaySet.add(tLJAPaySchema);
    //tmpMap.put(tLJAPaySchema, "INSERT");
    //return true;
  }
  
  private boolean finUrgeVerify() {
      for(int i=1 ; i <= mLJTempFeeSet.size() ;i++ ) {
          if (!mLJTempFeeSet.get(i).getTempFeeType().equals("2")) {
                  continue;
          }
          LJSPaySchema tLJSPaySchema = new LJSPaySchema();
          tLJSPaySchema.setGetNoticeNo(mLJTempFeeSet.get(i).getTempFeeNo());
          VData ttVdata = new VData();
          ttVdata.add(tLJSPaySchema);
          ttVdata.add(tGI);
          ChangeFinFeeStateBL bl = new ChangeFinFeeStateBL("4");
          if (!bl.submitData(ttVdata, "")) {
              CError tError = new CError();
              tError.moduleName = "TempFeeBL";
              tError.functionName = "finUrgeVerify";
              tError.errorMessage = "收费成功，但续期应收数据处理失败";
              mErrors.addOneError(tError);
              System.out.println(tError.errorMessage);
              continue;
          }
      }
      return true;
  }

  // * ↑ *** add *** liuhao *** 2005-05-17 *****
  /**
   * 出错处理
   * @param szFunc String
   * @param szErrMsg String
   */
  private void buildError(String szFunc, String szErrMsg) {
    CError cError = new CError();
    cError.moduleName = "TempFeeBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }

}
