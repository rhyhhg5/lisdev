 package com.sinosoft.lis.finfee;

import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单查询业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class TempFeeQueryForUrgeGetBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  /** 业务处理相关变量 */
  /** 暂交费表 */
  private LJTempFeeSet  mLJTempFeeSet = new LJTempFeeSet() ;
  private LJTempFeeSchema  mLJTempFeeSchema = new LJTempFeeSchema() ;

  public TempFeeQueryForUrgeGetBL() {}

  public static void main(String[] args) {
  }

  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
System.out.println("---getInputData---");

    //进行业务处理
	    if (!queryLJTempFee())
	      return false;
System.out.println("---queryLJTempFee---");

    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    // 检验查询条件
    mLJTempFeeSchema.setSchema((LJTempFeeSchema)cInputData.getObjectByObjectName("LJTempFeeSchema",0));

    if(mLJTempFeeSchema == null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "ProposalQueryBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "请输入查询条件!";
      this.mErrors.addOneError(tError) ;
      return false;
    }
    return true;
  }


  /**
   * 查询暂交费表信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean queryLJTempFee()
  {
  	// 保单信息
      LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
      tLJTempFeeDB.setSchema(mLJTempFeeSchema);

      String TempFeeNo=mLJTempFeeSchema.getTempFeeNo();
      String TempFeeType=mLJTempFeeSchema.getTempFeeType();
      String OtherNo=mLJTempFeeSchema.getOtherNo();
      String OtherNoType=mLJTempFeeSchema.getOtherNoType();
      String StartDate =mLJTempFeeSchema.getEnterAccDate();
      String EndDate =mLJTempFeeSchema.getConfDate();
      String ConfFlag =mLJTempFeeSchema.getConfFlag();

System.out.println("TempFeeNo:"+TempFeeNo);
System.out.println("ConfFlag:"+ConfFlag);

      if(TempFeeNo==null)
      {
      	String sqlStr="select * from LJTempFee where 1=1 ";
      	if(TempFeeType!=null)
      	 sqlStr=sqlStr+" and TempFeeType='"+TempFeeType+"'";
       if(OtherNo!=null)
      	 sqlStr=sqlStr+" and OtherNo='"+OtherNo+"'";
      	if(OtherNoType!=null)
      	 sqlStr=sqlStr+" and OtherNoType='"+OtherNoType+"'";
      	if(StartDate!=null)
      	 sqlStr=sqlStr+" and PayDate>='"+StartDate+"'";
      	if(EndDate!=null)
      	 sqlStr=sqlStr+" and PayDate<='"+EndDate+"'";
      	if(ConfFlag!=null)
      	 {
      	  if(ConfFlag.equals("0"))
      	    sqlStr=sqlStr+" and (ConfFlag='0' or ConfFlag is null) ";
      	  if(ConfFlag.equals("1"))
      	    sqlStr=sqlStr+" and ConfFlag='1' ";
      	 }
System.out.println("sqlStr="+sqlStr);
       mLJTempFeeSet = tLJTempFeeDB.executeQuery(sqlStr);
      }
      else{
      mLJTempFeeSet = tLJTempFeeDB.query();
      }
      if (tLJTempFeeDB.mErrors.needDealError() == true)
      {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJTempFeeDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "TempFeeQueryForUrgeGetBL";
      tError.functionName = "queryData";
      tError.errorMessage = "暂交费查询失败!";
      this.mErrors.addOneError(tError);
      mLJTempFeeSet.clear();
      return false;
    }
    if (mLJTempFeeSet.size() == 0)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "TempFeeQueryForUrgeGetBL";
      tError.functionName = "queryData";
      tError.errorMessage = "未找到相关数据!";
      this.mErrors.addOneError(tError);
      return false;
    }
    mResult.add(mLJTempFeeSet);
    return true;
    }
}
