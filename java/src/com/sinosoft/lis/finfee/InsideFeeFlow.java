package com.sinosoft.lis.finfee;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

import java.util.*;

/**
* <p>Title: Web业务系统</p>
* <p>Description:内部转账自动流转（暂对个人）
* 在已经发催收的前提下，实现内部转账所需要的财务暂交费流程和业务核销及财务付费的流程
* 调用内部唯一的公共方法：DoInsideFeeFlow
* 返回一个集合，包含所有准备好的数据
*  mLCPolSchema          //更新个人保单表
   mLCPremNewSet         //更新保费项表
   mLCDutySet            //更新责任表
   mLJTempFeeSchema      //插入暂交费表
   mLJTempFeeClassSet    //插入暂交费分类表
   mLJAPayPersonSet      //插入实收个人表
   mLJAPaySchema         //插入实收总表
   mLCInsureAccSet       //先删除再插入保险帐户表
   mLCInsureAccTraceSet  //插入保险帐户履历表
   mLJSPayPersonSet      //删除应收个人表
   mLJSPaySchema         //删除应收总表
   mLJAGetSchema         //插入实付总表
   mLJFIGetSchema        //插入财务给付表
   mLJAGetEndorseSet     //插入批改补退费表(实收/实付)

*
*
* </p>
* <p>Copyright: Copyright (c) 2003</p>
* <p>Company: Sinosoft</p>
* @author HZM
* @version 1.0
*/
public class InsideFeeFlow
{
    /**
     * 成员变量
     */
    public CErrors mErrors = new CErrors(); //错误类
    private GlobalInput mGI = new GlobalInput();
    private LCPolSet mLCPolSet = new LCPolSet(); //个人保单表
    private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet(); //暂交费表
    private LJTempFeeClassSchema mLJTempFeeClassSchema = new LJTempFeeClassSchema(); //暂交费分类表
    private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet(); //暂交费分类表
    private LCPremSet mLCPremSet = new LCPremSet(); //保费项表
    private LCPremSet mLCPremNewSet = new LCPremSet(); //保费项表
    private LCInsureAccSet mLCInsureAccSet = new LCInsureAccSet(); //保险帐户表
    private LCInsureAccTraceSet mLCInsureAccTraceSet = new LCInsureAccTraceSet(); //保险履历表
    private LCDutySet mLCDutySet = new LCDutySet(); //责任表
    private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet(); //实收个人表
    private LJAPaySchema mLJAPaySchema = new LJAPaySchema(); //实收总表
    private LJSPaySchema mLJSPaySchema = new LJSPaySchema(); //应收总表
    private LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet(); //应收个人表
    private LJAGetSchema mLJAGetSchema = new LJAGetSchema(); //实付总表
    private LJFIGetSchema mLJFIGetSchema = new LJFIGetSchema(); //财务给付表
    private LJAGetEndorseSet mLJAGetEndorseSet = new LJAGetEndorseSet(); //批改补退费表(实收/实付)
    private VData mVData = new VData();
    private String CurrentDate = PubFun.getCurrentDate(); //系统当前时间
    private String CurrentTime = PubFun.getCurrentTime();
    private String mLimit = ""; //流水号
    private String serNo = "";
    private String ActuGetNo = "";

    public MMap map = new MMap();

    public InsideFeeFlow()
    {
    }

    public static void main(String[] args)
    {
        InsideFeeFlow tInsideFeeFlow = new InsideFeeFlow();
        LCPolSchema tLCPolSchema = new LCPolSchema();
        VData tVData = new VData();
        LCPolSet tLCPolSet = new LCPolSet();

        //查询
        LCPolDB tLCPolDB = new LCPolDB();
        String PolNo = "86110020030210000078";
        String sqlStr = "select * from LCPol where PolNo='" + PolNo + "'";
        tLCPolSet = tLCPolDB.executeQuery(sqlStr);
        tLCPolSchema = tLCPolSet.get(1);
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86110000";
        tGI.ComCode = "86110000";
        tGI.Operator = "001";
        String ActuGetNo = "001";
        LJAGetSchema ttLJAGetSchema = new LJAGetSchema();
        LJAGetEndorseSet ttLJAGetEndorseSet = new LJAGetEndorseSet();

        //        tVData = tInsideFeeFlow.DoInsideFeeFlow(tLCPolSchema, tGI, ttLJAGetSchema,
        //                ttLJAGetEndorseSet);
    }

    /**
     * 实现内部转账收付费的自动流转:在已经催收的情况下.
     * 实现财务的续期暂交费收取和业务核销的数据处理过程,返回相关的处理过的数据集合
     * @param pLCPolSchema     个人保单集合
     * @param tLJAGetSchema    实付总表纪录
     * @param tLJAGetEndorseSet  批改补退费表纪录集
     * @param tGI       系统对象
     * @return VData    包含相关的Schema和Set
     */
    public VData DoInsideFeeFlow(LCPolSet pLCPolSet, GlobalInput tGI, LJAGetSchema tLJAGetSchema,
        LJAGetEndorseSet tLJAGetEndorseSet)
    {
        if ((pLCPolSet == null) || (tGI == null) || (tLJAGetSchema == null)
                || (tLJAGetEndorseSet == null) || (pLCPolSet.size() == 0))
        {
            CError tError = new CError();
            tError.moduleName = "InsideFeeFlow";
            tError.functionName = "DoInsideFeeFlow";
            tError.errorMessage = "传入参数不能为空!";
            this.mErrors.addOneError(tError);
            return null;
        }

        //保存到内部对象
        mGI = tGI;
        mLJAGetSchema = tLJAGetSchema;
        mLJAGetEndorseSet = tLJAGetEndorseSet;
        ActuGetNo = tLJAGetSchema.getActuGetNo(); //实付号码
        mLCPolSet.set(pLCPolSet);

        mLimit = PubFun.getNoLimit(mGI.ManageCom);
        serNo = PubFun1.CreateMaxNo("SERIALNO", mLimit);
        boolean flag = false;

        //1-模拟财务暂交费流程（交费方式：5 -- 内部转帐）
        String TempfeeNo = ""; //暂交费号
        flag = DoInsideTempfee();
        if (!flag)
        {
            return null;
        }

//        //2-模拟业务核销流程
//        flag = DoInsideVerify();
//        if (!flag)
//        {
//            return null;
//        }
//
//        //3-处理财务付费和核销
//        flag = DoFinFeePay();
//        if (!flag)
//        {
//            return null;
//        }
//
//        //4-处理数据
//        mVData.add(mLCPolSet); //更新个人保单表
//        mVData.add(mLCPremNewSet); //更新保费项表
//        mVData.add(mLCDutySet); //更新责任表
//
//        mVData.add(mLJAPayPersonSet); //插入实收个人表
//        mVData.add(mLJAPaySchema); //插入实收总表
//        mVData.add(mLJTempFeeSet); //插入暂交费表
//        mVData.add(mLJTempFeeClassSet); //插入暂交费分类表
//
//        mVData.add(mLJSPayPersonSet); //删除应收个人表
//        mVData.add(mLJSPaySchema); //删除应收总表
//
//        mVData.add(mLCInsureAccSet); //先删除再插入保险帐户表
//        mVData.add(mLCInsureAccTraceSet); //插入保险帐户履历表
//
//        mVData.add(mLJAGetSchema); //插入实付总表
//        mVData.add(mLJFIGetSchema); //插入财务给付表
//        mVData.add(mLJAGetEndorseSet); //插入批改补退费表(实收/实付)


//        map.put(mLCPolSet, "UPDATE");
//        map.put(mLCPremNewSet, "UPDATE");
//        map.put(mLCDutySet, "UPDATE");
//
//        map.put(mLJAPayPersonSet, "INSERT");
//        map.put(mLJAPaySchema, "INSERT");
        map.put(mLJTempFeeSet, "INSERT");
        map.put(mLJTempFeeClassSet, "INSERT");

//        map.put(mLJSPayPersonSet, "DELETE");
//        map.put(mLJSPaySchema, "DELETE");
//
//        map.put(mLCInsureAccSet, "DELETE&INSERT");
//        map.put(mLCInsureAccTraceSet, "INSERT");
//
//        map.put(mLJAGetSchema, "INSERT");
//        map.put(mLJFIGetSchema, "INSERT");
//        map.put(mLJAGetEndorseSet, "INSERT");

        return mVData;
    }

    /**
     * 模拟财务暂交费流程，如果成功，返回不为空的暂交费号
     * @return boolean
     */
    private boolean DoInsideTempfee()
    {
        //1-查询应收总表
        mLJSPaySchema = queryLJSPay();
        if (mLJSPaySchema == null)
        {
            return false;
        }

        for (int i = 0; i < mLCPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = mLCPolSet.get(i + 1);

            //一个险种（一个保单号）只保存一条暂交费记录
            boolean repeat = false;
            for (int j=0; j<mLJTempFeeSet.size(); j++) {
                if (mLJTempFeeSet.get(j+1).getRiskCode().equals(tLCPolSchema.getRiskCode())) {
                    repeat = true;
                }
            }
            if (repeat) {
                continue;
            }

            //2-查询是否已经交费
            LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
            tLJTempFeeSchema.setTempFeeNo(mLJSPaySchema.getGetNoticeNo());
            tLJTempFeeSchema.setTempFeeType("2");
            tLJTempFeeSchema.setRiskCode(mLCPolSet.get(i + 1).getRiskCode());
            if (!queryTempFee(tLJTempFeeSchema))
            {
                return false;
            }

            //获取该保单的应收总额
            LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
            tLJSPayPersonDB.setPolNo(mLCPolSet.get(i+1).getPolNo());
            LJSPayPersonSet tLJSPayPersonSet = tLJSPayPersonDB.query();
            double money = 0;
            for (int j=0; j<tLJSPayPersonSet.size(); j++) {
                money = money + tLJSPayPersonSet.get(j+1).getSumDuePayMoney();
            }

            //3-组成暂交费纪录和暂交费分类表纪录
            tLJTempFeeSchema = new LJTempFeeSchema();
            tLJTempFeeSchema.setTempFeeNo(mLJSPaySchema.getGetNoticeNo());
            tLJTempFeeSchema.setTempFeeType("2");
            tLJTempFeeSchema.setRiskCode(tLCPolSchema.getRiskCode());
            tLJTempFeeSchema.setPayIntv(tLCPolSchema.getPayIntv());
            tLJTempFeeSchema.setOtherNo(tLCPolSchema.getPolNo());
            tLJTempFeeSchema.setOtherNoType("0");

            tLJTempFeeSchema.setPayMoney(money);

            tLJTempFeeSchema.setPayDate(mLJSPaySchema.getPayDate());
            tLJTempFeeSchema.setEnterAccDate(CurrentDate);
            tLJTempFeeSchema.setConfDate(CurrentDate);
            tLJTempFeeSchema.setConfMakeDate(CurrentDate);
            tLJTempFeeSchema.setSaleChnl(tLCPolSchema.getSaleChnl());
            tLJTempFeeSchema.setManageCom(tLCPolSchema.getManageCom());
            tLJTempFeeSchema.setAgentCom(mLJSPaySchema.getAgentCom());
            tLJTempFeeSchema.setAgentType(mLJSPaySchema.getAgentType());
            tLJTempFeeSchema.setAPPntName(tLCPolSchema.getAppntName());
            tLJTempFeeSchema.setAgentGroup(mLJSPaySchema.getAgentGroup());
            tLJTempFeeSchema.setAgentCode(mLJSPaySchema.getAgentCode());
            tLJTempFeeSchema.setConfFlag("0");
            tLJTempFeeSchema.setSerialNo(serNo);
            tLJTempFeeSchema.setOperator(mGI.Operator);
            tLJTempFeeSchema.setMakeDate(CurrentDate);
            tLJTempFeeSchema.setModifyDate(CurrentDate);
            tLJTempFeeSchema.setMakeTime(CurrentTime);
            tLJTempFeeSchema.setModifyTime(CurrentTime);

            mLJTempFeeSet.add(tLJTempFeeSchema);
        }

        mLJTempFeeClassSchema.setTempFeeNo(mLJSPaySchema.getGetNoticeNo());
        mLJTempFeeClassSchema.setPayMode("5");
        mLJTempFeeClassSchema.setChequeNo(ActuGetNo);
        mLJTempFeeClassSchema.setPayMoney(mLJSPaySchema.getSumDuePayMoney());
        //2005.1.20杨明注释掉问题代码mLJTempFeeClassSchema.setAPPntName(mLCPolSet.get(1).getAppntName());
        mLJTempFeeClassSchema.setPayDate(mLJSPaySchema.getPayDate());
        mLJTempFeeClassSchema.setApproveDate(CurrentDate);
        mLJTempFeeClassSchema.setEnterAccDate(CurrentDate);
        mLJTempFeeClassSchema.setConfDate(CurrentDate);
        mLJTempFeeClassSchema.setConfFlag("0");
        mLJTempFeeClassSchema.setConfMakeDate(CurrentDate);
        mLJTempFeeClassSchema.setSerialNo(serNo);
        mLJTempFeeClassSchema.setMakeDate(CurrentDate);
        mLJTempFeeClassSchema.setMakeTime(CurrentTime);
        mLJTempFeeClassSchema.setModifyDate(CurrentDate);
        mLJTempFeeClassSchema.setModifyTime(CurrentTime);
        mLJTempFeeClassSchema.setManageCom(mLCPolSet.get(1).getManageCom());
        mLJTempFeeClassSchema.setOperator(mGI.Operator);

        //不核销的情况下，要在这里加入
        mLJTempFeeClassSet.add(mLJTempFeeClassSchema);

        return true;
    }

    /**
     * 模拟业务核销功能
     * @return 是否处理成功
     */
    private boolean DoInsideVerify()
    {
        //1-查询应收个人表
        if (!queryLJSPayPerson())
        {
            return false;
        }

        //2-查询保费项表和处理帐户
        if (!queryLCPrem())
        {
            return false;
        }

        //3-查询责任表
        if (!queryLCDuty())
        {
            return false;
        }

        //4-整理数据
        dealData();
        return true;
    }

    //--调用子程序

    /**
     * 查询应收总表
     * @return LJSPaySchema
     */
    private LJSPaySchema queryLJSPay()
    {
        String sqlStr = "select * from LJSPay where OtherNo='" + mLCPolSet.get(1).getMainPolNo()
            + "' and OtherNoType='2'";
        System.out.println("查询应收总表:" + sqlStr);
        LJSPayDB tLJSPayDB = new LJSPayDB();
        LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(sqlStr);
        if (tLJSPayDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLJSPayDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "InsideFeeFlow";
            tError.functionName = "queryLJSPay";
            tError.errorMessage = "查询应收总表失败!";
            this.mErrors.addOneError(tError);
            tLJSPaySet.clear();
            return null;
        }

        if (tLJSPaySet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "InsideFeeFlow";
            tError.functionName = "queryLJSPay";
            tError.errorMessage = "没有找到对应的应收总表纪录！";
            this.mErrors.addOneError(tError);
            return null;
        }

        return tLJSPaySet.get(1);
    }

    private boolean DoFinFeePay()
    {
        if (mLJAGetSchema == null)
        {
            return false;
        }

        //核销实付表
        mLJAGetSchema.setConfDate(CurrentDate);
        mLJAGetSchema.setEnterAccDate(CurrentDate);
        mLJAGetSchema.setMakeDate(CurrentDate);
        mLJAGetSchema.setMakeTime(CurrentTime);
        mLJAGetSchema.setModifyDate(CurrentDate);
        mLJAGetSchema.setModifyTime(CurrentTime);

        //查询财务给付表中是否有该纪录--有报错
        if (QyeryLJFIGet())
        {
            return false;
        }

        //3-查询实付总表:得到实付号码，其它号码类型
        String OtherNoType = mLJAGetSchema.getOtherNoType();
        if (OtherNoType.equals("3")) //更新 批改补退费表(实收/实付) LJAGetEndorse
        {
            //核销批改补退费表
            if (!updateLJAGetEndorse(mLJAGetSchema))
            {
                return false;
            }
        }
        else
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "InsideFeeFlow";
            tError.functionName = "DoFinFeePay";
            tError.errorMessage = "查询实付总表纪录中的其它号码类型不匹配!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //生成财务给付表数据
        mLJFIGetSchema.setActuGetNo(ActuGetNo);
        mLJFIGetSchema.setPayMode("5");
        mLJFIGetSchema.setOtherNo(mLJAGetSchema.getOtherNo());
        mLJFIGetSchema.setOtherNoType(mLJAGetSchema.getOtherNoType());
        mLJFIGetSchema.setGetMoney(mLJAGetSchema.getSumGetMoney());
        mLJFIGetSchema.setShouldDate(mLJAGetSchema.getShouldDate());
        mLJFIGetSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
        mLJFIGetSchema.setConfDate(mLJAGetSchema.getConfDate());
        mLJFIGetSchema.setSaleChnl(mLJAGetSchema.getSaleChnl());
        mLJFIGetSchema.setManageCom(mLJAGetSchema.getManageCom());
        mLJFIGetSchema.setAgentCom(mLJAGetSchema.getAgentCom());
        mLJFIGetSchema.setAgentType(mLJAGetSchema.getAgentType());
//        mLJFIGetSchema.setAPPntName(mLCPolSchema.getAppntName());
        mLJFIGetSchema.setAgentGroup(mLJAGetSchema.getAgentGroup());
        mLJFIGetSchema.setAgentCode(mLJAGetSchema.getAgentCode());
        mLJFIGetSchema.setSerialNo(serNo);
        mLJFIGetSchema.setDrawer(mLJAGetSchema.getDrawer());
        mLJFIGetSchema.setDrawerID(mLJAGetSchema.getDrawerID());
        mLJFIGetSchema.setOperator(mGI.Operator);
        mLJFIGetSchema.setMakeTime(CurrentTime);
        mLJFIGetSchema.setMakeDate(CurrentDate);
        mLJFIGetSchema.setModifyTime(CurrentTime);
        mLJFIGetSchema.setModifyDate(CurrentDate);

        return true;
    }

    /*
     *  查询暂交费表
     *  返回布尔值
     */
    private boolean queryTempFee(LJTempFeeSchema pLJTempFeeSchema)
    {
        String TempFeeNo = pLJTempFeeSchema.getTempFeeNo();
        String RiskCode = pLJTempFeeSchema.getRiskCode();
        if (TempFeeNo == null)
        {
            CError tError = new CError();
            tError.moduleName = "TempFeeBL";
            tError.functionName = "queryTempFee";
            tError.errorMessage = "暂交费号不能为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        String sqlStr = "select * from LJTempFee where TempFeeNo='" + TempFeeNo + "'";
        sqlStr = sqlStr + " and RiskCode='" + RiskCode + "'";
        System.out.println("查询暂交费表:" + sqlStr);
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.executeQuery(sqlStr);
        if (tLJTempFeeDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLJTempFeeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "TempFeeBL";
            tError.functionName = "queryLJTempFee";
            tError.errorMessage = "暂交费表查询失败!";
            this.mErrors.addOneError(tError);
            tLJTempFeeSet.clear();
            return false;
        }
        if (tLJTempFeeSet.size() > 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "TempFeeBL";
            tError.functionName = "queryLJTempFee";
            tError.errorMessage = "暂交费号为：" + TempFeeNo + " 的纪录已经存在！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 查询应收个人表
     * @return LJSPaySchema
     */
    private boolean queryLJSPayPerson()
    {
        LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
        String sqlStr = "select * from LJSPayPerson where GetNoticeNo='" + mLJSPaySchema.getGetNoticeNo() + "'";
        System.out.println("查询应收个人表:" + sqlStr);
        mLJSPayPersonSet = tLJSPayPersonDB.executeQuery(sqlStr);
        if (tLJSPayPersonDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLJSPayPersonDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "InsideFeeFlow";
            tError.functionName = "queryLJSPayPerson";
            tError.errorMessage = "应收个人表查询失败!";
            this.mErrors.addOneError(tError);
            mLJSPayPersonSet.clear();
            return false;
        }

        if (mLJSPayPersonSet.size() == 0)
        {
            CError tError = new CError();
            tError.moduleName = "InsideFeeFlow";
            tError.functionName = "queryLJSPayPerson";
            tError.errorMessage = "应收个人表中没有查询到相关数据!";
            this.mErrors.addOneError(tError);
            mLJSPayPersonSet.clear();
            return false;
        }
        return true;
    }

    /**
     * 查询保费项表和处理帐户
     * @return boolean
     */
    private boolean queryLCPrem()
    {
        System.out.println("查询保费项表:");
        VData tempVData = new VData();
        DealAccount tDealAccount = new DealAccount();
        LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
        LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
        LJSPayPersonSchema tLJSPayPersonSchema;
        LCPremSet tLCPremSet;
        LCPremSchema tLCPremSchema;
        LCPremDB tLCPremDB = new LCPremDB();

        for (int num = 1; num <= mLJSPayPersonSet.size(); num++)
        {
            tLJSPayPersonSchema = new LJSPayPersonSchema();
            tLJSPayPersonSchema.setSchema(mLJSPayPersonSet.get(num));

            String sqlStr = "select * from LCPrem where PolNo='" + tLJSPayPersonSchema.getPolNo()
                + "'";
            sqlStr = sqlStr + " and DutyCode='" + tLJSPayPersonSchema.getDutyCode() + "'";
            sqlStr = sqlStr + " and PayPlanCode='" + tLJSPayPersonSchema.getPayPlanCode() + "'";
            System.out.println("sql:" + sqlStr);
            tLCPremSet = tLCPremDB.executeQuery(sqlStr);
            if (tLCPremDB.mErrors.needDealError() == true)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCPremDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "InsideFeeFlow";
                tError.functionName = "queryLCPrem";
                tError.errorMessage = "保费项表查询失败!";
                this.mErrors.addOneError(tError);
                tLCPremSet.clear();
                mLCPremSet.clear();
                return false;
            }

            tLCPremSchema = new LCPremSchema();
            tLCPremSchema.setSchema(tLCPremSet.get(1).getSchema());
            mLCPremSet.add(tLCPremSchema);

            //处理帐户
            tempVData = tDealAccount.addPrem(tLCPremSchema, "2",
                    mLJSPaySchema.getGetNoticeNo(), "2", "BF", null);
            if (tempVData != null)
            {
                tempVData = tDealAccount.updateLCInsureAccTraceDate(mLJSPaySchema.getPayDate(),
                        tempVData);
                if (tempVData == null)
                {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "InsideFeeFlow";
                    tError.functionName = "queryLCPrem";
                    tError.errorMessage = "修改保险帐户表记价履历表纪录的交费日期时出错!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                tLCInsureAccSet = (LCInsureAccSet) tempVData.getObjectByObjectName("LCInsureAccSet",
                        0);
                tLCInsureAccTraceSet = (LCInsureAccTraceSet) tempVData.getObjectByObjectName("LCInsureAccTraceSet",
                        0);
                mLCInsureAccSet.add(tLCInsureAccSet);
                mLCInsureAccTraceSet.add(tLCInsureAccTraceSet);
            }
        }

        return true;
    }

    /**
     * 查询责任表
     * @return boolean
     */
    private boolean queryLCDuty()
    {
        for (int i = 1; i <= mLJSPayPersonSet.size(); i++)
        {
            LCDutyDB tLCDutyDB = new LCDutyDB();
            String sqlStr = "select * from LCDuty where PolNo='" + mLJSPayPersonSet.get(i).getPolNo() + "'";
            System.out.println("查询保险责任表:" + sqlStr);
            LCDutySet tLCDutySet = tLCDutyDB.executeQuery(sqlStr);
            if (tLCDutyDB.mErrors.needDealError() == true)
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCDutyDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "InsideFeeFlow";
                tError.functionName = "queryLCDuty";
                tError.errorMessage = "查询保险责任表!";
                this.mErrors.addOneError(tError);
                mLCDutySet.clear();
                return false;
            }

            if (tLCDutySet.size() == 0)
            {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "InsideFeeFlow";
                tError.functionName = "queryLCDuty";
                tError.errorMessage = "查询保险责任表没有找到相关数据!";
                this.mErrors.addOneError(tError);
                mLCDutySet.clear();
                return false;
            }

            mLCDutySet.add(tLCDutySet);
        }
        return true;
    }

    private boolean dealData()
    {
        //1-应收总表和暂交费表数据填充实收总表
        mLJAPaySchema.setPayNo(mLJSPaySchema.getGetNoticeNo()); //交费收据号码
        mLJAPaySchema.setIncomeNo(mLJSPaySchema.getOtherNo()); //应收/实收编号
        mLJAPaySchema.setIncomeType(mLJSPaySchema.getOtherNoType()); //应收/实收编号类型
        mLJAPaySchema.setAppntNo(mLJSPaySchema.getAppntNo()); //  投保人客户号码
        mLJAPaySchema.setSumActuPayMoney(mLJSPaySchema.getSumDuePayMoney()); // 总实交金额
        mLJAPaySchema.setEnterAccDate(CurrentDate); // 到帐日期
        mLJAPaySchema.setPayDate(mLJSPaySchema.getPayDate()); //交费日期
        mLJAPaySchema.setConfDate(CurrentDate); //确认日期
        mLJAPaySchema.setApproveCode(mLJSPaySchema.getApproveCode()); //复核人编码
        mLJAPaySchema.setApproveDate(mLJSPaySchema.getApproveDate()); //  复核日期
        mLJAPaySchema.setSerialNo(serNo); //流水号
        mLJAPaySchema.setOperator(mGI.Operator); // 操作员
        mLJAPaySchema.setMakeDate(CurrentDate); //入机时间
        mLJAPaySchema.setMakeTime(CurrentTime); //入机时间
        mLJAPaySchema.setModifyDate(CurrentDate); //最后一次修改日期
        mLJAPaySchema.setModifyTime(CurrentTime); //最后一次修改时间
        mLJAPaySchema.setBankCode(mLJSPaySchema.getBankCode()); //银行编码
        mLJAPaySchema.setBankAccNo(mLJSPaySchema.getBankAccNo()); //银行帐号
        mLJAPaySchema.setRiskCode(mLJSPaySchema.getRiskCode()); // 险种编码
        mLJAPaySchema.setManageCom(mLJSPaySchema.getManageCom());
        mLJAPaySchema.setAgentCode(mLJSPaySchema.getAgentCode());
        mLJAPaySchema.setAgentGroup(mLJSPaySchema.getAgentGroup());
        mLJAPaySchema.setAgentCom(mLJSPaySchema.getAgentCom());
        mLJAPaySchema.setAgentType(mLJSPaySchema.getAgentType());

        //2-暂交费表核销标志置为1
        for (int i = 0; i < mLJTempFeeSet.size(); i++)
        {
            mLJTempFeeSet.get(i + 1).setConfFlag("1"); //核销标志置为1
        }

        //3-暂交费分类表，核销标志置为1
        mLJTempFeeClassSchema.setConfFlag("1"); //核销标志置为1
        mLJTempFeeClassSet.add(mLJTempFeeClassSchema);

        //4-应收个人表填充实收个人表
        int iMax = 0;
        LJAPayPersonSchema tLJAPayPersonSchema;
        iMax = mLJSPayPersonSet.size();
        LJSPayPersonSchema tLJSPayPersonSchema;
        for (int i = 1; i <= iMax; i++)
        {
            tLJSPayPersonSchema = new LJSPayPersonSchema();
            tLJAPayPersonSchema = new LJAPayPersonSchema();
            tLJSPayPersonSchema.setSchema(mLJSPayPersonSet.get(i).getSchema());

            tLJAPayPersonSchema.setPolNo(tLJSPayPersonSchema.getPolNo()); //保单号码
            tLJAPayPersonSchema.setPayCount(tLJSPayPersonSchema.getPayCount()); //第几次交费
            tLJAPayPersonSchema.setGrpPolNo(tLJSPayPersonSchema.getGrpPolNo()); //集体保单号码
            tLJAPayPersonSchema.setContNo(tLJSPayPersonSchema.getContNo()); //总单/合同号码
            tLJAPayPersonSchema.setAppntNo(tLJSPayPersonSchema.getAppntNo()); //投保人客户号码
            tLJAPayPersonSchema.setPayNo(mLJAPaySchema.getPayNo()); //交费收据号码
            tLJAPayPersonSchema.setPayAimClass(tLJSPayPersonSchema.getPayAimClass()); //交费目的分类
            tLJAPayPersonSchema.setDutyCode(tLJSPayPersonSchema.getDutyCode()); //责任编码
            tLJAPayPersonSchema.setPayPlanCode(tLJSPayPersonSchema.getPayPlanCode()); //交费计划编码
            tLJAPayPersonSchema.setSumDuePayMoney(tLJSPayPersonSchema.getSumDuePayMoney()); //总应交金额
            tLJAPayPersonSchema.setSumActuPayMoney(tLJSPayPersonSchema.getSumActuPayMoney()); //总实交金额
            tLJAPayPersonSchema.setPayIntv(tLJSPayPersonSchema.getPayIntv()); //交费间隔
            tLJAPayPersonSchema.setPayDate(tLJSPayPersonSchema.getPayDate()); //交费日期
            tLJAPayPersonSchema.setPayType(tLJSPayPersonSchema.getPayType()); //交费类型
            tLJAPayPersonSchema.setEnterAccDate(CurrentDate); //到帐日期
            tLJAPayPersonSchema.setConfDate(CurrentDate); //确认日期
            tLJAPayPersonSchema.setLastPayToDate(tLJSPayPersonSchema.getLastPayToDate()); //原交至日期
            tLJAPayPersonSchema.setCurPayToDate(tLJSPayPersonSchema.getCurPayToDate()); //现交至日期
            tLJAPayPersonSchema.setInInsuAccState(tLJSPayPersonSchema.getInInsuAccState()); //转入保险帐户状态
            tLJAPayPersonSchema.setApproveCode(tLJSPayPersonSchema.getApproveCode()); //复核人编码
            tLJAPayPersonSchema.setApproveDate(tLJSPayPersonSchema.getApproveDate()); //复核日期
            tLJAPayPersonSchema.setSerialNo(serNo); //流水号
            tLJAPayPersonSchema.setOperator(tLJSPayPersonSchema.getOperator()); //操作员
            tLJAPayPersonSchema.setMakeDate(CurrentDate); //入机日期
            tLJAPayPersonSchema.setMakeTime(CurrentTime); //入机时间
            tLJAPayPersonSchema.setGetNoticeNo(tLJSPayPersonSchema.getGetNoticeNo()); //通知书号码
            tLJAPayPersonSchema.setModifyDate(CurrentDate); //最后一次修改日期
            tLJAPayPersonSchema.setModifyTime(CurrentTime); //最后一次修改时间
            tLJAPayPersonSchema.setManageCom(tLJSPayPersonSchema.getManageCom()); //管理机构
            tLJAPayPersonSchema.setAgentCom(tLJSPayPersonSchema.getAgentCom()); //代理机构
            tLJAPayPersonSchema.setAgentType(tLJSPayPersonSchema.getAgentType()); //代理机构内部分类
            tLJAPayPersonSchema.setRiskCode(tLJSPayPersonSchema.getRiskCode()); //险种编码

            mLJAPayPersonSet.add(tLJAPayPersonSchema);

            //5-更新保单表字段
            //实收款=actuMoney
            //1 ZC=100 YEL=0 YET=0  actuMoney=100
            //2 ZC=100 YEL=-50 YET=0 actuMoney=50
            //3 ZC=100 YEL=-200 YET=100 actuMoney=0
            double newLeavingMoney = 0; //余额
            double actuMoney = 0; //实交金额
            for (int j = 0; j < mLCPolSet.size(); j++)
            {
                //得到个人实际交款总和:交费方式为ZC的纪录
                if (tLJSPayPersonSchema.getPayType().equals("ZC"))
                {
                    actuMoney = tLJSPayPersonSchema.getSumDuePayMoney();
                }
                if (tLJSPayPersonSchema.getPayType().equals("YET")) //存放新余额的应收纪录
                {
                    newLeavingMoney = tLJSPayPersonSchema.getSumActuPayMoney();
                }

                if (mLCPolSet.get(j + 1).getPolNo().equals(tLJAPayPersonSchema.getPolNo()))
                {
                    mLCPolSet.get(j + 1).setPaytoDate(tLJSPayPersonSchema.getCurPayToDate()); //交至日期
                    mLCPolSet.get(j + 1).setSumPrem(mLCPolSet.get(j + 1).getSumPrem() + actuMoney); //总累计保费

                    mLCPolSet.get(j + 1).setLeavingMoney(newLeavingMoney); //取应收纪录中交费方式为“YET”的金额
                    mLCPolSet.get(j + 1).setModifyDate(CurrentDate); //最后一次修改日期
                    mLCPolSet.get(j + 1).setModifyTime(CurrentTime); //最后一次修改时间

                    break;
                }
            }

            //6-更新保费项表字段
            LCPremSchema tLCPremSchema = new LCPremSchema();
            for (int num = 1; num <= mLCPremSet.size(); num++)
            {
                if (!mLJSPayPersonSet.get(i).getPolNo().equals(mLCPremSet.get(num).getPolNo())) {
                    continue;
                }
                tLJSPayPersonSchema = mLJSPayPersonSet.get(i);

                tLCPremSchema = mLCPremSet.get(num);
                tLCPremSchema.setPayTimes(tLCPremSchema.getPayTimes() + 1); //已交费次数
                //tLCPremSchema.setPrem(tLJSPayPersonSchema.getSumActuPayMoney());//实际保费
                tLCPremSchema.setSumPrem(tLCPremSchema.getSumPrem() + tLCPremSchema.getPrem()); //累计保费
                tLCPremSchema.setPaytoDate(tLJSPayPersonSchema.getCurPayToDate()); //交至日期
                tLCPremSchema.setModifyDate(CurrentDate); //最后一次修改日期
                tLCPremSchema.setModifyTime(CurrentTime); //最后一次修改时间
                mLCPremNewSet.add(tLCPremSchema);
                //end if

                //6-2更新保险责任表
                for (int j=0; j<mLCDutySet.size(); j++) {
                    LCDutySchema tLCDutySchema = mLCDutySet.get(j+1);

                    if (tLCPremSchema.getPolNo().equals(tLCDutySchema.getPolNo())
                        && tLCPremSchema.getDutyCode().equals(tLCDutySchema.getDutyCode()))
                    {
                        tLCDutySchema.setPrem(tLCPremSchema.getPrem()); //实际保费
                        tLCDutySchema.setSumPrem(tLCDutySchema.getSumPrem() + tLCPremSchema.getPrem()); //累计保费
                        tLCDutySchema.setPaytoDate(tLCPremSchema.getPaytoDate()); //交至日期
                        tLCDutySchema.setModifyDate(CurrentDate); //最后一次修改日期
                        tLCDutySchema.setModifyTime(CurrentTime); //最后一次修改时间
                        break;
                    }
                }
            }
        }

        //end for
        return true;
    }

    /**
     * 查询财务实付表
     * @return
     */
    private boolean QyeryLJFIGet()
    {
        String PayMode = "5";
        if (ActuGetNo == null)
        {
            CError tError = new CError();
            tError.moduleName = "InsideFeeFlow";
            tError.functionName = "queryLJFIGet";
            tError.errorMessage = "实付号码不能为空!";
            this.mErrors.addOneError(tError);
            return true;
        }

        String sqlStr = "select * from LJFIGet where ActuGetNo='" + ActuGetNo + "'";
        sqlStr = sqlStr + " and PayMode='" + PayMode + "'";
        System.out.println("查询财务给付表:" + sqlStr);
        LJFIGetDB tLJFIGetDB = new LJFIGetDB();
        LJFIGetSet tLJFIGetSet = tLJFIGetDB.executeQuery(sqlStr);

        if (tLJFIGetDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLJFIGetDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "InsideFeeFlow";
            tError.functionName = "queryLJFIGet";
            tError.errorMessage = "财务给付表查询失败!";
            this.mErrors.addOneError(tError);
            tLJFIGetSet.clear();
            return true;
        }

        if (tLJFIGetSet.size() > 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "InsideFeeFlow";
            tError.functionName = "queryLJFIGet";
            tError.errorMessage = "财务给付表中实付号码为：" + ActuGetNo + " 的纪录已经存在！";
            this.mErrors.addOneError(tError);
            return true;
        }

        return false;
    }

    /**
     * 查询实付总表
     * @return
     */
    private LJAGetSchema QyeryLJAGet()
    {
        if ((ActuGetNo == null) || ActuGetNo.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "InsideFeeFlow";
            tError.functionName = "QyeryLJAGet";
            tError.errorMessage = "实付号码不能为空!";
            this.mErrors.addOneError(tError);
            return null;
        }
        String sqlStr = "select * from LJAGet where ActuGetNo='" + ActuGetNo + "'";
        System.out.println("查询实付总表:" + sqlStr);
        LJAGetSet tLJAGetSet = new LJAGetSet();
        LJAGetSchema tLJAGetSchema = new LJAGetSchema();
        LJAGetDB tLJAGetDB = new LJAGetDB();
        tLJAGetSet = tLJAGetDB.executeQuery(sqlStr);
        if (tLJAGetDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLJAGetDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "InsideFeeFlow";
            tError.functionName = "queryLJAGet";
            tError.errorMessage = "实付总表查询失败!";
            this.mErrors.addOneError(tError);
            tLJAGetSet.clear();
            return null;
        }
        if (tLJAGetSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "InsideFeeFlow";
            tError.functionName = "queryLJAGet";
            tError.errorMessage = "实付号码为：" + ActuGetNo + " 的实付总表纪录不存在！";
            this.mErrors.addOneError(tError);
            return null;
        }
        return tLJAGetSet.get(1);
    }

    /**
     * 查询给付表(生存领取_实付)
     * @param LJAGetBL
     * @return
     */
    private LJAGetDrawSet updateLJAGetDraw(LJAGetSchema mLJAGetSchema)
    {
        if (mLJAGetSchema == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OperFinFeeGetBL";
            tError.functionName = "updateLJAGetDraw";
            tError.errorMessage = "传入参数不能为空！";
            this.mErrors.addOneError(tError);
            return null;
        }
        String sqlStr = "select * from LJAGetDraw where ActuGetNo='" + mLJAGetSchema.getActuGetNo()
            + "'";
        System.out.println("查询给付表(生存领取_实付):" + sqlStr);
        LJAGetDrawSet tLJAGetDrawSet = new LJAGetDrawSet();
        LJAGetDrawSchema tLJAGetDrawSchema = new LJAGetDrawSchema();
        LJAGetDrawDB tLJAGetDrawDB = new LJAGetDrawDB();
        tLJAGetDrawSet = tLJAGetDrawDB.executeQuery(sqlStr);
        if (tLJAGetDrawDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLJAGetDrawDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "OperFinFeeGetBL";
            tError.functionName = "queryLJAGetDraw";
            tError.errorMessage = "给付表(生存领取_实付)查询失败!";
            this.mErrors.addOneError(tError);
            tLJAGetDrawSet.clear();
            return null;
        }
        LJAGetDrawSet newLJAGetDrawSet = new LJAGetDrawSet();
        for (int n = 1; n <= tLJAGetDrawSet.size(); n++)
        {
            tLJAGetDrawSchema = new LJAGetDrawSchema();
            tLJAGetDrawSchema = tLJAGetDrawSet.get(n);
            tLJAGetDrawSchema.setConfDate(mLJAGetSchema.getConfDate());
            tLJAGetDrawSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
            tLJAGetDrawSchema.setModifyDate(CurrentDate);
            tLJAGetDrawSchema.setModifyTime(CurrentTime);
            newLJAGetDrawSet.add(tLJAGetDrawSchema);
        }
        if (newLJAGetDrawSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OperFinFeeGetBL";
            tError.functionName = "updateLJAGetDraw";
            tError.errorMessage = "没有可以更新的给付表(生存领取_实付)！";
            this.mErrors.addOneError(tError);
            return null;
        }
        return newLJAGetDrawSet;
    }

    /**
     * 查询批改补退费表(实收/实付)
     * @param LJAGetBL
     * @return
     */
    private boolean updateLJAGetEndorse(LJAGetSchema mLJAGetSchema)
    {
        if (mLJAGetSchema == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OperFinFeeGetBL";
            tError.functionName = "updateLJAGetEndorse";
            tError.errorMessage = "传入参数不能为空！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (mLJAGetEndorseSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "InsideFeeFlow";
            tError.functionName = "updateLJAGetEndorse";
            tError.errorMessage = "没有可以更新的批改补退费表(实收/实付)！";
            this.mErrors.addOneError(tError);
            return false;
        }

        for (int n = 1; n <= mLJAGetEndorseSet.size(); n++)
        {
            mLJAGetEndorseSet.get(n).setGetConfirmDate(mLJAGetSchema.getConfDate());
            mLJAGetEndorseSet.get(n).setEnterAccDate(mLJAGetSchema.getEnterAccDate());
            mLJAGetEndorseSet.get(n).setModifyDate(CurrentDate);
            mLJAGetEndorseSet.get(n).setModifyTime(CurrentTime);
        }

        return true;
    }

    /**
     * 查询其他退费实付表
     * @param LJAGetBL
     * @return
     */
    private LJAGetOtherSet updateLJAGetOther(LJAGetSchema mLJAGetSchema)
    {
        if (mLJAGetSchema == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OperFinFeeGetBL";
            tError.functionName = "updateLJAGetOther";
            tError.errorMessage = "传入参数不能为空！";
            this.mErrors.addOneError(tError);
            return null;
        }
        String sqlStr = "select * from LJAGetOther where ActuGetNo='"
            + mLJAGetSchema.getActuGetNo() + "'";
        System.out.println("其他退费实付表:" + sqlStr);
        LJAGetOtherSet tLJAGetOtherSet = new LJAGetOtherSet();
        LJAGetOtherSchema tLJAGetOtherSchema = new LJAGetOtherSchema();
        LJAGetOtherDB tLJAGetOtherDB = new LJAGetOtherDB();
        tLJAGetOtherSet = tLJAGetOtherDB.executeQuery(sqlStr);
        if (tLJAGetOtherDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLJAGetOtherDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "OperFinFeeGetBL";
            tError.functionName = "updateLJAGetOther";
            tError.errorMessage = "其他退费实付表查询失败!";
            this.mErrors.addOneError(tError);
            tLJAGetOtherSet.clear();
            return null;
        }
        LJAGetOtherSet newLJAGetOtherSet = new LJAGetOtherSet();
        for (int n = 1; n <= tLJAGetOtherSet.size(); n++)
        {
            tLJAGetOtherSchema = new LJAGetOtherSchema();
            tLJAGetOtherSchema = tLJAGetOtherSet.get(n);
            tLJAGetOtherSchema.setConfDate(mLJAGetSchema.getConfDate());
            tLJAGetOtherSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
            tLJAGetOtherSchema.setModifyDate(CurrentDate);
            tLJAGetOtherSchema.setModifyTime(CurrentTime);
            newLJAGetOtherSet.add(tLJAGetOtherSchema);
        }
        if (newLJAGetOtherSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OperFinFeeGetBL";
            tError.functionName = "updateLJAGetOther";
            tError.errorMessage = "没有可以更新的其他退费实付表！";
            this.mErrors.addOneError(tError);
            return null;
        }
        return newLJAGetOtherSet;
    }

    /**
      * 赔付实付表
      * @param LJAGetSchema
      * @return
      */
    private LJAGetClaimSet updateLJAGetClaim(LJAGetSchema mLJAGetSchema)
    {
        if (mLJAGetSchema == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OperFinFeeGetBL";
            tError.functionName = "updateLJAGetClaim";
            tError.errorMessage = "传入参数不能为空！";
            this.mErrors.addOneError(tError);
            return null;
        }
        String sqlStr = "select * from LJAGetClaim where ActuGetNo='"
            + mLJAGetSchema.getActuGetNo() + "'";
        System.out.println("赔付实付表:" + sqlStr);
        LJAGetClaimSet tLJAGetClaimSet = new LJAGetClaimSet();
        LJAGetClaimSchema tLJAGetClaimSchema = new LJAGetClaimSchema();
        LJAGetClaimDB tLJAGetClaimDB = new LJAGetClaimDB();
        tLJAGetClaimSet = tLJAGetClaimDB.executeQuery(sqlStr);
        if (tLJAGetClaimDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLJAGetClaimDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "OperFinFeeGetBL";
            tError.functionName = "updateLJAGetClaim";
            tError.errorMessage = "赔付实付表查询失败!";
            this.mErrors.addOneError(tError);
            tLJAGetClaimSet.clear();
            return null;
        }
        LJAGetClaimSet newLJAGetClaimSet = new LJAGetClaimSet();
        for (int n = 1; n <= tLJAGetClaimSet.size(); n++)
        {
            tLJAGetClaimSchema = new LJAGetClaimSchema();
            tLJAGetClaimSchema = tLJAGetClaimSet.get(n);
            tLJAGetClaimSchema.setConfDate(mLJAGetSchema.getConfDate());
            tLJAGetClaimSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
            tLJAGetClaimSchema.setModifyDate(CurrentDate);
            tLJAGetClaimSchema.setModifyTime(CurrentTime);
            newLJAGetClaimSet.add(tLJAGetClaimSchema);
        }
        if (newLJAGetClaimSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OperFinFeeGetBL";
            tError.functionName = "updateLJAGetClaim";
            tError.errorMessage = "没有可以更新的赔付实付表！";
            this.mErrors.addOneError(tError);
            return null;
        }
        return newLJAGetClaimSet;
    }

    /**
      * 暂交费退费实付表
      * @param LJAGetSchema
      * @return
      */
    private LJAGetTempFeeSet updateLJAGetTempFee(LJAGetSchema mLJAGetSchema)
    {
        if (mLJAGetSchema == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OperFinFeeGetBL";
            tError.functionName = "updateLJAGetTempFee";
            tError.errorMessage = "传入参数不能为空！";
            this.mErrors.addOneError(tError);
            return null;
        }
        String sqlStr = "select * from LJAGetTempFee where ActuGetNo='"
            + mLJAGetSchema.getActuGetNo() + "'";
        System.out.println("暂交费退费实付表:" + sqlStr);
        LJAGetTempFeeSet tLJAGetTempFeeSet = new LJAGetTempFeeSet();
        LJAGetTempFeeSchema tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
        LJAGetTempFeeDB tLJAGetTempFeeDB = new LJAGetTempFeeDB();
        tLJAGetTempFeeSet = tLJAGetTempFeeDB.executeQuery(sqlStr);
        if (tLJAGetTempFeeDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLJAGetTempFeeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "OperFinFeeGetBL";
            tError.functionName = "updateLJAGetTempFee";
            tError.errorMessage = "暂交费退费实付表查询失败!";
            this.mErrors.addOneError(tError);
            tLJAGetTempFeeSet.clear();
            return null;
        }
        LJAGetTempFeeSet newLJAGetTempFeeSet = new LJAGetTempFeeSet();
        for (int n = 1; n <= tLJAGetTempFeeSet.size(); n++)
        {
            tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
            tLJAGetTempFeeSchema = tLJAGetTempFeeSet.get(n);
            tLJAGetTempFeeSchema.setConfDate(mLJAGetSchema.getConfDate());
            tLJAGetTempFeeSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
            tLJAGetTempFeeSchema.setModifyDate(CurrentDate);
            tLJAGetTempFeeSchema.setModifyTime(CurrentTime);
            newLJAGetTempFeeSet.add(tLJAGetTempFeeSchema);
        }
        if (newLJAGetTempFeeSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OperFinFeeGetBL";
            tError.functionName = "updateLJAGetTempFee";
            tError.errorMessage = "没有可以更新的暂交费退费实付表！";
            this.mErrors.addOneError(tError);
            return null;
        }
        return newLJAGetTempFeeSet;
    }

    /**
      * 红利给付实付表
      * @param LJAGetSchema
      * @return
      */
    private LJABonusGetSet updateLJABonusGet(LJAGetSchema mLJAGetSchema)
    {
        if (mLJAGetSchema == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OperFinFeeGetBL";
            tError.functionName = "updateLJABonusGet";
            tError.errorMessage = "传入参数不能为空！";
            this.mErrors.addOneError(tError);
            return null;
        }
        String sqlStr = "select * from LJABonusGet where ActuGetNo='"
            + mLJAGetSchema.getActuGetNo() + "'";
        System.out.println("红利给付实付表:" + sqlStr);
        LJABonusGetSet tLJABonusGetSet = new LJABonusGetSet();
        LJABonusGetSchema tLJABonusGetSchema = new LJABonusGetSchema();
        LJABonusGetDB tLJABonusGetDB = new LJABonusGetDB();
        tLJABonusGetSet = tLJABonusGetDB.executeQuery(sqlStr);
        if (tLJABonusGetDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLJABonusGetDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "OperFinFeeGetBL";
            tError.functionName = "updateLJABonusGet";
            tError.errorMessage = "红利给付实付表查询失败!";
            this.mErrors.addOneError(tError);
            tLJABonusGetSet.clear();
            return null;
        }
        LJABonusGetSet newLJABonusGetSet = new LJABonusGetSet();
        for (int n = 1; n <= tLJABonusGetSet.size(); n++)
        {
            tLJABonusGetSchema = new LJABonusGetSchema();
            tLJABonusGetSchema = tLJABonusGetSet.get(n);
            tLJABonusGetSchema.setConfDate(mLJAGetSchema.getConfDate());
            tLJABonusGetSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
            tLJABonusGetSchema.setModifyDate(CurrentDate);
            tLJABonusGetSchema.setModifyTime(CurrentTime);
            newLJABonusGetSet.add(tLJABonusGetSchema);
        }
        if (newLJABonusGetSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OperFinFeeGetBL";
            tError.functionName = "updateLJABonusGet";
            tError.errorMessage = "没有可以更新的红利给付实付表！";
            this.mErrors.addOneError(tError);
            return null;
        }
        return newLJABonusGetSet;
    }
}
