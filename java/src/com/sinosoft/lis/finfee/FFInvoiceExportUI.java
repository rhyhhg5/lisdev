package com.sinosoft.lis.finfee;

import java.util.List;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * 发票导出类
 * 
 * @author 张成轩
 */
public class FFInvoiceExportUI {
	public CErrors mErrors = new CErrors();

	private List mResult;

	public FFInvoiceExportUI() {
	}

	public boolean submitData(VData cInputData, String cOperate,
			String cManagecom) {
		try {
			if (!cOperate.equals("EXPORT")) {
				buildError("submitData", "不支持的操作字符串");
				return false;
			}

			// 获取机构的处理类
			LDCodeDB tLDCodeDB = new LDCodeDB();
			tLDCodeDB.setCodeType("invoiceEx");
			if (cManagecom != null && cManagecom.length() >= 4) {
				tLDCodeDB.setCode(cManagecom.substring(0, 4));
			} else {
				tLDCodeDB.setCode("default");
			}

			if (!tLDCodeDB.getInfo()) {
				buildError("submitData", "获取处理类失败，该机构尚未配置相关导出方式");
				return false;
			}

			Class cls = Class.forName(tLDCodeDB.getCodeAlias());
			InvoiceExport tFFIvoiceExportBL = (InvoiceExport) cls.newInstance();
			
			System.out.println("Start FFIvoiceExportBL Submit ...");
			if (!tFFIvoiceExportBL.submitData(cInputData, cOperate)) {
				if (tFFIvoiceExportBL.getErrors().needDealError()) {
					mErrors.copyAllErrors(tFFIvoiceExportBL.getErrors());
					return false;
				} else {
					buildError("submitData",
							"FFIvoiceExportBL 发生错误，但是没有提供详细的出错信息");
					return false;
				}
			} else {
				mResult = tFFIvoiceExportBL.getResult();
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			buildError("submitData", "意外错误");
			return false;
		}
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "FFIvoiceExportUI";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public List getResult() {
		return mResult;
	}

	public static void main(String[] args) {
	}
}
