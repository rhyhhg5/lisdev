package com.sinosoft.lis.finfee;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.db.LYReturnFromBankBDB;
import com.sinosoft.lis.vschema.LYReturnFromBankBSet;
import com.sinosoft.utility.StrTool;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import java.util.GregorianCalendar;
import com.sinosoft.lis.pubfun.FDate;
import java.util.Date;
import java.text.SimpleDateFormat;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.schema.LJSPaySchema;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class FirstHastenBL {

    /** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 返回数据的容器 */
    private VData mResult = new VData();
    /** 提交数据的容器 */
    private MMap map = new MMap();

    private TransferData inTransferData = new TransferData();

    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();
    /** 前台传入的公共变量 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mManageCom;

    LOPRTManagerSchema tLOPRTManager = new LOPRTManagerSchema();
    LOPRTManagerDB tLOPRTMangerDB = new LOPRTManagerDB();
    LOPRTManagerSchema tLOPRTMangerSchema=new LOPRTManagerSchema();


    public FirstHastenBL() {
    }

    public static void main(String[] args) {
        FirstHastenBL tFirstHastenBL = new FirstHastenBL();

        TransferData transferData1 = new TransferData();
        transferData1.setNameAndValue("PRTNO", "16000000539");
        transferData1.setNameAndValue("CUSTAGENT", "1101000012");
        transferData1.setNameAndValue("CUSTCOM", "8611");
        transferData1.setNameAndValue("PRTSEQ", "81000000717");

        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "001";

        VData inVData = new VData();
        inVData.add(transferData1);
        inVData.add(tGlobalInput);

        if (!tFirstHastenBL.submitData(inVData, "INSERT")) {
            System.out.println("Submit Failed!");
        } else {
            System.out.println("Submit Succed!");
        }

    }

    public boolean submitData(VData cInputData, String cOperate) {
        this.mOperate = cOperate;
        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("---FirstHastenBL getInputData---");
        //校验
        //if (!checkPrint())
        //    return false;
        System.out.println("---FirstHastenBL checkData---");
        // 数据操作业务处理
        if (!dealData()) {
            return false;
        }
        System.out.println("---FirstHastenBL dealData---");

        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("---FirstHastenBL prepareOutputData---");

        PubSubmit tPubSubmit = new PubSubmit();

        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            mResult.clear();
            return false;
        }
        System.out.println("End PubSubmit BLS Submit...");

        //准备给后台的数据
        //prepareOutputData();
        System.out.println("---FirstHastenBL prepareOutputData---");
        return true;
    }

    private boolean getInputData(VData cInputData) {
        try {
            inTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);

        } catch (Exception e) {
            // @@错误处理
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    private boolean checkPrint() {
        return true;
    }
    private String getDate(Date date) {

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(date);
    }
    private boolean dealData() {
        System.out.println("Come to dealData()...................");
        String prtNo = (String) inTransferData.getValueByName("PRTNO");
//        String custAgent = (String) inTransferData.getValueByName("CUSTAGENT");
//        String custCom = (String) inTransferData.getValueByName("CUSTCOM");
 //       String prtContSeq = (String) inTransferData.getValueByName("PRTSEQ");
//        String BankCode = (String) inTransferData.getValueByName("BankCode");

        System.out.println("****** " + prtNo + " *********** ");

        String comCode = mGlobalInput.ComCode;
        String operator = mGlobalInput.Operator;
        String currentDate = PubFun.getCurrentDate();
        String currentTime = PubFun.getCurrentTime();

        LCContDB tLCContDB=new LCContDB();
        LCContSet tLCContSet=new LCContSet();
        tLCContDB.setPrtNo(prtNo);
        tLCContSet=tLCContDB.query();
        if(tLCContSet.size()<=0){
            CError.buildErr(this, "取合同表数据失败!");
        }

        LOPRTManagerDB mLOPRTManagerDB=new LOPRTManagerDB();
        LOPRTManagerSet mLOPRTManagerSet=new LOPRTManagerSet();
        mLOPRTManagerDB.setOtherNo(prtNo);
        mLOPRTManagerSet=mLOPRTManagerDB.query();
        if(mLOPRTManagerSet.size()>=0)
        {
            int Printcount = mLOPRTManagerSet.size()+1;
            String prtSeq = PubFun1.CreateMaxNo("PRTSEQNO", null);
            System.out.println("prtSeq " + prtSeq);
            tLOPRTManager.setPrtSeq(prtSeq);

            tLOPRTManager.setOtherNo(prtNo);
            tLOPRTManager.setOtherNoType("05");
            if(StrTool.cTrim(tLCContSet.get(1).getPayMode()).equals("1")){
            tLOPRTManager.setCode("75"); //首期现金催缴通知书
            }else{
            tLOPRTManager.setCode("65"); //首期转帐催缴通知书
            }
            tLOPRTManager.setManageCom(tLCContSet.get(1).getManageCom());
            tLOPRTManager.setAgentCode(tLCContSet.get(1).getAgentCode());
            tLOPRTManager.setReqCom(comCode);
            tLOPRTManager.setReqOperator(operator);
            tLOPRTManager.setPrtType("0");
            tLOPRTManager.setStateFlag("0"); //提交状态
            tLOPRTManager.setMakeDate(currentDate);
            tLOPRTManager.setMakeTime(currentTime);
            tLOPRTManager.setPrintTimes(0);
            tLOPRTManager.setStandbyFlag1(String.valueOf(Printcount));
            //修改首期保费通知书
            LOPRTManagerSet tLOPRTManagerSet=new LOPRTManagerSet();
            String SQL="select * from loprtmanager where code='07' and otherno='"+tLCContSet.get(1).getProposalContNo()+"' order by makedate desc ";
            tLOPRTManagerSet=tLOPRTMangerDB.executeQuery(SQL);
            if (tLOPRTManagerSet.size()<=0) {
                CError.buildErr(this, "修改首期保费通知书发送状态失败");
                return false;
            }
            tLOPRTMangerSchema=tLOPRTManagerSet.get(1);
            tLOPRTMangerSchema.setForMakeDate(PubFun.calDate(currentDate,5,"D",null));
            tLOPRTMangerSchema.setForMakeTime(currentTime);

        }
        if(StrTool.cTrim(tLCContSet.get(1).getPayMode()).equals("4")){
                //更新LJSPay中的CanSendBank字段
               LJSPayDB mljspayDB = new LJSPayDB();
               LJSPaySet mLJSPaySet=new LJSPaySet();
               LJSPaySchema mLJSPaySchema=new LJSPaySchema();
               mljspayDB.setOtherNo(prtNo);
               mLJSPaySet=mljspayDB.query();
               mLJSPaySchema=mLJSPaySet.get(1);
               mLJSPaySchema.setCanSendBank("2");
               mLJSPaySchema.setStartPayDate(PubFun.calDate(currentDate,5,"D",null));
               map.put(mLJSPaySchema, "UPDATE");
        }


        /*     if(mOperate.equals("INSERT"))
             {
                 String prtSeq = PubFun1.CreateMaxNo("PRTSEQNO",null);
                 System.out.println("prtSeq "+prtSeq);
                 tLOPRTManager.setPrtSeq(prtSeq);

                 tLOPRTManager.setOtherNo(prtNo);
                 tLOPRTManager.setOtherNoType("05");
                 tLOPRTManager.setCode("75"); //首期保费催缴通知书
                 tLOPRTManager.setManageCom(custCom);
                 tLOPRTManager.setAgentCode(custAgent);
                 tLOPRTManager.setReqCom(comCode);
                 tLOPRTManager.setReqOperator(operator);
                 tLOPRTManager.setPrtType("0");
                 tLOPRTManager.setStateFlag("0"); //提交状态
                 tLOPRTManager.setMakeDate(currentDate);
                 tLOPRTManager.setMakeTime(currentTime);
                 tLOPRTManager.setPrintTimes(0);

                 //修改首期保费通知书
                 tLOPRTMangerDB.setPrtSeq(prtContSeq);
                 tLOPRTMangerDB.getInfo();
                 if (!tLOPRTMangerDB.getInfo()) {
                     CError.buildErr(this, "修改首期保费通知书发送状态失败");
                     return false;
                 }
                 tLOPRTMangerDB.setStateFlag("3");
                 tLOPRTMangerDB.setForMakeDate(currentDate);
                 tLOPRTMangerDB.setForMakeTime(currentTime);
             }
         */
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * @return 如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...................");
        try {
            map.put(tLOPRTManager, "INSERT");
            map.put(tLOPRTMangerSchema, "UPDATE");
            mInputData.clear();
            mInputData.add(map);
        } catch (Exception ex) {
            // @@错误处理
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
            return false;
        }

        return true;
    }
    /**
      * 出错处理
      * @param szFunc String
      * @param szErrMsg String
      */
     private void buildError(String szFunc, String szErrMsg)
     {
         CError cError = new CError();
         cError.moduleName = "FirstHastenBL";
         cError.functionName = szFunc;
         cError.errorMessage = szErrMsg;
         this.mErrors.addOneError(cError);
    }

}
