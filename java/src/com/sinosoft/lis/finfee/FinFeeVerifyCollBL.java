package com.sinosoft.lis.finfee;

import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class FinFeeVerifyCollBL  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 数据操作字符串 */
  private String mOperate;

//暂收费表
  private LJTempFeeSet       mLJTempFeeSet         = new LJTempFeeSet();
  private LJTempFeeSet       mLJTempFeeNewSet      = new LJTempFeeSet();
//暂收费分类表
  private LJTempFeeClassSet  mLJTempFeeClassSet    = new LJTempFeeClassSet();
  private LJTempFeeClassSet  mLJTempFeeClassNewSet = new LJTempFeeClassSet();
//应收个人交费表
  private LJSPayPersonSet    mLJSPayPersonSet      = new LJSPayPersonSet();
//应收总表
  private LJSPayBL           mLJSPayBL             = new LJSPayBL();
//应收集体交费表
  private LJSPayGrpBL        mLJSPayGrpBL          = new LJSPayGrpBL();
//实收个人交费表
  private LJAPayPersonSet    mLJAPayPersonSet      = new LJAPayPersonSet();
//实收总表
  private LJAPayBL           mLJAPayBL             = new LJAPayBL();
//实收集体交费表
  private LJAPayGrpBL        mLJAPayGrpBL          = new LJAPayGrpBL();

  //业务处理相关变量
  public FinFeeVerifyCollBL() {
  }
  public static void main(String[] args) {

  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
System.out.println("After getinputdata");

    //进行业务处理
    if (!dealData())
      return false;
System.out.println("After dealData！");
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
System.out.println("After prepareOutputData");

    System.out.println("Start FinFeeVerifyColl BL Submit...");

    FinFeeVerifyCollBLS tFinFeeVerifyCollBLS=new FinFeeVerifyCollBLS();
    tFinFeeVerifyCollBLS.submitData(mInputData,cOperate);

    System.out.println("End LJFinFeeVerifyColl BL Submit...");

    //如果有需要处理的错误，则返回
    if (tFinFeeVerifyCollBLS.mErrors .needDealError())
    {
        this.mErrors .copyAllErrors(tFinFeeVerifyCollBLS.mErrors ) ;
    }

    mInputData=null;
    return true;
  }

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData()
  {
    int i,iMax;
    boolean tReturn =false;
    //添加纪录
    if(this.mOperate.equals("VERIFY"))
    {
//1-应收总表和暂交费表数据填充实收总表
    mLJAPayBL.setIncomeNo(mLJSPayBL.getOtherNo());     // 应收/实收编号
    mLJAPayBL.setIncomeType(mLJSPayBL.getOtherNoType());   //应收/实收编号类型
    mLJAPayBL.setAppntNo(mLJSPayBL.getAppntNo());      //  投保人客户号码
    mLJAPayBL.setPayNo(mLJSPayBL.getGetNoticeNo());                //交费收据号码
    mLJAPayBL.setSumActuPayMoney(mLJSPayBL.getSumDuePayMoney()); // 总实交金额
    mLJAPayBL.setEnterAccDate("2002-1-1");    // 到帐日期
    mLJAPayBL.setOperator("Operator");//操作员
    mLJAPayBL.setMakeDate("1999/1/1");//入机时间
    mLJAPayBL.setMakeTime("21:12:12");//入机时间
    mLJAPayBL.setModifyDate("1999/1/1"); //最后一次修改日期
    mLJAPayBL.setModifyTime("21:12:12"); //最后一次修改时间

//2-暂交费表核销标志置为1
    iMax=mLJTempFeeSet.size() ;
    LJTempFeeBL tLJTempFeeBL;
    for (i=1;i<=iMax;i++)
    {
      tLJTempFeeBL = new LJTempFeeBL() ;
      tLJTempFeeBL.setSchema(mLJTempFeeSet.get(i).getSchema());
      tLJTempFeeBL.setConfFlag("1");//核销标志置为1
      mLJTempFeeNewSet.add(tLJTempFeeBL);
      tReturn=true;
    }
//3-暂交费分类表，核销标志置为1
    iMax=mLJTempFeeClassSet.size() ;
    LJTempFeeClassBL tLJTempFeeClassBL;
    for (i=1;i<=iMax;i++)
    {
      tLJTempFeeClassBL = new LJTempFeeClassBL() ;
      tLJTempFeeClassBL.setSchema(mLJTempFeeClassSet.get(i).getSchema());
      tLJTempFeeClassBL.setConfFlag("1");//核销标志置为1
      mLJTempFeeClassNewSet.add(tLJTempFeeClassBL);
      tReturn=true;
    }
//4-应收个人表填充实收个人表

    LJSPayPersonBL tLJSPayPersonBL;
    LJAPayPersonBL tLJAPayPersonBL;
    iMax=mLJSPayPersonSet.size() ;
    for (i=1;i<=iMax;i++)
    {
      tLJSPayPersonBL = new LJSPayPersonBL();
      tLJAPayPersonBL = new LJAPayPersonBL();
      tLJSPayPersonBL.setSchema(mLJSPayPersonSet.get(i).getSchema());
      tLJAPayPersonBL.setPolNo(tLJSPayPersonBL.getPolNo());
      tLJAPayPersonBL.setPayCount(tLJSPayPersonBL.getPayCount());
      tLJAPayPersonBL.setGrpPolNo(tLJSPayPersonBL.getGrpPolNo());
      tLJAPayPersonBL.setContNo(tLJSPayPersonBL.getContNo());
      tLJAPayPersonBL.setPayPlanCode(tLJSPayPersonBL.getPayPlanCode());
      tLJAPayPersonBL.setDutyCode(tLJSPayPersonBL.getDutyCode());
      tLJAPayPersonBL.setMakeDate("2002-1-1");
      tLJAPayPersonBL.setMakeTime("12:12:12");
      tLJAPayPersonBL.setModifyDate("2002-1-1");
      tLJAPayPersonBL.setModifyTime("12:12:12");
      tLJAPayPersonBL.setOperator("new");
      tLJAPayPersonBL.setEnterAccDate("2002-1-1");

      mLJAPayPersonSet.add(tLJAPayPersonBL);
      tReturn=true;
    }
//4-应收集体交费表填充实收集体交费表
     mLJAPayGrpBL.setGrpPolNo(mLJSPayGrpBL.getGrpPolNo());
     mLJAPayGrpBL.setPayCount(mLJSPayGrpBL.getPayCount());
     //2005.1.20杨明注释掉问题代码mLJAPayGrpBL.setContNo(mLJSPayGrpBL.getContNo());
//     mLJAPayGrpBL.setAppntNo(mLJSPayGrpBL.getAppntNo());
     mLJAPayGrpBL.setSumDuePayMoney(mLJSPayGrpBL.getSumDuePayMoney());
     mLJAPayGrpBL.setSumActuPayMoney(mLJSPayGrpBL.getSumActuPayMoney());
//     mLJAPayGrpBL.setPayIntv(mLJSPayGrpBL.getPayIntv());
     mLJAPayGrpBL.setPayDate(mLJSPayGrpBL.getPayDate());
//     mLJAPayGrpBL.setLastPayToDate(mLJSPayGrpBL.getLastPayToDate());
//     mLJAPayGrpBL.setCurPayToDate(mLJSPayGrpBL.getCurPayToDate());
//     mLJAPayGrpBL.setApproveCode(mLJSPayGrpBL.getApproveCode());
//     mLJAPayGrpBL.setApproveDate(mLJSPayGrpBL.getApproveDate());
//     mLJAPayGrpBL.setSerialNo(mLJSPayGrpBL.getSerialNo());
     mLJAPayGrpBL.setMakeDate("2002-1-1");
     mLJAPayGrpBL.setMakeTime("12:12:12");
     mLJAPayGrpBL.setModifyDate("2002-1-1");
     mLJAPayGrpBL.setModifyTime("12:12:12");
     mLJAPayGrpBL.setOperator("hzm");
      tReturn=true;
  }
  return tReturn;
}

 /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData mInputData)
  {
    // 暂交费表
    mLJTempFeeSet.set((LJTempFeeSet)mInputData.getObjectByObjectName("LJTempFeeSet",0));
    // 暂交费分类表
    mLJTempFeeClassSet.set((LJTempFeeClassSet)mInputData.getObjectByObjectName("LJTempFeeClassSet",0));
    //应收个人交费表
    mLJSPayPersonSet.set((LJSPayPersonSet)mInputData.getObjectByObjectName("LJSPayPersonSet",0));
    //应收总表
    mLJSPayBL.setSchema((LJSPaySchema)mInputData.getObjectByObjectName("LJSPaySchema",0));
    //应收集体交费表
    mLJSPayGrpBL.setSchema((LJSPayGrpSchema)mInputData.getObjectByObjectName("LJSPayGrpSchema",0));

    if(mLJTempFeeSet ==null || mLJTempFeeClassSet ==null||mLJSPayBL==null||mLJSPayPersonSet==null||mLJSPayGrpBL==null )
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="FinFeeVerifyCollBL";
      tError.functionName="getInputData";
      tError.errorMessage="没有得到足够的数据，请您确认!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData()
  {
    mInputData=new VData();
    try
    {
    mInputData.add(mLJAPayBL);	//实收总表
    mInputData.add(mLJSPayBL);    //应收总表
    mInputData.add(mLJTempFeeNewSet);//暂交费表
    mInputData.add(mLJTempFeeClassNewSet);//暂交费分类表
    mInputData.add(mLJAPayPersonSet);//实收个人表
    mInputData.add(mLJSPayPersonSet);    //应收个人交费表
    mInputData.add(mLJSPayGrpBL);  //应收集体交费表
    mInputData.add(mLJAPayGrpBL);  //实收集体交费表
System.out.println("prepareOutputData:");
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="FinFeeVerifyCollBL";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }
}


