/**
 * 2007-5-26
 */
package com.sinosoft.lis.finfee;

import com.sinosoft.lis.schema.LJInvoiceInfoSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class FFInvoiceBaseInfoUI
{
    public CErrors mErrors = new CErrors();

    private VData mResult;

    public FFInvoiceBaseInfoUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("INSERT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            if (!checkData(cInputData))
            {
                buildError("submitData", "业务数据信息有误，请确认后重新填写。");
                return false;
            }

            FFInvoiceBaseInfoBL tFFInvoiceBaseInfoBL = new FFInvoiceBaseInfoBL();
            System.out.println("Start FFInvoiceBaseInfoUI Submit ...");
            if (!tFFInvoiceBaseInfoBL.submitData(cInputData, cOperate))
            {
                if (tFFInvoiceBaseInfoBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tFFInvoiceBaseInfoBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData",
                            "FFIvoiceExportBL 发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tFFInvoiceBaseInfoBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("submitData", "其他未知异常");
            return false;
        }
    }

    private boolean checkData(VData cInputData)
    {
        LJInvoiceInfoSchema tLJInvoiceInfoSchema = (LJInvoiceInfoSchema) cInputData
                .getObjectByObjectName("LJINVOICEINFOSchema", 0);
        if (tLJInvoiceInfoSchema == null)
            return false;
        if (tLJInvoiceInfoSchema.getComCode() == null)
            return false;
        if (tLJInvoiceInfoSchema.getTaxpayerNo() == null)
            return false;
        if (tLJInvoiceInfoSchema.getTaxpayerName() == null)
            return false;
        if (tLJInvoiceInfoSchema.getInvoiceCode() == null)
            return false;
        if (tLJInvoiceInfoSchema.getInvoiceCodeExp() == null)
            return false;
        else if (tLJInvoiceInfoSchema.getInvoiceCodeExp().equals(""))
            tLJInvoiceInfoSchema.setInvoiceCodeExp(tLJInvoiceInfoSchema
                    .getInvoiceCode());
        if (tLJInvoiceInfoSchema.getInvoiceStartNo() == null)
            return false;
        if (tLJInvoiceInfoSchema.getInvoiceEndNo() == null)
            return false;
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FFInvoiceBaseInfoSaveBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
    }

}
