package com.sinosoft.lis.finfee;

import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 预收保费费退费</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class GetAdvanceDrawBL {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mResult = new VData();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 数据操作字符串 */
  private String tLimit="";
  private String tNo=""; //生成的实付号码
  private String tSo=""; //生成的流水号
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  //为核保撤单增加
  private String mGetNoticeNo = "";
  private String mNotBLS = "";
  private String tPayMode = "";
  private GlobalInput tG                            = new GlobalInput();
  private LJTempFeeSet inLJTempFeeSet               = new LJTempFeeSet();
  private LJAGetSet outLJAGetSet                     = new LJAGetSet();
  private LJTempFeeSet outLJTempFeeSet              = new LJTempFeeSet();
  private LJTempFeeClassSet outLJTempFeeClassSet    = new LJTempFeeClassSet();
  private double money = 0.0;
  public GetAdvanceDrawBL() {
  }


  //传输数据的公共方法
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) return false;
    System.out.println("---End getInputData---");

    //进行业务处理
    if (!dealData()) return false;
    System.out.println("---End dealData---");

    //准备往后台的数据
    if (!prepareOutputData()) return false;
    System.out.println("---End prepareOutputData---");

    //为外部调用提供接口，外部传入该参数后，程序将不自动调用BLS，外部可通过getResult方法获取准备好的数据VData
    if (!mNotBLS.equals("")) {
      mResult = mInputData;
      return true;
    }
    return true;
  }

  /**
   * 设置实收总表
   * @param tLJTempFeeSchema
   */
  private boolean setLJAGet(LJTempFeeSchema tLJTempFeeSchema) {
    tLimit = PubFun.getNoLimit(tLJTempFeeSchema.getManageCom());
    tNo = PubFun1.CreateMaxNo("GETNO", tLimit);
    //添加一条实付总表纪录
    tSo=PubFun1.CreateMaxNo("SERIALNO",tLimit);
    LJAGetSchema outLJAGetSchema = new LJAGetSchema();

    outLJAGetSchema.setActuGetNo(tNo);
    outLJAGetSchema.setOtherNo(tLJTempFeeSchema.getOtherNo());
    outLJAGetSchema.setOtherNoType("YS");

    //暂时只有现金和银行转账两种形式
    outLJAGetSchema.setPayMode(tPayMode);
    outLJAGetSchema.setSumGetMoney(money);
    outLJAGetSchema.setSaleChnl(tLJTempFeeSchema.getSaleChnl());
    outLJAGetSchema.setManageCom(tLJTempFeeSchema.getManageCom());
    outLJAGetSchema.setAgentCom(tLJTempFeeSchema.getAgentCom());
    outLJAGetSchema.setAgentType(tLJTempFeeSchema.getAgentType());

    outLJAGetSchema.setSaleChnl(tLJTempFeeSchema.getSaleChnl());
    outLJAGetSchema.setShouldDate(PubFun.getCurrentDate());
    outLJAGetSchema.setAgentCode(tLJTempFeeSchema.getAgentCode());
    outLJAGetSchema.setAgentGroup(tLJTempFeeSchema.getAgentGroup());
    outLJAGetSchema.setStartGetDate(PubFun.getCurrentDate());

    outLJAGetSchema.setSerialNo(tSo);
    outLJAGetSchema.setOperator(tG.Operator);
    outLJAGetSchema.setMakeDate(CurrentDate);
    outLJAGetSchema.setMakeTime(CurrentTime);
    outLJAGetSchema.setModifyDate(CurrentDate);
    outLJAGetSchema.setModifyTime(CurrentTime);
    //设置给付通知书号
    outLJAGetSchema.setGetNoticeNo(mGetNoticeNo);

      //查询所有的分类表，看是否有银行付款的方式，如果有，就用银行退费
      LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
      tLJTempFeeClassDB.setTempFeeNo(tLJTempFeeSchema.getTempFeeNo());
      LJTempFeeClassSet tLJTempFeeClassSet = tLJTempFeeClassDB.query();
      if (tLJTempFeeClassSet.size() == 0) {
          return false;
      }
      
      boolean chequeFlag = false;
      LJTempFeeClassSchema tLJTempFeeClassSchema = null;

      for (int i=0; i<tLJTempFeeClassSet.size(); i++) {
        //支票(2-现金支票,3-转账支票)，优先级最高，有则全部按该方式退费
        if (tLJTempFeeClassSet.get(i+1).getPayMode().equals("2")
            || tLJTempFeeClassSet.get(i+1).getPayMode().equals("3")) {
          chequeFlag = true;
          tLJTempFeeClassSchema = tLJTempFeeClassSet.get(i+1);
          break;
        }
      }

      //支票，优先级最高
      if (chequeFlag) {
        outLJAGetSchema.setPayMode(tLJTempFeeClassSchema.getPayMode());
        outLJAGetSchema.setBankCode(tLJTempFeeClassSchema.getBankCode());
        outLJAGetSchema.setBankAccNo(tLJTempFeeClassSchema.getChequeNo());
      }
      
      if(tPayMode == null || tPayMode.equals("")){
          outLJAGetSchema.setPayMode(tLJTempFeeClassSchema.getPayMode());
      } else {
          outLJAGetSchema.setPayMode(tPayMode);
      }
      

    outLJAGetSet.add(outLJAGetSchema);
    return true;
  }

  /**
   * 核销处理
   * @param tLJTempFeeSchema
   */
  private void confTempFee(LJTempFeeSchema tLJTempFeeSchema) {
    //核销暂交费表
    
    tLJTempFeeSchema.setTempFeeNo(tLJTempFeeSchema.getTempFeeNo());
    LJTempFeeSet conLJTempFeeSet = tLJTempFeeSchema.getDB().query();
    this.money = 0.0;
    for (int i=0; i<conLJTempFeeSet.size(); i++) {
      LJTempFeeSchema conLJTempFeeSchema = new LJTempFeeSchema();
      conLJTempFeeSchema = conLJTempFeeSet.get(i+1);
      conLJTempFeeSchema.setConfFlag("1");
      conLJTempFeeSchema.setConfDate(PubFun.getCurrentDate());
      conLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
      conLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
      this.money = this.money + conLJTempFeeSet.get(i+1).getPayMoney();
      outLJTempFeeSet.add(conLJTempFeeSchema);
      
//    核销暂交费分类表
      LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
      tLJTempFeeClassSchema.setTempFeeNo(conLJTempFeeSchema.getTempFeeNo());
      LJTempFeeClassSet tLJTempFeeClassSet = tLJTempFeeClassSchema.getDB().query();
      for (int j=0; j<tLJTempFeeClassSet.size(); j++) {
        tLJTempFeeClassSet.get(j+1).setConfFlag("1");
        tLJTempFeeClassSet.get(j+1).setConfDate(PubFun.getCurrentDate());
      }

      outLJTempFeeClassSet.add(tLJTempFeeClassSet);
    }
    System.out.println("剩余金额：" + money);
    

    
  }

  /**
   * 根据前面的输入数据，进行逻辑处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    try {
        System.out.println("---dealData---");
      for (int i=1; i<=inLJTempFeeSet.size(); i++) {  //SET是从1开始记数
        //取出一行，并查询出该行的全部信息
        LJTempFeeSchema tLJTempFeeSchema = inLJTempFeeSet.get(i);


        //核销处理
        confTempFee(tLJTempFeeSchema);
        System.out.println("---confTempFee-end---");
        //设置实付总表
        tLJTempFeeSchema = (tLJTempFeeSchema.getDB().query()).get(1);
        setLJAGet(tLJTempFeeSchema);

        System.out.println("End setLJAGet");
      }
    }
    catch(Exception e) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "TempFeeWithdrawBL";
      tError.functionName = "dealData";
      tError.errorMessage = "数据处理错误!" + e.getMessage();
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData mInputData) {
    // 暂交费表
    inLJTempFeeSet.set((LJTempFeeSet)mInputData.getObjectByObjectName("LJTempFeeSet",0));
    tG = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0);
    System.out.println("实付号码:"+tNo);
    
    try {
      TransferData tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);
      tPayMode = (String)tTransferData.getValueByName("PayMode");
      

      //容错处理
      if (mGetNoticeNo == null) mGetNoticeNo = "";
      if (mNotBLS == null) mNotBLS = "";
    }
    catch (Exception e) {
      System.out.println("正常退费，非核保退费！");
    }

    if(inLJTempFeeSet == null ||tG==null) {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="TempFeeWithdrawBL";
      tError.functionName="getInputData";
      tError.errorMessage="没有得到足够的数据，请您确认!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData() {
    mInputData = new VData();
    MMap outMap = new MMap();
    try {
      System.out.println("----before submit----");
      outMap.put(outLJTempFeeSet,"UPDATE");
      outMap.put(outLJAGetSet,"INSERT");
      outMap.put(outLJTempFeeClassSet,"UPDATE");
      mInputData.add( outMap );
      PubSubmit tPubSubmit = new PubSubmit();
      if(!tPubSubmit.submitData(mInputData,"")){
        CError.buildErr(this, "暂交退费数据更新有误", tPubSubmit.mErrors);
        return false;
      }
      System.out.println("----after submit----"+mInputData.get(0));
      System.out.println("----after submit----");
    }
    catch(Exception ex) {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="TempFeeWithdrawBL";
      tError.functionName="prepareOutputData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }

  public static void main(String[] args) {
  }
}
