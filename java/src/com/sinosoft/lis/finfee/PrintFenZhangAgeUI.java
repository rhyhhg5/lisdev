
package com.sinosoft.lis.finfee;

import java.util.Date;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;

public class PrintFenZhangAgeUI
{
//    private static final Date EndInputDate = null;

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private String strUrl = "";

    private GlobalInput mGlobalInput = new GlobalInput();
    
    private TransferData mDealElement = new TransferData();

    public PrintFenZhangAgeUI()
    {
    	
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        mDealElement = (TransferData) cInputData.getObjectByObjectName("TransferData",0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        System.out.println(mGlobalInput.Operator);
        System.out.println(mGlobalInput.ManageCom);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }
    
    
    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!cOperate.equals("CONFIRM"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }

        // 进行业务处理
        if (!dealData())
        {
            return false;
        }

        // 准备传往后台的数据
        //VData vData = new VData();

//        if (!prepareOutputData(vData))
//        {
//            return false;
//        }

        PrintFenZhangAgeBL tPrintFenZhangAgetBL = new PrintFenZhangAgeBL();
        
        System.out.println("Start PrintFirst UI Submit ...");
//        tPrintFenZhangAgetBL.setEndInputDate(EndInputDate);

        if (!tPrintFenZhangAgetBL.submitData(cInputData, cOperate))
        {
            if (tPrintFenZhangAgetBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tPrintFenZhangAgetBL.mErrors);
                return false;
            }
            else
            {
                buildError("submitData", "PrintFenZhangAgetBL发生错误，但是没有提供详细的出错信息");
                return false;
            }
        }
        else
        {
            mResult = tPrintFenZhangAgetBL.getResult();
            return true;
        }
    }


    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     * @param vData VData
     * @return boolean
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.add(strUrl);
            vData.add(mGlobalInput);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     */
    private boolean dealData()
    {
    	PrintFenZhangAgeBL tPrintFenZhangAgetBL = new PrintFenZhangAgeBL();
    	VData tVData = new VData();
    	tVData.add(mDealElement);
    	
    	tPrintFenZhangAgetBL.submitData(tVData, "CONFIRM");
        return true;
    }
   
    public VData getResult()
    {
        return this.mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "DerferAppF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    public static void main(String[] args)
    {
        PrintFenZhangAgeUI tPrintFenZhangAgeUI= new PrintFenZhangAgeUI();
        VData tVData = new VData();
        GlobalInput tG = new GlobalInput();//用户信息
        tG.ManageCom = "86110000";
        tG.Operator = "001";
        String url = "D:\\myProject\\ui";
        tVData.addElement(tG);
        tVData.addElement(url);

        tPrintFenZhangAgeUI.submitData(tVData, "CONFIRM");
    }
}
