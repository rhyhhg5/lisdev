/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.finfee;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Lis_New</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author Sinosoft
 * @version 1.0
 */

public class NewGrpContFeeWithdrawBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();


    /** 往后面传输数据的容器 */
    private VData mInputData;


    /** 数据操作字符串 */
    private GlobalInput mGI = new GlobalInput();
    private String mOperate;
    private String tLimit = "";
    private String serNo = ""; //流水号
    private String getNo = ""; //给付收据号码
    private String prtSeqNo = ""; //印刷流水号
    private boolean specWithDrawFlag = false; //特殊的退费标记。该标记为真时，不考虑outpayflag标志
    private TransferData tTransferData = new TransferData();

    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private LJAGetSchema mLJAGetSchema = new LJAGetSchema(); //实付总表
    private LJAGetSet mLJAGetSet = new LJAGetSet();

//    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema(); //集体合同表
    private LCGrpContSet mLCGrpContSet = new LCGrpContSet();
    private LCGrpContSchema mNewLCGrpContSchema = new LCGrpContSchema(); //集体合同表
    private LCGrpContSet mNewLCGrpContSet = new LCGrpContSet();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema(); //打印管理表
    private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();
    private LJAGetOtherSchema mLJAGetOtherShema = new LJAGetOtherSchema(); //其它其他退费实付表
    private LJAGetOtherSet mLJAGetOtherSet = new LJAGetOtherSet(); //其它其他退费实付表

    private String grpContSign = null; //签单调用标志


    public NewGrpContFeeWithdrawBL()
    {
    }


    //传输数据的公共方法
    public boolean submitData(VData tVData)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(tVData))
            return false;
        System.out.println("After getinputdata");

        //进行业务处理
        if (!dealData())
            return false;
        System.out.println("After dealData！");

        //准备往后台的数据
        if (!prepareOutputData())
            return false;

        if (grpContSign == null)
        {
            System.out.println("After prepareOutputData");

            System.out.println("Start NewGrpContFeeWithdrawBL BL Submit...");

            NewGrpContFeeWithdrawBLS tNewGrpContFeeWithdrawBLS = new
                    NewGrpContFeeWithdrawBLS();
            tNewGrpContFeeWithdrawBLS.submitData(mInputData);

            System.out.println("End NewGrpContFeeWithdrawBL BL Submit...");

            //如果有需要处理的错误，则返回
            if (tNewGrpContFeeWithdrawBLS.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tNewGrpContFeeWithdrawBLS.mErrors);
            }

            //mInputData = null;
        }
        return true;
    }

    public VData getVResult()
    {
        return this.mInputData;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData tVData)
    {
        mLCGrpContSet = (LCGrpContSet) tVData.getObjectByObjectName(
                "LCGrpContSet", 0);
        mGI = (GlobalInput) tVData.getObjectByObjectName("GlobalInput", 0);
        tTransferData = (TransferData) tVData.getObjectByObjectName(
                "TransferData", 0);

        if (mLCGrpContSet == null || mGI == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "NewGrpContFeeWithdrawBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tTransferData != null)
        {
            String inStr = (String) tTransferData.getValueByName("SpecWithDraw");
            if (inStr != null)
            {
                if (inStr.equals("1"))
                {
                    specWithDrawFlag = true;
                }
            }
            //团单签单标记
            grpContSign = (String) tTransferData.getValueByName("GrpContSign");

        }

        return true;
    }


    //根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData()
    {
        //产生流水号
        tLimit = PubFun.getNoLimit(mGI.ManageCom);
        serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);

        //获取外界传入数据
        String tPayMode=(String)tTransferData.getValueByName("PayMode");
        String tPayDate = (String) tTransferData.getValueByName("PayDate");
        String  tempGetDif = (String)tTransferData.getValueByName("PayDif");
        String  tDrawerId = (String)tTransferData.getValueByName("DrawerID");
        String  tDrawer = (String)tTransferData.getValueByName("Drawer");
        double tGetDif = Double.parseDouble(tempGetDif);
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        LCGrpContDB tLCGrpContDB = null;
        for (int index = 1; index <= mLCGrpContSet.size(); index++)
        {
            mNewLCGrpContSchema = mLCGrpContSet.get(index);
            //1-查询投保单号对应的已签单的保单，如果没有则跳过,否则保存查询到的纪录
            //如果是签单调用，则不用查询
            if (grpContSign == null)
            {
                tLCGrpContDB = new LCGrpContDB();
                tLCGrpContDB.setGrpContNo(mNewLCGrpContSchema.getGrpContNo());
                tLCGrpContSet = tLCGrpContDB.query();
                if (tLCGrpContSet == null)
                    continue;
                if (tLCGrpContSet.size() == 0)
                    continue;
                mNewLCGrpContSchema = tLCGrpContSet.get(1);

                if (mNewLCGrpContSchema.getAppFlag() == null ||
               !mNewLCGrpContSchema.getAppFlag().equals("1")) //如果没有签单返回
               continue;

            }


            if (mNewLCGrpContSchema.getOutPayFlag() == null) //溢交处理方式：1退费，2充当续期保费
            {
                mNewLCGrpContSchema.setOutPayFlag("1");
            }
            if (mNewLCGrpContSchema.getPayIntv() == 0) //如果交费方式是趸交，退费
            {
                mNewLCGrpContSchema.setOutPayFlag("1");
            }
            if (specWithDrawFlag) //如果特殊退费标记为真，那么默认退费
            {
                //mNewLCGrpPolSchema.setOutPayFlag("1");
                if (!(mNewLCGrpContSchema.getDif() > 0))
                    continue;
            }
            else
            {
                if (!(mNewLCGrpContSchema.getDif() > 0) ||
                    !mNewLCGrpContSchema.getOutPayFlag().equals("1")) //如果没有余额或者溢交处理方式不是退费,返回
                    continue;
            }
             if (!(mNewLCGrpContSchema.getDif() >= tGetDif))//如果余额大等于所退余额,返回
             {
                  continue;
             }
            //2-准备实付表纪录,产生实付号码
            tLimit = PubFun.getNoLimit(mNewLCGrpContSchema.getManageCom());
            getNo = PubFun1.CreateMaxNo("GETNO", tLimit);
            String sql = "select * from ljtempfeeclass where tempfeeno in(select tempfeeno from ljtempfee where otherno='" +
                         mNewLCGrpContSchema.getGrpContNo() +
                         "'  and confflag='1') ";
            LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
            LJTempFeeClassSet tLJTempFeeClassSet = tLJTempFeeClassDB.
                    executeQuery(sql);
            String payMode = "";
            if (tLJTempFeeClassSet != null)
            {
                if (tLJTempFeeClassSet.size() > 0)
                {
                    payMode = tLJTempFeeClassSet.get(1).getPayMode();
                }

            }

            //实付总表
            mLJAGetSchema.setPayMode(tPayMode);
            mLJAGetSchema.setShouldDate(CurrentDate);
            mLJAGetSchema.setStartGetDate(CurrentDate);
            mLJAGetSchema.setActuGetNo(getNo);
            mLJAGetSchema.setOtherNo(mNewLCGrpContSchema.getGrpContNo());
            mLJAGetSchema.setOtherNoType("8"); //其它类型退费(溢交保费退费)--集体单
            mLJAGetSchema.setAppntNo(mNewLCGrpContSchema.getAppntNo());
            mLJAGetSchema.setAccName(mNewLCGrpContSchema.getAccName());
            mLJAGetSchema.setBankCode(mNewLCGrpContSchema.getBankCode());
            mLJAGetSchema.setBankAccNo(mNewLCGrpContSchema.getBankAccNo());
            mLJAGetSchema.setDrawerID(tDrawerId);
            mLJAGetSchema.setDrawer(tDrawer);

            mLJAGetSchema.setSumGetMoney(tGetDif);
            mLJAGetSchema.setSaleChnl(mNewLCGrpContSchema.getSaleChnl());
            mLJAGetSchema.setSerialNo(serNo);
            mLJAGetSchema.setOperator(mGI.Operator);
            mLJAGetSchema.setManageCom(mNewLCGrpContSchema.getManageCom());
            mLJAGetSchema.setAgentCode(mNewLCGrpContSchema.getAgentCode());
            mLJAGetSchema.setAgentCom(mNewLCGrpContSchema.getAgentCom());
            mLJAGetSchema.setAgentGroup(mNewLCGrpContSchema.getAgentGroup());
            mLJAGetSchema.setShouldDate(CurrentDate);
            mLJAGetSchema.setStartGetDate(CurrentDate);
            mLJAGetSchema.setMakeDate(CurrentDate);
            mLJAGetSchema.setMakeTime(CurrentTime);
            mLJAGetSchema.setModifyDate(CurrentDate);
            mLJAGetSchema.setModifyTime(CurrentTime);
            mLJAGetSchema.setEnterAccDate(tPayDate);
            mLJAGetSet.add(mLJAGetSchema);
            //实付子表
            mLJAGetOtherShema = new LJAGetOtherSchema();
            mLJAGetOtherShema.setActuGetNo(getNo);
            mLJAGetOtherShema.setOtherNo(mNewLCGrpContSchema.getGrpContNo());
            mLJAGetOtherShema.setOtherNoType("8"); //其它类型退费(溢交保费退费)--集体单
            mLJAGetOtherShema.setPayMode(tPayMode);
            mLJAGetOtherShema.setGetMoney(tGetDif);
            mLJAGetOtherShema.setGetDate(CurrentDate);
            mLJAGetOtherShema.setFeeOperationType("YJTF"); //溢交退费
            mLJAGetOtherShema.setFeeFinaType("YJTF"); //溢交退费
            mLJAGetOtherShema.setManageCom(mNewLCGrpContSchema.getManageCom());
            mLJAGetOtherShema.setAgentCom(mNewLCGrpContSchema.getAgentCom());
            mLJAGetOtherShema.setAgentType(mNewLCGrpContSchema.getAgentType());
            mLJAGetOtherShema.setAPPntName(mNewLCGrpContSchema.getGrpName());
            mLJAGetOtherShema.setAgentGroup(mNewLCGrpContSchema.getAgentGroup());
            mLJAGetOtherShema.setAgentCode(mNewLCGrpContSchema.getAgentCode());
            mLJAGetOtherShema.setSerialNo(serNo);
            mLJAGetOtherShema.setOperator(mGI.Operator); ;
            mLJAGetOtherShema.setMakeTime(CurrentTime);
            mLJAGetOtherShema.setMakeDate(CurrentDate);
            mLJAGetOtherShema.setModifyDate(CurrentDate);
            mLJAGetOtherShema.setModifyTime(CurrentTime);
            mLJAGetOtherShema.setEnterAccDate(tPayDate);

            mLJAGetOtherSet.add(mLJAGetOtherShema);

            //4-更新集体保单数据
            mNewLCGrpContSchema.setDif(mNewLCGrpContSchema.getDif()- tGetDif);
            mNewLCGrpContSchema.setModifyDate(CurrentDate);
            mNewLCGrpContSchema.setModifyTime(CurrentTime);
            mNewLCGrpContSet.add(mNewLCGrpContSchema);

            if (grpContSign == null)
            {
               /* LCContDB tLCContDB = new LCContDB();
                tLCContDB.setGrpContNo(mNewLCGrpContSchema.getGrpContNo());
                LCContSet tLCContSet = new LCContSet();
                tLCContSet = tLCContDB.query();
                for (int i = 1; i <= tLCContSet.size(); i++)
                {
                    mNewLCContSchema = tLCContSet.get(i);
                    mNewLCContSchema.setDif((mNewLCGrpContSchema.getDif()- tGetDif)/tLCContSet.size());
                    mNewLCContSet.add(mNewLCContSchema);
                }*/

                /*LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
                tLCGrpPolDB.setGrpContNo(mNewLCGrpContSchema.getGrpContNo());
                LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
                tLCGrpPolSet = tLCGrpPolDB.query();
                for (int i = 1; i <= tLCGrpPolSet.size(); i++)
                {
                    mNewLCGrpPolSchema = tLCGrpPolSet.get(i);
                    mNewLCGrpPolSchema.setDif((mNewLCGrpContSchema.getDif()- tGetDif)/tLCGrpPolSet.size());
                    mNewLCGrpPolSet.add(mNewLCGrpPolSchema);
                }*/


            }

        }
        return true;
    }


    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData()
    {
        mInputData = new VData();
        try
        {
            //mInputData.add(mNewLCGrpPolSet);
            System.out.println("lcgrpContSet.size:" + mNewLCGrpContSet.size());
            mInputData.add(mNewLCGrpContSet);
            mInputData.add(mLJAGetSet);
            //mInputData.add(mLOPRTManagerSet);
            mInputData.add(mLJAGetOtherSet);

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "NewGrpContFeeWithdrawBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public static void main(String[] args)
    {
        NewGrpContFeeWithdrawBL NewGrpContFeeWithdrawBL1 = new
                NewGrpContFeeWithdrawBL();
        VData tVData = new VData();
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86110000";
        tGI.ComCode = "86";
        tGI.Operator = "wuser";
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema.setGrpContNo("0000019901");
        tLCGrpContSet.add(tLCGrpContSchema);
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("PayDate","2005-10-10");
        tTransferData.setNameAndValue("PayMode","1");
        tTransferData.setNameAndValue("PayDif","100.23");

        tVData.add(tLCGrpContSet);
        tVData.add(tGI);
        tVData.add(tTransferData);
        NewGrpContFeeWithdrawBL1.submitData(tVData);
    }
}
