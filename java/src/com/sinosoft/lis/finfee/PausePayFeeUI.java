package com.sinosoft.lis.finfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.schema.*;
/*
 * <p>Title: Web财务系统</p>
 *
 * <p>Description: 应收总表查询功能 && 银行催缴给付费进行暂停类</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author 张君
 * @version 1.0
 */

public class PausePayFeeUI {
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors = new CErrors();

    /** 数据操作字符串 */
    private String mOperate;

    public PausePayFeeUI() {
    }
    /**
传输数据的公共方法
*/
public boolean submitData(VData cInputData,String cOperate)
{
  //将操作数据拷贝到本类中
  this.mOperate = cOperate;

  PausePayFeeBL tPausePayFeeBL=new PausePayFeeBL();

  if (!tPausePayFeeBL.submitData(cInputData,mOperate))
      {
              // @@错误处理
    this.mErrors.copyAllErrors(tPausePayFeeBL.mErrors);
    CError tError = new CError();
    tError.moduleName = "FinFeeGetQueryUI";
    tError.functionName = "submitData";
    tError.errorMessage = "数据查询失败!";
    this.mErrors .addOneError(tError) ;
    return false;
      }
  return true;
}

}
