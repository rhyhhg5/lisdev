package com.sinosoft.lis.finfee;

//用于单证回收
import java.sql.Connection;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TempFeeClassBLS {
        //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

        /** 数据操作字符串 */
    private String mOperate;
    public TempFeeClassBLS() {
    }
    public static void main(String[] args)
    {
        TempFeeClassBLS mTempFeeClassBLS1 = new TempFeeClassBLS();
    }


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Start TempFee BLS Submit...");
        //信息保存
        if (this.mOperate.equals("INSERT") || this.mOperate.equals("UPDATE"))
        {
            tReturn = save(cInputData);
        }

        if (tReturn)
            System.out.println("Save sucessful");
        else
            System.out.println("Save failed");

        System.out.println("End TempFee BLS Submit...");

        return tReturn;
    }


//保存操作
    private boolean save(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn = DBConnPool.getConnection();

        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "TempFeeClassBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);

// 暂交费分类表
            System.out.println("Start 暂交费分类表...");
//删除
            LJTempFeeClassDBSet delLJTempFeeClassDBSet = new
                    LJTempFeeClassDBSet(conn);
            delLJTempFeeClassDBSet.set((LJTempFeeClassSet) mInputData.
                                       getObjectByObjectName(
                    "LJTempFeeClassSet", 2));
            if (delLJTempFeeClassDBSet != null)
            {
                if (!delLJTempFeeClassDBSet.deleteSQL())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(delLJTempFeeClassDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "暂交费分类表数据删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

//添加
            LJTempFeeClassDBSet tLJTempFeeClassDBSet = new LJTempFeeClassDBSet(
                    conn);
            tLJTempFeeClassDBSet.set((LJTempFeeClassSet) mInputData.
                                     getObjectByObjectName("LJTempFeeClassSet",
                    0));
            if (!tLJTempFeeClassDBSet.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLJTempFeeClassDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "TempFeeBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "暂交费表数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            conn.commit();
            conn.close();
            System.out.println("commit end");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "TempFeeBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
            tReturn = false;
        }
        return tReturn;
    }
}
