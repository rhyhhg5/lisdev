/**
 * 2011-10-22
 */
package com.sinosoft.lis.finfee;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.db.LJInvoiceInfoDB;
import com.sinosoft.lis.db.LOPRTInvoiceManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LJInvoiceInfoSet;
import com.sinosoft.lis.vschema.LOPRTInvoiceManagerSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author Zcx
 * 
 */
public class FFIvoiceExportZjBL implements InvoiceExport
{
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private ArrayList mResult = new ArrayList();

    private String mBasePath = "";

    private String mOutPath = "";

    private String mSsqq = "";

    private String mSsqz = "";

    private String mSFState = "";

    private String fileName = "";
    
    private String TaxpayerNo = "";
    
    private String TaxpayerName = "";

    private LOPRTInvoiceManagerSet tLOPRTInvoiceManagerSet = new LOPRTInvoiceManagerSet();

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData))
        {
            buildError("getInputData", "没有获取到足够业务信息或者机构不对");
            return false;
        }
        if (!dealData())
        {
            buildError("dealData", "生成txt文件失败");
            return false;
        }
        return true;
    }

    /**
     * 获取业务数据
     * 
     * @param cInputData
     * @return
     */
    private boolean getInputData(VData cInputData)
    {
        TransferData tParameters = (TransferData) cInputData
                .getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);

        if (mGlobalInput == null || tParameters == null)
        {
            return false;
        }

        this.mBasePath = (String) tParameters.getValueByName("hostBasePath")
                + "/";
        this.mOutPath = (String) tParameters.getValueByName("outPath") + "/";

        this.mSsqq = (String) tParameters.getValueByName("invoiceStartDate");
        this.mSsqz = (String) tParameters.getValueByName("invoiceEndDate");
        this.mSFState = (String) tParameters.getValueByName("SFState");
        // 获取条件内的相关发票打印信息
        String tSql = "";
        if ("1".equals(mSFState))
        {
            tSql = "select * from LOPRTInvoiceManager a "
                    + " where 1 = 1 "
                    + " and exists (select 1 from loprtmanager2 where code='37' and standbyflag4=a.invoiceno and makedate=a.makedate and comcode=a.comcode)"
                    + " and comcode like '" + mGlobalInput.ManageCom + "%'"
                    + " and makedate >= '" + mSsqq + "' "
                    + " and makedate <= '" + mSsqz + "'";
        }
        else
        {
            tSql = "select * from LOPRTInvoiceManager a "
                    + " where 1 = 1 "
                    + " and not exists (select 1 from loprtmanager2 where code='37' and standbyflag4=a.invoiceno and makedate=a.makedate and comcode=a.comcode)"
                    + " and comcode like '" + mGlobalInput.ManageCom + "%'"
                    + " and makedate >= '" + mSsqq + "' "
                    + " and makedate <= '" + mSsqz + "'";
        }
        LOPRTInvoiceManagerDB tLOPRTInvoiceManagerDB = new LOPRTInvoiceManagerDB();
        tLOPRTInvoiceManagerSet = tLOPRTInvoiceManagerDB.executeQuery(tSql);
        System.out.println(tSql);
        if (tLOPRTInvoiceManagerSet == null
                || tLOPRTInvoiceManagerSet.size() == 0)
        {
            return false;
        }
        
        tSql = "select * from LJInvoiceInfo where comcode = '"
            + mGlobalInput.ManageCom + "' " + " and invoicecode='"
            + tLOPRTInvoiceManagerSet.get(1).getInvoiceCode() + "'"
            + " order by makedate desc fetch first 1 rows only with ur ";
    System.out.println(tSql);
    LJInvoiceInfoDB tLJInvoiceInfoDB = new LJInvoiceInfoDB();
    LJInvoiceInfoSet tLJInvoiceInfoSet = tLJInvoiceInfoDB
            .executeQuery(tSql);
    if (tLJInvoiceInfoSet == null || tLJInvoiceInfoSet.size() <= 0)
        return false;
    this.TaxpayerNo = tLJInvoiceInfoSet.get(1).getTaxpayerNo();
    this.TaxpayerName = tLJInvoiceInfoSet.get(1).getTaxpayerName();
    
        return true;
    }

    private boolean dealData()
    {
        System.out.println("export txt begin...");
        try
        {
            String write = "发票代码\t开票日期\t发票号码\t付费方名称\t付费方识别号\t项目摘要\t开具金额\t发票类别\n";
            for (int i = 1; i <= tLOPRTInvoiceManagerSet.size(); i++)
            {
                write = write + "\"\t" + tLOPRTInvoiceManagerSet.get(i).getInvoiceCode() + "\t\"\t";
                String date = tLOPRTInvoiceManagerSet.get(i).getMakeDate().substring(0, 4) + tLOPRTInvoiceManagerSet.get(i).getMakeDate().substring(5, 7) + tLOPRTInvoiceManagerSet.get(i).getMakeDate().substring(8, 10);
                write = write + date + "\t";
                write = write + tLOPRTInvoiceManagerSet.get(i).getInvoiceNo() + "\t";
                if("1".equals(mSFState)){
                    write = write + TaxpayerName + "\t";
                    write = write + TaxpayerNo + "\t";
                    write = write + "保险费" + "\t";
                } else {
                    write = write + tLOPRTInvoiceManagerSet.get(i).getPayerName() + "\t";
                    write = write + "\t";
                    write = write + "保险费" + "\t";
                }
                write = write + (int)tLOPRTInvoiceManagerSet.get(i).getXSumMoney()*100 + "\t";
                if(tLOPRTInvoiceManagerSet.get(i).getStateFlag().equals("0")){
                    write = write + "1\n";
                } else if(tLOPRTInvoiceManagerSet.get(i).getContNo() == null || tLOPRTInvoiceManagerSet.get(i).getContNo().equals("")){
                    write = write + "3\n";
                } else {
                    write = write + "2\n";
                }
            }
            fileName = "Invoice_" + PubFun.getCurrentDate2() + ".xls";
            FileWriter fw = new FileWriter(mBasePath + mOutPath + fileName);
            fw.write(write);
            fw.close();
            System.out.println("导入文件：" + mBasePath + mOutPath + fileName);
            mResult.add(mOutPath + fileName);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
        System.out.println("export xml finished...");
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FFIvoiceExportZjBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public List getResult()
    {
        return mResult;
    }
    
    public CErrors getErrors(){
    	return mErrors;
    }

    public static void main(String[] args)
    {
        try
        {
            System.out.println("2011-01-11".substring(0, 4) + "2011-01-11".substring(5, 7) + "2011-01-11".substring(8, 10));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
