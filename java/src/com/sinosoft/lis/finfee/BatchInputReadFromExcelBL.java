package com.sinosoft.lis.finfee;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

import java.io.*;

//import org.w3c.dom.*;
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;

//import javax.xml.transform.Source;
//import javax.xml.transform.Result;
//import javax.xml.transform.dom.DOMSource;
//import javax.xml.transform.dom.DOMResult;
//import javax.xml.transform.TransformerFactory;
//import javax.xml.transform.Transformer;
//import javax.xml.transform.stream.StreamSource;

import java.io.IOException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.f1j.ss.BookModelImpl;
import com.f1j.ss.ReadParams;
import com.f1j.util.F1Exception;

public class BatchInputReadFromExcelBL
{
    private VData mInputData = new VData();
    /** 传出数据的容器 */
    private VData mResult = new VData();
    /** 提交数据的容器 */
    private MMap map = new MMap();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public  CErrors mErrors = new CErrors();
    
    private String mFilePath = "";
    private BookModelImpl book = new BookModelImpl();
    //业务数据
    private TransferData inTransferData = new TransferData();
    private GlobalInput inGlobalInput = new GlobalInput();
    private String fileName = "";
    private LDBankSet outLDBankSet = new LDBankSet();
    /** 选择银行编码 */
    private String bankCode = "";
    private Document dataDoc = null;
    private Document resultDoc = null;
    
    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"READ"和""
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate) {
      //将操作数据拷贝到本类中
      this.mInputData = (VData)cInputData.clone();
      this.mOperate = cOperate;

      //得到外部传入的数据,将数据备份到本类中
      if (!getInputData()) return false;
      System.out.println("---End getInputData---");

      //进行业务处理
      if (!dealData()) return false;
      System.out.println("---End dealData---");

      return true;
    }
    
    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * 
     * @return: boolean
     */
    private boolean getInputData()  {
      try {
        inTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);
        inGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0);
      }
      catch (Exception e) {
        // @@错误处理
        CError.buildErr(this, "接收数据失败");
        return false;
      }

      return true;
    }
  
    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
      try {
        //获取银行文件数据
        if (mOperate.equals("READ")) {
          //获取返回文件名称
          fileName = (String)inTransferData.getValueByName("fileName");
          fileName = fileName.replace('\\', '/');
          //读取银行返回文件，公共部分
          if (!readBankFile(fileName)){
          throw new Exception("读取银行编码文件失败");
          }
        }
      }
      catch(Exception e) {
        // @@错误处理
          System.out.println(e.getMessage());
        CError.buildErr(this, "数据处理错误:" + e.getMessage());
        return false;
      }

      return true;
    }

    /**
     * 读取银行编码文件
     * @param fileName
     * @return
     */
    private boolean readBankFile(String fileName) {
        try{
            mFilePath=fileName;
            boolean treturn=false;
            LDBankSchema tLDBankSchema = new LDBankSchema();
            FinBankAddUI tFinBankAddUI=new FinBankAddUI();
            book.initWorkbook();//初始
            book.read(mFilePath,new ReadParams());
            int tSheetNums = book.getNumSheets();//读去sheet的个数，就是一个excel包含几个sheet
            System.out.println("tSheetNums==="+tSheetNums);
            for( int sheetNum=0;sheetNum<tSheetNums;sheetNum++ ){
                book.setSheet(sheetNum);
                int tLastCol = book.getLastCol();//总共多少列，他的列数从０开始，比如有４列，那tLastCol的结果＝３
                int tLastRow = book.getLastRow();//总共多少行，他的行也是从０开始，比如有２行，那tLastRow的结果＝１
                System.out.println("tLastCol==="+tLastCol);
                System.out.println("tLastRow==="+tLastRow);
                String colName="";
                String colValue="";
                //按列循环
                    for(int  j=1;j<=tLastRow;j++ ){//循环总行数
                        for(int  i=0;i<=tLastCol;i++ ){//循环总列数．
                        if( !book.getText(j,i).trim().equals("") ){//判断第j行的第i列是否为空值．
                            System.out.println(book.getText(j,i));//打印出第j行的第i列的值
                            colValue=(String)book.getText(j,i).trim();
                            switch(i){
                                case 0:colName="BankCode";
                                 break;
                                case 1:colName="HeadBankName";
                                break;
                                case 2:colName="ManageCom";
                                break;
                                case 3:colName="ChildBankName";
                                break;
                            }
              
                        }
                        inTransferData.removeByName(colName);
                        inTransferData.setNameAndValue(colName.trim(), colValue.trim());
                   //     System.out.println(colName.trim()+":"+inTransferData.getValueByName(colName.trim()));
                   }
                        mInputData.add(inTransferData);
                        treturn=tFinBankAddUI.submitData(mInputData, "CHILD");
                 
//                       VData tVData=tFinBankAddUI.getVResult();

                        if(treturn==true){
                        tLDBankSchema=tFinBankAddUI.getLDBankSchema();
                        }else{
                            tLDBankSchema.setBankCode("");
                            tLDBankSchema.setBankName("");
                        }
                        System.out.println("tLDBankSchema:"+tLDBankSchema.getBankCode());
                        outLDBankSet.add(tLDBankSchema);    
                }
            }
            
        }catch(F1Exception e){
            e.printStackTrace();
            //mCErrors.addOneError("处理一线分摊录入数据错误，原因：" + e.getMessage());
            return false;
        }catch(IOException ioe){
            ioe.printStackTrace();
            //mCErrors.addOneError("IO错误，原因：" + ioe.getMessage());
            return false;
        }
        return true;
           
        }
    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public LDBankSet getLDBankSet() {
      return outLDBankSet;
    }

}
