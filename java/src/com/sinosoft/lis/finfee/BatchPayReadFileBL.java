package com.sinosoft.lis.finfee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.f1j.ss.BookModelImpl;
import com.f1j.ss.ReadParams;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJFIGetSchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJFIGetSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BatchPayReadFileBL {
	private VData mInputData = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	/** 错误处理类 */
	public CErrors mErrors = new CErrors();

	private String mFilePath = "";

	private BookModelImpl book = new BookModelImpl();

	// 业务数据
	private TransferData inTransferData = new TransferData();

	private String fileName = "";

	private GlobalInput tG = new GlobalInput();

	private List exList = new ArrayList();

	/**
	 * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	 * 
	 * @param cInputData
	 *            传入的数据,VData对象
	 * @param cOperate
	 *            数据操作字符串，主要包括"READ"和""
	 * @return 布尔值（true--提交成功, false--提交失败）
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}
		System.out.println("---End getInputData---");

		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		System.out.println("---End dealData---");

		return true;
	}

	/**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @param: 无
	 * 
	 * @return: boolean
	 */
	private boolean getInputData() {
		try {
			inTransferData = (TransferData) mInputData.getObjectByObjectName(
					"TransferData", 0);
			tG = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput",
					0);

			if (tG == null || inTransferData == null) {
				CError tError = new CError();
				tError.moduleName = "BatchPayReadFileBL";
				tError.functionName = "checkList";
				tError.errorMessage = "接收数据失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BatchPayReadFileBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "接收数据失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	/**
	 * 根据前面的输入数据，进行逻辑处理
	 * 
	 * @return 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		try {
			// 获取银行文件数据
			if (mOperate.equals("READ")) {
				// 获取返回文件名称
				fileName = (String) inTransferData.getValueByName("fileName");
				fileName = fileName.replace('\\', '/');
				// 读取银行返回文件，公共部分
				System.out.println("---开始读取文件---");
				if (!readTempFeeFile(fileName)) {
					return false;
				}

				if (!checkList()) {
					return false;
				}

				if (!setLJAGet()) {
					return false;
				}
			}
		} catch (Exception e) {
			// @@错误处理
			e.printStackTrace();
			CError tError = new CError();
			tError.moduleName = "BatchPayReadFileBL";
			tError.functionName = "dealData";
			tError.errorMessage = "数据处理错误:" + e.getMessage();
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	/**
	 * 读取收费导入文件
	 * 
	 * @param fileName
	 * @return
	 */
	private boolean readTempFeeFile(String fileName) {
		try {
			mFilePath = fileName;
			book.initWorkbook();// 初始
			book.read(mFilePath, new ReadParams());
			int tSheetNums = book.getNumSheets();// 读去sheet的个数，就是一个excel包含几个sheet
			System.out.println("tSheetNums===" + tSheetNums);

			for (int sheetNum = 0; sheetNum < tSheetNums; sheetNum++) {
				book.setSheet(sheetNum);
				int tLastCol = book.getLastCol();// 总共多少列，他的列数从０开始，比如有４列，那tLastCol的结果＝３
				int tLastRow = book.getLastRow();// 总共多少行，他的行也是从０开始，比如有２行，那tLastRow的结果＝１
				System.out.println("tLastCol===" + tLastCol);
				System.out.println("tLastRow===" + tLastRow);
				String colValue = "";
				// 按列循环
				for (int j = 1; j <= tLastRow; j++) {// 循环总行数
					Map tMap = new HashMap();
					for (int i = 0; i <= tLastCol; i++) {// 循环总列数
						if (book.getText(j, i) != null) {
							colValue = (String) book.getText(j, i).trim();
						} else {
							colValue = "";
						}
						switch (i) {
						case 0:
							tMap.put("ActuGetNo", colValue);
							break;
						case 1:
							tMap.put("OtherNo", colValue);
							break;
						case 2:
							tMap.put("OtherNoType", colValue);
							break;
						case 3:
							tMap.put("GetMoney", colValue);
							break;
						case 4:
							tMap.put("GetMode", colValue);
							break;
						case 5:
							tMap.put("InsBankCode", colValue);
							break;
						case 6:
							tMap.put("InsBankAccNo", colValue);
							break;
						case 7:
							tMap.put("BankCode", colValue);
							break;
						case 8:
							tMap.put("BankAccNo", colValue);
							break;
						case 9:
							tMap.put("AccName", colValue);
							break;
						case 10:
							tMap.put("Drawer", colValue);
							break;
						case 11:
							tMap.put("ChequeNo", colValue);
							break;
						case 12:
							tMap.put("EnterAccDate", colValue);
							break;
						case 13:
							tMap.put("DrawID", colValue);
							break;
						case 14:
							tMap.put("AgentCom", colValue);
							break;
						}

					}
					tMap.put("ManageCom", tG.ManageCom);
					exList.add(tMap);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			CError tError = new CError();
			tError.moduleName = "BatchPayReadFileBL";
			tError.functionName = "readTempFeeFile";
			tError.errorMessage = "读取导入文件错误:" + ex.getMessage();
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	private boolean checkList() {
		if (exList.size() == 0) {
			System.out.println("-----exList-size-0-----");
			this.mErrors.addOneError("未发现导入文件中存在收费信息!");
			return false;
		}

		String sql = "";
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();

		for (int k = 0; k < exList.size(); k++) {
			Map tMap = (Map) exList.get(k);

			System.out.println("ActuGetNo-" + tMap.get("ActuGetNo"));
			System.out.println("OtherNo-" + tMap.get("OtherNo"));
			System.out.println("OtherNoType-" + tMap.get("OtherNoType"));
			System.out.println("GetMoney-" + tMap.get("GetMoney"));
			System.out.println("GetMode-" + tMap.get("GetMode"));
			System.out.println("InsBankCode-" + tMap.get("InsBankCode"));
			System.out.println("InsBankAccNo-" + tMap.get("InsBankAccNo"));
			System.out.println("BankCode-" + tMap.get("BankCode"));
			System.out.println("BankAccNo-" + tMap.get("BankAccNo"));
			System.out.println("AccName-" + tMap.get("AccName"));
			System.out.println("Drawer-" + tMap.get("Drawer"));
			System.out.println("ChequeNo-" + tMap.get("ChequeNo"));
			System.out.println("EnterAccDate-" + tMap.get("EnterAccDate"));
			System.out.println("DrawID-" + tMap.get("DrawID"));
			System.out.println("AgentCom-" + tMap.get("AgentCom"));

			if (!tMap.get("GetMode").toString().matches("3|11|12")) {
				this.mErrors.addOneError("第" + (k + 2) + "行数据，付费方式错误!");
				return false;
			}

			if (!PubFun.checkDateForm(tMap.get("EnterAccDate").toString())) {
				this.mErrors.addOneError("第" + (k + 2) + "行数据，到账日期格式错误!");
				return false;
			} else if (PubFun.calInterval(tMap.get("EnterAccDate").toString(),
					PubFun.getCurrentDate(), "D") < 0) {
				this.mErrors.addOneError("第" + (k + 2) + "行数据，到账日期大于当天!");
				return false;
			}

			if (tMap.get("GetMode").toString().equals("3")
					&& (tMap.get("BankCode").equals("") || tMap.get("ChequeNo")
							.equals(""))) {
				this.mErrors.addOneError("第" + (k + 2)
						+ "行数据，付费方式为支票，对方开户银行和票据号码不能为空!");
				return false;
			}

			if (tMap.get("GetMode").toString().equals("11")
					&& (tMap.get("BankCode").equals("") || tMap
							.get("BankAccNo").equals(""))) {
				this.mErrors.addOneError("第" + (k + 2)
						+ "行数据，付费方式为银行汇款，对方开户银行和对方银行帐号不能为空!");
				return false;
			}

			if (tMap.get("InsBankCode").equals("")
					|| tMap.get("InsBankAccNo").equals("")) {
				this.mErrors.addOneError("第" + (k + 2)
						+ "行数据，付费方式不为现金或内部转账，本方银行编码和本方银行帐号不能为空!");
				return false;
			}
			sql = "select 1 from ldfinbank where bankcode='"
					+ tMap.get("InsBankCode") + "' and bankaccno='"
					+ tMap.get("InsBankAccNo")
					+ "' and finflag='F' and managecom like '" + tG.ManageCom
					+ "%'";
			tSSRS = tExeSQL.execSQL(sql);
			if (tSSRS.MaxRow == 0) {
				this.mErrors.addOneError("第" + (k + 2) + "行数据，本方银行账号不存在!");
				return false;
			}

			if (!tMap.get("BankCode").equals("")) {
				sql = "select 1 from ldbank where bankcode='"
						+ tMap.get("BankCode") + "'";
				tSSRS = tExeSQL.execSQL(sql);
				if (tSSRS.MaxRow == 0) {
					this.mErrors.addOneError("第" + (k + 2) + "行数据，对方银行编码不存在!");
					return false;
				}
			}
			
			if(tMap.get("Drawer").equals("")){
				this.mErrors.addOneError("第" + (k + 2)
						+ "行数据，领取人不能为空!");
				return false;
			}

			sql = "select otherno,othernotype,sumgetmoney,confdate,bankonthewayflag from ljaget where actugetno='"
					+ tMap.get("ActuGetNo") + "'";
			tSSRS = tExeSQL.execSQL(sql);
			if (tSSRS.MaxRow == 0) {
				this.mErrors.addOneError("第" + (k + 2) + "行数据，付费号不存在!");
				return false;
			}
			if (!tSSRS.GetText(1, 1).equals(tMap.get("OtherNo"))) {
				this.mErrors.addOneError("第" + (k + 2) + "行数据，其他号码错误!");
				return false;
			}
			if (!tSSRS.GetText(1, 2).equals(tMap.get("OtherNoType"))) {
				this.mErrors.addOneError("第" + (k + 2) + "行数据，其他号码类型错误!");
				return false;
			} else if (!tMap.get("OtherNoType").toString().matches("AC|BC|MC")) {
				this.mErrors.addOneError("第" + (k + 2) + "行数据，只能对手续费数据进行导入!");
				return false;
			} else if (tSSRS.GetText(1, 4) != null
					&& !tSSRS.GetText(1, 4).trim().equals("")) {
				this.mErrors.addOneError("第" + (k + 2) + "行数据，该笔手续费已付费!");
				return false;
			} else if (tSSRS.GetText(1, 5) != null
					&& tSSRS.GetText(1, 5).equals("1")) {
				this.mErrors.addOneError("第" + (k + 2) + "行数据，该笔手续费处于银行在途状态!");
				return false;
			}
			try {
				double money = Double.parseDouble(tMap.get("GetMoney")
						.toString());
				if (money != Double.parseDouble(tSSRS.GetText(1, 3))) {
					this.mErrors.addOneError("第" + (k + 2) + "行数据，金额与应收金额不符!");
					return false;
				}
			} catch (Exception ex) {
				this.mErrors.addOneError("第" + (k + 2) + "行数据，金额格式异常!");
				return false;
			}
		}
		return true;
	}

	public boolean setLJAGet() {
		LJFIGetSchema tLJFIGetSchema; // 财务给付表
		LJAGetSchema tLJAGetSchema; // 实付总表
		LJAGetSet mLJAGetSet = new LJAGetSet();
		LJFIGetSet mLJFIGetSet = new LJFIGetSet();

		VData mVData = new VData();

		for (int i = 0; i < exList.size(); i++) {
			Map tMap = (Map) exList.get(i);
			tLJAGetSchema = new LJAGetSchema();
			tLJAGetSchema.setActuGetNo(tMap.get("ActuGetNo").toString());
			VData tVData = new VData();
			tVData.add(tLJAGetSchema);
			LJAGetQueryUI tLJAGetQueryUI = new LJAGetQueryUI();
			if (!tLJAGetQueryUI.submitData(tVData, "QUERY")) {
				this.mErrors.addOneError("查询实付总表失败，原因是: "
						+ tLJAGetQueryUI.mErrors.getError(0).errorMessage);
				return false;
			} else {
				tVData.clear();
				tVData = tLJAGetQueryUI.getResult();
				tLJAGetSchema = null;
				tLJAGetSchema = ((LJAGetSet) tVData.getObjectByObjectName(
						"LJAGetSet", 0)).get(1);
				tLJAGetSchema.setEnterAccDate(tMap.get("EnterAccDate")
						.toString());
				tLJAGetSchema.setDrawer(tMap.get("Drawer").toString());
				tLJAGetSchema.setDrawerID(tMap.get("DrawID").toString());
				tLJAGetSchema.setOperator(tG.Operator);
				tLJAGetSchema.setChequeNo(tMap.get("ChequeNo").toString());
				tLJAGetSchema.setAccName(tMap.get("AccName").toString());
				tLJAGetSchema.setBankCode(tMap.get("BankCode").toString());
				tLJAGetSchema.setBankAccNo(tMap.get("BankAccNo").toString());
				tLJAGetSchema
						.setInsBankCode(tMap.get("InsBankCode").toString());
				tLJAGetSchema.setInsBankAccNo(tMap.get("InsBankAccNo")
						.toString());

				tLJFIGetSchema = new LJFIGetSchema();

				tLJFIGetSchema.setActuGetNo(tMap.get("ActuGetNo").toString());
				tLJFIGetSchema.setPayMode(tMap.get("GetMode").toString());
				tLJFIGetSchema.setOtherNo(tLJAGetSchema.getOtherNo());
				tLJFIGetSchema.setOtherNoType(tLJAGetSchema.getOtherNoType());
				tLJFIGetSchema.setGetMoney(tMap.get("GetMoney").toString());
				tLJFIGetSchema.setShouldDate(tLJAGetSchema.getShouldDate());
				tLJFIGetSchema.setEnterAccDate(tMap.get("EnterAccDate")
						.toString());
				tLJFIGetSchema.setSaleChnl(tLJAGetSchema.getSaleChnl());
				tLJFIGetSchema.setAgentGroup(tLJAGetSchema.getAgentGroup());
				tLJFIGetSchema.setAgentCode(tLJAGetSchema.getAgentCode());
				tLJFIGetSchema.setSerialNo(tLJAGetSchema.getSerialNo());
				tLJFIGetSchema.setDrawer(tMap.get("Drawer").toString());
				tLJFIGetSchema.setDrawerID(tMap.get("DrawID").toString());
				tLJFIGetSchema.setOperator(tG.Operator);
				tLJFIGetSchema.setBankCode(tLJAGetSchema.getBankCode());
				tLJFIGetSchema.setBankAccNo(tLJAGetSchema.getBankAccNo());
				tLJFIGetSchema.setChequeNo(tMap.get("ChequeNo").toString());
				tLJFIGetSchema.setAccName(tMap.get("AccName").toString());

				mLJFIGetSet.add(tLJFIGetSchema);
				mLJAGetSet.add(tLJAGetSchema);
			}
		}

		mVData.add(mLJFIGetSet);
		mVData.add(mLJAGetSet);
		mVData.add(tG);

		CErrors tError = null;
		BatchPayUI tBatchPayUI = new BatchPayUI();
		if (tBatchPayUI.submitData(mVData, "VERIFY")) {
			tBatchPayUI.submitData(mVData, "CLAIM");
			tError = tBatchPayUI.mErrors;
		} else {
			tError = tBatchPayUI.mErrors;
		}

		if (tError.needDealError()) {
			this.mErrors.addOneError("失败，原因:" + tError.getFirstError());
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 返回查询条件，用于前台界面展示
	 */
	public String getActuGetNo() {
		if (exList.size() == 0) {
			return "('')";
		}
		String sql = "(";
		for (int i = 0; i < exList.size() - 1; i++) {
			sql = sql + "'" + ((Map) exList.get(i)).get("ActuGetNo").toString()
					+ "',";
		}
		sql = sql
				+ "'"
				+ ((Map) exList.get(exList.size() - 1)).get("ActuGetNo")
						.toString() + "')";
		return sql;
	}

	public static void main(String[] arr) {
		VData tInputData = new VData();
		TransferData tTransferData = new TransferData();
		GlobalInput tGl = new GlobalInput();

		tGl.ManageCom = "86";
		tGl.ComCode = "86";
		tGl.Operator = "cwad";

		tTransferData.setNameAndValue("fileName", "E:\\手续费付费清单_cwad_5317.xls");
		tInputData.add(tGl);
		tInputData.add(tTransferData);

		BatchPayReadFileBL tBatchPayReadFileBL = new BatchPayReadFileBL();
		tBatchPayReadFileBL.submitData(tInputData, "READ");
		System.out.println(tBatchPayReadFileBL.mErrors.getFirstError());
		System.out.println(tBatchPayReadFileBL.getActuGetNo());
	}
}
