package com.sinosoft.lis.finfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class FFInvoiceDataReadFileUI 
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();

    /** 传出数据的容器 */
    private VData mResult = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private String InvoiceCode = "";
    private String InvoiceNo = "";
    private String ComCode = "";
    

    public FFInvoiceDataReadFileUI ()
    {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"READ"和""
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        // 数据操作字符串拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        System.out.println("---FFInvoiceDataReadFileBL BEGIN---");
        FFInvoiceDataReadFileBL tFFInvoiceDataReadFileBL = new FFInvoiceDataReadFileBL();

        if (!tFFInvoiceDataReadFileBL.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tFFInvoiceDataReadFileBL.mErrors);
            System.out.println("FFInvoiceDataReadFileBL处理失败:" + mErrors.getFirstError());
            mResult.clear();
            mResult.add(mErrors.getFirstError());
            return false;
        }
        else
        {
        	InvoiceCode = tFFInvoiceDataReadFileBL.getInvoiceCode();
        	InvoiceNo = tFFInvoiceDataReadFileBL.getInvoiceNo();
        	ComCode = tFFInvoiceDataReadFileBL.getComCode();
        	
        }
        System.out.println("---FFInvoiceDataReadFileBL END---");

        return true;
    }


    /**
     * 用于签单显示导入信息
     * 
     * @return invoicecode字符串，使用“，”分隔
     */
    public String getInvoiceCode()
    {
        System.out.println("--where--InvoiceCode--:" + InvoiceCode);
        return InvoiceCode;
    }
    public String getInvoiceNo()
    {
        System.out.println("--where--InvoiceNo--:" + InvoiceNo );
        return InvoiceNo;
    }
    public String getComCode()
    {
        System.out.println("--where--ComCode--:" + ComCode);
        return ComCode;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {

    }

}
