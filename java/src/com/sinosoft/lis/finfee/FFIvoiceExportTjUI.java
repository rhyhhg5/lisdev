/**
 * 2007-5-22
 */
package com.sinosoft.lis.finfee;

import java.util.List;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class FFIvoiceExportTjUI
{
    public CErrors mErrors = new CErrors();

    private List mResult;

    public FFIvoiceExportTjUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("EXPORT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            FFIvoiceExportTjBL tFFIvoiceExportTjBL = new FFIvoiceExportTjBL();
            System.out.println("Start FFIvoiceExportTjBL Submit ...");
            if (!tFFIvoiceExportTjBL.submitData(cInputData, cOperate))
            {
                if (tFFIvoiceExportTjBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tFFIvoiceExportTjBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData",
                            "FFIvoiceExportTjBL 发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tFFIvoiceExportTjBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("submitData", "意外错误");
            return false;
        }
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FFIvoiceExportTjUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public List getResult()
    {
        return mResult;
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "group";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("invoiceStartDate", "2007-5-1");
        tTransferData.setNameAndValue("invoiceEndDate", "2007-5-31");

        tTransferData.setNameAndValue("hostBasePath",
                "D:/hmyc/projects/PICCH/ui");
        tTransferData.setNameAndValue("prtTemplate",
                "prtXMLTemplate/liaoning_prtxml.properties");
        tTransferData.setNameAndValue("outPath", "printdata");

        VData tVDate = new VData();
        tVDate.add(tGlobalInput);
        tVDate.add(tTransferData);

        try
        {
            FFIvoiceExportHnBL tFFIvoiceExportHnBL = new FFIvoiceExportHnBL();
            if (!tFFIvoiceExportHnBL.submitData(tVDate, "EXPORT"))
            {
                System.out.println("failed...");
            }
            else
            {
                System.out.println("finished...");
            }
        }
        catch (Exception ex)
        {
            System.out.println("failed...");
        }
    }
}
