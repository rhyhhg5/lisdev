package com.sinosoft.lis.finfee;

import java.util.GregorianCalendar;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;

public class GrpSendToBankConfirmBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();;

    /** 往界面传输数据的容器 */
    private VData mResult = new VData();

    private TransferData mTransferData = new TransferData();

    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();

    private LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();

    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    private LCGrpAppntSchema mLCGrpAppntSchema = new LCGrpAppntSchema();

    private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();

    private LJTempFeeClassSchema mLJTempFeeClassSchema = new LJTempFeeClassSchema();

    private LYReturnFromBankBSet mLYReturnFromBankBSet = new LYReturnFromBankBSet();

    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();

    private GlobalInput mGlobalInput = new GlobalInput();

    private String mOperate;

    private String getNoticeNo = "";

    private String SaveUnSuccreason = "";

    private boolean submitedFlag = false;// 由于原程序逻辑混乱，基本无法在原基础上改造，但没有时间全部重做，暂时用该标志表示是否提交过数据库。

    public GrpSendToBankConfirmBL()
    {
    }

    private MMap map = new MMap();

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        System.out.println("---GrpSendToBankConfirmBL BEGIN---");
        mOperate = cOperate;
        if (!getInputData(cInputData))
        {
            System.out.println("getinputdata failed");
            return false;
        }
//      by gzh 校验印刷号是否为空 20110725
        if(tLCGrpContSchema.getPrtNo() == null || "".equals(tLCGrpContSchema.getPrtNo())){
//        	 @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpSendToBankConfirmBL";
            tError.functionName = "submitData";
            tError.errorMessage = "保单印刷号为空！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpSendToBankConfirmBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败GrpSendToBankConfirmBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (!prepareOutputData())
        {
            return false;
        }

        //数据提交

        GrpSendToBankConfirmBLS tGrpSendToBankConfirmBLS = new GrpSendToBankConfirmBLS();

        if (!submitedFlag
                && !tGrpSendToBankConfirmBLS.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tGrpSendToBankConfirmBLS.mErrors);
            CError tError = new CError();
            tError.moduleName = "GrpSendToBankConfirmBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("---GrpSendToBankConfirmBL END---");
        return true;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData()
    {

        if (mOperate.equals("PAYMODE"))
        {
            if (!changePaymode())
            {
                return false;
            }
        }
        if (mOperate.equals("ACC"))
        {
            if (!changeAccount())
            {
                return false;
            }
        }
        if (mOperate.equals("LOCK"))
        {
            if (!lockBankData())
            {
                return false;
            }
        }
        if (mOperate.equals("UNLOCK"))
        {
            if (!unlockBankData())
            {
                return false;
            }
        }
        if (mOperate.equals("SaveUnSuccreason"))
        {
            if (!SaveUnSuccreasonDate())
            {
                return false;
            }
        }
        System.out.println("dealdate mOperate:" + mOperate);

        return true;
    }

    private boolean changePaymode()
    {
        MMap tMMap = new MMap();

        submitedFlag = true;// 如果为修改缴费方式，不再次进行数据提交。

        mInputData.clear();
        String payMode = tLCGrpContSchema.getPayMode();
        LCGrpContDB mLCGrpContDB = new LCGrpContDB();
        mLCGrpContDB.setPrtNo(tLCGrpContSchema.getPrtNo());
        LCGrpContSet mLCGrpContSet = mLCGrpContDB.query();
        mLCGrpContSchema = mLCGrpContSet.get(1);

        LCGrpAppntDB mLCGrpAppntDB = new LCGrpAppntDB();
        mLCGrpAppntDB.setPrtNo(tLCGrpContSchema.getPrtNo());
        LCGrpAppntSet mLCGrpAppntSet = mLCGrpAppntDB.query();
        mLCGrpAppntSchema = mLCGrpAppntSet.get(1);
        LCGrpPolDB mLCGrpPolDB = new LCGrpPolDB();
        mLCGrpPolDB.setPrtNo(tLCGrpContSchema.getPrtNo());
        mLCGrpPolSet = mLCGrpPolDB.query();

        //修改保单缴费方式和帐户信息
        mLCGrpContSchema.setPayMode(payMode);
        mLCGrpContSchema.setBankCode(tLCGrpContSchema.getBankCode());
        mLCGrpContSchema.setAccName(tLCGrpContSchema.getAccName());
        mLCGrpContSchema.setBankAccNo(tLCGrpContSchema.getBankAccNo());
        mLCGrpContSchema.setModifyDate(PubFun.getCurrentDate());
        mLCGrpContSchema.setModifyTime(PubFun.getCurrentTime());
        mLCGrpContSchema.setOperator(mGlobalInput.Operator);
        //修改投保人缴费方式和帐户信息
        mLCGrpAppntSchema.setClaimBankCode(tLCGrpContSchema.getBankCode());
        mLCGrpAppntSchema.setClaimAccName(tLCGrpContSchema.getAccName());
        mLCGrpAppntSchema.setClaimBankAccNo(tLCGrpContSchema.getBankAccNo());
        mLCGrpAppntSchema.setModifyDate(PubFun.getCurrentDate());
        mLCGrpAppntSchema.setModifyTime(PubFun.getCurrentTime());
        mLCGrpAppntSchema.setOperator(mGlobalInput.Operator);
        //修改险种缴费方式
        for (int m = 1; m <= mLCGrpPolSet.size(); m++)
        {
            mLCGrpPolSet.get(m).setPayMode(payMode);
        }

        tMMap.put(mLCGrpContSchema, SysConst.UPDATE);
        tMMap.put(mLCGrpAppntSchema, SysConst.UPDATE);
        tMMap.put(mLCGrpPolSet, SysConst.UPDATE);

        mOperate = "PAYMODECASH";//默认为删除暂收数据

        if (StrTool.cTrim(mLCGrpContSchema.getUWFlag()).equals("4")
                || StrTool.cTrim(mLCGrpContSchema.getUWFlag()).equals("9"))
        {
            String tStrSql = " select * from LJTempFee ljtf "
                    + " where 1 = 1 and ljtf.EnterAccDate is null "
                    + " and ljtf.OtherNo = '" + mLCGrpContSchema.getPrtNo() + "' ";
            LJTempFeeDB mLJTempFeeDB = new LJTempFeeDB();
            mLJTempFeeSet = mLJTempFeeDB.executeQuery(tStrSql);

            if (mLJTempFeeSet.size() > 0)
            {
                // 如果存在未到帐的暂收，全部清除
                MMap tDelMap = null;
                tDelMap = delFinFee(mLCGrpContSchema);
                if (tDelMap == null)
                {
                    return false;
                }
                tMMap.add(tDelMap);
                tDelMap = null;
                // --------------------
            }

            if (payMode.equals("4"))
            {
                MMap tTempFeeMap = null;
                tTempFeeMap = dealTempFeeDataOfBank(mLCGrpContSchema);
                if (tTempFeeMap == null)
                {
                    return false;
                }
                tMMap.add(tTempFeeMap);
                tTempFeeMap = null;
                // --------------------
            }
        }

        if (!submit(tMMap))
        {
            return false;
        }

        return true;
    }

    private boolean changeAccount()
    {
        mInputData.clear();
        if(tLCGrpContSchema.getPrtNo() == null || "".equals(tLCGrpContSchema.getPrtNo()))
        {
            CError tError = new CError();
            tError.moduleName = "GrpSendToBankConfirmBL";
            tError.functionName = "changeAccount";
            tError.errorMessage = "数据查询失败GrpSendToBankConfirmBL-->保单的印刷号为空，无法进行修改!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LCGrpContDB mLCGrpContDB = new LCGrpContDB();
        mLCGrpContDB.setPrtNo(tLCGrpContSchema.getPrtNo());
        LCGrpContSet mLCGrpContSet = mLCGrpContDB.query();
        mLCGrpContSchema = mLCGrpContSet.get(1);
        mLCGrpContSchema.setBankCode(tLCGrpContSchema.getBankCode());
        mLCGrpContSchema.setAccName(tLCGrpContSchema.getAccName());
        mLCGrpContSchema.setBankAccNo(tLCGrpContSchema.getBankAccNo());
        mLCGrpContSchema.setModifyDate(PubFun.getCurrentDate());
        mLCGrpContSchema.setModifyTime(PubFun.getCurrentTime());
        mLCGrpContSchema.setOperator(mGlobalInput.Operator);

        LCGrpAppntDB mLCGrpAppntDB = new LCGrpAppntDB();
        mLCGrpAppntDB.setPrtNo(tLCGrpContSchema.getPrtNo());
        LCGrpAppntSet mLCGrpAppntSet = mLCGrpAppntDB.query();
        mLCGrpAppntSchema = mLCGrpAppntSet.get(1);
        mLCGrpAppntSchema.setClaimBankCode(tLCGrpContSchema.getBankCode());
        mLCGrpAppntSchema.setClaimAccName(tLCGrpContSchema.getAccName());
        mLCGrpAppntSchema.setClaimBankAccNo(tLCGrpContSchema.getBankAccNo());
        mLCGrpAppntSchema.setModifyDate(PubFun.getCurrentDate());
        mLCGrpAppntSchema.setModifyTime(PubFun.getCurrentTime());
        mLCGrpAppntSchema.setOperator(mGlobalInput.Operator);
        String strSql = "select * from ljtempfeeclass where tempfeeno in ("
                + "select distinct tempfeeno from ljtempfee where otherno='"
                + tLCGrpContSchema.getPrtNo() + "') and EnterAccDate is null";
        LJTempFeeClassSet mLJTempFeeClassSet = mLJTempFeeClassSchema.getDB()
                .executeQuery(strSql);
        if (mLJTempFeeClassSet.size() > 0)
        {
            mLJTempFeeClassSchema = mLJTempFeeClassSet.get(1);
            mLJTempFeeClassSchema.setBankCode(tLCGrpContSchema.getBankCode());
            mLJTempFeeClassSchema.setAccName(tLCGrpContSchema.getAccName());
            mLJTempFeeClassSchema.setBankAccNo(tLCGrpContSchema.getBankAccNo());
        }
        else
        {
            //如果没有暂收数据，则置空LJTempFeeClassSchema 
            mLJTempFeeClassSchema = null;
        }

        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setGetNoticeNo(getNoticeNo);
        if (tLJSPayDB.getInfo())
        {
            mLJSPaySchema = tLJSPayDB.getSchema();
            mLJSPaySchema.setBankCode(tLCGrpContSchema.getBankCode());
            mLJSPaySchema.setAccName(tLCGrpContSchema.getAccName());
            mLJSPaySchema.setBankAccNo(tLCGrpContSchema.getBankAccNo());
            mLJSPaySchema.setCanSendBank("0");//在修改缴费信息或缴费方式的同时系统会自动对该笔数据进行解锁
        }
        mInputData.add(mLJTempFeeClassSchema);
        mInputData.add(mLJSPaySchema);
        mInputData.add(mLCGrpContSchema);
        mInputData.add(mLCGrpAppntSchema);
        System.out.println(mOperate);
        return true;
    }

    private boolean lockBankData()
    {

        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setGetNoticeNo(getNoticeNo);
        LJSPaySet tLJSPaySet = tLJSPayDB.query();
        mLJSPaySchema = tLJSPaySet.get(1);
        mLJSPaySchema.setCanSendBank("1");
        return true;
    }

    private boolean unlockBankData()
    {

        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setGetNoticeNo(getNoticeNo);
        LJSPaySet tLJSPaySet = tLJSPayDB.query();
        mLJSPaySchema = tLJSPaySet.get(1);
        mLJSPaySchema.setCanSendBank("0");
        mLJSPaySchema.setStartPayDate(PubFun.getCurrentDate());
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        tLCGrpContSchema.setSchema((LCGrpContSchema) cInputData
                .getObjectByObjectName("LCGrpContSchema", 0));
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        this.getNoticeNo = (String) mTransferData.getValueByName("GetNoticeNo");
        SaveUnSuccreason = (String) mTransferData
                .getValueByName("SaveUnSuccreason");
        return true;
    }

    /**
     *准备需要保存的数据
     **/
    private boolean prepareOutputData()
    {
        if (mOperate.equals("LOCK"))
        {
            mInputData.add(mLJSPaySchema);
        }
        if (mOperate.equals("UNLOCK"))
        {
            mInputData.add(mLJSPaySchema);
        }

        return true;
    }

    /**
     * 出错处理
     * @param szFunc String
     * @param szErrMsg String
     */
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LCGrpContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean SaveUnSuccreasonDate()
    {
        LYReturnFromBankBDB tLYReturnFromBankBDB = new LYReturnFromBankBDB();
        LYReturnFromBankBSet tLYReturnFromBankBSet = new LYReturnFromBankBSet();
        LYReturnFromBankBSchema tLYReturnFromBankBSchema = new LYReturnFromBankBSchema();
        String tSQL = "select * from lyreturnfrombankb where paycode='"
                + getNoticeNo + "' order by serialno desc ";
        tLYReturnFromBankBSet = tLYReturnFromBankBDB.executeQuery(tSQL);
        if (tLYReturnFromBankBSet.size() > 0)
        {
            tLYReturnFromBankBSchema = tLYReturnFromBankBSet.get(1);
            tLYReturnFromBankBSchema.setBankUnSuccReason(this.SaveUnSuccreason);
            mLYReturnFromBankBSet.add(tLYReturnFromBankBSchema);
        }
        MMap tMMap = new MMap();
        tMMap.put(mLYReturnFromBankBSet, "UPDATE");
        VData tmpVData = new VData();
        PubSubmit tPubSubmit = new PubSubmit();
        tmpVData.add(tMMap);
        if (!tPubSubmit.submitData(tmpVData, ""))
        {
            buildError("SaveUnSuccreasonDate", "保存转帐失败原因失败!");
            return false;
        }

        return true;
    }

    private MMap delFinFee(LCGrpContSchema cContInfo)
    {
        MMap tMMap = new MMap();

        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(cContInfo.getPrtNo());
        LJSPaySet tLJSPaySet = tLJSPayDB.query();
        if (tLJSPaySet.size() > 0)
        {
            for (int i = 1; i <= tLJSPaySet.size(); i++)
            {
                if (StrTool.cTrim(tLJSPaySet.get(i).getBankOnTheWayFlag())
                        .equals("1"))
                {
                    CError tError = new CError();
                    tError.moduleName = "ReManuUWAfterInitService";
                    tError.functionName = "getInputData";
                    tError.errorMessage = "银行在途，不能核保订正！";
                    this.mErrors.addOneError(tError);
                    return null;
                }
            }
        }
        tMMap.put(tLJSPaySet, SysConst.DELETE);

        // 处理财务暂收
        MMap tDelMap = null;
        tDelMap = delTempFeeDatas(mLCGrpContSchema);
        if (tDelMap == null)
        {
            return null;
        }
        tMMap.add(tDelMap);
        tDelMap = null;
        // --------------------

        return tMMap;
    }

    /**
     * 处理暂收财务数据。
     * @param cContInfo
     * @return
     */
    private MMap delTempFeeDatas(LCGrpContSchema cContInfo)
    {
        MMap tMMap = new MMap();

        ExeSQL tExeSQL = new ExeSQL();
        String tResult = null;
        String tStrSql = null;

        // 如果存在银行在途，发盘数据未锁定，或财务录入但未作财务确认的数据时，不允许核保订正。
        tResult = null;
        tStrSql = null;
        tStrSql = " select 1 from LJTempFee ljtf "
                + " where 1 = 1 and ljtf.EnterAccDate is not null "
                + " and ljtf.ConfMakeDate is null and ljtf.OtherNoType = '5' "
                + " and ljtf.OtherNo = '"
                + cContInfo.getPrtNo()
                + "' union all "
                + " select 1 from LJSPay ljsp where 1 = 1 and (ljsp.BankOnTheWayFlag = '1' or ljsp.CanSendBank = '0') "
                + " and ljsp.OtherNo = '" + cContInfo.getPrtNo() + "' ";
        tResult = tExeSQL.getOneValue(tStrSql);
        if ("1".equals(tResult))
        {
            buildError("dealTempFeeDatas",
                    "存在银行在途、发盘数据未锁定，或财务录入但未作财务确认的数据，不允许核保订正。");
            return null;
        }
        tResult = null;
        tStrSql = null;
        // --------------------

        // 处理非到帐暂收
        tStrSql = null;

        String tPrtNo = cContInfo.getPrtNo();
        tStrSql = " select * from LJTempFee ljtf "
                + " where 1 = 1 and ljtf.EnterAccDate is null "
                + " and ljtf.OtherNo = '" + tPrtNo + "' ";
        LJTempFeeSet tTempFeeSet = new LJTempFeeDB().executeQuery(tStrSql);
        tMMap.put(tTempFeeSet, SysConst.DELETE);

        tStrSql = null;
        tStrSql = " select * from LJTempFeeClass ljtfc "
                + " where ljtfc.TempFeeNo in ("
                + " select ljtf.TempFeeNo from LJTempFee ljtf "
                + " where 1 = 1 and ljtf.EnterAccDate is null "
                + " and ljtf.OtherNo = '" + tPrtNo + "') ";
        LJTempFeeClassSet tTempFeeClassSet = new LJTempFeeClassDB()
                .executeQuery(tStrSql);
        tMMap.put(tTempFeeClassSet, SysConst.DELETE);
        tStrSql = null;
        // --------------------

        return tMMap;
    }

    /**
     * 产生银行发盘暂收数据。
     * @param cContInfo
     * @return
     */
    private MMap dealTempFeeDataOfBank(LCGrpContSchema cContInfo)
    {
        MMap tMMap = new MMap();

        String prtSeq = PubFun1
                .CreateMaxNo("PAYNOTICENO", cContInfo.getPrtNo());
        String serNo = PubFun1
                .CreateMaxNo("SERIALNO", cContInfo.getManageCom());

        GregorianCalendar Calendar = new GregorianCalendar();
        Calendar.setTime((new FDate()).getDate(PubFun.getCurrentDate()));
        Calendar.add(Calendar.DATE, 0);

        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
        tLJTempFeeSchema.setTempFeeNo(prtSeq);
        tLJTempFeeSchema.setTempFeeType("1");
        tLJTempFeeSchema.setRiskCode("000000");

        tLJTempFeeSchema.setAgentGroup(cContInfo.getAgentGroup());
        tLJTempFeeSchema.setAPPntName(cContInfo.getGrpName());
        tLJTempFeeSchema.setAgentCode(cContInfo.getAgentCode());
        tLJTempFeeSchema.setPayDate(Calendar.getTime());

        // 计算不足保费
        double tDifMoney = 0;
        tDifMoney = calDifPrem(cContInfo, null);
        if (tDifMoney >= 0)
        {
            System.out.println("暂收保费足额：【" + tDifMoney + "】");
            return new MMap();// 如果足额，不生成银行数据。
        }
        tDifMoney = -1 * tDifMoney;
        tLJTempFeeSchema.setPayMoney(tDifMoney);
        // --------------------

        tLJTempFeeSchema.setManageCom(cContInfo.getManageCom());
        tLJTempFeeSchema.setOtherNo(cContInfo.getPrtNo());
        tLJTempFeeSchema.setOtherNoType("5");
        tLJTempFeeSchema.setPolicyCom(cContInfo.getManageCom());
        tLJTempFeeSchema.setSerialNo(serNo);
        tLJTempFeeSchema.setConfFlag("0");
        tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
        tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
        tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());

        tMMap.put(tLJTempFeeSchema, SysConst.INSERT);

        LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
        tLJTempFeeClassSchema.setTempFeeNo(prtSeq);
        tLJTempFeeClassSchema.setPayMode("4");
        tLJTempFeeClassSchema.setPayDate(Calendar.getTime());
        tLJTempFeeClassSchema.setPayMoney(tDifMoney);
        tLJTempFeeClassSchema.setManageCom(cContInfo.getManageCom());
        tLJTempFeeClassSchema.setPolicyCom(cContInfo.getManageCom());

        String BankCode = cContInfo.getBankCode();
        String BankAccNo = cContInfo.getBankAccNo();
        String AccName = cContInfo.getAccName();
        tLJTempFeeClassSchema.setBankCode(BankCode);
        tLJTempFeeClassSchema.setBankAccNo(BankAccNo);
        tLJTempFeeClassSchema.setAccName(AccName);

        tLJTempFeeClassSchema.setSerialNo(serNo);
        tLJTempFeeClassSchema.setConfFlag("0");
        tLJTempFeeClassSchema.setOperator(mGlobalInput.Operator);
        tLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
        tLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());

        tMMap.put(tLJTempFeeClassSchema, SysConst.INSERT);

        return tMMap;
    }

    /**
     * 获取暂收保费与应缴保费差值。
     * 原则：差值 = 暂收保费 - 应缴保费
     * @param tContInfo
     * @return
     */
    private double calDifPrem(LCGrpContSchema tContInfo, String tAutoUWFlag)
    {
        ExeSQL tExeSQL = new ExeSQL();
        String tStrSql = null;
        String tResult = null;

        // 校验到帐保费是否足额。
        if (StrTool.cTrim(tAutoUWFlag).equals("1"))
        {
            // 自核通过的情况
            double tContSumPrem = 0;
            double tContTempFeeSumMoney = 0;

            // 获取整单保费，由于自核通过时，uwflag尚未更新数据库，因此视为该单下全部包含险种的总保费即为整单保费
            String tGrpContNo = tContInfo.getGrpContNo();
            tResult = null;
            tStrSql = null;
            tStrSql = "select nvl(sum(nvl(lcp.Prem, 0) + nvl(lcp.SupplementaryPrem, 0)), 0) from LCGrpPol lcp "
                    + " where lcp.GrpContNo = '" + tGrpContNo + "' ";
            tResult = tExeSQL.getOneValue(tStrSql);
            tContSumPrem = Double.parseDouble(tResult);
            tResult = null;
            tStrSql = null;
            // --------------------

            System.out.println("tContSumPrem:" + tContSumPrem);

            // 获取暂收总保费。
            String tPrtNo = tContInfo.getPrtNo();
            tResult = null;
            tStrSql = null;
            tStrSql = " select nvl(sum(ljtf.PayMoney), 0) from LJTempFee ljtf "
                    + " where ljtf.OtherNoType = '5' " // 团单暂收核销前标志
                    // + " and ljtf.RiskCode != '000000' " // 除去保全相关暂收数据
                    + " and ljtf.ConfFlag = '0' " // 可用销暂收数据
                    + " and ljtf.EnterAccDate is not null " // 财务到帐
                    + " and ljtf.OtherNo = '" + tPrtNo + "' ";
            tResult = tExeSQL.getOneValue(tStrSql);
            tContTempFeeSumMoney = Double.parseDouble(tResult);
            tResult = null;
            tStrSql = null;
            // --------------------

            System.out.println("tContTempFeeSumMoney:" + tContTempFeeSumMoney);
            System.out.println("Dif:" + (tContTempFeeSumMoney - tContSumPrem));

            return (tContTempFeeSumMoney - tContSumPrem);

        }
        else
        {
            String tPrtNo = tContInfo.getPrtNo();

            tResult = null;
            tStrSql = null;
            tStrSql = "select LF_PayMoneyNo('" + tPrtNo + "') from Dual";
            tResult = new ExeSQL().getOneValue(tStrSql);
            if ("".equals(tResult))
            {
                // 不会出现这种情况，为防止万一，如果产生，后台报错，但不阻断，不发通知书。
                String tStrErr = "以印刷号查询，未查到合同号。";
                System.out.println(tStrErr);
                buildError("getDifPrem", tStrErr);
                return 0;
            }
            double tOutPrem = 0;
            tOutPrem = Double.parseDouble(tResult);
            System.out.println("DifPrem:" + tOutPrem);
            tResult = null;
            tStrSql = null;
            return tOutPrem;
        }
    }

    /**
     * 调用PubSubmit将map中数据进行提交
     * @return boolean
     */
    private boolean submit(MMap cMMap)
    {
        VData data = new VData();
        data.add(cMMap);

        PubSubmit p = new PubSubmit();
        if (!p.submitData(data, ""))
        {
            System.out.println("提交数据失败");
            buildError("submitData", "提交数据失败");
            return false;
        }

        return true;
    }
}
