package com.sinosoft.lis.finfee;

/**
 * <p>Title: FinBankAddBL</p>
 * <p>Description:添加银行 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft</p>
 * @author : zhangjun
 * @date:2006-04-05
 * @version 1.0
 */
import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.ClaimDayBalanceBL;

public class MineTestBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	// private VData mResult = new VData();
	private String mActugetno;
	private String mGetbankcode;
	private String mGetbankaccno;
	private String mPayMode;
	private String mEnterAccDate;
	private TransferData mAddElement = new TransferData(); // 获取时间
	private GlobalInput mGlobalInput = new GlobalInput(); // 全局变量
	private LJAGetSchema mLjagetSchema = new LJAGetSchema();
	private LJAGetSet mLJAGetSet = new LJAGetSet();
	private LJFIGetSet mLJFIGetSet = new LJFIGetSet();
    private LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
	private String mResult;

	public MineTestBL() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		try {
			if (!cOperate.equals("DOPOSITIVE")
					&& !cOperate.equals("DONEGATIVE")
					&& !cOperate.equals("REPAIR") && !cOperate.equals("DELETE")) {
				buildError("submitData", "不支持的操作字符串");
				return false;
			}
			if (!getInputData(cInputData)) {
				return false;
			}
			mResult = "";

			if (cOperate.equals("REPAIR")) {
				if (!updateLGAGet()) {
					return false;
				}

			} else if (cOperate.equals("DELETE")) {
				if (!delChildBank()) {
					return false;
				}

			} else if (cOperate.equals("DONEGATIVE")) {
				if (!negativeRecoil()) {
					return false;
				}
			} else if (cOperate.equals("DOPOSITIVE")) {
				if (!positiveRecoil()) {
					return false;
				}
			}
		} catch (Exception e) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "MineTestBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理错误! " + e.getMessage();
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) { // 打印付费
		// 全局变量
		mAddElement = (TransferData) cInputData.getObjectByObjectName(
				"TransferData", 0);
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		if (mGlobalInput == null) {
			buildError("getInputData", "没有得到足够的信息！");
			return false;
		}
		mActugetno = (String) mAddElement.getValueByName("reActuGetNo");
		mGetbankcode = (String) mAddElement.getValueByName("getbankcode");
		mGetbankaccno = (String) mAddElement.getValueByName("getbankaccno");
		mPayMode = (String) mAddElement.getValueByName("PayMode");
		mEnterAccDate = (String) mAddElement.getValueByName("mEnterAccDate");
		System.out.println("mActugetno:" + mActugetno + " mGetbankcode:"
				+ mGetbankcode + " mGetbankaccno:" + mGetbankaccno
				+ " mPayMode:" + mPayMode + "mEnterAccDate" + mEnterAccDate);
		return true;
	}

	public String getResult() {
		return mResult;
	}

	public LJAGetSchema getLJAGetSchema() {
		return mLjagetSchema;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "MineTestBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	private boolean updateLGAGet() {
		LJAGetDB mLJAGetDB = new LJAGetDB();
		mLJAGetDB.setActuGetNo(mActugetno);

		if (mLJAGetDB.getInfo()) {
			mLjagetSchema = mLJAGetDB.getSchema();

		}
		mLjagetSchema.setActuGetNo(mActugetno);
		mLjagetSchema.setInsBankCode(mGetbankcode);
		mLjagetSchema.setInsBankAccNo(mGetbankaccno);
		mLjagetSchema.setPayMode(mPayMode);
		mLjagetSchema.setEnterAccDate(mEnterAccDate);
		mLjagetSchema.setModifyDate(PubFun.getCurrentDate());
		mLjagetSchema.setModifyTime(PubFun.getCurrentTime());

		mLJAGetDB.setSchema(mLjagetSchema);
		if (!mLJAGetDB.update()) {
			buildError("MineTestBL->", "更新失败");
			return false;
		}
		return true;
	}

	private boolean delChildBank() {
		mLjagetSchema.setActuGetNo(mActugetno);
		mLjagetSchema.setOtherNo(mGetbankaccno);

		LJAGetDB mLJAGetDB = new LJAGetDB();
		mLJAGetDB.setActuGetNo(mActugetno);
		if (mLJAGetDB.getInfo()) {
			mLjagetSchema = mLJAGetDB.getSchema();
		}

		if (!mLJAGetDB.delete()) {
			buildError("FinBankAddBL->delChildBank", "删除子银行失败");
			return false;
		}

		return true;
	}

	// 充负
	private boolean negativeRecoil() {
		LJAGetDB tLJAGetDB = new LJAGetDB();
		tLJAGetDB.setActuGetNo(mActugetno);

		LJFIGetDB tLJFIGetDB = new LJFIGetDB();
		tLJFIGetDB.setActuGetNo(mActugetno);
		mLJFIGetSet = tLJFIGetDB.query();
		mLJAGetSet = tLJAGetDB.query();
		LJAGetSet tLJAGetSet = mLJAGetSet;
		LJFIGetSet tLJFIGetSet = mLJFIGetSet;
		String tNewActugetNo = PubFun1.CreateMaxNo("PAYNO", PubFun.getNoLimit(mGlobalInput.ManageCom));
		System.out.println("充负的ActugetNo" + tNewActugetNo);
		MMap tMMap = new MMap();
		VData tVData = new VData();
		if(tLJAGetSet != null && tLJAGetSet.size() > 0)
		{	
		tLJAGetSet.get(1).setActuGetNo(tNewActugetNo);
		tLJAGetSet.get(1).setEnterAccDate(PubFun.getCurrentDate());
		tLJAGetSet.get(1).setConfDate(PubFun.getCurrentDate());
		tLJAGetSet.get(1).setMakeTime(PubFun.getCurrentTime());
		tLJAGetSet.get(1).setModifyTime(PubFun.getCurrentTime());
		tLJAGetSet.get(1).setApproveDate(PubFun.getCurrentDate());
		tLJAGetSet.get(1).setMakeDate(PubFun.getCurrentDate());
		tLJAGetSet.get(1).setModifyDate(PubFun.getCurrentDate());
		tLJAGetSet.get(1).setSumGetMoney(
				-1 * mLJAGetSet.get(1).getSumGetMoney());
		tMMap.put(mLJAGetSet, "INSERT");
		}
		if(tLJFIGetSet != null && tLJFIGetSet.size() > 0 )
		{	
		tLJFIGetSet.get(1).setActuGetNo(tNewActugetNo);
		tLJFIGetSet.get(1).setEnterAccDate(PubFun.getCurrentDate());
		tLJFIGetSet.get(1).setConfDate(PubFun.getCurrentDate());
		tLJFIGetSet.get(1).setMakeTime(PubFun.getCurrentTime());
		tLJFIGetSet.get(1).setModifyTime(PubFun.getCurrentTime());
		tLJFIGetSet.get(1).setConfMakeDate(PubFun.getCurrentDate());
		tLJFIGetSet.get(1).setMakeDate(PubFun.getCurrentDate());
		tLJFIGetSet.get(1).setModifyDate(PubFun.getCurrentDate());
		tLJFIGetSet.get(1).setGetMoney(-1 * mLJFIGetSet.get(1).getGetMoney());
		tMMap.put(mLJFIGetSet, "INSERT");
		}
		tVData.add(tMMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(tVData, "INSERT")) {
			CError tError = new CError();
			tError.moduleName = "MineTestBL";
			tError.functionName = "PubSubmit";
			tError.errorMessage = "数据充负失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	// 充正
	private boolean positiveRecoil() {
		LJAGetDB tLJAGetDB = new LJAGetDB();
		tLJAGetDB.setActuGetNo(mActugetno);

		LJFIGetDB tLJFIGetDB = new LJFIGetDB();
		tLJFIGetDB.setActuGetNo(mActugetno);

		mLJFIGetSet = tLJFIGetDB.query();
		mLJAGetSet = tLJAGetDB.query();
		LJAGetSet tLJAGetSet = mLJAGetSet;
		LJFIGetSet tLJFIGetSet = mLJFIGetSet;
		String tNewActugetNo = PubFun1.CreateMaxNo("PAYNO", PubFun.getNoLimit(mGlobalInput.ManageCom));
		System.out.println("充正的ActugetNo" + tNewActugetNo);

		MMap tMMap = new MMap();
		VData tVData = new VData();
		if(tLJAGetSet != null && tLJAGetSet.size() > 0)
		{
		tLJAGetSet.get(1).setActuGetNo(tNewActugetNo);
		tLJAGetSet.get(1).setEnterAccDate(PubFun.getCurrentDate());
		tLJAGetSet.get(1).setConfDate(PubFun.getCurrentDate());
		tLJAGetSet.get(1).setMakeTime(PubFun.getCurrentTime());
		tLJAGetSet.get(1).setModifyTime(PubFun.getCurrentTime());
		tLJAGetSet.get(1).setApproveDate(PubFun.getCurrentDate());
		tLJAGetSet.get(1).setMakeDate(PubFun.getCurrentDate());
		tLJAGetSet.get(1).setModifyDate(PubFun.getCurrentDate());
		if( mLJAGetSet.get(1).getSumGetMoney() < 0){
		tLJAGetSet.get(1).setSumGetMoney(
				 -1 * mLJAGetSet.get(1).getSumGetMoney());
		}
		tMMap.put(mLJAGetSet, "INSERT");
		}
		if(tLJFIGetSet != null && tLJFIGetSet.size() > 0 )
		{
		tLJFIGetSet.get(1).setActuGetNo(tNewActugetNo);
		tLJFIGetSet.get(1).setEnterAccDate(PubFun.getCurrentDate());
		tLJFIGetSet.get(1).setConfDate(PubFun.getCurrentDate());
		tLJFIGetSet.get(1).setMakeTime(PubFun.getCurrentTime());
		tLJFIGetSet.get(1).setModifyTime(PubFun.getCurrentTime());
		tLJFIGetSet.get(1).setConfMakeDate(PubFun.getCurrentDate());
		tLJFIGetSet.get(1).setMakeDate(PubFun.getCurrentDate());
		tLJFIGetSet.get(1).setModifyDate(PubFun.getCurrentDate());
		if( mLJFIGetSet.get(1).getGetMoney() < 0){
			tLJFIGetSet.get(1).setGetMoney(
					 -1 * mLJAGetSet.get(1).getSumGetMoney());
			tMMap.put(mLJAGetSet, "INSERT");
			}
		tMMap.put(mLJFIGetSet, "INSERT");
		}
		tVData.add(tMMap);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(tVData, "INSERT")) {
			CError tError = new CError();
			tError.moduleName = "MineTestBL";
			tError.functionName = "PubSubmit";
			tError.errorMessage = "数据充正失败!";
			this.mErrors.addOneError(tError);
			return false;
		  }
		return true;
		}
	
	// 生成XML函数
	private boolean productXml(String msql) {
		return true;
	}

}
