package com.sinosoft.lis.finfee;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LYBankLogDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class FeeStateQueryBL {

	/** 收付费号 */
	private String feeNo;

	/** 业务号码 */
	private String otherNo;

	/** 收付费标志 */
	private String feeFlag;

	/** 应暂标记 */
	private String feeState;

	/** 银行编码 */
	private String bankCode;

	/** 银行帐号 */
	private String bankAccNo;

	/** 银行户名 */
	private String accName;

	/** 提盘批次流水号 */
	private String serialNo;

	/** 核销日期 */
	private String confDate;

	/** 发票打印标记 */
	private String printFlag;

	/** 收付费日期 */
	private String feeDate;

	/** 应收应付费日期 */
	private String shouldDate;

	/** 缴至日期 */
	private String payToDate;

	/** 返回用的集合 */
	private Set mSet = new HashSet();

	/**
	 * 入口函数
	 * 
	 * @param cInputData
	 * @param cOperate
	 * @return
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		System.out.println("---FeeStateQueryBL--start---");

		// 由于前台已经进行过大部分数据的查询，因此后台不再进行过多的查询
		if (!getInputData(cInputData)) {
			return false;
		}
		System.out.println("---End getInputData---");

		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		System.out.println("---End dealData---");

		return true;
	}

	/**
	 * 拷贝数据
	 * 
	 * @param mInputData
	 * @return
	 */
	private boolean getInputData(VData mInputData) {
		TransferData tTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);

		if (tTransferData == null) {
			System.out.println("---FeeStateQueryBL--传入的数据为空---");
			return false;
		}

		bankCode = (String) tTransferData.getValueByName("BankCode");
		bankAccNo = (String) tTransferData.getValueByName("BankAccNo");
		accName = (String) tTransferData.getValueByName("AccName");
		feeNo = (String) tTransferData.getValueByName("FeeNo");
		otherNo = (String) tTransferData.getValueByName("OtherNo");
		feeFlag = (String) tTransferData.getValueByName("FeeFlag");
		feeState = (String) tTransferData.getValueByName("FeeState");
		serialNo = (String) tTransferData.getValueByName("SerialNo");
		printFlag = (String) tTransferData.getValueByName("PrintFlag");
		confDate = (String) tTransferData.getValueByName("ConfDate");
		feeDate = (String) tTransferData.getValueByName("FeeDate");
		shouldDate = (String) tTransferData.getValueByName("ShouldDate");
		payToDate = (String) tTransferData.getValueByName("PayToDate");

		System.out.println("BankCode---" + bankCode);
		System.out.println("BankAccNo---" + bankAccNo);
		System.out.println("accName---" + accName);
		System.out.println("feeNo---" + feeNo);
		System.out.println("otherNo---" + otherNo);
		System.out.println("feeFlag---" + feeFlag);
		System.out.println("feeState---" + feeState);
		System.out.println("printFlag---" + printFlag);
		System.out.println("confDate---" + confDate);
		System.out.println("feeDate---" + feeDate);
		System.out.println("feeDate---" + shouldDate);
		System.out.println("feeDate---" + payToDate);

		return true;
	}

	/**
	 * 处理类
	 * 
	 * @return
	 */
	private boolean dealData() {

		try {
			if ("S".equals(feeFlag)) {
				payDeal();
			} else {
				getDeal();
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * 收费处理
	 */
	private void payDeal() {
		System.out.println("---收费数据处理---");
		if ("Should".equals(feeState)) {

			boolean bankOntheWay = false;
			String feeType = null;
			String canSendBank = null;

			String typeQuery = "select othernotype,bankonthewayflag,cansendbank from ljspay where getnoticeno='" + feeNo + "'";
			SSRS typeSSRS = (new ExeSQL()).execSQL(typeQuery);

			String bankOntheWayFlag = null;
			String otherNoType = null;

			if (typeSSRS.getMaxRow() == 0) {
				feeType = "1";
				bankOntheWay = false;
				canSendBank = "0";
			} else {
				otherNoType = typeSSRS.GetText(1, 1);
				bankOntheWayFlag = typeSSRS.GetText(1, 2);

				canSendBank = typeSSRS.GetText(1, 3);

				if ("1".equals(bankOntheWayFlag)) {
					if (canSendBank != null && canSendBank.matches("6|8")) {
						bankOntheWay = false;
					} else if (canSendBank != null && canSendBank.matches("7|9") && isNull(serialNo)) {
						bankOntheWay = false;
					} else {
						bankOntheWay = true;
					}
				} else {
					bankOntheWay = false;
				}

				if ("5".equals(otherNoType) || "9".equals(otherNoType) || "16".equals(otherNoType)) {
					feeType = "1";
				} else if ("1".equals(otherNoType) || "2".equals(otherNoType)) {
					feeType = "2";
				} else if ("3".equals(otherNoType) || "10".equals(otherNoType)) {
					feeType = "3";
				} else {
					feeType = "4";
				}
			}

			if (bankOntheWay) {
				// 银行在途
				LYBankLogDB tBankLog = new LYBankLogDB();
				tBankLog.setSerialNo(serialNo);

				if (tBankLog.getInfo()) {

					LDBankDB tLDBankDB = new LDBankDB();
					tLDBankDB.setBankCode(tBankLog.getBankCode());

					tLDBankDB.getInfo();
					String bankType = tLDBankDB.getOperator();
					//BY JL 金联平台的状态
					//if ("sys".equals(bankType)){
					if ("sys".equals(bankType) || "system".equals(bankType)) {
						// 集中代收付
						if (isNull(tBankLog.getSendDate())) {
							// 未发盘
							addInfo(GetFeeStateInfoBL.getStateInft("S", "OnTheWay", "1", "1"));
						} else if (isNull(tBankLog.getDealState())) {
							// 已发盘未回盘
							addInfo(GetFeeStateInfoBL.getStateInft("S", "OnTheWay", "2", "1"));
						} else {
							mSet.add("集中代收付数据查询异常");
						}
					} else if ("ybt".equals(bankType)) {
						// 总公司邮储
						if (isNull(tBankLog.getDealState())) {
							// 已发盘未回盘
							addInfo(GetFeeStateInfoBL.getStateInft("S", "OnTheWay", "2", "2"));
						} else {
							mSet.add("总公司邮储数据查询异常");
						}

					} else {
						// 银行接口
						if (isNull(tBankLog.getOutFile())) {
							// 未生成文件
							addInfo(GetFeeStateInfoBL.getStateInft("S", "OnTheWay", "1", "3"));
						} else if (isNull(tBankLog.getInFile())) {
							// 未文件返回
							addInfo(GetFeeStateInfoBL.getStateInft("S", "OnTheWay", "2", "3"));
						} else if (isNull(tBankLog.getDealState())) {
							// 未返回处理
							addInfo(GetFeeStateInfoBL.getStateInft("S", "OnTheWay", "3", "3"));
						} else {
							mSet.add("银行接口数据查询异常");
						}
					}
				} else {
					mSet.add("银行转账数据查询异常");
				}
			} else {
				if (checkPayMode(feeType)) {

					boolean flag = true;

					if (canSendBank == null || "".equals(canSendBank) || "0".equals(canSendBank)) {
						flag = true;
					} else if ("8".equals(canSendBank) || "9".equals(canSendBank)) {
						addInfo(GetFeeStateInfoBL.getStateInft("S", "ShouldFee", "8", feeType));
						flag = false;
					} else if ("6".equals(canSendBank)) {
						addInfo(GetFeeStateInfoBL.getStateInft("S", "ShouldFee", "6", feeType));
						flag = false;
					} else if ("7".equals(canSendBank)) {
						addInfo(GetFeeStateInfoBL.getStateInft("S", "ShouldFee", "7", feeType));
						flag = false;
					} else {
						addInfo(GetFeeStateInfoBL.getStateInft("S", "ShouldFee", "1", feeType));
						flag = false;
					}

					// 校验是否已到缴费日期
					if (shouldDate == null) {

						addInfo(GetFeeStateInfoBL.getErrorInfo("4"));
						flag = false;
					}
					if (!PubFun.getLaterDate(PubFun.getCurrentDate(), shouldDate).equals(PubFun.getCurrentDate())) {

						addInfo(GetFeeStateInfoBL.getErrorInfo("2"));
						flag = false;
					}

					if (payToDate != null && !PubFun.getBeforeDate(PubFun.getCurrentDate(), payToDate).equals(PubFun.getCurrentDate())) {
						addInfo(GetFeeStateInfoBL.getErrorInfo("3"));
						flag = false;
					}

					if (flag) {

						// 银行转账
						LDBankDB tLDBankDB = new LDBankDB();
						tLDBankDB.setBankCode(bankCode);
						if (tLDBankDB.getInfo()) {
							if ("1".equals(tLDBankDB.getCanSendFlag())) {

								// 银行接口提取标记
								boolean bankFlag = false;
								boolean uniteFlag = false;

								if (!isNull(tLDBankDB.getAgentPaySendF())) {
									bankFlag = true;
									addInfo(GetFeeStateInfoBL.getStateInft("S", "ShouldFee", "4", feeType));
								}

								String sql = "select (select operator from ldbank where bankcode=bankunitecode),unitegroupcode,bankunitecode from ldbankunite where bankcode='"
										+ bankCode + "'";
								SSRS tSSRS = (new ExeSQL()).execSQL(sql);
								if (tSSRS.getMaxRow() != 0) {
									for (int index = 1; index <= tSSRS.getMaxRow(); index++) {
										String banktype = tSSRS.GetText(index, 1);

										if ("sys".equals(banktype) || "system".equals(banktype)) {

											String groupcode = tSSRS.GetText(index, 2);
											if ("1".equals(groupcode) || "3".equals(groupcode)) {
												
												if ("1".equals(otherNoType) || "3".equals(otherNoType)) {
													addInfo(GetFeeStateInfoBL.getErrorInfo("5"));
													continue;
												} 
												
												if (checkAccNo("S", tSSRS.GetText(1, 3))) {
													uniteFlag = true;
													addInfo(GetFeeStateInfoBL.getStateInft("S", "ShouldFee", "2", feeType));
												} else {
													addInfo(GetFeeStateInfoBL.getErrorInfo("1"));
												}

											} else {
												continue;
											}
										} else if ("ybt".equals(banktype)) {
											uniteFlag = true;
											addInfo(GetFeeStateInfoBL.getStateInft("S", "ShouldFee", "3", feeType));
										} else {
											uniteFlag = true;
											addInfo(GetFeeStateInfoBL.getStateInft("S", "ShouldFee", "4", feeType));
										}
									}
								}

								if (!bankFlag && !uniteFlag) {
									mSet.add("该银行编码不为核心系统签约银行，不能进行收费提盘操作。");
								}

							} else {
								mSet.add("该银行编码不为核心系统签约银行，不能进行提盘操作。");
							}

						} else {
							// 银行转账，无发盘银行，错误处理
							mSet.add("客户开户银行为空或不存在，银行接口不会对该笔数据进行提取。");
						}
					}
				} else {
					// 非银行转账
					addInfo(GetFeeStateInfoBL.getStateInft("S", "ShouldFee", "5", feeType));
				}
			}
		} else if ("Conf".equals(feeState)) {

			String feeType = null;

			String typeQuery = "select tempfeetype from ljtempfee where tempfeeno='" + feeNo + "'";
			SSRS typeSSRS = (new ExeSQL()).execSQL(typeQuery);

			String tempfeeType = typeSSRS.GetText(1, 1);

			if ("1".equals(tempfeeType) || "11".equals(tempfeeType) || "16".equals(tempfeeType)) {
				feeType = "1";
			} else if ("2".equals(tempfeeType)) {
				feeType = "2";
			} else if ("4".equals(tempfeeType) || "7".equals(tempfeeType)) {
				feeType = "3";
			} else {
				feeType = "4";
			}

			// 暂收实收部分
			if (isNull(confDate)) {
				// 未核销部分
				if (isNull(feeDate)) {
					// 未到账确认
					addInfo(GetFeeStateInfoBL.getStateInft("S", "TempFee", "1", feeType));
				} else {
					// 未到账确认
					addInfo(GetFeeStateInfoBL.getStateInft("S", "TempFee", "2", feeType));
				}
			} else {
				// 已核销部分
				if ("true".equals(printFlag)) {
					// 已打印发票
					addInfo(GetFeeStateInfoBL.getStateInft("S", "ConfFee", "2", feeType));
				} else {
					// 未打印发票
					addInfo(GetFeeStateInfoBL.getStateInft("S", "ConfFee", "1", feeType));
				}
			}
		}
	}

	/**
	 * 付费处理
	 */
	private void getDeal() {
		System.out.println("---付费数据处理---");
		if ("Should".equals(feeState)) {

			boolean bankOntheWay = false;

			String typeQuery = "select bankonthewayflag,cansendbank,othernotype from ljaget where actugetno='" + feeNo + "'";
			SSRS typeSSRS = (new ExeSQL()).execSQL(typeQuery);

			String bankOntheWayFlag = typeSSRS.GetText(1, 1);
			String canSendBank = typeSSRS.GetText(1, 2);
			String othernoType = typeSSRS.GetText(1, 3);

			if ("1".equals(bankOntheWayFlag)) {
				if (canSendBank != null && canSendBank.matches("6|8")) {
					bankOntheWay = false;
				} else if (canSendBank != null && canSendBank.matches("7|9") && isNull(serialNo)) {
					bankOntheWay = false;
				} else {
					bankOntheWay = true;
				}
			} else {
				bankOntheWay = false;
			}

			if (bankOntheWay) {
				// 银行在途
				LYBankLogDB tBankLog = new LYBankLogDB();
				tBankLog.setSerialNo(serialNo);

				if (tBankLog.getInfo()) {

					LDBankDB tLDBankDB = new LDBankDB();
					tLDBankDB.setBankCode(tBankLog.getBankCode());

					tLDBankDB.getInfo();
					String bankType = tLDBankDB.getOperator();
					if ("sys".equals(bankType) || "system".equals(bankType)) {
						// 集中代收付
						if (isNull(tBankLog.getSendDate())) {
							// 未发盘
							addInfo(GetFeeStateInfoBL.getStateInft("S", "OnTheWay", "1", "1"));
						} else if (isNull(tBankLog.getDealState())) {
							// 已发盘未回盘
							addInfo(GetFeeStateInfoBL.getStateInft("S", "OnTheWay", "2", "1"));
						} else {
							mSet.add("集中代收付数据查询异常");
						}
					} else if ("ybt".equals(bankType)) {
						// 总公司邮储
						if (isNull(tBankLog.getDealState())) {
							// 已发盘未回盘
							addInfo(GetFeeStateInfoBL.getStateInft("S", "OnTheWay", "2", "2"));
						} else {
							mSet.add("总公司邮储数据查询异常");
						}

					} else {
						// 银行接口
						if (isNull(tBankLog.getOutFile())) {
							// 未生成文件
							addInfo(GetFeeStateInfoBL.getStateInft("S", "OnTheWay", "1", "3"));
						} else if (isNull(tBankLog.getInFile())) {
							// 未文件返回
							addInfo(GetFeeStateInfoBL.getStateInft("S", "OnTheWay", "2", "3"));
						} else if (isNull(tBankLog.getDealState())) {
							// 未返回处理
							addInfo(GetFeeStateInfoBL.getStateInft("S", "OnTheWay", "3", "3"));
						} else {
							mSet.add("银行接口数据查询异常");
						}
					}
				} else {
					mSet.add("银行转账数据查询异常");
				}
			} else {
				if (checkGetMode()) {

					boolean flag = true;

					if (canSendBank == null || "".equals(canSendBank) || "0".equals(canSendBank)) {
						flag = true;
					} else if ("8".equals(canSendBank) || "9".equals(canSendBank)) {
						addInfo(GetFeeStateInfoBL.getStateInft("S", "ShouldFee", "8", null));
						flag = false;
					} else if ("6".equals(canSendBank)) {
						addInfo(GetFeeStateInfoBL.getStateInft("S", "ShouldFee", "6", null));
						flag = false;
					} else if ("7".equals(canSendBank)) {
						addInfo(GetFeeStateInfoBL.getStateInft("S", "ShouldFee", "7", null));
						flag = false;
					} else {
						addInfo(GetFeeStateInfoBL.getStateInft("S", "ShouldFee", "1", null));
						flag = false;
					}
					// 校验是否已到缴费日期
					if (shouldDate == null) {

						addInfo(GetFeeStateInfoBL.getErrorInfo("4"));
						flag = false;
					}
					if (!PubFun.getLaterDate(PubFun.getCurrentDate(), shouldDate).equals(PubFun.getCurrentDate())) {

						addInfo(GetFeeStateInfoBL.getErrorInfo("2"));
						flag = false;
					}
					if (flag) {
						// 银行转账
						LDBankDB tLDBankDB = new LDBankDB();
						tLDBankDB.setBankCode(bankCode);
						if (tLDBankDB.getInfo()) {
							if ("1".equals(tLDBankDB.getCanSendFlag())) {

								// 银行接口提取标记
								boolean bankFlag = false;
								boolean uniteFlag = false;

								if (!isNull(tLDBankDB.getAgentPaySendF())) {
									bankFlag = true;
									addInfo(GetFeeStateInfoBL.getStateInft("F", "ShouldFee", "4", null));
								}

								String sql = "select (select operator from ldbank where bankcode=bankunitecode),unitegroupcode,bankunitecode from ldbankunite where bankcode='"
										+ bankCode + "'";
								SSRS tSSRS = (new ExeSQL()).execSQL(sql);
								if (tSSRS.getMaxRow() != 0) {
									for (int index = 1; index <= tSSRS.getMaxRow(); index++) {
										String banktype = tSSRS.GetText(index, 1);

										if ("sys".equals(banktype) || "system".equals(banktype)) {
											String groupcode = tSSRS.GetText(index, 2);
											if ("2".equals(groupcode) || "3".equals(groupcode)) {

												if ("3".equals(othernoType)) {

													addInfo(GetFeeStateInfoBL.getErrorInfo("5"));
													continue;

												} else if ("5".equals(othernoType)) {

													String claimQuery = new ExeSQL().getOneValue("Select 1 From Llcase Where Caseno='" + otherNo
															+ "' with ur");
													if (!"1".equals(claimQuery)) {
														addInfo(GetFeeStateInfoBL.getErrorInfo("5"));
														continue;
													}
												}

												if (checkAccNo("F", tSSRS.GetText(1, 3))) {

													uniteFlag = true;
													addInfo(GetFeeStateInfoBL.getStateInft("F", "ShouldFee", "2", null));
												} else {

													addInfo(GetFeeStateInfoBL.getErrorInfo("1"));
												}
											} else {
												continue;
											}
										} else if ("ybt".equals(banktype)) {
											uniteFlag = true;
											addInfo(GetFeeStateInfoBL.getStateInft("F", "ShouldFee", "3", null));
										} else {
											uniteFlag = true;
											addInfo(GetFeeStateInfoBL.getStateInft("F", "ShouldFee", "4", null));
										}
									}
								}

								if (!bankFlag && !uniteFlag) {
									mSet.add("该银行编码不为核心系统签约银行，不能进行付费提盘操作。");
								}

							} else {
								mSet.add("该银行编码不为核心系统签约银行，不能进行提盘操作。");
							}

						} else {
							// 银行转账，无发盘银行，错误处理
							mSet.add("客户开户银行为空或不存在，银行接口不会对该笔数据进行提取。");
						}
					}

				} else {
					// 非银行转账
					addInfo(GetFeeStateInfoBL.getStateInft("F", "ShouldFee", "5", null));
				}
			}
		} else if ("Conf".equals(feeState)) {
			addInfo(GetFeeStateInfoBL.getStateInft("F", "ConfFee", "1", null));
		}
	}

	/**
	 * 校验集中代收付帐号信息
	 * 
	 * @param bankcode
	 * @return
	 */
	private boolean checkAccNo(String feeType, String bankcode) {

		String sql = "select * from ldcode where codetype='accnocheck" + feeType + "' and code='" + bankcode + "'";

		LDCodeDB tLDCodeDB = new LDCodeDB();
		LDCodeSet tLDCodeSet = tLDCodeDB.executeQuery(sql);
		if (tLDCodeSet != null && tLDCodeSet.size() > 0) {
			String checkReg = tLDCodeSet.get(1).getCodeName();
			if (bankAccNo != null && !bankAccNo.trim().matches(checkReg)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * 判断是否为银行转账
	 * 
	 * @return true--是 false--否
	 */
	private boolean checkPayMode(String feeType) {
		if ("2".equals(feeType)) {
			String sql = "select 1 from lccont where contno='" + otherNo + "' and conttype='1' and paymode='4' "
					+ " union all select 1 from lcgrpcont where grpcontno='" + otherNo + "' and paymode='4'";
			String paymode = (new ExeSQL()).getOneValue(sql);
			if (!isNull(paymode)) {
				return true;
			}
		} else {
			String sql = "select 1 from ljtempfeeclass where tempfeeno='" + feeNo + "' and paymode='4'";
			String paymode = (new ExeSQL()).getOneValue(sql);
			if (!isNull(paymode)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断是否为银行转账
	 * 
	 * @return true--是 false--否
	 */
	private boolean checkGetMode() {
		String sql = "select 1 from ljaget where actugetno='" + feeNo + "' and paymode='4'";
		String paymode = (new ExeSQL()).getOneValue(sql);
		if (!isNull(paymode)) {
			return true;
		}
		return false;
	}

	/**
	 * 是否为空
	 * 
	 * @return
	 */
	private boolean isNull(String str) {
		if (str == null || str.equals("") || str.equals("null")) {
			return true;
		}
		return false;
	}

	/**
	 * 添加返回信息
	 */
	public void addInfo(List list) {
		for (int index = 0; index < list.size(); index++) {
			mSet.add((String) list.get(index));
		}
	}

	/**
	 * 获取返回信息
	 * 
	 * @return
	 */
	public Set getResult() {
		return mSet;
	}

	public static void main(String[] arr) {
		VData mInputData = new VData();

		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("BankCode", "000000");
		tTransferData.setNameAndValue("FeeNo", "37000000");
		tTransferData.setNameAndValue("FeeType", "1");
		tTransferData.setNameAndValue("FeeFlag", "S");
		tTransferData.setNameAndValue("FeeState", "Conf");
		tTransferData.setNameAndValue("SerialNo", "");
		tTransferData.setNameAndValue("PrintFlag", "");
		tTransferData.setNameAndValue("ConfDate", "");
		tTransferData.setNameAndValue("FeeDate", "");

		mInputData.add(tTransferData);

		FeeStateQueryBL tFeeStateQueryBL = new FeeStateQueryBL();
		tFeeStateQueryBL.submitData(mInputData, "");

		Set tSet = tFeeStateQueryBL.getResult();

		Iterator tI = tSet.iterator();
		while (tI.hasNext()) {
			System.out.println(tI.next());
		}
	}
}
