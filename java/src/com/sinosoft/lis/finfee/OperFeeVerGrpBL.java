 package com.sinosoft.lis.finfee;

import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;
import java.lang.String;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class OperFeeVerGrpBL  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 数据操作字符串 */
  private String mOperate;
//实收总表  
  private LJAPayBL           mLJAPayBL            = new LJAPayBL();
//实收集体表
  private LJAPayGrpBL        mLJAPayGrpBL         = new LJAPayGrpBL();
//保费项表
  private LCPremSet          mLCPremSet           = new LCPremSet();
  private LCPremSet          mLCPremSetNew        = new LCPremSet();
//暂交费表 
  private LJTempFeeSet    mLJTempFeeSet     = new LJTempFeeSet();
  private LJTempFeeSet    mLJTempFeeSetNew  = new LJTempFeeSet();
//暂交费分类表 
  private LJTempFeeClassSet    mLJTempFeeClassSet     = new LJTempFeeClassSet();
  private LJTempFeeClassSet    mLJTempFeeClassSetNew  = new LJTempFeeClassSet();
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();  
  //业务处理相关变量

  public OperFeeVerGrpBL() {
  }
  public static void main(String[] args) {
    OperFeeVerGrpBL OperFeeVerGrpBL1 = new OperFeeVerGrpBL();
    LJAPayPersonBL mLJAPayPersonBL =new LJAPayPersonBL();    
    VData tv=new VData();
    tv.add(mLJAPayPersonBL);
    OperFeeVerGrpBL1.submitData(tv,"VERIFY");
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;  	
System.out.println("OperateData:  "+cOperate);
    

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
System.out.println("After getinputdata");
          
    //进行业务处理
    if (!dealData())
      return false;
System.out.println("After dealData！");    
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
System.out.println("After prepareOutputData");    
      
    System.out.println("Start LJAPayPerson BL Submit...");

    OperFeeVerGrpBLS tOperFeeVerGrpBLS=new OperFeeVerGrpBLS();
    tOperFeeVerGrpBLS.submitData(mInputData,cOperate);

    System.out.println("End LJAPayPerson BL Submit...");

    //如果有需要处理的错误，则返回
    if (tOperFeeVerGrpBLS.mErrors .needDealError())
    {
        this.mErrors .copyAllErrors(tOperFeeVerGrpBLS.mErrors ) ;
    }

    mInputData=null;
    return true;
  }

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData()
  {
    int i,iMax;
    boolean tReturn =false;
    String tNo="";
    String  CDate = CurrentDate;
    String  CTime = CurrentTime;
    
    //处理集体信息数据
    //添加纪录
    if(this.mOperate.equals("VERIFY"))
    {
//1-处理实收集体交费表
      mLJAPayGrpBL.setMakeDate(CDate);//入机日期
      mLJAPayGrpBL.setMakeTime(CTime);//入机时间
      mLJAPayGrpBL.setModifyDate(CDate);//最后一次修改日期
      mLJAPayGrpBL.setModifyTime(CTime);//最后一次修改时间

//2-实收总表，单项纪录        	  	
    mLJAPayBL.setMakeDate(CDate);
    mLJAPayBL.setMakeTime(CTime);
    mLJAPayBL.setModifyDate(CDate);
    mLJAPayBL.setModifyTime(CTime);
    
//3-处理收费项表，记录集，循环处理
    LCPremBL tLCPremBL;
    iMax=mLCPremSet.size() ;
    for (i=1;i<=iMax;i++)
    {
      tLCPremBL = new LCPremBL() ;
      tLCPremBL.setSchema(mLCPremSet.get(i).getSchema());
      tLCPremBL.setPayTimes(tLCPremBL.getPayTimes()+1);
      tLCPremBL.setModifyDate(CDate);
      tLCPremBL.setModifyTime(CTime);
      mLCPremSetNew.add(tLCPremBL);    
    }

//4-暂交费表
    LJTempFeeBL tLJTempFeeBL;
    iMax=mLJTempFeeSet.size() ;
    for (i=1;i<=iMax;i++)
    {
      tLJTempFeeBL = new LJTempFeeBL() ;
      tLJTempFeeBL.setSchema(mLJTempFeeSet.get(i).getSchema());
      tLJTempFeeBL.setConfFlag("1");
      mLJTempFeeSetNew.add(tLJTempFeeBL);    
    } 

//4-暂交费分类表
    LJTempFeeClassBL tLJTempFeeClassBL;
    iMax=mLJTempFeeClassSet.size() ;
    for (i=1;i<=iMax;i++)
    {
      tLJTempFeeClassBL = new LJTempFeeClassBL() ;
      tLJTempFeeClassBL.setSchema(mLJTempFeeClassSet.get(i).getSchema());
      tLJTempFeeClassBL.setConfFlag("1");
      mLJTempFeeClassSetNew.add(tLJTempFeeClassBL);    
    } 
      tReturn=true;       
}
    
    return tReturn ;
  }

  //从输入数据中得到所有对象
  //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData mInputData)
  {
    //实收总表 
    mLJAPayBL.setSchema((LJAPaySchema)mInputData.getObjectByObjectName("LJAPaySchema",0));
    // 实收集体交费表
    mLJAPayGrpBL.setSchema((LJAPayGrpSchema)mInputData.getObjectByObjectName("LJAPayGrpSchema",0));
    // 保费项表
    mLCPremSet.set((LCPremSet)mInputData.getObjectByObjectName("LCPremSet",0));
    //暂交费表
    mLJTempFeeSet.set((LJTempFeeSet)mInputData.getObjectByObjectName("LJTempFeeSet",0));
    //暂交费分类表
    mLJTempFeeClassSet.set((LJTempFeeClassSet)mInputData.getObjectByObjectName("LJTempFeeClassSet",0));
    
    if(mLJAPayBL==null || mLJAPayGrpBL ==null || mLCPremSet ==null ||mLJTempFeeSet ==null||mLJTempFeeClassSet ==null)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="OperFeeVerGrpBL";
      tError.functionName="getInputData";
      tError.errorMessage="没有得到足够的数据，请您确认!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }
  
  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData()
  {
    mInputData=new VData();  	
    try
    {    	
//1-处理实收集体交费表     
      mInputData.add(mLJAPayGrpBL);
//2-处理收费项表，记录集，循环处理      
      mInputData.add(mLCPremSetNew);      
//3-实收总表，单项纪录      
      mInputData.add(mLJAPayBL);
//4-暂交费表
      mInputData.add(mLJTempFeeSetNew);    
//5-暂交费分类表  
      mInputData.add(mLJTempFeeClassSetNew);          
      System.out.println("prepareOutputData:");      
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="OperFeeVerGrpBL";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  } 
}
 
 