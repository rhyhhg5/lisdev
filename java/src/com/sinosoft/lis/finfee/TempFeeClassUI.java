package com.sinosoft.lis.finfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TempFeeClassUI {
        //业务处理相关变量
    private String[] strResult;
    private VData mInputData;
    public CErrors mErrors = new CErrors();

    public TempFeeClassUI() {
    }
    public static void main(String[] args)
   {
    TempFeeClassUI TempFeeClassUI1 = new TempFeeClassUI();
    }


   //传输数据的公共方法
   public boolean submitData(VData cInputData, String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData = (VData) cInputData.clone();

    TempFeeClassBL tTempFeeClassBL = new TempFeeClassBL();
    System.out.println("Start TempFeeClass UI Submit...");
    tTempFeeClassBL.submitData(mInputData, cOperate);

    System.out.println("End TempFeeClass UI Submit...");

    //如果有需要处理的错误，则返回
    if (tTempFeeClassBL.mErrors.needDealError())
        this.mErrors.copyAllErrors(tTempFeeClassBL.mErrors);
    System.out.println("error num=" + mErrors.getErrorCount());
    mInputData = null;
    strResult = tTempFeeClassBL.getResult();
    return true;
   }

   public String[] getResult()
  {
    return strResult;
  }

}
