package com.sinosoft.lis.finfee;

import com.sinosoft.utility.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.JdbcUrl;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class PolFeeWithdrawBalanceBLS  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 数据操作字符串 */
  private String mOperate;

  public PolFeeWithdrawBalanceBLS() {
  }
  public static void main(String[] args) {
    PolFeeWithdrawBalanceBLS mPolFeeWithdrawBalanceBLS1 = new PolFeeWithdrawBalanceBLS();
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData)
  {
    boolean tReturn =false;
    System.out.println("Start PolFeeWithdrawBalance BLS Submit...");
    //信息保存
    {tReturn=save(cInputData);}

    if (tReturn)
      System.out.println("Save sucessful");
    else
      System.out.println("Save failed") ;

      System.out.println("End PolFeeWithdrawBalance BLS Submit...");

    return tReturn;
  }

//保存操作
  private boolean save(VData mInputData)
  {
   boolean tReturn =true;
    System.out.println("Start Save...");
    Connection conn = DBConnPool.getConnection();
    if (conn==null)
    {
    		// @@错误处理
    		CError tError = new CError();
		tError.moduleName = "PolFeeWithdrawBalanceBLS";
                tError.functionName = "saveData";
	        tError.errorMessage = "数据库连接失败!";
		this.mErrors .addOneError(tError) ;
 		return false;
    }
    try{
      conn.setAutoCommit(false);

// 个人保单表
      System.out.println("Start 个人保单表...");
      LCPolDB tLCPolDB=new LCPolDB(conn);
      tLCPolDB.setSchema((LCPolSchema)mInputData.getObjectByObjectName("LCPolSchema",0));
      if (!tLCPolDB.update())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLCPolDB.mErrors);
    		CError tError = new CError();
	        tError.moduleName = "PolFeeWithdrawBalanceBLS";
	        tError.functionName = "saveData";
	        tError.errorMessage = "个人保单表数据更新失败!";
	        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 	        return false;
      }

      /** 实付总表 */
      System.out.println("Start 实付总表...");
      LJAGetDB tLJAGetDB=new LJAGetDB(conn);
      tLJAGetDB.setSchema((LJAGetSchema)mInputData.getObjectByObjectName("LJAGetSchema",0));
      System.out.println("Get LJAGet");
      if (!tLJAGetDB.insert())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJAGetDB.mErrors);
    		CError tError = new CError();
		tError.moduleName = "PolFeeWithdrawBalanceBLS";
		tError.functionName = "saveData";
		tError.errorMessage = "实付总表数据保存失败!";
		this.mErrors .addOneError(tError) ;
        conn.rollback();
        conn.close();
 		return false;
      }

      /** 打印管理表 */
      System.out.println("Start 打印管理表...");
      LOPRTManagerDB tLOPRTManagerDB=new LOPRTManagerDB(conn);
      tLOPRTManagerDB.setSchema((LOPRTManagerSchema)mInputData.getObjectByObjectName("LOPRTManagerSchema",0));
      System.out.println("Get LOPRTManager");
      if (!tLOPRTManagerDB.insert())
      {
                    // @@错误处理
                this.mErrors.copyAllErrors(tLOPRTManagerDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "PolFeeWithdrawBalanceBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "打印管理表数据保存失败!";
                this.mErrors .addOneError(tError) ;
        conn.rollback();
        conn.close();
                 return false;
      }

      conn.commit() ;
      conn.close();
      System.out.println("commit over");
    }
    catch (Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="PolFeeWithdrawBalanceBLS";
      tError.functionName="submitData";
      tError.errorMessage=ex.toString();
      this.mErrors .addOneError(tError);
      try{conn.rollback() ;} catch(Exception e){}
      tReturn=false;
    }
    return tReturn;
  }
}