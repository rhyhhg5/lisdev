package com.sinosoft.lis.finfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import java.sql.Connection;

/**
 * <p>Title: Web业务系统到帐确认部分 </p>
 * <p>Description:数据库功能类 </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author WHN
 * @version 1.0
 */

public class SendToBankConfirmBLS
{
	//传输数据类
	private VData mInputData ;
	//错误处理类，每个需要错误处理的类中都放置该类
	public  CErrors mErrors = new CErrors();
	private LJSPaySchema mLJSPaySchema = new LJSPaySchema();
	private LCContSchema mLCContSchema = new LCContSchema();
	private LCAppntSchema mLCAppntSchema = new LCAppntSchema();
  private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
  private LJTempFeeClassSchema mLJTempFeeClassSchema = new LJTempFeeClassSchema();
  private LCPolSet   mLCPolSet =new LCPolSet();
  private String mOperate;

	public SendToBankConfirmBLS() {}


	//传输数据的公共方法
	public boolean submitData(VData cInputData,String cOperate)
	{
	    //首先将数据在本类中做一个备份
    this.mInputData = (VData)cInputData.clone();
    this.mOperate =cOperate;

    if (!getInputData()) return false;
    System.out.println("---End getInputData---");

    if (mOperate.equals("PAYMODECASH"))
    {
    	if (!saveDataCash())
      return false;
    }
    if (mOperate.equals("PAYMODEBANK"))
    {
    	if (!saveDataBank())
      return false;
    }
    if (mOperate.equals("ACC"))
    {
    	if (!saveDataAcc())
      return false;
    }
    if (mOperate.equals("LOCK")||mOperate.equals("UNLOCK"))
    {
    	if (!updateData())
      return false;
    }
    System.out.println("---End saveData---");

    return true;
	}

  private boolean getInputData()	{
    try {
    if (mOperate.equals("PAYMODECASH"))
    {
    mLCContSchema = (LCContSchema)mInputData.getObjectByObjectName("LCContSchema", 0);
    mLCAppntSchema = (LCAppntSchema)mInputData.getObjectByObjectName("LCAppntSchema", 0);
    mLJTempFeeSet = (LJTempFeeSet)mInputData.getObjectByObjectName("LJTempFeeSet", 0);
    mLJTempFeeClassSchema = (LJTempFeeClassSchema)mInputData.getObjectByObjectName("LJTempFeeClassSchema", 0);
    mLJSPaySchema = (LJSPaySchema)mInputData.getObjectByObjectName("LJSPaySchema", 0);
    mLCPolSet=(LCPolSet)mInputData.getObjectByObjectName("LCPolSet", 0);
   }
    if (mOperate.equals("PAYMODEBANK"))
    {
    mLCContSchema = (LCContSchema)mInputData.getObjectByObjectName("LCContSchema", 0);
    mLCAppntSchema = (LCAppntSchema)mInputData.getObjectByObjectName("LCAppntSchema", 0);
    mLJTempFeeSet = (LJTempFeeSet)mInputData.getObjectByObjectName("LJTempFeeSet", 0);
    mLJTempFeeClassSchema = (LJTempFeeClassSchema)mInputData.getObjectByObjectName("LJTempFeeClassSchema", 0);
    mLCPolSet=(LCPolSet)mInputData.getObjectByObjectName("LCPolSet", 0);
}
    if (mOperate.equals("ACC"))
    {
    mLCContSchema = (LCContSchema)mInputData.getObjectByObjectName("LCContSchema", 0);
    mLCAppntSchema = (LCAppntSchema)mInputData.getObjectByObjectName("LCAppntSchema", 0);
    mLJTempFeeClassSchema = (LJTempFeeClassSchema)mInputData.getObjectByObjectName("LJTempFeeClassSchema", 0);
    mLJSPaySchema = (LJSPaySchema)mInputData.getObjectByObjectName("LJSPaySchema", 0);
    System.out.println("getinputdata "+mOperate);
    }
    if (mOperate.equals("LOCK"))
    {
    mLJSPaySchema = (LJSPaySchema)mInputData.getObjectByObjectName("LJSPaySchema", 0);
    }
    if (mOperate.equals("UNLOCK"))
    {
    mLJSPaySchema = (LJSPaySchema)mInputData.getObjectByObjectName("LJSPaySchema", 0);
    }
    }
    catch (Exception e) {
      // @@错误处理
      e.printStackTrace();
      CError tError = new CError();
      tError.moduleName = "SendToBankConfirmBLS";
      tError.functionName = "getInputData";
      tError.errorMessage = "接收数据失败!!";
      this.mErrors.addOneError(tError) ;
      return false;
    }

    return true;
  }

	private boolean saveDataCash()
	{
    Connection conn=DBConnPool.getConnection();
    if (conn == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "SendToBankConfirmBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors.addOneError(tError) ;
      return false;
    }
    try {
           conn.setAutoCommit(false);

           // 修改部分
           LCContDB tLCContDB = new LCContDB(conn);
           tLCContDB.setSchema(mLCContSchema);
           if (!tLCContDB.update())
           {
           // @@错误处理
           conn.rollback() ;
           conn.close();
           System.out.println("LCCont Update Failed");
           return false;
           }
           LCPolDBSet tLCPolDBSet = new LCPolDBSet(conn);
           tLCPolDBSet.set(mLCPolSet);
           if (!tLCPolDBSet.update())
           {
                   // @@错误处理
                   conn.rollback() ;
                   conn.close();
                   System.out.println("LCPol Update Failed");
                   return false;
               }


           LCAppntDB tLCAppntDB = new LCAppntDB(conn);
           tLCAppntDB.setSchema(mLCAppntSchema);
           if (!tLCAppntDB.update())
           {
           // @@错误处理
           conn.rollback() ;
           conn.close();
           System.out.println("LCAppnt Update Failed");
           return false;
           }
           if(mLJTempFeeSet!=null){
           LJTempFeeDBSet tLJTempFeeDBSet = new LJTempFeeDBSet(conn);
           tLJTempFeeDBSet.set(mLJTempFeeSet);
           if (!tLJTempFeeDBSet.delete())
           {
           // @@错误处理
           conn.rollback() ;
           conn.close();
           System.out.println("LJTempFee Delete Failed");
           return false;
                }
           }
           if(mLJTempFeeClassSchema!=null){
           LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB(conn);
           tLJTempFeeClassDB.setSchema(mLJTempFeeClassSchema);
           if (!tLJTempFeeClassDB.delete())
           {
           // @@错误处理
           conn.rollback() ;
           conn.close();
           System.out.println("LJTempFeeClass Delete Failed");
           return false;
                    }
           }
           if (mLJSPaySchema!=null&&mLJSPaySchema.getGetNoticeNo()!=null)
           {
           LJSPayDB tLJSPayDB = new LJSPayDB(conn);
           tLJSPayDB.setSchema(mLJSPaySchema);
           if (!tLJSPayDB.delete())
           {
           // @@错误处理
           conn.rollback() ;
           conn.close();
           System.out.println("LJSPay Delete Failed");
           return false;
           }
           }

           conn.commit() ;
           conn.close();

         }
       catch (Exception ex)
              {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "SendToBankConfirmBLS";
              tError.functionName = "submitData";
              tError.errorMessage = ex.toString();
              this.mErrors.addOneError(tError);
              try {
                conn.rollback();
                conn.close();
              } catch (Exception e) {}
              return false;
              }
       return true;
  }

	private boolean saveDataBank()
	{
    Connection conn=DBConnPool.getConnection();
    if (conn == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "SendToBankConfirmBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors.addOneError(tError) ;
      return false;
    }
    try {
           conn.setAutoCommit(false);

           // 修改部分
           LCContDB tLCContDB = new LCContDB(conn);
           tLCContDB.setSchema(mLCContSchema);
           if (!tLCContDB.update())
           {
           // @@错误处理
           conn.rollback() ;
           conn.close();
           System.out.println("LCCont Update Failed");
           return false;
           }
           LCPolDBSet tLCPolDBSet = new LCPolDBSet(conn);
            tLCPolDBSet.set(mLCPolSet);
            if (!tLCPolDBSet.update())
            {
                    // @@错误处理
                    conn.rollback() ;
                    conn.close();
                    System.out.println("LCPol Update Failed");
                    return false;
                }

           LCAppntDB tLCAppntDB = new LCAppntDB(conn);
           tLCAppntDB.setSchema(mLCAppntSchema);
           if (!tLCAppntDB.update())
           {
           // @@错误处理
           conn.rollback() ;
           conn.close();
           System.out.println("LCAppnt Update Failed");
           return false;
           }
           if(mLJTempFeeSet!=null){
           LJTempFeeDBSet tLJTempFeeDBSet = new LJTempFeeDBSet(conn);
           tLJTempFeeDBSet.set(mLJTempFeeSet);
           if (!tLJTempFeeDBSet.insert())
           {
           // @@错误处理
           conn.rollback() ;
           conn.close();
           System.out.println("LJTempFee Insert Failed");
           return false;
           }
           }
           if(mLJTempFeeClassSchema!=null){
               LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB(conn);
               tLJTempFeeClassDB.setSchema(mLJTempFeeClassSchema);
               if (!tLJTempFeeClassDB.insert()) {
                   // @@错误处理
                   conn.rollback();
                   conn.close();
                   System.out.println("LJTempFeeClass Insert Failed");
                   return false;
               }
           }
           conn.commit() ;
           conn.close();
         }
       catch (Exception ex)
              {
                  ex.printStackTrace();
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "SendToBankConfirmBLS";
              tError.functionName = "submitData";
              tError.errorMessage = ex.toString();
              this.mErrors.addOneError(tError);
              try {
                conn.rollback();
                conn.close();
              } catch (Exception e) {}
              return false;
              }
       return true;
  }

	private boolean saveDataAcc()
	{
    Connection conn=DBConnPool.getConnection();
    if (conn == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "SendToBankConfirmBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors.addOneError(tError) ;
      return false;
    }
    try {
           conn.setAutoCommit(false);

           // 修改部分
           LCContDB tLCContDB = new LCContDB(conn);
           tLCContDB.setSchema(mLCContSchema);
           if (!tLCContDB.update())
           {
           // @@错误处理
           conn.rollback() ;
           conn.close();
           System.out.println("LCCont Update Failed");
           return false;
           }

           LCAppntDB tLCAppntDB = new LCAppntDB(conn);
           tLCAppntDB.setSchema(mLCAppntSchema);
           if (!tLCAppntDB.update())
           {
           // @@错误处理
           conn.rollback() ;
           conn.close();
           System.out.println("LCAppnt Update Failed");
           return false;
           }
           if(mLJTempFeeClassSchema!=null){
           LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB(conn);
           tLJTempFeeClassDB.setSchema(mLJTempFeeClassSchema);
           if (!tLJTempFeeClassDB.update())
           {
           // @@错误处理
           conn.rollback() ;
           conn.close();
           System.out.println("LJTempFeeClass Update Failed");
           return false;
           }
           }
           if (mLJSPaySchema!=null&&mLJSPaySchema.getGetNoticeNo()!=null)
           {
           LJSPayDB tLJSPayDB = new LJSPayDB(conn);
           tLJSPayDB.setSchema(mLJSPaySchema);
           if (!tLJSPayDB.update())
           {
           // @@错误处理
           conn.rollback() ;
           conn.close();
           System.out.println("LJSPay Update Failed");
           return false;
           }
           }

           conn.commit() ;
           conn.close();
           System.out.println("saveDataAcc");
         }
       catch (Exception ex)
              {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "SendToBankConfirmBLS";
              tError.functionName = "submitData";
              tError.errorMessage = ex.toString();
              this.mErrors.addOneError(tError);
              try {
                conn.rollback();
                conn.close();
              } catch (Exception e) {}
              return false;
              }
       return true;
  }

	private boolean updateData()
	{
    Connection conn=DBConnPool.getConnection();
    if (conn == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "SendToBankConfirmBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors.addOneError(tError) ;
      return false;
    }
    try {
           conn.setAutoCommit(false);

           // 修改部分
           LJSPayDB tLJSPayDB = new LJSPayDB(conn);
           tLJSPayDB.setSchema(mLJSPaySchema);
           if (!tLJSPayDB.update())
           {
           // @@错误处理
           conn.rollback() ;
           conn.close();
           System.out.println("LJSPay Update Failed");
           return false;
           }

           conn.commit() ;
           conn.close();

         }
       catch (Exception ex)
              {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "SendToBankConfirmBLS";
              tError.functionName = "submitData";
              tError.errorMessage = ex.toString();
              this.mErrors.addOneError(tError);
              try {
                conn.rollback();
                conn.close();
              } catch (Exception e) {}
              return false;
              }
       return true;
  }
}
