package com.sinosoft.lis.finfee;

import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCSerialnoHZSchema;
import com.sinosoft.lis.vdb.LCSerialnoHZDBSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class PayUnionserialnoApplyBL {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();
	// 操作人员
	private String mOperator;
	// 汇总批次号
	private String mSerialnoHZ;
	// 申请日期
	private String mApplyDate;
	// 管理机构
	private String mManageCom;
	//批次名
	private String mSerialnoHZName;

	public PayUnionserialnoApplyBL() {
	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		System.out.println("---PayUnionserialnoApplyBL BEGIN---");
		mOperator = cOperate;
		if (!getInputData(cInputData)) {
			System.out.println("getinputdata failed");
			return false;
		}

		if (!dealData()) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PayUnionserialnoApplyBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败PayUnionserialnoApplyBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean dealData() {
		
		mSerialnoHZ = PubFun1.CreateMaxNo("HZSerialno", 20);
	    System.out.println("生成的批次号为：: " + mSerialnoHZ);
        System.out.println("开始插入批次汇总表信息");
        
        // 生成汇总批次信息
        LCSerialnoHZSchema  mLCSerialnoHZSchema  = new LCSerialnoHZSchema();
        LCSerialnoHZDBSet mLCSerialnoHZDBSet = new LCSerialnoHZDBSet();
        mLCSerialnoHZSchema.setTranBatch(mSerialnoHZ);
        mLCSerialnoHZSchema.setTranBatchName(mSerialnoHZName);
        mLCSerialnoHZSchema.setApplyDate(mApplyDate);
        mLCSerialnoHZSchema.setoperator(mOperator);
        mLCSerialnoHZSchema.setMakeDate(PubFun.getCurrentDate());
        mLCSerialnoHZSchema.setMakeTime(PubFun.getCurrentTime());
        mLCSerialnoHZSchema.setManageCom(mManageCom);
        mLCSerialnoHZSchema.setModifyDate(PubFun.getCurrentDate());
        mLCSerialnoHZSchema.setModifyTime(PubFun.getCurrentTime());
        
        mLCSerialnoHZDBSet.add(mLCSerialnoHZSchema);
        mLCSerialnoHZDBSet.insert();
		return true;
	}
	public String getmSerialnoHZ(){
		return mSerialnoHZ;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		TransferData tTransferData = (TransferData) cInputData
				.getObjectByObjectName("TransferData", 0);
		mApplyDate = (String) tTransferData.getValueByName("ApplyDate");
		mManageCom = (String) tTransferData.getValueByName("ManageCom");
		mSerialnoHZName = (String) tTransferData.getValueByName("SerialnoHZName");
		mOperator = (String) tTransferData.getValueByName("Operator");

		return true;
	}


}
