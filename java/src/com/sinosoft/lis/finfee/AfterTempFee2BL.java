package com.sinosoft.lis.finfee;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.taskservice.BqXuQiHeXiaoTask;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LJTempFeeDB;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 *
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class AfterTempFee2BL implements AfterTempFeeBL
{
	public CErrors mErrors = new CErrors();
    private LJTempFeeSchema mLJTempFeeSchema = null;
    private GlobalInput mGlobalInput = null; //操作员信息

    private MMap map = new MMap();

    public AfterTempFee2BL()
    {
    }

    public MMap getSubmitMMap(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }
        if(!checkData())
        {
        	return map;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData()
    {
    	BqXuQiHeXiaoTask tBqXuQiHeXiaoTask = new BqXuQiHeXiaoTask(mLJTempFeeSchema.getTempFeeNo(),mGlobalInput);
    	if(!tBqXuQiHeXiaoTask.dealData())
    	{
    		mErrors.copyAllErrors(tBqXuQiHeXiaoTask.mErrors);
    		return false;
    	}


        return true;
    }
    private boolean checkData()
    {
        //如果已经核销则不能再次进行核销处理
    	String sql = "select 1 from ljapay a where getnoticeno = '" + mLJTempFeeSchema.getTempFeeNo() + "' with ur" ;
		String yiHeXiao = (new ExeSQL()).getOneValue(sql);
		if ((yiHeXiao!= null)&&(!yiHeXiao.equals("")))
		{
		   System.out.println("该应收["+mLJTempFeeSchema.getTempFeeNo()+"]已经核销！");
		   return false;
		}


        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData data)
    {
        mLJTempFeeSchema = (LJTempFeeSchema) data.getObjectByObjectName("LJTempFeeSchema", 0);
        mGlobalInput = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        if(mLJTempFeeSchema == null || mGlobalInput == null)
        {
        	CError tError = new CError();
            tError.moduleName = "AfterTempFee2BL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到相应的财务数据！";
            mErrors.addOneError(tError);
            return false;
        }
        System.out.println(PubFun.getCurrentDate() + " "+ PubFun.getCurrentTime() + ":" + mGlobalInput.Operator + " AfterTempFee2BL");

        return true;
    }

    public static void main(String[] args)
    {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";
        g.ManageCom = g.ComCode;

        LJTempFeeDB db = new LJTempFeeDB();
        db.setTempFeeNo("31000002443");

        VData d = new VData();
        d.add(g);
        d.add(db.query().get(1));

        AfterTempFee4BL bl = new AfterTempFee4BL();
        MMap map = bl.getSubmitMMap(d, "");
        if(map == null)
        {
            System.out.println(bl.mErrors.getErrContent());
        }

        d.clear();
        d.add(map);
        PubSubmit p = new PubSubmit();
        if(!p.submitData(d, ""))
        {
            System.out.println(p.mErrors.getErrContent());
        }
    }
}
