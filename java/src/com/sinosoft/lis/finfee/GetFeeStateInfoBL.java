package com.sinosoft.lis.finfee;

import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.sinosoft.lis.db.LDSysVarDB;

/**
 * 获取财务状态对应的明细信息
 * 
 * @author 张成轩
 */
public class GetFeeStateInfoBL {

	private static Element xmlConfig = init();

	/**
	 * 配置文件初始化，只在第一次调用时读取，若修改配置文件，可以执行下面的重载函数
	 * 
	 * @return Element节点
	 */
	private static Element init() {

		System.out.println("---收费明细状态配置初始化---");

		LDSysVarDB tLDSysVarDB = new LDSysVarDB();
		tLDSysVarDB.setSysVar("ServerRoot");
		if (!tLDSysVarDB.getInfo()) {
			return null;
		}
		String xmlPath = tLDSysVarDB.getSysVarValue() + "f1print/picctemplate/FeeStateConfig.xml";
		//xmlPath = "E:\\loong\\work\\ui\\f1print\\picctemplate\\FeeStateConfig.xml";
		System.out.println(xmlPath);

		SAXBuilder builder = new SAXBuilder(false);
		Element xmlConfig = null;
		try {
			Document doc = builder.build(xmlPath);
			xmlConfig = doc.getRootElement();
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}

		return xmlConfig;
	}

	/**
	 * 获取明细信息
	 * 
	 * @param feeFlag 收付费标识
	 * @param feeType 收付费状态标记
	 * @param state 收付费明细状态标记 具体含义看配置文件
	 * @param type 业务类型 具体含义看配置文件
	 */
	public static List getStateInft(String feeFlag, String feeType, String state, String type) {

		List infoList = new ArrayList();

		Element tElement = xmlConfig.getChild("FeeType");

		if ("S".equals(feeFlag)) {

			Element pay = tElement.getChild("Pay").getChild(feeType);

			List rowList = pay.getChildren();

			for (int index = 0; index < rowList.size(); index++) {
				Element row = (Element) rowList.get(index);
				String tState = row.getAttributeValue("state");
				String tType = row.getAttributeValue("type");

				if ((tState == null || tState.equals(state)) 
						&& (tType == null || (type == null && tType == null) 
								|| (type != null && type.equals(tType)))) {
					infoList.add(row.getText());
				}
			}

		} else {
			
			Element get = tElement.getChild("Get").getChild(feeType);

			List rowList = get.getChildren();

			for (int index = 0; index < rowList.size(); index++) {
				Element row = (Element) rowList.get(index);
				String tState = row.getAttributeValue("state");
				String tType = row.getAttributeValue("type");

				if ((tState == null || tState.equals(state)) 
						&& (tType == null || (type == null && tType == null) 
								|| (type != null && type.equals(tType)))) {
					infoList.add(row.getText());
				}
			}
		}

		return infoList;
	}
	
	/**
	 * 获取错误信息
	 * 
	 * @param state
	 * @return
	 */
	public static List getErrorInfo(String state){
		
		List infoList = new ArrayList();

		Element tElement = xmlConfig.getChild("ErrorType");
		
		List rowList = tElement.getChildren();
		
		for (int index = 0; index < rowList.size(); index++) {
			Element row = (Element) rowList.get(index);
			
			String tState = row.getAttributeValue("state");

			if (tState != null && tState.equals(state)) {
				infoList.add(row.getText());
			}
		}
		return infoList;
	}
	
	/**
	 * 重新读取配置文件
	 */
	public static void reload(){
		xmlConfig = init();
	}

	public static void main(String[] arr) {
		List test = GetFeeStateInfoBL.getStateInft("S", "ShouldFee", "5", "3");

		for (int index = 0; index < test.size(); index++) {
			System.out.println(test.get(index));
		}
	}
}
