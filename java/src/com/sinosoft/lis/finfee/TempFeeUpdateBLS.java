package com.sinosoft.lis.finfee;

import java.sql.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统投保暂交费功能部分 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002 </p>
 * <p>Company: Sinosoft< /p>
 * @author YT
 * @version 1.0
 */

public class TempFeeUpdateBLS
{
  //传输数据类
  private VData mInputData ;
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();
  private String mOperate;

  public TempFeeUpdateBLS() {}


  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;
    this.mOperate = cOperate;
    System.out.print("save TempFeeUpdateBLS data begin");
    if (!this.saveData())
      return false;
    System.out.println("End ClaimSave BLS Submit...");
    mInputData=null;
    return true;
  }




  private boolean saveData()
  {
  	LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
  	LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();

 	tLJTempFeeSet = (LJTempFeeSet)mInputData.getObjectByObjectName("LJTempFeeSet",0);
    	tLJTempFeeClassSet = (LJTempFeeClassSet)mInputData.getObjectByObjectName("LJTempFeeClassSet",0);
        String tTempFeeNo = tLJTempFeeSet.get(1).getTempFeeNo();
    	Connection conn = null;
    	conn = DBConnPool.getConnection();
    	if (conn==null)
    	{
      		// @@错误处理
      		CError tError = new CError();
      		tError.moduleName = "ClaimSaveBLS";
      		tError.functionName = "saveData";
      		tError.errorMessage = "数据库连接失败!";
      		this.mErrors .addOneError(tError) ;
      		return false;
    	}

    	try
    	{
              conn.setAutoCommit(false);
              // 保存现在的关联
              LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB( conn );
              tLJTempFeeDB.setTempFeeNo(tTempFeeNo);
              if (tLJTempFeeDB.deleteSQL() == false)
              {
                // @@错误处理
                this.mErrors.copyAllErrors(tLJTempFeeDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "TempFeeUpdateBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "删除数据失败!";
                this.mErrors .addOneError(tError) ;
                conn.rollback() ;
                conn.close();
                return false;
              }

              LJTempFeeDBSet tLJTempFeeDBSet = new LJTempFeeDBSet( conn );
              tLJTempFeeDBSet.set(tLJTempFeeSet);

              if (tLJTempFeeDBSet.insert() == false)
              {
                // @@错误处理
                this.mErrors.copyAllErrors(tLJTempFeeDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "TempFeeUpdateBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "保存数据失败!";
                this.mErrors .addOneError(tError) ;
                conn.rollback() ;
                conn.close();
                return false;
              }
              LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB( conn );
             tLJTempFeeClassDB.setTempFeeNo(tTempFeeNo);
             if (tLJTempFeeClassDB.deleteSQL() == false)
             {
               // @@错误处理
               this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
               CError tError = new CError();
               tError.moduleName = "TempFeeUpdateBLS";
               tError.functionName = "saveData";
               tError.errorMessage = "删除数据失败!";
               this.mErrors .addOneError(tError) ;
               conn.rollback() ;
               conn.close();
               return false;
             }

             LJTempFeeClassDBSet tLJTempFeeClassDBSet = new LJTempFeeClassDBSet( conn );
             tLJTempFeeClassDBSet.set(tLJTempFeeClassSet);

             if (tLJTempFeeClassDBSet.insert() == false)
             {
               // @@错误处理
               this.mErrors.copyAllErrors(tLJTempFeeClassDBSet.mErrors);
               CError tError = new CError();
               tError.moduleName = "TempFeeUpdateBLS";
               tError.functionName = "saveData";
               tError.errorMessage = "保存数据失败!";
               this.mErrors .addOneError(tError) ;
               conn.rollback() ;
               conn.close();
               return false;
             }


		conn.commit();
		conn.close();
		} // end of try
	catch (Exception ex)
	{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "TempFeeUpdateBLS";
			tError.functionName = "submitData";
			tError.errorMessage = ex.toString();
			this.mErrors .addOneError(tError);
			try{conn.rollback() ;} catch(Exception e){}

			return false;
	}
    return true;
  }

}