package com.sinosoft.lis.finfee;

import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.pubfun.*;

import java.util.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class BatchPayBL  {


  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData =new VData();
  /** 数据操作字符串 */
  private String mOperate;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  /** 提交数据的容器 */
  private MMap map = new MMap();

  //标志位
  String flag="";
  //给付表(生存领取_实付)
  private LJAGetDrawSet mLJAGetDrawSet = new LJAGetDrawSet();
  //批改补退费表(实收/实付)
  private LJAGetEndorseSet mLJAGetEndorseSet = new LJAGetEndorseSet();
  //暂交费退费实付表
  private LJAGetTempFeeSet mLJAGetTempFeeSet = new LJAGetTempFeeSet();
  //其他退费实付表
  private LJAGetOtherSet mLJAGetOtherSet = new LJAGetOtherSet();
  //  赔付实付表
  private LJAGetClaimSet mLJAGetClaimSet = new LJAGetClaimSet();
  /** 社保通调查费结算细目表 */
  private SocialSecurityFeeClaimSet mSocialSecurityFeeClaimSet = new SocialSecurityFeeClaimSet();
  //  赔付实付表
  private LLPrepaidClaimSet mLLPrepaidClaimSet = new LLPrepaidClaimSet();
  // 红利给付实付表
  private LJABonusGetSet mLJABonusGetSet = new LJABonusGetSet();
  //Session信息
  private GlobalInput mG = new GlobalInput();
  //理赔批案表
  private LLRegisterSet mLLRegisterSet = new LLRegisterSet();

  private LJAGetSet mLJAGetSet=null;
  private LJFIGetSet mLJFIGetSet=null;
  //业务处理相关变量

  public BatchPayBL() {
  }
  public static void main(String[] args) {
      String manageCom = "862101";
      int len = manageCom.length();
      for (int i = 0;i<8-len;i++){
          manageCom = manageCom+"0";
      }
      System.out.println(" managecom: "+manageCom);
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    System.out.println("OperateData:  "+cOperate);


    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
    System.out.println("After getinputdata");


    //进行业务处理
    if (!dealData())
      return false;
    System.out.println("After dealData！");


    if (mOperate.equals("VERIFY") ) {
        //准备往后台的数据
        if (!prepareOutputData()) return false;
        System.out.println("---End prepareOutputData---");

        System.out.println("Start PubSubmit BLS Submit...");
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
          // @@错误处理
          this.mErrors.copyAllErrors(tPubSubmit.mErrors);
          return false;
        }
        System.out.println("End PubSubmit BLS Submit...");
     }

    mInputData=null;
    return true;
  }

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData()
  {
    if(this.mOperate.equals("VERIFY"))
    {
    	if(!verifyCom()) return false;

        //首先查询财务给付表中是否有该纪录
        if(!QyeryLJFIGet()) return false;

        //处理给付表
        UpdateLJAGet();

        //更新 给付表(生存领取_实付)   LJAGetDraw
        if(!updateLJAGetDraw()) return false;

        //更新 批改补退费表(实收/实付) LJAGetEndorse
        if(!updateLJAGetEndorse()) return false;

        //更新 暂交费退费实付表
        if(!updateLJAGetTempFee()) return false;

        //更新 赔付实付表LJAGetClaim
        if(!updateLJAGetClaim()) return false;
        
        //更新 社保通调查费结算细目表SocialSecurityFeeClaim Add By Houyd 
        if(!updateSocialSecurityFeeClaim()) return false;

        //更新 其他退费实付表LJAGetOther
        if(!updateLJAGetOther()) return false;

        //更新 红利给付实付表         LJABonusGet
        if(!updateLJABonusGet()) return false;


    }else if(this.mOperate.equals("CLAIM")){
        if(!updateGroupLJAGetClaim()) return false;
    }
    return true ;
 }
  
   /**
    * add By Houyd 社保通调查费结算细目表在财务付费结束后状态的改变
    * @return
    */
   private boolean updateSocialSecurityFeeClaim() {

	      for(int i=1;i<=this.mLJAGetSet.size();i++){
	          String OtherNoType=this.mLJAGetSet.get(i).getOtherNoType();
	          if(OtherNoType.equals("23")){
	              String sqlStr="select * from SocialSecurityFeeClaim where ActuGetNo='"+mLJAGetSet.get(i).getActuGetNo()+"' with ur";
	              SocialSecurityFeeClaimSet tSocialSecurityFeeClaimSet = new SocialSecurityFeeClaimSet();
	              SocialSecurityFeeClaimSchema tSocialSecurityFeeClaimSchema = new SocialSecurityFeeClaimSchema();
	              SocialSecurityFeeClaimDB tSocialSecurityFeeClaimDB = new SocialSecurityFeeClaimDB();
	              tSocialSecurityFeeClaimSet = tSocialSecurityFeeClaimDB.executeQuery(sqlStr);
	              if (tSocialSecurityFeeClaimDB.mErrors.needDealError() == true)
	              {
	                  // @@错误处理
	                  this.mErrors.copyAllErrors(tSocialSecurityFeeClaimDB.mErrors);
	                  CError tError = new CError();
	                  tError.moduleName = "BatchPayBL";
	                  tError.functionName = "updateLJAGetClaim";
	                  tError.errorMessage = "社保通查勘费细目表查询失败!";
	                  this.mErrors.addOneError(tError);
	                  tSocialSecurityFeeClaimSet.clear();
	                  return false;
	              }
	              for(int n=1;n<=tSocialSecurityFeeClaimSet.size();n++)
	              {
	            	  tSocialSecurityFeeClaimSchema = new SocialSecurityFeeClaimSchema();
	            	  tSocialSecurityFeeClaimSchema = tSocialSecurityFeeClaimSet.get(n);
	            	  tSocialSecurityFeeClaimSchema.setSettlementState("02");//财务付费后，将状态置为02
	            	  tSocialSecurityFeeClaimSchema.setConfDate(mLJAGetSet.get(i).getConfDate());
	            	  tSocialSecurityFeeClaimSchema.setEnterAccDate(mLJAGetSet.get(i).getEnterAccDate());
	            	  tSocialSecurityFeeClaimSchema.setModifyDate(CurrentDate);
	            	  tSocialSecurityFeeClaimSchema.setModifyTime(CurrentTime);
	                  this.mSocialSecurityFeeClaimSet.add(tSocialSecurityFeeClaimSchema);
	              }
	              if(this.mSocialSecurityFeeClaimSet.size()==0){
	                  CError tError = new CError();
	                  tError.moduleName = "BatchPayBL";
	                  tError.functionName = "updateSocialSecurityFeeClaim";
	                  tError.errorMessage="没有可以更新的社保通查勘费细目表！";
	                  this.mErrors .addOneError(tError) ;
	                  return false;
	              }	              
	          }
	      }	      
	      return true;
	  
}
//对登录机构和用户进行权限控制
  private boolean verifyCom() {
          // TODO Auto-generated method stub
          /*if (mG.ComCode.length() <= 4) {
              String strSql =  "select * from ldcode where codeType='ljgetsxf' and Othersign='Y' and code='" + mG.ComCode + "'";
              LDCodeDB tLDCodeDB = new LDCodeDB();
              LDCodeSet tLDCodeSet = tLDCodeDB.executeQuery(strSql);
              if (tLDCodeSet.size() == 0) {
                  buildError("verifyCom", "对不起，您没有权限付费");
                  return false;
              }
              strSql = "select * from ldcode where codeType='ljgetsxfuser' and Othersign='Y' and comcode='"+mG.ComCode+"'";
              tLDCodeDB = new LDCodeDB();
              tLDCodeSet = tLDCodeDB.executeQuery(strSql);
              if (tLDCodeSet.size() != 0) {
                  strSql = "select * from ldcode where codeType='ljgetsxfuser' and Othersign='Y' and code = '"+mG.Operator+"' and comcode='"+mG.ComCode+"'";
                  tLDCodeDB = new LDCodeDB();
                  tLDCodeSet = tLDCodeDB.executeQuery(strSql);
                  if (tLDCodeSet.size() == 0) {
                      buildError("verifyCom", "对不起，您没有权限付费");
                      return false;
                  }
              }
          }*/
          return true;
      }

public boolean setLLCaseFalg(LJAGetClaimSet aLJAGetClaimSet)
 {
	 String tRgtNoTwo = "";
     String tRgtNo = "";
     String together = "";
     for (int k=1;k<=aLJAGetClaimSet.size();k++){
         String tfeeFinaType=aLJAGetClaimSet.get(k).getFeeFinaType();  
         boolean tsummoney=false;
         SSRS ttSSRS=new ExeSQL().execSQL("select 1 from (select sum(pay) a from ljagetclaim where actugetno='"+aLJAGetClaimSet.get(k).getActuGetNo()+"' and otherno='"+aLJAGetClaimSet.get(k).getOtherNo()+"' and feefinatype<>'YF')as newpay where a<>0 ");
         if(ttSSRS!=null && ttSSRS.getMaxRow()>0){
        	 tsummoney=true;
         }
         if(tfeeFinaType!=null && (tfeeFinaType.equals("JJ") || tfeeFinaType.equals("ZJ"))){
             continue;
         }else if(!tsummoney){	 
        	 continue;
         }else{
               String tRgtState = aLJAGetClaimSet.get(k).getOtherNo();
               if(!tRgtState.equals(null)&&!tRgtState.equals("null")&&!tRgtState.equals(""))
               {
               tRgtNoTwo = tRgtNo ;
               String RgtNoSQL1 = "Select rgtno from llcase where caseno = '"+tRgtState+"'";
               ExeSQL tRgtNoSQL = new ExeSQL();
               tRgtNo = tRgtNoSQL.getOneValue(RgtNoSQL1);
               if(!tRgtNo.equals("") && !tRgtNo.equals(null) && !tRgtNo.equals("null"))
               {
            	   String sql= "select togetherflag from llregister where rgtno = '"+tRgtNo+"'"; 
            	   ExeSQL tRgtTogetherSQL = new ExeSQL();
            	   together = tRgtTogetherSQL.getOneValue(sql);
            	   if(together.equals("3")||together.equals("4")){
            		   
            		   if(!tRgtNoTwo.equals(tRgtNo)){
            			   
            			   String RgtStateSQL1 = "Update LLCase Set RgtState = '12' where RgtNo = '"+tRgtNo+"' and RgtState = '11'";
                    	   map.put(RgtStateSQL1, "UPDATE");
                    	   
                    	   String RgtStateSQL = "Update LLRegister Set RgtState = '04' where RgtNo = '"+tRgtNo+"'";
                           map.put(RgtStateSQL, "UPDATE");  
            		   }
                	  
            	   }else{
            		   String RgtStateSQL2 = "Update LLCase Set RgtState = '12' where CaseNo = '"+tRgtState+"' and RgtState = '11'";
            		   map.put(RgtStateSQL2, "UPDATE");
            	   }            	   
               }
               }
             }
         }
     return true;
 }
  //从输入数据中得到所有对象
  //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData tInputData)
  {
      mLJAGetSet=(LJAGetSet)tInputData.getObjectByObjectName("LJAGetSet",0);
      mLJFIGetSet=(LJFIGetSet)tInputData.getObjectByObjectName("LJFIGetSet",0);
      mG = (GlobalInput)tInputData.getObjectByObjectName("GlobalInput",0);


      if(mLJAGetSet==null || mLJFIGetSet ==null)
      {
          // @@错误处理
          CError tError =new CError();
          tError.moduleName="BatchPayBL";
          tError.functionName="getInputData";
          tError.errorMessage="没有得到足够的数据，请您确认!";
          this.mErrors .addOneError(tError) ;
          return false;
      }
      if(mLJAGetSet.size()==0 || mLJFIGetSet.size()==0)
      {
          // @@错误处理
          CError tError =new CError();
          tError.moduleName="BatchPayBL";
          tError.functionName="getInputData";
          tError.errorMessage="没有得到足够的数据，请您确认!";
          this.mErrors .addOneError(tError) ;
          return false;
      }
      return true;
  }

  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData()
  {
      try {
          if(this.mLJFIGetSet!=null && this.mLJFIGetSet.size()>0){
              map.put(this.mLJFIGetSet, "INSERT");
          }
          if(this.mLJAGetSet!=null && this.mLJAGetSet.size()>0){
              map.put(this.mLJAGetSet, "UPDATE");
          }
          if(this.mLJAGetDrawSet!=null && this.mLJAGetDrawSet.size()>0){
              map.put(this.mLJAGetDrawSet, "UPDATE");
          }
          if(this.mLJAGetEndorseSet!=null && this.mLJAGetEndorseSet.size()>0){
              map.put(this.mLJAGetEndorseSet, "UPDATE");
          }
          if(this.mLJAGetTempFeeSet!=null && this.mLJAGetTempFeeSet.size()>0){
              map.put(this.mLJAGetTempFeeSet, "UPDATE");
          }
          if(this.mLJAGetClaimSet!=null && this.mLJAGetClaimSet.size()>0){
              map.put(this.mLJAGetClaimSet, "UPDATE");
              if(this.mLLRegisterSet!=null && this.mLLRegisterSet.size()>0){
                  map.put(this.mLLRegisterSet, "UPDATE");
              }
              if(this.mLLPrepaidClaimSet!=null && this.mLLPrepaidClaimSet.size()>0){
                  map.put(this.mLLPrepaidClaimSet, "UPDATE");
              }
          }
          //Add By Houyd 
          if(this.mSocialSecurityFeeClaimSet!=null && this.mSocialSecurityFeeClaimSet.size()>0){
              map.put(this.mSocialSecurityFeeClaimSet, "UPDATE");
          }
          if(this.mLJAGetOtherSet!=null && this.mLJAGetOtherSet.size()>0){
              map.put(this.mLJAGetOtherSet, "UPDATE");
          }
          if(this.mLJABonusGetSet!=null && this.mLJABonusGetSet.size()>0){
              map.put(this.mLJABonusGetSet, "UPDATE");
          }

          mInputData.clear();
          mInputData.add(map);
      }catch(Exception ex) {
          // @@错误处理
          System.out.println(ex.getMessage());
          CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
          return false;
      }
    return true;
  }

  private boolean QyeryLJFIGet()
  {
      for(int i=1;i<=this.mLJFIGetSet.size();i++){
          String sqlStr="select * from LJFIGet where ActuGetNo='"+mLJFIGetSet.get(i).getActuGetNo()+"' with ur";
          LJFIGetSet tLJFIGetSet = new LJFIGetSet();
          LJFIGetDB tLJFIGetDB = new LJFIGetDB();
          tLJFIGetSet = tLJFIGetDB.executeQuery(sqlStr);
          if (tLJFIGetDB.mErrors.needDealError() == true)
          {
              // @@错误处理
              this.mErrors.copyAllErrors(tLJFIGetDB.mErrors);
              CError tError = new CError();
              tError.moduleName = "BatchPayBL";
              tError.functionName = "queryLJFIGet";
              tError.errorMessage = "财务给付表查询失败!";
              this.mErrors.addOneError(tError);
              tLJFIGetSet.clear();
              return false;
          }
          if (tLJFIGetSet.size() > 0)
          {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "BatchPayBL";
              tError.functionName = "queryLJFIGet";
              tError.errorMessage="实付号码为："+mLJFIGetSet.get(i).getActuGetNo()+" 的纪录已经存在！";
              this.mErrors .addOneError(tError) ;
              return false;
          }
      }

    return true;
  }

  /**
   * 查询给付表(生存领取_实付)
   * @param LJAGetBL
   * @return
   */
  private boolean updateLJAGetDraw()
  {
      for(int i=1;i<=this.mLJAGetSet.size();i++){
          String OtherNoType=this.mLJAGetSet.get(i).getOtherNoType();
          if(OtherNoType.equals("1")||OtherNoType.equals("2")||OtherNoType.equals("0")){
              String sqlStr="select * from LJAGetDraw where ActuGetNo='"+mLJAGetSet.get(i).getActuGetNo()+"' with ur";
              LJAGetDrawSet tLJAGetDrawSet = new LJAGetDrawSet();
              LJAGetDrawSchema tLJAGetDrawSchema = new LJAGetDrawSchema();
              LJAGetDrawDB tLJAGetDrawDB = new LJAGetDrawDB();
              tLJAGetDrawSet = tLJAGetDrawDB.executeQuery(sqlStr);
              if (tLJAGetDrawDB.mErrors.needDealError() == true)
              {
                   // @@错误处理
                   this.mErrors.copyAllErrors(tLJAGetDrawDB.mErrors);
                   CError tError = new CError();
                   tError.moduleName = "BatchPayBL";
                   tError.functionName = "queryLJAGetDraw";
                   tError.errorMessage = "给付表(生存领取_实付)查询失败!";
                   this.mErrors.addOneError(tError);
                   tLJAGetDrawSet.clear();
                   return false;
              }

              for(int n=1;n<=tLJAGetDrawSet.size();n++)
              {
                  tLJAGetDrawSchema = new LJAGetDrawSchema();
                  tLJAGetDrawSchema = tLJAGetDrawSet.get(n);
                  tLJAGetDrawSchema.setConfDate(mLJAGetSet.get(i).getConfDate());
                  tLJAGetDrawSchema.setEnterAccDate(mLJAGetSet.get(i).getEnterAccDate());
                  tLJAGetDrawSchema.setModifyDate(CurrentDate);
                  tLJAGetDrawSchema.setModifyTime(CurrentTime);
                  this.mLJAGetDrawSet.add(tLJAGetDrawSchema);
              }
              if(mLJAGetDrawSet.size()==0){
                  CError tError = new CError();
                  tError.moduleName = "BatchPayBL";
                  tError.functionName = "updateLJAGetDraw";
                  tError.errorMessage="没有可以更新的给付表(生存领取_实付)！";
                  this.mErrors .addOneError(tError) ;
                  return false;
              }
          }

      }
      return true;
  }

  /**
   * 查询批改补退费表(实收/实付)
   * @param LJAGetBL
   * @return
   */
  private boolean updateLJAGetEndorse()
   {
      for(int i=1;i<=this.mLJAGetSet.size();i++){
          String OtherNoType=this.mLJAGetSet.get(i).getOtherNoType();
          if(OtherNoType.equals("3")||OtherNoType.equals("10")){
              String sqlStr="select * from LJAGetEndorse where ActuGetNo='"+this.mLJAGetSet.get(i).getActuGetNo()+"' with ur";
              LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
              LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
              LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
              tLJAGetEndorseSet = tLJAGetEndorseDB.executeQuery(sqlStr);
              if (tLJAGetEndorseDB.mErrors.needDealError() == true)
              {
                   // @@错误处理
                   this.mErrors.copyAllErrors(tLJAGetEndorseDB.mErrors);
                   CError tError = new CError();
                   tError.moduleName = "BatchPayBL";
                   tError.functionName = "updateLJAGetEndorse";
                   tError.errorMessage = "批改补退费表(实收/实付)查询失败!";
                   this.mErrors.addOneError(tError);
                   tLJAGetEndorseSet.clear();
                   return false;
              }
              for(int n=1;n<=tLJAGetEndorseSet.size();n++)
              {
                  tLJAGetEndorseSchema = new LJAGetEndorseSchema();
                  tLJAGetEndorseSchema = tLJAGetEndorseSet.get(n);
                  tLJAGetEndorseSchema.setGetConfirmDate(this.mLJAGetSet.get(i).getConfDate());
                  tLJAGetEndorseSchema.setEnterAccDate(this.mLJAGetSet.get(i).getEnterAccDate());
                  tLJAGetEndorseSchema.setModifyDate(CurrentDate);
                  tLJAGetEndorseSchema.setModifyTime(CurrentTime);
                  this.mLJAGetEndorseSet.add(tLJAGetEndorseSchema);
              }
              if(this.mLJAGetEndorseSet.size()==0){
                  CError tError = new CError();
                  tError.moduleName = "BatchPayBL";

        tError.functionName = "updateLJAGetEndorse";
                  tError.errorMessage="没有可以更新的批改补退费表(实收/实付)！";
                  this.mErrors .addOneError(tError) ;
                  return false;
              }
          }
      }
      return true;
   }

  /**
   * 查询其他退费实付表
   * @param LJAGetBL
   * @return
   */
  private boolean updateLJAGetOther()
   {
      for(int i=1;i<=this.mLJAGetSet.size();i++){
          String OtherNoType=this.mLJAGetSet.get(i).getOtherNoType();
          if(OtherNoType.equals("6") || OtherNoType.equals("8")){
              String sqlStr="select * from LJAGetOther where ActuGetNo='"+this.mLJAGetSet.get(i).getActuGetNo()+"' with ur";
              LJAGetOtherSet tLJAGetOtherSet = new LJAGetOtherSet();
              LJAGetOtherSchema tLJAGetOtherSchema = new LJAGetOtherSchema();
              LJAGetOtherDB tLJAGetOtherDB = new LJAGetOtherDB();
              tLJAGetOtherSet = tLJAGetOtherDB.executeQuery(sqlStr);
              if (tLJAGetOtherDB.mErrors.needDealError() == true)
              {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLJAGetOtherDB.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "BatchPayBL";
                  tError.functionName = "updateLJAGetOther";
                  tError.errorMessage = "其他退费实付表查询失败!";
                  this.mErrors.addOneError(tError);
                  tLJAGetOtherSet.clear();
                  return false;
             }
             LJAGetOtherSet newLJAGetOtherSet = new LJAGetOtherSet();
             for(int n=1;n<=tLJAGetOtherSet.size();n++)
             {
                 tLJAGetOtherSchema = new LJAGetOtherSchema();
                 tLJAGetOtherSchema = tLJAGetOtherSet.get(n);
                 tLJAGetOtherSchema.setConfDate(this.mLJAGetSet.get(i).getConfDate());
                 tLJAGetOtherSchema.setEnterAccDate(this.mLJAGetSet.get(i).getEnterAccDate());
                 tLJAGetOtherSchema.setModifyDate(CurrentDate);
                 tLJAGetOtherSchema.setModifyTime(CurrentTime);
                 this.mLJAGetOtherSet.add(tLJAGetOtherSchema);
             }
             if (mLJAGetOtherSet.size()==0)
             {
                 // @@错误处理
                 CError tError = new CError();
                 tError.moduleName = "BatchPayBL";
                 tError.functionName = "updateLJAGetOther";
                 tError.errorMessage="没有可以更新的其他退费实付表！";
                 this.mErrors .addOneError(tError) ;
                 return false;
             }
          }
      }
      return true;
  }
  private boolean updateGroupLJAGetClaim(){
      for(int i=1;i<=this.mLJAGetSet.size();i++){
          String OtherNoType=this.mLJAGetSet.get(i).getOtherNoType();
          if(OtherNoType.equals("5") || OtherNoType.equals("F") || OtherNoType.equals("C")){
              String sqlStr="select * from LJAGetClaim where ActuGetNo='"+mLJAGetSet.get(i).getActuGetNo()+"' with ur";
              LJAGetClaimSet tLJAGetClaimSet = new LJAGetClaimSet();
              LJAGetClaimDB tLJAGetClaimDB = new LJAGetClaimDB();
              tLJAGetClaimSet = tLJAGetClaimDB.executeQuery(sqlStr);
              String tRgtNo = "";
              String tCaseNo = "";
              for (int k=1;k<=tLJAGetClaimSet.size();k++){
                  String tfeeFinaType=tLJAGetClaimSet.get(k).getFeeFinaType();
                  if(tfeeFinaType!=null && (tfeeFinaType.equals("JJ") || tfeeFinaType.equals("ZJ"))){
                      continue;
                  }else{
                    String tRgtState = tLJAGetClaimSet.get(k).getOtherNo();
                    if(!tRgtState.equals(null)&&!tRgtState.equals("null")&&!tRgtState.equals(""))
                   {
                     String RgtNoSQL1 = "Select rgtno from llcase where caseno = '"+tRgtState+"'";
                     ExeSQL tRgtNoSQL = new ExeSQL();
                     tRgtNo = tRgtNoSQL.getOneValue(RgtNoSQL1);
                     tCaseNo = tRgtState;
                   }
                  }
              }
              //团单下最后一个人做完给付确认应更新团单案件的状态为“04”－所有个人给付完毕
              if(mLJAGetSet.get(i).getOtherNo().substring(0,1).equals("C")&&!tRgtNo.equals(tCaseNo)){
              String RgtClassSQL = "Select RgtClass from LLRegister where RgtNo = '"+tRgtNo+"'";
              ExeSQL tRgtClassSQL = new ExeSQL();
              String tRgtClass = tRgtClassSQL.getOneValue(RgtClassSQL);
	          if ("1".equals(tRgtClass)) {
                  boolean tFlag = true;
                  String mCaseNoSQL = "Select CaseNo,RgtState from LLCase where RgtNo = '"+tRgtNo+"'";
                  ExeSQL CaseNoSQL1 = new ExeSQL();
                  SSRS CaseNoSSRS = CaseNoSQL1.execSQL(mCaseNoSQL);
                  
                  for(int m=1;i<=CaseNoSSRS.getMaxRow();i++){
                      String mCaseNo = CaseNoSSRS.GetText(i, 1);
                      String mRgtState = CaseNoSSRS.GetText(i, 2);
                      if(mCaseNo.equals(tCaseNo))
                         continue;
                      if (!("12".equals(mRgtState) || "14".equals(mRgtState))) {
                         tFlag = false;
                      }
                      if (tFlag) {
                      String RgtStateSQL2 = "Update LLRegister Set RgtState = '04' where RgtNo = '"+tRgtNo+"'";
                      map.put(RgtStateSQL2, "UPDATE");
                  }
               }
                  }
              }
          }
      }
      return true;
  }

 /**
   * 赔付实付表
   * @param LJAGetBL
   * @return
   */
  private boolean updateLJAGetClaim()
   {
      for(int i=1;i<=this.mLJAGetSet.size();i++){
          String OtherNoType=this.mLJAGetSet.get(i).getOtherNoType();
          if(OtherNoType.equals("5") || OtherNoType.equals("F") || OtherNoType.equals("C")|| OtherNoType.equals("Y")){
              String sqlStr="select * from LJAGetClaim where ActuGetNo='"+mLJAGetSet.get(i).getActuGetNo()+"' with ur";
              LJAGetClaimSet tLJAGetClaimSet = new LJAGetClaimSet();
              LJAGetClaimSchema tLJAGetClaimSchema = new LJAGetClaimSchema();
              LJAGetClaimDB tLJAGetClaimDB = new LJAGetClaimDB();
              tLJAGetClaimSet = tLJAGetClaimDB.executeQuery(sqlStr);
              if (tLJAGetClaimDB.mErrors.needDealError() == true)
              {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLJAGetClaimDB.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "BatchPayBL";
                  tError.functionName = "updateLJAGetClaim";
                  tError.errorMessage = "赔付实付表查询失败!";
                  this.mErrors.addOneError(tError);
                  tLJAGetClaimSet.clear();
                  return false;
              }
              for(int n=1;n<=tLJAGetClaimSet.size();n++)
              {
                  tLJAGetClaimSchema = new LJAGetClaimSchema();
                  tLJAGetClaimSchema = tLJAGetClaimSet.get(n);
                  tLJAGetClaimSchema.setConfDate(mLJAGetSet.get(i).getConfDate());
                  tLJAGetClaimSchema.setEnterAccDate(mLJAGetSet.get(i).getEnterAccDate());
                  tLJAGetClaimSchema.setModifyDate(CurrentDate);
                  tLJAGetClaimSchema.setModifyTime(CurrentTime);
                  this.mLJAGetClaimSet.add(tLJAGetClaimSchema);
              }
              if(this.mLJAGetClaimSet.size()==0){
                  CError tError = new CError();
                  tError.moduleName = "BatchPayBL";
                  tError.functionName = "updateLJAGetEndorse";
                  tError.errorMessage="没有可以更新的批改补退费表(实收/实付)！";
                  this.mErrors .addOneError(tError) ;
                  return false;
              }
              if(OtherNoType.equals("Y"))
              {
            	  LLPrepaidClaimDB tLLPrepaidClaimDB = new LLPrepaidClaimDB();
            	  tLLPrepaidClaimDB.setPrepaidNo(tLJAGetClaimSchema.getOtherNo());
            	  if(tLLPrepaidClaimDB.getInfo())
            	  {
            		  LLPrepaidClaimSchema tLLPrepaidClaimSchema = tLLPrepaidClaimDB.getSchema();
            		  tLLPrepaidClaimSchema.setRgtState("06");
            		  this.mLLPrepaidClaimSet.add(tLLPrepaidClaimSchema);

            	  }
              }
          }
      }
      if(!setLLCaseFalg(mLJAGetClaimSet))
      {
    	  return false;
      }
      return true;
  }


 /**
   * 暂交费退费实付表
   * @param LJAGetBL
   * @return
   */
  private boolean updateLJAGetTempFee()
  {
      for(int i=1;i<=this.mLJAGetSet.size();i++){
          String OtherNoType=this.mLJAGetSet.get(i).getOtherNoType();
          if(OtherNoType.equals("4")){
              String sqlStr="select * from LJAGetTempFee where ActuGetNo='"+mLJAGetSet.get(i).getActuGetNo()+"' with ur";
              LJAGetTempFeeSet tLJAGetTempFeeSet = new LJAGetTempFeeSet();
              LJAGetTempFeeSchema tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
              LJAGetTempFeeDB tLJAGetTempFeeDB = new LJAGetTempFeeDB();
              tLJAGetTempFeeSet = tLJAGetTempFeeDB.executeQuery(sqlStr);
              if (tLJAGetTempFeeDB.mErrors.needDealError() == true)
              {
                   // @@错误处理
                   this.mErrors.copyAllErrors(tLJAGetTempFeeDB.mErrors);
                   CError tError = new CError();
                   tError.moduleName = "BatchPayBL";
                   tError.functionName = "updateLJAGetTempFee";
                   tError.errorMessage = "暂交费退费实付表查询失败!";
                   this.mErrors.addOneError(tError);
                   tLJAGetTempFeeSet.clear();
                   return false;
              }
              for(int n=1;n<=tLJAGetTempFeeSet.size();n++)
              {
                tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
                tLJAGetTempFeeSchema = tLJAGetTempFeeSet.get(n);
                tLJAGetTempFeeSchema.setConfDate(mLJAGetSet.get(i).getConfDate());
                tLJAGetTempFeeSchema.setEnterAccDate(mLJAGetSet.get(i).getEnterAccDate());
                tLJAGetTempFeeSchema.setModifyDate(CurrentDate);
                tLJAGetTempFeeSchema.setModifyTime(CurrentTime);
                this.mLJAGetTempFeeSet.add(tLJAGetTempFeeSchema);
              }
              if(this.mLJAGetTempFeeSet.size()==0){
                  CError tError = new CError();
                  tError.moduleName = "BatchPayBL";
                  tError.functionName = "updateLJAGetEndorse";
                  tError.errorMessage="没有可以更新的批改补退费表(实收/实付)！";
                  this.mErrors .addOneError(tError) ;
                  return false;
              }
          }
      }
      return true;
  }

 /**
   * 红利给付实付表
   * @param LJAGetBL
   * @return
   */
  private boolean updateLJABonusGet()
   {
      for(int i=1;i<=this.mLJAGetSet.size();i++){
          String OtherNoType=this.mLJAGetSet.get(i).getOtherNoType();
          if(OtherNoType.equals("7")){
              String sqlStr="select * from LJABonusGet where ActuGetNo='"+this.mLJAGetSet.get(i).getActuGetNo()+"' with ur";
              LJABonusGetSet tLJABonusGetSet = new LJABonusGetSet();
              LJABonusGetSchema tLJABonusGetSchema = new LJABonusGetSchema();
              LJABonusGetDB tLJABonusGetDB = new LJABonusGetDB();
              tLJABonusGetSet = tLJABonusGetDB.executeQuery(sqlStr);
              if (tLJABonusGetDB.mErrors.needDealError() == true)
              {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLJABonusGetDB.mErrors);
                  CError tError = new CError();
                  tError.moduleName = "BatchPayBL";
                  tError.functionName = "updateLJABonusGet";
                  tError.errorMessage = "红利给付实付表查询失败!";
                  this.mErrors.addOneError(tError);
                  tLJABonusGetSet.clear();
                  return false;
              }
              for(int n=1;n<=tLJABonusGetSet.size();n++)
              {
                  tLJABonusGetSchema = new LJABonusGetSchema();
                  tLJABonusGetSchema = tLJABonusGetSet.get(n);
                  tLJABonusGetSchema.setConfDate(this.mLJAGetSet.get(i).getConfDate());
                  tLJABonusGetSchema.setEnterAccDate(this.mLJAGetSet.get(i).getEnterAccDate());
                  tLJABonusGetSchema.setModifyDate(CurrentDate);
                  tLJABonusGetSchema.setModifyTime(CurrentTime);
                  this.mLJABonusGetSet.add(tLJABonusGetSchema);
              }
              if (mLJABonusGetSet.size()==0)
              {
                  // @@错误处理
                  CError tError = new CError();
                  tError.moduleName = "BatchPayBL";
                  tError.functionName = "updateLJABonusGet";
                  tError.errorMessage="没有可以更新的红利给付实付表！";
                  this.mErrors .addOneError(tError) ;
                  return false;
              }
          }
      }
     return true;
  }
  private void UpdateLJAGet(){
      for(int i=1;i<=this.mLJFIGetSet.size();i++){
          mLJAGetSet.get(i).setPayMode(mLJFIGetSet.get(i).getPayMode());
          mLJAGetSet.get(i).setConfDate(CurrentDate);
          mLJAGetSet.get(i).setModifyDate(CurrentDate);
          mLJAGetSet.get(i).setModifyTime(CurrentTime);

          mLJFIGetSet.get(i).setConfDate(CurrentDate);
          mLJFIGetSet.get(i).setMakeDate(CurrentDate);//入机日期
          mLJFIGetSet.get(i).setMakeTime(CurrentTime);//入机时间
          mLJFIGetSet.get(i).setModifyDate(CurrentDate);//最后一次修改日期
          mLJFIGetSet.get(i).setModifyTime(CurrentTime);//最后一次修改时间
          mLJFIGetSet.get(i).setConfMakeDate(CurrentDate);
          /**
           * 修改2011-01-24 by zhangbin start
           */
          String mngComf = mLJAGetSet.get(i).getManageCom();
          int lenf = mngComf.length();
          for(int j=0 ; j < 8-lenf ; j++){
              mngComf = mngComf+"0";
          }
          mLJFIGetSet.get(i).setPolicyCom(mngComf);
          mLJAGetSet.get(i).setManageCom(mngComf);
          
          String mngCom = mG.ComCode ;
          int len = mngCom.length();
          for(int j=0 ; j < 8-len ; j++){
              mngCom = mngCom+"0";
          }
          mLJFIGetSet.get(i).setManageCom(mngCom);
          /**
           * 修改2011-01-24 by zhangbin end
           */
          mLJFIGetSet.get(i).setAgentCom(mLJAGetSet.get(i).getAgentCom());
          mLJFIGetSet.get(i).setAgentType(mLJAGetSet.get(i).getAgentType());
          mLJFIGetSet.get(i).setAgentGroup(mLJAGetSet.get(i).getAgentGroup());
          mLJFIGetSet.get(i).setAgentCode(mLJAGetSet.get(i).getAgentCode());
          if(mLJAGetSet.get(i).getOtherNoType().equals("5")||mLJAGetSet.get(i).getOtherNoType().equals("F")||mLJAGetSet.get(i).getOtherNoType().equals("C"))
          {
              String tComCode="";
              SSRS ttSSRS=new ExeSQL().execSQL("select managecom from lccont where contno in (select contno from LJAGetClaim where feefinatype<>'JJ' and actugetno ='"+mLJAGetSet.get(i).getActuGetNo()+"') with ur");
              if(ttSSRS.getMaxRow()>0){
                  tComCode=ttSSRS.GetText(1,1);
              }
              if(tComCode!=null && !tComCode.trim().equals("")){
                  mLJFIGetSet.get(i).setPolicyCom(tComCode);
                  mLJAGetSet.get(i).setManageCom(tComCode);
              }
          }
          //add By Houyd 社保通调查费结算细目表在财务付费结束后状态的改变
          if("23".equals(mLJAGetSet.get(i).getOtherNoType()))
          {
              String tComCode="";
              SSRS ttSSRS=new ExeSQL().execSQL("select insurancecomcode from SocialSecurityFeeClaim where actugetno ='"+mLJAGetSet.get(i).getActuGetNo()+"' with ur");
              if(ttSSRS.getMaxRow()>0){
                  tComCode=ttSSRS.GetText(1,1);
              }
              if(tComCode!=null && !tComCode.trim().equals("")){
                  mLJFIGetSet.get(i).setPolicyCom(tComCode);
                  mLJAGetSet.get(i).setManageCom(tComCode);
              }
          }
      }
  }

  /*
     * add by kevin, 2002-10-14
     */
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "ReComManageBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


}
