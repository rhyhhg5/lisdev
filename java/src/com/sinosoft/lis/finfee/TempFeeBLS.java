package com.sinosoft.lis.finfee;


//用于单证回收
import java.sql.Connection;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class TempFeeBLS
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 数据操作字符串 */
    private String mOperate;
    private LJTempFeeSchema mLJTempFeeSchema = new LJTempFeeSchema();

    public TempFeeBLS()
    {
    }

    public static void main(String[] args)
    {
        TempFeeBLS mTempFeeBLS1 = new TempFeeBLS();
    }


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Start TempFee BLS Submit...");
        //信息保存
        if (this.mOperate.equals("INSERT") || this.mOperate.equals("UPDATE"))
        {
            tReturn = save(cInputData);
        }

        if (tReturn)
            System.out.println("Save sucessful");
        else
            System.out.println("Save failed");

        System.out.println("End TempFee BLS Submit...");

        return tReturn;
    }


//保存操作
    private boolean save(VData mInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn = DBConnPool.getConnection();

        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "TempFeeBLS";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            if (this.mOperate.equals("UPDATE"))
            {
                System.out.println("处理暂收修改");
                VData LastFinVData = new VData();
                VData ttVData = new VData();
                LastFinVData = ((VData) mInputData.getObject(5));
                for (int Index = 0; Index < LastFinVData.size(); Index++)
                {
                    ttVData = (VData) LastFinVData.get(Index);
                    LJFIGetSet tLJFIGetSet = new LJFIGetSet();
                    LJAGetSchema tLJAGetSchema = new LJAGetSchema();
                    SchemaSet tSchemaSet = new SchemaSet();
                    tLJFIGetSet = (LJFIGetSet) ttVData.getObjectByObjectName(
                            "LJFIGetSet", 0);
                    tLJAGetSchema = (LJAGetSchema) ttVData.
                                    getObjectByObjectName("LJAGetSchema", 0);
                    String tFlag = (String) ttVData.getObjectByObjectName(
                            "String", 0);
                    tSchemaSet = (SchemaSet) ttVData.getObject(3);
                    if (tLJFIGetSet.size() > 0)
                    {
                        LJFIGetDBSet tLJFIGetDBSet = new LJFIGetDBSet(conn);
                        tLJFIGetDBSet.set(tLJFIGetSet);
                        if (!tLJFIGetDBSet.deleteSQL())
                        {
                            // @@错误处理
                            this.mErrors.copyAllErrors(tLJFIGetDBSet.mErrors);
                            CError tError = new CError();
                            tError.moduleName = "TempFeeBLS";
                            tError.functionName = "saveData";
                            tError.errorMessage = "财务付费表数据删除失败!";
                            this.mErrors.addOneError(tError);
                            conn.rollback();
                            conn.close();
                            return false;
                        }
                    }
                    if (tLJAGetSchema != null)
                    {
                        LJAGetDB tLJAGetDB = new LJAGetDB(conn);
                        tLJAGetDB.setSchema(tLJAGetSchema);
                        if (!tLJAGetDB.update())
                        {
                            // @@错误处理
                            this.mErrors.copyAllErrors(tLJAGetDB.mErrors);
                            CError tError = new CError();
                            tError.moduleName = "TempFeeBLS";
                            tError.functionName = "saveData";
                            tError.errorMessage = "付费总表数据修改失败!";
                            this.mErrors.addOneError(tError);
                            conn.rollback();
                            conn.close();
                            return false;
                        }
                    }
                    if (tSchemaSet.size() > 0)
                    {
                        if (tFlag.equals("0") || tFlag.equals("1") ||
                            tFlag.equals("2"))
                        {
                            LJAGetDrawSet tLJAGetDrawSet = (LJAGetDrawSet)
                                    tSchemaSet;
                            LJAGetDrawDBSet tLJAGetDrawDBSet = new
                                    LJAGetDrawDBSet(conn);
                            tLJAGetDrawDBSet.set(tLJAGetDrawSet);
                            if (!tLJAGetDrawDBSet.update())
                            {
                                // @@错误处理
                                this.mErrors.copyAllErrors(tLJAGetDrawDBSet.
                                        mErrors);
                                CError tError = new CError();
                                tError.moduleName = "TempFeeBLS";
                                tError.functionName = "saveData";
                                tError.errorMessage = "付费明细表数据修改失败!";
                                this.mErrors.addOneError(tError);
                                conn.rollback();
                                conn.close();
                                return false;
                            }
                        }
                        if (tFlag.equals("3"))
                        {
                            LJAGetEndorseSet tLJAGetEndorseSet = (
                                    LJAGetEndorseSet) tSchemaSet;
                            LJAGetEndorseDBSet tLJAGetEndorseDBSet = new
                                    LJAGetEndorseDBSet(conn);
                            tLJAGetEndorseDBSet.set(tLJAGetEndorseSet);
                            if (!tLJAGetEndorseDBSet.update())
                            {
                                // @@错误处理
                                this.mErrors.copyAllErrors(tLJAGetEndorseDBSet.
                                        mErrors);
                                CError tError = new CError();
                                tError.moduleName = "TempFeeBLS";
                                tError.functionName = "saveData";
                                tError.errorMessage = "付费明细表数据修改失败!";
                                this.mErrors.addOneError(tError);
                                conn.rollback();
                                conn.close();
                                return false;
                            }
                        }

                        if (tFlag.equals("4"))
                        {
                            //LJAGetTempFeeSet tLJAGetTempFeeSet = (LJAGetTempFeeSet)tSchemaSet;
                            LJAGetTempFeeSet tLJAGetTempFeeSet = new
                                    LJAGetTempFeeSet();
                            tLJAGetTempFeeSet = (LJAGetTempFeeSet) tSchemaSet;
                            LJAGetTempFeeDBSet tLJAGetTempFeeDBSet = new
                                    LJAGetTempFeeDBSet(conn);
                            tLJAGetTempFeeDBSet.set(tLJAGetTempFeeSet);
                            if (!tLJAGetTempFeeDBSet.update())
                            {
                                // @@错误处理
                                this.mErrors.copyAllErrors(tLJAGetTempFeeDBSet.
                                        mErrors);
                                CError tError = new CError();
                                tError.moduleName = "TempFeeBLS";
                                tError.functionName = "saveData";
                                tError.errorMessage = "付费明细表数据修改失败!";
                                this.mErrors.addOneError(tError);
                                conn.rollback();
                                conn.close();
                                return false;
                            }
                        }

                        if (tFlag.equals("5"))
                        {
                            LJAGetClaimSet tLJAGetClaimSet = (LJAGetClaimSet)
                                    tSchemaSet;
                            LJAGetClaimDBSet tLJAGetClaimDBSet = new
                                    LJAGetClaimDBSet(conn);
                            tLJAGetClaimDBSet.set(tLJAGetClaimSet);
                            if (!tLJAGetClaimDBSet.update())
                            {
                                // @@错误处理
                                this.mErrors.copyAllErrors(tLJAGetClaimDBSet.
                                        mErrors);
                                CError tError = new CError();
                                tError.moduleName = "TempFeeBLS";
                                tError.functionName = "saveData";
                                tError.errorMessage = "付费明细表数据修改失败!";
                                this.mErrors.addOneError(tError);
                                conn.rollback();
                                conn.close();
                                return false;
                            }
                        }

                        if (tFlag.equals("6"))
                        {
                            LJAGetOtherSet tLJAGetOtherSet = (LJAGetOtherSet)
                                    tSchemaSet;
                            LJAGetOtherDBSet tLJAGetOtherDBSet = new
                                    LJAGetOtherDBSet(conn);
                            tLJAGetOtherDBSet.set(tLJAGetOtherSet);
                            if (!tLJAGetOtherDBSet.update())
                            {
                                // @@错误处理
                                this.mErrors.copyAllErrors(tLJAGetOtherDBSet.
                                        mErrors);
                                CError tError = new CError();
                                tError.moduleName = "TempFeeBLS";
                                tError.functionName = "saveData";
                                tError.errorMessage = "付费明细表数据修改失败!";
                                this.mErrors.addOneError(tError);
                                conn.rollback();
                                conn.close();
                                return false;
                            }
                        }

                        if (tFlag.equals("7"))
                        {
                            LJABonusGetSet tLJABonusGetSet = (LJABonusGetSet)
                                    tSchemaSet;
                            LJABonusGetDBSet tLJABonusGetDBSet = new
                                    LJABonusGetDBSet(conn);
                            tLJABonusGetDBSet.set(tLJABonusGetSet);
                            if (!tLJABonusGetDBSet.update())
                            {
                                // @@错误处理
                                this.mErrors.copyAllErrors(tLJABonusGetDBSet.
                                        mErrors);
                                CError tError = new CError();
                                tError.moduleName = "TempFeeBLS";
                                tError.functionName = "saveData";
                                tError.errorMessage = "付费明细表数据修改失败!";
                                this.mErrors.addOneError(tError);
                                conn.rollback();
                                conn.close();
                                return false;
                            }
                        }
                    }
                } // @@错误处理
                LJTempFeeSet updLJTempFeeSet = new LJTempFeeSet();
                updLJTempFeeSet = (LJTempFeeSet) mInputData.
                                  getObjectByObjectName("LJTempFeeSet", 0);
                if (updLJTempFeeSet != null)
                {
                    LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB(conn);
                    tLJTempFeeDB.setTempFeeNo(updLJTempFeeSet.get(1).
                                              getTempFeeNo());
                    if (!tLJTempFeeDB.deleteSQL())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLJTempFeeDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "TempFeeBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "暂收表数据删除失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                    LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB(
                            conn);
                    tLJTempFeeClassDB.setTempFeeNo(updLJTempFeeSet.get(1).
                            getTempFeeNo());
                    System.out.println("tempfeeno11111:" +
                                       updLJTempFeeSet.get(1).getTempFeeNo());

                    this.mLJTempFeeSchema.setSchema(updLJTempFeeSet.get(1));

                    if (!tLJTempFeeClassDB.deleteSQL())
                    {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "TempFeeBLS";
                        tError.functionName = "saveData";
                        tError.errorMessage = "暂收表数据删除失败!";
                        this.mErrors.addOneError(tError);
                        conn.rollback();
                        conn.close();
                        return false;
                    }
                }
            }

// 暂交费分类表
            System.out.println("Start 暂交费分类表...");
//删除
            LJTempFeeClassDBSet delLJTempFeeClassDBSet = new
                    LJTempFeeClassDBSet(conn);
            delLJTempFeeClassDBSet.set((LJTempFeeClassSet) mInputData.
                                       getObjectByObjectName(
                    "LJTempFeeClassSet", 2));
            if (delLJTempFeeClassDBSet != null)
            {
                if (!delLJTempFeeClassDBSet.deleteSQL())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(delLJTempFeeClassDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "暂交费分类表数据删除失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }

//添加
            LJTempFeeClassDBSet tLJTempFeeClassDBSet = new LJTempFeeClassDBSet(
                    conn);
            tLJTempFeeClassDBSet.set((LJTempFeeClassSet) mInputData.
                                     getObjectByObjectName("LJTempFeeClassSet",
                    0));
            if (!tLJTempFeeClassDBSet.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLJTempFeeClassDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "TempFeeBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "暂交费表数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

// 暂交费表
            System.out.println("Start 暂交费表...");
            LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
            tLJTempFeeSet = (LJTempFeeSet) mInputData.getObjectByObjectName(
                    "LJTempFeeSet", 0);

            LJTempFeeDBSet tLJTempFeeDBSet = new LJTempFeeDBSet(conn);
            tLJTempFeeDBSet.set((LJTempFeeSet) mInputData.getObjectByObjectName(
                    "LJTempFeeSet", 0));
            if (!tLJTempFeeDBSet.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLJTempFeeDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "TempFeeBLS";
                tError.functionName = "saveData";
                tError.errorMessage = "暂交费表数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

//财务付费流程
            System.out.println("Start 财务付费流程...");
            VData FinFeeVData = new VData();
            VData tVData = new VData();
            //FinFeeVData=((VData)mInputData.getObjectByObjectName("VData",0));
            FinFeeVData = (VData) mInputData.getObject(3);
            int Index = 0;
            for (Index = 0; Index < FinFeeVData.size(); Index++)
            {
                tVData = (VData) FinFeeVData.get(Index);
                /** 财务给付表*/
                System.out.println("Start 财务给付表...");
                LJFIGetDB tLJFIGetDB = new LJFIGetDB(conn);
                tLJFIGetDB.setSchema((LJFIGetSchema) tVData.
                                     getObjectByObjectName("LJFIGetSchema", 0));
                if (!tLJFIGetDB.insert())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLJFIGetDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBLS";
                    tError.functionName = "save";
                    tError.errorMessage = "财务给付表数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
                System.out.println("财务给付表 ok ");

                /** 实付总表 */
                System.out.println("Start 实付总表...");
                LJAGetDB tLJAGetDB = new LJAGetDB(conn);
                tLJAGetDB.setSchema((LJAGetSchema) tVData.getObjectByObjectName(
                        "LJAGetSchema", 0));
                System.out.println("Get LJAGet");
                if (!tLJAGetDB.update())
                {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLJAGetDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBLS";
                    tError.functionName = "save";
                    tError.errorMessage = "实付总表数据保存失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
                System.out.println("实付总表 ok");

                /**更新子表 */
                System.out.println("Start 子表...");
                String flag = (String) tVData.getObjectByObjectName("String", 0);
                System.out.println("flag:" + flag);

                if (flag.equals("1")) // 给付表(生存领取_实付)
                {
                    LJAGetDrawDBSet mLJAGetDrawDBSet = new LJAGetDrawDBSet(conn);
                    mLJAGetDrawDBSet.set((LJAGetDrawSet) tVData.
                                         getObjectByObjectName("LJAGetDrawSet",
                            0));
                    if (mLJAGetDrawDBSet != null)
                    {
                        if (!mLJAGetDrawDBSet.update())
                        {
                            // @@错误处理
                            this.mErrors.copyAllErrors(mLJAGetDrawDBSet.mErrors);
                            CError tError = new CError();
                            tError.moduleName = "TempFeeBLS";
                            tError.functionName = "save";
                            tError.errorMessage = "更新 给付表(生存领取_实付)失败!";
                            this.mErrors.addOneError(tError);
                            conn.rollback();
                            conn.close();
                            return false;
                        }
                    }
                }

                if (flag.equals("3")) //批改补退费表(实收/实付)
                {
                    LJAGetEndorseDBSet mLJAGetEndorseDBSet = new
                            LJAGetEndorseDBSet(conn);
                    mLJAGetEndorseDBSet.set((LJAGetEndorseSet) tVData.
                                            getObjectByObjectName(
                            "LJAGetEndorseSet", 0));
                    if (mLJAGetEndorseDBSet != null)
                    {
                        if (!mLJAGetEndorseDBSet.update())
                        {
                            // @@错误处理
                            this.mErrors.copyAllErrors(mLJAGetEndorseDBSet.
                                    mErrors);
                            CError tError = new CError();
                            tError.moduleName = "TempFeeBLS";
                            tError.functionName = "save";
                            tError.errorMessage = "更新 批改补退费表(实收/实付)失败!";
                            this.mErrors.addOneError(tError);
                            conn.rollback();
                            conn.close();
                            return false;
                        }
                    }
                }

                if (flag.equals("4")) //暂交费退费实付表
                {
                    LJAGetTempFeeDBSet mLJAGetTempFeeDBSet = new
                            LJAGetTempFeeDBSet(conn);
                    mLJAGetTempFeeDBSet.set((LJAGetTempFeeSet) tVData.
                                            getObjectByObjectName(
                            "LJAGetTempFeeSet", 0));
                    System.out.println("judge begin...");
                    if (mLJAGetTempFeeDBSet != null)
                    {
                        System.out.println(
                                "mLJAGetTempFeeDBSet update begin...");
                        System.out.println("size:" + mLJAGetTempFeeDBSet.size());
                        if (!mLJAGetTempFeeDBSet.update())
                        {
                            // @@错误处理
                            this.mErrors.copyAllErrors(mLJAGetTempFeeDBSet.
                                    mErrors);
                            CError tError = new CError();
                            tError.moduleName = "TempFeeBLS";
                            tError.functionName = "save";
                            tError.errorMessage = "更新 暂交费退费实付表失败!";
                            this.mErrors.addOneError(tError);
                            conn.rollback();
                            conn.close();
                            return false;
                        }
                    }
                }

                if (flag.equals("5")) //赔付实付表
                {
                    LJAGetClaimDBSet mLJAGetClaimDBSet = new LJAGetClaimDBSet(
                            conn);
                    mLJAGetClaimDBSet.set((LJAGetClaimSet) tVData.
                                          getObjectByObjectName(
                            "LJAGetClaimSet", 0));
                    if (mLJAGetClaimDBSet != null)
                    {
                        if (!mLJAGetClaimDBSet.update())
                        {
                            // @@错误处理
                            this.mErrors.copyAllErrors(mLJAGetClaimDBSet.
                                    mErrors);
                            CError tError = new CError();
                            tError.moduleName = "TempFeeBLS";
                            tError.functionName = "save";
                            tError.errorMessage = "更新 赔付实付表失败!";
                            this.mErrors.addOneError(tError);
                            conn.rollback();
                            conn.close();
                            return false;
                        }
                    }
                }

                if (flag.equals("6")) //其他退费实付表
                {
                    LJAGetOtherDBSet mLJAGetOtherDBSet = new LJAGetOtherDBSet(
                            conn);
                    mLJAGetOtherDBSet.set((LJAGetOtherSet) tVData.
                                          getObjectByObjectName(
                            "LJAGetOtherSet", 0));
                    if (mLJAGetOtherDBSet != null)
                    {
                        if (!mLJAGetOtherDBSet.update())
                        {
                            // @@错误处理
                            this.mErrors.copyAllErrors(mLJAGetOtherDBSet.
                                    mErrors);
                            CError tError = new CError();
                            tError.moduleName = "TempFeeBLS";
                            tError.functionName = "save";
                            tError.errorMessage = "更新 其他退费实付表失败!";
                            this.mErrors.addOneError(tError);
                            conn.rollback();
                            conn.close();
                            return false;
                        }
                    }
                }

                if (flag.equals("7")) //红利给付实付表
                {
                    LJABonusGetDBSet mLJABonusGetDBSet = new LJABonusGetDBSet(
                            conn);
                    mLJABonusGetDBSet.set((LJABonusGetSet) tVData.
                                          getObjectByObjectName(
                            "LJABonusGetSet", 0));
                    if (mLJABonusGetDBSet != null)
                    {
                        if (!mLJABonusGetDBSet.update())
                        {
                            // @@错误处理
                            this.mErrors.copyAllErrors(mLJABonusGetDBSet.
                                    mErrors);
                            CError tError = new CError();
                            tError.moduleName = "TempFeeBLS";
                            tError.functionName = "save";
                            tError.errorMessage = "更新 红利给付实付表失败!";
                            this.mErrors.addOneError(tError);
                            conn.rollback();
                            conn.close();
                            return false;
                        }
                    }
                }

                System.out.println("子表 ok");
            } //for循环结束

            // * ↓ *** add *** liuhao *** 2005-05-17 *****
            //判断如果有预打保单生效内容作如下操作
            if (mInputData.size() > 6) {
                //进行实收集体交费处理
                LJAPayGrpDBSet tLJAPayGrpDBSet = new LJAPayGrpDBSet(conn);
                tLJAPayGrpDBSet.set((LJAPayGrpSet) mInputData.
                                    getObjectByObjectName(
                                            "LJAPayGrpSet", 0));
                if (!tLJAPayGrpDBSet.insert()) {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLJTempFeeDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "增加实收集体交费失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;

                }
                //添加实收个人交费信息
                LJAPayPersonDBSet tLJAPayPersonDBSet = new LJAPayPersonDBSet(
                        conn);
                tLJAPayPersonDBSet.set((LJAPayPersonSet) mInputData.
                                       getObjectByObjectName(
                                               "LJAPayPersonSet", 0));
                if (!tLJAPayPersonDBSet.insert()) {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLJTempFeeDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "添加实收个人交费信息失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
                //添加实收总表信息
                LJAPayDBSet tLJAPayDBSet = new LJAPayDBSet(conn);
                tLJAPayDBSet.set((LJAPaySet) mInputData.getObjectByObjectName(
                        "LJAPaySet", 0));
                if (!tLJAPayDBSet.insert()) {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLJTempFeeDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "添加实收总表信息失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
                //进行集体保单数据更新
                LCGrpContDBSet tLCGrpContDBSet = new LCGrpContDBSet(conn);
                tLCGrpContDBSet.set((LCGrpContSet) mInputData.
                                    getObjectByObjectName(
                                            "LCGrpContSet", 0));
                if (!tLCGrpContDBSet.update()) {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLJTempFeeDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "更新集体保单表数据失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
                //进行集体保单数据更新
                LCGrpPolDBSet tLCGrpPolDBSet = new LCGrpPolDBSet(conn);
                tLCGrpPolDBSet.set((LCGrpPolSet) mInputData.
                                    getObjectByObjectName(
                                    "LCGrpPolSet", 0));
                if (!tLCGrpPolDBSet.update()) {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLJTempFeeDBSet.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBLS";
                    tError.functionName = "saveData";
                    tError.errorMessage = "更新集体保单表数据失败!";
                    this.mErrors.addOneError(tError);
                    conn.rollback();
                    conn.close();
                    return false;
                }
            }
            // * ↑ *** add *** liuhao *** 2005-05-17 *****

            conn.commit();
            conn.close();
            System.out.println("commit end");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "TempFeeBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {}
            tReturn = false;
        }
        if (this.mOperate.equals("UPDATE"))
        {
            LJSPayDB tLJSPayDB = new LJSPayDB();
            tLJSPayDB.setGetNoticeNo(mLJTempFeeSchema.getTempFeeNo());
            tLJSPayDB.delete();
        }
        return tReturn;
    }
}
