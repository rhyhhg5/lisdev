/**
 * 2007-5-26
 */
package com.sinosoft.lis.finfee;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJInvoiceInfoSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class FFInvoiceBaseInfoBL
{
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private String mOperate;

    private MMap map = new MMap();

    private VData mInputData = new VData();

    private GlobalInput mGlobalInput = new GlobalInput();

    private LJInvoiceInfoSchema mLJInvoiceInfoSchema = new LJInvoiceInfoSchema();

    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        if (!getInputData(cInputData))
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        if (!dealData())
        {
            buildError("dealData", "数据处理失败！");
            return false;
        }

        if (!prepareOutputData())
        {
            buildError("prepareOutputData", "在准备往后层处理所需要的数据时出错。");
            return false;
        }

        try
        {
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                buildError("dealData", "数据提交失败！");
                return false;
            }
            mResult.clear();
            mResult.add(mLJInvoiceInfoSchema);
        }
        catch (Exception ex)
        {
            buildError("submitData", "其他未知异常！");
            return false;
        }
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        mLJInvoiceInfoSchema = (LJInvoiceInfoSchema) cInputData
                .getObjectByObjectName("LJInvoiceInfoSchema", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mGlobalInput == null || mLJInvoiceInfoSchema == null)
            return false;
        return true;
    }

    private boolean dealData()
    {
        if (mOperate.equals("INSERT"))
        {
            mLJInvoiceInfoSchema.setMakeDate(PubFun.getCurrentDate());
            mLJInvoiceInfoSchema.setMakeTime(PubFun.getCurrentTime());
            mLJInvoiceInfoSchema.setModifyDate(PubFun.getCurrentDate());
            mLJInvoiceInfoSchema.setModifyTime(PubFun.getCurrentTime());
            mLJInvoiceInfoSchema.setOperator(mGlobalInput.Operator);
            map.put(mLJInvoiceInfoSchema, "INSERT"); //插入
        }
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            this.mInputData.add(this.mLJInvoiceInfoSchema);
            mInputData.add(this.map);
        }
        catch (Exception ex)
        {
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FFInvoiceBaseInfoSaveBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
