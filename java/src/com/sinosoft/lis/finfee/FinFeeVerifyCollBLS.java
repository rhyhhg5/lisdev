package com.sinosoft.lis.finfee;

import com.sinosoft.utility.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.JdbcUrl;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class FinFeeVerifyCollBLS  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 数据操作字符串 */
  private String mOperate;  

  public FinFeeVerifyCollBLS() {
  }
  public static void main(String[] args) {
    FinFeeVerifyCollBLS mFinFeeVerifyCollBLS1 = new FinFeeVerifyCollBLS();
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    boolean tReturn =false;
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    System.out.println("Start FinFeeVerifyColl BLS Submit...");
    //信息保存
    if(this.mOperate.equals("VERIFY"))
    {tReturn=verify(cInputData);}

    if (tReturn)
      System.out.println("Verify sucessful");
    else
      System.out.println("Verify failed") ;
      
      System.out.println("End FinFeeVerifyColl BLS Submit...");

    return tReturn;
  }
  
//核销事务操作  
  private boolean verify(VData mInputData)
  {
    boolean tReturn =true;
    System.out.println("Start Verify...");
    Connection conn = DBConnPool.getConnection();
    if (conn==null)
    {
    		// @@错误处理
    		CError tError = new CError();
		tError.moduleName = "FinFeeVerifyCollBLS";
                tError.functionName = "verifyData";
	        tError.errorMessage = "数据库连接失败!";
		this.mErrors .addOneError(tError) ;
 		return false;
    }
    try{
      conn.setAutoCommit(false);

// 暂交费分类表更新
      System.out.println("Start 暂交费分类表...");
      LJTempFeeClassDBSet tLJTempFeeClassDBSet=new LJTempFeeClassDBSet(conn);
      tLJTempFeeClassDBSet.set((LJTempFeeClassDBSet)mInputData.getObjectByObjectName("LJTempFeeClassDBSet",0));
      if (!tLJTempFeeClassDBSet.update())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJTempFeeClassDBSet.mErrors);
    		CError tError = new CError();
	        tError.moduleName = "FinFeeVerifyCollBLS";
	        tError.functionName = "updateData";
	        tError.errorMessage = "暂交费表数据保存失败!";
	        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 	        return false;
      }      
      System.out.println("暂交费分类表更新");      
// 暂交费表更新
      System.out.println("Start 暂交费表...");
      LJTempFeeDBSet tLJTempFeeDBSet=new LJTempFeeDBSet(conn);
      tLJTempFeeDBSet.set((LJTempFeeDBSet)mInputData.getObjectByObjectName("LJTempFeeDBSet",0));
      if (!tLJTempFeeDBSet.update())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJTempFeeDBSet.mErrors);
    		CError tError = new CError();
		tError.moduleName = "FinFeeVerifyCollBLS";
		tError.functionName = "updateData";
		tError.errorMessage = "暂交费表数据保存失败!";
		this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 		return false;
      }    
System.out.println("暂交费表更新");      

//实收总表添加 
System.out.println("Start 实收总表...");
      LJAPayDB tLJAPayDB=new LJAPayDB(conn);  
      tLJAPayDB.setSchema((LJAPayBL)mInputData.getObjectByObjectName("LJAPayBL",0));
      if (!tLJAPayDB.insert())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJAPayDB.mErrors);
    		CError tError = new CError();
		tError.moduleName = "FinFeeVerifyCollBLS";
		tError.functionName = "saveData";
		tError.errorMessage = "实收总表数据保存失败!";
		this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 		return false;
      }  
System.out.println("实收总表添加");

//实收集体交费表的添加
System.out.println("Start 实收集体交费表...");
      LJAPayGrpDB tLJAPayGrpDB=new LJAPayGrpDB(conn);  
      tLJAPayGrpDB.setSchema((LJAPayGrpBL)mInputData.getObjectByObjectName("LJAPayGrpBL",0));
      if (!tLJAPayGrpDB.insert())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJAPayGrpDB.mErrors);
    		CError tError = new CError();
		tError.moduleName = "FinFeeVerifyCollBLS";
		tError.functionName = "saveData";
		tError.errorMessage = "实收集体交费表数据保存失败!";
		this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 		return false;
      }  
System.out.println("实收集体交费表添加");

//实收个人交费表添加
System.out.println("Start 实收个人交费表...");
      LJAPayPersonDBSet tLJAPayPersonDBSet=new LJAPayPersonDBSet(conn);
      tLJAPayPersonDBSet.set((LJAPayPersonSet)mInputData.getObjectByObjectName("LJAPayPersonSet",0));
      if (!tLJAPayPersonDBSet.insert())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJAPayPersonDBSet.mErrors);
    		CError tError = new CError();
	        tError.moduleName = "FinFeeVerifyCollBLS";
	        tError.functionName = "saveData";
	        tError.errorMessage = "实收个人交费表数据保存失败!";
	        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 	        return false;
      }      
System.out.println("实收个人交费表添加");

//删除应收总表
      System.out.println("Start 删除应收总表...");
      LJSPayDB tLJSPayDB=new LJSPayDB(conn);  
      tLJSPayDB.setSchema((LJSPayBL)mInputData.getObjectByObjectName("LJSPayBL",0));
      System.out.println("Delete LJSPay");
      if (!tLJSPayDB.delete())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJSPayDB.mErrors);
    		CError tError = new CError();
		tError.moduleName = "FinFeeVerifyCollBLS";
		tError.functionName = "deleteData";
		tError.errorMessage = "应收总表数据删除失败!";
		this.mErrors .addOneError(tError) ;
        conn.rollback();
        conn.close();
 		return false;
      } 
System.out.println("删除应收总表");

//删除应收集体交费表
      System.out.println("Start 删除应收集体交费表...");
      LJSPayGrpDB tLJSPayGrpDB=new LJSPayGrpDB(conn);  
      tLJSPayGrpDB.setSchema((LJSPayGrpBL)mInputData.getObjectByObjectName("LJSPayGrpBL",0));
      System.out.println("Delete LJSPayGrp");
      if (!tLJSPayGrpDB.delete())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJSPayGrpDB.mErrors);
    		CError tError = new CError();
		tError.moduleName = "FinFeeVerifyCollBLS";
		tError.functionName = "deleteData";
		tError.errorMessage = "应收集体交费表数据删除失败!";
		this.mErrors .addOneError(tError) ;
        conn.rollback();
        conn.close();
 		return false;
      } 
System.out.println("删除应收集体交费表");

//删除应收个人交费表
      System.out.println("Start 删除个人交费表...");
      LJSPayPersonDBSet tLJSPayPersonDBSet=new LJSPayPersonDBSet(conn);
      tLJSPayPersonDBSet.set((LJSPayPersonSet)mInputData.getObjectByObjectName("LJSPayPersonSet",0));
      if (!tLJSPayPersonDBSet.delete())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJSPayPersonDBSet.mErrors);
    		CError tError = new CError();
	        tError.moduleName = "FinFeeVerifyCollBLS";
	        tError.functionName = "deleteData";
	        tError.errorMessage = "应收个人交费表数据删除失败!";
	        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 	        return false;
      }      

System.out.println("删除应收个人交费表");
     
      conn.commit() ;
      conn.close();

    }
    catch (Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="FinFeeVerifyCollBLS";
      tError.functionName="submitData";
      tError.errorMessage=ex.toString();
      this.mErrors .addOneError(tError);
      try{conn.rollback() ;} catch(Exception e){}
      tReturn=false;
    }
    return tReturn;
  }
} 