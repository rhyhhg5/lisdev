/**
 * 2011-10-22
 */
package com.sinosoft.lis.finfee;

import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import com.sinosoft.lis.db.LJInvoiceInfoDB;
import com.sinosoft.lis.db.LOPRTInvoiceManagerDB;
import com.sinosoft.lis.easyscan.ProposalDownloadBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LJInvoiceInfoSet;
import com.sinosoft.lis.vschema.LOPRTInvoiceManagerSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 * 
 */
public class FFIvoiceExportTjBL implements InvoiceExport
{
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private ArrayList mResult = new ArrayList();

    private String mBasePath = "";

    private String mOutPath = "";
    
    /**
     * 纳税人识别号
     */
    private String FSH = "";

    /**
     * 纳税人名称
     */
    private String FMC = "";

    /**
     * 所属期起
     */
    private String mSsqq = "";

    /**
     * 所属期止
     */
    private String mSsqz = "";

    /**
     * 收付费发票标记
     */
    private String mSFState = "";

    private int sequenceNo = 1;

    private String filePath = "";
    
    /**
     * 发票查询导出方式
     */
    private String mExPortType = "";
    
    /**
     * 发票代码 -按发票号码打印时使用
     */
    private String mInvoiceCode = "";
    
    /**
     * 发票号码 -按发票号码打印时使用
     */
    private String mInvoiceStartNo = "";
    private String mInvoiceEndNo = "";

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData))
        {
            buildError("getInputData", "没有获取到足够业务信息或者机构不对");
            return false;
        }
        if (!exportZIP())
        {
            buildError("exportZIP", "生成zip文件失败");
            return false;
        }
        return true;
    }

    /**
     * 获取业务数据
     * 
     * @param cInputData
     * @return
     */
    private boolean getInputData(VData cInputData)
    {
        TransferData tParameters = (TransferData) cInputData
                .getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        String strSql = "select SysVarValue from LDSysVar where SysVar = 'ZipFilePath'";

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(strSql);
        filePath = tSSRS.GetText(1, 1);

        if (mGlobalInput == null || tParameters == null)
        {
            return false;
        }

        this.mBasePath = (String) tParameters.getValueByName("hostBasePath") + "/";
        this.mOutPath = (String) tParameters.getValueByName("outPath") + "/";

        this.mSsqq = (String) tParameters.getValueByName("invoiceStartDate");
        this.mSsqz = (String) tParameters.getValueByName("invoiceEndDate");
        this.mSFState = (String) tParameters.getValueByName("SFState");
        this.mExPortType = (String) tParameters.getValueByName("ExPortType");
        
        this.mInvoiceCode = (String) tParameters.getValueByName("invoiceCode");
        this.mInvoiceStartNo = (String) tParameters.getValueByName("invoiceStartNo");
        this.mInvoiceEndNo = (String) tParameters.getValueByName("invoiceEndNo");
        // 获取条件内的相关发票打印信息
        String tSql = "";
        if(mExPortType == null || mExPortType.equals("")){
            System.out.println("-----按日期进行打印----");
            if ("1".equals(mSFState))
            {
                tSql = "select * from LOPRTInvoiceManager a "
                        + " where 1 = 1 "
                        + " and exists (select 1 from loprtmanager2 where code='37' and standbyflag4=a.invoiceno and makedate=a.makedate and comcode=a.comcode)"
                        + " and comcode like '" + mGlobalInput.ManageCom + "%'"
                        + " and makedate >= '" + mSsqq + "' "
                        + " and makedate <= '" + mSsqz + "'";
            }
            else
            {
                tSql = "select * from LOPRTInvoiceManager a "
                        + " where 1 = 1 "
                        + " and not exists (select 1 from loprtmanager2 where code='37' and standbyflag4=a.invoiceno and makedate=a.makedate and comcode=a.comcode)"
                        + " and comcode like '" + mGlobalInput.ManageCom + "%'"
                        + " and makedate >= '" + mSsqq + "' "
                        + " and makedate <= '" + mSsqz + "'";
            }
        } else {
            if (mInvoiceCode == null || mInvoiceStartNo == null || mInvoiceEndNo == null)
            {
                buildError("getInputData", "发票代码或发票号码为空");
                return false;
            } else {
                try {
                    Long.parseLong(mInvoiceCode);
                    Long.parseLong(mInvoiceStartNo);
                    Long.parseLong(mInvoiceEndNo);
                } catch (Exception ex){
                    ex.printStackTrace();
                    buildError("getInputData", "发票代码或发票号码格式错误");
                    return false;
                }
            }
            String makedateSql = "";
            if(mSsqq != null && !mSsqq.equals("")){
                makedateSql = makedateSql + " and makedate >= '" + mSsqq + "' ";
            }
            if(mSsqz != null && !mSsqz.equals("")){
                makedateSql = makedateSql + " and makedate <= '" + mSsqz + "'";
            }
            System.out.println("--发票代码:" + mInvoiceCode + "--发票号码:" + mInvoiceStartNo + "-" +  mInvoiceEndNo);
            System.out.println("-----按发票代码进行打印----");
            if ("1".equals(mSFState))
            {
                tSql = "select * from LOPRTInvoiceManager a "
                        + " where 1 = 1 "
                        + " and exists (select 1 from loprtmanager2 where code='37' and standbyflag4=a.invoiceno and makedate=a.makedate and comcode=a.comcode)"
                        + " and comcode like '" + mGlobalInput.ManageCom + "%'"
                        + " and invoicecode='" + mInvoiceCode + "' and int(invoiceno) between " + mInvoiceStartNo + " and " + mInvoiceEndNo + " ";
                tSql = tSql + makedateSql;
            }
            else
            {
                tSql = "select * from LOPRTInvoiceManager a "
                        + " where 1 = 1 "
                        + " and not exists (select 1 from loprtmanager2 where code='37' and standbyflag4=a.invoiceno and makedate=a.makedate and comcode=a.comcode)"
                        + " and comcode like '" + mGlobalInput.ManageCom + "%'"
                        + " and invoicecode='" + mInvoiceCode + "' and int(invoiceno) between " + mInvoiceStartNo + " and " + mInvoiceEndNo + " ";
                tSql = tSql + makedateSql;
            }
        }
        
        LOPRTInvoiceManagerDB tLOPRTInvoiceManagerDB = new LOPRTInvoiceManagerDB();
        LOPRTInvoiceManagerSet tLOPRTInvoiceManagerSet = tLOPRTInvoiceManagerDB
                .executeQuery(tSql);
        System.out.println(tSql);
        if (tLOPRTInvoiceManagerSet == null
                || tLOPRTInvoiceManagerSet.size() == 0){
            buildError("getInputData", "未查到相关发票数据");
            return false;
        }
        tSql = "select * from LJInvoiceInfo where comcode = '"
                + mGlobalInput.ManageCom + "' " + " and invoicecode='"
                + tLOPRTInvoiceManagerSet.get(1).getInvoiceCode() + "'"
                + " order by makedate desc fetch first 1 rows only with ur ";
        System.out.println(tSql);
        LJInvoiceInfoDB tLJInvoiceInfoDB = new LJInvoiceInfoDB();
        LJInvoiceInfoSet tLJInvoiceInfoSet = tLJInvoiceInfoDB
                .executeQuery(tSql);
        if (tLJInvoiceInfoSet == null || tLJInvoiceInfoSet.size() <= 0){
            return false;
        }
        this.FSH = tLJInvoiceInfoSet.get(1).getTaxpayerNo();
        this.FMC = tLJInvoiceInfoSet.get(1).getTaxpayerName();

        if (!exportXML(tLOPRTInvoiceManagerSet, tLJInvoiceInfoSet))
        {
            buildError("exportXML", "导出明细xml文件失败");
            return false;
        }

        if (!exportHzXML(tLOPRTInvoiceManagerSet, tLJInvoiceInfoSet))
        {
            buildError("exportXML", "导出汇总XML文件失败");
            return false;
        }
        return true;
    }

    private boolean exportXML(LOPRTInvoiceManagerSet tLOPRTInvoiceManagerSet,
            LJInvoiceInfoSet tLJInvoiceInfoSet)
    {
        System.out.println("export xml begin...");
        DecimalFormat df = new DecimalFormat("000");

        String outFileName = "";

        int runningNo = 1;

        try
        {
            while (runningNo <= tLOPRTInvoiceManagerSet.size())
            {
                double money = 0;
                int sum = 0;

                for (int i = runningNo; i <= tLOPRTInvoiceManagerSet.size()
                        && i <= (sequenceNo * 2000); i++)
                {
                    money = money
                            + PubFun.setPrecision(tLOPRTInvoiceManagerSet
                                    .get(i).getXSumMoney(), "0.00");
                    sum = sum + 1;
                }

                // 创建根节点 list
                Element root = new Element("FPXX");
                // 根节点添加到文档中
                Document doc = new Document(root);

                Element elements = new Element("BTXX");
                elements.addAttribute("QYBM", "30066182");
                elements.addAttribute("QYMC", "中国人民健康保险股份有限公司天津分公司");
                elements.addAttribute("SWDJZH", "120114792528320");
                elements.addAttribute("SBRQ", PubFun.getCurrentDate());
                elements.addAttribute("WJLX", "14");
                elements.addAttribute("FPHJJE", PubFun.format(money));
                elements.addAttribute("FPHJFS", sum + "");
                elements.addAttribute("SJLY", "2");
                root.addContent(elements);

                for (; runningNo <= tLOPRTInvoiceManagerSet.size()
                        && runningNo <= (sequenceNo * 2000); runningNo++)
                {
                    Element elements2 = new Element("ROW");
                    if ("1".equals(mSFState))
                    {
                        elements2.addAttribute("FMC", FMC);
                        elements2.addAttribute("FSH", FSH);
                    }
                    else
                    {
                        elements2.addAttribute("FMC", tLOPRTInvoiceManagerSet.get(runningNo).getPayerName());
                        elements2.addAttribute("FSH", "");
                    }
                    elements2.addAttribute("XMZY", "保险费");
                    elements2.addAttribute("JE", PubFun.format(tLOPRTInvoiceManagerSet
                                    .get(runningNo).getXSumMoney()));
                    if (tLOPRTInvoiceManagerSet.get(runningNo).getMakeDate() != null)
                    {
                        elements2.addAttribute("RQ", tLOPRTInvoiceManagerSet
                                .get(runningNo).getMakeDate());
                    }
                    else
                    {
                        elements2.addAttribute("RQ", PubFun.getCurrentDate());
                    }
                    elements2.addAttribute("DM", tLOPRTInvoiceManagerSet.get(runningNo)
                            .getInvoiceCode());
                    elements2.addAttribute("HM", tLOPRTInvoiceManagerSet.get(runningNo)
                            .getInvoiceNo());
                    elements2.addAttribute("SMC", FMC);
                    elements2.addAttribute("SSH", FSH);
                    if(tLOPRTInvoiceManagerSet.get(runningNo).getStateFlag().equals("0")){
                        elements2.addAttribute("FPZT", "0");
                    } else {
                        elements2.addAttribute("FPZT", "2");
                    }
                    elements2.addAttribute("CXDM", "");
                    elements2.addAttribute("CXHM", "");
                    root.addContent(elements2);
                }

                outFileName = "30066182_14_InvoiceInfo_"
                        + df.format(sequenceNo) + ".xml";
                sequenceNo++;
                XMLOutputter XMLOut = new XMLOutputter();

                String path = filePath + outFileName;
//                path = "E:\\loong\\test\\" + outFileName; //测试使用
                XMLOut.setEncoding("GBK");
                XMLOut.output(doc, new FileOutputStream(path));
                System.out.println("输出路径" + path);
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }

        System.out.println("created xml files " + filePath + outFileName);
        System.out.println("export xml finished...");
        return true;
    }

    private boolean exportHzXML(LOPRTInvoiceManagerSet tLOPRTInvoiceManagerSet,
            LJInvoiceInfoSet tLJInvoiceInfoSet)
    {
        System.out.println("export xml begin...");

        String outFileName = "30066182_FileList.xml";
        try
        {
            double money = 0;
            int sum = 0;

            for (int i = 1; i <= tLOPRTInvoiceManagerSet.size(); i++)
            {
                money = money
                        + PubFun.setPrecision(tLOPRTInvoiceManagerSet.get(i)
                                .getXSumMoney(), "0.00");
                sum = sum + 1;
            }
            // 创建根节点 list
            Element root = new Element("WJXX");
            // 根节点添加到文档中
            Document doc = new Document(root);

            Element elements = new Element("WJQD");
            Element elements2 = new Element("BTXX");
            elements.addContent(elements2);
            elements2.addAttribute("QYBM", "30066182");
            elements2.addAttribute("QYMC", "中国人民健康保险股份有限公司天津分公司");
            elements2.addAttribute("SWDJZH", "120114792528320");
            elements2.addAttribute("SBRQ", PubFun.getCurrentDate());
            elements2.addAttribute("WJLX", "14");
            elements2.addAttribute("FPHJJE", PubFun.format(money));
            elements2.addAttribute("FPHJFS", sum + "");
            elements2.addAttribute("SJLY", "2");

            Element elements3 = new Element("FileInfo");
            DecimalFormat df = new DecimalFormat("000");
            for (int i = 1; i < sequenceNo; i++)
            {
                Element elements4 = new Element("Row");
                elements4.addAttribute("WJLX", "14");
                elements4.addAttribute("WJMC", "30066182_14_InvoiceInfo_"
                        + df.format(i) + ".xml");
                elements3.addContent(elements4);
            }

            elements.addContent(elements3);
            // 给父节点list添加user子节点
            root.addContent(elements);
            XMLOutputter XMLOut = new XMLOutputter();

            // 输出 user.xml 文件
            String path = filePath + outFileName;
//            path = "E:\\loong\\test\\" + outFileName; //测试使用
            XMLOut.setEncoding("GBK");
            XMLOut.output(doc, new FileOutputStream(path));
            System.out.println("输出路径" + path);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }

        System.out.println("created xml files " + filePath + outFileName);
        System.out.println("export xml finished...");
        return true;
    }

    public boolean exportZIP()
    {
        // 压缩
        List mChecksValuesPath = new ArrayList();
        List mChecksValues = new ArrayList();

        DecimalFormat df = new DecimalFormat("000");

        String fileName = "";

        for (int i = 1; i < sequenceNo; i++)
        {
            fileName = "30066182_14_InvoiceInfo_" + df.format(i) + ".xml";
            mChecksValuesPath.add(filePath + fileName);
            mChecksValues.add(fileName);
            System.out.println("导入文件：" + filePath + fileName);
        }
        fileName = "30066182_FileList.xml";
        mChecksValuesPath.add(filePath + fileName);
        mChecksValues.add(fileName);
        System.out.println("导入文件：" + filePath + fileName);

        String[] cChecksValuesPath = (String[]) mChecksValuesPath
                .toArray(new String[0]);
        String[] cChecksValues = (String[]) mChecksValues
                .toArray(new String[0]);

        ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
        String zipName = "U_30066182_0001_invoiceinfo.zip";
        String zipfilepath = mBasePath + mOutPath + zipName;
        if (!tProposalDownloadBL.CreatZipFile(cChecksValuesPath, cChecksValues,
                zipfilepath))
        {
            System.out.println("生成压缩文件失败!");
            return false;
        }
        else
        {
            mResult.add(mOutPath + zipName);
            System.out.println(mBasePath + mOutPath + zipName);
            return true;
        }
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FFIvoiceExportTjBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public List getResult()
    {
        return mResult;
    }
    
    public CErrors getErrors(){
    	return mErrors;
    }

    public static void main(String[] args)
    {
        System.out.println(PubFun.format(1.0066884780000005E7));
        System.out.println(PubFun.format(PubFun.setPrecision(1.0066884780000005E7, "0.00")));
    }
}
