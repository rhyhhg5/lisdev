package com.sinosoft.lis.finfee;

/**
 * @author : yanjing
 * @date:2009-08-12
 * @version 1.0
 */
import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.LDCode1DBSet;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.ClaimDayBalanceBL;


public class FeeInvoiceAddBL {
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
         public CErrors mErrors = new CErrors();
//         private VData mResult = new VData();
         private String mWrapName ;
         private String mManageCom;
         private String mInvoiceName;
         private TransferData mAddElement = new TransferData(); //获取时间
         private GlobalInput mGlobalInput = new GlobalInput(); //全局变量
         private String mResult;
    public FeeInvoiceAddBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        try{
        if (!cOperate.equals("CHILD")&& !cOperate.equals("HEAD") &&!cOperate.equals("REPAIR") && !cOperate.equals("DELETE")) {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }
        if (!getInputData(cInputData)) {
            return false;
        }
        mResult="";
        System.out.println("yj come to saveInvoiceAdd");
            if (!saveInvoiceAdd()) {
                return false;
            }
            mResult="true";
        }catch(Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "FinBankAddBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理错误! " + e.getMessage();
            this.mErrors .addOneError(tError);
            return false;
          }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) { //打印付费
        //全局变量
        mAddElement = (TransferData) cInputData.getObjectByObjectName("TransferData",0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        if (mGlobalInput == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        mWrapName = (String) mAddElement.getValueByName("WrapName");
        mManageCom = (String) mAddElement.getValueByName("ManageCom");
        mInvoiceName = (String) mAddElement.getValueByName("InvoiceName");
        System.out.println("mWrapName:"+mWrapName+" mManageCom:"+mManageCom+" mInvoiceName:"+mInvoiceName);
        return true;
    }

    public String getResult() {
        return mResult;
    }
    

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "FinBankAddBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
// 添加子银行
    private boolean saveInvoiceAdd() {
        
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String sql = "select riskcode from ldriskwrap where riskwrapcode = '" +
        mWrapName + "' with ur";
        String sql2="";
        tSSRS = tExeSQL.execSQL(sql);
        if (tSSRS.getMaxRow()>0)
        {   LDCode1DBSet tLDCode1DBSet=new LDCode1DBSet();
            for(int a=1;a<=tSSRS.getMaxRow();a++){
                LDCode1Schema mLDCode1Schema = new LDCode1Schema();
                mLDCode1Schema.setCodeType("bankcheckappendrisk");
                mLDCode1Schema.setCode(mWrapName);
                mLDCode1Schema.setCode1(tSSRS.GetText(a, 1));
                mLDCode1Schema.setComCode(mManageCom);
                mLDCode1Schema.setRiskWrapPlanName(mInvoiceName);           
                tLDCode1DBSet.add(mLDCode1Schema);
                }
           
        sql2="select 1 from ldcode1 where codetype='bankcheckappendrisk' and code='"
            +mWrapName+"' with ur";
        if(tExeSQL.execSQL(sql2).getMaxRow()>0){
            if (!tLDCode1DBSet.update())
            {
                buildError("FeeInvoiceAddBL->saveInvoiceAdd","修改发票套餐信息失败");
                return false;
            }
        }else{
            if (!tLDCode1DBSet.insert())
            {
                buildError("FeeInvoiceAddBL->saveInvoiceAdd","插入发票套餐信息失败");
                return false;
            }
        }
        mResult=mWrapName;
        }else{
            buildError("FeeInvoiceAddBL->saveInvoiceAdd","ldriskwrap表没有该套餐描述");
            return false;
        }
        return true;
    }

    //生成XML函数
    private boolean productXml(String msql)
    {
        return true;
    }
}
