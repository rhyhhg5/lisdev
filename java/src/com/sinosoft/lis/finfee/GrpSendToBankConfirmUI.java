package com.sinosoft.lis.finfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

public class GrpSendToBankConfirmUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public GrpSendToBankConfirmUI() {}

  public static void main(String[] args)
  {
  GlobalInput tGI = new GlobalInput();
	TransferData tTransferData = new TransferData();
	LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
	GrpSendToBankConfirmUI tGrpSendToBankConfirmUI  = new GrpSendToBankConfirmUI();

  tTransferData.setNameAndValue( "GetNoticeNo","16000002810801");
  tLCGrpContSchema.setPrtNo("16000002810");
  tLCGrpContSchema.setPayMode("1");
  //tLCGrpContSchema.setBankCode("0701");
 // tLCGrpContSchema.setAccName("冬雨");
 // tLCGrpContSchema.setBankAccNo("11111");
  tGI.Operator="fi001";
  tGI.ManageCom="86";
  tGI.ComCode="86";

			VData tVData = new VData();
			tVData.add(tTransferData);
			tVData.add(tLCGrpContSchema);
			tVData.add( tGI );
      tGrpSendToBankConfirmUI.submitData(tVData,"PAYMODE");
  }

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    System.out.println("---GrpSendToBankConfirmUI BEGIN---");
    GrpSendToBankConfirmBL tGrpSendToBankConfirmBL = new GrpSendToBankConfirmBL();
    if (tGrpSendToBankConfirmBL.submitData(cInputData,mOperate) == false)
	{
  		// @@错误处理
      this.mErrors.copyAllErrors(tGrpSendToBankConfirmBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "GrpSendToBankConfirmUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors.addOneError(tError) ;
      mResult.clear();
      return false;
	}
	  System.out.println("---GrpSendToBankConfirmUI END---");
    return true;
  }
}
