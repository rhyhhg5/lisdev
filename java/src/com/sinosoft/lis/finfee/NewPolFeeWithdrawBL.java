package com.sinosoft.lis.finfee;

import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;
import java.util.*;
import java.sql.*;
import java.lang.String;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class NewPolFeeWithdrawBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData;
    /** 数据操作字符串 */
    private GlobalInput mGI = new GlobalInput();
    private String mOperate;
    private String tLimit = "";
    private String serNo = ""; //流水号
    private String getNo = ""; //给付收据号码
    private String prtSeqNo = ""; //印刷流水号
    private boolean specWithDrawFlag = false; //特殊的退费标记。该标记为真时，不考虑outpayflag标志
    private TransferData tTransferData = new TransferData();
    //private String mGetNoticeNo="";

    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();

    private LJAGetSchema mLJAGetSchema = new LJAGetSchema(); //实付总表
    private LJAGetSet mLJAGetSet = new LJAGetSet();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema(); //打印管理表
    private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();
    private LCPolSchema mLCPolSchema = new LCPolSchema(); //个人保单表
    private LCPolSet mLCPolSet = new LCPolSet();
    private LCPolSchema mNewLCPolSchema = new LCPolSchema(); //个人保单表
    private LCPolSet mNewLCPolSet = new LCPolSet();
    private LCPolSet mSaveLCPolSet = new LCPolSet();
    private LJAGetOtherSchema mLJAGetOtherShema = new LJAGetOtherSchema(); //其它其他退费实付表
    private LJAGetOtherSet mLJAGetOtherSet = new LJAGetOtherSet(); //其它其他退费实付表
    private LCContSchema mNewLCContSchema = new LCContSchema(); //缓存传入的新个单合同


    public NewPolFeeWithdrawBL() {
    }

    public static void main(String[] args) {
        NewPolFeeWithdrawBL NewPolFeeWithdrawBL1 = new NewPolFeeWithdrawBL();
        VData tVData = new VData();
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "8600";
        tGI.ComCode = "8600";
        tGI.Operator = "hzm";
        LCPolSet tLCPolSet = new LCPolSet();

        String tPolNo[] = {"21000011061", "21000011062", "21000011076"};
        String tContNo[] = {"00001645504", "00001645504", "00001645505"};
        for (int index = 0; index < tPolNo.length; index++) {
            LCPolSchema tLCPolSchema = new LCPolSchema();
            String PolNo = tPolNo[index];
            tLCPolSchema.setPolNo(PolNo);
            tLCPolSchema.setContNo(tContNo[index]);
            tLCPolSet.add(tLCPolSchema);
        }
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("SpecWithDraw", "1");
//    LCPolSchema tLCPolSchema = new LCPolSchema();
//    //tLCPolSchema.setProposalNo("86110020030110000065");
//    tLCPolSchema.setPolNo("86110020030210000128");
//    tLCPolSet.add(tLCPolSchema);
        tVData.add(tLCPolSet);
        tVData.add(tTransferData);
        tVData.add(tGI);

        NewPolFeeWithdrawBL1.submitData(tVData);
    }


//传输数据的公共方法
    public boolean submitData(VData tVData) {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(tVData)) {
            return false;
        }
        System.out.println("After getinputdata");

        //进行业务处理
        if (!dealData()) {
            return false;
        }
        System.out.println("After dealData！");
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("After prepareOutputData");

        System.out.println("Start NewPolFeeWithdrawBL BL Submit...");

        NewPolFeeWithdrawBLS tNewPolFeeWithdrawBLS = new NewPolFeeWithdrawBLS();
        tNewPolFeeWithdrawBLS.submitData(mInputData);

        System.out.println("End NewPolFeeWithdrawBL BL Submit...");

        //如果有需要处理的错误，则返回
        if (tNewPolFeeWithdrawBLS.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tNewPolFeeWithdrawBLS.mErrors);
        }

        mInputData = null;
        return true;
    }

    /**
     * 执行溢交保费退费和暂交费退费的共用模块
     * @param tVData
     * @return
     */
    public boolean submitDataAll(VData tVData) {
        // 处理具体请求
        boolean tResult = dealSubmitDataAll(tVData);
        if (tResult == false) {
            return false;
        }

        System.out.println("Start NewPolFeeWithdrawBL BL Submit...");

        NewPolFeeWithdrawBLS tNewPolFeeWithdrawBLS = new NewPolFeeWithdrawBLS();
        tNewPolFeeWithdrawBLS.submitData(mInputData);

        System.out.println("End NewPolFeeWithdrawBL BL Submit...");

        //如果有需要处理的错误，则返回
        if (tNewPolFeeWithdrawBLS.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tNewPolFeeWithdrawBLS.mErrors);
        }

        mInputData = null;
        return true;
    }

    private boolean dealSubmitDataAll(VData tVData) {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(tVData)) {
            return false;
        }
        System.out.println("After getinputdata");

        //进行业务处理
        if (!dealDataForMainRisk()) {
            return false;
        }
        System.out.println("After dealData！");
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        System.out.println("After prepareOutputData");

        //准备暂交费退费数据
        VData tempfeeData = prepareTempfeeData();
        if (tempfeeData != null) { //如果有数据
            mInputData.add(tempfeeData);
        }
        return true;
    }

    /**
     * 执行溢交保费退费和暂交费退费的共用模块
     * 修改自submitDataAll,只是准备数据，并且返回数据
     * @param tVData VData
     * @return MMap
     */
    public MMap submitDataAllNew(VData tVData) {

        boolean tResult = dealSubmitDataAll(tVData);
        if (tResult == false) {
            return null;
        }

        MMap tmpMap = new MMap();
//    tmpMap.put( (LCPolSet) mInputData.getObjectByObjectName("LCPolSet", 0),
//               "UPDATE"); //个人保单表
        tmpMap.put((LJAGetSet) mInputData.getObjectByObjectName("LJAGetSet", 0),
                   "INSERT"); //实付总表
        tmpMap.put((LJAGetOtherSet) mInputData.getObjectByObjectName(
                "LJAGetOtherSet", 0), "INSERT"); //实付子表
        tmpMap.put((LOPRTManagerSet) mInputData.getObjectByObjectName(
                "LOPRTManagerSet", 0), "INSERT"); /** 打印管理表 */
        //暂交费退费
        VData tempfeeVData = new VData();
        tempfeeVData = (VData) mInputData.getObjectByObjectName("VData", 0);
        if (tempfeeVData != null) {
            tmpMap.add((MMap) tempfeeVData.getObjectByObjectName("MMap", 0));

        }
        return tmpMap;
    }

//根据前面的输入数据，进行逻辑处理
//如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData() {
        //产生流水号
        tLimit = PubFun.getNoLimit(mGI.ManageCom);
        serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        //2-准备实付表纪录,产生实付号
        tLimit = PubFun.getNoLimit(this.mNewLCContSchema.getManageCom()); //主附险共用一个给付通知书号
        getNo = PubFun1.CreateMaxNo("GETNO", tLimit);
        LCPolSet tLCPolSet = new LCPolSet();
        String sql = "select * from ljtempfeeclass where tempfeeno in(select tempfeeno from ljtempfee where (otherno='"
                     + mNewLCContSchema.getContNo() + "' or otherno='" +
                     mNewLCContSchema.getPrtNo() + "' ) and confflag='1')";
        LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
        LJTempFeeClassSet tLJTempFeeClassSet = tLJTempFeeClassDB.
                                               executeQuery(sql);
        String payMode = "";
        String BankCode = "";
        String BankAccNo = "";
        String AccName = "";

        if (tLJTempFeeClassSet != null) {
            if (tLJTempFeeClassSet.size() > 0) {
                LJTempFeeClassSchema tLJTemp = tLJTempFeeClassSet.get(1);
                payMode = tLJTemp.getPayMode();
                if (payMode.equals("4")) {
                    if (tLJTemp.getBankAccNo() == null ||
                        tLJTemp.getBankCode() == null) {
                        //如果银行编号，账号为空那么认为是现金付费
                        payMode = "1";
                    } else {
                        BankCode = tLJTemp.getBankCode();
                        BankAccNo = tLJTemp.getBankAccNo();
                        AccName = tLJTemp.getAccName();
                    }
                }
            }
        }
        double sub = 0.0;
        for (int index = 1; index <= mLCPolSet.size(); index++) {
            mNewLCPolSchema = new LCPolSchema();
            mLCPolSchema = mLCPolSet.get(index);
            //1-查询投保单号对应的已签单的保单，如果没有则跳过,否则保存查询到的纪录
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(mLCPolSchema.getPolNo());
            tLCPolSet = tLCPolDB.query();
            if (tLCPolSet == null) {
                continue;
            }
            if (tLCPolSet.size() == 0) {
                continue;
            }
            mNewLCPolSchema = tLCPolSet.get(1);
            if (!"1".equals(mNewLCPolSchema.getAppFlag())) { //如果没有签单返回
                continue;
            }

            mSaveLCPolSet.add(mNewLCPolSchema); //保存查出的投保单，后面暂交退费用--只需要主险
            /*Lis5.3 upgrade set,get
                    if(mNewLCPolSchema.getOutPayFlag()==null)
                    {
                        mNewLCPolSchema.setOutPayFlag("1");
                    }
                    if(mNewLCPolSchema.getPayIntv()==0)//如果交费方式是趸交，退费
                    {
                        mNewLCPolSchema.setOutPayFlag("1");
                    }
             */
//        if(specWithDrawFlag)//如果特殊退费标记为真，那么默认退费
//        {
//            //mNewLCPolSchema.setOutPayFlag("1");
//            if(!(mNewLCPolSchema.getLeavingMoney()>0))
//                continue;
//        }
//        else
//        {
//            /*Lis5.3 upgrade set
//            if(!(mNewLCPolSchema.getLeavingMoney()>0)||!mNewLCPolSchema.getOutPayFlag().equals("1"))//如果没有余额或者溢交处理方式不是退费,返回
//                continue;
//              */
//             if(!(mNewLCPolSchema.getLeavingMoney()>0)) continue;
//        }
            if (mNewLCPolSchema.getLeavingMoney() < 0) {
                continue;
            }
            //3-准备打印数据,生成印刷流水号
            mLOPRTManagerSchema = new LOPRTManagerSchema();
            tLimit = PubFun.getNoLimit(mNewLCPolSchema.getManageCom());
            prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
            mLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_GET); //
            mLOPRTManagerSchema.setOtherNo(getNo); //实付号码
            mLOPRTManagerSchema.setMakeDate(CurrentDate);
            mLOPRTManagerSchema.setMakeTime(CurrentTime);
            mLOPRTManagerSchema.setManageCom(mNewLCPolSchema.getManageCom());
            mLOPRTManagerSchema.setAgentCode(mNewLCPolSchema.getAgentCode());
            mLOPRTManagerSchema.setCode(PrintManagerBL.CODE_REFUND);
            mLOPRTManagerSchema.setReqCom(mGI.ManageCom);
            mLOPRTManagerSchema.setReqOperator(mGI.Operator);
            mLOPRTManagerSchema.setPrtType("0");
            mLOPRTManagerSchema.setStateFlag("0");
            PrintManagerBL tPrintManagerBL = new PrintManagerBL();
            VData tempVData = new VData();
            tempVData.add(mLOPRTManagerSchema);
            tempVData.add(mGI);
            if (tPrintManagerBL.submitData(tempVData, "REQ")) { //打印数据处理
                tempVData = tPrintManagerBL.getResult();
                mLOPRTManagerSchema = (LOPRTManagerSchema) tempVData.
                                      getObjectByObjectName(
                                              "LOPRTManagerSchema", 0);
                mLOPRTManagerSet.add(mLOPRTManagerSchema);
            } else {
                continue;
            }
            //实付子表
            mLJAGetOtherShema = new LJAGetOtherSchema();
            mLJAGetOtherShema.setActuGetNo(getNo);
            mLJAGetOtherShema.setOtherNo(mNewLCPolSchema.getPolNo());
            mLJAGetOtherShema.setOtherNoType("6"); //其它类型退费(溢交保费退费)
            mLJAGetOtherShema.setPayMode(payMode);
            mLJAGetOtherShema.setGetMoney(mNewLCPolSchema.getLeavingMoney());
            mLJAGetOtherShema.setGetDate(CurrentDate);
            mLJAGetOtherShema.setFeeOperationType("YJTF"); //溢交退费
            mLJAGetOtherShema.setFeeFinaType("YJTF"); //溢交退费
            mLJAGetOtherShema.setManageCom(mNewLCPolSchema.getManageCom());
            mLJAGetOtherShema.setAgentCom(mNewLCPolSchema.getAgentCom());
            mLJAGetOtherShema.setAgentType(mNewLCPolSchema.getAgentType());
            mLJAGetOtherShema.setAPPntName(mNewLCPolSchema.getAppntName());
            mLJAGetOtherShema.setAgentGroup(mNewLCPolSchema.getAgentGroup());
            mLJAGetOtherShema.setAgentCode(mNewLCPolSchema.getAgentCode());
            mLJAGetOtherShema.setSerialNo(serNo);
            mLJAGetOtherShema.setOperator(mGI.Operator); ;
            mLJAGetOtherShema.setMakeTime(CurrentTime);
            mLJAGetOtherShema.setMakeDate(CurrentDate);
            mLJAGetOtherShema.setModifyDate(CurrentDate);
            mLJAGetOtherShema.setModifyTime(CurrentTime);
            mLJAGetOtherSet.add(mLJAGetOtherShema);

            //4-更新个人保单数据.
            System.out.println("mNewLCPolSchema.getLeavingMoney() " +
                               mNewLCPolSchema.getLeavingMoney());
            this.mNewLCContSchema.setDif(mNewLCContSchema.getDif() -
                                         mNewLCPolSchema.getLeavingMoney());
            sub += mNewLCPolSchema.getLeavingMoney();
            mNewLCPolSchema.setLeavingMoney(0);
            mNewLCPolSchema.setModifyDate(CurrentDate);
            mNewLCPolSchema.setModifyTime(CurrentTime);
            mNewLCPolSet.add(mNewLCPolSchema);

        }
        String tGetNoticeNo = PubFun1.CreateMaxNo("GETNOTICENO", tLimit); //产生即付通知书号
        //实付总表
        mLJAGetSchema = new LJAGetSchema();
        mLJAGetSchema.setActuGetNo(getNo);
        mLJAGetSchema.setPayMode(payMode);
        mLJAGetSchema.setShouldDate(CurrentDate);
        mLJAGetSchema.setStartGetDate(CurrentDate);
        mLJAGetSchema.setBankAccNo(BankAccNo);
        mLJAGetSchema.setBankCode(BankCode);
        mLJAGetSchema.setAccName(AccName);
        mLJAGetSchema.setOtherNo(mNewLCPolSchema.getContNo());
        mLJAGetSchema.setOtherNoType("6"); //其它类型退费(溢交保费退费)
        mLJAGetSchema.setGetNoticeNo(tGetNoticeNo);
        mLJAGetSchema.setAppntNo(mNewLCPolSchema.getAppntNo());
        mLJAGetSchema.setSumGetMoney(sub);
        mLJAGetSchema.setSaleChnl(mNewLCPolSchema.getSaleChnl());
        mLJAGetSchema.setSerialNo(serNo);
        mLJAGetSchema.setOperator(mGI.Operator);
        mLJAGetSchema.setManageCom(mNewLCPolSchema.getManageCom());
        mLJAGetSchema.setAgentCode(mNewLCPolSchema.getAgentCode());
        mLJAGetSchema.setAgentCom(mNewLCPolSchema.getAgentCom());
        mLJAGetSchema.setAgentGroup(mNewLCPolSchema.getAgentGroup());
        mLJAGetSchema.setShouldDate(CurrentDate);
        mLJAGetSchema.setStartGetDate(CurrentDate);
        mLJAGetSchema.setMakeDate(CurrentDate);
        mLJAGetSchema.setMakeTime(CurrentTime);
        mLJAGetSchema.setModifyDate(CurrentDate);
        mLJAGetSchema.setModifyTime(CurrentTime);
        mLJAGetSet.add(mLJAGetSchema);
        return true;
    }


    /**
     * 根据集合中主险投保单号查出同印刷号的主附险,使用同一个给付通知书号
     * @return
     */
    private boolean dealDataForMainRisk() {
        //产生流水号
        tLimit = PubFun.getNoLimit(mGI.ManageCom);
        serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);

        // LCPolSet tLCPolSet = new LCPolSet();
        for (int index = 1; index <= mLCPolSet.size(); index++) {
            //  mNewLCPolSchema=new LCPolSchema();
            mLCPolSchema = mLCPolSet.get(index);
            mNewLCPolSchema = mLCPolSchema;
            //1-查询投保单号对应的已签单的保单，如果没有则跳过,否则保存查询到的纪录
            //  LCPolDB tLCPolDB=new LCPolDB();
            //    tLCPolDB.setProposalNo(mLCPolSchema.getPolNo());
            // tLCPolSet=tLCPolDB.query();
            // if(tLCPolSet==null)     continue;
            //  if(tLCPolSet.size()==0) continue;
            //  mNewLCPolSchema = tLCPolSet.get(1);

//        if(mNewLCPolSchema.getAppFlag()==null||!mNewLCPolSchema.getAppFlag().equals("1")) //如果没有签单返回
//          continue;

            mSaveLCPolSet.add(mNewLCPolSchema); //保存查出的投保单，后面暂交退费用

//        tLCPolDB=new LCPolDB();
//        tLCPolDB.setPrtNo(mNewLCPolSchema.getPrtNo());
//        tLCPolSet=tLCPolDB.query();
            tLimit = PubFun.getNoLimit(mNewLCPolSchema.getManageCom()); //主附险共用一个给付通知书号
            String tGetNoticeNo = ""; //通知书号
            String payMode = ""; //交费方式
            String BankCode = ""; //银行编码
            String BankAccNo = ""; //银行账号
            String AccName = ""; //户名

            if (mNewLCContSchema != null) {
                payMode = mNewLCContSchema.getPayMode();
                BankCode = mNewLCContSchema.getBankCode();
                BankAccNo = mNewLCContSchema.getBankAccNo();
                AccName = mNewLCContSchema.getAccName();
            }

            for (int n = 1; n <= mLCPolSet.size(); n++) { //处理主附险
                mNewLCPolSchema = mLCPolSet.get(n);
                /*Lis5.3 upgrade set
                             if(mNewLCPolSchema.getOutPayFlag()==null)
                             {
                    mNewLCPolSchema.setOutPayFlag("1");
                             }
                             if(mNewLCPolSchema.getPayIntv()==0)//如果交费方式是趸交，退费
                             {
                    mNewLCPolSchema.setOutPayFlag("1");
                             }
                 */
//            if(specWithDrawFlag)//如果特殊退费标记为真，那么默认退费
//            {
//                //mNewLCPolSchema.setOutPayFlag("1");
//                if(mNewLCPolSchema.getLeavingMoney()<0)
//                    continue;
//            }
//            else
//            {
//              /*Lis5.3 upgrade set
//                if(!(mNewLCPolSchema.getLeavingMoney()>0)||!mNewLCPolSchema.getOutPayFlag().equals("1"))//如果没有余额或者溢交处理方式不是退费,返回
//                    continue;
//            */
//                  }
                if (mNewLCPolSchema.getLeavingMoney() < 0) {

                    //2-准备实付表纪录,产生实付号码
                    getNo = PubFun1.CreateMaxNo("GETNO", tLimit);
                }

                //如果通知书号已经产生，那么就不用产生打印数据了
                if (tGetNoticeNo == null || tGetNoticeNo.equals("")) {
                    tGetNoticeNo = PubFun1.CreateMaxNo("GETNOTICENO", tLimit); //产生即付通知书号

                    //3-准备打印数据,生成印刷流水号
                    mLOPRTManagerSchema = new LOPRTManagerSchema();
                    tLimit = PubFun.getNoLimit(mNewLCPolSchema.getManageCom());
                    prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
                    mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
                    mLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_GET); //
                    mLOPRTManagerSchema.setOtherNo(tGetNoticeNo); //给付通知书号码
                    mLOPRTManagerSchema.setMakeDate(CurrentDate);
                    mLOPRTManagerSchema.setMakeTime(CurrentTime);
                    mLOPRTManagerSchema.setManageCom(mNewLCPolSchema.
                            getManageCom());
                    mLOPRTManagerSchema.setAgentCode(mNewLCPolSchema.
                            getAgentCode());
                    mLOPRTManagerSchema.setCode(PrintManagerBL.CODE_REFUND);
                    mLOPRTManagerSchema.setReqCom(mGI.ManageCom);
                    mLOPRTManagerSchema.setReqOperator(mGI.Operator);
                    mLOPRTManagerSchema.setPrtType("0");
                    mLOPRTManagerSchema.setStateFlag("0");
                    mLOPRTManagerSchema.setStandbyFlag1(mNewLCPolSchema.
                            getMainPolNo());
                    PrintManagerBL tPrintManagerBL = new PrintManagerBL();
                    VData tempVData = new VData();
                    tempVData.add(mLOPRTManagerSchema);
                    tempVData.add(mGI);
                    if (tPrintManagerBL.submitData(tempVData, "REQ")) { //打印数据处理
                        tempVData = tPrintManagerBL.getResult();
                        mLOPRTManagerSchema = (LOPRTManagerSchema) tempVData.
                                              getObjectByObjectName(
                                "LOPRTManagerSchema", 0);
                        mLOPRTManagerSet.add(mLOPRTManagerSchema);
                    } else {
                        continue;
                    }

                    //如果通知书号不为空，找出退费方式（优先级依次为支票，银行，现金）
                    GetPayType tGetPayType = new GetPayType();
                    if (tGetPayType.getPayTypeForLCPol(mLCPolSchema.getPrtNo()) == false) {
                        this.mErrors.copyAllErrors(tGetPayType.mErrors);
                        return false;
                    } else {
                        payMode = tGetPayType.getPayMode(); //交费方式
                        BankCode = tGetPayType.getBankCode(); //银行编码
                        BankAccNo = tGetPayType.getBankAccNo(); //银行账号
                        AccName = tGetPayType.getAccName(); //户名
                    }

                }

                //实付总表
                mLJAGetSchema = new LJAGetSchema();
                mLJAGetSchema.setActuGetNo(getNo);
                mLJAGetSchema.setPayMode(payMode);
                mLJAGetSchema.setShouldDate(CurrentDate);
                mLJAGetSchema.setStartGetDate(CurrentDate);

                mLJAGetSchema.setBankAccNo(BankAccNo);
                mLJAGetSchema.setBankCode(BankCode);
                mLJAGetSchema.setAccName(AccName);
                mLJAGetSchema.setOtherNo(mNewLCPolSchema.getPolNo());
                mLJAGetSchema.setOtherNoType("6"); //其它类型退费(溢交保费退费)
                mLJAGetSchema.setGetNoticeNo(tGetNoticeNo);
                mLJAGetSchema.setAppntNo(mNewLCPolSchema.getAppntNo());
                mLJAGetSchema.setSumGetMoney(mNewLCPolSchema.getLeavingMoney());
                mLJAGetSchema.setSaleChnl(mNewLCPolSchema.getSaleChnl());
                mLJAGetSchema.setSerialNo(serNo);
                mLJAGetSchema.setOperator(mGI.Operator);
                mLJAGetSchema.setManageCom(mNewLCPolSchema.getManageCom());
                mLJAGetSchema.setAgentCode(mNewLCPolSchema.getAgentCode());
                mLJAGetSchema.setAgentCom(mNewLCPolSchema.getAgentCom());
                mLJAGetSchema.setAgentGroup(mNewLCPolSchema.getAgentGroup());
                mLJAGetSchema.setShouldDate(CurrentDate);
                mLJAGetSchema.setStartGetDate(CurrentDate);
                mLJAGetSchema.setMakeDate(CurrentDate);
                mLJAGetSchema.setMakeTime(CurrentTime);
                mLJAGetSchema.setModifyDate(CurrentDate);
                mLJAGetSchema.setModifyTime(CurrentTime);
                mLJAGetSet.add(mLJAGetSchema);
                //实付子表
                mLJAGetOtherShema = new LJAGetOtherSchema();
                mLJAGetOtherShema.setActuGetNo(getNo);
                mLJAGetOtherShema.setOtherNo(mNewLCPolSchema.getPolNo());
                mLJAGetOtherShema.setOtherNoType("6"); //其它类型退费(溢交保费退费)
                mLJAGetOtherShema.setPayMode(payMode);
                mLJAGetOtherShema.setGetMoney(mNewLCPolSchema.getLeavingMoney());
                mLJAGetOtherShema.setGetDate(CurrentDate);
                mLJAGetOtherShema.setFeeOperationType("YJTF"); //溢交退费
                mLJAGetOtherShema.setFeeFinaType("YJTF"); //溢交退费
                mLJAGetOtherShema.setManageCom(mNewLCPolSchema.getManageCom());
                mLJAGetOtherShema.setAgentCom(mNewLCPolSchema.getAgentCom());
                mLJAGetOtherShema.setAgentType(mNewLCPolSchema.getAgentType());
                mLJAGetOtherShema.setAPPntName(mNewLCPolSchema.getAppntName());
                mLJAGetOtherShema.setAgentGroup(mNewLCPolSchema.getAgentGroup());
                mLJAGetOtherShema.setAgentCode(mNewLCPolSchema.getAgentCode());
                mLJAGetOtherShema.setSerialNo(serNo);
                mLJAGetOtherShema.setOperator(mGI.Operator); ;
                mLJAGetOtherShema.setMakeTime(CurrentTime);
                mLJAGetOtherShema.setMakeDate(CurrentDate);
                mLJAGetOtherShema.setModifyDate(CurrentDate);
                mLJAGetOtherShema.setModifyTime(CurrentTime);
                mLJAGetOtherSet.add(mLJAGetOtherShema);

                //4-更新个人保单数据
                mNewLCPolSchema.setLeavingMoney(0);
                mNewLCPolSchema.setModifyDate(CurrentDate);
                mNewLCPolSchema.setModifyTime(CurrentTime);
                mNewLCPolSet.add(mNewLCPolSchema);
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData tVData) {
        mLCPolSet = (LCPolSet) tVData.getObjectByObjectName("LCPolSet", 0);
        mGI = (GlobalInput) tVData.getObjectByObjectName("GlobalInput", 0);
        tTransferData = (TransferData) tVData.getObjectByObjectName(
                "TransferData", 0);
        mNewLCContSchema = (LCContSchema) tVData.getObjectByObjectName(
                "LCContSchema", 0);
        if (mLCPolSet == null || mLCPolSet.size() <= 0 || mGI == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "NewPolFeeWithdrawBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tTransferData != null) {
            String inStr = (String) tTransferData.getValueByName("SpecWithDraw");
            if (inStr != null) {
                if (inStr.equals("1")) {
                    specWithDrawFlag = true;
                }
            }
        }
        if (mNewLCContSchema == null) {
            LCContDB tContDB = new LCContDB();
            tContDB.setContNo(mLCPolSet.get(1).getContNo());
            if (tContDB.getInfo()) {
                mNewLCContSchema = tContDB.getSchema();
            }
        }

        return true;
    }

//准备往后层输出所需要的数据
//输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData() {
        mInputData = new VData();
        try {
            mInputData.add(this.mNewLCContSchema);
            mInputData.add(mNewLCPolSet);
            mInputData.add(mLJAGetSet);
            mInputData.add(mLOPRTManagerSet);
            mInputData.add(mLJAGetOtherSet);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "NewPolFeeWithdrawBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 准备暂交费退费的数据(针对主险已签单但是附加险撤单的情况)
     * @return
     */
    private VData prepareTempfeeData() {
//      LJAGetTempFeeSet preLJAGetTempFeeSet = new LJAGetTempFeeSet();
//      LJTempFeeSet preLJTempFeeSet = new LJTempFeeSet();
//      LJAGetSet preLJAGetSet= new LJAGetSet();
//      LJTempFeeClassSet preLJTempFeeClassSet =new LJTempFeeClassSet();
        MMap returnMap = new MMap();
        VData ruturnData = new VData();
        String strSql = "";
        LCPolSchema tLCPolSchema = new LCPolSchema();
        for (int index = 1; index <= mSaveLCPolSet.size(); index++) {
            String payMode = ""; //交费方式
            String BankCode = ""; //银行编码
            String BankAccNo = ""; //银行账号
            String AccName = ""; //户名
            tLCPolSchema = mSaveLCPolSet.get(index);
            if (!tLCPolSchema.getPolNo().equals(tLCPolSchema.getMainPolNo())) {
                continue; //如果不是主险，跳到下一循环
            }

            LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
            LJTempFeeSet outLJTempFeeSet = new LJTempFeeSet();
            LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
            //查出主险对应的暂加费纪录（可能多条：譬如金额不足，交两次费）
            strSql = "select * from ljtempfee where EnterAccDate is not null  ";
            strSql = strSql + " and otherno in('" + tLCPolSchema.getPrtNo() +
                     "','" + tLCPolSchema.getPolNo() + "')";
            strSql = strSql + " and riskcode='" + tLCPolSchema.getRiskCode() +
                     "'";
            tLJTempFeeSet = tLJTempFeeDB.executeQuery(strSql);
            if (tLJTempFeeSet == null || tLJTempFeeSet.size() == 0) {
                continue; //如果没有暂交费，跳到下一循环
            }
            for (int n = 1; n <= tLJTempFeeSet.size(); n++) {
                //找出在暂交费中对应附加险投保单撤单的纪录(已经到帐,如果是扣款送途中，不容许撤单)
                strSql =
                        "select * from ljtempfee where EnterAccDate is not null and confflag='0' ";
                strSql = strSql +
                         " and riskcode not in( select riskcode from lcpol where prtno='" +
                         tLCPolSchema.getPrtNo() + "')";
                strSql = strSql + " and tempfeeno='" +
                         tLJTempFeeSet.get(n).getTempFeeNo() + "'";
                LJTempFeeSet tempLJTempFeeSet = tLJTempFeeDB.executeQuery(
                        strSql);
                if (tempLJTempFeeSet == null || tempLJTempFeeSet.size() == 0) {
                    continue; //如果没有暂交费，跳到下一循环
                }
                outLJTempFeeSet.add(tempLJTempFeeSet);
            }
            if (outLJTempFeeSet.size() == 0) { //如果没有找到这样的纪录
                continue;
            }

            //如果通知书号不为空，找出退费方式（优先级依次为支票，银行，现金）
            GetPayType tGetPayType = new GetPayType();
            if (tGetPayType.getPayTypeForLCPol(tLCPolSchema.getPrtNo()) == false) {
                this.mErrors.copyAllErrors(tGetPayType.mErrors);
                return ruturnData;
            } else {
                payMode = tGetPayType.getPayMode(); //交费方式
                BankCode = tGetPayType.getBankCode(); //银行编码
                BankAccNo = tGetPayType.getBankAccNo(); //银行账号
                AccName = tGetPayType.getAccName(); //户名
            }

            TransferData sTansferData = new TransferData();
            sTansferData.setNameAndValue("PayMode", payMode);
            if (payMode.equals("1")) {
                sTansferData.setNameAndValue("BankFlag", "0");
            } else {
                sTansferData.setNameAndValue("BankCode", BankCode);
                sTansferData.setNameAndValue("AccNo", BankAccNo);
                sTansferData.setNameAndValue("AccName", AccName);
                sTansferData.setNameAndValue("BankFlag", "1");
            }

            String tGetNoticeNo = "";
            //从溢交保费产生的实付表中查找是否有相同的投保单号，共用一个给付通知书号
            for (int i = 1; i <= mLJAGetSet.size(); i++) {
                if (mLJAGetSet.get(i).getOtherNo().equals(tLCPolSchema.getPolNo())) {
                    tGetNoticeNo = mLJAGetSet.get(i).getGetNoticeNo();
                    break;
                }
            }
            //和溢交保费相同的给付通知书号，如果没有溢交保费，即该号是空的,需要判断
            if (tGetNoticeNo == null || tGetNoticeNo.equals("")) {
                tLimit = PubFun.getNoLimit(tLCPolSchema.getManageCom());
                tGetNoticeNo = PubFun1.CreateMaxNo("GETNOTICENO", tLimit); //产生即付通知书号

                //3-准备打印数据,生成印刷流水号
                mLOPRTManagerSchema = new LOPRTManagerSchema();
                tLimit = PubFun.getNoLimit(tLCPolSchema.getManageCom());
                prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
                mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
                mLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_GET); //
                mLOPRTManagerSchema.setOtherNo(tGetNoticeNo); //给付通知书号码
                mLOPRTManagerSchema.setMakeDate(CurrentDate);
                mLOPRTManagerSchema.setMakeTime(CurrentTime);
                mLOPRTManagerSchema.setManageCom(tLCPolSchema.getManageCom());
                mLOPRTManagerSchema.setAgentCode(tLCPolSchema.getAgentCode());
                mLOPRTManagerSchema.setCode(PrintManagerBL.CODE_REFUND);
                mLOPRTManagerSchema.setReqCom(mGI.ManageCom);
                mLOPRTManagerSchema.setReqOperator(mGI.Operator);
                mLOPRTManagerSchema.setPrtType("0");
                mLOPRTManagerSchema.setStateFlag("0");
                mLOPRTManagerSchema.setStandbyFlag1(tLCPolSchema.getMainPolNo());
                PrintManagerBL tPrintManagerBL = new PrintManagerBL();
                VData tempVData = new VData();
                tempVData.add(mLOPRTManagerSchema);
                tempVData.add(mGI);
                if (tPrintManagerBL.submitData(tempVData, "REQ")) { //打印数据处理
                    tempVData = tPrintManagerBL.getResult();
                    mLOPRTManagerSchema = (LOPRTManagerSchema) tempVData.
                                          getObjectByObjectName(
                                                  "LOPRTManagerSchema", 0);
                    mLOPRTManagerSet.add(mLOPRTManagerSchema);
                } else {
                    continue;
                }

            }
            sTansferData.setNameAndValue("GetNoticeNo", tGetNoticeNo);
            //生成暂交费退费实付表
            LJAGetTempFeeSet tLJAGetTempFeeSet = new LJAGetTempFeeSet();
            for (int n = 1; n <= outLJTempFeeSet.size(); n++) {
                LJAGetTempFeeSchema tLJAGetTempFeeSchema = new
                        LJAGetTempFeeSchema();
                tLJAGetTempFeeSchema.setGetReasonCode("99");
                tLJAGetTempFeeSet.add(tLJAGetTempFeeSchema);
            }

            sTansferData.setNameAndValue("NotBLS", "1"); //暂交费退费程序需要

            VData tVData = new VData();
            tVData.add(outLJTempFeeSet);
            tVData.add(tLJAGetTempFeeSet);
            tVData.add(sTansferData);
            tVData.add(mGI);
            //调用暂交费退费的接口
            TempFeeWithdrawBL tTempFeeWithdrawBL = new TempFeeWithdrawBL();
            tTempFeeWithdrawBL.submitData(tVData, "INSERT");
            tVData = tTempFeeWithdrawBL.getResult();
            MMap tmpMap = (MMap) tVData.getObjectByObjectName("MMap", 0);
            if (tmpMap != null) {
                returnMap.add(tmpMap);
            }

//          if(tVData!=null&&tVData.size()>0)
//          {
//              preLJAGetTempFeeSet.add((LJAGetTempFeeSet)tVData.getObjectByObjectName("LJAGetTempFeeSet",0));
//              preLJTempFeeSet.add((LJTempFeeSet)tVData.getObjectByObjectName("LJTempFeeSet",0));
//              preLJAGetSet.add((LJAGetSet)tVData.getObjectByObjectName("LJAGetSet",0));
//              preLJTempFeeClassSet.add((LJTempFeeClassSet)tVData.getObjectByObjectName("LJTempFeeClassSet",0));
//          }
        }
//      ruturnData.add(preLJAGetTempFeeSet);
//      ruturnData.add(preLJTempFeeSet);
//      ruturnData.add(preLJAGetSet);
//      ruturnData.add(preLJTempFeeClassSet);
        ruturnData.add(returnMap);
        return ruturnData;
    }

}
