package com.sinosoft.lis.finfee;

import com.sinosoft.lis.db.LCSerialnoHZDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCPaySerialnoHZSchema;
import com.sinosoft.lis.schema.LCSerialnoHZSchema;
import com.sinosoft.lis.vschema.LCPaySerialnoHZSet;
import com.sinosoft.lis.vschema.LCSerialnoHZSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class PayUnionSerialnoTJBL
{

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	// 所有的实付数据
	private LCPaySerialnoHZSet mLCPaySerialnoHZSet = new LCPaySerialnoHZSet();
	/** 往界面传输数据的容器 */
	protected VData mVData = new VData();
	private String mOperator;

	public PayUnionSerialnoTJBL()
	{
	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate)
	{

		System.out.println("---PayUnionSerialnoTJBL BEGIN---");
		mOperator = cOperate;
		if (!getInputData(cInputData))
		{
			System.out.println("getinputdata failed");
			return false;
		}

		if (!dealData())
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "PayUnionSerialnoTJBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败PayUnionSerialnoTJBL-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean dealData()
	{
		MMap map = new MMap();
		double sumMoney =0.00;
		LCSerialnoHZSchema mLCSerialnoHZSchema = new LCSerialnoHZSchema();
		String serialno="";
		for (int i = 0; i < mLCPaySerialnoHZSet.size(); i++)
		{
			LCPaySerialnoHZSchema mLCPaySerialnoHZSchema = new LCPaySerialnoHZSchema();
			System.out.println(mLCPaySerialnoHZSet.size()+"选中数据的长度");
			mLCPaySerialnoHZSchema = mLCPaySerialnoHZSet.get(i + 1);
			sumMoney = sumMoney + mLCPaySerialnoHZSchema.getLocalTotalAmt();
			serialno=mLCPaySerialnoHZSchema.getTranBatch();
			System.out.println(serialno+"汇总批次号");
			System.out.println("业务号" + mLCPaySerialnoHZSchema.getTranSerial());
			map.put(mLCPaySerialnoHZSchema, "INSERT");
			
		}
		String Sql = "select * from LCSerialnoHZ where TranBatch='" +serialno+"'";
		LCSerialnoHZDB  mLCSerialnoHZDB = new LCSerialnoHZDB();
		LCSerialnoHZSet mLCSerialnoHZSet = mLCSerialnoHZDB.executeQuery(Sql);
		mLCSerialnoHZSchema = mLCSerialnoHZSet.get(1);
		mLCSerialnoHZSchema.setTranBatch(serialno);
		mLCSerialnoHZSchema.setSerialnoHZMoney(mLCSerialnoHZSchema.getSerialnoHZMoney() + sumMoney);
		mLCSerialnoHZSchema.setSerialnoHZNum(mLCSerialnoHZSchema.getSerialnoHZNum()+mLCPaySerialnoHZSet.size());
		mLCSerialnoHZSchema.setModifyDate(PubFun.getCurrentDate());
		mLCSerialnoHZSchema.setModifyTime(PubFun.getCurrentTime());
		map.put(mLCSerialnoHZSchema, "UPDATE");
		mVData.add(map);
		PubSubmit ps = new PubSubmit();
		if (!ps.submitData(mVData, ""))
		{
			this.mErrors.copyAllErrors(ps.mErrors);
			return false;
		}
		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData)
	{
		TransferData tTransferData = (TransferData) cInputData
				.getObjectByObjectName("TransferData", 0);
		mLCPaySerialnoHZSet = (LCPaySerialnoHZSet) tTransferData
				.getValueByName("mLCPaySerialnoHZSet");
		System.out.println("最后的size" + mLCPaySerialnoHZSet.size());
		return true;
	}

}
