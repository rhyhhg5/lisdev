package com.sinosoft.lis.finfee;

import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.pubfun.*;

import java.util.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class LJtempFeeUpdateBL  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData =new VData();
  /** 数据操作字符串 */
  private String mOperate;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private TransferData mTransferData;
  private String mmTempfeeNo;
  private String mTempfeeNo;
  private String mPayMode;
  private String mBankCode;
  private String mAccNo;
  private String mAgentCode;
  private Reflections mReflections=new Reflections();
  
  /** 提交数据的容器 */
  private MMap map = new MMap();

  //标志位
  String flag="";
  //Session信息
  private GlobalInput mG = new GlobalInput();

  //业务处理相关变量

  public LJtempFeeUpdateBL() {
  }
  public static void main(String[] args) {
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    System.out.println("OperateData:  "+cOperate);


    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
    System.out.println("After getinputdata");

        
    //进行业务处理
    if (!dealData())
      return false;
    System.out.println("After dealData！");
    
    
//  准备往后台的数据
    if (!prepareOutputData()) return false;
    System.out.println("---End prepareOutputData---");

    System.out.println("Start PubSubmit BLS Submit...");
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      return false;
    }
    System.out.println("End PubSubmit BLS Submit...");    

    mInputData=null;
    return true;
  }

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData()
  {
   
        if(!StrTool.cTrim(this.mmTempfeeNo).equals("")){
            //修改数据
            UpdateTempfeeData();
          
        }
   
    return true ;
 }
  //从输入数据中得到所有对象
  //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData tInputData)
  {
      mG = (GlobalInput)tInputData.getObjectByObjectName("GlobalInput",0);
//    全局变量
      mTransferData = (TransferData) tInputData.getObjectByObjectName("TransferData",0);
      mmTempfeeNo = (String) mTransferData.getValueByName("OtherNo");
      mTempfeeNo = (String) mTransferData.getValueByName("TempfeeNo");
      mPayMode = (String) mTransferData.getValueByName("PayMode");
      mBankCode= (String) mTransferData.getValueByName("BankCode");
      mAccNo = (String) mTransferData.getValueByName("AccNo");
      mAgentCode = (String) mTransferData.getValueByName("AgentCode");
     
      
        
      return true;
  }

  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData()
  {
      try {
          mInputData.clear();
          mInputData.add(map);
      }catch(Exception ex) {
          // @@错误处理
          System.out.println(ex.getMessage());
          CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
          return false;
      }
    return true;
  }
//修改暂收数据
  private boolean UpdateTempfeeData(){
      LJTempFeeDB tLJTempFeeDB=new LJTempFeeDB();
      tLJTempFeeDB.setTempFeeNo(this.mmTempfeeNo);
      LJTempFeeSet tLJTempFeeSet=tLJTempFeeDB.query();
      for(int i=1;i<=tLJTempFeeSet.size();i++){
          LJTempFeeSchema tLJTempFeeSchema=new LJTempFeeSchema();
          LJTempFeeSchema tLJTempFeeSchema1=new LJTempFeeSchema();
          tLJTempFeeSchema.setSchema(tLJTempFeeSet.get(i));
          tLJTempFeeSchema1.setSchema(tLJTempFeeSet.get(i));
          tLJTempFeeSchema.setTempFeeNo(this.mTempfeeNo);
          tLJTempFeeSchema.setModifyDate(this.CurrentDate);
          tLJTempFeeSchema.setModifyTime(this.CurrentTime);
          tLJTempFeeSchema.setOperator(this.mG.Operator);
          tLJTempFeeSchema.setAgentCode(this.mAgentCode); 
          this.map.put(tLJTempFeeSchema1,"DELETE");
          this.map.put(tLJTempFeeSchema, "INSERT");
      }
      LJTempFeeClassDB tLJTempFeeClassDB=new LJTempFeeClassDB();
      tLJTempFeeClassDB.setTempFeeNo(this.mmTempfeeNo);
      LJTempFeeClassSet tLJTempFeeClassSet=tLJTempFeeClassDB.query();
      for(int i=1;i<=tLJTempFeeClassSet.size();i++){
          LJTempFeeClassSchema tLJTempFeeClassSchema=new LJTempFeeClassSchema();
          LJTempFeeClassSchema tLJTempFeeClassSchema1=new LJTempFeeClassSchema();
          tLJTempFeeClassSchema.setSchema(tLJTempFeeClassSet.get(i));
          tLJTempFeeClassSchema1.setSchema(tLJTempFeeClassSet.get(i));
          tLJTempFeeClassSchema.setTempFeeNo(mTempfeeNo);
          tLJTempFeeClassSchema.setBankCode(this.mBankCode);
          tLJTempFeeClassSchema.setBankAccNo(this.mAccNo);
          tLJTempFeeClassSchema.setModifyDate(this.CurrentDate);
          tLJTempFeeClassSchema.setModifyTime(this.CurrentTime);
          tLJTempFeeClassSchema.setOperator(this.mG.Operator);
          this.map.put(tLJTempFeeClassSchema1, "DELETE");
          this.map.put(tLJTempFeeClassSchema, "INSERT");
      }
      return true;
  }
 
 

}