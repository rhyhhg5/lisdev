/**
 * 2007-5-22
 */
package com.sinosoft.lis.finfee;

import java.util.List;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class FFIvoiceExportHnUI
{
    public CErrors mErrors = new CErrors();

    private List mResult;

    public FFIvoiceExportHnUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("EXPORT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            FFIvoiceExportHnBL tFFIvoiceExportHnBL = new FFIvoiceExportHnBL();
            System.out.println("Start FFIvoiceExportHnBL Submit ...");
            if (!tFFIvoiceExportHnBL.submitData(cInputData, cOperate))
            {
                if (tFFIvoiceExportHnBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tFFIvoiceExportHnBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData",
                            "FFIvoiceExportHnBL 发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tFFIvoiceExportHnBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("submitData", "意外错误");
            return false;
        }
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FFIvoiceExportHnUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public List getResult()
    {
        return mResult;
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "group";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("invoiceStartDate", "2007-5-1");
        tTransferData.setNameAndValue("invoiceEndDate", "2007-5-31");

        tTransferData.setNameAndValue("hostBasePath",
                "D:/hmyc/projects/PICCH/ui");
        tTransferData.setNameAndValue("prtTemplate",
                "prtXMLTemplate/liaoning_prtxml.properties");
        tTransferData.setNameAndValue("outPath", "printdata");

        VData tVDate = new VData();
        tVDate.add(tGlobalInput);
        tVDate.add(tTransferData);

        try
        {
            FFIvoiceExportHnBL tFFIvoiceExportHnBL = new FFIvoiceExportHnBL();
            if (!tFFIvoiceExportHnBL.submitData(tVDate, "EXPORT"))
            {
                System.out.println("failed...");
            }
            else
            {
                System.out.println("finished...");
            }
        }
        catch (Exception ex)
        {
            System.out.println("failed...");
        }
    }
}
