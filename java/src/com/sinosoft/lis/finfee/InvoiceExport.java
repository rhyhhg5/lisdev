package com.sinosoft.lis.finfee;

import java.util.List;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public interface InvoiceExport
{
    public boolean submitData(VData cInputData, String cOperate);

    public List getResult();

    public CErrors getErrors();
}
