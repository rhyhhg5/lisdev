/**
 * Create on 2007-5-12
 */
package com.sinosoft.lis.finfee.invoiceprtxml;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author ly
 *
 */
public class FileHelper
{
    FileReader mFile = null;

    BufferedReader mBufRead = null;

    public FileHelper(String tStrFilePath) throws FileNotFoundException
    {
        mFile = new FileReader(tStrFilePath);
        mBufRead = new BufferedReader(mFile);
    }

    public String readLine() throws IOException
    {
        return mBufRead.readLine();
    }

    public void closed() throws IOException
    {
        if (mBufRead != null)
            mBufRead.close();
        if (mFile != null)
            mFile.close();
    }
}