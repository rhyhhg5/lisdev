/**
 * Create on 2007-5-12
 */
package com.sinosoft.lis.finfee.invoiceprtxml;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author ly
 *
 */
public class PrtTemplate
{
    private static String BASE_TEMPLATE_FILE = "template/prtxml.properties";

    private HashMap mNodes = new HashMap();

    private ArrayList mKey = new ArrayList();

    public final String NODEPATH_REGEX = "^\\w+(/\\w+)*$";

    public final String COMMENT_REGEX = "^#+";

    public PrtTemplate() throws InvoicePrtXmlException
    {
        loadTemplate(this.BASE_TEMPLATE_FILE);
    }

    public PrtTemplate(String tFilePath) throws InvoicePrtXmlException
    {
        loadTemplate(tFilePath);
    }

    /**
     * 加载模版
     * @param tStrFilePath 模版文件路径
     * @throws InvoicePrtXmlException
     */
    private void loadTemplate(String tStrFilePath)
            throws InvoicePrtXmlException
    {
        FileHelper file = null;
        try
        {
            file = new FileHelper(tStrFilePath);

            String str = null;
            while ((str = file.readLine()) != null)
            {
                if (checkedString(str, COMMENT_REGEX))
                {
                    System.out.println("comment: " + str);
                    continue;
                }
                if (checkedString(str, NODEPATH_REGEX))
                {
                    String nodeName = str.substring(
                            str.lastIndexOf("/") == -1 ? 0 : str
                                    .lastIndexOf("/") + 1, str.length());
                    mNodes.put(nodeName, str);
                    mKey.add(nodeName);
                }
            }
        }
        catch (FileNotFoundException e)
        {
            throw new InvoicePrtXmlException("文件未找到", e);
        }
        catch (IOException e)
        {
            throw new InvoicePrtXmlException("文件读取错误", e);
        }
        finally
        {
            if (file != null)
            {
                try
                {
                    file.closed();
                }
                catch (IOException e)
                {
                    throw new InvoicePrtXmlException("文件关闭失败", e);
                }
            }
        }

    }

    public boolean isIntegrityOfPath(String tKey)
    {
        if (tKey == null || tKey.equals(""))
            return false;
        String[] nodeName = getValue(tKey).split("/");
        if (nodeName == null)
            return false;
        for (int i = 0; i < nodeName.length; i++)
        {
            if (!find(nodeName[i]))
                return false;
        }
        return true;
    }

    public boolean find(String tKey)
    {
        return mNodes.get(tKey) != null;
    }

    public String getValue(String tKey)
    {
        return (String) mNodes.get(tKey);
    }

    public String getValue(int tKeyIndex)
    {
        return (String) getValue((String) mKey.get(tKeyIndex));
    }

    public String getValue(String tParentKey, String tSubKey)
    {
        String xPath = getValue(tSubKey);
        if (xPath == null)
            return null;
        int firstParentKey = xPath.lastIndexOf("/" + tParentKey + "/");
        if (firstParentKey == -1)
            return null;
        xPath = xPath.substring(firstParentKey + tParentKey.length() + 2, xPath
                .length());
        return xPath;
    }

    public List getKeyAll()
    {
        return mKey;
    }

    public int count()
    {
        return mKey.size();
    }

    public boolean checkedString(String tValue, String tRegex)
    {
        Pattern pattern = Pattern.compile(tRegex);
        Matcher matcher = pattern.matcher(tValue);
        return matcher.find();
    }
}
