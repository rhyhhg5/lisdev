/**
 * Create on 2007-5-12
 */
package com.sinosoft.lis.finfee.invoiceprtxml;

/**
 * @author ly
 *
 */
public class InvoicePrtXmlException extends Exception
{
    private static final long serialVersionUID = 4341272779131301293L;

    public InvoicePrtXmlException(String message)
    {
        super(message);
    }

    public InvoicePrtXmlException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public InvoicePrtXmlException(Throwable cause)
    {
        super(cause);
    }
}
