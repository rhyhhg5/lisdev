/**
 * Create on 2007-5-12
 */
package com.sinosoft.lis.finfee.invoiceprtxml;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

/**
 * @author ly
 *
 */
public class XmlHelper
{
    private static String BASE_OUTPUT_FILE = "output/prtXml.xml";

    private Document mDoc_Backup = null;

    private Document mDoc = null;

    private PrtTemplate mPt = null;

    private String mBasePath = "";

    public XmlHelper() throws InvoicePrtXmlException
    {
        mDoc = DocumentHelper.createDocument();
        createXMLDocTemplate();
        creatBackDoc(mDoc);
    }

    public XmlHelper(String tDocTemplatePathAndFileName)
            throws InvoicePrtXmlException
    {
        mDoc = DocumentHelper.createDocument();
        createXMLDocTemplate(tDocTemplatePathAndFileName);
        creatBackDoc(mDoc);
    }

    public XmlHelper(String tDocTemplatePath, String tBasePath)
            throws InvoicePrtXmlException
    {
        mBasePath = tBasePath.replaceAll("\\\\", "/") + "/";
        mDoc = DocumentHelper.createDocument();
        createXMLDocTemplate(mBasePath + tDocTemplatePath);
        creatBackDoc(mDoc);
    }

    private void creatBackDoc(Document tDoc) throws InvoicePrtXmlException
    {
        if (tDoc == null)
            throw new InvoicePrtXmlException("模版加载失败");
        try
        {
            this.mDoc_Backup = (Document) tDoc.clone();
        }
        catch (Exception e)
        {
            throw new InvoicePrtXmlException("模版备份失败", e);
        }
    }

    /**
     * 由模版建立一个XML文档
     * @param tDocTemplatePath 模版所在路径，如果参数值为null，则采用默认路径。
     * @throws InvoicePrtXmlException
     */
    private void createXMLDocTemplate(String tDocTemplatePath)
            throws InvoicePrtXmlException
    {
        try
        {
            if (tDocTemplatePath == null)
                mPt = new PrtTemplate();
            else
                mPt = new PrtTemplate(tDocTemplatePath);
        }
        catch (InvoicePrtXmlException e)
        {
            throw new InvoicePrtXmlException("模版加载失败", e);
        }

        List list = mPt.getKeyAll();
        if (list != null && list.size() > 0)
        {
            for (int i = 0; i < list.size(); i++)
            {
                addElement((String) list.get(i));
            }
        }
    }

    /**
     * 根据默认路径，由模版建立一个XML文档
     * @throws InvoicePrtXmlException
     */
    private void createXMLDocTemplate() throws InvoicePrtXmlException
    {
        createXMLDocTemplate(null);
    }

    public void write() throws InvoicePrtXmlException
    {
        write(this.BASE_OUTPUT_FILE);
    }

    public void write(String tFileName, String encoding)
            throws InvoicePrtXmlException
    {
        XMLWriter output = null;

        OutputFormat format = OutputFormat.createPrettyPrint();
        format.setEncoding(encoding);
        try
        {
            output = new XMLWriter(new FileWriter(new File(mBasePath
                    + tFileName)), format);
            output.write(mDoc);
        }
        catch (IOException e)
        {
            throw new InvoicePrtXmlException("文件输出失败", e);
        }
        finally
        {
            try
            {
                if (output != null)
                    output.close();
            }
            catch (IOException e)
            {
                throw new InvoicePrtXmlException("文件关闭失败", e);
            }
        }
    }

    public void write(String tFileName) throws InvoicePrtXmlException
    {
        write(tFileName, "GBK");
    }

    private boolean addElement(String tNode)
    {
        if (mDoc == null)
            return false;
        String xPath = mPt.getValue(tNode);
        if (xPath.indexOf("/") == -1)
        {
            mDoc.addElement(tNode);
        }
        else if (mPt.isIntegrityOfPath(tNode))
        {
            String subXPath = xPath.substring(0, xPath.lastIndexOf("/"));
            List list = mDoc.selectNodes(subXPath);
            if (list != null && list.size() > 0)
            {
                Element node = (Element) list.get(0);
                node.addElement(tNode);
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
        return true;
    }

    public void setNodeText(String tNode, String tValue, boolean isOnlyFirst)
            throws InvoicePrtXmlException
    {
        if (!mPt.find(tNode))
            throw new InvoicePrtXmlException(tNode + "结点不存在");
        String xPath = mPt.getValue(tNode);
        List list = mDoc.selectNodes(xPath);
        if (list != null && list.size() > 0)
        {
            for (int i = 0; i < list.size(); i++)
            {
                Element node = (Element) list.get(i);
                node.setText(tValue);
                if (isOnlyFirst)
                    break;
            }
        }
    }

    public void setNodeText(String tNode, String tValue)
            throws InvoicePrtXmlException
    {
        setNodeText(tNode, tValue, false);
    }

    public void removeSubNode(String tNode, boolean isOnlyFirst)
            throws InvoicePrtXmlException
    {
        if (!mPt.find(tNode))
            throw new InvoicePrtXmlException(tNode + "结点不存在");
        String xPath = mPt.getValue(tNode);
        List list = mDoc.selectNodes(xPath);
        if (list != null && list.size() > 0)
        {
            for (int i = 0; i < list.size(); i++)
            {
                Element node = (Element) list.get(i);
                Iterator subIter = node.elementIterator();
                while (subIter.hasNext())
                {
                    Element subNode = (Element) subIter.next();
                    node.remove(subNode);
                }
                if (isOnlyFirst)
                    break;
            }
        }
    }

    public void removeNode(String tNode, boolean isOnlyFirst)
            throws InvoicePrtXmlException
    {
        if (!mPt.find(tNode))
            throw new InvoicePrtXmlException(tNode + "结点不存在");
        String xPath = mPt.getValue(tNode);
        List list = mDoc.selectNodes(xPath);
        if (list != null && list.size() > 0)
        {
            for (int i = 0; i < list.size(); i++)
            {
                Element node = (Element) list.get(i);
                node.getParent().remove(node);
                if (isOnlyFirst)
                    break;
            }
        }
    }

    public void addSubItemValues(String tParentNode, String tNodeItem,
            HashMap tItem, boolean isClean) throws InvoicePrtXmlException
    {
        String xPath = mPt.getValue(tParentNode);
        List list = mDoc.selectNodes(xPath);
        if (list == null || list.size() <= 0)
            throw new InvoicePrtXmlException(tParentNode + "结点不存在");
        // 只复制第一个节点
        Element parentNodeItem = (Element) list.get(0);
        if (isClean)
        {
            removeSubNode(parentNodeItem.getName(), false);
        }
        xPath = mPt.getValue(tNodeItem);
        List listNewNodeItem = this.mDoc_Backup.selectNodes(xPath);
        if (listNewNodeItem == null || listNewNodeItem.size() <= 0)
            throw new InvoicePrtXmlException(tNodeItem + "结点不存在");
        Element newNodeItem = ((Element) listNewNodeItem.get(0)).createCopy();
        // 对新添加节点进行赋值
        Iterator nodesKey = tItem.keySet().iterator();
        while (nodesKey.hasNext())
        {
            String subNodeKey = (String) nodesKey.next();
            String subXPath = mPt.getValue(tNodeItem, subNodeKey);
            List lsTemp = newNodeItem.selectNodes(subXPath);
            if (lsTemp == null || lsTemp.size() <= 0)
                throw new InvoicePrtXmlException(subNodeKey + "结点不存在");
            Element subNode = (Element) lsTemp.get(0);
            if (subNode == null)
                throw new InvoicePrtXmlException("新建节点" + tNodeItem
                        + "中不包含直接子结点" + subNodeKey);
            subNode.setText((String) tItem.get(subNodeKey));
        }
        parentNodeItem = parentNodeItem.addElement(tNodeItem);
        parentNodeItem.appendContent(newNodeItem);
    }

    public void addSubItemValues(String tParentNode, String tNodeItem,
            HashMap tItem) throws InvoicePrtXmlException
    {
        addSubItemValues(tParentNode, tNodeItem, tItem, false);
    }
}
