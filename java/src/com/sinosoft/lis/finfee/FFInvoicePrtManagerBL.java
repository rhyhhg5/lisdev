/**
 * 2007-5-29
 */
package com.sinosoft.lis.finfee;

import com.sinosoft.lis.db.LJInvoiceInfoDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LOPRTInvoiceManagerSchema;
import com.sinosoft.lis.vschema.LJInvoiceInfoSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class FFInvoicePrtManagerBL
{
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private String mOperate;

    private MMap map = new MMap();

    private VData mInputData = new VData();

    private GlobalInput mGlobalInput =  null;
  
    private LOPRTInvoiceManagerSchema mLOPRTInvoiceManagerSchema = null;

    public boolean submitData(VData cInputData, String cOperate)
    {
        mOperate = cOperate;

        if (!getInputData(cInputData))
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        if (!dealData())
        {
            buildError("dealData", "数据处理失败！");
            return false;
        }

        if (!prepareOutputData())
        {
            buildError("prepareOutputData", "在准备往后层处理所需要的数据时出错。");
            return false;
        }

        try
        {
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                buildError("dealData", "数据提交失败！");
                return false;
            }
            mResult.clear();
            mResult.add(mLOPRTInvoiceManagerSchema);
        }
        catch (Exception ex)
        {
            buildError("submitData", "其他未知异常！");
            return false;
        }
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        mLOPRTInvoiceManagerSchema = (LOPRTInvoiceManagerSchema) cInputData
                .getObjectByObjectName("LOPRTInvoiceManagerSchema", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);

        if (mGlobalInput == null || mLOPRTInvoiceManagerSchema == null)
            return false;
        return true;
    }

    private boolean dealData()
    {
        if (mOperate.equals("INSERT"))
        {
        	String invoicecode = mLOPRTInvoiceManagerSchema.getInvoiceCode();
			// 大连分公司手工输入，从网页获取
			String sqlCondition = "";
			if (invoicecode != null && !"".equals(invoicecode.trim())) {
				sqlCondition = " and invoicecode = '" + invoicecode + "'";
			}
			//不同机构登录
			    String tSqlCom="comcode  like  '"+mGlobalInput.ManageCom+"%'";
			  			    
			    if(mGlobalInput.ManageCom.equals("869100")){
				   String mGlobalManage="86910000";
				   tSqlCom="comcode ='"+mGlobalManage+"'";	
				   mLOPRTInvoiceManagerSchema.setComCode(mGlobalManage);
			   }	
			   if(mGlobalInput.ManageCom.length()>=4){
			     if(mGlobalInput.ManageCom.substring(0, 4).equals("8612")){
				   tSqlCom="comcode  like  '8612%' ";
			      }
			   }
        		 // 根据当前机构、发票号，获取该发票的基本信息。
                String tSql = "select  *  from LJInvoiceInfo  where  "
                	    +tSqlCom
                        + " and invoicestartno <= '"
                        + mLOPRTInvoiceManagerSchema.getInvoiceNo()
                        + "' and invoiceendno >= '"
                        + mLOPRTInvoiceManagerSchema.getInvoiceNo()
                        + "'" 
                        + sqlCondition
                        +" order by makedate desc with ur ";
                LJInvoiceInfoDB tLJInvoiceInfoDB = new LJInvoiceInfoDB();
                LJInvoiceInfoSet tLJInvoiceInfoSet = tLJInvoiceInfoDB
                        .executeQuery(tSql);

                if (tLJInvoiceInfoSet != null && tLJInvoiceInfoSet.size() > 0)
                {
                    mLOPRTInvoiceManagerSchema.setInvoiceCode(tLJInvoiceInfoSet
                            .get(1).getInvoiceCode());
                    mLOPRTInvoiceManagerSchema.setInvoiceCodeEx(tLJInvoiceInfoSet
                            .get(1).getInvoiceCodeExp());
                    mLOPRTInvoiceManagerSchema.setPayeeName(tLJInvoiceInfoSet
                            .get(1).getTaxpayerName());
                }
                else
                {  
                    buildError("dealData", "该发票相关基本信息不存在！");
                    return false;
                }
        	
           

            mLOPRTInvoiceManagerSchema.setStateFlag("0");
            // 写入轨迹信息
            mLOPRTInvoiceManagerSchema.setMakeDate(PubFun.getCurrentDate());
            mLOPRTInvoiceManagerSchema.setMakeTime(PubFun.getCurrentTime());
            mLOPRTInvoiceManagerSchema.setModifyDate(PubFun.getCurrentDate());
            mLOPRTInvoiceManagerSchema.setModifyTime(PubFun.getCurrentTime());
            mLOPRTInvoiceManagerSchema.setOperator(mGlobalInput.Operator);
            map.put(mLOPRTInvoiceManagerSchema, "INSERT"); //插入
        }
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            this.mInputData.add(mLOPRTInvoiceManagerSchema);
            mInputData.add(this.map);
        }
        catch (Exception ex)
        {
            buildError("submitData", "后台传递数据失败！");
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FFInvoicePrtManagerBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "group";

        LOPRTInvoiceManagerSchema tLOPRTInvoiceManagerSchema = new LOPRTInvoiceManagerSchema();
        //        tLOPRTInvoiceManagerSchema.setInvoiceCode("221010632641");
        tLOPRTInvoiceManagerSchema.setInvoiceNo("05769005");
      tLOPRTInvoiceManagerSchema.setComCode(tGlobalInput.ManageCom);
        //        tLOPRTInvoiceManagerSchema.setInvoiceCodeEx("221010632641");
        tLOPRTInvoiceManagerSchema.setContNo("2300000029");
        tLOPRTInvoiceManagerSchema.setEndorNo("2300000021009");
        tLOPRTInvoiceManagerSchema.setXSumMoney(2210.10);
        //        tLOPRTInvoiceManagerSchema.setStateFlag("0");
        //        tLOPRTInvoiceManagerSchema.setPayerCode("");
        tLOPRTInvoiceManagerSchema.setPayerName("测试员");
        tLOPRTInvoiceManagerSchema.setPayerAddr("辽宁");
        //        tLOPRTInvoiceManagerSchema.setPayeeCode("");
        //        tLOPRTInvoiceManagerSchema.setPayee("");
        tLOPRTInvoiceManagerSchema.setPayeeName("收款人");
        //        tLOPRTInvoiceManagerSchema.setOperator("op");
        tLOPRTInvoiceManagerSchema.setOpdate("2007-05-20");
        VData tVData = new VData();
        tVData.add(tGlobalInput);
        tVData.add(tLOPRTInvoiceManagerSchema);
       FFInvoicePrtManagerBL tFFInvoicePrtManagerBL = new FFInvoicePrtManagerBL();
        if (!tFFInvoicePrtManagerBL.submitData(tVData, "INSERT"))
        {
            System.out.println("failed...");
        }
        else
        {
            System.out.println("successed...");
        }
    }
}
