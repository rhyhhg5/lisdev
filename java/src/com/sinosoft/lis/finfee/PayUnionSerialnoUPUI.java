package com.sinosoft.lis.finfee;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PayUnionSerialnoUPUI
{

	public CErrors mErrors = new CErrors();

	public PayUnionSerialnoUPUI()
	{
	}

	public boolean submitData(VData cInputData, String cOperate)
	{

		PayUnionSerialnoUPBL tPayUnionSerialnoUPBL = new PayUnionSerialnoUPBL();

		if (!tPayUnionSerialnoUPBL.submitData(cInputData, cOperate))
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tPayUnionSerialnoUPBL.mErrors);
			CError tError = new CError();
			tError.moduleName = "tPayUnionSerialnoUPUI";
			tError.functionName = "submitData";
			tError.errorMessage = "数据查询失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

}
