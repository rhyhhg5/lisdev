package com.sinosoft.lis.finfee;

import com.sinosoft.lis.operfee.VerDuePayFeeQueryUI;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 银保通续期查询类，用户查询校验续期是否处于应收状态
 * 
 * @author zhangchengxuan
 */
public class YBTPayQuery {

    /** 返回TransferData集合 */
    TransferData result = new TransferData();

    /** 保单号 */
    private String contNo;

    /** 金额 用于后续收费查询 */
    private double payMoney;

    /** 错误处理类 */
    private CErrors mErrors = new CErrors();

    /**
     * 程序调用入口
     * 
     * @param cInputData
     * @return 程序查询成功标记 true-可以进行续期收费 false-查询失败,不能进行续期收费
     */
    public boolean submitData(TransferData tTransferData) {

        System.out.println("---YBTPayQuery--start---");

        if (!getInputData(tTransferData)) {
            return false;
        }

        System.out.println("---End getInputData---");

        if (!dealData()) {
            // 查询失败，清空返回信息
            result = new TransferData();
            return false;
        }

        System.out.println("---End dealData---");
        return true;
    }

    /**
     * 获取查询失败原因
     * 
     * @return mErrors 错误信息集合
     */
    public CErrors getErrorInf() {

        return this.mErrors;
    }

    /**
     * 获取查询返回信息
     * 
     * @return result 返回信息集合
     */
    public TransferData getResult() {

        /*
         * TransferData中
         * 
         * ManageCom-保单机构 AppntName-投保人名称 ContNo-保单号 PayMoney-金额 StartPayDate-缴费起始日期
         */

        return this.result;
    }

    /**
     * 获取参数
     * 
     * @param tTransferData payMoney-收费金额 contNo-保单号
     * @return 传入信息成功标记
     */
    private boolean getInputData(TransferData tTransferData) {

        try {
            String money = (String) tTransferData.getValueByName("PayMoney");

            contNo = (String) tTransferData.getValueByName("ContNo");

            if (contNo == null || contNo.equals("")) {
                mErrors.addOneError("保单号为空");
                System.out.println("---传入的保单号为空---");
                return false;
            }
            if (money == null || money.trim().equals("")) {
                this.payMoney = -1;
            }
            else {
                // 用于后续收费时校验
                try {
                    this.payMoney = Double.parseDouble(money);
                    if (payMoney <= 0) {
                        mErrors.addOneError("收费金额小于零");
                        System.out.println("---收费金额小于零---");
                        return false;
                    }
                }
                catch (Exception ex) {
                    mErrors.addOneError("金额格式错误");
                    System.out.println("---金额格式错误---");
                    return false;
                }
            }
        }
        catch (Exception e) {
            mErrors.addOneError("获取传入的保单信息失败");
            System.out.println("---获取保单信息失败---");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 程序处理入口
     * 
     * @return 查询成功标记
     */
    private boolean dealData() {

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String querySql = "";

        querySql = "select getnoticeno,a.managecom,a.appntname from lccont a ,ljspay b where a.contno=b.otherno "
                + "and conttype='1' and b.othernotype='2' and b.otherno='"
                + contNo
                + "' union select b.getnoticeno,a.managecom,a.grpname from lcgrpcont a ,ljspaygrp b where a.grpcontno=b.grpcontno "
                + "and a.grpcontno<>'00000000000000000000' " + " and b.sumduepaymoney<>0 and b.grpcontno='" + contNo
                + "'";
        tSSRS = tExeSQL.execSQL(querySql);
        if (tSSRS.MaxRow == 0) {
            this.mErrors.addOneError("未查到保单相关信息");
            System.out.println("---未查到保单相关信息---");
            return false;
        }
        else if (tSSRS.MaxRow > 1) {
            this.mErrors.addOneError("查到多条保单应收信息");
            System.out.println("---查到多条保单应收信息---");
            return false;
        }
        else {

            // 设置返回信息
            result.setNameAndValue("ManageCom", tSSRS.GetText(1, 2));
            result.setNameAndValue("AppntName", tSSRS.GetText(1, 3));
            result.setNameAndValue("ContNo", contNo);

            result.setNameAndValue("GetNoticeNo", tSSRS.GetText(1, 1));

            LJSPaySchema tLJSPaySchema = new LJSPaySchema();
            tLJSPaySchema.setGetNoticeNo(tSSRS.GetText(1, 1));
            VData tVData = new VData();
            tVData.add(tLJSPaySchema);
            VerDuePayFeeQueryUI tVerDuePayFeeQueryUI = new VerDuePayFeeQueryUI();
            if (!tVerDuePayFeeQueryUI.submitData(tVData, "QUERYDETAIL")) {
                mErrors.addOneError("续期查询失败，原因是: " + tVerDuePayFeeQueryUI.mErrors.getError(0).errorMessage);
                System.out.println("续期查询失败，原因是: " + tVerDuePayFeeQueryUI.mErrors.getError(0).errorMessage);
                return false;
            }

            querySql = "Select (Select Count(1) " +
                    "From Ljspayb " +
                    "Where Otherno = Sp.Otherno And Dealstate = '1' And " +
                    "Makedate < (Select Makedate From Ljspayb Where Getnoticeno = '" + tSSRS.GetText(1, 1) + "')) + " +
                    "(Select Count(1)" +
                    " From Ljspayb " +
                    "Where Otherno = Sp.Otherno And Dealstate = '1' And " +
                    "Makedate = (Select Makedate From Ljspayb Where Getnoticeno = '" + tSSRS.GetText(1, 1) + "') And " +
                    "Maketime < (Select Maketime From Ljspayb Where Getnoticeno = '" + tSSRS.GetText(1, 1) + "')) + " +
                    "(Select Count(1) " +
                    "From Ljspayb " +
                    "Where Otherno = Sp.Otherno And Getnoticeno = '" + tSSRS.GetText(1, 1) + "') " +
                    "From Ljspay Sp Where Sp.Getnoticeno = '" + tSSRS.GetText(1, 1) + "'";

            // 获取续期的期数
            String payCount = "";
            ExeSQL tExe = new ExeSQL();
            payCount = tExe.getOneValue(querySql);
            System.out.println("获取续期期数----" + payCount);
            result.setNameAndValue("PayCount", payCount);

        }

        querySql = "select sumduepaymoney,startpaydate,paydate from ljspay where otherno='" + contNo
                + "' and othernotype='2' and sumduepaymoney<>0";
        tSSRS = tExeSQL.execSQL(querySql);
        if (tSSRS.MaxRow == 0) {
            this.mErrors.addOneError("未查到保单续期应收数据");
            System.out.println("---未查到保单续期应收数据---");
            return false;
        }
        else if (tSSRS.MaxRow > 1) {
            this.mErrors.addOneError("查询到该保单多笔续期应收数据");
            System.out.println("---查询到该保单多笔续期应收数据---");
            return false;
        }
        else {
            // 若传入金额，则进行金额校验
            if (payMoney > 0 && payMoney != Double.parseDouble(tSSRS.GetText(1, 1))) {
                this.mErrors.addOneError("金额与应收金额不一致");
                System.out.println("---金额与应收金额不一致---");
                return false;
            }

            // 设置返回信息
            result.setNameAndValue("PayMoney", tSSRS.GetText(1, 1));
            result.setNameAndValue("StartPayDate", tSSRS.GetText(1, 2));
            result.setNameAndValue("PayDate", tSSRS.GetText(1, 3));
        }

        querySql = "select 1 from ljspay where otherno='" + contNo + "' "
                + "and othernotype='2' and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null) "
                + "and not exists (select 1 from ljtempfee where tempfeeno=ljspay.getnoticeno) ";
        tSSRS = tExeSQL.execSQL(querySql);
        if (tSSRS.MaxRow == 0) {
            this.mErrors.addOneError("该笔续期收费已通过其他方式进行收费");
            System.out.println("---该笔续期收费已通过其他方式进行收费---");
            return false;
        }

        return true;
    }

    /**
     * 调用方法举例
     */
    public static void main(String[] arr) {

        CErrors cErrors = new CErrors();

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("ContNo", "000922666000001");

        YBTPayQuery tYBTPayQuery = new YBTPayQuery();

        if (!tYBTPayQuery.submitData(tTransferData)) {
            // 若查询失败，可获得返回信息
            cErrors = tYBTPayQuery.getErrorInf();
            System.out.println(cErrors.getLastError()); // 查询失败原因
        }
        else {
            TransferData result = tYBTPayQuery.getResult(); // getResult 信息返回函数
            System.out.println(result.getValueByName("ManageCom")); // 保单机构
            System.out.println(result.getValueByName("AppntName")); // 投保人名称
            System.out.println(result.getValueByName("ContNo")); // 保单号
            System.out.println(result.getValueByName("PayMoney")); // 金额
            System.out.println(result.getValueByName("StartPayDate")); // 缴费起始日期
            System.out.println(result.getValueByName("PayDate")); // 缴费截至日期
            System.out.println(result.getValueByName("PayCount")); // 期数
        }

    }
}
