package com.sinosoft.lis.finfee;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 付费方式修改功能</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

import java.util.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class ModifyLJAGetBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 传出数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public  CErrors mErrors = new CErrors();
  /** 全局基础数据 */
  private GlobalInput mGlobalInput = new GlobalInput();
  /** 额外传递的参数 */
  //private TransferData mTransferData = null;
  private Reflections mReflections = new Reflections();

  /** 传入的业务数据 */
  private LJAGetSet inLJAGetSet = new LJAGetSet();

  /** 传出的业务数据 */
  private LJAGetSet outLJAGetSet = new LJAGetSet();
  private LBAGetForPayModeSet outLBAGetForPayModeSet = new LBAGetForPayModeSet();

  public ModifyLJAGetBL() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"INSERT"
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) return false;
    System.out.println("---End getInputData---");

    //进行业务处理
    if (!dealData()) return false;
    System.out.println("---End dealData---");

    //需要传到后台处理
    if (mOperate.equals("INSERT")) {
      //准备往后台的数据
      if (!prepareOutputData()) return false;
      System.out.println("---End prepareOutputData---");

      System.out.println("Start ModifyLJAGet BLS Submit...");
      ModifyLJAGetBLS tModifyLJAGetBLS = new ModifyLJAGetBLS();
      if(tModifyLJAGetBLS.submitData(mInputData, cOperate) == false)	{
        //@@错误处理
        this.mErrors.copyAllErrors(tModifyLJAGetBLS.mErrors);
        mResult.clear();
        mResult.add(mErrors.getFirstError());
        return false;
      }	else {
        mResult = tModifyLJAGetBLS.getResult();
      }
      System.out.println("End ModifyLJAGet BLS Submit...");
    }
    //不需要传到后台处理
    else if (mOperate.equals("")) {
    }

    return true;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData()	{
    try {
      mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0);

      //mTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);

      inLJAGetSet = (LJAGetSet)mInputData.getObjectByObjectName("LJAGetSet", 0);
    }
    catch (Exception e) {
      // @@错误处理
      e.printStackTrace();
      CError tError = new CError();
      tError.moduleName = "ModifyLJAGetBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "接收数据失败!!";
      this.mErrors.addOneError(tError) ;
      return false;
    }

    return true;
  }

  /**
   * 根据前面的输入数据，进行逻辑处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    try {
      if (mOperate.equals("INSERT")) {
        LJAGetSchema tLJAGetSchema;
        for (int i=0; i<inLJAGetSet.size(); i++) {
          tLJAGetSchema = inLJAGetSet.get(i + 1);

          LJAGetDB tLJAGetDB = new LJAGetDB();
          tLJAGetDB.setActuGetNo(tLJAGetSchema.getActuGetNo());
          if (!tLJAGetDB.getInfo()) throw new Exception("实付表无数据！");

          if (tLJAGetDB.getEnterAccDate() != null) throw new Exception("该笔款项已付清，不能修改付费方式！");

          if (tLJAGetDB.getBankOnTheWayFlag()!=null && tLJAGetDB.getBankOnTheWayFlag().equals("1")) throw new Exception("该笔付款在送银行途中，不能修改付费方式！");

          //备份
          LBAGetForPayModeSchema tLBAGetForPayModeSchema = new LBAGetForPayModeSchema();
          mReflections.transFields(tLBAGetForPayModeSchema, tLJAGetDB.getSchema());
          tLBAGetForPayModeSchema.setChangeSerialNo(PubFun1.CreateMaxNo("LBAGet", 20));
          outLBAGetForPayModeSet.add(tLBAGetForPayModeSchema);

          //变更
          tLJAGetDB.setPayMode(tLJAGetSchema.getPayMode());
          tLJAGetDB.setBankCode(tLJAGetSchema.getBankCode());
          tLJAGetDB.setBankAccNo(tLJAGetSchema.getBankAccNo());
          tLJAGetDB.setAccName(tLJAGetSchema.getAccName());
          outLJAGetSet.add(tLJAGetDB.getSchema());
        }
      }
      else if (mOperate.equals("")) {
      }
    }
    catch(Exception e) {
      // @@错误处理
      e.printStackTrace();
      CError tError = new CError();
      tError.moduleName = "ModifyLJAGetBL";
      tError.functionName = "dealData";
      tError.errorMessage = "数据处理错误! " + e.getMessage();
      this.mErrors.addOneError(tError);
      return false;
    }

    return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * @return 如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData() {
    try {
      mInputData.clear();

      mInputData.add(outLJAGetSet);
      mInputData.add(outLBAGetForPayModeSet);
    }
    catch(Exception e) {
      // @@错误处理
      e.printStackTrace();
      CError tError =new CError();
      tError.moduleName="ModifyLJAGetBL";
      tError.functionName="prepareOutputData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错! ";
      this.mErrors.addOneError(tError);
      return false;
    }

    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }

  /**
   * 主函数，测试用
   */
  public static void main(String[] args) {
    ModifyLJAGetBL ModifyLJAGetBL1 = new ModifyLJAGetBL();
  }
}
