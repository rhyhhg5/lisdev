 package com.sinosoft.lis.finfee;

import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;
import java.lang.String;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class OperFinFeeGetBL  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 数据操作字符串 */
  private String mOperate;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
//标志位
  String flag="";
//实付总表
  private LJAGetBL   mLJAGetBL  = new LJAGetBL();
//财务给付表
  private LJFIGetBL  mLJFIGetBL = new LJFIGetBL();
//给付表(生存领取_实付)
  private LJAGetDrawSet mLJAGetDrawSet = new LJAGetDrawSet();
//批改补退费表(实收/实付)
//  private LJAGetEndorseSet mLJAGetEndorseSet = new LJAGetEndorseSet();
  private String mUpdateLJAGetEndorse="";
  private LLPrepaidClaimSet mLLPrepaidClaimSet = new LLPrepaidClaimSet(); 
//暂交费退费实付表
  private LJAGetTempFeeSet mLJAGetTempFeeSet = new LJAGetTempFeeSet();
//其他退费实付表
  private LJAGetOtherSet mLJAGetOtherSet = new LJAGetOtherSet();
//  赔付实付表
  private LJAGetClaimSet mLJAGetClaimSet = new LJAGetClaimSet();
  /** 社保通调查费结算细目表 */
  private SocialSecurityFeeClaimSet mSocialSecurityFeeClaimSet = new SocialSecurityFeeClaimSet();
// 红利给付实付表
  private LJABonusGetSet mLJABonusGetSet = new LJABonusGetSet();
//批改补退费表
  private LAChargeSet mLAChargeSet = new LAChargeSet();
//Session信息
  private GlobalInput mG = new GlobalInput();
//理赔批案表
  private LLRegisterSet mLLRegisterSet = new LLRegisterSet();
//理赔案件表
  private LLCaseSet mLLCaseSet = new LLCaseSet();

  //业务处理相关变量

  public OperFinFeeGetBL() {
  }
  public static void main(String[] args) 
  {
	  
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
System.out.println("OperateData:  "+cOperate);


    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
System.out.println("After getinputdata");

    //进行业务处理
    if (!dealData())
      return false;
System.out.println("After dealData！");
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
System.out.println("After prepareOutputData");

    System.out.println("Start LJAGet BL Submit...");

    OperFinFeeGetBLS tOperFinFeeGetBLS=new OperFinFeeGetBLS();
    tOperFinFeeGetBLS.submitData(mInputData,cOperate,mUpdateLJAGetEndorse);

    System.out.println("End LJAGet BL Submit...");

    //如果有需要处理的错误，则返回
    if (tOperFinFeeGetBLS.mErrors .needDealError())
    {
        this.mErrors .copyAllErrors(tOperFinFeeGetBLS.mErrors ) ;
    }

    mInputData=null;
    return true;
  }

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData()
  {
    int i,iMax;
    boolean tReturn =false;
    String tNo="";

    //处理集体信息数据
    //添加纪录
    if(this.mOperate.equals("VERIFY"))
    {
//首先查询财务给付表中是否有该纪录
      if(QyeryLJFIGet()) return false;

//1-处理财务给付表
      mLJFIGetBL.setConfDate(CurrentDate);
      mLJFIGetBL.setMakeDate(CurrentDate);//入机日期
      mLJFIGetBL.setMakeTime(CurrentTime);//入机时间
      mLJFIGetBL.setModifyDate(CurrentDate);//最后一次修改日期
      mLJFIGetBL.setModifyTime(CurrentTime);//最后一次修改时间
      mLJFIGetBL.setConfMakeDate(CurrentDate);
      mLJFIGetBL.setPolicyCom(mLJAGetBL.getManageCom());
      String poCom = mLJAGetBL.getManageCom().trim();
      if(poCom != null && !poCom.equals("") && poCom.length()<8){
          int len = poCom.length();
          for(int j=0 ; j < 8-len ; j++){
              poCom = poCom + "0";
          }
          mLJFIGetBL.setPolicyCom(poCom);
      }
      String mngCom = mG.ComCode ;
      int len = mngCom.length();
      for(int j=0 ; j < 8-len ; j++){
          mngCom = mngCom+"0";
      }
      mLJFIGetBL.setManageCom(mngCom);
      mLJFIGetBL.setAgentCom(mLJAGetBL.getAgentCom());
      mLJFIGetBL.setAgentType(mLJAGetBL.getAgentType());
      mLJFIGetBL.setAgentGroup(mLJAGetBL.getAgentGroup());
      mLJFIGetBL.setAgentCode(mLJAGetBL.getAgentCode());

//2-实付总表，单项纪录
     mLJAGetBL.setPayMode(mLJFIGetBL.getPayMode());
     mLJAGetBL.setConfDate(CurrentDate);
     mLJAGetBL.setManageCom(poCom);

     //mLJAGetBL.setMakeDate(CurrentDate);
     //mLJAGetBL.setMakeTime(CurrentTime);
     /*if(mLJFIGetBL.getChequeNo()!=null)
     {
        mLJAGetBL.setBankAccNo(mLJFIGetBL.getChequeNo());
     }
     if(mLJFIGetBL.getBankCode()!=null)
     {
         mLJAGetBL.setBankCode(mLJFIGetBL.getBankCode());
     }*/
     mLJAGetBL.setModifyDate(CurrentDate);
     mLJAGetBL.setModifyTime(CurrentTime);

//3-查询实付总表:得到实付号码，其它号码类型
     String ActuGetNo = mLJAGetBL.getActuGetNo();
     String OtherNoType = mLJAGetBL.getOtherNoType();
     System.out.println("LJAGet.getOtherNoType()="+OtherNoType);

     //更新 给付表(生存领取_实付)   LJAGetDraw
     if(OtherNoType.equals("1")||OtherNoType.equals("2")||OtherNoType.equals("0"))
     {
        mLJAGetDrawSet=updateLJAGetDraw(mLJAGetBL);
        if(mLJAGetDrawSet==null)
          return false;
        else flag="1";
     }
     if(OtherNoType.equals("3")||OtherNoType.equals("10"))//更新 批改补退费表(实收/实付) LJAGetEndorse
     {
    	System.out.println("更新 批改补退费表(实收/实付)");
        updateLJAGetEndorse(mLJAGetBL);
        if(StrTool.cTrim(this.mUpdateLJAGetEndorse).equals(""))
          return false;
        else flag="3";
     }

     if(OtherNoType.equals("4"))    //更新 暂交费退费实付表
     {
        mLJAGetTempFeeSet=updateLJAGetTempFee(mLJAGetBL);
        if(mLJAGetTempFeeSet==null)
          return false;
        else flag="4";
     }
     //社保通调查费结算细目表在财务付费结束后状态的改变,flag借用8	add by houyd
     if("23".equals(OtherNoType)){
    	 String tComCode="";
         SSRS ttSSRS=new ExeSQL().execSQL("select insurancecomcode from SocialSecurityFeeClaim where actugetno ='"+ActuGetNo+"' with ur");
         if(ttSSRS!=null && ttSSRS.getMaxRow()>0){
             tComCode=ttSSRS.GetText(1,1);
         }
         if(tComCode!=null && !tComCode.trim().equals("")){
             mLJFIGetBL.setPolicyCom(tComCode);
             mLJAGetBL.setManageCom(tComCode);
             
         }
    	 mSocialSecurityFeeClaimSet=updateSocialSecurityFeeClaim(mLJAGetBL);
    	 if(mSocialSecurityFeeClaimSet==null)
             return false;
           else flag="8";
     }

     if(OtherNoType.equals("5")||OtherNoType.equals("F")||OtherNoType.equals("C")||OtherNoType.equals("Y"))//更新 赔付实付表LJAGetClaim Y-理赔预付赔款
     {
         String tComCode="";
         SSRS ttSSRS=new ExeSQL().execSQL("select managecom from lccont where contno in (select contno from LJAGetClaim where actugetno ='"+ActuGetNo+"')");
         if(ttSSRS!=null && ttSSRS.getMaxRow()>0){
             tComCode=ttSSRS.GetText(1,1);
         }
         else{
        	 ttSSRS = new ExeSQL().execSQL("select managecom from lbcont where contno in (select contno from LJAGetClaim where actugetno ='"+ActuGetNo+"')");
        	  if(ttSSRS!=null && ttSSRS.getMaxRow()>0){
        		 tComCode=ttSSRS.GetText(1,1);
        	 }
         }
         if(tComCode!=null && !tComCode.trim().equals("")){
             mLJFIGetBL.setPolicyCom(tComCode);
             mLJAGetBL.setManageCom(tComCode);
             
         }
        mLJAGetClaimSet=updateLJAGetClaim(mLJAGetBL);
        if(mLJAGetClaimSet==null)
          return false;
        else flag="5";
        System.out.println("........Come in the setLLCaseFlag........");
        setLLCaseFalg(mLJAGetBL.getOtherNo(),mLJAGetClaimSet);
        if(OtherNoType.equals("Y"))
        {
      	  LLPrepaidClaimDB tLLPrepaidClaimDB = new LLPrepaidClaimDB();
      	  tLLPrepaidClaimDB.setPrepaidNo(mLJAGetBL.getOtherNo());
      	  if(tLLPrepaidClaimDB.getInfo())
      	  {
      		  LLPrepaidClaimSchema tLLPrepaidClaimSchema = tLLPrepaidClaimDB.getSchema();
      		  tLLPrepaidClaimSchema.setRgtState("06");
      		  this.mLLPrepaidClaimSet.add(tLLPrepaidClaimSchema);
      		  
      	  }
        }
//        LLRegisterDB tLLRegisterDB = new LLRegisterDB();
//        tLLRegisterDB.setRgtNo(mLJAGetBL.getOtherNo());
//        if(tLLRegisterDB.getInfo()){
//            tLLRegisterDB.setRgtState("04");
//            tLLRegisterDB.update();
//        }
//        String tRgtNo = "";
//        String tCaseNo = "";
//        for (int k=1;k<=mLJAGetClaimSet.size();k++){
//            LLCaseDB tLLCaseDB = new LLCaseDB();
//            tLLCaseDB.setCaseNo(mLJAGetClaimSet.get(k).getOtherNo());
//            if(tLLCaseDB.getInfo()){
//                tRgtNo = tLLCaseDB.getRgtNo();
//                tCaseNo = tLLCaseDB.getCaseNo();
//                tLLCaseDB.setRgtState("12");
//                tLLCaseDB.update();
//            }
//        }
//        //团单下最后一个人做完给付确认应更新团单案件的状态为“04”－所有个人给付完毕
//        if(mLJAGetBL.getOtherNo().substring(0,1).equals("C")&&!tRgtNo.equals(tCaseNo)){
//            LLRegisterDB tsLLRegisterDB = new LLRegisterDB();
//            tsLLRegisterDB.setRgtNo(tRgtNo);
//            if (tsLLRegisterDB.getInfo()) {
//                if ("1".equals(tsLLRegisterDB.getRgtClass())) {
//                    boolean tFlag = true;
//                    LLCaseDB tmpLLCaseDB = new LLCaseDB();
//                    LLCaseSet tmpLLCaseSet = new LLCaseSet();
//                    tmpLLCaseDB.setRgtNo(tsLLRegisterDB.getRgtNo());
//                    tmpLLCaseSet.set(tmpLLCaseDB.query());
//                    if (tmpLLCaseSet != null && tmpLLCaseSet.size() != 0) {
//                        for (int m = 1; m <= tmpLLCaseSet.size(); m++) {
//                            if (tmpLLCaseSet.get(m).getCaseNo().equals(tCaseNo))
//                                continue;
//                            if (!("12".equals(tmpLLCaseSet.get(m).getRgtState()))) {
//                                tFlag = false;
//                                break;
//                            }
//                        }
//                    }
//                    if (tFlag) {
//                        tsLLRegisterDB.setRgtState("04");
//                        tsLLRegisterDB.update();
//                    }
//                }
//            }
//        }
     }

     if(OtherNoType.equals("6")||OtherNoType.equals("8"))//更新 其他退费实付表         LJAGetOther
     {
        mLJAGetOtherSet=updateLJAGetOther(mLJAGetBL);
        if(mLJAGetOtherSet==null)
          return false;
        else flag="6";
     }

     if(OtherNoType.equals("7"))  //更新 红利给付实付表         LJABonusGet
     {
        mLJABonusGetSet=updateLJABonusGet(mLJAGetBL);
        if(mLJABonusGetSet==null)
          return false;
        else flag="7";
     }
     /*if(OtherNoType.equals("BC")||OtherNoType.equals("AC"))  //更新 销售实付表         LAChargeSet
     {
        mLAChargeSet=updateLAChargeSet(mLJAGetBL);
        if(mLAChargeSet==null)
          return false;
        else flag="BC";
     }*/

       tReturn=true;
    }
    return tReturn ;
 }
public boolean setLLCaseFalg(String strRgtNo,LJAGetClaimSet aLJAGetClaimSet)
 {
     LLRegisterDB tLLRegisterDB = new LLRegisterDB();
     tLLRegisterDB.setRgtNo(strRgtNo);
     if(tLLRegisterDB.getInfo()){
         tLLRegisterDB.setRgtState("04");
         this.mLLRegisterSet.add(tLLRegisterDB.getSchema());
     }
     String tRgtNo = "";
     String tCaseNo = "";
     for (int k=1;k<=aLJAGetClaimSet.size();k++){
         String tfeeFinaType=aLJAGetClaimSet.get(k).getFeeFinaType();  
         boolean tsummoney=false;
         SSRS ttSSRS=new ExeSQL().execSQL("select 1 from (select sum(pay) a from ljagetclaim where actugetno='"+aLJAGetClaimSet.get(k).getActuGetNo()+"' and otherno='"+aLJAGetClaimSet.get(k).getOtherNo()+"' and feefinatype<>'YF')as newpay where a<>0 ");
         if(ttSSRS!=null && ttSSRS.getMaxRow()>0){
        	 tsummoney=true;
         }
         if(tfeeFinaType!=null && (tfeeFinaType.equals("JJ") || tfeeFinaType.equals("ZJ"))){
             continue;
         }else if(!tsummoney){	 
        	 continue;
         }else{
             LLCaseDB tLLCaseDB = new LLCaseDB();
             tLLCaseDB.setCaseNo(aLJAGetClaimSet.get(k).getOtherNo());
             if(tLLCaseDB.getInfo()){
                 tRgtNo = tLLCaseDB.getRgtNo();
                 tCaseNo = tLLCaseDB.getCaseNo();
                 tLLCaseDB.setRgtState("12");
                 this.mLLCaseSet.add(tLLCaseDB.getSchema());
             }
         }
     }
     //团单下最后一个人做完给付确认应更新团单案件的状态为“04”－所有个人给付完毕
     if(strRgtNo.substring(0,1).equals("C")&&!tRgtNo.equals(tCaseNo)){
         LLRegisterDB tsLLRegisterDB = new LLRegisterDB();
         tsLLRegisterDB.setRgtNo(tRgtNo);
         if (tsLLRegisterDB.getInfo()) {
             if ("1".equals(tsLLRegisterDB.getRgtClass())) {
                 boolean tFlag = true;
                 LLCaseDB tmpLLCaseDB = new LLCaseDB();
                 LLCaseSet tmpLLCaseSet = new LLCaseSet();
                 tmpLLCaseDB.setRgtNo(tsLLRegisterDB.getRgtNo());
                 tmpLLCaseSet.set(tmpLLCaseDB.query());
                 if (tmpLLCaseSet != null && tmpLLCaseSet.size() != 0) {
                     for (int m = 1; m <= tmpLLCaseSet.size(); m++) {
                         if (tmpLLCaseSet.get(m).getCaseNo().equals(tCaseNo))
                             continue;
                         if (!("12".equals(tmpLLCaseSet.get(m).getRgtState()))) {
                             tFlag = false;
                             break;
                         }
                     }
                 }
                 if (tFlag) {
                     tsLLRegisterDB.setRgtState("04");
                     this.mLLRegisterSet.add(tsLLRegisterDB.getSchema());
                 }
             }
         }
        }
     return true;
 }
  //从输入数据中得到所有对象
  //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData mInputData)
  {
    //实付总表
    mLJAGetBL.setSchema((LJAGetSchema)mInputData.getObjectByObjectName("LJAGetSchema",0));
    // 财务给付表
    mLJFIGetBL.setSchema((LJFIGetSchema)mInputData.getObjectByObjectName("LJFIGetSchema",0));
    mG = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0);
    if(mLJAGetBL==null || mLJFIGetBL ==null)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="OperFinFeeGetBL";
      tError.functionName="getInputData";
      tError.errorMessage="没有得到足够的数据，请您确认!";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    if(mLJAGetBL.getBankOnTheWayFlag()!=null)
    {
        if(mLJAGetBL.getBankOnTheWayFlag().equals("1"))
        {
            // @@错误处理
            CError tError =new CError();
            tError.moduleName="OperFinFeeGetBL";
            tError.functionName="getInputData";
            tError.errorMessage="正在送银行途中，不能财务付费!";
            this.mErrors .addOneError(tError) ;
            return false;
        }
    }
    return true;
  }

  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData()
  {
    mInputData=new VData();
    try
    {
//1-处理财务给付表
      mInputData.add(mLJFIGetBL);
//2-实付总表，单项纪录
      mInputData.add(mLJAGetBL);
//3-检验标志位
      if(flag.equals("1")) mInputData.add(mLJAGetDrawSet);
      System.out.println("11111:"+mUpdateLJAGetEndorse);
      //if(flag.equals("3")) mInputData.add(this.mUpdateLJAGetEndorse);
      if(flag.equals("4")) mInputData.add(mLJAGetTempFeeSet);
      if(flag.equals("5")){
          mInputData.add(mLJAGetClaimSet);
          mInputData.add(this.mLLCaseSet);
          mInputData.add(this.mLLRegisterSet);
          mInputData.add(this.mLLPrepaidClaimSet);
      }
      if(flag.equals("6")) mInputData.add(mLJAGetOtherSet);
      if(flag.equals("7")) mInputData.add(mLJABonusGetSet);
      if(flag.equals("BC")) mInputData.add(mLAChargeSet);
      //add by Houyd 社保通调查费结算细目表在财务付费结束后状态的改变,flag借用8
      if("8".equals(flag)) mInputData.add(mSocialSecurityFeeClaimSet);
//4-加上标志位
      mInputData.add(flag);
      if(flag.equals("3")) mInputData.add(this.mUpdateLJAGetEndorse);

      System.out.println("prepareOutputData:");
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="OperFinFeeGetBL";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

  private boolean QyeryLJFIGet()
  {

    String ActuGetNo=mLJFIGetBL.getActuGetNo();
    String PayMode =mLJFIGetBL.getPayMode();
    if(ActuGetNo==null)
    {
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "queryLJFIGet";
      tError.errorMessage = "实付号码不能为空!";
      this.mErrors.addOneError(tError);
      return true;
    }
    String sqlStr="select * from LJFIGet where ActuGetNo='"+ActuGetNo+"'";
//    sqlStr=sqlStr+" and PayMode='"+PayMode+"'";
 System.out.println("查询财务给付表:"+sqlStr);
     LJFIGetSet tLJFIGetSet = new LJFIGetSet();
     LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();
     LJFIGetDB tLJFIGetDB = new LJFIGetDB();
     tLJFIGetSet = tLJFIGetDB.executeQuery(sqlStr);
     if (tLJFIGetDB.mErrors.needDealError() == true)
     {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJFIGetDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "queryLJFIGet";
      tError.errorMessage = "财务给付表查询失败!";
      this.mErrors.addOneError(tError);
      tLJFIGetSet.clear();
      return true;
     }
     if (tLJFIGetSet.size() > 0)
     {
     // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "queryLJFIGet";
      tError.errorMessage="实付号码为："+ActuGetNo+" 的纪录已经存在！";
      this.mErrors .addOneError(tError) ;
      return true;
      }

    return false;
  }

  /**
   * 查询给付表(生存领取_实付)
   * @param LJAGetBL
   * @return
   */
  private LJAGetDrawSet updateLJAGetDraw(LJAGetBL mLJAGetBL)
  {
    if(mLJAGetBL==null)
    {
      // @@错误处理
       CError tError = new CError();
       tError.moduleName = "OperFinFeeGetBL";
       tError.functionName = "updateLJAGetDraw";
       tError.errorMessage="传入参数不能为空！";
       this.mErrors .addOneError(tError) ;
       return null;
    }
    String sqlStr="select * from LJAGetDraw where ActuGetNo='"+mLJAGetBL.getActuGetNo()+"'";
 System.out.println("查询给付表(生存领取_实付):"+sqlStr);
     LJAGetDrawSet tLJAGetDrawSet = new LJAGetDrawSet();
     LJAGetDrawSchema tLJAGetDrawSchema = new LJAGetDrawSchema();
     LJAGetDrawDB tLJAGetDrawDB = new LJAGetDrawDB();
     tLJAGetDrawSet = tLJAGetDrawDB.executeQuery(sqlStr);
     if (tLJAGetDrawDB.mErrors.needDealError() == true)
     {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJAGetDrawDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "queryLJAGetDraw";
      tError.errorMessage = "给付表(生存领取_实付)查询失败!";
      this.mErrors.addOneError(tError);
      tLJAGetDrawSet.clear();
      return null;
     }
     LJAGetDrawSet newLJAGetDrawSet = new LJAGetDrawSet();
     for(int n=1;n<=tLJAGetDrawSet.size();n++)
     {
       tLJAGetDrawSchema = new LJAGetDrawSchema();
       tLJAGetDrawSchema = tLJAGetDrawSet.get(n);
       tLJAGetDrawSchema.setConfDate(mLJAGetBL.getConfDate());
       tLJAGetDrawSchema.setEnterAccDate(mLJAGetBL.getEnterAccDate());
       tLJAGetDrawSchema.setModifyDate(CurrentDate);
       tLJAGetDrawSchema.setModifyTime(CurrentTime);
       newLJAGetDrawSet.add(tLJAGetDrawSchema);
     }
     if (newLJAGetDrawSet.size()==0)
     {
     // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetDraw";
      tError.errorMessage="没有可以更新的给付表(生存领取_实付)！";
      this.mErrors .addOneError(tError) ;
      return null;
      }
     return newLJAGetDrawSet;
  }

  /**
   * 查询批改补退费表(实收/实付)
   * @param LJAGetBL
   * @return
   */
  private LJAGetEndorseSet updateLJAGetEndorse(LJAGetBL mLJAGetBL)
   {
    if(mLJAGetBL==null)
    {
      // @@错误处理
       CError tError = new CError();
       tError.moduleName = "OperFinFeeGetBL";
       tError.functionName = "updateLJAGetEndorse";
       tError.errorMessage="传入参数不能为空！";
       this.mErrors .addOneError(tError) ;
       return null;
    }
    String sqlStr="select * from LJAGetEndorse where ActuGetNo='"+mLJAGetBL.getActuGetNo()+"'";
 System.out.println("查询给付表(生存领取_实付):"+sqlStr);
     LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
     LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
     LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
     tLJAGetEndorseSet = tLJAGetEndorseDB.executeQuery(sqlStr);
     if (tLJAGetEndorseDB.mErrors.needDealError() == true)
     {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJAGetEndorseDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetEndorse";
      tError.errorMessage = "批改补退费表(实收/实付)查询失败!";
      this.mErrors.addOneError(tError);
      tLJAGetEndorseSet.clear();
      return null;
     }
     LJAGetEndorseSet newLJAGetEndorseSet = new LJAGetEndorseSet();
     for(int n=1;n<=tLJAGetEndorseSet.size();n++)
     {
       tLJAGetEndorseSchema = new LJAGetEndorseSchema();
       tLJAGetEndorseSchema = tLJAGetEndorseSet.get(n);
//       tLJAGetEndorseSchema.setGetConfirmDate(mLJAGetBL.getConfDate());
//       tLJAGetEndorseSchema.setEnterAccDate(mLJAGetBL.getEnterAccDate());
//       tLJAGetEndorseSchema.setModifyDate(CurrentDate);
//       tLJAGetEndorseSchema.setModifyTime(CurrentTime);
       newLJAGetEndorseSet.add(tLJAGetEndorseSchema);
       //数据量过大时可能出现问题更改  dongjian
       mUpdateLJAGetEndorse="update ljagetendorse set GetConfirmDate='"+mLJAGetBL.getConfDate()+"',"
       +" EnterAccDate = '"+mLJAGetBL.getEnterAccDate()+"',"
       +" ModifyDate = '"+CurrentDate+"',"
       +" ModifyTime = '"+CurrentTime+"'"
       +" where actugetno = '"+tLJAGetEndorseSchema.getActuGetNo()+"'";
       System.out.println("mUpdateLJAGetEndorse is "+mUpdateLJAGetEndorse);
     }
     if (newLJAGetEndorseSet.size()==0)
     {
     // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetEndorse";
      tError.errorMessage="没有可以更新的批改补退费表(实收/实付)！";
      this.mErrors .addOneError(tError) ;
      return null;
      }
     return newLJAGetEndorseSet;

   }

  /**
   * 查询其他退费实付表
   * @param LJAGetBL
   * @return
   */
  private LJAGetOtherSet updateLJAGetOther(LJAGetBL mLJAGetBL)
   {
    if(mLJAGetBL==null)
    {
      // @@错误处理
       CError tError = new CError();
       tError.moduleName = "OperFinFeeGetBL";
       tError.functionName = "updateLJAGetOther";
       tError.errorMessage="传入参数不能为空！";
       this.mErrors .addOneError(tError) ;
       return null;
    }
    String sqlStr="select * from LJAGetOther where ActuGetNo='"+mLJAGetBL.getActuGetNo()+"'";
 System.out.println("其他退费实付表:"+sqlStr);
     LJAGetOtherSet tLJAGetOtherSet = new LJAGetOtherSet();
     LJAGetOtherSchema tLJAGetOtherSchema = new LJAGetOtherSchema();
     LJAGetOtherDB tLJAGetOtherDB = new LJAGetOtherDB();
     tLJAGetOtherSet = tLJAGetOtherDB.executeQuery(sqlStr);
     if (tLJAGetOtherDB.mErrors.needDealError() == true)
     {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJAGetOtherDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetOther";
      tError.errorMessage = "其他退费实付表查询失败!";
      this.mErrors.addOneError(tError);
      tLJAGetOtherSet.clear();
      return null;
     }
     LJAGetOtherSet newLJAGetOtherSet = new LJAGetOtherSet();
     for(int n=1;n<=tLJAGetOtherSet.size();n++)
     {
       tLJAGetOtherSchema = new LJAGetOtherSchema();
       tLJAGetOtherSchema = tLJAGetOtherSet.get(n);
       tLJAGetOtherSchema.setConfDate(mLJAGetBL.getConfDate());
       tLJAGetOtherSchema.setEnterAccDate(mLJAGetBL.getEnterAccDate());
       tLJAGetOtherSchema.setModifyDate(CurrentDate);
       tLJAGetOtherSchema.setModifyTime(CurrentTime);
       newLJAGetOtherSet.add(tLJAGetOtherSchema);
     }
     if (newLJAGetOtherSet.size()==0)
     {
     // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetOther";
      tError.errorMessage="没有可以更新的其他退费实付表！";
      this.mErrors .addOneError(tError) ;
      return null;
      }
     return newLJAGetOtherSet;

  }

  /**
   * add By Houyd 社保通调查费结算细目表在财务付费结束后状态的改变
   * @return
   */
  private SocialSecurityFeeClaimSet updateSocialSecurityFeeClaim(LJAGetBL mLJAGetBL) {
	  
	  if(mLJAGetBL==null)
	    {
	      // @@错误处理
	       CError tError = new CError();
	       tError.moduleName = "OperFinFeeGetBL";
	       tError.functionName = "updateLJAGetOther";
	       tError.errorMessage="传入参数不能为空！";
	       this.mErrors .addOneError(tError) ;
	       return null;
	    }

      String sqlStr="select * from SocialSecurityFeeClaim where ActuGetNo='"+mLJAGetBL.getActuGetNo()+"' with ur";
      System.out.println("社保通调查费结算细目表SQL:"+sqlStr);
      SocialSecurityFeeClaimSet tSocialSecurityFeeClaimSet = new SocialSecurityFeeClaimSet();
      SocialSecurityFeeClaimSchema tSocialSecurityFeeClaimSchema = new SocialSecurityFeeClaimSchema();
      SocialSecurityFeeClaimDB tSocialSecurityFeeClaimDB = new SocialSecurityFeeClaimDB();
      tSocialSecurityFeeClaimSet = tSocialSecurityFeeClaimDB.executeQuery(sqlStr);
      if (tSocialSecurityFeeClaimDB.mErrors.needDealError() == true)
      {
          // @@错误处理
          this.mErrors.copyAllErrors(tSocialSecurityFeeClaimDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "OperFinFeeGetBL";
          tError.functionName = "updateSocialSecurityFeeClaim";
          tError.errorMessage = "社保通调查费结算细目表查询失败!";
          this.mErrors.addOneError(tError);
          tSocialSecurityFeeClaimSet.clear();
          return null;
      }
      SocialSecurityFeeClaimSet newSocialSecurityFeeClaimSet = new SocialSecurityFeeClaimSet();
      for(int n=1;n<=tSocialSecurityFeeClaimSet.size();n++)
      {
    	  tSocialSecurityFeeClaimSchema = new SocialSecurityFeeClaimSchema();
    	  tSocialSecurityFeeClaimSchema = tSocialSecurityFeeClaimSet.get(n);
    	  tSocialSecurityFeeClaimSchema.setConfDate(mLJAGetBL.getConfDate());
    	  tSocialSecurityFeeClaimSchema.setEnterAccDate(mLJAGetBL.getEnterAccDate());
    	  tSocialSecurityFeeClaimSchema.setSettlementState("02");//财务付费后，将状态置为02
    	  tSocialSecurityFeeClaimSchema.setModifyDate(CurrentDate);
    	  tSocialSecurityFeeClaimSchema.setModifyTime(CurrentTime);
    	  newSocialSecurityFeeClaimSet.add(tSocialSecurityFeeClaimSchema);
      }
      if(newSocialSecurityFeeClaimSet.size()==0){
          CError tError = new CError();
          tError.moduleName = "BatchPayBL";
          tError.functionName = "updateSocialSecurityFeeClaim";
          tError.errorMessage="没有可以更新的社保通调查费结算细目表！";
          this.mErrors .addOneError(tError) ;
          return null;
      }	                    
	      return newSocialSecurityFeeClaimSet;	  
  }

 /**
   * 赔付实付表
   * @param LJAGetBL
   * @return
   */
  private LJAGetClaimSet updateLJAGetClaim(LJAGetBL mLJAGetBL)
   {
    if(mLJAGetBL==null)
    {
      // @@错误处理
       CError tError = new CError();
       tError.moduleName = "OperFinFeeGetBL";
       tError.functionName = "updateLJAGetClaim";
       tError.errorMessage="传入参数不能为空！";
       this.mErrors .addOneError(tError) ;
       return null;
    }
    String sqlStr="select * from LJAGetClaim where ActuGetNo='"+mLJAGetBL.getActuGetNo()+"'";
 System.out.println("赔付实付表:"+sqlStr);
     LJAGetClaimSet tLJAGetClaimSet = new LJAGetClaimSet();
     LJAGetClaimSchema tLJAGetClaimSchema = new LJAGetClaimSchema();
     LJAGetClaimDB tLJAGetClaimDB = new LJAGetClaimDB();
     tLJAGetClaimSet = tLJAGetClaimDB.executeQuery(sqlStr);
     if (tLJAGetClaimDB.mErrors.needDealError() == true)
     {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJAGetClaimDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetClaim";
      tError.errorMessage = "赔付实付表查询失败!";
      this.mErrors.addOneError(tError);
      tLJAGetClaimSet.clear();
      return null;
     }
     LJAGetClaimSet newLJAGetClaimSet = new LJAGetClaimSet();
     for(int n=1;n<=tLJAGetClaimSet.size();n++)
     {
       tLJAGetClaimSchema = new LJAGetClaimSchema();
       tLJAGetClaimSchema = tLJAGetClaimSet.get(n);
       tLJAGetClaimSchema.setConfDate(mLJAGetBL.getConfDate());
       tLJAGetClaimSchema.setEnterAccDate(mLJAGetBL.getEnterAccDate());
       tLJAGetClaimSchema.setModifyDate(CurrentDate);
       tLJAGetClaimSchema.setModifyTime(CurrentTime);
       newLJAGetClaimSet.add(tLJAGetClaimSchema);
     }
     if (newLJAGetClaimSet.size()==0)
     {
     // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetClaim";
      tError.errorMessage="没有可以更新的赔付实付表！";
      this.mErrors .addOneError(tError) ;
      return null;
      }
     return newLJAGetClaimSet;

  }


 /**
   * 暂交费退费实付表
   * @param LJAGetBL
   * @return
   */
  private LJAGetTempFeeSet updateLJAGetTempFee(LJAGetBL mLJAGetBL)
   {
    if(mLJAGetBL==null)
    {
      // @@错误处理
       CError tError = new CError();
       tError.moduleName = "OperFinFeeGetBL";
       tError.functionName = "updateLJAGetTempFee";
       tError.errorMessage="传入参数不能为空！";
       this.mErrors .addOneError(tError) ;
       return null;
    }
    String sqlStr="select * from LJAGetTempFee where ActuGetNo='"+mLJAGetBL.getActuGetNo()+"'";
 System.out.println("暂交费退费实付表:"+sqlStr);
     LJAGetTempFeeSet tLJAGetTempFeeSet = new LJAGetTempFeeSet();
     LJAGetTempFeeSchema tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
     LJAGetTempFeeDB tLJAGetTempFeeDB = new LJAGetTempFeeDB();
     tLJAGetTempFeeSet = tLJAGetTempFeeDB.executeQuery(sqlStr);
     if (tLJAGetTempFeeDB.mErrors.needDealError() == true)
     {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJAGetTempFeeDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetTempFee";
      tError.errorMessage = "暂交费退费实付表查询失败!";
      this.mErrors.addOneError(tError);
      tLJAGetTempFeeSet.clear();
      return null;
     }
     LJAGetTempFeeSet newLJAGetTempFeeSet = new LJAGetTempFeeSet();
     for(int n=1;n<=tLJAGetTempFeeSet.size();n++)
     {
       tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
       tLJAGetTempFeeSchema = tLJAGetTempFeeSet.get(n);
       tLJAGetTempFeeSchema.setConfDate(mLJAGetBL.getConfDate());
       tLJAGetTempFeeSchema.setEnterAccDate(mLJAGetBL.getEnterAccDate());
       tLJAGetTempFeeSchema.setModifyDate(CurrentDate);
       tLJAGetTempFeeSchema.setModifyTime(CurrentTime);
       newLJAGetTempFeeSet.add(tLJAGetTempFeeSchema);
     }
     if (newLJAGetTempFeeSet.size()==0)
     {
     // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJAGetTempFee";
      tError.errorMessage="没有可以更新的暂交费退费实付表！";
      this.mErrors .addOneError(tError) ;
      return null;
      }
     return newLJAGetTempFeeSet;

  }

 /**
   * 红利给付实付表
   * @param LJAGetBL
   * @return
   */
  private LJABonusGetSet updateLJABonusGet(LJAGetBL mLJAGetBL)
   {
    if(mLJAGetBL==null)
    {
      // @@错误处理
       CError tError = new CError();
       tError.moduleName = "OperFinFeeGetBL";
       tError.functionName = "updateLJABonusGet";
       tError.errorMessage="传入参数不能为空！";
       this.mErrors .addOneError(tError) ;
       return null;
    }
    String sqlStr="select * from LJABonusGet where ActuGetNo='"+mLJAGetBL.getActuGetNo()+"'";
 System.out.println("红利给付实付表:"+sqlStr);
     LJABonusGetSet tLJABonusGetSet = new LJABonusGetSet();
     LJABonusGetSchema tLJABonusGetSchema = new LJABonusGetSchema();
     LJABonusGetDB tLJABonusGetDB = new LJABonusGetDB();
     tLJABonusGetSet = tLJABonusGetDB.executeQuery(sqlStr);
     if (tLJABonusGetDB.mErrors.needDealError() == true)
     {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJABonusGetDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJABonusGet";
      tError.errorMessage = "红利给付实付表查询失败!";
      this.mErrors.addOneError(tError);
      tLJABonusGetSet.clear();
      return null;
     }
     LJABonusGetSet newLJABonusGetSet = new LJABonusGetSet();
     for(int n=1;n<=tLJABonusGetSet.size();n++)
     {
       tLJABonusGetSchema = new LJABonusGetSchema();
       tLJABonusGetSchema = tLJABonusGetSet.get(n);
       tLJABonusGetSchema.setConfDate(mLJAGetBL.getConfDate());
       tLJABonusGetSchema.setEnterAccDate(mLJAGetBL.getEnterAccDate());
       tLJABonusGetSchema.setModifyDate(CurrentDate);
       tLJABonusGetSchema.setModifyTime(CurrentTime);
       newLJABonusGetSet.add(tLJABonusGetSchema);
     }
     if (newLJABonusGetSet.size()==0)
     {
     // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLJABonusGet";
      tError.errorMessage="没有可以更新的红利给付实付表！";
      this.mErrors .addOneError(tError) ;
      return null;
      }
     return newLJABonusGetSet;

  }
  
  private LAChargeSet updateLAChargeSet(LJAGetBL mLJAGetBL)
  {
   if(mLJAGetBL==null)
   {
     // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OperFinFeeGetBL";
      tError.functionName = "updateLAChargeSet";
      tError.errorMessage="传入参数不能为空！";
      this.mErrors .addOneError(tError) ;
      return null;
   }
   String sqlStr="select * from LACharge where  receiptno='"+mLJAGetBL.getOtherNo()+"'";
System.out.println("销售银代或手续费实付表:"+sqlStr);
    LAChargeSet tLAChargeSet = new LAChargeSet();
    LAChargeSchema tLAChargeSchema = new LAChargeSchema();
    LAChargeDB tLAChargeDB = new LAChargeDB();
    tLAChargeSet = tLAChargeDB.executeQuery(sqlStr);
    if (tLAChargeDB.mErrors.needDealError() == true)
    {
     // @@错误处理
     this.mErrors.copyAllErrors(tLAChargeDB.mErrors);
     CError tError = new CError();
     tError.moduleName = "OperFinFeeGetBL";
     tError.functionName = "updateLAChargeSet";
     tError.errorMessage = "银代或手续费实付表查询失败!";
     this.mErrors.addOneError(tError);
     tLAChargeSet.clear();
     return null;
    }
    LAChargeSet newLAChargeSet = new LAChargeSet();
    for(int n=1;n<=tLAChargeSet.size();n++)
    {
      tLAChargeSchema = new LAChargeSchema();
      tLAChargeSchema = tLAChargeSet.get(n);
      tLAChargeSchema.setChargeState("3");
      //tLAChargeSchema.setModifyDate(CurrentDate);
      //tLAChargeSchema.setModifyTime(CurrentTime);
      newLAChargeSet.add(tLAChargeSchema);
    }
    if (newLAChargeSet.size()==0)
    {
    // @@错误处理
     CError tError = new CError();
     tError.moduleName = "OperFinFeeGetBL";
     tError.functionName = "updateLAChargeSet";
     tError.errorMessage="没有可以更新的销售实付表！";
     this.mErrors .addOneError(tError) ;
     return null;
     }
    return newLAChargeSet;

 }



}

