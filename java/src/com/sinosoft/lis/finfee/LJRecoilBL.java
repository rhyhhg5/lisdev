package com.sinosoft.lis.finfee;

import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.pubfun.*;

import java.util.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class LJRecoilBL  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData =new VData();
  /** 数据操作字符串 */
  private String mOperate;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private TransferData mTransferData;
  private String mTempfeeNo;
  private String mActuGetNo;
  private String mPayMode;
  private String mBankCode;
  private String mAccNo;
  private String mGetBankCode;
  private String mGetBankAccno;
  private Reflections mReflections=new Reflections();
  
  /** 提交数据的容器 */
  private MMap map = new MMap();

  //标志位
  String flag="";
  //Session信息
  private GlobalInput mG = new GlobalInput();

  //业务处理相关变量

  public LJRecoilBL() {
  }
  public static void main(String[] args) {
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    System.out.println("OperateData:  "+cOperate);


    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
    System.out.println("After getinputdata");

        
    //进行业务处理
    if (!dealData())
      return false;
    System.out.println("After dealData！");
    
    
//  准备往后台的数据
    if (!prepareOutputData()) return false;
    System.out.println("---End prepareOutputData---");

    System.out.println("Start PubSubmit BLS Submit...");
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mInputData, mOperate)) {
      // @@错误处理
      this.mErrors.copyAllErrors(tPubSubmit.mErrors);
      return false;
    }
    System.out.println("End PubSubmit BLS Submit...");    

    mInputData=null;
    return true;
  }

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData()
  {
    if(this.mOperate.equals("GET"))
    {
        if(!StrTool.cTrim(this.mTempfeeNo).equals("")){
            //插入反冲数据
            insertGetRecoilData();
            //插入正确数据
            insertGetRightData();
        }
    }else if(this.mOperate.equals("PAY")){
//      插入反冲数据
        insertPayRecoilData();
        //插入正确数据
        insertPayRightData();
    }
    return true ;
 }
  //从输入数据中得到所有对象
  //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData tInputData)
  {
      mG = (GlobalInput)tInputData.getObjectByObjectName("GlobalInput",0);
//    全局变量
      mTransferData = (TransferData) tInputData.getObjectByObjectName("TransferData",0);
      mTempfeeNo = (String) mTransferData.getValueByName("TempfeeNo");
      mActuGetNo= (String) mTransferData.getValueByName("ActuGetNo");
      mPayMode = (String) mTransferData.getValueByName("PayMode");
      mBankCode= (String) mTransferData.getValueByName("BankCode");
      mAccNo = (String) mTransferData.getValueByName("AccNo");
      mGetBankCode = (String) mTransferData.getValueByName("GetBankCode");
      mGetBankAccno = (String) mTransferData.getValueByName("GetBankAccno");
      
        
      return true;
  }

  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData()
  {
      try {
          mInputData.clear();
          mInputData.add(map);
      }catch(Exception ex) {
          // @@错误处理
          System.out.println(ex.getMessage());
          CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
          return false;
      }
    return true;
  }
//插入暂收反冲数据
  private boolean insertGetRecoilData(){
      LJTempFeeDB tLJTempFeeDB=new LJTempFeeDB();
      tLJTempFeeDB.setTempFeeNo(this.mTempfeeNo);
      LJTempFeeSet tLJTempFeeSet=tLJTempFeeDB.query();
      SysMaxNoPicch tSysMaxNoPicch=new SysMaxNoPicch();
      String tTempfeeNo=tSysMaxNoPicch.CreateMaxNo("FC", 13);
      tTempfeeNo="FC"+tTempfeeNo;
      for(int i=1;i<=tLJTempFeeSet.size();i++){
          LJTempFeeSchema tLJTempFeeSchema=new LJTempFeeSchema();
          mReflections.transFields(tLJTempFeeSchema, tLJTempFeeSet.get(i)); 
          tLJTempFeeSchema.setTempFeeNo(tTempfeeNo);
          tLJTempFeeSchema.setPayMoney(-tLJTempFeeSchema.getPayMoney());
          tLJTempFeeSchema.setConfMakeDate(this.CurrentDate);
          tLJTempFeeSchema.setModifyDate(this.CurrentDate);
          tLJTempFeeSchema.setModifyTime(this.CurrentTime);
          tLJTempFeeSchema.setOperator(this.mG.Operator);
          this.map.put(tLJTempFeeSchema, "INSERT");
      }
      LJTempFeeClassDB tLJTempFeeClassDB=new LJTempFeeClassDB();
      tLJTempFeeClassDB.setTempFeeNo(this.mTempfeeNo);
      LJTempFeeClassSet tLJTempFeeClassSet=tLJTempFeeClassDB.query();
      for(int i=1;i<=tLJTempFeeClassSet.size();i++){
          LJTempFeeClassSchema tLJTempFeeClassSchema=new LJTempFeeClassSchema();
          mReflections.transFields(tLJTempFeeClassSchema, tLJTempFeeClassSet.get(i)); 
          tLJTempFeeClassSchema.setTempFeeNo(tTempfeeNo);
          tLJTempFeeClassSchema.setPayMoney(-tLJTempFeeClassSchema.getPayMoney());
          tLJTempFeeClassSchema.setConfMakeDate(this.CurrentDate);
          tLJTempFeeClassSchema.setModifyDate(this.CurrentDate);
          tLJTempFeeClassSchema.setModifyTime(this.CurrentTime);
          tLJTempFeeClassSchema.setOperator(this.mG.Operator);
          this.map.put(tLJTempFeeClassSchema, "INSERT");
      }
      return true;
  }
 //插入暂收正确数据
  private boolean insertGetRightData(){
      LJTempFeeDB tLJTempFeeDB=new LJTempFeeDB();
      tLJTempFeeDB.setTempFeeNo(this.mTempfeeNo);
      LJTempFeeSet tLJTempFeeSet=tLJTempFeeDB.query();
      SysMaxNoPicch tSysMaxNoPicch=new SysMaxNoPicch();
      String tTempfeeNo=tSysMaxNoPicch.CreateMaxNo("FC", 13);
      tTempfeeNo="FC"+tTempfeeNo;
      for(int i=1;i<=tLJTempFeeSet.size();i++){
          LJTempFeeSchema tLJTempFeeSchema=new LJTempFeeSchema();
          mReflections.transFields(tLJTempFeeSchema, tLJTempFeeSet.get(i)); 
          tLJTempFeeSchema.setTempFeeNo(tTempfeeNo);
          tLJTempFeeSchema.setConfMakeDate(this.CurrentDate);
          tLJTempFeeSchema.setModifyDate(this.CurrentDate);
          tLJTempFeeSchema.setModifyTime(this.CurrentTime);
          tLJTempFeeSchema.setOperator(this.mG.Operator);
          this.map.put(tLJTempFeeSchema, "INSERT");
      }
      LJTempFeeClassDB tLJTempFeeClassDB=new LJTempFeeClassDB();
      tLJTempFeeClassDB.setTempFeeNo(this.mTempfeeNo);
      LJTempFeeClassSet tLJTempFeeClassSet=tLJTempFeeClassDB.query();
      for(int i=1;i<=tLJTempFeeClassSet.size();i++){
          LJTempFeeClassSchema tLJTempFeeClassSchema=new LJTempFeeClassSchema();
          mReflections.transFields(tLJTempFeeClassSchema, tLJTempFeeClassSet.get(i)); 
          tLJTempFeeClassSchema.setTempFeeNo(tTempfeeNo);
          tLJTempFeeClassSchema.setPayMode(this.mPayMode);
          tLJTempFeeClassSchema.setBankCode(this.mBankCode);
          tLJTempFeeClassSchema.setBankAccNo(this.mAccNo);
          tLJTempFeeClassSchema.setInsBankAccNo(this.mGetBankAccno);
          tLJTempFeeClassSchema.setInsBankCode(this.mGetBankCode);
          tLJTempFeeClassSchema.setConfMakeDate(this.CurrentDate);
          tLJTempFeeClassSchema.setModifyDate(this.CurrentDate);
          tLJTempFeeClassSchema.setModifyTime(this.CurrentTime);
          tLJTempFeeClassSchema.setOperator(this.mG.Operator);
          this.map.put(tLJTempFeeClassSchema, "INSERT");
      }
      return true;
  }
//插入实付反冲数据
  private boolean insertPayRecoilData(){
      LJAGetDB tLJAGetDB=new LJAGetDB();
      tLJAGetDB.setActuGetNo(this.mActuGetNo);
      LJAGetSet tLJAGetSet=tLJAGetDB.query();
      if(tLJAGetSet!=null && tLJAGetSet.size()>0){
          SysMaxNoPicch tSysMaxNoPicch=new SysMaxNoPicch();
          String tActuGetNo=tSysMaxNoPicch.CreateMaxNo("FC", 13);
          tActuGetNo="FC"+tActuGetNo;
          LJAGetSchema tLJAGetSchema=new LJAGetSchema();
          mReflections.transFields(tLJAGetSchema, tLJAGetSet.get(1)); 
          tLJAGetSchema.setActuGetNo(tActuGetNo);
          tLJAGetSchema.setSumGetMoney(-tLJAGetSchema.getSumGetMoney());
          tLJAGetSchema.setConfDate(this.CurrentDate);
          tLJAGetSchema.setModifyDate(this.CurrentDate);
          tLJAGetSchema.setModifyTime(this.CurrentTime);
          tLJAGetSchema.setOperator(this.mG.Operator);
          
          LJFIGetDB tLJFIGetDB=new LJFIGetDB();
          tLJFIGetDB.setActuGetNo(this.mActuGetNo);
          LJFIGetSet tLJFIGetSet=tLJFIGetDB.query();
          LJFIGetSchema tLJFIGetSchema=new LJFIGetSchema();
          if(tLJFIGetSet!=null && tLJFIGetSet.size()>0){
              mReflections.transFields(tLJFIGetSchema, tLJFIGetSet.get(1)); 
              tLJFIGetSchema.setActuGetNo(tActuGetNo);
              tLJFIGetSchema.setGetMoney(-tLJFIGetSchema.getGetMoney());
              tLJFIGetSchema.setConfDate(this.CurrentDate);
              tLJFIGetSchema.setModifyDate(this.CurrentDate);
              tLJFIGetSchema.setModifyTime(this.CurrentTime);
              tLJFIGetSchema.setOperator(this.mG.Operator);
          }else{
              this.mErrors.addOneError("数据存储错误");
              return false;
          }
          String tOtherNoType=tLJAGetSchema.getOtherNoType();
          if(tOtherNoType.equals("1")||tOtherNoType.equals("2")||tOtherNoType.equals("0"))
          {
             updateLJAGetDraw(tLJAGetSet.get(1),tActuGetNo,1);
             this.map.put(tLJAGetSchema, "INSERT");
             this.map.put(tLJFIGetSchema, "INSERT");
          }
          if(tOtherNoType.equals("3")||tOtherNoType.equals("10"))//更新 批改补退费表(实收/实付) LJAGetEndorse
          {
             updateLJAGetEndorse(tLJAGetSet.get(1),tActuGetNo,1);
             this.map.put(tLJAGetSchema, "INSERT");
             this.map.put(tLJFIGetSchema, "INSERT");
          }

          if(tOtherNoType.equals("4"))    //更新 暂交费退费实付表
          {
             updateLJAGetTempFee(tLJAGetSet.get(1),tActuGetNo,1);
             this.map.put(tLJAGetSchema, "INSERT");
             this.map.put(tLJFIGetSchema, "INSERT");
          }

          if(tOtherNoType.equals("5")||tOtherNoType.equals("F")||tOtherNoType.equals("C"))//更新 赔付实付表             LJAGetClaim
          {
             updateLJAGetClaim(tLJAGetSet.get(1),tActuGetNo,1);
             this.map.put(tLJAGetSchema, "INSERT");
             this.map.put(tLJFIGetSchema, "INSERT");
          }

          if(tOtherNoType.equals("6")||tOtherNoType.equals("8"))//更新 其他退费实付表         LJAGetOther
          {
             updateLJAGetOther(tLJAGetSet.get(1),tActuGetNo,1);
             this.map.put(tLJAGetSchema, "INSERT");
             this.map.put(tLJFIGetSchema, "INSERT");
          }

          if(tOtherNoType.equals("7"))  //更新 红利给付实付表         LJABonusGet
          {
             updateLJABonusGet(tLJAGetSet.get(1),tActuGetNo,1);
             this.map.put(tLJAGetSchema, "INSERT");
             this.map.put(tLJFIGetSchema, "INSERT");
          }
          
          if(tOtherNoType.equals("20"))  //更新 红利给付实付表         LJABonusGet
          {
             updateLJSGetDrawGet(tLJAGetSet.get(1),tActuGetNo,1);
             this.map.put(tLJAGetSchema, "INSERT");
             this.map.put(tLJFIGetSchema, "INSERT");
          }
      }
      return true;
  }
  //插入实付正确数据
  private boolean insertPayRightData(){
      LJAGetDB tLJAGetDB=new LJAGetDB();
      tLJAGetDB.setActuGetNo(this.mActuGetNo);
      LJAGetSet tLJAGetSet=tLJAGetDB.query();
      if(tLJAGetSet!=null && tLJAGetSet.size()>0){
          SysMaxNoPicch tSysMaxNoPicch=new SysMaxNoPicch();
          String tActuGetNo=tSysMaxNoPicch.CreateMaxNo("FC", 13);
          tActuGetNo="FC"+tActuGetNo;
          LJAGetSchema tLJAGetSchema=new LJAGetSchema();
          mReflections.transFields(tLJAGetSchema, tLJAGetSet.get(1)); 
          tLJAGetSchema.setActuGetNo(tActuGetNo);
          tLJAGetSchema.setPayMode(this.mPayMode);
          tLJAGetSchema.setSumGetMoney(tLJAGetSchema.getSumGetMoney());
          tLJAGetSchema.setBankCode(this.mBankCode);
          tLJAGetSchema.setBankAccNo(this.mAccNo);
          tLJAGetSchema.setInsBankCode(this.mGetBankCode);
          tLJAGetSchema.setInsBankAccNo(this.mGetBankAccno);
          tLJAGetSchema.setConfDate(this.CurrentDate);
          tLJAGetSchema.setModifyDate(this.CurrentDate);
          tLJAGetSchema.setModifyTime(this.CurrentTime);
          tLJAGetSchema.setOperator(this.mG.Operator);
          
          LJFIGetDB tLJFIGetDB=new LJFIGetDB();
          tLJFIGetDB.setActuGetNo(this.mActuGetNo);
          LJFIGetSet tLJFIGetSet=tLJFIGetDB.query();
          LJFIGetSchema tLJFIGetSchema=new LJFIGetSchema();
          if(tLJFIGetSet!=null && tLJFIGetSet.size()>0){
              mReflections.transFields(tLJFIGetSchema, tLJFIGetSet.get(1)); 
              tLJFIGetSchema.setActuGetNo(tActuGetNo);
              tLJFIGetSchema.setGetMoney(tLJFIGetSchema.getGetMoney());
              tLJFIGetSchema.setPayMode(this.mPayMode);
              tLJFIGetSchema.setBankCode(this.mBankCode);
              tLJFIGetSchema.setBankAccNo(this.mAccNo);
              tLJFIGetSchema.setConfDate(this.CurrentDate);
              tLJFIGetSchema.setModifyDate(this.CurrentDate);
              tLJFIGetSchema.setModifyTime(this.CurrentTime);
              tLJFIGetSchema.setOperator(this.mG.Operator);
          }else{
              this.mErrors.addOneError("数据存储错误");
              return false;
          }
          String tOtherNoType=tLJAGetSchema.getOtherNoType();
          if(tOtherNoType.equals("1")||tOtherNoType.equals("2")||tOtherNoType.equals("0"))
          {
             updateLJAGetDraw(tLJAGetSet.get(1),tActuGetNo,2);
             this.map.put(tLJAGetSchema, "INSERT");
             this.map.put(tLJFIGetSchema, "INSERT");
          }
          if(tOtherNoType.equals("3")||tOtherNoType.equals("10"))//更新 批改补退费表(实收/实付) LJAGetEndorse
          {
             updateLJAGetEndorse(tLJAGetSet.get(1),tActuGetNo,2);
             this.map.put(tLJAGetSchema, "INSERT");
             this.map.put(tLJFIGetSchema, "INSERT");
          }

          if(tOtherNoType.equals("4"))    //更新 暂交费退费实付表
          {
             updateLJAGetTempFee(tLJAGetSet.get(1),tActuGetNo,2);
             this.map.put(tLJAGetSchema, "INSERT");
             this.map.put(tLJFIGetSchema, "INSERT");
          }

          if(tOtherNoType.equals("5")||tOtherNoType.equals("F")||tOtherNoType.equals("C"))//更新 赔付实付表             LJAGetClaim
          {
             updateLJAGetClaim(tLJAGetSet.get(1),tActuGetNo,2);
             this.map.put(tLJAGetSchema, "INSERT");
             this.map.put(tLJFIGetSchema, "INSERT");
          }

          if(tOtherNoType.equals("6")||tOtherNoType.equals("8"))//更新 其他退费实付表         LJAGetOther
          {
             updateLJAGetOther(tLJAGetSet.get(1),tActuGetNo,2);
             this.map.put(tLJAGetSchema, "INSERT");
             this.map.put(tLJFIGetSchema, "INSERT");
          }

          if(tOtherNoType.equals("7"))  //更新 红利给付实付表         LJABonusGet
          {
             updateLJABonusGet(tLJAGetSet.get(1),tActuGetNo,2);
             this.map.put(tLJAGetSchema, "INSERT");
             this.map.put(tLJFIGetSchema, "INSERT");
          }
          if(tOtherNoType.equals("20"))  //更新 红利给付实付表         LJABonusGet
          {
             updateLJSGetDrawGet(tLJAGetSet.get(1),tActuGetNo,2);
             this.map.put(tLJAGetSchema, "INSERT");
             this.map.put(tLJFIGetSchema, "INSERT");
          }
      }
      return true;
  }
  
  private boolean updateLJAGetDraw(LJAGetSchema tLJAGetSchema,String newActuGetNo,int flag){
      LJAGetDrawDB tLJAGetDrawDB = new LJAGetDrawDB();
      tLJAGetDrawDB.setActuGetNo(tLJAGetSchema.getActuGetNo());
      LJAGetDrawSet tLJAGetDrawSet = tLJAGetDrawDB.query();
      for(int i=1;i<=tLJAGetDrawSet.size();i++){
          LJAGetDrawSchema tLJAGetDrawSchema = new LJAGetDrawSchema();
          mReflections.transFields(tLJAGetDrawSchema, tLJAGetDrawSet.get(i)); 
          tLJAGetDrawSchema.setActuGetNo(newActuGetNo);
          if(flag==1){//反冲标识
              tLJAGetDrawSchema.setGetMoney(-tLJAGetDrawSchema.getGetMoney());
              tLJAGetDrawSchema.setConfDate(this.CurrentDate);
              tLJAGetDrawSchema.setModifyDate(this.CurrentDate);
              tLJAGetDrawSchema.setModifyTime(this.CurrentTime);
              tLJAGetDrawSchema.setOperator(this.mG.Operator);
          }else if(flag==2){//正确标识
              tLJAGetDrawSchema.setConfDate(this.CurrentDate);
              tLJAGetDrawSchema.setModifyDate(this.CurrentDate);
              tLJAGetDrawSchema.setModifyTime(this.CurrentTime);
              tLJAGetDrawSchema.setOperator(this.mG.Operator);
          }
          this.map.put(tLJAGetDrawSchema, "INSERT");
      }
      return true;
  }
  private boolean updateLJAGetEndorse(LJAGetSchema tLJAGetSchema,String newActuGetNo,int flag){
      LJAGetEndorseDB tLJAGetEndorseDB = new LJAGetEndorseDB();
      tLJAGetEndorseDB.setActuGetNo(tLJAGetSchema.getActuGetNo());
      LJAGetEndorseSet tLJAGetEndorseSet = tLJAGetEndorseDB.query();
      for(int i=1;i<=tLJAGetEndorseSet.size();i++){
          LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
          mReflections.transFields(tLJAGetEndorseSchema, tLJAGetEndorseSet.get(i)); 
          tLJAGetEndorseSchema.setActuGetNo(newActuGetNo);
          if(flag==1){//反冲标识
              tLJAGetEndorseSchema.setGetMoney(-tLJAGetEndorseSchema.getGetMoney());
              tLJAGetEndorseSchema.setModifyDate(this.CurrentDate);
              tLJAGetEndorseSchema.setModifyTime(this.CurrentTime);
              tLJAGetEndorseSchema.setOperator(this.mG.Operator);
          }else if(flag==2){//正确标识
              tLJAGetEndorseSchema.setModifyDate(this.CurrentDate);
              tLJAGetEndorseSchema.setModifyTime(this.CurrentTime);
              tLJAGetEndorseSchema.setOperator(this.mG.Operator);
          }
          this.map.put(tLJAGetEndorseSchema, "INSERT");
      }
      return true;
  }
  private boolean updateLJAGetTempFee(LJAGetSchema tLJAGetSchema,String newActuGetNo,int flag){
      LJAGetTempFeeDB tLJAGetTempFeeDB = new LJAGetTempFeeDB();
      tLJAGetTempFeeDB.setActuGetNo(tLJAGetSchema.getActuGetNo());
      LJAGetTempFeeSet tLJAGetTempFeeSet = tLJAGetTempFeeDB.query();
      for(int i=1;i<=tLJAGetTempFeeSet.size();i++){
          LJAGetTempFeeSchema tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
          mReflections.transFields(tLJAGetTempFeeSchema, tLJAGetTempFeeSet.get(i)); 
          tLJAGetTempFeeSchema.setActuGetNo(newActuGetNo);
          if(flag==1){//反冲标识
              tLJAGetTempFeeSchema.setConfDate(this.CurrentDate);
              tLJAGetTempFeeSchema.setGetMoney(-tLJAGetTempFeeSchema.getGetMoney());
              tLJAGetTempFeeSchema.setModifyDate(this.CurrentDate);
              tLJAGetTempFeeSchema.setModifyTime(this.CurrentTime);
              tLJAGetTempFeeSchema.setOperator(this.mG.Operator);
          }else if(flag==2){//正确标识
              tLJAGetTempFeeSchema.setConfDate(this.CurrentDate);
              tLJAGetTempFeeSchema.setModifyDate(this.CurrentDate);
              tLJAGetTempFeeSchema.setModifyTime(this.CurrentTime);
              tLJAGetTempFeeSchema.setOperator(this.mG.Operator);
          }
          this.map.put(tLJAGetTempFeeSchema, "INSERT");
      }
      return true;
  }
  private boolean updateLJAGetClaim(LJAGetSchema tLJAGetSchema,String newActuGetNo,int flag){
      LJAGetClaimDB tLJAGetClaimDB = new LJAGetClaimDB();
      tLJAGetClaimDB.setActuGetNo(tLJAGetSchema.getActuGetNo());
      LJAGetClaimSet tLJAGetClaimSet = tLJAGetClaimDB.query();
      for(int i=1;i<=tLJAGetClaimSet.size();i++){
          LJAGetClaimSchema tLJAGetClaimSchema = new LJAGetClaimSchema();
          mReflections.transFields(tLJAGetClaimSchema, tLJAGetClaimSet.get(i)); 
          tLJAGetClaimSchema.setActuGetNo(newActuGetNo);
          if(flag==1){//反冲标识
              tLJAGetClaimSchema.setPay(-tLJAGetClaimSchema.getPay());
              tLJAGetClaimSchema.setConfDate(this.CurrentDate);
              tLJAGetClaimSchema.setModifyDate(this.CurrentDate);
              tLJAGetClaimSchema.setModifyTime(this.CurrentTime);
              tLJAGetClaimSchema.setOperator(this.mG.Operator);
          }else if(flag==2){//正确标识
              tLJAGetClaimSchema.setConfDate(this.CurrentDate);
              tLJAGetClaimSchema.setModifyDate(this.CurrentDate);
              tLJAGetClaimSchema.setModifyTime(this.CurrentTime);
              tLJAGetClaimSchema.setOperator(this.mG.Operator);
          }
          this.map.put(tLJAGetClaimSchema, "INSERT");
      }
      return true;
  }
  private boolean updateLJAGetOther(LJAGetSchema tLJAGetSchema,String newActuGetNo,int flag){
      LJAGetOtherDB tLJAGetOtherDB = new LJAGetOtherDB();
      tLJAGetOtherDB.setActuGetNo(tLJAGetSchema.getActuGetNo());
      LJAGetOtherSet tLJAGetOtherSet = tLJAGetOtherDB.query();
      for(int i=1;i<=tLJAGetOtherSet.size();i++){
          LJAGetOtherSchema tLJAGetOtherSchema = new LJAGetOtherSchema();
          mReflections.transFields(tLJAGetOtherSchema, tLJAGetOtherSet.get(i)); 
          tLJAGetOtherSchema.setActuGetNo(newActuGetNo);
          if(flag==1){//反冲标识
              tLJAGetOtherSchema.setGetMoney(-tLJAGetOtherSchema.getGetMoney());
              tLJAGetOtherSchema.setConfDate(this.CurrentDate);
              tLJAGetOtherSchema.setModifyDate(this.CurrentDate);
              tLJAGetOtherSchema.setModifyTime(this.CurrentTime);
              tLJAGetOtherSchema.setOperator(this.mG.Operator);
          }else if(flag==2){//正确标识
              tLJAGetOtherSchema.setConfDate(this.CurrentDate);
              tLJAGetOtherSchema.setModifyDate(this.CurrentDate);
              tLJAGetOtherSchema.setModifyTime(this.CurrentTime);
              tLJAGetOtherSchema.setOperator(this.mG.Operator);
          }
          this.map.put(tLJAGetOtherSchema, "INSERT");
      }
      return true;
  }
  private boolean updateLJABonusGet(LJAGetSchema tLJAGetSchema,String newActuGetNo,int flag){
      LJABonusGetDB tLJABonusGetDB = new LJABonusGetDB();
      tLJABonusGetDB.setActuGetNo(tLJAGetSchema.getActuGetNo());
      LJABonusGetSet tLJABonusGetSet = tLJABonusGetDB.query();
      for(int i=1;i<=tLJABonusGetSet.size();i++){
          LJABonusGetSchema tLJABonusGetSchema = new LJABonusGetSchema();
          mReflections.transFields(tLJABonusGetSchema, tLJABonusGetSet.get(i)); 
          tLJABonusGetSchema.setActuGetNo(newActuGetNo);
          if(flag==1){//反冲标识
              tLJABonusGetSchema.setGetMoney(-tLJABonusGetSchema.getGetMoney());
              tLJABonusGetSchema.setConfDate(this.CurrentDate);
              tLJABonusGetSchema.setModifyDate(this.CurrentDate);
              tLJABonusGetSchema.setModifyTime(this.CurrentTime);
              tLJABonusGetSchema.setOperator(this.mG.Operator);
          }else if(flag==2){//正确标识
              tLJABonusGetSchema.setConfDate(this.CurrentDate);
              tLJABonusGetSchema.setModifyDate(this.CurrentDate);
              tLJABonusGetSchema.setModifyTime(this.CurrentTime);
              tLJABonusGetSchema.setOperator(this.mG.Operator);
          }
          this.map.put(tLJABonusGetSchema, "INSERT");
      }
      return true;
  }
  private boolean updateLJSGetDrawGet(LJAGetSchema tLJAGetSchema,String newActuGetNo,int flag){
      LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
      tLJSGetDrawDB.setGetNoticeNo(tLJAGetSchema.getActuGetNo());
      LJSGetDrawSet tLJSGetDrawSet = tLJSGetDrawDB.query();
      for(int i=1;i<=tLJSGetDrawSet.size();i++){
          LJSGetDrawSchema tLJSGetDrawSchema = new LJSGetDrawSchema();
          mReflections.transFields(tLJSGetDrawSchema, tLJSGetDrawSet.get(i)); 
          tLJSGetDrawSchema.setGetNoticeNo(newActuGetNo);
          if(flag==1){//反冲标识
              tLJSGetDrawSchema.setGetMoney(-tLJSGetDrawSchema.getGetMoney());
              tLJSGetDrawSchema.setConfDate(this.CurrentDate);
              tLJSGetDrawSchema.setModifyDate(this.CurrentDate);
              tLJSGetDrawSchema.setModifyTime(this.CurrentTime);
              tLJSGetDrawSchema.setOperator(this.mG.Operator);
          }else if(flag==2){//正确标识
              tLJSGetDrawSchema.setConfDate(this.CurrentDate);
              tLJSGetDrawSchema.setModifyDate(this.CurrentDate);
              tLJSGetDrawSchema.setModifyTime(this.CurrentTime);
              tLJSGetDrawSchema.setOperator(this.mG.Operator);
          }
          this.map.put(tLJSGetDrawSchema, "INSERT");
      }
      
      LJSGetDB tLJSGetDB = new LJSGetDB();
      tLJSGetDB.setGetNoticeNo(tLJAGetSchema.getActuGetNo());
      LJSGetSet tLJSGetSet = tLJSGetDB.query();
      for(int i=1;i<=tLJSGetSet.size();i++){
          LJSGetSchema tLJSGetSchema = new LJSGetSchema();
          mReflections.transFields(tLJSGetSchema, tLJSGetSet.get(i)); 
          tLJSGetSchema.setGetNoticeNo(newActuGetNo);
          if(flag==1){//反冲标识
              tLJSGetSchema.setSumGetMoney(-tLJSGetSchema.getSumGetMoney());
              tLJSGetSchema.setModifyDate(this.CurrentDate);
              tLJSGetSchema.setModifyTime(this.CurrentTime);
              tLJSGetSchema.setOperator(this.mG.Operator);
          }else if(flag==2){//正确标识
              tLJSGetSchema.setModifyDate(this.CurrentDate);
              tLJSGetSchema.setModifyTime(this.CurrentTime);
              tLJSGetSchema.setOperator(this.mG.Operator);
          }
          this.map.put(tLJSGetSchema, "INSERT");
      }
      return true;
  }
}