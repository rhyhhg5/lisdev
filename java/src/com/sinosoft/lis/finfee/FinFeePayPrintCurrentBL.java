package com.sinosoft.lis.finfee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;

public class FinFeePayPrintCurrentBL {
	public FinFeePayPrintCurrentBL() {
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 全局变量 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private String mSDate = "";
    
	private String mEDate = "";
	
	private VData mInputData = new VData();

	private String mOperate = "";

	private PubFun mPubFun = new PubFun();

	private ListTable mListTable = new ListTable();

	private TransferData mTransferData = new TransferData();

	private XmlExport mXmlExport = null;

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {

		mOperate = cOperate;
		mInputData = (VData) cInputData;
		if (!mOperate.equals("PRINT")) {
			this.bulidErrorB("submitData", "数据不完整");
			return false;
		}

		// 得到外部传入的数据，将数据备份到本类中
		if (!getInputData(mInputData)) {
			return false;
		}

		// 进行数据查询
		if (!queryData()) {
			return false;
		}

		System.out.println("dayinchenggong1232121212121");

		return true;
	}

	private void bulidErrorB(String cFunction, String cErrorMsg) {

		CError tCError = new CError();

		tCError.moduleName = "LAMarketBL";
		tCError.functionName = cFunction;
		tCError.errorMessage = cErrorMsg;

		this.mErrors.addOneError(tCError);

	}

	/**
	 * 取得传入的数据 如果没有传入管理机构和起止如期则查全部机构全年的信息
	 * 
	 * @return boolean
	 */
	private boolean getInputData(VData cInputData) {

		try {
			mGlobalInput.setSchema((GlobalInput) cInputData
					.getObjectByObjectName("GlobalInput", 0));
			mTransferData = (TransferData) cInputData.getObjectByObjectName(
					"TransferData", 0);
			// 页面传入的数据 三个

			mSDate = (String) mTransferData.getValueByName("tStartDate");
			mEDate = (String) mTransferData.getValueByName("tEndDate");
		} catch (Exception ex) {
			this.mErrors.addOneError("");
			return false;
		}
		return true;
	}

	private boolean queryData() {
		TextTag tTextTag = new TextTag();
		mXmlExport = new XmlExport();
		// 设置模版名称
		mXmlExport.createDocument("FinFeePayPrintCurrent.vts", "printer");

		String tMakeDate = "";
		String tMakeTime = "";
		tMakeDate = mPubFun.getCurrentDate();
		tMakeTime = mPubFun.getCurrentTime();
		System.out.print("dayin252");

		tTextTag.add("MakeDate", tMakeDate);
		tTextTag.add("Operator", mGlobalInput.Operator);
		tTextTag.add("SDate", mSDate);
		tTextTag.add("EDate", mEDate);
		String mngName = "select name from ldcom where comcode='"+mGlobalInput.ManageCom+"' with ur";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(mngName);
		String mName = "";
		if(tSSRS!=null){
			mName = tSSRS.GetText(1, 1);
		}
		tTextTag.add("MngCom", mName);
		

		System.out.println("1212121" + tMakeDate);
		if (tTextTag.size() < 1) {
			return false;
		}
		mXmlExport.addTextTag(tTextTag);
		String[] title = { "", "", "", "", "", "", "" };
		mListTable.setName("FAULT");
		if (!getDataList()) {
			return false;
		}
		if (mListTable == null || mListTable.equals("")) {
			bulidErrorB("getDataList", "没有符合条件的信息!");
			return false;
		}

		System.out.println("111");
		mXmlExport.addListTable(mListTable, title);
		System.out.println("121");
		mXmlExport.outputDocumentToFile("c:\\", "new1");
		this.mResult.clear();
		mResult.addElement(mXmlExport);

		return true;
	}


	private boolean getDataList() {
		String manageCom = mGlobalInput.ManageCom;
		String sql = "select actugetno,"
				+ "(select bankname from ldbank where bankcode=LJFIGet.bankcode) bn,"
				+ "accname,bankaccno,"
				+ "(select bankname from ldbank where bankcode=(select insbankaccno from ljaget where actugetno=LJFIGet.actugetno)) insbn,"
				+ "(select codename from ldcode where codetype='paymode' and code=char(LJFIGet.paymode)) paymode,"
				+ "getmoney from LJFIGet where paymode<>'4' and ManageCom like '" + manageCom
				+ "%'  and ConfMakeDate>='" + mSDate + "' and ConfMakeDate<='" +
				mEDate+"' with ur";

		System.out.println(sql);
		SSRS tSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		tSSRS = tExeSQL.execSQL(sql);
		if (tSSRS.getMaxRow() > 0) {
			for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
				String ListInfo[] = new String[7];
				ListInfo[0] = tSSRS.GetText(i, 3);
				ListInfo[1] = tSSRS.GetText(i, 2);
				ListInfo[2] = tSSRS.GetText(i, 4);
				ListInfo[3] = tSSRS.GetText(i, 5);
				ListInfo[4] = tSSRS.GetText(i, 6);
				ListInfo[5] = tSSRS.GetText(i, 7);
				ListInfo[6] = tSSRS.GetText(i, 1);
				mListTable.add(ListInfo);

			}

		} else if (tSSRS.getMaxRow() <= 0) {
			String ListInfo[] = new String[7];
			ListInfo[0] = "";
			ListInfo[1] = "";
			ListInfo[2] = "";
			ListInfo[3] = "";
			ListInfo[4] = "";
			ListInfo[5] = "";
			ListInfo[6] = "";
			mListTable.add(ListInfo);
		}
		return true;
	}

	/**
	 * @return VData
	 */
	public VData getResult() {
		return mResult;
	}

	private void jbInit() throws Exception {
	}

}
