package com.sinosoft.lis.finfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.LDBankSet;

public class BatchInputReadFromExcelUI
{
    /** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 传出数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private LDBankSet outLDBankSet = new LDBankSet();
    public BatchInputReadFromExcelUI() {
    }

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"READ"和""
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)    {
      // 数据操作字符串拷贝到本类中
      this.mInputData = (VData)cInputData.clone();
      this.mOperate = cOperate;

      System.out.println("---BatchInputReadFromFile BL BEGIN---");
      BatchInputReadFromExcelBL tBatchInputReadFromExcelBL = new BatchInputReadFromExcelBL();

      if(tBatchInputReadFromExcelBL.submitData(mInputData, mOperate) == false)   {
        // @@错误处理
        this.mErrors.copyAllErrors(tBatchInputReadFromExcelBL.mErrors);
        mResult.clear();
        mResult.add(mErrors.getFirstError());
        return false;
      } else {
//        mResult = tBatchInputReadFromExcelBL.getResult();
        outLDBankSet = tBatchInputReadFromExcelBL.getLDBankSet();
      }
      System.out.println("---BatchInputReadFromFile BL END---");

      return true;
    }
    public LDBankSet getLDBankSet() {
        System.out.println("outLDBankSet:"+outLDBankSet.size());
        return outLDBankSet;
      }
    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult() {
      return mResult;
    }

    public static void main(String[] args) {
      BatchInputReadFromExcelUI tBatchInputReadFromExcelUI = new BatchInputReadFromExcelUI();

      TransferData transferData1 = new TransferData();
      transferData1.setNameAndValue("fileName", "D:\\新人培训-基础\\新接口验收20080630\\银行信息批量导入\\模板.xls");

      GlobalInput tGlobalInput = new GlobalInput();
      tGlobalInput.Operator = "001";

      VData inVData = new VData();
      inVData.add(transferData1);
      inVData.add(tGlobalInput);

      if (!tBatchInputReadFromExcelUI.submitData(inVData, "READ")) {
        //VData rVData = tBatchInputReadFromExcelUI.getResult();
        //System.out.println("Submit Failed! " + (String)rVData.get(0));
      }
      else System.out.println("Submit Succed!");
    }

}
