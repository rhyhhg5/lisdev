package com.sinosoft.lis.finfee;

import com.sinosoft.utility.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.JdbcUrl;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class OperFinFeeGetBLS  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 数据操作字符串 */
  private String mOperate;

  public OperFinFeeGetBLS() {
  }
  public static void main(String[] args) {
    OperFinFeeGetBLS mOperFinFeeGetBLS1 = new OperFinFeeGetBLS();
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate,String mLjagetendorse)
  {
    boolean tReturn =false;
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    System.out.println("Start OperFinFeeGet BLS Submit...");
    //信息保存
    if(this.mOperate.equals("VERIFY"))
    {tReturn=save(cInputData,mLjagetendorse);}

    if (tReturn)
      System.out.println("Save sucessful");
    else
      System.out.println("Save failed") ;

      System.out.println("End OperFinFeeGet BLS Submit...");

    return tReturn;
  }

//保存操作
  private boolean save(VData mInputData,String mljagetendorse)
  {
   boolean tReturn =true;
    System.out.println("Start Save...");
    Connection conn = DBConnPool.getConnection();
    if (conn==null)
    {
    		// @@错误处理
    		CError tError = new CError();
		tError.moduleName = "OperFinFeeGetBLS";
                tError.functionName = "saveData";
	        tError.errorMessage = "数据库连接失败!";
		this.mErrors .addOneError(tError) ;
 		return false;
    }
    try{
      conn.setAutoCommit(false);

 /** 财务给付表*/
      System.out.println("Start 财务给付表...");
      LJFIGetDB tLJFIGetDB=new LJFIGetDB(conn);
      tLJFIGetDB.setSchema((LJFIGetBL)mInputData.getObjectByObjectName("LJFIGetBL",0));
      if (!tLJFIGetDB.insert())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJFIGetDB.mErrors);
    		CError tError = new CError();
	        tError.moduleName = "OperFinFeeGetBLS";
	        tError.functionName = "saveData";
	        tError.errorMessage = "财务给付表数据保存失败!";
	        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 	        return false;
      }
      System.out.println("财务给付表 ok ");

 /** 实付总表 */

      System.out.println("Start 实付总表...");
      LJAGetDB tLJAGetDB=new LJAGetDB(conn);
      tLJAGetDB.setSchema((LJAGetBL)mInputData.getObjectByObjectName("LJAGetBL",0));
      System.out.println("Get LJAGet");
      if (!tLJAGetDB.update())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJAGetDB.mErrors);
    		CError tError = new CError();
		tError.moduleName = "OperFinFeeGetBLS";
		tError.functionName = "saveData";
		tError.errorMessage = "实付总表数据保存失败!";
		this.mErrors .addOneError(tError) ;
        conn.rollback();
        conn.close();
 		return false;
      }
      System.out.println("实付总表 ok");

 /**更新子表 */
     System.out.println("Start 子表...");
     String flag=(String)mInputData.getObjectByObjectName("String",0);
     System.out.println("flag :" +flag);
     if(flag.equals("1"))// 给付表(生存领取_实付)
     {
       LJAGetDrawDBSet mLJAGetDrawDBSet=new LJAGetDrawDBSet(conn);
       mLJAGetDrawDBSet.set((LJAGetDrawSet)mInputData.getObjectByObjectName("LJAGetDrawSet",0));
       if(mLJAGetDrawDBSet!=null)
       {
          if (!mLJAGetDrawDBSet.update())
          {
                   // @@错误处理
           this.mErrors.copyAllErrors(mLJAGetDrawDBSet.mErrors);
           CError tError = new CError();
           tError.moduleName = "OperFinFeeGetBLS";
           tError.functionName = "save";
           tError.errorMessage = "更新 给付表(生存领取_实付)失败!";
           this.mErrors .addOneError(tError) ;
           conn.rollback() ;
           conn.close();
           return false;
         }
       }
     }
     if(flag.equals("3")) //批改补退费表(实收/实付)
     {
    	 //String updatesql=(String)mInputData.getObjectByObjectName("String",1);
    	 String updatesql=mljagetendorse;
    	 System.out.println("updatesql :" +updatesql);
    	 ExeSQL tExeSQL = new ExeSQL();
    	 if(!tExeSQL.execUpdateSQL(updatesql)){
    		 this.mErrors.copyAllErrors(tExeSQL.mErrors);
             CError tError = new CError();
             tError.moduleName = "OperFinFeeGetBLS";
             tError.functionName = "save";
             tError.errorMessage = "更新 批改补退费表(实收/实付)失败!";
             this.mErrors .addOneError(tError) ;
             conn.rollback() ;
             conn.close();
             return false;
    	 }
     }
     if(flag.equals("4"))//暂交费退费实付表
     {
       LJAGetTempFeeDBSet mLJAGetTempFeeDBSet=new LJAGetTempFeeDBSet(conn);
       mLJAGetTempFeeDBSet.set((LJAGetTempFeeSet)mInputData.getObjectByObjectName("LJAGetTempFeeSet",0));
       if(mLJAGetTempFeeDBSet!=null)
       {
          if (!mLJAGetTempFeeDBSet.update())
          {
                   // @@错误处理
           this.mErrors.copyAllErrors(mLJAGetTempFeeDBSet.mErrors);
           CError tError = new CError();
           tError.moduleName = "OperFinFeeGetBLS";
           tError.functionName = "save";
           tError.errorMessage = "更新 暂交费退费实付表失败!";
           this.mErrors .addOneError(tError) ;
           conn.rollback() ;
           conn.close();
           return false;
         }
       }
     }

     if(flag.equals("5"))//赔付实付表
     {
       LJAGetClaimDBSet mLJAGetClaimDBSet=new LJAGetClaimDBSet(conn);
       mLJAGetClaimDBSet.set((LJAGetClaimSet)mInputData.getObjectByObjectName("LJAGetClaimSet",0));
       if(mLJAGetClaimDBSet!=null)
       {
          if (!mLJAGetClaimDBSet.update())
          {
                   // @@错误处理
           this.mErrors.copyAllErrors(mLJAGetClaimDBSet.mErrors);
           CError tError = new CError();
           tError.moduleName = "OperFinFeeGetBLS";
           tError.functionName = "save";
           tError.errorMessage = "更新 赔付实付表失败!";
           this.mErrors .addOneError(tError) ;
           conn.rollback() ;
           conn.close();
           return false;
         }
       }
       
       LLCaseDBSet mLLCaseDBSet=new LLCaseDBSet(conn);
       mLLCaseDBSet.set((LLCaseSet)mInputData.getObjectByObjectName("LLCaseSet",0));
       if(mLLCaseDBSet!=null)
       {
          if (!mLLCaseDBSet.update())
          {
                   // @@错误处理
           this.mErrors.copyAllErrors(mLLCaseDBSet.mErrors);
           CError tError = new CError();
           tError.moduleName = "OperFinFeeGetBLS";
           tError.functionName = "save";
           tError.errorMessage = "更新 赔付实付表失败!";
           this.mErrors .addOneError(tError) ;
           conn.rollback() ;
           conn.close();
           return false;
         }
       }
       
       LLRegisterDBSet mLLRegisterDBSet=new LLRegisterDBSet(conn);
       mLLRegisterDBSet.set((LLRegisterSet)mInputData.getObjectByObjectName("LLRegisterSet",0));
       if(mLLRegisterDBSet!=null)
       {
          if (!mLLRegisterDBSet.update())
          {
                   // @@错误处理
           this.mErrors.copyAllErrors(mLLRegisterDBSet.mErrors);
           CError tError = new CError();
           tError.moduleName = "OperFinFeeGetBLS";
           tError.functionName = "save";
           tError.errorMessage = "更新 赔付实付表失败!";
           this.mErrors .addOneError(tError) ;
           conn.rollback() ;
           conn.close();
           return false;
         }
       }
       
       LLPrepaidClaimDBSet mLLPrepaidClaimDBSet=new LLPrepaidClaimDBSet(conn);
       mLLPrepaidClaimDBSet.set((LLPrepaidClaimSet)mInputData.getObjectByObjectName("LLPrepaidClaimSet",0));
       if(mLLPrepaidClaimDBSet!=null)
       {
          if (!mLLPrepaidClaimDBSet.update())
          {
                   // @@错误处理
           this.mErrors.copyAllErrors(mLLPrepaidClaimDBSet.mErrors);
           CError tError = new CError();
           tError.moduleName = "OperFinFeeGetBLS";
           tError.functionName = "save";
           tError.errorMessage = "更新 赔付实付表失败!";
           this.mErrors .addOneError(tError) ;
           conn.rollback() ;
           conn.close();
           return false;
         }
       }
     }
      if(flag.equals("6"))//其他退费实付表
     {
       LJAGetOtherDBSet mLJAGetOtherDBSet=new LJAGetOtherDBSet(conn);
       mLJAGetOtherDBSet.set((LJAGetOtherSet)mInputData.getObjectByObjectName("LJAGetOtherSet",0));
       if(mLJAGetOtherDBSet!=null)
       {
          if (!mLJAGetOtherDBSet.update())
          {
                   // @@错误处理
           this.mErrors.copyAllErrors(mLJAGetOtherDBSet.mErrors);
           CError tError = new CError();
           tError.moduleName = "OperFinFeeGetBLS";
           tError.functionName = "save";
           tError.errorMessage = "更新 其他退费实付表失败!";
           this.mErrors .addOneError(tError) ;
           conn.rollback() ;
           conn.close();
           return false;
         }
       }
     }

     if(flag.equals("7"))//红利给付实付表
     {
       LJABonusGetDBSet mLJABonusGetDBSet=new LJABonusGetDBSet(conn);
       mLJABonusGetDBSet.set((LJABonusGetSet)mInputData.getObjectByObjectName("LJABonusGetSet",0));
       if(mLJABonusGetDBSet!=null)
       {
          if (!mLJABonusGetDBSet.update())
          {
                   // @@错误处理
           this.mErrors.copyAllErrors(mLJABonusGetDBSet.mErrors);
           CError tError = new CError();
           tError.moduleName = "OperFinFeeGetBLS";
           tError.functionName = "save";
           tError.errorMessage = "更新 红利给付实付表失败!";
           this.mErrors .addOneError(tError) ;
           conn.rollback() ;
           conn.close();
           return false;
         }
       }
     }
     //add by houyd 社保通调查费结算细目表在财务付费结束后状态的改变,flag借用8
     if("8".equals(flag))//社保通调查费结算细目表
     {
       SocialSecurityFeeClaimDBSet mSocialSecurityFeeClaimDBSet=new SocialSecurityFeeClaimDBSet(conn);
       mSocialSecurityFeeClaimDBSet.set((SocialSecurityFeeClaimSet)mInputData.getObjectByObjectName("SocialSecurityFeeClaimSet",0));
       if(mSocialSecurityFeeClaimDBSet!=null)
       {
          if (!mSocialSecurityFeeClaimDBSet.update())
          {
           // @@错误处理
           this.mErrors.copyAllErrors(mSocialSecurityFeeClaimDBSet.mErrors);
           CError tError = new CError();
           tError.moduleName = "OperFinFeeGetBLS";
           tError.functionName = "save";
           tError.errorMessage = "更新 社保通调查费结算细目表失败!";
           this.mErrors .addOneError(tError) ;
           conn.rollback() ;
           conn.close();
           return false;
         }
       }
     }
 
     /*if(flag.equals("BC")||flag.equals("AC"))//其他退费实付表
     {
         LAChargeDBSet mLAChargeDBSet=new LAChargeDBSet(conn);
       mLAChargeDBSet.set((LAChargeSet)mInputData.getObjectByObjectName("LAChargeSet",0));
       if(mLAChargeDBSet!=null)
       {
          if (!mLAChargeDBSet.update())
          {
                   // @@错误处理
           this.mErrors.copyAllErrors(mLAChargeDBSet.mErrors);
           CError tError = new CError();
           tError.moduleName = "OperFinFeeGetBLS";
           tError.functionName = "save";
           tError.errorMessage = "更新 银代或手续费实付表失败!";
           this.mErrors .addOneError(tError) ;
           conn.rollback() ;
           conn.close();
           return false;
         }
       }
     }*/
     System.out.println("子表 ok");


      conn.commit() ;
      conn.close();
      System.out.println("事务完成");
    }
    catch (Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="OperFinFeeGetBLS";
      tError.functionName="submitData";
      tError.errorMessage=ex.toString();
      this.mErrors .addOneError(tError);
      try{conn.rollback() ;} catch(Exception e){}
      tReturn=false;
    }
    return tReturn;
  }
}