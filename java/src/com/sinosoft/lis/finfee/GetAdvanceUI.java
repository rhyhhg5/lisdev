package com.sinosoft.lis.finfee;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class GetAdvanceUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 数据操作字符串 */
    private String mOperate;

    public GetAdvanceUI()
    {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        GetAdvanceBL tGetAdvanceBL = new GetAdvanceBL();

        if (!tGetAdvanceBL.submitData(cInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tGetAdvanceBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "FinFeeGetQueryUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据查询失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

}