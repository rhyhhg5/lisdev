package com.sinosoft.lis.finfee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.f1j.ss.BookModelImpl;
import com.f1j.ss.ReadParams;
import com.sinosoft.lis.operfee.VerDuePayFeeQueryUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.taskservice.BqFinishTask;
import com.sinosoft.lis.taskservice.LLClaimFinishTask;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class TempFeeBatchReadFileBL
{
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private String mFilePath = "";

    private BookModelImpl book = new BookModelImpl();

    //业务数据
    private TransferData inTransferData = new TransferData();

    private String fileName = "";

    private int taskFlag4 = 0;

    private int taskFlag9 = 0;

    private LJTempFeeSet outLJTempFeeSet = new LJTempFeeSet();

    private LJTempFeeClassSet outLJTempFeeClassSet = new LJTempFeeClassSet();

    private GlobalInput tG = new GlobalInput();

    private List exList = new ArrayList();
    
    private String mContManageCom = null;

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"READ"和""
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
        {
            return false;
        }
        System.out.println("---End getInputData---");

        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        System.out.println("---End dealData---");

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * 
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            inTransferData = (TransferData) mInputData.getObjectByObjectName(
                    "TransferData", 0);
            tG = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput",
                    0);

            if (tG == null || inTransferData == null)
            {
                CError tError = new CError();
                tError.moduleName = "TempFeeBatchReadFileBL";
                tError.functionName = "checkList";
                tError.errorMessage = "接收数据失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "TempFeeBatchReadFileBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        try
        {
            //获取银行文件数据
            if (mOperate.equals("READ"))
            {
                //获取返回文件名称
                fileName = (String) inTransferData.getValueByName("fileName");
                fileName = fileName.replace('\\', '/');
                //读取银行返回文件，公共部分
                System.out.println("---开始读取文件---");
                if (!readTempFeeFile(fileName))
                {
                    return false;
                }

                if (!checkList())
                {
                    return false;
                }

                if (!setTempFee())
                {
                    return false;
                }

                VData tVData = new VData();
                tVData.clear();
                tVData.add(outLJTempFeeSet);
                tVData.add(outLJTempFeeClassSet);
                tVData.add(tG);

                TempFeeUI tTempFeeUI = new TempFeeUI();
                tTempFeeUI.submitData(tVData, "INSERT");

                CErrors tError = tTempFeeUI.mErrors;
                if (!tError.getFirstError().equals(""))
                {
                    CError tError2 = new CError();
                    tError2.moduleName = "TempFeeBatchReadFileBL";
                    tError2.functionName = "dealData";
                    tError2.errorMessage = "收费处理失败，原因:"
                            + tError.getFirstError();
                    this.mErrors.addOneError(tError2);
                    return false;
                }
                //调用保全确认
                if (taskFlag4 == 1)
                {
                    BqFinishTask tBqFinishTask = new BqFinishTask(tG.ManageCom);
                    tBqFinishTask.run();
                }
                //调用理赔确认
                if (taskFlag9 == 1)
                {
                    LLClaimFinishTask tLLClaimFinishTask = new LLClaimFinishTask(
                            tG.ManageCom);
                    tLLClaimFinishTask.run();
                }
            }
        }
        catch (Exception e)
        {
            // @@错误处理
            e.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "TempFeeBatchReadFileBL";
            tError.functionName = "dealData";
            tError.errorMessage = "数据处理错误:" + e.getMessage();
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 读取收费导入文件
     * @param fileName
     * @return
     */
    private boolean readTempFeeFile(String fileName)
    {
        try
        {
            mFilePath = fileName;
            book.initWorkbook();//初始
            book.read(mFilePath, new ReadParams());
            int tSheetNums = book.getNumSheets();//读去sheet的个数，就是一个excel包含几个sheet
            System.out.println("tSheetNums===" + tSheetNums);

            for (int sheetNum = 0; sheetNum < tSheetNums; sheetNum++)
            {
                book.setSheet(sheetNum);
                int tLastCol = book.getLastCol();//总共多少列，他的列数从０开始，比如有４列，那tLastCol的结果＝３
                int tLastRow = book.getLastRow();//总共多少行，他的行也是从０开始，比如有２行，那tLastRow的结果＝１
                System.out.println("tLastCol===" + tLastCol);
                System.out.println("tLastRow===" + tLastRow);
                String colValue = "";
                //按列循环
                for (int j = 1; j <= tLastRow; j++)
                {//循环总行数
                    Map tMap = new HashMap();
                    for (int i = 0; i <= tLastCol; i++)
                    {//循环总列数
                        if (book.getText(j, i) != null)
                        {
                            colValue = (String) book.getText(j, i).trim();
                        }
                        else
                        {
                            colValue = "";
                        }
                        switch (i)
                        {
                            case 0:
                                tMap.put("TempFeeType", colValue);
                                break;
                            case 1:
                                tMap.put("OtherNo", colValue);
                                break;
                            case 2:
                                tMap.put("TempFeeNo", colValue);
                                break;
                            case 3:
                                tMap.put("PayMode", colValue);
                                break;
                            case 4:
                                tMap.put("PayMoney", colValue);
                                break;
                            case 5:
                                tMap.put("PayDate", colValue);
                                break;
                            case 6:
                                tMap.put("ChequeNo", colValue);
                                break;
                            case 7:
                                tMap.put("ChequeDate", colValue);
                                break;
                            case 8:
                                tMap.put("InsBankCode", colValue);
                                break;
                            case 9:
                                tMap.put("InsBankAccNo", colValue);
                                break;
                        }

                    }
                    tMap.put("ManageCom", tG.ManageCom);
                    exList.add(tMap);
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "TempFeeBatchReadFileBL";
            tError.functionName = "readTempFeeFile";
            tError.errorMessage = "读取导入文件错误:" + ex.getMessage();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean checkList()
    {
        if (exList.size() == 0)
        {
            System.out.println("-----exList-size-0-----");
            CError tError = new CError();
            tError.moduleName = "TempFeeBatchReadFileBL";
            tError.functionName = "checkList";
            tError.errorMessage = "未发现导入文件中存在收费信息!";
            this.mErrors.addOneError(tError);
            return false;
        }

        for (int k = 0; k < exList.size(); k++)
        {
            Map tMap = (Map) exList.get(k);

            if (tMap.get("TempFeeNo").equals("")
                    || tMap.get("OtherNo").equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "TempFeeBatchReadFileBL";
                tError.functionName = "checkList";
                tError.errorMessage = "第" + (k + 1) + "笔数据，业务号码或暂收据号为空!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (!tMap.get("TempFeeType").toString().matches(
                    "1|2|4|9|11|30"))
            {
                CError tError = new CError();
                tError.moduleName = "TempFeeBatchReadFileBL";
                tError.functionName = "checkList";
                tError.errorMessage = "第" + (k + 1) + "笔数据，交费类型错误!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (!tMap.get("TempFeeType").toString().equals("4"))
            {
                taskFlag4 = 1;
            }
            if (!tMap.get("TempFeeType").toString().equals("9"))
            {
                taskFlag9 = 1;
            }
            if (!tMap.get("PayMode").toString().matches(
                    "1|2|3|5|6|11|12|13|16|17"))
            {
                CError tError = new CError();
                tError.moduleName = "TempFeeBatchReadFileBL";
                tError.functionName = "checkList";
                tError.errorMessage = "第" + (k + 1) + "笔数据，交费方式错误!";
                this.mErrors.addOneError(tError);
                return false;
            }
            try
            {
                if (Double.parseDouble(tMap.get("PayMoney").toString()) <= 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "checkList";
                    tError.errorMessage = "第" + (k + 1) + "笔数据，交费金额错误，金额必须大于零!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            catch (Exception ex)
            {
                CError tError = new CError();
                tError.moduleName = "TempFeeBatchReadFileBL";
                tError.functionName = "checkList";
                tError.errorMessage = "第" + (k + 1) + "笔数据，交费金额格式错误!";
                this.mErrors.addOneError(tError);
                return false;
            }

            if (!PubFun.checkDateForm(tMap.get("PayDate").toString()))
            {
                CError tError = new CError();
                tError.moduleName = "TempFeeBatchReadFileBL";
                tError.functionName = "checkList";
                tError.errorMessage = "第" + (k + 1) + "笔数据，交费日期格式错误！";
                this.mErrors.addOneError(tError);
                return false;
            }

            if (tMap.get("PayMode").toString().matches("14|15")
                    && Double.parseDouble(tMap.get("PayMoney").toString()) > 1000)
            {
                CError tError = new CError();
                tError.moduleName = "TempFeeBatchReadFileBL";
                tError.functionName = "checkList";
                tError.errorMessage = "第" + (k + 1) + "笔数据，营销员代收金额不能超过1000元!";
                this.mErrors.addOneError(tError);
                return false;
            }

            if (tMap.get("PayMode").toString().matches("11|12")
                    && PubFun.calInterval(tMap.get("PayDate").toString(),
                            PubFun.getCurrentDate(), "D") < 0)
            {
                CError tError = new CError();
                tError.moduleName = "TempFeeBatchReadFileBL";
                tError.functionName = "checkList";
                tError.errorMessage = "第" + (k + 1)
                        + "笔数据，交费方式为银行汇款、其他银行代收收费方式，交费日期不能超过今天！";
                this.mErrors.addOneError(tError);
                return false;
            }

            if (tMap.get("PayMode").toString().matches("2|3|6|11|12|15|16|17"))
            {
                if (tMap.get("InsBankCode").equals("")
                        || tMap.get("InsBankAccNo").equals(""))
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "checkList";
                    tError.errorMessage = "第" + (k + 1)
                            + "笔数据，交费方式为银行收费时，本方开户银行和本方银行帐号不能为空！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                else
                {
                    String bankSql = "select * from ldfinbank where bankaccno='"
                            + tMap.get("InsBankAccNo").toString()
                            + "' and bankcode='"
                            + tMap.get("InsBankCode").toString()
                            + "' and finflag='S' and managecom like '"
                            + tG.ManageCom + "%' with ur";
                    System.out.println(bankSql);
                    ExeSQL tExeSQL = new ExeSQL();
                    if (tExeSQL.execSQL(bankSql).MaxRow == 0)
                    {
                        CError tError = new CError();
                        tError.moduleName = "TempFeeBatchReadFileBL";
                        tError.functionName = "checkList";
                        tError.errorMessage = "第" + (k + 1) + "笔数据，本方银行帐号不存在";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
            }
            else
            {
                tMap.put("InsBankCode", "");
                tMap.put("InsBankAccNo", "");
            }

            if (tMap.get("PayMode").toString().matches("3|5|11|12")
                    && tMap.get("ChequeNo").equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "TempFeeBatchReadFileBL";
                tError.functionName = "checkList";
                tError.errorMessage = "第" + (k + 1)
                        + "笔数据，交费方式为转账支票、内部转账时，票据号码不能为空！";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        return true;
    }

    private boolean setTempFee()
    {
        for (int i = 0; i < exList.size(); i++)
        {
            Map tMap = (Map) exList.get(i);
            //团单新单收费
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = new SSRS();

            if (tMap.get("TempFeeType").toString().equals("1"))
            {
                String sql = "select 1 from lcgrpcont where prtno='"
                        + tMap.get("OtherNo").toString()
                        + "' and uwflag in ('9', '4') with ur";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，保单不存在或者没有核保通过";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                
                sql = "select tempfeeno from ljtempfee tf where tf.otherno='"
                        + tMap.get("OtherNo").toString()
                        + "' and exists (select 1 from ljtempfeeclass where tempfeeno=tf.tempfeeno and paymode='4')";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow != 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1)
                            + "笔数据，缴费方式是银行转帐，缴费凭证号是：" + tSSRS.GetText(1, 1);
                    this.mErrors.addOneError(tError);
                    return false;
                }
                sql = "select tempfeeno from ljtempfee tf where tf.otherno='"
                        + tMap.get("OtherNo").toString() + "'";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow != 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，已进行过收费，缴费凭证号是："
                            + tSSRS.GetText(1, 1);
                    this.mErrors.addOneError(tError);
                    return false;
                }

                sql = "select a.riskcode,b.riskname,sum(a.prem)-(Select nvl(Sum(Mo.Paymoney),0) From Ljtempfee Mo Where Mo.Riskcode = a.Riskcode And Mo.Otherno = '"
                        + tMap.get("OtherNo").toString()
                        + "' And Exists (Select 1 From Ljtempfeeclass Where Tempfeeno = Mo.Tempfeeno And Paymode = 'YS')) from lcgrppol a,lmrisk b where prtno='"
                        + tMap.get("OtherNo").toString()
                        + "' and a.riskcode=b.riskcode group by a.riskcode,b.riskname";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "未查到相关的保单险种信息";
                    this.mErrors.addOneError(tError);
                    return false;
                }

                String agSql = "select distinct a.managecom,a.AgentCode from lcgrpcont a where a.paymode<>'4' "
                        + "and a.signdate is null and appflag in ('0','9') "
                        + "and  (select count(1) from lcrnewstatelog where prtno=a.prtno)=0 "
                        + "and  (select count(1) from ljtempfee where enteraccdate is null and otherno=a.prtno)=0 and a.prtno='"
                        + tMap.get("OtherNo").toString()
                        + "' and a.managecom like '" + tG.ManageCom + "%' ";
                SSRS agSSRS = tExeSQL.execSQL(agSql);
                if (agSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，未查到相关的保单信息";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                
                mContManageCom = agSSRS.GetText(1, 1);
                
                tMap.put("AgentCode", agSSRS.GetText(1, 2));

                if (!setLJTempFeeSet(tSSRS, tMap))
                {
                    return false;
                }

                sql = "select prem - (select nvl(Sum(Mo.Paymoney),0) from ljtempfeeclass mo where exists (select 1 from ljtempfee where tempfeeno=mo.tempfeeno and otherno='"
                        + tMap.get("OtherNo").toString()
                        + "')),bankcode,bankaccno,accname from lcgrpcont,ldcode where prtno='"
                        + tMap.get("OtherNo").toString()
                        + "' and  codetype='paymode' and paymode=code";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "未查到相关的保单险种信息";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (!setLJTempFeeClassSet(tSSRS, tMap, i + 1))
                {
                    return false;
                }
            }
            //个单新单收费
            else if (tMap.get("TempFeeType").toString().equals("11"))
            {
                String sql = "select 1 from lccont where prtno='"
                        + tMap.get("OtherNo").toString()
                        + "' and signdate is null with ur";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1)
                            + "笔数据，保单已签单或尚未录单，不能进行收费操作";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                
                mContManageCom = tSSRS.GetText(1, 1);
                
                sql = "select tempfeeno from ljtempfee tf where tf.otherno='"
                        + tMap.get("OtherNo").toString()
                        + "' and exists (select 1 from ljtempfeeclass where tempfeeno=tf.tempfeeno and paymode='4')";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow != 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1)
                            + "笔数据，缴费方式是银行转帐，缴费凭证号是：" + tSSRS.GetText(1, 1);
                    this.mErrors.addOneError(tError);
                    return false;
                }
                sql = "select tempfeeno from ljtempfee tf where tf.tempfeeno='"
                        + tMap.get("TempFeeNo").toString() + "'";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow != 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1)
                            + "笔数据，暂收号码已经进行收费！缴费凭证号是:" + tSSRS.GetText(1, 1);
                    this.mErrors.addOneError(tError);
                    return false;
                }

                sql = "select '000000','',-LF_PayMoneyNo('"
                        + tMap.get("OtherNo").toString()
                        + "') from lcpol a where a.prtno='"
                        + tMap.get("OtherNo").toString()
                        + "' fetch first 1 rows only";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "未查到相关的保单险种信息";
                    this.mErrors.addOneError(tError);
                    return false;
                }

                String agSql = "select distinct a.managecom,a.AgentCode from lccont a where a.paymode<>'4' "
                        + "and a.signdate is null  "
                        + "and not exists(select 1 from lcrnewstatelog where grpcontno = '00000000000000000000' and prtno=a.prtno) "
                        + "and not exists (select 1 from ljtempfee where enteraccdate is null and otherno=a.prtno) "
                        + "and conttype = '1'  and a.prtno='"
                        + tMap.get("OtherNo").toString()
                        + "' and a.managecom like '" + tG.ManageCom + "%' ";
                SSRS agSSRS = tExeSQL.execSQL(agSql);
                if (agSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，未查到相关的保单信息";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                
                mContManageCom = agSSRS.GetText(1, 1);
                
                tMap.put("AgentCode", agSSRS.GetText(1, 2));

                if (!setLJTempFeeSet(tSSRS, tMap))
                {
                    return false;
                }

                sql = "select distinct -LF_PayMoneyNo('"
                        + tMap.get("OtherNo").toString()
                        + "'),a.bankcode,a.bankaccno,a.accname from lccont a,ldcode,lcinsured c ,lcpol d "
                        + "where a.prtno='"
                        + tMap.get("OtherNo").toString()
                        + "' and  ldcode.codetype='paymode' and a.paymode=code "
                        + " and a.contno=c.contno and a.contno=d.contno "
                        + " and c.insuredno=d.insuredno  and d.uwflag in ('4','9') "
                        + " group by a.paymode,ldcode.codename,a.bankcode,a.bankaccno,a.accname";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，未查到相关的保单信息";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (!setLJTempFeeClassSet(tSSRS, tMap, i + 1))
                {
                    return false;
                }
            }
            else if (tMap.get("TempFeeType").toString().equals("2")) //校验格式
            {
                String bCode = "";
                String agSql = "select a.managecom,'', "
                        + "a.AgentCode,getnoticeno,a.bankcode from lccont a ,ljspay b where a.contno=b.otherno "
                        + "and conttype='1' and b.othernotype='2' "
                        + "and not exists (select 1 from ljtempfee where enteraccdate is null and tempfeeno=b.getnoticeno) "
                        + "and a.paymode<>'4' and b.sumduepaymoney<>0 and b.otherno='"
                        + tMap.get("OtherNo").toString()
                        + "' and a.managecom like '"
                        + tG.ManageCom
                        + "%' union "
                        + "select a.managecom,'', "
                        + "a.AgentCode,b.getnoticeno,a.bankcode  from lcgrpcont a ,ljspaygrp b where a.grpcontno=b.grpcontno "
                        + " and a.paymode<>'4' and a.grpcontno<>'00000000000000000000' "
                        + "and not exists (select 1 from ljtempfee where enteraccdate is null and tempfeeno=b.getnoticeno) "
                        + " and b.sumduepaymoney<>0 and b.grpcontno='"
                        + tMap.get("OtherNo").toString()
                        + "' and a.managecom like '" + tG.ManageCom + "%' ";
                SSRS agSSRS = tExeSQL.execSQL(agSql);
                if (agSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，未查到相关的保单信息";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                
                mContManageCom = agSSRS.GetText(1, 1);
                
                if (!agSSRS.GetText(1, 4).trim().equals(
                        tMap.get("TempFeeNo").toString()))
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，交费通知书号码错误！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                bCode = agSSRS.GetText(1, 5).trim();
                tMap.put("AgentCode", agSSRS.GetText(1, 3));

                LJSPaySchema tLJSPaySchema = new LJSPaySchema();
                tLJSPaySchema.setGetNoticeNo(tMap.get("TempFeeNo").toString());
                VData tVData = new VData();
                tVData.add(tLJSPaySchema);
                VerDuePayFeeQueryUI tVerDuePayFeeQueryUI = new VerDuePayFeeQueryUI();
                if (!tVerDuePayFeeQueryUI.submitData(tVData, "QUERYDETAIL"))
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "查询失败，原因是: "
                            + tVerDuePayFeeQueryUI.mErrors.getError(0).errorMessage;
                    this.mErrors.addOneError(tError);
                    return false;
                }
                String sql = "select * from ljspay where getnoticeno='"
                        + tMap.get("TempFeeNo").toString() + "'";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，未查到相关应收数据!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                sql = "select * from ljspay where getnoticeno='"
                        + tMap.get("TempFeeNo").toString()
                        + "' and (bankonthewayFlag<>'1' or bankonthewayFlag is null) with ur";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1)
                    + "笔数据，已经存在银行转账数据，不能在页面进行交费 !";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                sql = "select '000000','',sumduepaymoney from ljspay where getnoticeno='"
                        + tMap.get("TempFeeNo").toString()
                        + "' and managecom like '"
                        + tG.ManageCom + "%' with ur";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，未查到相关应收数据!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (!setLJTempFeeSet(tSSRS, tMap))
                {
                    return false;
                }
            
                sql = "select sumduepaymoney,'" + bCode + "',bankaccno,accname from ljspay where getnoticeno='"
                        + tMap.get("TempFeeNo").toString()
                        + "' and managecom like '"
                        + tG.ManageCom + "%' with ur";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，未查到相关应收数据!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (!setLJTempFeeClassSet(tSSRS, tMap, i + 1))
                {
                    return false;
                }
            }
            else if (tMap.get("TempFeeType").toString().matches("4"))
            {
                String agSql = "select managecom,'', "
                        + "AgentCode,getnoticeno from ljspay where othernotype in ('3','10','13') "
                        + "and not exists (select 1 from ljtempfee where tempfeeno=ljspay.getnoticeno) and bankcode is null "
                        + "and sumduepaymoney<>0 and otherno='"
                        + tMap.get("OtherNo").toString() + "' and managecom like '"
                        + tG.ManageCom + "%' ";
                SSRS agSSRS = tExeSQL.execSQL(agSql);
                if (agSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage =  "第" + (i + 1) + "笔数据，未查到相关的保单信息";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                
                mContManageCom = agSSRS.GetText(1, 1);
                
                if (!agSSRS.GetText(1, 4).trim().equals(
                        tMap.get("TempFeeNo").toString()))
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，交费收据号码错误！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                tMap.put("AgentCode", agSSRS.GetText(1, 3));

                String sql = "select '000000','',sumduepaymoney from ljspay where getnoticeno='"
                        + tMap.get("TempFeeNo").toString()
                        + "' and managecom like '" + tG.ManageCom + "%' with ur";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，未查到相关应收数据!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (!setLJTempFeeSet(tSSRS, tMap))
                {
                    return false;
                }

                sql = "select sumduepaymoney,bankcode,bankaccno,accname from ljspay where getnoticeno='"
                        + tMap.get("TempFeeNo").toString()
                        + "' and managecom like '" + tG.ManageCom + "%' with ur";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (!setLJTempFeeClassSet(tSSRS, tMap, i + 1))
                {
                    return false;
                }
            }
            else if (tMap.get("TempFeeType").toString().matches("9"))
            {
                String agSql = "select a.managecom,'' name,"
                        + " AgentCode,getnoticeno from LJSPay a "
                        + " where a.otherno='"
                        + tMap.get("OtherNo").toString()
                        + "' and managecom like '" + tG.ManageCom + "%' with ur";
                SSRS agSSRS = tExeSQL.execSQL(agSql);
                if (agSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，未查到相关的保单信息";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                
                mContManageCom = agSSRS.GetText(1, 1);
                
                if (!agSSRS.GetText(1, 4).trim().equals(
                        tMap.get("TempFeeNo").toString()))
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，交费收据号码错误！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                tMap.put("AgentCode", agSSRS.GetText(1, 3));

                String sql = "select '000000','',sumduepaymoney from ljspay where getnoticeno='"
                        + tMap.get("TempFeeNo").toString()
                        + "' and managecom like '" + tG.ManageCom + "%' with ur";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，未查到相关应收数据!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (!setLJTempFeeSet(tSSRS, tMap))
                {
                    return false;
                }

                sql = "select sumduepaymoney,bankcode,bankaccno,accname from ljspay where getnoticeno='"
                        + tMap.get("TempFeeNo").toString()
                        + "' and managecom like '" + tG.ManageCom + "%' with ur";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (!setLJTempFeeClassSet(tSSRS, tMap, i + 1))
                {
                    return false;
                }
            }
            else if (tMap.get("TempFeeType").toString().equals("10"))
            {
                String sql = "select * from ("
                        + " select b.riskcode,c.riskname,sum(a.SumDuePayMoney) "
                        + " from LJSPayGrp a,lcgrppol b,lmrisk c  "
                        + " where a.grppolno=b.grppolno and b.riskcode=c.riskcode and a.SerialNo='"
                        + tMap.get("OtherNo").toString()
                        + "'  group by b.riskcode,c.riskname"
                        + "  union "
                        + " select b.riskcode,c.riskname,sum(a.SumDuePayMoney) "
                        + " from LJSPayGrp a,lbgrppol b,lmrisk c  "
                        + " where a.grppolno=b.grppolno and b.riskcode=c.riskcode and a.SerialNo='"
                        + tMap.get("OtherNo").toString()
                        + "' group by b.riskcode,c.riskname" + ") as x with ur";
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，未找到收费相关信息!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                
                if (!setLJTempFeeSet(tSSRS, tMap))
                {
                    return false;
                }
                sql = "select sum(SumDuePayMoney),bankcode,bankaccno,accname from LJSPay where SerialNo='"
                        + tMap.get("OtherNo").toString() + "'";
                tSSRS = tExeSQL.execSQL(sql);
                if (!setLJTempFeeClassSet(tSSRS, tMap, i + 1))
                {
                    return false;
                }
            }
            else if (tMap.get("TempFeeType").toString().equals("6"))
            {
                String agSql = "select b.managecom,'', "
                        + "case substr(a.receivecom,1,1) when 'D' then substr(a.receivecom,2,10) else '' end  "
                        + "from lzcard a ,LZCardPay b, lzcardnumber c where a.state in ('2', '12') "
                        + "and a.subcode=b.cardtype and a.startno=b.startno and c.cardtype = b.cardtype "
                        + "and b.startno = c.cardserno "
                        + "and b.payno='"
                        + tMap.get("OtherNo").toString()
                        + "' and b.payno  not  in (select tempfeeno from ljtempfeeclass) "
                        + "and b.managecom like '" + tG.ManageCom + "%'";
                SSRS agSSRS = tExeSQL.execSQL(agSql);
                if (agSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，未查到相关的保单信息";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                
                mContManageCom = agSSRS.GetText(1, 1);
                
                tMap.put("AgentCode", agSSRS.GetText(1, 3));

                tExeSQL = new ExeSQL();
                String sql = "select distinct handlercode,agentcom from lzcardpay where payno='"
                        + tMap.get("OtherNo").toString() + "'";
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，结算批次号码不存在!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                sql = "select '1' from ljtempfee where tempfeetype='6' and not exists (select '1' from ljagettempfee where tempfeeno =ljtempfee.tempfeeno) and otherno='"
                        + tMap.get("OtherNo").toString() + "' with ur";
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow != 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，此结算批次号已经结算!";
                    this.mErrors.addOneError(tError);
                    return false;
                }

                sql = "select a.RiskCode, b.RiskName, (select sum(actpaymoney) from lzcardpay where payno='"
                        + tMap.get("OtherNo").toString()
                        + "') , a.PremProp, a.Premlot from LMCardRisk a, LMRisk b where a.riskcode=b.riskcode and CertifyCode in ( select a.CertifyCode from lzcard a,lzcardpay b where a.Startno=b.startno and a.Endno=b.Endno and a.subcode=b.cardtype and b.payno='"
                        + tMap.get("OtherNo").toString() + "')";
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，未找到收费相关信息!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (!setLJTempFeeSet(tSSRS, tMap))
                {
                    return false;
                }
                sql = "select (select sum(actpaymoney),'','','' from lzcardpay where payno='"
                        + tMap.get("OtherNo").toString()
                        + "') , a.PremProp, a.Premlot from LMCardRisk a, LMRisk b where a.riskcode=b.riskcode and CertifyCode in ( select a.CertifyCode from lzcard a,lzcardpay b where a.Startno=b.startno and a.Endno=b.Endno and a.subcode=b.cardtype and b.payno='"
                        + tMap.get("OtherNo").toString() + "')";
                tSSRS = tExeSQL.execSQL(sql);
                if (!setLJTempFeeClassSet(tSSRS, tMap, i + 1))
                {
                    return false;
                }
            }
            else if (tMap.get("TempFeeType").toString().equals("20"))
            {
                String agSql = "select  distinct managecom,'',"
                        + "agentcode,trim(PrtNo)||'801',PrtNo from HCCont where PrtNo='"
                        + tMap.get("OtherNo").toString()
                        + "' and managecom like '"
                        + tG.ManageCom
                        + "%' union all select distinct managecom,'',"
                        + "agentcode,trim(GrpPrtNo)||'801',GrpPrtNo from HCGrpCont where grpprtno='"
                        + tMap.get("OtherNo").toString() + "' and managecom like '"
                        + tG.ManageCom + "%' with ur";
                SSRS agSSRS = tExeSQL.execSQL(agSql);
                if (agSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，未查到相关的保单信息";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                
                mContManageCom = agSSRS.GetText(1, 1);
                
                tMap.put("AgentCode", agSSRS.GetText(1, 3));

                String sql = "select '1' from HCGrpCont a,HCGrpProduct b where a.GrpPrtNo='"
                        + tMap.get("TempFeeNo").toString()
                        + "' and a.GrpContNo=b.GrpContNo and a.ContState='2' and a.ContKind='01' and a.signdate is null "
                        + " union all "
                        + "select '1' from HCCont a,HCProduct b where a.PrtNo='"
                        + tMap.get("TempFeeNo").toString()
                        + "' and a.ContNo=b.ContNo and a.ContState='2' and a.ContKind='01'  with ur";
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，该保单不存在或者没有核保通过！";
                    this.mErrors.addOneError(tError);
                }
                sql = "select tempfeeno from ljtempfee where otherno='"
                        + tMap.get("OtherNo").toString() + "' ";
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow != 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1)
                    + "笔数据，该笔数据已进行收费！缴费凭证号：" + tSSRS.GetText(1, 1);
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (tMap.get("OtherNo").toString().substring(0, 2).equals("11"))
                {
                    sql = "select substr(b.ProductNo,1,6),'健管产品',decimal(sum(b.Prem),12,2) from "
                            + "HCCont a,HCProduct b where a.ContNo=b.ContNo and a.ContState='2' and a.ContKind='01' and a.prtno='"
                            + tMap.get("OtherNo").toString()
                            + "' group by substr(b.ProductNo,1,6) with ur";
                    System.out.println(sql);
                    tSSRS = tExeSQL.execSQL(sql);
                    if (tSSRS.MaxRow == 0)
                    {
                        CError tError = new CError();
                        tError.moduleName = "TempFeeBatchReadFileBL";
                        tError.functionName = "setTempFee";
                        tError.errorMessage = "第" + (i + 1) + "笔数据，未查到相关应收数据!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    if (!setLJTempFeeSet(tSSRS, tMap))
                    {
                        return false;
                    }

                    sql = "select distinct decimal(sum(b.Prem),12,2),a.BankCode,a.BankAccNo,a.AccName from "
                            + "HCCont a,HCProduct b,ldcode c where a.ContNo=b.ContNo and a.ContState='2' and a.ContKind='01' "
                            + "and c.codetype='paymode' and a.paymode=c.code and a.prtno='"
                            + tMap.get("OtherNo").toString()
                            + "' group by a.PayMode,c.codename,a.BankCode,a.BankAccNo,a.AccName with ur";
                    System.out.println(sql);
                    tSSRS = tExeSQL.execSQL(sql);
                    if (tSSRS.MaxRow == 0)
                    {
                        CError tError = new CError();
                        tError.moduleName = "TempFeeBatchReadFileBL";
                        tError.functionName = "setTempFee";
                        tError.errorMessage = "第" + (i + 1) + "笔数据，未查到相关应收数据!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    if (!setLJTempFeeClassSet(tSSRS, tMap, i + 1))
                    {
                        return false;
                    }
                }
                else if (tMap.get("OtherNo").toString().substring(0, 2).equals(
                        "12"))
                {
                    sql = "select substr(b.ProductNo,1,6) ProductNo,'健管产品',decimal(sum(b.Prem),12,2) from "
                            + "HCGrpCont a,HCGrpProduct b where a.GrpContNo=b.GrpContNo and a.ContState='2' and "
                            + "a.ContKind='01' and a.GrpPrtNo='"
                            + tMap.get("OtherNo").toString()
                            + "' group by substr(b.ProductNo,1,6) with ur";
                    System.out.println(sql);
                    tSSRS = tExeSQL.execSQL(sql);
                    if (tSSRS.MaxRow == 0)
                    {
                        CError tError = new CError();
                        tError.moduleName = "TempFeeBatchReadFileBL";
                        tError.functionName = "setTempFee";
                        tError.errorMessage = "第" + (i + 1) + "笔数据，未查到相关应收数据!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    if (!setLJTempFeeSet(tSSRS, tMap))
                    {
                        return false;
                    }

                    sql = "select distinct decimal(sum(b.Prem),12,2),a.BankCode,a.BankAccNo,a.AccName "
                            + "from HCGrpCont a,HCGrpProduct b,ldcode c where a.GrpContNo=b.GrpContNo and a.ContState='2' "
                            + "and a.ContKind='01' and c.codetype='paymode' and a.paymode=c.code and a.GrpPrtNo='"
                            + tMap.get("OtherNo").toString()
                            + "' group by a.PayMode,c.codename,a.BankCode,a.BankAccNo,a.AccName with ur";
                    System.out.println(sql);
                    tSSRS = tExeSQL.execSQL(sql);
                    if (tSSRS.MaxRow == 0)
                    {
                        CError tError = new CError();
                        tError.moduleName = "TempFeeBatchReadFileBL";
                        tError.functionName = "setTempFee";
                        tError.errorMessage = "第" + (i + 1) + "笔数据，未查到相关应收数据!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                    if (!setLJTempFeeClassSet(tSSRS, tMap, i + 1))
                    {
                        return false;
                    }
                }
                else
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，业务号码有误！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            else if (tMap.get("TempFeeType").toString().equals("25"))
            {
                String agSql = "select a.managecom,(select distinct name from ldcom where comcode=a.managecom) name, "
                        + "a.AgentCode,b.getnoticeno from lcgrpcont a ,ljspay b where a.grpcontno=b.otherno and b.othernotype='25'"
                        + "and not exists (select 1 from ljtempfee where enteraccdate is null and tempfeeno=b.getnoticeno) "
                        + " and b.sumduepaymoney<>0 and b.otherno='"
                        + tMap.get("OtherNo").toString()
                        + "' and exists (select 1 from LLCasePayAdvancedTrace c where c.getnoticeno=b.getnoticeno and c.dealstate='03') "
                        + "and b.managecom like '"
                        + tG.ManageCom
                        + "%'  union "
                        + "select a.managecom,(select distinct name from ldcom where comcode=a.managecom) name, "
                        + "a.AgentCode,b.getnoticeno from lbgrpcont a ,ljspay b where a.grpcontno=b.otherno and b.othernotype='25'"
                        + "and not exists (select 1 from ljtempfee where enteraccdate is null and tempfeeno=b.getnoticeno) "
                        + "and exists (select 1 from LLCasePayAdvancedTrace c where c.getnoticeno=b.getnoticeno and c.dealstate='03') "
                        + " and b.sumduepaymoney<>0 and b.otherno='"
                        + tMap.get("OtherNo").toString()
                        + "' and b.managecom like '"
                        + tG.ManageCom
                        + "%' with ur";
                SSRS agSSRS = tExeSQL.execSQL(agSql);
                if (agSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，未查到相关的保单信息";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                
                mContManageCom = agSSRS.GetText(1, 1);
                
                if (!agSSRS.GetText(1, 4).trim().equals(
                        tMap.get("TempFeeNo").toString()))
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，交费收据号码错误！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                tMap.put("AgentCode", agSSRS.GetText(1, 3));

                String sql = "select '000000' ProductNo,'社保报销款',decimal(a.sumduepaymoney,12,2) from ljspay a where  a.getnoticeno='"
                        + tMap.get("TempFeeNo").toString()
                        + "' and a.othernotype='25' and exists (select 1 from LLCasePayAdvancedTrace b where b.getnoticeno=a.getnoticeno and b.dealstate='03') with ur";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，未查到相关应收数据!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (!setLJTempFeeSet(tSSRS, tMap))
                {
                    return false;
                }
                sql = "select decimal(a.sumduepaymoney,12,2),bankcode,bankaccno,accname from ljspay a where  a.getnoticeno='"
                        + tMap.get("TempFeeNo").toString()
                        + "' and a.othernotype='25' and exists (select 1 from LLCasePayAdvancedTrace b where b.getnoticeno=a.getnoticeno and b.dealstate='03') with ur";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (!setLJTempFeeClassSet(tSSRS, tMap, i + 1))
                {
                    return false;
                }
            }
            else if (tMap.get("TempFeeType").toString().matches("30"))
            {
                String agSql = "select a.managecom,(select distinct name from ldcom where comcode=a.managecom) name, "
                        + "a.AgentCode,a.getnoticeno from ljspay a where a.othernotype='30'"
                        + "and not exists (select 1 from ljtempfee where enteraccdate is null and tempfeeno=a.getnoticeno) "
                        + " and a.sumduepaymoney<>0 and a.otherno='"
                        + tMap.get("OtherNo").toString()
                        + "' and a.managecom like '"
                        + tG.ManageCom
                        + "%' with ur";
                SSRS agSSRS = tExeSQL.execSQL(agSql);
                if (agSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，未查到相关的保单信息";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                
                mContManageCom = agSSRS.GetText(1, 1);
                
                if (!agSSRS.GetText(1, 4).trim().equals(
                        tMap.get("TempFeeNo").toString()))
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，交费收据号码错误！";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                tMap.put("AgentCode", agSSRS.GetText(1, 3));

                String sql = "select '000000','',sumduepaymoney from ljspay where getnoticeno='"
                        + tMap.get("TempFeeNo").toString()
                        + "' and managecom like '" + tG.ManageCom + "%' with ur";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (tSSRS.MaxRow == 0)
                {
                    CError tError = new CError();
                    tError.moduleName = "TempFeeBatchReadFileBL";
                    tError.functionName = "setTempFee";
                    tError.errorMessage = "第" + (i + 1) + "笔数据，未查到相关应收数据!";
                    this.mErrors.addOneError(tError);
                    return false;
                }
                if (!setLJTempFeeSet(tSSRS, tMap))
                {
                    return false;
                }

                sql = "select sumduepaymoney,bankcode,bankaccno,accname from ljspay where getnoticeno='"
                        + tMap.get("TempFeeNo").toString()
                        + "' and managecom like '" + tG.ManageCom + "%' with ur";
                System.out.println(sql);
                tSSRS = tExeSQL.execSQL(sql);
                if (!setLJTempFeeClassSet(tSSRS, tMap, i + 1))
                {
                    return false;
                }
            }

        }
        return true;
    }

    public boolean setLJTempFeeSet(SSRS tSSRS, Map tMap)
    {
        for (int row = 1; row <= tSSRS.MaxRow; row++)
        {
            if(tSSRS.MaxCol == 0){
                CError tError = new CError();
                tError.moduleName = "TempFeeBatchReadFileBL";
                tError.functionName = "setLJTempFeeSet";
                tError.errorMessage = "接收数据失败！";
                this.mErrors.addOneError(tError);
                return false;
            }
            LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
            tLJTempFeeSchema = new LJTempFeeSchema();
            tLJTempFeeSchema.setTempFeeNo(tMap.get("TempFeeNo").toString());
            tLJTempFeeSchema.setTempFeeType(tMap.get("TempFeeType").toString());
            //如果是预打保单交费，交费类型改成‘a’
            if (tMap.get("TempFeeType").toString().equals("8"))
            {
                tLJTempFeeSchema.setTempFeeType("a");
            }
            //如果是健管交费，判断是个单还是团单
            if (tMap.get("TempFeeType").toString().equals("20"))
            {
                if ((tMap.get("TempFeeType").toString().substring(0, 2))
                        .equals("12"))
                {
                    tLJTempFeeSchema.setTempFeeType("21");
                }
            }
            if (tSSRS.GetText(row, 1).trim().equals(""))
            {
                tLJTempFeeSchema.setRiskCode("000000");
            }
            else
            {
                tLJTempFeeSchema.setRiskCode(tSSRS.GetText(row, 1));
            }
            tLJTempFeeSchema.setAgentCode(tMap.get("AgentCode").toString());

            String agSql = "select agentgroup from laagent where agentcode='"
                    + tMap.get("AgentCode").toString() + "'";
            System.out.println(agSql);
            ExeSQL tExeSQL = new ExeSQL();
            SSRS agSSRS = tExeSQL.execSQL(agSql);
            if (agSSRS.MaxRow != 0)
            {
                tLJTempFeeSchema.setAgentGroup(agSSRS.GetText(1, 1));
            }

            tLJTempFeeSchema.setPayDate(tMap.get("PayDate").toString());
            tLJTempFeeSchema.setEnterAccDate(PubFun.getCurrentDate()); //到帐日期 有问题 需要调整
            tLJTempFeeSchema.setPayMoney(tSSRS.GetText(row, 3));
            String manCom = tMap.get("ManageCom").toString();
            int len = manCom.length();
            for (int i = 0; i < 8 - len; i++)
            {
                manCom = manCom + "0";
            }
            tLJTempFeeSchema.setManageCom(manCom);
            tLJTempFeeSchema.setPolicyCom(mContManageCom);
            tLJTempFeeSchema.setOtherNo(tMap.get("OtherNo").toString());
            if (tMap.get("TempFeeType").toString().equals("1"))
            {
                tLJTempFeeSchema.setOtherNoType("4");
            }
            else if (tMap.get("TempFeeType").toString().equals("11"))
            {
                tLJTempFeeSchema.setOtherNoType("4");
            }
            else if (tMap.get("TempFeeType").toString().equals("8"))
            {
                tLJTempFeeSchema.setOtherNoType("4");
            }
            else if (tMap.get("TempFeeType").toString().equals("2"))
            {
				String strSQL = "select OtherNoType from LJSPay where GetNoticeNo='"
						+ tMap.get("TempFeeNo") + "'";
				SSRS tempSSRS = tExeSQL.execSQL(strSQL);
				String othernotype = tempSSRS.GetText(1, 1);
				if ("2".equals(othernotype)) {
					tLJTempFeeSchema.setOtherNoType("0");
				} else {
					tLJTempFeeSchema.setOtherNoType(othernotype);
				}
			}
            else if (tMap.get("TempFeeType").toString().equals("10"))
            {
                tLJTempFeeSchema.setOtherNoType("11");
            }
            else if (tMap.get("TempFeeType").toString().equals("20"))
            {
                tLJTempFeeSchema.setOtherNoType("20");
            }
            else if (tMap.get("TempFeeType").toString().equals("25"))
            {
                tLJTempFeeSchema.setOtherNoType("25");
            }
            else if (tMap.get("TempFeeType").toString().equals("30"))
            {
                tLJTempFeeSchema.setOtherNoType("30");
            }
            tLJTempFeeSchema.setOperator(tG.Operator);
            tLJTempFeeSchema.setCashier(tG.Operator);
            outLJTempFeeSet.add(tLJTempFeeSchema);
        }
        return true;
    }

    public boolean setLJTempFeeClassSet(SSRS tSSRS, Map tMap, int i)
    {
        if(tSSRS.MaxCol == 0){
            CError tError = new CError();
            tError.moduleName = "TempFeeBatchReadFileBL";
            tError.functionName = "setLJTempFeeClassSet";
            tError.errorMessage = "第" + i + "笔数据，接收数据失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        String sql = "select 1 from ljtempfee tf where tf.tempfeeno='"
                + tMap.get("TempFeeNo").toString()
                + "' and exists (select 1 from ljtempfeeclass where tempfeeno=tf.tempfeeno and paymode='4')";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tempSSRS = tExeSQL.execSQL(sql);
        if (tempSSRS.MaxRow != 0)
        {
            CError tError = new CError();
            tError.moduleName = "TempFeeBatchReadFileBL";
            tError.functionName = "setTempFee";
            tError.errorMessage = "第" + i + "笔数据，该笔收费的收费方式为银行转账！";
            this.mErrors.addOneError(tError);
            return false;
        }
        sql = "select tempfeeno from ljtempfee tf where tf.tempfeeno='"
                + tMap.get("TempFeeNo").toString() + "'";
        tempSSRS = tExeSQL.execSQL(sql);
        if (tempSSRS.MaxRow != 0)
        {
            CError tError = new CError();
            tError.moduleName = "TempFeeBatchReadFileBL";
            tError.functionName = "setTempFee";
            tError.errorMessage = "第" + i + "笔数据，该笔收费暂收费号已存在！";
            this.mErrors.addOneError(tError);
            return false;
        }

        LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
        tLJTempFeeClassSchema.setTempFeeNo(tMap.get("TempFeeNo").toString());
        tLJTempFeeClassSchema.setPayMode(tMap.get("PayMode").toString());
        tLJTempFeeClassSchema.setPayDate(tMap.get("PayDate").toString());

        if (Double.parseDouble((tMap.get("PayMoney").toString())) == Double
                .parseDouble(tSSRS.GetText(1, 1)))
        {
            tLJTempFeeClassSchema.setPayMoney(tMap.get("PayMoney").toString());
        }
        else
        {
            CError tError = new CError();
            tError.moduleName = "TempFeeBatchReadFileBL";
            tError.functionName = "setTempFee";
            tError.errorMessage = "第" + i + "笔数据，导入金额与应收金额不等!";
            this.mErrors.addOneError(tError);
            return false;
        }

        tLJTempFeeClassSchema.setChequeNo(tMap.get("ChequeNo").toString());
        tLJTempFeeClassSchema.setChequeDate(tMap.get("ChequeDate").toString());
        if ((tMap.get("PayMode").toString().equals("11") || tMap.get("PayMode")
                .toString().equals("12")))
        {
            tLJTempFeeClassSchema.setEnterAccDate(tMap.get("PayDate")
                    .toString());
        }
        else
        {
            tLJTempFeeClassSchema.setEnterAccDate(PubFun.getCurrentDate());
        }
        String manCom = tMap.get("ManageCom").toString();
        int len = manCom.length();
        for (int k = 0; k < 8 - len; k++)
        {
            manCom = manCom + "0";
        }
        tLJTempFeeClassSchema.setManageCom(manCom);
        tLJTempFeeClassSchema.setPolicyCom(mContManageCom);
        tLJTempFeeClassSchema.setBankCode(tSSRS.GetText(1, 2));
        tLJTempFeeClassSchema.setBankAccNo(tSSRS.GetText(1, 3));
        tLJTempFeeClassSchema.setAccName(tSSRS.GetText(1, 4));
        tLJTempFeeClassSchema
                .setInsBankCode(tMap.get("InsBankCode").toString());
        tLJTempFeeClassSchema.setInsBankAccNo(tMap.get("InsBankAccNo")
                .toString());
        tLJTempFeeClassSchema.setOperator(tG.Operator);
        tLJTempFeeClassSchema.setCashier(tG.Operator);
        outLJTempFeeClassSet.add(tLJTempFeeClassSchema);
        return true;
    }

    /**
     *  返回查询条件，用于前台界面展示
     */
    public String getTempFeeNo()
    {
        if (exList.size() == 0)
        {
            return "('')";
        }
        String sql = "(";
        for (int i = 0; i < exList.size() - 1; i++)
        {
            sql = sql + "'" + ((Map) exList.get(i)).get("TempFeeNo").toString()
                    + "',";
        }
        sql = sql
                + "'"
                + ((Map) exList.get(exList.size() - 1)).get("TempFeeNo")
                        .toString() + "')";
        return sql;
    }

    public static void main(String[] arr)
    {
        VData tInputData = new VData();
        TransferData tTransferData = new TransferData();
        GlobalInput tGl = new GlobalInput();

        tGl.ManageCom = "8621";
        tGl.ComCode = "8621";
        tGl.Operator = "cwad";

        tTransferData.setNameAndValue("fileName", "F:\\导入数据.xls");
        tInputData.add(tGl);
        tInputData.add(tTransferData);
        
        TempFeeBatchReadFileBL tTempFeeBatchReadFileBL = new TempFeeBatchReadFileBL();
        tTempFeeBatchReadFileBL.submitData(tInputData, "READ");
        System.out.println(tTempFeeBatchReadFileBL.mErrors.getFirstError());
    }
}
