package com.sinosoft.lis.finfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.LJAGetDB;

/**
 * <p>Title: 付费管理处理程序</p>
 *
 * <p>Description: 锁定付费，解锁克付费，更改交费方式，更改个人银行信息</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author : zhangjun
 * @version 1.0
 */
public class PayManageConfirmBL {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public CErrors mErrors = new CErrors();

/** 往后面传输数据的容器 */
private VData mInputData= new VData();;

/** 往界面传输数据的容器 */
private VData mResult = new VData();
private TransferData mTransferData = new TransferData();
private LJAGetSchema mLJAGetSchema = new LJAGetSchema();
private LJAGetDB tLJAGetDB = new LJAGetDB();
private LJAGetSchema tLJAGetSchema = new LJAGetSchema();
private GlobalInput mGlobalInput = new GlobalInput();
private String mOperate;
private String mActuGetNo="";
public PayManageConfirmBL() {}
    /**
    * 传输数据的公共方法
    * @param: cInputData 输入的数据
    *         cOperate 数据操作
    * @return:
    */
   public boolean submitData(VData cInputData, String cOperate) {

     System.out.println("---PayManageConfirmBL BEGIN---");
     mOperate = cOperate;
     if (!getInputData(cInputData)){
         System.out.println("getinputdata failed");
       return false;
     }

     if (!dealData())
     {
          // @@错误处理
        CError tError = new CError();
        tError.moduleName = "PayManageConfirmBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据处理失败PayManageConfirmBL-->dealData!";
        this.mErrors.addOneError(tError);
        return false;
      }
     if (!prepareOutputData())
       return false;

     //数据提交

     PayManageConfirmBLS tPayManageConfirmBLS = new PayManageConfirmBLS();

     if (!tPayManageConfirmBLS.submitData(mInputData, mOperate)) {
       // @@错误处理
       this.mErrors.copyAllErrors(tPayManageConfirmBLS.mErrors);
       CError tError = new CError();
       tError.moduleName = "PayManageConfirmBL";
       tError.functionName = "submitData";
       tError.errorMessage = "数据提交失败!";
       this.mErrors.addOneError(tError);
       return false;
     }
     System.out.println("---PayManageConfirmBL END---");
     return true;
   }


   /**
    * 数据操作类业务处理
    * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    */
   private boolean dealData() {
   tLJAGetDB.setActuGetNo(mActuGetNo);
   if(!tLJAGetDB.getInfo())
   {
       return false;
   }
   else
   {
       tLJAGetSchema = tLJAGetDB.getSchema();
   }
     if (mOperate.equals("ACC"))
     {
         if (!changeAccount())
       return false;
     }
     if (mOperate.equals("LOCK"))
     {
         if (!lockBankData())
       return false;
     }
     if (mOperate.equals("UNLOCK"))
     {
         if (!unlockBankData())
       return false;
     }
     System.out.println("dealdate mOperate:"+mOperate);

     return true;
 }

 private boolean changeAccount() {
 String mBankCode = StrTool.cTrim(mLJAGetSchema.getBankCode());
 String mAccName = StrTool.cTrim(mLJAGetSchema.getAccName());
 String mAccNo = StrTool.cTrim(mLJAGetSchema.getBankAccNo());
 if (StrTool.cTrim(mLJAGetSchema.getPayMode()).equals("4"))
 {
     if (mBankCode.equals("")||mAccName.equals("")||mAccNo.equals(""))
      return false;
     else
     {
         tLJAGetSchema.setAccName(mAccName);
         tLJAGetSchema.setBankAccNo(mAccNo);
         tLJAGetSchema.setBankCode(mBankCode);
     }
 }
      return true;
 }

 private boolean lockBankData() {
     tLJAGetSchema.setCanSendBank("1");
     return true;
 }

 private boolean unlockBankData() {
     tLJAGetSchema.setCanSendBank("0");
     return true;
 }

   /**
    * 从输入数据中得到所有对象
    *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
    */
   private boolean getInputData(VData cInputData) {
       mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
       mLJAGetSchema.setSchema((LJAGetSchema) cInputData.getObjectByObjectName("LJAGetSchema", 0));
       mActuGetNo = mLJAGetSchema.getActuGetNo();
       return true;
   }

   /**
    *准备需要保存的数据
    **/
   private boolean prepareOutputData() {
     mInputData.clear();
     mInputData.add(tLJAGetSchema);
     return true;
   }

}
