package com.sinosoft.lis.finfee;

import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.operfee.ChangeFinFeeStateBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 银保通续期收费确认类
 * 
 * @author zhangchengxuan
 */
public class YBTPayConf {

	/** 保单号 */
	private String contNo;

	/** 收费金额 */
	private String payMoney;

	/** 错误处理类 */
	private CErrors mErrors = new CErrors();

	/** GlobalInput信息对象 */
	private GlobalInput mGlobalInput;

	/** 暂收费号 */
	private String mTempFeeNo;

	/** 提交数据库VData集合 */
	private VData mInputData = new VData();

	/** 提交数据库MMap集合 */
	private MMap map = new MMap();

	/**
	 * 程序调用入口
	 * 
	 * @param cInputData
	 * @return 收费确认成功标记 true-续期收费确认成功 false-续期收费确认失败
	 */
	public boolean submitData(TransferData tTransferData) {

		System.out.println("---YBTPayConf--start---");

		if (!getInputData(tTransferData)) {
			return false;
		}

		System.out.println("---End getInputData---");

		if (!dealData()) {
			return false;
		}

		if (!prepareOutputData()) {
			return false;
		}
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, "")) {
			// 错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}

		if (!finUrgeVerify()) {
			return false;
		}

		if (!afterSubmit()) {
			return false;
		}

		System.out.println("---End dealData---");
		return true;
	}

	/**
	 * 获取查询失败原因
	 * 
	 * @return mErrors 错误信息集合
	 */
	public CErrors getErrorInf() {

		return this.mErrors;
	}

	/**
	 * 获取参数
	 * 
	 * @param tTransferData
	 *            payMoney-收费金额 contNo-保单号
	 * @return 传入信息成功标记
	 */
	private boolean getInputData(TransferData tTransferData) {

		try {
			this.contNo = (String) tTransferData.getValueByName("ContNo");
			this.payMoney = (String) tTransferData.getValueByName("PayMoney");
			this.mGlobalInput = (GlobalInput) tTransferData
					.getValueByName("GlobalInput");

			if (contNo == null || contNo.equals("")) {
				mErrors.addOneError("保单号为空");
				System.out.println("---传入的保单号为空---");
				return false;
			}

			if (payMoney == null || payMoney.equals("")) {
				mErrors.addOneError("金额为空");
				System.out.println("---传入的金额为空---");
				return false;
			}

			if (mGlobalInput == null) {
				mErrors.addOneError("传入的机构信息为空");
				System.out.println("---传入的机构信息为空---");
				return false;
			}
		} catch (Exception e) {
			mErrors.addOneError("获取传入的保单信息失败");
			System.out.println("---获取保单信息失败---");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * 程序处理入口
	 * 
	 * @return 收费确认处理成功标记
	 */
	private boolean dealData() {

		String checkSql = "select sum(paymoney) from ljtempfee where otherno='"
				+ contNo + "' and tempfeetype='18' and confdate is null";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(checkSql);
		if (tSSRS.MaxRow == 0 || tSSRS.GetText(1, 1) == null
				|| tSSRS.GetText(1, 1).equals("null")
				|| tSSRS.GetText(1, 1).equals("")) {
			mErrors.addOneError("未查到保单收费相关信息");
			System.out.println("---未查到保单收费相关信息---");
			return false;
		} else if (Double.parseDouble(tSSRS.GetText(1, 1)) == 0) {
			mErrors.addOneError("该笔收费已进行反冲");
			System.out.println("---该笔收费已进行反冲---");
			return false;
		} else if (Double.parseDouble(tSSRS.GetText(1, 1)) != Double
				.parseDouble(payMoney)) {
			mErrors.addOneError("金额与收费金额不符");
			System.out.println("---金额与收费金额不符---");
			return false;
		}

		String querySql = "select * from ljtempfee where otherno='"
				+ contNo
				+ "' and tempfeetype='18' and confdate is null and exists (select 1 from ljspay where getnoticeno=ljtempfee.tempfeeno)";
		System.out.println(querySql);
		LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
		LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.executeQuery(querySql);

		if (tLJTempFeeSet.size() == 0) {
			mErrors.addOneError("未查到保单收费相关信息");
			System.out.println("---未查到保单收费相关信息---");
			return false;
		}

		// 用于续期核销使用
		mTempFeeNo = tLJTempFeeSet.get(1).getTempFeeNo();

		// 要改主键 无奈
		String updateSql = "update ljtempfee set tempfeetype='2',modifydate=current date,modifytime=current time,operator='"
				+ mGlobalInput.Operator
				+ "' where tempfeeno='"
				+ mTempFeeNo
				+ "' and otherno='"
				+ contNo
				+ "' and tempfeetype='18' and confdate is null";

		String upLjspaySql = "update ljspay set bankonthewayflag='0',modifydate=current date,modifytime=current time,operator='"
				+ mGlobalInput.Operator
				+ "' where getnoticeno='"
				+ mTempFeeNo
				+ "'";

		this.map.put(updateSql, "UPDATE");
		this.map.put(upLjspaySql, "UPDATE");
		return true;
	}

	/**
	 * 设置更新数据
	 * 
	 * @return 成功标记
	 */
	private boolean prepareOutputData() {

		try {
			mInputData.clear();
			mInputData.add(map);
		} catch (Exception ex) {
			ex.printStackTrace();
			mErrors.addOneError("在准备往后层处理所需要的数据时出错");
			return false;
		}
		return true;
	}

	/**
	 * 调用续期后续核销类
	 * 
	 * @return 暂全部为成功
	 */
	private boolean afterSubmit() {

		LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
		tLJTempFeeSchema.setTempFeeNo(mTempFeeNo);

		AfterTempFee2BL tAfterTempFee2BL = new AfterTempFee2BL();
		VData data = new VData();
		data.add(mGlobalInput);
		data.add(tLJTempFeeSchema);

		MMap map = tAfterTempFee2BL.getSubmitMMap(data, "");

		if (map == null || map.size() == 0) {
			return true;
		}

		VData upData = new VData();
		upData.add(map);
		PubSubmit p = new PubSubmit();
		if (!p.submitData(upData, "")) {
			mErrors.addOneError("对账确认成功，但续期核销处理失败");
			System.out.println("--对账确认成功，但续期核销处理失败--");
			return true;
		}

		return true;
	}

	/**
	 * 修改续期应收备份表状态
	 * 
	 * @return 暂全部返回成功
	 */
	private boolean finUrgeVerify() {

		LJSPaySchema tLJSPaySchema = new LJSPaySchema();
		tLJSPaySchema.setGetNoticeNo(mTempFeeNo);
		VData ttVdata = new VData();
		ttVdata.add(tLJSPaySchema);
		ttVdata.add(mGlobalInput);
		ChangeFinFeeStateBL bl = new ChangeFinFeeStateBL("4");
		if (!bl.submitData(ttVdata, "")) {
			mErrors.addOneError("收费成功，但续期应收数据处理失败");
			System.out.println("--收费成功，但续期应收数据处理失败--");
			return true;
		}
		return true;
	}

	/**
	 * 调用方法举例
	 */
	public static void main(String[] arr) {

		CErrors cErrors = new CErrors();

		GlobalInput tGI = new GlobalInput();
		tGI.ManageCom = "86000000";
		tGI.Operator = "YBT";

		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("ContNo", "000922666000001");
		tTransferData.setNameAndValue("PayMoney", "19745.00");
		tTransferData.setNameAndValue("GlobalInput", tGI);

		YBTPayConf tYBTPayConf = new YBTPayConf();

		if (!tYBTPayConf.submitData(tTransferData)) {
			// 收费确认失败，可获得返回信息
			cErrors = tYBTPayConf.getErrorInf();
			System.out.println(cErrors.getLastError());
		}
	}
}
