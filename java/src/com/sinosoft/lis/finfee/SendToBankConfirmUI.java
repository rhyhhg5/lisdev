package com.sinosoft.lis.finfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

public class SendToBankConfirmUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 往界面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public SendToBankConfirmUI() {}

  public static void main(String[] args)
  {
  GlobalInput tGI = new GlobalInput();
	TransferData tTransferData = new TransferData();
	LCContSchema tLCContSchema = new LCContSchema();
	SendToBankConfirmUI tSendToBankConfirmUI  = new SendToBankConfirmUI();

  tTransferData.setNameAndValue( "GetNoticeNo","16000002810801");
  tLCContSchema.setPrtNo("16000002810");
  tLCContSchema.setPayMode("1");
  //tLCContSchema.setBankCode("0701");
 // tLCContSchema.setAccName("冬雨");
 // tLCContSchema.setBankAccNo("11111");
  tGI.Operator="fi001";
  tGI.ManageCom="86";
  tGI.ComCode="86";

			VData tVData = new VData();
			tVData.add(tTransferData);
			tVData.add(tLCContSchema);
			tVData.add( tGI );
      tSendToBankConfirmUI.submitData(tVData,"PAYMODE");
  }

  /**
  传输数据的公共方法
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    System.out.println("---SendToBankConfirmUI BEGIN---");
    SendToBankConfirmBL tSendToBankConfirmBL = new SendToBankConfirmBL();
    if (tSendToBankConfirmBL.submitData(cInputData,mOperate) == false)
	{
  		// @@错误处理
      this.mErrors.copyAllErrors(tSendToBankConfirmBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "SendToBankConfirmUI";
      tError.functionName = "submitData";
      tError.errorMessage = "数据查询失败!";
      this.mErrors.addOneError(tError) ;
      mResult.clear();
      return false;
	}
	  System.out.println("---SendToBankConfirmUI END---");
    return true;
  }
}
