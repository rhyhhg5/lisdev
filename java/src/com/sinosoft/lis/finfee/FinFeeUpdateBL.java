package com.sinosoft.lis.finfee;

import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.MMap;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class FinFeeUpdateBL {

    private String mOperate="";
    private VData mInputData=new VData();
    private VData mResultData=new VData();
    /** 提交数据的容器 */
    private MMap map = new MMap();

    private LJAGetSchema  tLJAGetSchema=new LJAGetSchema() ; //实付总表
    private LJAGetDB tLJAGetDB=new LJAGetDB();
    private TransferData updateData = new TransferData();
    public CErrors mErrors=new CErrors();

    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();

    public FinFeeUpdateBL() {
    }

    public boolean submitData(VData cInputData,String cOperate)
 {
   //将操作数据拷贝到本类中
   this.mOperate =cOperate;
   this.mInputData=cInputData;
System.out.println("OperateData:  "+cOperate);

   if(mOperate.equals("UPDATE"))
   {
       //得到外部传入的数据,将数据备份到本类中
       if (!getInputData(mInputData))
           return false;
       System.out.println("After getinputdata");

       //进行业务处理
       if (!dealData())
           return false;
       System.out.println("After dealData！");
       //准备往后台的数据
       if(!prepareOutputData())
           return false;
       System.out.println("After prepareOutputData！");

       PubSubmit tPubSubmit = new PubSubmit();
      if (!tPubSubmit.submitData(mResultData, mOperate)) {
          // @@错误处理
          this.mErrors.copyAllErrors(tPubSubmit.mErrors);
          mResultData.clear();
          return false;
      }
      System.out.println("End PubSubmit BLS Submit...");

   }
   return true;
 }

    /**
     * prepareOutputData
     *
     * @return boolean
     */
    private boolean prepareOutputData() {
        try {
            map.put(tLJAGetDB.getSchema(), "UPDATE");
            mResultData.clear();
            mResultData.add(map);
        } catch (Exception ex) {
           // @@错误处理
           CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
           return false;
       }
       return true;
    }

    /**
     * dealData
     *
     * @return boolean
     *
     *
     */
    private boolean dealData() {
        tLJAGetDB = new LJAGetDB();
        tLJAGetDB.setActuGetNo(tLJAGetSchema.getActuGetNo());
        if (!tLJAGetDB.getInfo())
        {
            CError.buildErr(this, "获取银行日志数据失败");
            return false;
        }

        tLJAGetDB.setPayMode((String) updateData.getValueByName("PayMode"));
        tLJAGetDB.setBankAccNo((String)updateData.getValueByName("BankAccNo"));
        tLJAGetDB.setBankCode((String)updateData.getValueByName("BankCode"));
        tLJAGetDB.setAccName((String)updateData.getValueByName("AccName"));
        tLJAGetDB.setInsBankAccNo((String)updateData.getValueByName("InsBankAccNo"));
        tLJAGetDB.setChequeNo((String)updateData.getValueByName("ChequeNo"));
        tLJAGetDB.setModifyDate(CurrentDate);
        tLJAGetDB.setModifyTime(CurrentTime);
System.out.println("in java  ModifyDate: "+CurrentDate+" ModifyTime: "+CurrentTime);
        //tLJAGetDB.setModifyDate(CurrentTime);
        tLJAGetDB.setOperator((String)updateData.getValueByName("Operator"));
         map.put(tLJAGetDB.getSchema(), "UPDATE");
        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        //实付总表
        tLJAGetSchema = (LJAGetSchema) cInputData.getObjectByObjectName(
                "LJAGetSchema", 0);
        // 财务给付表
        updateData = (TransferData) cInputData.getObjectByObjectName("TransferData",
                0);
        if (tLJAGetSchema == null || updateData == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OperFinFeeGetBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (tLJAGetSchema.getBankOnTheWayFlag() != null) {
            if (tLJAGetSchema.getBankOnTheWayFlag().equals("1")) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "OperFinFeeGetBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "正在送银行途中，不能修改财务付费数据!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;

    }
}
