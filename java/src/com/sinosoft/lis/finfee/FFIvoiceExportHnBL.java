/**
 * 2007-5-22
 */
package com.sinosoft.lis.finfee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.db.LJInvoiceInfoDB;
import com.sinosoft.lis.db.LOPRTInvoiceManagerDB;
import com.sinosoft.lis.db.LOPRTManager2DB;
import com.sinosoft.lis.finfee.invoiceprtxml.InvoicePrtXmlException;
import com.sinosoft.lis.finfee.invoiceprtxml.XmlHelper;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJAPayPersonSchema;
import com.sinosoft.lis.schema.LOPRTManager2Schema;
import com.sinosoft.lis.db.LJAPayPersonDB;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LJAPaySet;
import com.sinosoft.lis.vschema.LOPRTManager2Set;
import com.sinosoft.lis.vschema.LJInvoiceInfoSet;
import com.sinosoft.lis.vschema.LOPRTInvoiceManagerSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class FFIvoiceExportHnBL implements InvoiceExport
{
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private ArrayList mResult = new ArrayList();

    private String mBasePath = "";

    private String mOutPath = "";

    private String mPrtTemplate = "";

    /** 
     * 纳税人识别号
     */
    private String skfhm = "";

    /**
     * 纳税人名称
     */
    private String skfmc = "";

    /**
     * 所属期起
     */
    private String mSsqq = "";

    /**
     * 所属期止
     */
    private String mSsqz = "";

    /**
     * 收付费发票标记
     */
    private String mSFState = "";

    private String mType = "";

    /**
     * 办理日期
     */
    // private String minvoicecode = "";
    /**
     * 发票开票详细信息（多单）
     * 以hashmap代替结构体，保存每一张发票信息
     */
    private ArrayList mFpList = new ArrayList();

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData))
        {
            buildError("getInputData", "没有获取到足够业务信息或者机构不对");
            return false;
        }
        if (!exportXML())
        {
            buildError("exportXML", "导出XML文件失败");
            return false;
        }
        return true;
    }

    /**
     * 获取业务数据
     * @param cInputData
     * @return
     */
    private boolean getInputData(VData cInputData)
    {
        TransferData tParameters = (TransferData) cInputData
                .getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mGlobalInput == null || tParameters == null)
        {
            return false;
        }

        this.mBasePath = (String) tParameters.getValueByName("hostBasePath");
        this.mOutPath = (String) tParameters.getValueByName("outPath") + "/";
        this.mPrtTemplate = (String) tParameters.getValueByName("prtTemplate");

        this.mSsqq = (String) tParameters.getValueByName("invoiceStartDate");
        this.mSsqz = (String) tParameters.getValueByName("invoiceEndDate");
        this.mSFState = (String) tParameters.getValueByName("SFState");
        this.mType = (String) tParameters.getValueByName("Type");
        // 获取条件内的相关发票打印信息
        String tSql = "";
        if ("1".equals(mSFState))
        {
            tSql = "select * from LOPRTInvoiceManager a "
                    + " where 1 = 1 "
                    + " and exists (select 1 from loprtmanager2 where code='37' and standbyflag4=a.invoiceno and makedate=a.makedate and comcode=a.comcode)"
                    + " and comcode like '" + mGlobalInput.ManageCom + "%'"
                    + " and makedate >= '" + mSsqq + "' "
                    + " and makedate <= '" + mSsqz + "'";
        }
        else
        {
            tSql = "select * from LOPRTInvoiceManager a "
                    + " where 1 = 1 "
                    + " and not exists (select 1 from loprtmanager2 where code='37' and standbyflag4=a.invoiceno and makedate=a.makedate and comcode=a.comcode)"
                    + " and comcode like '" + mGlobalInput.ManageCom + "%'"
                    + " and makedate >= '" + mSsqq + "' "
                    + " and makedate <= '" + mSsqz + "'";
        }
        LOPRTInvoiceManagerDB tLOPRTInvoiceManagerDB = new LOPRTInvoiceManagerDB();
        LOPRTInvoiceManagerSet tLOPRTInvoiceManagerSet = tLOPRTInvoiceManagerDB
                .executeQuery(tSql);
        System.out.println(tSql);
        if (tLOPRTInvoiceManagerSet == null
                || tLOPRTInvoiceManagerSet.size() == 0)
            return false;
        tSql = "select * from LJInvoiceInfo where comcode = '"
                + mGlobalInput.ManageCom + "' " + " and invoicecode='"
                + tLOPRTInvoiceManagerSet.get(1).getInvoiceCode() + "'" 
                + " order by makedate desc fetch first 1 rows only with ur ";
        System.out.println(tSql);
        LJInvoiceInfoDB tLJInvoiceInfoDB = new LJInvoiceInfoDB();
        LJInvoiceInfoSet tLJInvoiceInfoSet = tLJInvoiceInfoDB
                .executeQuery(tSql);
        if (tLJInvoiceInfoSet == null || tLJInvoiceInfoSet.size() <= 0)
            return false;

        this.skfhm = tLJInvoiceInfoSet.get(1).getTaxpayerNo();
        this.skfmc = tLJInvoiceInfoSet.get(1).getTaxpayerName();

        for (int i = 1; i <= tLOPRTInvoiceManagerSet.size(); i++)
        {
            HashMap subNodeTemp = new HashMap();
            if ("0".equals(mType))
            {
                subNodeTemp.put("fp_dm", tLOPRTInvoiceManagerSet.get(i)
                        .getInvoiceCode());
                subNodeTemp.put("fphm", tLOPRTInvoiceManagerSet.get(i)
                        .getInvoiceNo());
            	   
            	subNodeTemp.put("fkfmc",tLOPRTInvoiceManagerSet.get(i).getPayerName());
            
                subNodeTemp.put("fkfhm", "");
                subNodeTemp.put("skfmc", skfmc);
                subNodeTemp.put("skfhm", skfhm);
                subNodeTemp
                        .put("je", Double.toString(PubFun.setPrecision(
                                tLOPRTInvoiceManagerSet.get(i).getXSumMoney(),
                                "0.00")));
                subNodeTemp.put("bz", "");
                subNodeTemp.put("swjg_dm", "241009007");
                if (tLOPRTInvoiceManagerSet.get(i).getStateFlag().equals("0"))
                {
                    subNodeTemp.put("kplx_dm", "11");
                }
                else if (tLOPRTInvoiceManagerSet.get(i).getContNo() == null)
                {
                    subNodeTemp.put("kplx_dm", "22");
                }
                else
                {
                    subNodeTemp.put("kplx_dm", "21");
                }
                subNodeTemp.put("kpsj", tLOPRTInvoiceManagerSet.get(i)
                        .getMakeDate());
                subNodeTemp.put("yfpzl_dm", "");
                subNodeTemp.put("yfphm", "");
                subNodeTemp.put("fphy_dm", "020302");
            }
            else
            {
            
                subNodeTemp.put("fp_dm", tLOPRTInvoiceManagerSet.get(i)
                        .getInvoiceCode());
                subNodeTemp.put("fphm", tLOPRTInvoiceManagerSet.get(i)
                        .getInvoiceNo());
                
                subNodeTemp.put("xh", "1");
               if("1".equals(mSFState)){
            	   LJAGetDB tLJAGetDB = new LJAGetDB();
                   LJAGetSet tLJAGetSet = new LJAGetSet();
                   LJAGetSchema mLJAGetSchema = new LJAGetSchema();
                   LOPRTManager2Schema tLOPRTManager2Schema = new LOPRTManager2Schema();
                   LOPRTManager2DB tLOPRTManager2DB = new LOPRTManager2DB();
                   LOPRTManager2Set tLOPRTManager2Set = new LOPRTManager2Set();
                   tLOPRTManager2DB.setStandbyFlag4(tLOPRTInvoiceManagerSet.get(i).getInvoiceNo());
                   tLOPRTManager2Set =tLOPRTManager2DB.query();
                   for(int j=1;j<=tLOPRTManager2Set.size();j++){
                	   tLOPRTManager2Schema=tLOPRTManager2Set.get(j);
                	   if(tLOPRTManager2Schema.getMakeDate().equals(tLOPRTInvoiceManagerSet.get(i).getMakeDate())&&tLOPRTManager2Schema.getManageCom().substring(0, 4).equals(tLOPRTInvoiceManagerSet.get(i).getComCode().substring(0, 4))){
                		   
                	          tLJAGetDB.setActuGetNo(tLOPRTManager2Schema.getOtherNo());
                	   }
                   }
                   tLJAGetSet = tLJAGetDB.query();
                   mLJAGetSchema=tLJAGetSet.get(1);
         
                   
//            	   //需要确定
//                   tLJAGetDB.setOtherNo(tLOPRTInvoiceManagerSet.get(i).getInvoiceNo());
//            	   tLJAGetSet = tLJAGetDB.query();
//            	   mLJAGetSchema=tLJAGetSet.get(1);
            	   ExeSQL tExeSQL = new ExeSQL();
                   SSRS tSSRS = new SSRS();   
               	   String tRiskName ="";
               	   String tRiskName2="";
            	   
            	   if (mLJAGetSchema.getOtherNoType().equals("5")||mLJAGetSchema.getOtherNoType().equals("C")||mLJAGetSchema.getOtherNoType().equals("F"))
                   {
                       tSql = "select (Select Drawer From Ljaget Where Actugetno = a.Actugetno),(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
                           + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
                           + "when '530301' then (select riskwrapplanname from ldcode1 "
                           + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),a.riskcode,c.AppntNo,c.contno,a.agentcode,a.confdate from ljagetclaim a,lmriskapp b,lccont c where c.ContNo = a.ContNo "
                           + "and a.Actugetno = '"+mLJAGetSchema.getActuGetNo()
                           + "' and a.riskcode=b.riskcode  and c.conttype='1' ";
                       tSql= tSql + "union select (Select Drawer From Ljaget Where Actugetno = a.Actugetno),(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
                       + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
                       + "when '530301' then (select riskwrapplanname from ldcode1 "
                       + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),a.riskcode,c.AppntNo,c.contno,a.agentcode,a.confdate from ljagetclaim a,lmriskapp b,lbcont c where c.ContNo = a.ContNo "
                       + "and a.Actugetno = '"+mLJAGetSchema.getActuGetNo()
                       + "' and a.riskcode=b.riskcode  and c.conttype='1' ";
                       tSql =tSql+  "union select (Select Drawer From Ljaget Where Actugetno = a.Actugetno),(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
                       + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
                       + "when '530301' then (select riskwrapplanname from ldcode1 "
                       + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),a.riskcode,c.AppntNo,c.grpcontno,a.agentcode,a.confdate from ljagetclaim a,lmriskapp b,lcgrpcont c where c.grpContNo = a.grpContNo "
                       + "and a.Actugetno = '"+mLJAGetSchema.getActuGetNo()
                       + "' and a.riskcode=b.riskcode  ";
                       tSql =tSql+  "union select (Select Drawer From Ljaget Where Actugetno = a.Actugetno),(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
                       + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
                       + "when '530301' then (select riskwrapplanname from ldcode1 "
                       + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),a.riskcode,c.AppntNo,c.grpcontno,a.agentcode,a.confdate from ljagetclaim a,lmriskapp b,lbgrpcont c where c.grpContNo = a.grpContNo "
                       + "and a.Actugetno = '"+mLJAGetSchema.getActuGetNo()
                       + "' and a.riskcode=b.riskcode  ";
                     
                       tSSRS=tExeSQL.execSQL(tSql);
                       if(tSSRS.getMaxRow()>0){
                       	 
                              tRiskName=tSSRS.GetText(1,2);
                              tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL,tSSRS.GetText(1,5));
                              if(tRiskName2!=""&&tRiskName2!=null){
                                  tRiskName=tRiskName2;
                                }
                       }else{
               tSql = "select (Select Drawer From Ljaget Where Actugetno = a.Actugetno),(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
                  + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
                  + "when '530301' then (select riskwrapplanname from ldcode1 "
                  + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),a.riskcode,c.AppntNo,c.contno,a.agentcode,a.confdate from ljagetclaim a,lmriskapp b,lbcont c where c.ContNo = a.ContNo "
                  + "and a.Actugetno = '"+mLJAGetSchema.getActuGetNo()
                  + "' and a.riskcode=b.riskcode  and c.conttype='1' ";
              tSql= tSql + "union select (Select Drawer From Ljaget Where Actugetno = a.Actugetno),(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
              + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
              + "when '530301' then (select riskwrapplanname from ldcode1 "
              + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),a.riskcode,c.AppntNo,c.contno,a.agentcode,a.confdate from ljagetclaim a,lmriskapp b,lbcont c where c.ContNo = a.ContNo "
              + "and a.Actugetno = '"+mLJAGetSchema.getActuGetNo()
              + "' and a.riskcode=b.riskcode  and c.conttype='1' ";
              tSql =tSql+  "union select (Select Drawer From Ljaget Where Actugetno = a.Actugetno),(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
              + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
              + "when '530301' then (select riskwrapplanname from ldcode1 "
              + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),a.riskcode,c.AppntNo,c.grpcontno,a.agentcode,a.confdate from ljagetclaim a,lmriskapp b,lcgrpcont c where c.grpContNo = a.grpContNo "
              + "and a.Actugetno = '"+mLJAGetSchema.getActuGetNo()
              + "' and a.riskcode=b.riskcode  ";
              tSql =tSql+  "union select (Select Drawer From Ljaget Where Actugetno = a.Actugetno),(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
              + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
              + "when '530301' then (select riskwrapplanname from ldcode1 "
              + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),a.riskcode,c.AppntNo,c.grpcontno,a.agentcode,a.confdate from ljagetclaim a,lmriskapp b,lbgrpcont c where c.grpContNo = a.grpContNo "
              + "and a.Actugetno = '"+mLJAGetSchema.getActuGetNo()
              + "' and a.riskcode=b.riskcode  ";
            
              tSSRS=tExeSQL.execSQL(tSql);
                if(tSSRS.getMaxRow()>0){
              	 
                     tRiskName=tSSRS.GetText(1,2);
                     tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL,tSSRS.GetText(1,5));
                     if(tRiskName2!=""&&tRiskName2!=null){
                         tRiskName=tRiskName2;
            
            
                     }
            
            
                   }
                   }
                       subNodeTemp.put("xmmc", tRiskName);
                   }
            	   
                   if (mLJAGetSchema.getOtherNoType().equals("10"))
                   {
                       tSql = "select  AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
                              + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
                              + "when '530301' then (select riskwrapplanname from ldcode1 "
                              + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),b.riskcode,b.AppntNo,b.contno,(select edorname from lmedoritem where edorcode=a.Feeoperationtype fetch first 1 rows only) from LJAGetEndorse a, lcpol b, lmriskapp c where ActuGetNo = '"
                              + mLJAGetSchema.getActuGetNo()
                              + "' and a.contno=b.contno and b.riskcode=c.riskcode  order by b.prem desc";
                       tSSRS=tExeSQL.execSQL(tSql);
                       if(tSSRS.getMaxRow()>0){
                         
                              tRiskName=tSSRS.GetText(1,2);
                              
                              tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                              
                              if(tRiskName2!=""&&tRiskName2!=null){
                                  tRiskName=tRiskName2;
                              }
                        }else{
                       	 tSql = "select  AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
                                + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
                                + "when '530301' then (select riskwrapplanname from ldcode1 "
                                + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),b.riskcode,b.AppntNo,b.contno,(select edorname from lmedoritem where edorcode=a.Feeoperationtype fetch first 1 rows only) from LJAGetEndorse a, lbpol b, lmriskapp c where ActuGetNo = '"
                                + mLJAGetSchema.getActuGetNo()
                                + "' and a.contno=b.contno and b.riskcode=c.riskcode  order by b.prem desc";
                         tSSRS=tExeSQL.execSQL(tSql);
                         if(tSSRS.getMaxRow()>0){
                               
                                tRiskName=tSSRS.GetText(1,2);
                              
                                tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                               
                                if(tRiskName2!=""&&tRiskName2!=null){
                                    tRiskName=tRiskName2;
                                }
                          }
                       	 
                        }
                       subNodeTemp.put("xmmc", tRiskName);
                   }
                   
                   if (mLJAGetSchema.getOtherNoType().equals("3"))
                   {
                       tSql = "select  b.GrpName,RiskName,b.riskcode,b.CustomerNo,b.grpcontno,(select edorname from lmedoritem where edorcode=a.Feeoperationtype fetch first 1 rows only) from LJAGetEndorse a, lcgrppol b, lmriskapp c where ActuGetNo = '"
                              + mLJAGetSchema.getActuGetNo()
                              + "' and a.grpcontno=b.grpcontno and a.grpcontno=(select  grpcontno from lpgrpedoritem where edorno=a.endorsementno fetch first 1 rows only)"
                              + " and b.riskcode=c.riskcode  order by b.prem desc";
                       tSSRS=tExeSQL.execSQL(tSql);
                       if(tSSRS.getMaxRow()>0){
                           
                           tRiskName=tSSRS.GetText(1,2);
                           
                           tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                           
                           if(tRiskName2!=""&&tRiskName2!=null){
                               tRiskName=tRiskName2;
                           }
                     }else{
                   	  tSql = "select  b.GrpName,RiskName,b.riskcode,b.CustomerNo,b.grpcontno,(select edorname from lmedoritem where edorcode=a.Feeoperationtype fetch first 1 rows only) from LJAGetEndorse a, lbgrppol b, lmriskapp c where ActuGetNo = '"
                             + mLJAGetSchema.getActuGetNo()
                             + "' and a.grpcontno=b.grpcontno and a.grpcontno=(select  grpcontno from lpgrpedoritem where edorno=a.endorsementno fetch first 1 rows only)"
                             + " and b.riskcode=c.riskcode  order by b.prem desc";
                   	  tSSRS=tExeSQL.execSQL(tSql);
                      if(tSSRS.getMaxRow()>0){
                             
                             tRiskName=tSSRS.GetText(1,2);
                            
                             tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                             
                             if(tRiskName2!=""&&tRiskName2!=null){
                                 tRiskName=tRiskName2;
                             }
                       } else {
                           tSql = "Select b.Grpname, Riskname, b.Riskcode, b.Customerno, b.Grpcontno, (Select Edorname  From Lmedoritem Where Edorcode = a.Feeoperationtype Fetch First 1 Rows Only) From Ljagetendorse a, Lcgrppol b, Lmriskapp c Where Actugetno = '"
                                + mLJAGetSchema.getActuGetNo()
                                + "' And a.Grpcontno = b.Grpcontno And a.Grpcontno = (Select contno From lgwork Where workno = a.Endorsementno and statusno = '5' and typeno = '070015' Fetch First 1 Rows Only) " 
                                + "And b.Riskcode = c.Riskcode Order By b.Prem Desc";
                         tSSRS=tExeSQL.execSQL(tSql);
                        if(tSSRS.getMaxRow()>0){
                               
                               tRiskName=tSSRS.GetText(1,2);
                               
                               tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                               
                               if(tRiskName2!=""&&tRiskName2!=null){
                                   tRiskName=tRiskName2;
                               }
                         } else {
                             tSql = "Select b.Grpname, Riskname, b.Riskcode, b.Customerno, b.Grpcontno, (Select Edorname  From Lmedoritem Where Edorcode = a.Feeoperationtype Fetch First 1 Rows Only) From Ljagetendorse a, LBgrppol b, Lmriskapp c Where Actugetno = '"
                                 + mLJAGetSchema.getActuGetNo()
                                 + "' And a.Grpcontno = b.Grpcontno And a.Grpcontno = (Select contno From lgwork Where workno = a.Endorsementno and statusno = '5' and typeno = '070015' Fetch First 1 Rows Only) " 
                                 + "And b.Riskcode = c.Riskcode Order By b.Prem Desc";
                             tSSRS=tExeSQL.execSQL(tSql);
                          if(tSSRS.getMaxRow()>0){
                                 
                                 tRiskName=tSSRS.GetText(1,2);
                                 
                                 tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                                 
                                 if(tRiskName2!=""&&tRiskName2!=null){
                                     tRiskName=tRiskName2;
                                 }
                           }
                         
                       }
                    	 
                     }
                      
                   }
                       subNodeTemp.put("xmmc", tRiskName);
                   }  
                   if (mLJAGetSchema.getOtherNoType().equals("20"))
                   {
                       tSql = "select  AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
                              + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
                              + "when '530301' then (select riskwrapplanname from ldcode1 "
                              + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),b.riskcode,b.AppntNo,b.contno from LJSGetdraw a, lcpol b, lmriskapp c where a.getnoticeno = '"
                              + mLJAGetSchema.getActuGetNo()
                              + "' and a.contno=b.contno and b.riskcode=c.riskcode   order by b.prem desc";
                       tSSRS=tExeSQL.execSQL(tSql);
                       if(tSSRS.getMaxRow()>0){
                              
                              tRiskName=tSSRS.GetText(1,2);
                             
                              tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                             
                              if(tRiskName2!=""&&tRiskName2!=null){
                                  tRiskName=tRiskName2;
                              }
                        }else{
                       	 tSql = "select  AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
                                + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
                                + "when '530301' then (select riskwrapplanname from ldcode1 "
                                + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),b.riskcode,b.AppntNo,b.contno from LJSGetdraw a, lbpol b, lmriskapp c where a.getnoticeno = '"
                                + mLJAGetSchema.getActuGetNo()
                                + "' and a.contno=b.contno and b.riskcode=c.riskcode   order by b.prem desc";
                         tSSRS=tExeSQL.execSQL(tSql);
                         if(tSSRS.getMaxRow()>0){
                               
                                tRiskName=tSSRS.GetText(1,2);
                               
                                tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                                
                                if(tRiskName2!=""&&tRiskName2!=null){
                                    tRiskName=tRiskName2;
                                }
                          }
                       	 
                        }
                       subNodeTemp.put("xmmc", tRiskName);
                   }
                   
                   if (mLJAGetSchema.getOtherNoType().equals("21"))
                   {
                       tSql = "select  b.GrpName,RiskName,b.riskcode,b.CustomerNo,b.grpcontno from LJSGetDraw a, lcgrppol b, lmriskapp c where a.getnoticeno = '"
                              + mLJAGetSchema.getActuGetNo()
                              + "' and a.grpcontno=b.grpcontno "
                              + " and b.riskcode=c.riskcode  order by b.prem desc";
                       tSSRS=tExeSQL.execSQL(tSql);
                       if(tSSRS.getMaxRow()>0){
                           
                           tRiskName=tSSRS.GetText(1,2);
                          
                           tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                        
                           if(tRiskName2!=""&&tRiskName2!=null){
                               tRiskName=tRiskName2;
                           }
                     }else{
                   	  tSql = "select  b.GrpName,RiskName,b.riskcode,b.CustomerNo,b.grpcontno from LJSGetDraw a, lbgrppol b, lmriskapp c where a.getnoticeno = '"
                             + mLJAGetSchema.getActuGetNo()
                             + "' and a.grpcontno=b.grpcontno "
                             + " and b.riskcode=c.riskcode  order by b.prem desc";
                   	  tSSRS=tExeSQL.execSQL(tSql);
                      if(tSSRS.getMaxRow()>0){
                         
                             tRiskName=tSSRS.GetText(1,2);
                            
                             tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                       
                             if(tRiskName2!=""&&tRiskName2!=null){
                                 tRiskName=tRiskName2;
                             }
                       }
                    	 
                     }
                   
                       subNodeTemp.put("xmmc", tRiskName); 
                   }
                   
              
                   
               }else{
                LJAPayDB tLJAPayDB = new LJAPayDB();
                LJAPaySet tLJAPaySet = new LJAPaySet();
                LJAPaySchema mLJAPaySchema = new LJAPaySchema();
                LOPRTManager2Schema tLOPRTManager2Schema = new LOPRTManager2Schema();
                LOPRTManager2DB tLOPRTManager2DB = new LOPRTManager2DB();
                LOPRTManager2Set tLOPRTManager2Set = new LOPRTManager2Set();
                //需要确定
                tLOPRTManager2DB.setStandbyFlag4(tLOPRTInvoiceManagerSet.get(i).getInvoiceNo());	
                 tLOPRTManager2Set = tLOPRTManager2DB.query();
                 for(int j=1;j<=tLOPRTManager2Set.size();j++){
              	   tLOPRTManager2Schema=tLOPRTManager2Set.get(j);
              	   if(tLOPRTManager2Schema.getMakeDate().equals(tLOPRTInvoiceManagerSet.get(i).getMakeDate())&&tLOPRTManager2Schema.getManageCom().substring(0, 4).equals(tLOPRTInvoiceManagerSet.get(i).getComCode().substring(0, 4))){
              		   
              	          tLJAPayDB.setPayNo(tLOPRTManager2Schema.getOtherNo());
              	   }
                 }
                 
                 
                 tLJAPaySet = tLJAPayDB.query();
                 mLJAPaySchema=tLJAPaySet.get(1);
//                tLJAPayDB.setIncomeNo(tLOPRTInvoiceManagerSet.get(i).getContNo());
//                	tLJAPaySet=tLJAPayDB.query();
//                	mLJAPaySchema=tLJAPaySet.get(1);
                	
                
                	ExeSQL tExeSQL = new ExeSQL();
                    SSRS tSSRS = new SSRS();   
                	String tRiskName ="";
                	String tRiskName2="";
                    if (mLJAPaySchema.getIncomeType().equals("2"))
                       {
                           tSql = "select c.AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
                               + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
                               + "when '530301' then (select riskwrapplanname from ldcode1 "
                               + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),a.riskcode,c.AppntNo from lcpol a,lmriskapp b,lccont c where c.ContNo = a.ContNo "
                               + "and a.ContNo = '"+mLJAPaySchema.getIncomeNo()
                               + "' and a.riskcode=b.riskcode and b.subriskflag='M' and a.stateflag='1' order by a.prem desc";
                           tSSRS=tExeSQL.execSQL(tSql);
                           
                           if(tSSRS.getMaxRow()>0){
                               
                                  tRiskName=tSSRS.GetText(1,2);
                                  
                                  tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL,mLJAPaySchema.getIncomeNo());
                                  if(tRiskName2!=""&&tRiskName2!=null){
                                      tRiskName=tRiskName2;
                                  }
                            }else{
                            	tSql = "select c.AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
                                    + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
                                    + "when '530301' then (select riskwrapplanname from ldcode1 "
                                    + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),a.riskcode,c.AppntNo from lcpol a,lmriskapp b,lbcont c where c.ContNo = a.ContNo "
                                    + "and a.ContNo = '"+mLJAPaySchema.getIncomeNo()
                                    + "' and a.riskcode=b.riskcode and b.subriskflag='M' and a.stateflag='1' order by a.prem desc";
                                tSSRS=tExeSQL.execSQL(tSql);	
                                if(tSSRS.getMaxRow()>0){
                                    
                                    tRiskName=tSSRS.GetText(1,2);
                                    
                                    tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL,mLJAPaySchema.getIncomeNo());
                                    if(tRiskName2!=""&&tRiskName2!=null){
                                        tRiskName=tRiskName2;
                            }
                          
                         }
                           
                       }
                           subNodeTemp.put("xmmc", tRiskName);
                       }    
                    if (mLJAPaySchema.getIncomeType().equals("1"))
                    {
                        tSql = "select  GrpName,RiskName,a.riskcode,a.CustomerNo from lcgrppol a,lmriskapp b where GrpContNo = '"
                            + mLJAPaySchema.getIncomeNo()
                            + "' and a.riskcode=b.riskcode and b.subriskflag='M'  order by a.prem desc";
                        tSSRS=tExeSQL.execSQL(tSql);
                        if(tSSRS.getMaxRow()>0){
                               
                        	tRiskName=tSSRS.GetText(1,2);
                               tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL,mLJAPaySchema.getIncomeNo());
                               if(tRiskName2!=""&&tRiskName2!=null){
                                   tRiskName=tRiskName2;
                               }
                         }
                        subNodeTemp.put("xmmc", tRiskName);
                    }
                    if (mLJAPaySchema.getIncomeType().equals("10"))
                    {
                        tSql = "select  AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
                               + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
                               + "when '530301' then (select riskwrapplanname from ldcode1 "
                               + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),b.riskcode,b.AppntNo,b.contno from LJAGetEndorse a, lcpol b, lmriskapp c where ActuGetNo = '"
                               + mLJAPaySchema.getPayNo()
                               + "'  and b.stateflag = '1' and a.contno=b.contno and b.riskcode=c.riskcode and c.subriskflag='M'  order by b.prem desc";
                        tSSRS=tExeSQL.execSQL(tSql);
                        if(tSSRS.getMaxRow()>0){
                               
                               tRiskName=tSSRS.GetText(1,2);
                               
                               tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
                               if(tRiskName2!=""&&tRiskName2!=null){
                                   tRiskName=tRiskName2;
                               }
                        }
                    
                        subNodeTemp.put("xmmc", tRiskName);
                    }
//                    if (mLJAPaySchema.getIncomeType().equals("10"))
//                    {
//                        tSql = "select  AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
//                               + "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
//                               + "when '530301' then (select riskwrapplanname from ldcode1 "
//                               + "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),b.riskcode,b.AppntNo,b.contno from LJAGetEndorse a, lcpol b, lmriskapp c where ActuGetNo = '"
//                               + mLJAPaySchema.getPayNo()
//                               + "'  and b.stateflag = '1' and a.contno=b.contno and b.riskcode=c.riskcode and c.subriskflag='M'  order by b.prem desc";
//                        tSSRS=tExeSQL.execSQL(tSql);
//                        if(tSSRS.getMaxRow()>0){
//                               
//                        	   tRiskName=tSSRS.GetText(1,2);
//                              
//                               tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL, tSSRS.GetText(1,5));
//                               if(tRiskName2!=""&&tRiskName2!=null){
//                                   tRiskName=tRiskName2;
//                               }
//                         } 
//                        subNodeTemp.put("xmmc", tRiskName);
//                    }
                    
                    if (mLJAPaySchema.getIncomeType().equals("3"))
                    {
                        tSql = "select  b.GrpName,RiskName,b.riskcode,b.CustomerNo,b.grpcontno from LJAGetEndorse a, lcgrppol b, lmriskapp c where ActuGetNo = '"
                               + mLJAPaySchema.getPayNo()
                               + "' and a.grpcontno=b.grpcontno and a.grpcontno=(select  grpcontno from lpgrpedoritem where edorno=a.endorsementno fetch first 1 rows only)"
                               + " and b.riskcode=c.riskcode and c.subriskflag='M' order by b.prem desc";
                        tSSRS=tExeSQL.execSQL(tSql);
                        if(tSSRS.getMaxRow()>0){
                               
                               tRiskName=tSSRS.GetText(1,2);
                              
                               tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL,tSSRS.GetText(1,5));
                               if(tRiskName2!=""&&tRiskName2!=null){
                                   tRiskName=tRiskName2;
                               }
                         }
                    
                        subNodeTemp.put("xmmc", tRiskName);
                    }
                    if (mLJAPaySchema.getIncomeType().equals("13"))
                    {
                        tSql = "Select  b.GrpName,RiskName,b.riskcode,b.CustomerNo,b.grpcontno from ljagetendorse a,lcgrppol b,lmriskapp c "
                               +"where actugetno in ( Select Btactuno from ljaedorbaldetail "
                               +"where actuno in ( Select getnoticeno from LJAPay where payno='"
                               +mLJAPaySchema.getPayNo()+"' )) "
                               +"and a.grpcontno=b.grpcontno and b.riskcode=c.riskcode and c.subriskflag='M' order by b.prem desc ";
                        tSSRS=tExeSQL.execSQL(tSql);
                        if(tSSRS.getMaxRow()>0){
                              
                               tRiskName=tSSRS.GetText(1,2);
                               
                               tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL,tSSRS.GetText(1,5));
                               if(tRiskName2!=""&&tRiskName2!=null){
                                   tRiskName=tRiskName2;
                               }
                        }
                    
                        subNodeTemp.put("xmmc", tRiskName);
                    }
                    
                    if (mLJAPaySchema.getIncomeType().equals("15"))
                    {
                        tSql = "select distinct '',b.RiskName,b.riskcode,'' from lmcardrisk a,lmriskapp b,LZCardPay c,LMCertifyDes d where c.PayNo = '"
                               + mLJAPaySchema.getIncomeNo()
                               + "' and a.riskcode=b.riskcode and b.subriskflag='M'  and d.CertifyCode=a.CertifyCode and d.subcode=c.cardtype "
                               + " order by  b.riskcode with ur";
                        tSSRS=tExeSQL.execSQL(tSql);
                        if(tSSRS.getMaxRow()>0){
                               
                               tRiskName=tSSRS.GetText(1,2);
                               
                               tRiskName2=getRiskName(tSSRS.GetText(1,3),tExeSQL,"");
                               if(tRiskName2!=""&&tRiskName2!=null){
                                   tRiskName=tRiskName2;
                               }
                        }
                        subNodeTemp.put("xmmc", tRiskName);
                        
                    }
              
                  
//              }
                
                
                       }
               subNodeTemp
               .put("hjje", Double.toString(PubFun.setPrecision(
                       tLOPRTInvoiceManagerSet.get(i).getXSumMoney(),
                       "0.00")));      
               
            }
            this.mFpList.add(subNodeTemp);
        } 
       
        return true;
    }

    
    private String getRiskName(String riskCode,ExeSQL tExeSQL,String tcontno){
        String riskName="";
        String riskSql="select riskwrapplanname from ldcode1 where codetype = 'bankcheckappendrisk' and code1 ='"+riskCode
        +"'  and code in(select riskwrapcode from lcriskwrap where grpcontno='"
        +tcontno+"' union select riskwrapcode from lcriskdutywrap where contno='"+tcontno+"' fetch first 1 rows only) with ur";
        SSRS tSSRS=new SSRS();
        tSSRS=tExeSQL.execSQL(riskSql);
        if(tSSRS.getMaxRow()>0){
            riskName=tSSRS.GetText(1, 1);
        }
        return riskName;
    }
    
    private boolean exportXML()
    {
        System.out.println("export xml begin...");
        String outFileName = "";
        if ("0".equals(mType))
        {
            outFileName = "d_ykpxx.xml";
        }
        else
        {
            outFileName = "d_ykpxxmx.xml";
        }

        try
        {
            XmlHelper xh = new XmlHelper(mPrtTemplate, mBasePath);
            if ("0".equals(mType))
            {
                // 清除子结点
                xh.removeSubNode("d_ykpxx", false);

                for (int i = 0; i < mFpList.size(); i++)
                {
                    xh.addSubItemValues("d_ykpxx", "d_ykpxx_row",
                            (HashMap) mFpList.get(i));
                }
            }
            else
            {
                // 清除子结点
                xh.removeSubNode("d_ykpxxmx", false);

                for (int i = 0; i < mFpList.size(); i++)
                {
                    xh.addSubItemValues("d_ykpxxmx", "d_ykpxxmx_row",
                            (HashMap) mFpList.get(i));
                }
            }

            xh.write(mOutPath + outFileName);
        }
        catch (InvoicePrtXmlException e)
        {
            e.printStackTrace();
        }
        mResult.add(mOutPath + outFileName);
        System.out.println("created xml files " + mOutPath + outFileName);
        System.out.println("export xml finished...");
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FFIvoiceExportHnBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public List getResult()
    {
        return mResult;
    }
    
    public CErrors getErrors(){
    	return mErrors;
    }
}
