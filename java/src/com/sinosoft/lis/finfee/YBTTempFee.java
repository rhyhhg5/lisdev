package com.sinosoft.lis.finfee;

import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.operfee.VerDuePayFeeQueryUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * 银保通续期收费类
 * 
 * @author zhangchengxuan
 */
public class YBTTempFee {

    /** 保单号 */
    private String contNo;

    /** 收费金额 */
    private String payMoney;

    /** 续期缴费通知书号 */
    private String getNoticeNo;

    /** 错误处理类 */
    private CErrors mErrors = new CErrors();

    /** 提交数据库VData集合 */
    private VData mInputData = new VData();

    /** 更新MMap集合 */
    private MMap map = new MMap();

    /** 本方银行编码 */
    private String bankCode;

    /** 银行编码 */
    private String insBankCode;

    /** 缴费方式 */
    private String payMode;

    /** 客户帐号 */
    private String bankAccNo;

    /** 客户账户姓名 */
    private String accName;

    /** 收费机构 */
    private String manageCom;

    /** GlobalInput信息对象 */
    private GlobalInput mGlobalInput;

    /**
     * 程序调用入口
     * 
     * @param cInputData
     * @return 收费成功标记 true-续期收费成功 false-续期收费失败
     */
    public boolean submitData(TransferData tTransferData) {

        System.out.println("---YBTTempFee--start---");

        if (!getInputData(tTransferData)) {
            return false;
        }

        System.out.println("---End getInputData---");

        if (!dealData()) {
            return false;
        }

        if (!prepareOutputData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            // 错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        //暂不修改LJSPayB状态
        /*if (!finUrgeVerify()) {
            return false;
        }*/
        System.out.println("---End dealData---");
        return true;
    }

    /**
     * 获取查询失败原因
     * 
     * @return mErrors 错误信息集合
     */
    public CErrors getErrorInf() {

        return this.mErrors;
    }

    /**
     * 获取参数
     * 
     * @param tTransferData payMoney-收费金额 contNo-保单号 PayMoney-金额 PayMode-缴费方式 BankAccNo-客户帐号 AccName-帐号名称
     * @return 传入信息成功标记
     */
    private boolean getInputData(TransferData tTransferData) {

        try {
            this.insBankCode = (String) tTransferData.getValueByName("BankCode");
            this.contNo = (String) tTransferData.getValueByName("ContNo");
            this.payMoney = (String) tTransferData.getValueByName("PayMoney");
            this.mGlobalInput = (GlobalInput) tTransferData.getValueByName("GlobalInput");

            this.payMode = (String) tTransferData.getValueByName("PayMode");
            this.bankAccNo = (String) tTransferData.getValueByName("BankAccNo");
            this.accName = (String) tTransferData.getValueByName("AccName");

            if (payMode == null || payMode.equals("")) {
                mErrors.addOneError("缴费方式为空");
                System.out.println("---缴费方式为空---");
                return false;
            }
            else if (!payMode.equals("1")) {
                if (bankAccNo == null || bankAccNo.equals("") || accName == null || accName.equals("")) {
                    mErrors.addOneError("非现缴费方式，客户帐号及帐号名称不能为空");
                    System.out.println("---非现缴费方式，客户帐号及帐号名称不能为空---");
                    return false;
                }
            }
            if (contNo == null || contNo.equals("")) {
                mErrors.addOneError("保单号为空");
                System.out.println("---传入的保单号为空---");
                return false;
            }

            if (payMoney == null || payMoney.equals("")) {
                mErrors.addOneError("金额为空");
                System.out.println("---传入的金额为空---");
                return false;
            }

            if (mGlobalInput == null) {
                mErrors.addOneError("传入的机构信息为空");
                System.out.println("---传入的机构信息为空---");
                return false;
            }
            else {
                manageCom = mGlobalInput.ManageCom;
                for (int num = 0; manageCom.length() < 8; num++) {
                    manageCom = manageCom + "0";
                }
            }

            if (insBankCode == null || insBankCode.equals("")) {
                mErrors.addOneError("银行编码为空");
                System.out.println("---传入的银行编码为空---");
                return false;
            }
            else {
                String bankSql = "select bankcode from ldbank where comcode like '" + manageCom
                        + "%' and bankcode like '" + insBankCode.trim() + "%' order by comcode";
                ExeSQL tExeSQL = new ExeSQL();
                SSRS tSSRS = tExeSQL.execSQL(bankSql);
                if (tSSRS.MaxRow == 0) {
                    bankSql = "select bankcode from ldbank where comcode like '" + manageCom.substring(0, 4)
                            + "%' and bankcode like '" + insBankCode.trim() + "%' order by comcode";
                    tSSRS = tExeSQL.execSQL(bankSql);
                    if (tSSRS.MaxRow == 0) {
                        mErrors.addOneError("未查到相应的银行编码");
                        System.out.println("---未查到相应的银行编码---");
                        return false;
                    }
                    else {
                        bankCode = tSSRS.GetText(1, 1);
                    }
                }
                else {
                    bankCode = tSSRS.GetText(1, 1);
                }
            }

            // 收费前校验
            YBTPayQuery tYBTPayQuery = new YBTPayQuery();

            if (!tYBTPayQuery.submitData(tTransferData)) {
                // 若查询失败，可获得返回信息
                mErrors = tYBTPayQuery.getErrorInf();
                return false;
            }
            else {
                getNoticeNo = (String) tYBTPayQuery.getResult().getValueByName("GetNoticeNo");
            }
        }
        catch (Exception e) {
            mErrors.addOneError("获取传入的保单信息失败");
            System.out.println("---获取保单信息失败---");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 程序处理入口
     * 
     * @return 收费处理成功标记
     */
    private boolean dealData() {

        LJSPaySchema tLJSPaySchema = new LJSPaySchema();
        tLJSPaySchema.setGetNoticeNo(getNoticeNo);
        VData tVData = new VData();
        tVData.add(tLJSPaySchema);

        VerDuePayFeeQueryUI tVerDuePayFeeQueryUI = new VerDuePayFeeQueryUI();
        if (!tVerDuePayFeeQueryUI.submitData(tVData, "QUERYDETAIL")) {

        }
        else {
            tVData.clear();

            tVData = tVerDuePayFeeQueryUI.getResult();
            SSRS tSSRS = (SSRS) tVData.getObjectByObjectName("SSRS", 0);

            LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
            for (int row = 1; row <= tSSRS.MaxRow; row++) {
                LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
                tLJTempFeeSchema.setRiskCode(tSSRS.GetText(row, 1));
                tLJTempFeeSchema.setPayMoney(tSSRS.GetText(row, 2));
                tLJTempFeeSchema.setOtherNo(tSSRS.GetText(row, 3));
                tLJTempFeeSet.add(tLJTempFeeSchema);
            }

            if (!setLJTempFee(tLJTempFeeSet)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 设置更新暂收表数据
     * 
     * @param tLJTempFeeSet集合
     * @return 成功标记
     */
    private boolean setLJTempFee(LJTempFeeSet tLJTempFeeSet) {

        String agentCode = "";
        String agentGroup = "";
        String policCom = "";

        ExeSQL tExeSQL = new ExeSQL();
        String querySql = "select a.AgentCode,(select agentgroup from laagent where agentcode=a.AgentCode),a.ManageCom,a.bankaccno,a.accname "
                + "from lccont a ,ljspay b where a.contno=b.otherno "
                + "and conttype='1' and b.othernotype='2' and b.otherno='"
                + contNo
                + "' and b.getnoticeno='"
                + getNoticeNo
                + "'"
                + "union select a.AgentCode,(select agentgroup from laagent where agentcode=a.AgentCode),a.ManageCom,a.bankaccno,a.accname "
                + "from lcgrpcont a ,ljspaygrp b where a.grpcontno=b.grpcontno "
                + "and a.grpcontno<>'00000000000000000000' "
                + " and b.sumduepaymoney<>0 and b.grpcontno='"
                + contNo
                + "' and b.getnoticeno='" + getNoticeNo + "'";
        SSRS tSSRS = tExeSQL.execSQL(querySql);
        if (tSSRS.MaxRow == 0) {
            this.mErrors.addOneError("未查到保单相关信息");
            System.out.println("---未查到保单相关信息---");
            return false;
        }
        else {
            agentCode = tSSRS.GetText(1, 1);
            agentGroup = tSSRS.GetText(1, 2);
            policCom = tSSRS.GetText(1, 3);
        }

        String serNo = PubFun1.CreateMaxNo("SERIALNO", PubFun.getNoLimit(mGlobalInput.ManageCom));

        LJTempFeeSet inLJTempFeeSet = new LJTempFeeSet();

        for (int row = 1; row <= tLJTempFeeSet.size(); row++) {
            LJTempFeeSchema tLJTempFeeSchema = tLJTempFeeSet.get(row);
            tLJTempFeeSchema.setTempFeeNo(getNoticeNo);
            tLJTempFeeSchema.setTempFeeType("18"); // 银保通待确认续期收费数据
            tLJTempFeeSchema.setPayIntv("");
            tLJTempFeeSchema.setOtherNoType("0");
            tLJTempFeeSchema.setPayDate(PubFun.getCurrentDate());
            tLJTempFeeSchema.setEnterAccDate(PubFun.getCurrentDate());
            tLJTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
            tLJTempFeeSchema.setConfMakeTime(PubFun.getCurrentTime());
            tLJTempFeeSchema.setManageCom(manageCom);
            tLJTempFeeSchema.setPolicyCom(policCom);
            tLJTempFeeSchema.setAgentCode(agentCode);
            tLJTempFeeSchema.setAgentGroup(agentGroup);
            tLJTempFeeSchema.setConfFlag("0");
            tLJTempFeeSchema.setSerialNo(serNo);
            tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
            tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
            tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
            tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
            tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
            inLJTempFeeSet.add(tLJTempFeeSchema);
        }

        LJTempFeeClassSchema inLJTempFeeClassSchema = new LJTempFeeClassSchema();
        inLJTempFeeClassSchema.setTempFeeNo(getNoticeNo);
        inLJTempFeeClassSchema.setPayMode(payMode);
        inLJTempFeeClassSchema.setPayMoney(payMoney);
        inLJTempFeeClassSchema.setPayDate(PubFun.getCurrentDate());
        inLJTempFeeClassSchema.setEnterAccDate(PubFun.getCurrentDate());
        inLJTempFeeClassSchema.setConfFlag("0");
        inLJTempFeeClassSchema.setSerialNo(serNo);
        inLJTempFeeClassSchema.setManageCom(manageCom);
        inLJTempFeeClassSchema.setPolicyCom(policCom);
        inLJTempFeeClassSchema.setBankCode(bankCode);
        inLJTempFeeClassSchema.setBankAccNo(bankAccNo);
        inLJTempFeeClassSchema.setAccName(accName);
        inLJTempFeeClassSchema.setConfMakeDate(PubFun.getCurrentDate());
        inLJTempFeeClassSchema.setConfMakeTime(PubFun.getCurrentTime());
        inLJTempFeeClassSchema.setOperator(mGlobalInput.Operator);
        inLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
        inLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
        inLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
        inLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
        if (!payMode.equals("1")) {
            querySql = "select bankaccno from ldfinbank where managecom='" + manageCom + "' and bankcode='" + insBankCode
                    + "' and operater='YBT'";
            tSSRS = tExeSQL.execSQL(querySql);
            if (tSSRS.MaxRow == 0) {
                this.mErrors.addOneError("未查到相应的本方银行账户");
                System.out.println("---未查到相应的本方银行账户---");
                return false;
            }
            else {
                inLJTempFeeClassSchema.setInsBankAccNo(tSSRS.GetText(1, 1));
                inLJTempFeeClassSchema.setInsBankCode(insBankCode);
            }
        }

        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setGetNoticeNo(getNoticeNo);
        if (!tLJSPayDB.getInfo()) {
            this.mErrors.addOneError("未查到保单应收数据");
            System.out.println("---未查到保单应收数据---");
            return false;
        }
        if(tLJSPayDB.getBankOnTheWayFlag() == null || tLJSPayDB.getBankOnTheWayFlag().equals("1")){
        	this.mErrors.addOneError("保单已通过银行转账方式进行收费");
            System.out.println("---保单已通过银行转账方式进行收费---");
            return false;
        }
        tLJSPayDB.setBankOnTheWayFlag("1");
        tLJSPayDB.setOperator(mGlobalInput.Operator);
        tLJSPayDB.setModifyDate(PubFun.getCurrentDate());
        tLJSPayDB.setModifyTime(PubFun.getCurrentTime());

        this.map.put(inLJTempFeeSet, "INSERT");
        this.map.put(inLJTempFeeClassSchema, "INSERT");
        this.map.put(tLJSPayDB.getSchema(), "UPDATE");

        return true;
    }

    /**
     * 设置更新数据
     * 
     * @return 成功标记
     */
    private boolean prepareOutputData() {

        try {
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            mErrors.addOneError("在准备往后层处理所需要的数据时出错");
            return false;
        }
        return true;
    }

    /**
     * 修改续期应收备份表状态
     * 
     * @return 暂全部返回成功
     */
    /*
    private boolean finUrgeVerify() {

        LJSPaySchema tLJSPaySchema = new LJSPaySchema();
        tLJSPaySchema.setGetNoticeNo(getNoticeNo);
        VData ttVdata = new VData();
        ttVdata.add(tLJSPaySchema);
        ttVdata.add(mGlobalInput);
        ChangeFinFeeStateBL bl = new ChangeFinFeeStateBL("4");
        if (!bl.submitData(ttVdata, "")) {
            mErrors.addOneError("收费成功，但续期应收数据处理失败");
            System.out.println("--收费成功，但续期应收数据处理失败--");
            return true;
        }
        return true;
    }*/

    /**
     * 调用方法举例
     */
    public static void main(String[] arr) {

        CErrors cErrors = new CErrors();

        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "8651";
        tGI.Operator = "YBT";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("ContNo", "000916021000004");
        tTransferData.setNameAndValue("PayMoney", "550.00");
        tTransferData.setNameAndValue("BankCode", "49");
        tTransferData.setNameAndValue("BankAccNo", "4");
        tTransferData.setNameAndValue("AccName", "4");
        tTransferData.setNameAndValue("PayMode", "4");
        tTransferData.setNameAndValue("GlobalInput", tGI);

        YBTTempFee tYBTTempFee = new YBTTempFee();

        if (!tYBTTempFee.submitData(tTransferData)) {
            // 收费失败，可获得返回信息
            cErrors = tYBTTempFee.getErrorInf();
            System.out.println(cErrors.getLastError());
        }
    }
}
