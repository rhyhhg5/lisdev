package com.sinosoft.lis.finfee;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

public class GetAdvanceBL
{
    private LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();

    private LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();

    private GlobalInput mGlobalInput = new GlobalInput();

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mInputData = new VData();

    private MMap map = new MMap();

    private String manCom = "";
    
    String no = "";
    String TempNo = "";
    
    String tLimit = "";
    String serNo = "";

    public GetAdvanceBL()
    {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        System.out.println("----GetAdvanceBL--Start----");
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("----dealData--Start----");
        if (!dealData())
        {
            return false;
        }
        if (!prepareOutputData())
        {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, cOperate))
        {
            //错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        System.out.println("----GetAdvanceBL--End----");
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mGlobalInput.setSchema((GlobalInput) cInputData
                .getObjectByObjectName("GlobalInput", 0));
        this.tLJTempFeeSet = (LJTempFeeSet) cInputData.getObjectByObjectName(
                "LJTempFeeSet", 0);
        this.tLJTempFeeClassSet = (LJTempFeeClassSet) cInputData
                .getObjectByObjectName("LJTempFeeClassSet", 0);
        if (this.tLJTempFeeSet == null || this.tLJTempFeeClassSet == null)
        {
            //错误处理
            System.out.println("传入的收费信息为空");
            CError tError = new CError();
            tError.moduleName = "PayAdvanceBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "请输入查询条件!";
            this.mErrors.addOneError(tError);
            return false;
        }
        this.manCom = mGlobalInput.ManageCom;
        for (int i = manCom.length(); i < 8; i++)
        {
            manCom = manCom + "0";
        }
        this.tLimit = PubFun.getNoLimit(manCom);
        this.serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        return true;
    }

    private boolean dealData()
    {
        for (int i = 0; i < tLJTempFeeSet.size(); i++)
        {
            LJTempFeeSchema tLJTempFeeSchema = tLJTempFeeSet.get(i + 1);
            this.no = "YS" + PubFun1.CreateMaxNo("YSNO", 8) ;
            this.TempNo = no + "801";
            if (!setLJTempFee(tLJTempFeeSchema))
            {
                return false;
            }
            LJTempFeeClassSchema tLJTempFeeClassSchema = tLJTempFeeClassSet.get(i + 1);
            setLJTempFeeClass(tLJTempFeeClassSchema);
        }
        this.map.put(tLJTempFeeSet, "INSERT");
        this.map.put(tLJTempFeeClassSet, "INSERT");
        return true;
    }

    private boolean setLJTempFee(LJTempFeeSchema tLJTempFeeSchema)
    {
        tLJTempFeeSchema.setTempFeeNo(TempNo);
        tLJTempFeeSchema.setOtherNo(no);
        tLJTempFeeSchema.setTempFeeType("YS");
        tLJTempFeeSchema.setRiskCode("000000");
        tLJTempFeeSchema.setPayIntv(0);
        tLJTempFeeSchema.setOtherNoType("YS");
        tLJTempFeeSchema.setManageCom(manCom);
        tLJTempFeeSchema.setPolicyCom(manCom);
        tLJTempFeeSchema.setPolicyCom(manCom);
        tLJTempFeeSchema.setConfFlag("0");
        
        tLJTempFeeSchema.setSerialNo(serNo);

        String agentCodet = tLJTempFeeSchema.getAgentCode();
        String agSql = "select agentgroup from laagent where agentcode='"
                + agentCodet + "'";
        System.out.println(agSql);
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(agSql);
        if (tSSRS.MaxRow == 0)
        {
            System.out.println("setLJTempFee--agentgroup不存在");
            CError tError = new CError();
            tError.moduleName = "PayAdvanceBL";
            tError.functionName = "setLJTempFee";
            tError.errorMessage = "未找到业务员代码!";
            this.mErrors.addOneError(tError);
            return false;
        }
        tLJTempFeeSchema.setAgentGroup(tSSRS.GetText(1, 1));
        tLJTempFeeSchema.setOperator(mGlobalInput.Operator);
        tLJTempFeeSchema.setMakeDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setMakeTime(PubFun.getCurrentTime());
        tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
        tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
        return true;
    }

    private void setLJTempFeeClass(LJTempFeeClassSchema tLJTempFeeClassSchema)
    {
        tLJTempFeeClassSchema.setTempFeeNo(TempNo);
        tLJTempFeeClassSchema.setManageCom(manCom);
        tLJTempFeeClassSchema.setPolicyCom(manCom);
        tLJTempFeeClassSchema.setOperator(mGlobalInput.Operator);
        tLJTempFeeClassSchema.setMakeDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setMakeTime(PubFun.getCurrentTime());
        tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
        tLJTempFeeClassSchema.setConfFlag("0");
        tLJTempFeeClassSchema.setSerialNo(serNo);
    }

    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(map);
        }
        catch (Exception ex)
        {
            // @@错误处理
            System.out.println(ex.getMessage());
            CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
            return false;
        }
        return true;
    }

    public static void main(String[] arr)
    {
        //tLJTempFeeSchema.getAgentCode();
        //        System.out.println(PubFun1.CreateMaxNo("YSNO","YS"));
    }
}