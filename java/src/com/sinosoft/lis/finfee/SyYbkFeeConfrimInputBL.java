package com.sinosoft.lis.finfee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.f1j.ss.BookModelImpl;
import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LYSendToSettleSchema;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LYSendToSettleSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class SyYbkFeeConfrimInputBL
{
    private VData mInputData = new VData();
    /** 提交数据的容器 */
    private MMap map = new MMap();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private String mFilePath = "";

    private BookModelImpl book = new BookModelImpl();

    //业务数据
    private TransferData inTransferData = new TransferData();

    private String fileName = "";
    LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
     


    
    LYSendToSettleSet  mLYSendToSettleSet = new LYSendToSettleSet(); 
     LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
     

    private GlobalInput tG = new GlobalInput();

    private List exList = new ArrayList();
    LYSendToSettleSchema tLYSendToSettleSchema;
    LJTempFeeClassSchema mLJTempFeeClassSchema;
    

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"READ"和""
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
        {
            return false;
        }
        System.out.println("---End getInputData---");

        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        System.out.println("---End dealData---");

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * 
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            inTransferData = (TransferData) mInputData.getObjectByObjectName(
                    "TransferData", 0);
            tG = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput",
                    0);

            if (tG == null || inTransferData == null)
            {
                CError tError = new CError();
                tError.moduleName = "SyYbkFeeConfrimInputBL";
                tError.functionName = "checkList";
                tError.errorMessage = "接收数据失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "SyYbkFeeConfrimInputBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
 {
		try {
			// 获取银行文件数据
			if (mOperate.equals("READ")) {
				// 获取返回文件名称
				fileName = (String) inTransferData.getValueByName("fileName");
				fileName = fileName.replace('\\', '/');
				// 读取银行返回文件，公共部分
				System.out.println("---开始读取文件---");
				if (!readBatchPayExcel(fileName)) {
					return false;
				}
				if (!checkList()) {
					return false;
				}
				if (!setSettleSchema()) {
					return false;
				}

				 mInputData.clear();
				 map.put(this.mLJTempFeeClassSet, "UPDATE");
				 map.put(this.mLYSendToSettleSet, "INSERT");
				 mInputData.add(map);
				System.out.println("Start PubSubmit BLS Submit...");
		        PubSubmit tPubSubmit = new PubSubmit();
		        if (!tPubSubmit.submitData(mInputData, mOperate)) {
		          // @@错误处理
		          this.mErrors.copyAllErrors(tPubSubmit.mErrors);
		          return false;
		        }
		        System.out.println("End PubSubmit BLS Submit...");

			}
		} catch (Exception e) {
			// @@错误处理
			e.printStackTrace();
			CError tError = new CError();
			tError.moduleName = "SyYbkFeeConfrimInputBL";
			tError.functionName = "dealData";
			tError.errorMessage = "数据处理错误:" + e.getMessage();
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

    /**
     * 读取付费导入文件
     * @param fileName
     * @return
     */
    private boolean readBatchPayExcel(String fileName)
    {
        try
        {
            mFilePath = fileName;
            book.initWorkbook();//初始
            book.read(mFilePath);
            int tSheetNums = book.getNumSheets();//读去sheet的个数，就是一个excel包含几个sheet
            System.out.println("tSheetNums===" + tSheetNums);

            for (int sheetNum = 0; sheetNum < tSheetNums; sheetNum++)
            {
                book.setSheet(sheetNum);
                int tLastCol = book.getLastCol();//总共多少列，他的列数从０开始，比如有４列，那tLastCol的结果＝３
                int tLastRow = book.getLastRow();//总共多少行，他的行也是从０开始，比如有２行，那tLastRow的结果＝１
                System.out.println("tLastCol===" + tLastCol);
                System.out.println("tLastRow===" + tLastRow);
                String colValue = "";
                //按列循环
                for (int j = 1; j <= tLastRow; j++)
                {//循环总行数
                    Map tMap = new HashMap();
                    for (int i = 0; i <= tLastCol; i++)
                    {//循环总列数
                        if (book.getText(j, i) != null)
                        {
                            colValue = (String) book.getText(j, i).trim();
                        }
                        else
                        {
                            colValue = "";
                        }
                        switch (i)
                        {
                            case 1:
                                tMap.put("TempfeeNo", colValue);
                                break;
                            case 2:
                                tMap.put("OtherNo", colValue);
                                break;
                            case 3:
                                tMap.put("PayMoney", colValue);
                                break;
                            case 4:
                                tMap.put("ConfDate", colValue);
                                break;
                            case 5:
                                tMap.put("InsBankCode", colValue);
                                break;
                            case 6:
                                tMap.put("InsBankAccNo", colValue);
                                break;
                        }

                    }
                    tMap.put("ManageCom", tG.ManageCom);
                    exList.add(tMap);
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "SyYbkFeeConfrimInputBL";
            tError.functionName = "readExcelFile";
            tError.errorMessage = "读取导入文件错误:" + ex.getMessage();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean checkList()
 {
		ExeSQL mExeSql = new ExeSQL();
		if (exList.size() == 0) {
			System.out.println("-----exList-size-0-----");
			CError tError = new CError();
			tError.moduleName = "SyYbkFeeConfrimInputBL";
			tError.functionName = "checkList";
			tError.errorMessage = "未发现导入文件中存在付费信息!";
			this.mErrors.addOneError(tError);
			return false;
		}

		for (int k = 0; k < exList.size(); k++) {
			Map tMap = (Map) exList.get(k);
			
			// 判断Excel信息是否正确
			if ("".equals(tMap.get("TempfeeNo"))) {
				CError tError = new CError();
				tError.moduleName = "SyYbkFeeConfrimInputBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k+1) + "行，暂交费号不能为空!";
				this.mErrors.addOneError(tError);
				return false;
			} else if ("".equals(tMap.get("OtherNo"))) {

				CError tError = new CError();
				tError.moduleName = "SyYbkFeeConfrimInputBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k+1)+ "行，业务号不能为空!";
				this.mErrors.addOneError(tError);
				return false;

			} else if ("".equals(tMap.get("PayMoney"))) {

				CError tError = new CError();
				tError.moduleName = "SyYbkFeeConfrimInputBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k+1)+ "行，结算金额不能为空!";
				this.mErrors.addOneError(tError);
				return false;
			} else if ("".equals(tMap.get("ConfDate"))) {
				CError tError = new CError();
				tError.moduleName = "SyYbkFeeConfrimInputBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k+1) + "行，结算日期不能为空!";
				this.mErrors.addOneError(tError);
				return false;
			}else if ("".equals(tMap.get("InsBankCode"))) {
					CError tError = new CError();
					tError.moduleName = "SyYbkFeeConfrimInputBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k+1) + "行，本方开户银行不能为空!";
					this.mErrors.addOneError(tError);
					return false;
				}else if ("".equals(tMap.get("InsBankAccNo"))) {
					CError tError = new CError();
					tError.moduleName = "SyYbkFeeConfrimInputBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k+1) + "行，本方银行账户不能为空!";
					this.mErrors.addOneError(tError);
					return false;
				}else {
				String mAGetSql = "select a.Tempfeeno,a.otherno,b.PayMoney from ljtempfee a ,ljtempfeeclass b  where a.tempfeeno=b.tempfeeno and   a.tempfeeno = '"
						+ tMap.get("TempfeeNo") + "'";
				SSRS xAGetResult = mExeSql.execSQL(mAGetSql);
				if (xAGetResult.MaxRow != 0) {
					String xResult[] = xAGetResult.getRowData(1);
					// 业务号码
					if (!xResult[1].trim().equals(tMap.get("OtherNo"))) {
						CError tError = new CError();
						tError.moduleName = "SyYbkFeeConfrimInputBL";
						tError.functionName = "checkList";
						tError.errorMessage = "第" + (k+1)+ "行，暂交费业务号为"
								+ tMap.get("TempfeeNo") + "的数据，业务号码与暂交费号不符!";
						this.mErrors.addOneError(tError);
						return false;
					}
					
					String mSendtosettllSql = "select 1 from lysendtosettle  where paycode = '"
							+ tMap.get("TempfeeNo") + "'";
					SSRS mSendtosettlResult = mExeSql.execSQL(mSendtosettllSql);
					if (mSendtosettlResult.MaxRow != 0) {
						// 业务号码
							CError tError = new CError();
							tError.moduleName = "SyYbkFeeConfrimInputBL";
							tError.functionName = "checkList";
							tError.errorMessage = "第" + (k+1)+ "行，暂交费业务号为"
									+ tMap.get("TempfeeNo") + "的数据，已结算!";
							this.mErrors.addOneError(tError);
							return false;
					}					
					
					
					// 号码类型
					if (!xResult[2].trim().equals(tMap.get("PayMoney"))) {
						CError tError = new CError();
						tError.moduleName = "SyYbkFeeConfrimInputBL";
						tError.functionName = "checkList";
						tError.errorMessage = "第" + (k+1) + "行，暂交费业务号为"
								+ tMap.get("TempfeeNo") + "的数据，结算金额与应付号码类型不符!";
						this.mErrors.addOneError(tError);
						return false;
					}
				}

			}
				// 校验归集账号是否正确
				if (!"".equals(tMap.get("InsBankAccNo"))) {
					String insBankAccSql = "select '1' from LDFinBank where FinFlag='S' and  bankaccno='"
							+ tMap.get("InsBankAccNo") + "' and bankcode='"+  tMap.get("InsBankCode")+"' with ur";
					SSRS mSSRS = mExeSql.execSQL(insBankAccSql);
					if (mSSRS.MaxNumber == 0) {
						CError tError = new CError();
						tError.moduleName = "SyYbkFeeConfrimInputBL";
						tError.functionName = "checkList";
						tError.errorMessage = "第" + (k+1) + "行，暂交费号为"
								+ tMap.get("TempfeeNo") + "的数据，本方银行帐号不存在！";
						this.mErrors.addOneError(tError);
						return false;
					}
			}

			try {
				if (!"".equals(tMap.get("PayMoney").toString())) {
					 if (Double.parseDouble(tMap.get("PayMoney").toString())
					 <= 0) {
					 CError tError = new CError();
					 tError.moduleName = "SyYbkFeeConfrimInputBL";
					 tError.functionName = "checkList";
					 tError.errorMessage = "第" + (k+1) + "行，暂交费号为"
					 + tMap.get("TempfeeNo") + "的数据，给付金额错误，金额必须大于零!";
					 this.mErrors.addOneError(tError);
					 return false;
					 }
				}
			} catch (Exception ex) {
				CError tError = new CError();
				tError.moduleName = "SyYbkFeeConfrimInputBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k+1) + "行，暂交费号为"
						+ tMap.get("TempfeeNo") + "的数据，结算金额格式错误!";
				this.mErrors.addOneError(tError);
				return false;
			}
			 if (!PubFun.checkDateForm(tMap.get("ConfDate").toString()))
			 {
			 CError tError = new CError();
			 tError.moduleName = "SyYbkFeeConfrimInputBL";
			 tError.functionName = "checkList";
			 tError.errorMessage = "第" + (k+1) + "行，暂交费号为"
						+ tMap.get("TempfeeNo") + "的数据，结算日期格式错误!";
			 this.mErrors.addOneError(tError);
			 return false;
			 }

		}

		return true;
	}
    /**
     * 给LJAGetSchema和LJFiGetSchema赋值
     * @return
     */
    private boolean setSettleSchema()
    {
	 	for (int j = 0; j < exList.size(); j++) {
	 	// 保单信息
	 		 LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
	 		  LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
	 	      LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
	 		Map xMap = (Map) exList.get(j);
			if(!"".equals((String)xMap.get("TempfeeNo")) && xMap.get("TempfeeNo")!=null){
	 	      
	 	      String strSqlLjtempfee = "select * from ljtempfee where tempfeeno='" + (String)xMap.get("TempfeeNo")+"'";
	 	      System.out.println(strSqlLjtempfee);
	 	      tLJTempFeeSet = tLJTempFeeDB.executeQuery(strSqlLjtempfee);
	 	      
	 	      String strSqlLjtempfeeClass = "select * from ljtempfeeclass where tempfeeno='" + (String)xMap.get("TempfeeNo")+"'";
	 	      System.out.println(strSqlLjtempfeeClass);
	 	    
	 	      tLJTempFeeClassSet = tLJTempFeeClassDB.executeQuery(strSqlLjtempfeeClass);
	 	   
	 	      if (tLJTempFeeDB.mErrors.needDealError() == true)
	 	      {
	 	      // @@错误处理
	 	      this.mErrors.copyAllErrors(tLJTempFeeDB.mErrors);
	 	      CError tError = new CError();
	 	      tError.moduleName = "LJAGetQueryBL";
	 	      tError.functionName = "queryData";
	 	      tError.errorMessage = "暂交费查询失败!";
	 	      this.mErrors.addOneError(tError);
	 	      tLJTempFeeSet.clear();
	 	      return false;
	 	    }
	 	    if (tLJTempFeeSet.size() == 0)
	 	    {
	 	      // @@错误处理
	 	      CError tError = new CError();
	 	      tError.moduleName = "SyYbkFeeConfrimInputBL.java";
	 	      tError.functionName = "queryData";
	 	      tError.errorMessage = "可能是：未找到相关数据,暂交费号有误 !";
	 	      this.mErrors.addOneError(tError);
	 	      tLJTempFeeSet.clear();
	 	      return false;
	 	    }
	 	  
			
		   for(int i =1;i<=tLJTempFeeClassSet.size();i++){
			   
	 	    	mLJTempFeeClassSchema = tLJTempFeeClassSet.get(i);
	 	      }
			}
			
			
	 		 //生成送银行表数据
			  tLYSendToSettleSchema = new LYSendToSettleSchema();
	 	      //设置统一的批次号
	 	      tLYSendToSettleSchema.setSerialNo(tLJTempFeeSet.get(1).getTempFeeNo());
	 	      //收费标记
	 	      tLYSendToSettleSchema.setDealType("A");
	 	      tLYSendToSettleSchema.setPayCode(tLJTempFeeSet.get(1).getTempFeeNo());
	 	      tLYSendToSettleSchema.setPayType(tLJTempFeeSet.get(1).getTempFeeType());
	 	      tLYSendToSettleSchema.setMedicalCode(mLJTempFeeClassSchema.getBankCode());
	 	      tLYSendToSettleSchema.setAccName(mLJTempFeeClassSchema.getAccName());
	 	      tLYSendToSettleSchema.setAccNo(mLJTempFeeClassSchema.getBankAccNo());

	 	      //因为改为前台录入财务数据，保单表中不一定有数据，所以不再从中取信息
	 	      if(tLJTempFeeSet.get(1).getTempFeeType().equals("1")){
	 	    	 tLYSendToSettleSchema.setNoType("9");
	 	      }else{
	 	    	 tLYSendToSettleSchema.setNoType("2");
	 	      }
	 	      
	 	      tLYSendToSettleSchema.setComCode(mLJTempFeeClassSchema.getManageCom());
	 	      tLYSendToSettleSchema.setPayMoney(mLJTempFeeClassSchema.getPayMoney());
	 	      tLYSendToSettleSchema.setConfMoney(mLJTempFeeClassSchema.getPayMoney());
	 	      tLYSendToSettleSchema.setSendDate(PubFun.getCurrentDate());
	 	      tLYSendToSettleSchema.setSendTime(PubFun.getCurrentTime());
	 	      tLYSendToSettleSchema.setDoType("1");
	 	      //因为没有为发送银行盘表设计操作员字段，所以暂时保存在备注字段中，add by Minim at 2004-2-5
	 	      tLYSendToSettleSchema.setRemark("YBK");
	 	      tLYSendToSettleSchema.setOperator("YBK");
	 	      tLYSendToSettleSchema.setModifyDate(PubFun.getCurrentDate());
	 	      tLYSendToSettleSchema.setModifyTime(PubFun.getCurrentTime());
	 	      tLYSendToSettleSchema.setMakeDate(PubFun.getCurrentDate());
	 	      tLYSendToSettleSchema.setMakeTime(PubFun.getCurrentTime());
	 	      tLYSendToSettleSchema.setSuccFlag("0");
	 	      tLYSendToSettleSchema.setConfDate((String)xMap.get("ConfDate"));
	 	      tLYSendToSettleSchema.setRiskCode(tLJTempFeeSet.get(1).getRiskCode());
	 	      tLYSendToSettleSchema.setDealState("2");
	 	     tLYSendToSettleSchema.setInsBankCode((String)xMap.get("InsBankCode"));
	 	    tLYSendToSettleSchema.setInsAccNo((String)xMap.get("InsBankAccNo"));
	 	      mLYSendToSettleSet.add(tLYSendToSettleSchema);
	 	     
	 	     mLJTempFeeClassSchema.setInsBankCode((String)xMap.get("InsBankCode"));
	 	    mLJTempFeeClassSchema.setInsBankAccNo((String)xMap.get("InsBankAccNo"));
	 	   mLJTempFeeClassSet.add(mLJTempFeeClassSchema);
	 	    //mLJTempFeeClassSet.set(tLJTempFeeClassSet);
	 	}
	 	
	 	return true;
		
		
		}
}
