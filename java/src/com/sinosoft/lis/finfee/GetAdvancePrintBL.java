package com.sinosoft.lis.finfee;

/**
 * <p>Title: AgentSystem</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.WriteToExcel;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GetAdvancePrintBL
{
    /**错误信息容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = null;
    private String mSql = null;
    private String mOutXmlPath = null;
    private String StartDate = "";
    private String EndDate = "";

    private String mCurDate = PubFun.getCurrentDate();
    private String mCurTime = PubFun.getCurrentTime();
    SSRS tSSRS=new SSRS();
    public GetAdvancePrintBL()
    {
      }
    public boolean submitData(VData cInputData, String operate)
    {

        if(!getInputData(cInputData))
        {
            return false;
        }

        if(!checkData())
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }

        return true;
    }


    /**
     * 校验操作是否合法
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     * 处理业务数据
     *
     * @return boolean：true提交成功, false提交失败
     */
    private boolean dealData()
    {
        ExeSQL tExeSQL = new ExeSQL();
        System.out.println("BL->dealDate()");
        mSql="select c.grpname,c.prtno,c.grpcontno,a.confmakedate," +
                "(select riskname from lmriskapp where riskcode = a.riskcode)," +
                "(select costcenter from labranchgroup where agentgroup = c.agentgroup)" +
                ",a.paymoney,c.cvalidate " +
                " from ljtempfee a,ljtempfeeclass b,lcgrpcont c " +
                " where a.tempfeeno = b.tempfeeno and a.otherno=c.grpcontno " +
                " and b.paymode='YS' and a.confdate is not null ";
        mSql = mSql + " and a.managecom like '" + mGI.ManageCom + "%' ";
        mSql = mSql + " and c.cvalidate between '" + StartDate + "' and '" + EndDate + "' ";
        System.out.println(mSql);
        System.out.println(mOutXmlPath);
        tSSRS = tExeSQL.execSQL(mSql);

        if(tExeSQL.mErrors.needDealError())
        {
            System.out.println(tExeSQL.mErrors.getErrContent());

            CError tError = new CError();
            tError.moduleName = "GetAdvancePrintBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到需要下载的数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        if(tSSRS.getMaxRow()==0)
        {
           
            CError tError = new CError();
            tError.moduleName = "GetAdvancePrintBL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有符合条件的数据，请重新输入条件";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
       String[][] mToExcel = new String[tSSRS.getMaxRow() + 15][30];
        mToExcel[0][0] = "预收保费报表";
        mToExcel[1][0] = "编制单位：";
        mToExcel[2][6] = "NO:"+mCurDate+" "+mCurTime;
        
        
        mToExcel[3][0] = "投保单位名称";
        mToExcel[3][1] = "保单印刷号";
        mToExcel[3][2] = "保单号";
        mToExcel[3][3] = "交费时间";        
        mToExcel[3][4] = "险种名称";
        mToExcel[3][5] = "成本中心";
        mToExcel[3][6] = "金额";        
        mToExcel[3][7] = "确认保费收入时间";


        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row + 3][col - 1] = tSSRS.GetText(row, col);
            }
        }
           
        mToExcel[tSSRS.getMaxRow()+5][0] = "制表";
        mToExcel[tSSRS.getMaxRow()+5][4] = "日期："+mCurDate;
        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(mOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * getInputData
     *将外部传入的数据分解到本类的属性中
     * @param cInputData VData：submitData中传入的VData对象
     * @return boolean：true提交成功, false提交失败
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        TransferData tf = (TransferData) data.getObjectByObjectName("TransferData", 0);

        if(mGI == null || tf == null)
        {
            CError tError = new CError();
            tError.moduleName = "GetAdvancePrintBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }        
        mOutXmlPath = (String) tf.getValueByName("OutXmlPath");
        StartDate = (String) tf.getValueByName("StartDate");
        EndDate = (String) tf.getValueByName("EndDate");

        if(StartDate == null || EndDate == null || mOutXmlPath == null)
        {
            CError tError = new CError();
            tError.moduleName = "GetAdvancePrintBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的信息不完整2";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }
        return true;
    }

    /*private String getName(String pmAgentCom)
    {
      String tSQL = "";
      String tRtValue="";
      tSQL = "select  name from lacom where agentcom='" + pmAgentCom +"'";
      SSRS tSSRS = new SSRS();
      ExeSQL tExeSQL = new ExeSQL();
      tSSRS = tExeSQL.execSQL(tSQL);
      if(tSSRS.getMaxRow()==0)
      {
      tRtValue="";
      }
      else
      tRtValue=tSSRS.GetText(1, 1);
      return tRtValue;
  }*/

    public static void main(String[] args)
    {
    }
}
