package com.sinosoft.lis.finfee;

import java.io.IOException;

import com.f1j.ss.BookModelImpl;
import com.f1j.ss.ReadParams;
import com.f1j.util.F1Exception;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LAAgentSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class GetAdvBatchReadFileBL
{
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private String mFilePath = "";

    private BookModelImpl book = new BookModelImpl();

    //业务数据
    private TransferData inTransferData = new TransferData();

    private String fileName = "";

    private LJTempFeeSet outLJTempFeeSet = new LJTempFeeSet();

    private LJTempFeeClassSet outLJTempFeeClassSet = new LJTempFeeClassSet();

    private GlobalInput tG = new GlobalInput();

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"READ"和""
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
        {
            return false;
        }
        System.out.println("---End getInputData---");

        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        System.out.println("---End dealData---");

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * 
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            inTransferData = (TransferData) mInputData.getObjectByObjectName(
                    "TransferData", 0);
            tG = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput",
                    0);
        }
        catch (Exception e)
        {
            // @@错误处理
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        try
        {
            //获取银行文件数据
            if (mOperate.equals("READ"))
            {
                //获取返回文件名称
                fileName = (String) inTransferData.getValueByName("fileName");
                fileName = fileName.replace('\\', '/');
                //读取银行返回文件，公共部分
                System.out.println("---开始读取文件---");
                if (!readTempFeeFile(fileName))
                {
                    return false;
                }

                VData tVData = new VData();
                tVData.clear();
                tVData.add(outLJTempFeeSet);
                tVData.add(outLJTempFeeClassSet);
                tVData.add(tG);
                GetAdvanceBL tGetAdvanceBL = new GetAdvanceBL();
                if (!tGetAdvanceBL.submitData(tVData, mOperate))
                {
                    outLJTempFeeClassSet.clear();
                    mErrors.addOneError(tGetAdvanceBL.mErrors.getLastError());
                    System.out.println("导入失败,请确认导入信息是否正确");
                    return false;
                }
            }
        }
        catch (Exception e)
        {
            // @@错误处理
            System.out.println(e.getMessage());
            CError.buildErr(this, "数据处理错误:" + e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * 读取银行编码文件
     * @param fileName
     * @return
     */
    private boolean readTempFeeFile(String fileName)
    {
        try
        {
            mFilePath = fileName;
            book.initWorkbook();//初始
            book.read(mFilePath, new ReadParams());
            int tSheetNums = book.getNumSheets();//读去sheet的个数，就是一个excel包含几个sheet
            System.out.println("tSheetNums===" + tSheetNums);
            LAAgentDB tLAAgentDB = new LAAgentDB();
            LAAgentSet tLAAgentSet = new LAAgentSet();
            for (int sheetNum = 0; sheetNum < tSheetNums; sheetNum++)
            {
                book.setSheet(sheetNum);
                int tLastCol = book.getLastCol();//总共多少列，他的列数从０开始，比如有４列，那tLastCol的结果＝３
                int tLastRow = book.getLastRow();//总共多少行，他的行也是从０开始，比如有２行，那tLastRow的结果＝１
                System.out.println("tLastCol===" + tLastCol);
                System.out.println("tLastRow===" + tLastRow);
                String colValue = "";
                //按列循环
                for (int j = 1; j <= tLastRow; j++)
                {//循环总行数
                    LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
                    LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
                    System.out.println("业务员代码" + book.getText(j, 4));
                    for (int i = 0; i <= tLastCol; i++)
                    {//循环总列数．
                        if (book.getText(j, i) != null
                                || !book.getText(j, i).trim().equals(""))
                        {//判断第j行的第i列是否为空值．
                            System.out.println(book.getText(j, i));//打印出第j行的第i列的值
                            colValue = (String) book.getText(j, i).trim();
                            System.out.println(i + "-" + j + "-" + colValue);
                            if(i==0){
                            	tLAAgentSet = tLAAgentDB.executeQuery("select * from laagent where groupagentcode='"+colValue+"' ");
                            	if(tLAAgentSet.size()>0){
                            		colValue=tLAAgentSet.get(1).getAgentCode();
                            	}
                            	
                            }
                            switch (i)
                            {
                                case 0:
                                    tLJTempFeeSchema.setAgentCode(colValue);
                                case 1:
                                    tLJTempFeeClassSchema
                                            .setAppntName(colValue);
                                    tLJTempFeeSchema.setAPPntName(colValue);
                                    break;
                                case 2:
                                    tLJTempFeeClassSchema.setPayMoney(colValue);
                                    tLJTempFeeSchema.setPayMoney(colValue);
                                    break;
                                case 3:
                                    tLJTempFeeClassSchema.setPayMode(colValue);
                                    break;
                                case 4:
                                    tLJTempFeeClassSchema.setPayDate(colValue);
                                    tLJTempFeeSchema.setPayDate(colValue);
                                    if (!tLJTempFeeClassSchema.getPayMode()
                                            .equals("3"))
                                    {
                                        tLJTempFeeClassSchema
                                                .setEnterAccDate(colValue);

                                        tLJTempFeeSchema
                                                .setEnterAccDate(colValue);
                                        tLJTempFeeSchema
                                                .setConfMakeDate(colValue);
                                        tLJTempFeeClassSchema
                                                .setConfMakeDate(colValue);
                                    }
                                    break;
                                case 5:
                                    tLJTempFeeClassSchema.setBankCode(colValue);
                                    break;
                                case 6:
                                    tLJTempFeeClassSchema.setChequeNo(colValue);
                                    break;
                                case 7:
                                    tLJTempFeeClassSchema
                                            .setBankAccNo(colValue);
                                    break;
                                case 8:
                                    tLJTempFeeClassSchema.setAccName(colValue);
                                    break;
                                case 9:
                                    tLJTempFeeClassSchema
                                            .setInsBankCode(colValue);
                                    break;
                                case 10:
                                    tLJTempFeeClassSchema
                                            .setInsBankAccNo(colValue);
                                    break;
                            }
                        }
                    }
                    if (!checkDate(tLJTempFeeSchema, tLJTempFeeClassSchema))
                    {
                        return false;
                    }
                    outLJTempFeeClassSet.add(tLJTempFeeClassSchema);
                    outLJTempFeeSet.add(tLJTempFeeSchema);
                }
            }
            mInputData.add(inTransferData);

        }
        catch (F1Exception e)
        {
            e.printStackTrace();
            return false;
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
            return false;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
        return true;

    }

    private boolean checkDate(LJTempFeeSchema tLJTempFeeSchema,
            LJTempFeeClassSchema tLJTempFeeClassSchema)
    {
        try
        {
            if (tLJTempFeeSchema.getAPPntName() == null
                    || tLJTempFeeSchema.getAPPntName().equals(""))
            {
                CError.buildErr(this, "交费单位名称为空");
                return false;
            }

            if (tLJTempFeeSchema.getPayMoney() == 0)
            {
                CError.buildErr(this, "缴费金额录入错误");
                return false;
            }

            if (tLJTempFeeClassSchema.getPayMode() == null
                    || tLJTempFeeClassSchema.getPayMode().equals(""))
            {
                CError.buildErr(this, "为录入缴费方式");
                return false;
            }
            else
            {
                int paymode = Integer.parseInt(tLJTempFeeClassSchema
                        .getPayMode());
                if (paymode == 2 || paymode == 3)
                {
                    if (tLJTempFeeClassSchema.getChequeNo() == null
                            || tLJTempFeeClassSchema.getChequeNo().equals(""))
                    {
                        CError.buildErr(this, "转账支票、现金支票必须录入票据号！");
                        return false;
                    }
                }
                if (paymode == 2 || paymode == 3 || paymode == 11
                        || paymode == 12 || paymode == 14 || paymode == 15)
                {
                    if (tLJTempFeeClassSchema.getInsBankAccNo() == null
                            || tLJTempFeeClassSchema.getInsBankAccNo().equals(
                                    ""))
                    {
                        CError.buildErr(this, "非现金、内部转账数据需要录入本方银行账户");
                        return false;
                    }
                    if (tLJTempFeeClassSchema.getInsBankCode() == null
                            || tLJTempFeeClassSchema.getInsBankAccNo().equals(
                                    ""))
                    {
                        CError.buildErr(this, "非现金、内部转账数据需要录入本方银行");
                        return false;
                    }
                    String agSql = "select 1 from ldfinbank where bankcode='"
                            + tLJTempFeeClassSchema.getInsBankCode()
                            + "' and bankaccno='"
                            + tLJTempFeeClassSchema.getInsBankAccNo() + "'";
                    System.out.println(agSql);
                    ExeSQL tExeSQL = new ExeSQL();
                    SSRS tSSRS = tExeSQL.execSQL(agSql);
                    if (tSSRS.MaxRow == 0)
                    {
                        System.out.println("本方银行账户不存在");
                        CError tError = new CError();
                        tError.moduleName = "PayAdvanceBL";
                        tError.functionName = "setLJTempFee";
                        tError.errorMessage = "本方银行账户不存在!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }

                    String agSql2 = "select 1 from ldbank where bankcode='"
                            + tLJTempFeeClassSchema.getBankCode() + "'";
                    System.out.println(agSql2);
                    ExeSQL tExeSQL2 = new ExeSQL();
                    SSRS tSSRS2 = tExeSQL2.execSQL(agSql2);
                    if (tSSRS2.MaxRow == 0)
                    {
                        System.out.println("对方开户银行不存在");
                        CError tError = new CError();
                        tError.moduleName = "PayAdvanceBL";
                        tError.functionName = "setLJTempFee";
                        tError.errorMessage = "对方开户银行不存在!";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }
                else if (paymode == 1 || paymode == 5)
                {
                    tLJTempFeeClassSchema.setBankCode("");
                    tLJTempFeeClassSchema.setBankAccNo("");
                    tLJTempFeeClassSchema.setAccName("");
                    tLJTempFeeClassSchema.setInsBankAccNo("");
                    tLJTempFeeClassSchema.setInsBankCode("");
                    tLJTempFeeClassSchema.setChequeNo("");
                }
                else
                {
                    CError.buildErr(this, "缴费方式错误！");
                    return false;
                }
            }
            if (!tLJTempFeeSchema.getPayDate().matches(
                    "\\d{4}\\-\\d{2}\\-\\d{2}"))
            {
                CError.buildErr(this, "日期格式错误，excel交费日期请使用文本格式.");
                return false;
            }
            return true;
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            CError.buildErr(this, "数据处理错误:" + e.getMessage());
            return false;
        }
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public LJTempFeeClassSet getLJTempFeeClassSet()
    {
        return outLJTempFeeClassSet;
    }

    public static void main(String[] arr)
    {
        System.out.println("OK");
    }
}
