package com.sinosoft.lis.finfee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.f1j.ss.BookModelImpl;
import com.f1j.ss.ReadParams;
import com.sinosoft.lis.operfee.VerDuePayFeeQueryUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJFIGetSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.taskservice.BqFinishTask;
import com.sinosoft.lis.taskservice.LLClaimFinishTask;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJFIGetSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class BatchPayReadExcelBL
{
    private VData mInputData = new VData();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private String mFilePath = "";

    private BookModelImpl book = new BookModelImpl();

    //业务数据
    private TransferData inTransferData = new TransferData();

    private String fileName = "";

    private int taskFlag4 = 0;

    private int taskFlag9 = 0;

    private LJAGetSet outLJAGetSet = new LJAGetSet();

    private LJFIGetSet outLJFIGetSet = new LJFIGetSet();

    private GlobalInput tG = new GlobalInput();

    private List exList = new ArrayList();
    
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();

    /**
     * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
     * @param cInputData 传入的数据,VData对象
     * @param cOperate 数据操作字符串，主要包括"READ"和""
     * @return 布尔值（true--提交成功, false--提交失败）
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData())
        {
            return false;
        }
        System.out.println("---End getInputData---");

        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        System.out.println("---End dealData---");

        return true;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * 
     * @return: boolean
     */
    private boolean getInputData()
    {
        try
        {
            inTransferData = (TransferData) mInputData.getObjectByObjectName(
                    "TransferData", 0);
            tG = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput",
                    0);

            if (tG == null || inTransferData == null)
            {
                CError tError = new CError();
                tError.moduleName = "BatchPayReadExcelBL";
                tError.functionName = "checkList";
                tError.errorMessage = "接收数据失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BatchPayReadExcelBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
 {
		try {
			// 获取银行文件数据
			if (mOperate.equals("READ")) {
				// 获取返回文件名称
				fileName = (String) inTransferData.getValueByName("fileName");
				fileName = fileName.replace('\\', '/');
				// 读取银行返回文件，公共部分
				System.out.println("---开始读取文件---");
				if (!readBatchPayExcel(fileName)) {
					return false;
				}
				if (!checkList()) {
					return false;
				}
				if (!setFiGetSchema()) {
					return false;
				}

				VData tVData = new VData();
				tVData.clear();
				tVData.add(outLJAGetSet);
				tVData.add(outLJFIGetSet);
				tVData.add(tG);

				BatchPayBL tBatchPayBL = new BatchPayBL();

				if (!tBatchPayBL.submitData(tVData, "VERIFY")) {
					this.mErrors.copyAllErrors(tBatchPayBL.mErrors);
					return false;
				}
				if (!tBatchPayBL.submitData(tVData, "CLAIM")) {
					this.mErrors.copyAllErrors(tBatchPayBL.mErrors);
					return false;
				}

				/*
				 * //调用保全确认 if (taskFlag4 == 1) { BqFinishTask tBqFinishTask =
				 * new BqFinishTask(tG.ManageCom); tBqFinishTask.run(); }
				 * //调用理赔确认 if (taskFlag9 == 1) { LLClaimFinishTask
				 * tLLClaimFinishTask = new LLClaimFinishTask( tG.ManageCom);
				 * tLLClaimFinishTask.run(); }
				 */
			}
		} catch (Exception e) {
			// @@错误处理
			e.printStackTrace();
			CError tError = new CError();
			tError.moduleName = "BatchPayReadExcelBL";
			tError.functionName = "dealData";
			tError.errorMessage = "数据处理错误:" + e.getMessage();
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

    /**
     * 读取付费导入文件
     * @param fileName
     * @return
     */
    private boolean readBatchPayExcel(String fileName)
    {
        try
        {
            mFilePath = fileName;
            book.initWorkbook();//初始
            book.read(mFilePath, new ReadParams());
            int tSheetNums = book.getNumSheets();//读去sheet的个数，就是一个excel包含几个sheet
            System.out.println("tSheetNums===" + tSheetNums);

            for (int sheetNum = 0; sheetNum < tSheetNums; sheetNum++)
            {
                book.setSheet(sheetNum);
                int tLastCol = book.getLastCol();//总共多少列，他的列数从０开始，比如有４列，那tLastCol的结果＝３
                int tLastRow = book.getLastRow();//总共多少行，他的行也是从０开始，比如有２行，那tLastRow的结果＝１
                System.out.println("tLastCol===" + tLastCol);
                System.out.println("tLastRow===" + tLastRow);
                String colValue = "";
                //按列循环
                for (int j = 1; j <= tLastRow; j++)
                {//循环总行数
                    Map tMap = new HashMap();
                    for (int i = 0; i <= tLastCol; i++)
                    {//循环总列数
                        if (book.getText(j, i) != null)
                        {
                            colValue = (String) book.getText(j, i).trim();
                        }
                        else
                        {
                            colValue = "";
                        }
                        switch (i)
                        {
                            case 0:
                                tMap.put("ActuGetNo", colValue);
                                break;
                            case 1:
                                tMap.put("OtherNo", colValue);
                                break;
                            case 2:
                                tMap.put("OtherNoType", colValue);
                                break;
                            case 3:
                                tMap.put("GetMoney", colValue);
                                break;
                            case 4:
                                tMap.put("PayMode", colValue);
                                break;
                            case 5:
                                tMap.put("InsBankCode", colValue);
                                break;
                            case 6:
                                tMap.put("InsBankAccNo", colValue);
                                break;
                            case 7:
                                tMap.put("BankCode", colValue);
                                break;
                            case 8:
                                tMap.put("BankAccNo", colValue);
                                break;
                            case 9:
                                tMap.put("AccName", colValue);
                                break;
                            case 10:
                                tMap.put("Drawer", colValue);
                                break;
                            case 11:
                                tMap.put("ChequeNo", colValue);
                                break;
                        }

                    }
                    tMap.put("ManageCom", tG.ManageCom);
                    exList.add(tMap);
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            CError tError = new CError();
            tError.moduleName = "BatchPayReadExcelBL";
            tError.functionName = "readExcelFile";
            tError.errorMessage = "读取导入文件错误:" + ex.getMessage();
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean checkList()
 {
		ExeSQL mExeSql = new ExeSQL();
		if (exList.size() == 0) {
			System.out.println("-----exList-size-0-----");
			CError tError = new CError();
			tError.moduleName = "BatchPayReadExcelBL";
			tError.functionName = "checkList";
			tError.errorMessage = "未发现导入文件中存在付费信息!";
			this.mErrors.addOneError(tError);
			return false;
		}

		for (int k = 0; k < exList.size(); k++) {
			Map tMap = (Map) exList.get(k);
			
			//#2191 社保通查勘费报销问题：按原始需求，仅支持现金结算
			if ("23".equals(tMap.get("OtherNoType")) && (!"11".equals(tMap.get("PayMode")) && !"1".equals(tMap.get("PayMode")))) {
				CError tError = new CError();
				tError.moduleName = "BatchPayReadExcelBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k + 2) + "行，付费号为"
						+ tMap.get("ActuGetNo") + "的数据，为社保通调查费，付费方式请选择现金或银行汇款进行给付！";
				this.mErrors.addOneError(tError);
				return false;
			}
			
			// 判断付费号是否为空
			// 判断业务号码，业务号码类型和给付金额于应付表是否相同
			if ("".equals(tMap.get("ActuGetNo"))) {
				CError tError = new CError();
				tError.moduleName = "BatchPayReadExcelBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k + 2) + "行，实付号不能为空!";
				this.mErrors.addOneError(tError);
				return false;
			} else if ("".equals(tMap.get("OtherNo"))) {

				CError tError = new CError();
				tError.moduleName = "BatchPayReadExcelBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k + 2) + "行，业务号不能为空!";
				this.mErrors.addOneError(tError);
				return false;

			} else if ("".equals(tMap.get("OtherNoType"))) {

				CError tError = new CError();
				tError.moduleName = "BatchPayReadExcelBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k + 2) + "行，业务号码类型不能为空!";
				this.mErrors.addOneError(tError);
				return false;
			} else if ("".equals(tMap.get("PayMode"))) {
				CError tError = new CError();
				tError.moduleName = "BatchPayReadExcelBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k + 2) + "行，付费方式不能为空!";
				this.mErrors.addOneError(tError);
				return false;
			} else {
				String mAGetSql = "select actugetno,otherno,othernotype,sumgetmoney from ljaget where actugetno = '"
						+ tMap.get("ActuGetNo") + "'";
				SSRS xAGetResult = mExeSql.execSQL(mAGetSql);
				if (xAGetResult.MaxRow != 0) {
					String xResult[] = xAGetResult.getRowData(1);
					// 业务号码
					if (!xResult[1].trim().equals(tMap.get("OtherNo"))) {
						CError tError = new CError();
						tError.moduleName = "BatchPayReadExcelBL";
						tError.functionName = "checkList";
						tError.errorMessage = "第" + (k + 2) + "行，付费号为"
								+ tMap.get("ActuGetNo") + "的数据，业务号码与应付业务号码不符!";
						this.mErrors.addOneError(tError);
						return false;
					}
					// 号码类型
					if (!xResult[2].trim().equals(tMap.get("OtherNoType"))) {
						CError tError = new CError();
						tError.moduleName = "BatchPayReadExcelBL";
						tError.functionName = "checkList";
						tError.errorMessage = "第" + (k + 2) + "行，付费号为"
								+ tMap.get("ActuGetNo") + "的数据，业务号类型与应付号码类型不符!";
						this.mErrors.addOneError(tError);
						return false;
					}
					// 给付金额
					double xAGetMoney = 0;// 应付表中数据
					double xExcelMoney = 0;// Excel录入数据
					try {
						xAGetMoney = Double.parseDouble(xResult[3].trim());
						xExcelMoney = Double.parseDouble(((String) tMap
								.get("GetMoney")).trim());
					} catch (NumberFormatException e) {
						CError tError = new CError();
						tError.moduleName = "BatchPayReadExcelBL";
						tError.functionName = "checkList";
						tError.errorMessage = "第" + (k + 2) + "行，付费号为"
								+ tMap.get("ActuGetNo") + "的数据，给付金额与应付金额不同!";
						this.mErrors.addOneError(tError);
						return false;
					}
					if (xAGetMoney != xExcelMoney) {
						CError tError = new CError();
						tError.moduleName = "BatchPayReadExcelBL";
						tError.functionName = "checkList";
						tError.errorMessage = "第" + (k + 2) + "行，付费号为"
								+ tMap.get("ActuGetNo")
								+ "的数据，给付金额与应付表单给付金额不符!";
						this.mErrors.addOneError(tError);
						return false;
					}
				}

			}
			// 不允许为9的付费方式
			if ("9".equals(tMap.get("PayMode"))) {
				CError tError = new CError();
				tError.moduleName = "BatchPayReadExcelBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k + 2) + "行，付费号为"
						+ tMap.get("ActuGetNo") + "的数据，付费方式不能为'9'!";
				this.mErrors.addOneError(tError);
				return false;
			}
			// 不允许为9的付费方式
			if ("4".equals(tMap.get("PayMode"))) {
				CError tError = new CError();
				tError.moduleName = "BatchPayReadExcelBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k + 2) + "行，付费号为"
						+ tMap.get("ActuGetNo") + "的数据，付费方式不能为'4'银行转账!";
				this.mErrors.addOneError(tError);
				return false;
			}
			// 不能录入的付费方式校验
			if (!"1".equals(tMap.get("PayMode"))
					&& !"2".equals(tMap.get("PayMode"))
					&& !"3".equals(tMap.get("PayMode"))
					&& !"5".equals(tMap.get("PayMode"))
					&& !"11".equals(tMap.get("PayMode"))
					&& !"12".equals(tMap.get("PayMode"))) {
				CError tError = new CError();
				tError.moduleName = "BatchPayReadExcelBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k + 2) + "行，付费号为"
						+ tMap.get("ActuGetNo") + "的数据，付费方式不在范围内!";
				this.mErrors.addOneError(tError);
				return false;
			}
			// 如果付费方式是支票类
			if ("2".equals(tMap.get("PayMode"))
					|| "3".equals(tMap.get("PayMode"))) {
				if ("".equals(tMap.get("BankCode"))
						|| "".equals(tMap.get("ChequeNo"))) {
					CError tError = new CError();
					tError.moduleName = "BatchPayReadExcelBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k + 2) + "行，付费号为"
							+ tMap.get("ActuGetNo")
							+ "的数据，付费方式是支票:对方开户银行和票据号码不能为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
				if ("".equals(tMap.get("InsBankCode"))) {
					CError tError = new CError();
					tError.moduleName = "BatchPayReadExcelBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k + 2) + "行，付费号为"
							+ tMap.get("ActuGetNo")
							+ "的数据，由于付费方式不是现金:本方银行编码不能为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
				if ("".equals(tMap.get("InsBankAccNo"))) {
					CError tError = new CError();
					tError.moduleName = "BatchPayReadExcelBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k + 2) + "行，付费号为"
							+ tMap.get("ActuGetNo")
							+ "的数据，由于付费方式不是现金:本方银行帐号不能为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
			}
			if ("11".equals(tMap.get("PayMode"))) {

				if ("".equals(tMap.get("BankCode"))) {
					CError tError = new CError();
					tError.moduleName = "BatchPayReadExcelBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k + 2) + "行，付费号为"
							+ tMap.get("ActuGetNo")
							+ "的数据，付费方式是银行汇款，对方开户银行不能为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
				if ("".equals(tMap.get("BankAccNo"))) {
					CError tError = new CError();
					tError.moduleName = "BatchPayReadExcelBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k + 2) + "行，付费号为"
							+ tMap.get("ActuGetNo")
							+ "的数据，付费方式是银行汇款，对方开户银行帐号不能为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
				if ("".equals(tMap.get("InsBankCode"))) {
					CError tError = new CError();
					tError.moduleName = "BatchPayReadExcelBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k + 2) + "行，付费号为"
							+ tMap.get("ActuGetNo")
							+ "的数据，由于付费方式不是现金:本方银行编码不能为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
				if ("".equals(tMap.get("InsBankAccNo"))) {
					CError tError = new CError();
					tError.moduleName = "BatchPayReadExcelBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k + 2) + "行，付费号为"
							+ tMap.get("ActuGetNo")
							+ "的数据，由于付费方式不是现金:本方银行帐号不能为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
			}
			// 如果付费方式是银行转账
			if ("4".equals(tMap.get("PayMode"))) {
				if ("".equals(tMap.get("BankCode"))) {
					CError tError = new CError();
					tError.moduleName = "BatchPayReadExcelBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k + 2) + "行，付费号为"
							+ tMap.get("ActuGetNo")
							+ "的数据，付费方式是银行转账，对方开户银行不能为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
				if ("".equals(tMap.get("BankAccNo"))) {
					CError tError = new CError();
					tError.moduleName = "BatchPayReadExcelBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k + 2) + "行，付费号为"
							+ tMap.get("ActuGetNo")
							+ "的数据，付费方式是银行转账，对方开户银行帐号不能为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
				if ("".equals(tMap.get("InsBankCode"))) {
					CError tError = new CError();
					tError.moduleName = "BatchPayReadExcelBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k + 2) + "行，付费号为"
							+ tMap.get("ActuGetNo")
							+ "的数据，由于付费方式不是现金:本方银行编码不能为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
				if ("".equals(tMap.get("InsBankAccNo"))) {
					CError tError = new CError();
					tError.moduleName = "BatchPayReadExcelBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k + 2) + "行，付费号为"
							+ tMap.get("ActuGetNo")
							+ "的数据，由于付费方式不是现金:本方银行帐号不能为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
			}
			if (!"1".equals(tMap.get("PayMode"))
					&& !"5".equals(tMap.get("PayMode"))) {
				if ("".equals(tMap.get("InsBankCode"))) {
					CError tError = new CError();
					tError.moduleName = "BatchPayReadExcelBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k + 2) + "行，付费号为"
							+ tMap.get("ActuGetNo")
							+ "的数据，由于付费方式不是现金:本方银行编码不能为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
				if ("".equals(tMap.get("InsBankAccNo"))) {
					CError tError = new CError();
					tError.moduleName = "BatchPayReadExcelBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k + 2) + "行，付费号为"
							+ tMap.get("ActuGetNo")
							+ "的数据，由于付费方式不是现金:本方银行帐号不能为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
				String accsql = "select 1 from ldfinbank where bankcode ='"
						+ tMap.get("InsBankCode") + "' and bankaccno='"
						+ tMap.get("InsBankAccNo") + "'";
				SSRS mSSRS = mExeSql.execSQL(accsql);
				if (mSSRS.MaxNumber == 0) {
					CError tError = new CError();
					tError.moduleName = "BatchPayReadExcelBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k + 2) + "行，付费号为"
							+ tMap.get("ActuGetNo") + "的数据，请确认本方银行账户！";
					this.mErrors.addOneError(tError);
					return false;
				}
				if ("".equals(tMap.get("AccName").toString().trim())) {
					CError tError = new CError();
					tError.moduleName = "BatchPayReadExcelBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k + 2) + "行，付费号为"
							+ tMap.get("ActuGetNo") + "的数据，对方银行账户名不能为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
				if (!"".equals(tMap.get("AccName").toString().trim())) {
					if (!tMap.get("AccName").equals(tMap.get("Drawer"))) {
						CError tError = new CError();
						tError.moduleName = "BatchPayReadExcelBL";
						tError.functionName = "checkList";
						tError.errorMessage = "第" + (k + 2) + "行，付费号为"
								+ tMap.get("ActuGetNo") + "的数据，对方账户名与领取人不一致！";
						this.mErrors.addOneError(tError);
						return false;
					}
				}
			}

			if (!"5".equals(tMap.get("PayMode"))) {

				if ("".equals(tMap.get("Drawer")) || tMap.get("Drawer") == null) {
					CError tError = new CError();
					tError.moduleName = "BatchPayReadExcelBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k + 2) + "行，付费号为"
							+ tMap.get("ActuGetNo") + "的数据，领取人不能为空！";
					this.mErrors.addOneError(tError);
					return false;
				}
			}

			// 校验付费方式是否正确
			String payModeSql = "SELECT '1' FROM ldcode WHERE codetype='paymode' and code='"
					+ tMap.get("PayMode") + "' with ur";
			SSRS payModeRS = mExeSql.execSQL(payModeSql);
			if (payModeRS.MaxNumber == 0) {
				CError tError = new CError();
				tError.moduleName = "BatchPayReadExcelBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k + 2) + "行，付费号为"
						+ tMap.get("ActuGetNo") + "的数据，付费方式不在输入的范围内！";
				this.mErrors.addOneError(tError);
				return false;
			}
			//
			if (!"1".equals(tMap.get("PayMode"))
					&& !"5".equals(tMap.get("PayMode"))) {
				// 校验归集账号是否正确
				if (!"".equals(tMap.get("InsBankCode"))) {
					String insBankSql = "select '1' from LDFinBank where FinFlag='F' and BankCode='"
							+ tMap.get("InsBankCode") + "' with ur";
					SSRS mSSRS = mExeSql.execSQL(insBankSql);
					if (mSSRS.MaxNumber == 0) {
						CError tError = new CError();
						tError.moduleName = "BatchPayReadExcelBL";
						tError.functionName = "checkList";
						tError.errorMessage = "第" + (k + 2) + "行，付费号为"
								+ tMap.get("ActuGetNo") + "的数据，本方银行不在输入的范围内！";
						this.mErrors.addOneError(tError);
						return false;
					}
				}
				// 校验归集账号是否正确
				if (!"".equals(tMap.get("InsBankAccNo"))) {
					String insBankAccSql = "select '1' from LDFinBank where FinFlag='F' and  bankaccno='"
							+ tMap.get("InsBankAccNo") + "' with ur";
					SSRS mSSRS = mExeSql.execSQL(insBankAccSql);
					if (mSSRS.MaxNumber == 0) {
						CError tError = new CError();
						tError.moduleName = "BatchPayReadExcelBL";
						tError.functionName = "checkList";
						tError.errorMessage = "第" + (k + 2) + "行，付费号为"
								+ tMap.get("ActuGetNo") + "的数据，本方银行帐号不在输入的范围内！";
						this.mErrors.addOneError(tError);
						return false;
					}
				}
				// 校验对方银行编码是否正确
				if ("".equals(tMap.get("BankCode"))) {
					String bankCodeSql = "select '1' from LDBank  where (BankUniteFlag is null or BankUniteFlag<>'1') and bankcode='"
							+ tMap.get("BankCode") + "' with ur";
					SSRS mSSRS = mExeSql.execSQL(bankCodeSql);
					if (mSSRS.MaxNumber == 0) {
						CError tError = new CError();
						tError.moduleName = "BatchPayReadExcelBL";
						tError.functionName = "checkList";
						tError.errorMessage = "第" + (k + 2) + "行，付费号为"
								+ tMap.get("ActuGetNo") + "的数据，本方银行帐号不在输入的范围内！";
						this.mErrors.addOneError(tError);
						return false;
					}
				}
			}

			try {
				if (!"".equals(tMap.get("GetMoney").toString())) {
					// if (Double.parseDouble(tMap.get("GetMoney").toString())
					// <= 0) {
					// CError tError = new CError();
					// tError.moduleName = "BatchPayReadExcelBL";
					// tError.functionName = "checkList";
					// tError.errorMessage = "第" + (k + 2) + "行，付费号为"
					// + tMap.get("ActuGetNo") + "的数据，给付金额错误，金额必须大于零!";
					// this.mErrors.addOneError(tError);
					// return false;
					// }
				} else {
					CError tError = new CError();
					tError.moduleName = "BatchPayReadExcelBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k + 2) + "行，付费号为"
							+ tMap.get("ActuGetNo") + "的数据，给付金额不能为空！";
					this.mErrors.addOneError(tError);
					return false;
				}

			} catch (Exception ex) {
				CError tError = new CError();
				tError.moduleName = "BatchPayReadExcelBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k + 2) + "行，付费号为"
						+ tMap.get("ActuGetNo") + "的数据，给付金额格式错误!";
				this.mErrors.addOneError(tError);
				return false;
			}

			// if (!PubFun.checkDateForm(tMap.get("PayDate").toString()))
			// {
			// CError tError = new CError();
			// tError.moduleName = "BatchPayReadExcelBL";
			// tError.functionName = "checkList";
			// tError.errorMessage = "第" + (k + 1) + "笔数据，交费日期格式错误！";
			// this.mErrors.addOneError(tError);
			// return false;
			// }

		}

		return true;
	}
    /**
     * 给LJAGetSchema和LJFiGetSchema赋值
     * @return
     */
    private boolean setFiGetSchema()
    {
    	LJFIGetSchema tLJFIGetSchema ;    
    	LJAGetSchema  tLJAGetSchema  ; 
	 
	 	
	 	for (int j = 0; j < exList.size(); j++) {
	 		Map xMap = (Map) exList.get(j);
			if(!"".equals((String)xMap.get("ActuGetNo")) && xMap.get("ActuGetNo")!=null){
				tLJAGetSchema=new LJAGetSchema();
				tLJAGetSchema.setActuGetNo((String) xMap.get("ActuGetNo"));
				VData tVData = new VData();
				tVData.add(tLJAGetSchema);
				LJAGetQueryUI tLJAGetQueryUI = new LJAGetQueryUI();
   			if(!tLJAGetQueryUI.submitData(tVData,"QUERY")){
   				CError tError = new CError();
   	            tError.moduleName = "BatchPayReadExcelBL";
   	            tError.functionName = "setFIGet";
   	            tError.errorMessage = "第" + (j+2) + "行，付费号为"+xMap.get("ActuGetNo") +"的数据数据查询失败，可能是：1.未找到相关数据 2.该数据已给付核销 3.在送银行途中!";
   	            this.mErrors.addOneError(tError);
   				return false;
   				
   			}else{
   				tVData.clear();      
			    tVData = tLJAGetQueryUI.getResult();
			    tLJAGetSchema=null;
			    tLJAGetSchema=((LJAGetSet)tVData.getObjectByObjectName("LJAGetSet",0)).get(1);
			    tLJAGetSchema.setEnterAccDate(CurrentDate);
    			tLJAGetSchema.setDrawer((String)xMap.get("Drawer"));
//    			tLJAGetSchema.setDrawerID();
    			tLJAGetSchema.setOperator(tG.Operator); 
    			tLJAGetSchema.setChequeNo((String)xMap.get("ChequeNo"));
    			//对方银行
    			tLJAGetSchema.setBankCode((String)xMap.get("BankCode"));
    			tLJAGetSchema.setBankAccNo((String)xMap.get("BankAccNo"));
    			tLJAGetSchema.setAccName((String)xMap.get("AccName"));
    			//本方银行
    			tLJAGetSchema.setInsBankCode((String)xMap.get("InsBankCode"));
    			tLJAGetSchema.setInsBankAccNo((String)xMap.get("InsBankAccNo"));
    			
			    tLJFIGetSchema=new LJFIGetSchema();
			    tLJFIGetSchema.setActuGetNo((String) xMap.get("ActuGetNo"));
			    tLJFIGetSchema.setPayMode((String) xMap.get("PayMode"));
			    tLJFIGetSchema.setOtherNo((String) xMap.get("OtherNo"));
			    tLJFIGetSchema.setOtherNoType((String) xMap.get("OtherNoType"));
			    tLJFIGetSchema.setGetMoney((String) xMap.get("GetMoney"));
			    tLJFIGetSchema.setShouldDate(tLJAGetSchema.getShouldDate());
			    tLJFIGetSchema.setEnterAccDate(CurrentDate);
			    tLJFIGetSchema.setSaleChnl(tLJAGetSchema.getSaleChnl());
			    tLJFIGetSchema.setAgentGroup(tLJAGetSchema.getAgentGroup());
			    tLJFIGetSchema.setAgentCode(tLJAGetSchema.getAgentCode());
			    tLJFIGetSchema.setSerialNo(tLJAGetSchema.getSerialNo());
			    tLJFIGetSchema.setDrawer((String) xMap.get("Drawer"));
			    tLJFIGetSchema.setDrawerID(tLJAGetSchema.getDrawerID());
			    tLJFIGetSchema.setOperator(tG.Operator);
			    tLJFIGetSchema.setBankCode(tLJAGetSchema.getBankCode());
			    tLJFIGetSchema.setBankAccNo(tLJAGetSchema.getBankAccNo());
			    tLJFIGetSchema.setChequeNo((String) xMap.get("ChequeNo"));    
			    tLJFIGetSchema.setAccName(tLJAGetSchema.getAccName()); 	
			    
			    outLJFIGetSet.add(tLJFIGetSchema);
			    outLJAGetSet.add(tLJAGetSchema);
   			}
			}
	 	}

	 	return true;
		
		
		}

    

    

    /**
     *  返回查询条件，用于前台界面展示
     */
    public String getActuGetNo()
    {
        if (exList.size() == 0)
        {
            return "('')";
        }
        String sql = "(";
        for (int i = 0; i < exList.size() - 1; i++)
        {
            sql = sql + "'" + ((Map) exList.get(i)).get("ActuGetNo").toString()
                    + "',";
        }
        sql = sql
                + "'"
                + ((Map) exList.get(exList.size() - 1)).get("ActuGetNo")
                        .toString() + "')";
        return sql;
    }

}
