package com.sinosoft.lis.finfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PayManageConfirmUI {
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
        public  CErrors mErrors = new CErrors();
        /** 往后面传输数据的容器 */
        private VData mInputData = new VData();
        /** 往界面传输数据的容器 */
        private VData mResult = new VData();
        /** 数据操作字符串 */
  private String mOperate;
    public PayManageConfirmUI() {
    }
    public static void main(String[] args)
    {
         GlobalInput tGI = new GlobalInput();
          LJAGetSchema tLJAGetSchema = new LJAGetSchema();
          PayManageConfirmUI tPayManageConfirmUI  = new PayManageConfirmUI();
    tLJAGetSchema.setActuGetNo("86310000000563");
    tLJAGetSchema.setPayMode("4");
    tLJAGetSchema.setBankCode("0701");
    tLJAGetSchema.setAccName("冬雨");
    tLJAGetSchema.setBankAccNo("11111");
    tGI.Operator="fi001";
    tGI.ManageCom="86";
    tGI.ComCode="86";

                          VData tVData = new VData();
                          tVData.add(tLJAGetSchema);
                          tVData.add( tGI );
        tPayManageConfirmUI.submitData(tVData,"UNLOCK");
    }

    /**
    传输数据的公共方法
    */
    public boolean submitData(VData cInputData,String cOperate)
    {
      //将操作数据拷贝到本类中
      this.mOperate = cOperate;
      System.out.println("---PayManageConfirmUI BEGIN---");
      PayManageConfirmBL tPayManageConfirmBL = new PayManageConfirmBL();
      if (tPayManageConfirmBL.submitData(cInputData,mOperate) == false)
          {
                  // @@错误处理
        this.mErrors.copyAllErrors(tPayManageConfirmBL.mErrors);
        CError tError = new CError();
        tError.moduleName = "PayManageConfirmUI";
        tError.functionName = "submitData";
        tError.errorMessage = "数据查询失败!";
        this.mErrors.addOneError(tError) ;
        mResult.clear();
        return false;
          }
          System.out.println("---PayManageConfirmUI END---");
      return true;
  }
}
