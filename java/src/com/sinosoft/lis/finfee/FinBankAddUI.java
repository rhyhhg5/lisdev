package com.sinosoft.lis.finfee;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author : lys
 * @date:2003-06-04
 * @version 1.0
 */
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LDBankSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.f1print.FinDayCheckYFUI;
import com.sinosoft.lis.f1print.FinDayCheckYFBL;
import com.sinosoft.utility.TransferData;

public class FinBankAddUI {
    public CErrors mErrors = new CErrors();
    private String mResult;
    private LDBankSchema mLDBankSchema=new LDBankSchema();
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mDealElement = new TransferData();

    public FinBankAddUI() {
    }
    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("CHILD") && !cOperate.equals("HEAD") &&!cOperate.equals("REPAIR") && !cOperate.equals("DELETE"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }
            // 得到外部传入的数据，将数据备份到本类中
            if (!getInputData(cInputData))
            {
                return false;
            }

            VData vData = new VData();
            if (!prepareOutputData(vData))
            {
                return false;
            }

            FinBankAddBL tFinBankAddBL = new FinBankAddBL();
            System.out.println("Start FinDayCheckBL Submit ...");
            if (!tFinBankAddBL.submitData(vData, cOperate))
            { 
                if (tFinBankAddBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tFinBankAddBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData",
                               "tNewFinDayCheckYFBL发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tFinBankAddBL.getResult();
                mLDBankSchema=tFinBankAddBL.getLDBankSchema();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            CError cError = new CError();
            cError.moduleName = "tFinBankAddUI";
            cError.functionName = "submit";
            cError.errorMessage = e.toString();
            mErrors.addOneError(cError);
            return false;
        }
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.add(mDealElement);
            vData.add(mGlobalInput);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

    /*
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mDealElement = (TransferData) cInputData.getObjectByObjectName("TransferData",0);
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        System.out.println(mGlobalInput.Operator);
        System.out.println(mGlobalInput.ManageCom);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }
        return true;
    }

    public VData getVResult() {
        VData tVData=new VData();
        tVData.add(this.mErrors.getFirstError());
        return tVData;
      }
  
    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FinBankAddUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    public String getResult() {
        return mResult;
      }
    public LDBankSchema getLDBankSchema(){
        return mLDBankSchema;
    }
    public static void main(String[] args)
    {
        FinBankAddUI fin = new FinBankAddUI();
        TransferData DealElement = new TransferData();
        DealElement.setNameAndValue("BankCode", "01");
        DealElement.setNameAndValue("ManageCom", "86110000");
        DealElement.setNameAndValue("ChildBankName","花旗银行北京分行");
        DealElement.setNameAndValue("ChildBankCode","220101");
        DealElement.setNameAndValue("HeadBankName", "帝国银行");
        DealElement.setNameAndValue("GetBankAccNo", "S1234567890123450000");
        DealElement.setNameAndValue("PayBankAccNo", "F123456789012345aaaa");

        VData tVData = new VData();
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86110000";
        tG.Operator = "001";
        tVData.addElement(tG);
        tVData.addElement(DealElement);
        fin.submitData(tVData, "REPAIR");
        System.out.println("bankcode : "+fin.getResult());
    }
}
