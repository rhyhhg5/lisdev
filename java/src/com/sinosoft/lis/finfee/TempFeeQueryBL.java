 package com.sinosoft.lis.finfee;

import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单查询业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class TempFeeQueryBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  /** 业务处理相关变量 */
  /** 暂交费表 */
  private LJTempFeeSet  mLJTempFeeSet = new LJTempFeeSet() ;
  private LJTempFeeSchema  mLJTempFeeSchema = new LJTempFeeSchema() ;
  private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
  private LJTempFeeClassSchema mLJTempFeeClassSchema = new LJTempFeeClassSchema();
  private String mStartDate ;
  private String mEndDate;
  private String mTempFeeStatus;
  private String mAgentCode;
  private String mPrtNo;
//  private String mChequeNo;
//  private String mChequeDate;

  private GlobalInput mGlobalInput =new GlobalInput() ;

  public TempFeeQueryBL() {}

  public static void main(String[] args) {

  GlobalInput tG = new GlobalInput();
  tG.ManageCom="86";
  tG.Operator="001";
  VData tVData = new VData();
  LJTempFeeSchema  tLJTempFeeSchema= new LJTempFeeSchema();
  LJTempFeeClassSchema  tLJTempFeeClassSchema= new LJTempFeeClassSchema();
  tVData.addElement(tG);
  String StartDate="";
  String EndDate="";
  String TempFeeStatus="";
  String ChequeNo="";
  String AgentCode="";
  tLJTempFeeSchema = new LJTempFeeSchema();
  tLJTempFeeSchema.setTempFeeNo("8000000045");
  tLJTempFeeClassSchema = new LJTempFeeClassSchema();
  tVData.add(tLJTempFeeSchema);
  //tVData.add(tLJTempFeeClassSchema);
  tVData.addElement(StartDate);
  tVData.addElement(EndDate);
  tVData.addElement(TempFeeStatus);
  tVData.addElement(AgentCode);
  //tVData.addElement(ChequeNo);


  TempFeeQueryUI tTempFeeQueryUI = new TempFeeQueryUI();
   tTempFeeQueryUI.submitData(tVData,"QUERYUpd");

  }
  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;

System.out.println("---getInputData---");
System.out.println("operate:"+mOperate);

    //进行业务处理
    if(mOperate.equals("QUERY")||mOperate.equals("QUERYUpd")){
      if (!queryLJTempFee())
	      return false;
      System.out.println("---queryLJTempFee---");
    }

    if(mOperate.equals("CHEQUE||QUERY")){
      if (!queryLJTempFeeClass())
              return false;
      System.out.println("---queryLJTempFeeClass---");
    }

    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    // 检验查询条件
    mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));

    if (this.mOperate.equals("QUERY")||this.mOperate.equals("QUERYUpd"))
    {
      mLJTempFeeSchema.setSchema((LJTempFeeSchema)cInputData.getObjectByObjectName("LJTempFeeSchema",0));
      if (this.mOperate.equals("QUERY")){
      mStartDate = (String)cInputData.get(2);
      mEndDate = (String)cInputData.get(3);
      mTempFeeStatus =(String)cInputData.get(4);
      System.out.println("mTempFeeStatus:"+mTempFeeStatus);
      System.out.println("mTempFeeStatus.length:"+mTempFeeStatus.length());
      }
    }
    if (this.mOperate.equals("CHEQUE||QUERY"))
    {
      mLJTempFeeClassSchema.setSchema((LJTempFeeClassSchema)cInputData.getObjectByObjectName("LJTempFeeClassSchema",0));
      mStartDate = (String)cInputData.get(2);
      mEndDate = (String)cInputData.get(3);
      mTempFeeStatus =(String)cInputData.get(4);
      mPrtNo = (String)cInputData.get(5);
      mAgentCode = (String)cInputData.get(6);
    }
    if (mTempFeeStatus==null || mTempFeeStatus.length()==0)
      mTempFeeStatus="6";
    System.out.println("mTempFeeStatus:"+mTempFeeStatus);
    return true;
  }


  /**
   * 查询暂交费表信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean queryLJTempFee()
  {
  	// 保单信息
      System.out.println("mTempFeeStatus:"+mTempFeeStatus);
      Integer iTemp = new Integer(mTempFeeStatus);
      int iTempFeeStatus = iTemp.intValue();
      String tSql="select * from LJTempFee where 1=1 ";
      switch(iTempFeeStatus){
      case 0:
        tSql = tSql + " and EnterAccDate is null ";
        break;
      case 1:
        tSql = tSql + " and ConfFlag='0' ";
        break;
      case 2:
        tSql = tSql + " and ConfFlag='1' ";
        break;
      case 3:
        tSql = tSql + " and (tempfeeno,riskcode) in(select tempfeeno,riskcode tempfeeno from LJAGetTempFee)";
        break;
      case 4:
        tSql = tSql + " and ConfFlag='0' and TempFeeType in('1','5')";
        break;
      case 5:
        tSql = tSql + " and ConfFlag='0' and TempFeeType not in('1','5')";
        break;
      case 6:
        break;
    }

    if (mStartDate!=null && mStartDate.length()!=0)
      tSql = tSql + " and MakeDate >='"+mStartDate+"' ";

    if (mEndDate!=null && mEndDate.length()!=0)
      tSql = tSql + " and MakeDate <='"+mEndDate+"' ";

    if (mLJTempFeeSchema.getRiskCode()!=null && mLJTempFeeSchema.getRiskCode().length()!=0)
      tSql = tSql + " and RiskCode ='"+mLJTempFeeSchema.getRiskCode()+"' ";

    if (mLJTempFeeSchema.getOperator()!=null && mLJTempFeeSchema.getOperator().length()!=0)
      tSql = tSql + " and Operator ='"+mLJTempFeeSchema.getOperator()+"' ";

    if (
        (mLJTempFeeSchema.getOtherNo()==null||mLJTempFeeSchema.getOtherNo().length() == 0) &&

        (mLJTempFeeSchema.getTempFeeNo()==null||mLJTempFeeSchema.getTempFeeNo().length() == 0)
        && mOperate.equals("QUERYUpd")) {
      CError tError = new CError();
      tError.moduleName = "TempFeeQueryBL";
      tError.functionName = "queryData";
      tError.errorMessage = "请录入暂收据号或印刷号";
      this.mErrors.addOneError(tError);
      mLJTempFeeSet.clear();
      return false;
    }

    else {
      if (mLJTempFeeSchema.getTempFeeNo() != null &&
          mLJTempFeeSchema.getTempFeeNo().length() != 0) {
        tSql = tSql + " and tempfeeno='" + mLJTempFeeSchema.getTempFeeNo() +
            "' ";
      }
      if (mLJTempFeeSchema.getOtherNo() != null &&
          mLJTempFeeSchema.getOtherNo().length() != 0) {
        tSql = tSql + " and OtherNoType='4' and OtherNo='" +
            mLJTempFeeSchema.getOtherNo() + "'";
      }

    }

    if (mLJTempFeeSchema.getAgentCode()!=null && mLJTempFeeSchema.getAgentCode().length()!=0)
      tSql = tSql + " and AgentCode=getAgentCode("+mLJTempFeeSchema.getAgentCode()+") ";


//    if(mChequeNo!=null && !mChequeNo.trim().equals("")){
//      LJTempFeeClassDB tLJTempFeeClassDB=new LJTempFeeClassDB();
//      tLJTempFeeClassDB.setChequeNo(mChequeNo);
//      LJTempFeeClassSet tLJTempFeeClassSet=new LJTempFeeClassSet();
//      tLJTempFeeClassSet=tLJTempFeeClassDB.query();
//      if(tLJTempFeeClassSet.size()==1){
//        tSql = tSql + " and TempFeeNo='"+tLJTempFeeClassSet.get(1).getTempFeeNo()+"'";
//      }
//      //tSql = tSql + " and
//    }
    tSql = tSql + " and ManageCom like '"+mGlobalInput.ManageCom+"%'";


    System.out.println(tSql);

    LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
    mLJTempFeeSet = tLJTempFeeDB.executeQuery(tSql);

    if (tLJTempFeeDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJTempFeeDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "TempFeeQueryBL";
      tError.functionName = "queryData";
      tError.errorMessage = "暂交费查询失败!";
      this.mErrors.addOneError(tError);
      mLJTempFeeSet.clear();
      return false;
    }
    if (mLJTempFeeSet.size() == 0)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "TempFeeQueryBL";
      tError.functionName = "queryData";
      tError.errorMessage = "未找到相关数据!";
      this.mErrors.addOneError(tError);
      mLJTempFeeSet.clear();
      return false;
    }
    if (this.mOperate.equals("QUERY"))
    {
      for (int i=1;i<=mLJTempFeeSet.size();i++)
      {
        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
        tLJTempFeeSchema.setSchema(mLJTempFeeSet.get(i));
        if(iTempFeeStatus==3)
          tLJTempFeeSchema.setState("已退费");
        else if (tLJTempFeeSchema.getEnterAccDate()==null)
          tLJTempFeeSchema.setState("未到账");
        else if(tLJTempFeeSchema.getConfFlag().equals("0"))
          tLJTempFeeSchema.setState("到账未核销");
        else
          tLJTempFeeSchema.setState("已核销");
        mLJTempFeeSet.set(i,tLJTempFeeSchema);
      }
    }
    
    //统一工号，页面展示修改
    for (int i=1;i<=mLJTempFeeSet.size();i++)
    {
      LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
      tLJTempFeeSchema.setSchema(mLJTempFeeSet.get(i));
      String tAgentCode = tLJTempFeeSchema.getAgentCode();
      String SQL = "select getUniteCode("+tAgentCode+") from dual where 1=1 with ur";
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = new SSRS();
      tSSRS = tExeSQL.execSQL(SQL);
	  if (tSSRS != null && tSSRS.getMaxRow() > 0) {
		  tLJTempFeeSchema.setAgentCode(tSSRS.GetText(1, 1));
	  }
      mLJTempFeeSet.set(i,tLJTempFeeSchema);
    }

    System.out.println("LJTempFee num="+mLJTempFeeSet.size());
	mResult.clear();
	mResult.add( mLJTempFeeSet );

    LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
    if (this.mOperate.equals("QUERYUpd"))
    {
      LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
      tLJTempFeeClassDB.setTempFeeNo(mLJTempFeeSchema.getTempFeeNo());
      tLJTempFeeClassSet=tLJTempFeeClassDB.query();
      mResult.add(tLJTempFeeClassSet);
    }
	return true;
  }

  /**
   * 查询暂交费分类表信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean queryLJTempFeeClass()
  {
          // 保单信息
      System.out.println("mTempFeeStatus:"+mTempFeeStatus);
      Integer iTemp = new Integer(mTempFeeStatus);
      int iTempFeeStatus = iTemp.intValue();
      String tSql="select * from LJTempFeeClass where 1=1 ";
      switch(iTempFeeStatus){
      case 0:
        tSql = tSql + " and EnterAccDate is null ";
        break;
      case 1:
        tSql = tSql + " and ConfFlag='0' ";
        break;
      case 2:
        tSql = tSql + " and ConfFlag='1' ";
        break;
      case 3:
        tSql = tSql + " and tempfeeno in(select tempfeeno from LJAGetTempFee)";
        break;
      case 4:
        tSql = tSql + " and ConfFlag='0' and TempFeeType in('1','5')";
        break;
      case 5:
        tSql = tSql + " and ConfFlag='0' and TempFeeType not in('1','5')";
        break;
      case 6:
        break;
    }
    int flag=0;
    if (mStartDate!=null && mStartDate.length()!=0){
      tSql = tSql + " and MakeDate >='"+mStartDate+"' ";
      flag=1;
    }

    if (mEndDate!=null && mEndDate.length()!=0){
      tSql = tSql + " and MakeDate <='"+mEndDate+"' ";
      flag=1;
    }
    if(flag==0){
        CError tError = new CError();
        tError.moduleName = "TempFeeQueryBL";
        tError.functionName = "queryData";
        tError.errorMessage = "请录入查询日期";
        this.mErrors.addOneError(tError);
        mLJTempFeeSet.clear();
        return false; 
    }

    if (mLJTempFeeClassSchema.getOperator()!=null && mLJTempFeeClassSchema.getOperator().length()!=0)
      tSql = tSql + " and Operator ='"+mLJTempFeeClassSchema.getOperator()+"' ";

    if (mLJTempFeeClassSchema.getTempFeeNo()!=null && mLJTempFeeClassSchema.getTempFeeNo().length()!=0)
      tSql = tSql + " and tempfeeno='"+mLJTempFeeClassSchema.getTempFeeNo()+"' ";
    else
    {
      if (mOperate.equals("QUERYUpd"))
      {
        CError tError = new CError();
        tError.moduleName = "TempFeeQueryBL";
        tError.functionName = "queryData";
        tError.errorMessage = "请录入暂收据号!";
        this.mErrors.addOneError(tError);
        mLJTempFeeSet.clear();
        return false;
      }
    }
    if (mAgentCode!=null && mAgentCode.length()!=0)
      tSql = tSql + " and TempFeeNo in (select tempfeeno from LJTempFee where AgentCode=getAgentCode("+mAgentCode+") ) ";

    //System.out.println("----prtNo:"+mPrtNo+"  AgentCode:"+mAgentCode);
    if (mPrtNo!=null&&!mPrtNo.trim().equals("")){
      tSql = tSql + " and TempFeeNo in (select TempFeeNo from LJTempFee where OtherNoType='4' and OtherNo='"+mPrtNo+"')";
    }
    if(mLJTempFeeClassSchema.getChequeNo()!=null && !mLJTempFeeClassSchema.getChequeNo().trim().equals("")){
      tSql = tSql + " and ChequeNo='"+mLJTempFeeClassSchema.getChequeNo()+"'";
    }
    if(mLJTempFeeClassSchema.getPayMode()!=null && !mLJTempFeeClassSchema.getPayMode().trim().equals("")){
      tSql = tSql + " and PayMode='"+mLJTempFeeClassSchema.getPayMode()+"'";
    }
    if(mLJTempFeeClassSchema.getChequeDate()!=null && !mLJTempFeeClassSchema.getChequeDate().trim().equals(""))
      tSql = tSql + " and ChequeDate='"+mLJTempFeeClassSchema.getChequeDate()+"'";
    
    tSql = tSql + " and ManageCom like '"+mGlobalInput.ManageCom+"%'";


    System.out.println(tSql);

    LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
    mLJTempFeeClassSet = tLJTempFeeClassDB.executeQuery(tSql);

      if (tLJTempFeeClassDB.mErrors.needDealError() == true)
      {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "TempFeeQueryBL";
      tError.functionName = "queryData";
      tError.errorMessage = "暂交费查询失败!";
      this.mErrors.addOneError(tError);
      mLJTempFeeSet.clear();
      return false;
    }
    if (mLJTempFeeClassSet.size() == 0)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "TempFeeQueryBL";
      tError.functionName = "queryData";
      tError.errorMessage = "未找到相关数据!";
      this.mErrors.addOneError(tError);
      mLJTempFeeSet.clear();
      return false;
    }

    System.out.println("LJTempFeeClass num="+mLJTempFeeClassSet.size());
        mResult.clear();
        mResult.add( mLJTempFeeClassSet );

        return true;
  }
  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData()
  {
    mResult.clear();
    try
    {
      mResult.add( mLJTempFeeSet );
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="TempFeeQueryBL";
      tError.functionName="prepareOutputData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
    }
}
