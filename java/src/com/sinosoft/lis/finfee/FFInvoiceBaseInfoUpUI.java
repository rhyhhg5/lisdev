package com.sinosoft.lis.finfee;

import com.sinosoft.lis.schema.LJInvoiceInfoSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class FFInvoiceBaseInfoUpUI
{
    public CErrors mErrors = new CErrors();

    private VData mResult;

    public FFInvoiceBaseInfoUpUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("UPDATE"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            if (!checkData(cInputData))
            {
                return false;
            }

            FFInvoiceBaseInfoUpBL tFFInvoiceBaseInfoUpBL = new FFInvoiceBaseInfoUpBL();
            System.out.println("Start FFInvoiceBaseInfoUI Submit ...");
            if (!tFFInvoiceBaseInfoUpBL.submitData(cInputData, cOperate))
            {
                if (tFFInvoiceBaseInfoUpBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tFFInvoiceBaseInfoUpBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData",
                            "FFInvoiceBaseInfoUpBL 发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tFFInvoiceBaseInfoUpBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("submitData", "其他未知异常");
            return false;
        }
    }

    private boolean checkData(VData cInputData)
    {
        LJInvoiceInfoSchema tLJInvoiceInfoSchema = (LJInvoiceInfoSchema) cInputData
                .getObjectByObjectName("LJINVOICEINFOSchema", 0);
        if (tLJInvoiceInfoSchema == null)
        {
            buildError("submitData", "业务数据信息有误，请确认后重新填写。");
            return false;
        }
        if (tLJInvoiceInfoSchema.getComCode() == null
                || tLJInvoiceInfoSchema.getComCode().trim().equals(""))
        {
            buildError("submitData", "业务数据信息有误，请确认后重新填写。");
            return false;
        }
        if (tLJInvoiceInfoSchema.getTaxpayerNo() == null
                || tLJInvoiceInfoSchema.getTaxpayerNo().trim().equals(""))
        {
            buildError("submitData", "业务数据信息有误，请确认后重新填写。");
            return false;
        }
        if (tLJInvoiceInfoSchema.getInvoiceCode() == null
                || tLJInvoiceInfoSchema.getInvoiceCode().trim().equals(""))
        {
            buildError("submitData", "业务数据信息有误，请确认后重新填写。");
            return false;
        }
        if (tLJInvoiceInfoSchema.getInvoiceStartNo() == null)
        {
            buildError("submitData", "发票起始号码为空，请确认后重新填写。");
            return false;
        }
        else if (!tLJInvoiceInfoSchema.getInvoiceStartNo().matches("\\d{8}"))
        {
            buildError("submitData", "发票起始号码需要为八位数字格式，请确认后重新填写");
            return false;
        }
        if (tLJInvoiceInfoSchema.getInvoiceEndNo() == null)
        {
            buildError("submitData", "发票终止号码为空，请确认后重新填写。");
            return false;
        }
        else if (!tLJInvoiceInfoSchema.getInvoiceEndNo().matches("\\d{8}"))
        {
            buildError("submitData", "发票终止号码需要为八位数字格式，请确认后重新填写");
            return false;
        }
        try
        {
            if(Integer.parseInt(tLJInvoiceInfoSchema.getInvoiceEndNo()) < Integer.parseInt(tLJInvoiceInfoSchema.getInvoiceStartNo())){
                buildError("submitData", "发票起始号码需要小于发票终止号码，请确认后重新填写");
                return false;
            }
        }
        catch (Exception ex)
        {
            buildError("submitData", "发票号码格式，请确认后重新填写");
            return false;
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FFInvoiceBaseInfoSaveBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
    }

}
