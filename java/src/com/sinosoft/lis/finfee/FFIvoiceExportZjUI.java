/**
 * 2007-5-22
 */
package com.sinosoft.lis.finfee;

import java.util.List;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class FFIvoiceExportZjUI
{
    public CErrors mErrors = new CErrors();

    private List mResult;

    public FFIvoiceExportZjUI()
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("EXPORT"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            FFIvoiceExportZjBL tFFIvoiceExportZjBL = new FFIvoiceExportZjBL();
            System.out.println("Start FFIvoiceExportZjBL Submit ...");
            if (!tFFIvoiceExportZjBL.submitData(cInputData, cOperate))
            {
                if (tFFIvoiceExportZjBL.mErrors.needDealError())
                {
                    mErrors.copyAllErrors(tFFIvoiceExportZjBL.mErrors);
                    return false;
                }
                else
                {
                    buildError("submitData",
                            "FFIvoiceExportZjBL 发生错误，但是没有提供详细的出错信息");
                    return false;
                }
            }
            else
            {
                mResult = tFFIvoiceExportZjBL.getResult();
                return true;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            buildError("submitData", "意外错误");
            return false;
        }
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FFIvoiceExportZjUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public List getResult()
    {
        return mResult;
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        
    }
}
