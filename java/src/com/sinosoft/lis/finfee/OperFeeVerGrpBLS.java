package com.sinosoft.lis.finfee;

import com.sinosoft.utility.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.JdbcUrl;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class OperFeeVerGrpBLS  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 数据操作字符串 */
  private String mOperate;  

  public OperFeeVerGrpBLS() {
  }
  public static void main(String[] args) {
    OperFeeVerGrpBLS mOperFeeVerGrpBLS1 = new OperFeeVerGrpBLS();
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    boolean tReturn =false;
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    System.out.println("Start OperFeeVerGrp BLS Submit...");
    //信息保存
    if(this.mOperate.equals("VERIFY"))
    {tReturn=save(cInputData);}

    if (tReturn)
      System.out.println("Save sucessful");
    else
      System.out.println("Save failed") ;
      
      System.out.println("End OperFeeVerGrp BLS Submit...");

    return tReturn;
  }
  
//保存操作  
  private boolean save(VData mInputData)
  {
   boolean tReturn =true;
    System.out.println("Start Save...");
    Connection conn = DBConnPool.getConnection();
    if (conn==null)
    {
    		// @@错误处理
    		CError tError = new CError();
		tError.moduleName = "OperFeeVerGrpBLS";
                tError.functionName = "saveData";
	        tError.errorMessage = "数据库连接失败!";
		this.mErrors .addOneError(tError) ;
 		return false;
    }
    try{
      conn.setAutoCommit(false);
// 保费项表
      System.out.println("Start 保费项表...");
      LCPremDBSet tLCPremDBSet=new LCPremDBSet(conn);
      tLCPremDBSet.set((LCPremSet)mInputData.getObjectByObjectName("LCPremSet",0));
      if (!tLCPremDBSet.update())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLCPremDBSet.mErrors);
    		CError tError = new CError();
		tError.moduleName = "OperFeeVerGrpBLS";
		tError.functionName = "saveData";
		tError.errorMessage = "保费项表数据保存失败!";
		this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();        
 		return false;
      }    
      System.out.println("1111");

// 实收集体交费表
      System.out.println("Start 实收集体交费表...");
      LJAPayGrpDB tLJAPayGrpDB=new LJAPayGrpDB(conn);
      tLJAPayGrpDB.setSchema((LJAPayGrpBL)mInputData.getObjectByObjectName("LJAPayGrpBL",0));
      if (!tLJAPayGrpDB.insert())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJAPayGrpDB.mErrors);
    		CError tError = new CError();
	        tError.moduleName = "OperFeeVerGrpBLS";
	        tError.functionName = "saveData";
	        tError.errorMessage = "实收集体交费表数据保存失败!";
	        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();        
 	        return false;
      }      
      System.out.println("2222");

      /** 实收总表 */
      
      System.out.println("Start 实收总表...");
      LJAPayDB tLJAPayDB=new LJAPayDB(conn);  
      tLJAPayDB.setSchema((LJAPayBL)mInputData.getObjectByObjectName("LJAPayBL",0));
      System.out.println("Get LJAPay");
      if (!tLJAPayDB.insert())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJAPayDB.mErrors);
    		CError tError = new CError();
		tError.moduleName = "OperFeeVerGrpBLS";
		tError.functionName = "saveData";
		tError.errorMessage = "实收总表数据保存失败!";
		this.mErrors .addOneError(tError) ;
        conn.rollback();
        conn.close();
 		return false;
      }      
      System.out.println("3333");

      /** 暂交费表 */
      
      System.out.println("Start 暂交费表...");
      LJTempFeeDBSet tLJTempFeeDBSet=new LJTempFeeDBSet(conn);
      tLJTempFeeDBSet.set((LJTempFeeSet)mInputData.getObjectByObjectName("LJTempFeeSet",0));
      if (!tLJTempFeeDBSet.update())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJTempFeeDBSet.mErrors);
    		CError tError = new CError();
		tError.moduleName = "OperFeeVerGrpBLS";
		tError.functionName = "saveData";
		tError.errorMessage = "暂交费表数据保存失败!";
		this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 		return false;
      }       
     
      System.out.println("4444");
      
      /** 暂交费分类表 */
      System.out.println("Start 暂交费分类表...");
      LJTempFeeClassDBSet tLJTempFeeClassDBSet=new LJTempFeeClassDBSet(conn);
      tLJTempFeeClassDBSet.set((LJTempFeeClassSet)mInputData.getObjectByObjectName("LJTempFeeClassSet",0));
      if (!tLJTempFeeClassDBSet.update())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJTempFeeClassDBSet.mErrors);
    		CError tError = new CError();
		tError.moduleName = "OperFeeVerGrpBLS";
		tError.functionName = "saveData";
		tError.errorMessage = "暂交费分类表数据保存失败!";
		this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 		return false;
      }       
     
      System.out.println("4444");
      conn.commit() ;
      conn.close();      
      System.out.println("5555");
    }
    catch (Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="OperFeeVerGrpBLS";
      tError.functionName="submitData";
      tError.errorMessage=ex.toString();
      this.mErrors .addOneError(tError);
      try{conn.rollback() ;} catch(Exception e){}
      tReturn=false;
    }
    return tReturn;
  }
}