package com.sinosoft.lis.finfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 暂交费退费</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class FFInvoiceNullifyUI {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mResult = new VData();
  /** 往后面传输数据的容器 */
  private VData mInputData ;

  public FFInvoiceNullifyUI() {
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData, String cOperate) {
    //首先将数据在本类中做一个备份
    mInputData = (VData)cInputData.clone();

    FFInvoiceNullifyBL tFFInvoiceNullifyBL = new FFInvoiceNullifyBL();

    System.out.println("Start FFInvoiceNullifyBL BL Submit...");
    if(!tFFInvoiceNullifyBL.submitData(mInputData, cOperate)){
        this.mErrors.copyAllErrors(tFFInvoiceNullifyBL.mErrors);
        return false;
    }
    System.out.println("End FFInvoiceNullifyBL BL Submit...");

    mInputData = null;
    return true;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }
}