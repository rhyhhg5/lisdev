package com.sinosoft.lis.finfee;

import java.sql.Connection;
import java.util.GregorianCalendar;
import com.sinosoft.lis.certify.PubCertifyTakeBack;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


public class APbalanceBL {

	/**
	 * @param args
	 */
	  //错误处理类，每个需要错误处理的类中都放置该类
	  public CErrors mErrors = new CErrors(
	      );
	  PubSubmit tPubSubmit = new PubSubmit();

	  /** 往后面传输数据的容器 */
	  private String[] existTempFeeNo;
	  private VData mInputData;
	  private GlobalInput tGI = new GlobalInput();

	  /** 数据操作字符串 */

	  private String mOperate;
	  private String CurrentDate = PubFun.getCurrentDate();
	  private String CurrentTime = PubFun.getCurrentTime();


	//  private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
	//  private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
	  private LJSPaySet mLJSPaySet = new LJSPaySet();
	  private LLCasePayAdvancedTraceSet mLLCasePayAdvancedTraceSet = new LLCasePayAdvancedTraceSet();
	  private LLCasePayAdvancedTraceSet outLLCasePayAdvancedTraceSet = new LLCasePayAdvancedTraceSet();
	  public APbalanceBL(){
		  
	  }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("===");
	}
	 //传输数据的公共方法
	  public boolean submitData(VData cInputData, String cOperate) {
	    //将操作数据拷贝到本类中
	    this.mOperate = cOperate;
	    System.out.println("Operate==" + cOperate);
	    //得到外部传入的数据,将数据备份到本类中
	    if (!getInputData(cInputData)) {
	      return false;
	    }
	    System.out.println("After getinputdata=="+mLJSPaySet.size());
	 //   LJSPaySchema sasd=(LJSPaySchema)mLJSPaySet.get(1);
	    
	    if (!prepareOutputData()) return false;
        System.out.println("---End prepareOutputData---");
	    //进行业务处理
	    if (!dealData()) {
	      return false;
	    }
	    System.out.println("After dealData！");
	    
	    //准备往后台的数据
	   
	    System.out.println("After save");

	    System.out.println("Start APbalanceBL  Submit...");

	    System.out.println("End APbalanceBL Submit...");

	    System.out.println("APbalanceBL end");
	  
	    return true;
	  }
	  /**
	   * 从输入数据中得到所有对象
	   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	   */
	  private boolean getInputData(VData tInputData)
	  {

		  mLJSPaySet=(LJSPaySet)tInputData.getObjectByObjectName("LJSPaySet",0);
		  mLLCasePayAdvancedTraceSet=(LLCasePayAdvancedTraceSet)tInputData.getObjectByObjectName("LLCasePayAdvancedTraceSet",0);
		  tGI = (GlobalInput)tInputData.getObjectByObjectName("GlobalInput",0);
		  System.out.println("mLJSPaySet:"+mLJSPaySet.size()+"mLJSPaySet:"+mLJSPaySet.size()+"tGI:"+tGI.toString());
	      if(mLJSPaySet==null||mLJSPaySet.size()==0||mLLCasePayAdvancedTraceSet==null||mLLCasePayAdvancedTraceSet.size()==0)
	      {
	          // @@错误处理
	          CError tError =new CError();
	          tError.moduleName="APbalanceBL";
	          tError.functionName="getInputData";
	          tError.errorMessage="没有得到足够的数据，请您确认!";
	          this.mErrors .addOneError(tError) ;
	          return false;
	      }
	     
	      return true;
	  }
	  private boolean dealData() {
		
		 if (!save()) {
			 this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			 System.out.println(tPubSubmit.mErrors);
			 return false;
	     }
	     return true;
		
	  }
	  private boolean prepareOutputData(){
		  LJSPaySchema tLJSPaySchema;
		  
		  if(mLJSPaySet.size()<1||mLLCasePayAdvancedTraceSet.size()<1){
			  System.out.print("prepareOutputData.errorMessage");
			  // @@错误处理
	          CError tError =new CError();
	          tError.moduleName="APbalanceBL";
	          tError.functionName="prepareOutputData";
	          tError.errorMessage="没有得到足够的数据，请您确认!";
	          this.mErrors .addOneError(tError) ;
			  return false;
		  }
		
		  for(int i=1;i<=mLJSPaySet.size();i++){
			  String prtSeq = PubFun1.CreateMaxNo("PAYNOTICENO",mLJSPaySet.get(i).getOtherNo());
			  System.out.println("ljspay.prtSeq(otherno)="+prtSeq);
	            String serNo = PubFun1.CreateMaxNo("SERIALNO",mLJSPaySet.get(i).getManageCom() );
	            System.out.println("ljspay.serNo="+serNo);
	            GregorianCalendar Calendar = new GregorianCalendar();
	            Calendar.setTime((new FDate()).getDate(PubFun.getCurrentDate()));
	            Calendar.add(Calendar.DATE, 0);
	            mLJSPaySet.get(i).setGetNoticeNo(prtSeq);
	            mLJSPaySet.get(i).setPayDate(CurrentDate);
	            mLJSPaySet.get(i).setStartPayDate(CurrentDate);
	            mLJSPaySet.get(i).setSerialNo(serNo); 
	            mLJSPaySet.get(i).setMakeDate(CurrentDate); 
	            mLJSPaySet.get(i).setMakeTime(CurrentTime);
	            mLJSPaySet.get(i).setModifyDate(CurrentDate);
	            mLJSPaySet.get(i).setModifyTime(CurrentTime);
			  
		  }
		  LLCasePayAdvancedTraceDB tLLCasePayAdvancedTraceDB;
		  LLCasePayAdvancedTraceSet ttLLCasePayAdvancedTraceSet;
		  LLCasePayAdvancedTraceSchema ttLLCasePayAdvancedTraceSchema;
		  for(int i=1;i<=mLLCasePayAdvancedTraceSet.size();i++){	
			  tLLCasePayAdvancedTraceDB=new LLCasePayAdvancedTraceDB();
			  tLLCasePayAdvancedTraceDB.setSchema(mLLCasePayAdvancedTraceSet.get(i));
			  ttLLCasePayAdvancedTraceSet=tLLCasePayAdvancedTraceDB.query();
			  for(int ii=1;ii<=ttLLCasePayAdvancedTraceSet.size();ii++){
				  ttLLCasePayAdvancedTraceSchema=ttLLCasePayAdvancedTraceSet.get(ii);
				  ttLLCasePayAdvancedTraceSchema.setGetNoticeNo(mLJSPaySet.get(1).getGetNoticeNo());
				  ttLLCasePayAdvancedTraceSchema.setDealState("03");
				  ttLLCasePayAdvancedTraceSchema.setModifyDate(mLJSPaySet.get(1).getModifyDate());
				  ttLLCasePayAdvancedTraceSchema.setModifyTime(mLJSPaySet.get(1).getModifyTime());
				  outLLCasePayAdvancedTraceSet.add(ttLLCasePayAdvancedTraceSchema);
			  }
			  	  
		  }
		  return true;
	  }
      private boolean save(){
	  
	  
	 	boolean tReturn = true;
	 	System.out.println("Start Save...");
	 	Connection conn = DBConnPool.getConnection();

	 	if (conn == null){
     // @@错误处理
     CError tError = new CError();
     tError.moduleName = "APbalanceBL";
     tError.functionName = "saveData";
     tError.errorMessage = "数据库连接失败!";
     this.mErrors.addOneError(tError);
     return false;
	 	}
 try{
     conn.setAutoCommit(false);
     LJSPayDBSet tLJSPayDBSet = new LJSPayDBSet(conn);
     tLJSPayDBSet.set(mLJSPaySet);
      if (!tLJSPayDBSet.insert())
      {  
          // @@错误处理
          this.mErrors.copyAllErrors(tLJSPayDBSet.mErrors);
          CError tError = new CError();
          tError.moduleName = "APbalanceBL";
          tError.functionName = "saveData";
          tError.errorMessage = "应收表数据保存失败!";
          this.mErrors.addOneError(tError);
          conn.rollback();
          conn.close();
          return false;
      }
      LLCasePayAdvancedTraceDBSet tLLCasePayAdvancedTraceDBSet=new LLCasePayAdvancedTraceDBSet(conn);
      tLLCasePayAdvancedTraceDBSet.set(outLLCasePayAdvancedTraceSet);
      if (!tLLCasePayAdvancedTraceDBSet.update())
      {  
          // @@错误处理
          this.mErrors.copyAllErrors(tLJSPayDBSet.mErrors);
          CError tError = new CError();
          tError.moduleName = "APbalanceBL";
          tError.functionName = "saveData";
          tError.errorMessage = "LLCasePayAdvancedTrace表数据更新失败!";
          this.mErrors.addOneError(tError);
          conn.rollback();
          conn.close();
          return false;
      }
      conn.commit();
      conn.close();
      System.out.println("commit end");
  }
  catch (Exception ex)
  {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "APbalanceBL";
      tError.functionName = "save";
      tError.errorMessage = ex.toString();
      this.mErrors.addOneError(tError);
      try
      {
          conn.rollback();
      }
      catch (Exception e)
      {}
      tReturn = false;
  }
  return tReturn;
 }
    
}
