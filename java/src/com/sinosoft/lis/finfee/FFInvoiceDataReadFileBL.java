package com.sinosoft.lis.finfee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.f1j.ss.BookModelImpl;
import com.f1j.ss.ReadParams;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LOPRTInvoiceManagerSchema;
import com.sinosoft.lis.vschema.LOPRTInvoiceManagerSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
  


public class FFInvoiceDataReadFileBL
{
    private VData mInputData = new VData();
    /** 提交数据的容器 */
    private MMap map = new MMap();

    /** 数据操作字符串 */
    private String mOperate;

    /** 错误处理类 */
    public CErrors mErrors = new CErrors();

    private String mFilePath = "";

    private BookModelImpl book = new BookModelImpl();

    //业务数据
    private TransferData inTransferData = new TransferData();

    private String fileName = "";

    private LOPRTInvoiceManagerSet outLOPRTInvoiceManagerSet = new LOPRTInvoiceManagerSet();


    private GlobalInput tG = new GlobalInput();

    private List exList = new ArrayList();

    /**
	 * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	 * 
	 * @param cInputData
	 *            传入的数据,VData对象
	 * @param cOperate
	 *            数据操作字符串，主要包括"READ"和""
	 * @return 布尔值（true--提交成功, false--提交失败）
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}
		System.out.println("---End getInputData---");

		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		System.out.println("---End dealData---");

		if (!prepareOutputData()) {
			return false;
		}

		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, cOperate)) {
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "FFInvoiceDataReadFileBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mInputData = null;
		return true;
	}

      

    /**
	 * 将外部传入的数据分解到本类的属性中
	 * 
	 * @param: 无
	 * 
	 * @return: boolean
	 */
    private boolean getInputData() {
		try {
			inTransferData = (TransferData) mInputData.getObjectByObjectName(
					"TransferData", 0);
			tG = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput",
					0);

			if (tG == null || inTransferData == null) {
				CError tError = new CError();
				tError.moduleName = "FFInvoiceDataReadFileBL";
				tError.functionName = "checkList";
				tError.errorMessage = "接收数据失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "FFInvoiceDataReadFileBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "接收数据失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

    /**
	 * 根据前面的输入数据，进行逻辑处理
	 * 
	 * @return 如果在处理过程中出错，则返回false,否则返回true
	 */
    private boolean dealData() {
		try {
			// 获取银行文件数据
			if (mOperate.equals("READ")) {
				// 获取返回文件名称
				fileName = (String) inTransferData.getValueByName("fileName");
				fileName = fileName.replace('\\', '/');
				// 读取银行返回文件，公共部分
				System.out.println("---开始读取文件---");
				if (!readFFInvoiceDataFile(fileName)) {
					return false;
				}

				if (!checkList()) {
					return false;
				}

				if (!setLOPRTInvoiceManager()) {
					return false;
				}

				VData tVData = new VData();
				tVData.clear();
				tVData.add(outLOPRTInvoiceManagerSet);
				tVData.add(tG);

			}
		} catch (Exception e) {
			// @@错误处理
			e.printStackTrace();
			CError tError = new CError();
			tError.moduleName = "FFInvoiceDataReadFileBL";
			tError.functionName = "dealData";
			tError.errorMessage = "数据处理错误:" + e.getMessage();
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

    /**
	 * 读取收费导入文件
	 * 
	 * @param fileName
	 * @return
	 */
    private boolean readFFInvoiceDataFile(String fileName) {
		try {
			mFilePath = fileName;
			book.initWorkbook();// 初始
			book.read(mFilePath, new ReadParams());
			int tSheetNums = book.getNumSheets();// 读去sheet的个数，就是一个excel包含几个sheet
			System.out.println("tSheetNums===" + tSheetNums);

			for (int sheetNum = 0; sheetNum < tSheetNums; sheetNum++) {
				book.setSheet(sheetNum);
				int tLastCol = book.getLastCol();// 总共多少列，他的列数从０开始，比如有４列，那tLastCol的结果＝３
				int tLastRow = book.getLastRow();// 总共多少行，他的行也是从０开始，比如有２行，那tLastRow的结果＝１
				System.out.println("tLastCol===" + tLastCol);
				System.out.println("tLastRow===" + tLastRow);
				String colValue = "";
				// 按列循环
				for (int j = 1; j <= tLastRow; j++) {// 循环总行数
					Map tMap = new HashMap();
					for (int i = 0; i <= tLastCol; i++) {// 循环总列数
						if (book.getText(j, i) != null) {
							colValue = (String) book.getText(j, i).trim();
						} else {
							colValue = "";
						}
						switch (i) {
						case 0:
							tMap.put("InvoiceCode", colValue);
							break;
						case 1:
							tMap.put("InvoiceNo", colValue);
							break;
						case 2:
							tMap.put("ContNo", colValue);
							break;
						case 3:
							tMap.put("XSumMoney", colValue);
							break;
						case 4:
							tMap.put("StateFlag", colValue);
							break;
						case 5:
							tMap.put("PayerName", colValue);
							break;
						case 6:
							tMap.put("Operator", colValue);
							break;
						case 7:
							tMap.put("Opdate", colValue);
							break;
						case 8:
							tMap.put("ComCode", colValue);
							break;
						case 9:
							tMap.put("CertifyCode", colValue);
							break;
						}

					}
					tMap.put("ManageCom", tG.ManageCom);
					exList.add(tMap);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			CError tError = new CError();
			tError.moduleName = "FFInvoiceDataReadFileBL";
			tError.functionName = "readFFInvoiceDataFile";
			tError.errorMessage = "读取导入文件错误:" + ex.getMessage();
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

    /**
     * 对导入发票数据进行校验
     */
    private boolean checkList() {
		if (exList.size() == 0) {
			System.out.println("-----exList-size-0-----");
			CError tError = new CError();
			tError.moduleName = "FFInvoiceDataReadFileBL";
			tError.functionName = "checkList";
			tError.errorMessage = "未发现导入文件中存在发票信息!";
			this.mErrors.addOneError(tError);
			return false;
		}

		for (int k = 0; k < exList.size(); k++) {
			Map tMap = (Map) exList.get(k);
			ExeSQL tExeSQL = new ExeSQL();
			SSRS tSSRS = new SSRS();
			String InvoiceCode;
			String InvoiceNo;
			InvoiceCode = (String) tMap.get("InvoiceCode");
			InvoiceNo = (String) tMap.get("InvoiceNo");
            
			//发票代码和发票号码不能为空			
			if (InvoiceCode.equals("") || InvoiceNo.equals("")) {
				CError tError = new CError();
				tError.moduleName = "TempFeeBatchReadFileBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k +2) + "行数据发票代码或发票号码不能为空!";
				this.mErrors.addOneError(tError);
				return false;
			}
			//发票代码必须为12位
			if (InvoiceCode.length() !=12) {
				System.out.println(InvoiceCode);
				CError tError = new CError();
				tError.moduleName = "FFInvoiceDataReadFileBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k +2) + "行数据发票代码不是12位，请核对数据!";
				this.mErrors.addOneError(tError);
				return false;
			}
             //发票号码必须为12位
			if (InvoiceNo.length() != 8) {
				System.out.println(InvoiceNo);
				CError tError = new CError();
				tError.moduleName = "FFInvoiceDataReadFileBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k +2) + "行数据发票号码不是8位，请核对数据!";
				this.mErrors.addOneError(tError);
				return false;
			}
			//校验保单信息是否存在
			if (!tMap.get("ContNo").toString().equals("")) {
				String sql = "select 1 from lccont where contno='"
						+ tMap.get("ContNo").toString() + "' union "
						+ "select 1 from lbcont where contno='"
						+ tMap.get("ContNo").toString() + "' union "
						+ "select 1 from lcgrpcont where grpcontno='"
						+ tMap.get("ContNo").toString() + "' union "
						+ "select 1 from lbgrpcont where grpcontno='"
						+ tMap.get("ContNo").toString() + "' with ur ";
				System.out.println(sql);
				tSSRS = tExeSQL.execSQL(sql);
				if (tSSRS.MaxRow == 0) {
					CError tError = new CError();
					tError.moduleName = "FFInvoiceDataReadFileBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k +2) + "行数据未查到相关的保单信息！";
					this.mErrors.addOneError(tError);
					return false;
				}
			}
		   // 金额不能为空
			if (tMap.get("XSumMoney").toString().equals("")) {
				CError tError = new CError();
				tError.moduleName = "FFInvoiceDataReadFileBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k +2) + "行数据金额不能为空！";
				this.mErrors.addOneError(tError);
				return false;
			}
             //发票状态不能为空
			if (tMap.get("StateFlag").toString().equals("")) {
				CError tError = new CError();
				tError.moduleName = "FFInvoiceDataReadFileBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k +2) + "行数据发票状态不能为空！";
				this.mErrors.addOneError(tError);
				return false;
			} else {
			//	发票状态只能为正常或者作废
				if (!(tMap.get("StateFlag").toString().equals("正常") || tMap
						.get("StateFlag").toString().equals("作废"))) {
					CError tError = new CError();
					tError.moduleName = "FFInvoiceDataReadFileBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k + 1)
							+ "笔数据发票状态异常，发票状态只能为正常或者作废！";
					this.mErrors.addOneError(tError);
					return false;
				}

			}
			// 付费方名称不能为空
			if (tMap.get("PayerName").toString().equals("")) {
				CError tError = new CError();
				tError.moduleName = "FFInvoiceDataReadFileBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k +2) + "行数据付费方名称不能为空！";
				this.mErrors.addOneError(tError);
				return false;
			}
			
			//校验开票人是否存在
			if (!tMap.get("Operator").toString().equals("")) {
			String sql = "select '1' from lduser where usercode='"
					+ tMap.get("Operator").toString() + "' with ur ";
			System.out.println(sql);
			tSSRS = tExeSQL.execSQL(sql);
			if (tSSRS.MaxRow == 0) {
				CError tError = new CError();
				tError.moduleName = "FFInvoiceDataReadFileBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k +2) + "行数据开票人员不存在！";
				this.mErrors.addOneError(tError);
				return false;
			    }
		    }
			// 校验日期格式 2012-09-01 或 2012-9-1

			if (!DateCheck(tMap.get("Opdate").toString())) {
				System.out.println(tMap.get("Opdate").toString());
				CError tError = new CError();
				tError.moduleName = "FFInvoiceDataReadFileBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k + 1)
						+ "笔数据开票日期格式错误,请校验是否为文本格式！";
				this.mErrors.addOneError(tError);
				return false;
			}
            //管理机构不能为空
			if (tMap.get("ComCode").toString().equals("")) {
				CError tError = new CError();
				tError.moduleName = "FFInvoiceDataReadFileBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k +2) + "行数据管理机构不能为空！";
				this.mErrors.addOneError(tError);
				return false;
			} else {
				// 校验管理机构是否存在
				String sql = "select '1' from ldcom where comcode='"
						+ tMap.get("ComCode").toString() + "' with ur ";
				System.out.println(sql);
				tSSRS = tExeSQL.execSQL(sql);
				if (tSSRS.MaxRow == 0) {
					CError tError = new CError();
					tError.moduleName = "FFInvoiceDataReadFileBL";
					tError.functionName = "checkList";
					tError.errorMessage = "第" + (k +2) + "行数据管理机构不存在！";
					this.mErrors.addOneError(tError);
					return false;
				}
			}
			
			String com = tMap.get("ComCode").toString();
			// 大连分公司 单证编码不能为空
			if (tMap.get("CertifyCode").toString().equals("") && com.length() > 3 && "8691".equals(com.substring(0, 4))) {
				CError tError = new CError();
				tError.moduleName = "FFInvoiceDataReadFileBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k +2) + "行数据单证编码不能为空！";
				this.mErrors.addOneError(tError);
				return false;
			} else {
				if(!tMap.get("CertifyCode").toString().equals("")){
					// 校验单证编码是否存在
					String sql = "select '1' from lmcertifydes where certifycode='"
							+ tMap.get("CertifyCode").toString() + "' with ur ";
					System.out.println(sql);
					tSSRS = tExeSQL.execSQL(sql);
					if (tSSRS.MaxRow == 0) {
						CError tError = new CError();
						tError.moduleName = "FFInvoiceDataReadFileBL";
						tError.functionName = "checkList";
						tError.errorMessage = "第" + (k +2) + "行数据单证编码不存在！";
						this.mErrors.addOneError(tError);
						return false;
					}
				}
			}
			
			//校验单证基本信息是否存在
			String sql = "select 1 from LJInvoiceInfo where invoicecode='"
				+ tMap.get("InvoiceCode").toString() + 
				"' and  '"
				+ tMap.get("InvoiceNo").toString() + 
				"' between invoicestartno and  invoiceendno "
				+" and  comcode='"
				+ tMap.get("ComCode").toString() + 
				"' with ur ";
			System.out.println(sql);
			tSSRS = tExeSQL.execSQL(sql);
			if (tSSRS.MaxRow == 0) {
				CError tError = new CError();
				tError.moduleName = "FFInvoiceDataReadFileBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k +2) + "行数据，发票信息不存在，请到发票基本信息界面进行发票录入！";
				this.mErrors.addOneError(tError);
				return false;
			}
			
			// 校验发票基本信息是否存在
			String exSql = "select 1 from LOPRTInvoiceManager where INVOICENO='"
					+ tMap.get("InvoiceNo").toString()
					+ "' and INVOICECODE='"
					+ tMap.get("InvoiceCode").toString()
					+ "' and COMCODE='"
					+ tMap.get("ComCode").toString() + "'";
			System.out.println(exSql);
			tSSRS = tExeSQL.execSQL(exSql);
			if (tSSRS.MaxRow != 0) {
				CError tError = new CError();
				tError.moduleName = "FFInvoiceDataReadFileBL";
				tError.functionName = "checkList";
				tError.errorMessage = "第" + (k + 2) + "行数据，发票信息已存在！";
				this.mErrors.addOneError(tError);
				return false;
			}
		}

		return true;
	}



    public boolean setLOPRTInvoiceManager() {
		for (int i = 0; i < exList.size(); i++) {

			Map tMap = (Map) exList.get(i);

			LOPRTInvoiceManagerSchema tLOPRTInvoiceManagerSchema = new LOPRTInvoiceManagerSchema();
			tLOPRTInvoiceManagerSchema.setInvoiceCode(tMap.get("InvoiceCode").toString());
			tLOPRTInvoiceManagerSchema.setInvoiceNo(tMap.get("InvoiceNo").toString());
			tLOPRTInvoiceManagerSchema.setContNo(tMap.get("ContNo").toString());
			tLOPRTInvoiceManagerSchema.setXSumMoney(tMap.get("XSumMoney").toString());
			tLOPRTInvoiceManagerSchema.setStateFlag(tMap.get("StateFlag").toString());
			tLOPRTInvoiceManagerSchema.setMemo(tMap.get("CertifyCode").toString());
			
			// 发票状态转换成代码  0-正常   2-作废
			if (tMap.get("StateFlag").toString().equals("正常")) {
				tLOPRTInvoiceManagerSchema.setStateFlag("0");

			} else if (tMap.get("StateFlag").toString().equals("作废")) {
				tLOPRTInvoiceManagerSchema.setStateFlag("2");
			}

			tLOPRTInvoiceManagerSchema.setPayerName(tMap.get("PayerName").toString());
            
			// 开票人为空  则默认为操作员
			if (tMap.get("Operator").toString().equals("")) {
				tLOPRTInvoiceManagerSchema.setOperator(tG.Operator);
			} else {
				tLOPRTInvoiceManagerSchema.setOperator(tMap.get("Operator").toString());
			}

			tLOPRTInvoiceManagerSchema.setOpdate(tMap.get("Opdate").toString());
			tLOPRTInvoiceManagerSchema.setComCode(tMap.get("ComCode").toString());
			tLOPRTInvoiceManagerSchema.setMakeDate(PubFun.getCurrentDate());
			tLOPRTInvoiceManagerSchema.setMakeTime(PubFun.getCurrentTime());
			tLOPRTInvoiceManagerSchema.setModifyDate(PubFun.getCurrentDate());
			tLOPRTInvoiceManagerSchema.setModifyTime(PubFun.getCurrentTime());
			outLOPRTInvoiceManagerSet.add(tLOPRTInvoiceManagerSchema);
		}
		return true;
	}
   
	// 校验日期格式 满足2012-9-1 和2012-09-01 
    public boolean DateCheck(String Date) {
	
		String eL = "[1-9][0-9]{3}-(([0]?[1-9])|(1[0-2]))-((0?[1-9])|([1-2][0-9])|(3[0-1]))";
		if (!Date.matches(eL))

		{
			return false;

		}
		return true;
	}
    
    /**
	 * 获取差选条件的值 用于前台界面展示
	 * 获取发票代码
	 */

	public String getInvoiceCode() {
		if (exList.size() == 0) {
			return "''";
		}
		String sql = "";
		for (int i = 0; i < exList.size() - 1; i++) {
			sql = sql + "'"
					+ ((Map) exList.get(i)).get("InvoiceCode").toString()
					+ "',";
		}
		sql = sql
				+ "'"
				+ ((Map) exList.get(exList.size() - 1)).get("InvoiceCode")
						.toString() + "'";
		return sql;
	}
	
	/**
	 * 获取差选条件的值 用于前台界面展示
	 * 获取发票号码
	 */

	public String getInvoiceNo() {
		if (exList.size() == 0) {
			return "''";
		}
		String sql = "";
		for (int i = 0; i < exList.size() - 1; i++) {
			sql = sql + "'" + ((Map) exList.get(i)).get("InvoiceNo").toString()
					+ "',";
		}
		sql = sql
				+ "'"
				+ ((Map) exList.get(exList.size() - 1)).get("InvoiceNo")
						.toString() + "'";
		return sql;
	}
	/**
	 * 获取差选条件的值 用于前台界面展示
	 * 获取管理机构代码
	 */

       public String getComCode() {
		if (exList.size() == 0) {
			return "''";
		}
		String sql = "";
		for (int i = 0; i < exList.size() - 1; i++) {
			sql = sql + "'" + ((Map) exList.get(i)).get("ComCode").toString()
					+ "',";
		}
		sql = sql
				+ "'"
				+ ((Map) exList.get(exList.size() - 1)).get("ComCode")
						.toString() + "'";
		return sql;
	}
          
       private boolean prepareOutputData() {
		try {

			map.put(outLOPRTInvoiceManagerSet, "INSERT");
			mInputData.clear();
			mInputData.add(map);
		} catch (Exception ex) {
			// @@错误处理
			CError.buildErr(this, "在准备往后层处理所需要的数据时出错");
			return false;
		}

		return true;
	}
    
       

    public static void main(String[] arr) {
		/*VData tInputData = new VData();
		TransferData tTransferData = new TransferData();
		GlobalInput tGl = new GlobalInput();

		tGl.ManageCom = "86110000";
		tGl.ComCode = "86110000";
		tGl.Operator = "cwad";

		tTransferData.setNameAndValue("fileName", "C:\\发票信息导入.xls");
		tInputData.add(tGl);
		tInputData.add(tTransferData);

		FFInvoiceDataReadFileBL tFFInvoiceDataReadFileBL = new FFInvoiceDataReadFileBL();
		tFFInvoiceDataReadFileBL.submitData(tInputData, "READ");
		System.out.println(tFFInvoiceDataReadFileBL.mErrors.getFirstError());*/

	}
}
