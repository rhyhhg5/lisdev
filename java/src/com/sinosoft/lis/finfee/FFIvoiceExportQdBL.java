/**
 * 2011-10-22
 */
package com.sinosoft.lis.finfee;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.db.LJInvoiceInfoDB;
import com.sinosoft.lis.db.LOPRTInvoiceManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LJInvoiceInfoSet;
import com.sinosoft.lis.vschema.LOPRTInvoiceManagerSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author Zcx
 * 
 */
public class FFIvoiceExportQdBL implements InvoiceExport {
	public CErrors mErrors = new CErrors();

	private GlobalInput mGlobalInput = null;

	private ArrayList mResult = new ArrayList();

	private String mBasePath = "";

	private String mOutPath = "";

	private String mSsqq = "";

	private String mSsqz = "";

	private String mSFState = "";

	private String fileName = "";

	private String TaxpayerNo = "";

	private String TaxpayerName = "";

	private SSRS mSSRS = new SSRS();

	private LOPRTInvoiceManagerSet tLOPRTInvoiceManagerSet = new LOPRTInvoiceManagerSet();

	public boolean submitData(VData cInputData, String cOperate) {
		if (!getInputData(cInputData)) {
			buildError("getInputData", "没有获取到足够业务信息或者机构不对");
			return false;
		}
		if (!dealData()) {
			buildError("dealData", "生成txt文件失败");
			return false;
		}
		return true;
	}

	/**
	 * 获取业务数据
	 * 
	 * @param cInputData
	 * @return
	 */
	private boolean getInputData(VData cInputData) {
		TransferData tParameters = (TransferData) cInputData
				.getObjectByObjectName("TransferData", 0);
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);

		if (mGlobalInput == null || tParameters == null) {
			return false;
		}

		this.mBasePath = (String) tParameters.getValueByName("hostBasePath")
				+ "/";
		this.mOutPath = (String) tParameters.getValueByName("outPath") + "/";

		this.mSsqq = (String) tParameters.getValueByName("invoiceStartDate");
		this.mSsqz = (String) tParameters.getValueByName("invoiceEndDate");
		this.mSFState = (String) tParameters.getValueByName("SFState");
		// 获取条件内的相关发票打印信息
		String tSql = "";
		if ("1".equals(mSFState)) {
			tSql = " select lja.otherno,lop.makedate from loprtmanager2 lop,ljaget lja "
					+ " where lop.code='37' "
					+ " and lop.standbyflag4 is not null "
					+ " and lop.makedate >= '"
					+ mSsqq
					+ "'"
					+ " and lop.makedate <= '"
					+ mSsqz
					+ "'"
					+ " and lop.managecom like '"
					+ mGlobalInput.ManageCom
					+ "%' "
					+ " and lop.otherno=lja.actugetno "
					+ " order by lop.makedate ";
			buildError("dealData", "不支持付费发票导出");
			return false;

		} else {
			tSql = " select startno,payno,paytype,makedate,money,income  "
					+ " from "
					+ " (select lop.standbyflag4 startno,lja.payno payno,lja.incometype paytype,lop.makedate makedate,lja.SumActuPayMoney  money ,lja.incomeno income"
					+ " from loprtmanager2 lop,ljapay lja "
					+ " where lop.code='35' "
					+ " and lop.standbyflag4 is not null  "
					+ " and lop.makedate >= '"
					+ mSsqq
					+ "' "
					+ " and lop.makedate <= '"
					+ mSsqz
					+ "' "
					+ " and lop.managecom like '"
					+ mGlobalInput.ManageCom
					+ "%' "
					+ " and lop.standbyflag5 <> '5' "
					+ " and lop.otherno=lja.payno "
					+ " union "
					+ " select lop.standbyflag4,lja.payno,lja.incometype,lop.makedate,lop.printamount,lja.incomeno from loprtmanager2 lop,ljapay lja  "
					+ " where lop.code='35' "
					+ " and lop.standbyflag4 is not null "
					+ " and lop.makedate >= '"
					+ mSsqq
					+ "' "
					+ " and lop.makedate <= '"
					+ mSsqz
					+ "' "
					+ " and lop.managecom like '"
					+ mGlobalInput.ManageCom
					+ "%' "
					+ " and lop.standbyflag5 = '5' "
					+ " and lop.otherno=lja.payno ) temp "
					+ " order by makedate ";
		}
		ExeSQL tExeSQL = new ExeSQL();
		mSSRS = tExeSQL.execSQL(tSql);
		System.out.println(tSql);
		if (mSSRS == null || mSSRS.getMaxRow() < 1) {
			return false;
		}

		// tSql = "select * from LJInvoiceInfo where comcode = '"
		// + mGlobalInput.ManageCom + "' " + " and invoicecode='"
		// + tLOPRTInvoiceManagerSet.get(1).getInvoiceCode() + "'"
		// + " order by makedate desc fetch first 1 rows only with ur ";
		// System.out.println(tSql);
		// LJInvoiceInfoDB tLJInvoiceInfoDB = new LJInvoiceInfoDB();
		// LJInvoiceInfoSet tLJInvoiceInfoSet = tLJInvoiceInfoDB
		// .executeQuery(tSql);
		// if (tLJInvoiceInfoSet == null || tLJInvoiceInfoSet.size() <= 0)
		// return false;
		// this.TaxpayerNo = tLJInvoiceInfoSet.get(1).getTaxpayerNo();
		// this.TaxpayerName = tLJInvoiceInfoSet.get(1).getTaxpayerName();

		return true;
	}

	private boolean dealData() {
		System.out.println("export txt begin...");
		try {
			String tSql = "";
			SSRS tSSRS = new SSRS();
			String tRiskName = "";
			String tRiskName2 = "";
			ExeSQL tExeSQL = new ExeSQL();
			String write = "发票号码\t开票日期\t险种\t开具金额\n";
			for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
				write = write + "\"\t" + mSSRS.GetText(i, 1) + "\t\"\t"; //发票号码
				String date = mSSRS.GetText(i, 4);  
				write = write + date + "\t";   //开票日期
				if (mSSRS.GetText(i, 3).equals("2")) {

					tSql = "select c.AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
							+ "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
							+ "when '530301' then (select riskwrapplanname from ldcode1 "
							+ "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),a.riskcode,c.AppntNo from lcpol a,lmriskapp b,lccont c where c.ContNo = a.ContNo "
							+ "and a.ContNo = '"
							+ mSSRS.GetText(i, 6)
							+ "' and a.riskcode=b.riskcode and b.subriskflag='M'  order by a.prem desc";
					tSSRS = tExeSQL.execSQL(tSql);
					if (tSSRS.getMaxRow() > 0) {
						tRiskName = tSSRS.GetText(1, 2);
						tRiskName2 = getRiskName(tSSRS.GetText(1, 3), tExeSQL,
								mSSRS.GetText(i, 6));
						if (tRiskName2 != "" && tRiskName2 != null) {
							tRiskName = tRiskName2;
						}
					} else {
						tSql = "select c.AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
								+ "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
								+ "when '530301' then (select riskwrapplanname from ldcode1 "
								+ "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),a.riskcode,c.AppntNo from lbpol a,lmriskapp b,lbcont c where c.ContNo = a.ContNo "
								+ "and a.ContNo = '"
								+ mSSRS.GetText(i, 6)
								+ "' and a.riskcode=b.riskcode and b.subriskflag='M'  order by a.prem desc";
						tSSRS = tExeSQL.execSQL(tSql);
						if (tSSRS.getMaxRow() > 0) {
							tRiskName = tSSRS.GetText(1, 2);
							tRiskName2 = getRiskName(tSSRS.GetText(1, 3),
									tExeSQL, mSSRS.GetText(i, 6));
							if (tRiskName2 != "" && tRiskName2 != null) {
								tRiskName = tRiskName2;
							}
						}
					}

				}
				if (mSSRS.GetText(i, 3).equals("1")) {
					tSql = "select  GrpName,RiskName,a.riskcode,a.CustomerNo from lcgrppol a,lmriskapp b where GrpContNo = '"
							+ mSSRS.GetText(i, 6)
							+ "' and a.riskcode=b.riskcode and b.subriskflag='M'  order by a.prem desc";
					tSSRS = tExeSQL.execSQL(tSql);
					if (tSSRS.getMaxRow() > 0) {
						tRiskName = tSSRS.GetText(1, 2);
						tRiskName2 = getRiskName(tSSRS.GetText(1, 3), tExeSQL,
								mSSRS.GetText(i, 6));
						if (tRiskName2 != "" && tRiskName2 != null) {
							tRiskName = tRiskName2;
						}
					} else {
						tSql = "select  GrpName,RiskName,a.riskcode,a.CustomerNo from lbgrppol a,lmriskapp b where GrpContNo = '"
								+ mSSRS.GetText(i, 6)
								+ "' and a.riskcode=b.riskcode and b.subriskflag='M'  order by a.prem desc";
						tSSRS = tExeSQL.execSQL(tSql);
						if (tSSRS.getMaxRow() > 0) {
							tRiskName = tSSRS.GetText(1, 2);
							tRiskName2 = getRiskName(tSSRS.GetText(1, 3),
									tExeSQL, mSSRS.GetText(i, 6));
							if (tRiskName2 != "" && tRiskName2 != null) {
								tRiskName = tRiskName2;
							}
						}

					}

				}
				if (mSSRS.GetText(i, 3).equals("10")) {
					tSql = "select  AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
							+ "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
							+ "when '530301' then (select riskwrapplanname from ldcode1 "
							+ "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),b.riskcode,b.AppntNo,b.contno from LJAGetEndorse a, lcpol b, lmriskapp c where ActuGetNo = '"
							+ mSSRS.GetText(i, 2)
							+ "'  and b.stateflag = '1' and a.contno=b.contno and b.riskcode=c.riskcode and c.subriskflag='M'  order by b.prem desc";
					tSSRS = tExeSQL.execSQL(tSql);
					if (tSSRS.getMaxRow() > 0) {
						tRiskName = tSSRS.GetText(1, 2);
						tRiskName2 = getRiskName(tSSRS.GetText(1, 3), tExeSQL,
								tSSRS.GetText(1, 5));
						if (tRiskName2 != "" && tRiskName2 != null) {
							tRiskName = tRiskName2;
						}
					} else {
						tSql = "select  AppntName,(case a.riskcode when '330501' then (select riskwrapplanname from ldcode1 "
								+ "where codetype = 'bankcheckappendrisk' and code1 = '330501') "
								+ "when '530301' then (select riskwrapplanname from ldcode1 "
								+ "where codetype = 'bankcheckappendrisk' and code1 = '330501') else RiskName end),b.riskcode,b.AppntNo,b.contno from LJAGetEndorse a, lbpol b, lmriskapp c where ActuGetNo = '"
								+ mSSRS.GetText(i, 2)
								+ "'  and b.stateflag = '1' and a.contno=b.contno and b.riskcode=c.riskcode and c.subriskflag='M'  order by b.prem desc";
						tSSRS = tExeSQL.execSQL(tSql);
						if (tSSRS.getMaxRow() > 0) {
							tRiskName = tSSRS.GetText(1, 2);
							tRiskName2 = getRiskName(tSSRS.GetText(1, 3),
									tExeSQL, tSSRS.GetText(1, 5));
							if (tRiskName2 != "" && tRiskName2 != null) {
								tRiskName = tRiskName2;
							}
						}

					}

				}
				if (mSSRS.GetText(i, 3).equals("3")) {
					tSql = "select  b.GrpName,RiskName,b.riskcode,b.CustomerNo,b.grpcontno from LJAGetEndorse a, lcgrppol b, lmriskapp c where ActuGetNo = '"
							+ mSSRS.GetText(i, 2)
							+ "' and a.grpcontno=b.grpcontno and a.grpcontno=(select  grpcontno from lpgrpedoritem where edorno=a.endorsementno fetch first 1 rows only)"
							+ " and b.riskcode=c.riskcode and c.subriskflag='M' order by b.prem desc";
					tSSRS = tExeSQL.execSQL(tSql);
					if (tSSRS.getMaxRow() > 0) {
						tRiskName = tSSRS.GetText(1, 2);
						tRiskName2 = getRiskName(tSSRS.GetText(1, 3), tExeSQL,
								tSSRS.GetText(1, 5));
						if (tRiskName2 != "" && tRiskName2 != null) {
							tRiskName = tRiskName2;
						}
					} else {
						tSql = "select  b.GrpName,RiskName,b.riskcode,b.CustomerNo,b.grpcontno from LJAGetEndorse a, lbgrppol b, lmriskapp c where ActuGetNo = '"
								+ mSSRS.GetText(i, 2)
								+ "' and a.grpcontno=b.grpcontno and a.grpcontno=(select  grpcontno from lpgrpedoritem where edorno=a.endorsementno fetch first 1 rows only)"
								+ " and b.riskcode=c.riskcode and c.subriskflag='M' order by b.prem desc";
						tSSRS = tExeSQL.execSQL(tSql);
						if (tSSRS.getMaxRow() > 0) {
							tRiskName = tSSRS.GetText(1, 2);
							tRiskName2 = getRiskName(tSSRS.GetText(1, 3),
									tExeSQL, tSSRS.GetText(1, 5));
							if (tRiskName2 != "" && tRiskName2 != null) {
								tRiskName = tRiskName2;
							}
						}

					}

				}
				if (mSSRS.GetText(i, 3).equals("13")) {

					tSql = "Select  b.GrpName,RiskName,b.riskcode,b.CustomerNo,b.grpcontno from ljagetendorse a,lcgrppol b,lmriskapp c "
							+ "where actugetno in ( Select Btactuno from ljaedorbaldetail "
							+ "where actuno in ( Select getnoticeno from LJAPay where payno='"
							+ mSSRS.GetText(i, 2)
							+ "' )) "
							+ "and a.grpcontno=b.grpcontno and b.riskcode=c.riskcode and c.subriskflag='M' order by b.prem desc ";
					tSSRS = tExeSQL.execSQL(tSql);
					if (tSSRS.getMaxRow() > 0) {
						tRiskName = tSSRS.GetText(1, 2);
						tRiskName2 = getRiskName(tSSRS.GetText(1, 3), tExeSQL,
								tSSRS.GetText(1, 5));
						if (tRiskName2 != "" && tRiskName2 != null) {
							tRiskName = tRiskName2;
						}
					} else {
						tSql = "Select  b.GrpName,RiskName,b.riskcode,b.CustomerNo,b.grpcontno from ljagetendorse a,lbgrppol b,lmriskapp c "
								+ "where actugetno in ( Select Btactuno from ljaedorbaldetail "
								+ "where actuno in ( Select getnoticeno from LJAPay where payno='"
								+ mSSRS.GetText(i, 2)
								+ "' )) "
								+ "and a.grpcontno=b.grpcontno and b.riskcode=c.riskcode and c.subriskflag='M' order by b.prem desc ";
						tSSRS = tExeSQL.execSQL(tSql);
						if (tSSRS.getMaxRow() > 0) {
							tRiskName = tSSRS.GetText(1, 2);
							tRiskName2 = getRiskName(tSSRS.GetText(1, 3),
									tExeSQL, tSSRS.GetText(1, 5));
							if (tRiskName2 != "" && tRiskName2 != null) {
								tRiskName = tRiskName2;
							}
						}

					}
				}
				if (mSSRS.GetText(i, 3).equals("15")) {
					tSql = "select distinct '',b.RiskName,b.riskcode,'' from lmcardrisk a,lmriskapp b,LZCardPay c,LMCertifyDes d where c.PayNo = '"
							+ mSSRS.GetText(i, 6)
							+ "' and a.riskcode=b.riskcode and b.subriskflag='M'  and d.CertifyCode=a.CertifyCode and d.subcode=c.cardtype "
							+ " order by  b.riskcode with ur";
					tSSRS = tExeSQL.execSQL(tSql);
					if (tSSRS.getMaxRow() > 0) {
						tRiskName = tSSRS.GetText(1, 2);
						tRiskName2 = getRiskName(tSSRS.GetText(1, 3), tExeSQL,
								"");
						if (tRiskName2 != "" && tRiskName2 != null) {
							tRiskName = tRiskName2;
						}
					}

				}
				write = write + tRiskName + "\t"; //险种
				write = write +  mSSRS.GetText(i, 5) + "\t\n";  //金额
				
			}
			fileName = "Invoice_" + PubFun.getCurrentDate2() + ".xls";
			FileWriter fw = new FileWriter(mBasePath + mOutPath + fileName);
			fw.write(write);
			fw.close();
			System.out.println("导入文件：" + mBasePath + mOutPath + fileName);
			mResult.add(mOutPath + fileName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		System.out.println("export xml finished...");
		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "FFIvoiceExportZjBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public List getResult() {
		return mResult;
	}

	public CErrors getErrors() {
		return mErrors;
	}

	/**
	 * 输入:险种编码 输出：险种套餐的套餐名 
	 */
	private String getRiskName(String riskCode, ExeSQL tExeSQL, String tcontno) {
		String riskName = "";
		String riskSql = "select riskwrapplanname from ldcode1 where codetype = 'bankcheckappendrisk' and code1 ='"
				+ riskCode
				+ "'  and code in(select riskwrapcode from lcriskwrap where grpcontno='"
				+ tcontno
				+ "' union select riskwrapcode from lcriskdutywrap where contno='"
				+ tcontno + "' fetch first 1 rows only) with ur";
		SSRS tSSRS = new SSRS();
		tSSRS = tExeSQL.execSQL(riskSql);
		if (tSSRS.getMaxRow() > 0) {
			riskName = tSSRS.GetText(1, 1);
		}
		return riskName;
	}

    public static void main(String[] args)
    {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86110000";
        tGlobalInput.Operator = "group";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("invoiceStartDate", "2014-1-22");
        tTransferData.setNameAndValue("invoiceEndDate", "2014-1-22");

        tTransferData.setNameAndValue("hostBasePath",
                "F:/");
        tTransferData.setNameAndValue("outPath", "printdata");

        VData tVDate = new VData();
        tVDate.add(tGlobalInput);
        tVDate.add(tTransferData);

        try
        {
            FFIvoiceExportQdBL tFFIvoiceExportHnBL = new FFIvoiceExportQdBL();
            if (!tFFIvoiceExportHnBL.submitData(tVDate, "EXPORT"))
            {
                System.out.println("failed...");
            }
            else
            {
                System.out.println("finished...");
            }
        }
        catch (Exception ex)
        {
            System.out.println("failed...");
        }
    }
}
