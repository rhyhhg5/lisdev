  package com.sinosoft.lis.finfee;

import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单查询业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class TempFeeTypeQueryBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  /** 业务处理相关变量 */
  /** 暂交费表 */
  private LCPolSet  mLCPolSet = new LCPolSet() ;
  private LCPolSchema  mLCPolSchema = new LCPolSchema() ;
  private LCGrpPolSet  mLCGrpPolSet = new LCGrpPolSet() ;
  private LCGrpPolSchema  mLCGrpPolSchema = new LCGrpPolSchema() ;

  public TempFeeTypeQueryBL() {}

  public static void main(String[] args) {
  }

  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
System.out.println("---getInputData---");

    //进行业务处理
    if (!queryType())
      return false;
System.out.println("---queryType---");
    if(!prepareOutputData())
      return false;

    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    // 检验查询条件
    mLCPolSchema=(LCPolSchema)cInputData.getObjectByObjectName("LCPolSchema",0);

    mLCGrpPolSchema=(LCGrpPolSchema)cInputData.getObjectByObjectName("LCGrpPolSchema",0);

    if(mLCPolSchema == null&&mLCGrpPolSchema == null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "TempFeeTypeQueryBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "请输入查询条件!";
      this.mErrors.addOneError(tError) ;
      return false;
    }
    return true;
  }


  /**
   * 查询暂交费表信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean queryType()
  {
  	// 保单信息
      String PolNo="";
      String ManageCom="";
      String MaxManageCom="";
      String MinManageCom="";
      String sqlStr="";
     if(mLCPolSchema!=null)
     {
       //查询个人保单表
       PolNo=mLCPolSchema.getPolNo();
       ManageCom=mLCPolSchema.getManageCom();
       MaxManageCom=PubFun.RCh(ManageCom,"9",8);
       MinManageCom=PubFun.RCh(ManageCom,"0",8);
       LCPolDB tLCPolDB = new LCPolDB();
       tLCPolDB.setSchema(mLCPolSchema);

       sqlStr="select * from LCPol where PolNo='"+PolNo+"' and AppFlag='1'";
       sqlStr=sqlStr+" and ManageCom>='"+MinManageCom+"' and ManageCom<='"+MaxManageCom+"'";
       //sqlStr=sqlStr+" and (StopFlag='0' or StopFlag is null)";
       //sqlStr=sqlStr+"and RiskCode in(select RiskCode from LMRiskPay where UrgePayFlag='N')";
System.out.println("个人sql:"+sqlStr);
       mLCPolSet = tLCPolDB.executeQuery(sqlStr);
       if (tLCPolDB.mErrors.needDealError() == true)
       {
       // @@错误处理
       this.mErrors.copyAllErrors(tLCPolDB.mErrors);
       CError tError = new CError();
       tError.moduleName = "TempFeeTypeQueryBL";
       tError.functionName = "queryType";
       tError.errorMessage = "个人保单表查询失败!";
       this.mErrors.addOneError(tError);
       mLCPolSet.clear();
       return false;
       }
       if (mLCPolSet.size() == 0)
       {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "TempFeeTypeQueryBL";
        tError.functionName = "queryType";
        tError.errorMessage = "未找到相关数据!";
        this.mErrors.addOneError(tError);
        mLCPolSet.clear();
        return false;
       }
       mLCPolSchema = new LCPolSchema();
       mLCPolSchema = mLCPolSet.get(1);
       mLCGrpPolSchema=null;
       return true;
     }
     else
     {
       //查询集体保单表
       PolNo=mLCGrpPolSchema.getGrpPolNo();
       ManageCom=mLCGrpPolSchema.getManageCom();
       MaxManageCom=PubFun.RCh(ManageCom,"9",8);
       MinManageCom=PubFun.RCh(ManageCom,"0",8);
       LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
       tLCGrpPolDB.setSchema(mLCGrpPolSchema);

       sqlStr="select * from LCGrpPol where GrpPolNo='"+PolNo+"' and AppFlag='1'";
       sqlStr=sqlStr+" and ManageCom>='"+MinManageCom+"' and ManageCom<='"+MaxManageCom+"'";
//       sqlStr=sqlStr+" and (StopFlag='0' or StopFlag is null)";
       //sqlStr=sqlStr+"and RiskCode in(select RiskCode from LMRiskPay where UrgePayFlag='N')";
System.out.println("集体sql:"+sqlStr);
       mLCGrpPolSet = tLCGrpPolDB.executeQuery(sqlStr);
       if (tLCGrpPolDB.mErrors.needDealError() == true)
       {
       // @@错误处理
       this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
       CError tError = new CError();
       tError.moduleName = "TempFeeTypeQueryBL";
       tError.functionName = "queryType";
       tError.errorMessage = "集体保单表查询失败!";
       this.mErrors.addOneError(tError);
       mLCGrpPolSet.clear();
       return false;
       }
       if (mLCGrpPolSet.size() == 0)
       {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "TempFeeTypeQueryBL";
        tError.functionName = "queryType";
        tError.errorMessage = "未找到相关数据!";
        this.mErrors.addOneError(tError);
        mLCGrpPolSet.clear();
        return false;
       }
       mLCGrpPolSchema = new LCGrpPolSchema();
       mLCGrpPolSchema = mLCGrpPolSet.get(1);
       mLCPolSchema=null;
       return true;
     }
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData()
  {
    mResult.clear();
    try
    {
      if(mLCPolSchema==null)
       mResult.add( mLCGrpPolSchema);
      else
       mResult.add( mLCPolSchema);
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="TempFeeTypeQueryBL";
      tError.functionName="prepareOutputData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
    }
}
