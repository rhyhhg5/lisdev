package com.sinosoft.lis.finfee;

import com.sinosoft.lis.bl.LJTempFeeClassBL;
import com.sinosoft.lis.db.LJABonusGetDB;
import com.sinosoft.lis.db.LJAGetClaimDB;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LJAGetDrawDB;
import com.sinosoft.lis.db.LJAGetEndorseDB;
import com.sinosoft.lis.db.LJAGetOtherDB;
import com.sinosoft.lis.db.LJAGetTempFeeDB;
import com.sinosoft.lis.db.LJFIGetDB;
import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LJABonusGetSchema;
import com.sinosoft.lis.schema.LJAGetClaimSchema;
import com.sinosoft.lis.schema.LJAGetDrawSchema;
import com.sinosoft.lis.schema.LJAGetEndorseSchema;
import com.sinosoft.lis.schema.LJAGetOtherSchema;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJAGetTempFeeSchema;
import com.sinosoft.lis.schema.LJFIGetSchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vschema.LJABonusGetSet;
import com.sinosoft.lis.vschema.LJAGetClaimSet;
import com.sinosoft.lis.vschema.LJAGetDrawSet;
import com.sinosoft.lis.vschema.LJAGetEndorseSet;
import com.sinosoft.lis.vschema.LJAGetOtherSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJAGetTempFeeSet;
import com.sinosoft.lis.vschema.LJFIGetSet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CodeJudge;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class TempFeeClassBL {
     //错误处理类，每个需要错误处理的类中都放置该类
      public CErrors mErrors = new CErrors(
          );

    /** 往后面传输数据的容器 */
    private String[] existTempFeeNo;
    private VData mInputData;
    private VData mLastFinData = new VData();
    private GlobalInput tGI = new GlobalInput();
    private VData FinFeeVData = new VData(); //存放财务付费数据

    /** 数据操作字符串 */
    private String serNo = ""; //流水号
    private String tLimit = "";
    private String tNo = ""; //生成的暂交费号
    private String mOperate;
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();

    private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
    private LJTempFeeClassSet mLJTempFeeClassSetNew = new LJTempFeeClassSet();
    private LJTempFeeClassSet mLJTempFeeClassSetDel = new LJTempFeeClassSet();


  //业务处理相关变量
    public TempFeeClassBL() {
    }

    public static void main(String[] args) {
  LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
  LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
  LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
  LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
  GlobalInput tGI = new GlobalInput();
  tGI.ComCode = "86110000";
  tGI.Operator = "001";
  tGI.ManageCom = "86110000";
  //    tLJTempFeeClassSchema = new LJTempFeeClassSchema();
  tLJTempFeeClassSchema   = new LJTempFeeClassSchema();
        tLJTempFeeClassSchema.setTempFeeNo("81000011844");
        tLJTempFeeClassSchema.setPayMode("11");
        tLJTempFeeClassSchema.setPayDate("2006-10-13");
        tLJTempFeeClassSchema.setPayMoney(22436552);
        tLJTempFeeClassSchema.setChequeNo("20061011");
        tLJTempFeeClassSchema.setChequeDate("");
        tLJTempFeeClassSchema.setEnterAccDate("");
        tLJTempFeeClassSchema.setManageCom("8653000");
        tLJTempFeeClassSchema.setBankCode("010101");
        tLJTempFeeClassSchema.setBankAccNo("");
        tLJTempFeeClassSchema.setAccName("");
        tLJTempFeeClassSchema.setInsBankCode("");
        tLJTempFeeClassSchema.setOperator("group");
  tLJTempFeeClassSet.add(tLJTempFeeClassSchema);

  tLJTempFeeClassSchema   = new LJTempFeeClassSchema();
    tLJTempFeeClassSchema.setTempFeeNo("81000011844");
    tLJTempFeeClassSchema.setPayMode("1");
    tLJTempFeeClassSchema.setPayDate("2006-10-13");
    tLJTempFeeClassSchema.setPayMoney(5638548);
    tLJTempFeeClassSchema.setChequeNo("");
    tLJTempFeeClassSchema.setChequeDate("");
    tLJTempFeeClassSchema.setEnterAccDate("");
    tLJTempFeeClassSchema.setManageCom("8653000");
    tLJTempFeeClassSchema.setBankCode("");
    tLJTempFeeClassSchema.setBankAccNo("");
    tLJTempFeeClassSchema.setAccName("");
    tLJTempFeeClassSchema.setInsBankCode("");
    tLJTempFeeClassSchema.setOperator("group");
    tLJTempFeeClassSet.add(tLJTempFeeClassSchema);


  VData tVData = new VData();
  tVData.addElement(tLJTempFeeClassSet);
  tVData.addElement(tGI);

  TempFeeClassBL tTempFeeClassBL = new TempFeeClassBL();

  tTempFeeClassBL.submitData(tVData, "INSERT");
}

//传输数据的公共方法
public boolean submitData(VData cInputData, String cOperate) {
  //将操作数据拷贝到本类中
  this.mOperate = cOperate;
  System.out.println("Operate==" + cOperate);
  //得到外部传入的数据,将数据备份到本类中
  if (!getInputData(cInputData)) {
    return false;
  }
  System.out.println("After getinputdata");

  if (!checkData()) {
    return false;
  }

  //进行业务处理
  if (!dealData()) {
    return false;
  }
  System.out.println("After dealData！");
  //准备往后台的数据
  if (!prepareOutputData()) {
    return false;
  }
  System.out.println("After prepareOutputData");

  System.out.println("Start TempFee BL Submit...");

  TempFeeClassBLS tTempFeeClassBLS = new TempFeeClassBLS();
  tTempFeeClassBLS.submitData(mInputData, cOperate);

  System.out.println("End LJTempFee BL Submit...");

  //如果有需要处理的错误，则返回
  if (tTempFeeClassBLS.mErrors.needDealError()) {
    this.mErrors.copyAllErrors(tTempFeeClassBLS.mErrors);
    for (int m = 0; m < tTempFeeClassBLS.mErrors.getErrorCount(); m++) {
      System.out.println(tTempFeeClassBLS.mErrors.getError(1).functionName);
      System.out.println(tTempFeeClassBLS.mErrors.getError(1).errorMessage);
    }
    mInputData = null;
    return false;
  }
  System.out.println("TempFeeBL end");
  mInputData = null;
  return true;
}


/**
 * creatActuPay
 *
 * @return boolean
 */
private boolean creatActuPay() {
  return true;
}

//根据前面的输入数据，进行逻辑处理
//如果在处理过程中出错，则返回false,否则返回true
private boolean dealData() {
  int i, iMax;
  boolean tReturn = false;
  String tempFeeFlag = "";
  String tEngageTempFeeNo = ""; // 记录预打报单交费收据号
  //产生流水号
  tLimit = PubFun.getNoLimit(tGI.ManageCom);

  if (this.mOperate.equals("INSERT")) {
    serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
  }
  boolean newFlag = false; //判断新单标记


//2-处理暂交费分类表，记录集，循环处理
    VData tempVData = new VData();
    LJTempFeeClassBL tLJTempFeeClassBL;
    iMax = mLJTempFeeClassSet.size();
    boolean existTempFeeClassFlag = false;

    for (i = 1; i <= iMax; i++) {
      existTempFeeClassFlag = false; //初始化为假
      tLJTempFeeClassBL = new LJTempFeeClassBL();
      tLJTempFeeClassBL.setSchema(mLJTempFeeClassSet.get(i).getSchema());

      //判断暂交费分类纪录是否也是重复的纪录，从数组中查找
      if (this.mOperate.equals("INSERT")) { //如果暂交费纪录有重复
          String strFlagSQL = "Select Count(1) From LJTempFeeClass where TempFeeNo = '"+tLJTempFeeClassBL.getTempFeeNo()+"' and Confdate is not null";
          ExeSQL mExeSQL = new ExeSQL();
          String strClassCount = mExeSQL.getOneValue(strFlagSQL);
          int intClassCount = Integer.parseInt(strClassCount);

          if (intClassCount>0) {
            existTempFeeClassFlag = true; //找到重复的暂交费分类纪录
          }

        if (existTempFeeClassFlag == true) {
          continue;
        }
      }

      tLJTempFeeClassBL.setSerialNo(serNo);
      if (this.mOperate.equals("INSERT")) {
        tLJTempFeeClassBL.setMakeDate(CurrentDate); //入机时间
        tLJTempFeeClassBL.setMakeTime(CurrentTime); //入机时间
      }

      //首期交费时，管理机构和交费机构相同
      if (tempFeeFlag.equals("1")) {
        tLJTempFeeClassBL.setPolicyCom(tLJTempFeeClassBL.
                                       getManageCom());
      }

      tLJTempFeeClassBL.setModifyDate(CurrentDate);
      tLJTempFeeClassBL.setModifyTime(CurrentTime);
      tLJTempFeeClassBL.setConfFlag("0"); //核销标志置0
      //* ↓ *** liuhao *** 2005-05-17 *** add *****
       //判断如果是预打保单交费帐单 设置核销标志为1
       if (tEngageTempFeeNo.indexOf(tLJTempFeeClassBL.getTempFeeNo()) != -1) {
         tLJTempFeeClassBL.setConfFlag("1"); //核销标志置1
       }
      //* ↑ *** liuhao *** 2005-05-17 *** add *****

       //如果号码类型不是交费收据号31，那么循环找到数组中对应的保单号，
       //(因为对于要产生暂交费号码的类型，其暂交费分类纪录的暂交费号存放的是保单号或印刷号)
       //所以和前面保存的保单号或者印刷号对比，如果匹配，将新生成的暂交费号赋予暂交费分类纪录的暂交费号
       if (! (tLJTempFeeClassBL.getTempFeeNo().length() == 20 &&
              CodeJudge.judgeCodeType(tLJTempFeeClassBL.getTempFeeNo(),
                                      "31")) &&
           this.mOperate.equals("INSERT")) {

       }

      //Modify by Minim，判断到帐日期，不为空则设置操作日期为当天
      if (tLJTempFeeClassBL.getEnterAccDate() != null &&
          !tLJTempFeeClassBL.getEnterAccDate().equals("")) {
        tLJTempFeeClassBL.setConfMakeDate(PubFun.getCurrentDate());
      }
//        }

      mLJTempFeeClassSetNew.add(tLJTempFeeClassBL);
      tReturn = true;
  }
  return tReturn;
}

/**
 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 *
 * @param mInputData VData
 * @return boolean
 */
private boolean getInputData(VData mInputData) {

  // 暂交费分类表
  mLJTempFeeClassSet.set( (LJTempFeeClassSet) mInputData.
                         getObjectByObjectName("LJTempFeeClassSet", 0));
  System.out.println("mLJTempFeeClassSet" +
                     mLJTempFeeClassSet.get(1).getEnterAccDate());
  // 公用变量
  tGI = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);

  if (mLJTempFeeClassSet == null || tGI == null) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "TempFeeBL";
    tError.functionName = "getInputData";
    tError.errorMessage = "没有得到足够的数据，请您确认!";
    this.mErrors.addOneError(tError);
    return false;
  }
  return true;
}

//准备往后层输出所需要的数据
//输出：如果准备数据时发生错误则返回false,否则返回true
private boolean prepareOutputData() {
  String arrPrtNo[] = null;
  String strAllPrtNo = "";
  mInputData = new VData();
  MMap tempMap = new MMap();

  try {
//2-处理暂交费分类表，记录集，循环处理
    mInputData.add(mLJTempFeeClassSetNew); //位置是1
//3-添加要删除的暂交费分类表
    mInputData.add(mLJTempFeeClassSetDel); //位置是2
//4-添加处理财务付费的数据
    mInputData.add(FinFeeVData); //位置是3
    mInputData.add(tGI); //位置是4
    mInputData.add(mLastFinData); //位置是5
  } catch (Exception ex) {
    // @@错误处理
    ex.printStackTrace();
    CError tError = new CError();
    tError.moduleName = "TempFeeBL";
    tError.functionName = "prepareData";
    tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
    System.out.println("在准备往后层处理所需要的数据时出错。");
    this.mErrors.addOneError(tError);
    return false;
  }
  return true;
}

/*
 *  查询暂交费表
 *  返回布尔值
 */
private boolean queryTempFee(LJTempFeeSchema pLJTempFeeSchema) {
  String TempFeeNo = pLJTempFeeSchema.getTempFeeNo();
  String RiskCode = pLJTempFeeSchema.getRiskCode();
  if (TempFeeNo == null) {
    CError tError = new CError();
    tError.moduleName = "TempFeeBL";
    tError.functionName = "queryTempFee";
    tError.errorMessage = "暂交费号不能为空!";
    this.mErrors.addOneError(tError);
    return false;
  }
  //查询凡是同一个收据号的纪录
  String sqlStr = "select * from LJTempFee where TempFeeNo='" + TempFeeNo +
      "'";
  //sqlStr=sqlStr+" and RiskCode='"+RiskCode+"'";
  System.out.println("查询暂交费表:" + sqlStr);
  LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
  LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
  LJTempFeeDB tLJTempFeeDB = tLJTempFeeSchema.getDB();
  tLJTempFeeSet = tLJTempFeeDB.executeQuery(sqlStr);
  if (tLJTempFeeDB.mErrors.needDealError() == true) {
    // @@错误处理
    this.mErrors.copyAllErrors(tLJTempFeeDB.mErrors);
    CError tError = new CError();
    tError.moduleName = "TempFeeBL";
    tError.functionName = "queryLJTempFee";
    tError.errorMessage = "暂交费表查询失败!";
    this.mErrors.addOneError(tError);
    tLJTempFeeSet.clear();
    return false;
  }
  if (tLJTempFeeSet.size() > 0) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "TempFeeBL";
    tError.functionName = "queryLJTempFee";
    tError.errorMessage = "暂交费号为：" + TempFeeNo + " 的纪录已经存在！";
    this.mErrors.addOneError(tError);
    return false;
  }
  return true;
}

/*
 *  查询暂交费分类表
 *  返回布尔值
 */
private LJTempFeeClassSchema queryLJTempFeeClass(LJTempFeeClassSchema
                                                 nLJTempFeeClassSchema) {
  String strSql = "select * from LJTempFeeClass where TempFeeNo='" +
      nLJTempFeeClassSchema.getTempFeeNo() + "'";
  strSql = strSql + " and PayMode='" + nLJTempFeeClassSchema.getPayMode() +
      "'";
  System.out.println("查询暂交费分类表:" + strSql);
  LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
  LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
  LJTempFeeClassDB tLJTempFeeClassDB = tLJTempFeeClassSchema.getDB();
  tLJTempFeeClassSet = tLJTempFeeClassDB.executeQuery(strSql);
  if (tLJTempFeeClassDB.mErrors.needDealError() == true) {
    // @@错误处理
    this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
    CError tError = new CError();
    tError.moduleName = "TempFeeBL";
    tError.functionName = "queryLJTempFeeClass";
    tError.errorMessage = "暂交费分类表查询失败!";
    this.mErrors.addOneError(tError);
    tLJTempFeeClassSet.clear();
    return null;
  }
  if (tLJTempFeeClassSet.size() == 0) {
    return null;
  }
  LJTempFeeClassSchema pLJTempFeeClassSchema = new LJTempFeeClassSchema();
  pLJTempFeeClassSchema = tLJTempFeeClassSet.get(1);
  return pLJTempFeeClassSchema;
}

/**
 * 处理财务付费流程
 *
 * @param pLJFIGetSchema 实付号码(票据号码)
 * @return com.sinosoft.utility.VData
 */
private VData DoFinFeePay(LJFIGetSchema pLJFIGetSchema) {
  if (pLJFIGetSchema == null) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "TempFeeBL";
    tError.functionName = "DoFinFeePay";
    tError.errorMessage = "传入参数不能为空!";
    this.mErrors.addOneError(tError);
    return null;
  }
  if (pLJFIGetSchema.getActuGetNo() == null || pLJFIGetSchema.getPayMode() == null ||
      pLJFIGetSchema.getEnterAccDate() == null) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "TempFeeBL";
    tError.functionName = "DoFinFeePay";
    tError.errorMessage = "实付号码（票据号码）,交费方式,到帐日期不能为空!";
    this.mErrors.addOneError(tError);
    return null;
  }
  VData tVData = new VData();
  //1-查询实付总表
  LJAGetSet tLJAGetSet = new LJAGetSet();
  LJAGetSchema tLJAGetSchema = new LJAGetSchema();
  tLJAGetSchema.setActuGetNo(pLJFIGetSchema.getActuGetNo());
  LJAGetDB tLJAGetDB = tLJAGetSchema.getDB();
  tLJAGetSet = tLJAGetDB.query();
  if (tLJAGetDB.mErrors.needDealError()) {
    // @@错误处理
    this.mErrors.copyAllErrors(tLJAGetDB.mErrors);
    CError tError = new CError();
    tError.moduleName = "TempFeeBL";
    tError.functionName = "DoFinFeePay";
    tError.errorMessage = "数据库查询失败!";
    this.mErrors.addOneError(tError);
    return null;
  }
  if (tLJAGetSet.size() == 0) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "TempFeeBL";
    tError.functionName = "DoFinFeePay";
    tError.errorMessage = "没有查询到票据号码对应的实付总表!票据号码:" +
        pLJFIGetSchema.getActuGetNo();
    this.mErrors.addOneError(tError);
    return null;
  }
  tLJAGetSchema = tLJAGetSet.get(1);
  //准备实付总表的更新纪录
  if (tLJAGetSchema.getSumGetMoney() != pLJFIGetSchema.getGetMoney()) {
    CError tError = new CError();
    tError.moduleName = "TempFeeBL";
    tError.functionName = "DoFinFeePay";
    tError.errorMessage = "作内部转账的退费金额与暂收金额不符";
    this.mErrors.addOneError(tError);
    return null;
  }
  if (!tLJAGetSchema.getPayMode().equals("5") &&
      tLJAGetSchema.getConfDate() != null) {
    CError tError = new CError();
    tError.moduleName = "TempFeeBL";
    tError.functionName = "DoFinFeePay";
    tError.errorMessage = "该退费已经给付确认";
    this.mErrors.addOneError(tError);
    return null;
  }
  //tLJAGetSchema.setSumGetMoney(tLJAGetSchema.getSumGetMoney()+pLJFIGetSchema.getGetMoney());
  tLJAGetSchema.setEnterAccDate(pLJFIGetSchema.getEnterAccDate());
  tLJAGetSchema.setConfDate(pLJFIGetSchema.getConfDate());
  tLJAGetSchema.setManageCom(tGI.ManageCom);
  tLJAGetSchema.setOperator(tGI.Operator);
  tLJAGetSchema.setModifyDate(CurrentDate);
  tLJAGetSchema.setModifyTime(CurrentTime);
  tLJAGetSchema.setSerialNo(pLJFIGetSchema.getSerialNo());
  tLJAGetSchema.setPayMode(pLJFIGetSchema.getPayMode());

  //2-查询财务给付表
  LJFIGetSet tLJFIGetSet = new LJFIGetSet();
  LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();
  tLJFIGetSchema.setActuGetNo(pLJFIGetSchema.getActuGetNo()); //注意是pLJFIGetSchema
  LJFIGetDB tLJFIGetDB = tLJFIGetSchema.getDB();
  tLJFIGetSet = tLJFIGetDB.query();
  if (tLJFIGetDB.mErrors.needDealError()) {
    // @@错误处理
    this.mErrors.copyAllErrors(tLJFIGetDB.mErrors);
    CError tError = new CError();
    tError.moduleName = "TempFeeBL";
    tError.functionName = "DoFinFeePay";
    tError.errorMessage = "数据库查询失败!";
    this.mErrors.addOneError(tError);
    return null;
  }
  if (tLJFIGetSet.size() > 0 && this.mOperate.equals("INSERT")) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "TempFeeBL";
    tError.functionName = "DoFinFeePay";
    tError.errorMessage = "已经存在实付号码为:" + pLJFIGetSchema.getActuGetNo() +
        ",交费方式为:" + pLJFIGetSchema.getPayMode() +
        "的财务给付纪录!";
    this.mErrors.addOneError(tError);
    return null;
  }
  //准备财务给付表纪录数据
  tLJFIGetSchema.setActuGetNo(pLJFIGetSchema.getActuGetNo());
  tLJFIGetSchema.setPayMode(pLJFIGetSchema.getPayMode());
  tLJFIGetSchema.setOtherNo(tLJAGetSchema.getOtherNo());
  tLJFIGetSchema.setOtherNoType(tLJAGetSchema.getOtherNoType());
  tLJFIGetSchema.setEnterAccDate(pLJFIGetSchema.getEnterAccDate());
  tLJFIGetSchema.setConfDate(CurrentDate);
  tLJFIGetSchema.setSaleChnl(tLJAGetSchema.getSaleChnl());
  tLJFIGetSchema.setManageCom(tGI.ManageCom);
  tLJFIGetSchema.setAgentCode(tLJAGetSchema.getAgentCode());
  tLJFIGetSchema.setAgentGroup(tLJAGetSchema.getAgentGroup());
  tLJFIGetSchema.setAgentCom(tLJAGetSchema.getAgentCom());
  tLJFIGetSchema.setSerialNo(pLJFIGetSchema.getSerialNo());
  tLJFIGetSchema.setMakeDate(CurrentDate);
  tLJFIGetSchema.setMakeTime(CurrentTime);
  tLJFIGetSchema.setModifyDate(CurrentDate);
  tLJFIGetSchema.setModifyTime(CurrentTime);
  tLJFIGetSchema.setOperator(tGI.Operator);
  tLJFIGetSchema.setGetMoney(pLJFIGetSchema.getGetMoney());

  //3-准备子表更新数据
  String OtherNoType = tLJAGetSchema.getOtherNoType();
  String flag = "";
  LJABonusGetSet mLJABonusGetSet = new LJABonusGetSet();
  LJAGetDrawSet mLJAGetDrawSet = new LJAGetDrawSet();
  LJAGetEndorseSet mLJAGetEndorseSet = new LJAGetEndorseSet();
  LJAGetTempFeeSet mLJAGetTempFeeSet = new LJAGetTempFeeSet();
  LJAGetClaimSet mLJAGetClaimSet = new LJAGetClaimSet();
  LJAGetOtherSet mLJAGetOtherSet = new LJAGetOtherSet();

  //更新 给付表(生存领取_实付)   LJAGetDraw
  if (OtherNoType.equals("1") || OtherNoType.equals("2") ||
      OtherNoType.equals("0")) {
    mLJAGetDrawSet = updateLJAGetDraw(tLJAGetSchema);
    if (mLJAGetDrawSet == null) {
      return null;
    } else {
      flag = "1";
    }
  }
  if (OtherNoType.equals("3")) { //更新 批改补退费表(实收/实付) LJAGetEndorse
    mLJAGetEndorseSet = updateLJAGetEndorse(tLJAGetSchema);
    if (mLJAGetEndorseSet == null) {
      return null;
    } else {
      flag = "3";
    }
  }

  if (OtherNoType.equals("4")) { //更新 暂交费退费实付表
    mLJAGetTempFeeSet = updateLJAGetTempFee(tLJAGetSchema);
    if (mLJAGetTempFeeSet == null) {
      return null;
    } else {
      flag = "4";
    }
  }

  if (OtherNoType.equals("5")) { //更新 赔付实付表             LJAGetClaim
    mLJAGetClaimSet = updateLJAGetClaim(tLJAGetSchema);
    if (mLJAGetClaimSet == null) {
      return null;
    } else {
      flag = "5";
    }
  }

  if (OtherNoType.equals("6") || OtherNoType.equals("8")) { //更新 其他退费实付表         LJAGetOther
    mLJAGetOtherSet = updateLJAGetOther(tLJAGetSchema);
    if (mLJAGetOtherSet == null) {
      return null;
    } else {
      flag = "6";
    }
  }

  if (OtherNoType.equals("7")) { //更新 红利给付实付表         LJABonusGet
    mLJABonusGetSet = updateLJABonusGet(tLJAGetSchema);
    if (mLJABonusGetSet == null) {
      return null;
    } else {
      flag = "7";
    }
  }

  tVData.add(tLJFIGetSchema); //1-财务给付表
  tVData.add(tLJAGetSchema); //2-实付总表，单项纪录
  if (flag.equals("1")) {
    tVData.add(mLJAGetDrawSet); //3-检验标志位
  }
  if (flag.equals("3")) {
    tVData.add(mLJAGetEndorseSet);
  }
  if (flag.equals("4")) {
    tVData.add(mLJAGetTempFeeSet);
  }
  if (flag.equals("5")) {
    tVData.add(mLJAGetClaimSet);
  }
  if (flag.equals("6")) {
    tVData.add(mLJAGetOtherSet);
  }
  if (flag.equals("7")) {
    tVData.add(mLJABonusGetSet);
  }
  tVData.add(flag); //4-加上标志位
  return tVData;
}

/**
 * 查询给付表(生存领取_实付)
 *
 * @param mLJAGetSchema LJAGetSchema
 * @return com.sinosoft.lis.vschema.LJAGetDrawSet
 */
private LJAGetDrawSet updateLJAGetDraw(LJAGetSchema mLJAGetSchema) {
  if (mLJAGetSchema == null) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "OperFinFeeGetBL";
    tError.functionName = "updateLJAGetDraw";
    tError.errorMessage = "传入参数不能为空！";
    this.mErrors.addOneError(tError);
    return null;
  }
  String sqlStr = "select * from LJAGetDraw where ActuGetNo='" +
      mLJAGetSchema.getActuGetNo() + "'";
  System.out.println("查询给付表(生存领取_实付):" + sqlStr);
  LJAGetDrawSet tLJAGetDrawSet = new LJAGetDrawSet();
  LJAGetDrawSchema tLJAGetDrawSchema = new LJAGetDrawSchema();
  LJAGetDrawDB tLJAGetDrawDB = tLJAGetDrawSchema.getDB();
  tLJAGetDrawSet = tLJAGetDrawDB.executeQuery(sqlStr);
  if (tLJAGetDrawDB.mErrors.needDealError() == true) {
    // @@错误处理
    this.mErrors.copyAllErrors(tLJAGetDrawDB.mErrors);
    CError tError = new CError();
    tError.moduleName = "OperFinFeeGetBL";
    tError.functionName = "queryLJAGetDraw";
    tError.errorMessage = "给付表(生存领取_实付)查询失败!";
    this.mErrors.addOneError(tError);
    tLJAGetDrawSet.clear();
    return null;
  }
  LJAGetDrawSet newLJAGetDrawSet = new LJAGetDrawSet();
  for (int n = 1; n <= tLJAGetDrawSet.size(); n++) {
    tLJAGetDrawSchema = new LJAGetDrawSchema();
    tLJAGetDrawSchema = tLJAGetDrawSet.get(n);
    tLJAGetDrawSchema.setConfDate(mLJAGetSchema.getConfDate());
    tLJAGetDrawSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
    tLJAGetDrawSchema.setModifyDate(CurrentDate);
    tLJAGetDrawSchema.setModifyTime(CurrentTime);
    tLJAGetDrawSchema.setSerialNo(mLJAGetSchema.getSerialNo());
    newLJAGetDrawSet.add(tLJAGetDrawSchema);
  }
  if (newLJAGetDrawSet.size() == 0) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "OperFinFeeGetBL";
    tError.functionName = "updateLJAGetDraw";
    tError.errorMessage = "没有可以更新的给付表(生存领取_实付)！";
    this.mErrors.addOneError(tError);
    return null;
  }
  return newLJAGetDrawSet;
}

/**
 * 查询批改补退费表(实收/实付)
 *
 * @param mLJAGetSchema LJAGetSchema
 * @return com.sinosoft.lis.vschema.LJAGetEndorseSet
 */
private LJAGetEndorseSet updateLJAGetEndorse(LJAGetSchema mLJAGetSchema) {
  if (mLJAGetSchema == null) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "OperFinFeeGetBL";
    tError.functionName = "updateLJAGetEndorse";
    tError.errorMessage = "传入参数不能为空！";
    this.mErrors.addOneError(tError);
    return null;
  }
  String sqlStr = "select * from LJAGetEndorse where ActuGetNo='" +
      mLJAGetSchema.getActuGetNo() + "'";
  System.out.println("查询给付表(生存领取_实付):" + sqlStr);
  LJAGetEndorseSet tLJAGetEndorseSet = new LJAGetEndorseSet();
  LJAGetEndorseSchema tLJAGetEndorseSchema = new LJAGetEndorseSchema();
  LJAGetEndorseDB tLJAGetEndorseDB = tLJAGetEndorseSchema.getDB();
  tLJAGetEndorseSet = tLJAGetEndorseDB.executeQuery(sqlStr);
  if (tLJAGetEndorseDB.mErrors.needDealError() == true) {
    // @@错误处理
    this.mErrors.copyAllErrors(tLJAGetEndorseDB.mErrors);
    CError tError = new CError();
    tError.moduleName = "OperFinFeeGetBL";
    tError.functionName = "updateLJAGetEndorse";
    tError.errorMessage = "批改补退费表(实收/实付)查询失败!";
    this.mErrors.addOneError(tError);
    tLJAGetEndorseSet.clear();
    return null;
  }
  LJAGetEndorseSet newLJAGetEndorseSet = new LJAGetEndorseSet();
  for (int n = 1; n <= tLJAGetEndorseSet.size(); n++) {
    tLJAGetEndorseSchema = new LJAGetEndorseSchema();
    tLJAGetEndorseSchema = tLJAGetEndorseSet.get(n);
    tLJAGetEndorseSchema.setGetConfirmDate(mLJAGetSchema.getConfDate());
    tLJAGetEndorseSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
    tLJAGetEndorseSchema.setModifyDate(CurrentDate);
    tLJAGetEndorseSchema.setModifyTime(CurrentTime);
    tLJAGetEndorseSchema.setSerialNo(mLJAGetSchema.getSerialNo());
    newLJAGetEndorseSet.add(tLJAGetEndorseSchema);
  }
  if (newLJAGetEndorseSet.size() == 0) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "OperFinFeeGetBL";
    tError.functionName = "updateLJAGetEndorse";
    tError.errorMessage = "没有可以更新的批改补退费表(实收/实付)！";
    this.mErrors.addOneError(tError);
    return null;
  }
  return newLJAGetEndorseSet;

}

/**
 * 查询其他退费实付表
 *
 * @param mLJAGetSchema LJAGetSchema
 * @return com.sinosoft.lis.vschema.LJAGetOtherSet
 */
private LJAGetOtherSet updateLJAGetOther(LJAGetSchema mLJAGetSchema) {
  if (mLJAGetSchema == null) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "OperFinFeeGetBL";
    tError.functionName = "updateLJAGetOther";
    tError.errorMessage = "传入参数不能为空！";
    this.mErrors.addOneError(tError);
    return null;
  }
  String sqlStr = "select * from LJAGetOther where ActuGetNo='" +
      mLJAGetSchema.getActuGetNo() + "'";
  System.out.println("其他退费实付表:" + sqlStr);
  LJAGetOtherSet tLJAGetOtherSet = new LJAGetOtherSet();
  LJAGetOtherSchema tLJAGetOtherSchema = new LJAGetOtherSchema();
  LJAGetOtherDB tLJAGetOtherDB = tLJAGetOtherSchema.getDB();
  tLJAGetOtherSet = tLJAGetOtherDB.executeQuery(sqlStr);
  if (tLJAGetOtherDB.mErrors.needDealError() == true) {
    // @@错误处理
    this.mErrors.copyAllErrors(tLJAGetOtherDB.mErrors);
    CError tError = new CError();
    tError.moduleName = "OperFinFeeGetBL";
    tError.functionName = "updateLJAGetOther";
    tError.errorMessage = "其他退费实付表查询失败!";
    this.mErrors.addOneError(tError);
    tLJAGetOtherSet.clear();
    return null;
  }
  LJAGetOtherSet newLJAGetOtherSet = new LJAGetOtherSet();
  for (int n = 1; n <= tLJAGetOtherSet.size(); n++) {
    tLJAGetOtherSchema = new LJAGetOtherSchema();
    tLJAGetOtherSchema = tLJAGetOtherSet.get(n);
    tLJAGetOtherSchema.setConfDate(mLJAGetSchema.getConfDate());
    tLJAGetOtherSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
    tLJAGetOtherSchema.setModifyDate(CurrentDate);
    tLJAGetOtherSchema.setModifyTime(CurrentTime);
    tLJAGetOtherSchema.setSerialNo(mLJAGetSchema.getSerialNo());
    newLJAGetOtherSet.add(tLJAGetOtherSchema);
  }
  if (newLJAGetOtherSet.size() == 0) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "OperFinFeeGetBL";
    tError.functionName = "updateLJAGetOther";
    tError.errorMessage = "没有可以更新的其他退费实付表！";
    this.mErrors.addOneError(tError);
    return null;
  }
  return newLJAGetOtherSet;

}

/**
 * 赔付实付表
 *
 * @param mLJAGetSchema LJAGetSchema
 * @return com.sinosoft.lis.vschema.LJAGetClaimSet
 */
private LJAGetClaimSet updateLJAGetClaim(LJAGetSchema mLJAGetSchema) {
  if (mLJAGetSchema == null) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "OperFinFeeGetBL";
    tError.functionName = "updateLJAGetClaim";
    tError.errorMessage = "传入参数不能为空！";
    this.mErrors.addOneError(tError);
    return null;
  }
  String sqlStr = "select * from LJAGetClaim where ActuGetNo='" +
      mLJAGetSchema.getActuGetNo() + "'";
  System.out.println("赔付实付表:" + sqlStr);
  LJAGetClaimSet tLJAGetClaimSet = new LJAGetClaimSet();
  LJAGetClaimSchema tLJAGetClaimSchema = new LJAGetClaimSchema();
  LJAGetClaimDB tLJAGetClaimDB = tLJAGetClaimSchema.getDB();
  tLJAGetClaimSet = tLJAGetClaimDB.executeQuery(sqlStr);
  if (tLJAGetClaimDB.mErrors.needDealError() == true) {
    // @@错误处理
    this.mErrors.copyAllErrors(tLJAGetClaimDB.mErrors);
    CError tError = new CError();
    tError.moduleName = "OperFinFeeGetBL";
    tError.functionName = "updateLJAGetClaim";
    tError.errorMessage = "赔付实付表查询失败!";
    this.mErrors.addOneError(tError);
    tLJAGetClaimSet.clear();
    return null;
  }
  LJAGetClaimSet newLJAGetClaimSet = new LJAGetClaimSet();
  for (int n = 1; n <= tLJAGetClaimSet.size(); n++) {
    tLJAGetClaimSchema = new LJAGetClaimSchema();
    tLJAGetClaimSchema = tLJAGetClaimSet.get(n);
    tLJAGetClaimSchema.setConfDate(mLJAGetSchema.getConfDate());
    tLJAGetClaimSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
    tLJAGetClaimSchema.setModifyDate(CurrentDate);
    tLJAGetClaimSchema.setModifyTime(CurrentTime);
    tLJAGetClaimSchema.setSerialNo(mLJAGetSchema.getSerialNo());
    newLJAGetClaimSet.add(tLJAGetClaimSchema);
  }
  if (newLJAGetClaimSet.size() == 0) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "OperFinFeeGetBL";
    tError.functionName = "updateLJAGetClaim";
    tError.errorMessage = "没有可以更新的赔付实付表！";
    this.mErrors.addOneError(tError);
    return null;
  }
  return newLJAGetClaimSet;

}

/**
 * 暂交费退费实付表
 *
 * @param mLJAGetSchema LJAGetSchema
 * @return com.sinosoft.lis.vschema.LJAGetTempFeeSet
 */
private LJAGetTempFeeSet updateLJAGetTempFee(LJAGetSchema mLJAGetSchema) {
  if (mLJAGetSchema == null) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "OperFinFeeGetBL";
    tError.functionName = "updateLJAGetTempFee";
    tError.errorMessage = "传入参数不能为空！";
    this.mErrors.addOneError(tError);
    return null;
  }
  String sqlStr = "select * from LJAGetTempFee where ActuGetNo='" +
      mLJAGetSchema.getActuGetNo() + "'";
  System.out.println("暂交费退费实付表:" + sqlStr);
  LJAGetTempFeeSet tLJAGetTempFeeSet = new LJAGetTempFeeSet();
  LJAGetTempFeeSchema tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
  LJAGetTempFeeDB tLJAGetTempFeeDB = tLJAGetTempFeeSchema.getDB();
  tLJAGetTempFeeSet = tLJAGetTempFeeDB.executeQuery(sqlStr);
  if (tLJAGetTempFeeDB.mErrors.needDealError() == true) {
    // @@错误处理
    this.mErrors.copyAllErrors(tLJAGetTempFeeDB.mErrors);
    CError tError = new CError();
    tError.moduleName = "OperFinFeeGetBL";
    tError.functionName = "updateLJAGetTempFee";
    tError.errorMessage = "暂交费退费实付表查询失败!";
    this.mErrors.addOneError(tError);
    tLJAGetTempFeeSet.clear();
    return null;
  }
  LJAGetTempFeeSet newLJAGetTempFeeSet = new LJAGetTempFeeSet();
  for (int n = 1; n <= tLJAGetTempFeeSet.size(); n++) {
    tLJAGetTempFeeSchema = new LJAGetTempFeeSchema();
    tLJAGetTempFeeSchema = tLJAGetTempFeeSet.get(n);
    tLJAGetTempFeeSchema.setConfDate(mLJAGetSchema.getConfDate());
    tLJAGetTempFeeSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
    tLJAGetTempFeeSchema.setModifyDate(CurrentDate);
    tLJAGetTempFeeSchema.setModifyTime(CurrentTime);
    tLJAGetTempFeeSchema.setSerialNo(mLJAGetSchema.getSerialNo());
    newLJAGetTempFeeSet.add(tLJAGetTempFeeSchema);
  }
  if (newLJAGetTempFeeSet.size() == 0) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "OperFinFeeGetBL";
    tError.functionName = "updateLJAGetTempFee";
    tError.errorMessage = "没有可以更新的暂交费退费实付表！";
    this.mErrors.addOneError(tError);
    return null;
  }
  return newLJAGetTempFeeSet;

}

/**
 * 红利给付实付表
 *
 * @param mLJAGetSchema LJAGetSchema
 * @return com.sinosoft.lis.vschema.LJABonusGetSet
 */
private LJABonusGetSet updateLJABonusGet(LJAGetSchema mLJAGetSchema) {
  if (mLJAGetSchema == null) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "OperFinFeeGetBL";
    tError.functionName = "updateLJABonusGet";
    tError.errorMessage = "传入参数不能为空！";
    this.mErrors.addOneError(tError);
    return null;
  }
  String sqlStr = "select * from LJABonusGet where ActuGetNo='" +
      mLJAGetSchema.getActuGetNo() + "'";
  System.out.println("红利给付实付表:" + sqlStr);
  LJABonusGetSet tLJABonusGetSet = new LJABonusGetSet();
  LJABonusGetSchema tLJABonusGetSchema = new LJABonusGetSchema();
  LJABonusGetDB tLJABonusGetDB = tLJABonusGetSchema.getDB();
  tLJABonusGetSet = tLJABonusGetDB.executeQuery(sqlStr);
  if (tLJABonusGetDB.mErrors.needDealError() == true) {
    // @@错误处理
    this.mErrors.copyAllErrors(tLJABonusGetDB.mErrors);
    CError tError = new CError();
    tError.moduleName = "OperFinFeeGetBL";
    tError.functionName = "updateLJABonusGet";
    tError.errorMessage = "红利给付实付表查询失败!";
    this.mErrors.addOneError(tError);
    tLJABonusGetSet.clear();
    return null;
  }
  LJABonusGetSet newLJABonusGetSet = new LJABonusGetSet();
  for (int n = 1; n <= tLJABonusGetSet.size(); n++) {
    tLJABonusGetSchema = new LJABonusGetSchema();
    tLJABonusGetSchema = tLJABonusGetSet.get(n);
    tLJABonusGetSchema.setConfDate(mLJAGetSchema.getConfDate());
    tLJABonusGetSchema.setEnterAccDate(mLJAGetSchema.getEnterAccDate());
    tLJABonusGetSchema.setModifyDate(CurrentDate);
    tLJABonusGetSchema.setModifyTime(CurrentTime);
    tLJABonusGetSchema.setSerialNo(mLJAGetSchema.getSerialNo());
    newLJABonusGetSet.add(tLJABonusGetSchema);
  }
  if (newLJABonusGetSet.size() == 0) {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "OperFinFeeGetBL";
    tError.functionName = "updateLJABonusGet";
    tError.errorMessage = "没有可以更新的红利给付实付表！";
    this.mErrors.addOneError(tError);
    return null;
  }
  return newLJABonusGetSet;

}

public String[] getResult() {
  return existTempFeeNo;
}

/**
 * 检验
 *
 * @return boolean
 */
public boolean checkData() { //设置TempFeeClass中的到账日期

  String currentTempFeeNo = "";
  String tMaxEnterAccDate = "";
  /**zhangjun*****2006-3-21***modify**/
  //如果是现金交费或者银行代收-那么存入当天系统时间,如果是支票，则置空
  /*
       PayMode     Mean
     1	现金     *
     2	现金支票
     3	转账支票（支票）*
     4	银行转账
     5	内部转帐
     6	银行托收
     7	其他
     8        赠送保险  *
     10       应收保费  *
     11       银行代收  *
   */
  for (int k = 1; k <= mLJTempFeeClassSet.size(); k++) {
    /**zhangjun*****2006-3-21***modify**/
    if (mLJTempFeeClassSet.get(k).getPayMode().equals("1") ||
        mLJTempFeeClassSet.get(k).getPayMode().equals("11") ||
        mLJTempFeeClassSet.get(k).getPayMode().equals("10") ||
        mLJTempFeeClassSet.get(k).getPayMode().equals("13")) {
      mLJTempFeeClassSet.get(k).setEnterAccDate(PubFun.getCurrentDate());
    } else if (mLJTempFeeClassSet.get(k).getPayMode().equals("2") ||
               mLJTempFeeClassSet.get(k).getPayMode().equals("3")) {
      mLJTempFeeClassSet.get(k).setEnterAccDate("");
    }
    if (mLJTempFeeClassSet.get(k).getPayMode().equals("6") &&
        (mLJTempFeeClassSet.get(k).getBankCode() == null ||
         mLJTempFeeClassSet.get(k).getBankCode().length() == 0)) {
      CError tError = new CError();
      tError.moduleName = "ProposalBL";
      tError.functionName = "checkData";
      tError.errorMessage = "请录入托收银行代码!";
      this.mErrors.addOneError(tError);
      return false;
    }
  }

  for (int n = 1; n <= mLJTempFeeClassSet.size(); n++) {
    tMaxEnterAccDate = "1900-1-1";
    currentTempFeeNo = mLJTempFeeClassSet.get(n).getTempFeeNo();

    //把分类表中同一暂交费的纪录查出，如果有一项到帐日期为空，则tMaxEnterAccDate置空。否则取最大的到帐日期
    for (int m = 1; m <= mLJTempFeeClassSet.size(); m++) {
      if (mLJTempFeeClassSet.get(m).getTempFeeNo().equals(
          currentTempFeeNo)) {
        if (mLJTempFeeClassSet.get(m).getEnterAccDate() == null ||
            mLJTempFeeClassSet.get(m).getEnterAccDate().equals("")) {
          tMaxEnterAccDate = "";
          break;
        } else {
          //  起始日期，终止日期 ,比较单位 (格式："YYYY-MM-DD")
          int t = PubFun.calInterval(tMaxEnterAccDate,
                                     mLJTempFeeClassSet.get(m).
                                     getEnterAccDate(),
                                     "D");
          if (t > 0) {
            tMaxEnterAccDate = mLJTempFeeClassSet.get(m).
                getEnterAccDate();
          }
        }
      }
    }

  }

  return true;
}


// * ↑ *** add *** liuhao *** 2005-05-17 *****
/**
 * 出错处理
 * @param szFunc String
 * @param szErrMsg String
 */
private void buildError(String szFunc, String szErrMsg) {
  CError cError = new CError();
  cError.moduleName = "TempFeeBL";
  cError.functionName = szFunc;
  cError.errorMessage = szErrMsg;
  this.mErrors.addOneError(cError);
}

}
