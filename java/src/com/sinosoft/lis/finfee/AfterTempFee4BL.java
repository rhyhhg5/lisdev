package com.sinosoft.lis.finfee;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LJTempFeeDB;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 *
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class AfterTempFee4BL implements AfterTempFeeBL
{
	public CErrors mErrors = new CErrors();
    private LJTempFeeSchema mLJTempFeeSchema = null;
    private GlobalInput mGlobalInput = null; //操作员信息

    private MMap map = new MMap();

    public AfterTempFee4BL()
    {
    }

    public MMap getSubmitMMap(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData()
    {
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setGetNoticeNo(mLJTempFeeSchema.getTempFeeNo());
        if(!tLJSPayDB.getInfo())
        {
            CError tError = new CError();
            tError.moduleName = "AfterTempFee4BL";
            tError.functionName = "dealData";
            tError.errorMessage = "没有查询到应收号为"
                                  + mLJTempFeeSchema.getTempFeeNo()
                                  + "的应收数据";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        String otherNoType = tLJSPayDB.getOtherNoType();

        //定期结算，清除由本结算设置的保单暂停状态
        if("13".equals(otherNoType))
        {
            String sql = "select GrpContNo from LPGrpEdorItem "
                         + "where EdorNo = '" + tLJSPayDB.getOtherNo() + "' ";
            String grpContNo = new ExeSQL().getOneValue(sql);

//            LCGrpContState

            String[] tables =
                              {"LCGrpCont ", "LCGrpPol ", "LCCont ", "LCPol "};
            for(int i = 0; i < tables.length; i++)
            {
                sql = "update " + tables[i]
                      + "set StateFlag = '" + BQ.STATE_FLAG_SIGN + "', "
                      + "   ModifyDate = '" + PubFun.getCurrentDate() + "', "
                      + "   ModifyTime = '" + PubFun.getCurrentTime() + "' "
                      + "where GrpContNo = '" + grpContNo + "' "
                      + "   and StateFlag = '" + BQ.STATE_FLAG_AVAILABLE + "' ";
                map.put(sql, SysConst.UPDATE);
            }

            sql = "update LCGrpContState "
                  + "set EndDate = StartDate, "
                  + "   Operator = '" + mGlobalInput.Operator + "', "
                  + "   ModifyDate = '" + PubFun.getCurrentDate() + "', "
                  + "   ModifyTime = '" + PubFun.getCurrentTime() + "' "
                  + "where GrpContNo = '" + grpContNo + "' "
                  + "   and OtherNo = '" + tLJSPayDB.getGetNoticeNo() + "' ";
            map.put(sql, SysConst.UPDATE);
        }


        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData data)
    {
        mLJTempFeeSchema = (LJTempFeeSchema) data
                           .getObjectByObjectName("LJTempFeeSchema", 0);
        mGlobalInput = (GlobalInput) data
                       .getObjectByObjectName("GlobalInput", 0);
        if(mLJTempFeeSchema == null || mGlobalInput == null)
        {
            return false;
        }

        System.out.println(PubFun.getCurrentDate() + " "
            + PubFun.getCurrentTime() + ":" + mGlobalInput.Operator
            + " AfterTempFee4BL");

        return true;
    }

    public static void main(String[] args)
    {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";
        g.ManageCom = g.ComCode;

        LJTempFeeDB db = new LJTempFeeDB();
        db.setTempFeeNo("31000002443");

        VData d = new VData();
        d.add(g);
        d.add(db.query().get(1));

        AfterTempFee4BL bl = new AfterTempFee4BL();
        MMap map = bl.getSubmitMMap(d, "");
        if(map == null)
        {
            System.out.println(bl.mErrors.getErrContent());
        }

        d.clear();
        d.add(map);
        PubSubmit p = new PubSubmit();
        if(!p.submitData(d, ""))
        {
            System.out.println(p.mErrors.getErrContent());
        }
    }
}
