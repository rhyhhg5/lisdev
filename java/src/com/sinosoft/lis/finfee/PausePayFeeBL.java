package com.sinosoft.lis.finfee;

import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: web财务系统</p>
 *
 * <p>Description: 应收总表查询和暂停给付处理</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: Sinosoft</p>
 *
 * @author 张君
 * @version 1.0
 */
public class PausePayFeeBL {
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
     public CErrors mErrors = new CErrors();

     /** 数据操作字符串 */
     private String mOperate;
     private String strSql;
     /** 业务处理相关变量 */
     /** 财务应付总表 */
     private LJSPaySet mLJSPaySet = new LJSPaySet();
     private LJSPaySchema mLJSPaySchema = new LJSPaySchema();

    public PausePayFeeBL() {
    }
    public static void main(String[] args)
        {
            GlobalInput tGlobalInput = new GlobalInput();

            tGlobalInput.ComCode = "86";
            tGlobalInput.ManageCom = "86";
            tGlobalInput.Operator = "lands";

            LJSPaySchema tLJSPaySchema = new LJSPaySchema();

            tLJSPaySchema.setGetNoticeNo("17700000050800");
           // tLJSPaySchema.setOtherNoType("4");
           // tLJSPaySchema.setBankOnTheWayFlag("2");
            VData vData = new VData();

            vData.add(tGlobalInput);
            vData.add(tLJSPaySchema);

            PausePayFeeBL tPausePayFeeBL = new PausePayFeeBL();

            tPausePayFeeBL.submitData(vData, "Pause");

            System.out.println(tPausePayFeeBL.mErrors.getFirstError());

        }


 /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
 public boolean submitData(VData cInputData, String cOperate) {
   //将操作数据拷贝到本类中
   this.mOperate = cOperate;

   //得到外部传入的数据,将数据备份到本类中
   if (!getInputData(cInputData))
     return false;

   //进行业务处理，查询到数据返回错误
   if (this.mOperate.equals("Query"))
   {
     if (!queryLJSPay())
         return false;
   }
   if (this.mOperate.equals("Pause"))
   {
       if (!setPauseFlag())
           return false;
   }
   if (this.mOperate.equals("Cancel"))
   {
       if (!cancelPauseFlag())
           return false;
   }
   return true;
 }

 /**
  * 从输入数据中得到所有对象
  *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
  */
 private boolean getInputData(VData cInputData) {
   // 检验查询条件
   mLJSPaySchema.setSchema((LJSPaySchema) cInputData.getObjectByObjectName(
       "LJSPaySchema", 0));

   if (mLJSPaySchema == null) {
     // @@错误处理
     CError tError = new CError();
     tError.moduleName = "PausePayFeeBL";
     tError.functionName = "getInputData";
     tError.errorMessage = "请输入查询条件!";
     this.mErrors.addOneError(tError);
     return false;
   }
   return true;
 }

 /**
  * 查询财务给付表信息
  * 输出：如果发生错误则返回false,否则返回true
  */
 private boolean queryLJSPay() {
   // 财务给付表信息
   strSql = "";
   LJSPayDB tLJSPayDB = new LJSPayDB();
   tLJSPayDB.setSchema(mLJSPaySchema);

   String tManageCom = mLJSPaySchema.getManageCom();
   String tPayDate = mLJSPaySchema.getPayDate();
   String tAgentCode = mLJSPaySchema.getAgentCode();
   String tBankCode = mLJSPaySchema.getBankCode();
   String tOtherNoType = mLJSPaySchema.getOtherNoType();
   String tBankOnTheWayFlag = mLJSPaySchema.getBankOnTheWayFlag();

          strSql = "select * from LJSPay ";
          switch (Integer.parseInt(tOtherNoType)) {
        case 1:
            strSql += "where OtherNoType in ('4','5','6','7','8','9') ";
            break;
        case 2:
            strSql += "where OtherNoType in ('1','2') ";
            break;
        case 3:
            strSql += "where OtherNoType in ('3','10') ";
            break;
        case 4:
            strSql += "where BankCode <> ''";
            break;
        default:
            break;
        }

  if (tBankOnTheWayFlag != null)
      if(!tBankOnTheWayFlag.equals(""))
          strSql += "and BankOnTheWayFlag = '"+tBankOnTheWayFlag+"'";
   if (tManageCom != null)
       if (!tManageCom.equals(""))
          strSql +="and ManageCom = '"+tManageCom+"'";
   if (tPayDate != null)
       if(!tPayDate.equals(""))
          strSql +="and PayDate = '"+tPayDate+"'";
   if (tAgentCode != null)
       if (!tAgentCode.equals(""))
          strSql +="and AgentCode = '"+tAgentCode+"'";
   if (tBankCode != null)
        if(!tBankCode.equals(""))
          strSql +="and BankCode = '"+tBankCode+"'";
   System.out.println(strSql);

   LJSPaySet tLJSPaySet = new LJSPayDB().executeQuery(strSql);

   if (tLJSPayDB.mErrors.needDealError() == true) {
     // @@错误处理
     this.mErrors.copyAllErrors(tLJSPayDB.mErrors);
     CError tError = new CError();
     tError.moduleName = "PausePayFeeBL";
     tError.functionName = "queryData";
     tError.errorMessage = "应付总表查询失败!";
     this.mErrors.addOneError(tError);
     mLJSPaySet.clear();
     return false;
   }
   if (tLJSPaySet.size() == 0) {
     // @@错误处理
     CError tError = new CError();
     tError.moduleName = "PausePayFeeBL";
     tError.functionName = "queryData";
     if (!tBankOnTheWayFlag.equals("2"))
     {
       tError.errorMessage = "给付总表没有找到要暂停的数据";
     }
     else
     {
       tError.errorMessage = "给付总表没有找到要去要暂停的数据";
     }
     this.mErrors.addOneError(tError);
     mLJSPaySet.clear();
     return false;
   }
   System.out.println("LJFIGet num=" + mLJSPaySet.size());
   return true;
 }
 private boolean setPauseFlag(){
     strSql = "";
     String strBankFlag = "";
     LJSPayDB tLJSPayDB = new LJSPayDB();
     CError tError = new CError();
     tLJSPayDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
     strBankFlag = mLJSPaySchema.getBankOnTheWayFlag();
     System.out.println("NoticeNo:  "+mLJSPaySchema.getGetNoticeNo());
     System.out.println("Flag:  "+strBankFlag);
     if (!tLJSPayDB.getInfo())
     {
         mErrors.copyAllErrors(tLJSPayDB.mErrors);
         return false;
     }
     else
     {
         System.out.println(strBankFlag);
         if (strBankFlag == null || strBankFlag.equals("")) strBankFlag = "0";
         if (strBankFlag.equals("0"))
         {
             strSql = "GetNoticeNo='" + mLJSPaySchema.getGetNoticeNo() + "'";
             tLJSPayDB.setBankOnTheWayFlag("2");
             tLJSPayDB.update(strSql);
             System.out.println("结果:  True");
             return true;

         }
         else if (strBankFlag.equals("1"))
         {
             tError.errorMessage = "数据在银行途中，不能进行操作！";
             this.mErrors.addOneError(tError);
             return false;
         }
         else
         {
             tError.errorMessage = "数据已经被暂停！";
             this.mErrors.addOneError(tError);
             return false;
         }

     }
     //;
 }
  private boolean cancelPauseFlag(){
      strSql = "";
      String strBankFlag = "";
      LJSPayDB tLJSPayDB = new LJSPayDB();
      CError tError = new CError();
      tLJSPayDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
      strBankFlag = mLJSPaySchema.getBankOnTheWayFlag();
      if (!tLJSPayDB.getInfo())
      {
          mErrors.copyAllErrors(tLJSPayDB.mErrors);
          return false;
      }
      else
      {
          if (strBankFlag == null || strBankFlag.equals("")) strBankFlag = "0";
          if (strBankFlag.equals("2"))
        {
            strSql = "GetNoticeNo='" + mLJSPaySchema.getGetNoticeNo() + "'";
            tLJSPayDB.setBankOnTheWayFlag("0");
            tLJSPayDB.update(strSql);
            return true;
        }
        else if (strBankFlag.equals("1"))
        {
            tError.errorMessage = "数据在银行途中，不能进行操作！";
            this.mErrors.addOneError(tError);
            return false;
        }
        else
        {
            tError.errorMessage = "数据已经被取消！";
            this.mErrors.addOneError(tError);
            return false;
        }

      }
      //return false;
  }
}
