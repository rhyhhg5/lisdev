 package com.sinosoft.lis.finfee;

import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单查询业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class TempFeeUpdateBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  /** 业务处理相关变量 */
  /** 暂交费表 */
  private LJTempFeeSet  mLJTempFeeSet = new LJTempFeeSet() ;
  private LJTempFeeClassSet  mLJTempFeeClassSet = new LJTempFeeClassSet() ;
  private LJTempFeeSchema  mLJTempFeeSchema = new LJTempFeeSchema() ;
  private LJTempFeeSchema mLJTempFeeSchemaOld = new LJTempFeeSchema();
  private GlobalInput mGlobalInput =new GlobalInput() ;

  public TempFeeUpdateBL() {}

  public static void main(String[] args) {

    LJTempFeeSet    tLJTempFeeSet     = new LJTempFeeSet();
    LJTempFeeClassSet  tLJTempFeeClassSet    = new LJTempFeeClassSet();
    LJTempFeeSchema    tLJTempFeeSchema     = new LJTempFeeSchema();
    LJTempFeeClassSchema  tLJTempFeeClassSchema    = new LJTempFeeClassSchema();
    GlobalInput tGI = new GlobalInput();
    tGI.ComCode="86110000";
    tGI.Operator="001";
    tGI.ManageCom="86110000";
//1st record:
    tLJTempFeeSchema     = new LJTempFeeSchema();
    tLJTempFeeSchema.setTempFeeNo("19970901000024");
    tLJTempFeeSchema.setTempFeeType("1");
    tLJTempFeeSchema.setPayDate("2004-02-23");
    tLJTempFeeSchema.setPayMoney(1000);
    tLJTempFeeSchema.setEnterAccDate("");
    tLJTempFeeSchema.setRiskCode("111301");
    tLJTempFeeSchema.setAgentCode("0000000001");
    tLJTempFeeSchema.setAgentGroup("010101");
    tLJTempFeeSchema.setManageCom("86110000");
    tLJTempFeeSchema.setOperator("001");
    tLJTempFeeSchema.setOtherNo("19970901000024");
    tLJTempFeeSchema.setOtherNoType("1");
 tLJTempFeeSet.add(tLJTempFeeSchema);

 tLJTempFeeClassSchema    = new LJTempFeeClassSchema();
 tLJTempFeeClassSchema.setTempFeeNo("19970901000024");
 tLJTempFeeClassSchema.setPayMode("3");
 tLJTempFeeClassSchema.setChequeNo("1234");
 tLJTempFeeClassSchema.setBankCode("1401");
 tLJTempFeeClassSchema.setPayDate("2004-02-23");
 tLJTempFeeClassSchema.setPayMoney(1000);
 tLJTempFeeClassSchema.setEnterAccDate("2004-02-23");
 tLJTempFeeClassSchema.setManageCom("86110000");
 tLJTempFeeClassSchema.setOperator("001");
 tLJTempFeeClassSchema.setManageCom("86110000");
tLJTempFeeClassSet.add(tLJTempFeeClassSchema);


VData tVData = new VData();
 tVData.addElement(tLJTempFeeSet);
 tVData.addElement(tLJTempFeeClassSet);
 tVData.addElement(tGI);

 TempFeeUpdateUI tTempFeeUpdateUI   = new TempFeeUpdateUI();

   tTempFeeUpdateUI.submitData(tVData,"UPDATE");

  }
  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    mInputData = (VData)cInputData.clone();
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
System.out.println("---getInputData---");

    //进行业务处理
    if (!updateLJTempFee())
	      return false;
System.out.println("---updateLJTempFee---");

if (!this.prepareOutputData())
              return false;

VData tVData = new VData();
tVData.addElement(mLJTempFeeSet);
tVData.addElement(mLJTempFeeClassSet);
tVData.addElement(mGlobalInput);

TempFeeBL tTempFeeBL   = new TempFeeBL();

   if (!tTempFeeBL.submitData(tVData,mOperate))
  {
    // @@错误处理
    this.mErrors.copyAllErrors(tTempFeeBL.mErrors);
    CError tError = new CError();
    tError.moduleName = "TempFeeUpdateBL";
    tError.functionName = "submitData";
    tError.errorMessage = "数据提交失败!";
    this.mErrors .addOneError(tError) ;
    return false;
    }
    System.out.println("tempfeeupdateBl END");
/*


TempFeeUpdateBLS tTempFeeUpdateBLS = new TempFeeUpdateBLS();
   if (!tTempFeeUpdateBLS.submitData(mInputData,mOperate))
   {
     // @@错误处理
     this.mErrors.copyAllErrors(tTempFeeUpdateBLS.mErrors);
     CError tError = new CError();
     tError.moduleName = "TempFeeUpdateBL";
     tError.functionName = "submitData";
     tError.errorMessage = "数据提交失败!";
     this.mErrors .addOneError(tError) ;
     return false;
    }
*/
    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    // 检验查询条件
    mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    mLJTempFeeSet=(LJTempFeeSet)cInputData.getObjectByObjectName("LJTempFeeSet",0);
    mLJTempFeeClassSet=(LJTempFeeClassSet)cInputData.getObjectByObjectName("LJTempFeeClassSet",0);
    if (mLJTempFeeSet==null || mLJTempFeeSet.size()==0)
      return false;
    mLJTempFeeSchema = mLJTempFeeSet.get(1);
    return true;
  }


  /**
   * 查询暂交费表信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean updateLJTempFee()
  {
  	// 保单信息
  System.out.println("update.class.enteraccdate:"+mLJTempFeeClassSet.get(1).getEnterAccDate());
  System.out.println("update.class.confmakedate:"+mLJTempFeeClassSet.get(1).getConfMakeDate());
  System.out.println("update.main.enteraccdate:"+mLJTempFeeSet.get(1).getEnterAccDate());
  System.out.println("update.main.confmakedate:"+mLJTempFeeSet.get(1).getConfMakeDate());

  String tSql="select * from LJTempFee where 1=1 ";
    if (mLJTempFeeSchema.getTempFeeNo()==null || mLJTempFeeSchema.getTempFeeNo().length()==0)
    {
      CError tError = new CError();
      tError.moduleName = "TempFeeQueryBL";
      tError.functionName = "queryData";
      tError.errorMessage = "暂收据号不能为空!";
      this.mErrors.addOneError(tError);
      mLJTempFeeSet.clear();
      return false;
    }

      tSql = tSql + " and tempfeeno='"+mLJTempFeeSchema.getTempFeeNo()+"' ";

    tSql = tSql + " and ManageCom like '"+mGlobalInput.ManageCom+"%'";


    System.out.println(tSql);

    LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
    LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
    tLJTempFeeSet = tLJTempFeeDB.executeQuery(tSql);

      if (tLJTempFeeDB.mErrors.needDealError() == true)
      {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJTempFeeDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "TempFeeQueryBL";
      tError.functionName = "queryData";
      tError.errorMessage = "暂交费查询失败!";
      this.mErrors.addOneError(tError);
      mLJTempFeeSet.clear();
      return false;
    }
    if (tLJTempFeeSet.size() == 0)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "TempFeeQueryBL";
      tError.functionName = "queryData";
      tError.errorMessage = "未找到相关数据!";
      this.mErrors.addOneError(tError);
      mLJTempFeeSet.clear();
      return false;
    }
    mLJTempFeeSchemaOld = tLJTempFeeSet.get(1);
    //mLJTempFeeSchemaOld
    boolean isAllEnterAcc = true;
    String tEnterAccDate="1900-01-01";
    double tSumBankMoney=0;
    LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
    tLJTempFeeClassDB.setTempFeeNo(mLJTempFeeSchemaOld.getTempFeeNo());
    LJTempFeeClassSet tLJTempFeeClassSetOld = tLJTempFeeClassDB.query();
    for (int i=1;i<=tLJTempFeeClassSetOld.size();i++)
    {
      LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
      tLJTempFeeClassSchema.setSchema(tLJTempFeeClassSetOld.get(i));
      if (tLJTempFeeClassSchema.getEnterAccDate()==null)
      {
        isAllEnterAcc = false; //没有全部都到账
        if (tLJTempFeeClassSchema.getPayMode().equals("4") && mLJTempFeeSchemaOld.getTempFeeType().equals("5"))
        {
          LJSPayDB tLJSPayDB = new LJSPayDB();
          tLJSPayDB.setGetNoticeNo(tLJTempFeeClassSchema.getTempFeeNo());
          if (tLJSPayDB.getInfo())
          {
            if (tLJSPayDB.getBankOnTheWayFlag().equals("1"))
            {
              CError tError = new CError();
              tError.moduleName = "TempFeeQueryBL";
              tError.functionName = "updateLJTempFee";
              tError.errorMessage = "该暂收费是银行首期扣款，数据已经送银行，不能修改!";
              this.mErrors.addOneError(tError);
              mLJTempFeeSet.clear();
              return false;
            }
          }
        }
      }
      else
      {
        if (tEnterAccDate.compareTo(tLJTempFeeClassSchema.getEnterAccDate())<0)
        {
          tEnterAccDate=tLJTempFeeClassSchema.getEnterAccDate();
        }
      }
      if (tLJTempFeeClassSchema.getPayMode().equals("4"))
      {
        tSumBankMoney=tSumBankMoney+tLJTempFeeClassSchema.getPayMoney();
      }
    }
    for (int i=1;i<=tLJTempFeeSet.size();i++)
    {
      LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
      tLJTempFeeSchema.setSchema(tLJTempFeeSet.get(i));
      if (tLJTempFeeSchema.getConfFlag().equals("1")) //已经核销
      {
        CError tError = new CError();
        tError.moduleName = "TempFeeQueryBL";
        tError.functionName = "updateLJTempFee";
        tError.errorMessage = "该暂收费已经核销，不能修改!";
        this.mErrors.addOneError(tError);
        mLJTempFeeSet.clear();
        return false;
      }
      if (isAllEnterAcc) //已经全部到账
      {
        if (!tLJTempFeeSchema.getMakeDate().equals(PubFun.getCurrentDate()))
        {
          CError tError = new CError();
          tError.moduleName = "TempFeeQueryBL";
          tError.functionName = "updateLJTempFee";
          tError.errorMessage = "该暂收已经全部到账，只能修改当天数据!";
          this.mErrors.addOneError(tError);
          mLJTempFeeSet.clear();
          return false;
        }
      }
    }



    int tMainCount=0;
    for (int i=1;i<=mLJTempFeeSet.size();i++)
    {
      LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
      tLJTempFeeSchema.setSchema(mLJTempFeeSet.get(i));
      //对于没有全部都到账确认的暂收费记录置空到账日期，否则取最晚的到账日期为暂收费到账日期
      if (isAllEnterAcc)
      {
        tLJTempFeeSchema.setEnterAccDate(tEnterAccDate);
      }
      else
      {
        tLJTempFeeSchema.setEnterAccDate("");
      }
      mLJTempFeeSet.set(i,tLJTempFeeSchema);

      LMRiskAppDB tLMRiskAppDB = new LMRiskAppDB();
      tLMRiskAppDB.setRiskCode(tLJTempFeeSchema.getRiskCode());
      if (!tLMRiskAppDB.getInfo())
      {
        CError tError = new CError();
        tError.moduleName = "TempFeeQueryBL";
        tError.functionName = "updateLJTempFee";
        tError.errorMessage = "无此险种编码:"+tLJTempFeeSchema.getRiskCode();
        this.mErrors.addOneError(tError);
        mLJTempFeeSet.clear();
        return false;
      }
      if (tLMRiskAppDB.getSubRiskFlag().equals("M"))
        tMainCount++;
      if (tMainCount>1 && tLJTempFeeSchema.getTempFeeType().equals("1"))
      {
        CError tError = new CError();
        tError.moduleName = "TempFeeQueryBL";
        tError.functionName = "updateLJTempFee";
        tError.errorMessage = "新单暂收不能录入多个主险";
        this.mErrors.addOneError(tError);
        mLJTempFeeSet.clear();
        return false;
      }

      System.out.println("otherno:"+tLJTempFeeSchema.getOtherNo());
      System.out.println("othertype:"+tLJTempFeeSchema.getOtherNoType());
      if (tLJTempFeeSchema.getOtherNo()==null || tLJTempFeeSchema.getOtherNoType()==null
          ||tLJTempFeeSchema.getOtherNo().length()==0 ||tLJTempFeeSchema.getOtherNoType().length()==0)
      {
        CError tError = new CError();
        tError.moduleName = "TempFeeQueryBL";
        tError.functionName = "updateLJTempFee";
        tError.errorMessage = "请录入其它号码类型和其它号码";
        this.mErrors.addOneError(tError);
        mLJTempFeeSet.clear();
        return false;
      }

      for (int j=i+1;j<=mLJTempFeeSet.size();j++)
      {
        LJTempFeeSchema ttLJTempFeeSchema = new LJTempFeeSchema();
        ttLJTempFeeSchema.setSchema(mLJTempFeeSet.get(j));
        if (ttLJTempFeeSchema.getRiskCode().equals(tLJTempFeeSchema.getRiskCode()))
        {
          CError tError = new CError();
          tError.moduleName = "TempFeeQueryBL";
          tError.functionName = "updateLJTempFee";
          tError.errorMessage = "不能录入重复的险种编码!";
          this.mErrors.addOneError(tError);
          mLJTempFeeSet.clear();
          return false;
        }
      }
    }
    if (tLJTempFeeSet.size()!=mLJTempFeeSet.size() && !mLJTempFeeSet.get(1).getTempFeeType().equals("1"))
    {
      CError tError = new CError();
      tError.moduleName = "TempFeeQueryBL";
      tError.functionName = "updateLJTempFee";
      tError.errorMessage = "续期不能增加条数!";
      this.mErrors.addOneError(tError);
      mLJTempFeeSet.clear();
      return false;
    }
    if (tLJTempFeeSet.get(1).getOtherNo()!=null && mLJTempFeeSet.get(1).getOtherNo()!=null)
    {
      if (!tLJTempFeeSet.get(1).getOtherNo().equals(mLJTempFeeSet.get(1).getOtherNo()) && !mLJTempFeeSet.get(1).getTempFeeType().equals("1") )
      {
        CError tError = new CError();
        tError.moduleName = "TempFeeQueryBL";
        tError.functionName = "updateLJTempFee";
        tError.errorMessage = "续期不能更改其它号码!";
        this.mErrors.addOneError(tError);
        mLJTempFeeSet.clear();
        return false;
      }
    }


    double tOldBankMoney=0;
    for (int i=1;i<=mLJTempFeeClassSet.size();i++)
    {
      LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
      tLJTempFeeClassSchema.setSchema(mLJTempFeeClassSet.get(i));
      if (tLJTempFeeClassSchema.getEnterAccDate()!=null)
      {
        if (tLJTempFeeClassSchema.getEnterAccDate().compareTo(PubFun.getCurrentDate())>0)
        {
          CError tError = new CError();
          tError.moduleName = "TempFeeQueryBL";
          tError.functionName = "updateLJTempFee";
          tError.errorMessage = "到帐日期不能超过今天!";
          this.mErrors.addOneError(tError);
          mLJTempFeeSet.clear();
          return false;
        }
      }
      if (tLJTempFeeClassSchema.getPayMode()==null || tLJTempFeeClassSchema.getPayMode().length()==0)
      {
        CError tError = new CError();
        tError.moduleName = "TempFeeQueryBL";
        tError.functionName = "updateLJTempFee";
        tError.errorMessage = "请录入交费方式!";
        this.mErrors.addOneError(tError);
        mLJTempFeeSet.clear();
        return false;
      }
      else if(tLJTempFeeClassSchema.getPayMode().equals("4"))
      {
        if (tLJTempFeeClassSchema.getAccName().length()==0 || tLJTempFeeClassSchema.getBankAccNo().length()==0 || tLJTempFeeClassSchema.getBankCode().length()==0)
        {
          CError tError = new CError();
          tError.moduleName = "TempFeeQueryBL";
          tError.functionName = "updateLJTempFee";
          tError.errorMessage = "请录入开户银行,银行账号,户名!";
          this.mErrors.addOneError(tError);
          mLJTempFeeSet.clear();
          return false;
        }
        if (tLJTempFeeClassSchema.getEnterAccDate()!=null)
        {
          CError tError = new CError();
          tError.moduleName = "TempFeeQueryBL";
          tError.functionName = "updateLJTempFee";
          tError.errorMessage = "交费方式银行转账，到帐日期必须为空";
          this.mErrors.addOneError(tError);
          mLJTempFeeSet.clear();
          return false;
        }
        tOldBankMoney=tOldBankMoney+tLJTempFeeClassSchema.getPayMoney();
      }
      else if (tLJTempFeeClassSchema.getPayMode().equals("5"))
      {
        if (tLJTempFeeClassSchema.getChequeNo().length()==0 || tLJTempFeeClassSchema.getEnterAccDate().length()==0)
        {
          CError tError = new CError();
          tError.moduleName = "TempFeeQueryBL";
          tError.functionName = "updateLJTempFee";
          tError.errorMessage = "内部转账时，票据号码和到帐日期不能为空";
          this.mErrors.addOneError(tError);
          mLJTempFeeSet.clear();
          return false;
        }
      }
      else if(tLJTempFeeClassSchema.getPayMode().equals("2") || tLJTempFeeClassSchema.getPayMode().equals("3"))
      {
        if (tLJTempFeeClassSchema.getChequeNo().length()==0 || tLJTempFeeClassSchema.getBankCode().length()==0)
        {
          CError tError = new CError();
          tError.moduleName = "TempFeeQueryBL";
          tError.functionName = "updateLJTempFee";
          tError.errorMessage = "交费方式为支票时，票据号码和银行不能为空";
          this.mErrors.addOneError(tError);
          mLJTempFeeSet.clear();
          return false;
        }
      }


      for (int j=i+1;j<=mLJTempFeeClassSet.size();j++)
      {
        LJTempFeeClassSchema ttLJTempFeeClassSchema = new LJTempFeeClassSchema();
        ttLJTempFeeClassSchema.setSchema(mLJTempFeeClassSet.get(j));
        if (ttLJTempFeeClassSchema.getPayMode()==null)
          continue;
        if (ttLJTempFeeClassSchema.getPayMode().equals(tLJTempFeeClassSchema.getPayMode()))
        {
          CError tError = new CError();
          tError.moduleName = "TempFeeQueryBL";
          tError.functionName = "updateLJTempFee";
          tError.errorMessage = "不能录入重复的交费方式!";
          this.mErrors.addOneError(tError);
          mLJTempFeeSet.clear();
          return false;
        }
      }
    }
    if (tOldBankMoney>0)
    {
      LJSPayDB tLJSPayDB = new LJSPayDB();
      tLJSPayDB.setGetNoticeNo(mLJTempFeeClassSet.get(1).getTempFeeNo());
      if (tLJSPayDB.getCount()>0)
      {
        CError tError = new CError();
        tError.moduleName = "TempFeeQueryBL";
        tError.functionName = "updateLJTempFee";
        tError.errorMessage = "该暂收费已经生成送银行数据!";
        this.mErrors.addOneError(tError);
        mLJTempFeeSet.clear();
        return false;
      }
    }
//    if (Math.abs(tOldBankMoney-tSumBankMoney)>=0.001)
//    {
//      CError tError = new CError();
//      tError.moduleName = "TempFeeQueryBL";
//      tError.functionName = "updateLJTempFee";
//      tError.errorMessage = "银行转账的暂收分类的金额不符!";
//      this.mErrors.addOneError(tError);
//      mLJTempFeeSet.clear();
//      return false;
//    }
    double tLJTempFeeSum=0;
    double tLJTempFeeClassSum=0;
    for (int k=1;k<=mLJTempFeeSet.size();k++)
    {
      tLJTempFeeSum=tLJTempFeeSum+mLJTempFeeSet.get(k).getPayMoney();
    }
    for (int k=1;k<=mLJTempFeeClassSet.size();k++)
    {
      tLJTempFeeClassSum=tLJTempFeeClassSum+mLJTempFeeClassSet.get(k).getPayMoney();
    }
    System.out.println("tLJTempFeeSum:"+tLJTempFeeSum);
    System.out.println("tLJTempFeeClassSum:"+tLJTempFeeClassSum);
    if (Math.abs((tLJTempFeeSum-tLJTempFeeClassSum))>=0.001)
    {
      CError tError = new CError();
      tError.moduleName = "TempFeeQueryBL";
      tError.functionName = "updateLJTempFee";
      tError.errorMessage = "暂收费和暂收分类的金额不符!";
      this.mErrors.addOneError(tError);
      mLJTempFeeSet.clear();
      return false;
    }



    return true;
  }


  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData()
  {
    LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
    tLJTempFeeClassDB.setTempFeeNo(this.mLJTempFeeSchemaOld.getTempFeeNo());
    LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();
    tLJTempFeeClassSet=tLJTempFeeClassDB.query();
    LJTempFeeClassSchema tLJTempFeeClassSchemaOld = new LJTempFeeClassSchema();
    tLJTempFeeClassSchemaOld=tLJTempFeeClassSet.get(1);
    String tEnterAccFlag="1";
    String tConfMakeFlag="1";
    String tEnterDate="1900-01-01";
    for (int i=1;i<=mLJTempFeeClassSet.size();i++)
    {
      LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
      tLJTempFeeClassSchema.setSchema(mLJTempFeeClassSet.get(i));
//      tLJTempFeeClassSchema.setAPPntName(tLJTempFeeClassSchemaOld.getAPPntName());
      tLJTempFeeClassSchema.setConfDate(tLJTempFeeClassSchemaOld.getConfDate());
      tLJTempFeeClassSchema.setApproveDate(tLJTempFeeClassSchemaOld.getApproveDate());
      tLJTempFeeClassSchema.setConfFlag(tLJTempFeeClassSchemaOld.getConfFlag());
      tLJTempFeeClassSchema.setSerialNo(tLJTempFeeClassSchemaOld.getSerialNo());
      tLJTempFeeClassSchema.setOperator(tLJTempFeeClassSchemaOld.getOperator());
      tLJTempFeeClassSchema.setMakeDate(tLJTempFeeClassSchemaOld.getMakeDate());
      tLJTempFeeClassSchema.setMakeTime(tLJTempFeeClassSchemaOld.getMakeTime());
      tLJTempFeeClassSchema.setModifyDate(PubFun.getCurrentDate());
      tLJTempFeeClassSchema.setModifyTime(PubFun.getCurrentTime());
      if (tLJTempFeeClassSchema.getPayMode().equals("1"))
      {
        tLJTempFeeClassSchema.setEnterAccDate(PubFun.getCurrentDate());
        tLJTempFeeClassSchema.setConfMakeDate(PubFun.getCurrentDate());
      }
      else
      {
        tConfMakeFlag="0"; //凡是有非现金交费方式，主表都不置到账确认日期
      }
      if (tLJTempFeeClassSchema.getEnterAccDate()!=null && tLJTempFeeClassSchema.getEnterAccDate().length()!=0 )
      {
        //tLJTempFeeClassSchema.setConfMakeDate(mLJTempFeeSchemaOld.getConfMakeDate());
        if (tLJTempFeeClassSchema.getEnterAccDate().compareTo(tEnterDate)>0)
        {
          tEnterDate=tLJTempFeeClassSchema.getEnterAccDate();
        }
      }
      else
      {
        tEnterAccFlag="0";
      }
      System.out.println("prepare.class.enteraccdate:"+tLJTempFeeClassSchema.getEnterAccDate());
      System.out.println("prepare.class.confmakedate:"+tLJTempFeeClassSchema.getConfMakeDate());

      //tLJTempFeeClassSchema.setConfMakeDate(tLJTempFeeClassSchemaOld.getConfMakeDate());
      mLJTempFeeClassSet.set(i,tLJTempFeeClassSchema);

    }

    for (int i=1;i<=mLJTempFeeSet.size();i++)
    {
      LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
      tLJTempFeeSchema.setSchema(mLJTempFeeSet.get(i));
      tLJTempFeeSchema.setPayIntv(mLJTempFeeSchemaOld.getPayIntv());
      tLJTempFeeSchema.setConfDate(mLJTempFeeSchemaOld.getConfDate());
      tLJTempFeeSchema.setSaleChnl(mLJTempFeeSchemaOld.getSaleChnl());
      tLJTempFeeSchema.setAgentCom(mLJTempFeeSchemaOld.getAgentCom());
      tLJTempFeeSchema.setAgentType(mLJTempFeeSchemaOld.getAgentType());
      tLJTempFeeSchema.setAPPntName(mLJTempFeeSchemaOld.getAPPntName());
      tLJTempFeeSchema.setConfFlag(mLJTempFeeSchemaOld.getConfFlag());
      tLJTempFeeSchema.setSerialNo(mLJTempFeeSchemaOld.getSerialNo());
      tLJTempFeeSchema.setOperator(mLJTempFeeSchemaOld.getOperator());
      tLJTempFeeSchema.setState(mLJTempFeeSchemaOld.getState());
      tLJTempFeeSchema.setMakeDate(mLJTempFeeSchemaOld.getMakeDate());
      tLJTempFeeSchema.setMakeTime(mLJTempFeeSchemaOld.getMakeTime());
      tLJTempFeeSchema.setModifyDate(PubFun.getCurrentDate());
      tLJTempFeeSchema.setModifyTime(PubFun.getCurrentTime());
      tLJTempFeeSchema.setConfDate(mLJTempFeeSchemaOld.getConfDate());

      if (tEnterAccFlag.equals("1"))
      {
        tLJTempFeeSchema.setEnterAccDate(tEnterDate);
      }
      if (tConfMakeFlag.equals("1"))
      {
        tLJTempFeeSchema.setConfMakeDate(PubFun.getCurrentDate());
      }
      //tLJTempFeeSchema.setEnterAccDate(mLJTempFeeSchemaOld.getEnterAccDate());
      System.out.println("prepare.main.enteraccdate:"+tLJTempFeeSchema.getEnterAccDate());
      System.out.println("prepare.main.confmakedate:"+tLJTempFeeSchema.getConfMakeDate());

      mLJTempFeeSet.set(i,tLJTempFeeSchema);
    }




    mInputData.clear();

    mInputData.add(mLJTempFeeSet);
    mInputData.add(mLJTempFeeClassSet);

    return true;
  }
}
