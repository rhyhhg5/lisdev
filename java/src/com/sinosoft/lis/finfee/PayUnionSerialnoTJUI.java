package com.sinosoft.lis.finfee;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PayUnionSerialnoTJUI {



	  //业务处理相关变量	
	  //private VData cInputData ;
	  public CErrors mErrors = new CErrors();
	  public PayUnionSerialnoTJUI() {
	  }
	  public boolean submitData(VData cInputData, String cOperate){
		  
		  //this.cInputData = (VData)cInputData.clone();
		  
		  PayUnionSerialnoTJBL tPayUnionSerialnoTJBL =new PayUnionSerialnoTJBL();
		 
		  if (!tPayUnionSerialnoTJBL.submitData(cInputData,cOperate))
	      {
	         // @@错误处理
	        this.mErrors.copyAllErrors(tPayUnionSerialnoTJBL.mErrors);
	        CError tError = new CError();
	        tError.moduleName = "tPayUnionSerialnoTJUI";
	        tError.functionName = "submitData";
	        tError.errorMessage = "数据查询失败!";
	        this.mErrors .addOneError(tError) ;
	        return false;
	      }
	  return true;  
	  }

}
