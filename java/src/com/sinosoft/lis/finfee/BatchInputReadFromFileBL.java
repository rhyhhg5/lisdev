package com.sinosoft.lis.finfee;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.io.*;

import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import javax.xml.transform.Source;
import javax.xml.transform.Result;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamSource;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 银行文件转换到数据模块</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author Minim
 * @version 1.0
 */

public class BatchInputReadFromFileBL {
  /** 传入数据的容器 */
  private VData mInputData = new VData();
  /** 传出数据的容器 */
  private VData mResult = new VData();
  /** 提交数据的容器 */
  private MMap map = new MMap();
  /** 数据操作字符串 */
  private String mOperate;
  /** 错误处理类 */
  public  CErrors mErrors = new CErrors();

  //业务数据
  private TransferData inTransferData = new TransferData();
  private GlobalInput inGlobalInput = new GlobalInput();
  private String fileName = "";

  private LDBankSet outLDBankSet = new LDBankSet();

  /** 返回批次号 */
  private String serialNo = "";
  private String DealType = "";
  /** 选择银行编码 */
  private String bankCode = "";



  private Document dataDoc = null;
  private Document resultDoc = null;

  private Reflections mReflections = new Reflections();

  public BatchInputReadFromFileBL() {
  }

  /**
   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
   * @param cInputData 传入的数据,VData对象
   * @param cOperate 数据操作字符串，主要包括"READ"和""
   * @return 布尔值（true--提交成功, false--提交失败）
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mInputData = (VData)cInputData.clone();
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData()) return false;
    System.out.println("---End getInputData---");

    //进行业务处理
    if (!dealData()) return false;
    System.out.println("---End dealData---");


    return true;
  }

  /**
   * 将外部传入的数据分解到本类的属性中
   * @param: 无
   * @return: boolean
   */
  private boolean getInputData()	{
    try {
      inTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);
      inGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput", 0);
    }
    catch (Exception e) {
      // @@错误处理
      CError.buildErr(this, "接收数据失败");
      return false;
    }

    return true;
  }

  

  /**
   * 创建一个xml文档对象DOM
   * @return
   */
  private Document buildDocument() {
    try {
      //Create the document builder
      DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder docbuilder = dbfactory.newDocumentBuilder();

      //Create the new document(s)
      return docbuilder.newDocument();
    }
    catch (Exception e) {
      System.out.println("Problem creating document: " + e.getMessage());
      return null;
    }
  }

  /**
   * 获取xsl文件路径
   * @return
   */
  public String getXslPath() throws Exception {
//    String tsql="select * from ldsysvar where sysvar='BankInput' with ur";
    String xslPath="";
    LDSysVarDB tLDSysVarDB=new LDSysVarDB();
    tLDSysVarDB.setSysVar("BankInput");
    if (!tLDSysVarDB.getInfo()) throw new Exception("获取银行编码批量导入XSL描述信息失败！");
    xslPath = tLDSysVarDB.getSysVarValue();
    System.out.println("银行编码批量导入XSL描述信息路径："+xslPath);
    return xslPath;
  }


  /**
   * Simple sample code to show how to run the XSL processor
   * from the API.
   */
  public boolean xmlTransform() {
    try {
   
      String xslPath = getXslPath();
   
      //本地调试"D:/LIS_DevelopVer/V1.1/ui/bank/ReturnFromBankFile/xsl/return_sz_bankunite_mx.xsl"
      System.out.println("xslPath:" + xslPath);

      File fStyle = new File(xslPath);
      Source source = new DOMSource(dataDoc);
      Result result = new DOMResult(resultDoc);
      Source style = new StreamSource(fStyle);

      //Create the Transformer
      TransformerFactory transFactory = TransformerFactory.newInstance();
      Transformer transformer = transFactory.newTransformer(style);

      //Transform the Document
      transformer.transform(source, result);

      System.out.println("Transform Success!");
    }
    catch(Exception e) {
      e.printStackTrace();
      System.out.println(e.getMessage());
      // @@错误处理
      CError.buildErr(this, "Xml处理失败");
      return false;
    }

    return true;
  }

  

  /**
   * 将数据存入数据库
   * @param tLYReturnFromBankSet
   * @return
   */
  private LDBankSet xmlToDatabase() {
    try {
      String innerBankCode = "";
      LDBankSet tLDBankSet = new LDBankSet();
      FinBankAddUI tFinBankAddUI=new FinBankAddUI();
      displayDocument(resultDoc);
      boolean treturn=false;
      //get all rows
      NodeList rows = resultDoc.getDocumentElement().getChildNodes();
      System.out.println("rows.getLength():"+rows.getLength());
      treturn=checkFile(rows);
      if(treturn==false){
          Exception tException= new Exception("银行编码导入文件不符合要求");
          System.out.println("tException:"+tException.getMessage());
          throw tException;
      }
      for(int i=0; i<rows.getLength(); i++){
        //For each row, get the row element and name
        Element thisRow = (Element)rows.item(i);

        //Get the columns for thisRow
        NodeList columns = thisRow.getChildNodes();
        LDBankSchema tLDBankSchema = new LDBankSchema();
        
        for(int j=0; j<columns.getLength(); j++){
          Element thisColumn = (Element)columns.item(j);

          String colName = thisColumn.getNodeName();
          String colValue = thisColumn.getFirstChild().getNodeValue();
          System.out.println("colName:" + colName + ":" + colValue);
          inTransferData.removeByName(colName);
          inTransferData.setNameAndValue(colName.trim(), colValue.trim());
         
        }
        mInputData.add(inTransferData);
        treturn=tFinBankAddUI.submitData(mInputData, "CHILD");
 
//       VData tVData=tFinBankAddUI.getVResult();

        if(treturn==true){
        tLDBankSchema=tFinBankAddUI.getLDBankSchema();
        }else{
            tLDBankSchema.setBankCode("");
            tLDBankSchema.setBankName("");
        }
        System.out.println("tLDBankSchema:"+tLDBankSchema.getBankCode());
        tLDBankSet.add(tLDBankSchema);
      }
      return tLDBankSet;
      }
    catch (Exception e) {
      e.printStackTrace();
      // @@错误处理
        CError.buildErr(this, "Xml转入数据库处理失败: " + e.getMessage());
      return null;
    }
  }
  /**
   * 检查文件中有不合要求的银行编码等信息
   * 
   * */
  private boolean checkFile(NodeList tNodeList){
      
      for(int i=0; i<tNodeList.getLength(); i++){
          //For each row, get the row element and name
          Element thisRow = (Element)tNodeList.item(i);

          //Get the columns for thisRow
          NodeList columns = thisRow.getChildNodes();
          LDBankSchema tLDBankSchema = new LDBankSchema();
          
          for(int j=0; j<columns.getLength(); j++){
            Element thisColumn = (Element)columns.item(j);

            String colName = thisColumn.getNodeName();
            String colValue = thisColumn.getFirstChild().getNodeValue();
            System.out.println("colName.trim():" + colName.trim() + ":" + colValue.trim());
            if(colName==null||colName==""||colValue==null||"".equalsIgnoreCase(colValue.trim()))
            { 
              return  false;
            }
          }
      }
      return true;
  }
  
  /**
   * 读取银行返回文件
   * @param fileName
   * @return
   */
  private boolean readBankFile(String fileName) {
    //Declare the document
    dataDoc = buildDocument();
    resultDoc = buildDocument();

    try {
      //读入文件到BUFFER中以提高处理效率
      BufferedReader in = new BufferedReader(
          new FileReader(fileName));

      //将所有文本以行为单位读入到VECTOR中
      String strLine = "";

      //创建根标签
      Element dataRoot = dataDoc.createElement("BANKDATA");

      //循环获取每一行
      while(true) {
        strLine = in.readLine();
        if (strLine == null) break;
        strLine = strLine.trim();
        //去掉空行
        if (strLine.length() < 3) continue;
        System.out.println(strLine);
        //System.out.println("strLen: " + strLine.length());

        //Create the element to hold the row
        Element rowEl = dataDoc.createElement("ROW");

        Element columnEl = dataDoc.createElement("COLUMN");
        columnEl.appendChild(dataDoc.createTextNode(strLine));
        rowEl.appendChild(columnEl);

        //Add the row element to the root
        dataRoot.appendChild(rowEl);
      }

      //Add the root to the document
      dataDoc.appendChild(dataRoot);
//      NodeList tables = dataDoc.getDocumentElement().getChildNodes();
      //System.out.println("tables.getLength():" + tables.getLength());

      //显示XML信息，调试用
      displayDocument(dataDoc);

      in.close();
    }
    catch (Exception e) {
      e.printStackTrace();
      // @@错误处理
      CError.buildErr(this, "读取银行返回文件失败");
      return false;
    }

    return true;
  }

  /**
   * 根据前面的输入数据，进行逻辑处理
   * @return 如果在处理过程中出错，则返回false,否则返回true
   */
  private boolean dealData() {
    try {
      //获取银行文件数据
      if (mOperate.equals("READ")) {
        //获取返回文件名称
        fileName = (String)inTransferData.getValueByName("fileName");
        fileName = fileName.replace('\\', '/');
        //读取银行返回文件，公共部分
        if (!readBankFile(fileName)) throw new Exception("读取银行编码文件失败");

        //转换xml，公共部分
        if (!xmlTransform()) throw new Exception("转换xml失败");

        //将数据存入数据库
        outLDBankSet = xmlToDatabase();
        if (outLDBankSet==null) return false;

      }
    }
    catch(Exception e) {
      // @@错误处理
        System.out.println(e.getMessage());
      CError.buildErr(this, "数据处理错误:" + e.getMessage());
      return false;
    }

    return true;
  }


  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public LDBankSet getLDBankSet() {
    return outLDBankSet;
  }

  /**
   * 数据输出方法，供外界获取数据处理结果
   * @return 包含有数据查询结果字符串的VData对象
   */
  public VData getResult() {
    return mResult;
  }

  public static void main(String[] args) {
    BatchInputReadFromFileBL writeToFileBL1 = new BatchInputReadFromFileBL();
  }

  public static int num = 0;
  public void displayDocument(Node d) {
    num += 2;

    if (d.hasChildNodes()) {
      NodeList nl = d.getChildNodes();

      for (int i=0; i<nl.getLength(); i++) {
        Node n = nl.item(i);

        for (int j=0; j<num; j++) {
          System.out.print(" ");
        }
        if (n.getNodeValue() == null) {
          System.out.println("<" + n.getNodeName() + ">");
        }
        else {
          System.out.println(n.getNodeValue());
        }

        displayDocument(n);

        num -= 2;
//        System.out.println("num:" + num);

        if (n.getNodeValue() == null) {
          for (int j=0; j<num; j++) {
            System.out.print(" ");
          }
          System.out.println("</" + n.getNodeName() + ">");
        }

      }

    }
  }
}
