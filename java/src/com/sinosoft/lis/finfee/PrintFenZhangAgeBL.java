/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.finfee;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import utils.system;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.ES_DOC_MAINDB;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCCUWMasterDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCUWMasterDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LMRiskDB;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCUWMasterSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.TransferData;
import java.util.Date;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.FDate;
public class PrintFenZhangAgeBL implements PrintService
{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();
    
    private VData mInputData;

    private String xmlUrl = "";

    private TransferData mTransferData = null;
    
    private Date EndInputDate;
    
    private String  dateEnd;
    
    private String lys_Flag = "0";

    private String lys_Flag_main = "0";

    private String FORMATMODOL = "0.00"; //保费保额计算出来后的精确位数

    private DecimalFormat mDecimalFormat = new DecimalFormat(FORMATMODOL); //数字转换对象

    //业务处理相关变量
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();//用户及系统信息

    private String beforeDate;
    private String afterDate;
    

    private String tOneDOne;
    private String tOneDTwo ;
    private String tOneDThree;
    private String tOneDFour;
    private String tTwoDOne;
    private String tTwoDTwo;
    private String tTwoDThree;
    private String tTwoDFour;
    
    StrTool tSrtTool = new StrTool();
    ExeSQL tExeSQL = new ExeSQL();
    
    
  //输入的查询sql语句
    //关联方
    

   private final String mSqlNGOne="select nvl(sum(a.summoney),0)"+
	  "from db2inst1.lidatatransresult a"+" "+
	  "where a.accountdate = 'inputParamdateEnd'"+
	   "and a.classtype = 'J-01'"+
	   "and exists"+
	 "(select 1 from lccont where contno = a.contno"+
	 "and (Appntname not like '%人保%' and appntname not like '%中国人民保险%')"+
	 "and firstpaydate between 'inputParamBeforData' and 'inputParamAfterData'"+
	 "union all"+" "+
	  "select 1"+
	  " from Lcgrpcont"+
	  " where grpcontno = a.contno"+" "+
	  "and (Grpname not like '%人保%' and  Grpname not like '%中国人民保险%')"+
	  " and signdate between 'inputParamBeforData' and 'inputParamAfterData')"+
	  "and a.finitemtype = 'D' with ur" ;
    
    private final String mSqlGOne =" select nvl(sum(a.summoney),0)"+
    "from db2inst1.lidatatransresult a"+" "+
    "where a.accountdate = 'inputParamdateEnd'"+
     " and a.classtype = 'J-01'"+
     " and exists"+
    "(select 1 from lccont where contno = a.contno"+
             "  and (Appntname like '%人保%' or appntname like '%中国人民保险%')"+
             " and firstpaydate between 'inputParamBeforData' and 'inputParamAfterData'"+
           "union all"+" "+
           "select 1"+
            " from Lcgrpcont"+" "+
            "where grpcontno = a.contno"+" "+
              "and (Grpname like '%人保%' or Grpname like '%中国人民保险%')"+
              "and signdate between 'inputParamBeforData' and 'inputParamAfterData') and a.finitemtype = 'D' with ur";
    
    public PrintFenZhangAgeBL()
    {
        System.out.print(dateEnd);
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
    	TransferData tTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
    	this.mTransferData = tTransferData;
    	
    	
    	dateEnd = (String)mTransferData.getValueByName("EndInputDate");
    	mInputData = (VData) cInputData.clone();
        if (!cOperate.equals("CONFIRM"))
        {
            buildError("submitData", "不支持的操作字符串");
            return false;
        }

        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData())
        {
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData VData
     * @return boolean
     */
    
    private boolean getInputData(VData cInputData) {
        try {
             mTransferData = (TransferData) cInputData.
                                         getObjectByObjectName("TransferData", 0);
             if(mTransferData==null){
            	 CError.buildErr(this, "接收数据失败");
                 return false;
             }

        } catch (Exception e) {
            CError.buildErr(this, "接收数据失败");
            return false;
        }

        return true;
    }
    
    

    public VData getResult()
    {
        return mResult;
    }

    public CErrors getErrors()
    {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LCPolF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    
    
    private boolean getPrintData()
    {

		
		    XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例 把数据放入此对象中
	        TextTag texttag = new TextTag(); //新建一个TextTag的实例 把模板中的变量值添加到此对象中
	        xmlexport.createDocument("PrintSPremium.vts", "printer"); //最好紧接着就初始化xml文档
	        
	   	   
	        String lessThreeMSql="select substr(char(date('"+dateEnd+"')-2 MONTHS),1,7)||'-01' from dual where 1=1";	        
	        Object obj1 = mOneDOne(lessThreeMSql);
	        Object obj11 = mOneDOne(lessThreeMSql);
	        String tOneDOne = "";
	        String tTwoDOne = "";
	        if(obj1 != null){
	        	tOneDOne = obj1.toString();
	        }
	        if(obj11 != null){
	        	tTwoDOne = obj11.toString();
	        }
			
			String betweenTAndSSqlBegin="select substr(char((date(substr(char(date('"+dateEnd+"')-2 MONTHS),1,7)||'-01')-1 days)-2 months),1,7)||'-01' from dual where 1=1";
			String betweenTAndSSqlAfter="select date(substr(char(date('"+dateEnd+"')-2 MONTHS),1,7)||'-01')-1 days from dual where 1=1";
			Object obj2=mOneDTwo(betweenTAndSSqlBegin,betweenTAndSSqlAfter);
			Object obj22=mOneDTwo(betweenTAndSSqlBegin,betweenTAndSSqlAfter);
			String tOneDTwo = "";
			String tTwoDTwo = "";
			if(obj2!=null){
				tOneDTwo=obj2.toString();
			}
			if(obj22!=null){
				tTwoDTwo=obj22.toString();
			}
			
			String betweenSandDSqlBegin="select date(substr(char((date(substr(char(date('"+dateEnd+"')-2 MONTHS),1,7)||'-01')-1 days)-2 months),1,7)||'-01')-1 days from dual";
	        String betweenSandDSqlAfter="select substr(char(date(date(substr(char((date(substr(char(date('"+dateEnd+"')-2 MONTHS),1,7)||'-01')-1 days)-2 months),1,7)||'-01')-1 days)-5 months),1,7)||'-01' from dual";
	        Object obj3=mOneDThree(betweenSandDSqlBegin,betweenSandDSqlAfter);
			String tOneDThree = "";
			if(obj3!=null){
				tOneDThree=obj3.toString();
			}
			 Object obj33=mOneDThree(betweenSandDSqlBegin,betweenSandDSqlAfter);
				String tTwoDThree = "";
				if(obj33!=null){
					tTwoDThree=obj33.toString();
				}
	        
			String moreThanDecSql="select substr(char(date('"+dateEnd+"')-11 MONTHS),1,7)||'-01' from dual";
	        String moreThanDecSqlAfter="select date(substr(char((date(substr(char(date('"+dateEnd+"')-11 MONTHS),1,7)||'-01')-1 days)-11 months),1,7)||'-01')-1 days from dual";
	        Object obj4=mOneDFour(moreThanDecSql,moreThanDecSqlAfter);
			String tOneDFour = "";
			if(obj4!=null){
				tOneDFour=obj4.toString();
			}
			  Object obj44=mOneDFour(moreThanDecSql,moreThanDecSqlAfter);
				String tTwoDFour = "";
				if(obj44!=null){
					tTwoDFour=obj44.toString();
				}
			
	
				double tSumOne;
		    	tSumOne=Double.parseDouble(tOneDOne)+Double.parseDouble(tOneDTwo)+Double.parseDouble(tOneDThree)+Double.parseDouble(tOneDFour);
		    	double tSumTwo;
		    	tSumTwo=Double.parseDouble(tTwoDOne)+Double.parseDouble(tTwoDTwo)+Double.parseDouble(tTwoDThree)+Double.parseDouble(tTwoDFour);
//		    	tSumTwo=(double)tTwoDOne+(double)tTwoDTwo+(double)tTwoDThree+(double)tTwoDFour;
		    	double tTotal;
		    	tTotal=tSumOne+tSumTwo;
		    	String ttSumOne=String.valueOf(tSumOne);
//		    	String ttSumOne=(String)tSumOne;
		    	String ttSumTwo=String.valueOf(tSumTwo);
		    	String ttTotal=String.valueOf(tTotal);
			      
	        texttag.add("SumOne", ttSumOne);
	        texttag.add("SumTwo", ttSumTwo);
	        texttag.add("OneDOne", tOneDOne);
	        texttag.add("OneDTwo", tOneDTwo);
	        texttag.add("OneDThree", tOneDThree);
	        texttag.add("OneDFour",tOneDFour);
	        texttag.add("TwoDOne", tTwoDOne);
	        texttag.add("TwoDTwo", tTwoDTwo);
	        texttag.add("TwoDThree", tTwoDThree);
	        texttag.add("TwoDFour", tTwoDFour);
	        texttag.add("EndInputDate", dateEnd);
	        texttag.add("Total", ttTotal);
	        xmlexport.addTextTag(texttag);

	        mResult.clear();
	        mResult.addElement(xmlexport); //添加到结果集***************************************************************
	        return true;
       
    }
    
    
    //关联方
        private String mOneDOne(String lessThreeMSql){
        	
  	     SSRS tSSRS1 = tExeSQL.execSQL(lessThreeMSql);
   	     beforeDate=tSSRS1.GetText(1,1);
   		 afterDate=dateEnd;
   		 
   		 String localSqlGOne= getFinalSqlOne(dateEnd,lessThreeMSql);
	     System.out.print(localSqlGOne);
   	     SSRS tSSRS2 = tExeSQL.execSQL(localSqlGOne);
   	     if(tSSRS2==null || tSSRS2.MaxRow==0){
   	    	 return null;
   	     }
   	    tOneDOne=tSSRS2.GetText(1,1);
//   	     return String.valueOf(tOneDOne);
   	  return tOneDOne;
    }
        
    private String getFinalSqlOne(String dateEnd, String lessThreeMSql){
    	ExeSQL tExeSQL = new ExeSQL();
    	String beforDate = tExeSQL.execSQL(lessThreeMSql).GetText(1, 1);
    	String localSqlGOne = mSqlGOne;
    	localSqlGOne = localSqlGOne.replaceAll("inputParamBeforData", beforDate);
    	localSqlGOne = localSqlGOne.replaceAll("inputParamAfterData", dateEnd);
    	localSqlGOne = localSqlGOne.replaceAll("inputParamdateEnd", dateEnd);
    	return localSqlGOne;
    }
    
    
    private String mOneDTwo(String betweenTAndSSqlBegin,String betweenTAndSSqlAfter){
    	
 	     SSRS tSSRS1 = tExeSQL.execSQL(betweenTAndSSqlBegin);
  	     beforeDate=tSSRS1.GetText(1,1);
  	     SSRS tSSRS2 = tExeSQL.execSQL(betweenTAndSSqlAfter);
  		 afterDate=tSSRS2.GetText(1,1);
  		 
  		 String localSqlGTwo= getFinalSqlTwo(dateEnd,betweenTAndSSqlBegin,betweenTAndSSqlAfter);
  	     SSRS tSSRS3 = tExeSQL.execSQL(localSqlGTwo);
  	     if(tSSRS3==null || tSSRS3.MaxRow==0){
  	    	 return null;
  	     }
  	    tOneDTwo=tSSRS3.GetText(1,1);
//  	     return String.valueOf(tOneDOne);
  	  return tOneDTwo;
   }
    private String getFinalSqlTwo(String dateEnd, String betweenTAndSSqlBegin,String betweenTAndSSqlAfter){
    	ExeSQL tExeSQL = new ExeSQL();
    	String beforDate = tExeSQL.execSQL(betweenTAndSSqlBegin).GetText(1, 1);
    	String afterDate = tExeSQL.execSQL(betweenTAndSSqlAfter).GetText(1, 1);
    	String localSqlGTwo = mSqlGOne;
    	localSqlGTwo = localSqlGTwo.replaceAll("inputParamBeforData", beforDate);
    	localSqlGTwo = localSqlGTwo.replaceAll("inputParamAfterData", afterDate);
    	localSqlGTwo = localSqlGTwo.replaceAll("inputParamdateEnd", dateEnd);
    	return localSqlGTwo;
    }
    
    
    
    private String mOneDThree(String betweenSandDSqlBegin,String betweenSandDSqlAfter){
    	
	     SSRS tSSRS1 = tExeSQL.execSQL(betweenSandDSqlBegin);
 	     beforeDate=tSSRS1.GetText(1,1);
 	     SSRS tSSRS2 = tExeSQL.execSQL(betweenSandDSqlAfter);
 		 afterDate=tSSRS2.GetText(1,1);
 		 
 		 String localSqlGThree= getFinalSqlThree(dateEnd,betweenSandDSqlBegin,betweenSandDSqlAfter);
 	     SSRS tSSRS3 = tExeSQL.execSQL(localSqlGThree);
 	     if(tSSRS3==null || tSSRS3.MaxRow==0){
 	    	 return null;
 	     }
 	    tOneDThree=tSSRS3.GetText(1,1);
// 	     return String.valueOf(tOneDOne);
 	  return tOneDThree;
  }
    private String getFinalSqlThree(String dateEnd, String betweenSandDSqlBegin,String betweenSandDSqlAfter){
    	ExeSQL tExeSQL = new ExeSQL();
    	String beforDate = tExeSQL.execSQL(betweenSandDSqlBegin).GetText(1, 1);
    	String afterDate = tExeSQL.execSQL(betweenSandDSqlAfter).GetText(1, 1);
    	String localSqlGThree = mSqlGOne;
    	localSqlGThree = localSqlGThree.replaceAll("inputParamBeforData", beforDate);
    	localSqlGThree = localSqlGThree.replaceAll("inputParamAfterData", afterDate);
    	localSqlGThree = localSqlGThree.replaceAll("inputParamdateEnd", dateEnd);
    	return localSqlGThree;
    }
 
    
    
    private String mOneDFour(String moreThanDecSql,String moreThanDecSqlAfter){
    	
	     SSRS tSSRS1 = tExeSQL.execSQL(moreThanDecSql);
 	     beforeDate=tSSRS1.GetText(1,1);
 	     SSRS tSSRS2 = tExeSQL.execSQL(moreThanDecSqlAfter);
 		 afterDate=tSSRS2.GetText(1,1);
 		 
 		 String localSqlGThree= getFinalSqlFour(dateEnd,moreThanDecSql,moreThanDecSqlAfter);
 	     SSRS tSSRS3 = tExeSQL.execSQL(localSqlGThree);
 	     if(tSSRS3==null || tSSRS3.MaxRow==0){
 	    	 return null;
 	     }
 	    tOneDThree=tSSRS3.GetText(1,1);
   	    return tOneDThree;
  }
    
    private String getFinalSqlFour(String dateEnd, String moreThanDecSql,String moreThanDecSqlAfter){
    	ExeSQL tExeSQL = new ExeSQL();
    	String beforDate = tExeSQL.execSQL(moreThanDecSql).GetText(1, 1);
    	String afterDate = tExeSQL.execSQL(moreThanDecSqlAfter).GetText(1, 1);
    	String localSqlGFour = mSqlGOne;
    	localSqlGFour = localSqlGFour.replaceAll("inputParamBeforData", beforDate);
    	localSqlGFour = localSqlGFour.replaceAll("inputParamAfterData", afterDate);
    	localSqlGFour = localSqlGFour.replaceAll("inputParamdateEnd", dateEnd);
    	return localSqlGFour;
    }
    
    
    
    
    //非关联方
    private String mTwoDOne(String lessThreeMSql){
    	
 	     SSRS tSSRS1 = tExeSQL.execSQL(lessThreeMSql);
  	     beforeDate=tSSRS1.GetText(1,1);
  		 afterDate=dateEnd;
  		 
  		 String localSqlGOne= getFinalSqlTOne(dateEnd,lessThreeMSql);
	     System.out.print(localSqlGOne);
  	     SSRS tSSRS2 = tExeSQL.execSQL(localSqlGOne);
  	     if(tSSRS2==null || tSSRS2.MaxRow==0){
  	    	 return null;
  	     }
  	    tOneDOne=tSSRS2.GetText(1,1);
//  	     return String.valueOf(tOneDOne);
  	  return tOneDOne;
   }
       
   private String getFinalSqlTOne(String dateEnd, String lessThreeMSql){
   	ExeSQL tExeSQL = new ExeSQL();
   	String beforDate = tExeSQL.execSQL(lessThreeMSql).GetText(1, 1);
   	String localSqlGOne = mSqlNGOne;
   	localSqlGOne = localSqlGOne.replaceAll("inputParamBeforData", beforDate);
   	localSqlGOne = localSqlGOne.replaceAll("inputParamAfterData", dateEnd);
   	localSqlGOne = localSqlGOne.replaceAll("inputParamdateEnd", dateEnd);
   	return localSqlGOne;
   }
   
   
   private String mTwoDTwo(String betweenTAndSSqlBegin,String betweenTAndSSqlAfter){
   	
	     SSRS tSSRS1 = tExeSQL.execSQL(betweenTAndSSqlBegin);
 	     beforeDate=tSSRS1.GetText(1,1);
 	     SSRS tSSRS2 = tExeSQL.execSQL(betweenTAndSSqlAfter);
 		 afterDate=tSSRS2.GetText(1,1);
 		 
 		 String localSqlGTwo= getFinalSqlTTwo(dateEnd,betweenTAndSSqlBegin,betweenTAndSSqlAfter);
 	     SSRS tSSRS3 = tExeSQL.execSQL(localSqlGTwo);
 	     if(tSSRS3==null || tSSRS3.MaxRow==0){
 	    	 return null;
 	     }
 	    tOneDTwo=tSSRS3.GetText(1,1);
// 	     return String.valueOf(tOneDOne);
 	  return tOneDTwo;
  }
   private String getFinalSqlTTwo(String dateEnd, String betweenTAndSSqlBegin,String betweenTAndSSqlAfter){
   	ExeSQL tExeSQL = new ExeSQL();
   	String beforDate = tExeSQL.execSQL(betweenTAndSSqlBegin).GetText(1, 1);
   	String afterDate = tExeSQL.execSQL(betweenTAndSSqlAfter).GetText(1, 1);
   	String localSqlGTwo = mSqlNGOne;
   	localSqlGTwo = localSqlGTwo.replaceAll("inputParamBeforData", beforDate);
   	localSqlGTwo = localSqlGTwo.replaceAll("inputParamAfterData", afterDate);
   	localSqlGTwo = localSqlGTwo.replaceAll("inputParamdateEnd", dateEnd);
   	return localSqlGTwo;
   }
   
   
   
   private String mTwoDThree(String betweenSandDSqlBegin,String betweenSandDSqlAfter){
   	
	     SSRS tSSRS1 = tExeSQL.execSQL(betweenSandDSqlBegin);
	     beforeDate=tSSRS1.GetText(1,1);
	     SSRS tSSRS2 = tExeSQL.execSQL(betweenSandDSqlAfter);
		 afterDate=tSSRS2.GetText(1,1);
		 
		 String localSqlGThree= getFinalSqlTThree(dateEnd,betweenSandDSqlBegin,betweenSandDSqlAfter);
	     SSRS tSSRS3 = tExeSQL.execSQL(localSqlGThree);
	     if(tSSRS3==null || tSSRS3.MaxRow==0){
	    	 return null;
	     }
	    tOneDThree=tSSRS3.GetText(1,1);
//	     return String.valueOf(tOneDOne);
	  return tOneDThree;
 }
   private String getFinalSqlTThree(String dateEnd, String betweenSandDSqlBegin,String betweenSandDSqlAfter){
   	ExeSQL tExeSQL = new ExeSQL();
   	String beforDate = tExeSQL.execSQL(betweenSandDSqlBegin).GetText(1, 1);
   	String afterDate = tExeSQL.execSQL(betweenSandDSqlAfter).GetText(1, 1);
   	String localSqlGThree = mSqlNGOne;
   	localSqlGThree = localSqlGThree.replaceAll("inputParamBeforData", beforDate);
   	localSqlGThree = localSqlGThree.replaceAll("inputParamAfterData", afterDate);
   	localSqlGThree = localSqlGThree.replaceAll("inputParamdateEnd", dateEnd);
   	return localSqlGThree;
   }

   
   
   private String mTwoDFour(String moreThanDecSql,String moreThanDecSqlAfter){
   	
	     SSRS tSSRS1 = tExeSQL.execSQL(moreThanDecSql);
	     beforeDate=tSSRS1.GetText(1,1);
	     SSRS tSSRS2 = tExeSQL.execSQL(moreThanDecSqlAfter);
		 afterDate=tSSRS2.GetText(1,1);
		 
		 String localSqlGThree= getFinalSqlTFour(dateEnd,moreThanDecSql,moreThanDecSqlAfter);
	     SSRS tSSRS3 = tExeSQL.execSQL(localSqlGThree);
	     if(tSSRS3==null || tSSRS3.MaxRow==0){
	    	 return null;
	     }
	    tOneDThree=tSSRS3.GetText(1,1);
  	    return tOneDThree;
 }
   
   private String getFinalSqlTFour(String dateEnd, String moreThanDecSql,String moreThanDecSqlAfter){
   	ExeSQL tExeSQL = new ExeSQL();
   	String beforDate = tExeSQL.execSQL(moreThanDecSql).GetText(1, 1);
   	String afterDate = tExeSQL.execSQL(moreThanDecSqlAfter).GetText(1, 1);
   	String localSqlGFour = mSqlNGOne;
   	localSqlGFour = localSqlGFour.replaceAll("inputParamBeforData", beforDate);
   	localSqlGFour = localSqlGFour.replaceAll("inputParamAfterData", afterDate);
   	localSqlGFour = localSqlGFour.replaceAll("inputParamdateEnd", dateEnd);
   	return localSqlGFour;
   }
 
   
   
   
//    private String getFinalSqlTOne(String dateEnd, String lessThreeMSql){
//    	ExeSQL tExeSQL = new ExeSQL();
//    	String beforDate = tExeSQL.execSQL(lessThreeMSql).GetText(1, 1);
//    	String localSqlGOne = mSqlGOne;
//    	localSqlGOne = localSqlGOne.replaceAll("inputParamBeforData", beforDate);
//    	localSqlGOne = localSqlGOne.replaceAll("inputParamAfterData", dateEnd);
//    	localSqlGOne = localSqlGOne.replaceAll("inputParamdateEnd", dateEnd);
//    	return localSqlGOne;
//    }
//    private String getFinalSqlTTwo(String dateEnd, String lessThreeMSql){
//    	ExeSQL tExeSQL = new ExeSQL();
//    	String beforDate = tExeSQL.execSQL(lessThreeMSql).GetText(1, 1);
//    	String localSqlGOne = mSqlGOne;
//    	localSqlGOne = localSqlGOne.replaceAll("inputParamBeforData", beforDate);
//    	localSqlGOne = localSqlGOne.replaceAll("inputParamAfterData", dateEnd);
//    	localSqlGOne = localSqlGOne.replaceAll("inputParamdateEnd", dateEnd);
//    	return localSqlGOne;
//    }
//    private String getFinalSqlTThree(String dateEnd, String lessThreeMSql){
//    	ExeSQL tExeSQL = new ExeSQL();
//    	String beforDate = tExeSQL.execSQL(lessThreeMSql).GetText(1, 1);
//    	String localSqlGOne = mSqlGOne;
//    	localSqlGOne = localSqlGOne.replaceAll("inputParamBeforData", beforDate);
//    	localSqlGOne = localSqlGOne.replaceAll("inputParamAfterData", dateEnd);
//    	localSqlGOne = localSqlGOne.replaceAll("inputParamdateEnd", dateEnd);
//    	return localSqlGOne;
//    }
//    private String getFinalSqlTFour(String dateEnd, String lessThreeMSql){
//    	ExeSQL tExeSQL = new ExeSQL();
//    	String beforDate = tExeSQL.execSQL(lessThreeMSql).GetText(1, 1);
//    	String localSqlGOne = mSqlGOne;
//    	localSqlGOne = localSqlGOne.replaceAll("inputParamBeforData", beforDate);
//    	localSqlGOne = localSqlGOne.replaceAll("inputParamAfterData", dateEnd);
//    	localSqlGOne = localSqlGOne.replaceAll("inputParamdateEnd", dateEnd);
//    	return localSqlGOne;
//    }
//    
    
    
    
   
//        private String mOneDTwo(String betweenTAndSSqlBegin,String betweenTAndSSqlAfter, String mSqlGOne){
//        	   SSRS tSSRS21 = tExeSQL.execSQL(betweenTAndSSqlBegin);
//		       beforeDate=tSSRS21.GetText(1,1);
//		       SSRS tSSRS22 = tExeSQL.execSQL(betweenTAndSSqlAfter);
//		       afterDate=tSSRS22.GetText(1,1);
//		       SSRS tSSRS23 = tExeSQL.execSQL(mSqlGOne);
//		       tOneDTwo=tSSRS23.GetText(1,1);	
//		       return tOneDTwo;
//        }
//        
//        private String mOneDThree(String betweenSandDSqlBegin, String betweenSandDSqlAfter, String mSqlGOne){
//        	   SSRS tSSRS31 = tExeSQL.execSQL(betweenSandDSqlBegin);
//			   beforeDate=tSSRS31.GetText(1,1);
//			   SSRS tSSRS32 = tExeSQL.execSQL(betweenSandDSqlAfter);
//			   afterDate=tSSRS32.GetText(1,1);
//			   SSRS tSSRS33 = tExeSQL.execSQL(mSqlGOne);
//			   tOneDThree=tSSRS33.GetText(1,1);
//			   return tOneDThree;
//        }
//  
//        private String mOneDFour(String moreThanDecSql, String moreThanDecSqlAfter, String mSqlGOne){
//        	    SSRS tSSRS41 = tExeSQL.execSQL(moreThanDecSql);
//		   	    beforeDate=tSSRS41.GetText(1,1);
//		   	    SSRS tSSRS42 = tExeSQL.execSQL(moreThanDecSqlAfter);
//		   	    afterDate=tSSRS42.GetText(1,1);
//		   	    SSRS tSSRS43 = tExeSQL.execSQL(mSqlGOne);
//		   	    tOneDFour=tSSRS43.GetText(1,1);
//		   	   return tOneDFour;
//		   	    
//        }
//        private String mTwoDOne(String lessThreeMSql, String mSqlNGOne){
//        	SSRS tSSRS51 = tExeSQL.execSQL(lessThreeMSql);
//	        beforeDate=tSSRS51.GetText(1,1);
//	   		 afterDate=dateEnd;
//	        SSRS tSSRS52 = tExeSQL.execSQL(mSqlNGOne);
//	       tTwoDOne=tSSRS52.GetText(1,1);
//	       return tTwoDOne;
//        }
//        
//        private String mTwoDTwo(String betweenTAndSSqlBegin, String betweenTAndSSqlAfter, String mSqlNGOne){
//        	 SSRS tSSRS61 = tExeSQL.execSQL(betweenTAndSSqlBegin);
//		        beforeDate=tSSRS61.GetText(1,1);
//		        SSRS tSSRS62 = tExeSQL.execSQL(betweenTAndSSqlAfter);
//		        afterDate=tSSRS62.GetText(1,1);
//		        SSRS tSSRS63 = tExeSQL.execSQL(mSqlNGOne);
//		        tTwoDTwo=tSSRS63.GetText(1,1);
//		        return tTwoDTwo;
//        }
//        private String mTwoDThree(String betweenSandDSqlBegin,String betweenSandDSqlAfter, String mSqlNGOne){
//        	SSRS tSSRS71 = tExeSQL.execSQL(betweenSandDSqlBegin);
//	   	    beforeDate=tSSRS71.GetText(1,1);
//	   	    SSRS tSSRS72 = tExeSQL.execSQL(betweenSandDSqlAfter);
//	   	    afterDate=tSSRS72.GetText(1,1);
//	   	    SSRS tSSRS73 = tExeSQL.execSQL(mSqlNGOne);
//	   	    tTwoDThree=tSSRS73.GetText(1,1);
//	   	    
//	   	    return String.valueOf(tTwoDThree);
//        	
//        }
//        
//        private String mTwoDFour(String moreThanDecSql, String moreThanDecSqlAfter, String mSqlNGOne){
//		   	  SSRS tSSRS81 = tExeSQL.execSQL(moreThanDecSql);
//		   	  beforeDate=tSSRS81.GetText(1,1);
//		      SSRS tSSRS82 = tExeSQL.execSQL(moreThanDecSqlAfter);
//		      afterDate=tSSRS82.GetText(1,1);
//		      SSRS tSSRS83 = tExeSQL.execSQL(mSqlNGOne);
//		      tTwoDFour=tSSRS83.GetText(1,1);
//		      
//		      return String.valueOf(tTwoDFour);
//        }
}		
       