package com.sinosoft.lis.finfee;

import com.sinosoft.lis.db.LOPRTInvoiceManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LOPRTInvoiceManagerSchema;
import com.sinosoft.lis.vschema.LOPRTInvoiceManagerSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 作废发票信息录入</p>
 * <p>Copyright: Copyright (c) 2011</p>
 * <p>Company: Sinosoft</p>
 * @author Zcx
 * @version 1.0
 */

public class FFInvoiceNullifyBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    private LOPRTInvoiceManagerSchema tLOPRTInvoiceManagerSchema = new LOPRTInvoiceManagerSchema();

    private LOPRTInvoiceManagerSet inLOPRTInvoiceManagerSet = new LOPRTInvoiceManagerSet();

    private GlobalInput tG = new GlobalInput();

    private String tOperate = "";

    private TransferData tTransferData = new TransferData();

    public FFInvoiceNullifyBL()
    {
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("---End getInputData---");

        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        System.out.println("---End dealData---");
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        System.out.println("---End prepareOutputData---");

        return true;
    }

    /**
     * 根据前面的输入数据，进行逻辑处理
     * @return 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        try
        {
            LOPRTInvoiceManagerDB tLOPRTInvoiceManagerDB = new LOPRTInvoiceManagerDB();
            String comCode = tLOPRTInvoiceManagerSchema.getComCode();
            String invoiceNo = tLOPRTInvoiceManagerSchema.getInvoiceNo();
            String invoiceCode = tLOPRTInvoiceManagerSchema.getInvoiceCode();
            String invoiceNoS = (String) tTransferData
                    .getValueByName("InvoiceStartNo");
            String invoiceNoE = (String) tTransferData
                    .getValueByName("InvoiceEndNo");
            if (Integer.parseInt(invoiceNo) < Integer.parseInt(invoiceNoS)
                    || Integer.parseInt(invoiceNo) > Integer
                            .parseInt(invoiceNoE))
            {
                System.out.println(Integer.parseInt(invoiceNo) + " "
                        + Integer.parseInt(invoiceNoS) + " "
                        + Integer.parseInt(invoiceNoE));
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "FFInvoiceNullifyBL";
                tError.functionName = "dealData";
                tError.errorMessage = "  输入的发票号码不在发票信息范围内！";
                this.mErrors.addOneError(tError);
                return false;
            }

            String sql = "select * from LOPRTInvoiceManager where InvoiceNo='"
                    + invoiceNo + "' and InvoiceCode='" + invoiceCode + "' ";
            if (comCode.trim().length() < 4)
            {
                sql = sql + "and comcode='" + comCode + "'";
            }
            else
            {
                sql = sql + "and comcode like '" + comCode.substring(0, 4)
                        + "%'";
            }
            sql = sql + " with ur";
            System.out.println("LOPRTInvoiceManager:" + sql);
            LOPRTInvoiceManagerSet tLOPRTInvoiceManagerSet = tLOPRTInvoiceManagerDB
                    .executeQuery(sql);
            if (tLOPRTInvoiceManagerSet.size() == 0)
            {
                System.out.println("未发现发票打印数据");
                setLOPRTInvoiceManager();
            }
            else
            {
                System.out.println("存在发票打印数据");
                setUpdateLOPRTInvoiceManager(tLOPRTInvoiceManagerSet);
            }
        }
        catch (Exception e)
        {
            System.out.println("数据处理错误!--dealData");
            e.printStackTrace();
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "FFInvoiceNullifyBL";
            tError.functionName = "dealData";
            tError.errorMessage = "数据处理错误!" + e.getMessage();
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    private boolean setLOPRTInvoiceManager()
    {
        tOperate = "INSERT";
        tLOPRTInvoiceManagerSchema.setMakeDate(PubFun.getCurrentDate());
        tLOPRTInvoiceManagerSchema.setMakeTime(PubFun.getCurrentTime());
        tLOPRTInvoiceManagerSchema.setModifyDate(PubFun.getCurrentDate());
        tLOPRTInvoiceManagerSchema.setModifyTime(PubFun.getCurrentTime());
        tLOPRTInvoiceManagerSchema.setOperator(tG.Operator);
        tLOPRTInvoiceManagerSchema.setOpdate(PubFun.getCurrentDate());
        inLOPRTInvoiceManagerSet.add(tLOPRTInvoiceManagerSchema);
        return true;
    }

    private boolean setUpdateLOPRTInvoiceManager(
            LOPRTInvoiceManagerSet tLOPRTInvoiceManagerSet)
    {
        tOperate = "UPDATE";
        LOPRTInvoiceManagerSchema mLOPRTInvoiceManagerSchema = tLOPRTInvoiceManagerSet
                .get(1);
        mLOPRTInvoiceManagerSchema.setStateFlag("2");
        mLOPRTInvoiceManagerSchema.setModifyDate(PubFun.getCurrentDate());
        mLOPRTInvoiceManagerSchema.setModifyTime(PubFun.getCurrentTime());
        mLOPRTInvoiceManagerSchema.setOperator(tG.Operator);
        inLOPRTInvoiceManagerSet.add(mLOPRTInvoiceManagerSchema);
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData)
    {
        tLOPRTInvoiceManagerSchema = (LOPRTInvoiceManagerSchema) mInputData
                .getObjectByObjectName("LOPRTInvoiceManagerSchema", 0);
        tTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        tG = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);

        if (tLOPRTInvoiceManagerSchema == null || tG == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "FFInvoiceNullifyBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData()
    {
        mInputData = new VData();
        MMap outMap = new MMap();
        try
        {
            System.out.println("----before submit----");
            outMap.put(inLOPRTInvoiceManagerSet, tOperate);
            mInputData.add(outMap);
            if (tOperate.equals("INSERT"))
            {
                PubSubmit tPubSubmit = new PubSubmit();
                if (!tPubSubmit.submitData(mInputData, ""))
                {
                    CError.buildErr(this, "作废信息更新失败", tPubSubmit.mErrors);
                    return false;
                }
            }
            //PubSubmit在update时不使用comcode，无奈。。
            else
            {
                String sql = "Update LOPRTInvoiceManager Set Stateflag='2',modifydate='"
                        + inLOPRTInvoiceManagerSet.get(1).getModifyDate()
                        + "',modifytime='"
                        + inLOPRTInvoiceManagerSet.get(1).getModifyTime()
                        + "',operator='"
                        + inLOPRTInvoiceManagerSet.get(1).getOperator()
                        + "',Invoiceno = '"
                        + inLOPRTInvoiceManagerSet.get(1).getInvoiceNo()
                        + "',Invoicecode = '"
                        + inLOPRTInvoiceManagerSet.get(1).getInvoiceCode()
                        + "',Comcode = '"
                        + inLOPRTInvoiceManagerSet.get(1).getComCode()
                        + "' Where Invoiceno = '"
                        + inLOPRTInvoiceManagerSet.get(1).getInvoiceNo()
                        + "' And Invoicecode = '"
                        + inLOPRTInvoiceManagerSet.get(1).getInvoiceCode()
                        + "' And Comcode = '"
                        + inLOPRTInvoiceManagerSet.get(1).getComCode() + "'";
                ExeSQL tExeSQL = new ExeSQL();
                System.out.println(sql);
                if(!tExeSQL.execUpdateSQL(sql)){
//                  @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "FFInvoiceNullifyBL";
                    tError.functionName = "prepareOutputData";
                    tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
                    this.mErrors.addOneError(tError);
                    return false;
                }
            }
            System.out.println("----after submit----");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "FFInvoiceNullifyBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 数据输出方法，供外界获取数据处理结果
     * @return 包含有数据查询结果字符串的VData对象
     */
    public VData getResult()
    {
        return mResult;
    }

    public static void main(String[] args)
    {
    }
}
