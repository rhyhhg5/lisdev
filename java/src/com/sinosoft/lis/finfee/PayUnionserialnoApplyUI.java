package com.sinosoft.lis.finfee;

import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class PayUnionserialnoApplyUI {


	  //业务处理相关变量	
	  private VData cInputData ;
	  public CErrors mErrors = new CErrors();
	  PayUnionserialnoApplyBL tPayUnionserialnoApplyBL =new PayUnionserialnoApplyBL();
	  public PayUnionserialnoApplyUI() {
	  }
	  public boolean submitData(VData cInputData, String cOperate){
		  
		  this.cInputData = (VData)cInputData.clone();
		  
		  
		 
		  if (!tPayUnionserialnoApplyBL.submitData(cInputData,cOperate))
	      {
	         // @@错误处理
	        this.mErrors.copyAllErrors(tPayUnionserialnoApplyBL.mErrors);
	        CError tError = new CError();
	        tError.moduleName = "PayUnionserialnoApplyUI";
	        tError.functionName = "submitData";
	        tError.errorMessage = "数据查询失败!";
	        this.mErrors .addOneError(tError) ;
	        return false;
	      }
	  return true;  
	  }
	  public String getHZSerialno(){
		  return tPayUnionserialnoApplyBL.getmSerialnoHZ();
	  }
}
