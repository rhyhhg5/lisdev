/**
 * 2011-10-22
 */
package com.sinosoft.lis.finfee;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import com.sinosoft.lis.db.LJInvoiceInfoDB;
import com.sinosoft.lis.db.LOPRTInvoiceManagerDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LJInvoiceInfoSet;
import com.sinosoft.lis.vschema.LOPRTInvoiceManagerSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author Zcx
 * 
 */
public class FFIvoiceExportDLBL implements InvoiceExport {
	public CErrors mErrors = new CErrors();

	private GlobalInput mGlobalInput = null;

	private ArrayList mResult = new ArrayList();

	private String mBasePath = "";

	private String mOutPath = "";

	private String mSsqq = "";

	private String mSsqz = "";

	private String mSFState = "";

	private String fileName = "";

	private String TaxpayerNo = "";

	private String TaxpayerName = "";
	
	private String certifyCode = "";

	private LOPRTInvoiceManagerSet tLOPRTInvoiceManagerSet = new LOPRTInvoiceManagerSet();

	public boolean submitData(VData cInputData, String cOperate) {
		if (!getInputData(cInputData)) {
			buildError("getInputData", "没有获取到足够业务信息或者机构不对");
			return false;
		}
		if (!dealData()) {
			buildError("dealData", "生成txt文件失败");
			return false;
		}
		return true;
	}

	/**
	 * 获取业务数据
	 * 
	 * @param cInputData
	 * @return
	 */
	private boolean getInputData(VData cInputData) {
		TransferData tParameters = (TransferData) cInputData
				.getObjectByObjectName("TransferData", 0);
		mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0);

		if (mGlobalInput == null || tParameters == null) {
			return false;
		}

		this.mBasePath = (String) tParameters.getValueByName("hostBasePath")
				+ "/";
		this.mOutPath = (String) tParameters.getValueByName("outPath") + "/";

		this.mSsqq = (String) tParameters.getValueByName("invoiceStartDate");
		this.mSsqz = (String) tParameters.getValueByName("invoiceEndDate");
		this.mSFState = (String) tParameters.getValueByName("SFState");
		this.certifyCode = (String) tParameters.getValueByName("CertifyCode");
		// 获取条件内的相关发票打印信息
		String tSql = "";
		
		// 判断收付费
		if ("1".equals(mSFState)) {
			tSql = "select * from LOPRTInvoiceManager a "
					+ " where 1 = 1 "
					+ " and exists (select 1 from loprtmanager2 where code='37' and standbyflag4=a.invoiceno and makedate=a.makedate and comcode=a.comcode)"
					+ " and comcode like '" + mGlobalInput.ManageCom + "%'"
					+ " and makedate >= '" + mSsqq + "' "
					+ " and makedate <= '" + mSsqz + "'";
		} else {
			tSql = "select * from LOPRTInvoiceManager a "
					+ " where 1 = 1 "
					+ " and not exists (select 1 from loprtmanager2 where code='37' and standbyflag4=a.invoiceno and makedate=a.makedate and comcode=a.comcode)"
					+ " and comcode like '" + mGlobalInput.ManageCom + "%'"
					+ " and makedate >= '" + mSsqq + "' "
					+ " and makedate <= '" + mSsqz + "'";
		}
		
		if(this.certifyCode != null && !"".equals(this.certifyCode)){
			tSql = tSql + " and memo='" + certifyCode + "'";
		}
		
		LOPRTInvoiceManagerDB tLOPRTInvoiceManagerDB = new LOPRTInvoiceManagerDB();
		tLOPRTInvoiceManagerSet = tLOPRTInvoiceManagerDB.executeQuery(tSql);
		System.out.println(tSql);
		if (tLOPRTInvoiceManagerSet == null
				|| tLOPRTInvoiceManagerSet.size() == 0) {
			return false;
		}

		tSql = "select * from LJInvoiceInfo where comcode = '"
				+ mGlobalInput.ManageCom + "' " + " and invoicecode='"
				+ tLOPRTInvoiceManagerSet.get(1).getInvoiceCode() + "'"
				+ " order by makedate desc fetch first 1 rows only with ur ";
		System.out.println(tSql);
		LJInvoiceInfoDB tLJInvoiceInfoDB = new LJInvoiceInfoDB();
		LJInvoiceInfoSet tLJInvoiceInfoSet = tLJInvoiceInfoDB
				.executeQuery(tSql);
		if (tLJInvoiceInfoSet == null || tLJInvoiceInfoSet.size() <= 0) {
			return false;
		}
		// 获取纳税人名称、识别号
		this.TaxpayerNo = tLJInvoiceInfoSet.get(1).getTaxpayerNo();
		this.TaxpayerName = tLJInvoiceInfoSet.get(1).getTaxpayerName();

		return true;
	}

	private boolean dealData() {
		System.out.println("export txt begin...");
		try {
			String write = tLOPRTInvoiceManagerSet.size() + "\r\n";
			for (int i = 1; i <= tLOPRTInvoiceManagerSet.size(); i++) {
				// 发票代码
				write = write + tLOPRTInvoiceManagerSet.get(i).getInvoiceCode()
						+ "\t";
				// 发票号码
				write = write + tLOPRTInvoiceManagerSet.get(i).getInvoiceNo()
						+ "\t";
				// 发票状态
				if (tLOPRTInvoiceManagerSet.get(i).getStateFlag().equals("0")) {
					write = write + "10\t";
				} else {
					write = write + "11\t";
				}
				// 收款单位名称
				write = write + this.TaxpayerName + "\t";
				// 收款单位识别号
				write = write + this.TaxpayerNo + "\t";
				// 付款方名称
				write = write + tLOPRTInvoiceManagerSet.get(i).getPayerName() + "\t";
				// 付款方证件号码
				write = write + "\t";
				// 开票日期
				write = write + tLOPRTInvoiceManagerSet.get(i).getOpdate() + " " + tLOPRTInvoiceManagerSet.get(i).getMakeTime() + ".0\t";
				// 合计金额（小写）
				write = write + PubFun.setPrecision1(tLOPRTInvoiceManagerSet.get(i).getXSumMoney(), "0.00") + "\t";
				
				String sql = "select username from lduser where usercode='" + tLOPRTInvoiceManagerSet.get(i).getOperator() + "'";
				ExeSQL tExeSQL = new ExeSQL();
				// 开票人
				write = write + tExeSQL.getOneValue(sql) + "\t";
				// 收款人
				//write = write + "\t";
				// 项目一内容
				write = write + "保险费\t";
				// 项目一金额
				write = write + PubFun.setPrecision1(tLOPRTInvoiceManagerSet.get(i).getXSumMoney(), "0.00") + "\t";
				// 项目二内容
				write = write + "\t";
				// 项目二金额
				write = write + "\t";
				// 项目三内容
				write = write + "\t";
				// 项目三金额
				write = write + "\t";
				// 项目四内容
				write = write + "\t";
				// 项目四金额
				write = write + "\t";
				// 项目五内容
				write = write + "\t";
				// 项目五金额
				write = write + "\t";
				// 项目六内容
				write = write + "\t";
				// 项目六金额
				write = write + "\t";
				// 项目七内容
				write = write + "\t";
				// 项目七金额
				write = write + "\t";
				// 项目八内容
				write = write + "\t";
				// 项目八金额
				write = write + "\t";
				// 项目九内容
				write = write + "\t";
				// 项目九金额
				write = write + "\t";
				// 项目十内容
				write = write + "\t";
				// 项目十金额
				write = write + "\r\n";
			}
			fileName = this.TaxpayerNo + PubFun.getCurrentDate2() + "00.txt";
			FileWriter fw = new FileWriter(mBasePath + mOutPath + fileName);
			fw.write(write);
			fw.close();
			System.out.println("导入文件：" + mBasePath + mOutPath + fileName);
			mResult.add(mOutPath + fileName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		System.out.println("export xml finished...");
		return true;
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "FFIvoiceExportDLBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public List getResult() {
		return mResult;
	}

	public CErrors getErrors() {
		return mErrors;
	}

	public static void main(String[] args) {
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}