/**
 * 2007-5-22
 */
package com.sinosoft.lis.finfee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.sinosoft.lis.db.LJInvoiceInfoDB;
import com.sinosoft.lis.db.LOPRTInvoiceManagerDB;
import com.sinosoft.lis.finfee.invoiceprtxml.InvoicePrtXmlException;
import com.sinosoft.lis.finfee.invoiceprtxml.XmlHelper;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.vschema.LJInvoiceInfoSet;
import com.sinosoft.lis.vschema.LOPRTInvoiceManagerSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * @author LY
 *
 */
public class FFIvoiceExportBL implements InvoiceExport
{
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;

    private ArrayList mResult = new ArrayList();

    private String mBasePath = "";

    private String mOutPath = "";

    private String mPrtTemplate = "";

    /** 
     * 纳税人识别号
     */
    private String mNsrsbh = "";

    /**
     * 纳税人名称
     */
    private String mNsrmc = "";

    /**
     * 所属期起
     */
    private String mSsqq = "";

    /**
     * 所属期止
     */
    private String mSsqz = "";

    /**
     * 办理日期
     */
    private String mRq = "";

    /**
     * 收付费发票标记
     */
    private String mSFState = "";
    /**
     * 办理日期
     */
   // private String minvoicecode = "";
    /**
     * 发票开票详细信息（多单）
     * 以hashmap代替结构体，保存每一张发票信息
     */
    private ArrayList mFpList = new ArrayList();

    public boolean submitData(VData cInputData, String cOperate)
    {
        if (!getInputData(cInputData))
        {
            buildError("getInputData", "没有获取到足够业务信息或者机构不对");
            return false;
        }
        if (!exportXML())
        {
            buildError("exportXML", "导出XML文件失败");
            return false;
        }
        return true;
    }

    /**
     * 获取业务数据
     * @param cInputData
     * @return
     */
    private boolean getInputData(VData cInputData)
    {
        TransferData tParameters = (TransferData) cInputData
                .getObjectByObjectName("TransferData", 0);
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        if (mGlobalInput == null || tParameters == null)
        {
            return false;
        }

        this.mBasePath = (String) tParameters.getValueByName("hostBasePath");
        this.mOutPath = (String) tParameters.getValueByName("outPath") + "/";
        this.mPrtTemplate = (String) tParameters.getValueByName("prtTemplate");

        this.mSsqq = (String) tParameters.getValueByName("invoiceStartDate");
        this.mSsqz = (String) tParameters.getValueByName("invoiceEndDate");
        this.mSFState = (String) tParameters.getValueByName("SFState");
        // 获取条件内的相关发票打印信息
        String tSql ="";
        if("1".equals(mSFState)){
        	 tSql = "select * from LOPRTInvoiceManager a " + " where 1 = 1 "
        	 + " and exists (select 1 from loprtmanager2 where code='37' and standbyflag4=a.invoiceno and makedate=a.makedate and comcode=a.comcode)"
             + " and comcode = '" + mGlobalInput.ManageCom + "'"
             + " and makedate >= '" + mSsqq + "' " + " and makedate <= '"
             + mSsqz + "'";
        }else{
        	 tSql = "select * from LOPRTInvoiceManager a " + " where 1 = 1 "
        	    + " and not exists (select 1 from loprtmanager2 where code='37' and standbyflag4=a.invoiceno and makedate=a.makedate and comcode=a.comcode)"
                + " and comcode = '" + mGlobalInput.ManageCom + "'"
                + " and makedate >= '" + mSsqq + "' " + " and makedate <= '"
                + mSsqz + "'";
        }
        LOPRTInvoiceManagerDB tLOPRTInvoiceManagerDB = new LOPRTInvoiceManagerDB();
        LOPRTInvoiceManagerSet tLOPRTInvoiceManagerSet = tLOPRTInvoiceManagerDB
                .executeQuery(tSql);
        System.out.println(tSql);
        if (tLOPRTInvoiceManagerSet == null||tLOPRTInvoiceManagerSet.size()==0)
            return false;
        for (int i = 1; i <= tLOPRTInvoiceManagerSet.size(); i++)
        {
            HashMap subNodeTemp = new HashMap();
            if (tLOPRTInvoiceManagerSet.get(i).getInvoiceCode() != null)
            {
                subNodeTemp.put("InitFpdm", tLOPRTInvoiceManagerSet.get(i)
                        .getInvoiceCode());
                subNodeTemp.put("Fpdm", tLOPRTInvoiceManagerSet.get(i)
                        .getInvoiceCode());
            }
            if (tLOPRTInvoiceManagerSet.get(i).getInvoiceCodeEx() != null)
                subNodeTemp.put("Fpdmext", tLOPRTInvoiceManagerSet.get(i)
                        .getInvoiceCodeEx());
            if (tLOPRTInvoiceManagerSet.get(i).getInvoiceNo() != null)
                subNodeTemp.put("Fphm", tLOPRTInvoiceManagerSet.get(i)
                        .getInvoiceNo());
            if (tLOPRTInvoiceManagerSet.get(i).getStateFlag() != null)
                subNodeTemp.put("Zfbz", tLOPRTInvoiceManagerSet.get(i)
                        .getStateFlag());
            if (tLOPRTInvoiceManagerSet.get(i).getMakeDate() != null)
                subNodeTemp.put("Kprq", tLOPRTInvoiceManagerSet.get(i)
                        .getMakeDate());
            subNodeTemp.put("Kpje", Double.toString(PubFun.setPrecision(
                    tLOPRTInvoiceManagerSet.get(i).getXSumMoney(), "0.00")));
            if (tLOPRTInvoiceManagerSet.get(i).getOperator() != null)
                subNodeTemp.put("Kpr", tLOPRTInvoiceManagerSet.get(i)
                        .getOperator());
            if (tLOPRTInvoiceManagerSet.get(i).getPayerCode() != null)
                subNodeTemp.put("Fkfnsrsbh", tLOPRTInvoiceManagerSet.get(i)
                        .getPayerCode());
            if (tLOPRTInvoiceManagerSet.get(i).getPayerName() != null)
                subNodeTemp.put("Fkfnrsmc", tLOPRTInvoiceManagerSet.get(i)
                        .getPayerName());
            if (tLOPRTInvoiceManagerSet.get(i).getPayerAddr() != null)
                subNodeTemp.put("Fkfaddr", tLOPRTInvoiceManagerSet.get(i)
                        .getPayerAddr());
            if (tLOPRTInvoiceManagerSet.get(i).getPayeeCode() != null)
                subNodeTemp.put("Skfnsrsbh", tLOPRTInvoiceManagerSet.get(i)
                        .getPayeeCode());
            if (tLOPRTInvoiceManagerSet.get(i).getPayeeName() != null)
                subNodeTemp.put("Skfnrsmc", tLOPRTInvoiceManagerSet.get(i)
                        .getPayeeName());
            if (tLOPRTInvoiceManagerSet.get(i).getPayee() != null)
                subNodeTemp.put("Skr", tLOPRTInvoiceManagerSet.get(i)
                        .getPayee());
            if (tLOPRTInvoiceManagerSet.get(i).getMemo() != null)
                subNodeTemp.put("Extmxinfo", tLOPRTInvoiceManagerSet.get(i)
                        .getMemo());

            this.mFpList.add(subNodeTemp);
        }

        tSql = "select * from LJInvoiceInfo where comcode = '"
                + mGlobalInput.ManageCom + "' "
                + " and invoicecode='"+tLOPRTInvoiceManagerSet.get(1).getInvoiceCode()+"'" 
                + " order by makedate desc fetch first 1 rows only with ur ";
        LJInvoiceInfoDB tLJInvoiceInfoDB = new LJInvoiceInfoDB();
        LJInvoiceInfoSet tLJInvoiceInfoSet = tLJInvoiceInfoDB
                .executeQuery(tSql);
        if (tLJInvoiceInfoSet == null || tLJInvoiceInfoSet.size() <= 0)
            return false;

        this.mNsrsbh = tLJInvoiceInfoSet.get(1).getTaxpayerNo();
        this.mNsrmc = tLJInvoiceInfoSet.get(1).getTaxpayerName();
        this.mRq = PubFun.getCurrentDate();
        return true;
    }

    private boolean exportXML()
    {    System.out.println("export xml begin...");
         String outFileName = ""; 
    	if("1".equals(mSFState)){
    	    	outFileName=PubFun.getCurrentDate2() + "F" + mNsrsbh + ".xml"; 
                }else{
                 outFileName=PubFun.getCurrentDate2() + "U" + mNsrsbh + ".xml";   
                }

        try
        {
            XmlHelper xh = new XmlHelper(mPrtTemplate, mBasePath);

            xh.setNodeText("Nsrsbh", mNsrsbh);
            xh.setNodeText("Nsrmc", mNsrmc);
            xh.setNodeText("Ssqq", mSsqq);
            xh.setNodeText("Ssqz", mSsqz);
            xh.setNodeText("Rq", mRq);

            // 清除子结点
            xh.removeSubNode("Fplist", false);

            for (int i = 0; i < mFpList.size(); i++)
            {
                xh.addSubItemValues("Fplist", "Fpinfo", (HashMap) mFpList
                        .get(i));
            }

            xh.write(mOutPath + outFileName);
        }
        catch (InvoicePrtXmlException e)
        {
            e.printStackTrace();
        }
        mResult.add(mOutPath + outFileName);
        System.out.println("created xml files " + mOutPath + outFileName);
        System.out.println("export xml finished...");
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FFIvoiceExportBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public List getResult()
    {
        return mResult;
    }
    
    public CErrors getErrors(){
    	return mErrors;
    }
}
