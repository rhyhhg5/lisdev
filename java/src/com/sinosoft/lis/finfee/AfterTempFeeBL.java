package com.sinosoft.lis.finfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public interface AfterTempFeeBL
{
    public CErrors mErrors = new CErrors();

    public MMap getSubmitMMap(VData cInputData, String cOperate);

//  public TransferData getReturnTransferData();
//  public CErrors getErrors();
}
