package com.sinosoft.lis.finfee;

import java.sql.Connection;
import java.util.GregorianCalendar;

import com.sinosoft.lis.certify.PubCertifyTakeBack;
import com.sinosoft.lis.db.LMCertifyDesDB;
import com.sinosoft.lis.db.LZCardDB;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LJTempFeeClassSchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.vdb.LJSPayDBSet;
import com.sinosoft.lis.vdb.LJTempFeeClassDBSet;
import com.sinosoft.lis.vdb.LJTempFeeDBSet;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.vschema.LJFIGetSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LMCertifyDesSet;
import com.sinosoft.lis.vschema.LZCardSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.VData;

public class FirstPayBL
{

    /**
     * @param args
     */
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    PubSubmit tPubSubmit = new PubSubmit();

    /** 往后面传输数据的容器 */
    private String[] existTempFeeNo;

    private VData mInputData;

    private GlobalInput tGI = new GlobalInput();

    /** 数据操作字符串 */
    private String serNo = ""; //流水号

    private String tLimit = "";

    private String tNo = ""; //生成的暂交费号

    private String mOperate;

    private String CurrentDate = PubFun.getCurrentDate();

    private String CurrentTime = PubFun.getCurrentTime();

    private String mEngagePrtNo = ""; // 记录预打报单交费收据号

    private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();

    private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();

    private LJSPaySet mLJSPaySet = new LJSPaySet();

    public FirstPayBL()
    {

    }

    public static void main(String[] args)
    {
        // TODO Auto-generated method stub
        System.out.println("===");
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("Operate==" + cOperate);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        System.out.println("After getinputdata==" + mLJTempFeeSet.size());
        LJTempFeeSchema sasd = (LJTempFeeSchema) mLJTempFeeSet.get(1);
        System.out.println("ljtempfee.prtSeq(tempfeeno)=" + sasd.getOtherNo());
        if (!prepareOutputData())
            return false;
        System.out.println("---End prepareOutputData---");
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        System.out.println("After dealData！");

        //准备往后台的数据

        System.out.println("After save");

        System.out.println("Start FirstPayBL  Submit...");

        System.out.println("End FirstPayBL Submit...");

        System.out.println("FirstPayBL end");

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData tInputData)
    {
        mLJTempFeeSet = (LJTempFeeSet) tInputData.getObjectByObjectName("LJTempFeeSet", 0);
        mLJTempFeeClassSet = (LJTempFeeClassSet) tInputData.getObjectByObjectName("LJTempFeeClassSet", 0);
        tGI = (GlobalInput) tInputData.getObjectByObjectName("GlobalInput", 0);
        System.out.println("mLJTempFeeSet:" + mLJTempFeeSet.size() + "mLJTempFeeClassSet:" + mLJTempFeeClassSet.size()
                + "tGI:" + tGI.toString());
        if (mLJTempFeeSet == null || mLJTempFeeClassSet == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "FirstPayBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mLJTempFeeSet.size() == 0 || mLJTempFeeClassSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "FirstPayBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean dealData()
    {

        if (!save())
        {
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            System.out.println(tPubSubmit.mErrors);
            return false;
        }
        return true;

    }

    private boolean prepareOutputData()
    {
        LJSPaySchema tLJSPaySchema;
        if (mLJTempFeeSet.size() < 1 || mLJTempFeeClassSet.size() < 1)
        {
            System.out.print("prepareOutputData.errorMessage");
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "FirstPayBL";
            tError.functionName = "prepareOutputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //	LJTempFeeSchema tLJTempFeeSchema =new LJTempFeeSchema();; //财务给付表      
        //   	LJTempFeeClassSchema  tLJTempFeeClassSchema  =new LJTempFeeClassSchema();; //实付总表
        for (int i = 1; i <= mLJTempFeeSet.size(); i++)
        {
            // 判断仅新单才可进行银行转帐方式的先收费业务
            LJTempFeeSchema tTempFeeInfo = mLJTempFeeSet.get(i);
            String tPrtNo = tTempFeeInfo.getOtherNo();
            String tStrSql = "select 1 from LCCont where PrtNo = '" + tPrtNo + "' and AppFlag = '1'";
            String tRes = new ExeSQL().getOneValue(tStrSql);
            if ("1".equals(tRes))
            {
                System.out.print("prepareOutputData.errorMessage");
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "FirstPayBL";
                tError.functionName = "prepareOutputData";
                tError.errorMessage = "[" + tPrtNo + "]存在已承保的保单信息，该功能仅支持新单的先收费业务!";
                this.mErrors.addOneError(tError);
                return false;
            }
            // --------------------

            System.out.println("ljtempfee.prtSeq(tempfeeno)=" + mLJTempFeeSet.get(i).getOtherNo());
            String prtSeq = PubFun1.CreateMaxNo("PAYNOTICENO", mLJTempFeeSet.get(i).getOtherNo());
            System.out.println("ljtempfee.prtSeq(tempfeeno)=" + prtSeq);
            String serNo = PubFun1.CreateMaxNo("SERIALNO", mLJTempFeeSet.get(i).getManageCom());
            System.out.println("ljtempfee.serNo=" + serNo);
            GregorianCalendar Calendar = new GregorianCalendar();
            Calendar.setTime((new FDate()).getDate(PubFun.getCurrentDate()));
            Calendar.add(Calendar.DATE, 0);

            mLJTempFeeSet.get(i).setTempFeeNo(prtSeq);
            mLJTempFeeSet.get(i).setTempFeeType("16");
            mLJTempFeeSet.get(i).setRiskCode("000000");
            mLJTempFeeSet.get(i).setPayDate(Calendar.getTime());
            mLJTempFeeSet.get(i).setSerialNo(serNo);
            mLJTempFeeSet.get(i).setConfFlag("0");
            mLJTempFeeSet.get(i).setMakeDate(CurrentDate);
            mLJTempFeeSet.get(i).setModifyDate(CurrentDate);
            mLJTempFeeSet.get(i).setModifyTime(CurrentTime);
            mLJTempFeeSet.get(i).setMakeTime(CurrentTime);

            mLJTempFeeClassSet.get(i).setTempFeeNo(prtSeq);
            mLJTempFeeClassSet.get(i).setPayMode("4");
            mLJTempFeeClassSet.get(i).setPayDate(Calendar.getTime());
            mLJTempFeeClassSet.get(i).setPolicyCom(mLJTempFeeClassSet.get(i).getManageCom());
            mLJTempFeeClassSet.get(i).setSerialNo(serNo);
            mLJTempFeeClassSet.get(i).setConfFlag("0");
            mLJTempFeeClassSet.get(i).setMakeDate(CurrentDate);
            mLJTempFeeClassSet.get(i).setModifyDate(CurrentDate);
            mLJTempFeeClassSet.get(i).setModifyTime(CurrentTime);
            mLJTempFeeClassSet.get(i).setMakeTime(CurrentTime);

            tLJSPaySchema = new LJSPaySchema();

            tLJSPaySchema.setGetNoticeNo(prtSeq);
            tLJSPaySchema.setOtherNo(mLJTempFeeSet.get(i).getOtherNo());
            tLJSPaySchema.setOtherNoType("16");
            // tLJSPaySchema.setAppntNo(tLCPolSchema.getAppntNo());
            tLJSPaySchema.setPayDate(mLJTempFeeClassSet.get(i).getPayDate());
            tLJSPaySchema.setStartPayDate(mLJTempFeeClassSet.get(i).getPayDate());
            tLJSPaySchema.setBankOnTheWayFlag("0");
            tLJSPaySchema.setBankSuccFlag("0");
            tLJSPaySchema.setSendBankCount(0); //送银行次数
            tLJSPaySchema.setAccName(mLJTempFeeClassSet.get(i).getAccName());
            tLJSPaySchema.setBankCode(mLJTempFeeClassSet.get(i).getBankCode());
            tLJSPaySchema.setBankAccNo(mLJTempFeeClassSet.get(i).getBankAccNo());
            tLJSPaySchema.setSumDuePayMoney(mLJTempFeeClassSet.get(i).getPayMoney());
            tLJSPaySchema.setManageCom(mLJTempFeeClassSet.get(i).getManageCom());
            tLJSPaySchema.setAgentCode(mLJTempFeeSet.get(i).getAgentCode());
            tLJSPaySchema.setSerialNo(serNo); //流水号
            tLJSPaySchema.setOperator(mLJTempFeeSet.get(i).getOperator());
            tLJSPaySchema.setMakeDate(CurrentDate);
            tLJSPaySchema.setMakeTime(CurrentTime);
            tLJSPaySchema.setModifyDate(CurrentDate);
            tLJSPaySchema.setModifyTime(CurrentTime);
            mLJSPaySet.add(tLJSPaySchema);
        }
        return true;
    }

    private boolean save()
    {

        boolean tReturn = true;
        System.out.println("Start Save...");
        Connection conn = DBConnPool.getConnection();

        if (conn == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "FirstPayBL";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        try
        {
            conn.setAutoCommit(false);
            LJTempFeeClassDBSet tLJTempFeeClassDBSet = new LJTempFeeClassDBSet(conn);
            tLJTempFeeClassDBSet.set(mLJTempFeeClassSet);
            if (!tLJTempFeeClassDBSet.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLJTempFeeClassDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "FirstPayBL";
                tError.functionName = "saveData";
                tError.errorMessage = "暂交费表数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            //暂交费表
            System.out.println("Start 暂交费表...");
            LJTempFeeDBSet tLJTempFeeDBSet = new LJTempFeeDBSet(conn);
            tLJTempFeeDBSet.set(mLJTempFeeSet);
            if (!tLJTempFeeDBSet.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLJTempFeeDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "FirstPayBL";
                tError.functionName = "saveData";
                tError.errorMessage = "暂交费表数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }

            //    应收表
            System.out.println("Start 应收表...");
            LJSPayDBSet mLJSPayDBSet = new LJSPayDBSet(conn);
            mLJSPayDBSet.set(mLJSPaySet);
            if (!mLJSPayDBSet.insert())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tLJTempFeeDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "FirstPayBL";
                tError.functionName = "saveData";
                tError.errorMessage = "应收表数据保存失败!";
                this.mErrors.addOneError(tError);
                conn.rollback();
                conn.close();
                return false;
            }
            conn.commit();
            conn.close();
            System.out.println("commit end");
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "TempFeeBLS";
            tError.functionName = "save";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {
            }
            tReturn = false;
        }
        return tReturn;
    }

}
