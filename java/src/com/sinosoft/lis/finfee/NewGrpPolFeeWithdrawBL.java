package com.sinosoft.lis.finfee;

import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;
import java.util.*;
import java.sql.*;
import java.lang.String;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class NewGrpPolFeeWithdrawBL  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 数据操作字符串 */
  private GlobalInput mGI=new GlobalInput();
  private String mOperate;
  private String tLimit="";
  private String serNo="";//流水号
  private String getNo="";//给付收据号码
  private String prtSeqNo="";//印刷流水号
  private boolean specWithDrawFlag=false; //特殊的退费标记。该标记为真时，不考虑outpayflag标志
  private TransferData tTransferData= new TransferData();

  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private LJAGetSchema         mLJAGetSchema       = new LJAGetSchema();//实付总表
  private LJAGetSet            mLJAGetSet          = new LJAGetSet();

  private LCGrpPolSchema          mLCGrpPolSchema        = new LCGrpPolSchema();//集体保单表
  private LCGrpPolSet             mLCGrpPolSet           = new LCGrpPolSet();
  private LCGrpPolSchema          mNewLCGrpPolSchema     = new LCGrpPolSchema();//集体保单表
  private LCGrpPolSet             mNewLCGrpPolSet        = new LCGrpPolSet();

  private LOPRTManagerSchema   mLOPRTManagerSchema = new LOPRTManagerSchema();//打印管理表
  private LOPRTManagerSet      mLOPRTManagerSet    = new LOPRTManagerSet();
  private LJAGetOtherSchema    mLJAGetOtherShema   = new LJAGetOtherSchema();//其它其他退费实付表
  private LJAGetOtherSet       mLJAGetOtherSet     = new LJAGetOtherSet();//其它其他退费实付表

  public NewGrpPolFeeWithdrawBL() {
  }
  public static void main(String[] args) {
    NewGrpPolFeeWithdrawBL NewGrpPolFeeWithdrawBL1 = new NewGrpPolFeeWithdrawBL();
    VData tVData = new VData();
    GlobalInput tGI = new GlobalInput();
    tGI.ManageCom="8600";
    tGI.Operator="hzm";
    LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
    LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
    tLCGrpPolSchema.setGrpPolNo("86110020030220000020");
    tLCGrpPolSet.add(tLCGrpPolSchema);
    tVData.add(tLCGrpPolSet);
    tVData.add(tGI);
    NewGrpPolFeeWithdrawBL1.submitData(tVData);
  }

  //传输数据的公共方法
  public boolean submitData(VData tVData)
  {
    boolean ds =dealSubmit(tVData);
    if (!ds) return false;

    System.out.println("Start NewGrpPolFeeWithdrawBL BL Submit...");

    NewGrpPolFeeWithdrawBLS tNewGrpPolFeeWithdrawBLS=new NewGrpPolFeeWithdrawBLS();
    tNewGrpPolFeeWithdrawBLS.submitData(mInputData);

    System.out.println("End NewGrpPolFeeWithdrawBL BL Submit...");

    //如果有需要处理的错误，则返回
    if (tNewGrpPolFeeWithdrawBLS.mErrors .needDealError())
    {
        this.mErrors .copyAllErrors(tNewGrpPolFeeWithdrawBLS.mErrors ) ;
    }

    mInputData=null;
    return true;
  }

  private boolean dealSubmit(VData tVData) {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(tVData))
          return false;
    System.out.println("After getinputdata");

        //进行业务处理
        if (!dealData())
          return false;
    System.out.println("After dealData！");
        //准备往后台的数据
        if (!prepareOutputData())
          return false;
    System.out.println("After prepareOutputData");
    return true;
  }

public VData submitDataAllNew(VData tVData){
//    boolean ds =dealSubmit(tVData);
//   if (!ds) return null;
    if (!getInputData(tVData))
         return null;
   System.out.println("After getinputdata");

       //进行业务处理
       if (!dealDataSign())
         return null;
   System.out.println("After dealData！");
//       //准备往后台的数据
//       if (!prepareOutputData())
//         return null;
//   System.out.println("After prepareOutputData");
//   return true;
VData tReturn = new VData();
//MMap tmpMap = new MMap();
   tReturn.add(mLJAGetSet);
      tReturn.add(mLJAGetOtherSet);
     tReturn.add(mLOPRTManagerSet);

  return tReturn;
 // tReturn.add(tmpMap);
  //tReturn.add( mNewLCGrpPolSet );
//  return mInputData;
  //  return tmpMap;
  }
  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData()
  {
    //产生流水号
    tLimit=PubFun.getNoLimit(mGI.ManageCom);
    serNo=PubFun1.CreateMaxNo("SERIALNO",tLimit);

    LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
    for(int index=1;index<=mLCGrpPolSet.size();index++)
    {
        mLCGrpPolSchema = mLCGrpPolSet.get(index);
        //1-查询投保单号对应的已签单的保单，如果没有则跳过,否则保存查询到的纪录
        LCGrpPolDB tLCGrpPolDB=new LCGrpPolDB();
        tLCGrpPolDB.setGrpPolNo(mLCGrpPolSchema.getGrpPolNo());
        tLCGrpPolSet=tLCGrpPolDB.query();
        if(tLCGrpPolSet==null)
          continue;
        if(tLCGrpPolSet.size()==0)
          continue;
        mNewLCGrpPolSchema = tLCGrpPolSet.get(1);
        if(mNewLCGrpPolSchema.getAppFlag()==null||!mNewLCGrpPolSchema.getAppFlag().equals("1")) //如果没有签单返回
          continue;
        if(mNewLCGrpPolSchema.getOutPayFlag()==null)
        {
            mNewLCGrpPolSchema.setOutPayFlag("1");
        }
        if(mNewLCGrpPolSchema.getPayIntv()==0)//如果交费方式是趸交，退费
        {
            mNewLCGrpPolSchema.setOutPayFlag("1");
        }
        if(specWithDrawFlag)//如果特殊退费标记为真，那么默认退费
        {
            //mNewLCGrpPolSchema.setOutPayFlag("1");
            if(!(mNewLCGrpPolSchema.getDif()>0))
                continue;
        }
        else
        {
            if(!(mNewLCGrpPolSchema.getDif()>0)||!mNewLCGrpPolSchema.getOutPayFlag().equals("1"))//如果没有余额或者溢交处理方式不是退费,返回
                continue;
        }
        //2-准备实付表纪录,产生实付号码
        tLimit=PubFun.getNoLimit(mNewLCGrpPolSchema.getManageCom());
        getNo=PubFun1.CreateMaxNo("GETNO",tLimit);

        /*
        //3-准备打印数据,生成印刷流水号
        tLimit=PubFun.getNoLimit(mNewLCGrpPolSchema.getManageCom());
        prtSeqNo=PubFun1.CreateMaxNo("PRTSEQNO",tLimit);
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_GET);//
        mLOPRTManagerSchema.setOtherNo(getNo);//实付号码
        mLOPRTManagerSchema.setMakeDate(CurrentDate);
        mLOPRTManagerSchema.setMakeTime(CurrentTime);
        mLOPRTManagerSchema.setManageCom(mNewLCGrpPolSchema.getManageCom());
        mLOPRTManagerSchema.setAgentCode(mNewLCGrpPolSchema.getAgentCode());
        mLOPRTManagerSchema.setCode(PrintManagerBL.CODE_REFUND);
        mLOPRTManagerSchema.setReqCom(mGI.ManageCom);
        mLOPRTManagerSchema.setReqOperator(mGI.Operator);
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        PrintManagerBL tPrintManagerBL=new PrintManagerBL();
        VData tempVData = new VData();
        tempVData.add(mLOPRTManagerSchema);
        tempVData.add(mGI);
        if(tPrintManagerBL.submitData(tempVData,"REQ"))//打印数据处理
        {
            tempVData=tPrintManagerBL.getResult();
            mLOPRTManagerSchema=(LOPRTManagerSchema)tempVData.getObjectByObjectName("LOPRTManagerSchema",0);
            mLOPRTManagerSet.add(mLOPRTManagerSchema);
        }
        else
        {
            continue;
        }
        */

        String sql="select * from ljtempfeeclass where tempfeeno in(select tempfeeno from ljtempfee where otherno='"+mNewLCGrpPolSchema.getGrpPolNo()+"'  and confflag='1') ";
        LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
        LJTempFeeClassSet tLJTempFeeClassSet=tLJTempFeeClassDB.executeQuery(sql);
        String payMode="";
        if(tLJTempFeeClassSet!=null)
        {
            if(tLJTempFeeClassSet.size()>0)
            {
                payMode=tLJTempFeeClassSet.get(1).getPayMode();
            }

        }
        mLJAGetSchema.setPayMode(payMode);
        mLJAGetSchema.setShouldDate(CurrentDate);
        mLJAGetSchema.setStartGetDate(CurrentDate);

        //实付总表
        mLJAGetSchema = new LJAGetSchema();
        mLJAGetSchema.setActuGetNo(getNo);
        mLJAGetSchema.setOtherNo(mNewLCGrpPolSchema.getGrpPolNo());
        mLJAGetSchema.setOtherNoType("8");//其它类型退费(溢交保费退费)--集体单
        /*Lis5.3 upgrade get
        mLJAGetSchema.setAppntNo(mNewLCGrpPolSchema.getGrpNo());
        */
        mLJAGetSchema.setSumGetMoney(mNewLCGrpPolSchema.getDif());
        mLJAGetSchema.setSaleChnl(mNewLCGrpPolSchema.getSaleChnl());
        mLJAGetSchema.setSerialNo(serNo);
        mLJAGetSchema.setOperator(mNewLCGrpPolSchema.getOperator());
        mLJAGetSchema.setManageCom(mNewLCGrpPolSchema.getManageCom());
        mLJAGetSchema.setAgentCode(mNewLCGrpPolSchema.getAgentCode());
        mLJAGetSchema.setAgentCom(mNewLCGrpPolSchema.getAgentCom());
        mLJAGetSchema.setAgentGroup(mNewLCGrpPolSchema.getAgentGroup());
        mLJAGetSchema.setShouldDate(CurrentDate);
        mLJAGetSchema.setStartGetDate(CurrentDate);
        mLJAGetSchema.setMakeDate(CurrentDate);
        mLJAGetSchema.setMakeTime(CurrentTime);
        mLJAGetSchema.setModifyDate(CurrentDate);
        mLJAGetSchema.setModifyTime(CurrentTime);
        mLJAGetSet.add(mLJAGetSchema);
        //实付子表
        mLJAGetOtherShema = new LJAGetOtherSchema();
        mLJAGetOtherShema.setActuGetNo(getNo);
        mLJAGetOtherShema.setOtherNo(mNewLCGrpPolSchema.getGrpPolNo());
        mLJAGetOtherShema.setOtherNoType("8");//其它类型退费(溢交保费退费)--集体单
        mLJAGetOtherShema.setPayMode("1");
        mLJAGetOtherShema.setGetMoney(mNewLCGrpPolSchema.getDif());
        mLJAGetOtherShema.setGetDate(CurrentDate);
        mLJAGetOtherShema.setFeeOperationType("YJTF");//溢交退费
        mLJAGetOtherShema.setFeeFinaType("YJTF");//溢交退费
        mLJAGetOtherShema.setManageCom(mNewLCGrpPolSchema.getManageCom());
        mLJAGetOtherShema.setAgentCom(mNewLCGrpPolSchema.getAgentCom());
        mLJAGetOtherShema.setAgentType(mNewLCGrpPolSchema.getAgentType());
        mLJAGetOtherShema.setAPPntName(mNewLCGrpPolSchema.getGrpName());
        mLJAGetOtherShema.setAgentGroup(mNewLCGrpPolSchema.getAgentGroup());
        mLJAGetOtherShema.setAgentCode(mNewLCGrpPolSchema.getAgentCode());
        mLJAGetOtherShema.setSerialNo(serNo);
        mLJAGetOtherShema.setOperator(mNewLCGrpPolSchema.getOperator());;
        mLJAGetOtherShema.setMakeTime(CurrentTime);
        mLJAGetOtherShema.setMakeDate(CurrentDate);
        mLJAGetOtherShema.setModifyDate(CurrentDate);
        mLJAGetOtherShema.setModifyTime(CurrentTime);
        mLJAGetOtherSet.add(mLJAGetOtherShema);

        //4-更新集体保单数据
        mNewLCGrpPolSchema.setDif(0);
        mNewLCGrpPolSchema.setModifyDate(CurrentDate);
        mNewLCGrpPolSchema.setModifyTime(CurrentTime);
        mNewLCGrpPolSet.add(mNewLCGrpPolSchema);
    }
    return true ;
  }
//根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealDataSign()
  {
    //产生流水号
    tLimit=PubFun.getNoLimit(mGI.ManageCom);
    serNo=PubFun1.CreateMaxNo("SERIALNO",tLimit);

    LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
    for(int index=1;index<=mLCGrpPolSet.size();index++)
    {
        mNewLCGrpPolSchema = mLCGrpPolSet.get(index);
        //1-查询投保单号对应的已签单的保单，如果没有则跳过,否则保存查询到的纪录
//        LCGrpPolDB tLCGrpPolDB=new LCGrpPolDB();
//        tLCGrpPolDB.setGrpPolNo(mLCGrpPolSchema.getGrpPolNo());
//        tLCGrpPolSet=tLCGrpPolDB.query();
//        if(tLCGrpPolSet==null)
//          continue;
//        if(tLCGrpPolSet.size()==0)
//          continue;
 //       mNewLCGrpPolSchema = tLCGrpPolSet.get(1);
//        if(mNewLCGrpPolSchema.getAppFlag()==null||!mNewLCGrpPolSchema.getAppFlag().equals("1")) //如果没有签单返回
//          continue;
//        if(mNewLCGrpPolSchema.getOutPayFlag()==null)
//        {
//            mNewLCGrpPolSchema.setOutPayFlag("1");
//        }
//        if(mNewLCGrpPolSchema.getPayIntv()==0)//如果交费方式是趸交，退费
//        {
//            mNewLCGrpPolSchema.setOutPayFlag("1");
//        }
        if(specWithDrawFlag)//如果特殊退费标记为真，那么默认退费
        {
            //mNewLCGrpPolSchema.setOutPayFlag("1");
            if(!(mNewLCGrpPolSchema.getDif()>0))
                continue;
        }
        else
        {
            if(!(mNewLCGrpPolSchema.getDif()>0)||!mNewLCGrpPolSchema.getOutPayFlag().equals("1"))//如果没有余额或者溢交处理方式不是退费,返回
                continue;
        }
        //2-准备实付表纪录,产生实付号码
        tLimit=PubFun.getNoLimit(mNewLCGrpPolSchema.getManageCom());
        getNo=PubFun1.CreateMaxNo("GETNO",tLimit);


        //3-准备打印数据,生成印刷流水号
//        tLimit=PubFun.getNoLimit(mNewLCGrpPolSchema.getManageCom());
        prtSeqNo=PubFun1.CreateMaxNo("PRTSEQNO",tLimit);
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_GET);//
        mLOPRTManagerSchema.setOtherNo(getNo);//实付号码
        mLOPRTManagerSchema.setMakeDate(CurrentDate);
        mLOPRTManagerSchema.setMakeTime(CurrentTime);
        mLOPRTManagerSchema.setManageCom(mNewLCGrpPolSchema.getManageCom());
        mLOPRTManagerSchema.setAgentCode(mNewLCGrpPolSchema.getAgentCode());
        mLOPRTManagerSchema.setCode(PrintManagerBL.CODE_REFUND);
        mLOPRTManagerSchema.setReqCom(mGI.ManageCom);
        mLOPRTManagerSchema.setReqOperator(mGI.Operator);
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        PrintManagerBL tPrintManagerBL=new PrintManagerBL();
        VData tempVData = new VData();
        tempVData.add(mLOPRTManagerSchema);
        tempVData.add(mGI);
        if(tPrintManagerBL.submitData(tempVData,"REQ"))//打印数据处理
        {
            tempVData=tPrintManagerBL.getResult();
            mLOPRTManagerSchema=(LOPRTManagerSchema)tempVData.getObjectByObjectName("LOPRTManagerSchema",0);
            mLOPRTManagerSet.add(mLOPRTManagerSchema);
        }
        else
        {
            continue;
        }


        String sql="select * from ljtempfeeclass where tempfeeno in(select tempfeeno from ljtempfee where otherno='"+mNewLCGrpPolSchema.getGrpPolNo()+"'  and confflag='1') ";
        LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
        LJTempFeeClassSet tLJTempFeeClassSet=tLJTempFeeClassDB.executeQuery(sql);
        String payMode="";
        if(tLJTempFeeClassSet!=null)
        {
            if(tLJTempFeeClassSet.size()>0)
            {
                payMode=tLJTempFeeClassSet.get(1).getPayMode();
            }

        }
        mLJAGetSchema.setPayMode(payMode);
        mLJAGetSchema.setShouldDate(CurrentDate);
        mLJAGetSchema.setStartGetDate(CurrentDate);

        //实付总表
        mLJAGetSchema = new LJAGetSchema();
        mLJAGetSchema.setActuGetNo(getNo);
        mLJAGetSchema.setOtherNo(mNewLCGrpPolSchema.getGrpPolNo());
        mLJAGetSchema.setOtherNoType("8");//其它类型退费(溢交保费退费)--集体单
        /*Lis5.3 upgrade get
        mLJAGetSchema.setAppntNo(mNewLCGrpPolSchema.getGrpNo());
        */

        mLJAGetSchema.setSumGetMoney(mNewLCGrpPolSchema.getDif());
        mLJAGetSchema.setSaleChnl(mNewLCGrpPolSchema.getSaleChnl());
        mLJAGetSchema.setSerialNo(serNo);
        mLJAGetSchema.setOperator(mNewLCGrpPolSchema.getOperator());
        mLJAGetSchema.setManageCom(mNewLCGrpPolSchema.getManageCom());
        mLJAGetSchema.setAgentCode(mNewLCGrpPolSchema.getAgentCode());
        mLJAGetSchema.setAgentCom(mNewLCGrpPolSchema.getAgentCom());
        mLJAGetSchema.setAgentGroup(mNewLCGrpPolSchema.getAgentGroup());
        mLJAGetSchema.setShouldDate(CurrentDate);
        mLJAGetSchema.setStartGetDate(CurrentDate);
        mLJAGetSchema.setMakeDate(CurrentDate);
        mLJAGetSchema.setMakeTime(CurrentTime);
        mLJAGetSchema.setModifyDate(CurrentDate);
        mLJAGetSchema.setModifyTime(CurrentTime);
        mLJAGetSet.add(mLJAGetSchema);
        //实付子表
        mLJAGetOtherShema = new LJAGetOtherSchema();
        mLJAGetOtherShema.setActuGetNo(getNo);
        mLJAGetOtherShema.setOtherNo(mNewLCGrpPolSchema.getGrpPolNo());
        mLJAGetOtherShema.setOtherNoType("8");//其它类型退费(溢交保费退费)--集体单
        mLJAGetOtherShema.setPayMode("1");
        mLJAGetOtherShema.setGetMoney(mNewLCGrpPolSchema.getDif());
        mLJAGetOtherShema.setGetDate(CurrentDate);
        mLJAGetOtherShema.setFeeOperationType("YJTF");//溢交退费
        mLJAGetOtherShema.setFeeFinaType("YJTF");//溢交退费
        mLJAGetOtherShema.setManageCom(mNewLCGrpPolSchema.getManageCom());
        mLJAGetOtherShema.setAgentCom(mNewLCGrpPolSchema.getAgentCom());
        mLJAGetOtherShema.setAgentType(mNewLCGrpPolSchema.getAgentType());
        mLJAGetOtherShema.setAPPntName(mNewLCGrpPolSchema.getGrpName());
        mLJAGetOtherShema.setAgentGroup(mNewLCGrpPolSchema.getAgentGroup());
        mLJAGetOtherShema.setAgentCode(mNewLCGrpPolSchema.getAgentCode());
        mLJAGetOtherShema.setSerialNo(serNo);
        mLJAGetOtherShema.setOperator(mNewLCGrpPolSchema.getOperator());;
        mLJAGetOtherShema.setMakeTime(CurrentTime);
        mLJAGetOtherShema.setMakeDate(CurrentDate);
        mLJAGetOtherShema.setModifyDate(CurrentDate);
        mLJAGetOtherShema.setModifyTime(CurrentTime);
        mLJAGetOtherSet.add(mLJAGetOtherShema);

        //4-更新集体保单数据
        mNewLCGrpPolSchema.setDif(0);
        mNewLCGrpPolSchema.setModifyDate(CurrentDate);
        mNewLCGrpPolSchema.setModifyTime(CurrentTime);
        mNewLCGrpPolSet.add(mNewLCGrpPolSchema);
    }
    return true ;
  }

 /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData tVData)
  {
    mLCGrpPolSet=(LCGrpPolSet)tVData.getObjectByObjectName("LCGrpPolSet",0);
    mGI=(GlobalInput)tVData.getObjectByObjectName("GlobalInput",0);
    tTransferData=(TransferData)tVData.getObjectByObjectName("TransferData",0);

    if(mLCGrpPolSet==null||mGI==null)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="NewGrpPolFeeWithdrawBL";
      tError.functionName="getInputData";
      tError.errorMessage="没有得到足够的数据，请您确认!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    if(tTransferData!=null)
    {
        String inStr=(String)tTransferData.getValueByName("SpecWithDraw");
        if(inStr!=null)
        {
            if(inStr.equals("1"))
            {
                specWithDrawFlag=true;
            }
        }
    }

    return true;
  }

  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData()
  {
    mInputData=new VData();
    try
    {
      mInputData.add(mNewLCGrpPolSet);
      mInputData.add(mLJAGetSet);
      //mInputData.add(mLOPRTManagerSet);
      mInputData.add(mLJAGetOtherSet);
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="NewGrpPolFeeWithdrawBL";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }
}

