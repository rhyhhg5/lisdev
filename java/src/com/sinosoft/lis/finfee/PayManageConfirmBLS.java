package com.sinosoft.lis.finfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import java.sql.Connection;

/**
 * <p>Title: 付费管理处理程序</p>
 *
 * <p>Description: 数据库功能</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author : zhangjun
 * @version 1.0
 */
public class PayManageConfirmBLS {
        //传输数据类
        private VData mInputData ;
        //错误处理类，每个需要错误处理的类中都放置该类
        public  CErrors mErrors = new CErrors();
        private LJAGetSchema mLJAGetSchema = new LJAGetSchema();
        private String mOperate;
    public PayManageConfirmBLS() {
    }
    //传输数据的公共方法
    public boolean submitData(VData cInputData,String cOperate)
    {
        //首先将数据在本类中做一个备份
this.mInputData = (VData)cInputData.clone();
this.mOperate =cOperate;

if (!getInputData()) return false;
System.out.println("---End getInputData---");

if (!updateData())
  return false;


System.out.println("---End saveData---");

return true;
    }

private boolean getInputData()	{
try {
mLJAGetSchema = (LJAGetSchema)mInputData.getObjectByObjectName("LJAGetSchema", 0);
System.out.println("getinputdata "+mOperate);
}
catch (Exception e) {
  // @@错误处理
  e.printStackTrace();
  CError tError = new CError();
  tError.moduleName = "PayManageConfirmBLS";
  tError.functionName = "getInputData";
  tError.errorMessage = "接收数据失败!!";
  this.mErrors.addOneError(tError) ;
  return false;
}

return true;
}

    private boolean updateData()
  {
Connection conn=DBConnPool.getConnection();
if (conn == null) {
  // @@错误处理
  CError tError = new CError();
  tError.moduleName = "PayManageConfirmBLS";
  tError.functionName = "saveData";
  tError.errorMessage = "数据库连接失败!";
  this.mErrors.addOneError(tError) ;
  return false;
}
try {
       conn.setAutoCommit(false);

       // 修改部分
       LJAGetDB tLJAGetDB = new LJAGetDB(conn);
       tLJAGetDB.setSchema(mLJAGetSchema);
       if (!tLJAGetDB.update())
       {
       // @@错误处理
       conn.rollback() ;
       conn.close();
       System.out.println("LCCont Update Failed");
       return false;
       }
       conn.commit() ;
       conn.close();
       System.out.println("saveDataAcc");
     }
   catch (Exception ex)
          {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "PayManageConfirmBLS";
          tError.functionName = "submitData";
          tError.errorMessage = ex.toString();
          this.mErrors.addOneError(tError);
          try {
            conn.rollback();
            conn.close();
          } catch (Exception e) {}
          return false;
          }
   return true;
}
}
