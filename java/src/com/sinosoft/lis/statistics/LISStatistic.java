package com.sinosoft.lis.statistics;

import java.util.*;
import java.io.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
/**import com.sinosoft.lis.check.*;*/
import java.sql.Connection;
import com.sinosoft.lis.pubfun.PubFun;

/**
 * <p>Title:数据有效性校验 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author cui
 * @version 1.0
 */
public class LISStatistic {


    //校验类容器
    VData tVData = new VData();

    //需要校验的模块，从save页传入
    VData mModuData = new VData();

    //错误处理类，每个需要错误处理的类中都放置该类
    public  CErrors mErrors = new CErrors();

    //前台传入的需要校验的模块
    StatConfigSet mStatConfigSet =  new StatConfigSet();
    StatConfigSchema mStatConfigSchema =  new StatConfigSchema();

    public LISStatistic()
    {
    }


    /* 动态加载校验类 */
    public void loadstatisticor()
    {
        String ClassCode =null;

        StatClassConfigDB tStatClassConfigDB = new StatClassConfigDB();
        StatClassConfigSet tStatClassConfigSet = new StatClassConfigSet();

        tStatClassConfigDB.setValiFlag("1");
        tStatClassConfigDB.setModuCode(mStatConfigSchema.getModuCode());

        tStatClassConfigSet = tStatClassConfigDB.query();
        System.out.println("该校验模块中有"+tStatClassConfigSet.size()+"个校验类");
        for(int i=1;i<=tStatClassConfigSet.size();i++)
       {

            ClassCode="com.sinosoft.lis.check." +
                      tStatClassConfigSet.get(i).getClassCode();
            System.out.println(ClassCode);
            try
            {
                 statisticor st = (statisticor)ClassLoader.getSystemClassLoader().
                     loadClass(ClassCode).newInstance();
                  //statisticor st = (statisticor)ClassLoader.getSystemClassLoader().
                  // loadClass("com.sinosoft.lis.check.Proposal.ProsalRecord").newInstance();

                 //statisticor st=(statisticor)Class.forName(ClassCode).newInstance();

                 tVData.addElement(st);
            } catch (ClassNotFoundException e)
            {
                e.printStackTrace(System.err);
            }
            catch (Exception ex)
            {
                ex.printStackTrace(System.err);
            }
      }
   }

    /* 执行校验规则 */
    public void dostatistic()
    {

      ExeSQL tExeSql=new ExeSQL();
      SSRS tSSRS=new SSRS();

      int row = 1000;


      //计算某个校验模块需要校验索引表数据量
     // tSSRS=tExeSql.execSQL("select count(1) from "+ mStatConfigSchema.getEntranceTable());
      tSSRS=tExeSql.execSQL("select  count(1) from "+ mStatConfigSchema.getEntranceTable());

      String  St = tSSRS.GetText(1,1);
      int count = Integer.parseInt(St);
      System.out.println("有"+count+"条数据需要校验...");
      //System.out.println("row"+count/1000);
      //开始校验，每次查找1000条数据
      for (int i=0;i<=count/row;i++)
      {
         // System.out.println("开始校验第"+(i+1)+"批数据.....");
          tSSRS=tExeSql.execSQL(mStatConfigSchema.getRunSql(),i*row+1,(i+1)*row);

             for (int j = 1; j <=tSSRS.getMaxRow(); j++)
             {

                   System.out.println("开始校验第"+(i*row+j)+"个数据.....");
                    VData ResultVData = new VData();

                   ResultVData.add(tSSRS.getRowData(j));


                     for (int k=0;k<tVData.size();k++)
                     {

                         System.out.println("执行第"+(k+1)+"个校验类");
                         statisticor statclass = (statisticor)tVData.get(k);
                         System.out.println(statclass.toString());
                         statclass.setData(ResultVData);
                         statclass.doStatistic();
//                         String theCurrentTime3 = PubFun.getCurrentTime();
//                         System.out.println (theCurrentTime3);

                     }
            }
            String theCurrentTime3 = PubFun.getCurrentTime();
            System.out.println ("TIME:"+theCurrentTime3);
       }


    }

    public boolean submitData(VData ModuData,String cOperate)
    {
        //首先将数据在本类中做一个备份
        mModuData = (VData)ModuData.clone();
        if (!(cOperate.equals("SQL"))) {
            mStatConfigSet = (StatConfigSet) mModuData.getObjectByObjectName(
                    "StatConfigSet", 0);

            if (mStatConfigSet.size() == 0) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LISStatistic";
                tError.functionName = "submitData";
                tError.errorMessage = "数据库操作失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        TransferData tTransferData = (TransferData) mModuData.
                                     getObjectByObjectName("TransferData",
                0);
        String RunSQL = (String) tTransferData.getValueByName("SQL");




          //Connection conn = null;
         // conn = DBConnPool.getConnection();
//          if (conn==null)
//          {
//                  // @@错误处理
//                  CError tError = new CError();
//                  tError.moduleName = "LISStatistic";
//                  tError.functionName = "submitData";
//                  tError.errorMessage = "数据库连接失败!";
//                  this.mErrors .addOneError(tError) ;
//                  return false;
//        }

         try {
             //conn.setAutoCommit(false);
             if (cOperate.equals("SQL")) {



             }else{

                 for (int i = 1; i <= mStatConfigSet.size(); i++) {
                     mStatConfigSchema = mStatConfigSet.get(i);
                     tVData.clear();
                     System.out.println("加载校验类.....");
                     this.loadstatisticor();
                     System.out.println("执行校验规则......");
                     this.dostatistic();
                     System.out.println("校验完毕！");
                 }
             }
             //conn.commit();
             //conn.close();



         }
        catch (Exception ex)
        {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LISStatistic";
                tError.functionName = "submitData";
                tError.errorMessage = ex.toString();
                System.out.println(ex.toString());
                this.mErrors .addOneError(tError);
//                try{conn.rollback(); conn.close(); }
//                catch(Exception e){}
                return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
//        String theCurrentTime1 = PubFun.getCurrentTime();
//        System.out.println ("开始时间："+theCurrentTime1);
//        LISStatistic sta = new LISStatistic();
//        VData tModuData = new VData();
//
//        StatConfigDB tStatConfigDB = new StatConfigDB();
//        StatConfigSet tStatConfigSet = new StatConfigSet();
//        StatConfigSchema tStatConfigSchema = new StatConfigSchema();
//        tStatConfigDB.setModuCode("Proposal");
//        System.out.println("本次要进行Proposal校验....");
//        tStatConfigSet = tStatConfigDB.query();//查询数据
//        //        System.out.println("将模块检索sql放入容器中....");
//        tModuData.add(tStatConfigSet);//放到容器中
//        System.out.println("校验开始.....");
//        sta.submitData(tModuData,"");//提交数据
//        System.out.println("！！！Proposal模块校验完毕！！！");
//        String theCurrentTime2 = PubFun.getCurrentTime();
//        System.out.println ("结束时间："+theCurrentTime2);
        String theCurrentTime1 = PubFun.getCurrentTime();
        System.out.println("开始时间：" + theCurrentTime1);
        LISStatistic sta = new LISStatistic();
        VData tModuData = new VData();

        StatSqlConfigDB tStatSqlConfigDB = new StatSqlConfigDB();
        StatSqlConfigSet tStatSqlConfigSet = new StatSqlConfigSet();
        StatSqlConfigSchema tStatSqlConfigSchema = new StatSqlConfigSchema();
        tStatSqlConfigDB.setModuCode("Proposal");
        tStatSqlConfigDB.setValiFlag("1");
        System.out.println("本次要进行sql校验....");
        tStatSqlConfigSet = tStatSqlConfigDB.query(); //查询数据
        //        System.out.println("将模块检索sql放入容器中....");
        TransferData tTransferData = new TransferData();
        String RunSQL ="";
        tTransferData.setNameAndValue("SQL",RunSQL);
        tModuData.add(tStatSqlConfigSet); //放到容器中
        tModuData.add(tTransferData);
        System.out.println("校验开始.....");
        sta.submitData(tModuData, "SQL"); //提交数据
        System.out.println("！！！Proposal模块sql校验完毕！！！");
        String theCurrentTime2 = PubFun.getCurrentTime();
        System.out.println("结束时间：" + theCurrentTime2);

    }
}
