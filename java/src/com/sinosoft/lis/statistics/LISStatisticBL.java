package com.sinosoft.lis.statistics;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title:数据有效性校验 </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author cui
 * @version 1.0
 */
public class LISStatisticBL {


    //校验类容器
    VData mInputData = new VData();
    //需要校验的模块，从save页传入
//    VData mCheckSqlData = new VData();
    //需要校验的数据的set
    VData mCheckDataSet = new VData();
    //操作标记
    String mOperate = "";
    //模块名
    String ModuCode = "";
    //数据筛选SQL
    String ModuRunSQL = "";
    //校验表的表名
    String ModeTableName = "";
    //校验表的主字段
    String ModeTableMainColumn = "";
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    //前台传入的需要校验的模块
    StatConfigSet mStatConfigSet = new StatConfigSet();
    StatSqlConfigSet mStatSqlConfigSet = new StatSqlConfigSet();
    StatConfigSchema mStatConfigSchema = new StatConfigSchema();

    public LISStatisticBL() {
    }

    /** gongqun  经测试发现无用,使用的是PubSubmit类的这个方法
        public boolean submitData(VData cInputData, String cOperate) {
            mInputData = (VData) cInputData.clone();
            this.mOperate = cOperate;
            System.out.println("now in LISStatisticBL submit");
            if (mOperate.equals("PubCal")) {
                if (!this.dealPubCal()) {
                    System.out.println("dealPubCal错误！");
                    return false;
                }
            } else {
                if (this.getInputData() == false) {
                    return false;
                }
                System.out.println("---getInputData---");
                if (this.dealData() == false) {
                    return false;
                }
            }
            return true;
        }
     */
    /**
     * getInputData
     *
     * @return boolean
     */
    public boolean getInputData() {
        try {
            TransferData tTransferData = (TransferData) mInputData.
                                         getObjectByObjectName("TransferData",
                    0);
            ModuCode = (String) tTransferData.getValueByName("ModuCode");
            ModuRunSQL = (String) tTransferData.getValueByName("ModuRunSQL");
            if (ModuCode.equals("") || ModuRunSQL.equals("")) {
                System.out.println("请传入模块名称和筛选SQL！");
                return false;
            }
            StatConfigDB tStatConfigDB = new StatConfigDB();
            tStatConfigDB.setModuCode(ModuCode);
            mStatConfigSet = tStatConfigDB.query();
            if (!(mStatConfigSet.size() < 1)) {
                System.out.println("请在StatConfig表中描述：" + ModuCode + "模块的数据！");
                return false;
            }
            //校验校验的sql是否为空
            StatSqlConfigDB tStatSqlConfigDB = new StatSqlConfigDB();
            tStatSqlConfigDB.setModuCode(ModuCode);
            tStatSqlConfigDB.setValiFlag("1");
            mStatSqlConfigSet = tStatSqlConfigDB.query();
            if (mStatSqlConfigSet.size() < 1) {
                System.out.println("请在StatSqlConfig表中描述：" + ModuCode + "模块的数据！");
                return false;
            }
            ModeTableName = mStatConfigSet.get(1).getEntranceTable();
            ModeTableMainColumn = mStatConfigSet.get(1).getEntranceCode();
//            String ModeTableNameDB = ModeTableName + "DB";
//            String ModeTableNameSchema = ModeTableName + "Schema";
//            String ModeTableNameSet = ModeTableName + "Set";

//            RSWrapper tRSWrapper = new RSWrapper();
//            SchemaSet tSchemaSet = new SchemaSet();
//            tSchemaSet = null;
//            tRSWrapper.prepareData(tSchemaSet, ModuRunSQL);
//            SchemaSet mSchemaSet = new SchemaSet();
//            mSchemaSet = tRSWrapper.getData();

            //查询用的db
            /*
                         Class classdb = Class.forName("com.sinosoft.lis.db." +
                                          ModeTableNameDB);
                         Object objdbdb = classdb.newInstance();
//            Class[] s =Class.forName("");
             Method methodquery = classdb.getMethod("executeQuery",null);
                         Object ojbdbquery = null;
                         try {
                ojbdbquery = methodquery.invoke(objdbdb, null);
                mCheckDataSet.add(ojbdbquery);
//                ojbdbquery.getcontno();
                         } catch (Exception ex1) {
                         }
             */


//            //db返回的set
//            Class ClassSet = Class.forName("com.sinosoft.lis.vschema." +
//                                           ModeTableNameSet);
//            Object ObjectSet=ClassSet.newInstance();
//            ObjectSet=ojbdbquery;

        } catch (Exception ex) {

        }

        return true;
    }

    public static void main(String[] args) {

        LISStatisticBL tLISStatisticBL = new LISStatisticBL();
        /** gongqun 这段没有用
                TransferData tTransferData = new TransferData();
                VData tVData = new VData();
                tTransferData.setNameAndValue("ModuCode", "Proposal");
                tTransferData.setNameAndValue("ModuRunSQL",
         "select contno from lccont where conttype='1' and appflag='1'");
                tVData.add(tTransferData);
                if (!tLISStatisticBL.submitData(tVData, "PubCal")) {
                    System.out.println("错误！");
                }
         */
//gongqun 改如下
        if (!tLISStatisticBL.dealPubCal()) {
            System.out.println("dealPubCal错误！");
        }
    }


    public boolean dealData() {
        ExeSQL tExeSql = new ExeSQL();
        SSRS tSSRS = new SSRS();

        int row = 1000;
        tSSRS = tExeSql.execSQL(ModuRunSQL);
        int MaxRow = tSSRS.getMaxRow();
        int MaxCol = tSSRS.getMaxCol();
        if (MaxRow > 5500) {
            System.out.println("筛选数据大于5500，效率过慢，请调整筛选条件。");
            return false;
        }
        if (MaxRow < 1) {
            System.out.println("没有符合条件的数据，请调整筛选条件。");
            return false;
        }
        if (MaxCol > 1) {
            System.out.println("目前只支持查询一列。");
            return false;
        }
        //开始循环校验数据
        for (int i = 1; i <= MaxRow; i++) {
            String rowdata = tSSRS.GetText(i, 1);
            System.out.println("开始校验第" + i + "条记录：" + rowdata);
//            ModeTableMainColumn;

            //循环其校验规则
            String tCalSql = "";
            int iSqlSize = mStatSqlConfigSet.size();    //gongqun 提高效率
            for (int j = 1; j <= iSqlSize; j++) {
                tCalSql = mStatSqlConfigSet.get(j).getCalSql();
                PubCalculator tPubCalculator = new PubCalculator();
                tPubCalculator.setCalSql(tCalSql);
                tPubCalculator.addBasicFactor(ModeTableMainColumn, rowdata);
                String CalValue = tPubCalculator.calculate();
                if (Integer.parseInt(CalValue) > 0) {
                    PubCalculator cPubCalculator = new PubCalculator();
                    cPubCalculator.addBasicFactor(ModeTableMainColumn, rowdata);
                    String tSql = mStatSqlConfigSet.get(j).getRemark();
                    String tStr = "";
                    String tStr1 = "";
                    try {
                        while (true) {
                            tStr = PubFun.getStr(tSql, 2, "$");
                            if (tStr.equals("")) {
                                break;
                            }
                            cPubCalculator.setCalSql(tStr);
                            tStr1 = "$" + tStr.trim() + "$";
                            //替换变量
                            String repla = cPubCalculator.calculate();
//                            System.out.println(tSql);
//                            System.out.println(tStr1);
//                            System.out.println(repla);
                            tSql = StrTool.replaceEx(tSql, tStr1,
                                    repla);
                            StatisticsErrSchema tStatisticsErrSchema = new
                                    StatisticsErrSchema();
                            tStatisticsErrSchema.setModuleName(ModuCode);
                            tStatisticsErrSchema.setMajorNo(ModeTableName);
                            tStatisticsErrSchema.setNoType(String.valueOf((new
                                    java.util.Date()).getTime()));
                            tStatisticsErrSchema.setErrMess(tSql);
                            tStatisticsErrSchema.setMakeDate(PubFun.
                                    getCurrentDate());
                            tStatisticsErrSchema.setMakeTime(PubFun.
                                    getCurrentTime());
                            MMap map = new MMap();
                            map.put(tStatisticsErrSchema, "INSERT");
                            PubSubmit p = new PubSubmit();
                            VData v = new VData();
                            v.add(map);
                            if (!p.submitData(v, "")) {
                                this.mErrors.copyAllErrors(p.mErrors);
                                System.out.println("保存失败");
                                return false;
                            }
                        }
                    } catch (Exception ex) {

                    }
//                    tPubCalculator.addBasicFactor();
                }
            }
        }
        return true;
    }

    public boolean dealPubCal() {

        StatConfigDB tStatConfigDB = new StatConfigDB();
        StatConfigSet tStatConfigSet = new StatConfigSet();
        /** gongqun每次运行的校验模块，取StatConfig.ModuCode where State='1' */
        tStatConfigDB.setState("1");
        tStatConfigDB.setModuCode(mStatConfigSchema.getModuCode());

        tStatConfigSet = tStatConfigDB.query();
        int iStatConfigSize = tStatConfigSet.size();    //gongqun 提高效率
        if (iStatConfigSize < 1) {
            System.out.println("请在StatConfig表中描述校验模块的数据！");
            return false;
        }

        StatSqlConfigDB tStatSqlConfigDB = new StatSqlConfigDB();
//        tStatSqlConfigDB.setModuCode("Proposal");
        /** gongqun 循环校验各模块*/
        int i = 0;
        String tCalSql = "";
        String tRemark = "";
        String tCalCode = "";
        int MacRow = 0;
        int iStatSqlConfigSize = 0;
        for (int j = 1; j <= iStatConfigSize; j++) {

            tStatSqlConfigDB.setModuCode(tStatConfigSet.get(j).getModuCode());
            tStatSqlConfigDB.setValiFlag("1");
            mStatSqlConfigSet = tStatSqlConfigDB.query();
            iStatSqlConfigSize = mStatSqlConfigSet.size();    //gongqun 提高效率
            if (iStatSqlConfigSize < 1) {
                System.out.println("StatSqlConfig表中没有对应校验的" +
                                   tStatConfigSet.get(j).getModuCode() +
                                   "模块的有效数据！");
//gongqun 应该继续            return false;
                continue;
            }
            tCalSql = "";
            tRemark = "";
            tCalCode = "";
            MacRow = 0;
            for (i = 1; i <= iStatSqlConfigSize; i++) {
                tCalSql = mStatSqlConfigSet.get(i).getCalSql();
                tRemark = mStatSqlConfigSet.get(i).getRemark();
                tCalCode = mStatSqlConfigSet.get(i).getCalCode();
                SSRS tSSRS = new SSRS();
                ExeSQL tExeSQL = new ExeSQL();
                tSSRS = tExeSQL.execSQL(tCalSql);
//gongqun 是否能得到结果 add if
                if (tExeSQL.mErrors.getErrorCount() == 0) {
                    MacRow = tSSRS.getMaxRow();
                    if (MacRow > 0) {
//                System.out.println("SQL:" + tCalSql + "校验出错误数据");
                        tCalSql = StrTool.replaceEx(tCalSql, "'", "’"); //gongqun 修改替换字符''
                        tRemark = StrTool.replaceEx(tRemark, "'", "’"); //gongqun
                        StatisticsErrSchema tStatisticsErrSchema = new
                                StatisticsErrSchema();
                        tStatisticsErrSchema.setModuleName(tStatConfigSet.get(j).
                                getModuName() + tCalCode);
                        tStatisticsErrSchema.setNoType(String.valueOf(MacRow) +
                                "条");
                        tStatisticsErrSchema.setMajorNo(String.valueOf((new
                                java.util.Date()).getTime()));
                        tStatisticsErrSchema.setErrMess("SQL:\"" + tCalSql +
                                "\"校验出错误数据," + tRemark);
                        tStatisticsErrSchema.setMakeDate(PubFun.getCurrentDate());
                        tStatisticsErrSchema.setMakeTime(PubFun.getCurrentTime());
                        MMap map = new MMap();
                        map.put(tStatisticsErrSchema, "INSERT");
                        PubSubmit p = new PubSubmit();
                        VData v = new VData();
                        v.add(map);
                        if (!p.submitData(v, "")) {
                            this.mErrors.copyAllErrors(p.mErrors);
                            System.out.println("保存失败");
//                    return false;   /** gongqun  一个保存失败，循环还能继续执行*/
                            continue;
                        }
                    }
                } else {
                    tCalSql = StrTool.replaceEx(tCalSql, "'", "’");
                    tRemark = StrTool.replaceEx(tRemark, "'", "’");
                    StatisticsErrSchema tStatisticsErrSchema = new
                            StatisticsErrSchema();
                    tStatisticsErrSchema.setModuleName(tStatConfigSet.get(j).
                            getModuName() + tCalCode);
                    tStatisticsErrSchema.setNoType(tStatConfigSet.get(j).
                            getModuCode());
                    tStatisticsErrSchema.setMajorNo(String.valueOf((new java.
                            util.Date()).getTime()));
                    tStatisticsErrSchema.setErrMess("执行出错的SQL:\"" + tCalSql +
                            "\"");
                    tStatisticsErrSchema.setMakeDate(PubFun.getCurrentDate());
                    tStatisticsErrSchema.setMakeTime(PubFun.getCurrentTime());
                    MMap map = new MMap();
                    map.put(tStatisticsErrSchema, "INSERT");
                    PubSubmit p = new PubSubmit();
                    VData v = new VData();
                    v.add(map);
                    p.submitData(v, "");
                }
            }
        }
        return true;
    }
}
