/**
 * <p>ClassName: RevisitDetBL.java </p>
 * <p>Description: 回访任务处理 </p>
 * <p>Copyright: Copyright (c) 2009 </p>
 * <p>Company: </p>
 * @author
 * @version 1.0
 * @CreateDate：2009-7-29
 */

//包名
package com.sinosoft.lis.revisit;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.workflowengine.ActivityOperator;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.Reflections;

public class RevisitDetBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();;

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 全局数据 */
	private GlobalInput mGlobalInput ;

	/** 数据操作字符串 */
	private String mOperate;

	/** 业务处理相关变量 */
	private LIIssuePolSet tLIIssuePolSet ;

	private LIAnswerInfoSchema tLIAnswerInfoSchema ;

	private LIAnswerInfoSet tLIAnswerInfoSet ;

	private String tAnswerStatus;

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();

	private MMap map = new MMap();

	private LCAppntSchema mLCAppntSchema = new LCAppntSchema();

	public RevisitDetBL() {

	}

	/**
	 * 传输数据的公共方法
	 * @param: cInputData 输入的数据
	 *         cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		//将操作数据拷贝到本类中
		this.mInputData = cInputData;
		this.mOperate = cOperate;

		//得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		//进行业务处理
		if (!dealData()) {
			return false;
		}

		if (!prepareOutputData()) {
			return false;
		}

		PubSubmit tPubSubmit = new PubSubmit();

		System.out.println("Start RevisitDetBL Submit...");

		if (!tPubSubmit.submitData(mInputData, "INSERT")) {
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}
		System.out.println("End RevisitDetBL Submit...");
		mInputData = null;
		return true;

	}

	/**
	 * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 *
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput= new GlobalInput();
		this.mGlobalInput.setSchema((GlobalInput) cInputData
				.getObjectByObjectName("GlobalInput", 0));
		if (this.mGlobalInput == null) {
			dealError("getInputData", "mGlobalInput为空！");
			return false;
		}
		this.tLIAnswerInfoSchema= new LIAnswerInfoSchema();
		this.tLIAnswerInfoSchema.setSchema((LIAnswerInfoSchema) cInputData
				.getObjectByObjectName("LIAnswerInfoSchema", 0));
		this.tLIIssuePolSet= new LIIssuePolSet();
		this.tLIIssuePolSet.set((LIIssuePolSet) cInputData
				.getObjectByObjectName("LIIssuePolSet", 0));
		this.tAnswerStatus = ((String) cInputData.getObjectByObjectName(
				"String", 0));
		this.mLCAppntSchema.setSchema((LCAppntSchema)cInputData
		.getObjectByObjectName("LCAppntSchema", 0));
		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理
	 * 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		tLIAnswerInfoSet= new LIAnswerInfoSet();
		tLIAnswerInfoSchema.setAnswerPerson(mGlobalInput.Operator);
		tLIAnswerInfoSchema.setAnswerDate(CurrentDate);
		tLIAnswerInfoSchema.setAnswerTime(CurrentTime);
		tLIAnswerInfoSchema.setModifyDate(CurrentDate);
		tLIAnswerInfoSchema.setModifyTime(CurrentTime);
		if (tAnswerStatus.equals("1")) {
			tLIAnswerInfoSet.add(tLIAnswerInfoSchema);
			map.put(tLIAnswerInfoSet, "UPDATE");
		} else if(tAnswerStatus.equals("4")) {
			tLIAnswerInfoSet.add(tLIAnswerInfoSchema);
			map.put(tLIAnswerInfoSet, "UPDATE");

			String sql = "select a.MissionID,a.SubMissionID from lwmission a where a.missionprop1='"
				+ tLIAnswerInfoSchema.getAnswerID()
				+ "' and a.activityid='0000003001'";
		SSRS nrs = new SSRS();
		ExeSQL exesql = new ExeSQL();
		nrs = exesql.execSQL(sql);
		String tMissionID = "";
		String tSubMissionID = "";
		if (nrs.getMaxRow() >= 1) {
				tMissionID = nrs.GetText(1, 1);
				tSubMissionID = nrs.GetText(1, 2);
		}
		String sql_u ="update  lwmission m set m.MissionProp6='"+mGlobalInput.Operator+"',m.MissionProp9='"+CurrentDate+"',m.MissionProp10='"+CurrentTime
		             +"',m.MissionProp7='"+tAnswerStatus+"',m.DEFAULTOPERATOR ='"+mGlobalInput.Operator+"' where m.MissionID ='"+tMissionID+"' and m.SubMissionID ='"+tSubMissionID+"' and m.activityid='0000003001'  ";
		map.put(sql_u, "UPDATE");

	    //makeBooking();//回访状态到预约
		} else if (tAnswerStatus.equals("2")) {//2问题件到转办处理
			makeLIIssuePol();
		} else if (tAnswerStatus.equals("3")||tAnswerStatus.equals("5")) {
			makeLIIssuePol2();//联系方式错误到转办 \5-多次联系不成功
		} else if (tAnswerStatus.equals("98") || tAnswerStatus.equals("99")) {
			makeSuccess();//状态成功或失败处理
		}

		/**
		 * 回访轨迹
		 */
		Reflections tReflections = new Reflections();
		LIAnswerInfoTraceSchema tLIAnswerInfoTraceSchema = new LIAnswerInfoTraceSchema();
		tReflections
		.transFields(tLIAnswerInfoTraceSchema, tLIAnswerInfoSchema);
		tLIAnswerInfoTraceSchema.setTraceType("02");
		tLIAnswerInfoTraceSchema.setSerialNo(PubFun1.CreateMaxNo("ANSWERINFOTRACE", 20));
		map.put(tLIAnswerInfoTraceSchema,"INSERT");

		/**
		 * 回访电话类型和回访电话的更新
		 */
	    String sqlLCAppnt = "update LCAppnt set AnswerPhoneType='"+mLCAppntSchema.getAnswerPhoneType()
	     +"',AnswerPhone='"+mLCAppntSchema.getAnswerPhone()+"' where contno='"+mLCAppntSchema.getContNo()
	      +"' and AppntNo='"+mLCAppntSchema.getAppntNo()+"'";
	    map.put(sqlLCAppnt,"UPDATE");
		return true;
	}

	private boolean prepareOutputData() {
		try {
			this.mInputData.add(this.map);
		} catch (Exception ex) {
			// @@错误处理
			dealError("prepareData", "在准备往后层处理所需要的数据时出错。");
			return false;
		}
		return true;
	}

	public static void main(String[] args) {

	}

	private void dealError(String FuncName, String ErrMsg) {
		CError tError = new CError();
		tError.moduleName = "AmlRiskBL";
		tError.functionName = FuncName;
		tError.errorMessage = ErrMsg;
		mErrors.addOneError(tError);
	}
	private boolean makeLIIssuePol(){
		tLIAnswerInfoSet.add(tLIAnswerInfoSchema);
		map.put(tLIAnswerInfoSet, "UPDATE");
		map.put("delete From LIIssuePol  where answerid='"
				+ tLIIssuePolSet.get(1).getAnswerID() + "'", "DELETE");
		map.put(tLIIssuePolSet, "DELETE&INSERT");
		TransferData tWorkFlowTransferData = new TransferData();
		tWorkFlowTransferData.setNameAndValue("AnswerID",
				tLIAnswerInfoSchema.getAnswerID());
		tWorkFlowTransferData.setNameAndValue("OtherNoType", "00");
		tWorkFlowTransferData.setNameAndValue("OtherNo",
				tLIAnswerInfoSchema.getOtherNo());
		tWorkFlowTransferData.setNameAndValue("AppntNo",
				tLIAnswerInfoSchema.getAppntNo());
		tWorkFlowTransferData
				.setNameAndValue("AnswerStatus", tAnswerStatus);
		tWorkFlowTransferData.setNameAndValue("AnswerType", "1");
		tWorkFlowTransferData.setNameAndValue("Operator",
				tLIAnswerInfoSchema.getOperator());
		if (tLIAnswerInfoSchema.getAnswerPerson() == null) {
			tWorkFlowTransferData.setNameAndValue("AnswerPerson", "");
		} else {
			tWorkFlowTransferData.setNameAndValue("AnswerPerson",
					tLIAnswerInfoSchema.getAnswerPerson());
		}
		if (tLIAnswerInfoSchema.getAnswerDate() == null) {
			tWorkFlowTransferData.setNameAndValue("AnswerDate", "");
		} else {
			tWorkFlowTransferData.setNameAndValue("AnswerDate",
					tLIAnswerInfoSchema.getAnswerDate());
		}
		if (tLIAnswerInfoSchema.getAnswerTime() == null) {
			tWorkFlowTransferData.setNameAndValue("AnswerTime", "");
		} else {
			tWorkFlowTransferData.setNameAndValue("AnswerTime",
					tLIAnswerInfoSchema.getAnswerTime());
		}
		if (tLIAnswerInfoSchema.getTakeMode() == null) {
			tWorkFlowTransferData.setNameAndValue("TakeMode", "");
		} else {
			tWorkFlowTransferData.setNameAndValue("TakeMode",
					tLIAnswerInfoSchema.getTakeMode());
		}
		if (tLIAnswerInfoSchema.getAnswerExplain() == null) {
			tWorkFlowTransferData.setNameAndValue("AnswerExplain", "");
		} else {
			tWorkFlowTransferData.setNameAndValue("AnswerExplain",
					tLIAnswerInfoSchema.getAnswerExplain());
		}
		if(tLIAnswerInfoSchema.getCloseReason() == null){
			tWorkFlowTransferData.setNameAndValue("CloseReason","");
		}
		else{
			tWorkFlowTransferData.setNameAndValue("CloseReason",
				tLIAnswerInfoSchema.getCloseReason());
		}
		if(tLIAnswerInfoSchema.getRemark() == null){
			tWorkFlowTransferData.setNameAndValue("Remark", "");
		}
		else{
			tWorkFlowTransferData.setNameAndValue("Remark", tLIAnswerInfoSchema.getRemark());
		}
		ActivityOperator tActivityOperator = new ActivityOperator();
		LWMissionSchema tLWMissionSchema = new LWMissionSchema();
		String sql = "select distinct a.MissionID," +
				                      "a.SubMissionID " +
				     "from lwmission a" +
				     " where a.missionprop1='"+ tLIAnswerInfoSchema.getAnswerID()
				       + "' and a.activityid='0000003001'";
		SSRS nrs = new SSRS();
		ExeSQL exesql = new ExeSQL();
		nrs = exesql.execSQL(sql);
	    /**modufy dww 如果没有值，则直接返回true **/
		if(nrs.MaxRow == 0)
		{
			return true;
		}
		String tMissionID = nrs.GetText(1, 1);
		String tSubMissionID = nrs.GetText(1, 2);
		VData tWorkFlowVData = new VData();
		tWorkFlowVData.add(mGlobalInput);
		tWorkFlowVData.add(tWorkFlowTransferData);
		try {
			if (tActivityOperator.CreateNextMission(tMissionID,
					tSubMissionID, "0000003001", tWorkFlowVData)) {
				VData tVData = new VData();
				tVData = tActivityOperator.getResult();
				MMap tmap = (MMap) tVData.getObjectByObjectName("MMap", 0);
				map.add(tmap);

			} else {
				this.mErrors.copyAllErrors(tActivityOperator.mErrors);

				return false;
			}
			if (tActivityOperator.DeleteMission(tMissionID, tSubMissionID,
					"0000003001", tWorkFlowVData)) {
				VData tVData = new VData();
				tVData = tActivityOperator.getResult();
				MMap tmap = (MMap) tVData.getObjectByObjectName("MMap", 0);
				map.add(tmap);
			} else {
				this.mErrors.copyAllErrors(tActivityOperator.mErrors);

				return false;
			}
		} catch (Exception ex) {
			// @@错误处理
			this.mErrors.copyAllErrors(tActivityOperator.mErrors);

			CError tError = new CError();
			tError.moduleName = "PEdorManuUWWorkFlowBL";
			tError.functionName = "dealData";
			tError.errorMessage = "工作流引擎执行保监会报表活动表任务出错!";
			this.mErrors.addOneError(tError);

			return false;
		}
		return true;
	}
	private boolean makeLIIssuePol2(){
		tLIAnswerInfoSet.add(tLIAnswerInfoSchema);
		map.put(tLIAnswerInfoSet, "UPDATE");
		TransferData tWorkFlowTransferData = new TransferData();
		tWorkFlowTransferData.setNameAndValue("AnswerID",
				tLIAnswerInfoSchema.getAnswerID());
		tWorkFlowTransferData.setNameAndValue("OtherNoType", "00");
		tWorkFlowTransferData.setNameAndValue("OtherNo",
				tLIAnswerInfoSchema.getOtherNo());
		tWorkFlowTransferData.setNameAndValue("AppntNo",
				tLIAnswerInfoSchema.getAppntNo());
		tWorkFlowTransferData
				.setNameAndValue("AnswerStatus", tAnswerStatus);
		tWorkFlowTransferData.setNameAndValue("AnswerType", "1");
		tWorkFlowTransferData.setNameAndValue("Operator",
				tLIAnswerInfoSchema.getOperator());
		if (tLIAnswerInfoSchema.getAnswerPerson() == null) {
			tWorkFlowTransferData.setNameAndValue("AnswerPerson", "");
		} else {
			tWorkFlowTransferData.setNameAndValue("AnswerPerson",
					tLIAnswerInfoSchema.getAnswerPerson());
		}
		if (tLIAnswerInfoSchema.getAnswerDate() == null) {
			tWorkFlowTransferData.setNameAndValue("AnswerDate", "");
		} else {
			tWorkFlowTransferData.setNameAndValue("AnswerDate",
					tLIAnswerInfoSchema.getAnswerDate());
		}
		if (tLIAnswerInfoSchema.getAnswerTime() == null) {
			tWorkFlowTransferData.setNameAndValue("AnswerTime", "");
		} else {
			tWorkFlowTransferData.setNameAndValue("AnswerTime",
					tLIAnswerInfoSchema.getAnswerTime());
		}
		if (tLIAnswerInfoSchema.getTakeMode() == null) {
			tWorkFlowTransferData.setNameAndValue("TakeMode", "");
		} else {
			tWorkFlowTransferData.setNameAndValue("TakeMode",
					tLIAnswerInfoSchema.getTakeMode());
		}
		if (tLIAnswerInfoSchema.getAnswerExplain() == null) {
			tWorkFlowTransferData.setNameAndValue("AnswerExplain", "");
		} else {
			tWorkFlowTransferData.setNameAndValue("AnswerExplain",
					tLIAnswerInfoSchema.getAnswerExplain());
		}
		if(tLIAnswerInfoSchema.getCloseReason() == null){
			tWorkFlowTransferData.setNameAndValue("CloseReason","");
		}
		else{
			tWorkFlowTransferData.setNameAndValue("CloseReason",
				tLIAnswerInfoSchema.getCloseReason());
		}
		if(tLIAnswerInfoSchema.getRemark() == null){
			tWorkFlowTransferData.setNameAndValue("Remark", "");
		}
		else{
			tWorkFlowTransferData.setNameAndValue("Remark", tLIAnswerInfoSchema.getRemark());
		}
		ActivityOperator tActivityOperator = new ActivityOperator();
		LWMissionSchema tLWMissionSchema = new LWMissionSchema();
		String sql = "select a.MissionID,a.SubMissionID from lwmission a where a.missionprop1='"
				+ tLIAnswerInfoSchema.getAnswerID()
				+ "' and a.activityid='0000003001'";
		SSRS nrs = new SSRS();
		ExeSQL exesql = new ExeSQL();
		nrs = exesql.execSQL(sql);
	    /**modufy dww 如果没有值，则直接返回true **/
		if(nrs.MaxRow == 0)
		{
			return true;
		}
		String tMissionID = nrs.GetText(1, 1);
		String tSubMissionID = nrs.GetText(1, 2);
		VData tWorkFlowVData = new VData();
		tWorkFlowVData.add(mGlobalInput);
		tWorkFlowVData.add(tWorkFlowTransferData);
		try {
			/*if (!tActivityOperator.ExecuteMission(tMissionID, tSubMissionID,
			 "0000003001", tWorkFlowVData))
			 {
			 // @@错误处理
			 this.mErrors.copyAllErrors(tActivityOperator.mErrors);

			 return false;
			 }*/

			//产生执行完保全工作流待人工核保活动表任务后的任务节点
			if (tActivityOperator.CreateNextMission(tMissionID,
					tSubMissionID, "0000003001", tWorkFlowVData)) {
				VData tVData = new VData();
				tVData = tActivityOperator.getResult();
				MMap tmap = (MMap) tVData.getObjectByObjectName("MMap", 0);
				map.add(tmap);

			} else {
				this.mErrors.copyAllErrors(tActivityOperator.mErrors);

				return false;
			}
			if (tActivityOperator.DeleteMission(tMissionID, tSubMissionID,
					"0000003001", tWorkFlowVData)) {
				VData tVData = new VData();
				tVData = tActivityOperator.getResult();
				MMap tmap = (MMap) tVData.getObjectByObjectName("MMap", 0);
				map.add(tmap);
			} else {
				this.mErrors.copyAllErrors(tActivityOperator.mErrors);

				return false;
			}
		} catch (Exception ex) {
			// @@错误处理
			this.mErrors.copyAllErrors(tActivityOperator.mErrors);

			CError tError = new CError();
			tError.moduleName = "PEdorManuUWWorkFlowBL";
			tError.functionName = "dealData";
			tError.errorMessage = "工作流引擎执行保监会报表活动表任务出错!";
			this.mErrors.addOneError(tError);

			return false;
		}
		return true;
	}
	private boolean makeSuccess(){
		tLIAnswerInfoSet.add(tLIAnswerInfoSchema);
		map.put(tLIAnswerInfoSet, "UPDATE");
		TransferData tWorkFlowTransferData = new TransferData();
		tWorkFlowTransferData.setNameAndValue("AnswerID",
				tLIAnswerInfoSchema.getAnswerID());
		tWorkFlowTransferData.setNameAndValue("OtherNoType", "00");
		tWorkFlowTransferData.setNameAndValue("OtherNo",
				tLIAnswerInfoSchema.getOtherNo());
		tWorkFlowTransferData.setNameAndValue("AppntNo",
				tLIAnswerInfoSchema.getAppntNo());
		tWorkFlowTransferData
				.setNameAndValue("AnswerStatus", tAnswerStatus);
		tWorkFlowTransferData.setNameAndValue("AnswerType", "1");
		if (tLIAnswerInfoSchema.getAnswerPerson() == null) {
			tWorkFlowTransferData.setNameAndValue("AnswerPerson", "");
		} else {
			tWorkFlowTransferData.setNameAndValue("AnswerPerson",
					tLIAnswerInfoSchema.getAnswerPerson());
		}
		if (tLIAnswerInfoSchema.getAnswerDate() == null) {
			tWorkFlowTransferData.setNameAndValue("AnswerDate", "");
		} else {
			tWorkFlowTransferData.setNameAndValue("AnswerDate",
					tLIAnswerInfoSchema.getAnswerDate());
		}
		if (tLIAnswerInfoSchema.getAnswerTime() == null) {
			tWorkFlowTransferData.setNameAndValue("AnswerTime", "");
		} else {
			tWorkFlowTransferData.setNameAndValue("AnswerTime",
					tLIAnswerInfoSchema.getAnswerTime());
		}
		if (tLIAnswerInfoSchema.getTakeMode() == null) {
			tWorkFlowTransferData.setNameAndValue("TakeMode", "");
		} else {
			tWorkFlowTransferData.setNameAndValue("TakeMode",
					tLIAnswerInfoSchema.getTakeMode());
		}
		if (tLIAnswerInfoSchema.getAnswerExplain() == null) {
			tWorkFlowTransferData.setNameAndValue("AnswerExplain", "");
		} else {
			tWorkFlowTransferData.setNameAndValue("AnswerExplain",
					tLIAnswerInfoSchema.getAnswerExplain());
		}
		if(tLIAnswerInfoSchema.getCloseReason() == null){
			tWorkFlowTransferData.setNameAndValue("CloseReason","");
		}
		else{
			tWorkFlowTransferData.setNameAndValue("CloseReason",
				tLIAnswerInfoSchema.getCloseReason());
		}
		if(tLIAnswerInfoSchema.getRemark() == null){
			tWorkFlowTransferData.setNameAndValue("Remark", "");
		}
		else{
			tWorkFlowTransferData.setNameAndValue("Remark", tLIAnswerInfoSchema.getRemark());
		}
		ActivityOperator tActivityOperator = new ActivityOperator();
		LWMissionSchema tLWMissionSchema = new LWMissionSchema();
		String sql = "select a.MissionID,a.SubMissionID from lwmission a where a.missionprop1='"
				+ tLIAnswerInfoSchema.getAnswerID()
				+ "' and a.activityid='0000003001'";
		SSRS nrs = new SSRS();
		ExeSQL exesql = new ExeSQL();
		nrs = exesql.execSQL(sql);
	    /**modufy dww 如果没有值，则直接返回true **/
		if(nrs.MaxRow == 0)
		{
			return true;
		}
		String tMissionID = nrs.GetText(1, 1);
		String tSubMissionID = nrs.GetText(1, 2);
		VData tWorkFlowVData = new VData();
		tWorkFlowVData.add(mGlobalInput);
		tWorkFlowVData.add(tWorkFlowTransferData);
		try {
			/*if (!tActivityOperator.ExecuteMission(tMissionID, tSubMissionID,
			 "0000003001", tWorkFlowVData))
			 {
			 // @@错误处理
			 this.mErrors.copyAllErrors(tActivityOperator.mErrors);

			 return false;
			 }*/

			//产生执行完保全工作流待人工核保活动表任务后的任务节点
			if (tActivityOperator.CreateNextMission(tMissionID,
					tSubMissionID, "0000003001", tWorkFlowVData)) {
				VData tVData = new VData();
				tVData = tActivityOperator.getResult();
				MMap tmap = (MMap) tVData.getObjectByObjectName("MMap", 0);
				map.add(tmap);

			} else {
				this.mErrors.copyAllErrors(tActivityOperator.mErrors);

				return false;
			}
			if (tActivityOperator.DeleteMission(tMissionID, tSubMissionID,
					"0000003001", tWorkFlowVData)) {
				VData tVData = new VData();
				tVData = tActivityOperator.getResult();
				MMap tmap = (MMap) tVData.getObjectByObjectName("MMap", 0);
				map.add(tmap);
			} else {
				this.mErrors.copyAllErrors(tActivityOperator.mErrors);

				return false;
			}
		} catch (Exception ex) {
			// @@错误处理
			this.mErrors.copyAllErrors(tActivityOperator.mErrors);

			CError tError = new CError();
			tError.moduleName = "PEdorManuUWWorkFlowBL";
			tError.functionName = "dealData";
			tError.errorMessage = "工作流引擎执行保监会报表活动表任务出错!";
			this.mErrors.addOneError(tError);

			return false;
		}
		return true;
	}
	private boolean makeBooking(){
		tLIAnswerInfoSchema.setOperator("");
		tLIAnswerInfoSet.add(tLIAnswerInfoSchema);
		map.put(tLIAnswerInfoSet, "UPDATE");
		TransferData tWorkFlowTransferData = new TransferData();
		tWorkFlowTransferData.setNameAndValue("AnswerID",
				tLIAnswerInfoSchema.getAnswerID());
		tWorkFlowTransferData.setNameAndValue("OtherNoType", "00");
		tWorkFlowTransferData.setNameAndValue("OtherNo",
				tLIAnswerInfoSchema.getOtherNo());
		tWorkFlowTransferData.setNameAndValue("AppntNo",
				tLIAnswerInfoSchema.getAppntNo());
		tWorkFlowTransferData
				.setNameAndValue("AnswerStatus", tAnswerStatus);
		tWorkFlowTransferData.setNameAndValue("AnswerType", "1");
		if (tLIAnswerInfoSchema.getAnswerPerson() == null) {
			tWorkFlowTransferData.setNameAndValue("AnswerPerson", "");
		} else {
			tWorkFlowTransferData.setNameAndValue("AnswerPerson",
					tLIAnswerInfoSchema.getAnswerPerson());
		}
		if (tLIAnswerInfoSchema.getAnswerDate() == null) {
			tWorkFlowTransferData.setNameAndValue("AnswerDate", "");
		} else {
			tWorkFlowTransferData.setNameAndValue("AnswerDate",
					tLIAnswerInfoSchema.getAnswerDate());
		}
		if (tLIAnswerInfoSchema.getAnswerTime() == null) {
			tWorkFlowTransferData.setNameAndValue("AnswerTime", "");
		} else {
			tWorkFlowTransferData.setNameAndValue("AnswerTime",
					tLIAnswerInfoSchema.getAnswerTime());
		}
		if (tLIAnswerInfoSchema.getTakeMode() == null) {
			tWorkFlowTransferData.setNameAndValue("TakeMode", "");
		} else {
			tWorkFlowTransferData.setNameAndValue("TakeMode",
					tLIAnswerInfoSchema.getTakeMode());
		}
		if (tLIAnswerInfoSchema.getAnswerExplain() == null) {
			tWorkFlowTransferData.setNameAndValue("AnswerExplain", "");
		} else {
			tWorkFlowTransferData.setNameAndValue("AnswerExplain",
					tLIAnswerInfoSchema.getAnswerExplain());
		}
		if(tLIAnswerInfoSchema.getCloseReason() == null){
			tWorkFlowTransferData.setNameAndValue("CloseReason","");
		}
		else{
			tWorkFlowTransferData.setNameAndValue("CloseReason",
				tLIAnswerInfoSchema.getCloseReason());
		}
		if(tLIAnswerInfoSchema.getRemark() == null){
			tWorkFlowTransferData.setNameAndValue("Remark", "");
		}
		else{
			tWorkFlowTransferData.setNameAndValue("Remark", tLIAnswerInfoSchema.getRemark());
		}
		ActivityOperator tActivityOperator = new ActivityOperator();
		LWMissionSchema tLWMissionSchema = new LWMissionSchema();
		String sql = "select a.MissionID,a.SubMissionID from lwmission a where a.missionprop1='"
				+ tLIAnswerInfoSchema.getAnswerID()
				+ "' and a.activityid='0000003001'";
		SSRS nrs = new SSRS();
		ExeSQL exesql = new ExeSQL();
		nrs = exesql.execSQL(sql);
		String tMissionID = nrs.GetText(1, 1);
		String tSubMissionID = nrs.GetText(1, 2);
		VData tWorkFlowVData = new VData();
		tWorkFlowVData.add(mGlobalInput);
		tWorkFlowVData.add(tWorkFlowTransferData);
		try {
			/*if (!tActivityOperator.ExecuteMission(tMissionID, tSubMissionID,
			 "0000003001", tWorkFlowVData))
			 {
			 // @@错误处理
			 this.mErrors.copyAllErrors(tActivityOperator.mErrors);

			 return false;
			 }*/

			//产生执行完保全工作流待人工核保活动表任务后的任务节点
			if (tActivityOperator.CreateNextMission(tMissionID,
					tSubMissionID, "0000003001", tWorkFlowVData)) {
				VData tVData = new VData();
				tVData = tActivityOperator.getResult();
				MMap tmap = (MMap) tVData.getObjectByObjectName("MMap", 0);
				map.add(tmap);

			} else {
				this.mErrors.copyAllErrors(tActivityOperator.mErrors);

				return false;
			}
			if (tActivityOperator.DeleteMission(tMissionID, tSubMissionID,
					"0000003001", tWorkFlowVData)) {
				VData tVData = new VData();
				tVData = tActivityOperator.getResult();
				MMap tmap = (MMap) tVData.getObjectByObjectName("MMap", 0);
				map.add(tmap);
			} else {
				this.mErrors.copyAllErrors(tActivityOperator.mErrors);

				return false;
			}
		} catch (Exception ex) {
			// @@错误处理
			this.mErrors.copyAllErrors(tActivityOperator.mErrors);

			CError tError = new CError();
			tError.moduleName = "PEdorManuUWWorkFlowBL";
			tError.functionName = "dealData";
			tError.errorMessage = "工作流引擎执行保监会报表活动表任务出错!";
			this.mErrors.addOneError(tError);

			return false;
		}
		return true;
	}
}
