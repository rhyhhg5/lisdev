/**
 * <p>
 * ClassName: RedealDetUI.java
 * </p>
 * <p>
 * Description: 转办任务处理
 * </p>
 * <p>
 * Copyright: Copyright (c) 2009
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author liwb
 * @version 1.0
 * @Database:
 * @CreateDate：2009-7-31
 */

// 包名
package com.sinosoft.lis.revisit;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.pubfun.*;

public class RedealDetUI {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 数据操作字符串 */
	private String mOperate;

	// 业务处理相关变量

	public RedealDetUI() {

	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		RedealDetBL tRedealDetBL = new RedealDetBL();
		tRedealDetBL.submitData(cInputData, mOperate);
		// 如果有需要处理的错误，则返回
		if (tRedealDetBL.mErrors.needDealError()) {
			// @@错误处理
			this.mErrors.copyAllErrors(tRedealDetBL.mErrors);
			CError tError = new CError();
			tError.moduleName = "RedealDetUI";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}

		if (mOperate.equals("INSERT||MAIN")) {
			this.mResult.clear();
			this.mResult = tRedealDetBL.getResult();
		}
		return true;
	}

	public static void main(String[] args) {

	}

	public VData getResult() {
		return this.mResult;
	}
}
