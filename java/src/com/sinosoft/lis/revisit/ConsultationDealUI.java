package com.sinosoft.lis.revisit;

/**
 * <p>
 * ClassName: ConsultationDealUI.java
 * </p>
 * <p>
 * Description: 咨诉任务处理
 * </p>
 * <p>
 * Copyright: Copyright (c) 2009
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @Database:
 * @CreateDate：2009-8-6
 */

// 包名
// package com.sinosoft.lis.health;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.pubfun.*;

public class ConsultationDealUI {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 数据操作字符串 */
	private String mOperate;

	// 业务处理相关变量

	public ConsultationDealUI() {

	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		ConsultationDealBL tConsultationDealBL = new ConsultationDealBL();
		tConsultationDealBL.submitData(cInputData, mOperate);
		// 如果有需要处理的错误，则返回
		if (tConsultationDealBL.mErrors.needDealError()) {
			// @@错误处理
			this.mErrors.copyAllErrors(tConsultationDealBL.mErrors);
			CError tError = new CError();
			tError.moduleName = "ConsultationDealUI";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	public static void main(String[] args) {

	}

}
