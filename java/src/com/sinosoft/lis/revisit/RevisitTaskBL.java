package com.sinosoft.lis.revisit;

/*
 * <p>ClassName: RevisitTaskBL </p>
 * <p>Description: 回访批处理 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Database:
 * @author：柯科
 * @CreateDate：2009-07-30
 */

import java.io.File;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.workflowengine.ActivityOperator;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;

public class RevisitTaskBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	/** 数据操作字符串 */
	private String mOperate;

	private String theCurrentDate = PubFun.getCurrentDate();

	private String theCurrentTime = PubFun.getCurrentTime();

	private MMap map = new MMap();

	/** 业务处理相关变量 */

	public RevisitTaskBL() {
	}

	/**
	 * 传输数据的公共方法
	 *
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;
		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}
		// 进行业务处理
		if (!dealData()) {
			return false;
		}

		/**
		// 准备往后台的数据
		if (!prepareOutputData()) {
			return false;
		}
		System.out.println("Start RevisitTaskBL Submit...");
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			dealError("submitData", "数据提交失败!");
			return false;
		}
		System.out.println("End RevisitTaskBL Submit...");
		mInputData = null;
		**/
		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		try {
			FDate tFDate = new FDate();

			// 回访抽取批处理开始，10天的时间段循环，避免不批处理无法抽档，
			for(int j = 9; j >= 0; j--)
			{
			    String dCurrentDate = tFDate.getString(PubFun.calDate(tFDate
						.getDate(theCurrentDate), -j, "D", null));
				makeAnswerInfo(dCurrentDate);
				// 回访抽取批处理结束
				// 转办问题件批处理开始
				makeSuccess(dCurrentDate);
				// 转办问题件批处理结束
				// 转办联系方式问题件批处理开始
				makeSuccess2(dCurrentDate);
				// 转办联系方式问题件批处理结束
			}
		} catch (Exception e) {
			// @@错误处理
			e.printStackTrace();
			CError tError = new CError();
			tError.moduleName = "WnCustomerReportBL";
			tError.functionName = "dealData";
			tError.errorMessage = "数据处理错误! " + e.getMessage();
			this.mErrors.addOneError(tError);
			return false;
		}

		return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData) {
		/*
		 * mGlobalInput.setSchema((GlobalInput) cInputData.
		 * getObjectByObjectName( "GlobalInput", 0));
		 *
		 * if (this.mGlobalInput == null) { dealError("getInputData",
		 * "mGlobalInput为空！"); return false; }
		 */
		mGlobalInput.Operator = "001";
		return true;
	}

	/**
	 * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
	 */

	private boolean prepareOutputData() {
		try {
			this.mInputData.add(this.map);
		} catch (Exception ex) {
			// @@错误处理
			dealError("prepareData", "在准备往后层处理所需要的数据时出错。");
			return false;
		}
		return true;

	}

	private void dealError(String FuncName, String ErrMsg) {
		CError tError = new CError();
		tError.moduleName = "RevisitTaskBL";
		tError.functionName = FuncName;
		tError.errorMessage = ErrMsg;
		mErrors.addOneError(tError);
	}

	public static void main(String[] args) {
		RevisitTaskBL tRevisitTaskBL = new RevisitTaskBL();
		VData mResult = new VData();
		tRevisitTaskBL.submitData(mResult,"001");
	}

	private boolean makeAnswerInfo(String dCurrentDate) {
		LIAnswerInfoSet tLIAnswerInfoSet = new LIAnswerInfoSet();
		LWMissionSet tLWMissionSet = new LWMissionSet();
		String tsql = "select a.contno,"
			              + " a.appntno,"
			              + " a.ManageCom,"
				          + " a.AgentCode,"
				          + " a.AppntName,"
				          + " ( case ( select d.riskprop"
                                   +" from lcpol c, lmriskapp d"
                                   +" where c.mainpolno = c.polno"
                                     +" and c.contno = a.contno"
                                     +" and c.riskcode = d.riskcode"
                                     +" fetch first 1 rows only)"
                            +" when 'Y' then '05'"
                            +" when 'I' then '01'"
                            +" else ''"
                            +" end ),"
				          + " ( select riskcode"
				          + "   from lcpol c"
				          + "   where c.mainpolno = c.polno"
				          + "     and c.contno = a.contno "
				          + "     fetch first 1 rows only),"
						  + " date('" + dCurrentDate + "')+ 1 days "
					+ " from lccont a, lcpolprint b "
					+ " where a.contno = b.mainpolno"
					  + " and a.conttype = '1'"
					  + " and a.appflag = '1'"
					  + " and b.makedate <=  date('"+ dCurrentDate + "') - 12 days "
					  + " and a.customgetpoldate is null and a.SaleChnl not in ('03','07') "
					  +" and exists (select 'Y' from lmriskapp e,lcpol f where "
					  +" e.riskperiod='L' and e.riskcode=f.riskcode and f.contno=a.contno and f.mainpolno = f.polno fetch first 1 rows only) "
					  +" and exists (select 'Y' from LCAppnt g where g.contno = a.contno and g.AppntNo=a.AppntNo and (g.answerflag='1' or g.answerflag is null)) "
					  +" and not exists (select 'Y' from LIAnswerInfo where OtherNo=a.contno) "
				     + " union "
					+ " select  a.contno,"
					        + " a.appntno,"
					        + " a.ManageCom,"
						    + " a.AgentCode,"
						    + " a.AppntName,"
						    + " ( case ( select d.riskprop"
                                     +" from lcpol c, lmriskapp d"
                                     +" where c.mainpolno = c.polno"
                                       +" and c.contno = a.contno"
                                       +" and c.riskcode = d.riskcode"
                                       +" fetch first 1 rows only)"
                              +" when 'Y' then '05'"
                              +" when 'I' then '01'"
                              +" else ''"
                              +" end ),"
						    + " ( select riskcode"
						      + " from lcpol c"
						      + " where c.mainpolno = c.polno"
						        + " and c.contno = a.contno "
						        + " fetch first 1 rows only),"
						    + " date('" + dCurrentDate + "')+ 1 days "
					+ " from lccont a, lcpolprint b"
					+ " where a.contno = b.mainpolno"
					  + " and a.conttype = '1'"
					  + " and a.appflag = '1' and a.SaleChnl not in ('03','07') "
					  + " and b.makedate >=  date('"+ dCurrentDate + "') - 12 days"
					  + " and a.customgetpoldate =  date('"+ dCurrentDate + "') "
					  +" and exists (select 'Y' from lmriskapp e,lcpol f where "
					  +" e.riskperiod='L' and e.riskcode=f.riskcode and f.contno=a.contno and f.mainpolno = f.polno fetch first 1 rows only) "
					  +" and exists (select 'Y' from LCAppnt g where g.contno = a.contno and g.AppntNo=a.AppntNo and (g.answerflag='1' or g.answerflag is null)) "
					  +" and not exists (select 'Y' from LIAnswerInfo where OtherNo=a.contno) "
					  + " union "
						+ " select  a.contno,"
						        + " a.appntno,"
						        + " a.ManageCom,"
							    + " a.AgentCode,"
							    + " a.AppntName,"
							    + " ( case ( select d.riskprop"
	                                     +" from lcpol c, lmriskapp d"
	                                     +" where c.mainpolno = c.polno"
	                                       +" and c.contno = a.contno"
	                                       +" and c.riskcode = d.riskcode"
	                                       +" fetch first 1 rows only)"
	                              +" when 'Y' then '05'"
	                              +" when 'I' then '01'"
	                              +" else ''"
	                              +" end ),"
							    + " ( select riskcode"
							      + " from lcpol c"
							      + " where c.mainpolno = c.polno"
							        + " and c.contno = a.contno "
							        + " fetch first 1 rows only),"
							    + " date('" + dCurrentDate + "') + 1 days "
						+ " from lccont a, lcpolprint b"
						+ " where a.contno = b.mainpolno"
						  + " and a.conttype = '1'"
						  + " and a.appflag = '1' and  a.SaleChnl  = '03' "
						  + " and( ( b.makedate =  date('"+ dCurrentDate + "') - 6 days  and a.GetPolMode = '1' ) or ( b.makedate =  date('"+ dCurrentDate + "') - 2 days  and a.GetPolMode = '2' )  ) "
						  +" and exists (select 'Y' from lmriskapp e,lcpol f where "
						  +" e.riskperiod='L' and e.riskcode=f.riskcode and f.contno=a.contno and f.mainpolno = f.polno fetch first 1 rows only) "
						  +" and exists (select 'Y' from LCAppnt g where g.contno = a.contno and g.AppntNo=a.AppntNo and (g.answerflag='1' or g.answerflag is null)) "
						  +" and not exists (select 'Y' from LIAnswerInfo where OtherNo=a.contno) "
						  + " union "
							+ " select  a.contno,"
							        + " a.appntno,"
							        + " a.ManageCom,"
								    + " a.AgentCode,"
								    + " a.AppntName,"
								    + " '07',"
								    + " ( select riskcode"
								      + " from lcpol c"
								      + " where c.mainpolno = c.polno"
								        + " and c.contno = a.contno "
								        + " fetch first 1 rows only),"
								    + " date('" + dCurrentDate + "')+ 1 days "
							+ " from lccont a where a.SaleChnl  = '07' and a.appflag = '1' and a.signdate= date('"+dCurrentDate
		                    +"') and exists (select 'Y' from lmriskapp e,lcpol f where "
		                    +" e.riskperiod='L' and e.risktype='A' and e.riskcode=f.riskcode and f.contno=a.contno and f.mainpolno = f.polno fetch first 1 rows only)"
			                +" and exists (select 'Y' from LCAppnt g where g.contno = a.contno and g.AppntNo=a.AppntNo and (g.answerflag='1' or g.answerflag is null)) "
							+" and not exists (select 'Y' from LIAnswerInfo where OtherNo=a.contno) fetch first 1 rows only ";

		SSRS trs = new SSRS();
		ExeSQL texesql = new ExeSQL();
		trs = texesql.execSQL(tsql);
		texesql.execUpdateSQL("alter session set nls_date_format='yyyy-mm-dd'");
		for (int i = 1; i <= trs.getMaxRow(); i++) {
			/** 区别是否是新契约,避免重复抽档**/
			LIAnswerInfoDB dLIAnswerInfoDB = new LIAnswerInfoDB();
			LIAnswerInfoSet dLIAnswerInfoSet = new LIAnswerInfoSet();
			dLIAnswerInfoSet = dLIAnswerInfoDB.executeQuery("select * from LIAnswerInfo where OtherNo='"+trs.GetText(i, 1)+"'");
			if(dLIAnswerInfoSet.size() >0)
			{
				continue;
			}
			/** **/

			/** 主险为意外险的电销产品取5%的概率抽取**/
			String dContno = trs.GetText(i, 1);
			String sql = "select 'Y' from lccont a,lcpol b,lmriskapp c where a.contno='" +dContno+"' and a.SaleChnl  = '07' and "
					+" a.contno=b.contno and b.riskcode=c.riskcode and b.mainpolno = b.polno and c.risktype='A'";
			SSRS drs = new SSRS();
			drs = texesql.execSQL(sql);
			/**  5%的概率 *******************/
			if(drs.getMaxRow() != 0)
			{
				Random rad = new Random();
				int aCount = rad.nextInt(100);
				if(aCount>=5)
				{
					continue;
				}
			}
			 /**  **/

			LIAnswerInfoSchema mLIAnswerInfoSchema = new LIAnswerInfoSchema();
			String AnswerID = PubFun1.CreateMaxNo("REVISIT", 10);
			mLIAnswerInfoSchema.setAnswerID(AnswerID);
			mLIAnswerInfoSchema.setAnswerType("1");
			mLIAnswerInfoSchema.setOtherNo(trs.GetText(i, 1));
			mLIAnswerInfoSchema.setOtherNoType("00");
			mLIAnswerInfoSchema.setAnswerStatus("0");
			mLIAnswerInfoSchema.setAppntNo(trs.GetText(i, 2));
			mLIAnswerInfoSchema.setManageCom(trs.GetText(i, 3));
			mLIAnswerInfoSchema.setAppntName(trs.GetText(i, 5));
			mLIAnswerInfoSchema.setAgentCode(trs.GetText(i, 4));
			mLIAnswerInfoSchema.setSaleChnl(trs.GetText(i, 6));
			mLIAnswerInfoSchema.setRiskCode(trs.GetText(i, 7));
			mLIAnswerInfoSchema.setShouldVisitDate(trs.GetText(i, 8));
			mLIAnswerInfoSchema.setMakeDate(theCurrentDate);
			mLIAnswerInfoSchema.setMakeTime(theCurrentTime);
			mLIAnswerInfoSchema.setModifyDate(theCurrentDate);
			mLIAnswerInfoSchema.setModifyTime(theCurrentTime);

			/**dww **/
			LIAnswerInfoDB dLIAnswerInfoDB1 = new LIAnswerInfoDB();
			LIAnswerInfoSet dLIAnswerInfoSet1 = new LIAnswerInfoSet();
			String dsql = "select * from LIAnswerInfo where AppntNo='"+mLIAnswerInfoSchema.getAppntNo()+"' and Operator is not null order by makedate desc,maketime desc ";
			dLIAnswerInfoSet1 = dLIAnswerInfoDB1.executeQuery(dsql);
			if(dLIAnswerInfoSet1.size()>=1)
			{
				mLIAnswerInfoSchema.setOperator(dLIAnswerInfoSet1.get(1).getOperator());
			}

			/****/

			tLIAnswerInfoSet.add(mLIAnswerInfoSchema);
			if (tLIAnswerInfoSet.mErrors.needDealError()) {
				dealError("dealData", "存入数据出错");
				return false;
			}
			TransferData tWorkFlowTransferData = new TransferData();
			tWorkFlowTransferData.setNameAndValue("AnswerID", AnswerID);
			tWorkFlowTransferData.setNameAndValue("OtherNoType", "00");
			tWorkFlowTransferData.setNameAndValue("OtherNo", trs.GetText(i, 1));
			tWorkFlowTransferData.setNameAndValue("AppntNo", trs.GetText(i, 2));
			tWorkFlowTransferData.setNameAndValue("AnswerStatus", "0");
			tWorkFlowTransferData.setNameAndValue("AnswerType", "1");
			tWorkFlowTransferData.setNameAndValue("Operator", "");
			tWorkFlowTransferData.setNameAndValue("AnswerPerson", "");
			ActivityOperator tActivityOperator = new ActivityOperator();
			LWMissionSchema tLWMissionSchema = new LWMissionSchema();
			VData tWorkFlowVData = new VData();
			tWorkFlowVData.add(mGlobalInput);
			tWorkFlowVData.add(tWorkFlowTransferData);
			tLWMissionSchema = tActivityOperator.CreateStartMission(
					"0000000010", tWorkFlowVData);
			tLWMissionSchema.setActivityID("0000003001");
			if (tLWMissionSchema != null) {
				tLWMissionSet.add(tLWMissionSchema);
			}

		}
		if (tLIAnswerInfoSet != null && tLIAnswerInfoSet.size() > 0) {
			map.put(tLIAnswerInfoSet, "INSERT");
		}
		if (tLWMissionSet != null && tLWMissionSet.size() > 0) {
			map.put(tLWMissionSet, "INSERT");
		}

		mInputData.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			dealError("submitData", "数据提交失败!");
			return false;
		}
		mInputData.clear();
		map = new MMap();
		return true;

	}

	private boolean makeSuccess(String dCurrentDate) {
		LWMissionSet mLWMissionSet = new LWMissionSet();
		String msql = " select  a.AnswerID,"
							+ " a.OtherNoType,"
							+ " a.OtherNo,"
							+ " a.AppntNo,"
							+ " a.AnswerStatus,"
							+ " a.AnswerType,"
				            + " ( select missionid "
				              + " from lwmission b "
				               +" where MissionProp1 = a.answerid "
                                +"   fetch first 1 rows only ),"
				            + " ( select submissionid "
				              + " from lwmission b "
				               +" where MissionProp1 = a.answerid "
				                +"  fetch first 1 rows only ),"
				            + " a.answerdate,"
				            + " a.answertime,"
				            + " a.TakeMode,"
				            + " a.AnswerExplain,"
				            + " a.CloseReason,"
				            + " a.Remark"
				      + " from LIAnswerInfo a "
				      + " where a.AnswerStatus = '2'"
				        + " and a.modifydate =  date('" + dCurrentDate + "') - 30 days fetch first 1 rows only ";
		SSRS mrs = new SSRS();
		ExeSQL mexesql = new ExeSQL();
		mrs = mexesql.execSQL(msql);
		for (int i = 1; i <= mrs.getMaxRow(); i++) {
			String sql = "update LIAnswerInfo set AnswerStatus = '92'," +
					                             "modifydate = '"+theCurrentDate+"'," +
					                             "modifytime = '"+theCurrentTime+"'" +
					     " where AnswerID='"+ mrs.GetText(i, 1) + "'";
			map.put(sql, "UPDATE");
			TransferData tWorkFlowTransferData = new TransferData();
			tWorkFlowTransferData
					.setNameAndValue("AnswerID", mrs.GetText(i, 1));
			tWorkFlowTransferData.setNameAndValue("OtherNoType", "00");
			tWorkFlowTransferData.setNameAndValue("OtherNo", mrs.GetText(i, 3));
			tWorkFlowTransferData.setNameAndValue("AppntNo", mrs.GetText(i, 4));
			tWorkFlowTransferData.setNameAndValue("AnswerStatus", "92");
			tWorkFlowTransferData.setNameAndValue("AnswerType", "1");
			tWorkFlowTransferData.setNameAndValue("AnswerDate", mrs.GetText(i,
					9));
			tWorkFlowTransferData.setNameAndValue("AnswerTime", mrs.GetText(i,
					10));
			tWorkFlowTransferData.setNameAndValue("TakeMode", mrs
					.GetText(i, 11));
			tWorkFlowTransferData.setNameAndValue("AnswerExplain", mrs.GetText(
					i, 12));
			tWorkFlowTransferData.setNameAndValue("CloseReason", mrs.GetText(i,
					13));
			tWorkFlowTransferData.setNameAndValue("Remark", mrs.GetText(i, 14));
			ActivityOperator tActivityOperator = new ActivityOperator();
			LWMissionSchema tLWMissionSchema = new LWMissionSchema();
			String tMissionID = mrs.GetText(i, 7);
			String tSubMissionID = mrs.GetText(i, 8);
			VData tWorkFlowVData = new VData();
			tWorkFlowVData.add(mGlobalInput);
			tWorkFlowVData.add(tWorkFlowTransferData);
			try {
				/*
				 * if (!tActivityOperator.ExecuteMission(tMissionID,
				 * tSubMissionID, "0000003002", tWorkFlowVData)) { // @@错误处理
				 * this.mErrors.copyAllErrors(tActivityOperator.mErrors);
				 *
				 * return false; }
				 */

				// 产生执行完保全工作流待人工核保活动表任务后的任务节点
				if (tActivityOperator.CreateNextMission(tMissionID,
						tSubMissionID, "0000003002", tWorkFlowVData)) {
					VData tVData = new VData();
					tVData = tActivityOperator.getResult();
					MMap tmap = (MMap) tVData.getObjectByObjectName("MMap", 0);
					map.add(tmap);
				} else {
					this.mErrors.copyAllErrors(tActivityOperator.mErrors);

					return false;
				}

				if (tActivityOperator.DeleteMission(tMissionID, tSubMissionID,
						"0000003002", tWorkFlowVData)) {
					VData tVData = new VData();
					tVData = tActivityOperator.getResult();
					MMap tmap = (MMap) tVData.getObjectByObjectName("MMap", 0);
					map.add(tmap);
				} else {
					this.mErrors.copyAllErrors(tActivityOperator.mErrors);

					return false;
				}
			} catch (Exception ex) {
				// @@错误处理
				this.mErrors.copyAllErrors(tActivityOperator.mErrors);

				CError tError = new CError();
				tError.moduleName = "PEdorManuUWWorkFlowBL";
				tError.functionName = "dealData";
				tError.errorMessage = "工作流引擎执行保监会报表活动表任务出错!";
				this.mErrors.addOneError(tError);

				return false;
			}

		}
		if (mLWMissionSet != null && mLWMissionSet.size() > 0) {
			map.put(mLWMissionSet, "INSERT");
		}

		mInputData.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			dealError("submitData", "数据提交失败!");
			return false;
		}
		mInputData.clear();
		map = new MMap();
		return true;
	}

	private boolean makeSuccess2(String dCurrentDate) {
		LWMissionSet nLWMissionSet = new LWMissionSet();
		String nsql = " select  a.AnswerID,"
							+ " a.OtherNoType,"
							+ " a.OtherNo,"
							+ " a.AppntNo,"
							+ " a.AnswerStatus,"
							+ " a.AnswerType,"
							+ " ( select missionid "
				              + " from lwmission b "
				               +" where MissionProp1 = a.answerid "
                              +"  fetch first 1 rows only ),"
				            + " ( select submissionid "
				              + " from lwmission b "
				               +" where MissionProp1 = a.answerid "
				                +" fetch first 1 rows only ),"
				            + " a.answerdate,"
				            + " a.answertime,"
				            + " a.TakeMode,"
				            + " a.AnswerExplain,"
				            + " a.CloseReason,"
				            + " a.Remark"
				    + " from LIAnswerInfo a "
				    + " where a.AnswerStatus in ('3','5')"
				     + " and a.modifydate =  date('" + dCurrentDate + "') - 30 days fetch first 1 rows only";
		SSRS nrs = new SSRS();
		ExeSQL nexesql = new ExeSQL();
		nrs = nexesql.execSQL(nsql);
		for (int i = 1; i <= nrs.getMaxRow(); i++) {
			String sql = "";
			if ("3".equals(nrs.GetText(i, 4))) {
				sql = "update LIAnswerInfo set AnswerStatus = '93',"
						+ "modifydate = '" + theCurrentDate + "',"
						+ "modifytime = '" + theCurrentTime + "'"
						+ "where AnswerID='" + nrs.GetText(i, 1) + "'";
			} else {
				sql = "update LIAnswerInfo set AnswerStatus = '95',"
						+ "modifydate = '" + theCurrentDate + "',"
						+ "modifytime = '" + theCurrentTime + "'"
						+ "where AnswerID='" + nrs.GetText(i, 1) + "'";
			}
			map.put(sql, "UPDATE");
			TransferData tWorkFlowTransferData = new TransferData();
			tWorkFlowTransferData
					.setNameAndValue("AnswerID", nrs.GetText(i, 1));
			tWorkFlowTransferData.setNameAndValue("OtherNoType", "00");
			tWorkFlowTransferData.setNameAndValue("OtherNo", nrs.GetText(i, 3));
			tWorkFlowTransferData.setNameAndValue("AppntNo", nrs.GetText(i, 4));
			tWorkFlowTransferData.setNameAndValue("AnswerStatus", "93");
			tWorkFlowTransferData.setNameAndValue("AnswerType", "1");
			tWorkFlowTransferData.setNameAndValue("AnswerDate", nrs.GetText(i,
					9));
			tWorkFlowTransferData.setNameAndValue("AnswerTime", nrs.GetText(i,
					10));
			tWorkFlowTransferData.setNameAndValue("TakeMode", nrs
					.GetText(i, 11));
			tWorkFlowTransferData.setNameAndValue("AnswerExplain", nrs.GetText(
					i, 12));
			tWorkFlowTransferData.setNameAndValue("CloseReason", nrs.GetText(i,
					13));
			tWorkFlowTransferData.setNameAndValue("Remark", nrs.GetText(i, 14));
			ActivityOperator tActivityOperator = new ActivityOperator();
			LWMissionSchema tLWMissionSchema = new LWMissionSchema();
			String tMissionID = nrs.GetText(i, 7);
			String tSubMissionID = nrs.GetText(i, 8);
			VData tWorkFlowVData = new VData();
			tWorkFlowVData.add(mGlobalInput);
			tWorkFlowVData.add(tWorkFlowTransferData);
			try {
				/*
				 * if (!tActivityOperator.ExecuteMission(tMissionID,
				 * tSubMissionID, "0000003002", tWorkFlowVData)) { // @@错误处理
				 * this.mErrors.copyAllErrors(tActivityOperator.mErrors);
				 *
				 * return false; }
				 */

				// 产生执行完保全工作流待人工核保活动表任务后的任务节点
				if (tActivityOperator.CreateNextMission(tMissionID,
						tSubMissionID, "0000003002", tWorkFlowVData)) {
					VData tVData = new VData();
					tVData = tActivityOperator.getResult();
					MMap tmap = (MMap) tVData.getObjectByObjectName("MMap", 0);
					map.add(tmap);

				} else {
					this.mErrors.copyAllErrors(tActivityOperator.mErrors);

					return false;
				}

				if (tActivityOperator.DeleteMission(tMissionID, tSubMissionID,
						"0000003002", tWorkFlowVData)) {
					VData tVData = new VData();
					tVData = tActivityOperator.getResult();
					MMap tmap = (MMap) tVData.getObjectByObjectName("MMap", 0);
					map.add(tmap);
				} else {
					this.mErrors.copyAllErrors(tActivityOperator.mErrors);

					return false;
				}
			} catch (Exception ex) {
				// @@错误处理
				this.mErrors.copyAllErrors(tActivityOperator.mErrors);

				CError tError = new CError();
				tError.moduleName = "PEdorManuUWWorkFlowBL";
				tError.functionName = "dealData";
				tError.errorMessage = "工作流引擎执行保监会报表活动表任务出错!";
				this.mErrors.addOneError(tError);

				return false;
			}

		}

		if (nLWMissionSet != null && nLWMissionSet.size() > 0) {
			map.put(nLWMissionSet, "INSERT");
		}

		mInputData.add(map);
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			dealError("submitData", "数据提交失败!");
			return false;
		}
		mInputData.clear();
		map = new MMap();
		return true;
	}
}
