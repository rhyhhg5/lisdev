/**
 * <p>
 * ClassName: RevisitAppBL.java
 * </p>
 * <p>
 * Description: 回访任务申请
 * </p>
 * <p>
 * Copyright: Copyright (c) 2009
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author
 * @version 1.0
 * @CreateDate：2009-7-29
 */

// 包名
package com.sinosoft.lis.revisit;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.workflowengine.ActivityOperator;
import com.sinosoft.lis.pubfun.*;

public class RevisitAppBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	
	private TransferData mTransferData = new TransferData();
	
	private String OperFlag ="";

	/** 数据操作字符串 */
	private String mOperate;

	private String msql;
	
	private LIAnswerInfoSchema mLIAnswerInfoSchema ;

	/** 业务处理相关变量 */
	private MMap map = new MMap();

	public RevisitAppBL() {

	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}

		if (!prepareOutputData()) {
			return false;
		}

		PubSubmit tPubSubmit = new PubSubmit();

		System.out.println("Start RevisitAppBL Submit...");

		if (!tPubSubmit.submitData(mInputData, mOperate)) {
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}
		System.out.println("End RevisitAppBL Submit...");
		mInputData = null;
		return true;

	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * 
	 */
	private boolean getInputData(VData cInputData) {
		this.mGlobalInput = new GlobalInput();
		this.mGlobalInput.setSchema((GlobalInput) cInputData
				.getObjectByObjectName("GlobalInput", 0));
		if (this.mGlobalInput == null) {
			dealError("getInputData", "mGlobalInput为空！");
			return false;
		}
		this.msql = ((String) cInputData.getObjectByObjectName("String", 0));
		this.mLIAnswerInfoSchema= new LIAnswerInfoSchema();
		LIAnswerInfoDB tLIAnswerInfoDB = new LIAnswerInfoDB();
		tLIAnswerInfoDB.setAnswerID(((LIAnswerInfoSchema) cInputData
				.getObjectByObjectName("LIAnswerInfoSchema", 0)).getAnswerID());
		if(!tLIAnswerInfoDB.getInfo())
		{
			dealError("getInputData", "查询数据出错！");
			return false;
		}
		this.mLIAnswerInfoSchema.setSchema(tLIAnswerInfoDB.getSchema());
		this.mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
		if (this.mTransferData == null) {
			dealError("getInputData", "mTransferData为空！");
			return false;
		}
		this.OperFlag = (String)mTransferData.getValueByName("OperFlag");
		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		//map.put(msql, "UPDATE");
		//如果状态为预约的话，则更新工作流状态，记录预约工作流轨迹
		if(mLIAnswerInfoSchema.getAnswerStatus().equalsIgnoreCase("4")&&null!=OperFlag&&!OperFlag.equals("returnBack"))
		{
			if(makeBooking()){
				return true;
			}else{
				return false;
			}
		}
		LIAnswerInfoDB tLIAnswerInfoDB = new LIAnswerInfoDB();
		LIAnswerInfoSchema tLIAnswerInfoSchema = new LIAnswerInfoSchema();
		tLIAnswerInfoDB.setAnswerID(mLIAnswerInfoSchema.getAnswerID());
		if(tLIAnswerInfoDB.getInfo()){
			tLIAnswerInfoSchema = tLIAnswerInfoDB.getSchema();
		}else{
			return false;
		}

		String sql2 = "";
		if ("returnBack".equals(OperFlag)) {
			sql2 = "select * from LIAnswerInfo a where AppntNo='"
					+ tLIAnswerInfoSchema.getAppntNo() + "' and  a.operator='"
					+ mGlobalInput.Operator
					+ "' and a.AnswerStatus in('1','4') "
					+ " and a.managecom like '" + mGlobalInput.ComCode + "%'";
		} else {
			sql2 = "select * from LIAnswerInfo a where AppntNo='"
					+ tLIAnswerInfoSchema.getAppntNo()
					+ "' and  (a.operator is null or a.operator ='') and a.AnswerStatus in('0','4') "
					+ " and a.managecom like '" + mGlobalInput.ComCode + "%'";
		}
		LIAnswerInfoSet tLIAnswerInfoSet = new LIAnswerInfoSet();
		LIAnswerInfoSet tLIAnswerInfoSet1 = new LIAnswerInfoSet();
		LIAnswerInfoSchema dLIAnswerInfoSchema = new LIAnswerInfoSchema();
		tLIAnswerInfoSet = tLIAnswerInfoDB.executeQuery(sql2);
		for (int i = 1; i <= tLIAnswerInfoSet.size(); i++) {
			dLIAnswerInfoSchema = new LIAnswerInfoSchema();
			dLIAnswerInfoSchema = tLIAnswerInfoSet.get(i);
			if("returnBack".equals(OperFlag))
			{
			    dLIAnswerInfoSchema.setOperator("");
				dLIAnswerInfoSchema.setAnswerStatus("0");
			}else
			{
			    dLIAnswerInfoSchema.setOperator(mGlobalInput.Operator);
				dLIAnswerInfoSchema.setAnswerStatus("1");
			}
			tLIAnswerInfoSet1.add(dLIAnswerInfoSchema);
		}
			map.put(tLIAnswerInfoSet1, "UPDATE");

		return true;
	}

	private boolean prepareOutputData() {
		try {
			this.mInputData.add(this.map);
		} catch (Exception ex) {
			// @@错误处理
			dealError("prepareData", "在准备往后层处理所需要的数据时出错。");
			return false;
		}
		return true;
	}

	public static void main(String[] args) {

	}

	private void dealError(String FuncName, String ErrMsg) {
		CError tError = new CError();
		tError.moduleName = "AmlRiskBL";
		tError.functionName = FuncName;
		tError.errorMessage = ErrMsg;
		mErrors.addOneError(tError);
	}
	private boolean makeBooking(){
		
		
		
		LIAnswerInfoDB tLIAnswerInfoDB = new LIAnswerInfoDB();
		LIAnswerInfoSchema tLIAnswerInfoSchema = new LIAnswerInfoSchema();
		tLIAnswerInfoDB.setAnswerID(mLIAnswerInfoSchema.getAnswerID());
		if(tLIAnswerInfoDB.getInfo()){
			tLIAnswerInfoSchema = tLIAnswerInfoDB.getSchema();
		}else{
			return false;
		}
		String sql1 = "select * from LIAnswerInfo a where AppntNo='"+tLIAnswerInfoSchema.getAppntNo()
		+"' and  (a.operator is null or a.operator ='') and a.AnswerStatus in('0','4') "
		+" and a.managecom like '"+mGlobalInput.ComCode+"%'";
		
		LIAnswerInfoSet tLIAnswerInfoSet = new LIAnswerInfoSet();
		
		tLIAnswerInfoSet = tLIAnswerInfoDB.executeQuery(sql1);
		
		for (int i = 1; i <= tLIAnswerInfoSet.size(); i++) {
            
			tLIAnswerInfoSchema = new LIAnswerInfoSchema();
			tLIAnswerInfoSchema = tLIAnswerInfoSet.get(i);
			TransferData tWorkFlowTransferData = new TransferData();
			tWorkFlowTransferData.setNameAndValue("AnswerID",
					tLIAnswerInfoSchema.getAnswerID());
			tWorkFlowTransferData.setNameAndValue("OtherNoType", "00");
			tWorkFlowTransferData.setNameAndValue("OtherNo",
					tLIAnswerInfoSchema.getOtherNo());
			tWorkFlowTransferData.setNameAndValue("AppntNo",
					tLIAnswerInfoSchema.getAppntNo());
			tWorkFlowTransferData.setNameAndValue("AnswerStatus", "1");
			tWorkFlowTransferData.setNameAndValue("AnswerType", "1");
			if (tLIAnswerInfoSchema.getAnswerPerson() == null) {
				tWorkFlowTransferData.setNameAndValue("AnswerPerson", "");
			} else {
				tWorkFlowTransferData.setNameAndValue("AnswerPerson",
						tLIAnswerInfoSchema.getAnswerPerson());
			}
			if (tLIAnswerInfoSchema.getAnswerDate() == null) {
				tWorkFlowTransferData.setNameAndValue("AnswerDate", "");
			} else {
				tWorkFlowTransferData.setNameAndValue("AnswerDate",
						tLIAnswerInfoSchema.getAnswerDate());
			}
			if (tLIAnswerInfoSchema.getAnswerTime() == null) {
				tWorkFlowTransferData.setNameAndValue("AnswerTime", "");
			} else {
				tWorkFlowTransferData.setNameAndValue("AnswerTime",
						tLIAnswerInfoSchema.getAnswerTime());
			}
			if (tLIAnswerInfoSchema.getTakeMode() == null) {
				tWorkFlowTransferData.setNameAndValue("TakeMode", "");
			} else {
				tWorkFlowTransferData.setNameAndValue("TakeMode",
						tLIAnswerInfoSchema.getTakeMode());
			}
			if (tLIAnswerInfoSchema.getAnswerExplain() == null) {
				tWorkFlowTransferData.setNameAndValue("AnswerExplain", "");
			} else {
				tWorkFlowTransferData.setNameAndValue("AnswerExplain",
						tLIAnswerInfoSchema.getAnswerExplain());
			}
			if (tLIAnswerInfoSchema.getCloseReason() == null) {
				tWorkFlowTransferData.setNameAndValue("CloseReason", "");
			} else {
				tWorkFlowTransferData.setNameAndValue("CloseReason",
						tLIAnswerInfoSchema.getCloseReason());
			}
			if (tLIAnswerInfoSchema.getRemark() == null) {
				tWorkFlowTransferData.setNameAndValue("Remark", "");
			} else {
				tWorkFlowTransferData.setNameAndValue("Remark",
						tLIAnswerInfoSchema.getRemark());
			}
			ActivityOperator tActivityOperator = new ActivityOperator();
			LWMissionSchema tLWMissionSchema = new LWMissionSchema();
			String sql = "select a.MissionID,a.SubMissionID from lwmission a where a.missionprop1="
					+ tLIAnswerInfoSchema.getAnswerID()
					+ " and a.activityid='0000003002'";
			SSRS nrs = new SSRS();
			ExeSQL exesql = new ExeSQL();
			nrs = exesql.execSQL(sql);
			if (null == nrs || nrs.getMaxRow() < 1) {
				dealError("makeBooking", "查询工作流数据出错，请检查！");
				return false;
			}
			String tMissionID = nrs.GetText(1, 1);
			String tSubMissionID = nrs.GetText(1, 2);
			VData tWorkFlowVData = new VData();
			tWorkFlowVData.add(mGlobalInput);
			tWorkFlowVData.add(tWorkFlowTransferData);
			try {
				/*
				 * if (!tActivityOperator.ExecuteMission(tMissionID,
				 * tSubMissionID, "0000003001", tWorkFlowVData)) { // @@错误处理
				 * this.mErrors.copyAllErrors(tActivityOperator.mErrors);
				 * 
				 * return false; }
				 */

				// 产生执行完保全工作流待人工核保活动表任务后的任务节点
				if (tActivityOperator.CreateNextMission(tMissionID,
						tSubMissionID, "0000003002", tWorkFlowVData)) {
					VData tVData = new VData();
					tVData = tActivityOperator.getResult();
					MMap tmap = (MMap) tVData.getObjectByObjectName("MMap", 0);
					map.add(tmap);

				} else {
					this.mErrors.copyAllErrors(tActivityOperator.mErrors);

					return false;
				}
				if (tActivityOperator.DeleteMission(tMissionID, tSubMissionID,
						"0000003002", tWorkFlowVData)) {
					VData tVData = new VData();
					tVData = tActivityOperator.getResult();
					MMap tmap = (MMap) tVData.getObjectByObjectName("MMap", 0);
					map.add(tmap);
				} else {
					this.mErrors.copyAllErrors(tActivityOperator.mErrors);

					return false;
				}
			} catch (Exception ex) {
				// @@错误处理
				this.mErrors.copyAllErrors(tActivityOperator.mErrors);

				CError tError = new CError();
				tError.moduleName = "PEdorManuUWWorkFlowBL";
				tError.functionName = "dealData";
				tError.errorMessage = "工作流引擎执行保监会报表活动表任务出错!";
				this.mErrors.addOneError(tError);

				return false;
			}
		}
		return true;
	}
}
