/**
 * <p>
 * ClassName: RedealDetBL.java
 * </p>
 * <p>
 * Description: 转办任务处理
 * </p>
 * <p>
 * Copyright: Copyright (c) 2009
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author liwb
 * @version 1.0
 * @CreateDate：2009-3-5
 */

// 包名
package com.sinosoft.lis.revisit;

import com.sinosoft.lis.db.LIAnswerInfoDB;
import com.sinosoft.lis.db.LIIssuePolDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAppntSchema;
import com.sinosoft.lis.schema.LIAnswerInfoSchema;
import com.sinosoft.lis.schema.LIAnswerInfoTraceSchema;
import com.sinosoft.lis.schema.LIIssuePolSchema;
import com.sinosoft.lis.vschema.LIIssuePolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.workflowengine.ActivityOperator;

public class RedealDetBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	/** 操作员 */
	private String mOperator = "";

	/** 转办类型 */
	private String mAnswerStatus = "";

	/** 业务处理相关变量 */
	private LIAnswerInfoSchema mLIAnswerInfoSchema = new LIAnswerInfoSchema();

	private LIAnswerInfoSchema mmLIAnswerInfoSchema = new LIAnswerInfoSchema();

	private LIAnswerInfoSchema mlLIAnswerInfoSchema = new LIAnswerInfoSchema();

	private LIIssuePolSet mLIIssuePolSet = new LIIssuePolSet();

	private LIIssuePolSet mmLIIssuePolSet = new LIIssuePolSet();

	private Reflections mReflections = new Reflections();

	private MMap map = new MMap();

	private TransferData mTransferData = new TransferData();
	
	private String CurrentDate = PubFun.getCurrentDate();
	private String CurrentTime = PubFun.getCurrentTime();
	private LCAppntSchema mLCAppntSchema = new LCAppntSchema();

	public RedealDetBL() {

	}

	// ==========================================================================

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println("\t@> RedealDetBL.submitData() 开始!");
		// 将操作数据拷贝到本类中
		this.mInputData = cInputData;

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}
		// 进行数据校验
		if (!checkData()) {
			return false;
		}
		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		// 准备输出数据
		if (!prepareOutputData()) {
			return false;
		}

		PubSubmit tPubSubmit = new PubSubmit();

		System.out.println("Start RedealDetBL PubSubmit...");

		if (!tPubSubmit.submitData(mResult, null)) {
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}
		map = new MMap();
		System.out.println("End RedealDetBL PubSubmit...");
		
		// 进行工作流处理
		if ("2".equals(mAnswerStatus)) {
			// 如果是问题件，校验是否所有问题件处理完毕
			if (checkAnswerData()) {
				if (!dealForWorkFlow()) {
					return false;
				}
				// 处理回访信息，需要将回访状态置为'99'成功状态
				//if (!dealForLIAnswerInfo()) {
					//return false;
				//}
				//map.put(mmLIAnswerInfoSchema, "UPDATE");
				mResult.add(map);
				tPubSubmit = new PubSubmit();
				if (!tPubSubmit.submitData(mResult, null)) {
					this.mErrors.copyAllErrors(tPubSubmit.mErrors);
					return false;
				}
			}
		} else {
			if (!dealForWorkFlow()) {
				return false;
			}
			// 处理回访信息，需要将数据置为初始状态
			//if (!dealForLIAnswerInfo()) {
				//return false;
			//}
			//map.put(mmLIAnswerInfoSchema, "UPDATE");
			mResult.add(map);
			tPubSubmit = new PubSubmit();
			if (!tPubSubmit.submitData(mResult, null)) {
				this.mErrors.copyAllErrors(tPubSubmit.mErrors);
				return false;
			}
		}

		mInputData = null;
		System.out.println("\t@> RedealDetBL.submitData() 结束!");
		return true;

	}// function submitData end

	// ==========================================================================

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * 
	 */
	private boolean getInputData() {
		System.out.println("\t@> RedealDetBL.getInputData() 开始!");
		mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		mLIAnswerInfoSchema = (LIAnswerInfoSchema) mInputData
				.getObjectByObjectName("LIAnswerInfoSchema", 0);
		if (mLIAnswerInfoSchema == null || "".equals(mLIAnswerInfoSchema)) {
			CError tError = new CError();
			tError.moduleName = "RedealDetBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据 LIAnswerInfoSchema 失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mAnswerStatus =(String)mTransferData.getValueByName( "AnswerStatus" ); // mLIAnswerInfoSchema.getAnswerStatus();
		if ("2".equals(mAnswerStatus)) {
			mLIIssuePolSet = (LIIssuePolSet) mInputData.getObjectByObjectName(
					"LIIssuePolSet", 0);
			if (mLIIssuePolSet == null || mLIIssuePolSet.size() < 0) {
				CError tError = new CError();
				tError.moduleName = "RedealDetBL";
				tError.functionName = "getInputData";
				tError.errorMessage = "前台传输全局公共数据 LIAnswerInfoSchema 失败!";
				this.mErrors.addOneError(tError);
				return false;
			}
		}
		mOperator = mGlobalInput.Operator;
		if (mOperator == null || "".equals(mOperator)) {
			CError tError = new CError();
			tError.moduleName = "RedealDetBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据 Operator 失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		this.mLCAppntSchema.setSchema((LCAppntSchema)mInputData
				.getObjectByObjectName("LCAppntSchema", 0));
		System.out.println("\t@> RedealDetBL.getInputData() 结束!");
		return true;
	}// function getInputData end

	// ==========================================================================

	/**
	 * 根据传入的数据进行业务逻辑层的合法性校验
	 * 
	 * @return boolean
	 */

	private boolean checkData() {
		System.out.println("\t@> RedealDetBL.checkData() 开始!");
		System.out.println("\t@> RedealDetBL.checkData() 结束!");
		return true;
	}// function checkData end

	// ==========================================================================

	/**
	 * 根据传入的数据进行业务逻辑层的合法性校验
	 * 
	 * @return boolean
	 */

	private boolean dealForLIAnswerInfo() {
		System.out.println("\t@> RedealDetBL.dealForLIAnswerInfo() 开始!");
	/*	if ("0".equals(mAnswerStatus)||"4".equals(mAnswerStatus)) {
			mmLIAnswerInfoSchema.setAnswerStatus("0");
		} else {*/
			mReflections
					.transFields(mlLIAnswerInfoSchema, mmLIAnswerInfoSchema);
			mlLIAnswerInfoSchema.setAnswerStatus(mAnswerStatus);
			mlLIAnswerInfoSchema.setAnswerDate("");
			mlLIAnswerInfoSchema.setAnswerTime("");
			mlLIAnswerInfoSchema.setAnswerPerson("");
			mlLIAnswerInfoSchema.setBookingDate("");
			mlLIAnswerInfoSchema.setBookingTime("");
			mlLIAnswerInfoSchema.setShouldVisitDate(CurrentDate);
			mlLIAnswerInfoSchema.setUrgencySign("");
			mlLIAnswerInfoSchema.setTakeMode("");
			mlLIAnswerInfoSchema.setAnswerExplain("");
			mlLIAnswerInfoSchema.setCloseReason("");
			mlLIAnswerInfoSchema.setRemark("");
			mlLIAnswerInfoSchema.setOperator("");
			mlLIAnswerInfoSchema.setMakeDate(CurrentDate);
			mlLIAnswerInfoSchema.setMakeTime(CurrentTime);
			mlLIAnswerInfoSchema.setModifyDate(CurrentDate);
			mlLIAnswerInfoSchema.setModifyTime(CurrentTime);
		//}
		System.out.println("\t@> RedealDetBL.dealForLIAnswerInfo() 结束!");
		return true;
	}// function dealForLIAnswerInfo end

	// ==========================================================================

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		System.out.println("\t@> RedealDetBL.dealData() 开始!");
		LIAnswerInfoDB tLIAnswerInfoDB = new LIAnswerInfoDB();
		tLIAnswerInfoDB.setAnswerID(mLIAnswerInfoSchema.getAnswerID());
		if (!tLIAnswerInfoDB.getInfo()) {
			CError tError = new CError();
			tError.moduleName = "RedealDetBL";
			tError.functionName = "dealData";
			tError.errorMessage = "没有查询到回访信息!";
			this.mErrors.addOneError(tError);
			return false;
		}
		String dAnswerStatus= mLIAnswerInfoSchema.getAnswerStatus();
		mmLIAnswerInfoSchema = tLIAnswerInfoDB.getSchema();
		// 将界面传入信息添加到schema中
		mmLIAnswerInfoSchema.setAnswerDate(CurrentDate);
		mmLIAnswerInfoSchema.setAnswerTime(CurrentTime);
		mmLIAnswerInfoSchema.setAnswerPerson(mGlobalInput.Operator);
		mmLIAnswerInfoSchema.setBookingDate(mLIAnswerInfoSchema.getBookingDate());
		mmLIAnswerInfoSchema.setBookingTime(mLIAnswerInfoSchema.getBookingTime());
		mmLIAnswerInfoSchema.setAnswerStatus(dAnswerStatus);
		mmLIAnswerInfoSchema.setRemark(mLIAnswerInfoSchema.getRemark());
		mmLIAnswerInfoSchema.setTakeMode(mLIAnswerInfoSchema.getTakeMode());
		mmLIAnswerInfoSchema.setAnswerExplain(mLIAnswerInfoSchema
				.getAnswerExplain());
		mmLIAnswerInfoSchema.setModifyDate(CurrentDate);
		mmLIAnswerInfoSchema.setModifyTime(CurrentTime);
		mmLIAnswerInfoSchema.setCloseReason(mLIAnswerInfoSchema.getCloseReason());
		if("0".equals(dAnswerStatus) || "4".equals(dAnswerStatus))
		{
			mmLIAnswerInfoSchema.setOperator("");
		}	
		// 如果是问题件转办，还要处理问题件信息
		if (mAnswerStatus != null && mAnswerStatus.equals("2")) {

			// 循环取出界面传入信息，更新数据库中的信息
			for (int i = 1; i <= mLIIssuePolSet.size(); i++) {
				LIIssuePolDB tLIIssuePolDB = new LIIssuePolDB();
				tLIIssuePolDB.setAnswerID(mLIIssuePolSet.get(i).getAnswerID());
				tLIIssuePolDB
						.setIssueType(mLIIssuePolSet.get(i).getIssueType());
				if (!tLIIssuePolDB.getInfo()) {
					CError tError = new CError();
					tError.moduleName = "RedealDetBL";
					tError.functionName = "dealData";
					tError.errorMessage = "没有查询到问题件信息!";
					this.mErrors.addOneError(tError);
					return false;
				}
				if (mLIIssuePolSet.get(i).getIssueStutas().equals("0")) {
					CError tError = new CError();
					tError.moduleName = "RedealDetBL";
					tError.functionName = "dealData";
					tError.errorMessage = "问题件信息还没有处理完成!";
					this.mErrors.addOneError(tError);
					return false;
				}
				
				LIIssuePolSchema tLIIssuePolSchema = new LIIssuePolSchema();
				tLIIssuePolSchema = tLIIssuePolDB.getSchema();

				tLIIssuePolSchema.setIssueType(mLIIssuePolSet.get(i)
						.getIssueType());
				tLIIssuePolSchema.setIssueStutas(mLIIssuePolSet.get(i)
						.getIssueStutas());
				tLIIssuePolSchema.setIssueExplain(mLIIssuePolSet.get(i)
						.getIssueExplain());
				tLIIssuePolSchema.setModifyDate(CurrentDate);
				tLIIssuePolSchema.setModifyTime(CurrentTime);

				mmLIIssuePolSet.add(tLIIssuePolSchema);
			}
		}
		System.out.println("\t@> RedealDetBL.dealData() 结束!");
		/**
		 * 回访轨迹
		 */
		Reflections tReflections = new Reflections();
		LIAnswerInfoTraceSchema tLIAnswerInfoTraceSchema = new LIAnswerInfoTraceSchema();
		tReflections
		.transFields(tLIAnswerInfoTraceSchema, mmLIAnswerInfoSchema);
		tLIAnswerInfoTraceSchema.setTraceType("02");
		tLIAnswerInfoTraceSchema.setSerialNo(PubFun1.CreateMaxNo("ANSWERINFOTRACE", 20));
		map.put(tLIAnswerInfoTraceSchema,"INSERT");
		
		/**
		 * 回访电话类型和回访电话的更新
		 */
	    String sqlLCAppnt = "update LCAppnt set AnswerPhoneType='"+mLCAppntSchema.getAnswerPhoneType()
	     +"',AnswerPhone='"+mLCAppntSchema.getAnswerPhone()+"' where contno='"+mLCAppntSchema.getContNo()
	      +"' and AppntNo='"+mLCAppntSchema.getAppntNo()+"'";
	    map.put(sqlLCAppnt,"UPDATE");
		
		return true;
	}// function dealData end

	// ==========================================================================

	/**
	 * 根据传入的数据进行业务逻辑层的合法性校验
	 * 
	 * @return boolean
	 */

	private boolean dealForWorkFlow() {
		System.out.println("\t@> RedealDetBL.dealForWorkFlow() 开始!");
		mTransferData.removeByName("AnswerStatus");
		// 准备工作流所需数据
		mTransferData.setNameAndValue("AnswerID", mmLIAnswerInfoSchema
				.getAnswerID());
		mTransferData.setNameAndValue("OtherNoType", mmLIAnswerInfoSchema
				.getOtherNoType());
		mTransferData.setNameAndValue("OtherNo", mmLIAnswerInfoSchema
				.getOtherNo());
		mTransferData.setNameAndValue("AppntNo", mmLIAnswerInfoSchema
				.getAppntNo());
		mTransferData.setNameAndValue("Operator", mOperator);
		mTransferData.setNameAndValue("AnswerPerson", mOperator);
	/*	if (mAnswerStatus != null && mAnswerStatus.equals("2")) {
			mTransferData.setNameAndValue("AnswerStatus", "99");
		} else {*/
			mTransferData.setNameAndValue("AnswerStatus", mmLIAnswerInfoSchema.getAnswerStatus());
		//}
		mTransferData.setNameAndValue("AnswerType", mmLIAnswerInfoSchema
				.getAnswerType());
		mTransferData.setNameAndValue("AnswerDate", CurrentDate);
		mTransferData.setNameAndValue("AnswerTime", CurrentTime);
		mTransferData.setNameAndValue("TakeMode", mmLIAnswerInfoSchema
				.getTakeMode());
		mTransferData.setNameAndValue("AnswerExplain", mmLIAnswerInfoSchema
				.getAnswerExplain());
		mTransferData.setNameAndValue("CloseReason", "");
		mTransferData.setNameAndValue("Remark", mmLIAnswerInfoSchema
				.getRemark());
		String tMissionID = (String) mTransferData.getValueByName("MissionID");
		ExeSQL exesql = new ExeSQL();
		String tSubMissionID = exesql
				.getOneValue("select a.SubMissionID from lwmission a where a.missionid = '"
						+ tMissionID + "'");
		String mOperate = "0000003002";// 转办节点
		mInputData.add(mTransferData);
		mResult.clear();
		ActivityOperator tActivityOperator = new ActivityOperator();
		try {
			if (tActivityOperator.CreateNextMission(tMissionID, tSubMissionID,
					mOperate, mInputData)) {
				VData tempVData = new VData();
				tempVData = tActivityOperator.getResult();
				if ((tempVData != null) && (tempVData.size() > 0)) {
					MMap map1 = (MMap) tempVData.getObjectByObjectName("MMap",
							0);
					map.add(map1);
					tempVData = null;
				}
			} else {
				this.mErrors.copyAllErrors(tActivityOperator.mErrors);
				return false;
			}

			tActivityOperator = new ActivityOperator();
			if (tActivityOperator.DeleteMission(tMissionID, tSubMissionID,
					mOperate, mInputData)) {
				VData tempVData = new VData();
				tempVData = tActivityOperator.getResult();
				if ((tempVData != null) && (tempVData.size() > 0)) {
					MMap map2 = (MMap) tempVData.getObjectByObjectName("MMap",
							0);
					map.add(map2);
					tempVData = null;
				}
			} else {
				this.mErrors.copyAllErrors(tActivityOperator.mErrors);
				return false;
			}
		} catch (Exception ex) {
			// @@错误处理
			this.mErrors.copyAllErrors(tActivityOperator.mErrors);
			CError tError = new CError();
			tError.moduleName = "RedealDetBL";
			tError.functionName = "dealForWorkFlow";
			tError.errorMessage = "执行回访工作流活动表任务出错!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("\t@> RedealDetBL.dealForWorkFlow() 结束!");
		return true;
	}// function dealForWorkFlow end

	// ==========================================================================

	/**
	 * 判断是否所有问题件都已经解决
	 * 
	 * @return boolean
	 */

	private boolean checkAnswerData() {
		System.out.println("\t@> RedealDetBL.checkAnswerData() 开始!");
		if (mmLIIssuePolSet != null && mmLIIssuePolSet.size() > 0) {
			for (int i = 1; i <= mmLIIssuePolSet.size(); i++) {
				if (mmLIIssuePolSet.get(i).getIssueStutas().equals("0")) {
					return false;
				}
			}
		}
		System.out.println("\t@> RedealDetBL.checkAnswerData() 结束!");
		return true;
	}// function checkAnswerData end

	// ==========================================================================
	/**
	 * 准备经过本类处理的输出数据
	 * 
	 * @return boolean
	 */

	private boolean prepareOutputData() {
		System.out.println("\t@> RedealDetBL.prepareOutputData() 开始!");
		mResult.clear();
		map.put(mmLIAnswerInfoSchema, "UPDATE");
		map.put(mmLIIssuePolSet, "UPDATE");
		mResult.add(map);
		System.out.println("\t@> RedealDetBL.prepareOutputData() 结束!");
		return true;
	}// function prepareOutputData end

	// ==========================================================================

	/**
	 * 返回经过本类处理的数据结果
	 * 
	 * @return VData
	 */
	public VData getResult() {
		System.out.println("\t@> RedealDetBL.getResult() 开始!");
		System.out.println("\t@> RedealDetBL.getResult() 结束!");
		return mResult;
	} // function getResult end

	// ==========================================================================

	/**
	 * 调试主程序业务方法
	 * 
	 * @param String[]
	 */
	public static void main(String[] args) {

	}// function main end

	// ==========================================================================
}
