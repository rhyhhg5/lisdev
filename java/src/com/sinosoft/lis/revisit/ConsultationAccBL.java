package com.sinosoft.lis.revisit;

/**
 * <p>ClassName: ConsultationAccBL.java </p>
 * <p>Description: 咨诉受理 </p>
 * <p>Copyright: Copyright (c) 2009 </p>
 * <p>Company: </p>
 * @author
 * @version 1.0
 * @CreateDate：2009-8-6
 */

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class ConsultationAccBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 全局数据 */
	private GlobalInput mGlobalInput;

	/** 数据操作字符串 */
	private String mOperate;

	/** 业务处理相关变量 */
	private LIComplaintsInfoSchema tLIComplaintsInfoSchema;

	private LIComplaintsInfoSet tLIComplaintsInfoSet;

	private String theCurrentDate = PubFun.getCurrentDate();

	private String theCurrentTime = PubFun.getCurrentTime();

	private MMap map = new MMap();

	public ConsultationAccBL() {

	}

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 将操作数据拷贝到本类中
		this.mOperate = cOperate;

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!checkData()) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}

		if (!prepareOutputData()) {
			return false;
		}

		PubSubmit tPubSubmit = new PubSubmit();

		System.out.println("Start ConsultationAccBL Submit...");

		if (!tPubSubmit.submitData(mInputData, null)) {
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}

		System.out.println("End ConsultationAccBL Submit...");
		mInputData = null;
		return true;

	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * 
	 */
	private boolean getInputData(VData cInputData) {
		mGlobalInput = new GlobalInput();
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));

		if (this.mGlobalInput == null) {
			dealError("getInputData", "mGlobalInput为空！");
			return false;
		}
		tLIComplaintsInfoSchema = new LIComplaintsInfoSchema();
		tLIComplaintsInfoSchema.setSchema((LIComplaintsInfoSchema) cInputData
				.getObjectByObjectName("LIComplaintsInfoSchema", 0));
		return true;
	}

	private boolean checkData() {
		return true;
	}

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		tLIComplaintsInfoSet = new LIComplaintsInfoSet();
		String aComplaintID = PubFun1.CreateMaxNo("REVISIT", 10);
		if (tLIComplaintsInfoSchema.getNeedDealFlag().equalsIgnoreCase("01")) {
			tLIComplaintsInfoSchema.setIssueState("00");
			//如果转交机构选择了本人，则在转交机构上置上本人的机构，和本人为处理人，状态置为问题件处理中
			if(tLIComplaintsInfoSchema.getTransmitCom().equalsIgnoreCase("5555")){
				tLIComplaintsInfoSchema.setTransmitCom(mGlobalInput.ComCode);
				tLIComplaintsInfoSchema.setDealOperator(mGlobalInput.Operator);
				tLIComplaintsInfoSchema.setIssueState("01");
			}
		} else if (tLIComplaintsInfoSchema.getNeedDealFlag().equalsIgnoreCase(
				"02")) {
			tLIComplaintsInfoSchema.setIssueState("03");
			if(tLIComplaintsInfoSchema.getTransmitCom().equalsIgnoreCase("5555")){
				tLIComplaintsInfoSchema.setTransmitCom(mGlobalInput.ComCode);
				tLIComplaintsInfoSchema.setDealOperator(mGlobalInput.Operator);
			}
		}
		tLIComplaintsInfoSchema.setComplaintID(aComplaintID);
		tLIComplaintsInfoSchema.setOtherNoType("00");
		tLIComplaintsInfoSchema.setTransmitDate(theCurrentDate);
		tLIComplaintsInfoSchema.setTransmitTime(theCurrentTime);
		tLIComplaintsInfoSchema.setMakeDate(theCurrentDate);
		tLIComplaintsInfoSchema.setMakeTime(theCurrentTime);
		tLIComplaintsInfoSchema.setModifyDate(theCurrentDate);
		tLIComplaintsInfoSchema.setModifyTime(theCurrentTime);
		tLIComplaintsInfoSet.add(tLIComplaintsInfoSchema);
		map.put(tLIComplaintsInfoSet, "INSERT");

		return true;
	}

	private boolean prepareOutputData() {
		try {
			this.mInputData.add(this.map);
		} catch (Exception ex) {
			// @@错误处理
			dealError("prepareData", "在准备往后层处理所需要的数据时出错。");
			return false;
		}
		return true;
	}

	public static void main(String[] args) {

	}

	private void dealError(String FuncName, String ErrMsg) {
		CError tError = new CError();
		tError.moduleName = "RevisitTaskBL";
		tError.functionName = FuncName;
		tError.errorMessage = ErrMsg;
		mErrors.addOneError(tError);
	}
}
