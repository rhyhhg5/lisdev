/**
 * <p>
 * ClassName: RedealAppBL.java
 * </p>
 * <p>
 * Description: 转办件任务申请
 * </p>
 * <p>
 * Copyright: Copyright (c) 2009
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author liwb
 * @version 1.0
 * @CreateDate：2009-7-31
 */

// 包名
package com.sinosoft.lis.revisit;

import com.sinosoft.lis.db.LWMissionDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LWMissionSchema;
import com.sinosoft.lis.vschema.LWMissionSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class RedealAppBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 返回数据 */
	private VData mResult = new VData();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	/** 全局数据 */
	private TransferData mTransferData = new TransferData();

	/** 操作员 */
	private String mOperator = "";

	/** 已有任务的操作员 */
	private String mDefaultOperator = "";

	/** 工作流ID */
	private String mMissionID = "";

	/** 数据库操作函数 */
	private ExeSQL mExeSQL = new ExeSQL();

	/** 业务处理相关变量 */

	private LWMissionDB mLWMissionDB = new LWMissionDB();

	private LWMissionSet mLWMissionSet = new LWMissionSet();

	private LWMissionSet mmLWMissionSet = new LWMissionSet();

	private LWMissionSchema mLWMissionSchema = new LWMissionSchema();

	private MMap map = new MMap();

	public RedealAppBL() {

	}

	// ==========================================================================

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println("\t@> RedealAppBL.submitData() 开始!");
		// 将操作数据拷贝到本类中
		this.mInputData = cInputData;
		// this.mOperate =cOperate;

		// 得到外部传入的数据,将数据备份到本类中
		if (!getInputData()) {
			return false;
		}
		// 进行数据校验
		if (!checkData()) {
			return false;
		}
		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		// 准备输出数据
		if (!prepareOutputData()) {
			return false;
		}

		PubSubmit tPubSubmit = new PubSubmit();

		System.out.println("Start RedealAppBL Submit...");

		if (!tPubSubmit.submitData(mResult, null)) {
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			return false;
		}
		// 如果有同一被访人的单子被其它坐席锁定，返回到界面
		if (mDefaultOperator != null) {
			mResult.clear();
			TransferData tTD = new TransferData();
			tTD.setNameAndValue("DefaultOperator", String
					.valueOf(mDefaultOperator));
			mResult.add(tTD);
		}
		mInputData = null;
		System.out.println("End RedealAppBL Submit...");
		System.out.println("\t@> RedealAppBL.submitData() 结束!");
		return true;
	}// function submitData end

	// ==========================================================================

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 * 
	 */
	private boolean getInputData() {
		System.out.println("\t@> RedealAppBL.getInputData() 开始!");
		mGlobalInput.setSchema((GlobalInput) mInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mTransferData = (TransferData) mInputData.getObjectByObjectName(
				"TransferData", 0);
		mOperator = mGlobalInput.Operator;
		if (mOperator == null || "".equals(mOperator)) {
			CError tError = new CError();
			tError.moduleName = "RedealAppBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据 Operator 失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mMissionID = (String) mTransferData.getValueByName("MissionID");
		if (mMissionID == null || "".equals(mMissionID)) {
			CError tError = new CError();
			tError.moduleName = "RedealAppBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "前台传输全局公共数据 MissionID 失败!";
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("\t@> RedealAppBL.getInputData() 结束!");
		return true;
	}// function getInputData end

	// ==========================================================================

	/**
	 * 根据传入的数据进行业务逻辑层的合法性校验
	 * 
	 * @return boolean
	 */
	private boolean checkData() {
		System.out.println("\t@> RedealAppBL.checkData() 开始!");
		mLWMissionDB.setMissionID(mMissionID);
		mLWMissionDB.setActivityID("0000003002");
		mLWMissionSet = mLWMissionDB.query();
		if (mLWMissionSet != null && mLWMissionSet.size() > 0) {
			mLWMissionSchema = mLWMissionSet.get(1);
		} else {
			CError tError = new CError();
			tError.moduleName = "RedealAppBL";
			tError.functionName = "checkData";
			tError.errorMessage = "查询回访工作流数据失败!" + mMissionID;
			this.mErrors.addOneError(tError);
			return false;
		}
		System.out.println("\t@> RedealAppBL.checkData() 结束!");
		return true;
	}// function checkData end

	// ==========================================================================

	/**
	 * 根据前面的输入数据，进行BL逻辑处理 如果在处理过程中出错，则返回false,否则返回true
	 */
	private boolean dealData() {
		System.out.println("\t@> RedealAppBL.dealData() 开始!");
		LWMissionDB tLWMissionDB = new LWMissionDB();
		LWMissionSet tLWMissionSet = new LWMissionSet();
		String tSql = "";
		// 查询投保人是否还有其它任务在待访池中，如果不为空，说明有
		tSql = "select * " + "from lwmission "
				+ "where activityid = '0000003002' "
				+ "   and missionprop4 = '"
				+ mLWMissionSchema.getMissionProp4() + "' "
				+ "   and missionid != '" + mMissionID + "'"
				+ "order by missionid";
		tLWMissionSet = tLWMissionDB.executeQuery(tSql);

		// 查询投保人是否有其它任务已被其他坐席申请
		tSql = "select DefaultOperator " + "from lwmission "
				+ "where activityid = '0000003002' "
				+ "   and missionprop4 = '"
				+ mLWMissionSchema.getMissionProp4() + "' "
				+ "   and defaultoperator is not null " + "order by missionid";
		SSRS tSSRS = mExeSQL.execSQL(tSql);
		if (tSSRS != null && tSSRS.getMaxRow() > 0) {
			mDefaultOperator = tSSRS.GetText(1, 1);
		}
		// 如果DefaultOperator不为空，说明被其他坐席申请了,将同一投保人的任务分配给同一个坐席
		if (mDefaultOperator != null && !"".equals(mDefaultOperator)) {
			mOperator = mDefaultOperator;
		}
		// 将任务置给指定的坐席
		mLWMissionSchema.setMissionProp7("1");
		mLWMissionSchema.setDefaultOperator(mOperator);
		mLWMissionSchema.setModifyDate(PubFun.getCurrentDate());
		mLWMissionSchema.setModifyTime(PubFun.getCurrentTime());
		mmLWMissionSet.add(mLWMissionSchema);
		// 如果待访池中还有其他任务，则同时置上同一个坐席
		if (tLWMissionSet != null && tLWMissionSet.size() > 0) {
			LWMissionSchema tLWMissionSchema = new LWMissionSchema();
			for (int i = 1; i <= tLWMissionSet.size(); i++) {
				tLWMissionSchema = tLWMissionSet.get(i);
				tLWMissionSchema.setMissionProp7("1");
				tLWMissionSchema.setDefaultOperator(mOperator);
				tLWMissionSchema.setModifyDate(PubFun.getCurrentDate());
				tLWMissionSchema.setModifyTime(PubFun.getCurrentTime());
				mmLWMissionSet.add(tLWMissionSchema);
			}
		}
		System.out.println("\t@> RedealAppBL.dealData() 结束!");
		return true;
	}// function dealData end

	// ==========================================================================

	/**
	 * 准备经过本类处理的输出数据
	 * 
	 * @return boolean
	 */
	private boolean prepareOutputData() {
		System.out.println("\t@> RedealAppBL.prepareOutputData() 开始!");
		mResult.clear();
		map.put(mmLWMissionSet, "UPDATE");
		mResult.add(map);
		System.out.println("\t@> RedealAppBL.prepareOutputData() 结束!");
		return true;
	}// function prepareOutputData end

	// ==========================================================================

	/**
	 * 返回经过本类处理的数据结果
	 * 
	 * @return VData
	 */
	public VData getResult() {
		System.out.println("\t@> RedealAppBL.getResult() 开始!");
		System.out.println("\t@> RedealAppBL.getResult() 结束!");
		return mResult;
	} // function getResult end

	// ==========================================================================

	/**
	 * 调试主程序业务方法
	 * 
	 * @param String[]
	 */
	public static void main(String[] args) {

	}// function main end

	// ==========================================================================
}
