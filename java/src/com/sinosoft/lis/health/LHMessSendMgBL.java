/*
 * <p>ClassName: LHMessSendMgBL </p>
 * <p>Description: LHMessSendMgBL类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-09-06 17:14:38
 */
package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class LHMessSendMgBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
private MMap map= new MMap();

/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
private String tNo;
/** 业务处理相关变量 */
private LHMessSendMgSet mLHMessSendMgSet=new LHMessSendMgSet();
private LHTaskCustomerRelaSet mLHTaskCustomerRelaSet=new LHTaskCustomerRelaSet();
private TransferData mTransferData = new TransferData();

    public LHMessSendMgBL()
    {
        try
        {
            jbInit();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        TransferData mTransferData = new TransferData();
        LHMessSendMgUI tLHMessSendMgUI = new LHMessSendMgUI();
        LHMessSendMgSet mLHMessSendMgSet = new LHMessSendMgSet();

        tG.Operator = "hm";
        tG.ManageCom = "86";
        tG.ComCode = "86";
        mTransferData.setNameAndValue("ServCaseCode","20060908200117");
        mTransferData.setNameAndValue("ServTaskNo","000000002174");
        mTransferData.setNameAndValue("HmMessCode","0000000001");
        mTransferData.setNameAndValue("ReMarkExp","备注说明");

        VData tVData = new VData();
        tVData.add(mLHMessSendMgSet);
        tVData.addElement(mTransferData);
        tVData.add(tG);
        tLHMessSendMgUI.submitData(tVData,"INSERT||MAIN");
    }
/**
* 传输数据的公共方法
        * @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
         //将操作数据拷贝到本类中
         this.mOperate =cOperate;
         //得到外部传入的数据,将数据备份到本类中
         if (!getInputData(cInputData))
             return false;
         //进行业务处理
         if (!dealData())
         {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "LHMessSendMgBL";
             tError.functionName = "submitData";
             tError.errorMessage = "数据处理失败LHMessSendMgBL-->dealData!";
             this.mErrors.addOneError(tError);
             return false;
         }
         //准备往后台的数据
         if (!prepareOutputData())
             return false;
         if (this.mOperate.equals("QUERY||MAIN"))
         {
             this.submitquery();
         }
         else
         {
             PubSubmit tPubSubmit = new PubSubmit();
             System.out.println("Start LHMessSendMgBL Submit...");
             //LHMessSendMgBLS tLHMessSendMgBLS=new LHMessSendMgBLS();
             //tLHMessSendMgBLS.submitData(mInputData,mOperate);
             if (!tPubSubmit.submitData(mInputData, mOperate))
             {
                     // @@错误处理
                     this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                     CError tError = new CError();
                     tError.moduleName = "LHMessSendMgBL";
                     tError.functionName = "submitData";
                     tError.errorMessage = "数据提交失败!";

                     this.mErrors.addOneError(tError);
                     return false;
             }

             System.out.println("End LHMessSendMgBL Submit...");
             //如果有需要处理的错误，则返回
             //if (tLHMessSendMgBLS.mErrors.needDealError())
             //{
             // @@错误处理
             //this.mErrors.copyAllErrors(tLHMessSendMgBLS.mErrors);
             // CError tError = new CError();
             //tError.moduleName = "LHMessSendMgBL";
             //tError.functionName = "submitDat";
             //tError.errorMessage ="数据提交失败!";
             //this.mErrors .addOneError(tError) ;
             //return false;
             //}
         }
         mInputData=null;
         return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
    if (this.mOperate.equals("INSERT||MAIN"))
    {
        String arrTaskCustomer[][];
        SSRS SSRS_TaskCustomer = new SSRS();
        ExeSQL tTaskCustomer= new ExeSQL();
        String  sqlServTaskNo = "'"+mTransferData.getValueByName("ServTaskNo").toString().replaceAll(",","','")+"'";
        String sql= " select (select a.CustomerNo from LHServCaseRela a where a.ServCaseCode=v.ServCaseCode and a.ServItemNo=v.ServItemNo) ,"
                    +" v.TaskExecNo,v.ServTaskNo,v.ServCaseCode,v.ServTaskCode,v.ServItemNo"
                    +" from LHTaskCustomerRela v "
                    +" where v.ServTaskNo in ("+ sqlServTaskNo +")"
                    ;
        SSRS_TaskCustomer = tTaskCustomer.execSQL(sql);
        arrTaskCustomer = SSRS_TaskCustomer.getAllData();
        mLHMessSendMgSet.clear();
        if(mTransferData != null && arrTaskCustomer.length != 0)
        {
            for (int j = 1; j <= arrTaskCustomer.length; j++)
            {
                LHMessSendMgSchema tLHMessSendMgSchema = new LHMessSendMgSchema();

                tLHMessSendMgSchema.setTaskExecNo(arrTaskCustomer[j-1][1]);
                tLHMessSendMgSchema.setServTaskNo(arrTaskCustomer[j-1][2]);
                tLHMessSendMgSchema.setServCaseCode(arrTaskCustomer[j-1][3]);
                tLHMessSendMgSchema.setServTaskCode(arrTaskCustomer[j-1][4]);
                tLHMessSendMgSchema.setServItemNo(arrTaskCustomer[j-1][5]);
                tLHMessSendMgSchema.setCustomerNo(arrTaskCustomer[j-1][0]);
                tLHMessSendMgSchema.setHmMessCode(mTransferData.getValueByName("HmMessCode").toString());
                tLHMessSendMgSchema.setReMarkExp(mTransferData.getValueByName("ReMarkExp").toString());

                tLHMessSendMgSchema.setOperator(mGlobalInput.Operator);
                tLHMessSendMgSchema.setMakeDate(PubFun.getCurrentDate());
                tLHMessSendMgSchema.setMakeTime(PubFun.getCurrentTime());
                tLHMessSendMgSchema.setModifyDate(PubFun.getCurrentDate());
                tLHMessSendMgSchema.setModifyTime(PubFun.getCurrentTime());
                tLHMessSendMgSchema.setManageCom(mGlobalInput.ManageCom);

                LHServItemDB tLHServItemDB = new LHServItemDB();
                LHServItemSet tLHServItemSet = new LHServItemSet();
                tLHServItemDB.setServItemNo(tLHMessSendMgSchema.getServItemNo());
                tLHServItemSet = tLHServItemDB.query();
                if (tLHServItemSet.get(1).getServPlanNo().equals("0"))
                { //团体
                    String tGrpServPlanNo = tLHServItemSet.get(1).getGrpServPlanNo();
                    LHGrpServPlanDB tLHGrpServPlanDB = new LHGrpServPlanDB();
                    tLHGrpServPlanDB.setGrpServPlanNo(tGrpServPlanNo);
                    tLHMessSendMgSchema.setContNo(tLHGrpServPlanDB.query().get(1).getGrpContNo());
                    tLHMessSendMgSchema.setGrpContNo(tLHGrpServPlanDB.query().get(1).getGrpContNo());
                }
                else
                {
                    String tServPlanNo = tLHServItemSet.get(1).getServPlanNo();
                    LHServPlanDB tLHServPlanDB = new LHServPlanDB();
                    tLHServPlanDB.setServPlanNo(tServPlanNo);
                    tLHMessSendMgSchema.setContNo(tLHServPlanDB.query().get(1).getContNo());
                }
                mLHMessSendMgSet.add(tLHMessSendMgSchema);
            }
            map.put(mLHMessSendMgSet, "INSERT");
            for (int j = 1; j <= arrTaskCustomer.length; j++)
            {
                map.put(" update LHTASKCUSTOMERRELA a set  a.TaskExecState='"+mTransferData.getValueByName("ExecState").toString()+"',"
                        +" a.ManageCom='"+mGlobalInput.ManageCom+"', a.Operator='"+mGlobalInput.Operator+"', "
                        +" a.ModifyDate='"+PubFun.getCurrentDate()+"',a.ModifyTime='"+PubFun.getCurrentTime()  +"' "
                        +" where  a.TaskExecNo ='"+arrTaskCustomer[j-1][1]+"'","UPDATE");//将个人服务受理的服务任务换为生成的服务任务编号
            }
        }
    }

    if (this.mOperate.equals("UPDATE||MAIN"))
    {
        for (int j= 1; j<= mLHMessSendMgSet.size(); j++)
        {
            mLHMessSendMgSet.get(j).setOperator(mGlobalInput.Operator);
            mLHMessSendMgSet.get(j).setModifyDate(PubFun.getCurrentDate());
            mLHMessSendMgSet.get(j).setModifyTime(PubFun.getCurrentTime());
            mLHMessSendMgSet.get(j).setManageCom(mGlobalInput.ManageCom);

            LHServItemDB tLHServItemDB = new LHServItemDB();
            LHServItemSet tLHServItemSet = new LHServItemSet();
            tLHServItemDB.setServItemNo(mLHMessSendMgSet.get(j).getServItemNo());
            tLHServItemSet = tLHServItemDB.query();
            if (tLHServItemSet.get(1).getServPlanNo().equals("0"))
            {//团体
                LHGrpServPlanDB tLHGrpServPlanDB = new LHGrpServPlanDB();
                tLHGrpServPlanDB.setGrpServPlanNo(tLHServItemSet.get(1).getGrpServPlanNo());
                mLHMessSendMgSet.get(j).setContNo(tLHGrpServPlanDB.query().get(1).getGrpContNo());
                mLHMessSendMgSet.get(j).setGrpContNo(tLHGrpServPlanDB.query().get(1).getGrpContNo());
            }
            else
            {
                LHServPlanDB tLHServPlanDB = new LHServPlanDB();
                tLHServPlanDB.setServPlanNo(tLHServItemSet.get(1).getServPlanNo());
                mLHMessSendMgSet.get(j).setContNo(tLHServPlanDB.query().get(1).getContNo());
            }

            System.out.println("TASKEXECNO   "+mLHMessSendMgSet.get(j).getTaskExecNo());
            map.put(" delete from  LHMessSendMg where TaskExecNo='" +
                    mLHMessSendMgSet.get(j).getTaskExecNo() + "'","DELETE");

            map.put(" update LHTASKCUSTOMERRELA a set  a.TaskExecState='"+mTransferData.getValueByName("ExecState").toString()+"',"
                    +" a.ManageCom='"+mGlobalInput.ManageCom+"', a.Operator='"+mGlobalInput.Operator+"', "
                    +" a.ModifyDate='"+PubFun.getCurrentDate()+"',a.ModifyTime='"+PubFun.getCurrentTime()  +"' "
                    +" where  a.TaskExecNo ='"+mLHMessSendMgSet.get(j).getTaskExecNo()+"'","UPDATE");//将个人服务受理的服务任务换为生成的服务任务编号

        }
        map.put(mLHMessSendMgSet, "INSERT");
    }

    if (this.mOperate.equals("DELETE||MAIN"))
    {
            //map.put(mLHMessSendMget, "DELETE");
    }
    return true;
}
/**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
 */
private boolean updateData()
{
    return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
    this.mLHMessSendMgSet.set((LHMessSendMgSet)cInputData.getObjectByObjectName("LHMessSendMgSet",0));
    this.mLHTaskCustomerRelaSet.set((LHTaskCustomerRelaSet)cInputData.getObjectByObjectName("LHTaskCustomerRelaSet",0));
    this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
//    if (mTransferData != null)
//    {
    mTransferData = ((TransferData) cInputData.getObjectByObjectName("TransferData", 0));
//    }

    return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
     this.mResult.clear();
     LHMessSendMgDB tLHMessSendMgDB=new LHMessSendMgDB();
     //tLHMessSendMgDB.setSchema(this.mLHMessSendMgSchema);
     //如果有需要处理的错误，则返回
     if (tLHMessSendMgDB.mErrors.needDealError())
     {
          // @@错误处理
          this.mErrors.copyAllErrors(tLHMessSendMgDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "LHMessSendMgBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
          this.mErrors.addOneError(tError);
          return false;
     }
      mInputData = null;
      return true;
}
 private boolean prepareOutputData()
 {
   try
   {
                this.mInputData.clear();
                this.mInputData.add(this.mLHMessSendMgSet);
                this.mInputData.add(this.mLHTaskCustomerRelaSet);
                mInputData.add(map);
                mResult.clear();
                mResult.add(this.mLHMessSendMgSet);
                mResult.add(this.mLHTaskCustomerRelaSet);
        }
        catch(Exception ex)
        {
                // @@错误处理
                CError tError =new CError();
                tError.moduleName="LHMessSendMgBL";
                tError.functionName="prepareData";
                tError.errorMessage="在准备往后层处理所需要的数据时出错。";
                this.mErrors .addOneError(tError) ;
                return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void jbInit() throws Exception {
    }
}
