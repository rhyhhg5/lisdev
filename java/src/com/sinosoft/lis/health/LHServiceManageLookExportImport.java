package com.sinosoft.lis.health;

import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.diskimport.*;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.LHTaskCustomerRelaSchema;
import com.sinosoft.utility.*;


public class LHServiceManageLookExportImport {
    public CErrors mErrrors = new CErrors();
    private VData mResult = new VData();
    /** 磁盘导入临时表的schema */
    private static String fileName;
    private static String configFileName;
    private static String ServTaskNo;
    private static String SheetName ;

    private static final String schemaClassName =
            "com.sinosoft.lis.schema.LHTaskCustomerRelaSchema";

    /** 磁盘导入临时表的set */
    private static final String schemaSetClassName =
            "com.sinosoft.lis.vschema.LHTaskCustomerRelaSet";

    /** 使用默认的导入方式 */
    private DefaultDiskImporter importer = null;

    //**数据操作字符串*/
    private static String mOperate = "UPDATE||MAIN";

    //**往后面传输数据的容器*/
    VData mInputData = new VData();
    private MMap map = new MMap();
    /**
     * 构造函数
     * @param fileName String
     * @param configFileName String
     * @param sheetName String
     */
    public LHServiceManageLookExportImport(String fileName, String configFileName,String ServTaskNo,String sheetName)
    {
        this.ServTaskNo = ServTaskNo;
        this.fileName = fileName;
        this.configFileName = configFileName;
        this.SheetName = sheetName;
        importer = new DefaultDiskImporter(fileName, configFileName, sheetName);
    }

    /**
     * 执行导入
     * @return boolean
     */
    public boolean doImport() {
        importer.setSchemaClassName(schemaClassName);
        importer.setSchemaSetClassName(schemaSetClassName);
        if (!importer.doImport()) {
            mErrrors.copyAllErrors(importer.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 得到导入结果
     * @return SchemaSet
     */
    public SchemaSet getSchemaSet() {
        return importer.getSchemaSet();
    }

    /**
     * JSP倒入接口
     * @return boolean
     */
    public boolean submitImport() {
        this.doImport();
        LHTaskCustomerRelaSet mLHTaskCustomerRelaSet = (LHTaskCustomerRelaSet)importer.getSchemaSet();
        LHTaskCustomerRelaSet dbLHTaskCustomerRelaSet = new LHTaskCustomerRelaSet();
        System.out.println("------------mLHTaskCustomerRelaSet.size is  :" +mLHTaskCustomerRelaSet.size() + "-----------");
        SSRS tSSRS = null;
        ExeSQL tExeSQL = new ExeSQL();
        String sql ="select distinct TaskExecState,TaskExecNo ,"
                    +" ServCaseCode,ServTaskNo,ServTaskCode ,ServItemNo,ManageCom,Operator,"
                    +" MakeDate,MakeTime  "
                    +" from LHTaskCustomerRela where ServTaskNo in ("+ServTaskNo +")";

        tSSRS = tExeSQL.execSQL(sql);
        int length = tSSRS.getMaxRow();
        System.out.println("length"+length);
        String arrTaskCustomer[][] = tSSRS.getAllData();

        if (mLHTaskCustomerRelaSet.size() != arrTaskCustomer.length) {
            // @@错误处理
            this.mErrrors.copyAllErrors(this.mErrrors);
            CError tError = new CError();
            tError.moduleName = "LHServiceManageLookExportImport";
            tError.functionName = "submitImport";
            tError.errorMessage = "传入数据与数据库中数据不相等!";
            this.mErrrors.addOneError(tError);
            return false;
        }

        for (int j = 1; j <= mLHTaskCustomerRelaSet.size(); j++)
        {
            String TaskExecState = ""; //服务任务状态
            if(mLHTaskCustomerRelaSet.get(j).getTaskExecState().equals("未执行"))
            {
                TaskExecState="1";
            }
            if(mLHTaskCustomerRelaSet.get(j).getTaskExecState().equals("已执行"))
            {
                TaskExecState="2";
            }
             LHTaskCustomerRelaSchema tLHTaskCustomerRelaSchema = new LHTaskCustomerRelaSchema(); //LHCaseTaskRela（事件任务关联表）
             tLHTaskCustomerRelaSchema.setTaskExecState(TaskExecState); //任务执行状态未执行
             tLHTaskCustomerRelaSchema.setTaskExecNo(arrTaskCustomer[j-1][1]);//任务执行状态未执行);//任务执行状态未执行
             tLHTaskCustomerRelaSchema.setModifyDate(PubFun.getCurrentDate());
             tLHTaskCustomerRelaSchema.setModifyTime(PubFun.getCurrentTime());
             tLHTaskCustomerRelaSchema.setServCaseCode(arrTaskCustomer[j-1][2]);
             tLHTaskCustomerRelaSchema.setServTaskNo(arrTaskCustomer[j-1][3]);
             tLHTaskCustomerRelaSchema.setServTaskCode(arrTaskCustomer[j-1][4]);
             tLHTaskCustomerRelaSchema.setServItemNo(arrTaskCustomer[j-1][5]);
             tLHTaskCustomerRelaSchema.setManageCom(arrTaskCustomer[j- 1][6]);
             tLHTaskCustomerRelaSchema.setOperator(arrTaskCustomer[j-1][7]);
             tLHTaskCustomerRelaSchema.setMakeDate(arrTaskCustomer[j-1][8]);
             tLHTaskCustomerRelaSchema.setMakeTime(arrTaskCustomer[j-1][9]);
             dbLHTaskCustomerRelaSet.add(tLHTaskCustomerRelaSchema);
        }
        mInputData.add(dbLHTaskCustomerRelaSet);
        map.put(dbLHTaskCustomerRelaSet,"DELETE&INSERT");

        //处理通讯
        for (int j = 1; j <= mLHTaskCustomerRelaSet.size(); j++)
        {
            map.put("update lhmesssendmg set hmmesscode = '"+mLHTaskCustomerRelaSet.get(j).getTaskExecDes()
                    +"' where taskexecno = '"+mLHTaskCustomerRelaSet.get(j).getTaskExecNo()+"'","UPDATE");
        }

        mInputData.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "mLHServiceManageLookExportImport";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrrors.addOneError(tError);
            return false;
        }
        System.out.println("----submitImport,success-----");
        return true;
    }
    public VData getResult()
    {
      return this.mResult;
}
    /**
     * 主函数，调试用
     * @param args String[]
     */
    public static void main(String[] args) {
       String fileName = "E:/workspace/ui/temp/LHExcuteState.xls";
       String configFileName = "E:/workspace/ui/temp/LHExcuteState.xml";
       ServTaskNo = "000000002388";
       String sheetName = "ExcuteState";
       LHServiceManageLookExportImport importer = new
                                                LHServiceManageLookExportImport(
               fileName, configFileName,
               ServTaskNo, sheetName);
       importer.doImport();
       LHTaskCustomerRelaSet set = (LHTaskCustomerRelaSet) importer.getSchemaSet();
       System.out.println(set.toString());
    }
}
