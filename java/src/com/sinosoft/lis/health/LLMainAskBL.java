/*
 * <p>ClassName: OLLMainAskBL </p>
 * <p>Description: OLLMainAskBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-17 18:32:42
 */
package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LLMainAskBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String tNo;
    /** 业务处理相关变量 */
    private LLMainAskSchema mLLMainAskSchema = new
                                               LLMainAskSchema();
    private LLConsultSchema mLLConsultSchema = new LLConsultSchema();
    private LLAnswerInfoSchema mLLAnswerInfoSchema = new LLAnswerInfoSchema();
    public LLMainAskBL() {
    }

    public static void main(String[] args) {

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLLMainAskBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLLMainAskBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start OLLMainAskBL Submit...");
            LLMainAskBLS tOLLMainAskBLS = new LLMainAskBLS();
            //tOLLMainAskBLS.submitData(mInputData,mOperate);
            System.out.println("End OLLMainAskBL Submit...");
            //如果有需要处理的错误，则返回
            if (tOLLMainAskBLS.mErrors.needDealError()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tOLLMainAskBLS.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLLMainAskBL";
                tError.functionName = "submitDat";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        PubSubmit tPubSubmit = new PubSubmit();
        tPubSubmit.submitData(mInputData, mOperate);
        //this.mResult=mInputData;
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        if (this.mOperate.equals("INSERT||MAIN")) {

            mLLMainAskSchema.setLogNo(PubFun1.CreateMaxNo("MainAsk", 9));

            mLLMainAskSchema.setOperator(mGlobalInput.Operator);
            mLLMainAskSchema.setMakeDate(PubFun.getCurrentDate());
            mLLMainAskSchema.setMakeTime(PubFun.getCurrentTime());
            mLLMainAskSchema.setModifyDate(PubFun.getCurrentDate());
            mLLMainAskSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(mLLMainAskSchema, "INSERT");

            mLLConsultSchema.setLogNo(
                    mLLMainAskSchema.getLogNo());

            mLLConsultSchema.setConsultNo(PubFun1.CreateMaxNo("Consult", 9));

            mLLConsultSchema.setOperator(mGlobalInput.
                                         Operator);
            mLLConsultSchema.setMakeDate(PubFun.
                                         getCurrentDate());
            mLLConsultSchema.setMakeTime(PubFun.
                                         getCurrentTime());
            mLLConsultSchema.setModifyDate(PubFun.
                                           getCurrentDate());
            mLLConsultSchema.setModifyTime(PubFun.
                                           getCurrentTime());

            map.put(mLLConsultSchema, "INSERT");

            mLLAnswerInfoSchema.setLogNo(
                    mLLMainAskSchema.getLogNo());

            mLLAnswerInfoSchema.setConsultNo(mLLConsultSchema.getConsultNo());

            // mLLAnswerInfochema.setOperator(mGlobalInput.Operator);
            mLLAnswerInfoSchema.setMakeDate(PubFun.
                                           getCurrentDate());
            mLLAnswerInfoSchema.setMakeTime(PubFun.
                                           getCurrentTime());
            mLLAnswerInfoSchema.setModifyDate(PubFun.
                                             getCurrentDate());
            mLLAnswerInfoSchema.setModifyTime(PubFun.
                                             getCurrentTime());

            map.put(mLLAnswerInfoSchema, "INSERT");

        }

        if (this.mOperate.equals("UPDATE||MAIN")) {

            mLLMainAskSchema.setOperator(mGlobalInput.Operator);

            mLLMainAskSchema.setModifyDate(PubFun.getCurrentDate());
            mLLMainAskSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(mLLMainAskSchema, "UPDATE");

            mLLConsultSchema.setOperator(mGlobalInput.
                                         Operator);
            mLLConsultSchema.setModifyDate(PubFun.
                                           getCurrentDate());
            mLLConsultSchema.setModifyTime(PubFun.
                                           getCurrentTime());

            map.put(mLLConsultSchema, "UPDATE");

            mLLAnswerInfoSchema.setLogNo(
                    mLLMainAskSchema.getLogNo());


            mLLAnswerInfoSchema.setModifyDate(PubFun.
                                             getCurrentDate());
            mLLAnswerInfoSchema.setModifyTime(PubFun.
                                             getCurrentTime());

            map.put(mLLAnswerInfoSchema, "UPDATE");

        }

        if (this.mOperate.equals("DELETE||MAIN")) {
            map.put(mLLMainAskSchema, "DELETE");
            map.put(mLLConsultSchema, "DELETE");
            map.put(mLLAnswerInfoSchema, "DELETE");

        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLLMainAskSchema.setSchema((LLMainAskSchema)
                                                 cInputData.
                                                 getObjectByObjectName(
                "LLMainAskSchema", 0));
        this.mLLConsultSchema.setSchema((LLConsultSchema) cInputData.
                                  getObjectByObjectName(
                                          "LLConsultSchema", 0));
        this.mLLAnswerInfoSchema.setSchema((LLAnswerInfoSchema) cInputData.
                                        getObjectByObjectName(
                                                "LLAnswerInfoSchema", 0));

        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
       LLMainAskDB tLLMainAskDB = new LLMainAskDB();
        tLLMainAskDB.setSchema(this.mLLMainAskSchema);
        //如果有需要处理的错误，则返回
        if (tLLMainAskDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLLMainAskDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LLMainAskDBBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();

            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLLMainAskSchema);
            mResult.add(this.mLLConsultSchema);
            mResult.add(this.mLLAnswerInfoSchema);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHCustomTestBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
