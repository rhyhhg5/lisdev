package com.sinosoft.lis.health;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

import java.io.File;
import com.sinosoft.lis.easyscan.ProposalDownloadBL;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 * <p>Title: 通讯管理查询</p>
 * <p>Description: 通讯管理查询清单</p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author xiep   修改于09/02/25 xls下载改为txt下载，解决大数据量问题
 * @version 1.0
 * @date 2008-08-27
 */

public class AddressCaseListBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private FileWriter mFileWriter;
    private BufferedWriter mBufferedWriter;

    String ValiDate = "";
    String MngCom = "";
    String SysPath = ""; //存放生成文件的路径
    String FullPath = "";
    String tFlag = "";
    public AddressCaseListBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cSysPath) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        SysPath = cSysPath;
        // 准备所有要打印的数据
        if (!downloadData()) {
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        TransferData tTransferData = (TransferData) cInputData.
                                     getObjectByObjectName("TransferData", 0);
        ValiDate = (String) tTransferData.getValueByName("ValiDate");
        MngCom = (String) tTransferData.getValueByName("MngCom");
        tFlag = (String) tTransferData.getValueByName("tFlag");
        return true;
    }

    public String getResult() {
        return FullPath;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private boolean downloadData(){
    	try{
    		if(tFlag.equals("address")||tFlag.equals("zixun")||tFlag.equals("jiangzuo")){
    			downloadAddressData();

    		}
    		else if(tFlag.equals("mobile")){
    			downloadMobileData();
    		}
    		else {
    			System.out.println("tFlag标识错误，无法进行打印");
    			return false;
    		}

    	}catch (Exception ex) {
            ex.toString();
            ex.printStackTrace();
        }
    	return true;
    }


    private boolean downloadAddressData() {
            String itemcode = "";//统计查询类型
            String name = "";//压缩包文件用名
           if(tFlag.equals("address")){
                itemcode = "006";
                name = "JKTongXun";
           }else if(tFlag.equals("zixun")){
               itemcode = "012";
               name = "JKZiXun";
           }else if(tFlag.equals("jiangzuo")){
               itemcode = "005";
               name = "JKJiangZuo";
           }
           String Path = "";
           try {
               Path = name+PubFun.getCurrentDate2() + "_" + PubFun.getCurrentTime2();
               mFileWriter = new FileWriter(SysPath + "healthmanage/" +Path+".txt");
               mBufferedWriter = new BufferedWriter(mFileWriter);

               String[] strArr1Head = new String[18];
                strArr1Head[0] = "保单号码";
                strArr1Head[1] = "机构代码";
                strArr1Head[2] = "投保人姓名";
                strArr1Head[3] = "投保人性别";
                strArr1Head[4] = "投保人身份证号码";
                strArr1Head[5] = "投保人联系地址";
                strArr1Head[6] = "投保人邮编";
                strArr1Head[7] = "投保人手机号";
                strArr1Head[8] = "联系电话";
                strArr1Head[9] = "销售渠道";
                strArr1Head[10] = "保费金额";
                strArr1Head[11] = "代理人姓名";
                strArr1Head[12] = "代理人性别";
                strArr1Head[13] = "代理人代码";
                strArr1Head[14] = "代理人手机号";
                strArr1Head[15] = "续保次数";
                strArr1Head[16] = "代理网点";
                strArr1Head[17] = "生效日期";
                //一个是客户所属代理网点，一个是保单生效日期
                String head="";
               for(int m=0;m<strArr1Head.length;m++){
                   head += strArr1Head[m]+"|";
               }
               mBufferedWriter.write(head+"\r\n");
//               mBufferedWriter.newLine();
               mBufferedWriter.flush();

           } catch (IOException ex) {
              ex.printStackTrace();
           }
            String sql = "select a.contno,a.managecom,a.appntname," +
            		"(case when a.appntsex = '1' then '女' else '男' end )," +
            		"a.appntidno," +
            		"(select d.postaladdress from lcaddress d where d.customerno = (select c.appntno from lcappnt c where c.contno = a.contno) and d.addressno = (select c.addressno from lcappnt c where c.contno = a.contno))," +
            		"(select d.zipcode from lcaddress d where d.customerno = (select c.appntno from lcappnt c where c.contno = a.contno) and d.addressno = (select c.addressno from lcappnt c where c.contno = a.contno))," +
            		"(case when length(trim((select d.mobile from lcaddress d where d.customerno = c.appntno and d.addressno = c.addressno)))=11 and (select d.mobile from lcaddress d where d.customerno = c.appntno and d.addressno = c.addressno ) like '1%' then (select d.mobile from lcaddress d where d.customerno = c.appntno and d.addressno = c.addressno) else '' end), " +
                        "(select d.phone  from lcaddress d where d.customerno = c.appntno  and d.addressno = c.addressno),"+
                        "(select codename from ldcode  where codetype = 'lcsalechnl' and code = a.salechnl)," +
            		"a.prem," +
            		"(select b.name from laagent b  where b.agentcode = a.agentcode)," +
            		"(case when (select b.sex from laagent b  where b.agentcode = a.agentcode) = '1' then '女' else '男' end), " +
            		"a.agentcode," +
                            "(select b.mobile from laagent b  where b.agentcode = a.agentcode),(case when length(p.servplaytype)!=1 then substr(p.servplaytype,2,3) else '0' end) " +
                    ", (select Name from LACom where AgentCom = a.AgentCom) 代理网点, a.CValiDate " +
            		"from lccont a,lcappnt c,lhservplan p  where a.conttype='1' and a.contno=c.contno and p.contno=a.contno "+
                        "and p.servplaytype != '1H' " +
                        " and p.comid like '"+MngCom+"%' " +
                        " and  p.StartDate <='"+ValiDate+"' and p.EndDate >='"+ValiDate+"' " +
                        " and exists (select 1 from lhservitem b where servitemcode='"+itemcode+"' and p.servplanno=b.servplanno )"+
                        " with ur";
           int start = 1;
           int nCount = 10000;
           while (true) {
               SSRS tSSRS = new ExeSQL().execSQL(sql, start, nCount);
               if (tSSRS.getMaxRow() <= 0) {
                   break;
               }
               if (tSSRS != null && tSSRS.getMaxRow() > 0) {
                for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                      try{
                        String result="";
                       for(int m=1;m<=tSSRS.getMaxCol();m++){
                             result += tSSRS.GetText(i,m)+"|";
                       }
                        mBufferedWriter.write(result+"\r\n");
//                        mBufferedWriter.newLine();
                        mBufferedWriter.flush();

                    } catch (IOException ex) {
                       ex.printStackTrace();
                    }
                }
            }
            start += nCount;
          }
          try {
           mBufferedWriter.close();
         } catch (IOException ex2) {
         }
          String[] FilePaths = new String[1];
          FilePaths[0] = SysPath + "healthmanage/"+Path+".txt";
          String[] FileNames = new String[1];
          FileNames[0] = Path+".txt";
          String newPath = SysPath + "healthmanage/"+Path+".zip";
          FullPath = Path+".zip";
          CreateZip(FilePaths,FileNames,newPath);
          try{
              File fd = new File(FilePaths[0]);
              fd.delete();
          }catch(Exception ex3){
              ex3.printStackTrace();
          }
        return true;
    }

    private boolean downloadMobileData() {
        String Path="";
        try {
            SysPath = SysPath + "healthmanage/" ;
            Path = "JKDuanXin"+PubFun.getCurrentDate2() + "_" + PubFun.getCurrentTime2();
            mFileWriter = new FileWriter(SysPath+Path+".txt");
            mBufferedWriter = new BufferedWriter(mFileWriter);

            String[] strArr1Head = new String[13];
                strArr1Head[0] = "保单号码";
                strArr1Head[1] = "机构代码";
                strArr1Head[2] = "投保人姓名";
                strArr1Head[3] = "投保人性别";
                strArr1Head[4] = "投保人身份证号码";
                strArr1Head[5] = "投保人手机号";
                strArr1Head[6] = "投保人联系电话";
                strArr1Head[7] = "销售渠道";
                strArr1Head[8] = "保费金额";
                strArr1Head[9] = "代理人姓名";
                strArr1Head[10] = "代理人性别";
                strArr1Head[11] = "代理人代码";
                strArr1Head[12] = "代理人手机号";
                String head = "";
            for(int m=0;m<strArr1Head.length;m++){
                        head += strArr1Head[m]+"|";
            }
            mBufferedWriter.write(head+"\r\n");
//            mBufferedWriter.newLine();
            mBufferedWriter.flush();
        }catch(Exception ex){
            ex.printStackTrace();
        }
         String sql = "select a.contno,a.managecom,a.appntname," +
            		"(case when a.appntsex = '1' then '女' else '男' end )," +
            		"a.appntidno," +
            		"(case when length(trim((select d.mobile from lcaddress d where d.customerno = c.appntno and d.addressno = c.addressno)))=11 and (select d.mobile from lcaddress d where d.customerno = c.appntno and d.addressno = c.addressno ) like '1%' then (select d.mobile from lcaddress d where d.customerno = c.appntno and d.addressno = c.addressno) else '' end), " +
                        "(case when length(trim((select d.phone from lcaddress d where d.customerno = c.appntno and d.addressno = c.addressno)))=11 and (select d.phone from lcaddress d where d.customerno = c.appntno and d.addressno = c.addressno ) like '1%' then (select d.phone from lcaddress d where d.customerno = c.appntno and d.addressno = c.addressno) else '' end), "+
            		"(select codename from ldcode  where codetype = 'lcsalechnl' and code = a.salechnl)," +
            		"a.prem," +
            		"(select b.name from laagent b  where b.agentcode = a.agentcode)," +
            		"(case when (select b.sex from laagent b  where b.agentcode = a.agentcode) = '1' then '女' else '男' end), " +
            		"a.agentcode," +
            		"(select b.mobile from laagent b  where b.agentcode = a.agentcode) " +
            		"from lccont a,lcappnt c,lhservplan p  where a.conttype='1' and a.contno=c.contno and a.contno=p.contno "+
                        "and p.servplaytype != '1H'" +
                        " and p.comid like '"+MngCom+"%' " +
                        " and  p.StartDate <='"+ValiDate+"' and p.EndDate >='"+ValiDate+"' " +
                        " and exists (select 1 from lhservitem b where servitemcode='015' and p.servplanno=b.servplanno )"+
                        " with ur";
            int start = 1;
             int nCount = 10000;
             while (true) {
                 SSRS tSSRS = new ExeSQL().execSQL(sql, start, nCount);
                 if (tSSRS.getMaxRow() <= 0) {
                     break;
                 }
                 if (tSSRS != null && tSSRS.getMaxRow() > 0) {
                  for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                        try{
                          String result="";
                         for(int m=1;m<=tSSRS.getMaxCol();m++){
                               result += tSSRS.GetText(i,m)+"|";
                         }
                          mBufferedWriter.write(result+"\r\n");
//                          mBufferedWriter.newLine();
                          mBufferedWriter.flush();

                      } catch (IOException ex) {
                         ex.printStackTrace();
                      }
                  }
              }
              start += nCount;
            }
            try {
             mBufferedWriter.close();
           } catch (IOException ex2) {
           }
            String[] FilePaths = new String[1];
            FilePaths[0] = SysPath+Path+".txt";
            String[] FileNames = new String[1];
            FileNames[0] = Path+".txt";
            String newPath = SysPath+Path+".zip";
            FullPath = Path+".zip";
            CreateZip(FilePaths,FileNames,newPath);
            try{
                File fd = new File(FilePaths[0]);
                fd.delete();
            }catch(Exception ex3){
                ex3.printStackTrace();
            }
          return true;
    }

    //生成压缩文件
    public boolean CreateZip(String[] tFilePaths, String[] tFileNames,
                             String tZipPath) {
        ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
        if (!tProposalDownloadBL.CreatZipFile(tFilePaths, tFileNames, tZipPath)) {
            System.out.println("生成压缩文件失败");
            CError.buildErr(this, "生成压缩文件失败");
            return false;
        }
        return true;
    }

    //判断取出的ContNo是否为符合通讯事件查询条件的保单号
    public boolean isAddressContNo(String tContNo){
    	String tSQL1 = "select 1 from lcpol where contno = '"+tContNo+"'  with ur";
    	SSRS tSSRS1 = new ExeSQL().execSQL(tSQL1);
    	String tSQL2 = "select 1 from lhservitem where contno = '"+tContNo+"' and servitemcode = '006' with ur";
    	SSRS tSSRS2 = new ExeSQL().execSQL(tSQL2);
    	if((tSSRS1.getMaxRow() != 0)&&(tSSRS2.getMaxRow() != 0))
    	{
    		return true;
    	}
    	else
    	{
            return false;
    	}
    }

//  判断取出的ContNo是否为符合短信服务查询条件的保单号
    public boolean isMobileContNo(String tContNo){
    	String tSQL1 = "select 1 from lcpol where contno = '"+tContNo+"'  with ur";
    	SSRS tSSRS1 = new ExeSQL().execSQL(tSQL1);
    	String tSQL2 = "select 1 from lhservitem where contno = '"+tContNo+"' and servitemcode = '015' with ur";
    	SSRS tSSRS2 = new ExeSQL().execSQL(tSQL2);
    	if((tSSRS1.getMaxRow() != 0)&&(tSSRS2.getMaxRow() != 0))
    	{
    		return true;
    	}
    	else
    	{
            return false;
    	}
    }

    public static void main(String[] args) {
    }
}
