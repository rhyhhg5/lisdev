/*
 * <p>ClassName: OLHGrpPerServExeTraceBL </p>
 * <p>Description: OLHGrpPerServExeTraceBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-03-09 10:43:01
 */
package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLHGrpPerServExeTraceBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LHServExeTraceSchema mLHServExeTraceSchema = new
            LHServExeTraceSchema();
    private LHServExeTraceSet mLHServExeTraceSet = new LHServExeTraceSet();
    public OLHGrpPerServExeTraceBL() {
    }

    public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLHGrpPerServExeTraceBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLHServPlanBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start OLHGrpPerServExeTraceBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();

            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                System.out.println("----------PubSubmit error------------");
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                CError tError = new CError();
                tError.moduleName = "OLHGrpPerServExeTraceBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }

        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("--------DealData---------");
        if (this.mOperate.equals("INSERT||MAIN")) {
            System.out.println("***********" + mLHServExeTraceSet.size());
            if (mLHServExeTraceSet != null && mLHServExeTraceSet.size() > 0) {

                for (int i = 1; i <= mLHServExeTraceSet.size(); i++) {
                    mLHServExeTraceSet.get(i).setOperator(mGlobalInput.Operator);
                    mLHServExeTraceSet.get(i).setManageCom(mGlobalInput.
                            ManageCom);
                    mLHServExeTraceSet.get(i).setMakeDate(PubFun.getCurrentDate());
                    mLHServExeTraceSet.get(i).setMakeTime(PubFun.getCurrentTime());
                    mLHServExeTraceSet.get(i).setModifyDate(PubFun.
                            getCurrentDate());
                    mLHServExeTraceSet.get(i).setModifyTime(PubFun.
                            getCurrentTime());
                }
                map.put(mLHServExeTraceSet, "INSERT");
            }
        }
        if (this.mOperate.equals("UPDATE||MAIN")) {
            for (int i = 1; i <= mLHServExeTraceSet.size(); i++) {

                mLHServExeTraceSet.get(i).setOperator(mGlobalInput.Operator);
                mLHServExeTraceSet.get(i).setManageCom(mGlobalInput.ManageCom);
                if (mLHServExeTraceSet.get(i).getMakeDate() == null ||
                    mLHServExeTraceSet.get(i).getMakeDate().equals("")) {
                    mLHServExeTraceSet.get(i).setServItemNo("G"+PubFun1.CreateMaxNo(
                            "LHGRPSERVITEM",19));
                    mLHServExeTraceSet.get(i).setMakeDate(PubFun.getCurrentDate());
                    mLHServExeTraceSet.get(i).setMakeTime(PubFun.getCurrentTime());
                }
                mLHServExeTraceSet.get(i).setModifyDate(PubFun.getCurrentDate());
                mLHServExeTraceSet.get(i).setModifyTime(PubFun.getCurrentTime());

                map.put("delete from LHServExeTrace where ServItemNo = '" +
                    mLHServExeTraceSet.get(i).getServItemNo() + "'", "DELETE");
            }
            //   System.out.println("------------"+mLHServExeTraceSet.get(1).getServPlanNo()+"111111111----------------");

            map.put(mLHServExeTraceSet, "INSERT");
        }
        if (this.mOperate.equals("DELETE||MAIN")) {
            if (mLHServExeTraceSet.size() > 0) {
                CError lError = new CError();
                lError.errorMessage = "mLHServItemSet.size()";
                map.put(mLHServExeTraceSet, "DELETE");
            }
        }

        return true;
    }


    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {

        this.mLHServExeTraceSet.set((LHServExeTraceSet) cInputData.
                                    getObjectByObjectName(
                                            "LHServExeTraceSet", 0));
        /*    this.mLHServExeTraceSchema.setSchema((LHServExeTraceSchema)
                                                    cInputData.
                                                    getObjectByObjectName(
                         "mLHServExeTraceSchema", 0));
         */
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LHServExeTraceDB tLHServExeTraceDB = new LHServExeTraceDB();
        tLHServExeTraceDB.setSchema(this.mLHServExeTraceSchema);
        //如果有需要处理的错误，则返回
        if (tLHServExeTraceDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHServExeTraceDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHServPlanBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLHServExeTraceSchema);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLHServExeTraceSchema);
            mResult.add(this.mLHServExeTraceSet);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLHGrpPerServExeTraceBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
