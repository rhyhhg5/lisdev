package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLHPersonCaseBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private LHServCaseRelaSet mLHServCaseRelaSet = new LHServCaseRelaSet();
    private LHServCaseDefSet mLHServCaseDefSet = new LHServCaseDefSet();
    private TransferData mTransferData = new TransferData();
    String ServItemNo;

    /** 数据操作字符串 */
    private String mOperate;
    String GrpServPlanCode;

    public OLHPersonCaseBL() {}

    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        this.mInputData = cInputData;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLHPersonCaseBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLHPersonCaseBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData())
            return false;

        System.out.println("Start OLHPersonCaseBL Submit...");
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "OLHPersonCaseBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("END OLHPersonCaseBL Submit...");
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            LHServItemSet tLHServItemSet = new LHServItemSet();
            LHServItemDB tLHServItem = new LHServItemDB();
            String Sql = "select * from LHServItem where GrpServItemNo ='" +
                         ServItemNo + "'";
            tLHServItemSet = tLHServItem.executeQuery(Sql);

            LHGrpServPlanSet mLHGrpServPlanSet = new LHGrpServPlanSet();
            LHGrpServPlanDB tLHGrpServPlan = new LHGrpServPlanDB();
            String Sql_1 = "select * from LHGrpServPlan where GrpServPlanNo ='" +
                           tLHServItemSet.get(1).getGrpServPlanNo() + "'";
            mLHGrpServPlanSet = tLHGrpServPlan.executeQuery(Sql_1);

            LHHealthServItemDB tLHHealthServItem = new LHHealthServItemDB();
            LHHealthServItemSet mLHHealthServItemSet = new LHHealthServItemSet();
            String Sql_2 =
                    "select * from LHHealthServItem where ServItemCode ='" +
                    tLHServItemSet.get(1).getServItemCode() + "'";
            mLHHealthServItemSet = tLHHealthServItem.executeQuery(Sql_2);

            for (int i = 1; i <= tLHServItemSet.size(); i++)
            {
                String MStartDate = PubFun.getCurrentDate().replaceAll("-", "");
                String HiddenCaseCode = MStartDate + "1";
                String ServCaseCode = "";
                SSRS tSSRS = new SSRS();
                ExeSQL tExeSQL = new ExeSQL();
                String sql =
                        "select max(substr(ServCaseCode, 10,5)) from LHServCaseDef where substr(ServCaseCode,1,8) = '" +
                        MStartDate + "'";

                tSSRS = tExeSQL.execSQL(sql);
                String rr[][] = tSSRS.getAllData();
                String result = rr[0][0].toString();

                if (result.equals("") || result == null || result.equals("null"))
                {
                    ServCaseCode = HiddenCaseCode + ServCaseCode + "0" +
                                   "000" + i;
                }
                else
                {
                    Integer ii = new Integer(result); //结果转为整型
                    int iii = ii.intValue();
                    int oo = iii++; //在数据库最大值加上‘1’
                    Integer result2 = new Integer(oo + i); //判断队列里有多少个
                    String result3 = result2.toString();
                    String result4 = result3;
                    if (result3.length() == 4)
                    {
                        result4 = "0" + result3.toString();
                    } else if (result3.length() == 3)
                    {
                        result4 = "00" + result3.toString();
                    } else if (result3.length() == 2)
                    {
                        result4 = "000" + result3.toString();
                    } else if (result3.length() == 1)
                    {
                        result4 = "0000" + result3.toString();
                    } else if (result3.length() == 0)
                    {
                        result4 = result3.toString();
                    }
                    ServCaseCode = HiddenCaseCode + ServCaseCode + result4;
                }

                LHServCaseRelaSchema mLHServCaseRelaSchema = new
                        LHServCaseRelaSchema();
                mLHServCaseRelaSchema.setServPlanNo(mLHGrpServPlanSet.get(1).
                        getServPlanCode());
                mLHServCaseRelaSchema.setServPlanName(mLHGrpServPlanSet.get(1).
                        getServPlanName());
                mLHServCaseRelaSchema.setGrpContNo(mLHGrpServPlanSet.get(1).
                        getGrpContNo());
                mLHServCaseRelaSchema.setGrpCustomerNo(mLHGrpServPlanSet.get(1).
                        getGrpCustomerNo());
                // mLHServCaseRelaSchema.setGrpName(mLHGrpServPlanSet.get(1).getGrpName());
                mLHServCaseRelaSchema.setContType("2");
                mLHServCaseRelaSchema.setServPlanLevel(mLHGrpServPlanSet.get(1).
                        getServPlanLevel());
                mLHServCaseRelaSchema.setServCaseCode(ServCaseCode);
                mLHServCaseRelaSchema.setComID(tLHServItemSet.get(i).getComID());
                mLHServCaseRelaSchema.setServItemNo(tLHServItemSet.get(i).
                        getServItemNo());
                mLHServCaseRelaSchema.setServItemCode(tLHServItemSet.get(i).
                        getServItemCode());
                mLHServCaseRelaSchema.setContNo(tLHServItemSet.get(i).getContNo());
                mLHServCaseRelaSchema.setCustomerName(tLHServItemSet.get(i).
                        getName());
                mLHServCaseRelaSchema.setCustomerNo(tLHServItemSet.get(i).
                        getCustomerNo());
                mLHServCaseRelaSchema.setServItemState("1");
                mLHServCaseRelaSchema.setMakeDate(PubFun.getCurrentDate());
                mLHServCaseRelaSchema.setMakeTime(PubFun.getCurrentTime());
                mLHServCaseRelaSchema.setModifyDate(PubFun.getCurrentDate());
                mLHServCaseRelaSchema.setModifyTime(PubFun.getCurrentTime());
                mLHServCaseRelaSchema.setManageCom(mGlobalInput.ManageCom);
                mLHServCaseRelaSchema.setOperator(mGlobalInput.Operator);
                mLHServCaseRelaSet.add(mLHServCaseRelaSchema);
                map.put(mLHServCaseRelaSet, "INSERT");

                LHServCaseDefSchema mLHServCaseDefSchema = new
                        LHServCaseDefSchema();
                mLHServCaseDefSchema.setServCaseType("1");
                mLHServCaseDefSchema.setServCaseCode(ServCaseCode);
                mLHServCaseDefSchema.setServCaseName(mLHHealthServItemSet.get(1).
                        getServItemName() + "-" + tLHServItemSet.get(i).getName());
                mLHServCaseDefSchema.setServCaseState("0");
                mLHServCaseDefSchema.setCaseLaunchState("1");
                mLHServCaseDefSchema.setMakeDate(PubFun.getCurrentDate());
                mLHServCaseDefSchema.setMakeTime(PubFun.getCurrentTime());
                mLHServCaseDefSchema.setModifyDate(PubFun.getCurrentDate());
                mLHServCaseDefSchema.setModifyTime(PubFun.getCurrentTime());
                mLHServCaseDefSchema.setManageCom(mGlobalInput.ManageCom);
                mLHServCaseDefSchema.setOperator(mGlobalInput.Operator);

                mLHServCaseDefSet.add(mLHServCaseDefSchema);
                map.put(mLHServCaseDefSet, "INSERT");
            }
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData()
    {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        mTransferData = ((TransferData) cInputData.getObjectByObjectName("TransferData", 0));
        ServItemNo = (String) mTransferData.getValueByName("tGrpServItemNo");
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }


    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            mInputData.add(map);
            //   mResult.clear();
            //   mResult.add(this.mLHServItemSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHPersonCaseBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

}
