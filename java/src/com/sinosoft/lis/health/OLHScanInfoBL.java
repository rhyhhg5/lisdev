/*
 * <p>ClassName: OLHScanInfoBL </p>
 * <p>Description: OLHScanInfoBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-19 13:58:55
 */
package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class OLHScanInfoBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
private MMap map= new MMap();
/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
/** 业务处理相关变量 */
private SSRS tSSRS = new SSRS();
private TransferData mSendFactor = new TransferData();

private LHScanInfoSet mLHScanInfoSet=new LHScanInfoSet();
public OLHScanInfoBL() {
}
public static void main(String[] args) {
    GlobalInput tG = new GlobalInput();
    TransferData tSendFactor = new TransferData();
    OLHScanInfoUI tOLHScanInfoUI   = new OLHScanInfoUI();
    tG.ManageCom="86";
    tG.Operator="hm";
    String tOperate = "INSERT||MAIN";
    tSendFactor.setNameAndValue("GrpFlag","0");
    tSendFactor.setNameAndValue("ScanTypePerson","10");
    tSendFactor.setNameAndValue("CustomerNo","000000003");

    VData tVData = new VData();
    tVData.add(tSendFactor);
    tVData.add(tG);
    tOLHScanInfoUI.submitData(tVData,tOperate);


}
/**
* 传输数据的公共方法
	* @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "OLHScanInfoBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败OLHScanInfoBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
        System.out.println("Start OLHScanInfoBL Submit...");
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHScanInfoBL";
            tError.functionName = "OLHScanInfoBL";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("End OLHScanInfoBL Submit...");

    }
    mInputData = null;
    return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
  if (this.mOperate.equals("INSERT||MAIN"))
  {
      String GrpFlag = (String)mSendFactor.getValueByName("GrpFlag");

      if(GrpFlag.equals("1"))
      {//团体
          String SerialNo = "";
          String GrpContNo = (String) mSendFactor.getValueByName("GrpContNo");
          String ScanType = (String) mSendFactor.getValueByName("ScanType");
          String sql = "select insuredno from lccont where grpcontno = '" + GrpContNo +
                       "'";
          ExeSQL tExeSQL = new ExeSQL();
          tSSRS = tExeSQL.execSQL(sql);
          String[][] GrpCustom = tSSRS.getAllData();
          int len = tSSRS.getMaxRow();
          for (int i = 0; i < len; i++) {
              String sql2 =
                      "select MAX(Serialno) from lhscaninfo where substr(serialno,6) = '" +
                      GrpCustom[i][0] + "' and substr(serialno,1,2) = '" + ScanType +
                      "'";
              SSRS temp = new SSRS();
              ExeSQL xExeSQL = new ExeSQL();
              temp = xExeSQL.execSQL(sql2);
              if (temp.getAllData()[0][0].equals("")) {
                  SerialNo = ScanType + "001" + GrpCustom[i][0];
              } else {
                  SerialNo = temp.getAllData()[0][0].substring(2, 5);
                  int tCodeInt = Integer.parseInt(SerialNo);
                  tCodeInt++;
                  SerialNo = ScanType + "00" + Integer.toString(tCodeInt) +
                             GrpCustom[i][0];
              }
      //          System.out.println("---------SerialNo:"+SerialNo+"----------");
              LHScanInfoSchema mLHScanInfoSchema = new LHScanInfoSchema();
              mLHScanInfoSchema.setSerialNo(SerialNo);
              mLHScanInfoSchema.setOtherNo(GrpCustom[i][0]);
              mLHScanInfoSchema.setOtherNoType(ScanType);
              mLHScanInfoSchema.setComment(GrpContNo);
              mLHScanInfoSchema.setContent(SerialNo.substring(2, 5));
              mLHScanInfoSchema.setScanState("0");//0-已生成号码,未扫描; 1-已扫描,未录入; 2-已录入
              mLHScanInfoSchema.setManageCom(mGlobalInput.ManageCom);
              mLHScanInfoSchema.setMakeDate(PubFun.getCurrentDate());
              mLHScanInfoSchema.setMakeTime(PubFun.getCurrentTime());
              mLHScanInfoSchema.setModifyDate(PubFun.getCurrentDate());
              mLHScanInfoSchema.setModifyTime(PubFun.getCurrentTime());
              mLHScanInfoSchema.setOperator(mGlobalInput.Operator);
              mLHScanInfoSet.add(mLHScanInfoSchema);
          }
      }


      if(GrpFlag.equals("0"))
      {//个人
          String SerialNo = "";
          String CustomerNo = (String) mSendFactor.getValueByName("CustomerNo");
          String ScanType   = (String) mSendFactor.getValueByName("ScanTypePerson");

          String sql2 =
                      "select MAX(Serialno) from lhscaninfo where substr(serialno,6) = '" +
                      CustomerNo + "' and substr(serialno,1,2) = '" + ScanType +
                      "'";
              SSRS temp = new SSRS();
              ExeSQL xExeSQL = new ExeSQL();
              temp = xExeSQL.execSQL(sql2);
              if (temp.getAllData()[0][0].equals(""))
              {
                  SerialNo = ScanType + "001" + CustomerNo;
              }
              else
              {
                  SerialNo = temp.getAllData()[0][0].substring(2, 5);
                  int tCodeInt = Integer.parseInt(SerialNo);
                  tCodeInt++;

                  if((tCodeInt+"").length() == 1)
                  {
                      SerialNo = ScanType + "00" + Integer.toString(tCodeInt) +
                         CustomerNo;
                  }
                  if((tCodeInt+"").length() == 2)
                  {
                      SerialNo = ScanType + "0" + Integer.toString(tCodeInt) +
                         CustomerNo;
                  }
                  if((tCodeInt+"").length() == 3)
                  {
                      SerialNo = ScanType + Integer.toString(tCodeInt) +
                         CustomerNo;
                  }
              }
              //          System.out.println("---------SerialNo:"+SerialNo+"----------");
              LHScanInfoSchema mLHScanInfoSchema = new LHScanInfoSchema();
              mLHScanInfoSchema.setSerialNo(SerialNo);
              mLHScanInfoSchema.setOtherNo(CustomerNo);
              mLHScanInfoSchema.setOtherNoType(ScanType);
//              mLHScanInfoSchema.setComment(GrpContNo);
              mLHScanInfoSchema.setScanState("0");//0-已生成号码,未扫描; 1-已扫描,未录入; 2-已录入
              mLHScanInfoSchema.setContent(SerialNo.substring(2, 5));
              mLHScanInfoSchema.setManageCom(mGlobalInput.ManageCom);
              mLHScanInfoSchema.setMakeDate(PubFun.getCurrentDate());
              mLHScanInfoSchema.setMakeTime(PubFun.getCurrentTime());
              mLHScanInfoSchema.setModifyDate(PubFun.getCurrentDate());
              mLHScanInfoSchema.setModifyTime(PubFun.getCurrentTime());
              mLHScanInfoSchema.setOperator(mGlobalInput.Operator);
              mLHScanInfoSet.add(mLHScanInfoSchema);
      }

      map.put(mLHScanInfoSet, "INSERT");
  }
  if (this.mOperate.equals("UPDATE||MAIN"))
  {
//    map.put(mLHScanInfoSchema, "UPDATE");
  }
  if (this.mOperate.equals("DELETE||MAIN"))
  {
//    map.put(mLHScanInfoSchema, "DELETE");
  }


  return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
    mSendFactor = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
    this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    return true;
}
/**
 * 准备往后层输出所需要的数据
 * 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
    this.mResult.clear();
    LHScanInfoDB tLHScanInfoDB=new LHScanInfoDB();
//    tLHScanInfoDB.setSchema(this.mLHScanInfoSchema);
    //如果有需要处理的错误，则返回
    if (tLHScanInfoDB.mErrors.needDealError())
    {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHScanInfoDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHScanInfoBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
    }
    mInputData=null;
    return true;
}
 private boolean prepareOutputData()
 {
   try
   {
       this.mInputData.clear();
       this.mInputData.add(this.mLHScanInfoSet);
       mInputData.add(map);
       mResult.clear();
       mResult.add(this.mLHScanInfoSet);
   }
   catch(Exception ex)
   {
       // @@错误处理
       CError tError = new CError();
       tError.moduleName = "LHScanInfoBL";
       tError.functionName = "prepareData";
       tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
       this.mErrors.addOneError(tError);
       return false;
   }
   return true;
   }
           public VData getResult() {
       return this.mResult;
   }
}
