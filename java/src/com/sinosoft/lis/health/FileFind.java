package com.sinosoft.lis.health;

import java.io.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.db.LHQueryInfoDB;
import com.sinosoft.lis.vschema.LHQueryInfoSet;
import com.sinosoft.lis.schema.LHQueryInfoSchema;

public class FileFind
{
    /** 系统路径 */
    private String sysPath = null;

    /** 应用路径 */
    private String  uiPath = null;

    /** 完整路径 */
    private String fullPath = null;

    /** 文件名 */
    private String fileName = null;

    /** 传入的关键字 */
    private String itemname = null;
//    private String keyWord = null;
    private String[] keyWordArray = null;

    /** 查询结果数组 */
    private String[] fileList = new String[3000];
    private static int num = 0;
    String result = "";
    private int startIndex = 0;     //当缓存不够重新查询时的起始位置
    private String mEncodedResult = "";   //数据量过大不返回Jsp,直接去查询,结果赋给此变量

    public FileFind()
    {

    }


    public FileFind(int x)
    {
        this.sysPath = "E:\\workspace\\ui\\";
        this.uiPath = "healthmanage\\QueryFile\\Disease";
        this.fullPath = sysPath + uiPath;
        this.keyWordArray = "发热,咽痛,咳嗽".split(",");
    }
    public FileFind(String sysPath, String uiPath, String keyWord, String itemname, String startIndex)
    {
        this.sysPath = sysPath.replaceAll("/","\\\\");
        this.uiPath = uiPath.replaceAll("/","\\\\");
        this.itemname = itemname;
        keyWord = StrTool.unicodeToGBK(keyWord);
        this.keyWordArray = keyWord.split(",");
        this.fullPath = this.sysPath + this.uiPath ;
        this.startIndex = Integer.parseInt(startIndex)+1;
    }

    public static void main(String[] arg)
    {

      FileFind f = new FileFind();
      f.addClickCount("10000001");
          //调试
//        FileFind ff = new FileFind();
//        File f = new File(ff.fullPath);
//        String[] allFileList = f.list();
//        for (int FileNum = 0; FileNum < allFileList.length; FileNum++)
//        {   //从主文件夹下取得为文件夹的文件对象
//            File eachFile = new File(f +"\\"+ allFileList[FileNum]);
//            System.out.print("--------"+eachFile.toString()+"----------");
////            String s = ff.getFileList(eachFile.toString());getFileListFromJsp
//            String s = ff.getFileListFromJsp();
//            if(!s.equals(""))
//            {
//                ff.fileList[num++] = s;
//            }
//        }
//        for(int i = 0; i <= num-1; i++)
//        {
//            System.out.println(ff.fileList[i]);
//        }


          //Jsp调用
//          FileFind ff = new FileFind("E:/workspace/ui/", "healthmanage/QueryFile/", "a","", "200");
//          String s = ff.getFileListFromJsp();
//          if(s.length() > 1900)
//          {
//              ExeSQL tExeSQL = new ExeSQL();
//              s = "'"+s+"'";
//              s = s.replaceAll(",","','");
//
//              String mSQL = " select itemcode, itemname, itemkeyword from lhqueryinfo where itemcode in ("+s+")";
//              ff.mEncodedResult = tExeSQL.getEncodedResultLarge(mSQL, ff.startIndex);
//
//              System.out.println(ff.mEncodedResult);
//          }
//          else
//          {
////              System.out.println(s);
//          }
    }

/**
 * Jsp的入口
 * @return String
 */

    public String getFileListFromJsp()
    {
        File f = new File(this.fullPath);
        String[] allFileList = f.list();
        int  LoopLen = allFileList.length;

        for (int FileNum = 0; FileNum < LoopLen; FileNum++)
        {   //从主文件夹下取得为文件夹的文件对象
            File eachFile = new File(f + "\\" + allFileList[FileNum]);
            String s = this.getFileList(eachFile.toString());
            if (!s.equals(""))
            {
                result += s.substring(0, 8) + ",";
            }
        }
        if(result.equals("") || result == null)
        {
            return "";
        }
        else
        {
            result = result.substring(0, result.length() - 1);
//            return result;
            ExeSQL tExeSQL = new ExeSQL();
            result = "'"+result+"'";
            result = result.replaceAll(",","','");

              String mSQL = " select itemcode, itemname, itemkeyword from lhqueryinfo where itemname like '%"+this.itemname+"%' and itemcode in ("+result+")";
              String backStringResult = tExeSQL.getEncodedResult(mSQL, this.startIndex);
              System.out.println(backStringResult);
            return backStringResult;
        }
    }

/**
 * 得到包含关键字的文件
 * @return String
 */

    public String getFileList(String fileFullName)
    {
        boolean b = false;
        String fileName = "";
        try
        {

            File f = new File(fileFullName); //读文件实例
            fileName = f.getName();
            FileReader in = new FileReader(f);
            FileInputStream fis = new FileInputStream(f);

            char[] buffer = new char[fis.available()]; // Read 一个文件大小的字节 at a time
            int len;
            if ((len = in.read(buffer)) != -1)
            {

                String s = new String(buffer, 0, len); // Convert to a string
                for(int i = 0; i < this.keyWordArray.length; i++)
                {
                    if(s.indexOf(this.keyWordArray[i]) != -1)
                    {
                        b = true;
                        continue;
                    }
                    else
                    {
                        b = false;
                        break;
                    }
                }
            }

        }
        catch(Exception e)
        {e.printStackTrace();}
        if( b == true)
        {
            return fileName;
        }
        else
        {
            return "";
        }
    }

    public void addClickCount(String itemname)
    {

        SSRS tSSRS = null;
        ExeSQL tExeSQL = new ExeSQL();
        String sql= "select Clickcount from lhqueryinfo where Itemcode = '"+itemname+"' ";
        tSSRS = tExeSQL.execSQL(sql);
        String arr[][] = tSSRS.getAllData();
        int x  = 0;
        if(arr[0][0].equals(""))
        {
            System.out.println("arr[0][0] value is null");
        }
        else
        {
            Integer i = new Integer(arr[0][0]);
            x = i.intValue();
        }

        SSRS tSSRS2 = null;
        ExeSQL tExeSQL2= new ExeSQL();
        String sql2= "update lhqueryinfo set Clickcount='"+(++x)+"' where Itemcode = '"+itemname+"'";
        boolean b = tExeSQL.execUpdateSQL(sql2);
    }
    public void findPath()
    {

    }

}
