/*
 * <p>ClassName: OLHMedicalServiceRecordBL </p>
 * <p>Description: OLHMedicalServiceRecordBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-04-22 09:47:51
 */
//package com.sinosoft.lis.onetable;
package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class OLHMedicalServiceRecordBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
private MMap map = new MMap();

/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;

/** 数据操作字符串 */
private String mOperate;

/** 业务处理相关变量 */
private LHMedicalServiceRecordSchema mLHMedicalServiceRecordSchema=new LHMedicalServiceRecordSchema();
private LHMedicalServiceRecordSet mLHMedicalServiceRecordSet=new LHMedicalServiceRecordSet();

public OLHMedicalServiceRecordBL() {
}
public static void main(String[] args) {
//    GlobalInput tG = new GlobalInput();
//       tG.ManageCom="8600";
//       tG.Operator="001";
//       tG.ComCode="8600";
//
//       LHMedicalServiceRecordSchema tLHMedicalServiceRecordSchema = new LHMedicalServiceRecordSchema();
//       OLHMedicalServiceRecordUI tOLHMedicalServiceRecordUI = new OLHMedicalServiceRecordUI();
//      System.out.println("***********begin init**************");
//      //输出参数
//
//
//      //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
//     String transact = "INSERT||MAIN";
//     tLHMedicalServiceRecordSchema.setRecordNo("111");
//
//    tLHMedicalServiceRecordSchema.setCustomerNo("CustomerNo");
//    tLHMedicalServiceRecordSchema.setFirstRecoDate("2005-8-8");
//    tLHMedicalServiceRecordSchema.setFirstRecoTime("10:21");
//    tLHMedicalServiceRecordSchema.setServiceState("1");
//    tLHMedicalServiceRecordSchema.setApplyDutyItemCode("1111111");
//    tLHMedicalServiceRecordSchema.setDescriptionDetail("DescriptionDetail");
//    tLHMedicalServiceRecordSchema.setServiceDate("2005-8-8");
//    tLHMedicalServiceRecordSchema.setServiceTime("10:21");
//    tLHMedicalServiceRecordSchema.setHospitCode("1111111");
//    tLHMedicalServiceRecordSchema.setDoctNo("DoctNo");
//    tLHMedicalServiceRecordSchema.setApplyReceiveType("1");
//    tLHMedicalServiceRecordSchema.setServiceFlag("1");
//    tLHMedicalServiceRecordSchema.setIsBespeakOk("1");
//    tLHMedicalServiceRecordSchema.setBespeakServiceItem("BespeakServiceItem");
//    tLHMedicalServiceRecordSchema.setApplyServiceDes("ApplyServiceDes");
//    tLHMedicalServiceRecordSchema.setBespeakServiceDate("2005-8-8");
//    tLHMedicalServiceRecordSchema.setBespeakServiceTime("10:21");
//    tLHMedicalServiceRecordSchema.setBespeakServiceHospit("1111111");
//    tLHMedicalServiceRecordSchema.setBespeakServiceDoctNo("1111111");
//    tLHMedicalServiceRecordSchema.setNoCompareCause("NoCompareCause");
//    tLHMedicalServiceRecordSchema.setBespeaLostCause("BespeaLostCause");
//    tLHMedicalServiceRecordSchema.setServiceExecState("21");
//    tLHMedicalServiceRecordSchema.setExcuteServiceItem("ExcuteServiceItem");
//    tLHMedicalServiceRecordSchema.setExcuteDescription("ExcuteDescription");
//    tLHMedicalServiceRecordSchema.setExcuteServiceDate("2005-8-8");
//    tLHMedicalServiceRecordSchema.setExcuteServiceTime("10:21");
//    tLHMedicalServiceRecordSchema.setExcuteServiceHospit("1111113");System.out.println("ExcuteServiceHospit");
//    tLHMedicalServiceRecordSchema.setExcuteServiceDocNO("1111112");
//    tLHMedicalServiceRecordSchema.setExNoCompareCause("ExNoCompareCause");
//    tLHMedicalServiceRecordSchema.setExcutePay("100");
//    tLHMedicalServiceRecordSchema.setSatisfactionDegree("21");
//    tLHMedicalServiceRecordSchema.setOtherServiceInfo("OtherServiceInfo");
//    tLHMedicalServiceRecordSchema.setCancleServiceCause("CancleServiceCause");
//    tLHMedicalServiceRecordSchema.setOperator("Operator");
//    tLHMedicalServiceRecordSchema.setMakeDate("2005-8-8");
//    tLHMedicalServiceRecordSchema.setMakeTime("10:21");
//    tLHMedicalServiceRecordSchema.setModifyDate("2005-8-8");
//    tLHMedicalServiceRecordSchema.setModifyTime("10:21");
//    tLHMedicalServiceRecordSchema.setIsBookSame("1");
//    tLHMedicalServiceRecordSchema.setIsExecSame("1");
//
//      System.out.println("***********end get data **************");
//
//          // 准备传输数据 VData
//          VData tVData = new VData();
//          tVData.add(tLHMedicalServiceRecordSchema);
//          tVData.add(tG);
//
//          tOLHMedicalServiceRecordUI.submitData(tVData, transact);
//          System.out.println("***********after submit data**************");
}
/**
* 传输数据的公共方法
* @param: cInputData 输入的数据
*         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    this.mInputData = cInputData;
    //#############
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "OLHMedicalServiceRecordBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败OLHMedicalServiceRecordBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
        PubSubmit tPubSubmit = new PubSubmit();
 //       tPubSubmit.submitData(mInputData, mOperate);
        if (!tPubSubmit.submitData(mInputData, mOperate))
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);

        CError tError = new CError();
        tError.moduleName = "OLHCustomDiseasBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据提交失败!";

        this.mErrors.addOneError(tError);
        return false;
      }

//      System.out.println("Start OLHMedicalServiceRecordBL Submit...");
//      OLHMedicalServiceRecordBLS tOLHMedicalServiceRecordBLS=new OLHMedicalServiceRecordBLS();
//      tOLHMedicalServiceRecordBLS.submitData(mInputData,mOperate);
//      System.out.println("End OLHMedicalServiceRecordBL Submit...");
//      //如果有需要处理的错误，则返回
//      if (tOLHMedicalServiceRecordBLS.mErrors.needDealError())
//      {
//        // @@错误处理
//        this.mErrors.copyAllErrors(tOLHMedicalServiceRecordBLS.mErrors);
//        CError tError = new CError();
//        tError.moduleName = "OLHMedicalServiceRecordBL";
//        tError.functionName = "submitDat";
//        tError.errorMessage ="数据提交失败!";
//        this.mErrors .addOneError(tError) ;
//        return false;
//      }
    }
    mInputData=null;
    return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{

    if (this.mOperate.equals("INSERT||MAIN"))
    {
            mLHMedicalServiceRecordSchema.setRecordNo(PubFun1.CreateMaxNo("medicals", 9));
            mLHMedicalServiceRecordSchema.setOperator(mGlobalInput.Operator);
            mLHMedicalServiceRecordSchema.setMakeDate(PubFun.getCurrentDate());
            mLHMedicalServiceRecordSchema.setMakeTime(PubFun.getCurrentTime());
            mLHMedicalServiceRecordSchema.setModifyDate(PubFun.getCurrentDate());
            mLHMedicalServiceRecordSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(mLHMedicalServiceRecordSchema, "INSERT");
    }

        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            System.out.println(mLHMedicalServiceRecordSchema.getMakeDate());
            System.out.println(mLHMedicalServiceRecordSchema.getMakeTime());
            System.out.println(mLHMedicalServiceRecordSchema.getRecordNo());

            mLHMedicalServiceRecordSchema.setOperator(mGlobalInput.Operator);
            mLHMedicalServiceRecordSchema.setModifyDate(PubFun.getCurrentDate());
            mLHMedicalServiceRecordSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(mLHMedicalServiceRecordSchema, "UPDATE");
            System.out.println("**************************");
            System.out.println("UPDATE In BL");
        }

        if (this.mOperate.equals("DELETE||MAIN"))
        {
            map.put(mLHMedicalServiceRecordSchema, "DELETE");
        }

        return true;


}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
         this.mLHMedicalServiceRecordSchema.setSchema((LHMedicalServiceRecordSchema)cInputData.getObjectByObjectName("LHMedicalServiceRecordSchema",0));
         this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
    this.mResult.clear();
    LHMedicalServiceRecordDB tLHMedicalServiceRecordDB=new LHMedicalServiceRecordDB();
    tLHMedicalServiceRecordDB.setSchema(this.mLHMedicalServiceRecordSchema);
		//如果有需要处理的错误，则返回
		if (tLHMedicalServiceRecordDB.mErrors.needDealError())
 		{
		  // @@错误处理
 			this.mErrors.copyAllErrors(tLHMedicalServiceRecordDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "LHMedicalServiceRecordBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors .addOneError(tError) ;
 			return false;
    }
    mInputData=null;
    return true;
}
 private boolean prepareOutputData()
 {
   try{
       this.mInputData.clear();
       this.mInputData.add(this.mLHMedicalServiceRecordSchema);
       this.mInputData.add(map);
       mResult.clear();
       mResult.add(this.mLHMedicalServiceRecordSchema);
        }catch(Exception ex)
        {
 		// @@错误处理

               CError tError = new CError();
 		tError.moduleName="LHMdicalServiceRecordBL";
 //                tError.functionName="preareData";
                 tError.functionName="prepareData";
                 tError.errorMessage="在准备往后层处理所需要的数据时出错。";
 		//this.mErrors .addOneError(tError);
		this.mErrors.addOneError(tError);
            return false;
	}
	return true;
	}

	public VData getResult()
	{
  	return this.mResult;
	}
}
