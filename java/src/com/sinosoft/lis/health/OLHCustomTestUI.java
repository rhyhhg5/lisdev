/*
 * <p>ClassName: OLHCustomTestUI </p>
 * <p>Description: OLHCustomTestUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-17 18:32:42
 */
package com.sinosoft.lis.health;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.pubfun.*;
public class OLHCustomTestUI {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();

/** 往后面传输数据的容器 */
private VData mInputData =new VData();
/** 数据操作字符串 */
private String mOperate;
//业务处理相关变量
 /** 全局数据 */
private LHCustomTestSchema mLHCustomTestSchema=new LHCustomTestSchema();
private static String fileName;
private static String configFileName;
private static String SheetName = "TEST";


 /**
  * 构造方法
  * @param fileName String
  * @param configFileName String
  */

 public OLHCustomTestUI ()
{
}

 public OLHCustomTestUI (String fileName, String configFileName)
{
    this.fileName = fileName;
    this.configFileName = configFileName;
    System.out.println(fileName+"----"+configFileName);
}

 public boolean submitImport()
 {
     OLHCustomTestDiskImporter mOLHCustomTestDiskImporter = new OLHCustomTestDiskImporter(fileName, configFileName, SheetName);
     System.out.println("---PubSubmit----");
     if (!mOLHCustomTestDiskImporter.submitImport())
     {
         // @@错误处理
         this.mErrors.copyAllErrors(mOLHCustomTestDiskImporter.mErrrors);
         CError tError = new CError();
         tError.moduleName = "OLHCustomTestUI";
         tError.functionName = "submitImport";
         tError.errorMessage = "文件提交失败!";
         this.mErrors.addOneError(tError);
         return false;
     }
     return true;
 }
 /**
  *传输数据的公共方法
  */
public boolean submitData(VData cInputData,String cOperate)
{
  //将操作数据拷贝到本类中
  this.mOperate =cOperate;
  OLHCustomTestBL tOLHCustomTestBL=new OLHCustomTestBL();

  //System.out.println("Start OLHCustomTest UI Submit...");
  tOLHCustomTestBL.submitData(cInputData,mOperate);
  //System.out.println("End OLHCustomTest UI Submit...");
  //如果有需要处理的错误，则返回
  if (tOLHCustomTestBL.mErrors .needDealError() )
  {
     // @@错误处理
     this.mErrors.copyAllErrors(tOLHCustomTestBL.mErrors);
     CError tError = new CError();
     tError.moduleName = "OLHCustomTestUI";
     tError.functionName = "submitData";
     tError.errorMessage = "数据提交失败!";
     this.mErrors .addOneError(tError) ;
     return false;
  }
  if (mOperate.equals("INSERT||MAIN"))
  {
     this.mResult.clear();
     this.mResult=tOLHCustomTestBL.getResult();
  }
  if (mOperate.equals("UPDATE||MAIN"))
  {
     this.mResult.clear();
     this.mResult=tOLHCustomTestBL.getResult();
  }

  mInputData=null;
  return true;
  }
  public static void main(String[] args)
  {
      OLHCustomTestUI tOLHCustomTestUI = new OLHCustomTestUI("E:/workspace/ui/temp/LHCustomTest.xls", "E:/workspace/ui/temp/LHCustomTest.xml");
      tOLHCustomTestUI.submitImport();
  }
  /**
  * 准备往后层输出所需要的数据
  * 输出：如果准备数据时发生错误则返回false,否则返回true
  */
 private boolean prepareOutputData()
 {
    try
    {
      mInputData.clear();
      mInputData.add(this.mLHCustomTestSchema);
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LHCustomTestUI";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
}
/**
 * 根据前面的输入数据，进行UI逻辑处理
  * 如果在处理过程中出错，则返回false,否则返回true
  */
 private boolean dealData()
 {
      boolean tReturn =false;
      //此处增加一些校验代码
      tReturn=true;
      return tReturn ;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
  //全局变量
  this.mLHCustomTestSchema.setSchema((LHCustomTestSchema)cInputData.getObjectByObjectName("LHCustomTestSchema",0));

  return true;
}
public VData getResult()
{
  return this.mResult;
}
}
