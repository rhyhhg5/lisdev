/*
 * <p>ClassName: LHServPlanUpBL </p>
 * <p>Description: LHServPlanUpBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-03-09 10:43:01
 */
package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;

public class LHServPlanUpBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LHServPlanSchema mLHServPlanSchema = new LHServPlanSchema();
    public LHServPlanUpBL() {
    }

    public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData 输入的数据
     *         cOperate 数据操作
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServPlanUpBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LHServPlanUpBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start LHServPlanUpBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();

            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                CError tError = new CError();
                tError.moduleName = "LHServPlanUpBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }

        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     *
     */
    private boolean dealData() {
        System.out.println("--------DealData---------");
        System.out.println("AAAAA  AAA" + mLHServPlanSchema.getCaseState());
        if (this.mOperate.equals("UPDATE||MAIN")) {
            System.out.println("BBBBBBBBBBBBBBBBBBB" +
                               mLHServPlanSchema.getContNo());
            System.out.println("CCCCCCCCCCCCCCCCCccc" +
                               mLHServPlanSchema.getCustomerNo());

            mLHServPlanSchema.setOperator(mGlobalInput.Operator);
            mLHServPlanSchema.setMakeDate(PubFun.getCurrentDate());
            mLHServPlanSchema.setMakeTime(PubFun.getCurrentTime());
            mLHServPlanSchema.setManageCom(mGlobalInput.ManageCom);
            mLHServPlanSchema.setModifyDate(PubFun.getCurrentDate());
            mLHServPlanSchema.setModifyTime(PubFun.getCurrentTime());

            map.put(mLHServPlanSchema, "DELETE&INSERT");

            //考虑到核保体检费用结算时，也要生成服务计划，这里判断如果生成了正常的健管服务计划，则为正常流程需要修改lwmission的状态。
            String sql = " select count(*) from ( select distinct p.insuredno from lcpol p where p.contno ='" +
                         mLHServPlanSchema.getContNo() + "' and  p.riskcode in (select distinct m.riskcode from lmriskapp m where  m.risktype2 = '5')) as x";
            SSRS tSSRS = new ExeSQL().execSQL(sql);
            String rr[][] = tSSRS.getAllData();
            String sql2 =
                    " select count(distinct l.Customerno) from LHServPlan  l where l.contno='" +
                    mLHServPlanSchema.getContNo() +
                    "' and l.servplaytype <> '1H'";
            SSRS tSSRS2 = new ExeSQL().execSQL(sql2);
            String rr2[][] = tSSRS2.getAllData();
            if (rr[0][0].equals(rr2[0][0])) {
                map.put(" update lwmission set Missionprop20='1' ,Lastoperator='"+mGlobalInput.Operator
                        +"',ModifyDate='" +
                        PubFun.getCurrentDate() + "',ModifyTime = '" +
                        PubFun.getCurrentTime() + "'  where activityid = '0000001190' and  Missionprop1='" +
                        mLHServPlanSchema.getContNo() + "' ", "UPDATE");
            }
        }

        return true;
    }
    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true\
     * @return boolean
     * @param cInputData
     */
    private boolean getInputData(VData cInputData) {
        this.mLHServPlanSchema.setSchema((LHServPlanSchema) cInputData.
                                         getObjectByObjectName(
                                                 "LHServPlanSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     * @return boolean
     */
    private boolean submitquery() {
        this.mResult.clear();
        LHServPlanDB tLHServPlanDB = new LHServPlanDB();
        tLHServPlanDB.setSchema(this.mLHServPlanSchema);
        //如果有需要处理的错误，则返回
        if (tLHServPlanDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHServPlanDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHServPlanUpBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 准备往后层处理所需要的数据
     * @return boolean
     */

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLHServPlanSchema);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLHServPlanSchema);
            //mResult.add(this.mLHServItemSet);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServPlanUpBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 返回结果
     * @return VData
     */

    public VData getResult() {
        return this.mResult;
    }
}
