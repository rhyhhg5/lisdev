/*
 * <p>ClassName: OLHICDOPSTypeBL </p>
 * <p>Description: OLHICDOPSTypeBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-04-28 16:59:22
 */
package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLHICDOPSTypeBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LHICDOPSTypeSchema mLHICDOPSTypeSchema = new LHICDOPSTypeSchema();
//private LHICDOPSTypeSet mLHICDOPSTypeSet=new LHICDOPSTypeSet();
    public OLHICDOPSTypeBL() {
    }

    public static void main(String[] args) {

        LHICDOPSTypeSchema tLHICDOPSTypeSchema = new LHICDOPSTypeSchema();
        OLHICDOPSTypeUI tOLHICDOPSTypeUI = new OLHICDOPSTypeUI();
        //输出参数
        String transact = "";
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "8600";
        tG.Operator = "001";
        tG.ComCode = "8600";

        CErrors tError = null;
        String tRela = "";
        String FlagStr = "";
        String Content = "";

        System.out.println("ddddddddddddddd");

        //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录


        tLHICDOPSTypeSchema.setOPSTypeLevel("OPSTypeLevel");
        tLHICDOPSTypeSchema.setICDBeginCode("ICDBeginCode");
        tLHICDOPSTypeSchema.setICDEndCode("ICDEndCode");
        tLHICDOPSTypeSchema.setOPSType("OPSType");

        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(tLHICDOPSTypeSchema);
        tVData.add(tG);
        tOLHICDOPSTypeUI.submitData(tVData, transact);

        System.out.println(tVData);

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        this.mInputData = cInputData;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLHICDOPSTypeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLHICDOPSTypeBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start OLDICDDiseasTypeBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLHICDOPSTypeBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("End OLHICDOPSTypeBL Submit...");

        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        if (this.mOperate.equals("INSERT||MAIN")) {
            map.put(mLHICDOPSTypeSchema, "INSERT");
        }
        if (this.mOperate.equals("UPDATE||MAIN")) {
            System.out.println("---------------In Update---------------");
            map.put("update LHICDOPSType set  OPSTypeLevel='"
                    +mLHICDOPSTypeSchema.getOPSTypeLevel()
                    +"',ICDBeginCode='"+mLHICDOPSTypeSchema.getICDBeginCode()
                    +"',ICDEndCode='"+mLHICDOPSTypeSchema.getICDEndCode()
                    +"',OPSType='"+mLHICDOPSTypeSchema.getOPSType()
                    +"' where OPSTypeLevel='"+mTransferData.getValueByName("OPSTypeLevel_origin")
                    +"' and ICDBeginCode='"+mTransferData.getValueByName("ICDBeginCode_origin")
                    +"' and ICDEndCode='"+mTransferData.getValueByName("ICDEndCode_origin")
                    +"'", "UPDATE");
        }
        if (this.mOperate.equals("DELETE||MAIN")) {
            map.put(mLHICDOPSTypeSchema, "DELETE");
        }

        return true;

    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLHICDOPSTypeSchema.setSchema((LHICDOPSTypeSchema) cInputData.
                                           getObjectByObjectName(
                "LHICDOPSTypeSchema", 0));
    if (mTransferData != null) {
        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
//        System.out.println(mTransferData.getValueByName("DiseasTypeLevel_origin")
//                +"' and ICDBeginCode='"+mTransferData.getValueByName("ICDBeginCode_origin")
//                +"' and ICDEndCode='"+mTransferData.getValueByName("ICDEndCode_origin"));
    }

        //this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LHICDOPSTypeDB tLHICDOPSTypeDB = new LHICDOPSTypeDB();
        tLHICDOPSTypeDB.setSchema(this.mLHICDOPSTypeSchema);
        //如果有需要处理的错误，则返回
        if (tLHICDOPSTypeDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHICDOPSTypeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHICDOPSTypeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLHICDOPSTypeSchema);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLHICDOPSTypeSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHICDOPSTypeBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
