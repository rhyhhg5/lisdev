package com.sinosoft.lis.health;

/**
 * <p>Title:OLHUnitContraUI </p>
 *
 * <p>Description:OLHUnitContraUI </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author lyg
 * @version 1.0
 * @CreateDate：2005-04-121 18:32:42
 */
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.pubfun.*;

public class LHUnitContraUI {
    public LHUnitContraUI() {
    }

    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
//业务处理相关变量
    /** 全局数据 */
    private LHUnitContraSchema mLHUnitContraSchema = new LHUnitContraSchema();
    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        LHUnitContraBL tLHUnitContraBL = new LHUnitContraBL();

        tLHUnitContraBL.submitData(cInputData, mOperate);

        //如果有需要处理的错误，则返回
        if (tLHUnitContraBL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHUnitContraBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHUnitContraUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mOperate.equals("INSERT||MAIN")) {
            this.mResult.clear();
            this.mResult = tLHUnitContraBL.getResult();
        }
        if (mOperate.equals("UPDATE||MAIN")) {
            this.mResult.clear();
            this.mResult = tLHUnitContraBL.getResult();
        }
        if (mOperate.equals("DELETE||MAIN")) {
            this.mResult.clear();
            this.mResult = tLHUnitContraBL.getResult();
        }

        mInputData = null;
        return true;
    }

    public static void main(String[] args) {
        LHUnitContraUI tLHUnitContraUI = new LHUnitContraUI();
        LHUnitContraSchema tLHUnitContraSchema = new LHUnitContraSchema();
        LHContraAssoSettingSet tLHContraAssoSettingSet = new
                LHContraAssoSettingSet();
        LHContItemSet tLHContItemSet = new LHContItemSet();
        GlobalInput tG = new GlobalInput();

        VData tVData = new VData();
        tVData.add(tLHUnitContraSchema);
        tVData.add(tLHContraAssoSettingSet);
        tVData.add(tLHContItemSet);
        tVData.add(tG);

        tLHUnitContraUI.submitData(tVData, "INSERT||MAIN");

    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(this.mLHUnitContraSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHUnitContraUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        this.mLHUnitContraSchema.setSchema((LHUnitContraSchema) cInputData.
                                           getObjectByObjectName(
                "LHUnitContraSchema", 0));

        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

}
