/*
 * <p>ClassName: OLHCustomInHospitalBL </p>
 * <p>Description: OLHCustomInHospitalBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-18 09:08:27
 */
package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLHCustomInHospitalBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String tNo;
    /** 业务处理相关变量 */
    private LHCustomInHospitalSchema mLHCustomInHospitalSchema = new
            LHCustomInHospitalSchema();
//private LHCustomInHospitalSet mLHCustomInHospitalSet=new LHCustomInHospitalSet();
    private LHDiagnoSet mLHDiagnoSet = new LHDiagnoSet();
    private LHCustomTestSet mLHCustomTestSet = new LHCustomTestSet();
    private LHCustomOPSSet mLHCustomOPSSet = new LHCustomOPSSet();
    private LHCustomOtherCureSet mLHCustomOtherCureSet = new
            LHCustomOtherCureSet();
    private LHFeeInfoSet mLHFeeInfoSet = new LHFeeInfoSet();
    public OLHCustomInHospitalBL() {
    }

    public static void main(String[] args) {

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLHCustomInHospitalBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLHCustomInHospitalBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start OLHCustomInHospitalBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LHCustomInHospitalBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("End OLHCustomInHospitalBL Submit...");

        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        if (this.mOperate.equals("INSERT||MAIN")) {
            try {
                SSRS tSSRS = new SSRS();
                String sql = "Select Case When max(to_number(InHospitNo)) Is Null Then 0 Else max(to_number(InHospitNo)) End from LHCustomInHospital where CustomerNo='"
                             + mLHCustomInHospitalSchema.getCustomerNo() +
                             "'";
                ExeSQL tExeSQL = new ExeSQL();
                tSSRS = tExeSQL.execSQL(sql);
                Integer firstinteger = Integer.valueOf(tSSRS.GetText(1, 1));
                int ttNo = firstinteger.intValue() + 1;
                Integer integer = new Integer(ttNo);
                tNo = integer.toString();
                System.out.println("得到的InHospitNo是：" + tNo);
                mLHCustomInHospitalSchema.setInHospitNo(tNo);

            } catch (Exception e) {
                CError tError = new CError();
                tError.moduleName = "OLHCustomInHospitalBL";
                tError.functionName = "createAddressNo";
                tError.errorMessage = "地址码超长,生成InHospitNo失败,请先删除原来的超长地址码!";
                this.mErrors.addOneError(tError);
                mLHCustomInHospitalSchema.setInHospitNo("");
            }

            mLHCustomInHospitalSchema.setOperator(mGlobalInput.Operator);
            mLHCustomInHospitalSchema.setMakeDate(PubFun.getCurrentDate());
            mLHCustomInHospitalSchema.setMakeTime(PubFun.getCurrentTime());
            mLHCustomInHospitalSchema.setModifyDate(PubFun.getCurrentDate());
            mLHCustomInHospitalSchema.setModifyTime(PubFun.getCurrentTime());
            mLHCustomInHospitalSchema.setManageCom(mGlobalInput.ManageCom);
            map.put(mLHCustomInHospitalSchema, "INSERT");

            if (mLHDiagnoSet != null && mLHDiagnoSet.size() > 0) {
                int j_DiagnoseNo = 1;
                for (int i = 1; i <= mLHDiagnoSet.size(); i++) {
                    mLHDiagnoSet.get(i).setInHospitNo(
                            mLHCustomInHospitalSchema.
                            getInHospitNo());
                    try {
                        SSRS tSSRS = new SSRS();
                        String sql = "Select Case When max(to_number(DiagnoseNo)) Is Null Then 0 Else max(to_number(DiagnoseNo)) End from LHDiagno where CustomerNo='"
                                     +
                                     mLHCustomInHospitalSchema.getCustomerNo() +
                                     "' and InHospitNo='" +
                                     mLHCustomInHospitalSchema.getInHospitNo() +
                                     "' ";
                        ExeSQL tExeSQL = new ExeSQL();
                        tSSRS = tExeSQL.execSQL(sql);
                        Integer firstinteger = Integer.valueOf(tSSRS.
                                GetText(1, 1));
                        int ttNo = firstinteger.intValue() + (j_DiagnoseNo++);
                        Integer integer = new Integer(ttNo);
                        tNo = integer.toString();
                        System.out.println("得到的DiagnoseNo是：" + tNo);
                        mLHDiagnoSet.get(i).setDiagnoseNo(tNo);

                    } catch (Exception e) {
                        CError tError = new CError();
                        tError.moduleName = "OLHCustomInHospitalBL";
                        tError.functionName = "createAddressNo";
                        tError.errorMessage =
                                "地址码超长,生成DiagnoseNo失败,请先删除原来的超长地址码!";
                        this.mErrors.addOneError(tError);
                        mLHDiagnoSet.get(i).setDiagnoseNo("");
                    }

                    mLHDiagnoSet.get(i).setOperator(mGlobalInput.
                            Operator);

                    mLHDiagnoSet.get(i).setMakeDate(PubFun.
                            getCurrentDate());
                    mLHDiagnoSet.get(i).setMakeTime(PubFun.
                            getCurrentTime());
                    mLHDiagnoSet.get(i).setModifyDate(PubFun.
                            getCurrentDate());
                    mLHDiagnoSet.get(i).setModifyTime(PubFun.
                            getCurrentTime());
                    mLHDiagnoSet.get(i).setManageCom(mGlobalInput.ManageCom);

                }
                map.put(mLHDiagnoSet, "INSERT");
            }

            if (mLHCustomTestSet != null && mLHCustomTestSet.size() > 0) {
                int j_TestNo = 1;
                for (int i = 1; i <= mLHCustomTestSet.size(); i++) {
                    mLHCustomTestSet.get(i).setInHospitNo(
                            mLHCustomInHospitalSchema.getInHospitNo());
                    try {
                        SSRS tSSRS = new SSRS();
                        String sql = "Select Case When max(to_number(TestNo)) Is Null Then 0 Else max(to_number(TestNo)) End from LHCustomTest where CustomerNo='"
                                     +
                                     mLHCustomInHospitalSchema.getCustomerNo() +
                                     "' and InHospitNo='" +
                                     mLHCustomInHospitalSchema.getInHospitNo() +
                                     "' ";
                        ExeSQL tExeSQL = new ExeSQL();
                        tSSRS = tExeSQL.execSQL(sql);
                        Integer firstinteger = Integer.valueOf(tSSRS.
                                GetText(1, 1));
                        int ttNo = firstinteger.intValue() + (j_TestNo++);
                        Integer integer = new Integer(ttNo);
                        tNo = integer.toString();
                        System.out.println("得到的TestNo是：" + tNo);
                        mLHCustomTestSet.get(i).setTestNo(tNo);

                    } catch (Exception e) {
                        CError tError = new CError();
                        tError.moduleName = "OLHCustomInHospitalBL";
                        tError.functionName = "createAddressNo";
                        tError.errorMessage =
                                "地址码超长,生成TestNo失败,请先删除原来的超长地址码!";
                        this.mErrors.addOneError(tError);
                        mLHCustomTestSet.get(i).setTestNo("");
                    }

                    mLHCustomTestSet.get(i).setOperator(mGlobalInput.
                            Operator);
                    mLHCustomTestSet.get(i).setMakeDate(PubFun.
                            getCurrentDate());
                    mLHCustomTestSet.get(i).setMakeTime(PubFun.
                            getCurrentTime());
                    mLHCustomTestSet.get(i).setModifyDate(PubFun.
                            getCurrentDate());
                    mLHCustomTestSet.get(i).setModifyTime(PubFun.
                            getCurrentTime());
                    mLHCustomTestSet.get(i).setManageCom(mGlobalInput.ManageCom);

                }
                map.put(mLHCustomTestSet, "INSERT");
            }
            if (mLHCustomOPSSet != null && mLHCustomOPSSet.size() > 0) {
                int j_OPSNo = 1;
                for (int i = 1; i <= mLHCustomOPSSet.size(); i++) {
                    mLHCustomOPSSet.get(i).setInHospitNo(
                            mLHCustomInHospitalSchema.getInHospitNo());
                    try {
                        SSRS tSSRS = new SSRS();
                        String sql = "Select Case When max(to_number(OPSNo)) Is Null Then 0 Else max(to_number(OPSNo)) End from LHCustomOPS where CustomerNo='"
                                     +
                                     mLHCustomInHospitalSchema.getCustomerNo() +
                                     "' and InHospitNo='" +
                                     mLHCustomInHospitalSchema.getInHospitNo() +
                                     "' ";
                        ExeSQL tExeSQL = new ExeSQL();
                        tSSRS = tExeSQL.execSQL(sql);
                        Integer firstinteger = Integer.valueOf(tSSRS.
                                GetText(1, 1));
                        int ttNo = firstinteger.intValue() + (j_OPSNo++);
                        Integer integer = new Integer(ttNo);
                        tNo = integer.toString();
                        System.out.println("得到的OPSNo是：" + tNo);
                        mLHCustomOPSSet.get(i).setOPSNo(tNo);

                    } catch (Exception e) {
                        CError tError = new CError();
                        tError.moduleName = "OLHCustomInHospitalBL";
                        tError.functionName = "createAddressNo";
                        tError.errorMessage =
                                "地址码超长,生成OPSNo失败,请先删除原来的超长地址码!";
                        this.mErrors.addOneError(tError);
                        mLHCustomOPSSet.get(i).setOPSNo("");
                    }

                    mLHCustomOPSSet.get(i).setOperator(mGlobalInput.
                            Operator);
                    mLHCustomOPSSet.get(i).setMakeDate(PubFun.
                            getCurrentDate());
                    mLHCustomOPSSet.get(i).setMakeTime(PubFun.
                            getCurrentTime());
                    mLHCustomOPSSet.get(i).setModifyDate(PubFun.
                            getCurrentDate());
                    mLHCustomOPSSet.get(i).setModifyTime(PubFun.
                            getCurrentTime());
                    mLHCustomOPSSet.get(i).setManageCom(mGlobalInput.ManageCom);

                }
                map.put(mLHCustomOPSSet, "INSERT");
            }
            if (mLHCustomOtherCureSet != null &&
                mLHCustomOtherCureSet.size() > 0) {
                int j_OtherCureNo = 1;
                for (int i = 1; i <= mLHCustomOtherCureSet.size(); i++) {
                    mLHCustomOtherCureSet.get(i).setInHospitNo(
                            mLHCustomInHospitalSchema.getInHospitNo());
                    try {
                        SSRS tSSRS = new SSRS();
                        String sql = "Select Case When max(to_number(OtherCureNo)) Is Null Then 0 Else max(to_number(OtherCureNo)) End from LHCustomOtherCure where CustomerNo='"
                                     +
                                     mLHCustomInHospitalSchema.getCustomerNo() +
                                     "' and InHospitNo='" +
                                     mLHCustomInHospitalSchema.getInHospitNo() +
                                     "' ";
                        ExeSQL tExeSQL = new ExeSQL();
                        tSSRS = tExeSQL.execSQL(sql);
                        Integer firstinteger = Integer.valueOf(tSSRS.
                                GetText(1, 1));
                        int ttNo = firstinteger.intValue() + (j_OtherCureNo++);
                        Integer integer = new Integer(ttNo);
                        tNo = integer.toString();
                        System.out.println("得到的OtherCureNo是：" + tNo);
                        mLHCustomOtherCureSet.get(i).setOtherCureNo(tNo);

                    } catch (Exception e) {
                        CError tError = new CError();
                        tError.moduleName = "OLHCustomInHospitalBL";
                        tError.functionName = "createAddressNo";
                        tError.errorMessage =
                                "地址码超长,生成OtherCureNo失败,请先删除原来的超长地址码!";
                        this.mErrors.addOneError(tError);
                        mLHCustomOtherCureSet.get(i).setOtherCureNo("");
                    }

                    mLHCustomOtherCureSet.get(i).setOperator(mGlobalInput.
                            Operator);
                    mLHCustomOtherCureSet.get(i).setMakeDate(PubFun.
                            getCurrentDate());
                    mLHCustomOtherCureSet.get(i).setMakeTime(PubFun.
                            getCurrentTime());
                    mLHCustomOtherCureSet.get(i).setModifyDate(PubFun.
                            getCurrentDate());
                    mLHCustomOtherCureSet.get(i).setModifyTime(PubFun.
                            getCurrentTime());
                    mLHCustomOtherCureSet.get(i).setManageCom(mGlobalInput.ManageCom);

                }
                map.put(mLHCustomOtherCureSet, "INSERT");
            }

            if (mLHFeeInfoSet != null &&
                mLHFeeInfoSet.size() > 0) {
                int j_FeeNo = 1;
                for (int i = 1; i <= mLHFeeInfoSet.size(); i++) {
                    mLHFeeInfoSet.get(i).setInHospitNo(
                            mLHCustomInHospitalSchema.getInHospitNo());

//                    mLHFeeInfoSet.get(i).setFeeNo(PubFun1.
//                                                  CreateMaxNo("fee", 9));
                    try {
                        SSRS tSSRS = new SSRS();
                        String sql = "Select Case When max(to_number(FeeNo)) Is Null Then 0 Else max(to_number(FeeNo)) End from LHFeeInfo where CustomerNo='"
                                     +
                                     mLHCustomInHospitalSchema.getCustomerNo() +
                                     "' and InHospitNo='" +
                                     mLHCustomInHospitalSchema.getInHospitNo() +
                                     "' ";
                        ExeSQL tExeSQL = new ExeSQL();
                        tSSRS = tExeSQL.execSQL(sql);
                        Integer firstinteger = Integer.valueOf(tSSRS.
                                GetText(1, 1));
                        int ttNo = firstinteger.intValue() + (j_FeeNo++);
                        Integer integer = new Integer(ttNo);
                        tNo = integer.toString();
                        System.out.println("得到的OtherCureNo是：" + tNo);
                        mLHFeeInfoSet.get(i).setFeeNo(tNo);

                    } catch (Exception e) {
                        CError tError = new CError();
                        tError.moduleName = "OLHCustomInHospitalBL";
                        tError.functionName = "createAddressNo";
                        tError.errorMessage =
                                "地址码超长,生成OtherCureNo失败,请先删除原来的超长地址码!";
                        this.mErrors.addOneError(tError);
                        mLHFeeInfoSet.get(i).setFeeNo("");
                    }

                    mLHFeeInfoSet.get(i).setOperator(mGlobalInput.
                            Operator);
                    mLHFeeInfoSet.get(i).setMakeDate(PubFun.
                            getCurrentDate());
                    mLHFeeInfoSet.get(i).setMakeTime(PubFun.
                            getCurrentTime());
                    mLHFeeInfoSet.get(i).setModifyDate(PubFun.
                            getCurrentDate());
                    mLHFeeInfoSet.get(i).setModifyTime(PubFun.
                            getCurrentTime());
                    mLHFeeInfoSet.get(i).setManageCom(mGlobalInput.ManageCom);

                }
                map.put(mLHFeeInfoSet, "INSERT");
            }

        }

        if (this.mOperate.equals("UPDATE||MAIN")) {
            mLHCustomInHospitalSchema.setModifyDate(PubFun.getCurrentDate());
            mLHCustomInHospitalSchema.setModifyTime(PubFun.getCurrentTime());
            mLHCustomInHospitalSchema.setOperator(mGlobalInput.Operator);
            mLHCustomInHospitalSchema.setManageCom(mGlobalInput.ManageCom);

            map.put(mLHCustomInHospitalSchema, "DELETE&INSERT");

//            if (mLHDiagnoSet != null && mLHDiagnoSet.size() > 0) {
            int j_DiagNo = 1;
            for (int i = 1; i <= mLHDiagnoSet.size(); i++) {
                //若DiagnoseNo不存在则进行生成
                if (mLHDiagnoSet.get(i).getDiagnoseNo() == null ||
                    mLHDiagnoSet.get(i).getDiagnoseNo().equals("")) {
                    try {
                        SSRS tSSRS = new SSRS();
                        String sql = "Select Case When max(to_number(DiagnoseNo)) Is Null Then 0 Else max(to_number(DiagnoseNo)) End from LHDiagno where CustomerNo='"
                                     +
                                     mLHCustomInHospitalSchema.
                                     getCustomerNo() +
                                     "' and InHospitNo='" +
                                     mLHCustomInHospitalSchema.
                                     getInHospitNo() +
                                     "' ";
                        ExeSQL tExeSQL = new ExeSQL();
                        tSSRS = tExeSQL.execSQL(sql);
                        Integer firstinteger = Integer.valueOf(tSSRS.
                                GetText(1, 1));
                        int ttNo = firstinteger.intValue() + (j_DiagNo++);
                        Integer integer = new Integer(ttNo);
                        tNo = integer.toString();
                        System.out.println("得到的DiagnoseNo是：" + tNo);
                        mLHDiagnoSet.get(i).setDiagnoseNo(tNo);

                    } catch (Exception e) {
                        CError tError = new CError();
                        tError.moduleName = "OLHCustomInHospitalBL";
                        tError.functionName = "createAddressNo";
                        tError.errorMessage =
                                "地址码超长,生成DiagnoseNo失败,请先删除原来的超长地址码!";
                        this.mErrors.addOneError(tError);
                        mLHDiagnoSet.get(i).setDiagnoseNo("");
                    }

                }

                mLHDiagnoSet.get(i).setOperator(mGlobalInput.
                                                Operator);
                mLHDiagnoSet.get(i).setModifyDate(PubFun.
                                                  getCurrentDate());
                mLHDiagnoSet.get(i).setModifyTime(PubFun.
                                                  getCurrentTime());
                mLHDiagnoSet.get(i).setManageCom(mGlobalInput.ManageCom);

            }
            map.put("DELETE FROM lhdiagno where CustomerNo = '" +
                    mLHCustomInHospitalSchema.getCustomerNo() +
                    "' and InhospitNo = '" +
                    mLHCustomInHospitalSchema.getInHospitNo() +
                    "' ", "DELETE");
            map.put(mLHDiagnoSet, "INSERT");
//            }

//            if (mLHCustomTestSet != null && mLHCustomTestSet.size() > 0) {
            int j_TestNo = 1;
            for (int i = 1; i <= mLHCustomTestSet.size(); i++) {
                if (mLHCustomTestSet.get(i).getTestNo() == null ||
                    mLHCustomTestSet.get(i).getTestNo().equals("")) {

                    try {
                        SSRS tSSRS = new SSRS();
                        String sql = "Select Case When max(to_number(TestNo)) Is Null Then 0 Else max(to_number(TestNo)) End from LHCustomTest where CustomerNo='"
                                     +
                                     mLHCustomInHospitalSchema.
                                     getCustomerNo() +
                                     "' and InHospitNo='" +
                                     mLHCustomInHospitalSchema.
                                     getInHospitNo() +
                                     "' ";
                        ExeSQL tExeSQL = new ExeSQL();
                        tSSRS = tExeSQL.execSQL(sql);
                        Integer firstinteger = Integer.valueOf(tSSRS.
                                GetText(1, 1));
                        int ttNo = firstinteger.intValue() + (j_TestNo++);
                        Integer integer = new Integer(ttNo);
                        tNo = integer.toString();
                        System.out.println("得到的TestNo是：" + tNo);
                        mLHCustomTestSet.get(i).setTestNo(tNo);
                        System.out.println(tNo);

                    } catch (Exception e) {
                        CError tError = new CError();
                        tError.moduleName = "OLHCustomInHospitalBL";
                        tError.functionName = "createAddressNo";
                        tError.errorMessage =
                                "地址码超长,生成TestNo失败,请先删除原来的超长地址码!";
                        this.mErrors.addOneError(tError);
                        mLHCustomTestSet.get(i).setTestNo("");
                    }
                }

                mLHCustomTestSet.get(i).setOperator(
                        mGlobalInput.
                        Operator);
                System.out.println(mLHCustomTestSet.get(i).getOperator());
                mLHCustomTestSet.get(i).setModifyDate(PubFun.
                        getCurrentDate());
                mLHCustomTestSet.get(i).setModifyTime(PubFun.
                        getCurrentTime());
                mLHCustomTestSet.get(i).setManageCom(mGlobalInput.ManageCom);
            }

            map.put("DELETE FROM LHCustomTest where CustomerNo = '" +
                    mLHCustomInHospitalSchema.getCustomerNo() +
                    "' and InhospitNo = '" +
                    mLHCustomInHospitalSchema.getInHospitNo() +
                    "' ", "DELETE");
            map.put(mLHCustomTestSet, "INSERT");

//            }
//            if (mLHCustomOPSSet != null && mLHCustomOPSSet.size() > 0) {
            int j_OPSNo = 1;
            for (int i = 1; i <= mLHCustomOPSSet.size(); i++) {
                if (mLHCustomOPSSet.get(i).getOPSNo() == null ||
                    mLHCustomOPSSet.get(i).getOPSNo().equals("")) {
                    try {
                        SSRS tSSRS = new SSRS();
                        String sql = "Select Case When max(to_number(OPSNo)) Is Null Then 0 Else max(to_number(OPSNo)) End from LHCustomOPS where CustomerNo='"
                                     +
                                     mLHCustomInHospitalSchema.
                                     getCustomerNo() +
                                     "' and InHospitNo='" +
                                     mLHCustomInHospitalSchema.
                                     getInHospitNo() +
                                     "' ";
                        ExeSQL tExeSQL = new ExeSQL();
                        tSSRS = tExeSQL.execSQL(sql);
                        Integer firstinteger = Integer.valueOf(tSSRS.
                                GetText(1, 1));
                        int ttNo = firstinteger.intValue() + (j_OPSNo++);
                        Integer integer = new Integer(ttNo);
                        tNo = integer.toString();
                        System.out.println("得到的OPSNo是：" + tNo);
                        mLHCustomOPSSet.get(i).setOPSNo(tNo);

                    } catch (Exception e) {
                        CError tError = new CError();
                        tError.moduleName = "OLHCustomInHospitalBL";
                        tError.functionName = "createAddressNo";
                        tError.errorMessage =
                                "地址码超长,生成OPSNo失败,请先删除原来的超长地址码!";
                        this.mErrors.addOneError(tError);
                        mLHCustomOPSSet.get(i).setOPSNo("");
                    }

                }
                mLHCustomOPSSet.get(i).setOperator(mGlobalInput.
                        Operator);

                mLHCustomOPSSet.get(i).setModifyDate(PubFun.
                        getCurrentDate());
                mLHCustomOPSSet.get(i).setModifyTime(PubFun.
                        getCurrentTime());
                mLHCustomOPSSet.get(i).setManageCom(mGlobalInput.ManageCom);
            }
            map.put("DELETE FROM LHCustomOPS where CustomerNo = '" +
                    mLHCustomInHospitalSchema.getCustomerNo() +
                    "' and InhospitNo = '" +
                    mLHCustomInHospitalSchema.getInHospitNo() +
                    "' ", "DELETE");
            map.put(mLHCustomOPSSet, "INSERT");
//            }
//            if (mLHCustomOtherCureSet != null &&
//                mLHCustomOtherCureSet.size() > 0) {
            int j_OtherCureNo = 1;
            for (int i = 1; i <= mLHCustomOtherCureSet.size(); i++) {
                if (mLHCustomOtherCureSet.get(i).getOtherCureNo() == null ||
                    mLHCustomOtherCureSet.get(i).getOtherCureNo().equals("")) {
                    try {
                        SSRS tSSRS = new SSRS();
                        String sql = "Select Case When max(to_number(OtherCureNo)) Is Null Then 0 Else max(to_number(OtherCureNo)) End from LHCustomOtherCure where CustomerNo='"
                                     +
                                     mLHCustomInHospitalSchema.
                                     getCustomerNo() +
                                     "' and InHospitNo='" +
                                     mLHCustomInHospitalSchema.
                                     getInHospitNo() +
                                     "' ";
                        ExeSQL tExeSQL = new ExeSQL();
                        tSSRS = tExeSQL.execSQL(sql);
                        Integer firstinteger = Integer.valueOf(tSSRS.
                                GetText(1, 1));
                        int ttNo = firstinteger.intValue() + (j_OtherCureNo++);
                        Integer integer = new Integer(ttNo);
                        tNo = integer.toString();
                        System.out.println("得到的OtherCureNo是：" + tNo);
                        mLHCustomOtherCureSet.get(i).setOtherCureNo(tNo);

                    } catch (Exception e) {
                        CError tError = new CError();
                        tError.moduleName = "OLHCustomInHospitalBL";
                        tError.functionName = "createAddressNo";
                        tError.errorMessage =
                                "地址码超长,生成OtherCureNo失败,请先删除原来的超长地址码!";
                        this.mErrors.addOneError(tError);
                        mLHCustomOtherCureSet.get(i).setOtherCureNo("");
                    }

                }

                mLHCustomOtherCureSet.get(i).setOperator(mGlobalInput.
                        Operator);

                mLHCustomOtherCureSet.get(i).setModifyDate(PubFun.
                        getCurrentDate());
                mLHCustomOtherCureSet.get(i).setModifyTime(PubFun.
                        getCurrentTime());
                mLHCustomOtherCureSet.get(i).setManageCom(mGlobalInput.ManageCom);

            }
            map.put("DELETE FROM LHCustomOtherCure where CustomerNo = '" +
                    mLHCustomInHospitalSchema.getCustomerNo() +
                    "' and InhospitNo = '" +
                    mLHCustomInHospitalSchema.getInHospitNo() +
                    "' ", "DELETE");
            map.put(mLHCustomOtherCureSet, "INSERT");

//           }

//            if (mLHFeeInfoSet != null &&
//                mLHFeeInfoSet.size() > 0) {
            int j_FeeNo = 1;
            for (int i = 1; i <= mLHFeeInfoSet.size(); i++) {
                if (mLHFeeInfoSet.get(i).getFeeNo() == null ||
                    mLHFeeInfoSet.get(i).getFeeNo().equals("")) {
                    try {
                        SSRS tSSRS = new SSRS();
                        String sql = "Select Case When max(to_number(FeeNo)) Is Null Then 0 Else max(to_number(FeeNo)) End from LHFeeInfo where CustomerNo='"
                                     +
                                     mLHCustomInHospitalSchema.
                                     getCustomerNo() +
                                     "' and InHospitNo='" +
                                     mLHCustomInHospitalSchema.
                                     getInHospitNo() +
                                     "' ";
                        ExeSQL tExeSQL = new ExeSQL();
                        tSSRS = tExeSQL.execSQL(sql);
                        Integer firstinteger = Integer.valueOf(tSSRS.
                                GetText(1, 1));
                        int ttNo = firstinteger.intValue() + (j_FeeNo++);
                        Integer integer = new Integer(ttNo);
                        tNo = integer.toString();
                        System.out.println("得到的OtherCureNo是：" + tNo);
                        mLHFeeInfoSet.get(i).setFeeNo(tNo);

                    } catch (Exception e) {
                        CError tError = new CError();
                        tError.moduleName = "OLHCustomInHospitalBL";
                        tError.functionName = "createAddressNo";
                        tError.errorMessage =
                                "地址码超长,生成OtherCureNo失败,请先删除原来的超长地址码!";
                        this.mErrors.addOneError(tError);
                        mLHFeeInfoSet.get(i).setFeeNo("");
                    }

                }

                mLHFeeInfoSet.get(i).setOperator(mGlobalInput.
                                                 Operator);

                mLHFeeInfoSet.get(i).setModifyDate(PubFun.
                        getCurrentDate());
                mLHFeeInfoSet.get(i).setModifyTime(PubFun.
                        getCurrentTime());
                mLHFeeInfoSet.get(i).setManageCom(mGlobalInput.ManageCom);

            }
            map.put("DELETE FROM LHFeeInfo where CustomerNo = '" + mLHCustomInHospitalSchema.getCustomerNo() +
                    "' and InhospitNo = '" + mLHCustomInHospitalSchema.getInHospitNo() + "' ","DELETE");
            map.put(mLHFeeInfoSet, "INSERT");
        }

//        }

        if (this.mOperate.equals("DELETE||MAIN")) {
            map.put(mLHCustomInHospitalSchema, "DELETE");

            map.put(mLHDiagnoSet, "DELETE");
            map.put(mLHCustomOPSSet, "DELETE");
            map.put(mLHCustomOtherCureSet, "DELETE");
            map.put(mLHFeeInfoSet, "DELETE");
            map.put(mLHCustomTestSet, "DELETE");
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLHCustomInHospitalSchema.setSchema((LHCustomInHospitalSchema)
                                                 cInputData.
                                                 getObjectByObjectName(
                "LHCustomInHospitalSchema", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));

        this.mLHCustomTestSet.set((LHCustomTestSet) cInputData.
                                  getObjectByObjectName("LHCustomTestSet", 0));

        this.mLHDiagnoSet.set((LHDiagnoSet) cInputData.
                              getObjectByObjectName(
                                      "LHDiagnoSet", 0));
        this.mLHCustomOPSSet.set((LHCustomOPSSet) cInputData.
                                 getObjectByObjectName("LHCustomOPSSet", 0));
       // this.mLHCustomOtherCureSet.set((LHCustomOtherCureSet) cInputData.
                                      // getObjectByObjectName(
                                          //     "LHCustomOtherCureSet", 0));
        this.mLHFeeInfoSet.set((LHFeeInfoSet) cInputData.
                               getObjectByObjectName(
                                       "LHFeeInfoSet", 0));

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LHCustomInHospitalDB tLHCustomInHospitalDB = new
                LHCustomInHospitalDB();
        tLHCustomInHospitalDB.setSchema(this.mLHCustomInHospitalSchema);
        //如果有需要处理的错误，则返回
        if (tLHCustomInHospitalDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHCustomInHospitalDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHCustomInHospitalBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLHCustomInHospitalSchema);
            this.mInputData.add(this.mLHCustomTestSet);
            this.mInputData.add(this.mLHDiagnoSet);
            this.mInputData.add(this.mLHCustomOPSSet);
            this.mInputData.add(this.mLHCustomOtherCureSet);
            this.mInputData.add(this.mLHFeeInfoSet);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLHCustomInHospitalSchema);
            mResult.add(this.mLHCustomTestSet);
            mResult.add(this.mLHDiagnoSet);
            mResult.add(this.mLHCustomOPSSet);
            mResult.add(this.mLHCustomOtherCureSet);
            mResult.add(this.mLHFeeInfoSet);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHCustomInHospitalBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
