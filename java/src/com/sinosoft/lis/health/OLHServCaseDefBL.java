package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLHServCaseDefBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public  CErrors mErrors=new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData= new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput =new GlobalInput() ;
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LHServCaseDefSchema mLHServCaseDefSchema=new LHServCaseDefSchema();
    private LHServCaseRelaSet mLHServCaseRelaSet=new LHServCaseRelaSet();

    public OLHServCaseDefBL() {
    }

    public boolean submitData(VData cInputData,String cOperate)
    {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    this.mInputData = cInputData;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    System.out.println("<<<<<<<<<<<<<<<<  this.mOperate  "+this.mOperate);
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "OLHServCaseDefBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败OLHServCaseDefBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }

    String a= mLHServCaseDefSchema.getServCaseCode();
    String b= "select ServCaseCode from LHServCaseDef where ServCaseCode = '"+a+"'";
    ExeSQL tExeSQL = new ExeSQL();

    SSRS tTask = null;
    tTask =  tExeSQL.execSQL(b);
    System.out.println(tTask.getMaxRow());
    if (this.mOperate.equals("INSERT||MAIN") && tTask.getMaxRow()!=0)
            {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "OLHServCaseDefBL";
              tError.functionName = "submitData";
              tError.errorMessage = "该记录已存在!";

              this.mErrors.addOneError(tError);
              return false;
            }


    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
      System.out.println("Start OLHServCaseDefBL Submit...");
      PubSubmit tPubSubmit = new PubSubmit();
           if (!tPubSubmit.submitData(mInputData, mOperate))
             {
               // @@错误处理
               this.mErrors.copyAllErrors(tPubSubmit.mErrors);
               CError tError = new CError();
               tError.moduleName = "OLHServCaseDefBL";
               tError.functionName = "submitData";
               tError.errorMessage = "数据提交失败!";

               this.mErrors.addOneError(tError);
               return false;
             }
             System.out.println("END OLHServCaseDefBL Submit...");
    }
    mInputData=null;
    return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
    if (this.mOperate.equals("INSERT||MAIN"))
   {
       mLHServCaseDefSchema.setOperator(mGlobalInput.Operator);
       System.out.println("---------Insert--------"+mGlobalInput.Operator);
      // mLHServCaseDefSchema.setManageCom(mGlobalInput.ManageCom);
       mLHServCaseDefSchema.setMakeDate(PubFun.getCurrentDate());
       mLHServCaseDefSchema.setMakeTime(PubFun.getCurrentTime());
       mLHServCaseDefSchema.setModifyDate(PubFun.getCurrentDate());
       mLHServCaseDefSchema.setModifyTime(PubFun.getCurrentTime());

       map.put(mLHServCaseDefSchema, "INSERT");
   }
   if (this.mOperate.equals("UPDATE||MAIN"))
   {
    System.out.println("---------------In Update---------------");
     //mLHServCaseDefSchema.setManageCom(mGlobalInput.ManageCom);
     mLHServCaseDefSchema.setModifyDate(PubFun.getCurrentDate());
     mLHServCaseDefSchema.setModifyTime(PubFun.getCurrentTime());
     map.put(mLHServCaseDefSchema, "UPDATE");
   }
   if (this.mOperate.equals("DELETE||MAIN"))
   {
     map.put(mLHServCaseDefSchema, "DELETE");
     map.put("delete from LHSERVCASERELA WHERE servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"'","DELETE");
     map.put("delete from LHCASETASKRELA WHERE servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"'","DELETE");
     map.put("delete from LHTASKCUSTOMERRELA WHERE servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"'","DELETE");
     map.put("delete from LHEvaReport WHERE servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"'","DELETE");
     map.put("delete from LHExecFeeBalance WHERE servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"'","DELETE");
     map.put("delete from LHFeeCharge WHERE servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"'","DELETE");
     map.put("delete from LHHmServBeManage WHERE servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"'","DELETE");
     map.put("delete from LHMessSendMg WHERE servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"'","DELETE");
     map.put("delete from LHQuesImportMain WHERE servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"'","DELETE");
     map.put("delete from LHQuesImportDetail WHERE cusregistno in ( select distinct cusregistno from LHQuesImportmain where servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"')","DELETE");
     map.put("delete from LHEvalueExpMain WHERE servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"'","DELETE");
     map.put("delete from LHEvalueExpDetail WHERE cusevalcode in ( select distinct cusevalcode from LHEvalueExpmain where servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"')","DELETE");
   }
   if (this.mOperate.equals("DELRELA||MAIN"))
   {
       System.out.println("<<<<<<<<<<<<<<<<getInputData---mGlobalInput is null"+this.mOperate);
       this.mLHServCaseRelaSet.set((LHServCaseRelaSet)mInputData.getObjectByObjectName("LHServCaseRelaSet",0));
       if (mLHServCaseRelaSet == null )
       {// @@错误处理
           CError tError = new CError();
           tError.moduleName = "OLHServCaseDefBL";
           tError.functionName = "dealData";
           tError.errorMessage = "LHServCaseRelaSet没有得到足够的数据，请您确认!";
           this.mErrors.addOneError(tError);
           return false;
       }
       System.out.println("<<<<<<<<<<<<<<<<  mLHServCaseRelaSet.size() = "+mLHServCaseRelaSet.size());
       for(int i = 1; i <= mLHServCaseRelaSet.size(); i++)
       {
           map.put("delete from LHSERVCASERELA WHERE servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"' and servitemno = '"+mLHServCaseRelaSet.get(i).getServItemNo()+"'","DELETE");
//           map.put("delete from LHCASETASKRELA WHERE servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"'","DELETE");
           map.put("delete from LHTASKCUSTOMERRELA WHERE servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"' and servitemno = '"+mLHServCaseRelaSet.get(i).getServItemNo()+"'","DELETE");
           map.put("delete from LHEvaReport WHERE servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"' and servitemno = '"+mLHServCaseRelaSet.get(i).getServItemNo()+"'","DELETE");
           map.put("delete from LHExecFeeBalance WHERE servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"' and servitemno = '"+mLHServCaseRelaSet.get(i).getServItemNo()+"'","DELETE");
           map.put("delete from LHFeeCharge WHERE servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"' and servitemno = '"+mLHServCaseRelaSet.get(i).getServItemNo()+"'","DELETE");
           map.put("delete from LHHmServBeManage WHERE servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"' and servitemno = '"+mLHServCaseRelaSet.get(i).getServItemNo()+"'","DELETE");
           map.put("delete from LHMessSendMg WHERE servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"' and servitemno = '"+mLHServCaseRelaSet.get(i).getServItemNo()+"'","DELETE");
           map.put("delete from LHQuesImportMain WHERE servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"' and servitemno = '"+mLHServCaseRelaSet.get(i).getServItemNo()+"'","DELETE");
           map.put("delete from LHQuesImportDetail WHERE cusregistno in ( select distinct cusregistno from LHQuesImportmain where servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"' and servitemno = '"+mLHServCaseRelaSet.get(i).getServItemNo()+"')","DELETE");
           map.put("delete from LHEvalueExpMain WHERE servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"' and servitemno = '"+mLHServCaseRelaSet.get(i).getServItemNo()+"'","DELETE");
           map.put("delete from LHEvalueExpDetail WHERE cusevalcode in ( select distinct cusevalcode from LHEvalueExpmain where servcasecode = '"+mLHServCaseDefSchema.getServCaseCode()+"' and servitemno = '"+mLHServCaseRelaSet.get(i).getServItemNo()+"')","DELETE");
       }
   }
    return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
    this.mLHServCaseDefSchema.setSchema((LHServCaseDefSchema)cInputData.getObjectByObjectName("LHServCaseDefSchema",0));
    this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    if (mGlobalInput == null || mLHServCaseDefSchema == null )
    {// @@错误处理
        System.out.println("<<<<<<<<<<<<<<<<getInputData---mGlobalInput is null");
        CError tError = new CError();
        tError.moduleName = "OLHServCaseDefBL";
        tError.functionName = "getInputData";
        tError.errorMessage = "没有得到足够的数据，请您确认!";
        this.mErrors.addOneError(tError);
        return false;
    }

    return true;
}

private boolean submitquery()
{
    this.mResult.clear();
    LHServCaseDefDB tLHServCaseDefDB=new LHServCaseDefDB();
    tLHServCaseDefDB.setSchema(this.mLHServCaseDefSchema);
    //如果有需要处理的错误，则返回
    if (tLHServCaseDefDB.mErrors.needDealError())
    {// @@错误处理
        this.mErrors.copyAllErrors(tLHServCaseDefDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "LHServCaseDefBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据提交失败!";
        this.mErrors .addOneError(tError) ;
        return false;
    }
    mInputData=null;
    return true;
}
 private boolean prepareOutputData()
 {
   try
   {
       this.mInputData.clear();
       this.mInputData.add(this.mLHServCaseDefSchema);
       mInputData.add(map);
       mResult.clear();
       mResult.add(this.mLHServCaseDefSchema);
   }
   catch(Exception ex)
   {// @@错误处理
       CError tError =new CError();
       tError.moduleName="LHServCaseDefBL";
       tError.functionName="prepareData";
       tError.errorMessage="在准备往后层处理所需要的数据时出错。";
       this.mErrors .addOneError(tError) ;
       return false;
   }
   return true;
 }
 public VData getResult()
 {
     return this.mResult;
 }
}


