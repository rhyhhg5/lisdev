/*
 * <p>ClassName: OLHCaseLaunchBL </p>
 * <p>Description: OLHCaseLaunchBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: SinoSoft </p>
 * @Database:
 * @CreateDate：2006-08-15 11:16:51
 */
package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLHCaseLaunchBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LHServTaskRelaSet mLHServTaskRelaSet = new LHServTaskRelaSet();
    public OLHCaseLaunchBL(){}

    public static void main(String[] args) {}

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLHCaseLaunchBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLHCaseLaunchBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start OLHCaseLaunchBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("Start OLHCaseLaunchBL Submit...");
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLHCaseLaunchBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if (this.mOperate.equals("INSERT||MAIN"))
        {System.out.println(mLHServTaskRelaSet.size());
            if (mLHServTaskRelaSet != null && mLHServTaskRelaSet.size() > 0)
            {
                System.out.println(mLHServTaskRelaSet.size());
                for (int i = 1; i <= mLHServTaskRelaSet.size(); i++)
                {
                    mLHServTaskRelaSet.get(i).setSerialNo(PubFun1.CreateMaxNo("LHSERVTASKRELA", 10));
                    mLHServTaskRelaSet.get(i).setOperator(mGlobalInput.Operator);
                    mLHServTaskRelaSet.get(i).setManageCom(mGlobalInput.ManageCom);
                    mLHServTaskRelaSet.get(i).setMakeDate(PubFun.getCurrentDate());
                    mLHServTaskRelaSet.get(i).setMakeTime(PubFun.getCurrentTime());
                    mLHServTaskRelaSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLHServTaskRelaSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
                map.put(mLHServTaskRelaSet, "INSERT");
            }
        }

        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            if (mLHServTaskRelaSet != null && mLHServTaskRelaSet.size() > 0)
            {
//                System.out.println(mLHServTaskRelaSet.size());
                for (int i = 1; i <= mLHServTaskRelaSet.size(); i++)
                {
                    if(mLHServTaskRelaSet.get(i).getSerialNo() == null
                       || mLHServTaskRelaSet.get(i).getSerialNo().equals(""))
                    {
                        mLHServTaskRelaSet.get(i).setSerialNo(PubFun1.CreateMaxNo("LHSERVTASKRELA", 10));
//                        System.out.println(mLHServTaskRelaSet.get(i).getSerialNo());
                        mLHServTaskRelaSet.get(i).setMakeDate(PubFun.getCurrentDate());
                        mLHServTaskRelaSet.get(i).setMakeTime(PubFun.getCurrentTime());
                    }
                    mLHServTaskRelaSet.get(i).setOperator(mGlobalInput.Operator);
                    mLHServTaskRelaSet.get(i).setManageCom(mGlobalInput.ManageCom);
                    mLHServTaskRelaSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLHServTaskRelaSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
                map.put("delete from LHServTaskRela where ServCaseType = '"+mLHServTaskRelaSet.get(1).getServCaseType()
                        +"' and ComID = '"+mLHServTaskRelaSet.get(1).getComID()
                        +"' and ServItemCode = '"+mLHServTaskRelaSet.get(1).getServItemCode()+"' ","DELETE");
                map.put(mLHServTaskRelaSet, "INSERT");
            }
        }

        if (this.mOperate.equals("DELETE||MAIN"))
        {
            map.put(mLHServTaskRelaSet, "DELETE");
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData()
    {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLHServTaskRelaSet.set((LHServTaskRelaSet) cInputData.
                           getObjectByObjectName("LHServTaskRelaSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        LHServTaskRelaDB tLHServTaskRelaDB = new LHServTaskRelaDB();
        //如果有需要处理的错误，则返回
        if (tLHServTaskRelaDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHServTaskRelaDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "OLHCaseLaunchBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            this.mInputData.add(this.mLHServTaskRelaSet);
            this.mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLHServTaskRelaSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLHCaseLaunchBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
