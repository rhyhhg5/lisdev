/*
 * <p>ClassName: OLHCustomHealthStatusBLS </p>
 * <p>Description: OLHCustomHealthStatusBLS类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-18 10:11:20
 */
package com.sinosoft.lis.health;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;
 public class OLHCustomHealthStatusBLS {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
//传输数据类
  private VData mInputData ;
 /** 数据操作字符串 */
private String mOperate;
public OLHCustomHealthStatusBLS() {
	}
public static void main(String[] args) {
}
 /**
 传输数据的公共方法
*/
public boolean submitData(VData cInputData,String cOperate)
{
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
	  mInputData=(VData)cInputData.clone();
    if(this.mOperate.equals("INSERT||MAIN"))
    {if (!saveLHCustomHealthStatus())
        return false;
    }
    if (this.mOperate.equals("DELETE||MAIN"))
    {if (!deleteLHCustomHealthStatus())
        return false;
    }
    if (this.mOperate.equals("UPDATE||MAIN"))
    {if (!updateLHCustomHealthStatus())
        return false;
    }
  return true;
}
 /**
* 保存函数
*/
private boolean saveLHCustomHealthStatus()
{
  LHCustomHealthStatusSchema tLHCustomHealthStatusSchema = new LHCustomHealthStatusSchema();
  tLHCustomHealthStatusSchema = (LHCustomHealthStatusSchema)mInputData.getObjectByObjectName("LHCustomHealthStatusSchema",0);
  Connection conn;
  conn=null;
  conn=DBConnPool.getConnection();
  if (conn==null)
  {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "OLHCustomHealthStatusBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据库连接失败!";
      this.mErrors .addOneError(tError) ;
      return false;
   }
	try{
 	  conn.setAutoCommit(false);
    LHCustomHealthStatusDB tLHCustomHealthStatusDB=new LHCustomHealthStatusDB(conn);
    tLHCustomHealthStatusDB.setSchema(tLHCustomHealthStatusSchema);
    if (!tLHCustomHealthStatusDB.insert())
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLHCustomHealthStatusDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "OLHCustomHealthStatusBLS";
      tError.functionName = "saveData";
      tError.errorMessage = "数据保存失败!";
      this.mErrors .addOneError(tError) ;
      conn.rollback();
      conn.close();
      return false;
    }
    conn.commit() ;
    conn.close();
  }
  catch (Exception ex)
  {
    // @@错误处理
    CError tError =new CError();
    tError.moduleName="OLHCustomHealthStatusBLS";
    tError.functionName="submitData";
    tError.errorMessage=ex.toString();
    this.mErrors .addOneError(tError);
      try{
      conn.rollback() ;
      conn.close();
      }
      catch(Exception e){}
    return false;
	}
  return true;
}
    /**
    * 保存函数
    */
    private boolean deleteLHCustomHealthStatus()
    {
        LHCustomHealthStatusSchema tLHCustomHealthStatusSchema = new LHCustomHealthStatusSchema();
        tLHCustomHealthStatusSchema = (LHCustomHealthStatusSchema)mInputData.getObjectByObjectName("LHCustomHealthStatusSchema",0);
        System.out.println("Start Save...");
        Connection conn;
        conn=null;
        conn=DBConnPool.getConnection();
        if (conn==null)
        {
		// @@错误处理
		CError tError = new CError();
           tError.moduleName = "OLHCustomHealthStatusBLS";
           tError.functionName = "saveData";
           tError.errorMessage = "数据库连接失败!";
           this.mErrors .addOneError(tError) ;
           return false;
        }
        try{
           conn.setAutoCommit(false);
           System.out.println("Start 保存...");
           LHCustomHealthStatusDB tLHCustomHealthStatusDB=new LHCustomHealthStatusDB(conn);
           tLHCustomHealthStatusDB.setSchema(tLHCustomHealthStatusSchema);
           if (!tLHCustomHealthStatusDB.delete())
           {
		// @@错误处理
		    this.mErrors.copyAllErrors(tLHCustomHealthStatusDB.mErrors);
 		    CError tError = new CError();
		    tError.moduleName = "OLHCustomHealthStatusBLS";
		    tError.functionName = "saveData";
		    tError.errorMessage = "数据删除失败!";
		    this.mErrors .addOneError(tError) ;
               conn.rollback();
               conn.close();
               return false;
           }
               conn.commit() ;
               conn.close();
         }
       catch (Exception ex)
       {
      // @@错误处理
          CError tError =new CError();
          tError.moduleName="OLHCustomHealthStatusBLS";
          tError.functionName="submitData";
          tError.errorMessage=ex.toString();
          this.mErrors .addOneError(tError);
          try{conn.rollback() ;
          conn.close();} catch(Exception e){}
         return false;
         }
         return true;
}
/**
  * 保存函数
*/
private boolean updateLHCustomHealthStatus()
{
     LHCustomHealthStatusSchema tLHCustomHealthStatusSchema = new LHCustomHealthStatusSchema();
     tLHCustomHealthStatusSchema = (LHCustomHealthStatusSchema)mInputData.getObjectByObjectName("LHCustomHealthStatusSchema",0);
     System.out.println("Start Save...");
     Connection conn;
     conn=null;
     conn=DBConnPool.getConnection();
     if (conn==null)
     {
	     CError tError = new CError();
        tError.moduleName = "OLHCustomHealthStatusBLS";
        tError.functionName = "updateData";
        tError.errorMessage = "数据库连接失败!";
        this.mErrors .addOneError(tError) ;
        return false;
     }
     try{
           conn.setAutoCommit(false);
           System.out.println("Start 保存...");
           LHCustomHealthStatusDB tLHCustomHealthStatusDB=new LHCustomHealthStatusDB(conn);
	tLHCustomHealthStatusDB.setSchema(tLHCustomHealthStatusSchema);
           if (!tLHCustomHealthStatusDB.update())
           {
	          // @@错误处理
	         this.mErrors.copyAllErrors(tLHCustomHealthStatusDB.mErrors);
	         CError tError = new CError();
	         tError.moduleName = "OLHCustomHealthStatusBLS";
	         tError.functionName = "saveData";
            tError.errorMessage = "数据保存失败!";
            this.mErrors .addOneError(tError) ;
            conn.rollback();
            conn.close();
            return false;
            }
            conn.commit() ;
            conn.close();
       }
       catch (Exception ex)
       {
       // @@错误处理
               CError tError =new CError();
               tError.moduleName="OLHCustomHealthStatusBLS";
               tError.functionName="submitData";
               tError.errorMessage=ex.toString();
               this.mErrors .addOneError(tError);
               try{conn.rollback() ;
               conn.close();} catch(Exception e){}
               return false;
     }
               return true;
     }
}
