package com.sinosoft.lis.health;

import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;

/**
 * 医师情检索统计
 * <p>Title:LDDoctorSearchBL </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author liuli
 * @version 1.0
 */
public class LDDoctorSearchBL {
    public LDDoctorSearchBL() {
    }

    private VData mResult = new VData();
    private CErrors mErrors = new CErrors();
    private LDDoctorSchema mLDDoctorSchema = new LDDoctorSchema();
    private GlobalInput mGlobalInput = new GlobalInput();
    private String mflag;
    public boolean submitData(VData cInputData, String cOperate) {
        System.out.println("============LDDoctorSearchBL  start==========");
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        if (!dealData(cInputData)) {
            return false;
        }
        // 保存数据
        return true;
    }

    public VData getResult() {
        return mResult;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput",
                0);
        LDDoctorSchema tLDDoctorSchema = new LDDoctorSchema();
        tLDDoctorSchema = (LDDoctorSchema) cInputData.getObjectByObjectName(
                "LDDoctorSchema", 0);
        mflag = tLDDoctorSchema.getDoctNo();
        if (mflag == null || mflag.equals("")) {
            buildError("getInputData", "没有获取到所需要的数据");
            return false;
        }
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData(VData cInputData) {
        mLDDoctorSchema = (LDDoctorSchema) cInputData.getObjectByObjectName(
                "LDDoctorSchema", 0);
        if (mflag.equals("State")) {
            doctorState(mLDDoctorSchema); //统计
        } else if (mflag.equals("SERACH")) {
            doctorSearch(mLDDoctorSchema); //检索
        }
        return true;
    }

    /**
     * doctorState医师信息统计
     *
     * @return boolean
     */
    private boolean doctorState(LDDoctorSchema tLDDoctorSchema) {
        TextTag texttag = new TextTag();
        String MakeDate1 = tLDDoctorSchema.getMakeDate();
        String MakeDate2 = tLDDoctorSchema.getModifyDate();
        String MakeDate = "";
        if (MakeDate1 != null && MakeDate2 != null) {
            MakeDate = " and MakeDate between '" + MakeDate1 + "' and '" +
                       MakeDate2 + "'";
            texttag.add("MakeDate1", MakeDate1);
            texttag.add("MakeDate2", MakeDate2);
        } else {
            MakeDate2 = new PubFun().getCurrentDate();
            texttag.add("MakeDate2", MakeDate2);
        }
        String sql =
                "select (select name from ldcom a where a.comcode=b.managecom) Managecom,"
                +
                "(select count(*) from lddoctor a where EngageClass='0' and  a.managecom=b.managecom) a,"
                +
                "(select count(*) from lddoctor a where EngageClass='1' and  a.managecom=b.managecom) b,"
                +
                "(select count(*) from lddoctor a where TechPostCode in ('101','102','201','202','301','302','401','402') and a.managecom=b.managecom) 高级职称,"
                +
                "(select count(*) from lddoctor a where TechPostCode in ('103','203','303','403') and a.managecom=b.managecom) 中级职称,"
                +
                "(select count(*) from lddoctor a where TechPostCode in ('104','204','304','404') and a.managecom=b.managecom) 初级职称,"
                +
                "(select count(*) from lddoctor a where SecName in('03','19') and a.managecom=b.managecom) as 内科,"
                +
                "(select count(*) from lddoctor a where SecName in('04') and a.managecom=b.managecom) as 外科,"
                +
                "(select count(*) from lddoctor a where SecName in('05','06') and a.managecom=b.managecom) as 妇科,"
                +
                "(select count(*) from lddoctor a where SecName in('07','08','09') and a.managecom=b.managecom) as 儿科,"
                +
                "(select count(*) from lddoctor a where SecName in('10','11','12') and a.managecom=b.managecom) as 五官科,"
                +
                "(select count(*) from lddoctor a where SecName in('15') and a.managecom=b.managecom) as 精神科,"
                +
                "(select count(*) from lddoctor a where SecName in('02') and a.managecom=b.managecom) as 全科医疗,"
                +
                "(select count(*) from lddoctor a where SecName in('01','21','22') and a.managecom=b.managecom) as 预防保健,"
                +
                "(select count(*) from lddoctor a where SecName in('50','52') and a.managecom=b.managecom) as 中医,"
                +
                "(select count(*) from lddoctor a where SecName in('26','30','31','32') and a.managecom=b.managecom) as 医学检验等辅助科室,"
                +
                "(select count(*) from lddoctor a where SecName in('13','14','16','17','18','20','23','24','25','51','61','79','99') and a.managecom=b.managecom) as 其它,"
                + "count(*) as 合计  "
                + "from LDdoctor b where 1=1"
                + MakeDate
                + " group by ManageCom with ur";
          //  System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        ListTable tListTable = new ListTable();
        tListTable.setName("table1");
        if (tSSRS != null) {
            for (int m = 1; m < tSSRS.getMaxRow(); m++) {
                String[] value = new String[20];
                for (int i = 0; i < value.length; i++) {
                    value[i] = "";
                }
                for (int n = 1; n < value.length; n++) {
                    value[n - 1] = tSSRS.GetText(m, n);
                }
                tListTable.add(value);
            }
        }
        String sqlb = "select (select count(*) from lddoctor a where EngageClass='0'"+MakeDate+") a,"
                +
                "(select count(*) from lddoctor a where EngageClass='1'"+MakeDate+") b,"
                +
                "(select count(*) from lddoctor a where TechPostCode in ('101','102','201','202','301','302','401','402')"+MakeDate+") 高级职称,"
                +
                "(select count(*) from lddoctor a where TechPostCode in ('103','203','303','403')"+MakeDate+") 中级职称,"
                +
                "(select count(*) from lddoctor a where TechPostCode in ('104','204','304','404')"+MakeDate+") 初级职称,"
                +
                "(select count(*) from lddoctor a where SecName in('03','19')"+MakeDate+") as 内科,"
                +
                "(select count(*) from lddoctor a where SecName in('04')"+MakeDate+") as 外科,"
                +
                "(select count(*) from lddoctor a where SecName in('05','06')"+MakeDate+") as 妇科,"
                +
                "(select count(*) from lddoctor a where SecName in('07','08','09')"+MakeDate+") as 儿科,"
                +
                "(select count(*) from lddoctor a where SecName in('10','11','12')"+MakeDate+") as 五官科,"
                +
                "(select count(*) from lddoctor a where SecName in('15')"+MakeDate+") as 精神科,"
                +
                "(select count(*) from lddoctor a where SecName in('02')"+MakeDate+") as 全科医疗,"
                +
                "(select count(*) from lddoctor a where SecName in('01','21','22')"+MakeDate+") as 预防保健,"
                +
                "(select count(*) from lddoctor a where SecName in('50','52')"+MakeDate+") as 中医,"
                +
                "(select count(*) from lddoctor a where SecName in('26','30','31','32')"+MakeDate+") as 医学检验等辅助科室,"
                +
                "(select count(*) from lddoctor a where SecName in('13','14','16','17','18','20','23','24','25','51','61','79','99')"+MakeDate+") as 其它,"
                + "count(*) as 合计  "
                + "from LDdoctor b where 1=1"
                + MakeDate
                + " with ur";

       //System.out.println(sqlb);
       SSRS tSSRS2 = new ExeSQL().execSQL(sqlb);
       if (tSSRS2 != null) {
               String[] value = new String[20];
               for (int i = 0; i < value.length; i++) {
                   value[i] = "";
               }
               value[0] = "合计";
               for (int n = 1; n <= tSSRS2.getMaxCol(); n++) {
                   value[n] = tSSRS2.GetText(1, n);
               }
               tListTable.add(value);
       }


        texttag.add("OPERATER", mGlobalInput.GetServerIP());

        XmlExport xmlexport = new XmlExport();
        xmlexport.createDocument("DoctorInfostat.vts", "printer"); //全系统
        xmlexport.addListTable(tListTable, new String[20]);
        xmlexport.addTextTag(texttag);
        //保存信息
        xmlexport.outputDocumentToFile("e:\\", "testHZM-Doctorstat"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    /**
     * doctorSearch医师信息检索
     */
    private boolean doctorSearch(LDDoctorSchema tLDDoctorSchema) {

        String ManageCom = tLDDoctorSchema.getManageCom(); //管理机构
        String WorkPlace = tLDDoctorSchema.getWorkPlace(); //地区名称
        String HospitCode = tLDDoctorSchema.getHospitCode(); //所属机构,医院号码
        String EngageClass = tLDDoctorSchema.getEngageClass(); //聘用方式
        String SecName = tLDDoctorSchema.getSecName(); //科室专业
        String CarreerClass = tLDDoctorSchema.getCarreerClass(); //职业类型
        String TechPostCode = tLDDoctorSchema.getTechPostCode(); //职称
        String ExpertFlag = tLDDoctorSchema.getExpertFlag(); //专家标识
        String EduLevelCode = tLDDoctorSchema.getEduLevelCode(); //学历
        String DoctName = tLDDoctorSchema.getDoctName(); //姓名
        String BirthYear = tLDDoctorSchema.getBirthYear(); //出生年
        String Sex = tLDDoctorSchema.getSex(); //性别

        if (ManageCom != null && !ManageCom.equals("")) {
            if (ManageCom.equals("86000000") || ManageCom.equals("86")) {
                ManageCom = "";
            } else {
                ManageCom = ManageCom.substring(0,4);
                ManageCom = " and ManageCom like '" + ManageCom + "%'";
            }
        } else {
            ManageCom = "";
        }
        if (WorkPlace != null && !WorkPlace.equals("")) {
            WorkPlace = " and WorkPlace = '" + WorkPlace + "'";
        } else {
            WorkPlace = "";
        }
        if (HospitCode != null && !HospitCode.equals("")) {
            HospitCode = " and HospitCode = '" + HospitCode + "'";
        } else {
            HospitCode = "";
        }

        if (EngageClass != null && !EngageClass.equals("")) {
            EngageClass = " and EngageClass = '" + EngageClass + "'";
        } else {
            EngageClass = "";
        }

        if (SecName != null && !SecName.equals("")) {
            SecName = " and SecName = '" + SecName + "'";
        } else {
            SecName = "";
        }

        if (CarreerClass != null && !CarreerClass.equals("")) {
            CarreerClass = " and CarreerClass = '" + CarreerClass + "'";
        } else {
            CarreerClass = "";
        }

        if (TechPostCode != null && !TechPostCode.equals("")) {
            TechPostCode = " and TechPostCode = '" + TechPostCode + "'";
        } else {
            TechPostCode = "";
        }

        if (ExpertFlag != null && !ExpertFlag.equals("")) {
            ExpertFlag = " and ExpertFlag = '" + ExpertFlag + "'";
        } else {
            ExpertFlag = "";
        }

        if (EduLevelCode != null && !EduLevelCode.equals("")) {
            EduLevelCode = " and EduLevelCode = '" + EduLevelCode + "'";
        } else {
            EduLevelCode = "";
        }

        if (DoctName != null && !DoctName.equals("")) {
            DoctName = " and DoctName like '" + DoctName + "%'";
        } else {
            DoctName = "";
        }

        if (BirthYear != null && !BirthYear.equals("")) {
            BirthYear = " and Birthday = '" + BirthYear + "'";
        } else {
            BirthYear = "";
        }

        if (Sex != null && !Sex.equals("")) {
            Sex = " and Sex = '" + Sex + "'";
        } else {
            Sex = "";
        }
        String sql = "select int(Row_Number() over()),Doctname,(select Name from ldcom where comcode=l.ManageCom ) as ManageCom,"
                     + "(select  CodeName from LDCode where codetype = 'hmareacode' and code=l.WorkPlace) as WorkPlace,"
                     + "(select CodeName from ldcode where  codetype = 'engageclasscode' and code=l.EngageClass) EngageClass,"
                     + "(select HospitName from LDHospital where HospitCode = l.Hospitcode ) Hospitcode,"
                     +
                     "case when birthyear is not null and birthyear !='' then "
                     +
                     "to_char(year(current date)-to_number(birthyear)) else '' end as  birthyear,"
                     +
                     " case when l.sex='1' then '女' when l.sex='0' then '男' else '' end as sex,"
                     + "(select CodeName from ldcode where codetype = 'hmedulevelcode' and code = l.edulevelcode) edulevelcode,"
                     + "(select CodeName from ldcode where codetype = 'carreertype' and code=l.CarreerClass) CarreerClass,"
                     + "(select Codename from LDCode where codetype ='TechPostCode' and code = l.TechPostCode) TechPostCode,"
                     + "(select CodeName from ldcode where codetype = 'hmsecname' and code=l.SecName) SecName,"
                     + "case when ExpertFlag='0' then '否' when ExpertFlag='1' then '是' else '' end as ExpertFlag"
                     + " from lddoctor l where 1=1"
                     + ManageCom
                     + WorkPlace
                     + HospitCode
                     + EngageClass
                     + SecName
                     + CarreerClass
                     + TechPostCode
                     + ExpertFlag
                     + EduLevelCode
                     + DoctName
                     + BirthYear
                     + Sex
                     + " with ur"
                     ;
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        ListTable tListTable = new ListTable();
        tListTable.setName("table1");
        if (tSSRS != null) {
            for (int m = 1; m < tSSRS.getMaxRow(); m++) {
                String[] value = new String[15];
                for (int i = 0; i < value.length; i++) {
                    value[i] = "";
                }
                for (int n = 1; n < value.length; n++) {
                    value[n - 1] = tSSRS.GetText(m, n);
                }
                tListTable.add(value);
            }
        }

        TextTag texttag = new TextTag();
        texttag.add("OPERATER", mGlobalInput.GetServerIP());
        XmlExport xmlexport = new XmlExport();
        xmlexport.createDocument("DoctorInfo.vts", "printer"); //全系统
        xmlexport.addListTable(tListTable, new String[15]);
        xmlexport.addTextTag(texttag);
        //保存信息
        xmlexport.outputDocumentToFile("e:\\", "testHZM-DoctorInfo"); //输出xml文档到文件
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "HospitalBasicBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
}
