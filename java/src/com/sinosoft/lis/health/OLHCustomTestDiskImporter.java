package com.sinosoft.lis.health;

import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.diskimport.*;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LHCustomInHospitalSchema;


public class OLHCustomTestDiskImporter
{
    public CErrors mErrrors = new CErrors();

    /** 磁盘导入临时表的schema */
    private static final String schemaClassName =
            "com.sinosoft.lis.schema.LHCustomTestSchema";

    /** 磁盘导入临时表的set */
    private static final String schemaSetClassName =
            "com.sinosoft.lis.vschema.LHCustomTestSet";

    /** 使用默认的导入方式 */
    private DefaultDiskImporter importer = null;

    //**数据操作字符串*/
    private static String mOperate = "INSERT||MAIN";

    //**往后面传输数据的容器*/
    VData mInputData = new VData();
    private MMap map = new MMap();
    /**
     * 构造函数
     * @param fileName String
     * @param configFileName String
     * @param sheetName String
     */
    public OLHCustomTestDiskImporter(String fileName, String configFileName,
            String sheetName)
    {
        importer = new DefaultDiskImporter(fileName, configFileName, sheetName);
    }

    /**
     * 执行导入
     * @return boolean
     */
    public boolean doImport()
    {
        importer.setSchemaClassName(schemaClassName);
        importer.setSchemaSetClassName(schemaSetClassName);
        if (!importer.doImport())
        {
            mErrrors.copyAllErrors(importer.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 得到导入结果
     * @return SchemaSet
     */
    public SchemaSet getSchemaSet()
    {
        return importer.getSchemaSet();
    }

    /**
     * JSP倒入接口
     * @return boolean
     */
    public boolean submitImport()
    {
        String tempCustomerNo = "";
        String tempInHospitalNo = "";
        this.doImport();
        LHCustomTestSet mLHCustomTestSet = (LHCustomTestSet) importer.getSchemaSet();
        LHCustomInHospitalSet mLHCustomInHospitalSet = new LHCustomInHospitalSet();

        for(int i = 1; i <= mLHCustomTestSet.size(); i++)
        {
            if(tempCustomerNo.equals(mLHCustomTestSet.get(i).getCustomerNo())
               && tempInHospitalNo.equals(mLHCustomTestSet.get(i).getInHospitNo()))
            {
                if(!mLHCustomTestSet.get(i).getDoctNo().equals(""))
                {
                    mLHCustomTestSet.get(i).setDoctNo("");
                }
                continue;
            }
            else
            {
                LHCustomInHospitalSchema mLHCustomInHospitalSchema = new LHCustomInHospitalSchema();
                mLHCustomInHospitalSchema.setCustomerNo(mLHCustomTestSet.get(i).getCustomerNo());
                mLHCustomInHospitalSchema.setInHospitNo(mLHCustomTestSet.get(i).getInHospitNo());
                mLHCustomInHospitalSchema.setHospitCode(mLHCustomTestSet.get(i).getDoctNo());
                mLHCustomInHospitalSchema.setInHospitDate(mLHCustomTestSet.get(i).getTestDate());
                mLHCustomInHospitalSchema.setInHospitMode(mLHCustomTestSet.get(i).getTestGrpUsed());
                mLHCustomInHospitalSchema.setOperator(mLHCustomTestSet.get(i).getOperator());
                mLHCustomInHospitalSchema.setMakeDate(mLHCustomTestSet.get(i).getMakeDate());
                mLHCustomInHospitalSchema.setMakeTime(mLHCustomTestSet.get(i).getMakeTime());
                mLHCustomInHospitalSchema.setModifyDate(mLHCustomTestSet.get(i).getModifyDate());
                mLHCustomInHospitalSchema.setModifyTime(mLHCustomTestSet.get(i).getModifyTime());
                mLHCustomInHospitalSchema.setManageCom(mLHCustomTestSet.get(i).getManageCom());
                System.out.println("------------(mLHCustomTestSet.get(i).getCustomerNo():"+mLHCustomTestSet.get(i).getCustomerNo()+"-----------");

                mLHCustomInHospitalSet.add(mLHCustomInHospitalSchema);
                if(!mLHCustomTestSet.get(i).getDoctNo().equals(""))
               {
                   mLHCustomTestSet.get(i).setDoctNo("");
               }

                tempCustomerNo = mLHCustomTestSet.get(i).getCustomerNo();
                tempInHospitalNo = mLHCustomTestSet.get(i).getInHospitNo();
            }
        }

        mInputData.add(mLHCustomTestSet);
        mInputData.add(mLHCustomInHospitalSet);
        map.put(mLHCustomTestSet, "INSERT");
        map.put(mLHCustomInHospitalSet, "INSERT");
        mInputData.add(map);
        System.out.println("----mOLHCustomTestDiskImporter.submitImport-----");
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLHCustomTestBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrrors.addOneError(tError);
                return false;
            }
        System.out.println("----submitImport,success-----");
        return true;
    }

    /**
     * 主函数，调试用
     * @param args String[]
     */
    public static void main(String[] args)
    {
        String fileName = "E:/workspace/ui/temp/LHCustomTest.xls";
        String configFileName = "E:/workspace/ui/temp/LHCustomTest.xml";
        String sheetName = "TEST";
        OLHCustomTestDiskImporter importer = new OLHCustomTestDiskImporter(fileName, configFileName,
                sheetName);
        importer.doImport();
        LHCustomTestSet set = (LHCustomTestSet) importer.getSchemaSet();
        System.out.println(set.toString());
    }
}
