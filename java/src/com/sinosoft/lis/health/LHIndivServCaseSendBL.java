/*
 * <p>ClassName: LHIndivServCaseSendBL </p>
 * <p>Description: LHIndivServCaseSendBL类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-08-30 16:17:38
 */
package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class LHIndivServCaseSendBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
private MMap map= new MMap();

/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
private String tNo;
/** 业务处理相关变量 */
private LHCaseTaskRelaSchema mLHCaseTaskRelaSchema=new LHCaseTaskRelaSchema();
private LHTaskCustomerRelaSet mLHTaskCustomerRelaSet=new LHTaskCustomerRelaSet();
private LHIndiServAcceptSchema mLHIndiServAcceptSchema=new LHIndiServAcceptSchema();
public LHIndivServCaseSendBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
public static void main(String[] args) {
}
/**
* 传输数据的公共方法
        * @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "LHIndivServCaseSendBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败LHIndivServCaseSendBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
      PubSubmit tPubSubmit = new PubSubmit();
      System.out.println("Start LHIndivServCaseSendBL Submit...");
      if (!tPubSubmit.submitData(mInputData, mOperate))
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);

        CError tError = new CError();
        tError.moduleName = "LHIndivServCaseSendBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据提交失败!";

        this.mErrors.addOneError(tError);
        return false;
      }

      System.out.println("End LHIndivServCaseSendBL Submit...");
      //如果有需要处理的错误，则返回
    }
    mInputData=null;
    return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
     System.out.println("aaaaaaaaaaaaaaaaaaa "+this.mOperate);
    if (this.mOperate.equals("INSERT||MAIN"))
    {
                mLHCaseTaskRelaSchema.setServTaskNo(PubFun1.CreateMaxNo(
                             "ServTaskNo", 12));
                mLHCaseTaskRelaSchema.setOperator(mGlobalInput.Operator);
                mLHCaseTaskRelaSchema.setMakeDate(PubFun.getCurrentDate());
                mLHCaseTaskRelaSchema.setMakeTime(PubFun.getCurrentTime());
                mLHCaseTaskRelaSchema.setModifyDate(PubFun.getCurrentDate());
                mLHCaseTaskRelaSchema.setModifyTime(PubFun.getCurrentTime());
                mLHCaseTaskRelaSchema.setManageCom(mGlobalInput.ManageCom);
                map.put(mLHCaseTaskRelaSchema, "INSERT");

                 if (mLHTaskCustomerRelaSet != null && mLHTaskCustomerRelaSet.size() > 0)
                 {

                       for (int j = 1; j <= mLHTaskCustomerRelaSet.size(); j++)
                       {
                           mLHTaskCustomerRelaSet.get(j).setTaskExecNo(PubFun1.CreateMaxNo("TaskExecNo", 12));
                           mLHTaskCustomerRelaSet.get(j).setServTaskNo(mLHCaseTaskRelaSchema.getServTaskNo());
                           mLHTaskCustomerRelaSet.get(j).setOperator(mGlobalInput.Operator);
                           mLHTaskCustomerRelaSet.get(j).setMakeDate(PubFun.getCurrentDate());
                           mLHTaskCustomerRelaSet.get(j).setMakeTime(PubFun.getCurrentTime());
                           mLHTaskCustomerRelaSet.get(j).setModifyDate(PubFun.getCurrentDate());
                           mLHTaskCustomerRelaSet.get(j).setModifyTime(PubFun.getCurrentTime());
                           mLHTaskCustomerRelaSet.get(j).setManageCom(mGlobalInput.ManageCom);
                           System.out.println(j);
                       }
                        map.put(mLHTaskCustomerRelaSet, "INSERT");
                }

               map.put(" update LHIndiServAccept a set  a.ServTaskCode='"+mLHCaseTaskRelaSchema.getServTaskNo()+"',"
                        +" a.ManageCom='"+mGlobalInput.ManageCom+"', a.Operator='"+mGlobalInput.Operator+"', "
                        +" a.ModifyDate='"+PubFun.getCurrentDate()+"',a.ModifyTime='"+PubFun.getCurrentTime()  +"' "
                        +" where  ServAccepNo ='"+mLHIndiServAcceptSchema.getServAccepNo()+"'","UPDATE");//将个人服务受理的服务任务换为生成的服务任务编号
    }

        if (this.mOperate.equals("UPDATE||MAIN")) {

                mLHCaseTaskRelaSchema.setOperator(mGlobalInput.Operator);
                mLHCaseTaskRelaSchema.setMakeDate(PubFun.getCurrentDate());
                mLHCaseTaskRelaSchema.setMakeTime(PubFun.getCurrentTime());
                mLHCaseTaskRelaSchema.setModifyDate(PubFun.getCurrentDate());
                mLHCaseTaskRelaSchema.setModifyTime(PubFun.getCurrentTime());
                mLHCaseTaskRelaSchema.setManageCom(mGlobalInput.ManageCom);
                map.put(mLHCaseTaskRelaSchema, "UPDATE");
        }

        if (this.mOperate.equals("DELETE||MAIN")) {
            map.put(" delete from  LHCaseTaskRela where ServTaskNo='" +
                   mLHCaseTaskRelaSchema.getServTaskNo()+ "'", "DELETE");
        }

        return true;


}
/**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
 */
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
         this.mLHCaseTaskRelaSchema.setSchema((LHCaseTaskRelaSchema)cInputData.getObjectByObjectName("LHCaseTaskRelaSchema",0));
         this.mLHTaskCustomerRelaSet.set((LHTaskCustomerRelaSet)cInputData.getObjectByObjectName("LHTaskCustomerRelaSet",0));
          this.mLHIndiServAcceptSchema.setSchema((LHIndiServAcceptSchema)cInputData.getObjectByObjectName("LHIndiServAcceptSchema",0));
         this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));


         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
    this.mResult.clear();
    LHCaseTaskRelaDB tLHCaseTaskRelaDB=new LHCaseTaskRelaDB();
//    tLHCaseTaskRelaDB.set(this.mLHCaseTaskRelaSet);
                //如果有需要处理的错误，则返回
                if (tLHCaseTaskRelaDB.mErrors.needDealError())
                {
                  // @@错误处理
                        this.mErrors.copyAllErrors(tLHCaseTaskRelaDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "LHIndivServCaseSendBL";
                        tError.functionName = "submitData";
                        tError.errorMessage = "数据提交失败!";
                        this.mErrors .addOneError(tError) ;
                        return false;
    }
    mInputData=null;
    return true;
}
 private boolean prepareOutputData()
 {
   try
        {
                this.mInputData.clear();
                this.mInputData.add(this.mLHCaseTaskRelaSchema);
                this.mInputData.add(this.mLHTaskCustomerRelaSet);
                this.mInputData.add(this.mLHIndiServAcceptSchema);
                mInputData.add(map);
                mResult.clear();
                mResult.add(this.mLHCaseTaskRelaSchema);//mLHTaskCustomerRelaSchema
                mResult.add(this.mLHTaskCustomerRelaSet);
                mResult.add(this.mLHIndiServAcceptSchema);
        }
        catch(Exception ex)
        {
                // @@错误处理
                CError tError =new CError();
                tError.moduleName="LHIndivServCaseSendBL";
                tError.functionName="prepareData";
                tError.errorMessage="在准备往后层处理所需要的数据时出错。";
                this.mErrors .addOneError(tError) ;
                return false;
        }
        return true;
        }
        public VData getResult()
        {
        return this.mResult;
        }

    private void jbInit() throws Exception {
    }
}
