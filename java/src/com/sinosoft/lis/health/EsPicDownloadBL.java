package com.sinosoft.lis.health;

import java.util.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LDSysVarSchema;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.schema.LCScanDownloadSchema;
import com.sinosoft.lis.vdb.LCScanDownloadDBSet;
import com.sinosoft.lis.easyscan.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class EsPicDownloadBL
{
    public EsPicDownloadBL() {}

    /** 往后面传输数据的容器 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private ES_DOC_MAINSet mES_DOC_MAINSet = new ES_DOC_MAINSet();
    private ES_DOC_MAINSet tES_DOC_MAINSet = new ES_DOC_MAINSet();
    private GlobalInput mGlobalInput = new GlobalInput();
    List mFilePaths = new ArrayList();
    List mFileNames = new ArrayList();

    /** 数据操作字符串 */
    private String mOperate;

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("DOWNLOAD") && !cOperate.equals("REDOWNLOAD"))
            {
                buildError("submitData", "不支持的操作字符串");
                return false;
            }

            //全局变量赋值
            mOperate = cOperate;
            if (!getInputData(cInputData))
            {
                return false;
            }
            if (mOperate.equals("DOWNLOAD"))
            {
                if (!Downloadpic())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            if (mOperate.equals("REDOWNLOAD"))
            {
                if (!ReDownloadpic())
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
            else
            {
                return true;
            }
        } catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("submit", ex.getMessage());
            return false;
        }
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "EsPicDownloadBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean getInputData(VData cInputData) {
        tES_DOC_MAINSet.set((ES_DOC_MAINSet) cInputData.getObjectByObjectName("ES_DOC_MAINSet", 0));
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if (mGlobalInput == null)
        {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        if (tES_DOC_MAINSet.size() > 0)
        {
            for (int i = 1; i <= tES_DOC_MAINSet.size(); i++)
            {
                ES_DOC_MAINDB tES_DOC_MAINDB = new ES_DOC_MAINDB();
                ES_DOC_MAINSet cES_DOC_MAINSet = new ES_DOC_MAINSet();
                tES_DOC_MAINDB.setDocID(tES_DOC_MAINSet.get(i).getDocID());
                cES_DOC_MAINSet = tES_DOC_MAINDB.query();
                mES_DOC_MAINSet.add(cES_DOC_MAINSet.get(1).getSchema());
            }

            return true;
        } else
        {
            return false;
        }
    }

    private boolean Downloadpic()
    {
        ES_SERVER_INFODB mES_SERVER_INFODB = new ES_SERVER_INFODB();
        ES_SERVER_INFOSet mES_SERVER_INFOSet = new ES_SERVER_INFOSet();
        mES_SERVER_INFOSet = mES_SERVER_INFODB.query();
        LDSysVarDB mLDSysVarDB = new LDSysVarDB();
        mLDSysVarDB.setSysVar("ZipFilePath");
        LDSysVarSet mLDSysVarSet = new LDSysVarSet();
        mLDSysVarSet = mLDSysVarDB.query();
        String ZipPath = mLDSysVarSet.get(1).getSysVarValue();
        String RealPath = "";
        RealPath = mES_SERVER_INFOSet.get(1).getServerBasePath();
        String cDate = PubFun.getCurrentDate2();
        String mDate = PubFun.getCurrentDate();
        String mTime = PubFun.getCurrentTime();
        System.out.println("mES_DOC_MAINSet.size() is " + mES_DOC_MAINSet.size());
        //定义一个数组，用于将所有数据放入到一个excel中；
        if (mES_DOC_MAINSet.size() > 0) {
            String[][] mToExcel = new String[mES_DOC_MAINSet.size() + 2][3];
            mToExcel[0][0] = "机构编码";
            mToExcel[0][1] = "印刷号码";
            mToExcel[0][2] = "保单性质";
            for (int i = 1; i <= mES_DOC_MAINSet.size(); i++) {
                mToExcel[i][0] = mES_DOC_MAINSet.get(i).getManageCom();
                mToExcel[i][1] = mES_DOC_MAINSet.get(i).getDocCode();
                String subtype = mES_DOC_MAINSet.get(i).getSubType();
                if (subtype.equals("HM01")) {
                    mToExcel[i][2] = "医院合同";
                } else if (subtype.equals("HM02")) {
                    mToExcel[i][2] = "医生合同";
                }else if (subtype.equals("HM03")) {
                    mToExcel[i][2] = "社保政策";
                }else if (subtype.equals("HM10")) {
                    mToExcel[i][2] = "体检清单";
                }else {
                    mToExcel[i][2] = "";
                }
            }
            mToExcel[mES_DOC_MAINSet.size() + 1][0] = "合计";
            mToExcel[mES_DOC_MAINSet.size() + 1][1] = mES_DOC_MAINSet.size() + "";
            mToExcel[mES_DOC_MAINSet.size() + 1][2] = "";
            try
            {
                WriteToExcel t = new WriteToExcel("downlog.xls");
                t.createExcelFile();
                String[] sheetName = {cDate};
                t.addSheet(sheetName);
                t.setData(0, mToExcel);
                System.out.println("ZipPath  : " + ZipPath);
                t.write(ZipPath);
            } catch (Exception ex)
            {
                ex.toString();
                ex.printStackTrace();
            }

        }
        for (int i = 1; i <= mES_DOC_MAINSet.size(); i++)
        {
            ES_DOC_PAGESDB mES_DOC_PAGESDB = new ES_DOC_PAGESDB();
            ES_DOC_PAGESSet mES_DOC_PAGESSet = new ES_DOC_PAGESSet();
            mES_DOC_PAGESDB.setDocID(mES_DOC_MAINSet.get(i).getDocID());
            mES_DOC_PAGESSet = mES_DOC_PAGESDB.query();
            if (mES_DOC_PAGESSet.size() > 0) {
                for (int j = 1; j <= mES_DOC_PAGESSet.size(); j++) {
                    mFilePaths.add(RealPath +
                                   mES_DOC_PAGESSet.get(j).getPicPath() +
                                   mES_DOC_PAGESSet.get(j).getPageName() +
                                   ".tif");
                    System.out.println("mFilePaths is " + mFilePaths);
                    mFileNames.add(cDate + "\\" +
                                   mES_DOC_MAINSet.get(i).getManageCom() + "\\" +
                                   mES_DOC_MAINSet.get(i).getDocCode() + "_" +
                                   mES_DOC_PAGESSet.size() + "_" + j + ".tif");
                    System.out.println("mFileNames is " + mFileNames);
                }
            }

        }
        mFilePaths.add(ZipPath + "downlog.xls");
        mFileNames.add(cDate + "\\" + cDate + ".xls");
        String[] tFilePaths = (String[]) mFilePaths.toArray(new String[0]);
        String[] tFileNames = (String[]) mFileNames.toArray(new String[0]);
        for (int m = 0; m < tFilePaths.length; m++) {
            System.out.println("tFilePaths" + tFilePaths[m] + "");
            System.out.println("tFileNames" + tFileNames[m] + "");

        }
        String tZipPath = ZipPath + "downloadfile.zip";
        ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
        if (!tProposalDownloadBL.CreatZipFile(tFilePaths, tFileNames, tZipPath)) {
            System.out.println("生成压缩文件失败");
            return false;
        } else {
//            for (int k = 1; k <= mES_DOC_MAINSet.size(); k++) {
//                LCScanDownloadSchema tLCScanDownloadSchema = new
//                        LCScanDownloadSchema();
//                tLCScanDownloadSchema.setDocID(mES_DOC_MAINSet.get(k).getDocID());
//                tLCScanDownloadSchema.setDocCode(mES_DOC_MAINSet.get(k).
//                                                 getDocCode());
//                tLCScanDownloadSchema.setBussType(mES_DOC_MAINSet.get(k).
//                                                  getBussType());
//                tLCScanDownloadSchema.setSubType(mES_DOC_MAINSet.get(k).
//                                                 getSubType());
//                tLCScanDownloadSchema.setNumPages(mES_DOC_MAINSet.get(k).
//                                                  getNumPages());
//                tLCScanDownloadSchema.setDocFlag(mES_DOC_MAINSet.get(k).
//                                                 getDocFlag());
//                tLCScanDownloadSchema.setManageCom(mGlobalInput.ManageCom);
//                tLCScanDownloadSchema.setDownCount("1");
//                tLCScanDownloadSchema.setOperator(mGlobalInput.Operator);
//                tLCScanDownloadSchema.setMakeDate(mDate);
//                tLCScanDownloadSchema.setMakeTime(mTime);
//                tLCScanDownloadSchema.setModifyDate(mDate);
//                tLCScanDownloadSchema.setModifyTime(mTime);
//                mLCScanDownloadSet.add(tLCScanDownloadSchema);
//            }
//            LCScanDownloadDBSet mLCScanDownloadDBSet = new LCScanDownloadDBSet();
//            mLCScanDownloadDBSet.set(mLCScanDownloadSet);
//            if (!mLCScanDownloadDBSet.insert()) {
//                System.out.println("更新LCScanDownload表失败");
//                return false;
//            } else {
                System.out.println("生成压缩文件成功");
                return true;
//            }
        }

    }

    private boolean ReDownloadpic() {
        ES_SERVER_INFODB mES_SERVER_INFODB = new ES_SERVER_INFODB();
        ES_SERVER_INFOSet mES_SERVER_INFOSet = new ES_SERVER_INFOSet();
        mES_SERVER_INFOSet = mES_SERVER_INFODB.query();
        LDSysVarDB mLDSysVarDB = new LDSysVarDB();
        mLDSysVarDB.setSysVar("ZipFilePath");
        LDSysVarSet mLDSysVarSet = new LDSysVarSet();
        mLDSysVarSet = mLDSysVarDB.query();
        String ZipPath = mLDSysVarSet.get(1).getSysVarValue();
        String RealPath = "";
        RealPath = mES_SERVER_INFOSet.get(1).getServerBasePath();
        String cDate = PubFun.getCurrentDate2();
        String mDate = PubFun.getCurrentDate();
        String mTime = PubFun.getCurrentTime();
        System.out.println("mES_DOC_MAINSet.size() is " + mES_DOC_MAINSet.size());
        //定义一个数组，用于将所有数据放入到一个excel中；
        if (mES_DOC_MAINSet.size() > 0) {
            String[][] mToExcel = new String[mES_DOC_MAINSet.size() + 2][3];
            mToExcel[0][0] = "机构编码";
            mToExcel[0][1] = "保单号码";
            mToExcel[0][2] = "保单性质";
            for (int i = 1; i <= mES_DOC_MAINSet.size(); i++) {
                mToExcel[i][0] = mES_DOC_MAINSet.get(i).getManageCom();
                mToExcel[i][1] = mES_DOC_MAINSet.get(i).getDocCode();
                String subtype = mES_DOC_MAINSet.get(i).getSubType();
                if (subtype.equals("HM01")) {
                    mToExcel[i][2] = "医院合同";
                } else if (subtype.equals("HM02")) {
                    mToExcel[i][2] = "医生合同";
                }else if (subtype.equals("HM03")) {
                    mToExcel[i][2] = "社保政策";
                }else if (subtype.equals("HM10")) {
                    mToExcel[i][2] = "体检清单";
                }else {
                    mToExcel[i][2] = "";
                }

            }
            mToExcel[mES_DOC_MAINSet.size() + 1][0] = "合计";
            mToExcel[mES_DOC_MAINSet.size() +
                    1][1] = mES_DOC_MAINSet.size() + "";
            mToExcel[mES_DOC_MAINSet.size() + 1][2] = "";
            try {
                WriteToExcel t = new WriteToExcel("downlog.xls");
                t.createExcelFile();
                String[] sheetName = {cDate};
                t.addSheet(sheetName);
                t.setData(0, mToExcel);
                System.out.println("zip paht " + ZipPath);
                t.write(ZipPath);

            } catch (Exception ex) {
                ex.toString();
                ex.printStackTrace();
            }
        }

        for (int i = 1; i <= mES_DOC_MAINSet.size(); i++) {
            ES_DOC_PAGESDB mES_DOC_PAGESDB = new ES_DOC_PAGESDB();
            ES_DOC_PAGESSet mES_DOC_PAGESSet = new ES_DOC_PAGESSet();
            mES_DOC_PAGESDB.setDocID(mES_DOC_MAINSet.get(i).getDocID());
            mES_DOC_PAGESSet = mES_DOC_PAGESDB.query();
            if (mES_DOC_PAGESSet.size() > 0) {
                for (int j = 1; j <= mES_DOC_PAGESSet.size(); j++) {
                    mFilePaths.add(RealPath +
                                   mES_DOC_PAGESSet.get(j).getPicPath() +
                                   mES_DOC_PAGESSet.get(j).getPageName() +
                                   ".tif");
                    System.out.println("mFilePaths is " + mFilePaths);
                    mFileNames.add(cDate + "\\" +
                                   mES_DOC_MAINSet.get(i).getManageCom() + "\\" +
                                   mES_DOC_MAINSet.get(i).getDocCode() + "_" +
                                   mES_DOC_PAGESSet.size() + "_" + j + ".tif");
                    System.out.println("mFileNames is " + mFileNames);

                }

            }

        }
        mFilePaths.add(ZipPath + "downlog.xls");
        mFileNames.add(cDate + "\\" + cDate + ".xls");
        String[] tFilePaths = (String[]) mFilePaths.toArray(new String[0]);
        String[] tFileNames = (String[]) mFileNames.toArray(new String[0]);
        for (int m = 0; m < tFilePaths.length; m++) {
            System.out.println("tFilePaths" + tFilePaths[m] + "");
            System.out.println("tFileNames" + tFileNames[m] + "");

        }
        String tZipPath = ZipPath + "downloadfile.zip";
        ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
        if (!tProposalDownloadBL.CreatZipFile(tFilePaths, tFileNames, tZipPath)) {
            System.out.println("生成压缩文件失败");
            return false;
        } else {
            return true;
        }

    }

}
