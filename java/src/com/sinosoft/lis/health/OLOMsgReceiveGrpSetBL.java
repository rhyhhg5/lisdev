/*
 * <p>ClassName: OLOMsgReceiveGrpSetBL </p>
 * <p>Description: OLOMsgReceiveGrpSetBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-19 13:04:32
 */
package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class OLOMsgReceiveGrpSetBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
private MMap map= new MMap();
/** 全局数据 */
private static String DEPARTMENT = "HM";
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
/** 业务处理相关变量 */
private LHMsgCustomGrpSchema mLHMsgCustomGrpSchema = new LHMsgCustomGrpSchema();
private LHMsgCustomGrpDetailSchema mLHMsgCustomGrpDetailSchema = new LHMsgCustomGrpDetailSchema();
private LHMsgCustomGrpDetailSet mLHMsgCustomGrpDetailSet = new LHMsgCustomGrpDetailSet();
public OLOMsgReceiveGrpSetBL() {
}
public static void main(String[] args) {

    OLOMsgReceiveGrpSetBL tOLOMsgReceiveGrpSetBL = new OLOMsgReceiveGrpSetBL();

    LHMsgCustomGrpSchema tLHMsgCustomGrpSchema = new LHMsgCustomGrpSchema();
    LHMsgCustomGrpDetailSet tLHMsgCustomGrpDetailSet = new LHMsgCustomGrpDetailSet();

    String tRela  = "";
    String FlagStr = "";
    String Content = "";
    String transact = "";
    String GrpNo = "";
    GlobalInput tG = new GlobalInput();
    tG.ManageCom = "86";
    tG.Operator = "hm";

    transact = "INSERT||MAIN";

    tLHMsgCustomGrpSchema.setCustomGrpNo("00000000000000");
    tLHMsgCustomGrpSchema.setCustomGrpName("000000");
    tLHMsgCustomGrpSchema.setContent("abcdefghijklmn");


    for(int i = 0; i <1 ; i++)
    {
        LHMsgCustomGrpDetailSchema tLHMsgCustomGrpDetailSchema = new LHMsgCustomGrpDetailSchema();
        tLHMsgCustomGrpDetailSchema.setCustomerNo("000000001");
        tLHMsgCustomGrpDetailSchema.setGrpContNo("000000000000");
        tLHMsgCustomGrpDetailSchema.setContNo("13000000089");

        tLHMsgCustomGrpDetailSchema.setMakeDate(PubFun.getCurrentDate());
        tLHMsgCustomGrpDetailSchema.setMakeTime("20:20:00");
        tLHMsgCustomGrpDetailSchema.setModifyDate(PubFun.getCurrentDate());
        tLHMsgCustomGrpDetailSchema.setModifyTime("20:20:00");
        tLHMsgCustomGrpDetailSet.add(tLHMsgCustomGrpDetailSchema);
    }



  // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(tLHMsgCustomGrpSchema);
        tVData.add(tLHMsgCustomGrpDetailSet);
        tVData.add(tG);System.out.println("-------Submit:"+tG.Operator+"-------");
        tOLOMsgReceiveGrpSetBL.submitData(tVData,transact);


}
/**
* 传输数据的公共方法
	* @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "OLOMsgReceiveGrpSetBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败OLOMsgReceiveGrpSetBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
        System.out.println("Start OLOMsgReceiveGrpSetBL Submit...");
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
          // @@错误处理
          this.mErrors.copyAllErrors(tPubSubmit.mErrors);
          CError tError = new CError();
          tError.moduleName = "LHCustomInHospitalBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";

          this.mErrors.addOneError(tError);
          return false;
        }
        System.out.println("End OLHCustomInHospitalBL Submit...");

      }
      mInputData = null;
      return true;
    }

    /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
  if (this.mOperate.equals("INSERT||MAIN"))
    {
        mLHMsgCustomGrpSchema.setCustomGrpNo(PubFun1.CreateMaxNo("HMMsgCusGrp",12));
        mLHMsgCustomGrpSchema.setDepartment(DEPARTMENT);
        mLHMsgCustomGrpSchema.setOperator(mGlobalInput.Operator);
        mLHMsgCustomGrpSchema.setMakeDate(PubFun.getCurrentDate());
        mLHMsgCustomGrpSchema.setMakeTime(PubFun.getCurrentTime());
        mLHMsgCustomGrpSchema.setModifyDate(PubFun.getCurrentDate());
        mLHMsgCustomGrpSchema.setModifyTime(PubFun.getCurrentTime());
        mLHMsgCustomGrpSchema.setManageCom(mGlobalInput.ManageCom);
        map.put(mLHMsgCustomGrpSchema, "INSERT");

        for(int i = 1; i <= mLHMsgCustomGrpDetailSet.size(); i++)
        {System.out.println("-----**"+i+"***----");
            mLHMsgCustomGrpDetailSet.get(i).setCustomGrpNo(mLHMsgCustomGrpSchema.getCustomGrpNo());
            System.out.println(mLHMsgCustomGrpDetailSet.get(i).getContNo());
            System.out.println(mLHMsgCustomGrpDetailSet.get(i).getGrpContNo());
            mLHMsgCustomGrpDetailSet.get(i).setOperator(mGlobalInput.Operator);System.out.println(mLHMsgCustomGrpDetailSet.get(i).getOperator());
            mLHMsgCustomGrpDetailSet.get(i).setMakeDate(PubFun.getCurrentDate());System.out.println( mLHMsgCustomGrpDetailSet.get(i).getMakeDate());
            mLHMsgCustomGrpDetailSet.get(i).setMakeTime(PubFun.getCurrentTime());System.out.println(mLHMsgCustomGrpDetailSet.get(i).getMakeTime());
            mLHMsgCustomGrpDetailSet.get(i).setModifyDate(PubFun.getCurrentDate());System.out.println(mLHMsgCustomGrpDetailSet.get(i).getModifyDate());
            mLHMsgCustomGrpDetailSet.get(i).setModifyTime(PubFun.getCurrentTime());System.out.println( mLHMsgCustomGrpDetailSet.get(i).getModifyTime());
            mLHMsgCustomGrpDetailSet.get(i).setManageCom(mGlobalInput.ManageCom);System.out.println( mLHMsgCustomGrpDetailSet.get(i).getManageCom());
        }

        map.put(mLHMsgCustomGrpDetailSet, "INSERT");
    }

    if (this.mOperate.equals("UPDATE||MAIN"))
    {
        mLHMsgCustomGrpSchema.setOperator(mGlobalInput.Operator);
        mLHMsgCustomGrpSchema.setModifyDate(PubFun.getCurrentDate());
        mLHMsgCustomGrpSchema.setModifyTime(PubFun.getCurrentTime());
        mLHMsgCustomGrpSchema.setManageCom(mGlobalInput.ManageCom);

        map.put("delete from LHMsgCustomGrp where CustomGrpNo = '"+mLHMsgCustomGrpSchema.getCustomGrpNo()+"'","DELETE");
        map.put(mLHMsgCustomGrpSchema, "INSERT");

        for(int i = 1; i <= mLHMsgCustomGrpDetailSet.size(); i++)
        {System.out.println("-----**"+i+"***----");
            mLHMsgCustomGrpDetailSet.get(i).setCustomGrpNo(mLHMsgCustomGrpSchema.getCustomGrpNo());System.out.println(mLHMsgCustomGrpSchema.getCustomGrpNo());
            mLHMsgCustomGrpDetailSet.get(i).setOperator(mGlobalInput.Operator);System.out.println(mLHMsgCustomGrpDetailSet.get(i).getOperator());
            mLHMsgCustomGrpDetailSet.get(i).setModifyDate(PubFun.getCurrentDate());System.out.println(mLHMsgCustomGrpDetailSet.get(i).getModifyDate());
            mLHMsgCustomGrpDetailSet.get(i).setModifyTime(PubFun.getCurrentTime());System.out.println( mLHMsgCustomGrpDetailSet.get(i).getModifyTime());
            mLHMsgCustomGrpDetailSet.get(i).setManageCom(mGlobalInput.ManageCom);System.out.println( mLHMsgCustomGrpDetailSet.get(i).getManageCom());
        }
        map.put("delete from LHMsgCustomGrpDetail where CustomGrpNo = '"+mLHMsgCustomGrpSchema.getCustomGrpNo()+"'","DELETE");
        map.put(mLHMsgCustomGrpDetailSet, "INSERT");
    }

    if (this.mOperate.equals("DELETE||MAIN"))
    {
      mLHMsgCustomGrpDetailSchema.setCustomGrpNo(mLHMsgCustomGrpSchema.getCustomGrpNo());
      map.put(mLHMsgCustomGrpSchema, "DELETE");
      map.put(mLHMsgCustomGrpDetailSchema, "DELETE");
    }

  return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
    this.mLHMsgCustomGrpSchema.setSchema((LHMsgCustomGrpSchema)cInputData.getObjectByObjectName("LHMsgCustomGrpSchema",0));
    if(!mOperate.equals("DELETE||MAIN"))
    {
        this.mLHMsgCustomGrpDetailSet.set((LHMsgCustomGrpDetailSet) cInputData.
        getObjectByObjectName("LHMsgCustomGrpDetailSet", 0));
    }
    this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
     return true;
}
 private boolean prepareOutputData()
 {
   try
   {
       this.mInputData.clear();
       this.mInputData.add(this.mLHMsgCustomGrpSchema);
       this.mInputData.add(this.mLHMsgCustomGrpDetailSet);
       mInputData.add(map);
       mResult.clear();
       mResult.add(this.mLHMsgCustomGrpSchema);
       mResult.add(this.mLHMsgCustomGrpDetailSet);
   }
   catch(Exception ex)
   {
           // @@错误处理
           CError tError =new CError();
           tError.moduleName="LHStatuteBL";
           tError.functionName="prepareData";
           tError.errorMessage="在准备往后层处理所需要的数据时出错。";
           this.mErrors .addOneError(tError) ;
           return false;
    }
    return true;
  }
  public VData getResult() {
      return this.mResult;
  }
}
