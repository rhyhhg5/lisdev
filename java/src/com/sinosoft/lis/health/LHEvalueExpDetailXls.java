package com.sinosoft.lis.health;

import java.util.*;
import java.io.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LHEvalueExpMainSchema;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import com.sinosoft.lis.easyscan.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class LHEvalueExpDetailXls
{
    public LHEvalueExpDetailXls() {}
    String FullPath = "";
    String  []FileSheetNum;
    String ZipPathName="";
    String FilePath="";

    public static void main(String[] args)
    {
        LHEvalueExpMainSet tLHEvalueExpMainSet = new LHEvalueExpMainSet();
        LHEvalueExpMainSchema tLHEvalueExpMainSchema = new LHEvalueExpMainSchema();
        tLHEvalueExpMainSchema.setCusEvalCode("00000000000000000896");
        tLHEvalueExpMainSet.add(tLHEvalueExpMainSchema);
        LHEvalueExpMainSchema tLHEvalueExpMainSchema2 = new LHEvalueExpMainSchema();
        tLHEvalueExpMainSchema2.setCusEvalCode("00000000000000000898");
        tLHEvalueExpMainSet.add(tLHEvalueExpMainSchema2);

        LHEvalueExpDetailXls t = new LHEvalueExpDetailXls();
        t.DownloadSubmit(tLHEvalueExpMainSet, "E:\\workspace\\ui\\");
        t.getResult();
    }

    public boolean CreateZip(String[] tFilePaths, String[] tFileNames, String tZipPath)
    {
        ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
        if (!tProposalDownloadBL.CreatZipFile(tFilePaths, tFileNames, tZipPath))
        {
            System.out.println("生成压缩文件失败");
            CError.buildErr(this,"生成压缩文件失败");
            return false;
        }
        return true;
    }

    public boolean DownloadSubmit(LHEvalueExpMainSet tLHEvalueExpMainSet, String Path)
    {
        FullPath = Path + "healthmanage/";   //客户端下载文件的保存路径
        String[][] mToExcel = new String[1][10];
        mToExcel[0][0] = "评估明细流水号码";
        mToExcel[0][1] = "问卷流水号";
        mToExcel[0][2] = "客户评估号码";
        mToExcel[0][3] = "标准问题代码";
        mToExcel[0][4] = "标准问题名称";
        mToExcel[0][5] = "文字答案";
        mToExcel[0][6] = "标准问题单位";
        mToExcel[0][7] = "管理机构";
        mToExcel[0][8] = "操作员代码";
        mToExcel[0][9] = "客户姓名";


        try
        {
            WriteToExcel t = new WriteToExcel("LHEvalueExpDetail.xls");
            t.createExcelFile();
            String[] sheetName = new String[tLHEvalueExpMainSet.size()];
            for(int i = 0; i <tLHEvalueExpMainSet.size(); i++ )
            {
                String sqlCustomerNo =
                        "  select a.CustomerNo from LHEvalueExpMain a where  a.CusEvalCode='" +
                        tLHEvalueExpMainSet.get(i + 1).getCusEvalCode().toString() + "'"
                        ;
                String mCustomerNo =  new ExeSQL().getOneValue(sqlCustomerNo); //健康评估客户评估号码对应的客户号
                //System.out.println("内部客户号客户号 " + mCustomerNo);
                sheetName[i] =mCustomerNo;
            }
            t.addSheet(sheetName);
            for(int i = 0; i <sheetName.length; i++ )
            {
                t.setData(i, mToExcel);
                String sqlx = "select distinct a.EvalExportNo,"
                              +"(select b.ServPlanNo from LHEvalueExpMain b where b.CusEvalCode=a.CusEvalCode),"
                              +"a.CusEvalCode,a.StandCode,"
                              +"(select c.StandContent from LHManaStandPro c where c.StandCode =a.StandCode ),"
                              +" (case when a.StandCode='08010003' then (select Occupationname from ldoccupation where Occupationcode=a.LetterResult) else a.LetterResult end ),"//zsjing 2007.07.09
                              //+"a.LetterResult,"
                              +"(select c.StandUnit from LHManaStandPro c where c.StandCode =a.StandCode ),"
                              +" (select s.Comid  from lhservitem s ,LHEvalueExpMain m  where s.ServItemno=m.ServItemno and m.CusEvalCode=a.CusEvalCode),"
                              +" a.Operator, "
                              +" (select n.Name from ldperson n ,LHEvalueExpMain m  where n.CustomerNo=m.CustomerNo and m.CusEvalCode=a.CusEvalCode)"
                              +" from LHEvalueExpDetail a where (a.Quesstatus is null or Quesstatus ='') and a.CusEvalCode='"
                              +tLHEvalueExpMainSet.get(i+1).getCusEvalCode().toString()
                              + "' order by StandCode"
                              ;
                t.setData(i, sqlx);
            }
            t.write(FullPath);
        }
        catch (Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }
        ZipPathName = FullPath+(PubFun.getCurrentDate().replaceAll("-","")+"-" + PubFun.getCurrentTime().substring(0,5)+"-LHEvalueExpDetail.zip").replaceAll(":", "");//压缩文件名
        FilePath  = (PubFun.getCurrentDate().replaceAll("-","")+"-" + PubFun.getCurrentTime().substring(0,5)+"-LHEvalueExpDetail.zip").replaceAll(":", "");//压缩文件名
        String []FilePath  = {FullPath+"LHEvalueExpDetail.xls"}; //压缩文件路径全名
        String []FileName = {"LHEvalueExpDetail.xls"};//Excel文件名
        CreateZip(FilePath, FileName, ZipPathName);
        File fd = new File(FilePath[0]);
        fd.delete();
        return true;
    }

    public String getResult()
    {
        System.out.println("FullPath  : " + FilePath);
        return FilePath;
    }

}
