/*
 * <p>ClassName: LHContPlanSettingBL </p>
 * <p>Description: LHContPlanSettingBL类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-07-04 13:55:01
 */
package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.lang.*;

public class LHContPlanSettingBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    public String getNo = "";
    /** 业务处理相关变量 */
    private LHServCaseRelaSchema mLHServCaseRelaSchema = new LHServCaseRelaSchema();
    private LHServCaseRelaSet mLHServCaseRelaSet = new LHServCaseRelaSet();
    private LHServCaseDefSet mLHServCaseDefSet = new LHServCaseDefSet();
    public LHContPlanSettingBL()
    {
    }


    public static void main(String[] args) {
        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "86";
        tG.Operator = "hm";
        tG.ComCode = "86";

        LHServCaseRelaSet tLHServCaseRelaSet = new LHServCaseRelaSet();
        LHServCaseDefSet tLHServCaseDefSet = new LHServCaseDefSet();
        LHContPlanSettingUI tLHContPlanSettingUI = new LHContPlanSettingUI();

        System.out.println("***********begin init**************");
        //输出参数
        //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
        String transact = "INSERT||MAIN";

        for (int i = 0; i < 2; i++) {
            LHServCaseRelaSchema tLHServCaseRelaSchema = new
                    LHServCaseRelaSchema();
            tLHServCaseRelaSchema.setServPlanNo("ServPlanNo"); //服务计划号码
            tLHServCaseRelaSchema.setServPlanName("ServPlanName"); //服务计划名称
            tLHServCaseRelaSchema.setComID("ComID"); //机构属性标识
            tLHServCaseRelaSchema.setServPlanLevel("ServPlanLevel"); //服务计划档次
            tLHServCaseRelaSchema.setContNo("ContNo"); //保单号
            tLHServCaseRelaSchema.setCustomerNo("CustomerNo"); //客户号
            tLHServCaseRelaSchema.setCustomerName("Name"); //姓名

            tLHServCaseRelaSchema.setManageCom("86");
            tLHServCaseRelaSchema.setOperator("hm");
            tLHServCaseRelaSchema.setMakeDate("2006-05-05");
            tLHServCaseRelaSchema.setMakeTime("20:00:00");
            tLHServCaseRelaSchema.setServItemCode("001");
            tLHServCaseRelaSet.add(tLHServCaseRelaSchema);

            LHServCaseDefSchema tLHServCaseDefSchema = new LHServCaseDefSchema();

            tLHServCaseDefSchema.setServCaseType("1");
            tLHServCaseDefSchema.setServCaseCode("1111111");
            tLHServCaseDefSchema.setServCaseName("aaaaaaaaaaaa");
            tLHServCaseDefSchema.setServCaseState("1");

            tLHServCaseDefSchema.setOperator("hm");
            tLHServCaseDefSchema.setMakeDate("2006-05-05");
            tLHServCaseDefSchema.setMakeTime("20:00:00");
            tLHServCaseDefSchema.setModifyDate("2006-05-05");
            tLHServCaseDefSchema.setModifyTime("20:00:00");
            tLHServCaseDefSet.add(tLHServCaseDefSchema);

        }
        VData tVData = new VData();
        //tVData.add(tLHServCaseDefSchema);
        tVData.add(tLHServCaseDefSet);
        tVData.add(tLHServCaseRelaSet);

        tVData.add(tG);
        tLHContPlanSettingUI.submitData(tVData, transact);

    }

    /**
     * 传输数据的公共方法
     * @param cInputData 输入的数据
     *         cOperate 数据操作
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHContPlanSettingBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LHContPlanSettingBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            System.out.println("Start LHContPlanSettingBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();

            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                CError tError = new CError();
                tError.moduleName = "LHContPlanSettingBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }

        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     *
     */
    private boolean dealData()
    {
        if (this.mOperate.equals("INSERT||MAIN"))
        {//插入操作
            if (mLHServCaseDefSet != null && mLHServCaseDefSet.size() > 0)
            {//如果需要修改的数据不为空
                for (int i = 1; i <= mLHServCaseDefSet.size(); i++)
                {//从第一个开始
                    if (mLHServCaseDefSet.get(i).getServCaseType().equals("1"))
                    {//如果是个单
                        String MStartDate = PubFun.getCurrentDate().replaceAll("-", "");
                        String HiddenCaseCode = MStartDate +mLHServCaseDefSet.get(i).getServCaseType();
                        String ServCaseCode = "";
                        SSRS tSSRS = new SSRS();
                        ExeSQL tExeSQL = new ExeSQL();
                        String sql =
                                "select max(substr(ServCaseCode, 10,5)) from LHServCaseDef where makedate='"+ PubFun.getCurrentDate() +"' ";

                        tSSRS = tExeSQL.execSQL(sql);
                        String rr[][] = tSSRS.getAllData();
                        String result = rr[0][0].toString();

                        if (result.equals("") || result.equals(null) | result.equals("null"))
                        {
                            ServCaseCode = HiddenCaseCode + ServCaseCode + "0" +
                                           "000" + i;
                        }
                        else
                        {
                            Integer ii = new Integer(result);     //结果转为整型
                            int iii = ii.intValue();
                            int oo = iii++;                       //在数据库最大值加上‘1’
                            Integer result2 = new Integer(oo + i);//判断队列里有多少个
                            String result3 = result2.toString();
                            String result4 = "";
                            if (result3.length() == 4)
                            {
                                result4 = "0" + result3.toString();
                            } else if (result3.length() == 3)
                            {
                                result4 = "00" + result3.toString();
                            } else if (result3.length() == 2)
                            {
                                result4 = "000" + result3.toString();
                            } else if (result3.length() == 1)
                            {
                                result4 = "0000" + result3.toString();
                            } else if (result3.length() == 5)
                            {
                                result4 = result3.toString();
                            }

                            ServCaseCode = HiddenCaseCode + ServCaseCode + result4;
                        }

                        mLHServCaseDefSet.get(i).setServCaseCode(ServCaseCode);
                        mLHServCaseDefSet.get(i).setServCaseName(
                                mLHServCaseRelaSet.get(i).getGrpName() + "-" +
                                mLHServCaseRelaSet.get(i).getCustomerName());
                        mLHServCaseDefSet.get(i).setOperator(mGlobalInput.Operator);
                   //     mLHServCaseDefSet.get(i).setManageCom(mGlobalInput.ManageCom);
                        mLHServCaseDefSet.get(i).setMakeDate(PubFun.getCurrentDate());
                        mLHServCaseDefSet.get(i).setMakeTime(PubFun.getCurrentTime());
                        mLHServCaseDefSet.get(i).setModifyDate(PubFun.getCurrentDate());
                        mLHServCaseDefSet.get(i).setModifyTime(PubFun.getCurrentTime());
                    }
                }
                map.put(mLHServCaseDefSet, "INSERT");
            }
            if (mLHServCaseRelaSet != null && mLHServCaseRelaSet.size() > 0)
            {
                for (int j = 1; j <= mLHServCaseRelaSet.size(); j++)
                {
                    mLHServCaseRelaSet.get(j).setServCaseCode(mLHServCaseDefSet.get(j).getServCaseCode());
                    mLHServCaseRelaSet.get(j).setOperator(mGlobalInput.Operator);
                    mLHServCaseRelaSet.get(j).setMakeDate(PubFun.getCurrentDate());
                    mLHServCaseRelaSet.get(j).setMakeTime(PubFun.getCurrentTime());
                    mLHServCaseRelaSet.get(j).setManageCom(mGlobalInput.ManageCom);
                    mLHServCaseRelaSet.get(j).setModifyDate(PubFun.getCurrentDate());
                    mLHServCaseRelaSet.get(j).setModifyTime(PubFun.getCurrentTime());
                }
                map.put(mLHServCaseRelaSet, "INSERT");
            }
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            mLHServCaseRelaSchema.setOperator(mGlobalInput.Operator);
            mLHServCaseRelaSchema.setManageCom(mGlobalInput.ManageCom);
            mLHServCaseRelaSchema.setModifyDate(PubFun.getCurrentDate());
            mLHServCaseRelaSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(mLHServCaseRelaSchema, "DELETE&INSERT");
        }

        if (this.mOperate.equals("DELETE||MAIN"))
        {
            map.put(mLHServCaseRelaSchema, "DELETE");
            map.put("delete from LHServExeTrace where ServPlanNo = '" +
                    mLHServCaseRelaSchema.getServPlanNo() + "'", "DELETE");
            if (mLHServCaseDefSet.size() > 0)
            {
                map.put(mLHServCaseDefSet, "DELETE");
            }
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     *
     */
    private boolean updateData()
    {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true\
     * @return boolean
     * @param cInputData
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLHServCaseRelaSet.set((LHServCaseRelaSet) cInputData.getObjectByObjectName("LHServCaseRelaSet", 0));
        this.mLHServCaseDefSet.set((LHServCaseDefSet) cInputData.getObjectByObjectName("LHServCaseDefSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     * @return boolean
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        LHServCaseRelaDB tLHServCaseRelaDB = new LHServCaseRelaDB();
        tLHServCaseRelaDB.setSchema(this.mLHServCaseRelaSchema);
        //如果有需要处理的错误，则返回
        if (tLHServCaseRelaDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHServCaseRelaDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHContPlanSettingBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 准备往后层处理所需要的数据
     * @return boolean
     */

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            this.mInputData.add(this.mLHServCaseRelaSet);
            this.mInputData.add(this.mLHServCaseDefSet);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLHServCaseRelaSet);
            mResult.add(this.mLHServCaseDefSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHContPlanSettingBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 返回结果
     * @return VData
     */

    public VData getResult() {
        return this.mResult;
    }
}
