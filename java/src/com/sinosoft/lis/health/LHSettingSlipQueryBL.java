/*
 * <p>ClassName: LHSettingSlipQueryBL </p>
 * <p>Description: LHSettingSlipQueryBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-07-26 14:30:01
 */
package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class LHSettingSlipQueryBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LHServCaseDefSchema mLHServCaseDefSchema = new LHServCaseDefSchema();
    private LHServCaseDefSet mLHServCaseDefSet = new LHServCaseDefSet();

    public LHSettingSlipQueryBL() {
    }

    public static void main(String[] args) {

    }

    /**
     * 传输数据的公共方法
     * @param cInputData 输入的数据
     *         cOperate 数据操作
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHSettingSlipQueryBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LHSettingSlipQueryBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if(!addData()){
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     *
     */
    private boolean dealData() {
        System.out.println("--------DealData---------");
        System.out.println("AAAAA  AAA" + mLHServCaseDefSchema.getServCaseCode());
        System.out.println("BB BBBBBBBB " +
                           mLHServCaseDefSchema.getServCaseState());
        if (this.mOperate.equals("UPDATE||MAIN")) {
            System.out.println("?????????" + mLHServCaseDefSet.size());
            for (int i = 1; i <= mLHServCaseDefSet.size(); i++) {
               //  mLHServCaseDefSet.get(i).setOperator(mGlobalInput.Operator);//liuli 注掉于08/05/07 发现没有什么用
               // mLHServCaseDefSet.get(i).setMakeDate(PubFun.getCurrentDate());
               // mLHServCaseDefSet.get(i).setMakeTime(PubFun.getCurrentTime());
               // mLHServCaseDefSet.get(i).setModifyDate(PubFun.getCurrentDate());
               // mLHServCaseDefSet.get(i).setModifyTime(PubFun.getCurrentTime());
               //mLHServCaseDefSet.get(i).setManageCom(mGlobalInput.ManageCom); liuli 改于08/05/07 解决机构号问题
                map.put(
                        " update LHServCaseDef set  ServCaseState='1',ModifyDate=current date,ModifyTime=current time,"
                        +" Operator='"+mGlobalInput.Operator+"' where ServCaseCode='" +
                        mLHServCaseDefSet.get(i).getServCaseCode() + "' ",
                        "UPDATE");

            }
        }
        this.mInputData.clear();
       // this.mInputData.add(this.mLHServCaseDefSet);
        mInputData.add(map);
        mResult.clear();
        mResult.add(this.mLHServCaseDefSet);
        if(!prepareOutputData(mInputData,mOperate)){
            return false;
        }
        return true;
    }

    /**
     * 当启动事件为短信或通讯服务事件时,在启动之后生成新一期的短信和通讯服务事件.add 2008/1/22
     * 当为短信时,生成14天之后的下一期,当为通讯时,生成12个月之后的下一期.
     * 对名字有一定的规则要求,本期的名字格式不对,则不能正确生成下一期的服务事件名称.
     * @return boolean
     */
    private boolean addData() {
     //   LHServCaseDefSet tLHServCaseDefSet = new LHServCaseDefSet();
        for (int i = 1; i <= mLHServCaseDefSet.size(); i++) {
            MMap tmap = new MMap();
            String sqla = " select distinct a.servcasetype,a.servcasedes,a.managecom from lhservcasedef a where "
                          +" a.servcasecode='"+mLHServCaseDefSet.get(i).getServCaseCode()+"' with ur ";
            SSRS ta = new ExeSQL().execSQL(sqla);//获取事件的类型项目号码
            if(ta.MaxRow == 1&&ta.GetText(1,1).equals("2")&&(ta.GetText(1,2).equals("015")||ta.GetText(1,2).equals("006"))){
                 String sqlb = "select count(servcasecode) from LHServCaseRela where servcasecode ='"
                               +mLHServCaseDefSet.get(i).getServCaseCode()+"' with ur";
                 SSRS tb = new ExeSQL().execSQL(sqlb);//该事件下关联客户总数
                 if(tb.GetText(1,1).equals("0")&&ta.GetText(1,2).equals("015")){
                     tmap.put("delete from LHServCaseDef where ServCaseCode='" +
                         mLHServCaseDefSet.get(i).getServCaseCode() + "' ",
                         "DELETE");
                 }
                  LHServCaseDefSchema tLHServCaseDefSchema = new LHServCaseDefSchema();
                  tLHServCaseDefSchema.setServCaseType("2");
                  String tdate = PubFun.getCurrentDate();
                  String sqlm = "select  max(substr(ServCaseCode,10,5))  from  LHServCaseDef where makedate='"
                                +tdate+"'";
                  String a = new ExeSQL().getOneValue(sqlm);
                  String servcasecode = "";//服务事件号码
                  if(a ==null || a==""){
                      servcasecode = tdate.replaceAll("-","")+"2"+"00001";
                  }else{
                      String b = (Integer.parseInt(a)+1)+"";
                      int xx = b.length();
                      for(int m = 0;m<5-xx;m++){
                          b="0"+b;
                      }
                      servcasecode = tdate.replaceAll("-","")+"2"+b;
                  }

                  String servcasename = "";//服务事件的名称
                  String sqln = "select servcasename from lhservcasedef where servcasecode='"+mLHServCaseDefSet.get(i).getServCaseCode()+"'";
                  String name = new ExeSQL().getOneValue(sqln);
                  if(ta.GetText(1,2).equals("015")){
                      int xx = name.indexOf("-", name.indexOf("-") + 1) + 1;
                      String dd = name.substring(xx, xx + 8);
                      String dd2 = "20" + dd.replaceAll("/", "-");
                      String dd3 = PubFun.calDate(dd2, 14, "D", null);
                      String dd4 = dd3.substring(2, 10).replaceAll("-", "/");
                      servcasename = name.replaceAll(dd, dd4);
                  }else if(ta.GetText(1,2).equals("006")){
                      String dd = name.substring(name.indexOf("/")+1,name.indexOf("/")+6);
                      String dd1 = "20"+dd.replaceAll("/","-")+"-01";
                      String dd3 = PubFun.calDate(dd1,12,"M",null);
                      String dd4 = dd3.substring(2,7).replaceAll("-","/");
                      servcasename = name.replaceAll(dd,dd4);
                      //System.out.println(servcasename);
                  }
                  tLHServCaseDefSchema.setServCaseCode(servcasecode);
                  tLHServCaseDefSchema.setServCaseName(servcasename);
                  tLHServCaseDefSchema.setServCaseState("0");
                  if(ta.GetText(1,2).equals("015")){
                      tLHServCaseDefSchema.setServCaseDes("015");
                  }else if(ta.GetText(1,2).equals("006")){
                      tLHServCaseDefSchema.setServCaseDes("006");
                  }
                  tLHServCaseDefSchema.setOperator(mGlobalInput.Operator);
                  tLHServCaseDefSchema.setManageCom(ta.GetText(1,3));//liuli  08/05/07 取之前的机构号
                  tLHServCaseDefSchema.setMakeDate(PubFun.getCurrentDate());
                  tLHServCaseDefSchema.setMakeTime(PubFun.getCurrentTime());
                  tLHServCaseDefSchema.setModifyDate(PubFun.getCurrentDate());
                  tLHServCaseDefSchema.setModifyTime(PubFun.getCurrentTime());

                 // tLHServCaseDefSet.add(tLHServCaseDefSchema);
                  tmap.put(tLHServCaseDefSchema,"INSERT");
                  VData tvdata = new VData();
                  tvdata.add(tmap);
                  if(!prepareOutputData(tvdata,"")){
                      return false;
                  }
            }
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     *
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true\
     * @return boolean
     * @param cInputData
     */
    private boolean getInputData(VData cInputData) {
        //this.mLHServCaseDefSchema.setSchema((LHServCaseDefSchema) cInputData.
        //getObjectByObjectName(
        // "LHServCaseDefSchema", 0));
        this.mLHServCaseDefSet.set((LHServCaseDefSet) cInputData.
                                   getObjectByObjectName("LHServCaseDefSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     * @return boolean
     */
    private boolean submitquery() {
        this.mResult.clear();
        LHServCaseDefDB tLHServCaseDefDB = new LHServCaseDefDB();
        // tLHServCaseDefDB.setSchema(this.mLHServCaseDefSchema);
        //如果有需要处理的错误，则返回
        if (tLHServCaseDefDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHServCaseDefDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHSettingSlipQueryBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 准备往后层处理所需要的数据
     * @return boolean
     */

    private boolean prepareOutputData(VData tVData,String tOperate) {
        try {
            System.out.println("Start LHSettingSlipQueryBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();

            if (!tPubSubmit.submitData(tVData, tOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                CError tError = new CError();
                tError.moduleName = "LHSettingSlipQueryBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }

            //mResult.add(this.mLHServItemSet);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHSettingSlipQueryBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 返回结果
     * @return VData
     */

    public VData getResult() {
        return this.mResult;
    }
}
