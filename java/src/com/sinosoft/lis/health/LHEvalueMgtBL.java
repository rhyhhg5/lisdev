/*
 * <p>ClassName: LHEvalueMgtBL </p>
 * <p>Description: LHEvalueMgtBL类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-10-16 14:31:38
 */
package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LHEvalueMgtBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String tNo;
    /** 业务处理相关变量 */
    private LHEvalueExpMainSet mLHEvalueExpMainSet = new LHEvalueExpMainSet();
//private LHEvalueExpMainSchema mLHEvalueExpMainSchema=new LHEvalueExpMainSchema();
    private LHEvalueExpDetailSet mLHEvalueExpDetailSet = new LHEvalueExpDetailSet();
    private TransferData mTransferData = new TransferData();
    public LHEvalueMgtBL() {
        try
        {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHEvalueMgtBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LHEvalueMgtBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("Start LHEvalueMgtBL Submit...");
            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                CError tError = new CError();
                tError.moduleName = "LHEvalueMgtBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }

            System.out.println("End LHEvalueMgtBL Submit...");
            //如果有需要处理的错误，则返回

        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {

        String MStartDate = (String) mTransferData.getValueByName("Begin");
        String MEndDate = (String) mTransferData.getValueByName("End");
        String temp_StandCode[][]; //健康问卷的标准代码
        int count_StandCode;//问卷录入表的标准代码的数据
        String temp_MediCode[][]; //体检药物名称将转换为相应的标准代码
        String temp_Result[][]; //问卷录入结果
        String mRegisNo="";//最大号
        String mMaxDate="";//最大日期

        if (this.mOperate.equals("INSERT||MAIN"))
        {

            for (int j = 1; j <= mLHEvalueExpMainSet.size(); j++)//中间表的主表信息
            {
                    //主表信息
                   ExeSQL tExeMaxDate = new ExeSQL();
                   String sqlMaxDate =" select max(a.SubmitDate) from LHQuesImportMain a"
                         +" where  a.QuesType='" + mLHEvalueExpMainSet.get(1).getEvalType()
                         + "'  and a.CustomerNo='" + mLHEvalueExpMainSet.get(j).getCustomerNo()
                         + "' and a.SubmitDate between '" + MStartDate + "'"+ " and  '" + MEndDate + "'"
                         +" and a.WhethAchieve in('2','3') "
                         ;
                   mMaxDate = tExeMaxDate.getOneValue(sqlMaxDate);//问卷录入表的最大日期
                   ExeSQL tExeRegisNo = new ExeSQL();
                   String sqlRegisNo =
                         "  select a.CusRegistNo from LHQuesImportMain a "
                         + "  where  a.QuesType='"+ mLHEvalueExpMainSet.get(1).getEvalType()
                         + "' and a.CustomerNo='" +mLHEvalueExpMainSet.get(j).getCustomerNo()
                         + "'  and a.SubmitDate  = '" + mMaxDate + "'"
                         +" and a.WhethAchieve in('2','3') "
                         ;
                   mRegisNo = tExeRegisNo.getOneValue(sqlRegisNo);//最大日期下的客户相关流水号
                   mLHEvalueExpMainSet.get(j).setCusEvalCode(PubFun1.CreateMaxNo("CusEvalCode", 20));
                   mLHEvalueExpMainSet.get(j).setServPlanNo(mRegisNo);//将流水号存入中问表的计划号中
                   mLHEvalueExpMainSet.get(j).setEvalDate(PubFun.getCurrentDate());
                   mLHEvalueExpMainSet.get(j).setEvalTime(PubFun.getCurrentTime());
                   mLHEvalueExpMainSet.get(j).setOperator(mGlobalInput.Operator);
                   mLHEvalueExpMainSet.get(j).setMakeDate(PubFun.getCurrentDate());
                   mLHEvalueExpMainSet.get(j).setMakeTime(PubFun.getCurrentTime());
                   mLHEvalueExpMainSet.get(j).setModifyDate(PubFun.getCurrentDate());
                   mLHEvalueExpMainSet.get(j).setModifyTime(PubFun.getCurrentTime());
                   mLHEvalueExpMainSet.get(j).setManageCom(mGlobalInput.ManageCom);
                   //明细表信息
                   SSRS tSSRS_StandCode = new SSRS();
                   ExeSQL tExeStandCode = new ExeSQL();
                   String sqlStandCode =
                           " select b.StandCode from LHQuesImportDetail b "
                           + " where b.CusRegistNo='"+mRegisNo+"' and (b.Quesstatus is null or Quesstatus ='')"
                           ;
                   tSSRS_StandCode = tExeStandCode.execSQL(sqlStandCode);
                   temp_StandCode = tSSRS_StandCode.getAllData();
                   count_StandCode = tSSRS_StandCode.getMaxRow();//问卷录入相关的标准代码
                   for (int m = 1; m <= count_StandCode; m++)
                   { //查询流水号对应的标准代码
                       LHEvalueExpDetailSchema tLHEvalueExpDetailSchema = new LHEvalueExpDetailSchema();
                       tLHEvalueExpDetailSchema.setEvalExportNo(PubFun1.CreateMaxNo("EvalExportNo", 20));
                       tLHEvalueExpDetailSchema.setCusEvalCode(mLHEvalueExpMainSet.get(j).getCusEvalCode());
                       tLHEvalueExpDetailSchema.setStandCode(temp_StandCode[m-1][0]);//标准代码
                       SSRS tSSRS_Result = new SSRS();
                       ExeSQL tExeResultSQL = new ExeSQL();
                       String sqlResult =
                               " select a.LetterResult, a.QuesStatus from LHQuesImportDetail  a"
                               + " where  a.CusRegistNo='" +mRegisNo + "'"
                               + " and a.StandCode='" +temp_StandCode[m - 1][0] + "'";
                       tSSRS_Result = tExeResultSQL.execSQL(sqlResult);
                       temp_Result = tSSRS_Result.getAllData();
                       tLHEvalueExpDetailSchema.setLetterResult(temp_Result[0][0]);//问卷录入表的标准代码对应的问问题结果
                       tLHEvalueExpDetailSchema.setQuesStatus(temp_Result[0][1]);//问卷录入表的标准代对对应的问题状态
                       tLHEvalueExpDetailSchema.setOperator(mGlobalInput.Operator);
                       tLHEvalueExpDetailSchema.setMakeDate(PubFun.getCurrentDate());
                       tLHEvalueExpDetailSchema.setMakeTime(PubFun.getCurrentTime());
                       tLHEvalueExpDetailSchema.setModifyDate(PubFun.getCurrentDate());
                       tLHEvalueExpDetailSchema.setModifyTime(PubFun.getCurrentTime());
                       tLHEvalueExpDetailSchema.setManageCom(mGlobalInput.ManageCom);
                       mLHEvalueExpDetailSet.add(tLHEvalueExpDetailSchema);
                   }
               }//中间表的主表信息的FOR循环
               map.put(mLHEvalueExpMainSet, "INSERT");
               map.put(mLHEvalueExpDetailSet, "INSERT");

              for (int k= 1; k<= mLHEvalueExpMainSet.size(); k++)//中间表的主表信息
              {
                  ExeSQL tExeWhethAchie = new ExeSQL();
                  String sqlWhethAchie =" select a.WhethAchieve  from LHQuesImportMain a "
                                        + " where a.CusRegistNo='"+ mLHEvalueExpMainSet.get(k).getServPlanNo() + "'"
                                        ;
                  String mWhethAchie = tExeWhethAchie.getOneValue(sqlWhethAchie);//查询客户流水号对应的问卷录入状态
                  if (mWhethAchie.equals("3") || mWhethAchie.equals("2"))//在完成的问卷录入表中问题状态没有填写时
                  {
                       ExeSQL tExeMaxComDate = new ExeSQL();
                       String sqlMaxComDate = " select max(a.TestDate) from LHCustomTest a"
                                              + " where  a.TestDate between '" + MStartDate + "' and  '" + MEndDate +
                                              "' and a.CustomerNo='" +mLHEvalueExpMainSet.get(k).getCustomerNo() + "'";
                       String mMaxComDate = tExeMaxComDate.getOneValue(sqlMaxComDate);//查询客户体检表中对应的最最大日期
                       if (mMaxComDate.equals("") || mMaxComDate.equals("null") || mMaxComDate == null)
                       {
                           if (mWhethAchie.equals("3"))
                           {
                               System.out.println("问卷录入体检信息和健康档案体检信息都不存在，无法导出");
                               CError.buildErr(this,"问卷录入体检信息和健康档案体检信息都不存在，无法导出");
                               //return false;
                           } //体检最大日期不为空的结束标记
                           if (mWhethAchie.equals("2"))
                           {
                               System.out.println("无需导入体检信息");
                               //return true;
                           }
                       }
                       //System.out.println("查询的问卷录入相关的客户体检日期 "+mMaxComDate);
                       if (!mMaxComDate.equals("") && !mMaxComDate.equals("null") && !mMaxComDate.equals( null))
                       {
                       ExeSQL tExeMaxComPDate = new ExeSQL();
                       String sqlMaxComPDate =
                               " select case when TO_DATE('" +mMaxDate +"') > TO_DATE( '" +mMaxComDate + "') "
                               + " then '2' when TO_DATE('" +mMaxDate +"') < TO_DATE( '" +mMaxComDate + "') "
                               + " then '3' else '' end  from dual ";
                       String mMaxComPDate = tExeMaxComPDate.getOneValue(sqlMaxComPDate);//查询客户体检表中对应的最最大日期
                       if (mMaxComPDate.equals("3"))//当体检日期大时
                       {
                            String standcode[]={"05010001","05010002","05010003","05010004","05010005","05010006","05010007","05010015","05010008","05010016","05010009","05010017","05010010","05010018","05010011","05010019","05010012","05010020","05010013","05010021","05010014"};
                           for (int p= 1; p<= standcode.length; p++)
                           {
                            map.put(" update LHEvalueExpDetail a set  a.LetterResult='0', a.ManageCom='"+mGlobalInput.ManageCom
                                    +"', a.Operator='"+mGlobalInput.Operator+"', a.ModifyDate='"+PubFun.getCurrentDate()
                                    +"',a.ModifyTime='"+PubFun.getCurrentTime()  +
                                    "' where  a.StandCode ='"+standcode[p-1]+"' and a.CusEvalCode='"
                                    +mLHEvalueExpMainSet.get(k).getCusEvalCode()+"'  ","UPDATE");//身高
                           }
                           SSRS tSSRS_MediCode = new SSRS(); //根据最大日期和客户号码找出对应的医药代码和检查结果
                           ExeSQL tExeMediCode = new ExeSQL();
                           //必须保证一名客户一天只做一次体检记录
                           //找到该客户需要的一次体检信息
                           //有错！必须将所有问卷涉及的体检信息update
                           String sqlMediCode =" select a.MedicaItemCode,a.TestResult,inhospitno from LHCustomTest a"
                                          + " where  a.TestDate ='" + mMaxComDate + "' "
                                          + " and a.CustomerNo='" + mLHEvalueExpMainSet.get(k).getCustomerNo() +
                                          "'";
                            tSSRS_MediCode = tExeMediCode.execSQL(sqlMediCode);
                            int count_MediCode = tSSRS_MediCode.getMaxRow();
                            temp_MediCode = tSSRS_MediCode.getAllData();
                            for (int v = 0; v < count_MediCode; v++)
                            { //查询问卷录入表中相关的最大时间
                                    ExeSQL tExeResuAffiInfo = new ExeSQL();
                                    String sqlResuAffiInfo = " select a.StandCode from LHManaStandPro a "
                                                + " where  a.ResuAffiInfo ='" + temp_MediCode[v][0] + "'";
                                    String mResuAffiInfo = tExeResuAffiInfo.getOneValue(sqlResuAffiInfo); //从问卷标准化管理表中查询出相关附属信息对应的标准代码
                                    map.put("update LHEvalueExpDetail a set  a.LetterResult='"+temp_MediCode[v][1]+"', a.FileInfoCode = '"+temp_MediCode[v][2]
                                                      +"', a.ManageCom='"+mGlobalInput.ManageCom+"', a.Operator='"+mGlobalInput.Operator
                                                      +"', a.ModifyDate='"+PubFun.getCurrentDate()+"',a.ModifyTime='"+PubFun.getCurrentTime()
                                                      +"'  where a.CusEvalCode='"+mLHEvalueExpMainSet.get(k).getCusEvalCode()+"'  and a.StandCode='"+mResuAffiInfo+"' ", "UPDATE");
                            }
                       }//比较完问卷日期和体检日期，当体检日期大时问卷录入状态为3时的结束标记
                       }
                   }//对保存完成的中间表的信息问题状态为3时的结束标记
              }
           }//INSERT||MAIN
           if (this.mOperate.equals("UPDATE||MAIN")) {}
           if (this.mOperate.equals("DELETE||MAIN")) {}
           return true;
       }

       /**
        * 根据前面的输入数据，进行BL逻辑处理
        * 如果在处理过程中出错，则返回false,否则返回true
        */
       private boolean updateData()
       {
           return true;
       }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLHEvalueExpMainSet.set((LHEvalueExpMainSet) cInputData.getObjectByObjectName("LHEvalueExpMainSet",0));
        if (mLHEvalueExpMainSet == null || mLHEvalueExpMainSet.size() < 0)
        {
            CError.buildErr(this,"没有得到问卷信息！");
            return false;
        }

        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        mTransferData = ((TransferData) cInputData.getObjectByObjectName("TransferData", 0));
        if (mTransferData == null)
        {
            CError.buildErr(this,"没有得到足够的信息！");
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        LHEvalueExpMainDB tLHEvalueExpMainDB = new LHEvalueExpMainDB();
        //如果有需要处理的错误，则返回
        if (tLHEvalueExpMainDB.mErrors.needDealError())
        {   // @@错误处理
            this.mErrors.copyAllErrors(tLHEvalueExpMainDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHEvalueMgtBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            this.mInputData.add(this.mLHEvalueExpMainSet);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLHEvalueExpMainSet);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHEvalueMgtBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void jbInit() throws Exception {
    }
}
