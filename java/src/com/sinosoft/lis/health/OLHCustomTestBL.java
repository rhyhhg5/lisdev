/*
 * <p>ClassName: OLHCustomTestBL </p>
 * <p>Description: OLHCustomTestBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-17 18:32:42
 */
package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLHCustomTestBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String tNo;

    /** 业务处理相关变量 */
    private LHCustomTestSchema mLHCustomTestSchema = new LHCustomTestSchema();
    private LHCustomInHospitalSchema mLHCustomInHospitalSchema = new
            LHCustomInHospitalSchema();
    private LCPENoticeResultSet mLCPENoticeResultSet = new LCPENoticeResultSet();
    private LHCustomTestSet mLHCustomTestSet = new LHCustomTestSet();
    //private LHFeeInfoSchema mLHFeeInfoSchema = new LHFeeInfoSchema();
//private LHCustomTestSet mLHCustomTestSet=new LHCustomTestSet();
    public OLHCustomTestBL() {
    }

    public static void main(String[] args) {

        GlobalInput tG = new GlobalInput();
        tG.ManageCom = "8600";
        tG.Operator = "001";
        tG.ComCode = "8600";

        LHCustomInHospitalSchema tLHCustomInHospitalSchema = new
                LHCustomInHospitalSchema();
        //OLHCustomInHospitalUI tOLHCustomInHospitalUI   = new OLHCustomInHospitalUI();
        OLHCustomTestUI tOLHCustomTestUI = new OLHCustomTestUI();
        LHCustomTestSet tLHCustomTestSet = new LHCustomTestSet();
       // LHFeeInfoSchema tLHFeeInfoSchema = new LHFeeInfoSchema();
        //输出参数
        CErrors tError = null;
        String tRela = "";
        String FlagStr = "";
        String Content = "";
        String transact = "DELETE||MAIN";

        System.out.println("ddddddddddddddd");
        //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录


        tLHCustomInHospitalSchema.setCustomerNo("000000146");
        tLHCustomInHospitalSchema.setIsPhysicalExam("1");
        tLHCustomInHospitalSchema.setInHospitNo("1");
        tLHCustomInHospitalSchema.setInHospitMode("TestModeCode");
        tLHCustomInHospitalSchema.setInHospitDate("InHospitDate");
        tLHCustomInHospitalSchema.setHospitCode("HospitCode");

        tLHCustomInHospitalSchema.setOperator("Operator");
        tLHCustomInHospitalSchema.setMakeDate("MakeDate");
        tLHCustomInHospitalSchema.setMakeTime("MakeTime");
        tLHCustomInHospitalSchema.setModifyDate("ModifyDate");
        tLHCustomInHospitalSchema.setModifyTime("ModifyTime");

        //tLHFeeInfoSchema.setCustomerNo("CustomerNo");
        //tLHFeeInfoSchema.setFeeCode("6");
        //tLHFeeInfoSchema.setFeeAmount("9");
       // tLHFeeInfoSchema.setInHospitNo("InHospitNo");
        //tLHFeeInfoSchema.setFeeNo("FeeNo");

       // tLHFeeInfoSchema.setOperator("Operator");
       // tLHFeeInfoSchema.setMakeDate("MakeDate");
       // tLHFeeInfoSchema.setMakeTime("MakeTime");
       // tLHFeeInfoSchema.setModifyDate("ModifyDate");
       // tLHFeeInfoSchema.setModifyTime("ModifyTime");

        System.out.println("---------normal get----------");
        for (int i = 0; i < 3; i++) {
            LHCustomTestSchema tLHCustomTestSchema = new LHCustomTestSchema();
//           tLHCustomTestSchema.setContraNo("343432");
//           tLHCustomTestSchema.setContraItemNo("34");
//           tLHCustomTestSchema.setDutyItemCode("01010");
//           tLHCustomTestSchema.setDutyState("1");
//           tLHCustomTestSchema.setDutyExolai("2dfesdf");
//           tLHCustomTestSchema.setFeeExplai("dfdfesadsfa");

            tLHCustomTestSchema.setOperator("hm");
            tLHCustomTestSchema.setMakeDate("2004-05-05");
            tLHCustomTestSchema.setMakeTime("2004-05-05");
            tLHCustomTestSchema.setModifyDate("2004-05-05");
            tLHCustomTestSchema.setModifyTime("2004-05-05");

            tLHCustomTestSet.add(tLHCustomTestSchema);
        }
        System.out.println("***********end get data **************");

        //  System.out.println("############savepage ready");

        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(tLHCustomInHospitalSchema);
        //tVData.add(tLHFeeInfoSchema);
        System.out.println("set fee info " + tVData);
        tVData.add(tLHCustomTestSet);
        tVData.add(tG);

        tOLHCustomTestUI.submitData(tVData, transact);

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            System.out.println("*********getInputData************");
            return false;
        }
        //进行业务处理

        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLHCustomTestBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLHCustomTestBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("**********after dealData***********");

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }

        System.out.println("*******after prepareOutputData*******");
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            PubSubmit tPubSubmit = new PubSubmit();
//            tPubSubmit.submitData(mInputData, mOperate);
            System.out.println("-----------Pub Submit Data----------");

            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                CError tError = new CError();
                tError.moduleName = "OLHCustomTestBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }
        }

        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        if (this.mOperate.equals("INSERT||MAIN")) {
            try {
                SSRS tSSRS = new SSRS();
                String sql = "Select Case When max(to_number(InHospitNo)) Is Null Then 0 Else max(to_number(InHospitNo)) End from LHCustomInHospital where CustomerNo='"
                             + mLHCustomInHospitalSchema.getCustomerNo() +
                             "'";
                ExeSQL tExeSQL = new ExeSQL();
                tSSRS = tExeSQL.execSQL(sql);
                Integer firstinteger = Integer.valueOf(tSSRS.GetText(1, 1));
                int ttNo = firstinteger.intValue() + 1;
                Integer integer = new Integer(ttNo);
                tNo = integer.toString();
                System.out.println("得到的InHospitNo是：" + tNo);
                mLHCustomInHospitalSchema.setInHospitNo(tNo);

            } catch (Exception e) {
                CError tError = new CError();
                tError.moduleName = "OLHCustomInHospitalBL";
                tError.functionName = "createAddressNo";
                tError.errorMessage = "地址码超长,生成InHospitNo失败,请先删除原来的超长地址码!";
                this.mErrors.addOneError(tError);
                mLHCustomInHospitalSchema.setInHospitNo("");
            }

            mLHCustomInHospitalSchema.setOperator(mGlobalInput.Operator);
            mLHCustomInHospitalSchema.setMakeDate(PubFun.getCurrentDate());
            mLHCustomInHospitalSchema.setMakeTime(PubFun.getCurrentTime());
            mLHCustomInHospitalSchema.setModifyDate(PubFun.getCurrentDate());
            mLHCustomInHospitalSchema.setModifyTime(PubFun.getCurrentTime());
            mLHCustomInHospitalSchema.setManageCom(mGlobalInput.ManageCom);
            map.put(mLHCustomInHospitalSchema, "INSERT");

            try {
                SSRS tSSRS = new SSRS();
                String sql = "Select Case When max(to_number(FeeNo)) Is Null Then 0 Else max(to_number(FeeNo)) End from LHFeeInfo where CustomerNo='"
                             +
                             mLHCustomInHospitalSchema.getCustomerNo() +
                             "' and InHospitNo='" +
                             mLHCustomInHospitalSchema.getInHospitNo() +
                             "' ";
                ExeSQL tExeSQL = new ExeSQL();
                tSSRS = tExeSQL.execSQL(sql);
                Integer firstinteger = Integer.valueOf(tSSRS.
                        GetText(1, 1));
                int ttNo = firstinteger.intValue() + 1;
                Integer integer = new Integer(ttNo);
                tNo = integer.toString();
                System.out.println("得到的OtherCureNo是：" + tNo);
                //mLHFeeInfoSchema.setFeeNo(tNo);

            } catch (Exception e) {
                CError tError = new CError();
                tError.moduleName = "OLHCustomInHospitalBL";
                tError.functionName = "createAddressNo";
                tError.errorMessage =
                        "地址码超长,生成OtherCureNo失败,请先删除原来的超长地址码!";
                this.mErrors.addOneError(tError);
                //mLHFeeInfoSchema.setFeeNo("");
            }

           // mLHFeeInfoSchema.setInHospitNo(mLHCustomInHospitalSchema.
                                         //  getInHospitNo());
           // mLHFeeInfoSchema.setOperator(mGlobalInput.Operator);
           // mLHFeeInfoSchema.setMakeDate(PubFun.getCurrentDate());
          //  mLHFeeInfoSchema.setMakeTime(PubFun.getCurrentTime());
          //  mLHFeeInfoSchema.setModifyDate(PubFun.getCurrentDate());
          //  mLHFeeInfoSchema.setModifyTime(PubFun.getCurrentTime());
         //   mLHFeeInfoSchema.setManageCom(mGlobalInput.ManageCom);

          //  map.put(mLHFeeInfoSchema, "INSERT");

            if (mLHCustomTestSet != null && mLHCustomTestSet.size() > 0) {
                int j_TestNo = 1;
                for (int i = 1; i <= mLHCustomTestSet.size(); i++) {
                    mLHCustomTestSet.get(i).setInHospitNo(
                            mLHCustomInHospitalSchema.getInHospitNo());
                    if (mLHCustomTestSet.get(i).getTestNo() == null ||
                        mLHCustomTestSet.get(i).getTestNo().equals("")) {
                        try {
                            SSRS tSSRS = new SSRS();
                            String sql = "Select Case When max(to_number(TestNo)) Is Null Then 0 Else max(to_number(TestNo)) End from LHCustomTest where CustomerNo='"
                                         +mLHCustomInHospitalSchema.getCustomerNo() +
                                         "' and InHospitNo='" + mLHCustomInHospitalSchema.getInHospitNo() +
                                         "' ";
                            ExeSQL tExeSQL = new ExeSQL();
                            tSSRS = tExeSQL.execSQL(sql);
                            Integer firstinteger = Integer.valueOf(tSSRS.GetText(1, 1));
                            int ttNo = firstinteger.intValue() + (j_TestNo++);
                            Integer integer = new Integer(ttNo);
                            tNo = integer.toString();
                            System.out.println("得到的TestNo是：" + tNo);
                            mLHCustomTestSet.get(i).setTestNo(tNo);
                        } catch (Exception e) {
                            CError tError = new CError();
                            tError.moduleName = "OLHCustomInHospitalBL";
                            tError.functionName = "createAddressNo";
                            tError.errorMessage = "地址码超长,生成TestNo失败,请先删除原来的超长地址码!";
                            this.mErrors.addOneError(tError);
                            mLHCustomTestSet.get(i).setTestNo("");
                        }
                    }
                    mLHCustomTestSet.get(i).setOperator(mGlobalInput.Operator);
                    mLHCustomTestSet.get(i).setMakeDate(PubFun.getCurrentDate());
                    mLHCustomTestSet.get(i).setMakeTime(PubFun.getCurrentTime());
                    mLHCustomTestSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLHCustomTestSet.get(i).setModifyTime(PubFun.getCurrentTime());
                    mLHCustomTestSet.get(i).setManageCom(mGlobalInput.ManageCom);
                }
                map.put(mLHCustomTestSet, "INSERT");
            }

            //体检结果部分
            if (mLCPENoticeResultSet != null && mLCPENoticeResultSet.size() > 0) {
                //先得到InhospitNo，并将其装入Set
                try {
                    SSRS tSSRS = new SSRS();
                    String sql = "Select Case When max(to_number(InhospitNo)) Is Null Then 1 Else max(to_number(InhospitNo))+1 End from LHCustomTest where CustomerNo='"
                                 + mLHCustomInHospitalSchema.getCustomerNo() +
                                 "' ";
                    ExeSQL tExeSQL = new ExeSQL();
                    tSSRS = tExeSQL.execSQL(sql);
                    String tInNo = tSSRS.GetText(1, 1);
                    System.out.println("LCPENoticeResult得到的InhospitNo是：" +
                                       tInNo);
                    for (int t = 1; t <= mLCPENoticeResultSet.size(); t++) {
                        mLCPENoticeResultSet.get(t).setPrtSeq(tInNo);
                    }
                } catch (Exception e) {
                    CError tError = new CError();
                    tError.moduleName = "OLHCustomTestBL";
                    tError.functionName = "createInhospitNo";
                    tError.errorMessage =
                            "生成InhospitNo失败!";
                    this.mErrors.addOneError(tError);
                }
                for (int i = 1; i <= mLCPENoticeResultSet.size(); i++) {
                    try {
                        SSRS tSSRS = new SSRS();
                        String sql = "Select Case When max(to_number(TestNo)) Is Null Then 1 Else max(to_number(TestNo))+1 End from LHCustomTest where CustomerNo='"
                                     + mLHCustomInHospitalSchema.getCustomerNo() +
                                     "'  and InHospitNo='" +mLHCustomInHospitalSchema.getInHospitNo() +
                                     "' ";
                        ExeSQL tExeSQL = new ExeSQL();
                        tSSRS = tExeSQL.execSQL(sql);
                        Integer firstinteger = Integer.valueOf(tSSRS.
                                GetText(1, 1));
                        int ttNo = firstinteger.intValue() + i - 1;
                        Integer integer = new Integer(ttNo);
                        tNo = integer.toString();
                        mLCPENoticeResultSet.get(i).setSerialNo(tNo);
                        System.out.println(
                                "mLCPENoticeResultSet.get(i).getSerialNo()" +
                                mLCPENoticeResultSet.get(i).getSerialNo());
                    } catch (Exception e) {
                        CError tError = new CError();
                        tError.moduleName = "OLHCustomInHospitalBL";
                        tError.functionName = "createAddressNo";
                        tError.errorMessage = "地址码超长,生成TestNo失败,请先删除原来的超长地址码!";
                        this.mErrors.addOneError(tError);
                    }
                    mLCPENoticeResultSet.get(i).setContNo("0");
                    mLCPENoticeResultSet.get(i).setCustomerNo(
                            mLHCustomInHospitalSchema.getCustomerNo());
                    mLCPENoticeResultSet.get(i).setName("0");
                    mLCPENoticeResultSet.get(i).setOperator(mGlobalInput.
                            Operator);
                    mLCPENoticeResultSet.get(i).setDisDesb("0");
                    mLCPENoticeResultSet.get(i).setDisResult("0");
                    mLCPENoticeResultSet.get(i).setMakeDate(PubFun.getCurrentDate());
                    mLCPENoticeResultSet.get(i).setMakeTime(PubFun.getCurrentTime());
                    mLCPENoticeResultSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLCPENoticeResultSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
                map.put(mLCPENoticeResultSet, "INSERT");
            }
        }

        if (this.mOperate.equals("UPDATE||MAIN")) {

            mLHCustomInHospitalSchema.setOperator(mGlobalInput.Operator);
            mLHCustomInHospitalSchema.setModifyDate(PubFun.getCurrentDate());
            mLHCustomInHospitalSchema.setModifyTime(PubFun.getCurrentTime());
            mLHCustomInHospitalSchema.setManageCom(mGlobalInput.ManageCom);
            map.put(mLHCustomInHospitalSchema, "UPDATE");

//            if (mLHCustomTestSet != null && mLHCustomTestSet.size() > 0) {
                int j_TestNo = 1;
                for (int i = 1; i <= mLHCustomTestSet.size(); i++) {
                    System.out.println(mLHCustomTestSet.get(i).getTestNo());
                    if (mLHCustomTestSet.get(i).getTestNo() == null ||
                        mLHCustomTestSet.get(i).getTestNo().equals(""))
                    {
                        try
                        {
                            SSRS tSSRS = new SSRS();
                            String sql = "Select Case When max(to_number(TestNo)) Is Null Then 0 Else max(to_number(TestNo)) End from LHCustomTest where CustomerNo='"
                                         +mLHCustomInHospitalSchema.getCustomerNo() +"' and InHospitNo='" + mLHCustomInHospitalSchema.getInHospitNo() + "' "
                                         ;
                            ExeSQL tExeSQL = new ExeSQL();
                            tSSRS = tExeSQL.execSQL(sql);
                            Integer firstinteger = Integer.valueOf(tSSRS.GetText(1, 1));
                            int ttNo = firstinteger.intValue() + (j_TestNo++);
                            Integer integer = new Integer(ttNo);
                            tNo = integer.toString();
                            System.out.println("得到的TestNo是：" + tNo);
                            mLHCustomTestSet.get(i).setTestNo(tNo);
                        } catch (Exception e)
                        {
                            CError tError = new CError();
                            tError.moduleName = "OLHCustomInHospitalBL";
                            tError.functionName = "createAddressNo";
                            tError.errorMessage = "地址码超长,生成TestNo失败,请先删除原来的超长地址码!";
                            this.mErrors.addOneError(tError);
                            mLHCustomTestSet.get(i).setTestNo("");
                        }
                    }
                    mLHCustomTestSet.get(i).setOperator(mGlobalInput.Operator);
                    mLHCustomTestSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLHCustomTestSet.get(i).setModifyTime(PubFun.getCurrentTime());
                    mLHCustomTestSet.get(i).setManageCom(mGlobalInput.ManageCom);
                }
                map.put("DELETE FROM LHCustomtest where CustomerNo = '" + mLHCustomInHospitalSchema.getCustomerNo() +
                   "' and InhospitNo = '" + mLHCustomInHospitalSchema.getInHospitNo() + "' ", "DELETE");
                map.put(mLHCustomTestSet, "INSERT");
//            }

           // map.put("UPDATE LHFeeInfo set FeeAmount = "+mLHFeeInfoSchema.getFeeAmount()
                //    +", ModifyDate = '"+PubFun.getCurrentDate()
                //    +"', ModifyTime = '"+PubFun.getCurrentTime()
                //    +"' where customerno = '"+mLHCustomInHospitalSchema.getCustomerNo()
               //     +"' and InhospitNo = '" +mLHCustomInHospitalSchema.getInHospitNo()+"'" ,"UPDATE");

            if (mLCPENoticeResultSet != null && mLCPENoticeResultSet.size() > 0) {
                for (int i = 1; i <= mLCPENoticeResultSet.size(); i++) {
                    try {
                        SSRS tSSRS = new SSRS();
                        String sql = "Select Case When max(to_number(TestNo)) Is Null Then 1 Else max(to_number(TestNo))+1 End from LHCustomTest where CustomerNo='"
                                     + mLHCustomInHospitalSchema.getCustomerNo() +
                                     "'  and InHospitNo='" +
                                     mLHCustomInHospitalSchema.getInHospitNo() +
                                     "' ";
                        ExeSQL tExeSQL = new ExeSQL();
                        tSSRS = tExeSQL.execSQL(sql);
                        Integer firstinteger = Integer.valueOf(tSSRS.
                                GetText(1, 1));
                        int ttNo = firstinteger.intValue() + i - 1;
                        Integer integer = new Integer(ttNo);
                        tNo = integer.toString();
                        System.out.println("tNo:" + tNo);
                        mLCPENoticeResultSet.get(i).setSerialNo(tNo);
                        mLCPENoticeResultSet.get(i).setPrtSeq(
                                mLHCustomInHospitalSchema.getInHospitNo());
                        System.out.println(
                                "mLCPENoticeResultSet.get(i).getSerialNo()" +
                                mLCPENoticeResultSet.get(i).getSerialNo());
                    } catch (Exception e) {
                        CError tError = new CError();
                        tError.moduleName = "OLHCustomInHospitalBL";
                        tError.functionName = "createAddressNo";
                        tError.errorMessage =
                                "地址码超长,生成TestNo失败,请先删除原来的超长地址码!";
                        this.mErrors.addOneError(tError);
//                        mLCPENoticeResultSet.get(i).setSerialNo("");
                    }
                    mLCPENoticeResultSet.get(i).setContNo("0");
                    mLCPENoticeResultSet.get(i).setCustomerNo(
                            mLHCustomInHospitalSchema.getCustomerNo());
                    mLCPENoticeResultSet.get(i).setName("0");
                    mLCPENoticeResultSet.get(i).setOperator(mGlobalInput.
                            Operator);
                    mLCPENoticeResultSet.get(i).setDisDesb("0");
                    mLCPENoticeResultSet.get(i).setDisResult("0");
                    mLCPENoticeResultSet.get(i).setMakeDate(PubFun.
                            getCurrentDate());
                    mLCPENoticeResultSet.get(i).setMakeTime(PubFun.
                            getCurrentTime());
                    mLCPENoticeResultSet.get(i).setModifyDate(PubFun.
                            getCurrentDate());
                    mLCPENoticeResultSet.get(i).setModifyTime(PubFun.
                            getCurrentTime());
                }
            }
            map.put("DELETE FROM LCPENoticeResult where ProposalContno = '" +
                    mLHCustomInHospitalSchema.getCustomerNo() +
                    "' and Prtseq = '" +
                    mLHCustomInHospitalSchema.getInHospitNo() +
                    "' and SortDepart = '02'", "DELETE");
            map.put(mLCPENoticeResultSet, "INSERT");
        }

        if (this.mOperate.equals("DELETE||MAIN")) {
            map.put("DELETE FROM LHCUSTOMTEST WHERE CUSTOMERNO = '" +
                    mLHCustomInHospitalSchema.getCustomerNo() +
                    "' AND INHOSPITNO = '" +
                    mLHCustomInHospitalSchema.getInHospitNo() + "'", "DELETE");
            map.put("DELETE FROM LHCUSTOMINHOSPITAL WHERE CUSTOMERNO = '" +
                    mLHCustomInHospitalSchema.getCustomerNo() +
                    "' AND INHOSPITNO = '" +
                    mLHCustomInHospitalSchema.getInHospitNo() + "'", "DELETE");
            map.put("DELETE FROM LHFEEINFO WHERE CUSTOMERNO = '" +
                    mLHCustomInHospitalSchema.getCustomerNo() +
                    "' AND INHOSPITNO = '" +
                    mLHCustomInHospitalSchema.getInHospitNo() + "'", "DELETE");
            map.put("DELETE FROM LCPENoticeResult where ProposalContno = '" +
                     mLHCustomInHospitalSchema.getCustomerNo() +
                     "' and Prtseq = '" +
                     mLHCustomInHospitalSchema.getInHospitNo() +
                     "' and SortDepart = '02'", "DELETE");


            System.out.println("___________________________________" +
                               mLHCustomInHospitalSchema.getCustomerNo() +
                               mLHCustomInHospitalSchema.getInHospitNo());
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
//         this.mLHCustomTestSchema.setSchema((LHCustomTestSchema)cInputData.getObjectByObjectName("LHCustomTestSchema",0));
        this.mLHCustomInHospitalSchema.setSchema((LHCustomInHospitalSchema)
                                                 cInputData.
                                                 getObjectByObjectName(
                "LHCustomInHospitalSchema", 0));

    if((LHCustomTestSet)cInputData.getObjectByObjectName("LHCustomTestSet", 0) != null)
    {    this.mLHCustomTestSet.set((LHCustomTestSet) cInputData.
                                  getObjectByObjectName("LHCustomTestSet", 0));
    }
    else{System.out.println("LHCustomTestSet is null");}

    //if((LHFeeInfoSchema) cInputData.getObjectByObjectName("LHFeeInfoSchema",0)!=null)
   // {
       // this.mLHFeeInfoSchema.setSchema((LHFeeInfoSchema) cInputData.
                                      //  getObjectByObjectName("LHFeeInfoSchema",
          //      0));
   // }

        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        this.mLCPENoticeResultSet.set((LCPENoticeResultSet) cInputData.
                                      getObjectByObjectName(
                                              "LCPENoticeResultSet", 0));

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LHCustomTestDB tLHCustomTestDB = new LHCustomTestDB();
        tLHCustomTestDB.setSchema(this.mLHCustomTestSchema);
        //如果有需要处理的错误，则返回
        if (tLHCustomTestDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHCustomTestDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHCustomTestBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLHCustomInHospitalSchema);
            this.mInputData.add(this.mLHCustomTestSet);
           // this.mInputData.add(this.mLHFeeInfoSchema);
            this.mInputData.add(this.mLCPENoticeResultSet);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLHCustomTestSchema);
            mResult.add(this.mLHCustomTestSet);
           // mResult.add(this.mLHFeeInfoSchema);
            mResult.add(this.mLHCustomInHospitalSchema);
            mResult.add(this.mLCPENoticeResultSet);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHCustomTestBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
