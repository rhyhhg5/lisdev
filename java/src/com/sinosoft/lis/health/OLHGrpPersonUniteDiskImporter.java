package com.sinosoft.lis.health;

import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SchemaSet;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.diskimport.*;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LHGrpPersonUniteSchema;
import com.sinosoft.utility.*;


public class OLHGrpPersonUniteDiskImporter {
    public CErrors mErrrors = new CErrors();

    /** 磁盘导入临时表的schema */
    private static String GrpServPlanNo;
    private static final String schemaClassName =
            "com.sinosoft.lis.schema.LHGrpPersonUniteSchema";

    /** 磁盘导入临时表的set */
    private static final String schemaSetClassName =
            "com.sinosoft.lis.vschema.LHGrpPersonUniteSet";

    /** 使用默认的导入方式 */
    private DefaultDiskImporter importer = null;

    //**数据操作字符串*/
    private static String mOperate = "INSERT||MAIN";

    //**往后面传输数据的容器*/
    VData mInputData = new VData();
    private MMap map = new MMap();
    /**
     * 构造函数
     * @param fileName String
     * @param configFileName String
     * @param sheetName String
     */
    public OLHGrpPersonUniteDiskImporter(String fileName, String configFileName,
                                         String GrpServPlanNo,
                                         String sheetName) {
        this.GrpServPlanNo = GrpServPlanNo;
        importer = new DefaultDiskImporter(fileName, configFileName, sheetName);
    }

    /**
     * 执行导入
     * @return boolean
     */
    public boolean doImport() {
        importer.setSchemaClassName(schemaClassName);
        importer.setSchemaSetClassName(schemaSetClassName);
        if (!importer.doImport()) {
            mErrrors.copyAllErrors(importer.mErrors);
            return false;
        }
        return true;
    }

    /**
     * 得到导入结果
     * @return SchemaSet
     */
    public SchemaSet getSchemaSet() {
        return importer.getSchemaSet();
    }

    /**
     * JSP倒入接口
     * @return boolean
     */
    public boolean submitImport() {
        this.doImport();
        LHGrpPersonUniteSet mLHGrpPersonUniteSet = (LHGrpPersonUniteSet)
                importer.getSchemaSet();
        LHGrpPersonUniteSet dbLHGrpPersonUniteSet = new LHGrpPersonUniteSet();
        System.out.println(
                "------------mLHGrpPersonUniteSet.size:" +
                mLHGrpPersonUniteSet.size() + "-----------");
        SSRS tSSRS = null;
        ExeSQL tExeSQL = new ExeSQL();
        String sql =
                "select distinct ServPlanLevel,GrpServPlanNo from LHGrpServPlan where GrpContNo = (select distinct GrpContNo from LHGrpServPlan where GrpServPlanNo = '" +
                GrpServPlanNo + "')";

        tSSRS = tExeSQL.execSQL(sql);
        int length = tSSRS.getMaxRow();
        String arr[][] = tSSRS.getAllData();

        for (int i = 1; i <= mLHGrpPersonUniteSet.size(); i++) {

            LHGrpPersonUniteSchema mLHGrpPersonUniteSchema = new
                    LHGrpPersonUniteSchema();
            for (int j = 0; j < length; j++) {
                System.out.println(j+arr[j][0]+"----GrpServPlanNo-----"+arr[j][1]+"-----------Level----"+mLHGrpPersonUniteSet.get(i).getLevel());
                if (arr[j][0].equals(mLHGrpPersonUniteSet.get(i).getLevel())) {
                    mLHGrpPersonUniteSchema.setGrpServPlanNo(arr[j][1]);
                }
            }

            System.out.println("++++" + GrpServPlanNo);
            mLHGrpPersonUniteSchema.setGrpServItemNo("0");
            mLHGrpPersonUniteSchema.setGrpContNo(mLHGrpPersonUniteSet.get(i).
                                                 getGrpContNo());
            mLHGrpPersonUniteSchema.setGrpCustomerNo(mLHGrpPersonUniteSet.get(i).
                    getGrpCustomerNo());
            mLHGrpPersonUniteSchema.setGrpName(mLHGrpPersonUniteSet.get(i).
                                               getGrpName());
            mLHGrpPersonUniteSchema.setCustomerNo(mLHGrpPersonUniteSet.get(i).
                                                  getCustomerNo());
            mLHGrpPersonUniteSchema.setName(mLHGrpPersonUniteSet.get(i).getName());
            mLHGrpPersonUniteSchema.setSex(mLHGrpPersonUniteSet.get(i).getSex());
            mLHGrpPersonUniteSchema.setBirthday(mLHGrpPersonUniteSet.get(i).
                                                getBirthday());
            mLHGrpPersonUniteSchema.setIDType(mLHGrpPersonUniteSet.get(i).
                                              getIDType());
            mLHGrpPersonUniteSchema.setIDNo(mLHGrpPersonUniteSet.get(i).getIDNo());
            mLHGrpPersonUniteSchema.setLevel(mLHGrpPersonUniteSet.get(i).
                                             getLevel());
            mLHGrpPersonUniteSchema.setOperator(mLHGrpPersonUniteSet.get(i).
                                                getOperator());
            mLHGrpPersonUniteSchema.setMakeDate(mLHGrpPersonUniteSet.get(i).
                                                getMakeDate());
            mLHGrpPersonUniteSchema.setMakeTime(mLHGrpPersonUniteSet.get(i).
                                                getMakeTime());
            mLHGrpPersonUniteSchema.setModifyDate(mLHGrpPersonUniteSet.get(i).
                                                  getModifyDate());
            mLHGrpPersonUniteSchema.setModifyTime(mLHGrpPersonUniteSet.get(i).
                                                  getModifyTime());
            mLHGrpPersonUniteSchema.setManageCom(mLHGrpPersonUniteSet.get(i).
                                                 getManageCom());
            System.out.println(
                    "------------(mLHGrpPersonUniteSet.get(i).getCustomerNo():" +
                    mLHGrpPersonUniteSet.get(i).getGrpServPlanNo() +
                    "-----------");

            dbLHGrpPersonUniteSet.add(mLHGrpPersonUniteSchema);
        }

        mInputData.add(dbLHGrpPersonUniteSet);
        map.put(dbLHGrpPersonUniteSet, "INSERT");
        mInputData.add(map);
        System.out.println(
                "----mOLHGrpPersonUniteDiskImporter.submitImport-----");
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "mOLHGrpPersonUniteDiskImporter";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrrors.addOneError(tError);
            return false;
        }
        System.out.println("----submitImport,success-----");
        return true;
    }

    /**
     * 主函数，调试用
     * @param args String[]
     */
    public static void main(String[] args) {
        String fileName = "E:/workspace/ui/temp/LHGrpPersonUnite.xls";
        String configFileName = "E:/workspace/ui/temp/LHGrpPersonUnite.xml";
        GrpServPlanNo = "10000000000000000088";
        String sheetName = "GRP";
        OLHGrpPersonUniteDiskImporter importer = new
                                                 OLHGrpPersonUniteDiskImporter(
                fileName, configFileName,
                GrpServPlanNo, sheetName);
        importer.submitImport();
        LHGrpPersonUniteSet set = (LHGrpPersonUniteSet) importer.getSchemaSet();
        System.out.println(set.toString());
    }
}
