package com.sinosoft.lis.health;

/**
 * <p>Title:OLHGroupContUI </p>
 * <p>Description:OLHGroupContUI </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author
 * @version 1.0
 * @CreateDate：2006-12-04 18:32:42
 */
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.pubfun.*;

public class OLHServFeeUI
{
    public OLHServFeeUI() {}
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    /** 全局数据 */
    private LJAGetHealthSchema mLJAGetHealthSchema = new LJAGetHealthSchema();
    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
            //将操作数据拷贝到本类中
            this.mOperate = cOperate;

            OLHServFeeBL tOLHServFeeBL = new OLHServFeeBL();
            tOLHServFeeBL.submitData(cInputData, mOperate);

            //如果有需要处理的错误，则返回
            if (tOLHServFeeBL.mErrors.needDealError())
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tOLHServFeeBL.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLHServFeeUI";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            mInputData = null;
            return true;
    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        LHCaseTaskRelaSchema mLHCaseTaskRelaSchema = new LHCaseTaskRelaSchema();
        LHCaseTaskRelaSet mLHCaseTaskRelaSet = new LHCaseTaskRelaSet();
        OLHServFeeUI tOLHServFeeUI = new OLHServFeeUI();

        tG.Operator = "hm";
        tG.ManageCom = "86";
        tG.ComCode = "86";
        mLHCaseTaskRelaSchema.setServTaskNo("000000002382");
        mLHCaseTaskRelaSet.add(mLHCaseTaskRelaSchema);

        VData tVData = new VData();
        tVData.add(mLHCaseTaskRelaSet);
        tVData.add(tG);
        tOLHServFeeUI.submitData(tVData,"INSERT||MAIN");
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
        try
        {
            mInputData.clear();
            mInputData.add(this.mLJAGetHealthSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLHServFeeUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
           return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        //全局变量
        this.mLJAGetHealthSchema.setSchema((LJAGetHealthSchema) cInputData.
                                           getObjectByObjectName("LJAGetHealthSchema", 0));
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

}
