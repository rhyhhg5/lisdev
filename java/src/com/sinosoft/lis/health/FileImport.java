/**
  * 将过滤后的数据倒入数据库
  */

package com.sinosoft.lis.health;

import java.io.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

public class FileImport
{

    private static String value = null;

    private String fileName = null;

    private static String tableName = "LQDisease";

    private static String fieldName = "DiseaseCode";

    private String KeyWordValue = null;

    static String[] col_blob =
    {"Summarize", "Diagnose", "Curation", "Etiology", "Pathogenesis", "PathologyChange", "Pathogeny",
     "Epidemiology", "ClinicShow", "Complex", "AssistantTest", "RateDiag", "Prevent", "Prognosis"};

    public FileImport(String fileName, String KeyWordValue)
    {
        this.fileName = fileName;			//目标文件
//        this.tableName = "LQDisease";		        //表名
//        this.fieldName = "DiseaseCode";		//关键字段名
        this.KeyWordValue = KeyWordValue;	        //关键字段值
    	getValue(fileName);	//文件内容
    }

    public FileImport(String fileName)
    {
        this.fileName = fileName;			//目标文件
//        this.tableName = "LQDisease";		        //表名
//        this.fieldName = "DiseaseCode";		//关键字段名
        getValue(fileName);	//文件内容
    }


    static void getValue(String fileName)
    {
        try
        {
            File f = new File(fileName);
            FileReader in = new FileReader(f);              //读文件的实例变量
            FileInputStream fis = new FileInputStream(f);   //获得可读取字节数，以确定char数组长度
            char[] buffer = new char[fis.available()];      // Read 一个文件大小的字节 at a time
            int len;
            if ((len = in.read(buffer)) != -1)
            {
                 value = new String(buffer, 0, len);
            }
            else
            {
               value = "" ;
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void getSQL()
    {
        String tDiseaseCode = PubFun1.CreateMaxNo("LQDisease",6);
        String sql = " INSERT INTO LQDISEASE (DiseaseCode, DiseaseName, OtherName, Symptom, DrugCure ) values ( '"
                     +tDiseaseCode+"', ";
        String sql_update_blob = "";
        String sql_end = "";
        System.out.println(tDiseaseCode);
        String[] col_value = new String[24];

        int i = 0;
        int left = 0;
        int right = 0;
        if(value.indexOf("\n") != value.lastIndexOf("\n"))
        {
            left = value.indexOf("\n") + 1;
            value = value.replaceFirst("\n","∏");

            while(i < 4)
            {
                right = value.indexOf("$", left);
                value = value.substring(0, right).concat("#" +
                        value.substring(right + 1));
                col_value[i] = value.substring(left, right);
                left = right + 1;

                if (i == 3)
                {
                    sql = sql + "'" + col_value[i] + "') ";
                } else
                {
                    sql = sql + "'" + col_value[i] + "', ";
                }
                i++;
            }
            CommonImport(sql);
            while(i > 3 && i < 16)
            {
                right = value.indexOf("$", left);
                value = value.substring(0, right).concat("#" +
                            value.substring(right + 1));
                col_value[i] = value.substring(left, right);
                left = right + 1;
                if(!col_value[i].equals(""))
                {
                    sql_update_blob = "update LQDISEASE set " + col_blob[i - 4] +
                                      " = ? " + "where DiseaseCode = '" + tDiseaseCode + "'";
                    BlobImport(sql_update_blob, col_value[i]);
                }
                i++;
            }
            while(i > 15 && i < 18)
            {
                right = value.indexOf("$", left);
                value = value.substring(0, right).concat("#" +
                        value.substring(right + 1));
                col_value[i] = value.substring(left, right);
                if( !col_value[i].equals(""))
                {
                    sql = "update LQDISEASE set " + col_blob[i - 4] + " = '" +
                          col_value[i] + "' where DiseaseCode = '" + tDiseaseCode + "'";
                    CommonImport(sql);
                }
                left = right + 1;
                i++;
            }
        }
        else{value = value.replaceFirst("\n","∏");}
    }

    public void CommonImport(String sql)
    {
        try {
            Connection conn = DBConnPool.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            ps.close();
            conn.close();
        } catch (SQLException ex)
        {
            System.out.println(sql);
            ex.printStackTrace();
        } catch (Exception ex) {
            System.out.println(sql);
            ex.printStackTrace();
        }
    }

    public void BlobImport(String sql, String valus)
    {
        try
        {
            Connection conn = DBConnPool.getConnection();
            InputStream in = new ByteArrayInputStream(valus.getBytes());
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setBinaryStream(1, in, in.available());
            ps.execute();
            ps.close();
            in.close();
            conn.close();
        }
        catch (SQLException ex)
        {
            System.out.println(sql);
            ex.printStackTrace();
        }
        catch (IOException ex)
        {
            System.out.println(sql);
            ex.printStackTrace();
        }
        catch (Exception ex)
        {
            System.out.println(sql);
            ex.printStackTrace();
        }
    }

    public void Import()
    {
        try
        {
            while(value.indexOf("\n") != -1)
            {
                getSQL();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
