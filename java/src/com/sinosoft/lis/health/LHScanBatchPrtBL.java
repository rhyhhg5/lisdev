package com.sinosoft.lis.health;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.utility.StrTool;
import org.jdom.Element;
import java.io.File;

/**
 * <p>Title: 保险业务系统</p>
 * <p>Description: 健管扫描分割页批量打印</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: Sinosoft</p>
 * @author
 */
public class LHScanBatchPrtBL
{


    private MMap tmpMap = new MMap();
    private VData mResult = new VData();
    public  CErrors mErrors = new CErrors();
    private GlobalInput mG = new GlobalInput();

    private LHScanInfoSet mLHScanInfoSet = new LHScanInfoSet();
    private LHScanInfoSchema mLHScanInfoSchema = new LHScanInfoSchema();
    private LJSPayBSchema mLJSPayBSchema = new LJSPayBSchema();
    private TransferData mPrintFactor = new TransferData();
    private LDSysVarSchema mLDSysVarSchema = new LDSysVarSchema();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private LOBatchPRTManagerSet mLOBatchPRTManagerSet = new LOBatchPRTManagerSet();

    private int mCount=0;
    private String FlagStr = "";
    private String strLogs = "";
    private String Content = "";
    private String mOperate;
    private String mUserCode = "";
    private String mxmlFileName = "";
    private boolean operFlag = true;


  /**
   * 传输数据的公共方法
   * @param: cInputData 输入的数据
   *         cOperate 数据操作
   * @return:
   */
  public boolean submitData(VData cInputData,String cOperate)
  {
      cInputData = (VData)cInputData.clone();
      this.mOperate = cOperate;

      if (!getInputData(cInputData))
          return false;

      if(mOperate.equals("PRINT") || mOperate.equals("INSERT"))
      {
          if(!dealData())
              return false;
      }
//      if(mOperate.equals("INSERT"))
//      {
//          if (!dealData())
//              return false;
//          PubSubmit ps = new PubSubmit();
//          if (!ps.submitData(this.mResult, null)) {
//              CError.buildErr(this, "数据保存失败");
//              return false;
//          }
//      }
      return true;
  }

  public VData getResult()
  {
      return mResult;
  }
  private boolean getInputData(VData cInputData)
  {
      mLHScanInfoSet = (LHScanInfoSet)cInputData.getObjectByObjectName("LHScanInfoSet",0);
      if(mLHScanInfoSet == null)
      {
           // @@错误处理
           CError tError = new CError();
           tError.moduleName = "LHScanBatchPrtBL";
           tError.functionName = "getInputData";
           tError.errorMessage = "传入的数据为空!";
           this.mErrors.addOneError(tError);
           return false;
      }
      mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
      mLDSysVarSchema.setSchema((LDSysVarSchema)cInputData.getObjectByObjectName("LDSysVarSchema",0));
      return true;
  }

  /*****************************************************************************************
   * Name     :checkdate
   * Function :判断数据逻辑上的正确性
   * 1:判断用户代码和用户姓名的关联是否正确
   * 2:校验用户和录入的管理机构是否匹配
   * 3:若不是最高的核赔权限的用户一定要有上级用户代码
   * 4:若是新增，判断该用户代码是否已经存在。
   *
   */
  private boolean checkdata()
  {
      return true;
  }
  private boolean querydata()
  {
    LLClaimUserDB tLLClaimUserDB = new LLClaimUserDB();
    LLClaimUserSchema tLLClaimUserSchema = new LLClaimUserSchema();
    tLLClaimUserDB.setUserCode(mUserCode);
    if(!tLLClaimUserDB.getInfo())
    {
      buildError("ClaimUserOperateBL","该用户的查询信息出错！请及时对其进行维护！");
      return false;
    }
    tLLClaimUserSchema.setSchema(tLLClaimUserDB.getSchema());
    LDComDB tLDComDB = new LDComDB();
    if(!(tLLClaimUserSchema.getComCode()==null||tLLClaimUserSchema.getComCode().equals("")))
    {

      tLDComDB.setComCode(tLLClaimUserSchema.getComCode());
      if(!tLDComDB.getInfo())
      {
        buildError("ClaimUserOperateBL","LDCom查询出错，请及时维护!");
        return false;
      }
    }
    mResult.clear();
    mResult.addElement(tLDComDB.getSchema().getName());
    mResult.add(tLLClaimUserSchema);
    return true;
  }
  public int getCount()
  {
      return  mCount;
  }
  /**
   * 数据操作类业务处理
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean dealData()
  {
          VData tVData;
          XmlExport txmlExportAll = new XmlExport();
          txmlExportAll.createDocument("ON");

          mCount=mLHScanInfoSet.size();
          String mXmlFileName[] = new String[mCount];

          for(int i= 1;i <= mCount ;i++)
          {
             LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
             tLOPRTManagerSchema.setCode("HM01");
             mLHScanInfoSchema = mLHScanInfoSet.get(i);
//             VData xVData = new VData();
//             xVData.addElement(mLDSysVarSchema);
//             xVData.addElement(mG);
//             xVData.addElement(mLHScanInfoSet.get(i));
//
//             LHScanSinglPrtBL tLHScanSinglPrtBL = new LHScanSinglPrtBL();
//             if (!tLHScanSinglPrtBL.submitData(xVData, "INSERT"))
//             {
//                 operFlag = false;
//                 Content = tLHScanSinglPrtBL.mErrors.getErrContent();
//             }


             if(!callPrintService(tLOPRTManagerSchema))
             {
                  Content=" 印刷号"+tLOPRTManagerSchema.getPrtSeq()+"："+this.mErrors.getFirstError().toString();
                  strLogs=strLogs+Content;
                  continue;
             }

//             tLOPRTManagerDB.setPrtSeq(mLHScanInfoSet.get(i).getSerialNo());
             tLOPRTManagerSchema = (LOPRTManagerSchema)mResult.getObjectByObjectName("LOPRTManagerSchema",0);

             mxmlFileName = StrTool.unicodeToGBK(tLOPRTManagerSchema.getCode())+"-"+tLOPRTManagerSchema.getPrtSeq()+"-"+tLOPRTManagerSchema.getOtherNo();
             mXmlFileName[i-1]=mxmlFileName;

             XmlExport txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
             if (txmlExport==null)
             {
                  Content="印刷号"+tLOPRTManagerSchema.getPrtSeq()+"没有得到要显示的数据文件！";
                  strLogs=strLogs+Content;
                  continue;
             }
             if (operFlag == true)
             {
                 File f = new File(mLDSysVarSchema.getSysVarValue());
                 f.mkdir() ;
                 System.out.println("PATH : "+mLDSysVarSchema.getSysVarValue());
                 System.out.println("PATH2 : "+mLDSysVarSchema.getSysVarValue().substring(0,mLDSysVarSchema.getSysVarValue().length()-1));
                 txmlExport.outputDocumentToFile(mLDSysVarSchema.getSysVarValue().substring(0,mLDSysVarSchema.getSysVarValue().length()-1) + File.separator + "printdata"
                                                 + File.separator + "data" + File.separator + "brief" + File.separator, mxmlFileName);
             }
         }
         tVData = new VData();
         tVData.add(mXmlFileName);
         mResult=tVData;

     return true;
 }

  private boolean prepareOutputData()
  {
      String tLimit = PubFun.getNoLimit(this.mG.ManageCom);
      String BatchCode = PubFun1.CreateMaxNo("BatchCode", tLimit);
      String tBatchType = (String) mPrintFactor.getValueByName("BatchType");
      String tGrpDetailPrt = (String) mPrintFactor.getValueByName("GrpDetailPrt");
      System.out.println(tGrpDetailPrt);
      String tDetailPrt = (String) mPrintFactor.getValueByName("DetailPrt");
      String tNoticePrt = (String) mPrintFactor.getValueByName("NoticePrt");
      LLCaseSet tLLCaseSet = new LLCaseSet();
      if (tBatchType.equals("1"))
      {
          String tRgtNo = (String) mPrintFactor.getValueByName("RgtNo");
          LLCaseDB tLLCaseDB = new LLCaseDB();
          tLLCaseDB.setRgtNo(tRgtNo);
          tLLCaseSet = tLLCaseDB.query();
          //打印团体汇总细目表，前台判断案件状态，此处不做判断，如状态不正确也能出当前已有数据的细目表
          if (tGrpDetailPrt.equals("on")) {
              LOPRTManagerSchema tLOPRTManagerSchema = new
                      LOPRTManagerSchema();
              String prtSeq = PubFun1.CreateMaxNo("prtSeq", tLimit);

              tLOPRTManagerSchema.setPrtSeq(prtSeq);
              tLOPRTManagerSchema.setOtherNo(tRgtNo);
              tLOPRTManagerSchema.setOtherNoType("7");
              tLOPRTManagerSchema.setPrtType("1");
              tLOPRTManagerSchema.setStateFlag("0");
              tLOPRTManagerSchema.setManageCom(mG.ManageCom);
              System.out.println("~~~~"+tRgtNo.substring(1,3));
              if(tRgtNo.substring(1,3).equals("12"))
              {
                tLOPRTManagerSchema.setCode("lp011");
              }
              else
                tLOPRTManagerSchema.setCode("lp003");
              tLOPRTManagerSchema.setReqCom(mG.ManageCom);
              tLOPRTManagerSchema.setReqOperator(mG.Operator);
              tLOPRTManagerSchema.setExeOperator(mG.Operator);
              tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
              tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
              mLOBatchPRTManagerSet.add(tLOPRTManagerSchema);
          }
      }
      if (tBatchType.equals("2")) {
          String tScaseno = (String) mPrintFactor.getValueByName("SCaseNo");
          String tEcaseno = (String) mPrintFactor.getValueByName("ECaseNo");
          LLCaseDB tLLCaseDB = new LLCaseDB();
          String sql =
                  "select * from llcase where substr(caseno,6)>=substr('" +
                  tScaseno + "',6) and substr(caseno,6)<=substr('" + tEcaseno +
                  "',6) and rgtstate in ('09','11','12') order by CaseNo";
          System.out.println(sql);
          tLLCaseSet = tLLCaseDB.executeQuery(sql);
      }
      if (tBatchType.equals("3")) {
          String tempstr = (String) mPrintFactor.getValueByName("CaseNoBatch");
          String tCaseNoBatch = tempstr.trim();
          while (tCaseNoBatch.length() >= 17) {
              LLCaseSchema tLLCaseSchema = new LLCaseSchema();
              int Cindex = tCaseNoBatch.indexOf("C");
              String tCaseNo = tCaseNoBatch.substring(Cindex, Cindex + 17);
              LLCaseDB tLLCaseDB = new LLCaseDB();
              tLLCaseDB.setCaseNo(tCaseNo);
              if (tLLCaseDB.getInfo()) {
                  tLLCaseSchema.setSchema(tLLCaseDB.getSchema());
                  tLLCaseSet.add(tLLCaseSchema);
              }
              tCaseNoBatch = tCaseNoBatch.substring(Cindex+17);
          }
      }
      int count = tLLCaseSet.size();
      for (int i = 1; i <= count; i++) {
          String trgtstate = tLLCaseSet.get(i).getRgtState();
          if (!trgtstate.equals("09") && !trgtstate.equals("10") &&
              !trgtstate.equals("11") && !trgtstate.equals("12")) {
              continue;
          } else {
              if (tNoticePrt.equals("on")) {
                  LOPRTManagerSchema tLOPRTManagerSchema = new
                          LOPRTManagerSchema();
                  String prtSeq = PubFun1.CreateMaxNo("prtSeq", tLimit);

                  tLOPRTManagerSchema.setPrtSeq(prtSeq);
                  tLOPRTManagerSchema.setOtherNo(tLLCaseSet.get(i).
                          getCaseNo());
                  tLOPRTManagerSchema.setOtherNoType("5");
                  tLOPRTManagerSchema.setPrtType("1");
                  tLOPRTManagerSchema.setStateFlag("0");
                  tLOPRTManagerSchema.setManageCom(mG.ManageCom);
                  tLOPRTManagerSchema.setCode("lp000");
                  tLOPRTManagerSchema.setReqCom(mG.ManageCom);
                  tLOPRTManagerSchema.setReqOperator(mG.Operator);
                  tLOPRTManagerSchema.setExeOperator(mG.Operator);
                  tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
                  tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
                  mLOBatchPRTManagerSet.add(tLOPRTManagerSchema);
              }
              if (tDetailPrt.equals("on")) {
                  LLCaseRelaDB tLLCaseRelaDB = new LLCaseRelaDB();
                  LLCaseRelaSet tLLCaseRelaSet = new LLCaseRelaSet();
                  tLLCaseRelaDB.setCaseNo(tLLCaseSet.get(i).getCaseNo());
                  tLLCaseRelaSet = tLLCaseRelaDB.query();
                  if (tLLCaseRelaSet != null && tLLCaseRelaSet.size() > 0) {
                      for (int k = 1; k <= tLLCaseRelaSet.size(); k++) {
                          LOPRTManagerSchema tLOPRTManagerSchema = new
                                  LOPRTManagerSchema();
                          String prtSeq = PubFun1.CreateMaxNo("prtSeq",
                                  tLimit);

                          tLOPRTManagerSchema.setPrtSeq(prtSeq);
                          tLOPRTManagerSchema.setOtherNo(tLLCaseSet.get(
                                  i).getCaseNo());
                          tLOPRTManagerSchema.setStandbyFlag1(
                                  tLLCaseRelaSet.get(k).getCaseRelaNo());
                          tLOPRTManagerSchema.setOtherNoType("5");
                          tLOPRTManagerSchema.setPrtType("1");
                          tLOPRTManagerSchema.setStateFlag("0");
                          tLOPRTManagerSchema.setManageCom(mG.ManageCom);
                          tLOPRTManagerSchema.setCode("lp001");
                          tLOPRTManagerSchema.setReqCom(mG.ManageCom);
                          tLOPRTManagerSchema.setReqOperator(mG.Operator);
                          tLOPRTManagerSchema.setExeOperator(mG.Operator);
                          tLOPRTManagerSchema.setMakeDate(PubFun.
                                  getCurrentDate());
                          tLOPRTManagerSchema.setMakeTime(PubFun.
                                  getCurrentTime());
                          mLOBatchPRTManagerSet.add(tLOPRTManagerSchema);

                      }
                  }
              }
          }
      }

    if(mLOBatchPRTManagerSet==null||mLOBatchPRTManagerSet.size()<=0)
    {
        buildError("LHScanBatchPrtBL","没有符合打印条件的数据，请确认案件是否已结案！");
        return false;
    }
    else{
        tmpMap.put(mLOBatchPRTManagerSet,"INSERT");
    }
    mResult.clear();
    mResult.add(tmpMap);
    return true;
  }
  public void buildError(String szFunc, String szErrMsg)
  {
    CError cError = new CError( );
    cError.moduleName = "LHScanBatchPrtBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }
  public static void main(String[] args)
  {

//      TransferData PrintElement = new TransferData();
//      PrintElement.setNameAndValue("RgtNo","P1200061030000001");
//      PrintElement.setNameAndValue("SCaseNo","C0000051107000002");
//      PrintElement.setNameAndValue("ECaseNo","C0000051107000002");
//      PrintElement.setNameAndValue("CaseNoBatch","C1100061116000095");
//      PrintElement.setNameAndValue("BatchType","3");
//      PrintElement.setNameAndValue("DetailPrt","on");
//      PrintElement.setNameAndValue("GrpDetailPrt","off");
//      PrintElement.setNameAndValue("NoticePrt","off");

      GlobalInput tGlobalInput = new GlobalInput();
      tGlobalInput.ComCode = "86";
      tGlobalInput.ManageCom = "86";
      tGlobalInput.Operator = "001";
      LDSysVarSchema mLDSysVarSchema = new LDSysVarSchema();
      //mLDSysVarSchema.setSysVar("xmlrealpath");
      mLDSysVarSchema.setSysVarValue("E:\\workspace\\ui\\");

      LHScanInfoSet tLHScanInfoSet = new LHScanInfoSet();

          LHScanInfoSchema tLHScanInfoSchema = new LHScanInfoSchema();
          tLHScanInfoSchema.setSerialNo("20001000833843");
          tLHScanInfoSchema.setOtherNo("000833843");
          tLHScanInfoSchema.setOtherNoType("01");
          tLHScanInfoSet.add(tLHScanInfoSchema);
          LHScanInfoSchema tLHScanInfoSchema2 = new LHScanInfoSchema();
          tLHScanInfoSchema2.setSerialNo("20002000833843");
          tLHScanInfoSchema2.setOtherNo("000833843");
          tLHScanInfoSchema2.setOtherNoType("01");
          tLHScanInfoSet.add(tLHScanInfoSchema2);
          LHScanInfoSchema tLHScanInfoSchema3 = new LHScanInfoSchema();
          tLHScanInfoSchema3.setSerialNo("20003000833843");
          tLHScanInfoSchema3.setOtherNo("000833843");
          tLHScanInfoSchema3.setOtherNoType("01");
          tLHScanInfoSet.add(tLHScanInfoSchema3);




      VData aVData = new VData();
      aVData.add(tGlobalInput);
      aVData.add(tLHScanInfoSet);
      aVData.add(mLDSysVarSchema);
    /*
    String aUserCode = "000011";
    VData aVData = new VData();
    aVData.addElement(aUserCode);
    */
    LHScanBatchPrtBL tLHScanBatchPrtBL = new LHScanBatchPrtBL();
    tLHScanBatchPrtBL.submitData(aVData,"PRINT");
    int tCount = tLHScanBatchPrtBL.getCount();
    System.out.println("~~~~~~~~~~~~~~~~~~~"+tCount);
    String tFileName[] = new String[tCount];
    VData tResult = new VData();
    tResult = tLHScanBatchPrtBL.getResult();
    tFileName=(String[])tResult.getObject(0);
    System.out.println("~~~~~~~~~~~~~~~~~~~2"+tFileName);

    }
    private boolean callPrintService(LOPRTManagerSchema aLOPRTManagerSchema)
       {

           // 查找打印服务
           String strSQL = "SELECT * FROM LDCode WHERE CodeType = 'print_service'";
           strSQL += " AND Code = '" + aLOPRTManagerSchema.getCode() + "'";
           strSQL += " AND OtherSign = '0'";
           System.out.println(strSQL);
           LDCodeSet tLDCodeSet = new LDCodeDB().executeQuery(strSQL);

           if (tLDCodeSet.size() == 0)
           {
               buildError("dealData",
                          "找不到对应的打印服务类(Code = '" + aLOPRTManagerSchema.getCode() +
                          "')");
               return false;
           }

           // 调用打印服务
           LDCodeSchema tLDCodeSchema = tLDCodeSet.get(1);

           try
           {
               Class cls = Class.forName(tLDCodeSchema.getCodeAlias());
               PrintService ps = (PrintService) cls.newInstance();

               // 准备数据
               String strOperate = tLDCodeSchema.getCodeName();

               VData vData = new VData();

               vData.add(mG);
               vData.add(aLOPRTManagerSchema);
               vData.addElement(mLDSysVarSchema);
               vData.addElement(mLHScanInfoSchema);


               if (!ps.submitData(vData, strOperate))
               {
                   mErrors.copyAllErrors(ps.getErrors());
                   return false;
               }

               mResult = ps.getResult();

           }
           catch (Exception ex)
           {
               ex.printStackTrace();
               buildError("callPrintService", ex.toString());
               return false;
           }

           return true;
    }
}
