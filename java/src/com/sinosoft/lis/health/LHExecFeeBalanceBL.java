/*
 * <p>ClassName: LHExecFeeBalanceBL </p>
 * <p>Description: LHExecFeeBalanceBL类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-09-06 17:14:38
 */
package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class LHExecFeeBalanceBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
private MMap map= new MMap();

/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
private String tNo;
/** 业务处理相关变量 */
private TransferData mTransferData = new TransferData();
private LHExecFeeBalanceSet mLHExecFeeBalanceSet=new LHExecFeeBalanceSet();
private LHTaskCustomerRelaSet mLHTaskCustomerRelaSet=new LHTaskCustomerRelaSet();


public LHExecFeeBalanceBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
public static void main(String[] args) {
}
/**
* 传输数据的公共方法
        * @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "LHExecFeeBalanceBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败LHExecFeeBalanceBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
      PubSubmit tPubSubmit = new PubSubmit();
      System.out.println("Start LHExecFeeBalanceBL Submit...");
      //LHExecFeeBalanceBLS tLHExecFeeBalanceBLS=new LHExecFeeBalanceBLS();
      //tLHExecFeeBalanceBLS.submitData(mInputData,mOperate);
      if (!tPubSubmit.submitData(mInputData, mOperate))
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);

        CError tError = new CError();
        tError.moduleName = "LHExecFeeBalanceBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据提交失败!";

        this.mErrors.addOneError(tError);
        return false;
      }

      System.out.println("End LHExecFeeBalanceBL Submit...");
      //如果有需要处理的错误，则返回
      //if (tLHExecFeeBalanceBLS.mErrors.needDealError())
      //{
        // @@错误处理
        //this.mErrors.copyAllErrors(tLHExecFeeBalanceBLS.mErrors);
       // CError tError = new CError();
        //tError.moduleName = "LHExecFeeBalanceBL";
        //tError.functionName = "submitDat";
        //tError.errorMessage ="数据提交失败!";
        //this.mErrors .addOneError(tError) ;
        //return false;
      //}
    }
    mInputData=null;
    return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
     if (this.mOperate.equals("INSERT||MAIN"))
     {
         String arrTaskCustomer[][];
         SSRS SSRS_TaskCustomer = new SSRS();
         ExeSQL tTaskCustomer= new ExeSQL();
         String  sqlServTaskNo = "'"+mTransferData.getValueByName("ServTaskNo").toString().replaceAll(",","','")+"'";
         String sql= " select (select a.CustomerNo from LHServCaseRela a where a.ServCaseCode=v.ServCaseCode and a.ServItemNo=v.ServItemNo) ,"
                     +" v.TaskExecNo,v.ServTaskNo,v.ServCaseCode,v.ServTaskCode,v.ServItemNo,"
                     +"  (select a.ContNo from LHServCaseRela a where a.ServCaseCode=v.ServCaseCode and a.ServItemNo=v.ServItemNo) "
                     +" from LHTaskCustomerRela v  where v.ServTaskNo in ("+ sqlServTaskNo +")"
                     ;
         SSRS_TaskCustomer = tTaskCustomer.execSQL(sql);
         arrTaskCustomer = SSRS_TaskCustomer.getAllData();
         mLHExecFeeBalanceSet.clear();
         if(mTransferData != null && arrTaskCustomer.length != 0)
         {
             for (int j = 1; j <= arrTaskCustomer.length; j++)
             {
                 LHExecFeeBalanceSchema mLHExecFeeBalanceSchema = new LHExecFeeBalanceSchema();

                 mLHExecFeeBalanceSchema.setTaskExecNo(arrTaskCustomer[j-1][1]);
                 mLHExecFeeBalanceSchema.setServTaskNo(arrTaskCustomer[j-1][2]);
                 mLHExecFeeBalanceSchema.setServCaseCode(arrTaskCustomer[j-1][3]);
                 mLHExecFeeBalanceSchema.setServTaskCode(arrTaskCustomer[j-1][4]);
                 mLHExecFeeBalanceSchema.setServItemNo(arrTaskCustomer[j-1][5]);
                 mLHExecFeeBalanceSchema.setCustomerNo(arrTaskCustomer[j-1][0]);
                 mLHExecFeeBalanceSchema.setContNo(arrTaskCustomer[j-1][6]);
                 mLHExecFeeBalanceSchema.setHospitCode(mTransferData.getValueByName("HospitCode").toString());
                 mLHExecFeeBalanceSchema.setContraNo(mTransferData.getValueByName("ContraNo").toString());
                 mLHExecFeeBalanceSchema.setContraItemNo(mTransferData.getValueByName("DutyItemCode").toString());
                 mLHExecFeeBalanceSchema.setServPlanNo(mTransferData.getValueByName("Contraitemno").toString());
                 mLHExecFeeBalanceSchema.setOperator(mGlobalInput.Operator);
                 mLHExecFeeBalanceSchema.setMakeDate(PubFun.getCurrentDate());
                 mLHExecFeeBalanceSchema.setMakeTime(PubFun.getCurrentTime());
                 mLHExecFeeBalanceSchema.setModifyDate(PubFun.getCurrentDate());
                 mLHExecFeeBalanceSchema.setModifyTime(PubFun.getCurrentTime());
                 mLHExecFeeBalanceSchema.setManageCom(mGlobalInput.ManageCom);
                 mLHExecFeeBalanceSet.add(mLHExecFeeBalanceSchema);
             }
             map.put(mLHExecFeeBalanceSet, "INSERT");
             for (int j = 1; j <= arrTaskCustomer.length; j++)
             {
                 map.put(" update LHTASKCUSTOMERRELA a set  a.TaskExecState='"+mTransferData.getValueByName("ExecState").toString()+"',"
                         +" a.ManageCom='"+mGlobalInput.ManageCom+"', a.Operator='"+mGlobalInput.Operator+"', "
                         +" a.ModifyDate='"+PubFun.getCurrentDate()+"',a.ModifyTime='"+PubFun.getCurrentTime()  +"' "
                         +" where  a.TaskExecNo ='"+arrTaskCustomer[j-1][1]+"'","UPDATE");
             }
         }
     }

     if (this.mOperate.equals("UPDATE||MAIN"))
     {
         if (mLHExecFeeBalanceSet != null && mLHExecFeeBalanceSet.size() > 0)
         {
              for (int j= 1; j<= mLHExecFeeBalanceSet.size(); j++)
              {
                  mLHExecFeeBalanceSet.get(j).setOperator(mGlobalInput.Operator);
                  mLHExecFeeBalanceSet.get(j).setModifyDate(PubFun.getCurrentDate());
                  mLHExecFeeBalanceSet.get(j).setModifyTime(PubFun.getCurrentTime());
                  mLHExecFeeBalanceSet.get(j).setManageCom(mGlobalInput.ManageCom);
                  map.put(" delete from  LHExecFeeBalance where TaskExecNo='" +mLHExecFeeBalanceSet.get(j).getTaskExecNo() + "'",
                          "DELETE");
              }
              map.put(mLHExecFeeBalanceSet, "INSERT");
              for (int i= 1; i<= mLHExecFeeBalanceSet.size(); i++)
              {
                  map.put(" update LHTASKCUSTOMERRELA a set  a.TaskExecState='"+mTransferData.getValueByName("ExecState").toString()+"',"
                          +" a.ManageCom='"+mGlobalInput.ManageCom+"', a.Operator='"+mGlobalInput.Operator+"', "
                          +" a.ModifyDate='"+PubFun.getCurrentDate()+"',a.ModifyTime='"+PubFun.getCurrentTime()  +"' "
                          +" where  a.TaskExecNo ='"+mLHExecFeeBalanceSet.get(i).getTaskExecNo()+"'","UPDATE");
              }
         }
     }
     return true;
}
/**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
 */
private boolean updateData()
{
    return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
    return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
    this.mLHExecFeeBalanceSet.set((LHExecFeeBalanceSet)cInputData.getObjectByObjectName("LHExecFeeBalanceSet",0));
    this.mLHTaskCustomerRelaSet.set((LHTaskCustomerRelaSet)cInputData.getObjectByObjectName("LHTaskCustomerRelaSet",0));
    this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    if (mTransferData != null)
    {
        mTransferData = ((TransferData) cInputData.getObjectByObjectName("TransferData", 0));
    }

    return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
    this.mResult.clear();
    LHExecFeeBalanceDB tLHExecFeeBalanceDB=new LHExecFeeBalanceDB();
    //tLHExecFeeBalanceDB.setSchema(this.mLHExecFeeBalanceSchema);
    //如果有需要处理的错误，则返回
    if (tLHExecFeeBalanceDB.mErrors.needDealError())
    {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHExecFeeBalanceDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHExecFeeBalanceBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors .addOneError(tError) ;
            return false;
    }
    mInputData=null;
    return true;
}
private boolean prepareOutputData()
{
     try
     {
         this.mInputData.clear();
         this.mInputData.add(this.mLHExecFeeBalanceSet);
         this.mInputData.add(this.mLHTaskCustomerRelaSet);
         mInputData.add(map);
         mResult.clear();
         mResult.add(this.mLHExecFeeBalanceSet);
         mResult.add(this.mLHTaskCustomerRelaSet);
     }
     catch(Exception ex)
     {
         // @@错误处理
         CError tError = new CError();
         tError.moduleName = "LHExecFeeBalanceBL";
         tError.functionName = "prepareData";
         tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
         this.mErrors.addOneError(tError);
         return false;
     }
     return true;
}

    public VData getResult()
    {
        return this.mResult;
    }

    private void jbInit() throws Exception {}
}
