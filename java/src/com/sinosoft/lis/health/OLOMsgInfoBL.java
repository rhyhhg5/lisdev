/*
 * <p>ClassName: OLOMsgInfoBL </p>
 * <p>Description: OLOMsgInfoBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-11-02 14:02:36
 */
package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLOMsgInfoBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LOMsgInfoSchema mLOMsgInfoSchema = new LOMsgInfoSchema();
    private LOMsgReceiveSchema mLOMsgReceiveSchema = new LOMsgReceiveSchema();
    private LOMsgReceiveSet mLOMsgReceiveSet = new LOMsgReceiveSet();
//private Set mSet=new Set();
    public OLOMsgInfoBL() {
    }

    public static void main(String[] args) {
        LOMsgInfoSchema tLOMsgInfoSchema = new LOMsgInfoSchema();
        LOMsgReceiveSet tLOMsgReceiveSet = new LOMsgReceiveSet();
        OLOMsgInfoUI tOLOMsgInfoUI = new OLOMsgInfoUI();
        GlobalInput tGlobalInput = new GlobalInput();

        VData tVData = new VData();

        tLOMsgInfoSchema.setSendEmail("from");
        tLOMsgInfoSchema.setMsgTopic("title");
        tLOMsgInfoSchema.setMsgContent("content");
        tLOMsgInfoSchema.setSendType("SendType");
        tLOMsgInfoSchema.setMsgSend("MsgSend");
        tLOMsgInfoSchema.setMsgNo(PubFun1.CreateMaxNo("HMMSG", 12));
        tLOMsgInfoSchema.setOperator("HM");
        tLOMsgInfoSchema.setMakeDate("2005-08-08");
        tLOMsgInfoSchema.setMakeTime("22:22:22");
        tLOMsgInfoSchema.setModifyDate("2005-08-08");
        tLOMsgInfoSchema.setModifyTime("22:22:22");
        tLOMsgInfoSchema.setManageCom("HM");
        tLOMsgInfoSchema.setDepartment("HM");
        tLOMsgInfoSchema.setMsgType("1");

        tGlobalInput.ComCode = "86";
        tGlobalInput.Operator = "hm";

        tVData.add(tLOMsgInfoSchema);
        tVData.add(tLOMsgReceiveSet);
        tVData.add(tGlobalInput);

        tOLOMsgInfoUI.submitData(tVData, "INSERT||MAIN");

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        this.mInputData = cInputData;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLOMsgInfoBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLOMsgInfoBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start OLOMsgInfoBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("End OLOMsgInfoBL Submit...");
            //如果有需要处理的错误，则返回
            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLOMsgInfoBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;

            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        if (this.mOperate.equals("INSERT||MAIN")) {
            mLOMsgInfoSchema.setMsgNo(PubFun1.CreateMaxNo("HMMSG", 12));
            mLOMsgInfoSchema.setOperator(mGlobalInput.Operator);
            mLOMsgInfoSchema.setMakeDate(PubFun.getCurrentDate());
            mLOMsgInfoSchema.setMakeTime(PubFun.getCurrentTime());
            mLOMsgInfoSchema.setModifyDate(PubFun.getCurrentDate());
            mLOMsgInfoSchema.setModifyTime(PubFun.getCurrentTime());
            mLOMsgInfoSchema.setManageCom(mGlobalInput.ManageCom);
            mLOMsgInfoSchema.setDepartment("HM");
            mLOMsgInfoSchema.setMsgType("1");

            for (int i = 1; i <= mLOMsgReceiveSet.size(); i++) {
                if (mLOMsgReceiveSet.get(i).getMsgNo() == null ||
                    mLOMsgReceiveSet.get(i).getMsgNo().equals("")) {
                    mLOMsgReceiveSet.get(i).setMsgNo(mLOMsgInfoSchema.getMsgNo());
                }
                mLOMsgReceiveSet.get(i).setSeqNo(PubFun1.CreateMaxNo("MSGSEQ", 12));
                mLOMsgReceiveSet.get(i).setOperator(mGlobalInput.
                        Operator);
                mLOMsgReceiveSet.get(i).setMakeDate(PubFun.getCurrentDate());
                mLOMsgReceiveSet.get(i).setMakeTime(PubFun.getCurrentTime());
                mLOMsgReceiveSet.get(i).setModifyDate(PubFun.
                        getCurrentDate());
                mLOMsgReceiveSet.get(i).setModifyTime(PubFun.
                        getCurrentTime());
                mLOMsgReceiveSet.get(i).setManageCom(mGlobalInput.ManageCom);
            }

            map.put(mLOMsgInfoSchema, "INSERT");
            map.put(mLOMsgReceiveSet, "INSERT");
        }

        if (this.mOperate.equals("UPDATE||MAIN"))
        {System.out.println("------UPDATE--------");
            mLOMsgInfoSchema.setOperator(mGlobalInput.Operator);
            mLOMsgInfoSchema.setModifyDate(PubFun.getCurrentDate());
            mLOMsgInfoSchema.setModifyTime(PubFun.getCurrentTime());
            mLOMsgInfoSchema.setManageCom(mGlobalInput.ManageCom);
            mLOMsgInfoSchema.setDepartment("HM");
            mLOMsgInfoSchema.setMsgType("1");

            for (int i = 1; i <= mLOMsgReceiveSet.size(); i++) {
                if (mLOMsgReceiveSet.get(i).getMsgNo() == null ||
                    mLOMsgReceiveSet.get(i).getMsgNo().equals("")) {
                    mLOMsgReceiveSet.get(i).setMsgNo(mLOMsgInfoSchema.getMsgNo());
                }
                mLOMsgReceiveSet.get(i).setOperator(mGlobalInput.
                        Operator);
                mLOMsgReceiveSet.get(i).setModifyDate(PubFun.
                        getCurrentDate());
                mLOMsgReceiveSet.get(i).setModifyTime(PubFun.
                        getCurrentTime());
                mLOMsgReceiveSet.get(i).setManageCom(mGlobalInput.ManageCom);
            }


            map.put(mLOMsgInfoSchema, "UPDATE");System.out.println("---------2rd map---------");
            map.put(mLOMsgReceiveSet, "UPDATE");
        }

        if (this.mOperate.equals("DELETE||MAIN")) {

            map.put(mLOMsgInfoSchema, "DELETE");
            map.put(" delete from LOMsgReceive where MsgNo='"+mLOMsgInfoSchema.getMsgNo()+"' ", "DELETE");
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLOMsgInfoSchema.setSchema((LOMsgInfoSchema) cInputData.
                                        getObjectByObjectName("LOMsgInfoSchema",
                0));
        this.mLOMsgReceiveSet.set((LOMsgReceiveSet) cInputData.
                                  getObjectByObjectName(
                                          "LOMsgReceiveSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LOMsgInfoDB tLOMsgInfoDB = new LOMsgInfoDB();
        tLOMsgInfoDB.setSchema(this.mLOMsgInfoSchema);
        //如果有需要处理的错误，则返回
        if (tLOMsgInfoDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLOMsgInfoDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "BL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLOMsgInfoSchema);
            this.mInputData.add(this.mLOMsgReceiveSet);
            this.mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLOMsgInfoSchema);
            mResult.add(this.mLOMsgReceiveSet);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "BL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
