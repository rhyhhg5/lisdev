/*
 * <p>ClassName: OLHServFeeBL </p>
 * <p>Description:OLHServFeeBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-12-05 18:32:42
 */
package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLHServFeeBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;

    /** 业务处理相关变量 */
    String SNo ="";
    LJAGetSchema mLJAGetSchema = new LJAGetSchema();
    LJAGetHealthSet mLJAGetHealthSet = new LJAGetHealthSet();
    LHCaseTaskRelaSet mLHCaseTaskRelaSet = new LHCaseTaskRelaSet();


    public OLHServFeeBL() {}
    public static void main(String[] args)
    {
        String transact = "INSERT||MAIN";
        GlobalInput tG = new GlobalInput();
        tG.ManageCom="86";
        tG.Operator="hm";
        tG.ComCode="86";

        LHCaseTaskRelaSet mLHCaseTaskRelaSet = new LHCaseTaskRelaSet();
        LHCaseTaskRelaSchema mLHCaseTaskRelaSchema = new LHCaseTaskRelaSchema();
        OLHServFeeUI tOLHServFeeUI = new OLHServFeeUI();

        //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
        mLHCaseTaskRelaSchema.setServTaskNo("000000002382");
        mLHCaseTaskRelaSet.add(mLHCaseTaskRelaSchema);

        // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(tG);
        tVData.add(mLHCaseTaskRelaSet);
        tOLHServFeeUI.submitData(tVData, transact);
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLHServFeeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLHServFeeBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        PubSubmit tPubSubmit = new PubSubmit();
        System.out.println("Start OLHServFeeBL Submit...");
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
             // @@错误处理
             this.mErrors.copyAllErrors(tPubSubmit.mErrors);
             CError tError = new CError();
             tError.moduleName = "OLHServFeeBL";
             tError.functionName = "submitData";
             tError.errorMessage = "数据提交失败!";
             this.mErrors.addOneError(tError);
             return false;
         }
         System.out.println("End OLHServFeeBL Submit...");
         mInputData = null;
         return true;
     }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            SSRS vTaskCust = new SSRS();
            ExeSQL eTaskCust= new ExeSQL();
            String aTaskCust[][];
            SNo = PubFun1.CreateMaxNo("LHSERVTASKFEE", 12);

            for(int i = 1; i<=mLHCaseTaskRelaSet.size(); i++)
            {
                String sTaskCust = " select f.taskexecno,f.contno, "
                                   +" (case when (select distinct a.servplanno from lhservplan a,lhservitem b where a.servplanno=b.servplanno and b.servitemno=f.servitemno) is null "
                                   +" then (select distinct a.grpcontno from lhgrpservplan a,lhservitem b where a.grpservplanno=b.grpservplanno and b.servitemno=f.servitemno) else '0' end), "
                                   +" (select distinct a.servplancode from lhservplan a,lhservitem b where a.servplanno=b.servplanno and b.servitemno=f.servitemno), "
                                   +" f.customerno,f.servitemno,f.servtaskno,f.feepay from lhtaskcustomerrela t, lhfeecharge f "
                                   +" where t.taskexecno = f.taskexecno and f.servtaskno = '"+mLHCaseTaskRelaSet.get(i).getServTaskNo()+"'";
                ;
                vTaskCust = eTaskCust.execSQL(sTaskCust);
                aTaskCust = vTaskCust.getAllData();



                for(int j=0; j<vTaskCust.getMaxRow(); j++)
                {
                    LJAGetHealthSet tLJAGetHealthSet = new LJAGetHealthSet();
                    LJAGetHealthDB tLJAGetHealthDB = new LJAGetHealthDB();
                    tLJAGetHealthDB.setOtherNo(aTaskCust[j][0]);
                    tLJAGetHealthSet = tLJAGetHealthDB.query();
                    if(tLJAGetHealthSet.size() != 0)
                    {
                        ExeSQL t= new ExeSQL();
                        String sCaseName = t.getOneValue("select distinct (select distinct d.servcasename from lhservcasedef d where a.servcasecode=d.servcasecode)from lhcasetaskrela a where  servtaskno = '"+aTaskCust[j][6]+"'");
                        String sTaskName = t.getOneValue("select distinct (select distinct d.servtaskname from lhservtaskdef d where a.servtaskcode=d.servtaskcode)from lhcasetaskrela a where  servtaskno = '"+aTaskCust[j][6]+"'");
                        String sCustName = t.getOneValue(" select distinct name from ldperson where customerno = '"+aTaskCust[j][4]+"'");

                        CError tError = new CError();
                        tError.moduleName = "OLHServFeeBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = "服务事件“"+sCaseName+"”的“"+sTaskName+"”任务（"+aTaskCust[j][6]+"）中的客户 "+sCustName+"（"+aTaskCust[j][4]+"）对应的服务项目已进行过结算，请确认！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }


                    //增加个人Polno
                    String tPolno;
                    if(aTaskCust[j][2].equals("0"))
                    {
                        SSRS sPol = new SSRS();
                        ExeSQL ePol= new ExeSQL();
                        //System.out.println("  select distinct polno from lcpol where contno='"+aTaskCust[j][1]+"' and insuredno = '"+aTaskCust[j][4]+"' and riskcode='"+aTaskCust[j][3]+"'");
                        sPol = ePol.execSQL(" select distinct polno from lcpol where contno='"+aTaskCust[j][1]+"' and insuredno = '"+aTaskCust[j][4]+"' and riskcode='"+aTaskCust[j][3]+"'");
                        if(sPol.MaxRow != 1)
                        {
                            ExeSQL t= new ExeSQL();
                            String sCaseName = t.getOneValue("select distinct (select distinct d.servcasename from lhservcasedef d where a.servcasecode=d.servcasecode)from lhcasetaskrela a where  servtaskno = '"+aTaskCust[j][6]+"'");
                            String sTaskName = t.getOneValue("select distinct (select distinct d.servtaskname from lhservtaskdef d where a.servtaskcode=d.servtaskcode)from lhcasetaskrela a where  servtaskno = '"+aTaskCust[j][6]+"'");
                            String sCustName = t.getOneValue(" select distinct name from ldperson where customerno = '"+aTaskCust[j][4]+"'");

                            CError tError = new CError();
                            tError.moduleName = "OLHServFeeAllBL";
                            tError.functionName = "dealData";
                            tError.errorMessage = "服务事件“"+sCaseName+"”的“"+sTaskName+"”任务（"+aTaskCust[j][6]+"）中的客户 "+sCustName+"（"+aTaskCust[j][4]+"）的险种号不存在或不唯一！";
                            this.mErrors.addOneError(tError);
                            return false;
                        }

                        tPolno  = sPol.getAllData()[0][0];
                        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@"+tPolno.length());
                        if(tPolno.equals("") || tPolno==null || tPolno.length() != 11)
                        {
                            ExeSQL t= new ExeSQL();
                            String sCaseName = t.getOneValue("select distinct (select distinct d.servcasename from lhservcasedef d where a.servcasecode=d.servcasecode)from lhcasetaskrela a where  servtaskno = '"+aTaskCust[j][6]+"'");
                            String sTaskName = t.getOneValue("select distinct (select distinct d.servtaskname from lhservtaskdef d where a.servtaskcode=d.servtaskcode)from lhcasetaskrela a where  servtaskno = '"+aTaskCust[j][6]+"'");
                            String sCustName = t.getOneValue(" select distinct name from ldperson where customerno = '"+aTaskCust[j][4]+"'");

                            CError tError = new CError();
                            tError.moduleName = "OLHServFeeAllBL";
                            tError.functionName = "dealData";
                            tError.errorMessage = "服务事件“"+sCaseName+"”的“"+sTaskName+"”任务（"+aTaskCust[j][6]+"）中的客户 "+sCustName+"（"+aTaskCust[j][4]+"）对应的险种号出错！";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                    }

                    //增加团单Polno
                    else
                    {
                        SSRS sGrpPol = new SSRS();
                        ExeSQL eGrpPol= new ExeSQL();

                        String tGrpPol = " select polno from lcpol where grpcontno = '"+aTaskCust[j][2]+"'"
                                        +" and insuredno = '"+aTaskCust[j][4]+"' "
                                        +" and riskcode in (select distinct riskcode from lmriskapp where risktype2 = '5') "
                                        ;
                       sGrpPol = eGrpPol.execSQL(tGrpPol);
                       if(sGrpPol.MaxRow != 1)
                       {
                           ExeSQL t= new ExeSQL();
                           String sCaseName = t.getOneValue("select distinct (select distinct d.servcasename from lhservcasedef d where a.servcasecode=d.servcasecode)from lhcasetaskrela a where  servtaskno = '"+aTaskCust[j][6]+"'");
                           String sTaskName = t.getOneValue("select distinct (select distinct d.servtaskname from lhservtaskdef d where a.servtaskcode=d.servtaskcode)from lhcasetaskrela a where  servtaskno = '"+aTaskCust[j][6]+"'");
                           String sCustName = t.getOneValue(" select distinct name from ldperson where customerno = '"+aTaskCust[j][4]+"'");

                           CError tError = new CError();
                           tError.moduleName = "OLHServFeeAllBL";
                           tError.functionName = "dealData";
                           tError.errorMessage = "服务事件“"+sCaseName+"”的“"+sTaskName+"”任务（"+aTaskCust[j][6]+"）中的客户 "+sCustName+"（"+aTaskCust[j][4]+"）的险种号不存在或不唯一！";
                           this.mErrors.addOneError(tError);
                           return false;
                       }

                       tPolno = sGrpPol.getAllData()[0][0];
                       if(tPolno.equals("") || tPolno==null || tPolno.length() != 11)
                        {
                            ExeSQL t= new ExeSQL();
                            String sCaseName = t.getOneValue("select distinct (select distinct d.servcasename from lhservcasedef d where a.servcasecode=d.servcasecode)from lhcasetaskrela a where  servtaskno = '"+aTaskCust[j][6]+"'");
                            String sTaskName = t.getOneValue("select distinct (select distinct d.servtaskname from lhservtaskdef d where a.servtaskcode=d.servtaskcode)from lhcasetaskrela a where  servtaskno = '"+aTaskCust[j][6]+"'");
                            String sCustName = t.getOneValue(" select distinct name from ldperson where customerno = '"+aTaskCust[j][4]+"'");

                            CError tError = new CError();
                            tError.moduleName = "OLHServFeeAllBL";
                            tError.functionName = "dealData";
                            tError.errorMessage = "服务事件“"+sCaseName+"”的“"+sTaskName+"”任务（"+aTaskCust[j][6]+"）中的客户 "+sCustName+"（"+aTaskCust[j][4]+"）对应的险种号出错！";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                    }

                    LJAGetHealthSchema mLJAGetHealthSchema = new LJAGetHealthSchema();
                    mLJAGetHealthSchema.setActuGetNo(SNo);
                    mLJAGetHealthSchema.setOtherNo(aTaskCust[j][0]);
                    mLJAGetHealthSchema.setContNo(aTaskCust[j][1]);
                    mLJAGetHealthSchema.setGrpContNo(aTaskCust[j][2]);
                    mLJAGetHealthSchema.setRiskCode(aTaskCust[j][3]);
                    mLJAGetHealthSchema.setCustomerNo(aTaskCust[j][4]);
                    mLJAGetHealthSchema.setServItemNo(aTaskCust[j][5]);
                    mLJAGetHealthSchema.setServTaskNo(aTaskCust[j][6]);
                    mLJAGetHealthSchema.setPay(aTaskCust[j][7]);
                    mLJAGetHealthSchema.setPolNo(tPolno);//Polno
                    mLJAGetHealthSchema.setOperator(mGlobalInput.Operator);
                    mLJAGetHealthSchema.setManageCom(mGlobalInput.ManageCom);
                    mLJAGetHealthSchema.setMakeDate(PubFun.getCurrentDate());
                    mLJAGetHealthSchema.setMakeTime(PubFun.getCurrentTime());
                    mLJAGetHealthSchema.setModifyDate(PubFun.getCurrentDate());
                    mLJAGetHealthSchema.setModifyTime(PubFun.getCurrentTime());
                    mLJAGetHealthSet.add(mLJAGetHealthSchema);
                }

                map.put("update LHCaseTaskRela set ServTaskAffirm = '2' where ServTaskNo = '"
                        +mLHCaseTaskRelaSet.get(i).getServTaskNo()+"' ","UPDATE");
            }
            map.put(mLJAGetHealthSet, "INSERT");

            //操作LJAGet表
            String ServTaskNo="";
            for(int k=1; k<=mLHCaseTaskRelaSet.size(); k++)
            {
                ServTaskNo = ServTaskNo+",'"+mLHCaseTaskRelaSet.get(k).getServTaskNo()+"'";
            }
            ServTaskNo = ServTaskNo.substring(1);

            String aFee[][];
            String aHospitCode[][];
            String aFeeNum[][];
            SSRS vFee = new SSRS();
            ExeSQL eFee= new ExeSQL();

            aFeeNum = eFee.execSQL(" select distinct sum(feepay) from lhfeecharge where ServTaskNo in ("
                                +ServTaskNo+")").getAllData();

            aHospitCode = eFee.execSQL(" select distinct hospitcode from lhexecfeebalance "
                                +"where taskexecno in (select distinct feetaskexecno from lhfeecharge where ServTaskNo in ("
                                +ServTaskNo+"))").getAllData();

            vFee = eFee.execSQL("select distinct c.ContBalanceNo,c.BalanceType,c.BankCode,c.BankName, "
                         +" c.AccName,c.Accounts,c.Drawer from LHChargeBalance c,lhgroupcont h "
                         +" where h.contrano = c.contrano and h.hospitcode = '"+aHospitCode[0][0]+"'");

            if(vFee.getMaxRow() != 1)
            {
                CError tError = new CError();
                tError.moduleName = "OLHServFeeBL";
                tError.functionName = "dealData";
                tError.errorMessage = "此次结算对应的结算方式不唯一或者不存在，请确认！";
                this.mErrors.addOneError(tError);
                return false;
            }
            aFee = vFee.getAllData();

            mLJAGetSchema.setActuGetNo(6+SNo);
            mLJAGetSchema.setOtherNo(SNo);
            mLJAGetSchema.setOtherNoType("16");
            mLJAGetSchema.setPayMode(aFee[0][1]);
            mLJAGetSchema.setAccName(aFee[0][4]);
            mLJAGetSchema.setSumGetMoney(aFeeNum[0][0]);
            mLJAGetSchema.setShouldDate(PubFun.getCurrentDate());
            mLJAGetSchema.setBankCode(aFee[0][2]);
            mLJAGetSchema.setBankAccNo(aFee[0][5]);
            mLJAGetSchema.setDrawer(aFee[0][6]);
            mLJAGetSchema.setOperator(mGlobalInput.Operator);
            mLJAGetSchema.setManageCom(mGlobalInput.ManageCom);
            mLJAGetSchema.setMakeDate(PubFun.getCurrentDate());
            mLJAGetSchema.setMakeTime(PubFun.getCurrentTime());
            mLJAGetSchema.setModifyDate(PubFun.getCurrentDate());
            mLJAGetSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(mLJAGetSchema, "INSERT");
        }

        if (this.mOperate.equals("UPDATE||MAIN")){}
        if (this.mOperate.equals("DELETE||MAIN")){}
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData()
    {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLHCaseTaskRelaSet.set((LHCaseTaskRelaSet)cInputData.
                                    getObjectByObjectName("LHCaseTaskRelaSet", 0));
        if(mLHCaseTaskRelaSet == null)
        {
            CError tError = new CError();
            tError.moduleName = "OLHServFeeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "提交的任务数据mLHCaseTaskRelaSet为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        LJAGetHealthDB tLJAGetHealthDB = new LJAGetHealthDB();
        //如果有需要处理的错误，则返回
        if (tLJAGetHealthDB.mErrors.needDealError())
        {
                // @@错误处理
                this.mErrors.copyAllErrors(tLJAGetHealthDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "OLHServFeeBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            this.mInputData.add(this.mLJAGetHealthSet);
            this.mInputData.add(this.mLJAGetSchema);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLJAGetHealthSet);
            mResult.add(this.mLJAGetSchema);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHCustomTestBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }
}
