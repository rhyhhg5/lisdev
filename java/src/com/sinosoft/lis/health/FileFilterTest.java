/**
 *   药品无需过虑字符
 *
 */

package com.sinosoft.lis.health;

import java.io.*;
import java.lang.*;
import com.sinosoft.lis.db.LHQueryInfoDB;
import com.sinosoft.lis.vschema.LHQueryInfoSet;
import com.sinosoft.lis.schema.LHQueryInfoSchema;
import com.sinosoft.lis.health.FileImport;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class FileFilterTest
{
    public FileFilterTest()
    { try{ jbInit();} catch (Exception ex) {ex.printStackTrace();} }

    //用于生成文件名
    static int fileName = 40000001;
    static int num = 0;

    //临时文件名（任意取）
    String tempName = "temp.txt";

    //必须过滤的标签数组
    static String[] Filter_Key =
    {
    };

    //标题数组
    static String[] Title =
    {
         "检查名称","英文名称","相关疾病","概述","参考值","分析变异","方法学变异",
         "生物学变异","临床意义","药物影响","病理变异","病理学改变","医学决定性水平"
    };


    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private MMap map = new MMap();
    private VData mInputData = new VData();
    private LHQueryInfoSet mLHQueryInfoSet = new LHQueryInfoSet();

    /** 数据操作字符串 */
    private String mOperate;
    private String [][]loadData = new String[700][4];

    public static void main(String[] arg)
    {
        String func = "3";//1为过滤，2为倒入数据库，3为将文件拆分为每个疾病一个txt文件
        try
        {
            if(func.equals("1"))
            {
                String FilePath = "E:\\MyProject\\提取数据\\Filter\\medicine\\"; //主路径
                File mainFile = new File(FilePath);
                String[] mainFileName = mainFile.list();     //取得该路径下所有文件文件夹
                for (int FileNum = 0; FileNum < mainFileName.length; FileNum++)
                { //从主文件夹下取得为文件夹的文件对象
                    File branchFile = new File(FilePath + mainFileName[FileNum]);
                    FileFilterTest Fr = new FileFilterTest();
                    System.out.println("-----" + branchFile.toString() + "进入-----");
                    Fr.Filter(branchFile.toString());
                    System.out.println("-----" + branchFile.toString() + "完成-----");
                }
            }
            if(func.equals("2"))
            {
                String FilePath = "E:\\MyProject\\提取数据\\Filted\\medicine\\";
                File mainFile = new File(FilePath);
                String[] mainFileName = mainFile.list();     //取得该路径下所有文件文件夹
                for (int FileNum = 0; FileNum < mainFileName.length; FileNum++)
                { //从主文件夹下取得为文件夹的文件对象
                    File branchFile = new File(FilePath + mainFileName[FileNum]);
                    FileImport fi = new FileImport(branchFile.toString());
                    System.out.println(branchFile.toString());
                    fi.Import();
                }
            }
            if(func.equals("3"))
            {
                String FilePath = "E:\\MyProject\\提取数据\\DOWNLOAD\\提取数据\\检验\\SysUse\\";
                File mainFile = new File(FilePath);
                String[] mainFileName = mainFile.list();     //取得该路径下所有文件文件夹
                FileFilterTest Fr = new FileFilterTest();

                for (int mainFileNum = 0; mainFileNum < mainFileName.length; mainFileNum++)
                {   //从主文件夹下取得为文件夹的文件对象
                    String branchFile = FilePath + mainFileName[mainFileNum];
                    Fr.FileDispatch(branchFile);
                }

                Fr.submitData(Fr.loadData);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void Filter(String s)
    {
        try
        {
            int Filter_Num = 0;
            int Filter_LEN = Filter_Key.length;             //须过虑字符的个数
            File f = new File(s);
            File f1 = new File(s.replaceFirst("Filter","Filted"));
            FileReader in = new FileReader(f); 		    //读文件的实例变量
            FileInputStream fis = new FileInputStream(f);   //获得可读取字节数，以确定char数组长度
            FileWriter out = new FileWriter(f1);            //写文件的实例变量
            char[] buffer = new char[fis.available()];      // Read 一个文件大小的字节 at a time
            int len;
            if ((len = in.read(buffer)) != -1)
            {
                String s_temp = new String(buffer, 0, len); // Convert to a string
                while (Filter_Num < Filter_LEN - 1)
                {
                    System.out.println(Filter_Key[Filter_Num]);
                    s_temp = s_temp.replaceAll(Filter_Key[Filter_Num++],
                                     Filter_Key[Filter_Num]); //去掉
                    System.out.println(Filter_Key[Filter_Num++]);
                }
                out.write(s_temp);
            }
            out.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void FileDispatch(String s)
    {
        try
        {
            File f = new File(s);     //读文件实例
            FileReader in = new FileReader(f);
            FileInputStream fis = new FileInputStream(f);

            File tempFile = new File(tempName);
            String absPath = tempFile.getAbsoluteFile().toString();
            String usePath = absPath.replaceFirst("java\\\\src","ui\\\\healthmanage\\\\QueryFile\\\\Testitem");
            usePath = usePath.replaceFirst(tempName,"");
            System.out.println(usePath);

            char[] buffer = new char[fis.available()];           // Read 一个文件大小的字节 at a time
            int len;
            if ((len = in.read(buffer)) != -1)
            {
                String s_temp = new String(buffer, 0, len);      // Convert to a string
                dispatchFile(s_temp, usePath);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    void dispatchFile(String s, String filePath)
    {//两个换行符之间为一条信息
        int preEnter = s.indexOf("\n");
        int nextEnter = 0;
        int lastEnter = s.lastIndexOf("\n");

        while (preEnter != lastEnter)
        {
            s = s.replaceFirst("\n","∏");
            nextEnter = s.indexOf("\n");
            this.loadData[num][0] = ""+fileName;  //得到LHQueryInfo的ItemCode
            System.out.println(fileName);
            try
            {
                File outFile = new File(filePath + (fileName++) + ".txt");
                FileWriter out = new FileWriter(outFile);
                String temp = s.substring(preEnter+1, nextEnter);
                loadData[num][2] = "";

                temp = "＄<p class=itemname>"+temp;    //原提取数据没有CSS样式，为其增加CSS样式

                for(int i = 0; i < 5; i++ )
                {
                    temp = temp.replaceFirst("\\$", "</p>＄<p class=duanluo>");
                }

                temp = temp.replaceFirst("\\$", "</p>＄");
                temp = temp.replaceAll("＄", "\\$");

                //为每条信息增加内容说明，即 疾病名称、别名……这些
                for(int i = 0; i < Title.length; i++)
                {
                    int a = temp.indexOf("$");
                    temp  = temp.replaceFirst("\\$","＄");
                    int b = temp.indexOf("$");

                    if(i == 0)
                    {
                         String temp_temp = temp.substring(a).replaceFirst("<","");
                         temp_temp = temp_temp.substring(0,temp_temp.indexOf("<")+1);
                         loadData[num][i+1] = temp_temp.substring(temp_temp.indexOf(">")+1,temp_temp.indexOf("<"));
                    }

                    if(a == b-1)
                    {   //如果某一属性没有内容，则不显示<p>标签
                        temp = temp.replaceFirst("＄", "");
                        continue;
                    }
                    else
                    {   //属性有内容则添加标签
                        temp = temp.replaceFirst("＄", "<p class=title>"+Title[i]+"</p>");
                    }
                }
                temp = temp.replaceFirst("\\$", "");
                out.write(temp);
                out.close();
                preEnter = nextEnter;
                num++;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
    }

    private boolean dealData(String [][] s, int i)
    {
        for(int x = 0; x < 10 && x+i<num; x++)
        {
            LHQueryInfoSchema mLHQueryInfoSchema = new LHQueryInfoSchema();

            mLHQueryInfoSchema.setItemCode(s[x+i][0]);
            mLHQueryInfoSchema.setItemName(s[x+i][1]);
            mLHQueryInfoSchema.setItemType("2");
            mLHQueryInfoSchema.setItemOtherName(s[x+i][2]);
            mLHQueryInfoSchema.setItemKeyWord(s[x+i][3]);
            mLHQueryInfoSchema.setPageName(s[x+i][0]);
            mLHQueryInfoSchema.setPageSuffix("txt");
            mLHQueryInfoSchema.setPagePathFTP("healthmanage/QueryFile/Testitem/");
            mLHQueryInfoSchema.setManageCom("86");
            mLHQueryInfoSchema.setOperator("hm");
            mLHQueryInfoSchema.setMakeDate(PubFun.getCurrentDate());
            mLHQueryInfoSchema.setMakeTime(PubFun.getCurrentTime());
            mLHQueryInfoSchema.setModifyDate(PubFun.getCurrentDate());
            mLHQueryInfoSchema.setModifyTime(PubFun.getCurrentTime());

            mLHQueryInfoSet.add(mLHQueryInfoSchema);
        }
        map.put(mLHQueryInfoSet, "INSERT");
        return true;
    }

    public boolean submitData(String [][] s)
    {
        //将操作数据拷贝到本类中
        this.mOperate = "INSERT";
        int i = 0;
        for(; i < num; i = i + 10)
        {
            //进行业务处理
            mLHQueryInfoSet.clear();
            if (!dealData(s, i)) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "FileFilterTest-->Submit";
                tError.functionName = "dealData";
                tError.errorMessage = "将疾病数据拆分放入Set时出错";
                this.mErrors.addOneError(tError);
                return false;
            }
            //准备往后台的数据
            if (!prepareOutputData()) {
                return false;
            }

            //提交数据
            System.out.println("Start OLDTestGrpMgtBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                CError tError = new CError();
                tError.moduleName = "FileFilterTest-->Submit";
                tError.functionName = "PubSubmitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            this.mInputData.add(this.mLHQueryInfoSet);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLHQueryInfoSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "FileFilterTest-->Submit";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            ex.printStackTrace();
            return false;
        }
        return true;
    }


    private void jbInit() throws Exception {
    }
}
