/*
 * <p>ClassName: OLHServPlanBL </p>
 * <p>Description: OLHServPlanBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-03-09 10:43:01
 */
package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class OLHServPlanBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LHServPlanSchema mLHServPlanSchema = new LHServPlanSchema();
    private LHServItemSet mLHServItemSet = new LHServItemSet();
    private LHServExeTraceSet mLHServExeTraceSet = new LHServExeTraceSet();
    public OLHServPlanBL() {
    }

    public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData 输入的数据
     *         cOperate 数据操作
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLHServPlanBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLHServPlanBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start OLHServPlanBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();

            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                CError tError = new CError();
                tError.moduleName = "OLHServPlanBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }

        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     *
     */
    private boolean dealData() {
        System.out.println("--------DealData---------");
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            //增加存储之前的校验，如果已经存在同保单同客户同服务计划的服务事件，则不进行再次的保存。
            String test = " select * from lhservplan where contno='"+mLHServPlanSchema.getContNo()+"' and customerno='"+mLHServPlanSchema.getCustomerNo()+"'"
                         +" and servplancode='"+mLHServPlanSchema.getServPlanCode()+"' and ServPlayType='"+mLHServPlanSchema.getServPlayType()+"'";
            map.put(test,"SELECT");

            mLHServPlanSchema.setGrpServPlanNo("0000000");
            mLHServPlanSchema.setServPlanNo(PubFun1.CreateMaxNo("LHServPlanNo",20));
            mLHServPlanSchema.setOperator(mGlobalInput.Operator);
            mLHServPlanSchema.setMakeDate(PubFun.getCurrentDate());
            mLHServPlanSchema.setMakeTime(PubFun.getCurrentTime());
            mLHServPlanSchema.setManageCom(mGlobalInput.ManageCom);
            mLHServPlanSchema.setModifyDate(PubFun.getCurrentDate());
            mLHServPlanSchema.setModifyTime(PubFun.getCurrentTime());

            map.put(mLHServPlanSchema, "INSERT");
            System.out.println("------------mLHServPlanSchema----------------");
            if (mLHServItemSet != null && mLHServItemSet.size() > 0)
            {
                System.out.println("***********" + mLHServItemSet.size());
                for (int i = 1; i <= mLHServItemSet.size(); i++)
                {
                    mLHServItemSet.get(i).setGrpServPlanNo(mLHServPlanSchema.getGrpServPlanNo());
                    mLHServItemSet.get(i).setServPlanNo(mLHServPlanSchema.getServPlanNo());
                    mLHServItemSet.get(i).setGrpServItemNo("0000000");
                    mLHServItemSet.get(i).setCustomerNo(mLHServPlanSchema.getCustomerNo());
                    mLHServItemSet.get(i).setContNo(mLHServPlanSchema.getContNo());
                    mLHServItemSet.get(i).setName(mLHServPlanSchema.getName());
                    mLHServItemSet.get(i).setServItemNo(PubFun1.CreateMaxNo("LHServItemNoSet", 20));
                    mLHServItemSet.get(i).setOperator(mGlobalInput.Operator);
                    mLHServItemSet.get(i).setManageCom(mGlobalInput.ManageCom);
                    mLHServItemSet.get(i).setMakeDate(PubFun.getCurrentDate());
                    mLHServItemSet.get(i).setMakeTime(PubFun.getCurrentTime());
                    mLHServItemSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLHServItemSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
                map.put(mLHServItemSet, "INSERT");

                for (int i = 1; i <= mLHServExeTraceSet.size(); i++)
                {

                    mLHServExeTraceSet.get(i).setServPlanNo(mLHServPlanSchema.getServPlanNo());
                    mLHServExeTraceSet.get(i).setServItemNo(mLHServItemSet.get(i).getServItemNo());
                    mLHServExeTraceSet.get(i).setExeState("0");
                    mLHServExeTraceSet.get(i).setExeDate("1000-01-01");
                    mLHServExeTraceSet.get(i).setContNo(mLHServPlanSchema.getContNo());
                    mLHServExeTraceSet.get(i).setServDesc("无");
                    mLHServExeTraceSet.get(i).setOperator(mGlobalInput.Operator);
                    mLHServExeTraceSet.get(i).setManageCom(mGlobalInput.ManageCom);
                    mLHServExeTraceSet.get(i).setMakeDate(PubFun.getCurrentDate());
                    mLHServExeTraceSet.get(i).setMakeTime(PubFun.getCurrentTime());
                    mLHServExeTraceSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLHServExeTraceSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
                map.put(mLHServExeTraceSet, "INSERT");
            }
        }
        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            //  mLHServPlanSchema.setGrpServPlanNo("0000000");
//            mLHServPlanSchema.setServPlanNo(PubFun1.CreateMaxNo("LHComID", 20));
            //  mLHServPlanSchema.setServPlanCode("0000000");
            //    mLHServPlanSchema.setServPlayType("000000");
            mLHServPlanSchema.setOperator(mGlobalInput.Operator);
            mLHServPlanSchema.setManageCom(mGlobalInput.ManageCom);
            mLHServPlanSchema.setModifyDate(PubFun.getCurrentDate());
            mLHServPlanSchema.setModifyTime(PubFun.getCurrentTime());
            map.put(mLHServPlanSchema, "DELETE&INSERT");

            for (int i = 1; i <= mLHServItemSet.size(); i++)
            {
                mLHServItemSet.get(i).setGrpServPlanNo(mLHServPlanSchema.getGrpServPlanNo());
                mLHServItemSet.get(i).setServPlanNo(mLHServPlanSchema.getServPlanNo());
                mLHServItemSet.get(i).setGrpServItemNo("0000000");
                mLHServItemSet.get(i).setCustomerNo(mLHServPlanSchema.getCustomerNo());
                mLHServItemSet.get(i).setContNo(mLHServPlanSchema.getContNo());
                mLHServItemSet.get(i).setName(mLHServPlanSchema.getName());

                if (mLHServItemSet.get(i).getServItemNo() == null ||
                    mLHServItemSet.get(i).getServItemNo().equals(""))
                {
                    mLHServItemSet.get(i).setServItemNo(PubFun1.CreateMaxNo(
                            "LHServItemNoSet", 20));
                }
                mLHServItemSet.get(i).setOperator(mGlobalInput.Operator);
                mLHServItemSet.get(i).setManageCom(mGlobalInput.ManageCom);
                mLHServItemSet.get(i).setModifyDate(PubFun.getCurrentDate());
                mLHServItemSet.get(i).setModifyTime(PubFun.getCurrentTime());
            }
            map.put("delete from LHServItem where ServPlanNo = '" +
                    mLHServPlanSchema.getServPlanNo() + "'", "DELETE");
            map.put(mLHServItemSet, "INSERT");
        }

        if (this.mOperate.equals("DELETE||MAIN"))
        {
            map.put(mLHServPlanSchema, "DELETE");
            map.put("delete from LHServExeTrace where ServPlanNo = '" +
                    mLHServPlanSchema.getServPlanNo() + "'", "DELETE");
            if (mLHServItemSet.size() > 0)
            {
                map.put(mLHServItemSet, "DELETE");
            }
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     *
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true\
     * @return boolean
     * @param cInputData
     */
    private boolean getInputData(VData cInputData) {
        this.mLHServPlanSchema.setSchema((LHServPlanSchema) cInputData.
             getObjectByObjectName("LHServPlanSchema", 0));
        this.mLHServItemSet.set((LHServItemSet) cInputData.
             getObjectByObjectName("LHServItemSet", 0));
//        this.mLHServExeTraceSet.set((LHServExeTraceSet) cInputData.
//             getObjectByObjectName("LHServExeTraceSet",0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
             getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     * @return boolean
     */
    private boolean submitquery() {
        this.mResult.clear();
        LHServPlanDB tLHServPlanDB = new LHServPlanDB();
        tLHServPlanDB.setSchema(this.mLHServPlanSchema);
        //如果有需要处理的错误，则返回
        if (tLHServPlanDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHServPlanDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHServPlanBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 准备往后层处理所需要的数据
     * @return boolean
     */

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLHServPlanSchema);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLHServPlanSchema);
            mResult.add(this.mLHServItemSet);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServPlanBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 返回结果
     * @return VData
     */

    public VData getResult() {
        return this.mResult;
    }
}
