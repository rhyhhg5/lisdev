package com.sinosoft.lis.health;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import java.io.*;
import java.util.*;
import java.net.InetAddress;

import com.g1.doc1.archive.RenderClient;
import com.g1.doc1.archive.IndexSearchType;
import com.g1.doc1.archive.FormatType;
import freemarker.template.*;
import com.lowagie.text.pdf.*;

public class ArchiveServlet extends HttpServlet {
    private String appRoot = null;
    private static String e2Host = "10.252.128.132";
    private static String e2Port = "6003";
    private static String dbName = "PICC";
    private static WebFormStorage storage = null;

    public final static int INDEXNO_ACCOUNT = 1;
    public final static int INDEXNO_DATE = 4;

    public void init(ServletConfig config) throws ServletException {
        ServletContext context = config.getServletContext();
        appRoot = context.getRealPath("/");
        if (!appRoot.endsWith(File.separator)) {
            appRoot = appRoot + File.separator;
        }
        storage = new WebFormStorage(new File(appRoot + "templates"));
    }

    public void service(HttpServletRequest request,
                        HttpServletResponse response) throws ServletException,
            IOException {
        request.setCharacterEncoding("UTF-8");
        String action = request.getParameter("action");
        if ("display".equals(action)) {
            display(request, response);
        } else if ("render".equals(action)) {
            render(request, response);
        } else if ("viewPDF".equals(action)) {
            viewPDF(request, response);
        } else if ("print".equals(action)) {
            print(request, response);
        }

    }

    private void print(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RenderClient client = getRenderClient();
        RenderClient.Database e2db = getDatabase(client);
        String acc = request.getParameter("account");
        String file = request.getParameter("filename");
        RenderClient.Document doc = searchDocument(e2db, acc, file);
        if (doc == null) {
            return;
        }

        // Write a PDF file for the document
        String ext = ".pdf";
        long timestamp = System.currentTimeMillis();
        String docFilename = appRoot + "cache" + File.separator + timestamp + ext;
        FileOutputStream fos = new FileOutputStream(docFilename);
        doc.renderAllPagesToStream( fos, FormatType.PDF );
        fos.close();
        releaseRenderClient(client);

        // Add Print JS code for the PDF document
        List pdfnames = new ArrayList();
        pdfnames.add(docFilename);
        String copyFilename = appRoot + "cache" + File.separator + timestamp + "-js" + ext;
        PdfRender pdfRender = new PdfRender();
        File copyPDF = pdfRender.concatPDF(copyFilename, pdfnames);

        // Write the merged PDF back to client
        response.setContentType("application/pdf");
        renderFile(copyPDF, response.getOutputStream());
        copyPDF.delete();
    }



    private void viewPDF(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RenderClient client = getRenderClient();
        RenderClient.Database e2db = getDatabase(client);
        String acc = request.getParameter("account");
        String file = request.getParameter("filename");
        RenderClient.Document doc = searchDocument(e2db, acc, file);
        if (doc == null) {
            return;
        }
        // Write a PDF file for the document
        String ext = ".pdf";
        long timestamp = System.currentTimeMillis();
        String docFilename = appRoot + "cache" + File.separator + timestamp + ext;
        File docFile = new File(docFilename);
        FileOutputStream fos = new FileOutputStream(docFile);
        doc.renderAllPagesToStream( fos, FormatType.PDF );
        fos.close();
        releaseRenderClient(client);

        // Write the PDF file back to client
        response.setContentType("application/pdf");
        renderFile(docFile, response.getOutputStream());
        docFile.delete();
    }

    private void render(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String file = request.getParameter("file");
        File docFile = new File(file);
        if (!docFile.exists()) {
            return;
        }
        ServletOutputStream sos = response.getOutputStream();
        renderFile(docFile, sos);
        docFile.delete();
    }

    private void display(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RenderClient client = getRenderClient();
        RenderClient.Database e2db = getDatabase(client);
        RenderClient.Document doc = null;
        String acc = request.getParameter("account");
        String date = request.getParameter("date");
        String file = request.getParameter("filename");
        Map hash = new HashMap();

        IndexSearchType dateIndex = e2db.getIndexSearch(INDEXNO_DATE);
        List docs = e2db.searchForDocuments(acc, dateIndex, date, RenderClient.SEARCH_RESULTS_ANY_ORDER);
        if (docs == null || docs.size() == 0) {
            throw new IllegalArgumentException("Does not fetch a document under this account number: " + acc + ".");
        }
        List docList = new ArrayList();
        for (int i = 0; i < docs.size(); i++) {
            boolean bSelected = false;
            RenderClient.Document d = (RenderClient.Document) docs.get(i);
            if (i == 0 && file == null) {
                // Fetch the most recent document under this account
                doc = d;
                bSelected = true;
            }
            if (d.getFileName().equals(file)) {
                // Fetch the matched document under this account
                doc = d;
                bSelected = true;
            }
            Map docMap = new HashMap();
            docMap.put("selected", Boolean.valueOf(bSelected));
            docMap.put("date", d.getDate());
            docMap.put("filename", d.getFileName());
            docList.add(docMap);
        }
        hash.put("list_doc", docList);
        if (doc == null) {
            throw new IllegalArgumentException("Does not match a single, unique document under this account number: " + acc + ".");
        }

        System.out.println("Date under this account: " + doc.getDate());
        System.out.println("File name: " + doc.getFileName());

        hash.put("type", "gif");
        int pageCount = doc.getPageCount(FormatType.GIF);
        hash.put("pagecount", new Integer(pageCount));
        hash.put("filename", doc.getFileName());
        hash.put("account", acc);
        int page = getIntParameter(request, "page");
        if (page == Integer.MIN_VALUE) {
            // Display the first page of the document
            page = 1;
        }
        hash.put("pagenum", new Integer(page));

        // Write file of the document
        String ext = ".gif";
        String docFilename = System.currentTimeMillis() + ext;
        docFilename = appRoot + "cache" + File.separator + docFilename;
        File docFile = new File(docFilename);
        FileOutputStream fos = new FileOutputStream(docFile);
        //doc.renderAllPagesToStream( fos, FormatType.PDF );
        doc.renderPagesToStream( fos, FormatType.GIF, 640, page, 1 );
        fos.close();
        releaseRenderClient(client);


        hash.put("filepath", docFilename);
        hash.put("page", "view");
        paint(request, response, "/display.html", hash);
    }

    private RenderClient getRenderClient() throws IOException {
        RenderClient client = new RenderClient();
        client.connect(InetAddress.getByName(e2Host),
                       Integer.parseInt(e2Port));
        return client;
    }

    private RenderClient.Database getDatabase(RenderClient client) throws
            IOException {
        return (RenderClient.Database) client.getDatabase(dbName);
    }

    private void releaseRenderClient(RenderClient client) throws IOException {
        client.close();
    }

    private void renderFile(File file, ServletOutputStream sos) throws IOException {
        FileInputStream fis = new FileInputStream(file);

        BufferedOutputStream bos = new BufferedOutputStream(sos,
                1024 * 1024);
        int c = -1;
        int n = 0;
        byte[] bytes = new byte[1024 * 1024];
        while ((c = fis.read(bytes)) != -1) {
            bos.write(bytes, 0, c);
            bos.flush();
            n += c;
        }
        fis.close();
    }

    private void paint(HttpServletRequest req, HttpServletResponse res, String template, Map hash) {
        hash.put("db_name", dbName);
        res.setContentType("text/html; charset=UTF-8");
        WebForm form = storage.getWebForm(template);
        try {
            Writer out = new BufferedWriter(new OutputStreamWriter(res.getOutputStream(), "UTF-8"));
            form.render(out, hash);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (WebFormException e) {
            e.printStackTrace();
        }
    }

    private RenderClient.Document searchDocument(RenderClient.Database e2db, String account, String filename) throws IOException {
        IndexSearchType dateIndex = e2db.getIndexSearch(INDEXNO_DATE);
        List docs = e2db.searchForDocuments(account, dateIndex, null, RenderClient.SEARCH_RESULTS_ANY_ORDER);
        if (docs == null || docs.size() == 0) {
            throw new IllegalArgumentException("Does not fetch a document under this account number: " + account + ".");
        }

        RenderClient.Document doc = null;
        for (int i = 0; i < docs.size(); i++) {
            RenderClient.Document d = (RenderClient.Document) docs.get(i);
            if (d.getFileName().equals(filename)) {
                // Fetch the matched document under this account
                doc = d;
                break;
            }
        }
        if (doc == null) {
            throw new IllegalArgumentException("Does not match a single, unique document under this account number: " + account + ".");
        }

        System.out.println("Date under this account: " + doc.getDate());
        System.out.println("File name: " + doc.getFileName());

        return doc;
    }

    private int getIntParameter(HttpServletRequest req, String key) {
        try {
            int v = Integer.parseInt(req.getParameter(key));
            return v;
        } catch (Exception e) {
            return Integer.MIN_VALUE;
        }
    }
}
