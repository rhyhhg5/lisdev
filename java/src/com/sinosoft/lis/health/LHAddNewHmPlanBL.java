/*
 * <p>ClassName: LHAddNewHmPlanBL  </p>
 * <p>Description: LHAddNewHmPlanBL 类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-07-06 10:10:52
 */
package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class LHAddNewHmPlanBL   {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
private MMap map= new MMap();
/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
/** 业务处理相关变量 */
private LHServCaseDefSchema  mLHServCaseDefSchema=new LHServCaseDefSchema ();
private LHServCaseRelaSchema mLHServCaseRelaSchema = new LHServCaseRelaSchema();
public LHAddNewHmPlanBL () {
}
public static void main(String[] args) {
}
/**
* 传输数据的公共方法
        * @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "LHAddNewHmPlanBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败LHAddNewHmPlanBL -->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
      System.out.println("Start LHAddNewHmPlanBL  Submit...");
/*      LHAddNewHmPlanBL S tLHAddNewHmPlanBL S=new LHAddNewHmPlanBL S();
      tLHAddNewHmPlanBL S.submitData(mInputData,mOperate);
      System.out.println("End LHAddNewHmPlanBL  Submit...");
      //如果有需要处理的错误，则返回
      if (tLHAddNewHmPlanBL S.mErrors.needDealError())
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tLHAddNewHmPlanBL S.mErrors);
        CError tError = new CError();
        tError.moduleName = "LHAddNewHmPlanBL ";
        tError.functionName = "submitDat";
        tError.errorMessage ="数据提交失败!";
        this.mErrors .addOneError(tError) ;
        return false;
      }
 */
         PubSubmit tPubSubmit = new PubSubmit();
         if (!tPubSubmit.submitData(mInputData, mOperate))
         {
           // @@错误处理
           this.mErrors.copyAllErrors(tPubSubmit.mErrors);
           CError tError = new CError();
           tError.moduleName = "LHAddNewHmPlanBL";
           tError.functionName = "submitData";
           tError.errorMessage = "数据提交失败!";

           this.mErrors.addOneError(tError);
           return false;
         }
         System.out.println("End LHAddNewHmPlanBL Submit...");

       }
       mInputData = null;
       return true;
     }

     /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
  if (this.mOperate.equals("INSERT||MAIN"))
    {
      //mLDDoctorSchema.setOperator(mGlobalInput.Operator);
      mLHServCaseDefSchema.setManageCom(this.mGlobalInput.ManageCom);
      mLHServCaseDefSchema.setOperator(this.mGlobalInput.Operator);
      mLHServCaseDefSchema.setMakeDate(PubFun.getCurrentDate());
      mLHServCaseDefSchema.setMakeTime(PubFun.getCurrentTime());
      mLHServCaseDefSchema.setModifyDate(PubFun.getCurrentDate());
      mLHServCaseDefSchema.setModifyTime(PubFun.getCurrentTime());

      map.put(mLHServCaseDefSchema, "INSERT");

      mLHServCaseRelaSchema.setManageCom(this.mGlobalInput.ManageCom);
     mLHServCaseRelaSchema.setOperator(this.mGlobalInput.Operator);
     mLHServCaseRelaSchema.setMakeDate(PubFun.getCurrentDate());
     mLHServCaseRelaSchema.setMakeTime(PubFun.getCurrentTime());
     mLHServCaseRelaSchema.setModifyDate(PubFun.getCurrentDate());
     mLHServCaseRelaSchema.setModifyTime(PubFun.getCurrentTime());
     map.put(mLHServCaseRelaSchema, "INSERT");
    }

    if (this.mOperate.equals("UPDATE||MAIN"))
    {
        mLHServCaseDefSchema.setManageCom(this.mGlobalInput.ManageCom);
        mLHServCaseDefSchema.setOperator(this.mGlobalInput.Operator);
        mLHServCaseDefSchema.setMakeDate(PubFun.getCurrentDate());
        mLHServCaseDefSchema.setMakeTime(PubFun.getCurrentTime());
        mLHServCaseDefSchema.setModifyDate(PubFun.getCurrentDate());
        mLHServCaseDefSchema.setModifyTime(PubFun.getCurrentTime());
      map.put(mLHServCaseDefSchema, "UPDATE");
    }

    if (this.mOperate.equals("DELETE||MAIN"))
    {
      map.put(mLHServCaseDefSchema, "DELETE");
    }

    return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
         this.mLHServCaseDefSchema.setSchema((LHServCaseDefSchema)cInputData.getObjectByObjectName("LHServCaseDefSchema",0));
          this.mLHServCaseRelaSchema.setSchema((LHServCaseRelaSchema)cInputData.getObjectByObjectName("LHServCaseRelaSchema",0));
         this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
    this.mResult.clear();
    LHServCaseDefDB tLHServCaseDefDB=new LHServCaseDefDB();
    tLHServCaseDefDB.setSchema(this.mLHServCaseDefSchema);
                //如果有需要处理的错误，则返回
                if (tLHServCaseDefDB.mErrors.needDealError())
                {
                  // @@错误处理
                        this.mErrors.copyAllErrors(tLHServCaseDefDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "LHAddNewHmPlanBL";
                        tError.functionName = "submitData";
                        tError.errorMessage = "数据提交失败!";
                        this.mErrors .addOneError(tError) ;
                        return false;
    }
    LHServCaseRelaDB tLHServCaseRelaDB=new LHServCaseRelaDB();
   tLHServCaseRelaDB.setSchema(this.mLHServCaseRelaSchema);
               //如果有需要处理的错误，则返回
               if (tLHServCaseRelaDB.mErrors.needDealError())
               {
                 // @@错误处理
                       this.mErrors.copyAllErrors(tLHServCaseRelaDB.mErrors);
                       CError tError = new CError();
                       tError.moduleName = "LHAddNewHmPlanBL";
                       tError.functionName = "submitData";
                       tError.errorMessage = "数据提交失败!";
                       this.mErrors .addOneError(tError) ;
                       return false;
   }

    mInputData=null;
    return true;
}
 private boolean prepareOutputData()
 {
   try
        {
                this.mInputData.clear();
                this.mInputData.add(this.mLHServCaseDefSchema);
                this.mInputData.add(this.mLHServCaseRelaSchema);
                mInputData.add(map);
                mResult.clear();
    mResult.add(this.mLHServCaseDefSchema);
    mResult.add(this.mLHServCaseRelaSchema);
        }
        catch(Exception ex)
        {
                // @@错误处理
                CError tError =new CError();
                tError.moduleName="LHAddNewHmPlanBL";
                tError.functionName="prepareData";
                tError.errorMessage="在准备往后层处理所需要的数据时出错。";
                this.mErrors .addOneError(tError) ;
                return false;
        }
        return true;
        }
        public VData getResult()
        {
        return this.mResult;
        }
}
