package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLHServCaseChangeStateBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LHServCaseDefSet mLHServCaseDefSet = new LHServCaseDefSet();

    public OLHServCaseChangeStateBL() {}
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLHServCaseChangeStateBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLHServCaseChangeStateBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        System.out.println("Start OLHServCaseChangeStateBL Submit...");
        PubSubmit tPubSubmit = new PubSubmit();

        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "OLHServCaseChangeStateBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */

    private boolean dealData()
    {

        for(int i = 1; i <= mLHServCaseDefSet.size(); i++)
        {
            LHServCaseDefDB mLHServCaseDefDB = new LHServCaseDefDB();
            mLHServCaseDefDB.setServCaseCode(mLHServCaseDefSet.get(i).getServCaseCode());

            if(mLHServCaseDefDB.getInfo() == false)
            {
                CError tError = new CError();
                tError.moduleName = "OLHServCaseChangeStateBL";
                tError.functionName = "dealData";
                tError.errorMessage = "事件号: "+mLHServCaseDefSet.get(i).getServCaseCode()+" 无效";
                this.mErrors.addOneError(tError);
                return false;
            }

            LHServCaseDefSet tLHServCaseDefSet = mLHServCaseDefDB.query();
            tLHServCaseDefSet.get(1).setServCaseState(mLHServCaseDefSet.get(i).getServCaseState());
            mLHServCaseDefSet.get(i).setSchema( tLHServCaseDefSet.get(1));
            mLHServCaseDefSet.get(i).setModifyDate(PubFun.getCurrentDate());
            mLHServCaseDefSet.get(i).setModifyTime(PubFun.getCurrentTime());
        }
//        map.put(" update LHServCaseDef set ServCaseState = '"+mLHServCaseDefSet.get(i).getServCaseState()
//                    +"' where ServCaseCode = '"+mLHServCaseDefSet.get(i).getServCaseCode()+"'", "UPDATE");
        map.put(mLHServCaseDefSet,"UPDATE");
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData()
    {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLHServCaseDefSet.set((LHServCaseDefSet)cInputData.getObjectByObjectName("LHServCaseDefSet",0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            mInputData.add(this.mLHServCaseDefSet);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLHServCaseDefSet);
        } catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLHServCaseChangeStateBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    public static void main(String[] arg)
    {

        LHServCaseDefSet tLHServCaseDefSet = new LHServCaseDefSet();
        OLHServCaseChangeStateUI tOLHServCaseChangeStateUI = new OLHServCaseChangeStateUI();

        LHServCaseDefSchema tLHServCaseDefSchema = new LHServCaseDefSchema();
        tLHServCaseDefSchema.setServCaseCode("20060807100005");
        tLHServCaseDefSchema.setServCaseState("3");
        tLHServCaseDefSet.add(tLHServCaseDefSchema);

        LHServCaseDefSchema tLHServCaseDefSchema2 = new LHServCaseDefSchema();
        tLHServCaseDefSchema2.setServCaseCode("20060807100006");
        tLHServCaseDefSchema2.setServCaseState("2");
        tLHServCaseDefSet.add(tLHServCaseDefSchema2);

        VData tVData = new VData();
        tVData.add(tLHServCaseDefSet);
        tOLHServCaseChangeStateUI.submitData(tVData, "INSERT||MAIN");
    }

}
