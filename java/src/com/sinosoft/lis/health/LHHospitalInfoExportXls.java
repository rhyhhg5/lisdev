package com.sinosoft.lis.health;

import java.util.*;
import java.io.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LHTaskCustomerRelaSchema;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import com.sinosoft.lis.easyscan.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: </p>
 * @author guoly
 * @version 1.0
 */

public class LHHospitalInfoExportXls
{
    public LHHospitalInfoExportXls() {}
    String FullPath = "";
    String  []FileSheetNum;
    String ZipPathName="";
    String FilePath="";
    public static void main(String[] args)
    {
    }

    public boolean CreateZip(String[] tFilePaths, String[] tFileNames, String tZipPath)
    {
        ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
        if (!tProposalDownloadBL.CreatZipFile(tFilePaths, tFileNames, tZipPath))
        {
            System.out.println("生成压缩文件失败");
            CError.buildErr(this,"生成压缩文件失败");
            return false;
        }
        return true;
    }

    public boolean DownloadSubmit(String tHospitCode,String tRiskCode,String ZipPath)
    {
        try {
          ZipPath = ZipPath + "healthmanage/";
          String Path;
          Path = PubFun.getCurrentDate() + PubFun.getCurrentTime() + ".xls";
          Path = Path.replaceAll(":", "-");
          WriteToExcel t = new WriteToExcel(Path);
          String[] FilePaths = new String[1];
          FilePaths[0] = ZipPath + Path;
          String[] FileNames = new String[1];
          FileNames[0] = "LLFeeMainExport.xls"; //PubFun.getCurrentDate()+PubFun.getCurrentTime();
          String[][] mToExcel = new String[1][19];
          mToExcel[0][0] = "客户号";
          mToExcel[0][1] = "姓名";
          mToExcel[0][2] = "就诊方式";
          mToExcel[0][3] = "就诊日期";
          mToExcel[0][4] = "主要诊断";
          mToExcel[0][5] = "住院天数";
          mToExcel[0][6] = "费用总额";
          mToExcel[0][7] = "药品费";
          mToExcel[0][8] = "检查费";
          mToExcel[0][9] = "手术费";
          mToExcel[0][10] = "治疗费";
          mToExcel[0][11] = "护理费";
          mToExcel[0][12] = "诊疗费";
          mToExcel[0][13] = "体检费";
          mToExcel[0][14] = "其他费用";
          mToExcel[0][15] = "自付费用";
          mToExcel[0][16] = "公司支付费用";
          mToExcel[0][17] = "赔付险种类别";
          mToExcel[0][18] = "医疗机构名称";
          t.createExcelFile();
          String[] sheetName = {"ExcuteState"};
          t.addSheet(sheetName);
          t.setData(0, mToExcel);
          String sqlx = "select distinct m.CustomerNo ,m.Customername ,"
                      +" (select l.codename from ldcode l  where l.codetype='hminhospitalmode' and l.code=m.FeeType ),"
                      +" m.FeeDate,'',m.RealHospDate, "
                      +" m.SumFee,"
                      +"(select value(sum (b.Fee),0)   from llcasereceipt b where b.Feeitemcode in ('202','203','204','121') and b.Mainfeeno=m.Mainfeeno),"//药品
                      +"(select value(sum (b.Fee),0)   from llcasereceipt b where b.Feeitemcode in ('205','107','207','111','209','112','110','214') and b.Mainfeeno=m.Mainfeeno),"//检查费
                      +"(select value(sum (b.Fee),0)   from llcasereceipt b where b.Feeitemcode in ('208','113','217') and b.Mainfeeno=m.Mainfeeno),"//手术
                      +"(select value(sum (b.Fee),0)   from llcasereceipt b where b.Feeitemcode in ('206','104','221','124','210','211') and b.Mainfeeno=m.Mainfeeno),"//治疗费
                      +"(select value(sum (b.Fee),0)   from llcasereceipt b where b.Feeitemcode in ('212') and b.Mainfeeno=m.Mainfeeno),"//护理费
                      +"(select value(sum (b.Fee),0)   from llcasereceipt b where b.Feeitemcode in ('213','106') and b.Mainfeeno=m.Mainfeeno),"//诊疗费
                      +"(select value(sum (b.Fee),0)   from llcasereceipt b where b.Feeitemcode in ('115','227') and b.Mainfeeno=m.Mainfeeno),"//体检费
                      +"(select value(sum (b.Fee),0)   from llcasereceipt b where b.Feeitemcode in ('226','201','218','219','220','222','118','223','123','224','216','212','114','225') and b.Mainfeeno=m.Mainfeeno),"//其他费用
                      +" m.SelfAmnt,value((select  r.RealPay from llclaim r where r.caseno=m.caseno),0),"
                      +"  (select distinct  r.Riskname  from lmriskapp R , llclaimpolicy p where p.RiskCode=r.Riskcode and  p.CaseNo=m.CaseNo and p.CaseRelaNo=m.CaseRelaNo) ,"
                      +" m.Hospitalname"
                      +" from LLFeeMain m"
                      +"  where "
                      +"  m.HospitalCode='"+tHospitCode+"' "
                      +" and m.caseno in ( select distinct caseno from llclaimpolicy "
                      +" where riskcode in ('"+tRiskCode+"')) "
                      ;
          System.out.println(sqlx);
          t.setData(0,sqlx);
          System.out.println("ZipPath  : " + ZipPath);
          t.write(ZipPath);
          String NewPath;
          NewPath = PubFun.getCurrentDate() + PubFun.getCurrentTime() +
                    ".zip";
          NewPath = NewPath.replaceAll(":", "-");
          FullPath = NewPath;
          NewPath = ZipPath + NewPath;
          CreateZip(FilePaths, FileNames, NewPath);
          File fd = new File(FilePaths[0]);
          fd.delete();
          System.out.println("FullPath!  : " + NewPath);
        /*  File Zipd = new File(NewPath);
          Zipd.delete();
         */

      } catch (Exception ex) {
          ex.toString();
          ex.printStackTrace();
      }
      return true;

    }

    public String getResult()
    {
        System.out.println("FullPath@  : " + FullPath);
        return FullPath;
    }

}
