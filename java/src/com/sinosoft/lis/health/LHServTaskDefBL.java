/*
 * <p>ClassName: LHServTaskDefBL </p>
 * <p>Description: LHServTaskDefBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-08-02 10:26:38
 */
package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class LHServTaskDefBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
private MMap map= new MMap();

/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
private String tNo;
/** 业务处理相关变量 */
private LHServTaskDefSchema mLHServTaskDefSchema=new LHServTaskDefSchema();
private LHCaseTaskRelaSet mLHCaseTaskRelaSet=new LHCaseTaskRelaSet();


public LHServTaskDefBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
public static void main(String[] args) {
}
/**
* 传输数据的公共方法
        * @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "LHServTaskDefBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败LHServTaskDefBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
      PubSubmit tPubSubmit = new PubSubmit();
      System.out.println("Start LHServTaskDefBL Submit...");
      //LHServTaskDefBLS tLHServTaskDefBLS=new LHServTaskDefBLS();
      //tLHServTaskDefBLS.submitData(mInputData,mOperate);
      if (!tPubSubmit.submitData(mInputData, mOperate))
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);

        CError tError = new CError();
        tError.moduleName = "LHServTaskDefBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据提交失败!";

        this.mErrors.addOneError(tError);
        return false;
      }

      System.out.println("End LHServTaskDefBL Submit...");
      //如果有需要处理的错误，则返回
      //if (tLHServTaskDefBLS.mErrors.needDealError())
      //{
        // @@错误处理
        //this.mErrors.copyAllErrors(tLHServTaskDefBLS.mErrors);
       // CError tError = new CError();
        //tError.moduleName = "LHServTaskDefBL";
        //tError.functionName = "submitDat";
        //tError.errorMessage ="数据提交失败!";
        //this.mErrors .addOneError(tError) ;
        //return false;
      //}
    }
    mInputData=null;
    return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
    if (this.mOperate.equals("INSERT||MAIN"))
    {
        if (mLHCaseTaskRelaSet != null && mLHCaseTaskRelaSet.size() > 0)
        {
            for (int j = 1; j <= mLHCaseTaskRelaSet.size(); j++)
            {
                mLHCaseTaskRelaSet.get(j).setServTaskNo(PubFun1.CreateMaxNo("ServTaskNo", 12));
                mLHCaseTaskRelaSet.get(j).setOperator(mGlobalInput.Operator);
                mLHCaseTaskRelaSet.get(j).setMakeDate(PubFun.getCurrentDate());
                mLHCaseTaskRelaSet.get(j).setMakeTime(PubFun.getCurrentTime());
                mLHCaseTaskRelaSet.get(j).setModifyDate(PubFun.getCurrentDate());
                mLHCaseTaskRelaSet.get(j).setModifyTime(PubFun.getCurrentTime());
                mLHCaseTaskRelaSet.get(j).setManageCom(mGlobalInput.ManageCom);
            }
            map.put(mLHCaseTaskRelaSet, "INSERT");
        }
    }

    if (this.mOperate.equals("UPDATE||MAIN"))
    {
        for (int j= 1; j<= mLHCaseTaskRelaSet.size(); j++)
        {
            if (mLHCaseTaskRelaSet.get(j).getServTaskNo() == null ||
                mLHCaseTaskRelaSet.get(j).getServTaskNo().equals(""))
            {
                mLHCaseTaskRelaSet.get(j).setServTaskNo(PubFun1.CreateMaxNo("ServTaskNo",12));
            }

            mLHCaseTaskRelaSet.get(j).setOperator(mGlobalInput.Operator);
            mLHCaseTaskRelaSet.get(j).setMakeDate(PubFun.getCurrentDate());
            mLHCaseTaskRelaSet.get(j).setMakeTime(PubFun.getCurrentTime());
            mLHCaseTaskRelaSet.get(j).setModifyDate(PubFun.getCurrentDate());
            mLHCaseTaskRelaSet.get(j).setModifyTime(PubFun.getCurrentTime());
            mLHCaseTaskRelaSet.get(j).setManageCom(mGlobalInput.ManageCom);

            map.put(" delete from  LHCaseTaskRela where ServCaseCode='" +
                    mLHCaseTaskRelaSet.get(j).getServCaseCode() + "'","DELETE");
            map.put(mLHCaseTaskRelaSet, "INSERT");
        }

        //Delete relative info of the ServTask
        LHCaseTaskRelaSet deletedSet = new LHCaseTaskRelaSet();
        LHCaseTaskRelaSet preSet = new LHCaseTaskRelaSet();

        LHCaseTaskRelaDB preCaseTaskRelaDB = new LHCaseTaskRelaDB();
        preCaseTaskRelaDB.setServCaseCode(mLHCaseTaskRelaSet.get(1).getServCaseCode());
        preSet = preCaseTaskRelaDB.query();

aaa:           for(int t=1; t<=preSet.size(); t++)
        {
            for(int t2=1; t2<=mLHCaseTaskRelaSet.size(); t2++)
            {
                if(preSet.get(t).getServTaskCode().equals(mLHCaseTaskRelaSet.get(t2).getServTaskCode()))
                {
                    continue aaa;
                }
            }
            deletedSet.add(preSet.get(t));
        }

        for(int d=1; d<=deletedSet.size(); d++)
        {
            map.put("delete from LHTASKCUSTOMERRELA WHERE ServTaskNo = '"+deletedSet.get(d).getServTaskNo()+"'","DELETE");
            map.put("delete from LHEvaReport WHERE ServTaskNo = '"+deletedSet.get(d).getServTaskNo()+"'","DELETE");
            map.put("delete from LHExecFeeBalance WHERE ServTaskNo = '"+deletedSet.get(d).getServTaskNo()+"'","DELETE");
            map.put("delete from LHFeeCharge WHERE ServTaskNo = '"+deletedSet.get(d).getServTaskNo()+"'","DELETE");
            map.put("delete from LHHmServBeManage WHERE ServTaskNo = '"+deletedSet.get(d).getServTaskNo()+"'","DELETE");
            map.put("delete from LHMessSendMg WHERE ServTaskNo = '"+deletedSet.get(d).getServTaskNo()+"'","DELETE");
            map.put("delete from LHQuesImportMain WHERE ServTaskNo = '"+deletedSet.get(d).getServTaskNo()+"'","DELETE");
            map.put("delete from LHQuesImportDetail WHERE cusregistno in ( select distinct cusregistno from LHQuesImportmain where ServTaskNo = '"+deletedSet.get(d).getServTaskNo()+"')","DELETE");
            map.put("delete from LHEvalueExpMain WHERE ServTaskNo = '"+deletedSet.get(d).getServTaskNo()+"'","DELETE");
            map.put("delete from LHEvalueExpDetail WHERE cusevalcode in ( select distinct cusevalcode from LHEvalueExpmain where ServTaskNo = '"+deletedSet.get(d).getServTaskNo()+"')","DELETE");
        }
    }

    if (this.mOperate.equals("DELETE||MAIN"))
    {
        for (int j = 1; j <= mLHCaseTaskRelaSet.size(); j++)
        {
            map.put(" delete from  LHCaseTaskRela where ServCaseCode='" +
                    mLHCaseTaskRelaSet.get(1).getServCaseCode() +
                    //"' and ServTaskCode='" +
                    //  mLHCaseTaskRelaSet.get(j).getServTaskCode() +
                    "'and ServTaskNo='" + mLHCaseTaskRelaSet.get(j).getServTaskNo() +
                    "'", "DELETE");
        }
        //map.put(mLHCaseTaskRelaSet, "DELETE");
    }

    return true;
}
/**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
 */
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{

         this.mLHCaseTaskRelaSet.set((LHCaseTaskRelaSet)cInputData.getObjectByObjectName("LHCaseTaskRelaSet",0));



         this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
    this.mResult.clear();
    LHContraAssoSettingDB tLHContraAssoSettingDB=new LHContraAssoSettingDB();
//    tLHContraAssoSettingDB.set(this.mLHContraAssoSettingSet);
                //如果有需要处理的错误，则返回
                if (tLHContraAssoSettingDB.mErrors.needDealError())
                {
                  // @@错误处理
                        this.mErrors.copyAllErrors(tLHContraAssoSettingDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "LHServTaskDefBL";
                        tError.functionName = "submitData";
                        tError.errorMessage = "数据提交失败!";
                        this.mErrors .addOneError(tError) ;
                        return false;
    }
    mInputData=null;
    return true;
}
 private boolean prepareOutputData()
 {
   try
        {
                this.mInputData.clear();

                this.mInputData.add(this.mLHCaseTaskRelaSet);
                mInputData.add(map);
                mResult.clear();

                mResult.add(this.mLHCaseTaskRelaSet);
        }
        catch(Exception ex)
        {
                // @@错误处理
                CError tError =new CError();
                tError.moduleName="LHServTaskDefBL";
                tError.functionName="prepareData";
                tError.errorMessage="在准备往后层处理所需要的数据时出错。";
                this.mErrors .addOneError(tError) ;
                return false;
        }
        return true;
        }
        public VData getResult()
        {
        return this.mResult;
        }

    private void jbInit() throws Exception {
    }
}
