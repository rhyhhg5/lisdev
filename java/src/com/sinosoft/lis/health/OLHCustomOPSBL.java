/*
 * <p>ClassName: OLHCustomOPSBL </p>
 * <p>Description: OLHCustomOPSBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-17 17:17:33
 */
package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class OLHCustomOPSBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
MMap map = new MMap();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
private String tNo;
/** 业务处理相关变量 */
private LHCustomOPSSchema mLHCustomOPSSchema=new LHCustomOPSSchema();
//private LHCustomOPSSet mLHCustomOPSSet=new LHCustomOPSSet();
public OLHCustomOPSBL() {
}
public static void main(String[] args) {
}
/**
* 传输数据的公共方法
	* @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "OLHCustomOPSBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败OLHCustomOPSBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
      System.out.println("Start OLHCustomOPSBL Submit...");
      OLHCustomOPSBLS tOLHCustomOPSBLS=new OLHCustomOPSBLS();
      //tOLHCustomOPSBLS.submitData(mInputData,mOperate);
      System.out.println("End OLHCustomOPSBL Submit...");
      //如果有需要处理的错误，则返回
      if (tOLHCustomOPSBLS.mErrors.needDealError())
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tOLHCustomOPSBLS.mErrors);
        CError tError = new CError();
        tError.moduleName = "OLHCustomOPSBL";
        tError.functionName = "submitDat";
        tError.errorMessage ="数据提交失败!";
        this.mErrors .addOneError(tError) ;
        return false;
      }
    }
    PubSubmit tPubSubmit = new PubSubmit();
    tPubSubmit.submitData(mInputData, mOperate);
    mInputData=null;
    return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
  if(this.mOperate.equals("INSERT||MAIN"))
{
    try {
      SSRS tSSRS = new SSRS();
      String sql = "Select Case When max(OPSNo) Is Null Then 0 Else max(OPSNo) End from LHCustomOPS where CustomerNo='"
          + mLHCustomOPSSchema.getCustomerNo() + "'";
      ExeSQL tExeSQL = new ExeSQL();
      tSSRS = tExeSQL.execSQL(sql);
      Integer firstinteger = Integer.valueOf(tSSRS.GetText(1, 1));
      int ttNo = firstinteger.intValue() + 1;
      Integer integer = new Integer(ttNo);
      tNo = integer.toString();
      System.out.println("得到的地址码是：" + tNo);
      mLHCustomOPSSchema.setOPSNo(tNo);

    }
    catch (Exception e) {
      CError tError = new CError();
      tError.moduleName = "ContIsuredBL";
      tError.functionName = "createAddressNo";
      tError.errorMessage = "地址码超长,生成号码失败,请先删除原来的超长地址码!";
      this.mErrors.addOneError(tError);
      mLHCustomOPSSchema.setOPSNo("");
    }

    mLHCustomOPSSchema.setOperator(mGlobalInput.Operator);
    mLHCustomOPSSchema.setMakeDate(PubFun.getCurrentDate());
    mLHCustomOPSSchema.setMakeTime(PubFun.getCurrentTime());
    mLHCustomOPSSchema.setModifyDate(PubFun.getCurrentDate());
    mLHCustomOPSSchema.setModifyTime(PubFun.getCurrentTime());
    map.put(mLHCustomOPSSchema, "INSERT");
}

if(this.mOperate.equals("UPDATE||MAIN"))
{
  mLHCustomOPSSchema.setModifyDate(PubFun.getCurrentDate());
  mLHCustomOPSSchema.setModifyTime(PubFun.getCurrentTime());
  map.put(mLHCustomOPSSchema, "UPDATE");
}

if(this.mOperate.equals("DELETE||MAIN"))
{
  map.put(mLHCustomOPSSchema, "DELETE");
}

  return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
         this.mLHCustomOPSSchema.setSchema((LHCustomOPSSchema)cInputData.getObjectByObjectName("LHCustomOPSSchema",0));
         this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
    this.mResult.clear();
    LHCustomOPSDB tLHCustomOPSDB=new LHCustomOPSDB();
    tLHCustomOPSDB.setSchema(this.mLHCustomOPSSchema);
		//如果有需要处理的错误，则返回
		if (tLHCustomOPSDB.mErrors.needDealError())
 		{
		  // @@错误处理
 			this.mErrors.copyAllErrors(tLHCustomOPSDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "LHCustomOPSBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors .addOneError(tError) ;
 			return false;
    }
    mInputData=null;
    return true;
}
 private boolean prepareOutputData()
 {
   try
	{
		this.mInputData.clear();
		this.mInputData.add(this.mLHCustomOPSSchema);
                this.mInputData.add(map);
		mResult.clear();
    mResult.add(this.mLHCustomOPSSchema);
	}
	catch(Exception ex)
	{
 		// @@错误处理
		CError tError =new CError();
 		tError.moduleName="LHCustomOPSBL";
 		tError.functionName="prepareData";
 		tError.errorMessage="在准备往后层处理所需要的数据时出错。";
 		this.mErrors .addOneError(tError) ;
		return false;
	}
	return true;
	}
	public VData getResult()
	{
  	return this.mResult;
	}
}
