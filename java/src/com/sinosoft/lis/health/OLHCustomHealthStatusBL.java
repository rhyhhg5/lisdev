/*
 * <p>ClassName: OLHCustomHealthStatusBL </p>
 * <p>Description: OLHCustomHealthStatusBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-18 10:11:20
 */
package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLHCustomHealthStatusBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String tNo;
    /** 业务处理相关变量 */
    private LHCustomHealthStatusSchema mLHCustomHealthStatusSchema = new
            LHCustomHealthStatusSchema();
//private LHCustomGymSchema mLHCustomGymSchema=new LHCustomGymSchema();
//private LHCustomFamilyDiseasSchema mLHCustomFamilyDiseasSchema=new LHCustomFamilyDiseasSchema();
    private LHCustomGymSet mLHCustomGymSet = new LHCustomGymSet(); //客户健身信息
    private LHCustomFamilyDiseasSet mLHCustomFamilyDiseasSet = new
            LHCustomFamilyDiseasSet(); //家庭疾病
//private LHCustomHealthStatusSet mLHCustomHealthStatusSet=new LHCustomHealthStatusSet();
    public OLHCustomHealthStatusBL() {
    }

    public static void main(String[] args) {
//        VData tVData = new VData();
//        OLHCustomHealthStatusUI tOLHCustomHealthStatusUI = new
//                OLHCustomHealthStatusUI();
//        LHCustomHealthStatusSchema tLHCustomHealthStatusSchema = new
//                LHCustomHealthStatusSchema();
//        LHCustomGymSchema tLHCustomGymSchema = new LHCustomGymSchema();
//        LHCustomFamilyDiseasSchema tLHCustomFamilyDiseasSchema = new
//                LHCustomFamilyDiseasSchema();
//        tLHCustomHealthStatusSchema.setCustomerNo("123123");
//        tLHCustomHealthStatusSchema.setAddDate("2005-02-22");
//        tLHCustomHealthStatusSchema.setStature("3");
//        tLHCustomHealthStatusSchema.setAvoirdupois("12");
//        tLHCustomHealthStatusSchema.setAvoirdIndex("12");
//        tLHCustomHealthStatusSchema.setBloodPressHigh("324");
//        tLHCustomHealthStatusSchema.setBloodPressLow("324");
//        tLHCustomHealthStatusSchema.setSmoke("3");
//        tLHCustomHealthStatusSchema.setKissCup("43");
//        tLHCustomHealthStatusSchema.setSitUp("234");
//        tLHCustomHealthStatusSchema.setDiningNoRule("34");
//        tLHCustomHealthStatusSchema.setBadHobby("4");
//
//        tLHCustomGymSchema.setCustomerNo("34");
//        tLHCustomGymSchema.setGymItemCode("4");
//        tLHCustomGymSchema.setGymFreque("4");
//        tLHCustomGymSchema.setGymTime("34");
//
//        tLHCustomFamilyDiseasSchema.setCustomerNo("43");
//        tLHCustomFamilyDiseasSchema.setFamilyCode("3");
//        tLHCustomFamilyDiseasSchema.setExplai("32");
//        tVData.add(tLHCustomHealthStatusSchema);
//        tVData.add(tLHCustomGymSchema);
//        tVData.add(tLHCustomFamilyDiseasSchema);
//        GlobalInput tG = new GlobalInput();
//        tG.Operator = "001";
//        tG.ComCode = "86";
//        tG.ManageCom = "8600";
//        tVData.add(tG);
//        tOLHCustomHealthStatusUI.submitData(tVData, "INSERT||MAIN");
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        System.out.println("mOperate:" + mOperate);
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLHCustomHealthStatusBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLHCustomHealthStatusBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start OLHCustomHealthStatusBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();

            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                CError tError = new CError();
                tError.moduleName = "OLHCustomHealthStatusBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }

            System.out.println("---commitData---");

        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        if (this.mOperate.equals("INSERT||MAIN")) {

            try {
                SSRS tSSRS = new SSRS();
                String sql = "Select Case When max(to_number(HealthNo)) Is Null Then 0 Else max(to_number(HealthNo)) End from LHCustomHealthStatus where CustomerNo='"
                             + mLHCustomHealthStatusSchema.getCustomerNo() +
                             "'";
                ExeSQL tExeSQL = new ExeSQL();
                tSSRS = tExeSQL.execSQL(sql);
                Integer firstinteger = Integer.valueOf(tSSRS.GetText(1, 1));
                int ttNo = firstinteger.intValue() + 1;
                Integer integer = new Integer(ttNo);
                tNo = integer.toString();

                // System.out.println("得到的HealthNo是：" + tNo);
                mLHCustomHealthStatusSchema.setHealthNo(tNo);

            } catch (Exception e) {
                CError tError = new CError();
                tError.moduleName = "ContIsuredBL";
                tError.functionName = "createAddressNo";
                tError.errorMessage = "setHealthNo失败!";
                this.mErrors.addOneError(tError);
                mLHCustomHealthStatusSchema.setHealthNo("");
            }

            mLHCustomHealthStatusSchema.setOperator(mGlobalInput.Operator);
            mLHCustomHealthStatusSchema.setMakeDate(PubFun.getCurrentDate());
            mLHCustomHealthStatusSchema.setMakeTime(PubFun.getCurrentTime());
            mLHCustomHealthStatusSchema.setModifyDate(PubFun.getCurrentDate());
            mLHCustomHealthStatusSchema.setModifyTime(PubFun.getCurrentTime());
            mLHCustomHealthStatusSchema.setManageCom(mGlobalInput.ManageCom);
            map.put(mLHCustomHealthStatusSchema, "INSERT");

            if (mLHCustomGymSet != null && mLHCustomGymSet.size() > 0) {
                int j_Customergymno = 1;
                for (int i = 1; i <= mLHCustomGymSet.size(); i++) {

//                    try {
//                        SSRS tSSRS = new SSRS();
//                        String sql = "Select Case When max(to_number(Customergymno)) Is Null Then 0 Else max(to_number(Customergymno)) End from lhcustomgym where CustomerNo='"
//                                     +
//                                     mLHCustomHealthStatusSchema.
//                                     getCustomerNo() +
//                                     "'";
//                        ExeSQL tExeSQL = new ExeSQL();
//                        tSSRS = tExeSQL.execSQL(sql);
//                        Integer firstinteger = Integer.valueOf(tSSRS.
//                                GetText(1, 1));
//                        int ttNo = firstinteger.intValue() + (j_Customergymno++);
//                        Integer integer = new Integer(ttNo);
//                        tNo = integer.toString();
//                        mLHCustomGymSet.get(i).setCustomerGymNo(
//                                tNo);
//
//                    } catch (Exception e) {
//                        CError tError = new CError();
//                        tError.moduleName = "OLHCustomHealthStatusBL";
//                        tError.functionName = "setFamilyDiseaseNO";
//                        tError.errorMessage = "setFamilyDiseaseNO失败!";
//                        this.mErrors.addOneError(tError);
//                        mLHCustomGymSet.get(i).setCustomerGymNo(
//                                "");
//                    }
                    mLHCustomGymSet.get(i).setCustomerGymNo(
                            mLHCustomHealthStatusSchema.getHealthNo());
                    mLHCustomGymSet.get(i).setOperator(mGlobalInput.
                            Operator);
                    mLHCustomGymSet.get(i).setMakeDate(PubFun.
                            getCurrentDate());
                    mLHCustomGymSet.get(i).setMakeTime(PubFun.
                            getCurrentTime());
                    mLHCustomGymSet.get(i).setModifyDate(PubFun.
                            getCurrentDate());
                    mLHCustomGymSet.get(i).setModifyTime(PubFun.
                            getCurrentTime());
                    mLHCustomGymSet.get(i).setManageCom(mGlobalInput.ManageCom);
                }
                map.put(mLHCustomGymSet, "INSERT");
            }

            if (mLHCustomFamilyDiseasSet != null &&
                mLHCustomFamilyDiseasSet.size() > 0) {
                int j_FamilyDiseasNo = 1;
                for (int i = 1; i <= mLHCustomFamilyDiseasSet.size(); i++) {

                    mLHCustomFamilyDiseasSet.get(i).setFamilyDiseaseNO(
                            mLHCustomHealthStatusSchema.getHealthNo());
                    mLHCustomFamilyDiseasSet.get(i).setOperator(
                            mGlobalInput.
                            Operator);
                    mLHCustomFamilyDiseasSet.get(i).setMakeDate(PubFun.
                            getCurrentDate());
                    mLHCustomFamilyDiseasSet.get(i).setMakeTime(PubFun.
                            getCurrentTime());
                    mLHCustomFamilyDiseasSet.get(i).setModifyDate(PubFun.
                            getCurrentDate());
                    mLHCustomFamilyDiseasSet.get(i).setModifyTime(PubFun.
                            getCurrentTime());
                    mLHCustomFamilyDiseasSet.get(i).setManageCom(mGlobalInput.ManageCom);

                }
                map.put(mLHCustomFamilyDiseasSet, "INSERT");
            }

        }

        if (this.mOperate.equals("UPDATE||MAIN")) {
            mLHCustomHealthStatusSchema.setModifyDate(PubFun.getCurrentDate());
            mLHCustomHealthStatusSchema.setModifyTime(PubFun.getCurrentTime());
            mLHCustomHealthStatusSchema.setOperator(mGlobalInput.Operator);
            mLHCustomHealthStatusSchema.setManageCom(mGlobalInput.ManageCom);
            map.put(mLHCustomHealthStatusSchema, "DELETE&INSERT");

//            if (mLHCustomGymSet != null && mLHCustomGymSet.size() > 0) {
            int j_Customergymno = 1;
            for (int i = 1; i <= mLHCustomGymSet.size(); i++) {
//                if (mLHCustomGymSet.get(i).getCustomerGymNo() == null ||
//                    mLHCustomGymSet.get(i).getCustomerGymNo().
//                    equals("")) {
//                    try {
//                        SSRS tSSRS = new SSRS();
//                        String sql = "Select Case When max(to_number(CustomerGymNo)) Is Null Then 0 Else max(to_number(CustomerGymNo)) End from LHCustomGym where CustomerNo='"
//                                     +
//                                     mLHCustomHealthStatusSchema.
//                                     getCustomerNo() +
//                                     "'";
//                        ExeSQL tExeSQL = new ExeSQL();
//                        tSSRS = tExeSQL.execSQL(sql);
//                        Integer firstinteger = Integer.valueOf(tSSRS.
//                                GetText(1, 1));
//                        int ttNo = firstinteger.intValue() +
//                                   (j_Customergymno++);
//                        Integer integer = new Integer(ttNo);
//                        tNo = integer.toString();
//                        mLHCustomGymSet.get(i).setCustomerGymNo(
//                                tNo);
//
//                    } catch (Exception e) {
//                        CError tError = new CError();
//                        tError.moduleName = "OLHCustomHealthStatusBL";
//                        tError.functionName = "setCustomerGymNo";
//                        tError.errorMessage = "setCustomerGymNo失败!";
//                        this.mErrors.addOneError(tError);
//                        mLHCustomGymSet.get(i).setCustomerGymNo(
//                                "");
//                    }
//
//                }
                mLHCustomGymSet.get(i).setCustomerGymNo(
                        mLHCustomHealthStatusSchema.getHealthNo());
                mLHCustomGymSet.get(i).setOperator(mGlobalInput.
                        Operator);

                mLHCustomGymSet.get(i).setModifyDate(PubFun.
                        getCurrentDate());
                mLHCustomGymSet.get(i).setModifyTime(PubFun.
                        getCurrentTime());
                mLHCustomGymSet.get(i).setManageCom(mGlobalInput.ManageCom);
            }
            map.put("delete from LHCustomGym where CustomerNo = '" +
                    mLHCustomHealthStatusSchema.getCustomerNo() + "' and "
                    + " CustomerGymNo ='" +
                    mLHCustomHealthStatusSchema.getHealthNo() + "'",
                    "DELETE");
            map.put(mLHCustomGymSet, "INSERT");
//            }

//            if (mLHCustomFamilyDiseasSet != null &&
//                mLHCustomFamilyDiseasSet.size() > 0) {
            int j_FamilyDiseasNo = 1;
            for (int i = 1; i <= mLHCustomFamilyDiseasSet.size(); i++) {
//                if (mLHCustomFamilyDiseasSet.get(i).getFamilyDiseaseNO() == null ||
//                    mLHCustomFamilyDiseasSet.get(i).getFamilyDiseaseNO().
//                    equals("")) {
//                    try {
//                        SSRS tSSRS = new SSRS();
//                        String sql = "Select Case When max(to_number(FamilyDiseaseNO)) Is Null Then 0 Else max(to_number(FamilyDiseaseNO)) End from LHCustomFamilyDiseas where CustomerNo='"
//                                     +
//                                     mLHCustomHealthStatusSchema.
//                                     getCustomerNo() +
//                                     "'";
//                        ExeSQL tExeSQL = new ExeSQL();
//                        tSSRS = tExeSQL.execSQL(sql);
//                        Integer firstinteger = Integer.valueOf(tSSRS.
//                                GetText(1, 1));
//                        int ttNo = firstinteger.intValue() +
//                                   (j_FamilyDiseasNo++);
//                        Integer integer = new Integer(ttNo);
//                        tNo = integer.toString();
//                        mLHCustomFamilyDiseasSet.get(i).setFamilyDiseaseNO(
//                                tNo);
//
//                    } catch (Exception e) {
//                        CError tError = new CError();
//                        tError.moduleName = "OLHCustomHealthStatusBL";
//                        tError.functionName = "setFamilyDiseaseNO";
//                        tError.errorMessage = "setFamilyDiseaseNO失败!";
//                        this.mErrors.addOneError(tError);
//                        mLHCustomFamilyDiseasSet.get(i).setFamilyDiseaseNO(
//                                "");
//                    }
//
//                }
                mLHCustomFamilyDiseasSet.get(i).setFamilyDiseaseNO(
                        mLHCustomHealthStatusSchema.getHealthNo());
                mLHCustomFamilyDiseasSet.get(i).setOperator(
                        mGlobalInput.
                        Operator);

                mLHCustomFamilyDiseasSet.get(i).setModifyDate(PubFun.
                        getCurrentDate());
                mLHCustomFamilyDiseasSet.get(i).setModifyTime(PubFun.
                        getCurrentTime());
                mLHCustomFamilyDiseasSet.get(i).setManageCom(mGlobalInput.ManageCom);
            }
            map.put("delete from LHCustomFamilyDiseas where CustomerNo = '" +
                    mLHCustomHealthStatusSchema.getCustomerNo() + "'"
                    + " and FamilyDiseaseNO = '" +
                    mLHCustomHealthStatusSchema.getHealthNo() + "'"
                    , "DELETE");
            map.put(mLHCustomFamilyDiseasSet, "INSERT");
//                map.put(mLHCustomFamilyDiseasSet, "DELETE&INSERT");
//            }

        }
        if (this.mOperate.equals("DELETE||MAIN")) {
            map.put(mLHCustomHealthStatusSchema, "DELETE");
            map.put(mLHCustomGymSet, "DELETE");
            map.put(mLHCustomFamilyDiseasSet, "DELETE");
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLHCustomHealthStatusSchema.setSchema((LHCustomHealthStatusSchema)
                cInputData.getObjectByObjectName(
                        "LHCustomHealthStatusSchema",
                        0));
        this.mLHCustomGymSet.set((LHCustomGymSet) cInputData.
                                 getObjectByObjectName("LHCustomGymSet", 0));
        this.mLHCustomFamilyDiseasSet.set((LHCustomFamilyDiseasSet) cInputData.
                                          getObjectByObjectName(
                                                  "LHCustomFamilyDiseasSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LHCustomHealthStatusDB tLHCustomHealthStatusDB = new
                LHCustomHealthStatusDB();
        tLHCustomHealthStatusDB.setSchema(this.mLHCustomHealthStatusSchema);
        //如果有需要处理的错误，则返回
        if (tLHCustomHealthStatusDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHCustomHealthStatusDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "OLHCustomHealthStatusBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLHCustomHealthStatusSchema);
            this.mInputData.add(this.mLHCustomGymSet);
            this.mInputData.add(this.mLHCustomFamilyDiseasSet);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLHCustomHealthStatusSchema);
            mResult.add(this.mLHCustomGymSet);
            mResult.add(this.mLHCustomFamilyDiseasSet);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLHCustomHealthStatusBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
