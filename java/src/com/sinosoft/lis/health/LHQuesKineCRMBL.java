/*
 * <p>ClassName: LHQuesKineCRMBL </p>
 * <p>Description: LHQuesKineCRMBL类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-10-27 15:08:38
 */
package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class LHQuesKineCRMBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
private MMap map= new MMap();

/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
private String tNo;
private String BeforeCurrent;
/** 业务处理相关变量 */
private LHQuesImportMainSet mLHQuesImportMainSet=new LHQuesImportMainSet();//问卷录入管理表

private LHServCaseDefSet mLHServCaseDefSet= new LHServCaseDefSet();//LHServCaseDef（服务事件定义表），封装所有的Case，用于比较
private LHServCaseDefSet mLHServCaseDefSetForIns= new LHServCaseDefSet();//LHServCaseDef（服务事件定义表），只封装新增的Case

private LHServCaseRelaSet mLHServCaseRelaSet= new LHServCaseRelaSet();//LHServCaseRela（服务事件关联表），封装所有的Case，用于比较
private LHServCaseRelaSet mLHServCaseRelaSetForIns= new LHServCaseRelaSet();//LHServCaseRela（服务事件关联表），只封装新增的Case

private LHCaseTaskRelaSet mLHCaseTaskRelaSet=new LHCaseTaskRelaSet();//LHCaseTaskRela（事件任务关联表）
private LHTaskCustomerRelaSet mLHTaskCustomerRelaSet=new LHTaskCustomerRelaSet();//LHTaskCustomerRela（任务客户关联表）



    public LHQuesKineCRMBL()
    {
        try
        {
            jbInit();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
public static void main(String[] args)
{
    LHQuesKineCRMBL tLHQuesKineCRMBL=new LHQuesKineCRMBL();
    tLHQuesKineCRMBL.TaskExecEntry();
}
 public boolean TaskExecEntry()
 {
     BeforeCurrent = new ExeSQL().getOneValue(" select current date - 1 day from dual ");

     String sqlAllCount =" select count(*) from LHQuesImportMain a where a.WhethAchieve in('2','3') "
                         +" and a.Source ='1' and a.ModifyDate='"+BeforeCurrent+"' and a.Taskexecno is null "
                         +" and a.servitemno not in (select distinct m.servitemno from LHQuesImportMain m where m.Taskexecno is not null)"
                         ;
     String AllCount= new ExeSQL().getOneValue(sqlAllCount);//总的已提交的并且数据源不是6的数据亘
     System.out.println("统计的提交单的数量"+AllCount);
     int NullNum = Integer.parseInt(AllCount);

     if(this.LHServCaseDefInfo(NullNum) == false)//（服务事件定义表）
     {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "LHQuesKineCRMBL";
        tError.functionName = "LHServCaseDefInfo(NullNum)";
        tError.errorMessage = "创建LHServCaseDef表数据失败!";
        this.mErrors.addOneError(tError);
        return false;
    }

     if(this.LHServCaseRelaInfo(NullNum) == false)//（服务事件关联表）
     {
         // @@错误处理
         CError tError = new CError();
         tError.moduleName = "LHQuesKineCRMBL";
         tError.functionName = "LHServCaseRelaInfo(NullNum)";
         tError.errorMessage = "创建HServCaseRela表数据失败!";
         this.mErrors.addOneError(tError);
         return false;
     }

     if(this.LHCaseTaskRelaInfo(NullNum) == false)//（事件任务关联表）
     {
         // @@错误处理
         CError tError = new CError();
         tError.moduleName = "LHQuesKineCRMBL";
         tError.functionName = "LHCaseTaskRelaInfo(NullNum)";
         tError.errorMessage = "创建LHCaseTaskRela表数据失败!";
         this.mErrors.addOneError(tError);
         return false;
     }

     if(this.LHTaskCustomerRelaInfo(NullNum) == false)//（任务客户关联表）
     {
         // @@错误处理
         CError tError = new CError();
         tError.moduleName = "LHQuesKineCRMBL";
         tError.functionName = "LHTaskCustomerRelaInfo(NullNum)";
         tError.errorMessage = "创建LHTaskCustomerRela表数据失败!";
         this.mErrors.addOneError(tError);
         return false;
     }

     if(this.LHQuesImportMainInfo(NullNum) == false)//（问卷录入管理表）
     {
         // @@错误处理
         CError tError = new CError();
         tError.moduleName = "LHQuesKineCRMBL";
         tError.functionName = "LHQuesImportMainInfo(NullNum)";
         tError.errorMessage = "创建LHQuesImportMain表数据失败!";
         this.mErrors.addOneError(tError);
         return false;
     }


     if (!prepareOutputData())
     {
         // @@错误处理
         CError tError = new CError();
         tError.moduleName = "LHQuesKineCRMBL";
         tError.functionName = "prepareOutputData";
         tError.errorMessage = "数据处理失败LHQuesKineCRMBL-->prepareOutputData!";
         this.mErrors.addOneError(tError);
         return false;
     }

     PubSubmit tPubSubmit = new PubSubmit();
     System.out.println("Start LHQuesKineCRMBL Submit...");
     if (!tPubSubmit.submitData(mInputData, mOperate)) {
         // @@错误处理
         this.mErrors.copyAllErrors(tPubSubmit.mErrors);
         CError tError = new CError();
         tError.moduleName = "LHQuesKineCRMBL";
         tError.functionName = "submitData";
         tError.errorMessage = "数据提交失败!";

         this.mErrors.addOneError(tError);
         return false;
     }

     System.out.println("End LHQuesKineCRMBL Submit...");

     return true;
 }

 public boolean LHServCaseDefInfo(int num)
 {
     String transact = "INSERT||MAIN";

     SSRS tSSRS_ManaOpera = new SSRS();
     ExeSQL tExeManaOpera = new ExeSQL();
     String temp_anaOpera[][]; //管理机构和操作员
     //查找有几个CRM传入的数据
     String sqlManaOpera =
            " select a.ManageCom,a.Operator,a.ServItemNo,a.CustomerNo from LHQUESIMPORTMAIN a "
            + " where a.Whethachieve in('2','3') and a.Source ='1' and "
            + " a.ModifyDate='" + BeforeCurrent + "' and a.Taskexecno is null  "
            +" and a.servitemno not in (select distinct m.servitemno from LHQuesImportMain m where m.Taskexecno is not null) order by a.Cusregistno"
            ;
     tSSRS_ManaOpera = tExeManaOpera.execSQL(sqlManaOpera);
     temp_anaOpera = tSSRS_ManaOpera.getAllData();

     for (int i = 1; i <= num; i++)
     {
         String ServItemNo = temp_anaOpera[i - 1][2]; //服务项目代码
         String CustomerNo = temp_anaOpera[i - 1][3]; //服务项目代码对应的客户代码
         //先查找服务项目是否有对应启动事件
         String sqlBelongCase =
                 "select distinct d.servcasecode from lhservcaserela r,lhservcasedef d "
                 + " where  r.servitemno = '" + ServItemNo + "' "
                 +" and r.servcasecode = d.servcasecode and d.servcasestate = '1'"
                 ;
         String dataBelongCase[][] = new ExeSQL().execSQL(sqlBelongCase).getAllData();
         //如果此服务项目有对应启动服务事件，只判断启动事件数量为1的情况。业务保证只有一个，所以如果出现多个启动事件，新生成
         if (dataBelongCase.length == 1)
         {
             LHServCaseDefDB tLHServCaseDefDB = new LHServCaseDefDB();
             tLHServCaseDefDB.setServCaseCode(dataBelongCase[0][0]);

             if(!tLHServCaseDefDB.getInfo())
             {// @@错误处理
                 CError tError = new CError();
                 tError.moduleName = "LHQuesKineCRMBL";
                 tError.functionName = "LHServCaseDefInfo";
                 tError.errorMessage = "LHServCaseDefInfo没有得到数据，请确认!";
                 this.mErrors.addOneError(tError);
                 return false;
             }
             mLHServCaseDefSet.add(tLHServCaseDefDB.getSchema());
         }
         else
         {//服务项目没有对应的启动服务事件
             String MStartDate = PubFun.getCurrentDate().replaceAll("-", "");
             String HiddenCaseCode = MStartDate + "1";
             String ServCaseCode = "";
             SSRS tSSRS = new SSRS();
             ExeSQL tExeSQL = new ExeSQL();
             String sql =
                     "select max(substr(ServCaseCode, 10,5)) from LHServCaseDef where makedate='" +
                     PubFun.getCurrentDate() + "'";
             tSSRS = tExeSQL.execSQL(sql);
             String rr[][] = tSSRS.getAllData();
             String result = rr[0][0].toString();
             if (result.equals("") || result.equals(null) || result.equals("null"))
             {
                 ServCaseCode = HiddenCaseCode + ServCaseCode + "0" + "000" + i;
             }
             else
             {
                 Integer ii = new Integer(result); //结果转为整型
                 int iii = ii.intValue();
                 int oo = iii++; //在数据库最大值加上‘1’
                 Integer result2 = new Integer(oo + i); //判断队列里有多少个
                 String result3 = result2.toString();
                 String result4 = "";
                 if (result3.length() == 4) {
                     result4 = "0" + result3.toString();
                 } else if (result3.length() == 3) {
                     result4 = "00" + result3.toString();
                 } else if (result3.length() == 2) {
                     result4 = "000" + result3.toString();
                 } else if (result3.length() == 1) {
                     result4 = "0000" + result3.toString();
                 } else if (result3.length() == 5) {
                     result4 = result3.toString();
                 }
                 ServCaseCode = HiddenCaseCode + ServCaseCode + result4;
             }
             String sqlItemName =
                     " select a.ServItemName  from LHHealthServItem a,LHServItem b"
                     + "  where a.ServItemCode=b.ServItemCode "
                     + " and b.ServItemNo='" + ServItemNo + "'"
                     ;
             String ServItemName = new ExeSQL().getOneValue(sqlItemName); //标准项目代码对应的标准项目名称
             String sqlName =
                     " select Name from LDPerson a  where a.CustomerNo ='" +
                     CustomerNo + "'";
             String CustomerName = new ExeSQL().getOneValue(sqlName); //客户号对应的客户名称
             String ServCaseName = ServItemName + "-" + CustomerName;

             LHServCaseDefSchema tLHServCaseDefSchema = new LHServCaseDefSchema(); //LHServCaseDef（服务事件定义表）
             tLHServCaseDefSchema.setServCaseCode(ServCaseCode); //服务事件号码
             tLHServCaseDefSchema.setServCaseName(ServCaseName); //服务事件名称
             tLHServCaseDefSchema.setServCaseState("1"); //服务事件状态
             tLHServCaseDefSchema.setServCaseType("1"); //服务事件类型
             tLHServCaseDefSchema.setOperator(temp_anaOpera[i - 1][1]);
             tLHServCaseDefSchema.setManageCom(temp_anaOpera[i - 1][0]);
             tLHServCaseDefSchema.setMakeDate(PubFun.getCurrentDate());
             tLHServCaseDefSchema.setMakeTime(PubFun.getCurrentTime());
             tLHServCaseDefSchema.setModifyDate(PubFun.getCurrentDate());
             tLHServCaseDefSchema.setModifyTime(PubFun.getCurrentTime());
             mLHServCaseDefSet.add(tLHServCaseDefSchema);
             mLHServCaseDefSetForIns.add(tLHServCaseDefSchema);
         }
     }
     map.put(mLHServCaseDefSetForIns, "INSERT");
     return true;
 }
  public boolean LHServCaseRelaInfo(int num)
  {
     String transact = "INSERT||MAIN";
     String temp_ContNo[][]; //保单计划号结果
     String temp_PlanInfo[][]; //服务计划名称机构
     SSRS tSSRS_ManaOpera = new SSRS();
     ExeSQL tExeManaOpera = new ExeSQL();
     String temp_anaOpera[][]; //管理机构和操作员
     String sqlManaOpera =" select a.ManageCom,a.Operator,a.ServItemNo,a.CustomerNo from LHQUESIMPORTMAIN  a "
                          +" where a.Whethachieve in('2','3') and a.Source ='1' and a.ModifyDate='"+BeforeCurrent+"' and a.Taskexecno is null "
                          +" and a.servitemno not in (select distinct m.servitemno from LHQuesImportMain m where m.Taskexecno is not null) order by a.Cusregistno"
                          ;
     tSSRS_ManaOpera = tExeManaOpera.execSQL(sqlManaOpera);
     temp_anaOpera = tSSRS_ManaOpera.getAllData();

     for (int i = 1; i <= num; i++)
     {
          //如果此服务项目有对应启动服务事件,事件的MakeDate是CurrentDate，那么是刚生成的事件，需要新生成CaseRela
          if (mLHServCaseDefSet.get(i).getMakeDate().equals(PubFun.getCurrentDate()))
          {
              String ServItemNo=temp_anaOpera[i-1][2];//服务项目代码
              String CustomerNo=temp_anaOpera[i-1][3];//服务项目代码对应的客户代码
              LHServCaseRelaSchema  tLHServCaseRelaSchema = new LHServCaseRelaSchema();//LHServCaseRela（服务事件关联表）
              String sqlName =" select Name from LDPerson a  where a.CustomerNo ='" + CustomerNo+"'";
              String CustomerName= new ExeSQL().getOneValue(sqlName);//客户号对应的客户名称
              SSRS tSSRS_ContNo= new SSRS();
              ExeSQL tExeContNoSQL = new ExeSQL();
              String sqlContNo ="  select b.ContNo,b.ServPlanNo from LHServItem b  where  b.ServItemNo='"+ServItemNo +"'";
              tSSRS_ContNo = tExeContNoSQL.execSQL(sqlContNo);
              temp_ContNo = tSSRS_ContNo.getAllData();
              SSRS tSSRS_PlanInfo= new SSRS();
              ExeSQL tExePlanInfo = new ExeSQL();
              String sqlPlanInfo =" select ServPlanName,ComID, ServPlanLevel from LHServPlan a  where a.ServPlanNo ='" + temp_ContNo[0][1] +"'";
              tSSRS_PlanInfo = tExePlanInfo.execSQL(sqlPlanInfo);
              temp_PlanInfo = tSSRS_PlanInfo.getAllData();
              String sqlServItemCode =" select ServItemCode from LHServItem a  where a.ServItemNo ='" + ServItemNo +"'";
              String ServItemCode= new ExeSQL().getOneValue(sqlServItemCode);//服务项目编号
              tLHServCaseRelaSchema.setContNo(temp_ContNo[0][0]);//保单号
              tLHServCaseRelaSchema.setServPlanNo(temp_ContNo[0][1]);//服务计划号码
              tLHServCaseRelaSchema.setServPlanName(temp_PlanInfo[0][0]);//服务计划名称
              tLHServCaseRelaSchema.setComID(temp_PlanInfo[0][1]);//服务计机构
              tLHServCaseRelaSchema.setServPlanLevel(temp_PlanInfo[0][2]);//服务计划档次
              tLHServCaseRelaSchema.setServItemCode(ServItemCode);//服务计划档次
              tLHServCaseRelaSchema.setServCaseCode(mLHServCaseDefSet.get(i).getServCaseCode());//事件号
              tLHServCaseRelaSchema.setServItemNo(ServItemNo);//服务项目号码
              tLHServCaseRelaSchema.setCustomerNo(CustomerNo);
              tLHServCaseRelaSchema.setCustomerName(CustomerName);
              tLHServCaseRelaSchema.setOperator(temp_anaOpera[i-1][1]);
              tLHServCaseRelaSchema.setManageCom(temp_anaOpera[i-1][0]);
              tLHServCaseRelaSchema.setMakeDate(PubFun.getCurrentDate());
              tLHServCaseRelaSchema.setMakeTime(PubFun.getCurrentTime());
              tLHServCaseRelaSchema.setModifyDate(PubFun.getCurrentDate());
              tLHServCaseRelaSchema.setModifyTime(PubFun.getCurrentTime());
              mLHServCaseRelaSet.add(tLHServCaseRelaSchema);
              mLHServCaseRelaSetForIns.add(tLHServCaseRelaSchema);
          }
          else
          {
              LHServCaseRelaDB tLHServCaseRelaDB = new LHServCaseRelaDB();
              tLHServCaseRelaDB.setServCaseCode(mLHServCaseDefSet.get(i).getServCaseCode());
              tLHServCaseRelaDB.setServItemNo(temp_anaOpera[i-1][2]);
              if(!tLHServCaseRelaDB.getInfo())
              {// @@错误处理
                  CError tError = new CError();
                  tError.moduleName = "LHQuesKineCRMBL";
                  tError.functionName = "LHServCaseRelaInfo";
                  tError.errorMessage = "LHServCaseRelaInfo没有得到数据，请确认!";
                  this.mErrors.addOneError(tError);
                  return false;
              }
              mLHServCaseRelaSet.add(tLHServCaseRelaDB.getSchema());
          }
     }
     map.put(mLHServCaseRelaSetForIns, "INSERT");
     return true;
  }
  public boolean LHCaseTaskRelaInfo(int num)
  {
      String transact = "INSERT||MAIN";
      SSRS tSSRS_ManaOpera = new SSRS();
      ExeSQL tExeManaOpera = new ExeSQL();
      String temp_anaOpera[][]; //管理机构和操作员
      String sqlManaOpera = " select a.ManageCom,a.Operator from LHQUESIMPORTMAIN  a "
                            +" where a.Whethachieve in('2','3') and a.Source ='1' and a.ModifyDate='"+BeforeCurrent+"' and a.Taskexecno is null "
                            +" and a.servitemno not in (select distinct m.servitemno from LHQuesImportMain m where m.Taskexecno is not null) order by a.Cusregistno"
                            ;
      tSSRS_ManaOpera = tExeManaOpera.execSQL(sqlManaOpera);
      temp_anaOpera = tSSRS_ManaOpera.getAllData();

      for (int j = 1; j <= num; j++)
      {
           String ServTaskCode="00203";//服务任务代码评估问卷录入
           LHCaseTaskRelaSchema  tLHCaseTaskRelaSchema=new LHCaseTaskRelaSchema();//LHCaseTaskRela（事件任务关联表）
           tLHCaseTaskRelaSchema.setServTaskNo("6"+PubFun1.CreateMaxNo("ServTaskNoIn", 11));//服务任务编号从CRM接口进入的生成规则
           tLHCaseTaskRelaSchema.setServCaseCode(mLHServCaseDefSet.get(j).getServCaseCode());//服务事件号码
           tLHCaseTaskRelaSchema.setServTaskCode(ServTaskCode);//服务任务代码
           tLHCaseTaskRelaSchema.setServTaskAffirm("1");//服务任务确认未确认
           tLHCaseTaskRelaSchema.setServTaskState("1");//服务任务状态等待执行
           tLHCaseTaskRelaSchema.setPlanExeDate(PubFun.getCurrentDate());//计划实施时间
           tLHCaseTaskRelaSchema.setTaskDesc("任务说明:（请填写任务要求等）；完成情况：（请于任务完成时填写）");//服务任务说明
           tLHCaseTaskRelaSchema.setOperator(temp_anaOpera[j-1][1]);
           tLHCaseTaskRelaSchema.setMakeDate(PubFun.getCurrentDate());
           tLHCaseTaskRelaSchema.setMakeTime(PubFun.getCurrentTime());
           tLHCaseTaskRelaSchema.setModifyDate(PubFun.getCurrentDate());
           tLHCaseTaskRelaSchema.setModifyTime(PubFun.getCurrentTime());
           tLHCaseTaskRelaSchema.setManageCom(temp_anaOpera[j-1][0]);
           mLHCaseTaskRelaSet.add(tLHCaseTaskRelaSchema);
      }

      map.put(mLHCaseTaskRelaSet, "INSERT");
      return true;
  }
public boolean LHTaskCustomerRelaInfo(int num)
{
    String transact = "INSERT||MAIN";
    SSRS tSSRS_ManaOpera = new SSRS();
    ExeSQL tExeManaOpera = new ExeSQL();
    String temp_anaOpera[][]; //管理机构和操作员
    String sqlManaOpera =" select a.ManageCom,a.Operator,a.ServItemNo,a.CustomerNo from LHQUESIMPORTMAIN a "
                         +" where a.Whethachieve in('2','3') and a.Source ='1'  and a.ModifyDate='"+BeforeCurrent+"' and a.Taskexecno is null "
                         +" and a.servitemno not in (select distinct m.servitemno from LHQuesImportMain m where m.Taskexecno is not null) order by a.Cusregistno"
                         ;
    tSSRS_ManaOpera = tExeManaOpera.execSQL(sqlManaOpera);
    temp_anaOpera = tSSRS_ManaOpera.getAllData();

    for (int j = 1; j <= num; j++)
    {
        String ServItemNo=temp_anaOpera[j-1][2];//服务项目代码
        String CustomerNo=temp_anaOpera[j-1][3];//服务项目代码对应的客户代码
        String ServTaskCode="00203";//服务任务代码评估问卷录入
        LHTaskCustomerRelaSchema tLHTaskCustomerRelaSchema = new LHTaskCustomerRelaSchema(); //LHCaseTaskRela（事件任务关联表）
        tLHTaskCustomerRelaSchema.setTaskExecNo("6"+PubFun1.CreateMaxNo("TaskExecNoIn", 11));//任务实施号码从CRM接口进入的生成规则
        tLHTaskCustomerRelaSchema.setServCaseCode(mLHServCaseDefSet.get(j).getServCaseCode());
        tLHTaskCustomerRelaSchema.setServTaskNo(mLHCaseTaskRelaSet.get(j).getServTaskNo());
        tLHTaskCustomerRelaSchema.setServItemNo(ServItemNo);
        tLHTaskCustomerRelaSchema.setServTaskCode(ServTaskCode);//服务任务代码
        tLHTaskCustomerRelaSchema.setTaskExecState("1");//任务执行状态未执行
        tLHTaskCustomerRelaSchema.setOperator(temp_anaOpera[j-1][1]);
        tLHTaskCustomerRelaSchema.setMakeDate(PubFun.getCurrentDate());
        tLHTaskCustomerRelaSchema.setMakeTime(PubFun.getCurrentTime());
        tLHTaskCustomerRelaSchema.setModifyDate(PubFun.getCurrentDate());
        tLHTaskCustomerRelaSchema.setModifyTime(PubFun.getCurrentTime());
        tLHTaskCustomerRelaSchema.setManageCom(temp_anaOpera[j-1][0]);
        mLHTaskCustomerRelaSet.add(tLHTaskCustomerRelaSchema);
    }
    map.put(mLHTaskCustomerRelaSet, "INSERT");
    return true;
}
public boolean LHQuesImportMainInfo(int num)
{
    String transact = "UPDATE||MAIN";
    String temp_Result[][]; //结果
    String CusReNum[][];
    String CusRegistNo[]=new String[num];
    SSRS tSSRS_CusReNum = new SSRS();
    ExeSQL tExeCusReNum = new ExeSQL();
    String sqlCusReNum =" select a.Cusregistno,a.Source,a.WhethAchieve,a.SubmitDate,a.SubmitTime,a.MakeDate,a.MakeTime "
                        +" from LHQUESIMPORTMAIN a where a.Whethachieve in('2','3') and a.Source ='1' and a.ModifyDate='"+BeforeCurrent+"' and a.Taskexecno is null "
                        +" and a.servitemno not in (select distinct m.servitemno from LHQuesImportMain m where m.Taskexecno is not null) order by a.Cusregistno"
                        ;
    tSSRS_CusReNum = tExeCusReNum.execSQL(sqlCusReNum);
    int count_CusReNum = tSSRS_CusReNum.getMaxRow();
    CusReNum = tSSRS_CusReNum.getAllData();
    SSRS tSSRS_ManaOpera = new SSRS();
    ExeSQL tExeManaOpera = new ExeSQL();
    String temp_anaOpera[][]; //管理机构和操作员
    String sqlManaOpera = " select ManageCom,Operator,ServItemNo,CustomerNo from LHQUESIMPORTMAIN a "
                          +" where Whethachieve in('2','3') and Source ='1'  and a.ModifyDate='"+BeforeCurrent+"' and a.Taskexecno is null "
                          +" and a.servitemno not in (select distinct m.servitemno from LHQuesImportMain m where m.Taskexecno is not null) order by a.Cusregistno"
                          ;
    tSSRS_ManaOpera = tExeManaOpera.execSQL(sqlManaOpera);
    temp_anaOpera = tSSRS_ManaOpera.getAllData();

    //System.out.println("客户登记号码数量"+count_CusReNum);
    for(int j=1; j<=count_CusReNum; j++)
    {
        CusRegistNo[j-1]=CusReNum[j-1][0];
        String ServItemNo=temp_anaOpera[j-1][2];//服务项目代码
        String CustomerNo=temp_anaOpera[j-1][3];//服务项目代码对应的客户代码
        String QuesType="01";//问卷类型
        String ServTaskCode="00203";//服务任务代码评估问卷录入
        LHQuesImportMainSchema  tLHQuesImportMainSchema=new LHQuesImportMainSchema();//问卷录入管理表
        SSRS tSSRS_Result = new SSRS();
        ExeSQL tExeResultSQL = new ExeSQL();
        String sqlResult ="  select b.ContNo,b.ServPlanNo  from LHServItem b "
                         +" where  b.ServItemNo='"+ServItemNo +"'";
        tSSRS_Result = tExeResultSQL.execSQL(sqlResult);
        temp_Result = tSSRS_Result.getAllData();
        tLHQuesImportMainSchema.setServItemNo(ServItemNo);
        tLHQuesImportMainSchema.setServTaskCode(ServTaskCode);//服务任务代码
        tLHQuesImportMainSchema.setCusRegistNo(CusRegistNo[j-1]);//客户登记号码
        tLHQuesImportMainSchema.setQuesType(QuesType);//问卷类型
        tLHQuesImportMainSchema.setContNo(temp_Result[0][0]);//实施号码对应的项目号对应的保单号
        tLHQuesImportMainSchema.setServPlanNo(temp_Result[0][1]);//实施号码对应的项目号对应的计划号
        tLHQuesImportMainSchema.setServCaseCode(mLHServCaseDefSet.get(j).getServCaseCode());//服务事件号码
        // System.out.println("服务事件号码 "+tLHQuesImportMainSchema.getServCaseCode());
        tLHQuesImportMainSchema.setServTaskNo(mLHCaseTaskRelaSet.get(j).getServTaskNo());//服务任务号码
        // System.out.println("服务任务号码 "+tLHQuesImportMainSchema.getServTaskNo());
        tLHQuesImportMainSchema.setTaskExecNo(mLHTaskCustomerRelaSet.get(j).getTaskExecNo());//客户关联号码
       // System.out.println("客户关联号码 "+tLHQuesImportMainSchema.getTaskExecNo());
        tLHQuesImportMainSchema.setCustomerNo(CustomerNo);//客户代码
        tLHQuesImportMainSchema.setSource(CusReNum[j-1][1]);//录入途径1-CRM传入，客户为核心系统客户；
        tLHQuesImportMainSchema.setWhethAchieve(CusReNum[j-1][2]);//保存是否完成
        tLHQuesImportMainSchema.setOperator(temp_anaOpera[j-1][1]);
        tLHQuesImportMainSchema.setSubmitDate(CusReNum[j-1][3]);//客户登记号码相关的提交日期
        tLHQuesImportMainSchema.setSubmitTime(CusReNum[j-1][4]);//客户登记号码相关的提交时间
        tLHQuesImportMainSchema.setMakeDate(CusReNum[j-1][5]);//客户登记号码相关的入机日期
        tLHQuesImportMainSchema.setMakeTime(CusReNum[j-1][6]);//客户登记号码相关的入机时间
        tLHQuesImportMainSchema.setModifyDate(PubFun.getCurrentDate());
        tLHQuesImportMainSchema.setModifyTime(PubFun.getCurrentTime());
        tLHQuesImportMainSchema.setManageCom(temp_anaOpera[j-1][0]);
        mLHQuesImportMainSet.add(tLHQuesImportMainSchema);
    }
    map.put(mLHQuesImportMainSet, "DELETE&INSERT");
    return true;
}
/**
* 传输数据的公共方法
        * @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "LHQuesKineCRMBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败LHQuesKineCRMBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
      PubSubmit tPubSubmit = new PubSubmit();
      System.out.println("Start LHQuesKineCRMBL Submit...");
      if (!tPubSubmit.submitData(mInputData, mOperate))
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
        CError tError = new CError();
        tError.moduleName = "LHQuesKineCRMBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据提交失败!";

        this.mErrors.addOneError(tError);
        return false;
      }

      System.out.println("End LHQuesKineCRMBL Submit...");
      //如果有需要处理的错误，则返回
    }
    mInputData=null;
    return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
//    //System.out.println("***********begin init**************"+this.mOperate);
//    if (this.mOperate.equals("INSERT||MAIN"))
//    {
//           map.put(mLHServCaseDefSet, "INSERT");
//           map.put(mLHServCaseRelaSet, "INSERT");
//           map.put(mLHCaseTaskRelaSet, "INSERT");
//           map.put(mLHTaskCustomerRelaSet, "INSERT");
//    }
//    if (this.mOperate.equals("UPDATE||MAIN"))
//    {
//            String CusReNum[][];
//            SSRS tSSRS_CusReNum = new SSRS();
//            ExeSQL tExeCusReNum = new ExeSQL();
//            String sqlCusReNum =" select Cusregistno from LHQUESIMPORTMAIN where Whethachieve in('2','3') and Source ='1'";
//            tSSRS_CusReNum = tExeCusReNum.execSQL(sqlCusReNum);
//            int count_CusReNum = tSSRS_CusReNum.getMaxRow();
//            CusReNum = tSSRS_CusReNum.getAllData();
//            String CusRegistNo[]=new String[count_CusReNum];
//            //System.out.println("客户登记号码数量"+count_CusReNum);
//            for(int j=1; j<=count_CusReNum; j++)
//            {
//                CusRegistNo[j-1]=CusReNum[j-1][0];
//                map.put(" delete from  LHQuesImportMain where CusRegistNo='"+CusRegistNo[j-1]+"'","DELETE");
//            }
//            map.put(mLHQuesImportMainSet, "INSERT");
//    }
//    if (this.mOperate.equals("DELETE||MAIN"))
//    { }
    return true;
}
/**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
 */
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
//         this.mLHServCaseDefSet.set((LHServCaseDefSet) cInputData.getObjectByObjectName("LHServCaseDefSet", 0));
//         this.mLHServCaseRelaSet.set((LHServCaseRelaSet) cInputData.getObjectByObjectName("LHServCaseRelaSet", 0));
//         this.mLHCaseTaskRelaSet.set((LHCaseTaskRelaSet) cInputData.getObjectByObjectName("LHCaseTaskRelaSet", 0));
//         this.mLHTaskCustomerRelaSet.set((LHTaskCustomerRelaSet) cInputData.getObjectByObjectName("LHTaskCustomerRelaSet", 0));
//         this.mLHQuesImportMainSet.set((LHQuesImportMainSet) cInputData.getObjectByObjectName("LHQuesImportMainSet", 0));
//         this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
//    this.mResult.clear();
//    LHQuesImportMainDB tLHQuesImportMainDB=new LHQuesImportMainDB();
//    //如果有需要处理的错误，则返回
//    if (tLHQuesImportMainDB.mErrors.needDealError())
//    {
//            // @@错误处理
//            this.mErrors.copyAllErrors(tLHQuesImportMainDB.mErrors);
//            CError tError = new CError();
//            tError.moduleName = "LHQuesKineCRMBL";
//            tError.functionName = "submitData";
//            tError.errorMessage = "数据提交失败!";
//            this.mErrors.addOneError(tError);
//            return false;
//    }
//    mInputData=null;
    return true;
}
 private boolean prepareOutputData()
 {
     try
     {
         this.mInputData.clear();
         this.mInputData.add(this.mLHServCaseDefSetForIns);
         this.mInputData.add(this.mLHServCaseRelaSetForIns);
         this.mInputData.add(this.mLHCaseTaskRelaSet);
         this.mInputData.add(this.mLHTaskCustomerRelaSet);
         this.mInputData.add(this.mLHQuesImportMainSet);
         mInputData.add(map);
         mResult.clear();
         mResult.add(this.mLHServCaseDefSet);
         mResult.add(this.mLHServCaseRelaSet);
         mResult.add(this.mLHCaseTaskRelaSet);
         mResult.add(this.mLHTaskCustomerRelaSet);
         mResult.add(this.mLHQuesImportMainSet);
     } catch (Exception ex)
     {
         // @@错误处理
         CError tError = new CError();
         tError.moduleName = "LHQuesKineCRMBL";
         tError.functionName = "prepareData";
         tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
         this.mErrors.addOneError(tError);
         return false;
     }
     return true;
 }
 public VData getResult() {
     return this.mResult;
 }
    private void jbInit() throws Exception {
    }
}
