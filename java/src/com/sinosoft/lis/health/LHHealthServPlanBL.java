/*
 * <p>ClassName: LHHealthServPlanBL </p>
 * <p>Description:LHHealthServPlanBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-03-20 15:36:42
 */
package com.sinosoft.lis.health;

import java.lang.Integer;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LHHealthServPlanBL {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate;
    private String tNo;
    String manage = "";

    /** 业务处理相关变量 */
    private LHHealthServPlanSchema mLHHealthServPlanSchema = new
            LHHealthServPlanSchema();
    private LHHealthServPlanSet mLHHealthServPlanSet = new LHHealthServPlanSet();
    public LHHealthServPlanBL() {
    }

    public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHHealthServPlanBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LHHealthServPlanBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }

        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start LHHealthServPlanBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("End LHHealthServPlanBL Submit...");
            //如果有需要处理的错误，则返回
            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                CError tError = new CError();
                tError.moduleName = "LHHealthServPlanBL";
                tError.functionName = "submitData";
                tError.errorMessage = "您输入的服务计划信息不能全部重复!";

                this.mErrors.addOneError(tError);
                return false;
            }
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        String FeeLevel = "";
        if (this.mOperate.equals("INSERT||MAIN")) {
            if (mLHHealthServPlanSet != null && mLHHealthServPlanSet.size() > 0)
            {
                if (mLHHealthServPlanSet.get(1).getPlanType().equals("2") &&
                    mLHHealthServPlanSet.get(1).getServPlanLevel().equals("0"))
                {
                    FeeLevel = getLevelAccordFee();
                }

                for (int i = 1; i <= mLHHealthServPlanSet.size(); i++)
                {
                    if (mLHHealthServPlanSet.get(i).getPlanType().equals("2") &&
                        mLHHealthServPlanSet.get(i).getServPlanLevel().equals("0"))
                    {
                        mLHHealthServPlanSet.get(i).setServPlanLevel(FeeLevel);
                    }
                    mLHHealthServPlanSet.get(i).setOperator(mGlobalInput.
                            Operator);
                    mLHHealthServPlanSet.get(i).setMakeDate(PubFun.
                            getCurrentDate());
                    mLHHealthServPlanSet.get(i).setMakeTime(PubFun.
                            getCurrentTime());
                    mLHHealthServPlanSet.get(i).setModifyDate(PubFun.
                            getCurrentDate());
                    mLHHealthServPlanSet.get(i).setModifyTime(PubFun.
                            getCurrentTime());
                    mLHHealthServPlanSet.get(i).setManageCom(manage);
                }
                map.put(mLHHealthServPlanSet, "INSERT");

            } else {
                System.out.println("mLHHealthServPlanSet is null");
            }
        }

        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            if (mLHHealthServPlanSet != null && mLHHealthServPlanSet.size() > 0)
            {
                if (mLHHealthServPlanSet.get(1).getPlanType().equals("2") &&
                    mLHHealthServPlanSet.get(1).getServPlanLevel().equals("0"))
                {
                    FeeLevel = getLevelAccordFee();
                }

                for (int i = 1; i <= mLHHealthServPlanSet.size(); i++)
                {
                    if (mLHHealthServPlanSet.get(i).getPlanType().equals("2") &&
                        mLHHealthServPlanSet.get(i).getServPlanLevel().equals("0"))
                    {
                        mLHHealthServPlanSet.get(i).setServPlanLevel(FeeLevel);
                    }

                    mLHHealthServPlanSet.get(i).setOperator(mGlobalInput.
                            Operator);
                    mLHHealthServPlanSet.get(i).setMakeDate(PubFun.
                            getCurrentDate());
                    mLHHealthServPlanSet.get(i).setMakeTime(PubFun.
                            getCurrentTime());
                    mLHHealthServPlanSet.get(i).setModifyDate(PubFun.
                            getCurrentDate());
                    mLHHealthServPlanSet.get(i).setModifyTime(PubFun.
                            getCurrentTime());
                    mLHHealthServPlanSet.get(i).setManageCom(manage);
                }
                map.put(" delete from  LHHealthServPlan where ServPlanCode='" +
                        mLHHealthServPlanSet.get(1).getServPlanCode() +
                        "' and ServPlanLevel='" +
                        mLHHealthServPlanSet.get(1).getServPlanLevel() +
                        "' and ComID='" + mLHHealthServPlanSet.get(1).getComID() +
                        "' ", "DELETE");
                map.put(mLHHealthServPlanSet, "INSERT");

            }
        }

        if (this.mOperate.equals("DELETE||MAIN")) {
            map.put(" delete from  LHHealthServPlan where ServPlanCode='" +
                    mLHHealthServPlanSet.get(1).getServPlanCode() +
                    "' and ServPlanLevel='" +
                    mLHHealthServPlanSet.get(1).getServPlanLevel() +
                    "' and ComID='" + mLHHealthServPlanSet.get(1).getComID() +
                    "' ", "DELETE");
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLHHealthServPlanSet.set((LHHealthServPlanSet) cInputData.
                                      getObjectByObjectName(
                "LHHealthServPlanSet", 0));
        if (mLHHealthServPlanSet.size() < 1) {
            CError tError = new CError();
            tError.moduleName = "LHHealthServPlanBL";
            tError.functionName = "submitData";
            tError.errorMessage = "服务计划内容信息不许为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));

        if (mGlobalInput.ManageCom.length() == 8) {
            manage = mGlobalInput.ManageCom.substring(0, 4);
        } else {
            manage = mGlobalInput.ManageCom;
        }
        for (int i = 1; i <= mLHHealthServPlanSet.size(); i++) {
            if (mLHHealthServPlanSet.get(i).getServItemCode() == null ||
                mLHHealthServPlanSet.get(i).getServItemCode().equals("") ||
                mLHHealthServPlanSet.get(i).getServItemCode().equals("null")) {
                CError tError = new CError();
                tError.moduleName = "LHHealthServPlanBL";
                tError.functionName = "submitData";
                tError.errorMessage = "您输入的标准服务项目代码不许为空!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }

        for (int i = 1; i <= mLHHealthServPlanSet.size(); i++) {
            if (mLHHealthServPlanSet.get(i).getServItemSN() == null ||
                mLHHealthServPlanSet.get(i).getServItemSN().equals("") ||
                mLHHealthServPlanSet.get(i).getServItemSN().equals("null")) {
                CError tError = new CError();
                tError.moduleName = "LHHealthServPlanBL";
                tError.functionName = "submitData";
                tError.errorMessage = "您输入的服务项目序号不许为空!";
                this.mErrors.addOneError(tError);
                return false;
            }
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LHHealthServPlanDB tLHHealthServPlanDB = new LHHealthServPlanDB();
        tLHHealthServPlanDB.setSchema(this.mLHHealthServPlanSchema);
        //如果有需要处理的错误，则返回
        if (tLHHealthServPlanDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHHealthServPlanDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHHealthServPlanBL";
            tError.functionName = "submitData";
            tError.errorMessage = "您输入的服务计划信息不能全部重复!!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLHHealthServPlanSet);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLHHealthServPlanSet);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHHealthServPlanBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    public String getLevelAccordFee()
    {
        String FeeLevel = "";
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        String t_sql =
                " select  max(ServplanLevel) from LHHEALTHSERVPLAN "
                + " where servplancode = '" +
                mLHHealthServPlanSet.get(1).getServPlanCode()
                + "' and ComID = '" + mLHHealthServPlanSet.get(1).getComID() +
                "' and plantype = '2' "
                ;
        System.out.println(t_sql);
        tSSRS = tExeSQL.execSQL(t_sql);
        System.out.println(tSSRS);
        String t[][] = tSSRS.getAllData();
        if (t[0][0].equals("") || t[0][0] == null)
        {
            System.out.println("FeeLevel = 90 ");
            return FeeLevel = "90";
        }
        else
        {
            System.out.println("按保费生成新流水号-----FeeLevel = " + FeeLevel);
            Integer tt = new Integer(t[0][0]);
            int i = tt.intValue();
            i = i + 1;
            FeeLevel = i + "";
            System.out.println("按保费生成新流水号-----FeeLevel = " + FeeLevel);
            return FeeLevel;
        }
    }
}
