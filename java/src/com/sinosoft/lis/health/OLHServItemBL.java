package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLHServItemBL {
        /** 错误处理类，每个需要错误处理的类中都放置该类 */
      public  CErrors mErrors=new CErrors();
      private VData mResult = new VData();
      /** 往后面传输数据的容器 */
      private VData mInputData= new VData();
      private MMap map = new MMap();
      /** 全局数据 */
      private GlobalInput mGlobalInput =new GlobalInput() ;
      private LHServItemSet mLHServItemSet=new LHServItemSet();
      private TransferData mTransferData = new TransferData();

      /** 数据操作字符串 */
      private String mOperate;
      String GrpServPlanCode;
    public OLHServItemBL() {
    }

    public boolean submitData(VData cInputData,String cOperate)
   {
   //将操作数据拷贝到本类中
   this.mOperate =cOperate;
   this.mInputData = cInputData;
   //得到外部传入的数据,将数据备份到本类中
   if (!getInputData(cInputData))
   {        return false;}
    System.out.println("----------");
   //进行业务处理
   if (!dealData())
   {
         // @@错误处理
         CError tError = new CError();
         tError.moduleName = "OLHServItemBL";
         tError.functionName = "submitData";
         tError.errorMessage = "数据处理失败OLHServItemBL-->dealData!";
         this.mErrors .addOneError(tError) ;
         return false;
   }

     //准备往后台的数据
   if (!prepareOutputData())
     return false;

     System.out.println("Start OLHServCaseDefBL Submit...");
     PubSubmit tPubSubmit = new PubSubmit();
          if (!tPubSubmit.submitData(mInputData, mOperate))
            {
              // @@错误处理
              this.mErrors.copyAllErrors(tPubSubmit.mErrors);
              CError tError = new CError();
              tError.moduleName = "OLHServItemBL";
              tError.functionName = "submitData";
              tError.errorMessage = "数据提交失败!";

              this.mErrors.addOneError(tError);
              return false;
            }
            System.out.println("END OLHServItemBL Submit...");

   mInputData=null;
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
    if (this.mOperate.equals("INSERT||MAIN"))
    {
        LHGrpServItemSet tLHGrpServItemSet = new LHGrpServItemSet();
        LHGrpServItemDB tLHGrpServItem = new LHGrpServItemDB();
        String sql = "select * from lhgrpservitem where GrpServPlanNo = '" + GrpServPlanCode + "'";
        tLHGrpServItemSet = tLHGrpServItem.executeQuery(sql);
        LHGrpPersonUniteSet tLHGrpPersonUniteSet = new LHGrpPersonUniteSet();
        LHGrpPersonUniteDB tLHGrpPersonUnite = new LHGrpPersonUniteDB();
        String sql_1 = "select * from LHGrpPersonUnite where GrpServPlanNo = '" + GrpServPlanCode + "'";
        tLHGrpPersonUniteSet = tLHGrpPersonUnite.executeQuery(sql_1);
//        LHGrpServPlanSet tLHGrpServPlanSet = new LHGrpServPlanSet();
//        LHGrpServPlanDB tLHGrpServPlanDB = new LHGrpServPlanDB();
//        tLHGrpServPlanDB.setGrpServPlanNo(GrpServPlanCode);
//        tLHGrpServPlanSet = tLHGrpServPlanDB.query();

        for (int i = 1; i<=tLHGrpServItemSet.size(); i++)
        {
            int x = tLHGrpPersonUniteSet.size();
            String tGrpContNo = new ExeSQL().getOneValue(" select distinct grpcontno from lhgrpservplan where  grpservplanno = '"+tLHGrpServItemSet.get(i).getGrpServPlanNo()+"' ");
            for (int j = 1; j<=tLHGrpPersonUniteSet.size(); j++)
            {
              LHServItemSchema mLHServItemSchema=new LHServItemSchema();
              mLHServItemSchema.setGrpServPlanNo( tLHGrpServItemSet.get(i).getGrpServPlanNo());
              mLHServItemSchema.setServPlanNo("0");
              mLHServItemSchema.setGrpServItemNo(tLHGrpServItemSet.get(i).getGrpServItemNo());
              mLHServItemSchema.setServItemNo( "1" + PubFun1.CreateMaxNo("ServItemNoSet", 19));
              mLHServItemSchema.setServItemCode(tLHGrpServItemSet.get(i).getServItemCode());
              mLHServItemSchema.setCustomerNo(tLHGrpPersonUniteSet.get(j).getCustomerNo());
              mLHServItemSchema.setName(tLHGrpPersonUniteSet.get(j).getName());
              mLHServItemSchema.setContNo(tGrpContNo);
              mLHServItemSchema.setComID(tLHGrpServItemSet.get(i).getComID());
              mLHServItemSchema.setServItemType("0");
              mLHServItemSchema.setServPriceCode(tLHGrpServItemSet.get(i).getServPriceCode());
              mLHServItemSchema.setManageCom(mGlobalInput.ManageCom);
              mLHServItemSchema.setOperator(mGlobalInput.Operator);
              mLHServItemSchema.setMakeDate(PubFun.getCurrentDate());
              mLHServItemSchema.setMakeTime(PubFun.getCurrentTime());
              mLHServItemSchema.setModifyDate(PubFun.getCurrentDate());
              mLHServItemSchema.setModifyTime(PubFun.getCurrentTime());
              mLHServItemSet.add(mLHServItemSchema);
              map.put(mLHServItemSet, "INSERT");


          }
      }
       LHGrpServPlanDB mLHGrpServPlanDB = new LHGrpServPlanDB();
       mLHGrpServPlanDB.setGrpServPlanNo(tLHGrpServItemSet.get(1).getGrpServPlanNo());
       LHGrpServPlanSet eLHGrpServPlanSet = mLHGrpServPlanDB.query();
       System.out.println(eLHGrpServPlanSet+"12----------");
       map.put("update lwmission set missionprop20 = '1' where activityid  = '0000002010' and  missionprop1 ='"+eLHGrpServPlanSet.get(1).getGrpContNo()+"'", "UPDATE");

  }

    return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean updateData()
{
  return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
 return true;
}
/**
* 从输入数据中得到所有对象
*输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
*/
private boolean getInputData(VData cInputData)
{
        mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
        GrpServPlanCode = (String) mTransferData.getValueByName("tGrpServPlanCode");
        System.out.println(GrpServPlanCode+"++++++++++");
        this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
        return true;
}


private boolean prepareOutputData()
{
  try
       {
               this.mInputData.clear();
               this.mInputData.add(this.mLHServItemSet);
               mInputData.add(map);
            //   mResult.clear();
            //   mResult.add(this.mLHServItemSchema);
       }
       catch(Exception ex)
       {
               // @@错误处理
               CError tError =new CError();
               tError.moduleName="LHServItemBL";
               tError.functionName="prepareData";
               tError.errorMessage="在准备往后层处理所需要的数据时出错。";
               this.mErrors .addOneError(tError) ;
               return false;
       }
       return true;
   }
       public VData getResult()
       {
       return this.mResult;
       }

}
