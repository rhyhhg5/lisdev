/*
 * <p>ClassName: OLHGrpServItrmBL </p>
 * <p>Description: OLHServPlanBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-03-09 10:43:01
 */
package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLHGrpServItemBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    //   private LHGrpServPlanSchema mLHGrpServPlanSchema = new LHGrpServPlanSchema();
    private LHGrpServItemSet mLHGrpServItemSet = new LHGrpServItemSet();
    private LHGrpServExeTraceSet mLHGrpServExeTraceSet = new
            LHGrpServExeTraceSet();
    public OLHGrpServItemBL() {
    }

    public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLHGrpServPlanBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败OLHGrpServPlanBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start OLHGrpServItemBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();

            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                CError tError = new CError();
                tError.moduleName = "OLHGrpServPlanBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }

        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        System.out.println("--------DealData---------");
        if (this.mOperate.equals("INSERT||MAIN")) {
            if (mLHGrpServItemSet != null && mLHGrpServItemSet.size() > 0) {
                System.out.println("***********" + mLHGrpServItemSet.size());
                for (int i = 1; i <= mLHGrpServItemSet.size(); i++) {
                    mLHGrpServItemSet.get(i).setGrpServItemNo(PubFun1.
                            CreateMaxNo("LHGrpServItemNo", 20));
                    mLHGrpServItemSet.get(i).setOperator(mGlobalInput.Operator);
                    mLHGrpServItemSet.get(i).setManageCom(mGlobalInput.
                            ManageCom);
                    mLHGrpServItemSet.get(i).setMakeDate(PubFun.getCurrentDate());
                    mLHGrpServItemSet.get(i).setMakeTime(PubFun.getCurrentTime());
                    mLHGrpServItemSet.get(i).setModifyDate(PubFun.
                            getCurrentDate());
                    mLHGrpServItemSet.get(i).setModifyTime(PubFun.
                            getCurrentTime());

                    //   System.out.println("------------"+mLHServItemSet.get(i).getServItemNo()+"----------------");
                }
                map.put(mLHGrpServItemSet, "INSERT");

                for (int i = 1; i <= mLHGrpServExeTraceSet.size(); i++) {

                    mLHGrpServExeTraceSet.get(i).setGrpServItemNo(
                            mLHGrpServItemSet.get(
                                    i).getGrpServItemNo());
                    mLHGrpServExeTraceSet.get(i).setExeState("0");
                    mLHGrpServExeTraceSet.get(i).setExeDate("1000-01-01");

                    mLHGrpServExeTraceSet.get(i).setServDesc("无");
                    mLHGrpServExeTraceSet.get(i).setOperator(mGlobalInput.
                            Operator);
                    mLHGrpServExeTraceSet.get(i).setManageCom(mGlobalInput.
                            ManageCom);
                    mLHGrpServExeTraceSet.get(i).setMakeDate(PubFun.
                            getCurrentDate());
                    mLHGrpServExeTraceSet.get(i).setMakeTime(PubFun.
                            getCurrentTime());
                    mLHGrpServExeTraceSet.get(i).setModifyDate(PubFun.
                            getCurrentDate());
                    mLHGrpServExeTraceSet.get(i).setModifyTime(PubFun.
                            getCurrentTime());

                }
                map.put(mLHGrpServExeTraceSet, "INSERT");

            }
        }
        if (this.mOperate.equals("UPDATE||MAIN")) {
            LHGrpServExeTraceSet lLHGrpServExeTraceSet = new
                    LHGrpServExeTraceSet();
            for (int i = 1; i <= mLHGrpServItemSet.size(); i++) {
                if (mLHGrpServItemSet.get(i).getGrpServItemNo() == null ||
                    mLHGrpServItemSet.get(i).getGrpServItemNo().equals("")) {
                    mLHGrpServItemSet.get(i).setGrpServItemNo(PubFun1.
                            CreateMaxNo("LHGrpServItemNo", 20));
                    mLHGrpServItemSet.get(i).setMakeDate(PubFun.getCurrentDate());
                    mLHGrpServItemSet.get(i).setMakeTime(PubFun.getCurrentTime());
                    LHGrpServExeTraceSchema tLHGrpServExeTraceSchema = new
                            LHGrpServExeTraceSchema();
                    tLHGrpServExeTraceSchema.setGrpServItemNo(
                            mLHGrpServItemSet.get(i).getGrpServItemNo());
                    tLHGrpServExeTraceSchema.setGrpServPlanNo(
                            mLHGrpServItemSet.get(1).getGrpServPlanNo());
                    tLHGrpServExeTraceSchema.setGrpContNo(
                            mLHGrpServItemSet.get(1).getGrpContNo());
                    tLHGrpServExeTraceSchema.setExeState("0");
                    tLHGrpServExeTraceSchema.setExeDate("1000-01-01");

                    tLHGrpServExeTraceSchema.setServDesc("无");
                    tLHGrpServExeTraceSchema.setOperator(mGlobalInput.
                            Operator);
                    tLHGrpServExeTraceSchema.setManageCom(mGlobalInput.
                            ManageCom);
                    tLHGrpServExeTraceSchema.setMakeDate(PubFun.
                            getCurrentDate());
                    tLHGrpServExeTraceSchema.setMakeTime(PubFun.
                            getCurrentTime());
                    tLHGrpServExeTraceSchema.setModifyDate(PubFun.
                            getCurrentDate());
                    tLHGrpServExeTraceSchema.setModifyTime(PubFun.
                            getCurrentTime());

                    lLHGrpServExeTraceSet.add(tLHGrpServExeTraceSchema);
                }
                mLHGrpServItemSet.get(i).setOperator(mGlobalInput.Operator);
                mLHGrpServItemSet.get(i).setManageCom(mGlobalInput.ManageCom);
                mLHGrpServItemSet.get(i).setModifyDate(PubFun.getCurrentDate());
                mLHGrpServItemSet.get(i).setModifyTime(PubFun.getCurrentTime());
            }
            map.put("delete from LHGrpServItem where GrpServPlanNo = '" +
                    mLHGrpServItemSet.get(1).getGrpServPlanNo() + "'", "DELETE");
            map.put(mLHGrpServItemSet, "INSERT");
            if (lLHGrpServExeTraceSet != null) {

                map.put(lLHGrpServExeTraceSet, "INSERT");
            }

        }
        if (this.mOperate.equals("DELETE||MAIN")) {
            map.put("delete from LHGrpServItem where GrpServPlanNo = '" +
                    mLHGrpServItemSet.get(1).getGrpServPlanNo() + "'", "DELETE");
            if (mLHGrpServItemSet.size() > 0) {
                map.put(mLHGrpServItemSet, "DELETE");
            }
            map.put(mLHGrpServExeTraceSet, "DELETE");
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLHGrpServItemSet.set((LHGrpServItemSet) cInputData.
                                   getObjectByObjectName(
                                           "LHGrpServItemSet", 0));

        this.mLHGrpServExeTraceSet.set((LHGrpServExeTraceSet) cInputData.
                                       getObjectByObjectName(
                                               "LHGrpServExeTraceSet", 0));

        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LHServPlanDB tLHServPlanDB = new LHServPlanDB();
        //如果有需要处理的错误，则返回
        if (tLHServPlanDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHServPlanDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "OLHGrpServItemBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLHGrpServItemSet);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLHGrpServItemBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
