/*
 * <p>ClassName: OLDHospitalBL </p>
 * <p>Description: OLDHospitalBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-15 14:25:18
 */
package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LDHospitalBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LDHospitalSchema mLDHospitalSchema = new LDHospitalSchema();
    private LDHospitalInfoIntroSet mLDHospitalInfoIntroSet = new
            LDHospitalInfoIntroSet();
    private LDComHospitalSet mLDComHospitalSet = new LDComHospitalSet();


//private LDHospitalSet mLDHospitalSet=new LDHospitalSet();
    public LDHospitalBL() {
    }

    public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        this.mInputData = cInputData;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDHospitalBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LDHospitalBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start LDHospitalBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("Start LDHospitalBL Submit...");

            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                CError tError = new CError();
                tError.moduleName = "OLHCustomHealthStatusBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }

        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        if (this.mOperate.equals("INSERT||MAIN")) {
            mLDHospitalSchema.setOperator(mGlobalInput.Operator);
            mLDHospitalSchema.setMakeDate(PubFun.getCurrentDate());
            mLDHospitalSchema.setMakeTime(PubFun.getCurrentTime());
            mLDHospitalSchema.setModifyDate(PubFun.getCurrentDate());
            mLDHospitalSchema.setModifyTime(PubFun.getCurrentTime());

            map.put(mLDHospitalSchema, "INSERT");

            if (mLDHospitalInfoIntroSet != null && mLDHospitalInfoIntroSet.size() > 0)
            {
                System.out.println(mLDHospitalInfoIntroSet.size());
                for (int i = 1; i <= mLDHospitalInfoIntroSet.size(); i++)
                {
                    System.out.println(mLDHospitalInfoIntroSet.get(i).getSpecialName());
                    mLDHospitalInfoIntroSet.get(i).setFlowNo(PubFun1.CreateMaxNo("HospitalInfo", 9));
                    mLDHospitalInfoIntroSet.get(i).setOperator(mGlobalInput.Operator);
                    mLDHospitalInfoIntroSet.get(i).setMakeDate(PubFun.getCurrentDate());
                    mLDHospitalInfoIntroSet.get(i).setMakeTime(PubFun.getCurrentTime());
                    mLDHospitalInfoIntroSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLDHospitalInfoIntroSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
                map.put(mLDHospitalInfoIntroSet, "INSERT");
            }

            if (mLDComHospitalSet != null &&  mLDComHospitalSet.size() > 0)
            {
                for (int i = 1; i <= mLDComHospitalSet.size(); i++)
                {
                    System.out.println(mLDComHospitalSet.get(i).getAssociatClass());
                    mLDComHospitalSet.get(i).setSerialNo(PubFun1.CreateMaxNo("ComHospital", 9));
                    mLDComHospitalSet.get(i).setOperator(mGlobalInput.Operator);
                    mLDComHospitalSet.get(i).setMakeDate(PubFun.getCurrentDate());
                    mLDComHospitalSet.get(i).setMakeTime(PubFun.getCurrentTime());
                    mLDComHospitalSet.get(i).setModifyDate(PubFun.getCurrentDate());
                    mLDComHospitalSet.get(i).setModifyTime(PubFun.getCurrentTime());
                }
                map.put(mLDComHospitalSet, "INSERT");
            }
        }

        if (this.mOperate.equals("UPDATE||MAIN")) {
            mLDHospitalSchema.setOperator(mGlobalInput.Operator);
            mLDHospitalSchema.setModifyDate(PubFun.getCurrentDate());
            mLDHospitalSchema.setModifyTime(PubFun.getCurrentTime());

            map.put(mLDHospitalSchema, "DELETE&INSERT");

//            if (mLDHospitalInfoIntroSet != null &&
//                mLDHospitalInfoIntroSet.size() > 0) {
                for (int i = 1; i <= mLDHospitalInfoIntroSet.size(); i++) {
                    if (mLDHospitalInfoIntroSet.get(i).getFlowNo() == null ||
                        mLDHospitalInfoIntroSet.get(i).getFlowNo().equals("")) {
                        mLDHospitalInfoIntroSet.get(i).setFlowNo(PubFun1.
                                CreateMaxNo("HospitalInfo", 9));
                    }
                    mLDHospitalInfoIntroSet.get(i).setOperator(mGlobalInput.
                            Operator);
                    mLDHospitalInfoIntroSet.get(i).setMakeDate(
                            mLDHospitalSchema.getModifyDate());
                    mLDHospitalInfoIntroSet.get(i).setMakeTime(
                            mLDHospitalSchema.getModifyTime());
                    mLDHospitalInfoIntroSet.get(i).setModifyDate(PubFun.
                            getCurrentDate());
                    mLDHospitalInfoIntroSet.get(i).setModifyTime(PubFun.
                            getCurrentTime());
                }
                map.put("delete from LDHospitalInfoIntro where HospitCode = '"+mLDHospitalSchema.getHospitCode()+"' and Specialclass = 'SpecSecDia'","DELETE");
                map.put("delete from LDHospitalInfoIntro where HospitCode = '"+mLDHospitalSchema.getHospitCode()+"' and Specialclass = 'Instrument'","DELETE");
                map.put(mLDHospitalInfoIntroSet, "INSERT");
//            }

//            if (mLDComHospitalSet != null &&
//                mLDComHospitalSet.size() > 0) {
                for (int i = 1; i <= mLDComHospitalSet.size(); i++) {
                    if (mLDComHospitalSet.get(i).getSerialNo() == null ||
                        mLDComHospitalSet.get(i).getSerialNo().equals("")) {
                        mLDComHospitalSet.get(i).setSerialNo(PubFun1.
                                CreateMaxNo("ComHospital", 9));
                    }
                    mLDComHospitalSet.get(i).setOperator(mGlobalInput.
                            Operator);
                    mLDComHospitalSet.get(i).setMakeDate(
                            mLDHospitalSchema.getModifyDate());
                    mLDComHospitalSet.get(i).setMakeTime(
                            mLDHospitalSchema.getModifyTime());
                    mLDComHospitalSet.get(i).setModifyDate(PubFun.
                            getCurrentDate());
                    mLDComHospitalSet.get(i).setModifyTime(PubFun.
                            getCurrentTime());
                }
                map.put("delete from LDComHospital where HospitCode = '"+mLDHospitalSchema.getHospitCode()+"'","DELETE");
                map.put(mLDComHospitalSet, "INSERT");
//            }


//            if (mLDHospitalInfoIntroSet != null &&
//                mLDHospitalInfoIntroSet.size() > 0) {
//                for (int i = 1; i <= mLDHospitalInfoIntroSet.size(); i++)
//                {
//                    if (mLDHospitalInfoIntroSet.get(i).getFlowNo() == null ||
//                        mLDHospitalInfoIntroSet.get(i).getFlowNo().equals(""))
//                    {
//                        mLDHospitalInfoIntroSet.get(i).setFlowNo(PubFun1.
//                                CreateMaxNo("FlowNo", 9));
//                    }
//                    mLDHospitalInfoIntroSet.get(i).setOperator(mGlobalInput.
//                            Operator);
//                    mLDHospitalInfoIntroSet.get(i).setMakeDate(
//                            mLDHospitalSchema.getModifyDate());
//                    mLDHospitalInfoIntroSet.get(i).setMakeTime(
//                            mLDHospitalSchema.getModifyTime());
//                    mLDHospitalInfoIntroSet.get(i).setModifyDate(PubFun.
//                            getCurrentDate());
//                    mLDHospitalInfoIntroSet.get(i).setModifyTime(PubFun.
//                            getCurrentTime());
//                }
////                map.put("delete from LDHospitalInfoIntro where HospitCode = '"+mLDHospitalSchema.getHospitCode()+"' and Specialclass = 'Instrument'","DELETE");
//                map.put(mLDHospitalInfoIntroSet, "DELETE||INSERT");
//            }

        }

        if (this.mOperate.equals("DELETE||MAIN"))
        {
            String type=mLDHospitalSchema.getHospitalType();
            if(type.equals("1"))
            {
                map.put(mLDHospitalSchema, "DELETE");
                map.put("delete from ldcomhospital where hospitcode = '"
                        + mLDComHospitalSet.get(1).getHospitCode() + "'",
                        "DELETE");
                map.put(mLDHospitalInfoIntroSet, "DELETE");
            }
            else
            {
                 map.put(mLDHospitalSchema, "DELETE");
            }


        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLDHospitalSchema.setSchema((LDHospitalSchema) cInputData.
                                         getObjectByObjectName(
                "LDHospitalSchema", 0));
        this.mLDHospitalInfoIntroSet.set((LDHospitalInfoIntroSet) cInputData.
                                         getObjectByObjectName(
                "LDHospitalInfoIntroSet", 0));
        this.mLDComHospitalSet.set((LDComHospitalSet) cInputData.
                                getObjectByObjectName("LDComHospitalSet",0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LDHospitalDB tLDHospitalDB = new LDHospitalDB();
        tLDHospitalDB.setSchema(this.mLDHospitalSchema);
        //如果有需要处理的错误，则返回
        if (tLDHospitalDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLDHospitalDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDHospitalBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLDHospitalSchema);
            this.mInputData.add(this.mLDHospitalInfoIntroSet);
            this.mInputData.add(this.mLDComHospitalSet);
            this.mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLDHospitalSchema);
            mResult.add(this.mLDHospitalInfoIntroSet);
            mResult.add(this.mLDComHospitalSet);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDHospitalBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
