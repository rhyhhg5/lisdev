/*
 * <p>ClassName: LHManaStandProBL </p>
 * <p>Description: LHManaStandProBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-09-11 19:50:38
 */
package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class LHManaStandProBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
private MMap map= new MMap();

/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
private String tNo;
/** 业务处理相关变量 */
private LHManaStandProSchema mLHManaStandProSchema=new LHManaStandProSchema();
//private LHManaStandProSet mLHManaStandProSet=new LHManaStandProSet();
    private LHStaQuOptionSet mLHStaQuOptionSet=new LHStaQuOptionSet();
public LHManaStandProBL() {

    }
public static void main(String[] args) {
}
/**
* 传输数据的公共方法
        * @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "LHManaStandProBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败LHManaStandProBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
      PubSubmit tPubSubmit = new PubSubmit();
      System.out.println("Start LHManaStandProBL Submit...");
      //LHManaStandProBLS tLHManaStandProBLS=new LHManaStandProBLS();
      //tLHManaStandProBLS.submitData(mInputData,mOperate);
      if (!tPubSubmit.submitData(mInputData, mOperate))
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);

        CError tError = new CError();
        tError.moduleName = "LHManaStandProBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据提交失败!";

        this.mErrors.addOneError(tError);
        return false;
      }

      System.out.println("End LHManaStandProBL Submit...");
      //如果有需要处理的错误，则返回
      //if (tLHManaStandProBLS.mErrors.needDealError())
      //{
        // @@错误处理
        //this.mErrors.copyAllErrors(tLHManaStandProBLS.mErrors);
       // CError tError = new CError();
        //tError.moduleName = "LHManaStandProBL";
        //tError.functionName = "submitDat";
        //tError.errorMessage ="数据提交失败!";
        //this.mErrors .addOneError(tError) ;
        //return false;
      //}
    }
    mInputData=null;
    return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
    if (this.mOperate.equals("INSERT||MAIN"))
    {
      mLHManaStandProSchema.setOperator(mGlobalInput.Operator);
      mLHManaStandProSchema.setMakeDate(PubFun.getCurrentDate());
      mLHManaStandProSchema.setMakeTime(PubFun.getCurrentTime());
      mLHManaStandProSchema.setModifyDate(PubFun.getCurrentDate());
      mLHManaStandProSchema.setModifyTime(PubFun.getCurrentTime());
      mLHManaStandProSchema.setManageCom(mGlobalInput.ManageCom);


      map.put(mLHManaStandProSchema, "INSERT");
      System.out.println("EEEEEEEEEEEEEEEEE "+ mLHStaQuOptionSet.size() );
      if (mLHStaQuOptionSet != null && mLHStaQuOptionSet.size() > 0) {

          for (int i = 1; i <= mLHStaQuOptionSet.size(); i++) {

                mLHStaQuOptionSet.get(i).setStandCode(mLHManaStandProSchema.getStandCode());

                 mLHStaQuOptionSet.get(i).setQuOptionNo(PubFun1.CreateMaxNo("QuOptionNo", 15));

              mLHStaQuOptionSet.get(i).setOperator(mGlobalInput.
                      Operator);
              mLHStaQuOptionSet.get(i).setMakeDate(PubFun.
                      getCurrentDate());
              mLHStaQuOptionSet.get(i).setMakeTime(PubFun.
                      getCurrentTime());
              mLHStaQuOptionSet.get(i).setModifyDate(PubFun.
                      getCurrentDate());
              mLHStaQuOptionSet.get(i).setModifyTime(PubFun.
                      getCurrentTime());
              mLHStaQuOptionSet.get(i).setManageCom(mGlobalInput.ManageCom);
              System.out.println(i);
          }

          map.put(mLHStaQuOptionSet, "INSERT");


      } else {
          System.out.println("mLHStaQuOptionSet is null");
      }
    }

    if (this.mOperate.equals("UPDATE||MAIN"))
    {
        mLHManaStandProSchema.setOperator(mGlobalInput.Operator);
        System.out.println("-------UPDATE--------");
        mLHManaStandProSchema.setOperator(mGlobalInput.Operator);
        mLHManaStandProSchema.setMakeDate(PubFun.getCurrentDate());
        mLHManaStandProSchema.setMakeTime(PubFun.getCurrentTime());
        mLHManaStandProSchema.setModifyDate(PubFun.getCurrentDate());
        mLHManaStandProSchema.setModifyTime(PubFun.getCurrentTime());
        mLHManaStandProSchema.setManageCom(mGlobalInput.ManageCom);
        map.put(mLHManaStandProSchema, "DELETE&INSERT");

        for (int i = 1; i <= mLHStaQuOptionSet.size(); i++)
        {
            if(mLHStaQuOptionSet.get(i).getQuOptionNo() == null ||
               mLHStaQuOptionSet.get(i).getQuOptionNo().equals("") )
            {
                mLHStaQuOptionSet.get(i).setQuOptionNo(PubFun1.CreateMaxNo(
                     "QuOptionNo", 15));
            }
            mLHStaQuOptionSet.get(i).setOperator(mGlobalInput.
                     Operator);
             mLHStaQuOptionSet.get(i).setMakeDate(PubFun.
                     getCurrentDate());
             mLHStaQuOptionSet.get(i).setMakeTime(PubFun.
                     getCurrentTime());
             mLHStaQuOptionSet.get(i).setModifyDate(PubFun.
                     getCurrentDate());
             mLHStaQuOptionSet.get(i).setModifyTime(PubFun.
                     getCurrentTime());
             mLHStaQuOptionSet.get(i).setManageCom(mGlobalInput.ManageCom);


        }
            map.put(" delete from  LHStaQuOption where StandCode='"+mLHManaStandProSchema.getStandCode()+"'","DELETE");
            map.put(mLHStaQuOptionSet, "INSERT");



    }

    if (this.mOperate.equals("DELETE||MAIN"))
    {
       map.put(mLHManaStandProSchema, "DELETE");
       map.put(mLHStaQuOptionSet, "DELETE");
    }

    return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData) {
      this.mLHManaStandProSchema.setSchema((LHManaStandProSchema)
                                         cInputData.
                                         getObjectByObjectName(
              "LHManaStandProSchema", 0));
      this.mLHStaQuOptionSet.set((LHStaQuOptionSet) cInputData.
                                      getObjectByObjectName(
                                              "LHStaQuOptionSet", 0));

      this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                  getObjectByObjectName("GlobalInput", 0));



      return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean submitquery() {
      this.mResult.clear();
      LHManaStandProDB tLHManaStandProDB = new LHManaStandProDB();
      tLHManaStandProDB.setSchema(this.mLHManaStandProSchema);
      //如果有需要处理的错误，则返回
      if (tLHManaStandProDB.mErrors.needDealError()) {
          // @@错误处理
          this.mErrors.copyAllErrors(tLHManaStandProDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "LHGroupContBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据提交失败!";
          this.mErrors.addOneError(tError);
          return false;
      }
      mInputData = null;
      return true;
  }

  private boolean prepareOutputData() {
      try {
          this.mInputData.clear();
          this.mInputData.add(this.mLHManaStandProSchema);
          this.mInputData.add(this.mLHStaQuOptionSet);
          this.mInputData.add(map);
          mResult.clear();
          mResult.add(this.mLHManaStandProSchema);
          mResult.add(this.mLHStaQuOptionSet);
      } catch (Exception ex) {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "LHManaStandProBL";
          tError.functionName = "prepareData";
          tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
          this.mErrors.addOneError(tError);
          return false;
      }
      return true;
  }

  public VData getResult() {
      return this.mResult;
  }
}
