/*
 * <p>ClassName: OLDTestGrpMgtBL </p>
 * <p>Description: OLDTestGrpMgtBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-06-13 10:00:52
 */
package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLDTestGrpMgtBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LDTestGrpMgtSchema mLDTestGrpMgtSchema = new LDTestGrpMgtSchema();
    private LDTestGrpMgtSet mLDTestGrpMgtSet = new LDTestGrpMgtSet();
    public OLDTestGrpMgtBL() {
    }

    public static void main(String[] args) {
        LDTestGrpMgtSchema tLDTestGrpMgtSchema = new LDTestGrpMgtSchema();
        LDTestGrpMgtSet tLDTestGrpMgtSet = new LDTestGrpMgtSet();
        OLDTestGrpMgtUI tOLDTestGrpMgtUI = new OLDTestGrpMgtUI();
        //输出参数
        CErrors tError = null;
        String tRela = "";
        String FlagStr = "";
        String Content = "";
        String transact = "INSERT||MAIN";
        GlobalInput tG = new GlobalInput();
        tG.Operator="001";
        tG.ComCode="86";
        tG.ManageCom="8600";


        //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录

        tLDTestGrpMgtSchema.setTestGrpCode("TestGrpCode");
        tLDTestGrpMgtSchema.setTestGrpName("TestGrpName");
        tLDTestGrpMgtSchema.setTestGrpType("TestGrpType");



            for (int i = 0; i < 3; i++) {
                tLDTestGrpMgtSchema.setTestItemCode("111111");

                tLDTestGrpMgtSchema.setOperator("hmhmhm");
                tLDTestGrpMgtSchema.setMakeDate("2004-05-05");
                tLDTestGrpMgtSchema.setMakeTime("2004-05-05");
                tLDTestGrpMgtSchema.setModifyDate("2004-05-05");
                tLDTestGrpMgtSchema.setModifyTime("2004-05-05");


                tLDTestGrpMgtSet.add(tLDTestGrpMgtSchema);
            }


        // 准备传输数据 VData
        System.out.println("------准备传输数据 VData--------");
        VData tVData = new VData();
        tVData.add(tLDTestGrpMgtSchema);
        tVData.add(tLDTestGrpMgtSet);
        tVData.add(tG);
        tOLDTestGrpMgtUI.submitData(tVData, transact);

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        this.mInputData = cInputData;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OLDTestGrpMgtBL";
            tError.functionName = "submitData";
            tError.errorMessage = "(OLDTestGrpMgtBL-->dealData!)套餐项目基本信息为空";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start OLDTestGrpMgtBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();

            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                CError tError = new CError();
                tError.moduleName = "OLDTestGrpMgtBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }

        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        if (this.mOperate.equals("INSERT||MAIN")) {
//            map.put(mLDTestGrpMgtSchema, "INSERT");
            if (mLDTestGrpMgtSet != null && mLDTestGrpMgtSet.size() > 0) {
            for (int i = 1; i <= mLDTestGrpMgtSet.size(); i++) {

//                mLDTestGrpMgtSet.get(i).setTestGrpCode(mLDTestGrpMgtSchema.getTestGrpCode());
//                mLDTestGrpMgtSet.get(i).setTestGrpName(mLDTestGrpMgtSchema.getTestGrpName());
//                mLDTestGrpMgtSet.get(i).setTestGrpType(mLDTestGrpMgtSchema.getTestGrpType());
                mLDTestGrpMgtSet.get(i).setOperator(mGlobalInput.
                        Operator);
                mLDTestGrpMgtSet.get(i).setMakeDate(PubFun.
                        getCurrentDate());
                mLDTestGrpMgtSet.get(i).setMakeTime(PubFun.
                        getCurrentTime());
                mLDTestGrpMgtSet.get(i).setModifyDate(PubFun.
                        getCurrentDate());
                mLDTestGrpMgtSet.get(i).setModifyTime(PubFun.
                        getCurrentTime());
                mLDTestGrpMgtSet.get(i).setManageCom(mGlobalInput.ManageCom);
            }
            map.put(mLDTestGrpMgtSet, "INSERT");
            }
            else return false;
        }


        if (this.mOperate.equals("UPDATE||MAIN")) {

//            map.put(mLDTestGrpMgtSchema, "DELETE&INSERT");
            if (mLDTestGrpMgtSet != null && mLDTestGrpMgtSet.size() > 0) {
            for (int i = 1; i <= mLDTestGrpMgtSet.size(); i++) {

                mLDTestGrpMgtSet.get(i).setOperator(mGlobalInput.
                        Operator);
                mLDTestGrpMgtSet.get(i).setMakeDate(
                        mLDTestGrpMgtSchema.getMakeDate());
                mLDTestGrpMgtSet.get(i).setMakeTime(
                        mLDTestGrpMgtSchema.getMakeTime());
                mLDTestGrpMgtSet.get(i).setModifyDate(PubFun.
                        getCurrentDate());
                mLDTestGrpMgtSet.get(i).setModifyTime(PubFun.
                        getCurrentTime());
                mLDTestGrpMgtSet.get(i).setManageCom(mGlobalInput.ManageCom);
            }

//            map.put("delete from LDTestGrpMgt where TestGrpCode ='" +
//                    mLDTestGrpMgtSchema.getTestGrpCode() + "'"
//                    + " and TestGrpType = '" + mLDTestGrpMgtSet.get(1).getTestGrpType() +
//                    "'", "DELETE");

            if(mLDTestGrpMgtSet.get(1).getTestGrpType().equals("5") ||
                 mLDTestGrpMgtSet.get(1).getTestGrpType().equals("2")   )
            {
                map.put("delete from LDTestGrpMgt where TestGrpCode ='" +
                   mLDTestGrpMgtSchema.getTestGrpCode()
                   +"' and TestGrpType = '" + mLDTestGrpMgtSet.get(1).getTestGrpType()
                   +"' and Explain = '"+mLDTestGrpMgtSet.get(1).getExplain()+"'", "DELETE");
            }
            else
            {
                map.put("delete from LDTestGrpMgt where TestGrpCode ='" +
                        mLDTestGrpMgtSchema.getTestGrpCode()
                        + "' and TestGrpType = '" +
                        mLDTestGrpMgtSet.get(1).getTestGrpType() + "'",
                        "DELETE");
            }


            map.put(mLDTestGrpMgtSet, "INSERT");
            }
            else return false;
        }

        if (this.mOperate.equals("DELETE||MAIN")) {
            System.out.println(mLDTestGrpMgtSchema.getTestGrpCode());
            if(mLDTestGrpMgtSet.get(1).getTestGrpType().equals("5") ||
                 mLDTestGrpMgtSet.get(1).getTestGrpType().equals("2")   )
            {
                map.put("delete from LDTestGrpMgt where TestGrpCode ='" +
                   mLDTestGrpMgtSchema.getTestGrpCode()
                   +"' and TestGrpType = '" + mLDTestGrpMgtSet.get(1).getTestGrpType()
                   +"' and Explain = '"+mLDTestGrpMgtSet.get(1).getExplain()+"'", "DELETE");
            }
            else
            {
                map.put("delete from LDTestGrpMgt where TestGrpCode ='" +
                        mLDTestGrpMgtSchema.getTestGrpCode()
                        + "' and TestGrpType = '" +
                        mLDTestGrpMgtSet.get(1).getTestGrpType() + "'",
                        "DELETE");
            }
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        this.mLDTestGrpMgtSchema.setSchema((LDTestGrpMgtSchema) cInputData.
                                           getObjectByObjectName(
                "LDTestGrpMgtSchema", 0));
        this.mLDTestGrpMgtSet.set((LDTestGrpMgtSet) cInputData.
                                  getObjectByObjectName("LDTestGrpMgtSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery() {
        this.mResult.clear();
        LDTestGrpMgtDB tLDTestGrpMgtDB = new LDTestGrpMgtDB();
        tLDTestGrpMgtDB.setSchema(this.mLDTestGrpMgtSchema);
        //如果有需要处理的错误，则返回
        if (tLDTestGrpMgtDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLDTestGrpMgtDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDTestGrpMgtBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLDTestGrpMgtSchema);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLDTestGrpMgtSchema);
            mResult.add(this.mLDTestGrpMgtSet);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LDTestGrpMgtBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }
}
