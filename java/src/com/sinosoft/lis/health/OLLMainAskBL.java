/*
 * <p>ClassName: OLLMainAskBL </p>
 * <p>Description: OLLMainAskBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-04-28 23:49:31
 */
package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class OLLMainAskBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
private MMap map= new MMap();

/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
/** 业务处理相关变量 */
private LLMainAskSchema mLLMainAskSchema=new LLMainAskSchema();
private LLConsultSchema mLLConsultSchema = new LLConsultSchema();
private LLAnswerInfoSchema mLLAnswerInfoSchema = new LLAnswerInfoSchema();


//private LLMainAskSet mLLMainAskSet=new LLMainAskSet();
public OLLMainAskBL() {
}
public static void main(String[] args) {

//输出参数
  String transact = "INSERT||MAIN";
  GlobalInput tG = new GlobalInput();
        tG.ManageCom="8600";
        tG.Operator="001";
        tG.ComCode="8600";

        CErrors tError = null;
               String tRela = "";
               String FlagStr = "";
               String Content = "";

               System.out.println("ddddddddddddddd");

    LLMainAskSchema tLLMainAskSchema   = new LLMainAskSchema();
      LLConsultSchema tLLConsultSchema = new LLConsultSchema();
      LLAnswerInfoSchema tLLAnswerInfoSchema=new LLAnswerInfoSchema();
      OLLMainAskBL tOLLMainAskBL   = new OLLMainAskBL();
      //输出参数


      //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录

       tLLMainAskSchema.setLogNo("LogNo");
       tLLMainAskSchema.setLogState("LogState");
       tLLMainAskSchema.setAskType("AskType");
       tLLMainAskSchema.setAskMode("AskMode");
       tLLMainAskSchema.setLogerNo("LogerNo");
       tLLMainAskSchema.setLogDate("LogDate");
       tLLMainAskSchema.setLogTime("LogTime");
       tLLMainAskSchema.setAnswerType("AnswerType");
       tLLMainAskSchema.setAnswerMode("AnswerMode");
//   tLLMainAskSchema.setExpectRevertDate("ExpectRevertDate");
//   tLLMainAskSchema.setExpectRevertTime("ExpectRevertTime");
       tLLMainAskSchema.setAskStyle("AskStyle");
       tLLMainAskSchema.setPromiseAnswerType("PromiseAnswerType");
       tLLMainAskSchema.setDelayRemain("DelayRemain");
       tLLMainAskSchema.setAskTotalTime("AskTotalTime");
       tLLMainAskSchema.setDelayEndDate("DelayEndDate");
       tLLMainAskSchema.setDelayEndTime("DelayEndTime");
       tLLMainAskSchema.setDelayBeginDate("DelayBeginDate");
       tLLMainAskSchema.setDelayBeginTime("DelayBeginTime");
       tLLMainAskSchema.setOperator("Operator");
       tLLMainAskSchema.setMakeDate("MakeDate");
       tLLMainAskSchema.setMakeTime("MakeTime");
       tLLMainAskSchema.setModifyDate("ModifyDate");
       tLLMainAskSchema.setModifyTime("ModifyTime");
       tLLMainAskSchema.setMngCom("MngCom");

       tLLConsultSchema.setConsultNo("ConsultNo");
       tLLConsultSchema.setLogNo("LogNo");
       tLLConsultSchema.setCContent("CContent");
       tLLConsultSchema.setReplyState("ReplyState");
       tLLConsultSchema.setOperator("Operator");
       tLLConsultSchema.setMakeDate("MakeDate");
       tLLConsultSchema.setMakeTime("MakeTime");
       tLLConsultSchema.setModifyDate("ModifyDate");
       tLLConsultSchema.setModifyTime("ModifyTime");
       tLLConsultSchema.setMngCom("MngCom");
       tLLConsultSchema.setCustomerNo("LogerNo");


       tLLAnswerInfoSchema.setConsultNo("ConsultNo");
       tLLAnswerInfoSchema.setLogNo("LogNo");
       tLLAnswerInfoSchema.setAnswer("Answer");
       tLLAnswerInfoSchema.setAnswerDate("AnswerDate");
       tLLAnswerInfoSchema.setAnswerTime("AnswerTime");
       tLLAnswerInfoSchema.setAnswerEndDate("AnswerEndDate");
       tLLAnswerInfoSchema.setAnswerEndTime("AnswerEndTime");
       tLLAnswerInfoSchema.setAnswerCostTime("AnswerCostTime");
       tLLAnswerInfoSchema.setRemark	("Remark");
       //tLLAnswerInfoSchema.setOperator("Operator");
       tLLAnswerInfoSchema.setMakeDate("MakeDate");
       tLLAnswerInfoSchema.setMakeTime("MakeTime");
       tLLAnswerInfoSchema.setModifyDate("ModifyDate");
       tLLAnswerInfoSchema.setModifyTime("ModifyTime");
       tLLAnswerInfoSchema.setSerialNo("1");




      // 准备传输数据 VData
             VData tVData = new VData();
              tVData.add(tLLMainAskSchema);

              tVData.add(tLLConsultSchema);
              tVData.add(tLLAnswerInfoSchema);

            tVData.add(tG);
         tOLLMainAskBL.submitData(tVData,transact);


}
/**
* 传输数据的公共方法
	* @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "OLLMainAskBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败OLLMainAskBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
      System.out.println("Start OLLMainAskBL Submit...");
//      OLLMainAskBLS tOLLMainAskBLS=new OLLMainAskBLS();

      PubSubmit tPubSubmit = new PubSubmit();
           if (!tPubSubmit.submitData(mInputData, mOperate))
             {
               // @@错误处理
               this.mErrors.copyAllErrors(tPubSubmit.mErrors);
               CError tError = new CError();
               tError.moduleName = "OLLMainAskBL";
               tError.functionName = "submitData";
               tError.errorMessage = "数据提交失败!";

               this.mErrors.addOneError(tError);
               return false;
             }
    }
    mInputData=null;
    return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
    if (this.mOperate.equals("INSERT||MAIN"))
   {
      mLLMainAskSchema.setLogNo(PubFun1.CreateMaxNo("CNNO","86110000"));System.out.println(mLLMainAskSchema.getLogNo());
      mLLMainAskSchema.setOperator(mGlobalInput.Operator);
      mLLMainAskSchema.setMakeDate(PubFun.getCurrentDate());
      mLLMainAskSchema.setMakeTime(PubFun.getCurrentTime());
      mLLMainAskSchema.setModifyDate(PubFun.getCurrentDate());
      mLLMainAskSchema.setModifyTime(PubFun.getCurrentTime());
      mLLMainAskSchema.setMngCom(mGlobalInput.ManageCom);

      mLLConsultSchema.setConsultNo(mLLMainAskSchema.getLogNo());
      mLLConsultSchema.setLogNo(mLLMainAskSchema.getLogNo());
      mLLConsultSchema.setOperator(mGlobalInput.Operator);
      mLLConsultSchema.setMngCom(mGlobalInput.ManageCom);
      mLLConsultSchema.setMakeDate(PubFun.getCurrentDate());
      mLLConsultSchema.setMakeTime(PubFun.getCurrentTime());
      mLLConsultSchema.setModifyDate(PubFun.getCurrentDate());
      mLLConsultSchema.setModifyTime(PubFun.getCurrentTime());

      mLLAnswerInfoSchema.setConsultNo(mLLMainAskSchema.getLogNo());
      mLLAnswerInfoSchema.setLogNo(mLLMainAskSchema.getLogNo());
      mLLAnswerInfoSchema.setMakeDate(PubFun.getCurrentDate());
      mLLAnswerInfoSchema.setMakeTime(PubFun.getCurrentTime());
      mLLAnswerInfoSchema.setModifyDate(PubFun.getCurrentDate());
      mLLAnswerInfoSchema.setModifyTime(PubFun.getCurrentTime());

     map.put(mLLMainAskSchema, "INSERT");
     map.put(mLLConsultSchema, "INSERT");
     map.put(mLLAnswerInfoSchema, "INSERT");
   }
   if (this.mOperate.equals("UPDATE||MAIN"))
   {
    System.out.println("---------------In Update---------------");
    mLLMainAskSchema.setOperator(mGlobalInput.Operator);
    mLLMainAskSchema.setMngCom(mGlobalInput.ManageCom);
    mLLMainAskSchema.setModifyDate(PubFun.getCurrentDate());
    mLLMainAskSchema.setModifyTime(PubFun.getCurrentTime());
    mLLConsultSchema.setOperator(mGlobalInput.Operator);
    mLLConsultSchema.setMngCom(mGlobalInput.ManageCom);
    mLLConsultSchema.setModifyDate(PubFun.getCurrentDate());
    mLLConsultSchema.setModifyTime(PubFun.getCurrentTime());
    mLLAnswerInfoSchema.setModifyDate(PubFun.getCurrentDate());
    mLLAnswerInfoSchema.setModifyTime(PubFun.getCurrentTime());

   map.put(mLLMainAskSchema, "UPDATE");
		  map.put(mLLConsultSchema, "UPDATE");
		  map.put(mLLAnswerInfoSchema, "UPDATE");
   }
   if (this.mOperate.equals("DELETE||MAIN"))
   {
     map.put(mLLMainAskSchema, "DELETE");
		 map.put(mLLConsultSchema, "DELETE");
		 map.put(mLLAnswerInfoSchema, "DELETE");
   }
    return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
    this.mLLMainAskSchema.setSchema((LLMainAskSchema)cInputData.getObjectByObjectName("LLMainAskSchema",0));

    this.mLLConsultSchema.setSchema((LLConsultSchema)cInputData.getObjectByObjectName("LLConsultSchema",0));

    this.mLLAnswerInfoSchema.setSchema((LLAnswerInfoSchema)cInputData.getObjectByObjectName("LLAnswerInfoSchema",0));
    this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
    this.mResult.clear();
    LLMainAskDB tLLMainAskDB=new LLMainAskDB();
    tLLMainAskDB.setSchema(this.mLLMainAskSchema);
		//如果有需要处理的错误，则返回
		if (tLLMainAskDB.mErrors.needDealError())
 		{
		  // @@错误处理
 			this.mErrors.copyAllErrors(tLLMainAskDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "LLMainAskBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors .addOneError(tError) ;
 			return false;
    }
    mInputData=null;
    return true;
}
 private boolean prepareOutputData()
 {

            try
            {
                this.mInputData.clear();
                this.mInputData.add(this.mLLMainAskSchema);
//                this.mInputData.add(this.mLLConsultSchema);
//                this.mInputData.add(this.mLLAnswerInfoSchema);
                mInputData.add(map);
                mResult.clear();
                this.mResult.add(this.mLLMainAskSchema);
//                this.mResult.add(this.mLLConsultSchema);
//                this.mResult.add(this.mLLAnswerInfoSchema);
            } catch (Exception ex) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LLMainAskUI";
                tError.functionName = "prepareData";
                tError.errorMessage = "在准备往后层处所需要的数据时出错。";
                this.mErrors.addOneError(tError);
                return false;
            }


	return true;
	}
	public VData getResult()
	{
  	return this.mResult;
	}
}
