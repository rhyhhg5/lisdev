package com.sinosoft.lis.health;

import java.util.*;
import java.io.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LDSysVarSchema;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;
import com.sinosoft.lis.schema.LCScanDownloadSchema;
import com.sinosoft.lis.vdb.LCScanDownloadDBSet;
import com.sinosoft.lis.easyscan.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class OLHGrpPersonUniteXls {
    public OLHGrpPersonUniteXls() {}

    String FullPath = "";

    public static void main(String[] args) {
        OLHGrpPersonUniteXls t = new OLHGrpPersonUniteXls();
        t.Downloadxls("1400002432", "E:\\workspace\\ui\\");
        t.getResult();
    }

    public boolean CreateZip(String[] tFilePaths, String[] tFileNames,
                             String tZipPath) {
        ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
        System.out.println(tFilePaths + "-----------" + tFileNames +
                           "---------" + tZipPath);
        if (!tProposalDownloadBL.CreatZipFile(tFilePaths, tFileNames, tZipPath)) {
            System.out.println("生成压缩文件失败");
            return false;
        } else {
            return true;
        }
    }

    public boolean Downloadxls(String grpcontno, String ZipPath) {
        try {
            ZipPath = ZipPath + "healthmanage/";
            String Path;
            Path = PubFun.getCurrentDate() + PubFun.getCurrentTime() + ".xls";
            Path = Path.replaceAll(":", "-");
            WriteToExcel t = new WriteToExcel(Path);
            String[] FilePaths = new String[1];
            FilePaths[0] = ZipPath + Path;
            String[] FileNames = new String[1];
            FileNames[0] = "LHGrpPersonUnite.xls"; //PubFun.getCurrentDate()+PubFun.getCurrentTime();
            String[][] mToExcel = new String[1][18];
            mToExcel[0][0] = "团体服务计划号码";
            mToExcel[0][1] = "团体服务项目号码";
            mToExcel[0][2] = "团体保单号";
            mToExcel[0][3] = "团体客户号";
            mToExcel[0][4] = "团体客户名称";
            mToExcel[0][5] = "客户号";
            mToExcel[0][6] = "姓名";
            mToExcel[0][7] = "性别";
            mToExcel[0][8] = "出生日期";
            mToExcel[0][9] = "证件类型";
            mToExcel[0][10] = "证件号码";
            mToExcel[0][11] = "分级标识";
            mToExcel[0][12] = "管理机构";
            mToExcel[0][13] = "操作员代码";
            mToExcel[0][14] = "入机日期";
            mToExcel[0][15] = "入机时间";
            mToExcel[0][16] = "最后一次修改日期";
            mToExcel[0][17] = "最后一次修改时间";

            t.createExcelFile();
            String[] sheetName = {"GRP"};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            String sqlx = "select distinct '请勿修改','请勿修改',a.grpcontno,b.appntno, b.grpname,a.insuredno,a.name,a.sex,a.birthday,a.idtype,a.idno,a.contplancode,a.ManageCom,a.Operator,a.MakeDate,a.MakeTime,a.ModifyDate,a.ModifyTime"
                      + " from lcinsured a,lcgrpcont b where  a.grpcontno='"
                      + grpcontno
                      + "'  and b.grpcontno=a.grpcontno"
                      ;
            System.out.println(sqlx);
            t.setData(0,sqlx);
            System.out.println("ZipPath  : " + ZipPath);
            t.write(ZipPath);
            String NewPath;
            NewPath = PubFun.getCurrentDate() + PubFun.getCurrentTime() +
                      ".zip";
            NewPath = NewPath.replaceAll(":", "-");
            FullPath = NewPath;
            NewPath = ZipPath + NewPath;
            CreateZip(FilePaths, FileNames, NewPath);
            File fd = new File(FilePaths[0]);
            fd.delete();
            System.out.println("FullPath  : " + NewPath);
          /*  File Zipd = new File(NewPath);
            Zipd.delete();
           */

        } catch (Exception ex) {
            ex.toString();
            ex.printStackTrace();
        }
        return true;
    }

    public String getResult() {
        System.out.println("FullPath  : " + FullPath);
        return FullPath;

    }
}
