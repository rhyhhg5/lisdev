/*
 * <p>ClassName: LHServiceManageLookBL </p>
 * <p>Description: LHServiceManageLookBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-08-16 9:04:38
 */
package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class LHServiceManageLookBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
private MMap map= new MMap();

/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
private String tNo;
/** 业务处理相关变量 */
    private TransferData mTransferData = new TransferData();
private LHTaskCustomerRelaSet mLHTaskCustomerRelaSet=new LHTaskCustomerRelaSet();


    public LHServiceManageLookBL()
    {
        try {jbInit();}
        catch (Exception ex)
        {ex.printStackTrace();}
    }
public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        TransferData mTransferData = new TransferData();
        LHServiceManageLookUI tLHServiceManageLookUI   = new LHServiceManageLookUI();
        LHTaskCustomerRelaSet mLHTaskCustomerRelaSet=new LHTaskCustomerRelaSet();

        tG.Operator = "hm";
        tG.ManageCom = "86";
        tG.ComCode = "86";
        mTransferData.setNameAndValue("ServCaseCode","20060908200117");
        mTransferData.setNameAndValue("ServTaskNo","000000002174");

        VData tVData = new VData();
        tVData.add(mLHTaskCustomerRelaSet);
        tVData.addElement(mTransferData);
        tVData.add(tG);
        tLHServiceManageLookUI.submitData(tVData,"INSERT||MAIN");

    }
/**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "LHServiceManageLookBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败LHServiceManageLookBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
      PubSubmit tPubSubmit = new PubSubmit();
      System.out.println("Start LHServiceManageLookBL Submit...");
      //LHServiceManageLookBLS tLHServiceManageLookBLS=new LHServiceManageLookBLS();
      //tLHServiceManageLookBLS.submitData(mInputData,mOperate);
      if (!tPubSubmit.submitData(mInputData, mOperate))
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);

        CError tError = new CError();
        tError.moduleName = "LHServiceManageLookBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据提交失败!";

        this.mErrors.addOneError(tError);
        return false;
      }

      System.out.println("End LHServiceManageLookBL Submit...");
      //如果有需要处理的错误，则返回
      //if (tLHServiceManageLookBLS.mErrors.needDealError())
      //{
        // @@错误处理
        //this.mErrors.copyAllErrors(tLHServiceManageLookBLS.mErrors);
       // CError tError = new CError();
        //tError.moduleName = "LHServiceManageLookBL";
        //tError.functionName = "submitDat";
        //tError.errorMessage ="数据提交失败!";
        //this.mErrors .addOneError(tError) ;
        //return false;
      //}
    }
    mInputData=null;
    return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
    if (this.mOperate.equals("INSERT||MAIN"))
    {
        if(mTransferData != null)
        {
            String arrTaskCustomer[][];
            SSRS SSRS_TaskCustomer = new SSRS();
            ExeSQL tTaskCustomer= new ExeSQL();
            String  sqlServTaskNo = "'"+mTransferData.getValueByName("ServTaskNo").toString().replaceAll(",","','")+"'";
            String sql= " select a.ServCaseCode,d.ServTaskNo,b.ServItemNo,d.ServTaskCode"
                        +" from LHServCaseDef a,LHServCaseRela b ,LHCaseTaskRela d"
                        +" where a.ServCaseCode=b.ServCaseCode and a.ServCaseCode=d.ServCaseCode "
                        +" and d.ServTaskNo IN ("+sqlServTaskNo+")"
                        ;
            SSRS_TaskCustomer = tTaskCustomer.execSQL(sql);
            arrTaskCustomer = SSRS_TaskCustomer.getAllData();
            mLHTaskCustomerRelaSet.clear();

            System.out.println("该任务对应的客户数量："+arrTaskCustomer.length);
            for (int j = 1; j <= arrTaskCustomer.length; j++)
            {
                LHTaskCustomerRelaSchema tLHTaskCustomerRelaSchema = new LHTaskCustomerRelaSchema();
                tLHTaskCustomerRelaSchema.setTaskExecNo(PubFun1.CreateMaxNo("TaskExecNo", 12));
                tLHTaskCustomerRelaSchema.setServCaseCode(arrTaskCustomer[j-1][0]);
                tLHTaskCustomerRelaSchema.setServTaskNo(arrTaskCustomer[j-1][1]);
                tLHTaskCustomerRelaSchema.setServItemNo(arrTaskCustomer[j-1][2]);
                tLHTaskCustomerRelaSchema.setServTaskCode(arrTaskCustomer[j-1][3]);
                System.out.println("000000000000000000000"+mTransferData.getValueByName("StateFlag").toString());
                if(mTransferData.getValueByName("StateFlag").toString().equals("finish"))
                {
                    tLHTaskCustomerRelaSchema.setTaskExecState("2");
                }
                else
                {
                    tLHTaskCustomerRelaSchema.setTaskExecState("1");
                }
                tLHTaskCustomerRelaSchema.setOperator(mGlobalInput.Operator);
                tLHTaskCustomerRelaSchema.setMakeDate(PubFun.getCurrentDate());
                tLHTaskCustomerRelaSchema.setMakeTime(PubFun.getCurrentTime());
                tLHTaskCustomerRelaSchema.setModifyDate(PubFun.getCurrentDate());
                tLHTaskCustomerRelaSchema.setModifyTime(PubFun.getCurrentTime());
                tLHTaskCustomerRelaSchema.setManageCom(mGlobalInput.ManageCom);
                mLHTaskCustomerRelaSet.add(tLHTaskCustomerRelaSchema);
            }
            map.put(mLHTaskCustomerRelaSet, "INSERT");
        }
    }

    if (this.mOperate.equals("UPDATE||MAIN"))
    {
         for (int j= 1; j<= mLHTaskCustomerRelaSet.size(); j++)
         {
//             if (mLHTaskCustomerRelaSet.get(j).getTaskExecNo() == null ||
//                 mLHTaskCustomerRelaSet.get(j).getTaskExecNo().equals(""))
//             {
//                 mLHTaskCustomerRelaSet.get(j).setTaskExecNo(PubFun1.CreateMaxNo("TaskExecNo", 12));
//             }
             mLHTaskCustomerRelaSet.get(j).setOperator(mGlobalInput.Operator);
             mLHTaskCustomerRelaSet.get(j).setMakeDate(PubFun.getCurrentDate());
             mLHTaskCustomerRelaSet.get(j).setMakeTime(PubFun.getCurrentTime());
             mLHTaskCustomerRelaSet.get(j).setModifyDate(PubFun.getCurrentDate());
             mLHTaskCustomerRelaSet.get(j).setModifyTime(PubFun.getCurrentTime());
             mLHTaskCustomerRelaSet.get(j).setManageCom(mGlobalInput.ManageCom);

             map.put(" delete from  LHTaskCustomerRela where TaskExecNo = '"
                     +mLHTaskCustomerRelaSet.get(j).getTaskExecNo()+"'","DELETE");
         }
         map.put(mLHTaskCustomerRelaSet, "INSERT");
     }

     if (this.mOperate.equals("DELETE||MAIN"))
     {
         for (int j = 1; j <= mLHTaskCustomerRelaSet.size(); j++)
         {
             map.put(" delete from  LHCaseTaskRela where ServTaskNo='" +
                     mLHTaskCustomerRelaSet.get(1).getServTaskNo() +
                     "'","DELETE");
         }
     }

     return true;
}
/**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
 */
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{

         this.mLHTaskCustomerRelaSet.set((LHTaskCustomerRelaSet)cInputData.getObjectByObjectName("LHTaskCustomerRelaSet",0));
         this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         if (mTransferData != null)
         {
             mTransferData = ((TransferData) cInputData.getObjectByObjectName("TransferData", 0));
         }
         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
    this.mResult.clear();
    LHTaskCustomerRelaDB tLHTaskCustomerRelaDB=new LHTaskCustomerRelaDB();

    //如果有需要处理的错误，则返回
    if (tLHTaskCustomerRelaDB.mErrors.needDealError())
    {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHTaskCustomerRelaDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHServiceManageLookBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors .addOneError(tError) ;
            return false;
    }
    mInputData=null;
    return true;
}
 private boolean prepareOutputData()
 {
     try
     {
         this.mInputData.clear();
         this.mInputData.add(this.mLHTaskCustomerRelaSet);
         mInputData.add(map);
         mResult.clear();
         mResult.add(this.mLHTaskCustomerRelaSet);
     }
     catch(Exception ex)
     {
          // @@错误处理
          CError tError =new CError();
          tError.moduleName="LHServiceManageLookBL";
          tError.functionName="prepareData";
          tError.errorMessage="在准备往后层处理所需要的数据时出错。";
          this.mErrors .addOneError(tError) ;
          return false;
      }
      return true;
  }
  public VData getResult()
  {
      return this.mResult;
  }

  private void jbInit() throws Exception {
  }
}
