/*
 * <p>ClassName: OLDDoctorBL </p>
 * <p>Description: OLDDoctorBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-19 10:10:52
 */
package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class OLDDoctorBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
private MMap map= new MMap();
/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
/** 业务处理相关变量 */
private LDDoctorSchema mLDDoctorSchema=new LDDoctorSchema();
//private LDDoctorSet mLDDoctorSet=new LDDoctorSet();
public OLDDoctorBL() {
}
public static void main(String[] args) {
}
/**
* 传输数据的公共方法
	* @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "OLDDoctorBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败OLDDoctorBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
      System.out.println("Start OLDDoctorBL Submit...");
/*      OLDDoctorBLS tOLDDoctorBLS=new OLDDoctorBLS();
      tOLDDoctorBLS.submitData(mInputData,mOperate);
      System.out.println("End OLDDoctorBL Submit...");
      //如果有需要处理的错误，则返回
      if (tOLDDoctorBLS.mErrors.needDealError())
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tOLDDoctorBLS.mErrors);
        CError tError = new CError();
        tError.moduleName = "OLDDoctorBL";
        tError.functionName = "submitDat";
        tError.errorMessage ="数据提交失败!";
        this.mErrors .addOneError(tError) ;
        return false;
      }
 */
         PubSubmit tPubSubmit = new PubSubmit();
         if (!tPubSubmit.submitData(mInputData, mOperate))
         {
           // @@错误处理
           this.mErrors.copyAllErrors(tPubSubmit.mErrors);
           CError tError = new CError();
           tError.moduleName = "OLDDoctor";
           tError.functionName = "submitData";
           tError.errorMessage = "数据提交失败!";

           this.mErrors.addOneError(tError);
           return false;
         }
         System.out.println("End OLDDoctor Submit...");

       }
       mInputData = null;
       return true;
     }

     /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
  if (this.mOperate.equals("INSERT||MAIN"))
    {
      mLDDoctorSchema.setOperator(mGlobalInput.Operator);
      mLDDoctorSchema.setMakeDate(PubFun.getCurrentDate());
      mLDDoctorSchema.setMakeTime(PubFun.getCurrentTime());
      mLDDoctorSchema.setModifyDate(PubFun.getCurrentDate());
      mLDDoctorSchema.setModifyTime(PubFun.getCurrentTime());

      map.put(mLDDoctorSchema, "INSERT");
    }

    if (this.mOperate.equals("UPDATE||MAIN"))
    {
        mLDDoctorSchema.setOperator(mGlobalInput.Operator);
        mLDDoctorSchema.setMakeDate(PubFun.getCurrentDate());
        mLDDoctorSchema.setMakeTime(PubFun.getCurrentTime());
        mLDDoctorSchema.setModifyDate(PubFun.getCurrentDate());
        mLDDoctorSchema.setModifyTime(PubFun.getCurrentTime());
      map.put(mLDDoctorSchema, "UPDATE");
    }

    if (this.mOperate.equals("DELETE||MAIN"))
    {
      map.put(mLDDoctorSchema, "DELETE");
    }

    return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
         this.mLDDoctorSchema.setSchema((LDDoctorSchema)cInputData.getObjectByObjectName("LDDoctorSchema",0));
         this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
    this.mResult.clear();
    LDDoctorDB tLDDoctorDB=new LDDoctorDB();
    tLDDoctorDB.setSchema(this.mLDDoctorSchema);
		//如果有需要处理的错误，则返回
		if (tLDDoctorDB.mErrors.needDealError())
 		{
		  // @@错误处理
 			this.mErrors.copyAllErrors(tLDDoctorDB.mErrors);
			CError tError = new CError();
			tError.moduleName = "LDDoctorBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据提交失败!";
			this.mErrors .addOneError(tError) ;
 			return false;
    }
    mInputData=null;
    return true;
}
 private boolean prepareOutputData()
 {
   try
	{
		this.mInputData.clear();
                mInputData.add(map);
		mResult.clear();
                mResult.add(this.mLDDoctorSchema);
	}
	catch(Exception ex)
	{
 		// @@错误处理
		CError tError =new CError();
 		tError.moduleName="LDDoctorBL";
 		tError.functionName="prepareData";
 		tError.errorMessage="在准备往后层处理所需要的数据时出错。";
 		this.mErrors .addOneError(tError) ;
		return false;
	}
	return true;
	}
	public VData getResult()
	{
  	return this.mResult;
	}
}
