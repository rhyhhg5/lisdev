package com.sinosoft.lis.health;

import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.vschema.LCScanDownloadSet;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.vschema.ES_DOC_MAINSet;
import com.sinosoft.lis.schema.ES_DOC_MAINSchema;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class EsPicDownloadUI {
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private VData mInputData;
    private String mOperate;
    private GlobalInput mGlobalInput = new GlobalInput();
    private ES_DOC_MAINSet mES_DOC_MAINSet = new ES_DOC_MAINSet();

    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if (!cOperate.equals("DOWNLOAD") //   第一次下载
                && !cOperate.equals("REDOWNLOAD")) { //重新下载
                buildError("submitData", "不支持的操作字符串");
                return false;
            }
            if (!getInputData(cInputData)) {
                return false;
            }
            if (!dealData()) {
                return false;
            }
            VData vData = new VData();

            if (!prepareOutputData(vData)) {
                return false;
            }
            EsPicDownloadBL mEsPicDownloadBL = new EsPicDownloadBL();
            System.out.println("Start EsPicDownloadUI Submit ...");

            if (!mEsPicDownloadBL.submitData(vData, cOperate)) {
                System.out.println("下载失败");
                return false;
            } else {
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("submit", "发生异常");
            return false;
        }
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "LCContF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args) {
        ES_DOC_MAINSchema tES_DOC_MAINSchema = new ES_DOC_MAINSchema();
        ES_DOC_MAINSet tES_DOC_MAINSet = new ES_DOC_MAINSet();
        tES_DOC_MAINSchema.setDocID(82);
        tES_DOC_MAINSchema.setDocCode("16000001915");
        tES_DOC_MAINSchema.setManageCom("86110000");
        tES_DOC_MAINSet.add(tES_DOC_MAINSchema);
        GlobalInput ccGlobalInput = new GlobalInput();
        ccGlobalInput.Operator = "picch";
        ccGlobalInput.ManageCom = "86110000";

        VData tVData = new VData();
        tVData.add(tES_DOC_MAINSet);
        tVData.add(ccGlobalInput);
// 准备传输数据 VData
        EsPicDownloadUI tEsPicDownloadUI = new EsPicDownloadUI();
        System.out.println("--EsPicDownload.jsp--before submitData");
        tEsPicDownloadUI.submitData(tVData, "DOWNLOAD");
        System.out.println("--EsPicDownload.jsp--after  submitData");

    }

    private boolean getInputData(VData cInputData) {
        mES_DOC_MAINSet.set((ES_DOC_MAINSet) cInputData.
                            getObjectByObjectName(
                                    "ES_DOC_MAINSet", 0));
        mGlobalInput = (GlobalInput) cInputData.
                       getObjectByObjectName(
                               "GlobalInput", 0);
        System.out.println("mGlobalInput" + mGlobalInput.Operator);
        System.out.println("mGlobalInput" + mGlobalInput.ManageCom);
        if (mGlobalInput == null) {
            buildError("getInputData", "没有得到足够的信息！");
            return false;
        }

        if (mES_DOC_MAINSet.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    private static boolean dealData() {
        return true;
    }

    private boolean prepareOutputData(VData vData) {
        try {
            vData.clear();
            vData.add(mGlobalInput);
            vData.add(mES_DOC_MAINSet);
        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }

}
