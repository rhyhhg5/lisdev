/*
 * <p>ClassName: OLDContraItemDutyBL </p>
 * <p>Description: OLDContraItemDutyBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-05-04 15:09:46
 */
package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class OLDContraItemDutyBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
private MMap map = new MMap();
/** 数据操作字符串 */
private String mOperate;
/** 业务处理相关变量 */
private LDContraItemDutySchema mLDContraItemDutySchema=new LDContraItemDutySchema();
//private LDContraItemDutySet mLDContraItemDutySet=new LDContraItemDutySet();
public OLDContraItemDutyBL() {
}
public static void main(String[] args) {

    OLDContraItemDutyUI tOLDContraItemDutyUI = new OLDContraItemDutyUI();
  LDContraItemDutySchema tLDContraItemDutySchema = new LDContraItemDutySchema();

  //输出参数
  CErrors tError = null;
  String tRela  = "008";
  String FlagStr = "008";
  String Content = "008";
  String transact = "008";
  GlobalInput tG = new GlobalInput();

  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
        transact = "fmtransact";
    tLDContraItemDutySchema.setDutyItemCode("DutyItemCode");
    tLDContraItemDutySchema.setDutyItemName("DutyItemName");
    tLDContraItemDutySchema.setDutyItemTye("DutyItemTye");
    tLDContraItemDutySchema.setDutyItemExplain("DutyItemExplain");

    tLDContraItemDutySchema.setOperator("Operator");
    tLDContraItemDutySchema.setMakeDate("MakeDate");
    tLDContraItemDutySchema.setMakeTime("MakeTime");
    tLDContraItemDutySchema.setModifyDate("ModifyDate");
    tLDContraItemDutySchema.setModifyTime("ModifyTime");

  // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(tLDContraItemDutySchema);
        tVData.add(tG);
    tOLDContraItemDutyUI.submitData(tVData,transact);



}
/**
* 传输数据的公共方法
	* @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    this.mInputData = cInputData;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "OLDContraItemDutyBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败OLDContraItemDutyBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
      System.out.println("Start OLDContraItemDutyBL Submit...");
       PubSubmit tPubSubmit = new PubSubmit();
       if (!tPubSubmit.submitData(mInputData, mOperate))
       {
         // @@错误处理
         this.mErrors.copyAllErrors(tPubSubmit.mErrors);
         CError tError = new CError();
         tError.moduleName = "OLDContraItemDutyBL";
         tError.functionName = "submitData";
         tError.errorMessage = "数据提交失败!";

         this.mErrors.addOneError(tError);
         return false;
       }

    }
    mInputData=null;
    return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
    if (this.mOperate.equals("INSERT||MAIN"))
    {
      System.out.println("---------------In INSERT---------------");
      mLDContraItemDutySchema.setOperator(mGlobalInput.Operator);
      mLDContraItemDutySchema.setMakeDate(PubFun.getCurrentDate());
      mLDContraItemDutySchema.setMakeTime(PubFun.getCurrentTime());
      mLDContraItemDutySchema.setModifyDate(PubFun.getCurrentDate());
      mLDContraItemDutySchema.setModifyTime(PubFun.getCurrentTime());
      mLDContraItemDutySchema.setManageCom(mGlobalInput.ManageCom);
      map.put(mLDContraItemDutySchema, "INSERT");
    }
    if (this.mOperate.equals("UPDATE||MAIN"))
    {
        System.out.println("---------------In Update---------------");
        mLDContraItemDutySchema.setModifyDate(PubFun.getCurrentDate());
        mLDContraItemDutySchema.setModifyTime(PubFun.getCurrentTime());
        mLDContraItemDutySchema.setOperator(mGlobalInput.ManageCom);
        mLDContraItemDutySchema.setManageCom(mGlobalInput.ManageCom);
        map.put(mLDContraItemDutySchema, "UPDATE");
    }
    if (this.mOperate.equals("DELETE||MAIN"))
    {
        map.put(mLDContraItemDutySchema, "DELETE");
    }
    return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
         this.mLDContraItemDutySchema.setSchema((LDContraItemDutySchema)cInputData.getObjectByObjectName("LDContraItemDutySchema",0));
         this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
    this.mResult.clear();
    LDContraItemDutyDB tLDContraItemDutyDB=new LDContraItemDutyDB();
    tLDContraItemDutyDB.setSchema(this.mLDContraItemDutySchema);
		//如果有需要处理的错误，则返回
    if (tLDContraItemDutyDB.mErrors.needDealError())
    {
            // @@错误处理
            this.mErrors.copyAllErrors(tLDContraItemDutyDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LDContraItemDutyBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors .addOneError(tError) ;
            return false;
        }
        mInputData=null;
        return true;
}
 private boolean prepareOutputData()
 {
   try
	{
		this.mInputData.clear();
		this.mInputData.add(this.mLDContraItemDutySchema);
                mInputData.add(map);
		mResult.clear();
                mResult.add(this.mLDContraItemDutySchema);
	}
	catch(Exception ex)
	{
 		// @@错误处理
		CError tError =new CError();
 		tError.moduleName="LDContraItemDutyBL";
 		tError.functionName="prepareData";
 		tError.errorMessage="在准备往后层处理所需要的数据时出错。";
 		this.mErrors .addOneError(tError) ;
		return false;
	}
	return true;
	}
	public VData getResult()
	{
  	return this.mResult;
	}
}
