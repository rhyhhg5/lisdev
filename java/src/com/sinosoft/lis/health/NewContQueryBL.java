package com.sinosoft.lis.health;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

import java.io.File;
import com.sinosoft.lis.easyscan.ProposalDownloadBL;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 * <p>Title: 通讯管理查询</p>
 * <p>Description: 通讯管理查询清单</p>
 * <p>Copyright: Copyright (c) 2005 </p>
 * <p>Company: Sinosoft </p>
 * @author xiep   修改于09/02/25 xls下载改为txt下载，解决大数据量问题
 * @version 1.0
 * @date 2008-08-27
 */

public class NewContQueryBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    private FileWriter mFileWriter;
    private BufferedWriter mBufferedWriter;

    String ValiDate = "";
    String MngCom = "";
    String SysPath = ""; //存放生成文件的路径
    String FullPath = "";
    String tFlag = "";
    public NewContQueryBL() {
    }

    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cSysPath) {
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        SysPath = cSysPath;
        // 准备所有要打印的数据
        if (!downloadData()) {
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        mTransferData = (TransferData) cInputData.
                                     getObjectByObjectName("TransferData", 0);
        return true;
    }

    public String getResult() {
        return FullPath;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    private boolean downloadData() {
        String StartDate = (String)mTransferData.getValueByName("MakeDate1");
        String EndDate = (String)mTransferData.getValueByName("MakeDate2");
        String ManageCom = (String)mTransferData.getValueByName("ManageCom");
        String RiskCode = (String)mTransferData.getValueByName("RiskCode");

        String str = " and a.signdate between '"+StartDate+"' and '"+EndDate+"'";
        if(ManageCom!=null && !ManageCom.equals("")){
            str +=" and a.managecom like '"+ManageCom+"%'";
        }
        if(RiskCode!=null && !RiskCode.equals("")){
            str +=" and a.riskcode ='"+RiskCode+"'";
        }else{
            str +=" and a.riskcode in ('331201','331301','230501')";
        }

        String Path="";
        try {
            SysPath = SysPath + "healthmanage/" ;
            Path = "XinDan"+PubFun.getCurrentDate2() + "_" + PubFun.getCurrentTime2();
            mFileWriter = new FileWriter(SysPath+Path+".txt");
            mBufferedWriter = new BufferedWriter(mFileWriter);

            String[] strArr1Head = new String[17];
                strArr1Head[0] = "保单号码";
                strArr1Head[1] = "印刷号";
                strArr1Head[2] = "投保人姓名";
                strArr1Head[3] = "被保人姓名";
                strArr1Head[4] = "保额";
                strArr1Head[5] = "保费";
                strArr1Head[6] = "险种名称";
                strArr1Head[7] = "险种代码";
                strArr1Head[8] = "保障开始时间";
                strArr1Head[9] = "代理人姓名";
                strArr1Head[10] = "签单日期";
                strArr1Head[11] = "业务员代码";
                strArr1Head[12] = "业务员姓名";
                strArr1Head[13] = "营销机构名称";
                strArr1Head[14] = "保单机构";
                strArr1Head[15] = "投保人联系电话";
                strArr1Head[16] = "联系地址";
                String head = "";
            for(int m=0;m<strArr1Head.length;m++){
                        head += strArr1Head[m]+"|";
            }
            mBufferedWriter.write(head+"\r\n");
//            mBufferedWriter.newLine();
            mBufferedWriter.flush();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        String sql = " select a.contno,prtno,appntname,insuredname,amnt,prem,"
                     +" (select riskname from lmriskapp b where a.riskcode=b.riskcode),riskcode,"
                     +" cvalidate,signdate,a.agentcode,(select b.name from laagent b  where b.agentcode = a.agentcode),"
                     +" (select b.name from labranchgroup b where b.agentgroup=a.agentgroup),managecom"
                     +" ,b.mobile,b.postaladdress from lcpol a left outer join ("
                     +" select c.contno,d.mobile,d.postaladdress from lcaddress d ,lcappnt c where d.addressno=c.addressno"
                     +" and d.customerno = c.appntno) b on a.contno=b.contno where 1=1 "
                     + str
                     +" with ur";

            int start = 1;
             int nCount = 10000;
             while (true) {
                 SSRS tSSRS = new ExeSQL().execSQL(sql, start, nCount);
                 if (tSSRS.getMaxRow() <= 0) {
                     break;
                 }
                 if (tSSRS != null && tSSRS.getMaxRow() > 0) {
                  for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                        try{
                          String result="";
                         for(int m=1;m<=tSSRS.getMaxCol();m++){
                               result += tSSRS.GetText(i,m)+"|";
                         }
                          mBufferedWriter.write(result+"\r\n");
//                          mBufferedWriter.newLine();
                          mBufferedWriter.flush();

                      } catch (IOException ex) {
                         ex.printStackTrace();
                      }
                  }
              }
              start += nCount;
            }
            try {
             mBufferedWriter.close();
           } catch (IOException ex2) {
           }
            String[] FilePaths = new String[1];
            FilePaths[0] = SysPath+Path+".txt";
            String[] FileNames = new String[1];
            FileNames[0] = Path+".txt";
            String newPath = SysPath+Path+".zip";
            FullPath = Path+".zip";
            CreateZip(FilePaths,FileNames,newPath);
            try{
                File fd = new File(FilePaths[0]);
                fd.delete();
            }catch(Exception ex3){
                ex3.printStackTrace();
            }
          return true;
    }

    //生成压缩文件
    public boolean CreateZip(String[] tFilePaths, String[] tFileNames,
                             String tZipPath) {
        ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
        if (!tProposalDownloadBL.CreatZipFile(tFilePaths, tFileNames, tZipPath)) {
            System.out.println("生成压缩文件失败");
            CError.buildErr(this, "生成压缩文件失败");
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
    }
}
