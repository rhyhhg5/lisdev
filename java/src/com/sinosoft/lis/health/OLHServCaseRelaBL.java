package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class OLHServCaseRelaBL {
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors=new CErrors();
  private VData mResult = new VData();
  /** 往后面传输数据的容器 */
  private VData mInputData= new VData();
  private MMap map = new MMap();
  /** 全局数据 */
  private GlobalInput mGlobalInput =new GlobalInput() ;
  private TransferData mTransferData = new TransferData();
  private LHServCaseRelaSet mLHServCaseRelaSet = new LHServCaseRelaSet();
  /** 数据操作字符串 */
  private String mOperate;
  String GrpServPlanCode;

    public OLHServCaseRelaBL() {
    }
    public boolean submitData(VData cInputData,String cOperate)
  {
  //将操作数据拷贝到本类中
  this.mOperate =cOperate;
  this.mInputData = cInputData;
  //得到外部传入的数据,将数据备份到本类中
  if (!getInputData(cInputData))
  {        return false;}
   System.out.println("----------");
  //进行业务处理
  if (!dealData())
  {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "OLHServCaseRelaBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据处理失败OLHServCaseRelaBL-->dealData!";
        this.mErrors .addOneError(tError) ;
        return false;
  }

    //准备往后台的数据
  if (!prepareOutputData())
    return false;

    System.out.println("Start OLHServCaseRelaBL Submit...");
    PubSubmit tPubSubmit = new PubSubmit();
         if (!tPubSubmit.submitData(mInputData, mOperate))
           {
             // @@错误处理
             this.mErrors.copyAllErrors(tPubSubmit.mErrors);
             CError tError = new CError();
             tError.moduleName = "OLHServCaseRelaBL";
             tError.functionName = "submitData";
             tError.errorMessage = "数据提交失败!";

             this.mErrors.addOneError(tError);
             return false;
           }
           System.out.println("END OLHServCaseRelaBL Submit...");

  mInputData=null;
  return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
    System.out.println("******000*****" + mLHServCaseRelaSet.size());
  if (this.mOperate.equals("INSERT||MAIN"))
 {
     if (mLHServCaseRelaSet != null && mLHServCaseRelaSet.size() > 0) {
         System.out.println("***********" + mLHServCaseRelaSet.size());
         for (int i = 1; i <= mLHServCaseRelaSet.size(); i++) {
             String ServItemNo = mLHServCaseRelaSet.get(i).getServItemNo();
             LHServItemDB tLHServItemDB = new LHServItemDB();
             tLHServItemDB.setServItemNo(ServItemNo);
             LHServItemSet wLHServItemSet = tLHServItemDB.query();
             mLHServCaseRelaSet.get(i).setContNo(wLHServItemSet.get(1).getContNo());

             String ServPlanNo = wLHServItemSet.get(1).getGrpServPlanNo();
             LHGrpServPlanDB tLHGrpServPlanDB = new LHGrpServPlanDB();
             tLHGrpServPlanDB.setGrpServPlanNo(ServPlanNo);
             LHGrpServPlanSet wLHGrpServPlanSet = tLHGrpServPlanDB.query();
             mLHServCaseRelaSet.get(i).setServPlanNo(ServPlanNo);
             mLHServCaseRelaSet.get(i).setComID(wLHGrpServPlanSet.get(1).getComID());
             mLHServCaseRelaSet.get(i).setServPlanLevel(wLHGrpServPlanSet.get(1).getServPlanLevel());
             mLHServCaseRelaSet.get(i).setGrpCustomerNo(wLHGrpServPlanSet.get(1).getGrpCustomerNo());
             mLHServCaseRelaSet.get(i).setGrpName(wLHGrpServPlanSet.get(1).getGrpName());
             mLHServCaseRelaSet.get(i).setManageCom(mGlobalInput.ManageCom);
             mLHServCaseRelaSet.get(i).setOperator(mGlobalInput.Operator);
             mLHServCaseRelaSet.get(i).setMakeDate(PubFun.getCurrentDate());
             mLHServCaseRelaSet.get(i).setMakeTime(PubFun.getCurrentTime());
             mLHServCaseRelaSet.get(i).setModifyDate(PubFun.getCurrentDate());
             mLHServCaseRelaSet.get(i).setModifyTime(PubFun.getCurrentTime());
             map.put(mLHServCaseRelaSet,"INSERT");
         }
         }
     }
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean updateData()
{
 return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
return true;
}
/**
* 从输入数据中得到所有对象
*输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
*/
private boolean getInputData(VData cInputData)
{
      this.mLHServCaseRelaSet.set((LHServCaseRelaSet) cInputData.
                             getObjectByObjectName(
                                     "LHServCaseRelaSet", 0));
       this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
       return true;
}


private boolean prepareOutputData()
{
 try
      {
              this.mInputData.clear();
              this.mInputData.add(this.mLHServCaseRelaSet);
              mInputData.add(map);
           //   mResult.clear();
           //   mResult.add(this.mLHServItemSchema);
      }
      catch(Exception ex)
      {
              // @@错误处理
              CError tError =new CError();
              tError.moduleName="LHServCaseRelaBL";
              tError.functionName="prepareData";
              tError.errorMessage="在准备往后层处理所需要的数据时出错。";
              this.mErrors .addOneError(tError) ;
              return false;
      }
      return true;
  }
      public VData getResult()
      {
      return this.mResult;
      }

}
