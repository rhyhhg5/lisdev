/*
 * <p>ClassName: LHFeeChargeBL </p>
 * <p>Description: LHFeeChargeBL类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-09-06 17:14:38
 */
package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LHFeeChargeBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    private String tNo;
    /** 业务处理相关变量 */
    private TransferData mTransferData = new TransferData();
    private LHFeeChargeSet mLHFeeChargeSet = new LHFeeChargeSet();
    private LHTaskCustomerRelaSet mLHTaskCustomerRelaSet = new LHTaskCustomerRelaSet();


    public LHFeeChargeBL()
    {
        try {jbInit();}
        catch (Exception ex)
        {ex.printStackTrace();}
    }

    public static void main(String[] args)
    {
        GlobalInput tG = new GlobalInput();
        tG.Operator = "hm";
        tG.ManageCom = "86";

        TransferData mTransferData = new TransferData();

        mTransferData.setNameAndValue("ServTaskNo","000000002502");
        mTransferData.setNameAndValue("Fee","500");
        mTransferData.setNameAndValue("ExecState","1");


        LHFeeChargeSet tLHFeeChargeSet = new LHFeeChargeSet();		//服务预约管理表
        LHFeeChargeSchema tLHFeeChargeSchema = new LHFeeChargeSchema();

        tLHFeeChargeSchema.setServTaskNo("000000002502");	 //任务编号
        tLHFeeChargeSchema.setServCaseCode("20061211100008");	 //事件号码
        tLHFeeChargeSchema.setServTaskCode("01602");	 //任务代码
        tLHFeeChargeSchema.setServItemNo("00000000000000005386");	//项目号码
        tLHFeeChargeSchema.setCustomerNo("000851318");	//客户号
        tLHFeeChargeSchema.setContNo("00085131801");	//保单号
        tLHFeeChargeSchema.setFeeNormal("500");	//保单号
        tLHFeeChargeSchema.setMakeDate("2006-12-29");
        tLHFeeChargeSchema.setMakeTime("15:00:00");
        tLHFeeChargeSet.add(tLHFeeChargeSchema);

        VData tVData = new VData();

        tVData.add(tLHFeeChargeSet);
        tVData.add(tG);
        tVData.addElement(mTransferData);
        LHFeeChargeUI tLHFeeChargeUI   = new LHFeeChargeUI();
        tLHFeeChargeUI.submitData(tVData,"INSERT||MAIN");

    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
        //进行业务处理
        if (!dealData())
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHFeeChargeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LHFeeChargeBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
        {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN"))
        {
            this.submitquery();
        }
        else
        {
            PubSubmit tPubSubmit = new PubSubmit();
            System.out.println("Start LHFeeChargeBL Submit...");
            if (!tPubSubmit.submitData(mInputData, mOperate))
            {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);
                CError tError = new CError();
                tError.moduleName = "LHFeeChargeBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            System.out.println("End LHFeeChargeBL Submit...");
        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
        if (this.mOperate.equals("INSERT||MAIN"))
        {
            String arrTaskCustomer[][];
            SSRS SSRS_TaskCustomer = new SSRS();
            ExeSQL tTaskCustomer= new ExeSQL();
            String  sqlServTaskNo = "'"+mTransferData.getValueByName("ServTaskNo").toString().replaceAll(",","','")+"'";
            String sql= " select (select a.CustomerNo from LHServCaseRela a where a.ServCaseCode=v.ServCaseCode and a.ServItemNo=v.ServItemNo) ,"
                        +" v.TaskExecNo,v.ServTaskNo,v.ServCaseCode,v.ServTaskCode,v.ServItemNo,"
                        +" (select a.ContNo from LHServCaseRela a where a.ServCaseCode=v.ServCaseCode and a.ServItemNo=v.ServItemNo), "
                        +" (select A.TaskExecNo from LHTASKCUSTOMERRELA A where A.SERVTASKCODE = '01601' and A.SERVITEMNO = v.SERVITEMNO), "
                        +" value((select x.MoneySum from LHContServPrice x where x.contrano = p.ContraNo and x.contraitemno = p.servplanno),0),"
                        +" p.TaskExecNo,p.servplanno,p.hospitcode,p.contrano from LHTaskCustomerRela v,LHExecFeeBalance p  where v.ServTaskNo in ("+ sqlServTaskNo +")"
                        +" and p.TaskExecNo = (select A.TaskExecNo from LHTASKCUSTOMERRELA A where A.SERVTASKCODE = '01601' and A.SERVITEMNO = v.SERVITEMNO) "
                        ;
            SSRS_TaskCustomer = tTaskCustomer.execSQL(sql);
            arrTaskCustomer = SSRS_TaskCustomer.getAllData();
            mLHFeeChargeSet.clear();

            for (int j = 1; j <= arrTaskCustomer.length; j++)
            {
                LHFeeChargeSchema tLHFeeChargeSchema = new LHFeeChargeSchema();

                tLHFeeChargeSchema.setTaskExecNo(arrTaskCustomer[j-1][1]);
                tLHFeeChargeSchema.setServTaskNo(arrTaskCustomer[j-1][2]);
                tLHFeeChargeSchema.setServCaseCode(arrTaskCustomer[j-1][3]);
                tLHFeeChargeSchema.setServTaskCode(arrTaskCustomer[j-1][4]);
                tLHFeeChargeSchema.setServItemNo(arrTaskCustomer[j-1][5]);
                tLHFeeChargeSchema.setCustomerNo(arrTaskCustomer[j-1][0]);
                tLHFeeChargeSchema.setContNo(arrTaskCustomer[j-1][6]);
                tLHFeeChargeSchema.setTaskExecNo(arrTaskCustomer[j-1][7]);
                tLHFeeChargeSchema.setFeeNormal(arrTaskCustomer[j-1][8]);
                tLHFeeChargeSchema.setFeePay(arrTaskCustomer[j-1][8]);
                tLHFeeChargeSchema.setFeeTaskExecNo(arrTaskCustomer[j-1][9]);
                tLHFeeChargeSchema.setFeeType("1");

                tLHFeeChargeSchema.setOperator(mGlobalInput.Operator);
                tLHFeeChargeSchema.setMakeDate(PubFun.getCurrentDate());
                tLHFeeChargeSchema.setMakeTime(PubFun.getCurrentTime());
                tLHFeeChargeSchema.setModifyDate(PubFun.getCurrentDate());
                tLHFeeChargeSchema.setModifyTime(PubFun.getCurrentTime());
                tLHFeeChargeSchema.setManageCom(mGlobalInput.ManageCom);

                //核保体检费用处理
                if(new ExeSQL().getOneValue("select distinct DutyItemCode from LHContItem where ContraItemNo ='"+arrTaskCustomer[j-1][10]+"'").equals("HB0001"))
                {
                    String aProposalNo = "";
                    String aPrtSeq = "";
                    String aContNoCount = new ExeSQL().getOneValue("select count(contno) from LCCont where proposalcontno ='"+arrTaskCustomer[j-1][6]+"'");
                    if(aContNoCount.equals("1"))
                    {//contno是Proposalno
                        aProposalNo = arrTaskCustomer[j-1][6];
                    }
                    else
                    {//contno是contno
                        aProposalNo = new ExeSQL().getOneValue(" select distinct ProposalContno from lccont where  contno = '"+arrTaskCustomer[j-1][6]+"' ");
                    }
                    aPrtSeq = new ExeSQL().getOneValue(" select distinct prtseq from lcpenotice where ProposalContno = '"+aProposalNo+"' and customerno = '"+arrTaskCustomer[j-1][0]+"' ");
                    String sqlSum = " select value(sum(decimal((a.medicaitemprice),12,2)),0) from lcpenoticeitem p,ldtestpricemgt a "
                                        + " where p.ProposalContno = '"+aProposalNo+"' and  a.hospitcode = '"+arrTaskCustomer[j-1][11]
                                        +"' and a.ContraNo = '" +arrTaskCustomer[j-1][12]+"' and a.medicaitemcode = p.peitemcode   and p.PrtSeq = '"+aPrtSeq+"' ";

                    String HBFee = new ExeSQL().getOneValue(sqlSum);

                    System.out.println("----------------核保体检费用："+HBFee);
                    tLHFeeChargeSchema.setFeeNormal(HBFee);
                    tLHFeeChargeSchema.setFeePay(HBFee);
                }





                mLHFeeChargeSet.add(tLHFeeChargeSchema);
            }
            map.put(mLHFeeChargeSet, "INSERT");
            for (int j = 1; j <= arrTaskCustomer.length; j++)
            {
                map.put(" update LHTASKCUSTOMERRELA a set a.TaskExecState='"+mTransferData.getValueByName("ExecState").toString()+"',"
                        +" a.ManageCom='"+mGlobalInput.ManageCom+"', a.Operator='"+mGlobalInput.Operator+"', "
                        +" a.ModifyDate='"+PubFun.getCurrentDate()+"',a.ModifyTime='"+PubFun.getCurrentTime()  +"' "
                        +" where  a.TaskExecNo ='"+arrTaskCustomer[j-1][1]+"'","UPDATE");//将个人服务受理的服务任务换为生成的服务任务编号
            }
        }

//            if (mLHFeeChargeSet != null && mLHFeeChargeSet.size() > 0)
//            {
//                for (int j = 1; j <= mLHFeeChargeSet.size(); j++)
//                {
//                    mLHFeeChargeSet.get(j).setOperator(mGlobalInput.Operator);
//                    mLHFeeChargeSet.get(j).setMakeDate(PubFun.getCurrentDate());
//                    mLHFeeChargeSet.get(j).setMakeTime(PubFun.getCurrentTime());
//                    mLHFeeChargeSet.get(j).setModifyDate(PubFun.getCurrentDate());
//                    mLHFeeChargeSet.get(j).setModifyTime(PubFun.getCurrentTime());
//                    mLHFeeChargeSet.get(j).setManageCom(mGlobalInput.ManageCom);
//                }
//                map.put(mLHFeeChargeSet, "INSERT");
//
//                for (int j = 1; j <= mLHTaskCustomerRelaSet.size(); j++)
//                {
//                    if (mLHTaskCustomerRelaSet.get(j).getTaskExecNo() == null ||
//                        mLHTaskCustomerRelaSet.get(j).getTaskExecNo().equals(""))
//                    {
//                        mLHTaskCustomerRelaSet.get(j).setTaskExecNo(PubFun1.
//                                CreateMaxNo("TaskExecNo", 12));
//                    }
//
//                    mLHTaskCustomerRelaSet.get(j).setOperator(mGlobalInput.
//                            Operator);
//                    mLHTaskCustomerRelaSet.get(j).setMakeDate(PubFun.
//                            getCurrentDate());
//                    mLHTaskCustomerRelaSet.get(j).setMakeTime(PubFun.
//                            getCurrentTime());
//                    mLHTaskCustomerRelaSet.get(j).setModifyDate(PubFun.
//                            getCurrentDate());
//                    mLHTaskCustomerRelaSet.get(j).setModifyTime(PubFun.
//                            getCurrentTime());
//                    mLHTaskCustomerRelaSet.get(j).setManageCom(mGlobalInput.
//                            ManageCom);
//
//                    map.put(
//                            " delete from  LHTaskCustomerRela where ServTaskNo='" +
//                            mLHTaskCustomerRelaSet.get(j).getServTaskNo() +
//                            "' and ServCaseCode='" +
//                            mLHTaskCustomerRelaSet.get(j).getServCaseCode() +
//                            "'", "DELETE");
//                }
//                map.put(mLHTaskCustomerRelaSet, "INSERT");
//            }
//    }

        if (this.mOperate.equals("UPDATE||MAIN"))
        {
            for (int j = 1; j <= mLHFeeChargeSet.size(); j++)
            {
                mLHFeeChargeSet.get(j).setOperator(mGlobalInput.Operator);
//                mLHFeeChargeSet.get(j).setMakeDate(PubFun.getCurrentDate());
//                mLHFeeChargeSet.get(j).setMakeTime(PubFun.getCurrentTime());
                mLHFeeChargeSet.get(j).setModifyDate(PubFun.getCurrentDate());
                mLHFeeChargeSet.get(j).setModifyTime(PubFun.getCurrentTime());
                mLHFeeChargeSet.get(j).setManageCom(mGlobalInput.ManageCom);
                map.put(" delete from  LHFeeCharge where TaskExecNo='" +
                        mLHFeeChargeSet.get(j).getTaskExecNo() + "'","DELETE");

                map.put(" update LHTASKCUSTOMERRELA a set  a.TaskExecState='"+mTransferData.getValueByName("ExecState").toString()+"',"
                   +" a.ManageCom='"+mGlobalInput.ManageCom+"', a.Operator='"+mGlobalInput.Operator+"', "
                   +" a.ModifyDate='"+PubFun.getCurrentDate()+"',a.ModifyTime='"+PubFun.getCurrentTime()  +"' "
                   +" where  a.TaskExecNo ='"+mLHFeeChargeSet.get(j).getTaskExecNo()+"'","UPDATE");//将个人服务受理的服务任务换为生成的服务任务编号
            }
            map.put(mLHFeeChargeSet, "INSERT");

//            for (int j = 1; j <= mLHTaskCustomerRelaSet.size(); j++)
//            {
//                if (mLHTaskCustomerRelaSet.get(j).getTaskExecNo() == null ||
//                    mLHTaskCustomerRelaSet.get(j).getTaskExecNo().equals(""))
//                {
//                    mLHTaskCustomerRelaSet.get(j).setTaskExecNo(PubFun1.
//                            CreateMaxNo("TaskExecNo", 12));
//                }
//                mLHTaskCustomerRelaSet.get(j).setOperator(mGlobalInput.Operator);
//                mLHTaskCustomerRelaSet.get(j).setMakeDate(PubFun.getCurrentDate());
//                mLHTaskCustomerRelaSet.get(j).setMakeTime(PubFun.getCurrentTime());
//                mLHTaskCustomerRelaSet.get(j).setModifyDate(PubFun.
//                        getCurrentDate());
//                mLHTaskCustomerRelaSet.get(j).setModifyTime(PubFun.
//                        getCurrentTime());
//                mLHTaskCustomerRelaSet.get(j).setManageCom(mGlobalInput.
//                        ManageCom);
//
//                map.put(" delete from  LHTaskCustomerRela where ServTaskNo='" +
//                        mLHTaskCustomerRelaSet.get(j).getServTaskNo() +
//                        "' and ServCaseCode='" +
//                        mLHTaskCustomerRelaSet.get(j).getServCaseCode() +
//                        "'", "DELETE");
//            }
//            map.put(mLHTaskCustomerRelaSet, "INSERT");
        }

        if (this.mOperate.equals("DELETE||MAIN"))
        {
            //map.put(mLHFeeChargeet, "DELETE");
        }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean updateData()
    {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData()
    {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
        this.mLHFeeChargeSet.set((LHFeeChargeSet) cInputData.getObjectByObjectName("LHFeeChargeSet", 0));
//        this.mLHTaskCustomerRelaSet.set((LHTaskCustomerRelaSet) cInputData.getObjectByObjectName("LHTaskCustomerRelaSet", 0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
         mTransferData = ((TransferData) cInputData.getObjectByObjectName("TransferData", 0));
        if (mTransferData == null)
        {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "LHFeeChargeBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "TransferData为空!";
                this.mErrors.addOneError(tError);
                return false;
         }
         return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        LHFeeChargeDB tLHFeeChargeDB = new LHFeeChargeDB();
        //如果有需要处理的错误，则返回
        if (tLHFeeChargeDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHFeeChargeDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHFeeChargeBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    private boolean prepareOutputData()
    {
        try
        {
            this.mInputData.clear();
            this.mInputData.add(this.mLHFeeChargeSet);
//            this.mInputData.add(this.mLHTaskCustomerRelaSet);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLHFeeChargeSet);
//            mResult.add(this.mLHTaskCustomerRelaSet);
        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHFeeChargeBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public VData getResult()
    {
        return this.mResult;
    }

    private void jbInit() throws Exception
    {
    }
}
