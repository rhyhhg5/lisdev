/*
 * <p>ClassName: LHQuesKineBL </p>
 * <p>Description: LHQuesKineBL类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-9-22 16:08:38
 */
package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class LHQuesKineBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
private MMap map= new MMap();

/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
private String tNo;
/** 业务处理相关变量 */
private LHQuesImportMainSchema mLHQuesImportMainSchema=new LHQuesImportMainSchema();
private LHQuesImportDetailSet mLHQuesImportDetailSet=new LHQuesImportDetailSet();
private TransferData mTransferData = new TransferData();

public LHQuesKineBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
public static void main(String[] args) {
}
/**
* 传输数据的公共方法
        * @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "LHQuesKineBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败LHQuesKineBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
      PubSubmit tPubSubmit = new PubSubmit();
      System.out.println("Start LHQuesKineBL Submit...");
      if (!tPubSubmit.submitData(mInputData, mOperate))
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);

        CError tError = new CError();
        tError.moduleName = "LHQuesKineBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据提交失败!";

        this.mErrors.addOneError(tError);
        return false;
      }

      System.out.println("End LHQuesKineBL Submit...");
      //如果有需要处理的错误，则返回
    }
    mInputData=null;
    return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
    if (this.mOperate.equals("INSERT||MAIN"))
    {
        mLHQuesImportMainSchema.setCusRegistNo(mLHQuesImportMainSchema.getCustomerNo()+
                                               mLHQuesImportMainSchema.getQuesType()+
                                               PubFun1.CreateMaxNo("CusRegistNo", 9));
        mLHQuesImportMainSchema.setSubmitTime(PubFun.getCurrentTime());
        mLHQuesImportMainSchema.setManageCom(mGlobalInput.ManageCom);
        mLHQuesImportMainSchema.setOperator(mGlobalInput.Operator);
        mLHQuesImportMainSchema.setMakeDate(PubFun.getCurrentDate());
        mLHQuesImportMainSchema.setMakeTime(PubFun.getCurrentTime());
        mLHQuesImportMainSchema.setModifyDate(PubFun.getCurrentDate());
        mLHQuesImportMainSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(mLHQuesImportMainSchema, "INSERT");
        for (int j = 1; j <= mLHQuesImportDetailSet.size(); j++)
        {
                mLHQuesImportDetailSet.get(j).setCusRegistNo(mLHQuesImportMainSchema.getCusRegistNo());
                mLHQuesImportDetailSet.get(j).setOperator(mGlobalInput.Operator);
                mLHQuesImportDetailSet.get(j).setMakeDate(PubFun.getCurrentDate());
                mLHQuesImportDetailSet.get(j).setMakeTime(PubFun.getCurrentTime());
                mLHQuesImportDetailSet.get(j).setModifyDate(PubFun.getCurrentDate());
                mLHQuesImportDetailSet.get(j).setModifyTime(PubFun.getCurrentTime());
                mLHQuesImportDetailSet.get(j).setManageCom(mGlobalInput.ManageCom);
        }
        map.put(mLHQuesImportDetailSet, "INSERT");
    }
    if (this.mOperate.equals("UPDATE||MAIN"))
    {
        mLHQuesImportMainSchema.setOperator(mGlobalInput.Operator);
        mLHQuesImportMainSchema.setSubmitTime(PubFun.getCurrentTime());
        mLHQuesImportMainSchema.setModifyDate(PubFun.getCurrentDate());
        mLHQuesImportMainSchema.setModifyTime(PubFun.getCurrentTime());
        mLHQuesImportMainSchema.setManageCom(mGlobalInput.ManageCom);
        map.put(mLHQuesImportMainSchema, "DELETE&INSERT");
        for (int i = 1; i <= mLHQuesImportDetailSet.size(); i++)
        {
                mLHQuesImportDetailSet.get(i).setCusRegistNo(mLHQuesImportMainSchema.getCusRegistNo());
                mLHQuesImportDetailSet.get(i).setOperator(mGlobalInput.Operator);
                mLHQuesImportDetailSet.get(i).setModifyDate(PubFun.getCurrentDate());
                mLHQuesImportDetailSet.get(i).setModifyTime(PubFun.getCurrentTime());
                mLHQuesImportDetailSet.get(i).setManageCom(mGlobalInput.ManageCom);
        }
        map.put(" delete from  LHQuesImportDetail where CusRegistNo='"+mLHQuesImportMainSchema.getCusRegistNo()+"'","DELETE");
        map.put(mLHQuesImportDetailSet, "INSERT");

                String Letterresult = mLHQuesImportDetailSet.get(172).getLetterResult();//性别问题的编号
                if(Letterresult.equals("1"))//性别为男
                {//把性别为女的记录状态改为N，此下为所有编号为女性问题的编号
                       map.put(" update LHQUESIMPORTDETAIL a set  a.Quesstatus='N', a.ManageCom='"+mGlobalInput.ManageCom
                               +"', a.Operator='"+mGlobalInput.Operator+"', a.ModifyDate='"+PubFun.getCurrentDate()
                               +"',a.ModifyTime='"+PubFun.getCurrentTime()  +
                               "' where  a.StandCode in ('03030001','03030002','03030003','03030004','03030005','03030006','03030007','03030008','01010001','01010002','01010004','06010001','03020001','03020002','03010002','03010003') and "
                               +" a.CusRegistNo='"+mLHQuesImportMainSchema.getCusRegistNo()+"' ","UPDATE");
                }
                if(Letterresult.equals("2"))//性别为女
                {//此下所以编号为男性问题的编号
                       map.put(" update LHQUESIMPORTDETAIL a set  a.Quesstatus='N', a.ManageCom='"+mGlobalInput.ManageCom
                               +"', a.Operator='"+mGlobalInput.Operator+"', a.ModifyDate='"+PubFun.getCurrentDate()
                               +"',a.ModifyTime='"+PubFun.getCurrentTime()  +
                               "' where  a.StandCode in ('03020003','01020001','01020002','01020003') and "
                               +" a.CusRegistNo='"+mLHQuesImportMainSchema.getCusRegistNo()+"' ","UPDATE");
                       String Time = mLHQuesImportDetailSet.get(36).getLetterResult();////您是否服用雌激素类的药物?
                       if(Time.equals("2"))//您是否服用雌激素类的药物?如未服用雌激素时间可不填
                       {
                           map.put(" update LHQUESIMPORTDETAIL a set  a.Quesstatus='N', a.ManageCom='"+mGlobalInput.ManageCom
                                   +"', a.Operator='"+mGlobalInput.Operator+"', a.ModifyDate='"+PubFun.getCurrentDate()
                                   +"',a.ModifyTime='"+PubFun.getCurrentTime() +"' "
                                   +"where  a.StandCode in ('03010003') and "
                                   +" a.CusRegistNo='"+mLHQuesImportMainSchema.getCusRegistNo()+"' ","UPDATE");
                       }
                }
                String[] High = new String[2];
                High[0] = mLHQuesImportDetailSet.get(4).getLetterResult();//高血压的问题的结果
                High[1] = mLHQuesImportDetailSet.get(8).getLetterResult();//高血压性心脏病的结果
                if(High[0]==null&&High[1]==null)
                { //服高血压药物史吗 药物名称
                    map.put(" update LHQUESIMPORTDETAIL a set  a.Quesstatus='N', a.ManageCom='"+mGlobalInput.ManageCom
                           +"', a.Operator='"+mGlobalInput.Operator+"', a.ModifyDate='"+PubFun.getCurrentDate()
                           +"',a.ModifyTime='"+PubFun.getCurrentTime() +"' where  a.StandCode in ('03010001','03010008') and "
                           +" a.CusRegistNo='"+mLHQuesImportMainSchema.getCusRegistNo()+"' ","UPDATE");
                }
                else
                {
                    String HighHistory = mLHQuesImportDetailSet.get(19).getLetterResult();////你有服高血压药物史吗??
                    if(HighHistory.equals("2"))//无服高血压药物史吗??
                    {
                        map.put(" update LHQUESIMPORTDETAIL a set  a.Quesstatus='N', a.ManageCom='"+mGlobalInput.ManageCom
                           +"', a.Operator='"+mGlobalInput.Operator+"', a.ModifyDate='"+PubFun.getCurrentDate()
                           +"',a.ModifyTime='"+PubFun.getCurrentTime() +"' where  a.StandCode in ('03010008') and "
                           +" a.CusRegistNo='"+mLHQuesImportMainSchema.getCusRegistNo()+"' ","UPDATE");//药物名称
                    }
                }
                String[] Diablse = new String[3];
                Diablse[0] = mLHQuesImportDetailSet.get(42).getLetterResult();//糖尿病
                Diablse[1] = mLHQuesImportDetailSet.get(46).getLetterResult();//冠心病
                Diablse[2] = mLHQuesImportDetailSet.get(48).getLetterResult();//中 风
                if(!Diablse[0].equals("1"))//糖尿病 家族史 父母、兄弟姐妹、子女 不为“是”时
                {//是否有人在40 岁以前?
                    map.put(" update LHQUESIMPORTDETAIL a set  a.Quesstatus='N', a.ManageCom='"+mGlobalInput.ManageCom
                       +"', a.Operator='"+mGlobalInput.Operator+"', a.ModifyDate='"+PubFun.getCurrentDate()
                       +"',a.ModifyTime='"+PubFun.getCurrentTime() +"' where  a.StandCode in ('01030001') and "
                       +" a.CusRegistNo='"+mLHQuesImportMainSchema.getCusRegistNo()+"' ","UPDATE");
                }
                if(!Diablse[1].equals("1"))//冠心病不为“是”时
                {//如是，是否有人在50 岁以前?
                    map.put(" update LHQUESIMPORTDETAIL a set  a.Quesstatus='N', a.ManageCom='"+mGlobalInput.ManageCom
                       +"', a.Operator='"+mGlobalInput.Operator+"', a.ModifyDate='"+PubFun.getCurrentDate()
                       +"',a.ModifyTime='"+PubFun.getCurrentTime() +"' where  a.StandCode in ('01040002') and "
                       +" a.CusRegistNo='"+mLHQuesImportMainSchema.getCusRegistNo()+"' ","UPDATE");
                }
                if(!Diablse[2].equals("1"))//中 风不为“是”时
                {//如是，是否有人在60 岁以前?
                    map.put(" update LHQUESIMPORTDETAIL a set  a.Quesstatus='N', a.ManageCom='"+mGlobalInput.ManageCom
                            +"', a.Operator='"+mGlobalInput.Operator+"', a.ModifyDate='"+PubFun.getCurrentDate()
                            +"',a.ModifyTime='"+PubFun.getCurrentTime() +"' where  a.StandCode in ('01050002') and "
                            +" a.CusRegistNo='"+mLHQuesImportMainSchema.getCusRegistNo()+"' ","UPDATE");
                }
                String SmokeIn = mLHQuesImportDetailSet.get(125).getLetterResult();//吸烟情况你现在吸烟吗结果
                if(SmokeIn.equals("1"))//吸烟的情况
                {//吸烟情的6-10可不录
                    map.put(" update LHQUESIMPORTDETAIL a set  a.Quesstatus='N', a.ManageCom='"+mGlobalInput.ManageCom
                          +"', a.Operator='"+mGlobalInput.Operator+"', a.ModifyDate='"+PubFun.getCurrentDate()
                          +"',a.ModifyTime='"+PubFun.getCurrentTime() +"' "
                          +"where  a.StandCode in ('02020008','02020009','02020010','02020011','02020012') and "
                          +" a.CusRegistNo='"+mLHQuesImportMainSchema.getCusRegistNo()+"' ","UPDATE");
                }
                if(SmokeIn.equals("2"))//不吸烟的情况
                {//吸烟情的2-8可不录
                    map.put(" update LHQUESIMPORTDETAIL a set  a.Quesstatus='N', a.ManageCom='"+mGlobalInput.ManageCom
                           +"', a.Operator='"+mGlobalInput.Operator+"', a.ModifyDate='"+PubFun.getCurrentDate()
                           +"',a.ModifyTime='"+PubFun.getCurrentTime() +"' "
                           +"where  a.StandCode in ('02020002','02020006','02020007','02020003','02020008','02020009','02020010') and "
                           +" a.CusRegistNo='"+mLHQuesImportMainSchema.getCusRegistNo()+"' ","UPDATE");
                      String ClaSmoke = mLHQuesImportDetailSet.get(133).getLetterResult();//和您一起工作的同事或一起生活的家人中是否有人吸烟?
                      if(ClaSmoke.equals("2"))//和您一起工作的同事或一起生活的家人中否有人吸烟?
                      {
                          map.put(" update LHQUESIMPORTDETAIL a set  a.Quesstatus='N', a.ManageCom='"+mGlobalInput.ManageCom
                                  +"', a.Operator='"+mGlobalInput.Operator+"', a.ModifyDate='"+PubFun.getCurrentDate()
                                  +"',a.ModifyTime='"+PubFun.getCurrentTime() +"' "
                                  +"where  a.StandCode in ('02020012') and "
                                  +" a.CusRegistNo='"+mLHQuesImportMainSchema.getCusRegistNo()+"' ","UPDATE");
                      }
                }
                if(SmokeIn.equals("3"))//戒烟的情况
                {//吸烟情的2-3可不录
                      map.put(" update LHQUESIMPORTDETAIL a set  a.Quesstatus='N', a.ManageCom='"+mGlobalInput.ManageCom
                             +"', a.Operator='"+mGlobalInput.Operator+"', a.ModifyDate='"+PubFun.getCurrentDate()
                             +"',a.ModifyTime='"+PubFun.getCurrentTime() +"' "
                             +"where  a.StandCode in ('02020002','02020006') and "
                             +" a.CusRegistNo='"+mLHQuesImportMainSchema.getCusRegistNo()+"' ","UPDATE");
                     String ClaSmoke = mLHQuesImportDetailSet.get(133).getLetterResult();//和您一起工作的同事或一起生活的家人中是否有人吸烟?
                      if(ClaSmoke.equals("2"))//和您一起工作的同事或一起生活的家人中否有人吸烟?
                      {
                          map.put(" update LHQUESIMPORTDETAIL a set  a.Quesstatus='N', a.ManageCom='"+mGlobalInput.ManageCom
                                  +"', a.Operator='"+mGlobalInput.Operator+"', a.ModifyDate='"+PubFun.getCurrentDate()
                                  +"',a.ModifyTime='"+PubFun.getCurrentTime() +"' "
                                  +"where  a.StandCode in ('02020012') and "
                                  +" a.CusRegistNo='"+mLHQuesImportMainSchema.getCusRegistNo()+"' ","UPDATE");
                      }

                }
                String ExiseIN = mLHQuesImportDetailSet.get(139).getLetterResult();//体育锻炼 您参加体育锻炼吗?
                if(ExiseIN.equals("2"))//不参加体育锻炼的情况
                {//平均每周锻炼几次? 锻炼方式是什么? 锻炼时间是多少分钟? 不用录入
                    map.put(" update LHQUESIMPORTDETAIL a set  a.Quesstatus='N', a.ManageCom='"+mGlobalInput.ManageCom
                             +"', a.Operator='"+mGlobalInput.Operator+"', a.ModifyDate='"+PubFun.getCurrentDate()
                             +"',a.ModifyTime='"+PubFun.getCurrentTime() +"' "
                             +"where  a.StandCode in ('02040006','02040007','02040008') and "
                             +" a.CusRegistNo='"+mLHQuesImportMainSchema.getCusRegistNo()+"' ","UPDATE");
                }
                String WhethAchieve = (String) mTransferData.getValueByName("statusI");//提交后的判断是否选择体检信息
                if(WhethAchieve.equals("3"))//提交后的判断没有选择体检信息时
                {
                    map.put(" update LHQUESIMPORTDETAIL a set  a.Quesstatus='N', a.ManageCom='"+mGlobalInput.ManageCom
                             +"', a.Operator='"+mGlobalInput.Operator+"', a.ModifyDate='"+PubFun.getCurrentDate()
                             +"',a.ModifyTime='"+PubFun.getCurrentTime() +"' "
                             +"where  a.StandCode in ('05010001','05010002','05010003','05010004','05010005','05010006','05010007','05010015','05010008','05010016','05010009','05010017','05010010','05010018','05010011','05010019','05010012','05010020','05010013','05010021','05010014') and "
                             +" a.CusRegistNo='"+mLHQuesImportMainSchema.getCusRegistNo()+"' ","UPDATE");
                }
    }

    if (this.mOperate.equals("DELETE||MAIN"))
    {
        map.put(mLHQuesImportMainSchema, "DELETE");
        map.put(mLHQuesImportDetailSet, "DELETE");
    }
    return true;
}
/**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
 */
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
         this.mLHQuesImportMainSchema.setSchema((LHQuesImportMainSchema)cInputData.getObjectByObjectName("LHQuesImportMainSchema",0));
         this.mLHQuesImportDetailSet.set((LHQuesImportDetailSet)cInputData.getObjectByObjectName("LHQuesImportDetailSet",0));
         this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         mTransferData = ((TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0));
         if (mTransferData == null) {
           System.out.println("没有得到足够的信息！");
            return false;
        }

         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
    this.mResult.clear();
    LHQuesImportMainDB tLHQuesImportMainDB=new LHQuesImportMainDB();
//    tLHContraAssoSettingDB.set(this.mLHContraAssoSettingSet);
    //如果有需要处理的错误，则返回
    if (tLHQuesImportMainDB.mErrors.needDealError())
    {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHQuesImportMainDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHQuesKineBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
    }
    mInputData=null;
    return true;
}
 private boolean prepareOutputData()
 {
     try
     {
         this.mInputData.clear();
         this.mInputData.add(this.mLHQuesImportMainSchema);
         this.mInputData.add(this.mLHQuesImportDetailSet);
         mInputData.add(map);
         mResult.clear();
         mResult.add(this.mLHQuesImportMainSchema);
         mResult.add(this.mLHQuesImportDetailSet);
     } catch (Exception ex)
     {
         // @@错误处理
         CError tError = new CError();
         tError.moduleName = "LHQuesKineBL";
         tError.functionName = "prepareData";
         tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
         this.mErrors.addOneError(tError);
         return false;
     }
     return true;
 }
 public VData getResult() {
     return this.mResult;
 }
    private void jbInit() throws Exception {
    }
}
