package com.sinosoft.lis.health;

import java.util.*;
import java.io.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.LHTaskCustomerRelaSchema;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import com.sinosoft.lis.easyscan.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: </p>
 * @author guoly
 * @version 1.0
 */

public class LHServiceManageLookExportXls
{
    public LHServiceManageLookExportXls() {}
    String FullPath = "";
    String  []FileSheetNum;
    String ZipPathName="";
    String FilePath="";
    public static void main(String[] args)
    {
    }

    public boolean CreateZip(String[] tFilePaths, String[] tFileNames, String tZipPath)
    {
        ProposalDownloadBL tProposalDownloadBL = new ProposalDownloadBL();
        if (!tProposalDownloadBL.CreatZipFile(tFilePaths, tFileNames, tZipPath))
        {
            System.out.println("生成压缩文件失败");
            CError.buildErr(this,"生成压缩文件失败");
            return false;
        }
        return true;
    }

    public boolean DownloadSubmit(String tTaskNo,String ZipPath)
    {
        try {
          ZipPath = ZipPath + "healthmanage/";
          String Path;
          Path = PubFun.getCurrentDate() + PubFun.getCurrentTime() + ".xls";
          Path = Path.replaceAll(":", "-");
          WriteToExcel t = new WriteToExcel(Path);
          String[] FilePaths = new String[1];
          FilePaths[0] = ZipPath + Path;
          String[] FileNames = new String[1];
          FileNames[0] = "LHExcuteState.xls"; //PubFun.getCurrentDate()+PubFun.getCurrentTime();
          String[][] mToExcel = new String[1][11];
          mToExcel[0][0] = "服务事件编号";
          mToExcel[0][1] = "服务任务编号";
          mToExcel[0][2] = "任务状态";
          mToExcel[0][3] = "任务实施号码";
          mToExcel[0][4] = "服务事件名称";
          mToExcel[0][5] = "服务任务名称";
          mToExcel[0][6] = "客户号";
          mToExcel[0][7] = "客户姓名";
          mToExcel[0][8] = "保单号";
          mToExcel[0][9] = "服务项目名称";
          mToExcel[0][10] = "序号";
          t.createExcelFile();
          String[] sheetName = {"ExcuteState"};
          t.addSheet(sheetName);
          t.setData(0, mToExcel);
          String sqlx = "select m.ServCaseCode,m.ServTaskNo,"
                      +" (select k.codename from ldcode k where k.codetype='taskexecstuas' and k.code=m.TaskExecState),"
                      +" m.TaskExecNo, "
                      +"(select d.ServCaseName  from LHServCaseDef d where d.ServCaseCode=m.ServCaseCode),"
                      +" (select c.ServTaskName from LHServTaskDef c where c.ServTaskCode=m.ServTaskCode),"
                      +" b.CustomerNo,b.CustomerName,b.ContNo,"
                      +" (select m.ServItemName from LHHealthServItem m where m.ServItemCode=b.ServItemCode),"
                      +" (select m.ServItemType from LHServItem m where m.ServItemNo=b.ServItemNo) "
                      +" from LHServCaseRela b , LHTaskCustomerRela m"
                      +" where  m.ServCaseCode=b.ServCaseCode and m.ServItemNo=b.ServItemNo"
                      +" and m.ServTaskNo in ("+tTaskNo+") order by TaskExecNo"
                      ;
          System.out.println(sqlx);
          t.setData(0,sqlx);
          System.out.println("ZipPath  : " + ZipPath);
          t.write(ZipPath);
          String NewPath;
          NewPath = PubFun.getCurrentDate() + PubFun.getCurrentTime() +
                    ".zip";
          NewPath = NewPath.replaceAll(":", "-");
          FullPath = NewPath;
          NewPath = ZipPath + NewPath;
          CreateZip(FilePaths, FileNames, NewPath);
          File fd = new File(FilePaths[0]);
          fd.delete();
          System.out.println("FullPath!  : " + NewPath);
        /*  File Zipd = new File(NewPath);
          Zipd.delete();
         */

      } catch (Exception ex) {
          ex.toString();
          ex.printStackTrace();
      }
      return true;

    }

    public String getResult()
    {
        System.out.println("FullPath@  : " + FullPath);
        return FullPath;
    }

}
