/*
 * <p>ClassName: LHTaskServItemRelaBL </p>
 * <p>Description: LHTaskServItemRelaBL类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-08-02 16:08:38
 */
package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class LHTaskServItemRelaBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
private MMap map= new MMap();

/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
private String tNo;
/** 业务处理相关变量 */
//private LHServTaskDefSet mLHServTaskDefSet=new LHServTaskDefSet();
private LHServTaskDefSchema mLHServTaskDefSchema=new LHServTaskDefSchema();
private LHTaskServItemRelaSet mLHTaskServItemRelaSet=new LHTaskServItemRelaSet();


public LHTaskServItemRelaBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
public static void main(String[] args) {
}
/**
* 传输数据的公共方法
        * @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "LHTaskServItemRelaBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败LHTaskServItemRelaBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
      PubSubmit tPubSubmit = new PubSubmit();
      System.out.println("Start LHTaskServItemRelaBL Submit...");
      //LHTaskServItemRelaBLS tLHTaskServItemRelaBLS=new LHTaskServItemRelaBLS();
      //tLHTaskServItemRelaBLS.submitData(mInputData,mOperate);
      if (!tPubSubmit.submitData(mInputData, mOperate))
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);

        CError tError = new CError();
        tError.moduleName = "LHTaskServItemRelaBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据提交失败!";

        this.mErrors.addOneError(tError);
        return false;
      }

      System.out.println("End LHTaskServItemRelaBL Submit...");
      //如果有需要处理的错误，则返回
      //if (tLHTaskServItemRelaBLS.mErrors.needDealError())
      //{
        // @@错误处理
        //this.mErrors.copyAllErrors(tLHTaskServItemRelaBLS.mErrors);
       // CError tError = new CError();
        //tError.moduleName = "LHTaskServItemRelaBL";
        //tError.functionName = "submitDat";
        //tError.errorMessage ="数据提交失败!";
        //this.mErrors .addOneError(tError) ;
        //return false;
      //}
    }
    mInputData=null;
    return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{
    if (this.mOperate.equals("INSERT||MAIN"))
    {
        mLHServTaskDefSchema.setManageCom(mGlobalInput.ManageCom);
        mLHServTaskDefSchema.setOperator(mGlobalInput.Operator);
        mLHServTaskDefSchema.setMakeDate(PubFun.getCurrentDate());
        mLHServTaskDefSchema.setMakeTime(PubFun.getCurrentTime());
        mLHServTaskDefSchema.setModifyDate(PubFun.getCurrentDate());
        mLHServTaskDefSchema.setModifyTime(PubFun.getCurrentTime());
        map.put(mLHServTaskDefSchema, "INSERT");

        for (int j = 1; j <= mLHTaskServItemRelaSet.size(); j++)
        {
            mLHTaskServItemRelaSet.get(j).setSerialNo(PubFun1.CreateMaxNo(
                    "LHTASKITEMRELA", 8));
        }
        map.put(mLHTaskServItemRelaSet, "INSERT");
    }

    if (this.mOperate.equals("UPDATE||MAIN"))
    {
        LHServTaskDefDB tLHServTaskDefDB = new LHServTaskDefDB();
        tLHServTaskDefDB.setServTaskCode(mLHServTaskDefSchema.getServTaskCode());

        mLHServTaskDefSchema.setModuleURL(tLHServTaskDefDB.query().get(1).getModuleURL());
        mLHServTaskDefSchema.setOperator(mGlobalInput.Operator);
        mLHServTaskDefSchema.setMakeDate(PubFun.getCurrentDate());
        mLHServTaskDefSchema.setMakeTime(PubFun.getCurrentTime());
        mLHServTaskDefSchema.setModifyDate(PubFun.getCurrentDate());
        mLHServTaskDefSchema.setModifyTime(PubFun.getCurrentTime());
        mLHServTaskDefSchema.setManageCom(mGlobalInput.ManageCom);
        map.put(mLHServTaskDefSchema, "DELETE&INSERT");

        for (int j= 1; j<= mLHTaskServItemRelaSet.size(); j++)
        {
            if (mLHTaskServItemRelaSet.get(j).getSerialNo() == null ||
                mLHTaskServItemRelaSet.get(j).getSerialNo().equals(""))
            {
                mLHTaskServItemRelaSet.get(j).setSerialNo(PubFun1.
                        CreateMaxNo("LHTASKITEMRELA", 8));
            }
        }
        map.put("delete from LHTaskServItemRela where ServTaskCode='" +
                    mLHServTaskDefSchema.getServTaskCode() + "'","DELETE");
        map.put(mLHTaskServItemRelaSet, "INSERT");
    }

    if (this.mOperate.equals("DELETE||MAIN"))
    {
        map.put(mLHServTaskDefSchema, "DELETE");
        map.put(mLHTaskServItemRelaSet, "DELETE");
    }
    return true;
}
/**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
 */
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
         this.mLHServTaskDefSchema.setSchema((LHServTaskDefSchema)cInputData.getObjectByObjectName("LHServTaskDefSchema",0));
         this.mLHTaskServItemRelaSet.set((LHTaskServItemRelaSet)cInputData.getObjectByObjectName("LHTaskServItemRelaSet",0));
         this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
    this.mResult.clear();
    LHServTaskDefDB tLHServTaskDefDB=new LHServTaskDefDB();
//    tLHContraAssoSettingDB.set(this.mLHContraAssoSettingSet);
    //如果有需要处理的错误，则返回
    if (tLHServTaskDefDB.mErrors.needDealError())
    {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHServTaskDefDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHTaskServItemRelaBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
    }
    mInputData=null;
    return true;
}
 private boolean prepareOutputData()
 {
     try
     {
         this.mInputData.clear();
         this.mInputData.add(this.mLHServTaskDefSchema);
         this.mInputData.add(this.mLHTaskServItemRelaSet);
         mInputData.add(map);
         mResult.clear();
         mResult.add(this.mLHServTaskDefSchema);
         mResult.add(this.mLHTaskServItemRelaSet);
     } catch (Exception ex)
     {
         // @@错误处理
         CError tError = new CError();
         tError.moduleName = "LHTaskServItemRelaBL";
         tError.functionName = "prepareData";
         tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
         this.mErrors.addOneError(tError);
         return false;
     }
     return true;
 }
 public VData getResult() {
     return this.mResult;
 }
    private void jbInit() throws Exception {
    }
}
