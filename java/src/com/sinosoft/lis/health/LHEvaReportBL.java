/*
 * <p>ClassName: LHEvaReportBL </p>
 * <p>Description: LHEvaReportBL类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-12-13 10:48:38
 */
package com.sinosoft.lis.health;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
public class LHEvaReportBL  {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData= new VData();
private MMap map= new MMap();

/** 全局数据 */
private GlobalInput mGlobalInput =new GlobalInput() ;
/** 数据操作字符串 */
private String mOperate;
private String tNo;
/** 业务处理相关变量 */
private LHEvaReportSet mLHEvaReportSet=new LHEvaReportSet();
private LHTaskCustomerRelaSet mLHTaskCustomerRelaSet=new LHTaskCustomerRelaSet();

public LHEvaReportBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
public static void main(String[] args) {
}
/**
* 传输数据的公共方法
        * @param: cInputData 输入的数据
   *         cOperate 数据操作
* @return:
*/
 public boolean submitData(VData cInputData,String cOperate)
 {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
         return false;
    //进行业务处理
    if (!dealData())
    {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "LHEvaReportBL";
          tError.functionName = "submitData";
          tError.errorMessage = "数据处理失败LHEvaReportBL-->dealData!";
          this.mErrors .addOneError(tError) ;
          return false;
    }
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
    if (this.mOperate.equals("QUERY||MAIN"))
    {
      this.submitquery();
    }
    else
    {
      PubSubmit tPubSubmit = new PubSubmit();
      System.out.println("Start LHEvaReportBL Submit...");
      //LHEvaReportBLS tLHEvaReportBLS=new LHEvaReportBLS();
      //tLHEvaReportBLS.submitData(mInputData,mOperate);
      if (!tPubSubmit.submitData(mInputData, mOperate))
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);

        CError tError = new CError();
        tError.moduleName = "LHEvaReportBL";
        tError.functionName = "submitData";
        tError.errorMessage = "数据提交失败!";

        this.mErrors.addOneError(tError);
        return false;
      }

      System.out.println("End LHEvaReportBL Submit...");
      //如果有需要处理的错误，则返回
      //if (tLHEvaReportBLS.mErrors.needDealError())
      //{
        // @@错误处理
        //this.mErrors.copyAllErrors(tLHEvaReportBLS.mErrors);
       // CError tError = new CError();
        //tError.moduleName = "LHEvaReportBL";
        //tError.functionName = "submitDat";
        //tError.errorMessage ="数据提交失败!";
        //this.mErrors .addOneError(tError) ;
        //return false;
      //}
    }
    mInputData=null;
    return true;
}
 /**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean dealData()
{

    if (this.mOperate.equals("INSERT||MAIN")) {

         if (mLHEvaReportSet != null && mLHEvaReportSet.size() > 0)
         {
            for (int j = 1; j <= mLHEvaReportSet.size(); j++) {
                     mLHEvaReportSet.get(j).setOperator(mGlobalInput.Operator);
                     mLHEvaReportSet.get(j).setMakeDate(PubFun.getCurrentDate());
                     mLHEvaReportSet.get(j).setMakeTime(PubFun.getCurrentTime());
                     mLHEvaReportSet.get(j).setModifyDate(PubFun.getCurrentDate());
                     mLHEvaReportSet.get(j).setModifyTime(PubFun.getCurrentTime());
                     mLHEvaReportSet.get(j).setManageCom(mGlobalInput.ManageCom);
                     String CustomnerNo=mLHEvaReportSet.get(j).getCustomerNo();//客户号
                     String MStartDate = PubFun.getCurrentDate().replaceAll("-", "");

                     String ReportName = "";
                     SSRS tSSRS = new SSRS();
                     ExeSQL tExeSQL = new ExeSQL();
                    String sql = null;
                    try {
                        sql ="select max(substr(ReportName, 10,3)) from LHEvaReport where makedate='" +PubFun.getCurrentDate() + "' ";
                    } catch (Exception ex) {
                    }

                     tSSRS = tExeSQL.execSQL(sql);
                     String rr[][] = tSSRS.getAllData();
                     String result = rr[0][0].toString();

                     if (result.equals("") ||
                         result.equals(null) | result.equals("null")) {
                         ReportName = CustomnerNo +  "00" + j;
                     } else {
                         Integer ii = new Integer(result); //结果转为整型
                         int iii = ii.intValue();
                         int oo = iii++; //在数据库最大值加上‘1’
                         Integer result2 = new Integer(oo + j); //判断队列里有多少个
                         String result3 = result2.toString();
                         String result4 = "";
                         if (result3.length() == 2) {
                             result4 = "0" + result3.toString();
                         } else if (result3.length() == 1) {
                             result4 = "00" + result3.toString();
                         } else if (result3.length() == 3) {
                             result4 = result3.toString();
                         }
                         ReportName = CustomnerNo + result4;
                     }
                     mLHEvaReportSet.get(j).setReportName(ReportName);//报告文件名
              }
              map.put(mLHEvaReportSet, "INSERT");
              for (int j= 1; j<= mLHTaskCustomerRelaSet.size(); j++)
              {
                      if (mLHTaskCustomerRelaSet.get(j).getTaskExecNo() == null || mLHTaskCustomerRelaSet.get(j).getTaskExecNo().equals(""))
                      {
                           mLHTaskCustomerRelaSet.get(j).setTaskExecNo(PubFun1.CreateMaxNo("TaskExecNo", 12));
                      }
                      mLHTaskCustomerRelaSet.get(j).setOperator(mGlobalInput.Operator);
                      mLHTaskCustomerRelaSet.get(j).setMakeDate(PubFun.getCurrentDate());
                      mLHTaskCustomerRelaSet.get(j).setMakeTime(PubFun.getCurrentTime());
                      mLHTaskCustomerRelaSet.get(j).setModifyDate(PubFun.getCurrentDate());
                      mLHTaskCustomerRelaSet.get(j).setModifyTime(PubFun.getCurrentTime());
                      mLHTaskCustomerRelaSet.get(j).setManageCom(mGlobalInput.ManageCom);
                      map.put(" delete from  LHTaskCustomerRela where TaskExecNo='" +
                      mLHTaskCustomerRelaSet.get(j).getTaskExecNo() +"'","DELETE");
              }
              map.put(mLHTaskCustomerRelaSet, "INSERT");
        }
    }

        if (this.mOperate.equals("UPDATE||MAIN")) {

        }

        if (this.mOperate.equals("DELETE||MAIN")) {
            for (int i = 1; i <= mLHEvaReportSet.size(); i++) {
                map.put(" delete from  LHEvaReport where TaskExecNo='" +mLHEvaReportSet.get(i).getTaskExecNo() +"'","DELETE");
            }
        }

        return true;


}
/**
 * 根据前面的输入数据，进行BL逻辑处理
 * 如果在处理过程中出错，则返回false,否则返回true
 */
private boolean updateData()
{
   return true;
}
/**
* 根据前面的输入数据，进行BL逻辑处理
* 如果在处理过程中出错，则返回false,否则返回true
*/
private boolean deleteData()
{
  return true;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
         this.mLHEvaReportSet.set((LHEvaReportSet)cInputData.getObjectByObjectName("LHEvaReportSet",0));
         this.mLHTaskCustomerRelaSet.set((LHTaskCustomerRelaSet)cInputData.getObjectByObjectName("LHTaskCustomerRelaSet",0));
         this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
         return true;
}
/**
* 准备往后层输出所需要的数据
* 输出：如果准备数据时发生错误则返回false,否则返回true
*/
 private boolean submitquery()
{
    this.mResult.clear();
    LHHmServBeManageDB tLHHmServBeManageDB=new LHHmServBeManageDB();
    //tLHHmServBeManageDB.setSchema(this.mLHHmServBeManageSchema);
                //如果有需要处理的错误，则返回
                if (tLHHmServBeManageDB.mErrors.needDealError())
                {
                  // @@错误处理
                        this.mErrors.copyAllErrors(tLHHmServBeManageDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "LHEvaReportBL";
                        tError.functionName = "submitData";
                        tError.errorMessage = "数据提交失败!";
                        this.mErrors .addOneError(tError) ;
                        return false;
    }
    mInputData=null;
    return true;
}
 private boolean prepareOutputData()
 {
   try
        {
                this.mInputData.clear();

                this.mInputData.add(this.mLHEvaReportSet);
                this.mInputData.add(this.mLHTaskCustomerRelaSet);
                mInputData.add(map);
                mResult.clear();
                mResult.add(this.mLHEvaReportSet);
                mResult.add(this.mLHTaskCustomerRelaSet);

        }
        catch(Exception ex)
        {
                // @@错误处理
                CError tError =new CError();
                tError.moduleName="LHEvaReportBL";
                tError.functionName="prepareData";
                tError.errorMessage="在准备往后层处理所需要的数据时出错。";
                this.mErrors .addOneError(tError) ;
                return false;
        }
        return true;
        }
        public VData getResult()
        {
        return this.mResult;
        }

    private void jbInit() throws Exception {
    }
}
