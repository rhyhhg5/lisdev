/*
 * <p>ClassName: LHServTaskAffirmBL </p>
 * <p>Description: LHServTaskAffirmBL类文件 </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2006-08-23 10:29:01
 */
package com.sinosoft.lis.health;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

public class LHServTaskAffirmBL {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap map = new MMap();
    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate;
    /** 业务处理相关变量 */
    private LHCaseTaskRelaSchema mLHCaseTaskRelaSchema = new LHCaseTaskRelaSchema();
    private LHCaseTaskRelaSet mLHCaseTaskRelaSet= new LHCaseTaskRelaSet();

    public LHServTaskAffirmBL() {
    }

    public static void main(String[] args) {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData 输入的数据
     *         cOperate 数据操作
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServTaskAffirmBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据处理失败LHServTaskAffirmBL-->dealData!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData()) {
            return false;
        }
        if (this.mOperate.equals("QUERY||MAIN")) {
            this.submitquery();
        } else {
            System.out.println("Start LHServTaskAffirmBL Submit...");
            PubSubmit tPubSubmit = new PubSubmit();

            if (!tPubSubmit.submitData(mInputData, mOperate)) {
                // @@错误处理
                this.mErrors.copyAllErrors(tPubSubmit.mErrors);

                CError tError = new CError();
                tError.moduleName = "LHServTaskAffirmBL";
                tError.functionName = "submitData";
                tError.errorMessage = "数据提交失败!";

                this.mErrors.addOneError(tError);
                return false;
            }

        }
        mInputData = null;
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     * @return boolean
     *
     */
    private boolean dealData() {
        System.out.println("--------DealData---------");
        System.out.println("AAAAA  AAA"+ mLHCaseTaskRelaSchema.getServTaskNo());
        System.out.println("BB BBBBBBBB "+ mLHCaseTaskRelaSchema.getServTaskAffirm());
        if (this.mOperate.equals("UPDATE||MAIN")) {
                          System.out.println("?????????"+mLHCaseTaskRelaSet.size());
                          for (int i = 1; i <= mLHCaseTaskRelaSet.size(); i++) {
                              mLHCaseTaskRelaSet.get(i).setOperator(mGlobalInput.Operator);
                              mLHCaseTaskRelaSet.get(i).setMakeDate(PubFun.getCurrentDate());
                              mLHCaseTaskRelaSet.get(i).setMakeTime(PubFun.getCurrentTime());
                              mLHCaseTaskRelaSet.get(i).setModifyDate(PubFun.getCurrentDate());
                              mLHCaseTaskRelaSet.get(i).setModifyTime(PubFun.getCurrentTime());
                              mLHCaseTaskRelaSet.get(i).setManageCom(mGlobalInput.ManageCom);
                              map.put(" update LHCaseTaskRela set  ServTaskAffirm='2' where ServTaskNo='" +
                                     mLHCaseTaskRelaSet.get(i).getServTaskNo() + "' ",
                                    "UPDATE");

                          }
                    }

        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     *
     */
    private boolean updateData() {
        return true;
    }

    /**
     * 根据前面的输入数据，进行BL逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean deleteData() {
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true\
     * @return boolean
     * @param cInputData
     */
    private boolean getInputData(VData cInputData) {
        //this.mLHCaseTaskRelaSchema.setSchema((LHCaseTaskRelaSchema) cInputData.
                                         //getObjectByObjectName(
                                                // "LHCaseTaskRelaSchema", 0));
        this.mLHCaseTaskRelaSet.set((LHCaseTaskRelaSet)cInputData.getObjectByObjectName("LHCaseTaskRelaSet",0));
        this.mGlobalInput.setSchema((GlobalInput) cInputData.
                                    getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     * @return boolean
     */
    private boolean submitquery() {
        this.mResult.clear();
        LHCaseTaskRelaDB tLHCaseTaskRelaDB = new LHCaseTaskRelaDB();
       // tLHCaseTaskRelaDB.setSchema(this.mLHCaseTaskRelaSchema);
        //如果有需要处理的错误，则返回
        if (tLHCaseTaskRelaDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHCaseTaskRelaDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHServTaskAffirmBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }

    /**
     * 准备往后层处理所需要的数据
     * @return boolean
     */

    private boolean prepareOutputData() {
        try {
            this.mInputData.clear();
            this.mInputData.add(this.mLHCaseTaskRelaSet);
            mInputData.add(map);
            mResult.clear();
            mResult.add(this.mLHCaseTaskRelaSet);
            //mResult.add(this.mLHServItemSet);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHServTaskAffirmBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 返回结果
     * @return VData
     */

    public VData getResult() {
        return this.mResult;
    }
}
