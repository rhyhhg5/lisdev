/*
 * <p>ClassName: OLDSocialInsOrganUI </p>
 * <p>Description: OLDSocialInsOrganUI类文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: testcompany </p>
 * @Database:
 * @CreateDate：2005-01-15 13:07:18
 */
package com.sinosoft.lis.health;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.pubfun.*;
public class LDSocialInsOrganUI {
/** 错误处理类，每个需要错误处理的类中都放置该类 */
public  CErrors mErrors=new CErrors();
private VData mResult = new VData();
/** 往后面传输数据的容器 */
private VData mInputData =new VData();
/** 数据操作字符串 */
private String mOperate;
//业务处理相关变量
 /** 全局数据 */
private LDSocialInsOrganSchema mLDSocialInsOrganSchema=new LDSocialInsOrganSchema();
private GlobalInput mGlobalInput =new GlobalInput() ;
public LDSocialInsOrganUI ()
{
}
 /**
传输数据的公共方法
*/
public boolean submitData(VData cInputData,String cOperate)
{
  //将操作数据拷贝到本类中
  this.mOperate =cOperate;
  this.mInputData = cInputData;
  //得到外部传入的数据,将数据备份到本类中
//  if (!getInputData(cInputData))
//    return false;

  //进行业务处理
//  if (!dealData())
//    return false;

  //准备往后台的数据
//  if (!prepareOutputData())
//    return false;

  LDSocialInsOrganBL tLDSocialInsOrganBL=new LDSocialInsOrganBL();

  //System.out.println("Start OLDSocialInsOrgan UI Submit...");
  tLDSocialInsOrganBL.submitData(mInputData,mOperate);
  //System.out.println("End OLDSocialInsOrgan UI Submit...");
  //如果有需要处理的错误，则返回
  if (tLDSocialInsOrganBL.mErrors.needDealError() )
  {
     // @@错误处理
     this.mErrors.copyAllErrors(tLDSocialInsOrganBL.mErrors);
     CError tError = new CError();
     tError.moduleName = "LDSocialInsOrganUI";
     tError.functionName = "submitData";
     tError.errorMessage = "数据提交失败!";
     this.mErrors .addOneError(tError) ;
     return false;
  }
  if (mOperate.equals("INSERT||MAIN"))
  {
//     this.mResult.clear();
//     this.mResult=tLDSocialInsOrganBL.getResult();
  }
  mInputData=null;
  return true;
  }
  public static void main(String[] args)
  {
      LDSocialInsOrganSchema tLDSocialInsOrganSchema   = new LDSocialInsOrganSchema();
      LDSocialInsOrganUI tLDSocialInsOrganUI   = new LDSocialInsOrganUI();
          //输出参数
          CErrors tError = null;
          String tRela  = "";
          String FlagStr = "";
          String Content = "";
          String transact = "INSERT||MAIN";

          String tG="GI";

          //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
          transact = "fmtransact";
    tLDSocialInsOrganSchema.setSIOrganCode("DocHM随风倒光荣感第三国如果如果让公司热狗UploadContSIOrganCode");
    tLDSocialInsOrganSchema.setSuperSIOrganCode("DocHMUploadContSuperSIOrganCode");
    tLDSocialInsOrganSchema.setSIOrganName("DocHMUploadContSIOrganName");
    tLDSocialInsOrganSchema.setAreaCode("DocHMUploadContAreaCode");
    tLDSocialInsOrganSchema.setAddress("Address");
    tLDSocialInsOrganSchema.setZipCode("ZipCode");
    tLDSocialInsOrganSchema.setPhone("Phone");
    tLDSocialInsOrganSchema.setWebAddress("WebAddress");
    tLDSocialInsOrganSchema.setFax("Fax");
    tLDSocialInsOrganSchema.setbankCode("bankCode");
    tLDSocialInsOrganSchema.setAccName("AccName");
    tLDSocialInsOrganSchema.setbankAccNO("bankAccNO");
    tLDSocialInsOrganSchema.setSatrapName("SatrapName");
    tLDSocialInsOrganSchema.setLinkman("Linkman");
    tLDSocialInsOrganSchema.setLastModiDate("LastModiDate");

  // 准备传输数据 VData
        VData tVData = new VData();
        tVData.add(tLDSocialInsOrganSchema);
        tVData.add(tG);
    tLDSocialInsOrganUI.submitData(tVData,"INSERT||MAIN");

  }
  /**
  * 准备往后层输出所需要的数据
  * 输出：如果准备数据时发生错误则返回false,否则返回true
  */
 private boolean prepareOutputData()
 {
    try
    {
      mInputData.clear();
      mInputData.add(this.mLDSocialInsOrganSchema);
      mInputData.add(this.mInputData);
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LDSocialInsOrganUI";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
}
/**
 * 根据前面的输入数据，进行UI逻辑处理
  * 如果在处理过程中出错，则返回false,否则返回true
  */
 private boolean dealData()
 {
      boolean tReturn =false;
      //此处增加一些校验代码
      tReturn=true;
      return tReturn ;
}
 /**
 * 从输入数据中得到所有对象
 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 */
private boolean getInputData(VData cInputData)
{
  //全局变量
  this.mLDSocialInsOrganSchema.setSchema((LDSocialInsOrganSchema)cInputData.getObjectByObjectName("LDSocialInsOrganSchema",0));
  this.mGlobalInput.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
  return true;
}
public VData getResult()
{
  return this.mResult;
}
}
