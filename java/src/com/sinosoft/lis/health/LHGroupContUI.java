package com.sinosoft.lis.health;

/**
 * <p>Title:OLHGroupContUI </p>
 *
 * <p>Description:OLHGroupContUI </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author lyg
 * @version 1.0
 * @CreateDate：2005-04-121 18:32:42
 */
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.sys.*;
import com.sinosoft.lis.pubfun.*;

public class LHGroupContUI {
    public LHGroupContUI() {
    }

    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;
//业务处理相关变量
    /** 全局数据 */
    private LHGroupContSchema mLHGroupContSchema = new LHGroupContSchema();
    /**
     传输数据的公共方法
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        LHGroupContBL tLHGroupContBL = new LHGroupContBL();
        tLHGroupContBL.submitData(cInputData, mOperate);

        //如果有需要处理的错误，则返回
        if (tLHGroupContBL.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLHGroupContBL.mErrors);
            CError tError = new CError();
            tError.moduleName = "LHGroupContUI";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
//        if (mOperate.equals("INSERT||MAIN")) {
//            this.mResult.clear();
//            this.mResult = tLHGroupContBL.getResult();
//        }
//        if (mOperate.equals("UPDATE||MAIN")) {
//            this.mResult.clear();
//            this.mResult = tLHGroupContBL.getResult();
//        }
//        if (mOperate.equals("DELETE||MAIN")) {
//            this.mResult.clear();
//            this.mResult = tLHGroupContBL.getResult();
//        }

        mInputData = null;
        return true;
    }

    public static void main(String[] args) {
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        try {
            mInputData.clear();
            mInputData.add(this.mLHGroupContSchema);
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LHGroupContUI";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData() {
        boolean tReturn = false;
        //此处增加一些校验代码
        tReturn = true;
        return tReturn;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        //全局变量
        this.mLHGroupContSchema.setSchema((LHGroupContSchema) cInputData.
                                           getObjectByObjectName(
                "LHGroupContSchema", 0));

        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

}
