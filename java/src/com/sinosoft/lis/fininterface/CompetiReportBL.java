package com.sinosoft.lis.fininterface;

import java.io.File;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


import jxl.*;
import jxl.write.*;
import jxl.write.biff.RowsExceededException;
import jxl.format.UnderlineStyle;

public class CompetiReportBL
{
	public  CErrors mErrors=new CErrors();
	private VData mInputData= new VData();
	private TransferData tTransferData = new TransferData();
	/** 全局数据 */
	private GlobalInput mGlobalInput =new GlobalInput() ;
	/** 数据操作字符串 */

    private String mOperater;
    private String mManageCom;
    private String Bdate;
    private String Edate;
    private String YBdate;
    private String YEdate;
    
	public CompetiReportBL()
	{}
    
    
    public boolean submitData(VData cInputData,String path) throws RowsExceededException, WriteException,Exception
	{
		//将操作数据拷贝到本类中
		this.mInputData = cInputData;
		System.out.println("---UpLoadWriteFinBL getInputData---");
		
		if (!getInputData(cInputData)) {
	            return false;
	        }
	      
		if (!checkData())
		{
			return false;
		}

		//进行业务处理
		if (!WriteXls(path)) 
		{
			return false;
		}

		if (!prepareOutputData())
		{
			return false;
		}
		
		if(!pubSubmit())
		{
			return false;
		}

		System.out.println("End UpLoadWriteFinBL Submit...");
		mInputData=null;
		return true;
	}
    
    
    /**
     * 创建Excel
     * @throws WriteException 
     * @throws RowsExceededException 
     */
    public boolean WriteXls(String path)
    {
    	
        	
    	WritableWorkbook book=null;
			try {
				
				//建新的Excel
				System.out.println(path+"ModelExcel-2012.xls");
				System.out.println("fin-"+mOperater.trim()+"-2012.xls");
				book= Workbook.createWorkbook(new File(path+"fin-"+mOperater.trim()+"-2012.xls"),Workbook.getWorkbook(new File(path+"ModelExcel-2012.xls")));
				
				//获取Excel工作薄
				WritableSheet sh1=book.getSheet(0);
				WritableSheet sh2=book.getSheet(1);
				
				//查询条件
				String gg=" and accountcode in('6031000000', '7031000000') and length(managecom) = 4";  //公共条件
				String bfsrDate=" and accountdate between '"+Bdate+"' and '"+Edate+"' ";   //保费收入日期限制
			    String ituibao=" and exists(select 1 from ljagetendorse b where feeoperationtype = 'WT' and a.contno = b.contno and b.makedate between '"+YBdate+"' and '"+YEdate+"') "; //犹豫期退保检验-个单
			    String gtuibao=" and exists(select 1 from ljagetendorse b where feeoperationtype = 'WT' and a.contno = b.grpcontno and b.makedate between '"+YBdate+"' and '"+YEdate+"') "; //犹豫期退保检验-团单
			    String gxian=" and substr(a.costcenter, 5, 2) = '91' "; //团险系列
			    String ixian=" and substr(a.costcenter, 5, 2) = '90' "; //个险系列
			    String yxian=" and substr(a.costcenter, 5, 2) = '92' "; //银保系列
			    String bf=" sum(case finitemtype when 'D' then -summoney else summoney end ) as money ";
			    String ltx=" and riskcode not in('1605','170101','170106')";  //排除老特需
			    
				
				//查询函数
				String str1=" select (case managecom when '1000' then '总公司本级'" 
					       +" when '9210' then '大连分公司本级'" 
					       +" when '9370' then '青岛分公司本级'"                                                                                           
					       +" when '9440' then '深圳分公司本级' else (select name from ldcom where comcode=('86' ||managecom || '00'))end)"   
						   +" ,dec(cast(nvl(sum(money1),0) as float)/10000+0.005,20,2),'','',dec(cast(nvl(sum(money2),0) as float)/10000+0.005,20,2),'','',dec(cast(nvl(sum(money3),0) as float)/10000+0.005,20,2),'','',dec(cast(nvl(sum(money4),0) as float)/10000+0.005,20,2),'','',dec(cast(nvl(sum(money5),0) as float)/10000+0.005,20,2),'','',dec(cast(nvl(sum(money6),0) as float)/10000+0.005,20,2),'','',dec(cast(nvl(sum(money7),0) as float)/10000+0.005,20,2),'','',dec(cast(nvl(sum(money8),0) as float)/10000+0.005,20,2),'','',dec(cast(nvl(sum(money9),0) as float)/10000+0.005,20,2)  from ("
					       +" select  (case managecom when '9210' then '9210'"  
						   +" when '1000' then '1000'" 
						   +" when '9370' then '9370'"                                                                                           
						   +" when '9440' then '9440'"                                                                                           
						   +" else substr(managecom,1,2) || '00' end) as managecom, money1 as money1,money2 as money2,money3 as money3,money4 as money4,money5 as money5,money6 as money6,money7 as money7,money8 as money8,money9 as money9  from( "
						   +" select aa.managecom,aa.money as money1,bb.money as money2,cc.money as money3,dd.money as money4,ee.money as money5,ff.money as money6,gg.money as money7,hh.money as money8,ii.money as money9 from ("
                           +" select managecom,sum(money) as money from (select managecom,"+bf
                           +" from lidatatransresult a where 1=1 "+gg+ltx+ituibao+bfsrDate+" group by managecom "
                           +" union all"
                           +" select managecom,"+bf+" from lidatatransresult a"
                           +" where 1=1 "+gg+ltx+gtuibao+bfsrDate+" group by managecom )as a1 where 1=1 group by managecom) as aa left join"
                           +" (select managecom,"+bf
                           +" from lidatatransresult a where 1=1"+gg+ltx+gxian+gtuibao+bfsrDate+" group by managecom)as bb on aa.managecom=bb.managecom left join"
                           +" (select managecom,"+bf+" from lidatatransresult a where 1=1 and markettype in('2','3','4','5','6','7','8','10','11','99')"+gg+gxian+gtuibao+bfsrDate+" group by managecom )as cc on aa.managecom=cc.managecom left join"
                           +" (select managecom,"+bf+" from lidatatransresult a where 1=1 and exists (select 1 from lmriskapp where riskcode = a.riskcode and risktype = 'A' and riskperiod in ('M', 'S')) "+gg+gxian+gtuibao+bfsrDate+" group by managecom )as dd on aa.managecom=dd.managecom left join"
                           +" (select managecom,"+bf+" from lidatatransresult a where 1=1 "+gg+ixian+ituibao+bfsrDate+" group by managecom )as ee on aa.managecom=ee.managecom left join"
                           +" (select managecom, sum(money) as money from("
                           +" select serialno,managecom,"+bf+" from lidatatransresult a where 1=1 and substr(a.riskcode, 1, 4) in('2302','2303','2305','2306','2309','2401','2403','3301','3303','3304','3309','3401') and pcont = '21'"+gg+ixian+ituibao+bfsrDate+" group by managecom,serialno"
                           +" union "
                           +" select serialno,managecom,"+bf+" from lidatatransresult a where 1=1 and premiumtype in ('10', '99') and pcont = '21'"+gg+ixian+ituibao+bfsrDate+" group by managecom,serialno) as f1"
                           +" where 1=1 group by managecom) as ff on aa.managecom = ff.managecom left join"
                           +" (select managecom,"+bf+" from lidatatransresult a where 1=1 and pcont = '31' "+gg+ixian+ituibao+bfsrDate+" group by managecom )as gg on aa.managecom=gg.managecom left join"
                           +" (select managecom,"+bf+" from lidatatransresult a where 1=1 "+gg+yxian+ituibao+bfsrDate+" group by managecom )as hh on aa.managecom=hh.managecom left join"
                           +" (select managecom,"+bf+" from lidatatransresult a where 1=1 and substr(a.riskcode, 1, 4) in('1703', '1704') "+gg+gtuibao+bfsrDate+" group by managecom )as ii on aa.managecom=ii.managecom"
                           +" )as hh)as zz where 1=1  group by managecom";
				
				String str2="select id,(select name from ldcom where comcode=('86' ||manage || '00')),(select name from ldcom where comcode=('86' ||managecom || '00')),f1,f2,f3,f4,f5,f6" 
						   +" from ( select  char(rownumber() over()) as id,substr(aa.managecom,1,2) || '00' as manage,aa.managecom as managecom,dec(cast(nvl(aa.money,0) as float)/10000+0.005,20,2) as f1,dec(cast(nvl(bb.money,0) as float)/10000+0.005,20,2) as f2,dec(cast(nvl(cc.money,0) as float)/10000+0.005,20,2) as f3,dec(cast(nvl(dd.money,0) as float)/10000+0.005,20,2) as f4,dec(cast(nvl(ee.money,0) as float)/10000+0.005,20,2) as f5,dec(cast(nvl(ff.money,0) as float)/10000+0.005,20,2) as f6 from ("
                           +" select managecom,sum(money) as money from ("
                           +" select managecom,"+bf+" from lidatatransresult a where 1=1 "+gg+ltx+ituibao+bfsrDate+" group by managecom "
                           +" union all"
                           +" select managecom,"+bf+" from lidatatransresult a where 1=1 "+gg+ltx+gtuibao+bfsrDate+" group by managecom "
                           +" ) as a1 where 1=1  group by managecom)as aa left join"
                           +" (select managecom,"+bf+" from lidatatransresult a where 1=1 and exists (select 1 from lmriskapp where riskcode = a.riskcode and risktype = 'A' and riskperiod in ('M', 'S')) "+gg+gxian+gtuibao+bfsrDate+" group by managecom )as bb on aa.managecom=bb.managecom left join"
                           +" (select managecom, sum(money) as money from("
                           +" select serialno,managecom,"+bf+" from lidatatransresult a where 1=1 and substr(a.riskcode, 1, 4) in('2302','2303','2305','2306','2309','2401','2403','3301','3303','3304','3309','3401') and pcont = '21'"+gg+ixian+ituibao+bfsrDate+" group by managecom,serialno"
                           +" union "
                           +" select serialno,managecom,"+bf+" from lidatatransresult a where 1=1 and premiumtype in ('10', '99') and pcont = '21'"+gg+ixian+ituibao+bfsrDate+" group by managecom,serialno) as c1"
                           +" where 1=1 group by managecom) as cc on aa.managecom = cc.managecom left join"
                           +" (select managecom,"+bf+" from lidatatransresult a where 1=1 "+gg+yxian+ituibao+bfsrDate+" group by managecom )as dd on aa.managecom=dd.managecom left join"
                           +" (select managecom,"+bf+" from lidatatransresult a where 1=1 and substr(a.riskcode, 1, 4) in('1703', '1704') "+gg+gtuibao+bfsrDate+" group by managecom )as ee on aa.managecom=ee.managecom left join"
                           +" (select managecom,"+bf+" from lidatatransresult a where 1=1 and markettype in('2','3','4','5','6','7','8','10','11','99')"+gg+gxian+gtuibao+bfsrDate+" group by managecom )as ff on aa.managecom=ff.managecom "
                           +" where aa.managecom not like '%00' and aa.managecom not in('9210','9370','9440') order by aa.money desc )as ee with ur";
				
				//定义单元格格式
				WritableFont wfc = new WritableFont(WritableFont.ARIAL, 10,
				WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				jxl.format.Colour.BLACK); // 定义格式 字体 下划线 斜体 粗体 颜色
				WritableCellFormat wcft = new WritableCellFormat(wfc); // 单元格定义
				//wcft.setBackground(jxl.format.Colour.WHITE); // 设置单元格的背景颜色
				wcft.setAlignment(jxl.format.Alignment.LEFT); // 设置对齐方式   
				wcft.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
				wcft.setBorder(jxl.format.Border.ALL,jxl.format.BorderLineStyle.THIN); 
				
				//写入2012年分公司业务发展突出贡献奖
				WriteSheet(sh1,str1,26,7,wcft,1);
				
				//写入2012年地市级机构业务发展突出贡献奖
				WriteSheet(sh2,str2,9,5,wcft,3);
								
				//写入
				book.write();			
				book.close();
				
	        	System.out.println("true");
				return true;
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				bulidErr("WriteXls","生成报表失败!原因是"+e.toString());
		        return false;
			}

            
    }
//数据写入Excel中    
    private boolean WriteSheet(WritableSheet sheet,String sql,int x,int y,WritableCellFormat wf,int FirstSum) throws RowsExceededException, WriteException,Exception
    {
    	try{
    	ExeSQL es = new ExeSQL();
    	SSRS mSSRS = new SSRS();
    	mSSRS = es.execSQL(sql);
    	int maxrow=mSSRS.getMaxRow();
    	for(int a=0;a<x;a++)
    	{
    		for(int i=y;i<=(maxrow+y-1);i++)
	    	{ 
	    		sheet.addCell(new Label(a,(i-1),mSSRS.GetText((i-y+1), a+1),wf));
	    	}
    	}
    	WriteSheetSum(sheet,maxrow,x,y,wf,FirstSum);
    		
    	return true;
    	}catch(Exception e){
    		Exception cv =  new Exception("写入excel文件失败"+e.getMessage());
   		    throw cv;
    	}
    }
//写入合计值
    private boolean WriteSheetSum(WritableSheet sheet,int maxrow,int x,int y,WritableCellFormat wf,int FirstSum) throws Exception
    {
    	try{
    	sheet.addCell(new Label(0,(y+maxrow-1),"合计",wf));
    	for(int i=1;i<x;i++){
    	if(i<FirstSum) {
    		sheet.addCell(new Label(i,(y+maxrow-1),"",wf));
    		continue;
    	}
    	String cellCont=sheet.getCell(i,y-1).getContents();
    	if(cellCont==null || cellCont.equals("")){
    	   sheet.addCell(new Label(i,(y+maxrow-1),"",wf));
    	}else{
    		double sum=0;
    		for(int a1=(y-1);a1<(y+maxrow-1);a1++){
    			sum=sum+Double.parseDouble(sheet.getCell(i,a1).getContents().trim());
    		}
    		sum=Math.round(sum*100)/100.0;
    		System.out.println("sum="+sum);
    		sheet.addCell(new Label(i,(y+maxrow-1),Double.toString(sum),wf));
    	}
    	}
    	}catch(Exception e)
    	{
    		 Exception cv =  new Exception("写入合计值失败"+e.getMessage());
    		 throw cv;
    	}
    	return true;
    }
  
    private boolean getInputData(VData cInputData)
	{
		mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0);
		tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData",0);
		if (mGlobalInput == null) {
            // @@错误处理
            bulidErr("getInputData", "前台传输全局公共数据失败!");
            return false;
        }

        //获得操作员编码
		mOperater = mGlobalInput.Operator;
        if ((mOperater == null) || mOperater.trim().equals("")) {
            // @@错误处理
        	bulidErr("getInputData", "前台传输全局公共数据Operate失败!");
            return false;
        }

        //获得登陆机构编码
        mManageCom = mGlobalInput.ManageCom;
        if ((mManageCom == null) || mManageCom.trim().equals("")) {
            // @@错误处理
        	bulidErr("getInputData", "前台传输全局公共数据ManageCom失败!");
            return false;
        }
        Bdate=tTransferData.getValueByName("Bdate").toString();
        if ((Bdate == null) || Bdate.trim().equals("")) {
            // @@错误处理
        	bulidErr("getInputData", "前台传输保费起始日期失败!");
            return false;
        }
        Edate=tTransferData.getValueByName("Edate").toString();
        if ((Edate == null) || Edate.trim().equals("")) {
            // @@错误处理
        	bulidErr("getInputData", "前台传输保费终止日期失败!");
            return false;
        }
        YBdate=tTransferData.getValueByName("YBdate").toString();
        if ((YBdate == null) || YBdate.trim().equals("")) {
            // @@错误处理
        	bulidErr("getInputData", "前台传输犹豫期退保起始日期失败!");
            return false;
        }
        YEdate=tTransferData.getValueByName("YEdate").toString();
        if ((YEdate == null) || YEdate.trim().equals("")) {
            // @@错误处理
        	bulidErr("getInputData", "前台传输犹豫期退保终止日期失败!");
            return false;
        }
        
		return true;
	}
    
	private boolean checkData()
	{
		return true;
	}
	
	private boolean prepareOutputData()
	{
		return true;
	}
 
	/**
	 * 提交数据
	 * @return
	 */
	private boolean pubSubmit()
	{
		return true;
	}
	 private void bulidErr(String functionName,String errorMessage) {
		 CError tError = new CError();
         tError.moduleName = "CompetiReportBL";
         tError.functionName = functionName;
         tError.errorMessage = errorMessage;
         this.mErrors.addOneError(tError);
	}
	
    /**
     * 测试
     * @param args
     * @throws WriteException 
     * @throws RowsExceededException 
     */
    public static void main(String[] args)
    {
    	/*BusCompetiReportBL read = new BusCompetiReportBL();
        try {
			//read.WriteXls("D:/");
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
//			System.out.(e.printStackTrace());
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

    }
}
