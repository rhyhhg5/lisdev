package com.sinosoft.lis.fininterface;

import java.util.Calendar;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.LIOperationDataClassDefSchema;
import com.sinosoft.lis.db.LIDistillErrDB;
import com.sinosoft.lis.db.LIOperationDataClassDefDB;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.vschema.LITransInfoDefSet;
import com.sinosoft.lis.db.LITransInfoDefDB;
import com.sinosoft.lis.schema.LITransInfoDefSchema;
import com.sinosoft.lis.schema.LIAboriginalDataSchema;
import com.sinosoft.lis.schema.LIDataTransResultSchema;
import com.sinosoft.lis.vschema.LIDataTransResultSet;
import com.sinosoft.lis.schema.LIDistillInfoSchema;
import com.sinosoft.lis.vschema.LIDistillInfoSet;
import com.sinosoft.lis.vschema.LIDistillLogSet;
import com.sinosoft.lis.vschema.LIDistillErrSet;
import com.sinosoft.lis.vschema.LIAboriginalDataSet;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LIDistillErrSchema;
import com.sinosoft.lis.pubfun.PubFun;

/**
 * <p>Title: FinDataType </p>
 *
 * <p>Description:财务务原始业务数据类型定义 </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company:sinosoft </p>
 *
 * @author jianan
 * @version 1.0
 */
public class FinDataType {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据类型定义表*/
    private LIOperationDataClassDefSchema mLIOperationDataClassDefSchema ;
    /** 存储数据关联科目 */
    private LITransInfoDefSet mLITransInfoDefSet;
    /** 存储数据关联科目处理类 */
    private AccountItemType mAccountItemType[];
    /** 数据主键定义表*/
    private LogDeal mLogDeal ;
    /** 数据批次处理限制数*/
    private int mMaxDealNUm = 2000;

    long tTime1 = 0;
    long tTime2 = 0;



    //业务数据
    LIAboriginalDataSet mLIAboriginalDataSet;
    LIDataTransResultSet mLIDataTransResultSet;
    LIDistillInfoSet mLIDistillInfoSet;
    LIDistillErrSet mLIDistillErrSet;
    LIDistillLogSet mLIDistillLogSet;
    GlobalInput mGlobalInput = new GlobalInput();
    String StartDate = "";
    String EndDate = "";
    String BatchNo ="";
    /**LIAboriginalData表批次流水号*/
    String aSerailNo[];
    /**LIDataTransResult表批次流水号*/
    String dSerailNo[];



    public FinDataType() {
    }

    public boolean InitFinDataTypeInfo(String ClassID)
    {
       LIOperationDataClassDefDB tLIOperationDataClassDefDB = new LIOperationDataClassDefDB();
       tLIOperationDataClassDefDB.setClassID(ClassID);
       if(!tLIOperationDataClassDefDB.getInfo())
       {
           buildError("InitFinDataTypeInfo函数",
                      "数据类型编码为" + ClassID + "的LIOperationDataClassDef表查询无数据");
                return false;
       }
       if(tLIOperationDataClassDefDB.mErrors.needDealError())
       {
           buildError("InitFinDataTypeInfo函数",
                      "数据类型编码为" + ClassID + "LIOperationDataClassDef表查询出错");
                return false;
       }
       mLIOperationDataClassDefSchema = new  LIOperationDataClassDefSchema();
       mLIOperationDataClassDefSchema.setSchema(tLIOperationDataClassDefDB.getSchema());

       LITransInfoDefDB tLITransInfoDefDB = new LITransInfoDefDB();
       tLITransInfoDefDB.setClassID(ClassID);
       mLITransInfoDefSet = tLITransInfoDefDB.query();
       if(tLITransInfoDefDB.mErrors.needDealError())
       {
           buildError("InitFinDataTypeInfo函数",
                      "数据类型编码为" + ClassID + "的LITransInfoDef表查询出错");
                return false;
       }
       if(mLITransInfoDefSet.size()<=0)
       {
           buildError("InitFinDataTypeInfo函数",
                     "数据类型编码为" + ClassID + "的LITransInfoDef表无定义");
               return false;
       }
       if(!InitAccountItemType())
       {
           return false;
       }
       mLogDeal = new LogDeal();
       if(!mLogDeal.InitLogDealInfo(ClassID))
       {
           this.mErrors.copyAllErrors(mLogDeal.mErrors);
           return false;
       }
       return true;
    }
    private LIDataTransResultSet DealOperationDataInfo(LIAboriginalDataSchema tLIAboriginalDataSchema,String[] tSerailNo) throws Exception
    {
    	String msg = "";
    	LIDataTransResultSet tLIDataTransResultSet = new LIDataTransResultSet();
    	LIOperationDataClassDefDB cLIOperationDataClassDefDB = new LIOperationDataClassDefDB();
        try
        {
            for(int i=0;i<mAccountItemType.length;i++)
            {	cLIOperationDataClassDefDB.setClassID(tLIAboriginalDataSchema.getClassID());
                LIDataTransResultSchema tLIDataTransResultSchema = mAccountItemType[i].DealInfo(tLIAboriginalDataSchema);
                if(tLIDataTransResultSchema.getAccountCode().equalsIgnoreCase("2202010000")){
                	tLIDataTransResultSchema.setStandByString1(tLIAboriginalDataSchema.getStandByString1());
                }
                tLIDataTransResultSchema.setSerialNo(tSerailNo[i]);
                tLIDataTransResultSchema.setKeyUnionValue(mLogDeal.CreatKeyUnionValue(tLIAboriginalDataSchema));
                tLIDataTransResultSchema.setFinItemType(mLITransInfoDefSet.get(i+1).getFinItemType());
                tLIDataTransResultSchema.setClassType(mLIOperationDataClassDefSchema.getClassType());
                tLIDataTransResultSchema.setStandByString3(tLIAboriginalDataSchema.getSerialNo());
                tLIDataTransResultSet.add(tLIDataTransResultSchema);
            }
        }
        catch (Exception ex)
        {
        	msg = ex.getMessage() + getErrorLinkInfo(tLIAboriginalDataSchema);

        	LIDistillErrDB tLIDistillErrDB = new  LIDistillErrDB();
            tLIDistillErrDB.setBatchNo(tLIAboriginalDataSchema.getBatchNo());
            tLIDistillErrDB.setClassID(tLIAboriginalDataSchema.getClassID());
            tLIDistillErrDB.setKeyUnionValue(mLogDeal.CreatKeyUnionValue(tLIAboriginalDataSchema));
            tLIDistillErrDB.setBussDate(tLIAboriginalDataSchema.getConfDate());
            tLIDistillErrDB.setErrInfo(msg);
            tLIDistillErrDB.setMakeDate(PubFun.getCurrentDate());
            tLIDistillErrDB.setMakeTime(PubFun.getCurrentTime());
            tLIDistillErrDB.setManageCom(mGlobalInput.ManageCom);
            tLIDistillErrDB.setOperator(mGlobalInput.Operator);

            if(!tLIDistillErrDB.insert()){
                System.out.println(tLIDistillErrDB.mErrors.getFirstError());
            }
            /*************************************************************/
        	Exception mv = new Exception(msg);
            throw mv;
        }
        return tLIDataTransResultSet;
    }

    private LIDistillInfoSchema DealOperationDataLog(LIAboriginalDataSchema tLIAboriginalDataSchema) throws Exception
    {
        try
        {
            LIDistillInfoSchema tLIDistillInfoSchema = mLogDeal.CreatLIDistillInfo(tLIAboriginalDataSchema);
            return tLIDistillInfoSchema;
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
            Exception cv =  new Exception(ex.getMessage() + getErrorLinkInfo(tLIAboriginalDataSchema));
            throw cv;
        }
    }

    public static String getErrorLinkInfo(LIAboriginalDataSchema tLIAboriginalDataSchema){
    	String tLinkInfo = "，索引信息：";
    		for(int i = 3;i<=8;i++){
	    	    String FieldName = tLIAboriginalDataSchema.getFieldName(i);   	     
	    	    String FieldValue = tLIAboriginalDataSchema.getV(FieldName);
	    	    if(FieldValue!=""&&FieldValue!=null&&!FieldValue.equals("null"))
	    	    	tLinkInfo = tLinkInfo + FieldName + "=" + FieldValue + " ";
    		}
//        System.out.println(tLinkInfo);
    	if(tLinkInfo.equals("，索引信息："))
    		tLinkInfo = "";
    	return tLinkInfo;
    }
    
    private boolean InitAccountItemType()
    {
         mAccountItemType = new  AccountItemType[mLITransInfoDefSet.size()];
        for(int i=1;i<=mLITransInfoDefSet.size();i++)
        {
            LITransInfoDefSchema  tLITransInfoDefSchema =  mLITransInfoDefSet.get(i);
            mAccountItemType[i-1] = new  AccountItemType();
            if(!mAccountItemType[i-1].InitItemDefInfo(tLITransInfoDefSchema.getFinItemID()))
            {
                this.mErrors.copyAllErrors(mAccountItemType[i-1].mErrors);
                return false;
            }
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "FinDataType";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }

    public boolean dealProcess(VData cInputData)
    {
        if(!InitParameter())
        {
            return false;
        }
        if(!getInputData(cInputData))
        {
            return false;
        }
        if(!DistillData())
        {
            return false;
        }
        if(!DealWithData())
        {
            return false;
        }
        if(!SaveData())
        {
            return false;
        }

        return true;
    }

    public boolean haveDataUnsettled (VData cInputData) throws Exception
    {
        try{
            tTime1 = Calendar.getInstance().getTimeInMillis();
        	
            GlobalInput tGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
            String tStartDate =   (String)cInputData.get(1);
            String tEndDate   =   (String)cInputData.get(2);


            String tDistillMode = mLIOperationDataClassDefSchema.getDistillMode();
            if (tDistillMode == null && tDistillMode.equals("")) {
                buildError("distillData",
                           "类型为" + mLIOperationDataClassDefSchema.getClassID() +
                           "数据提取方式未定义!");
                return false;
            }
            if (tDistillMode.equals("1")) {
                PubCalculator tPubCalculator = new PubCalculator();
                tPubCalculator.addBasicFactor("ManageCom",
                                              tGlobalInput.ManageCom);
                tPubCalculator.addBasicFactor("StartDate", tStartDate);
                tPubCalculator.addBasicFactor("EndDate", tEndDate);
                String sql = mLIOperationDataClassDefSchema.getDistillSQL();
                if (sql == null && sql.equals("")) {
                    buildError("distillData",
                               "类型为" +
                               mLIOperationDataClassDefSchema.getClassID() +
                               "数据提取SQL未定义!");
                    return false;
                }
                tPubCalculator.setCalSql(sql);
                sql = tPubCalculator.calculateEx();
                sql = "select count(1) from (" + sql + ") as tab1 with ur";
//                System.out.println("类型" + mLIOperationDataClassDefSchema.getClassID() + "计算后的提数判断条件为：" + sql);
                ExeSQL tExeSQL = new ExeSQL();
                int countNum = Integer.parseInt(tExeSQL.getOneValue(sql));
                System.out.println("类型" + mLIOperationDataClassDefSchema.getClassID() + "在时间" + tStartDate + "到" + tEndDate + "范围内还有" + countNum + "条数据");
                
                tTime2 = Calendar.getInstance().getTimeInMillis();
                System.out.println("@@@@@@@ 该SQL执行消耗的时间为：" + String.valueOf((tTime2 - tTime1) / 1000) + "秒 @@@@@@@");
                
                if(countNum>0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            else if(tDistillMode.equals("2"))
            {
                Class tClass = Class.forName(mLIOperationDataClassDefSchema.getDistillClass());
                DistillType mDistillTypeClass = (DistillType) tClass.newInstance();
                return mDistillTypeClass.haveDataUnsettled(cInputData);
            }
            
        }
        catch(Exception ex)
        {
            buildError("distillData",
                       "类型为" +
                       mLIOperationDataClassDefSchema.getClassID() +
                             ex.getMessage() );
            return false;
        }
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
        mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        StartDate =   (String)cInputData.get(1);
        EndDate   =   (String)cInputData.get(2);
        BatchNo   =   (String)cInputData.get(3);

        if (mGlobalInput == null)
        {
            buildError("getInputData", "前台传入的登陆信息为空!");
            return false;
        }
        if (StartDate==null&&StartDate.equals(""))
        {
            buildError("getInputData", "没有起始日期!");
            return false;
        }
        if (EndDate==null&&EndDate.equals(""))
        {
            buildError("getInputData", "没有终止日期!");
            return false;
        }
        if (BatchNo==null&&BatchNo.equals(""))
        {
            buildError("getInputData", "没有批次号码!");
            return false;
        }
        return true;
    }

    private boolean InitParameter()
    {
        try
        {
            mErrors = new CErrors();
            mLIAboriginalDataSet = null;
            mLIDataTransResultSet = null;
            mLIDistillInfoSet = null;
            mLIDistillErrSet = null;
            mLIDistillLogSet = null;
            mGlobalInput = new GlobalInput();
            StartDate = "";
            EndDate = "";
            BatchNo ="";
            return true;
        }
        catch (Exception ex)
        {
            buildError("InitParameter",ex.getMessage());
           return false;
        }
    }

    private LIAboriginalDataSet getDealDataBySql(String sql) throws Exception
    {
        LIAboriginalDataSet tLIAboriginalDataSet = new LIAboriginalDataSet();
        try
        {
            SSRS tSSRS = new SSRS();
            ExeSQL tExeSQL = new ExeSQL();
            sql = "select * from (" + sql + ") as tab1  fetch first " + mMaxDealNUm+" rows only with ur";
 //            System.out.println("提取数据" + sql);
            tSSRS = tExeSQL.execSQL(sql);
            //判断是否有记录如果没有记录则返回空
            if(tSSRS.getMaxRow()>0)
            {
                aSerailNo = FinCreateSerialNo.getSerialNoByFINDATA(tSSRS.getMaxRow());
            }
            for (int i=1;i<=tSSRS.getMaxRow();i++)
            {
               LIAboriginalDataSchema tLIAboriginalDataSchema = new  LIAboriginalDataSchema();
               tLIAboriginalDataSchema.setSerialNo(aSerailNo[i-1]);
               tLIAboriginalDataSchema.setBatchNo(BatchNo);
               tLIAboriginalDataSchema.setClassID(mLIOperationDataClassDefSchema.getClassID());
               tLIAboriginalDataSchema.setTempFeeNo(tSSRS.GetText(i, 1));
               tLIAboriginalDataSchema.setActuGetNo(tSSRS.GetText(i, 2));
               tLIAboriginalDataSchema.setPayNo(tSSRS.GetText(i, 3));
               tLIAboriginalDataSchema.setEdorAcceptNo(tSSRS.GetText(i, 4));
               tLIAboriginalDataSchema.setEndorsementNo(tSSRS.GetText(i, 5));
               tLIAboriginalDataSchema.setCaseNo(tSSRS.GetText(i, 6));
               tLIAboriginalDataSchema.setFeeOperationType(tSSRS.GetText(i, 7));
               tLIAboriginalDataSchema.setFeeFinaType(tSSRS.GetText(i, 8));
               tLIAboriginalDataSchema.setListFlag(tSSRS.GetText(i, 9));
               tLIAboriginalDataSchema.setGrpContNo(tSSRS.GetText(i, 10));
               tLIAboriginalDataSchema.setGrpPolNo(tSSRS.GetText(i, 11));
               tLIAboriginalDataSchema.setContNo(tSSRS.GetText(i, 12));
               tLIAboriginalDataSchema.setPolNo(tSSRS.GetText(i, 13));
               tLIAboriginalDataSchema.setPayIntvFlag(tSSRS.GetText(i, 14));
               tLIAboriginalDataSchema.setPayIntv(tSSRS.GetText(i, 15));
               tLIAboriginalDataSchema.setPayDate(tSSRS.GetText(i, 16));
               tLIAboriginalDataSchema.setPayType(tSSRS.GetText(i, 17));
               tLIAboriginalDataSchema.setEnterAccDate(tSSRS.GetText(i, 18));
               tLIAboriginalDataSchema.setConfDate(tSSRS.GetText(i, 19));
               tLIAboriginalDataSchema.setLastPayToDate(tSSRS.GetText(i, 20));
               tLIAboriginalDataSchema.setCurPayToDate(tSSRS.GetText(i, 21));
               tLIAboriginalDataSchema.setCValiDate(tSSRS.GetText(i, 22));
               tLIAboriginalDataSchema.setPolYear(tSSRS.GetText(i, 23));
               tLIAboriginalDataSchema.setFirstYearFlag(tSSRS.GetText(i, 24));
               tLIAboriginalDataSchema.setPayCount(tSSRS.GetText(i, 25));
               tLIAboriginalDataSchema.setFirstTermFlag(tSSRS.GetText(i, 26));
               tLIAboriginalDataSchema.setYears(tSSRS.GetText(i, 27));
               tLIAboriginalDataSchema.setRiskCode(tSSRS.GetText(i, 28));
               tLIAboriginalDataSchema.setRiskPeriod(tSSRS.GetText(i, 29));
               tLIAboriginalDataSchema.setRiskType(tSSRS.GetText(i, 30));
               tLIAboriginalDataSchema.setRiskType1(tSSRS.GetText(i, 31));
               tLIAboriginalDataSchema.setSaleChnl(tSSRS.GetText(i, 32));
               tLIAboriginalDataSchema.setManageCom(tSSRS.GetText(i, 33));
               tLIAboriginalDataSchema.setExecuteCom(tSSRS.GetText(i, 34));
               tLIAboriginalDataSchema.setAgentCom(tSSRS.GetText(i, 35));
               tLIAboriginalDataSchema.setAgentCode(tSSRS.GetText(i, 36));
               tLIAboriginalDataSchema.setAgentGroup(tSSRS.GetText(i, 37));
               tLIAboriginalDataSchema.setBankCode(tSSRS.GetText(i, 38));
               //System.out.println("111111:"+tSSRS.GetText(i, 38));
               tLIAboriginalDataSchema.setAccName(tSSRS.GetText(i, 39));
               tLIAboriginalDataSchema.setBankAccNo(tSSRS.GetText(i, 40));
               //System.out.println("111111:"+tSSRS.GetText(i, 40));
               tLIAboriginalDataSchema.setSumDueMoney(tSSRS.GetText(i, 41));
               //System.out.println("111111:"+tSSRS.GetText(i, 41));
               tLIAboriginalDataSchema.setSumActuMoney(tSSRS.GetText(i, 42));
               //System.out.println("111111:"+tSSRS.GetText(i, 42));
               tLIAboriginalDataSchema.setCurrency(tSSRS.GetText(i, 43));
               tLIAboriginalDataSchema.setPayMode(tSSRS.GetText(i, 44));
               tLIAboriginalDataSchema.setOperationType(tSSRS.GetText(i, 45));
               tLIAboriginalDataSchema.setBudget(tSSRS.GetText(i, 46));
               tLIAboriginalDataSchema.setSaleChnlDetail(tSSRS.GetText(i, 47));
               tLIAboriginalDataSchema.setStandByString1(tSSRS.GetText(i, 48));
               tLIAboriginalDataSchema.setStandByString2(tSSRS.GetText(i, 49));
               tLIAboriginalDataSchema.setStandByString3(tSSRS.GetText(i, 50));
               tLIAboriginalDataSchema.setStandByNum1(tSSRS.GetText(i, 51));
               tLIAboriginalDataSchema.setStandByNum2(tSSRS.GetText(i, 52));
               tLIAboriginalDataSchema.setStandByDate1(tSSRS.GetText(i, 53));
               tLIAboriginalDataSchema.setStandByDate2(tSSRS.GetText(i, 54));
               tLIAboriginalDataSchema.setBClient(tSSRS.GetText(i, 55));
               tLIAboriginalDataSchema.setMarketType(tSSRS.GetText(i, 56));
               tLIAboriginalDataSchema.setPremiumType(tSSRS.GetText(i, 57));
               tLIAboriginalDataSchema.setFirstYear(tSSRS.GetText(i, 58));
               tLIAboriginalDataSet.add(tLIAboriginalDataSchema);
            }
        }
        catch(Exception ex)
        {
            throw ex;
        }
        return tLIAboriginalDataSet;
    }

    private boolean DistillData()
    {
        try{
            String tDistillMode = mLIOperationDataClassDefSchema.getDistillMode();
            if (tDistillMode == null && tDistillMode.equals("")) {
                buildError("distillData",
                           "类型为" + mLIOperationDataClassDefSchema.getClassID() +
                           "数据提取方式未定义!");
                return false;
            }
            if (tDistillMode.equals("1")) {
                PubCalculator tPubCalculator = new PubCalculator();
                tPubCalculator.addBasicFactor("ManageCom",
                                              mGlobalInput.ManageCom);
                tPubCalculator.addBasicFactor("StartDate", StartDate);
                tPubCalculator.addBasicFactor("EndDate", EndDate);
                String sql = mLIOperationDataClassDefSchema.getDistillSQL();
                if (sql == null && sql.equals("")) {
                    buildError("distillData",
                               "类型为" +
                               mLIOperationDataClassDefSchema.getClassID() +
                               "数据提取SQL未定义!");
                    return false;
                }
                tPubCalculator.setCalSql(sql);
//                System.out.println("类型为" + mLIOperationDataClassDefSchema.getClassID() + "提数：" + sql);
                sql = tPubCalculator.calculateEx();
//                System.out.println("类型为" + mLIOperationDataClassDefSchema.getClassID() + "计算后的提数：" + sql);
                mLIAboriginalDataSet = getDealDataBySql(sql);
            }
            else if(tDistillMode.equals("2"))
            {
                Class tClass = Class.forName(mLIOperationDataClassDefSchema.getDistillClass());
                DistillType mDistillTypeClass = (DistillType) tClass.newInstance();
                VData sVData = new VData();
                sVData.add(mGlobalInput);
                sVData.add(StartDate);
                sVData.add(EndDate);
                sVData.add(BatchNo);
                mLIAboriginalDataSet = mDistillTypeClass.DitillInfo(sVData);
            }
        }
        catch(Exception ex)
        {
            buildError("distillData","错误信息:" + ex.getMessage());
        	return false;
        }
        return true;
    }

    private boolean DealWithData()
    {
        try
        {
            mLIDataTransResultSet = new  LIDataTransResultSet();
            mLIDistillInfoSet = new  LIDistillInfoSet();
            dSerailNo = FinCreateSerialNo.getSerialNo(mLIAboriginalDataSet.size() * mLITransInfoDefSet.size());

            for(int i =1;i<=mLIAboriginalDataSet.size();i++)
            {
                String tSerailNo[] = new String[mLITransInfoDefSet.size()];
                for(int j=0;j<mLITransInfoDefSet.size();j++)
                {
                    tSerailNo[j] = dSerailNo[(i-1)*mLITransInfoDefSet.size()+j];
                }
                mLIDataTransResultSet.add(this.DealOperationDataInfo(mLIAboriginalDataSet.get(i),tSerailNo));
                mLIDistillInfoSet.add(this.DealOperationDataLog(mLIAboriginalDataSet.get(i)));
            }

            return true;
        }
        catch (Exception ex)
        {
        	buildError("DealWithData",ex.getMessage());
        	return false;
        }
    }

    private boolean SaveData()
    {
       try
       {
           VData mInputData = new VData();
           mInputData.add(mLIAboriginalDataSet);
           mInputData.add(mLIDataTransResultSet);
           mInputData.add(mLIDistillInfoSet);
           mInputData.add(mLIDistillErrSet);
           mInputData.add(mLIDistillLogSet);
           DataToSubmit tDataToSubmit = new  DataToSubmit();
           if(!tDataToSubmit.submitData(mInputData,""))
           {
               this.mErrors.copyAllErrors(tDataToSubmit.mErrors);
               return false;
           }
           return true;
       }
       catch (Exception ex) {
    	   buildError("SaveData",ex.getMessage());
    	   return false;
       }
    }

    public static void main(String[] args) {
//
//        FinDataType tFinDataType = new  FinDataType();
//        if(!tFinDataType.InitFinDataTypeInfo("L000001"))
//        {
//            System.out.println("111111111");
//            System.out.println("222222222");
//            System.out.println("3333333333");
//        }
//        else{
//
//        }
//    }
    	LIAboriginalDataSchema mLIAboriginalDataSchema = new LIAboriginalDataSchema();
    	mLIAboriginalDataSchema.setTempFeeNo("111");
    	mLIAboriginalDataSchema.setActuGetNo("22");
    	mLIAboriginalDataSchema.setCaseNo("555");
    	String tOut = getErrorLinkInfo(mLIAboriginalDataSchema);
    	System.out.println(tOut);
    }

}
