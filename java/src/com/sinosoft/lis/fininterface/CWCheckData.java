package com.sinosoft.lis.fininterface;

import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LIDataTransResultDB;
import com.sinosoft.lis.vschema.LIDataTransResultSet;

/**
 * 财务接口核对信息查询处理类
 * 按照科目信息或者业务号码来核对相关数据
 */
public class CWCheckData
{

	/**
	 * 通过科目信息查询 额外条件：起始日期---结束日期、机构
	 * 需要核对的信息
	 * 业务类型、科目代码、渠道信息、险种、金额、成本中心、业务日期、管理机构、批次号码
	 */
	public LIDataTransResultSet queryByAccountCode(String sAccountCode, String strStartDate, String strEndDate,
			String strComCode)
	{

		LIDataTransResultSet oDataTransResultSet = new LIDataTransResultSet();
		try
		{
			String strSQL = "select * from lidatatransresult where trim(accountcode) = trim('" + sAccountCode + "')";

			/*******************************************************************
			 * 条件组合判断生成SQL
			 */
			if (strStartDate != null && !strStartDate.equals(""))
			{
				strSQL += strSQL + " and accountdate >= '" + strStartDate + "'";
			}
			if (strEndDate != null && !strEndDate.equals(""))
			{
				strSQL += strSQL + " and accountdate <= '" + strEndDate + "'";
			}
			if (strComCode != null && !strComCode.equals(""))
			{
				strSQL += strSQL + " and managecom like '" + strComCode + "%'";
			}
			LIDataTransResultDB oDataTransResultDB = new LIDataTransResultDB();

			oDataTransResultSet = oDataTransResultDB.executeQuery(strSQL);
		}
		catch (Exception e)
		{
			e.printStackTrace();

		}
		return oDataTransResultSet;

	}

	/***************************************************************************
	 * 通过业务号码查询 参数：strOtherno 表示查询的业务号码 strNoType
	 * 表示该业务号码的业务类型[新契约、保全、理赔、渠道(代理人代码)] 
	 * 1 新契约投保单号码 2 新契约保单号码 3 保全受理号码 4 保全批单号码
	 * 5 赔案号码 6 代理人代码 7 结算号码[暂不用]
	 */
	public LIDataTransResultSet queryByOtherNo(String strOtherNo, String strNoType)
	{

		LIDataTransResultSet oDataTransResultSet = new LIDataTransResultSet();
		String strSQL = "select * from lidatatransresult where 1=1 ";
		if (strNoType != null)
		{
			if (strNoType.equals("1"))
			{
				// 新契约对应的号码 需要反查对应的保单号码
				LCContDB oContDB = new LCContDB();
				oContDB.setPrtNo(strOtherNo);
				if (!oContDB.getInfo())
				{
					// 没有找到该投保单号码
				}
				String strConNo = oContDB.getContNo();
				if (strConNo.trim().equals(strOtherNo.trim()))
				{
					strSQL += strSQL + " and trim(contno) = trim('" + strOtherNo + "')";
				}
				else
				{
					strSQL += strSQL + " and trim(contno) = trim('" + strOtherNo + "') or trim(contno) = trim('"
							+ strConNo + "')";
				}
			}
			if (strNoType.equals("2"))
			{
				// 方案同上 lccont
			}
			if (strNoType.equals("3"))
			{

				// 保全 通过反查查到对应的批单号码来核对这些数据 lpedoritem

			}
			if (strNoType.equals("4"))
			{

				// 保全 通过反查查到对应的批单号码来核对这些数据 lpedoritem

			}
			if (strNoType.equals("5"))
			{

				// 理赔 通过反查查到对应的 llcasedetail

			}
			if (strNoType.equals("6"))
			{

				// 代理人相关信息 针对佣金数据的核对

			}

		}

		return oDataTransResultSet;
	}
	
	public static void main(String args[]){
		
		CWCheckData checkFinInterfaceInfo = new CWCheckData();

		LIDataTransResultSet oDataTransResultSet = new LIDataTransResultSet();
		oDataTransResultSet = checkFinInterfaceInfo.queryByAccountCode("11210103", "", "", "8643");
//		oDataTransResultSet = checkFinInterfaceInfo.queryByOtherNo("9000001", "5");
		
		System.out.println("End");
		
		
		
	}
}
