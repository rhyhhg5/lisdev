package com.sinosoft.lis.fininterface;

import java.io.File;

import com.sinosoft.lis.f1print.GetSQLFromXML;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


import jxl.*;
import jxl.write.*;
import jxl.write.biff.RowsExceededException;
import jxl.format.UnderlineStyle;

public class CompetiThreeBL
{
	public  CErrors mErrors=new CErrors();
	private VData mInputData= new VData();
	private TransferData tTransferData = new TransferData();
	/** 全局数据 */
	private GlobalInput mGlobalInput =new GlobalInput() ;
	/** 数据操作字符串 */

    private String mOperater;
    private String mManageCom;
    private String Bdate;
    private String Edate;
    private String YBdate;
    private String YEdate;
    private String tPath;
    
	public CompetiThreeBL()
	{}
    
    
    public boolean submitData(VData cInputData,String path) throws RowsExceededException, WriteException,Exception
	{
		//将操作数据拷贝到本类中
		this.mInputData = cInputData;
		this.tPath=path;
		System.out.println("---UpLoadWriteFinBL getInputData---");
		
		if (!getInputData(cInputData)) {
	            return false;
	        }
	      
		if (!checkData())
		{
			return false;
		}

		//进行业务处理
		if (!WriteXls()) 
		{
			return false;
		}
		System.out.println("End UpLoadWriteFinBL Submit...");
		mInputData=null;
		return true;
	}
    
    
    /**
     * 创建Excel
     * @throws WriteException 
     * @throws RowsExceededException 
     */
    public boolean WriteXls()
    {
    	
        
    	WritableWorkbook book=null;
			try {
				
				//建新的Excel
				System.out.println(tPath+"ModelExcel-2013.xls");
				System.out.println("fin-"+mOperater.trim()+"-2013.xls");
				book= Workbook.createWorkbook(new File(tPath+"fin-"+mOperater.trim()+"-2013.xls"),Workbook.getWorkbook(new File(tPath+"ModelExcel-2013.xls")));
				
				//获取Excel工作薄
				WritableSheet sh1=book.getSheet(0);
				WritableSheet sh2=book.getSheet(1);
				
				//获取查询sql
				String str1=getSQL("SJFGS");
				String str2=getSQL("DSJJG");
                				
				//定义单元格格式
				WritableFont wfc = new WritableFont(WritableFont.ARIAL, 10,
				WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				jxl.format.Colour.BLACK); // 定义格式 字体 下划线 斜体 粗体 颜色
				WritableCellFormat wcft = new WritableCellFormat(wfc); // 单元格定义
				//wcft.setBackground(jxl.format.Colour.WHITE); // 设置单元格的背景颜色
				wcft.setAlignment(jxl.format.Alignment.LEFT); // 设置对齐方式   
				wcft.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
				wcft.setBorder(jxl.format.Border.ALL,jxl.format.BorderLineStyle.THIN); 
				
				System.out.println("str1:"+str1);
				System.out.println("str2:"+str2);
				
				//写入2013年省级分公司业务数据
				WriteSheet(sh1,str1,19,6,wcft,1);
				
				//写入2013年地市级机构业务数据
				WriteSheet(sh2,str2,21,6,wcft,3);
								
				//写入
				book.write();			
				book.close();
				
	        	System.out.println("生成报表成功！");
				return true;
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				bulidErr("WriteXls","生成报表失败!原因是"+e.toString());
		        return false;
			}

            
    }
//数据写入Excel中    
    private boolean WriteSheet(WritableSheet sheet,String sql,int x,int y,WritableCellFormat wf,int FirstSum) throws RowsExceededException, WriteException,Exception
    {
    	try{
    	ExeSQL es = new ExeSQL();
    	SSRS mSSRS = new SSRS();
    	mSSRS = es.execSQL(sql);
    	int maxrow=mSSRS.getMaxRow();
    	for(int a=0;a<x;a++)
    	{
    		for(int i=y;i<=(maxrow+y-1);i++)
	    	{ 
	    		sheet.addCell(new Label(a,(i-1),mSSRS.GetText((i-y+1), a+1),wf));
	    	}
    	}
    	WriteSheetSum(sheet,maxrow,x,y,wf,FirstSum);
    		
    	return true;
    	}catch(Exception e){
    		Exception cv =  new Exception("写入excel文件失败"+e.getMessage());
   		    throw cv;
    	}
    }
//写入合计值
    private boolean WriteSheetSum(WritableSheet sheet,int maxrow,int x,int y,WritableCellFormat wf,int FirstSum) throws Exception
    {
    	try{
    	sheet.addCell(new Label(0,(y+maxrow-1),"合计",wf));
    	for(int i=1;i<x;i++){
    	if(i<FirstSum) {
    		sheet.addCell(new Label(i,(y+maxrow-1),"",wf));
    		continue;
    	}
    	String cellCont=sheet.getCell(i,y-1).getContents();
    	if(cellCont==null || cellCont.equals("")){
    	   sheet.addCell(new Label(i,(y+maxrow-1),"",wf));
    	}else{
    		double sum=0;
    		for(int a1=(y-1);a1<(y+maxrow-1);a1++){
    			sum=sum+Double.parseDouble(sheet.getCell(i,a1).getContents().trim());
    		}
    		sum=Math.round(sum*100)/100.0;
    		System.out.println("sum="+sum);
    		sheet.addCell(new Label(i,(y+maxrow-1),Double.toString(sum),wf));
    	}
    	}
    	}catch(Exception e)
    	{
    		 Exception cv =  new Exception("写入合计值失败"+e.getMessage());
    		 throw cv;
    	}
    	return true;
    }
  
    private boolean getInputData(VData cInputData)
	{
		mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0);
		tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData",0);
		if (mGlobalInput == null) {
            // @@错误处理
            bulidErr("getInputData", "前台传输全局公共数据失败!");
            return false;
        }

        //获得操作员编码
		mOperater = mGlobalInput.Operator;
        if ((mOperater == null) || mOperater.trim().equals("")) {
            // @@错误处理
        	bulidErr("getInputData", "前台传输全局公共数据Operate失败!");
            return false;
        }

        //获得登陆机构编码
        mManageCom = mGlobalInput.ManageCom;
        if ((mManageCom == null) || mManageCom.trim().equals("")) {
            // @@错误处理
        	bulidErr("getInputData", "前台传输全局公共数据ManageCom失败!");
            return false;
        }
        Bdate=tTransferData.getValueByName("Bdate").toString();
        if ((Bdate == null) || Bdate.trim().equals("")) {
            // @@错误处理
        	bulidErr("getInputData", "前台传输保费起始日期失败!");
            return false;
        }
        Edate=tTransferData.getValueByName("Edate").toString();
        if ((Edate == null) || Edate.trim().equals("")) {
            // @@错误处理
        	bulidErr("getInputData", "前台传输保费终止日期失败!");
            return false;
        }
        YBdate=tTransferData.getValueByName("YBdate").toString();
        if ((YBdate == null) || YBdate.trim().equals("")) {
            // @@错误处理
        	bulidErr("getInputData", "前台传输犹豫期退保起始日期失败!");
            return false;
        }
        YEdate=tTransferData.getValueByName("YEdate").toString();
        if ((YEdate == null) || YEdate.trim().equals("")) {
            // @@错误处理
        	bulidErr("getInputData", "前台传输犹豫期退保终止日期失败!");
            return false;
        }
        
		return true;
	}
    
	private boolean checkData()
	{
		return true;
	}
    private String getSQL(String sqlType) {
        GetSQLFromXML tGetSQLFromXML = new GetSQLFromXML();

        tGetSQLFromXML.setParameters("Bdate", Bdate);
        tGetSQLFromXML.setParameters("Edate", Edate);
        tGetSQLFromXML.setParameters("YBdate", YBdate);
        tGetSQLFromXML.setParameters("YEdate", YEdate);
        
        String tServerPath = (new ExeSQL()).getOneValue("select sysvarvalue from LDSysVar where sysvar='ServerRoot'");
        //tServerPath= "D:/workspace/picc/WebContent/";
        String tSQL=tGetSQLFromXML.getSql(tServerPath+"f1print/picctemplate/CompetitySql.xml", sqlType);
		return tSQL;
	}
	 private void bulidErr(String functionName,String errorMessage) {
		 CError tError = new CError();
         tError.moduleName = "CompetiThreeBL";
         tError.functionName = functionName;
         tError.errorMessage = errorMessage;
         this.mErrors.addOneError(tError);
	}
	
    /**
     * 测试
     * @param args
     * @throws WriteException 
     * @throws RowsExceededException 
     */
    public static void main(String[] args)
    {
    	CompetiThreeBL c=new CompetiThreeBL();
    	c.mOperater="cwjk";
    	c.Bdate="2012-10-01";
    	c.Edate="2012-10-31";
    	c.YBdate="2012-11-01";
    	c.YEdate="2012-11-10";
    	c.tPath="D:/workspace/picc/WebContent/fininterface/";
    	//c.WriteXls();
    }
}
