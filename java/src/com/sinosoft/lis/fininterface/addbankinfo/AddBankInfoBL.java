package com.sinosoft.lis.fininterface.addbankinfo;

import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LDFinBankDB;
import com.sinosoft.lis.db.LICodeTransDB;
import com.sinosoft.lis.db.LIDetailFinItemCodeDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.schema.LDFinBankSchema;
import com.sinosoft.lis.schema.LICodeTransSchema;
import com.sinosoft.lis.schema.LIDetailFinItemCodeSchema;
import com.sinosoft.lis.vschema.LDFinBankSet;
import com.sinosoft.lis.vschema.LIDetailFinItemCodeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

 

public class AddBankInfoBL {

	private LDFinBankDB mLDFinBankDB = new LDFinBankDB();
	private LDCodeDB mLDCodeDB= new LDCodeDB();
	private LICodeTransDB mLICodeTransDB = new LICodeTransDB();
	private LIDetailFinItemCodeDB mLIDetailFinItemCodeDB = new LIDetailFinItemCodeDB();
	
	private LDFinBankSchema mLDFinBankSchema = new LDFinBankSchema();
	private LDCodeSchema mLDCodeSchema = new LDCodeSchema();
	private LICodeTransSchema mLICodeTransSchema = new LICodeTransSchema();
	private LIDetailFinItemCodeSchema mLIDetailFinItemCodeSchema = new LIDetailFinItemCodeSchema();
	

	public CErrors mErrors = new CErrors();

	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();
	private String mOperate = "";
	private String mBankAccNo = "";
	private String mCode = "";
	private boolean OneAccNo = true;
	private boolean OneCode = false;


	// 测试用
	public static void main(String[] args) {
		//AddBankInfoBL abi = new AddBankInfoBL();
 
	}
	
 
	
	/**
	 * 查看要操作的数据是否在数据库中存在。
	 * @return
	 */
	public boolean isExist(){
		
		mLDFinBankDB.setSchema(mLDFinBankSchema);
		mLDCodeDB.setSchema(mLDCodeSchema);
		mLICodeTransDB.setSchema(mLICodeTransSchema);
		
		mLIDetailFinItemCodeDB.setSchema(mLIDetailFinItemCodeSchema);
		
		System.out.println(mLIDetailFinItemCodeSchema.getLevelCondition());
		System.out.println(mLDFinBankSchema.getBankName());
		if( mLDFinBankDB.query().size() == 0 && mLDCodeDB.query().size() == 0 
				&& mLICodeTransDB.query().size()==0 
				&& mLIDetailFinItemCodeDB.query().size() ==0
				){
			return false;
		}
		LDFinBankDB tLDFinBankDB = new LDFinBankDB();
		LDFinBankSet tLDFinBankSet = new LDFinBankSet();
		tLDFinBankDB.setBankAccNo(mBankAccNo);
		tLDFinBankSet = tLDFinBankDB.query();
		if(tLDFinBankSet.size()>1)
		{
			OneAccNo = false;
		}
		
		LIDetailFinItemCodeDB tLIDetailFinItemCodeDB = new LIDetailFinItemCodeDB();
		LIDetailFinItemCodeSet tLIDetailFinItemCodeSet = new LIDetailFinItemCodeSet();
		tLIDetailFinItemCodeDB.setLevelCode(mCode.substring(4,4+6));
		tLIDetailFinItemCodeSet = tLIDetailFinItemCodeDB.query();
		if(tLIDetailFinItemCodeSet.size()==1)
		{
			OneCode = true;
		}
		
//		String sql = "select * from Lidetailfinitemcode where FinItemID = '0000003' and FinItemlevel = '2' and JudgementNo = '02' and LevelCondition ='"+mLIDetailFinItemCodeDB.getLevelCondition()+"' with ur";
		return true;
	}
	
  
	/**
	 * 提交处理数据。
	 * @param cInputData-被VData封装的数据。
	 * @param cOperate-操作类别， "insert" "delete" "update"
	 * @return true-处理成功 false-失败
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		 
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!dealDate(cOperate)) {
			CError tError = new CError();
			tError.moduleName = "AddBankInfoBL";
			tError.functionName = "submitData";
			tError.errorMessage = "数据处理失败-->dealData!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 根据cOperate操作，进行处理。
	 * @param cOperate-操作动作
	 * @return true-操作成功 false-操作失败
	 */
	private boolean dealDate(String cOperate) {
		
		if(cOperate.equals("insert")){
			return doInsert();
		}
		else if(cOperate.equals("delete")){
			return doDelete();
		}
		else if(cOperate.equals("update")){
	//		return beforeUpdate();
	//		return doUpdate();
		}
		return false;
	}
 
	
	
	/**
	 * 更新数据
	 * @return true-更新成功 false-更新失败
	 */
	private boolean doUpdate(){
		if( isExist()){
			
			
			LDFinBankSet tLDFinBankSet = mLDFinBankDB.query();
			
			LDFinBankSchema tempLDFinBankSchema = tLDFinBankSet.get(0);
			System.out.println("makeDate:"+tempLDFinBankSchema.getMakeDate());
			System.out.println("makeTime:"+tempLDFinBankSchema.getMakeTime());
			mLDFinBankSchema.setMakeDate(tempLDFinBankSchema.getMakeDate());
			mLDFinBankSchema.setMakeTime(tempLDFinBankSchema.getMakeTime());
			mLDFinBankSchema.setModifyDate(PubFun.getCurrentDate());
			mLDFinBankSchema.setModifyTime(PubFun.getCurrentTime());
			mLDFinBankDB.setSchema(mLDFinBankSchema);
			
			CError tError = new CError();
			tError.moduleName = "AddBankInfoBL";
			tError.functionName = "deleteData";
			if( !mLDFinBankDB.update()){
				tError.errorMessage = " 数据更新失败-->LDFinBank!";
				this.mErrors.addOneError(tError);
				return false;			
			}
			if( !mLDCodeDB.update()){
				tError.errorMessage = " 数据更新失败-->LDFinBank!";
				this.mErrors.addOneError(tError);
				return false;			
			}
			if( !mLICodeTransDB.update()){
				tError.errorMessage = " 数据更新失败-->LDFinBank!";
				this.mErrors.addOneError(tError);
				return false;			
			}
			if( !mLIDetailFinItemCodeDB.update()){
				tError.errorMessage = " 数据更新失败-->LDFinBank!";
				this.mErrors.addOneError(tError);
				return false;			
			}
		}
		return false;
	}
	
	/**
	 * 删除数据
	 * @return -true 删除成功 false- 删除失败
	 */
	private boolean doDelete(){
		if( isExist()){

			CError tError = new CError();
			tError.moduleName = "AddBankInfoBL";
			tError.functionName = "deleteData";
//			System.out.println("-----fakjkajlfakjdug"+mLDFinBankDB.getManageCom());
			if( !mLDFinBankDB.delete() ){
				tError.errorMessage = " 数据删除失败-->LDFinBank!";
				this.mErrors.addOneError(tError);
				return false;
			}
			if(OneAccNo)//当银行账号只有一条数据的时候进行全部删除
			{
				if( !mLIDetailFinItemCodeDB.delete()){
					tError.errorMessage = " 数据删除失败-->LIDetailFinItemCode!";
					this.mErrors.addOneError(tError);
					return false;
				}
			}
			if(OneCode)
			{
			if( !mLDCodeDB.delete()){
				tError.errorMessage = " 数据删除失败-->LDCode!";
				this.mErrors.addOneError(tError);
				return false;
			}
			if( !mLICodeTransDB.delete()){
				tError.errorMessage = " 数据删除失败-->LICodeTrans!";
				this.mErrors.addOneError(tError);
				return false;
			}
			}
		}
		return true;
	}
	
	/**
	 * 数据插入
	 * @return true-插入成功 false-插入失败
	 */
	private boolean doInsert(){
//		if( !isExist() ){
//		mLDFinBankDB.setSchema(mLDFinBankSchema);
//		mLDCodeDB.setSchema(mLDCodeSchema);
//		mLICodeTransDB.setSchema(mLICodeTransSchema);
//		mLIDetailFinItemCodeDB.setSchema(mLIDetailFinItemCodeSchema);
		
			System.out.println("开始插入.");
			
			mLDFinBankSchema.setMakeDate(PubFun.getCurrentDate());
			mLDFinBankSchema.setMakeTime(PubFun.getCurrentTime());
			mLDFinBankDB.setSchema(mLDFinBankSchema);
			mLDCodeDB.setSchema(mLDCodeSchema);
			mLIDetailFinItemCodeDB.setSchema(mLIDetailFinItemCodeSchema);
			mLICodeTransDB.setSchema(mLICodeTransSchema);
		
			String sql = "select * from LIDetailFinItemCode where FinItemID='0000003' and JudgementNo='02' and LevelCondition='"+mLIDetailFinItemCodeSchema.getLevelCondition()+"' with ur";
			System.out.println("执行SQL: " + sql);
			if(mLIDetailFinItemCodeDB.executeQuery(sql).size() == 0){
				if(!mLIDetailFinItemCodeDB.insert()){
					System.out.println("insert mLIDetailFinItemCodeDB fail.");
					return false;
				}
			}
			
			sql = "select * from LDFinBank where managecom='"+mLDFinBankSchema.getManageCom()+"' and FinFlag='"+mLDFinBankSchema.getFinFlag()+"' and bankaccno='"+mLDFinBankSchema.getBankAccNo()+"'";
			System.out.println("执行SQL: " + sql);
			if(mLDFinBankDB.executeQuery(sql).size() == 0){
				if(!mLDFinBankDB.insert()){
					System.out.println("insert mLDFinBankDB fail.");
					return false;
				}
			}
			
			sql = "select * from LDCode where CodeType='accountcode' and Code='"+mLDCodeSchema.getCode()+"' and OtherSign='1'";
			System.out.println("执行SQL: " + sql);
			if(mLDCodeDB.executeQuery(sql).size() == 0){
				if(!mLDCodeDB.insert()){
					System.out.println("insert mLDCodeDB fail.");
					return false;
				}
			}
			sql = "select * from LICodeTrans where CodeType='accountcode' and Code ='"+mLICodeTransSchema.getCode()+"' and OtherSign='1'";
			System.out.println("执行SQL: " + sql);
			if(mLICodeTransDB.executeQuery(sql).size() == 0){
				if(!mLICodeTransDB.insert()){
					System.out.println("insert mLICodeTransDB fail.");
					return false;
				}
			}
//			if( mLDFinBankDB.insert() && mLDCodeDB.insert() && mLIDetailFinItemCodeDB.insert()
//					&& mLICodeTransDB.insert() ){
//				System.out.println("插入成功.");
//				return true;
//			}else{
//				CError tError = new CError();
//				tError.moduleName = "AddBankInfoBL";
//				tError.functionName = "submitData";
//				tError.errorMessage = "插入失败。-->dealData!";
//				this.mErrors.addOneError(tError);
//				return false;
//			}
//		}
			
		return true;
	}

	/**
	 * 取出从前台中VData中传来的数据，并赋值给本类。
	 * @param cInputData
	 * @return
	 */
	private boolean getInputData(VData cInputData) {
		try {
			mLDFinBankSchema = (LDFinBankSchema) cInputData.getObjectByObjectName("LDFinBankSchema", 0);
			mLDCodeSchema = (LDCodeSchema) cInputData.getObjectByObjectName("LDCodeSchema", 0);
			mLIDetailFinItemCodeSchema = (LIDetailFinItemCodeSchema) cInputData.getObjectByObjectName("LIDetailFinItemCodeSchema", 0);
			mLICodeTransSchema = (LICodeTransSchema) cInputData.getObjectByObjectName("LICodeTransSchema", 0);
			mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
			
			mBankAccNo = (String)cInputData.get(5);//删除机构对应银行账号时校验用
			mCode = (String)cInputData.get(6);//删除科目时校验用
			
			System.out.println("mBankAccNo:"+mBankAccNo);
			System.out.println("mCode:"+mCode);
			
			if (mLDFinBankSchema == null || mGlobalInput == null || mLDCodeSchema==null || 
					mLIDetailFinItemCodeSchema==null || mLICodeTransSchema==null ) {
				CError tError = new CError();
				tError.moduleName = "AddBankInfoBL";
				tError.functionName = "getInputData";
				tError.errorMessage = "没有得到足够的信息！";
				this.mErrors.addOneError(tError);
				return false;
			}
			if (mLDFinBankSchema.getManageCom().equals("")
					|| mLDFinBankSchema.getBankCode().equals("")
					|| mLDFinBankSchema.getBankName().equals("")
					|| mLDFinBankSchema.getBankCode().equals("")
					|| mLDFinBankSchema.getFinFlag().equals("")) {
				CError tError = new CError();
				tError.moduleName = "AddBankInfoBL";
				tError.functionName = "getInputData";
				tError.errorMessage = "没有得到足够的信息！";
				this.mErrors.addOneError(tError);
				return false;
			}
			return true;
		} catch (Exception e) {
			CError.buildErr(this, "接收数据失败");
			return false;
		}
	}

}
