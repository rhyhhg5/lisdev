package com.sinosoft.lis.fininterface;

import com.sinosoft.lis.vschema.LIDataKeyDefSet;
import com.sinosoft.lis.db.LIDataKeyDefDB;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.LIAboriginalDataSchema;
import com.sinosoft.lis.schema.LIDistillInfoSchema;
import com.sinosoft.lis.schema.LIDataKeyDefSchema;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LIDistillErrSchema;

/**
 * <p>Title: LogDeal</p>
 *
 * <p>Description: 日志处理类</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft </p>
 *
 * @author jianan
 * @version 1.0
 */
public class LogDeal {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 数据主键定义表*/
    private LIDataKeyDefSet mLIDataKeyDefSet;

    public LogDeal() {
    }

    public boolean InitLogDealInfo(String ClassID)
    {
        LIDataKeyDefDB tLIDataKeyDefDB = new  LIDataKeyDefDB();
        String Sql =  "select * from LIDataKeyDef where ClassID = '" + ClassID + "' order by KeyOrder";
        mLIDataKeyDefSet = tLIDataKeyDefDB.executeQuery(Sql);
        if(tLIDataKeyDefDB.mErrors.needDealError())
        {
            buildError("InitLogDealInfo函数",
                       "数据类型编码为" + ClassID + "的LIDataKeyDef表查询出错");
                 return false;
        }
        if(mLIDataKeyDefSet.size()<=0)
        {
            buildError("InitLogDealInfo函数",
                       "数据类型编码为" + ClassID + "的LIDataKeyDef表无定义");
                 return false;
       }
       LIAboriginalDataSchema tLIAboriginalDataSchema = new  LIAboriginalDataSchema();
       for(int i=1;i<= mLIDataKeyDefSet.size();i++)
       {
           LIDataKeyDefSchema tLIDataKeyDefSchema = mLIDataKeyDefSet.get(i);
           String temp = tLIDataKeyDefSchema.getKeyID();
           if (tLIAboriginalDataSchema.getFieldIndex(temp) < 0)
           {
               buildError("InitLIDataKeyDef",
                          "LIAboriginalData表中不在字段" + temp);
               return false;
            }

        }
       return true;
    }

    public LIDistillInfoSchema CreatLIDistillInfo( LIAboriginalDataSchema tLIAboriginalDataSchema ) throws Exception
    {
        LIDistillInfoSchema tLIDistillInfoSchema = new  LIDistillInfoSchema();
        String tKeyUnionValue = "";
        for(int i=1;i<= mLIDataKeyDefSet.size();i++)
        {
            LIDataKeyDefSchema tLIDataKeyDefSchema = mLIDataKeyDefSet.get(i);
            String temp = tLIDataKeyDefSchema.getKeyID();
            String tempvalue = tLIAboriginalDataSchema.getV(temp);
            if(tempvalue.equals("null"))
            {
                Exception e = new Exception("类型为" + tLIAboriginalDataSchema.getClassID() + "的业务数据数据在生成处理日志时出错，因为必要信息字段" + temp +"值为空");
                throw e;
            }
            else
            {
               if(i==1)
               {
                  tKeyUnionValue = tKeyUnionValue +  tempvalue;
               }
               else
               {
                  tKeyUnionValue = tKeyUnionValue + "," + tempvalue;
               }
            }
        }
        tLIDistillInfoSchema.setBatchNo(tLIAboriginalDataSchema.getBatchNo());
        tLIDistillInfoSchema.setClassID(tLIAboriginalDataSchema.getClassID());
        tLIDistillInfoSchema.setKeyUnionValue(tKeyUnionValue);
        tLIDistillInfoSchema.setMakeDate(PubFun.getCurrentDate());
        tLIDistillInfoSchema.setMakeTime(PubFun.getCurrentTime());
        tLIDistillInfoSchema.setBussDate(tLIAboriginalDataSchema.getPayDate());
        tLIDistillInfoSchema.setManageCom(tLIAboriginalDataSchema.getManageCom());
        String flag = tLIAboriginalDataSchema.getListFlag();
        // modify by huxj 1-取个单合同号 2-取团体合同号 3-取险种号码
        if(flag==null)
        {
           Exception e = new Exception("类型为" + tLIAboriginalDataSchema.getClassID() + "的业务数据数据在生成处理日志时出错，因为必要信息字段ListFlag值为空");
           throw e;
        }
        else if(flag.equals("1"))
        {
            // tLIDistillInfoSchema.setPolNo(tLIAboriginalDataSchema.getGrpContNo());
        	tLIDistillInfoSchema.setPolNo(tLIAboriginalDataSchema.getContNo());
        }
        else if(flag.equals("2"))
        {
            // tLIDistillInfoSchema.setPolNo(tLIAboriginalDataSchema.getContNo());
        	tLIDistillInfoSchema.setPolNo(tLIAboriginalDataSchema.getGrpContNo());
        }
        else if(flag.equals("3"))
        {
            tLIDistillInfoSchema.setPolNo(tLIAboriginalDataSchema.getPolNo());
        }
        else
        {
             tLIDistillInfoSchema.setPolNo(tLIAboriginalDataSchema.getContNo());
        }
        return tLIDistillInfoSchema;
    }

    public String CreatKeyUnionValue( LIAboriginalDataSchema tLIAboriginalDataSchema ) throws Exception
    {
        String tKeyUnionValue = "";
        for(int i=1;i<= mLIDataKeyDefSet.size();i++)
        {
            LIDataKeyDefSchema tLIDataKeyDefSchema = mLIDataKeyDefSet.get(i);
            String temp = tLIDataKeyDefSchema.getKeyID();
            String tempvalue = tLIAboriginalDataSchema.getV(temp);
            if(tempvalue.equals("null"))
            {
                Exception e = new Exception("类型为" + tLIAboriginalDataSchema.getClassID() + "的业务数据数据在生成处理日志时出错，因为必要信息字段" + temp +"值为空");
                throw e;
            }
            else
            {
               if(i==1)
               {
                  tKeyUnionValue = tKeyUnionValue +  tempvalue;
               }
               else
               {
                  tKeyUnionValue = tKeyUnionValue + "," + tempvalue;
               }
            }
        }

        return tKeyUnionValue;
    }


    public LIDistillErrSchema CreatLLIDistillErr( LIAboriginalDataSchema tLIAboriginalDataSchema ,CErrors tErrors) throws Exception
    {
        LIDistillErrSchema tLIDistillErrSchema = new LIDistillErrSchema();
        String tKeyUnionValue = "";
        for(int i=1;i<= mLIDataKeyDefSet.size();i++)
        {
            LIDataKeyDefSchema tLIDataKeyDefSchema = mLIDataKeyDefSet.get(i);
            String temp = tLIDataKeyDefSchema.getKeyID();
            String tempvalue = tLIAboriginalDataSchema.getV(temp);
            if(tempvalue.equals("null"))
            {
                Exception e = new Exception("类型为" + tLIAboriginalDataSchema.getClassID() + "的业务数据数据在生成处理日志时出错，因为必要信息字段" + temp +"值为空");
                throw e;
            }
            else
            {
               if(i==1)
               {
                  tKeyUnionValue = tKeyUnionValue +  tempvalue;
               }
               else
               {
                  tKeyUnionValue = tKeyUnionValue + "," + tempvalue;
               }
            }
        }
        tLIDistillErrSchema.setBatchNo(tLIAboriginalDataSchema.getBatchNo());
        tLIDistillErrSchema.setClassID(tLIAboriginalDataSchema.getClassID());
        tLIDistillErrSchema.setKeyUnionValue(tKeyUnionValue);
        tLIDistillErrSchema.setMakeDate(PubFun.getCurrentDate());
        tLIDistillErrSchema.setMakeTime(PubFun.getCurrentTime());
        tLIDistillErrSchema.setManageCom(tLIAboriginalDataSchema.getManageCom());
        tLIDistillErrSchema.setBussDate(tLIAboriginalDataSchema.getPayDate());
        tLIDistillErrSchema.setErrInfo(tErrors.getFirstError());
        return tLIDistillErrSchema;
    }



    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();
        cError.moduleName = "LogDeal";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }
}
