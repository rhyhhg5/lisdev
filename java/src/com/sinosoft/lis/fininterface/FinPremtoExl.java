package com.sinosoft.lis.fininterface;

import java.io.File;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


import jxl.*;
import jxl.write.*;
import jxl.write.biff.RowsExceededException;
import jxl.format.UnderlineStyle;

public class FinPremtoExl
{
	public  CErrors mErrors=new CErrors();
	private VData mInputData= new VData();
	private TransferData tTransferData = new TransferData();
	/** 全局数据 */
	private GlobalInput mGlobalInput =new GlobalInput() ;
	/** 数据操作字符串 */

    private String mOperater;
    private String mManageCom;
    private String YAdate;
    private String YBdate;
    private String ManageCom1;
    private String ManageCom2;
    private MMap map=new MMap();
    
	public FinPremtoExl()
	{

	}
    
    
    public boolean submitData(VData cInputData,String path) throws RowsExceededException, WriteException,Exception
	{
		//将操作数据拷贝到本类中
		this.mInputData = cInputData;
		System.out.println("---UpLoadWriteFinBL getInputData010---");
		
		if (!getInputData(cInputData)) {
	            return false;
	       }
	      

		if (!checkData())
		{
			return false;
		}
		//进行业务处理
		if (!WriteXls(path)) 
		{
			return false;
		}

		if (!prepareOutputData())
		{
			return false;
		}
		
		if(!pubSubmit())
		{
			return false;
		}

		//System.out.println("End UpLoadWriteFinBL Submit...");
		mInputData=null;
		return true;
	}
    
    
    /**
     * 创建Excel
     * @throws WriteException 
     * @throws RowsExceededException 
     */
    public boolean WriteXls(String path)
    {
    	
        	
    	WritableWorkbook book=null;
			try {
				
				//建新的Excel
				//System.out.println(path+"20140331.xls");
				//System.out.println("fin-"+mOperater.trim()+".xls");
				 book= Workbook.createWorkbook(new File(path+"fin-"+mOperater.trim()+".xls"),Workbook.getWorkbook(new File(path+"20140401.xls")));
				
				//获取Excel工作薄
				WritableSheet sh1=book.getSheet(0);
				
		//	String str1=" select '1100','130301','10000','89','45','78','23','10000' from lccont where 1=1 fetch first 3 rows only with ur";
				       
				//查询函数
				/*String str1=" select  'managecom'  ,substr(riskcode,1,4),pcont ,Years ,PremiumType , '' , '' ,'' " 
                    + "from  fivoucherdatadetail "
                    + "where managecom between '"+ManageCom1+"' and '"+ManageCom2+"' "
                    + "and accountdate between '"+YAdate+"' and '"+YBdate+"' "
                    //+ "where managecom between 'ManageCom1' and 'ManageCom2'"
                    //+ "and accountdate between 'YAdate' and 'YBdate'"
                    + "and accountcode like '6531%' fetch first 2 rows only with ur";	  */     

				String str1="select manageCom,riskcode,pcont,polyear,premiumtype,sum(tmoney),nvl(shu,0),sum(tmoney)*nvl(shu,0),(case salechnl when '01' then '个人直销' when '02' then '团险直销' when '03' then '中介(团险)' when '04' then '银行代理' when '06' then '个销团 ' when '07' then '职团开拓 ' when '08' then '电销(中介)' when '09' then '电话直销' when '10' then '中介(个险)' when '11' then '财代健 ' when '12' then '寿代健 ' when '13' then '银代直销' when '14' then '互动直销' when '15' then '互动中介' when '16' then '社保直销' when '17' then '健管直销' when '99' then '其他' end) from( "
					+"select "
					+"substr(a.salechnl,1,2) salechnl,a.manageCom manageCom,substr(a.riskcode,1,4) riskcode, a.pcont pcont,a.polyear polyear,a.premiumtype premiumtype, "
					+"(case a.FinItemType when 'D' then a.summoney else -a.summoney end) tmoney, "
					+"(select  "
					+"(Case when codename in('19','32','33','35') then 0.1  "
					+"      when codename in ('21','25','36') then  "
					+"      	(case a.pcont when '11' then 0.2  " +
							"				  when '21' then a.polyear/10 " +
							"				  when '31' then 1 " +
							"	end)            "
					+"      When codename='34' then " +
							"	(case when (a.premiumtype in('10','99') and a.pcont='21') then a.polyear/10  " +
							"		  when (a.premiumtype in('10','99') and a.pcont='31') then 1 " +
							"		  when (a.premiumtype in('20','30')) then 1 " +
							"		  when (a.premiumtype in('10','99')) then 0.1 " +
							"	end) "
					+"   else 0  "
					+" end) shu "
					+"from FICodeTrans  where  code=substr(a.riskcode,1,4) "
					+"and codetype='KHWD' ) "
					+"from FIVoucherDataDetail a where 1=1  "
					+"and a.managecom between '"+ManageCom1+"' and '"+ManageCom2+"' "
					+"and a.accountdate between '"+YAdate+"' and '"+YBdate+"' "
					+"and a.accountdate >='2013-1-1' "
					+"and a.checkflag='00' "
					+"and substr(a.accountcode,1,4) in ('6531','7531') "
					+"and exists (select 1 from lbpol b where b.contno=a.contno and a.listflag='1' and (DAYS(a.accountdate) - DAYS(b.cvalidate))<365 " +
						   "union select 1 from lcpol c where c.contno=a.contno and a.listflag='1' and (DAYS(a.accountdate) - DAYS(c.cvalidate))<365 " +
						   "union select 1 from lbgrppol d where d.grpcontno=a.contno and a.listflag='2' and (DAYS(a.accountdate) - DAYS(d.cvalidate))<365 " +
						   "union select 1 from lcgrppol e where e.grpcontno=a.contno and a.listflag='2' and (DAYS(a.accountdate) - DAYS(e.cvalidate))<365 " +
						   "fetch first row only )"
					//+"group by a.manageCom,substr(a.riskcode,1,4), a.pcont,a.polyear,a.premiumtype "
					//+") as aa "
					
					+"union all "
					
					//+"select manageCom,riskcode,pcont,polyear,premiumtype,tmoney,nvl(shu,0),tmoney*nvl(shu,0) from( "
					+"select "
					+"substr(a.salechnl,1,2) salechnl,a.manageCom manageCom,substr(a.riskcode,1,4) riskcode, a.pcont pcont,a.polyear polyear,a.premiumtype premiumtype, "
					+"(case a.FinItemType when 'D' then a.summoney else -a.summoney end) tmoney, "
					+"(select  "
					+"(Case when codename in('19','32','33','35') then 0.1  "
					+"      when codename in ('21','25','36') then  "
					+"      	(case a.pcont when '11' then 0.2  " +
							"				  when '21' then a.polyear/10 " +
							"				  when '31' then 1 " +
							"	end)            "
					+"      When codename='34' then " +
							"	(case when (a.premiumtype in('10','99') and a.pcont='21') then a.polyear/10  " +
							"		  when (a.premiumtype in('10','99') and a.pcont='31') then 1 " +
							"		  when (a.premiumtype in('20','30')) then 1 " +
							"		  when (a.premiumtype in('10','99')) then 0.1 " +
							"	end) "
					+"   else 0 "
					+" end) shu "
					+"from FICodeTrans  where  code=substr(a.riskcode,1,4) "
					+"and codetype='KHWD' ) "
					+"from LIDataTransResult a where 1=1  "
					+"and a.managecom between '"+ManageCom1+"' and '"+ManageCom2+"' "
					+"and a.accountdate between '"+YAdate+"' and '"+YBdate+"' "
					+"and a.accountdate < '2013-1-1' "
					+"and substr(a.accountcode,1,4) in ('6531','7531') "
					+"and exists (select 1 from lbpol b where b.contno=a.contno and a.listflag='1' and (DAYS(a.accountdate) - DAYS(b.cvalidate))<365 " +
						   "union select 1 from lcpol c where c.contno=a.contno and a.listflag='1' and (DAYS(a.accountdate) - DAYS(c.cvalidate))<365 " +
						   "union select 1 from lbgrppol d where d.grpcontno=a.contno and a.listflag='2' and (DAYS(a.accountdate) - DAYS(d.cvalidate))<365 " +
						   "union select 1 from lcgrppol e where e.grpcontno=a.contno and a.listflag='2' and (DAYS(a.accountdate) - DAYS(e.cvalidate))<365 " +
						   "fetch first row only )"
					//+"group by a.manageCom,substr(a.riskcode,1,4), a.pcont,a.polyear,a.premiumtype "
					+") as bb "
					+"group by (case salechnl when '01' then '个人直销' when '02' then '团险直销' when '03' then '中介(团险)' when '04' then '银行代理' when '06' then '个销团 ' when '07' then '职团开拓 ' when '08' then '电销(中介)' when '09' then '电话直销' when '10' then '中介(个险)' when '11' then '财代健 ' when '12' then '寿代健 ' when '13' then '银代直销' when '14' then '互动直销' when '15' then '互动中介' when '16' then '社保直销' when '17' then '健管直销' when '99' then '其他' end),manageCom,riskcode,pcont,polyear,premiumtype,nvl(shu,0) "
					+" with ur "
;
				//定义单元格格式
				WritableFont wfc = new WritableFont(WritableFont.ARIAL, 10,WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,jxl.format.Colour.BLACK); // 定义格式 字体 下划线 斜体 粗体 颜色
				WritableCellFormat wcft = new WritableCellFormat(wfc); // 单元格定义
				//wcft.setBackground(jxl.format.Colour.WHITE); // 设置单元格的背景颜色
				wcft.setAlignment(jxl.format.Alignment.LEFT); // 设置对齐方式   
				wcft.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
				wcft.setBorder(jxl.format.Border.ALL,jxl.format.BorderLineStyle.THIN); 
		
				
				WriteSheet(sh1,str1,9,2,wcft,1);
			
				//写入
				book.write();			
				book.close();
				
	        	System.out.println("true");
				return true;
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				 CError tError = new CError();
		            tError.moduleName = "NewTestBL";
		            tError.functionName = "getInputData";
		            tError.errorMessage = "生成报表失败！";
		            this.mErrors.addOneError(tError);
		            return false;
			}

            
    }
//数据写入Excel中    
    private boolean WriteSheet(WritableSheet sheet,String sql,int x,int y,WritableCellFormat wf,int FirstSum) throws RowsExceededException, WriteException,Exception
    {
    	System.out.println("sql111111111111"+sql);
    	try{
    	ExeSQL es = new ExeSQL();
    	SSRS mSSRS = new SSRS();
    	mSSRS = es.execSQL(sql);
    	int maxrow=mSSRS.getMaxRow();
    	for(int a=0;a<x;a++)
    	{
    		for(int i=y;i<=(maxrow+y-1);i++)
	    	{ 
	    		sheet.addCell(new Label(a,(i-1),mSSRS.GetText((i-y+1), a+1),wf));
	    	}
    	}
    	WriteSheetSum(sheet,maxrow,x,y,wf,FirstSum);
    		
    	return true;
    	}catch(Exception e){
    		Exception cv =  new Exception("写入excel文件失败"+e.getMessage());
   		    throw cv;
    	}
    }
//写入合计值
    private boolean WriteSheetSum(WritableSheet sheet,int maxrow,int x,int y,WritableCellFormat wf,int FirstSum) throws Exception
    {
    	try{
    	sheet.addCell(new Label(0,(y+maxrow-1),"合计",wf));
    	

    		double sum=0;
    		double sum2 = 0 ;
    		for(int a1=(y-1);a1<(y+maxrow-1);a1++){
    			sum=sum+Double.parseDouble(sheet.getCell(5,a1).getContents().trim());
    		}
    		for(int a1=(y-1);a1<(y+maxrow-1);a1++){
    			sum2=sum2+Double.parseDouble(sheet.getCell(7,a1).getContents().trim());
    		}
    		sum=Math.round(sum*100)/100.0;
    		sum2=Math.round(sum2*100)/100.0;
    		System.out.println("sum="+sum);
    		System.out.println("sum2="+sum2);
    		
    		sheet.addCell(new Label(5,(y+maxrow-1),Double.toString(sum),wf));
    		sheet.addCell(new Label(7,(y+maxrow-1),Double.toString(sum2),wf));
    	
    	
    	}catch(Exception e)
    	{
    		 Exception cv =  new Exception("写入合计值失败"+e.getMessage());
    		 throw cv;
    	}
    	return true;
    }

    private boolean getInputData(VData cInputData)
	{
		mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0);
		tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData",0);
		ManageCom1=tTransferData.getValueByName("ManageCom1").toString();
		ManageCom2=tTransferData.getValueByName("ManageCom2").toString();
		System.out.println("前台传输全局公共数据");
		if (mGlobalInput == null) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "BusCompetiReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //获得操作员编码
		mOperater = mGlobalInput.Operator;
        if ((mOperater == null) || mOperater.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "NewTestBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //获得登陆机构编码
        mManageCom = mGlobalInput.ManageCom;
        if ((mManageCom == null) || mManageCom.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "NewTestBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
            this.mErrors.addOneError(tError);
            System.out.println("前台传输全局公共数据ManageCom失败");
            return false;
        }
       
        YAdate=tTransferData.getValueByName("YAdate").toString();
        if ((YAdate == null) || YAdate.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "NewTestBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输保费起始日期失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        YBdate=tTransferData.getValueByName("YBdate").toString();
        if ((YBdate == null) || YBdate.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "NewTestBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输保费终止日期失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
       
		return true;
	}
    
	private boolean checkData()
	{
		return true;
	}
	
	private boolean prepareOutputData()
	{
		try
		{
			mInputData.clear();
			mInputData.add(map);
		}
		catch(Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "NewTsetBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
 
	/**
	 * 提交数据
	 * @return
	 */
	private boolean pubSubmit()
	{
		// 进行数据提交
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, ""))
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "BusCompetiReportBL";
			tError.functionName = "PubSubmit.submitData";
			tError.errorMessage = "数据提交失败BusCompetiReportBL-->pubSubmit!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

    /**
     * 测试
     * @param args
     * @throws WriteException 
     * @throws RowsExceededException 
     */
    public static void main(String[] args)
    {
    	/*BusCompetiReportBL read = new BusCompetiReportBL();
        try {
			//read.WriteXls("D:/");
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
//			System.out.(e.printStackTrace());
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

    }
    
    
}
