package com.sinosoft.lis.fininterface;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.vschema.LIDataTransResultSet;
import com.sinosoft.lis.vschema.LIDistillInfoSet;
import com.sinosoft.lis.vschema.LIDistillErrSet;
import com.sinosoft.lis.vschema.LIDistillLogSet;
import com.sinosoft.lis.vschema.LIAboriginalDataSet;
import java.sql.Connection;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.lis.vdb.LIAboriginalDataDBSet;
import com.sinosoft.lis.vdb.LIDataTransResultDBSet;
import com.sinosoft.lis.vdb.LIDistillInfoDBSet;
import com.sinosoft.lis.vdb.LIDistillErrDBSet;
import com.sinosoft.lis.vdb.LIDistillLogDBSet;


/**
 * <p>Title: DataToSubmit</p>
 *
 * <p>Description: 处理数据提交</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author jianan
 * @version 1.0
 */
public class DataToSubmit {

    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    /** 传入数据的容器 */
    private VData mInputData = new VData();
    /** 数据操作字符串 */
    private String mOperate;

    //业务数据
    LIAboriginalDataSet mLIAboriginalDataSet;
    LIDataTransResultSet mLIDataTransResultSet;
    LIDistillInfoSet mLIDistillInfoSet;
    LIDistillErrSet mLIDistillErrSet;
    LIDistillLogSet mLIDistillLogSet;

    public DataToSubmit() {
    }

    public boolean submitData(VData cInputData, String cOperate) {
    	
	        boolean tReturn = false;
	
	        //将操作数据拷贝到本类中
	        this.mInputData = (VData) cInputData.clone();
	        this.mOperate = cOperate;
	
	        //得到外部传入的数据,将数据备份到本类中
	        if (!getInputData()) {
	            return false;
	        }
	        System.out.println("---End DataToSubmit getInputData---");
	        tReturn = save();
	        if (tReturn) {
	            System.out.println("Save sucessful");
	        } else {
	            System.out.println("Save failed");
	        }
	        return tReturn;
    }

    /**
     * 将外部传入的数据分解到本类的属性中
     * @param: 无
     * @return: boolean
     */
    private boolean getInputData() {
        try {
            mLIAboriginalDataSet = (LIAboriginalDataSet) mInputData.
                                   getObjectByObjectName("LIAboriginalDataSet",
                    0);
            mLIDataTransResultSet = (LIDataTransResultSet) mInputData.
                                    getObjectByObjectName(
                    "LIDataTransResultSet", 0);
            mLIDistillInfoSet = (LIDistillInfoSet) mInputData.
                                getObjectByObjectName("LIDistillInfoSet", 0);
            mLIDistillErrSet = (LIDistillErrSet) mInputData.
                               getObjectByObjectName("LIDistillErrSet", 0);
            mLIDistillLogSet = (LIDistillLogSet) mInputData.
                               getObjectByObjectName("LIDistillLogSet", 0);
        } catch (Exception e) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "DataToSubmit";
            tError.functionName = "getInputData";
            tError.errorMessage = "接收数据失败!!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    //保存操作
    private boolean save() {

        boolean tReturn = true;
        System.out.println("Start Save...");

        //建立数据库连接
        Connection conn = DBConnPool.getConnection();
        if (conn == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "DataToSubmit";
            tError.functionName = "saveData";
            tError.errorMessage = "数据库连接失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        try {
            //开始事务，锁表
            conn.setAutoCommit(false);

            if (mLIAboriginalDataSet != null && mLIAboriginalDataSet.size() > 0) {
                LIAboriginalDataDBSet tLIAboriginalDataDBSet = new
                        LIAboriginalDataDBSet(conn);
                tLIAboriginalDataDBSet.set(mLIAboriginalDataSet);
                if (!tLIAboriginalDataDBSet.insert()) {
                    try {
                        conn.rollback();
                    } catch (Exception e) {
                    	
                    }
                    conn.close();
                    System.out.println("LIAboriginalData Insert Failed");
                    this.mErrors.copyAllErrors(tLIAboriginalDataDBSet.mErrors);
                    return false;
                }
                System.out.println("End 生成LIAboriginalData数据 ...");
            }

            if (mLIDataTransResultSet != null &&
                mLIDataTransResultSet.size() > 0) {
                LIDataTransResultDBSet tLIDataTransResultDBSet = new
                        LIDataTransResultDBSet(conn);
                tLIDataTransResultDBSet.set(mLIDataTransResultSet);
                if (!tLIDataTransResultDBSet.insert()) {
                    try {
                        conn.rollback();
                    } catch (Exception e) {
                    	
                    }
                    conn.close();
                    System.out.println("LIDataTransResult Insert Failed");
                    this.mErrors.copyAllErrors(tLIDataTransResultDBSet.mErrors);
                    return false;
                }
                System.out.println("End 生成LIDataTransResult数据 ...");
            }

            if (mLIDistillInfoSet != null && mLIDistillInfoSet.size() > 0) {
                LIDistillInfoDBSet tLIDistillInfoDBSet = new LIDistillInfoDBSet(
                        conn);
                tLIDistillInfoDBSet.set(mLIDistillInfoSet);
                if (!tLIDistillInfoDBSet.insert()) {
                    try {
                        conn.rollback();
                    } catch (Exception e) {}
                    conn.close();
                    this.mErrors.copyAllErrors(tLIDistillInfoDBSet.mErrors);
                    System.out.println("LIDistillInfo Insert Failed");
                    return false;
                }
                System.out.println("End 生成LIDistillInfo数据 ...");
            }

            if (mLIDistillErrSet != null && mLIDistillErrSet.size() > 0) {
                LIDistillErrDBSet tLIDistillErrDBSet = new LIDistillErrDBSet(
                        conn);
                System.out.println("LIDistillErr Insert");
                tLIDistillErrDBSet.set(mLIDistillErrSet);
                if (!tLIDistillErrDBSet.insert()) {
                    try {
                        conn.rollback();
                    } catch (Exception e) {}
                    conn.close();
                    this.mErrors.copyAllErrors(tLIDistillErrDBSet.mErrors);
                    System.out.println("LIDistillErr Insert Failed");
                    return false;
                }
                System.out.println("End 生成LIDistillErr数据 ...");
            }

            if (mLIDistillLogSet != null && mLIDistillLogSet.size() > 0) {
                LIDistillLogDBSet tLIDistillLogDBSet = new LIDistillLogDBSet(
                        conn);
                tLIDistillLogDBSet.set(mLIDistillLogSet);
                if (!tLIDistillLogDBSet.insert()) {
                    try {
                        conn.rollback();
                    } catch (Exception e) {}
                    conn.close();
                    this.mErrors.copyAllErrors(tLIDistillLogDBSet.mErrors);
                    System.out.println("LIDistillLog Insert Failed");
                    return false;
                }
                System.out.println("End 生成LIDistillLog数据 ...");
            }

            conn.commit();
            conn.close();
            System.out.println("End Committed");
        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PayReturnFromBankBLS";
            tError.functionName = "submitData";
            tError.errorMessage = ex.toString();
            this.mErrors.addOneError(tError);
            try {
                conn.rollback();
            } catch (Exception e) {}
            tReturn = false;
        }
        System.out.println("End Save...");
        return tReturn;
    }

}
