package com.sinosoft.lis.fininterface;

import java.sql.SQLException;

import com.sinosoft.lis.db.LICodeTransDB;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.schema.LICodeTransSchema;
import com.sinosoft.lis.vschema.LICodeTransSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>
 * Title: DataDetailCheckQuery.java
 * </p>
 * 
 * <p>
 * Description: 查询对帐不平的明细数据
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2007
 * </p>
 * 
 * <p>
 * Company: sinosoft
 * </p>
 * 
 * @author linyun
 * @version 1.0
 */

public class CheckCountMoneyBL {

	String tResult = "";
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mResult = new VData();
	private LICodeTransSet m_oCodeTransSet = null;
	private String m_sStartDate = "";
	private String m_sEndDate = "";
	
	
	public CheckCountMoneyBL() {
		
	}

	/**
	 * 保存错误信息
	 * @param sFuncName
	 * @param sErrorInfo
	 */
	private void buildError(String sFuncName, String sErrorInfo)
	{

		CError oCError = new CError();
		oCError.moduleName = "FinInterfaceTransBatchBL";
		oCError.functionName = sFuncName;
		oCError.errorMessage = sErrorInfo;
		this.mErrors.addOneError(oCError);

	}
	
	/***
	 * 
	 * @return
	 */
	public VData getResult(){		
		return mResult;
	}
	
	/******
	 * @param tStartDate  起始日期
	 * @param tEndDate    终止日期
	 * @param tManageCom  发生机构
	 * @param sCheckType  校验类型
	 * @return
	 */
	public boolean CheckData(String tStartDate, String tEndDate,
			String tManageCom) {

		try {			
			//对相关数据进行核对
			if(tManageCom != null && !tManageCom.equals("")
					&& tStartDate != null && !tStartDate.equals("")
					&& tEndDate != null && !tEndDate.equals("")){
				
				m_sStartDate = tStartDate;
				m_sEndDate = tEndDate;
				LICodeTransDB oCodeTransDB = new LICodeTransDB();
				oCodeTransDB.setCodeType("CheckCountType");
				if(!oCodeTransDB.getInfo()){
					buildError("CheckData","没有查询到对应的校验规则!");
				}
				
				LICodeTransSet oCodeTransSet = oCodeTransDB.query();
				if(oCodeTransSet.size() > 0){
					m_oCodeTransSet = new LICodeTransSet();
					for(int i=1;i<=oCodeTransSet.size();i++){
						
						LICodeTransSchema oCodeTransSchema = new LICodeTransSchema();
						LICodeTransSchema oCodeTransSchema1 = oCodeTransSet.get(i);
						String strSQL1 = oCodeTransSet.get(i).getCodeName();  //核对已经提取过的财务接口数据
						String strSQL2 = oCodeTransSet.get(i).getCodeAlias(); //核对现有系统中的数据汇总
						
						/***
						 * 核对已经提取过的财务接口数据
						 */
						PubCalculator tPubCalculator1 = new PubCalculator();
						tPubCalculator1.addBasicFactor("ManageCom", tManageCom);
						tPubCalculator1.addBasicFactor("StartDate", tStartDate);
						tPubCalculator1.addBasicFactor("EndDate", tEndDate);
						tPubCalculator1.setCalSql(strSQL1);								
						strSQL1 = tPubCalculator1.calculateEx();
						ExeSQL oExeSQL1 = new ExeSQL();
						SSRS oSsrs1 = oExeSQL1.execSQL(strSQL1);
						
						/*****
						 * 核对现有系统中的数据汇总
						 */
						PubCalculator tPubCalculator2 = new PubCalculator();
						tPubCalculator2.addBasicFactor("ManageCom", tManageCom);
						tPubCalculator2.addBasicFactor("StartDate", tStartDate);
						tPubCalculator2.addBasicFactor("EndDate", tEndDate);
						tPubCalculator2.setCalSql(strSQL2);								
						strSQL2 = tPubCalculator2.calculateEx();
						ExeSQL oExeSQL2 = new ExeSQL();
						SSRS oSsrs2 = oExeSQL2.execSQL(strSQL2);
						
						if(oSsrs1.MaxRow > 0 && oSsrs2.MaxRow > 0){
							
							if(Double.parseDouble(oSsrs1.GetText(1, 1)) != Double.parseDouble((oSsrs2.GetText(1, 1)))){						
								oCodeTransSchema.setOtherSign(oCodeTransSchema1.getOtherSign());
								oCodeTransSchema.setCodeName(oSsrs1.GetText(1, 1));//接口提取数据
								oCodeTransSchema.setCodeAlias(oSsrs2.GetText(1, 1));//业务数据
								oCodeTransSchema.setCode("区间为:" + tStartDate + "至" + tEndDate);
								m_oCodeTransSet.add(oCodeTransSchema);
//								return false;
							}else{
//								return true;
								continue;  //当前循环无数据
							}
						}else{							
							oCodeTransSchema.setOtherSign(oCodeTransSchema1.getOtherSign());
							oCodeTransSchema.setCodeName(oSsrs1.GetText(1, 1));//接口提取数据
							oCodeTransSchema.setCodeAlias(oSsrs2.GetText(1, 1));//业务数据
							oCodeTransSchema.setCode("区间为:" + tStartDate + "至" + tEndDate);
							m_oCodeTransSet.add(oCodeTransSchema);																			
//							return false;							
						}								
					}
				}				
			}
			
			if(m_oCodeTransSet != null && m_oCodeTransSet.size() > 0){
				mResult.clear();
				mResult.add(m_oCodeTransSet);
			}
			return true;				
		
		} catch (Exception ex) {
			buildError("CheckData","引擎处理失败:" + ex.getMessage());	
			return false;
		}
	}

	
	/****
	 * 根据对应的SQL脚本来查询对应的数据
	 * @param strSQL
	 * @return
	 */
	private String getSumMoney(String strSQL){
				
		String strsql1 = "";
		PubCalculator tPubCalculator2 = new PubCalculator();
//		tPubCalculator2.addBasicFactor("ManageCom", tManageCom);
		tPubCalculator2.addBasicFactor("StartDate", m_sStartDate);
		tPubCalculator2.addBasicFactor("EndDate", m_sEndDate);
		tPubCalculator2.setCalSql(strSQL);								
		strsql1 = tPubCalculator2.calculateEx();
		ExeSQL oExeSQL2 = new ExeSQL();
		SSRS oSsrs2 = oExeSQL2.execSQL(strsql1);
		
		if(oSsrs2.MaxRow > 0){
			return oSsrs2.GetText(1, 1);
		}else{
		    return "0";	
		}
		
	}
	
	public static void main(String[] args) throws SQLException {
	
		
		CheckCountMoneyBL oCheckCountMoneyBL = new CheckCountMoneyBL();
		if(oCheckCountMoneyBL.CheckData("2007-6-22", "2007-6-22", "86")){
			VData oData = new VData();
			oData = oCheckCountMoneyBL.getResult();
			System.out.println("000000++++");
			
			LICodeTransSet oCodeTransSet = new LICodeTransSet();
		    oCodeTransSet.set((LICodeTransSet)oData.getObjectByObjectName("LICodeTransSet",0));
		    System.out.println(oCodeTransSet.encode());
		}

	}
}