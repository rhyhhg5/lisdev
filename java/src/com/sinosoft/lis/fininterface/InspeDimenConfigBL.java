package com.sinosoft.lis.fininterface;

import com.sinosoft.lis.db.FICodeTransDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.FICodeTransSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class InspeDimenConfigBL {
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private String mOperate = "";
    //业务处理相关变量
    /** 全局数据 */

    MMap map = new MMap();
    private GlobalInput mGlobalInput = new GlobalInput();
    private FICodeTransSchema mFICodeTransSchema = new FICodeTransSchema();
    

    public InspeDimenConfigBL() {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */    
    public boolean submitData(VData cInputData, String cOperate)
    {
    	System.out.println("ui页面调用bl成功！");
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        //进行业务处理
        if (!dealData())
        {
            return false;
        }
        //准备往后台的数据
        if (!prepareOutputData())
            return false;
    
    	PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "OLDDiseaseBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";

            this.mErrors.addOneError(tError);
            return false;
        }
        System.out.println("ceshi");
        return true;
    }
    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "DerferAppF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData)
    {
    	
        mFICodeTransSchema=(FICodeTransSchema) cInputData.getObjectByObjectName("FICodeTransSchema",0);
        //System.out.println("groupname的值："+mFICodeTransSchema.getSchema().getGroupName());
        mGlobalInput=(GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        return true;
    }
    
    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean submitquery()
    {
        this.mResult.clear();
        FICodeTransDB tFICodeTransDB = new FICodeTransDB();
        tFICodeTransDB.setSchema(this.mFICodeTransSchema);
//      如果有需要处理的错误，则返回
        if (tFICodeTransDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tFICodeTransDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "InspeDimenConfigBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mInputData = null;
        return true;
    }
    
    public VData getResult() {
        return this.mResult;
    }
    /**
     * 根据前面的输入数据，进行UI逻辑处理 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	System.out.println("sql语句插入开始。。。。。。。。。。");
        if (mOperate.equals("INSERT"))
        {
        	System.out.println("判断成功，mOperate=INSERT");
        	mFICodeTransSchema.setCodeType("KHWD");
        	mFICodeTransSchema.setOtherSign("1");
        	mFICodeTransSchema.setVersionNo("1");
        	map.put(mFICodeTransSchema, "DELETE&INSERT");
        }
        if (mOperate.equals("DELETE"))
        {
            if(!checkDelete())
            {
                mErrors.addOneError("删除失败。");
                return false;
            }
            mFICodeTransSchema.setCodeType("KHWD");
        	mFICodeTransSchema.setOtherSign("1");
        	mFICodeTransSchema.setVersionNo("1");
            map.put(mFICodeTransSchema, "DELETE"); //删除
        }

        return true;
    }
    
    private boolean checkDelete()
    {
        return true;
    }
    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData()
    {
    	mInputData.add(map);
    	return true;
    }
}
