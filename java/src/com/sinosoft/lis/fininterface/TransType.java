package com.sinosoft.lis.fininterface;

import com.sinosoft.lis.schema.LIAboriginalDataSchema;

/**
 * <p>Title: TransType</p>
 *
 * <p>Description: 代码转换处理类借口定义</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author jianan
 * @version 1.0
 */
public interface TransType {

    public String transInfo(String TransValue,LIAboriginalDataSchema tLIAboriginalDataSchema) throws Exception;

}
