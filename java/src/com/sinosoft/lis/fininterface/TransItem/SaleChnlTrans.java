package com.sinosoft.lis.fininterface.TransItem;

import com.sinosoft.lis.fininterface.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.vschema.LICodeTransSet;
import com.sinosoft.lis.db.LICodeTransDB;
import com.sinosoft.lis.vschema.LAComSet;
//import com.sinosoft.lis.db.*;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LBGrpContDB;
import com.sinosoft.utility.Reflections;;

/**
 * <p>Title: SaleChnl</p>
 *
 * <p>Description: 销售渠道转换类</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author jianan
 * @version 1.0
 */
public class SaleChnlTrans implements TransType {

    public String transInfo(String TransValue,LIAboriginalDataSchema tLIAboriginalDataSchema) throws Exception
    {
        try
        {
        	String tResult = new String();
        	tResult = tLIAboriginalDataSchema.getSaleChnl();
        	if(!tLIAboriginalDataSchema.getSaleChnlDetail().equals("")&&!tLIAboriginalDataSchema.getSaleChnlDetail().equals("null")&&tLIAboriginalDataSchema.getSaleChnlDetail()!=null){
        		tResult = tResult + tLIAboriginalDataSchema.getSaleChnlDetail();
        	}
        	if(!tLIAboriginalDataSchema.getAgentCom().equals("")&&!tLIAboriginalDataSchema.getAgentCom().equals("null")&&tLIAboriginalDataSchema.getAgentCom()!=null){
        		tResult = tResult + tLIAboriginalDataSchema.getAgentCom();
        	}
        	return tResult;
        	
//    		if (tLIAboriginalDataSchema.getListFlag().equals("1")) {
//    			if (tLIAboriginalDataSchema.getSaleChnl().equals("03")) {
//    				if (tLIAboriginalDataSchema.getSaleChnlDetail().equals("07")) {
//    					tResult = tLIAboriginalDataSchema.getSaleChnl() + tLIAboriginalDataSchema.getSaleChnlDetail() + "998";
//    				} else if (tLIAboriginalDataSchema.getSaleChnlDetail().equals("71")
//    						|| tLIAboriginalDataSchema.getSaleChnlDetail().equals("72")
//    						|| tLIAboriginalDataSchema.getSaleChnlDetail().equals("73")) {
//    					tResult = tLIAboriginalDataSchema.getSaleChnl() + tLIAboriginalDataSchema.getSaleChnlDetail() + "997";
//    				} 
//    				else{
//    					tResult = tLIAboriginalDataSchema.getSaleChnl() + tLIAboriginalDataSchema.getSaleChnlDetail();
//    					if(tLIAboriginalDataSchema.getAgentCom().equals(""))
//    					tResult = tResult + tLIAboriginalDataSchema.getAgentCom().substring(0, 3);
//    				}
//    			}else{
//					tResult = tLIAboriginalDataSchema.getSaleChnl() + tLIAboriginalDataSchema.getSaleChnlDetail();
//					if(!tLIAboriginalDataSchema.getAgentCom().equals("")&&tLIAboriginalDataSchema.getAgentCom().length()>=3)
//					tResult = tResult + tLIAboriginalDataSchema.getAgentCom().substring(0, 3);
////    				tResult = tLIAboriginalDataSchema.getSaleChnl() + tLIAboriginalDataSchema.getSaleChnl() + tLIAboriginalDataSchema.getSaleChnlDetail();
//    			}
//    		} else {
//    			if (tLIAboriginalDataSchema.getSaleChnl().equals("03") && !tLIAboriginalDataSchema.getAgentCom().equals("")) {
//    				tResult = tLIAboriginalDataSchema.getSaleChnl() + tLIAboriginalDataSchema.getSaleChnlDetail() + tLIAboriginalDataSchema.getAgentCom().substring(0, 3);
//    			}
//    			else{
//    				tResult = tLIAboriginalDataSchema.getSaleChnl() + tLIAboriginalDataSchema.getSaleChnlDetail();
//    			}
//    		}
//    		return tResult;
        }
        catch (Exception ex) {

            Exception e = new  Exception("销售渠道转换出错！原因是：" + ex.getMessage());
            throw e;
        }
    }
}
