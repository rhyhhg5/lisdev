package com.sinosoft.lis.fininterface.TransItem;

import com.sinosoft.lis.fininterface.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.vschema.LICodeTransSet;
import com.sinosoft.lis.db.LICodeTransDB;
import com.sinosoft.lis.vschema.LAComSet;
import com.sinosoft.lis.db.LAComDB;

/**
 * <p>Title: SaleChnl</p>
 *
 * <p>Description: 销售渠道转换类</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author jianan
 * @version 1.0
 */
public class SaleChnl implements TransType {

    private LAComSet mLAComSet;
    private LICodeTransSet mLICodeTransSet;
    public SaleChnl() throws Exception
    {
        try
        {
//            LAComDB tLAComDB = new LAComDB();
            LICodeTransDB tLICodeTransDB = new  LICodeTransDB();
//            mLAComSet = tLAComDB.query();
            tLICodeTransDB.setCodeType("SaleChnl");
            mLICodeTransSet = tLICodeTransDB.query();
            if(mLICodeTransSet.size()<=0)
            {
                Exception e = new  Exception("销售渠道转换编码未定义");
                throw e;
            }

        }
        catch (Exception ex) {

            Exception e = new  Exception("销售渠道转换出错" + ex.getMessage());
            throw e;
        }

    }


    public String transInfo(String TransValue,LIAboriginalDataSchema tLIAboriginalDataSchema) throws Exception
    {
        String resultString = "";
        String temp = TransValue;
        // temp = temp + ",";
        if("03".equals(TransValue))
        {
        	 if(tLIAboriginalDataSchema.getAgentCom()!=null&&!tLIAboriginalDataSchema.getAgentCom().trim().equals(""))
        	 {
        		 LAComDB tLAComDB = new LAComDB();
        		 tLAComDB.setAgentCom(tLIAboriginalDataSchema.getAgentCom());
        		 if(!tLAComDB.getInfo())
        		 {
        			 Exception e = new  Exception("销售渠道转换出错,代理机构编码" + tLIAboriginalDataSchema.getAgentCom() + "未定义");
                     throw e;
        		 }
        		 else
        		 {
        			 temp = temp + tLAComDB.getACType();
        		 }
        	 }
        	 else
        	 {
        		 Exception e = new  Exception("销售渠道为+"+TransValue+",但代理机构为空");
                 throw e;
        	 }
//            for(int i=1;i<=mLAComSet.size();i++)
//            {
//                 if(mLAComSet.get(i).getAgentCom().trim().equals(tLIAboriginalDataSchema.getAgentCom().trim()))
//                 {
//
//                	 //modify by lijs 2006-12-26 start
//                	 //before modify
//                	 //if(mLAComSet.get(i).getBankCode()!=null&&!mLAComSet.get(i).getBankCode().equals(""))
//                     //{
//                     //    temp = temp + mLAComSet.get(i).getBankCode().trim();
//                     //}
//                	 //after modify
//                     //if(mLAComSet.get(i).getACType().equals("07")){ //银行代理
//                     if(TransValue.equals("07")){
//                    	 temp = temp + SaleChnlForBank.getBankCode(mLAComSet.get(i).getAgentCom());
//                     }
//                     //modify by lijs 2006-12-26 end
//                     else
//                     {
//                         temp = temp + "N";
//                     }
//                     break;
//                 }
//                 if(i==mLAComSet.size())
//                 {
//                	 System.out.println(tLIAboriginalDataSchema.getAgentCom());
//                	 Exception e = new  Exception("销售渠道转换出错,代理机构编码" + tLIAboriginalDataSchema.getAgentCom() + "未定义");
//                     throw e;
//                 }
//            }
        	
        }
        else
        {
          // temp = temp + "N";
        }
//        for(int i=1;i<= mLICodeTransSet.size();i++)
//        {
//            if(mLICodeTransSet.get(i).getCode().trim().equals(temp))
//            {
//               resultString = mLICodeTransSet.get(i).getCodeAlias();
//               break;
//            }
//            if(i==mLICodeTransSet.size())
//            {
//                Exception e = new  Exception("销售渠道转换出错,转换编码" + temp + "未定义");
//                throw e;
//            }
//        }
        resultString=temp;
        return resultString;
    }
    public static void main(String[] args)
    {
     try
     {
         LIAboriginalDataSchema tLIAboriginalDataSchema = new
                 LIAboriginalDataSchema();
         tLIAboriginalDataSchema.setAgentCom("8611096666");
         SaleChnl tSaleChnl = new SaleChnl();
         String t = tSaleChnl.transInfo("07",tLIAboriginalDataSchema);
        System.out.println(t);
     }
     catch(Exception e)
     {
         System.out.println(e.getMessage());
     }

    }
}
