package com.sinosoft.lis.fininterface.TransItem;

import com.sinosoft.lis.fininterface.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.LICodeTransDB;
import com.sinosoft.lis.vschema.LICodeTransSet;

/**
 * <p>Title: CostCenterForPAL</p>
 *
 * <p>Description: 损益类科目成本中心转换</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author jianan
 * @version 1.0
 */
public class CostCenterForPAL implements TransType {

    private LICodeTransSet mLICodeTransSet;
    public CostCenterForPAL() throws Exception
    {
        try
        {
            LICodeTransDB tLICodeTransDB = new  LICodeTransDB();
            tLICodeTransDB.setCodeType("CostCenter");
            mLICodeTransSet = tLICodeTransDB.query();
            if(mLICodeTransSet.size()<=0)
            {
                Exception e = new  Exception("成本中心转换编码未定义");
                throw e;
            }

        }
        catch (Exception ex) {

            Exception e = new  Exception("成本中心编码转换出错" + ex.getMessage());
            throw e;
        }

    }


    public String transInfo(String TransValue,LIAboriginalDataSchema tLIAboriginalDataSchema) throws Exception
    {
        String resultString = "";
        String temp = TransValue;
        int tLength = temp.length();
        if(tLength>6)
        {
            temp = temp.substring(0,6);
        }

        for(int i=1;i<= mLICodeTransSet.size();i++)
        {
            if(mLICodeTransSet.get(i).getCode().trim().equals(temp))
            {
               resultString = mLICodeTransSet.get(i).getCodeAlias();
               break;
            }
            if(i==mLICodeTransSet.size())
            {
                Exception e = new  Exception("成本中心编码转换出错,转换编码" + temp + "未定义");
                throw e;
            }
        }

        return resultString;
    }
    public static void main(String[] args)
    {

    }
}
