package com.sinosoft.lis.fininterface.TransItem;

import com.sinosoft.lis.fininterface.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.LICodeTransDB;
import com.sinosoft.lis.vschema.LICodeTransSet;

/**
 * <p>Title: ManageCom</p>
 *
 * <p>Description: 转换机构代码</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author jianan
 * @version 1.0
 */
public class ManageCom implements TransType {

    private LICodeTransSet mLICodeTransSet;
    public ManageCom() throws Exception
    {
        try
        {
            LICodeTransDB tLICodeTransDB = new  LICodeTransDB();
            tLICodeTransDB.setCodeType("ManageCom");
            mLICodeTransSet = tLICodeTransDB.query();
            if(mLICodeTransSet.size()<=0)
            {
                Exception e = new  Exception("机构转换编码未定义");
                throw e;
            }

        }
        catch (Exception ex) {

            Exception e = new  Exception("机构编码转换出错" + ex.getMessage());
            throw e;
        }

    }


    public String transInfo(String TransValue,LIAboriginalDataSchema tLIAboriginalDataSchema) throws Exception
    {
        String resultString = "";
        String temp = TransValue;
        int tLength = temp.length();
        if(tLength>6)
        {
            temp = temp.substring(0,6);
        }

        for(int i=1;i<= mLICodeTransSet.size();i++)
        {
            if(mLICodeTransSet.get(i).getCode().trim().equals(temp))
            {
               resultString = mLICodeTransSet.get(i).getCodeAlias();
               break;
            }
            if(i==mLICodeTransSet.size())
            {
                Exception e = new  Exception("机构编码转换出错,转换编码" + temp + "未定义");
                throw e;
            }
        }
       
        //if(tLIAboriginalDataSchema.getManageCom().subSequence(0, 4) != tLIAboriginalDataSchema.getExecuteCom().substring(0, 4)){
        //	resultString = "C" + resultString;
        // }
        return resultString;
        
    }
    
    public LIAboriginalDataSchema ManageComTrans(LIAboriginalDataSchema tLIAboriginalDataSchema) throws Exception{
    	String tManageCom = tLIAboriginalDataSchema.getManageCom();
    	if(tManageCom.length()<=6){
    		return tLIAboriginalDataSchema;
    	}
    	else{
    		tLIAboriginalDataSchema.setManageCom(tManageCom.substring(0,6));
    		return tLIAboriginalDataSchema;
    	}
    }
    
    public static void main(String[] args)
    {
     try
     {
         LIAboriginalDataSchema tLIAboriginalDataSchema = new
                 LIAboriginalDataSchema();
//         tLIAboriginalDataSchema.setAgentCom("");
         ManageCom tManageCom = new ManageCom();
         String t = tManageCom.transInfo("86110000",tLIAboriginalDataSchema);
        System.out.println(t);
     }
     catch(Exception e)
     {
         System.out.println(e.getMessage());
     }

    }
}
