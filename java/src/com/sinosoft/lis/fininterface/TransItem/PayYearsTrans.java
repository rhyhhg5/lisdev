package com.sinosoft.lis.fininterface.TransItem;

import com.sinosoft.lis.fininterface.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.vschema.LICodeTransSet;
import com.sinosoft.lis.db.LICodeTransDB;
import com.sinosoft.lis.vschema.LAComSet;
//import com.sinosoft.lis.db.*;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LBContDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LBGrpContDB;
import com.sinosoft.utility.Reflections;;

/**
 * <p>Title: PayYearsTrans</p>
 *
 * <p>Description: 缴费年期转换类</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author linyun
 * @version 1.0
 */
public class PayYearsTrans implements TransType {

    public String transInfo(String TransValue,LIAboriginalDataSchema tLIAboriginalDataSchema) throws Exception
    {
        try
        {
        	String tResult = new String();
        	tResult = Double.toString(tLIAboriginalDataSchema.getStandByNum1());
        	if (tLIAboriginalDataSchema.getPayIntv()==0||tLIAboriginalDataSchema.getPayIntv()==-1) {
    			tResult = "";
    		}
        	return tResult;
        }
        catch (Exception ex) {

            Exception e = new  Exception("缴费年期转换出错！原因是：" + ex.getMessage());
            throw e;
        }

    }

}
