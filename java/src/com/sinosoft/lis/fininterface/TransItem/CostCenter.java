package com.sinosoft.lis.fininterface.TransItem;

import com.sinosoft.lis.fininterface.TransType;
import com.sinosoft.lis.schema.LIAboriginalDataSchema;
import com.sinosoft.lis.db.LABranchGroupDB; 

/**
 * <p>Title: CostCenterTrans</p>
 *
 * <p>Description: 资产负债类科目成本中心转换</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author jianan
 * @version 1.0
 */
public class CostCenter implements TransType {
    public CostCenter() {
    }


    public String transInfo(String TransValue,LIAboriginalDataSchema tLIAboriginalDataSchema) throws Exception
    {
        String temp = "";
        LABranchGroupDB tLABranchGroupDB=new LABranchGroupDB();
        tLABranchGroupDB.setAgentGroup(tLIAboriginalDataSchema.getAgentGroup());
        if(!tLABranchGroupDB.getInfo())
        {
        	 Exception e = new  Exception("该代理机构编码在labranchgroup找不到");
             throw e;
        }
        else
        {
        	 
        	if(tLABranchGroupDB.getSchema().getCostCenter()==null||"".equals(tLABranchGroupDB.getSchema().getCostCenter())) 
        		{
        		 Exception e = new  Exception(TransValue+"机构的成本中心代码为空");
                 throw e;
        		}
        	else
        	{
        		temp=tLABranchGroupDB.getSchema().getCostCenter();
        	}
        }
        return temp;
    }
    
    public static void main(String []args)throws Exception{
    	LIAboriginalDataSchema oLIAboriginalDataSchema = new LIAboriginalDataSchema();
    	oLIAboriginalDataSchema.setManageCom("8699999");
    	CostCenter oCostCenter = new CostCenter();
    	String sCode = oCostCenter.transInfo("111", oLIAboriginalDataSchema);
    	System.out.println("虚拟机构 ：" + sCode);
    }
}
