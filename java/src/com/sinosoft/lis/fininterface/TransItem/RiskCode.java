package com.sinosoft.lis.fininterface.TransItem;

import com.sinosoft.lis.db.LICodeTransDB;
import com.sinosoft.lis.schema.LIAboriginalDataSchema;
import com.sinosoft.lis.vschema.LICodeTransSet;

public class RiskCode {
   
	
	private LICodeTransSet mLICodeTransSet;	
    public RiskCode() throws Exception
    {
        try
        {
            LICodeTransDB tLICodeTransDB = new  LICodeTransDB();
            tLICodeTransDB.setCodeType("RiskCode");
            mLICodeTransSet = tLICodeTransDB.query();
            if(mLICodeTransSet.size()<=0)
            {
                Exception e = new  Exception("机构转换编码未定义");
                throw e;
            }
            System.out.println("/****************riskcode************************/");
            System.out.println("mLICodeTransSet.encode=" + mLICodeTransSet.encode());
            System.out.println("/****************riskcode*************************/");

        }
        catch (Exception ex) {

            Exception e = new  Exception("机构编码转换出错" + ex.getMessage());
            throw e;
        }

    }
    /**
     * 
     * @param TransValue
     * @param tLIAboriginalDataSchema
     * @return
     * @throws Exception
     */
    public  String transInfo(String sTransValue) throws Exception
    {
        String resultString = "";
        String sTemp = sTransValue;
//        int tLength = sTemp.length();
//        if(tLength>6)
//        {
//            sTemp = sTemp.substring(0,6);
//        }

        //为空时用0来填充
        if(sTemp == null || sTemp.equals("")){
        	
        	return resultString = "0";   
        	
        }else{
            for(int i=1;i<= mLICodeTransSet.size();i++)
            {               
            	if(mLICodeTransSet.get(i).getCode().trim().equals(sTemp))
                {
                   resultString = mLICodeTransSet.get(i).getCodeAlias();
                   break;
                }
                if(i==mLICodeTransSet.size())
                {
                    //Exception e = new  Exception("机构编码转换出错,转换编码" + sTemp + "未定义");
                    //throw e;
                	resultString = sTemp;
                }
            }
        }
        return resultString;
    }
    
    public static void main(String[] args) throws Exception{
    	
    	RiskCode oRiskCode = new RiskCode();
    	String sRiskCode = oRiskCode.transInfo("");
    	System.out.println(sRiskCode);   
    }

}
