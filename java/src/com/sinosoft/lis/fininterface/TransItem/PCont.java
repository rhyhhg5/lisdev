package com.sinosoft.lis.fininterface.TransItem;

import com.sinosoft.lis.fininterface.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.LICodeTransDB;
import com.sinosoft.lis.vschema.LICodeTransSet;


/**
 * <p>Title: PCont</p>
 *
 * <p>Description: 转换机构代码</p>
 *
 * <p>Copyright: Copyright (c) 2008</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author jianan
 * @version 1.0
 */
public class PCont implements TransType {

    private LICodeTransSet mLICodeTransSet;
    public PCont() throws Exception
    {
        try
        {
//            LICodeTransDB tLICodeTransDB = new  LICodeTransDB();
//            tLICodeTransDB.setCodeType("PCont");
//            mLICodeTransSet = tLICodeTransDB.query();
//            if(mLICodeTransSet.size()<=0)
//            {
//                Exception e = new  Exception("期数未定义");
//                throw e;
//            }

        }
        catch (Exception ex) {

            Exception e = new  Exception("期数转换出错" + ex.getMessage());
            throw e;
        }

    }


    public String transInfo(String TransValue,LIAboriginalDataSchema tLIAboriginalDataSchema) throws Exception
    {
        String resultString = "";
        String temp = TransValue;
        if(tLIAboriginalDataSchema.getPayIntv()==0) 
        {
        	resultString="01";// 期数 趸交
        }
        else
        {
        	if("S".equals(tLIAboriginalDataSchema.getFirstTermFlag()))
        	{
        		resultString="02";// 期数 首期非趸交
        	}
        	else if("X".equals(tLIAboriginalDataSchema.getFirstTermFlag()))
        	{
        		resultString="03";// 期数 续期非趸交
        	}
        	else
        	{
        		  Exception e = new  Exception("首期续期标志为空");
                  throw e;
        	}
        }
        
        return resultString;
        
    }
    
   
    public static void main(String[] args)
    {
     try
     {
        
     }
     catch(Exception e)
     {
         System.out.println(e.getMessage());
     }

    }
}
