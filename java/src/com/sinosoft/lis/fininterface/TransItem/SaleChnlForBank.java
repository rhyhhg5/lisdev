package com.sinosoft.lis.fininterface.TransItem;

import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LICodeTransDB;
import com.sinosoft.lis.vschema.LICodeTransSet;

/**
 * <p>Title: SaleChnlForBank</p>
 *
 * <p>Description: 销售渠道-银行代理中介机构转换类</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author lijs
 * @version 1.0
 */

public class SaleChnlForBank {

	
	public static void main(String[] args)throws Exception {
		String strSQL = "8633090300";
		String str = getBankCode(strSQL);
		System.out.println(str);
	}
	
	/**
	 * @param sAgentCom 银行代理--中介机构代码
	 * @return  BankCode 对应的财务接口中的银行代码
	 * @throws Exception
	 */
	public static String getBankCode(String sAgentCom) throws Exception{
		
		String sCode = ""; //存放销售渠道中的中介机构中的对应银行类别码
		String sBankCode = "";
		/*** 中介机构的第5,6位表示银行类别编码 ****/
		sCode = sAgentCom.substring(4, 6);
        LICodeTransDB tLICodeTransDB = new  LICodeTransDB();
        tLICodeTransDB.setCodeType("SaleChnlBank");
        LICodeTransSet mLICodeTransSet = tLICodeTransDB.query();
        for(int i=1;i<= mLICodeTransSet.size();i++){
        	if(mLICodeTransSet.get(i).getCode().trim().equals(sCode)){
               sBankCode = mLICodeTransSet.get(i).getCodeAlias();
               break;
            }
            if(i==mLICodeTransSet.size()){
                Exception e = new  Exception("销售渠道银行代理中介机构转换出错,转换编码" + sCode + "未定义");
                throw e;
            }
        }
		return sBankCode;
	}
	
}
