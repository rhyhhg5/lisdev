package com.sinosoft.lis.fininterface;

import java.io.File;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;


import jxl.*;
import jxl.write.*;
import jxl.write.biff.RowsExceededException;
import jxl.format.UnderlineStyle;

public class BusCompetiReportBL
{
	public  CErrors mErrors=new CErrors();
	private VData mInputData= new VData();
	private TransferData tTransferData = new TransferData();
	/** 全局数据 */
	private GlobalInput mGlobalInput =new GlobalInput() ;
	/** 数据操作字符串 */

    private String mOperater;
    private String mManageCom;
    private String ImportPath;
    private String VersionNo ;
    private String Bdate;
    private String Edate;
    private String YBdate;
    private String YEdate;
    private MMap map=new MMap();
    
	public BusCompetiReportBL()
	{

	}
    
    
    public boolean submitData(VData cInputData,String path) throws RowsExceededException, WriteException,Exception
	{
		//将操作数据拷贝到本类中
		this.mInputData = cInputData;
		System.out.println("---UpLoadWriteFinBL getInputData---");
		
		if (!getInputData(cInputData)) {
	            return false;
	        }
	      

		if (!checkData())
		{
			return false;
		}

		//进行业务处理
		if (!WriteXls(path)) 
		{
			return false;
		}

		if (!prepareOutputData())
		{
			return false;
		}
		
		if(!pubSubmit())
		{
			return false;
		}

		System.out.println("End UpLoadWriteFinBL Submit...");
		mInputData=null;
		return true;
	}
    
    
    /**
     * 创建Excel
     * @throws WriteException 
     * @throws RowsExceededException 
     */
    public boolean WriteXls(String path)
    {
    	
        	
    	WritableWorkbook book=null;
			try {
				
				//建新的Excel
				System.out.println(path+"ModelExcel.xls");
				System.out.println("fin-"+mOperater.trim()+".xls");
				 book= Workbook.createWorkbook(new File(path+"fin-"+mOperater.trim()+".xls"),Workbook.getWorkbook(new File(path+"ModelExcel.xls")));
				
				//获取Excel工作薄
				WritableSheet sh1=book.getSheet(0);
				WritableSheet sh2=book.getSheet(1);
				WritableSheet sh3=book.getSheet(2);
				
				//查询函数
				String str1=" select managecom,dec(cast(nvl(sum(money1),0) as float)/10000+0.005,20,2),'','',dec(cast(nvl(sum(money2),0) as float)/10000+0.005,20,2),'','',dec(cast(nvl(sum(money3),0) as float)/10000+0.005,20,2),'','',dec(cast(nvl(sum(money4),0) as float)/10000+0.005,20,2),'','',dec(cast(nvl(sum(money5),0) as float)/10000+0.005,20,2),'','',dec(cast(nvl(sum(money6),0) as float)/10000+0.005,20,2)  from ("
					       +" select  (case managecom when '9210' then '9210'"                                                                           
						   +" when '9370' then '9370'"                                                                                           
						   +" when '9440' then '9440'"                                                                                           
						   +" else substr(managecom,1,2) || '00' end) as managecom, money1 as money1,money2 as money2,money3 as money3,money4 as money4,money5 as money5,money6 as money6  from( "
						   +" select aa.managecom,aa.money as money1,bb.money as money2,cc.money as money3,dd.money as money4,ee.money as money5,ff.money as money6 from ("
                           +" select managecom,sum(money) as money from (select managecom,sum(case finitemtype when 'D' then -summoney else summoney end ) as money"
                           +" from lidatatransresult a where accountcode in('6031000000','7031000000') and length(managecom)=4"
                           +"  and exists(" 
                           +" select 1 from ljagetendorse b  where feeoperationtype='WT' and a.contno=b.contno and b.makedate between '"+YBdate+"' and '"+YEdate+"')"
                           +" and accountdate between '"+Bdate+"' and '"+Edate+"' group by managecom"
                           +" union all"
                           +" select managecom,sum(case finitemtype when 'D' then -summoney else summoney end ) as money from lidatatransresult a"
                           +" where accountcode in('6031000000','7031000000') and length(managecom)=4  and exists("
		                   +" select 1 from ljagetendorse b  where feeoperationtype='WT'  and a.contno=b.grpcontno and b.makedate between '"+YBdate+"' and '"+YEdate+"') " 
		                   +" and accountdate between '"+Bdate+"' and '"+Edate+"' group by managecom )as a1 where 1=1 group by managecom) as aa left join"
                           +" (select managecom,sum(case finitemtype when 'D' then -summoney else summoney end ) as money"
                           +" from lidatatransresult a where accountcode in('6031000000','7031000000') and length(managecom)=4  and substr(a.costcenter,5,2)='91'"
                           +" and exists(select 1 from ljagetendorse b  where feeoperationtype='WT'  and a.contno=b.grpcontno and b.makedate between '"+YBdate+"' and '"+YEdate+"')"
                           +" and accountdate between '"+Bdate+"' and '"+Edate+"' group by managecom)as bb on aa.managecom=bb.managecom left join"
                           +" (select managecom,sum(case finitemtype when 'D' then -summoney else summoney end ) as money from lidatatransresult a where accountcode in('6031000000','7031000000')"
                           +" and length(managecom)=4  and substr(a.costcenter,5,2)='90' and exists(select 1 from ljagetendorse b  where feeoperationtype='WT'  and a.contno=b.contno and b.makedate between '"+YBdate+"' and '"+YEdate+"')"
                           +" and accountdate between '"+Bdate+"' and '"+Edate+"' group by managecom )as cc on aa.managecom=cc.managecom left join"
                           +" (select managecom,sum(case finitemtype when 'D' then -summoney else summoney end ) as money from lidatatransresult a where accountcode in('6031000000','7031000000')"
                           +" and length(managecom)=4  and substr(a.costcenter,5,2)='92' and exists(select 1 from ljagetendorse b  where feeoperationtype='WT'  and a.contno=b.contno and b.makedate between '"+YBdate+"' and '"+YEdate+"')"
                           +" and accountdate between '"+Bdate+"' and '"+Edate+"' group by managecom )as dd on aa.managecom=dd.managecom left join"
                           +" (select managecom,sum(case finitemtype when 'D' then -summoney else summoney end ) as money from lidatatransresult a where accountcode in('6031000000','7031000000')"
                           +" and length(managecom)=4  and substr(a.costcenter,5,2)='91' and substr(a.riskcode,1,4) in('1603','1611','1612','1613','1616','4602','1614','5603','5604','5605','1301')"
                           +" and markettype in('2','3','4','5','6','7','8','99') and exists(select 1 from ljagetendorse b  where feeoperationtype='WT'  and a.contno=b.grpcontno and b.makedate between '"+YBdate+"' and '"+YEdate+"')"
                           +" and accountdate between '"+Bdate+"' and '"+Edate+"' group by managecom )as ee on aa.managecom=ee.managecom left join"
                           +" (select managecom,sum(case finitemtype when 'D' then -summoney else summoney end ) as money from lidatatransresult a where accountcode in('6031000000','7031000000') and length(managecom)=4"
                           +"  and substr(a.riskcode,1,4) in('1703','1704') and exists(select 1 from ljagetendorse b  where feeoperationtype='WT'  and a.contno=b.grpcontno and b.makedate between '"+YBdate+"' and '"+YEdate+"')"
                           +" and accountdate between '"+Bdate+"' and '"+Edate+"' group by managecom)as ff on aa.managecom=ff.managecom )as hh)as zz where 1=1  group by managecom";
				
				String str2="select * from ( select  char(rownumber() over()) as id,substr(aa.managecom,1,2) || '00',aa.managecom,dec(cast(nvl(aa.money,0) as float)/10000+0.005,20,2),dec(cast(nvl(bb.money,0) as float)/10000+0.005,20,2),dec(cast(nvl(cc.money,0) as float)/10000+0.005,20,2),dec(cast(nvl(dd.money,0) as float)/10000+0.005,20,2) from ("
                           +" select managecom,sum(money) as money from ("
                           +" select managecom,sum(case finitemtype when 'D' then -summoney else summoney end ) as money"
                           +" from lidatatransresult a where accountcode in('6031000000','7031000000') and length(managecom)=4 "
                           +" and exists(select 1 from ljagetendorse b  where feeoperationtype='WT'  and a.contno=b.contno and b.makedate between '"+YBdate+"' and '"+YEdate+"') and accountdate between '"+Bdate+"' and '"+Edate+"'"
                           +" group by managecom"
                           +" union all"
                           +" select managecom,sum(case finitemtype when 'D' then -summoney else summoney end ) as money"
                           +" from lidatatransresult a where accountcode in('6031000000','7031000000') and length(managecom)=4 "
                           +" and exists(select 1 from ljagetendorse b  where feeoperationtype='WT' and a.contno=b.grpcontno and b.makedate between '"+YBdate+"' and '"+YEdate+"') " 
                           +" and accountdate between '"+Bdate+"' and '"+Edate+"' group by managecom) as a1 where 1=1  group by managecom)as aa left join ("
                           +" select managecom,sum(case finitemtype when 'D' then -summoney else summoney end ) as money"
                           +" from lidatatransresult a where accountcode in('6031000000','7031000000') and length(managecom)=4  and substr(a.costcenter,5,2)='91'"
                           +" and exists (select 1 from lmriskapp where riskcode=a.riskcode  and risktype='A' and riskperiod in('M','S'))"
                           +" and exists(select 1 from ljagetendorse b  where feeoperationtype='WT' and a.contno=b.grpcontno and b.makedate between '"+YBdate+"' and '"+YEdate+"')" 
                           +" and accountdate between '"+Bdate+"' and '"+Edate+"' group by managecom) as bb on aa.managecom=bb.managecom left join ("
                           +" select managecom,sum(money) as money from ("
                           +" select managecom,sum(case finitemtype when 'D' then -summoney else summoney end ) as money"
                           +" from lidatatransresult a where accountcode in('6031000000','7031000000') and length(managecom)=4 "
                           +" and substr(a.costcenter,5,2)='90' and substr(a.riskcode,1,4) in('2302','2303','2305','2306','2309','2401','2403','2301','3303','3304','3309','3401','2402','3302','2308','3301')"
                           +" and pcont='21' and exists(select 1 from ljagetendorse b  where feeoperationtype='WT' and a.contno=b.contno and b.makedate between '"+YBdate+"' and '"+YEdate+"')"
                           +" and accountdate between '"+Bdate+"' and '"+Edate+"' group by managecom"
                           +" union all"
                           +" select managecom,sum(case finitemtype when 'D' then -summoney else summoney end ) as money"
                           +" from lidatatransresult a where accountcode in('6031000000','7031000000')"
                           +" and length(managecom)=4  and substr(a.costcenter,5,2)='90'"
                           +" and substr(a.riskcode,1,4) in('3307','3308','3312','3313','3316','3317','3318','3319','5303','5301','5302','5701','5401') and pcont='21' and premiumtype in('10','20','99')"
                           +" and exists(select 1 from ljagetendorse b  where feeoperationtype='WT' and a.contno=b.contno and b.makedate between '2011-12-04' and '2011-12-15')"
                           +" and accountdate between '"+Bdate+"' and '"+Edate+"' group by managecom ) as c1  where 1=1 group by managecom) as cc on aa.managecom=cc.managecom left join("
                           +" select managecom,sum(case finitemtype when 'D' then -summoney else summoney end ) as money"
                           +" from lidatatransresult a where accountcode in('6031000000','7031000000') and length(managecom)=4 "
                           +" and substr(a.riskcode,1,4) in('1703','1704')"
                           +" and exists(select 1 from ljagetendorse b  where feeoperationtype='WT'  and a.contno=b.grpcontno and b.makedate between '"+YBdate+"' and '"+YEdate+"')"
                           +" and accountdate between '"+Bdate+"' and '"+Edate+"' group by managecom) as dd on aa.managecom=dd.managecom"
                           +" where aa.managecom not like '%00' and aa.managecom not in('9210','9370','9440') order by aa.money desc )as ee with ur";
				
				String str3="select managecom,dec(cast(nvl(sum(money1),0) as float)/10000+0.005,20,2),'','',dec(cast(nvl(sum(money2),0) as float)/10000+0.005,20,2),'','',dec(cast(nvl(sum(money3),0) as float)/10000+0.005,20,2),'','',dec(cast(nvl(sum(money4),0) as float)/10000+0.005,20,2),'','',dec(cast(nvl(sum(money5),0) as float)/10000+0.005,20,2),'','',dec(cast(nvl(sum(money6),0) as float)/10000+0.005,20,2) from ("                   
							+" select"                                                                                                            
							+" (case managecom when '9210' then '9210'"                                                                           
							+" when '9370' then '9370'"                                                                                           
							+" when '9440' then '9440'"                                                                                           
							+" else substr(managecom,1,2) || '00' end) as managecom,"                                                             
							+" max(case id when '1' then money else 0 end) as money1,"                                                            
							+" max(case id when '2' then money else 0 end) as money2,"                                                            
							+" max(case id when '3' then money else 0 end) as money3,"                                                            
							+" max(case id when '4' then money else 0 end) as money4,"                                                            
							+" max(case id when '5' then money else 0 end) as money5,"                                                            
							+" max(case id when '6' then money else 0 end) as money6"                                                             
							+" from("                                                                                                             
							+" select managecom,'1' as id,sum(case finitemtype when 'D' then -summoney else summoney end ) as money"              
							+" from lidatatransresult a"                                                                                          
							+" where accountcode in('6031000000','7031000000')"                                                                   
							+" and length(managecom)=4"                                                                                                                                                                         
							+" and substr(a.costcenter,5,2)='91'"                                                                                 
							+" and exists(select 1 from ljagetendorse b"                                                                          
							+" where feeoperationtype='WT'"                                                                                       
							+" and a.contno=b.grpcontno and makedate between '"+YBdate+"' and '"+YEdate+"')"                                      
							+" and accountdate between '"+Bdate+"' and '"+Edate+"'"                                                             
							+" group by managecom"                                                                                                
							+" union all"                                                                                                         
							+" select managecom,'2' as id,sum(case finitemtype when 'D' then -summoney else summoney end ) as money"              
							+" from lidatatransresult a"                                                                                          
							+" where accountcode in('6031000000','7031000000')"                                                                   
							+" and length(managecom)=4"                                                                                                                                                                        
							+" and substr(a.costcenter,5,2)='91'"                                                                                 
							+" and exists (select 1 from lmriskapp"                                                                               
							+" where riskcode=a.riskcode  and risktype='A' and riskperiod in('M','S'))"                                           
							+" and exists(select 1 from ljagetendorse b"                                                                          
							+" where feeoperationtype='WT'"                                                                                       
							+" and a.contno=b.grpcontno and makedate between '"+YBdate+"' and '"+YEdate+"')"                                      
							+" and accountdate between '"+Bdate+"' and '"+Edate+"'"                                                             
							+" group by managecom"                                                                                                
							+" union all"                                                                                                        
							+" select managecom,'3' as id,sum(case finitemtype when 'D' then -summoney else summoney end ) as money"              
							+" from lidatatransresult a"                                                                                          
							+" where accountcode in('6031000000','7031000000')"                                                                   
							+" and length(managecom)=4"                                                                                                                                                                       
							+" and substr(a.costcenter,5,2)='90'"                                                                                 
							+" and exists(select 1 from ljagetendorse b"                                                                          
							+" where feeoperationtype='WT'"                                                                                       
							+" and a.contno=b.contno and makedate between '"+YBdate+"' and '"+YEdate+"')"                                         
							+" and accountdate between '"+Bdate+"' and '"+Edate+"'"                                                             
							+" group by managecom"                                                                                                
							+" union all"                                                                                                         
							+" select managecom ,'4' as id,sum(ff.money) as money from ("                                                         
							+" select managecom,sum(case finitemtype when 'D' then -summoney else summoney end ) as money"                        
							+" from lidatatransresult a"                                                                                          
							+" where accountcode in('6031000000','7031000000')"                                                                   
							+" and length(managecom)=4"                                                                                                                                                                          
							+" and substr(a.costcenter,5,2)='90'"                                                                                 
							+" and substr(a.riskcode,1,4) in('2302','2303','2305','2306','2309','2401','2403','2301','3303','3304','3309','3401','2402','3302','2308','3301')"
							+" and pcont='21'"                                                                                                    
							+" and exists(select 1 from ljagetendorse b"                                                                          
							+" where feeoperationtype='WT'"                                                                                       
							+" and a.contno=b.contno and makedate between '"+YBdate+"' and '"+YEdate+"')"                                         
							+" and accountdate between '"+Bdate+"' and '"+Edate+"'"                                                             
							+" group by managecom"                                                                                                
							+" union all"                                                                                                         
							+" select managecom,sum(case finitemtype when 'D' then -summoney else summoney end ) as money"                        
							+" from lidatatransresult a"                                                                                          
							+" where accountcode in('6031000000','7031000000')"                                                                   
							+" and length(managecom)=4"                                                                                                                                                                         
							+" and substr(a.costcenter,5,2)='90'"                                                                                 
							+" and substr(a.riskcode,1,4) in('3307','3308','3312','3313','3316','3317','3318','3319','5303','5301','5302','5701','5401')"                            
							+" and pcont='21'"                                                                                                    
							+" and premiumtype in('10','20','99')"                                                                                
							+" and exists(select 1 from ljagetendorse b"                                                                          
							+" where feeoperationtype='WT'"                                                                                       
							+" and a.contno=b.contno and makedate between '"+YBdate+"' and '"+YEdate+"')"                                         
							+" and accountdate between '"+Bdate+"' and '"+Edate+"'"                                                             
							+" group by managecom"                                                                                                
							+" )as ff"                                                                                                            
							+" where 1=1"                                                                                                         
							+" group by managecom"                                                                                                
							+" union all"                                                                                                         
							+" select managecom,'5' as id,sum(case finitemtype when 'D' then -summoney else summoney end ) as money"              
							+" from lidatatransresult a"                                                                                          
							+" where accountcode in('6031000000','7031000000')"                                                                   
							+" and length(managecom)=4"                                                                                                                                                                         
							+" and substr(a.costcenter,5,2)='92'"                                                                                 
							+" and exists(select 1 from ljagetendorse b"                                                                          
							+" where feeoperationtype='WT'"                                                                                       
							+" and a.contno=b.contno and makedate between '"+YBdate+"' and '"+YEdate+"')"                                         
							+" and accountdate between '"+Bdate+"' and '"+Edate+"'"                                                             
							+" group by managecom"                                                                                                
							+" union all"                                                                                                         
							+" select"                                                                                                            
							+" managecom,'6' as id,sum(case finitemtype when 'D' then -summoney else summoney end ) as money"                     
							+" from lidatatransresult a"                                                                                          
							+" where accountcode in('6031000000','7031000000')"                                                                   
							+" and length(managecom)=4"                                                                                                                                                                                                                                                       
							+" and substr(a.riskcode,1,4) in('1703','1704')"                                                                      
							+" and exists(select 1 from ljagetendorse b"                                                                          
							+" where feeoperationtype='WT'"                                                                                       
							+" and a.contno=b.grpcontno and makedate between '"+YBdate+"' and '"+YEdate+"')"                                      
							+" and accountdate between '"+Bdate+"' and '"+Edate+"'"                                                             
							+" group by managecom"                                                                                                
							+" )as aa"                                                                                                            
							+" where 1=1"                                                                                                         
							+" group by managecom"                                                                                                
							+" )as bb"                                                                                                            
							+" where 1=1"                                                                                                         
							+" group by managecom"                                                                                                
							+" with ur";                                                                                     
				
				//定义单元格格式
				WritableFont wfc = new WritableFont(WritableFont.ARIAL, 10,
				WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				jxl.format.Colour.BLACK); // 定义格式 字体 下划线 斜体 粗体 颜色
				WritableCellFormat wcft = new WritableCellFormat(wfc); // 单元格定义
				//wcft.setBackground(jxl.format.Colour.WHITE); // 设置单元格的背景颜色
				wcft.setAlignment(jxl.format.Alignment.LEFT); // 设置对齐方式   
				wcft.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
				wcft.setBorder(jxl.format.Border.ALL,jxl.format.BorderLineStyle.THIN); 
				
				//写入2011年优秀省级分公司奖工作薄
				WriteSheet(sh1,str1,17,6,wcft,1);
				
				//写入2011年优秀地市级机构奖
				WriteSheet(sh2,str2,7,5,wcft,3);
				
				//写入2011年优秀省级分公司渠道负责人奖
				WriteSheet(sh3,str3,17,7,wcft,1);
				
				//写入
				book.write();			
				book.close();
				
	        	System.out.println("true");
				return true;
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				 CError tError = new CError();
		            tError.moduleName = "BusCompetiReportBL";
		            tError.functionName = "getInputData";
		            tError.errorMessage = "生成报表失败！";
		            this.mErrors.addOneError(tError);
		            return false;
			}

            
    }
//数据写入Excel中    
    private boolean WriteSheet(WritableSheet sheet,String sql,int x,int y,WritableCellFormat wf,int FirstSum) throws RowsExceededException, WriteException,Exception
    {
    	try{
    	ExeSQL es = new ExeSQL();
    	SSRS mSSRS = new SSRS();
    	mSSRS = es.execSQL(sql);
    	int maxrow=mSSRS.getMaxRow();
    	for(int a=0;a<x;a++)
    	{
    		for(int i=y;i<=(maxrow+y-1);i++)
	    	{ 
	    		sheet.addCell(new Label(a,(i-1),mSSRS.GetText((i-y+1), a+1),wf));
	    	}
    	}
    	WriteSheetSum(sheet,maxrow,x,y,wf,FirstSum);
    		
    	return true;
    	}catch(Exception e){
    		Exception cv =  new Exception("写入excel文件失败"+e.getMessage());
   		    throw cv;
    	}
    }
//写入合计值
    private boolean WriteSheetSum(WritableSheet sheet,int maxrow,int x,int y,WritableCellFormat wf,int FirstSum) throws Exception
    {
    	try{
    	sheet.addCell(new Label(0,(y+maxrow-1),"合计",wf));
    	for(int i=1;i<x;i++){
    	if(i<FirstSum) {
    		sheet.addCell(new Label(i,(y+maxrow-1),"",wf));
    		continue;
    	}
    	String cellCont=sheet.getCell(i,y-1).getContents();
    	if(cellCont==null || cellCont.equals("")){
    	   sheet.addCell(new Label(i,(y+maxrow-1),"",wf));
    	}else{
    		double sum=0;
    		for(int a1=(y-1);a1<(y+maxrow-1);a1++){
    			sum=sum+Double.parseDouble(sheet.getCell(i,a1).getContents().trim());
    		}
    		sum=Math.round(sum*100)/100.0;
    		System.out.println("sum="+sum);
    		sheet.addCell(new Label(i,(y+maxrow-1),Double.toString(sum),wf));
    	}
    	}
    	}catch(Exception e)
    	{
    		 Exception cv =  new Exception("写入合计值失败"+e.getMessage());
    		 throw cv;
    	}
    	return true;
    }
  
    private boolean getInputData(VData cInputData)
	{
		mGlobalInput = (GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0);
		tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData",0);
		if (mGlobalInput == null) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "BusCompetiReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //获得操作员编码
		mOperater = mGlobalInput.Operator;
        if ((mOperater == null) || mOperater.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "BusCompetiReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据Operate失败!";
            this.mErrors.addOneError(tError);

            return false;
        }

        //获得登陆机构编码
        mManageCom = mGlobalInput.ManageCom;
        if ((mManageCom == null) || mManageCom.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "BusCompetiReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输全局公共数据ManageCom失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        Bdate=tTransferData.getValueByName("Bdate").toString();
        if ((Bdate == null) || Bdate.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "BusCompetiReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输保费起始日期失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        Edate=tTransferData.getValueByName("Edate").toString();
        if ((Edate == null) || Edate.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "BusCompetiReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输保费终止日期失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        YBdate=tTransferData.getValueByName("YBdate").toString();
        if ((YBdate == null) || YBdate.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "BusCompetiReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输犹豫期退保起始日期失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        YEdate=tTransferData.getValueByName("YEdate").toString();
        if ((YEdate == null) || YEdate.trim().equals("")) {
            // @@错误处理
            //this.mErrors.copyAllErrors( tLCPolDB.mErrors );
            CError tError = new CError();
            tError.moduleName = "BusCompetiReportBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "前台传输犹豫期退保终止日期失败!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
		return true;
	}
    
	private boolean checkData()
	{
		return true;
	}
	
	private boolean prepareOutputData()
	{
		try
		{
			mInputData.clear();
			mInputData.add(map);
		}
		catch(Exception ex)
		{
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "BusCompetiReportBL";
			tError.functionName = "prepareData";
			tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
 
	/**
	 * 提交数据
	 * @return
	 */
	private boolean pubSubmit()
	{
		// 进行数据提交
		PubSubmit tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, ""))
		{
			// @@错误处理
			this.mErrors.copyAllErrors(tPubSubmit.mErrors);
			CError tError = new CError();
			tError.moduleName = "BusCompetiReportBL";
			tError.functionName = "PubSubmit.submitData";
			tError.errorMessage = "数据提交失败BusCompetiReportBL-->pubSubmit!";
			this.mErrors.addOneError(tError);
			return false;
		}
		return true;
	}
	
    /**
     * 测试
     * @param args
     * @throws WriteException 
     * @throws RowsExceededException 
     */
    public static void main(String[] args)
    {
    	/*BusCompetiReportBL read = new BusCompetiReportBL();
        try {
			//read.WriteXls("D:/");
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
//			System.out.(e.printStackTrace());
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

    }
}
