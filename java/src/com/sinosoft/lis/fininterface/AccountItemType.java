package com.sinosoft.lis.fininterface;


import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.db.LIFinItemDefDB;
import com.sinosoft.lis.db.LIDetailFinItemDefDB;
import com.sinosoft.lis.schema.LIFinItemDefSchema;
import com.sinosoft.lis.schema.LIDetailFinItemDefSchema;
import com.sinosoft.lis.schema.LIInfoFinItemAssociatedSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.vschema.LIDetailFinItemDefSet;
import com.sinosoft.lis.vschema.LIInfoFinItemAssociatedSet;
import com.sinosoft.lis.db.LIInfoFinItemAssociatedDB;
import com.sinosoft.lis.schema.LIAboriginalDataSchema;
import com.sinosoft.lis.schema.LIDataTransResultSchema;
import com.sinosoft.lis.vschema.LIDetailFinItemCodeSet;
import com.sinosoft.lis.db.LIDetailFinItemCodeDB;
import com.sinosoft.lis.schema.LIDetailFinItemCodeSchema;

/**
 * <p>Title: AccountItemType</p>
 *
 * <p>Description:财务科目和处理规则定义 </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft </p>
 *
 * @author jianan
 * @version 1.0
 */
public class AccountItemType {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 科目类型定义 */
    private LIFinItemDefSchema mLIFinItemDefSchema;
    /** 明细科目类型定义 */
    private LIDetailFinItemDefSet mLIDetailFinItemDefSet;
    /** 明细科目类型判断条件 */
    private String ItemCondition[][];
    /** 科目关联核算项*/
    private LIInfoFinItemAssociatedSet mLIInfoFinItemAssociatedSet;
    /** 关联核算项目转换处理类 */
    private ValueTrans mValueTrans[];
    /** 明细科目类型判断条件数值组合对照表 */
    private LIDetailFinItemCodeSet mLIDetailFinItemCodeSet;
    /** 细科目类型判断条件数值组合对照表索引*/
    private LIDetailFinItemCodeSet[] mLIDetailFinItemCodeSetUint;

    public AccountItemType()
    {
    }

    public boolean InitItemDefInfo(String FinItemID) {
        try
        {
            LIFinItemDefDB tLIFinItemDefDB = new LIFinItemDefDB();
            tLIFinItemDefDB.setFinItemID(FinItemID);
            if (!tLIFinItemDefDB.getInfo()) {
            	
                buildError("InitItemDefInfo函数",
                           "科目类型编码为" + FinItemID + "的LIFinItemDef表查询无数据");
                return false;
            }
            if (tLIFinItemDefDB.mErrors.needDealError()) {
                buildError("InitItemDefInfo函数",
                           "科目类型编码为" + FinItemID + "LIFinItemDef表查询出错");
                return false;
            }

            mLIFinItemDefSchema  = new  LIFinItemDefSchema();
            mLIFinItemDefSchema.setSchema(tLIFinItemDefDB.getSchema());
            LIDetailFinItemDefDB tLIDetailFinItemDefDB = new
                    LIDetailFinItemDefDB();
            String tSql = "select * from LIDetailFinItemDef where FinItemID ='" +  FinItemID + "' order by FinItemlevel,JudgementNo";
            mLIDetailFinItemDefSet = tLIDetailFinItemDefDB.executeQuery(tSql);
            if (tLIDetailFinItemDefDB.mErrors.needDealError())
            {
                buildError("InitItemDefInfo函数",
                           "科目类型编码为" + FinItemID + "LIDetailFinItemDef表查询出错");
                return false;
            }
            LIInfoFinItemAssociatedDB tLIInfoFinItemAssociatedDB = new
                    LIInfoFinItemAssociatedDB();
            tLIInfoFinItemAssociatedDB.setFinItemID(FinItemID);
            mLIInfoFinItemAssociatedSet = tLIInfoFinItemAssociatedDB.query();
            if (tLIInfoFinItemAssociatedDB.mErrors.needDealError()) {
                buildError("InitItemDefInfo函数",
                           "科目类型编码为" + FinItemID +
                           "LIInfoFinItemAssociated表查询出错");
                return false;
            }
            LIDetailFinItemCodeDB tLIDetailFinItemCodeDB = new  LIDetailFinItemCodeDB();
            tSql = "select * from LIDetailFinItemCode where FinItemID ='" +  FinItemID + "' order by FinItemlevel,JudgementNo";
            mLIDetailFinItemCodeSet = tLIDetailFinItemCodeDB.executeQuery(tSql);
            if (tLIDetailFinItemCodeDB.mErrors.needDealError()) {
                buildError("InitItemDefInfo函数",
                           "科目类型编码为" + FinItemID +
                           "LIDetailFinItemCode表查询出错");
                return false;
            }
            if(!InitItemCondition())
            {
                return false;
            }
            if(!InitInfoAssociated())
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            buildError("InitItemDefInfo函数",ex.getMessage());
            return false;
        }
       return true;
    }

    public LIDataTransResultSchema DealInfo(LIAboriginalDataSchema tLIAboriginalDataSchema) throws Exception
    {
        LIDataTransResultSchema tLIDataTransResultSchema = new LIDataTransResultSchema();;
        try
        {
            if(mLIFinItemDefSchema.getDealMode().equals("1"))
            {
                if (!BuildItemCode(tLIAboriginalDataSchema,
                                   tLIDataTransResultSchema)) {

                    Exception e = new Exception(this.mErrors.getFirstError());
                    throw e;

                }
                if (!SetAssociatedInfo(tLIAboriginalDataSchema,
                                       tLIDataTransResultSchema)) {
                    Exception e = new Exception(this.mErrors.getFirstError());
                    throw e;
                }
                if (!SetImportanceInfo(tLIAboriginalDataSchema,
                                       tLIDataTransResultSchema)) {
                    Exception e = new Exception(this.mErrors.getFirstError());
                    throw e;
                }
            }
            else if(mLIFinItemDefSchema.getDealMode().equals("2"))
            {
                Class tClass = Class.forName(mLIFinItemDefSchema.getDealSpecialClass());
                AccItemType mAccItemType = (AccItemType) tClass.newInstance();
                tLIDataTransResultSchema = mAccItemType.DealInfo(tLIAboriginalDataSchema);
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
            throw ex;
        }
        return tLIDataTransResultSchema;
    }


    private boolean InitItemCondition()
    {
        LIAboriginalDataSchema tLIAboriginalDataSchema = new  LIAboriginalDataSchema();
        if(mLIDetailFinItemDefSet.size()>0)
        {
            ItemCondition = new  String[mLIDetailFinItemDefSet.size()][];
            for (int i = 0; i < mLIDetailFinItemDefSet.size(); i++)
            {
                LIDetailFinItemDefSchema tLIDetailFinItemDefSchema =
                        mLIDetailFinItemDefSet.get(i+1);
                String tLevelCondition[] = tLIDetailFinItemDefSchema.
                                           getLevelCondition().trim().split(",");
                ItemCondition[i] = new  String[tLevelCondition.length];
                for (int j = 0; j < tLevelCondition.length; j++)
                {
                    String temp = tLevelCondition[j];
                    if (tLIAboriginalDataSchema.getFieldIndex(temp) < 0)
                    {
                        buildError("InitItemCondition",
                                   "科目定义编号为"+ mLIFinItemDefSchema.getFinItemID()+ "，LIAboriginalData表中没有字段" + temp);
                        return false;
                    }
                    else
                    {
                       ItemCondition[i][j] = temp;
                    }
                }
            }

            if(mLIDetailFinItemCodeSet.size()==0)
            {
                buildError("InitItemCondition",
                            "科目定义编号为"+ mLIFinItemDefSchema.getFinItemID()+ "的LIDetailFinItemDef表定义存在明细科目但LIDetailFinItemCode数据定义为空");
                        return false;
            }

            mLIDetailFinItemCodeSetUint = new LIDetailFinItemCodeSet[mLIDetailFinItemDefSet.size()];
            int tempIndex=0;
            String tempJudgementNo = mLIDetailFinItemDefSet.get(1).getJudgementNo();
            if(!tempJudgementNo.equals(mLIDetailFinItemCodeSet.get(1).getJudgementNo()))
            {
                buildError("InitItemCondition",
                             "科目定义编号为"+ mLIFinItemDefSchema.getFinItemID()+ "的LIDetailFinItemDef表定义和LIDetailFinItemCode数据定义排序不匹配，LIDetailFinItemCode表可能缺少或多余定义");
                        return false;
            }

            for(int i = 1;i<=mLIDetailFinItemCodeSet.size();i++)
            {
                 if(i==1)
                 {
                    mLIDetailFinItemCodeSetUint[tempIndex] = new LIDetailFinItemCodeSet();
                 }

                 if(mLIDetailFinItemCodeSet.get(i).getJudgementNo().equals(tempJudgementNo))
                 {
                     mLIDetailFinItemCodeSetUint[tempIndex].add(mLIDetailFinItemCodeSet.get(i));
                 }
                 else
                 {
                     tempIndex++;
                     if(tempIndex>=mLIDetailFinItemDefSet.size())
                     {
                         buildError("InitItemCondition",
                                      "科目定义编号为"+ mLIFinItemDefSchema.getFinItemID()+ "的LIDetailFinItemDef表定义和LIDetailFinItemCode数据定义不匹配，LIDetailFinItemCode表可能缺少或多余定义");
                         return false;
                     }
                     tempJudgementNo = mLIDetailFinItemCodeSet.get(i).getJudgementNo();
                     if(!tempJudgementNo.equals(mLIDetailFinItemDefSet.get(tempIndex+1).getJudgementNo()))
                     {
                         buildError("InitItemCondition",
                                      "科目定义编号为"+ mLIFinItemDefSchema.getFinItemID()+ "的LIDetailFinItemDef表定义和LIDetailFinItemCode数据定义排序不匹配，LIDetailFinItemCode表可能缺少或多余定义");
                                 return false;
                     }
                     mLIDetailFinItemCodeSetUint[tempIndex] = new LIDetailFinItemCodeSet();
                     mLIDetailFinItemCodeSetUint[tempIndex].add(mLIDetailFinItemCodeSet.get(i));
                 }

            }




        }
        return true;
    }

    private boolean InitInfoAssociated()
    {
        LIAboriginalDataSchema tLIAboriginalDataSchema = new  LIAboriginalDataSchema();
        LIDataTransResultSchema tLIDataTransResultSchema = new  LIDataTransResultSchema();
        if(mLIInfoFinItemAssociatedSet.size()>0)
        {
            mValueTrans = new ValueTrans[mLIInfoFinItemAssociatedSet.size()];
            for (int i = 0; i < mLIInfoFinItemAssociatedSet.size(); i++)
            {
                LIInfoFinItemAssociatedSchema tLIInfoFinItemAssociatedSchema =
                        mLIInfoFinItemAssociatedSet.get(i+1);
                String tempFountainID = tLIInfoFinItemAssociatedSchema.getFountainID();
                String tempTargetID = tLIInfoFinItemAssociatedSchema.getTargetID();
                if (tLIAboriginalDataSchema.getFieldIndex(tempFountainID) < 0) {
                    buildError("InitInfoAssociated",
                               "LIInfoFinItemAssociated表核算段" + tempFountainID+"在LIAboriginalData表中不存在（注意大小写）");
                    return false;
                }
                if(tLIDataTransResultSchema.getFieldIndex(tempTargetID)<0)
                {
                    buildError("InitInfoAssociated",
                    		"LIInfoFinItemAssociated表核算段" + tempFountainID+"在LIAboriginalData表中不存在（注意大小写）");
                    return false;
                }
                mValueTrans[i] = new ValueTrans();
                if(!mValueTrans[i].InitInfo(tLIInfoFinItemAssociatedSchema))
                {
                    this.mErrors.copyAllErrors(mValueTrans[i].mErrors);
                    return false;
                }

            }
        }
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "AccountItemType";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }


    private boolean BuildItemCode(LIAboriginalDataSchema tLIAboriginalDataSchema,LIDataTransResultSchema tLIDataTransResultSchema) {

        try {

            String maincode = mLIFinItemDefSchema.getItemMainCode();
            if (mLIDetailFinItemDefSet.size() > 0)
            {
                String nextJudgementNo = mLIDetailFinItemDefSet.get(1).getJudgementNo();
                while(!nextJudgementNo.equals("end"))
                {
                    int index = this.getDetailFinItemIndex(nextJudgementNo)-1;
                    String SegCode = "";
                    for (int j = 0; j < ItemCondition[index].length; j++) {
                        String temp = tLIAboriginalDataSchema.getV(
                                ItemCondition[index][j]).
                                      trim();
                        if (temp.equals("null")) {
                            buildError("BuildItemCode",
                                       "类型为" +
                                       tLIAboriginalDataSchema.getClassID() +
                                       "的业务数据数据在生成科目代码为" + maincode +
                                       "的财务数据信息时出错，因为必要信息字段" +
                                       ItemCondition[index][j].trim() + "值为空");
                            return false;
                        }
                        if (j != ItemCondition[index].length - 1) {
                            temp += ",";
                        }
                        SegCode += temp;
                    }
                    LIDetailFinItemCodeSchema tLIDetailFinItemCodeSchema =
                            TRansItemCode(
                                    mLIFinItemDefSchema.getFinItemID(),
                                    nextJudgementNo,SegCode);
                    String partcode = tLIDetailFinItemCodeSchema.getLevelCode();
                    if ((partcode == null || partcode.equals(""))&&(tLIDetailFinItemCodeSchema.getJudgementNo()==null||tLIDetailFinItemCodeSchema.getJudgementNo().equals(""))) {
                    	buildError("BuildItemCode",
                                   "类型为" + tLIAboriginalDataSchema.getClassID() +
                                   "的业务数据数据在生成科目代码为" + maincode +
                                   "的财务数据信息时出错，因为层级" +
                                   mLIDetailFinItemDefSet.get(index+1).
                                   getFinItemlevel() +
                                   "的科目判断编号为" + nextJudgementNo + "的信息" + SegCode +
                                   "无对应转换值");
                        return false;
                    }
                    else
                    {
                        if(!(partcode == null || partcode.equals("")))
                        {
                            maincode += partcode;
                        }
                    }
                    if(tLIDetailFinItemCodeSchema.getNextJudgementNo()==null||tLIDetailFinItemCodeSchema.getNextJudgementNo().equals(""))
                    {
                        nextJudgementNo="end";
                    }
                    else
                    {
                        nextJudgementNo=tLIDetailFinItemCodeSchema.getNextJudgementNo();
                    }
                }
            }
            tLIDataTransResultSchema.setAccountCode(maincode);
            return true;
        }
        catch (Exception ex)
        {
            buildError("BuildItemCode",
                       "类型为" + tLIAboriginalDataSchema.getClassID() +
                       "的业务数据数据在生成科目代码为" + mLIFinItemDefSchema.getItemMainCode() + "的财务数据信息时出错，原因是：" + ex.getMessage());
            return false;
        }

    }


    private boolean SetAssociatedInfo(LIAboriginalDataSchema tLIAboriginalDataSchema,LIDataTransResultSchema tLIDataTransResultSchema)
    {
        try{
            for (int i = 1; i <= mLIInfoFinItemAssociatedSet.size(); i++)
            {
                LIInfoFinItemAssociatedSchema tLIInfoFinItemAssociatedSchema = mLIInfoFinItemAssociatedSet.get(i);
                String temp = tLIAboriginalDataSchema.getV(tLIInfoFinItemAssociatedSchema.getFountainID());
                if (temp.equals("null")||"".equals(temp)||temp==null)
                {
                    buildError("BuildItemCode","类型为" + tLIAboriginalDataSchema.getClassID() +"的业务数据数据在生成科目核算信息出错，因为必要信息字段" +tLIInfoFinItemAssociatedSchema.getFountainID() +"值为空");
                    return false;
                }
                else
                {
                    temp = mValueTrans[i-1].TransDeal(temp,tLIAboriginalDataSchema);
                    tLIDataTransResultSchema.setV(tLIInfoFinItemAssociatedSchema.getTargetID(), temp);
                }
            }
        }
        catch(Exception ex)
        {
           buildError("BuildItemCode",ex.getMessage());
           return false;
        }
        return true;
    }

    private boolean SetImportanceInfo(LIAboriginalDataSchema tLIAboriginalDataSchema,LIDataTransResultSchema tLIDataTransResultSchema)
    {
        //tLIDataTransResultSchema.setSerialNo("");
        tLIDataTransResultSchema.setBatchNo(tLIAboriginalDataSchema.getBatchNo());
        tLIDataTransResultSchema.setClassID(tLIAboriginalDataSchema.getClassID());
        //tLIDataTransResultSchema.setKeyUnionValue("");
        //tLIDataTransResultSchema.setFinItemType("");
        tLIDataTransResultSchema.setSumMoney(tLIAboriginalDataSchema.getSumActuMoney());
        tLIDataTransResultSchema.setAccountDate(tLIAboriginalDataSchema.getConfDate());
        // tLIDataTransResultSchema.setManageCom(tLIAboriginalDataSchema.getManageCom());
        tLIDataTransResultSchema.setListFlag(tLIAboriginalDataSchema.getListFlag());
        String flag = tLIAboriginalDataSchema.getListFlag();
        // flag 1 取个单号码 2 取团体保单号码
        if(flag==null)
        {
           buildError("BuildItemCode","类型为" + tLIAboriginalDataSchema.getClassID() +"的业务数据数据在生成科目重要信息出错，因为必要信息字段ListFlag值为空");
           return false;
        }
        else if(flag.equals("1"))
        {
            //tLIDataTransResultSchema.setContNo(tLIAboriginalDataSchema.getGrpContNo());
        	tLIDataTransResultSchema.setContNo(tLIAboriginalDataSchema.getContNo());
        }
        else if(flag.equals("2"))
        {
            //tLIDataTransResultSchema.setContNo(tLIAboriginalDataSchema.getContNo());
        	tLIDataTransResultSchema.setContNo(tLIAboriginalDataSchema.getGrpContNo());
        }
        else if(flag.equals("3"))
        {
            tLIDataTransResultSchema.setContNo(tLIAboriginalDataSchema.getPolNo());
        }
        else
        {
             tLIDataTransResultSchema.setContNo(tLIAboriginalDataSchema.getContNo());
        }
        if(tLIDataTransResultSchema.getContNo()==null||"".equals(tLIDataTransResultSchema.getContNo()))
        {
        	buildError("BuildItemCode","类型为" + tLIAboriginalDataSchema.getClassID() +
        			"的业务数据数据必要信息字段ContNo值为空,其中ListFlag="+tLIAboriginalDataSchema.getListFlag()+
        			"且KeyUnionValue=" +tLIDataTransResultSchema.getKeyUnionValue()+"");
            return false;
        }
        
        return true;
    }




    private LIDetailFinItemCodeSchema TRansItemCode(String FinItemID,String JudgementNo,String LevelCondition)
    {

        LIDetailFinItemCodeSchema tLIDetailFinItemCodeSchema = new LIDetailFinItemCodeSchema();
        int index =0;
        for(int i=1;i<=mLIDetailFinItemDefSet.size();i++)
        {
            if(mLIDetailFinItemDefSet.get(i).getJudgementNo().equals(JudgementNo))
            {
               index = i-1;
               break;
            }
            if(i==mLIDetailFinItemDefSet.size())
            {
               return tLIDetailFinItemCodeSchema;
            }
        }

        for(int i=1;i<=mLIDetailFinItemCodeSetUint[index].size();i++)
        {
            LIDetailFinItemCodeSchema tt = mLIDetailFinItemCodeSetUint[index].get(i);
            if(tt.getFinItemID().trim().equals(FinItemID.trim())&&tt.getJudgementNo().trim().equals(JudgementNo)&&tt.getLevelCondition().trim().equals(LevelCondition.trim()))
            {
               tLIDetailFinItemCodeSchema.setSchema(tt);
               break;
            }
        }
        return tLIDetailFinItemCodeSchema;
    }

    private int getDetailFinItemIndex(String JudgementNo) throws Exception
    {
        for(int i=1;i<=mLIDetailFinItemDefSet.size();i++)
        {
            if(mLIDetailFinItemDefSet.get(i).getJudgementNo().equals(JudgementNo))
            {
                return i;
            }
        }
        Exception e = new Exception("科目定义编号为" + this.mLIFinItemDefSchema.getFinItemID()+ "处理实例中不存在判断编号为"+ JudgementNo + "数据准备信息" );
        throw e;
    }


    public static void main(String[] args) {

        AccountItemType tAccountItemType = new  AccountItemType();

        LIAboriginalDataSchema tLIAboriginalDataSchema = new LIAboriginalDataSchema();
        LIDataTransResultSchema tLIDataTransResultSchema = new  LIDataTransResultSchema();
        tLIAboriginalDataSchema.setClassID("ClassID");
        tLIAboriginalDataSchema.setListFlag("1");
        tLIAboriginalDataSchema.setPayIntv("0");
        tLIAboriginalDataSchema.setPolYear("1");
        tLIAboriginalDataSchema.setRiskType("1");
        tLIAboriginalDataSchema.setRiskCode("PPPP");
        tLIAboriginalDataSchema.setFeeOperationType("XQ");
        tLIAboriginalDataSchema.setPayMode("5");
        tLIAboriginalDataSchema.setSaleChnl("03");
        tLIAboriginalDataSchema.setSaleChnlDetail("0526");
        tLIAboriginalDataSchema.setAgentCom("86065022");
        tLIAboriginalDataSchema.setSumActuMoney("");
        tLIAboriginalDataSchema.setBatchNo("001");
        tLIAboriginalDataSchema.setConfDate("2007-09-16");
        tLIAboriginalDataSchema.setManageCom("86010101");
        tLIAboriginalDataSchema.setContNo("80023016");
        tLIAboriginalDataSchema.setOperationType("51");
        tLIAboriginalDataSchema.setBankCode("AAA");
        tLIAboriginalDataSchema.setFeeFinaType("LPCK");
        tLIAboriginalDataSchema.setExecuteCom("86020101");
        tLIAboriginalDataSchema.setPayIntv("1");
        tLIAboriginalDataSchema.setBudget("0601003");
//        tLIAboriginalDataSchema.setBankCode("ABCD");
//        tLIAboriginalDataSchema.setBankAccNo("32324.01");
        tLIAboriginalDataSchema.setFirstTermFlag("3");
        tLIAboriginalDataSchema.setFirstYearFlag("S");
        tLIAboriginalDataSchema.setFirstTermFlag("X");
        tLIAboriginalDataSchema.setBankAccNo("AAA");
        tLIAboriginalDataSchema.setStandByString1("01");
        try
        {
            if(!tAccountItemType.InitItemDefInfo("0000002"))
            {
//                System.out.println("1111");
//                System.out.println("2222");
            };
           tLIDataTransResultSchema = tAccountItemType.DealInfo(tLIAboriginalDataSchema);
           System.out.println(tLIDataTransResultSchema.getAccountCode());
        }
        catch(Exception e)
        {
           System.out.println(e.getMessage());
        }
    }
}
