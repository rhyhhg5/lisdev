package com.sinosoft.lis.fininterface.uphold;

import com.sinosoft.lis.fininterface.uphold.FinInterfaceTransCode;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;

/**
 * 实现财务批处理的查询和已经跑完数据的财务接口报表查询及数据的展现.
 * @author lijs
 * @createTime 2006-12-28
 */

public class FinInterfaceTransBatchBL implements FinInterfaceTransCode {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/** 往后面传输数据的容器 */
	private VData mResult = new VData();	
	/******相关业务数据*******/
	private String m_sOperate = "";
	private String m_sStartDate = "";
	private String m_sEndDate = "";
	/**  处理类型 **/
	private String m_sFinInterfaceInfo = "";
	private LIDistillLogSet m_oLIDistillLogSet = new LIDistillLogSet();
	
	/**
	 * 返回错误信息
	 * @see com.sinosoft.lis.fininterface.uphold.FinInterfaceTransCode#getErrors()
	 */
	public CErrors getErrors() {
		return mErrors;		
	}

	/**
	 * 返回处理结果集
	 * @see com.sinosoft.lis.fininterface.uphold.FinInterfaceTransCode#getResult()
	 */
	public VData getResult() {		
		return mResult;		
	}

	public boolean submitData(VData cInputData, String cOperate) {
		
		m_sOperate = cOperate;
		if(!getInputData(cInputData)){
			buidlError("submitData","getInputData");
			return false;
		}
		System.out.println("--------end getInputData--------------");		
		if(m_sOperate.equals("query")){
			if(!queryFinInterface()){
				return false;
			}
		}
		System.out.println("--------end queryFinInterface--------------");	
		return true;
	}

	/**
	 * 
	 * @param cInputData
	 * @return
	 */
	private boolean getInputData(VData cInputData){
		
		try{
			if(m_sOperate.equals("query")){			
				m_sStartDate = (String) cInputData.get(1);
				m_sEndDate = (String) cInputData.get(2);
			    m_sFinInterfaceInfo = (String) cInputData.get(3);
			    System.out.println("m_sStartDate == " + m_sStartDate);
			    System.out.println("m_sFinInterfaceInfo == " + m_sFinInterfaceInfo);
			}	
		}catch(Exception e){
			System.out.println("异常信息1getInputData!");
			buidlError("getInputData","异常信息:" + e.getMessage());
			return false;
		}				
		return true;
	}
	
	/**
	 * 
	 * @return
	 */
	private boolean queryFinInterface(){
		
		String strSQL = "";
		//m_sFinInterfaceInfo:1 批处理 2 财务接口报表
		if(m_sFinInterfaceInfo.equals("1")){
			strSQL = " select a.batchno,a.operator, (select count(1)/2 from lidatatransresult where batchno = a.batchno) as num,a.makedate," +
					 " (select sum(summoney)/2 from lidatatransresult where batchno = a.batchno) as money" +
					 " from LIDistillLog a" +
					 " where a.makedate >= '" + m_sStartDate + "' and a.makedate <= '" + m_sEndDate + "'" +
					 " order by a.batchno";
		}else if(m_sFinInterfaceInfo.equals("2")){
			//明细
			strSQL = " select a.batchno,(select d.operator from LIDistillLog d where d.batchno = a.batchno), (select count(1) from liaboriginaldata where batchno = a.batchno) as num,a.makedate," +
			 " (select sum(sumactumoney) from liaboriginaldata where batchno = a.batchno) as money" +
			 " from litranlog a" +
			 " where a.makedate >= '" + m_sStartDate + "' and a.makedate <= '" + m_sEndDate + "' and flag = '3'" +
			 " order by a.batchno";
		}else{
			buidlError("queryFinInterface","查询类型未定义");
			return false;
		}
		
		ExeSQL oExeSQL = new ExeSQL();
		SSRS oSSRS = new SSRS();
		oSSRS = oExeSQL.execSQL(strSQL);
		try{
			if(oSSRS.MaxRow > 0){
				for(int i=1;i <= oSSRS.MaxRow;i++){
					LIDistillLogSchema oLIDistillLogSchema = new LIDistillLogSchema();
					oLIDistillLogSchema.setMakeDate(oSSRS.GetText(i, 4));
					oLIDistillLogSchema.setOperator(oSSRS.GetText(i, 2));
					String number = oSSRS.GetText(i, 3);
					String money = oSSRS.GetText(i, 5);
					System.out.println("number===" + number);
					if(number.equals("null") || number.equals("")){
						number = "0";
					}
					if(money.equals("null") || money.equals("")){
						money = "0";
				    }
				    int m = Math.round(Float.parseFloat(number));
				    System.out.println("m===" + m);
					oLIDistillLogSchema.setNumBuss(m);
					oLIDistillLogSchema.setFileName(money);//存放金额
					oLIDistillLogSchema.setBatchNo(oSSRS.GetText(i, 1));			
					//oLIDistillLogSchema.setFilePath(m_sFinInterfaceInfo);//科目类型定义
					/************************************************************/
//					System.out.println("这个是第" + i + "个Schema的相关数据的开始");
//					System.out.println(oLIDistillLogSchema.encode());
//					System.out.println("这个是第" + i + "个Schema的相关数据的结束");
					/************************************************************/
					m_oLIDistillLogSet.add(oLIDistillLogSchema);
				}
			}else{
				System.out.println("没有找到数据!");
				buidlError("queryFinInterface","没有查到相关数据");
				return false;
			}
		}catch(Exception e){
			System.out.println("异常信息queryFinInterface!");
			buidlError("queryFinInterface","异常信息:" + e.getMessage());
			return false;
		}
		mResult.clear();
		mResult.add(m_oLIDistillLogSet);
		return true;
	}
	
	/**
	 * 保存错误信息 
	 * @param sFuncName
	 * @param sErrorInfo
	 */
	private void buidlError(String sFuncName,String sErrorInfo){
		
		CError oCError = new CError();
		oCError.moduleName = "FinInterfaceTransBatchBL";
		oCError.functionName = sFuncName;
		oCError.errorMessage = sErrorInfo;
		this.mErrors.addOneError(oCError);
		
	}
}
