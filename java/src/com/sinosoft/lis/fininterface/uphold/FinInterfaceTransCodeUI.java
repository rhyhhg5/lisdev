package com.sinosoft.lis.fininterface.uphold;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:财务接口转换处理
 * </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * <p>Company: Sinosoft</p>
 * @author lijs
 * @version 1.0
 */
public class FinInterfaceTransCodeUI
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  public FinInterfaceTransCodeUI() {
	  
  }

  public static void main(String []agrs) throws Exception{
	  
//	  FinInterfaceTransCodeUI oFinInterfaceTransCodeUI = new FinInterfaceTransCodeUI();
//	  LICodeTransSet tLICodeTransSet = new LICodeTransSet();
//	  LICodeTransSchema tLICodeTransSchema = new LICodeTransSchema();
//	  tLICodeTransSchema.setCode("86881");
//	  tLICodeTransSchema.setCodeName("国民学生平安一年定期寿险");
//	  tLICodeTransSchema.setCodeAlias("86882");
//	  tLICodeTransSchema.setOtherSign("868883");
//	  tLICodeTransSet.add(tLICodeTransSchema);
//	  VData oVData = new VData();
//	  oVData.add("ManageCom");
//	  oVData.add("8699");
//	  oVData.add("国民学生平安一年定期寿险!");
//	  oVData.addElement(tLICodeTransSet);
//
//	  if(oFinInterfaceTransCodeUI.submitData(oVData, "del")){
//		  System.out.println("ok");
//	  }else{
//		  System.out.println("fail");
//	  }
//	  LICodeTransSet oLICodeTransSet = new LICodeTransSet();
//	  VData mVData = oFinInterfaceTransCodeUI.getResult();
//	  oLICodeTransSet.set((LICodeTransSet)mVData.getObjectByObjectName("LICodeTransSet",0));
//	  System.out.println(oLICodeTransSet.encode());
	  
	  //测试账户信息的维护
//	  FinInterfaceTransCodeUI oFinInterfaceTransCodeUI = new FinInterfaceTransCodeUI();
//	  LDComToBankSet tLDComToBankSet = new LDComToBankSet();
//	  LDComToBankSchema tLDComToBankSchema = new LDComToBankSchema();
//	  tLDComToBankSchema.setAccNo("0200004529024668687");
//	  tLDComToBankSchema.setComCode("861100"); 
//	  tLDComToBankSchema.setAccType("1");
//	  tLDComToBankSchema.setBankCode("021101");
//	  tLDComToBankSchema.setBankName("工商银行");
//	  tLDComToBankSchema.setGLkemu("B110000110201");
//	  tLDComToBankSchema.setReMark("S中国工商银行北京海淀镇支行");
//	  tLDComToBankSchema.setState("3");
//	  tLDComToBankSet.add(tLDComToBankSchema);
//	  VData oVData = new VData();
//	  oVData.add("BankAccNo");
//	  oVData.add("861100");
//	  oVData.addElement(tLDComToBankSet);
//	  if(oFinInterfaceTransCodeUI.submitData(oVData, "query")){
//		  System.out.println("ok");
//	  }else{
//		  System.out.println("fail");
//	  }
	  
	  /******测试财务接口批处理*********/
	  FinInterfaceTransCodeUI oFinInterfaceTransCodeUI = new FinInterfaceTransCodeUI();
	  LICodeTransSet tLICodeTransSet = new LICodeTransSet();
	  LICodeTransSchema tLICodeTransSchema = new LICodeTransSchema();
	  tLICodeTransSchema.setCode("07,06");
	  tLICodeTransSchema.setCodeName("学生");
	  tLICodeTransSchema.setCodeAlias("3201");
	  tLICodeTransSchema.setOtherSign("14");
	  tLICodeTransSet.add(tLICodeTransSchema);
	  VData oVData = new VData();
	  oVData.add("SaleChnlCode");
	  oVData.add("07");
	  //oVData.add("2006-12-01");
	  //oVData.add("2007-01-12");
	  //oVData.add("");
	  oVData.addElement(tLICodeTransSet);

	  if(oFinInterfaceTransCodeUI.submitData(oVData, "add")){
		  System.out.println("ok");
	  }else{
		  System.out.println("fail");
	  }
	  //LICodeTransSet oLICodeTransSet = new LICodeTransSet();
	 // VData mVData = oFinInterfaceTransCodeUI.getResult();
	  //oLICodeTransSet.set((LICodeTransSet)mVData.getObjectByObjectName("LICodeTransSet",0));
	  //System.out.println(oLICodeTransSet.encode());
	  
	  
  }
  /**
  传输数据的公共方法
 * @throws Exception 
  */
  public boolean submitData(VData cInputData,String cOperate) throws Exception
  {
    //将操作数据拷贝到本类中
    System.out.println("FinInterfaceTransCodeUI BEGIN------------------");
    this.mOperate = cOperate;
    String operateName = (String)cInputData.get(0);//
    String sClassName = "com.sinosoft.lis.fininterface.uphold.FinInterfaceTrans" + operateName + "BL";
    Class oClass = Class.forName(sClassName);
    FinInterfaceTransCode oFinInterfaceTransCode = (FinInterfaceTransCode)oClass.newInstance();

    if(oFinInterfaceTransCode.submitData(cInputData,mOperate) == false)
	{
	  		// @@错误处理
	      this.mErrors.copyAllErrors(oFinInterfaceTransCode.getErrors());
	      CError tError = new CError();
	      tError.moduleName = "FinDayCheckNCLQueryBL";
	      tError.functionName = "submitData";
	      tError.errorMessage = "数据查询失败!";
	      this.mErrors.addOneError(tError) ;
	      mInputData.clear();
    	  return false;
	}
	else
		mInputData = oFinInterfaceTransCode.getResult();
    return true;
  }

  public VData getResult()
  {
  	return mInputData;
  }

}