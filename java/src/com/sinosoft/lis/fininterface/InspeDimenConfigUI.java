package com.sinosoft.lis.fininterface;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.FICodeTransSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.fininterface.InspeDimenConfigBL;

public class InspeDimenConfigUI {
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();
	private VData mResult = new VData();
	/** 往后面传输数据的容器 */
	private VData mInputData =new VData();
	/** 数据操作字符串 */
	private String mOperate;
	// 业务处理相关变量
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput();

	private FICodeTransSchema mFICodeTransSchema = new FICodeTransSchema();

	public InspeDimenConfigUI() {
	}

	/**
	 * 传输数据的公共方法
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		System.out.println("save页面调用ui成功！");
		System.out.println(cInputData);
		System.out.println(cOperate);
		  //将操作数据拷贝到本类中
		  this.mOperate =cOperate;
		  
		  //得到外部传入的数据,将数据备份到本类中
		  if (!getInputData(cInputData))
		    return false;

		  //进行业务处理
		  if (!dealData())
		    return false;

		  //准备往后台的数据
		  if (!prepareOutputData(mInputData))
		    return false;
		
		  InspeDimenConfigBL tInspeDimenConfigBL = new InspeDimenConfigBL();
		 // tLGGroupPBTestBL.submitData(cInputData, mOperate);
		  if (!tInspeDimenConfigBL.submitData(cInputData, mOperate))
		  {
		     // @@错误处理
		     this.mErrors.copyAllErrors(tInspeDimenConfigBL.mErrors);
		     CError tError = new CError();
		     tError.moduleName = "InspeDimenConfigUI";
		     tError.functionName = "submitData";
		     tError.errorMessage = "数据提交失败!";
		     this.mErrors .addOneError(tError) ;
		     return false;
		  }
		  mInputData=null;
		  return true;
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */

    private void buildError(String szFunc, String szErrMsg)
    {
        CError cError = new CError();

        cError.moduleName = "DerferAppF1PUI";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }
    
    public VData getResult()
    {
        return this.mResult;
    }
    
    /**
     * 根据前面的输入数据，进行UI逻辑处理
     * 如果在处理过程中出错，则返回false,否则返回true
     */
    private boolean dealData()
    {
    	boolean tReturn =false;
        //此处增加一些校验代码
        tReturn=true;
        return tReturn ;
    }
    
    
    
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
	private boolean getInputData(VData cInputData) {
		// 全局变量
		mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mFICodeTransSchema = (FICodeTransSchema) cInputData.getObjectByObjectName(
				"FICodeTransSchema", 0);
		return true;
	}
    
    
    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData(VData vData)
    {
        try
        {
            vData.clear();
            vData.add(mGlobalInput);
            vData.add(mFICodeTransSchema);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            buildError("prepareOutputData", "发生异常");
            return false;
        }
        return true;
    }
    
}
