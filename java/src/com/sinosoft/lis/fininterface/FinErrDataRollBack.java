package com.sinosoft.lis.fininterface;

import java.sql.Connection;
import java.sql.SQLException;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
/**
 * 数据删除
 * @author linyun
 * @create date 2007-12-20
 */
public class FinErrDataRollBack {
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/*m代表全局变量，o代表是对象，a代表是数组，s代表是字符串*/
	String tBatchno = "";
    private MMap m_oaMap = new MMap();
    private Connection conn;
    private Connection m_oconn;
    boolean flag = true;
	
	public boolean dataRollBack(String tBatchno){
		
		try{
			conn = DBConnPool.getConnection();
			conn.setAutoCommit(false);
			// m_oconn = DBConnPool.getConnection();//new DBConn();
			// m_oconn.setAutoCommit(false);
			
		    ExeSQL tExeSQL = new ExeSQL(conn);
		   //  ExeSQL rExeSQL = new ExeSQL(m_oconn);
			
//			if(!checkData(tBatchno)){
//				System.out.println("在检查数据合法性的过程中出现错误！");
//				return false;
//			}
//			
		    String bsql="select distinct depcode as managecom,importtype,chargedate     from interfacetable  where readstate='3' and batchno='"+tBatchno+"' with ur";
		    ExeSQL tExecSQL=new ExeSQL();
		    SSRS tSSRS =new SSRS();
		    tSSRS=tExecSQL.execSQL(bsql);
		    for(int i=1;i<=tSSRS.getMaxRow();i++)
		    {
		    
			System.out.println("Start to delete data ...");
			
			if(!tExeSQL.execUpdateSQL("delete from LIAboriginalData where batchno = '" + tBatchno + "' and exists (select  1 from licodetrans where codetype='ManageCom' and codealias ='"+tSSRS.GetText(i, 1)+"' and code=managecom )" +
					"  and classid in (select classid from LIOperationDataClassDef where classtype='"+tSSRS.GetText(i, 2)+"')"   )){
				flag = false;
				break ;
			}
			if(!tExeSQL.execUpdateSQL("delete from LIDataTransResult where batchno = '" + tBatchno + "' and   ManageCom ='"+tSSRS.GetText(i, 1)+"'    " +
					"and classtype = '"+tSSRS.GetText(i, 2)+"'")){
				flag = false;
				break ;
			}					 
			if(!tExeSQL.execUpdateSQL("delete from LIDistillInfo where batchno = '" + tBatchno + "' and  exists (select  1 from licodetrans where codetype='ManageCom' and codealias ='"+tSSRS.GetText(i, 1)+"' and code=managecom )" +
					"  and classid in (select classid from LIOperationDataClassDef where classtype='"+tSSRS.GetText(i, 2)+"')" )){
				flag = false;
				break ;
			}
			
			if(!tExeSQL.execUpdateSQL("delete from interfacetable  where batchno = '" + tBatchno + "' and depcode='"+tSSRS.GetText(i, 1)+"' " +
					"  and  importtype='"+tSSRS.GetText(i, 2)+"' " +
					"  and  chargedate ='"+tSSRS.GetText(i, 3)+"'    ")){
				flag = false;
				break ;
			}
 
			
		    }
		    if(!tExeSQL.execUpdateSQL("delete from LIDistilllog  where batchno = '" + tBatchno + "'")){
				flag = false;
			}
			System.out.println("Delete data Ending ...");
			
//			LIAboriginalDataDB tLIAboriginalDataDB = new LIAboriginalDataDB(conn);
//			LIAboriginalDataSet tLIAboriginalDataSet = new LIAboriginalDataSet();
//			tLIAboriginalDataDB.setBatchNo(tBatchno);
//			tLIAboriginalDataSet = tLIAboriginalDataDB.query();
//			for(int i=1;i<=tLIAboriginalDataSet.size();i++){
//				tLIAboriginalDataDB = tLIAboriginalDataSet.get(i).getDB();
//				if(tLIAboriginalDataDB.delete()){
//					System.out.println("OK");
//				}else{
//					System.out.println("FAIL");
//					flag = false;
//				}
//			}
//			
//			LIDataTransResultDB tLIDataTransResultDB = new LIDataTransResultDB(conn);
//			LIDataTransResultSet tLIDataTransResultSet = new LIDataTransResultSet();
//			tLIDataTransResultDB.setBatchNo(tBatchno);
//			tLIDataTransResultSet = tLIDataTransResultDB.query();
//			for(int i=1;i<=tLIDataTransResultSet.size();i++){
//				tLIDataTransResultDB = tLIDataTransResultSet.get(i).getDB();
//				if(tLIDataTransResultDB.delete()){
//					System.out.println("OK");
//				}else{
//					System.out.println("FAIL");
//					flag = false;
//				}
//			}
//			
//			LIDistillErrDB tLIDistillErrDB = new LIDistillErrDB(conn);
//			LIDistillErrSet tLIDistillErrSet = new LIDistillErrSet();
//			tLIDistillErrDB.setBatchNo(tBatchno);
//			tLIDistillErrSet = tLIDistillErrDB.query();
//			for(int i=1;i<=tLIDistillErrSet.size();i++){
//				tLIDistillErrDB = tLIDistillErrSet.get(i).getDB();
//				if(tLIDistillErrDB.delete()){
//					System.out.println("OK");
//				}else{
//					System.out.println("FAIL");
//					flag = false;
//				}
//			}
//			
//			LIDistillLogDB tLIDistillLogDB = new LIDistillLogDB(conn);
//			LIDistillLogSet tLIDistillLogSet = new LIDistillLogSet();
//			tLIDistillLogDB.setBatchNo(tBatchno);
//			tLIDistillLogSet = tLIDistillLogDB.query();
//			for(int i=1;i<=tLIDistillLogSet.size();i++){
//				tLIDistillLogDB = tLIDistillLogSet.get(i).getDB();
//				if(tLIDistillLogDB.delete()){
//					System.out.println("OK");
//				}else{
//					System.out.println("FAIL");
//					flag = false;
//				}
//			}
//			
//			LIDistillInfoDB tLIDistillInfoDB = new LIDistillInfoDB(conn);
//			LIDistillInfoSet tLIDistillInfoSet = new LIDistillInfoSet();
//			tLIDistillInfoDB.setBatchNo(tBatchno);
//			tLIDistillInfoSet = tLIDistillInfoDB.query();
//			for(int i=1;i<=tLIDistillInfoSet.size();i++){
//				tLIDistillInfoDB = tLIDistillInfoSet.get(i).getDB();
//				if(tLIDistillInfoDB.delete()){
//					System.out.println("OK");
//				}else{
//					System.out.println("FAIL");
//					flag = false;
//				}
//			}
//			
//			LITranLogDB tLITranLogDB = new LITranLogDB(conn);
//			LITranLogSet tLITranLogSet = new LITranLogSet();
//			tLITranLogDB.setBatchNo(tBatchno);
//			tLITranLogSet = tLITranLogDB.query();
//			for(int i=1;i<=tLITranLogSet.size();i++){
//				tLITranLogDB = tLITranLogSet.get(i).getDB();
//				if(tLITranLogDB.delete()){
//					System.out.println("OK");
//				}else{
//					System.out.println("FAIL");
//					flag = false;
//				}
//			}
//			
//			OF_INTERFACE_DETAILDB tOF_INTERFACE_DETAILDB = new OF_INTERFACE_DETAILDB(m_oconn);
//			OF_INTERFACE_DETAILSet tOF_INTERFACE_DETAILSet = new OF_INTERFACE_DETAILSet();
//			tOF_INTERFACE_DETAILDB.setPOSTING_ID(tBatchno);
//			tOF_INTERFACE_DETAILSet = tOF_INTERFACE_DETAILDB.query();
//			for(int i=1;i<=tOF_INTERFACE_DETAILSet.size();i++){
//				tOF_INTERFACE_DETAILDB = tOF_INTERFACE_DETAILSet.get(i).getDB();
//				if(tOF_INTERFACE_DETAILDB.delete()){
//					System.out.println("OK");
//				}else{
//					System.out.println("FAIL");
//					flag = false;
//				}
//			}
//			
//			OF_INTERFACEDB tOF_INTERFACEDB = new OF_INTERFACEDB(m_oconn);
//			OF_INTERFACESet tOF_INTERFACESet = new OF_INTERFACESet();
//			tOF_INTERFACEDB.setPOSTING_ID(tBatchno);
//			tOF_INTERFACESet = tOF_INTERFACEDB.query();
//			for(int i=1;i<=tOF_INTERFACESet.size();i++){
//				tOF_INTERFACEDB = tOF_INTERFACESet.get(i).getDB();
//				if(tOF_INTERFACEDB.delete()){
//					System.out.println("OK");
//				}else{
//					System.out.println("FAIL");
//					flag = false;
//				}
//			}
			
		    if(flag){
	    		conn.commit();
	    	//	m_oconn.commit();
	    		System.out.println("commit");
		   	}else{
		   		conn.rollback();
		   	//	m_oconn.rollback();
				buildError("dataRollBack","回滚已提取数据失败！");
		   		System.out.println("Nocommit");
			    return false;
		   	}
		    conn.close();
		   // m_oconn.close();
			return true;
		}catch(Exception ex){
			ex.printStackTrace();
			buildError("dataRollBack","在回滚已提取数据过程中发生异常，原因是：" + ex.getMessage());
			return false;
		}finally{
	    	try{
	    		conn.close();
	    	//	m_oconn.close();
	    	}catch(Exception e){
	    		e.printStackTrace();
	    		System.out.println("关闭连接池失败!");
	    	}
	    }
	}
	
	private boolean checkData(String tBatchno){
		
		try{
			ExeSQL cExeSQL = new ExeSQL(m_oconn);
			String tSQL = "select count(1) from interfacetable where batchno = '" + tBatchno + "' and readstate='3' ";
			SSRS cSSRS = cExeSQL.execSQL(tSQL);
			System.out.println("tSQL:" + tSQL);
			if(cSSRS.MaxRow<=0){
				System.out.println("该批次数据已全部成功导入财务系统中，无法删除！");
				buildError("checkData","该批次数据已全部成功导入财务系统中，无法删除！");
				return false;
			}
			return true;
		}catch(Exception ex){
			buildError("checkData","检验数据合法性失败，原因是：" + ex.getMessage());
		    return false;
		}
	}
	
	private void buildError(String szFunc, String szErrMsg){
		CError cError = new CError();
		cError.moduleName = "FinDataRollBack";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
		System.out.print(szErrMsg);
	}

	public static void main(String[] args) throws SQLException{
		FinDataRollBack tFinDataRollBack = new FinDataRollBack();
		String tBatchno = "00000000000000000575";
		if(tFinDataRollBack.dataRollBack(tBatchno))
			System.out.println(tBatchno + "批次的数据已经成功回滚，可以再次提取数据！");
		else
			System.out.println(tBatchno + "批次的数据回滚失败，请检查程序！");
	}
	
}