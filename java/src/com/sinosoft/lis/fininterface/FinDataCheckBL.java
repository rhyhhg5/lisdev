package com.sinosoft.lis.fininterface;

import java.util.Vector;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.PubCalculator;
import com.sinosoft.lis.pubfun.PubFun;

/**
 * <p>Title: PICCH业务系统</p>
 * <p>Description: 财务提数数据质量检查BL</p>
 * <p>Copyright: Copyright (c) 2009</p>
 * <p>Company: Sinosoft</p>
 * @author yanjing
 * @version 1.0
 * @since 2009-07-10
 */

public class FinDataCheckBL
{
	private TransferData mTransferData = new TransferData();
	/** 错误处理类 */
	public CErrors mErrors = new CErrors();
	/**
	 * 数据校验
	 * @return
	 */
	private boolean check()
	{   
		SSRS mSSRS = new SSRS();
		SSRS rSSRS = new SSRS();
		ExeSQL tExeSQL = new ExeSQL();
		String tCalCode = "";
		String tCalSql = "";
		String str = "select * from LIFinDataCheck  where Codetype = 'FinDataCheck' order by Code";
		System.out.println("校验SQL的查询语句->str:"+str);
        mSSRS = tExeSQL.execSQL(str);
        int trowsize = mSSRS.getMaxRow();
        System.out.println("校验SQL的个数->mSSRS:"+trowsize);
		//若没有校验SQL
		if (trowsize < 1)
		{
			buildError("check", "LFCAuditCheck表中没有需要执行的检查语句！");
			System.out.println("LFCAuditCheck表中没有需要执行的检查语句！");
			return false;
		}
		System.out.println("开始循环校验SQL->FinDataCheckBL：51行");
		for (int i = 1; i <= trowsize; i++)
		{   
			rSSRS = new SSRS();
			tExeSQL = new ExeSQL();
			try
			{   
				tCalCode = mSSRS.GetText(i, 2);
				tCalSql = mSSRS.GetText(i, 4);
				tCalSql = StrTool.replaceEx(tCalSql, "’", "'"); //替换字符
		        if(tCalSql.toUpperCase().indexOf("WITH ")>0)
		        {   System.out.println("校验sql语句中不能出现with ur。");
		        	buildError("check", "sql语句中不能出现with ur。");
		        	return false;
		        }
                
				//添加计算要素，获取校验sql。
				String tCheckSql = getCheckSQL(tCalSql, mTransferData);
				System.out.println("财务校验类型："+mSSRS.GetText(i, 3));
                System.out.println("校验sql："+tCheckSql);
                rSSRS=tExeSQL.execSQL(tCheckSql);
                //如果校验SQL查询结果集不为空
                System.out.println("校验结果："+rSSRS.getMaxRow()+";内容："+rSSRS.GetText(1, 1));
                if(rSSRS.getMaxRow()>=1){
                    //计算数据记录条数的语句并更新 
                	String DataCount = rSSRS.GetText(1, 1);
                	if (tExeSQL.mErrors.getErrorCount() != 0)
                        DataCount = "无表";
                    else if (DataCount.equals("") || DataCount == null)
                        DataCount = "0";
                    else if (DataCount.length() > 30)
                        DataCount = DataCount.substring(30);
                    System.out.println("效验的错误数据量：" + DataCount);
                    
                    String strsql = "update LIFinDataCheck set othersign='" + DataCount + "' WHERE Code='" + tCalCode+ "' and codetype='FinDataCheck'";
                    if (!tExeSQL.execUpdateSQL(strsql))
                    {
                        System.out.println(tCalCode + "表更新数据失败");
                    }
                }
                
			}
			catch (Exception ex)
			{ 
				System.err.print(ex);
				return false;
			}
		}
		return true;
	}
//
//    private void countdata(String TcCode, String CalSql)
//    {
//        String DataCount = "";
//        ExeSQL tSQL1 = new ExeSQL();
//        DataCount = tSQL1.getOneValue(CalSql);
//        if (tSQL1.mErrors.getErrorCount() != 0)
//            DataCount = "无表";
//        else if (DataCount.equals("") || DataCount == null)
//            DataCount = "0";
//        else if (DataCount.length() > 30)
//            DataCount = DataCount.substring(30);
//        System.out.println("效验的数量" + DataCount);

//        String changeState = "update LIFinDataCheck set othersign='0' WHERE codetype='FinDataCheck'";
//        if(!tSQL1.execUpdateSQL(changeState)){
//        	System.out.println("表更新数据失败");
//        }
        
//        String strsql = "update LIFinDataCheck set othersign='" + DataCount + "' WHERE Code='" + TcCode+ "' and codetype='FinDataCheck'";
//        if (!tSQL1.execUpdateSQL(strsql))
//        {
//            System.out.println(TcCode + "表更新数据失败");
//        }
//
//    }

	/**
	 * 获取提数insert语句。
	 * @param tLFCAuditCheckSchema
	 * @param tTransferData
	 * @return
	 */
	private String getCheckSQL(String tCalSql,TransferData tTransferData) {
		System.out.println("FinDataCheckBL->getCheckSQL：139行");
        PubCalculator tPubCalculator = new PubCalculator();
		// 准备计算要素
		Vector tVector = (Vector) tTransferData.getValueNames();
        
		//普通要素
		for (int i = 0; i < tVector.size(); i++) {
			String tName = (String) tVector.get(i);
			String tValue = (String) tTransferData.getValueByName((Object) tName).toString();
			tPubCalculator.addBasicFactor(tName, tValue);
		}
		//校验SQL是否为空，并且替换SQL中的变量
		System.out.println("准备数据校验sql，替换变量等------");
		tPubCalculator.setCalSql(tCalSql);
		String strSQL = tPubCalculator.calculateEx();

		return strSQL;
	}
	
	private boolean getInputData(VData tInputData)
	{   
		mTransferData = (TransferData) tInputData.getObjectByObjectName("TransferData", 0);
		if(mTransferData == null)
		{
			buildError("getInputData", "传入参数不正确。");
			return false;
		}
		return true;
	}
	
	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		// 得到外部传入的数据，将数据备份到本类中
        
		if (!getInputData(cInputData)) {
			return false;
		}

//		if(!ReCheck())
//		{
//			return false;
//		}
		
		// 处理提数逻辑
		if (!check()) {
			return false;
		}

		return true;
	}
	
	public boolean ReCheck()
	{
		ExeSQL tExeSQL = new ExeSQL();
		String str = "update LFCAuditCheck set ErrCount='0' , ValiFlag = '1' ," + " MakeDate='"
				+ PubFun.getCurrentDate() + "',MakeTime='" + PubFun.getCurrentTime()+"'";
		if (!tExeSQL.execUpdateSQL(str))
		{
			buildError("check", "更新数据失败！");
			return false;
		}
		return true;
	}
	
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "AuditDataCheckBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}
	
	public static void main(String args[])
	{
		System.out.println("start");
		FinDataCheckBL tFinDataCheckBL = new FinDataCheckBL();
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		
		tTransferData.setNameAndValue("ManageCom", "8611");
		tTransferData.setNameAndValue("StartDate", "2008-01-01");
		tTransferData.setNameAndValue("EndDate", "2008-06-30");
		
		tVData.add(tTransferData);
      //  tFinDataCheckBL.ReCheck();
        tFinDataCheckBL.submitData(tVData,"");
		System.out.println("end");
	}


}
