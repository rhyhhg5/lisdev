package com.sinosoft.lis.fininterface;

import java.sql.Connection;
import java.sql.SQLException;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.fininterface.TransItem.*;
/**
 * 导出数据
 * @author lijs
 * @create date 2006-11-05
 */
public class MoveDataFromGRPToPRO {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	/*m代表全局变量，o代表是对象，a代表是数组，s代表是字符串*/
	String m_sDate = PubFun.getCurrentDate();
	String m_sTime = PubFun.getCurrentTime();
    private VData m_oaResult = new VData();
    private MMap m_oaMap = new MMap();
    private Connection conn;
    private Connection m_oconn;
    private boolean flag = false;
    private String classtypename = "";
    private ExeSQL tExeSQL = new ExeSQL();

    private LICodeTransDB tLICodeTransDB = new LICodeTransDB();
    private LICodeTransSet tLICodeTransSet = new LICodeTransSet();

    public static void main(String[] args) throws SQLException{
	   /* Select * From LIDataTransResult Order By  serialno
		* Select *  From litranlog Where  flag = '3'
		* Select * From  OF_INTERFACE_DETAIL Where
		* Delete From OF_INTERFACE_DETAIL Where currency_code = 'RMB'
		* 33 =
			  (DESCRIPTION =
			    (ADDRESS_LIST =
			      (ADDRESS = (PROTOCOL = TCP)(HOST = 10.0.8.33)(PORT = 1521))
			    )
			    (CONNECT_DATA =
			      (SERVICE_NAME = orahx)
			    )
			  )
				用户名和口令是gmlifetemp
				要操作的库为interface.OF_INTERFACE_DETAIL
	    */

    	MoveDataFromGRPToPRO oMoveDataFromGRPToPRO = new MoveDataFromGRPToPRO();
		//String sBatchNo = "00000000000000000047";
		//String sBatchNo = "00000000000000000050";
		String sBatchNo = "00000000000000002369";
		if(!oMoveDataFromGRPToPRO.moveData(sBatchNo)){
			System.out.println("false@@@@@执行失败.");
		}else{
			System.out.println("true@@@@@执行成功.");
		}
	}

    public boolean moveData(String sBatchNo){
        //
        if (!updateCostCenter())
        {
            CError tError = new CError();
               tError.moduleName = "MoveDataFromGRP";
               tError.functionName = "updateCostCenter";
               tError.errorMessage = "成本中心更新失败";
                this.mErrors.addOneError(tError);
                return false;
        }
    	//第一步判断是否已经提取过数据
    	if(!searchByBatchNo(sBatchNo)){
    		//this.mErrors.copyAllErrors(mErrors);
    		CError tError = new CError();
		    tError.moduleName = "MoveDataFromGRP";
		    tError.functionName = "moveData";
		    tError.errorMessage = "该流水号已经提取过数据!";
		    this.mErrors.addOneError(tError);
		    return false;
    	}
    	if(!moveDataFromGRPToPRO(sBatchNo)){
    		this.mErrors.copyAllErrors(mErrors);
    		CError tError = new CError();
		    tError.moduleName = "MoveDataFromGRP";
		    tError.functionName = "moveData";
		    tError.errorMessage = "提取失败!";
		    this.mErrors.addOneError(tError);
		    return false;
    	}
    	return true;
    }
    /**
     *
     */
    public boolean searchByBatchNo(String sBatchNo)
    {
    	//从表litranlog中查询 条件是 batchNo 和 flag = '3'
    	/*LITranLogDB oLITranLogDB = new LITranLogDB();
    	oLITranLogDB.setBatchNo(sBatchNo);
    	oLITranLogDB.setFlag("3");
    	if()*/
    	String strSQL = "select count(*) from litranlog where flag = '3' and batchno = '" + sBatchNo + "'";
    	SSRS oSSRS = new SSRS();
    	ExeSQL rExeSQL = new ExeSQL();
    	oSSRS = rExeSQL.execSQL(strSQL);
        String newperd = oSSRS.GetText(1, 1);
        //没有查询到结果
        if(newperd == null || newperd.equals("null") || newperd.equals("") || newperd.equals("0")) {
        	System.out.println("没有提取过数据");
        	return true;
        }else{
        	System.out.println("提取过数据");
    		this.mErrors.copyAllErrors(mErrors);
    		CError tError = new CError();
		    tError.moduleName = "MoveDataFromGRP";
		    tError.functionName = "moveData";
		    tError.errorMessage = "提取过数据!";
		    this.mErrors.addOneError(tError);
        	return false;
    	}
    }

    /**
     *
     * @param strCode
     * @return
     */
    private static String changeString(String strCode){
    	String sCMD = "";
    	if(strCode == null || strCode.equals("")){
    		sCMD = "0";
    	}else{
    		sCMD = strCode;
    	}
    	return sCMD;
    }

    /**
     * 通过循环一个LICodeTransSet中的数据，在不查询数据库的情况下得到转化后的大凭证类型
     * */
    private String getClassTypeTrans(String tClassType){
    	String bClassType = "CLASSTYPE NOT FOUND";
    	for(int i=1;i<=tLICodeTransSet.size();i++){
    		if(tLICodeTransSet.get(i).getCode().equals(tClassType)){
    			bClassType = tLICodeTransSet.get(i).getCodeAlias();
    		}
    	}
        return bClassType;
    }

	/**
	 * 通过批次号查询财务接口表LIDataTransResult,把相应数据放入业务系统中总帐接口表OF_INTERFACE_DETAIL
	 * @param sBatchNoNo 批次号
	 * @throws SQLException
	 */
	public  boolean moveDataFromGRPToPRO(String sBatchNo){

	try{
		InterfaceTableSet tInterfaceTableSet=new InterfaceTableSet();
    	String strSQL = "select distinct   classtype from lidatatransresult " +
    					"where batchno = '" + sBatchNo + "' and not exists (select 1 from licodetrans a where a.codetype = 'ClassType' and  (a.code) =   (classtype)) with ur";
    	System.out.println("校验凭证类型  "+strSQL);
    	SSRS oSSRS = new SSRS();
    	ExeSQL rExeSQL = new ExeSQL();
    	oSSRS = rExeSQL.execSQL(strSQL);
    	if(oSSRS.MaxRow>0){
    		String tEOut = "";
    		for(int i=1;i<=oSSRS.MaxRow;i++){
    			tEOut = tEOut + oSSRS.GetText(i, 1);
    			if(i<oSSRS.MaxRow){
    				tEOut = tEOut + "，";
    			}
    		}
        	System.out.println("凭证类型为" + tEOut + "的数据在LICodeTrans表中无对应映射值！");
    		this.mErrors.copyAllErrors(mErrors);
    		CError tError = new CError();
		    tError.moduleName = "MoveDataFromGRP";
		    tError.functionName = "moveDataFromGRPToPRO";
		    tError.errorMessage = "凭证类型为" + tEOut + "的数据在LICodeTrans表中无对应映射值！";
		    this.mErrors.addOneError(tError);
        	return false;
    	}

    	/*校验供应商*/
    	String accSQL = "select  distinct  serialno  from lidatatransresult " +
    	"where batchno = '" + sBatchNo + "' and classtype ='S-01' and finitemtype = 'C' and (standbystring1 is null and accountcode is null)  with ur";
    	System.out.println("校验供应商  "+accSQL);
    	SSRS accSSRS = new SSRS();
    	ExeSQL accExeSQL = new ExeSQL();
    	accSSRS = accExeSQL.execSQL(accSQL);
    	if(accSSRS.MaxRow>0){
    		String accEOut = "";
    		for(int i=1;i<=accSSRS.MaxRow;i++){
    			accEOut = accEOut + accSSRS.GetText(i, 1);
    			if(i<oSSRS.MaxRow){
    				accEOut = accEOut + "，";
    			}
    		}
        	System.out.println("批次号为" + accEOut + "的数据的供应商字段为空！");
    		this.mErrors.copyAllErrors(mErrors);
    		CError tError = new CError();
		    tError.moduleName = "MoveDataFromGRP";
		    tError.functionName = "moveDataFromGRPToPRO";
		    tError.errorMessage = "批次号为" + accEOut + "的数据的供应商字段为空！";
		    this.mErrors.addOneError(tError);
        	return false;
    	}
    	
    	
    	/*校验供应商2*/
    	String acc2SQL = "select  distinct  serialno  from lidatatransresult " +
    	"where batchno = '" + sBatchNo + "' and classtype ='S-02' and finitemtype = 'D' and (standbystring1 is null and accountcode is null)  with ur";
    	System.out.println("校验供应商  "+acc2SQL);
    	SSRS acc2SSRS = new SSRS();
    	ExeSQL acc2ExeSQL = new ExeSQL();
    	acc2SSRS = acc2ExeSQL.execSQL(acc2SQL);
    	if(acc2SSRS.MaxRow>0){
    		String acc2EOut = "";
    		for(int i=1;i<=acc2SSRS.MaxRow;i++){
    			acc2EOut = acc2EOut + acc2SSRS.GetText(i, 1);
    			if(i<oSSRS.MaxRow){
    				acc2EOut = acc2EOut + "，";
    			}
    		}
        	System.out.println("批次号为" + acc2EOut + "的数据的供应商字段为空！");
    		this.mErrors.copyAllErrors(mErrors);
    		CError tError = new CError();
		    tError.moduleName = "MoveDataFromGRP";
		    tError.functionName = "moveDataFromGRPToPRO";
		    tError.errorMessage = "批次号为" + acc2EOut + "的数据的供应商字段为空！";
		    this.mErrors.addOneError(tError);
        	return false;
    	}
    	
    	
    	String pSQL = "select  distinct  managecom  from liaboriginaldata " +
		"where batchno = '" + sBatchNo + "'    and not exists (select 1 from licodetrans a where a.codetype = 'ManageCom' and a.code = liaboriginaldata.ManageCom ) with ur";
    	System.out.println("校验管理机构  "+pSQL);
    	SSRS pSSRS = new SSRS();
    	ExeSQL pExeSQL = new ExeSQL();
    	pSSRS = pExeSQL.execSQL(pSQL);
    	if(pSSRS.MaxRow>0){
    		String pEOut = "";
    		for(int i=1;i<=pSSRS.MaxRow;i++){
    			pEOut = pEOut + pSSRS.GetText(i, 1);
    			if(i<oSSRS.MaxRow){
    				pEOut = pEOut + "，";
    			}
    		}
        	System.out.println("管理机构为" + pEOut + "的数据在LICodeTrans表中无对应映射值！");
    		this.mErrors.copyAllErrors(mErrors);
    		CError tError = new CError();
		    tError.moduleName = "MoveDataFromGRP";
		    tError.functionName = "moveDataFromGRPToPRO";
		    tError.errorMessage = "管理机构为" + pEOut + "的数据在LICodeTrans表中无对应映射值！";
		    this.mErrors.addOneError(tError);
        	return false;
    	}
    	String nSQL = "select  distinct  ExecuteCom  from lidatatransresult " +
    	"where batchno = '" + sBatchNo + "' and ExecuteCom is not null and length(ExecuteCom)=8  and not exists (select 1 from licodetrans a where a.codetype = 'ManageCom' and a.code = lidatatransresult.ExecuteCom ) with ur";
    	System.out.println("校验执行机构  "+pSQL);
    	SSRS nSSRS = new SSRS();
    	ExeSQL nExeSQL = new ExeSQL();
    	pSSRS = nExeSQL.execSQL(nSQL);
    	if(nSSRS.MaxRow>0){
    		String nEOut = "";
    		for(int i=1;i<=nSSRS.MaxRow;i++){
    			nEOut = nEOut + nSSRS.GetText(i, 1);
    			if(i<oSSRS.MaxRow){
    				nEOut = nEOut + "，";
    			}
    		}
        	System.out.println("执行机构为" + nEOut + "的数据在LICodeTrans表中无对应映射值！");
    		this.mErrors.copyAllErrors(mErrors);
    		CError tError = new CError();
		    tError.moduleName = "MoveDataFromGRP";
		    tError.functionName = "moveDataFromGRPToPRO";
		    tError.errorMessage = "执行机构为" + nEOut + "的数据在LICodeTrans表中无对应映射值！";
		    this.mErrors.addOneError(tError);
        	return false;
    	}
    	String qSQL = "select  distinct  agentgroup   from lidatatransresult " +
		"where batchno = '" + sBatchNo + "' and agentgroup is not null and  not exists (select 1 from labranchgroup  a where a.agentgroup= lidatatransresult.agentgroup  and a.costcenter is not null ) with ur";
    	System.out.println("校验成本中心  "+qSQL);
    	SSRS qSSRS = new SSRS();
    	ExeSQL qExeSQL = new ExeSQL();
    	qSSRS = qExeSQL.execSQL(qSQL);
    	if(qSSRS.MaxRow>0){
    		String qEOut = "";
    		for(int i=1;i<=qSSRS.MaxRow;i++){
    			qEOut = qEOut + qSSRS.GetText(i, 1);
    			if(i<qSSRS.MaxRow){
    				qEOut = qEOut + "，";
    			}
    		}
        	System.out.println("成本中心为" + qEOut + "的数据在LAbranchgroup表中无对应映射值！");
    		this.mErrors.copyAllErrors(mErrors);
    		CError tError = new CError();
		    tError.moduleName = "MoveDataFromGRP";
		    tError.functionName = "moveDataFromGRPToPRO";
		    tError.errorMessage = "成本中心为" + qEOut + "的数据在LAbranchgroup表中无对应映射值！";
		    this.mErrors.addOneError(tError);
        	return false;
    	} 
    	else{
    		ExeSQL  tManageExeSQL=new ExeSQL();
    		
    	    conn = DBConnPool.getConnection();
	    	conn.setAutoCommit(false);
    		ExeSQL  updateExeSQL=new ExeSQL(conn);
    		
    		
    		try
    		{
	    		updateExeSQL.execUpdateSQL("update lidatatransresult a set a.classtype=a.classtype||'-Y' where a.managecom='86650105' and a.batchno='"+sBatchNo+"'");
	    		conn.commit();
    		}catch(Exception ex)
    		{
    			conn.rollback();
    			conn.close();
    			ex.printStackTrace();
    			System.out.println("wrong update!");
   	         	CError tError = new CError();
   	         	tError.moduleName = "MoveDataFromGRP";
   	         	tError.functionName = "moveDataFromGRPToPRO";
   	         	tError.errorMessage = "更新共保数据失败!" + ex.getMessage();
   	         	this.mErrors.addOneError(tError);
   	         	return false;
    		}

    		try
    		{
    			//"select code,codealias from licodetrans where codetype='ManageCom'"
    			SSRS tManageSSRS=tManageExeSQL.execSQL("select a.code,a.codealias from licodetrans a where 1=1 and a.codetype='ManageCom'");
    			if(tManageSSRS!=null && tManageSSRS.MaxRow>0)
    			{
    				for(int manageIndex =1;manageIndex<=tManageSSRS.MaxRow;manageIndex++)
    				{
    					
    	    	    	updateExeSQL.execUpdateSQL("update lidatatransresult a set a.costcenter =(select b.costcenter from labranchgroup b where b.agentgroup =a.agentgroup)  where batchno='"+sBatchNo+"' and  a.agentgroup is not null and managecom='"+tManageSSRS.GetText(manageIndex, 1)+"' ");
    	    	    	updateExeSQL.execUpdateSQL("update lidatatransresult a set accountcode='2611020101' where batchno='"+sBatchNo+"' and accountcode in('2611020301','2611020401','2611020108','2611020702','2611020701') and riskcode in ('170106','1605') and managecom='"+tManageSSRS.GetText(manageIndex, 1)+"' ");
    	    	    	updateExeSQL.execUpdateSQL("update lidatatransresult a set accountcode='6051010000' where batchno='"+sBatchNo+"' and accountcode in ('6051010500','6051010100') and riskcode in ('170106','1605') and managecom='"+tManageSSRS.GetText(manageIndex, 1)+"' ");    	    	    	
    	    	    	
    	    	    	updateExeSQL.execUpdateSQL("update lidatatransresult a set a.ExecuteCom =(select b.codealias from licodetrans b where b.code =a.ExecuteCom and b.codetype = 'ManageCom') where batchno='"+sBatchNo+"' and managecom='"+tManageSSRS.GetText(manageIndex, 1)+"' ");
    	    	    	updateExeSQL.execUpdateSQL("update lidatatransresult a set a.managecom='"+tManageSSRS.GetText(manageIndex, 2)+"' where batchno='"+sBatchNo+"' and managecom='"+tManageSSRS.GetText(manageIndex, 1)+"' ");
    	    	    	conn.commit(); 
    				}
    			}
    			SSRS tManageSSRS11=tManageExeSQL.execSQL("select count(1) from lidatatransresult where batchno='"+sBatchNo+"' and Length(managecom)>4");
    			if(tManageSSRS11!=null && !("0".equals(tManageSSRS11.GetText(1, 1)))){
    				System.out.println("仍然存在8位managecom数据！！");
    			}
    			
    		}catch(Exception ex)
    		{
    			conn.rollback();
    			conn.close();
    			ex.printStackTrace();
    			System.out.println("wrong update!");
   	         	CError tError = new CError();
   	         	tError.moduleName = "MoveDataFromGRP";
   	         	tError.functionName = "moveDataFromGRPToPRO";
   	         	tError.errorMessage = "更新数据失败!" + ex.getMessage();
   	         	this.mErrors.addOneError(tError);
   	         	return false;
    		}
    		finally
    		{
    			try{
    				conn.close();
    	    	}catch(Exception e){
    	    		System.out.println("关闭连接池失败!");
    	    	}
    		}
    		
    		InterfaceTableDB tInterfaceTableDB = new InterfaceTableDB();
			ExeSQL  tExeSQL=new ExeSQL();
			SSRS tSSRS = new SSRS();
		    String tSql="select  '' as  SerialNo,a.BatchNo as BatchNo , a.ClassType as ImportType, a.FinItemType as DCFlag, case when exists (select 1 from licodetrans where codetype='ClassType' and code=a.classtype) then " +
		    		" (select codealias from licodetrans where codetype='ClassType' and code=a.classtype) when exists (select 1 from licodetrans where codetype='ClassType' and code=substr(a.classtype,1,length(a.classtype)-2)) " +
		    		"  then (select 'Y'||substr(codealias,2,length(codealias)-1) from licodetrans where codetype='ClassType' and code=substr(a.classtype,1,length(a.classtype)-2)) end   as VoucherType , " +
		    		" 'CNY' as Currency, a.ManageCom  as DepCode ,a.ExecuteCom as FinMangerCount, a.AccountCode as AccountCode , a.BClient as BClient ," +
		    		"  a.CostCenter   as CostCenter, " +
		    		" a.riskcode as RiskCode, a.MarketType  as  MarketType, a.SaleChnl  as  Chinal, a.PCont as PCont , a.Budget as CashFlowNo, " +
		    		" a.AccountDate as ChargeDate, '' as ChargeTime,sum(a.SumMoney) as SumMoney, ''as VoucherID , " +
		    		" '' as ReadState, '' as VoucherYear, '' as BackVDate, '' as BackVTime, '' as ReadDate , '' as ReadTime , '' as  MakeDate ,'' as  MakeTime ," +
		    		" '' as ModifyDate , '' as ModifyTime , '' as Operator,a.PolYear as PolYear,a.Years  as Years,'' as AgentNo,a.Premiumtype as Premiumtype,a.Firstyear as Firstyear  from lidatatransresult a  where batchno='"+sBatchNo+"'" +
		    		"  and accountcode<>'2202010000' " +
		    		" group by a.BatchNo,a.ClassType,a.AccountCode, " +
		    		" a.FinItemType,a.AccountDate,a.SaleChnl,a.ManageCom,a.ExecuteCom,a.CostCenter,a.Budget,a.BClient,a.MarketType,a.PCont,a.riskcode,a.PolYear,a.Years,a.Premiumtype,a.Firstyear  having sum(a.SumMoney) <>0  "+
		    		"union all "+
		    "select  '' as  SerialNo,a.BatchNo as BatchNo , a.ClassType as ImportType, a.FinItemType as DCFlag, case when exists (select 1 from licodetrans where codetype='ClassType' and code=a.classtype) then " +
		    		" (select codealias from licodetrans where codetype='ClassType' and code=a.classtype) when exists (select 1 from licodetrans where codetype='ClassType' and code=substr(a.classtype,1,length(a.classtype)-2)) " +
		    		"  then (select 'Y'||substr(codealias,2,length(codealias)-1) from licodetrans where codetype='ClassType' and code=substr(a.classtype,1,length(a.classtype)-2)) end as VoucherType , " +
    		" 'CNY' as Currency, a.ManageCom  as DepCode ,a.ExecuteCom as FinMangerCount, '' as AccountCode , a.BClient as BClient ," +
    		"  a.CostCenter   as CostCenter, " +
    		" a.riskcode as RiskCode, a.MarketType  as  MarketType, a.SaleChnl  as  Chinal, a.PCont as PCont , a.Budget as CashFlowNo, " +
    		" a.AccountDate as ChargeDate, '' as ChargeTime,sum(a.SumMoney) as SumMoney, ''as VoucherID , " +
    		" '' as ReadState, '' as VoucherYear, '' as BackVDate, '' as BackVTime, '' as ReadDate , '' as ReadTime , '' as  MakeDate ,'' as  MakeTime ," +
    		" '' as ModifyDate , '' as ModifyTime , '' as Operator,a.PolYear as PolYear,a.Years  as Years,a.standbystring1 as AgentNo,a.Premiumtype as Premiumtype,a.Firstyear as Firstyear   from lidatatransresult a  where batchno='"+sBatchNo+"'" +
    		"  and accountcode='2202010000' " +
    		" group by a.BatchNo,a.ClassType,a.AccountCode, " +
    		" a.FinItemType,a.AccountDate,a.SaleChnl,a.ManageCom,a.ExecuteCom,a.CostCenter,a.Budget,a.BClient,a.MarketType,a.PCont,a.riskcode,a.PolYear,a.Years,a.standbystring1 ,a.Premiumtype,a.Firstyear having sum(a.SumMoney) <>0 with ur ";
//		    String tSql="select  '' as  SerialNo,a.BatchNo as BatchNo , a.ClassType as ImportType, a.FinItemType as DCFlag, (select codealias from licodetrans where codetype='ClassType' and code=a.classtype) as VoucherType , " +
//    		" 'CNY' as Currency, a.ManageCom  as DepCode ,a.ExecuteCom as FinMangerCount, a.AccountCode as AccountCode , a.BClient as BClient ," +
//    		"  a.CostCenter   as CostCenter, " +
//    		" a.riskcode as RiskCode, a.MarketType  as  MarketType, a.SaleChnl  as  Chinal, a.PCont as PCont , a.Budget as CashFlowNo, " +
//    		" a.AccountDate as ChargeDate, '' as ChargeTime,sum(a.SumMoney) as SumMoney, ''as VoucherID , " +
//    		" '' as ReadState, '' as VoucherYear, '' as BackVDate, '' as BackVTime, '' as ReadDate , '' as ReadTime , '' as  MakeDate ,'' as  MakeTime ," +
//    		" '' as ModifyDate , '' as ModifyTime , '' as Operator,a.PolYear as PolYear,a.Years  as Years,'' as AgentNo  from lidatatransresult a  where batchno='"+sBatchNo+"'" +
//    		"  and accountcode<>'2202010000' " +
//    		" group by a.BatchNo,a.ClassType,a.AccountCode, " +
//    		" a.FinItemType,a.AccountDate,a.SaleChnl,a.ManageCom,a.ExecuteCom,a.CostCenter,a.Budget,a.BClient,a.MarketType,a.PCont,a.riskcode,a.PolYear,a.Years  having sum(a.SumMoney) <>0  "+
//    		"union all "+
//    "select  '' as  SerialNo,a.BatchNo as BatchNo , a.ClassType as ImportType, a.FinItemType as DCFlag, (select codealias from licodetrans where codetype='ClassType' and code=a.classtype) as VoucherType , " +
//	" 'CNY' as Currency, a.ManageCom  as DepCode ,a.ExecuteCom as FinMangerCount, '' as AccountCode , a.BClient as BClient ," +
//	"  a.CostCenter   as CostCenter, " +
//	" a.riskcode as RiskCode, a.MarketType  as  MarketType, a.SaleChnl  as  Chinal, a.PCont as PCont , a.Budget as CashFlowNo, " +
//	" a.AccountDate as ChargeDate, '' as ChargeTime,sum(a.SumMoney) as SumMoney, ''as VoucherID , " +
//	" '' as ReadState, '' as VoucherYear, '' as BackVDate, '' as BackVTime, '' as ReadDate , '' as ReadTime , '' as  MakeDate ,'' as  MakeTime ," +
//	" '' as ModifyDate , '' as ModifyTime , '' as Operator,a.PolYear as PolYear,a.Years  as Years,a.standbystring1 as AgentNo   from lidatatransresult a  where batchno='"+sBatchNo+"'" +
//	"  and accountcode='2202010000' " +
//	" group by a.BatchNo,a.ClassType,a.AccountCode, " +
//	" a.FinItemType,a.AccountDate,a.SaleChnl,a.ManageCom,a.ExecuteCom,a.CostCenter,a.Budget,a.BClient,a.MarketType,a.PCont,a.riskcode,a.PolYear,a.Years,a.standbystring1  having sum(a.SumMoney) <>0 with ur ";
		    System.out.println("SQL:  "+tSql);
		    tSSRS = tExeSQL.execSQL(tSql);
//		    System.out.println(oLIDataTransResultSet.size());
	     //   tLICodeTransSet = tLICodeTransDB.executeQuery("select * from licodetrans where codetype = 'ClassType'");
		    //System.out.println()

		    /*1 先要得到连接(为了连接另外一个数据库)
		     *2 getconnection来实现
		     *3 DBConnectPol.getConnection来得到*/


			if(tSSRS.getMaxRow() == 0) flag =true;
			m_oconn = DBConnPool.getConnection();
	    	m_oconn.setAutoCommit(false);
	    	       //  conn = NewDBConnPool.getConnection();//new DBConn();
		   //	 conn.setAutoCommit(false);
			// 需要 汇总LIDataTrans

		    for(int i=1;i<=tSSRS.getMaxRow();i++){
		    	String tSerailNo=PubFun1.CreateMaxNo("FININTERFACE", 20);
		    	InterfaceTableDB oInterfaceTableDB=new InterfaceTableDB(m_oconn);

		    	oInterfaceTableDB.setSerialNo(tSerailNo);
		    	oInterfaceTableDB.setBatchNo(tSSRS.GetText(i,2));
		    	oInterfaceTableDB.setImportType(tSSRS.GetText(i,3));
		    	oInterfaceTableDB.setDCFlag(tSSRS.GetText(i,4));
		    	oInterfaceTableDB.setVoucherType(tSSRS.GetText(i,5));
		    	oInterfaceTableDB.setCurrency(tSSRS.GetText(i,6));

		    	oInterfaceTableDB.setDepCode(tSSRS.GetText(i,7));
		    	oInterfaceTableDB.setFinMangerCount(tSSRS.GetText(i,8));
		    	oInterfaceTableDB.setAccountCode(tSSRS.GetText(i,9));
		    	oInterfaceTableDB.setBClient(tSSRS.GetText(i,10));

		    	oInterfaceTableDB.setCostCenter(tSSRS.GetText(i,11));

		    	oInterfaceTableDB.setRiskCode(tSSRS.GetText(i,12));
		    	oInterfaceTableDB.setMarketType(tSSRS.GetText(i,13));
		    	oInterfaceTableDB.setChinal(tSSRS.GetText(i,14));
		    	oInterfaceTableDB.setPCont(tSSRS.GetText(i,15));
		    	oInterfaceTableDB.setCashFlowNo(tSSRS.GetText(i,16));
		    	oInterfaceTableDB.setChargeDate(tSSRS.GetText(i,17));
		    	oInterfaceTableDB.setChargeTime(tSSRS.GetText(i,18));
		    	oInterfaceTableDB.setSumMoney(tSSRS.GetText(i,19));
		    	oInterfaceTableDB.setVoucherID(tSSRS.GetText(i,20));
		    	oInterfaceTableDB.setReadState(tSSRS.GetText(i,21));
		    	oInterfaceTableDB.setVoucherYear(tSSRS.GetText(i,22));
		    	oInterfaceTableDB.setBackVDate(tSSRS.GetText(i,23));
		    	oInterfaceTableDB.setBackVTime(tSSRS.GetText(i,24));
		    	oInterfaceTableDB.setReadDate(tSSRS.GetText(i,25));
		    	oInterfaceTableDB.setReadTime(tSSRS.GetText(i,26));

		    	oInterfaceTableDB.setMakeDate(m_sDate);
		    	oInterfaceTableDB.setMakeTime(m_sTime);
		    	oInterfaceTableDB.setModifyDate(m_sDate);
		    	oInterfaceTableDB.setModifyTime(m_sTime);
		    	oInterfaceTableDB.setOperator("fin");

		    	oInterfaceTableDB.setPolYear(tSSRS.GetText(i,32));
		    	oInterfaceTableDB.setYears(tSSRS.GetText(i,33));
		    	oInterfaceTableDB.setAgentNo(tSSRS.GetText(i,34));
		    	oInterfaceTableDB.setPremiumType(tSSRS.GetText(i,35));
		    	oInterfaceTableDB.setFirstYear(tSSRS.GetText(i,36));
				if(oInterfaceTableDB.insert()){
					System.out.println("oK");
					flag = true;
				}else{
					System.out.println("FAIL");
					flag = false;
					break ;
				}
				System.out.println("/************InterfaceTable:" + i + "*************************/");
//				System.out.println("/************" + i + "新的标志*************************/");


		    }

		    if(flag){
		    	//存进当前数据库中的litranlog表中

		    	LITranLogDB oLITranLogDB = new LITranLogDB(m_oconn);
		    	oLITranLogDB.setBatchNo(sBatchNo);
		    	oLITranLogDB.setNumBuss(tInterfaceTableSet.size());
		    	//操作员 ，管理机构 ，入机日期， 入机时间
		    	oLITranLogDB.setMakeDate(m_sDate);
		    	oLITranLogDB.setMakeTime(m_sTime);
		    	oLITranLogDB.setFlag("3");
		    	if(oLITranLogDB.insert()){
//		    		System.out.println(oLITranLogDB.encode());
		    		// conn.commit();
		    		m_oconn.commit();
		    		System.out.println("commit");
		    	}else{
		    		// conn.rollback();
		    		m_oconn.rollback();
		    		System.out.println("Nocommit");
		    		CError tError = new CError();
				    tError.moduleName = "MoveDataFromGRP";
				    tError.functionName = "moveDataFromGRPToPRO";
				    tError.errorMessage = "更新数据失败!";
				    this.mErrors.addOneError(tError);
				    return false;
		    	}
		    }
		    else{
		    	m_oconn.rollback();
		    	CError tError = new CError();
			    tError.moduleName = "MoveDataFromGRP";
			    tError.functionName = "moveDataFromGRPToPRO";
			    tError.errorMessage = "插入数据失败!";
			    this.mErrors.addOneError(tError);
		    	return false;
		    }
		    m_oconn.close();
		    //conn.close();


        }
	  }catch(Exception e){
	         System.out.println("wrong!");
	         e.printStackTrace();
	         CError tError = new CError();
			 tError.moduleName = "MoveDataFromGRP";
			 tError.functionName = "moveDataFromGRPToPRO";
			 tError.errorMessage = "更新数据失败!" + e.getMessage();
			 this.mErrors.addOneError(tError);
			 return false;
	   }
	    finally{
	    	try{
	    		//conn.close();
	    		 m_oconn.close();
	    	}catch(Exception e){
	    		System.out.println("关闭连接池失败!");
	    	}
	    }

		return true;
	}
        /**/
        private boolean updateCostCenter()
        {
            try
            {
                conn = DBConnPool.getConnection();
                conn.setAutoCommit(false);
                ExeSQL updateExeSQL = new ExeSQL(conn);
                SSRS tSSRS = new SSRS();
                String branchseries=new String();
                try {
                    //updateExeSQL.execUpdateSQL("update labranchgroup a  set a.costcenter =(select b.costcenter  from labranchgroup b where b.agentgroup=substr(a.branchseries,1,12)) where 1=1");
                    tSSRS=updateExeSQL.execSQL("select distinct a.branchseries from labranchgroup a,labranchgroup b where 1=1 " 
                           +"and a.upbranch is null "
                           +"and a.agentgroup=substr(b.branchseries,1,12) "
                           +"and ( (b.costcenter is null and a.costcenter is not null) or a.costcenter <> b.costcenter ) "
                           +"with ur");
                    for(int i=1;i<=tSSRS.MaxRow;i++){
                    branchseries=tSSRS.GetText(i, 1);
                    updateExeSQL.execUpdateSQL("update labranchgroup a  set a.costcenter =(select b.costcenter  from labranchgroup b where b.agentgroup='"
                            +branchseries+"') where substr(a.branchseries,1,12) = '"+branchseries+"' with ur");
                    }
                    conn.commit(); 
                } catch (Exception ex) {
                    conn.rollback();
                    conn.close();
                    ex.printStackTrace();
                    System.out.println("wrong update!");
                    CError tError = new CError();
                    tError.moduleName = "MoveDataFromGRP";
                    tError.functionName = "updateCostCenter";
                    tError.errorMessage = "更新数据失败!" + ex.getMessage();
                    this.mErrors.addOneError(tError);
                    return false;
                } finally {
                    try {
                        conn.close();
                    } catch (Exception e) {
                        System.out.println("关闭连接池失败!");
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

}
