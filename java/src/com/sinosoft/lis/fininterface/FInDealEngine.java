package com.sinosoft.lis.fininterface;

import java.util.Calendar;
import java.util.Date;

import com.sinosoft.lis.db.LIDistillLogDB;
import com.sinosoft.lis.db.LIOperationDataClassDefDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LIDistillLogSchema;
import com.sinosoft.lis.vschema.LIDistillLogSet;
import com.sinosoft.lis.vschema.LIOperationDataClassDefSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
// import com.sinosoft.lis.fininterface.updateLcMidAirPol;


/**
 * <p>
 * Title: FInDealEngine
 * </p>
 * 
 * <p>
 * Description: 财务接口转换处理类
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2006
 * </p>
 * 
 * <p>
 * Company: sinosoft
 * </p>
 * 
 * @author jianan
 * @version 1.0
 */

public class FInDealEngine {

	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();

	/** 存储处理信息 */
	private VData mInputInfo = new VData();
	
	public boolean SerFlag = false;

	/** 存储业务数据类型 */
	private LIOperationDataClassDefSet mLIOperationDataClassDefSet;

	/** 存储业务类型处理类 */
	private FinDataType mFinDataType[];
	
	private long sTime1;
	private long sTime2;
	private String strTime1 = "";

	/** 业务数据 */
	GlobalInput mGlobalInput = new GlobalInput();

	String StartDate = "";

	String EndDate = "";

	String BatchNo = "";
	
	String cClassType = "";
	
	String cGetType = "";

	public FInDealEngine() {
	}

	/**
	 * 更新批处理跑的日志
	 * 
	 * @return
	 */
	public synchronized boolean DealDate(VData tVData) {

		try{
			boolean serviceFlag = false;
			LIDistillLogDB oLIDistillLogDB = new LIDistillLogDB();
			oLIDistillLogDB.setBatchNo("FinInterFaceService");
			String strDate = PubFun.getCurrentDate();
			String strTime = PubFun.getCurrentTime();
			String strCom = mGlobalInput.ManageCom;
			String strOperator = mGlobalInput.Operator;
			if (!oLIDistillLogDB.getInfo()) {
				// 没有配置 重新配置
				oLIDistillLogDB.setFlag("1");// 没有跑批日志信息
				oLIDistillLogDB.setStartDate(tVData.getObject(1).toString());
				oLIDistillLogDB.setEndDate(tVData.getObject(2).toString());
				oLIDistillLogDB.setMakeDate(strDate);
				oLIDistillLogDB.setMakeTime(strTime);
				oLIDistillLogDB.setManageCom(strCom);
				oLIDistillLogDB.setOperator(strOperator);
				serviceFlag = true;
				if (!oLIDistillLogDB.insert()) {
					buildError("DealDate", "配置批处理初始信息失败!具体失败原因:"
							+ oLIDistillLogDB.mErrors.getFirstError());
					return false;
				}
			}
	
			/**
			 * 对应的批处理存在核对
			 */
			if (!serviceFlag) {
				//没有配置信息的时候需要更新对应的标志
				if (oLIDistillLogDB.getFlag() != null
						&& !oLIDistillLogDB.getFlag().equals("null")
						&& oLIDistillLogDB.getFlag().equals("1")) {
					// 已经存在了 已经有批处理在进行中
					buildError("DealDate", "已经存在批处理进行中!");
					return false;
				} else {
					GlobalInput mG = new GlobalInput();
					mG.setSchema((GlobalInput)tVData.getObjectByObjectName(
							"GlobalInput", 0));
					// 没有 更新为有批处理在进行中
					oLIDistillLogDB.setFlag("1");
					oLIDistillLogDB.setMakeDate(PubFun.getCurrentDate());
					oLIDistillLogDB.setMakeTime(PubFun.getCurrentTime());
					oLIDistillLogDB.setStartDate(tVData.getObject(1).toString());
					oLIDistillLogDB.setEndDate(tVData.getObject(2).toString());
					oLIDistillLogDB.setManageCom(mG.ManageCom);
					oLIDistillLogDB.setOperator(mG.Operator);

					
					/**
					 * 对应的更新批处理日志文件
					 */
					if (!oLIDistillLogDB.update()) {
						// 更新成功
						buildError("DealDate", "更新批处理日志失败!具体原因:"
								+ oLIDistillLogDB.mErrors.getFirstError());
						return false;
					}
				}
			}
			//表示有批处理在进行不需要更新对应的日志信息
			SerFlag = true;
			
			sTime1 = Calendar.getInstance().getTimeInMillis();
			strTime1 = PubFun.getCurrentDate() + " " + PubFun.getCurrentTime();
			System.out.println("/****************/开始提数时间 :" + strTime1);
			return true;
		}
		catch (Exception ex) {
			buildError("DealDate", ex.getMessage());
			return false;
		}
	}

	/**
	 * 更新日志----没有批处理提取 以便前台直接调用
	 * 
	 * @return
	 */
	public synchronized boolean updateLIDistillLog() {

		try{
			String strSQL = "update lidistilllog set flag = '0' where batchno = 'FinInterFaceService'";
			MMap oMap = new MMap();
			oMap.put(strSQL,"UPDATE");
			VData mResult = new VData();
			mResult.add(oMap);
			PubSubmit oPubSubmit = new PubSubmit();
			if(!oPubSubmit.submitData(mResult,"UPADTE")){
			
				buildError("updateLIDistillLog","具体错误信息:" + oPubSubmit.mErrors.getFirstError());
				return false;
			}
			SerFlag = false;
			return true;
		}
		catch (Exception ex) {
			buildError("updateLIDistillLog", ex.getMessage());
			return false;
		}
	}

	public boolean dealProcess(VData cInputData) {
		
		if (!getInputData(cInputData)) {
			return false;
		}
		if (!InitInfo()) {
			return false;
		}
		if (!DealWithData()) {
			return false;
		}
		if (!WriteLog("SUCC")) {
			return false;
		}
		
		System.out.println("/****************/开始提数时间 :" + strTime1);
		System.out.println("/****************/结束提数时间 :"
				+ PubFun.getCurrentDate() + " " + PubFun.getCurrentTime());
		sTime2 = Calendar.getInstance().getTimeInMillis();

		System.out.println("/****************/时间间隔 :"
				+ String.valueOf((sTime2 - sTime1) / 1000) + "秒");
		return true;
	}

	private boolean getInputData(VData cInputData) {
		
		try{
			BatchNo = PubFun1.CreateMaxNo("FinBatch", 20);
			mGlobalInput.setSchema((GlobalInput) cInputData.getObjectByObjectName(
					"GlobalInput", 0));
//			System.out.println(mGlobalInput.ManageCom);
//			System.out.println(mGlobalInput.Operator);
			StartDate = (String) cInputData.get(1);
			EndDate = (String) cInputData.get(2);
			cClassType = (String) cInputData.get(3);
			cGetType = (String) cInputData.get(4);
	
			if (mGlobalInput == null) {
				buildError("getInputData", "前台传入的登陆信息为空!");
				return false;
			}
			if (StartDate == null || StartDate.equals("")) {
				buildError("getInputData", "没有起始日期!");
				return false;
			}
			if (EndDate == null || EndDate.equals("")) {
				buildError("getInputData", "没有终止日期!");
				return false;
			}
			if (cGetType == null || cGetType.equals("")) {
				buildError("getInputData", "没有提取方式!");
				return false;
			}
			if ((cClassType == null || cClassType.equals("")) && cGetType.equals("2")) {
				buildError("getInputData", "没有凭证类型信息!");
				return false;
			}
			
			return true;
		}catch (Exception ex) {
			buildError("getInputData", ex.getMessage());
			return false;
		}
		
	}

	private boolean InitInfo() {
		try {
			LIOperationDataClassDefDB tLIOperationDataClassDefDB = new LIOperationDataClassDefDB();
			
			String oSQL = getClassTypeInfo();
			System.out.println(oSQL);
			mLIOperationDataClassDefSet = tLIOperationDataClassDefDB
					.executeQuery(oSQL);
			if (mLIOperationDataClassDefSet.size() > 0) {
				mFinDataType = new FinDataType[mLIOperationDataClassDefSet
						.size()];

				for (int i = 1; i <= mLIOperationDataClassDefSet.size(); i++) {
					mFinDataType[i - 1] = new FinDataType();
					if (!mFinDataType[i - 1]
							.InitFinDataTypeInfo(mLIOperationDataClassDefSet
									.get(i).getClassID())) {
						buildError("InitInfo", "处理引擎初始化失败" + mFinDataType[i - 1].mErrors.getFirstError());
						return false;
					}
				}
				return true;
			} else {
				buildError("InitInfo", "处理引擎初始化失败,无任何数据类型定义");
				return false;
			}
		} catch (Exception ex) {
			buildError("InitInfo", ex.getMessage());
			return false;
		}

	}

	private String getClassTypeInfo(){

		String tSQL = "select * from lioperationdataclassdef where state = 'Y' ";
		String strClassType = "";
		if(cGetType.equals("2")){
			System.out.println("cClassType:" + cClassType);
			String oArray[] = cClassType.split(",");
			strClassType = "and classtype in (";
			
			for(int i=0;i<oArray.length;i++){
				strClassType = strClassType + "'" + oArray[i] + "'";
				if(i<oArray.length-1){
					strClassType = strClassType + ",";
				}
			}
			strClassType = strClassType + ")";
			System.out.println(strClassType);
		}
		tSQL = tSQL + strClassType + " order by classid";
		return tSQL;
	}
	
	private boolean DealWithData() {
		try {
            FDate chgdate = new FDate();
            Date dbdate = chgdate.getDate(StartDate);
            Date dedate = chgdate.getDate(EndDate);
            long ClassTime1 = 0;
            long ClassTime2 = 0;
            
            while (dbdate.compareTo(dedate) <= 0) {
            	mInputInfo = new VData();
    			mInputInfo.add(mGlobalInput);
    			mInputInfo.add(chgdate.getString(dbdate));
    			mInputInfo.add(chgdate.getString(dbdate));
    			mInputInfo.add(BatchNo); 
    			System.out.println("开始提取" + dbdate + "的数据...");
				for (int i = 0; i < mFinDataType.length; i++) {
					ClassTime1 = Calendar.getInstance().getTimeInMillis();
					while(mFinDataType[i].haveDataUnsettled(mInputInfo)) {
						if (!mFinDataType[i].dealProcess(mInputInfo)) {
							this.mErrors.copyAllErrors(mFinDataType[i].mErrors);
							return false;
						}
					}
					if (mFinDataType[i].mErrors.needDealError()) {
						this.mErrors.copyAllErrors(mFinDataType[i].mErrors);
						return false;
					}
					ClassTime2 = Calendar.getInstance().getTimeInMillis();
					System.out.println("=======提取该业务类型耗费的总时间为：" + String.valueOf((ClassTime2 - ClassTime1) / 1000) + "秒=======");
				}
                dbdate = PubFun.calDate(dbdate, 1, "D", null);
            }
			return true;
		} catch (Exception ex) {
			buildError("DealWithData", ex.getMessage());
			return false;
		}

	}

	private boolean WriteLog(String tFlag) {
		try {
			DataToSubmit tDataToSubmit = new DataToSubmit();
			VData lInputData = new VData();
			LIDistillLogSet tLIDistillLogSet = new LIDistillLogSet();
			LIDistillLogSchema tLIDistillLogSchema = new LIDistillLogSchema();
			tLIDistillLogSchema.setBatchNo(BatchNo);
			tLIDistillLogSchema.setStartDate(StartDate);
			tLIDistillLogSchema.setEndDate(EndDate);
			tLIDistillLogSchema.setFlag(tFlag);
			tLIDistillLogSchema.setMakeDate(PubFun.getCurrentDate());
			tLIDistillLogSchema.setMakeTime(PubFun.getCurrentTime());
			tLIDistillLogSchema.setManageCom(mGlobalInput.ManageCom);
			tLIDistillLogSchema.setOperator(mGlobalInput.Operator);
			tLIDistillLogSet.add(tLIDistillLogSchema);
			lInputData.add(tLIDistillLogSet);
			if (!tDataToSubmit.submitData(lInputData, "")) {
				buildError("WriteLog", "记录提数日志出错！");
				return false;
			}

			return true;
		} catch (Exception ex) {
			buildError("WriteLog", ex.getMessage());
			return false;
		}
	}

	public boolean WriteErrLog() {
		try {
			if (this.mErrors.needDealError()) {
				System.out.println("WriteErrLog Start ...");
				DataToSubmit tDataToSubmit = new DataToSubmit();
				VData lInputData = new VData();
				LIDistillLogSet tLIDistillLogSet = new LIDistillLogSet();
				LIDistillLogSchema tLIDistillLogSchema = new LIDistillLogSchema();
				tLIDistillLogSchema.setBatchNo(BatchNo);
				tLIDistillLogSchema.setStartDate(StartDate);
				tLIDistillLogSchema.setEndDate(EndDate);
				tLIDistillLogSchema.setFlag("Lose");
				tLIDistillLogSchema.setFilePath(this.mErrors.getFirstError());
				tLIDistillLogSchema.setMakeDate(PubFun.getCurrentDate());
				tLIDistillLogSchema.setMakeTime(PubFun.getCurrentTime());
				tLIDistillLogSchema.setManageCom(mGlobalInput.ManageCom);
				tLIDistillLogSchema.setOperator(mGlobalInput.Operator);
				tLIDistillLogSet.add(tLIDistillLogSchema);
				lInputData.add(tLIDistillLogSet);
				if (!tDataToSubmit.submitData(lInputData, "")) {
					this.mErrors = new CErrors();
					buildError("WriteErrLog", "记录提数日志出错！"
							+ tDataToSubmit.mErrors.getFirstError());
					return false;
				}
				return true;
			} else {
				return true;
			}

		} catch (Exception ex) {
			this.mErrors = new CErrors();
			buildError("WriteLog", ex.getMessage());
			return false;
		}
	}

	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "FInDealEngine";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
		System.out.print(szErrMsg);
	}

	public static void main(String[] args) {

		VData vData = new VData();
		GlobalInput tG = new GlobalInput();
		tG.Operator = "001";
		tG.ManageCom = "86";
		// String bdate = "2007-05-01";
		// String edate = "2007-06-30";

		String bdate = "2007-07-09";
		String edate = "2007-07-13";
		String cClassType = "B-01,B-02,B-03,";
		String cGetType = "2";

		vData.addElement(tG);
		vData.addElement(bdate);
		vData.addElement(edate);
		vData.addElement(cClassType);
		vData.addElement(cGetType);
		
		System.out.println(vData.getObject(1).toString());
		
		FInDealEngine tFInDealEngine = new FInDealEngine();
        if(tFInDealEngine.DealDate(vData))
        {
		// System.out.println();
		// Calendar oCalendar = Calendar.getInstance();
		long sTime1 = Calendar.getInstance().getTimeInMillis();
		System.out.println("/****************/开始提数时间 :"
				+ Calendar.getInstance().getTimeInMillis());

		if (!tFInDealEngine.dealProcess(vData)) {
			System.out.println(tFInDealEngine.mErrors.getFirstError());
		}
		if (!tFInDealEngine.updateLIDistillLog()) {
			System.out.println(tFInDealEngine.mErrors.getFirstError());
			System.out.println("afdafasdasdfasdf");
		}
		//Calendar oCalendar2 = Calendar.getInstance();

		// long sTime2 = oCalendar2.getTimeInMillis();
		System.out.println("/****************/结束提数时间 :"
				+ Calendar.getInstance().getTimeInMillis());
		long sTime2 = Calendar.getInstance().getTimeInMillis();
		System.out.println("/****************/开始提数时间 :" + sTime1);
		System.out.println("/****************/时间间隔 :"
				+ String.valueOf((sTime2 - sTime1) / 1000) + "秒");
        }
	}

}
