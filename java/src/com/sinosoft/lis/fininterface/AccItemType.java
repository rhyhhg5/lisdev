package com.sinosoft.lis.fininterface;

import com.sinosoft.lis.schema.LIDataTransResultSchema;
import com.sinosoft.lis.schema.LIAboriginalDataSchema;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public interface AccItemType {
    public LIDataTransResultSchema DealInfo(LIAboriginalDataSchema tLIAboriginalDataSchema) throws Exception;
}
