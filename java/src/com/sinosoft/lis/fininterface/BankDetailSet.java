package com.sinosoft.lis.fininterface;

import java.sql.Connection;
import java.sql.SQLException;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;

public class BankDetailSet{
	
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public CErrors mErrors = new CErrors();
	private String mBankCode = "";
	private String mManageCom = "";
	private String mAccountType = "";
	private String mBankDetail = "";
	private String mDealType = "";
	
	
	
	private boolean getInputData(VData tVData){
		try{
			mBankCode = (String)tVData.get(0);
			mManageCom = (String)tVData.get(1);
			mAccountType = (String)tVData.get(2);
			mBankDetail = (String)tVData.get(3);
			mDealType = (String)tVData.get(4);
			
			if(mBankCode==null||mBankCode.equals("")||mManageCom==null||mManageCom.equals("")
					||mAccountType==null||mAccountType.equals("")||mDealType==null||mDealType.equals("")){
				buildError("getInputData", "前台传入的信息为空!");
				return false;	
			}
			
			if(mDealType.equals("add")||mDealType.equals("update")){
				if(mBankDetail.equals("")||mBankDetail==null){
					buildError("getInputData", "前台传入的信息为空!");
					return false;
				}
			}
			
			if(mAccountType.equals("S"))
				mAccountType = "bankdetailS";
			else
				mAccountType = "bankdetailF";
			
			return true;
		}catch(Exception ex){
			buildError("getInputData", "在取得前台传入的信息时失败，原因是：" + ex.getMessage());
			return false;
		}
	}
	
	private boolean checkData(){
		try{

			SSRS oSSRS = new SSRS();
			ExeSQL tExeSQL = new ExeSQL();
			oSSRS = tExeSQL.execSQL("select 1 from ldbank where bankcode = '" + mBankCode + "' and comcode = '" + mManageCom + "'");
			if(oSSRS.MaxRow<=0){
				buildError("checkData", "系统中没有机构" + mManageCom + "下的银行" + mBankCode + "的定义！");
				return false;
			}
			
			if(mDealType.equals("add")){
				if(mManageCom.length()==4)
					mManageCom = mManageCom + "01";
				if(mManageCom.length()>=6)
					mManageCom = mManageCom.substring(0,6);
				oSSRS = tExeSQL.execSQL("select 1 from ldcode1 where code = '" + mBankCode + "' and code1 = '" + mManageCom + "' and codetype = '" + mAccountType + "'");
				if(oSSRS.MaxRow>0){
					buildError("checkData", "系统中已有该银行帐户的定义！");
					return false;
				}
			}
			
			if(mDealType.equals("update")||mDealType.equals("del")){
				if(mManageCom.length()==4)
					mManageCom = mManageCom + "01";
				if(mManageCom.length()>=6)
					mManageCom = mManageCom.substring(0,6);
				oSSRS = tExeSQL.execSQL("select 1 from ldcode1 where code = '" + mBankCode + "' and code1 = '" + mManageCom + "' and codetype = '" + mAccountType + "'");
				if(oSSRS.MaxRow==0){
					buildError("checkData", "系统中尚无该银行帐户明细的定义，无法修改！");
					return false;
				}
			}
			
			return true;
		}catch(Exception ex){
			buildError("getInputData", "在检查银行帐户信息时失败，原因是：" + ex.getMessage());
			return false;
		}
	}
	
	public boolean dealData(VData tVData){
		if(!getInputData(tVData))
			return false;
		if(!checkData())
			return false;
		
		try{
			String tSQL = "";
			ExeSQL tExeSQL = new ExeSQL();
			
			if(mDealType.equals("add")){
				tSQL = "insert into ldcode1 values ('" + mAccountType + "','" + mBankCode + "','" + mManageCom + "','" + mBankDetail + "','','','')";
				tExeSQL.execUpdateSQL(tSQL);
			}
			if(mDealType.equals("update")){
				tSQL = "update ldcode1 set codename = '" + mBankDetail + "' where codetype = '" + mAccountType + "' and code = '" + mBankCode + "' and code1 = '" + mManageCom + "'";
				tExeSQL.execUpdateSQL(tSQL);		
			}
			if(mDealType.equals("del")){
				tSQL = "delete ldcode1 where codetype = '" + mAccountType + "' and code = '" + mBankCode + "' and code1 = '" + mManageCom + "'";
				tExeSQL.execUpdateSQL(tSQL);	
			}
			return true;
		}catch(Exception ex){
			buildError("dealData", "在添加银行帐户信息时失败，原因是：" + ex.getMessage());
			return false;
		}
	}
	
	private void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "FInDealEngine";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
		System.out.print(szErrMsg);
	}
	
	public static void main(String[] args) {
		VData tVData = new VData();
		tVData.add("0101");
		tVData.add("86010101");
		tVData.add("S");
		tVData.add("");
		tVData.add("del");
		
		BankDetailSet tBankDetailSet = new BankDetailSet();
		if(tBankDetailSet.dealData(tVData)){
			System.out.println("AAA");
		}
		System.out.println("BBB");
	}
}