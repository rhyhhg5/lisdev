package com.sinosoft.lis.fininterface;

import java.sql.SQLException;

import com.sinosoft.lis.db.LIDistillLogDB;
import com.sinosoft.lis.db.LIDataTransResultDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LIDataTransResultSchema;
import com.sinosoft.lis.vschema.LIDataTransResultSet;
import com.sinosoft.lis.vschema.LIOperationDataClassDefSet;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;

/**
 * <p>
 * Title: DataDetailCheckQuery.java
 * </p>
 * 
 * <p>
 * Description: 查询对帐不平的明细数据
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2007
 * </p>
 * 
 * <p>
 * Company: sinosoft
 * </p>
 * 
 * @author linyun
 * @version 1.0
 */
 
public class DataDetailCheckQuery {
	
	String tSQL = "";
	ExeSQL tExeSQL = new ExeSQL();
	
	public DataDetailCheckQuery(){
	}

	public boolean CheckDetailData(String tClassType){
		
		try{
			String KeyField = new String();
			KeyField = tExeSQL.getOneValue("select keyid from liclasstypekeydef where classtype = '" + tClassType + "'");
			System.out.println("KeyField = " + KeyField);
			String[] tKey = KeyField.split(",");
			
			//在liclasstypekeydef表中的keyid字段可以考虑用","分隔不同的标识字段，类似于lidetailfinitemdef的处理方式
			//此处暂时不需要分隔，因此下面的SQL中只取了数组的第一个值
//			tSQL = "select * from (select (select keyname from liclasstypekeydef where keyid = '" + tKey[0] + "') as " + tKey[0] + ",classtype,sum(cside) as cside,sum(dside) as dside,accountdate,managecom from " +
//					" (select a." + tKey[0] + " as " + tKey[0] + ",(select classtypename from liclasstypekeydef where classtype = b.classtype) as classtype,b.finitemtype as finitemtype, " +
//					" (case finitemtype when 'C' then 0 when 'D' then sum(b.summoney) end) as cside, " +
//					" (case finitemtype when 'D' then 0 when 'C' then sum(b.summoney) end) as dside, " +
//					" b.accountdate as accountdate,b.managecom as managecom from liaboriginaldata a,lidatatransresult b " +
//					" where a.serialno = b.standbystring3 and b.batchno = '" + tBatchNo + "' and b.classtype = '" + tClassType + "'" +
//					" group by a." + tKey[0] + " ,b.classtype,b.finitemtype,b.accountdate,b.managecom) " +
//					" group by " + tKey[0] + ",classtype,accountdate,managecom) " +
//					" where cside <> dside" +
//					" order by accountdate";
			
//			tSQL = "select (select classtypename from liclasstypekeydef where classtype = r.classtype)," + 
//					" r.accountcode||'-'||(select codename from ldcode where codetype = 'accountcode' and trim(code) = trim(r.accountcode))," +
//					" r.finitemtype,(select keyname from liclasstypekeydef where classtype = '" + tClassType + "') as fieldname,s." + tKey[0] + "," +
//					" sum(r.summoney),r.accountdate from lidatatransresult r,liaboriginaldata s where r.standbystring3 = s.serialno" +
//					" and s." + tKey[0] + " in " +
//					" (select " + tKey[0] + " from (select " + tKey[0] + " as " + tKey[0] + ",sum(cside) as cside,sum(dside) as dside from" +
//					" (select a." + tKey[0] + " as " + tKey[0] + ",b.finitemtype as finitemtype," +
//					" (case finitemtype when 'C' then 0 when 'D' then sum(b.summoney) end) as cside," +
//					" (case finitemtype when 'D' then 0 when 'C' then sum(b.summoney) end) as dside," +
//					" b.accountdate as accountdate from liaboriginaldata a,lidatatransresult b " +
//					" where a.serialno = b.standbystring3  and b.batchno = '" + tBatchNo + "' and b.classtype = '" + tClassType + "'" +
//					" group by a." + tKey[0] +" ,b.finitemtype,b.accountdate) " +
//					" group by " + tKey[0] +",accountdate) " +
//					" where cside <> dside) and r.classtype = '" + tClassType + "' and r.batchno = '" + tBatchNo + "'" +
//					" group by r.classtype,r.accountcode,r.finitemtype,s." + tKey[0] +",r.accountdate " +
//					" order by r.classtype,s." + tKey[0] +",r.finitemtype desc,r.accountdate";
			
//			tSQL = "select * from (select classtype,(select keyname from liclasstypekeydef " +
//					" where classtype = '" + tClassType + "') as name," + tKey[0] + ",sum(dside) as dside,sum(cside) as cside,accountdate,managecom from " +
//					" (select a." + tKey[0] + " as " + tKey[0] + ",(select classtypename from liclasstypekeydef " +
//					" where classtype = b.classtype) as classtype,b.finitemtype as finitemtype," +
//					" (case finitemtype when 'C' then 0 when 'D' then sum(b.summoney) end) as cside," +
//					" (case finitemtype when 'D' then 0 when 'C' then sum(b.summoney) end) as dside," +
//					" b.accountdate as accountdate,b.managecom as managecom from liaboriginaldata a,lidatatransresult b " +
//					" where a.serialno = b.standbystring3 and a.batchno = '" + tBatchNo + "' and b.classtype = '" + tClassType + "'" +
//					" group by a." + tKey[0] + " ,b.classtype,b.finitemtype,b.accountdate,b.managecom)" +
//					" group by " + tKey[0] + ",classtype,accountdate,managecom)" +
//					" where cside <> dside" +
//					" order by classtype," + tKey[0] + ",accountdate,managecom";
			
			tSQL = "select (select classtypename from liclasstypekeydef where classtype = '" + tClassType + "'),(select keyid from liclasstypekeydef where classtype = '" + tClassType + "')," +
					" (select keyname from liclasstypekeydef where classtype = '" + tClassType + "')," + tKey[0] + 
					",sum(dside),sum(cside),managecom,accoutdate from " +
					" (select b.classtype as classtype,a." + tKey[0] + " as " + tKey[0] + "," +
					" (case b.finitemtype when 'C' then 0 when 'D' then sum(summoney) end) as dside," +
					" (case b.finitemtype when 'D' then 0 when 'C' then sum(summoney) end) as cside," +
					" b.accountdate as accoutdate,b.managecom as managecom" +
					" from liaboriginaldata a,lidatatransresult b" +
					" where a.serialno = b.standbystring3 and b.classtype = '" + tClassType + "'" +
					" group by b.classtype,a." + tKey[0] + ", b.managecom,b.accountdate,b.finitemtype) tab1" +
					"  group by classtype," + tKey[0] + ",accoutdate,managecom having sum(dside)<>sum(cside)";
			
			System.out.println(tSQL);
				
			return true;
		}catch(Exception ex){
			System.out.println(ex.getMessage());
			return false;
		}
  }
	
	public String getResult(){
		return tSQL;
	}

    public static void main(String[] args) throws SQLException{
    	DataDetailCheckQuery tDataDetailCheckQuery = new DataDetailCheckQuery();
    	String cSQL = "";
    	if(tDataDetailCheckQuery.CheckDetailData("C-02"))
    		cSQL = tDataDetailCheckQuery.getResult();
    	System.out.println(cSQL);
    }
}