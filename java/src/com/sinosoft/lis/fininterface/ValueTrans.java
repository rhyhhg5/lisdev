package com.sinosoft.lis.fininterface;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.vschema.LICodeTransSet;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LIInfoFinItemAssociatedSchema;
import com.sinosoft.lis.db.LICodeTransDB;
import com.sinosoft.lis.schema.LIAboriginalDataSchema;
import com.sinosoft.lis.schema.LICodeTransSchema;

/**
 * <p>Title: ValueTrans</p>
 *
 * <p>Description: 代码转换类 </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author jianan
 * @version 1.0
 */
public class ValueTrans {

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 存储返回结果 */
    private VData mResult = new VData();
    /** 存储转换数据对照表*/
    private LICodeTransSet mLICodeTransSet;
    /** 转换处理类*/
    private TransType mTransTypeClass;
    /** 转换处理标志*/
    private String mTransMode;


    public ValueTrans() {
    }

    public boolean InitInfo(LIInfoFinItemAssociatedSchema tLIInfoFinItemAssociatedSchema)
    {
       try{
           mTransMode = tLIInfoFinItemAssociatedSchema.getTransFlag();
           if (mTransMode == null || mTransMode.equals("")) {
               buildError("InitInfo",
                          "科目" + tLIInfoFinItemAssociatedSchema.getFinItemID() +
                          "关联转换项" +
                          tLIInfoFinItemAssociatedSchema.getFountainID() +
                          "的转换方式未定义");
               return false;
           }
           if (mTransMode.equals("1")) {

           } else if (mTransMode.equals("2")) {
               LICodeTransDB tLICodeTransDB = new LICodeTransDB();
               tLICodeTransDB.setCodeType(tLIInfoFinItemAssociatedSchema.
                                          getTransCode());
               mLICodeTransSet = tLICodeTransDB.query();
               if (tLICodeTransDB.mErrors.needDealError()) {
                   buildError("InitInfo",
                              "查询转换编码" +
                              tLIInfoFinItemAssociatedSchema.getTransCode() +
                              "出错");
                   return false;
               } else {
                   if (mLICodeTransSet.size() == 0) {
                       buildError("InitInfo",
                                  "转换编码" +
                                  tLIInfoFinItemAssociatedSchema.getTransCode() +
                                  "未定义");
                       return false;
                   }
               }
           } else if (mTransMode.equals("3")) {
               Class tClass = Class.forName(tLIInfoFinItemAssociatedSchema.getTransClass());
               mTransTypeClass = (TransType) tClass.newInstance();
           } else {
               mTransMode = "1";
           }
           return true;
       }
       catch(Exception ex)
       {
           buildError("InitInfo",ex.getMessage());
           return false;
       }
    }

    public String TransDeal(String TransValue,LIAboriginalDataSchema tLIAboriginalDataSchema) throws Exception
    {
       String resultValue = "";
        try {
            if (mTransMode.equals("1")) {
                resultValue = TransValue;
            } else if (mTransMode.equals("2")) {

                for (int i = 1; i <= mLICodeTransSet.size(); i++) {
                    LICodeTransSchema tLICodeTransSchema = mLICodeTransSet.get(i);
                    if (tLICodeTransSchema.getCode().trim().equals(TransValue.trim())) {
                        resultValue = tLICodeTransSchema.getCodeAlias();
                        break;
                    }
                    if (i == mLICodeTransSet.size()) {
                        Exception error = new  Exception("查询转换编码" + TransValue + "出错");
                        throw error;
                    }
                }

            } else if (mTransMode.equals("3")) {
                resultValue = mTransTypeClass.transInfo(TransValue,
                        tLIAboriginalDataSchema);
            }
        }
        catch(Exception ex)
        {
           throw ex;
        }
        return resultValue;
    }


    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "ValueTrans";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
        System.out.print(szErrMsg);
    }


    public static void main(String[] args) {


        LIInfoFinItemAssociatedSchema  tt = new   LIInfoFinItemAssociatedSchema();
        LIAboriginalDataSchema tLIAboriginalDataSchema = new  LIAboriginalDataSchema();

        tt.setTransFlag("3");
        tt.setTransCode("test");
        tt.setTransClass("com.sinosoft.lis.fininterface.TransItem.testu88");


        try {

             ValueTrans tValueTrans = new ValueTrans();
             if(!tValueTrans.InitInfo(tt))
             {
                 System.out.println("111");
                 System.out.println("222");
                 System.out.println("333");
                 System.out.println("444");
                 System.out.println("555");
             }
             String temp = "fff";
             temp = tValueTrans.TransDeal(temp,tLIAboriginalDataSchema);
             System.out.println(temp);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

}
