package com.sinosoft.lis.fininterface;

/**
 * <Title>:处理财务接口中每批生成数据的流水号</Title>
 * <Company>:SINSOFT</Company>
 * @creatTime 2007-2-9
 * @author lijs
 */
import java.sql.Connection;
import com.sinosoft.utility.DBConnPool;
import com.sinosoft.utility.ExeSQL;

public class FinCreateSerialNo {

    /**
     * @param args
     */
    public static void main(String[] args) {

        try {
            String[] strArrSerialNo = getSerialNo(2);
            for (int i = 0; i < 10; i++) {
                System.out.println(strArrSerialNo[i]);
            }
        } catch (Exception e) {

        }

    }

    /**在处理批次前自动生成系列号
     * 参数:size 长度
     */
    public static synchronized String[] getSerialNo(int size) throws Exception {

        int num = 0; //标记初始的数字
        String tSBql = "select maxno from ldmaxno where notype = 'FINSERIALNO' and nolimit = 'SN' for update";
        Connection conn = DBConnPool.getConnection();
        try {
            conn.setAutoCommit(false);
            ExeSQL exeSQL = new ExeSQL(conn);
            String rsData = null;
            rsData = exeSQL.getOneValue(tSBql.toString());
            /***********************************************************************/
            /** 创建新的序列号 **/
            //如果没有创建的话则直接创建并且添加size个序号
            if ((rsData == null) || rsData.equals("")) {
                tSBql =
                        "insert into ldmaxno(notype, nolimit, maxno) values('FINSERIALNO','SN'," +
                        size + ")";
                exeSQL = new ExeSQL(conn);
                if (!exeSQL.execUpdateSQL(tSBql.toString())) {
                    System.out.println("CreateMaxNo 插入失败，请重试!");
                    conn.rollback();
                    conn.close();
                    Exception e = new Exception("CreateMaxNo 插入失败");
                    throw e;
                } else {
                    num = 1;
                }
            } else {
                //如果已经有了,只要直接添加就可以
                num = Integer.parseInt(rsData);
                tSBql = "update ldmaxno set maxno = maxno + " + size +
                        " where notype = 'FINSERIALNO' and nolimit = 'SN'";
                exeSQL = new ExeSQL(conn);
                if (!exeSQL.execUpdateSQL(tSBql.toString())) {
                    System.out.println("CreateMaxNo 更新失败，请重试!");
                    conn.rollback();
                    conn.close();
                    Exception e = new Exception("CreateMaxNo 更新失败");
                    throw e;
                } else {
                    num = Integer.parseInt(rsData) + 1; //表示已经存在 并更新成功 则自动从下一个序列号开始增加
                }
            }
            conn.commit();
            conn.close();
        } catch (Exception ex) {
            try {
                conn.rollback();
                conn.close();
            } catch (Exception ex1) {
                System.out.println("流水号的错误!" + ex1.getMessage());
                throw ex1;
            }
            throw ex;
        }
        /****************************************************/
        String[] strArr = new String[size]; //返回数组的长度
        for (int i = 0; i < size; i++) {
            strArr[i] = getStringValue(num + i, 25);
        }
        System.out.println("流水号生成完毕!");
        return strArr;
    }

    /**在处理批次前自动生成系列号
     * 参数:size 长度
     */
    public static synchronized String[] getSerialNoByFINDATA(int size) throws
            Exception {

        int num = 0; //标记初始的数字
        String tSBql = "select maxno from ldmaxno where notype = 'FINDATA' and nolimit = 'SN' for update";
        Connection conn = DBConnPool.getConnection();
        try {
            conn.setAutoCommit(false);
            ExeSQL exeSQL = new ExeSQL(conn);
            String rsData = null;
            rsData = exeSQL.getOneValue(tSBql.toString());
            /***********************************************************************/
            /** 创建新的序列号 **/
            //如果没有创建的话则直接创建并且添加size个序号
            if ((rsData == null) || rsData.equals("")) {
                tSBql =
                        "insert into ldmaxno(notype, nolimit, maxno) values('FINDATA','SN'," +
                        size + ")";
                exeSQL = new ExeSQL(conn);
                if (!exeSQL.execUpdateSQL(tSBql.toString())) {
                    System.out.println("CreateMaxNo 插入失败，请重试!");
                    conn.rollback();
                    conn.close();
                    Exception e = new Exception("CreateMaxNo 插入失败");
                    throw e;
                } else {
                    num = 1;
                }
            } else {
                //如果已经有了,只要直接添加就可以
                num = Integer.parseInt(rsData);
                tSBql = "update ldmaxno set maxno = maxno + " + size +
                        " where notype = 'FINDATA' and nolimit = 'SN'";
                exeSQL = new ExeSQL(conn);
                if (!exeSQL.execUpdateSQL(tSBql.toString())) {
                    System.out.println("CreateMaxNo 更新失败，请重试!");
                    conn.rollback();
                    conn.close();
                    Exception e = new Exception("CreateMaxNo 更新失败");
                    throw e;
                } else {
                    num = Integer.parseInt(rsData) + 1; //表示已经存在 并更新成功 则自动从下一个序列号开始增加
                }
            }
            conn.commit();
            conn.close();
        } catch (Exception ex) {
            try {
                conn.rollback();
                conn.close();
            } catch (Exception ex1) {
                System.out.println("流水号的错误!" + ex1.getMessage());
                throw ex1;
            }
            throw ex;
        }
        /****************************************************/
        String[] strArr = new String[size]; //返回数组的长度
        for (int i = 0; i < size; i++) {
            strArr[i] = getStringValue(num + i, 25);
        }
        System.out.println("流水号生成完毕!");
        return strArr;
    }

    /**
     * 把相关的数字转换成相应长度的流水号
     * @param n 需要转换的数字,size则是转换后的长度
     * @return String
     */
    public static String getStringValue(int n, int size) {

        String str = Integer.toString(n);
        int len = str.length();
        String temp = "0000000000000000000000000000";
        str = temp.substring(0, size - len) + str; //未满的用0补充

        return str;
    }

}
