package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GrpConPayListPrintUI {

    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private String mOperate = "";

    public GrpConPayListPrintUI() {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

//        this.mOperate = cOperate;
        GrpConPayListPrintBL tGrpConPayListPrintBL = new GrpConPayListPrintBL();

        if (!tGrpConPayListPrintBL.submitData(cInputData, cOperate)) {
            this.mErrors.copyAllErrors(tGrpConPayListPrintBL.mErrors);
            this.mResult.clear();
            return false;
        } else {
            this.mResult = tGrpConPayListPrintBL.getResult();
        }
        return true;
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }

    public static void main(String[] args) {

    }
}
