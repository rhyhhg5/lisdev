package com.sinosoft.lis.operfee;

/**
 * <p>Title: lis</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class FeeConst
{
    public FeeConst()
    {
    }

    /**待收费*/
    public static final String DEALSTATE_WAITCHARGE = "0";
    /**催收成功*/
    public static final String DEALSTATE_URGESUCCEED = "1";
    /**应收作废*/
    public static final String DEALSTATE_CANCEL = "2";
    /**撤销*/
    public static final String DEALSTATE_REPEAL = "3";
    /**待核销*/
    public static final String DEALSTATE_WAITCONFIRM = "4";
    /**正转出 */
    public static final String DEALSTATE_BACK = "5";
    /**转出成功 */
    public static final String DEALSTATE_BACK_SUCC = "6";

    /**通知书号标示*/
    public static final String GETNOTICENO = "GETNOTICENO";

    /**电话催缴结果,重设缴费事项*/
    public static final String HASTEN_FINISHTYPE_REDATE = "1";
    /**电话催缴结果,重设缴费事项*/
    public static final String HASTEN_FINISHTYPE_CANCEL = "0";

    /**开始时间*/
    public static final String STARTDATE = "StartDate";
    /**结束时间*/
    public static final String ENDDATE = "EndDate";
    /**开始时间*/
    public static final String MANAGECOM = "ManageCom";
    /**开始时间*/
    public static final String AgentCode = "AgentCode";
    /**开始时间*/
    public static final String GROUP01 = "Group01";
    /**开始时间*/
    public static final String GROUP02 = "Group02";
    /**开始时间*/
    public static final String GROUP03 = "Group03";
}
