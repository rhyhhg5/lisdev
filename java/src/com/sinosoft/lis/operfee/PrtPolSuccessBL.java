package com.sinosoft.lis.operfee;

import java.util.*;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.task.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 续期续保业务报表1-达成率分析(保单)
 *
 * 阅读说明，本报表的Sql比较复杂，阅读时请按各uinion项单独阅读，各union项属于不同的业务，
 * 均有相应的注释，uinon之后再对各项求和
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class PrtPolSuccessBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private static String COM86 = "全系统";
    private static int COL_NUM = 23;

    private GlobalInput mGI = null; //操作员信息
    private TransferData mTransferData = null;

    private String mStartDate = null;
    private String mEndDate = null;
    private String mManageCom = null;

    HashMap mHashMap = new HashMap(); //存储清单数据
    HashSet mHashSet = new HashSet();  //存储涉及的机构

    private ExeSQL mExeSQL = new ExeSQL();

    private XmlExport xmlexport = new XmlExport();

    public PrtPolSuccessBL()
    {
    }

    /**
     * 操作的提交方法，得到打印清单数据。
     * @param sql String
     * @return XmlExport
     */
    public XmlExport getXmlExport(VData cInputData, String operate)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return xmlexport;
    }

    /**
     * createXmlExport
     * 根据dealData中得到的数据生成清单
     * @return boolean
     */
    private boolean createXmlExport()
    {
        ListTable tListTable = new ListTable();

        SSRS tSSRS = getPolData(true);
        if(tSSRS == null)
        {
            return false;
        }

        if(tSSRS.getMaxRow() == 0)
        {
            return true;
        }

        String riskType = null;  //险种类别
        String comCode = null;  //管理机构
        String[] sumInfo = null;
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            //险种类型改变，则需要按险种类型显示合计
            if(riskType != null && !riskType.equals(tSSRS.GetText(i, 3))
                || comCode != null && !comCode.equals(tSSRS.GetText(i, 1)))
            {
                String[] sum = getOneRow(sumInfo);
                tListTable.add(sum);
                sumInfo = new String[COL_NUM];
            }

            String[] info = getOneRow(tSSRS.getRowData(i));
            tListTable.add(info);

            sumInfo = CommonBL.add(sumInfo, tSSRS.getRowData(i), 5);
            sumInfo[0] = tSSRS.GetText(i, 1);
            sumInfo[1] = tSSRS.GetText(i, 2);
            sumInfo[2] = tSSRS.GetText(i, 3);
            sumInfo[3] = "小计";
            sumInfo[4] = "";

            riskType = tSSRS.GetText(i, 3);
            comCode = tSSRS.GetText(i , 1);
        }
        String[] lastSum = getOneRow(sumInfo);
        tListTable.add(lastSum);


        //全系统求和
        SSRS tSSRSSum = getPolData(false);
        if(tSSRSSum == null)
        {
            return false;
        }

        if(tSSRSSum.getMaxRow() == 0)
        {
            return true;
        }

        riskType = null;
        String[] sumInfoSum = null;
        for(int i = 1; i <= tSSRSSum.getMaxRow(); i++)
        {
            //险种类型改变，则需要按险种类型显示合计
            if(riskType != null && !riskType.equals(tSSRSSum.GetText(i, 3)))
            {
                String[] sum = getOneRow(sumInfoSum);
                sum[0] = COM86;
                tListTable.add(sum);
                sumInfoSum = new String[COL_NUM];
            }

            String[] info = getOneRow(tSSRSSum.getRowData(i));
            info[0] = COM86;
            tListTable.add(info);

            sumInfoSum = CommonBL.add(sumInfoSum, tSSRSSum.getRowData(i), 5);
            sumInfoSum[0] = tSSRSSum.GetText(i, 1);
            sumInfoSum[1] = tSSRSSum.GetText(i, 2);
            sumInfoSum[2] = tSSRSSum.GetText(i, 3);
            sumInfoSum[3] = "小计";
            sumInfoSum[4] = "";

            riskType = tSSRSSum.GetText(i, 3);
        }
        String[] lastSumSum = getOneRow(sumInfoSum);
        lastSumSum[0] = COM86;
        tListTable.add(lastSumSum);



        tListTable.setName("List");

        String[] colNames = {"机构","业务类型",
                            "应收件数","应收保费",
                            "应收成功件数","应收成功保费",
                            "收费成功率(%)件数","收费成功率(%)保费",
                            "平均收费时效",
                            "待收费件数","待收费保费",
                            "未收失效件数","未收失效保费",
                            "个险转账成功率件数","个险转账成功率保费",
                            "个险自缴成功率件数","个险自缴成功率保费", "处理时长"};
        xmlexport.addListTable(tListTable, colNames);

        return true;
    }

    /**
     * getOneRow
     * 得到报表一行数据
     * @param strings String[]
     * @return String[]
     */
    private String[] getOneRow(String[] row)
    {
        String[] info = new String[COL_NUM];

        info[0] = GetMemberInfo.getComNameByComCode(row[0]);
        info[1] = ("I".equals(row[1]) ? "个险" : "团险");
        if("B".equals(row[2]))
        {
            info[2] = "保证续保险种";
        }
        else
        {
            info[2] = ("C".equals(row[2]) ? "非保证续保险种" : "长期险");
        }
        if(!"I".equals(row[1]))
        {
            info[2] = "";
        }

        for(int i = 3; i < row.length; i++)
        {
            info[i] = row[i];
        }

        info[7] = getRate(row[6], row[5], 100);
        info[10] = getRate(row[9], row[8], 100);
        //首年内
        info[11] = getRate(row[11], row[5], 100);
        info[12] = getRate(row[12], row[8], 100);
        info[13] = getRate(row[13], row[6], 100);
        info[14] = getRate(row[14], row[9], 100);
        //两年内
        info[15] = getRate(row[15], row[5], 100);
        info[16] = getRate(row[16], row[8], 100);
        info[17] = getRate(row[17], row[6], 100);
        info[18] = getRate(row[18], row[9], 100);
        //三年以
        info[19] = getRate(row[19], row[5], 100);
        info[20] = getRate(row[20], row[8], 100);
        info[21] = getRate(row[21], row[6], 100);
        info[22] = getRate(row[22], row[9], 100);

        return info;
    }

    /**
     * getRate
     * 计算sub/sum * n
     * @param string String
     * @param string1 String
     * @param n 倍数
     * @return String
     */
    private String getRate(String sub, String sum, double n)
    {
        if(sum == null || sum.equals("") || sub == null || sum.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "PrtOperfeePayModBL";
            tError.functionName = "getRate";
            tError.errorMessage = "求比例时分子分母均不能为空";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        if("0".equals(sum))
        {
            return "0.00";
        }

        try
        {
            double down = Double.parseDouble(sum);
            double up = Double.parseDouble(sub);
            return CommonBL.bigDoubleToCommonString(up / down * n, "0.00");
        }
        catch(NumberFormatException ex)
        {
            CError tError = new CError();
            tError.moduleName = "PrtOperfeePayModBL";
            tError.functionName = "getRate";
            tError.errorMessage = "求比例时分子分母均应为数字";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
    }

    /**
     * RiskType：B保证续保；C可续保，L长期险
     * @param groupFlag boolean：是否需要分组的标记，true：需要分组
     * @return SSRS
     */
    private SSRS getPolData(boolean groupFlag)
    {
        String manageCom = groupFlag ? "ManageCom ,  " : "'86' , ";

        String sql = "select " + manageCom + "  RiskProp, RiskType, RiskCode, RiskName,sum(q) "
                     +",sum(w),sum(e),sum(r),sum(g),sum(t),sum(y),sum(u),sum(i),sum(o), "
                     +" sum(p),sum(a),sum(s),sum(d),sum(f),sum(h),sum(j),sum(k) "
                     +"from ( "
                     +"select " + manageCom + "  RiskProp, RiskType, RiskCode, RiskName, sum(c) + sum(d) q, sum(d) w, sum(e) e, sum(f) + sum(g) r, sum(g) g, 0 t, "
                     //一年内
                     + "   case h when 1 then sum(c) + sum(d) else 0 end y, "
                     + "   case h when 1 then sum(f) + sum(g) else 0 end u, "
                     + "   case h when 1 then sum(d) else 0 end i, "
                     + "   case h when 1 then sum(g) else 0 end o, "
                     //一年到两年内
                     + "   case h when 2 then sum(c) + sum(d) else 0 end p, "
                     + "   case h when 2 then sum(f) + sum(g) else 0 end a, "
                     + "   case h when 2 then sum(d) else 0 end s, "
                     + "   case h when 2 then sum(g) else 0 end d, "
                     //三年以上
                     + "   case h when 3 then sum(c) + sum(d) else 0 end f, "
                     + "   case h when 3 then sum(f) + sum(g) else 0 end h, "
                     + "   case h when 3 then sum(d) else 0 end j, "
                     + "   case h when 3 then sum(g) else 0 end k "
                     + " from( "
                     //一年期险未核销
                     + "   select a.ManageCom ManageCom, 'I' RiskProp, case b.RnewFlag when 'B' then 'B' else 'C' end RiskType, a.RiskCode RiskCode, c.RiskName, "
                     + "		count(distinct PolNo) c, 0 d,0 e, sum(Prem) f,0 g, "
                     + "		case year(date(PayToDate) - 1 days - date((select min(LastPayToDate) from LJAPayPerson where PolNo = a.PolNo))) + 1 "
                     + "	  		when 1 then 1 "
                     + "	  		when 2 then 2 "
                     + "	  		else 3 "
                     + "		end h "  //应收时间与保单生效日之差
                     + "	from LCPol a, LMRisk b, LMRiskApp c "
                     + "	where a.RiskCode = b.RiskCode "
                     + "		and b.RiskCode = c.RiskCode "
                     + "                and c.RiskType != 'M' " //剔除健管险种
                     + "		and c.RiskProp = 'I' "  //个险
                     + "		and a.ContType = '1' "  //个险
                     + "		and c.RiskPeriod != 'L' "  //非长险
                     + groupPart("a")
                     + agentPart("a")
                     + "		and a.AppFlag = '1' "
                     	//有效
                     + "		and (a.StateFlag is null or a.StateFlag not in( '0', '3')) "  //未终止
                     + "		and a.ManageCom like '" + mManageCom + "%' "
                     + "		and PayToDate between '" + mStartDate + "' and '" + mEndDate + "' "  //交至起止
                       //可续期
                     + "		and (PayToDate < a.EndDate and PayIntv > 0 "
                     + "					or "
                      //或可续保
                     + "				b.RnewFlag != 'N' and c.RiskType5 = '2' "
                     + "			 ) "
                     + "	group by a.ManageCom, b.RnewFlag, a.RiskCode, c.RiskName, a.PolNo, a.PayToDate "
                     + "	union all "
                     //未生成实收长险种
                     + "	select a.ManageCom ManageCom, c.RiskProp RiskProp, c.RiskPeriod RiskType, b.RiskCode RiskCode, b.RiskName, "
                     + "		 count(distinct PolNo) c, 0 d,0 e, sum(Prem) f,0 g, "
                     + "		case year(date(PayToDate) - 1 days - date((select min(LastPayToDate) from LJAPayPerson where PolNo = a.PolNo))) + 1 "
                     + "	  		when 1 then 1 "
                     + "	  		when 2 then 2 "
                     + "	  		else 3 "
                     + "		end h "  //应收时间与保单生效日之差
                     + "	from LCPol a, LMRisk b, LMRiskApp c "
                     + "	where a.RiskCode = b.RiskCode "
                     + "		and b.RiskCode = c.RiskCode "
                     + "                and c.RiskType != 'M' " //剔除健管险种
                     + "		and c.RiskProp = 'I' "
                     + "		and a.ContType = '1' "
                     + "		and c.RiskPeriod = 'L' "  //长期险
                     + "		and a.AppFlag = '1' "
                     + groupPart("a")
                     + agentPart("a")
                     + "		and (a.StateFlag is null or a.StateFlag not in( '0', '3')) "  //未终止
                     + "		and a.ManageCom like '" + mManageCom + "%' "
                     + "		and PayToDate between '" + mStartDate + "' and '" + mEndDate + "' "  //取缴费截至日期
                     + "		and PayToDate < a.EndDate "
                     + "		and PayIntv > 0 "
                     + "	group by a.ManageCom, c.RiskProp, c.RiskPeriod, b.RiskCode, b.RiskName, a.PolNo, a.PayToDate "
                     + "	union all "
                     //未生成实收团险
                     + "	select ManageCom, 'G' RiskProp, '' RiskType, a.RiskCode RiskCode, b.RiskName, "
                     + "		count(distinct GrpPolNo) c,0 d,0 e, sum(Prem) f,0 g, "
                     + "		case year(date(PayToDate) - 1 days - date((select min(LastPayToDate) from LJAPayGrp where GrpPolNo = a.GrpPolNo))) + 1 "
                     + "	  		when 1 then 1 "
                     + "	  		when 2 then 2 "
                     + "	  		else 3 "
                     + "		end h "  //应收时间与保单生效日之差
                     + "	from LCGrpPol a, LMRisk b "
                     + "	where a.RiskCode = b.RiskCode "
                     + "              and a.riskcode not in (select riskcode from lmriskapp where risktype ='M') "
                     + "		and PayToDate < PayEndDate "  //可续期
                     + "		and PayIntv>0 "  //可续期
                     + "		and a.AppFlag = '1' "
                     + "		and (a.StateFlag is null or a.StateFlag not in( '0', '3')) " //未终止
                     + "		and a.ManageCom like '" + mManageCom + "%' "
                     + "		and PayToDate between '" + mStartDate + "' and '" + mEndDate + "' "  //应收日期
                     + "	group by ManageCom, a.RiskCode, b.RiskName, a.GrpPolNo, a.PayToDate "
                     + "	union all "
                     //实收非长险
                     + "	select a ManageCom, b RiskProp, RiskType , RiskCode, RiskName, 0 c,count(GetNoticeNo) d, 0 e,0 f,sum(Money) g, h "
                     + "	from( "
                     + "    select a.ManageCom a , c.RiskProp b , case b.RnewFlag when 'B' then 'B' else 'C' end RiskType, "
                     + "  		GetNoticeNo, sum(SumActuPayMoney) Money, a.PolNo PolNo, b.RiskCode RiskCode, b.RiskName, "
                     + "  		case year(date(LastPayToDate) - 1 days - date((select min(LastPayToDate) from LJAPayPerson where PolNo = a.PolNo))) + 1 "
                     + "	  		when 1 then 1 "
                     + "	  		when 2 then 2 "
                     + "	  		else 3 "
                     + "		end h "  //应收时间与保单生效日之差
                     + "  	from LJAPayPerson a, LMRisk b, LMRiskApp c "
                     + "  	where a.RiskCode = b.RiskCode "
                     + "                and c.RiskType != 'M' " //剔除健管险种
                     + "  		and b.RiskCode = c.RiskCode "
                     + "  		and c.RiskProp = 'I' "  //个险
                     + "  		and c.RiskPeriod != 'L' "  //非长险
                     + "        and a.PayType = 'ZC' "
                     + groupPart("a")
                     + agentPart("a")
                     + "  		and a.ManageCom like '" + mManageCom + "%' "
                     + "  		and LastPayToDate between '" + mStartDate + "' and '" + mEndDate + "' "  //交至日期
                     + "  		and exists (select 1 from LJSPayB where GetNoticeNo= a.GetNoticeNo and DealState = '1' and OtherNoType = '2') "  //个单核销
                     + "  	group by a.ManageCom, a.GetNoticeNo, c.RiskProp, b.RnewFlag, a.PolNo, b.RiskCode, b.RiskName,LastPayToDate "
                     + "	) t "
                     + "	group by a, b, RiskType, RiskCode, RiskName, h "
                     + "	union all "
                     //生成实收长险种
                     + "	select a ManageCom, b RiskProp, RiskType, RiskCode, RiskName, 0 c, count(GetNoticeNo) d, 0 e,0 f,sum(Money) g, h "
                     + "	from( "
                     + "		select a.ManageCom a, c.RiskProp b, c.RiskPeriod RiskType, "
                     + "		   GetNoticeNo, sum(SumActuPayMoney) Money, a.PolNo, b.RiskCode RiskCode, b.RiskName RiskName, "
                     + "		   case year(date(a.LastPayToDate) - 1 days - date((select min(LastPayToDate) from LJAPayPerson where PolNo = a.PolNo))) + 1 "
                     + "	  		  when 1 then 1 "
                     + "	  		  when 2 then 2 "
                     + "	  		  else 3 "
                     + "		   end h "  //应收时间与保单生效日之差
                     + "		from LJAPayPerson a, LMRisk b, LMRiskApp c "
                     + "		where a.RiskCode = b.RiskCode "
                     + "			and b.RiskCode = c.RiskCode "
                     + "                and c.RiskType != 'M' " //剔除健管险种
                     + "			and c.RiskProp = 'I' "
                     + "			and c.RiskPeriod = 'L'  "  //长险
                     + "            and a.PayType = 'ZC' "
                     + groupPart("a")
                     + agentPart("a")
                     + "			and a.ManageCom like '" + mManageCom + "%' "
                     + "			and LastPayToDate between '" + mStartDate + "' and '" + mEndDate + "' "
                     + "			and exists (select 1 from LJSPayB where GetNoticeNo= a.GetNoticeNo and OtherNoType = '2' and DealState = '1') "  //个单核销
                     + "		group by a.ManageCom, c.RiskProp, GetNoticeNo, c.RiskPeriod, PolNo, b.RiskCode, b.RiskName, a.LastPayToDate "
                     + "	) t "
                     + "	group by a, b, RiskType, RiskCode, RiskName, h "
                     + "	union all "
                     //团单实收成功
                     + "	select ManageCom, 'G' RiskProp, '' RiskType, a.RiskCode RiskCode, b.RiskName, 0 c, count(distinct GetNoticeNo) d, 0 e,0 f, "
                     + "		sum(SumActuPayMoney) g, "
                     + "		case year(date(a.LastPayToDate) - 1 days - date((select min(LastPayToDate) from LJAPayGrp where GrpPolNo = a.GrpPolNo))) + 1 "
                     + "	  		when 1 then 1 "
                     + "	  		when 2 then 2 "
                     + "	  		else 3 "
                     + "		end h "  //应收时间与保单生效日之差
                     + "	from LJAPayGrp a, LMRisk b "
                     + "	where a.RiskCode = b.RiskCode "
                     + "              and a.riskcode not in (select riskcode from lmriskapp where risktype ='M') "
                     + "		and exists (select 1 from LJSPayB where OtherNo = a.GrpContNo "
                     + "							and OtherNoType = '1' and DealState = '1') "  //团单续期
                     + "		and a.PayType = 'ZC' "
                     + groupPart("a")
                     + agentPart("a")
                     + "		and a.ManageCom like '" + mManageCom + "%' "
                     + "		and LastPayToDate between '" + mStartDate + "' and '" + mEndDate + "' "  //应收日期
                     + "	group by ManageCom,a.RiskCode, b.RiskName, a.LastPayToDate, a.GrpPolNo"
                     + ") t "
                     + " group by " + manageCom + " RiskProp, RiskType, RiskCode, RiskName, h "
                     +") t "
                     +" group by " + manageCom + "  RiskProp, RiskType, RiskCode, RiskName "
                     + " order by " + manageCom + " RiskProp desc, RiskType, RiskCode "
                     + " with ur "
                     ;
        System.out.println(sql);
        SSRS tSSRS = mExeSQL.execSQL(sql);
        if(mExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "PrtPolSuccessBL";
            tError.functionName = "getPolData";
            tError.errorMessage = "";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        return tSSRS;
    }

    private boolean checkData()
    {
        mStartDate = (String) mTransferData
                           .getValueByName(FeeConst.STARTDATE);  //开始日期
        mEndDate = (String) mTransferData.getValueByName(FeeConst.ENDDATE);  //截至日期
        mManageCom = (String) mTransferData
                           .getValueByName(FeeConst.MANAGECOM);  //管理机构

        if(mStartDate == null || mStartDate.equals("")
           || mEndDate == null || mEndDate.equals("")
            || mManageCom == null || mManageCom.equals(""))
       {
           CError tError = new CError();
           tError.moduleName = "PrtContSuccessBL";
           tError.functionName = "dealData";
           tError.errorMessage = "应收起止日期个和管理机构均不能为空";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return false;
       }

        return true;
    }

    /**
     * 得到录入的数据
     * @param data VData：包括TransferData对象和GlobalInput对象
     * @return boolean: 成功true，否则false
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        mTransferData = (TransferData) data
                        .getObjectByObjectName("TransferData", 0);

        if(mGI == null || mGI.Operator == null || mGI.ManageCom == null
            || mTransferData == null)
        {
            CError tError = new CError();
            tError.moduleName = "PrtContSuccessBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * 处理业务逻辑，生成打印数据XmlExport。
     * @return boolean:操作成功true，否则false
     */
    private boolean dealData()
    {
        xmlexport.createDocument("PrtPolSuccess.vts", "printer");

        if(!dealHead())
        {
            return false;
        }

        if(!createXmlExport())
        {
            return false;
        }

        xmlexport.outputDocumentToFile("D:\\", "PrtPolSuccess");

        return true;
    }

    /**
     * dealHead
     *生成抬头信息
     * @return boolean
     */
    private boolean dealHead()
    {
        TextTag tag = new TextTag();
        tag.add("ManageCom", mGI.ManageCom);
        tag.add("ManageComName", GetMemberInfo.getComName(mGI.Operator));
        tag.add("Operator", mGI.Operator);
        tag.add("OperatorName", GetMemberInfo.getMemberName(mGI));
        tag.add("PrintDate", PubFun.getCurrentDate());
        tag.add("StartDate", StrTool.cTrim(this.mStartDate));
        tag.add("EndDate", StrTool.cTrim(this.mEndDate));

        String com = StrTool.cTrim(this.mManageCom);
        tag.add("ManageComSelect", com);
        tag.add("ManageComNameSelect",
                com.equals("") ? "" : GetMemberInfo.getComNameByComCode(com));

        String group03 = StrTool.cTrim((String) mTransferData
                                       .getValueByName(FeeConst.GROUP03));
        String group02 = StrTool.cTrim((String) mTransferData
                                       .getValueByName(FeeConst.GROUP02));
        String group01 = StrTool.cTrim((String) mTransferData
                                       .getValueByName(FeeConst.GROUP01));
        tag.add("Group01", group03);
        tag.add("Group01Name", getGroupName(group03));
        tag.add("Group02", group02);
        tag.add("Group02Name", getGroupName(group02));
        tag.add("Group03", group01);
        tag.add("Group03Name", getGroupName(group01));

        String agentCode = (String) mTransferData
                           .getValueByName(FeeConst.AgentCode); //销售人员代码
        if(agentCode != null && !agentCode.equals(""))
        {
            tag.add("Agent", agentCode);
            tag.add("AgentName", getAgentName(agentCode));
        }

        xmlexport.addTextTag(tag);

        return true;
    }

    /**
     * getAgentName
     *
     * @param agentCode String
     * @return String
     */
    private String getAgentName(String agentCode)
    {
        String sql = "select Name from LAAgent "
                     + "where AgentCode = getAgentCode('" + agentCode + "') ";
        return new ExeSQL().getOneValue(sql);
    }

    /**
     * 得到agentGroup对应名字
     * @param agentGroup String
     * @return String
     */
    private String getGroupName(String agentGroup)
    {
        if("".equals(StrTool.cTrim(agentGroup)))
        {
            return "";
        }

        String sql = "select Name "
                     + "from LABranchGroup "
                     + "where AgentGroup = '" + agentGroup + "' ";
        return new ExeSQL().getOneValue(sql);
    }

    /**
     * 由于部、区、处的隶属关系，只需要的到最小级别的团体即可
     * @return String
     */
    private String groupPart(String table)
    {
        if(table == null )
        {
            table = "";
        }
        else
        {
            table = table + ".";
        }

        String group01 = (String) mTransferData.getValueByName(FeeConst.GROUP01); //部
        String group02 = (String) mTransferData.getValueByName(FeeConst.GROUP02); //区
        String group03 = (String) mTransferData.getValueByName(FeeConst.GROUP03); //处

        String group = null;
        if(group03 != null && !group03.equals(""))
        {
            group = group03;
        }
        else if(group02 != null && !group02.equals(""))
        {
            group = group02;
        }
        else if(group01 != null && !group01.equals(""))
        {
            group = group01;
        }

        if(group == null)
        {
            return " ";
        }

        return "   and " + table + "agentcode in  (select a.agentcode from laagent a  where agentgroup in ( select agentgroup from labranchgroup    "
                     +" where  BranchSeries like '" + group + "%') )";
    }

    /**
     * 业务员
     * @return String
     */
    private String agentPart(String table)
    {
        if(table == null)
        {
            table = " ";
        }
        else
        {
            table = table + ".";
        }

        String agentCode = (String) mTransferData
                           .getValueByName(FeeConst.AgentCode); //销售人员代码
        if(agentCode != null && !agentCode.equals(""))
        {
            return "   and " + table + "AgentCode = getAgentCode('" + agentCode + "') ";
        }
        return " ";
    }

    public static void main(String[] args)
    {
        GlobalInput mGlobalInput = new GlobalInput(); //操作员信息
        mGlobalInput.Operator = "pa0001";
        mGlobalInput.ManageCom = "86110000";
        mGlobalInput.ComCode = mGlobalInput.ManageCom;

        TransferData t = new TransferData();
        t.setNameAndValue(FeeConst.STARTDATE, "2007-01-01");
        t.setNameAndValue(FeeConst.ENDDATE, "2007-01-31");
        t.setNameAndValue(FeeConst.MANAGECOM, "86");
        t.setNameAndValue(FeeConst.GROUP03, "");
        t.setNameAndValue(FeeConst.GROUP02, "");
        t.setNameAndValue(FeeConst.GROUP01, "");


        VData d = new VData();
        d.add(mGlobalInput);
        d.add(t);

        PrtPolSuccessBL bl = new PrtPolSuccessBL();
        if(bl.getXmlExport(d, "") == null)
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
