package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class DuePayFeeGrpBL  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 数据操作字符串 */
  private String mOperate;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();

  private LJSPayGrpSet       mLJSPayGrpSet        = new LJSPayGrpSet();
  private LJSPayGrpSet       mLJSPayGrpSetNew     = new LJSPayGrpSet();
  private LJSPayPersonSet    mLJSPayPersonSet     = new LJSPayPersonSet();
  private LJSPayPersonSet    mLJSPayPersonSetNew  = new LJSPayPersonSet();
  private LJSPaySet          mLJSPaySet            = new LJSPaySet();
  private LJSPaySet          mLJSPaySetNew            = new LJSPaySet();
  private LOPRTManagerSet    mLOPRTManagerSet  = new LOPRTManagerSet();
  //业务处理相关变量

  public DuePayFeeGrpBL() {
  }
  public static void main(String[] args) {
    DuePayFeeGrpBL DuePayFeeGrpBL1 = new DuePayFeeGrpBL();
    LJSPayPersonBL mLJSPayPersonBL =new LJSPayPersonBL();
    VData tv=new VData();
    tv.add(mLJSPayPersonBL);
    DuePayFeeGrpBL1.submitData(tv,"INSERT");
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
System.out.println("OperateData:  "+cOperate);


    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
System.out.println("After getinputdata");

    //进行业务处理
    if (!dealData())
      return false;
System.out.println("After dealData！");
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
System.out.println("After prepareOutputData");

    System.out.println("Start LJSPayPerson BL Submit...");

    DuePayFeeGrpBLS tDuePayFeeGrpBLS=new DuePayFeeGrpBLS();
    tDuePayFeeGrpBLS.submitData(mInputData,cOperate);

    System.out.println("End LJSPayPerson BL Submit...");

    //如果有需要处理的错误，则返回
    if (tDuePayFeeGrpBLS.mErrors .needDealError())
    {
        this.mErrors .copyAllErrors(tDuePayFeeGrpBLS.mErrors ) ;
    }

    mInputData=null;
    return true;
  }

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData()
  {
    int i,iMax;
    boolean tReturn =false;
    String tNo="";
    //处理数据
    if(this.mOperate.equals("INSERT"))
    {
//1-处理应收个人交费表，记录集，循环处理
    LJSPayPersonBL  tLJSPayPersonBL;
    iMax=mLJSPayPersonSet.size() ;
    for (i=1;i<=iMax;i++)
    {
      tLJSPayPersonBL = new LJSPayPersonBL() ;
      tLJSPayPersonBL.setSchema(mLJSPayPersonSet.get(i));
      tLJSPayPersonBL.setMakeDate(CurrentDate);//入机日期
      tLJSPayPersonBL.setMakeTime(CurrentTime);//入机时间
      tLJSPayPersonBL.setModifyDate(CurrentDate);//最后一次修改日期
      tLJSPayPersonBL.setModifyTime(CurrentTime);//最后一次修改时间
      mLJSPayPersonSetNew.add(tLJSPayPersonBL);
      tReturn=true;
    }

//2-应收集体交费表，单项纪录
    LJSPayGrpBL mLJSPayGrpBL = new LJSPayGrpBL();
    for(int n=1;n<=mLJSPayGrpSet.size();n++)
    {
     mLJSPayGrpBL = new LJSPayGrpBL();
     mLJSPayGrpBL.setSchema(mLJSPayGrpSet.get(n));
     mLJSPayGrpBL.setMakeDate(CurrentDate);
     mLJSPayGrpBL.setMakeTime(CurrentTime);
     mLJSPayGrpBL.setModifyDate(CurrentDate);
     mLJSPayGrpBL.setModifyTime(CurrentTime);
     mLJSPayGrpSetNew.add(mLJSPayGrpBL);
    }
    tReturn=true;

//3-应收总表，单项纪录
    LJSPayBL mLJSPayBL=new LJSPayBL();
    for(int m=1;m<=mLJSPaySet.size();m++)
    {
      mLJSPayBL=new LJSPayBL();
      mLJSPayBL.setSchema(mLJSPaySet.get(m));
      mLJSPayBL.setMakeDate(CurrentDate);
      mLJSPayBL.setMakeTime(CurrentTime);
      mLJSPayBL.setModifyDate(CurrentDate);
      mLJSPayBL.setModifyTime(CurrentTime);
      mLJSPaySetNew.add(mLJSPayBL);
    }
     tReturn = true;

}

    return tReturn ;
  }

  //从输入数据中得到所有对象
  //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData mInputData)
  {
    //应收总表
    mLJSPaySet.set((LJSPaySet)mInputData.getObjectByObjectName("LJSPaySet",0));
    // 应收个人交费表
    mLJSPayPersonSet.set((LJSPayPersonSet)mInputData.getObjectByObjectName("LJSPayPersonSet",0));
    LJSPayPersonBL tLJSPayPersonBL = new LJSPayPersonBL() ;
    tLJSPayPersonBL.setSchema(mLJSPayPersonSet.get(1));
    // 应收集体交费表
    mLJSPayGrpSet.set((LJSPayGrpSet)mInputData.getObjectByObjectName("LJSPayGrpSet",0));

    mLOPRTManagerSet=(LOPRTManagerSet)mInputData.getObjectByObjectName("LOPRTManagerSet",0);
    if(mLJSPaySet==null || mLJSPayPersonSet ==null ||mLJSPayGrpSet ==null )
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="DuePayFeeGrpBL";
      tError.functionName="getInputData";
      tError.errorMessage="没有得到足够的数据，请您确认!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData()
  {
    mInputData=new VData();
    try
    {
//1-处理应收个人交费表，记录集，循环处理
      mInputData.add(mLJSPayPersonSetNew);
//2-应收集体交费表
      mInputData.add(mLJSPayGrpSetNew);
//3-应收总表，单项纪录
      mInputData.add(mLJSPaySetNew);
      mInputData.add(mLOPRTManagerSet);

      System.out.println("prepareOutputData:");
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="DuePayFeeGrpBL";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }
}

