package com.sinosoft.lis.operfee;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GrpExpirBenefitBatchUI {

    //业务处理相关变量
    private VData mInputData;
    public CErrors mErrors = new CErrors();

    public GrpExpirBenefitBatchUI() {
    }

    /**
     * 用户接口类，接收页面传入的数据，传入后台进行批量抽档业务逻辑的处理
     * @param cInputData VData:包括
     * 1、	GlobalInput对象，完整的登陆用户信息
     * 2、	TransferData对象，包含StartDate， EndDate， ManageCom
     * @param cOperate String：操作类型，此可为空字符串“”
     * @return boolean，成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        GrpExpirBenefitBatchBL tGrpExpirBenefitBatchBL = new GrpExpirBenefitBatchBL();
        System.out.println("Start ExpirBenefitBatch UI Submit...");
        tGrpExpirBenefitBatchBL.submitData(mInputData, cOperate);

        System.out.println("End ExpirBenefitBatch UI Submit...");

        mInputData = null;
        mInputData = tGrpExpirBenefitBatchBL.getResult();
        //如果有需要处理的错误，则返回
        if (tGrpExpirBenefitBatchBL.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tGrpExpirBenefitBatchBL.mErrors);
            return false;
        }
        System.out.println("error num=" + mErrors.getErrorCount());
        return true;
    }

    public VData getResult() {
        return mInputData;
    }
}
