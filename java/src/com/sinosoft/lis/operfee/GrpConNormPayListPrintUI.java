package com.sinosoft.lis.operfee;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

public class GrpConNormPayListPrintUI {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mCErrors = new CErrors();
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate = "";

    public GrpConNormPayListPrintUI() {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        GrpConNormPayListPrintBL tGrpConNormPayListPrintBL = new
                GrpConNormPayListPrintBL();

        if (!tGrpConNormPayListPrintBL.submitData(cInputData, cOperate)) {

            this.mCErrors.copyAllErrors(tGrpConNormPayListPrintBL.mCErrors);
            this.mResult.clear();
            return false;
        } else {
            this.mResult = tGrpConNormPayListPrintBL.getResult();
        }

        return true;
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult() {

        return mResult;
    }

    public static void main(String[] args) {

    }
}
