package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.xb.PRnewDueFeeBL;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class IndiDueFeeMultiForChildBL {
    public IndiDueFeeMultiForChildBL() {
    }

    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private GlobalInput tGI = new GlobalInput();

    //   private VData saveData = new VData();
    //应收总表交费日期
    //  private String  strNewLJSPayDate;
    //应收总表最早交费日期
    //   private String  strNewLJSStartDate;

    /** 数据操作字符串 */
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String StartDate = ""; //应收开始时间
    private String EndDate = ""; //应收结束时间
    private String mserNo = ""; //批次号

    private SSRS mSSRS = new SSRS();

    private TransferData mTransferData = new TransferData();

    /**
     * @param cInputData VData，包含：
     * 1、	GlobalInput对象，完整的登陆用户信息
     * 2、	TransferData对象，包含StartDate， EndDate， ManageCom
     * @param cOperate String：操作类型，此可为空字符串“”
     * @return boolean：操作成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.mOperate = cOperate;
        //接收传入数据
        if (!getInputData(cInputData)) {
            return false;
        }
        //处理数据
        if (!dealData()) {
            return false;
        }

        return true;
    }

    private boolean getInputData(VData mInputData)
    {
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));

        mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData",0);
        System.out.println("mTransferData:" + mTransferData);

        if (tGI == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "IndiDueFeeMultiBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    private boolean dealData()
    {
        StartDate = (String) mTransferData.getValueByName("StartDate");
        EndDate = (String) mTransferData.getValueByName("EndDate");
        String manageCom = (String) mTransferData.getValueByName("ManageCom");
        String querySql = "";

        querySql= " select ContNo "
                  +"   from LCCont "
                  +"  where AppFlag = '1' "
                  +"    and ContType = '1' "
                  +"    and (StateFlag is null or StateFlag = '1') "
                  +"    and exists "
                  +"  (select 'X' "
                  +"           from  LCPol "
                  +"          where contno = lccont.contno "
                  +"            and lcpol.riskcode in ('320106', '120706') "
                  +"            and (PaytoDate >= '"+StartDate+"' and PaytoDate <= '"+EndDate+"' and "
                  +"                (polstate is null or "
                  +"                (polstate is not null and polstate not like '02%%' and "
                  +"                polstate not like '03%%')) and managecom like '86%%' and "
                  +"                AppFlag = '1' and (StateFlag is null or StateFlag = '1') and "
                  +"                (StopFlag = '0' or StopFlag is null) and "
                  +"                (StateFlag is null or StateFlag = '1'))) "
                  +"  order by contNo with ur "
                  ;
            mSSRS = new ExeSQL().execSQL(querySql);
            if (mSSRS.getMaxRow()==0 || mSSRS == null)
            {
                CError.buildErr(this, "系统中没有符合催收时间范围的保单信息！");
                return false;
            }

            //抽档批次号
            String tLimit = PubFun.getNoLimit(manageCom);
            String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
            mserNo = serNo;

            for (int i = 1; i <= mSSRS.getMaxRow(); i++) {


                String contNo = mSSRS.GetText(i, 1);
                LCContSchema tempLCContSchema = new LCContSchema();
                tempLCContSchema.setContNo(contNo);

                VData tVData = new VData();
                tVData.add(tempLCContSchema);
                tVData.add(tGI);
                tVData.add(serNo);
                mTransferData.setNameAndValue("serNo", mserNo);
                mTransferData.setNameAndValue("queryType","3");
                tVData.add(mTransferData);

                //针对保单进行抽档操作（例如：生成应收数据，应收抽档通知书等）
                PRnewDueFeeBL tPRnewDueFeeBL = new PRnewDueFeeBL();
                if (!tPRnewDueFeeBL.submitData(tVData, "INSERT")) {
                    CError.buildErr(this, "保单号为：" + contNo + "的保单催收失败:"
                                    + tPRnewDueFeeBL.mErrors.getFirstError());
                    continue;
                }
            }

        return true ;
    }
}
