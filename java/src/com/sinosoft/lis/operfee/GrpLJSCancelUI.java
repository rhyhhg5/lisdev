package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class GrpLJSCancelUI {

  //业务处理相关变量
  private VData mInputData ;
  public  CErrors mErrors=new CErrors();

  public GrpLJSCancelUI() {
  }
  public static void main(String[] args) {
   // GrpLJSCancelUI GrpDueFeeUI1 = new GrpLJSCancelUI();
    GlobalInput tGI = new GlobalInput();
    tGI.ComCode="86";
    tGI.Operator="001";
    tGI.ManageCom="86";
    LJSPaySchema tLJSPaySchema = new LJSPaySchema();
    tLJSPaySchema.setGetNoticeNo("31000000145");

    TransferData tTransferData= new TransferData();
    tTransferData.setNameAndValue("CancelMode","0");
    GrpLJSCancelUI tGrpLJSCancelUI= new GrpLJSCancelUI();
    VData tVData = new VData();
    tVData.add(tGI);
    tVData.add(tLJSPaySchema);
    tVData.add(tTransferData);

    tGrpLJSCancelUI.submitData(tVData,"INSERT");

  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    GrpLJSCancelBL tGrpLJSCancelBL=new GrpLJSCancelBL();
    System.out.println("Start GrpLJSCancelBL UI Submit...");
    tGrpLJSCancelBL.submitData(mInputData,cOperate);

    System.out.println("End GrpLJSCancelBL UI Submit...");

    mInputData=null;
    //如果有需要处理的错误，则返回
    if (tGrpLJSCancelBL.mErrors.needDealError() )
       {
       this.mErrors.copyAllErrors(tGrpLJSCancelBL.mErrors ) ;
       return false;
       }
   System.out.println("error num="+mErrors.getErrorCount());
    return true;
  }

}
