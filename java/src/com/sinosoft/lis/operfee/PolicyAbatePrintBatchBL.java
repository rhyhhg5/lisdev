package com.sinosoft.lis.operfee;

import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.schema.LDSysVarSchema;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.f1print.PDFPrintBatchManagerBL;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.vschema.LOPRTManagerSet;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class PolicyAbatePrintBatchBL {

    private String mOperate;
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private GlobalInput mG = new GlobalInput();

    private LDSysVarSchema mLDSysVarSchema = new LDSysVarSchema();
    private LCContSet mLCContSet = new LCContSet();

    TransferData mTransferData = new TransferData();

    private int mCount = 0;
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap mMap = new MMap();
    private PubSubmit tPubSubmit = new PubSubmit();


    public PolicyAbatePrintBatchBL() {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        cInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        if (!getInputData(cInputData)) {
            return false;
        }
        if (mOperate.equals("PRINT")) {
            if (!dealData()) {
                return false;
            }
        }
        if (!prepareOutputData()) {
            return false;
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    private boolean getInputData(VData cInputData) {
        mLCContSet = (LCContSet) cInputData.getObjectByObjectName("LCContSet",
                0);
        mTransferData = (TransferData) cInputData.getObjectByObjectName(
                "TransferData", 0);
        if (mLCContSet == null || mTransferData == null) { //（服务事件关联表）
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "OmnipAnnalsBatchBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的数据为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLDSysVarSchema.setSchema((LDSysVarSchema) cInputData.
                                  getObjectByObjectName("LDSysVarSchema", 0));

        return true;
    }


    public int getCount() {
        return mCount;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData() {
        if (mOperate.equals("PRINT")) {
            VData tVData;

            mCount = mLCContSet.size();

            LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet(); //新增，用来更新打印状态，次数等信息。by huxl @ 20071024
            for (int i = 1; i <= mCount; i++) {
                LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();

                tLOPRTManagerSchema.setCode("42");
                tLOPRTManagerSchema.setOtherNo(mLCContSet.get(i).getContNo());

                tLOPRTManagerSet.add(tLOPRTManagerSchema);

            }
            tVData = new VData();
            tVData.add(tLOPRTManagerSet);
            tVData.add(mG);
            PDFPrintBatchManagerBL tPDFPrintBatchManagerBL = new
                    PDFPrintBatchManagerBL();
            if (!tPDFPrintBatchManagerBL.submitData(tVData, "batch")) {
                mErrors.addOneError(tPDFPrintBatchManagerBL.mErrors.getError(1));
                return false;
            }

            mResult = tVData;
        }
        return true;
    }

    public void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "OmnipAnnalsBatchBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "FirstPayBatchPrtBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
}
