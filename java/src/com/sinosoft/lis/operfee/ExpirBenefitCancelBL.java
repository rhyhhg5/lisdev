package com.sinosoft.lis.operfee;
//程序名称：ExpirBenefitCancelBL.java
//程序功能：续期收付费批量解锁
//创建日期：2009-7-30
//创建人  ：zhanggm
//更新记录：  更新人  zhanggm  更新日期   2009-8-11  更新原因/内容 可以通过复选框选择多条付费记录进行解锁，如果不选择，默认解锁列表中全部付费记录。
import java.util.ArrayList;

import com.sinosoft.utility.*;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.db.LJSGetDB;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;

public class ExpirBenefitCancelBL
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors = new CErrors();
	
	private MMap map = new MMap();
	
	/** 统一更新日期 */
	private String mCurrentDate = PubFun.getCurrentDate();
	
	/** 统一更新时间 */
	private String mCurrentTime = PubFun.getCurrentTime();
	
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput() ;
	
	private String mSQL = null ;
	
	ArrayList mArrayList = new ArrayList();
	
	ExeSQL mExeSQL = new ExeSQL();
	
	public ExpirBenefitCancelBL() 
	{
	}
	
	/**
	* 传输数据的公共方法
	* @param: cInputData 输入的数据
	*         cOperate 数据操作
	* @return:
	*/
    public boolean submitData(VData cInputData,String cYWType)
	{
        //将操作数据拷贝到本类中
	    if (!getInputData(cInputData))
	    {
	    	CError tError = new CError();
            tError.moduleName = "ExpirBenefitCancelBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "数据处理失败ExpirBenefitCancelBL-->getInputData!";
            this.mErrors.addOneError(tError);
	        return false;
	    }
	    
        //数据校验
        if (!checkData()) 
        {
            return false;
        }
	    
	    //进行业务处理
	    if (!dealData())
	    {
	        // @@错误处理
            CError tError = new CError();
	        tError.moduleName = "ExpirBenefitCancelBL";
	        tError.functionName = "submitData";
	        tError.errorMessage = "数据处理失败ExpirBenefitCancelBL-->dealData!";
	        this.mErrors .addOneError(tError) ;
	        return false;
	    }
	    
	    //提交
	    if(!submitData())
	    {
	    	return false;
	    }
	    return true;
	}
	 /**
	 * 根据前面的输入数据，进行BL逻辑处理
	 * 如果在处理过程中出错，则返回false,否则返回true
	*/
	private boolean dealData()
	{
		if(mArrayList.size()==0)
		{
			String sql = "select distinct d.GetNoticeNo from LJSGet a ,LJSGetDraw d " + mSQL; 
			SSRS tSSRS = new SSRS();
			tSSRS = mExeSQL.execSQL(sql);
			for(int i=1; i<=tSSRS.getMaxRow(); i++)
			{
				String tGetNoticeNo = tSSRS.GetText(i, 1);
				if(!isNull(tGetNoticeNo))
				{
					if(!cancelData(tGetNoticeNo))
					{
						continue;
					}
				}
			}
		}
		else
		{
			for(int i=0; i<mArrayList.size(); i++)
			{
				String tGetNoticeNo = (String)mArrayList.get(i);
				if(!isNull(tGetNoticeNo))
				{
					if(!cancelData(tGetNoticeNo))
					{
						continue;
					}
				}
			}
		}
	    return true;
	}
	
	/**
	 * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData)
	{
	    this.mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput", 0);

	    TransferData tTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
	    mSQL = (String)tTransferData.getValueByName("sql");
	    if (mGlobalInput == null) 
        {
        	CError tError = new CError();
            tError.moduleName = "ExpirBenefitCancelBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的mGlobalInput为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if(isNull(mSQL))
        {
        	CError tError = new CError();
            tError.moduleName = "ExpirBenefitCancelBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入查询sql为空";
            this.mErrors.addOneError(tError);
            return false;
        }
        mArrayList = (ArrayList)tTransferData.getValueByName("arrayList");
		return true;
	}
	 
    private boolean submitData()
    {
    	VData tVData = new VData();
    	tVData.add(map);
    	PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(tVData, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitCancelBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
	    return true;
    }
    
    private boolean checkData() 
    {
        System.out.println("进行校验.....");
        
        return true;
    }
    
    private boolean cancelData(String aGetNoticeNo) 
    {
    	String sql = "select ContNo from LJSGetDraw where GetNoticeNo = '" + aGetNoticeNo + "' ";
    	String tContNo = mExeSQL.getOneValue(sql);
    	if(isNull(tContNo))
    	{
    		CError tError = new CError();
            tError.moduleName = "ExpirBenefitCancelBL";
            tError.functionName = "cancelData";
            tError.errorMessage = "给付数据"+aGetNoticeNo+"的LJSGetDraw数据缺失";
            this.mErrors.addOneError(tError);
            return false;
    	}
    	
    	LJAGetDB tLJAGetDB = new LJAGetDB();
    	tLJAGetDB.setActuGetNo(aGetNoticeNo);
    	if(tLJAGetDB.getInfo())
       	{
    		System.out.println(aGetNoticeNo+"已给付确认");
    		
    		if("1".equals(tLJAGetDB.getBankOnTheWayFlag()))
           	{
           		CError tError = new CError();
           		tError.moduleName = "ExpirBenefitCancelBL";
           		tError.functionName = "cancelData";
           		tError.errorMessage = "给付任务" + tLJAGetDB.getOtherNo() + "银行在途";
           		System.out.println(tError.errorMessage);
           		this.mErrors.addOneError(tError);
           		return false;
           	}
            if(null != tLJAGetDB.getEnterAccDate() && !tLJAGetDB.getEnterAccDate().equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "ExpirBenefitCancelBL";
                tError.functionName = "cancelData";
                tError.errorMessage = "给付任务" + tLJAGetDB.getOtherNo() + "银行已经回盘";
                System.out.println(tError.errorMessage);
                this.mErrors.addOneError(tError);
                return false;
            }
            if(null != tLJAGetDB.getConfDate() && !tLJAGetDB.getConfDate().equals(""))
            {
                CError tError = new CError();
                tError.moduleName = "ExpirBenefitCancelBL";
                tError.functionName = "cancelData";
                tError.errorMessage = "给付任务" + tLJAGetDB.getOtherNo() + "已经付费成功";
                System.out.println(tError.errorMessage);
                this.mErrors.addOneError(tError);
                return false;
            }
    		LJAGetSchema tLJAGetSchema = new LJAGetSchema();
    		tLJAGetSchema.setSchema(tLJAGetDB.getSchema());
    		map.put(tLJAGetSchema, SysConst.DELETE);
    		
    		LockTableSchema tLockTableLJA = new LockTableSchema();
    		tLockTableLJA.setNoType("M2");
    		tLockTableLJA.setNoLimit(tContNo);
    		map.put(tLockTableLJA, SysConst.DELETE);
    		
       	}

    	LJSGetDB tLJSGetDB = new LJSGetDB();
       	tLJSGetDB.setGetNoticeNo(aGetNoticeNo);
       	if(!tLJSGetDB.getInfo())
       	{
       		CError tError = new CError();
       		tError.moduleName = "ExpirBenefitCancelBL";
       		tError.functionName = "cancelData";
       		tError.errorMessage =  "给付任务" + aGetNoticeNo +"没有得到LJSGet!";
       		this.mErrors.addOneError(tError);
       		return false;
       	}
       	
       	LJSGetSchema tLJSGetSchema = new LJSGetSchema();
       	tLJSGetSchema.setSchema(tLJSGetDB.getSchema());
       	
    	map.put(tLJSGetSchema, SysConst.DELETE);
    	
    	String deleteSQL = "delete from LJSGetDraw where GetNoticeNo = '" + aGetNoticeNo + "' ";
    	map.put(deleteSQL, SysConst.DELETE);
    	
    	LockTableSchema tLockTableLJS = new LockTableSchema();
    	tLockTableLJS.setNoType("MJ");
    	tLockTableLJS.setNoLimit(tContNo);
		map.put(tLockTableLJS, SysConst.DELETE);
    		
        return true;
        
    }
    
    //判断字符串是否为空，空返回true，非空返回false
    //空包括 ""，null， "null" 
    private boolean isNull(String s)
    {
    	if("".equals(s) || null==s || "null".equals(s))
		{
    		return true;
		}
    	else
    	{
    		return false;
    	}
    }
}
