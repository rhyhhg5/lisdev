/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.operfee;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

/**
 * <p>Title: Web业务系统</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class NewIndiDueFeeMultiUI
{

    //业务处理相关变量
    private VData mInputData;
    public CErrors mErrors = new CErrors();

    public NewIndiDueFeeMultiUI()
    {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        NewIndiDueFeeMultiBL tNewIndiDueFeeMultiBL = new NewIndiDueFeeMultiBL();
        //如果有需要处理的错误，则返回
        tNewIndiDueFeeMultiBL.submitData(mInputData, cOperate);
        if (tNewIndiDueFeeMultiBL.mErrors.needDealError())
        {
            this.mErrors.copyAllErrors(tNewIndiDueFeeMultiBL.mErrors);
            return false;
        }
        System.out.println("error num=" + mErrors.getErrorCount());
        return true;
    }

    /**
     * 测试函数
     * @param args String[]
     */
    public static void main(String[] args)
    {
        LCPolSchema tLCPolSchema = new LCPolSchema(); // 个人保单表
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "8695";
        tGI.ManageCom = "8695";
        tGI.Operator = "001";
        tLCPolSchema.setGetStartDate("1900-01-01"); //将判断条件设置在起领日期字段中
        tLCPolSchema.setPayEndDate("2007-10-24"); ////将判断条件设置在终交日期字段中
        // by zhanghui 2005.2.18
        TransferData transferData1 = new TransferData();
        transferData1.setNameAndValue("bankCode", "9995");

        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tLCPolSchema);
        //by zhanghui 2005.2.18
        tVData.add(transferData1);

        NewIndiDueFeeMultiUI tNewIndiDueFeeMultiUI = new NewIndiDueFeeMultiUI();
        tNewIndiDueFeeMultiUI.submitData(tVData, "INSERT");
    }
}
