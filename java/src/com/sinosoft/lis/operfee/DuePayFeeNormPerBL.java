package com.sinosoft.lis.operfee;

import java.util.*;
import java.lang.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class DuePayFeeNormPerBL  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 数据操作字符串 */
  private String mOperate;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private LJSPaySchema       mLJSPaySchema        = new LJSPaySchema();
  private LJSPayPersonSet    mLJSPayPersonSet     = new LJSPayPersonSet();
  private LJSPayPersonSet    mLJSPayPersonSetNew  = new LJSPayPersonSet();

  //业务处理相关变量

  public DuePayFeeNormPerBL() {
  }
  public static void main(String[] args) {
    DuePayFeeNormPerBL DuePayFeeNormPerBL1 = new DuePayFeeNormPerBL();
    LJSPayPersonBL mLJSPayPersonBL =new LJSPayPersonBL();
    VData tv=new VData();
    tv.add(mLJSPayPersonBL);
    DuePayFeeNormPerBL1.submitData(tv,"INSERT");
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
System.out.println("OperateData:  "+cOperate);


    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
System.out.println("After getinputdata");

    //进行业务处理
    if (!dealData())
      return false;
System.out.println("After dealData！");
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
System.out.println("After prepareOutputData");

System.out.println("Start LJSPayPerson BL Submit...");

    DuePayFeeNormPerBLS tDuePayFeeNormPerBLS=new DuePayFeeNormPerBLS();
    tDuePayFeeNormPerBLS.submitData(mInputData,cOperate);

System.out.println("End LJSPayPerson BL Submit...");

    //如果有需要处理的错误，则返回
    if (tDuePayFeeNormPerBLS.mErrors .needDealError())
    {
        this.mErrors .copyAllErrors(tDuePayFeeNormPerBLS.mErrors ) ;
    }

    mInputData=null;
    return true;
  }

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData()
  {
    int i,iMax;
    boolean tReturn =false;
    String tNo="";
    //处理个人信息数据
    //添加纪录
    if(this.mOperate.equals("INSERT"))
    {
     LCPremSet     mLCPremSet      = new LCPremSet();
     LCPremSchema  mLCPremSchema   = new LCPremSchema();
     LCPolSet      mLCPolSet       = new LCPolSet();
     LCPolSchema   mLCPolSchema    = new LCPolSchema();
//1-查询个人保单表
     LCPolQueryUI tLCPolQueryUI = new LCPolQueryUI();
     mLCPolSchema.setPolNo(mLJSPaySchema.getOtherNo());
     VData tVData = new VData();
     tVData.addElement(mLCPolSchema);
     tLCPolQueryUI.submitData(tVData,"QUERY");
     tVData.clear();
     tVData=tLCPolQueryUI.getResult();
     mLCPolSet.set((LCPolSet)tVData.getObjectByObjectName("LCPolSet",0));
     mLCPolSchema = (LCPolSchema)mLCPolSet.get(1);

     Date baseDate = new Date();
     Date paytoDate = new Date();  //交至日期
     Date payDate = new Date();  //交费日期
     String strPayDate="";
     String strPayToDate="";
     String unit = "";
     int interval=0; //交费间隔，以月为单位
     FDate fDate = new FDate();
//2-填充应收总表，单项纪录
//计算交费日期 =个人保单表交至日期+2个月
    paytoDate=fDate.getDate(mLCPolSchema.getPaytoDate());
    if(paytoDate!=null)//下面的函数第一个参数不能为空
    {
    payDate = PubFun.calDate(paytoDate,2,"M",null);
    strPayDate = fDate.getString(payDate);
    }
    mLJSPaySchema.setPayDate(strPayDate);
    mLJSPaySchema.setApproveCode(mLCPolSchema.getApproveCode());
    mLJSPaySchema.setApproveDate(mLCPolSchema.getApproveDate());
    mLJSPaySchema.setMakeDate(CurrentDate);
    mLJSPaySchema.setMakeTime(CurrentTime);
    mLJSPaySchema.setModifyDate(CurrentDate);
    mLJSPaySchema.setModifyTime(CurrentTime);
    mLJSPaySchema.setRiskCode(mLCPolSchema.getRiskCode());
    /*Lis5.3 upgrade set
    mLJSPaySchema.setBankAccNo(mLCPolSchema.getBankAccNo());
    mLJSPaySchema.setBankCode(mLCPolSchema.getBankCode());
    */
    mLJSPaySchema.setAgentCode(mLCPolSchema.getAgentCode());
    mLJSPaySchema.setAgentGroup(mLCPolSchema.getAgentGroup());

    tReturn=true;

//3-处理应收个人交费表，记录集，循环处理
    LJSPayPersonSchema  tLJSPayPersonSchema;
    iMax=mLJSPayPersonSet.size() ;
    LCPremQueryUI tLCPremQueryUI;
    for (i=1;i<=iMax;i++)
    {

      tLJSPayPersonSchema = new LJSPayPersonSchema() ;
      mLCPremSchema = new LCPremSchema();
      tLJSPayPersonSchema=mLJSPayPersonSet.get(i);
//保费项表查询
      mLCPremSchema.setPolNo(tLJSPayPersonSchema.getPolNo());
      mLCPremSchema.setDutyCode(tLJSPayPersonSchema.getDutyCode());
      mLCPremSchema.setPayPlanCode(tLJSPayPersonSchema.getPayPlanCode());
      tVData = new VData();
      tLCPremQueryUI = new LCPremQueryUI();
      tVData.addElement(mLCPremSchema);
      tLCPremQueryUI.submitData(tVData,"QUERY");
      tVData.clear();
      tVData=tLCPremQueryUI.getResult();
      mLCPremSet.set((LCPremSet)tVData.getObjectByObjectName("LCPremSet",0));
      mLCPremSchema = (LCPremSchema)mLCPremSet.get(1);
//查询完毕--------------------------------------------------
//下面计算 现交至日期=交至日期+交费间隔
      baseDate = fDate.getDate(mLCPremSchema.getPaytoDate());
      interval = mLCPremSchema.getPayIntv();
      if (interval == -1) interval = 0;	// 不定期缴费
      unit = "M";
//repair:日期不能为空，否则下面的函数处理会有问题
      if(baseDate!=null)
       {
//现交至日期
        paytoDate = PubFun.calDate(baseDate,interval,unit,null);
//计算交费日期 =交至日期+2个月=失效日期
        payDate = PubFun.calDate(paytoDate,2,unit,null);
//得到日期型，转换成String型
        strPayToDate = fDate.getString(paytoDate);
        strPayDate = fDate.getString(payDate);
        }
//计算完毕--------------------------------------------------------
//填充应收个人纪录
      /*Lis5.3 upgrade set
      tLJSPayPersonSchema.setGrpPolNo(mLCPremSchema.getGrpPolNo());
      */
      tLJSPayPersonSchema.setContNo(mLCPolSchema.getContNo());  //总单/合同号码
      tLJSPayPersonSchema.setPayIntv(mLCPremSchema.getPayIntv());
      tLJSPayPersonSchema.setPayDate(strPayDate);
      tLJSPayPersonSchema.setLastPayToDate(mLCPremSchema.getPaytoDate());
      tLJSPayPersonSchema.setCurPayToDate(strPayToDate);
      /*Lis5.3 upgrade get
      tLJSPayPersonSchema.setBankCode(mLCPolSchema.getBankCode());
      tLJSPayPersonSchema.setBankAccNo(mLCPolSchema.getBankAccNo());
      */
      tLJSPayPersonSchema.setApproveCode(mLCPolSchema.getApproveCode());
      tLJSPayPersonSchema.setApproveDate(mLCPolSchema.getApproveDate());
      tLJSPayPersonSchema.setMakeDate(CurrentDate);//入机日期
      tLJSPayPersonSchema.setMakeTime(CurrentTime);//入机时间
      tLJSPayPersonSchema.setModifyDate(CurrentDate);//最后一次修改日期
      tLJSPayPersonSchema.setModifyTime(CurrentTime);//最后一次修改时间
      tLJSPayPersonSchema.setManageCom(mLCPolSchema.getManageCom());
      tLJSPayPersonSchema.setAgentCom(mLCPolSchema.getAgentCom());
      tLJSPayPersonSchema.setAgentType(mLCPolSchema.getAgentType());
      tLJSPayPersonSchema.setRiskCode(mLCPolSchema.getRiskCode());
      tLJSPayPersonSchema.setAgentCode(mLCPolSchema.getAgentCode());
      tLJSPayPersonSchema.setAgentGroup(mLCPolSchema.getAgentGroup());
      mLJSPayPersonSetNew.add(tLJSPayPersonSchema);
      tReturn=true;
    }

}
    return tReturn ;

}

  //从输入数据中得到所有对象
  //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData mInputData)
  {
    //应收总表
    mLJSPaySchema.setSchema((LJSPaySchema)mInputData.getObjectByObjectName("LJSPaySchema",0));
    // 应收个人交费表
    mLJSPayPersonSet.set((LJSPayPersonSet)mInputData.getObjectByObjectName("LJSPayPersonSet",0));

    if(mLJSPaySchema==null || mLJSPayPersonSet ==null  )
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="DuePayFeeNormPerBL";
      tError.functionName="getInputData";
      tError.errorMessage="没有得到足够的数据，请您确认!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData()
  {
    mInputData=new VData();
    try
    {
//1-处理应收个人交费表，记录集，循环处理
      mInputData.add(mLJSPayPersonSetNew);

//2-应收总表，单项纪录
      mInputData.add(mLJSPaySchema);

      System.out.println("prepareOutputData:");
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="DuePayFeeNormPerBL";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }
}

