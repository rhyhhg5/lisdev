package com.sinosoft.lis.operfee;

import com.sinosoft.lis.bq.PEdorCTDetailBL;
import com.sinosoft.lis.bq.PEdorCTDetailUI;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class ContPreserveUI {
	  /** 错误处理类，每个需要错误处理的类中都放置该类 */
	  public  CErrors mErrors = new CErrors();
	  /** 往后面传输数据的容器 */
	  private VData mInputData = new VData();
	  /** 往界面传输数据的容器 */
	  private VData mResult = new VData();
	  /** 数据操作字符串 */
	  private String mOperate;

	  public ContPreserveUI() {}

	  /**
	  传输数据的公共方法
	  */
	  public boolean submitData(VData cInputData,String cOperate)
	  {
	    //将操作数据拷贝到本类中
	    this.mOperate = cOperate;
	    ContPreserveBL tContPreserveBL = new ContPreserveBL();
	    if (tContPreserveBL.submitData(cInputData,mOperate) == false)
	    {
	      // @@错误处理
	      this.mErrors.copyAllErrors(tContPreserveBL.mErrors);
	      CError tError = new CError();
	      tError.moduleName = "PInsuredUI";
	      tError.functionName = "submitData";
	      tError.errorMessage = "数据查询失败!";
	      this.mErrors .addOneError(tError) ;
	      mResult.clear();
	      return false;
	    }
	    else
	      mResult = tContPreserveBL.getResult();
	    return true;
	  }

	  public VData getResult()
	  {
	    return mResult;
	  }

	  public static void main(String[] args)
	  {}
}
