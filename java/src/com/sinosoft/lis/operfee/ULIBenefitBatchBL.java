package com.sinosoft.lis.operfee;

//程序名称：ULIBenefitBatchBL.java
//程序功能：万能老年关爱给付抽档批量
//创建日期：2009-09-03 
//创建人  ：zhanggm
//更新记录：更新人    更新日期     更新原因/内容

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;

import java.lang.String;

public class ULIBenefitBatchBL 
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private GlobalInput tGI = new GlobalInput();

    /** 数据操作字符串 */
    private String mserNo = ""; //批次号

    private TransferData mTransferData = new TransferData();

    public ULIBenefitBatchBL() 
    {
    }

    public boolean submitData(VData cInputData, String cOperate) 
    {
        //接收传入数据
        if (!getInputData(cInputData)) 
        {
            return false;
        }
        //处理数据
        if (!dealData()) 
        {
            return false;
        }
        return true;
    }

    /**
     *
     * @return boolean
     */
    public boolean dealData() 
    {
        String manageCom = (String) mTransferData.getValueByName("ManageCom");
        String querySql = (String)mTransferData.getValueByName("QuerySql");

        //批量抽档数据查询
        if(querySql == null || querySql.equals(""))
        {
        	CError tError = new CError();
            tError.moduleName = "ULIBenefitBatchBL";
            tError.functionName = "checkData";
            tError.errorMessage = "后台程序没有接收到前台查询语句";
            mErrors.addOneError(tError);
            return false;
        }
        System.out.println(querySql);

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(querySql);
        if (tSSRS == null || tSSRS.getMaxRow() == 0) 
        {
        	CError tError = new CError();
            tError.moduleName = "ULIBenefitBatchBL";
            tError.functionName = "checkData";
            tError.errorMessage = "系统中没有符合给付的保单信息！";
            mErrors.addOneError(tError);
            return false;
        }

        //抽档批次号
        String tLimit = PubFun.getNoLimit(manageCom);
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        mserNo = serNo;
        
        for ( int i = 1; i<= tSSRS.getMaxRow() ; i++)
        {
            String contNo = tSSRS.GetText(i, 1);
            LCContSchema tLCContSchema = new LCContSchema();
            tLCContSchema.setContNo(contNo);

            VData tVData = new VData();
            tVData.add(tLCContSchema);
            tVData.add(tGI);
            tVData.add(serNo);
            mTransferData.setNameAndValue("serNo", serNo);
            tVData.add(mTransferData);

            ExpirBenefitGetBL  tExpirBenefitGetBL = new ExpirBenefitGetBL();
            if (!tExpirBenefitGetBL.submitData(tVData,"INSERT"))
            {
            	CError tError = new CError();
                tError.moduleName = "ULIBenefitBatchBL";
                tError.functionName = "dealData";
                tError.errorMessage = "保单" + contNo + "给付抽档失败:" 
                                    + tExpirBenefitGetBL.mErrors.getFirstError();
                mErrors.addOneError(tError);
                continue;
            }
        }
        return true;
    }

     /**
      * 从输入数据中得到所有对象
      * @param mInputData:
      *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
      */
     private boolean getInputData(VData mInputData) 
     {
         tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
         mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData",0);
         System.out.println("mTransferData:" + mTransferData);

         if (tGI == null) 
         {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "ULIBenefitBatchBL";
             tError.functionName = "getInputData";
             tError.errorMessage = "没有得到足够的数据!";
             this.mErrors.addOneError(tError);
             return false;
         }
         return true;
     }

    public VData getResult() 
    {
        VData t = new VData();
        t.add(0,mserNo);
        return t;
    }
}
