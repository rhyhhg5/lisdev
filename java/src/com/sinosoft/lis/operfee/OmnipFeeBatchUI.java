package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class OmnipFeeBatchUI {
	 //业务处理相关变量
    private VData mInputData;
    public CErrors mErrors = new CErrors();

    public OmnipFeeBatchUI() {
    }
	public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        OmnipFeeBatchBL tOmnipFeeBatchBL = new OmnipFeeBatchBL();
        //TODO delete later
        System.out.println("Start OmnipFeeBatchUI UI Submit...");
        tOmnipFeeBatchBL.submitData(mInputData, cOperate);
      //TODO delete later
        System.out.println("End OmnipFeeBatchUI UI Submit...");

        mInputData = null;
        //如果有需要处理的错误，则返回
        if (tOmnipFeeBatchBL.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tOmnipFeeBatchBL.mErrors);
            return false;
        }
      //TODO delete later
        System.out.println("error num=" + mErrors.getErrorCount());
        return true;
    }
}
