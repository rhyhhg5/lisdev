package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 用户接口类，接收页面传入的数据，传入后台进行工单结案业务逻辑的处理。
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class IndiVisitOverUI
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = null;

    public IndiVisitOverUI()
    {
    }

    /**
     * 操作的提交方法，作为页面数据的入口，准备完数据后执行入库操作。
     * @param cInputData VData:包含：
     a)	TransferData对象，本次查询工单的sql
     b)	GlobalInput对象，完整的登陆用户信息
     * @param cOperate String：操作类型，此可为空字符创“”
     * @return boolean：操作成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        IndiVisitOverBL bl = new IndiVisitOverBL();
        if(!bl.submitData(cInputData, cOperate))
        {
            this.mErrors = bl.mErrors;
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        IndiVisitOverUI indivisitoverui = new IndiVisitOverUI();
    }
}
