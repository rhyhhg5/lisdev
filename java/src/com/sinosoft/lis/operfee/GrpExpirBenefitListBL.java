package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.vschema.LDCodeSet;
import java.sql.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GrpExpirBenefitListBL {

    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = new TransferData();
    private XmlExport xmlexport = null;
    private VData mResult = null;
    private SSRS mSSRS;
    private TextTag textTag = new TextTag(); //新建一个TextTag的实例
    private String mGrpContNo = "";
    private String mGrpName ="";
    private String mManageCom = "";
    private String mAgentGroup = "";
    private String mAgentName ="";

    public GrpExpirBenefitListBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //取得外部传入数据
        if (!getInputData(cInputData)) {
            return false;
        }

        //校验数据合法性
        if (!getListData()) {
            return false;
        }

        //获取打印所需数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        try {

            mGlobalInput = (GlobalInput) cInputData.
                           getObjectByObjectName("GlobalInput", 0);
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
        } catch (Exception e) {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("传入的数据不完整，" + e.toString());
            return false;
        }
        //mStartDate = (String) mTransferData.getValueByName("SingleStartDate");
       // mEndDate = (String) mTransferData.getValueByName("SingleEndDate");
       // mDealState = (String) mTransferData.getValueByName("DealState");
        mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
        System.out.println(mGrpContNo);
        return true;
    }

    /**
     * 查询列表显示数据
     * @param schema LCContSchema
     * @return String
     */

    private boolean getListData() {

        String tSQL =" select a.grpcontno,a.appntname,insuredno,a.riskcode,amnt,payenddate "
                    +" ,(select case when count(printtimes) = 0 then '未打印' else '已打印' end  From loprtmanager where code ='bq002' and a.grpcontno = otherno)  "
                    +" ,(select name from ldcom where a.managecom = comcode ), (select name From labranchgroup  where agentgroup =a.agentgroup ) "
                    +" ,(select name From laagent where agentcode =a.agentcode ) "
                    +" ,(select case count(1) when 0 then '未回销' else '已回销' end from LPEdorEspecialData where Edorno = a.grpcontno and EdorType = 'TJ' and DetailType = 'QUERYTYPE') "
                    +" ,a.insuredname "
                    +" From LCPol a, LMRiskApp c, LMRisk d where 1 =1 and  c.riskcode = a.riskcode "
                    +" and c.riskcode = d.riskcode and D.getflag ='Y'	and RiskType3 !='7' "
                    +" and a.stateflag in('1','3') "
                    +" and a.GrpContNo ='"+mGrpContNo+"' "
                    ;

        ExeSQL tExeSQL = new ExeSQL();

        mSSRS = tExeSQL.execSQL(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "creatFile";
            tError.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    /**
     * 获取打印所需数据
     * @param schema LCContSchema
     * @return String
     */
    private boolean getPrintData() {

        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("GrpExpirBenefitList.vts", "printer"); //最好紧接着就初始化xml文档

        String[] title = {"序", "保单号", "投保单位", "客户号", "给付险种",
                         "给付金额", "应给付日期", "通知书打印标示","保单管理机构","营销部门","保单业务员", "任务状态","被保人姓名"};

        xmlexport.addListTable(getListTable(), title);

        if (textTag.size() > 0) {
            xmlexport.addTextTag(textTag);
        }

        xmlexport.outputDocumentToFile("C:\\", "GrpExpirBenefitList");
        mResult = new VData();
        mResult.addElement(xmlexport);

        return true;
    }

    /**
     *  获取列表数据
     * @param schema LCContSchema
     * @return String
     */
    private ListTable getListTable() {
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[13];
            //标记序列号
//            mGrpContNo = mSSRS.GetText(i,2);
            mGrpName = mSSRS.GetText(i,2);
            mManageCom = mSSRS.GetText(i,8);
            mAgentGroup = mSSRS.GetText(i,9);
            mAgentName = mSSRS.GetText(i,10);

            info[0] = String.valueOf(i);
            info[1] = mSSRS.GetText(i,1);
            info[2] = mSSRS.GetText(i,2);
            info[3] = mSSRS.GetText(i,3);
            info[4] = mSSRS.GetText(i,4);
            info[5] = mSSRS.GetText(i,5);
            info[6] = mSSRS.GetText(i,6);
            info[7] = mSSRS.GetText(i,7);
            info[8] = mSSRS.GetText(i,8);
            info[9] = mSSRS.GetText(i,9);
            info[10] = mSSRS.GetText(i,10);
            info[11] = mSSRS.GetText(i,11);
            info[12] = mSSRS.GetText(i,12);
            tListTable.add(info);
        }

        textTag.add("GrpContNo", mGrpContNo);
        textTag.add("GrpName", mGrpName);
        textTag.add("ManageCom", mManageCom);
        textTag.add("AgentGroup", mAgentGroup);
        textTag.add("AgentName",mAgentName);
//        textTag.add()
        tListTable.setName("List");
        return tListTable;
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


    private void jbInit() throws Exception {
    }

    //查询公司名称
    private String getCompanyName() {
        //通过LDCode得到保单号，然后得到公司名称
        String manageCode = mGlobalInput.ManageCom;
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("station");
        tLDCodeDB.setCode(manageCode);
        LDCodeSet set = tLDCodeDB.query();
        if (set == null || set.size() == 0) {

            mErrors.addOneError("没有查到公司名");
            System.out.println("没有查到公司名" + tLDCodeDB.mErrors.getErrContent());
            return null;
        } else {
            return set.get(1).getCodeName();
        }
    }

}
