package com.sinosoft.lis.operfee;


import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.bq.CommonBL;
import java.util.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.xb.*;

public class PChildInsurXBDealBL{

  //错误处理类，每个需要错误处理的类中都放置该类
  public CErrors mErrors = new CErrors();
  private String dealtype ;
  private String customerBackDate;
  private String getNoticeNo;
  private VData mSubmitData = new VData();
  private GlobalInput tGI = new GlobalInput();

  /** 数据操作字符串 */
  private String serNo = ""; //流水号
  private String tLimit = "";
  private String tNo = ""; //生成的暂交费号
  private String mOperate;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();

  private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
  private LJTempFeeSet mLJTempFeeSetNew = new LJTempFeeSet();
  private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
  private LJTempFeeClassSet mLJTempFeeClassSetNew = new LJTempFeeClassSet();
  private LJTempFeeClassSet mLJTempFeeClassSetDel = new LJTempFeeClassSet();

  //传输数据的公共方法
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    System.out.println("After getinputdata");

    //进行业务处理
    if (!dealData()) {
        return false;
  }
    System.out.println("After dealData！");
    return true;
}

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData() {
          //查找应收
          LJSPayDB tLJSPayDB = new LJSPayDB();
          tLJSPayDB.setGetNoticeNo(getNoticeNo);
          if(!tLJSPayDB.getInfo())
          {
              mErrors.addOneError("获取应收失败。");
              return false;
          }
          LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
          tLJSPayPersonDB.setGetNoticeNo(getNoticeNo);
          LJSPayPersonSet tLJSPayPersonSet = tLJSPayPersonDB.query();
          if(tLJSPayPersonSet.size()==0)
          {
              mErrors.addOneError("获取应收明细失败。");
              return false;
          }
          //生成暂收数据
          //产生流水号
          tLimit = PubFun.getNoLimit(tLJSPayDB.getManageCom());
          serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);

          LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
          double money =0;
          for(int i=1;i<=tLJSPayPersonSet.size();i++)
          {
              LJSPayPersonSchema tLJSPayPersonSchema = tLJSPayPersonSet.get(i);
              LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
              tLJTempFeeSchema.setTempFeeNo(getNoticeNo);
              tLJTempFeeSchema.setTempFeeType("2");//续期续保
              tLJTempFeeSchema.setRiskCode(tLJSPayPersonSchema.getRiskCode());
              tLJTempFeeSchema.setPayIntv(0);
              tLJTempFeeSchema.setOtherNo(tLJSPayPersonSchema.getPolNo());
              tLJTempFeeSchema.setOtherNoType("2");
              money +=tLJSPayPersonSchema.getSumActuPayMoney();
              tLJTempFeeSchema.setPayMoney(tLJSPayPersonSchema.getSumActuPayMoney());
              tLJTempFeeSchema.setPayDate(CurrentDate);
              tLJTempFeeSchema.setEnterAccDate(CurrentDate);
              tLJTempFeeSchema.setConfMakeDate(CurrentDate);
              tLJTempFeeSchema.setConfMakeTime(CurrentTime);
              tLJTempFeeSchema.setManageCom(tLJSPayPersonSchema.getManageCom());
              tLJTempFeeSchema.setConfFlag("0");
              tLJTempFeeSchema.setSerialNo(serNo);
              tLJTempFeeSchema.setOperator(tGI.Operator);
              tLJTempFeeSchema.setMakeDate(CurrentDate);
              tLJTempFeeSchema.setMakeTime(CurrentTime);
              tLJTempFeeSchema.setModifyDate(CurrentDate);
              tLJTempFeeSchema.setModifyTime(CurrentTime);
              tLJTempFeeSet.add(tLJTempFeeSchema);
          }
          //生成暂收分类表数据
          LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
          tLJTempFeeClassSchema.setTempFeeNo(getNoticeNo);
          tLJTempFeeClassSchema.setPayMode("5");
          tLJTempFeeClassSchema.setPayMoney(money);
          tLJTempFeeClassSchema.setPayDate(CurrentDate);
          tLJTempFeeClassSchema.setEnterAccDate(CurrentDate);
          tLJTempFeeClassSchema.setConfMakeDate(CurrentDate);
          tLJTempFeeClassSchema.setConfMakeTime(CurrentTime);
          tLJTempFeeClassSchema.setManageCom(tLJSPayDB.getManageCom());
          tLJTempFeeClassSchema.setConfFlag("0");
          tLJTempFeeClassSchema.setSerialNo(serNo);
          tLJTempFeeClassSchema.setOperator(tGI.Operator);
          tLJTempFeeClassSchema.setMakeDate(CurrentDate);
          tLJTempFeeClassSchema.setMakeTime(CurrentTime);
          tLJTempFeeClassSchema.setModifyDate(CurrentDate);
          tLJTempFeeClassSchema.setModifyTime(CurrentTime);

          //修改应收状态
          String updateLjspayb = "update ljspayb set dealstate ='4' where getnoticeno ='"+getNoticeNo+"'";
          String updateLjspayPersonB = "update ljspaypersonb set dealstate ='4' where getnoticeno ='"+getNoticeNo+"'";

          //查询保单
          LCContDB tLCContDB = new LCContDB();
          tLCContDB.setContNo(tLJSPayDB.getOtherNo());
          if (!tLCContDB.getInfo()) {
              mErrors.addOneError("查询保单错误");
              return false;
          }

          //生成暂收数据
          MMap tMMap = new MMap();
          tMMap.put(tLJTempFeeSet,"DELETE&INSERT");
          tMMap.put(tLJTempFeeClassSchema,"DELETE&INSERT");
          tMMap.put(updateLjspayb,"UPDATE");
          tMMap.put(updateLjspayPersonB,"UPDATE");
          //存储客户录入的信息

          mSubmitData.clear();
          mSubmitData.add(tMMap);
          PubSubmit tPubSubmit = new PubSubmit();
          if(!tPubSubmit.submitData(mSubmitData, mOperate))
          {
              mErrors.addOneError("生成财务暂收失败");
              return false;
          }
          //调用核销程序

/*
          PRnewDueVerifyUI tPRnewDueVerifyUI = new PRnewDueVerifyUI();
          VData tVData = new VData();
          tVData.add(tGI);
          tVData.addElement(tLCContDB.getSchema());
          tPRnewDueVerifyUI.submitData(tVData,"VERIFY");
          if(tPRnewDueVerifyUI.mErrors.needDealError())
          {
              mErrors.addOneError(" 核销失败，原因是: " + tPRnewDueVerifyUI.mErrors.getFirstError());
              mSubmitData.clear();
              tMMap = new MMap();
              tMMap.put(tLJTempFeeSet,"DELETE");
              tMMap.put(tLJTempFeeClassSchema,"DELETE");
              updateLjspayb = "update ljspayb set dealstate ='0' where getnoticeno ='"+getNoticeNo+"'";
              updateLjspayPersonB = "update ljspaypersonb set dealstate ='0' where getnoticeno ='"+getNoticeNo+"'";
              tMMap.put(updateLjspayb,"UPDATE");
              tMMap.put(updateLjspayPersonB,"UPDATE");
              mSubmitData.add(tMMap);
              tPubSubmit.submitData(mSubmitData, mOperate);
              return false;
          }
*/
          return true;
      }
      private boolean getInputData(VData mInputData) {
              // 公用变量
              tGI = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
              TransferData tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);
              if (tGI == null || tTransferData == null)
              {
                  mErrors.addOneError("传入的数据不完整。");
                  return false;
              }
              dealtype = (String) tTransferData.getValueByName("dealType");
              customerBackDate = (String) tTransferData.getValueByName("customerBackDate");
              getNoticeNo = (String) tTransferData.getValueByName("getNoticeNo");
              if(!(valiData(dealtype)&&valiData(customerBackDate)&&valiData(getNoticeNo)))
              {
                  mErrors.addOneError("传入的数据不完整。");
                  return false;
              }
              return true;
          }
          private boolean valiData(String aData)
          {
              if(aData==null||"".equals(aData))
              {
                  return false;
              }
              return true;
          }
      }
