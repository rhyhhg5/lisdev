package com.sinosoft.lis.operfee;

import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LGErrorLogSchema;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.db.LCGrpContDB;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GrpExpirBenefitBatchBL {

    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private GlobalInput tGI = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String StartDate = ""; //应收开始时间
    private String EndDate = ""; //应收结束时间
    private String mserNo = ""; //批次号
    private String mGrpContNo = "";

    private boolean mDuebySpecFlag = false;

    private TransferData mTransferData = new TransferData();

    public GrpExpirBenefitBatchBL() {
    }


    /**
     * @param cInputData VData，包含：
     * 1、	GlobalInput对象，完整的登陆用户信息
     * 2、	TransferData对象，包含StartDate， EndDate， ManageCom
     * @param cOperate String：操作类型，此可为空字符串“”
     * @return boolean：操作成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.mOperate = cOperate;
        //接收传入数据
        if (!getInputData(cInputData)) {
            return false;
        }
        //处理数据
        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return boolean
     */
    public boolean dealData() {
        //接收外部传入数据
        StartDate = (String) mTransferData.getValueByName("StartDate");
        EndDate = (String) mTransferData.getValueByName("EndDate");
        String manageCom = (String) mTransferData.getValueByName("ManageCom");
        String querySql = (String) mTransferData.getValueByName("QuerySql");
        String queryType = (String) mTransferData.getValueByName("QueryType");
        String PayMode = (String) mTransferData.getValueByName("PayMode"); //个单给付方式
        String GPayMode=(String)mTransferData.getValueByName("GPayMode"); //团体给付方式
        //批量抽档数据查询

        if (querySql == null || querySql.equals("")) {
            CError.buildErr(this, "后台程序没有接收到前台查询语句");
            return false;
        }
        System.out.println(querySql);

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(querySql);
        if (tSSRS == null || tSSRS.getMaxRow() == 0) {
            CError.buildErr(this, "系统中没有符合给付的保单信息！");
            return false;
        }

        //抽档批次号
        String tLimit = PubFun.getNoLimit(manageCom);
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        mserNo = serNo;
        //个人给付
        if (queryType.equals("3")) {
            //循环处理数据
            for (int i = 1; i <= tSSRS.getMaxRow(); i++) {

                String grpContNo = tSSRS.GetText(i, 1);
                LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
                LCGrpContDB tLCGrpContDB = new LCGrpContDB();
                tLCGrpContDB.setGrpContNo(grpContNo);
                if(!tLCGrpContDB.getInfo())
                {
                    CError.buildErr(this, "保单号为：" + grpContNo + "查询保单信息失败!");
                    return false ;
                }
                tLCGrpContSchema = tLCGrpContDB.getSchema();
                VData tVData = new VData();
                tVData.add(tLCGrpContSchema);
                tVData.add(tGI);
                tVData.add(serNo);
                mTransferData.setNameAndValue("serNo", serNo);
                mTransferData.setNameAndValue("PayMode",PayMode);
                mTransferData.setNameAndValue("EndDate",EndDate);
                tVData.add(mTransferData);

                //针对保单进行抽档操作（例如：生成应收数据，应收抽档通知书等）
                GrpExpirBenefitBL tGrpExpirBenefitBL = new GrpExpirBenefitBL();
                if (!tGrpExpirBenefitBL.submitData(tVData, "INSERT")) {
                    CError.buildErr(this, "保单号为：" + grpContNo + "的保单催收失败:"
                                    + tGrpExpirBenefitBL.mErrors.getFirstError());
                    continue;
                }
            }
        } else {//团体统一给付
            for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                String grpContNo = tSSRS.GetText(i, 1);
                LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
                LCGrpContDB tLCGrpContDB = new LCGrpContDB();
                tLCGrpContDB.setGrpContNo(grpContNo);
                if(!tLCGrpContDB.getInfo())
                {
                    CError.buildErr(this, "保单号为：" + grpContNo + "查询保单信息失败!");
                    return false ;
                }
                tLCGrpContSchema = tLCGrpContDB.getSchema();
                VData tVData = new VData();
                tVData.add(tLCGrpContSchema);
                tVData.add(tGI);
                tVData.add(serNo);
                mTransferData.setNameAndValue("serNo", serNo);
                mTransferData.setNameAndValue("GPayMode",GPayMode);
                mTransferData.setNameAndValue("EndDate",EndDate);
                tVData.add(mTransferData);

                GrpExpirBenefitGetBL tGrpExpirBenefitGetBL = new GrpExpirBenefitGetBL();
                if (!tGrpExpirBenefitGetBL.submitData(tVData, "INSERT")) {
                    CError.buildErr(this, "保单号为：" + grpContNo + "的保单催收失败:"
                                    + tGrpExpirBenefitGetBL.mErrors.getFirstError());
                    continue;
                }
            }
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param mInputData:
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) {
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));

        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData",
                0);
        System.out.println("mTransferData:" + mTransferData);

        if (tGI == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitBatchBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData() {
        mInputData = new VData();
        return true;
    }

    public VData getResult() {
        VData t = new VData();
        t.add(0, mserNo);
        return t;
    }

    /**
     * 加入到日志表数据,记录因为各种原因抽档失败的单子
     * @param
     * @return boolean
     */
    private boolean dealErrorLog(LCPolSchema tLCPolSchema, String Error) {
        LGErrorLogSchema tLGErrorLogSchema = new LGErrorLogSchema();
        tLGErrorLogSchema.setSerialNo(mserNo);
        tLGErrorLogSchema.setErrorType("0002");
        tLGErrorLogSchema.setErrorTypeSub("01");
        tLGErrorLogSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
        tLGErrorLogSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
        tLGErrorLogSchema.setContNo(tLCPolSchema.getContNo());
        tLGErrorLogSchema.setPolNo(tLCPolSchema.getPolNo());
        tLGErrorLogSchema.setOperator(tGI.Operator);
        tLGErrorLogSchema.setMakeDate(CurrentDate);
        tLGErrorLogSchema.setMakeTime(CurrentTime);
        tLGErrorLogSchema.setModifyDate(CurrentDate);
        tLGErrorLogSchema.setModifyTime(CurrentTime);
        tLGErrorLogSchema.setDescribe(Error);

        MMap tMap = new MMap();
        tMap.put(tLGErrorLogSchema, "DELETE&INSERT");
        VData tInputData = new VData();
        tInputData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tInputData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LGErrorLog 表失败!");
            return false;
        }
        return true;
    }

}
