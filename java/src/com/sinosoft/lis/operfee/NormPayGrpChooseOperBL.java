package com.sinosoft.lis.operfee;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */
public class NormPayGrpChooseOperBL {
  //错误处理类，每个需要错误处理的类中都放置该类
  public CErrors mErrors = new CErrors();

  /** 往后面传输数据的容器 */
  private VData mInputData;

  /** 数据操作字符串 */
  private String mOperate;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
 // private String serNo = ""; //流水号
 // private String tLimit = "";
 // private String payNo = ""; //交费收据号

  //暂收费表
  private LJTempFeeBL mLJTempFeeBL = new LJTempFeeBL();
  private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
  //private
 // private LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
  //暂收费分类表
  private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
  private LJTempFeeClassSet mLJTempFeeClassNewSet = new LJTempFeeClassSet();

  //应收个人交费表
  private LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet();
  private LJSPayPersonSet mLJSPayPersonNewSet = new LJSPayPersonSet();

  //实收个人交费表
  private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();

  //实收集体交费表
 // private LJAPayGrpBL mLJAPayGrpBL = new LJAPayGrpBL();
  private LJAPayGrpSet mLJAPayGrpSet=new LJAPayGrpSet();
  //实收总表
  private LJAPayBL mLJAPayBL = new LJAPayBL();
 // private double realsumTotalMoney;
  //个人保单表
  private LCPolSet mLCPolSet = new LCPolSet();
  private LCContSet mLCContSet=new LCContSet();
  //集体保单表
//  private LCGrpContSchema  tLCGrpContSchema = new LCGrpContSchema();//取值用
  private LCGrpContSchema  mLCGrpContSchema = new LCGrpContSchema();
  private LCGrpContSchema  mLCGrpContSchemaNew = new LCGrpContSchema();
  private LCGrpPolBL mLCGrpPolBL = new LCGrpPolBL();
  private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
 // private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();//保存用
  //保费项表
  private LCPremSet mLCPremSet = new LCPremSet();
//  private LCPremSet mLCPremNewSet = new LCPremSet();

  //保险责任表LCDuty
  private LCDutySet mLCDutySet = new LCDutySet();

  //保险帐户表
  private LCInsureAccSet mLCInsureAccSet = new LCInsureAccSet();

  //保险帐户表记价履历表
  private LCInsureAccTraceSet mLCInsureAccTraceSet = new LCInsureAccTraceSet();
  private TransferData mTransferData=new TransferData();
  private MMap mMMap = new MMap();
  private VData mResult = new VData();
  //业务处理相关变量
  public NormPayGrpChooseOperBL() {
  }

  public static void main(String[] args) {
    GlobalInput mGI = new GlobalInput();
    mGI.Operator = "001";
    mGI.ComCode = "86";
    mGI.ManageCom = "86";

  //  LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
  LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
  tLCGrpContSchema.setGrpContNo("9028000000008088");
  tLCGrpContSchema.setManageCom(mGI.ManageCom);
  tLCGrpContSchema.setOperator(mGI.Operator);
   // tLCGrpPolSchema.setGrpPolNo("86110020030220000054");
   // tLCGrpPolSchema.setPaytoDate("2003-9-14");
   // tLCGrpPolSchema.setManageCom("86110000");
   // tLCGrpPolSchema.setOperator("001");
   // tLCGrpPolSchema.setManageFeeRate(0);
   TransferData tTransferData=new TransferData();
  tTransferData.setNameAndValue("SubmitPayDate","2005-6-14");
  tTransferData.setNameAndValue("ManageFeeRate","0");

    NormPayGrpChooseOperBL tNormPayGrpChooseOperBL = new NormPayGrpChooseOperBL();
    VData tVData = new VData();
    tVData.add(tLCGrpContSchema);
    tVData.add(tTransferData);
    tNormPayGrpChooseOperBL.submitData(tVData, "VERIFY");
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    //this.mOperate = cOperate;
    //add by guo xiang
    this.mOperate = verifyOperate(cOperate);

    if (mOperate.equals("")) {
      CError tError = new CError();
      tError.moduleName = "NormPayGrpChooseOperBL";
      tError.functionName = "submitData";
      tError.errorMessage = "传输数据失败!";
      this.mErrors.addOneError(tError);

      return false;
    }

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    System.out.println("After getinputdata");

    //进行业务处理
    if (!dealData()) {
      return false;
    }
    System.out.println("After dealData！");
    if(!prepareData())
      return false;
    PubSubmit tPubSubmit = new PubSubmit();
    if (tPubSubmit.submitData(mResult, "") == false)
      {
        // System.out.println("delete fail!");
        // CError.buildErr(this, "系统处理数据异常！");
        this.mErrors.copyAllErrors(tPubSubmit.mErrors);
        return false;
      }

    //准备往后台的数据
   /* if (!prepareOutputData()) {
      return false;
    }*/
  //  System.out.println("After prepareOutputData");

   // System.out.println("Start NormPayGrpChooseOper BL Submit...");
/*
    NormPayGrpChooseOperBLS tNormPayGrpChooseOperBLS = new
        NormPayGrpChooseOperBLS();
    tNormPayGrpChooseOperBLS.submitData(mInputData, cOperate);

    System.out.println("End LJNormPayGrpChooseOper BL Submit...");

    //如果有需要处理的错误，则返回
    if (tNormPayGrpChooseOperBLS.mErrors.needDealError()) {
      this.mErrors.copyAllErrors(tNormPayGrpChooseOperBLS.mErrors);
    }
*/
    mInputData = null;

    return true;
  }
  private boolean prepareData()
  {
    mMMap.put(mLJSPayPersonSet,"DELETE");//把所有保存时生成的应收个人记录删除，包括inputflag为0的
    mMMap.put(mLJAPayPersonSet,"INSERT");
    mMMap.put(mLJAPayGrpSet,"INSERT");
    mMMap.put(mLJAPayBL.getSchema(),"INSERT");
    mMMap.put(mLCPremSet,"UPDATE");
    mMMap.put(mLCDutySet,"UPDATE");
    mMMap.put(mLCPolSet,"UPDATE");
    mMMap.put(mLCContSet,"UPDATE");
    mMMap.put(mLCGrpPolSet,"UPDATE");
  //  mMMap.put(mLCGrpContSchema,"UPDATE");
    mMMap.put(mLCGrpContSchemaNew,"UPDATE");
    mMMap.put(mLJTempFeeBL.getSchema(),"UPDATE");
    mMMap.put(mLJTempFeeClassNewSet,"UPDATE");
    mResult.add(mMMap);
    return true;
  }
  /**
   * 20050725更新dealData的处理逻辑
   * @return boolean
   */
  private boolean dealData()
  {
    String GrpContNo = mLCGrpContSchema.getGrpContNo();
    String Operator = mLCGrpContSchema.getOperator();
    String ManageCom = mLCGrpContSchema.getManageCom();

    String tLimit = PubFun.getNoLimit(ManageCom);
    String  serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);

      //产生交费收据号
          tLimit = PubFun.getNoLimit(ManageCom);
      //tLimit=PubFun.getNoLimit(mLJTempFeeBL.getManageCom());
    String  payNo = PubFun1.CreateMaxNo("PAYNO", tLimit);
    String PayDate = (String)mTransferData.getValueByName("SubmitPayDate");
    //获取团体合同信息
    LCGrpContDB tLCGrpContDB=new LCGrpContDB();
    tLCGrpContDB.setGrpContNo(GrpContNo);
    if(!tLCGrpContDB.getInfo())
    {
      CError.buildErr(this, "团单："+GrpContNo+"系统无相关信息！");
      return false;
    }
    mLCGrpContSchemaNew.setSchema(tLCGrpContDB.getSchema());
    //判断是否已交费并得到财务确认！
    LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
    String tempFeeSqlStr = "select * from LJTempFee where OtherNo='" + GrpContNo
              + "' and ConfFlag='0'";

    // sqlStr = sqlStr + " and ConfFlag='0' ";
    LJTempFeeSet tLJTempFeeSet=new LJTempFeeSet();
    tLJTempFeeSet=tLJTempFeeDB.executeQuery(tempFeeSqlStr);
    if(tLJTempFeeSet.size()==0)
    {
      CError.buildErr(this, "查询暂交费信息时未发现相关数据，请去财务核对！");
      return false;
    }
    mLJTempFeeBL.setSchema(tLJTempFeeSet.get(1).getSchema());
    //下面判断它是否已经财务确认
    if(mLJTempFeeBL.getEnterAccDate()==null)
    {
      CError.buildErr(this,"财务还没有到帐，不能核销！");
      return false;
    }
    /*if(mLJTempFeeBL.getFinanceDate()==null)
    {
      CError.buildErr(this,"财务还没作会计确认，不能核销！");
      return false;
    }*/
    //下面处理ljtempfeeclass的记录
    LJTempFeeClassDB tLJTempFeeClassDB=new LJTempFeeClassDB();
    //mLJTempFeeClassSet
    tLJTempFeeClassDB.setTempFeeNo(mLJTempFeeBL.getTempFeeNo());
    tLJTempFeeClassDB.setConfFlag("0");
    mLJTempFeeClassSet=tLJTempFeeClassDB.query();
    if(mLJTempFeeClassSet.size()==0)
    {
      CError.buildErr(this,"暂交费分类记录缺失，请与财务处确认！");
      return false;
    }
    //准备 保存时产生的应收个人记录，准备核销时将其删除！
    LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
   // LJSPayPersonSet tLJSPayPersonSet=new LJSPayPersonSet();
    String payPersonSqlStr="select * from ljspayperson where GrpContNo='"+GrpContNo+"'";
    mLJSPayPersonSet=tLJSPayPersonDB.executeQuery(payPersonSqlStr);
    if(mLJSPayPersonSet.size()==0)
    {
      CError.buildErr(this, "团单下还没产生应收个人信息，请先保存页面上的选中记录再核销！");
      return false;
    }
    String payPersonSqlStr2="select * from ljspayperson where GrpContNo='"+GrpContNo+"'"
             +" and InputFlag='1'"; //判断是否有选中的记录
    LJSPayPersonSet tSelLJSPayPersonSet=new LJSPayPersonSet();
    tSelLJSPayPersonSet=tLJSPayPersonDB.executeQuery(payPersonSqlStr2);
    if(tSelLJSPayPersonSet.size()==0)
    {
      CError.buildErr(this, "团单下没有保存标记为1的应收个人信息，请选中相关记录保存后再核销！");
      return false;
    }
    LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
    LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
    String grpPolSqlStr = "select * from LCGrpPol where GrpContNo='" + GrpContNo
          + "' and Payintv=-1 "
          + "and GrpPolNo not in (select GrpPolNo from LJSPayGrp)"
          +" and GrpPolNo in (select GrpPolNo from LJSPayPerson where InputFlag='1')";
    tLCGrpPolSet= tLCGrpPolDB.executeQuery(grpPolSqlStr);
    if(tLCGrpPolSet.size()==0)
    {
      CError.buildErr(this, "团单："+GrpContNo+"下没有要催收的团体险种信息！");
      return false;
    }
    double grpContPremTotal=0.0;
    for(int i=1;i<=tLCGrpPolSet.size();i++)
    {
      String grpPolNo=tLCGrpPolSet. get(i).getGrpPolNo();
      LCGrpPolBL tLCGrpPolBL=new LCGrpPolBL();
      tLCGrpPolBL.setSchema(tLCGrpPolSet.get(i).getSchema());
      int payGrpCount=0;//团体险种的交费次数
      double grpPolPremTotal=0.0;
      //获取lcpol
      LCPolDB tLCPolDB=new LCPolDB();
      LCPolSet tLCPolSet=new LCPolSet();
      String polSqlStr="select * from lcpol where grpcontno='"+GrpContNo+"'"
          +" and grppolno='"+grpPolNo+"' and polno in (select polno from"
          +" ljspayperson where grpcontno='"+GrpContNo+"' and grppolno='"+grpPolNo
          +"' and InputFlag='1')";
      tLCPolSet=tLCPolDB.executeQuery(polSqlStr);
      if(tLCPolSet.size()>0)
      {
        for(int j=1;j<=tLCPolSet.size();j++)
        {
          String polNo=tLCPolSet. get(j).getPolNo();
          LCPolBL tLCPolBL=new LCPolBL();
          tLCPolBL.setSchema(tLCPolSet.get(j).getSchema());
          double polPremTotal=0.0;
          //获取lcduty
          LCDutyDB tLCDutyDB=new LCDutyDB();
          LCDutySet tLCDutySet=new LCDutySet();
          String dutySqlStr="select * from lcduty where polno='"+polNo+"' and "
              +"dutycode in (select dutycode from ljspayperson where polno='"+polNo+"'"
              +" and grpcontno='"+GrpContNo+"' and grppolno='"+grpPolNo+"' and InputFlag='1')";
          tLCDutySet=tLCDutyDB.executeQuery(dutySqlStr);
          if(tLCDutySet.size()>0)
          {
            for(int m=1;m<=tLCDutySet.size();m++)
            {
              String dutyCode=tLCDutySet. get(m).getDutyCode();
              double dutyPremTotal=0.0;//累计duty下的实际保费
              LCDutyBL tLCDutyBL=new LCDutyBL();
              tLCDutyBL.setSchema(tLCDutySet.get(m).getSchema());
              LCPremDB tLCPremDB=new LCPremDB();
              LCPremSet tLCPremSet=new LCPremSet();
              String premSqlStr="select * from lcprem where polno='"+polNo+"' and "
                  +"dutycode='"+dutyCode+"' and payplancode in (select payplancode"
                  +" from ljspayperson where polno='"+polNo+"' and dutycode='"+dutyCode
                  +"' and InputFlag='1')";
             tLCPremSet= tLCPremDB.executeQuery(premSqlStr);
             if(tLCPremSet.size()>0)
             {
               for(int n=1;n<=tLCPremSet.size();n++)
               {
                 String payPlanCode=tLCPremSet. get(n).getPayPlanCode();
                 LCPremBL tLCPremBL=new LCPremBL();
                 tLCPremBL.setSchema(tLCPremSet.get(n).getSchema());
                 //下面获取ljspayperson
                 LJSPayPersonDB tempLJSPayPersonDB=new LJSPayPersonDB();
                 LJSPayPersonSet tempLJSPayPersonSet=new LJSPayPersonSet();
                 tempLJSPayPersonDB.setPolNo(polNo);
                 tempLJSPayPersonDB.setDutyCode(dutyCode);
                 tempLJSPayPersonDB.setPayPlanCode(payPlanCode);
                 tempLJSPayPersonDB.setPayAimClass("2");
                 tempLJSPayPersonSet=tempLJSPayPersonDB.query();
                 LJSPayPersonBL tLJSPayPersonBL=new LJSPayPersonBL();
                 tLJSPayPersonBL.setSchema(tempLJSPayPersonSet.get(1).getSchema());
                 //下面应收转实收
                 LJAPayPersonBL tLJAPayPersonBL = new LJAPayPersonBL();
                 tLJAPayPersonBL.setPolNo(tLJSPayPersonBL.getPolNo()); //保单号码
              tLJAPayPersonBL.setPayCount(tLJSPayPersonBL.getPayCount()); //第几次交费
              tLJAPayPersonBL.setGrpPolNo(tLJSPayPersonBL.getGrpPolNo()); //集体保单号码
              tLJAPayPersonBL.setContNo(tLJSPayPersonBL.getContNo());
              tLJAPayPersonBL.setGrpContNo(tLJSPayPersonBL.getGrpContNo());
              tLJAPayPersonBL.setAppntNo(tLJSPayPersonBL.getAppntNo()); //投保人客户号码
              tLJAPayPersonBL.setPayNo(payNo); //交费收据号码
              tLJAPayPersonBL.setPayAimClass(tLJSPayPersonBL.
                      getPayAimClass()); //交费目的分类
              tLJAPayPersonBL.setDutyCode(tLJSPayPersonBL.getDutyCode()); //责任编码
              tLJAPayPersonBL.setPayPlanCode(tLJSPayPersonBL.
                      getPayPlanCode()); //交费计划编码
              tLJAPayPersonBL.setSumDuePayMoney(tLJSPayPersonBL
                      .getSumDuePayMoney()); //总应交金额
              tLJAPayPersonBL.setSumActuPayMoney(tLJSPayPersonBL
                      .getSumActuPayMoney()); //总实交金额
              tLJAPayPersonBL.setPayIntv(tLJSPayPersonBL.getPayIntv()); //交费间隔
              tLJAPayPersonBL.setPayDate(tLJSPayPersonBL.getPayDate()); //交费日期
              tLJAPayPersonBL.setPayType(tLJSPayPersonBL.getPayType()); //交费类型
              tLJAPayPersonBL.setEnterAccDate(mLJTempFeeBL.
                      getEnterAccDate()); //到帐日期
              tLJAPayPersonBL.setConfDate(CurrentDate); //确认日期
              tLJAPayPersonBL.setLastPayToDate(tLJSPayPersonBL
                      .getLastPayToDate()); //原交至日期
              tLJAPayPersonBL.setCurPayToDate(tLJSPayPersonBL.
                      getCurPayToDate()); //现交至日期
              tLJAPayPersonBL.setInInsuAccState(tLJSPayPersonBL
                      .getInInsuAccState()); //转入保险帐户状态
              tLJAPayPersonBL.setApproveCode(tLJSPayPersonBL.
                      getApproveCode()); //复核人编码
              tLJAPayPersonBL.setApproveDate(tLJSPayPersonBL.
                      getApproveDate()); //复核日期
              tLJAPayPersonBL.setSerialNo(serNo); //流水号
              tLJAPayPersonBL.setOperator(Operator); //操作员
              tLJAPayPersonBL.setMakeDate(CurrentDate); //入机日期
              tLJAPayPersonBL.setMakeTime(CurrentTime); //入机时间
              tLJAPayPersonBL.setGetNoticeNo(mLJTempFeeBL.getTempFeeNo()); //通知书号码
              tLJAPayPersonBL.setModifyDate(CurrentDate); //最后一次修改日期
              tLJAPayPersonBL.setModifyTime(CurrentTime); //最后一次修改时间
              tLJAPayPersonBL.setAgentGroup(tLJSPayPersonBL.getAgentGroup());
              tLJAPayPersonBL.setAgentCode(tLJSPayPersonBL.getAgentCode());
              tLJAPayPersonBL.setManageCom(tLJSPayPersonBL.getManageCom()); //管理机构
              tLJAPayPersonBL.setAgentCom(tLJSPayPersonBL.getAgentCom()); //代理机构
              tLJAPayPersonBL.setAgentType(tLJSPayPersonBL.getAgentType()); //代理机构内部分类
              tLJAPayPersonBL.setRiskCode(tLJSPayPersonBL.getRiskCode()); //险种编码
              mLJAPayPersonSet.add(tLJAPayPersonBL);

              //下面同时修改prem的相关字段
              tLCPremBL.setPayTimes(tLCPremBL.getPayTimes() + 1); //已交费次数
              tLCPremBL.setPrem(tLJSPayPersonBL.getSumActuPayMoney()); //实际保费
              tLCPremBL.setSumPrem(tLCPremBL.getSumPrem()
                                   + tLJSPayPersonBL.getSumActuPayMoney()); //累计保费
              tLCPremBL.setPaytoDate(CurrentDate); //交至日期
              tLCPremBL.setModifyDate(CurrentDate); //最后一次修改日期
              tLCPremBL.setModifyTime(CurrentTime); //最后一次修改时间
              mLCPremSet.add(tLCPremBL) ;
              dutyPremTotal+=tLJSPayPersonBL.getSumActuPayMoney();
              if(payGrpCount<tLJSPayPersonBL.getPayCount())
              {
                payGrpCount=tLJSPayPersonBL.getPayCount();//取最大的应交表交费次数
              }
               }
             }
             //下面修改lcduty的相关字段
             if(dutyPremTotal>0)
             {
               tLCDutyBL.setSumPrem(tLCDutyBL.getSumPrem() + dutyPremTotal);
               tLCDutyBL.setPaytoDate(CurrentDate);
               tLCDutyBL.setModifyDate(CurrentDate); //最后一次修改日期
               tLCDutyBL.setModifyTime(CurrentTime); //最后一次修改时间
               mLCDutySet.add(tLCDutyBL);
             }
             polPremTotal+=dutyPremTotal;
            }
          }
          //下面修改lcpol的相关字段信息
          tLCPolBL.setSumPrem(tLCPolBL.getSumPrem() + polPremTotal);
          tLCPolBL.setPaytoDate(CurrentDate);
          tLCPolBL.setModifyDate(CurrentDate); //最后一次修改日期
          tLCPolBL.setModifyTime(CurrentTime); //最后一次修改时间
          mLCPolSet.add(tLCPolBL);
          grpPolPremTotal+=polPremTotal;
        }
      }
      //下面更新lcgrppol的相关字段信息，同时生成ljapaygrp(实收集体表)的相关记录
      tLCGrpPolBL.setSumPrem(tLCGrpPolBL.getSumPrem()+grpPolPremTotal);
      tLCGrpPolBL.setPaytoDate(CurrentDate);
      tLCGrpPolBL.setModifyDate(CurrentDate);
      tLCGrpPolBL.setModifyTime(CurrentTime);
      mLCGrpPolSet.add(tLCGrpPolBL);
      //下面生成应收集体表
      LJAPayGrpBL tLJAPayGrpBL = new LJAPayGrpBL();
      tLJAPayGrpBL.setGrpPolNo(grpPolNo);
      tLJAPayGrpBL.setPayCount(payGrpCount); //交费次数
      tLJAPayGrpBL.setGrpContNo(tLCGrpPolBL.getGrpContNo());
      tLJAPayGrpBL.setAppntNo(mLCGrpContSchema.getAppntNo());
      tLJAPayGrpBL.setPayNo(payNo);
      tLJAPayGrpBL.setSumDuePayMoney(tLCGrpPolBL.getPrem()); //总应交金额
          tLJAPayGrpBL.setSumActuPayMoney(grpPolPremTotal); //总实交金额
          tLJAPayGrpBL.setPayIntv(mLCGrpPolBL.getPayIntv());
          tLJAPayGrpBL.setPayDate(PayDate);
          tLJAPayGrpBL.setPayType("ZC"); //repair:交费类型
          tLJAPayGrpBL.setEnterAccDate(mLJTempFeeBL.getEnterAccDate());
          tLJAPayGrpBL.setConfDate(CurrentDate); //确认日期
          tLJAPayGrpBL.setLastPayToDate(tLCGrpPolBL.getPaytoDate()); //原交至日期
          tLJAPayGrpBL.setCurPayToDate(CurrentDate);
          tLJAPayGrpBL.setApproveCode(tLCGrpPolBL.getApproveCode());
          tLJAPayGrpBL.setApproveDate(tLCGrpPolBL.getApproveDate());
          tLJAPayGrpBL.setSerialNo(serNo);
          tLJAPayGrpBL.setOperator(Operator);
          tLJAPayGrpBL.setMakeDate(CurrentDate);
          tLJAPayGrpBL.setMakeTime(CurrentTime);
          tLJAPayGrpBL.setGetNoticeNo(mLJTempFeeBL.getTempFeeNo()); //通知书号码
          tLJAPayGrpBL.setModifyDate(CurrentDate);
          tLJAPayGrpBL.setModifyTime(CurrentTime);
          tLJAPayGrpBL.setManageCom(tLCGrpPolBL.getManageCom()); //管理机构
          tLJAPayGrpBL.setAgentCom(tLCGrpPolBL.getAgentCom()); //代理机构
          tLJAPayGrpBL.setAgentType(tLCGrpPolBL.getAgentType()); //代理机构内部分类
          tLJAPayGrpBL.setRiskCode(tLCGrpPolBL.getRiskCode()); //险种编码
          tLJAPayGrpBL.setAgentCode(tLCGrpPolBL.getAgentCode());
          tLJAPayGrpBL.setAgentGroup(tLCGrpPolBL.getAgentGroup());
          mLJAPayGrpSet.add(tLJAPayGrpBL);
          grpContPremTotal+=grpPolPremTotal;//累计 团体合同 下的实交保费和
    }
    //下面更新lccont的相关信息   杨红于2005-07-25说明：lcgrppol与lccont是平行的，所以得单独用一个loop处理
    //更新lccont开始
    LCContDB tLCContDB=new LCContDB();
    LCContSet tLCContSet=new LCContSet();
    String contSqlStr="select * from lccont where grpcontno='"+GrpContNo+"'"
        +" and contno in (select contno from lcpol where grpcontno='"+GrpContNo+"'"
        +" and polno in (select polno from ljspayperson where grpcontno='"
        +GrpContNo+"' and inputflag='1'))";
    tLCContSet=tLCContDB.executeQuery(contSqlStr);
    if(tLCContSet.size()==0)
    {
      CError.buildErr(this,"团单下没有个单合同信息，不能核销！");
      return false;
    }
    for(int i=1;i<=tLCContSet.size();i++)
    {
      LCContBL tLCContBL=new LCContBL();
      tLCContBL.setSchema(tLCContSet.get(i).getSchema());
      String contNo=tLCContBL.getContNo();
      String sqlStr="select * from ljspayperson where contno='"+contNo+"' and grpcontno='"+GrpContNo+"'"
          +" and inputflag='1' and polno in (select polno from lccont where "
          +"contno='"+contNo+"')";
      LJSPayPersonDB tmpLJSPayPersonDB=new LJSPayPersonDB();
      LJSPayPersonSet tmpLJSPayPersonSet=new LJSPayPersonSet();
      tmpLJSPayPersonSet=tmpLJSPayPersonDB.executeQuery(sqlStr);
      double contPremTotal=0.0;
      if(tmpLJSPayPersonSet.size()>0)
      {
        for(int j=1;j<=tmpLJSPayPersonSet.size();j++)
        {
          double payMoney=tmpLJSPayPersonSet.get(j).getSumActuPayMoney();
          contPremTotal+=payMoney;
        }
      }
      tLCContBL.setSumPrem(tLCContBL.getSumPrem()+contPremTotal);
      tLCContBL.setPaytoDate(CurrentDate);
      tLCContBL.setModifyDate(CurrentDate); //最后一次修改日期
      tLCContBL.setModifyTime(CurrentTime);
      mLCContSet.add(tLCContBL);
    }
    //更新lccont结束
    //最后生成应收总表,同时修改lcgrpcont相关字段信息
    double dif=mLCGrpContSchemaNew.getDif();//第一次的余额
    if(dif+mLJTempFeeBL.getPayMoney()!=grpContPremTotal)
    {
      CError.buildErr(this, "财务交费金额为"+mLJTempFeeBL.getPayMoney()+",上次的余额为"
          +dif+",页面中保存数据的实交总金额为"+grpContPremTotal+",请更改保存数据的交费金额"
          +",使实交总金额为"+(mLJTempFeeBL.getPayMoney()+dif));
      return false;
    }
    mLCGrpContSchemaNew.setDif(dif+mLJTempFeeBL.getPayMoney()-grpContPremTotal);
    mLCGrpContSchemaNew.setPrem(grpContPremTotal);
    mLCGrpContSchemaNew.setSumPrem(mLCGrpContSchema.getSumPrem()+grpContPremTotal);
    mLCGrpContSchemaNew.setModifyDate(CurrentDate);
    mLCGrpContSchemaNew.setModifyTime(CurrentTime);
    //生成应收总表
          mLJAPayBL.setPayNo(payNo); //交费收据号码
          mLJAPayBL.setIncomeNo(mLJTempFeeBL.getOtherNo()); //应收/实收编号
          mLJAPayBL.setIncomeType("1"); //应收/实收编号类型
          mLJAPayBL.setAppntNo(mLCGrpContSchema.getAppntNo()); //  投保人客户号码
          mLJAPayBL.setSumActuPayMoney(grpContPremTotal); // 总实交金额
          mLJAPayBL.setGetNoticeNo(mLJTempFeeBL.getTempFeeNo()); //交费通知书号
          mLJAPayBL.setEnterAccDate(mLJTempFeeBL.getEnterAccDate()); // 到帐日期
          mLJAPayBL.setPayDate(PayDate); //交费日期
          mLJAPayBL.setConfDate(CurrentDate); //确认日期
          mLJAPayBL.setApproveCode(mLCGrpContSchema.getApproveCode()); //复核人编码
          mLJAPayBL.setApproveDate(mLCGrpContSchema.getApproveDate()); //  复核日期
          mLJAPayBL.setSerialNo(serNo); //流水号
          mLJAPayBL.setOperator(Operator); // 操作员
          mLJAPayBL.setMakeDate(CurrentDate); //入机时间
          mLJAPayBL.setMakeTime(CurrentTime); //入机时间
          mLJAPayBL.setModifyDate(CurrentDate); //最后一次修改日期
          mLJAPayBL.setModifyTime(CurrentTime); //最后一次修改时间
          mLJAPayBL.setRiskCode("000000"); // 险种编码
          mLJAPayBL.setManageCom(mLCGrpContSchema.getManageCom());
          mLJAPayBL.setAgentCode(mLCGrpContSchema.getAgentCode());
          mLJAPayBL.setAgentGroup(mLCGrpContSchema.getAgentGroup());
    //将暂交费表及分类表核销
    mLJTempFeeBL.setConfFlag("1");
    mLJTempFeeBL.setConfDate(CurrentDate);
    mLJTempFeeBL.setModifyDate(CurrentDate);
    mLJTempFeeBL.setModifyTime(CurrentTime);

    for(int i=1;i<=mLJTempFeeClassSet.size();i++)
    {
      LJTempFeeClassBL tLJTempFeeClassBL=new LJTempFeeClassBL();
      tLJTempFeeClassBL.setSchema(mLJTempFeeClassSet.get(i).getSchema());
      tLJTempFeeClassBL.setConfFlag("1");
      tLJTempFeeClassBL.setConfDate(CurrentDate);
      tLJTempFeeClassBL.setModifyDate(CurrentDate);
      tLJTempFeeClassBL.setModifyTime(CurrentTime);
      mLJTempFeeClassNewSet.add(tLJTempFeeClassBL);
    }
    return true;
  }
  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  /**
  private boolean dealData() {
    boolean tReturn = false;
    String sqlStr = "";
    String GetNoticeNo = ""; //通知书号
    String GrpContNo = tLCGrpContSchema.getGrpContNo();
    String Operator = tLCGrpContSchema.getOperator();
    String ManageCom = tLCGrpContSchema.getManageCom();
    LCGrpContDB mLCGrpContDB=new LCGrpContDB();
    mLCGrpContDB.setSchema(tLCGrpContSchema);
    mLCGrpContDB.getInfo();
    mLCGrpContSchema=mLCGrpContDB.getSchema();
    double balanceMoney=0;
    String PayDate = (String)mTransferData.getValueByName("SubmitPayDate"); //实际是页面接受的交费日期传进来
    try {
      //产生流水号
      tLimit = PubFun.getNoLimit(ManageCom);
      serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);

      //产生交费收据号
      tLimit = PubFun.getNoLimit(ManageCom);
      //tLimit=PubFun.getNoLimit(mLJTempFeeBL.getManageCom());
      payNo = PubFun1.CreateMaxNo("PAYNO", tLimit);

      //step one-查询数据
      //1-查询集体保单表

      LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
      LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();

      sqlStr = "select * from LCGrpPol where GrpContNo='" + GrpContNo
          + "' and Payintv=-1 ";
      sqlStr = sqlStr
          + "and GrpPolNo not in (select GrpPolNo from LJSPayGrp)"
          +" and GrpPolNo in (select GrpPolNo from LJSPayPerson)";
      System.out.println("sqlStr=" + sqlStr);//不定期没有催收，所以在应收集体表没有数据
      tLCGrpPolSet = tLCGrpPolDB.executeQuery(sqlStr);
      if (tLCGrpPolDB.mErrors.needDealError() == true) {
        // @@错误处理
        this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);

        CError tError = new CError();
        tError.moduleName = "NormPayGrpChooseOperBL";
        tError.functionName = "dealData";
        tError.errorMessage = "集体保单表查询失败!";
        this.mErrors.addOneError(tError);
        tLCGrpPolSet.clear();
        return false;
      }
      if (tLCGrpPolSet.size() < 1) {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "NormPayGrpChooseOperBL";
        tError.functionName = "dealData";
        tError.errorMessage = "集体保单表没有查询到相关纪录!";
        this.mErrors.addOneError(tError);
        tLCGrpPolSet.clear();

        return false;
      }
      //3 查询暂交费表，比较所交金额是否相等 --保存后用
      mLJTempFeeBL = new LJTempFeeBL();
      mLJTempFeeSet = new LJTempFeeSet();

      LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();

      sqlStr = "select * from LJTempFee where OtherNo='" + GrpContNo
               + "' ";
      sqlStr = sqlStr + " and ConfFlag='0' ";
      tLJTempFeeSet = tLJTempFeeDB.executeQuery(sqlStr);
      if (tLJTempFeeDB.mErrors.needDealError() == true)
      {
          // @@错误处理
          this.mErrors.copyAllErrors(tLJTempFeeDB.mErrors);

          CError tError = new CError();
          tError.moduleName = "NormPayGrpChooseOperBL";
          tError.functionName = "dealData";
          tError.errorMessage = "查询暂交费表失败!";
          this.mErrors.addOneError(tError);
          mLJTempFeeSet.clear();

          return false;
      }
      if ((tLJTempFeeSet.size() == 0) || (tLJTempFeeSet == null))
      {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "NormPayGrpChooseOperBL";
          tError.functionName = "dealData";
          tError.errorMessage = "查询暂交费表时未发现相关数据!请去财务核对";
          this.mErrors.addOneError(tError);
          mLJTempFeeSet.clear();

          return false;
      }
      mLJTempFeeBL.setSchema(tLJTempFeeSet.get(1).getSchema());
      GetNoticeNo = mLJTempFeeBL.getTempFeeNo();
      if (mLJTempFeeBL.getEnterAccDate() == null)
      {
          CError tError = new CError();
          tError.moduleName = "NormPayGrpChooseOperBL";
          tError.functionName = "dealData";
          tError.errorMessage = "财务缴费还没有到帐，不能签单!（暂交费收据号："
                                + mLJTempFeeBL.getTempFeeNo().trim()
                                + "）";
          this.mErrors.addOneError(tError);

          return false;
      }
//开始循环处理该合同号下的集体险种
      for(int z=1;z<=tLCGrpPolSet.size();z++)
      {
          //保存查询出来的集体保单表後用
          mLCGrpPolBL.setSchema(tLCGrpPolSet.get(z));
          mLCGrpPolBL.setPaytoDate(CurrentDate);
          mLCGrpPolBL.setModifyDate(CurrentDate);
          mLCGrpPolBL.setModifyTime(CurrentTime);
          System.out.println("查询应收表");
          String GrpPolNo = mLCGrpPolBL.getGrpPolNo();
          //2查询应收表：两个集合：1 符合集体保单号的应收纪录 2 符合集体保单号和录入标记的应收纪录
          //对集合2循环处理:更新对应的个人保单表，保费项表，填充实收表
          //对集合1添加到VData中，最后全部删除
          //2-1 得到应收个人集合1
          LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
          sqlStr = "select * from LJSPayPerson where GrpPolNo='" +
                   GrpPolNo
                   + "'";
          mLJSPayPersonSet = tLJSPayPersonDB.executeQuery(sqlStr);
          if (tLJSPayPersonDB.mErrors.needDealError() == true)
          {
              // @@错误处理
              this.mErrors.copyAllErrors(tLJSPayPersonDB.mErrors);

              CError tError = new CError();
              tError.moduleName = "NormPayGrpChooseOperBL";
              tError.functionName = "dealData";
              tError.errorMessage = "应收个人表查询失败!";
              this.mErrors.addOneError(tError);
              mLJSPayPersonSet.clear();

              return false;
          }
          if ((mLJSPayPersonSet.size() == 0) || (mLJSPayPersonSet == null))
          {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "NormPayGrpChooseOperBL";
              tError.functionName = "dealData";
              tError.errorMessage = "应收个人表没有查询到相关纪录!";
              this.mErrors.addOneError(tError);
              mLJSPayPersonSet.clear();

              return false;
          }
          System.out.println("2-2 得到应收个人集合2");

          //2-2 得到应收个人集合2
          tLJSPayPersonDB = new LJSPayPersonDB();

          LJSPayPersonSet tempLJSPayPersonSet = new LJSPayPersonSet();
          sqlStr = "select * from LJSPayPerson where GrpPolNo='" +
                   GrpPolNo
                   + "' ";
          sqlStr = sqlStr + "and InputFlag='1'";
          System.out.println("2-2 sqlStr:" + sqlStr);
          tempLJSPayPersonSet = tLJSPayPersonDB.executeQuery(sqlStr);
          if (tLJSPayPersonDB.mErrors.needDealError() == true)
          {
              // @@错误处理
              this.mErrors.copyAllErrors(tLJSPayPersonDB.mErrors);

              CError tError = new CError();
              tError.moduleName = "NormPayGrpChooseOperBL";
              tError.functionName = "dealData";
              tError.errorMessage = "应收个人表查询失败!";
              this.mErrors.addOneError(tError);
              tempLJSPayPersonSet.clear();

              return false;
          }
          if ((tempLJSPayPersonSet.size() == 0)
              || (tempLJSPayPersonSet == null))
          {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "NormPayGrpChooseOperBL";
              tError.functionName = "dealData";
              tError.errorMessage = "应收个人表没有查询到缴纳费用的用户!";
              this.mErrors.addOneError(tError);
              tempLJSPayPersonSet.clear();

              return false;
          }

          System.out.println("后面对集合2循环处理");

          DealAccount tDealAccount = new DealAccount();
          VData tempVData = new VData();

          //保险帐户表
          LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();

          //保险帐户表记价履历表
          LCInsureAccTraceSet tLCInsureAccTraceSet = new
                  LCInsureAccTraceSet();

          //后面对集合2循环处理
          LJSPayPersonBL tLJSPayPersonBL;
          double sumTotalMoney = 0;
          System.out.println("2-2 num:" + tempLJSPayPersonSet.size());
          for (int i = 1; i <= tempLJSPayPersonSet.size(); i++)
          {
              tLJSPayPersonBL = new LJSPayPersonBL();
              tLJSPayPersonBL.setSchema(tempLJSPayPersonSet.get(i));
              sumTotalMoney = sumTotalMoney
                              + tLJSPayPersonBL.getSumActuPayMoney();
          }
          //保存余额

          //-查询相关表并处理
          //4-循环查询录入标志=1的应收纪录，个人保单表，保费项表，填充实收个人表
          LCPremSet tLCPremSet;
          LCPremBL tLCPremBL;
          LCPremDB tLCPremDB;
          LJAPayPersonBL tLJAPayPersonBL;
          int payCount = 0;
          for (int i = 1; i <= tempLJSPayPersonSet.size(); i++)
          {
              tLJSPayPersonBL = new LJSPayPersonBL();
              tLJSPayPersonBL.setSchema(tempLJSPayPersonSet.get(i));
              System.out.println("查询保费项表");

              //4-2  查询保费项表,根据应收个人交费表查询该表项
              tLCPremDB = new LCPremDB();
              sqlStr = "select * from LCPrem where PolNo='"
                       + tLJSPayPersonBL.getPolNo() + "' ";
              sqlStr = sqlStr + " and DutyCode='"
                       + tLJSPayPersonBL.getDutyCode() + "' ";
              sqlStr = sqlStr + " and PayPlanCode='"
                       + tLJSPayPersonBL.getPayPlanCode() + "'";
              System.out.println("Sql=" + sqlStr);
              tLCPremSet = new LCPremSet();
              tLCPremSet = tLCPremDB.executeQuery(sqlStr);
              if (tLCPremDB.mErrors.needDealError() == true)
              {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLCPremDB.mErrors);

                  CError tError = new CError();
                  tError.moduleName = "NormPayGrpChooseOperBL";
                  tError.functionName = "dealData";
                  tError.errorMessage = "保费项表查询失败!";
                  this.mErrors.addOneError(tError);
                  tLCPremSet.clear();
                  mLCPremSet.clear();

                  return false;
              }
              if ((tLCPremSet.size() == 0) || (tLCPremSet == null))
              {
                  // @@错误处理
                  CError tError = new CError();
                  tError.moduleName = "NormPayGrpChooseOperBL";
                  tError.functionName = "dealData";
                  tError.errorMessage = "保费项表没有查询到相关纪录!";
                  this.mErrors.addOneError(tError);
                  tLCPremSet.clear();
                  mLCPremSet.clear();

                  return false;
              }
              tLCPremBL = new LCPremBL();
              tLCPremBL.setSchema(tLCPremSet.get(1).getSchema());
              tLCPremBL.setPayTimes(tLCPremBL.getPayTimes() + 1); //已交费次数
              tLCPremBL.setPrem(tLJSPayPersonBL.getSumActuPayMoney()); //实际保费
              tLCPremBL.setSumPrem(tLCPremBL.getSumPrem()
                                   + tLJSPayPersonBL.getSumActuPayMoney()); //累计保费
              tLCPremBL.setPaytoDate(CurrentDate); //交至日期
              tLCPremBL.setModifyDate(CurrentDate); //最后一次修改日期
              tLCPremBL.setModifyTime(CurrentTime); //最后一次修改时间
              mLCPremSet.add(tLCPremBL);

              //处理帐户
              if (mOperate.equals("VERIFY"))
              {
                  //普通账户
                  tempVData = tDealAccount.addPrem(tLCPremBL.getSchema(),
                          "2", tLCPremBL.getPolNo(),
                          "1", "BF", null);

                  //tempVData = tDealAccount.addPrem(tLCPremSet.get(1).getSchema(),"2",GetNoticeNo,"2","BF",null);
              }
              if (mOperate.equals("HEATHVERIFY"))
              {
                  //for health 健康险
                  tempVData = tDealAccount.addPremHealth(tLCPremBL.
                          getSchema(),
                          "2",
                          tLCPremBL.getPolNo(),
                          "1", "BF", null);
              }
              if (tempVData != null)
              {
                  tempVData = tDealAccount.updateLCInsureAccTraceDate(
                          mLJTempFeeBL
                          .getPayDate(),
                          tempVData);
                  if (tempVData == null)
                  {
                      // @@错误处理
                      CError tError = new CError();
                      tError.moduleName = "IndiFinUrgeVerifyBL";
                      tError.functionName = "dealData";
                      tError.errorMessage = "修改保险帐户表记价履历表纪录的交费日期时出错!";
                      this.mErrors.addOneError(tError);

                      return false;
                  }
                  tLCInsureAccSet = (LCInsureAccSet) tempVData
                                    .getObjectByObjectName(
                          "LCInsureAccSet", 0);
                  tLCInsureAccTraceSet = (LCInsureAccTraceSet) tempVData
                                         .getObjectByObjectName(
                          "LCInsureAccTraceSet",
                          0);
                  mLCInsureAccSet.add(tLCInsureAccSet);
                  mLCInsureAccTraceSet.add(tLCInsureAccTraceSet);
              }

              System.out.println("填充实收个人表");

              //4-3填充实收个人表
              tLJAPayPersonBL = new LJAPayPersonBL();
              tLJAPayPersonBL.setPolNo(tLJSPayPersonBL.getPolNo()); //保单号码
              tLJAPayPersonBL.setPayCount(tLJSPayPersonBL.getPayCount()); //第几次交费
              tLJAPayPersonBL.setGrpPolNo(tLJSPayPersonBL.getGrpPolNo()); //集体保单号码
              System.out.println("总单/合同号码 :ContNo不能为空");
              tLJAPayPersonBL.setContNo(tLJSPayPersonBL.getContNo());
              tLJAPayPersonBL.setGrpContNo(tLJSPayPersonBL.getGrpContNo());
              tLJAPayPersonBL.setAppntNo(tLJSPayPersonBL.getAppntNo()); //投保人客户号码
              tLJAPayPersonBL.setPayNo(payNo); //交费收据号码
              tLJAPayPersonBL.setPayAimClass(tLJSPayPersonBL.
                      getPayAimClass()); //交费目的分类
              tLJAPayPersonBL.setDutyCode(tLJSPayPersonBL.getDutyCode()); //责任编码
              tLJAPayPersonBL.setPayPlanCode(tLJSPayPersonBL.
                      getPayPlanCode()); //交费计划编码
              tLJAPayPersonBL.setSumDuePayMoney(tLJSPayPersonBL
                      .getSumDuePayMoney()); //总应交金额
              tLJAPayPersonBL.setSumActuPayMoney(tLJSPayPersonBL
                      .getSumActuPayMoney()); //总实交金额
              tLJAPayPersonBL.setPayIntv(tLJSPayPersonBL.getPayIntv()); //交费间隔
              tLJAPayPersonBL.setPayDate(tLJSPayPersonBL.getPayDate()); //交费日期
              tLJAPayPersonBL.setPayType(tLJSPayPersonBL.getPayType()); //交费类型
              tLJAPayPersonBL.setEnterAccDate(mLJTempFeeBL.
                      getEnterAccDate()); //到帐日期
              tLJAPayPersonBL.setConfDate(CurrentDate); //确认日期
              tLJAPayPersonBL.setLastPayToDate(tLJSPayPersonBL
                      .getLastPayToDate()); //原交至日期
              tLJAPayPersonBL.setCurPayToDate(tLJSPayPersonBL.
                      getCurPayToDate()); //现交至日期
              tLJAPayPersonBL.setInInsuAccState(tLJSPayPersonBL
                      .getInInsuAccState()); //转入保险帐户状态
              tLJAPayPersonBL.setApproveCode(tLJSPayPersonBL.
                      getApproveCode()); //复核人编码
              tLJAPayPersonBL.setApproveDate(tLJSPayPersonBL.
                      getApproveDate()); //复核日期
              tLJAPayPersonBL.setSerialNo(serNo); //流水号
              tLJAPayPersonBL.setOperator(Operator); //操作员
              tLJAPayPersonBL.setMakeDate(CurrentDate); //入机日期
              tLJAPayPersonBL.setMakeTime(CurrentTime); //入机时间
              tLJAPayPersonBL.setGetNoticeNo(GetNoticeNo); //通知书号码
              tLJAPayPersonBL.setModifyDate(CurrentDate); //最后一次修改日期
              tLJAPayPersonBL.setModifyTime(CurrentTime); //最后一次修改时间
              tLJAPayPersonBL.setAgentGroup(mLCGrpPolBL.getAgentGroup());
              tLJAPayPersonBL.setAgentCode(mLCGrpPolBL.getAgentCode());

              tLJAPayPersonBL.setManageCom(tLJSPayPersonBL.getManageCom()); //管理机构
              tLJAPayPersonBL.setAgentCom(tLJSPayPersonBL.getAgentCom()); //代理机构
              tLJAPayPersonBL.setAgentType(tLJSPayPersonBL.getAgentType()); //代理机构内部分类
              tLJAPayPersonBL.setRiskCode(tLJSPayPersonBL.getRiskCode()); //险种编码

              mLJAPayPersonSet.add(tLJAPayPersonBL);

              if (payCount < tLJSPayPersonBL.getPayCount())
              {
                  payCount = tLJSPayPersonBL.getPayCount();
              }
          }
          //end for 4


          System.out.println("7-集体保单表和暂交费表数据填充实收集体表");

          //7-集体保单表和暂交费表数据填充实收集体表
          mLJAPayGrpBL = new LJAPayGrpBL();
          mLJAPayGrpBL.setGrpPolNo(GrpPolNo);
          mLJAPayGrpBL.setPayCount(payCount); //交费次数
          //Lis5.3 upgrade get
                       mLJAPayGrpBL.setContNo(mLCGrpPolBL.getContNo());
           //
          mLJAPayGrpBL.setAppntNo(mLCGrpPolBL.getGrpProposalNo());
          mLJAPayGrpBL.setPayNo(payNo);

          //     mLJAPayGrpBL.setEndorsementNo(); //批单号码?
          mLJAPayGrpBL.setSumDuePayMoney(mLCGrpPolBL.getPrem()); //总应交金额
          mLJAPayGrpBL.setSumActuPayMoney(sumTotalMoney); //总实交金额
          mLJAPayGrpBL.setPayIntv(mLCGrpPolBL.getPayIntv());
          mLJAPayGrpBL.setPayDate(PayDate);
          mLJAPayGrpBL.setPayType("ZC"); //repair:交费类型
          mLJAPayGrpBL.setEnterAccDate(mLJTempFeeBL.getEnterAccDate());
          mLJAPayGrpBL.setConfDate(CurrentDate); //确认日期
          mLJAPayGrpBL.setLastPayToDate(mLCGrpPolBL.getPaytoDate()); //原交至日期
          mLJAPayGrpBL.setCurPayToDate(CurrentDate);
          mLJAPayGrpBL.setApproveCode(mLCGrpPolBL.getApproveCode());
          mLJAPayGrpBL.setApproveDate(mLCGrpPolBL.getApproveDate());
          mLJAPayGrpBL.setSerialNo(serNo);
          mLJAPayGrpBL.setOperator(Operator);
          mLJAPayGrpBL.setMakeDate(CurrentDate);
          mLJAPayGrpBL.setMakeTime(CurrentTime);
          mLJAPayGrpBL.setGetNoticeNo(GetNoticeNo); //通知书号码
          mLJAPayGrpBL.setModifyDate(CurrentDate);
          mLJAPayGrpBL.setModifyTime(CurrentTime);

          mLJAPayGrpBL.setManageCom(mLCGrpPolBL.getManageCom()); //管理机构
          mLJAPayGrpBL.setAgentCom(mLCGrpPolBL.getAgentCom()); //代理机构
          mLJAPayGrpBL.setAgentType(mLCGrpPolBL.getAgentType()); //代理机构内部分类
          mLJAPayGrpBL.setRiskCode(mLCGrpPolBL.getRiskCode()); //险种编码
          mLJAPayGrpBL.setAgentCode(mLCGrpPolBL.getAgentCode());
          mLJAPayGrpBL.setAgentGroup(mLCGrpPolBL.getAgentGroup());

          //8-更新保单表字段，取第一个应收个人交费纪录
          LCPolDB tLCPolDB = new LCPolDB();
          LCPolSet tLCPolSet = new LCPolSet();

          //查出在应收纪录中录入标记为1的集体下个人保单项
          sqlStr = "select * from LCPol where PolNo in (select PolNo from LJSPayPerson where GrpPolNo='"
                   + GrpPolNo + "' and InputFlag='1')";
          System.out.println("sqlStr=" + sqlStr);
          tLCPolSet = tLCPolDB.executeQuery(sqlStr);
          if (tLCPolDB.mErrors.needDealError() == true)
          {
              // @@错误处理
              this.mErrors.copyAllErrors(tLCPolDB.mErrors);

              CError tError = new CError();
              tError.moduleName = "NormPayCollOperBL";
              tError.functionName = "dealData";
              tError.errorMessage = "个人保单表查询失败!";
              this.mErrors.addOneError(tError);
              tLCPolSet.clear();

              return false;
          }
          if (tLCPolSet.size() < 1)
          {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "NormPayCollOperBL";
              tError.functionName = "dealData";
              tError.errorMessage = "个人保单表没有查询到相关纪录!";
              this.mErrors.addOneError(tError);
              tLCPolSet.clear();

              return false;
          }

          LCPolBL tLCPolBL;
          LJSPayPersonSet tLJSPayPersonSet = new LJSPayPersonSet();
          tLJSPayPersonBL = new LJSPayPersonBL();

          double sumMoney = 0;
          for (int i = 1; i <= tLCPolSet.size(); i++)
          {
              tLCPolBL = new LCPolBL();
              tLCPolBL.setSchema(tLCPolSet.get(i));

              //下面循环得到个人保单下应收款的总和
              sqlStr = "select * from LJSPayPerson where PolNo='"
                       + tLCPolBL.getPolNo() + "'";
              System.out.println("sqlStr:" + sqlStr);
              tLJSPayPersonDB = new LJSPayPersonDB();
              tLJSPayPersonSet = tLJSPayPersonDB.executeQuery(sqlStr);
              if (tLJSPayPersonDB.mErrors.needDealError() == true)
              {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLJSPayPersonDB.mErrors);

                  CError tError = new CError();
                  tError.moduleName = "NormPayCollOperBL";
                  tError.functionName = "dealData";
                  tError.errorMessage = "个人保单表查询失败!";
                  this.mErrors.addOneError(tError);
                  tLJSPayPersonSet.clear();

                  return false;
              }
              sumMoney = 0;
              for (int n = 1; n <= tLJSPayPersonSet.size(); n++)
              {
                  tLJSPayPersonBL.setSchema(tLJSPayPersonSet.get(n));
                  sumMoney = sumMoney +
                             tLJSPayPersonBL.getSumActuPayMoney();
              }
              System.out.println("sumMoney 2 =" + sumMoney);
              tLCPolBL.setSumPrem(tLCPolBL.getSumPrem() + sumMoney);
              tLCPolBL.setPaytoDate(CurrentDate);
              tLCPolBL.setModifyDate(CurrentDate); //最后一次修改日期
              tLCPolBL.setModifyTime(CurrentTime); //最后一次修改时间
              mLCPolSet.add(tLCPolBL);
          }
          tReturn = true;

          //9-更新保险责任表
          LCDutyBL tLCDutyBL;
          LCDutyDB tLCDutyDB = new LCDutyDB();
          LCDutySet tLCDutySet = new LCDutySet();

          //查出在应收纪录中录入标记为1的集体下保险责任表项
          sqlStr = "select * from LCDuty where PolNo in (select PolNo from LJSPayPerson where GrpPolNo='"
                   + GrpPolNo + "' and InputFlag='1')";
          System.out.println("sqlStr=" + sqlStr);
          tLCDutySet = tLCDutyDB.executeQuery(sqlStr);
          if (tLCDutyDB.mErrors.needDealError() == true)
          {
              // @@错误处理
              this.mErrors.copyAllErrors(tLCDutyDB.mErrors);

              CError tError = new CError();
              tError.moduleName = "NormPayCollOperBL";
              tError.functionName = "dealData";
              tError.errorMessage = "保险责任表查询失败!";
              this.mErrors.addOneError(tError);
              tLCDutySet.clear();

              return false;
          }
          if (tLCDutySet.size() < 1)
          {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "NormPayCollOperBL";
              tError.functionName = "dealData";
              tError.errorMessage = "保险责任表没有查询到相关纪录!";
              this.mErrors.addOneError(tError);
              tLCDutySet.clear();

              return false;
          }
          tLJSPayPersonSet = new LJSPayPersonSet();
          tLJSPayPersonBL = new LJSPayPersonBL();
          for (int i = 1; i <= tLCDutySet.size(); i++)
          {
              tLCDutyBL = new LCDutyBL();
              tLCDutyBL.setSchema(tLCDutySet.get(i));

              //下面循环得到保险责任表下应收款的总和
              sqlStr = "select * from LJSPayPerson where PolNo='"
                       + tLCDutyBL.getPolNo() + "' and DutyCode='"
                       + tLCDutyBL.getDutyCode() + "'";
              ;
              tLJSPayPersonDB = new LJSPayPersonDB();
              tLJSPayPersonSet = tLJSPayPersonDB.executeQuery(sqlStr);
              if (tLJSPayPersonDB.mErrors.needDealError() == true)
              {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLJSPayPersonDB.mErrors);

                  CError tError = new CError();
                  tError.moduleName = "NormPayCollOperBL";
                  tError.functionName = "dealData";
                  tError.errorMessage = "保险责任表查询失败!";
                  this.mErrors.addOneError(tError);
                  tLJSPayPersonSet.clear();

                  return false;
              }
              sumMoney = 0;
              for (int n = 1; n <= tLJSPayPersonSet.size(); n++)
              {
                  tLJSPayPersonBL.setSchema(tLJSPayPersonSet.get(n));
                  sumMoney = sumMoney +
                             tLJSPayPersonBL.getSumActuPayMoney();
              }
              tLCDutyBL.setSumPrem(tLCDutyBL.getSumPrem() + sumMoney);

              tLCDutyBL.setPaytoDate(CurrentDate);
              tLCDutyBL.setModifyDate(CurrentDate); //最后一次修改日期
              tLCDutyBL.setModifyTime(CurrentTime); //最后一次修改时间
              mLCDutySet.add(tLCDutyBL);
          }
          realsumTotalMoney=realsumTotalMoney+sumTotalMoney;
          mLCGrpPolSet.add(mLCGrpPolBL);
      }
      //保存集体保单的余额
      balanceMoney = mLCGrpContSchema.getDif();
      System.out.println("集体保单余额：" + balanceMoney);
      System.out.println("暂交费金额：" + mLJTempFeeBL.getPayMoney());
      System.out.println("缴纳金额：" + realsumTotalMoney);

      //比较交费金额  暂交费金额+余额>=应收金额
      if ((mLJTempFeeBL.getPayMoney() + balanceMoney) < realsumTotalMoney)
      {
          // @@错误处理
          CError tError = new CError();
          tError.moduleName = "NormPayGrpChooseOperBL";
          tError.functionName = "dealData";
          tError.errorMessage = "交费金额不足!";
          this.mErrors.addOneError(tError);
          mLJTempFeeSet.clear();

          return false;
      }
      //-暂交费表核销标志置为1
      mLJTempFeeBL.setConfFlag("1"); //核销标志置为1
      mLJTempFeeBL.setConfDate(CurrentDate);
      mLJTempFeeBL.setModifyDate(CurrentDate);
      mLJTempFeeBL.setModifyTime(CurrentTime);

      mLCGrpContSchema.setDif((mLJTempFeeBL.getPayMoney() + balanceMoney)
                             - realsumTotalMoney);
      mLCGrpContSchema.setPrem(realsumTotalMoney);
      mLCGrpContSchema.setSumPrem(mLCGrpContSchema.getSumPrem()+realsumTotalMoney);
          System.out.println("查询暂交费分类表");
          //5-查询暂交费分类表
          LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
          sqlStr = "select * from LJTempFeeClass where TempFeeNo='"
                   + mLJTempFeeBL.getTempFeeNo() + "'";
          sqlStr = sqlStr + " and ConfFlag='0'";
          System.out.println("sqlStr:" + sqlStr);
          mLJTempFeeClassSet = tLJTempFeeClassDB.executeQuery(sqlStr);
          if (tLJTempFeeClassDB.mErrors.needDealError() == true)
          {
              // @@错误处理
              this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);

              CError tError = new CError();
              tError.moduleName = "NormPayGrpChooseOperBL";
              tError.functionName = "dealData";
              tError.errorMessage = "暂交费分类表表查询失败!";
              this.mErrors.addOneError(tError);
              mLJTempFeeClassSet.clear();

              return false;
          }
          System.out.println("num:" + mLJTempFeeClassSet.size());
          if ((mLJTempFeeClassSet.size() == 0)
              || (mLJTempFeeClassSet == null))
          {
              // @@错误处理
              CError tError = new CError();
              tError.moduleName = "NormPayGrpChooseOperBL";
              tError.functionName = "dealData";
              tError.errorMessage = "暂交费分类表表没有查询到相关纪录!";
              this.mErrors.addOneError(tError);
              mLJTempFeeClassSet.clear();

              return false;
          }
          //-暂交费分类表，核销标志置为1
          LJTempFeeClassBL tLJTempFeeClassBL;
          for (int i = 1; i <= mLJTempFeeClassSet.size(); i++)
          {
              tLJTempFeeClassBL = new LJTempFeeClassBL();
              tLJTempFeeClassBL.setSchema(mLJTempFeeClassSet.get(i).
                                          getSchema());
              tLJTempFeeClassBL.setConfFlag("1"); //核销标志置为1
              tLJTempFeeClassBL.setConfDate(CurrentDate);
              tLJTempFeeClassBL.setModifyDate(CurrentDate);
              tLJTempFeeClassBL.setModifyTime(CurrentTime);
              mLJTempFeeClassNewSet.add(tLJTempFeeClassBL);
          }

          System.out.println("6-集体保单表和暂交费表数据填充实收总表");

          //6-集体保单表和暂交费表数据填充实收总表
          mLJAPayBL = new LJAPayBL();
          mLJAPayBL.setPayNo(payNo); //交费收据号码
          mLJAPayBL.setIncomeNo(mLJTempFeeBL.getOtherNo()); //应收/实收编号
          mLJAPayBL.setIncomeType(mLJTempFeeBL.getOtherNoType()); //应收/实收编号类型
          mLJAPayBL.setAppntNo(mLCGrpPolBL.getGrpProposalNo()); //  投保人客户号码
          mLJAPayBL.setSumActuPayMoney(realsumTotalMoney); // 总实交金额
          mLJAPayBL.setGetNoticeNo(GetNoticeNo); //交费通知书号
          mLJAPayBL.setEnterAccDate(mLJTempFeeBL.getEnterAccDate()); // 到帐日期
          mLJAPayBL.setPayDate(PayDate); //交费日期
          mLJAPayBL.setConfDate(mLJTempFeeBL.getConfDate()); //确认日期
          mLJAPayBL.setApproveCode(mLCGrpPolBL.getApproveCode()); //复核人编码
          mLJAPayBL.setApproveDate(mLCGrpPolBL.getApproveDate()); //  复核日期
          mLJAPayBL.setSerialNo(serNo); //流水号
          mLJAPayBL.setOperator(Operator); // 操作员
          mLJAPayBL.setMakeDate(CurrentDate); //入机时间
          mLJAPayBL.setMakeTime(CurrentTime); //入机时间
          mLJAPayBL.setModifyDate(CurrentDate); //最后一次修改日期
          mLJAPayBL.setModifyTime(CurrentTime); //最后一次修改时间

          //Lis5.3 upgrade set
                 mLJAPayBL.setBankCode(mLCGrpPolBL.getBankCode()); //银行编码
           mLJAPayBL.setBankAccNo(mLCGrpPolBL.getBankAccNo()); //银行帐号

          mLJAPayBL.setRiskCode(mLCGrpPolBL.getRiskCode()); // 险种编码
          mLJAPayBL.setManageCom(mLCGrpPolBL.getManageCom());
          mLJAPayBL.setAgentCode(mLCGrpPolBL.getAgentCode());
          mLJAPayBL.setAgentGroup(mLCGrpPolBL.getAgentGroup());
      tReturn = true;
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "NormPayGrpChooseOperBL";
      tError.functionName = "dealData";
      tError.errorMessage = "在处理后层所需要的数据时出错。";
      this.mErrors.addOneError(tError);

      return false;
    }

    return true;
  }*/

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData mInputData) {
    //应收总表
   // System.out.println("in getinputdata");
    mLCGrpContSchema.setSchema( (LCGrpContSchema) mInputData.getObjectByObjectName(
        "LCGrpContSchema",
        0));
    mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData",0);

    if (mTransferData == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "NormPayGrpChooseOperBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "没有得到足够的数据，请您确认!";
      this.mErrors.addOneError(tError);

      return false;
    }

    return true;
  }

  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData() {
    mInputData = new VData();
    try {
      mInputData.add(mLCGrpContSchema); //集体合同表
      mInputData.add(mLJAPayBL); //实收总表
     // mInputData.add(mLJAPayGrpBL); //实收集体表
      mInputData.add(mLCGrpPolSet); //集体保单险种表
      mInputData.add(mLJTempFeeSet); //暂交费表
      mInputData.add(mLJTempFeeClassNewSet); //暂交费分类表
      mInputData.add(mLJAPayPersonSet); //实收个人表
      mInputData.add(mLJSPayPersonSet); //应收个人交费表
      mInputData.add(mLCPolSet); //个人保单险种表
      mInputData.add(mLCPremSet); //保费项表
      mInputData.add(mLCDutySet); //保险责任表
      mInputData.add(mLCInsureAccSet); //保险帐户表
      mInputData.add(mLCInsureAccTraceSet); //保险帐户表记价履历表
      System.out.println("prepareOutputData:");
    }
    catch (Exception ex) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "NormPayGrpChooseOperBL";
      tError.functionName = "prepareData";
      tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
      this.mErrors.addOneError(tError);

      return false;
    }

    return true;
  }

  private String verifyOperate(String szOperate) {
    String szReturn = "";
    String[] szOperates = {
        "HEATHVERIFY", "VERIFY"};

    for (int nIndex = 0; nIndex < szOperates.length; nIndex++) {
      if (szOperate.equals(szOperates[nIndex])) {
        szReturn = szOperate;
      }
    }

    return szReturn;
  }
}
