package com.sinosoft.lis.operfee;

import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.vschema.LDCodeSet;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GRnewPayListPrintBL {

    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = new TransferData();
    private XmlExport xmlexport = null;
    private VData mResult = null;
    private SSRS mSSRS;
    private String mPayDate = ""; //应收止期，批量核销
    private String mGrpContNo = ""; //保单号，个案核销
    private String mVerifyType = ""; //1：批量核销；2：个案核销


    public GRnewPayListPrintBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //取得外部传入数据
        if (!getInputData(cInputData)) {
            return false;
        }

        //校验数据合法性
        if (!getListData()) {
            return false;
        }

        //获取打印所需数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        try {

            mGlobalInput = (GlobalInput) cInputData.
                           getObjectByObjectName("GlobalInput", 0);
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
        } catch (Exception e) {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("传入的数据不完整，" + e.toString());
            return false;
        }
        mPayDate = (String) mTransferData.getValueByName("PayDate");
        mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
        mVerifyType = (String) mTransferData.getValueByName("VerifyType");

        return true;
    }

    /**
     * 查询列表显示数据
     * @param schema LCContSchema
     * @return String
     */

    private boolean getListData() {
        StringBuffer sqlSB = new StringBuffer();
        if (mVerifyType.equals("2")) {
            sqlSB.append("select a.GrpContNo,b.GrpName,a.GetNoticeNo,sum(a.SumDuePayMoney),min(a.LastPayToDate),b.Dif,(select max(payDate) from LJTempFee where TempFeeNo= a.GetNoticeNo and ConfFlag='0')")
                    .append(" ,(select codename from ldcode where codetype='paymode' and code=b.PayMode),min(a.CurPayToDate),a.AgentCode,c.Operator")
                    .append("  from LJSPayPerson a ,LCGrpCont  b,LJSPay c ")
                    .append(" where a.GrpContNo='")
                    .append(mGrpContNo)
                    .append("'")
                    .append(" and b.PayIntv=0 and b.AppFlag='1' and c.othernotype='1' and c.OtherNo=b.GrpContNo")
                    .append(
                            " and a.RiskCode = '162801'")
                    .append(" and a.GrpContNo=b.GrpContNo")
                    .append(" group by a.GrpContNo,b.GrpName,a.GetNoticeNo,a.AgentCode,b.GrpContNo,b.Dif,b.PayMode,c.Operator")
                    ;
        } else {
            sqlSB.append("select a.GrpContNo,b.GrpName,a.GetNoticeNo,c.SumDuePayMoney,min(a.LastPayToDate),b.Dif,(select max(payDate) from LJTempFee where TempFeeNo= a.GetNoticeNo and ConfFlag='0')")
                    .append(" ,(select codename from ldcode where codetype='paymode' and code=b.PayMode),min(a.CurPayToDate),a.AgentCode,c.Operator")
                    .append("  from LJSPayPerson a ,LCGrpCont  b,LJSPay c ")
                    .append(" where a.LastPayToDate <='")
                    .append(mPayDate)
                    .append("'")
                    .append(" and b.PayIntv=0 and b.AppFlag='1' and c.othernotype='1' and c.OtherNo=b.GrpContNo")
                    .append(
                            " and a.RiskCode = '162801'")
                    .append(" and a.GrpContNo=b.GrpContNo")
                    .append(" group by a.GrpContNo,b.GrpName,a.GetNoticeNo,a.AgentCode,b.GrpContNo,b.Dif,b.PayMode,c.SumDuePayMoney,c.Operator")
                    ;
        }

        String tSQL = sqlSB.toString();
        System.out.println(tSQL);
        ExeSQL tExeSQL = new ExeSQL();

        mSSRS = tExeSQL.execSQL(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "creatFile";
            tError.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 获取打印所需数据
     * @param schema LCContSchema
     * @return String
     */
    private boolean getPrintData() {
        TextTag tag = new TextTag();
        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("GrpNormPayList.vts", "printer"); //最好紧接着就初始化xml文档

        //得到公司名
        String grpName = this.getCompanyName();
        System.out.println("ManageName" + grpName);
        if (grpName == null) {
            return false;
        }
        tag.add("ManageName", grpName);
        xmlexport.addTextTag(tag);

        String[] title = {"保单号", "投保人", "应收记录号", "应收金额", "应收日期",
                         "保费帐户余额", "收费日期","收费方式", "下一应收日期","业务员代码", "操作人"};
        xmlexport.addListTable(getListTable(), title);
        //xmlexport.outputDocumentToFile("C:\\", "GrpNormPayList");
        mResult = new VData();
        mResult.addElement(xmlexport);

        return true;
    }

    /**
     *  获取列表数据
     * @param schema LCContSchema
     * @return String
     */
    private ListTable getListTable() {
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[11];
            for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
                info[j - 1] = mSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }

        tListTable.setName("ZT");
        return tListTable;
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


    public static void main(String[] args)
    {
        GRnewPayListPrintBL p = new GRnewPayListPrintBL();
        LPEdorAppSchema schema = new LPEdorAppSchema();
        schema.setEdorAcceptNo("20050915000022");

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("GrpContNo", "0000000703");
        tTransferData.setNameAndValue("VerifyType", "2");

        VData v = new VData();
        v.add(schema);
        v.add(tTransferData);
        if (!p.submitData(v, "")) {
            System.out.println(p.mErrors.getErrContent());
        } else {
            System.out.println("OK");
        }
    }

    private void jbInit() throws Exception {
    }

    //查询公司名称
    private String getCompanyName() {
        //通过LDCode得到保单号，然后得到公司名称
        String manageCode = mGlobalInput.ManageCom;
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("station");
        tLDCodeDB.setCode(manageCode);
        LDCodeSet set = tLDCodeDB.query();
        if (set == null || set.size() == 0) {

            mErrors.addOneError("没有查到公司名");
            System.out.println("没有查到公司名" + tLDCodeDB.mErrors.getErrContent());
            return null;
        } else {
            return set.get(1).getCodeName();
        }
    }

}
