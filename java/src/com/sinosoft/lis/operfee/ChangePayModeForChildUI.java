package com.sinosoft.lis.operfee;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class ChangePayModeForChildUI {

    public CErrors mErrors = new CErrors();

    public ChangePayModeForChildUI() {
    }


    /**
     * 提交变更给付方式接口
     * @param data VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData data, String operate) {
        ChangePayModeForChildBL tChangePayModeForChildBL = new ChangePayModeForChildBL();

        if (!tChangePayModeForChildBL.submitData(data, operate)) {
            mErrors.copyAllErrors(tChangePayModeForChildBL.mErrors);
            return false;
        }

        return true;
    }

}
