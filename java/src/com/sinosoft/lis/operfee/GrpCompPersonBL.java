package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;
import java.lang.String;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class GrpCompPersonBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private GlobalInput tGI = new GlobalInput();

    /** 数据操作字符串 */
    private TransferData mTransferData = new TransferData();
    /** 数据表  保存数据*/
    //集体保单表
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    //应收总表集
    //应收总表集
    private LCAddDecInsuredSet mLCAddDecInsuredSet = new LCAddDecInsuredSet();

    //针对一个集体单可能对应多个集体险种单的情况
    //缴费通知书

    private MMap map = new MMap();
    private MMap mapPer = new MMap();
    private String mOpertor = "INSERT";
    private String mGrpContNo = "";
    private String mBankCode = "";
    private String mBankAccNo = "";
    private String mBankAccName = "";
    private String mCValidate = "";
    private String mCount = "2"; //续期期数
    private String mAddSql; //增人sql
    private String mDecSql; //减人sql
    private String mDleSql; //删除临时表sql
    private String mContPlan; //保障计划号
    private String mBirthDaySql; //更新出生日期
    private String mSexSql; //更新性别
    private String mDelInsuSql; //删除比对错误数据
    private String mFinishSql; //设置完成标志


    public GrpCompPersonBL() {
    }

    public static void main(String[] args) {

        GrpCompPersonBL GrpCompPersonBL1 = new GrpCompPersonBL();
        GlobalInput tGt = new GlobalInput();
        tGt.ComCode = "86";
        tGt.Operator = "wuser";
        tGt.ManageCom = "86";
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema.setGrpContNo("0000004801");
        //tLCGrpContSchema.setPrtNo(PrtNo);//Yangh于2005-07-19将以前的注销
        TransferData tempTransferData = new TransferData();
        tempTransferData.setNameAndValue("BankCode", "ddd");
        tempTransferData.setNameAndValue("BankAccNo", "dfdfdf");
        tempTransferData.setNameAndValue("BankAccName", "wanglong");
        tempTransferData.setNameAndValue("CValidate", "2005-01-01");
        VData tVData = new VData();
        tVData.add(tLCGrpContSchema);
        tVData.add(tGt);
        tVData.add(tempTransferData);

        //  LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
        //  tLCGrpPolSchema.setGrpPolNo("86110020040220000102");
        // VData tv = new VData();
        //  tv.add(tLCGrpPolSchema);
        // tv.add(tGt);
        GrpCompPersonBL1.submitData(tVData, "INSERT");
    }

    // 为更新数据库准备数据
    public boolean prepareData() {
        mInputData = new VData();
        map = new MMap();
        map.put(mBirthDaySql, "UPDATE");
        map.put(mSexSql, "UPDATE");
        map.put(mFinishSql, "UPDATE");
        map.put(mDleSql, "DELETE");

        mInputData.add(map);
        return true;
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {

        System.out.println("GrpCompPersonBL Begin......");

        if (!getInputData(cInputData)) {
            return false;
        }
        if (!checkData()) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        if (!prepareData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(mInputData, "") == false) {

            this.mErrors.addOneError("PubSubmit:删除客户临时表失败!");

            return false;
        }

        return true;
    }

    /**
     * Yangh于2005-07-19创建新的dealData的处理逻辑
     * @return boolean
     */
    private boolean dealData() {

        StringBuffer tSBql = new StringBuffer(128);
        tSBql.append(
                " select count(*) from LCAddDecInsured where GrpContNo='")
                .append(mGrpContNo)
                .append("' and BatchNo='")
                .append(mCount)
                .append("' and FinishFlag='1' with ur")
                ;
        String strInsu = tSBql.toString();
        ExeSQL tExeSQL = new ExeSQL();
        String strCount = tExeSQL.getOneValue(strInsu);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "DealData";
            tError.errorMessage = "查询数据 LCAddDecInsured 出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (strCount != null && strCount != "0") {
            tSBql = new StringBuffer(128);
            tSBql.append(
                    " delete from LCAddDecInsured where GrpContNo='")
                    .append(mGrpContNo)
                    .append("' and BatchNo='")
                    .append(mCount)
                    .append("'  ")
                    ;
            mDelInsuSql = tSBql.toString();
            if(!deleteInsured()){
                return false;
            }
        }
        //下面查找保障计划
        String InsuSql =
                "select ContPlanCode from LCInsured where GrpContNo='" +
                mGrpContNo + "' fetch first row only with ur";

        tExeSQL = new ExeSQL();
        mContPlan = tExeSQL.getOneValue(InsuSql);
        if (tExeSQL.mErrors.needDealError()) {
            CError.buildErr(this, "查询数据 LCInsured 出错！");
            return false;
        }
        if (mContPlan == null) {
            CError.buildErr(this, "团单下的保障计划号缺失！");
            return false;
        }

        //取被拨人序号
        int iCount = 0;
        tSBql = new StringBuffer(128);
        tSBql.append(
                " SELECT max(int(InsuredID)) from LCAddDecInsured  where GrpContNo='")
                .append(mGrpContNo)
                .append("' and BatchNo='")
                .append(mCount)
                .append("' with ur")
                ;
        tExeSQL = new ExeSQL();
        String strCnt = tSBql.toString();
        String strInsuCount = tExeSQL.getOneValue(strCnt);
        if (tExeSQL.mErrors.needDealError()) {
            CError.buildErr(this, "查询数据 LCAddDecInsured 出错！");
            return false;
        }
        if (strInsuCount == null || strInsuCount == "" || strInsuCount == "0") {
            iCount = 0;
        } else {
            iCount = Integer.parseInt(strInsuCount);
        }

        //增人处理
        tSBql = new StringBuffer(128);
        tSBql.append(" SELECT * from LDPersonTemp a")
                .append(
                        " WHERE not exists(select 'x' from LCInsured where OthIDNo=a.OthIDNo")
                .append(" and GrpContNo='")
                .append(mGrpContNo)
                .append("') and not exists(select 'x' from LCAddDecInsured where OthIDNo=a.OthIDNo and GrpContNo='")
                .append(mGrpContNo)
                .append("' and BatchNo='")
                .append(mCount)
                .append("') with ur")

                ;
        mAddSql = tSBql.toString();
        /** 新增方法:
         * 游标查询方法 */
        RSWrapper rswrapper = new RSWrapper();
        LDPersonTempSet tLDPersonTempSet = new LDPersonTempSet();
        rswrapper.prepareData(tLDPersonTempSet, mAddSql);
        do {
            //循环取得2000条数据，进行处理
            rswrapper.getData();

            for (int j = 1; j <= tLDPersonTempSet.size(); j++) {

                System.out.println("续期获取增人清单");
                mLCAddDecInsuredSet.clear();
                iCount += 1;
                LDPersonTempSchema tLDPersonTempSchema = new LDPersonTempSchema();
                tLDPersonTempSchema = tLDPersonTempSet.get(j);
                LCAddDecInsuredSchema tLCAddDecInsuredSchema = new
                        LCAddDecInsuredSchema();
                tLCAddDecInsuredSchema.setGrpContNo(mGrpContNo);
                tLCAddDecInsuredSchema.setInsuredID(Integer.toString(iCount));
                tLCAddDecInsuredSchema.setState("1");
                tLCAddDecInsuredSchema.setBatchNo(mCount);
                tLCAddDecInsuredSchema.setEmployeeName(tLDPersonTempSchema.
                        getName());
                tLCAddDecInsuredSchema.setInsuredName(tLDPersonTempSchema.
                        getName());
                tLCAddDecInsuredSchema.setRelation("00");
                tLCAddDecInsuredSchema.setSex(tLDPersonTempSchema.
                                              getSex());
                tLCAddDecInsuredSchema.setBirthday(tLDPersonTempSchema.
                        getBirthday());
                tLCAddDecInsuredSchema.setIDType("0");
                tLCAddDecInsuredSchema.setIDNo(tLDPersonTempSchema.
                                               getIDNo());
                tLCAddDecInsuredSchema.setContPlanCode(mContPlan);
                tLCAddDecInsuredSchema.setAddDecFlag("NI");
                tLCAddDecInsuredSchema.setOthIDNo(tLDPersonTempSchema.
                                                  getOthIDNo());
                tLCAddDecInsuredSchema.setBankCode(mBankCode);
                tLCAddDecInsuredSchema.setBankAccNo(mBankAccNo);
                tLCAddDecInsuredSchema.setAccName(mBankAccName);
                tLCAddDecInsuredSchema.setEdorValiDate(mCValidate);
                tLCAddDecInsuredSchema.setOperator(tGI.Operator);
                tLCAddDecInsuredSchema.setMakeDate(PubFun.getCurrentDate()); //入机日期
                tLCAddDecInsuredSchema.setMakeTime(PubFun.getCurrentTime()); //入机时间
                tLCAddDecInsuredSchema.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
                tLCAddDecInsuredSchema.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间
                // tLCAddDecInsuredSchema.setFinishFlag("1");
                mLCAddDecInsuredSet.add(tLCAddDecInsuredSchema);
                if (!dealInsured()) {
                    return false;
                }

            }
        } while (tLDPersonTempSet.size() > 0);

        //取被保人序号
        tSBql = new StringBuffer(128);
        tSBql.append(
                " SELECT max(int(InsuredID)) from LCAddDecInsured  where GrpContNo='")
                .append(mGrpContNo)
                .append("' and BatchNo='")
                .append(mCount)
                .append("' with ur")
                ;
        tExeSQL = new ExeSQL();
        strCnt = tSBql.toString();
        strCount = tExeSQL.getOneValue(strCnt);
        if (tExeSQL.mErrors.needDealError()) {
            CError.buildErr(this, "查询数据 LCAddDecInsured 出错！");
            return false;
        }
        if (strCount == null || strCount == "" || strCount == "0") {
            iCount = 0;
        } else {
            iCount = Integer.parseInt(strCount);
        }

        //减人处理
        tSBql = new StringBuffer(128);
        tSBql.append(" select * from LCInsured   a")
                .append(
                        " WHERE NOT exists(select 'x' from LDPersonTemp b where OthIDNo=a.OthIDNo)")
                .append(" and a.GrpContNo='")
                .append(mGrpContNo)
                .append("' and not exists(select 'x' from LCAddDecInsured where OthIDNo=a.OthIDNo and GrpContNo='")
                .append(mGrpContNo)
                .append("' and BatchNo='")
                .append(mCount)
                .append("') with ur")

                ;
        mDecSql = tSBql.toString();
        RSWrapper rswrapper1 = new RSWrapper();
        LCInsuredSet tLCInsuredSet = new LCInsuredSet();
        rswrapper1.prepareData(tLCInsuredSet, mDecSql);
        do {
            //循环取得2000条数据，进行处理
            rswrapper1.getData();
            for (int i = 1; i <= tLCInsuredSet.size(); i++) {

                System.out.println("续期获取减人清单");
                iCount += 1;
                mLCAddDecInsuredSet.clear();
                LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
                tLCInsuredSchema = tLCInsuredSet.get(i);
                LCAddDecInsuredSchema tLCAddDecInsuredSchema = new
                        LCAddDecInsuredSchema();
                tLCAddDecInsuredSchema.setGrpContNo(mGrpContNo);
                tLCAddDecInsuredSchema.setInsuredID(Integer.toString(iCount));
                tLCAddDecInsuredSchema.setState("1");
                tLCAddDecInsuredSchema.setInsuredNo(tLCInsuredSchema.
                        getInsuredNo());
                tLCAddDecInsuredSchema.setContNo(tLCInsuredSchema.getContNo());
                tLCAddDecInsuredSchema.setBatchNo(mCount);
                tLCAddDecInsuredSchema.setEmployeeName(tLCInsuredSchema.
                        getName());
                tLCAddDecInsuredSchema.setInsuredName(tLCInsuredSchema.
                        getName());
                tLCAddDecInsuredSchema.setRelation("00");
                tLCAddDecInsuredSchema.setSex(tLCInsuredSchema.
                                              getSex());
                tLCAddDecInsuredSchema.setBirthday(tLCInsuredSchema.
                        getBirthday());
                tLCAddDecInsuredSchema.setIDType(tLCInsuredSchema.
                                                 getIDType());
                tLCAddDecInsuredSchema.setIDNo(tLCInsuredSchema.
                                               getIDNo());
                tLCAddDecInsuredSchema.setContPlanCode(tLCInsuredSchema.
                        getContPlanCode());
                tLCAddDecInsuredSchema.setBankCode(tLCInsuredSchema.getBankCode());
                tLCAddDecInsuredSchema.setBankAccNo(tLCInsuredSchema.
                        getBankAccNo());
                tLCAddDecInsuredSchema.setAccName(tLCInsuredSchema.getAccName());
                tLCAddDecInsuredSchema.setEdorValiDate(mCValidate);
                tLCAddDecInsuredSchema.setAddDecFlag("ZT");
                tLCAddDecInsuredSchema.setOthIDNo(tLCInsuredSchema.getOthIDNo());
                tLCAddDecInsuredSchema.setOperator(tGI.Operator);
                tLCAddDecInsuredSchema.setMakeDate(PubFun.getCurrentDate()); //入机日期
                tLCAddDecInsuredSchema.setMakeTime(PubFun.getCurrentTime()); //入机时间
                tLCAddDecInsuredSchema.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
                tLCAddDecInsuredSchema.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间
                // tLCAddDecInsuredSchema.setFinishFlag("1");
                mLCAddDecInsuredSet.add(tLCAddDecInsuredSchema);
                if (!dealInsured()) {
                    return false;
                }

            }

        } while (tLCInsuredSet.size() > 0);

        //更新出生日期
        tSBql = new StringBuffer();
        tSBql.append(" update  lcadddecinsured  a set a.Birthday=")
                .append("(SELECT case when length(idno)=18 then date(substr(idno,7,4)||'-'||substr(idno,11,2)||'-'||substr(idno,13,2)) when length(idno)=15 then date('19'||substr(idno,7,2)||'-'||substr(idno,9,2)||'-'||substr(idno,11,2)) else date('1900-01-01') end ")
                .append(" from lcadddecinsured  where grpcontno='")
                .append(mGrpContNo)
                .append("' and BatchNo ='")
                .append(mCount)
                .append("' AND addDecFlag = 'NI' and OthIDNo=a.OthIDNo)")
                .append(" where a.grpcontno='")
                .append(mGrpContNo)
                .append("' and a.BatchNo ='")
                .append(mCount)
                .append("' AND a.addDecFlag = 'NI' ");
        mBirthDaySql = tSBql.toString();
        System.out.println(mBirthDaySql);
        //更新性别
        tSBql = new StringBuffer();
        tSBql.append(" update  lcadddecinsured  a set a.Sex=")
                .append(
                        "(SELECT case when Sex='1' then '0' when Sex='2' then '1' else '2' end ")
                .append(" from lcadddecinsured  where grpcontno='")
                .append(mGrpContNo)
                .append("' and BatchNo ='")
                .append(mCount)
                .append("'  AND addDecFlag = 'NI' and OthIDNo=a.OthIDNo)")
                .append(" where a.grpcontno='")
                .append(mGrpContNo)
                .append("' and a.BatchNo ='")
                .append(mCount)
                .append("' AND a.addDecFlag = 'NI'");

        mSexSql = tSBql.toString();
        System.out.println(mSexSql);

        //设置比对完成标志
        tSBql = new StringBuffer();
        tSBql.append("update  lcadddecinsured  a set a.FinishFlag= '1'")
                .append(" where a.grpcontno='")
                .append(mGrpContNo)
                .append("' and a.BatchNo ='")
                .append(mCount)
                .append("' ");

        mFinishSql = tSBql.toString();

        //删除客户临时表
        tSBql = new StringBuffer();
        tSBql.append("delete from LDPersonTemp ");
        mDleSql = tSBql.toString();

        return true;

    }

    /**
     * Yangh于2005-07-19创建新的dealData的处理逻辑
     * @return boolean
     */
    private boolean checkData() {

        System.out.println("GrpCompPersonBL dealData Begin");
        //下面查找LJSPayPerson的记录
        StringBuffer tSBql = new StringBuffer(128);
        tSBql.append(
                " select max(paycount)+1 from ljapaygrp where GrpContNo='")
                .append(mGrpContNo)
                .append("'")
                ;

        String strPerSQL = tSBql.toString();
        ExeSQL tExeSQL = new ExeSQL();
        mCount = tExeSQL.getOneValue(strPerSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "DealData";
            tError.errorMessage = "查询数据 mGrpContNo 出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (mCount == null || mCount == "" || mCount == "0") {
            CError.buildErr(this,
                            "团单号:" + mGrpContNo +
                            "比对错误，ljapaygrp表中缺少数据!");
            return false;
        }

        tSBql = new StringBuffer(128);
        tSBql.append(" select count(*) from LDPersonTemp with ur")
                ;
        String sqlComPerson = tSBql.toString();
        String strPerCount = tExeSQL.getOneValue(sqlComPerson);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "DealData";
            tError.errorMessage = "查询数据 LDPersonTemp 出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (strPerCount == null || strPerCount == "" || strPerCount == "0") {
            CError.buildErr(this,
                            "团单号:" + mGrpContNo +
                            "比对错误，请先导入被保人到LDPersonTemp表!");
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) {

        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput",
                0));
        mLCGrpContSchema = ((LCGrpContSchema) mInputData.
                            getObjectByObjectName(
                                    "LCGrpContSchema", 0));
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        if (tGI == null || mLCGrpContSchema == null || mTransferData == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpDueFeeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        if (tLCGrpContDB.getInfo() == false) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpDueFeeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCGrpContSchema.setSchema(tLCGrpContDB.getSchema());
        mGrpContNo = mLCGrpContSchema.getGrpContNo();
        mBankCode = (String) mTransferData.getValueByName("BankCode");
        mBankAccNo = (String) mTransferData.getValueByName("BankAccNo");
        mBankAccName = (String) mTransferData.getValueByName("BankAccName");
        mCValidate = (String) mTransferData.getValueByName("CValidate");
        return true;
    }

    // 为更新数据库准备数据

    /**
     * 加入到催收核销日志表数据
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT
     * @return
     */
    private boolean dealInsured() {
        MMap tMap = new MMap();
        tMap.put(mLCAddDecInsuredSet, "INSERT");
        VData tInputData = new VData();
        tInputData.add(tMap);

        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tInputData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理 LCAddDecInsured 表失败!");
            return false;
        }
        return true;
    }

    /**
     * 加入到催收核销日志表数据
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT
     * @return
     */
    private boolean deleteInsured() {
        MMap tMap = new MMap();
        tMap.put(mDelInsuSql, "DELETE");
        VData tInputData = new VData();
        tInputData.add(tMap);

        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tInputData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理 删除LCAddDecInsured 表失败!");
            return false;
        }
        return true;
    }
}
