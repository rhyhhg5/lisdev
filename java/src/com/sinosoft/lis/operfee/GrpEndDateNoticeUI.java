package com.sinosoft.lis.operfee;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class GrpEndDateNoticeUI
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public GrpEndDateNoticeUI()
    {
    }

    /**
     * 操作的提交方法，得到通知数据
     * @param cInputData VData：包括
     *   1.	Sql：页面查询清单的Sql语句。
     *   2. LCGrpContSet：选择的保单信息
     *   3. GlobalInput:对象，完整的登陆用户信息
     * @param cOperate String:操作方式，此为空
     * @return XmlExport：通知书数据，失败则为null
     */
    public XmlExport getXmlExport(VData cInputData, String cOperate)
    {
        GrpEndDateNoticeBL bl = new GrpEndDateNoticeBL();

        XmlExport xml = bl.getXmlExport(cInputData, cOperate);
        if(xml == null && bl.mErrors.needDealError())
        {
            mErrors.copyAllErrors(bl.mErrors);
            return null;
        }

        return xml;
    }

    /**
     * 调用getXmlExport得到通知数据，并存储到数据库
     *   1.	Sql：页面查询清单的Sql语句。
     *   2. LCGrpContSet：选择的保单信息
     *   3. GlobalInput:对象，完整的登陆用户信息
     * @param cOperate String:操作方式，此为空
     * @return boolean: 操作成功true，失败则为false
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        GrpEndDateNoticeBL bl = new GrpEndDateNoticeBL();
        if(!bl.submitData(cInputData, cOperate))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        GrpEndDateNoticeUI grpenddatenoticeui = new GrpEndDateNoticeUI();
    }
}
