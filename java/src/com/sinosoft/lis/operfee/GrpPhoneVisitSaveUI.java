package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class GrpPhoneVisitSaveUI {

    //业务处理相关变量
    private VData mInputData;
    public CErrors mErrors = new CErrors();

    public GrpPhoneVisitSaveUI() {
    }

    public static void main(String[] args) {

        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "001";
        tGI.ManageCom = "86";
        String tPayDate = "2005-12-10";
        String tPayMode = "4";
        TransferData tempTransferData = new TransferData();
        tempTransferData.setNameAndValue("PayDate", tPayDate);
        tempTransferData.setNameAndValue("PayMode", tPayMode);
        tempTransferData.setNameAndValue("GetNoticeNo", "31000002502");

        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema.setGrpContNo("0000000302");
        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tLCGrpContSchema);
        tVData.add(tempTransferData);
        GrpPhoneVisitSaveUI tGrpPhoneVisitSaveUI = new GrpPhoneVisitSaveUI();
        tGrpPhoneVisitSaveUI.submitData(tVData);

    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        GrpPhoneVisitSaveBL tGrpPhoneVisitSaveBL = new GrpPhoneVisitSaveBL();
        System.out.println("Start GrpPhoneVisitOver UI Submit...");
        tGrpPhoneVisitSaveBL.submitData(mInputData,"INSERT");

        System.out.println("End GrpPhoneVisitOver UI Submit...");

        mInputData = null;
        //如果有需要处理的错误，则返回
        if (tGrpPhoneVisitSaveBL.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tGrpPhoneVisitSaveBL.mErrors);
            return false;
        }
        System.out.println("error num=" + mErrors.getErrorCount());
        return true;
    }

}
