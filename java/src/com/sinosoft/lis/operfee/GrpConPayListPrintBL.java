package com.sinosoft.lis.operfee;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.TextTag;
import com.sinosoft.lis.pubfun.PubFun;

/**
 * <p>Title:GrpConPayListPrintBL </p>
 *
 * <p>Description:GrpConPayListPrintBL类文件 </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GrpConPayListPrintBL {

    public CErrors mErrors = new CErrors();

    private VData mResult = new VData();

    private VData mInputData = new VData();

    private String mOperate = "";

    private TransferData mTransferData = new TransferData();

    private GlobalInput mGlobalInput = new GlobalInput();

    private String SerialNo = ""; //批次号
    private String GrpContNo = ""; //保单号
    private String mSQL = ""; //查询语句

    private String ManageCom = "";

    private SSRS mSSRS = new SSRS();

    public GrpConPayListPrintBL() {
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.mInputData = cInputData;
        this.mOperate = cOperate;
        if (!mOperate.equals("PRINT")) {
            this.bulidError("submitData", "不支持的操作字符串");
            return false;
        }

        //外部传入数据
        if (!getInputData(mInputData)) {
            this.bulidError("getInputData", "数据不完整");
            return false;
        }

        //数据合法性
        if (!getListTable()) {
            return false;
        }

        //获取打印数据
        if (!getPrintData()) {
            return false;
        }

        return true;
    }

    /**
     * 往后台传送数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

        try {
            System.out.println("666666666666666666666666666666");
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName(
                                           "GlobalInput", 0));

            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
        } catch (Exception e) {
            this.mErrors.addOneError("数据不完整");
            return false;
        }
        System.out.println("7777777777777777777777777777777777");
        this.mSQL = (String) mTransferData.getValueByName("SQL");
        System.out.println(mSQL);
        this.ManageCom = mGlobalInput.ManageCom;

        System.out.println("8888888888888888888888888888888888");
        return true;
    }

    /**
     * 查询出来的数据
     * @return boolean
     */
    private boolean getListTable() {

        String sql =  mSQL;;

        ExeSQL tExeSQL = new ExeSQL();
        System.out.println( sql);
        mSSRS = tExeSQL.execSQL(sql.toString());
        if (tExeSQL.mErrors.needDealError()) {
            CError error = new CError();
            error.moduleName = "MakeXMLBL";
            error.functionName = "creatFile";
            error.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(error);

            return false;
        }

        return true;
    }

    /**
     * 将数据存放到XmlExport中
     * @return boolean
     */
    private boolean getPrintData() {

        XmlExport tXmlExport = new XmlExport();
        System.out.println("1010101010101010101010101010101010");
        tXmlExport.createDocument("GrpConPayList.vts", "printer");

        TextTag tTextTag = new TextTag();

        tTextTag.add("ManageName", this.getManageName()); //机构
        tTextTag.add("PrintDate", PubFun.getCurrentDate()); //打印时间
        tTextTag.add("PrintOperator", mGlobalInput.Operator); //打印人

        if (tTextTag.size() > 1) {
            tXmlExport.addTextTag(tTextTag);
        }

// updated by suyanlin 2007/11/21
//      String[] title = {"收据号", "投保人", "保单号", "应收金额", "应缴时间","实收金额",
//                         "核销时间", "经办人", "代理人姓名", "代理人性别", "代理人电话"};
        String[] title = {"收据号", "投保人", "保单号", "应收金额", "应缴时间","实收金额",
                         "核销时间", "经办人", "代理人姓名", "代理人性别", "代理人手机", "代理人固话"};

        tXmlExport.addListTable(this.getListTableData(), title);

//        tXmlExport.outputDocumentToFile("c:\\", "new");

        mResult.clear();

        mResult.add(tXmlExport);

        return true;
    }

    /**
     * 将数据存放到ListTable中
     * @return ListTable
     */
    private ListTable getListTableData() {
        ListTable tListTable = new ListTable();

        tListTable.setName("ZT");

        if (mSSRS.getMaxRow() > 0) {

            for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
// updated by suyanlin 2007/11/21
//              String info[] = new String[11];
                String info[] = new String[18];

                info[0] = mSSRS.GetText(i, 1);
                info[1] = mSSRS.GetText(i, 2);
                info[2] = mSSRS.GetText(i, 3);
                info[3] = mSSRS.GetText(i, 4);
                info[4] = mSSRS.GetText(i, 5);
                info[5] = mSSRS.GetText(i, 6);
                info[6] = mSSRS.GetText(i, 7);
                info[7] = mSSRS.GetText(i, 8);
                info[8] = mSSRS.GetText(i, 9);
                info[9] = mSSRS.GetText(i, 10);
                info[10] = mSSRS.GetText(i, 11);
// added by suyanlin 2007/11/21
                info[11] = mSSRS.GetText(i, 12);
                info[12] = mSSRS.GetText(i, 13);
                info[13] = mSSRS.GetText(i, 14);
                info[14] = mSSRS.GetText(i, 15);
                info[15] = mSSRS.GetText(i, 16);
                info[16] = mSSRS.GetText(i, 17);


                tListTable.add(info);

            }

        }

        return tListTable;
    }

    /**
     * 写入错误消息
     * @param cFunction String
     * @param cErrorMsg String
     */
    private void bulidError(String cFunction, String cErrorMsg) {
        CError error = new CError();

        error.moduleName = "GrpConPayListPrintBL";
        error.functionName = cFunction;
        error.errorMessage = cErrorMsg;

        this.mErrors.addOneError(error);

    }

    /**
     * 查询机构名称
     * @return String
     */
    private String getManageName() {
        String ManageName = "";
        String sql = "";

        sql = "select name from ldcom where comcode='" + ManageCom + "'";

        SSRS tSSRS = new SSRS();

        ExeSQL tExeSQL = new ExeSQL();

        tSSRS = tExeSQL.execSQL(sql);
        System.out.println(sql);

        if (tExeSQL.mErrors.needDealError()) {
            CError error = new CError();
            error.moduleName = "GrpConPayListPrintBL";
            error.functionName = "getManageName";
            error.errorMessage = "机构名不存在！";
            this.mErrors.addOneError(error);
        }

        if (tSSRS.getMaxRow() > 0) {
            System.out.println(tSSRS.GetText(1, 1));
            ManageName = tSSRS.GetText(1, 1);
        } else {
            CError error = new CError();
            error.moduleName = "GrpConPayListPrintBL";
            error.functionName = "getManageName";
            error.errorMessage = "机构名不存在！";
            this.mErrors.addOneError(error);
        }

        return ManageName;
    }

    public VData getResult() {
        return this.mResult;
    }

    public static void main(String[] args) {

    }
}
