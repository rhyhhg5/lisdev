package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class GrpLJSPlanCancelUI {

  //业务处理相关变量
  private VData mInputData ;
  public  CErrors mErrors=new CErrors();

  public GrpLJSPlanCancelUI() {
  }
  public static void main(String[] args) {
   // GrpLJSPlanCancelUI GrpDueFeeUI1 = new GrpLJSPlanCancelUI();
    GlobalInput tGI = new GlobalInput();
    tGI.ComCode="86";
    tGI.Operator="001";
    tGI.ManageCom="86";
    LJSPaySchema tLJSPaySchema = new LJSPaySchema();
    tLJSPaySchema.setGetNoticeNo("31000000145");

    TransferData tTransferData= new TransferData();
    tTransferData.setNameAndValue("CancelMode","0");
    GrpLJSPlanCancelUI tGrpLJSPlanCancelUI= new GrpLJSPlanCancelUI();
    VData tVData = new VData();
    tVData.add(tGI);
    tVData.add(tLJSPaySchema);
    tVData.add(tTransferData);

    tGrpLJSPlanCancelUI.submitData(tVData,"INSERT");

  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    GrpLJSPlanCancelBL tGrpLJSPlanCancelBL=new GrpLJSPlanCancelBL();
    System.out.println("Start GrpLJSPlanCancelBL UI Submit...");
    tGrpLJSPlanCancelBL.submitData(mInputData,cOperate);

    System.out.println("End GrpLJSPlanCancelBL UI Submit...");

    mInputData=null;
    //如果有需要处理的错误，则返回
    if (tGrpLJSPlanCancelBL.mErrors.needDealError() )
       {
       this.mErrors.copyAllErrors(tGrpLJSPlanCancelBL.mErrors ) ;
       return false;
       }
   System.out.println("error num="+mErrors.getErrorCount());
    return true;
  }

}
