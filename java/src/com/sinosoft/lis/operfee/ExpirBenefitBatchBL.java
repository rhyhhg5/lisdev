package com.sinosoft.lis.operfee;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

import java.lang.String;

import java.util.*;



/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */
public class ExpirBenefitBatchBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private GlobalInput tGI = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String StartDate = ""; //应收开始时间
    private String EndDate = ""; //应收结束时间
    private String mserNo = ""; //批次号

    private boolean mDuebySpecFlag = false;

    private TransferData mTransferData = new TransferData();

    public ExpirBenefitBatchBL() {
    }

    public static void main(String[] args) {

    }

    /**
     * @param cInputData VData，包含：
     * 1、	GlobalInput对象，完整的登陆用户信息
     * 2、	TransferData对象，包含StartDate， EndDate， ManageCom
     * @param cOperate String：操作类型，此可为空字符串“”
     * @return boolean：操作成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate) {
        this.mOperate = cOperate;
        //接收传入数据
        if (!getInputData(cInputData)) {
            return false;
        }
        //处理数据
        if (!dealData()) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return boolean
     */
    public boolean dealData() {
        //接收外部传入数据
        StartDate = (String) mTransferData.getValueByName("StartDate");
        EndDate = (String) mTransferData.getValueByName("EndDate");
        String manageCom = (String) mTransferData.getValueByName("ManageCom");
        String querySql = (String)mTransferData.getValueByName("QuerySql");
        String queryType = (String)mTransferData.getValueByName("QueryType");


        //批量抽档数据查询

        if(querySql == null || querySql.equals(""))
        {
            CError.buildErr(this, "后台程序没有接收到前台查询语句");
            return false;
        }
        System.out.println(querySql);

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(querySql);
        if (tSSRS == null || tSSRS.getMaxRow() == 0) {
            CError.buildErr(this, "系统中没有符合给付的保单信息！");
            return false;
        }

        //抽档批次号
        String tLimit = PubFun.getNoLimit(manageCom);
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        mserNo = serNo;
        if (queryType.equals("3"))
        {
            //循环处理数据
            for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            	
            	try
            	{
            		String contNo = tSSRS.GetText(i, 1);
                    LCContSchema tLCContSchema = new LCContSchema();
                    tLCContSchema.setContNo(contNo);

                    VData tVData = new VData();
                    tVData.add(tLCContSchema);
                    tVData.add(tGI);
                    tVData.add(serNo);
                    mTransferData.setNameAndValue("serNo", serNo);
                    tVData.add(mTransferData);

                    //针对保单进行抽档操作（例如：生成应收数据，应收抽档通知书等）
                    ExpirBenefitBL tExpirBenefitBL = new ExpirBenefitBL();
                    if (!tExpirBenefitBL.submitData(tVData, "INSERT")) {
                        CError.buildErr(this, "保单号为 " + contNo + " 的保单抽档失败："
                                        + tExpirBenefitBL.mErrors.getFirstError());
                        continue;
                    }
            	}
            	catch(Exception e)
            	{
            		
            	}
            }
        }
        else
        {
        	ArrayList contNos =null;
            for ( int i = 1; i<= tSSRS.getMaxRow() ; i++)
            {
            	try
            	{
            		boolean contIsSameFlag=false;
            		if(contNos==null)
                	{
                		contNos=new ArrayList();
                		contNos.add(tSSRS.GetText(i, 1));
                	}
                	else
                	{
            	    	for(int j=0;j<contNos.size();j++)
            	    	{    		
            	    		if(tSSRS.GetText(i, 1).equals(contNos.get(j)))
            	    		{
            	    			contIsSameFlag=true;
            	    		}
            	    		else
            	    		{
            	    			contNos.add(tSSRS.GetText(i, 1));
            	    		}
            	    		
            	    	}
                	}
            		
            		if(!contIsSameFlag)
            		{
	            		String contNo = tSSRS.GetText(i, 1);
	                    LCContSchema tLCContSchema = new LCContSchema();
	                    tLCContSchema.setContNo(contNo);
	
	                    VData tVData = new VData();
	                    tVData.add(tLCContSchema);
	                    tVData.add(tGI);
	                    tVData.add(serNo);
	                    mTransferData.setNameAndValue("serNo", serNo);
	                    tVData.add(mTransferData);
	
	                    ExpirBenefitGetBL  tExpirBenefitGetBL = new ExpirBenefitGetBL();
	                    if (!tExpirBenefitGetBL.submitData(tVData,"INSERT"))
	                    {
	                        CError.buildErr(this, "保单号为：" + contNo + "的保单催收失败:"
	                                        + tExpirBenefitGetBL.mErrors.getFirstError());
	                        continue;
	                    }
            		}
            		else
            		{
            			continue;
            		}
            	}
            	catch(Exception e)
            	{
            		
            	}
                
            }
        }
        return true;
    }

     /**
      * 从输入数据中得到所有对象
      * @param mInputData:
      *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
      */
     private boolean getInputData(VData mInputData) {
         tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));

         mTransferData = (TransferData) mInputData.getObjectByObjectName(
                 "TransferData",
                 0);
         System.out.println("mTransferData:" + mTransferData);

         if (tGI == null) {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "ExpirBenefitBatchBL";
             tError.functionName = "getInputData";
             tError.errorMessage = "没有得到足够的数据，请您确认!";
             this.mErrors.addOneError(tError);

             return false;
         }

         return true;
     }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData() {
        mInputData = new VData();

        return true;
    }

    public VData getResult() {
        VData t = new VData();
        t.add(0,mserNo);
        return t;
    }
    /**
        * 加入到日志表数据,记录因为各种原因抽档失败的单子
        * @param
        * @return boolean
        */
    private boolean dealErrorLog(LCPolSchema tLCPolSchema, String Error) {
           LGErrorLogSchema tLGErrorLogSchema = new LGErrorLogSchema();
           tLGErrorLogSchema.setSerialNo(mserNo);
           tLGErrorLogSchema.setErrorType("0002");
           tLGErrorLogSchema.setErrorTypeSub("01");
           tLGErrorLogSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
           tLGErrorLogSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
           tLGErrorLogSchema.setContNo(tLCPolSchema.getContNo());
           tLGErrorLogSchema.setPolNo(tLCPolSchema.getPolNo());
           tLGErrorLogSchema.setOperator(tGI.Operator);
           tLGErrorLogSchema.setMakeDate(CurrentDate);
           tLGErrorLogSchema.setMakeTime(CurrentTime);
           tLGErrorLogSchema.setModifyDate(CurrentDate);
           tLGErrorLogSchema.setModifyTime(CurrentTime);
           tLGErrorLogSchema.setDescribe(Error);

           MMap tMap = new MMap();
           tMap.put(tLGErrorLogSchema, "DELETE&INSERT");
           VData tInputData = new VData();
           tInputData.add(tMap);
           PubSubmit tPubSubmit = new PubSubmit();
           if (tPubSubmit.submitData(tInputData, "") == false) {
               this.mErrors.addOneError("PubSubmit:处理LGErrorLog 表失败!");
               return false;
           }
           return true;
    }

}
