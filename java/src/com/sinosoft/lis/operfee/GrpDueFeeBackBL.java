package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;
import java.lang.String;
import com.sinosoft.lis.bq.AppAcc;
import com.sinosoft.lis.llcase.LLCaseCommon;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Sinosoft </p>
 *
 * @author QLQ
 * @version 1.0
 */
public class GrpDueFeeBackBL {
    private FeeConst tDEAL = new FeeConst();
    public CErrors mErrors; //错误的容器。
    private GlobalInput mGlobalInput; //完整的操作员信息
    private String mCurrentDate; //当前日期
    private String mCurrentTime; //当前时间
    private VData mInputData = new VData();
    private MMap map = new MMap();
    //实收个人保费表
    private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();
    private LJAPayPersonSchema mLJAPayPersonSchema = new LJAPayPersonSchema();

    //实收团体交费表
    private LJAPayGrpSet mLJaPayGrpSet = new LJAPayGrpSet();
    private LJAPayGrpSchema mLJAPayGrpSchema = new LJAPayGrpSchema();
    private LJAPaySet mLJAPaySet = new LJAPaySet();
    private LJAPaySchema mLJAPaySchema = new LJAPaySchema();

    //应收总表备份表
    private LJSPayBSchema mLJSPayBSchema = new LJSPayBSchema();

    //应收总表集
//    private LJAPaySet tLJSPaySet = new LJAPaySet();
//    private LJAPayGrpSet mLJSPayGrpSet = new LJAPayGrpSet();
//    private LJAPayGrpSet mYelLJSPayGrpSet = new LJAPayGrpSet();

    private boolean flag = true;


    public GrpDueFeeBackBL() {
        mCurrentDate = PubFun.getCurrentDate();
        mCurrentTime = PubFun.getCurrentTime();
        mErrors = new CErrors();
    }

    public boolean submitData(VData cInputData, String cOperate) {
        if(!this.getInputData(cInputData))
        {
            return false;
        }
        if(!this.checkData())
        {
            //清除正在转出状态。
            if(flag == true)
            {
                ExeSQL tExeSQL = new ExeSQL();
                String sqlLock = " update ljspayb set DealState = '"
                                 + tDEAL.DEALSTATE_URGESUCCEED
                                 + "' where GetNoticeNo = '"
                                 + this.mLJSPayBSchema.getGetNoticeNo() + "'";
                if (!tExeSQL.execUpdateSQL(sqlLock))
                    this.addError("GrpDueFeeBackBL", "getInputData",
                                  "实收转出状态撤消错误!");
            }
            return false;
        }

        if(!this.dealData())
        {
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */

    private boolean getInputData(VData data) {
        mGlobalInput = (GlobalInput) data.getObject(1);
        mLJSPayBSchema = (LJSPayBSchema) data.getObject(0);
        if (mGlobalInput == null || mLJSPayBSchema == null) {
            // @@错误处理
            this.addError("GrpDueFeeBackBL","getInputData","没有得到足够的数据，请您确认!");
            return false;
        }
        LJSPayBSet tLJSPayBSet = new LJSPayBSet();
        LJSPayBDB tLJSPayBDB = mLJSPayBSchema.getDB();
        String sql = "select * from  LJSPayB where getnoticeno = '"
                     + mLJSPayBSchema.getGetNoticeNo()
                     +"'";
        tLJSPayBSet = tLJSPayBDB.executeQuery(sql);
        if (tLJSPayBDB.mErrors.needDealError()) {
        this.mErrors.addOneError("查询错误!");
        return false;
        }

        if (tLJSPayBSet == null || tLJSPayBSet.size() == 0) {
            // @@错误处理
            this.addError("GrpDueFeeBL","getInputData","没有得到足够的数据，请您确认!");
            return false;
        }
        mLJSPayBSchema.setSchema(tLJSPayBSet.get(1));
        return true;
    }


    private boolean checkData() {
        ExeSQL tExeSQL = new ExeSQL();
        if (mLJSPayBSchema.getDealState().equals(tDEAL.DEALSTATE_BACK) &&
            !checkErrorFlag()) {
            this.addError("GrpDueFeeBL", "checkData",
                          "您好，保单正在进行实收保费转出处理，不能再次执行。");
            flag = false;
            return false;
        }
        else
        {
            //清除错误标记
            if (!delErrorLog()) {
                this.setErrorLog();
                this.addError("", "dealdata", "清除处理错误标志操作失败!");
                return false;

            }

            //将表的处理状态标记改为 5  锁定记录
            if (!this.changeState(mLJSPayBSchema.getGetNoticeNo())) {
                this.addError("GrpDueFeeBackBL", "dealData", "更新记录状态错误，无法完成操作");
                return false;
            }
        }

        String sql = " select * from LJSPayB where otherno = '"
                     + mLJSPayBSchema.getOtherNo()
                     + "' and getnoticeno > '"
                     + mLJSPayBSchema.getGetNoticeNo()
                     + "' and dealstate in ('1','5') "
                     ;
        String result = tExeSQL.getOneValue(sql);
        if (!result.equals("")) {
            this.addError("GrpDueFeeBL", "checkDate",
                          "只能按实收保费核销时间从后往前的顺序进行实收保费转出，请您确认!");
            return false;
        }


        sql = " select * from ljspayperson where grpcontno = '"
              + mLJSPayBSchema.getOtherNo()
              + "' and getnoticeno > '"
              + mLJSPayBSchema.getGetNoticeNo()
              + "'"
              ;
        result = tExeSQL.getOneValue(sql);
        if (!result.equals("")) {
            this.addError("GrpDueFeeBL", "checkData",
                          "您好，保单正在做续期业务，不能转出。");
            return false;

        }

        //new校验是否有险种保单发生过理赔

//        sql = " select * from LLRegister a where RgtObjNo = '"
//              + mLJSPayBSchema.getOtherNo()
//              + "' and rgtdate >= "
//              + "       (select min(lastPayTodate) from LJAPayPerson "
//              + "       where grpContNo = '"
//              + mLJSPayBSchema.getOtherNo()
//              + "'          and getNoticeNo = '"
//              + mLJSPayBSchema.getGetNoticeNo() + "' )"
//              ;
//
//        result = tExeSQL.getOneValue(sql);
//        if (!result.equals("")) {
//            this.addError("GrpDueFeeBL", "checkData",
//                          "您好，保单正在做理赔业务或发生过结案的理陪，不能转出。");
//
//            return false;
//
//        }

        //校验报单续期后是否发生过理赔


//        sql = " select * from llclaimdetail a where GrpcontNo = '"
//              + mLJSPayBSchema.getOtherNo()
//              + "' and makedate >= "
//              + "       (select min(lastPayTodate) from LJAPayPerson "
//              + "       where grpContNo = '"
//              + mLJSPayBSchema.getOtherNo()
//              + "'          and getNoticeNo = '"
//              + mLJSPayBSchema.getGetNoticeNo() + "' )"
//              ;
//
//        result = tExeSQL.getOneValue(sql);
//        if (!result.equals("")) {
//            this.addError("GrpDueFeeBL", "checkData",
//                          "您好，保单正在做理赔业务或发生过结案的理陪，不能转出。");
//
//            return false;
//
//        }
        
        //校验是否有险种在某保障年度发生过理赔
        
//      先查看是否有正在进行的理赔
    	String currentclaimsql = "select 1 from llcase a where " +
		       "exists (select 1 from lcinsured where grpcontno ='" + mLJSPayBSchema.getOtherNo() 
             + "' and insuredno = a.customerno) and endcasedate is null and rgtstate <> '14' ";
    	System.out.println(currentclaimsql);
        String rgtNo = new ExeSQL().getOneValue(currentclaimsql);
        if (rgtNo != null && !rgtNo.equals("")) 
           {
            mErrors.addOneError("保单"+mLJSPayBSchema.getOtherNo()+"有理陪，不能给付确认。");
            return false;
           }
        //然后查看是否有某个保障年度操作的理赔
    	System.out.println("续收保费号"+mLJSPayBSchema.getGetNoticeNo());
        String sqlClaim = "select  1 from " +
    			"llcase a," +
    			"llcaserela b," +
    			"llsubreport c," +
    			"llclaimpolicy d " +
    			"where a.caseno=b.caseno " +
    			"and b.subrptno=c.subrptno " +
    			"and a.caseno=d.caseno " +
    			"and b.caserelano=d.caserelano " +
    			"and a.rgtstate in ('09','11','12') " +
    			"and a.endcasedate is not null " +
    			"and c.accdate <(select max(curpaytodate) from ljapayperson where getnoticeno='"+mLJSPayBSchema.getGetNoticeNo()+"')" +
    			"and c.accdate >(select min(lastpaytodate) from ljapayperson where getnoticeno='"+mLJSPayBSchema.getGetNoticeNo()+"')" +
    			"and d.grpcontno='"+mLJSPayBSchema.getOtherNo()+"' with ur";	
    	System.out.println(sqlClaim);
    	String result1 = new ExeSQL().getOneValue(sqlClaim);
    	if(!result1.equals(""))
        {
            mErrors.addOneError("该保障年度内发生过结案的理陪，不能转出。");
            return false;
        }
        

    	//正在做保全
        sql = " select * from lpedorapp , lpgrpedoritem "
              + " where LPGrpEdorItem.GrpContNO = '"
              + mLJSPayBSchema.getOtherNo()
              +"' and lpedorapp.EdorAcceptNo = lpgrpedoritem.EdorAcceptNo "
              + " and lpedorapp.edorstate <> '0'";

        result = tExeSQL.getOneValue(sql);
        if (!result.equals("")) {
            this.addError("GrpDueFeeBL", "checkDate",
                          "您好，保单正在做保全业务，不能转出。");
            return false;
        }

        sql = " select * from LJAGetEndorse "
              + "where LJAGetEndorse.GrpContNo = '"
              + mLJSPayBSchema.getOtherNo()
              + "' and LJAGetEndorse.getmoney <> 0 "
              + " and makedate >= "
              + " (select min(lastPayTodate) from LJAPayPerson "
              + "  where grpContNo = '"
              + mLJSPayBSchema.getOtherNo()
              + "'       and getNoticeNo = '"
              + mLJSPayBSchema.getGetNoticeNo() + "' )"
              ;
        result = tExeSQL.getOneValue(sql);
        if (!result.equals("")) {
            this.addError("GrpDueFeeBL", "checkData",
                          "您好，保单有过对保费有影响的保全业务，不能转出。");
            return false;
        }

        sql = " select * from lpgrpedoritem a "
              +
              "where EdorType in ( 'NI' ,'ZT')"
              + "and GrpContNo = '"
              + mLJSPayBSchema.getOtherNo()
              + "'  and edorAppDate >= "
              + "       (select makeDate from LJAPay "
              + "       where incomeNo = a.grpContNo "
              + "          and getNoticeNo = '"
              + mLJSPayBSchema.getGetNoticeNo() + "' )"
              ;
        result = tExeSQL.getOneValue(sql);
        if (!result.equals("")) {
            this.addError("GrpDueFeeBL",
                          "checkDate",
                          "您好，保单有过对保费有影响的保全业务，不能转出。");
            return false;
        }

        sql = " select * from lcgrpcontstate "
              +
              "where lcgrpcontstate.GrpcontNo = '"
              + mLJSPayBSchema.getOtherNo()
              +
              "' and lcgrpcontstate.startdate < '"
              + this.mCurrentDate
              +
              "' and ( lcgrpcontstate.endDate is null or endDate > '"
              + this.mCurrentDate
              + "' )"
              ;
        result = tExeSQL.getOneValue(sql);
        if (!result.equals("")) {
            this.addError("GrpDueFeeBL",
                          "checkDate",
                          "您好，当前保单或其险种处于失效或终止状态，不能转出。");
            return false;
        }

        return true;

    }

    private boolean setErrorLog()
    {

        LCUrgeVerifyLogSchema tLCUrgeVerifyLogSchema = new LCUrgeVerifyLogSchema();
        //加到催收核销日志表
        tLCUrgeVerifyLogSchema.setSerialNo(mLJSPayBSchema.getSerialNo());
        tLCUrgeVerifyLogSchema.setRiskFlag("1");
        tLCUrgeVerifyLogSchema.setOperateType("3"); //1：续期催收操作,2：续期核销操作,3:实收保费转出
        tLCUrgeVerifyLogSchema.setOperateFlag("1"); //1：个案操作,2：批次操作
        tLCUrgeVerifyLogSchema.setOperator(mGlobalInput.Operator);
        tLCUrgeVerifyLogSchema.setDealState("2"); //1 : 正在处理中,2 : 处理错误,3 : 处理已完成

        tLCUrgeVerifyLogSchema.setContNo(mLJSPayBSchema.getOtherNo());
        tLCUrgeVerifyLogSchema.setMakeDate(mCurrentDate);
        tLCUrgeVerifyLogSchema.setMakeTime(mCurrentTime);
        tLCUrgeVerifyLogSchema.setModifyDate(mCurrentDate);
        tLCUrgeVerifyLogSchema.setModifyTime(mCurrentTime);

        MMap tMap = new MMap();
        tMap.put(tLCUrgeVerifyLogSchema, "INSERT");

        VData tInputData = new VData();
        tInputData.add(tMap);

        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tInputData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LCUrgeVerifyLog 表失败!");
            return false;
        }
        return true;
}

    private boolean changeState(String getNoticeNo){
        ExeSQL tExeSQL = new ExeSQL();
        String  sqlLock = " update ljspayb set DealState = '"
                          + tDEAL.DEALSTATE_BACK +
                          "' where GetNoticeNo = '"+getNoticeNo+"'";
        return tExeSQL.execUpdateSQL(sqlLock);
    }

    private boolean dealData() {

        //产生新的PayNo
        String tPayNo = PubFun1.CreateMaxNo("PayNo",PubFun.getNoLimit(this.mLJSPayBSchema.getManageCom()));

        LJAPayDB tLJAPayDB = new LJAPayDB();
        //获取数据
        StringBuffer tSBql = new StringBuffer(128);
        tSBql.append("select * from LJAPay where GetNoticeNo='")
                    .append(mLJSPayBSchema.getGetNoticeNo())
                    .append("'");
        String sqlStr = tSBql.toString();

        mLJAPaySet = tLJAPayDB.executeQuery(sqlStr);

        tSBql = new StringBuffer(128);
        tSBql.append("select * ")
                .append("from LJAPayPerson ")
                .append("where payno = '")
                .append(mLJAPaySet.get(1).getPayNo())
                .append("' with ur ");
        LJAPayPersonSet set = new LJAPayPersonSet();
        RSWrapper rswrapper = new RSWrapper();
      //  LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
        rswrapper.prepareData(this.mLJAPayPersonSet,tSBql.toString());
        Reflections tReflections = new Reflections();

        do {

            rswrapper.getData();
            //循环取得5000条数据，进行处理

            set.clear();
            for (int j = 1; j <= this.mLJAPayPersonSet.size(); j++) {

                LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
                this.mLJAPayPersonSchema = this.mLJAPayPersonSet.get(j);
                //交换记录
                tReflections.transFields(tLJAPayPersonSchema,
                                         this.mLJAPayPersonSchema);
                tLJAPayPersonSchema.setSumDuePayMoney( -this.
                        mLJAPayPersonSchema.getSumDuePayMoney());
                tLJAPayPersonSchema.setSumActuPayMoney( -this.
                        mLJAPayPersonSchema.getSumActuPayMoney());
                tLJAPayPersonSchema.setPayDate(this.mCurrentDate);
                tLJAPayPersonSchema.setEnterAccDate(this.mCurrentDate);
                tLJAPayPersonSchema.setConfDate(this.mCurrentDate);
                tLJAPayPersonSchema.setPayNo(tPayNo);
                tLJAPayPersonSchema.setMoneyNoTax(null);
                tLJAPayPersonSchema.setMoneyTax(null);
                tLJAPayPersonSchema.setBusiType(null);
                tLJAPayPersonSchema.setTaxRate(null); 
                tLJAPayPersonSchema.setMakeDate(this.mCurrentDate);
                tLJAPayPersonSchema.setMakeTime(this.mCurrentTime);
                tLJAPayPersonSchema.setOperator(mGlobalInput.Operator);
                tLJAPayPersonSchema.setModifyDate(this.mCurrentDate);
                tLJAPayPersonSchema.setModifyTime(this.mCurrentTime);
                set.add(tLJAPayPersonSchema);
            }

            MMap tMMap = new MMap();
            tMMap.put(set, "INSERT");
            VData data = new VData();
            data.add(tMMap);
            PubSubmit submit = new PubSubmit();
            submit.submitData(data, "INSERT");
            if (submit.mErrors.needDealError()) {
                this.setErrorLog();
                this.addError("GrpDueFeeBackBL", "dealData", "生成负实收记录错误");
                return false;
            }
        }while (this.mLJAPayPersonSet.size() > 0);
        //生成负团单险种实收记录
         tSBql = new StringBuffer(128);
         tSBql.append("select * from LJAPayGrp where GetNoticeNo = '")
                 .append(this.mLJSPayBSchema.getGetNoticeNo())
                 .append("'");
         String sql = tSBql.toString();
         this.mLJaPayGrpSet = new LJAPayGrpDB().executeQuery(sql);
         for(int j = 1;j<=this.mLJaPayGrpSet.size();j++)
         {
         this.mLJAPayGrpSchema = this.mLJaPayGrpSet.get(j);
         LJAPayGrpSchema tLJAPayGrpSchema = new LJAPayGrpSchema();
         tReflections.transFields(tLJAPayGrpSchema,this.mLJAPayGrpSchema);
         tLJAPayGrpSchema.setSumActuPayMoney(-this.mLJAPayGrpSchema.getSumActuPayMoney());
         tLJAPayGrpSchema.setSumDuePayMoney(-mLJAPayGrpSchema.getSumDuePayMoney());
         tLJAPayGrpSchema.setPayDate(this.mCurrentDate);
         tLJAPayGrpSchema.setEnterAccDate(this.mCurrentDate);
         tLJAPayGrpSchema.setPayNo(tPayNo);
         tLJAPayGrpSchema.setMoneyNoTax(null);
         tLJAPayGrpSchema.setMoneyTax(null);
         tLJAPayGrpSchema.setBusiType(null);
         tLJAPayGrpSchema.setTaxRate(null); 
         tLJAPayGrpSchema.setMakeDate(this.mCurrentDate);
         tLJAPayGrpSchema.setMakeTime(this.mCurrentTime);
         tLJAPayGrpSchema.setOperator(mGlobalInput.Operator);
         tLJAPayGrpSchema.setConfDate(this.mCurrentDate);
         tLJAPayGrpSchema.setModifyDate(this.mCurrentDate);
         tLJAPayGrpSchema.setModifyTime(this.mCurrentTime);
         map.put(tLJAPayGrpSchema,"INSERT");
         }

         //生成负团单实收记录
         tSBql = new StringBuffer(128);
         tSBql.append("select * from LJAPay where GetNoticeNo = '")
                 .append(this.mLJSPayBSchema.getGetNoticeNo())
                 .append("'");
         String sql1 = tSBql.toString();
         this.mLJAPaySchema = new LJAPayDB().executeQuery(sql1).get(1);
         LJAPaySchema tLJAPaySchema = new LJAPaySchema();
         tReflections.transFields(tLJAPaySchema, this.mLJAPaySchema);
         tLJAPaySchema.setSumActuPayMoney(-this.mLJAPaySchema.getSumActuPayMoney());
         tLJAPaySchema.setPayDate(this.mCurrentDate);
         tLJAPaySchema.setEnterAccDate(this.mCurrentDate);
         tLJAPaySchema.setPayNo(tPayNo);
         tLJAPaySchema.setMakeDate(this.mCurrentDate);
         tLJAPaySchema.setMakeTime(this.mCurrentTime);
         tLJAPaySchema.setOperator(mGlobalInput.Operator);
         tLJAPaySchema.setConfDate(this.mCurrentDate);
         tLJAPaySchema.setModifyDate(this.mCurrentDate);
         tLJAPaySchema.setModifyTime(this.mCurrentTime);
         map.put(tLJAPaySchema,"INSERT");



         //回退保单数据
         //回退LCPrem
         if(!LCPremBack()){
          return false;
         }
          //回退LCduty
         if(!LCdutyBack()){
         return false;
         }
          //回退LCpol
         if(!LCpolBack()){
              return false;
         }
         //回退LCCont
         if(!LCContBack()){
           return false;
         }
         //回退LCGrpPol
         if(! LCGrpPolBack()){
         return false;
         }
         //回退LCGrpCont
           if(!LCGrpContBack()){
            return false;
           }

           mInputData.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
         if (tPubSubmit.submitData(mInputData, "") == false) {
             //添加失败记录到日志表
            this.setErrorLog();
            this.addError("","dealdata","回退操作失败!");
            return false;
         }
         MMap tMMap = addAppAcc();
         if (tMMap == null) {
             return false;
         }
         String  sqlBackSucc = " update ljspayb set dealstate = '"
                    +tDEAL.DEALSTATE_BACK_SUCC+
                    "' where GetNoticeNo = '"
                    +this.mLJSPayBSchema.getGetNoticeNo()+"'";

         tMMap.put(sqlBackSucc,"UPDATE");
         tPubSubmit = new PubSubmit();
         VData t = new VData();
         t.add(tMMap);
         if (tPubSubmit.submitData(t,"") == false) {
             //添加失败记录到日志表
            this.setErrorLog();
            this.addError("","dealdata","回退操作失败!");
            return false;
         }
         if(!delErrorLog())
         {
             this.setErrorLog();
             this.addError("", "dealdata", "修改标志操作失败!");
             return false;

         }
         return true;
    }

    private void addError(String moduleName, String functionName,
                          String errorMessage) {
        CError tError = new CError();
        tError.moduleName = moduleName;
        tError.functionName = functionName;
        tError.errorMessage = errorMessage;
        this.mErrors.addOneError(tError);
    }

    private boolean LCPremBack() {
        StringBuffer tSBql = new StringBuffer(128);
        tSBql.append(" update LCprem a set paytimes = paytimes -1 , sumPrem = sumPrem - prem ,")
                .append(" paytodate = (select min(lastPayToDate) from LJAPayPerson where polNo = a.polNO and dutyCode = a.dutyCode and payPlanCode = a.payPlanCode and getNoticeNo = '" +
                        mLJSPayBSchema.getGetNoticeNo() + "') ")
                .append(", Operator = '")
                .append(mGlobalInput.Operator)
                .append("',managecom = '")
                .append(mGlobalInput.ManageCom)
                .append("', ModifyDate = '")
                .append(this.mCurrentDate)
                .append("', Modifytime = '")
                .append(this.mCurrentTime)
                .append("' ")
                .append("where grpcontno = '")
                .append(this.mLJSPayBSchema.getOtherNo())
                .append("'");

        map.put(tSBql.toString(), "UPDATE");
        return true;
    }

    /**
     * 将LCDuty的sumPrem, prem, payToDate设为LCPrem之和
     * @return boolean
     */
    private boolean LCdutyBack() {
        StringBuffer sql = new StringBuffer();
        sql.append("update LCDuty a ")
                .append("set (sumPrem, prem, payToDate) ")
                .append("    = (select sum(sumPrem), sum(prem), min(payToDate) from LCPrem ")
                .append("   where polNo = a.polNo and dutyCode = a.dutyCode), ")
                .append(" Operator = '")
               .append(mGlobalInput.Operator)
               .append("', ModifyDate = '")
               .append(this.mCurrentDate)
               .append("', Modifytime = '")
               .append(this.mCurrentTime)
               .append("' ")
                .append("where ContNo in (select contno from lccont where Grpcontno = '")
                .append(mLJSPayBSchema.getOtherNo())
                .append("' )");
        map.put(sql.toString(), "UPDATE");
        return true;
    }

    private boolean LCpolBack(){
           StringBuffer tSBql = new StringBuffer(128);
            tSBql.append(" update LCpol a ")
                   .append(" set (sumprem ,paytodate) ")
                   .append(" = (select sum(sumprem),min(paytodate) from lcduty ")
                   .append(" where polno = a.polno ),")
                   .append(" Operator = '")
                   .append(mGlobalInput.Operator)
                   .append("', ModifyDate = '")
                   .append(this.mCurrentDate)
                   .append("', Modifytime = '")
                   .append(this.mCurrentTime)
                   .append("' where a.GrpContNo = '")
                   .append(mLJSPayBSchema.getOtherNo())
                   .append("'");
                map.put(tSBql.toString(), "UPDATE");
                return true;
        }

    private boolean LCContBack(){
        StringBuffer tSBql = new StringBuffer(128);

        tSBql.append(" update LCCont a set (sumprem ,paytodate) ")
                .append(" = (select sum(sumprem),min(paytodate) from lcpol ")
                .append(" where contno = a.contno ),")
                .append(" Operator = '")
                .append(mGlobalInput.Operator)
                .append("', ModifyDate = '")
                .append(this.mCurrentDate)
                .append("', Modifytime = '")
                .append(this.mCurrentTime)
                .append("' where grpcontno = '")
                .append(mLJSPayBSchema.getOtherNo())
                .append("'");
        map.put(tSBql.toString(), "UPDATE");

    return true;
    }

    private boolean LCGrpPolBack(){

        StringBuffer tSBql = new StringBuffer(128);

            tSBql.append(" update LCGrpPol a set (sumprem,paytodate)  ")
                    .append(" = (select sum(sumprem),min(paytodate) from lccont ")
                    .append(" where grpcontno = a.grpcontno ),")
                    .append(" Operator = '")
                    .append(mGlobalInput.Operator)
                    .append("', ModifyDate = '")
                    .append(this.mCurrentDate)
                    .append("', Modifytime = '")
                    .append(this.mCurrentTime)
                    .append("' where grpcontno = '")
                    .append(mLJSPayBSchema.getOtherNo())
                    .append("'");
            map.put(tSBql.toString(), "UPDATE");

        return true;

    }

    private boolean LCGrpContBack() {

        StringBuffer tSBql = new StringBuffer(128);

            tSBql.append(" update LCGrpCont a set (sumprem)  ")
                    .append(" = (select sum(sumprem) from lcgrppol")
                    .append(" where grpcontno = a.grpcontno ),")
                    .append(" Operator = '")
                    .append(mGlobalInput.Operator)
                    .append("', ModifyDate = '")
                    .append(this.mCurrentDate)
                    .append("', Modifytime = '")
                    .append(this.mCurrentTime)
                    .append("' where grpcontno = '")
                    .append(mLJSPayBSchema.getOtherNo())
                    .append("'");
            map.put(tSBql.toString(), "UPDATE");
        return true;

    }
/**
*检测是否有回退错误发生，如果有返回true
*/
    private boolean checkErrorFlag() {


        LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new LCUrgeVerifyLogDB();
        LCUrgeVerifyLogSet tLCUrgeVerifyLogSet = new LCUrgeVerifyLogSet();
        tLCUrgeVerifyLogDB.setSerialNo(mLJSPayBSchema.getSerialNo());
        tLCUrgeVerifyLogDB.setOperateType("3");//3：实收保费转出
        tLCUrgeVerifyLogDB.setOperateFlag("1");
        tLCUrgeVerifyLogDB.setRiskFlag("1");
        tLCUrgeVerifyLogDB.setDealState("2");
        tLCUrgeVerifyLogSet = tLCUrgeVerifyLogDB.query();
        if ((tLCUrgeVerifyLogSet == null) || tLCUrgeVerifyLogSet.size() == 0)
        {
            return false;
        }
        return true;

    }
    /**
     * 余额存入帐户(使用的是保单服务退费)
     * */
    private MMap addAppAcc(){
        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
        tLCAppAccTraceSchema.setCustomerNo(mLJSPayBSchema.getAppntNo());
        tLCAppAccTraceSchema.setOtherNo(mLJSPayBSchema.getOtherNo());
        tLCAppAccTraceSchema.setOtherType("1");
        tLCAppAccTraceSchema.setMoney(getSumPayMoney());
        tLCAppAccTraceSchema.setOperator(mGlobalInput.Operator);
        tLCAppAccTraceSchema.setBakNo(mLJSPayBSchema.getGetNoticeNo());//续期应收号码，modify by fuxin 2009-3-2
        AppAcc tAppAcc = new AppAcc();
        MMap tMMap = tAppAcc.accShiftToSSZC(tLCAppAccTraceSchema);
        if(tMMap == null || tMMap.size() == 0)
        {
            this.setErrorLog();
            mErrors.copyAllErrors(tAppAcc.mErrors);
            return null;
        }

        return tMMap;

    }

    /**
     * 得到原续期所交保费，包括客户交费和帐户抵扣余额
     * @return double
     */
    private double getSumPayMoney()
    {
        String sql = "select sum(sumDuePayMoney) "
                     + "from LJAPayGrp "
                     + "where payType = 'ZC' "
                     + "   and sumDuePayMoney >= 0 "
                     + "   and getNoticeNO = '"
                     + mLJAPaySchema.getGetNoticeNo() + "' ";
        String moneyStr = new ExeSQL().getOneValue(sql);
        if(moneyStr.equals("") || moneyStr.equals("null"))
        {
            return Double.MIN_VALUE;
        }
        else
        {
            return Double.parseDouble(moneyStr);
        }
    }

    private boolean delErrorLog()
    {

        LCUrgeVerifyLogSchema tLCUrgeVerifyLogSchema = new LCUrgeVerifyLogSchema();
        //加到催收核销日志表
        tLCUrgeVerifyLogSchema.setSerialNo(mLJSPayBSchema.getSerialNo());
        tLCUrgeVerifyLogSchema.setRiskFlag("1");
        tLCUrgeVerifyLogSchema.setOperateType("3"); //1：续期催收操作,2：续期核销操作,3:实收保费转出
        tLCUrgeVerifyLogSchema.setOperateFlag("1"); //1：个案操作,2：批次操作

        MMap tMap = new MMap();
        tMap.put(tLCUrgeVerifyLogSchema, "DELETE");

        VData tInputData = new VData();
        tInputData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tInputData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LCUrgeVerifyLog 表失败!");
            return false;
        }
        return true;

    }
}

