package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class GrpDueFeeMultiUI {

  //业务处理相关变量
  private VData mInputData ;
  public  CErrors mErrors=new CErrors();

  public GrpDueFeeMultiUI() {
  }
  public static void main(String[] args) {
    GrpDueFeeMultiUI GrpDueFeeMultiUI1 = new GrpDueFeeMultiUI();
    GlobalInput tGI = new GlobalInput();
    tGI.ComCode="86";
    tGI.Operator="001";
    tGI.ManageCom="86";
    TransferData tTransferData=new TransferData();
    tTransferData.setNameAndValue("StartDate","2006-07-01");
    tTransferData.setNameAndValue("EndDate","2006-8-31");
    tTransferData.setNameAndValue("ManageCom","86");

    VData tVData = new VData();
    tVData.add(tGI);
    tVData.add(tTransferData);
    GrpDueFeeMultiUI tGrpDueFeeMultiUI = new GrpDueFeeMultiUI();
    if(tGrpDueFeeMultiUI.submitData(tVData,"INSERT")==false)
    {
        System.out.println("团单批处理催收失败："+tGrpDueFeeMultiUI.mErrors.getFirstError());
    }
    else
    {
        System.out.println("团单批处理催收成功");
    }

  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    GrpDueFeeMultiBL tGrpDueFeeMultiBL=new GrpDueFeeMultiBL();
    System.out.println("Start GrpDueFeeMulti UI Submit...");
    tGrpDueFeeMultiBL.submitData(mInputData,cOperate);

    System.out.println("End GrpDueFeeMulti UI Submit...");

    mInputData=null;
    //如果有需要处理的错误，则返回
    if (tGrpDueFeeMultiBL .mErrors .needDealError() )
       {
       this.mErrors .copyAllErrors(tGrpDueFeeMultiBL.mErrors ) ;
       return false;
       }
System.out.println("error num="+mErrors.getErrorCount());
    return true;
  }

}
