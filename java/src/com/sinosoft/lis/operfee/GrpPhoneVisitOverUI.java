package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class GrpPhoneVisitOverUI {

    //业务处理相关变量
    private VData mInputData;
    public CErrors mErrors = new CErrors();

    public GrpPhoneVisitOverUI() {
    }

    public static void main(String[] args) {

        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "wuser";
        tGI.ManageCom = "86";
        TransferData tempTransferData = new TransferData();
        tempTransferData.setNameAndValue("CustomerNo", "00000003");

        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tempTransferData);
        GrpPhoneVisitOverUI tGrpPhoneVisitOverUI = new GrpPhoneVisitOverUI();
        tGrpPhoneVisitOverUI.submitData(tVData);

    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        GrpPhoneVisitOverBL tGrpPhoneVisitOverBL = new GrpPhoneVisitOverBL();
        System.out.println("Start GrpPhoneVisitOver UI Submit...");
        tGrpPhoneVisitOverBL.submitData(mInputData,"INSERT");

        System.out.println("End GrpPhoneVisitOver UI Submit...");

        mInputData = null;
        //如果有需要处理的错误，则返回
        if (tGrpPhoneVisitOverBL.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tGrpPhoneVisitOverBL.mErrors);
            return false;
        }
        System.out.println("error num=" + mErrors.getErrorCount());
        return true;
    }

}
