package com.sinosoft.lis.operfee;

import java.util.*;

import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.task.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 续期续保业务报表1-达成率分析(保单)
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class PrtContSuccessBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private static String W = "W";  //无名单
    private static String N = "N";  //非名单
    private static String L = "L";  //长险保单
    private static String O = "O";  //非长险保单
    private static String P = "P";  //个人
    private static String G = "G";  //团体
    private static String SUCCESS = "Suc";  //成功
    private static String WAIT_CHARGE = "Waitcharge";  //团单成功
    private static String INVALID = "Invalid";  //失效
    private static String PAYMODE4 = "M4";  //银行转帐
    private static String PAYMODEO = "MO";  //其他收费方式
    private static String COM86 = "全系统";
    private static int COL_NUM = 20;

    private String[] mInfoSum1 = null;  //全系统团险无名单续期
    private String[] mInfoSum2 = null;  //全系统团险有名单续期
    private String[] mInfoSumSub1 = null;  //全系统团险合计
    private String[] mInfoSum3 = null;  //全系统个险长期保单
    private String[] mInfoSum4 = null;  //全系统个险一年期保单
    private String[] mInfoSumSub2 = null;  //全系统个险合计

    private GlobalInput mGI = null; //操作员信息
    private TransferData mTransferData = null;

    private String mStartDate = null;
    private String mEndDate = null;
    private String mManageCom = null;

    HashMap mHashMap = new HashMap(); //存储清单数据
    HashSet mHashSet = new HashSet();  //存储涉及的机构

    HashMap mHashMapBank = new HashMap(); //存储银行应收数据
    HashMap mHashMapNotBank = new HashMap(); //存储非银行应收数据

    private ExeSQL mExeSQL = new ExeSQL();

    private XmlExport xmlexport = new XmlExport();

    public PrtContSuccessBL()
    {
    }

    /**
     * 操作的提交方法，得到打印清单数据。
     * @param sql String
     * @return XmlExport
     */
    public XmlExport getXmlExport(VData cInputData, String operate)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return xmlexport;
    }

    /**
     * createXmlExport
     * 根据dealData中得到的数据生成清单
     * @return boolean
     */
    private boolean createXmlExport()
    {
        ListTable tListTable = new ListTable();

        Object[] manageComs = mHashSet.toArray();

        for(int i = 0; i < manageComs.length; i++)
        {
            String manageCom = (String)manageComs[i];

            System.out.println("团单无名单");
            String[] info1 = getNoNameContInfo(manageCom, W, G);
            tListTable.add(info1);
            mInfoSum1 = getSumSub(info1, mInfoSum1, G);

            System.out.println("团单有名单");
            String[] info2 = getNoNameContInfo(manageCom, N, G);
            tListTable.add(info2);
            mInfoSum2 = getSumSub(info2, mInfoSum2, G);

            String[] infoSum1 = getSumSub(info1, info2, G);
            tListTable.add(infoSum1);
            mInfoSumSub1 = getSumSub(infoSum1, mInfoSumSub1, G);

            System.out.println("个单长险");
            String[] info3 = getNoNameContInfo(manageCom, L, P);
            tListTable.add(info3);
            mInfoSum3 = getSumSub(info3, mInfoSum3, P);

            System.out.println("个单一年期");
            String[] info4 = getNoNameContInfo(manageCom, O, P);
            tListTable.add(info4);
            mInfoSum4 = getSumSub(info4, mInfoSum4, P);

            String[] infoSum2 = getSumSub(info3, info4, P);
            //生成最后四列
            //得到机构的转帐应收
            double []value =(double []) mHashMapBank.get(manageCom);
            if(!infoSum2[13].equals("0.00"))
            {
                String count ,money;
                count = infoSum2[13];
                money = infoSum2[14];
                if(Math.abs(value[0])<=0.00001)
                {
                    infoSum2[13] ="0.00";
                    infoSum2[14] ="0.00";
                }else
                {
                    infoSum2[13] = String.valueOf(CommonBL.carry((Double.parseDouble(count) /
                                                                  value[0]) * 100));
                    infoSum2[14] = String.valueOf(CommonBL.carry((Double.parseDouble(money) /
                                                                  value[1]) * 100));
                }
                if(Math.abs(Double.parseDouble(infoSum2[2])-value[0])<=0.00001)
                {
                    infoSum2[15] = "0.00";
                    infoSum2[16] = "0.00";

                }else
                {
                    infoSum2[15] = String.valueOf(CommonBL.carry(((Double.parseDouble(infoSum2[15]) -
                            Double.parseDouble(count)) /(Double.parseDouble(infoSum2[2]) - value[0])) * 100));
                    infoSum2[16] = String.valueOf(CommonBL.carry(((Double.parseDouble(infoSum2[16]) -
                            Double.parseDouble(money)) /(Double.parseDouble(infoSum2[3]) - value[1])) * 100));
                }
            }
            tListTable.add(infoSum2);
            mInfoSumSub2 = getSumSub(infoSum2, mInfoSumSub2, P);
        }

        tListTable.setName("List");

        String[] colNames = {"机构","业务类型",
                            "应收件数","应收保费",
                            "应收成功件数","应收成功保费",
                            "收费成功率(%)件数","收费成功率(%)保费",
                            "平均收费时效",
                            "待收费件数","待收费保费",
                            "未收失效件数","未收失效保费",
                            "个险转账成功率件数","个险转账成功率保费",
                            "个险自缴成功率件数","个险自缴成功率保费", "处理时长"};

        xmlexport.addListTable(tListTable, colNames);

        //生成总合计
        ListTable aListTable = new ListTable();
        aListTable.setName("Sum");
        if(mInfoSum1==null)
        {
        	mInfoSum1 = new String [2];
      	}
        mInfoSum1[0] = COM86;
        mInfoSum1[1] = "团险无名单续期";
        aListTable.add(mInfoSum1);
       	if(mInfoSum2==null)
        {
        	mInfoSum2 = new String [2];
      	}
        mInfoSum2[0] = COM86;
        mInfoSum2[1] = "团险有名单续期";
        aListTable.add(mInfoSum2);
        if(mInfoSumSub1==null)
        {
        	mInfoSumSub1 = new String [2];
      	}
        mInfoSumSub1[0] = COM86;
        mInfoSumSub1[1] = "团险合计";
        aListTable.add(mInfoSumSub1);
        double [] pcont = new double [2];
        if(mInfoSum3 ==null)
        {
        	mInfoSum3 = new String [2];
        }else
        {
            //一个机构的话不用处理
            if(mHashSet.size()>1)
            {
                if (!mInfoSum3[13].equals("0.00")) {
                    double valueL[] = (double[]) mHashMapBank.get(L);
                    String count, money;
                    count = mInfoSum3[13];
                    money = mInfoSum3[14];
                    pcont[0] = valueL[0];
                    pcont[1] = valueL[1];
                    if (Math.abs(valueL[0]) <= 0.00001) {
                        mInfoSum3[13] = "0.00";
                        mInfoSum3[13] = "0.00";
                    } else {
                        mInfoSum3[13] = String.valueOf(CommonBL.carry((Double.
                                parseDouble(count) /
                                valueL[0]) * 100));
                        mInfoSum3[14] = String.valueOf(CommonBL.carry((Double.
                                parseDouble(money) /
                                valueL[1]) * 100));

                    }
                    if(Math.abs(Double.parseDouble(mInfoSum3[2]) - valueL[0])<=0.001)
                    {
                        mInfoSum3[15] ="0.00";
                        mInfoSum3[16] ="0.00";
                    }else
                    {
                        mInfoSum3[15] = String.valueOf(CommonBL.carry(((
                                Double.parseDouble(mInfoSum3[15]) -
                                Double.parseDouble(count)) /
                                (Double.parseDouble(mInfoSum3[2]) - valueL[0])) *
                                100));
                        mInfoSum3[16] = String.valueOf(CommonBL.carry(((
                                Double.parseDouble(mInfoSum3[16]) -
                                Double.parseDouble(money)) /
                                (Double.parseDouble(mInfoSum3[3]) - valueL[1])) *
                                100));
                    }
                }
            }
        }
        mInfoSum3[0] = COM86;
        mInfoSum3[1] = "个险长期保单";
        aListTable.add(mInfoSum3);
        if(mInfoSum4 == null)
        {
        	mInfoSum4 = new String [2];
        }else
        {
             if(mHashSet.size()>1)
             {
                 if (!mInfoSum4[13].equals("0.00")) {
                     double valueO[] = (double[]) mHashMapBank.get(O);
                     String count, money;
                     count = mInfoSum4[13];
                     money = mInfoSum4[14];
                     pcont[0] += valueO[0];
                     pcont[1] += valueO[1];
                     if (Math.abs(valueO[0]) <= 0.00001) {
                         mInfoSum4[13] = "0.00";
                         mInfoSum4[14] = "0.00";
                     } else {
                         mInfoSum4[13] = String.valueOf(CommonBL.carry((Double.
                                 parseDouble(count) /
                                 valueO[0]) * 100));
                         mInfoSum4[14] = String.valueOf(CommonBL.carry((Double.
                                 parseDouble(money) /
                                 valueO[1]) * 100));
                     }
                     if(Math.abs(Double.parseDouble(mInfoSum4[2]) - valueO[0])<=0.001)
                     {
                         mInfoSum4[15] = "0.00";
                         mInfoSum4[16] = "0.00";
                     }else
                     {
                         mInfoSum4[15] = String.valueOf(CommonBL.carry(((Double.
                                 parseDouble(
                                         mInfoSum4[15]) - Double.parseDouble(count)) /
                                 (Double.parseDouble(mInfoSum4[2]) - valueO[0])) *
                                                                       100));
                         mInfoSum4[16] = String.valueOf(CommonBL.carry(((Double.
                                 parseDouble(
                                         mInfoSum4[16]) - Double.parseDouble(money)) /
                                 (Double.parseDouble(mInfoSum4[3]) - valueO[1])) *
                                                                       100));
                     }
                 }
             }
        }
        mInfoSum4[0] = COM86;
        mInfoSum4[1] = "个险一年期保单";
        aListTable.add(mInfoSum4);
        if(mInfoSumSub2 == null)
        {
        	mInfoSumSub2 = new String [2];
        }else
        {
             if(mHashSet.size()>1)
             {
                 if (!mInfoSumSub2[13].equals("0.00")) {
                     String count, money;
                     count = mInfoSumSub2[13];
                     money = mInfoSumSub2[14];
                     if (Math.abs(pcont[0]) <= 0.00001) {
                         mInfoSumSub2[13] = "0.00";
                         mInfoSumSub2[14] = "0.00";
                     } else {

                         mInfoSumSub2[13] = String.valueOf(CommonBL.carry((
                                 Double.
                                 parseDouble(count) /
                                 pcont[0]) * 100));
                         mInfoSumSub2[14] = String.valueOf(CommonBL.carry((
                                 Double.
                                 parseDouble(money) /
                                 pcont[1]) * 100));
                     }
                     if(Math.abs(Double.parseDouble(mInfoSumSub2[2]) - pcont[0])<=0.001)
                     {
                         mInfoSumSub2[15] = "0.00";
                         mInfoSumSub2[16] = "0.00";
                     }else
                     {
                         mInfoSumSub2[15] = String.valueOf(CommonBL.carry(((
                                 Double.
                                 parseDouble(
                                         mInfoSumSub2[15]) -
                                 Double.parseDouble(count)) /
                                 (Double.parseDouble(mInfoSumSub2[2]) -
                                  pcont[0])) * 100));
                         mInfoSumSub2[16] = String.valueOf(CommonBL.carry(((
                                 Double.
                                 parseDouble(
                                         mInfoSumSub2[16]) -
                                 Double.parseDouble(money)) /
                                 (Double.parseDouble(mInfoSumSub2[3]) -
                                  pcont[1])) * 100));
                     }
                 }
             }
        }
        mInfoSumSub2[0] = COM86;
        mInfoSumSub2[1] = "个险合计";
        aListTable.add(mInfoSumSub2);
        xmlexport.addListTable(aListTable, colNames);


        return true;
    }

    /**
     * 计算合计
     * @param info1 String[]
     * @param info2 String[]
     * @param contType String
     * @return String[]
     */
    private String[] getSumSub(String[] info1, String[] info2, String contType)
    {
        if(info1 == null)
        {
            return info2;
        }
        else if(info2 == null)
        {
            return info1;
        }

        String[] info3 = new String[this.COL_NUM];
        info3[0] = info1[0];
        info3[1] = ("P".equals(contType) ? "个险合计" : "团险合计");
        info3[2] = String.valueOf(Integer.parseInt(info1[2])
                                  + Integer.parseInt(info2[2]));
        info3[3] = CommonBL.bigDoubleToCommonString(
            (Double.parseDouble(info1[3])+ Double.parseDouble(info2[3])),"0.00");
        info3[4] = String.valueOf(Integer.parseInt(info1[4])
                                  + Integer.parseInt(info2[4]));
        info3[5] = CommonBL.bigDoubleToCommonString(
            (Double.parseDouble(info1[5])+ Double.parseDouble(info2[5])),"0.00");
        //收费成功率(%): 成功/应收
        if(Math.abs(Double.parseDouble(info3[2]) - 0) < 0.00001 || Math.abs(Double.parseDouble(info3[3]) - 0) < 0.00001)
        {
            info3[6] = "0";
            info3[7] = "0";
        }
        else
        {
            double temp = Double.parseDouble(info3[4])
                          / Double.parseDouble(info3[2]);
            info3[6] = String.valueOf(CommonBL.carry(temp * 100));
            double temp2 = Double.parseDouble(info3[5])
                           / Double.parseDouble(info3[3]);
            info3[7] = String.valueOf(CommonBL.carry(temp2 * 100));
        }
        //平均收费时效=收费成功件自抽档日到核销时间之间的的天数之和/收费成功件数之和
        if(info3[4].equals("0"))
        {
            info3[8] = "0";
            info3[17] = "0";
        }
        else
        {
            int count = Integer.parseInt(info3[4]);
            int dayIntv = Integer.parseInt(info1[17])
                                  + Integer.parseInt(info2[17]);
            info3[17] = String.valueOf(dayIntv);
            info3[8] = String.valueOf(CommonBL.carry(dayIntv / count));
        }

        info3[9] = String.valueOf(Integer.parseInt(info1[9])
                                  + Integer.parseInt(info2[9]));
        info3[10] = CommonBL.bigDoubleToCommonString(
            (Double.parseDouble(info1[10])+ Double.parseDouble(info2[10])),"0.00");
        info3[11] = String.valueOf(Integer.parseInt(info1[11])
                                   + Integer.parseInt(info2[11]));
        info3[12] = CommonBL.bigDoubleToCommonString(
            (Double.parseDouble(info1[12])+ Double.parseDouble(info2[12])),"0.00");
        if(P.equals(contType))
        {

            //暂存转账缴费方式数据，为求和时计算缴费方式比例提供数据
            info3[18] = String.valueOf(Integer.parseInt(info1[18])
                                       + Integer.parseInt(info2[18]));
            info3[19] = CommonBL.bigDoubleToCommonString(
                Double.parseDouble(info1[19])
                + Double.parseDouble(info2[19]), "0.00");

            double m4Count = Integer.parseInt(info3[18]);
            double ljaCount = Integer.parseInt(info3[4]);

            double m4CountRate = 0.00;
            double ljaMoneyRate = 0.00;
            if(Math.abs(ljaCount - 0) > 0.0001)
            {
                m4CountRate = CommonBL.carry(m4Count / ljaCount * 100);
                double m4Money = Double.parseDouble(info3[19]);
                double ljaMoney = Double.parseDouble(info3[5]);
                ljaMoneyRate = CommonBL.carry(m4Money / ljaMoney * 100);

                info3[13] = String.valueOf(m4Count);
                info3[14] = String.valueOf(m4Money);

//                info3[15] = String.valueOf(CommonBL.carry(((Double.parseDouble(info3[3])-m4Count) / (Double.parseDouble(info3[3])-ljaCount)) * 100));
//                info3[16] = String.valueOf(CommonBL.carry(((Double.parseDouble(info3[4])-m4Money) / (Double.parseDouble(info3[4])-ljaMoney)) * 100));
                info3[15] =info3[4];
                info3[16] = info3[5];
            }
            else
            {
                info3[13] = "0.00";
                info3[14] = "0.00";

                info3[15] = "0.00";
                info3[16] = "0.00";
            }


//            info3[13] = String.valueOf(Integer.parseInt(info1[13])
//                                       + Integer.parseInt(info2[13]));
//            info3[14] = CommonBL.bigDoubleToCommonString(
//                Double.parseDouble(info1[14])
//                + Double.parseDouble(info2[14]), "0.00");
//            info3[15] = String.valueOf(Integer.parseInt(info1[15])
//                                       + Integer.parseInt(info2[15]));
//            info3[16] = CommonBL.bigDoubleToCommonString(
//                Double.parseDouble(info1[16])
//                + Double.parseDouble(info2[16]), "0.00");
        }
        else
        {
            info3[13] = "-";
            info3[14] = "-";
            info3[15] = "-";
            info3[16] = "-";
        }

        return info3;
    }

    /**
     * 得到某机构团单无名单行数据
     * @param manageCom String
     * @return String[]
     */
    private String[] getNoNameContInfo(String manageCom, String type,
                                       String contType)
    {
        String[] info = new String[COL_NUM];

        String typeName = "";
        if(W.equals(type))
        {
            typeName = "团险无名单续期";
        }
        else if(N.equals(type))
        {
            typeName = "团险有名单续期";
        }
        else if(L.equals(type))
        {
            typeName = "个险长期保单";
        }
        else if(O.equals(type))
        {
            typeName = "个险一年期保单";
        }


        //团险无名单续期成功
        String sucgKey = manageCom + type + this.SUCCESS + contType; //团单无名单
        String[] values = getValues(sucgKey);

        info[0] = GetMemberInfo.getComNameByComCode(manageCom);
        info[1] = typeName;
        info[4] = values[0];
        info[5] = CommonBL.bigDoubleToCommonString(Double.parseDouble(values[1]), "0.00");

        //平均收费时效=收费成功件自抽档日到核销时间之间的的天数之和/收费成功件数之和
        if(info[4].equals("0"))
        {
            info[8] = "0";
            info[17] = "0";
        }
        else
        {
            int count = Integer.parseInt(info[4]);
            int dayIntv = Integer.parseInt(values[2]);
            info[17] = values[2];
            info[8] = String.valueOf(CommonBL.carry(dayIntv / count));
        }

        //团险无名单续期待收费
        sucgKey = manageCom + type + this.WAIT_CHARGE + contType;
        values = getValues(sucgKey);
        info[9] = values[0];
        info[10] = CommonBL.bigDoubleToCommonString(Double.parseDouble(values[1]), "0.00");

        //团险无名单未收失效
        sucgKey = manageCom + type + this.INVALID + contType;
        values = getValues(sucgKey);
        info[11] = values[0];
        info[12] = CommonBL.bigDoubleToCommonString(Double.parseDouble(values[1]), "0.00");


        //应收
        int sumCount = Integer.parseInt(info[4]) + Integer.parseInt(info[9])
                          + Integer.parseInt(info[11]);
        info[2] = String.valueOf(sumCount);
        double sumPrem = Double.parseDouble(info[5]) + Double.parseDouble(info[10])
                         + Double.parseDouble(info[12]);
        info[3] =  CommonBL.bigDoubleToCommonString(sumPrem, "0.00");

        //收费成功率(%): 成功/应收
        if(Math.abs(Double.parseDouble(info[2]) - 0) < 0.00001 || Math.abs(Double.parseDouble(info[3]) - 0) < 0.00001)
        {
            info[6] = "0";
            info[7] = "0";
        }
        else
        {
            double temp = Double.parseDouble(info[4])
                          / Double.parseDouble(info[2]);
            info[6] = String.valueOf(CommonBL.carry(temp * 100));
            double temp2 = Double.parseDouble(info[5])
                           / Double.parseDouble(info[3]);
            info[7] = String.valueOf(CommonBL.carry(temp2 * 100));
        }
        //银行转账、自缴
        if("P".equals(contType))
        {
            sucgKey = manageCom + type + this.PAYMODE4;
            values = getValues(sucgKey);

            //暂存转账缴费方式数据，为求和时计算缴费方式比例提供数据
            info[18] = String.valueOf(Integer.parseInt(values[0]));
            info[19] = CommonBL.bigDoubleToCommonString(
                Double.parseDouble(values[1]), "0.00");

            double ljaCountBoth = Integer.parseInt(info[4]);
    //        double ljaMoney = Double.parseDouble(info[5]);
            double ljaCount = 0.0;
            double ljaMoney = 0.0;
           sucgKey = manageCom + type + this.SUCCESS + contType + "Bank";
           values = getValues(sucgKey);
           ljaCount += Integer.parseInt(values[0]);
           ljaMoney += Double.parseDouble(values[1]);
           sucgKey = manageCom + type + this.WAIT_CHARGE + contType + "Bank";
           values = getValues(sucgKey);
           ljaCount += Integer.parseInt(values[0]);
           ljaMoney += Double.parseDouble(values[1]);
           sucgKey = manageCom + type + this.INVALID + contType + "Bank";
           values = getValues(sucgKey);
           ljaCount += Integer.parseInt(values[0]);
           ljaMoney += Double.parseDouble(values[1]);
           double temp [] ;
//           temp = new double[2];
           if(mHashMapBank.containsKey(manageCom))
           {
               temp = (double[])mHashMapBank.get(manageCom);
               temp[0] +=ljaCount;
               temp[1] +=ljaMoney;
               mHashMapBank.remove(manageCom);
               mHashMapBank.put(manageCom,temp);
           }
           else
           {
               temp = new double[2];
                temp[0] =ljaCount;
                temp[1] =ljaMoney;
                mHashMapBank.put(manageCom,temp);
           }
           if(mHashMapBank.containsKey(type))
           {
               temp = (double[]) mHashMapBank.get(type);
               temp[0] += ljaCount;
               temp[1] += ljaMoney;
               mHashMapBank.remove(type);
               mHashMapBank.put(type, temp);
           } else {
               temp = new double[2];
               temp[0] = ljaCount;
               temp[1] = ljaMoney;
               mHashMapBank.put(type, temp);
           }

            double m4CountRate = 0.00;
            double ljaMoneyRate = 0.00;
            if(Math.abs(ljaCountBoth - 0) > 0.0001)
            {
                double m4Count = Integer.parseInt(info[18]);
                double m4Money = Double.parseDouble(info[19]);
                 if(Math.abs(ljaCount - 0) > 0.0001)
                 {
                     m4CountRate = CommonBL.carry(m4Count / ljaCount * 100);
                     ljaMoneyRate = CommonBL.carry(m4Money / ljaMoney * 100);
                 }
                info[13] = String.valueOf(m4CountRate);
                info[14] = String.valueOf(ljaMoneyRate);
                if((sumCount-ljaCount)!=0)
                {
                    info[15] = String.valueOf(CommonBL.carry(((Double.
                            parseDouble(info[4]) - m4Count) /
                            (sumCount - ljaCount)) * 100));
                    info[16] = String.valueOf(CommonBL.carry(((Double.
                            parseDouble(info[5]) - m4Money) /
                            (sumPrem - ljaMoney)) * 100));
                }else
                {
                    info[15] = "0.00";
                    info[16] = "0.00";
                }
            }
            else
            {
                info[13] = "0.00";
                info[14] = "0.00";

                info[15] = "0.00";
                info[16] = "0.00";
            }

//            sucgKey = manageCom + type + this.PAYMODEO;
//            values = getValues(sucgKey);
//            info[15] = String.valueOf(Integer.parseInt(values[0]));
//            info[16] = CommonBL.bigDoubleToCommonString(
//                Double.parseDouble(values[1]), "0.00");

        }
        else
        {
            info[13] = "-";
            info[14] = "-";
            info[15] = "-";
            info[16] = "-";
        }

        return info;
    }

    /**
     * getValues
     * 得到键对应的值,若无则填充"0"
     * @param sucgKey String
     * @return String[]
     */
    private String[] getValues(String sucgKey)
    {
        System.out.println("key:"+sucgKey);

        String[] values = (String[]) mHashMap.get(sucgKey);
        if(values == null)
        {
            values = new String[10];
            for(int i = 0; i < values.length; i++)
            {
                values[i] = "0";
            }
        }

        return values;
    }

    private boolean checkData()
    {
        mStartDate = (String) mTransferData
                           .getValueByName(FeeConst.STARTDATE);  //开始日期
        mEndDate = (String) mTransferData.getValueByName(FeeConst.ENDDATE);  //截至日期
        mManageCom = (String) mTransferData
                           .getValueByName(FeeConst.MANAGECOM);  //管理机构

        if(mStartDate == null || mStartDate.equals("")
           || mEndDate == null || mEndDate.equals("")
            || mManageCom == null || mManageCom.equals(""))
       {
           CError tError = new CError();
           tError.moduleName = "PrtContSuccessBL";
           tError.functionName = "dealData";
           tError.errorMessage = "应收起止日期个和管理机构均不能为空";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return false;
       }

        return true;
    }

    /**
     * 得到录入的数据
     * @param data VData：包括TransferData对象和GlobalInput对象
     * @return boolean: 成功true，否则false
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        mTransferData = (TransferData) data
                        .getObjectByObjectName("TransferData", 0);

        if(mGI == null || mGI.Operator == null || mGI.ManageCom == null
            || mTransferData == null)
        {
            CError tError = new CError();
            tError.moduleName = "PrtContSuccessBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * 处理业务逻辑，生成打印数据XmlExport。
     * @return boolean:操作成功true，否则false
     */
    private boolean dealData()
    {
        xmlexport.createDocument("PrtContSuccess.vts", "printer");

        if(!dealHead())
        {
            return false;
        }

        if(!dealUnSpecial())
        {
            return false;
        }

//        if(!dealMJ())
//        {
//            return false;
//        }

        if(!createXmlExport())
        {
            return false;
        }

        xmlexport.outputDocumentToFile("D:\\", "PrtContSuccess");

        return true;
    }

    /**
     * dealMJ
     *生成满期结算清单
     * @return boolean
     */
    private boolean dealMJ()
    {
        String sql =
        //续保未结案
            "select a, sum(b),sum(c),sum(d),sum(e),sum(f),sum(g),sum(h),sum(i),sum(j),sum(k) from"
            + "( "
            + "    select ManageCom a, count(distinct a.GrpContNo) b, (select sum(InsuAccBala) from LCInsureAcc where GrpPolNo = a.GrpPolNo) c,"
            + "    	0 d,0 e,0 f,0 g,0 h,0 i,0 j,0 k"
            + "    from LCPol a, LMRiskApp b "
            + "    where a.riskCode = b.riskCode"
            + "    	 and b.riskType3 = '7' and b.risktype != 'M'"
            + "    	 and a.EndDate between '" + mStartDate + "' and '" + mEndDate + "'"
            + manageComPart()
            + agentPart("a")
            + groupPart("a")
            + " and not exists (select 1 from LPEdorEspecialData x, lgwork y where x.edorno = y.workno and y.contno = a.grpcontno "
            + " and  x.EdorType = 'MJ' and x.DetailType = 'ENDTIME_DEAL' and x.EdorValue = '2' and y.statusno ='5')"
            + "    group by a.ManageCom, a.GrpPolNo"
            + "    union all"
            //续保成功
            + "    select a.ManageCom a, count(distinct a.EndorsementNo) b, abs(sum(GetMoney)) c,"
            + "    	0 d,0 e,0 f,0 g,0 h,0 i,0 j,0 k"
            + "    from LJAGetEndorse a, LPPol b "
            + "    where b.EdorType = a.FeeOperationType"
            + "    	 and b.EdorNo = a.EndorsementNo"
            + "    	 and a.FeeOperationType = 'MJ'"
            + "    	 and b.EndDate between '" + mStartDate + "' and '" + mEndDate + "'"
            + "      and b.GrpPolNo = a.GrpPolNo" 
            + "    	 and a.ManageCom like '" + mManageCom + "%' "
            + groupPart("a")
            + agentPart("b")
            + "    group by a.ManageCom, a.GrpContNo"
            + "    union all "
            //续保
            + "    select a.ManageCom a, 0 b,0 c, count(distinct b.EdorNo) d, abs(sum(a.GetMoney)) e, "
            + "    	days(a.MakeDate) - days((select MakeDate from LGWork where WorkNo = a.EndorsementNo)) f, "
            + "    	0 g,0 h,0 i,0 j,0 k "
            + "    from LJAGetEndorse a, LPEdorEspecialData b ,LPPol c "
            + "    where a.OtherNo = b.EdorNo "
            + "    	 and b.EdorType = 'MJ' " 
            + "      and c.EndDate between '" + mStartDate + "' and '" + mEndDate + "' "
            + "      and c.GrpPolNo = a.GrpPolNo"
            + "      and c.EdorNo = b.EdorNo"
            + "      and c.EdorType = b.EdorType"
            + manageComPart()
            + groupPart("a")
            + agentPart("a")
            + "    	 and b.DetailType = 'ENDTIME_DEAL' "
            + "    	 and b.EdorValue = '1' "
            + "    group by a.ManageCom, a.EndorsementNo, a.MakeDate "
            + "    union all"
            //满期终止
            + "    select a.ManageCom a,0 b,0 c,0 d,0 e,0 f, count(distinct b.EdorNo) g, abs(sum(a.GetMoney)) h,"
            + "    	days(a.MakeDate) - days((select MakeDate from LGWork where WorkNo = a.EndorsementNo)) i,"
            + "    	0 j,0 k"
            + "    from LJAGetEndorse a, LPEdorEspecialData b,LPPol c"
            + "    where a.OtherNo = b.EdorNo"
            + "    	 and b.EdorType = 'MJ'"
            + "      and c.EndDate between '" + mStartDate + "' and '" + mEndDate + "' "
            + "      and c.GrpPolNo = a.GrpPolNo"
            + "      and c.EdorNo = b.EdorNo"
            + "      and c.EdorType = b.EdorType"
            + manageComPart()
            + groupPart("a")
            + agentPart("a")
            + "    	 and b.DetailType = 'ENDTIME_DEAL'"
            + "    	 and b.EdorValue = '2'"
            + "    group by a.ManageCom, a.EndorsementNo, a.MakeDate "
            //未结案
            + "    union all "
            + "    select c.ManageCom a, 0 b,0 c,0 d,0 e,0 f,0 g,0 h,0 i, count(distinct a.WorkNo) j, "
            + "      (select sum(InsuAccBala) from LCInsureAcc  where GrpPolNo = c.GrpPolNo) k "
            + "    from LGWork a, LPEdorEspecialData b, LCPol c "
            + "    where a.WorkNo = b.EdorNo "
            + "    	 and a.ContNo = c.GrpContNo "
            + "    	 and b.EdorType = 'MJ' "
            + "    	 and b.DetailType = 'MJSTATE' "
            + "    	 and b.EdorValue != '0' "
            + "    	 and exists(select 1 from LMRiskApp where RiskCode = c.RiskCode and riskType3 = '7' and risktype !='M') "
            + "    	 and c.EndDate between '" + mStartDate + "' and '" + mEndDate + "' "
            + manageComPart()
            + groupPart("c")
            + agentPart("c")
            + "    group by c.ManageCom, c.GrpPolNo, GrpContNo "
            + ") t "
            + "group by a "
            + "order by a "
            + "with ur ";
            System.out.println(sql);
        ExeSQL e = new ExeSQL();
        SSRS tSSRS = e.execSQL(sql);
        if(e.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "PrtContSuccessBL";
            tError.functionName = "dealMJ";
            tError.errorMessage = "查询特需清单出错";
            mErrors.addOneError(tError);
            System.out.println(e.mErrors.getErrContent());
            System.out.println(tError.errorMessage);
            return false;
        }

        if(tSSRS.getMaxRow() == 0)
        {
            return true;
        }

        String[] sumInfo = null;
        ListTable lt = new ListTable();
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String[] info = getInfoMJ(tSSRS.getRowData(i));
            lt.add(info);

            sumInfo = CommonBL.add(sumInfo, info, 1);
        }
        String[] info = getInfoMJ(sumInfo);
        info[0] = "全系统";
        lt.add(info);

        lt.setName("MJ");

        String[] colNames = {"机构","满期件数","满期帐户金额",
                            "续保件数","续保金额", "时效",
                            "满期终止件数","满期终止给付金额", "时效",
                            "未结案件数","未结案账户金额",
                            "平均时效"};

        xmlexport.addListTable(lt, colNames);

        return true;
    }

    /**
     * 生成特需表行数据
     * @param tSSRS SSRS：查询的到的数据
     * @param i int：行标
     * @return String[]
     */
    private String[] getInfoMJ(String[] row)
    {
        String[] info = new String[COL_NUM];

        info[0] = GetMemberInfo.getComNameByComCode(row[0]);
        info[1] = StrTool.cTrim(row[1]);
        info[2] = StrTool.cTrim(row[2]);
        info[3] = StrTool.cTrim(row[3]);
        info[4] = StrTool.cTrim(row[4]);
        info[5] = StrTool.cTrim(row[5]);
        info[6] = StrTool.cTrim(row[6]);
        info[7] = StrTool.cTrim(row[7]);
        info[8] = StrTool.cTrim(row[8]);
        info[9] = StrTool.cTrim(row[9]);
        info[10] = StrTool.cTrim(row[10]);

        //时效
        String[][] data = new String[2][2];
        data[0][0] = info[3];
        data[0][1] = info[5];
        data[1][0] = info[6];
        data[1][1] = info[8];
        info[11] = String.valueOf(getPeriod(data));

        return info;
    }

    /**
     * 计算data中件数-时效的平均总时效
     * @param data String[][]
     * @return double
     */
    private double getPeriod(String[][] data)
    {
        if(data == null)
        {
            return 0;
        }

        int count = 0;
        int days = 0;
        for(int i = 0; i < data.length; i++)
        {
            count += Integer.parseInt(data[i][0]);
            days += Integer.parseInt(data[i][1]);
        }

        if(count == 0)
        {
            return 0;
        }

        return PubFun.setPrecision(days/count, "0.00");
    }

    /**
     * dealHead
     *生成抬头信息
     * @return boolean
     */
    private boolean dealHead()
    {
        TextTag tag = new TextTag();
        tag.add("ManageCom", mGI.ManageCom);
        tag.add("ManageComName", GetMemberInfo.getComName(mGI.Operator));
        tag.add("Operator", mGI.Operator);
        tag.add("OperatorName", GetMemberInfo.getMemberName(mGI));
        tag.add("PrintDate", PubFun.getCurrentDate());
        tag.add("StartDate", StrTool.cTrim(this.mStartDate));
        tag.add("EndDate", StrTool.cTrim(this.mEndDate));

        String com = StrTool.cTrim(this.mManageCom);
        tag.add("ManageComSelect", com);
        tag.add("ManageComNameSelect",
                com.equals("") ? "" : GetMemberInfo.getComNameByComCode(com));

        String group03 = StrTool.cTrim((String) mTransferData
                                       .getValueByName(FeeConst.GROUP03));
        tag.add("Group01", group03);
        tag.add("Group01Name", getGroupName(group03));
        String agentCode = (String) mTransferData
                           .getValueByName(FeeConst.AgentCode); //销售人员代码
        if(agentCode != null && !agentCode.equals(""))
        {
            tag.add("Agent", agentCode);
            tag.add("AgentName", getAgentName(agentCode));
        }

        xmlexport.addTextTag(tag);

        return true;
    }

    private String getAgentName(String agentCode)
    {
        String sql = "select Name from LAAgent "
                     + "where AgentCode = getAgentCode('" + agentCode + "') ";
        return new ExeSQL().getOneValue(sql);
    }

    /**
     * 得到agentGroup对应名字
     * @param agentGroup String
     * @return String
     */
    private String getGroupName(String agentGroup)
    {
        if("".equals(StrTool.cTrim(agentGroup)))
        {
            return "";
        }

        String sql = "select Name "
                     + "from LABranchGroup "
                     + "where AgentGroup = '" + agentGroup + "' ";
        return new ExeSQL().getOneValue(sql);
    }
    /**
     * dealUnSpecial
     *处理非特需险种的报表
     * @return boolean
     */
    private boolean dealUnSpecial()
    {
        if(!successG())
        {
            return false;
        }

        if(!successP())
        {
            return false;
        }

        if(!waitChargeG())
        {
            return false;
        }

        if(!waitChargeP())
        {
            return false;
        }

        if(!invalidG())

        {
            return false;
        }

        if(!invalidP())
        {
            return false;
        }

        if(!payModeBank())
        {
            return false;
        }

        return true;
    }

    /**
     * invalidP
     *个单失效
     * @return boolean
     */
    private boolean invalidP()
    {
        System.out.println("\n个单失效:");
        String sql = "select ManageCom, LongFlag, count(ContNo),sum(Prem) from "
                     + "( "
                     + "	select distinct ManageCom, a.ContNo ContNo,  "
                     + "	   sum(a.prem) Prem,hasLongRisk(a.ContNo) LongFlag "
                     + "	from LCCont a "
                     + "	where a.StateFlag = '"
                     + BQ.STATE_FLAG_AVAILABLE + "' "
                     + "       and ContType = '1' "
                     + groupPart(null)
                     + agentPart(null)
                     + manageComPart()
                     + " and (a.cardflag = '0' or a.cardflag is null)"
                     + "    and a.PayToDate between '" + mStartDate + "' "
                     + "       and '" + mEndDate + "' "
                     + "	group by ManageCom, a.ContNo "
                     + ") t "
                     + "group by ManageCom, LongFlag  with ur ";
        System.out.println(sql);
        SSRS tSSRS = mExeSQL.execSQL(sql);
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String key = tSSRS.GetText(i, 1) + tSSRS.GetText(i, 2) + INVALID + P;
            System.out.println(key);
            String[] values =
                {tSSRS.GetText(i, 3), tSSRS.GetText(i, 4)};
            mHashMap.put(key, values);
            mHashSet.add(tSSRS.GetText(i, 1));
        }
        //个单银行转帐失效
        sql = "select ManageCom, LongFlag, count(ContNo),sum(Prem) from "
              + "( "
              + "	select distinct ManageCom, a.ContNo ContNo,  "
              + "	   sum(a.prem) Prem,hasLongRisk(a.ContNo) LongFlag "
              + "	from LCCont a "
              + "	where a.StateFlag = '"
              + BQ.STATE_FLAG_AVAILABLE + "' "
              + "       and ContType = '1' "
              + " and a.paymode ='4' "
              + groupPart(null)
              + agentPart(null)
              + manageComPart()
              + " and (a.cardflag = '0' or a.cardflag is null)"
              + "    and a.PayToDate between '" + mStartDate + "' "
              + "       and '" + mEndDate + "' "
              + "	group by ManageCom, a.ContNo "
              + ") t "
              + "group by ManageCom, LongFlag  with ur ";
        System.out.println(sql);
        tSSRS = mExeSQL.execSQL(sql);
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            String key = tSSRS.GetText(i, 1) + tSSRS.GetText(i, 2) + INVALID + P +"Bank";
            System.out.println(key);
            String[] values = {tSSRS.GetText(i, 3), tSSRS.GetText(i, 4)};
            mHashMap.put(key, values);
        }

        return true;
    }

    /**
     * invalidG
     *团单失效
     * @return boolean
     */
    private boolean invalidG()
    {
        System.out.println("\n团单失效:");
        String sql = "select ManageCom, NoNameFlag, count(GrpContNo), sum(Prem) from "
                     + "( "
                     + "	select distinct ManageCom, a.GrpContNo GrpContNo, "
                     + "		sum(a.prem) Prem, isNoNameCont(a.GrpContNo) NoNameFlag "
                     + "	from LCGrpCont a "
                     + "	where a.StateFlag = '"
                     + BQ.STATE_FLAG_AVAILABLE + "' "
                     + "		and (select min(PayToDate) from LCGrpPol "
                     + "            where GrpContNo = a.GrpContNo) between '"
                     + mStartDate + "' and '" + mEndDate + "' "
                     + "        and PayIntv > 0 "
                     + "        and not exists(select 1 from LCGrpPol where GrpContNo = a.GrpContNo and RiskCode in(select RiskCode from LMRiskApp where RiskType3 = '7' and risktype !='M')) "
                     + " and a.cardflag is null "
                     + agentPart(null)
                     + groupPart(null)
                     + manageComPart()
                     + "	group by ManageCom, a.GrpContNo "
                     + ") t "
                     + "group by ManageCom, NoNameFlag  with ur ";
        System.out.println(sql);
        SSRS tSSRS = mExeSQL.execSQL(sql);
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String key = tSSRS.GetText(i, 1) + tSSRS.GetText(i, 2)+INVALID + G;
            System.out.println(key);
            String[] values =
                {tSSRS.GetText(i, 3), tSSRS.GetText(i, 4)};
            mHashMap.put(key, values);
            mHashSet.add(tSSRS.GetText(i, 1));
        }

        return true;
    }

    /**
     * waitChargeG
     *团单待缴费
     * 只要是同时满足一下条件的保单均可认为是待催缴单：
     * 1、保单有效：StateFlag is null or StateFlag = '1'
     * 2、保单为满期：min(LCGrpPol.PayToDate) < EndDate
     * 3、保单还需要缴费：PaytoDate<PayEndDate
     * 4、保单不是顿缴：PayIntv>0
     * @return boolean
     */
    private boolean waitChargeG()
    {
        System.out.println("\n团单待缴费:");
        String sql = "select ManageCom, NoNameFlag, sum(c), sum(Prem) from "
                     + "( "
                     + "	select ManageCom, count(distinct GrpContNo) c, sum(Prem) Prem, isNoNameCont(b.GrpContNo) NoNameFlag "
                     + "	from LCGrpPol b where "
                     //有效
                     + "		(StateFlag is null or StateFlag = '"
                     + BQ.STATE_FLAG_SIGN + "') "
                     + "		and PaytoDate<PayEndDate "
                     + "		and PayIntv>0 "
                     + "        and not exists(select 1 from LCGrpPol where GrpContNo = b.GrpContNo and RiskCode in(select RiskCode from LMRiskApp where RiskType3 = '7' and risktype !='M')) "
                     + " and b.riskcode not in (select riskCode from lmriskapp where risktype ='M') "
                     + groupPart(null)
                     + agentPart(null)
                     + manageComPart()
                     + "        and PayToDate between '" + mStartDate + "' "
                     + "        and '" + mEndDate + "' "
                     + "		and AppFlag = '" + BQ.APPFLAG_SIGN + "' "
                     + "	group by ManageCom, b.GrpContNo "
                     + ") t "
                     + "group by ManageCom, NoNameFlag with ur  ";
        System.out.println(sql);
        SSRS tSSRS = mExeSQL.execSQL(sql);
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String key = tSSRS.GetText(i, 1) + tSSRS.GetText(i, 2) + WAIT_CHARGE + G;
            System.out.println(key);
            String[] values = {tSSRS.GetText(i, 3), tSSRS.GetText(i, 4)};
            mHashMap.put(key, values);
            mHashSet.add(tSSRS.GetText(i, 1));
        }

        return true;
    }

    /**
     * successP
     *个单应收成功
     * @return boolean
     */
    private boolean successP()
    {
        System.out.println("\n个单应收成功:");
        String sql =
            //机构，件数，总金额，是否无名单，收费时长
            "select ManageCom, LongFlag, count(distinct(GetNoticeNo)), "
            + "   sum(sumMoney), sum(DayIntv) from "
            + "( "
            + "	select ManageCom, GetNoticeNo, sum(SumActuPayMoney) sumMoney, "
            + "    hasLongRisk(a.ContNo) LongFlag, "
            + "    (select days(a.MakeDate) - days(MakeDate) from LJSPayB where GetNoticeNo = a.GetNoticeNo) DayIntv "
            + "	from LJAPayPerson a where GetNoticeNo in "
            + "			(select GetNoticeNo from LJSPayB where OtherNoType in('2') and DealState = '1') "
            + "		and PayType = 'ZC' and sumactupaymoney <> 0"
            + groupPart(null)
            + agentPart(null)
            + manageComPart()
            + "    and LastPayToDate between '" + mStartDate + "' "
            + "       and '" + mEndDate + "' "
            + "	group by ManageCom, ContNo, GetNoticeNo, a.MakeDate "
            + ") t "
            + "group by ManageCom, LongFlag  with ur ";
        System.out.println(sql);
        SSRS tSSRS = mExeSQL.execSQL(sql);
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String key = tSSRS.GetText(i, 1) + tSSRS.GetText(i, 2) + SUCCESS + P;
            System.out.println("key:" + key);
            String[] values =
                {tSSRS.GetText(i, 3), tSSRS.GetText(i, 4), tSSRS.GetText(i, 5)};
            mHashMap.put(key, values);
            mHashSet.add(tSSRS.GetText(i, 1));
        }
        //转帐成功件数和保费
        sql =//机构，长短险，件数，总金额
            "select ManageCom, LongFlag, count(distinct(GetNoticeNo)),sum(sumMoney)from "
            + "( "
            + "	select ManageCom, GetNoticeNo, sum(SumActuPayMoney) sumMoney, "
            + "    hasLongRisk(a.ContNo) LongFlag "
            + "	from LJAPayPerson a where GetNoticeNo in "
            + " (select GetNoticeNo from LJSPayB where OtherNoType in('2') and DealState = '1') "
            + "		and PayType = 'ZC'  and sumactupaymoney <> 0"
            + " and exists (select 1 from ljtempfeeClass where tempfeeno = a.Getnoticeno and paymode ='4')"
            + groupPart(null)
            + agentPart(null)
            + manageComPart()
            + "    and LastPayToDate between '" + mStartDate + "' "
            + "       and '" + mEndDate + "' "
            + "	group by ManageCom, ContNo, GetNoticeNo, a.MakeDate "
            + ") t "
            + "group by ManageCom, LongFlag  with ur ";
        System.out.println(sql);
        tSSRS = mExeSQL.execSQL(sql);
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String key = tSSRS.GetText(i, 1) + tSSRS.GetText(i, 2) + SUCCESS + P+"Bank";
            System.out.println("key:" + key);
            String[] values = {tSSRS.GetText(i, 3), tSSRS.GetText(i, 4)};
            mHashMap.put(key, values);
        }
        return true;
    }

    /**
     * 个单银行转帐
     * @return boolean
     */
    private boolean payModeBank()
    {
        System.out.println("\n个单银行转帐:");
        String sql =
            //机构，件数，总金额，是否无名单，收费时长
            "select ManageCom, LongFlag,PayMode,count(distinct(GetNoticeNo)), "
            + "   sum(sumMoney) from "
            + "( "
            + "	select ManageCom, GetNoticeNo, sum(SumActuPayMoney) sumMoney, "
            + "    hasLongRisk(a.ContNo) LongFlag, "
            + "    case when (select max(distinct paymode) from ljtempfeeclass where tempfeeno = a.GetNoticeNo) = '4' then 'M4' else 'MO' end PayMode "
            + "	from LJAPayPerson a where GetNoticeNo in "
            + "			(select GetNoticeNo from LJSPayB where OtherNoType in('2') and DealState = '1') "
            + "		and PayType = 'ZC' "
            + groupPart(null)
            + agentPart(null)
            + manageComPart()
            + "    and LastPayToDate between '" + mStartDate + "' "
            + "       and '" + mEndDate + "' "
            + "	group by ManageCom, ContNo, GetNoticeNo, a.MakeDate "
            + ") t "
            + "group by ManageCom, LongFlag, PayMode  with ur ";
        System.out.println(sql);
        SSRS tSSRS = mExeSQL.execSQL(sql);
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String key = tSSRS.GetText(i, 1) + tSSRS.GetText(i, 2)
                         + tSSRS.GetText(i, 3);
            System.out.println(key);
            String[] values =
                {tSSRS.GetText(i, 4), tSSRS.GetText(i, 5)};
            mHashMap.put(key, values);
            mHashSet.add(tSSRS.GetText(i, 1));
        }

        return true;
    }

    /**
     * 个单待缴费
     *
     * @return boolean
     */
    private boolean waitChargeP()
    {
        System.out.println("\n个单待缴费:");
        String sql = "select ManageCom, LongFlag, sum(c), sum(Prem) from "
                     + "( "
                     + "    select ManageCom, count(distinct ContNo) c, sum(Prem) Prem, "
                     + "		hasLongRisk(a.ContNo) LongFlag "
                     + "    from LCPol a where ContType = '1' "
                    //有效
                     + "		and (a.StateFlag is null or a.StateFlag = '"
                     + BQ.STATE_FLAG_SIGN + "') "
                     //符合续期续保抽档条件
                     + "		and (PayToDate < PayEndDate and PayIntv > 0 "  //续期
                     + "           or exists "  ////续保
                     + "				(select 1 from lmrisk WHERE rnewFlag!='N' and riskcode=a.riskcode) "
                     + "					and exists (select 1 from LMRiskApp where riskCode = a.riskCode and riskType5 = '2')) "
                     + "        and AppFlag = '" + BQ.APPFLAG_SIGN + "' "
                     + " and RiskCode not in (select RiskCode from lmriskapp where risktype ='M' ) "
                     + groupPart(null)
                     + agentPart(null)
                     + manageComPart()
                     + "        and PayToDate between '" + mStartDate + "' "
                     + "        and '" + mEndDate + "' "
                     + "	group by ManageCom, a.ContNo "
                     + ") t "
                     + "group by ManageCom, LongFlag  with ur ";
        System.out.println(sql);
        SSRS tSSRS = mExeSQL.execSQL(sql);
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String key = tSSRS.GetText(i, 1) + tSSRS.GetText(i, 2) + WAIT_CHARGE + P;
            System.out.println(key);
            String[] values =
                {tSSRS.GetText(i, 3), tSSRS.GetText(i, 4)};
            mHashMap.put(key, values);
            mHashSet.add(tSSRS.GetText(i, 1));
        }
        //个单银行待缴费
        sql = "select ManageCom, LongFlag, sum(c), sum(Prem) from "
             + "( "
             + "    select ManageCom, count(distinct ContNo) c, sum(Prem) Prem, "
             + "		hasLongRisk(a.ContNo) LongFlag "
             + "    from LCPol a where ContType = '1' "
            //有效
             + "		and (a.StateFlag is null or a.StateFlag = '"
             + BQ.STATE_FLAG_SIGN + "') "
             //符合续期续保抽档条件
             + "		and (PayToDate < PayEndDate and PayIntv > 0 "  //续期
             + "           or exists "  ////续保
             + "	(select 1 from lmrisk WHERE rnewFlag!='N' and riskcode=a.riskcode) "
             + "	and exists (select 1 from LMRiskApp where riskCode = a.riskCode and riskType5 = '2')) "
             + "        and AppFlag = '" + BQ.APPFLAG_SIGN + "' "
             + " and RiskCode not in (select RiskCode from lmriskapp where risktype ='M' ) "
             + " and exists (select 1 from lccont where contno = a.contno and paymode = '4') "
             + groupPart(null)
             + agentPart(null)
             + manageComPart()
             + "        and PayToDate between '" + mStartDate + "' "
             + "        and '" + mEndDate + "' "
             + "	group by ManageCom, a.ContNo "
             + ") t "
             + "group by ManageCom, LongFlag  with ur ";
       System.out.println(sql);
       tSSRS = mExeSQL.execSQL(sql);
       for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
           String key = tSSRS.GetText(i, 1) + tSSRS.GetText(i, 2) + WAIT_CHARGE +
                        P + "Bank";
           System.out.println(key);
           String[] values = {tSSRS.GetText(i, 3), tSSRS.GetText(i, 4)};
           mHashMap.put(key, values);
       }

        return true;
    }

    /**
     * dealSuccessG
     *团单应收成功
     * @return boolean
     */
    private boolean successG()
    {
        System.out.println("\n团单应收成功:");
        String sql =
            //机构，件数，总金额，是否无名单，收费时长
            "select ManageCom, NoNameFlag, count(distinct(GetNoticeNo)), sum(sumMoney), sum(DayIntv) from "
            + "(   "
            + "	 select ManageCom, GetNoticeNo,sum(SumActuPayMoney) sumMoney, "
            + "		isNoNameCont(a.GrpContNo) NoNameFlag, "
            + "     (select days(a.MakeDate) - days(MakeDate) from LJSPayB where GetNoticeNo = a.GetNoticeNo) DayIntv "
            + "	 from LJAPayGrp a where GetNoticeNo in "
            + "			(select GetNoticeNo from LJSPayB where OtherNoType in('1') and DealState = '1') "
            + "    and not exists(select 1 from LCGrpPol where GrpContNo = a.GrpContNo and RiskCode in(select RiskCode from LMRiskApp where RiskType3 = '7' AND RISKTYPE !='M')) "
            + "    and LastPayToDate between '" + mStartDate + "' "
            + "       and '" + mEndDate + "' "
            + "    and PayType = 'ZC' "
            + agentPart(null)
            + groupPart(null)
            + manageComPart()
            + "	 group by ManageCom, GrpContNo, GetNoticeNo, a.MakeDate "
            + ") t "
            + "group by ManageCom, NoNameFlag with ur ";
        System.out.println(sql);
        SSRS tSSRS = mExeSQL.execSQL(sql);

        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String key = tSSRS.GetText(i, 1) + tSSRS.GetText(i, 2) + SUCCESS + G;
            System.out.println("key:" + key);
            String[] values =
                {tSSRS.GetText(i, 3), tSSRS.GetText(i, 4), tSSRS.GetText(i, 5)};
            mHashMap.put(key, values);

            mHashSet.add(tSSRS.GetText(i, 1));
        }

        return true;
    }

    /**
     * 由于部、区、处的隶属关系，只需要的到最小级别的团体即可
     * @return String
     */
    private String groupPart(String table)
    {
        if(table == null )
        {
            table = "";
        }
        else
        {
            table = table + ".";
        }

        String group01 = (String) mTransferData.getValueByName(FeeConst.GROUP01); //部
        String group02 = (String) mTransferData.getValueByName(FeeConst.GROUP02); //区
        String group03 = (String) mTransferData.getValueByName(FeeConst.GROUP03); //处

        String group = null;
        if(group03 != null && !group03.equals(""))
        {
            group = group03;
        }
        else if(group02 != null && !group02.equals(""))
        {
            group = group02;
        }
        else if(group01 != null && !group01.equals(""))
        {
            group = group01;
        }

        if(group == null)
        {
            return " ";
        }

        return "   and " + table + "agentcode in  (select a.agentcode from laagent a  where agentgroup in ( select agentgroup from labranchgroup    "
                     +" where  BranchSeries like '" + group + "%') )";
    }

    /**
     * 业务员
     * @return String
     */
    private String agentPart(String table)
    {
        if(table == null)
        {
            table = " ";
        }
        else
        {
            table = table + ".";
        }

        String agentCode = (String) mTransferData
                           .getValueByName(FeeConst.AgentCode); //销售人员代码
        if(agentCode != null && !agentCode.equals(""))
        {
            return "   and " + table + "AgentCode = getAgentCode('" + agentCode + "') ";
        }
        return " ";
    }

    /**
     * 页面选择的机构
     * @return String
     */
    private String manageComPart()
    {
        return "   and ManageCom like '" + mManageCom + "%' ";
    }

    public static void main(String[] args)
    {
        GlobalInput mGlobalInput = new GlobalInput(); //操作员信息
        mGlobalInput.Operator = "pa0001";
        mGlobalInput.ManageCom = "86";
        mGlobalInput.ComCode = mGlobalInput.ManageCom;

        TransferData t = new TransferData();
        t.setNameAndValue(FeeConst.STARTDATE, "2007-02-01");
        t.setNameAndValue(FeeConst.ENDDATE, "2007-03-01");
        t.setNameAndValue(FeeConst.MANAGECOM, "86");
        t.setNameAndValue(FeeConst.GROUP03, "");
        t.setNameAndValue(FeeConst.GROUP02, "");
        t.setNameAndValue(FeeConst.GROUP01, "");


        VData d = new VData();
        d.add(mGlobalInput);
        d.add(t);

        PrtContSuccessBL bl = new PrtContSuccessBL();
        if(bl.getXmlExport(d, "") == null)
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
