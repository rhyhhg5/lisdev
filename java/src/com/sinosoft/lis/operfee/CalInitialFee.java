package com.sinosoft.lis.operfee;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LMRiskFeeSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.StrTool;
import com.sinosoft.utility.VData;

/**
*健康宝A（万能型）期交保险费的减少应从最新增加的部分开始减；期交保险费的增加，增加部分都归属第一年度。根据你所举的例子具体初始费用的扣除如下：
*首期期交保险费为3000元，基本保险金额为9万元。第二年期交保险费变更为2000元，第三年期交保险费增加至6000元，第四年期交保险费减少至5000元。
*1、第一年初始费用，3000元*50%=1500元；
*2、第二年初始费用，2000元*25%=500元；
*3、第三年初始费用，2000元*15%+2500元*50%+1500元*5%=300元+1250元+75元=1625元
*（新增的4000元按首年的初始费用比例扣除，同时按投保时的基本保险金额的1/20区分不同的比例）
*4、第四年初始费用，2000元*10%+2500元*25%+500元*5%=200元+625元+25元=850元。
*/

public class CalInitialFee 
{
	private ExeSQL mExeSQL = new ExeSQL();
	
	private String mFirstPrem ; //首期保费
	private double mDuePayPrem = 0; //本次缴费保费
	private int mDuePayPeriod = 1; //本次缴费缴费次数
	private double mBasePrem = 0; //当前基本保费

	private double mCurPrem = 0; //正在计算期数保费-临时变量
	private int mCurPeriod = 1; //正在计算缴费次数-临时变量
	
	private LCPolSchema mLCPolSchema = new LCPolSchema();
	
	private String mPolNo = "";
	
	private double mSumDuePayMoney = 0;
	
	private String mCurPayToDate = "";
	
	
		private String mtypeFlag = "";
	
	private LJSPayPersonSchema mLJSPayPersonSchema = new LJSPayPersonSchema();
	
	public CalInitialFee(LJSPayPersonSchema aLJSPayPersonSchema, double basePrem) 
	{
		mLJSPayPersonSchema.setSchema(aLJSPayPersonSchema);
		mBasePrem = basePrem;
	}
	public CalInitialFee(String tPolNo,double tSumDuePayMoney,String tCurPayToDate , double basePrem,String typeFlag)
	{
		mPolNo = tPolNo;
		mSumDuePayMoney = tSumDuePayMoney;
		mCurPayToDate = tCurPayToDate;
		mBasePrem = basePrem;
		mtypeFlag = typeFlag;
	}	
	
	
	
	public CalInitialFee() 
	{
	}

	private boolean printSplitPremPeriod(Hashtable hash, String key)
	{
		
		String PremPeriod = (String)hash.get(key);
//		System.out.println("PremPeriod == " + PremPeriod);
		String[] sPremPeriod = new String[2];
		sPremPeriod = PremPeriod.split("@");
//		System.out.println("sPremPeriod[0] == "+ sPremPeriod[0]);
		mCurPrem = Double.parseDouble(sPremPeriod[0]);
		mCurPeriod = Integer.parseInt(sPremPeriod[1]);
		System.out.println("curPrem == " + mCurPrem);
		System.out.println("curPeriod == " + mCurPeriod);
		return true;
	}
	

	private boolean splitPremPeriod(Hashtable hash, String key) 
	{
		String PremPeriod = (String) hash.get(key);
		String[] sPremPeriod = new String[2];
		sPremPeriod = PremPeriod.split("@");
		mCurPrem = Double.parseDouble(sPremPeriod[0]);
		mCurPeriod = Integer.parseInt(sPremPeriod[1]);
		return true;
	}
	
	private Hashtable calCurr(double curPrem, Hashtable last)
	{
		double tempPrem = 0;
		Hashtable cur = new Hashtable();
		for(int i=0; i< last.size(); i++){			
			String curKey = String.valueOf(i+1);
			splitPremPeriod(last, curKey);
			if(curPrem <= mCurPrem){
				cur.put(curKey, (String.valueOf(curPrem) + "@" + String.valueOf(mCurPeriod+1)));
				break;
			}else{
				curPrem = curPrem - mCurPrem;
				tempPrem = mCurPrem;
				cur.put(curKey, (String.valueOf(tempPrem) + "@" + String.valueOf(mCurPeriod+1)));
			}
			if(i == (last.size()-1)){
				cur.put(String.valueOf(last.size()+1), (String.valueOf(curPrem) + "@" + "1"));
			}
		}
		return cur;
	}
	
	/**
	 * 获取当前正在计算期数的保费
	 * @param Period
	 * @return
	 */
	private double getCurPrem(int Period) 
	{

		double duePayPrem = 0;
		
		if(Period==mDuePayPeriod)
		{
			duePayPrem = mDuePayPrem;
		}
		else
		{
//			String sql = "select sum(sumduepaymoney) from ljapayperson where PolNo = '" + mLCPolSchema.getPolNo() 
//					+ "' and CurPayToDate - " + Period + " years = '" + mLCPolSchema.getCValiDate() + "' "
//					+ " and PayType = 'ZC' ";
//			解决闰年2月29日生效的保单不能正常核销的问题
			String sql = "select sum(sumduepaymoney) from ljapayperson where PolNo = '" + mLCPolSchema.getPolNo() 
			+ "' and (date('" + mLCPolSchema.getCValiDate() + "') + 1 years) + "+(Period-1)+" years= CurPayToDate "
			+ " and PayType = 'ZC' ";
			System.out.println(sql);
			String sPayPrem = mExeSQL.getOneValue(sql);
			if(!sPayPrem.equals("") && !sPayPrem.equals("null"))
			{
				duePayPrem = Double.parseDouble(sPayPrem);
			}
		}
		
		return duePayPrem;
	}
	
	public Hashtable calTest(){
		
		if(!getInputData())
		{
			return null;
		}
		
		Hashtable last = new Hashtable();
		//假设第一期保费1000.使用HASHTABLE保存，输入格式为：保费@期数
		last.put("1", mFirstPrem+"@1");
		
		for (int i = 1; i <= last.size(); i++) 
		{
			printSplitPremPeriod(last, String.valueOf(i));
		}
		
		Hashtable cur = new Hashtable();
		double minusPrem = 0;//额外保费
		for(int i=2; i<=mDuePayPeriod; i++)
		{//从第二期开始计算
			
			double curPrem = getCurPrem(i);
			
			if(curPrem==0)
			{
				System.out.println("获取第"+i+"期保费收取的初始费用失败");
				return null;
			}
			
			System.out.println("==========第"+i+"期保费为："+ curPrem +"，收取初始费用情况：");
			if (curPrem > mBasePrem) 
			{
				minusPrem = curPrem - mBasePrem;
				curPrem = mBasePrem;
				System.out.println("额外保费保费为："+ minusPrem+",基本保费为："+curPrem);
			}
			cur = calCurr(curPrem,last);
			
			for (int j = 1; j <= cur.size(); j++) 
			{
				printSplitPremPeriod(cur, String.valueOf(j));
			}
			last = cur;
		}
		
		return cur;
	}
	
	public Hashtable calMinus(){
		
		if(!getInputData())
		{
			return null;
		}

//		为了额外保费计算重置mFirstPrem
		if(Double.parseDouble(mFirstPrem)>mBasePrem){
			mFirstPrem=	String.valueOf(Double.parseDouble(mFirstPrem)-mBasePrem);
		}else{
			mFirstPrem="0";
		}
		Hashtable last = new Hashtable();
		//假设第一期额外保费为1000.使用HASHTABLE保存，输入格式为：保费@期数
		last.put("1", mFirstPrem+"@1");
		
		for (int i = 1; i <= last.size(); i++) 
		{
			printSplitPremPeriod(last, String.valueOf(i));
		}
		
		Hashtable cur = new Hashtable();
		for(int i=2; i<=mDuePayPeriod; i++)
		{//从第二期开始计算
			
			double curPrem = getCurPrem(i);
			
			if(curPrem==0)
			{
				System.out.println("获取第"+i+"期保费收取的续期费用失败");
				return null;
			}
			
			System.out.println("==========第"+i+"期保费为："+ curPrem +"，其额外保费收取初始费用情况：");
//			每次将额外保费放入hashtable
			if(curPrem>mBasePrem){
				curPrem=curPrem-mBasePrem;
			}else{
				curPrem=0;
			}
			System.out.println("本次额外保费为："+ curPrem);
			cur = calCurr(curPrem,last);
			
			for (int j = 1; j <= cur.size(); j++) 
			{
				printSplitPremPeriod(cur, String.valueOf(j));
			}
			last = cur;
		}
		
		return cur;
	}
	
	
	private boolean getInputData() 
	{
		if(mtypeFlag!=null&&!"".equals(mtypeFlag)&&("FX".equals(mtypeFlag.toUpperCase())||"MF".equals(mtypeFlag.toUpperCase()))){
			LCPolDB tLCPolDB = new LCPolDB();
			tLCPolDB.setPolNo(mPolNo);
			tLCPolDB.getInfo();
			mLCPolSchema.setSchema(tLCPolDB.getSchema());
			
			if(mLCPolSchema.getPayIntv() != 12)
			{
				System.out.println("目前系统不支持非年缴方式的初始费用计算");
				return false;
			}
			
			mDuePayPrem = mSumDuePayMoney;
			
			String tPayDate =mCurPayToDate;
			mDuePayPeriod = PubFun.calInterval(mLCPolSchema.getCValiDate(),tPayDate,"M")/mLCPolSchema.getPayIntv();
			
			String sql = "select sum(sumduepaymoney) from ljapayperson where PolNo = '" + mPolNo 
			+ "' and CurPayToDate - 1 year <= '" + mLCPolSchema.getCValiDate() + "' and  paytype = 'ZC'";
			
			mFirstPrem = mExeSQL.getOneValue(sql); //首期保费
//			mFirstPrem = "8000.0"; //首期保费
			System.out.println("==========第1期保费为："+ mFirstPrem +"。");
			
		}else{
		LCPolDB tLCPolDB = new LCPolDB();
		tLCPolDB.setPolNo(mLJSPayPersonSchema.getPolNo());
		tLCPolDB.getInfo();
		mLCPolSchema.setSchema(tLCPolDB.getSchema());
		
		if(mLCPolSchema.getPayIntv() != 12)
		{
			System.out.println("目前系统不支持非年缴方式的初始费用计算");
			return false;
		}
		
		mDuePayPrem = mLJSPayPersonSchema.getSumDuePayMoney();
		
		String tPayDate =mLJSPayPersonSchema.getCurPayToDate();
		mDuePayPeriod = PubFun.calInterval(mLCPolSchema.getCValiDate(),tPayDate,"M")/mLCPolSchema.getPayIntv();
		/*当闰年遇上闰年时通过PubFun.calInterval()计算期数会出错，
		为防止未知风险暂不使用Ljspyperson.paycount代替计算期数
		--20150320	lzy
		*/
		if(isLeapYear(mLCPolSchema.getCValiDate(),tPayDate)){
			mDuePayPeriod = calInterval(mLCPolSchema.getCValiDate(),tPayDate,"M")/mLCPolSchema.getPayIntv();
		}
		
		String sql = "select sum(sumduepaymoney) from ljapayperson where PolNo = '" + mLJSPayPersonSchema.getPolNo() 
				+ "' and CurPayToDate - 1 year <= '" + mLCPolSchema.getCValiDate() + "' and  paytype = 'ZC'";
		
		mFirstPrem = mExeSQL.getOneValue(sql); //首期保费
		System.out.println("==========第1期保费为："+ mFirstPrem +"。");
		}
		
		return true;
	}
	
	/**
	 * 判断起止日期均为闰年
	 * @param cstartDate
	 * @return
	 */
	private boolean isLeapYear(String cstartDate,String cendDate)
    {
        FDate fDate = new FDate();
        Date startDate = fDate.getDate(cstartDate);
        Date endDate = fDate.getDate(cendDate);
        if (fDate.mErrors.needDealError())
        {
            return false;
        }
        GregorianCalendar sCalendar = new GregorianCalendar();
        sCalendar.setTime(startDate);
        GregorianCalendar eCalendar = new GregorianCalendar();
        eCalendar.setTime(endDate);
        int eYears = eCalendar.get(Calendar.YEAR);
        int sYears = sCalendar.get(Calendar.YEAR);
        if ((sYears % 4) == 0 && (eYears % 4) == 0){
            return true;
        }
        return false;
    }
	
	/**
	 * 万能续期闰年问题计算，根据PubFun.calInterval()修改，仅供续期使用
	 * @param cstartDate
	 * @param cendDate
	 * @param unit
	 * @return
	 */
	private int calInterval(String cstartDate, String cendDate, String unit)
    {
        FDate fDate = new FDate();
        Date startDate = fDate.getDate(cstartDate);
        Date endDate = fDate.getDate(cendDate);
        if (fDate.mErrors.needDealError())
        {
            return 0;
        }

        int interval = 0;

        GregorianCalendar sCalendar = new GregorianCalendar();
        sCalendar.setTime(startDate);
        int sYears = sCalendar.get(Calendar.YEAR);
        int sMonths = sCalendar.get(Calendar.MONTH);
        int sDays = sCalendar.get(Calendar.DAY_OF_MONTH);
        int sDaysOfYear = sCalendar.get(Calendar.DAY_OF_YEAR);

        GregorianCalendar eCalendar = new GregorianCalendar();
        eCalendar.setTime(endDate);
        int eYears = eCalendar.get(Calendar.YEAR);
        int eMonths = eCalendar.get(Calendar.MONTH);
        int eDays = eCalendar.get(Calendar.DAY_OF_MONTH);
        int eDaysOfYear = eCalendar.get(Calendar.DAY_OF_YEAR);

        if (StrTool.cTrim(unit).equals("Y"))
        {
            interval = eYears - sYears;

            if (eMonths < sMonths)
            {
                interval--;
            }
            else
            {
                if (eMonths == sMonths && eDays < sDays)
                {
                    interval--;
                    if (eMonths == 1)
                    { //如果同是2月，校验润年问题
                        if ((sYears % 4) == 0 && (eYears % 4) != 0)
                        { //如果起始年是润年，终止年不是润年
                            if (eDays == 28)
                            { //如果终止年不是润年，且2月的最后一天28日，那么补一
                                interval++;
                            }
                        }
                    }
                }
            }
        }
        if (StrTool.cTrim(unit).equals("M"))
        {
            interval = eYears - sYears;
            interval = interval * 12;
            interval = eMonths - sMonths + interval;

            if (eDays < sDays)
            {
                interval--;
                //eDays如果是月末，则认为是满一个月
                int maxDate = eCalendar.getActualMaximum(Calendar.DATE);
                if (eDays == maxDate)
                {
                    interval++;
                    if (sDays == 1)
                    {
                        interval++;
                    }

                }
                //起止年都是闰年，因续期是年交的原因，补一
                if ((sYears % 4) == 0 && (eYears % 4) == 0 && eDays == 28){
                	interval++;
                }
            }
        }
        if (StrTool.cTrim(unit).equals("D"))
        {
            interval = eYears - sYears;
            interval = interval * 365;

            interval = eDaysOfYear - sDaysOfYear + interval;

            // 处理润年
            int n = 0;
            eYears--;
            if (eYears > sYears)
            {
                int i = sYears % 4;
                if (i == 0)
                {
                    sYears++;
                    n++;
                }
                int j = (eYears) % 4;
                if (j == 0)
                {
                    eYears--;
                    n++;
                }
                n += (eYears - sYears) / 4;
            }
            if (eYears == sYears)
            {
                int i = sYears % 4;
                if (i == 0)
                {
                    n++;
                }
            }
            interval += n;
        }
        return interval;
    }
	
	public int getMDuePayPeriod()
	{
		return mDuePayPeriod;
	}
	
	public static void main(String[] args)
	{
	}

}
