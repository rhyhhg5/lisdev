package com.sinosoft.lis.operfee;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.utility.StrTool;
import java.io.File;

/**
 * <p>
 * Title: 保险业务系统
 * </p>
 * <p>
 * Description: 应收通知书批量打印
 * </p>
 * <p>
 * Copyright: Copyright (c) 2007
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author
 */
public class NewGrpDueFeeBatchPrtBL {
	private String mOperate;

	public CErrors mErrors = new CErrors();

	private VData mResult = new VData();

	private GlobalInput mG = new GlobalInput();

	public NewGrpDueFeeBatchPrtBL() {
	}

	private LDSysVarSchema mLDSysVarSchema = new LDSysVarSchema();

	private LJSPayBSet mLJSPayBSet = new LJSPayBSet();

	private boolean operFlag = true;

	private String strLogs = "";

	private String Content = "";

	private String FlagStr = "";

	private String mxmlFileName = "";

	private int mCount = 0;

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	private MMap mMap = new MMap();

	private PubSubmit tPubSubmit = new PubSubmit();
	
	private LOPRTManagerSet mLOPRTManagerSet = new LOPRTManagerSet();

	/**
	 * 传输数据的公共方法
	 * 
	 * @param: cInputData 输入的数据 cOperate 数据操作
	 * @return:
	 */
	public boolean submitData(VData cInputData, String cOperate) {
		cInputData = (VData) cInputData.clone();
		this.mOperate = cOperate;
		if (!getInputData(cInputData)) {
			return false;
		}

		if (!dealData()) {
			return false;
		}
		
		tPubSubmit = new PubSubmit();
		if (!tPubSubmit.submitData(mInputData, "")) {
			if (tPubSubmit.mErrors.needDealError()) {
				buildError("submitData", "保存再保计算结果出错!");
			}
			return false;
		}

		return true;
	}

	public VData getResult() {
		return mResult;
	}

	private boolean getInputData(VData cInputData) {
		mLJSPayBSet = (LJSPayBSet) cInputData.getObjectByObjectName(
				"LJSPayBSet", 0);
		if (mLJSPayBSet == null) { // （服务事件关联表）
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GrpDueFeeBatchPrtBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入的数据为空!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
				"GlobalInput", 0));
		mLDSysVarSchema.setSchema((LDSysVarSchema) cInputData
				.getObjectByObjectName("LDSysVarSchema", 0));
		return true;
	}

	public int getCount() {
		return mCount;
	}

	/**
	 * 数据操作类业务处理 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean dealData() {
		if (!prepareOutputData()) {
			return false;
		}
		 mCount = mLOPRTManagerSet.size();
         VData tVData = new VData();
         tVData.add(mG);
         tVData.add(mLOPRTManagerSet);
         PDFPrintBatchManagerBL tPDFPrintBatchManagerBL = new PDFPrintBatchManagerBL();
         if(!tPDFPrintBatchManagerBL.submitData(tVData,mOperate)){
              mErrors.addOneError(tPDFPrintBatchManagerBL.mErrors.getError(1));
              return false;
         }	
		return true;
	}

	public void buildError(String szFunc, String szErrMsg) {
		CError cError = new CError();
		cError.moduleName = "GrpDueFeeBatchPrtBL";
		cError.functionName = szFunc;
		cError.errorMessage = szErrMsg;
		this.mErrors.addOneError(cError);
	}

	public static void main(String[] args) {
		GlobalInput tGlobalInput = new GlobalInput();
		tGlobalInput.ComCode = "86";
		tGlobalInput.ManageCom = "86";
		tGlobalInput.Operator = "001";
		LDSysVarSchema mLDSysVarSchema = new LDSysVarSchema();
		mLDSysVarSchema.setSysVarValue("E:\\workspace\\ui\\");

		LJSPayBSet tLJSPayBSet = new LJSPayBSet();

		LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
		tLJSPayBSchema.setGetNoticeNo("31000006028");
		tLJSPayBSet.add(tLJSPayBSchema);
		LJSPayBSchema tLJSPayBSchema2 = new LJSPayBSchema();
		tLJSPayBSchema2.setGetNoticeNo("31000005931");
		tLJSPayBSet.add(tLJSPayBSchema2);
		LJSPayBSchema tLJSPayBSchema3 = new LJSPayBSchema();
		tLJSPayBSchema3.setGetNoticeNo("31000000575");
		tLJSPayBSet.add(tLJSPayBSchema3);

		VData aVData = new VData();
		aVData.add(tGlobalInput);
		aVData.add(tLJSPayBSet);
		aVData.add(mLDSysVarSchema);
		/*
		 * String aUserCode = "000011"; VData aVData = new VData();
		 * aVData.addElement(aUserCode);
		 */
		GrpDueFeeBatchPrtBL tGrpDueFeeBatchPrtBL = new GrpDueFeeBatchPrtBL();
		tGrpDueFeeBatchPrtBL.submitData(aVData, "PRINT");
		int tCount = tGrpDueFeeBatchPrtBL.getCount();
		System.out.println("~~~~~~~~~~~~~~~~~~~" + tCount);
		String tFileName[] = new String[tCount];
		VData tResult = new VData();
		tResult = tGrpDueFeeBatchPrtBL.getResult();
		tFileName = (String[]) tResult.getObject(0);
		System.out.println("~~~~~~~~~~~~~~~~~~~2" + tFileName);

	}
	/**
	 * 准备往后层输出所需要的数据 输出：如果准备数据时发生错误则返回false,否则返回true
	 */
	private boolean prepareOutputData() {
		mCount = mLJSPayBSet.size();
		String mXmlFileName[] = new String[mCount];
		for (int i = 1; i <= mCount; i++) {
			LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
			LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
			tLOPRTManagerDB.setCode("91");
			System.out.println("*************"
					+ mLJSPayBSet.get(i).getGetNoticeNo());
			tLOPRTManagerDB.setStandbyFlag2(mLJSPayBSet.get(i)
					.getGetNoticeNo());
			tLOPRTManagerSchema = tLOPRTManagerDB.query().get(1);
			if (tLOPRTManagerSchema == null) {
				System.out
						.println("*******LOPRTManagerSchema is null******");
				String GrpContNo = "";
				LJSPayBDB tLJSPayBDB = new LJSPayBDB();
				tLJSPayBDB.setGetNoticeNo(mLJSPayBSet.get(i)
						.getGetNoticeNo());
				System.out
						.println("*******mLJSPayBSet.get(i).getGetNoticeNo()******"
								+ mLJSPayBSet.get(i).getGetNoticeNo());
				if (!tLJSPayBDB.getInfo()) {
					// @@错误处理
					CError tError = new CError();
					tError.moduleName = "dealData";
					tError.functionName = "getInputData";
					tError.errorMessage = "传入的数据为空!";
					this.mErrors.addOneError(tError);
					continue;
				} else {
					GrpContNo = tLJSPayBDB.getOtherNo();
					System.out.println("*******ContNo is ******"
							+ GrpContNo);
				}

				LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
				tLCGrpContSchema.setGrpContNo(GrpContNo);

				VData xVData = new VData();
				xVData.addElement(tLCGrpContSchema);
				xVData.addElement(mG);
				xVData.addElement(tLJSPayBDB);

				GrpDueFeePrintBL tGrpDueFeePrintBL = new GrpDueFeePrintBL();
				if (!tGrpDueFeePrintBL.submitData(xVData, "INSERT")) {
					operFlag = false;
					Content = tGrpDueFeePrintBL.mErrors.getErrContent();
				}
				tLOPRTManagerDB.setStandbyFlag2(mLJSPayBSet.get(i)
						.getGetNoticeNo());
				tLOPRTManagerSchema = tLOPRTManagerDB.query().get(1);
			}
			// 下面封装要更新的打印管理信息
			tLOPRTManagerSchema.setStateFlag("1");
			tLOPRTManagerSchema.setPrintTimes(tLOPRTManagerSchema
					.getPrintTimes() + 1);
			tLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
			tLOPRTManagerSchema.setDoneTime(PubFun.getCurrentTime());
			mLOPRTManagerSet.add(tLOPRTManagerSchema);

			mxmlFileName = StrTool.unicodeToGBK("0"
					+ tLOPRTManagerSchema.getCode())
					+ "-"
					+ tLOPRTManagerSchema.getPrtSeq()
					+ "-"
					+ tLOPRTManagerSchema.getOtherNo();
			mXmlFileName[i - 1] = mxmlFileName;

			XmlExport txmlExport = (XmlExport) mResult
					.getObjectByObjectName("XmlExport", 0);
			if (txmlExport == null) {
				FlagStr = "Fail";
				Content = "印刷号" + tLOPRTManagerSchema.getPrtSeq()
						+ "没有得到要显示的数据文件！";
				strLogs = strLogs + Content;
				continue;
			}
			if (operFlag == true) {
				File f = new File(mLDSysVarSchema.getSysVarValue());
				f.mkdir();
				System.out.println("PATH : "
						+ mLDSysVarSchema.getSysVarValue());
				System.out.println("PATH2 : "
						+ mLDSysVarSchema.getSysVarValue()
								.substring(
										0,
										mLDSysVarSchema.getSysVarValue()
												.length() - 1));
				txmlExport.outputDocumentToFile(
						mLDSysVarSchema.getSysVarValue()
								.substring(
										0,
										mLDSysVarSchema.getSysVarValue()
												.length() - 1)
								+ File.separator
								+ "printdata"
								+ File.separator
								+ "data"
								+ File.separator
								+ "brief" + File.separator, mxmlFileName);
			}
		}
		return true;
	}
}
