package com.sinosoft.lis.operfee;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.rmi.*;

import javax.xml.rpc.*;

import com.sinosoft.lis.message.*;

import java.util.*;

/**
 * <p>Title: 客户续期短信通知</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class CpayRnewMsgBL {
    private GlobalInput mG = new GlobalInput();
    private String mCurrentDate = PubFun.getCurrentDate();
    private ExeSQL mExeSQL = new ExeSQL();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public CpayRnewMsgBL() {}

    private boolean getInputData(VData cInputData) 
    {
    	mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        return true;
    }

    public boolean submitData(VData cInputData, String cOperate) {
        cInputData = (VData) cInputData.clone();

        if (!getInputData(cInputData)) 
        {
        	mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0));
        	if(mG.Operator==null || mG.Operator.equals("") || mG.Operator.equals("null"))
        	{
        		mG.Operator="001";
        	}
        	if(mG.ManageCom==null || mG.ManageCom.equals("") || mG.ManageCom.equals("null"))
        	{
        		mG.ManageCom="86";
        	}
            return false;
        }
        if (cOperate != null && cOperate.trim().equals("XQMSG")) {
            sendMsg();
        } else {
            mErrors.addOneError(new CError("不支持的操作字符串！"));
            return false;
        }
        return true;
    }

    private boolean sendMsg() {
        System.out.println("客户续期短信通知批处理开始......");
        SMSClient smsClient = new SMSClient();
        
        Vector vec = new Vector();
        vec = getMessage();
//        Vector tempVec = new Vector(); //临时变量--实际发短信时传的参数
        //拆分Vector
        System.out.println("---------vec="+vec.size());

        if (!vec.isEmpty()) 
        {
        	for(int i=0;i<vec.size();i++)
            {
//        		tempVec.clear();
//            	tempVec.add(vec.get(i));
            	
            	System.out.print("测试：调用接口！！！");
            	
            	SmsMessagesNew msgNews = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的Messages元素            
            	msgNews = (SmsMessagesNew) vec.elementAt(i);
            	smsClient.sendSMS(msgNews);
            }
        } 
        else 
        {
            System.out.print("续期无符合条件的短信！");
        }
        System.out.println("客户续期短信通知批处理正常结束......");
        return true;
    }

    private Vector getMessage() {
        Vector tVector = new Vector();
        Vector tVectemp = new Vector();
        
//  第一部分:距满期前60天催缴保费的短信
//  包含一条条短信
//  对系统有标识的客户，选择为“是”不发短信提醒。选择为“否”，在满期前60天发送短信至保单所属业务人员。
 try {
        String tSQLsucc =
        	"select c.appntname, "
        	+"(select mobile from laagent where agentcode=c.agentcode),  "
        	+"(select name from laagent where agentcode=c.agentcode),"
        	+"(select (case sex when '1' then '女士' when '0' then '先生' else '先生/女士' end) from laagent where agentcode=c.agentcode),"
        	+"(select lj.lastpaytodate  from ljspaypersonb lj  where lj.contno=c.contno "
        	+" and lj.getnoticeno=a.getnoticeno order by lj.lastpaytodate fetch first 1 row only ), " //业务说的满期日期
        	+"a.managecom,c.contno,a.bankaccno,a.sumduepaymoney,a.getnoticeno, "
            +"(select agentstate from laagent where agentcode=c.agentcode),c.agentcode,c.DueFeeMsgFlag,c.Salechnl "
        	+"from ljspayb a,lccont c  "
        	+"where 1=1  "	
        	+"and c.conttype='1' "
         	+"and a.othernotype='2' and a.dealstate = '0' "
            +"and a.getnoticeno like '31%' "
            +"and a.otherno=c.contno "
            +"and not exists (select 1 from LCCSpec d where d.contno=c.contno and d.SpecType='xq01' and d.EndorsementNo=(select lj.lastpaytodate from ljspaypersonb lj  where lj.contno=c.contno"
            +"     			  and lj.getnoticeno=a.getnoticeno order by lj.lastpaytodate fetch first 1 row only ) ) "
            //测试后要放开	
            +"and (select lj.lastpaytodate -60 days from ljspaypersonb lj  where lj.contno=c.contno"
            +"     and lj.getnoticeno=a.getnoticeno order by lj.lastpaytodate fetch first 1 row only )<=current date " 
            +"and current date < (select lj.lastpaytodate -10 days from ljspaypersonb lj  where lj.contno=c.contno "
            +"                    and lj.getnoticeno=a.getnoticeno order by lj.lastpaytodate fetch first 1 row only ) "
            +"and c.DueFeeMsgFlag is not null and c.DueFeeMsgFlag = 'N' "
            //完成测试后去掉
//            +" and c.contno='000018753000001' "
            +" and not exists (select 1 from ljspaypersonb where getnoticeno=a.getnoticeno and riskcode='320106') "
            +" order by c.contNo fetch first 5000 rows only "
            +"with ur "; 
        System.out.println(tSQLsucc);
        SSRS tMsgSuccSSRS =mExeSQL.execSQL(tSQLsucc);

        for (int i = 1; i <= tMsgSuccSSRS.getMaxRow(); i++) 
        {
        	String tCustomerName = tMsgSuccSSRS.GetText(i, 1);
        	String tAgentMobile = tMsgSuccSSRS.GetText(i, 2);
        	String tAgentName = tMsgSuccSSRS.GetText(i, 3);
        	String tAgentSex = tMsgSuccSSRS.GetText(i, 4);
        	String tPayToDate = tMsgSuccSSRS.GetText(i, 5);
        	String tManageCom = tMsgSuccSSRS.GetText(i, 6);
        	String tContNo = tMsgSuccSSRS.GetText(i, 7);
        	String tBankNo = tMsgSuccSSRS.GetText(i,8);
        	String tPayMoney = tMsgSuccSSRS.GetText(i,9);
        	String tGetNoticeNo = tMsgSuccSSRS.GetText(i,10);
        	String tAgentState=tMsgSuccSSRS.GetText(i,11); //区分业务员是否是离职状态
        	String tAgentCode=tMsgSuccSSRS.GetText(i,12);  //查离职状态的业务员的上级用
        	
//  		  短信提醒标志,选择为“是”不发短信提醒。选择为“否”，在满期前60天发送短信至保单所属业务人员。
        	String tDueFeeMsgFlag = tMsgSuccSSRS.GetText(i,13);
        	System.out.println("DueFeeMsgFlag="+tDueFeeMsgFlag);
        	
        	//2017-5-11 direct by lichang 销售渠道，用于判定部门
        	String tSalechnl = tMsgSuccSSRS.GetText(i,14);
        	
        	if(!"N".equals(tDueFeeMsgFlag))
        	{
        		continue;
        	}
        	
        	if (tManageCom == null || "".equals(tManageCom) ||
                    "null".equals(tManageCom)) {
                    continue;
                }
        	
        	if (tContNo == null || "".equals(tContNo)
					|| "null".equals(tContNo)) {
				continue;
		    }
		    if (tPayToDate == null || "".equals(tPayToDate)
					|| "null".equals(tPayToDate)) {
				continue;
		    }
		    if (tPayMoney == null || "".equals(tPayMoney)
					|| "null".equals(tPayMoney)) {
				continue;
		    }

		    if (tAgentState == null || "".equals(tAgentState) ||
                    "null".equals(tAgentCode)) {
                    continue;
                }
            if (tAgentCode == null || "".equals(tAgentCode) ||
                    "null".equals(tAgentCode)) {
                    continue;
                }
            
//          业务员是否离职状态
            if(Integer.parseInt(tAgentState)>=6) 
            {	
//				SSRS tMsgAgentSSRS=getAgentInfo( tAgentState, tAgentCode);
//				if(tMsgAgentSSRS!=null)
//				{
//					tAgentMobile = tMsgAgentSSRS.GetText(1, 1);
//		        	tAgentName = tMsgAgentSSRS.GetText(1, 2);
//		        	tAgentSex = tMsgAgentSSRS.GetText(1, 3);
//			    }
//				else
//				{
					continue;
//				}
			}
            
            if (tAgentMobile == null || "".equals(tAgentMobile) || "null".equals(tAgentMobile)) 
        	{
                continue;
            }

            if (tAgentName == null || "".equals(tAgentName) || "null".equals(tAgentName)) 
            {
                continue;
            }
                
            if (tAgentSex == null || "".equals(tAgentSex) || "null".equals(tAgentSex)) 
            {
                continue;
            }
            
            if (tSalechnl == null || "".equals(tSalechnl) || "null".equals(tSalechnl)) 
            {
                continue;
            }
							
		   //短信内容
           String tAgentContents=""; 
           String tDepartMent="";
           tDepartMent = getDepartment(tSalechnl);
            
           //现金方式缴费的
        	if (tBankNo == null || "".equals(tBankNo) ||
                    "null".equals(tBankNo)) {        		                   
                    //发送给业务员的短信 
                    SmsMessageNew tAgentMsg = new SmsMessageNew(); //创建SmsMessageNew对象，定义同上文下行短信格式中的Message元素
//                    tAgentContents = "尊敬的"+tAgentName+tAgentSex 
//                                          + "，您好！您的客户"+tCustomerName+"的保单号为"+tContNo+"的保单" 
//                                          + "将于"+tPayToDate+"到期，请您尽快联系您的客户以现金方式缴纳下期保费"+tPayMoney+"元。工作辛苦了，祝您健康。";
                    tAgentContents = "尊敬的"+tAgentName+tAgentSex 
				                   + "，您好！您的客户"+tCustomerName+"的保单(保单号"+tContNo+"，本期保费"+tPayMoney+"元)" 
				                   + "将于"+tPayToDate+"进入缴费期，请您尽快联系您的客户收取续期保费。";
                    System.out.println("现金发送短信：" + tAgentName + tContNo +"日期:"+ tPayToDate+"金额:"+ tPayMoney +"业务员手机号："+ tAgentMobile);
                    tAgentMsg.setReceiver(tAgentMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//                    tAgentMsg.setReceiver("18500905130");
                    tAgentMsg.setContents(tAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                    //和IT讨论后，归类到二级机构
                    //tAgentMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                    tAgentMsg.setOrgCode(tManageCom); //保单机构
                    //添加SmsMessageNew对象                   
                    
                    tVectemp.add(tAgentMsg);
                    
                                        
                    System.out.print("！！！封装短信内容开始！！！");
                    
                    SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的Messages元素
                    msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                    msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                    msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                    msgs.setTaskValue(tDepartMent);
                    msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                    msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                    msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                    msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                    msgs.setMessages((SmsMessageNew[]) tVectemp.toArray(new SmsMessageNew[tVectemp.size()]));
                    tVectemp.clear();
                    tVector.add(msgs);
                    
                    System.out.print("！！！封装短信内容结束！！！");
        		
                }
        	//银行转账方式缴费的
        	else 
        	{                  			
                //发送给业务员的短信 
        		SmsMessageNew tAgentMsg = new SmsMessageNew(); //创建SmsMessageNew对象，定义同上文下行短信格式中的Message元素
//                tAgentContents = "尊敬的"+tAgentName+tAgentSex
//                                      + "，您好！您的客户"+tCustomerName+"的保单号为"+tContNo+"的保单" 
//                                      + "将于"+tPayToDate+"到期，请您尽快联系您的客户将下期保费"+tPayMoney+"元存入缴费帐户。工作辛苦了，祝您健康。";
                tAgentContents = "尊敬的"+tAgentName+tAgentSex 
				               + "，您好！您的客户"+tCustomerName+"的保单(保单号"+tContNo+"，本期保费"+tPayMoney+"元)" 
				               + "将于"+tPayToDate+"进入缴费期，请您尽快联系您的客户收取续期保费。";
                System.out.println("发送短信：" + tAgentName + tContNo +"日期:"+ tPayToDate+"金额:"+ tPayMoney+"业务员手机号："+ tAgentMobile );
                tAgentMsg.setReceiver(tAgentMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//                tAgentMsg.setReceiver("18500905130");
                tAgentMsg.setContents(tAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                //和IT讨论后，归类到二级机构
                //tAgentMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                tAgentMsg.setOrgCode(tManageCom); //保单机构

                //添加SmsMessageNew对象
                tVectemp.add(tAgentMsg);
                
                
                System.out.print("！！！封装短信内容开始！！！");
                
                
                SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的Messages元素
                msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                msgs.setTaskValue(tDepartMent);
                msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                msgs.setMessages((SmsMessageNew[]) tVectemp.toArray(new SmsMessageNew[tVectemp.size()]));
                tVectemp.clear();
                tVector.add(msgs);
                
                
                System.out.print("！！！封装短信内容结束！！！");
        	}
            
                                         
            // 在LCCSpec表中插入值  ********************************************************  
            LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
            tLCCSpecSchema.setGrpContNo(BQ.GRPFILLDATA);
            tLCCSpecSchema.setContNo(tContNo); //主键
            tLCCSpecSchema.setProposalContNo(BQ.GRPFILLDATA);          
            tLCCSpecSchema.setPrtSeq(tGetNoticeNo);//催收的 放 ljspay--getnoticeno                       
            String tLimit = PubFun.getNoLimit(tManageCom);
	        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
	        tLCCSpecSchema.setSerialNo(serNo); //流水号  //主键          	                 
            tLCCSpecSchema.setEndorsementNo(tPayToDate);//lccont--paytodate           
            tLCCSpecSchema.setSpecType("xq01"); //xq01-续期缴费截止日前60天短信类型 xq02-续期缴费截止日前10天短信类型，xq03-续期缴费核销短信类型
            tLCCSpecSchema.setSpecCode("003");//001-投保人 002-被保人 003-业务员 
            tLCCSpecSchema.setOperator(mG.Operator);
            tLCCSpecSchema.setSpecContent(tAgentContents+"业务员手机号："+ tAgentMobile);//短信内容
            tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
            tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
            tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
            tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
            tLCCSpecSchema.getDB().insert();           
        }
        System.out.println("1------------"+tVector.size());
        
	} catch (Exception e) {
			System.out.println("CpayRnewMsgBL.java报错");
			e.printStackTrace();
	}
//  第二部分:距满期前10天催缴保费的短信
//  按缴费方式分为银行转账或者现金两种方式
//  每种方式同样包含两条短信
//  需要发给业务员的和客户，如果为“是”，发给客户和业务员，如果为“否”只发给业务员
   
 try{
       String tSQLnotify = "select "
//				       	 + "(select mobile from lcaddress x ,lcappnt y where x.customerno = y.appntno and x.addressno = y.addressno and y.contno = c.contno fetch first 1 rows only),  "
    	   				 +" mobile(c.contno), "
				       	 + "c.appntname,  "
				       	 + "(case c.appntsex when '1' then '女士' when '0' then '先生' else '先生/女士' end),  "
				       	 + "(select mobile from laagent where agentcode=c.agentcode),  "
				       	 + "(select name from laagent where agentcode=c.agentcode),"
				       	 + "(select (case sex when '1' then '女士' when '0' then '先生' else '先生/女士' end) from laagent where agentcode=c.agentcode),"
				       	 + "(select lj.lastpaytodate  from ljspaypersonb lj  where lj.contno=c.contno "
				       	 + " and lj.getnoticeno=a.getnoticeno order by lj.lastpaytodate fetch first 1 row only ) ," 
				       	 + "c.contno,a.managecom,a.bankaccno,a.sumduepaymoney,a.getnoticeno, "
				       	 + "(select agentstate from laagent where agentcode=c.agentcode),c.agentcode, c.DueFeeMsgFlag, c.salechnl," 
				       	 + "(select r.riskname from lmriskapp r, lcpol p where p.contno = c.contno and r.riskcode = p.riskcode and r.subriskflag = 'M' and r.risktype6 = '1' and r.riskcode not in ('340501','340601') order by r.riskperiod fetch first 1 rows only) "
				       	 + "from ljspayb a,lccont c  "
				       	 + "where 1=1  "	
				       	 + "and c.conttype='1' "
				         + "and a.othernotype='2' and a.dealstate = '0' "
				         + "and a.getnoticeno like '31%' "
				         + "and a.otherno=c.contno "
				         + "and not exists (select 1 from LCCSpec d where d.contno=c.contno and d.SpecType='xq02' and d.EndorsementNo=(select lj.lastpaytodate  from ljspaypersonb lj  where lj.contno=c.contno "
				       	 + " 				and lj.getnoticeno=a.getnoticeno order by lj.lastpaytodate fetch first 1 row only )) "
				        //测试后要放开	
				         + "and (select lj.lastpaytodate -10 days from ljspaypersonb lj  where lj.contno=c.contno"
				         + "     and lj.getnoticeno=a.getnoticeno order by lj.lastpaytodate fetch first 1 row only )<= current date " 
				         + "and current date <= (select lj.lastpaytodate from ljspaypersonb lj  where lj.contno=c.contno "
				         + "                     and lj.getnoticeno=a.getnoticeno order by lj.lastpaytodate fetch first 1 row only )"
				         + " and not exists (select 1 from ljspaypersonb where getnoticeno=a.getnoticeno and riskcode='320106') "
//				       	 + " and c.contno in ('000018753000001')"  //014060643000002
				         + " order by c.contNo "
				        //下2行完成测试后去掉
//				         + "fetch first 10 rows only "
				         + " with ur ";
       
        SSRS tMsgNotifySSRS =mExeSQL.execSQL(tSQLnotify);
        System.out.println(tMsgNotifySSRS.getMaxRow()+"tMsgSuccSSRS.getMaxRow()");
        for (int i = 1; i <= tMsgNotifySSRS.getMaxRow(); i++) 
        {
        	String tCustomerMobile = tMsgNotifySSRS.GetText(i, 1);
        	String tCustomerName = tMsgNotifySSRS.GetText(i, 2);
        	String tCustomerSex = tMsgNotifySSRS.GetText(i, 3);
        	String tAgentMobile = tMsgNotifySSRS.GetText(i, 4);
        	String tAgentName = tMsgNotifySSRS.GetText(i, 5);
        	String tAgentSex = tMsgNotifySSRS.GetText(i, 6);
        	String tPayToDate = tMsgNotifySSRS.GetText(i, 7);
        	String tManageCom = tMsgNotifySSRS.GetText(i, 9);
        	String tContNo = tMsgNotifySSRS.GetText(i, 8);
        	String tBankNo = tMsgNotifySSRS.GetText(i,10);
        	String tPayMoney = tMsgNotifySSRS.GetText(i,11);
        	String tGetNoticeNo = tMsgNotifySSRS.GetText(i,12);
        	String tAgentState=tMsgNotifySSRS.GetText(i,13); //区分业务员是否是离职状态 
        	String tAgentCode=tMsgNotifySSRS.GetText(i,14);  //查离职状态的业务员的上级用
        	
//		  短信提醒标志,前10天，如果为“是”，发给客户和业务员，如果为“否”只发给业务员
        	String tDueFeeMsgFlag = tMsgNotifySSRS.GetText(i,15);
        	System.out.println("DueFeeMsgFlag="+tDueFeeMsgFlag);
        	
        	//2017-5-11 direct by lichang 销售渠道，用于判定部门
        	String tSalechnl = tMsgNotifySSRS.GetText(i,16);
        	String riskName = tMsgNotifySSRS.GetText(i, 17);
        	
        	if (tManageCom == null || "".equals(tManageCom) ||
                    "null".equals(tManageCom)) {
                    continue;
                }
        	
        	if (tContNo == null || "".equals(tContNo) ||
                    "null".equals(tContNo)) {
                    continue;
                }

            if (tPayToDate == null || "".equals(tPayToDate) ||
                    "null".equals(tPayToDate)) {
                    continue;
                }
            
            if (tPayMoney == null || "".equals(tPayMoney) ||
                    "null".equals(tPayMoney)) {
                    continue;
                }
            
            if (tSalechnl == null || "".equals(tSalechnl) ||
                    "null".equals(tSalechnl)) {
                    continue;
                }
        	
            boolean customerFlag = true;
            boolean agentFlag = true;
            
            if (tCustomerMobile == null || "".equals(tCustomerMobile) ||
                    "null".equals(tCustomerMobile)) {
            	customerFlag = false;
                }
            if (tCustomerName == null || "".equals(tCustomerName) ||
                    "null".equals(tCustomerName)) {
            	customerFlag = false;
                }
            
            if (tCustomerSex == null || "".equals(tCustomerSex) ||
                "null".equals(tCustomerSex)) {
            	customerFlag = false;
            }
            
            if(!"N".equals(tDueFeeMsgFlag) && customerFlag)
            {
            	String tCustomerContents = "";
            	String tDepartMent="";
                tDepartMent = getDepartment(tSalechnl);
//              现金方式缴费的
            	if (tBankNo == null || "".equals(tBankNo) || "null".equals(tBankNo)) 
            	{
//					发送给客户的短信     
            		//lijia
            		SmsMessageNew tCustomerMsg = new SmsMessageNew(); //创建SmsMessageNew对象，定义同上文下行短信格式中的Message元素
            		String sql = "select 1 from lcpol where riskcode in (select code from ldcode where codetype='ybkriskcode') and contno ='"+tContNo+"'";
            		String flag = new ExeSQL().getOneValue(sql);
            		if(!"".equals(flag)&&flag!=null){
                        tCustomerContents = "尊敬的"+tCustomerName+tCustomerSex 
                                + "，您好！您购买的"+riskName+"（保单号"+tContNo+"）将于"+tPayToDate+"进入缴费期，为保障您的权益，"
                                + "请您尽快联系公司缴纳下期保费"+tPayMoney+"元。为保障您的权益，我们将按照您购买时选择的缴费方式进行扣费，请确认您的缴费帐户内是否余额充足。祝您健康永驻！客服电话：95591";
                    System.out.println("现金发送短信：" + tCustomerName + tContNo +"日期:"+tPayToDate +"金额:"+ tPayMoney +"客户手机号："+ tCustomerMobile);
            		}else{
                    tCustomerContents = "尊敬的"+tCustomerName+tCustomerSex 
                                             + "，您好！您购买的"+riskName+"（保单号"+tContNo+"）将于"+tPayToDate+"进入缴费期，为保障您的权益，"
                                             + "请您尽快联系公司缴纳下期保费"+tPayMoney+"元，祝您健康。客服电话：95591。";
                    System.out.println("现金发送短信：" + tCustomerName + tContNo +"日期:"+tPayToDate +"金额:"+ tPayMoney +"客户手机号："+ tCustomerMobile);
            		}
                    tCustomerMsg.setReceiver(tCustomerMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//                    tCustomerMsg.setReceiver("18500905130");
                    tCustomerMsg.setContents(tCustomerContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                    //和IT讨论后，归类到二级机构
                    tCustomerMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素

                    //添加SmsMessageNew对象
                    tVectemp.add(tCustomerMsg);
                    
                    SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的Messages元素
                    msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                    msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                    msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                    msgs.setTaskValue(tDepartMent);
                    msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                    msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                    msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                    msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                    msgs.setMessages((SmsMessageNew[]) tVectemp.toArray(new SmsMessageNew[tVectemp.size()]));
                    tVectemp.clear();
                    tVector.add(msgs);
                }
            	//银行转账方式缴费的
            	else 
            	{                  
                    //发送给客户的短信            
            		SmsMessageNew tCustomerMsg = new SmsMessageNew(); //创建SmsMessageNew对象，定义同上文下行短信格式中的Message元素
                    
                    //缴费账户后四位   By LC （续期短信内容调整）
                	String AccNo = null;
                	if(tBankNo != null && !"".equals(tBankNo) && tBankNo.length() > 4){
                		AccNo = tBankNo.substring(tBankNo.length()-4, tBankNo.length());
                	} else {
                		AccNo = tBankNo;
                	}
                	
                	          	
                    tCustomerContents = "尊敬的"+tCustomerName+tCustomerSex 
                                             + "，您好！您购买的"+riskName+"（保单号"+tContNo+"）将于"+tPayToDate+"进入缴费期，为保障您的权益，" 
                                             + "请您尽快将下期保费"+tPayMoney+"元存入您的尾号"+AccNo+"的缴费帐户，祝您健康。客服电话：95591。";
                    System.out.println("发送短信：" + tCustomerName + tContNo +"日期:"+tPayToDate +"金额:"+ tPayMoney+"客户手机号："+ tCustomerMobile +"缴费账户后四位："+AccNo );
                    tCustomerMsg.setReceiver(tCustomerMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//                    tCustomerMsg.setReceiver("18500905130");
                    tCustomerMsg.setContents(tCustomerContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                    //和IT讨论后，归类到二级机构
                    //tCustomerMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                    tCustomerMsg.setOrgCode(tManageCom); //保单机构

                    //添加SmsMessageNew对象
                    tVectemp.add(tCustomerMsg);
                    
                    SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的Messages元素
                    msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                    msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                    msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                    msgs.setTaskValue(tDepartMent);
                    msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                    msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                    msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                    msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                    msgs.setMessages((SmsMessageNew[]) tVectemp.toArray(new SmsMessageNew[tVectemp.size()]));
                    tVectemp.clear();
                    tVector.add(msgs);
            	}
//            	在LCCSpec表中插入值  ********************************************************  
                LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
                tLCCSpecSchema.setGrpContNo(BQ.GRPFILLDATA);
                tLCCSpecSchema.setContNo(tContNo);
                tLCCSpecSchema.setProposalContNo(BQ.GRPFILLDATA);
                tLCCSpecSchema.setPrtSeq(tGetNoticeNo);//催收的 放 ljspay--getnoticeno                              
                String tLimit = PubFun.getNoLimit(tManageCom);
    	        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
    	        tLCCSpecSchema.setSerialNo(serNo); //流水号
                tLCCSpecSchema.setEndorsementNo(tPayToDate);//lccont--paytodate 
                tLCCSpecSchema.setSpecType("xq02"); //xq01-续期缴费截止日前60天短信类型 xq02-续期缴费截止日前10天短信类型，xq03-续期缴费核销短信类型
                tLCCSpecSchema.setSpecCode("001");//001-投保人 002-被保人 003-业务员  
				tLCCSpecSchema.setSpecContent(tCustomerContents+"客户手机号："+ tCustomerMobile);//短信内容
                tLCCSpecSchema.setOperator(mG.Operator);
                tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
                tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
                tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
                tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
                tLCCSpecSchema.getDB().insert();
            }
            
            if (tAgentState == null || "".equals(tAgentState) ||
                    "null".equals(tAgentCode)) {
            	agentFlag = false;
                }
            if (tAgentCode == null || "".equals(tAgentCode) ||
                    "null".equals(tAgentCode)) {
            	agentFlag = false;
                }
            
//          业务员是否离职状态
            if(Integer.parseInt(tAgentState)>=6) 
            {	
//				SSRS tMsgAgentSSRS=getAgentInfo( tAgentState, tAgentCode);
//				if(tMsgAgentSSRS!=null)
//				{
//					tAgentMobile = tMsgAgentSSRS.GetText(1, 1);
//		        	tAgentName = tMsgAgentSSRS.GetText(1, 2);
//		        	tAgentSex = tMsgAgentSSRS.GetText(1, 3);
//			    }
//				else
//				{
					agentFlag = false;
//				}
			}
            
            if (tAgentMobile == null || "".equals(tAgentMobile) || "null".equals(tAgentMobile)) 
        	{
            	agentFlag = false;
            }

            if (tAgentName == null || "".equals(tAgentName) || "null".equals(tAgentName)) 
            {
            	agentFlag = false;
            }
                
            if (tAgentSex == null || "".equals(tAgentSex) || "null".equals(tAgentSex)) 
            {
            	agentFlag = false;
            }
            if(agentFlag)
            {
            	String tAgentContents = "";
            	String tDepartMent="";
                tDepartMent = getDepartment(tSalechnl);
//              现金方式缴费的
            	if (tBankNo == null || "".equals(tBankNo) ||
                        "null".equals(tBankNo)) 
            	{
//                  发送给业务员的短信 
            		SmsMessageNew tAgentMsg = new SmsMessageNew(); //创建SmsMessageNew对象，定义同上文下行短信格式中的Message元素
//                    tAgentContents = "尊敬的"+tAgentName+tAgentSex 
//                                          + "，您好！您的客户"+tCustomerName+"的保单号为"+tContNo+"的保单" 
//                                          + "将于"+tPayToDate+"到期，请您尽快联系您的客户以现金方式缴纳下期保费"+tPayMoney+"元。工作辛苦了，祝您健康。";
                    tAgentContents = "尊敬的"+tAgentName+tAgentSex 
				                   + "，您好！您的客户"+tCustomerName+"的保单(保单号"+tContNo+"，本期保费"+tPayMoney+"元)" 
				                   + "将于"+tPayToDate+"进入缴费期，请您尽快联系您的客户收取续期保费。";
                    System.out.println("现金发送短信：" + tAgentName + tContNo +"日期:"+ tPayToDate+"金额:"+ tPayMoney+"业务员手机号："+ tAgentMobile );
                    tAgentMsg.setReceiver(tAgentMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//                    tAgentMsg.setReceiver("18500905130");
                    tAgentMsg.setContents(tAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                    //和IT讨论后，归类到二级机构
                    //tAgentMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                    tAgentMsg.setOrgCode(tManageCom); //保单机构

                    //添加SmsMessageNew对象
                    tVectemp.add(tAgentMsg);
                    
                    SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的Messages元素
                    msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                    msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                    msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                    msgs.setTaskValue(tDepartMent);
                    msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                    msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                    msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                    msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                    msgs.setMessages((SmsMessageNew[]) tVectemp.toArray(new SmsMessageNew[tVectemp.size()]));
                    tVectemp.clear();
                    tVector.add(msgs);
                }
            	//银行转账方式缴费的
            	else 
            	{                  
                    //发送给业务员的短信 
            		SmsMessageNew tAgentMsg = new SmsMessageNew(); //创建SmsMessageNew对象，定义同上文下行短信格式中的Message元素
//                    tAgentContents = "尊敬的"+tAgentName+tAgentSex 
//                                          + "，您好！您的客户"+tCustomerName+"的保单号为"+tContNo+"的保单" 
//                                          + "将于"+tPayToDate+"到期，请您尽快联系您的客户将下期保费"+tPayMoney+"元存入缴费帐户。工作辛苦了，祝您健康。";
                    tAgentContents = "尊敬的"+tAgentName+tAgentSex 
				                   + "，您好！您的客户"+tCustomerName+"的保单(保单号"+tContNo+"，本期保费"+tPayMoney+"元)" 
				                   + "将于"+tPayToDate+"进入缴费期，请您尽快联系您的客户收取续期保费。";
                    System.out.println("发送短信：" + tAgentName + tContNo +"日期:"+ tPayToDate+"金额:"+ tPayMoney+"业务员手机号："+ tAgentMobile );
                    tAgentMsg.setReceiver(tAgentMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//                    tAgentMsg.setReceiver("18500905130");
                    tAgentMsg.setContents(tAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                    //和IT讨论后，归类到二级机构
                    //tAgentMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                    tAgentMsg.setOrgCode(tManageCom); //保单机构

                    //添加SmsMessageNew对象
                    tVectemp.add(tAgentMsg);
                    
                    SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的Messages元素
                    msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                    msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                    msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                    msgs.setTaskValue(tDepartMent);
                    msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                    msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                    msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                    msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                    msgs.setMessages((SmsMessageNew[]) tVectemp.toArray(new SmsMessageNew[tVectemp.size()]));
                    tVectemp.clear();
                    tVector.add(msgs);
            	}
//            	在LCCSpec表中插入值  ********************************************************  
                LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
                tLCCSpecSchema.setGrpContNo(BQ.GRPFILLDATA);
                tLCCSpecSchema.setContNo(tContNo);
                tLCCSpecSchema.setProposalContNo(BQ.GRPFILLDATA);
                tLCCSpecSchema.setPrtSeq(tGetNoticeNo);//催收的 放 ljspay--getnoticeno                              
                String tLimit = PubFun.getNoLimit(tManageCom);
    	        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
    	        tLCCSpecSchema.setSerialNo(serNo); //流水号
                tLCCSpecSchema.setEndorsementNo(tPayToDate);//lccont--paytodate 
                tLCCSpecSchema.setSpecType("xq02"); //xq01-续期缴费截止日前60天短信类型 xq02-续期缴费截止日前10天短信类型，xq03-续期缴费核销短信类型
                tLCCSpecSchema.setSpecCode("003");//001-投保人 002-被保人 003-业务员  
                tLCCSpecSchema.setSpecContent(tAgentContents+"业务员手机号："+ tAgentMobile);//短信内容
                tLCCSpecSchema.setOperator(mG.Operator);
                tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
                tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
                tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
                tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
                tLCCSpecSchema.getDB().insert();
            }
            
        }
        System.out.println("2------------"+tVector.size());
	} catch (Exception e) {
			System.out.println("CpayRnewMsgBL.java报错");
			e.printStackTrace();
	}

//  第三部分:续期续保收费成功的短信
//  包含两条短信
//  发给业务员的和发给客户的

        try{
               String ttSQLsucc = "select "
//			                	+ "(select mobile from lcaddress x ,lcappnt y where x.customerno = y.appntno and x.addressno = y.addressno and y.contno = c.contno fetch first 1 rows only),  "
            	   				+" mobile(c.contno), "
			                	+ "c.appntname,  "
			                	+ "(case c.appntsex when '1' then '女士' when '0' then '先生' else '先生/女士' end),  "
			                	+ "(select (case when phone is not null then phone else mobile end) from laagent where agentcode=c.agentcode),  "
			                	+ "(select name from laagent where agentcode=c.agentcode),"
			                	+ "(select (case sex when '1' then '女士' when '0' then '先生' else '先生/女士' end) from laagent where agentcode=c.agentcode),"
			                	+ "c.contno,a.managecom,a.confdate,a.sumactupaymoney ,a.getnoticeno, "
			                	+ "(select agentstate from laagent where agentcode=c.agentcode),c.agentcode,c.paytodate,c.salechnl, "
						       	+ "(select r.riskname from lmriskapp r, lcpol p where p.contno = c.contno and r.riskcode = p.riskcode and r.subriskflag = 'M' and r.risktype6 = '1' and r.riskcode not in ('340501','340601') order by r.riskperiod fetch first 1 rows only) "
			                	+ "from ljapay a,lccont c  "
			                	+ "where 1=1  "
			                	+ "and c.conttype='1' "
			                	+ "and a.incometype='2'    "
			                	+ "and a.getnoticeno like '31%' "
			                	+ "and a.incomeno=c.contno "
			                	+ "and exists (select 1 from ljspayb where getnoticeno = a.getnoticeno and dealstate = '1') "
			                	+ "and not exists (select 1 from LCCSpec d where d.contno=c.contno and d.SpecType='xq03' and d.EndorsementNo=c.paytodate ) "
			                	//测试后放开
			                	+ "and a.confdate between current date - 7 days and current date "
			                	+" and not exists (select 1 from ljapayperson where payno=a.payno and riskcode='320106') "
			                	+" and not exists (select 1 from ldcode where codetype='DSXBDX' and (code = c.agentcode or code = c.agentcom) and c.salechnl=codealias) "
			             // 完成测试去掉下一行
//			                	+ " and c.contno in ('014318190000001')" 
//			                	+ "fetch first 10 rows only "
			                	+ "with ur ";
               
               System.out.println(ttSQLsucc);

                SSRS ttMsgSuccSSRS =mExeSQL.execSQL(ttSQLsucc);

                for (int i = 1; i <= ttMsgSuccSSRS.getMaxRow(); i++) 
                {
                	String tCustomerMobile = ttMsgSuccSSRS.GetText(i, 1);
                	String tCustomerName = ttMsgSuccSSRS.GetText(i, 2);
                	String tCustomerSex = ttMsgSuccSSRS.GetText(i, 3);
                	String tAgentMobile = ttMsgSuccSSRS.GetText(i, 4);
                	String tAgentName = ttMsgSuccSSRS.GetText(i, 5);
                	String tAgentSex = ttMsgSuccSSRS.GetText(i, 6);
                	String tManageCom = ttMsgSuccSSRS.GetText(i, 8);
                	String tContNo = ttMsgSuccSSRS.GetText(i, 7);
                	String tConfDate = ttMsgSuccSSRS.GetText(i, 9);
                	String tPayMoney = ttMsgSuccSSRS.GetText(i, 10);
                	String tGetNoticeNo = ttMsgSuccSSRS.GetText(i,11);
                	String tAgentState=ttMsgSuccSSRS.GetText(i,12); //区分业务员是否是离职状态，这个功能暂时还未完成
                	String tAgentCode=ttMsgSuccSSRS.GetText(i,13);  //查离职状态的业务员的上级用
                	
                	String tPayToDate=ttMsgSuccSSRS.GetText(i,14);  //查离职状态的业务员的上级用
                	
                	//2017-5-11 direct by lichang 销售渠道，用于判定部门
                	String tSalechnl = ttMsgSuccSSRS.GetText(i,15);
                	String riskName = ttMsgSuccSSRS.GetText(i, 16);
                	
                    if (tManageCom == null || "".equals(tManageCom) ||
                        "null".equals(tManageCom)) {
                        continue;
                    }
                    
                    if (tContNo == null || "".equals(tContNo) ||
                            "null".equals(tContNo)) {
                            continue;
                        }

                    if (tConfDate == null || "".equals(tConfDate) ||
                            "null".equals(tConfDate)) {
                            continue;
                        }
                    
                    if (tPayMoney == null || "".equals(tPayMoney) ||
                            "null".equals(tPayMoney)) {
                            continue;
                        }
                    
                    if (tSalechnl == null || "".equals(tSalechnl) ||
                            "null".equals(tSalechnl)) {
                            continue;
                        }
                    
                    boolean customerFlag = true;
                    boolean agentFlag = true;

                    if (tCustomerMobile == null || "".equals(tCustomerMobile) ||
                        "null".equals(tCustomerMobile)) {
                    	customerFlag = false;
                    }

                    if (tCustomerName == null || "".equals(tCustomerName) ||
                            "null".equals(tCustomerName)) {
                    	customerFlag = false;
                    }
                    
                    if (tCustomerSex == null || "".equals(tCustomerSex) ||
                        "null".equals(tCustomerSex)) {
                    	customerFlag = false;
                    }
                    
                    String tDepartMent="";
                    tDepartMent = getDepartment(tSalechnl);
                    
                    if(customerFlag)
                    {
                        //发送给客户的短信            
                    	SmsMessageNew tCustomerMsg = new SmsMessageNew(); //创建SmsMessageNew对象，定义同上文下行短信格式中的Message元素
                        String tCustomerContents = "尊敬的"+tCustomerName+tCustomerSex 
                                                 + "，您好！您购买的"+riskName+"（保单号"+tContNo+"）已于"+tConfDate+"成功缴纳了下期保费，" 
                                                 + "缴费金额为"+tPayMoney+"元。感谢您的支持，祝您健康。客服电话：95591。";
                        System.out.println("发送短信：" + tCustomerName + tContNo +"日期:"+ tConfDate +"金额:"+ tPayMoney+"客户手机号："+ tCustomerMobile );
                        tCustomerMsg.setReceiver(tCustomerMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//                        tCustomerMsg.setReceiver("18500905130");
                        tCustomerMsg.setContents(tCustomerContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                        //和IT讨论后，归类到二级机构
                        //tCustomerMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                        tCustomerMsg.setOrgCode(tManageCom); //保单机构

                        //添加SmsMessageNew对象
                        tVectemp.add(tCustomerMsg);
                        
                        SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的Messages元素
                        msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                        msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                        msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                        msgs.setTaskValue(tDepartMent);
                        msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                        msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                        msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                        msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                        msgs.setMessages((SmsMessageNew[]) tVectemp.toArray(new SmsMessageNew[tVectemp.size()]));
                        tVectemp.clear();
                        tVector.add(msgs);
                        
//                        在LCCSpec表中插入值  ******************************************************** 
                        LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
                        tLCCSpecSchema.setGrpContNo(BQ.GRPFILLDATA);
                        tLCCSpecSchema.setContNo(tContNo);
                        tLCCSpecSchema.setProposalContNo(BQ.GRPFILLDATA);               
                        tLCCSpecSchema.setPrtSeq(tGetNoticeNo);//催收的 放 ljspay--getnoticeno 
                        String tLimit = PubFun.getNoLimit(tManageCom);
            	         String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
            	         tLCCSpecSchema.setSerialNo(serNo); //流水号                
                        tLCCSpecSchema.setEndorsementNo(tPayToDate);//ljapay--confdate 
                        tLCCSpecSchema.setSpecType("xq03"); //xq01-续期缴费截止日前60天短信类型 xq02-续期缴费截止日前10天短信类型，xq03-续期缴费核销短信类型
                        tLCCSpecSchema.setSpecCode("001");////001-投保人 002-被保人 003-业务员
                        tLCCSpecSchema.setSpecContent(tCustomerContents+"客户手机号："+ tCustomerMobile);
                        tLCCSpecSchema.setOperator(mG.Operator);
                        tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
                        tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
                        tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
                        tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
                        tLCCSpecSchema.getDB().insert();
                    }

                    if (tAgentState == null || "".equals(tAgentState) ||
                            "null".equals(tAgentCode)) {
                    	agentFlag = false;
                        }
                    if (tAgentCode == null || "".equals(tAgentCode) ||
                            "null".equals(tAgentCode)) {
                    	agentFlag = false;
                        }
                    
//                  业务员是否离职状态
                    if(Integer.parseInt(tAgentState)>=6) 
                    {	
//        				SSRS tMsgAgentSSRS=getAgentInfo( tAgentState, tAgentCode);
//        				if(tMsgAgentSSRS!=null)
//        				{
//        					tAgentMobile = tMsgAgentSSRS.GetText(1, 1);
//        		        	tAgentName = tMsgAgentSSRS.GetText(1, 2);
//        		        	tAgentSex = tMsgAgentSSRS.GetText(1, 3);
//        			    }
//        				else
//        				{
        					agentFlag = false;
//        				}
        			}
                    
                    if (tAgentMobile == null || "".equals(tAgentMobile) || "null".equals(tAgentMobile)) 
		        	{
                    	agentFlag = false;
	                }

	                if (tAgentName == null || "".equals(tAgentName) || "null".equals(tAgentName)) 
	                {
	                	agentFlag = false;
	                }
	                    
	                if (tAgentSex == null || "".equals(tAgentSex) || "null".equals(tAgentSex)) 
	                {
	                	agentFlag = false;
	                }
                        
                    if(agentFlag)
                    {

//                      发送给业务员的短信 
                    	SmsMessageNew tAgentMsg = new SmsMessageNew(); //创建SmsMessageNew对象，定义同上文下行短信格式中的Message元素
//                        String tAgentContents = "尊敬的"+tAgentName+tAgentSex 
//                                              + "，您好！您的客户"+tCustomerName+"的保单号为"+tContNo+"的保单" 
//                                              + "已于"+tConfDate+"成功缴纳了下期保费，缴费金额为"+tPayMoney+"元。工作辛苦了，祝您展业顺利。";
                        String tAgentContents   = "尊敬的"+tAgentName+tAgentSex 
						                        + "，您好！您的客户"+tCustomerName+"的保单（保单号为"+tContNo+"，本期保费"+tPayMoney+"元）" 
						                        + "已于"+tConfDate+"成功缴纳了本期保费。";
                        System.out.println("发送短信：" + tAgentName + tContNo +"日期:"+ tConfDate +"金额:"+ tPayMoney+"业务员手机号："+ tAgentMobile );
                        tAgentMsg.setReceiver(tAgentMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//                        tAgentMsg.setReceiver("18500905130");
                        
                        tAgentMsg.setContents(tAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                        //和IT讨论后，归类到二级机构
                        //tAgentMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                        tAgentMsg.setOrgCode(tManageCom); //保单机构

                        //添加SmsMessageNew对象
                        tVectemp.add(tAgentMsg);
                        
                        SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的Messages元素
                        msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                        msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                        msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                        msgs.setTaskValue(tDepartMent);
                        msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                        msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                        msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                        msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                        msgs.setMessages((SmsMessageNew[]) tVectemp.toArray(new SmsMessageNew[tVectemp.size()]));
                        tVectemp.clear();
                        tVector.add(msgs);
                        
//                      在LCCSpec表中插入值  ******************************************************** 
                        LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
                        tLCCSpecSchema.setGrpContNo(BQ.GRPFILLDATA);
                        tLCCSpecSchema.setContNo(tContNo);
                        tLCCSpecSchema.setProposalContNo(BQ.GRPFILLDATA);               
                        tLCCSpecSchema.setPrtSeq(tGetNoticeNo);//催收的 放 ljspay--getnoticeno 
                        String tLimit = PubFun.getNoLimit(tManageCom);
            	         String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
            	         tLCCSpecSchema.setSerialNo(serNo); //流水号                
                        tLCCSpecSchema.setEndorsementNo(tPayToDate);//ljapay--confdate 
                        tLCCSpecSchema.setSpecType("xq03"); //xq01-续期缴费截止日前60天短信类型 xq02-续期缴费截止日前10天短信类型，xq03-续期缴费核销短信类型
                        tLCCSpecSchema.setSpecCode("003");////001-投保人 002-被保人 003-业务员
                        tLCCSpecSchema.setSpecContent(tAgentContents+"业务员手机号："+ tAgentMobile);
                        tLCCSpecSchema.setOperator(mG.Operator);
                        tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
                        tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
                        tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
                        tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
                        tLCCSpecSchema.getDB().insert();
                    }
                    
                }
                System.out.println("3------------"+tVector.size());

    } catch (Exception e) {
			System.out.println("CpayRnewMsgBL.java报错");
			e.printStackTrace();
	}
//      第四部分:失效中止提醒通知短信
//      包含两条短信
//      发给业务员的和发给客户的
    		try{
               //改此部分即可
                String ttSQLShiXiao = "select " 
//    			                	+ "(select mobile from lcaddress x ,lcappnt y where x.customerno = y.appntno and x.addressno = y.addressno and y.contno = c.contno fetch first 1 rows only),  "//1
                					+" mobile(c.contno), "
    			                	+ "c.appntname,  "//2
    			                	+ "(case c.appntsex when '1' then '女士' when '0' then '先生' else '先生/女士' end),  "//3
    			                	+ "(select (case when phone is not null then phone else mobile end) from laagent where agentcode=c.agentcode),  "//4
    			                	+ "(select name from laagent where agentcode=c.agentcode),"//5
    			                	+ "(select (case sex when '1' then '女士' when '0' then '先生' else '先生/女士' end) from laagent where agentcode=c.agentcode)," //6
    			                	+ "c.contno," //7
    			                	+ "c.managecom," //8
    			                	+ "a.makedate," //9
    			                	+ "c.paytodate+ 2 years + 60 days, " //10
    			                	+ "(select agentstate from laagent where agentcode=c.agentcode)," //11
    			                	+ "c.agentcode, " //12
    			                	+ "a.polno, " //13
    			                	+ "c.salechnl "//14
    			                	+ "from lccontstate a,lccont c  "
    			                	+ "where 1=1  "
    			                	+ "and a.contno=c.contno "
//    			                	redmine#1517 分红和万能也需要发送失效短信
//    			                	+ "and not exists (select 1 from lcpol b where contno=c.contno and riskcode='730101') "//分红险不发短信
//    			                	+ "and not exists (select 1 from lcpol b where contno=c.contno  and exists (select 1 from lmriskapp where riskcode=b.riskcode and risktype4='4')) "//万能险不发送短信
    			                	+ "and exists (select 1 from lcpol b where contno=c.contno  and exists (select 1 from lmriskapp where riskcode=b.riskcode and riskperiod='L')) "//保单含有长期险才发短信
    			                	+ "and c.conttype='1' "
    			                	+ "and c.stateflag='2' "
    			                	+ "and a.statetype='Available' "
    			                	+ "and a.polno = '000000' "//只有保单失效才会发送短信
    			                	+ "and not exists (select 1 from LCCSpec d where d.contno=c.contno and d.SpecType='ShiX' and d.EndorsementNo=a.makedate ) "
    			                	+ "and a.makedate between current date - 7 days and current date "
    			                	+ "with ur "; 
                   System.out.println(ttSQLShiXiao);

                   SSRS ttShiXiaoMsgSuccSSRS =mExeSQL.execSQL(ttSQLShiXiao);

                   for (int i = 1; i <= ttShiXiaoMsgSuccSSRS.getMaxRow(); i++) 
                   {
                   	String tCustomerMobile = ttShiXiaoMsgSuccSSRS.GetText(i, 1);
                   	String tCustomerName = ttShiXiaoMsgSuccSSRS.GetText(i, 2);
                   	String tCustomerSex = ttShiXiaoMsgSuccSSRS.GetText(i, 3);
                   	String tAgentMobile = ttShiXiaoMsgSuccSSRS.GetText(i, 4);
                   	String tAgentName = ttShiXiaoMsgSuccSSRS.GetText(i, 5);
                   	String tAgentSex = ttShiXiaoMsgSuccSSRS.GetText(i, 6);
                   	String tContNo = ttShiXiaoMsgSuccSSRS.GetText(i, 7);
                	String tManageCom = ttShiXiaoMsgSuccSSRS.GetText(i, 8);
                   	String tShiXiaoDate = ttShiXiaoMsgSuccSSRS.GetText(i, 9);
                   	String tZhongZhiDate = ttShiXiaoMsgSuccSSRS.GetText(i, 10);
                   	String tPolNo = ttShiXiaoMsgSuccSSRS.GetText(i, 13);
                   	String tAgentState=ttShiXiaoMsgSuccSSRS.GetText(i,11); //区分业务员是否是离职状态，这个功能暂时还未完成
                   	String tAgentCode=ttShiXiaoMsgSuccSSRS.GetText(i,12);  //查离职状态的业务员的上级用
                   	
                   	//2017-5-11 direct by lichang 销售渠道，用于判定部门
                	String tSalechnl = ttShiXiaoMsgSuccSSRS.GetText(i,14);
                   	
                       if (tManageCom == null || "".equals(tManageCom) ||"null".equals(tManageCom)) 
                       {
                           continue;
                       }
                       
                       if (tContNo == null || "".equals(tContNo) || "null".equals(tContNo))
                       {
                           continue;
                       }

                       if (tShiXiaoDate == null || "".equals(tShiXiaoDate) ||"null".equals(tShiXiaoDate)) 
                       {
                           continue;
                       }
                       
                       if (tZhongZhiDate == null || "".equals(tZhongZhiDate) || "null".equals(tZhongZhiDate)) 
                       {
                           continue;
                       }
                       
                       if (tSalechnl == null || "".equals(tSalechnl) || "null".equals(tSalechnl)) 
                       {
                           continue;
                       }
                       
                       boolean customerFlag = true;
                       boolean agentFlag = true;

                       if (tCustomerMobile == null || "".equals(tCustomerMobile) ||"null".equals(tCustomerMobile)) 
                       {
                       	   customerFlag = false;
                       }

                       if (tCustomerName == null || "".equals(tCustomerName) ||"null".equals(tCustomerName)) 
                       {
                       	   customerFlag = false;
                       }
                       
                       if (tCustomerSex == null || "".equals(tCustomerSex) ||"null".equals(tCustomerSex)) 
                       {
                       	    customerFlag = false;
                       }
                       
                       String tDepartMent="";
                       tDepartMent = getDepartment(tSalechnl);
                       
                       if(customerFlag)
                       {
                           //发送给客户的短信            
                    	   SmsMessageNew tCustomerMsg = new SmsMessageNew(); //创建SmsMessageNew对象，定义同上文下行短信格式中的Message元素
                           String tCustomerContents = "尊敬的"+tCustomerName+tCustomerSex 
                                                    + "，您好！您的保单号为"+tContNo+"的保单已于"+tShiXiaoDate+"失效中止，" 
                                                    + "为保障您的权益，使您和您的家人继续拥有保险保障，您可于"+tZhongZhiDate+"前到公司办理合同复效手续。祝您健康。客服电话：95591。";
                           
                           System.out.println("发送短信：" + tCustomerName + tContNo +"日期:"+ tShiXiaoDate +"最终复效日期:"+ tZhongZhiDate+"客户手机号："+ tCustomerMobile );
                           tCustomerMsg.setReceiver(tCustomerMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//                           tCustomerMsg.setReceiver("18500905130"); //测试用                           
                           
                           tCustomerMsg.setContents(tCustomerContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                           //和IT讨论后，归类到二级机构
                           //tCustomerMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                           tCustomerMsg.setOrgCode(tManageCom); //保单机构

                           //添加SmsMessageNew对象
                           tVectemp.add(tCustomerMsg);
                           
                           SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的Messages元素
                           msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                           msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                           msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                           msgs.setTaskValue(tDepartMent);
                           msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                           msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                           msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                           msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                           msgs.setMessages((SmsMessageNew[]) tVectemp.toArray(new SmsMessageNew[tVectemp.size()]));
                           tVectemp.clear();
                           tVector.add(msgs);
                           
//                           在LCCSpec表中插入值  ******************************************************** 
                           LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
                           tLCCSpecSchema.setGrpContNo(BQ.GRPFILLDATA);
                           tLCCSpecSchema.setContNo(tContNo);
                           tLCCSpecSchema.setProposalContNo(BQ.GRPFILLDATA);               
                           tLCCSpecSchema.setPrtSeq(tPolNo);//失效的险种 放 lccontstate--polno 
                           String tLimit = PubFun.getNoLimit(tManageCom);
               	           String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
               	           tLCCSpecSchema.setSerialNo(serNo); //流水号                
                           tLCCSpecSchema.setEndorsementNo(tShiXiaoDate);//lccontstate--makedate 
                           tLCCSpecSchema.setSpecType("ShiX"); //xq01-续期缴费截止日前60天短信类型 xq02-续期缴费截止日前10天短信类型，xq03-续期缴费核销短信类型，ShiX-失效提醒通知短信类型
                           tLCCSpecSchema.setSpecCode("001");////001-投保人 002-被保人 003-业务员
                           tLCCSpecSchema.setSpecContent(tCustomerContents+"客户手机号："+ tCustomerMobile);
                           tLCCSpecSchema.setOperator(mG.Operator);
                           tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
                           tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
                           tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
                           tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
                           tLCCSpecSchema.getDB().insert();
                       }

                       if (tAgentState == null || "".equals(tAgentState) ||"null".equals(tAgentCode)) 
                       {
                       	    agentFlag = false;
                       }
                       if (tAgentCode == null || "".equals(tAgentCode) ||"null".equals(tAgentCode)) 
                       {
                       	    agentFlag = false;
                       }
//                     业务员是否离职状态
                       if(Integer.parseInt(tAgentState)>=6) 
                       {	
           				SSRS tMsgAgentSSRS=getAgentInfo( tAgentState, tAgentCode);
           				if(tMsgAgentSSRS!=null)
           				{
           					tAgentMobile = tMsgAgentSSRS.GetText(1, 1);
           		        	tAgentName = tMsgAgentSSRS.GetText(1, 2);
           		        	tAgentSex = tMsgAgentSSRS.GetText(1, 3);
           			    }
           				else
           				{
           					agentFlag = false;
           				}
           			}
                       
                    if (tAgentMobile == null || "".equals(tAgentMobile) || "null".equals(tAgentMobile)) 
   		        	{
                       	agentFlag = false;
   	                }

   	                if (tAgentName == null || "".equals(tAgentName) || "null".equals(tAgentName)) 
   	                {
   	                	agentFlag = false;
   	                }
   	                    
   	                if (tAgentSex == null || "".equals(tAgentSex) || "null".equals(tAgentSex)) 
   	                {
   	                	agentFlag = false;
   	                }
                           
                       if(agentFlag)
                       {

//                         发送给业务员的短信 
                    	   SmsMessageNew tAgentMsg = new SmsMessageNew(); //创建SmsMessageNew对象，定义同上文下行短信格式中的Message元素
                           String tAgentContents = "尊敬的"+tAgentName+tAgentSex 
                                                 + "，您好！您的客户"+tCustomerName+"的保单号为"+tContNo+"的保单" 
                                                 + "已于"+tShiXiaoDate+"失效中止，请您尽快联系您的客户办理复效手续。工作辛苦了，祝您健康。";
                           
                           System.out.println("发送短信：" + tAgentName + tContNo +"日期:"+ tShiXiaoDate +"业务员手机号："+ tAgentMobile );
                           tAgentMsg.setReceiver(tAgentMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
                           tAgentMsg.setContents(tAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                           //和IT讨论后，归类到二级机构
                           //tAgentMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                           tAgentMsg.setOrgCode(tManageCom); //保单机构
                           
                           //添加SmsMessage对象
                           tVectemp.add(tAgentMsg);
                           
                           SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的Messages元素
                           msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                           msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                           msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                           msgs.setTaskValue(tDepartMent);
                           msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                           msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                           msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                           msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                           msgs.setMessages((SmsMessageNew[]) tVectemp.toArray(new SmsMessageNew[tVectemp.size()]));
                           tVectemp.clear();
                           tVector.add(msgs);
                           
//                         在LCCSpec表中插入值  ******************************************************** 
                           LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
                           tLCCSpecSchema.setGrpContNo(BQ.GRPFILLDATA);
                           tLCCSpecSchema.setContNo(tContNo);
                           tLCCSpecSchema.setProposalContNo(BQ.GRPFILLDATA);               
                           tLCCSpecSchema.setPrtSeq(tPolNo);//催收的 放 ljspay--getnoticeno 
                           String tLimit = PubFun.getNoLimit(tManageCom);
               	           String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
               	           tLCCSpecSchema.setSerialNo(serNo); //流水号                
                           tLCCSpecSchema.setEndorsementNo(tShiXiaoDate);//ljapay--confdate 
                           tLCCSpecSchema.setSpecType("ShiX"); //xq01-续期缴费截止日前60天短信类型 xq02-续期缴费截止日前10天短信类型，xq03-续期缴费核销短信类型 ShiX-失效提醒通知短信类型
                           tLCCSpecSchema.setSpecCode("003");////001-投保人 002-被保人 003-业务员
                           tLCCSpecSchema.setSpecContent(tAgentContents+"业务员手机号："+ tAgentMobile);
                           tLCCSpecSchema.setOperator(mG.Operator);
                           tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
                           tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
                           tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
                           tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
                           tLCCSpecSchema.getDB().insert();
                       }
                       
                   }
                   System.out.println("4------失效中止提醒通知短信的记录数:"+tVector.size());
                   
               //到此处
    	} catch (Exception e) {
    			System.out.println("CpayRnewMsgBL.java报错");
    			e.printStackTrace();
    	}
        return tVector;
    }
    
    
    //区分业务员是否离职，离职的继续查他的上级领导
   private SSRS getAgentInfo(String tAgentState,String tAgentCode)
   {
	   SSRS tMsgAgentSSRS=null;
	   if(Integer.parseInt(tAgentState)>=6)
	   {		   
				String tSQLAgent="select "
					            +"(select mobile from laagent where agentcode=c.upagent),  "
					            +"(select name from laagent where agentcode=c.upagent),"
					            +"(select (case sex when '1' then '女士' when '0' then '先生' else '先生/女士' end) from laagent where agentcode=c.upagent) ,"
					            +"(select agentstate from laagent where agentcode=c.upagent),c.upagent "
								+" from latree c where agentcode='"+tAgentCode+"'";
				tMsgAgentSSRS =mExeSQL.execSQL(tSQLAgent);
				
				System.out.println("tMsgAgentSSRS=="+tMsgAgentSSRS.MaxRow);
				if(tMsgAgentSSRS!=null)
				{
					String tAgentState1=tMsgAgentSSRS.GetText(1,4); //上级状态
					String tAgentCode1 =tMsgAgentSSRS.GetText(1,5); //上级代码
					
					if(tAgentState1==null || tAgentState1.equals("") || "null".equals(tAgentState1))
					{
						 System.out.println("业务员状态为空");
						 return null;
					}
					if(tAgentCode1==null || tAgentCode1.equals("") || "null".equals(tAgentCode1))
					{
						 System.out.println("业务员代码为空");
						 return null;
					}
					if(tAgentCode1.equals(tAgentCode))
					{
						System.out.println("业务员上级为本人--错误数据");
						return null;
					}
					if(Integer.parseInt(tAgentState)>=6)
					{
						getAgentInfo(tAgentState1,tAgentCode1);
					}
				}
	   }
	  return tMsgAgentSSRS;  
   }

   private String getDepartment(String Salechnl){
	   String department = "";
	   if("01".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue10;
	   } else if("02".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue9;
	   } else if("03".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue9;
	   } else if("04".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue11;
	   } else if("06".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue10;
	   } else if("07".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue9;
	   } else if("10".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue10;
	   } else if("13".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue11;
	   } else if("14".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue12;
	   } else if("15".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue12;
	   } else if("16".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue8;
	   } else if("17".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue14;
	   } else if("18".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue8;
	   } else {
		   department = SMSClient.mes_taskVlaue15;
	   }	   
	   return department;
   }
   
    public static void main(String[] args) {
        GlobalInput mGlobalInput = new GlobalInput();
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);
        
        CpayRnewMsgBL tCpayRnewMsgBL = new CpayRnewMsgBL();
        tCpayRnewMsgBL.submitData(mVData, "XQMSG");
    }


}
