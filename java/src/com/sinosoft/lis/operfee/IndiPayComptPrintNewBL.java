package com.sinosoft.lis.operfee;

import java.util.ArrayList;

import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.f1print.PrintPDFManagerBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.f1print.PrintService;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class IndiPayComptPrintNewBL implements PrintService {

    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private XmlExport xmlexport = null;
    private VData mResult = null;
    private SSRS mSSRS;
    private String mContNo = ""; //保单号
    private String mComCode = ""; //管理机构
    private String mManageName = ""; //管理机构名称
    private String mPayDate = ""; //交费日期
    private String mDueMoney = ""; //应交金额
    private String mDif = ""; //应交金额
    private String mCanRiskName = ""; //取消的险种
    private boolean  printFlag = false; //打印标志
    private LJAPaySchema tLJAPaySchema; //单号，个案核销
    private String mVerifyType = ""; //1：批量核销；2：个案核销
    TextTag textTag = new TextTag();
    private LCContSchema tLCContSchema = new LCContSchema();
    private LCAddressSchema tLCAddressSchema = new LCAddressSchema();
    private TransferData mTransferData = new TransferData();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

    private String mOperate = "";
    private int needPrt = -1;


    public IndiPayComptPrintNewBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        this.mOperate = cOperate;
        //取得外部传入数据
        if (!getInputData(cInputData)) {
            return false;
        }

        //获取打印所需数据
        if (!getPrintData()) {
            return false;
        }
        if (!dealPrintMag()) {
            return false;
        }

        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

        try {
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            if (this.mOperate.equals("INSERT")) {
                tLJAPaySchema = (LJAPaySchema) cInputData.getObjectByObjectName(
                        "LJAPaySchema", 0);
                mTransferData = (TransferData) cInputData.getObjectByObjectName(
                        "TransferData", 0);
                if (mGlobalInput == null || tLJAPaySchema == null ||
                    mTransferData == null) {
                    // @@错误处理
                    CError tError = new CError();
                    tError.moduleName = "GrpDueFeeBL";
                    tError.functionName = "getInputData";
                    tError.errorMessage = "没有得到足够的数据，请您确认!";
                    this.mErrors.addOneError(tError);
                    return false;
                }

                mPayDate = (String) mTransferData.getValueByName("PayDate");
                if (mPayDate == null || mPayDate.equals("")) {
                    mPayDate = tLJAPaySchema.getPayDate();
                }
                mDueMoney = (String) mTransferData.getValueByName("DueMoney");
                if (mDueMoney == null || mDueMoney.equals("")) {
                    mDueMoney = tLJAPaySchema.getSumActuPayMoney() + "";
                }

                mDif = (String) mTransferData.getValueByName("Dif");
            } else {
                mLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.
                                      getObjectByObjectName(
                                              "LOPRTManagerSchema", 0);
                if (mLOPRTManagerSchema == null) {
                    System.out.println("~~~~~~~~~~~~~~~~~~~~~~" +
                                       tLJAPaySchema.getPayNo());
                }
                LJAPayDB tLJAPayDB = new LJAPayDB();
                tLJAPayDB.setPayNo(mLOPRTManagerSchema.getStandbyFlag2());
                if (!tLJAPayDB.getInfo()) {
                    mErrors.addOneError("传入的数据不完整。");
                    return false;
                }
                tLJAPaySchema = tLJAPayDB.getSchema();
                mPayDate = mLOPRTManagerSchema.getStandbyFlag1();
                mDueMoney = mLOPRTManagerSchema.getStandbyFlag3();
                mDif = mLOPRTManagerSchema.getStandbyFlag4();
            }
        } catch (Exception e) {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("传入的数据不完整，" + e.toString());
            return false;
        }

        return true;
    }

    /**
     * 获取打印所需数据
     * @param schema LCContSchema
     * @return String
     */
    private boolean getPrintData() {
        TextTag tag = new TextTag();
        xmlexport = new XmlExport(); //新建一个XmlExport的实例

        textTag.add("JetFormType", mLOPRTManagerSchema.getCode());
        //借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='" +
                            mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            CError.buildErr("getInputData", "操作员机构查询出错！");
            return false;
        }
        String printcom =
                "select codename from ldcode where codetype='pdfprintcom' and code='" +
                comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);

        textTag.add("ManageComLength4", printcode);
        textTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        if (("batch").equals(mOperate)) {
            textTag.add("previewflag", "0");
        } else {
            textTag.add("previewflag", "1");
        }


        LJAPayDB tLJAPayDB = new LJAPayDB();
        tLJAPayDB.setPayNo(tLJAPaySchema.getPayNo());
        if (!tLJAPayDB.getInfo()) {
            CError.buildErr(this, "实收表缺少数据");
            return false;
        }
        String ContNo = tLJAPayDB.getIncomeNo();
        mContNo = ContNo;
        mComCode = tLJAPayDB.getManageCom();
        System.out.println("ContNo=" + ContNo);

        String sqlhos = "select count(1) from ldcode where codetype='printriskcode' and othersign='1' and code in (select riskcode from LCPol where ContNo='"
            + ContNo + "')";
        ExeSQL tExeSQL = new ExeSQL();
        String tCount = tExeSQL.getOneValue(sqlhos);
//如果查询的结果是0，表示该合同下没有险种需要打印医院信息
        if (tCount.equals("0")){
        	textTag.add("HospitalFlag", "0");
        }
        else {
        	sqlhos = " select b.HospitName from LDHospital b,LCCont a "+
               " where b.associateclass in ('1','2') and b.managecom = substr(a.managecom,1,4) "+ " and a.ContNo='" + ContNo + "'";
            SSRS tSSRS = tExeSQL.execSQL(sqlhos);
        if (tSSRS.getMaxRow() < 1) {
            CError tError = new CError();
            tError.moduleName = "IndiPayComptPrintBL";
            tError.functionName = "getSpecHospital";
            tError.errorMessage = "查询推荐医院出错";
            this.mErrors.addOneError(tError);
            return false;
        }
            textTag.add("HospitalFlag", "1");
        }

        textTag.add("ConfDate", tLJAPayDB.getConfDate());
        String sql = "";
        String sqlone = "";

        //设置公司地址等信息
        if (!setFixedInfo()) {
            CError.buildErr(this,
                            "机构信息表缺少数据");
            return false;

        }

        //获取团体保单信息
        sqlone = "select * from lccont where ContNo='" + mContNo + "'";
        System.out.println("sqlone=" + sqlone);
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = tLCContDB.executeQuery(sqlone);
        if (tLCContSet.size() == 0) {
            CError.buildErr(this, "保单表缺少数据");
            return false;
        }

        tLCContSchema = tLCContSet.get(1).getSchema();
        textTag.add("ContNo", tLCContSchema.getContNo());

        //业务员信息
        String agntPhone = "";
        String temPhone = "";
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(tLCContSchema.getAgentCode());
        tLaAgentDB.getInfo();
        textTag.add("AgentName", tLaAgentDB.getName());
        textTag.add("AgentCode", tLaAgentDB.getAgentCode());
        textTag.add("XI_ManageCom", mComCode);
        temPhone = tLaAgentDB.getMobile();
        if (temPhone == null || temPhone.equals("") || temPhone.equals("null")) {
            agntPhone = tLaAgentDB.getPhone();
        } else {
            agntPhone = temPhone;
        }
        textTag.add("Phone", agntPhone);

        //机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
//        tLABranchGroupDB.setAgentGroup(tLCContSchema.getAgentGroup());
//        tLABranchGroupDB.getInfo();
        String branchSQL = " select * from LABranchGroup where agentgroup = "
                           +
                           " (select subStr(branchseries,1,12) from LABranchGroup where agentgroup = "
                           +
                           " (select agentgroup from laagent where agentcode ='"
                           + tLaAgentDB.getAgentCode() + "'))"
                           ;
        LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(
                branchSQL);
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
            CError.buildErr(this, "查询业务员机构失败");
            return false;
        }
        textTag.add("AgentGroup", tLABranchGroupSet.get(1).getName());

        String showPaymode = "";
        String showPayintv = "";
        switch (Integer.parseInt(tLCContSchema.getPayMode())) {
        case 1:
            showPaymode = "现金";

//            showPaymode = "自行交费";
            xmlexport.createDocument("IndiPayComPrint.vts", "printer"); //最好紧接着就初始化xml文档
            break;

        case 2:
            showPaymode = "现金支票";

//            showPaymode = "自行交费";
            xmlexport.createDocument("IndiPayComPrint.vts", "printer"); //最好紧接着就初始化xml文档
            break;
        case 3:
            showPaymode = "转帐支票";

//            showPaymode = "银行转帐";
//            showPaymode = "1";
            xmlexport.createDocument("IndiPayComBankPrint.vts", "printer"); //最好紧接着就初始化xml文档
            break;

        case 4:
            showPaymode = "银行转账";

//            showPaymode = "银行转帐";
//            showPaymode = "1";
            xmlexport.createDocument("IndiPayComBankPrint.vts", "printer"); //最好紧接着就初始化xml文档
            break;
//add by xp 添加case11的情况并添加case5-11的createDocument
        case 5:
            showPaymode = "内部转帐";
            xmlexport.createDocument("IndiPayComPrint.vts", "printer"); //最好紧接着就初始化xml文档
            break;
        case 6:
            showPaymode = "银行托收";
            xmlexport.createDocument("IndiPayComPrint.vts", "printer"); //最好紧接着就初始化xml文档
            break;
        case 7:
            showPaymode = "业务员信用卡";
            xmlexport.createDocument("IndiPayComPrint.vts", "printer"); //最好紧接着就初始化xml文档
            break;
        case 9:
            showPaymode = "其他";
            xmlexport.createDocument("IndiPayComPrint.vts", "printer"); //最好紧接着就初始化xml文档
            break;
        case 11:
            showPaymode = "银行汇款";
            xmlexport.createDocument("IndiPayComPrint.vts", "printer"); //最好紧接着就初始化xml文档
            break;
        case 12:
            showPaymode = "其他银行代收代付";
            xmlexport.createDocument("IndiPayComPrint.vts", "printer"); //最好紧接着就初始化xml文档
            break;
        default:
        	showPaymode = "其他银行代收代付";
        	xmlexport.createDocument("IndiPayComPrint.vts", "printer"); //最好紧接着就初始化xml文档
        	break;

        }
        ;

        //交费频次
        showPayintv = Integer.toString(tLCContSchema.getPayIntv());
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("payintv");
        tLDCodeDB.setCode(showPayintv);

        if (!(tLDCodeDB.getInfo())) {
            CError.buildErr(this, "集体保单表缺少数据");
            return false;
        }

        //交费频次，交费方式设置
        textTag.add("PayMode", showPaymode);

        //取得银行名称
        if ("银行转账".equals(showPaymode) || "转帐支票".equals(showPaymode)) {
            LDBankDB tLDBankDB = new LDBankDB();
            tLDBankDB.setBankCode(tLCContSchema.getBankCode());
            if (!tLDBankDB.getInfo()) {
                textTag.add("BankName", "-");
            } else {
                textTag.add("BankName", tLDBankDB.getBankName());
            }
        } else {
            textTag.add("BankName", "-");
        }

        //获取投保人地址
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(ContNo);

        if (!tLCAppntDB.getInfo()) {
            CError.buildErr(this,
                            "团单投保人表中缺少数据");
            return false;
        }
//      获取投保人性别
        String sexCode = tLCAppntDB.getAppntSex();
        String sex;
        if (sexCode == null) {
            sex = "先生/女士";
        } else if (sexCode.equals("0")) {
            sex = "先生";
        } else if (sexCode.equals("1")) {
            sex = "女士";
        } else {
            sex = "先生/女士";
        }
        textTag.add("LinkManSex", sex);

        sql = "select * from LCAddress where CustomerNo='" +
              tLCContSchema.getAppntNo() + "' AND AddressNo = '" +
              tLCAppntDB.getAddressNo() +
              "'";
        System.out.println("sql=" + sql);
        LCAddressDB tLCAddressDB = new LCAddressDB();
        LCAddressSet tLCAddressSet = tLCAddressDB.executeQuery(sql);
        if (tLCAddressSet.size() == 0) {
            CError.buildErr(this,
                            "客户地址表中缺少数据");
            return false;
        }

        tLCAddressSchema = tLCAddressSet.get(tLCAddressSet.size()).
                           getSchema();
        //qulq 070403 modify
        String GrpZipCode = tLCAddressSchema.getZipCode();
        String GrpAddress = tLCAddressSchema.getPostalAddress();
        if (GrpZipCode == null || GrpZipCode.equals("")) {
            GrpZipCode = tLCAddressSchema.getHomeZipCode();
        }
        if (GrpAddress == null || GrpAddress.equals("")) {
            GrpAddress = tLCAddressSchema.getHomeAddress();
        }
        textTag.add("GrpZipCode", GrpZipCode);
        textTag.add("GrpAddress", GrpAddress);
//        textTag.add("GrpZipCode", tLCAddressSchema.getHomeZipCode());
//        textTag.add("GrpAddress", tLCAddressSchema.getHomeAddress());
        String appntPhoneStr = "";
        if (tLCAddressSchema.getPhone() != null) {
            appntPhoneStr += tLCAddressSchema.getPhone();
        } else if (tLCAddressSchema.getHomePhone() != null) {
            appntPhoneStr = tLCAddressSchema.getHomePhone();
        } else if (tLCAddressSchema.getCompanyPhone() != null) {
            appntPhoneStr = tLCAddressSchema.getCompanyPhone();
        } else if (tLCAddressSchema.getMobile() != null) {
            appntPhoneStr = tLCAddressSchema.getMobile();
        }
        textTag.add("AppntPhone", appntPhoneStr);
        textTag.add("LinkMan1", tLCContSchema.getAppntName());
        textTag.add("AppntNo", tLCContSchema.getAppntNo());
        textTag.add("AppntName", tLCContSchema.getAppntName());

        String sumActuPayMoney
                = CommonBL.bigDoubleToCommonString(tLJAPayDB.getSumActuPayMoney(),
                "0.00");
        if (sumActuPayMoney == null || sumActuPayMoney.equals("")) {
            mErrors.addOneError("没有取到实交金额");
            return false;
        }
        textTag.add("ActuMoney", sumActuPayMoney);

        textTag.add("BankAcc", tLJAPayDB.getBankAccNo());
        textTag.add("PayDate", mPayDate);

        if (mDif == null || mDif.equals("") || mDif.equals("null")) {
            mDif = "0.00";
        }
        mDif = CommonBL.bigDoubleToCommonString(Double.parseDouble(mDif),
                                                "0.00");
        textTag.add("Dif", mDif);

        mDueMoney = CommonBL.bigDoubleToCommonString(Double.parseDouble(
                mDueMoney),
                "0.00");
        if (mDueMoney == null || mDueMoney.equals("")) {
            mErrors.addOneError("没有取到应交金额");
            return false;
        }
        textTag.add("DueMoney", mDueMoney);
        //得到公司名
        textTag.add("ManageName", mManageName);

        textTag.add("BarCode1", tLJAPayDB.getPayNo());
        textTag.add("BarCodeParam1",
                    "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");
        System.out.println("ActuGetNo" + tLJAPayDB.getPayNo());

        if (textTag.size() > 0) {
            xmlexport.addTextTag(textTag);
        }

        String[] title = {"险种序号", "险种名称", "被保险人", "保险期间", "保险金额", "交费频次",
                         "期交保费", "保费性质"};
        //校验数据合法性
        if (!getListData()) {
            return false;
        }
        xmlexport.addListTable(getListTable(), title);

        ListTable tEndTable = new ListTable();
        tEndTable.setName("END");
        String[] tEndTitle = new String[0];
        xmlexport.addListTable(tEndTable, tEndTitle);

        //添加指定医院和推荐医院 详见需求 为《个险续保续期对账单》增加附件－《指定医院、推荐医院清单》。
        //指定医院
        if (!getHospital(xmlexport, ContNo)) {
            return false;
        }

        //添加推荐医院
        if (!getSpecHospital(xmlexport, ContNo)) {
            return false;
        }
        
        if(printFlag)
		  {
        	
       	 String xianZhongMing = "select   (select riskname from lmriskapp where riskcode = a.riskcode) " 
             + "  from LDHospital b, LCpol a where associateclass in ('2','1') " 
             + " and b.managecom = substr(a.managecom, 1, 4) and a.ContNo = '" + mContNo + "'"
             + " and exists (select 1 from lddinghospital c where b.Hospitcode = c.Hospitcode and subStr(a.managecom, 1, 4) = c.managecom "	
             + " and exists (select 1 from lccont where contno=a.contno and InputDate between c.validate and c.cinValidate) and riskcode =a.riskcode)"
             ;
             ExeSQL tExeSQLxianZhongMing = new ExeSQL();
             SSRS tSSRSxianZhongMing = tExeSQLxianZhongMing.execSQL(xianZhongMing);
        	if(tSSRSxianZhongMing!=null)
        	{
        		  ArrayList tArrayList=new ArrayList();
	          	  for(int i=1;i<=tSSRSxianZhongMing.MaxRow;i++)
	          	  {
	          		  boolean daYin=true;
	          		  
	          		  for(int j=0;j<tArrayList.size();j++)
	          		  {
	          			  if(tArrayList.get(j).equals(tSSRSxianZhongMing.GetText(i,1)))
	          			  {
	          				  daYin=false;
	          				  continue;
	          			  }
	          		  }
	          		  if(daYin)
	          		  {
	          		     tArrayList.add(tSSRSxianZhongMing.GetText(i,1)) ;
	          		     mCanRiskName+="《"+tSSRSxianZhongMing.GetText(i,1)+"》"; 
	          		  }
	          	  }
        	}
        	  
			  TextTag tTextTag=new TextTag();
		      tTextTag.add("BF", SysConst.TEG_START);
		      tTextTag.add("BF_Hospital", "带※字符的医院不是"+mCanRiskName+"的指定及推荐医院。");
		      tTextTag.add("BF", SysConst.TEG_END);
		      xmlexport.addTextTag1(tTextTag);  
		  }
		  else
		  {
				//		      其他分公司打印时保留此标签，标签内容为空
		        TextTag tTextTag=new TextTag();
		        tTextTag.add("BF", SysConst.TEG_START);
		        tTextTag.add("BF_Hospital", " ");
		        tTextTag.add("BF", SysConst.TEG_END);
		        xmlexport.addTextTag1(tTextTag); 
		  }
        
        ListTable ttEndTable = new ListTable();
        ttEndTable.setName("End2");
        String[] ttEndTitle = new String[0];
        xmlexport.addListTable(ttEndTable, ttEndTitle);
        
        
        xmlexport.outputDocumentToFile("C:\\", "indipaycomepthuxl");
        mResult = new VData();
        mResult.addElement(xmlexport);

        //放入打印列表
//        LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
//        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
//        tLOPRTManagerDB.setStandbyFlag2(tLJAPayDB.getPayNo());
//        tLOPRTManagerDB.setCode("92");
//        tLOPRTManagerSet = tLOPRTManagerDB.query();
//        needPrt = tLOPRTManagerDB.query().size();
//        if (needPrt == 0) { //没有数据,进行封装
        String tLimit = PubFun.getNoLimit(tLCContSchema.getManageCom());
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNo(tLCContSchema.getContNo());
        mLOPRTManagerSchema.setOtherNoType("00");
        mLOPRTManagerSchema.setCode(PrintPDFManagerBL.CODE_INDIPAY);
        mLOPRTManagerSchema.setManageCom(tLCContSchema.getManageCom());
        mLOPRTManagerSchema.setAgentCode(tLCContSchema.getAgentCode());
        mLOPRTManagerSchema.setReqCom(tLCContSchema.getManageCom());
        mLOPRTManagerSchema.setReqOperator(tLCContSchema.getOperator());
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        mLOPRTManagerSchema.setStandbyFlag2(tLJAPayDB.getPayNo()); //这里存放 （也为交费收据号）
        mLOPRTManagerSchema.setStandbyFlag1(mPayDate); //这里存放mPayDate
        mLOPRTManagerSchema.setStandbyFlag3(mDueMoney); //这里存放mDueMoney
        mLOPRTManagerSchema.setStandbyFlag4(mDif); //这里存放mDif
//        }
        return true;
    }

    /**
     * 查询列表显示数据
     * @param schema LCContSchema
     * @return String
     */

    private boolean getListData() {
        StringBuffer sqlSB = new StringBuffer();

        //qulq 070113 modify  两句SQL都可以用。但是都有问题啊，当初就不该这样设计啊。应该搞成静态的。
        sqlSB.append(
                "select distinct a.RiskSeqNo,(select riskname  from lmrisk where riskcode=a.riskcode),a.InsuredName")
                .append(" ,(select char(max(lastpaytodate))||'～'||char(max(curpaytodate) - 1 day) from ljapayperson where polno=a.polno and PayNo = c.PayNo)")
                .append(",case when b.FactorValue='1' then trim(to_char(a.Amnt)) || '.00'  when b.FactorValue='3' then to_char(a.Mult) end")
                .append(
                        " ,(select codename from ldcode where codetype='payintv' and code=char(a.PayIntv))")
                .append(" ,a.prem,(select case payTypeFlag when '1' then '续保保费' else '续期保费' end from LJaPayPerson where polNo = a.polNo and PayNo =c.PayNo fetch first 1 rows only) ")
                .append("  from lcpol a,LDRiskParamPrint  b ,LJAPayPerson c ")
                .append(" where  a.ContNo='")
                .append(mContNo)
                .append("' and a.RiskCode=b.RiskCode and c.paytype ='ZC'")
                .append(" and a.PolNo = c.PolNo and c.PayNo ='" +
                        tLJAPaySchema.getPayNo() + "'")
                .append(" order by a.RiskSeqNo ")
                // " group by a.LastPayToDate,a.CurPayToDate,b.PayDate,a.ContNo")
                ;

        /*
                sqlSB.append(
                        "select a.RiskSeqNo,(select riskname  from lmrisk where riskcode=a.riskcode),a.InsuredName")
                        .append(
                                " ,(select char(max(lastpaytodate))||'～'||char(max(curpaytodate)) from ljspaypersonb where polno=a.polno)")
                        .append(",case when b.FactorValue='1' then trim(to_char(a.Amnt)) || '.00'  when b.FactorValue='3' then to_char(a.Mult) end")
                        .append(
                                " ,(select codename from ldcode where codetype='payintv' and code=char(a.PayIntv))")
                        .append(" ,a.prem,(select case payTypeFlag when '1' then '续保保费' else '续期保费' end from LJSPayPersonB where polNo = a.polNo fetch first 1 rows only) ")
                        .append("  from lcpol a,LDRiskParamPrint  b  ")
                        .append(" where  a.ContNo='")
                        .append(mContNo)
                        .append("' and a.RiskCode=b.RiskCode")
                        //.append(
                        // " group by a.LastPayToDate,a.CurPayToDate,b.PayDate,a.ContNo")
                        ;
         */
        String tSQL = sqlSB.toString();
        System.out.println(tSQL);
        ExeSQL tExeSQL = new ExeSQL();

        mSSRS = tExeSQL.execSQL(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "creatFile";
            tError.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     *  获取列表数据
     * @param schema LCContSchema
     * @return String
     */
    private ListTable getListTable() {
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[8];
            for (int j = 1; j <= mSSRS.getMaxCol(); j++) {

                info[j - 1] = mSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }

        tListTable.setName("ZT");
        return tListTable;
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }

    public static void main(String[] args) {
        IndiPayComptPrintNewBL p = new IndiPayComptPrintNewBL();
        String tPayNo = "32001213744";
        GlobalInput mGI = new GlobalInput();
        mGI.Operator = "test";
        mGI.ComCode = "86";
        mGI.ManageCom = "86";

        LJAPaySchema tLJAPaySchema = new LJAPaySchema();
        tLJAPaySchema.setPayNo(tPayNo);
        VData v = new VData();
        v.addElement(mGI);
        v.addElement(tLJAPaySchema);
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("PayDate", "2007-1-23");
        tTransferData.setNameAndValue("DueMoney", "2000");
        tTransferData.setNameAndValue("Dif", "30");
        v.add(tTransferData);
        if (!p.submitData(v, "INSERT")) {
            System.out.println(p.mErrors.getErrContent());
        } else {
            System.out.println("OK");
        }
    }

    private void jbInit() throws Exception {
    }

    //查询公司名称
    private String getCompanyName() {
        //通过LDCode得到保单号，然后得到公司名称
        String manageCode = mGlobalInput.ManageCom;
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("station");
        tLDCodeDB.setCode(manageCode);
        LDCodeSet set = tLDCodeDB.query();
        if (set == null || set.size() == 0) {

            mErrors.addOneError("没有查到公司名");
            System.out.println("没有查到公司名" + tLDCodeDB.mErrors.getErrContent());
            return null;
        } else {
            return set.get(1).getCodeName();
        }
    }

    /**
     * 设置i通知书的公司信息等
     */
    private boolean setFixedInfo() {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(mComCode);
        if (!tLDComDB.getInfo()) {
            CError.buildErr(this,
                            "管理结构表中缺少数据");
            return false;
        }
        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("letterservicename", tLDComDB.getLetterServiceName());
        mManageName = tLDComDB.getLetterServiceName();
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getServicePostZipcode());
        textTag.add("MakeDate", PubFun.getCurrentDate());
        textTag.add("Fax", tLDComDB.getFax());
        //textTag.add("Operator", mGlobalInput.Operator);
        return true;
    }

    /**
     * 加入到打印列表
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealPrintMag() {
//        if (needPrt == 0) {
//            MMap tMap = new MMap();
//            tMap.put(mLOPRTManagerSchema, "INSERT");
//            VData tInputData = new VData();
//            tInputData.add(tMap);
//            PubSubmit tPubSubmit = new PubSubmit();
//            if (tPubSubmit.submitData(tInputData, "") == false) {
//                this.mErrors.addOneError("PubSubmit:处理LOPRTManager 表失败!");
//                return false;
//            }
//        }
        mResult.addElement(mLOPRTManagerSchema);
        return true;
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public CErrors getErrors() {
        return mErrors;
    }

    /**
     * 返回BOOL值，真为成功，
     */
    private boolean getHospital(XmlExport aXmlexport, String aContNo) {
        //查询ldcode表，获取是否需要打印医院信息
        if (aContNo == null || aContNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "IndiPayComptPrintBL";
            tError.functionName = "getHospital";
            tError.errorMessage = "保单号不能为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        String sql = "select count(1) from ldcode where codetype='printriskcode' and codealias='1' and code in (select riskcode from LCPol where ContNo='"
                     + aContNo + "')"
                     ;
        ExeSQL tExeSQL = new ExeSQL();
        String tCount = tExeSQL.getOneValue(sql);
        //如果查询的结果是0，表示该合同下没有险种需要打印医院信息
        if (tCount.equals("0")) {
            return true;
        }
        else 
        {
            sql = "select b.HospitName,b.Address from LDHospital b,LCCont a " +
                  " where associateclass in ('2') and b.managecom = substr(a.managecom,1,4) " +
                  " and a.ContNo='" + aContNo + "'";
            SSRS tSSRS = tExeSQL.execSQL(sql);
            if (tSSRS.getMaxRow() < 1) {
            	 ListTable tListTable = new ListTable();
                 for (int i = 1; i <= 1; i++) {
                     String[] info = new String[2];
                     for (int j = 1; j <= 1; j++) {
                         info[j - 1] = "无";
                     }
                     tListTable.add(info);
                 }
                 String[] title = {"指定医院名称", "联系地址"};
                 tListTable.setName("Hospital");
                 aXmlexport.addListTable(tListTable, title);
                 return true ;
            }
            ListTable tListTable = new ListTable();
            
            //对于北京分公司续保的保单，如果原保单的录入日期（InputDate）在2011-5-1（含）之后的，并且保单包含《守护专家住院定额个人医疗保险》的，在保单定点医院打印时，在定点医院清单中对以上15个医院加括号说明：本医院对部分险种不是定点医院，具体险种见下方注释。
            String sqlBeiJing = "select b.HospitName,b.Address from LDHospital b,LCCont a " 
						      + " where associateclass in ('2') and b.managecom = substr(a.managecom,1,4) " 
						      + " and a.ContNo='" + aContNo + "'"
						      + " and exists (select 1 from lddinghospital c where b.Hospitcode=c.Hospitcode and   subStr(a.managecom,1,4)=c.managecom  and a.InputDate between c.validate and c.cinValidate and riskcode in (select riskcode from lcpol where contno =a.contno))"
						      ;
			
			ExeSQL tExeSQLBeiJing = new ExeSQL();
			SSRS tSSRSBeiJing = tExeSQLBeiJing.execSQL(sqlBeiJing);
			if(tSSRSBeiJing.getMaxRow() >0)
			{
				printFlag=true;
	        	//查出需要加括号的定点医院
	        	 String sqlBeiFen1 = "select b.HospitName,b.Address from LDHospital b,LCCont a " 
	        		               + " where associateclass in ('2') and b.managecom = substr(a.managecom,1,4) " 
	        		               + " and a.ContNo='" + aContNo + "'"
					               +" and exists (select 1 from lddinghospital c where b.Hospitcode=c.Hospitcode and   subStr(a.managecom,1,4)=c.managecom  and a.InputDate between c.validate and c.cinValidate and riskcode in (select riskcode from lcpol where contno =a.contno))"	
					               ;
				ExeSQL tExeSQLBeiFen1 = new ExeSQL();
				SSRS tSSRSBeiFen1 = tExeSQLBeiFen1.execSQL(sqlBeiFen1);
				
				 for (int i = 1; i <= tSSRSBeiFen1.getMaxRow(); i++) {
			            String[] info = new String[2];
			            for (int j = 1; j <= tSSRSBeiFen1.getMaxCol(); j++) {
			            	if(j==1)
			            	{
			            		info[j - 1] = tSSRSBeiFen1.GetText(i, j)+"      ※ ";
			            	}
			            	else
			            	{
			                 info[j - 1] = tSSRSBeiFen1.GetText(i, j);
			            	}
			            }
			            tListTable.add(info);
			        }
//				查出不需要加括号的定点医院
				 String sqlBeiFen2 = "select b.HospitName,b.Address from LDHospital b,LCCont a " 
					               + " where associateclass in ('2') and b.managecom = substr(a.managecom,1,4) " 
					               + " and a.ContNo='" + aContNo + "'"
					               + " and not exists (select 1 from lddinghospital c where b.Hospitcode=c.Hospitcode and   subStr(a.managecom,1,4)=c.managecom  and a.InputDate between c.validate and c.cinValidate and riskcode in (select riskcode from lcpol where contno =a.contno))"
					               ;
				 
				 
				 
				 ExeSQL tExeSQLBeiFen2 = new ExeSQL();
				SSRS tSSRSBeiFen2 = tExeSQLBeiFen2.execSQL(sqlBeiFen2);
				
				 for (int i = 1; i <= tSSRSBeiFen2.getMaxRow(); i++) {
			            String[] info = new String[2];
			            for (int j = 1; j <= tSSRSBeiFen2.getMaxCol(); j++) {
			                 info[j - 1] = tSSRSBeiFen2.GetText(i, j);
			            }
			            tListTable.add(info);
			        }

		        
		        String[] title = {"指定医院名称", "联系地址"};
		        tListTable.setName("Hospital");
		        aXmlexport.addListTable(tListTable, title);   
			}
			else
			{            
	            for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
	                String[] info = new String[2];
	                for (int j = 1; j <= tSSRS.getMaxCol(); j++) {
	                    info[j - 1] = tSSRS.GetText(i, j);
	                }
	                tListTable.add(info);
	            }
	            String[] title = {"指定医院名称", "联系地址"};
	            tListTable.setName("Hospital");
	            aXmlexport.addListTable(tListTable, title);
	            
	        }
        
        }

        return true;
    }

    /**
     * 返回BOOL值，真为成功，
     */
    private boolean getSpecHospital(XmlExport aXmlexport, String aContNo) {
        if (aContNo == null || aContNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "IndiPayComptPrintBL";
            tError.functionName = "getHospital";
            tError.errorMessage = "保单号不能为空！";
            this.mErrors.addOneError(tError);
            return false;
        }
        String sql = "select count(1) from ldcode where codetype='printriskcode' and othersign='1' and code in (select riskcode from LCPol where ContNo='"
                     + aContNo + "')"
                     ;
        ExeSQL tExeSQL = new ExeSQL();
        String tCount = tExeSQL.getOneValue(sql);
        //如果查询的结果是0，表示该合同下没有险种需要打印医院信息
        if (tCount.equals("0")) {
            return true;
        } 
        else 
        {
	         sql =" select b.HospitName ,b.Address from LDHospital b,LCCont a " +
	                " where associateclass = '1' and b.managecom = substr(a.managecom,1,4)" +
	                " and a.ContNo='" + aContNo + "'";
	        SSRS tSSRS = tExeSQL.execSQL(sql);
	        if (tSSRS.getMaxRow() < 1) 
	        {
                ListTable tListTable = new ListTable();
                for (int i = 1; i <= 1; i++) {
                    String[] info = new String[2];
                    for (int j = 1; j <= 1; j++) {
                        info[j - 1] = "无";
                    }
                    tListTable.add(info);
                }
                String[] title = {"推荐医院名称", "联系地址"};
                tListTable.setName("SpecHospital");
                aXmlexport.addListTable(tListTable, title);
                
                return true ;
            }
	        ListTable tListTable = new ListTable();        
	        //对于北京分公司续保的保单，如果原保单的录入日期（InputDate）在2011-5-1（含）之后的，并且保单包含《守护专家住院定额个人医疗保险》的，在保单定点医院打印时，在定点医院清单中对以上15个医院加括号说明：本医院对部分险种不是定点医院，具体险种见下方注释。
	        String sqlBeiJing = "select b.HospitName,b.Address from LDHospital b,LCCont a " 
						      + " where associateclass in ('1') and b.managecom = substr(a.managecom,1,4) " 
						      + " and a.ContNo='" + aContNo + "'"
						      +" and exists (select 1 from lddinghospital c where b.Hospitcode=c.Hospitcode and   subStr(a.managecom,1,4)=c.managecom  and a.InputDate between c.validate and c.cinValidate and riskcode in (select riskcode from lcpol where contno =a.contno))"
						      ;
			ExeSQL tExeSQLBeiJing = new ExeSQL();
			SSRS tSSRSBeiJing = tExeSQLBeiJing.execSQL(sqlBeiJing);
			if(tSSRSBeiJing.getMaxRow() >0)
			{
				printFlag=true;
	        	//查出需要加括号的定点医院
	        	 String sqlBeiFen1 = "select b.HospitName,b.Address from LDHospital b,LCCont a "
	        		               +" where associateclass = '1' and b.managecom = substr(a.managecom,1,4) " 
	        		               +" and a.ContNo='" + aContNo + "'"
	                               +" and exists (select 1 from lddinghospital c where b.Hospitcode=c.Hospitcode and   subStr(a.managecom,1,4)=c.managecom  and a.InputDate between c.validate and c.cinValidate and riskcode in (select riskcode from lcpol where contno =a.contno))"
	                               ;
				ExeSQL tExeSQLBeiFen1 = new ExeSQL();
				SSRS tSSRSBeiFen1 = tExeSQLBeiFen1.execSQL(sqlBeiFen1);
				
				 for (int i = 1; i <= tSSRSBeiFen1.getMaxRow(); i++) {
			            String[] info = new String[2];
			            for (int j = 1; j <= tSSRSBeiFen1.getMaxCol(); j++) {
			            	if(j==1)
			            	{
			            		info[j - 1] = tSSRSBeiFen1.GetText(i, j)+"      ※ ";
			            	}
			            	else
			            	{
			                 info[j - 1] = tSSRSBeiFen1.GetText(i, j);
			            	}
			            }
			            tListTable.add(info);
			        }
	//			查出不需要加括号的定点医院
				 String sqlBeiFen2 = "select b.HospitName,b.Address from LDHospital b,LCCont a " 
								   + " where associateclass = '1' and b.managecom = substr(a.managecom,1,4) " 
								   +" and a.ContNo='" + aContNo + "'"
	                               +" and not exists (select 1 from lddinghospital c where b.Hospitcode=c.Hospitcode and   subStr(a.managecom,1,4)=c.managecom  and a.InputDate between c.validate and c.cinValidate and riskcode in (select riskcode from lcpol where contno =a.contno))"
	                               ;
				ExeSQL tExeSQLBeiFen2 = new ExeSQL();
				SSRS tSSRSBeiFen2 = tExeSQLBeiFen2.execSQL(sqlBeiFen2);
				
				 for (int i = 1; i <= tSSRSBeiFen2.getMaxRow(); i++) {
			            String[] info = new String[2];
			            for (int j = 1; j <= tSSRSBeiFen2.getMaxCol(); j++) {
			                 info[j - 1] = tSSRSBeiFen2.GetText(i, j);
			            }
			            tListTable.add(info);
			        }
	
		        
		        String[] title = {"指定医院名称", "联系地址"};
		        tListTable.setName("SpecHospital");
		        aXmlexport.addListTable(tListTable, title);   
			}
			else
			{                           
		        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
		            String[] info = new String[2];
		            for (int j = 1; j <= tSSRS.getMaxCol(); j++) {
		                info[j - 1] = tSSRS.GetText(i, j);
		            }
		            tListTable.add(info);
		        }
		        String[] title = {"推荐医院名称", "联系地址"};
		        tListTable.setName("SpecHospital");
		        aXmlexport.addListTable(tListTable, title);
		        
			}	
		 		  	        
        }
        
      

        return true;
    }
}
