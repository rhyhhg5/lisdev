package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class GrpDueFeeSplitUI {

  //业务处理相关变量
  private VData mInputData ;
  public  CErrors mErrors=new CErrors();

  public GrpDueFeeSplitUI() {
  }
  public static void main(String[] args) {


    GrpDueFeePlanUI GrpDueFeePlanUI1 = new GrpDueFeePlanUI();
    GlobalInput tGI = new GlobalInput();
    tGI.ComCode="86";
    tGI.Operator="wuser";
    tGI.ManageCom="86";
    TransferData tempTransferData=new TransferData();
    tempTransferData.setNameAndValue("StartDate","2006-04-02");
    tempTransferData.setNameAndValue("EndDate","2006-08-25");
    tempTransferData.setNameAndValue("ManageCom",tGI.ManageCom);

    LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
    tLCGrpContSchema.setGrpContNo("0000018001");
    VData tVData = new VData();
    tVData.add(tGI);
    tVData.add(tLCGrpContSchema);
    tVData.add(tempTransferData);
    GrpDueFeePlanUI tGrpDueFeeUI = new GrpDueFeePlanUI();
    tGrpDueFeeUI.submitData(tVData,"INSERT");

  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    GrpDueFeeSplitBL tGrpDueFeeSplitBL=new GrpDueFeeSplitBL();
    System.out.println("Start GrpDueFeePlan UI Submit...");
    tGrpDueFeeSplitBL.submitData(mInputData,cOperate);

    System.out.println("End GrpDueFeePlan UI Submit...");

    mInputData=null;
    //如果有需要处理的错误，则返回
    if (tGrpDueFeeSplitBL .mErrors .needDealError() )
       {
       this.mErrors .copyAllErrors(tGrpDueFeeSplitBL.mErrors ) ;
       return false;
       }
System.out.println("error num="+mErrors.getErrorCount());
    return true;
  }

}
