package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.CommonBL;
import java.util.*;
import java.lang.String;
import com.sinosoft.lis.bq.AppAcc;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author HZM
 * @version 1.0
 */

public class GrpDueFeePlanBL {
	// 错误处理类，每个需要错误处理的类中都放置该类
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	private GlobalInput tGI = new GlobalInput();

	/** 数据操作字符串 */
	private String mOperate = "";

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();
	
	private Reflections mRef = new Reflections();

	private String mGrpContNo = ""; // 团体保单号

	private String mPrtNo = ""; // 印刷号

	private String mProposalGrpContNo = ""; // 团体投保单号

	private String mPlanCode = ""; // 缴费代码
	
	private String mFlag = "";//是否审批标记 Y为需要审批
	
	double totalPay = 0;//一共收费

	private String tNo = ""; //缴费通知书号
	
	private String prtSeqNo = "";
	
	private TransferData mTransferData = new TransferData();

	/** 数据表 保存数据 */
	// 集体保单表
	private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

	// 集体险种保单表
	private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();

	private LCGrpPayPlanSet mLCGrpPayPlanSet = new LCGrpPayPlanSet();
	
	private LCGrpPayPlanDetailSet mLCGrpPayPlanDetailSet = new LCGrpPayPlanDetailSet(); 

	private LCGrpPayActuSet mLCGrpPayActuSet = new LCGrpPayActuSet();
	
	private LCGrpPayActuDetailSet mLCGrpPayActuDetailSet = new LCGrpPayActuDetailSet();
	// 集体险种保单表,接受页面传入的参数
	private LCGrpPayActuSet tLCGrpPayActuSet = new LCGrpPayActuSet();

	// 应收总表
	private LJSPaySchema mLJSPaySchema = new LJSPaySchema();

	private LJSPayBSchema mLJSPayBSchema = new LJSPayBSchema();

	// 应收总表集
	private LJSPayGrpSet mLJSPayGrpSet = new LJSPayGrpSet();

	private LJSPayGrpSet mYelLJSPayGrpSet = new LJSPayGrpSet();

	// 应收总表集备份表
	private LJSPayGrpBSet mLJSPayGrpBSet = new LJSPayGrpBSet();

	private LJSPayGrpBSet mYelLJSPayGrpBSet = new LJSPayGrpBSet();

	// 应收总表交费日期
	private String strNewLJSPayDate;

	// 应收总表最早交费日期
	private String strNewLJSStartDate;
	
	// 团单生效日
	private String mLastPayToDate="";

	// 团单交至日
	private String mCurPayToDate="";

	// 应收总表费额
	private double totalsumPay = 0;

	private LCUrgeVerifyLogSchema mLCUrgeVerifyLogSchema = new LCUrgeVerifyLogSchema();

	// 缴费通知书
	private LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();

	private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

	private LOPRTManagerSubSet mLOPRTManagerSubSet = new LOPRTManagerSubSet();

	private MMap map = new MMap();
	
	private MMap mapAcc = new MMap();

	private String mOpertor = "INSERT";

	private String mOperateFlag = "1"; // 1 : 个案催收，2：批量催收

	private String mserNo = ""; // 批次号

	public GrpDueFeePlanBL() {
	}

	public static void main(String[] args) {

		GlobalInput tGt = new GlobalInput();
		tGt.ComCode = "86";
		tGt.Operator = "wuser";
		tGt.ManageCom = "86";

		LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
		tLCGrpContSchema.setGrpContNo("0000019706");

		TransferData tempTransferData = new TransferData();
		tempTransferData.setNameAndValue("StartDate", "2005-01-01");
		tempTransferData.setNameAndValue("EndDate", "2008-8-13");
		tempTransferData.setNameAndValue("ManageCom", "86");
		tempTransferData.setNameAndValue("LoadFlag", "WMD");

		VData tVData = new VData();
		tVData.add(tLCGrpContSchema);
		tVData.add(tGt);
		tVData.add(tempTransferData);

		// LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
		// tLCGrpPolSchema.setGrpPolNo("86110020040220000102");
		// VData tv = new VData();
		// tv.add(tLCGrpPolSchema);
		// tv.add(tGt);

		GrpDueFeePlanBL GrpDueFeeBL1 = new GrpDueFeePlanBL();
		GrpDueFeeBL1.submitData(tVData, "INSERT");
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData mInputData) {

		tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
		tLCGrpPayActuSet = ((LCGrpPayActuSet) mInputData.getObjectByObjectName("LCGrpPayActuSet", 0));
		mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
		if (tGI == null || tLCGrpPayActuSet == null || mTransferData == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GrpDueFeeBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的数据，请您确认!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mPrtNo = (String) mTransferData.getValueByName("PrtNo");
		mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
		mProposalGrpContNo = (String) mTransferData.getValueByName("ProposalGrpContNo");
		mPlanCode = (String) mTransferData.getValueByName("PlanCode");
		mFlag = (String) mTransferData.getValueByName("Flag");
		if (mPrtNo == null || mPrtNo.equals("")) {
			// @@错误处理
			System.out.println("GrpDueFeePlanBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "GrpDueFeePlanBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入印刷号为空!";
			mErrors.addOneError(tError);
			return false;
		}
		if (mGrpContNo == null || mGrpContNo.equals("")) {
			// @@错误处理
			System.out.println("GrpDueFeePlanBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "GrpDueFeePlanBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入保单号为空!";
			mErrors.addOneError(tError);
			return false;
		}
		if (mProposalGrpContNo == null || mProposalGrpContNo.equals("")) {
			// @@错误处理
			System.out.println("GrpDueFeePlanBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "GrpDueFeePlanBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入投保号为空!";
			mErrors.addOneError(tError);
			return false;
		}
		if (mPlanCode == null || mPlanCode.equals("")) {
			// @@错误处理
			System.out.println("GrpDueFeePlanBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "GrpDueFeePlanBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入约定缴费代码号为空!";
			mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 按照GrpContNo查询是否有保全案件正在处理
	 * 
	 * @param aPolNo
	 *            String
	 * @return boolean
	 */
	public boolean checkEdorItemState(String aGrpContNo) {
		String sql = "select a.* " + "from LPGrpEdorItem a, LPEdorApp b " + "where a.EdorAcceptNo = b.EdorAcceptNo " + "   and a.grpcontno = '" + aGrpContNo + "' " + "   and b.EdorState != '0' ";
		System.out.println(sql);
		LPEdorItemSet set = new LPEdorItemDB().executeQuery(sql);
		if (set.size() > 0) {
			String edorInfo = "";
			for (int i = 1; i <= set.size(); i++) {
				edorInfo += set.get(i).getEdorNo() + " " + set.get(i).getEdorType() + ", ";
			}
			mErrors.addOneError("保单有未结案的保全项目:" + edorInfo);
			return false;
		}
		return true;
	}

	/**
	 * 按照GrpContNo查询是否有理赔案件正在处理
	 * 
	 * @param aPolNo
	 *            String
	 * @return boolean
	 */
	public boolean checkClaimState(String aGrpContNo) {
		boolean tClaimFlag = true;
		String tSQL = "SELECT 1 FROM llcasepolicy a,llcase b WHERE a.caseno=b.caseno " + "AND b.rgtstate NOT IN ('11','12','14')" // 11
				// 通知状态
				// 12
				// 给付状态
				// 14
				// 撤件状态
				+ " AND a.grpcontno='" + aGrpContNo + "'";
		ExeSQL exesql = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = exesql.execSQL(tSQL);
		if (tSSRS.getMaxRow() > 0) {
			tClaimFlag = false;
		}
		return tClaimFlag;
	}

	/**
	 * 按照GrpContNo查询是否已经被催收
	 * 
	 * @param aPolNo
	 *            String
	 * @return boolean
	 */
	public boolean checkLjsState(String aGrpContNo) {
		boolean tClaimFlag = true;
		String tSQL = "SELECT 1 FROM ljspayb a WHERE   a.dealstate IN ('0','4','5')" + " AND a.otherno='" + aGrpContNo + "' and a.othernotype='1' ";
		ExeSQL exesql = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = exesql.execSQL(tSQL);
		if (tSSRS.getMaxRow() > 0) {
			tClaimFlag = false;
		}
		return tClaimFlag;
	}

	/**
	 * 杨红于2005-07-19添加数据校验，判断页面中查询出的信息是否已被催收
	 * 
	 * @return boolean
	 */
	public boolean checkData() {
		System.out.println("约定缴费续期抽档校验");

		// 下面校验lcgrppayplan是否有符合条件的记录
		StringBuffer tSBql = new StringBuffer(128);
		tSBql.append("select * from lcgrppayplan where  prtno='").append(mPrtNo).append("' and plancode='").append(mPlanCode).append("'");
		String GrpPayPlanStr = tSBql.toString();
		System.out.println(GrpPayPlanStr);
		LCGrpPayPlanDB tLCGrpPayPlanDB = new LCGrpPayPlanDB();
		mLCGrpPayPlanSet = tLCGrpPayPlanDB.executeQuery(GrpPayPlanStr);
		if (tLCGrpPayPlanDB.mErrors.needDealError()) {
			CError.buildErr(this, "团单号:" + mGrpContNo + "下的集体险种查询失败！");
			return false;
		}
		if (mLCGrpPayPlanSet.size() == 0) {
			CError.buildErr(this, "团单号:" + mGrpContNo + "下没有符合催收条件的集体险种！");
			return false;
		}
		if (mLCGrpPayPlanSet.size() != tLCGrpPayActuSet.size()) {
			CError.buildErr(this, "约定缴费的计划和续期抽档录入的计划不符！");
			return false;
		}
		
		// 获取lcgrppayplandetail是否有符合条件的记录
		tSBql = new StringBuffer(128);
		tSBql.append("select * from lcgrppayplandetail where state='2' and prtno='").append(mPrtNo).append("' and plancode='").append(mPlanCode).append("'");
		String GrpPayPlanDeStr = tSBql.toString();
		System.out.println(GrpPayPlanDeStr);
		LCGrpPayPlanDetailDB tLCGrpPayPlanDetailDB = new LCGrpPayPlanDetailDB();
		mLCGrpPayPlanDetailSet = tLCGrpPayPlanDetailDB.executeQuery(GrpPayPlanDeStr);
		if (tLCGrpPayPlanDetailDB.mErrors.needDealError()) {
			CError.buildErr(this, "团单号:" + mGrpContNo + "下的集体险种查询失败！");
			return false;
		}
		if (mLCGrpPayPlanDetailSet.size() == 0) {
			CError.buildErr(this, "团单号:" + mGrpContNo + "下没有符合催收条件的集体险种！");
			return false;
		}

		if (!checkEdorItemState(mGrpContNo)) {
			CError.buildErr(this, "团单号:" + mGrpContNo + "催收错误:正在做保全!");
			return false;
		}
//		if (!checkClaimState(mGrpContNo)) {
//			CError.buildErr(this, "团单号:" + mGrpContNo + "催收错误:正在做理赔!");
//			return false;
//		}
		if (!checkLjsState(mGrpContNo)) {
			CError.buildErr(this, "团单号:" + mGrpContNo + "催收错误:该团单已经存在尚未缴费核销的催收!");
			return false;
		}

		tSBql = new StringBuffer(128);
		tSBql.append("select * from LCGrpPol where GrpContNo='").append(mGrpContNo).append("'  and AppFlag='1' and (StateFlag is null or StateFlag = '1') ");
		String GrpPolStr = tSBql.toString();
		LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
		System.out.println(GrpPolStr);
		mLCGrpPolSet = tLCGrpPolDB.executeQuery(GrpPolStr);
		if (tLCGrpPolDB.mErrors.needDealError()) {
			CError.buildErr(this, "团单号:" + mGrpContNo + "下的集体险种查询失败！");
			return false;
		}
		if (mLCGrpPolSet.size() == 0) {
			CError.buildErr(this, "团单号:" + mGrpContNo + "下没有符合催收条件的集体险种！");
			return false;
		}
		tSBql = new StringBuffer(128);
		tSBql.append("select count(distinct riskcode) from lcgrppayplandetail where state='2' and  prtno='").append(mPrtNo).append("'  and plancode='").append(mPlanCode).append("'");
		int countrisk = Integer.parseInt(new ExeSQL().getOneValue(tSBql.toString())); 
		if (mLCGrpPolSet.size()!=countrisk) {
			CError.buildErr(this, "团单号:" + mGrpContNo + "下约定缴费险种和团单的险种个数不一致！");
			return false;
		}

		LCGrpContDB tLCGrpContDB = new LCGrpContDB();
		tLCGrpContDB.setGrpContNo(mGrpContNo);
		if (!tLCGrpContDB.getInfo()) {
			CError.buildErr(this, "团单号:" + mGrpContNo + "下的集体保单查询失败！");
			return false;
		}
		mLCGrpContSchema=tLCGrpContDB.getSchema();
		return true;
	}

	public double getCalPrem(LCGrpPayPlanDetailSchema tLCGrpPayPlanDetailSchema ,double actu){
		String allprem= new ExeSQL().getOneValue("select prem from LCGrpPayPlan where prtno='"+mPrtNo+"' and plancode='"+tLCGrpPayPlanDetailSchema.getPlanCode()+"' and contplancode='"+tLCGrpPayPlanDetailSchema.getContPlanCode()+"'");
		double tallprem=Double.parseDouble(allprem);
		if(tallprem==0){
			return 0;
		}
		else
		return Arith.round(actu*tLCGrpPayPlanDetailSchema.getPrem()/Double.parseDouble(allprem),2);
	}
	
	
	// 为更新数据库准备数据
	public boolean prepareData() {

		map.put(mLCGrpPayActuSet, "INSERT");
		map.put(mLCGrpPayActuDetailSet, "INSERT");
		map.put(mLJSPayGrpSet, "INSERT");
		map.put(mLJSPaySchema, "INSERT");
		map.put(mLJSPayGrpBSet, "INSERT");
		map.add(mapAcc);
		map.put(mLJSPayBSchema, "INSERT");
		map.put(mLOPRTManagerSubSet, "INSERT");
		map.put(mLOPRTManagerSchema, "INSERT");
		updateDealState();
		mInputData.add(map);
		return true;
	}

	// 传输数据的公共方法
	public boolean submitData(VData cInputData, String cOperate) {

		System.out.println("GrpDueFeeBL Begin......");
		this.mOperate = cOperate;

		if (!getInputData(cInputData)) {
			return false;
		}
		if (!checkData()) {
			return false;
		}
		// 20120203 xp 避免并发错误，加锁
		if (!lockGrpCont()) {
			return false;
		}

		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		if (!prepareData()) {
			return false;
		}
		PubSubmit tPubSubmit = new PubSubmit();
		if (tPubSubmit.submitData(mInputData, "") == false) {
			// 添加数据到催收核销日志表
			if (!dealUrgeLog("2", "PubSubmit:处理总应收表等失败!", "UPDATE")) {
				return false;
			}

			this.mErrors.addOneError("PubSubmit:处理数据库失败!");

			return false;
		}
		if (!dealUrgeLog("3", "", "UPDATE")) {
			return false;
		}

		return true;
	}
	
	// 设置续期相关表
	public boolean SetActu() {
		for (int i = 1; i <= tLCGrpPayActuSet.size(); i++) {
			LCGrpPayActuSchema tLCGrpPayActuSchema=new LCGrpPayActuSchema();
			mRef.transFields(tLCGrpPayActuSchema, tLCGrpPayActuSet.get(i));
//			对录入的金额四舍五入
			tLCGrpPayActuSchema.setPrem(Arith.round(tLCGrpPayActuSchema.getPrem(),2));
			tLCGrpPayActuSchema.setGetNoticeNo(tNo);
			tLCGrpPayActuSchema.setGrpContNo(mGrpContNo);
			tLCGrpPayActuSchema.setPrtNo(mPrtNo);
			tLCGrpPayActuSchema.setProposalGrpContNo(mProposalGrpContNo);
			tLCGrpPayActuSchema.setState("2");//刚抽档状态
			tLCGrpPayActuSchema.setOperator(tGI.Operator);
			tLCGrpPayActuSchema.setMakeDate(CurrentDate);
			tLCGrpPayActuSchema.setMakeTime(CurrentTime);
			tLCGrpPayActuSchema.setModifyDate(CurrentDate);
			tLCGrpPayActuSchema.setModifyTime(CurrentTime);
			mLCGrpPayActuSet.add(tLCGrpPayActuSchema);
			double tactu=tLCGrpPayActuSchema.getPrem();
			double tcal = 0;
//			设置续期相关明细表
			for (int j = 1; j <= mLCGrpPayPlanDetailSet.size(); j++) {
				if(tLCGrpPayActuSchema.getContPlanCode().equals(mLCGrpPayPlanDetailSet.get(j).getContPlanCode())){
					LCGrpPayActuDetailSchema tLCGrpPayActuDetailSchema=new LCGrpPayActuDetailSchema();
					mRef.transFields(tLCGrpPayActuDetailSchema, mLCGrpPayPlanDetailSet.get(j));
					tLCGrpPayActuDetailSchema.setGrpContNo(mGrpContNo);
					tLCGrpPayActuDetailSchema.setGetNoticeNo(tNo);
					tLCGrpPayActuDetailSchema.setPrem(getCalPrem(mLCGrpPayPlanDetailSet.get(j),tactu));
					tLCGrpPayActuDetailSchema.setOperator(tGI.Operator);
					tLCGrpPayActuDetailSchema.setMakeDate(CurrentDate);
					tLCGrpPayActuDetailSchema.setMakeTime(CurrentTime);
					tLCGrpPayActuDetailSchema.setModifyDate(CurrentDate);
					tLCGrpPayActuDetailSchema.setModifyTime(CurrentTime);
					mLCGrpPayActuDetailSet.add(tLCGrpPayActuDetailSchema);
					tcal+=tLCGrpPayActuDetailSchema.getPrem();
				}
			}
//			补齐4舍5入差额
			if(tcal!=tactu){
				for (int j = 1; j <= mLCGrpPayActuDetailSet.size(); j++) {
					if(mLCGrpPayActuDetailSet.get(j).getContPlanCode().equals(tLCGrpPayActuSchema.getContPlanCode())){
						mLCGrpPayActuDetailSet.get(j).setPrem(Arith.round(mLCGrpPayActuDetailSet.get(j).getPrem()+tactu-tcal,2));
						break;
					}
				}
			}
		}
		return true;
	}

	/**
	 * Yangh于2005-07-19创建新的dealData的处理逻辑
	 * 
	 * @return boolean
	 */
	private boolean dealData() {

		System.out.println("GrpDueFeeBL dealData Begin");
		System.out.println("约定缴费续期团险抽档开始:" + mGrpContNo);
        //添加数据到催收核销日志表
        tNo = PubFun1.CreateMaxNo("PayNoticeNo", PubFun.getNoLimit(mLCGrpContSchema.getManageCom()));
		prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", PubFun.getNoLimit(mLCGrpContSchema.getManageCom()));
		mserNo=PubFun1.CreateMaxNo("SERIALNO", PubFun.getNoLimit(mLCGrpContSchema.getManageCom()));

        if (!dealUrgeLog("1", "", "INSERT")) {
            return false;
        }
        
//      设置续期相关表
        SetActu();

//		 计算相关日期
		if (!calPayDate()) {
			return false;
		}
		
		for (int i = 1; i <= mLCGrpPolSet.size(); i++) {

			int grpPayTimes = getGrpPayTimes(mLCGrpPolSet.get(i).getGrpPolNo()); // 获取ljspaygrp的交费次数
			double grpSumPay = 0.0;

			LCGrpPolSchema tLCGrpPolSchema = mLCGrpPolSet.get(i);
			String GrpPolNo = tLCGrpPolSchema.getGrpPolNo();
			
			System.out.println("GrpDueFeePlanBL： 取险种缴费金额数据 ");			
			for (int j = 1; j <= mLCGrpPayActuDetailSet.size(); j++) {
				if(mLCGrpPayActuDetailSet.get(j).getRiskCode().equals(tLCGrpPolSchema.getRiskCode())){
					grpSumPay+=mLCGrpPayActuDetailSet.get(j).getPrem();
					grpSumPay = Arith.round(grpSumPay,2);
				}
			}
			LJSPayGrpSchema tLJSPayGrpSchema = new LJSPayGrpSchema();
			tLJSPayGrpSchema.setGrpPolNo(GrpPolNo);
			tLJSPayGrpSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
			tLJSPayGrpSchema.setAppntNo(mLCGrpContSchema.getAppntNo()); // 随便取一个lcpol的投保人
			tLJSPayGrpSchema.setPayCount(grpPayTimes + 1); // 交费次数
			tLJSPayGrpSchema.setGetNoticeNo(tNo); // 通知书号
			tLJSPayGrpSchema.setSumDuePayMoney(grpSumPay);
			tLJSPayGrpSchema.setSumActuPayMoney(grpSumPay);
			tLJSPayGrpSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
			tLJSPayGrpSchema.setPayDate(strNewLJSPayDate); // 交费日期
			tLJSPayGrpSchema.setPayType("ZC"); // 交费类型=ZC 正常交费
			tLJSPayGrpSchema.setLastPayToDate(mLastPayToDate);
			tLJSPayGrpSchema.setCurPayToDate(mCurPayToDate);
			tLJSPayGrpSchema.setApproveCode(tLCGrpPolSchema.getApproveCode());
			tLJSPayGrpSchema.setApproveDate(tLCGrpPolSchema.getApproveDate());
			tLJSPayGrpSchema.setApproveTime(tLCGrpPolSchema.getApproveTime());
			tLJSPayGrpSchema.setManageCom(tLCGrpPolSchema.getManageCom());
			tLJSPayGrpSchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
			tLJSPayGrpSchema.setAgentType(tLCGrpPolSchema.getAgentType());
			tLJSPayGrpSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
			tLJSPayGrpSchema.setSerialNo(mserNo);
			tLJSPayGrpSchema.setInputFlag("1");
			tLJSPayGrpSchema.setOperator(tGI.Operator);
			tLJSPayGrpSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
			tLJSPayGrpSchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());
			tLJSPayGrpSchema.setMakeDate(PubFun.getCurrentDate()); // 入机日期
			tLJSPayGrpSchema.setMakeTime(PubFun.getCurrentTime()); // 入机时间
			tLJSPayGrpSchema.setModifyDate(PubFun.getCurrentDate()); // 最后一次修改日期
			tLJSPayGrpSchema.setModifyTime(PubFun.getCurrentTime()); // 最后一次修改时间
			mLJSPayGrpSet.add(tLJSPayGrpSchema);
			// 备份表
			LJSPayGrpBSchema tLJSPayGrpBSchema = new LJSPayGrpBSchema();
			tLJSPayGrpBSchema.setGrpPolNo(GrpPolNo);
			tLJSPayGrpBSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
			tLJSPayGrpBSchema.setAppntNo(mLCGrpContSchema.getAppntNo()); // 随便取一个lcpol的投保人
			tLJSPayGrpBSchema.setPayCount(grpPayTimes + 1); // 交费次数
			tLJSPayGrpBSchema.setGetNoticeNo(tNo); // 通知书号
			tLJSPayGrpBSchema.setSumDuePayMoney(grpSumPay);
			tLJSPayGrpBSchema.setSumActuPayMoney(grpSumPay);
			tLJSPayGrpBSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
			tLJSPayGrpBSchema.setPayDate(strNewLJSPayDate); // 交费日期
			tLJSPayGrpBSchema.setPayType("ZC"); // 交费类型=ZC 正常交费
			tLJSPayGrpBSchema.setLastPayToDate(mLastPayToDate);
			tLJSPayGrpBSchema.setCurPayToDate(mCurPayToDate);
			tLJSPayGrpBSchema.setApproveCode(tLCGrpPolSchema.getApproveCode());
			tLJSPayGrpBSchema.setApproveDate(tLCGrpPolSchema.getApproveDate());
			tLJSPayGrpBSchema.setApproveTime(tLCGrpPolSchema.getApproveTime());
			tLJSPayGrpBSchema.setManageCom(tLCGrpPolSchema.getManageCom());
			tLJSPayGrpBSchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
			tLJSPayGrpBSchema.setAgentType(tLCGrpPolSchema.getAgentType());
			tLJSPayGrpBSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
			tLJSPayGrpBSchema.setSerialNo(mserNo);
			tLJSPayGrpBSchema.setInputFlag("1");
			tLJSPayGrpBSchema.setOperator(tGI.Operator);
			tLJSPayGrpBSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
			tLJSPayGrpBSchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());
			tLJSPayGrpBSchema.setMakeDate(PubFun.getCurrentDate()); // 入机日期
			tLJSPayGrpBSchema.setMakeTime(PubFun.getCurrentTime()); // 入机时间
			tLJSPayGrpBSchema.setModifyDate(PubFun.getCurrentDate()); // 最后一次修改日期
			tLJSPayGrpBSchema.setModifyTime(PubFun.getCurrentTime()); // 最后一次修改时间
			if (tLJSPayGrpBSchema.getSumDuePayMoney() > 0.01)
				tLJSPayGrpBSchema.setDealState("0"); // 催收状态：不成功
			else
				tLJSPayGrpBSchema.setDealState("4"); // 催收状态：待核销
			mLJSPayGrpBSet.add(tLJSPayGrpBSchema);

			totalPay += grpSumPay; // 计算出合同下总的应交费用
			LOPRTManagerSubSchema tLOPRTManagerSubSchema = new LOPRTManagerSubSchema();
			tLOPRTManagerSubSchema.setPrtSeq(prtSeqNo);
			tLOPRTManagerSubSchema.setGetNoticeNo(tNo);
			tLOPRTManagerSubSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
			tLOPRTManagerSubSchema.setOtherNoType("01");
			tLOPRTManagerSubSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
			tLOPRTManagerSubSchema.setDuePayMoney(grpSumPay);
			tLOPRTManagerSubSchema.setAppntName(mLCGrpContSchema.getAppntNo());
			tLOPRTManagerSubSchema.setTypeFlag("1");
			mLOPRTManagerSubSet.add(tLOPRTManagerSubSchema);
		}

		boolean yelFlag = false; // 上次余额的标志
		boolean yetFlag = false; // 置是否有余额的标志位（上期合同余额用于交本期保费还有剩余，则置true）
		LJSPayGrpSchema yelLJSPayGrpSchema = new LJSPayGrpSchema(); // 如果有差额，添加一个应收集体交费schema
		LJSPayGrpSchema yetLJSPayGrpSchema = new LJSPayGrpSchema();
		LJSPayGrpBSchema yelLJSPayGrpBSchema = new LJSPayGrpBSchema(); // 如果有差额，添加一个应收集体交费schema
		LJSPayGrpBSchema yetLJSPayGrpBSchema = new LJSPayGrpBSchema();
		
		System.out.println("GrpDueFeePlanBL： 产生应收记录" + mGrpContNo);
		// 杨红于2005-07-20说明：下面根据情况生成yel和yet的ljspaygrp的记录
		double leavingMoney = getAccBala(mLCGrpContSchema.getAppntNo());
		if (leavingMoney != Double.MIN_VALUE && leavingMoney > 0.0) {
			yelFlag = true;
			yelLJSPayGrpSchema.setGrpPolNo(mLCGrpPolSet.get(1).getGrpPolNo());
			yelLJSPayGrpSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
			yelLJSPayGrpSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
			yelLJSPayGrpSchema.setGetNoticeNo(tNo); // 通知书号
			yelLJSPayGrpSchema.setSumDuePayMoney(-leavingMoney);
			yelLJSPayGrpSchema.setSumActuPayMoney(-leavingMoney);
			yelLJSPayGrpSchema.setPayIntv(mLCGrpPolSet.get(1).getPayIntv());
			yelLJSPayGrpSchema.setPayDate(strNewLJSPayDate); // 交费日期
			yelLJSPayGrpSchema.setPayType("YEL");
			yelLJSPayGrpSchema.setLastPayToDate(mLastPayToDate);
			yelLJSPayGrpSchema.setCurPayToDate(mCurPayToDate);
			yelLJSPayGrpSchema.setApproveCode(mLCGrpPolSet.get(1).getApproveCode());
			yelLJSPayGrpSchema.setApproveDate(mLCGrpPolSet.get(1).getApproveDate());
			yelLJSPayGrpSchema.setApproveTime(mLCGrpPolSet.get(1).getApproveTime());

			yelLJSPayGrpSchema.setManageCom(mLCGrpPolSet.get(1).getManageCom());
			yelLJSPayGrpSchema.setAgentCom(mLCGrpPolSet.get(1).getAgentCom());
			yelLJSPayGrpSchema.setAgentCode(mLCGrpPolSet.get(1).getAgentCode());
			yelLJSPayGrpSchema.setAgentGroup(mLCGrpPolSet.get(1).getAgentGroup());
			yelLJSPayGrpSchema.setAgentType(mLCGrpPolSet.get(1).getAgentType());
			yelLJSPayGrpSchema.setAgentType(mLCGrpPolSet.get(1).getAgentType());
			yelLJSPayGrpSchema.setRiskCode(mLCGrpPolSet.get(1).getRiskCode());
			yelLJSPayGrpSchema.setInputFlag("1");
			yelLJSPayGrpSchema.setSerialNo(mserNo);
			yelLJSPayGrpSchema.setOperator(tGI.Operator);
			yelLJSPayGrpSchema.setMakeDate(PubFun.getCurrentDate()); // 入机日期
			yelLJSPayGrpSchema.setMakeTime(PubFun.getCurrentTime()); // 入机时间
			yelLJSPayGrpSchema.setModifyDate(PubFun.getCurrentDate()); // 最后一次修改日期
			yelLJSPayGrpSchema.setModifyTime(PubFun.getCurrentTime()); // 最后一次修改时间

			mLJSPayGrpSet.add(yelLJSPayGrpSchema);

			// 备份表

			yelLJSPayGrpBSchema.setGrpPolNo(mLCGrpPolSet.get(1).getGrpPolNo());
			yelLJSPayGrpBSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
			yelLJSPayGrpBSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
			yelLJSPayGrpBSchema.setGetNoticeNo(tNo); // 通知书号
			yelLJSPayGrpBSchema.setSumDuePayMoney(-leavingMoney);
			yelLJSPayGrpBSchema.setSumActuPayMoney(-leavingMoney);
			yelLJSPayGrpBSchema.setPayIntv(mLCGrpPolSet.get(1).getPayIntv());
			yelLJSPayGrpBSchema.setPayDate(strNewLJSPayDate); // 交费日期
			yelLJSPayGrpBSchema.setPayType("YEL");
			yelLJSPayGrpBSchema.setLastPayToDate(mLastPayToDate);
			yelLJSPayGrpBSchema.setCurPayToDate(mCurPayToDate);
			yelLJSPayGrpBSchema.setApproveCode(mLCGrpPolSet.get(1).getApproveCode());
			yelLJSPayGrpBSchema.setApproveDate(mLCGrpPolSet.get(1).getApproveDate());
			yelLJSPayGrpBSchema.setApproveTime(mLCGrpPolSet.get(1).getApproveTime());

			yelLJSPayGrpBSchema.setManageCom(mLCGrpPolSet.get(1).getManageCom());
			yelLJSPayGrpBSchema.setAgentCom(mLCGrpPolSet.get(1).getAgentCom());
			yelLJSPayGrpBSchema.setAgentType(mLCGrpPolSet.get(1).getAgentType());
			yelLJSPayGrpBSchema.setRiskCode(mLCGrpPolSet.get(1).getRiskCode());
			yelLJSPayGrpBSchema.setInputFlag("1");
			yelLJSPayGrpBSchema.setSerialNo(mserNo);
			yelLJSPayGrpBSchema.setOperator(tGI.Operator);
			yelLJSPayGrpBSchema.setMakeDate(PubFun.getCurrentDate()); // 入机日期
			yelLJSPayGrpBSchema.setMakeTime(PubFun.getCurrentTime()); // 入机时间
			yelLJSPayGrpBSchema.setModifyDate(PubFun.getCurrentDate()); // 最后一次修改日期
			yelLJSPayGrpBSchema.setModifyTime(PubFun.getCurrentTime()); // 最后一次修改时间
			yelLJSPayGrpBSchema.setDealState("0"); // 催收状态：不成功

			mLJSPayGrpBSet.add(yelLJSPayGrpBSchema);

			dealAccBala(yelLJSPayGrpBSchema);
		}
		if (yelFlag && leavingMoney > totalPay) {
			yetFlag = true;
			yetLJSPayGrpSchema.setGrpPolNo(mLCGrpPolSet.get(1).getGrpPolNo());
			// System.out.println("Test_yh:"+tLCGrpContSchema.getGrpContNo());
			yetLJSPayGrpSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
			yetLJSPayGrpSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
			yetLJSPayGrpSchema.setGetNoticeNo(tNo); // 通知书号
			yetLJSPayGrpSchema.setSumDuePayMoney(leavingMoney - totalPay);
			yetLJSPayGrpSchema.setSumActuPayMoney(leavingMoney - totalPay);
			yetLJSPayGrpSchema.setPayIntv(mLCGrpPolSet.get(1).getPayIntv());
			yetLJSPayGrpSchema.setPayDate(strNewLJSPayDate); // 交费日期
			yetLJSPayGrpSchema.setPayType("YET"); // 交费类型=yet
			yetLJSPayGrpSchema.setLastPayToDate(mLastPayToDate);
			yetLJSPayGrpSchema.setCurPayToDate(mCurPayToDate);
			yetLJSPayGrpSchema.setApproveCode(mLCGrpPolSet.get(1).getApproveCode());
			yetLJSPayGrpSchema.setApproveDate(mLCGrpPolSet.get(1).getApproveDate());
			yetLJSPayGrpSchema.setApproveTime(mLCGrpPolSet.get(1).getApproveTime());

			yetLJSPayGrpSchema.setManageCom(mLCGrpPolSet.get(1).getManageCom());
			yetLJSPayGrpSchema.setAgentCom(mLCGrpPolSet.get(1).getAgentCom());
			yetLJSPayGrpSchema.setAgentType(mLCGrpPolSet.get(1).getAgentType());
			yetLJSPayGrpSchema.setRiskCode(mLCGrpPolSet.get(1).getRiskCode());
			yetLJSPayGrpSchema.setAgentCode(mLCGrpPolSet.get(1).getAgentCode());
			yetLJSPayGrpSchema.setAgentGroup(mLCGrpPolSet.get(1).getAgentGroup());
			yetLJSPayGrpSchema.setInputFlag("1");
			yetLJSPayGrpSchema.setSerialNo(mserNo);
			yetLJSPayGrpSchema.setOperator(tGI.Operator);
			yetLJSPayGrpSchema.setMakeDate(PubFun.getCurrentDate()); // 入机日期
			yetLJSPayGrpSchema.setMakeTime(PubFun.getCurrentTime()); // 入机时间
			yetLJSPayGrpSchema.setModifyDate(PubFun.getCurrentDate()); // 最后一次修改日期
			yetLJSPayGrpSchema.setModifyTime(PubFun.getCurrentTime()); // 最后一次修改时间

			mLJSPayGrpSet.add(yetLJSPayGrpSchema);

			// 备份表
			yetLJSPayGrpBSchema.setGrpPolNo(mLCGrpPolSet.get(1).getGrpPolNo());
			// System.out.println("Test_yh:"+tLCGrpContSchema.getGrpContNo());
			yetLJSPayGrpBSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
			yetLJSPayGrpBSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
			yetLJSPayGrpBSchema.setGetNoticeNo(tNo); // 通知书号
			yetLJSPayGrpBSchema.setSumDuePayMoney(leavingMoney - totalPay);
			yetLJSPayGrpBSchema.setSumActuPayMoney(leavingMoney - totalPay);
			yetLJSPayGrpBSchema.setPayIntv(mLCGrpPolSet.get(1).getPayIntv());
			yetLJSPayGrpBSchema.setPayDate(strNewLJSPayDate); // 交费日期
			yetLJSPayGrpBSchema.setPayType("YET"); // 交费类型=yet
			yetLJSPayGrpBSchema.setLastPayToDate(mLastPayToDate);
			yetLJSPayGrpBSchema.setCurPayToDate(mCurPayToDate);
			yetLJSPayGrpBSchema.setApproveCode(mLCGrpPolSet.get(1).getApproveCode());
			yetLJSPayGrpBSchema.setApproveDate(mLCGrpPolSet.get(1).getApproveDate());
			yetLJSPayGrpBSchema.setApproveTime(mLCGrpPolSet.get(1).getApproveTime());
			yetLJSPayGrpBSchema.setManageCom(mLCGrpPolSet.get(1).getManageCom());
			yetLJSPayGrpBSchema.setAgentCom(mLCGrpPolSet.get(1).getAgentCom());
			yetLJSPayGrpBSchema.setAgentType(mLCGrpPolSet.get(1).getAgentType());
			yetLJSPayGrpBSchema.setRiskCode(mLCGrpPolSet.get(1).getRiskCode());
			yetLJSPayGrpBSchema.setInputFlag("1");
			yetLJSPayGrpBSchema.setSerialNo(mserNo);
			yetLJSPayGrpBSchema.setOperator(tGI.Operator);
			yetLJSPayGrpBSchema.setMakeDate(PubFun.getCurrentDate()); // 入机日期
			yetLJSPayGrpBSchema.setMakeTime(PubFun.getCurrentTime()); // 入机时间
			yetLJSPayGrpBSchema.setModifyDate(PubFun.getCurrentDate()); // 最后一次修改日期
			yetLJSPayGrpBSchema.setModifyTime(PubFun.getCurrentTime()); // 最后一次修改时间
			yetLJSPayGrpBSchema.setDealState("0"); // 催收状态：不成功
			mLJSPayGrpBSet.add(yetLJSPayGrpBSchema);

			dealAccBalaYET(yetLJSPayGrpBSchema);// 账户余额大于续期保费要在账户轨迹退费处理

		}
		// 下面生成应收总表的记录LJSPAY
		if (yelFlag) {
			// 如果团单上有余额,并且余额足够支付团单下应交的所有费用
			if (yetFlag) {
				totalsumPay = 0;
			} else {
				totalsumPay = totalPay - leavingMoney;
			}
		} else {
			totalsumPay = totalPay;
		}
		LJSPaySchema tLJSPaySchema = new LJSPaySchema();
		tLJSPaySchema.setGetNoticeNo(tNo); // 通知书号
		tLJSPaySchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
		tLJSPaySchema.setOtherNoType("1");
		tLJSPaySchema.setAppntNo(mLCGrpContSchema.getAppntNo());
		tLJSPaySchema.setSumDuePayMoney(totalsumPay);
		tLJSPaySchema.setPayDate(strNewLJSPayDate); // 有冲突,暂定为取集体险种最早的日期
		tLJSPaySchema.setStartPayDate(strNewLJSStartDate); // 交费最早应缴日期保存上次交至日期/暂定为取集体险种最晚的交至日期
		tLJSPaySchema.setBankOnTheWayFlag("0");
		tLJSPaySchema.setBankSuccFlag("0");
		tLJSPaySchema.setSendBankCount(0);
		tLJSPaySchema.setApproveCode(mLCGrpPolSet.get(1).getApproveCode());
		tLJSPaySchema.setApproveDate(mLCGrpPolSet.get(1).getApproveDate());
		tLJSPaySchema.setRiskCode("");
		tLJSPaySchema.setAccName(mLCGrpContSchema.getAccName());
		tLJSPaySchema.setBankAccNo(mLCGrpContSchema.getBankAccNo());
		tLJSPaySchema.setBankCode(mLCGrpContSchema.getBankCode());
		tLJSPaySchema.setSerialNo(mserNo);
		tLJSPaySchema.setOperator(tGI.Operator);
		tLJSPaySchema.setManageCom(mLCGrpContSchema.getManageCom());
		tLJSPaySchema.setAgentCom(mLCGrpContSchema.getAgentCom());
		tLJSPaySchema.setAgentCode(mLCGrpContSchema.getAgentCode());
		tLJSPaySchema.setAgentType(mLCGrpContSchema.getAgentType());
		tLJSPaySchema.setAgentGroup(mLCGrpContSchema.getAgentGroup());
		tLJSPaySchema.setMakeDate(PubFun.getCurrentDate());
		tLJSPaySchema.setMakeTime(PubFun.getCurrentTime());
		tLJSPaySchema.setModifyDate(PubFun.getCurrentDate());
		tLJSPaySchema.setModifyTime(PubFun.getCurrentTime());

		String sql = "select LF_SaleChnl('2', '" + mLCGrpContSchema.getGrpContNo() + "'),LF_MarketType('" + mLCGrpContSchema.getGrpContNo() + "') from dual where 1=1";
		SSRS tSSRS = new SSRS();
		tSSRS = new ExeSQL().execSQL(sql);
		if (tSSRS.getMaxRow() > 0) {
			String tSaleChnl = tSSRS.GetText(1, 1);
			String tMarketType = tSSRS.GetText(1, 2);
			tLJSPaySchema.setSaleChnl(tSaleChnl);
			tLJSPaySchema.setMarketType(tMarketType);
		}
		if("Y".equals(mFlag)){
			
			tLJSPaySchema.setCanSendBank(mFlag);
		}
		mLJSPaySchema.setSchema(tLJSPaySchema);
		// 备份表
		LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
		tLJSPayBSchema.setGetNoticeNo(tNo); // 通知书号
		tLJSPayBSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
		tLJSPayBSchema.setOtherNoType("1");
		tLJSPayBSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
		tLJSPayBSchema.setSumDuePayMoney(totalsumPay);
		tLJSPayBSchema.setPayDate(strNewLJSPayDate); // 有冲突,暂定为取集体险种最早的日期
		tLJSPayBSchema.setStartPayDate(strNewLJSStartDate); // 交费最早应缴日期保存上次交至日期/暂定为取集体险种最晚的交至日期
		tLJSPayBSchema.setBankOnTheWayFlag("0");
		tLJSPayBSchema.setBankSuccFlag("0");
		tLJSPayBSchema.setSendBankCount(0);
		tLJSPayBSchema.setApproveCode(mLCGrpPolSet.get(1).getApproveCode());
		tLJSPayBSchema.setApproveDate(mLCGrpPolSet.get(1).getApproveDate());
		tLJSPayBSchema.setRiskCode("");
		tLJSPayBSchema.setAccName(mLCGrpContSchema.getAccName());
		tLJSPayBSchema.setBankAccNo(mLCGrpContSchema.getBankAccNo());
		tLJSPayBSchema.setBankCode(mLCGrpContSchema.getBankCode());
		tLJSPayBSchema.setSerialNo(mserNo);
		tLJSPayBSchema.setOperator(tGI.Operator);
		tLJSPayBSchema.setManageCom(mLCGrpContSchema.getManageCom());
		tLJSPayBSchema.setAgentCom(mLCGrpContSchema.getAgentCom());
		tLJSPayBSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
		tLJSPayBSchema.setAgentType(mLCGrpContSchema.getAgentType());
		tLJSPayBSchema.setAgentGroup(mLCGrpContSchema.getAgentGroup());
		tLJSPayBSchema.setMakeDate(PubFun.getCurrentDate());
		tLJSPayBSchema.setMakeTime(PubFun.getCurrentTime());
		tLJSPayBSchema.setModifyDate(PubFun.getCurrentDate());
		tLJSPayBSchema.setModifyTime(PubFun.getCurrentTime());
		if (tLJSPayBSchema.getSumDuePayMoney() > 0.01)
			tLJSPayBSchema.setDealState("0"); // 催收状态：不成功
		else
			tLJSPayBSchema.setDealState("4"); // 催收状态：不成功

		tLJSPayBSchema.setSaleChnl(mLJSPaySchema.getSaleChnl());
		tLJSPayBSchema.setMarketType(mLJSPaySchema.getMarketType());

		mLJSPayBSchema.setSchema(tLJSPayBSchema);

		mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
		mLOPRTManagerSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
		mLOPRTManagerSchema.setOtherNoType("01");
		mLOPRTManagerSchema.setCode(PrintManagerBL.CODE_REPAY);
		mLOPRTManagerSchema.setManageCom(mLCGrpContSchema.getManageCom());
		mLOPRTManagerSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
		mLOPRTManagerSchema.setReqCom(mLCGrpContSchema.getManageCom());
		mLOPRTManagerSchema.setReqOperator(mLCGrpContSchema.getOperator());
		mLOPRTManagerSchema.setPrtType("0");
		mLOPRTManagerSchema.setStateFlag("0");
		mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
		mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
		mLOPRTManagerSchema.setStandbyFlag2(tNo); // 这里存放 催缴通知书号（也为交费收据号）

		return true;
	}

	/**
	 * 计算缴费日期
	 * 
	 * @return boolean
	 */
	private boolean calPayDate() {
        LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
		LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
		tLCGrpPolSchema.setPaytoDate(tLCGrpPayActuSet.get(1).getPaytoDate());
		tLCGrpPolSet.add(tLCGrpPolSchema);
		CalPayDateBL bl = new CalPayDateBL();
		HashMap dates = bl.calPayDateG(tLCGrpPolSet, mLCGrpContSchema.getPayMode());
		if (dates == null) {
			mErrors.copyAllErrors(bl.mErrors);
			return false;
		}

		strNewLJSStartDate = (String) dates.get(CalPayDateBL.STARTPAYDATE);
		strNewLJSPayDate = (String) dates.get(CalPayDateBL.PAYDATE);
		if (strNewLJSStartDate == null || strNewLJSStartDate.equals("") || strNewLJSPayDate == null || strNewLJSPayDate.equals("")) {
			mErrors.addOneError("计算失败，没有得到缴费日期。");
			return false;
		}
		LJAPayGrpDB tLJAPayGrpDB=new LJAPayGrpDB();
		tLJAPayGrpDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
		tLJAPayGrpDB.setPayType("ZC");
		tLJAPayGrpDB.setPayCount(1);
		mLastPayToDate=tLJAPayGrpDB.query().get(1).getLastPayToDate();
		mCurPayToDate=tLJAPayGrpDB.query().get(1).getCurPayToDate();
		if (mLastPayToDate == null || mLastPayToDate.equals("") || mCurPayToDate == null || mCurPayToDate.equals("")) {
			mErrors.addOneError("计算失败，没有得到交至日期。");
			return false;
		}
		return true;
	}

	/**
	 * 得到险种的最大交费次数
	 * 
	 * @param grpPolNo
	 *            String
	 * @return int
	 */
	private int getGrpPayTimes(String grpPolNo) {
		StringBuffer sql = new StringBuffer();
		sql.append("select max(int(payCount)) ").append("from LJAPayGrp ").append("where grpPolNo = '").append(grpPolNo).append("' ");
		String maxPayCount = new ExeSQL().getOneValue(sql.toString());
		if (maxPayCount.equals("") || maxPayCount.equals("null")) {
			return 0;
		}
		return Integer.parseInt(maxPayCount);
	}

	/**
	 * 得到投保人帐户余额
	 * 
	 * @param appntNo
	 *            String：投保人
	 * @return double：帐户余额
	 */
	private double getAccBala(String appntNo) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select accGetMoney ").append("from LCAppAcc ").append("where customerNo = '").append(appntNo).append("' ");
		String accGetBala = new ExeSQL().getOneValue(sql.toString());
		if (accGetBala.equals("") || accGetBala.equals("null")) {
			return Double.MIN_VALUE;
		}
		return Double.parseDouble(accGetBala);
	}

	/**
	 * 生成使用余额轨迹
	 * 
	 * @param yelLJSPayGrpBSchema
	 *            LJSPayGrpBSchema：余额财务应收数据
	 * @return boolean：处理成功true，否则false
	 */
	private boolean dealAccBalaYET(LJSPayGrpBSchema yelLJSPayGrpBSchema) {
		LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
		tLCAppAccTraceSchema.setCustomerNo(yelLJSPayGrpBSchema.getAppntNo());
		tLCAppAccTraceSchema.setOtherType("1"); // 团单号
		tLCAppAccTraceSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
		tLCAppAccTraceSchema.setMoney(yelLJSPayGrpBSchema.getSumActuPayMoney());
		tLCAppAccTraceSchema.setOperator(tGI.Operator);

		AppAcc tAppAcc = new AppAcc();
		MMap tMMap = tAppAcc.accTakeOutXQYET(tLCAppAccTraceSchema);
		if(tMMap!=null){
			// 这里得到的是引用
			LCAppAccSchema tLCAppAccSchema = (LCAppAccSchema) tMMap.getObjectByObjectName("LCAppAccSchema", 0);
			tLCAppAccTraceSchema = (LCAppAccTraceSchema) tMMap.getObjectByObjectName("LCAppAccTraceSchema", 0);
			// 续期业务未结束，不能直接更新余额
			tLCAppAccSchema.setAccBala(getAccBala(tLCAppAccSchema.getCustomerNo()));
			tLCAppAccSchema.setState("0"); // 续期核销时赋值为有效1
			
			tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala());
			tLCAppAccTraceSchema.setState(tLCAppAccSchema.getState());
			tLCAppAccTraceSchema.setBakNo(yelLJSPayGrpBSchema.getGetNoticeNo());
			mapAcc.put(tLCAppAccTraceSchema, "INSERT");
		}

		return true;
	}

	/**
	 * 生成使用余额轨迹
	 * 
	 * @param yelLJSPayGrpBSchema
	 *            LJSPayGrpBSchema：余额财务应收数据
	 * @return boolean：处理成功true，否则false
	 */
	private boolean dealAccBala(LJSPayGrpBSchema yelLJSPayGrpBSchema) {
		LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
		tLCAppAccTraceSchema.setCustomerNo(yelLJSPayGrpBSchema.getAppntNo());
		tLCAppAccTraceSchema.setOtherType("1"); // 团单号
		tLCAppAccTraceSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
		tLCAppAccTraceSchema.setMoney(yelLJSPayGrpBSchema.getSumActuPayMoney());
		tLCAppAccTraceSchema.setOperator(tGI.Operator);

		AppAcc tAppAcc = new AppAcc();
		MMap tMMap = tAppAcc.accTakeOutXQ(tLCAppAccTraceSchema);
		if(tMMap!=null){
			// 这里得到的是引用
			LCAppAccSchema tLCAppAccSchema = (LCAppAccSchema) tMMap.getObjectByObjectName("LCAppAccSchema", 0);
			tLCAppAccTraceSchema = (LCAppAccTraceSchema) tMMap.getObjectByObjectName("LCAppAccTraceSchema", 0);
			// 续期业务未结束，不能直接更新余额
			tLCAppAccSchema.setAccBala(getAccBala(tLCAppAccSchema.getCustomerNo()));
			tLCAppAccSchema.setState("0"); // 续期核销时赋值为有效1
			
			tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala());
			tLCAppAccTraceSchema.setState(tLCAppAccSchema.getState());
			tLCAppAccTraceSchema.setBakNo(yelLJSPayGrpBSchema.getGetNoticeNo());
			mapAcc.put(tLCAppAccTraceSchema, "INSERT");
			mapAcc.put(tLCAppAccSchema, "UPDATE");
		}

		return true;
	}

	// 取得宽限期
	// 输出：int 宽限期
	private int getDay() {
		String showPaymode = "";
		int showDay = 0;
		switch (Integer.parseInt(mLCGrpContSchema.getPayMode())) {
		case 1:
			showPaymode = "现金";
			showDay = 15;
			break;

		case 2:
			showPaymode = "现金支票";
			showDay = 15;
			break;
		case 3:
			showPaymode = "转帐支票";
			showDay = 7;
			break;

		case 4:
			showPaymode = "银行转账";
			showDay = 7;
			break;
		}
		return showDay;
	}

	/**
	 * 加入到催收核销日志表数据
	 * 
	 * @param pmDealState
	 * @param pmReason
	 * @param pmOpreat :
	 *            INSERT,UPDATE,DELETE
	 * @return
	 */
	private boolean dealUrgeLog(String pmDealState, String pmReason, String pmOpreat) {

		LCUrgeVerifyLogSchema tLCUrgeVerifyLogSchema = new LCUrgeVerifyLogSchema();
		// 加到催收核销日志表
		if (pmOpreat.equals("INSERT")) {
			tLCUrgeVerifyLogSchema.setSerialNo(mserNo);
			tLCUrgeVerifyLogSchema.setRiskFlag("1");
			tLCUrgeVerifyLogSchema.setOperateType("1"); // 1：续期催收操作,2：续期核销操作
			tLCUrgeVerifyLogSchema.setOperateFlag("1"); // 1：个案操作,2：批次操作
			tLCUrgeVerifyLogSchema.setOperator(tGI.Operator);
			tLCUrgeVerifyLogSchema.setDealState(pmDealState); // 1 : 正在处理中,2 :
			// 处理错误,3 :
			// 处理已完成

			tLCUrgeVerifyLogSchema.setContNo(mLCGrpContSchema.getGrpContNo());
			tLCUrgeVerifyLogSchema.setMakeDate(CurrentDate);
			tLCUrgeVerifyLogSchema.setMakeTime(CurrentTime);
			tLCUrgeVerifyLogSchema.setModifyDate(CurrentDate);
			tLCUrgeVerifyLogSchema.setModifyTime(CurrentTime);
			String StartDate = tLCUrgeVerifyLogSchema.getMakeDate();
			String StartTime = tLCUrgeVerifyLogSchema.getMakeTime();
			;
			System.out.println("StartDate" + StartDate);
			System.out.println("StartTime" + StartTime);
			tLCUrgeVerifyLogSchema.setErrReason(pmReason);
		} else {
			LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new LCUrgeVerifyLogDB();
			LCUrgeVerifyLogSet tLCUrgeVerifyLogSet = new LCUrgeVerifyLogSet();
			tLCUrgeVerifyLogDB.setSerialNo(mserNo);
			tLCUrgeVerifyLogDB.setOperateType("1");
			tLCUrgeVerifyLogDB.setOperateFlag("1");
			tLCUrgeVerifyLogDB.setRiskFlag("1");
			tLCUrgeVerifyLogSet = tLCUrgeVerifyLogDB.query();
			if (!(tLCUrgeVerifyLogSet == null) && tLCUrgeVerifyLogSet.size() > 0) {
				tLCUrgeVerifyLogSchema = tLCUrgeVerifyLogSet.get(1);
				tLCUrgeVerifyLogSchema.setModifyDate(PubFun.getCurrentDate());
				tLCUrgeVerifyLogSchema.setModifyTime(PubFun.getCurrentTime());
				tLCUrgeVerifyLogSchema.setDealState(pmDealState);
				tLCUrgeVerifyLogSchema.setErrReason(pmReason);
			} else {
				return false;
			}

		}

		MMap tMap = new MMap();
		tMap.put(tLCUrgeVerifyLogSchema, pmOpreat);

		VData tInputData = new VData();
		tInputData.add(tMap);

		PubSubmit tPubSubmit = new PubSubmit();
		if (tPubSubmit.submitData(tInputData, "") == false) {
			this.mErrors.addOneError("PubSubmit:处理LCUrgeVerifyLog 表失败!");
			return false;
		}
		return true;
	}

	/**
	 * 准备打印数据
	 * 
	 * @param tLCPolSchema
	 * @return
	 */
	public LOPRTManagerSchema getPrintData(LCGrpContSchema tLCGrpContSchema, String GetNoticeNo) {
		LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
		try {
			String tLimit = PubFun.getNoLimit(tLCGrpContSchema.getManageCom());
			String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
			tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
			tLOPRTManagerSchema.setOtherNo(tLCGrpContSchema.getGrpContNo());
			tLOPRTManagerSchema.setOtherNoType("01");
			tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_REPAY);
			tLOPRTManagerSchema.setManageCom(tLCGrpContSchema.getManageCom());
			tLOPRTManagerSchema.setAgentCode(tLCGrpContSchema.getAgentCode());
			tLOPRTManagerSchema.setReqCom(tLCGrpContSchema.getManageCom());
			tLOPRTManagerSchema.setReqOperator(tLCGrpContSchema.getOperator());
			tLOPRTManagerSchema.setPrtType("0");
			tLOPRTManagerSchema.setStateFlag("0");
			tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
			tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
			tLOPRTManagerSchema.setStandbyFlag2(GetNoticeNo);
		} catch (Exception ex) {
			return null;
		}
		return tLOPRTManagerSchema;
	}

	// 20081204 zhanggm 同步LJSPayGrpB表的DealState状态。
	// 当保费从LCAppAcc表抵充、SumActuPayMoney==0时,DealState应该为4而不是0,同LJSPayB是一样的
	private void updateDealState() {
		String sql = "update LJSPayGrpB set DealState = '" + mLJSPayBSchema.getDealState() + "' " + "where GetNoticeNo = '" + mLJSPayBSchema.getGetNoticeNo() + "' ";
		System.out.println(sql);
		map.put(sql, "UPDATE");
	}

	private boolean lockGrpCont() {
		MMap tCekMap = null;
		tCekMap = lockBalaCount(mLCGrpContSchema.getGrpContNo());
		if (tCekMap == null) {
			return false;
		}
		map.add(tCekMap);
		return true;

	}

	/**
	 * 锁定动作
	 * 
	 * @param aGrpContNo
	 * @return MMap
	 */
	private MMap lockBalaCount(String aGrpContNo) {
		MMap tMMap = null;
		/** 约定缴费续期催收"XP" */
		String tLockNoType = "XP";
		/** 锁定有效时间（秒） */
		String tAIS = "3600";
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("LockNoKey", aGrpContNo);
		tTransferData.setNameAndValue("LockNoType", tLockNoType);
		tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

		VData tVData = new VData();
		tVData.add(tGI);
		tVData.add(tTransferData);

		LockTableActionBL tLockTableActionBL = new LockTableActionBL();
		tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
		if (tMMap == null) {
			mErrors.copyAllErrors(tLockTableActionBL.mErrors);
			return null;
		}
		return tMMap;
	}

}
