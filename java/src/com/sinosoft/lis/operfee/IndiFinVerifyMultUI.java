

package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Description:
 * 续期续保批量抽档用户接口，接收页面数据
 * </p>

 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class IndiFinVerifyMultUI {

  //业务处理相关变量
  private VData mInputData ;
  public  CErrors mErrors=new CErrors();

  public IndiFinVerifyMultUI() {
  }
  public static void main(String[] args) {

    GlobalInput tGI = new GlobalInput();
    tGI.ComCode="86";
    tGI.Operator="001";
    tGI.ManageCom="86";
    TransferData tTransferData=new TransferData();
    tTransferData.setNameAndValue("PayDate","2006-5-29");

    VData tVData = new VData();
    tVData.add(tGI);
    tVData.add(tTransferData);
    IndiFinVerifyMultUI tIndiFinVerifyMultUI = new IndiFinVerifyMultUI();
    if(tIndiFinVerifyMultUI.submitData(tVData,"VERIFY")==false)
    {
        System.out.println("个单批处理核销失败："+tIndiFinVerifyMultUI.mErrors.getFirstError());
    }
    else
    {
        System.out.println("个单批处理核销成功");
    }

  }

  /**
   * 操作的提交方法，作为页面数据的入口
   * @param cInputData VData
   * @param cOperate String
   * @return boolean
   */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    IndiFinVerifyMultBL tIndiFinVerifyMultBL=new IndiFinVerifyMultBL();
    System.out.println("Start IndiFinVerifyMultBL UI Submit...");
    tIndiFinVerifyMultBL.submitData(mInputData,cOperate);

    System.out.println("End IndiFinVerifyMultBL UI Submit...");

    mInputData=null;
    //如果有需要处理的错误，则返回
    if (tIndiFinVerifyMultBL.mErrors .needDealError() )
       {
       this.mErrors .copyAllErrors(tIndiFinVerifyMultBL.mErrors ) ;
       return false;
       }
System.out.println("error num="+mErrors.getErrorCount());
    return true;
  }

}
