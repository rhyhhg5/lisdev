package com.sinosoft.lis.operfee;



import java.util.Date;

import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.xb.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 接收IndiDueFeeBackUI传入的数据，进行业务逻辑的处理。
 * 通过生成对应实收记录的负费记录方式进行原实收保费的抵消，实现需求的实收保费转出。
 * 1、	生成对应的负费记录，实现转出财务数据LJAPayPerson、LJAPayGrp、LJAPay操作
 * 2、	回退保单数据到续保续保抽档前状态。
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class MOmniYJBackBL
{
    /**错误的容器。*/
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = null;  //完整的操作员信息
    private LCInsureAccBalanceSchema mLCInsureAccBalanceSchema = null;  //本次回退的月结数据
    private LCContSchema mLCContSchema = null;  //本保单信息
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    private Reflections ref = new Reflections();

    private MMap map = new MMap();  //待提交的数据集合

    public MOmniYJBackBL()
    {
    }

    /**
     * 操作的提交方法，进行续期续保实收保费转出业务逻辑处理并提交数据库。
     * 开始时将应收状态更新为正转出5，成功后为转出6，若操作失败则回退为核销成功1
     * @param cInputData VData:
     * A.	GlobalInput对象，完整的登陆用户信息。
     * B.	LJSPayB对象，应收记录备份信息，只需GetNoticeNo即可。
     * @param cOperate String：此为“”
     * @return boolean：boolean, 成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if(!dealSubmit(cInputData, cOperate))
            {
                return false;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 执行回退动作
     * @param cInputData VData：submitData中串入的VData
     * @param cOperate String：“”
     * @return boolean：boolean, 成功true，否则false
     */
    private boolean dealSubmit(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return false;
        }
        
//        if(!checkData())
//        {
//            return false;
//        }

        //进行业务处理
        if(!dealData())
        {
            return false;
        }

        VData d = new VData();
        d.add(map);
        PubSubmit p = new PubSubmit();
        if(!p.submitData(d, ""))
        {
            System.out.println(p.mErrors.getErrContent());
            mErrors.addOneError("提交数据错误");
            return false;
        }

        return true;
    }

    /**
     * 进行业务逻辑处理。
     i.	生成对应的负费记录，实现转出财务数据LJAPayPerson、LJAPay操作
     ii.调用类PRnewContBackBL进行保单数据的回退。
     * @return boolean：操作成功true，否则false
     */
    private boolean dealData()
    {
        if(!backInsuacc())
        {
            return false;
        }
        
        if(!backAccBalance())
        {
            return false;
        }

        return true;
    }
    
    /**
     * 删除月结数据
     * @return boolean
     */
    private boolean backAccBalance()
    {
    	LCInsureAccBalanceDB tLCInsureAccBalanceDB =new LCInsureAccBalanceDB();
    	tLCInsureAccBalanceDB.setSequenceNo( mLCInsureAccBalanceSchema.getSequenceNo());
    	tLCInsureAccBalanceDB.setPolNo(mLCInsureAccBalanceSchema.getPolNo());
    	if (!(tLCInsureAccBalanceDB.getInfo()))
        {
            System.out.println("tLCInsureAccBalanceDB的SequenceNo"+mLCInsureAccBalanceSchema.getSequenceNo());
            mErrors.addOneError("没有查询到续期轨迹表的记录");
            return false;
        }
    	LCInsureAccBalanceSchema tLCInsureAccBalanceSchema=tLCInsureAccBalanceDB.getSchema();
    	map.put(tLCInsureAccBalanceSchema, "DELETE");
    	    	
    	String LCInsureAccSQL="UPDATE LCInsureAcc a SET BalaDate=(select max(duebaladate) from lcinsureaccbalance where contno=a.contno) where ContNo='"+mLCInsureAccBalanceSchema.getContNo()+"'";
		map.put(LCInsureAccSQL,"UPDATE");
		String LCInsureAccFeeSQL="UPDATE LCInsureAccFee a SET BalaDate=(select max(duebaladate) from lcinsureaccbalance where contno=a.contno) where ContNo='"+mLCInsureAccBalanceSchema.getContNo()+"'";
		map.put(LCInsureAccFeeSQL,"UPDATE");
		String LCInsureAccClassFeeSQL="UPDATE LCInsureAccClassFee a SET BalaDate=(select max(duebaladate) from lcinsureaccbalance where contno=a.contno) where ContNo='"+mLCInsureAccBalanceSchema.getContNo()+"'";
		map.put(LCInsureAccClassFeeSQL,"UPDATE");
		String LCInsureAccClassSQL="UPDATE LCInsureAccClass a SET BalaDate=(select max(duebaladate) from lcinsureaccbalance where contno=a.contno) where ContNo='"+mLCInsureAccBalanceSchema.getContNo()+"'";
		map.put(LCInsureAccClassSQL,"UPDATE");
    	
    	return true;
    }
    
    /**
     * 反冲帐户金额
     * @return boolean
     */
    private boolean backInsuacc()
    {
    	//查找出月结管理费的轨迹,作为查找相关表的基础表
    	LCInsureAccTraceSchema aLCInsureAccTraceSchema=new LCInsureAccTraceSchema();
    	//月结进入帐户总金额
    	double sumMoney=Double.parseDouble(new ExeSQL().getOneValue("Select Sum(Money) From LCInsureAccTrace Where ContNo='"+mLCInsureAccBalanceSchema.getContNo()+"' and Otherno='"+mLCInsureAccBalanceSchema.getSequenceNo()+"'"));
    	//月结收费总金额
    	double sumFee=Double.parseDouble(new ExeSQL().getOneValue("Select Sum(Fee) From LCInsureAccFeeTrace Where ContNo='"+mLCInsureAccBalanceSchema.getContNo()+"' and Otherno='"+mLCInsureAccBalanceSchema.getSequenceNo()+"'"));
    	
    	
    	//先对ACC轨迹表进行反冲,创建日期和修改日期都需要重置
    	LCInsureAccTraceDB tLCInsureAccTraceDB = new LCInsureAccTraceDB();
		tLCInsureAccTraceDB.setContNo(mLCInsureAccBalanceSchema.getContNo());
		tLCInsureAccTraceDB.setOtherNo(mLCInsureAccBalanceSchema.getSequenceNo());
		LCInsureAccTraceSet tLCInsureAccTraceSet =tLCInsureAccTraceDB.query(); 
        if (!(tLCInsureAccTraceSet.size()>0))
        {
            System.out.println("LCInsureAccTrace的otherno"+mLCInsureAccBalanceSchema.getSequenceNo());
            mErrors.addOneError("没有查询到续期轨迹表的记录");
            return false;
        }
        for (int i = 1; i <= tLCInsureAccTraceSet.size(); i++) {
        	LCInsureAccTraceSchema tLCInsureAccTraceSchema=tLCInsureAccTraceSet.get(i);
        	if(tLCInsureAccTraceSchema.getMoneyType().equals("RP"))
        	{
        		aLCInsureAccTraceSchema=tLCInsureAccTraceSet.get(i);
        	}
        	tLCInsureAccTraceSchema.setSerialNo(PubFun1.CreateMaxNo("SERIALNO",tLCInsureAccTraceSchema.getManageCom()));
        	tLCInsureAccTraceSchema.setMoney(-tLCInsureAccTraceSchema.getMoney());
        	//20141010区分月结回退的记录与原月结记录的otherno，以便财务接口正常流转
        	tLCInsureAccTraceSchema.setOtherNo(tLCInsureAccTraceSchema.getOtherNo()+"F");
        	tLCInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
        	tLCInsureAccTraceSchema.setMakeDate(mCurrentDate);
            tLCInsureAccTraceSchema.setMakeTime(mCurrentTime);
            tLCInsureAccTraceSchema.setModifyDate(mCurrentDate);
            tLCInsureAccTraceSchema.setModifyTime(mCurrentTime);
            tLCInsureAccTraceSchema.setMoneyNoTax(null);
            tLCInsureAccTraceSchema.setMoneyTax(null);
            tLCInsureAccTraceSchema.setBusiType(null);
            tLCInsureAccTraceSchema.setTaxRate(null); 
    		map.put(tLCInsureAccTraceSchema,"INSERT");
			
		}
        
      //再对ACCFEE轨迹表进行反冲,创建日期和修改日期都需要重置
    	LCInsureAccFeeTraceDB tLCInsureAccFeeTraceDB = new LCInsureAccFeeTraceDB();
		tLCInsureAccFeeTraceDB.setContNo(mLCInsureAccBalanceSchema.getContNo());
		tLCInsureAccFeeTraceDB.setOtherNo(mLCInsureAccBalanceSchema.getSequenceNo());
		LCInsureAccFeeTraceSet tLCInsureAccFeeTraceSet =tLCInsureAccFeeTraceDB.query(); 
        if (!(tLCInsureAccFeeTraceSet.size()>0))
        {
            System.out.println("LCInsureAccFeeTrace的otherno"+mLCInsureAccBalanceSchema.getSequenceNo());
            mErrors.addOneError("没有查询到续期费用轨迹表的记录");
            return false;
        }
        for (int i = 1; i <= tLCInsureAccFeeTraceSet.size(); i++) {
        	LCInsureAccFeeTraceSchema tLCInsureAccFeeTraceSchema=tLCInsureAccFeeTraceSet.get(i);
        	tLCInsureAccFeeTraceSchema.setSerialNo(PubFun1.CreateMaxNo("SERIALNO",tLCInsureAccFeeTraceSchema.getManageCom()));
        	tLCInsureAccFeeTraceSchema.setFee(-tLCInsureAccFeeTraceSchema.getFee());
        	tLCInsureAccFeeTraceSchema.setOtherNo(tLCInsureAccFeeTraceSchema.getOtherNo()+"F");
        	tLCInsureAccFeeTraceSchema.setOperator(mGlobalInput.Operator);
            tLCInsureAccFeeTraceSchema.setMakeDate(mCurrentDate);
            tLCInsureAccFeeTraceSchema.setMakeTime(mCurrentTime);
            tLCInsureAccFeeTraceSchema.setModifyDate(mCurrentDate);
            tLCInsureAccFeeTraceSchema.setModifyTime(mCurrentTime);
            tLCInsureAccFeeTraceSchema.setMoneyNoTax(null);
            tLCInsureAccFeeTraceSchema.setMoneyTax(null);
            tLCInsureAccFeeTraceSchema.setBusiType(null);
            tLCInsureAccFeeTraceSchema.setTaxRate(null);   
    		map.put(tLCInsureAccFeeTraceSchema,"INSERT");
			
		}
        
        //维护帐户总表
        LCInsureAccDB tLCInsureAccDB=new LCInsureAccDB();
        tLCInsureAccDB.setPolNo(aLCInsureAccTraceSchema.getPolNo());
        tLCInsureAccDB.setInsuAccNo(aLCInsureAccTraceSchema.getInsuAccNo());
        if(!tLCInsureAccDB.getInfo())
        {
        	System.out.println("LCInsureAcc的polno"+aLCInsureAccTraceSchema.getPolNo());
            mErrors.addOneError("没有查询到万能帐户的记录");
            return false;
        }
        LCInsureAccSchema tLCInsureAccSchema=tLCInsureAccDB.getSchema();
        tLCInsureAccSchema.setInsuAccBala(tLCInsureAccSchema.getInsuAccBala()-sumMoney);
        tLCInsureAccSchema.setOperator(mGlobalInput.Operator);
        tLCInsureAccSchema.setModifyDate(mCurrentDate);
        tLCInsureAccSchema.setModifyTime(mCurrentTime);
        map.put(tLCInsureAccSchema,"UPDATE");
        
        //维护费用总表
        LCInsureAccFeeDB tLCInsureAccFeeDB=new LCInsureAccFeeDB();
        tLCInsureAccFeeDB.setPolNo(aLCInsureAccTraceSchema.getPolNo());
        tLCInsureAccFeeDB.setInsuAccNo(aLCInsureAccTraceSchema.getInsuAccNo());
        if(!tLCInsureAccFeeDB.getInfo())
        {
        	System.out.println("LCInsureAccFee的polno"+aLCInsureAccTraceSchema.getPolNo());
            mErrors.addOneError("没有查询到万能帐户的记录");
            return false;
        }
        LCInsureAccFeeSchema tLCInsureAccFeeSchema=tLCInsureAccFeeDB.getSchema();
        tLCInsureAccFeeSchema.setFee(tLCInsureAccFeeSchema.getFee()-sumFee);
        tLCInsureAccFeeSchema.setOperator(mGlobalInput.Operator);
        tLCInsureAccFeeSchema.setModifyDate(mCurrentDate);
        tLCInsureAccFeeSchema.setModifyTime(mCurrentTime);
        map.put(tLCInsureAccFeeSchema,"UPDATE");
        
//
//        //再对ACCClass表进行维护
//    	LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
//		tLCInsureAccClassDB.setPolNo(aLCInsureAccTraceSchema.getPolNo());
//		tLCInsureAccClassDB.setInsuAccNo(aLCInsureAccTraceSchema.getInsuAccNo());
////		tLCInsureAccClassDB.setPayPlanCode(aLCInsureAccTraceSchema.getPayPlanCode());
//		LCInsureAccClassSet tLCInsureAccClassSet =tLCInsureAccClassDB.query(); 
//        if (!(tLCInsureAccClassSet.size()>0))
//        {
//            System.out.println("LCInsureAccClass的PolNo"+aLCInsureAccTraceSchema.getPolNo());
//            mErrors.addOneError("没有查询到ACCClass表的记录");
//            return false;
//        }
//        LCInsureAccClassSchema tLCInsureAccClassSchema=tLCInsureAccClassSet.get(1);
//        tLCInsureAccClassSchema.setInsuAccBala(tLCInsureAccClassSchema.getInsuAccBala()-sumMoney);
//        tLCInsureAccClassSchema.setOperator(mGlobalInput.Operator);
//        tLCInsureAccClassSchema.setModifyDate(mCurrentDate);
//        tLCInsureAccClassSchema.setModifyTime(mCurrentTime);
//        map.put(tLCInsureAccClassSchema,"UPDATE");
//        
//        
//        //再对ACCFEEClass表进行维护
//        LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
//		tLCInsureAccClassFeeDB.setPolNo(aLCInsureAccTraceSchema.getPolNo());
//		tLCInsureAccClassFeeDB.setInsuAccNo(aLCInsureAccTraceSchema.getInsuAccNo());
//		tLCInsureAccClassFeeDB.setPayPlanCode(aLCInsureAccTraceSchema.getPayPlanCode());
//		LCInsureAccClassFeeSet tLCInsureAccClassFeeSet =tLCInsureAccClassFeeDB.query(); 
//        if (!(tLCInsureAccClassFeeSet.size()>0))
//        {
//            System.out.println("LCInsureAccClassFee的PolNo"+aLCInsureAccTraceSchema.getPolNo());
//            mErrors.addOneError("没有查询到ACCClassFee表的记录");
//            return false;
//        }
//        LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema=tLCInsureAccClassFeeSet.get(1);
//        tLCInsureAccClassFeeSchema.setFee(tLCInsureAccClassFeeSchema.getFee()-sumFee);
//        tLCInsureAccClassFeeSchema.setOperator(mGlobalInput.Operator);
//        tLCInsureAccClassFeeSchema.setModifyDate(mCurrentDate);
//        tLCInsureAccClassFeeSchema.setModifyTime(mCurrentTime);
//        map.put(tLCInsureAccClassFeeSchema,"UPDATE");
        
//    	add wdl 2013-4-16 对于万能附加险含有231001，即万能险和231001一起卖的。此时会有两个payplancode，造成，查找accclass表时会报错
//    	更改为对含有多个payplancode的险种，就要取其主险的payplancode来用。兼容万能附加重疾的情况
//      逻辑：若只有一个账户表，可以只走万能主险的payplancode，若有多个，可以分开走或者报错
        //再对ACCClass表进行维护
    	LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
		tLCInsureAccClassDB.setPolNo(aLCInsureAccTraceSchema.getPolNo());
		tLCInsureAccClassDB.setInsuAccNo(aLCInsureAccTraceSchema.getInsuAccNo());
//		tLCInsureAccClassDB.setPayPlanCode(aLCInsureAccTraceSchema.getPayPlanCode());
		LCInsureAccClassSet tLCInsureAccClassSet =tLCInsureAccClassDB.query(); 
		if(tLCInsureAccClassSet.size()==1){
			LCInsureAccClassSchema tLCInsureAccClassSchema=tLCInsureAccClassSet.get(1);
	        tLCInsureAccClassSchema.setInsuAccBala(tLCInsureAccClassSchema.getInsuAccBala()-sumMoney);
	        tLCInsureAccClassSchema.setOperator(mGlobalInput.Operator);
	        tLCInsureAccClassSchema.setModifyDate(mCurrentDate);
	        tLCInsureAccClassSchema.setModifyTime(mCurrentTime);
	        map.put(tLCInsureAccClassSchema,"UPDATE");
		}else {
			return false;
		}
        if (!(tLCInsureAccClassSet.size()>0))
        {
            System.out.println("LCInsureAccClass的PolNo"+aLCInsureAccTraceSchema.getPolNo());
            mErrors.addOneError("没有查询到ACCClass表的记录");
            return false;
        }     
        
        //再对ACCFEEClass表进行维护
        LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
		tLCInsureAccClassFeeDB.setPolNo(aLCInsureAccTraceSchema.getPolNo());
		tLCInsureAccClassFeeDB.setInsuAccNo(aLCInsureAccTraceSchema.getInsuAccNo());
//		tLCInsureAccClassFeeDB.setPayPlanCode(aLCInsureAccTraceSchema.getPayPlanCode());
		LCInsureAccClassFeeSet tLCInsureAccClassFeeSet =tLCInsureAccClassFeeDB.query(); 
		if(tLCInsureAccClassFeeSet.size()==1){
			LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema=tLCInsureAccClassFeeSet.get(1);
	        tLCInsureAccClassFeeSchema.setFee(tLCInsureAccClassFeeSchema.getFee()-sumFee);
	        tLCInsureAccClassFeeSchema.setOperator(mGlobalInput.Operator);
	        tLCInsureAccClassFeeSchema.setModifyDate(mCurrentDate);
	        tLCInsureAccClassFeeSchema.setModifyTime(mCurrentTime);
	        map.put(tLCInsureAccClassFeeSchema,"UPDATE");
		}else {
			return false;
		}
        if (!(tLCInsureAccClassFeeSet.size()>0))
        {
            System.out.println("LCInsureAccClassFee的PolNo"+aLCInsureAccTraceSchema.getPolNo());
            mErrors.addOneError("没有查询到ACCClassFee表的记录");
            return false;
        }        


        return true;
    }


    /**
     * 得到待转出的实收信息
     * @return boolean：操作成功true，否则false
     */
    private boolean queryContInfo()
    {

        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLCInsureAccBalanceSchema.getContNo());
        tLCContDB.getInfo();
        mLCContSchema = tLCContDB.getSchema();

        return true;
    }

    /**
     * 校验是否可进行实收保费转出操作，校验细节间数据流图“校验部分”。
     * @return boolean，校验通过true，否则false
     */
    private boolean checkData()
    {
        if(!queryContInfo())
        {
            return false;
        }
        
        if(!checkDueFee())
        {
            return false;
        }
        if(!checkClaim())
        {
            return false;
        }
        if(!checkBQ())
        {
            return false;
        }
        
        if(!checkPolState())
        {
            return false;
        }
        if(!checkSixMonth())
        {
        	return false;
        }
        return true;
    }
   
    /**
     * 校验是否月结的实收保费转出在6个月之内
     * @return boolean
     */
    private boolean checkSixMonth()
    {
    	String  sql = "select max(confdate) from ljapayperson where contno='"+mLCInsureAccBalanceSchema.getContNo()+"'  "
                    + "and paytypeflag='0' and paytype<>'YEL' group by getnoticeno having sum(sumduepaymoney)<>0 "
                    + "order by max(confdate) desc "
                    + "with ur";
    	String result=new ExeSQL().getOneValue(sql);
    	System.out.println(result);
    	if((result!=null)&&(!result.equals("null"))&&(!result.equals("")))
    	{
	    	int day=PubFun.calInterval(result, mCurrentDate, "D");
	    	if(day>180)
	    	{
	    		mErrors.addOneError("续期已超过6个月，不能回退。");
	    		return false;
	    	}
	    	
    	}
    	else
    	{
    		mErrors.addOneError("该单没有续期，不能回退。");
    		return false;
    	}

        
        return true;
    }
    
    /**
     * 校验保单状态
     * @return boolean
     */
    private boolean checkPolState()
    {
        String sql = " select 1 from LCContState a "
                     + " where contNo = '" + mLCInsureAccBalanceSchema.getContNo() + "' "
                     + "   and startDate < '" + mCurrentDate + "' "
                     + "   and ( endDate is null or endDate > '"
                     + mCurrentDate + "') "
                     + " and state = '1' "
                     ;

        String result = new ExeSQL().getOneValue(sql);
        if(!result.equals(""))
        {
            mErrors.addOneError("当前保单或其险种处于失效或终止状态，不能回退。");
            return false;
        }
        return true;
    }
    

    /**
     * 校验保全
     * @return boolean
     */
    private boolean checkBQ()
    {
        //是否有未处理完毕的保全业务
        String sql = " select b.edorNo from LPEdorApp a, LPEdorItem b "
                     + "where a.edorAcceptNo = b.edorAcceptNo "
                     + "   and b.contNo = '" + mLCInsureAccBalanceSchema.getContNo() + "' "
                     + "   and a.edorState != '" + BQ.EDORSTATE_CONFIRM + "' ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if(tSSRS.getMaxRow() != 0)
        {
            String edorNos = "";
            for(int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                edorNos += tSSRS.GetText(i, 1) + ", ";
            }
            mErrors.addOneError("保单有未处理完毕的全业务，不能回退，受理号为："
                                + edorNos);
            return false;
        }

      //是否有个单迁移
         sql = " select b.edorNo from LPEdorApp a, LPEdorItem b "
                     + "where a.edorAcceptNo = b.edorAcceptNo "
                     + "   and b.contNo = '" + mLCInsureAccBalanceSchema.getContNo() + "' "
                     + "   and b.edortype='PR' "
                     + "   and a.confdate > '" + mLCInsureAccBalanceSchema.getMakeDate() + "' " ;
        System.out.println(sql);
        String resultPR = new ExeSQL().getOneValue(sql);
        if(!(resultPR.equals("")))
        {
        	mErrors.addOneError("保单月结后做过个单迁移,不能回退,工单号为" + resultPR);
            return false;
        }

        //续期续保核销后发生过收退费的项目
        sql = " select sum(getmoney) from LJAGetEndorse "
              + "where contNo = '" + mLCInsureAccBalanceSchema.getContNo() + "' "
              + "   and makeDate > '" + mLCInsureAccBalanceSchema.getMakeDate() + "' " 
              + "   and feeoperationtype not in (select Code from LDCode where CodeType = 'edortypechangeprem') ";
        String result = new ExeSQL().getOneValue(sql);
        if(!(result.equals("0.00")|result.equals("")))
        {
            mErrors.addOneError("保单有过对保费有影响的保全业务，不能回退。");
            return false;
        }

        return true;
    }

    /**
     * 校验报单续期续保后是否发生过理赔
     * @return boolean: 发生true
     */
    private boolean checkClaim()
    {
        String sql = " select 1 from LLClaimDetail a "
                     + "where a.contNo = '"
                     + mLCInsureAccBalanceSchema.getContNo() + "' "
                     + "   and a.makeDate >  (select max(lastPayToDate) from "
                     +" ljspaypersonb where contno = a.contno and dealstate ='1') ";
        String result = new ExeSQL().getOneValue(sql);
        if(!result.equals(""))
        {
            mErrors.addOneError("保单正在做理赔业务或发生过结案的理赔，不能回退。");
            return false;
        }

        return true;
    }


    /**
     * 校验报单是否正在做续期续保
     * @return boolean
     */
    private boolean checkDueFee()
    {

        String sql = " select getNoticeNo from ljspay "
              + "where OtherNo = '" + mLCInsureAccBalanceSchema.getContNo() + "' "
              + "  and othernotype = '2'";
        String result = new ExeSQL().getOneValue(sql);
        if(!result.equals(""))
        {
            mErrors.addOneError("保单正在做续期续保业务，不能回退，应收记录号" + result);
            return false;
        }
        sql = "  select 1 from LCRnewStateLog "
              + "where contNo = '" + mLCInsureAccBalanceSchema.getContNo() + "' "
              + "   and state in('" +  XBConst.RNEWSTATE_APP + "', '"
              + XBConst.RNEWSTATE_UNUW + "', '" + XBConst.RNEWSTATE_UNHASTEN
              + "')";
        result = new ExeSQL().getOneValue(sql);
        if(!result.equals(""))
        {
            mErrors.addOneError("保单正在做续期续保业务，不能回退。");
            return false;
        }

        return true;

    }


    /**
     * 从输入数据中得到所有对象
     * @param inputData VData: submitData中传入的VData
     * @return boolean: 操作成功true，否则false
     */
    private boolean getInputData(VData inputData)
    {
        mGlobalInput = (GlobalInput) inputData
                       .getObjectByObjectName("GlobalInput", 0);
        mLCInsureAccBalanceSchema = (LCInsureAccBalanceSchema) inputData
                       .getObjectByObjectName("LCInsureAccBalanceSchema", 0);
        LCInsureAccBalanceDB tLCInsureAccBalance=new LCInsureAccBalanceDB();
        tLCInsureAccBalance.setSequenceNo(mLCInsureAccBalanceSchema.getSequenceNo());
        tLCInsureAccBalance.setContNo(mLCInsureAccBalanceSchema.getContNo());
        mLCInsureAccBalanceSchema=tLCInsureAccBalance.query().get(1);
        
        if(mGlobalInput == null || mLCInsureAccBalanceSchema == null
           || mLCInsureAccBalanceSchema.getSequenceNo() == null
            || mLCInsureAccBalanceSchema.getSequenceNo().equals(""))
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }
        return true;
    }

    public static void main(String[] args)
    {
        GlobalInput g = new GlobalInput();
        g.Operator = "xpbak";
        g.ComCode = "86";

//        LCInsureAccBalanceSet set = new LCInsureAccBalanceDB().executeQuery("select * from lcinsureaccbalance where contno='013933883000001' and balacount>60 order by balacount desc with ur");
        LCInsureAccBalanceSet set = new LCInsureAccBalanceDB().executeQuery("select * from lcinsureaccbalance where contno='013933582000001' and balacount>12 order by balacount desc with ur");

        for (int i = 1; i <= set.size(); i++) {
            System.out.println(set.get(i).getBalaCount());
        	LCInsureAccBalanceSchema tLCInsureAccBalanceSchema =set.get(i); 

            VData d = new VData();
            d.add(tLCInsureAccBalanceSchema);
            d.add(g);

            MOmniYJBackBL bl = new MOmniYJBackBL();
            if(!bl.submitData(d, ""))
            {
                System.out.println(bl.mErrors.getErrContent());
            }
            else
            {
                System.out.println("OK");
            }
		}
    }
}
