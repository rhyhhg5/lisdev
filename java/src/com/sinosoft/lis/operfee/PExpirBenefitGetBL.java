package com.sinosoft.lis.operfee;

import com.sinosoft.lis.schema.LJSGetSchema;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LCContStateSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJSGetDrawSet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.vschema.LMDutyGetAliveSet;
import java.util.*;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.schema.LJSGetDrawSchema;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.db.LCContStateDB;
import com.sinosoft.lis.db.LMDutyGetAliveDB;
import com.sinosoft.lis.db.LJSGetDrawDB;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCContStateSchema;
import com.sinosoft.lis.schema.LMDutyGetAliveSchema;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCGetDB;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.utility.SysConst;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author fuxin
 * @version 1.0
 */
public class PExpirBenefitGetBL {

    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData saveData = new VData();
    private MMap mMMap = new MMap();
    private GlobalInput tGI = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    /** 数据操作字符串 */
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String mserNo = "1"; //批次号
    private String mSerGrpNo = ""; //接受外部参数

    private String mGetNoticeNo = null; //通知书号

    private String payMode = "Q"; //判断前台点击的是现金给付，还是普通给付。
    //保单缴费方式
    private String mBankCode = "";
    private String mBankAccNo = "";
    private String mAccName = "";
    private String mPrem  = "" ;
    private int mMyYears = 0;
    private boolean ChangTimeFlag  = false ;
    
    private String StartDate = ""; //应付开始时间
    private String EndDate = ""; //应付结束时间

    private Reflections ref = new Reflections();

    //个人保单表
    private LCContSchema mLCContSchema = new LCContSchema();

    //应付个人交费表
    private LJSGetSchema tLJSGetSchema = new LJSGetSchema();
    //应付个人明细表
    private LJSGetDrawSet tLJSGetDrawSet = new LJSGetDrawSet();
    
    private LJSGetDrawSchema tLJSGetDrawSchema = null;
    //实付表
    private LJAGetSchema mLJAGetSchema = new LJAGetSchema();

	private ExeSQL mExeSQL = new ExeSQL();

    private String mPrtSeqNo = null;
    
    private String mBirthday = null;

    private String anniversary = null ; //抽档当年对应的保单周年日

    public PExpirBenefitGetBL() {
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public boolean submitData(VData cInputData) {

        LJSGetDrawSet data = getSubmitData(cInputData);
        if (data == null) {
            return false;
        }
        return true;
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public LJSGetDrawSet getSubmitData(VData cInputData) {

        if (!getInputData(cInputData)) {
            return null;
        }
        System.out.println("After ExpirBenefitGetBL getInputData");
        if (!checkData()) {
            return null;
        }
        System.out.println("After ExpirBenefitGetBL checkData");
        //进行业务处理
        if (!dealData()) {
            return null;
        }

        System.out.println("After ExpirBenefitGetBL dealData");
        return tLJSGetDrawSet;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) {
//        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mLCContSchema = ((LCContSchema) mInputData.getObjectByObjectName(
                "LCContSchema",
                0));


        if(mLCContSchema == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitGetBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLCContSchema.getContNo());
        if (tLCContDB.getInfo() == false) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "PExpirBenefitGetBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;

        }
        mLCContSchema.setSchema(tLCContDB.getSchema());
        return true;
    }

    /**
     *  数据校验
     */
    private boolean checkData() 
    {
    	//校验保全
        if (!checkBQ())
        {
            return false ;
        }
        
        //校验理赔
        if (!checkLP())
        {
            return false ;
        }
        
        //校验保单是否冻结，冻结的保单不允许给付
        if (!checkCF())
        {
            return false ;
        }
        return true;
    }

    /**
     * 业务处理
     */
    private boolean dealData() 
    {
//20090907 zhanggm 防止并发加锁
    	MMap tCekMap = null;
    	tCekMap = lockLJAGet(mLCContSchema);
        if (tCekMap == null)
        {
            return false;
        }
        mMMap.add(tCekMap);
//--------------------------

        //满期金给付
        if(!doBenefit()){
            return false ;
        }

        return true;
    }

    /**
     * 如果保单存在CT,XT,BF,CC 不允许做个险给付。
     * @return boolean
     */
    private boolean checkBQ()
    {
        //校验保全项目
        String sql = "select a.edorNo "
                     + "from LPEdorItem a, LPEdorApp b "
                     + "where a.edorAcceptNo = b.edorAcceptNo "
                     + "   and a.ContNo = '" + mLCContSchema.getContNo() + "' "
                     + "   and edorType in ('CT','XT','BF','CC','WT') "
                     + "   and b.edorState != '" + BQ.EDORSTATE_CONFIRM
                     + "' ";
        String edorNo = new ExeSQL().getOneValue(sql);
        if (!edorNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitGetBL";
            tError.functionName = "checkBQ";
            tError.errorMessage = "保单" + mLCContSchema.getContNo() + "正在做保全业务：" +
                                  edorNo + "，不能给付抽档";
            mErrors.addOneError(tError);
            return false;
        }
        //如果是万能险，有未完成的保全项目则不能做满期给付抽档
        if(CommonBL.hasULIRisk(mLCContSchema.getContNo()))
        {
        	sql = "select a.edorNo,(select min(edorname) from lmedoritem where edorcode = a.edortype), "
     	        + "codename ('appedorstate',b.edorstate) from LPEdorItem a, LPEdorApp b "
                + "where a.edorAcceptNo = b.edorAcceptNo "
                + "and a.ContNo = '" + mLCContSchema.getContNo() + "' "
                + "and a.edortype <>'HA' "//分红险满期金领取
                + "and b.edorState != '" + BQ.EDORSTATE_CONFIRM + "' ";
        	SSRS tSSRS = new SSRS();
            tSSRS = new ExeSQL().execSQL(sql);
            if(tSSRS.getMaxRow()!=0)
            {
            	edorNo = tSSRS.GetText(1, 1);
                String edorType = tSSRS.GetText(1, 2);
                String edorState = tSSRS.GetText(1, 3);
                if (edorNo != null && !edorNo.equals("")) 
                {
                    CError tError = new CError();
                    tError.moduleName = "ULIBenefitGetBL";
                    tError.functionName = "checkBQ";
                    tError.errorMessage = "原因是，正在做保全业务：" 
                                        + edorType + "，工单号：" + edorNo + "，状态：" 
                                        + edorState + "。";
                    mErrors.addOneError(tError);
                    return false;
                }
            }
        }
        return true ;
    }

    /**
     * 校验理赔
     * @return boolean
     */
    private boolean checkLP()
    {
        //正在做理赔不允许做满期给付
       String sql = "select 1 from llcase where customerno in (select insuredno from lcinsured where contno ='" +
              mLCContSchema.getContNo() + "')"
              + " and endcasedate is null and rgtstate not in('11','12','14') "  //11- 通知 12-给付 14-扯件
              ;
        String rgtNo = new ExeSQL().getOneValue(sql);
        if (rgtNo != null && !rgtNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitGetBL";
            tError.functionName = "checkLP";
            tError.errorMessage = "保单有未结案的理陪。";
            mErrors.addOneError(tError);
            return false;
        }
        //理赔已经赔付，确认是否是主线给付，如果主险做了赔付不允许做满期给付。
        String sqlcliam = "select polno From ljagetclaim where contno ='"+mLCContSchema.getContNo()+"'";

        //得到给付责任的险种
        String sqlPolNo =" select distinct polno from lcget a where contno ='"+mLCContSchema.getContNo()+"'"
                        +" and exists ( "
                        +" select 1 From LMDutyGetAlive b, LMDutyGetRela c where a.GetDutyCode = b.GetDutyCode "
                        +" and a.dutycode = c.dutycode) "
                        +" with ur "
                        ;

        String dutyPolNo = new ExeSQL().getOneValue(sqlPolNo);
        SSRS tSSRS = new SSRS();
        tSSRS = new ExeSQL().execSQL(sqlcliam);
        for ( int i = 1; i <= tSSRS.getMaxRow(); i ++ )
        {
            String tPolNo = tSSRS.GetText(1,i);
            if (tPolNo.equals(dutyPolNo))
            {
                CError tError = new CError();
                tError.moduleName = "ExpirBenefitGetBL";
                tError.functionName = "checkLP";
                tError.errorMessage = "保单满期责任已经有过理赔。";
                mErrors.addOneError(tError);
                return false ;
            }
        }
        return true ;
    }

    private boolean doBenefit() {
    	
    	if(CommonBL.hasBonusRisk(mLCContSchema.getContNo()) && getBonusMJGet(mLCContSchema.getContNo())){
    		
            if(!bonusMJDeal(mLCContSchema.getContNo())){
            	return false;
            }
    		return true;
    	}

        //得到被保人需要给付的险种
//        String startDate = (String) mTransferData.getValueByName("StartDate");
//        String endDate = CurrentDate;
        //取得被保人信息
        String sql = "select distinct insuredno from lcget a,lmdutygetalive b "
            + " where a.getdutycode = b.getdutycode and contno ='"
            + mLCContSchema.getContNo() + "' and a.gettodate<='"+CurrentDate+"'"
//          + " and ((a.getdutykind ='0' and  a.gettodate<=a.getenddate"
            + " and ((a.getdutykind ='0' and (a.gettodate<=a.getenddate or a.getenddate is null)" 
            +" and not exists (select 1 from ljsgetdraw where contno=a.contno and getdutycode=a.getdutycode and insuredno=a.insuredno)"
            +		") or( a.getdutykind !='0' and" 
            + " ((a.gettodate<a.getenddate) or ((select max(curgettodate) from ljsgetdraw where contno=a.contno and getdutycode=a.getdutycode and insuredno=a.insuredno)<=a.getenddate))))"
            +" fetch first 1 rows only";
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        HashMap t = new HashMap();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {


        	
            LCGetSet tLCGetSet = getLCGetNeedGet(CurrentDate,mLCContSchema.getContNo(),tSSRS.GetText(i, 1));

            if (tLCGetSet == null) {
                continue;
            }

            //得到生日
            String birthdaySQL  =
                "select birthday from lcinsured where contno ='"
                + mLCContSchema.getContNo() + "' and insuredno='" +
                tSSRS.GetText(i, 1) + "'";
             
             mBirthday = new ExeSQL().getOneValue(birthdaySQL);
            


            //20090907 zhanggm 万能老年关爱给付需要传入账户余额
            sql = "select insuaccbala from lcinsureacc where contno ='" + mLCContSchema.getContNo() 
                + "' and insuredno='" + tSSRS.GetText(i, 1) + "'";
            

            double sumGetMoney = 0; //总领取金额
            //循环每个给付责任进行处理
            tLJSGetDrawSet.clear();
            for (int j = 1; j <= tLCGetSet.size(); j++) {

            	//20141011以免多个给付责任同时处理时增加基本要素混乱
            	Calculator tCalculator = new Calculator();
            	tCalculator.addBasicFactor("AccMoney", new ExeSQL().getOneValue(sql));
            	
                //获取合同下的每条给付的明细信息
                LCGetSchema tLCGetSchema = tLCGetSet.get(j).getSchema();
                
                //提前60天抽档时被保人的年龄取领至日期当天的年龄
                String tAppntAge = String.valueOf(PubFun.calInterval(mBirthday,PubFun.getCurrentDate(), "Y"));
                Date curDate=CommonBL.stringToDate(PubFun.getCurrentDate());
                Date getDate=CommonBL.stringToDate(tLCGetSchema.getGettoDate());
                Date mDate=PubFun.calDate(curDate, 60, "D", null);
                if(curDate.before(getDate) && !mDate.before(getDate)){
                	tAppntAge=String.valueOf(PubFun.calInterval(mBirthday,tLCGetSchema.getGettoDate(), "Y"));
                }
                tCalculator.addBasicFactor("AppntAge", tAppntAge);
            	tCalculator.addBasicFactor("Age", tAppntAge);
                
                LMDutyGetAliveDB tLMDutyGetaLiveDB = new LMDutyGetAliveDB();

                tLMDutyGetaLiveDB.setGetDutyCode(tLCGetSchema.getGetDutyCode());
                LMDutyGetAliveSet tLMDutyGetAliveSet = tLMDutyGetaLiveDB.query();
                LMDutyGetAliveSchema tLMDutyGetAliveSchema = tLMDutyGetAliveSet.get(1);
                String pCalCode = "";
                if (tLMDutyGetAliveSchema.getCalCode() != null &&
                    !"".equals(tLMDutyGetAliveSchema.getCalCode())) {
                    pCalCode = tLMDutyGetAliveSchema.getCalCode();
                }
                if (tLMDutyGetAliveSchema.getCnterCalCode() != null &&
                    !"".equals(tLMDutyGetAliveSchema.getCnterCalCode())) {
                    pCalCode = tLMDutyGetAliveSchema.getCnterCalCode();
                }
                if (tLMDutyGetAliveSchema.getOthCalCode() != null &&
                    !"".equals(tLMDutyGetAliveSchema.getOthCalCode())) {
                    pCalCode = tLMDutyGetAliveSchema.getOthCalCode();
                }

                tCalculator.setCalCode(pCalCode);

                //责任保额
                String amnt = "select sum(amnt) from lcduty where polno='"
                              + tLCGetSchema.getPolNo() + "' and dutycode='"
                              + tLCGetSchema.getDutyCode() + "'"
                              ;


                if (tLMDutyGetAliveSchema.getGetDutyKind().equals("12"))
                {
                    String kindsql =" select d.* From lmdutygetclm a, lmriskduty b ,lmdutygetrela c ,llclaimdetail d "
                                   +" where a.getdutycode = c.getdutycode and b.dutycode = c.dutycode "
                                   +" and d.getdutycode = c.getdutycode "
                                   +" and d.polno ='"+tLCGetSchema.getPolNo()+"' "
                                   +" and afterget ='003' "
                                   ;
                    SSRS kindSSRS = new ExeSQL().execSQL(kindsql);
                    if (kindSSRS.getMaxRow()>0)
                    {
                        CError tError = new CError();
                        tError.moduleName = "ExpirBenefitGetBL";
                        tError.functionName = "getInputData";
                        tError.errorMessage = "责任已经理赔，不能做满期给付。";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }

                //判断主险是否出险,满期给付责任在主险。
                if (getPolAttribute(tLCGetSchema.getPolNo())
                        && tLMDutyGetAliveSchema.getDiscntFlag().equals("0"))
                {
                    //得到该险种的附加险种
                   String annexPolNoSQL = "select code From ldcode1 where code1 =(select riskcode from lcpol where polno ='"+tLCGetSchema.getPolNo()+"')  and codetype='checkappendrisk' " ;
                   String annexRisk = new ExeSQL().getOneValue(annexPolNoSQL);
                   String annexPolNo ="select polno from lcpol where contno='"+tLCGetSchema.getContNo()+"' and riskcode ='"+annexRisk+"'";
                   if (annexRisk.equals("") && annexRisk =="")
                   {
                       CError tError = new CError();
                       tError.moduleName = "ExpirBenefitGetBL";
                       tError.functionName = "getInputData";
                       tError.errorMessage = "没有得到附加险信息。";
                       this.mErrors.addOneError(tError);
                   }
                   //1."如果附加的有LP就不做给付";
                   String sqlcliam = "select polNo From ljagetclaim where polno ='"+new ExeSQL().getOneValue(annexPolNo)+"'";
                   String cliam = new ExeSQL().getOneValue(sqlcliam);
                   if ( !cliam.equals("") && cliam !="" && cliam !="null" )
                   {
                       CError tError = new CError();
                       tError.moduleName = "ExpirBenefitGetBL";
                       tError.functionName = "getInputData";
                       tError.errorMessage = "附加险已经出险，不能做满期给付。";
                       this.mErrors.addOneError(tError);
                       return false ;
                   }
                   //2."主险出现了并且是死亡责任的不做给付"
                   String mainSQL = "select polNo From ljagetclaim where polno ='"+tLCGetSchema.getPolNo()+"'"
                                  + " and getdutykind in('501','502','503','504') " ;
                   String main = new ExeSQL().getOneValue(mainSQL);
                   if (!main.equals("") && main !="null")
                   {
                       CError tError = new CError();
                       tError.moduleName = "ExpirBenefitGetBL";
                       tError.functionName = "getInputData";
                       tError.errorMessage = "主险责任出现理赔，并且是死亡责任，不能满期给付。";
                       this.mErrors.addOneError(tError);
                       return false;
                   }
                  //3.如果满期金方式的责任出险，不做给付。
                  if (tLMDutyGetAliveSchema.getGetDutyKind().equals("0"))
                  {
                      String SQL = "select polNo From ljagetclaim where polno ='"+tLCGetSchema.getPolNo()+"'";
                      String polSLQ = new ExeSQL().getOneValue(SQL);
                      if (!polSLQ.equals("") && polSLQ !="null")
                      {
                          CError tError = new CError();
                          tError.moduleName = "ExpirBenefitGetBL";
                          tError.functionName = "getInputData";
                          tError.errorMessage = "满期责任已经出险，不做给付。";
                          this.mErrors.addOneError(tError);
                          return false;
                      }
                  }
                  //给付合计保费。
                  String polSQL = " select code,code1 From ldcode1 where  code1 in(select riskcode from lcpol where polno ='" +tLCGetSchema.getPolNo() +"')  and codetype='checkappendrisk' "
                                  + " union "
                                  + " select code,code1 From ldcode1 where  code  in(select riskcode from lcpol where polno ='" +tLCGetSchema.getPolNo() +"')  and codetype='checkappendrisk' "
                                  ;
                  SSRS xSSRS = new ExeSQL().execSQL(polSQL);
                  mPrem = new ExeSQL().getOneValue("select sum(prem) From lcpol where InsuredNo='" +tLCGetSchema.getInsuredNo() + "' and riskcode in('" +xSSRS.GetText(1, 1) + "','" + xSSRS.GetText(1, 2) +"') and contno ='" + tLCGetSchema.getContNo() + "'");
                }
                //满期责任在附加险，如果主险出险责任终止，不做满期给付。
                if (tLMDutyGetAliveSchema.getDiscntFlag().equals("3")
                      && !getPolAttribute(tLCGetSchema.getPolNo()))
                {
                    //得到该责任对应的主险，查看是否有LP，如果有理赔就不做满期给付。
                    //
                    String SQL =" select c.polno From lcpol c,ljagetclaim d where c.riskcode in( "
                               +" select code1 From ldcode1  where code in( "
                               +" select distinct riskcode From lcpol a,lcget b where a.polno = b.polno "
                               +" and a.polno ='"+tLCGetSchema.getPolNo()+"')"
                               +" and code1 = c.riskcode  and codetype='checkappendrisk' ) "
                               +" and c.contno ='"+tLCGetSchema.getContNo()+"'"
                               +" and c.polno = d.polno "
                               +" union "
                               +" select polno from ljagetclaim where polno = '"+tLCGetSchema.getPolNo()+"'"
                               +" with ur "
                               ;
                   tSSRS = new ExeSQL().execSQL(SQL);
                   if (tSSRS.getMaxRow() != 0)
                   {
                       CError tError = new CError();
                       tError.moduleName = "ExpirBenefitGetBL";
                       tError.functionName = "getInputData";
                       tError.errorMessage = "主险责任已经出现，不做满期给付。";
                       this.mErrors.addOneError(tError);
                       return false;
                   }
                   String polSQL = " select code,code1 From ldcode1 where  code1 in(select riskcode from lcpol where polno ='"+tLCGetSchema.getPolNo()+"')  and codetype='checkappendrisk' "
                                   + " union "
                                   + " select code,code1 From ldcode1 where  code  in(select riskcode from lcpol where polno ='"+tLCGetSchema.getPolNo()+"')  and codetype='checkappendrisk' "
                                   ;
                   SSRS xSSRS= new ExeSQL().execSQL(polSQL);
                   mPrem = new ExeSQL().getOneValue("select sum(prem) From lcpol where InsuredNo='"+tLCGetSchema.getInsuredNo()+"' and riskcode in('"+xSSRS.GetText(1,1)+"','"+xSSRS.GetText(1,2)+"') and contno ='"+tLCGetSchema.getContNo()+"'");
                }
                //附加险出险，赔合计保费。
                if (tLMDutyGetAliveSchema.getDiscntFlag().equals("1"))
                {
                    //如果主险出险，不做满期给付。
                    if (getPolAttribute(tLCGetSchema.getPolNo())) {
                        String SQL  = " select * From lcpol c,ljagetclaim d where c.riskcode in( "
                                    + " select code1 From ldcode1  where code in( "
                                    +" select distinct riskcode From lcpol a,lcget b where a.polno = b.polno "
                                    + " and a.polno ='" + tLCGetSchema.getPolNo() +"')"
                                    + " and code1 = c.riskcode  and codetype='checkappendrisk' ) "
                                    + " and c.contno ='" +tLCGetSchema.getContNo() + "'"
                                    + " and c.polno = d.polno with ur "
                                ;
                        tSSRS = new ExeSQL().execSQL(SQL);

                        if (tSSRS.getMaxRow() != 0) {
                            CError tError = new CError();
                            tError.moduleName = "ExpirBenefitGetBL";
                            tError.functionName = "getInputData";
                            tError.errorMessage = "主险责任已经出现，不做满期给付。";
                            this.mErrors.addOneError(tError);
                            return false;
                        }

                    }
                    //得到责任对应的，主副险险种。
                     String polSQL = " select code,code1 From ldcode1 where  code1 in(select riskcode from lcpol where polno ='"+tLCGetSchema.getPolNo()+"')  and codetype='checkappendrisk' "
                                   + " union "
                                   + " select code,code1 From ldcode1 where  code  in(select riskcode from lcpol where polno ='"+tLCGetSchema.getPolNo()+"')  and codetype='checkappendrisk' "
                                   ;
                     SSRS xSSRS= new ExeSQL().execSQL(polSQL);
                     mPrem = new ExeSQL().getOneValue("select sum(prem) From lcpol where InsuredNo='"+tLCGetSchema.getInsuredNo()+"' and riskcode in('"+xSSRS.GetText(1,1)+"','"+xSSRS.GetText(1,2)+"') and contno ='"+tLCGetSchema.getContNo()+"'");
                }

                //附加险出现，赔付主险保费。
                if (tLMDutyGetAliveSchema.getDiscntFlag().equals("2"))
                {
                    //如果主险出险，不做满期给付。
                    if (getPolAttribute(tLCGetSchema.getPolNo()))
                    {
                        String sqlcliam = "select polno From ljagetclaim where polno ='"+tLCGetSchema.getPolNo()+"'";
                        tSSRS = new ExeSQL().execSQL(sqlcliam);
                        if (tSSRS.getMaxRow() != 0)
                        {
                            CError tError = new CError();
                            tError.moduleName = "ExpirBenefitGetBL";
                            tError.functionName = "getInputData";
                            tError.errorMessage = "主险责任已经出现，不做满期给付。";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                    }
                    //如果附加险，出险给付主险保费
                    else if (!getPolAttribute(tLCGetSchema.getPolNo()))
                    {
                        String sqlcliam = "select polno From ljagetclaim where polno ='"+tLCGetSchema.getPolNo()+"'";
                        tSSRS = new ExeSQL().execSQL(sqlcliam);
                        if (tSSRS.getMaxRow() != 0)
                        {
                            String SQL = " select  a.polno,prem From lcget a,lcpol b ,lmriskapp c where a.polno  = b.polno "
                                         + " and c.subriskflag ='M' "
                                         + " and b.riskcode = c.riskcode "
                                         + " and a.polno ='" + tLCGetSchema.getPolNo() + "' "
                                         + " group by a.polno,prem "
                                         ;
                            tSSRS = new ExeSQL().execSQL(SQL);
                            mPrem = tSSRS.GetText(1,2);
                        }
                    }
                    //否则给付合计保费
                    else
                    {
                        //得到责任对应的，主副险险种。
                        String polSQL = " select code,code1 From ldcode1 where  code1 in(select riskcode from lcpol where polno ='"
                                        +tLCGetSchema.getPolNo() +"')  and codetype='checkappendrisk' "
                                        + " union "
                                        + " select code,code1 From ldcode1 where  code  in(select riskcode from lcpol where polno ='"
                                        + tLCGetSchema.getPolNo() +"')  and codetype='checkappendrisk' "
                                        ;
                        SSRS xSSRS = new ExeSQL().execSQL(polSQL);
                        mPrem = new ExeSQL().getOneValue("select sum(prem) From lcpol where InsuredNo='" +tLCGetSchema.getInsuredNo() +"' and riskcode in('" + xSSRS.GetText(1, 1) +"','" + xSSRS.GetText(1, 2) +"') and contno ='" + tLCGetSchema.getContNo() +"'");
                    }
                }
                //主险出险，不赔付主附险保费、保额



                tCalculator.addBasicFactor("Amnt",new ExeSQL().getOneValue(amnt));
                tCalculator.addBasicFactor("Get",CommonBL.bigDoubleToCommonString(tLCGetSchema.getActuGet(), "0.00"));
                tCalculator.addBasicFactor("PolNo", tLCGetSchema.getPolNo());
                tCalculator.addBasicFactor("Prem",mPrem);

                String tStr = tCalculator.calculate();
                double tValue = 0;

                if (tStr == null || tStr.trim().equals("")) {
                    tValue = 0;
                } else {
                    tValue = Double.parseDouble(tStr);
                }
                sumGetMoney += tValue;

                //全无忧产品
                if (tLMDutyGetAliveSchema.getDiscntFlag().equals("9")) {
                    
                	//modify by xp 090515 解决第一次抽档getdate也会设置当年保单周年日的问题.
                	String NewGetDate=new ExeSQL().getOneValue("select getdate+1 year from ljsgetdraw where dutycode='"+tLCGetSchema.getDutyCode()+"' and contno ='"+tLCGetSchema.getContNo()+"' order by getdate desc fetch first 1 row only with ur");
                	if (NewGetDate.equals("")||NewGetDate=="")//第一次抽档
                	{                	
                		anniversary = new ExeSQL().getOneValue("select  (case when substr(varchar(a.cvalidate), 5, 6) > substr(varchar(a.insuredbirthday), 5, 6)	then substr(varchar(a.insuredbirthday + 60 year), 1, 4) ||substr(varchar(a.cvalidate), 5, 6) else substr(varchar(a.insuredbirthday + 61 year), 1, 4)||substr(varchar(a.cvalidate), 5, 6) end) from lcpol a,lcget b , lmdutygetalive c where a.conttype ='1' and a.appflag ='1' and a.polno = b.polno and b.getdutycode = c.getdutycode and a.insuredno = b.insuredno and c.DiscntFlag='9'  and a.contno ='"+tLCGetSchema.getContNo()+"'" );
                	}
                	else
                	{
                		anniversary=NewGetDate;
                	}
                    
                    String abs = new ExeSQL().getOneValue(
                            "select sum(getmoney) from ljsgetdraw where ContNo ='" +
                            tLCGetSchema.getContNo() + "' and insuredno ='" +
                            tLCGetSchema.getInsuredNo() + "'");
                    double XQMoney = 0;
                    if (abs.equals("")||abs=="")
                    {
                        XQMoney = 0 ;
                    }else{
                        XQMoney = Double.parseDouble(abs);
                    }

                    double LPMoney = 0; //理赔部分尚未开发。
                    if (XQMoney + LPMoney + sumGetMoney > 2 * Double.parseDouble(new ExeSQL().getOneValue(amnt))) {
                        CError tError = new CError();
                        tError.moduleName = "ExpirBenefitGetBL";
                        tError.functionName = "getInputData";
                        tError.errorMessage = "长期护理保险金和老年护理保险金的累计给付总额不能超过保险金额的2倍，给付抽档失败。";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }

                tLJSGetDrawSchema=new LJSGetDrawSchema();

                tLJSGetDrawSchema.setPolNo(tLCGetSchema.getPolNo());
                tLJSGetDrawSchema.setDutyCode(tLCGetSchema.getDutyCode());
                tLJSGetDrawSchema.setGetDutyKind(tLCGetSchema.getGetDutyKind());
                tLJSGetDrawSchema.setGetDutyCode(tLCGetSchema.getGetDutyCode());
                tLJSGetDrawSchema.setContNo(tLCGetSchema.getContNo());
                tLJSGetDrawSchema.setAppntNo(mLCContSchema.getAppntNo());
                tLJSGetDrawSchema.setInsuredNo(tLCGetSchema.getInsuredNo());
                if (tLMDutyGetAliveSchema.getDiscntFlag().equals("9")) {
                    tLJSGetDrawSchema.setGetDate(anniversary);
                }else
                {
                    tLJSGetDrawSchema.setGetDate(CurrentDate);
                }
                tLJSGetDrawSchema.setGetMoney(tValue);
                tLJSGetDrawSchema.setEnterAccDate(CurrentDate);
                tLJSGetDrawSchema.setFeeOperationType("EB");
                tLJSGetDrawSchema.setFeeFinaType("TF");
                tLJSGetDrawSchema.setManageCom(mLCContSchema.getManageCom());
                tLJSGetDrawSchema.setAgentCom(mLCContSchema.getAgentCom());
                tLJSGetDrawSchema.setAgentType(mLCContSchema.getAgentType());
                tLJSGetDrawSchema.setAgentCode(mLCContSchema.getAgentCode());
                tLJSGetDrawSchema.setAgentGroup(mLCContSchema.getAgentGroup());
                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setPolNo(tLCGetSchema.getPolNo());
                tLCPolDB.getInfo();
                tLJSGetDrawSchema.setRiskCode(tLCPolDB.getRiskCode());
                if (tLMDutyGetAliveSchema.getGetDutyKind().equals("0")) {
                    tLJSGetDrawSchema.setLastGettoDate(tLCPolDB.getEndDate());
                    tLJSGetDrawSchema.setCurGetToDate(tLCPolDB.getEndDate());
                }
                else if (!tLMDutyGetAliveSchema.getGetDutyKind().equals("0")) {
                    LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
                    String query =
                            " Select max(CurGetToDate) from ljsgetdraw a,ljsget b where "
                            +
                            " a.getnoticeno = b.getnoticeno and b.dealstate <>'2' and a.polno='"
                            + tLCGetSchema.getPolNo() + "' and getdutycode ='"
                            + tLCGetSchema.getGetDutyCode() +
                            "' and insuredno ='"
                            + tLCGetSchema.getInsuredNo() + "'";
                    SSRS getToDateSSRS = new ExeSQL().execSQL(query);
                    if ((!getToDateSSRS.GetText(1, 1).equals("") &&
                         getToDateSSRS.GetText(1, 1) != null) &&
                        getToDateSSRS.getMaxRow() > 0) {
                    	

                        tLJSGetDrawSchema.setLastGettoDate(getToDateSSRS.GetText(1, 1));
                        tLJSGetDrawSchema.setCurGetToDate(PubFun.calDate(getToDateSSRS.GetText(1, 1),Integer.parseInt(tLMDutyGetAliveSchema.getGetDutyKind()), "M", null));
//                    	}
                    } else { //第一次给付
                    //咨询产品StartDateCalRef为S则为按投保日期进行计算,对应GetStartUnit为Y;
                    //StartDateCalRef为B则为按被保人出生日期进行计算,对应GetStartUnit为A;
                        if (((tLMDutyGetAliveSchema.getStartDateCalRef().equals("S"))
                        			&&(tLMDutyGetAliveSchema.getGetStartUnit().equals("Y")))||((tLMDutyGetAliveSchema.getStartDateCalRef().equals("S"))
                                			&&(tLMDutyGetAliveSchema.getGetStartUnit().equals("A")))){
                        	mMyYears = PubFun.calInterval(mBirthday,tLCGetSchema.getGettoDate(), "Y");
                        	if((!tLMDutyGetAliveSchema.getGetDutyKind().equals("0")))
                        	{

                        			tLJSGetDrawSchema.setLastGettoDate(tLCGetSchema.getGettoDate());
                                    tLJSGetDrawSchema.setCurGetToDate(PubFun.
                                            calDate(
                                                    tLCGetSchema.getGettoDate(),
                                                    Integer.parseInt(tLMDutyGetAliveSchema.getGetDutyKind()), "M", null));
//                        		}
                        	}
                        	else
                        	{
                        	
                                tLJSGetDrawSchema.setLastGettoDate(PubFun.
                                        calDate(
                                                tLCPolDB.getCValiDate(),
                                                tLMDutyGetAliveSchema.
                                                getGetStartPeriod(),
                                                tLMDutyGetAliveSchema.
                                                getGetStartUnit(), null));
                                tLJSGetDrawSchema.setCurGetToDate(PubFun.
                                        calDate(
                                                tLCPolDB.getCValiDate(),
                                                tLMDutyGetAliveSchema.
                                                getGetStartPeriod(),
                                                tLMDutyGetAliveSchema.
                                                getGetStartUnit(), null));
                        	}

                        } else if ((tLMDutyGetAliveSchema.getStartDateCalRef().equals("B")) 
                        		&&(tLMDutyGetAliveSchema.getGetStartUnit().equals("A"))) {
                                tLJSGetDrawSchema.setLastGettoDate(tLCGetSchema.getGettoDate());
                                System.out.println("LastGettoDate:"+PubFun.calDate(tLCPolDB.getInsuredBirthday(),tLMDutyGetAliveSchema.getGetStartPeriod(), "Y", null));
                                tLJSGetDrawSchema.setCurGetToDate(PubFun.
                                        calDate(
                                                tLCGetSchema.getGettoDate(),
                                                Integer.parseInt(tLMDutyGetAliveSchema.getGetDutyKind()), "M", null));
                                System.out.println("CurGetToDate:"+PubFun.calDate(tLCGetSchema.getGettoDate(),Integer.parseInt(tLMDutyGetAliveSchema.getGetDutyKind()), "Y", null));
                            
                        }
                    }
                }
                //万能险老年关爱给付日期取的是被保人70周岁对应的保单周年日和抽档当天的最大值
                if(CommonBL.isULIRisk(tLCPolDB.getRiskCode()))
                {
                	StringBuffer sql1 = new StringBuffer();
                	sql1.append("select case when a.insuredbirthday+70 years <= ")
                	    .append("a.cvalidate + (year(a.insuredbirthday+70 years) - year(a.cvalidate)) years ")
                	    .append("then a.cvalidate + (year(a.insuredbirthday+70 years) - year(a.cvalidate)) years ")
                	    .append("else a.cvalidate + (year(a.insuredbirthday+71 years) - year(a.cvalidate)) years end ")
                        .append("from lcpol a where polno = '").append(tLCPolDB.getPolNo()).append("' with ur");
                	System.out.println(sql1.toString());
                	String tStrDate = new ExeSQL().getOneValue(sql1.toString());
                	
                	Date tCurGetToDate = CommonBL.stringToDate(tStrDate);
                	Date tCurrentDate = CommonBL.stringToDate(CurrentDate);
                	
                	if(tCurGetToDate.before(tCurrentDate))
                	{
                		tCurGetToDate = tCurrentDate;
                	}
                	
                	tLJSGetDrawSchema.setLastGettoDate(tCurGetToDate);
                	tLJSGetDrawSchema.setCurGetToDate(tCurGetToDate);
                	tLJSGetDrawSchema.setGetDate(tCurGetToDate);
                }
                		
                tLJSGetDrawSchema.setSerialNo(mserNo);
                tLJSGetDrawSchema.setOperator(tGI.Operator);
                tLJSGetDrawSchema.setMakeDate(CurrentDate);
                tLJSGetDrawSchema.setMakeTime(CurrentTime);
                tLJSGetDrawSchema.setModifyDate(CurrentDate);
                tLJSGetDrawSchema.setModifyTime(CurrentTime);
                this.tLJSGetDrawSet.add(tLJSGetDrawSchema);

//              计算其它奖金
                LDCodeDB tLDCodeDB = new LDCodeDB();
                tLDCodeDB.setCodeType("RiskGetAlive");
                tLDCodeDB.setCode(tLCPolDB.getRiskCode());
                if (tLDCodeDB.getInfo()) 
                {
                    if (!t.containsKey(tLCPolDB.getPolNo())) 
                    {
                        t.put(tLCPolDB.getPolNo(), "");
                        if(tLDCodeDB.getCodeAlias()==null || tLDCodeDB.getCodeAlias().equals(""))//特殊险种特殊算
                        {
                        	if(tLCPolDB.getRiskCode().equals("330501"))//常无忧B
                        	{
//                        		默认情况，公式三，不需要“息降我不降”
                        		String tFormula = "C";
                        		String tRateFlag = "N";
                        		
                        		String tManageCom = mLCContSchema.getManageCom();
                        		String tAgentCom = mLCContSchema.getAgentCom();
                        		
                        		/*	
                        		 * 功能 #568 常无忧B五年期保单满期给付程序支持
                        		 * 添加对于3年期和5年期分开处理的流程，保持原有3年期流程不变。 
                        		 * add by xp 20120619
                        		 * */
                        		System.out.println("保单的保障区间为:"+tLCPolDB.getInsuYear());
                        		if(tLCPolDB.getInsuYear()==3){
	                        		if(tAgentCom==null || tAgentCom.equals(""))
	                        		{
	                        			tAgentCom = "P";
	                        			System.out.println("不让代理点为空……避免上海代理点为空的保单走公式三。正确情况是：上海代理点为空的保单走公式三（息降我不降）。");
	                        		}
                        		
	                        		if(tAgentCom==null || tAgentCom.equals(""))
	                        		{
	                        			System.out.println("代理点为空，走默认情况。");
	                        		}
	                        		else
	                        		{
	                        			String sql1 = "select FormuCode, RateFlag,length(trim(Agentcom)) from Formu330501 where ManageCom like '" + tManageCom + "%' and UseFlag = 'Y' "
	                        						+ " and StartDate <= '" + mLCContSchema.getCValiDate() + "' and EndDate >= '" + mLCContSchema.getCValiDate() 
	                		            			+ "' and agentcom = substr('" + tAgentCom + "', 1, (case when length(trim(Agentcom)) >= length('" 
	                		            			+ tAgentCom + "') then length('" + tAgentCom + "') else  length(trim(Agentcom)) end)) order by 3 desc ";
	                        			SSRS tSSRS1 = new SSRS();
	                            		tSSRS1 = new ExeSQL().execSQL(sql1);
	                            		
	                            		if(tSSRS1.getMaxRow()>0)
	                            		{
	                                		tFormula = tSSRS1.GetText(1, 1);
	                                    	tRateFlag = tSSRS1.GetText(1, 2);
	                            		}
	                            		else 
	                            		{
	                            			sql1 = "select FormuCode, RateFlag,length(trim(Agentcom)) from Formu330501 where ManageCom like '" + tManageCom + "%' and UseFlag = 'Y' "
	                            			     + " and StartDate <= '" + mLCContSchema.getCValiDate() + "' and EndDate >= '" + mLCContSchema.getCValiDate()  
	                            			     + "' and AgentCom = '000000' order by 3 desc";
	                            			SSRS tSSRS2 = new SSRS();
	                                		tSSRS2 = new ExeSQL().execSQL(sql1);
	                                		if(tSSRS2.getMaxRow()>0)
	                                		{
	                                    		tFormula = tSSRS2.GetText(1, 1);
	                                        	tRateFlag = tSSRS2.GetText(1, 2);
	                                		}
	                            		}
	                        		}
                        		}
                        		else if (tLCPolDB.getInsuYear()==5){
//                        			5年期的常无忧B处理
                        			if(tAgentCom==null || tAgentCom.equals(""))
	                        		{
	                        			tAgentCom = "P";
	                        			System.out.println("不让代理点为空……避免出现代理点为空的保单走标准的公式三。");
	                        		}
                        			String sql1 = "select FormuCode, RateFlag,length(trim(Agentcom)) from Formu330501_year where ManageCom like '" + tManageCom + "%' and UseFlag = 'Y' "
                        						+ " and insuyear="+tLCPolDB.getInsuYear()+" and StartDate <= '" + mLCContSchema.getCValiDate() + "' and EndDate >= '" + mLCContSchema.getCValiDate() 
                		            			+ "' and agentcom = substr('" + tAgentCom + "', 1, (case when length(trim(Agentcom)) >= length('" 
                		            			+ tAgentCom + "') then length('" + tAgentCom + "') else  length(trim(Agentcom)) end)) order by 3 desc ";
                        			SSRS tSSRS1 = new SSRS();
                            		tSSRS1 = new ExeSQL().execSQL(sql1);
                            		
                            		if(tSSRS1.getMaxRow()>0)
                            		{
                                		tFormula = tSSRS1.GetText(1, 1);
                                    	tRateFlag = tSSRS1.GetText(1, 2);
                            		}
                            		else 
                            		{
                            			sql1 = "select FormuCode, RateFlag,length(trim(Agentcom)) from Formu330501_year where ManageCom like '" + tManageCom + "%' and UseFlag = 'Y' "
                            			     + " and insuyear="+tLCPolDB.getInsuYear()+" and StartDate <= '" + mLCContSchema.getCValiDate() + "' and EndDate >= '" + mLCContSchema.getCValiDate()  
                            			     + "' and AgentCom = '000000' order by 3 desc";
                            			SSRS tSSRS2 = new SSRS();
                                		tSSRS2 = new ExeSQL().execSQL(sql1);
                                		if(tSSRS2.getMaxRow()>0)
                                		{
                                    		tFormula = tSSRS2.GetText(1, 1);
                                        	tRateFlag = tSSRS2.GetText(1, 2);
                                		}
                            		}
                        		}
                        		else if (tLCPolDB.getInsuYear()==10){
//                        			10年期的常无忧B处理
                        		}else {
                        			// @@错误处理
									System.out.println("ExpirBenefitGetBL+doBenefit++--");
									CError tError = new CError();
									tError.moduleName = "ExpirBenefitGetBL";
									tError.functionName = "doBenefit";
									tError.errorMessage = "保单的保障期间查询失败!";
									mErrors.addOneError(tError);
									return false;
                        		}
                        		
                        		System.out.println("常无忧B满期给付计算,保障期限为"+tLCPolDB.getInsuYear()+"年,启用公式"+tFormula+",息降我不降标志为"+tRateFlag);
                        		
                        		Cal330501Bonus tCal330501Bonus = new Cal330501Bonus(tLCPolDB.getSchema());

                        		HashMap tHashMap = tCal330501Bonus.getBonus(tFormula,tRateFlag); 
                        		if (tCal330501Bonus.getError() == null) 
                        		{
                        			String initMoney = (String)tHashMap.get("InitMoney"); //初始忠诚奖
                        			String addMoney = (String)tHashMap.get("AddMoney"); //外加忠诚奖
                        			
                        			LJSGetDrawSchema initLJSGetDrawSchema = new LJSGetDrawSchema();
                        			initLJSGetDrawSchema.setSchema(tLJSGetDrawSchema);
                        			initLJSGetDrawSchema.setDutyCode("000000");
                        			initLJSGetDrawSchema.setGetMoney(initMoney);
                                    this.tLJSGetDrawSet.add(initLJSGetDrawSchema);
                                    sumGetMoney += Double.parseDouble(initMoney);
                                    
                                    LJSGetDrawSchema addLJSGetDrawSchema = new LJSGetDrawSchema();
                                    addLJSGetDrawSchema.setSchema(tLJSGetDrawSchema);
                                    addLJSGetDrawSchema.setDutyCode("000001");
                                    addLJSGetDrawSchema.setGetMoney(addMoney);
                                    this.tLJSGetDrawSet.add(addLJSGetDrawSchema);
                                    sumGetMoney += Double.parseDouble(addMoney);
                        		}
                        	}
                        }
                        else
                        {
                        	try {
                                Class tClass = Class.forName(tLDCodeDB.getCodeAlias());
                                CalOther tCalOther = (CalOther) tClass.newInstance();
                                double otherMoney = tCalOther.cal(tLCPolDB.getPolNo());
                                if (tCalOther.getError() == null) {
                                    LJSGetDrawSchema ttLJSGetDrawSchema = new
                                            LJSGetDrawSchema();
                                    ttLJSGetDrawSchema.setSchema(tLJSGetDrawSchema);
                                    ttLJSGetDrawSchema.setDutyCode("000000");
                                    ttLJSGetDrawSchema.setGetMoney(otherMoney);
                                    this.tLJSGetDrawSet.add(ttLJSGetDrawSchema);
                                    sumGetMoney += otherMoney;
                                } else {
                                    mErrors.copyAllErrors(tCalOther.getError());
                                    return false;
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                CError.buildErr(this,
                                                tLCPolDB.getRiskCode() + "奖金给付问题");
                                return false;
                            }
                        }
                    }
                }
     
                //如果不是满期金，将GettoDate在此顺延到下一个给付日期。
                if(!tLCGetSchema.getGetDutyKind().equals("0"))
                {
                    LCGetDB xLCGetDB = new LCGetDB();
                    LCGetSchema xLCGetSchema = new LCGetSchema();
                    xLCGetDB.setPolNo(tLCGetSchema.getPolNo());
                    xLCGetDB.setGetDutyCode(tLCGetSchema.getGetDutyCode());
                    xLCGetDB.setDutyCode(tLCGetSchema.getDutyCode());
                    xLCGetDB.getInfo();
                    xLCGetSchema = xLCGetDB.getSchema();

                    String newGetToDate = " select gettodate +"+xLCGetDB.getGetDutyKind()+" month From lcget where polno ='"+xLCGetDB.getPolNo()+"'"
                                        + " and getDutyCode = '"+xLCGetDB.getGetDutyCode()+"'";
                    xLCGetSchema.setGettoDate(new ExeSQL().getOneValue(newGetToDate).trim()); //得到下次给付日期
//                    }
                    xLCGetSchema.setModifyDate(PubFun.getCurrentDate());
                    xLCGetSchema.setModifyTime(PubFun.getCurrentTime());
                }
                
                
            }



        }
        return true;
    }


    public LCGetSet getLCGetNeedGet(String endDate,String contNo, String insuredNo) {
        String tSBql = " select b.* from lcpol a,lcget b , lmdutygetalive c  where 1=1 "
                        + " and a.polno = b.polno and b.getdutycode = c.getdutycode   "
                        + " and a.contno='" + contNo + "' "
                        + " and a.appflag='1' "
                        + " and a.riskcode not in ('320106','120706') "
                        + " and ((a.paytodate>=a.payenddate) or ( a.paytodate<a.payenddate and b.gettodate<= a.paytodate))  "
                        + " and not exists( select 1 from ljsgetdraw where a.contno= b.contno and getdutykind ='0' and a.polno = polno ) "
//                      + " and ((b.getdutykind ='0' and  b.gettodate<=b.getenddate) or( b.getdutykind !='0' and"
                        + " and ((b.getdutykind ='0' and  (b.gettodate<=b.getenddate or b.getenddate is null)) or( b.getdutykind !='0' and"
//                        + " ((b.gettodate<b.getenddate) or ((select max(Curgettodate) from ljsgetdraw where contno=b.contno and getdutycode=b.getdutycode and insuredno=b.insuredno)<=b.getenddate))))"
                        + " b.gettodate<=b.getenddate )) "//add by hhw 美满今生getenddate<gettodate时，不给付健康护理保险金。。
                        + " and b.gettodate <= '"+endDate+"'"
                        + " and b.insuredno='"+insuredNo+"'"
                        + " with ur ";

        LCGetSet tLCGetSet = new LCGetSet();
        LCGetDB tLCGetDB = new LCGetDB();
        tLCGetSet = tLCGetDB.executeQuery(tSBql);
        System.out.println(tSBql.toString());

        if (tLCGetSet.size() == 0) {
            CError.buildErr(this, "保单号为：" + contNo + "的保单已被其他业务员抽档。");
            return null;
        }
        return tLCGetSet;
    }
    /**
     * 判断给付责任的险种是主险还是附加险
     * 主险返回: true;
     * 附加返回：false;
     * @param polNo String
     * @return boolean
     */

    private boolean getPolAttribute(String polNo)
    {
        //不知道当时怎么想得用了一个那么不好懂的判断。现在直接取lmriskapp描述，判断是主线还是附加险。
        String polSQL = " select subriskflag from lcpol a,lmriskapp b where  a.riskcode = b.riskcode and a.polno ='"+polNo+"'"
                      ;
        SSRS tSSRS = new ExeSQL().execSQL(polSQL);

        if (tSSRS.getMaxRow()==0)
        {
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitGetBL";
            tError.functionName = "getPolAttribute";
            tError.errorMessage = "获得主险信息描述失败。";
            this.mErrors.addOneError(tError);
            return false;
        }
        // M-- 主险
        // S-- 附加险
        if (tSSRS.GetText(1,1).equals("M"))
        {
            return true ;
        }
        else{
            return false ;
        }
    }
    
    /**
     * 校验保单是否冻结，冻结的保单不允许给付
     * @return boolean
     */
    private boolean checkCF()
    {
    	String sql = "select hanguptype,hangupno from LCContHangUpState where contno = '" 
    		       + mLCContSchema.getContNo() + "' and state = '" + BQ.HANGUSTATE_ON 
    		       + "' order by makedate,maketime desc with ur";
    	SSRS tSSRS = new SSRS();
        tSSRS = new ExeSQL().execSQL(sql);
        if(tSSRS.getMaxRow()!=0)
        {
        	String hanguptype = tSSRS.GetText(1, 1);
            String hangupno = tSSRS.GetText(1, 2);
            CError tError = new CError();
            tError.moduleName = "ULIBenefitGetBL";
            tError.functionName = "checkCF";
            if (hanguptype.equals(BQ.HANGUPTYPE_BQ)) 
            {
            	tError.errorMessage = "原因是，该保单为冻结状态，相关保全工单号：" + hangupno + "。";
            }
            else
            {
            	tError.errorMessage = "原因是，该保单为冻结状态。";
            }
            mErrors.addOneError(tError);
            return false;
        }
        return true ;
    }
    
    /**
     * 锁定动作
     * @param cLCContSchema
     * @return
     */
    private MMap lockLJAGet(LCContSchema cLCContSchema)
    {
        MMap tMMap = null;
        /**满期给付锁定标志"MJ"*/
        String tLockNoType = "MJ";
        /**锁定时间*/
        String tAIS = "3600";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", cLCContSchema.getContNo());
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }
        return tMMap;
    }
    
    private boolean setStateFlag()
    {
    	String tContNo = mLCContSchema.getContNo();
    	if(CommonBL.hasULIRisk(tContNo))
    	{
    		LCPolSet tLCPolSet = new LCPolSet();
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setContNo(tContNo);
            tLCPolSet = tLCPolDB.query();
            MMap tMap = new MMap();
            for(int i=1;i<=tLCPolSet.size();i++)
            {
            	tMap.add(getPolState(tLCPolSet.get(i)));
            }
            if(!BQ.STATE_FLAG_TERMINATE.equals(mLCContSchema.getStateFlag()))
    		{
    			String updatecont = "update LCCont Set StateFlag = '"
    		       	     + BQ.STATE_FLAG_TERMINATE + "', ModifyDate = '" + CurrentDate
    			         + "', ModifyTime = '" + CurrentTime + "', Operator = '" + tGI.Operator
    			         + "' where ContNo = '" + tContNo + "' ";
    			tMap.put(updatecont, SysConst.UPDATE);
    			LCContStateSchema tLCContStateSchema = new LCContStateSchema();
    			tLCContStateSchema.setContNo(tContNo);
                tLCContStateSchema.setPolNo(BQ.FILLDATA);
                tLCContStateSchema.setGrpContNo(BQ.FILLDATA);
                tLCContStateSchema.setInsuredNo(mLCContSchema.getInsuredNo());
                tLCContStateSchema.setStateType("Terminate");
                tLCContStateSchema.setOtherNoType("MJ");
                tLCContStateSchema.setOtherNo(mGetNoticeNo);
                tLCContStateSchema.setState("1");
                tLCContStateSchema.setStateReason("MJ");
                tLCContStateSchema.setStartDate(CurrentDate);
                tLCContStateSchema.setEndDate("");
                tLCContStateSchema.setRemark("给付抽档同时保单终止");
                tLCContStateSchema.setOperator(tGI.Operator);
                tLCContStateSchema.setMakeDate(CurrentDate);
                tLCContStateSchema.setMakeTime(CurrentTime);
                tLCContStateSchema.setModifyDate(CurrentDate);
                tLCContStateSchema.setModifyTime(CurrentTime);
                tMap.put(tLCContStateSchema, SysConst.DELETE_AND_INSERT);
//              准备满期终止通知书打印管理表数据
                String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
                String serNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
                LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
                tLOPRTManagerSchema.setPrtSeq(serNo);
                tLOPRTManagerSchema.setOtherNo(tContNo);
                tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_CONT); //合同号
                tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_PEdorTerminate); //满期终止通知书
                tLOPRTManagerSchema.setManageCom(mLCContSchema.getManageCom());
                tLOPRTManagerSchema.setAgentCode(mLCContSchema.getAgentCode());
                tLOPRTManagerSchema.setReqCom(mLCContSchema.getManageCom());
                tLOPRTManagerSchema.setReqOperator(tGI.Operator);
                tLOPRTManagerSchema.setPrtType(PrintManagerBL.PT_FRONT ); //前台打印
                tLOPRTManagerSchema.setStateFlag("0"); //提交打印
                tLOPRTManagerSchema.setStandbyFlag1(CurrentDate);  
                tLOPRTManagerSchema.setStandbyFlag2(mGetNoticeNo);
                tLOPRTManagerSchema.setMakeDate(CurrentDate);
                tLOPRTManagerSchema.setMakeTime(CurrentTime);
//                tMap.put(tLOPRTManagerSchema, SysConst.DELETE_AND_INSERT);
    		}
//            mMMap.add(tMap);
		}
    	return true;
    }

	private MMap getPolState(LCPolSchema aLCPolSchema) 
	{
		MMap tMap = new MMap();
		if(!BQ.STATE_FLAG_TERMINATE.equals(aLCPolSchema.getStateFlag()))
		{
			String updatepol = "update LCPol Set StateFlag = '"
		       	     + BQ.STATE_FLAG_TERMINATE + "', ModifyDate = '" + CurrentDate
			         + "', ModifyTime = '" + CurrentTime + "', Operator = '" + tGI.Operator
			         + "' where Polno = '" + aLCPolSchema.getPolNo() + "'";
			tMap.put(updatepol, SysConst.UPDATE);
			LCContStateSchema tLCContStateSchema = new LCContStateSchema();
			tLCContStateSchema.setContNo(aLCPolSchema.getContNo());
            tLCContStateSchema.setPolNo(aLCPolSchema.getPolNo());
            tLCContStateSchema.setGrpContNo(BQ.FILLDATA);
            tLCContStateSchema.setInsuredNo(aLCPolSchema.getInsuredNo());
            tLCContStateSchema.setStateType("Terminate");
            tLCContStateSchema.setOtherNoType("MJ");
            tLCContStateSchema.setOtherNo(mGetNoticeNo);
            tLCContStateSchema.setState("1");
            tLCContStateSchema.setStateReason("MJ");
            tLCContStateSchema.setStartDate(CurrentDate);
            tLCContStateSchema.setEndDate("");
            tLCContStateSchema.setRemark("给付抽档同时险种终止");
            tLCContStateSchema.setOperator(tGI.Operator);
            tLCContStateSchema.setMakeDate(CurrentDate);
            tLCContStateSchema.setMakeTime(CurrentTime);
            tLCContStateSchema.setModifyDate(CurrentDate);
            tLCContStateSchema.setModifyTime(CurrentTime);
//            tMap.put(tLCContStateSchema, SysConst.DELETE_AND_INSERT);
//            return tMap;
		}
		return null;
	}
	
	private boolean bonusMJDeal(String ContNo){
        String sql = " select distinct a.contno from LCPol a "
  			 + " where a.conttype='1' "
               + " and a.appflag='1' "
               + " and a.stateflag in ( '1','3')"
               + " and a.riskcode in (select riskcode from lmriskapp where risktype4='2') " //分红险
               + " and a.polstate <>'03060002'"
               + " and a.payenddate<=a.paytodate"
               + " and a.enddate <='" + CurrentDate + "' "
               + " and a.ContNo = '" +ContNo+ "'"
               + " and not exists (select 1 from lpedoritem p,lpedorapp q where p.edorno=q.edoracceptno" 
               + "                and p.contno=a.contno and p.edortype='HA' and q.edorstate='"+BQ.EDORSTATE_CONFIRM+"') "
               + " order by a.ContNo with ur";
           System.out.println(sql);
           SSRS tSSRS = new SSRS();
           tSSRS = new ExeSQL().execSQL(sql);
           if(tSSRS.getMaxRow()==0)
           {
        	  return false;
           }
//		LJSGetDrawSet getLJSGetDrawSet=new LJSGetDrawSet();
         
		// 通过LCGET表计算保单未领取的满期金
		String tsql = "select distinct insuredno from lcget a,lmdutygetalive b "
				+ " where a.getdutycode = b.getdutycode and contno ='"+ mLCContSchema.getContNo()+"'"
				+ " and a.gettodate<='"+ CurrentDate+ "'"
				+ " and ((a.getdutykind ='0' and (a.gettodate<=a.getenddate or a.getenddate is null)"
				+ " and not exists (select 1 from ljsgetdraw where contno=a.contno and getdutycode=a.getdutycode and insuredno=a.insuredno)))"
				+ " with ur";
		 tSSRS = new ExeSQL().execSQL(tsql);
		for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
			String insuredNo = tSSRS.GetText(i, 1);
			LCGetSet tLCGetSet = getLCGetBonusGet(mLCContSchema.getContNo(),
					insuredNo);

			String strPol = "select * from lcpol where  contno='"
					+ mLCContSchema.getContNo()
					+ "' and insuredno='"
					+ insuredNo
					+ "' and riskcode in (select riskcode from lmriskapp where risktype4='2')";
			LCPolSet getLCPolSet = new LCPolDB().executeQuery(strPol);
			LCPolSchema getLCPolSchema = getLCPolSet.get(1);
			double sumGetMoney = 0; // 总领取金额
			tLJSGetDrawSet.clear();
			for (int j = 1; j <= tLCGetSet.size(); j++) {
				// 获取合同下的每条给付的明细信息
				LCGetSchema tLCGetSchema = tLCGetSet.get(j).getSchema();

				String birthdaySQL = "select birthday from lcinsured where contno ='"
						+ mLCContSchema.getContNo()
						+ "'"
						+ " and insuredno='"
						+ insuredNo + "' ";
				String mBirthday = mExeSQL.getOneValue(birthdaySQL);
				String tAppntAge = String.valueOf(PubFun.calInterval(mBirthday,
						tLCGetSchema.getGettoDate(), "Y"));
				Calculator tCalculator = new Calculator();
				tCalculator.addBasicFactor("AppntAge", tAppntAge);
				tCalculator.addBasicFactor("Age", tAppntAge);
				LMDutyGetAliveDB tLMDutyGetaLiveDB = new LMDutyGetAliveDB();

				tLMDutyGetaLiveDB.setGetDutyCode(tLCGetSchema.getGetDutyCode());
				LMDutyGetAliveSet tLMDutyGetAliveSet = tLMDutyGetaLiveDB
						.query();
				LMDutyGetAliveSchema tLMDutyGetAliveSchema = tLMDutyGetAliveSet
						.get(1);
				String pCalCode = "";
				if (tLMDutyGetAliveSchema.getCalCode() != null
						&& !"".equals(tLMDutyGetAliveSchema.getCalCode())) {
					pCalCode = tLMDutyGetAliveSchema.getCalCode();
				}
				if (tLMDutyGetAliveSchema.getCnterCalCode() != null
						&& !"".equals(tLMDutyGetAliveSchema.getCnterCalCode())) {
					pCalCode = tLMDutyGetAliveSchema.getCnterCalCode();
				}
				if (tLMDutyGetAliveSchema.getOthCalCode() != null
						&& !"".equals(tLMDutyGetAliveSchema.getOthCalCode())) {
					pCalCode = tLMDutyGetAliveSchema.getOthCalCode();
				}
				tCalculator.setCalCode(pCalCode);

				// 责任保额
				String strAmnt = "select sum(amnt) from lcduty where polno='"
						+ tLCGetSchema.getPolNo() + "' and dutycode='"
						+ tLCGetSchema.getDutyCode() + "'";
				String amnt = mExeSQL.getOneValue(strAmnt);

				// 判断主险是否出险,满期给付责任在主险。
				if (getPolAttribute(tLCGetSchema.getPolNo())
						&& tLMDutyGetAliveSchema.getDiscntFlag().equals("0")) {
					// 得到该险种的附加险种
					String annexPolNoSQL = "select code From ldcode1 where code1 =(select riskcode from lcpol where polno ='"
							+ tLCGetSchema.getPolNo()
							+ "')  and codetype='checkappendrisk' ";
					String annexRisk = mExeSQL.getOneValue(annexPolNoSQL);
					String annexPolNo = "select polno from lcpol where contno='"
							+ tLCGetSchema.getContNo()
							+ "' and riskcode ='"
							+ annexRisk + "'";
					if (annexRisk.equals("") && annexRisk == "") {
						System.out.println("没有得到附加险信息。");
						continue;
					}
					// 1."如果附加的有LP就不做给付";
					String sqlcliam = "select polNo From ljagetclaim where polno ='"
							+ new ExeSQL().getOneValue(annexPolNo) + "'";
					String cliam = mExeSQL.getOneValue(sqlcliam);
					if (!cliam.equals("") && cliam != "" && cliam != "null") {
						System.out.println("附加险已经出险，不能做满期给付。");
						continue;
					}
					// 2."主险出现了并且是死亡责任的不做给付"
					String mainSQL = "select polNo From ljagetclaim where polno ='"
							+ tLCGetSchema.getPolNo()
							+ "'"
							+ " and getdutykind in('501','502','503','504') ";
					String main = mExeSQL.getOneValue(mainSQL);
					if (!main.equals("") && main != "null") {
						System.out.println("主险责任出现理赔，并且是死亡责任，不能满期给付。");
						continue;
					}
					// 3.如果满期金方式的责任出险，不做给付。
					if (tLMDutyGetAliveSchema.getGetDutyKind().equals("0")) {
						String SQL = "select polNo From ljagetclaim where polno ='"
								+ tLCGetSchema.getPolNo() + "'";
						String polSLQ = mExeSQL.getOneValue(SQL);
						if (!polSLQ.equals("") && polSLQ != "null") {
							System.out.println("满期责任已经出险，不做给付。");
							continue;
						}
					}
				}

				String payIntv = String.valueOf(getLCPolSchema.getPayIntv());
				tCalculator.addBasicFactor("Amnt", amnt);
				tCalculator.addBasicFactor("PolNo", tLCGetSchema.getPolNo());
				tCalculator.addBasicFactor("PayIntv", payIntv);
				String tStr = tCalculator.calculate();
				double tValue = 0;
				if (tStr == null || tStr.trim().equals("")) {
					tValue = 0;
				} else {
					tValue = Double.parseDouble(tStr);
				}
				sumGetMoney += tValue;

				tLJSGetDrawSchema = new LJSGetDrawSchema();

				tLJSGetDrawSchema.setPolNo(tLCGetSchema.getPolNo());
				tLJSGetDrawSchema.setDutyCode(tLCGetSchema.getDutyCode());
				tLJSGetDrawSchema.setGetDutyKind(tLCGetSchema.getGetDutyKind());
				tLJSGetDrawSchema.setGetDutyCode(tLCGetSchema.getGetDutyCode());
				tLJSGetDrawSchema.setContNo(tLCGetSchema.getContNo());
				tLJSGetDrawSchema.setAppntNo(mLCContSchema.getAppntNo());
				tLJSGetDrawSchema.setInsuredNo(tLCGetSchema.getInsuredNo());
				tLJSGetDrawSchema.setGetDate(CurrentDate);

				tLJSGetDrawSchema.setGetMoney(sumGetMoney);
				tLJSGetDrawSchema.setEnterAccDate(CurrentDate);
				tLJSGetDrawSchema.setFeeOperationType("EB");
				tLJSGetDrawSchema.setFeeFinaType("TF");
				tLJSGetDrawSchema.setManageCom(mLCContSchema.getManageCom());
				tLJSGetDrawSchema.setAgentCom(mLCContSchema.getAgentCom());
				tLJSGetDrawSchema.setAgentType(mLCContSchema.getAgentType());
				tLJSGetDrawSchema.setAgentCode(mLCContSchema.getAgentCode());
				tLJSGetDrawSchema.setAgentGroup(mLCContSchema.getAgentGroup());

				tLJSGetDrawSchema.setRiskCode(getLCPolSchema.getRiskCode());
				tLJSGetDrawSchema.setLastGettoDate(getLCPolSchema.getEndDate());
				tLJSGetDrawSchema.setCurGetToDate(getLCPolSchema.getEndDate());

				tLJSGetDrawSchema.setSerialNo(mserNo);
				tLJSGetDrawSchema.setOperator(tGI.Operator);
				tLJSGetDrawSchema.setMakeDate(CurrentDate);
				tLJSGetDrawSchema.setMakeTime(CurrentTime);
				tLJSGetDrawSchema.setModifyDate(CurrentDate);
				tLJSGetDrawSchema.setModifyTime(CurrentTime);
				this.tLJSGetDrawSet.add(tLJSGetDrawSchema);
			}

		}
		return true;
	
		
	}
	
	
	
    private LCGetSet getLCGetBonusGet(String contNo, String insuredNo) {
        String tSBql = " select b.* from lcpol a,lcget b , lmdutygetalive c  where 1=1 "
                        + " and a.polno = b.polno and b.getdutycode = c.getdutycode   "
                        + " and a.contno='" + contNo + "' "
                        + " and a.appflag='1' "
                        + " and b.getdutykind ='0' "
                        + " and a.riskcode in (select riskcode from LMRiskApp where  RiskType4 = '2') "
                        + " and ((a.paytodate>=a.payenddate) or ( a.paytodate<a.payenddate and b.gettodate<= a.paytodate))  "
                        + " and not exists( select 1 from ljsgetdraw where a.contno= b.contno and getdutykind ='0' and a.polno = polno ) "
                        + " and ( (b.getdutykind ='0' and  (b.gettodate<=b.getenddate or b.getenddate is null))) "
                        + " and b.gettodate <= '"+CurrentDate+"'"
                        + " and b.insuredno='"+insuredNo+"'"
                        + " with ur ";

        LCGetSet tLCGetSet = new LCGetSet();
        LCGetDB tLCGetDB = new LCGetDB();
        tLCGetSet = tLCGetDB.executeQuery(tSBql);
        System.out.println(tSBql.toString());

        if (tLCGetSet.size() == 0) {
            System.out.println("保单号为：" + contNo + "，没有查询到可给付的满期金");
            return new LCGetSet();
        }
        return tLCGetSet;
    }
    
    private boolean getBonusMJGet(String contNo) {
        String tSBql = " select b.* from lcpol a,lcget b , lmdutygetalive c  where 1=1 "
                        + " and a.polno = b.polno and b.getdutycode = c.getdutycode   "
                        + " and a.contno='" + contNo + "' "
                        + " and a.appflag='1' "
                        + " and b.getdutykind ='0' "
                        + " and a.riskcode in (select riskcode from LMRiskApp where  RiskType4 = '2') "
                        + " and ((a.paytodate>=a.payenddate) or ( a.paytodate<a.payenddate and b.gettodate<= a.paytodate))  "
                        + " and not exists( select 1 from ljsgetdraw where a.contno= b.contno and getdutykind ='0' and a.polno = polno ) "
                        + " and ( (b.getdutykind ='0' and  (b.gettodate<=b.getenddate or b.getenddate is null))) "
                        + " and b.gettodate <= '"+CurrentDate+"'"
                        + " with ur ";

        LCGetSet tLCGetSet = new LCGetSet();
        LCGetDB tLCGetDB = new LCGetDB();
        tLCGetSet = tLCGetDB.executeQuery(tSBql);
        System.out.println(tSBql.toString());

        if (tLCGetSet.size() == 0) {
            System.out.println("保单号为：" + contNo + "，没有查询到可给付的满期金");
            return false;
        }else{
        	return true;
        }
    }
    
    
	public static void main(String[] args) {
		System.out.println(PubFun.calDate("2009-01-24",60,"A", null));
	}
}
