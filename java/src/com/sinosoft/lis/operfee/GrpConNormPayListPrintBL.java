package com.sinosoft.lis.operfee;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.TextTag;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.ListTable;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.vschema.LDCodeSet;

public class GrpConNormPayListPrintBL {
    /**错误的容器*/
    public CErrors mCErrors = new CErrors();
    /** 往界面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate = "";
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();

    private TransferData mTransferData = new TransferData();

    private GlobalInput mGlobalInput = new GlobalInput();

    private String mPayDate = ""; //应收止期，批量核销

    private String mGrpContNo = ""; //保单号，个案核销

    private String mVerifyType = ""; //1：批量核销；2：个案核销

    private SSRS mSSRS = new SSRS();

    private XmlExport xmlexport = new XmlExport();

    private String mLJASql = "";


    public GrpConNormPayListPrintBL() {
    }

    /**
     *
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        this.mInputData = (VData) cInputData;
        this.mOperate = cOperate;
        System.out.println("submitData");
        if (!mOperate.equals("PRINT")) {
            this.bulidError("", "");
            return false;
        }
        //取得外部传入数据
        System.out.println("print");
        if (!getInputData(mInputData)) {
            this.bulidError("getInputData", "");
            return false;
        }
        //校验数据合法性
        System.out.println("getInputData");
        if (!getListData()) {
//            this.bulidError("getListData", "");
            return false;
        }
        //获取打印所需数据
        System.out.println("getListData");
        if (!getPrintData()) {
//            this.bulidError("getPrintData", "");
            return false;
        }
        System.out.println("getPrintData");
        return true;
    }

    /**
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

        try {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName("GlobalInput", 0));
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);

        } catch (Exception ex) {
            this.mCErrors.addOneError("数据不完整");
            return false;
        }

        mPayDate = (String) mTransferData.getValueByName("PayDate");
        mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
        mVerifyType = (String) mTransferData.getValueByName("VerifyType");
        mLJASql = (String) mTransferData.getValueByName("LJASql");
        System.out.println("444444444444444444444444444444" + mLJASql);

        return true;
    }

    /**
     * 查询列表显示数据
     * @return boolean
     */
    private boolean getListData() {

//        StringBuffer sql = new StringBuffer();

        //批次核销
//        if (mVerifyType.equals("1")) {
//            sql.append(
//                    "select b.ContNo,b.AppntName,a.GetNoticeNo,c.SumActuPayMoney,");
//            sql.append("min(a.LastPayToDate),b.Dif,");
//            sql.append("(select max(payDate) from LJTempFee where TempFeeNo= a.GetNoticeNo and ConfFlag='1'),");
//            sql.append(
//                    "(select codename from ldcode where codetype='paymode' and code=b.PayMode),");
//            sql.append("min(a.CurPayToDate),a.AgentCode,c.Operator");
//            sql.append(" from LJAPayPerson a ,LCCont  b,LJAPay c ");
//            sql.append(" where a.LastPayToDate <='" + mPayDate +
//                       "' and b.PayIntv>0 and b.AppFlag='1'");
//            sql.append(
//                    " and a.paycount>1 and c.IncomeType='2' and c.IncomeNo=b.ContNo");
//            sql.append(" and a.ContNo=b.ContNo and a.payno=c.payno and c.PayNo=(select max(payno) from LJAPay where IncomeNo=b.ContNo)");
//            sql.append(" group by b.ContNo,b.AppntName,a.GetNoticeNo,c.SumActuPayMoney,b.PayMode,b.Dif,a.AgentCode,c.Operator,c.PayNo");
//        } else {
//            sql.append(
//                    "select  b.ContNo,b.AppntName,c.GetNoticeNo,c.SumActuPayMoney,");
//            sql.append(
//                    "(select  min(LastPayToDate) from LJAPayPerson where PayNo=c.PayNo)");
//            sql.append(",b.Dif,(select max(payDate) from LJTempFee where TempFeeNo= c.GetNoticeNo and ConfFlag='1'),");
//            sql.append(
//                    "(select codename from ldcode where codetype='paymode' and code=b.PayMode)");
//            sql.append(",(select  min(CurPayToDate) from LJAPayperson where PayNo=c.PayNo),b.AgentCode,");
//            sql.append(
//                    "c.Operator from LCCont  b,LJAPay c where b.ContNo='" +
//                    mGrpContNo + "' ");
//            sql.append(" and b.PayIntv>0 and b.AppFlag='1' and c.IncomeType='2' and c.IncomeNo=b.ContNo ");
//            sql.append(
//                    " and c.PayNo=(select max(payno) from LJAPay where IncomeNo=b.ContNo)");
//
//        }

        if (this.mLJASql.equals("")) {
            this.bulidError("getListData", "数据不完整");
            return false;
        }

        ExeSQL tExeSQL = new ExeSQL();

//        System.out.println(sql.toString());
        System.out.println(mLJASql);
//        mSSRS = tExeSQL.execSQL(sql.toString());
        mSSRS = tExeSQL.execSQL(mLJASql);

        if (tExeSQL.mErrors.needDealError()) {
            CError tCError = new CError();
            tCError.moduleName = "MakeXMLBL";
            tCError.functionName = "creatFile";
            tCError.errorMessage = "查询XML数据出错！";
            this.mCErrors.addOneError(tCError);

            return false;

        }
        return true;
    }

    /**
     * 获取打印所需要的数据
     * @return boolean
     */
    private boolean getPrintData() {

        TextTag tTextTag = new TextTag();
        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("GrpConNormPayList.vts", "printer"); //最好紧接着就初始化xml文档

        //得到公司名
        String grpName = this.getCompanyName();

        if (grpName == null) {
            return false;
        }

        tTextTag.add("ManageName", grpName);

        xmlexport.addTextTag(tTextTag);

        String[] title = {"保单号", "投保人", "应收记录号", "应收金额", "应收日期",
                         "保费帐户余额", "收费日期", "收费方式", "下一应收日期", "业务员代码", "操作人"};

        System.out.println("11111111111111111");
        xmlexport.addListTable(this.getListTable(), title);

        System.out.println("22222222222222222");
        mResult.clear();

        mResult.addElement(xmlexport);

        return true;
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult() {
        return this.mResult;

    }


    private void bulidError(String cFunction, String cErrorMsg) {

        CError tCError = new CError();
        tCError.moduleName = "GrpConNormPayListPrintBL";
        tCError.functionName = cFunction;
        tCError.errorMessage = cErrorMsg;
        this.mCErrors.addOneError(tCError);
    }

    /**
     * 获取列表数据
     * @return ListTable
     */
    private ListTable getListTable() {
        ListTable tListTable = new ListTable();
        System.out.println(mSSRS.getMaxRow());
        int col = 0;
        col = mSSRS.getMaxCol();

        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[11];
            System.out.println(mSSRS.getMaxCol());
            if (col == 12) {
                for (int j = 1; j < mSSRS.getMaxCol(); j++) {
                    info[j - 1] = mSSRS.GetText(i, j);
                    System.out.println(j);

                    System.out.println(info[j - 1] + "=" + mSSRS.GetText(i, j));

                }

            } else {
                for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
                    info[j - 1] = mSSRS.GetText(i, j);
                    System.out.println(j);

                    System.out.println(info[j - 1] + "=" + mSSRS.GetText(i, j));
                }
            }

            tListTable.add(info);

        }

        System.out.println("33333333333333333333");

        tListTable.setName("ZT");

        return tListTable;

    }

    private String getCompanyName() {

        //通过LDCode得到保单号，然后得到公司名称
        String manageCode = mGlobalInput.ManageCom;
        LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("station");
        tLDCodeDB.setCode(manageCode);
        LDCodeSet set = tLDCodeDB.query();
        if (set == null || set.size() == 0) {

            mCErrors.addOneError("没有查到公司名");
            System.out.println("没有查到公司名" + tLDCodeDB.mErrors.getErrContent());
            return null;
        } else {
            return set.get(1).getCodeName();
        }

    }

    public static void main(String[] args) {

    }
}
