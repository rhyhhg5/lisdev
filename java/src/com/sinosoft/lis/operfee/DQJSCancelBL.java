package com.sinosoft.lis.operfee;

import bqxqmodify.PayNo;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vschema.LJSPayBSet;
import com.sinosoft.lis.schema.LJSPayBSchema;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LJAPaySet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.schema.LCAppAccTraceSchema;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.LCAppAccTraceDB;
import com.sinosoft.lis.vschema.LCAppAccTraceSet;
import com.sinosoft.lis.xb.PRnewAppCancelBL;
import com.sinosoft.lis.xb.XBConst;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.db.LGPhoneHastenDB;
import com.sinosoft.lis.vschema.LGPhoneHastenSet;
import com.sinosoft.lis.taskservice.NextFeeHastenTask;
import com.sinosoft.lis.schema.LGPhoneHastenSchema;
import com.sinosoft.task.TaskPhoneFinishBL;
import com.sinosoft.lis.db.LCAppAccDB;
import com.sinosoft.utility.SysConst;

/**
 * <p>Title: lis</p>
 *
 * <p>Description:
 * 处理应收作废业务逻辑
 * a)	删除财务应收记录，应收备份表状态变更为已作废
 * b)	回退续保操作，删除续保临时数据
 * c)	若已收费，则财务暂收转入投保人帐户

 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class DQJSCancelBL {

    public CErrors mCErrors = new CErrors();

    private VData mInputData = new VData();

    private String mOperate = "";

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();

    private boolean mIsRepeal = false;  //撤销标志

    private String mEdorNo = "";

    private String mContNo = "";

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    private LYSendToBankSet mLYSendToBankSet = new LYSendToBankSet();

    private LCContSet mLCContSet = new LCContSet();

    private LCContSchema mLCContSchema = new LCContSchema();
    
    private LJAGetSchema mLJAGetSchema=new LJAGetSchema();
    
    private LJAPaySchema mLJAPaySchema=new LJAPaySchema();

    private LJSPayBSet tLJSPayBSet = new LJSPayBSet();

    private LJSPayBSchema mLJSPayBSchema = new LJSPayBSchema();

    private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();

    private LJSPayBSet mLJSPayBSet = new LJSPayBSet();

    private String mLPEdorAppSql = "";

    private String mLPGrpEdorItemSql = "";
    
    private String mLPGrpEdorMainSql = "";
    
    private String mLJAPaySql = "";
    
    private String mLJAEdorBalDetailSql="";

    private MMap map = new MMap();

    public DQJSCancelBL() {
    }

    /**
     * 为外部调用提供的接口，进行作废逻辑处理
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

    System.out.println("------Begin--------------DQJSCancelBL------------------");
       if(getSubmitMap(cInputData, cOperate) == null)
       {
           return false;
       }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            System.out.println("delete fail!");
            return false;
        }

    	
    	
        return true;
    }

    /**
     * 为外部调用提供的接口，进行作废逻辑处理
     * @param cInputData VData
     * @param cOperate String
     * @return MMap
     */
    public MMap getSubmitMap(VData cInputData, String cOperate){

        mOperate = cOperate;

        mInputData = (VData) cInputData;

        if (!mOperate.equals("INSERT")) {

            return null;
        }

        if (!this.getInputData(mInputData)) {
            return null;
        }
        
        if(!prepareData())
        {
            this.bulidError("prepareData",
                            "存数据到MAP错误");
            return null;
        }

        return map;
    }

  

    /**
     * 得到外部传入的数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

        try {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName(
                                           "GlobalInput", 0));

            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            if (mGlobalInput == null || mTransferData == null || mLJSPaySchema == null) {
                this.bulidError("getInputData", "数据不完整");
                return false;
            }                                    
            mContNo = (String) mTransferData.getValueByName("ContNo");
            mEdorNo = (String) mTransferData.getValueByName("EdorNo");
            String isRepeal = (String)mTransferData.getValueByName("IsRepeal");

        } catch (Exception ex) {
            this.mCErrors.addOneError("数据不完整");
            return false;
        }

        return true;
    }

  
    
   

    /**
     * 准备数据
     * @return boolean
     */
    public boolean prepareData() { 
    	//撤销相关定期结算单:
        map.add(dealLCGrpBalPlan());
        
        //保全表处理
        map.add(dealLGWork());
        mLPEdorAppSql="delete from lpedorapp where edoracceptno='"+mEdorNo+"'";
        map.put(mLPEdorAppSql, "DELETE");
        
        //删除保全表
        mLPGrpEdorItemSql="delete from lpgrpedoritem where edoracceptno='"+mEdorNo+"'";
        map.put(mLPGrpEdorItemSql, "DELETE");        
        mLPGrpEdorMainSql="delete from lpgrpedormain where edoracceptno='"+mEdorNo+"'";
        map.put(mLPGrpEdorMainSql, "DELETE");
        
        //更新财务数据
        dealLJAPay();       
        dealLJAGet();
        map.add(insertLJAGetOrLJAPay());
         
        if(getLJAGet())
        {
	        mLJAEdorBalDetailSql="delete from LJAEdorBalDetail where ActuNo='"+mLJAGetSchema.getActuGetNo()+"'";
	        map.put(mLJAEdorBalDetailSql, "DELETE");
        }

        mInputData.add(map);
        return true;
    }
   
    /*
     * 插入ljaget
     */
    private MMap  insertLJAGetOrLJAPay()
    {
    	MMap tMMap=new MMap();
    	if(mLJAPaySchema.getPayNo()!=null&&mLJAPaySchema.getConfDate()==null)
    	{
    		map.add(dealLJAPay());       
            
    		tMMap.put(deleteLJSPay(),SysConst.DELETE);
    		String payNo=PayNo.CreateMaxNo("PAYNO",null);
    		mLJAPaySchema.setPayNo(payNo);
    		mLJAPaySchema.setSumActuPayMoney(-(mLJAPaySchema.getSumActuPayMoney()));
    		tMMap.put(mLJAPaySchema,SysConst.INSERT);
    		
    	}

    	if(mLJAGetSchema.getActuGetNo()!=null&&mLJAGetSchema.getConfDate()==null)
    	{
    		map.add(dealLJAGet());
    		String actuGetNo=PubFun1.CreateMaxNo("GETNO", null);
    		mLJAGetSchema.setActuGetNo(actuGetNo);
    		mLJAGetSchema.setSumGetMoney(-(mLJAGetSchema.getSumGetMoney()));
    		tMMap.put(mLJAGetSchema,SysConst.INSERT);
    	}
    
    	return tMMap;
    }
    
    /*
     *  未缴费时删除
     */
    private LJSPaySchema deleteLJSPay()
    {
    	LJSPayDB tLJSPayDB=new LJSPayDB();
    	tLJSPayDB.setOtherNo(mEdorNo);
    	if(!tLJSPayDB.getInfo())
    	{
    		return null;
    	}
    	LJSPaySchema tLJSPaySchema=tLJSPayDB.getSchema();
    	return tLJSPaySchema;
    	
    }
    
    
    /*
     * 处理LJAPAY
     */   
    private MMap  dealLJAPay()
    {
      LJAPayDB tLJAPayDB = new LJAPayDB();
      String sql="select payno from ljapay where incomeno='"+mContNo+"'";
      SSRS cSSRS =new SSRS();
      ExeSQL dExeSQL  = new ExeSQL ();
      cSSRS=dExeSQL.execSQL(sql);
      if(cSSRS==null)
      {
    	  return null;
      }
 	  tLJAPayDB.setPayNo(cSSRS.GetText(1,1));
 	  if(!tLJAPayDB.getInfo())
 	  {
 		  return null;
 	  }	
 	  mLJAPaySchema=tLJAPayDB.getSchema();
 	  tLJAPayDB.setIncomeType("B"); 	  
 	  tLJAPayDB.setModifyDate(mCurrentDate);
 	  tLJAPayDB.setModifyTime(mCurrentTime); 
 	 
 	  
 	  MMap tMap = new MMap();
      tMap.put(tLJAPayDB.getSchema(), SysConst.UPDATE);

      return tMap;
    }
    /*
     * 得到ljaget
     */
    private boolean getLJAGet()
    {
 	  LJAGetDB tLJAGetDB = new LJAGetDB();
 	  String sql="select ActuGetNo from ljaget where otherno='"+mEdorNo+"'";
 	  SSRS cSSRS =new SSRS();
       ExeSQL dExeSQL  = new ExeSQL ();
       cSSRS=dExeSQL.execSQL(sql);
       if(cSSRS!=null)
       {
     	  return false;
       }
      tLJAGetDB.setActuGetNo(cSSRS.GetText(0,0));
 	  if(!tLJAGetDB.getInfo())
 	  {
 		  return false;
 	  }	
 	  mLJAGetSchema=tLJAGetDB.getSchema();

      return true;
    }
   
    
    
    
    /*
    处理ljaget
   */ 
   private MMap  dealLJAGet()
   {
	  LJAGetDB tLJAGetDB = new LJAGetDB();
	  String sql="select ActuGetNo from ljaget where otherno='"+mEdorNo+"'";
	  SSRS cSSRS =new SSRS();
      ExeSQL dExeSQL  = new ExeSQL ();
      cSSRS=dExeSQL.execSQL(sql);
      if(cSSRS!=null)
      {
    	  return null;
      }
      
      tLJAGetDB.setActuGetNo(cSSRS.GetText(0,0));
	  if(!tLJAGetDB.getInfo())
	  {
		  return null;
	  }	
	  mLJAGetSchema=tLJAGetDB.getSchema();
	  tLJAGetDB.setPayMode("B"); 
	  tLJAGetDB.setModifyDate(mCurrentDate);
	  tLJAGetDB.setModifyTime(mCurrentTime); 
	  
	  
	  MMap tMap = new MMap();
      tMap.put(tLJAGetDB.getSchema(), SysConst.UPDATE);

       return tMap;
   }
    /*
     * 更改工单状态
    */
    private MMap dealLGWork()
    {
    	LGWorkDB tLGWorkDB =new LGWorkDB();
    	tLGWorkDB.setWorkNo(mEdorNo);
    	if(!tLGWorkDB.getInfo())
    	{
    		return null;
    	}
    	tLGWorkDB.setStatusNo("8");
    	tLGWorkDB.setModifyDate(mCurrentDate);
    	tLGWorkDB.setModifyTime(mCurrentTime);
    	   	
    	MMap tMap = new MMap();
        tMap.put(tLGWorkDB.getSchema(), SysConst.UPDATE);
    	return tMap;
    }

   /*
    撤销相关定期结算单
   */ 
   private MMap  dealLCGrpBalPlan()
   {
	  LCGrpBalPlanDB tLCGrpBalPlanDB = new LCGrpBalPlanDB();
	  tLCGrpBalPlanDB.setGrpContNo(mContNo);
	  if(!tLCGrpBalPlanDB.getInfo())
	  {
		  return null;
	  }
	  tLCGrpBalPlanDB.setState("0"); 
	  tLCGrpBalPlanDB.setBalMoney(0);
	  tLCGrpBalPlanDB.setModifyDate(mCurrentDate);
	  tLCGrpBalPlanDB.setModifyTime(mCurrentTime); 
	  
	  MMap tMap = new MMap();
      tMap.put(tLCGrpBalPlanDB.getSchema(), SysConst.UPDATE);

       return tMap;
   }
    
  
    /**
     *
     * @param cFunctionName String
     * @param cErrorsMsg String
     */
    private void bulidError(String cFunctionName, String cErrorsMsg) {

        CError tCError = new CError();
        tCError.moduleName = "IndiLJSCancelBL";
        tCError.functionName = cFunctionName;
        tCError.errorMessage = cErrorsMsg;

        this.mCErrors.addOneError(tCError);
    }

    public static void main(String[] args) {

    	TransferData tTransferData= new TransferData();
    	tTransferData.setNameAndValue("EdorNo","20080922000009");
    	tTransferData.setNameAndValue("ContNo","00017065000001");
    	System.out.println("20100629000001");

        GlobalInput g = new GlobalInput();
        g.ComCode = "86";
        g.Operator = "zgm";

        VData tVData = new VData();
        tVData.addElement(g);
        tVData.addElement(tTransferData);

        DQJSCancelBL bl = new DQJSCancelBL();
        if(!bl.submitData(tVData, "INSERT"))
        {
            System.out.println(bl.mCErrors.getErrContent());
        }
    }
}
