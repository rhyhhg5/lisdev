package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.CommonBL;
import java.util.*;
import java.lang.String;
import com.sinosoft.lis.bq.AppAcc;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class GrpDueFeeBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private VData mPerInputData = new VData();
    private GlobalInput tGI = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String mStartDate = ""; //应收开始日期
    private String mEndDate = ""; //应收结束日期
    private String StartDate = ""; //操作开始日期
    private String StartTime = ""; //操作结束时间
    private boolean mINNC = false; //Is NoName Cont若loadFlag == true，则只查询无名单数据
    private TransferData mTransferData = new TransferData();
    private String mSerGrpNo = ""; //接受外部参数
    private SSRS mSSRS;
    private SSRS mPerSSRS;
    /** 数据表  保存数据*/
    //个人保单表
    private LCPolSet mLCPolSet = new LCPolSet();
    private LCPolSchema mLCPolSchema = new LCPolSchema();
    //集体保单表
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

    //保费项表
    private LCPremSet mLCPremSet = new LCPremSet();
    private LCPremSchema mLCPremSchema = new LCPremSchema();

    //应收个人交费表
    private LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet();
    private LJSPayPersonBSet mLJSPayPersonBSet = new LJSPayPersonBSet();
    //private LJSPayPersonSchema mLJSPayPersonSchema = new LJSPayPersonSchema();
    //应收团体交费表
    private LJSPayGrpSet tLJSPayGrpSet = new LJSPayGrpSet();
    private LJSPaySet mLJSPaySet = new LJSPaySet();
    private LJSPayBSet mLJSPayBSet = new LJSPayBSet();
    //应收总表
    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();
    private LJSPayBSchema mLJSPayBSchema = new LJSPayBSchema();
    //应收总表集
    private LJSPaySet tLJSPaySet = new LJSPaySet();
    private LJSPayGrpSet mLJSPayGrpSet = new LJSPayGrpSet();
    private LJSPayGrpSet mYelLJSPayGrpSet = new LJSPayGrpSet();
    //应收总表集备份表
    private LJSPayGrpBSet mLJSPayGrpBSet = new LJSPayGrpBSet();
    private LJSPayGrpBSet mYelLJSPayGrpBSet = new LJSPayGrpBSet();
    //应收总表交费日期
    private String strNewLJSPayDate;
    //应收总表最早交费日期
    private String strNewLJSStartDate;
    //应收总表费额
    private double totalsumPay = 0;
    //保险责任表
    private LCDutySet mLCDutySet = new LCDutySet();
    private LCDutySchema mLCDutySchema = new LCDutySchema();

    //无名单续期录入保费
    private LCNoNamePremTraceSet mLCNoNamePremTraceSet = null;

    //集体险种保单表
    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet(); //杨红于2005-07-19添加该私有变量
    //针对一个集体单可能对应多个集体险种单的情况
    private LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
    private LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
    private LCUrgeVerifyLogSchema mLCUrgeVerifyLogSchema = new
            LCUrgeVerifyLogSchema();


    //缴费通知书
    private LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private LOPRTManagerSubSet mLOPRTManagerSubSet = new LOPRTManagerSubSet();
    private MMap map = new MMap();
    private MMap mapPer = new MMap();
    private MMap mapAcc = new MMap();
    private String mOpertor = "INSERT";
    private String mOperateFlag = "1"; // 1 : 个案催收，2：批量催收
    private String mserNo = "1"; //批次号

    //qulq 2007-2-7 modify
    //保单缴费方式
    private String mBankCode = "";
    private String mBankAccNo = "";
    private String mAccName = "";

    public GrpDueFeeBL() {
    }

    public static void main(String[] args) {

        GlobalInput tGt = new GlobalInput();
        tGt.ComCode = "86";
        tGt.Operator = "wuser";
        tGt.ManageCom = "86";

        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema.setGrpContNo("0000019706");

        TransferData tempTransferData = new TransferData();
        tempTransferData.setNameAndValue("StartDate", "2005-01-01");
        tempTransferData.setNameAndValue("EndDate", "2008-8-13");
        tempTransferData.setNameAndValue("ManageCom", "86");
        tempTransferData.setNameAndValue("LoadFlag", "WMD");

        VData tVData = new VData();
        tVData.add(tLCGrpContSchema);
        tVData.add(tGt);
        tVData.add(tempTransferData);

        //  LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
        //  tLCGrpPolSchema.setGrpPolNo("86110020040220000102");
        // VData tv = new VData();
        //  tv.add(tLCGrpPolSchema);
        // tv.add(tGt);

        GrpDueFeeBL GrpDueFeeBL1 = new GrpDueFeeBL();
        GrpDueFeeBL1.submitData(tVData, "INSERT");
    }

    /**
     * 杨红于2005-07-19添加数据校验，判断页面中查询出的信息是否已被催收
     * @return boolean
     */
    public boolean checkData() {

        System.out.println("续期抽档校验");

        // count = null;
        StringBuffer tSBql = new StringBuffer(128);
        tSBql.append(
                "SELECT a.* FROM LPGrpEdorMain a WHERE a.edorstate<>'0' ")
                .append(" and (select count(EdorAcceptNo) from LPGrpEdorItem where EdorAcceptNo=a.EdorAcceptNo")
                .append(" and EdorNo = a.EdorNo")
                .append(" and EdorType not in ('AC','LP','BC'))>0")
                .append(" AND a.GrpContNo='")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("'")
                ;
        String sqlEndor = tSBql.toString();
        System.out.println(sqlEndor);
        LPGrpEdorMainDB tLPGrpEdorMainDB = new LPGrpEdorMainDB();
        LPGrpEdorMainSet tLPGrpEdorMainSet = new LPGrpEdorMainSet();
        tLPGrpEdorMainSet = tLPGrpEdorMainDB.executeQuery(sqlEndor);
        if (tLPGrpEdorMainDB.mErrors.needDealError()) {
            this.mErrors.addOneError("团单号:" + mLCGrpContSchema.getGrpContNo() +
                                     "查询错误!");
            return false;
        }

        if ((tLPGrpEdorMainSet != null) && tLPGrpEdorMainSet.size() != 0) {
            CError.buildErr(this,
                            "团单号:" + mLCGrpContSchema.getGrpContNo() +
                            "催收错误:正在做保全!");
            return false;
        }
        StartDate = (String) mTransferData.getValueByName("CurrStartDate");
        StartTime = (String) mTransferData.getValueByName("CurrStartTime");
        mStartDate = (String) mTransferData.getValueByName("StartDate");
        mEndDate = (String) mTransferData.getValueByName("EndDate");
        String ManageCom = (String) mTransferData.getValueByName("ManageCom");
        String loadFlag = (String) mTransferData.getValueByName("LoadFlag");
        if (loadFlag != null && loadFlag.equals("WMD")) {
            mINNC = true;
        } else {
            String isWMD =
                    "select 1 from LCCont where polType = '1' and appFlag = '1' and (StateFlag is null or StateFlag = '1') and GrpContNo = '"
                    + mLCGrpContSchema.getGrpContNo() + "'";
            ExeSQL tExeSQL = new ExeSQL();
            SSRS tSSRS = tExeSQL.execSQL(isWMD);
            if (tSSRS.getMaxRow() >= 1) {
                this.mErrors.addOneError("无名单:" + mLCGrpContSchema.getGrpContNo() +
                                         "不能在团单续期界面中催收!");
                return false;
            }
        }
        //添加对团单是否正在进行实收转出的校验
        //qulq 添加于2006-10-8
        if(CommonBL.checkGrpBack(mLCGrpContSchema.getGrpContNo()))
        {
            this.mErrors.addOneError("您好，团单正在进行实收保费转出，不能催收。");
            return false;
        }
        //
        tSBql = new StringBuffer(128);
        tSBql.append(
                "select * from LCGrpCont where AppFlag='1' and (StateFlag is null or StateFlag = '1') and GrpContNo='")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("' and managecom like '")
                .append(ManageCom)
                .append("%%'")
                .append(
                        " and GrpContNo not in (select otherno from ljspay where othernotype='1')")
                .append(" and GrpContNo in (select GrpContNo from LCGrpPol")
                .append(" where (PaytoDate>='")
                .append(mStartDate)
                .append("' and PaytoDate<='")
                .append(mEndDate)
                .append("' and PaytoDate<PayEndDate )")
                .append(" and PayIntv>0 and AppFlag='1' and (StateFlag is null or StateFlag = '1') )")
                ;

        String GrpContStr = tSBql.toString();
        System.out.println(GrpContStr);
        System.out.println("续期团单抽档开始");
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        tLCGrpContSet = tLCGrpContDB.executeQuery(GrpContStr);
        if (tLCGrpContDB.mErrors.needDealError()) {
            this.mErrors.addOneError("团单号:" + mLCGrpContSchema.getGrpContNo() +
                                     "查询错误!");
            return false;
        }
        if (tLCGrpContSet.size() == 0) {
            CError.buildErr(this,
                            "团单号:" + mLCGrpContSchema.getGrpContNo() +
                            "催收错误,可能原因：已被其他业务员催收!");
            return false;
        }
        //下面校验lcgrppol是否有符合条件的记录，将符合条件的lcgrppol保存到mLCGrpPolSet私有变量中
        tSBql = new StringBuffer(128);
        tSBql.append("select * from LCGrpPol where GrpContNo='")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("' and PaytoDate>='")
                .append(mStartDate)
                .append("' and PaytoDate<='")
                .append(mEndDate)
                .append(
                        "' and PaytoDate<PayEndDate  and AppFlag='1' and (StateFlag is null or StateFlag = '1') and ManageCom like '")
                .append(ManageCom)
                .append("%%'")
                .append(
                        " and PayIntv>0 and GrpPolNo not in (select GrpPolNo from LJSPayGrp)")
                .append(
                        " and RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y')")
                ;
        String GrpPolStr = tSBql.toString();
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        System.out.println(GrpPolStr);
        System.out.println("续期团险抽档开始");
        mLCGrpPolSet = tLCGrpPolDB.executeQuery(GrpPolStr);
        if (tLCGrpPolDB.mErrors.needDealError()) {
            CError.buildErr(this,
                            "团单号:" + mLCGrpContSchema.getGrpContNo() +
                            "下的集体险种查询失败！");
            return false;
        }
        if (mLCGrpPolSet.size() == 0) {
            CError.buildErr(this,
                            "团单号:" + mLCGrpContSchema.getGrpContNo() +
                            "下没有符合催收条件的集体险种！");
            return false;
        }

        return true;
    }

    // 为更新数据库准备数据
    public boolean prepareData() {

        map.put(mLJSPayGrpSet, "INSERT");
        map.put(mLJSPaySchema, "INSERT");
        map.put(mLJSPayGrpBSet, "INSERT");
        map.add(mapAcc);
        map.put(mLJSPayBSchema, "INSERT");
        map.put(mLOPRTManagerSubSet, "INSERT");
        map.put(mLOPRTManagerSchema, "INSERT");

        String sql = "  delete from LCNoNamePremTrace "
                     + "where grpContNo = '" + mLCGrpContSchema.getGrpContNo()
                     + "'  and getNoticeNo = '"
                     + mLCGrpContSchema.getGrpContNo() + "' "; //存储无名单录入保费人数是GetNoticeNo为团单号
        map.put(sql, "DELETE");
        updateDealState();
        map.put(mLCNoNamePremTraceSet, "DELETE&INSERT");
        mInputData.add(map);
        return true;
    }

    // 为更新应收个人表准备数据
    public boolean preparePersonData(String aPolNo) {
        //mapPer.put(mLJSPayPersonSet, "INSERT");
        //mapPer.put(mLJSPayPersonBSet, "INSERT");
        MMap tMap = new MMap();
        //加锁
        tMap.add(lockBalaCount(aPolNo));
        tMap.put(mLJSPayPersonSet, SysConst.DELETE_AND_INSERT);
        mPerInputData.clear();
        mPerInputData.add(tMap);
        return true;
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {

        System.out.println("GrpDueFeeBL Begin......");
        this.mOperate = cOperate;

        if (!getInputData(cInputData)) {
            return false;
        }
        if (!checkData()) {
            return false;
        }
//    	20100113 zhanggm 避免并发错误，加锁
        if(!lockGrpCont())
        {
        	return false;
        }

        //进行业务处理
        if (!dealData()) {
            return false;
        }
        if (!prepareData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(mInputData, "") == false) {
            //添加数据到催收核销日志表
            if (!dealUrgeLog("2", "PubSubmit:处理总应收表等失败!", "UPDATE")) {
                return false;
            }

            this.mErrors.addOneError("PubSubmit:处理数据库失败!");

            return false;
        }
        //添加数据到催收核销日志表
        //非批次核销
        if (mSerGrpNo == null || mSerGrpNo.equals("")) {
            if (!dealUrgeLog("3", "", "UPDATE")) {
                return false;
            }
        }

        return true;
    }

    /**
     * Yangh于2005-07-19创建新的dealData的处理逻辑
     * @return boolean
     */
    private boolean dealData() {

        System.out.println("GrpDueFeeBL dealData Begin");

        //获取页面参数
        String startDate = (String) mTransferData.getValueByName("StartDate");
        String endDate = (String) mTransferData.getValueByName("EndDate"); //获取页面
        mSerGrpNo = (String) mTransferData.getValueByName("serNo");

        String Operator = tGI.Operator; //保存登陆管理员账号
        String ManageCom = (String) mTransferData.getValueByName("ManageCom");

        double totalPay = 0; //计算合同下lcprem保费项记录的交费总和
        String serNo = "";
        String prtSeqNo = "";
        String tNo = "";
        String tLimit = "";

        int intCont = 0;

        //下面查找LJSPayPerson的记录
        String strPerSQL =
                " SELECT PolNo,GetNoticeNo,SerialNo  FROM LJSPayPerson where GrpContNo='"
                + mLCGrpContSchema.getGrpContNo() + "' fetch first row only";
        ExeSQL tExeSQL = new ExeSQL();
        mPerSSRS = tExeSQL.execSQL(strPerSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "DealData";
            tError.errorMessage = "查询数据 LJSPayPerson 出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (mPerSSRS.MaxRow >= 1) {
            intCont = 1;
            tNo = mPerSSRS.GetText(1, 2);
            serNo = mPerSSRS.GetText(1, 3);
        }
        mserNo = serNo;
        if (intCont != 1) {
            tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
            tNo = PubFun1.CreateMaxNo("PayNoticeNo", tLimit);
            if (mSerGrpNo == null || mSerGrpNo.equals("")) {
                serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
                mserNo = serNo;
                mOperateFlag = "1";
                //添加数据到催收核销日志表
                if (!dealUrgeLog("1", "", "INSERT")) {
                    return false;
                }

            } else {
                serNo = mSerGrpNo;
                mserNo = serNo;
                mOperateFlag = "2";
            }
        }

        // 产生通知书号--所有个人共用一个交费通知书号
        tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
        prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);

        boolean yelFlag = false; //上次余额的标志
        boolean yetFlag = false; //置是否有余额的标志位（上期合同余额用于交本期保费还有剩余，则置true）
        LJSPayGrpSchema yelLJSPayGrpSchema = new LJSPayGrpSchema(); //如果有差额，添加一个应收集体交费schema
        LJSPayGrpSchema yetLJSPayGrpSchema = new LJSPayGrpSchema();
        LJSPayGrpBSchema yelLJSPayGrpBSchema = new LJSPayGrpBSchema(); //如果有差额，添加一个应收集体交费schema
        LJSPayGrpBSchema yetLJSPayGrpBSchema = new LJSPayGrpBSchema();

        //计算 缴费日期
        if(!calPayDate(mLCGrpPolSet, mLCGrpContSchema.getPayMode()))
        {
            return false;
        }
        String strNewPayToDate2 = "";

        System.out.println("GrpDueFeeBL： 产生应收记录");

        //一下HashMap数据结构在无名单续期时使用
        HashMap contPlanPremInput = null;
        HashMap contPlanPrem = null;
        HashMap contPlanPremSet = null;
        HashMap contPlanPremUsed = null;
        if (mINNC) {
            contPlanPremInput = getContPlanPremInput(tNo); //存储保障计划<->录入保费
            contPlanPrem = getContPlanPrem(); //存储保障计划<->保费
            contPlanPremSet = getcontPlanPremSet(); //存储保障计划<->保费项
            contPlanPremUsed = new HashMap(); //存储保障计划下已使用的保费
        }

        for (int i = 1; i <= mLCGrpPolSet.size(); i++) {

            int grpPayTimes = getGrpPayTimes(mLCGrpPolSet.get(i).getGrpPolNo()); //获取ljspaygrp的交费次数
            double grpSumPay = 0.0;

            LCGrpPolSchema tLCGrpPolSchema = mLCGrpPolSet.get(i);
            int PayInterval = tLCGrpPolSchema.getPayIntv(); //用它算出新的交至日期
            String GrpPolNo = tLCGrpPolSchema.getGrpPolNo();

//            int showDay = getDay();

            FDate tD = new FDate();
            Date newBaseDate = tD.getDate(tLCGrpPolSchema.getPaytoDate());
            Date NewPayToDate = PubFun.calDate(newBaseDate, PayInterval, "M", null);
            String strNewPayToDate = tD.getString(NewPayToDate); //存入到ljspaygrp的新的交至日期
//            Date NewPayDate = new Date();
//            if (CurrentDate.compareTo(newBaseDate.toString()) > 0) {
//                NewPayDate = PubFun.calDate(tD.getDate(CurrentDate), 5, "D", null);
//            } else {
//                NewPayDate = PubFun.calDate(newBaseDate, showDay, "D", null);
//            }

//            String strNewPayDate = tD.getString(NewPayDate); //最迟的交费日期

            //下面获取第一条记录的 newPayDate和newPayToDate以方便以后在yelLjsPayGrp和yetLjsPayGrp里赋值
            if (i == 1) {
                strNewPayToDate2 = strNewPayToDate;
            }
//            if (strNewLJSStartDate == null || strNewLJSStartDate.equals("")) {
//                strNewLJSStartDate = tLCGrpPolSchema.getPaytoDate();
//            } else {
//                strNewLJSStartDate = PubFun.getLaterDate(strNewLJSStartDate,
//                        tLCGrpPolSchema.getPaytoDate());
//            }
//            if (strNewLJSPayDate == null) {
//                strNewLJSPayDate = strNewPayDate;
//            } else {
//                strNewLJSPayDate = PubFun.getBeforeDate(strNewLJSPayDate,
//                        strNewPayDate);
//            }

            System.out.println("LCPol 表的第" + i + "条");
            //下面查询出符合条件的lcpol
            StringBuffer tSBql = new StringBuffer(128);
            tSBql.append("select * from lcpol where grppolno='")
                    .append(GrpPolNo)
                    .append("' and appflag='1' and (StateFlag is null or StateFlag = '1')")
                    .append(" and PaytoDate>='")
                    .append(startDate)
                    .append("' and PaytoDate<='")
                    .append(endDate)
                    .append("' and PaytoDate<PayEndDate")
                    .append(" and ManageCom like '")
                    .append(ManageCom)
                    .append("%'")
                    .append(" and payintv>0")
                    .append(
                            " and polno not in (select polno from ljspayperson)")
                    .append(" and RiskCode  in ")
                    .append(
                            "     (select RiskCode from LMRiskPay where UrgePayFlag='Y')")
                    .append(mINNC ? " and polTypeFlag = '1' " : "") //若是无名单，则只处理无名单数据
                    .append("order by polNo ")
                    .append("with ur ");

            String polSqlStr = tSBql.toString();

            System.out.println("GrpDueFeeBL： 取 lcpol数据 ");
            System.out.println(polSqlStr);

            /** 新增方法:
             * 游标查询方法 */
            RSWrapper rswrapper = new RSWrapper();
            LCPolSet tLCPolSet = new LCPolSet();
            rswrapper.prepareData(tLCPolSet, polSqlStr);
            do {
                //循环取得2000条数据，进行处理
                rswrapper.getData();

                for (int j = 1; j <= tLCPolSet.size(); j++) {

                    mLJSPayPersonBSet.clear();
                    mLJSPayPersonSet.clear();

                    LCPolSchema tLCPolSchema = new LCPolSchema();
                    tLCPolSchema = tLCPolSet.get(j);
                    String polNo = tLCPolSchema.getPolNo();


                    //下面判断是否有符合条件的lcprem,如果有将相关记录映射为应收个人表ljspayperson的信息
                    System.out.println("GrpDueFeeBL： 取 LCPrem 数据 ");

                    tSBql = new StringBuffer(128);
                    tSBql.append("select * from lcprem where polno='")
                            .append(polNo)
                            // .append("' and dutycode='")
                            //  .append(dutyCode)
                            .append("' and payintv>0 and UrgePayFlag='Y'")
                            .append(" and (paytodate>='")
                            .append(startDate)
                            .append("' and paytodate<='")
                            .append(endDate)
                            .append("' and paytodate<payenddate)")
                            ;
                    String premSqlStr = tSBql.toString();
                    System.out.println(premSqlStr);

                    LCPremDB tLCPremDB = new LCPremDB();
                    LCPremSet tLCPremSet = tLCPremDB.executeQuery(
                            premSqlStr);
                    if (tLCPremSet.size() == 0) {
                        continue;
                    }

                    System.out.println("GrpDueFeeBL： 设置LJSPayPerson 数据 ");

                    for (int n = 1; n <= tLCPremSet.size(); n++) {
                        LCPremSchema tLCPremSchema = tLCPremSet.get(n);

                        LJSPayPersonSchema tLJSPayPersonSchema = new
                                LJSPayPersonSchema();
                        tLJSPayPersonSchema.setPolNo(polNo);
                        tLJSPayPersonSchema.setPayCount(tLCPremSchema.
                                getPayTimes() + 1);
                        tLJSPayPersonSchema.setGrpPolNo(tLCPolSchema.
                                getGrpPolNo());
                        tLJSPayPersonSchema.setGrpContNo(mLCGrpContSchema.
                                getGrpContNo());
                        tLJSPayPersonSchema.setContNo(tLCPolSchema.
                                getContNo());
                        tLJSPayPersonSchema.setAppntNo(tLCPolSchema.
                                getAppntNo());
                        tLJSPayPersonSchema.setAgentCom(tLCPolSchema.
                                getAgentCom());
                        tLJSPayPersonSchema.setAgentType(tLCPolSchema.
                                getAgentType());
                        tLJSPayPersonSchema.setGetNoticeNo(tNo); // 共用通知书号
                        tLJSPayPersonSchema.setPayAimClass("2"); //交费目的分类=集体
                        tLJSPayPersonSchema.setDutyCode(tLCPremSchema.
                                getDutyCode());
                        tLJSPayPersonSchema.setPayPlanCode(tLCPremSchema.
                                getPayPlanCode());

                        //收费=期交保费*保费变更比例*(1-责任原定续期收费比例)
                        double money = getMoney(tLCPolSchema,
                                                tLCPremSchema,
                                                contPlanPrem,
                                                contPlanPremSet,
                                                contPlanPremUsed,
                                                contPlanPremInput);
                        tLJSPayPersonSchema.setSumDuePayMoney(money);
                        tLJSPayPersonSchema.setSumActuPayMoney(
                                tLJSPayPersonSchema
                                .getSumDuePayMoney());
                        tLJSPayPersonSchema.setPayIntv(tLCPremSchema.getPayIntv());
                        // 交费日期=失效日期 =现交至日期+2个月:
                        //下面计算 现交至日期
                        FDate fDate = new FDate();
                        Date baseDate = fDate.getDate(tLCPremSchema.
                                getPaytoDate());
                        int interval = tLCPremSchema.getPayIntv();
                        // if (interval == -1) interval = 0; // 不定期缴费
                        // unit = "M";
                        //repair:日期不能为空，否则下面的函数处理会有问题
                        String strPayToDate = "";
                        Date paytoDate;
                        if (baseDate != null) {
                            paytoDate = PubFun.calDate(baseDate, interval,
                                    "M", null);
                            //得到日期型，转换成String型
                            strPayToDate = fDate.getString(paytoDate);
                        }
                        tLJSPayPersonSchema.setPayDate(strNewLJSPayDate);
                        tLJSPayPersonSchema.setPayType("ZC"); //交费类型=ZC即"正常"
                        tLJSPayPersonSchema.setLastPayToDate(tLCPremSchema.
                                getPaytoDate());
                        //原交至日期=保费项表的交至日期
                        //现交至日期CurPayToDate=交至日期+日期（interval=12 12个月-年交，interval=1 即1个月-月交）待修改
                        tLJSPayPersonSchema.setCurPayToDate(strPayToDate);
                        //qulq 2007-2-7 modify
                        if (mLCGrpContSchema.getPayMode().equals("4"))
                        {
                            mBankCode = mLCGrpContSchema.getBankCode();
                            mBankAccNo = mLCGrpContSchema.getBankAccNo();
                            mAccName = mLCGrpContSchema.getAccName();
                        }
                        tLJSPayPersonSchema.setBankCode(mBankCode);
                        tLJSPayPersonSchema.setBankAccNo(mBankAccNo);
                        tLJSPayPersonSchema.setBankOnTheWayFlag("0");
                        tLJSPayPersonSchema.setBankSuccFlag("0");
                        tLJSPayPersonSchema.setApproveCode(tLCPolSchema.
                                getApproveCode());
                        tLJSPayPersonSchema.setApproveDate(tLCPolSchema.
                                getApproveDate());
                        tLJSPayPersonSchema.setApproveTime(tLCPolSchema.
                                getApproveTime());
                        tLJSPayPersonSchema.setManageCom(tLCPolSchema.
                                getManageCom());
                        tLJSPayPersonSchema.setAgentCom(tLCPolSchema.
                                getAgentCom());
                        tLJSPayPersonSchema.setAgentType(tLCPolSchema.
                                getAgentType());
                        tLJSPayPersonSchema.setRiskCode(tLCPolSchema.
                                getRiskCode());
                        tLJSPayPersonSchema.setSerialNo(serNo); //统一一个流水号
                        tLJSPayPersonSchema.setInputFlag("1"); //置交费标志为未交费
                        tLJSPayPersonSchema.setOperator(Operator);
                        tLJSPayPersonSchema.setAgentCode(tLCPolSchema.
                                getAgentCode());
                        tLJSPayPersonSchema.setAgentGroup(tLCPolSchema.
                                getAgentGroup());
                        tLJSPayPersonSchema.setMakeDate(PubFun.getCurrentDate()); //入机日期
                        tLJSPayPersonSchema.setMakeTime(PubFun.getCurrentTime()); //入机时间
                        tLJSPayPersonSchema.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
                        tLJSPayPersonSchema.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间

                        mLJSPayPersonSet.add(tLJSPayPersonSchema);
                        System.out.println("lcgrpcont" + i + "lcpol:" + j);
                    }
                    // }
                    if (!preparePersonData(polNo)) {
                        return false;
                    }
                    PubSubmit tPubSubmit = new PubSubmit();
                    if (tPubSubmit.submitData(mPerInputData, "") == false) {
                        //添加数据到催收核销日志表
                        if (!dealUrgeLog("2", "处理5000数据失败", "UPDATE")) {
                            return false;
                        }

                        System.out.println("5000 行数据处理时 PubSubmit 失败!");
                        CError tError = new CError();
                        tError.moduleName = "PubSubmit";
                        tError.functionName = "submitData";
                        tError.errorMessage = "5000 行数据处理时 PubSubmit 失败!";
                        this.mErrors.addOneError(tError);

                        return false;
                    }

                }
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } while (tLCPolSet.size() > 0);

            //下面生成ljspaygrp的记录
            String strSQL =
                    " SELECT SUM(SumDuePayMoney) FROM LJSPayPerson where GrpPolNo='"
                    + tLCGrpPolSchema.getGrpPolNo() + "'";
            tExeSQL = new ExeSQL();
            String strGrpSumPay = tExeSQL.getOneValue(strSQL);
            if (tExeSQL.mErrors.needDealError()) {
                //添加数据到催收核销日志表
                if (!dealUrgeLog("2", "取LJSPayPerson总保费失败", "UPDATE")) {
                    return false;
                }

                CError tError = new CError();
                tError.moduleName = "GrpDueFeeBL";
                tError.functionName = "DealData";
                tError.errorMessage = "查询数据 LJSPayPerson 出错！";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (strGrpSumPay == null || strGrpSumPay.equals("")) {
                grpSumPay = 0;
            } else {
                grpSumPay = Double.parseDouble(strGrpSumPay);
            }

            LJSPayGrpSchema tLJSPayGrpSchema = new LJSPayGrpSchema();
            //杨红于2005-07-20说明：集体应交的交费次数为ljspayperson的最大次数
            tLJSPayGrpSchema.setGrpPolNo(GrpPolNo);
            tLJSPayGrpSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            tLJSPayGrpSchema.setAppntNo(mLCGrpContSchema.getAppntNo()); //随便取一个lcpol的投保人
            tLJSPayGrpSchema.setPayCount(grpPayTimes + 1); //交费次数
            tLJSPayGrpSchema.setGetNoticeNo(tNo); // 通知书号
            tLJSPayGrpSchema.setSumDuePayMoney(grpSumPay);
            tLJSPayGrpSchema.setSumActuPayMoney(grpSumPay);
            tLJSPayGrpSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
            tLJSPayGrpSchema.setPayDate(strNewLJSPayDate); //交费日期
            tLJSPayGrpSchema.setPayType("ZC"); //交费类型=ZC 正常交费
            tLJSPayGrpSchema.setLastPayToDate(tLCGrpPolSchema.getPaytoDate());
            tLJSPayGrpSchema.setCurPayToDate(strNewPayToDate);
            tLJSPayGrpSchema.setApproveCode(tLCGrpPolSchema.getApproveCode());
            tLJSPayGrpSchema.setApproveDate(tLCGrpPolSchema.getApproveDate());
            tLJSPayGrpSchema.setApproveTime(tLCGrpPolSchema.getApproveTime());
            tLJSPayGrpSchema.setManageCom(tLCGrpPolSchema.getManageCom());
            tLJSPayGrpSchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
            tLJSPayGrpSchema.setAgentType(tLCGrpPolSchema.getAgentType());
            tLJSPayGrpSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
            tLJSPayGrpSchema.setSerialNo(serNo);
            tLJSPayGrpSchema.setInputFlag("1");


            tLJSPayGrpSchema.setOperator(Operator);
            tLJSPayGrpSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
            tLJSPayGrpSchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());
            tLJSPayGrpSchema.setMakeDate(PubFun.getCurrentDate()); //入机日期
            tLJSPayGrpSchema.setMakeTime(PubFun.getCurrentTime()); //入机时间
            tLJSPayGrpSchema.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
            tLJSPayGrpSchema.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间

            mLJSPayGrpSet.add(tLJSPayGrpSchema);
            //备份表
            LJSPayGrpBSchema tLJSPayGrpBSchema = new LJSPayGrpBSchema();
            //杨红于2005-07-20说明：集体应交的交费次数为ljspayperson的最大次数
            tLJSPayGrpBSchema.setGrpPolNo(GrpPolNo);
            tLJSPayGrpBSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            tLJSPayGrpBSchema.setAppntNo(mLCGrpContSchema.getAppntNo()); //随便取一个lcpol的投保人
            tLJSPayGrpBSchema.setPayCount(grpPayTimes + 1); //交费次数
            tLJSPayGrpBSchema.setGetNoticeNo(tNo); // 通知书号
            tLJSPayGrpBSchema.setSumDuePayMoney(grpSumPay);
            tLJSPayGrpBSchema.setSumActuPayMoney(grpSumPay);
            tLJSPayGrpBSchema.setPayIntv(tLCGrpPolSchema.getPayIntv());
            tLJSPayGrpBSchema.setPayDate(strNewLJSPayDate); //交费日期
            tLJSPayGrpBSchema.setPayType("ZC"); //交费类型=ZC 正常交费
            tLJSPayGrpBSchema.setLastPayToDate(tLCGrpPolSchema.getPaytoDate());
            tLJSPayGrpBSchema.setCurPayToDate(strNewPayToDate);
            tLJSPayGrpBSchema.setApproveCode(tLCGrpPolSchema.getApproveCode());
            tLJSPayGrpBSchema.setApproveDate(tLCGrpPolSchema.getApproveDate());
            tLJSPayGrpBSchema.setApproveTime(tLCGrpPolSchema.getApproveTime());
            tLJSPayGrpBSchema.setManageCom(tLCGrpPolSchema.getManageCom());
            tLJSPayGrpBSchema.setAgentCom(tLCGrpPolSchema.getAgentCom());
            tLJSPayGrpBSchema.setAgentType(tLCGrpPolSchema.getAgentType());
            tLJSPayGrpBSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
            tLJSPayGrpBSchema.setSerialNo(serNo);

            tLJSPayGrpBSchema.setInputFlag("1");

            tLJSPayGrpBSchema.setOperator(Operator);
            tLJSPayGrpBSchema.setAgentCode(tLCGrpPolSchema.getAgentCode());
            tLJSPayGrpBSchema.setAgentGroup(tLCGrpPolSchema.getAgentGroup());
            tLJSPayGrpBSchema.setMakeDate(PubFun.getCurrentDate()); //入机日期
            tLJSPayGrpBSchema.setMakeTime(PubFun.getCurrentTime()); //入机时间
            tLJSPayGrpBSchema.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
            tLJSPayGrpBSchema.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间
            if(tLJSPayGrpBSchema.getSumDuePayMoney()>0.01)
                tLJSPayGrpBSchema.setDealState("0"); //催收状态：不成功
            else
                tLJSPayGrpBSchema.setDealState("4"); //催收状态：待核销
            mLJSPayGrpBSet.add(tLJSPayGrpBSchema);

            totalPay += grpSumPay; //杨红于2005-07-20说明：计算出合同下总的应交费用
            LOPRTManagerSubSchema tLOPRTManagerSubSchema = new
                    LOPRTManagerSubSchema();
            tLOPRTManagerSubSchema.setPrtSeq(prtSeqNo);
            tLOPRTManagerSubSchema.setGetNoticeNo(tNo);
            tLOPRTManagerSubSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
            tLOPRTManagerSubSchema.setOtherNoType("01");
            tLOPRTManagerSubSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
            tLOPRTManagerSubSchema.setDuePayMoney(grpSumPay);
            tLOPRTManagerSubSchema.setAppntName(mLCGrpContSchema.getAppntNo());
            tLOPRTManagerSubSchema.setTypeFlag("1");
            mLOPRTManagerSubSet.add(tLOPRTManagerSubSchema);

        }
        //杨红于2005-07-20说明：下面根据情况生成yel和yet的ljspaygrp的记录
        double leavingMoney = getAccBala(mLCGrpContSchema.getAppntNo());
        if (leavingMoney != Double.MIN_VALUE && leavingMoney > 0.0) {
            yelFlag = true;
            yelLJSPayGrpSchema.setGrpPolNo(mLCGrpPolSet.get(1).getGrpPolNo());
            yelLJSPayGrpSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            yelLJSPayGrpSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
            yelLJSPayGrpSchema.setGetNoticeNo(tNo); //通知书号
            yelLJSPayGrpSchema.setSumDuePayMoney( -leavingMoney);
            yelLJSPayGrpSchema.setSumActuPayMoney( -leavingMoney);
            yelLJSPayGrpSchema.setPayIntv(mLCGrpPolSet.get(1).getPayIntv());
            yelLJSPayGrpSchema.setPayDate(strNewLJSPayDate); //交费日期
            yelLJSPayGrpSchema.setPayType("YEL");
            yelLJSPayGrpSchema.setLastPayToDate(mLCGrpPolSet.get(1).
                                                getPaytoDate());
            yelLJSPayGrpSchema.setCurPayToDate(strNewPayToDate2);
            yelLJSPayGrpSchema.setApproveCode(mLCGrpPolSet.get(1).
                                              getApproveCode());
            yelLJSPayGrpSchema.setApproveDate(mLCGrpPolSet.get(1).
                                              getApproveDate());
            yelLJSPayGrpSchema.setApproveTime(mLCGrpPolSet.get(1).
                                              getApproveTime());

            yelLJSPayGrpSchema.setManageCom(mLCGrpPolSet.get(1).getManageCom());
            yelLJSPayGrpSchema.setAgentCom(mLCGrpPolSet.get(1).getAgentCom());
            yelLJSPayGrpSchema.setAgentCode(mLCGrpPolSet.get(1).getAgentCode());
            yelLJSPayGrpSchema.setAgentGroup(mLCGrpPolSet.get(1).getAgentGroup());
            yelLJSPayGrpSchema.setAgentType(mLCGrpPolSet.get(1).getAgentType());
            yelLJSPayGrpSchema.setAgentType(mLCGrpPolSet.get(1).getAgentType());
            yelLJSPayGrpSchema.setRiskCode(mLCGrpPolSet.get(1).getRiskCode());
            yelLJSPayGrpSchema.setInputFlag("1");
            yelLJSPayGrpSchema.setSerialNo(serNo);
            yelLJSPayGrpSchema.setOperator(Operator);
            yelLJSPayGrpSchema.setMakeDate(PubFun.getCurrentDate()); //入机日期
            yelLJSPayGrpSchema.setMakeTime(PubFun.getCurrentTime()); //入机时间
            yelLJSPayGrpSchema.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
            yelLJSPayGrpSchema.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间

            mLJSPayGrpSet.add(yelLJSPayGrpSchema);

            //备份表

            yelLJSPayGrpBSchema.setGrpPolNo(mLCGrpPolSet.get(1).getGrpPolNo());
            yelLJSPayGrpBSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            yelLJSPayGrpBSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
            yelLJSPayGrpBSchema.setGetNoticeNo(tNo); //通知书号
            yelLJSPayGrpBSchema.setSumDuePayMoney( -leavingMoney);
            yelLJSPayGrpBSchema.setSumActuPayMoney( -leavingMoney);
            yelLJSPayGrpBSchema.setPayIntv(mLCGrpPolSet.get(1).getPayIntv());
            yelLJSPayGrpBSchema.setPayDate(strNewLJSPayDate); //交费日期
            yelLJSPayGrpBSchema.setPayType("YEL");
            yelLJSPayGrpBSchema.setLastPayToDate(mLCGrpPolSet.get(1).
                                                 getPaytoDate());
            yelLJSPayGrpBSchema.setCurPayToDate(strNewPayToDate2);
            yelLJSPayGrpBSchema.setApproveCode(mLCGrpPolSet.get(1).
                                               getApproveCode());
            yelLJSPayGrpBSchema.setApproveDate(mLCGrpPolSet.get(1).
                                               getApproveDate());
            yelLJSPayGrpBSchema.setApproveTime(mLCGrpPolSet.get(1).
                                               getApproveTime());

            yelLJSPayGrpBSchema.setManageCom(mLCGrpPolSet.get(1).getManageCom());
            yelLJSPayGrpBSchema.setAgentCom(mLCGrpPolSet.get(1).getAgentCom());
            yelLJSPayGrpBSchema.setAgentType(mLCGrpPolSet.get(1).getAgentType());
            yelLJSPayGrpBSchema.setRiskCode(mLCGrpPolSet.get(1).getRiskCode());
            yelLJSPayGrpBSchema.setInputFlag("1");
            yelLJSPayGrpBSchema.setSerialNo(serNo);
            yelLJSPayGrpBSchema.setOperator(Operator);
            yelLJSPayGrpBSchema.setMakeDate(PubFun.getCurrentDate()); //入机日期
            yelLJSPayGrpBSchema.setMakeTime(PubFun.getCurrentTime()); //入机时间
            yelLJSPayGrpBSchema.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
            yelLJSPayGrpBSchema.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间
            yelLJSPayGrpBSchema.setDealState("0"); //催收状态：不成功

            mLJSPayGrpBSet.add(yelLJSPayGrpBSchema);

            dealAccBala(yelLJSPayGrpBSchema);
        }
        if (yelFlag && leavingMoney > totalPay) {
            yetFlag = true;
            yetLJSPayGrpSchema.setGrpPolNo(mLCGrpPolSet.get(1).getGrpPolNo());
            //System.out.println("Test_yh:"+tLCGrpContSchema.getGrpContNo());
            yetLJSPayGrpSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            yetLJSPayGrpSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
            yetLJSPayGrpSchema.setGetNoticeNo(tNo); //通知书号
            yetLJSPayGrpSchema.setSumDuePayMoney(leavingMoney - totalPay);
            yetLJSPayGrpSchema.setSumActuPayMoney(leavingMoney - totalPay);
            yetLJSPayGrpSchema.setPayIntv(mLCGrpPolSet.get(1).getPayIntv());
            yetLJSPayGrpSchema.setPayDate(strNewLJSPayDate); //交费日期
            yetLJSPayGrpSchema.setPayType("YET"); //交费类型=yet
            yetLJSPayGrpSchema.setLastPayToDate(mLCGrpPolSet.get(1).
                                                getPaytoDate());
            yetLJSPayGrpSchema.setCurPayToDate(strNewPayToDate2);
            yetLJSPayGrpSchema.setApproveCode(mLCGrpPolSet.get(1).
                                              getApproveCode());
            yetLJSPayGrpSchema.setApproveDate(mLCGrpPolSet.get(1).
                                              getApproveDate());
            yetLJSPayGrpSchema.setApproveTime(mLCGrpPolSet.get(1).
                                              getApproveTime());

            yetLJSPayGrpSchema.setManageCom(mLCGrpPolSet.get(1).getManageCom());
            yetLJSPayGrpSchema.setAgentCom(mLCGrpPolSet.get(1).getAgentCom());
            yetLJSPayGrpSchema.setAgentType(mLCGrpPolSet.get(1).getAgentType());
            yetLJSPayGrpSchema.setRiskCode(mLCGrpPolSet.get(1).getRiskCode());
            yetLJSPayGrpSchema.setAgentCode(mLCGrpPolSet.get(1).getAgentCode());
            yetLJSPayGrpSchema.setAgentGroup(mLCGrpPolSet.get(1).getAgentGroup());
            yetLJSPayGrpSchema.setInputFlag("1");
            yetLJSPayGrpSchema.setSerialNo(serNo);
            yetLJSPayGrpSchema.setOperator(Operator);
            yetLJSPayGrpSchema.setMakeDate(PubFun.getCurrentDate()); //入机日期
            yetLJSPayGrpSchema.setMakeTime(PubFun.getCurrentTime()); //入机时间
            yetLJSPayGrpSchema.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
            yetLJSPayGrpSchema.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间

            mLJSPayGrpSet.add(yetLJSPayGrpSchema);
            

            //备份表
            yetLJSPayGrpBSchema.setGrpPolNo(mLCGrpPolSet.get(1).getGrpPolNo());
            //System.out.println("Test_yh:"+tLCGrpContSchema.getGrpContNo());
            yetLJSPayGrpBSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
            yetLJSPayGrpBSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
            yetLJSPayGrpBSchema.setGetNoticeNo(tNo); //通知书号
            yetLJSPayGrpBSchema.setSumDuePayMoney(leavingMoney - totalPay);
            yetLJSPayGrpBSchema.setSumActuPayMoney(leavingMoney - totalPay);
            yetLJSPayGrpBSchema.setPayIntv(mLCGrpPolSet.get(1).getPayIntv());
            yetLJSPayGrpBSchema.setPayDate(strNewLJSPayDate); //交费日期
            yetLJSPayGrpBSchema.setPayType("YET"); //交费类型=yet
            yetLJSPayGrpBSchema.setLastPayToDate(mLCGrpPolSet.get(1).
                                                 getPaytoDate());
            yetLJSPayGrpBSchema.setCurPayToDate(strNewPayToDate2);
            yetLJSPayGrpBSchema.setApproveCode(mLCGrpPolSet.get(1).
                                               getApproveCode());
            yetLJSPayGrpBSchema.setApproveDate(mLCGrpPolSet.get(1).
                                               getApproveDate());
            yetLJSPayGrpBSchema.setApproveTime(mLCGrpPolSet.get(1).
                                               getApproveTime());
            yetLJSPayGrpBSchema.setManageCom(mLCGrpPolSet.get(1).getManageCom());
            yetLJSPayGrpBSchema.setAgentCom(mLCGrpPolSet.get(1).getAgentCom());
            yetLJSPayGrpBSchema.setAgentType(mLCGrpPolSet.get(1).getAgentType());
            yetLJSPayGrpBSchema.setRiskCode(mLCGrpPolSet.get(1).getRiskCode());
            yetLJSPayGrpBSchema.setInputFlag("1");
            yetLJSPayGrpBSchema.setSerialNo(serNo);
            yetLJSPayGrpBSchema.setOperator(Operator);
            yetLJSPayGrpBSchema.setMakeDate(PubFun.getCurrentDate()); //入机日期
            yetLJSPayGrpBSchema.setMakeTime(PubFun.getCurrentTime()); //入机时间
            yetLJSPayGrpBSchema.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
            yetLJSPayGrpBSchema.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间
            yetLJSPayGrpBSchema.setDealState("0"); //催收状态：不成功
            mLJSPayGrpBSet.add(yetLJSPayGrpBSchema);
            
            dealAccBalaYET(yetLJSPayGrpBSchema);//账户余额大于续期保费要在账户轨迹退费处理
                        

        }
        //下面生成应收总表的记录LJSPAY
        if (yelFlag) {
            //如果团单上有余额,并且余额足够支付团单下应交的所有费用
            if (yetFlag) {
                totalsumPay = 0;
            } else {
                totalsumPay = totalPay - leavingMoney;
            }
        } else {
            totalsumPay = totalPay;
        }
        LJSPaySchema tLJSPaySchema = new LJSPaySchema();
        tLJSPaySchema.setGetNoticeNo(tNo); // 通知书号
        tLJSPaySchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
        tLJSPaySchema.setOtherNoType("1");
        tLJSPaySchema.setAppntNo(mLCGrpContSchema.getAppntNo());
        tLJSPaySchema.setSumDuePayMoney(totalsumPay);
        tLJSPaySchema.setPayDate(strNewLJSPayDate); //有冲突,暂定为取集体险种最早的日期
        tLJSPaySchema.setStartPayDate(strNewLJSStartDate); //交费最早应缴日期保存上次交至日期/暂定为取集体险种最晚的交至日期
        tLJSPaySchema.setBankOnTheWayFlag("0");
        tLJSPaySchema.setBankSuccFlag("0");
        tLJSPaySchema.setSendBankCount(0);
        tLJSPaySchema.setApproveCode(mLCGrpPolSet.get(1).getApproveCode());
        tLJSPaySchema.setApproveDate(mLCGrpPolSet.get(1).getApproveDate());
        tLJSPaySchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
        tLJSPaySchema.setAccName(mAccName);
        tLJSPaySchema.setBankAccNo(mBankAccNo);
        tLJSPaySchema.setBankCode(mBankCode);
        tLJSPaySchema.setSerialNo(serNo);
        tLJSPaySchema.setOperator(Operator);
        tLJSPaySchema.setManageCom(mLCGrpContSchema.getManageCom());
        tLJSPaySchema.setAgentCom(mLCGrpContSchema.getAgentCom());
        tLJSPaySchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        tLJSPaySchema.setAgentType(mLCGrpContSchema.getAgentType());
        tLJSPaySchema.setAgentGroup(mLCGrpContSchema.getAgentGroup());
        tLJSPaySchema.setMakeDate(PubFun.getCurrentDate());
        tLJSPaySchema.setMakeTime(PubFun.getCurrentTime());
        tLJSPaySchema.setModifyDate(PubFun.getCurrentDate());
        tLJSPaySchema.setModifyTime(PubFun.getCurrentTime());

        String sql = "select LF_SaleChnl('2', '"+ mLCGrpContSchema.getGrpContNo()+"'),LF_MarketType('"+ mLCGrpContSchema.getGrpContNo()+"') from dual where 1=1";
        SSRS tSSRS = new SSRS();
        tSSRS = new ExeSQL().execSQL(sql);
        String tSaleChnl = tSSRS.GetText(1, 1);
        String tMarketType = tSSRS.GetText(1, 2);
        tLJSPaySchema.setSaleChnl(tSaleChnl);
        tLJSPaySchema.setMarketType(tMarketType);
        
        mLJSPaySchema.setSchema(tLJSPaySchema);
        //备份表
        LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
        tLJSPayBSchema.setGetNoticeNo(tNo); // 通知书号
        tLJSPayBSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
        tLJSPayBSchema.setOtherNoType("1");
        tLJSPayBSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
        tLJSPayBSchema.setSumDuePayMoney(totalsumPay);
        tLJSPayBSchema.setPayDate(strNewLJSPayDate); //有冲突,暂定为取集体险种最早的日期
        tLJSPayBSchema.setStartPayDate(strNewLJSStartDate); //交费最早应缴日期保存上次交至日期/暂定为取集体险种最晚的交至日期
        tLJSPayBSchema.setBankOnTheWayFlag("0");
        tLJSPayBSchema.setBankSuccFlag("0");
        tLJSPayBSchema.setSendBankCount(0);
        tLJSPayBSchema.setApproveCode(mLCGrpPolSet.get(1).getApproveCode());
        tLJSPayBSchema.setApproveDate(mLCGrpPolSet.get(1).getApproveDate());
        tLJSPayBSchema.setRiskCode(tLCGrpPolSchema.getRiskCode());
        tLJSPayBSchema.setAccName(mAccName);
        tLJSPayBSchema.setBankAccNo(mBankAccNo);
        tLJSPayBSchema.setBankCode(mBankCode);
        tLJSPayBSchema.setSerialNo(serNo);
        tLJSPayBSchema.setOperator(Operator);
        tLJSPayBSchema.setManageCom(mLCGrpContSchema.getManageCom());
        tLJSPayBSchema.setAgentCom(mLCGrpContSchema.getAgentCom());
        tLJSPayBSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        tLJSPayBSchema.setAgentType(mLCGrpContSchema.getAgentType());
        tLJSPayBSchema.setAgentGroup(mLCGrpContSchema.getAgentGroup());
        tLJSPayBSchema.setMakeDate(PubFun.getCurrentDate());
        tLJSPayBSchema.setMakeTime(PubFun.getCurrentTime());
        tLJSPayBSchema.setModifyDate(PubFun.getCurrentDate());
        tLJSPayBSchema.setModifyTime(PubFun.getCurrentTime());
        if(tLJSPayBSchema.getSumDuePayMoney()>0.01)
            tLJSPayBSchema.setDealState("0"); //催收状态：不成功
        else
            tLJSPayBSchema.setDealState("4"); //催收状态：不成功
        
        tLJSPayBSchema.setSaleChnl(tSaleChnl);
        tLJSPayBSchema.setMarketType(tMarketType);

        mLJSPayBSchema.setSchema(tLJSPayBSchema);

        // mLJSPaySet.add(tLJSPaySchema);
        //LOPRTManagerSchema tLOPRTManagerSchema=new LOPRTManagerSchema();
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
        mLOPRTManagerSchema.setOtherNoType("01");
        mLOPRTManagerSchema.setCode(PrintManagerBL.CODE_REPAY);
        mLOPRTManagerSchema.setManageCom(mLCGrpContSchema.getManageCom());
        mLOPRTManagerSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        mLOPRTManagerSchema.setReqCom(mLCGrpContSchema.getManageCom());
        mLOPRTManagerSchema.setReqOperator(mLCGrpContSchema.getOperator());
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        mLOPRTManagerSchema.setStandbyFlag2(tNo); //这里存放 催缴通知书号（也为交费收据号）

        return true;
    }

    /**
     * 计算缴费日期
     * @return boolean
     */
    private boolean calPayDate(LCGrpPolSet tLCGrpPolSetXQ, String payMode)
    {
        CalPayDateBL bl = new CalPayDateBL();
        HashMap dates = bl.calPayDateG(tLCGrpPolSetXQ, payMode);
        if(dates == null)
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        strNewLJSStartDate = (String) dates.get(CalPayDateBL.STARTPAYDATE);
        strNewLJSPayDate = (String) dates.get(CalPayDateBL.PAYDATE);
        if(strNewLJSStartDate == null || strNewLJSStartDate.equals("")
           || strNewLJSPayDate == null || strNewLJSPayDate.equals(""))
        {
            mErrors.addOneError("计算失败，没有得到缴费日期。");
            return false;
        }

        return true;
    }

    /**
     * 得到险种的最大交费次数
     * @param grpPolNo String
     * @return int
     */
    private int getGrpPayTimes(String grpPolNo) {
        StringBuffer sql = new StringBuffer();
        sql.append("select max(payCount) ")
                .append("from LJAPayGrp ")
                .append("where grpPolNo = '").append(grpPolNo).append("' ");
        String maxPayCount = new ExeSQL().getOneValue(sql.toString());
        if (maxPayCount.equals("") || maxPayCount.equals("null")) {
            return 0;
        }
        return Integer.parseInt(maxPayCount);
    }

    /**
     * 得到投保人帐户余额
     * @param appntNo String：投保人
     * @return double：帐户余额
     */
    private double getAccBala(String appntNo) {
        StringBuffer sql = new StringBuffer();
        sql.append(" select accGetMoney ")
                .append("from LCAppAcc ")
                .append("where customerNo = '").append(appntNo).append("' ");
        String accGetBala = new ExeSQL().getOneValue(sql.toString());
        if (accGetBala.equals("") || accGetBala.equals("null")) {
            return Double.MIN_VALUE;
        }
        return Double.parseDouble(accGetBala);
    }
    /**
     * 生成使用余额轨迹
     * @param yelLJSPayGrpBSchema LJSPayGrpBSchema：余额财务应收数据
     * @return boolean：处理成功true，否则false
     */
    private boolean dealAccBalaYET(LJSPayGrpBSchema yelLJSPayGrpBSchema) {
        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
        tLCAppAccTraceSchema.setCustomerNo(yelLJSPayGrpBSchema.getAppntNo());
        tLCAppAccTraceSchema.setOtherType("1"); //团单号
        tLCAppAccTraceSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
        tLCAppAccTraceSchema.setMoney(yelLJSPayGrpBSchema.getSumActuPayMoney());
        tLCAppAccTraceSchema.setOperator(tGI.Operator);

        AppAcc tAppAcc = new AppAcc();
        MMap tMMap = tAppAcc.accTakeOutXQYET(tLCAppAccTraceSchema);
        if(tMMap!=null){
        	//这里得到的是引用
        	LCAppAccSchema tLCAppAccSchema
        	= (LCAppAccSchema) tMMap.getObjectByObjectName("LCAppAccSchema",
        			0);
        	tLCAppAccTraceSchema = (LCAppAccTraceSchema) tMMap
        	.getObjectByObjectName("LCAppAccTraceSchema", 0);
        	//续期业务未结束，不能直接更新余额
        	tLCAppAccSchema.setAccBala(getAccBala(tLCAppAccSchema.getCustomerNo()));
        	tLCAppAccSchema.setState("0"); //续期核销时赋值为有效1
        	
        	tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala());
        	tLCAppAccTraceSchema.setState(tLCAppAccSchema.getState());
        	tLCAppAccTraceSchema.setBakNo(yelLJSPayGrpBSchema.getGetNoticeNo());
        	mapAcc.put(tLCAppAccTraceSchema, "INSERT");
//        mapAcc.put(tLCAppAccSchema, "UPDATE");
        }

        return true;
    }

    /**
     * 生成使用余额轨迹
     * @param yelLJSPayGrpBSchema LJSPayGrpBSchema：余额财务应收数据
     * @return boolean：处理成功true，否则false
     */
    private boolean dealAccBala(LJSPayGrpBSchema yelLJSPayGrpBSchema) {
        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
        tLCAppAccTraceSchema.setCustomerNo(yelLJSPayGrpBSchema.getAppntNo());
        tLCAppAccTraceSchema.setOtherType("1"); //团单号
        tLCAppAccTraceSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
        tLCAppAccTraceSchema.setMoney(yelLJSPayGrpBSchema.getSumActuPayMoney());
        tLCAppAccTraceSchema.setOperator(tGI.Operator);

        AppAcc tAppAcc = new AppAcc();
        MMap tMMap = tAppAcc.accTakeOutXQ(tLCAppAccTraceSchema);
        if(tMMap!=null){
        	//这里得到的是引用
        	LCAppAccSchema tLCAppAccSchema
        	= (LCAppAccSchema) tMMap.getObjectByObjectName("LCAppAccSchema",
        			0);
        	tLCAppAccTraceSchema = (LCAppAccTraceSchema) tMMap
        	.getObjectByObjectName("LCAppAccTraceSchema", 0);
        	//续期业务未结束，不能直接更新余额
        	tLCAppAccSchema.setAccBala(getAccBala(tLCAppAccSchema.getCustomerNo()));
        	tLCAppAccSchema.setState("0"); //续期核销时赋值为有效1
        	
        	tLCAppAccTraceSchema.setAccBala(tLCAppAccSchema.getAccBala());
        	tLCAppAccTraceSchema.setState(tLCAppAccSchema.getState());
        	tLCAppAccTraceSchema.setBakNo(yelLJSPayGrpBSchema.getGetNoticeNo());
        	mapAcc.put(tLCAppAccTraceSchema, "INSERT");
        	mapAcc.put(tLCAppAccSchema, "UPDATE");
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) {

        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mLCGrpContSchema = ((LCGrpContSchema) mInputData.getObjectByObjectName(
                "LCGrpContSchema", 0));
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        if (tGI == null || mLCGrpContSchema == null || mTransferData == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpDueFeeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        if (tLCGrpContDB.getInfo() == false) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpDueFeeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCGrpContSchema.setSchema(tLCGrpContDB.getSchema());
        return true;
    }

    //取得宽限期
    //输出：int 宽限期
    private int getDay() {
        String showPaymode = "";
        int showDay = 0;
        switch (Integer.parseInt(mLCGrpContSchema.getPayMode())) {
        case 1:
            showPaymode = "现金";
            showDay = 15;
            break;

        case 2:
            showPaymode = "现金支票";
            showDay = 15;
            break;
        case 3:
            showPaymode = "转帐支票";
            showDay = 7;
            break;

        case 4:
            showPaymode = "银行转账";
            showDay = 7;
            break;
        }
        return showDay;
    }

    /**
     * 加入到催收核销日志表数据
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealUrgeLog(String pmDealState, String pmReason,
                                String pmOpreat) {

        LCUrgeVerifyLogSchema tLCUrgeVerifyLogSchema = new
                LCUrgeVerifyLogSchema();
        //加到催收核销日志表
        if (pmOpreat.equals("INSERT")) {
            tLCUrgeVerifyLogSchema.setSerialNo(mserNo);
            tLCUrgeVerifyLogSchema.setRiskFlag("1");
            tLCUrgeVerifyLogSchema.setOperateType("1"); //1：续期催收操作,2：续期核销操作
            tLCUrgeVerifyLogSchema.setOperateFlag(mOperateFlag); //1：个案操作,2：批次操作
            tLCUrgeVerifyLogSchema.setOperator(tGI.Operator);
            tLCUrgeVerifyLogSchema.setDealState(pmDealState); //1 : 正在处理中,2 : 处理错误,3 : 处理已完成

            tLCUrgeVerifyLogSchema.setContNo(mLCGrpContSchema.getGrpContNo());
            tLCUrgeVerifyLogSchema.setMakeDate(CurrentDate);
            tLCUrgeVerifyLogSchema.setMakeTime(CurrentTime);
            tLCUrgeVerifyLogSchema.setModifyDate(CurrentDate);
            tLCUrgeVerifyLogSchema.setModifyTime(CurrentTime);
            StartDate = tLCUrgeVerifyLogSchema.getMakeDate();
            StartTime = tLCUrgeVerifyLogSchema.getMakeTime(); ;
            System.out.println("StartDate" + StartDate);
            System.out.println("StartTime" + StartTime);
            tLCUrgeVerifyLogSchema.setErrReason(pmReason);
        } else {
            LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new LCUrgeVerifyLogDB();
            LCUrgeVerifyLogSet tLCUrgeVerifyLogSet = new
                    LCUrgeVerifyLogSet();
            tLCUrgeVerifyLogDB.setSerialNo(mserNo);
            tLCUrgeVerifyLogDB.setOperateType("1");
            tLCUrgeVerifyLogDB.setOperateFlag(mOperateFlag);
            tLCUrgeVerifyLogDB.setRiskFlag("1");
            tLCUrgeVerifyLogSet = tLCUrgeVerifyLogDB.query();
            if (!(tLCUrgeVerifyLogSet == null) &&
                tLCUrgeVerifyLogSet.size() > 0) {
                tLCUrgeVerifyLogSchema = tLCUrgeVerifyLogSet.get(1);
                tLCUrgeVerifyLogSchema.setModifyDate(PubFun.getCurrentDate());
                tLCUrgeVerifyLogSchema.setModifyTime(PubFun.getCurrentTime());
                tLCUrgeVerifyLogSchema.setDealState(pmDealState);
                tLCUrgeVerifyLogSchema.setErrReason(pmReason);
            } else {
                return false;
            }

        }

        MMap tMap = new MMap();
        tMap.put(tLCUrgeVerifyLogSchema, pmOpreat);

        VData tInputData = new VData();
        tInputData.add(tMap);

        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tInputData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LCUrgeVerifyLog 表失败!");
            return false;
        }
        return true;
    }


    /**
     * 准备打印数据
     * @param tLCPolSchema
     * @return
     */
    public LOPRTManagerSchema getPrintData(LCGrpContSchema tLCGrpContSchema,
                                           String GetNoticeNo) {
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        try {
            String tLimit = PubFun.getNoLimit(tLCGrpContSchema.getManageCom());
            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
            tLOPRTManagerSchema.setOtherNo(tLCGrpContSchema.getGrpContNo());
            tLOPRTManagerSchema.setOtherNoType("01");
            tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_REPAY);
            tLOPRTManagerSchema.setManageCom(tLCGrpContSchema.getManageCom());
            tLOPRTManagerSchema.setAgentCode(tLCGrpContSchema.getAgentCode());
            tLOPRTManagerSchema.setReqCom(tLCGrpContSchema.getManageCom());
            tLOPRTManagerSchema.setReqOperator(tLCGrpContSchema.getOperator());
            tLOPRTManagerSchema.setPrtType("0");
            tLOPRTManagerSchema.setStateFlag("0");
            tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
            tLOPRTManagerSchema.setStandbyFlag2(GetNoticeNo);
        } catch (Exception ex) {
            return null;
        }
        return tLOPRTManagerSchema;
    }

    /**
     * 得到保单保障计划录入保费
     * @return HashMap：存储保障计划<->录入保费
     */
    private HashMap getContPlanPremInput(String getNoticeNo) {
        HashMap tHashMap = new HashMap();

        LCNoNamePremTraceDB tLCNoNamePremTraceDB = new LCNoNamePremTraceDB();
        tLCNoNamePremTraceDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        tLCNoNamePremTraceDB.setGetNoticeNo(mLCGrpContSchema.getGrpContNo()); //录入保费时GetNoTiceNo中存入保单号
        mLCNoNamePremTraceSet = tLCNoNamePremTraceDB.query();

        //若为录，则自动获得期交保费信息
        if (mLCNoNamePremTraceSet.size() == 0) {
            String sql = "  select contPlanCode, peoples2, "
                         + "   (select min(payToDate) from LCPol "
                         + "   where grpContNo = a.grpContNo "
                         + "      and contPlanCode = a.contPlanCode "
                         + "      and polTypeFlag = '1' and appFlag = '1' and (StateFlag is null or StateFlag = '1')), "
                         //保障计划保费
                         + "   (select sum(b.prem) "
                         + "   from LCPol b "
                         + "   where b.grpContNo = a.grpContNo "
                         + "       and b.appFlag = '1' and (b.StateFlag is null or b.StateFlag = '1')"
                         + "       and b.polTypeFlag = '1' "
                         + "       and b.contPlanCode = a.contPlanCode) "
                         + "from LCContPlan a "
                         + "where grpContNo = '"
                         + mLCGrpContSchema.getGrpContNo() + "' "
                         + "   and contPlanCode != '11' ";
            SSRS tSSRS = new ExeSQL().execSQL(sql);
            if (tSSRS.getMaxRow() == 0) {
                System.out.println("查询保障计划保费失败:"
                                   + tSSRS.mErrors.getErrContent());
                mErrors.addOneError("查询保障计划保费失败。");
                return null;
            }
            for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
                LCNoNamePremTraceSchema schema = new LCNoNamePremTraceSchema();
                schema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
                schema.setContPlanCode(tSSRS.GetText(i, 1));
                schema.setGetNoticeNo(getNoticeNo);
                schema.setPayToDate(tSSRS.GetText(i, 3));
                schema.setPrem(tSSRS.GetText(i, 4));
                schema.setPremInput(schema.getPrem());
                schema.setPeoples2(tSSRS.GetText(i, 2));
                schema.setPeoples2Input(schema.getPeoples2());
                schema.setInputDate(this.CurrentDate);
                schema.setInputTime(this.CurrentTime);
                schema.setInputOperator(tGI.Operator);
                schema.setRemark("操作员未录入期交保费和人数，自动生成。");
                schema.setOperator(schema.getInputOperator());
                PubFun.fillDefaultField(schema);
                mLCNoNamePremTraceSet.add(schema);
            }
        }

        for (int i = 1; i <= mLCNoNamePremTraceSet.size(); i++) {
            tHashMap.put(mLCNoNamePremTraceSet.get(i).getContPlanCode(),
                         String.valueOf(mLCNoNamePremTraceSet.get(i).
                                        getPremInput()));
            mLCNoNamePremTraceSet.get(i).setGetNoticeNo(getNoticeNo);
        }

        return tHashMap;
    }

    /**
     * 得到各保障计划下的保费项数据
     * @return HashMap:得到保单保障计划下交费项数据
     */
    private HashMap getcontPlanPremSet() {
        HashMap tHashMap = new HashMap();

        LCContPlanDB tLCContPlanDB = new LCContPlanDB();
        tLCContPlanDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
        LCContPlanSet set = tLCContPlanDB.query();

        StringBuffer sql = new StringBuffer();
        for (int i = 1; i <= set.size(); i++) {
            sql.delete(0, sql.length());
            sql.append(" select b.* ")
                    .append("from LCPol a, LCPrem b ")
                    .append("where a.polNo = b.polNo ")
                    .append("   and a.grpContNo = '")
                    .append(mLCGrpContSchema.getGrpContNo())
                    .append("' and a.poltypeflag = '1' and a.contPlanCode = '")
                    .append(set.get(i).getContPlanCode()).append("' ");
            LCPremSet tLCPremSet = new LCPremDB().executeQuery(sql.toString());
            tHashMap.put(set.get(i).getContPlanCode(), tLCPremSet);
        }

        return tHashMap;
    }

    /**
     * 得到各保障计划下的保费项数据
     * @return HashMap:得到保单保障计划下交费项数据
     */
    private HashMap getContPlanPrem() {
        HashMap tHashMap = new HashMap();

        StringBuffer sql = new StringBuffer();
        sql.delete(0, sql.length());
        sql.append(" select contPlanCode, sum(prem) ")
                .append("from LCPol ")
                .append("where grpContNo = '")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("' group by contPlanCode ");
        System.out.println(sql.toString());
        SSRS tSSRS = new ExeSQL().execSQL(sql.toString());
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
            tHashMap.put(tSSRS.GetText(i, 1), tSSRS.GetText(i, 2));
        }
        return tHashMap;
    }

    /**
     * 得到交费责任tLCPremSchema应收费
     * @param rate double:保费变更比例
     * @param tLCPolSchema LCPolSchema:险种
     * @param tLCPremSchema LCPremSchema:责任
     * @param contPlanPremSet HashMap:存储保障计划<->保费项
     * @param contPlanPremUsed HashMap:存储保障计划下已使用的保费
     * @return double:应收费
     */
    private double getMoney(LCPolSchema tLCPolSchema,
                            LCPremSchema tLCPremSchema,
                            HashMap contPlanPrem,
                            HashMap contPlanPremSet,
                            HashMap contPlanPremUsed,
                            HashMap contPlanPremInput) {
        double money = 0;

        if (!mINNC) {
            money = tLCPremSchema.getPrem() * (1 - tLCPremSchema.getFreeRate());
        } else {
            String contPlanCode = tLCPolSchema.getContPlanCode();
            //保障计划录入的保费
            double inputPrem
                    = Double.parseDouble((String) contPlanPremInput.get(
                            contPlanCode));
            //保障计划原保费
            double prem
                    = Double.parseDouble((String) contPlanPrem.get(contPlanCode));
            //保障计划已使用的录入保费和
            double usedPrem;
            String usedPremString = (String) contPlanPremUsed.get(contPlanCode);
            if (usedPremString == null || usedPremString.equals("")) {
                usedPrem = 0;
            } else {
                usedPrem = Double.parseDouble(usedPremString);
            }
            //这里用的是引用
            LCPremSet tLCPremSet = (LCPremSet) contPlanPremSet
                                   .get(contPlanCode);
            if (tLCPremSet == null) {
                return 0;
            }
            //若只剩最后一个责任，则应收保费=录入保费-已使用保费
            if (tLCPremSet.size() == 1) {
                money = PubFun.setPrecision(inputPrem - usedPrem, "0.00");
            } else {
                //应收保费=保障记录录入保费 * (责任保费/保障计划保费)
                money = PubFun.setPrecision(
                        inputPrem * tLCPremSchema.getPrem() / prem, "0.00");
                usedPrem += money;
                contPlanPremUsed.put(contPlanCode, String.valueOf(usedPrem));
                //去除处理过的责任
                for (int i = 1; i <= tLCPremSet.size(); i++) {
                    if (tLCPremSet.get(i).getPolNo()
                        .equals(tLCPremSchema.getPolNo())
                        && tLCPremSet.get(i).getDutyCode()
                        .equals(tLCPremSchema.getDutyCode())
                        && tLCPremSet.get(i).getPayPlanCode()
                        .equals(tLCPremSchema.getPayPlanCode())) {
                        tLCPremSet.removeRange(i, i);
                    }
                }
            }
        }
        return money;
    }
    
    //20081204 zhanggm 同步LJSPayGrpB表的DealState状态。
    //当保费从LCAppAcc表抵充、SumActuPayMoney==0时,DealState应该为4而不是0,同LJSPayB是一样的
    private void updateDealState()
    {
    	String sql = "update LJSPayGrpB set DealState = '" + mLJSPayBSchema.getDealState() + "' "
    	           + "where GetNoticeNo = '" + mLJSPayBSchema.getGetNoticeNo() + "' ";
    	System.out.println(sql);
    	map.put(sql, "UPDATE");
    }
    
    private boolean lockGrpCont()
    {
    	MMap tCekMap = null;
    	tCekMap = lockBalaCount(mLCGrpContSchema.getGrpContNo());
        if (tCekMap == null)
        {
            return false;
        }
        map.add(tCekMap);
        return true;

    }
    /**
     * 锁定动作
     * @param aGrpContNo
     * @return MMap
     */
    private MMap lockBalaCount(String aGrpContNo)
    {
        MMap tMMap = null;
        /**续期催收"XQ"*/
        String tLockNoType = "XQ";
        /**锁定有效时间（秒）*/
        String tAIS = "600";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", aGrpContNo);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }
        return tMMap;
    }

    
}
