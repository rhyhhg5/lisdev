package com.sinosoft.lis.operfee;

import java.util.HashMap;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCGetDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LJSGetDB;
import com.sinosoft.lis.db.LMDutyGetAliveDB;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LGWorkSchema;
import com.sinosoft.lis.schema.LJSGetDrawSchema;
import com.sinosoft.lis.schema.LJSGetSchema;
import com.sinosoft.lis.schema.LMDutyGetAliveSchema;
import com.sinosoft.lis.schema.LPEdorEspecialDataSchema;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.vschema.LJSGetDrawSet;
import com.sinosoft.lis.vschema.LMDutyGetAliveSet;
import com.sinosoft.task.Task;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

//程序名称：ExpirBenefitCalBL.java
//程序功能：重算忠诚奖
//创建日期：2010-06-10 16:06
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

public class ExpirBenefitCalBL {

    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData saveData = new VData();
    private MMap mMMap = new MMap();
    private GlobalInput tGI = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String mserNo = "1"; //批次号
    
    private String mFormula = null;
    
    private String mRateFlag = null;
    
    private String mGetNoticeNo = null;

    private LJSGetSchema mLJSGetSchema = new LJSGetSchema();; //通知书号

    private String mPrem  = "" ;

    private Reflections ref = new Reflections();

    //个人保单表
    private LCContSchema mLCContSchema = new LCContSchema();

    //应付个人明细表
    private LJSGetDrawSet tLJSGetDrawSet = new LJSGetDrawSet();


    private String anniversary = null ; //抽档当年对应的保单周年日
    
    //20110512添加修改意见和金额合计add by xp
    private String remark = null ; //修改意见
    private String ssallmoney = null ; //之前试算的总金额
    private String mFormulaBefore = null ; //页面的公式代码
    private String mContNo = null ; //页面的公式代码

    public ExpirBenefitCalBL() {
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public boolean submitData(VData cInputData, String cOperate) {
    	
    	System.out.println("in ExpirBenefitCalBL----------------");

        VData data = getSubmitData(cInputData, cOperate);
        if (data == null) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(data, "") == false) {
            this.mErrors.addOneError("PubSubmit:生成给付失败!");
            return false;
        }
        return true;
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public VData getSubmitData(VData cInputData, String cOperate) 
    {
        if (!getInputData(cInputData)) 
        {
            return null;
        }
        System.out.println("After ExpirBenefitCalBL getInputData");
        
        if (!checkData()) 
        {
            return null;
        }
        System.out.println("After ExpirBenefitCalBL checkData");
        
        //进行业务处理
        if (!dealData()) 
        {
            return null;
        }
        System.out.println("After ExpirBenefitCalBL dealData");
        
        return saveData;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) 
    {
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        
        mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0); 
        
        if ((tGI == null) || mTransferData == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitCalBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        String tContNo = (String)mTransferData.getValueByName("contno");
        mContNo = (String)mTransferData.getValueByName("contno");
        
        mFormula = (String)mTransferData.getValueByName("formula");
                
        mRateFlag = (String)mTransferData.getValueByName("rateflag");
        
        mGetNoticeNo = (String)mTransferData.getValueByName("getnoticeno");
        
        if (tContNo == null || tContNo == "" || tContNo.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitCalBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到ContNo，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        if (mRateFlag == null || mRateFlag == "" || mRateFlag.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitCalBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到mRateFlag，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        if (mFormula == null || mFormula == "" || mFormula.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitCalBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到mFormula，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        if (mGetNoticeNo == null || mGetNoticeNo == "" || mGetNoticeNo.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitCalBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到mGetNoticeNo，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        //20110512添加修改意见和金额合计add by xp
        remark = (String)mTransferData.getValueByName("remark");
        ssallmoney = (String)mTransferData.getValueByName("ssallmoney");
        mFormulaBefore=mFormula;
        if (remark == null || remark == "" || remark.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitCalBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "修改意见不能为空!";
            this.mErrors.addOneError(tError);

            return false;
        }
        if (ssallmoney == null || ssallmoney == "" || ssallmoney.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitCalBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "试算的给付总金额为空!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        
        
        LJSGetDB tLJSGetDB = new LJSGetDB();
        tLJSGetDB.setGetNoticeNo(mGetNoticeNo);
        if(!tLJSGetDB.getInfo())
        {
//        	 @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitCalBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据LJSGet，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        mLJSGetSchema.setSchema(tLJSGetDB.getSchema()); 
        if(mFormula.equals("2"))
        {
        	mFormula = "B";
        }
        else if(mFormula.equals("3"))
        {
        	mFormula = "C";
        }
        if(mFormula.equals("4"))
        {
        	mFormula = "D";
        }
        else if(mFormula.equals("5"))
        {
        	mFormula = "E";
        }
        if(mFormula.equals("6"))
        {
        	mFormula = "F";
        }
        else if(mFormula.equals("7"))
        {
        	mFormula = "G";
        }
        		
        
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tContNo);
        if (!tLCContDB.getInfo()) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitCalBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据LCCont，请您确认!";
            this.mErrors.addOneError(tError);
            return false;

        }
        mLCContSchema.setSchema(tLCContDB.getSchema());
        return true;
    }

    /**
     *  数据校验
     */
    private boolean checkData() 
    {
        return true;
    }

    /**
     * 业务处理
     */
    private boolean dealData() 
    {
    	if(!createTask()){
    		return false;
    	}
        if(!doBenefit())
        {
            return false ;
        }

        return true;
    }

    private boolean doBenefit() 
    {
        Calculator tCalculator = new Calculator();
        mserNo = mLJSGetSchema.getSerialNo();

        //取得被保人信息
        String sql = "select distinct insuredno from lcget a,lmdutygetalive b "
                     + " where a.getdutycode = b.getdutycode and contno ='"
                     + mLCContSchema.getContNo() + "'";
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        HashMap t = new HashMap();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) 
        {
            LCGetSet tLCGetSet = getLCGetNeedGet(mLCContSchema.getContNo(),tSSRS.GetText(i, 1));

            if (tLCGetSet == null) 
            {
                continue;
            }
            
            double sumGetMoney = 0; //总领取金额
            //循环每个给付责任进行处理
            tLJSGetDrawSet.clear();
            for (int j = 1; j <= tLCGetSet.size(); j++) {

                //获取合同下的每条给付的明细信息
                LCGetSchema tLCGetSchema = tLCGetSet.get(j).getSchema();

                LMDutyGetAliveDB tLMDutyGetaLiveDB = new LMDutyGetAliveDB();

                tLMDutyGetaLiveDB.setGetDutyCode(tLCGetSchema.getGetDutyCode());
                LMDutyGetAliveSet tLMDutyGetAliveSet = tLMDutyGetaLiveDB.query();
                LMDutyGetAliveSchema tLMDutyGetAliveSchema = tLMDutyGetAliveSet.get(1);
                String pCalCode = "";
                if (tLMDutyGetAliveSchema.getCalCode() != null &&
                    !"".equals(tLMDutyGetAliveSchema.getCalCode())) {
                    pCalCode = tLMDutyGetAliveSchema.getCalCode();
                }
                if (tLMDutyGetAliveSchema.getCnterCalCode() != null &&
                    !"".equals(tLMDutyGetAliveSchema.getCnterCalCode())) {
                    pCalCode = tLMDutyGetAliveSchema.getCnterCalCode();
                }
                if (tLMDutyGetAliveSchema.getOthCalCode() != null &&
                    !"".equals(tLMDutyGetAliveSchema.getOthCalCode())) {
                    pCalCode = tLMDutyGetAliveSchema.getOthCalCode();
                }

                tCalculator.setCalCode(pCalCode);

                //责任保额
                String amnt = "select sum(amnt) from lcduty where polno='"
                              + tLCGetSchema.getPolNo() + "' and dutycode='"
                              + tLCGetSchema.getDutyCode() + "'"
                              ;


                if (tLMDutyGetAliveSchema.getGetDutyKind().equals("12"))
                {
                    String kindsql =" select d.* From lmdutygetclm a, lmriskduty b ,lmdutygetrela c ,llclaimdetail d "
                                   +" where a.getdutycode = c.getdutycode and b.dutycode = c.dutycode "
                                   +" and d.getdutycode = c.getdutycode "
                                   +" and d.polno ='"+tLCGetSchema.getPolNo()+"' "
                                   +" and afterget ='003' "
                                   ;
                    SSRS kindSSRS = new ExeSQL().execSQL(kindsql);
                    if (kindSSRS.getMaxRow()>0)
                    {
                        CError tError = new CError();
                        tError.moduleName = "ExpirBenefitCalBL";
                        tError.functionName = "getInputData";
                        tError.errorMessage = "责任已经理赔，不能做满期给付！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }

                //判断主险是否出险,满期给付责任在主险。
                if (getPolAttribute(tLCGetSchema.getPolNo())
                        && tLMDutyGetAliveSchema.getDiscntFlag().equals("0"))
                {
                    //得到该险种的附加险种
                   String annexPolNoSQL = "select code From ldcode1 where code1 =(select riskcode from lcpol where polno ='"+tLCGetSchema.getPolNo()+"')  and codetype='mainsubriskrela' " ;
                   String annexRisk = new ExeSQL().getOneValue(annexPolNoSQL);
                   String annexPolNo ="select polno from lcpol where contno='"+tLCGetSchema.getContNo()+"' and riskcode ='"+annexRisk+"'";
                   if (annexRisk.equals("") && annexRisk =="")
                   {
                       CError tError = new CError();
                       tError.moduleName = "ExpirBenefitCalBL";
                       tError.functionName = "getInputData";
                       tError.errorMessage = "没有得到附加险信息！";
                       this.mErrors.addOneError(tError);
                   }
                   //1."如果附加的有LP就不做给付";
                   String sqlcliam = "select polNo From ljagetclaim where polno ='"+new ExeSQL().getOneValue(annexPolNo)+"'";
                   String cliam = new ExeSQL().getOneValue(sqlcliam);
                   if ( !cliam.equals("") && cliam !="" && cliam !="null" )
                   {
                       CError tError = new CError();
                       tError.moduleName = "ExpirBenefitCalBL";
                       tError.functionName = "getInputData";
                       tError.errorMessage = "附加险已经出险，不能做满期给付！";
                       this.mErrors.addOneError(tError);
                       return false ;
                   }
                   //2."主险出现了并且是死亡责任的不做给付"
                   String mainSQL = "select polNo From ljagetclaim where polno ='"+tLCGetSchema.getPolNo()+"'"
                                  + " and getdutykind in('501','502','503','504') " ;
                   String main = new ExeSQL().getOneValue(mainSQL);
                   if (!main.equals("") && main !="null")
                   {
                       CError tError = new CError();
                       tError.moduleName = "ExpirBenefitCalBL";
                       tError.functionName = "getInputData";
                       tError.errorMessage = "主险责任出现理赔，并且是死亡责任，不能满期给付！";
                       this.mErrors.addOneError(tError);
                       return false;
                   }
                  //3.如果满期金方式的责任出险，不做给付。
                  if (tLMDutyGetAliveSchema.getGetDutyKind().equals("0"))
                  {
                      String SQL = "select polNo From ljagetclaim where polno ='"+tLCGetSchema.getPolNo()+"'";
                      String polSLQ = new ExeSQL().getOneValue(SQL);
                      if (!polSLQ.equals("") && polSLQ !="null")
                      {
                          CError tError = new CError();
                          tError.moduleName = "ExpirBenefitCalBL";
                          tError.functionName = "getInputData";
                          tError.errorMessage = "满期责任已经出险，不做给付！";
                          this.mErrors.addOneError(tError);
                          return false;
                      }
                  }
                  //给付合计保费。
                  String polSQL = " select code,code1 From ldcode1 where  code1 in(select riskcode from lcpol where polno ='" +tLCGetSchema.getPolNo() +"')  and codetype='mainsubriskrela' "
                                  + " union "
                                  + " select code,code1 From ldcode1 where  code  in(select riskcode from lcpol where polno ='" +tLCGetSchema.getPolNo() +"')  and codetype='mainsubriskrela' "
                                  ;
                  SSRS xSSRS = new ExeSQL().execSQL(polSQL);
                  mPrem = new ExeSQL().getOneValue("select sum(prem) From lcpol where InsuredNo='" +tLCGetSchema.getInsuredNo() + "' and riskcode in('" +xSSRS.GetText(1, 1) + "','" + xSSRS.GetText(1, 2) +"') and contno ='" + tLCGetSchema.getContNo() + "'");
                }
                //满期责任在附加险，如果主险出险责任终止，不做满期给付。
                if (tLMDutyGetAliveSchema.getDiscntFlag().equals("3")
                      && !getPolAttribute(tLCGetSchema.getPolNo()))
                {
                    //得到该责任对应的主险，查看是否有LP，如果有理赔就不做满期给付。
                    //
                    String SQL =" select c.polno From lcpol c,ljagetclaim d where c.riskcode in( "
                               +" select code1 From ldcode1  where code in( "
                               +" select distinct riskcode From lcpol a,lcget b where a.polno = b.polno "
                               +" and a.polno ='"+tLCGetSchema.getPolNo()+"')"
                               +" and code1 = c.riskcode  and codetype='mainsubriskrela' ) "
                               +" and c.contno ='"+tLCGetSchema.getContNo()+"'"
                               +" and c.polno = d.polno "
                               +" union "
                               +" select polno from ljagetclaim where polno = '"+tLCGetSchema.getPolNo()+"'"
                               +" with ur "
                               ;
                   tSSRS = new ExeSQL().execSQL(SQL);
                   if (tSSRS.getMaxRow() != 0)
                   {
                       CError tError = new CError();
                       tError.moduleName = "ExpirBenefitCalBL";
                       tError.functionName = "getInputData";
                       tError.errorMessage = "主险责任已经出现，不做满期给付！";
                       this.mErrors.addOneError(tError);
                       return false;
                   }
                   String polSQL = " select code,code1 From ldcode1 where  code1 in(select riskcode from lcpol where polno ='"+tLCGetSchema.getPolNo()+"')  and codetype='mainsubriskrela' "
                                   + " union "
                                   + " select code,code1 From ldcode1 where  code  in(select riskcode from lcpol where polno ='"+tLCGetSchema.getPolNo()+"')  and codetype='mainsubriskrela' "
                                   ;
                   SSRS xSSRS= new ExeSQL().execSQL(polSQL);
                   mPrem = new ExeSQL().getOneValue("select sum(prem) From lcpol where InsuredNo='"+tLCGetSchema.getInsuredNo()+"' and riskcode in('"+xSSRS.GetText(1,1)+"','"+xSSRS.GetText(1,2)+"') and contno ='"+tLCGetSchema.getContNo()+"'");
                }
                //附加险出险，赔合计保费。
                if (tLMDutyGetAliveSchema.getDiscntFlag().equals("1"))
                {
                    //如果主险出险，不做满期给付。
                    if (getPolAttribute(tLCGetSchema.getPolNo())) {
                        String SQL  = " select * From lcpol c,ljagetclaim d where c.riskcode in( "
                                    + " select code1 From ldcode1  where code in( "
                                    +" select distinct riskcode From lcpol a,lcget b where a.polno = b.polno "
                                    + " and a.polno ='" + tLCGetSchema.getPolNo() +"')"
                                    + " and code1 = c.riskcode  and codetype='mainsubriskrela' ) "
                                    + " and c.contno ='" +tLCGetSchema.getContNo() + "'"
                                    + " and c.polno = d.polno with ur "
                                ;
                        tSSRS = new ExeSQL().execSQL(SQL);

                        if (tSSRS.getMaxRow() != 0) {
                            CError tError = new CError();
                            tError.moduleName = "ExpirBenefitCalBL";
                            tError.functionName = "getInputData";
                            tError.errorMessage = "主险责任已经出现，不做满期给付！";
                            this.mErrors.addOneError(tError);
                            return false;
                        }

                    }
                    //得到责任对应的，主副险险种。
                     String polSQL = " select code,code1 From ldcode1 where  code1 in(select riskcode from lcpol where polno ='"+tLCGetSchema.getPolNo()+"')  and codetype='mainsubriskrela' "
                                   + " union "
                                   + " select code,code1 From ldcode1 where  code  in(select riskcode from lcpol where polno ='"+tLCGetSchema.getPolNo()+"')  and codetype='mainsubriskrela' "
                                   ;
                     SSRS xSSRS= new ExeSQL().execSQL(polSQL);
                     mPrem = new ExeSQL().getOneValue("select sum(prem) From lcpol where InsuredNo='"+tLCGetSchema.getInsuredNo()+"' and riskcode in('"+xSSRS.GetText(1,1)+"','"+xSSRS.GetText(1,2)+"') and contno ='"+tLCGetSchema.getContNo()+"'");
                }

                //附加险出现，赔付主险保费。
                if (tLMDutyGetAliveSchema.getDiscntFlag().equals("2"))
                {
                    //如果主险出险，不做满期给付。
                    if (getPolAttribute(tLCGetSchema.getPolNo()))
                    {
                        String sqlcliam = "select polno From ljagetclaim where polno ='"+tLCGetSchema.getPolNo()+"'";
                        tSSRS = new ExeSQL().execSQL(sqlcliam);
                        if (tSSRS.getMaxRow() != 0)
                        {
                            CError tError = new CError();
                            tError.moduleName = "ExpirBenefitCalBL";
                            tError.functionName = "getInputData";
                            tError.errorMessage = "主险责任已经出现，不做满期给付！";
                            this.mErrors.addOneError(tError);
                            return false;
                        }
                    }
                    //如果附加险，出险给付主险保费
                    else if (!getPolAttribute(tLCGetSchema.getPolNo()))
                    {
                        String sqlcliam = "select polno From ljagetclaim where polno ='"+tLCGetSchema.getPolNo()+"'";
                        tSSRS = new ExeSQL().execSQL(sqlcliam);
                        if (tSSRS.getMaxRow() != 0)
                        {
                            String SQL = " select  a.polno,prem From lcget a,lcpol b ,lmriskapp c where a.polno  = b.polno "
                                         + " and c.subriskflag ='M' "
                                         + " and b.riskcode = c.riskcode "
                                         + " and a.polno ='" + tLCGetSchema.getPolNo() + "' "
                                         + " group by a.polno,prem "
                                         ;
                            tSSRS = new ExeSQL().execSQL(SQL);
                            mPrem = tSSRS.GetText(1,2);
                        }
                    }
                    //否则给付合计保费
                    else
                    {
                        //得到责任对应的，主副险险种。
                        String polSQL = " select code,code1 From ldcode1 where  code1 in(select riskcode from lcpol where polno ='"
                                        +tLCGetSchema.getPolNo() +"')  and codetype='mainsubriskrela' "
                                        + " union "
                                        + " select code,code1 From ldcode1 where  code  in(select riskcode from lcpol where polno ='"
                                        + tLCGetSchema.getPolNo() +"')  and codetype='mainsubriskrela' "
                                        ;
                        SSRS xSSRS = new ExeSQL().execSQL(polSQL);
                        mPrem = new ExeSQL().getOneValue("select sum(prem) From lcpol where InsuredNo='" +tLCGetSchema.getInsuredNo() +"' and riskcode in('" + xSSRS.GetText(1, 1) +"','" + xSSRS.GetText(1, 2) +"') and contno ='" + tLCGetSchema.getContNo() +"'");
                    }
                }
                //主险出险，不赔付主附险保费、保额



                tCalculator.addBasicFactor("Amnt",new ExeSQL().getOneValue(amnt));
                tCalculator.addBasicFactor("Get",CommonBL.bigDoubleToCommonString(tLCGetSchema.getActuGet(), "0.00"));
                tCalculator.addBasicFactor("PolNo", tLCGetSchema.getPolNo());
                tCalculator.addBasicFactor("Prem",mPrem);

                String tStr = tCalculator.calculate();
                double tValue = 0;

                if (tStr == null || tStr.trim().equals("")) {
                    tValue = 0;
                } else {
                    tValue = Double.parseDouble(tStr);
                }
                sumGetMoney += tValue;

                //全无忧产品
                if (tLMDutyGetAliveSchema.getDiscntFlag().equals("9")) {
                    
                	//modify by xp 090515 解决第一次抽档getdate也会设置当年保单周年日的问题.
                	String NewGetDate=new ExeSQL().getOneValue("select getdate+1 year from ljsgetdraw where dutycode='"+tLCGetSchema.getDutyCode()+"' and contno ='"+tLCGetSchema.getContNo()+"' order by getdate desc fetch first 1 row only with ur");
                	if (NewGetDate.equals("")||NewGetDate=="")//第一次抽档
                	{                	
                		anniversary = new ExeSQL().getOneValue("select  (case when substr(varchar(a.cvalidate), 5, 6) > substr(varchar(a.insuredbirthday), 5, 6)	then substr(varchar(a.insuredbirthday + 60 year), 1, 4) ||substr(varchar(a.cvalidate), 5, 6) else substr(varchar(a.insuredbirthday + 61 year), 1, 4)||substr(varchar(a.cvalidate), 5, 6) end) from lcpol a,lcget b , lmdutygetalive c where a.conttype ='1' and a.appflag ='1' and a.polno = b.polno and b.getdutycode = c.getdutycode and a.insuredno = b.insuredno and c.DiscntFlag='9'  and a.contno ='"+tLCGetSchema.getContNo()+"'" );
                	}
                	else
                	{
                		anniversary=NewGetDate;
                	}
                    
                    String abs = new ExeSQL().getOneValue(
                            "select sum(getmoney) from ljsgetdraw where ContNo ='" +
                            tLCGetSchema.getContNo() + "' and insuredno ='" +
                            tLCGetSchema.getInsuredNo() + "'");
                    double XQMoney = 0;
                    if (abs.equals("")||abs=="")
                    {
                        XQMoney = 0 ;
                    }else{
                        XQMoney = Double.parseDouble(abs);
                    }

                    double LPMoney = 0; //理赔部分尚未开发。
                    if (XQMoney + LPMoney + sumGetMoney > 2 * Double.parseDouble(new ExeSQL().getOneValue(amnt))) {
                        CError tError = new CError();
                        tError.moduleName = "ExpirBenefitCalBL";
                        tError.functionName = "getInputData";
                        tError.errorMessage = "长期护理保险金和老年护理保险金的累计给付总额不能超过保险金额的2倍，给付抽档失败！";
                        this.mErrors.addOneError(tError);
                        return false;
                    }
                }


                LJSGetDrawSchema tLJSGetDrawSchema = new LJSGetDrawSchema();

                tLJSGetDrawSchema.setGetNoticeNo(mLJSGetSchema.getGetNoticeNo());
                tLJSGetDrawSchema.setPolNo(tLCGetSchema.getPolNo());
                tLJSGetDrawSchema.setDutyCode(tLCGetSchema.getDutyCode());
                tLJSGetDrawSchema.setGetDutyKind(tLCGetSchema.getGetDutyKind());
                tLJSGetDrawSchema.setGetDutyCode(tLCGetSchema.getGetDutyCode());
                tLJSGetDrawSchema.setContNo(tLCGetSchema.getContNo());
                tLJSGetDrawSchema.setAppntNo(mLCContSchema.getAppntNo());
                tLJSGetDrawSchema.setInsuredNo(tLCGetSchema.getInsuredNo());
                if (tLMDutyGetAliveSchema.getDiscntFlag().equals("9")) {
                    tLJSGetDrawSchema.setGetDate(anniversary);
                }else
                {
                    tLJSGetDrawSchema.setGetDate(CurrentDate);
                }
                tLJSGetDrawSchema.setGetMoney(tValue);
                tLJSGetDrawSchema.setEnterAccDate(CurrentDate);
                tLJSGetDrawSchema.setFeeOperationType("EB");
                tLJSGetDrawSchema.setFeeFinaType("TF");
                tLJSGetDrawSchema.setManageCom(mLCContSchema.getManageCom());
                tLJSGetDrawSchema.setAgentCom(mLCContSchema.getAgentCom());
                tLJSGetDrawSchema.setAgentType(mLCContSchema.getAgentType());
                tLJSGetDrawSchema.setAgentCode(mLCContSchema.getAgentCode());
                tLJSGetDrawSchema.setAgentGroup(mLCContSchema.getAgentGroup());
                LCPolDB tLCPolDB = new LCPolDB();
                tLCPolDB.setPolNo(tLCGetSchema.getPolNo());
                tLCPolDB.getInfo();
                tLJSGetDrawSchema.setRiskCode(tLCPolDB.getRiskCode());
                if (tLMDutyGetAliveSchema.getGetDutyKind().equals("0")) {
                    tLJSGetDrawSchema.setLastGettoDate(tLCPolDB.getEndDate());
                    tLJSGetDrawSchema.setCurGetToDate(tLCPolDB.getEndDate());
                }
                else if (!tLMDutyGetAliveSchema.getGetDutyKind().equals("0")) {
                    String query =
                            " Select max(CurGetToDate) from ljsgetdraw a,ljsget b where "
                            +
                            " a.getnoticeno = b.getnoticeno and b.dealstate <>'2' and a.polno='"
                            + tLCGetSchema.getPolNo() + "' and getdutycode ='"
                            + tLCGetSchema.getGetDutyCode() +
                            "' and insuredno ='"
                            + tLCGetSchema.getInsuredNo() + "'";
                    SSRS getToDateSSRS = new ExeSQL().execSQL(query);
                    if ((!getToDateSSRS.GetText(1, 1).equals("") &&
                         getToDateSSRS.GetText(1, 1) != null) &&
                        getToDateSSRS.getMaxRow() > 0) {
                        tLJSGetDrawSchema.setLastGettoDate(getToDateSSRS.GetText(1, 1));
                        tLJSGetDrawSchema.setCurGetToDate(PubFun.calDate(getToDateSSRS.GetText(1, 1),Integer.parseInt(tLMDutyGetAliveSchema.getGetDutyKind()), "M", null));

                    } else { //第一次给付
                    //咨询产品StartDateCalRef为S则为按投保日期进行计算,对应GetStartUnit为Y;
                    //StartDateCalRef为B则为按被保人出生日期进行计算,对应GetStartUnit为A;
                        if ((tLMDutyGetAliveSchema.getStartDateCalRef().equals("S"))
                        			&&(tLMDutyGetAliveSchema.getGetStartUnit().equals("Y"))){
                                tLJSGetDrawSchema.setLastGettoDate(PubFun.
                                        calDate(
                                                tLCPolDB.getCValiDate(),
                                                tLMDutyGetAliveSchema.
                                                getGetStartPeriod(),
                                                tLMDutyGetAliveSchema.
                                                getGetStartUnit(), null));
                                tLJSGetDrawSchema.setCurGetToDate(PubFun.
                                        calDate(
                                                tLCPolDB.getCValiDate(),
                                                tLMDutyGetAliveSchema.
                                                getGetStartPeriod(),
                                                tLMDutyGetAliveSchema.
                                                getGetStartUnit(), null));
                            

                        } else if ((tLMDutyGetAliveSchema.getStartDateCalRef().equals("B")) 
                        		&&(tLMDutyGetAliveSchema.getGetStartUnit().equals("A"))) {
                                tLJSGetDrawSchema.setLastGettoDate(tLCGetSchema.getGettoDate());
                                System.out.println("LastGettoDate:"+PubFun.calDate(tLCPolDB.getInsuredBirthday(),tLMDutyGetAliveSchema.getGetStartPeriod(), "Y", null));
                                tLJSGetDrawSchema.setCurGetToDate(PubFun.
                                        calDate(
                                                tLCGetSchema.getGettoDate(),
                                                Integer.parseInt(tLMDutyGetAliveSchema.getGetDutyKind()), "M", null));
                                System.out.println("CurGetToDate:"+PubFun.calDate(tLCGetSchema.getGettoDate(),Integer.parseInt(tLMDutyGetAliveSchema.getGetDutyKind()), "Y", null));
                            
                        }
                    }
                }
                tLJSGetDrawSchema.setSerialNo(mserNo);
                tLJSGetDrawSchema.setOperator(tGI.Operator);
                tLJSGetDrawSchema.setMakeDate(CurrentDate);
                tLJSGetDrawSchema.setMakeTime(CurrentTime);
                tLJSGetDrawSchema.setModifyDate(CurrentDate);
                tLJSGetDrawSchema.setModifyTime(CurrentTime);
                this.tLJSGetDrawSet.add(tLJSGetDrawSchema);

//              计算其它奖金
                LDCodeDB tLDCodeDB = new LDCodeDB();
                tLDCodeDB.setCodeType("RiskGetAlive");
                tLDCodeDB.setCode(tLCPolDB.getRiskCode());
                if (tLDCodeDB.getInfo()) 
                {
                    if (!t.containsKey(tLCPolDB.getPolNo())) 
                    {
                        t.put(tLCPolDB.getPolNo(), "");
                        if(tLDCodeDB.getCodeAlias()==null || tLDCodeDB.getCodeAlias().equals(""))//特殊险种特殊算
                        {
                        	if(tLCPolDB.getRiskCode().equals("330501"))//常无忧B
                        	{
                        		Cal330501BonusEasy tCal330501BonusEasy = new Cal330501BonusEasy(tLCPolDB.getSchema());
                        		HashMap tHashMap = tCal330501BonusEasy.getBonus(mFormula,mRateFlag); 
                        		if (tCal330501BonusEasy.getError() == null) 
                        		{
                        			String initMoney = (String)tHashMap.get("InitMoney"); //初始忠诚奖
                        			String addMoney = (String)tHashMap.get("AddMoney"); //外加忠诚奖
                        			
                        			LJSGetDrawSchema initLJSGetDrawSchema = new LJSGetDrawSchema();
                        			initLJSGetDrawSchema.setSchema(tLJSGetDrawSchema);
                        			initLJSGetDrawSchema.setDutyCode("000000");
                        			initLJSGetDrawSchema.setGetMoney(initMoney);
                                    this.tLJSGetDrawSet.add(initLJSGetDrawSchema);
                                    sumGetMoney += Double.parseDouble(initMoney);
                                    
                                    LJSGetDrawSchema addLJSGetDrawSchema = new LJSGetDrawSchema();
                                    addLJSGetDrawSchema.setSchema(tLJSGetDrawSchema);
                                    addLJSGetDrawSchema.setDutyCode("000001");
                                    addLJSGetDrawSchema.setGetMoney(addMoney);
                                    this.tLJSGetDrawSet.add(addLJSGetDrawSchema);
                                    sumGetMoney += Double.parseDouble(addMoney);
                        		}
                        	}
                        }
                        else
                        {
                        	try {
                                Class tClass = Class.forName(tLDCodeDB.getCodeAlias());
                                CalOther tCalOther = (CalOther) tClass.newInstance();
                                double otherMoney = tCalOther.cal(tLCPolDB.getPolNo());
                                if (tCalOther.getError() == null) {
                                    LJSGetDrawSchema ttLJSGetDrawSchema = new
                                            LJSGetDrawSchema();
                                    ttLJSGetDrawSchema.setSchema(tLJSGetDrawSchema);
                                    ttLJSGetDrawSchema.setDutyCode("000000");
                                    ttLJSGetDrawSchema.setGetMoney(otherMoney);
                                    this.tLJSGetDrawSet.add(ttLJSGetDrawSchema);
                                    sumGetMoney += otherMoney;
                                } else {
                                    mErrors.copyAllErrors(tCalOther.getError());
                                    return false;
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                CError.buildErr(this,
                                                tLCPolDB.getRiskCode() + "奖金给付问题");
                                return false;
                            }
                        }
                    }
                }
                //如果不是满期金，将GettoDate在此顺延到下一个给付日期。
                if(!tLCGetSchema.getGetDutyKind().equals("0"))
                {
                    LCGetDB xLCGetDB = new LCGetDB();
                    LCGetSchema xLCGetSchema = new LCGetSchema();
                    xLCGetDB.setPolNo(tLCGetSchema.getPolNo());
                    xLCGetDB.setGetDutyCode(tLCGetSchema.getGetDutyCode());
                    xLCGetDB.setDutyCode(tLCGetSchema.getDutyCode());
                    xLCGetDB.getInfo();
                    xLCGetSchema = xLCGetDB.getSchema();
                    String newGetToDate = " select gettodate +"+xLCGetDB.getGetDutyKind()+" month From lcget where contno ='"+xLCGetDB.getContNo()+"'"
                                        + " and getDutyCode = '"+xLCGetDB.getGetDutyCode()+"'";
                    xLCGetSchema.setGettoDate(new ExeSQL().getOneValue(newGetToDate).trim()); //得到下次给付日期
                    xLCGetSchema.setModifyDate(PubFun.getCurrentDate());
                    xLCGetSchema.setModifyTime(PubFun.getCurrentTime());
                    mMMap.put(xLCGetSchema,SysConst.UPDATE);
                }
            }
            mMMap.put(tLJSGetDrawSet, "DELETE&INSERT");
            LJSGetSchema tLJSGetSchema = new LJSGetSchema();
            
            ref.transFields(tLJSGetSchema, mLJSGetSchema);
            tLJSGetSchema.setSumGetMoney(sumGetMoney);
            tLJSGetSchema.setOperator(tGI.Operator);
            tLJSGetSchema.setMakeDate(CurrentDate);
            tLJSGetSchema.setMakeTime(CurrentTime);
            tLJSGetSchema.setModifyDate(CurrentDate);
            tLJSGetSchema.setModifyTime(CurrentTime);
            mMMap.put(tLJSGetSchema, "DELETE&INSERT");
            String updateDate = " update ljsget a set getdate = (select max(LastGetToDate) from ljsgetdraw where getnoticeno = a.getnoticeno ) "
                                + " where a.getnoticeno ='" + mGetNoticeNo +
                                "'"
                                ;
            mMMap.put(updateDate, "UPDATE");

        }
        saveData.clear();
        saveData.add(mMMap);
        return true;
    }

    public LCGetSet getLCGetNeedGet(String contNo, String insuredNo) {
        String tSBql = " select b.* from lcpol a,lcget b , lmdutygetalive c  where 1=1 "
                        + " and a.polno = b.polno and b.getdutycode = c.getdutycode   "
                        + " and a.contno='" + contNo + "' "
                        + " and a.appflag='1' "
                        + " and a.riskcode not in ('320106','120706') "
                        + " and b.insuredno='"+insuredNo+"'"
                        + " with ur "
                        ;

        LCGetSet tLCGetSet = new LCGetSet();
        LCGetDB tLCGetDB = new LCGetDB();
        tLCGetSet = tLCGetDB.executeQuery(tSBql);
        System.out.println(tSBql.toString());

        if (tLCGetSet.size() == 0) {
            CError.buildErr(this, "保单号为：" + contNo + "的保单已被其他业务员抽档。");
            return null;
        }
        return tLCGetSet;
    }
    /**
     * 判断给付责任的险种是主险还是附加险
     * 主险返回: true;
     * 附加返回：false;
     * @param polNo String
     * @return boolean
     */

    private boolean getPolAttribute(String polNo)
    {
        //不知道当时怎么想得用了一个那么不好懂的判断。现在直接取lmriskapp描述，判断是主线还是附加险。
        String polSQL = " select subriskflag from lcpol a,lmriskapp b where  a.riskcode = b.riskcode and a.polno ='"+polNo+"'"
                      ;
        SSRS tSSRS = new ExeSQL().execSQL(polSQL);

        if (tSSRS.getMaxRow()==0)
        {
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitCalBL";
            tError.functionName = "getPolAttribute";
            tError.errorMessage = "获得主险信息描述失败！";
            this.mErrors.addOneError(tError);
            return false;
        }
        // M-- 主险
        // S-- 附加险
        if (tSSRS.GetText(1,1).equals("M"))
        {
            return true ;
        }
        else{
            return false ;
        }
    }
    
    /**
     * createTask
     *
     * @return MMap
     */
    private boolean createTask()
    {
    	String tWorkNo = com.sinosoft.task.CommonBL.createWorkNo();
//    	存之前的MONEY
    	LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
        tLPEdorEspecialDataSchema.setEdorAcceptNo(tWorkNo);
        tLPEdorEspecialDataSchema.setEdorNo(tWorkNo);
        tLPEdorEspecialDataSchema.setEdorType("XX");
        tLPEdorEspecialDataSchema.setDetailType("BeforeMoney");
        tLPEdorEspecialDataSchema.setPolNo(BQ.FILLDATA);
        tLPEdorEspecialDataSchema.setEdorValue(Double.toString(mLJSGetSchema.getSumGetMoney()));
        mMMap.put(tLPEdorEspecialDataSchema, "DELETE&INSERT");
//      存之后的MONEY
    	LPEdorEspecialDataSchema tLPEdorEspecialDataSchemaA = new LPEdorEspecialDataSchema();
    	tLPEdorEspecialDataSchemaA.setEdorAcceptNo(tWorkNo);
    	tLPEdorEspecialDataSchemaA.setEdorNo(tWorkNo);
    	tLPEdorEspecialDataSchemaA.setEdorType("XX");
    	tLPEdorEspecialDataSchemaA.setDetailType("AfterMoney");
    	tLPEdorEspecialDataSchemaA.setPolNo(BQ.FILLDATA);
    	tLPEdorEspecialDataSchemaA.setEdorValue(ssallmoney);
        mMMap.put(tLPEdorEspecialDataSchemaA, "DELETE&INSERT");
        
    	String appntno=new ExeSQL().getOneValue("select appntno from lccont where contno='"+mContNo+"'");
        if (appntno == null || appntno == "" || appntno.equals("")) 
        {
        	// @@错误处理
			System.out.println("ExpirBenefitCalBL+createTask++--");
			CError tError = new CError();
			tError.moduleName = "ExpirBenefitCalBL";
			tError.functionName = "createTask";
			tError.errorMessage = "保单的投保人号无法获取";
			this.mErrors.addOneError(tError);
			return false;
        }
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setWorkNo(tWorkNo);
        tLGWorkSchema.setCustomerNo(appntno);
        tLGWorkSchema.setContNo(mContNo);
        tLGWorkSchema.setInnerSource(mGetNoticeNo);
        tLGWorkSchema.setAcceptWayNo(Task.ACCEPTWAY_INNER);
        tLGWorkSchema.setStatusNo(Task.WORKSTATUS_DONE);
        tLGWorkSchema.setApplyName("自动");
        tLGWorkSchema.setTypeNo("0300025");
        StringBuffer tRemark = new StringBuffer();
        tRemark.append("自动批注：常无忧B变更公式重算费，变更前的应付总金额为:[").append(mLJSGetSchema.getSumGetMoney())
        		.append("元]，变更后的公式为:[公式")
        		.append(mFormulaBefore);
        System.out.println("mRateFlag"+mRateFlag);
        System.out.println("mFormula"+mFormula);
        		if(mRateFlag.equals("Y")&&mFormulaBefore.equals("3")){
        			tRemark.append("(息降我不降)");
        		}
        tRemark.append("]，给付总金额为:[").append(ssallmoney)
               .append("元]，修改原因:[").append(remark)
               .append("]。");
        tLGWorkSchema.setRemark(tRemark.toString());
        String workBoxNo = getWorkBox();
        if(workBoxNo == null)
        {
            // @@错误处理
			System.out.println("ExpirBenefitCalBL+createTask++--");
			CError tError = new CError();
			tError.moduleName = "ExpirBenefitCalBL";
			tError.functionName = "createTask";
			tError.errorMessage = "操作员的个人信箱号未找到";
			this.mErrors.addOneError(tError);
			return false;
        }

        VData tVData = new VData();
        tVData.add(tLGWorkSchema);
        tVData.add(tGI);
        tVData.add(workBoxNo);

        TaskInputBL tTaskInputBL = new TaskInputBL();
        MMap tMMap = tTaskInputBL.getSubmitData(tVData, "");
        if(tMMap == null)
        {
            // @@错误处理
			System.out.println("ExpirBenefitCalBL+createTask++--");
			CError tError = new CError();
			tError.moduleName = "ExpirBenefitCalBL";
			tError.functionName = "createTask";
			tError.errorMessage = "生成工单信息失败";
			this.mErrors.addOneError(tError);
			return false;
        }else{
        	mMMap.add(tMMap);
            return true;
        }
    }
    
    /**
     * getWorkBox
     *
     * @return String
     */
    private String getWorkBox()
    {
        String sql = "select WorkBoxNo from LGWorkBox "
                     + "where OwnerNo = '" + tGI.Operator + "' ";
        ExeSQL tExeSQL = new ExeSQL();
        String tWorkBox = tExeSQL.getOneValue(sql);
        if(tWorkBox == null || tWorkBox.equals("") || tWorkBox.equals("null"))
        {
            mErrors.addOneError("没有查询到您的工单信息，不能继续处理业务");
            return null;
        }
        return tWorkBox;
    }
    
}
