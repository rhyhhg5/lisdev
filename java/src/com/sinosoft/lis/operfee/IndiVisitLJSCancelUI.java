package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: lis</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class IndiVisitLJSCancelUI
{
    /**错误的容器*/
    public CErrors mErrors = null;

    public IndiVisitLJSCancelUI()
    {
    }

    /**
     * 的存储回访处理结果集
     * @param cInputData VData：传入的数据待处理数据集合，包含：
     * 1、LGPhoneHastenSchema对象，回访结果结果，
     * 需要WorkNo,GetNoticeNo
     * 2、GlobalInput对象，完整的用户信息
     * @param cOperate String：此为""
     * @return MMap:处理后的数据集合
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        IndiVisitLJSCancelBL bl = new IndiVisitLJSCancelBL();
        if(!bl.submitData(cInputData, cOperate))
        {
            mErrors = bl.mErrors;
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        IndiVisitLJSCancelUI indivisitljscancelui = new IndiVisitLJSCancelUI();
    }
}
