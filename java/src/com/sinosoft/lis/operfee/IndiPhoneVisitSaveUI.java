package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:
 * 用户接口类，得到用户录入的数据并传给后台IndiPhoneVisitSaveBL.java进行处理
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author YangYalin
 * @version 1.3
 */

public class IndiPhoneVisitSaveUI
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = null;

    public IndiPhoneVisitSaveUI()
    {
    }

    /**
     * 存储回访处理结果集
     * @param cInputData VData：传入的数据待处理数据机和，包含：
     * 1、LGPhoneHastenSchema对象，回访结果结果，
     * 需要WorkNo,GetNoticeNo,FinishType,OldPayMode,OldPayDate,OldBankCode,OldBackAccNo，均为回访后的结果
     * 2、GlobalInput对象，完整的用户信息
     * @param cOperate String：此为""
     * @return boolean: 成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        IndiPhoneVisitSaveBL bl = new IndiPhoneVisitSaveBL();
        if(!bl.submitData(cInputData, cOperate))
        {
            this.mErrors = bl.mErrors;
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        GlobalInput tGI = new GlobalInput();

        //合同号

        LGPhoneHastenSchema tLGPhoneHastenSchema = new LGPhoneHastenSchema();
        tLGPhoneHastenSchema.setWorkNo("20060827000010");
        tLGPhoneHastenSchema.setOldPayDate("2006-08-30");
        tLGPhoneHastenSchema.setOldPayMode("1");
        tLGPhoneHastenSchema.setOldBankCode("0153");
        tLGPhoneHastenSchema.setOldBackAccNo("2502120701010697781");
        tLGPhoneHastenSchema.setGetNoticeNo("31000000343");

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("OperatorType", "1");

        VData tVData = new VData();
        tVData.addElement(tLGPhoneHastenSchema);
        tVData.addElement(tGI);
        tVData.addElement(tTransferData);

        IndiPhoneVisitSaveUI tIndiPhoneVisitSaveBL = new IndiPhoneVisitSaveUI();
        if(!tIndiPhoneVisitSaveBL.submitData(tVData, ""))
        {
            System.out.println(tIndiPhoneVisitSaveBL.mErrors.getErrContent());
        }
    }
}
