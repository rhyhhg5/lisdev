package com.sinosoft.lis.operfee;

import java.util.*;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 续期续保业务报表1-达成率分析(保单)
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class PrtOperfeePayModBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private static String COM86 = "全系统";
    private static int COL_NUM = 16;

    private GlobalInput mGI = null; //操作员信息
    private TransferData mTransferData = null;

    private String mStartDate = null;
    private String mEndDate = null;
    private String mManageCom = null;

    private XmlExport xmlexport = new XmlExport();

    public PrtOperfeePayModBL()
    {
    }

    /**
     * 操作的提交方法，得到打印清单数据。
     * @param sql String
     * @return XmlExport
     */
    public XmlExport getXmlExport(VData cInputData, String operate)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return xmlexport;
    }

    private boolean checkData()
    {
        mStartDate = (String) mTransferData
                           .getValueByName(FeeConst.STARTDATE);  //开始日期
        mEndDate = (String) mTransferData.getValueByName(FeeConst.ENDDATE);  //截至日期
        mManageCom = (String) mTransferData
                           .getValueByName(FeeConst.MANAGECOM);  //管理机构

        if(mStartDate == null || mStartDate.equals("")
           || mEndDate == null || mEndDate.equals("")
            || mManageCom == null || mManageCom.equals(""))
       {
           CError tError = new CError();
           tError.moduleName = "PrtContSuccessBL";
           tError.functionName = "dealData";
           tError.errorMessage = "实收起止日期和管理机构均不能为空";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return false;
       }

        return true;
    }

    /**
     * 得到录入的数据
     * @param data VData：包括TransferData对象和GlobalInput对象
     * @return boolean: 成功true，否则false
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        mTransferData = (TransferData) data
                        .getObjectByObjectName("TransferData", 0);

        if(mGI == null || mGI.Operator == null || mGI.ManageCom == null
            || mTransferData == null)
        {
            CError tError = new CError();
            tError.moduleName = "PrtContSuccessBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * 处理业务逻辑，生成打印数据XmlExport。
     * @return boolean:操作成功true，否则false
     */
    private boolean dealData()
    {
        xmlexport.createDocument("PrtOperfeePayMod.vts", "printer");

        if(!dealHead())
        {
            return false;
        }

        if(!dealList())
        {
            return false;
        }

        xmlexport.outputDocumentToFile("D:\\", "PrtOperfeePayMod.");

        return true;
    }

    /**
     * dealMJ
     *生成满期结算清单
     * @return boolean
     */
    private boolean dealList()
    {
        String sql =
            "select ManageCom, count(GetNoticeNo), varchar(sum(Money)), "
            + "	 sum(SelfCount) SelfCount, varchar(sum(SelfMoney)) SelfMoney, sum(SelfPhoneCount) SelfPhoneCount, sum(SelfLimit) SelfLimit, "
            + "	 sum(BankPhoneCount) BankPhoneCount, sum(BankLimit) BankLimit "
            + "from ( "
            + "	 select ManageCom, GetNoticeNo, money, "  //总件数、金额
            + "  	case when BankAccNo is null then 1 else 0 end SelfCount, "   //自缴件数
            + "  	case when BankAccNo is null then money else 0 end SelfMoney, "  //自缴保费
            + "		case when BankAccNo is null then limit else 0 end SelfLimit, "  //自缴时长
            + "		case when BankAccNo is not null then limit else 0 end BankLimit, "  //银行时长
            + "		case when BankAccNo is null then PhoneCount else 0 end SelfPhoneCount, "  //自缴是否催缴
            + "		case when BankAccNo is not null then PhoneCount else 0 end BankPhoneCount "  //银行是否催缴
            + "	 from ( "
            + "		select a.ManageCom ManageCom, a.GetNoticeNo GetNoticeNo, "
            + "			(select Sum(SumActuPayMoney) from LJAPayPerson where GetNoticeNo = a.GetNoticeNO and PayType = 'ZC') money, "
            + "			BankAccNo, days(a.MakeDate) - days((select MakeDate from LJSPayB where GetNoticeNo=a.GetNoticeNo and DealState = '1')) limit, " //时长=核销-生成应收
            + "			case (select count(1) from LGPhoneHasten where GetNoticeNo = a.GetNoticeNo) when 0 then 1 else 0 end PhoneCount "  //催前成功标志
            + "		from LJAPay a "
            + "		where exists (select 1 from LJSPayB where GetNoticeNo = a.GetNoticeNo and OtherNoType in('2')) "
            + "			and ManageCom like '" + mManageCom + "%' "
            + "			and MakeDate between '" + mStartDate + "' and '" + mEndDate + "' "
            + "		group by a.ManageCom, a.GetNoticeNo, BankAccNo, a.MakeDate "
            + "	 ) t "
            + "	 group by ManageCom, GetNoticeNo, BankAccNo, Money, limit, PhoneCount "
            + ") tb "
            + "group by ManageCom ";
            System.out.println(sql);
        ExeSQL e = new ExeSQL();
        SSRS tSSRS = e.execSQL(sql);
        if(e.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "PrtContSuccessBL";
            tError.functionName = "dealMJ";
            tError.errorMessage = "查询清单出错";
            mErrors.addOneError(tError);
            System.out.println(e.mErrors.getErrContent());
            System.out.println(tError.errorMessage);
            return false;
        }

        if(tSSRS.getMaxRow() == 0)
        {
            return true;
        }

        String[] sumInfo = new String[this.COL_NUM];
        ListTable tListTable = new ListTable();
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            sumInfo = CommonBL.add(sumInfo, tSSRS.getRowData(i), 1);

            String[] info = getOneRow(tSSRS.getRowData(i));
            tListTable.add(info);
        }

        String[] info = getOneRow(sumInfo);
        info[1] = this.COM86;
        tListTable.add(info);

        tListTable.setName("List");

        String[] colNames = {"序号","机构","收费总件数","收费总金额",
                            "自缴件件数","自缴件件数占比(%)", "自缴件金额",
                            "自缴件金额占比(%)", "自缴件催前成功率(%)","自缴件时效",
                            "银行件数","银行件数占比(%)", "银行金额",
                            "银行金额占比(%)", "银行催前成功率(%)","银行时效"};

        xmlexport.addListTable(tListTable, colNames);

        return true;
    }

    /**
     * 根据查询的到的行记录生成报表行数据
     * @param tSSRS SSRS
     * @param i int
     * @return String[]
     */
    private String[] getOneRow(String[] row)
    {
        String[] info = new String[COL_NUM];

        info[0] = "";
        info[1] = com.sinosoft.task.GetMemberInfo.getComNameByComCode(row[0]);
        //总件数、保费
        info[2] = StrTool.cTrim(row[1]);
        info[3] = StrTool.cTrim(row[2]);
        //自缴
        info[4] = StrTool.cTrim(row[3]); //自缴件数
        info[5] = getRate(info[4], info[2], 100); //自缴件数占比=自缴件数/总件数
        info[6] = StrTool.cTrim(row[4]); //自缴金额
        info[7] = getRate(info[6], info[3], 100); //金额占比=自缴金额/总金额
        info[8] = getRate(row[5], info[4], 100); //催前成功率=未催缴件数/自缴件数
        info[9] = getRate(row[6], info[4], 1); //时效=自缴总时长/自缴件
        //银行转帐
        info[10] = String.valueOf(Integer.parseInt(info[2])
                                  - Integer.parseInt(info[4])); //银行件数金额=总件数-自缴件数
        info[11] = String.valueOf(CommonBL.carry(100 - Double.parseDouble(info[5]))); //银行件数占比=100-自缴件数占比
        info[12] = String.valueOf(Double.parseDouble(info[3])
                                  - Double.parseDouble(info[6])); //银行金额=总金额-自缴金额
        info[13] = String.valueOf(com.sinosoft.lis.bq.CommonBL
                                  .carry(100 - Double.parseDouble(info[7]))); //银行金额占比=100-自缴金额占比
        info[14] = getRate(row[7], info[10], 100); //催前成功率=未催缴件数/自缴件数
        info[15] = getRate(row[8], info[10], 1); //时效=自缴总时长/自缴件

        return info;
    }

    /**
     * getRate
     * 计算sub/sum * n
     * @param string String
     * @param string1 String
     * @param n 倍数
     * @return String
     */
    private String getRate(String sub, String sum, double n)
    {
        if(sum == null || sum.equals("") || sub == null || sum.equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "PrtOperfeePayModBL";
            tError.functionName = "getRate";
            tError.errorMessage = "求比例时分子分母均不能为空";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        if("0".equals(sum))
        {
            return "0.00";
        }

        try
        {
            double down = Double.parseDouble(sum);
            double up = Double.parseDouble(sub);
            return CommonBL.bigDoubleToCommonString(up / down * n, "0.00");
        }
        catch(NumberFormatException ex)
        {
            CError tError = new CError();
            tError.moduleName = "PrtOperfeePayModBL";
            tError.functionName = "getRate";
            tError.errorMessage = "求比例时分子分母均应为数字";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
    }

    /**
     * dealHead
     *生成抬头信息
     * @return boolean
     */
    private boolean dealHead()
    {
        TextTag tag = new TextTag();
        tag.add("ManageCom", mGI.ManageCom);
        tag.add("ManageComName", com.sinosoft.task.GetMemberInfo.getComName(mGI.Operator));
        tag.add("Operator", mGI.Operator);
        tag.add("OperatorName", com.sinosoft.task.GetMemberInfo.getMemberName(mGI));
        tag.add("PrintDate", PubFun.getCurrentDate());
        tag.add("StartDate", StrTool.cTrim(this.mStartDate));
        tag.add("EndDate", StrTool.cTrim(this.mEndDate));

        String comCode = StrTool.cTrim(this.mManageCom);
        tag.add("ManageComSelect", comCode);
        tag.add("ManageComNameSelect",
                comCode.equals("") ? "" : com.sinosoft.task.GetMemberInfo.getComNameByComCode(comCode));

        xmlexport.addTextTag(tag);

        return true;
    }

    public static void main(String[] args)
    {
        GlobalInput mGlobalInput = new GlobalInput(); //操作员信息
        mGlobalInput.Operator = "pa0001";
        mGlobalInput.ManageCom = "86";
        mGlobalInput.ComCode = mGlobalInput.ManageCom;

        TransferData t = new TransferData();
        t.setNameAndValue(FeeConst.STARTDATE, "2006-01-01");
        t.setNameAndValue(FeeConst.ENDDATE, "2006-12-15");
        t.setNameAndValue(FeeConst.MANAGECOM, "86");

        VData d = new VData();
        d.add(mGlobalInput);
        d.add(t);

        PrtOperfeePayModBL bl = new PrtOperfeePayModBL();
        if(bl.getXmlExport(d, "") == null)
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
