package com.sinosoft.lis.operfee;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.ChangeCodeBL;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.rmi.*;
import com.sinosoft.lis.message.*;

import java.util.*;

/**
 * <p>Title: 宽限期短信通知</p>
 *	
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2013</p
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GracePeriodSDMsgBL {
    private GlobalInput mG = new GlobalInput();
    private String mCurrentDate = PubFun.getCurrentDate();
    private ExeSQL mExeSQL = new ExeSQL();
    private String mContNo = null;
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    private boolean getInputData(VData cInputData) 
    {
    	mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        return true;
    }
    
    public void runOneCont(String cContNo)
    {
        mContNo = cContNo;
        GlobalInput mGlobalInput = new GlobalInput();
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "863706";//山东烟台
        mVData.add(mGlobalInput);
        submitData(mVData, "SDMSG");
    }

    public boolean submitData(VData cInputData, String cOperate) {
        cInputData = (VData) cInputData.clone();

        if (!getInputData(cInputData)) 
        {
        	mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0));
        	if(mG.Operator==null || mG.Operator.equals("") || mG.Operator.equals("null"))
        	{
        		mG.Operator="001";
        	}
        	if(mG.ManageCom==null || mG.ManageCom.equals("") || mG.ManageCom.equals("null"))
        	{
        		mG.ManageCom="86";
        	}
            return false;
        }
        if (cOperate != null && cOperate.trim().equals("SDMSG")) {
            sendMsg();
        } else {
            mErrors.addOneError(new CError("不支持的操作字符串！"));
            return false;
        }
        return true;
    }

    private boolean sendMsg() {
        System.out.println("长短险宽限期短信发送批处理开始");
        SMSClient smsClient = new SMSClient();
        
        Vector vec = new Vector();
        vec = getMessage();
//        Vector tempVec = new Vector(); //临时变量--实际发短信时传的参数
        //拆分Vector
        System.out.println("---------vec="+vec.size());

        if (!vec.isEmpty()) 
        {
        	for(int i=0;i<vec.size();i++)
            {
//        		tempVec.clear();
//            	tempVec.add(vec.get(i));
            	SmsMessagesNew msgNews = new SmsMessagesNew();
            	msgNews = (SmsMessagesNew) vec.elementAt(i);
            	smsClient.sendSMS(msgNews);
            }
        } 
        else 
        {
            System.out.print("续期无符合条件的短信！");
        }
        System.out.println("客户续期短信通知批处理正常结束......");
        return true;
    }

    private Vector getMessage() {
    	System.out.println("进入获取信息 getMessage()------------------------");
        Vector tVector = new Vector();
        Vector tVectemp = new Vector();
        ///宽限期短信提醒(长险)--------------------------------
        String tSQLkx =
        	"select a.contNo, "   //保单号
//        	+"(select mobile from lcaddress x, lcappnt y  where x.customerno = y.appntno and x.addressno = y.addressno  and y.contno = a.contno fetch first 1 rows only),  "//客户手机号码
        	+" mobile(a.contno), "
        	+" b.sumduepaymoney, "       //应缴金额
//        	+"(select max(GracePeriod) from LMRiskPay rp, lcpol cp where cp.contno = a.contno  and rp.RiskCode = cp.RiskCode  and (a.stateflag = '1' or a.StateFlag is null)  and a.AppFlag = '1'),"//宽限期到期日
        	+"(select lj.lastpaytodate  from ljspaypersonb lj  where lj.contno=a.contno "
        	+" and lj.getnoticeno=b.getnoticeno order by lj.lastpaytodate fetch first 1 row only ), "
        	+"b.getNoticeNo, "
        	+"b.serialNo, "
			+"a.ManageCom,a.paymode,a.appntname,(case a.appntsex when '0' then '先生' when '1' then '女士'  end),b.bankaccno, a.salechnl, "
			+"(select r.riskname from lmriskapp r, lcpol p where p.contno = a.contno and r.riskcode = p.riskcode and r.riskperiod = 'L' and r.subriskflag = 'M' and risktype6 = '1' and r.riskcode not in ('340501','340601') fetch first 1 rows only)"
        	+"from lccont a, ljspayb b "
        	+"where 1=1 "
        	+"and a.conttype='1' "
        	+"and b.othernotype='2' and b.dealstate = '0' "
        	+"and b.getnoticeno like '31%' " 
        	+"and a.contNo = b.OtherNo "
            +"and (select lj.lastpaytodate + 29 days  from ljspaypersonb lj  where lj.contno=a.contno and lj.getnoticeno=b.getnoticeno " 
            +"     order by lj.lastpaytodate fetch first 1 row only  )  = current date  "
            +"and exists (select 1 from lmriskapp r, lcpol p where p.contno = a.contno and r.riskcode = p.riskcode  and r.riskperiod = 'L') "
            +"and a.payintv <> 0 "
            +"and (a.DueFeeMsgFlag is null or a.DueFeeMsgFlag='' or a.DueFeeMsgFlag = 'Y') "
            +"and not exists (select 1 from LCCSpec  where contno=a.contno and SpecType='kxL' and EndorsementNo=b.getNoticeNo ) "
            //+"and a.ManageCom in ('86370600','86371000') "
            + (mContNo != null ? (" and a.ContNo = '" + mContNo + "' ") : "")
            +" with ur";
        System.out.println("宽限期短信提醒(长险)---------------"+tSQLkx);
        SSRS tMsgSuccSSRS =mExeSQL.execSQL(tSQLkx);
        for (int i = 1; i <= tMsgSuccSSRS.getMaxRow(); i++) 
        {
        	String tContNo = tMsgSuccSSRS.GetText(i, 1);
        	String tCustomerMobile = tMsgSuccSSRS.GetText(i, 2);
//        	String tCustomerMobile = "18500905130";
        	String tPayMoney = tMsgSuccSSRS.GetText(i,3);
        	String tLastDate = tMsgSuccSSRS.GetText(i,4);
        	String tGetNoticeNo = tMsgSuccSSRS.GetText(i,5);
        	String tSerialNo = tMsgSuccSSRS.GetText(i,6);
        	String tManageCom = tMsgSuccSSRS.GetText(i,7);
        	String tPayMode = tMsgSuccSSRS.GetText(i,8);
        	String tAppntName = tMsgSuccSSRS.GetText(i,9);
        	String tAppntSex = tMsgSuccSSRS.GetText(i,10);
        	String tBankAccNo = tMsgSuccSSRS.GetText(i,11);
        	String tSalechnl = tMsgSuccSSRS.GetText(i,12);
        	String riskName = tMsgSuccSSRS.GetText(i, 13);
        	
        	if (tContNo == null || "".equals(tContNo)
					|| "null".equals(tContNo)) {
				continue;
		    }
		    if (tCustomerMobile == null || "".equals(tCustomerMobile)
					|| "null".equals(tCustomerMobile)) {
				continue;
		    }
		    if (tPayMoney == null || "".equals(tPayMoney)
					|| "null".equals(tPayMoney)) {
				continue;
		    }

		    if (tLastDate == null || "".equals(tLastDate) ||
                    "null".equals(tLastDate)) {
                    continue;
                }
		    if (tGetNoticeNo == null || "".equals(tGetNoticeNo) ||
                    "null".equals(tGetNoticeNo)) {
                    continue;
                }
		    if (tSerialNo == null || "".equals(tSerialNo) ||
                    "null".equals(tSerialNo)) {
                    continue;
                }
		    if (tManageCom == null || "".equals(tManageCom) ||
                    "null".equals(tManageCom)) {
                    continue;
                }
		    if (tPayMode == null || "".equals(tPayMode) ||
                    "null".equals(tPayMode)) {
                    continue;
                }
		    if (tAppntName == null || "".equals(tAppntName) ||
                    "null".equals(tAppntName)) {
                    continue;
                }
		    if (tAppntSex == null || "".equals(tAppntSex) ||
                    "null".equals(tAppntSex)) {
                    continue;
                }
		    if (tBankAccNo == null || "".equals(tBankAccNo) ||
                    "null".equals(tBankAccNo)) {
                    continue;
                }
		    if (tSalechnl == null || "".equals(tSalechnl) ||
                    "null".equals(tSalechnl)) {
                    continue;
                }
            
		   //短信内容
           String tMSGContents=""; 
           String tDepartMent="";
           tDepartMent = getDepartment(tSalechnl);
            
            SmsMessageNew tKXMsg = new SmsMessageNew(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
            if("4".equals(tPayMode)){
            	//缴费账户后四位   By LC （续期短信内容调整）
            	String AccNo = null;
            	if(tBankAccNo != null && !"".equals(tBankAccNo) && tBankAccNo.length() > 4){
            		AccNo = tBankAccNo.substring(tBankAccNo.length()-4, tBankAccNo.length());
            	} else {
            		AccNo = tBankAccNo;
            	}
            	
            	tMSGContents = "尊敬的"+tAppntName+tAppntSex+"，您好！您购买的"+riskName+"（保单号"+tContNo+"）" +
            	 		"已于"+tLastDate+"进入缴费期，请您尽快将下期保费"+tPayMoney+"元存入您的尾号"+AccNo+"的缴费帐户，祝您健康。" +
            	 				"客服电话：95591。";
            }
            else{
            	tMSGContents = "尊敬的"+tAppntName+tAppntSex+"，您好！您购买的"+riskName+"(保单号"+tContNo+")" +
    	 		"已于"+tLastDate+"进入缴费期，请您尽快联系公司缴纳下期保费"+tPayMoney+"元，祝您健康。客服电话：95591";
            	
            }
           
            tKXMsg.setReceiver(tCustomerMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
            tKXMsg.setContents(tMSGContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素

            //direct by lc 2017-3-27 机构
            tKXMsg.setOrgCode(tManageCom); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
            
            tVectemp.add(tKXMsg);
            
            SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的Messages元素
            msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
            msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
            msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
            msgs.setTaskValue(tDepartMent);
            msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
            msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
            msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
            msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
            msgs.setMessages((SmsMessageNew[]) tVectemp.toArray(new SmsMessageNew[tVectemp.size()]));
            tVectemp.clear();
            tVector.add(msgs);      		
                                         
            // 在LCCSpec表中插入值  ********************************************************  
        	LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
            tLCCSpecSchema.setGrpContNo(BQ.GRPFILLDATA);
            tLCCSpecSchema.setContNo(tContNo);
            tLCCSpecSchema.setProposalContNo(BQ.GRPFILLDATA);               
            tLCCSpecSchema.setPrtSeq(tSerialNo);// 放 ljspayb   SerialNo
            String tLimit = PubFun.getNoLimit(tManageCom);
	           String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
	           tLCCSpecSchema.setSerialNo(serNo); //流水号                
            tLCCSpecSchema.setEndorsementNo(tGetNoticeNo);
            tLCCSpecSchema.setSpecType("kxL"); //kxD-短险宽限期发送短息
            tLCCSpecSchema.setSpecCode("001");////001-投保人 002-被保人 003-业务员
            tLCCSpecSchema.setSpecContent(tMSGContents+"客户手机号："+ tCustomerMobile);
            tLCCSpecSchema.setOperator(mG.Operator);
            tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
            tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
            tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
            tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
            tLCCSpecSchema.getDB().insert();
        }
//  
//        //宽限期短信提醒（短险）-------------------------------------------------
//        
        String ttSQLkxdq = 
        	"select a.contNo, "   //保单号
//        	+"(select mobile from lcaddress x, lcappnt y  where x.customerno = y.appntno and x.addressno = y.addressno  and y.contno = a.contno fetch first 1 rows only),  "//客户手机号码
        	+" mobile(a.contno), "
        	+" b.sumduepaymoney, "       //应缴金额
//        	+"(select max(GracePeriod) from LMRiskPay rp, lcpol cp where cp.contno = a.contno  and rp.RiskCode = cp.RiskCode  and (a.stateflag = '1' or a.StateFlag is null)  and a.AppFlag = '1'),"//宽限期到期日
        	+"(select lj.lastpaytodate  from ljspaypersonb lj  where lj.contno=a.contno "
        	+" and lj.getnoticeno=b.getnoticeno order by lj.lastpaytodate fetch first 1 row only ), "
        	+"b.getNoticeNo, "
        	+"b.serialNo, "
			+"a.ManageCom,a.PayMode,a.appntname,(case a.appntsex when '0' then '先生' when '1' then '女士'end),b.bankaccno,a.salechnl, "
			+"(select r.riskname from lmriskapp r, lcpol p where p.contno = a.contno and r.riskcode = p.riskcode and r.subriskflag = 'M' and risktype6 = '1' and r.riskcode not in ('340501','340601') fetch first 1 rows only)"
        	+"from lccont a, ljspayb b "
        	+"where 1=1 "
        	+"and a.conttype='1' "
        	+"and b.othernotype='2' and b.dealstate = '0' "
        	+"and b.getnoticeno like '31%' " 
        	+"and a.contNo = b.OtherNo "
            +"and (select lj.lastpaytodate + 14 days  from ljspaypersonb lj  where lj.contno=a.contno and lj.getnoticeno=b.getnoticeno " 
            +"     order by lj.lastpaytodate fetch first 1 row only  )  = current date  "
            +"and not exists (select 1 from lmriskapp r, lcpol p where p.contno = a.contno and r.riskcode = p.riskcode  and r.riskperiod = 'L') "
            +"and a.payintv <> 0 "
            +"and (a.DueFeeMsgFlag is null or a.DueFeeMsgFlag='' or a.DueFeeMsgFlag = 'Y') "
            +"and not exists (select 1 from LCCSpec  where contno=a.contno and SpecType='kxL' and EndorsementNo=b.getNoticeNo ) "
            //+"and a.ManageCom in ('86370600','86371000') "
            + (mContNo != null ? (" and a.ContNo = '" + mContNo + "' ") : "")
            + "with ur";
        System.out.println("宽限期短信提醒（短险）------------"+ttSQLkxdq);
        SSRS tKXDQMsgSSRS = mExeSQL.execSQL(ttSQLkxdq);
//        tKXDQMsgSSRS.setMaxRow(2);
        for(int i=1;i<=tKXDQMsgSSRS.getMaxRow();i++){
        	
        	String tContNo = tKXDQMsgSSRS.GetText(i, 1);
        	String tCustomerMobile = tKXDQMsgSSRS.GetText(i, 2);
        	String tShouldPay = tKXDQMsgSSRS.GetText(i, 3);
        	String tLastDay = tKXDQMsgSSRS.GetText(i,4);
        	String tGetNoticeNo = tKXDQMsgSSRS.GetText(i,5);
        	String tSerialNo = tKXDQMsgSSRS.GetText(i,6);
        	String tManageCom = tKXDQMsgSSRS.GetText(i,7);
        	String tPayMode = tKXDQMsgSSRS.GetText(i,8);
        	String tAppntName = tKXDQMsgSSRS.GetText(i,9);
        	String tAppntSex = tKXDQMsgSSRS.GetText(i,10);
        	String tBankAccNo = tKXDQMsgSSRS.GetText(i,11);
        	String tSalechnl = tKXDQMsgSSRS.GetText(i,12);
        	String riskName = tKXDQMsgSSRS.GetText(i, 13);
        	
        	 if (tContNo == null || "".equals(tContNo) ||
                     "null".equals(tContNo)) {
                     continue;
                 }
             
             if (tCustomerMobile == null || "".equals(tCustomerMobile) ||
                     "null".equals(tCustomerMobile)) {
                     continue;
                 }
             if (tShouldPay == null || "".equals(tShouldPay) ||
                     "null".equals(tShouldPay)) {
                     continue;
                 }
         	
         	if (tLastDay == null || "".equals(tLastDay) ||
                     "null".equals(tLastDay)) {
                     continue;
                 }
         	if (tGetNoticeNo == null || "".equals(tGetNoticeNo) ||
                    "null".equals(tGetNoticeNo)) {
                    continue;
                }
         	if (tSerialNo == null || "".equals(tSerialNo) ||
                    "null".equals(tSerialNo)) {
                    continue;
                }
        	
        	if (tManageCom == null || "".equals(tManageCom) ||
                    "null".equals(tManageCom)) {
                    continue;
                }
        	if (tPayMode == null || "".equals(tPayMode) ||
                    "null".equals(tPayMode)) {
                    continue;
                }
		    if (tAppntName == null || "".equals(tAppntName) ||
                    "null".equals(tAppntName)) {
                    continue;
                }
		    if (tAppntSex == null || "".equals(tAppntSex) ||
                    "null".equals(tAppntSex)) {
                    continue;
                }
		    if (tBankAccNo == null || "".equals(tBankAccNo) ||
                    "null".equals(tBankAccNo)) {
                    continue;
                }
//         	
         	String tCustomerContents = "";
         	String tDepartMent="";
            tDepartMent = getDepartment(tSalechnl);
//				发送给客户的短信            
            SmsMessageNew tCustomerMsg = new SmsMessageNew(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
            if("4".equals(tPayMode)){
            	//direct by lc 2017-1-23
            	String AccNo = null;
            	if(tBankAccNo != null && !"".equals(tBankAccNo) && tBankAccNo.length() > 4){
            		AccNo = tBankAccNo.substring(tBankAccNo.length()-4, tBankAccNo.length());
            	} else {
            		AccNo = tBankAccNo;
            	}
            	
            	tCustomerContents = "尊敬的"+tAppntName+tAppntSex+"，您好！您购买的"+riskName+"（保单号"+tContNo+"）" +
           	 		"已于"+tLastDay+"进入缴费期，请您尽快将下期保费"+tShouldPay+"元存入您尾号"+AccNo+"的缴费账户，祝您健康。" +
           	 				"客服电话：95591。";
           }
           else{
        	   tCustomerContents = "尊敬的"+tAppntName+tAppntSex+"，您好！您购买的"+riskName+"（保单号"+tContNo+"）" +
	   	 		"已于"+tLastDay+"进入缴费期，请您尽快联系公司缴纳下期保费"+tShouldPay+"元，祝您健康。客服电话：95591";
           	
           }
            tCustomerMsg.setReceiver(tCustomerMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
            //tCustomerMsg.setReceiver("13581900861");
            tCustomerMsg.setContents(tCustomerContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
            
            //direct by lc 2017-3-27 机构
            tCustomerMsg.setOrgCode(tManageCom); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素

            //添加SmsMessage对象
            tVectemp.add(tCustomerMsg);
            
            SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的Messages元素
            msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
            msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
            msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
            msgs.setTaskValue(tDepartMent);
            msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
            msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
            msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
            msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
            msgs.setMessages((SmsMessageNew[]) tVectemp.toArray(new SmsMessageNew[tVectemp.size()]));
            tVectemp.clear();
            tVector.add(msgs);
                
//        	在LCCSpec表中插入值  ********************************************************  
        	LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
            tLCCSpecSchema.setGrpContNo(BQ.GRPFILLDATA);
            tLCCSpecSchema.setContNo(tContNo);
            tLCCSpecSchema.setProposalContNo(BQ.GRPFILLDATA);               
            tLCCSpecSchema.setPrtSeq(tSerialNo);// 放 ljspayb getNoticeNo 
            String tLimit = PubFun.getNoLimit(tManageCom);
	           String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
	           tLCCSpecSchema.setSerialNo(serNo); //流水号                
            tLCCSpecSchema.setEndorsementNo(tGetNoticeNo);//应缴保费
            tLCCSpecSchema.setSpecType("kxL"); //kxD-短险宽限期发送短息
            tLCCSpecSchema.setSpecCode("001");////001-投保人 002-被保人 003-业务员
            tLCCSpecSchema.setSpecContent(tCustomerContents+"客户手机号："+ tCustomerMobile);
            tLCCSpecSchema.setOperator(mG.Operator);
            tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
            tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
            tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
            tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
            tLCCSpecSchema.getDB().insert();
        }
        			
 

                   
               //到此处
        System.out.println("都结束了----------------------------------------------------------------");
        return tVector;
    }
    
    private String getDepartment(String Salechnl){
 	   String department = "";
 	   if("01".equals(Salechnl)){
 		   department = SMSClient.mes_taskVlaue10;
 	   } else if("02".equals(Salechnl)){
 		   department = SMSClient.mes_taskVlaue9;
 	   } else if("03".equals(Salechnl)){
 		   department = SMSClient.mes_taskVlaue9;
 	   } else if("04".equals(Salechnl)){
 		   department = SMSClient.mes_taskVlaue11;
 	   } else if("06".equals(Salechnl)){
 		   department = SMSClient.mes_taskVlaue10;
 	   } else if("07".equals(Salechnl)){
 		   department = SMSClient.mes_taskVlaue9;
 	   } else if("10".equals(Salechnl)){
 		   department = SMSClient.mes_taskVlaue10;
 	   } else if("13".equals(Salechnl)){
 		   department = SMSClient.mes_taskVlaue11;
 	   } else if("14".equals(Salechnl)){
 		   department = SMSClient.mes_taskVlaue12;
 	   } else if("15".equals(Salechnl)){
 		   department = SMSClient.mes_taskVlaue12;
 	   } else if("16".equals(Salechnl)){
 		   department = SMSClient.mes_taskVlaue8;
 	   } else if("17".equals(Salechnl)){
 		   department = SMSClient.mes_taskVlaue14;
 	   } else if("18".equals(Salechnl)){
 		   department = SMSClient.mes_taskVlaue8;
 	   } else {
 		   department = SMSClient.mes_taskVlaue15;
 	   }	   
 	   return department;
    }
    
    public static void main(String[] args) {
        GlobalInput mGlobalInput = new GlobalInput();
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);
        
        GracePeriodSDMsgBL tSDwMsgBL = new GracePeriodSDMsgBL();
        tSDwMsgBL.submitData(mVData, "SDMSG");
    }

}
