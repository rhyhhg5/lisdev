package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.xb.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.pubfun.*;
import java.util.Date;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import com.sinosoft.lis.taskservice.NextFeeHastenTask;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.task.TaskPhoneFinishBL;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @author Yuanaq
 * Yangh于2005-07-17修改
 * @version 1.0
 */

public class IndiFinUrgeVerifyBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData;

    //保存操作员和管理机构的类
    private GlobalInput mGI = new GlobalInput();

    private boolean mNeedRnewVerify = false;

    /** 数据操作字符串 */
    private String mOperate;
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String serNo = ""; //流水号
    private String tLimit = "";
    private String payNo = ""; //交费收据号
    private double mDif = 0; //余额
    private MMap map = new MMap();
    private String mGetNoticeNo ;

//暂收费表
    private LJTempFeeBL mLJTempFeeBL = new LJTempFeeBL();
    private LJTempFeeSchema mLJTempFeeSchema = new LJTempFeeSchema();
    private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
    private LJTempFeeSet mLJTempFeeNewSet = new LJTempFeeSet();
    private LJTempFeeSet mLJTempFeeNewInsSet = new LJTempFeeSet();


//暂收费分类表
    private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
    private LJTempFeeClassSet mLJTempFeeClassNewSet = new LJTempFeeClassSet();
    private LJTempFeeClassSet mLJTempFeeClassNewInsSet = new LJTempFeeClassSet();


//应收个人交费表
    private LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet();
    private LJSPayPersonSet mLJSPayPersonNewSet = new LJSPayPersonSet();

//应收总表
    private LJSPayBL mLJSPayBL = new LJSPayBL();
    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();
//实收个人交费表
    private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();

//实收总表
    private LJAPayBL mLJAPayBL = new LJAPayBL();
    private LJAPaySchema mLJAPaySchema = new LJAPaySchema();
//个人合同表
    private LCContSchema mLCContSchema = new LCContSchema();
    // private LOPRTManagerSchema mLOPRTManagerSchema,mLOPRTManagerSchemaNew;
//个人保单表
    private LCPolSet mLCPolSet = new LCPolSet();
//保费项表
    private LCPremSet mLCPremSet = new LCPremSet();
    private LCPremSet mLCPremNewSet = new LCPremSet();

//保险责任表LCDuty
    private LCDutySet mLCDutySet = new LCDutySet();
    private LCDutySet mLCDutyNewSet = new LCDutySet();

//保险帐户表
    private LCInsureAccSet mLCInsureAccSet = new LCInsureAccSet();

//保险帐户表记价履历表
    private LCInsureAccTraceSet mLCInsureAccTraceSet = new LCInsureAccTraceSet();
    private boolean mUrgeFlag = true; // 是否进行核销操作
    private boolean mPayMode = true; // 交费方式，默认为现金模式
    private String mLJSPayBSql = ""; // 用于更新总应收备份表
    private String mLJSPayPerBSql = ""; // 用于更新个人应收备份表
    private String mPayDate = ""; //缴费日期
    private String mEnterAccDate = ""; //到帐日期
    private String mConfDate = ""; //财务确认日期
    private TransferData mTransferData;
    private String mRnewState = null;


    //业务处理相关变量
    public IndiFinUrgeVerifyBL() {
    }

    public static void main(String[] args) {
        GlobalInput mGlobalInput = new GlobalInput();
        mGlobalInput.Operator = "endor0";
        mGlobalInput.ComCode = "86";
        mGlobalInput.ManageCom = "86";

        LCContSchema schema = new LCContSchema();
        schema.setContNo("00000000101");

        TransferData td = new TransferData();
        td.setNameAndValue("NeedRnewVerify", "");

        VData tVData = new VData();
        tVData.add(mGlobalInput);
        tVData.add(schema);
        tVData.add(td);

        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
        tLJTempFeeSchema.setOtherNo("9026000000058888");
        // tLJTempFeeSchema.setTempFeeNo("1021010000045788");
        tVData.add(tLJTempFeeSchema);

        IndiFinUrgeVerifyBL tIndiFinUrgeVerifyBL = new IndiFinUrgeVerifyBL();
        if (!tIndiFinUrgeVerifyBL.submitData(tVData, "VERIFY")) {

        }
    }

    public MMap getSubmitMap(VData cInputData, String cOperate) {

        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData)) {
            return null;
        }
        System.out.println("After getinputdata");

        //如果附加险有续保，置标记退出
        /**杨红于2005-07-17说明：续保的逻辑以后再添加
         if (checkRnew() == true) {
           System.out.println("有附加险续保，在续保核销时一并处理");
           return true;
         }*/

        //进行业务处理
        if (!dealData()) {
            return null;
        }

        if(!dealPhoneHasten())
        {
            return null;
        }

        System.out.println("After dealData！");
        //准备往后台的数据
        if (!prepareOutputData()) {
            return null;
        }

        if(!dealPRnewSign())
        {
            return null;
        }

        System.out.println("After prepareOutputData");

        return map;
    }

    /**
     * 个单应收记录作废/撤销类，在IndiLJSCancelBL.getSubmitMap方法 中增加对催缴工单的处理
     * @return boolean
     */
    private boolean dealPhoneHasten()
    {
        String sql = "select * "
                     + "from LGPhoneHasten "
                     + "where getNoticeNo = '"
                     + mLJSPayBL.getGetNoticeNo() + "' "
                     + "   and HastenType = '"
                     + NextFeeHastenTask.HASTEN_TYPE_NEXTFEEP + "' "
                     + "   and FinishType is null ";
        System.out.println(sql);
        LGPhoneHastenSet set = new LGPhoneHastenDB().executeQuery(sql);
        if(set.size() == 0)
        {
            //若无催缴就查不出来也
            return true;
        }

        LGPhoneHastenSchema schema = set.get(1);
        schema.setFinishType("4");
        schema.setRemark("应收记录" + mLJSPayBL.getGetNoticeNo() + "已交费");

        VData data = new VData();
        data.add(schema);
        data.add(mGI);

        TaskPhoneFinishBL bl = new TaskPhoneFinishBL();
        MMap tMMap = bl.getSubmitMap(data, "");
        if(tMMap == null)
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }
        map.add(tMMap);

        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * @param mInputData：包括
     * 1、LCContSchema：保单信息
     * 2、GlobalInput：操作员信息
     * @param cOperate: 操作方式，此为""
     *输出：操作失败返回false,否则返回true
     */
    public boolean submitData(VData cInputData, String cOperate) {

        getSubmitMap(cInputData, cOperate);
        if (map == null || mErrors.needDealError()) {
            return false;
        }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, mOperate)) {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "ContBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 处理保单续保确认
     * @return boolean
     */
    private boolean dealPRnewSign()
    {
        if(!needPRnewOperate())
        {
            return true;
        }

        if(mRnewState.equals(XBConst.RNEWSTATE_UNCONFIRM))
        {
            VData data = new VData();

            LCPolDB db = new LCPolDB();
            db.setContNo(mLCContSchema.getContNo());
//            LCPolSet tLCPolSet = db.query();
            String sqlPolSet ="select a.* From lcpol a, ljspayperson b  where a.contno ='"+mLCContSchema.getContNo()+"'"
                             +" and a.polno = b.polno "
                             +" and b.getnoticeno ='"+mGetNoticeNo+"'"
                             ;
           LCPolSet tLCPolSet = db.executeQuery(sqlPolSet);

            data.add(tLCPolSet);
            data.add(mGI);
            data.add(new TransferData());

            System.out.println(
                "Beginning of PRnewDueVerifyBL.dealData: PRnewContSign");
            PRnewContSignBL tPRnewContSignBL = new PRnewContSignBL();
            MMap tMMap = tPRnewContSignBL.getSubmitData(data, mOperate);
            if(tMMap == null && tPRnewContSignBL.mErrors.needDealError())
            {
                mErrors.copyAllErrors(tPRnewContSignBL.mErrors);
                System.out.println(tPRnewContSignBL.mErrors.getErrContent());
                return false;
            }
            map.add(tMMap);
        }

        return true;
    }

    /**
     * 校验是否需要续保确认和数据转移
     * 1、没有续保轨迹，不需要续保转移，false
     * 2、有续保轨迹但状态不是待确认、确认成功，转移成功，false
     * @return boolean
     */
    private boolean needPRnewOperate()
    {
        String sql = "  select * "
                     + "from LCRnewStateLog a,ljspay b "
                     + "where contNo = '" + mLCContSchema.getContNo() + "' "
                     + "   and newContNo = "
                     + "      (select max(newContNo) "
                     + "      from LCRnewStateLog "
                     + "      where contNo = a.contNo "
                     + "         and state != '6') "
                     + " and a.contno = b.otherno and getnoticeno='"+mLJSPayBL.getGetNoticeNo()+"' "
                     ;
        System.out.println(sql);
        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
        LCRnewStateLogSet tLCRnewStateLogSet
            = tLCRnewStateLogDB.executeQuery(sql);
        if(tLCRnewStateLogSet.size() == 0)
        {
            return false;
        }
        mRnewState = tLCRnewStateLogSet.get(1).getState();
        if(tLCRnewStateLogSet.get(1).getState()
           .compareTo(XBConst.RNEWSTATE_UNCONFIRM) < 0)
        {
            return false;
        }

        return true;
    }

    
    private boolean dealData() {
//step one-查询数据
        // step one-查询数据
        String ContNo = mLJSPayBL.getOtherNo();
        LJSPayPersonDB tmpLJSPayPersonDB = new LJSPayPersonDB();
        // 需要根据通知书号码进行查询，以保证唯一
        tmpLJSPayPersonDB.setGetNoticeNo(mLJSPayBL.getGetNoticeNo());
        mLJSPayPersonSet = tmpLJSPayPersonDB.query();
        String ManageCom = mGI.ManageCom;

        // 杨红于2005-07-29添加：获取lopermanage信息以便将stateflag置为4
        // LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        // tLOPRTManagerDB.setOtherNo(ContNo);
        // tLOPRTManagerDB.setStandbyFlag2(mLJTempFeeBL.getTempFeeNo());
        // tLOPRTManagerDB.setStateFlag("0");
        // LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
        // //mLOPRTManagerSchema
        // tLOPRTManagerSet = tLOPRTManagerDB.query();
        // if (tLOPRTManagerSet.size() > 0)
        // {
        // mLOPRTManagerSchema = tLOPRTManagerSet.get(1).getSchema();
        // mLOPRTManagerSchemaNew.setSchema(mLOPRTManagerSchema);
        // mLOPRTManagerSchemaNew.setStateFlag("4");
        // }

        // 合同的保费合计金额
        double ContactuMoney = 0;
        boolean actuVerifyFlag = false;
        
        
        //通用配置 direct by lc 2017-1-23
        //校验是医诊专家否在交至日前核销，
        ExeSQL tExeSQL = new ExeSQL();
        String Risksql = "select riskcode from lcpol where contno = '" + mLJSPayBL.getOtherNo() + "' and riskcode in (select code from ldcode1 where codetype='WLPYH')"; 
        String resultRisk = tExeSQL.getOneValue(Risksql);
        if(resultRisk!=null&&(!"".equals(resultRisk))&&(!"null".equals(resultRisk))){
        String WLPsql= "select code,code1 from ldcode1 where codetype='WLPYH' and code = '" + resultRisk + "'";
        SSRS tLDCode1SSRS = null;
        tLDCode1SSRS = tExeSQL.execSQL(WLPsql);
	        if(tLDCode1SSRS.getMaxRow()>0){
	        	String sqlWLP = "select 1 from ljspayperson where contno='"+ mLJSPayBL.getOtherNo() +"' and getnoticeno='"+ mLJSPayBL.getGetNoticeNo() +"' and riskcode='" + tLDCode1SSRS.GetText(1, 1) + "' and paytype='ZC'";
	        	String RHX = tExeSQL.getOneValue(sqlWLP);    
				if ("1".equals(RHX)) {
					String mContInfosSQL = "select paytodate,current date,paytodate - 3 years from lcpol where contno='" + mLJSPayBL.getOtherNo() + "'";
					SSRS tContInfosSSRS = null;
					tContInfosSSRS = tExeSQL.execSQL(mContInfosSQL);
					String date = tContInfosSSRS.GetText(1, 1);
					String date2 = tContInfosSSRS.GetText(1, 2);
					String date3 = tContInfosSSRS.GetText(1, 3);
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					// 判断是否打过9折
					String sqlljs = "select sum(sumDuePayMoney) from LJSPayPersonB where contno = '"
							+ mLJSPayBL.getOtherNo()
							+ "' and paytype = 'ZC' and getnoticeno='"
							+ mLJSPayBL.getGetNoticeNo()
							+ "' and riskcode='" + tLDCode1SSRS.GetText(1, 1) + "'";
					double ljsprem = Double.parseDouble(tExeSQL.getOneValue(sqlljs));
					double WLPRate = Double.parseDouble(tLDCode1SSRS.GetText(1, 2));
					double LjsMoney = ljsprem / WLPRate;
					BigDecimal money = new BigDecimal(LjsMoney);
					money = money.setScale(2, BigDecimal.ROUND_HALF_UP);
					double money1 = money.doubleValue() + 0.01;
					double money2 = money.doubleValue() - 0.01;
					double money3 = money.doubleValue();
					String sqlprem = "select sum(prem) from lcpol where polno in (select newpolno from LCRnewStateLog where state !='6' and polno in (select polno from ljspayperson where contno = '"
							+ mLJSPayBL.getOtherNo()
							+ "' and riskcode='" + tLDCode1SSRS.GetText(1, 1) + "' and paytype='ZC'))";
					String resultprem = new ExeSQL().getOneValue(sqlprem);
					if (!"".equals(resultprem) && !"null".equals(resultprem)) {
						double sumpol = Double.parseDouble(resultprem);
						if (money1 == sumpol || money2 == sumpol
								|| money3 == sumpol) {
							try {
								Date dt1 = df.parse(date);
								Date dt2 = df.parse(date2);
								if (dt1.after(dt2)) {
									CError tError = new CError();
									tError.moduleName = "IndiFinUrgeVerifyBL";
									tError.functionName = "dealData";
									tError.errorMessage = "险种：" + tLDCode1SSRS.GetText(1, 1) + "，未到满期日，暂时无法核销";
									this.mErrors.addOneError(tError);
									return false;
								}
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}
			}
        }
		
        // 0-判断应收总表中的应收是否=0
        if (mLJSPayBL.getSumDuePayMoney() == 0) {
            // "应收总表应收款为0!请执行自动核销";
            actuVerifyFlag = true;
        }

        // //保险帐户表
        // LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
        // //保险帐户表记价履历表
        // LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();
        // 产生流水号
        if (mTransferData == null) {
            tLimit = PubFun.getNoLimit(ManageCom);
            serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); //给应收表生成统一的流水号
        } else {
            serNo = (String) mTransferData.getValueByName("GrpSerNo");
            tLimit = PubFun.getNoLimit(ManageCom);
            if(serNo == null || serNo.equals(""))
            {
                serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); //给应收表生成统一的流水号
            }
            mNeedRnewVerify = ((String) mTransferData
                               .getValueByName("NeedRnewVerify")) != null;
        }
        payNo = PubFun1.CreateMaxNo("PayNo", tLimit); //给实收表生成统一的　收据号

        // 1-查询需要核销的保单险种，在此将续期续保一起核销
        LCPolDB tLCPolDB = new LCPolDB();
        StringBuffer tSBql = new StringBuffer(128);

        //续期险种
        tSBql.append("select * ")
            .append("from LCPol a ")
            .append("where ContNo = '")
            .append(ContNo)
            .append("'  and AppFlag = '1' ")
            .append("  and (StateFlag = '"+BQ.STATE_FLAG_SIGN+"' or StateFlag is null) ")
            .append("   and polNo not in ")
            .append("       (select polNo from LCRnewStateLog ")
            .append("        where contNo = a.contNo ")
            .append("           and state != '6') ");

        //若有续保险种，那么
        if(mNeedRnewVerify)
        {
            tSBql.append("union ")
                    .append("select * ")
                    .append("from LCPol ")
                    .append("where contNo = ")
                    .append("      (select max(newContNo) ")
                    .append("      from LCRnewStateLog ")
                    .append("      where contNo = '")
                    .append(ContNo)
                    .append("'        and state = '")
                    .append(XBConst.RNEWSTATE_UNCONFIRM)
                    .append("' ")
                    .append("and polno in (select polno from LJSPayPerson where GetNoticeNo='"+mGetNoticeNo+"') ")
                    .append(") ");
        }

        System.out.println(tSBql);
        LCPolSet tLCPolSet = tLCPolDB.executeQuery(tSBql.toString());
        if (tLCPolDB.mErrors.needDealError()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCPolDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "IndiFinUrgeVerifyBL";
            tError.functionName = "dealData";
            tError.errorMessage = "个人保单险种表查询失败!";
            this.mErrors.addOneError(tError);
            tLCPolSet.clear();
            return false;
        }
        if (tLCPolSet.size() == 0) {
            CError.buildErr(this, "保单号为" + ContNo + "下的险种信息缺失，不能核销！");
            return false;
        }

        String polPayToDate = "";
        for (int n = 1; n <= tLCPolSet.size(); n++) {
            LCPolBL tempLCPolBL = new LCPolBL();
            tempLCPolBL.setSchema(tLCPolSet.get(n));

            //若为续保险种应收转实收，则得到原险种信息
            LCPolSchema oldLCPolSchema = getOldLCPolSchema(tempLCPolBL);
            if(oldLCPolSchema == null)
            {
                return false;
            }

            double polTotalPrem = 0.0; // 保存单个险种下的所交保费总和
            int polCount = 0; // 判断该险种下是否有对应的应收数据信息，有则修改险种信息，否则不变，杨红于2005-07-17说明
            String polNo = tempLCPolBL.getPolNo(); // 保单险种号

            // 查询责任项
            LCDutyDB tLCDutyDB = new LCDutyDB();
            tLCDutyDB.setPolNo(polNo);
            LCDutySet tLCDutySet = tLCDutyDB.query();
            if (tLCDutyDB.mErrors.needDealError()) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCDutyDB.mErrors);
                CError tError = new CError();
                tError.moduleName = "IndiFinUrgeVerifyBL";
                tError.functionName = "dealData";
                tError.errorMessage = "险种责任表查询失败!";
                this.mErrors.addOneError(tError);
                return false;
            }
            if (tLCDutySet.size() == 0) {
                CError.buildErr(this,
                                "保单号:" + ContNo + "，险种号：" + polNo +
                                "下的险种责任信息缺失，不能核销！");
                return false;
            }

            for (int m = 1; m <= tLCDutySet.size(); m++) {
                LCDutyBL tLCDutyBL = new LCDutyBL();
                tLCDutyBL.setSchema(tLCDutySet.get(m));
                // 责任编码
                String dutyCode = tLCDutySet.get(m).getDutyCode();

                LCPremDB tLCPremDB = new LCPremDB();
                tLCPremDB.setPolNo(polNo);
                tLCPremDB.setDutyCode(dutyCode);
                LCPremSet tLCPremSet = tLCPremDB.query();
                if (tLCPremDB.mErrors.needDealError()) {
                    // @@错误处理
                    this.mErrors.copyAllErrors(tLCPremDB.mErrors);
                    CError tError = new CError();
                    tError.moduleName = "IndiFinUrgeVerifyBL";
                    tError.functionName = "dealData";
                    tError.errorMessage = "保费项信息查询失败!";
                    this.mErrors.addOneError(tError);
                    // tLCPolSet.clear();
                    return false;
                }
                if (tLCPremSet.size() == 0) {
                    CError.buildErr(this,
                                    "保单号:" + ContNo + "，险种号：" + polNo +
                                    ",责任编码:" +
                                    dutyCode + "下的险种责任信息缺失，不能核销！");
                    return false;
                }

                int dutyCount = 0; // 责任的保费项个数
                double totalPrem = 0.0; // 责任下的保费项保费
                String PaytoDate = ""; // 责任下的保费项交至日期
                for (int t = 1; t <= tLCPremSet.size(); t++) {
                    LCPremBL tLCPremBL = new LCPremBL();
                    tLCPremBL.setSchema(tLCPremSet.get(t));
                    // 交费计划编码
                    String payPlanCode = tLCPremSet.get(t).getPayPlanCode();

                    // 查询应收个人交费表中的正常交费记录
                    LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
                    tLJSPayPersonDB.setPolNo(oldLCPolSchema.getPolNo());
                    tLJSPayPersonDB.setDutyCode(dutyCode);
                    tLJSPayPersonDB.setPayPlanCode(payPlanCode);
                    // 应收转实收，应收什么样子，实收就什么样子，哈哈
                    // tLJSPayPersonDB.setPayType("ZC");
                    // 由于支持多次抽档，因此所有的LJS表查询都需要加上GetNoticeNo条件
                    tLJSPayPersonDB.setGetNoticeNo(mLJSPayBL.getGetNoticeNo());
                    LJSPayPersonSet tLJSPayPersonSet = tLJSPayPersonDB.query();
                    if (tLJSPayPersonDB.mErrors.needDealError()) {
                        // @@错误处理
                        this.mErrors.copyAllErrors(tLJSPayPersonDB.mErrors);
                        CError tError = new CError();
                        tError.moduleName = "IndiFinUrgeVerifyBL";
                        tError.functionName = "dealData";
                        tError.errorMessage = "应收个人表查询失败!";
                        this.mErrors.addOneError(tError);
                        // mLJSPayPersonSet.clear();
                        return false;
                    }
                    if (tLJSPayPersonSet.size() == 0) {
                        continue;
                    } else {
                        // 这里tLJSPayPersonSet.size()肯定为1
                        tLCPremBL.setPayTimes(tLCPremBL.getPayTimes() + 1); // 已交费次数
                        tLCPremBL.setSumPrem(tLCPremBL.getSumPrem() +
                                             tLCPremBL.getPrem()); // 累计保费
                        tLCPremBL.setPaytoDate(tLJSPayPersonSet.get(1).
                                               getCurPayToDate()); // 交至日期
                        tLCPremBL.setModifyDate(CurrentDate); // 最后一次修改日期
                        tLCPremBL.setModifyTime(CurrentTime); // 最后一次修改时间
                        mLCPremNewSet.add(tLCPremBL);

                        // 杨红说明：取保费项较早的paytodate作为责任表的paytodate
                        if (PaytoDate.equals("")) {
                            PaytoDate = tLCPremBL.getPaytoDate();
                        } else {
                            FDate tFDate = new FDate();
                            Date tDate = tFDate.getDate(PaytoDate);
                            Date tDate1 = tFDate.getDate(tLCPremBL.getPaytoDate());
                            if (tDate1.before(tDate)) {
                                PaytoDate = tLCPremBL.getPaytoDate();
                            }
                        }
                        polPayToDate = tLCPremBL.getPaytoDate(); // 险种的交至日期，随便取一个应收表的信息即可，杨红
                        // 说明

                        totalPrem += tLCPremBL.getPrem();
                        polTotalPrem += tLCPremBL.getPrem();
                        dutyCount++;
                        polCount++;

                        // 多少应收信息产生多少实收信息，方便以后统计使用
                        for (int k = 1; k <= tLJSPayPersonSet.size(); k++) {
                            // 将应收个人表的数据映射到实收个人表中去
                            LJAPayPersonBL tLJAPayPersonBL = new LJAPayPersonBL();
                            tLJAPayPersonBL.setPolNo(tLJSPayPersonSet.get(k).
                                    getPolNo()); // 保单号码
                            tLJAPayPersonBL.setPayCount(tLJSPayPersonSet.get(k).
                                    getPayCount()); // 第几次交费
                            tLJAPayPersonBL.setGrpContNo(tLJSPayPersonSet.get(k).
                                    getGrpContNo()); // 集体保单号码
                            tLJAPayPersonBL.setGrpPolNo(tLJSPayPersonSet.get(k).
                                    getGrpPolNo()); // 集体保单号码
                            tLJAPayPersonBL.setContNo(tLJSPayPersonSet.get(k).
                                    getContNo()); // 总单/合同号码
                            tLJAPayPersonBL.setAppntNo(tLJSPayPersonSet.get(k).
                                    getAppntNo()); // 投保人客户号码
                            tLJAPayPersonBL.setPayNo(payNo); // 交费收据号码
                            tLJAPayPersonBL.setPayTypeFlag(tLJSPayPersonSet.get(
                                    k).getPayTypeFlag());
                            tLJAPayPersonBL.setPayAimClass(tLJSPayPersonSet.get(
                                    k).getPayAimClass()); // 交费目的分类
                            tLJAPayPersonBL.setDutyCode(tLJSPayPersonSet.get(k).
                                    getDutyCode()); // 责任编码
                            tLJAPayPersonBL.setPayPlanCode(tLJSPayPersonSet.get(
                                    k).getPayPlanCode()); // 交费计划编码
                            tLJAPayPersonBL.setSumDuePayMoney(tLJSPayPersonSet.
                                    get(k).getSumDuePayMoney()); // 总应交金额
                            tLJAPayPersonBL.setSumActuPayMoney(tLJSPayPersonSet.
                                    get(k).getSumActuPayMoney()); // 总实交金额
                            tLJAPayPersonBL.setPayIntv(tLJSPayPersonSet.get(k).
                                    getPayIntv()); // 交费间隔
                            tLJAPayPersonBL.setPayDate(mPayDate); // 交费日期
                            tLJAPayPersonBL.setPayType(tLJSPayPersonSet.get(k).
                                    getPayType()); // 交费类型
                            if (actuVerifyFlag) {
                                tLJAPayPersonBL.setEnterAccDate(mEnterAccDate); // 到帐日期
                                tLJAPayPersonBL.setConfDate(mConfDate); // 确认日期
                            } else {
                                // 如果不需要自动核销
                                tLJAPayPersonBL.setEnterAccDate(mEnterAccDate); // 到帐日期
                                tLJAPayPersonBL.setConfDate(mConfDate); // 确认日期
                            }
                            tLJAPayPersonBL.setLastPayToDate(tLJSPayPersonSet.
                                    get(k).getLastPayToDate()); // 原交至日期
                            tLJAPayPersonBL.setCurPayToDate(tLJSPayPersonSet.
                                    get(k).getCurPayToDate()); // 现交至日期
                            tLJAPayPersonBL.setInInsuAccState(tLJSPayPersonSet.
                                    get(k).getInInsuAccState()); // 转入保险帐户状态
                            tLJAPayPersonBL.setApproveCode(tLJSPayPersonSet.get(
                                    k).getApproveCode()); // 复核人编码
                            tLJAPayPersonBL.setApproveDate(tLJSPayPersonSet.get(
                                    k).getApproveDate()); // 复核日期
                            tLJAPayPersonBL.setApproveTime(tLJSPayPersonSet.get(
                                    k).getApproveTime()); // 复核时间
                            tLJAPayPersonBL.setAgentCode(tLJSPayPersonSet.get(k).
                                    getAgentCode());
                            tLJAPayPersonBL.setAgentGroup(tLJSPayPersonSet.get(
                                    k).getAgentGroup());
                            tLJAPayPersonBL.setSerialNo(serNo); // 流水号
                            // tLJAPayPersonBL.setOperator(tLJSPayPersonSet.get(k).getOperator());
                            // //产生应收的操作员
                            tLJAPayPersonBL.setOperator(mGI.Operator); // 实际截转为实收的操作员
                            tLJAPayPersonBL.setMakeDate(CurrentDate); // 入机日期
                            tLJAPayPersonBL.setMakeTime(CurrentTime); // 入机时间
                            tLJAPayPersonBL.setGetNoticeNo(tLJSPayPersonSet.get(
                                    k).getGetNoticeNo()); // 通知书号码
                            tLJAPayPersonBL.setModifyDate(CurrentDate); // 最后一次修改日期
                            tLJAPayPersonBL.setModifyTime(CurrentTime); // 最后一次修改时间
                            tLJAPayPersonBL.setManageCom(tLJSPayPersonSet.get(k).
                                    getManageCom()); // 管理机构
                            tLJAPayPersonBL.setAgentCom(tLJSPayPersonSet.get(k).
                                    getAgentCom()); // 代理机构
                            tLJAPayPersonBL.setAgentType(tLJSPayPersonSet.get(k).
                                    getAgentType()); // 代理机构内部分类
                            tLJAPayPersonBL.setRiskCode(tLJSPayPersonSet.get(k).
                                    getRiskCode()); // 险种编码
                            mLJAPayPersonSet.add(tLJAPayPersonBL);
                        }
                        // 应收个人 转 实收个人 结束
                    }
                }
                if (dutyCount == 0) {
                    continue;
                } else {
                    tLCDutyBL.setPrem(totalPrem); // 实际保费
                    tLCDutyBL.setSumPrem(tLCDutyBL.getSumPrem() + totalPrem); // 累计保费
                    tLCDutyBL.setPaytoDate(PaytoDate); // 交至日期
                    tLCDutyBL.setModifyDate(CurrentDate); // 最后一次修改日期
                    tLCDutyBL.setModifyTime(CurrentTime); // 最后一次修改时间
                    mLCDutyNewSet.add(tLCDutyBL);
                }
            }
            if (polCount == 0) {
                continue;
            } else {
                // 修改险种信息
                tempLCPolBL.setPaytoDate(polPayToDate); // 交至日期
                tempLCPolBL.setSumPrem(tempLCPolBL.getSumPrem() + polTotalPrem); // 总累计保费
                // 求总余额:如果应收总表应收款=0，表明有余额，且这次的余额值存放在责任编码为
                // "yet"的应收个人交费纪录中（见个人催收流程图）,取出放在个人保单表的余额字段中
                // 否则，个人保单表余额纪录置为0
                tempLCPolBL.setModifyDate(CurrentDate); // 最后一次修改日期
                tempLCPolBL.setModifyTime(CurrentTime); // 最后一次修改时间
                mLCPolSet.add(tempLCPolBL);
            }
            ContactuMoney += polTotalPrem;
        }

        // 杨红于2005-07-17说明: 下面处理合同的余额问题，续期中很重要的一个环节
        tSBql = new StringBuffer(128);
        tSBql.append("select * from ljspayperson where polno in (select polno from lcpol where contno='");
        tSBql.append(ContNo);
        tSBql.append("') and paytype='YET' and GetNoticeNo = '");
        tSBql.append(mLJSPayBL.getGetNoticeNo());
        tSBql.append("'");
        LJSPayPersonDB tyetLJSPayPersonDB = new LJSPayPersonDB();
        LJSPayPersonSet tyetLJSPayPersonSet = tyetLJSPayPersonDB.executeQuery(
                tSBql.toString());

        // 获取最新合同的余额
        // 如果无预交记录
        double tDif = 0.0;  //本次核销产生的余额

        //多抵扣的账户余额
        for(int t = 1; t <= tyetLJSPayPersonSet.size(); t++)
        {
            tDif += tyetLJSPayPersonSet.get(1).getSumActuPayMoney();
        }

        //多交的保费
        double tempFeeTotalMoney = 0.0;
        for(int i = 1; i <= mLJTempFeeSet.size(); i++)
        {
            // 此保单交费未核销的金额总计
            tempFeeTotalMoney += mLJTempFeeSet.get(i).getPayMoney();
        }
        if(tempFeeTotalMoney > mLJSPayBL.getSumDuePayMoney())
        {
            tDif += (tempFeeTotalMoney - mLJSPayBL.getSumDuePayMoney());
        }

        // 如果有余额产生，而且交费方式为转账的核销操作
        if(tDif>=-0.0001 && tDif<=0.0001)
        {
            tDif = 0;
        }
        else
        {
            addAccBala(tDif);
        }

        // 查询在应交日到核销日期间内，是否做过减保操作
        tSBql = new StringBuffer(128);
        tSBql.append("select 1 from LPEdorItem where EdorType='PT' and EdorState='0' and EdorAppDate<='");
        tSBql.append(CurrentDate);
        tSBql.append("' and EdorAppDate>='");
        tSBql.append(mLJSPayBL.getStartPayDate());
        tSBql.append("'");
//        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(tSBql.toString());
        // 如果有做过减保操作，则需要对转账金额进行退费
        if (tSSRS.getMaxRow() > 0) {
            tSBql = new StringBuffer(128);
            tSBql.append("select Sum(SumDuePayMoney) from LJSPayPerson where PayType = 'ZC' and GetNoticeNo = '");
            tSBql.append(mLJSPayBL.getGetNoticeNo());
            tSBql.append("'");
            // 获取当期实际要交纳金额
            double tActPayMoney = Double.parseDouble(tExeSQL.getOneValue(
                    tSBql.toString()));

            tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
            String tNo = PubFun1.CreateMaxNo("PAYNOTICENO", tLimit);

            LJTempFeeBL tLJTempFeeBL = new LJTempFeeBL();
            tLJTempFeeBL.setSchema(mLJTempFeeSet.get(1).getSchema());
            tLJTempFeeBL.setTempFeeNo(tNo);
            tLJTempFeeBL.setOtherNo(mLJTempFeeSet.get(1).getTempFeeNo()); // 其他号码为原先交费的通知书号
            tLJTempFeeBL.setOtherNoType("X"); // 特殊的类型
            tLJTempFeeBL.setTempFeeType("X"); // 特殊的续期退费类型
            tLJTempFeeBL.setConfFlag("0"); // 设置未核销状态
            tLJTempFeeBL.setPayMoney(mLJTempFeeSet.get(1).getPayMoney() -
                                     tActPayMoney);
            tLJTempFeeBL.setConfDate(CurrentDate);
            tLJTempFeeBL.setModifyDate(CurrentDate);
            tLJTempFeeBL.setModifyTime(CurrentTime);
            mLJTempFeeNewInsSet.add(tLJTempFeeBL);

            LJTempFeeClassBL tLJTempFeeClassBL = new LJTempFeeClassBL();
            tLJTempFeeClassBL.setSchema(mLJTempFeeClassSet.get(1).getSchema());
            tLJTempFeeClassBL.setTempFeeNo(tNo);
            tLJTempFeeClassBL.setPayMoney(mLJTempFeeSet.get(1).getPayMoney() -
                                          tActPayMoney);
            tLJTempFeeClassBL.setConfFlag("0"); // 设置未核销状态
            tLJTempFeeClassBL.setConfDate(CurrentDate);
            tLJTempFeeClassBL.setModifyDate(CurrentDate);
            tLJTempFeeClassBL.setModifyTime(CurrentTime);
            mLJTempFeeClassNewInsSet.add(tLJTempFeeClassBL);

            // 原则上这里的金额应该为0
            tDif = mLCContSchema.getDif();
        }
//        mDif = tDif;
//        mLCContSchema.setDif(tDif);

        // 在这里处理合同级的PayToDate
        mLCContSchema.setPaytoDate(polPayToDate);
        mLCContSchema.setPrem(ContactuMoney);
        mLCContSchema.setSumPrem(mLCContSchema.getSumPrem() + ContactuMoney);
        mLCContSchema.setModifyDate(CurrentDate); // 最后一次修改日期
        mLCContSchema.setModifyTime(CurrentTime); // 最后一次修改时间
        // 获取交费分类表的信息，准备核销动作。

        // 核销暂收表中的交费记录
        if (mLJTempFeeSet != null && mLJTempFeeSet.size() > 0) {
            for (int t = 1; t <= mLJTempFeeSet.size(); t++) {
                LJTempFeeBL tLJTempFeeBL = new LJTempFeeBL();
                tLJTempFeeBL.setSchema(mLJTempFeeSet.get(t).getSchema());
                tLJTempFeeBL.setConfFlag("1");
                tLJTempFeeBL.setConfDate(CurrentDate);
                tLJTempFeeBL.setModifyDate(CurrentDate);
                tLJTempFeeBL.setModifyTime(CurrentTime);
                mLJTempFeeNewSet.add(tLJTempFeeBL);
            }
        }
        if (mLJTempFeeClassSet != null && mLJTempFeeClassSet.size() > 0) {
            for (int t = 1; t <= mLJTempFeeClassSet.size(); t++) {
                LJTempFeeClassBL tLJTempFeeClassBL = new LJTempFeeClassBL();
                tLJTempFeeClassBL.setSchema(mLJTempFeeClassSet.get(t).getSchema());
                tLJTempFeeClassBL.setConfFlag("1");
                tLJTempFeeClassBL.setConfDate(CurrentDate);
                tLJTempFeeClassBL.setModifyDate(CurrentDate);
                tLJTempFeeClassBL.setModifyTime(CurrentTime);
                mLJTempFeeClassNewSet.add(tLJTempFeeClassBL);

            }
        }
        mLJAPayBL.setPayNo(payNo); // 交费收据号码
        mLJAPayBL.setIncomeNo(mLJSPayBL.getOtherNo()); // 应收/实收编号
        mLJAPayBL.setIncomeType(mLJSPayBL.getOtherNoType()); // 应收/实收编号类型
        mLJAPayBL.setAppntNo(mLJSPayBL.getAppntNo()); // 投保人客户号码
        
        //通用配置 direct by lc 2017-2-6
        //医诊专家无理赔奖励，实收数据打9折
        String Risksql2 = "select riskcode from lcpol where contno = '" + mLJSPayBL.getOtherNo() + "' and riskcode in (select code from ldcode1 where codetype='WLPYH')"; 
        String resultRisk2 = tExeSQL.getOneValue(Risksql2);
        String WLPsql2= "select code,code1 from ldcode1 where codetype='WLPYH' and code = '" + resultRisk2 + "'";
        SSRS tLDCode1SSRS2 = null;
        tLDCode1SSRS2 = tExeSQL.execSQL(WLPsql2);
        if(tLDCode1SSRS2.getMaxRow()>0){
        	String premsql = "select sum(prem) from lcpol where polno in (select newpolno from LCRnewStateLog where state !='6' and polno in (select polno from ljspayperson where contno = '" + mLJSPayBL.getOtherNo() + "' and riskcode='" + tLDCode1SSRS2.GetText(1, 1) + "' and paytype='ZC'))";
        	String riskprem = new ExeSQL().getOneValue(premsql);
        	if(!"".equals(riskprem) && !"null".equals(riskprem)){
        		double a = Double.parseDouble(riskprem);
        		String sqlljs = "select sum(sumDuePayMoney) from LJSPayPersonB where contno = '" + mLJSPayBL.getOtherNo() + "' and paytype = 'ZC' and getnoticeno='" + mLJSPayBL.getGetNoticeNo() +"' and riskcode='" + tLDCode1SSRS2.GetText(1, 1) + "'";
                double ljsprem = Double.parseDouble(tExeSQL.getOneValue(sqlljs));
                double premRate = Double.parseDouble(tLDCode1SSRS2.GetText(1, 2));
                double LjsMoney = ljsprem / premRate;
                BigDecimal c= new BigDecimal(LjsMoney);
            	c = c.setScale(2,BigDecimal.ROUND_HALF_UP);
            	double d = c.doubleValue() + 0.01;
            	double e = c.doubleValue() - 0.01;
            	double f = c.doubleValue();
            	String sql = "select 1 from LJSPayPersonB where contno='"+ mLJSPayBL.getOtherNo() +"' and getnoticeno='"+ mLJSPayBL.getGetNoticeNo() +"' and riskcode='" + tLDCode1SSRS2.GetText(1, 1) + "'";
        		String RNO = tExeSQL.getOneValue(sql);
        		if("1".equals(RNO)){
                	if(a == d || a == e || a == f  ){
                		String sumprem = "select sum(sumDuePayMoney) from LJSPayPersonB where contno = '" + mLJSPayBL.getOtherNo() + "' and paytype = 'ZC' and getnoticeno='" + mLJSPayBL.getGetNoticeNo() +"'";
                		String sum = new ExeSQL().getOneValue(sumprem);
                		double sump = Double.parseDouble(sum);  
                		mLJAPayBL.setSumActuPayMoney(sump); // 本期总实交金额
                	} else {
                		mLJAPayBL.setSumActuPayMoney(ContactuMoney); // 本期总实交金额
                	}
                } else {
                	mLJAPayBL.setSumActuPayMoney(ContactuMoney); // 本期总实交金额
                } 
        	} else {
        		mLJAPayBL.setSumActuPayMoney(ContactuMoney); // 本期总实交金额
        	}
        } else {
    		mLJAPayBL.setSumActuPayMoney(ContactuMoney); // 本期总实交金额
    	}
        
    	    
        mLJAPayBL.setStartPayDate(mLJSPayBL.getStartPayDate());
        if (actuVerifyFlag) {
            mLJAPayBL.setEnterAccDate(mEnterAccDate); // 到帐日期
            mLJAPayBL.setConfDate(mConfDate); // 确认日期
        } else {
            // 如果不需要自动核销
            mLJAPayBL.setEnterAccDate(mEnterAccDate); // 到帐日期
            mLJAPayBL.setConfDate(mConfDate); // 确认日期
        }
        mLJAPayBL.setGetNoticeNo(mLJSPayBL.getGetNoticeNo()); // 交费通知书号码
        mLJAPayBL.setPayDate(mPayDate); // 交费日期
        mLJAPayBL.setApproveCode(mLJSPayBL.getApproveCode()); // 复核人编码
        mLJAPayBL.setApproveDate(mLJSPayBL.getApproveDate()); // 复核日期
        mLJAPayBL.setSerialNo(serNo); // 流水号
        // mLJAPayBL.setOperator(mLJSPayBL.getOperator()); // 操作员
        mLJAPayBL.setOperator(mGI.Operator); // 实际操作员
        mLJAPayBL.setMakeDate(CurrentDate); // 入机时间
        mLJAPayBL.setMakeTime(CurrentTime); // 入机时间
        mLJAPayBL.setModifyDate(CurrentDate); // 最后一次修改日期
        mLJAPayBL.setModifyTime(CurrentTime); // 最后一次修改时间
        mLJAPayBL.setBankCode(mLJSPayBL.getBankCode()); // 银行编码
        mLJAPayBL.setBankAccNo(mLJSPayBL.getBankAccNo()); // 银行帐号
        mLJAPayBL.setAccName(mLJSPayBL.getAccName()); // 账户名
        mLJAPayBL.setRiskCode(mLJSPayBL.getRiskCode()); // 险种编码
        mLJAPayBL.setManageCom(mLJSPayBL.getManageCom()); // 管理机构
        mLJAPayBL.setAgentCode(mLJSPayBL.getAgentCode()); // 代理人
        mLJAPayBL.setAgentGroup(mLJSPayBL.getAgentGroup()); // 营业组
        mLJAPayBL.setDueFeeType("1"); // 续期，续保收费 add by fuxin 2008-5-28/财务新接口
        //20110523添加LJAPay.Agentcom的值，使其和LJSPay.Agentcom保持一致即可
        mLJAPayBL.setAgentCom(mLJSPayBL.getAgentCom()); 
        mLJAPayBL.setSaleChnl(mLJSPayBL.getSaleChnl());
        mLJAPayBL.setMarketType(mLJSPayBL.getMarketType());

        tSBql = new StringBuffer(128);
        tSBql.append(
                "UPDATE  LJSPayB SET  DealState ='1' , ConfFlag='1'")
                .append(" , ModifyDate ='")
                .append(CurrentDate)
                .append("' , ModifyTime ='")
                .append(CurrentTime)
                .append("'")
                .append(" where GetNoticeNo='")
                .append(mLJSPayBL.getGetNoticeNo())
                .append("'")
                ;
        mLJSPayBSql = tSBql.toString();
        System.out.println(" 续期核销时，更新LJSPayB");

        tSBql = new StringBuffer(128);
        tSBql.append(
                "UPDATE  LJSPayPersonB SET  DealState ='1' , ConfFlag='1'")
                .append(" , ModifyDate ='")
                .append(CurrentDate)
                .append("' , ModifyTime ='")
                .append(CurrentTime)
                .append("'")
                .append(" where GetNoticeNo='")
                .append(mLJSPayBL.getGetNoticeNo())
                .append("'")
                ;
        mLJSPayPerBSql = tSBql.toString();
        System.out.println(" 续期核销时，更新LJSPayPersonB");

        return true;

    }

    /**
     * 更改抵扣余额的状态为有效
     * @return boolean：成功true，否则false
     */
    private MMap dealAccState()
    {
        LCAppAccDB tLCAppAccDB = new LCAppAccDB();
        tLCAppAccDB.setCustomerNo(mLCContSchema.getAppntNo());
        if(!tLCAppAccDB.getInfo())
        {
            return null;
        }

        LCAppAccTraceDB tOldLCAppAccTraceDB = new LCAppAccTraceDB();
        tOldLCAppAccTraceDB.setCustomerNo(mLCContSchema.getAppntNo());
        tOldLCAppAccTraceDB.setOtherNo(mLCContSchema.getContNo());
        tOldLCAppAccTraceDB.setOtherType("2");
        tOldLCAppAccTraceDB.setAccType("0");
        tOldLCAppAccTraceDB.setDestSource("02");
        tOldLCAppAccTraceDB.setBakNo(mLJSPaySchema.getGetNoticeNo());
        LCAppAccTraceSet tOldLCAppAccTraceSet = tOldLCAppAccTraceDB.query();

        //若没有续期
        if(tOldLCAppAccTraceSet.size() == 0)
        {
            return null;
        }

        LCAppAccTraceSchema tOldLCAppAccTraceShema
            = tOldLCAppAccTraceSet.get(1).getSchema();

        //抵扣账户余额
        tLCAppAccDB.setAccBala(tLCAppAccDB.getAccBala()
            + tOldLCAppAccTraceShema.getMoney());  //续期抵扣金额为负
        tLCAppAccDB.setOperator(this.mGI.Operator);
        tLCAppAccDB.setModifyDate(this.CurrentDate);
        tLCAppAccDB.setModifyTime(this.CurrentTime);

        //余额使用轨迹生效
        tOldLCAppAccTraceShema.setState("1");
        tOldLCAppAccTraceShema.setAccBala(tOldLCAppAccTraceShema.getAccBala()
            + tOldLCAppAccTraceShema.getMoney());  //续期抵扣金额为负
        tOldLCAppAccTraceShema.setOperator(this.mGI.Operator);
        tOldLCAppAccTraceShema.setModifyDate(this.CurrentDate);
        tOldLCAppAccTraceShema.setModifyTime(this.CurrentTime);

        MMap tMap = new MMap();
        tMap.put(tLCAppAccDB.getSchema(), SysConst.UPDATE);
        tMap.put(tOldLCAppAccTraceShema, SysConst.UPDATE);

        return tMap;
    }

    /**
     * 往帐户里增钱
     * @param accBala double
     * @return boolean
     */
    private boolean addAccBala(double accBala)
    {
        AppAcc tAppAcc = new AppAcc();

        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
        tLCAppAccTraceSchema.setCustomerNo(mLCContSchema.getAppntNo());
        tLCAppAccTraceSchema.setAccType("1");
        tLCAppAccTraceSchema.setOtherNo(mLCContSchema.getContNo());
        tLCAppAccTraceSchema.setBakNo(mLJSPayBL.getGetNoticeNo());
        tLCAppAccTraceSchema.setOtherType("2");
        tLCAppAccTraceSchema.setMoney(accBala);
        tLCAppAccTraceSchema.setOperator(mGI.Operator);
        MMap tMap = tAppAcc.accShiftToXSYIJ(tLCAppAccTraceSchema);
        if(tMap == null)
        {
            mErrors.copyAllErrors(tAppAcc.mErrors);
            return false;
        }
        map.add(tMap);

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * @param mInputData：包括
     * 1、LCContSchema：保单信息
     * 2、GlobalInput：操作员信息
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) {

        mGI = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
        mTransferData = (TransferData) mInputData
                        .getObjectByObjectName("TransferData", 0);
         mGetNoticeNo =(String) mTransferData.getValueByName("GetNoticeNo");
        System.out.println("你得什么的干活："+mGetNoticeNo);
        LCContSchema tLCContSchema = new LCContSchema();
        tLCContSchema = (LCContSchema) mInputData
                        .getObjectByObjectName("LCContSchema", 0);
        if (tLCContSchema == null) {
            CError.buildErr(this, "页面数据传入失败!");
            return false;
        }
        // 传入保单号或者暂交费号
        // 2005-09-13说明:允许多次预交续期保费,直接根据合同号做核销动作
        //if (mLJTempFeeBL.getOtherNo() != null) {
        // 保单合同号
        // 0-查询保单表
        String tContNo = tLCContSchema.getContNo();
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(tContNo);
        // 校验合同表，如果查询失败，则保单已经解约，或者其他原因，所以此保单不能交费
        if (!tLCContDB.getInfo()) {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCContDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "IndiFinUrgeVerifyBL";
            tError.functionName = "dealData";
            tError.errorMessage = "保单信息不存在，请查询!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLCContSchema.setSchema(tLCContDB.getSchema());

        double sumDueMoney = 0.0;
        // 不能仅仅根据合同号＆类型进行查询，还需要对应收的顺序做排序，以便核销第一笔数据
        // 同时需要添加交至日的条件，应为如果未到交至日，则需要将交费放入余额
        StringBuffer tSBql = new StringBuffer(128);
        tSBql.append(
                "select * from LJSPay where OtherNoType = '2' and GetNoticeNO = '");
        tSBql.append(mGetNoticeNo);
        tSBql.append("'");
        // tSBql.append("' and StartPayDate <= '");
        //tSBql.append(CurrentDate);
        //tSBql.append("' order by StartPayDate");
        LJSPayDB tLJSPayDB = new LJSPayDB();
        LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(tSBql.toString());
        // 如果发现没有应收记录，有两种情况产生：
        // 1。没有产生应收
        // 2。产生了应收，但是没有到核销日
        // 无论是上面的那种情况产生，都需要进行余额更新、应收记录更新（若有），不进行核销操作，也不允许打印发票等信息
        if (tLJSPaySet == null || tLJSPaySet.size() <= 0) {
            // 只有现金交费才会产生这种情况
            // 表示此时不需要进行核销处理
            mUrgeFlag = false;
            CError.buildErr(this, "系统不存在满足条件的应收记录!");
            return false;

        }
        // 如果查询到应收数据，则取出本期应交保费总金额
        sumDueMoney = tLJSPaySet.get(1).getSumDuePayMoney();
        // 获得最早一笔需要核销的应收记录
        mLJSPayBL.setSchema(tLJSPaySet.get(1).getSchema());

        // 这个判定还有待考虑
        /* if ("N".equals(tLJSPaySet.get(1).getExtendFlag())) {
             CError.buildErr(this,
                             "保单：" + tLJSPaySet.get(1).getOtherNo() +
                             "正在做保全或理赔项目，暂时不允许收费。");
             return false;
         }*/

        double tempMoney = 0.0; // 交费的金额
        boolean tNeedPay = true; // 是否需要交费
        // 如果应交总额为０，则不需要有暂交费信息（即不需要预交续期保费），且存在应收记录
        if (sumDueMoney <= 0.0 && mUrgeFlag) {
            // return true;
            // 设置交费标志为可不交费
            tNeedPay = false;
        }

        // 查询此保单下的所有未核销的暂收交费记录
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        tSBql = new StringBuffer(128);
        tSBql.append(
                "select * from ljtempfee where TempFeeType='2' and TempFeeNo='");
        tSBql.append(mLJSPayBL.getGetNoticeNo());
        tSBql.append(
                "' and (ConfFlag='0' or ConfFlag is null) and EnterAccDate is not null");
        System.out.println(tSBql.toString());
        mLJTempFeeSet = tLJTempFeeDB.executeQuery(tSBql.toString());
        if (mLJTempFeeSet == null || mLJTempFeeSet.size() == 0) {
            // 只有当需要交费，且没有交费记录的时候才报错
            if (tNeedPay) {
                CError.buildErr(this, "系统不存在满足条件的预交费记录!");
                return false;
            }

            //需要财务缴费，将所需的财务日期设为核销当天
            mPayDate = CurrentDate;
            mEnterAccDate = CurrentDate;
        } else {
            // 查询此保单下的所有未核销的暂收交费记录
            LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
            tSBql = new StringBuffer(128);
            tSBql.append("select * from ljtempfeeclass where TempFeeNo in (select TempFeeNo from ljtempfee where TempFeeType='2' and TempFeeNo='");
            tSBql.append(mLJSPayBL.getGetNoticeNo());
            tSBql.append(
                    "' and (ConfFlag='0' or ConfFlag is null) and EnterAccDate is not null)");
            mLJTempFeeClassSet = tLJTempFeeClassDB.executeQuery(tSBql.
                    toString());
            if (mLJTempFeeClassSet == null) {
                CError.buildErr(this, "系统不存在满足条件的预交费记录!");
                return false;
            }
            // 判定交费方式
            if (mLJTempFeeClassSet.get(1).getPayMode().equals("4")) {
                // 设置交费方式为转账模式
                mPayMode = false;
            }
            //取得缴费日期
            mPayDate = mLJTempFeeSet.get(1).getPayDate();
            mEnterAccDate = mLJTempFeeSet.get(1).getEnterAccDate();
            tempMoney = 0.0;
            for (int i = 1; i <= mLJTempFeeSet.size(); i++) {
                // 该保单下实际交费金额
                tempMoney += mLJTempFeeSet.get(i).getPayMoney();

            }
        }
        mConfDate = CurrentDate;

        // 如果实交金额不足以核销本次收费，则返回错误信息
        // 产生这种情况的时候只可能是现金交费，此时需要将交费金额放入保单帐户余额中
        // 已经确认，当产生应收的时候，现金交费必须足够核销一期应收金额
        if (tempMoney < sumDueMoney) {
            mUrgeFlag = false;
            return true;
        }

        //  }

        String sql = "  select edorNo,b.edortype "
                     + "from LPEdorApp a,LPEdorItem b "
                     + "where a.edorAcceptNo = b.edorNo "
                     + "   and a.edorState != '" + BQ.EDORSTATE_CONFIRM + "' "
                     + "   and b.contNo = '" + mLCContSchema.getContNo() + "' ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        String edorNos = "";
        boolean xbHbFlag = false;
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            edorNos += tSSRS.GetText(i, 1);
          //若保全项目为理赔续保核保，则校验续保抽挡时间与续保核保时间的先后
        	//校验保全项目是否为理赔续保核保
        	if("XB".equals(tSSRS.GetText(i, 2))){
        		String uwSQL = "select * from LWMission where ProcessId = '0000000003'  and ActivityId = '0000001181' " +
        				" and Missionprop5 in (select caseno from llclaimpolicy where contno='"+mLCContSchema.getContNo()+"') with ur ";
        		LWMissionDB tLWMissionDB = new LWMissionDB();
        		LWMissionSet tLWMissionSet = new LWMissionSet();
        		tLWMissionSet = tLWMissionDB.executeQuery(uwSQL);
        		if(tLWMissionSet!=null&&tLWMissionSet.size()>0){
        			//续保核保时间
        			String uwDate = tLWMissionSet.get(1).getMakeDate();
        			//查询续保抽挡时间
        	        String xbDate = tLJSPaySet.get(1).getMakeDate();
        	        
        	        //判断续保核保时间与续保抽挡时间的先后
        	        if(CommonBL.stringToDate(xbDate).before(CommonBL.stringToDate(uwDate))){
        	        	System.out.println("续保抽挡在续保核保之前，续保通过");
        	        	xbHbFlag = true;
        	        }else if(xbDate.equals(uwDate)){
        	        	if(tLWMissionSet.get(1).getMakeTime().compareTo(tLJSPaySet.get(1).getMakeTime())>0){
        	        	  	System.out.println("续保抽挡在续保核保之前，续保通过");
        	        	  	xbHbFlag = true;
        	        	}
        	        }
        		}
        		
        	}
        }
        if(!edorNos.equals(""))
        {
        	if(xbHbFlag){
        		System.out.println("续保抽挡在续保核保之前，续保通过");
        	}else{
            mErrors.addOneError("保单" + mLCContSchema.getContNo()
                                + "有未完成的保全受理，不能进行核销操作：" + edorNos);
            return false;
        	}
        }

        return true;
    }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData() {
        mInputData = new VData();
        try {
//        	20090409 zhanggm 避免同一应收收成条实收记录
        	MMap tCekMap = null;
        	tCekMap = lockLJAPay(mGetNoticeNo);
            if (tCekMap == null)
            {
                return false;
            }
            map.add(tCekMap);
//    --------------------------
            // if(mLOPRTManagerSchemaNew!=null)
            // {
            //   map.put(mLOPRTManagerSchemaNew, "UPDATE");
            // }
            mLJAPaySchema.setSchema(mLJAPayBL);
            mLJSPaySchema.setSchema(mLJSPayBL);
            mLJTempFeeSchema.setSchema(mLJTempFeeBL);
            map.put(mLJAPaySchema, "INSERT");
            map.put(mLJSPaySchema, "DELETE");
            map.put(mLJTempFeeNewSet, "UPDATE");
            map.put(mLJTempFeeClassNewSet, "UPDATE");
            map.put(mLJAPayPersonSet, "INSERT");
            map.put(mLJSPayPersonSet, "DELETE");
            // qulq modify 2007-3-16 如果是续保保单就不用更新原保单信息。


            if(!isXBCont(mLCContSchema.getContNo()))
            {
           	 map.put(mLCContSchema, "UPDATE");
            }
            map.put(mLCPolSet, "UPDATE");
            map.put(mLCPremNewSet, "UPDATE");
            map.put(mLCDutyNewSet, "UPDATE");
            map.put(mLJSPayBSql, "UPDATE");
            map.put(mLJSPayPerBSql, "UPDATE");

            MMap tMMap = dealAccState();
            map.add(tMMap);

            mInputData.add(map);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "IndiFinUrgeVerifyBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 公共核销程序
     * @param TempFeeNo 暂交费号
     * @return 包含纪录的集合(纪录如何处理具体见prepareOutputData函数)
     */
    public VData ReturnData(String TempFeeNo) {
        if (TempFeeNo == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "IndiFinUrgeVerifyBL";
            tError.functionName = "ReturnData";
            tError.errorMessage = "传入暂交费号不能为空";
            this.mErrors.addOneError(tError);
            return null;
        }

//1-查询暂交费表，将TempFeeNo输入Schema中传入，查询得到Set集
        VData tVData = new VData();
        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
        LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
        TempFeeQueryUI tTempFeeQueryUI = new TempFeeQueryUI();
        tLJTempFeeSchema.setTempFeeNo(TempFeeNo);
        tLJTempFeeSchema.setTempFeeType("2"); //交费类型为2：续期催收交费
        tVData.add(tLJTempFeeSchema);
        if (!tTempFeeQueryUI.submitData(tVData, "QUERY")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tTempFeeQueryUI.mErrors);
            CError tError = new CError();
            tError.moduleName = "IndiFinUrgeVerifyBL";
            tError.functionName = "ReturnData";
            tError.errorMessage = "暂交费查询失败";
            this.mErrors.addOneError(tError);
            return null;
        }
        tVData.clear();
        tVData = tTempFeeQueryUI.getResult();
        tLJTempFeeSet.set((LJTempFeeSet) tVData.getObjectByObjectName(
                "LJTempFeeSet", 0));
        tLJTempFeeSchema = (LJTempFeeSchema) tLJTempFeeSet.get(1);
        double tempMoney = tLJTempFeeSchema.getPayMoney();
//2-查询应收总表
        tVData.clear();
        LJSPaySchema tLJSPaySchema = new LJSPaySchema();
        LJSPaySet tLJSPaySet = new LJSPaySet();
        VerDuePayFeeQueryUI tVerDuePayFeeQueryUI = new VerDuePayFeeQueryUI();
        tLJSPaySchema.setGetNoticeNo(TempFeeNo);
        tVData.add(tLJSPaySchema);
        if (!tVerDuePayFeeQueryUI.submitData(tVData, "QUERY")) {
            // @@错误处理
            this.mErrors.copyAllErrors(tVerDuePayFeeQueryUI.mErrors);
            CError tError = new CError();
            tError.moduleName = "IndiFinUrgeVerifyBL";
            tError.functionName = "ReturnData";
            tError.errorMessage = "应收总查询失败";
            this.mErrors.addOneError(tError);
            return null;
        }
        tVData.clear();
        tVData = tVerDuePayFeeQueryUI.getResult();
        tLJSPaySet.set((LJSPaySet) tVData.getObjectByObjectName("LJSPaySet",
                0));
        tLJSPaySchema = (LJSPaySchema) tLJSPaySet.get(1);
        double sumDueMoney = tLJSPaySchema.getSumDuePayMoney();
        if (sumDueMoney != tempMoney) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "IndiFinUrgeVerifyBL";
            tError.functionName = "ReturnData";
            tError.errorMessage = "应收总表纪录中的金额和暂交费纪录中的金额不相等！";
            this.mErrors.addOneError(tError);
            return null;
        }
        if (tLJTempFeeSchema.getEnterAccDate() == null) {
            CError tError = new CError();
            tError.moduleName = "IndiFinUrgeVerifyBL";
            tError.functionName = "ReturnData";
            tError.errorMessage = "财务缴费还没有到帐!（暂交费收据号：" +
                                  tLJTempFeeSchema.getTempFeeNo().trim() +
                                  "）";
            this.mErrors.addOneError(tError);
            return null;
        }
//      if(PubFun.calInterval(PubFun.getCurrentDate(),tLJSPaySchema.getPayDate(),"D")<0)
//      {
//          CError tError = new CError();
//          tError.moduleName = "IndiFinUrgeVerifyBL";
//          tError.functionName = "ReturnData";
//          tError.errorMessage = "保单已过失效日期:应收纪录超过失效期核销 ";
//          this.mErrors .addOneError(tError) ;
//          return null;
//      }
        tVData.clear();
        tVData.add(tLJTempFeeSchema);
        tVData.add(tLJSPaySchema);
//3-调用核销程序
        this.mOperate = "VERIFY";
        if (!getInputData(tVData)) {
            return null;
        }

        //进行业务处理
        if (!dealData()) {
            return null;
        }
        System.out.println("After dealData！");
        //准备往后台的数据
        if (!prepareOutputData()) {
            return null;
        }

        return mInputData;
    }

    /**
     * 得到原责任信息
     * @param tLCPolSchema LCPolSchema:当前险种信息
     * @return LCPolSchema：原险种信息
     */
    private LCPolSchema getOldLCPolSchema(LCPolSchema tLCPolSchema)
    {
        //如果不是续保险种，则直接返回
        if(!isXBPol(tLCPolSchema.getPolNo()))
        {
            return tLCPolSchema;
        }

        StringBuffer sql = new StringBuffer();
        sql.append("select a.* ")
            .append("from LCPol a, LCRnewStateLog b ")
            .append("where a.polNo = b.polNo ")
            .append("   and a.renewCount = b.renewCount -1 ")  //原险种续保次数比续保临时数据少1次
            .append("   and b.newPolNo = '")
            .append(tLCPolSchema.getPolNo()).append("' ");
        System.out.println(sql.toString());

        LCPolSet set = new LCPolDB().executeQuery(sql.toString());
        if(set.size() == 0)
        {
            mErrors.addOneError("险种" + tLCPolSchema.getRiskCode()
                                + "是续保险种，但是没有查询到原险种信息。");
            return null;
        }
        return set.get(1);
    }

    /**
     * 校验险种是否续保险种
     * @param polNo String: 险种号
     * @return boolean:是续保险种true，否则false
     */
    private boolean isXBPol(String polNo)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select 1 ")
            .append("from LCRnewStateLog ")
            .append("where newPolNo = '").append(polNo)
            .append("'  and state != '")
            .append(XBConst.RNEWSTATE_DELIVERED).append("' ");
        String rs = new ExeSQL().getOneValue(sql.toString());
        if(!rs.equals("") && !rs.equals("null"))
        {
            return true;
        }

        return false;
    }
    /**
     * 校验险种是否续保险种
     * @param polNo String: 险种号
     * @return boolean:是续保险种true，否则false
     */
    private boolean isXBCont(String contNo)
    {
        StringBuffer sql = new StringBuffer();
        sql.append("select 1 ")
            .append("from LCRnewStateLog ")
            .append("where ContNo = '").append(contNo)
            .append("'  and state != '")
            .append(XBConst.RNEWSTATE_DELIVERED).append("' ");
        String rs = new ExeSQL().getOneValue(sql.toString());
        if(!rs.equals("") && !rs.equals("null"))
        {
            return true;
        }

        return false;
    }
    
    /**
     * 锁定动作
     * @param cGetNoticeNo
     * @return 
     */
    private MMap lockLJAPay(String cGetNoticeNo)
    {
        MMap tMMap = null;
        /**实收核销标志"HX"*/
        String tLockNoType = "HX";
        /**锁定有效时间*/
        String tAIS = "3600";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", cGetNoticeNo);
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(mGI);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }
        return tMMap;
    }
}
