package com.sinosoft.lis.operfee;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: lis</p>
 *
 * <p>Description:
 * 处理回访结果
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class IndiVisitLJSCancelBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = new GlobalInput();
    private LGPhoneHastenSchema mLGPhoneHastenSchema = null;

    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();

    /** 数据表  保存数据*/
    private MMap map = new MMap();

    public IndiVisitLJSCancelBL()
    {
    }

    /**
     * 的存储回访处理结果集
     * @param cInputData VData：传入的数据待处理数据机和，包含：
     * 1、LGPhoneHastenSchema对象，回访结果结果，
     * 需要WorkNo,GetNoticeNo
     * 2、GlobalInput对象，完整的用户信息
     * @param cOperate String：此为""
     * @return MMap:处理后的数据集合
     */
    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }
        if(!checkData())
        {
            return null;
        }
        //进行业务处理
        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 校验录入的数据是否符合业务需求
     * 结案的催缴不可更改答案
     * @return boolean：验证通过true，否则false
     */
    private boolean checkData()
    {
        LGPhoneHastenDB tLGPhoneHastenDB = new LGPhoneHastenDB();
        tLGPhoneHastenDB.setWorkNo(mLGPhoneHastenSchema.getWorkNo());
        if(!tLGPhoneHastenDB.getInfo())
        {
            mErrors.addOneError("查询催缴任务失败");
            return false;
        }
        if(tLGPhoneHastenDB.getFinishType() != null
           && (tLGPhoneHastenDB.getFinishType()
               .equals(FeeConst.HASTEN_FINISHTYPE_CANCEL)
               || tLGPhoneHastenDB.getFinishType()
               .equals(FeeConst.HASTEN_FINISHTYPE_REDATE)))
        {
            mErrors.addOneError("已录入回访结果，不能再次录入");
            return false;
        }

        return true;
    }

    /**
     * 处理回访结果
     * @return boolean：处理成功true，否则false
     */
    private boolean dealData()
    {
        if(!calcelLJSPay())
        {
            return false;
        }

        if(!dealLGPhone())
        {
            return false;
        }

        return true;
    }

    /**
     * dealLGPhone
     * 处理催缴工单信息
     * @return boolean:处理成功true,否则false
     */
    private boolean dealLGPhone()
    {
        String sql = "  update LGPhoneHasten "
                     + "set FinishType = '" + FeeConst.HASTEN_FINISHTYPE_CANCEL + "', "
                     + "   operator = '" + this.mGlobalInput.Operator + "', "
                     + "   modifyDate = '" + this.CurrentDate + "', "
                     + "   modifyTime = '" + this.CurrentTime + "' "
                     + "where workNo = '" + mLGPhoneHastenSchema.getWorkNo() + "' "
                     + "   and getNoticeNo = '"
                     + mLGPhoneHastenSchema.getGetNoticeNo() + "' ";
        map.put(sql, "UPDATE");

        return true;
    }

    /**
     * 撤销应收记录,调用IndiLJSCancelBL进行应收数据的撤销
     * @return boolean：撤销成功true，否则false
     */
    private boolean calcelLJSPay()
    {
        LJSPaySchema tLJSPaySchema = new LJSPaySchema();
        tLJSPaySchema.setGetNoticeNo(mLGPhoneHastenSchema.getGetNoticeNo());

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("CancelMode", "3"); //撤销
        tTransferData.setNameAndValue("IsRepeal", "Y");

        VData tVData = new VData();
        tVData.addElement(tLJSPaySchema);
        tVData.addElement(mGlobalInput);
        tVData.addElement(tTransferData);

        IndiLJSCancelBL tIndiLJSCancelBL = new IndiLJSCancelBL();
        MMap tMMap = tIndiLJSCancelBL.getSubmitMap(tVData, "INSERT");
        if(tMMap == null)
        {
            mErrors.copyAllErrors(tIndiLJSCancelBL.mCErrors);
            return false;
        }
        map.add(tMMap);

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * submitData中传入的对象
     * @return boolean: 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData data)
    {
        mGlobalInput = ((GlobalInput) data
                        .getObjectByObjectName("GlobalInput", 0));
        mLGPhoneHastenSchema = (LGPhoneHastenSchema) data.
                               getObjectByObjectName("LGPhoneHastenSchema", 0);

        if(mGlobalInput == null || mLGPhoneHastenSchema == null
           || mLGPhoneHastenSchema.getWorkNo() == null
           || mLGPhoneHastenSchema.getWorkNo().equals("")
           || mLGPhoneHastenSchema.getGetNoticeNo() == null
           || mLGPhoneHastenSchema.getGetNoticeNo().equals(""))
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpPhoneVisitOverBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 的存储回访处理结果集
     * @param cInputData VData：传入的数据待处理数据机和，包含：
     * 1、LGPhoneHastenSchema对象，回访结果结果，
     * 需要WorkNo,GetNoticeNo
     * 2、GlobalInput对象，完整的用户信息
     * @param cOperate String：此为""
     * @return MMap:处理后的数据集合
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        MMap map = getSubmitMap(cInputData, cOperate);
        if((map == null || map.size() == 0) && mErrors.needDealError())
        {
            return false;
        }

        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if(tPubSubmit.submitData(data, "") == false)
        {
            mErrors.addOneError("更新数据失败");
            return false;
        }

        return true;
    }


    public static void main(String[] args)
    {
        LGPhoneHastenSchema tLGPhoneHastenSchema = new LGPhoneHastenSchema();
        tLGPhoneHastenSchema.setGetNoticeNo("31000000592");
        tLGPhoneHastenSchema.setWorkNo("20060829000006");

        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "pa0004";
        tGI.ComCode = "86";

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("CancelMode", "0");

        VData tVData = new VData();
        tVData.addElement(tLGPhoneHastenSchema);
        tVData.addElement(tGI);

       IndiVisitLJSCancelBL bl = new IndiVisitLJSCancelBL();
       bl.submitData(tVData, "INSERT");

    }
}
