package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class CollTimePayPersonBL  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData;
   private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;
  private GlobalInput mGI;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private MMap mMMap = new MMap();
//  private String serNo="";//流水号
//  private String tLimit="";

//应收个人交费表
  private LJSPayPersonSet    mLJSPayPersonSet         = new LJSPayPersonSet();
  private LJSPayPersonSet    mLJSPayPersonUpdateSet   = new LJSPayPersonSet();
  private LJSPayPersonSet    mLJSPayPersonNewSet      = new LJSPayPersonSet();
  //业务处理相关变量
  public CollTimePayPersonBL() {
  }
  public static void main(String[] args) {

  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    this.mOperate =cOperate;

    if (!getInputData(cInputData))
      return false;

    if (!dealData())
      return false;

    if(!prepareData())
      return false;
    PubSubmit tPubSubmit = new PubSubmit();
    if (tPubSubmit.submitData(mResult, "") == false)
       {
          // System.out.println("delete fail!");
          CError.buildErr(this, "系统处理数据异常！");
          return false;
       }

    /*
    if (!prepareOutputData())
      return false;

    CollTimePayPersonBLS tCollTimePayPersonBLS=new CollTimePayPersonBLS();
    tCollTimePayPersonBLS.submitData(mInputData,cOperate);

    //如果有需要处理的错误，则返回
    if (tCollTimePayPersonBLS.mErrors .needDealError())
    {
        this.mErrors .copyAllErrors(tCollTimePayPersonBLS.mErrors ) ;
    }
   */
    mInputData=null;
    return true;
  }
  /**
   * 杨红于2005-07-21创建新的dealData的方法
   * @return boolean
   */
  private boolean dealData()
  {
    for(int i=1;i<=mLJSPayPersonSet.size();i++)
    {
      LJSPayPersonDB  tLJSPayPersonDB=new LJSPayPersonDB();
      String polNo=mLJSPayPersonSet.get(i).getPolNo();
      tLJSPayPersonDB.setPolNo(polNo);
      LJSPayPersonSet tempLJSPayPersonSet=new LJSPayPersonSet();
      tempLJSPayPersonSet=tLJSPayPersonDB.query();
      if(tempLJSPayPersonSet.size()==0)
      {
        CError.buildErr(this, "未查询到应收个人数据！");
        return false;
      }
      for(int m=1;m<=tempLJSPayPersonSet.size();m++)
      {
        LJSPayPersonSchema  LJSPayPersonSchema=tempLJSPayPersonSet.get(m).getSchema();
        LJSPayPersonSchema.setInputFlag(mLJSPayPersonSet.get(i).getInputFlag());
        mLJSPayPersonNewSet.add(LJSPayPersonSchema);
      }
     /* if(!tLJSPayPersonDB.getInfo())
      {
        CError.buildErr(this, "未查询到应收个人数据！");
        return false;
      }*/
     // LJSPayPersonSchema  LJSPayPersonSchema=tLJSPayPersonDB.getSchema();
     // LJSPayPersonSchema.setInputFlag(mLJSPayPersonSet.get(i).getInputFlag());
     // mLJSPayPersonNewSet.add(LJSPayPersonSchema);
    }
    return true;
  }
  private boolean prepareData()
  {
    mMMap.put(mLJSPayPersonNewSet,"UPDATE");
    mResult.add(mMMap);
    return true;
  }
  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
 /** private boolean dealData()
  {
   boolean tReturn =false;
   if(this.mOperate.equals("INSERT"))
   {
       //查询应收个人表:更新同一保单号下的多条纪录
       LJSPayPersonBL tLJSPayPersonBL;
       LJSPayPersonSet tempLJSPayPersonSet = new LJSPayPersonSet();
       int iMax=mLJSPayPersonSet.size() ;
       String sqlStr="";
       String PolNo="";
       String PayAimClass="";
       for (int i=1;i<=iMax;i++)
       {
           tLJSPayPersonBL=new LJSPayPersonBL();
           tempLJSPayPersonSet = new LJSPayPersonSet();
           tLJSPayPersonBL.setSchema(mLJSPayPersonSet.get(i).getSchema());
           PolNo = tLJSPayPersonBL.getPolNo();
           LJSPayPersonDB tLJSPayPersonDB   = new LJSPayPersonDB();
           sqlStr = "select * from LJSPayPerson where PolNo='"+PolNo+"'";
           tempLJSPayPersonSet = tLJSPayPersonDB.executeQuery(sqlStr);
           if(tempLJSPayPersonSet.size()==0)
           {
               // @@错误处理
               CError tError =new CError();
               tError.moduleName="CollTimePayPersonBL";
               tError.functionName="getInputData";
               tError.errorMessage="没有找到对应的应收个人纪录，请您确认!";
               this.mErrors .addOneError(tError) ;
               return false;
           }
           else //加入更新记录集合
           {
               LJSPayPersonBL cLJSPayPersonBL = new LJSPayPersonBL();
               for(int n=1;n<=tempLJSPayPersonSet.size();n++)
               {
                   cLJSPayPersonBL = new LJSPayPersonBL();
                   cLJSPayPersonBL.setSchema(tempLJSPayPersonSet.get(n).getSchema());
                   cLJSPayPersonBL.setInputFlag(tLJSPayPersonBL.getInputFlag()); //录入标记
                   mLJSPayPersonUpdateSet.add(cLJSPayPersonBL);
               }
           }
           tReturn=true;
       }
   }
   return tReturn;
}*/

 /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData mInputData)
  {
    // 应收个人纪录集合
    mGI=((GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0));
    mLJSPayPersonSet=(LJSPayPersonSet)mInputData.getObjectByObjectName("LJSPayPersonSet",0);
    if(mGI==null)
    {
      CError.buildErr(this, "页面已超时，请重新登陆！");
      return false;
    }
    //如果应收个人表被撤销了，这里做了校验动作！
    if(mLJSPayPersonSet.size()==0)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="CollTimePayPersonBL";
      tError.functionName="getInputData";
      tError.errorMessage="没有得到足够的数据，请您确认!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
 /* private boolean prepareOutputData()
  {
    mInputData=new VData();
    try
    {
        mInputData.add(mLJSPayPersonUpdateSet);
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="CollTimePayPersonBL";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }

    return true;
  }*/
}

