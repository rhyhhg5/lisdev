package com.sinosoft.lis.operfee;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.task.Task;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.lis.bq.CommonBL;


/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class ExpirBenefitChangePayModeBL
{
    public CErrors mErrors = new CErrors();

    private GlobalInput mGI = new GlobalInput();
    private LJSGetSchema mLJSGetSchema = null;
    private LJSGetSchema mLJSGetSchemaOld = null;
    private LJSGetDrawSchema mLJSGetDrawSchema = null;

    private MMap map = new MMap();

    public ExpirBenefitChangePayModeBL()
    {
    }

    /**
     * 提交变更给付方式接口
     * @param data VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData data, String operate)
    {
        MMap tMMap = this.getSubmitMap(data, operate);
        if(tMMap == null)
        {
            return false;
        }

        VData tVData = new VData();
        tVData.add(tMMap);

        PubSubmit tPubSubmit = new PubSubmit();
        if(!tPubSubmit.submitData(tVData, ""))
        {
            System.out.println(tPubSubmit.mErrors.getErrContent());
            mErrors.addOneError("提交数库失败");
            return false;
        }

        return true;
    }

    /**
     * 提交变更给付方式接口
     * @param data VData
     * @param operate String
     * @return boolean
     */
    public MMap getSubmitMap(VData data, String operate)
    {
        if(!getInputData(data))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * checkData
     *
     * @return boolean
     */
    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData()
    {
    	//增加更新lccont表的帐户信息 modify by hyy
//        LCContDB tLCCont = new LCContDB();
//        tLCCont.setContNo(mLJSGetDrawSchema.getContNo());
//        if (!tLCCont.getInfo()) {
//            mErrors.addOneError("没有查询到应的保单信息"
//                                + mLJSGetSchema.getOtherNo());
//            return false;
//        }
//        tLCCont.setPayMode(mLJSGetSchema.getPayMode());
//        tLCCont.setBankCode(mLJSGetSchema.getBankCode());
//        tLCCont.setAccName(mLJSGetSchema.getAccName());
//        tLCCont.setBankAccNo(mLJSGetSchema.getBankAccNo());
//        tLCCont.setOperator(mGI.Operator);
//        tLCCont.setModifyDate(PubFun.getCurrentDate());
//        tLCCont.setModifyTime(PubFun.getCurrentTime());
//        //放到map里
//        map.put(tLCCont.getSchema(), SysConst.UPDATE);

        LJSGetDB tLJSGetDB = this.mLJSGetSchema.getDB();
        if(!tLJSGetDB.getInfo())
        {
            mErrors.addOneError("没有查询到对应的应付记录"
                                + tLJSGetDB.getGetNoticeNo());
            return false;
        }
        if("2".equals(tLJSGetDB.getDealState()))
        {
            mErrors.addOneError("处于应付撤销的任务不能变更给付方式"
                    + tLJSGetDB.getGetNoticeNo());
            return false;

        }

        mLJSGetSchemaOld = tLJSGetDB.getSchema();

        tLJSGetDB.setPayMode(mLJSGetSchema.getPayMode());
        tLJSGetDB.setBankCode(mLJSGetSchema.getBankCode());
        tLJSGetDB.setAccName(mLJSGetSchema.getAccName());
        tLJSGetDB.setBankAccNo(mLJSGetSchema.getBankAccNo());
        tLJSGetDB.setOperator(mGI.Operator);
        tLJSGetDB.setModifyDate(PubFun.getCurrentDate());
        tLJSGetDB.setModifyTime(PubFun.getCurrentTime());

        //将处理后的LJSGet信息赋值给mLJSGetSchema
        mLJSGetSchema = tLJSGetDB.getSchema();

        map.put(mLJSGetSchema, SysConst.UPDATE);

        LJAGetDB tLJAGetDB =new LJAGetDB();
        tLJAGetDB.setActuGetNo(tLJSGetDB.getGetNoticeNo());
        if (!tLJAGetDB.getInfo()) {
            mErrors.addOneError("没有查询到应收号对应的应付记录"
                                + tLJAGetDB.getGetNoticeNo());
            return false;
        }
        if("1".equals(tLJAGetDB.getBankOnTheWayFlag()))
        {
            mErrors.addOneError("银行在途不能修改"
                    + tLJAGetDB.getGetNoticeNo());
            return false;

        }
        // modify by fuxin 2008-5-5 已经回盘不可以做修改，因为变更给付方式会把实付锁去掉。
        if (null != tLJAGetDB.getEnterAccDate() && !tLJAGetDB.getEnterAccDate().equals(""))
        {
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitChangePayModeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "银行已经回盘不能修改!";
            this.mErrors.addOneError(tError);
            return false;
        }

        tLJAGetDB.setPayMode(mLJSGetSchema.getPayMode());
        tLJAGetDB.setBankCode(mLJSGetSchema.getBankCode());
        tLJAGetDB.setAccName(mLJSGetSchema.getAccName());
        tLJAGetDB.setBankAccNo(mLJSGetSchema.getBankAccNo());
        tLJAGetDB.setCanSendBank("0");
        tLJAGetDB.setOperator(mGI.Operator);
        tLJAGetDB.setModifyDate(PubFun.getCurrentDate());
        tLJAGetDB.setModifyTime(PubFun.getCurrentTime());
        map.put(tLJAGetDB.getSchema(), SysConst.UPDATE);

        MMap tMMap = createTask();
        if(tMMap == null)
        {
            return false;
        }
        map.add(tMMap);

        return true;
    }

    /**
     * createTask
     *
     * @return MMap
     */
    private MMap createTask()
    {
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setCustomerNo(mLJSGetSchema.getAppntNo());
        tLGWorkSchema.setInnerSource(mLJSGetSchema.getGetNoticeNo());
        tLGWorkSchema.setAcceptWayNo(Task.ACCEPTWAY_INNER);
        tLGWorkSchema.setStatusNo(Task.WORKSTATUS_DONE);
        tLGWorkSchema.setApplyName("自动");
        tLGWorkSchema.setTypeNo("03");

        String payModeNameOld = CommonBL.getCodeName("paymode", mLJSGetSchemaOld.getPayMode());
        String payModeName = CommonBL.getCodeName("paymode", mLJSGetSchema.getPayMode());
        if(payModeNameOld == null || payModeName == null)
        {
            return null;
        }

        tLGWorkSchema.setRemark("自动批注：个险给付变更付费方式，由[缴费方式"
                                + mLJSGetSchemaOld.getPayMode()
                                + "(" + payModeNameOld + "), "
                                + "银行编码" + StrTool.cTrim(mLJSGetSchemaOld.getBankCode())
                                + ", 账户名" + StrTool.cTrim(mLJSGetSchemaOld.getAccName())
                                + ", 账号" + StrTool.cTrim(mLJSGetSchemaOld.getBankAccNo())
                                + "] 变更为 [缴费方式"
                                + mLJSGetSchema.getPayMode()
                                + "(" + payModeName + "), "
                                + "银行编码" + StrTool.cTrim(mLJSGetSchema.getBankCode())
                                + ", 账户名" + StrTool.cTrim(mLJSGetSchema.getAccName())
                                + ", 账号" + StrTool.cTrim(mLJSGetSchema.getBankAccNo())
                                + "]");

        String workBoxNo = getWorkBox();
        if(workBoxNo == null)
        {
            return null;
        }

        VData tVData = new VData();
        tVData.add(tLGWorkSchema);
        tVData.add(mGI);
        tVData.add(workBoxNo);

        TaskInputBL tTaskInputBL = new TaskInputBL();
        MMap tMMap = tTaskInputBL.getSubmitData(tVData, "");
        if(tMMap == null)
        {
            mErrors.addOneError("生成工单信息失败:\n"
                + tTaskInputBL.mErrors.getFirstError());
            return null;
        }

        return tMMap;
    }

    /**
     * 得到cBankCode对应的银行名
     * @param cBankCode String
     * @return String
     */
    private String getBankName(String cBankCode)
    {
        String sql = "select BankName from LDBank "
                     + "where BankCode = '" + cBankCode + "' ";
        ExeSQL tExeSQL = new ExeSQL();
        String bankName = tExeSQL.getOneValue(sql);
        if(tExeSQL.mErrors.needDealError())
        {
            mErrors.addOneError("查询银行代码对应的银行名称出错" + cBankCode);
            return null;
        }

        return bankName;
    }

    /**
     * getWorkBox
     *
     * @return String
     */
    private String getWorkBox()
    {
        String sql = "select WorkBoxNo from LGWorkBox "
                     + "where OwnerNo = '" + mGI.Operator + "' ";
        ExeSQL tExeSQL = new ExeSQL();
        String tWorkBox = tExeSQL.getOneValue(sql);
        if(tWorkBox == null || tWorkBox.equals("") || tWorkBox.equals("null"))
        {
            mErrors.addOneError("没有查询到您的工单信息，不能继续处理业务");
            return null;
        }

        return tWorkBox;
    }

    /**
     * getInputData
     * 得到传入的数据，VData中只需要LJSGet和GI即可
     * @param data VData
     * @return boolean
     */
    private boolean getInputData(VData data)
    {
        mGI.setSchema((GlobalInput) data
                               .getObjectByObjectName("GlobalInput", 0));
        mLJSGetSchema = (LJSGetSchema) data
                        .getObjectByObjectName("LJSGetSchema", 0);

        mLJSGetDrawSchema =(LJSGetDrawSchema)data.getObjectByObjectName("LJSGetDrawSchema",0);

        if(mGI == null || mGI.Operator == null)
        {
            mErrors.addOneError("没有获取到操作员信息");
            return false;
        }

        if(mLJSGetSchema == null
           || mLJSGetSchema.getGetNoticeNo() == null
           || mLJSGetSchema.getGetNoticeNo().equals("")
           || mLJSGetSchema.getPayMode() == null
           || mLJSGetSchema.getPayMode().equals(""))
        {
            mErrors.addOneError("必须录入应付号码和付费方式");
            return false;
        }

        if(mLJSGetDrawSchema == null || mLJSGetDrawSchema.getContNo().equals(""))
        {
            mErrors.addOneError("获取LJSGetDraw信息失败！");
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        ExpirBenefitChangePayModeBL bl = new ExpirBenefitChangePayModeBL();
    }
}
