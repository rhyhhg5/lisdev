package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;
import java.lang.String;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class DuePayFeeBL  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 数据操作字符串 */
  private String mOperate;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private LJSPayBL           mLJSPayBL            = new LJSPayBL();
  private LJSPayPersonSet    mLJSPayPersonSet     = new LJSPayPersonSet();
  private LJSPayPersonSet    mLJSPayPersonSetNew  = new LJSPayPersonSet();
  private LOPRTManagerSchema mLOPRTManagerSchema  = new LOPRTManagerSchema();
  private LOPRTManagerSubSchema mLOPRTManagerSubSchema  = new LOPRTManagerSubSchema();


  //业务处理相关变量

//  private LCPremBL        mLCPremBL       = new LCPremBL();

  public DuePayFeeBL() {
  }
  public static void main(String[] args) {
    DuePayFeeBL DuePayFeeBL1 = new DuePayFeeBL();
    LJSPayPersonBL mLJSPayPersonBL =new LJSPayPersonBL();
    VData tv=new VData();
    tv.add(mLJSPayPersonBL);
    DuePayFeeBL1.submitData(tv,"INSERT");
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
System.out.println("OperateData:  "+cOperate);


    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
System.out.println("After getinputdata");

    //进行业务处理
    if (!dealData())
      return false;
System.out.println("After dealData！");
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
System.out.println("After prepareOutputData");

    System.out.println("Start LJSPayPerson BL Submit...");

    DuePayFeeBLS tDuePayFeeBLS=new DuePayFeeBLS();
    tDuePayFeeBLS.submitData(mInputData,cOperate);

    System.out.println("End LJSPayPerson BL Submit...");

    //如果有需要处理的错误，则返回
    if (tDuePayFeeBLS.mErrors .needDealError())
    {
        this.mErrors .copyAllErrors(tDuePayFeeBLS.mErrors ) ;
    }

    mInputData=null;
    return true;
  }

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData()
  {
    int i=0,iMax=0;
    boolean tReturn =false;
    String tNo=""; //通知单号

    //处理个人信息数据
    //添加纪录
System.out.println("mOperate:"+mOperate);
    if(mOperate.equals("INSERT"))
    {
//1-处理应收个人交费表，记录集，循环处理
    LJSPayPersonBL  tLJSPayPersonBL;
    iMax=mLJSPayPersonSet.size() ;
System.out.println("iMax:"+iMax);
    for (i=1;i<=iMax;i++)
    {
      tLJSPayPersonBL = new LJSPayPersonBL() ;
      tLJSPayPersonBL.setSchema(mLJSPayPersonSet.get(i).getSchema());
System.out.println("panplancode:"+tLJSPayPersonBL.getPayPlanCode());
      tLJSPayPersonBL.setMakeDate(CurrentDate);//入机日期
      tLJSPayPersonBL.setMakeTime(CurrentTime);//入机时间
      tLJSPayPersonBL.setModifyDate(CurrentDate);//最后一次修改日期
      tLJSPayPersonBL.setModifyTime(CurrentTime);//最后一次修改时间
      mLJSPayPersonSetNew.add(tLJSPayPersonBL);
    }
//3-应收总表，单项纪录
    mLJSPayBL.setMakeDate(CurrentDate);
    mLJSPayBL.setMakeTime(CurrentTime);
    mLJSPayBL.setModifyDate(CurrentDate);
    mLJSPayBL.setModifyTime(CurrentTime);

      tReturn=true;
   }
   return tReturn ;
  }

 /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData mInputData)
  {
    //应收总表
    mLJSPayBL.setSchema((LJSPaySchema)mInputData.getObjectByObjectName("LJSPaySchema",0));
    // 应收个人交费表
    mLJSPayPersonSet.set((LJSPayPersonSet)mInputData.getObjectByObjectName("LJSPayPersonSet",0));

    mLOPRTManagerSchema=(LOPRTManagerSchema)mInputData.getObjectByObjectName("LOPRTManagerSchema",0);

    mLOPRTManagerSubSchema=(LOPRTManagerSubSchema)mInputData.getObjectByObjectName("LOPRTManagerSubSchema",0);


    if(mLJSPayBL==null || mLJSPayPersonSet ==null )
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="DuePayFeeBL";
      tError.functionName="getInputData";
      tError.errorMessage="没有得到足够的数据，请您确认!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData()
  {
    mInputData=new VData();
    try
    {
//1-处理应收个人交费表，记录集，循环处理
      mInputData.add(mLJSPayPersonSetNew);
//2-应收总表，单项纪录
      mInputData.add(mLJSPayBL);


      String autoVerifyFlag="N";
      LDSysVarDB tLDSysVarDB=new LDSysVarDB();
      tLDSysVarDB.setSysVar("AutoVerifyStation");
      if(tLDSysVarDB.getInfo()==true)
      {
          //如果系统变量为催收时核销并且应收总表应收款=0，那么催收时核销
          if(tLDSysVarDB.getSysVarValue().equals("2")&&mLJSPayBL.getSumDuePayMoney()==0)
          {
              autoVerifyFlag="Y";
          }
      }
      TransferData tTransferData=new TransferData();
      tTransferData.setNameAndValue("autoVerifyFlag",autoVerifyFlag);
      mInputData.add(tTransferData);

      mInputData.add(mLOPRTManagerSchema);
      mInputData.add(mLOPRTManagerSubSchema);


    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="DuePayFeeBL";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }
}

