package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
//import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
//import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;

import java.text.*;
import java.util.Date;
import com.sinosoft.lis.taskservice.NextFeeHastenTask;
import com.sinosoft.task.TaskPhoneFinishBL;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class NormPayCollOperBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();;
    private VData mPerInputData = new VData();;
    private SSRS mPerSSRS;
    private SSRS mCheck_flag;

    /** 数据操作字符串 */
    private String mOperate;
    private GlobalInput tGI = new GlobalInput();
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
//  private String serNo = ""; //流水号
//  private String tLimit = "";
//  private String payNo = ""; //交费收据号

    private String mSqlLjsPayPer;
    private String mSqlLjsPayPerB;
    private String mSqlLccont;
    private String mSqlDuty;
    private String mSqlPrem;
    private String mSqlPol;

    //更新无名单交至日期
    private String mSqlLccontWMD;
    private String mSqlDutyWMD;
    private String mSqlPremWMD;
    private String mSqlPolWMD;
    private MMap mAccMap = new MMap();

    private TransferData mTransferData;
    private LCGrpContSchema mLCGrpContSchema;
    private boolean mINNC = false;  //无名单续期标志Is NOName Cont

    /*转换精确位数的对象   */
    private String FORMATMODOL = "0.000"; //保费保额计算出来后的精确位数
    private DecimalFormat mDecimalFormat = new DecimalFormat(FORMATMODOL); //数字转换对象
    private LCContSet mLCContSet = new LCContSet();
//暂收费表
    private LJTempFeeSchema mLJTempFeeSchema = new LJTempFeeSchema();
    private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
    private LJTempFeeSet mLJTempFeeNewSet = new LJTempFeeSet();
//暂收费分类表
    private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
    private LJTempFeeClassSet mLJTempFeeClassNewSet = new LJTempFeeClassSet();
    private String mPayDate = ""; //缴费日期
    private String mEnterAccDate = ""; //到帐日期
    private String mConfDate = ""; //财务确认日期

//实收个人交费表
    private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();

//应收集体交费表

    private LJSPayGrpSet mLJSPayGrpSet = new LJSPayGrpSet();

//实收集体交费表
    private LJAPayGrpSet mLJAPayGrpSet = new LJAPayGrpSet();
    private LJSPayGrpBSet mLJSPayGrpBSet = new LJSPayGrpBSet();

//应收总表
    private LJSPayBL mLJSPayBL = new LJSPayBL();
    private LJSPayBSchema mLJSPayBSchema = new LJSPayBSchema();
//实收总表
    private LJAPayBL mLJAPayBL = new LJAPayBL();
//个人保单表
    private LCPolSet mLCPolSet = new LCPolSet();
    private MMap mMMap = new MMap();
    private MMap mapPer = new MMap();
    private VData mResult = new VData();
    private VData mLPtableMMap = new VData();
    
//集体保单表
    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
    private LCGrpPolSet mLCGrpPolNewSet = new LCGrpPolSet();
//保费项表
    private LCPremSet mLCPremSet = new LCPremSet();

//保险责任表LCDuty
    private LCDutySet mLCDutySet = new LCDutySet();
    private String mOpertor = "INSERT";
    private String mOperateFlag = "1"; // 1 : 个案核销，2：批量核销
    private String mserNo = "1"; //批次号
    private String mSerGrpNo = "";


    //业务处理相关变量
    public NormPayCollOperBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        GlobalInput mGI = new GlobalInput();
        mGI.Operator = "pa0003";
        mGI.ComCode = "86";
        mGI.ManageCom = "86";

//        LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
//        tLCGrpPolSchema.setGrpPolNo("86110020040220000362");
//        tLCGrpPolSchema.setPaytoDate("2004-7-1");
//        tLCGrpPolSchema.setManageCom("86");
//        tLCGrpPolSchema.setOperator("000015");

        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema.setGrpContNo("0000001804");

        NormPayCollOperBL tNormPayCollOperBL = new NormPayCollOperBL();
        VData tVData = new VData();
        tVData.add(tLCGrpContSchema);
        tVData.add(mGI);
//        tVData.add("WMD");

        tNormPayCollOperBL.submitData(tVData, "VERIFY");
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中

        if (!getInputData(cInputData)) {
            return false;
        }
        System.out.println("After getinputdata");

        //进行业务处理
        if (!dealData()) {
            return false;
        }
        if(!dealPhoneHasten())
        {
            return false;
        }
        System.out.println("After dealData！");
        if (!prepareData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(mResult, "") == false) {
            // System.out.println("delete fail!");
            CError.buildErr(this, "系统处理数据异常！");
            //添加数据到催收核销日志表
            if (!dealUrgeLog("2", "插入LJAPay等表的数据失败", "UPDATE")) {
                return false;
            }

            return false;
        }
//        System.out.println("Commit over,Successful!");
//        if (!dealOtherData()) {
//            CError.buildErr(this, "Sql 处理大数据失败！");
//            //添加数据到催收核销日志表
//            if (!dealUrgeLog("2", "更新lcprem 等大表的数据失败", "UPDATE")) {
//                return false;
//            }
//
//            return false;
//        }
        mInputData = null;
        //添加数据到催收核销日志表
        //非批次核销
        if (mSerGrpNo == null || mSerGrpNo.equals("")) {
            if (!dealUrgeLog("3", "", "UPDATE")) {
                return false;
            }
        }

        return true;
    }

    /**
     * 个单应收记录作废/撤销类，在IndiLJSCancelBL.getSubmitMap方法 中增加对催缴工单的处理
     * @return boolean
     */
    private boolean dealPhoneHasten()
    {
        String sql = "select * "
                     + "from LGPhoneHasten "
                     + "where getNoticeNo = '"
                     + mLJSPayBL.getGetNoticeNo() + "' "
                     + "   and HastenType = '"
                     + NextFeeHastenTask.HASTEN_TYPE_NEXTFEEG + "' "
                     + "   and FinishType is null ";
        LGPhoneHastenSet set = new LGPhoneHastenDB().executeQuery(sql);
        if(set.size() == 0)
        {
            //若无催缴就查不出来也
            return true;
        }

        LGPhoneHastenSchema schema = set.get(1);
        schema.setFinishType("4");
        schema.setRemark("应收记录" + mLJSPayBL.getGetNoticeNo()
                         + "已收费");

        VData data = new VData();
        data.add(schema);
        data.add(tGI);

        TaskPhoneFinishBL bl = new TaskPhoneFinishBL();
        MMap tMMap = bl.getSubmitMap(data, "");
        if(tMMap == null)
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }
        mMMap.add(tMMap);

        return true;
    }


    /**
     * 团单下个人 核销处理逻辑 杨红于2005-07-22创建新的dealData方法
     * 只将选中的应收个人核销并转实收(inputflag='1')
     * 说明：dealAccount的逻辑还没有处理，待添加
     *
     * @return boolean
     */
    private boolean dealData() {

//    	20110504 YangTianZheng 防止并发加锁
    	MMap tCekMap1 = null;
    	tCekMap1 = lockLJAGet(mLCGrpContSchema);
        if (tCekMap1 == null)
        {
            return false;
        }
 
      VData  tableInputData = new VData();
      tableInputData.add(tCekMap1);
      PubSubmit tablePubSubmit = new PubSubmit();
      if (tablePubSubmit.submitData(tableInputData, "") == false) {
          System.out.println(tablePubSubmit.mErrors.getFirstError());
          CError tError = new CError();
          tError.moduleName = "PubSubmit";
          tError.functionName = "submitData";
          tError.errorMessage = "该团单正在进行核销操作，请不要重复操作";

          this.mErrors.addOneError(tError);
          return false;
      }
//--------------------------
    	
        String ManageCom = tGI.ManageCom;
        String Operator = tGI.Operator;
        String tLimit = "";
        String serNo = "";
        String payNo = "";

        String grpContNo = mLCGrpContSchema.getGrpContNo(); //获取从前台页面传入的团单合同号
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(grpContNo);
        if (!tLCGrpContDB.getInfo()) {
            CError.buildErr(this, "团单：" + grpContNo + "系统中信息缺失，不能核销！");
            return false;
        }
        System.out.println(" 续期核销时，取团单信息");
        mLCGrpContSchema.setSchema(tLCGrpContDB.getSchema());

        String str = "select a.edorAcceptNo "
                     + "from LPEdorApp a, LPGrpEdorItem b "
                     + "where a.edorAcceptNo = b.edorNo "
                     + "   and b.grpContNo = '" + mLCGrpContSchema.getGrpContNo()
                     + "' "
                     + "   and a.edorState not in('0')";
        String temp = new ExeSQL().getOneValue(str);
        if(!temp.equals(""))
        {
            mErrors.addOneError("保单正在做保全不能核销" + mLCGrpContSchema.getGrpContNo());
            return false;
        }

        //根据grpcontno获取应收总表记录
        LJSPayDB tLJSPayDB = new LJSPayDB();
        LJSPaySet tLJSPaySet = new LJSPaySet();
        tLJSPayDB.setOtherNo(grpContNo);
        tLJSPayDB.setOtherNoType("1");

        tLJSPaySet = tLJSPayDB.query();
        System.out.println(" 续期核销时，取LJSPay信息");
        if (tLJSPaySet.size() == 0) {
            CError.buildErr(this, "团单没被催收，请催收后再核销！");
            return false;
        }

        mLJSPayBL.setSchema(tLJSPaySet.get(1)); //核销时，记录会被删除
//        if (Math.abs(mLJSPayBL.getSumDuePayMoney()-0.0)<0.00001) {
//            CError.buildErr(this, "团单下的余额足够支付本次续期，请做自动核销");
//            return false;
//        }

        //下面查找LJSPayPerson的记录
        int intCont = 0;
        StringBuffer tSBql = new StringBuffer(128);
        tSBql.append(
                " SELECT distinct PayNo,SerialNo  FROM LJAPayPerson where GrpContNo='")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("' and GetNoticeNo ='")
                .append(mLJSPayBL.getGetNoticeNo())
                .append("' fetch first row only with ur")
                ;
        String strPerSQL = tSBql.toString();
        ExeSQL tExeSQL = new ExeSQL();
        mPerSSRS = tExeSQL.execSQL(strPerSQL);
        System.out.println(" 续期核销时，取LJAPayPerson:PayNo信息");
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "NormPayCollOperBL";
            tError.functionName = "DealData";
            tError.errorMessage = "查询数据 LJAPayPerson 出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        if (mPerSSRS.MaxRow >= 1) {
            intCont = 1;
            payNo = mPerSSRS.GetText(1, 1);
            serNo = mPerSSRS.GetText(1, 2);
        }
        if (intCont != 1) {
            if (mTransferData == null) {
                tLimit = PubFun.getNoLimit(ManageCom);
                serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); //给应收表生成统一的流水号
                mOperateFlag = "1";
                mserNo = serNo;
                //添加数据到催收核销日志表
                if (!dealUrgeLog("1", "", "INSERT")) {
                    return false;
                }

            } else {
                serNo = (String) mTransferData.getValueByName("GrpSerNo");
                mSerGrpNo = serNo;
                tLimit = PubFun.getNoLimit(ManageCom);
                mOperateFlag = "2";
            }
            payNo = PubFun1.CreateMaxNo("PayNo", tLimit); //给实收表生成统一的　收据号
        }
        mserNo = serNo;
        System.out.println(" 续期核销时，取PayNo:" + payNo + "信息");
        String tempdealstate = "4";
//        if(Math.abs(mLJSPayBL.getSumDuePayMoney()-0.00)<0.0000001){
//            tempdealstate = "0";
//        }
        String ljspaybSQL = "select * from LJSPayB where GetNoticeNo = '"+mLJSPayBL.getGetNoticeNo()+
                            "' and OtherNoType = '1' and dealstate = '"+tempdealstate+"'";
        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        LJSPayBSet tLJSPayBSet = tLJSPayBDB.executeQuery(ljspaybSQL);
        System.out.println(" 续期核销时，取LJSPayB信息");
        if (tLJSPayBSet.size() == 0) {
            CError.buildErr(this, "团单下的备份数据缺失，请催收后再核销！");
            return false;
        }
        mLJSPayBSchema = tLJSPayBSet.get(1);

        //下面获取应收集体数据，准备核销时删除！
        LJSPayGrpDB tLJSPayGrpDB = new LJSPayGrpDB();
        tLJSPayGrpDB.setGrpContNo(grpContNo);
        mLJSPayGrpSet = tLJSPayGrpDB.query(); //获取应收集体数据，准备核销时删除！
        //下面判断在LJSPayGrp记录中是否有YET类型的数据，如果有，则走自动核销，不需要　暂交费
        if (mLJSPayGrpSet.size() == 0) {
            CError.buildErr(this, "团单下的集体催收数据缺失，不能核销！");
            return false;
        }
        System.out.println(" 续期核销时，取LJSPayGrp信息");
        //下面判断在LJSPayGrp记录中是否有YET类型的数据，如果有，则走自动核销，不需要　暂交费

        //下面获取应收集体数据，准备核销时删除！备份
        LJSPayGrpBDB tLJSPayGrpBDB = new LJSPayGrpBDB();
        tLJSPayGrpBDB.setGrpContNo(grpContNo);
        tLJSPayGrpBDB.setDealState(tempdealstate);
        tLJSPayGrpBDB.setGetNoticeNo(mLJSPayBL.getGetNoticeNo());
        LJSPayGrpBSet tLJSPayGrpBSet = new LJSPayGrpBSet();
        tLJSPayGrpBSet = tLJSPayGrpBDB.query(); //获取应收集体数据，准备核销时删除！
        System.out.println(" 续期核销时，取LJSPayGrpB信息");
        //下面判断在LJSPayGrp记录中是否有YET类型的数据，如果有，则走自动核销，不需要　暂交费
        if (tLJSPayGrpBSet.size() == 0) {
            CError.buildErr(this, "团单下的集体催收数据缺失，不能核销！");
            return false;
        }

        //下面获取应收个人数据
        String strPerCntSQL =
                " SELECT count(*) FROM LJsPayPerson where GrpContNo='"
                + mLCGrpContSchema.getGrpContNo() + "' with ur";
        tExeSQL = new ExeSQL();
        String strPerCnt = tExeSQL.getOneValue(strPerCntSQL);
        System.out.println(" 续期核销时，取LJsPayPerson条数:" + strPerCnt + "信息");
        if (tExeSQL.mErrors.needDealError()) {
            CError.buildErr(this, "查询数据 LJsPayPerson 出错！");
            return false;
        }
        if (strPerCnt == null || strPerCnt.equals("0")) {
            CError.buildErr(this, "团单下的个人催收数据缺失，不能核销！");
            return false;
        }

        /*String strPerBCntSQL =
                " SELECT count(*) FROM LJsPayPersonB where GrpContNo='"
                + mLCGrpContSchema.getGrpContNo() + "' with ur";
                 tExeSQL = new ExeSQL();
                 String strPerBCnt = tExeSQL.getOneValue(strPerBCntSQL);
                 if (tExeSQL.mErrors.needDealError()) {
            CError.buildErr(this, "查询数据 LJsPayPersonB 出错！");
            return false;
                 }
                 if (strPerBCnt == null || strPerBCnt == "0") {
            CError.buildErr(this, "团单下的个人催收备份数据缺失，不能核销！");
            return false;
                 }
         System.out.println(" 续期核销时，取LJsPayPerson条数:" + strPerCnt + "信息");*/
        //下面判断暂交费是否到帐，否则不能核销！
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        tLJTempFeeDB.setTempFeeNo(mLJSPayBL.getGetNoticeNo()); //说明：催收产生的催缴通知书号对应交费通知书号
        tLJTempFeeDB.setConfFlag("0");
        mLJTempFeeSet = tLJTempFeeDB.query();
        System.out.println(" 续期核销时，取LJTempFee信息");
        if (mLJSPayBL.getSumDuePayMoney() > 0 && mLJTempFeeSet.size() == 0) {
            CError.buildErr(this, "财务未交费，请财务交费后再做核销！");
            return false;
        }
        //不需要财务缴费
        else if(Math.abs(0 - mLJSPayBL.getSumDuePayMoney()) < 0.00001){
            mPayDate = CurrentDate;
            mEnterAccDate = CurrentDate;
        }
        else{
            mLJTempFeeSchema = mLJTempFeeSet.get(1).getSchema(); //至于暂交费金额与应收总金额(inputflag='1')的比较

            mPayDate = mLJTempFeeSchema.getPayDate();
            mEnterAccDate = mLJTempFeeSchema.getEnterAccDate();
        }
        mConfDate = CurrentDate;

        LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
        tLJTempFeeClassDB.setTempFeeNo(mLJSPayBL.getGetNoticeNo()); //说明：催收产生的催缴通知书号对应交费通知书号
        tLJTempFeeClassDB.setConfFlag("0");
        mLJTempFeeClassSet = tLJTempFeeClassDB.query();
        System.out.println(" 续期核销时，取LJTempFeeClass信息");
        if (mLJSPayBL.getSumDuePayMoney() > 0 && mLJTempFeeClassSet.size() == 0) {
            CError.buildErr(this, "暂交费分类信息缺失，不能核销！");
            return false;
        }

        // mLJTempFeeSchemaNew
        //要到后面，根据ljspayperson相关信息判断
        if (mLJSPayBL.getSumDuePayMoney() > 0 && mLJTempFeeSchema.getEnterAccDate() == null) {
            CError.buildErr(this, "财务未到帐，请到帐并作会计确认后再做核销！");
            return false;
        }
        //下面取得应收日期
        tSBql = new StringBuffer(128);
        tSBql.append("select min(PayToDate) from lccont where grpcontno='")
                .append(grpContNo)
                .append("' with ur")
                ;
        String strSQL = tSBql.toString();
        System.out.println(strSQL);
        System.out.println(" 续期核销时，取PayToDate信息");
        tExeSQL = new ExeSQL();
//        String tempPayToDate = tExeSQL.getOneValue(strSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "NormPayCollOperBL";
            tError.functionName = "DealData";
            tError.errorMessage = "查询数据 LJAPayPerson 出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

//        if (mLJTempFeeSchema.getEnterAccDate() == tempPayToDate) {
//            mLCGrpContSchema.setDif(mLJTempFeeSchema.getPayMoney());
//            mLCGrpContSchema.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
//            mLCGrpContSchema.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间
//            prepareOutputData();
//            PubSubmit tPubSubmit = new PubSubmit();
//            if (tPubSubmit.submitData(mInputData, "") == false) {
//                CError.buildErr(this, "数据处理时 PubSubmit 失败!");
//
//                return false;
//            }
//
//            CError.buildErr(this, "周年日收取的保费进入保费余额帐户，不能核销！");
//            return false;
//        }

        //下面处理LCGrpPol,即团单下个险的信息
        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        tSBql = new StringBuffer(128);
        tSBql.append("select * from LCGrpPol where GrpContNo='")
                .append(grpContNo)
                .append("'")
                .append(
                        " and GrpPolNo in (select GrpPolNo from LJSPayGrp where paytype='ZC')")
                .append(
                        " and GrpPolNo not in (select GrpPolNo from LJAPayGrp where paytype='ZC'")
                .append(" AND GrpContNo =' ")
                .append(grpContNo)
                .append("') with ur")
                ; //校验类型是ZC的应收集体记录
        String grpPolSqlStr = tSBql.toString();
        System.out.println(grpPolSqlStr);
        System.out.println(" 续期核销时，取LCGrpPol信息");
        mLCGrpPolSet = tLCGrpPolDB.executeQuery(grpPolSqlStr);
        if (mLCGrpPolSet.size() == 0) {
            CError.buildErr(this, "团单下的集体催收数据缺失，不能核销！");
            return false;
        }

        //下面处理lccont,即团单下个人的信息
        tSBql = new StringBuffer(128);
        tSBql.append("select COUNT(*) from lccont where grpcontno='")
                .append(grpContNo)
                .append("'")
                .append(" and contno  in (select contno from ljspayperson where inputflag='1' and GrpContNo='")
                .append(grpContNo)
                .append("') with ur")
                ;
        String contSql = tSBql.toString();
        tExeSQL = new ExeSQL();
        String strlCContCnt = tExeSQL.getOneValue(contSql);
        System.out.println(" 续期核销时，取lccont条数:" + strlCContCnt + "信息");
        if (tExeSQL.mErrors.needDealError()) {
            CError.buildErr(this, "查询数据 LCCont出错！");
            return false;
        }
        if (strlCContCnt == null || strlCContCnt.equals("0")) {
            CError.buildErr(this, "团单下的个单信息数据缺失，不能核销！");
            return false;
        }

        //说明：核销时只处理应收个人表中inputflag='1'的记录
        String getNoticeNo = mLJSPayBL.getGetNoticeNo();
        double grpContDif = mLCGrpContSchema.getDif(); //保存上次余额
        double grpContPremTotal = 0.0;
        for (int i = 1; i <= mLCGrpPolSet.size(); i++) {
            LCGrpPolBL tLCGrpPolBL = new LCGrpPolBL();
            tLCGrpPolBL.setSchema(mLCGrpPolSet.get(i).getSchema());
            String grpPolNo = mLCGrpPolSet.get(i).getGrpPolNo();
            String grpPolPayToDate = "";
            double grpPolPremTotal = 0.0;
            //获取有对应ljspayperson的lcpol集合
            tSBql = new StringBuffer(128);
            tSBql.append("select * from lcpol where GrpPolNo='")
                    .append(grpPolNo)
                    .append("'")
                    .append(
                            " and polno in (select polno from ljspayperson where InputFlag='1' and GrpPolNo='")
                    .append(grpPolNo)
                    .append("' and grpcontno='")
                    .append(mLCGrpPolSet.get(i).getGrpContNo())
                    .append("')")
                    .append(
                            " and polno not in (select polno from ljapayperson ")
                    .append(" where GetNoticeNo='")
                    .append(getNoticeNo)
                    .append("' and GrpPolNo='")
                    .append(grpPolNo)
                    .append("'and grpcontno='")
                    .append(mLCGrpPolSet.get(i).getGrpContNo())
                    .append("') with ur")

                    ;
            String polSqlStr = tSBql.toString();
            System.out.println(polSqlStr);
            System.out.println("获取个险表数据数据");

            /** 新增方法:
             * 游标查询方法 */
            RSWrapper rswrapper = new RSWrapper();
            LCPolSet tLCPolSet = new LCPolSet();
            rswrapper.prepareData(tLCPolSet, polSqlStr);
            do {
                //循环取得2000条数据，进行处理
                rswrapper.getData();

                for (int j = 1; j <= tLCPolSet.size(); j++) {

                    // mLCDutySet.clear();
                    mLJAPayPersonSet.clear();
                    // mLCPremSet.clear();
                    //mLCPolSet.clear();

                    LCPolBL tLCPolBL = new LCPolBL();
                    tLCPolBL.setSchema(tLCPolSet.get(j).getSchema());
                    double polPremTotal = 0.0;
                    String polPayToDate = "";

                    String polNo = tLCPolSet.get(j).getPolNo(); //团单下　个单　保单险种号
                    /*tSBql = new StringBuffer(128);
                     tSBql.append("select * from lcduty where polno='")
                            .append(polNo)
                            .append("'")
                            .append(" and dutycode in (select dutycode from ljspayperson where InputFlag='1' and polno='")
                            .append(polNo)
                            .append("') ")
                            ;
                                         String dutySqlStr = tSBql.toString();

                                         LCDutyDB tLCDutyDB = new LCDutyDB();
                     LCDutySet tLCDutySet = new LCDutySet();
                     tLCDutySet = tLCDutyDB.executeQuery(dutySqlStr);
                     System.out.println("获取责任表数据数据:" + tLCDutySet.size());
                                         System.out.println(dutySqlStr);
                                         //获取可催收的　保费项信息lcprem,注意duty下的部分prem可能不会产生应收记录，切记！
                                        // for (int m = 1; m <= tLCDutySet.size(); m++) {

                        LCDutyBL tLCDutyBL = new LCDutyBL();
                        tLCDutyBL.setSchema(tLCDutySet.get(m).getSchema());
                        String dutyCode = tLCDutySet.get(m).getDutyCode();*/
                    double dutyPremTotal = 0.0;
                    String dutyPayToDate = "";
                    tSBql = new StringBuffer(128);
                    tSBql.append("select * from lcprem where polno='")
                            .append(polNo)
                            .append("' and PayPlanCode in")
                            .append(
                                    " (select payplancode from ljspayperson where InputFlag='1' and polno='")
                            .append(polNo)
                            .append("' AND CONTNO='")
                            .append(tLCPolBL.getContNo())
                            .append("')")
                            ;
                    String premSqlStr = tSBql.toString();

                    LCPremDB tLCPremDB = new LCPremDB();
                    LCPremSet tLCPremSet = new LCPremSet();
                    tLCPremSet = tLCPremDB.executeQuery(premSqlStr);
                    System.out.println("获取保费表数据数据;" + tLCPremSet.size());
                    System.out.println(premSqlStr);
                    for (int n = 1; n <= tLCPremSet.size(); n++) {
                        //下面将应收转为实收，并更新保费项信息 lcprem
                        String payPlanCode = tLCPremSet.get(n).
                                             getPayPlanCode();
                        LJSPayPersonDB tempLJSPayPersonDB = new
                                LJSPayPersonDB();
                        LJSPayPersonSet tempLJSPayPersonSet = new
                                LJSPayPersonSet();
                        tempLJSPayPersonDB.setGrpContNo(tLCPolBL.getGrpContNo());
                        tempLJSPayPersonDB.setContNo(tLCPolBL.getContNo());
                        tempLJSPayPersonDB.setPolNo(polNo);
                        tempLJSPayPersonDB.setDutyCode(tLCPremSet.get(n).
                                getDutyCode());
                        tempLJSPayPersonDB.setPayPlanCode(
                                payPlanCode);
                        tempLJSPayPersonSet = tempLJSPayPersonDB.
                                              query(); //根据上面的判定逻辑，这里tempLJSPayPersonSet
                        //肯定不为空
                        LJSPayPersonBL tLJSPayPersonBL = new
                                LJSPayPersonBL(); //tempLJSPayPersonSet.get(1);
                        tLJSPayPersonBL.setSchema(
                                tempLJSPayPersonSet.get(1));
                        if (n == 1) {
                            dutyPayToDate = tLJSPayPersonBL.
                                            getCurPayToDate();
                        } else {
                            FDate chgdate = new FDate();
                            Date dbdate = chgdate.getDate(
                                    dutyPayToDate);
                            Date dedate = chgdate.getDate(
                                    tLJSPayPersonBL.getCurPayToDate());
                            if (dedate.before(dbdate)) {
                                dutyPayToDate = tLJSPayPersonBL.
                                                getCurPayToDate();
                            }
                        }
                        //开始应收转实收！
                        LJAPayPersonBL tLJAPayPersonBL = new
                                LJAPayPersonBL();
                        tLJAPayPersonBL.setPolNo(tLJSPayPersonBL.
                                                 getPolNo()); //保单号码
                        tLJAPayPersonBL.setPayCount(tLJSPayPersonBL.
                                getPayCount()); //第几次交费
                        tLJAPayPersonBL.setGrpPolNo(tLJSPayPersonBL.
                                getGrpPolNo()); //集体保单险种号码
                        tLJAPayPersonBL.setGrpContNo(
                                tLJSPayPersonBL.getGrpContNo()); //集体保单号码
                        tLJAPayPersonBL.setContNo(tLJSPayPersonBL.
                                                  getContNo()); //集体保单号码
                        tLJAPayPersonBL.setAppntNo(tLJSPayPersonBL.
                                getAppntNo()); //投保人客户号码
                        tLJAPayPersonBL.setPayNo(payNo); //交费收据号码
                        tLJAPayPersonBL.setPayAimClass(
                                tLJSPayPersonBL.
                                getPayAimClass()); //交费目的分类
                        tLJAPayPersonBL.setDutyCode(tLJSPayPersonBL.
                                getDutyCode()); //责任编码
                        tLJAPayPersonBL.setPayPlanCode(
                                tLJSPayPersonBL.
                                getPayPlanCode()); //交费计划编码
                        tLJAPayPersonBL.setSumDuePayMoney(
                                tLJSPayPersonBL.
                                getSumDuePayMoney()); //总应交金额
                        tLJAPayPersonBL.setSumActuPayMoney(
                                tLJSPayPersonBL.
                                getSumActuPayMoney()); //总实交金额
                        tLJAPayPersonBL.setPayIntv(tLJSPayPersonBL.
                                getPayIntv()); //交费间隔
                        tLJAPayPersonBL.setPayDate(mPayDate); //交费日期
                        tLJAPayPersonBL.setPayType(tLJSPayPersonBL.
                                getPayType()); //交费类型
                        tLJAPayPersonBL.setEnterAccDate(this.mEnterAccDate); //到帐日期
                        tLJAPayPersonBL.setConfDate(
                                this.mConfDate); //确认日期
                        tLJAPayPersonBL.setLastPayToDate(
                                tLJSPayPersonBL.
                                getLastPayToDate()); //原交至日期
                        tLJAPayPersonBL.setCurPayToDate(
                                tLJSPayPersonBL.
                                getCurPayToDate()); //现交至日期
                        tLJAPayPersonBL.setInInsuAccState(
                                tLJSPayPersonBL.
                                getInInsuAccState()); //转入保险帐户状态
                        tLJAPayPersonBL.setApproveCode(
                                tLJSPayPersonBL.
                                getApproveCode()); //复核人编码
                        tLJAPayPersonBL.setApproveDate(
                                tLJSPayPersonBL.
                                getApproveDate()); //复核日期
                        tLJAPayPersonBL.setSerialNo(serNo); //流水号
                        tLJAPayPersonBL.setOperator(Operator); //操作员
                        tLJAPayPersonBL.setMakeDate(CurrentDate); //入机日期
                        tLJAPayPersonBL.setMakeTime(CurrentTime); //入机时间
                        tLJAPayPersonBL.setGetNoticeNo(
                                tLJSPayPersonBL.
                                getGetNoticeNo()); //通知书号码
                        tLJAPayPersonBL.setModifyDate(CurrentDate); //最后一次修改日期
                        tLJAPayPersonBL.setModifyTime(CurrentTime); //最后一次修改时间
                        tLJAPayPersonBL.setManageCom(
                                tLJSPayPersonBL.getManageCom()); //管理机构
                        tLJAPayPersonBL.setAgentCom(tLJSPayPersonBL.
                                getAgentCom()); //代理机构
                        tLJAPayPersonBL.setAgentType(
                                tLJSPayPersonBL.getAgentType()); //代理机构内部分类
                        tLJAPayPersonBL.setRiskCode(tLJSPayPersonBL.
                                getRiskCode()); //险种编码
                        tLJAPayPersonBL.setAgentCode(
                                tLJSPayPersonBL.getAgentCode());
                        tLJAPayPersonBL.setAgentGroup(
                                tLJSPayPersonBL.getAgentGroup());
                        mLJAPayPersonSet.add(tLJAPayPersonBL);
                        System.out.println("mLJAPayPersonSet添加次数：" + n);
                        /*LCPremBL tLCPremBL = new LCPremBL();
                         tLCPremBL.setSchema(tLCPremSet.get(n).
                                            getSchema());
                         tLCPremBL.setPayTimes(tLCPremBL.getPayTimes() +
                                              1); //已交费次数
                         tLCPremBL.setPrem(tLJSPayPersonBL.
                                          getSumActuPayMoney()); //实际保费
                         tLCPremBL.setSumPrem(tLCPremBL.getSumPrem() +
                                             tLJSPayPersonBL.
                                             getSumActuPayMoney()); //累计保费
                         tLCPremBL.setPaytoDate(tLJSPayPersonBL.
                                getCurPayToDate()); //交至日期
                         tLCPremBL.setModifyDate(CurrentDate); //最后一次修改日期
                         tLCPremBL.setModifyTime(CurrentTime); //最后一次修改时间
                         mLCPremSet.add(tLCPremBL);*/


                        dutyPremTotal += tLJSPayPersonBL.
                                getSumActuPayMoney();

                    }

                    /*if (dutyPremTotal > 0.0) {
                        tLCDutyBL.setSumPrem(tLCDutyBL.getSumPrem() +
                                             dutyPremTotal);
                        tLCDutyBL.setPaytoDate(dutyPayToDate); //选择保费项中较早的交至日期
                        tLCDutyBL.setModifyDate(CurrentDate); //最后一次修改日期
                        tLCDutyBL.setModifyTime(CurrentTime); //最后一次修改时间
                        mLCDutySet.add(tLCDutyBL);

                                             }
                                             if (m == 1) {
                        polPayToDate = dutyPayToDate;
                                             } else {
                        FDate chgdate = new FDate();
                        Date dbdate = chgdate.getDate(polPayToDate);
                        Date dedate = chgdate.getDate(dutyPayToDate);
                        if (dedate.before(dbdate)) {
                            polPayToDate = dutyPayToDate;
                        }

                                             }*/
                    polPremTotal += dutyPremTotal;
                    //}

                    /*if (polPremTotal > 0.0) {
                        tLCPolBL.setPaytoDate(polPayToDate); //获取lcduty中较早的交至日期
                        tLCPolBL.setModifyDate(CurrentDate); //最后一次修改日期
                        tLCPolBL.setModifyTime(CurrentTime); //最后一次修改时间
                        mLCPolSet.add(tLCPolBL);
                                         }*/

                    if (!preparePersonData()) {
                        return false;
                    }
                    System.out.println(" 续期核销时，插入信息LJAPayPerson等数据");
                    PubSubmit tPubSubmit = new PubSubmit();
                    if (tPubSubmit.submitData(mPerInputData, "") == false) {
                        //添加数据到催收核销日志表
                        if (!dealUrgeLog("2", "插入LJAPayPerson数据失败", "UPDATE")) {
                            return false;
                        }

                        System.out.println(tPubSubmit.mErrors.getFirstError());
                        CError tError = new CError();
                        tError.moduleName = "PubSubmit";
                        tError.functionName = "submitData";
                        tError.errorMessage = "2000 行数据处理时 PubSubmit 失败!";

                        this.mErrors.addOneError(tError);

                        return false;
                    }

                }
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } while (tLCPolSet.size() > 0);

            //下面生成ljspaygrp的记录

            tSBql = new StringBuffer(128);
            tSBql.append(
                    " SELECT SUM(SumDuePayMoney) FROM LJAPayPerson where GrpPolNo='")
                    .append(grpPolNo)
                    .append("' ")
                    .append(" and GetNoticeNo='")
                    .append(getNoticeNo)
                    .append("'")
                    ;
            tExeSQL = new ExeSQL();
            strSQL = tSBql.toString();
            System.out.println(" 续期核销时，取总保费");
            String strGrpSumPay = tExeSQL.getOneValue(strSQL);
            if (tExeSQL.mErrors.needDealError()) {
                //添加数据到催收核销日志表
                if (!dealUrgeLog("2", "取LJSPayPerson总保费失败", "UPDATE")) {
                    return false;
                }

                CError tError = new CError();
                tError.moduleName = "NormPayCollOperBL";
                tError.functionName = "DealData";
                tError.errorMessage = "查询数据 LJAPayPerson 出错！";
                this.mErrors.addOneError(tError);
                return false;
            }

            //保存　团险下的保费和
            if (strGrpSumPay == null || strGrpSumPay.equals("")) {
                grpPolPremTotal = 0.0;
            } else {
                grpPolPremTotal = Double.parseDouble(strGrpSumPay);
            }
            //grpPolPayToDate 保存　团险下的交至日期
            tSBql = new StringBuffer(128);
            tSBql.append(
                    " SELECT min(CurPayToDate) FROM LJAPayPerson where GrpPolNo='")
                    .append(grpPolNo)
                    .append("' ")
                    .append(" and GetNoticeNo='")
                    .append(getNoticeNo)
                    .append("' with ur")
                    ;

            tExeSQL = new ExeSQL();
            strSQL = tSBql.toString();
            System.out.println(" 续期核销时，取CurPayToDate");
            String strPayToDate = tExeSQL.getOneValue(strSQL);
            if (tExeSQL.mErrors.needDealError()) {
                CError tError = new CError();
                tError.moduleName = "NormPayCollOperBL";
                tError.functionName = "DealData";
                tError.errorMessage = "查询数据 LJAPayPerson 出错！";
                this.mErrors.addOneError(tError);
                return false;
            }
            grpPolPayToDate = strPayToDate;
            //下面处理集体险种信息，并将LJSPayGrp转为LJAPayGrp
            LJSPayGrpDB tempLJSPayGrpDB = new LJSPayGrpDB();
            tempLJSPayGrpDB.setGrpPolNo(grpPolNo);
            tempLJSPayGrpDB.setGetNoticeNo(getNoticeNo);
            tempLJSPayGrpDB.setPayType("ZC");
            LJSPayGrpSet tempLJSPayGrpSet = new LJSPayGrpSet();
            tempLJSPayGrpSet = tempLJSPayGrpDB.query();
            LJSPayGrpBL tLJSPayGrpBL = new LJSPayGrpBL();
            tLJSPayGrpBL.setSchema(tempLJSPayGrpSet.get(1).getSchema());
            //下面应收转实收
            LJAPayGrpBL tLJAPayGrpBL = new LJAPayGrpBL();
            tLJAPayGrpBL.setGrpPolNo(grpPolNo);
            tLJAPayGrpBL.setGrpContNo(grpContNo);
            tLJAPayGrpBL.setPayCount(tLJSPayGrpBL.getPayCount());
            tLJAPayGrpBL.setAppntNo(tLJSPayGrpBL.getAppntNo());
            tLJAPayGrpBL.setPayNo(payNo);
            tLJAPayGrpBL.setSumDuePayMoney(tLJSPayGrpBL.getSumDuePayMoney()); //总应交金额
            tLJAPayGrpBL.setSumActuPayMoney(grpPolPremTotal); //总实交金额
            tLJAPayGrpBL.setPayIntv(tLJSPayGrpBL.getPayIntv());
            tLJAPayGrpBL.setPayDate(mPayDate);
            tLJAPayGrpBL.setPayType("ZC"); //repair:交费类型
            tLJAPayGrpBL.setEnterAccDate(this.mEnterAccDate);
            tLJAPayGrpBL.setConfDate(this.mConfDate); //确认日期
            tLJAPayGrpBL.setLastPayToDate(tLJSPayGrpBL.getLastPayToDate()); //原交至日期
            tLJAPayGrpBL.setCurPayToDate(tLJSPayGrpBL.getCurPayToDate());
            tLJAPayGrpBL.setApproveCode(mLCGrpPolSet.get(i).getApproveCode());
            tLJAPayGrpBL.setApproveDate(mLCGrpPolSet.get(i).getApproveDate());
            tLJAPayGrpBL.setSerialNo(serNo);
            tLJAPayGrpBL.setOperator(Operator);
            tLJAPayGrpBL.setMakeDate(PubFun.getCurrentDate());
            tLJAPayGrpBL.setMakeTime(PubFun.getCurrentTime());
            tLJAPayGrpBL.setGetNoticeNo(tLJSPayGrpBL.getGetNoticeNo()); //通知书号码
            tLJAPayGrpBL.setModifyDate(PubFun.getCurrentDate());
            tLJAPayGrpBL.setModifyTime(PubFun.getCurrentTime());
            tLJAPayGrpBL.setManageCom(mLCGrpPolSet.get(i).getManageCom()); //管理机构
            tLJAPayGrpBL.setAgentCom(mLCGrpPolSet.get(i).getAgentCom()); //代理机构
            tLJAPayGrpBL.setAgentType(mLCGrpPolSet.get(i).getAgentType()); //代理机构内部分类
            tLJAPayGrpBL.setRiskCode(mLCGrpPolSet.get(i).getRiskCode()); //险种编码
            tLJAPayGrpBL.setAgentCode(mLCGrpPolSet.get(i).getAgentCode());
            tLJAPayGrpBL.setAgentGroup(mLCGrpPolSet.get(i).getAgentGroup());
            mLJAPayGrpSet.add(tLJAPayGrpBL);
            //下面更新lcgrppol的记录
            tLCGrpPolBL.setSumPrem(tLCGrpPolBL.getSumPrem() + grpPolPremTotal);
            //获取lcgrpPol的交至日期
            //grpPolPayToDate
            tLCGrpPolBL.setPaytoDate(grpPolPayToDate);
            tLCGrpPolBL.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
            tLCGrpPolBL.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间
            mLCGrpPolNewSet.add(tLCGrpPolBL);
            grpContPremTotal += grpPolPremTotal;
        }
        //将类型为YEL的LJSPayGrp应收转为LJAPayGrp实收，（财务会做相关处理）
        tSBql = new StringBuffer(128);
        tSBql.append("select * from ljspaygrp where GrpContNo='")
                .append(grpContNo)
                .append("'")
                .append(" and GetNoticeNo='" + mLJSPayBL.getGetNoticeNo())
                .append("' and PayType='YEL'")
                ;
        String yelSqlStr = tSBql.toString();
        LJSPayGrpDB tempLJSPayGrpDB = new LJSPayGrpDB();
        LJSPayGrpSet tempLJSPayGrpSet = new LJSPayGrpSet();
        tempLJSPayGrpSet = tempLJSPayGrpDB.executeQuery(yelSqlStr);
        System.out.println(" 续期核销时，取余额");
        if (tempLJSPayGrpSet.size() > 0) {
            //将类型为YEL的LJSPayGrp应收转为LJAPayGrp实收
            LJSPayGrpBL yelLJSPayGrpBL = new LJSPayGrpBL();
            yelLJSPayGrpBL.setSchema(tempLJSPayGrpSet.get(1).getSchema());
            LJAPayGrpBL yelLJAPayGrpBL = new LJAPayGrpBL();
            yelLJAPayGrpBL.setGrpContNo(yelLJSPayGrpBL.getGrpContNo());
            yelLJAPayGrpBL.setPayCount(yelLJSPayGrpBL.getPayCount());
            yelLJAPayGrpBL.setGrpPolNo(yelLJSPayGrpBL.getGrpPolNo());
            yelLJAPayGrpBL.setPayCount(yelLJSPayGrpBL.getPayCount());
            yelLJAPayGrpBL.setAppntNo(mLCGrpContSchema.getAppntNo());
            yelLJAPayGrpBL.setPayNo(payNo); //交费收据号码
            yelLJAPayGrpBL.setSumDuePayMoney(yelLJSPayGrpBL.
                                             getSumDuePayMoney());
            yelLJAPayGrpBL.setSumActuPayMoney(yelLJSPayGrpBL.
                                              getSumActuPayMoney());
            yelLJAPayGrpBL.setPayIntv(yelLJSPayGrpBL.getPayIntv());
            yelLJAPayGrpBL.setLastPayToDate(yelLJSPayGrpBL.
                                            getLastPayToDate());
            yelLJAPayGrpBL.setCurPayToDate(yelLJSPayGrpBL.getCurPayToDate());
            yelLJAPayGrpBL.setPayDate(mPayDate);
            yelLJAPayGrpBL.setEnterAccDate(this.mEnterAccDate);
            yelLJAPayGrpBL.setPayType("YEL");
            yelLJAPayGrpBL.setSerialNo(serNo);
            yelLJAPayGrpBL.setGetNoticeNo(yelLJSPayGrpBL.getGetNoticeNo());
            yelLJAPayGrpBL.setOperator(Operator);
            yelLJAPayGrpBL.setMakeDate(PubFun.getCurrentDate());
            yelLJAPayGrpBL.setMakeTime(PubFun.getCurrentTime());
            yelLJAPayGrpBL.setModifyDate(PubFun.getCurrentDate());
            yelLJAPayGrpBL.setModifyTime(PubFun.getCurrentTime());
            yelLJAPayGrpBL.setManageCom(yelLJSPayGrpBL.getManageCom()); //管理机构
            yelLJAPayGrpBL.setAgentCom(yelLJSPayGrpBL.getAgentCom()); //代理机构
            yelLJAPayGrpBL.setAgentType(yelLJSPayGrpBL.getAgentType()); //代理机构内部分类
            yelLJAPayGrpBL.setRiskCode(yelLJSPayGrpBL.getRiskCode()); //险种编码
            yelLJAPayGrpBL.setAgentCode(yelLJSPayGrpBL.getAgentCode());
            yelLJAPayGrpBL.setAgentGroup(yelLJSPayGrpBL.getAgentGroup());
            mLJAPayGrpSet.add(yelLJAPayGrpBL);

//            grpContPremTotal += yelLJAPayGrpBL.getSumActuPayMoney();

            dealAppAcc(yelLJAPayGrpBL);
        }

        
         //核销时在ljapaygrp中出入YET的记录
        tSBql = new StringBuffer(128);
        tSBql.append("select * from ljspaygrp where GrpContNo='")
                .append(grpContNo)
                .append("'")
                .append(" and GetNoticeNo='" + mLJSPayBL.getGetNoticeNo())
                .append("' and PayType='YET'")
                ;
        String yetSqlStr = tSBql.toString();
        LJSPayGrpDB tempYELJSPayGrpDB = new LJSPayGrpDB();
        LJSPayGrpSet tempYELJSPayGrpSet = new LJSPayGrpSet();
        tempYELJSPayGrpSet = tempYELJSPayGrpDB.executeQuery(yetSqlStr);
        System.out.println(" 续期核销时，退回账户金额");
        if (tempYELJSPayGrpSet.size() > 0) {
            //将类型为YET的LJSPayGrp应收转为LJAPayGrp实收
            LJSPayGrpBL yetLJSPayGrpBL = new LJSPayGrpBL();
            yetLJSPayGrpBL.setSchema(tempYELJSPayGrpSet.get(1).getSchema());
            LJAPayGrpBL yetLJAPayGrpBL = new LJAPayGrpBL();
            yetLJAPayGrpBL.setGrpContNo(yetLJSPayGrpBL.getGrpContNo());
            yetLJAPayGrpBL.setPayCount(yetLJSPayGrpBL.getPayCount());
            yetLJAPayGrpBL.setGrpPolNo(yetLJSPayGrpBL.getGrpPolNo());
            yetLJAPayGrpBL.setPayCount(yetLJSPayGrpBL.getPayCount());
            yetLJAPayGrpBL.setAppntNo(mLCGrpContSchema.getAppntNo());
            yetLJAPayGrpBL.setPayNo(payNo); //交费收据号码
            yetLJAPayGrpBL.setSumDuePayMoney(yetLJSPayGrpBL.
                                             getSumDuePayMoney());
            yetLJAPayGrpBL.setSumActuPayMoney(yetLJSPayGrpBL.
                                              getSumActuPayMoney());
            yetLJAPayGrpBL.setPayIntv(yetLJSPayGrpBL.getPayIntv());
            yetLJAPayGrpBL.setLastPayToDate(yetLJSPayGrpBL.
                                            getLastPayToDate());
            yetLJAPayGrpBL.setCurPayToDate(yetLJSPayGrpBL.getCurPayToDate());
            yetLJAPayGrpBL.setPayDate(mPayDate);
            yetLJAPayGrpBL.setEnterAccDate(this.mEnterAccDate);
            yetLJAPayGrpBL.setPayType("YET");
            yetLJAPayGrpBL.setSerialNo(serNo);
            yetLJAPayGrpBL.setGetNoticeNo(yetLJSPayGrpBL.getGetNoticeNo());
            yetLJAPayGrpBL.setOperator(Operator);
            yetLJAPayGrpBL.setMakeDate(PubFun.getCurrentDate());
            yetLJAPayGrpBL.setMakeTime(PubFun.getCurrentTime());
            yetLJAPayGrpBL.setModifyDate(PubFun.getCurrentDate());
            yetLJAPayGrpBL.setModifyTime(PubFun.getCurrentTime());
            yetLJAPayGrpBL.setManageCom(yetLJSPayGrpBL.getManageCom()); //管理机构
            yetLJAPayGrpBL.setAgentCom(yetLJSPayGrpBL.getAgentCom()); //代理机构
            yetLJAPayGrpBL.setAgentType(yetLJSPayGrpBL.getAgentType()); //代理机构内部分类
            yetLJAPayGrpBL.setRiskCode(yetLJSPayGrpBL.getRiskCode()); //险种编码
            yetLJAPayGrpBL.setAgentCode(yetLJSPayGrpBL.getAgentCode());
            yetLJAPayGrpBL.setAgentGroup(yetLJSPayGrpBL.getAgentGroup());
            mLJAPayGrpSet.add(yetLJAPayGrpBL);

//            grpContPremTotal += yelLJAPayGrpBL.getSumActuPayMoney();

//            dealAppAcc(yetLJAPayGrpBL);
        }
        
        
        
        
        
        //应收总表转实收总表
        mLJAPayBL.setPayNo(payNo); //交费收据号码
        mLJAPayBL.setIncomeNo(mLJSPayBL.getOtherNo()); //应收/实收编号,集体合同号
        mLJAPayBL.setIncomeType(mLJSPayBL.getOtherNoType()); //应收/实收编号类型
        mLJAPayBL.setAppntNo(mLJSPayBL.getAppntNo()); //  投保人客户号码
        mLJAPayBL.setSumActuPayMoney(grpContPremTotal); // 总实交金额
        mLJAPayBL.setGetNoticeNo(mLJSPayBL.getGetNoticeNo()); //交费收据号,暂时先取应收总表的通知号
        mLJAPayBL.setEnterAccDate(mEnterAccDate); // 到帐日期
        mLJAPayBL.setPayDate(mPayDate); //交费日期,仍然取了最小日期
        mLJAPayBL.setConfDate(mConfDate); //确认日期
        mLJAPayBL.setApproveCode(mLJSPayBL.getApproveCode()); //复核人编码
        mLJAPayBL.setApproveDate(mLJSPayBL.getApproveDate()); //  复核日期
        mLJAPayBL.setSerialNo(serNo); //流水号
        mLJAPayBL.setOperator(Operator); // 操作员
        mLJAPayBL.setMakeDate(PubFun.getCurrentDate()); //入机时间
        mLJAPayBL.setMakeTime(PubFun.getCurrentTime()); //入机时间
        mLJAPayBL.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
        mLJAPayBL.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间
        mLJAPayBL.setRiskCode(mLJSPayBL.getRiskCode()); // 险种编码
        mLJAPayBL.setManageCom(mLJSPayBL.getManageCom());
        mLJAPayBL.setAgentCode(mLJSPayBL.getAgentCode());
        mLJAPayBL.setAgentGroup(mLJSPayBL.getAgentGroup());
        mLJAPayBL.setDueFeeType("1"); //add by fuxin 2008-7-3 新财务接口
        
        mLJAPayBL.setSaleChnl(mLJSPayBL.getSaleChnl());
        mLJAPayBL.setAgentCom(mLCGrpContSchema.getAgentCom());
        mLJAPayBL.setMarketType(mLJSPayBL.getMarketType());

        //下面更新lcgrpcont里面的信息
        mLCGrpContSchema.setSumPrem(mLCGrpContSchema.getSumPrem() +
                                    grpContPremTotal);
        mLCGrpContSchema.setModifyDate(PubFun.getCurrentDate()); //最后一次修改日期
        mLCGrpContSchema.setModifyTime(PubFun.getCurrentTime()); //最后一次修改时间
        //处理　余额
        double tempFeeSum = 0;
        for (int n = 1; n <= mLJTempFeeSet.size(); n++) {
            tempFeeSum += mLJTempFeeSet.get(n).getPayMoney();
        }
        grpContPremTotal = Arith.round(grpContPremTotal, 2);
        /*if (tempFeeSum + grpContDif < grpContPremTotal) {
            CError.buildErr(this, "交费不足，不能核销！");
            return false;
                 }*/
        //mLCGrpContSchema.setDif(tempFeeSum + grpContDif - grpContPremTotal);
        //设置LCCont 表数据
        System.out.println(" 续期核销时，更新LCCont ");
        mSqlLccont = "";

        //更新LCDuty 表
        if(!mINNC)
        {

            tSBql = new StringBuffer(128);
            tSBql.append(
                "UPDATE  LCCont SET  (SumPrem,PaytoDate) =")
                .append("(select SumPrem+sum(SumDuePayMoney),min(CurPayToDate) from ljspayperson where inputflag='1' and GrpContNo='")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("' and Contno=LCCont.Contno")
                .append(")")
                .append(",ModifyDate='")
                .append(PubFun.getCurrentDate())
                .append("' , ModifyTime ='")
                .append(PubFun.getCurrentTime())
                .append("'")
                .append(" where GrpContNo='")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("'")
                .append(" and contno  in (select distinct contno from ljspayperson where inputflag='1' and GrpContNo='")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("')");
            mSqlLccont = tSBql.toString();
            System.out.println(mSqlLccont);
            //更新LCPrem 表
            tSBql = new StringBuffer(128);
            tSBql.append(
                "UPDATE  LCPrem a  SET  PayTimes =PayTimes+ 1")
                .append(",(Prem,SumPrem,PaytoDate)=")
                .append(
                    "(SELECT SumActuPayMoney ,SumActuPayMoney+a.sumprem,CurPayToDate")
                .append(" FROM LJSPayPerson b WHERE b.PolNo=a.PolNo and b.DutyCode=a.DutyCode and b.PayPlanCode=a.PayPlanCode ")
                .append(" and b.grpcontno='")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("' fetch first row only )")
                .append(",ModifyDate='")
                .append(PubFun.getCurrentDate())
                .append("',ModifyTime='")
                .append(PubFun.getCurrentTime())
                .append("' where a.grpcontno='")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("' ")
                .append(" and exists(select distinct PolNo from ljspayperson where inputflag='1' and GrpContNo='")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("' and a.polno=polno)");

            ;
            mSqlPrem = tSBql.toString();
            System.out.println(mSqlPrem);
            tSBql = new StringBuffer(128);
            tSBql.append(
                "UPDATE  LCDuty a  SET  (SumPrem,PaytoDate)=")
                .append(
                    "(SELECT sum(SumActuPayMoney)+a.sumprem,min(CurPayToDate)")
                .append(
                    " FROM LJSPayPerson b WHERE b.PolNo=a.PolNo and b.DutyCode=a.DutyCode  ")
                .append(" and b.grpcontno='")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("' )")
                .append(",ModifyDate='")
                .append(PubFun.getCurrentDate())
                .append("',ModifyTime='")
                .append(PubFun.getCurrentTime())
                .append("' where exists(select distinct PolNo from ljspayperson where inputflag='1' and GrpContNo='")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("' and a.polno=polno)");

            ;
            mSqlDuty = tSBql.toString();
            System.out.println(mSqlDuty);

            //更新LCPol 表
            tSBql = new StringBuffer(128);
            tSBql.append(
                "UPDATE  LCPol a  SET  (SumPrem,PaytoDate)=")
                .append(
                    "(SELECT sum(SumActuPayMoney)+a.sumprem,min(CurPayToDate)")
                .append(" FROM LJSPayPerson b WHERE b.PolNo=a.PolNo  ")
                .append(" and b.grpcontno='")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("'  )")
                .append(",ModifyDate='")
                .append(PubFun.getCurrentDate())
                .append("',ModifyTime='")
                .append(PubFun.getCurrentTime())
                .append("' where a.grpcontno='")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("' ")
                .append(" and exists(select distinct PolNo from ljspayperson where inputflag='1' and GrpContNo='")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("' and a.polno=polno)");
            mSqlPol = tSBql.toString();
            System.out.println(mSqlPol);
        }
        //将无名单实名化人员的交至日期维护成无名单的交至日期，
        //无名单续期收费不影响处理payToDate外的其他保单数据
        else
        {
            StringBuffer sql = new StringBuffer();
//            sql.append("update LCPol a set payToDate = ")
//                .append("   ( case when polTypeFlag = '1' then (select min(b.CurPayToDate) from LJSPayPerson b where b.polNo = a.polNo) ")
//                .append("          else (select min(b.payToDate) from LCPol b")
//                .append("               where riskCode = a.riskCode")
//                .append("                 and b.contPlanCode = a.contPlanCode ")
//                .append("                 and b.polTypeFlag = '1') ")
//                .append("     end) ")
//                .append("where grpContNo = '")
//                .append(mLCGrpContSchema.getGrpContNo())
//                .append("' ");

            sql.append("update LCPol a set payToDate = ")
                .append("   (select min(CurPayToDate) from LJSPayPerson b where GrpContNo = a.GrpContNo and riskCode = a.riskCode) ")
                .append("where grpContNo = '")
                .append(mLCGrpContSchema.getGrpContNo())
                .append("'  and riskCode in(select riskCode from LJSPayPerson where grpcontNo = a.grpcontNo) ");
            this.mSqlPolWMD = sql.toString();

            sql.delete(0, sql.length());
            sql.append("update LCCont a set payToDate = ")
                .append("   (select min(payToDate) from LCPol ")
                .append("   where contNo = a.contNo) ")
                .append("where grpContNo = '")
                .append(mLCGrpContSchema.getGrpContNo()).append("' ");
            this.mSqlLccontWMD = sql.toString();

            sql.delete(0, sql.length());
            sql.append("update LCDuty a ")
                .append("set payToDate = ")
                .append("    (select payToDate from LCPol ")
                .append("    where polNo = a.polNo ) ")
                .append("where contNo in")
                .append("    (select contNo from LCCont where grpContNo = '"
                        + mLCGrpContSchema.getGrpContNo() + "') ");
            this.mSqlDutyWMD = sql.toString();

            sql.delete(0, sql.length());
            sql.append("update LCPrem a ")
                .append("set payToDate = ")
                .append("    (select payToDate from LCPol ")
                .append("    where polNo = a.polNo ) ")
                .append("where grpContNo = '")
                .append(mLCGrpContSchema.getGrpContNo()).append("' ");
            this.mSqlPremWMD = sql.toString();
        }

        //最后将暂交费和　暂交费分类信息　置核销标志
        System.out.println(" 续期核销时，更新LJTempFee ");
        for (int i = 1; i <= mLJTempFeeSet.size(); i++) {
            LJTempFeeBL tLJTempFeeBL = new LJTempFeeBL();
            tLJTempFeeBL.setSchema(mLJTempFeeSet.get(i));
            tLJTempFeeBL.setConfFlag("1");
            tLJTempFeeBL.setConfDate(CurrentDate);
            mLJTempFeeNewSet.add(tLJTempFeeBL);
        }
        System.out.println(" 续期核销时，更新LJTempFeeClass ");
        for (int i = 1; i <= mLJTempFeeClassSet.size(); i++) {
            LJTempFeeClassBL tLJTempFeeClassBL = new LJTempFeeClassBL();
            tLJTempFeeClassBL.setSchema(mLJTempFeeClassSet.get(i));
            tLJTempFeeClassBL.setConfFlag("1");
            tLJTempFeeClassBL.setConfDate(PubFun.getCurrentDate());
            mLJTempFeeClassNewSet.add(tLJTempFeeClassBL);
        }
        //设置应收总表备份表
        mLJSPayBSchema.setDealState("1");
        mLJSPayBSchema.setConfFlag("1");
        mLJSPayBSchema.setModifyDate(PubFun.getCurrentDate());
        mLJSPayBSchema.setModifyTime(PubFun.getCurrentTime());


        //设置应收集体表备份表
        System.out.println(" 续期核销时，更新LJSPayGrpBS");
        for (int s = 1; s <= tLJSPayGrpBSet.size(); s++) {
            LJSPayGrpBSchema tLJSPayGrpBSchema = new LJSPayGrpBSchema();
            tLJSPayGrpBSchema = tLJSPayGrpBSet.get(s);
            tLJSPayGrpBSchema.setDealState("1");
            tLJSPayGrpBSchema.setConfFlag("1");
            tLJSPayGrpBSchema.setModifyDate(PubFun.getCurrentDate());
            tLJSPayGrpBSchema.setModifyTime(PubFun.getCurrentTime());
            mLJSPayGrpBSet.add(tLJSPayGrpBSchema);
        }

        /* tSBql = new StringBuffer(128);
         tSBql.append(
                 "UPDATE  LJSPayPerSonB SET  DealState ='1' , ConfFlag='1'")
                 .append(" , ModifyDate ='")
                 .append(PubFun.getCurrentDate())
                 .append("' , ModifyTime ='")
                 .append(PubFun.getCurrentTime())
                 .append("'")
                 .append(" where GrpContNo='")
                 .append(mLCGrpContSchema.getGrpContNo())
                 .append("' and GetNoticeNo='")
                 .append(mLJSPayBL.getGetNoticeNo())
                 .append("'")
                 ;
         mSqlLjsPayPerB = tSBql.toString();
         System.out.println(mSqlLjsPayPerB);
         System.out.println(" 续期核销时，更新LJSPayPerSonB");*/

        return true;
    }

    /**
     * 投保人帐户余额抵扣生效
     * @param yelLJAPayGrpBL LJAPayGrpBL
     */
    private void dealAppAcc(LJAPayGrpBL yelLJAPayGrpBL)
    {
        LCAppAccTraceDB tLCAppAccTraceDB = new LCAppAccTraceDB();
        tLCAppAccTraceDB.setCustomerNo(yelLJAPayGrpBL.getAppntNo());
        tLCAppAccTraceDB.setOtherNo(yelLJAPayGrpBL.getGrpContNo());
        tLCAppAccTraceDB.setBakNo(yelLJAPayGrpBL.getGetNoticeNo());
        LCAppAccTraceSet set = tLCAppAccTraceDB.query();
        if(set.size() == 0)
        {
            return;
        }
       double money=0;
       for(int j=1;j<=set.size();j++)
       {
    	
	        LCAppAccTraceSchema schema = set.get(j);
	        if(schema.getMoney()>0)
	        {
	        	money=schema.getMoney(); //账户余额更新成正记录，否则为零
	        } 
       }
       
       for(int i=1;i<=set.size();i++)
       {
    	
	        LCAppAccTraceSchema schema = set.get(i);
	        if(schema.getMoney()>0)
	        {
	        	money=schema.getMoney(); //账户余额更新成正记录，否则为零
	        }
//	        schema.setAccBala(schema.getAccBala() + schema.getMoney());
	        schema.setAccBala(money);
	        schema.setState("1");
	        mAccMap.put(schema, "UPDATE");
       }

        String sql = "  update LCAppAcc "
                     + "set accBala =  " + money + ", "
                     + " accGetMoney =  " + money + ", "
                     + "   state = '1' "
                     + "where customerNo = '" + set.get(1).getCustomerNo() + "' ";
        mAccMap.put(sql, "UPDATE");
    }
    
    
    

    /**
     * 加入到催收核销日志表数据
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealUrgeLog(String pmDealState, String pmReason,
                                String pmOpreat) {

        LCUrgeVerifyLogSchema tLCUrgeVerifyLogSchema = new
                LCUrgeVerifyLogSchema();
        //加到催收核销日志表
        if (pmOpreat.equals("INSERT")) {
            tLCUrgeVerifyLogSchema.setSerialNo(mserNo);
            tLCUrgeVerifyLogSchema.setRiskFlag("1");
            tLCUrgeVerifyLogSchema.setOperateType("2"); //1：续期催收操作,2：续期核销操作
            tLCUrgeVerifyLogSchema.setOperateFlag(mOperateFlag); //1：个案操作,2：批次操作
            tLCUrgeVerifyLogSchema.setOperator(tGI.Operator);
            tLCUrgeVerifyLogSchema.setDealState(pmDealState); //1 : 正在处理中,2 : 处理错误,3 : 处理已完成

            tLCUrgeVerifyLogSchema.setContNo(mLCGrpContSchema.getGrpContNo());
            tLCUrgeVerifyLogSchema.setMakeDate(PubFun.getCurrentDate());
            tLCUrgeVerifyLogSchema.setMakeTime(PubFun.getCurrentTime());
            tLCUrgeVerifyLogSchema.setModifyDate(PubFun.getCurrentDate());
            tLCUrgeVerifyLogSchema.setModifyTime(PubFun.getCurrentTime());
            tLCUrgeVerifyLogSchema.setErrReason(pmReason);
        } else {
            LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new LCUrgeVerifyLogDB();
            LCUrgeVerifyLogSet tLCUrgeVerifyLogSet = new
                    LCUrgeVerifyLogSet();
            tLCUrgeVerifyLogDB.setSerialNo(mserNo);
            tLCUrgeVerifyLogDB.setOperateType("1");
            tLCUrgeVerifyLogDB.setOperateFlag(mOperateFlag);
            tLCUrgeVerifyLogDB.setRiskFlag("1");
            tLCUrgeVerifyLogSet = tLCUrgeVerifyLogDB.query();
            if (!(tLCUrgeVerifyLogSet == null) &&
                tLCUrgeVerifyLogSet.size() > 0) {
                tLCUrgeVerifyLogSchema = tLCUrgeVerifyLogSet.get(1);
                tLCUrgeVerifyLogSchema.setModifyDate(PubFun.getCurrentDate());
                tLCUrgeVerifyLogSchema.setModifyTime(PubFun.getCurrentTime());
                tLCUrgeVerifyLogSchema.setDealState(pmDealState);
                tLCUrgeVerifyLogSchema.setErrReason(pmReason);
            } else {
                return false;
            }

        }

        MMap tMap = new MMap();
        tMap.put(tLCUrgeVerifyLogSchema, pmOpreat);
        VData tInputData = new VData();
        tInputData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tInputData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LCUrgeVerifyLog 表失败!");
            return false;
        }
        return true;
    }


    private boolean prepareData() {
        mMMap.put(mLJSPayBL.getSchema(), "DELETE");
        mMMap.put(mLJSPayBSchema, "UPDATE");
        mMMap.put(mLJAPayBL.getSchema(), "INSERT");
        mMMap.put(mLJSPayGrpSet, "DELETE");
        mMMap.put(mLJSPayGrpBSet, "UPDATE");
        mMMap.put(mLJAPayGrpSet, "INSERT");
        mMMap.put(mLCGrpPolNewSet, "UPDATE");
        mMMap.put(mLCGrpContSchema, "UPDATE");
        // mMMap.put(mSqlLjsPayPer, "DELETE");
        // mMMap.put(mSqlLjsPayPerB, "UPDATE");
        // mMMap.put(mSqlLccont, "UPDATE");
        mMMap.put(mLJTempFeeNewSet, "UPDATE");
        mMMap.put(mLJTempFeeClassNewSet, "UPDATE");


        if(!mINNC)
        {
            mMMap.put(mSqlLccont, "UPDATE");
            mMMap.put(mSqlDuty, "UPDATE");
            mMMap.put(mSqlPrem, "UPDATE");
            mMMap.put(mSqlPol, "UPDATE");
        }
        else
        {
            mMMap.put(mSqlPolWMD, "UPDATE");  //必须在其他sql之前
            mMMap.put(mSqlLccontWMD, "UPDATE");
            mMMap.put(mSqlDutyWMD, "UPDATE");
            mMMap.put(mSqlPremWMD, "UPDATE");
        }
        mMMap.add(mAccMap);
        // mMMap.put(mSqlLjsPayPerB, "UPDATE");
        System.out.println(" 续期核销时，删除LJSPayPerSon");

        String strPerSQL =
            "select distinct dutycode from ljspayperson where grpcontno='" +
            mLCGrpContSchema.getGrpContNo() + "' with ur ";
        System.out.println(strPerSQL);
        ExeSQL tExeSQL = new ExeSQL();
        mPerSSRS = tExeSQL.execSQL(strPerSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "NormPayCollOperBL";
            tError.functionName = "dealOtherData";
            tError.errorMessage = "查询数据 LJAPayPerson 出错！";
            this.mErrors.addOneError(tError);
            return false;
        }
        for (int i = 1; i <= mPerSSRS.MaxRow; i++) {
            StringBuffer tSBql = new StringBuffer(128);
            tSBql.append("DELETE from LJSPayPerSon where GrpContNo='")
                    .append(mLCGrpContSchema.getGrpContNo())
                    .append("' and dutycode='")
                    .append(mPerSSRS.GetText(i,1))
                    .append("'")
                    ;
            mSqlLjsPayPer = tSBql.toString();

            mMMap.put(mSqlLjsPayPer, "DELETE");
        }


        mResult.add(mMMap);

        return true;
    }

    // 为更新应收个人表准备数据
    public boolean preparePersonData() {

        System.out.println(" 续期核销时，放入LJAPayPersonSet等数据，用于更新");
        
        MMap tMap = new MMap();
        tMap.put(mLJAPayPersonSet, SysConst.DELETE_AND_INSERT);
        mPerInputData.clear();
        mPerInputData.add(tMap);
        
        //mapPer.put(mLCDutySet, "UPDATE");
        //mapPer.put(mLCPremSet, "UPDATE");
        // mapPer.put(mLCPolSet, "UPDATE");
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) {

        //应收总表
        //  System.out.println("in getinputdata");
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mLCGrpContSchema = (LCGrpContSchema) mInputData.getObjectByObjectName(
                "LCGrpContSchema", 0);
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        String loadFlag = (String)mInputData.getObjectByObjectName("String", 0);
        if(loadFlag != null && loadFlag.equals("WMD"))
        {
            mINNC = true;
        }
        /*
         * *********************************************************************
         * 添加校验说明：无名单团单核销后，团单下被保人名为“无名单”的那种分单的交至日期是正确的，
         * 其他分单的交至日期没有更新，还是原来的日期。同时还发现，被保人为“无名单”
         * 的那种分单的保费也产生了错误。【OoO？】杨天政-20111121
         * *********************************************************************
         * 1.再次查询LCCONT无名单的标识符
         */
        String SQL_CHECK =
            "select 1 from lccont where grpcontno='" +
            mLCGrpContSchema.getGrpContNo() + "' " +
            		" and conttype='2' and poltype='1'" +
            		" with ur ";
        System.out.println(SQL_CHECK);
        ExeSQL tExeSQL = new ExeSQL();
        
        mCheck_flag = tExeSQL.execSQL(SQL_CHECK);
        /*
         * 2.进行判断
         */
        if(mCheck_flag.MaxRow>0)
        {
        	//-----------------日志服务器不识别汉字，因此只能用英文输出相关的提示信息------------------------
        	System.out.println("This is a WMD policy,the grpcontno is："+mLCGrpContSchema.getGrpContNo());
        	mINNC = true;
        }
        else
        {
        	System.out.println("This is a normal policy,the grpcontno is："+mLCGrpContSchema.getGrpContNo());
        	mINNC = false;
        }
        /*
         * ------------THE END------------------
         */
        
        if (mLCGrpContSchema == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "NormPayCollOperBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData() {
        mInputData = new VData();
        mMMap.put(mLCGrpContSchema, "UPDATE");
        mInputData.add(mMMap);

        System.out.println("prepareOutputData:");

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
//    private boolean dealOtherData() {
//        mInputData = new VData();
//        mMMap = new MMap();
//
//
//        mInputData.add(mMMap);
//        PubSubmit tPubSubmit = new PubSubmit();
//        if (tPubSubmit.submitData(mInputData, "") == false) {
//            System.out.println(tPubSubmit.mErrors.getFirstError());
//            CError tError = new CError();
//            tError.moduleName = "PubSubmit";
//            tError.functionName = "submitData";
//            tError.errorMessage = "sql语句数据处理时 PubSubmit 失败!";
//
//            this.mErrors.addOneError(tError);
//            return false;
//        }
//
//        return true;
//    }

    
    private MMap lockLJAGet(LCGrpContSchema mLCGrpContSchema)
    {
    	
    	
        MMap tMMap = null;
        /**锁定标志"TX"*/
        String tLockNoType = "TX";
        /**锁定时间*/
        String tAIS = "3600";
        System.out.println("设置了60分的解锁时间");
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", mLCGrpContSchema.getGrpContNo());
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);
       
        VData tVData = new VData();
        tVData.add(tTransferData);
        tVData.add(tGI);
        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        
        if (tMMap == null)
        {           	
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }
        return tMMap;
    }

    private void jbInit() throws Exception {
    }


}
