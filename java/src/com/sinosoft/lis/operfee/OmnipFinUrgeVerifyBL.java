package com.sinosoft.lis.operfee;

import java.util.Date;
import java.util.Hashtable;

import com.sinosoft.lis.bl.LCDutyBL;
import com.sinosoft.lis.bl.LCPolBL;
import com.sinosoft.lis.bl.LCPremBL;
import com.sinosoft.lis.bl.LJAPayBL;
import com.sinosoft.lis.bl.LJAPayPersonBL;
import com.sinosoft.lis.bl.LJSPayBL;
import com.sinosoft.lis.bl.LJTempFeeBL;
import com.sinosoft.lis.bl.LJTempFeeClassBL;
import com.sinosoft.lis.bq.AppAcc;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LCAppAccDB;
import com.sinosoft.lis.db.LCAppAccTraceDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCContStateDB;
import com.sinosoft.lis.db.LCDutyDB;
import com.sinosoft.lis.db.LCInsureAccClassDB;
import com.sinosoft.lis.db.LCInsureAccClassFeeDB;
import com.sinosoft.lis.db.LCInsureAccDB;
import com.sinosoft.lis.db.LCInsureAccFeeDB;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCPremDB;
import com.sinosoft.lis.db.LCRnewStateLogDB;
import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.db.LJSPayPersonDB;
import com.sinosoft.lis.db.LJTempFeeClassDB;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.db.LMRiskFeeDB;
import com.sinosoft.lis.finfee.TempFeeQueryUI;
import com.sinosoft.lis.pubfun.Arith;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCAppAccTraceSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCContStateSchema;
import com.sinosoft.lis.schema.LCInsureAccClassFeeSchema;
import com.sinosoft.lis.schema.LCInsureAccClassSchema;
import com.sinosoft.lis.schema.LCInsureAccFeeSchema;
import com.sinosoft.lis.schema.LCInsureAccFeeTraceSchema;
import com.sinosoft.lis.schema.LCInsureAccSchema;
import com.sinosoft.lis.schema.LCInsureAccTraceSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LDCode1Schema;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.lis.schema.LJSPayPersonSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.schema.LMRiskFeeSchema;
import com.sinosoft.lis.vschema.LCAppAccTraceSet;
import com.sinosoft.lis.vschema.LCContStateSet;
import com.sinosoft.lis.vschema.LCDutySet;
import com.sinosoft.lis.vschema.LCInsureAccClassFeeSet;
import com.sinosoft.lis.vschema.LCInsureAccClassSet;
import com.sinosoft.lis.vschema.LCInsureAccFeeSet;
import com.sinosoft.lis.vschema.LCInsureAccSet;
import com.sinosoft.lis.vschema.LCInsureAccTraceSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LCPremSet;
import com.sinosoft.lis.vschema.LCRnewStateLogSet;
import com.sinosoft.lis.vschema.LJAPayPersonSet;
import com.sinosoft.lis.vschema.LJSPayPersonSet;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.lis.vschema.LJTempFeeClassSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LMRiskFeeSet;
import com.sinosoft.lis.xb.PRnewContSignBL;
import com.sinosoft.lis.xb.XBConst;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class OmnipFinUrgeVerifyBL {
	   public CErrors mErrors = new CErrors();

	    /** 往后面传输数据的容器 */
	    private VData mInputData;

	    //保存操作员和管理机构的类
	    private GlobalInput mGI = new GlobalInput();

	    private boolean mNeedRnewVerify = false;

	    /** 数据操作字符串 */
	    private String mOperate;
	    private String CurrentDate = PubFun.getCurrentDate();
	    private String CurrentTime = PubFun.getCurrentTime();
	    private String serNo = ""; //流水号
	    private String tLimit = "";
	    private String payNo = ""; //交费收据号
	    private double mDif = 0; //余额
	    private MMap map = new MMap();
	    private String mGetNoticeNo ;

	//暂收费表
	    private LJTempFeeBL mLJTempFeeBL = new LJTempFeeBL();
	    private LJTempFeeSchema mLJTempFeeSchema = new LJTempFeeSchema();
	    private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
	    private LJTempFeeSet mLJTempFeeNewSet = new LJTempFeeSet();
	    private LJTempFeeSet mLJTempFeeNewInsSet = new LJTempFeeSet();


	//暂收费分类表
	    private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
	    private LJTempFeeClassSet mLJTempFeeClassNewSet = new LJTempFeeClassSet();
	    private LJTempFeeClassSet mLJTempFeeClassNewInsSet = new LJTempFeeClassSet();


	//应收个人交费表
	    private LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet();
	    private LJSPayPersonSet mLJSPayPersonNewSet = new LJSPayPersonSet();

	//应收总表
	    private LJSPayBL mLJSPayBL = new LJSPayBL();
	    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();
	//实收个人交费表
	    private LJAPayPersonSet mLJAPayPersonSet = new LJAPayPersonSet();

	//实收总表
	    private LJAPayBL mLJAPayBL = new LJAPayBL();
	    private LJAPaySchema mLJAPaySchema = new LJAPaySchema();
	//个人合同表
	    private LCContSchema mLCContSchema = new LCContSchema();
	    // private LOPRTManagerSchema mLOPRTManagerSchema,mLOPRTManagerSchemaNew;
	//个人保单表
	    private LCPolSet mLCPolSet = new LCPolSet();
	//保费项表
	    private LCPremSet mLCPremSet = new LCPremSet();
	    private LCPremSet mLCPremNewSet = new LCPremSet();

	//保险责任表LCDuty
	    private LCDutySet mLCDutySet = new LCDutySet();
	    private LCDutySet mLCDutyNewSet = new LCDutySet();
	    LCInsureAccFeeTraceSchema mLCInsureAccFeeTraceSchema = new LCInsureAccFeeTraceSchema();
	//保险帐户表
	    private LCInsureAccSet mLCInsureAccSet = new LCInsureAccSet();
	    LCInsureAccTraceSchema mLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
	   
	//保险帐户表记价履历表
	    private LCInsureAccSchema mLCInsureAccSchema =new LCInsureAccSchema();
	    private LCInsureAccTraceSet mLCInsureAccTraceSet = new LCInsureAccTraceSet();
	    private boolean mUrgeFlag = true; // 是否进行核销操作
	    private boolean mPayMode = true; // 交费方式，默认为现金模式
	    private String mLJSPayBSql = ""; // 用于更新总应收备份表
	    private String mLJSPayPerBSql = ""; // 用于更新个人应收备份表
	    private String mPayDate = ""; //缴费日期
	    private String mEnterAccDate = ""; //到帐日期
	    private String mConfDate = ""; //财务确认日期
	    private TransferData mTransferData;
	    private String mRnewState = null;
		private LCPolSchema mLCPolSchema = new LCPolSchema();
		private String mInsuAccNo = "";
		private Reflections ref = new Reflections();
		private String aMakeDate = PubFun.getCurrentDate();
		private String aMakeTime = PubFun.getCurrentTime();
		
		private LCInsureAccClassSet mLCInsureAccClassSet = new LCInsureAccClassSet();
		private LCInsureAccFeeSchema mLCInsureAccFeeSchema = new LCInsureAccFeeSchema();
		private LCInsureAccClassFeeSet mLCInsureAccClassFeeSet = new LCInsureAccClassFeeSet();
		private String mFeeCode;
		private double ContactuMoney = 0.0; // 合同的保费合计金额
	    private int mPayTimes = 0; 
	    private MMap pMap=new MMap();

	    public MMap getSubmitMap(VData cInputData, String cOperate) {

	        //将操作数据拷贝到本类中
	        this.mOperate = cOperate;

	        //得到外部传入的数据,将数据备份到本类中
	        if (!getInputData(cInputData)) {
	            return null;
	        }
	        System.out.println("After getinputdata");
	        //进行业务处理
	        if (!dealData()) {
	            return null;
	        }
	        if(!setInsuredAcc()){
	        	return null;
	        }
          //TODO
	        /*
	        if(!dealPhoneHasten())
	        {
	            return null;
	        }
*/
	        //万能自动缓交记录
	        if(!dealPolState()){
	        	return null;
	        }
	        
	        //准备往后台的数据
	        if (!prepareOutputData()) {
	            return null;
	        }

	        if(!dealPRnewSign())
	        {
	            return null;
	        }

	        System.out.println("After prepareOutputData");

	        return map;
	    }

	    /**
	     * 个单应收记录作废/撤销类，在IndiLJSCancelBL.getSubmitMap方法 中增加对催缴工单的处理
	     * @return boolean
	     */
	    /*
	    private boolean dealPhoneHasten()
	    {
	        String sql = "select * "
	                     + "from LGPhoneHasten "
	                     + "where getNoticeNo = '"
	                     + mLJSPayBL.getGetNoticeNo() + "' "
	                     + "   and HastenType = '"
	                     + NextFeeHastenTask.HASTEN_TYPE_NEXTFEEP + "' "
	                     + "   and FinishType is null ";
	        System.out.println(sql);
	        LGPhoneHastenSet set = new LGPhoneHastenDB().executeQuery(sql);
	        if(set.size() == 0)
	        {
	            //若无催缴就查不出来也
	            return true;
	        }

	        LGPhoneHastenSchema schema = set.get(1);
	        schema.setFinishType("4");
	        schema.setRemark("应收记录" + mLJSPayBL.getGetNoticeNo() + "已交费");

	        VData data = new VData();
	        data.add(schema);
	        data.add(mGI);

	        TaskPhoneFinishBL bl = new TaskPhoneFinishBL();
	        MMap tMMap = bl.getSubmitMap(data, "");
	        if(tMMap == null)
	        {
	            mErrors.copyAllErrors(bl.mErrors);
	            return false;
	        }
	        map.add(tMMap);

	        return true;
	    }

*/
	    /**
	     * 从输入数据中得到所有对象
	     * @param mInputData：包括
	     * 1、LCContSchema：保单信息
	     * 2、GlobalInput：操作员信息
	     * @param cOperate: 操作方式，此为""
	     *输出：操作失败返回false,否则返回true
	     */
	    public boolean submitData(VData cInputData, String cOperate) {

	        getSubmitMap(cInputData, cOperate);
	        if (map == null || mErrors.needDealError()) {
	            return false;
	        }
	        
	        PubSubmit tPubSubmit = new PubSubmit();
	        if (!tPubSubmit.submitData(mInputData, mOperate)) {
	            // @@错误处理
	            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
	            CError tError = new CError();
	            tError.moduleName = "ContBL";
	            tError.functionName = "submitData";
	            tError.errorMessage = "数据提交失败!";
	            this.mErrors.addOneError(tError);
	            return false;
	        }

	        return true;
	    }

	    /**
	     * 处理保单续保确认
	     * @return boolean
	     */
	    private boolean dealPRnewSign()
	    {
	        if(!needPRnewOperate())
	        {
	            return true;
	        }

	        if(mRnewState.equals(XBConst.RNEWSTATE_UNCONFIRM))
	        {
	            VData data = new VData();

	            LCPolDB db = new LCPolDB();
	            db.setContNo(mLCContSchema.getContNo());
//	            LCPolSet tLCPolSet = db.query();
	            String sqlPolSet ="select a.* From lcpol a, ljspayperson b  where a.contno ='"+mLCContSchema.getContNo()+"'"
	                             +" and a.polno = b.polno "
	                             +" and b.getnoticeno ='"+mGetNoticeNo+"'"
	                             ;
	           LCPolSet tLCPolSet = db.executeQuery(sqlPolSet);

	            data.add(tLCPolSet);
	            data.add(mGI);
	            data.add(new TransferData());

	            System.out.println(
	                "Beginning of PRnewDueVerifyBL.dealData: PRnewContSign");
	            PRnewContSignBL tPRnewContSignBL = new PRnewContSignBL();
	            MMap tMMap = tPRnewContSignBL.getSubmitData(data, mOperate);
	            if(tMMap == null && tPRnewContSignBL.mErrors.needDealError())
	            {
	                mErrors.copyAllErrors(tPRnewContSignBL.mErrors);
	                System.out.println(tPRnewContSignBL.mErrors.getErrContent());
	                return false;
	            }
	            map.add(tMMap);
	        }

	        return true;
	    }

	    /**
	     * 校验是否需要续保确认和数据转移
	     * 1、没有续保轨迹，不需要续保转移，false
	     * 2、有续保轨迹但状态不是待确认、确认成功，转移成功，false
	     * @return boolean
	     */
	    private boolean needPRnewOperate()
	    {
	        String sql = "  select * "
	                     + "from LCRnewStateLog a,ljspay b "
	                     + "where contNo = '" + mLCContSchema.getContNo() + "' "
	                     + "   and newContNo = "
	                     + "      (select max(newContNo) "
	                     + "      from LCRnewStateLog "
	                     + "      where contNo = a.contNo "
	                     + "         and state != '6') "
	                     + " and a.contno = b.otherno and getnoticeno='"+mLJSPayBL.getGetNoticeNo()+"' "
	                     ;
	        System.out.println(sql);
	        LCRnewStateLogDB tLCRnewStateLogDB = new LCRnewStateLogDB();
	        LCRnewStateLogSet tLCRnewStateLogSet
	            = tLCRnewStateLogDB.executeQuery(sql);
	        if(tLCRnewStateLogSet.size() == 0)
	        {
	            return false;
	        }
	        mRnewState = tLCRnewStateLogSet.get(1).getState();
	        if(tLCRnewStateLogSet.get(1).getState()
	           .compareTo(XBConst.RNEWSTATE_UNCONFIRM) < 0)
	        {
	            return false;
	        }

	        return true;
	    }


	    private boolean dealData() {
	
//	    	20111010 YangTianZheng 防止并发加锁
	    	MMap tCekMap1 = null;
	    	tCekMap1 = lockLJAGet(mLCContSchema);
	        if (tCekMap1 == null)
	        {
	            return false;
	        }
	        map.add(tCekMap1);
//	--------------------------
	        // step one-查询数据
	        String ContNo = mLCContSchema.getContNo();
	        LJSPayPersonDB tmpLJSPayPersonDB = new LJSPayPersonDB();
	        // 需要根据通知书号码进行查询，以保证唯一
	        tmpLJSPayPersonDB.setGetNoticeNo(mLJSPayBL.getGetNoticeNo());
	        mLJSPayPersonSet = tmpLJSPayPersonDB.query();
	        String ManageCom = mGI.ManageCom;
	        // 合同的保费合计金额
	        double ContactuMoney = 0;
	        boolean actuVerifyFlag = false;
	        // 0-判断应收总表中的应收是否=0
	        if (mLJSPayBL.getSumDuePayMoney() == 0) {
	            // "应收总表应收款为0!请执行自动核销";
	            actuVerifyFlag = true;
	        }
	       
	        // 产生流水号
	        if (mTransferData == null) {
	            tLimit = PubFun.getNoLimit(ManageCom);
	            serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); //给应收表生成统一的流水号
	        } else {
	            
	            tLimit = PubFun.getNoLimit(ManageCom);	            
	            serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); //给应收表生成统一的流水号
	           
	            mNeedRnewVerify = ((String) mTransferData
	                               .getValueByName("NeedRnewVerify")) != null;
	        }
	        payNo = PubFun1.CreateMaxNo("PayNo", tLimit); //给实收表生成统一的　收据号

	        // 1-查询需要核销的保单险种
	        LCPolDB tLCPolDB = new LCPolDB();
	        StringBuffer tSBql = new StringBuffer(128);

	        //续期险种
	        tSBql.append("select * ")
	            .append("from LCPol a ")
	            .append("where ContNo = '")
	            .append(ContNo)
	            .append("'  and AppFlag = '1' ")
	            .append("  and (StateFlag = '"+BQ.STATE_FLAG_SIGN+"' or StateFlag is null) and (a.StandbyFlag1 is null or a.StandbyFlag1 = '0' or a.StandbyFlag1 = '' or a.StandbyFlag1='2')")//modify by 20150730新缓交状态的保单得允许核销啊
	            .append("   and polNo not in ")
	            .append("       (select polNo from LCRnewStateLog ")
            	.append("        where contNo = a.contNo ")
            	.append("           and state != '6') ");
	        	//20150303万能险种和传统险一起承保，添加续保险种
            if(mNeedRnewVerify)
            {
            	tSBql.append(" union ")
		        	 .append("select * ")
	                 .append("from LCPol ")
	                 .append("where contNo = ")
	                 .append(" (select max(newContNo) ")
	                 .append(" from LCRnewStateLog ")
	                 .append(" where contNo = '")
	                 .append(ContNo)
	                 .append("' and state = '")
	                 .append(XBConst.RNEWSTATE_UNCONFIRM)
	                 .append("' ")
	                 .append("and polno in (select polno from LJSPayPerson where GetNoticeNo='"+mGetNoticeNo+"') ")
	                 .append(") ");
            }
	        LCPolSet tLCPolSet = tLCPolDB.executeQuery(tSBql.toString());
	        if (tLCPolDB.mErrors.needDealError()) {
	         
	            this.mErrors.copyAllErrors(tLCPolDB.mErrors);
	            CError tError = new CError();
	            tError.moduleName = "OmnipFinUrgeVerifyBL";
	            tError.functionName = "dealData";
	            tError.errorMessage = "万能个险险种表查询失败!";
	            this.mErrors.addOneError(tError);
	            tLCPolSet.clear();
	            return false;
	        }
					ExeSQL tExeSQL = new ExeSQL();
	        String strS="select count(1) from LCPol a where ContNo = '"+ContNo+"' and AppFlag = '1' and (StateFlag = '"+BQ.STATE_FLAG_SIGN+"' or StateFlag is null) and (a.StandbyFlag1 is null or a.StandbyFlag1 = '0' or a.StandbyFlag1 = '' or a.StandbyFlag1='2')";
	        String polFlag=tExeSQL.getOneValue(strS);
	        
	        if (tLCPolSet.size() == 0 || polFlag.equals("0")) {
	            CError.buildErr(this, "保单号为" + ContNo + "下的险种信息缺失，不能核销！");
	            return false;
	        }

	        String polPayToDate = "";
	        for (int n = 1; n <= tLCPolSet.size(); n++) {
	            LCPolBL tempLCPolBL = new LCPolBL();
	            tempLCPolBL.setSchema(tLCPolSet.get(n));

	            //若为续保险种应收转实收，则得到原险种信息
	            LCPolSchema oldLCPolSchema = getOldLCPolSchema(tempLCPolBL);//TODO 不是续保险种，不需此步骤
	         

	            double polTotalPrem = 0.0; // 保存单个险种下的所交保费总和
	            int polCount = 0; // 判断该险种下是否有对应的应收数据信息，有则修改险种信息，否则不变，杨红于2005-07-17说明
	            String polNo = tempLCPolBL.getPolNo(); // 保单险种号

	            // 查询责任项
	            LCDutyDB tLCDutyDB = new LCDutyDB();
	            tLCDutyDB.setPolNo(polNo);
	            LCDutySet tLCDutySet = tLCDutyDB.query();
	            if (tLCDutyDB.mErrors.needDealError()) {
	                // @@错误处理
	                this.mErrors.copyAllErrors(tLCDutyDB.mErrors);
	                CError tError = new CError();
	                tError.moduleName = "IndiFinUrgeVerifyBL";
	                tError.functionName = "dealData";
	                tError.errorMessage = "险种责任表查询失败!";
	                this.mErrors.addOneError(tError);
	                return false;
	            }
	            if (tLCDutySet.size() == 0) {
	                CError.buildErr(this,
	                                "保单号:" + ContNo + "，险种号：" + polNo +
	                                "下的险种责任信息缺失，不能核销！");
	                return false;
	            }

	            for (int m = 1; m <= tLCDutySet.size(); m++) {
	                LCDutyBL tLCDutyBL = new LCDutyBL();
	                tLCDutyBL.setSchema(tLCDutySet.get(m));
	                // 责任编码
	                String dutyCode = tLCDutySet.get(m).getDutyCode();

	                LCPremDB tLCPremDB = new LCPremDB();
	                tLCPremDB.setPolNo(polNo);
	                tLCPremDB.setDutyCode(dutyCode);
	                LCPremSet tLCPremSet = tLCPremDB.query();
	                if (tLCPremDB.mErrors.needDealError()) {
	                    // @@错误处理
	                    this.mErrors.copyAllErrors(tLCPremDB.mErrors);
	                    CError tError = new CError();
	                    tError.moduleName = "IndiFinUrgeVerifyBL";
	                    tError.functionName = "dealData";
	                    tError.errorMessage = "保费项信息查询失败!";
	                    this.mErrors.addOneError(tError);
	                    // tLCPolSet.clear();
	                    return false;
	                }
	                if (tLCPremSet.size() == 0) {
	                    CError.buildErr(this,
	                                    "保单号:" + ContNo + "，险种号：" + polNo +
	                                    ",责任编码:" +
	                                    dutyCode + "下的险种责任信息缺失，不能核销！");
	                    return false;
	                }

	                int dutyCount = 0; // 责任的保费项个数
	                double totalPrem = 0.0; // 责任下的保费项保费
	                String PaytoDate = ""; // 责任下的保费项交至日期
	                for (int t = 1; t <= tLCPremSet.size(); t++) {
	                    LCPremBL tLCPremBL = new LCPremBL();
	                    tLCPremBL.setSchema(tLCPremSet.get(t));
	                    // 交费计划编码
	                    String payPlanCode = tLCPremSet.get(t).getPayPlanCode();

	                    // 查询应收个人交费表中的正常交费记录
	                    LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
	                    tLJSPayPersonDB.setPolNo(oldLCPolSchema.getPolNo());
	                    tLJSPayPersonDB.setDutyCode(dutyCode);
	                    tLJSPayPersonDB.setPayPlanCode(payPlanCode);
	                  
	                    // tLJSPayPersonDB.setPayType("ZC");
	                    // 由于支持多次抽档，因此所有的LJS表查询都需要加上GetNoticeNo条件
	                    tLJSPayPersonDB.setGetNoticeNo(mLJSPayBL.getGetNoticeNo());
	                    LJSPayPersonSet tLJSPayPersonSet = tLJSPayPersonDB.query();
	                    if (tLJSPayPersonDB.mErrors.needDealError()) {
	                        // @@错误处理
	                        this.mErrors.copyAllErrors(tLJSPayPersonDB.mErrors);
	                        CError tError = new CError();
	                        tError.moduleName = "IndiFinUrgeVerifyBL";
	                        tError.functionName = "dealData";
	                        tError.errorMessage = "应收个人表查询失败!";
	                        this.mErrors.addOneError(tError);
	                       
	                        return false;
	                    }
	                    if (tLJSPayPersonSet.size() == 0) {
	                        continue;
	                    } else {
	                        // 这里tLJSPayPersonSet.size()肯定为1
	                        tLCPremBL.setPayTimes(tLCPremBL.getPayTimes() + 1); // 已交费次数
	                        mPayTimes =tLCPremBL.getPayTimes();
	                        tLCPremBL.setSumPrem(tLCPremBL.getSumPrem() +
	                                             tLCPremBL.getPrem()); // 累计保费
	                        tLCPremBL.setPaytoDate(tLJSPayPersonSet.get(1).
	                                               getCurPayToDate()); // 交至日期
	                        tLCPremBL.setModifyDate(CurrentDate); // 最后一次修改日期
	                        tLCPremBL.setModifyTime(CurrentTime); // 最后一次修改时间
	                        mLCPremNewSet.add(tLCPremBL);

	                        // 杨红说明：取保费项较早的paytodate作为责任表的paytodate
	                        if (PaytoDate.equals("")) {
	                            PaytoDate = tLCPremBL.getPaytoDate();
	                        } else {
	                            FDate tFDate = new FDate();
	                            Date tDate = tFDate.getDate(PaytoDate);
	                            Date tDate1 = tFDate.getDate(tLCPremBL.getPaytoDate());
	                            if (tDate1.before(tDate)) {
	                                PaytoDate = tLCPremBL.getPaytoDate();
	                            }
	                        }
	                        polPayToDate = tLCPremBL.getPaytoDate(); // 险种的交至日期，随便取一个应收表的信息即可，杨红
	                        // 说明

	                        totalPrem += tLCPremBL.getPrem();
	                        polTotalPrem += tLCPremBL.getPrem();
	                        dutyCount++;
	                        polCount++;

	                        // 多少应收信息产生多少实收信息，方便以后统计使用
	                        for (int k = 1; k <= tLJSPayPersonSet.size(); k++) {
	                            // 将应收个人表的数据映射到实收个人表中去
	                            LJAPayPersonBL tLJAPayPersonBL = new LJAPayPersonBL();
	                            tLJAPayPersonBL.setPolNo(tLJSPayPersonSet.get(k).
	                                    getPolNo()); // 保单号码
	                            tLJAPayPersonBL.setPayCount(tLJSPayPersonSet.get(k).
	                                    getPayCount()); // 第几次交费
	                            tLJAPayPersonBL.setGrpContNo(tLJSPayPersonSet.get(k).
	                                    getGrpContNo()); // 集体保单号码
	                            tLJAPayPersonBL.setGrpPolNo(tLJSPayPersonSet.get(k).
	                                    getGrpPolNo()); // 集体保单号码
	                            tLJAPayPersonBL.setContNo(tLJSPayPersonSet.get(k).
	                                    getContNo()); // 总单/合同号码
	                            tLJAPayPersonBL.setAppntNo(tLJSPayPersonSet.get(k).
	                                    getAppntNo()); // 投保人客户号码
	                            tLJAPayPersonBL.setPayNo(payNo); // 交费收据号码
	                            tLJAPayPersonBL.setPayTypeFlag(tLJSPayPersonSet.get(
	                                    k).getPayTypeFlag());
	                            tLJAPayPersonBL.setPayAimClass(tLJSPayPersonSet.get(
	                                    k).getPayAimClass()); // 交费目的分类
	                            tLJAPayPersonBL.setDutyCode(tLJSPayPersonSet.get(k).
	                                    getDutyCode()); // 责任编码
	                            tLJAPayPersonBL.setPayPlanCode(tLJSPayPersonSet.get(
	                                    k).getPayPlanCode()); // 交费计划编码
	                            tLJAPayPersonBL.setSumDuePayMoney(tLJSPayPersonSet.
	                                    get(k).getSumDuePayMoney()); // 总应交金额
	                            tLJAPayPersonBL.setSumActuPayMoney(tLJSPayPersonSet.
	                                    get(k).getSumActuPayMoney()); // 总实交金额
	                            tLJAPayPersonBL.setPayIntv(tLJSPayPersonSet.get(k).
	                                    getPayIntv()); // 交费间隔
	                            tLJAPayPersonBL.setPayDate(mPayDate); // 交费日期
	                            tLJAPayPersonBL.setPayType(tLJSPayPersonSet.get(k).
	                                    getPayType()); // 交费类型
	                            if (actuVerifyFlag) {
	                                tLJAPayPersonBL.setEnterAccDate(mEnterAccDate); // 到帐日期
	                                tLJAPayPersonBL.setConfDate(mConfDate); // 确认日期
	                            } else {
	                                // 如果不需要自动核销
	                                tLJAPayPersonBL.setEnterAccDate(mEnterAccDate); // 到帐日期
	                                tLJAPayPersonBL.setConfDate(mConfDate); // 确认日期
	                            }
	                            tLJAPayPersonBL.setLastPayToDate(tLJSPayPersonSet.
	                                    get(k).getLastPayToDate()); // 原交至日期
	                            tLJAPayPersonBL.setCurPayToDate(tLJSPayPersonSet.
	                                    get(k).getCurPayToDate()); // 现交至日期
	                            tLJAPayPersonBL.setInInsuAccState(tLJSPayPersonSet.
	                                    get(k).getInInsuAccState()); // 转入保险帐户状态
	                            tLJAPayPersonBL.setApproveCode(tLJSPayPersonSet.get(
	                                    k).getApproveCode()); // 复核人编码
	                            tLJAPayPersonBL.setApproveDate(tLJSPayPersonSet.get(
	                                    k).getApproveDate()); // 复核日期
	                            tLJAPayPersonBL.setApproveTime(tLJSPayPersonSet.get(
	                                    k).getApproveTime()); // 复核时间
	                            tLJAPayPersonBL.setAgentCode(tLJSPayPersonSet.get(k).
	                                    getAgentCode());
	                            tLJAPayPersonBL.setAgentGroup(tLJSPayPersonSet.get(
	                                    k).getAgentGroup());
	                            tLJAPayPersonBL.setSerialNo(serNo); // 流水号
	                            // tLJAPayPersonBL.setOperator(tLJSPayPersonSet.get(k).getOperator());
	                            // //产生应收的操作员
	                            tLJAPayPersonBL.setOperator(mGI.Operator); // 实际截转为实收的操作员
	                            tLJAPayPersonBL.setMakeDate(CurrentDate); // 入机日期
	                            tLJAPayPersonBL.setMakeTime(CurrentTime); // 入机时间
	                            tLJAPayPersonBL.setGetNoticeNo(tLJSPayPersonSet.get(
	                                    k).getGetNoticeNo()); // 通知书号码
	                            tLJAPayPersonBL.setModifyDate(CurrentDate); // 最后一次修改日期
	                            tLJAPayPersonBL.setModifyTime(CurrentTime); // 最后一次修改时间
	                            tLJAPayPersonBL.setManageCom(tLJSPayPersonSet.get(k).
	                                    getManageCom()); // 管理机构
	                            tLJAPayPersonBL.setAgentCom(tLJSPayPersonSet.get(k).
	                                    getAgentCom()); // 代理机构
	                            tLJAPayPersonBL.setAgentType(tLJSPayPersonSet.get(k).
	                                    getAgentType()); // 代理机构内部分类
	                            tLJAPayPersonBL.setRiskCode(tLJSPayPersonSet.get(k).
	                                    getRiskCode()); // 险种编码
	                            mLJAPayPersonSet.add(tLJAPayPersonBL);
	                        }
	                        // 应收个人 转 实收个人 结束
	                    }
	                }
	                if (dutyCount == 0) {
	                    continue;
	                } else {
	                    tLCDutyBL.setPrem(totalPrem); // 实际保费
	                    tLCDutyBL.setSumPrem(tLCDutyBL.getSumPrem() + totalPrem); // 累计保费
	                    tLCDutyBL.setPaytoDate(PaytoDate); // 交至日期
	                    tLCDutyBL.setModifyDate(CurrentDate); // 最后一次修改日期
	                    tLCDutyBL.setModifyTime(CurrentTime); // 最后一次修改时间
	                    mLCDutyNewSet.add(tLCDutyBL);
	                }
	            }
	            if (polCount == 0) {
	                continue;
	            } else {
	                // 修改险种信息
	                tempLCPolBL.setPaytoDate(polPayToDate); // 交至日期
	                tempLCPolBL.setSumPrem(tempLCPolBL.getSumPrem() + polTotalPrem); // 总累计保费
	                // 求总余额:如果应收总表应收款=0，表明有余额，且这次的余额值存放在责任编码为
	                // "yet"的应收个人交费纪录中（见个人催收流程图）,取出放在个人保单表的余额字段中
	                // 否则，个人保单表余额纪录置为0
	                tempLCPolBL.setModifyDate(CurrentDate); // 最后一次修改日期
	                tempLCPolBL.setModifyTime(CurrentTime); // 最后一次修改时间
	                mLCPolSet.add(tempLCPolBL);
	            }
	            ContactuMoney += polTotalPrem;
	        }

	        // 杨红于2005-07-17说明: 下面处理合同的余额问题，续期中很重要的一个环节
	        tSBql = new StringBuffer(128);
	        tSBql.append("select * from ljspayperson where polno in (select polno from lcpol where contno='");
	        tSBql.append(ContNo);
	        tSBql.append("') and paytype='YET' and GetNoticeNo = '");
	        tSBql.append(mLJSPayBL.getGetNoticeNo());
	        tSBql.append("'");
	        LJSPayPersonDB tyetLJSPayPersonDB = new LJSPayPersonDB();
	        LJSPayPersonSet tyetLJSPayPersonSet = tyetLJSPayPersonDB.executeQuery(
	                tSBql.toString());

	        // 获取最新合同的余额
	        // 如果无预交记录
	        double tDif = 0.0;  //本次核销产生的余额

	        //多抵扣的账户余额
	        for(int t = 1; t <= tyetLJSPayPersonSet.size(); t++)
	        {
	            tDif += tyetLJSPayPersonSet.get(1).getSumActuPayMoney();
	        }

	        //多交的保费
	        double tempFeeTotalMoney = 0.0;
	        for(int i = 1; i <= mLJTempFeeSet.size(); i++)
	        {
	            // 此保单交费未核销的金额总计
	            tempFeeTotalMoney += mLJTempFeeSet.get(i).getPayMoney();
	        }
	        if(tempFeeTotalMoney > mLJSPayBL.getSumDuePayMoney())
	        {
	            tDif += (tempFeeTotalMoney - mLJSPayBL.getSumDuePayMoney());
	        }

	        // 如果有余额产生，而且交费方式为转账的核销操作
	        if(tDif>=-0.0001 && tDif<=0.0001)
	        {
	            tDif = 0;
	        }
	        else
	        {
	            addAccBala(tDif);
	        }

	        // 查询在应交日到核销日期间内，是否做过减保操作
	        tSBql = new StringBuffer(128);
	        tSBql.append("select 1 from LPEdorItem where EdorType='PT' and EdorState='0' and EdorAppDate<='");
	        tSBql.append(CurrentDate);
	        tSBql.append("' and EdorAppDate>='");
	        tSBql.append(mLJSPayBL.getStartPayDate());
	        tSBql.append("'");

	        SSRS tSSRS = tExeSQL.execSQL(tSBql.toString());
	        // 如果有做过减保操作，则需要对转账金额进行退费
	        if (tSSRS.getMaxRow() > 0) {
	            tSBql = new StringBuffer(128);
	            tSBql.append("select Sum(SumDuePayMoney) from LJSPayPerson where PayType = 'ZC' and GetNoticeNo = '");
	            tSBql.append(mLJSPayBL.getGetNoticeNo());
	            tSBql.append("'");
	            // 获取当期实际要交纳金额
	            double tActPayMoney = Double.parseDouble(tExeSQL.getOneValue(
	                    tSBql.toString()));

	            tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
	            String tNo = PubFun1.CreateMaxNo("PAYNOTICENO", tLimit);

	            LJTempFeeBL tLJTempFeeBL = new LJTempFeeBL();
	            tLJTempFeeBL.setSchema(mLJTempFeeSet.get(1).getSchema());
	            tLJTempFeeBL.setTempFeeNo(tNo);
	            tLJTempFeeBL.setOtherNo(mLJTempFeeSet.get(1).getTempFeeNo()); // 其他号码为原先交费的通知书号
	            tLJTempFeeBL.setOtherNoType("X"); // 特殊的类型
	            tLJTempFeeBL.setTempFeeType("X"); // 特殊的续期退费类型
	            tLJTempFeeBL.setConfFlag("0"); // 设置未核销状态
	            tLJTempFeeBL.setPayMoney(mLJTempFeeSet.get(1).getPayMoney() -
	                                     tActPayMoney);
	            tLJTempFeeBL.setConfDate(CurrentDate);
	            tLJTempFeeBL.setModifyDate(CurrentDate);
	            tLJTempFeeBL.setModifyTime(CurrentTime);
	            mLJTempFeeNewInsSet.add(tLJTempFeeBL);

	            LJTempFeeClassBL tLJTempFeeClassBL = new LJTempFeeClassBL();
	            tLJTempFeeClassBL.setSchema(mLJTempFeeClassSet.get(1).getSchema());
	            tLJTempFeeClassBL.setTempFeeNo(tNo);
	            tLJTempFeeClassBL.setPayMoney(mLJTempFeeSet.get(1).getPayMoney() -
	                                          tActPayMoney);
	            tLJTempFeeClassBL.setConfFlag("0"); // 设置未核销状态
	            tLJTempFeeClassBL.setConfDate(CurrentDate);
	            tLJTempFeeClassBL.setModifyDate(CurrentDate);
	            tLJTempFeeClassBL.setModifyTime(CurrentTime);
	            mLJTempFeeClassNewInsSet.add(tLJTempFeeClassBL);

	            // 原则上这里的金额应该为0
	            tDif = mLCContSchema.getDif();
	        }

	        // 在这里处理合同级的PayToDate
	        // 如果是税优续保,不修改paytodate
			String sysql = "select 1 from LCPol where "
							+"contNo = (select max(newContNo) from LCRnewStateLog  "
							+ "where contNo = '"+ContNo+"' "
							+ "and state <> '6' )";
			System.out.println(sysql);
			ExeSQL tEexSQL = new ExeSQL();
			SSRS ttSSRS = tEexSQL.execSQL(sysql);

			if (null == ttSSRS || "".equals(ttSSRS)) {
				mLCContSchema.setPaytoDate(polPayToDate);
			}
	        mLCContSchema.setPrem(ContactuMoney);
	        mLCContSchema.setSumPrem(mLCContSchema.getSumPrem() + ContactuMoney);
	        mLCContSchema.setModifyDate(CurrentDate); // 最后一次修改日期
	        mLCContSchema.setModifyTime(CurrentTime); // 最后一次修改时间
	        // 获取交费分类表的信息，准备核销动作。

	        // 核销暂收表中的交费记录
	        if (mLJTempFeeSet != null && mLJTempFeeSet.size() > 0) {
	            for (int t = 1; t <= mLJTempFeeSet.size(); t++) {
	                LJTempFeeBL tLJTempFeeBL = new LJTempFeeBL();
	                tLJTempFeeBL.setSchema(mLJTempFeeSet.get(t).getSchema());
	                tLJTempFeeBL.setConfFlag("1");
	                tLJTempFeeBL.setConfDate(CurrentDate);
	                tLJTempFeeBL.setModifyDate(CurrentDate);
	                tLJTempFeeBL.setModifyTime(CurrentTime);
	                mLJTempFeeNewSet.add(tLJTempFeeBL);
	            }
	        }
	        if (mLJTempFeeClassSet != null && mLJTempFeeClassSet.size() > 0) {
	            for (int t = 1; t <= mLJTempFeeClassSet.size(); t++) {
	                LJTempFeeClassBL tLJTempFeeClassBL = new LJTempFeeClassBL();
	                tLJTempFeeClassBL.setSchema(mLJTempFeeClassSet.get(t).getSchema());
	                tLJTempFeeClassBL.setConfFlag("1");
	                tLJTempFeeClassBL.setConfDate(CurrentDate);
	                tLJTempFeeClassBL.setModifyDate(CurrentDate);
	                tLJTempFeeClassBL.setModifyTime(CurrentTime);
	                mLJTempFeeClassNewSet.add(tLJTempFeeClassBL);

	            }
	        }
	        mLJAPayBL.setPayNo(payNo); // 交费收据号码
	        mLJAPayBL.setIncomeNo(mLJSPayBL.getOtherNo()); // 应收/实收编号
	        mLJAPayBL.setIncomeType(mLJSPayBL.getOtherNoType()); // 应收/实收编号类型
	        mLJAPayBL.setAppntNo(mLJSPayBL.getAppntNo()); // 投保人客户号码
	        mLJAPayBL.setSumActuPayMoney(ContactuMoney); // 本期总实交金额
	        mLJAPayBL.setStartPayDate(mLJSPayBL.getStartPayDate());
	        if (actuVerifyFlag) {
	            mLJAPayBL.setEnterAccDate(mEnterAccDate); // 到帐日期
	            mLJAPayBL.setConfDate(mConfDate); // 确认日期
	        } else {
	            // 如果不需要自动核销
	            mLJAPayBL.setEnterAccDate(mEnterAccDate); // 到帐日期
	            mLJAPayBL.setConfDate(mConfDate); // 确认日期
	        }
	        mLJAPayBL.setGetNoticeNo(mLJSPayBL.getGetNoticeNo()); // 交费通知书号码
	        mLJAPayBL.setPayDate(mPayDate); // 交费日期
	        mLJAPayBL.setApproveCode(mLJSPayBL.getApproveCode()); // 复核人编码
	        mLJAPayBL.setApproveDate(mLJSPayBL.getApproveDate()); // 复核日期
	        mLJAPayBL.setSerialNo(serNo); // 流水号
	        // mLJAPayBL.setOperator(mLJSPayBL.getOperator()); // 操作员
	        mLJAPayBL.setOperator(mGI.Operator); // 实际操作员
	        mLJAPayBL.setMakeDate(CurrentDate); // 入机时间
	        mLJAPayBL.setMakeTime(CurrentTime); // 入机时间
	        mLJAPayBL.setModifyDate(CurrentDate); // 最后一次修改日期
	        mLJAPayBL.setModifyTime(CurrentTime); // 最后一次修改时间
	        mLJAPayBL.setBankCode(mLJSPayBL.getBankCode()); // 银行编码
	        mLJAPayBL.setBankAccNo(mLJSPayBL.getBankAccNo()); // 银行帐号
	        mLJAPayBL.setAccName(mLJSPayBL.getAccName()); // 账户名
	        mLJAPayBL.setRiskCode(mLJSPayBL.getRiskCode()); // 险种编码
	        mLJAPayBL.setManageCom(mLJSPayBL.getManageCom()); // 管理机构
	        mLJAPayBL.setAgentCode(mLJSPayBL.getAgentCode()); // 代理人
	        mLJAPayBL.setAgentGroup(mLJSPayBL.getAgentGroup()); // 营业组
	        mLJAPayBL.setDueFeeType("1"); // 续期，续保收费 add by fuxin 2008-5-28/财务新接口
            //20110523添加LJAPay.Agentcom的值，使其和LJSPay.Agentcom保持一致即可
	        mLJAPayBL.setAgentCom(mLJSPayBL.getAgentCom()); 
	        mLJAPayBL.setSaleChnl(mLJSPayBL.getSaleChnl());
	        mLJAPayBL.setMarketType(mLJSPayBL.getMarketType());

	        tSBql = new StringBuffer(128);
	        tSBql.append(
	                "UPDATE  LJSPayB SET  DealState ='1' , ConfFlag='1'")
	                .append(" , ModifyDate ='")
	                .append(CurrentDate)
	                .append("' , ModifyTime ='")
	                .append(CurrentTime)
	                .append("'")
	                .append(" where GetNoticeNo='")
	                .append(mLJSPayBL.getGetNoticeNo())
	                .append("'")
	                ;
	        mLJSPayBSql = tSBql.toString();
	        System.out.println(" 续期核销时，更新LJSPayB");

	        tSBql = new StringBuffer(128);
	        tSBql.append(
	                "UPDATE  LJSPayPersonB SET  DealState ='1' , ConfFlag='1'")
	                .append(" , ModifyDate ='")
	                .append(CurrentDate)
	                .append("' , ModifyTime ='")
	                .append(CurrentTime)
	                .append("'")
	                .append(" where GetNoticeNo='")
	                .append(mLJSPayBL.getGetNoticeNo())
	                .append("'")
	                ;
	        mLJSPayPerBSql = tSBql.toString();
	        System.out.println(" 续期核销时，更新LJSPayPersonB");

	        return true;

	    }

	    /**
	     * 更改抵扣余额的状态为有效
	     * @return boolean：成功true，否则false
	     */
	    private MMap dealAccState()
	    {
	        LCAppAccDB tLCAppAccDB = new LCAppAccDB();
	        tLCAppAccDB.setCustomerNo(mLCContSchema.getAppntNo());
	        if(!tLCAppAccDB.getInfo())
	        {
	            return null;
	        }

	        LCAppAccTraceDB tOldLCAppAccTraceDB = new LCAppAccTraceDB();
	        tOldLCAppAccTraceDB.setCustomerNo(mLCContSchema.getAppntNo());
	        tOldLCAppAccTraceDB.setOtherNo(mLCContSchema.getContNo());
	        tOldLCAppAccTraceDB.setOtherType("2");
	        tOldLCAppAccTraceDB.setAccType("0");
	        tOldLCAppAccTraceDB.setDestSource("02");
	        tOldLCAppAccTraceDB.setBakNo(mLJSPaySchema.getGetNoticeNo());
	        LCAppAccTraceSet tOldLCAppAccTraceSet = tOldLCAppAccTraceDB.query();

	        //若没有续期
	        if(tOldLCAppAccTraceSet.size() == 0)
	        {
	            return null;
	        }

	        LCAppAccTraceSchema tOldLCAppAccTraceSchema
	            = tOldLCAppAccTraceSet.get(1).getSchema();

	        //抵扣账户余额
	        tLCAppAccDB.setAccBala(tLCAppAccDB.getAccBala()
	            + tOldLCAppAccTraceSchema.getMoney());  //续期抵扣金额为负
	        tLCAppAccDB.setOperator(this.mGI.Operator);
	        tLCAppAccDB.setModifyDate(this.CurrentDate);
	        tLCAppAccDB.setModifyTime(this.CurrentTime);

	        //余额使用轨迹生效
	        tOldLCAppAccTraceSchema.setState("1");
	        tOldLCAppAccTraceSchema.setAccBala(tOldLCAppAccTraceSchema.getAccBala()
	            + tOldLCAppAccTraceSchema.getMoney());  //续期抵扣金额为负
	        tOldLCAppAccTraceSchema.setOperator(this.mGI.Operator);
	        tOldLCAppAccTraceSchema.setModifyDate(this.CurrentDate);
	        tOldLCAppAccTraceSchema.setModifyTime(this.CurrentTime);

	        MMap tMap = new MMap();
	        tMap.put(tLCAppAccDB.getSchema(), SysConst.UPDATE);
	        tMap.put(tOldLCAppAccTraceSchema, SysConst.UPDATE);

	        return tMap;
	    }

	    /**
	     * 往帐户里增钱
	     * @param accBala double
	     * @return boolean
	     */
	    private boolean addAccBala(double accBala)
	    {
	        AppAcc tAppAcc = new AppAcc();

	        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
	        tLCAppAccTraceSchema.setCustomerNo(mLCContSchema.getAppntNo());
	        tLCAppAccTraceSchema.setAccType("1");
	        tLCAppAccTraceSchema.setOtherNo(mLCContSchema.getContNo());
	        tLCAppAccTraceSchema.setBakNo(mLJSPayBL.getGetNoticeNo());
	        tLCAppAccTraceSchema.setOtherType("2");
	        tLCAppAccTraceSchema.setMoney(accBala);
	        tLCAppAccTraceSchema.setOperator(mGI.Operator);
	        MMap tMap = tAppAcc.accShiftToXSYIJ(tLCAppAccTraceSchema);
	        if(tMap == null)
	        {
	            mErrors.copyAllErrors(tAppAcc.mErrors);
	            return false;
	        }
	        map.add(tMap);

	        return true;
	    }

	    /**
	     * 从输入数据中得到所有对象
	     * @param mInputData：包括
	     * 1、LCContSchema：保单信息
	     * 2、GlobalInput：操作员信息
	     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	     */
	    private boolean getInputData(VData mInputData) {

	        mGI = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
	        mTransferData = (TransferData) mInputData
	                        .getObjectByObjectName("TransferData", 0);
	        mGetNoticeNo =(String) mTransferData.getValueByName("GetNoticeNo");
	        
	        LCContSchema tLCContSchema = new LCContSchema();
	        tLCContSchema = (LCContSchema) mInputData
	                        .getObjectByObjectName("LCContSchema", 0);
	        if (tLCContSchema == null) {
	            CError.buildErr(this, "页面数据传入失败!");
	            return false;
	        }
	        
	        // 传入保单号或者暂交费号
	        // 说明:允许多次预交续期保费,直接根据合同号做核销动作
	        //if (mLJTempFeeBL.getOtherNo() != null) {
	        // 保单合同号
	        // 0-查询保单表
	        String tContNo = tLCContSchema.getContNo();
	        LCContDB tLCContDB = new LCContDB();
	        tLCContDB.setContNo(tContNo);
	        // 校验合同表，如果查询失败，则保单已经解约，或者其他原因，所以此保单不能交费
	        if (!tLCContDB.getInfo()) {
	            // @@错误处理
	            this.mErrors.copyAllErrors(tLCContDB.mErrors);
	            CError tError = new CError();
	            tError.moduleName = "IndiFinUrgeVerifyBL";
	            tError.functionName = "dealData";
	            tError.errorMessage = "保单信息不存在，请查询!";
	            this.mErrors.addOneError(tError);
	            return false;
	        }
	        mLCContSchema.setSchema(tLCContDB.getSchema());

	        double sumDueMoney = 0.0;
	        // 不能仅仅根据合同号＆类型进行查询，还需要对应收的顺序做排序，以便核销第一笔数据
	        // 同时需要添加交至日的条件，应为如果未到交至日，则需要将交费放入余额
	        StringBuffer tSBql = new StringBuffer(128);
	        tSBql.append(
	                "select * from LJSPay where OtherNoType = '2' and GetNoticeNO = '");
	        tSBql.append(mGetNoticeNo);
	        tSBql.append("'");	       
	        LJSPayDB tLJSPayDB = new LJSPayDB();
	        LJSPaySet tLJSPaySet = tLJSPayDB.executeQuery(tSBql.toString());
	        // 如果发现没有应收记录，有两种情况产生：
	        // 1。没有产生应收
	        // 2。产生了应收，但是没有到核销日
	        // 无论是上面的那种情况产生，都需要进行余额更新、应收记录更新（若有），不进行核销操作，也不允许打印发票等信息
	        if (tLJSPaySet == null || tLJSPaySet.size() <= 0) {
	            // 只有现金交费才会产生这种情况
	            // 表示此时不需要进行核销处理
	            mUrgeFlag = false;
	            CError.buildErr(this, "系统不存在满足条件的应收记录!");
	            return false;

	        }
	        // 如果查询到应收数据，则取出本期应交保费总金额
	        sumDueMoney = tLJSPaySet.get(1).getSumDuePayMoney();
	        // 获得最早一笔需要核销的应收记录
	        mLJSPayBL.setSchema(tLJSPaySet.get(1).getSchema());
	        double tempMoney = 0.0; // 交费的金额
	        boolean tNeedPay = true; // 是否需要交费
	        // 如果应交总额为０，则不需要有暂交费信息（即不需要预交续期保费），且存在应收记录
	        if (sumDueMoney <= 0.0 && mUrgeFlag) {
	            // return true;
	            // 设置交费标志为可不交费
	            tNeedPay = false;
	        }

	        // 查询此保单下的所有未核销的暂收交费记录
	        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
	        tSBql = new StringBuffer(128);
	        tSBql.append(
	                "select * from ljtempfee where TempFeeType='2' and TempFeeNo='");
	        tSBql.append(mLJSPayBL.getGetNoticeNo());
	        tSBql.append(
	                "' and (ConfFlag='0' or ConfFlag is null) and EnterAccDate is not null");	       
	        mLJTempFeeSet = tLJTempFeeDB.executeQuery(tSBql.toString());
	        if (mLJTempFeeSet == null || mLJTempFeeSet.size() == 0) {
	            // 只有当需要交费，且没有交费记录的时候才报错
	            if (tNeedPay) {
	                CError.buildErr(this, "系统不存在满足条件的预交费记录!");
	                return false;
	            }

	            //需要财务缴费，将所需的财务日期设为核销当天
	            mPayDate = CurrentDate;
	            mEnterAccDate = CurrentDate;
	        } else {
	            // 查询此保单下的素有未核销的暂收交费记录
	            LJTempFeeClassDB tLJTempFeeClassDB = new LJTempFeeClassDB();
	            tSBql = new StringBuffer(128);
	            tSBql.append("select * from ljtempfeeclass where TempFeeNo in (select TempFeeNo from ljtempfee where TempFeeType='2' and TempFeeNo='");
	            tSBql.append(mLJSPayBL.getGetNoticeNo());
	            tSBql.append(
	                    "' and (ConfFlag='0' or ConfFlag is null) and EnterAccDate is not null)");
	            mLJTempFeeClassSet = tLJTempFeeClassDB.executeQuery(tSBql.
	                    toString());
	            if (mLJTempFeeClassSet == null) {
	                CError.buildErr(this, "系统不存在满足条件的预交费记录!");
	                return false;
	            }
	            // 判定交费方式
	            if (mLJTempFeeClassSet.get(1).getPayMode().equals("4")) {
	                // 设置交费方式为转账模式
	                mPayMode = false;
	            }
	            //取得缴费日期
	            mPayDate = mLJTempFeeSet.get(1).getPayDate();
	            mEnterAccDate = mLJTempFeeSet.get(1).getEnterAccDate();
	            tempMoney = 0.0;
	            for (int i = 1; i <= mLJTempFeeSet.size(); i++) {
	                // 该保单下实际交费金额
	                tempMoney += mLJTempFeeSet.get(i).getPayMoney();

	            }
	        }
	        mConfDate = CurrentDate;

	        // 如果实交金额不足以核销本次收费，则返回错误信息
	        // 产生这种情况的时候只可能是现金交费，此时需要将交费金额放入保单帐户余额中
	        // 已经确认，当产生应收的时候，现金交费必须足够核销一期应收金额
	        if (tempMoney < sumDueMoney) {
	            mUrgeFlag = false;
	            return true;
	        }

	        //  }

	        String sql = "  select edorNo "
	                     + "from LPEdorApp a,LPEdorItem b "
	                     + "where a.edorAcceptNo = b.edorNo "
	                     + "   and a.edorState != '" + BQ.EDORSTATE_CONFIRM + "' "
	                     + "   and b.contNo = '" + mLCContSchema.getContNo() + "' ";
	        
	        SSRS tSSRS = new ExeSQL().execSQL(sql);
	        String edorNos = "";
	        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
	        {
	            edorNos += tSSRS.GetText(i, 1);
	        }
	        if(!edorNos.equals(""))
	        {
	            mErrors.addOneError("保单" + mLCContSchema.getContNo()
	                                + "有未完成的保全受理，不能进行核销操作：" + edorNos);
	            return false;
	        }

	        return true;
	    }

	    //准备往后层输出所需要的数据
	    //输出：如果准备数据时发生错误则返回false,否则返回true
	    private boolean prepareOutputData() {
	        mInputData = new VData();
	        try {
	           
	            mLJAPaySchema.setSchema(mLJAPayBL);
	            mLJSPaySchema.setSchema(mLJSPayBL);
	            mLJTempFeeSchema.setSchema(mLJTempFeeBL);
	            map.put(mLJAPaySchema, "INSERT");
	            map.put(mLJSPaySchema, "DELETE");
	            map.put(mLJTempFeeNewSet, "UPDATE");
	            map.put(mLJTempFeeClassNewSet, "UPDATE");
	            map.put(mLJAPayPersonSet, "INSERT");
	            map.put(mLJSPayPersonSet, "DELETE");
	           	map.put(mLCContSchema, "UPDATE");
	            map.put(mLCPolSet, "UPDATE");
	            map.put(mLCPremNewSet, "UPDATE");
	            map.put(mLCDutyNewSet, "UPDATE");
	            map.put(mLJSPayBSql, "UPDATE");
	            map.put(mLJSPayPerBSql, "UPDATE");
	            
	            map.put(mLCInsureAccSchema,"UPDATE");
	            map.put(mLCInsureAccTraceSet,"INSERT");	 
	            map.put(mLCInsureAccFeeTraceSchema, "INSERT");
	            map.put(mLCInsureAccFeeSchema, "UPDATE");
	        	map.put(mLCInsureAccClassSet, "DELETE&INSERT");
	        	map.put(mLCInsureAccClassFeeSet, "UPDATE");
	        	map.add(pMap);
	            MMap tMMap = dealAccState();
	            map.add(tMMap); 
	          
				
	            mInputData.add(map);

	        } catch (Exception ex) {
	            // @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "IndiFinUrgeVerifyBL";
	            tError.functionName = "prepareData";
	            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
	            this.mErrors.addOneError(tError);
	            return false;
	        }

	        return true;
	    }
	    private boolean setInsuredAcc(){
	    	    	// 续期所收保费按实际到帐日（保费核销日）计算利息，核销后，根据合同约定扣除初始费用后，记入保单账户价值。
	    	    	String tContNo =  mLCContSchema.getContNo();
	    	    	String ManageCom = mGI.ManageCom ;
	    	    	int payTime=0;
	    	    	double currentTimeFee=0.0;
	    	    	double sumFee=0.0; // 累计扣费
	    	    	double tSumMoney=0.0; // 余额
	    	    	// 续期险种查询
	    	    	ExeSQL tExeSQL = new ExeSQL();
	    	    	
	    	    	LJSPayPersonSet tLJSPayPersonSet=new LJSPayPersonSet();
	    	     	LJSPayPersonDB tLJSPayPersonDB=new LJSPayPersonDB(); 
   	     	
	    	     	StringBuffer cSBql = new StringBuffer(128);
	    	     	cSBql.append("select * from LJSPayPerson where contno='");
	    	     	cSBql.append(tContNo);
	    	     	cSBql.append("' and GetNoticeNo='");
	    	     	cSBql.append(mGetNoticeNo);
	    	     	cSBql.append("' and SumDuepaymoney>0  and exists (select 1 from lmriskapp where  risktype4='4' and riskcode  = LJSPayPerson.riskcode ) with ur");
	    	     	//modify by fuxin 2015-2-5 处理当万能险和传统险同时承保但是取不到帐户，所以增加只取万能险的条件。
	    	     	LJSPayPersonSchema tLJSPayPersonSchema =new LJSPayPersonSchema();
	    	     	tLJSPayPersonSet = tLJSPayPersonDB.executeQuery(cSBql.toString());
	    	        tLJSPayPersonSchema = tLJSPayPersonSet.get(1);
	    	    	
    	     		
	    	        String mainPol="";
	 	     	    mainPol=tLJSPayPersonSchema.getPolNo();
	 	     	   
     	   
	    	     	LCPolDB tLCPolDB=new LCPolDB();
	    	     	tLCPolDB.setPolNo(mainPol);
	    	     	LCPolSet tLCPolSet = new LCPolSet();
	    	     	tLCPolSet =tLCPolDB.query();
	    	     	mLCPolSchema =tLCPolSet.get(1);
	    	     	//TODO
	    	     	 LCPremSet tLCPremSet= mLCPremSet;
	    	     	 LJAPayPersonSet tLJAPayPersonSet = mLJAPayPersonSet;
	    	     	 
	    	     	 LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
	    	 		 LCInsureAccDB tLCInsureAccDB =new LCInsureAccDB();
	    	 		 tLCInsureAccDB.setContNo(tContNo);
	    	 		 tLCInsureAccDB.setPolNo(mainPol);
	    	 		 LCInsureAccSet tLCInsureAccSet=new LCInsureAccSet();
	    	 		 tLCInsureAccSet =tLCInsureAccDB.query(); 
    	 		     tLCInsureAccSchema= tLCInsureAccSet.get(1);	    	 		
	    	 		 payTime =tLJSPayPersonSchema.getPayCount() ;
	    	     	 currentTimeFee = tLJSPayPersonSchema.getSumDuePayMoney(); 
	    	     		// 得到初始扣费
	    	     	double tFee=0.0; 
	    	     	//税优风险保费
	    	     	double rpFee=0.0;
	    	     	String tRiskCode = tLJSPayPersonSchema.getRiskCode();
	    	     	String sql = "select count(1) from ldcode where codetype = 'ulifee' and code = '" + tRiskCode + "' with ur";
	    	     	String tCount = tExeSQL.getOneValue(sql);
	    	     	
	    	     	double tAmnt = CommonBL.getTBAmnt(mLCPolSchema);
	    	     	
	    	     	if(!"0".equals(tCount))
	    	     	{
//		    	     	String sqlhunan = "select count(1) from lpedorespecialdata where edorvalue='1' and edortype = 'WN' and detailtype = 'ULIFEE' and edorno = '" + tLJSPayPersonSchema.getContNo() + "' with ur";
//		    	     	String tCounthunan = tExeSQL.getOneValue(sqlhunan);
//		    	     	if((!"0".equals(tCounthunan))&&"330801".equals(tLJSPayPersonSchema.getRiskCode()))
//		    	     	{
//		    	     		tFee=getFee(tLJSPayPersonSchema ,currentTimeFee,tLCInsureAccSchema.getInsuAccNo(),tRiskCode);
//		    	     	}
//		    	     	else{
		    	     		tFee=getULIFee(tLJSPayPersonSchema ,tLCInsureAccSchema.getInsuAccNo(),tAmnt);
//		    	     	}	    	     		
	    	     	}
	    	     	else
	    	     	{
//	    	     		直接在ldcode中描述,注释掉这部分硬编码
//	    	     		if(tRiskCode.equals("331701"))
//	    	     		{
//	    	     			tFee=getULIFee331701(tLJSPayPersonSchema ,tLCInsureAccSchema.getInsuAccNo(),tAmnt);
//	    	     		}
//	    	     		else
//	    	     		{
	    	     			tFee=getFee(tLJSPayPersonSchema ,currentTimeFee,tLCInsureAccSchema.getInsuAccNo(),tRiskCode);
	    	     			if(syFlag(tRiskCode)){
	    	     				rpFee=getRPFee(tLJSPayPersonSchema ,currentTimeFee,tLCInsureAccSchema.getInsuAccNo(),tRiskCode);
	    	     			}
//	    	     		}
	    	     	}
	    	     	
	    	     	if(tFee == -1)
    	     		{
    	     			CError tError = new CError();
	    	    		tError.moduleName = "OmnipFinUrgeVerifyBL";
	    	    		tError.functionName = "setInsuredAcc";
	    	    		tError.errorMessage = "计算初始费用失败!";
	    	    		mErrors.addOneError(tError);
	    	    		return false;
    	     		}
	    	     	
	    	     	System.out.println("得到扣费***********"+tFee);
//	    	     	double ctFee = Double.parseDouble("-"+tFee);//modify by lzy 不知道为何这样写，容易出错，现在改一下
	    	     	double ctFee = -Math.abs(tFee);
	    	     	double tempMoney =tLJSPayPersonSchema.getSumDuePayMoney()+ctFee;	    	     	
	    	     	tSumMoney =tLCInsureAccSchema.getInsuAccBala()+tempMoney;

	    	     	if(rpFee>0){
	    	     		tSumMoney-=rpFee;
	    	    		tempMoney-=rpFee;
	    	     	}
	    	     		// 处理持续奖励
	    	    	double tJL=getJL(tLJSPayPersonSchema);
	    	    	if(tJL==-2){
	    	    		CError tError = new CError();
	    	    		tError.moduleName = "PEdorZBAppConfirmBL";
	    	    		tError.functionName = "setInsuredAcc";
	    	    		tError.errorMessage = "计算持续奖励失败!";
	    	    		mErrors.addOneError(tError);
	    	    		return false;
	    	    		
	    	    	}
	    	    	if(tJL>0){
	    	    		tSumMoney+=tJL;
	    	    		tempMoney+=tJL;
	    	    	}
	    	    	System.out.println("持续奖金是*******tJL = "+tJL);
	    	    	tLCInsureAccSchema.setInsuAccBala(tSumMoney);
	    	    	tLCInsureAccSchema.setModifyDate(aMakeDate);
	    	    	tLCInsureAccSchema.setModifyTime(aMakeTime);
//	    	    	tLCInsureAccSchema.setSumPay(tLCInsureAccSchema.getSumPay()+currentTimeFee);
//	    	    	tLCInsureAccSchema.
//	    	    	tLCInsureAccSchema.setInsuAccGetMoney(tLCInsureAccSchema.getInsuAccGetMoney()+tempMoney);
	    	    	
	    	    	mLCInsureAccSchema = tLCInsureAccSchema;
	    	    	 // 处理帐户分类表
	    	    	
	    	    		
	    	    	LCInsureAccClassSet tLCInsureAccClassSet = new LCInsureAccClassSet();
	    	    	LCInsureAccClassDB tLCInsureAccClassDB = new LCInsureAccClassDB();
	    	    	tLCInsureAccClassDB.setPolNo(mLCPolSchema.getPolNo());
	    	    	tLCInsureAccClassDB.setPayPlanCode(tLJSPayPersonSchema.getPayPlanCode());
	    	    	tLCInsureAccClassDB.setContNo(tContNo);
	  	    	    tLCInsureAccClassDB.setInsuAccNo(tLCInsureAccSchema.getInsuAccNo()); 
	  	    	    tLCInsureAccClassSet=tLCInsureAccClassDB.query();
	  	    	    if(tLCInsureAccClassSet.size()<1)
		    	    {
		    	      CError tError = new CError();
		    	      tError.moduleName = "PEdorZBAppConfirmBL";
		    	     		tError.functionName = "setInsuredAcc";
		    	     		tError.errorMessage = "查询帐户分类表数据失败!";
		    	     		mErrors.addOneError(tError);
		    	     		return false;
		    	     			
		    	        }  
	  	    	    LCInsureAccClassSchema cLCInsureAccClassSchema=tLCInsureAccClassSet.get(1);
	    	        this.setLCInsureAccTrace(cLCInsureAccClassSchema, "BF", currentTimeFee);
	    	        this.setLCInsureAccTrace(cLCInsureAccClassSchema, "GL", ctFee);
	    	        this.setLCInsureAccFeeTrace(cLCInsureAccClassSchema,"KF",tFee);// 扣费轨迹
	    	        if(tJL>0){
	    	          this.setLCInsureAccTrace(cLCInsureAccClassSchema,  "B",tJL);// 持续奖励轨迹(acctrace)	    	         
	    	        }  
	    	        if(syFlag(tRiskCode) && rpFee>0){
	    	        	this.setLCInsureAccTrace(cLCInsureAccClassSchema,  "RP",-rpFee);
	    	        	this.setLCInsureAccFeeTrace(cLCInsureAccClassSchema,"RP",rpFee);// 扣费轨迹
	    	        }
	    	           // 处理管理费帐户主表
	    	        LCInsureAccFeeSchema tLCInsureAccFeeSchema = new LCInsureAccFeeSchema();
	    	        LCInsureAccFeeSet tLCInsureAccFeeSet = new LCInsureAccFeeSet();
	    	        LCInsureAccFeeDB tLCInsureAccFeeDB = new LCInsureAccFeeDB();
	    	        tLCInsureAccFeeDB.setInsuAccNo(tLCInsureAccSchema.getInsuAccNo());
	    	        tLCInsureAccFeeDB.setPolNo(mLCPolSchema.getPolNo());
	    	        
	    	        tLCInsureAccFeeSet= tLCInsureAccFeeDB.query();
	    	        if(tLCInsureAccFeeSet.size()<1)
	    	        {
	    	       		CError tError = new CError();
	    	     		tError.moduleName = "PEdorZBAppConfirmBL";
	    	     		tError.functionName = "setInsuredAcc";
	    	     		tError.errorMessage = "查询帐户管理费分类表数据失败!";
	    	     		mErrors.addOneError(tError);
	    	     		return false;
	    	     	 }
	    	       tLCInsureAccFeeSchema=tLCInsureAccFeeSet.get(1);      
	    	       ref.transFields(mLCInsureAccFeeSchema, tLCInsureAccFeeSchema);
	    	       //20160316初始扣费+风险保费-->税优
	    	       mLCInsureAccFeeSchema.setFee(mLCInsureAccFeeSchema.getFee()+Math.abs(tFee)+Math.abs(rpFee));

	    	       mLCInsureAccFeeSchema.setModifyDate(aMakeDate);
	    	       mLCInsureAccFeeSchema.setModifyTime(aMakeTime);
	    	       // 处理管理费帐户分类表
	    	       LCInsureAccClassFeeSet tLCInsureAccClassFeeSet = new LCInsureAccClassFeeSet();
	    	       LCInsureAccClassFeeDB tLCInsureAccClassFeeDB = new LCInsureAccClassFeeDB();
	    	       tLCInsureAccClassFeeDB.setPolNo(mLCPolSchema.getPolNo());
	    	       //tLCInsureAccClassFeeDB.setAccType(BQ.ACCTYPE_INSURED);
	    	       tLCInsureAccClassFeeDB.setPayPlanCode(tLJSPayPersonSchema.getPayPlanCode());
	    	       tLCInsureAccClassFeeDB.setInsuAccNo(tLCInsureAccSchema.getInsuAccNo());
	    	       tLCInsureAccClassFeeSet=tLCInsureAccClassFeeDB.query();
	    	       if(tLCInsureAccClassFeeSet.size()<1)
	    	       {
	    	       		CError tError = new CError();
	    	     		tError.moduleName = "PEdorZBAppConfirmBL";
	    	     		tError.functionName = "setInsuredAcc";
	    	     		tError.errorMessage = "查询帐户分类表数据失败!";
	    	     		mErrors.addOneError(tError);
	    	     		return false;
	    	     			
	    	       }  
	    	       LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema= new LCInsureAccClassFeeSchema();
	    	       tLCInsureAccClassFeeSchema=tLCInsureAccClassFeeSet.get(1);
	    	       LCInsureAccClassFeeSchema tLCInsureAccClassFeeSchema2= new LCInsureAccClassFeeSchema();
	    	       ref.transFields(tLCInsureAccClassFeeSchema2, tLCInsureAccClassFeeSchema);
	    	       tLCInsureAccClassFeeSchema2.setModifyDate(aMakeDate);
	               tLCInsureAccClassFeeSchema2.setModifyTime(aMakeTime);
	               tLCInsureAccClassFeeSchema2.setFee(tLCInsureAccClassFeeSchema.getFee()+Math.abs(tFee)+Math.abs(rpFee));
	    	       mLCInsureAccClassFeeSet.add(tLCInsureAccClassFeeSchema2);
	    	       
	    	     	// 处理帐户分类表
	    	        LCInsureAccClassSet cLCInsureAccClassSet = new LCInsureAccClassSet();
	    	        LCInsureAccClassDB cLCInsureAccClassDB = new LCInsureAccClassDB();
	    	        cLCInsureAccClassDB.setPolNo(mLCPolSchema.getPolNo());
	    	        cLCInsureAccClassDB.setAccType("002");
	    	        cLCInsureAccClassSet=tLCInsureAccClassDB.query();
	    	        if(tLCInsureAccClassSet.size()<1)
	    	        {
	    	  			CError tError = new CError();
	    				tError.moduleName = "PEdorZBAppConfirmBL";
	    				tError.functionName = "setInsuredAcc";
	    				tError.errorMessage = "查询帐户分类表数据失败!";
	    				mErrors.addOneError(tError);
	    				return false;
	    	        } 
	    	        LCInsureAccClassSchema tLCInsureAccClassSchema= new LCInsureAccClassSchema();
	    	        tLCInsureAccClassSchema=tLCInsureAccClassSet.get(1);
	    	        ref.transFields(cLCInsureAccClassSchema, tLCInsureAccClassSchema);
	    	        cLCInsureAccClassSchema.setModifyDate(aMakeDate);
	    	        cLCInsureAccClassSchema.setModifyTime(aMakeTime);
	    	        cLCInsureAccClassSchema.setInsuAccBala(tSumMoney);
	    	        //cLCInsureAccClassSchema.setInsuAccGetMoney(tLCInsureAccSchema.getInsuAccGetMoney()+tempMoney);
                    mLCInsureAccClassSet.add(tLCInsureAccClassSchema);
	    	     
	    	        return true;
	    	 }

		private boolean dealPolState(){
			ExeSQL tExeSQL=new ExeSQL();
			String mContNo=mLCContSchema.getContNo();
			String mCvalidate=mLCContSchema.getCValiDate();
			String strPol="select 1 from LCPol where contno='"+mContNo+"' "
						+ " and (standbyflag1 is not null and standbyflag1='"+BQ.ULI_DEFER+"')";
			String polstate=tExeSQL.getOneValue(strPol);
			String strState="select * from LCContState where ContNo='"+mLCContSchema.getContNo()+"'"
						+ " and StateType='"+BQ.STATETYPE_ULIDEFER+"' "
						+ " and state='1' ";
			LCContStateDB tLCContStateDB=new LCContStateDB();
			LCContStateSchema tLCContStateSchema=null;
			LCContStateSet tLCContStateSet=tLCContStateDB.executeQuery(strState);
			if(tLCContStateSet.size()==0){
				if(null==polstate || "".equals(polstate)){
					System.out.println(mLCContSchema.getContNo()+"保单没有缓交记录");
					return true;
				}else{//保单为新缓交状态，但没有查到缓交记录
					CError.buildErr(this, "保单缓交记录查询失败！");
					return false;
				}
			}
			for(int i=1; i<=tLCContStateSet.size(); i++){
				tLCContStateSchema=tLCContStateSet.get(i);
				//核销时还需校验保单新的交至日是否仍满期缓交条件
				String strDate="select CurPayToDate from LJSPayPerson a where contno='"+mContNo+"'"
	    	     				+" and GetNoticeNo='"+mGetNoticeNo+"'"
	    	     				+" and polNo='"+tLCContStateSchema.getPolNo()+"'";
				String newPayToDate=tExeSQL.getOneValue(strDate);
	    	    if(null==newPayToDate || "".equals(newPayToDate)){
	    	    	System.out.println("没有查询到险种"+tLCContStateSchema.getPolNo()+"的缴费记录");
	    	    	continue;
	    	    }
	    	    
	    	    int polYear=PubFun.calInterval(mCvalidate, newPayToDate, "Y");//续期后的保单年度
	    	    String standByflag="null";
	    	    FDate tFDate = new FDate();
	    		String GraceDate=PubFun.calDate(newPayToDate, 60+1 , "D", "");
	        	if(tFDate.getDate(GraceDate).before(tFDate.getDate(CurrentDate))){
	        		if(polYear<5){
	        			System.out.println("险种新交至日仍需缓交");
		        		continue;
	        		}
	        		standByflag="1";//第五保单年度及以后只是缓交，对账户结算不影响
	        	}
				tLCContStateSchema.setState("0");
				tLCContStateSchema.setEndDate(CurrentDate);
				tLCContStateSchema.setModifyDate(CurrentDate);
				tLCContStateSchema.setModifyTime(CurrentTime);
				pMap.put(tLCContStateSchema, SysConst.DELETE_AND_INSERT);
				
				String update="update lcpol set standbyflag1="+standByflag+",modifydate=current date,modifytime=current time"
					+ " where contno='"+mContNo+"'";
				pMap.put(update, SysConst.UPDATE);
			}
				
			return true;
		}
	 
		private double getFee(LJSPayPersonSchema tempLJSPayPersonSchema,
				 double currentFee,String cInsuAccNo,String RiskCode) {
			double tFee;
			String tInsuAccNo = cInsuAccNo;

			// 查询管理费
			LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
			tLMRiskFeeDB.setInsuAccNo(tInsuAccNo);
			// tLMRiskFeeDB.setPayPlanCode(mPayPlanCode);
			tLMRiskFeeDB.setFeeKind("03"); // 03-个单管理费
			tLMRiskFeeDB.setFeeItemType("04");// 04-初始扣费
			tLMRiskFeeDB.setFeeTakePlace("01"); // 01－交费时		
			LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
			if (tLMRiskFeeDB.mErrors.needDealError()) {
				CError.buildErr(this, "账户管理费查询失败!");
				return -1;
			}
			int tPayCount=0;
			String tPayDate =tempLJSPayPersonSchema.getCurPayToDate();
			//TODO 
			tPayCount=PubFun.calInterval(mLCPolSchema.getCValiDate(),tPayDate,"M")/mLCPolSchema.getPayIntv();
			mFeeCode = tLMRiskFeeSet.get(1).getFeeCode();
			tFee = calRiskFee(tLMRiskFeeSet.get(1), currentFee, tPayCount);
			System.out.println("@@@@@@@@@@@ fee = "+tFee);
			return tFee;
		}
		
		/**税优处理风险保费**/
		private double getRPFee(LJSPayPersonSchema tempLJSPayPersonSchema,
				 double currentFee,String cInsuAccNo,String RiskCode) {
			double tFee = 0;
			String tInsuAccNo = cInsuAccNo;
			
			//税优--加风险保费的处理
			if(!syFlag(RiskCode)){
				return 0;
			}
			LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
			tLMRiskFeeDB.setInsuAccNo(tInsuAccNo);
			tLMRiskFeeDB.setFeeKind("03"); // 03-个单管理费
			tLMRiskFeeDB.setFeeItemType("02");// 04-初始扣费
			tLMRiskFeeDB.setFeeTakePlace("01"); // 01－交费时		
			LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
			
			if (tLMRiskFeeDB.mErrors.needDealError()) {
				CError.buildErr(this, "账户管理费查询失败!");
				return -1;
			}
			int tPayCount=0;
			String tPayDate =tempLJSPayPersonSchema.getCurPayToDate();
			if(isXBPol(tempLJSPayPersonSchema.getPolNo())){
				tPayCount=1;//续保的保单目前用不到该字段，先设置为1
			}else{
				tPayCount=PubFun.calInterval(mLCPolSchema.getCValiDate(),tPayDate,"M")/mLCPolSchema.getPayIntv();
			}
			mFeeCode = tLMRiskFeeSet.get(1).getFeeCode();
			tFee = calRiskFee(tLMRiskFeeSet.get(1), currentFee, tPayCount);
			
			System.out.println("@@@@@@@@@@@ fee = "+tFee);
			return tFee;
		}
		
		/**
		* ①  期缴保费新增首年、次年、第三年增费部分分别按照30%、20%、10%扣除初始扣费（目前根据业管办法，无保费大于6000元的情况）；
		* ②多次申请增加保费的，较上期保费新增的部分均首年、次年、第三年均分别按30%、20%、10%扣除初始扣费。
		* ③对每次增费部分系统需增加相关标记进行计算。
		* 例1：有一客户投保金利宝，选择6年缴费，期缴保费1000元，投保第一年，期交保费1000元首年初始扣费30%；投保第二年，客户申请将保费变更为2000元，则投保首年选择的期缴1000元保费按第二年初始扣费20%扣除，增费1000元部分按增费首年30%扣除；投保第三年，客户申请将保费变更为3000元，则投保首年选择的期缴保费1000元按第三年初始扣费10%扣除，第二年增费部分1000元按增费次年20%扣除，第三年增费部分1000元按增费首年30%扣除。
		* 例2：有一客户投保金利宝，选择6年缴费，期缴保费2000元，投保第一年，期交保费2000元首年初始扣费30%；投保第二年，客户申请将保费变更为1000元，则变更后保费按第二年初始扣费20%扣除；投保第三年，客户申请将保费变更为2000元，其中1000元按第三年初始扣费10%扣除，第三年增费部分1000元按增费首年30%扣除。
		* 例3：有一客户投保金利宝，选择6年缴费，首期期交保险费为3000元。第二年期交保险费变更为2000元，第三年期交保险费增加至6000元，第四年期交保险费减少至5000元。 
	    * 1、第一年初始费用，3000元*30%=900元； 
	    * 2、第二年初始费用，2000元*20%=400元； 
	    * 3、第三年初始费用，2000元*10%+4000元*30%=200元+1200元 =1400元（新增的4000元按首年的初始费用比例扣除） 
	    * 4、第四年初始费用，3000元*20%=600元。
        * 3、期缴保费调整最低不能低于1000元，高不能高于6000元，且变更后数额均应为千元整数倍。
		*/
		private double getULIFee331701(LJSPayPersonSchema tempLJSPayPersonSchema,String cInsuAccNo, double cAmnt) 
		{
			
			double fee = 0; //管理费
			
			double curPrem = tempLJSPayPersonSchema.getSumDuePayMoney(); //本期保费
			
			double basePrem = curPrem;//基本保费：期交保费
			
			
			
			CalInitialFee tCalInitialFee = new CalInitialFee(tempLJSPayPersonSchema,basePrem);
			Hashtable cur = tCalInitialFee.calTest();
			
			if(cur == null)
			{
				return -1;
			}
			
			for (int j = 1; j <= cur.size(); j++) 
			{
				String PremPeriod = (String)cur.get(String.valueOf(j));
				String[] sPremPeriod = new String[2];
				sPremPeriod = PremPeriod.split("@");
				
				//缴费金额
				double tCurPrem = Double.parseDouble(sPremPeriod[0]);
				
				//第几次缴费
				int tPayCount = Integer.parseInt(sPremPeriod[1]);
				
				System.out.println("curPrem == " + tCurPrem);
				System.out.println("curPeriod == " + tPayCount);
				
//				 查询管理费
				LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
				tLMRiskFeeDB.setInsuAccNo(cInsuAccNo);
				tLMRiskFeeDB.setFeeKind("03"); // 03-个单管理费
				tLMRiskFeeDB.setFeeItemType("04");// 04-初始扣费
				tLMRiskFeeDB.setFeeTakePlace("01"); // 01－交费时		
				LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
				if (tLMRiskFeeDB.mErrors.needDealError()) 
				{
					CError.buildErr(this, "账户管理费查询失败!");
					return -1;
				}
				
				double tempFee = calRiskFee(tLMRiskFeeSet.get(1), tCurPrem, tPayCount);
				System.out.println("缴费金额"+tCurPrem+"按照第 "+tPayCount+" 次计算管理费 tempFee = " + tempFee);
				fee += tempFee;
			}
			
			double minusPrem = 0; //基本保费之外的额外保费
			
			if (curPrem > basePrem) 
			{
				minusPrem = curPrem - basePrem;
			}
			
			double minusRate = 0.05; //额外保费管理费率固定为 5%
			fee += minusPrem * minusRate;
			System.out.println("额外保费为 "+ minusPrem + "，管理费率5%，收取管理费" + fee );

			System.out.println("总共收取续期管理费 = " + fee);
			return fee;
		}
		
		/**
		*健康宝A（万能型）期交保险费的减少应从最新增加的部分开始减；期交保险费的增加，增加部分都归属第一年度。根据你所举的例子具体初始费用的扣除如下：
		*首期期交保险费为3000元，基本保险金额为9万元。第二年期交保险费变更为2000元，第三年期交保险费增加至6000元，第四年期交保险费减少至5000元。
		*1、第一年初始费用，3000元*50%=1500元；
		*2、第二年初始费用，2000元*25%=500元；
		*3、第三年初始费用，2000元*15%+2500元*50%+1500元*5%=300元+1250元+75元=1625元
		*（新增的4000元按首年的初始费用比例扣除，同时按投保时的基本保险金额的1/20区分不同的比例）
		*4、第四年初始费用，2000元*10%+2500元*25%+500元*5%=200元+625元+25元=850元。
		*/
		private double getULIFee(LJSPayPersonSchema tempLJSPayPersonSchema,String cInsuAccNo, double cAmnt) 
		{
			
			double fee = 0; //管理费
			
			double curPrem = tempLJSPayPersonSchema.getSumDuePayMoney(); //本期保费
			
			double basePrem = 0;//基本保费：期交保费，保额的1/20和6000较小的一个
			
			if(cAmnt != 0.0 && (cAmnt/20)<6000)
			{
				basePrem = (cAmnt/20);
			}
			else
			{
				basePrem = 6000;
			}
			
			if(curPrem < basePrem)
			{
				basePrem = curPrem;
			}
			/* modify 20150608  福泽B款基本保费特别计算
			 * 投保时被保险人的年龄不满18，基本保费为 期缴保费、 5000 中的较小者
			 * 投保时年满18，基本保费：期缴保费、保额的1/20、10000中的较小者
			 */
			if("334701".equals(tempLJSPayPersonSchema.getRiskCode())){
				//得到投保年龄
				int appAge=PubFun.getInsuredAppAge(mLCPolSchema.getPolApplyDate(),mLCPolSchema.getInsuredBirthday());
				if(appAge < 0){
					CError.buildErr(this, "被保险人投保年龄计算失败!");
					return -1;
				}
				if(appAge >= 18 ){
					if(cAmnt != 0.0 && (cAmnt/20)<10000)
					{
						basePrem = (cAmnt/20);
					}
					else
					{
						basePrem = 10000;
					}
					
					if(curPrem < basePrem)
					{
						basePrem = curPrem;
					}
				}else{
					basePrem = 5000;
					if(curPrem < basePrem)
					{
						basePrem = curPrem;
					}
				}
				
			}
			
			CalInitialFee tCalInitialFee = new CalInitialFee(tempLJSPayPersonSchema,basePrem);
			Hashtable cur = tCalInitialFee.calTest();
			
			if(cur == null)
			{
				return -1;
			}
			
			for (int j = 1; j <= cur.size(); j++) 
			{
				String PremPeriod = (String)cur.get(String.valueOf(j));
				String[] sPremPeriod = new String[2];
				sPremPeriod = PremPeriod.split("@");
				
				//缴费金额
				double tCurPrem = Double.parseDouble(sPremPeriod[0]);
				
				//第几次缴费
				int tPayCount = Integer.parseInt(sPremPeriod[1]);
				
				System.out.println("curPrem == " + tCurPrem);
				System.out.println("curPeriod == " + tPayCount);
				
//				 查询管理费
				LMRiskFeeDB tLMRiskFeeDB = new LMRiskFeeDB();
				tLMRiskFeeDB.setInsuAccNo(cInsuAccNo);
				tLMRiskFeeDB.setFeeKind("03"); // 03-个单管理费
				tLMRiskFeeDB.setFeeItemType("04");// 04-初始扣费
				tLMRiskFeeDB.setFeeTakePlace("01"); // 01－交费时		
				LMRiskFeeSet tLMRiskFeeSet = tLMRiskFeeDB.query();
				if (tLMRiskFeeDB.mErrors.needDealError()) 
				{
					CError.buildErr(this, "账户管理费查询失败!");
					return -1;
				}
				
				double tempFee = calRiskFee(tLMRiskFeeSet.get(1), tCurPrem, tPayCount);
				System.out.println("缴费金额"+tCurPrem+"按照第 "+tPayCount+" 次计算管理费 tempFee = " + tempFee);
				fee += tempFee;
			}
			
/*			基本保费部分循环算费已经完成,下面是额外保费的循环算费部分
 *			对5期以内的保单,保持现有程序不动,对5期以上的保单的额外保费初始扣费,调整程序支持
 *			add by xp 20120329 
*/
//			if(tCalInitialFee.getMDuePayPeriod()<=5){
//				double minusPrem = 0; //基本保费之外的额外保费
//				
//				if (curPrem > basePrem) 
//				{
//					minusPrem = curPrem - basePrem;
//				}
//	
//				double minusRate = 0.05; //额外保费管理费率固定为 5%
//				
//				
//				//额外保费的费率在描述表中取，不是固定为5%了
//				String sqlFeeLv="select rate from rate"+tempLJSPayPersonSchema.getRiskCode()+" where type='1' and "+tCalInitialFee.getMDuePayPeriod()
//				               +"  >= mincount and "+tCalInitialFee.getMDuePayPeriod()+"<=maxcount  and payintv=(select payintv from lccont where contno ='"
//				               +tempLJSPayPersonSchema.getContNo()+"')  with ur";
//				String resultMinusRate = new ExeSQL().getOneValue(sqlFeeLv);
//				if (resultMinusRate != null && !resultMinusRate.equals("")) 
//				{
//					minusRate=Double.parseDouble(resultMinusRate);
//				}
//				else
//				{
//					CError.buildErr(this, "额外保费费率查询有误!");
//					return -1;
//				}
//				
//							
//				fee += minusPrem * minusRate;
//				System.out.println("额外保费为 "+ minusPrem + "，管理费率"+minusRate+"，收取管理费" + fee );
//			}		
/*			
 *			对于5年期以上的保单的额外保费初始扣费新写程序支持
 *			add by xp 20120329 
*/
			//统一调整为同一个计算程序,一劳永逸 2012-3-31
//			else{
//				本次续期存在额外保费的才进行处理
				if (curPrem > basePrem) 
				{
					CalInitialFee tminusCalInitialFee = new CalInitialFee(tempLJSPayPersonSchema,basePrem);
					Hashtable minuscur = tminusCalInitialFee.calMinus();
					if(minuscur == null)
					{
						return -1;
					}
					
					for (int j = 1; j <= minuscur.size(); j++) 
					{
						String PremPeriod = (String)minuscur.get(String.valueOf(j));
						String[] sPremPeriod = new String[2];
						sPremPeriod = PremPeriod.split("@");
						
						//缴费金额
						double tCurMinusPrem = Double.parseDouble(sPremPeriod[0]);
						
						//第几次缴费
						int tPayCount = Integer.parseInt(sPremPeriod[1]);
						
						System.out.println("curPrem == " + tCurMinusPrem);
						System.out.println("curPeriod == " + tPayCount);
						
//						额外保费的费率在描述表中取
						String sqlFeeLv="select rate from rate"+tempLJSPayPersonSchema.getRiskCode()+" where type='1' and "+tPayCount
						               +"  >= mincount and "+tPayCount+"<=maxcount ";
						if(!tempLJSPayPersonSchema.getRiskCode().equals("331701")){
							sqlFeeLv+=" and payintv=(select payintv from lccont where contno ='"
						               +tempLJSPayPersonSchema.getContNo()+"')  with ur";
						}
						String resultMinusRate = new ExeSQL().getOneValue(sqlFeeLv);
						if (resultMinusRate != null && !resultMinusRate.equals("")) 
						{
							double minusRate=Double.parseDouble(resultMinusRate);
							double minusFee=tCurMinusPrem*minusRate;
							System.out.println("缴费金额的额外保费金额中"+tCurMinusPrem+"按照第 "+tPayCount+" 次计算管理费 tempFee = " + minusFee);
							fee += minusFee;
						}
						else
						{
							CError.buildErr(this, "额外保费费率查询有误!");
							return -1;
						}
					}
					
				}
//			}
			System.out.println("拢共收取续期管理费 = " + fee);
			return fee;
		}

		// 设置LCInsuAccTrace
		private void setLCInsureAccTrace(
				LCInsureAccClassSchema aLCInsureAccClassSchema, String aMoneyType,
				double aMoney) {

			LCInsureAccTraceSchema tLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
			// 相同部分
			ref.transFields(tLCInsureAccTraceSchema, aLCInsureAccClassSchema);
			// 不同部分
			String serialNo = PubFun1.CreateMaxNo("SERIALNO",
					aLCInsureAccClassSchema.getManageCom());
			tLCInsureAccTraceSchema.setSerialNo(serialNo);
			tLCInsureAccTraceSchema.setMoneyType(aMoneyType);
//			tLCInsureAccTraceSchema.setPayDate(mLJAPayBL.getPayDate());
			
			//20091211 zhanggm 万能续期保费从保单周年日的下一天开始计算利息。为了满足月结批处理程序，修改轨迹表paydate为对应的保单周年日。
			String tPayToDate = mLCPolSchema.getPaytoDate();
			int i = PubFun.calInterval(CurrentDate, tPayToDate, "D");
			String tPayDate = i<=0 ? CurrentDate : tPayToDate;
			tLCInsureAccTraceSchema.setPayDate(tPayDate);
			
			//20090817 zhanggm 为了区别首期保费，将续期othertype设置为‘2’，otherno = 应收号。
			tLCInsureAccTraceSchema.setOtherNo(mGetNoticeNo);
			tLCInsureAccTraceSchema.setOtherType("2");
			
			tLCInsureAccTraceSchema.setOperator(mGI.Operator);
			tLCInsureAccTraceSchema.setMakeDate(tPayDate);
			tLCInsureAccTraceSchema.setMakeTime(aMakeTime);
			tLCInsureAccTraceSchema.setModifyDate(aMakeDate);
			tLCInsureAccTraceSchema.setModifyTime(aMakeTime);
			tLCInsureAccTraceSchema.setMoney(aMoney);
			
			mLCInsureAccTraceSet.add(tLCInsureAccTraceSchema);
		}

		// 设置LCInsureAccFeeTrace
		private void setLCInsureAccFeeTrace(
				LCInsureAccClassSchema aLCInsureAccClassSchema, String moneyType,
				 double manageMoney) {

			LCInsureAccFeeTraceSchema tLCInsureAccFeeTraceSchema = new LCInsureAccFeeTraceSchema();
			// 相同部分
			ref.transFields(tLCInsureAccFeeTraceSchema, aLCInsureAccClassSchema);
			// 不同部分
			String serialNo = PubFun1.CreateMaxNo("SERIALNO",
					aLCInsureAccClassSchema.getManageCom());
			tLCInsureAccFeeTraceSchema.setSerialNo(serialNo);
			tLCInsureAccFeeTraceSchema.setMoneyType(moneyType);
			tLCInsureAccFeeTraceSchema.setFeeRate(0);
			tLCInsureAccFeeTraceSchema.setFee(manageMoney);
			tLCInsureAccFeeTraceSchema.setFeeUnit(aLCInsureAccClassSchema
					.getUnitCount());
			tLCInsureAccFeeTraceSchema.setManageCom(mGI.ManageCom);
//			tLCInsureAccFeeTraceSchema.setPayDate(mPayDate);
			
//			//20091211 zhanggm 万能续期保费从保单周年日的下一天开始计算利息。为了满足月结批处理程序，修改轨迹表paydate为对应的保单周年日。
			String tPayToDate = mLCPolSchema.getPaytoDate();
			int i = PubFun.calInterval(CurrentDate, tPayToDate, "D");
			String tPayDate = i<=0 ? CurrentDate : tPayToDate;
			tLCInsureAccFeeTraceSchema.setPayDate(tPayDate);
			
			//tLCInsureAccFeeTraceSchema.setFeeCode("");
			tLCInsureAccFeeTraceSchema.setInerSerialNo("");
			
//			20090817 zhanggm 为了区别首期保费，将续期othertype设置为‘2’，otherno = 应收号。
			tLCInsureAccFeeTraceSchema.setOtherNo(mGetNoticeNo);
			tLCInsureAccFeeTraceSchema.setOtherType("2");
			
			tLCInsureAccFeeTraceSchema.setOperator(mGI.Operator);
			tLCInsureAccFeeTraceSchema.setMakeDate(tPayDate);
			tLCInsureAccFeeTraceSchema.setMakeTime(aMakeTime);
			tLCInsureAccFeeTraceSchema.setModifyDate(aMakeDate);
			tLCInsureAccFeeTraceSchema.setModifyTime(aMakeTime);
			mLCInsureAccFeeTraceSchema= tLCInsureAccFeeTraceSchema ;
		}
	private double getJL(LJSPayPersonSchema tLJSPayPersonSchema) {
		double fee = 0.0;
		double tPrem = tLJSPayPersonSchema.getSumDuePayMoney();
		System.out.println("本次保费*******tPrem = "+tPrem);
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = new SSRS();
		
		String tLapseDate = PubFun.calDate(tLJSPayPersonSchema.getLastPayToDate(),60, "D", null);
		FDate tFDate= new FDate();
		if(tFDate.getDate(CurrentDate).after(tFDate.getDate(tLapseDate)))
		{
			System.out.println("已过缴费宽限期，无需赠送!");
			return -1;
		}
		
/*		获取持续奖励描述
 *	     添加至ldcode1表中
	    CodeType =‘CXJL ’
	    Code1 -险种编码
	    CodeName -开始领取保单年度
	    CodeAlias ：0-趸交，1-期交
	    ComCode ：赠送比例
	    */
		LDCode1Schema tLDCode1Schema=new LDCode1Schema();
		LDCode1DB tLDCode1DB=new LDCode1DB();
		tLDCode1DB.setCodeType("CXJL");
		tLDCode1DB.setCode1(tLJSPayPersonSchema.getRiskCode());
		if(tLDCode1DB.query().size()>1){
			System.out.println("持续奖励查询描述表失败!");
			return -2;
		}else if(tLDCode1DB.query().size()<1){
			return -1;
		}
		tLDCode1Schema=tLDCode1DB.query().get(1);
		
		if(tLDCode1Schema.getCodeAlias().equals("0")){
			System.out.println("趸交不在此处处理持续奖金!");
			return -1;
		}
		
		String tSql = "select 1 from llclaimdetail where (getdutykind like '5%'  or getdutykind='800') and contno='"
					+tLJSPayPersonSchema.getContNo()+"' and riskcode='"+tLJSPayPersonSchema.getRiskCode()+"'  with ur";
		String tResult=tExeSQL.getOneValue(tSql);
		if(tResult!=null&&tResult.equals("1")){
			System.out.println("被保人已经身故，无需给付!");
			return -1;
		}
		
//		起始保单年度,在产品条款中定义的赠送持续奖金的年份上加1
		int tShouldYear=Integer.parseInt(tLDCode1Schema.getCodeName());
//		给付比例
		double trate=Double.parseDouble(tLDCode1Schema.getComCode());
		
		String tPayDate = tLJSPayPersonSchema.getCurPayToDate();
		int tInterval = PubFun.calInterval( mLCPolSchema.getCValiDate(),tPayDate, "Y");
		System.out.println("交费年度*******tInterval = "+tInterval);
		if (tInterval < tShouldYear) {
			return -1;
		} else {
			// 累计部分领取的个人账户价值不超过累计追加保险费与累计已领取的持续奖金二项之和
			tSql = "select count(*) from ldsysvar where sysvar='onerow' and (select value(sum(-getmoney),0) from "
					+ "ljagetendorse where polno='"
					+ this.mLCPolSchema.getPolNo()
					+ "'  and "
					+ "feeoperationtype='LQ')<=(select value(sum(money),0) from lcinsureacctrace where polno='"
					+ this.mLCPolSchema.getPolNo()
					+ "' and moneytype in('ZF','B'))";
			tSSRS = tExeSQL.execSQL(tSql);
			if (tSSRS == null) {
				System.out.println("持续奖励查询帐户表失败");
				return -2;
			}
			if (tSSRS.GetText(1, 1).equals("0")) {
				return -1;
			}
			fee = CommonBL.carry(Arith.mul(tPrem, trate));
		}
			return fee;
		}

		private double calRiskFee(LMRiskFeeSchema pLMRiskFeeSchema,
				double dSumPrem, int CountTime) {
			
			double dRiskFee = 0.0;
			if (pLMRiskFeeSchema.getFeeCalModeType().equals("0")) // 0-直接取值
			{
				if (pLMRiskFeeSchema.getFeeCalMode().equals("01")) // 固定值内扣
				{
					dRiskFee = pLMRiskFeeSchema.getFeeValue();
				} else if (pLMRiskFeeSchema.getFeeCalMode().equals("02")) // 固定比例内扣
				{
					dRiskFee = dSumPrem * pLMRiskFeeSchema.getFeeValue();
				} else {
					dRiskFee = pLMRiskFeeSchema.getFeeValue(); // 默认情况
				}
			} else if (pLMRiskFeeSchema.getFeeCalModeType().equals("1")) // 1-SQL算法描述
			{
				// 准备计算要素
				Calculator tCalculator = new Calculator();
				tCalculator.setCalCode(pLMRiskFeeSchema.getFeeCalCode());
				// 累计已交保费
				tCalculator.addBasicFactor("Prem", String.valueOf(dSumPrem));
				// 下面三个要素用来计算个险万能首期交费的初始费用计算。added by huxl @20080512
				
				double tAmnt = CommonBL.getTBAmnt(mLCPolSchema);
				//福泽B款增加投保年龄要素
				int appAge=PubFun.getInsuredAppAge(mLCPolSchema.getPolApplyDate(),mLCPolSchema.getInsuredBirthday());
				//add by lzy 201509税优续保的险种处理
				if(isXBPol(mLCPolSchema.getPolNo())){
					LCPolSchema xbLCPolSchema=getNewLCPolSchema(mLCPolSchema);
					appAge=PubFun.getInsuredAppAge(xbLCPolSchema.getPolApplyDate(),xbLCPolSchema.getInsuredBirthday());
				}
				tCalculator.addBasicFactor("AppAge", String.valueOf(appAge));
				
				tCalculator.addBasicFactor("Amnt", String.valueOf(tAmnt));
				tCalculator.addBasicFactor("PayIntv", String.valueOf(mLCPolSchema
						.getPayIntv()));
				tCalculator.addBasicFactor("CountTime", String.valueOf(CountTime)); //
				tCalculator.addBasicFactor("polno", mLCPolSchema.getPolNo());//税优产品风险保费增加polno参数

				//税优产品总折扣因子
				String TotalFactorSql = "select Totaldiscountfactor from lccontsub where prtno='" + mLCPolSchema.getPrtNo() + "'";
				ExeSQL tExeSQL = new ExeSQL();
				String tTotalFactorResult=tExeSQL.getOneValue(TotalFactorSql);
				if(null == tTotalFactorResult || "".equals(tTotalFactorResult) || "null".equals(tTotalFactorResult)){
					tCalculator.addBasicFactor("Totaldiscountfactor", "1");
				} else {
					tCalculator.addBasicFactor("Totaldiscountfactor", tTotalFactorResult);
				}
				
				String sCalResultValue = tCalculator.calculate();
				if (tCalculator.mErrors.needDealError()) {
					CError.buildErr(this, "管理费计算失败!");
					return -1;
				}

				try {
					dRiskFee = Double.parseDouble(sCalResultValue);
				} catch (Exception e) {
					CError.buildErr(this, "管理费计算结果错误!" + "错误结果：" + sCalResultValue);
					return -1;
				}
			}

			return dRiskFee;
		}


	    /**
	     * 公共核销程序
	     * @param TempFeeNo 暂交费号
	     * @return 包含纪录的集合(纪录如何处理具体见prepareOutputData函数)
	     */
	    public VData ReturnData(String TempFeeNo) {
	        if (TempFeeNo == null) {
	            // @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "IndiFinUrgeVerifyBL";
	            tError.functionName = "ReturnData";
	            tError.errorMessage = "传入暂交费号不能为空";
	            this.mErrors.addOneError(tError);
	            return null;
	        }

	//1-查询暂交费表，将TempFeeNo输入Schema中传入，查询得到Set集
	        VData tVData = new VData();
	        LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
	        LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
	        TempFeeQueryUI tTempFeeQueryUI = new TempFeeQueryUI();
	        tLJTempFeeSchema.setTempFeeNo(TempFeeNo);
	        tLJTempFeeSchema.setTempFeeType("2"); //交费类型为2：续期催收交费
	        tVData.add(tLJTempFeeSchema);
	        if (!tTempFeeQueryUI.submitData(tVData, "QUERY")) {
	            // @@错误处理
	            this.mErrors.copyAllErrors(tTempFeeQueryUI.mErrors);
	            CError tError = new CError();
	            tError.moduleName = "IndiFinUrgeVerifyBL";
	            tError.functionName = "ReturnData";
	            tError.errorMessage = "暂交费查询失败";
	            this.mErrors.addOneError(tError);
	            return null;
	        }
	        tVData.clear();
	        tVData = tTempFeeQueryUI.getResult();
	        tLJTempFeeSet.set((LJTempFeeSet) tVData.getObjectByObjectName(
	                "LJTempFeeSet", 0));
	        tLJTempFeeSchema = (LJTempFeeSchema) tLJTempFeeSet.get(1);
	        double tempMoney = tLJTempFeeSchema.getPayMoney();
	//2-查询应收总表
	        tVData.clear();
	        LJSPaySchema tLJSPaySchema = new LJSPaySchema();
	        LJSPaySet tLJSPaySet = new LJSPaySet();
	        VerDuePayFeeQueryUI tVerDuePayFeeQueryUI = new VerDuePayFeeQueryUI();
	        tLJSPaySchema.setGetNoticeNo(TempFeeNo);
	        tVData.add(tLJSPaySchema);
	        if (!tVerDuePayFeeQueryUI.submitData(tVData, "QUERY")) {
	            // @@错误处理
	            this.mErrors.copyAllErrors(tVerDuePayFeeQueryUI.mErrors);
	            CError tError = new CError();
	            tError.moduleName = "IndiFinUrgeVerifyBL";
	            tError.functionName = "ReturnData";
	            tError.errorMessage = "应收总查询失败";
	            this.mErrors.addOneError(tError);
	            return null;
	        }
	        tVData.clear();
	        tVData = tVerDuePayFeeQueryUI.getResult();
	        tLJSPaySet.set((LJSPaySet) tVData.getObjectByObjectName("LJSPaySet",
	                0));
	        tLJSPaySchema = (LJSPaySchema) tLJSPaySet.get(1);
	        double sumDueMoney = tLJSPaySchema.getSumDuePayMoney();
	        if (sumDueMoney != tempMoney) {
	            // @@错误处理
	            CError tError = new CError();
	            tError.moduleName = "IndiFinUrgeVerifyBL";
	            tError.functionName = "ReturnData";
	            tError.errorMessage = "应收总表纪录中的金额和暂交费纪录中的金额不相等！";
	            this.mErrors.addOneError(tError);
	            return null;
	        }
	        if (tLJTempFeeSchema.getEnterAccDate() == null) {
	            CError tError = new CError();
	            tError.moduleName = "IndiFinUrgeVerifyBL";
	            tError.functionName = "ReturnData";
	            tError.errorMessage = "财务缴费还没有到帐!（暂交费收据号：" +
	                                  tLJTempFeeSchema.getTempFeeNo().trim() +
	                                  "）";
	            this.mErrors.addOneError(tError);
	            return null;
	        }
//	      if(PubFun.calInterval(PubFun.getCurrentDate(),tLJSPaySchema.getPayDate(),"D")<0)
//	      {
//	          CError tError = new CError();
//	          tError.moduleName = "IndiFinUrgeVerifyBL";
//	          tError.functionName = "ReturnData";
//	          tError.errorMessage = "保单已过失效日期:应收纪录超过失效期核销 ";
//	          this.mErrors .addOneError(tError) ;
//	          return null;
//	      }
	        tVData.clear();
	        tVData.add(tLJTempFeeSchema);
	        tVData.add(tLJSPaySchema);
	//3-调用核销程序
	        this.mOperate = "VERIFY";
	        if (!getInputData(tVData)) {
	            return null;
	        }

	        //进行业务处理
	        if (!dealData()) {
	            return null;
	        }
	        System.out.println("After dealData！");
	        //准备往后台的数据
	        if (!prepareOutputData()) {
	            return null;
	        }

	        return mInputData;
	    }

	    /**
	     * 得到原责任信息
	     * @param tLCPolSchema LCPolSchema:当前险种信息
	     * @return LCPolSchema：原险种信息
	     */
	    private LCPolSchema getOldLCPolSchema(LCPolSchema tLCPolSchema)
	    {
	        //如果不是续保险种，则直接返回
	        if(!isXBPol(tLCPolSchema.getPolNo()))
	        {
	            return tLCPolSchema;
	        }

	        StringBuffer sql = new StringBuffer();
	        sql.append("select a.* ")
	            .append("from LCPol a, LCRnewStateLog b ")
	            .append("where a.polNo = b.polNo ")
	            .append("   and a.renewCount = b.renewCount -1 ")  //原险种续保次数比续保临时数据少1次
	            .append("   and b.newPolNo = '")
	            .append(tLCPolSchema.getPolNo()).append("' ");
	        System.out.println(sql.toString());

	        LCPolSet set = new LCPolDB().executeQuery(sql.toString());
	        if(set.size() == 0)
	        {
	            mErrors.addOneError("险种" + tLCPolSchema.getRiskCode()
	                                + "是续保险种，但是没有查询到原险种信息。");
	            return null;
	        }
	        return set.get(1);
	    }
	    
	    /**
	     * 得到新险种信息
	     * @param tLCPolSchema LCPolSchema:原险种信息
	     * @return LCPolSchema：当前续保险种信息
	     */
	    private LCPolSchema getNewLCPolSchema(LCPolSchema tLCPolSchema)
	    {
	        //如果不是续保险种，则直接返回
	    	if(!isXBPol(tLCPolSchema.getPolNo()))
	        {
	            return tLCPolSchema;
	        }

	        StringBuffer sql = new StringBuffer();
	        sql.append("select a.* ")
	            .append("from LCPol a, LCRnewStateLog b ")
	            .append("where a.polNo = b.newPolNo ")
	            .append(" and state != '")
	            .append(XBConst.RNEWSTATE_DELIVERED).append("' ")
	            .append("   and b.PolNo = '")
	            .append(tLCPolSchema.getPolNo()).append("' ");
	        System.out.println(sql.toString());

	        LCPolSet set = new LCPolDB().executeQuery(sql.toString());
	        if(set.size() == 0)
	        {
	            mErrors.addOneError("险种" + tLCPolSchema.getRiskCode()
	                                + "是续保险种，但是没有查询到原险种信息。");
	            return null;
	        }
	        return set.get(1);
	    }

	    /**
	     * 校验险种是否续保险种
	     * @param polNo String: 使用原险种号或新险种号均可判断
	     * @return boolean:是续保险种true，否则false
	     */
	    private boolean isXBPol(String polNo)
	    {
	        StringBuffer sql = new StringBuffer();
	        sql.append("select 1 ")
	            .append("from LCRnewStateLog ")
	            .append("where newPolNo = '").append(polNo)
	            .append("'  and state != '")
	            .append(XBConst.RNEWSTATE_DELIVERED).append("' ")
	            .append(" union ")
	            .append("select 1 ")
	            .append("from LCRnewStateLog ")
	            .append("where PolNo = '").append(polNo)
	            .append("'  and state != '")
	            .append(XBConst.RNEWSTATE_DELIVERED).append("' ");
	        String rs = new ExeSQL().getOneValue(sql.toString());
	        if(!rs.equals("") && !rs.equals("null"))
	        {
	            return true;
	        }

	        return false;
	    }
	    /**
	     * 校验险种是否续保险种
	     * @param polNo String: 险种号
	     * @return boolean:是续保险种true，否则false
	     */
	    private boolean isXBCont(String contNo)
	    {
	        StringBuffer sql = new StringBuffer();
	        sql.append("select 1 ")
	            .append("from LCRnewStateLog ")
	            .append("where ContNo = '").append(contNo)
	            .append("'  and state != '")
	            .append(XBConst.RNEWSTATE_DELIVERED).append("' ");
	        String rs = new ExeSQL().getOneValue(sql.toString());
	        if(!rs.equals("") && !rs.equals("null"))
	        {
	            return true;
	        }

	        return false;
	    }
	    /**是否为税优保单**/
	    private boolean syFlag(String RiskCode){
	    	String str="select 1 from lmriskapp where riskcode='"+RiskCode+"' and TaxOptimal='Y'";
			String SYflag=new ExeSQL().getOneValue(str);
			if(null != SYflag && !"".equals(SYflag)){
				return true;
			}
			return false;
	    }
	    
/*	    *//**
	     * 得到投保时保额
	     * @param aLCPolSchema LCPolSchema 
	     * @return double
	     * 2010-5-25 计算初始保费要用投保时保额 CQ号：
	     *//*
	    private double getTBAmnt(LCPolSchema aLCPolSchema)
	    {
	    	ExeSQL tExeSQL = new ExeSQL();
	    	double tAmnt = aLCPolSchema.getAmnt();
	    	
//	    	添加对于湖南相关保单的配置
	    	String sqlcheck = "select count(1) from lpedorespecialdata where edorvalue='1' and edortype = 'WN' and detailtype = 'ULIFEE' and edorno = '" + aLCPolSchema.getContNo() + "' with ur";
	     	String tCount = tExeSQL.getOneValue(sqlcheck);
	     	if(!"0".equals(tCount))
	     	{
	     		return tAmnt;
	     	}	     	
	    	
	    	try 
	    	{
				String tContNo = aLCPolSchema.getContNo();
				String sql = "select edoracceptno from LPEdorItem a where ContNo = '" + tContNo + "' and EdorType = 'BA' "
					       + "and exists (select 1 from LPEdorApp where EdorAcceptNo = a.EdorAcceptNo and EdorState = '0') " 
					       + "order by edoracceptno ";
				String firstEdorNo = tExeSQL.getOneValue(sql);
				if(!firstEdorNo.equals(""))
				{
					sql = "select Amnt from LPPol where EdorNo = '" + firstEdorNo  + "' and PolNo = '" + aLCPolSchema.getPolNo() 
					    + "' and EdorType = 'BA' ";
					String sAmnt = tExeSQL.getOneValue(sql);
					tAmnt = Double.parseDouble(sAmnt);
				}
				
			} 
	    	catch (RuntimeException e)
	    	{
				e.printStackTrace();
				return tAmnt;
			}
	    	return tAmnt;
	    }*/
	    private MMap lockLJAGet(LCContSchema ttLCContSchema)
	    {
	    	
	    	
	        MMap tMMap = null;
	        /**锁定标志"TX"*/
	        String tLockNoType = "TX";
	        /**锁定时间*/
	        String tAIS = "3600";
	        System.out.println("设置了60秒的解锁时间");
	        TransferData tTransferData = new TransferData();
	        tTransferData.setNameAndValue("LockNoKey", ttLCContSchema.getContNo());
	        tTransferData.setNameAndValue("LockNoType", tLockNoType);
	        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);
	       
	        VData tVData = new VData();
	        tVData.add(tTransferData);
	        tVData.add(mGI);
	        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
	        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
	        
	        if (tMMap == null)
	        {           	
	            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
	            return null;
	        }
	        return tMMap;
	    }
}
