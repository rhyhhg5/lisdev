package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class GRnewLJSCancelUI {

  //业务处理相关变量
  private VData mInputData ;
  public  CErrors mErrors=new CErrors();

  public GRnewLJSCancelUI() {
  }
  public static void main(String[] args) {
   // GrpLJSCancelUI GrpDueFeeUI1 = new GrpLJSCancelUI();
    GlobalInput tGI = new GlobalInput();
    tGI.ComCode="86";
    tGI.Operator="001";
    tGI.ManageCom="86";
    LJSPaySchema tLJSPaySchema = new LJSPaySchema();
    tLJSPaySchema.setGetNoticeNo("31000000145");

    TransferData tTransferData= new TransferData();
    tTransferData.setNameAndValue("CancelMode","0");
    GRnewLJSCancelUI tGRnewLJSCancelUI= new GRnewLJSCancelUI();
    VData tVData = new VData();
    tVData.add(tGI);
    tVData.add(tLJSPaySchema);
    tVData.add(tTransferData);

    tGRnewLJSCancelUI.submitData(tVData,"INSERT");

  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    GRnewLJSCancelBL tGRnewLJSCancelBL=new GRnewLJSCancelBL();
    System.out.println("Start GRnewLJSCancelBL UI Submit...");
    tGRnewLJSCancelBL.submitData(mInputData,cOperate);

    System.out.println("End GRnewLJSCancelBL UI Submit...");

    mInputData=null;
    //如果有需要处理的错误，则返回
    if (tGRnewLJSCancelBL.mErrors.needDealError() )
       {
       this.mErrors.copyAllErrors(tGRnewLJSCancelBL.mErrors ) ;
       return false;
       }
   System.out.println("error num="+mErrors.getErrorCount());
    return true;
  }

}
