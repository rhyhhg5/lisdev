 package com.sinosoft.lis.operfee;

import java.util.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单查询业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class NewLCPolQueryBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  /** 业务处理相关变量 */
  /** 保费项表 */
  private LCPolSchema  mLCPolSchema = new LCPolSchema() ;
  private LCPolSet  mLCPolSet = new LCPolSet() ;

  public NewLCPolQueryBL() {}

  public static void main(String[] args) {
  }

  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
       return false;
System.out.println("---getInputData---");

    //进行业务处理
    if (!queryLCPol())
       return false;
System.out.println("---queryLCPol---");

     return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    // 检验查询条件
    mLCPolSchema.setSchema((LCPolSchema)cInputData.getObjectByObjectName("LCPolSchema",0));

    if(mLCPolSchema == null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "NewLCPolQueryBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "请输入查询条件!";
      this.mErrors.addOneError(tError) ;
      return false;
    }
    return true;
  }


  /**
   * 查询个人保单表信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean queryLCPol()
  {
      //拼写SQL串,多条件查询
      String PolNo=mLCPolSchema.getPolNo();
      String GrpPolNo=mLCPolSchema.getGrpPolNo();
      String ManageCom=mLCPolSchema.getManageCom();
      String startDate=mLCPolSchema.getGetStartDate();
      String endDate=mLCPolSchema.getPayEndDate();
      String RiskCode=mLCPolSchema.getRiskCode();//险种编码对应催交标志
      String UWFlag=mLCPolSchema.getUWFlag();//是否检验核保标记
      String PaytoDate=mLCPolSchema.getPaytoDate();//是否检验交至日期
      String PrtNo=mLCPolSchema.getPrtNo();   //得到印刷号
      int    PayIntv = mLCPolSchema.getPayIntv();

      if(ManageCom==null) ManageCom="";
      if(startDate==null) startDate="";
      if(endDate==null) endDate="";
      if(RiskCode==null) RiskCode="";
      if(UWFlag==null) UWFlag="";
      if(PaytoDate==null) PaytoDate="";
      if(PrtNo==null) PrtNo="";

      String sqlStr="select * from LCPol";
      if(GrpPolNo==null||GrpPolNo=="")//如果是查询个人保单（集体保单号=20个"0",则查询条件GrpPolNo设为空值）
      {
       if(PolNo!=null&&PolNo!="")//个单查询
        {
         sqlStr=sqlStr+" where PolNo='"+PolNo+"' and AppFlag='0' ";
         sqlStr=sqlStr+"  and PayLocation='8'  ";
         if(UWFlag.equals("Y"))
         {
           sqlStr=sqlStr+"  and (UWFlag='3' or UWFlag='4' or UWFlag='9') ";
         }
         if(!ManageCom.equals(""))
          {
           String MaxManageCom=PubFun.RCh(ManageCom,"9",8);
           String MinManageCom=PubFun.RCh(ManageCom,"0",8);;
           sqlStr=sqlStr+" and ManageCom>='"+MinManageCom+"' and ManageCom<='"+MaxManageCom+"'";
          }
         if(PayIntv==-1)
           sqlStr=sqlStr+" and PayIntv!=0 ";

//判断当前日期>=交至日期且当前日期-2个月<=交至日期 (2个月内有效)
         FDate tD=new FDate();
         Date newBaseDate =new Date();
         String CurrentDate = PubFun.getCurrentDate();
System.out.println("当前日期:"+CurrentDate);
         newBaseDate=tD.getDate(CurrentDate);
         Date AfterDate = PubFun.calDate(newBaseDate,-2,"M",null);
         String SubDate=tD.getString(AfterDate);
System.out.println("当前日期-2M:"+SubDate);
        if(PaytoDate.equals("Y"))
        {
          sqlStr=sqlStr+" and ((PaytoDate<='"+CurrentDate+"' and PaytoDate>='"+SubDate+"') or PaytoDate is null)";
        }
         sqlStr=sqlStr+" and (StopFlag='0' or StopFlag is null) and GrpPolNo='00000000000000000000'" ;
         sqlStr=sqlStr+" and (PaytoDate<PayEndDate or PaytoDate is null) ";
         if(RiskCode.equals("N"))//如果险种编码对应不催交
         {
           sqlStr=sqlStr+" and RiskCode in(select RiskCode from LMRiskPay where UrgePayFlag='N') ";
         }
         else if(RiskCode.equals("Y"))//如果险种编码对应催交
         {
           sqlStr=sqlStr+" and RiskCode in(select RiskCode from LMRiskPay where UrgePayFlag='Y') ";
         }
        }
        else     //批量查询，日期范围
        {
         sqlStr=sqlStr+" where ((PaytoDate>='"+startDate+"' and PaytoDate<='"+endDate+"') or PaytoDate is null) ";
         sqlStr=sqlStr+"  and PayLocation='8'  ";
         sqlStr=sqlStr+"  and AppFlag='0' ";
         if(UWFlag.equals("Y"))
         {
           sqlStr=sqlStr+"  and (UWFlag='3' or UWFlag='4' or UWFlag='9') ";
         }
         if(!ManageCom.equals(""))
           {
           String MaxManageCom=PubFun.RCh(ManageCom,"9",8);
           String MinManageCom=PubFun.RCh(ManageCom,"0",8);
           sqlStr=sqlStr+" and ManageCom>='"+MinManageCom+"' and ManageCom<='"+MaxManageCom+"'";
           }
         if(PayIntv==-1)
           sqlStr=sqlStr+" and	PayIntv!=0";

         sqlStr=sqlStr+" and (StopFlag='0' or StopFlag is null) and GrpPolNo='00000000000000000000'" ;
         sqlStr=sqlStr+" and (PaytoDate<PayEndDate or PaytoDate is null) ";
         if(RiskCode.equals("N"))//如果险种编码对应不催交
          sqlStr=sqlStr+" and RiskCode in(select RiskCode from LMRiskPay where UrgePayFlag='N') ";
         else if(RiskCode.equals("Y"))//如果险种编码对应催交
          sqlStr=sqlStr+" and RiskCode in(select RiskCode from LMRiskPay where UrgePayFlag='Y') ";
        }
      }
      else //查询集体保单下的个人保单，则查询条件GrpPolNo不为空
      {
      	 sqlStr=sqlStr+"  where GrpPolNo='"+GrpPolNo+"' and AppFlag='0'";
         sqlStr=sqlStr+"  and PayLocation='8'  ";
         if(UWFlag.equals("Y"))
         {
           sqlStr=sqlStr+"  and (UWFlag='3' or UWFlag='4' or UWFlag='9') ";
         }
      	 if(!ManageCom.equals(""))
           {
           String MaxManageCom=PubFun.RCh(ManageCom,"9",8);
           String MinManageCom=PubFun.RCh(ManageCom,"0",8);;
           sqlStr=sqlStr+" and ManageCom>='"+MinManageCom+"' and ManageCom<='"+MaxManageCom+"'";

           }
         if(PayIntv==-1)
           sqlStr=sqlStr+" and	PayIntv!=0";

         sqlStr=sqlStr+" and (StopFlag='0' or StopFlag is null)" ;
         sqlStr=sqlStr+" and (PaytoDate<PayEndDate or PaytoDate is null) ";
         if(RiskCode.equals("N"))//如果险种编码对应不催交
          sqlStr=sqlStr+" and RiskCode in(select RiskCode from LMRiskPay where UrgePayFlag='N') ";
         else if(RiskCode.equals("Y"))//如果险种编码对应催交
          sqlStr=sqlStr+" and RiskCode in(select RiskCode from LMRiskPay where UrgePayFlag='Y') ";

      }
System.out.println("in BL SQL="+sqlStr);

      LCPolDB tLCPolDB = new LCPolDB();
      tLCPolDB.setSchema(mLCPolSchema);
      mLCPolSet = tLCPolDB.executeQuery(sqlStr);
      if (tLCPolDB.mErrors.needDealError() == true)
      {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCPolDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "NewLCPolQueryBL";
      tError.functionName = "queryData";
      tError.errorMessage = "保单表查询失败!";
      this.mErrors.addOneError(tError);
      mLCPolSet.clear();
      return false;
    }
    if (mLCPolSet.size() == 0)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "NewLCPolQueryBL";
      tError.functionName = "queryData";
      tError.errorMessage = "未找到相关数据!";
      this.mErrors.addOneError(tError);
      mLCPolSet.clear();
      return false;
    }
	mResult.clear();
	mResult.add( mLCPolSet );
	return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData()
  {
    mResult.clear();
    try
    {
      mResult.add( mLCPolSet );
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="NewLCPolQueryBL";
      tError.functionName="prepareOutputData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
    }
}
