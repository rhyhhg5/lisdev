package com.sinosoft.lis.operfee;

//程序名称：PrtContContinueBL.java
//程序功能：个险保单继续率统计,查询保单继续率汇总,下载清单。
//创建日期：2011-4-18 
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import com.sinosoft.lis.db.LCContactDB;
import com.sinosoft.lis.pubfun.CreateExcelList;
import com.sinosoft.lis.pubfun.FDate;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.schema.LCContactSchema;
import com.sinosoft.lis.vschema.LCContactSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class PrtContContinueBL {

  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public CErrors mErrors=new CErrors();

  private VData mResult = new VData();
  
  private CreateExcelList mCreateExcelList = new CreateExcelList("");
  
  private TransferData mTransferData = new TransferData();
  
  private GlobalInput mGlobalInput =new GlobalInput();
  
  //对比起始日期
  private String mStartDate="";
  private String mEndDate="";
  private String mManageCom="";
  private String mContinueType=""; 
  private String mRiskType=""; 
  
  public PrtContContinueBL() 
  {
  }
  
  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
	  mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
	  if(mGlobalInput==null) 
	  {
	    buildError("getInputData", "没有得到公共信息！");
	    return false;
	  }
	  mTransferData = (TransferData) cInputData.getObjectByObjectName("TransferData", 0);
	  mStartDate = (String) mTransferData.getValueByName("StartDate");
	  mEndDate = (String) mTransferData.getValueByName("EndDate");
	  mManageCom = (String) mTransferData.getValueByName("ManageCom");
	  mContinueType = (String) mTransferData.getValueByName("ContinueType");
	  System.out.println("mContinueType:1-13个月，2-25个月---"+mContinueType);
	  mRiskType = (String) mTransferData.getValueByName("RiskType");
	  System.out.println("mRiskType:1-传统险,2-万能---"+mRiskType);
	  if(mStartDate.equals("")||mEndDate.equals("")||mStartDate==null||mEndDate==null) 
	  {
	    buildError("getInputData", "没有得到足够的信息:起始和终止日期不能为空！");
	    return false;
	  }
	  return true;
  }

/**
  传输数据的公共方法
*/
    public CreateExcelList getsubmitData(VData cInputData, String cOperate)
    {

      // 得到外部传入的数据，将数据备份到本类中
      if(!getInputData(cInputData)) 
      {
        return null;
      }

      if(mRiskType.equals("1")) //1-传统险,2-万能
      {
    	  if(mManageCom.equals("86")) //总公司 86-总公司
    	  {
    		  if(mContinueType.equals("1")) //1-13个月，2-25个月
        	  {
    			  if(!getPrintDataCTZ13()) 
    	          {
    	            return null;
    	          }
        	  }
        	  else//25个月
        	  {
        		  if(!getPrintDataCTZ25()) 
    	          {
    	            return null;
    	          }
        	  }
    	  }
    	  else//分公司
    	  {
    		  if(mContinueType.equals("1")) //1-13个月，2-25个月
        	  {
    			  if(!getPrintDataCTF13()) 
    	          {
    	            return null;
    	          }
        	  }
        	  else//25个月
        	  {
        		  if(!getPrintDataCTF25()) 
    	          {
    	            return null;
    	          }
        	  }
    	  }
      }
      if(mRiskType.equals("2")) //1-传统险,2-万能
      {
    	  if(mManageCom.equals("86")) //总公司 86-总公司
    	  {
    		  if(mContinueType.equals("1")) //1-13个月，2-25个月
        	  {
    			  if(!getPrintDataULIZ13()) 
    	          {
    	            return null;
    	          }
        	  }
        	  else//25个月
        	  {
        		  if(!getPrintDataULIZ25()) 
    	          {
    	            return null;
    	          }
        	  }
    	  }
    	  else//分公司
    	  {
    		  if(mContinueType.equals("1")) //1-13个月，2-25个月
        	  {
    			  if(!getPrintDataULIF13()) 
    	          {
    	            return null;
    	          }
        	  }
        	  else//25个月
        	  {
        		  if(!getPrintDataULIF25()) 
    	          {
    	            return null;
    	          }
        	  }
    	  }
      }

      if(mCreateExcelList==null)
      {
    	  buildError("submitData", "Excel数据为空");
          return null;
      }
      return mCreateExcelList;
    }
  
  public static void main(String[] args)
  {
	  PrtContContinueBL tbl =new PrtContContinueBL();
      GlobalInput tGlobalInput = new GlobalInput();
      TransferData tTransferData = new TransferData();
      tTransferData.setNameAndValue("StartDate", "2009-01-23"); // 生效日期
      tTransferData.setNameAndValue("EndDate", "2009-02-23");
      tTransferData.setNameAndValue("ManageCom", "8612");
      tTransferData.setNameAndValue("ContType", "0");  //0|全部^1|个险^2|银保
      tTransferData.setNameAndValue("SaleChnl", "00"); //渠道 00-全部
      tTransferData.setNameAndValue("ContinueType", "2"); //1-13个月，2-25个月
      tTransferData.setNameAndValue("Orphans", "0"); //保单服务状态, 1-在职单 2-孤儿单 0-全部
      tTransferData.setNameAndValue("RiskType", "1"); //1-传统险,2-万能
      tGlobalInput.ManageCom="86";
      tGlobalInput.Operator="zgm";
      VData tData = new VData();
      tData.add(tGlobalInput);
      tData.add(tTransferData);

      CreateExcelList tCreateExcelList=new CreateExcelList();
      tCreateExcelList=tbl.getsubmitData(tData,"1");
      if(tCreateExcelList==null)
      {
    	  System.out.println("112321231");
      }
      else
      {
    	  try
          {
        	  tCreateExcelList.write("c:\\contactcompare.xls");
          }catch(Exception e)
          {
        	  System.out.println("EXCEL生成失败！");
          }
      }
  }

  public VData getResult()
  {
    return mResult;
  }

  public CErrors getErrors()
  {
    return mErrors;
  }

  private void buildError(String szFunc, String szErrMsg)
  {
    CError cError = new CError( );

    cError.moduleName = "PrtContContinueBL";
    cError.functionName = szFunc;
    cError.errorMessage = szErrMsg;
    this.mErrors.addOneError(cError);
  }

  private boolean getPrintDataCTZ13()
  {
	  //创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      String[][] tTitle = {{"机构代码", "分公司", "继续率考核保费", "继续率考核件数", "续期实收保费", "续期实收件数", 
    	  "复效保费", "复效件数", "退保保费", "保费继续率", "件数继续率", "退保率"}};
      
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) 
      {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      
//    0|全部^1|个险^2|银保
      String tContType = (String) mTransferData.getValueByName("ContType");
      System.out.println("保单类型：0|全部^1|个险^2|银保--"+tContType);
      String sql1 = "";
      if(tContType.equals("0"))
      {
    	  sql1+=" and a.salechnl in ('01','04','10','13','14','15') ";
    	  System.out.println("保单类型：0|全部");  
      }
      else if (tContType.equals("1"))
      {
    	  sql1 = " and a.salechnl in ('01','10') ";
    	  System.out.println("保单类型：1|个险");
      }
      else if (tContType.equals("2"))
      {
    	  sql1 = " and a.salechnl in ('04','13') ";
    	  System.out.println("保单类型：2|银保");
      }
      else if (tContType.equals("3"))
      {
    	  sql1 = " and a.salechnl in ('14','15') ";
    	  System.out.println("保单类型：3|互动");
      }
      //4其他待定
      else
      {
    	  buildError("getPrintData", "保单类型传入错误！");
          return false;
      }
      
//    00|全部^01|个险直销^10|中介代理^04|银行代理^07|职团开拓^08|电话销售^11|产代健^13|银代直销^99|其它
      String tSaleChnl = (String) mTransferData.getValueByName("SaleChnl"); 
      System.out.println("销售渠道："+tSaleChnl);
      String sql2 = "";
      if(tSaleChnl.equals("00"))
      {
    	  sql2 = " and a.SaleChnl in ('01','04','10','13','14','15') ";
      }
//      else if(tSaleChnl.equals("99"))
//      {
//    	  sql2 = " and a.SaleChnl not in ('01','03','04','07','08','10','11','13') ";
//      }
      else
      {
    	  sql2 = " and a.SaleChnl = '" + tSaleChnl + "' ";
      }
      
//保单服务状态 0-全部，1-在职单，2-孤儿单
//      获取孤儿单标志GETORPHANSSTATE(ContNo,AgentCode)
//      --返回 1 是孤儿单，返回 0 不是孤儿单
      String tOrphans = (String) mTransferData.getValueByName("Orphans"); 
      String sql3 = "";
      if(tOrphans.equals("0"))
      {
    	  System.out.println("保单服务状态 0-全部");
      }
      else if(tOrphans.equals("1"))
      {
    	  sql3 = " and GETORPHANSSTATE(a.ContNo,a.AgentCode) = '0' ";
    	  System.out.println("保单服务状态, 1-在职单");
      }
      else if(tOrphans.equals("2"))
      {
    	  sql3 = " and GETORPHANSSTATE(a.ContNo,a.AgentCode) = '1' ";
    	  System.out.println("保单服务状态，2-孤儿单");
      }
      else
      {
    	  buildError("getPrintData", "保单服务状态类型传入错误！");
          return false;
      }
      
      //获得EXCEL列信息      
      StringBuffer tSQL = new StringBuffer();
      tSQL.append("select Com4, (select Name from LDCom where ComCode = Com4), sum(InitPrem),count(distinct ContNo), ")
	  .append("sum(ActuPrem), count(distinct(case when ActuPrem <> 0 then ContNo end)), ")
	  .append("sum(FxPrem), count(distinct(case when FxPrem <> 0 then ContNo end)), ")
	  .append("sum(TbPrem), '','','' from ( ")
	  .append("select ContNo, substr(ManageCom,1,4) Com4, " )
	  .append("GETINITPREM(a.PolNo) InitPrem, ")
	  .append("GETACTUPREM(a.PolNo,'13') ActuPrem, ")
	  .append("GETFXPREM(a.PolNo,'13') FxPrem, ")
	  .append("GETTBPREM(a.PolNo,'13') TbPrem ")
	  .append("from LCPol a where 1=1 " )
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') ")
	  .append("and not exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') ")
	  .append("and ContType = '1' and AppFlag = '1' and Prem <>0 and PayIntv = 12 ") //PayIntv先设置为年缴的 
	  .append("and year(PayEndDate)-year(CvaliDate) >= 2 ") //对于1年缴保单，13、25月继续率清单均不列；对于2年缴保单，列13个月，不列25个月；对于3年及以上缴费，13、25个月都列。
	  .append(sql1) //保单类型
	  .append(sql2) //销售渠道
	  .append(sql3) //是否孤儿单
	  .append("and CvaliDate between '" + mStartDate + "' and '" + mEndDate + "' ")
	  .append("and ManageCom like '" + mManageCom + "%' ")
	  .append(" union all ")
	  .append("select ContNo, substr(ManageCom,1,4) Com4, " )
	  .append("GETINITPREM(a.PolNo) InitPrem, ")
	  .append("GETACTUPREM(a.PolNo,'13') ActuPrem, ")
	  .append("GETFXPREM(a.PolNo,'13') FxPrem, ")
	  .append("GETTBPREM(a.PolNo,'13') TbPrem ")
	  .append("from LBPol a where 1=1 " )
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') ")
	  .append("and not exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') ")
	  .append("and ContType = '1' and Prem <>0 and PayIntv = 12 ") //PayIntv先设置为年缴的
//	  解约和协议退保的，不包含犹豫期退保
	  .append("and exists (select 1 from LPEdorItem where EdorNo = a.EdorNo and EdorType in ('CT','XT')) "  )
	  .append("and year(PayEndDate)-year(CvaliDate) >= 2 ") //对于1年缴保单，13、25月继续率清单均不列；对于2年缴保单，列13个月，不列25个月；对于3年及以上缴费，13、25个月都列。
	  .append(sql1) //保单类型
	  .append(sql2) //销售渠道
	  .append(sql3) //是否孤儿单
	  .append("and CvaliDate between '" + mStartDate + "' and '" + mEndDate + "' ")
	  .append("and ManageCom like '" + mManageCom + "%' ")
	  .append(") as b group by Com4 order by Com4 with ur");
      
      System.out.println("查询sql：" + tSQL.toString());
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "PrtContContinueBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }

      String[][] tGetData = null;
      tGetData=tSSRS.getAllData();
      
      if(tGetData==null)
      {
    	  CError tError = new CError();
    	  tError.moduleName = "PrtContContinueBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }
      
      System.out.println("拢共有这些条："+tGetData.length);
      
      for (int j = 0; j < tGetData.length;j++) 
      {
//    	  "机构代码", "分公司", "继续率考核保费", "继续率考核件数", "续期实收保费", "续期实收件数", 
//    	  "复效保费", "复效件数", "退保保费", "保费继续率", "件数继续率", "退保率"
    	  double initPrem =  Double.parseDouble(tGetData[j][2]); //继续率考核保费
    	  double initCount =  Double.parseDouble(tGetData[j][3]); //继续率考核件数
    	  double actuPrem =  Double.parseDouble(tGetData[j][4]); //续期实收保费
    	  double actuCount =  Double.parseDouble(tGetData[j][5]); //续期实收件数
    	  double fxPrem =  Double.parseDouble(tGetData[j][6]); //复效保费
    	  double fxCount =  Double.parseDouble(tGetData[j][7]); //复效件数
    	  double tbPrem =  Double.parseDouble(tGetData[j][8]); //退保保费
    	  
    	  double premRate = (actuPrem+fxPrem)/initPrem ; //    	  保费继续率：(续期实收保费+复效保费)/继续率考核保费。
    	  double countRate = (actuCount+fxCount)/initCount ; //    	  件数继续率：(续期实收件数+复效件数)/继续率考核件数。
    	  double tbRate = (tbPrem)/initPrem ;//    	  退保率：退保保费/继续率考核保费，13个月继续率统计，25个月继续率不统计。
    	  
    	  tGetData[j][9] = String.valueOf(PubFun.setPrecision(premRate, "0.0000")); //4舍6入5凑偶
    	  tGetData[j][10] = String.valueOf(PubFun.setPrecision(countRate, "0.0000"));
    	  tGetData[j][11] = String.valueOf(PubFun.setPrecision(tbRate, "0.0000"));

      }
      
      if(mCreateExcelList.setData(tGetData,displayData)==-1)
      {
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }

      return true;
  }
  
  private boolean getPrintDataCTZ25()
  {
	  //创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      String[][] tTitle = {{"机构代码", "分公司", "继续率考核保费", "继续率考核件数", "续期实收保费", "续期实收件数", 
    	  "复效保费", "复效件数", "保费继续率", "件数继续率"}};
      
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6,7,8,9,10};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6,7,8,9,10};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) 
      {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      
//    0|全部^1|个险^2|银保
      String tContType = (String) mTransferData.getValueByName("ContType");
      System.out.println("保单类型："+tContType);
      String sql1 = "";
      if(tContType.equals("0"))
      {
    	  sql1+=" and a.salechnl in ('01','04','10','13','14','15') ";
    	  System.out.println("保单类型：0|全部");  
      }
      else if (tContType.equals("1"))
      {
    	  sql1 = " and a.salechnl in ('01','10') ";
    	  System.out.println("保单类型：1|个险");
      }
      else if (tContType.equals("2"))
      {
    	  sql1 = " and a.salechnl in ('04','13') ";
    	  System.out.println("保单类型：2|银保");
      }
      else if (tContType.equals("3"))
      {
    	  sql1 = " and a.salechnl in ('14','15') ";
    	  System.out.println("保单类型：3|互动");
      }
      //4其他待定
      else
      {
    	  buildError("getPrintData", "保单类型传入错误！");
          return false;
      }
      
//    00|全部^01|个险直销^10|中介代理^04|银行代理^07|职团开拓^08|电话销售^11|产代健^13|银代直销^99|其它
      String tSaleChnl = (String) mTransferData.getValueByName("SaleChnl"); 
      System.out.println("销售渠道："+tSaleChnl);
      String sql2 = "";
      if(tSaleChnl.equals("00"))
      {
    	  sql2 = " and a.SaleChnl in ('01','04','10','13','14','15') ";
      }
//      else if(tSaleChnl.equals("99"))
//      {
//    	  sql2 = " and a.SaleChnl not in ('01','03','04','07','08','10','11','13') ";
//      }
      else
      {
    	  sql2 = " and a.SaleChnl = '" + tSaleChnl + "' ";
      }
      
//保单服务状态 0-全部，1-在职单，2-孤儿单
//      获取孤儿单标志GETORPHANSSTATE(ContNo,AgentCode)
//      --返回 1 是孤儿单，返回 0 不是孤儿单
      String tOrphans = (String) mTransferData.getValueByName("Orphans"); 
      String sql3 = "";
      if(tOrphans.equals("0"))
      {
    	  System.out.println("保单服务状态 0-全部");
      }
      else if(tOrphans.equals("1"))
      {
    	  sql3 = " and GETORPHANSSTATE(a.ContNo,a.AgentCode) = '0' ";
    	  System.out.println("保单服务状态, 1-在职单");
      }
      else if(tOrphans.equals("2"))
      {
    	  sql3 = " and GETORPHANSSTATE(a.ContNo,a.AgentCode) = '1' ";
    	  System.out.println("保单服务状态，2-孤儿单");
      }
      else
      {
    	  buildError("getPrintData", "保单服务状态类型传入错误！");
          return false;
      }
      
      //获得EXCEL列信息      
      StringBuffer tSQL = new StringBuffer(); 
  	    tSQL.append("select Com4, (select Name from LDCom where ComCode = Com4), sum(InitPrem),count(distinct ContNo), ")
	  .append("sum(ActuPrem), count(distinct(case when ActuPrem <> 0 then ContNo end)), ")
	  .append("sum(FxPrem), count(distinct(case when FxPrem <> 0 then ContNo end)), ")
	  .append(" '','' from ( ")
	  .append("select ContNo, substr(ManageCom,1,4) Com4, ") 
	  .append("GETINITPREM(a.PolNo) InitPrem, ")
	  .append("GETACTUPREM(a.PolNo,'25') ActuPrem, ")
	  .append("GETFXPREM(a.PolNo,'25') FxPrem ")
	  .append("from LCPol a where 1=1 ") 
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') ")
	  .append("and not exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') ")
	  .append("and ContType = '1' and AppFlag = '1' and Prem <>0 and PayIntv = 12 ") //PayIntv先设置为年缴的 
	  .append("and year(PayEndDate)-year(CvaliDate) >= 3 ") //对于1年缴保单，13、25月继续率清单均不列；对于2年缴保单，列13个月，不列25个月；对于3年及以上缴费，13、25个月都列。
  	  .append(sql1) //保单类型		
  	  .append(sql2) //销售渠道
  	  .append(sql3) //是否孤儿单
	  .append("and CvaliDate between '" + mStartDate + "' and '" + mEndDate + "' ")
	  .append("and ManageCom like '" + mManageCom + "%' ")
	  .append(" union all ")
	  .append("select ContNo, substr(ManageCom,1,4) Com4, ") 
	  .append("GETINITPREM(a.PolNo) InitPrem, ")
	  .append("GETACTUPREM(a.PolNo,'25') ActuPrem, ")
	  .append("GETFXPREM(a.PolNo,'25') FxPrem ")
	  .append("from LBPol a where 1=1 " )
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') ")
	  .append("and not exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') ")
	  .append("and ContType = '1' and Prem <>0 and PayIntv = 12 ") //PayIntv先设置为年缴的
//  	  解约和协议退保的，不包含犹豫期退保
	  .append("and exists (select 1 from LPEdorItem where EdorNo = a.EdorNo and EdorType in ('CT','XT')) ")  
	  .append("and year(PayEndDate)-year(CvaliDate) >= 3 ") //对于1年缴保单，13、25月继续率清单均不列；对于2年缴保单，列13个月，不列25个月；对于3年及以上缴费，13、25个月都列。
  	  .append(sql1) //保单类型
  	  .append(sql2) //销售渠道
  	  .append(sql3) //是否孤儿单
	  .append("and CvaliDate between '" + mStartDate + "' and '" + mEndDate + "' ")
	  .append("and ManageCom like '" + mManageCom + "%' ")
	  .append(") as b group by Com4 order by Com4 with ur");
      
      System.out.println("查询sql：" + tSQL.toString());
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "PrtContContinueBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }

      String[][] tGetData = null;
      tGetData=tSSRS.getAllData();
      
      if(tGetData==null)
      {
    	  CError tError = new CError();
    	  tError.moduleName = "PrtContContinueBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }
      
      System.out.println("拢共有这些条："+tGetData.length);
      
      for (int j = 0; j < tGetData.length;j++) 
      {
//    	  "机构代码", "分公司", "继续率考核保费", "继续率考核件数", "续期实收保费", "续期实收件数", 
//    	  "复效保费", "复效件数", "退保保费", "保费继续率", "件数继续率", "退保率"
    	  double initPrem =  Double.parseDouble(tGetData[j][2]); //继续率考核保费
    	  double initCount =  Double.parseDouble(tGetData[j][3]); //继续率考核件数
    	  double actuPrem =  Double.parseDouble(tGetData[j][4]); //续期实收保费
    	  double actuCount =  Double.parseDouble(tGetData[j][5]); //续期实收件数
    	  double fxPrem =  Double.parseDouble(tGetData[j][6]); //复效保费
    	  double fxCount =  Double.parseDouble(tGetData[j][7]); //复效件数
    	  
    	  double premRate = (actuPrem+fxPrem)/initPrem ; //    	  保费继续率：(续期实收保费+复效保费)/继续率考核保费。
    	  double countRate = (actuCount+fxCount)/initCount ; //    	  件数继续率：(续期实收件数+复效件数)/继续率考核件数。
    	  
    	  tGetData[j][8] = String.valueOf(PubFun.setPrecision(premRate, "0.0000")); //4舍6入5凑偶
    	  tGetData[j][9] = String.valueOf(PubFun.setPrecision(countRate, "0.0000"));
    	  
      }
      
      if(mCreateExcelList.setData(tGetData,displayData)==-1)
      {
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }

      return true;
  }
  
  private boolean getPrintDataCTF13()
  {
	  //创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      String[][] tTitle = {{"机构代码", "分公司","支公司代码", "支公司", "继续率考核保费", "继续率考核件数", "续期实收保费", "续期实收件数", 
    	  "复效保费", "复效件数", "退保保费", "保费继续率", "件数继续率", "退保率"}};
      
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) 
      {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      
//    0|全部^1|个险^2|银保
      String tContType = (String) mTransferData.getValueByName("ContType");
      System.out.println("保单类型："+tContType);
      String sql1 = "";
      if(tContType.equals("0"))
      {
    	  sql1+=" and a.salechnl in ('01','04','10','13','14','15') ";
    	  System.out.println("保单类型：0|全部");  
      }
      else if (tContType.equals("1"))
      {
    	  sql1 = " and a.salechnl in ('01','10') ";
    	  System.out.println("保单类型：1|个险");
      }
      else if (tContType.equals("2"))
      {
    	  sql1 = " and a.salechnl in ('04','13') ";
    	  System.out.println("保单类型：2|银保");
      }
      else if (tContType.equals("3"))
      {
    	  sql1 = " and a.salechnl in ('14','15') ";
    	  System.out.println("保单类型：3|互动");
      }
      //4其他待定
      else
      {
    	  buildError("getPrintData", "保单类型传入错误！");
          return false;
      }
      
//    00|全部^01|个险直销^10|中介代理^04|银行代理^07|职团开拓^08|电话销售^11|产代健^13|银代直销^99|其它
      String tSaleChnl = (String) mTransferData.getValueByName("SaleChnl"); 
      System.out.println("销售渠道："+tSaleChnl);
      String sql2 = "";
      if(tSaleChnl.equals("00"))
      {
    	  sql2 = " and a.SaleChnl in ('01','04','10','13','14','15') ";
      }
//      else if(tSaleChnl.equals("99"))
//      {
//    	  sql2 = " and a.SaleChnl not in ('01','03','04','07','08','10','11','13') ";
//      }
      else
      {
    	  sql2 = " and a.SaleChnl = '" + tSaleChnl + "' ";
      }
      
//保单服务状态 0-全部，1-在职单，2-孤儿单
//      获取孤儿单标志GETORPHANSSTATE(ContNo,AgentCode)
//      --返回 1 是孤儿单，返回 0 不是孤儿单
      String tOrphans = (String) mTransferData.getValueByName("Orphans"); 
      String sql3 = "";
      if(tOrphans.equals("0"))
      {
    	  System.out.println("保单服务状态 0-全部");
      }
      else if(tOrphans.equals("1"))
      {
    	  sql3 = " and GETORPHANSSTATE(a.ContNo,a.AgentCode) = '0' ";
    	  System.out.println("保单服务状态, 1-在职单");
      }
      else if(tOrphans.equals("2"))
      {
    	  sql3 = " and GETORPHANSSTATE(a.ContNo,a.AgentCode) = '1' ";
    	  System.out.println("保单服务状态，2-孤儿单");
      }
      else
      {
    	  buildError("getPrintData", "保单服务状态类型传入错误！");
          return false;
      }
      
      //获得EXCEL列信息      
      StringBuffer tSQL = new StringBuffer();
	  tSQL.append("select Com4, (select Name from LDCom where ComCode = Com4), ")
	  .append("Com, (select Name from LDCom where ComCode = Com), sum(InitPrem),count(distinct ContNo), ")
	  .append("sum(ActuPrem), count(distinct(case when ActuPrem <> 0 then ContNo end)), ")
	  .append("sum(FxPrem), count(distinct(case when FxPrem <> 0 then ContNo end)), ")
	  .append("sum(TbPrem), '','','' from ( ")
	  .append("select ContNo, substr(ManageCom,1,4) Com4, ManageCom Com, ")
	  .append("GETINITPREM(a.PolNo) InitPrem, ")
	  .append("GETACTUPREM(a.PolNo,'13') ActuPrem, ")
	  .append("GETFXPREM(a.PolNo,'13') FxPrem, ")
	  .append("GETTBPREM(a.PolNo,'13') TbPrem ")
	  .append("from LCPol a where 1=1 ")
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') ")
	  .append("and not exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') ")
	  .append("and ContType = '1' and AppFlag = '1' and Prem <>0 and PayIntv = 12 ") //PayIntv先设置为年缴的
	  .append("and year(PayEndDate)-year(CvaliDate) >= 2 ") //对于1年缴保单，13、25月继续率清单均不列；对于2年缴保单，列13个月，不列25个月；对于3年及以上缴费，13、25个月都列。
	  .append(sql1) //保单类型
	  .append(sql2) //销售渠道
	  .append(sql3) //是否孤儿单
	  .append("and CvaliDate between '" + mStartDate + "' and '" + mEndDate + "' ")
	  .append("and ManageCom like '" + mManageCom + "%' ")
	  .append(" union all ")
	  .append("select ContNo, substr(ManageCom,1,4) Com4, ManageCom Com, ")
	  .append("GETINITPREM(a.PolNo) InitPrem, ")
	  .append("GETACTUPREM(a.PolNo,'13') ActuPrem, ")
	  .append("GETFXPREM(a.PolNo,'13') FxPrem, ")
	  .append("GETTBPREM(a.PolNo,'13') TbPrem ")
	  .append("from LBPol a where 1=1 ")
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') ")
	  .append("and not exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') ")
	  .append("and ContType = '1' and Prem <>0 and PayIntv = 12 ") //PayIntv先设置为年缴的
//解约和协议退保的，不包含犹豫期退保
	  .append("and exists (select 1 from LPEdorItem where EdorNo = a.EdorNo and EdorType in ('CT','XT')) ")
	  .append("and year(PayEndDate)-year(CvaliDate) >= 2 ") //对于1年缴保单，13、25月继续率清单均不列；对于2年缴保单，列13个月，不列25个月；对于3年及以上缴费，13、25个月都列。
	  .append(sql1) //保单类型
	  .append(sql2) //销售渠道
	  .append(sql3) //是否孤儿单
	  .append("and CvaliDate between '" + mStartDate + "' and '" + mEndDate + "' ")
	  .append("and ManageCom like '" + mManageCom + "%' ")
	  .append(") as b group by Com4, Com order by Com4,Com with ur");
      
      System.out.println("查询sql：" + tSQL.toString());
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "PrtContContinueBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }

      String[][] tGetData = null;
      tGetData=tSSRS.getAllData();
      
      if(tGetData==null)
      {
    	  CError tError = new CError();
    	  tError.moduleName = "PrtContContinueBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }
      
      System.out.println("拢共有这些条："+tGetData.length);
      
      for (int j = 0; j < tGetData.length;j++) 
      {
//    	  "机构代码", "分公司", "继续率考核保费", "继续率考核件数", "续期实收保费", "续期实收件数", 
//    	  "复效保费", "复效件数", "退保保费", "保费继续率", "件数继续率", "退保率"
    	  double initPrem =  Double.parseDouble(tGetData[j][4]); //继续率考核保费
    	  double initCount =  Double.parseDouble(tGetData[j][5]); //继续率考核件数
    	  double actuPrem =  Double.parseDouble(tGetData[j][6]); //续期实收保费
    	  double actuCount =  Double.parseDouble(tGetData[j][7]); //续期实收件数
    	  double fxPrem =  Double.parseDouble(tGetData[j][8]); //复效保费
    	  double fxCount =  Double.parseDouble(tGetData[j][9]); //复效件数
    	  double tbPrem =  Double.parseDouble(tGetData[j][10]); //退保保费
    	  
    	  double premRate = (actuPrem+fxPrem)/initPrem ; //    	  保费继续率：(续期实收保费+复效保费)/继续率考核保费。
    	  double countRate = (actuCount+fxCount)/initCount ; //    	  件数继续率：(续期实收件数+复效件数)/继续率考核件数。
    	  double tbRate = (tbPrem)/initPrem ;//    	  退保率：退保保费/继续率考核保费，13个月继续率统计，25个月继续率不统计。
    	  
    	  tGetData[j][11] = String.valueOf(PubFun.setPrecision(premRate, "0.0000")); //4舍6入5凑偶
    	  tGetData[j][12] = String.valueOf(PubFun.setPrecision(countRate, "0.0000"));
    	  tGetData[j][13] = String.valueOf(PubFun.setPrecision(tbRate, "0.0000"));
    	  
      }
      
      if(mCreateExcelList.setData(tGetData,displayData)==-1)
      {
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }

      return true;
  }
  
  private boolean getPrintDataCTF25()
  {
	  //创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      String[][] tTitle = {{"机构代码", "分公司","支公司代码", "支公司", "继续率考核保费", "继续率考核件数", "续期实收保费", "续期实收件数", 
    	  "复效保费", "复效件数", "保费继续率", "件数继续率"}};
      
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) 
      {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      
//    0|全部^1|个险^2|银保
      String tContType = (String) mTransferData.getValueByName("ContType");
      System.out.println("保单类型："+tContType);
      String sql1 = "";
      if(tContType.equals("0"))
      {
    	  sql1+=" and a.salechnl in ('01','04','10','13','14','15') ";
    	  System.out.println("保单类型：0|全部");  
      }
      else if (tContType.equals("1"))
      {
    	  sql1 = " and a.salechnl in ('01','10') ";
    	  System.out.println("保单类型：1|个险");
      }
      else if (tContType.equals("2"))
      {
    	  sql1 = " and a.salechnl in ('04','13') ";
    	  System.out.println("保单类型：2|银保");
      }
      else if (tContType.equals("3"))
      {
    	  sql1 = " and a.salechnl in ('14','15') ";
    	  System.out.println("保单类型：3|互动");
      }
      //4其他待定
      else
      {
    	  buildError("getPrintData", "保单类型传入错误！");
          return false;
      }
      
//    00|全部^01|个险直销^10|中介代理^04|银行代理^07|职团开拓^08|电话销售^11|产代健^13|银代直销^99|其它
      String tSaleChnl = (String) mTransferData.getValueByName("SaleChnl"); 
      System.out.println("销售渠道："+tSaleChnl);
      String sql2 = "";
      if(tSaleChnl.equals("00"))
      {
    	  sql2 = " and a.SaleChnl in ('01','04','10','13','14','15') ";
      }
//      else if(tSaleChnl.equals("99"))
//      {
//    	  sql2 = " and a.SaleChnl not in ('01','03','04','07','08','10','11','13') ";
//      }
      else
      {
    	  sql2 = " and a.SaleChnl = '" + tSaleChnl + "' ";
      }
      
//保单服务状态 0-全部，1-在职单，2-孤儿单
//      获取孤儿单标志GETORPHANSSTATE(ContNo,AgentCode)
//      --返回 1 是孤儿单，返回 0 不是孤儿单
      String tOrphans = (String) mTransferData.getValueByName("Orphans"); 
      String sql3 = "";
      if(tOrphans.equals("0"))
      {
    	  System.out.println("保单服务状态 0-全部");
      }
      else if(tOrphans.equals("1"))
      {
    	  sql3 = " and GETORPHANSSTATE(a.ContNo,a.AgentCode) = '0' ";
    	  System.out.println("保单服务状态, 1-在职单");
      }
      else if(tOrphans.equals("2"))
      {
    	  sql3 = " and GETORPHANSSTATE(a.ContNo,a.AgentCode) = '1' ";
    	  System.out.println("保单服务状态，2-孤儿单");
      }
      else
      {
    	  buildError("getPrintData", "保单服务状态类型传入错误！");
          return false;
      }
      
      //获得EXCEL列信息      
      StringBuffer tSQL = new StringBuffer();
	  tSQL.append("select Com4, (select Name from LDCom where ComCode = Com4), ")
	  .append("Com, (select Name from LDCom where ComCode = Com), sum(InitPrem),count(distinct ContNo), ")
	  .append("sum(ActuPrem), count(distinct(case when ActuPrem <> 0 then ContNo end)), ")
	  .append("sum(FxPrem), count(distinct(case when FxPrem <> 0 then ContNo end)), ")
	  .append("'','' from ( ")
	  .append("select ContNo, substr(ManageCom,1,4) Com4, ManageCom Com, ")
	  .append("GETINITPREM(a.PolNo) InitPrem, ")
	  .append("GETACTUPREM(a.PolNo,'25') ActuPrem, ")
	  .append("GETFXPREM(a.PolNo,'25') FxPrem ")
	  .append("from LCPol a where 1=1 ")
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') ")
	  .append("and not exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') ")
	  .append("and ContType = '1' and AppFlag = '1' and Prem <>0 and PayIntv = 12 ") //PayIntv先设置为年缴的
	  .append("and year(PayEndDate)-year(CvaliDate) >= 3 ") //对于1年缴保单，13、25月继续率清单均不列；对于2年缴保单，列13个月，不列25个月；对于3年及以上缴费，13、25个月都列。
	  .append(sql1) //保单类型
	  .append(sql2) //销售渠道
	  .append(sql3) //是否孤儿单
	  .append("and CvaliDate between '" + mStartDate + "' and '" + mEndDate + "' ")
	  .append("and ManageCom like '" + mManageCom + "%' ")
	  .append(" union all ")
	  .append("select ContNo, substr(ManageCom,1,4) Com4, ManageCom Com, ")
	  .append("GETINITPREM(a.PolNo) InitPrem, ")
	  .append("GETACTUPREM(a.PolNo,'25') ActuPrem, ")
	  .append("GETFXPREM(a.PolNo,'25') FxPrem ")
	  .append("from LBPol a where 1=1 ")
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') ")
	  .append("and not exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') ")
	  .append("and ContType = '1' and Prem <>0 and PayIntv = 12 ") //PayIntv先设置为年缴的
//   解约和协议退保的，不包含犹豫期退保
	  .append("and exists (select 1 from LPEdorItem where EdorNo = a.EdorNo and EdorType in ('CT','XT')) ")
	  .append("and year(PayEndDate)-year(CvaliDate) >= 3 ") //对于1年缴保单，13、25月继续率清单均不列；对于2年缴保单，列13个月，不列25个月；对于3年及以上缴费，13、25个月都列。
	  .append(sql1) //保单类型
	  .append(sql2) //销售渠道
	  .append(sql3) //是否孤儿单
	  .append("and CvaliDate between '" + mStartDate + "' and '" + mEndDate + "' ")
	  .append("and ManageCom like '" + mManageCom + "%' ")
	  .append(") as b group by Com4, Com order by Com4,Com with ur");
      
      System.out.println("查询sql：" + tSQL.toString());
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "PrtContContinueBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }

      String[][] tGetData = null;
      tGetData=tSSRS.getAllData();
      
      if(tGetData==null)
      {
    	  CError tError = new CError();
    	  tError.moduleName = "PrtContContinueBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }
      
      System.out.println("拢共有这些条："+tGetData.length);
      
      for (int j = 0; j < tGetData.length;j++) 
      {
//    	  "机构代码", "分公司", "继续率考核保费", "继续率考核件数", "续期实收保费", "续期实收件数", 
//    	  "复效保费", "复效件数", "退保保费", "保费继续率", "件数继续率", "退保率"
    	  double initPrem =  Double.parseDouble(tGetData[j][4]); //继续率考核保费
    	  double initCount =  Double.parseDouble(tGetData[j][5]); //继续率考核件数
    	  double actuPrem =  Double.parseDouble(tGetData[j][6]); //续期实收保费
    	  double actuCount =  Double.parseDouble(tGetData[j][7]); //续期实收件数
    	  double fxPrem =  Double.parseDouble(tGetData[j][8]); //复效保费
    	  double fxCount =  Double.parseDouble(tGetData[j][9]); //复效件数
    	  
    	  double premRate = (actuPrem+fxPrem)/initPrem ; //    	  保费继续率：(续期实收保费+复效保费)/继续率考核保费。
    	  double countRate = (actuCount+fxCount)/initCount ; //    	  件数继续率：(续期实收件数+复效件数)/继续率考核件数。
    	  
    	  tGetData[j][10] = String.valueOf(PubFun.setPrecision(premRate, "0.0000")); //4舍6入5凑偶
    	  tGetData[j][11] = String.valueOf(PubFun.setPrecision(countRate, "0.0000"));
    	  
      }
      
      if(mCreateExcelList.setData(tGetData,displayData)==-1)
      {
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }

      return true;
  }
  
  private boolean getPrintDataULIZ13()
  {
	  //创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      String[][] tTitle = {{"机构代码","分公司","继续率考核保费（基本）","继续率考核保费（额外）","继续率考核保费（全部）","继续率考核件数","续期实收保费（基本）",
    	  "续期实收保费（额外）","续期实收保费（全部）","续期实收件数","退保保费（全部）","保费继续率","件数继续率","退保率"}};
      
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) 
      {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      
//    0|全部^1|个险^2|银保
      String tContType = (String) mTransferData.getValueByName("ContType");
      System.out.println("保单类型："+tContType);
      String sql1 = "";
      if(tContType.equals("0"))
      {
    	  sql1+=" and a.salechnl in ('01','04','10','13','14','15') ";
    	  System.out.println("保单类型：0|全部");  
      }
      else if (tContType.equals("1"))
      {
    	  sql1 = " and a.salechnl in ('01','10') ";
    	  System.out.println("保单类型：1|个险");
      }
      else if (tContType.equals("2"))
      {
    	  sql1 = " and a.salechnl in ('04','13') ";
    	  System.out.println("保单类型：2|银保");
      }
      else if (tContType.equals("3"))
      {
    	  sql1 = " and a.salechnl in ('14','15') ";
    	  System.out.println("保单类型：3|互动");
      }
      //4其他待定
      else
      {
    	  buildError("getPrintData", "保单类型传入错误！");
          return false;
      }
      
//    00|全部^01|个险直销^10|中介代理^04|银行代理^07|职团开拓^08|电话销售^11|产代健^13|银代直销^99|其它
      String tSaleChnl = (String) mTransferData.getValueByName("SaleChnl"); 
      System.out.println("销售渠道："+tSaleChnl);
      String sql2 = "";
      if(tSaleChnl.equals("00"))
      {
    	  sql2 = " and a.SaleChnl in ('01','04','10','13','14','15') ";
      }
//      else if(tSaleChnl.equals("99"))
//      {
//    	  sql2 = " and a.SaleChnl not in ('01','03','04','07','08','10','11','13') ";
//      }
      else
      {
    	  sql2 = " and a.SaleChnl = '" + tSaleChnl + "' ";
      }
      
//保单服务状态 0-全部，1-在职单，2-孤儿单
//      获取孤儿单标志GETORPHANSSTATE(ContNo,AgentCode)
//      --返回 1 是孤儿单，返回 0 不是孤儿单
      String tOrphans = (String) mTransferData.getValueByName("Orphans"); 
      String sql3 = "";
      if(tOrphans.equals("0"))
      {
    	  System.out.println("保单服务状态 0-全部");
      }
      else if(tOrphans.equals("1"))
      {
    	  sql3 = " and GETORPHANSSTATE(a.ContNo,a.AgentCode) = '0' ";
    	  System.out.println("保单服务状态, 1-在职单");
      }
      else if(tOrphans.equals("2"))
      {
    	  sql3 = " and GETORPHANSSTATE(a.ContNo,a.AgentCode) = '1' ";
    	  System.out.println("保单服务状态，2-孤儿单");
      }
      else
      {
    	  buildError("getPrintData", "保单服务状态类型传入错误！");
          return false;
      }
      
//      "机构代码","分公司","继续率考核保费（基本）","继续率考核保费（额外）","继续率考核保费（全部）","继续率考核件数","续期实收保费（基本）",
//	  "续期实收保费（额外）","续期实收保费（全部）","续期实收件数","退保保费（全部）","保费继续率","件数继续率","退保率"
      
      //获得EXCEL列信息      
      StringBuffer tSQL = new StringBuffer();
	  tSQL.append("select Com4, (select Name from LDCom where ComCode = Com4), sum(InitBasePrem),'额外',sum(InitPrem), count(distinct ContNo), ")
	  .append("sum(ActuBasePrem),'额外',sum(ActuPrem), count(distinct(case when ActuPrem <> 0 then ContNo end)), ")
	  .append("sum(TbPrem), '','','' from ( ")
	  .append("select ContNo, substr(ManageCom,1,4) Com4, ")
	  .append("GETINITBASEPREM(a.PolNo) InitBasePrem, ") //继续率考核保费（基本）
	  .append("GETINITPREM(a.PolNo) InitPrem, ")
	  .append("GETACTUBASEPREM(a.PolNo,'13') ActuBasePrem, ")//实收保费（基本）
	  .append("GETACTUPREM(a.PolNo,'13') ActuPrem, ")
	  .append("GETTBPREM(a.PolNo,'13') TbPrem ")
	  .append("from LCPol a where 1=1 ")
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') ")
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') ")
	  .append("and ContType = '1' and AppFlag = '1' and Prem <>0 and PayIntv = 12 " )//PayIntv先设置为年缴的
	  .append("and year(PayEndDate)-year(CvaliDate) >= 2 ") //对于1年缴保单，13、25月继续率清单均不列；对于2年缴保单，列13个月，不列25个月；对于3年及以上缴费，13、25个月都列。
	  .append(sql1) //保单类型
	  .append(sql2) //销售渠道
	  .append(sql3) //是否孤儿单
	  .append("and CvaliDate between '" + mStartDate + "' and '" + mEndDate + "' ")
	  .append("and ManageCom like '" + mManageCom + "%' ")
	  .append(" union all ")
	  .append("select ContNo, substr(ManageCom,1,4) Com4, ")
	  .append("GETINITBASEPREM(a.PolNo) InitBasePrem, ") //继续率考核保费（基本）
	  .append("GETINITPREM(a.PolNo) InitPrem, ")
	  .append("GETACTUBASEPREM(a.PolNo,'13') ActuBasePrem, ")//实收保费（基本）
	  .append("GETACTUPREM(a.PolNo,'13') ActuPrem, ")
	  .append("GETTBPREM(a.PolNo,'13') TbPrem ")
	  .append("from LBPol a where 1=1 ")
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') ")
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') ")
	  .append("and ContType = '1' and Prem <>0 and PayIntv = 12 ") //PayIntv先设置为年缴的
//	  解约和协议退保的，不包含犹豫期退保
	  .append("and exists (select 1 from LPEdorItem where EdorNo = a.EdorNo and EdorType in ('CT','XT')) ")
	  .append("and year(PayEndDate)-year(CvaliDate) >= 2 ") //对于1年缴保单，13、25月继续率清单均不列；对于2年缴保单，列13个月，不列25个月；对于3年及以上缴费，13、25个月都列。
	  .append(sql1) //保单类型
	  .append(sql2) //销售渠道
	  .append(sql3) //是否孤儿单
	  .append("and CvaliDate between '" + mStartDate + "' and '" + mEndDate + "' ")
	  .append("and ManageCom like '" + mManageCom + "%' ")
	  .append(") as b group by Com4 order by Com4 with ur");
      
      System.out.println("查询sql：" + tSQL.toString());
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "PrtContContinueBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }

      String[][] tGetData = null;
      tGetData=tSSRS.getAllData();
      
      if(tGetData==null)
      {
    	  CError tError = new CError();
    	  tError.moduleName = "PrtContContinueBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }
      
      System.out.println("拢共有这些条："+tGetData.length);
      
      for (int j = 0; j < tGetData.length;j++) 
      {
//        "机构代码","分公司","继续率考核保费（基本）","继续率考核保费（额外）","继续率考核保费（全部）","继续率考核件数","续期实收保费（基本）",
//    	  "续期实收保费（额外）","续期实收保费（全部）","续期实收件数","退保保费（全部）","保费继续率","件数继续率","退保率"
    	  
    	  double initPrem =  Double.parseDouble(tGetData[j][4]); //继续率考核保费
    	  double initBasePrem =  Double.parseDouble(tGetData[j][2]); //继续率考核保费（基本）
    	  double initAddPrem =  initPrem-initBasePrem; //继续率考核保费（额外）
    	  tGetData[j][3] = String.valueOf(initAddPrem); 
    	  double initCount =  Double.parseDouble(tGetData[j][5]); //继续率考核件数
    	  double actuPrem =  Double.parseDouble(tGetData[j][8]); //续期实收保费
    	  double actuBasePrem =  Double.parseDouble(tGetData[j][6]); //续期实收保费（基本）
    	  double actuAddPrem =  actuPrem - actuBasePrem; //续期实收保费（额外）
    	  tGetData[j][7] = String.valueOf(actuAddPrem); 
    	  double actuCount =  Double.parseDouble(tGetData[j][9]); //续期实收件数
    	  double tbPrem =  Double.parseDouble(tGetData[j][10]); //退保保费
    	  
    	  double premRate = actuPrem/initPrem ; //    	  保费继续率：(续期实收保费+复效保费)/继续率考核保费。
    	  double countRate = actuCount/initCount ; //    	  件数继续率：(续期实收件数+复效件数)/继续率考核件数。
    	  double tbRate = (tbPrem)/initPrem ;//    	  退保率：退保保费/继续率考核保费，13个月继续率统计，25个月继续率不统计。
    	  
    	  tGetData[j][11] = String.valueOf(PubFun.setPrecision(premRate, "0.0000")); //4舍6入5凑偶
    	  tGetData[j][12] = String.valueOf(PubFun.setPrecision(countRate, "0.0000"));
    	  tGetData[j][13] = String.valueOf(PubFun.setPrecision(tbRate, "0.0000"));

      }
      
      if(mCreateExcelList.setData(tGetData,displayData)==-1)
      {
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }

      return true;
  }
  
  private boolean getPrintDataULIZ25()
  {
//	创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      String[][] tTitle = {{"机构代码","分公司","继续率考核保费（基本）","继续率考核保费（额外）","继续率考核保费（全部）","继续率考核件数","续期实收保费（基本）",
    	  "续期实收保费（额外）","续期实收保费（全部）","续期实收件数","保费继续率","件数继续率",}};
      
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) 
      {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      
//    0|全部^1|个险^2|银保
      String tContType = (String) mTransferData.getValueByName("ContType");
      System.out.println("保单类型："+tContType);
      String sql1 = "";
      if(tContType.equals("0"))
      {
    	  sql1+=" and a.salechnl in ('01','04','10','13','14','15') ";
    	  System.out.println("保单类型：0|全部");  
      }
      else if (tContType.equals("1"))
      {
    	  sql1 = " and a.salechnl in ('01','10') ";
    	  System.out.println("保单类型：1|个险");
      }
      else if (tContType.equals("2"))
      {
    	  sql1 = " and a.salechnl in ('04','13') ";
    	  System.out.println("保单类型：2|银保");
      }
      else if (tContType.equals("3"))
      {
    	  sql1 = " and a.salechnl in ('14','15') ";
    	  System.out.println("保单类型：3|互动");
      }
      //4其他待定
      else
      {
    	  buildError("getPrintData", "保单类型传入错误！");
          return false;
      }
      
//    00|全部^01|个险直销^10|中介代理^04|银行代理^07|职团开拓^08|电话销售^11|产代健^13|银代直销^99|其它
      String tSaleChnl = (String) mTransferData.getValueByName("SaleChnl"); 
      System.out.println("销售渠道："+tSaleChnl);
      String sql2 = "";
      if(tSaleChnl.equals("00"))
      {
    	  sql2 = " and a.SaleChnl in ('01','04','10','13','14','15') ";
      }
//      else if(tSaleChnl.equals("99"))
//      {
//    	  sql2 = " and a.SaleChnl not in ('01','10','04','07','08','10','11','13') ";
//      }
      else
      {
    	  sql2 = " and a.SaleChnl = '" + tSaleChnl + "' ";
      }
      
//保单服务状态 0-全部，1-在职单，2-孤儿单
//      获取孤儿单标志GETORPHANSSTATE(ContNo,AgentCode)
//      --返回 1 是孤儿单，返回 0 不是孤儿单
      String tOrphans = (String) mTransferData.getValueByName("Orphans"); 
      String sql3 = "";
      if(tOrphans.equals("0"))
      {
    	  System.out.println("保单服务状态 0-全部");
      }
      else if(tOrphans.equals("1"))
      {
    	  sql3 = " and GETORPHANSSTATE(a.ContNo,a.AgentCode) = '0' ";
    	  System.out.println("保单服务状态, 1-在职单");
      }
      else if(tOrphans.equals("2"))
      {
    	  sql3 = " and GETORPHANSSTATE(a.ContNo,a.AgentCode) = '1' ";
    	  System.out.println("保单服务状态，2-孤儿单");
      }
      else
      {
    	  buildError("getPrintData", "保单服务状态类型传入错误！");
          return false;
      }
      
//    "机构代码","分公司","继续率考核保费（基本）","继续率考核保费（额外）","继续率考核保费（全部）","继续率考核件数","续期实收保费（基本）",
//	  "续期实收保费（额外）","续期实收保费（全部）","续期实收件数","退保保费（全部）","保费继续率","件数继续率","退保率"
      
      //获得EXCEL列信息      
      StringBuffer tSQL = new StringBuffer();
	  tSQL.append("select Com4, (select Name from LDCom where ComCode = Com4), sum(InitBasePrem),'额外',sum(InitPrem), count(distinct ContNo), ")
	  .append("sum(ActuBasePrem),'额外',sum(ActuPrem), count(distinct(case when ActuPrem <> 0 then ContNo end)), ")
	  .append(" '','' from ( ")
	  .append("select ContNo, substr(ManageCom,1,4) Com4, ")
	  .append("GETINITBASEPREM(a.PolNo) InitBasePrem, ") //继续率考核保费（基本）
	  .append("GETINITPREM(a.PolNo) InitPrem, ")
	  .append("GETACTUBASEPREM(a.PolNo,'25') ActuBasePrem, ")//实收保费（基本）
	  .append("GETACTUPREM(a.PolNo,'25') ActuPrem ")
	  .append("from LCPol a where 1=1 ")
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') ")
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') ")
	  .append("and ContType = '1' and AppFlag = '1' and Prem <>0 and PayIntv = 12 " )//PayIntv先设置为年缴的
	  .append("and year(PayEndDate)-year(CvaliDate) >= 3 ") //对于1年缴保单，13、25月继续率清单均不列；对于2年缴保单，列13个月，不列25个月；对于3年及以上缴费，13、25个月都列。
	  .append(sql1) //保单类型
	  .append(sql2) //销售渠道
	  .append(sql3) //是否孤儿单
	  .append("and CvaliDate between '" + mStartDate + "' and '" + mEndDate + "' ")
	  .append("and ManageCom like '" + mManageCom + "%' ")
	  .append(" union all ")
	  .append("select ContNo, substr(ManageCom,1,4) Com4, ")
	  .append("GETINITBASEPREM(a.PolNo) InitBasePrem, ") //继续率考核保费（基本）
	  .append("GETINITPREM(a.PolNo) InitPrem, ")
	  .append("GETACTUBASEPREM(a.PolNo,'25') ActuBasePrem, ")//实收保费（基本）
	  .append("GETACTUPREM(a.PolNo,'25') ActuPrem ")
	  .append("from LBPol a where 1=1 ")
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') ")
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') ")
	  .append("and ContType = '1' and Prem <>0 and PayIntv = 12 ") //PayIntv先设置为年缴的
//	  解约和协议退保的，不包含犹豫期退保
	  .append("and exists (select 1 from LPEdorItem where EdorNo = a.EdorNo and EdorType in ('CT','XT')) ")
	  .append("and year(PayEndDate)-year(CvaliDate) >= 3 ") //对于1年缴保单，13、25月继续率清单均不列；对于2年缴保单，列13个月，不列25个月；对于3年及以上缴费，13、25个月都列。
	  .append(sql1) //保单类型
	  .append(sql2) //销售渠道
	  .append(sql3) //是否孤儿单
	  .append("and CvaliDate between '" + mStartDate + "' and '" + mEndDate + "' ")
	  .append("and ManageCom like '" + mManageCom + "%' ")
	  .append(") as b group by Com4 order by Com4 with ur");
      
      System.out.println("查询sql：" + tSQL.toString());
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "PrtContContinueBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }

      String[][] tGetData = null;
      tGetData=tSSRS.getAllData();
      
      if(tGetData==null)
      {
    	  CError tError = new CError();
    	  tError.moduleName = "PrtContContinueBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }
      
      System.out.println("拢共有这些条："+tGetData.length);
      
      for (int j = 0; j < tGetData.length;j++) 
      {
//        "机构代码","分公司","继续率考核保费（基本）","继续率考核保费（额外）","继续率考核保费（全部）","继续率考核件数","续期实收保费（基本）",
//    	  "续期实收保费（额外）","续期实收保费（全部）","续期实收件数", "保费继续率","件数继续率"
    	  
    	  double initPrem =  Double.parseDouble(tGetData[j][4]); //继续率考核保费
    	  double initBasePrem =  Double.parseDouble(tGetData[j][2]); //继续率考核保费（基本）
    	  double initAddPrem =  initPrem-initBasePrem; //继续率考核保费（额外）
    	  tGetData[j][3] = String.valueOf(initAddPrem); 
    	  double initCount =  Double.parseDouble(tGetData[j][5]); //继续率考核件数
    	  double actuPrem =  Double.parseDouble(tGetData[j][8]); //续期实收保费
    	  double actuBasePrem =  Double.parseDouble(tGetData[j][6]); //续期实收保费（基本）
    	  double actuAddPrem =  actuPrem - actuBasePrem; //续期实收保费（额外）
    	  tGetData[j][7] = String.valueOf(actuAddPrem); 
    	  double actuCount =  Double.parseDouble(tGetData[j][9]); //续期实收件数
    	      	  
    	  double premRate = actuPrem/initPrem ; //    	  保费继续率：(续期实收保费+复效保费)/继续率考核保费。
    	  double countRate = actuCount/initCount ; //    	  件数继续率：(续期实收件数+复效件数)/继续率考核件数。
    	  
    	  tGetData[j][10] = String.valueOf(PubFun.setPrecision(premRate, "0.0000")); //4舍6入5凑偶
    	  tGetData[j][11] = String.valueOf(PubFun.setPrecision(countRate, "0.0000"));

      }
      
      if(mCreateExcelList.setData(tGetData,displayData)==-1)
      {
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }

      return true;
  }
  
  private boolean getPrintDataULIF13()
  {
//	创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      String[][] tTitle = {{"机构代码","分公司","支公司代码","支公司","继续率考核保费（基本）","继续率考核保费（额外）","继续率考核保费（全部）","继续率考核件数","续期实收保费（基本）",
    	  "续期实收保费（额外）","续期实收保费（全部）","续期实收件数","退保保费（全部）","保费继续率","件数继续率","退保率"}};
      
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) 
      {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      
//    0|全部^1|个险^2|银保
      String tContType = (String) mTransferData.getValueByName("ContType");
      System.out.println("保单类型："+tContType);
      String sql1 = "";
      if(tContType.equals("0"))
      {
    	  sql1+=" and a.salechnl in ('01','04','10','13','14','15') ";
    	  System.out.println("保单类型：0|全部");  
      }
      else if (tContType.equals("1"))
      {
    	  sql1 = " and a.salechnl in ('01','10') ";
    	  System.out.println("保单类型：1|个险");
      }
      else if (tContType.equals("2"))
      {
    	  sql1 = " and a.salechnl in ('04','13') ";
    	  System.out.println("保单类型：2|银保");
      }
      else if (tContType.equals("3"))
      {
    	  sql1 = " and a.salechnl in ('14','15') ";
    	  System.out.println("保单类型：3|互动");
      }
      //4其他待定
      else
      {
    	  buildError("getPrintData", "保单类型传入错误！");
          return false;
      }
      
//    00|全部^01|个险直销^10|中介代理^04|银行代理^07|职团开拓^08|电话销售^11|产代健^13|银代直销^99|其它
      String tSaleChnl = (String) mTransferData.getValueByName("SaleChnl"); 
      System.out.println("销售渠道："+tSaleChnl);
      String sql2 = "";
      if(tSaleChnl.equals("00"))
      {
    	  sql2 = " and a.SaleChnl in ('01','04','10','13','14','15') ";
      }
//      else if(tSaleChnl.equals("99"))
//      {
//    	  sql2 = " and a.SaleChnl not in ('01','10','04','07','08','10','11','13') ";
//      }
      else
      {
    	  sql2 = " and a.SaleChnl = '" + tSaleChnl + "' ";
      }
      
//保单服务状态 0-全部，1-在职单，2-孤儿单
//      获取孤儿单标志GETORPHANSSTATE(ContNo,AgentCode)
//      --返回 1 是孤儿单，返回 0 不是孤儿单
      String tOrphans = (String) mTransferData.getValueByName("Orphans"); 
      String sql3 = "";
      if(tOrphans.equals("0"))
      {
    	  System.out.println("保单服务状态 0-全部");
      }
      else if(tOrphans.equals("1"))
      {
    	  sql3 = " and GETORPHANSSTATE(a.ContNo,a.AgentCode) = '0' ";
    	  System.out.println("保单服务状态, 1-在职单");
      }
      else if(tOrphans.equals("2"))
      {
    	  sql3 = " and GETORPHANSSTATE(a.ContNo,a.AgentCode) = '1' ";
    	  System.out.println("保单服务状态，2-孤儿单");
      }
      else
      {
    	  buildError("getPrintData", "保单服务状态类型传入错误！");
          return false;
      }
      
//      "机构代码","分公司","支公司代码","支公司","继续率考核保费（基本）","继续率考核保费（额外）","继续率考核保费（全部）","继续率考核件数","续期实收保费（基本）",
//	  "续期实收保费（额外）","续期实收保费（全部）","续期实收件数","退保保费（全部）","保费继续率","件数继续率","退保率"
      
      //获得EXCEL列信息      
      StringBuffer tSQL = new StringBuffer();
	  tSQL.append("select Com4, (select Name from LDCom where ComCode = Com4), Com, (select Name from LDCom where ComCode = Com),")
	  .append("sum(InitBasePrem),'额外',sum(InitPrem), count(distinct ContNo), ")
	  .append("sum(ActuBasePrem),'额外',sum(ActuPrem), count(distinct(case when ActuPrem <> 0 then ContNo end)), ")
	  .append("sum(TbPrem), '','','' from ( ")
	  .append("select ContNo, substr(ManageCom,1,4) Com4, ManageCom Com, ")
	  .append("GETINITBASEPREM(a.PolNo) InitBasePrem, ") //继续率考核保费（基本）
	  .append("GETINITPREM(a.PolNo) InitPrem, ")
	  .append("GETACTUBASEPREM(a.PolNo,'13') ActuBasePrem, ")//实收保费（基本）
	  .append("GETACTUPREM(a.PolNo,'13') ActuPrem, ")
	  .append("GETTBPREM(a.PolNo,'13') TbPrem ")
	  .append("from LCPol a where 1=1 ")
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') ")
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') ")
	  .append("and ContType = '1' and AppFlag = '1' and Prem <>0 and PayIntv = 12 " )//PayIntv先设置为年缴的
	  .append("and year(PayEndDate)-year(CvaliDate) >= 2 ") //对于1年缴保单，13、25月继续率清单均不列；对于2年缴保单，列13个月，不列25个月；对于3年及以上缴费，13、25个月都列。
	  .append(sql1) //保单类型
	  .append(sql2) //销售渠道
	  .append(sql3) //是否孤儿单
	  .append("and CvaliDate between '" + mStartDate + "' and '" + mEndDate + "' ")
	  .append("and ManageCom like '" + mManageCom + "%' ")
	  .append(" union all ")
	  .append("select ContNo, substr(ManageCom,1,4) Com4, ManageCom Com, ")
	  .append("GETINITBASEPREM(a.PolNo) InitBasePrem, ") //继续率考核保费（基本）
	  .append("GETINITPREM(a.PolNo) InitPrem, ")
	  .append("GETACTUBASEPREM(a.PolNo,'13') ActuBasePrem, ")//实收保费（基本）
	  .append("GETACTUPREM(a.PolNo,'13') ActuPrem, ")
	  .append("GETTBPREM(a.PolNo,'13') TbPrem ")
	  .append("from LBPol a where 1=1 ")
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') ")
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') ")
	  .append("and ContType = '1' and Prem <>0 and PayIntv = 12 ") //PayIntv先设置为年缴的
//	  解约和协议退保的，不包含犹豫期退保
	  .append("and exists (select 1 from LPEdorItem where EdorNo = a.EdorNo and EdorType in ('CT','XT')) ")
	  .append("and year(PayEndDate)-year(CvaliDate) >= 2 ") //对于1年缴保单，13、25月继续率清单均不列；对于2年缴保单，列13个月，不列25个月；对于3年及以上缴费，13、25个月都列。
	  .append(sql1) //保单类型
	  .append(sql2) //销售渠道
	  .append(sql3) //是否孤儿单
	  .append("and CvaliDate between '" + mStartDate + "' and '" + mEndDate + "' ")
	  .append("and ManageCom like '" + mManageCom + "%' ")
	  .append(") as b group by Com4,Com order by Com4,Com with ur");
      
      System.out.println("查询sql：" + tSQL.toString());
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "PrtContContinueBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }

      String[][] tGetData = null;
      tGetData=tSSRS.getAllData();
      
      if(tGetData==null)
      {
    	  CError tError = new CError();
    	  tError.moduleName = "PrtContContinueBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }
      
      System.out.println("拢共有这些条："+tGetData.length);
      
      for (int j = 0; j < tGetData.length;j++) 
      {
//        "机构代码","分公司","支公司代码","支公司","继续率考核保费（基本）","继续率考核保费（额外）","继续率考核保费（全部）","继续率考核件数","续期实收保费（基本）",
//    	  "续期实收保费（额外）","续期实收保费（全部）","续期实收件数","退保保费（全部）","保费继续率","件数继续率","退保率"
    	  
    	  double initPrem =  Double.parseDouble(tGetData[j][6]); //继续率考核保费
    	  double initBasePrem =  Double.parseDouble(tGetData[j][4]); //继续率考核保费（基本）
    	  double initAddPrem =  initPrem-initBasePrem; //继续率考核保费（额外）
    	  tGetData[j][5] = String.valueOf(initAddPrem); 
    	  double initCount =  Double.parseDouble(tGetData[j][7]); //继续率考核件数
    	  double actuPrem =  Double.parseDouble(tGetData[j][10]); //续期实收保费
    	  double actuBasePrem =  Double.parseDouble(tGetData[j][8]); //续期实收保费（基本）
    	  double actuAddPrem =  actuPrem - actuBasePrem; //续期实收保费（额外）
    	  tGetData[j][9] = String.valueOf(actuAddPrem); 
    	  double actuCount =  Double.parseDouble(tGetData[j][11]); //续期实收件数
    	  double tbPrem =  Double.parseDouble(tGetData[j][12]); //退保保费
    	  
    	  double premRate = actuPrem/initPrem ; //    	  保费继续率：(续期实收保费+复效保费)/继续率考核保费。
    	  double countRate = actuCount/initCount ; //    	  件数继续率：(续期实收件数+复效件数)/继续率考核件数。
    	  double tbRate = (tbPrem)/initPrem ;//    	  退保率：退保保费/继续率考核保费，13个月继续率统计，25个月继续率不统计。
    	  
    	  tGetData[j][13] = String.valueOf(PubFun.setPrecision(premRate, "0.0000")); //4舍6入5凑偶
    	  tGetData[j][14] = String.valueOf(PubFun.setPrecision(countRate, "0.0000"));
    	  tGetData[j][15] = String.valueOf(PubFun.setPrecision(tbRate, "0.0000"));

      }
      
      if(mCreateExcelList.setData(tGetData,displayData)==-1)
      {
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }

      return true;
  }
  
  private boolean getPrintDataULIF25()
  {
//		创建EXCEL列表
      mCreateExcelList.createExcelFile();
      String[] sheetName ={"list"};
      mCreateExcelList.addSheet(sheetName);
      
      //设置表头
      String[][] tTitle = {{"机构代码","分公司","支公司代码","支公司","继续率考核保费（基本）","继续率考核保费（额外）","继续率考核保费（全部）","继续率考核件数","续期实收保费（基本）",
    	  "续期实收保费（额外）","续期实收保费（全部）","续期实收件数","保费继续率","件数继续率"}};
      
//    表头的显示属性
      int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
//    数据的显示属性
      int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
     
      int row = mCreateExcelList.setData(tTitle,displayTitle);
      if(row ==-1) 
      {
    	  buildError("getPrintData", "EXCEL中指定数据失败！");
          return false;
      }
      
//    0|全部^1|个险^2|银保
      String tContType = (String) mTransferData.getValueByName("ContType");
      System.out.println("保单类型："+tContType);
      String sql1 = "";
      if(tContType.equals("0"))
      {
    	  sql1+=" and a.salechnl in ('01','04','10','13','14','15') ";
    	  System.out.println("保单类型：0|全部");  
      }
      else if (tContType.equals("1"))
      {
    	  sql1 = " and a.salechnl in ('01','10') ";
    	  System.out.println("保单类型：1|个险");
      }
      else if (tContType.equals("2"))
      {
    	  sql1 = " and a.salechnl in ('04','13') ";
    	  System.out.println("保单类型：2|银保");
      }
      else if (tContType.equals("3"))
      {
    	  sql1 = " and a.salechnl in ('14','15') ";
    	  System.out.println("保单类型：3|互动");
      }
      //4其他待定
      else
      {
    	  buildError("getPrintData", "保单类型传入错误！");
          return false;
      }
      
//    00|全部^01|个险直销^10|中介代理^04|银行代理^07|职团开拓^08|电话销售^11|产代健^13|银代直销^99|其它
      String tSaleChnl = (String) mTransferData.getValueByName("SaleChnl"); 
      System.out.println("销售渠道："+tSaleChnl);
      String sql2 = "";
      if(tSaleChnl.equals("00"))
      {
    	  sql2 = " and a.SaleChnl in ('01','04','10','13','14','15') ";
      }
//      else if(tSaleChnl.equals("99"))
//      {
//    	  sql2 = " and a.SaleChnl not in ('01','03','04','07','08','10','11','13') ";
//      }
      else
      {
    	  sql2 = " and a.SaleChnl = '" + tSaleChnl + "' ";
      }
      
//保单服务状态 0-全部，1-在职单，2-孤儿单
//      获取孤儿单标志GETORPHANSSTATE(ContNo,AgentCode)
//      --返回 1 是孤儿单，返回 0 不是孤儿单
      String tOrphans = (String) mTransferData.getValueByName("Orphans"); 
      String sql3 = "";
      if(tOrphans.equals("0"))
      {
    	  System.out.println("保单服务状态 0-全部");
      }
      else if(tOrphans.equals("1"))
      {
    	  sql3 = " and GETORPHANSSTATE(a.ContNo,a.AgentCode) = '0' ";
    	  System.out.println("保单服务状态, 1-在职单");
      }
      else if(tOrphans.equals("2"))
      {
    	  sql3 = " and GETORPHANSSTATE(a.ContNo,a.AgentCode) = '1' ";
    	  System.out.println("保单服务状态，2-孤儿单");
      }
      else
      {
    	  buildError("getPrintData", "保单服务状态类型传入错误！");
          return false;
      }
      
//      "机构代码","分公司","支公司代码","支公司","继续率考核保费（基本）","继续率考核保费（额外）","继续率考核保费（全部）","继续率考核件数","续期实收保费（基本）",
//	  "续期实收保费（额外）","续期实收保费（全部）","续期实收件数", "保费继续率","件数继续率"
      
      //获得EXCEL列信息      
      StringBuffer tSQL = new StringBuffer();
	  tSQL.append("select Com4, (select Name from LDCom where ComCode = Com4), Com, (select Name from LDCom where ComCode = Com),")
	  .append("sum(InitBasePrem),'额外',sum(InitPrem), count(distinct ContNo), ")
	  .append("sum(ActuBasePrem),'额外',sum(ActuPrem), count(distinct(case when ActuPrem <> 0 then ContNo end)), ")
	  .append("'','' from ( ")
	  .append("select ContNo, substr(ManageCom,1,4) Com4, ManageCom Com, ")
	  .append("GETINITBASEPREM(a.PolNo) InitBasePrem, ") //继续率考核保费（基本）
	  .append("GETINITPREM(a.PolNo) InitPrem, ")
	  .append("GETACTUBASEPREM(a.PolNo,'25') ActuBasePrem, ")//实收保费（基本）
	  .append("GETACTUPREM(a.PolNo,'25') ActuPrem ")
	  .append("from LCPol a where 1=1 ")
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') ")
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') ")
	  .append("and ContType = '1' and AppFlag = '1' and Prem <>0 and PayIntv = 12 " )//PayIntv先设置为年缴的
	  .append("and year(PayEndDate)-year(CvaliDate) >= 3 ") //对于1年缴保单，13、25月继续率清单均不列；对于2年缴保单，列13个月，不列25个月；对于3年及以上缴费，13、25个月都列。
	  .append(sql1) //保单类型
	  .append(sql2) //销售渠道
	  .append(sql3) //是否孤儿单
	  .append("and CvaliDate between '" + mStartDate + "' and '" + mEndDate + "' ")
	  .append("and ManageCom like '" + mManageCom + "%' ")
	  .append(" union all ")
	  .append("select ContNo, substr(ManageCom,1,4) Com4, ManageCom Com, ")
	  .append("GETINITBASEPREM(a.PolNo) InitBasePrem, ") //继续率考核保费（基本）
	  .append("GETINITPREM(a.PolNo) InitPrem, ")
	  .append("GETACTUBASEPREM(a.PolNo,'25') ActuBasePrem, ")//实收保费（基本）
	  .append("GETACTUPREM(a.PolNo,'25') ActuPrem ")
	  .append("from LBPol a where 1=1 ")
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskPeriod = 'L') ")
	  .append("and exists (select 1 from LMRiskApp where RiskCode = a.RiskCode and RiskType4 = '4') ")
	  .append("and ContType = '1' and Prem <>0 and PayIntv = 12 ") //PayIntv先设置为年缴的
//	  解约和协议退保的，不包含犹豫期退保
	  .append("and exists (select 1 from LPEdorItem where EdorNo = a.EdorNo and EdorType in ('CT','XT')) ")
	  .append("and year(PayEndDate)-year(CvaliDate) >= 3 ") //对于1年缴保单，13、25月继续率清单均不列；对于2年缴保单，列13个月，不列25个月；对于3年及以上缴费，13、25个月都列。
	  .append(sql1) //保单类型
	  .append(sql2) //销售渠道
	  .append(sql3) //是否孤儿单
	  .append("and CvaliDate between '" + mStartDate + "' and '" + mEndDate + "' ")
	  .append("and ManageCom like '" + mManageCom + "%' ")
	  .append(") as b group by Com4,Com order by Com4,Com with ur");
      
      System.out.println("查询sql：" + tSQL.toString());
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS = tExeSQL.execSQL(tSQL.toString());
      if (tExeSQL.mErrors.needDealError()) 
      {
    	  CError tError = new CError();
    	  tError.moduleName = "PrtContContinueBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要下载的数据";
    	  mErrors.addOneError(tError);
    	  return false;
      }

      String[][] tGetData = null;
      tGetData=tSSRS.getAllData();
      
      if(tGetData==null)
      {
    	  CError tError = new CError();
    	  tError.moduleName = "PrtContContinueBL";
    	  tError.functionName = "getPrintData";
    	  tError.errorMessage = "没有查询到需要输出的数据";
    	  return false;
      }
      
      System.out.println("拢共有这些条："+tGetData.length);
      
      for (int j = 0; j < tGetData.length;j++) 
      {
//        "机构代码","分公司","支公司代码","支公司","继续率考核保费（基本）","继续率考核保费（额外）","继续率考核保费（全部）","继续率考核件数","续期实收保费（基本）",
//    	  "续期实收保费（额外）","续期实收保费（全部）","续期实收件数","保费继续率","件数继续率"
    	  
    	  double initPrem =  Double.parseDouble(tGetData[j][6]); //继续率考核保费
    	  double initBasePrem =  Double.parseDouble(tGetData[j][4]); //继续率考核保费（基本）
    	  double initAddPrem =  initPrem-initBasePrem; //继续率考核保费（额外）
    	  tGetData[j][5] = String.valueOf(initAddPrem); 
    	  double initCount =  Double.parseDouble(tGetData[j][7]); //继续率考核件数
    	  double actuPrem =  Double.parseDouble(tGetData[j][10]); //续期实收保费
    	  double actuBasePrem =  Double.parseDouble(tGetData[j][8]); //续期实收保费（基本）
    	  double actuAddPrem =  actuPrem - actuBasePrem; //续期实收保费（额外）
    	  tGetData[j][9] = String.valueOf(actuAddPrem); 
    	  double actuCount =  Double.parseDouble(tGetData[j][11]); //续期实收件数
    	  
    	  double premRate = actuPrem/initPrem ; //    	  保费继续率：(续期实收保费+复效保费)/继续率考核保费。
    	  double countRate = actuCount/initCount ; //    	  件数继续率：(续期实收件数+复效件数)/继续率考核件数。
    	  
    	  tGetData[j][12] = String.valueOf(PubFun.setPrecision(premRate, "0.0000")); //4舍6入5凑偶
    	  tGetData[j][13] = String.valueOf(PubFun.setPrecision(countRate, "0.0000"));

      }
      
      if(mCreateExcelList.setData(tGetData,displayData)==-1)
      {
    	  buildError("getPrintData", "EXCEL中设置数据失败！");
          return false;
      }

      return true;
  }
}

