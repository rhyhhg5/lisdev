package com.sinosoft.lis.operfee;

import com.sinosoft.utility.VData;
import com.sinosoft.utility.CErrors;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class ExpirBenefitChangePayModeUI
{
    public CErrors mErrors = new CErrors();

    public ExpirBenefitChangePayModeUI()
    {
    }

    /**
     * 提交变更给付方式接口
     * @param data VData
     * @param operate String
     * @return boolean
     */
    public boolean submitData(VData data, String operate)
    {
        ExpirBenefitChangePayModeBL bl = new ExpirBenefitChangePayModeBL();

        if(!bl.submitData(data, operate))
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }

        return true;
    }


    public static void main(String[] args)
    {
        ExpirBenefitChangePayModeUI expirbenefitchangepaymodeui = new
            ExpirBenefitChangePayModeUI();
    }
}
