package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;
import java.lang.String;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class GrpPhoneVisitOverBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private GlobalInput tGI = new GlobalInput();

    /** 数据操作字符串 */

    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private TransferData mTransferData = new TransferData();
    /** 数据表  保存数据*/

    //应收团体交费表
    private LGWorkSet mLGWorkSet = new LGWorkSet();
    //客户号
    private String mCustomerNo;

    //缴费通知书
    private MMap map = new MMap();
    public GrpPhoneVisitOverBL() {
    }

    public static void main(String[] args) {
        GrpPhoneVisitOverBL GrpPhoneVisitOverBL1 = new GrpPhoneVisitOverBL();
        GlobalInput tGt = new GlobalInput();
        tGt.ComCode = "86";
        tGt.Operator = "001";
        tGt.ManageCom = "86";

        TransferData tempTransferData = new TransferData();
        tempTransferData.setNameAndValue("StartDate", "2005-01-01");

        VData tVData = new VData();

        tVData.add(tGt);
        tVData.add(tempTransferData);
        GrpPhoneVisitOverBL1.submitData(tVData, "INSERT");
    }


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {

        if (!getInputData(cInputData)) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        if (!prepareData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(mInputData, "") == false) {
            System.out.println("delete fail!");
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) {
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        if (tGI == null || mTransferData == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpPhoneVisitOverBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * Yangh于2005-07-19创建新的dealData的处理逻辑
     * @return boolean
     */
    private boolean dealData() {

        //获取从前台页面传入的数据
        mCustomerNo = (String) mTransferData.getValueByName("CustomerNo");
        String strOperator = (String) tGI.Operator;

        //个人保单
        //下面处理lccont,即团单下个人的信息 主要是更新一些字段信息
        StringBuffer sqlSB = new StringBuffer();
        String sqlCons = "";
        if (!(mCustomerNo == null || mCustomerNo.equals(""))) {
            sqlCons = " and w.CustomerNo='" + mCustomerNo + "'";
        }

        sqlSB.append( "select w.*")
                .append(" from LGWork w, LGWorkTrace t  ")
                .append(" where  w.WorkNo = t.WorkNo  	 and    w.NodeNo = t.NodeNo  ")
                .append(" and  t.WorkBoxNo = (select WorkBoxNo from LGWorkBox where  OwnerTypeNo = '2' and OwnerNo = '")
                .append(strOperator)
                .append("')")
                .append("  and w.TypeNo='070014'")
                .append(sqlCons)
                .append(" union ")
                .append(" select w.*")
                .append(" from LGWork w, LGWorkTrace t  ")
                .append( " where  w.WorkNo = t.WorkNo  	 and    w.NodeNo = t.NodeNo  ")
                .append(" and  t.WorkBoxNo = (select WorkBoxNo from LGWorkBox where  OwnerTypeNo = '2' and OwnerNo = '")
                .append(strOperator)
                .append("')")
                .append("  and w.TypeNo='070014'")
                .append(sqlCons)
                .append("and (typeNo not like '03'   or customerNo is null)")

                ;
        String tSQL = sqlSB.toString();

        LGWorkDB tLGWorkDB = new LGWorkDB();
        LGWorkSet tLGWorkSet = new LGWorkSet();
        tLGWorkSet = tLGWorkDB.executeQuery(tSQL);
        if (tLGWorkSet.size() == 0) {

            CError.buildErr(this, "投保人：" + mCustomerNo + "下的工单信息缺失，不能结案！");
            return false;
        }
        for (int t = 1; t <= tLGWorkSet.size(); t++) {
            LGWorkSchema tLGWorkSchema = new LGWorkSchema();
            tLGWorkSchema.setSchema(tLGWorkSet.get(t).getSchema());
            tLGWorkSchema.setStatusNo("5");
            tLGWorkSchema.setModifyDate(CurrentDate); //最后一次修改日期
            tLGWorkSchema.setModifyTime(CurrentTime);
            mLGWorkSet.add(tLGWorkSchema);
        }

        return true;
    }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData() {
        mInputData = new VData();
        return true;
    }

    public boolean prepareData() {

        map.put(mLGWorkSet, "UPDATE");

        mInputData.add(map);
        return true;
    }


}
