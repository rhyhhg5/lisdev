package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;

public class GrpDueFeeCanBL  {
  public  CErrors mErrors=new CErrors();
  private VData mData ;
  private String mOperate;
  private GlobalInput mGI;
  private LCGrpContSchema mLCGrpContSchema=new LCGrpContSchema();
  private LJSPaySchema mLJSPaySchema=new LJSPaySchema();
  private LJSPayBSchema mLJSPayBSchema=new LJSPayBSchema();
  private LJSPayGrpSet mLJSPayGrpSet=new LJSPayGrpSet();
  private LJSPayGrpBSet mLJSPayGrpBSet=new LJSPayGrpBSet();
  private LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet();
  private LJSPayPersonBSet mLJSPayPersonBSet = new LJSPayPersonBSet();
  private LOPRTManagerSchema mLOPRTManagerSchema=new LOPRTManagerSchema();
  private LOPRTManagerSubSet mLOPRTManagerSubSet=new LOPRTManagerSubSet();
  private MMap mMMap = new MMap();
  private VData mResult = new VData();
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  public GrpDueFeeCanBL()
  {
  }

  public static void main(String[] args)
  {
    GlobalInput tGI = new GlobalInput();
    tGI.ComCode = "86";
    tGI.Operator = "001";
    tGI.ManageCom="86";

    LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
    tLCGrpContSchema.setGrpContNo("9028000000005588");
   VData tVData = new VData();
   tVData.add(tLCGrpContSchema);
   tVData.add(tGI);
   GrpDueFeeCanBL tGrpDueFeeCanUI = new GrpDueFeeCanBL();
   tGrpDueFeeCanUI.submitData(tVData,"DELETE");

  }

  public boolean submitData(VData cInputData,String cOperate)
  {
    this.mOperate =cOperate;
    if (!getInputData(cInputData))
      return false;
    if(!checkData())
      return false;
    if(!prepareData())
      return false;
    if(!dealData())
      return false;
    return true;
  }

  private boolean getInputData(VData cInputData)
  {
    mGI=((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
    mLCGrpContSchema=((LCGrpContSchema)cInputData.getObjectByObjectName("LCGrpContSchema",0));

    if(mGI==null || mLCGrpContSchema ==null )
    {
      CError.buildErr(this,"没有得到足够的数据，请您确认!");
      return false;
    }
    return true;
  }
  private boolean prepareData()
  {
    mMMap.put(mLJSPaySchema, "DELETE");
    mMMap.put(mLJSPayGrpSet, "DELETE");
    mMMap.put(mLJSPayPersonSet, "DELETE");
    //mMMap.put(mLJSPayBSchema, "DELETE");
    //mMMap.put(mLJSPayGrpBSet, "DELETE");
    //mMMap.put(mLJSPayPersonBSet, "DELETE");
    mMMap.put(mLOPRTManagerSchema, "DELETE");
    mMMap.put(mLOPRTManagerSubSet, "DELETE");
    mResult.add(mMMap);
    return true;
  }
  /**
   * 杨红于2005-07-21添加该方法！
   * @return boolean
   */
  private boolean checkData()
  {
    LCGrpContDB tLCGrpContDB=new LCGrpContDB();
    tLCGrpContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
    if (tLCGrpContDB.getInfo() == false)
       {
           CError.buildErr(this, "保单查询失败");
           return false;
       }
     mLCGrpContSchema = tLCGrpContDB.getSchema();//判断团单是否存在
     LJSPaySet tLJSPaySet = new LJSPaySet();
     LJSPayDB tLJSPayDB = new LJSPayDB();
     tLJSPayDB.setOtherNo(mLCGrpContSchema.getGrpContNo());
     tLJSPayDB.setOtherNoType("1");
     tLJSPaySet = tLJSPayDB.query();
        if (tLJSPaySet.size() == 0)
        {
            CError.buildErr(this, "没有查询到团体保单续期催收的应收数据!");
            return false;
        }
        mLJSPaySchema = tLJSPaySet.get(1);
        if (mLJSPaySchema.getBankOnTheWayFlag() != null)
        {
            if (mLJSPaySchema.getBankOnTheWayFlag().equals("1"))
            {
                CError.buildErr(this, "应收数据目前银行在途中,不能撤销!");
                return false;
            }
        }

        /*//应收总表备份表
        LJSPayBSet tLJSPayBSet = new LJSPayBSet();
        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        tLJSPayBDB.setOtherNo(mLCGrpContSchema.getGrpContNo());
        tLJSPayBDB.setOtherNoType("1");
        tLJSPayBSet = tLJSPayBDB.query();
        if (tLJSPayBSet.size() == 0)
        {
            CError.buildErr(this, "没有查询到团体保单续期催收的应收备份数据!");
            return false;
        }
        mLJSPayBSchema = tLJSPayBSet.get(1);
        if (mLJSPayBSchema.getBankOnTheWayFlag() != null)
        {
            if (mLJSPayBSchema.getBankOnTheWayFlag().equals("1"))
            {
                CError.buildErr(this, "应收数据目前银行在途中,不能撤销!");
                return false;
            }
        }*/

     LJSPayGrpDB tLJSPayGrpDB=new LJSPayGrpDB();
    // LJSPayGrpSet tLJSPayGrpSet=new LJSPayGrpSet();
     tLJSPayGrpDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
     tLJSPayGrpDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
     mLJSPayGrpSet=tLJSPayGrpDB.query();
     if(mLJSPayGrpSet.size()==0)
     {
       CError.buildErr(this, "没有查询到团体保单续期催收的应收集体数据!");
       return false;
     }
     /*LJSPayGrpBDB tLJSPayGrpBDB=new LJSPayGrpBDB();
    // LJSPayGrpSet tLJSPayGrpSet=new LJSPayGrpSet();
     tLJSPayGrpBDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
     tLJSPayGrpBDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
     mLJSPayGrpBSet=tLJSPayGrpBDB.query();
     if(mLJSPayGrpBSet.size()==0)
     {
       CError.buildErr(this, "没有查询到团体保单续期催收的应收备份集体数据!");
       return false;
     }*/

     LJSPayPersonDB tLJSPayPersonDB=new LJSPayPersonDB();
     tLJSPayPersonDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
     tLJSPayPersonDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
     mLJSPayPersonSet=tLJSPayPersonDB.query();
     if(mLJSPayPersonSet.size()==0)
     {
       CError.buildErr(this, "没有查询到团体保单续期催收的应收个人数据!");
       return false;
     }
     /*LJSPayPersonBDB tLJSPayPersonBDB=new LJSPayPersonBDB();
     tLJSPayPersonBDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
     tLJSPayPersonBDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
     mLJSPayPersonBSet=tLJSPayPersonBDB.query();
     if(mLJSPayPersonBSet.size()==0)
     {
       CError.buildErr(this, "没有查询到团体保单续期催收的应收个人备份数据!");
       return false;
     }*/

     LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
     tLJTempFeeDB.setTempFeeNo(mLJSPaySchema.getGetNoticeNo());
     tLJTempFeeDB.setTempFeeType("2");
     LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.query();
     if (tLJTempFeeSet.size() > 0)
       {
           CError.buildErr(this, "财务已经录入暂交费,不能撤销！");
           return false;
       }
     //下面开始校验打印管理表和打印子表的信息
     LOPRTManagerDB tLOPRTManagerDB=new LOPRTManagerDB();
     tLOPRTManagerDB.setOtherNo(mLCGrpContSchema.getGrpContNo());
     tLOPRTManagerDB.setOtherNoType("01");
     LOPRTManagerSet tLOPRTManagerSet=new LOPRTManagerSet();
     tLOPRTManagerSet=tLOPRTManagerDB.query();
     if(tLOPRTManagerSet.size()==0)
     {
           CError.buildErr(this, "打印管理信息丢失，不能撤销！");
           return false;
     }
     mLOPRTManagerSchema.setSchema(tLOPRTManagerSet.get(1).getSchema());
     LOPRTManagerSubDB tLOPRTManagerSubDB=new LOPRTManagerSubDB();
     tLOPRTManagerSubDB.setPrtSeq(mLOPRTManagerSchema.getPrtSeq());
     mLOPRTManagerSubSet = tLOPRTManagerSubDB.query();
     if (mLOPRTManagerSubSet.size() == 0)
     {
      CError.buildErr(this, "没有找到续期打印子表数据，不能撤销！");
      return false;
     }

    return true;
  }
  private boolean dealData()
  {
    PubSubmit tPubSubmit = new PubSubmit();
    if (tPubSubmit.submitData(mResult, "") == false)
       {
          // System.out.println("delete fail!");
          CError.buildErr(this, "系统处理数据异常！");
          return false;
       }

    return true;
  }
/**  private boolean dealData()
  {
    String GrpContNo = mLCGrpContSchema.getGrpContNo();

    LJSPaySchema tLJSPaySchema = new LJSPaySchema();
    tLJSPaySchema.setOtherNo(GrpContNo);
    tLJSPaySchema.setOtherNoType("1");
    LJSPayDB tLJSPayDB = new LJSPayDB();
    tLJSPayDB.setSchema(tLJSPaySchema);
    LJSPaySet tLJSPaySet = tLJSPayDB.query();
    if(tLJSPaySet.size() < 1)
    {
      CError.buildErr(this,"查询应收总表失败！团单号是"+GrpContNo);
      return false;
    }
    tLJSPaySchema = tLJSPaySet.get(1);
    if(tLJSPaySchema.getBankOnTheWayFlag().equals("1"))
    {
      CError.buildErr(this,"有银行在途数据，不允许撤销续期催收通知书!团单号是"+GrpContNo);
      return false;
    }

     LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
     tLJTempFeeSchema.setOtherNoType("1");
     tLJTempFeeSchema.setOtherNo(GrpContNo);
     tLJTempFeeSchema.setTempFeeType("2");
     tLJTempFeeSchema.setRiskCode(tLJSPaySchema.getRiskCode());
     LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
     tLJTempFeeDB.setSchema(tLJTempFeeSchema);
     LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.query();
     if(tLJTempFeeSet.size() > 0)
     {
       CError.buildErr(this,"已有暂交费记录，请保户先做暂交费退费！团单号是"+GrpContNo);
       return false;
     }


    return true;
  }*/
  private boolean preparedata()
  {
    String GrpContNo = mLCGrpContSchema.getGrpContNo();
    mData = new VData();

    LJSPaySchema tLJSPaySchema = new LJSPaySchema();
    tLJSPaySchema.setOtherNo(GrpContNo);
    tLJSPaySchema.setOtherNoType("1");
    LJSPayDB tLJSPayDB = new LJSPayDB();
    tLJSPayDB.setSchema(tLJSPaySchema);
    LJSPaySet tLJSPaySet = tLJSPayDB.query();
    tLJSPaySchema = tLJSPaySet.get(1);
    mData.add(tLJSPaySchema);

    String GetNoticeNo = tLJSPaySchema.getGetNoticeNo();

    LJSPayGrpSchema tLJSPayGrpSchema = new LJSPayGrpSchema();
    tLJSPayGrpSchema.setGrpContNo(GrpContNo);
    tLJSPayGrpSchema.setGetNoticeNo(GetNoticeNo);
    LJSPayGrpDB tLJSPayGrpDB = new LJSPayGrpDB();
    tLJSPayGrpDB.setSchema(tLJSPayGrpSchema);
    LJSPayGrpSet tLJSPayGrpSet = tLJSPayGrpDB.query();
    //tLJSPayGrpSchema = tLJSPayGrpSet.get(1);
    mData.add(tLJSPayGrpSet);

    LJSPayPersonSchema tLJSPayPersonSchema = new LJSPayPersonSchema();
    tLJSPayPersonSchema.setGrpContNo(GrpContNo);
    tLJSPayPersonSchema.setGetNoticeNo(GetNoticeNo);
    LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
    tLJSPayPersonDB.setSchema(tLJSPayPersonSchema);
    LJSPayPersonSet tLJSPayPersonSet = tLJSPayPersonDB.query();
    mData.add(tLJSPayPersonSet);

    LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
    tLOPRTManagerSchema.setOtherNoType("01");
    tLOPRTManagerSchema.setOtherNo(GrpContNo);
    LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
    tLOPRTManagerDB.setSchema(tLOPRTManagerSchema);
    LOPRTManagerSet tLOPRTManagerSet = tLOPRTManagerDB.query();
    mData.add(tLOPRTManagerSet);

    return true;
  }
}
