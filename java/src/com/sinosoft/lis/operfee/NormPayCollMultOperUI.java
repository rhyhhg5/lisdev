package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class NormPayCollMultOperUI {

  //业务处理相关变量
  private VData mInputData ;
  public  CErrors mErrors=new CErrors();

  public NormPayCollMultOperUI() {
  }
  public static void main(String[] args) {
    NormPayCollMultOperUI GrpDueFeeMultiUI1 = new NormPayCollMultOperUI();
    GlobalInput tGI = new GlobalInput();
    tGI.ComCode="86";
    tGI.Operator="001";
    tGI.ManageCom="86";
    TransferData tTransferData=new TransferData();
    tTransferData.setNameAndValue("PayDate","2006-8-31");

    VData tVData = new VData();
    tVData.add(tGI);
    tVData.add(tTransferData);
    NormPayCollMultOperUI tNormPayCollMultOperUI = new NormPayCollMultOperUI();
    if(tNormPayCollMultOperUI.submitData(tVData,"VERIFY")==false)
    {
        System.out.println("团单批处理核销失败："+tNormPayCollMultOperUI.mErrors.getFirstError());
    }
    else
    {
        System.out.println("团单批处理核销成功");
    }

  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    NormPayCollMultOperBL tNormPayCollMultOperBL=new NormPayCollMultOperBL();
    System.out.println("Start NormPayCollMultOper UI Submit...");
    tNormPayCollMultOperBL.submitData(mInputData,cOperate);

    System.out.println("End NormPayCollMultOper UI Submit...");

    mInputData=null;
    //如果有需要处理的错误，则返回
    if (tNormPayCollMultOperBL.mErrors .needDealError() )
       {
       this.mErrors .copyAllErrors(tNormPayCollMultOperBL.mErrors ) ;
       return false;
       }
System.out.println("error num="+mErrors.getErrorCount());
    return true;
  }

}
