package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class DuePayFeeCollUpdateBL  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 数据操作字符串 */
  private String mOperate;
  private LJSPayGrpBL        mLJSPayGrpBL         = new LJSPayGrpBL();
  private LJSPayBL           mLJSPayBL            = new LJSPayBL();
  private LJSPayPersonSet    mLJSPayPersonSet     = new LJSPayPersonSet();
  private LJSPayPersonSet    mLJSPayPersonSetNew  = new LJSPayPersonSet();
  private LCPremSet          mLCPremSet           = new LCPremSet();
  private LCPremSet          mLCPremSetNew        = new LCPremSet();

  //业务处理相关变量

  public DuePayFeeCollUpdateBL() {
  }
  public static void main(String[] args) {
    DuePayFeeCollUpdateBL DuePayFeeCollUpdateBL1 = new DuePayFeeCollUpdateBL();
    LJSPayPersonBL mLJSPayPersonBL =new LJSPayPersonBL();    
    VData tv=new VData();
    tv.add(mLJSPayPersonBL);
    DuePayFeeCollUpdateBL1.submitData(tv,"UPDATE");
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;  	
System.out.println("OperateData:  "+cOperate);
    

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
System.out.println("After getinputdata");
          
    //进行业务处理
    if (!dealData())
      return false;
System.out.println("After dealData！");    
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
System.out.println("After prepareOutputData");    
      
System.out.println("Start LJSPayPerson BL Submit...");

    DuePayFeeCollUpdateBLS tDuePayFeeCollUpdateBLS=new DuePayFeeCollUpdateBLS();
    tDuePayFeeCollUpdateBLS.submitData(mInputData,cOperate);

System.out.println("End LJSPayPerson BL Submit...");

    //如果有需要处理的错误，则返回
    if (tDuePayFeeCollUpdateBLS.mErrors .needDealError())
    {
        this.mErrors .copyAllErrors(tDuePayFeeCollUpdateBLS.mErrors ) ;
    }

    mInputData=null;
    return true;
  }

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData()
  {
    int i,iMax;
    boolean tReturn =false;
    String tNo="";
    //处理个人信息数据
    //添加纪录
    if(this.mOperate.equals("UPDATE"))
    {
//1-处理应收个人交费表，记录集，循环处理
    LJSPayPersonBL  tLJSPayPersonBL;
    iMax=mLJSPayPersonSet.size() ;
    for (i=1;i<=iMax;i++)
    {
      tLJSPayPersonBL = new LJSPayPersonBL() ;
      tLJSPayPersonBL.setSchema(mLJSPayPersonSet.get(i).getSchema());
      tLJSPayPersonBL.setContNo("1234");  //总单/合同号码 not null
      tLJSPayPersonBL.setBankCode("1234");//银行编码,not null
      tLJSPayPersonBL.setMakeDate("1999/1/1");//入机日期
      tLJSPayPersonBL.setMakeTime("21:12:12");//入机时间
      tLJSPayPersonBL.setModifyDate("1999/1/1");//最后一次修改日期
      tLJSPayPersonBL.setModifyTime("21:12:12");//最后一次修改时间
      mLJSPayPersonSetNew.add(tLJSPayPersonBL);
      tReturn=true;
System.out.println("paycount="+tLJSPayPersonBL.getPayCount());            
    }
//2-处理收费项表，记录集，循环处理
    LCPremBL tLCPremBL;
    iMax=mLCPremSet.size() ;
    for (i=1;i<=iMax;i++)
    {
      tLCPremBL = new LCPremBL() ;
      tLCPremBL.setSchema(mLCPremSet.get(i).getSchema());
      tLCPremBL.setModifyDate("1999/1/1");
      tLCPremBL.setModifyTime("21:12:12");
      mLCPremSetNew.add(tLCPremBL);
      tReturn=true;      
    }
//3-应收总表，单项纪录        	  	
    mLJSPayBL.setMakeDate("1999/1/1");
    mLJSPayBL.setMakeTime("21:12:12");
    mLJSPayBL.setModifyDate("1999/1/1");
    mLJSPayBL.setModifyTime("21:12:12");
    tReturn=true;
//4-应收集体交费表，单项纪录
    mLJSPayGrpBL.setMakeDate("1999/1/1");
    mLJSPayGrpBL.setMakeTime("21:12:12");
    mLJSPayGrpBL.setModifyDate("1999/1/1");
    mLJSPayGrpBL.setModifyTime("21:12:12");    
    tReturn=true;
}
    return tReturn ;

}

  //从输入数据中得到所有对象
  //输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
 /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData mInputData)
  {
    //应收总表 
    mLJSPayBL.setSchema((LJSPaySchema)mInputData.getObjectByObjectName("LJSPaySchema",0));
    // 应收个人交费表
    mLJSPayPersonSet.set((LJSPayPersonSet)mInputData.getObjectByObjectName("LJSPayPersonSet",0));
    // 保费项表
    mLCPremSet.set((LCPremSet)mInputData.getObjectByObjectName("LCPremSet",0));
    //应收集体交费表
    mLJSPayGrpBL.setSchema((LJSPayGrpSchema)mInputData.getObjectByObjectName("LJSPayGrpSchema",0));
    
    if(mLJSPayBL==null || mLJSPayPersonSet ==null || mLCPremSet ==null||mLJSPayGrpBL==null )
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="DuePayFeeCollUpdateBL";
      tError.functionName="getInputData";
      tError.errorMessage="没有得到足够的数据，请您确认!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }
  
  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData()
  {
    mInputData=new VData();  	
    try
    {    	
//1-处理应收个人交费表，记录集，循环处理     
      mInputData.add(mLJSPayPersonSetNew);
//2-处理收费项表，记录集，循环处理      
      mInputData.add(mLCPremSetNew);      
//3-应收总表，单项纪录      
      mInputData.add(mLJSPayBL);
//4-应收集体交费表，单项纪录   
      mInputData.add(mLJSPayGrpBL);      
            
      System.out.println("prepareOutputData:");      
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="DuePayFeeCollUpdateBL";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  } 
}
 
 