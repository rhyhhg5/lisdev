package com.sinosoft.lis.operfee;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.rmi.*;

import javax.xml.rpc.*;

import com.sinosoft.lis.message.*;

import java.util.*;

/**
 * <p>Title: 客户续期短信通知</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GrpPayRnewMsgBL {
    private GlobalInput mG = new GlobalInput();
    private String mCurrentDate = PubFun.getCurrentDate();
    private ExeSQL mExeSQL = new ExeSQL();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public GrpPayRnewMsgBL() {}

    private boolean getInputData(VData cInputData) 
    {
    	mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        return true;
    }

    public boolean submitData(VData cInputData, String cOperate) {
        cInputData = (VData) cInputData.clone();

        if (!getInputData(cInputData)) 
        {
        	mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0));
        	if(mG.Operator==null || mG.Operator.equals("") || mG.Operator.equals("null"))
        	{
        		mG.Operator="001";
        	}
        	if(mG.ManageCom==null || mG.ManageCom.equals("") || mG.ManageCom.equals("null"))
        	{
        		mG.ManageCom="86";
        	}
            return false;
        }
        if (cOperate != null && cOperate.trim().equals("XQMSG")) {
            sendMsg();
        } else {
            mErrors.addOneError(new CError("不支持的操作字符串！"));
            return false;
        }
        return true;
    }

    private boolean sendMsg() {
        System.out.println("客户续期短信通知批处理开始......");
        SMSClient smsClient = new SMSClient();
        
        Vector vec = new Vector();
        vec = getMessage();
//        Vector tempVec = new Vector(); //临时变量--实际发短信时传的参数
        //拆分Vector
        System.out.println("---------vec="+vec.size());

        if (!vec.isEmpty()) 
        {
        	for(int i=0;i<vec.size();i++)
            {
//        		tempVec.clear();
//            	tempVec.add(vec.get(i));
            	
            	System.out.print("测试：调用接口！！！");
            	
            	SmsMessagesNew msgNews = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的Messages元素            
            	msgNews = (SmsMessagesNew) vec.elementAt(i);
            	smsClient.sendSMS(msgNews);
            }
        } 
        else 
        {
            System.out.print("续期无符合条件的短信！");
        }
        System.out.println("客户续期短信通知批处理正常结束......");
        return true;
    }

    private Vector getMessage() {
        Vector tVector = new Vector();
        Vector tVectemp = new Vector();
        

//  第一部分:距满期前10天催缴保费的短信
//  按缴费方式分为银行转账或者现金两种方式
//  每种方式同样包含两条短信
//  需要发给业务员的和客户
   
 try{
       String tSQLnotify = "select "
    		    + " (select phone1 from lcgrpaddress where customerno=c.appntno and addressno=c.addressno fetch first 1 rows only), "
				+ " c.grpname, " 
		       	+ " '先生/女士', " 
		       	+ " (select mobile from laagent where agentcode=c.agentcode),  "
		       	+ " (select name from laagent where agentcode=c.agentcode),"
		       	+ " (select (case sex when '1' then '女士' when '0' then '先生' else '先生/女士' end) from laagent where agentcode=c.agentcode),"
		       	+ " (select lj.lastpaytodate  from ljspaygrpb lj  where lj.grpcontno=c.grpcontno "
		       			+ " and lj.getnoticeno=a.getnoticeno order by lj.lastpaytodate fetch first 1 row only ) , "
		       	+ " c.grpcontno,a.managecom,a.bankaccno,a.sumduepaymoney,a.getnoticeno, "
		       	+ " (select agentstate from laagent where agentcode=c.agentcode),c.agentcode, c.salechnl, "
		       	+ " (select r.riskname from lmriskapp r, lcgrppol p where p.grpcontno = c.grpcontno and r.riskcode = p.riskcode and p.riskcode in ('162601','162801') order by r.riskperiod fetch first 1 rows only) "
		       	+ " from ljspayb a,lcgrpcont c  "
		       	+ " where 1=1  	"
		       	+ " and a.othernotype='1' and a.dealstate = '0' "
		        + " and a.getnoticeno like '31%' "
		        + " and a.otherno=c.grpcontno "
		        + " and not exists (select 1 from LCCSpec d where d.grpcontno=c.grpcontno and d.SpecType='xq02' and d.EndorsementNo=(select lj.lastpaytodate  from ljspaygrpb lj  where lj.grpcontno=c.grpcontno "
		        + " and lj.getnoticeno=a.getnoticeno order by lj.lastpaytodate fetch first 1 row only )) "
		        //测试后要放开	
                + " and (select lj.lastpaytodate - 10 days from ljspaygrpb lj  where lj.grpcontno=c.grpcontno "
		        + " and lj.getnoticeno=a.getnoticeno order by lj.lastpaytodate fetch first 1 row only )<= current date  "
		        + " and current date <= (select lj.lastpaytodate from ljspaygrpb lj  where lj.grpcontno=c.grpcontno  "
		        + " and lj.getnoticeno=a.getnoticeno order by lj.lastpaytodate fetch first 1 row only ) "
		        + " and  exists (select 1 from lcgrppol where grpcontno=c.grpcontno and riskcode in ('162601','162801')) "
//		       	   and c.grpcontno in ('00334799000002')  //014060643000002
       			+ " order by c.grpcontNo "
				+ " with ur ";
       
        SSRS tMsgNotifySSRS =mExeSQL.execSQL(tSQLnotify);
        System.out.println(tMsgNotifySSRS.getMaxRow()+"tMsgSuccSSRS.getMaxRow()");
        for (int i = 1; i <= tMsgNotifySSRS.getMaxRow(); i++) 
        {
        	String tCustomerMobile = tMsgNotifySSRS.GetText(i, 1);
        	String tCustomerName = tMsgNotifySSRS.GetText(i, 2);
        	String tCustomerSex = tMsgNotifySSRS.GetText(i, 3);
        	String tAgentMobile = tMsgNotifySSRS.GetText(i, 4);
        	String tAgentName = tMsgNotifySSRS.GetText(i, 5);
        	String tAgentSex = tMsgNotifySSRS.GetText(i, 6);
        	String tPayToDate = tMsgNotifySSRS.GetText(i, 7);
        	String tManageCom = tMsgNotifySSRS.GetText(i, 9);
        	String tGrpContNo = tMsgNotifySSRS.GetText(i, 8);
        	String tBankNo = tMsgNotifySSRS.GetText(i,10);
        	String tPayMoney = tMsgNotifySSRS.GetText(i,11);
        	String tGetNoticeNo = tMsgNotifySSRS.GetText(i,12);
        	String tAgentState=tMsgNotifySSRS.GetText(i,13); //区分业务员是否是离职状态 
        	String tAgentCode=tMsgNotifySSRS.GetText(i,14);  //查离职状态的业务员的上级用
        	
        	//团单续保短信，客户和业务员均发送
        	//2018-10-15 direct by hhw 销售渠道，用于判定部门
        	String tSalechnl = tMsgNotifySSRS.GetText(i,15);
        	String riskName = tMsgNotifySSRS.GetText(i, 16);
        	
        	if (tManageCom == null || "".equals(tManageCom) ||
                    "null".equals(tManageCom)) {
                    continue;
                }
        	
        	if (tGrpContNo == null || "".equals(tGrpContNo) ||
                    "null".equals(tGrpContNo)) {
                    continue;
                }

            if (tPayToDate == null || "".equals(tPayToDate) ||
                    "null".equals(tPayToDate)) {
                    continue;
                }
            
            if (tPayMoney == null || "".equals(tPayMoney) ||
                    "null".equals(tPayMoney)) {
                    continue;
                }
            
            if (tSalechnl == null || "".equals(tSalechnl) ||
                    "null".equals(tSalechnl)) {
                    continue;
                }
        	
            boolean customerFlag = true;
            boolean agentFlag = true;
            
            if (tCustomerMobile == null || "".equals(tCustomerMobile) ||
                    "null".equals(tCustomerMobile)) {
            	customerFlag = false;
                }
            if (tCustomerName == null || "".equals(tCustomerName) ||
                    "null".equals(tCustomerName)) {
            	customerFlag = false;
                }
            
            if (tCustomerSex == null || "".equals(tCustomerSex) ||
                "null".equals(tCustomerSex)) {
            	customerFlag = false;
            }
            
            if(customerFlag)
            {
            	String tCustomerContents = "";
            	String tDepartMent="";
                tDepartMent = getDepartment(tSalechnl);
//              现金方式缴费的
            	if (tBankNo == null || "".equals(tBankNo) || "null".equals(tBankNo)) 
            	{
//					发送给客户的短信     
            		//lijia
            		SmsMessageNew tCustomerMsg = new SmsMessageNew(); //创建SmsMessageNew对象，定义同上文下行短信格式中的Message元素
                    tCustomerContents = "尊敬的"+tCustomerName+tCustomerSex 
                                             + "，您好！您购买的"+riskName+"（保单号"+tGrpContNo+"）将于"+tPayToDate+"进入缴费期，为保障您的权益，"
                                             + "请您尽快联系公司缴纳下期保费"+tPayMoney+"元，祝您健康。客服电话：95591。";
                    System.out.println("现金发送短信：" + tCustomerName + tGrpContNo +"日期:"+tPayToDate +"金额:"+ tPayMoney +"客户手机号："+ tCustomerMobile);

                    tCustomerMsg.setReceiver(tCustomerMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//                    tCustomerMsg.setReceiver("18500905130");
                    tCustomerMsg.setContents(tCustomerContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                    //和IT讨论后，归类到二级机构
                    tCustomerMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素

                    //添加SmsMessageNew对象
                    tVectemp.add(tCustomerMsg);
                    
                    SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的Messages元素
                    msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                    msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                    msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                    msgs.setTaskValue(tDepartMent);
                    msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                    msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                    msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                    msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                    msgs.setMessages((SmsMessageNew[]) tVectemp.toArray(new SmsMessageNew[tVectemp.size()]));
                    tVectemp.clear();
                    tVector.add(msgs);
                }
            	//银行转账方式缴费的
            	else 
            	{                  
                    //发送给客户的短信            
            		SmsMessageNew tCustomerMsg = new SmsMessageNew(); //创建SmsMessageNew对象，定义同上文下行短信格式中的Message元素
                    
                	String AccNo = null;
                	if(tBankNo != null && !"".equals(tBankNo) && tBankNo.length() > 4){
                		AccNo = tBankNo.substring(tBankNo.length()-4, tBankNo.length());
                	} else {
                		AccNo = tBankNo;
                	}
	          	
                    tCustomerContents = "尊敬的"+tCustomerName+tCustomerSex 
                                             + "，您好！您购买的"+riskName+"（保单号"+tGrpContNo+"）将于"+tPayToDate+"进入缴费期，为保障您的权益，" 
                                             + "请您尽快将下期保费"+tPayMoney+"元存入您的尾号"+AccNo+"的缴费帐户，祝您健康。客服电话：95591。";
                    System.out.println("发送短信：" + tCustomerName + tGrpContNo +"日期:"+tPayToDate +"金额:"+ tPayMoney+"客户手机号："+ tCustomerMobile +"缴费账户后四位："+AccNo );
                    tCustomerMsg.setReceiver(tCustomerMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//                    tCustomerMsg.setReceiver("18500905130");
                    tCustomerMsg.setContents(tCustomerContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                    tCustomerMsg.setOrgCode(tManageCom); //保单机构

                    //添加SmsMessageNew对象
                    tVectemp.add(tCustomerMsg);
                    
                    SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的Messages元素
                    msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                    msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                    msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                    msgs.setTaskValue(tDepartMent);
                    msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                    msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                    msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                    msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                    msgs.setMessages((SmsMessageNew[]) tVectemp.toArray(new SmsMessageNew[tVectemp.size()]));
                    tVectemp.clear();
                    tVector.add(msgs);
            	}
//            	在LCCSpec表中插入值  ********************************************************  
                LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
                tLCCSpecSchema.setGrpContNo(tGrpContNo);
                tLCCSpecSchema.setContNo(BQ.FILLDATA);
                tLCCSpecSchema.setProposalContNo(tGrpContNo);
                tLCCSpecSchema.setPrtSeq(tGetNoticeNo);//催收的 放 ljspay--getnoticeno                              
                String tLimit = PubFun.getNoLimit(tManageCom);
    	        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
    	        tLCCSpecSchema.setSerialNo(serNo); //流水号
                tLCCSpecSchema.setEndorsementNo(tPayToDate);//lccont--paytodate 
                tLCCSpecSchema.setSpecType("xq02"); //xq01-续期缴费截止日前60天短信类型 xq02-续期缴费截止日前10天短信类型，xq03-续期缴费核销短信类型
                tLCCSpecSchema.setSpecCode("001");//001-投保人 002-被保人 003-业务员  
				tLCCSpecSchema.setSpecContent(tCustomerContents+"客户手机号："+ tCustomerMobile);//短信内容
                tLCCSpecSchema.setOperator(mG.Operator);
                tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
                tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
                tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
                tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
                tLCCSpecSchema.getDB().insert();
            }
            
            if (tAgentState == null || "".equals(tAgentState) ||
                    "null".equals(tAgentCode)) {
            	agentFlag = false;
                }
            if (tAgentCode == null || "".equals(tAgentCode) ||
                    "null".equals(tAgentCode)) {
            	agentFlag = false;
                }
            
//          业务员是否离职状态
            if(Integer.parseInt(tAgentState)>=6) 
            {	
					agentFlag = false;
			}
            
            if (tAgentMobile == null || "".equals(tAgentMobile) || "null".equals(tAgentMobile)) 
        	{
            	agentFlag = false;
            }

            if (tAgentName == null || "".equals(tAgentName) || "null".equals(tAgentName)) 
            {
            	agentFlag = false;
            }
                
            if (tAgentSex == null || "".equals(tAgentSex) || "null".equals(tAgentSex)) 
            {
            	agentFlag = false;
            }
            if(agentFlag)
            {
            	String tAgentContents = "";
            	String tDepartMent="";
                tDepartMent = getDepartment(tSalechnl);
//              现金方式缴费的
            	if (tBankNo == null || "".equals(tBankNo) ||
                        "null".equals(tBankNo)) 
            	{
//                  发送给业务员的短信 
            		SmsMessageNew tAgentMsg = new SmsMessageNew(); //创建SmsMessageNew对象，定义同上文下行短信格式中的Message元素
                    tAgentContents = "尊敬的"+tAgentName+tAgentSex 
				                   + "，您好！您的客户"+tCustomerName+"的保单(保单号"+tGrpContNo+"，本期保费"+tPayMoney+"元)" 
				                   + "将于"+tPayToDate+"进入缴费期，请您尽快联系您的客户收取续期保费。";
                    System.out.println("现金发送短信：" + tAgentName + tGrpContNo +"日期:"+ tPayToDate+"金额:"+ tPayMoney+"业务员手机号："+ tAgentMobile );
                    tAgentMsg.setReceiver(tAgentMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//                    tAgentMsg.setReceiver("18500905130");
                    tAgentMsg.setContents(tAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                    //和IT讨论后，归类到二级机构
                    //tAgentMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                    tAgentMsg.setOrgCode(tManageCom); //保单机构

                    //添加SmsMessageNew对象
                    tVectemp.add(tAgentMsg);
                    
                    SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的Messages元素
                    msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                    msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                    msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                    msgs.setTaskValue(tDepartMent);
                    msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                    msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                    msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                    msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                    msgs.setMessages((SmsMessageNew[]) tVectemp.toArray(new SmsMessageNew[tVectemp.size()]));
                    tVectemp.clear();
                    tVector.add(msgs);
                }
            	//银行转账方式缴费的
            	else 
            	{                  
                    //发送给业务员的短信 
            		SmsMessageNew tAgentMsg = new SmsMessageNew(); //创建SmsMessageNew对象，定义同上文下行短信格式中的Message元素
                    tAgentContents = "尊敬的"+tAgentName+tAgentSex 
				                   + "，您好！您的客户"+tCustomerName+"的保单(保单号"+tGrpContNo+"，本期保费"+tPayMoney+"元)" 
				                   + "将于"+tPayToDate+"进入缴费期，请您尽快联系您的客户收取续期保费。";
                    System.out.println("发送短信：" + tAgentName + tGrpContNo +"日期:"+ tPayToDate+"金额:"+ tPayMoney+"业务员手机号："+ tAgentMobile );
                    tAgentMsg.setReceiver(tAgentMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//                    tAgentMsg.setReceiver("18500905130");
                    tAgentMsg.setContents(tAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                    tAgentMsg.setOrgCode(tManageCom); //保单机构

                    //添加SmsMessageNew对象
                    tVectemp.add(tAgentMsg);
                    
                    SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的Messages元素
                    msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                    msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                    msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                    msgs.setTaskValue(tDepartMent);
                    msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                    msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                    msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                    msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                    msgs.setMessages((SmsMessageNew[]) tVectemp.toArray(new SmsMessageNew[tVectemp.size()]));
                    tVectemp.clear();
                    tVector.add(msgs);
            	}
//            	在LCCSpec表中插入值  ********************************************************  
                LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
                tLCCSpecSchema.setGrpContNo(tGrpContNo);
                tLCCSpecSchema.setContNo(BQ.FILLDATA);
                tLCCSpecSchema.setProposalContNo(tGrpContNo);
                tLCCSpecSchema.setPrtSeq(tGetNoticeNo);//催收的 放 ljspay--getnoticeno                              
                String tLimit = PubFun.getNoLimit(tManageCom);
    	        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
    	        tLCCSpecSchema.setSerialNo(serNo); //流水号
                tLCCSpecSchema.setEndorsementNo(tPayToDate);//lccont--paytodate 
                tLCCSpecSchema.setSpecType("xq02"); //xq01-续期缴费截止日前60天短信类型 xq02-续期缴费截止日前10天短信类型，xq03-续期缴费核销短信类型
                tLCCSpecSchema.setSpecCode("003");//001-投保人 002-被保人 003-业务员  
                tLCCSpecSchema.setSpecContent(tAgentContents+"业务员手机号："+ tAgentMobile);//短信内容
                tLCCSpecSchema.setOperator(mG.Operator);
                tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
                tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
                tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
                tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
                tLCCSpecSchema.getDB().insert();
            }
            
        }
        System.out.println("2------------"+tVector.size());
	} catch (Exception e) {
			System.out.println("CpayRnewMsgBL.java报错");
			e.printStackTrace();
	}

//  第二部分:续期续保收费成功的短信
//  包含两条短信
//  发给业务员的和发给客户的

        try{
               String ttSQLsucc = "select "
            		     + " (select phone1 from lcgrpaddress where customerno=c.customerno and addressno=c.addressno fetch first 1 rows only), " 
	                	 + " c.grpname, "
	                	 + " '先生/女士', "
	                	 + " (select (case when phone is not null then phone else mobile end) from laagent where agentcode=c.agentcode),  "
	                	 + " (select name from laagent where agentcode=c.agentcode),"
	                	 + " (select (case sex when '1' then '女士' when '0' then '先生' else '先生/女士' end) from laagent where agentcode=c.agentcode),"
	                	 + " c.grpcontno,a.managecom,a.confdate,a.sumactupaymoney ,a.getnoticeno, "
	                	 + " (select agentstate from laagent where agentcode=c.agentcode),c.agentcode,c.paytodate,c.salechnl, "
	                	 + " (select r.riskname from lmriskapp r where  r.riskcode = c.riskcode and r.riskcode in ('162601','162801')  fetch first 1 rows only) "
	                	 + " from ljapay a,lcgrppol c  "
	                	 + " where 1=1  "
	                	 + " and a.incometype='1' " 
	                	 + " and a.getnoticeno like '31%' "
	                	 + " and a.incomeno=c.grpcontno "
	                	 + " and exists (select 1 from ljspayb where getnoticeno = a.getnoticeno and dealstate = '1') "
                         + " and not exists (select 1 from LCCSpec d where d.contno=c.grpcontno and d.SpecType='xq03' and d.EndorsementNo=c.paytodate ) "
	                	 //测试后放开
                         + " and a.confdate between current date - 7 days and current date "
	                	 + " and c.riskcode in ('162601','162801') "
	                     // 完成测试去掉下一行
//	                	 + " and c.grpcontno in ('014318190000001') "
                         + " fetch first 10 rows only "
			             + " with ur ";
               
               System.out.println(ttSQLsucc);

                SSRS ttMsgSuccSSRS =mExeSQL.execSQL(ttSQLsucc);

                for (int i = 1; i <= ttMsgSuccSSRS.getMaxRow(); i++) 
                {
                	String tCustomerMobile = ttMsgSuccSSRS.GetText(i, 1);
                	String tCustomerName = ttMsgSuccSSRS.GetText(i, 2);
                	String tCustomerSex = ttMsgSuccSSRS.GetText(i, 3);
                	String tAgentMobile = ttMsgSuccSSRS.GetText(i, 4);
                	String tAgentName = ttMsgSuccSSRS.GetText(i, 5);
                	String tAgentSex = ttMsgSuccSSRS.GetText(i, 6);
                	String tManageCom = ttMsgSuccSSRS.GetText(i, 8);
                	String tContNo = ttMsgSuccSSRS.GetText(i, 7);
                	String tConfDate = ttMsgSuccSSRS.GetText(i, 9);
                	String tPayMoney = ttMsgSuccSSRS.GetText(i, 10);
                	String tGetNoticeNo = ttMsgSuccSSRS.GetText(i,11);
                	String tAgentState=ttMsgSuccSSRS.GetText(i,12); //区分业务员是否是离职状态，这个功能暂时还未完成
                	String tAgentCode=ttMsgSuccSSRS.GetText(i,13);  //查离职状态的业务员的上级用
                	
                	String tPayToDate=ttMsgSuccSSRS.GetText(i,14);  //查离职状态的业务员的上级用
                	
                	//2017-5-11 direct by lichang 销售渠道，用于判定部门
                	String tSalechnl = ttMsgSuccSSRS.GetText(i,15);
                	String riskName = ttMsgSuccSSRS.GetText(i, 16);
                	
                    if (tManageCom == null || "".equals(tManageCom) ||
                        "null".equals(tManageCom)) {
                        continue;
                    }
                    
                    if (tContNo == null || "".equals(tContNo) ||
                            "null".equals(tContNo)) {
                            continue;
                        }

                    if (tConfDate == null || "".equals(tConfDate) ||
                            "null".equals(tConfDate)) {
                            continue;
                        }
                    
                    if (tPayMoney == null || "".equals(tPayMoney) ||
                            "null".equals(tPayMoney)) {
                            continue;
                        }
                    
                    if (tSalechnl == null || "".equals(tSalechnl) ||
                            "null".equals(tSalechnl)) {
                            continue;
                        }
                    
                    boolean customerFlag = true;
                    boolean agentFlag = true;

                    if (tCustomerMobile == null || "".equals(tCustomerMobile) ||
                        "null".equals(tCustomerMobile)) {
                    	customerFlag = false;
                    }

                    if (tCustomerName == null || "".equals(tCustomerName) ||
                            "null".equals(tCustomerName)) {
                    	customerFlag = false;
                    }
                    
                    if (tCustomerSex == null || "".equals(tCustomerSex) ||
                        "null".equals(tCustomerSex)) {
                    	customerFlag = false;
                    }
                    
                    String tDepartMent="";
                    tDepartMent = getDepartment(tSalechnl);
                    
                    if(customerFlag)
                    {
                        //发送给客户的短信            
                    	SmsMessageNew tCustomerMsg = new SmsMessageNew(); //创建SmsMessageNew对象，定义同上文下行短信格式中的Message元素
                        String tCustomerContents = "尊敬的"+tCustomerName+tCustomerSex 
                                                 + "，您好！您购买的"+riskName+"（保单号"+tContNo+"）已于"+tConfDate+"成功缴纳了下期保费，" 
                                                 + "缴费金额为"+tPayMoney+"元。感谢您的支持，祝您健康。客服电话：95591。";
                        System.out.println("发送短信：" + tCustomerName + tContNo +"日期:"+ tConfDate +"金额:"+ tPayMoney+"客户手机号："+ tCustomerMobile );
                        tCustomerMsg.setReceiver(tCustomerMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//                        tCustomerMsg.setReceiver("18500905130");
                        tCustomerMsg.setContents(tCustomerContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                        //和IT讨论后，归类到二级机构
                        //tCustomerMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                        tCustomerMsg.setOrgCode(tManageCom); //保单机构

                        //添加SmsMessageNew对象
                        tVectemp.add(tCustomerMsg);
                        
                        SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的Messages元素
                        msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                        msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                        msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                        msgs.setTaskValue(tDepartMent);
                        msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                        msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                        msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                        msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                        msgs.setMessages((SmsMessageNew[]) tVectemp.toArray(new SmsMessageNew[tVectemp.size()]));
                        tVectemp.clear();
                        tVector.add(msgs);
                        
//                        在LCCSpec表中插入值  ******************************************************** 
                        LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
                        tLCCSpecSchema.setGrpContNo(BQ.GRPFILLDATA);
                        tLCCSpecSchema.setContNo(tContNo);
                        tLCCSpecSchema.setProposalContNo(BQ.GRPFILLDATA);               
                        tLCCSpecSchema.setPrtSeq(tGetNoticeNo);//催收的 放 ljspay--getnoticeno 
                        String tLimit = PubFun.getNoLimit(tManageCom);
            	         String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
            	         tLCCSpecSchema.setSerialNo(serNo); //流水号                
                        tLCCSpecSchema.setEndorsementNo(tPayToDate);//ljapay--confdate 
                        tLCCSpecSchema.setSpecType("xq03"); //xq01-续期缴费截止日前60天短信类型 xq02-续期缴费截止日前10天短信类型，xq03-续期缴费核销短信类型
                        tLCCSpecSchema.setSpecCode("001");////001-投保人 002-被保人 003-业务员
                        tLCCSpecSchema.setSpecContent(tCustomerContents+"客户手机号："+ tCustomerMobile);
                        tLCCSpecSchema.setOperator(mG.Operator);
                        tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
                        tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
                        tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
                        tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
                        tLCCSpecSchema.getDB().insert();
                    }

                    if (tAgentState == null || "".equals(tAgentState) ||
                            "null".equals(tAgentCode)) {
                    	agentFlag = false;
                        }
                    if (tAgentCode == null || "".equals(tAgentCode) ||
                            "null".equals(tAgentCode)) {
                    	agentFlag = false;
                        }
                    
//                  业务员是否离职状态
                    if(Integer.parseInt(tAgentState)>=6) 
                    {	
        					agentFlag = false;

        			}
                    
                    if (tAgentMobile == null || "".equals(tAgentMobile) || "null".equals(tAgentMobile)) 
		        	{
                    	agentFlag = false;
	                }

	                if (tAgentName == null || "".equals(tAgentName) || "null".equals(tAgentName)) 
	                {
	                	agentFlag = false;
	                }
	                    
	                if (tAgentSex == null || "".equals(tAgentSex) || "null".equals(tAgentSex)) 
	                {
	                	agentFlag = false;
	                }
                        
                    if(agentFlag)
                    {

//                      发送给业务员的短信 
                    	SmsMessageNew tAgentMsg = new SmsMessageNew(); //创建SmsMessageNew对象，定义同上文下行短信格式中的Message元素
                        String tAgentContents   = "尊敬的"+tAgentName+tAgentSex 
						                        + "，您好！您的客户"+tCustomerName+"的保单（保单号为"+tContNo+"，本期保费"+tPayMoney+"元）" 
						                        + "已于"+tConfDate+"成功缴纳了本期保费。";
                        System.out.println("发送短信：" + tAgentName + tContNo +"日期:"+ tConfDate +"金额:"+ tPayMoney+"业务员手机号："+ tAgentMobile );
                        tAgentMsg.setReceiver(tAgentMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
//                        tAgentMsg.setReceiver("18500905130");
                        
                        tAgentMsg.setContents(tAgentContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                        tAgentMsg.setOrgCode(tManageCom); //保单机构

                        //添加SmsMessageNew对象
                        tVectemp.add(tAgentMsg);
                        
                        SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessagesNew对象，该对象对应于上文下行短信格式中的Messages元素
                        msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                        msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                        msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                        msgs.setTaskValue(tDepartMent);
                        msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                        msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                        msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                        msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                        msgs.setMessages((SmsMessageNew[]) tVectemp.toArray(new SmsMessageNew[tVectemp.size()]));
                        tVectemp.clear();
                        tVector.add(msgs);
                        
//                      在LCCSpec表中插入值  ******************************************************** 
                        LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
                        tLCCSpecSchema.setGrpContNo(BQ.GRPFILLDATA);
                        tLCCSpecSchema.setContNo(tContNo);
                        tLCCSpecSchema.setProposalContNo(BQ.GRPFILLDATA);               
                        tLCCSpecSchema.setPrtSeq(tGetNoticeNo);//催收的 放 ljspay--getnoticeno 
                        String tLimit = PubFun.getNoLimit(tManageCom);
            	         String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
            	         tLCCSpecSchema.setSerialNo(serNo); //流水号                
                        tLCCSpecSchema.setEndorsementNo(tPayToDate);//ljapay--confdate 
                        tLCCSpecSchema.setSpecType("xq03"); //xq01-续期缴费截止日前60天短信类型 xq02-续期缴费截止日前10天短信类型，xq03-续期缴费核销短信类型
                        tLCCSpecSchema.setSpecCode("003");////001-投保人 002-被保人 003-业务员
                        tLCCSpecSchema.setSpecContent(tAgentContents+"业务员手机号："+ tAgentMobile);
                        tLCCSpecSchema.setOperator(mG.Operator);
                        tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
                        tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
                        tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
                        tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
                        tLCCSpecSchema.getDB().insert();
                    }
                    
                }
                System.out.println("3------------"+tVector.size());

    } catch (Exception e) {
			System.out.println("CpayRnewMsgBL.java报错");
			e.printStackTrace();
	}

        return tVector;
    }
    
    
    //区分业务员是否离职，离职的继续查他的上级领导
   private SSRS getAgentInfo(String tAgentState,String tAgentCode)
   {
	   SSRS tMsgAgentSSRS=null;
	   if(Integer.parseInt(tAgentState)>=6)
	   {		   
				String tSQLAgent="select "
					            +"(select mobile from laagent where agentcode=c.upagent),  "
					            +"(select name from laagent where agentcode=c.upagent),"
					            +"(select (case sex when '1' then '女士' when '0' then '先生' else '先生/女士' end) from laagent where agentcode=c.upagent) ,"
					            +"(select agentstate from laagent where agentcode=c.upagent),c.upagent "
								+" from latree c where agentcode='"+tAgentCode+"'";
				tMsgAgentSSRS =mExeSQL.execSQL(tSQLAgent);
				
				System.out.println("tMsgAgentSSRS=="+tMsgAgentSSRS.MaxRow);
				if(tMsgAgentSSRS!=null)
				{
					String tAgentState1=tMsgAgentSSRS.GetText(1,4); //上级状态
					String tAgentCode1 =tMsgAgentSSRS.GetText(1,5); //上级代码
					
					if(tAgentState1==null || tAgentState1.equals("") || "null".equals(tAgentState1))
					{
						 System.out.println("业务员状态为空");
						 return null;
					}
					if(tAgentCode1==null || tAgentCode1.equals("") || "null".equals(tAgentCode1))
					{
						 System.out.println("业务员代码为空");
						 return null;
					}
					if(tAgentCode1.equals(tAgentCode))
					{
						System.out.println("业务员上级为本人--错误数据");
						return null;
					}
					if(Integer.parseInt(tAgentState)>=6)
					{
						getAgentInfo(tAgentState1,tAgentCode1);
					}
				}
	   }
	  return tMsgAgentSSRS;  
   }

   private String getDepartment(String Salechnl){
	   String department = "";
	   if("01".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue10;
	   } else if("02".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue9;
	   } else if("03".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue9;
	   } else if("04".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue11;
	   } else if("06".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue10;
	   } else if("07".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue9;
	   } else if("10".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue10;
	   } else if("13".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue11;
	   } else if("14".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue12;
	   } else if("15".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue12;
	   } else if("16".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue8;
	   } else if("17".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue14;
	   } else if("18".equals(Salechnl)){
		   department = SMSClient.mes_taskVlaue8;
	   } else {
		   department = SMSClient.mes_taskVlaue15;
	   }	   
	   return department;
   }
   
    public static void main(String[] args) {
        GlobalInput mGlobalInput = new GlobalInput();
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);
        
        GrpPayRnewMsgBL tCpayRnewMsgBL = new GrpPayRnewMsgBL();
        tCpayRnewMsgBL.submitData(mVData, "XQMSG");
    }


}
