package com.sinosoft.lis.operfee;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.task.*;


//程序名称：GEdorTypeLPSaveBL.java
//程序功能：
//创建日期：2008-11-13
//创建人  ：Zhang Guoming
//更新记录：  更新人    更新日期     更新原因/内容

public class GEdorTypeLPSaveBL
{
    /**全局信息*/
    private GlobalInput mGI = null;
    private String mEdorNo = null;
    private String mGrpContNo = null;
    private String mEdorType = "LP";
    public CErrors mErrors = new CErrors();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LPDiskImportSet mLPDiskImportSet = null;
    private Reflections mReflections = new Reflections();
    private TransferData mTransferData = new TransferData();
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    private MMap map = new MMap();
    private String mdoType ;  // B--通知书页面进入；Q--给付查询/失败处理
    public GEdorTypeLPSaveBL()
    {
    }

    public boolean submitData(String aEdorNo, String aGrpContNo,GlobalInput gi,VData tVData)
    {
        //接受传入的数据
        if(!getInputData(aEdorNo, aGrpContNo,gi,tVData))
        {
            return false;
        }
        //校验数据
        if (!checkData())
        {
            return false;
        }
        //业务逻辑的处理
        if(!dealData())
        {
            return false;
        }
        //提交数据
        if(!submit())
        {
        	return false;
        }
        return true;
    }

    /**
     * 接受传入的数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(String aEdorNo, String aGrpContNo,GlobalInput gi,VData tVData)
    {
    	mEdorNo = aEdorNo;
    	mGrpContNo = aGrpContNo;
        mGI = gi;
        if(mEdorNo == null || "".equals(mEdorNo) || "null".equals(mEdorNo))
        {
            mErrors.addOneError("传入的数据不完整,缺少mEdorNo");
            return false;
        }
        if(mGrpContNo == null || "".equals(mGrpContNo) || "null".equals(mGrpContNo))
        {
            mErrors.addOneError("传入的数据不完整,缺少mGrpContNo");
            return false;
        }
        if(mGI == null )
        {
            mErrors.addOneError("传入的数据不完整,缺少mGI");
            return false;
        }
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(mGrpContNo);
        if(!tLCGrpContDB.getInfo())
        {
        	mErrors.addOneError("LCGrpCont的数据不完整");
            return false;
        }
        mLCGrpContSchema = tLCGrpContDB.getSchema();
        mLPDiskImportSet = CommonBL.getLPDiskImportSet(mEdorNo, mEdorType,mGrpContNo, BQ.IMPORTSTATE_SUCC);
        if (mLPDiskImportSet.size() == 0)
        {
            mErrors.addOneError("找不到磁盘导入数据！");
            return false;
        }
        mTransferData = (TransferData) tVData.getObjectByObjectName("TransferData", 0);

        mdoType = (String) mTransferData.getValueByName("doType");
        return true;
    }

    /**
     * 业务逻辑的处理
     * @return boolean
     */
    private boolean dealData()
    {
        if (!UpdateLCInsured())
        {
            return false;
        }
        if (!setLPInsured())
        {
            return false;
        }
        if (!setEdorInfo())
        {
            return false;
        }
        if (mdoType.equals("Q"))
        {
            if (!setLJAGet()) {
                return false;
            }
        }
        return true;
    }

    /**
     * 数据提交
     */
    private boolean submit()
    {
    	PubSubmit tPubSubmit = new PubSubmit();
        VData tVData = new VData();
        tVData.add(map);
        if(!tPubSubmit.submitData(tVData, ""))
        {
            System.out.println(tPubSubmit.mErrors.getErrContent());
            mErrors.addOneError("提交数库失败");
            return false;
        }
        return true;
    }

    /**
     * 校验数据
     * @return boolean
     */
    private boolean checkData()
    {
        if (!checkType())
        {
            return false;
        }
        if (!checkCustomers())
        {
            return false;
        }
        if (!checkBank())
        {
            return false;
        }
        if(mdoType.equals("Q"))
        {
            if (!chenkFinanceState()) {
                return false;
            }
        }
        return true;
    }

    /**
     * 检查客户的有效性
     * @return boolean
     */
    private boolean checkCustomers()
    {
        boolean flag = true;
        String tErrorReason;
        MMap tMap = new MMap();
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            tErrorReason="";
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
            if(tLPDiskImportSchema.getInsuredNo()!=null&&!tLPDiskImportSchema.getInsuredNo().equals(""))
            {//如果InsuredNo不为空
                tLCInsuredSchema=checkInsuredNo(tLPDiskImportSchema.getInsuredNo());
                if(tLCInsuredSchema!=null)
                {//如果InsuredNo正确,如果五要素都为空则回写五要素
                    if ((tLPDiskImportSchema.getInsuredName() == null ||
                         tLPDiskImportSchema.getInsuredName().equals("")) &&
                        (tLPDiskImportSchema.getSex() == null ||
                         tLPDiskImportSchema.getSex().equals("")) &&
                        (tLPDiskImportSchema.getBirthday() == null ||
                         tLPDiskImportSchema.getBirthday().equals("")) &&
                        (tLPDiskImportSchema.getIDType() == null ||
                         tLPDiskImportSchema.getIDType().equals("")) &&
                        (tLPDiskImportSchema.getIDNo() == null ||
                         tLPDiskImportSchema.getIDNo().equals("")))
                    {
                        tLPDiskImportSchema.setInsuredName(tLCInsuredSchema.getName());
                        tLPDiskImportSchema.setSex(tLCInsuredSchema.getSex());
                        tLPDiskImportSchema.setBirthday(tLCInsuredSchema.getBirthday());
                        tLPDiskImportSchema.setIDType(tLCInsuredSchema.getIDType());
                        tLPDiskImportSchema.setIDNo(tLCInsuredSchema.getIDNo());
                        tMap.put(tLPDiskImportSchema, "UPDATE");
                    }
                    else
                    {//如果不为空校验是否正确
                        if(tLCInsuredSchema.getName()!=null)
                        {
                            if(!tLCInsuredSchema.getName().equals(tLPDiskImportSchema.getInsuredName()))
                            {
                                tErrorReason = "被保人姓名不正确，导入无效！";
                            }
                        }
                        if(tLCInsuredSchema.getSex()!=null)
                        {
                            if(!tLCInsuredSchema.getSex().equals(tLPDiskImportSchema.getSex()))
                            {
                                tErrorReason = "被保人性别不正确，导入无效！";
                            }
                        }
                        if(tLCInsuredSchema.getBirthday()!=null)
                        {
                            if(!tLCInsuredSchema.getBirthday().equals(tLPDiskImportSchema.getBirthday()))
                            {
                                tErrorReason = "被保人出生日期 不正确，导入无效！";
                            }
                        }
                        if(tLCInsuredSchema.getIDType()!=null)
                        {
                            if(!tLCInsuredSchema.getIDType().equals(tLPDiskImportSchema.getIDType()))
                            {
                                tErrorReason = "被保人证件类型不正确，导入无效！";
                            }
                        }
                        if(tLCInsuredSchema.getIDNo()!=null)
                        {
                            if(!tLCInsuredSchema.getIDNo().equals(tLPDiskImportSchema.getIDNo()))
                            {
                                tErrorReason = "被保人证件号码不正确，导入无效！";
                            }
                        }
                    }
                }
                else
                {//如果InsuredNo不正确
                    tErrorReason = "被保人客户号不正确，导入无效！";
                }
            }
            else
            {//如果InsuredNo为空，校验五要素
                tLCInsuredSchema=checkInsuredInfo(tLPDiskImportSchema);
                if(tLCInsuredSchema!=null)
                {//如果正确更新InsuredNo
                    tLPDiskImportSchema.setInsuredNo(tLCInsuredSchema.getInsuredNo());
                    tMap.put(tLPDiskImportSchema, "UPDATE");
                }else
                {
                    tErrorReason = "保单下不存在该客户，导入无效！";
                }
            }

            if (!tErrorReason.equals(""))
            {
                String sql = "Update LPDiskImport " +
                        "Set State = '" + BQ.IMPORTSTATE_FAIL + "', " +
                        " ErrorReason = '"+tErrorReason+"', " +
                        " Operator = '" + mGI.Operator + "', " +
                        " ModifyDate = '" + mCurrentDate + "', " +
                        " ModifyTime = '" + mCurrentTime + "' " +
                        "where EdorNo = '" + mEdorNo + "' " +
                        "and EdorType = '" + mEdorType + "' " +
                        "and GrpContNo = '" + mGrpContNo + "' " +
                        "and SerialNo = '" +
                        tLPDiskImportSchema.getSerialNo() +
                        "' ";
                tMap.put(sql, "UPDATE");
                flag = false;
            }
        }
        VData data = new VData();
        data.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        if (flag == false)
        {
            mErrors.addOneError("导入数据中有无效客户");
            return false;
        }
        return true;
    }

    private LCInsuredSchema checkInsuredInfo(LPDiskImportSchema pLPDiskImportSchema)
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setGrpContNo(pLPDiskImportSchema.getGrpContNo());
        tLCInsuredDB.setName(pLPDiskImportSchema.getInsuredName());
        tLCInsuredDB.setSex(pLPDiskImportSchema.getSex());
        tLCInsuredDB.setBirthday(pLPDiskImportSchema.getBirthday());
        tLCInsuredDB.setIDType(pLPDiskImportSchema.getIDType());
        tLCInsuredDB.setIDNo(pLPDiskImportSchema.getIDNo());
        LCInsuredSet tLCInsuredSet = new LCInsuredSet();
        tLCInsuredSet = tLCInsuredDB.query();
        if (tLCInsuredSet.size() == 0)
        {
            return null;
        }
        return tLCInsuredSet.get(1);
    }
    private LCInsuredSchema checkInsuredNo(String insuredNo)
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setGrpContNo(mGrpContNo);
        tLCInsuredDB.setInsuredNo(insuredNo);
        LCInsuredSet tLCInsuredSet = new LCInsuredSet();
        tLCInsuredSet = tLCInsuredDB.query();
        if (tLCInsuredSet.size() == 0)
        {
            return null;
        }
        return tLCInsuredSet.get(1);
    }
    /**
     * 检查银行的有效性
     * @return boolean
     */
    /**
     * 检查银行的有效性
     * @return boolean
     */
    private boolean checkBank()
    {
        boolean flag = true;
        MMap tMap = new MMap();
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            String bankCode = tLPDiskImportSchema.getBankCode();
            String bankAccNo = tLPDiskImportSchema.getBankAccNo();
            String accName = tLPDiskImportSchema.getAccName();

            if (bankCode != null && !bankCode.equals("") &&
                bankAccNo != null && !bankAccNo.equals("") &&
                accName != null && !accName.equals("")) //如果银行代码，银行帐号，户名都不为空，则校验银行代码
            {
                LDBankDB tLDBankDB = new LDBankDB();
                tLDBankDB.setBankCode(bankCode);
                if (tLDBankDB.query().size() == 0)
                {
                    String sql = "Update LPDiskImport " +
                                 "Set State = '" + BQ.IMPORTSTATE_FAIL + "', " +
                                 " ErrorReason = '银行代码无效！', " +
                                 " Operator = '" + mGI.Operator +
                                 "', " +
                                 " ModifyDate = '" + mCurrentDate + "', " +
                                 " ModifyTime = '" + mCurrentTime + "' " +
                                 "where EdorNo = '" + mEdorNo + "' " +
                                 "and EdorType = '" + mEdorType + "' " +
                                 "and GrpContNo = '" + mGrpContNo + "' " +
                                 "and SerialNo = '" +
                                 tLPDiskImportSchema.getSerialNo() +
                                 "' ";
                    tMap.put(sql, "UPDATE");
                    flag = false;
                }
            }
            else if ((bankCode == null || bankCode.equals("")) &&
                       (bankAccNo == null || bankAccNo.equals("")) &&
                       (accName == null || accName.equals(""))) ////如果银行代码，银行帐号，户名都为空
            {
                flag = true;
            }
            else
            {
                String sql = "Update LPDiskImport " +
                             "Set State = '" + BQ.IMPORTSTATE_FAIL + "', " +
                             " ErrorReason = '银行代码,户名,帐号填写不完整!', " +
                             " Operator = '" + mGI.Operator +
                             "', " +
                             " ModifyDate = '" + mCurrentDate + "', " +
                             " ModifyTime = '" + mCurrentTime + "' " +
                             "where EdorNo = '" + mEdorNo + "' " +
                             "and EdorType = '" + mEdorType + "' " +
                             "and GrpContNo = '" + mGrpContNo + "' " +
                             "and SerialNo = '" +
                             tLPDiskImportSchema.getSerialNo() +
                             "' ";
                tMap.put(sql, "UPDATE");
                flag = false;
            }
        }
        VData data = new VData();
        data.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(data, ""))
        {
            mErrors.copyAllErrors(tPubSubmit.mErrors);
            return false;
        }
        if (flag == false)
        {
            mErrors.addOneError("导入数据中有无效的银行帐户信息");
        	return false;
        }
        return true;
    }

    /**
     * 把LCInsure中的信息存入P表
     */
    private boolean setLPInsured()
    {
    	map.put("delete from LPInsured where edorno='"+mEdorNo+"' and edortype='"+mEdorType+"'", "DELETE");
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            LCInsuredSet tLCInsuredSet = getLCInsured(tLPDiskImportSchema);
            for (int j = 1; j <= tLCInsuredSet.size(); j++)
            {
                LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
                mReflections.transFields(tLPInsuredSchema, tLCInsuredSet.get(j));
                tLPInsuredSchema.setEdorNo(mEdorNo);
                tLPInsuredSchema.setEdorType(mEdorType);
                tLPInsuredSchema.setOperator(mGI.Operator);
                tLPInsuredSchema.setMakeDate(mCurrentDate);
                tLPInsuredSchema.setMakeTime(mCurrentTime);
                tLPInsuredSchema.setModifyDate(mCurrentDate);
                tLPInsuredSchema.setModifyTime(mCurrentTime);
                map.put(tLPInsuredSchema, "INSERT");
            }

        }
        return true;
    }

    private LCInsuredSet getLCInsured(LPDiskImportSchema aLPDiskImportSchema)
    {
        LCInsuredDB tLCInsuredDB = new LCInsuredDB();
        tLCInsuredDB.setGrpContNo(aLPDiskImportSchema.getGrpContNo());
        String insuredNo = aLPDiskImportSchema.getInsuredNo();
        if (insuredNo != null)
        {
            tLCInsuredDB.setInsuredNo(insuredNo);
            return tLCInsuredDB.query();
        }else
        {
            tLCInsuredDB.setName(aLPDiskImportSchema.getInsuredName());
            tLCInsuredDB.setSex(aLPDiskImportSchema.getSex());
            tLCInsuredDB.setBirthday(aLPDiskImportSchema.getBirthday());
            tLCInsuredDB.setIDType(aLPDiskImportSchema.getIDType());
            tLCInsuredDB.setIDNo(aLPDiskImportSchema.getIDNo());
            return tLCInsuredDB.query();
        }
    }

//  修改LCInsure中的信息
    private boolean UpdateLCInsured()
    {
        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            LCInsuredSet tLCInsuredSet = getLCInsured(tLPDiskImportSchema);
            for (int j = 1; j <= tLCInsuredSet.size(); j++)
            {
                LCInsuredSchema tLCInsuredSchema = tLCInsuredSet.get(j);
                tLCInsuredSchema.setBankCode(tLPDiskImportSchema.getBankCode());
                tLCInsuredSchema.setBankAccNo(tLPDiskImportSchema.getBankAccNo());
                tLCInsuredSchema.setAccName(tLPDiskImportSchema.getAccName());
                tLCInsuredSchema.setOperator(mGI.Operator);
                tLCInsuredSchema.setModifyDate(mCurrentDate);
                tLCInsuredSchema.setModifyTime(mCurrentTime);
                map.put(tLCInsuredSchema, "UPDATE");
            }
        }
        return true;
    }

    //设置保全数据
    private boolean setEdorInfo()
    {
    	LGWorkDB tLGWorkDB = new LGWorkDB();
    	tLGWorkDB.setWorkNo(mEdorNo);
    	LGWorkSchema tLGWorkSchema = new LGWorkSchema();
    	if(!tLGWorkDB.getInfo())
    	{
    		mErrors.addOneError("tLGWorkDB的数据不完整");
            return false;
    	}
    	tLGWorkSchema = tLGWorkDB.getSchema();
    	tLGWorkSchema.setOperator(mGI.Operator);
    	tLGWorkSchema.setModifyDate(mCurrentDate);
    	tLGWorkSchema.setModifyTime(mCurrentTime);
    	tLGWorkSchema.setStatusNo(Task.WORKSTATUS_DONE);
    	tLGWorkSchema.setInnerSource("LPJOver");
    	map.put(tLGWorkSchema, "UPDATE");

    	LPEdorAppDB tLPEdorAppDB = new LPEdorAppDB();
    	tLPEdorAppDB.setEdorAcceptNo(mEdorNo);
    	LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
    	if(!tLPEdorAppDB.getInfo())
    	{
    		mErrors.addOneError("tLPEdorAppDB的数据不完整");
            return false;
    	}
    	tLPEdorAppSchema = tLPEdorAppDB.getSchema();
    	tLPEdorAppSchema.setEdorState(BQ.EDORSTATE_CONFIRM);
    	tLPEdorAppSchema.setConfOperator(mGI.Operator);
    	tLPEdorAppSchema.setConfDate(mCurrentDate);
    	tLPEdorAppSchema.setConfTime(mCurrentTime);
    	tLPEdorAppSchema.setModifyDate(mCurrentDate);
    	tLPEdorAppSchema.setModifyTime(mCurrentTime);
    	map.put(tLPEdorAppSchema, "UPDATE");

    	LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
    	mReflections.transFields(tLPGrpEdorMainSchema, tLPEdorAppSchema);
        tLPGrpEdorMainSchema.setEdorNo(mEdorNo);
        tLPGrpEdorMainSchema.setEdorAppNo(mEdorNo);
        tLPGrpEdorMainSchema.setGrpContNo(mGrpContNo);
        tLPGrpEdorMainSchema.setEdorAppDate(mCurrentDate);
        tLPGrpEdorMainSchema.setEdorValiDate(mCurrentDate);
        tLPGrpEdorMainSchema.setMakeDate(mCurrentDate);
        tLPGrpEdorMainSchema.setMakeTime(mCurrentTime);
        tLPGrpEdorMainSchema.setModifyDate(mCurrentDate);
        tLPGrpEdorMainSchema.setModifyTime(mCurrentTime);
        map.put(tLPGrpEdorMainSchema, "DELETE&INSERT");

    	LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
    	mReflections.transFields(tLPGrpEdorItemSchema, tLPGrpEdorMainSchema);
        tLPGrpEdorItemSchema.setEdorType(mEdorType);
        map.put(tLPGrpEdorItemSchema, "DELETE&INSERT");

        for (int i = 1; i <= mLPDiskImportSet.size(); i++)
        {
            LPDiskImportSchema tLPDiskImportSchema = mLPDiskImportSet.get(i);
            LBDiskImportSchema tLBDiskImportSchema = new LBDiskImportSchema();
            mReflections.transFields(tLBDiskImportSchema, tLPDiskImportSchema);
            map.put(tLBDiskImportSchema, "DELETE&INSERT");
        }
        String delDiskImport="delete from lpdiskimport where edorno ='"+mEdorNo+"' and edortype='"+mEdorType+"'";
        map.put(delDiskImport,"DELETE");

    	return true;
    }

    /**
     * 查看客户回复类型，只有个人给付才可以做批量变更
     * @return boolean
     */
    private boolean checkType()
    {
      String sql = "select * From lpedorespecialdata where EdorNo ='"+mLPDiskImportSet.get(1).getGrpContNo()+"' and EDORTYPE ='TJ' and Detailtype ='QUERYTYPE' and edorvalue='3' with ur";
      SSRS tSSRS = new ExeSQL().execSQL(sql);

      if(tSSRS.getMaxRow()==0)
      {
          mErrors.addOneError("只有客户回复为个人给付的保单才可以做此变更！");
          return false;
      }
      return true;
    }

    /**
     * 应付状态
     * @return boolean
     */

    private boolean chenkFinanceState()
    {
        for (int i = 1 ; i <= mLPDiskImportSet.size() ; i++)
        {
            String sql =" select ContNo From lccont where grpcontno ='"+mLPDiskImportSet.get(i).getGrpContNo()+"' "
                       +" and insuredname ='"+mLPDiskImportSet.get(i).getInsuredName()+"' "
                       +" and insuredidno ='"+mLPDiskImportSet.get(i).getIDNo()+"' ";
           SSRS tSSRS = new ExeSQL().execSQL(sql);
           String tContNo = tSSRS.GetText(1,1);
           if(tContNo.equals("") || tContNo =="")
           {
               mErrors.addOneError("查询团单下个单信息失败！");
               return false ;
           }
           LJAGetDB tLJAGetDB = new LJAGetDB();
           LJAGetSchema tLJAGetSchema = new LJAGetSchema();
           String getLJAGet =" select b.* from LJSGetDraw a, LJAGet b where grpcontno ='"+mLPDiskImportSet.get(i).getGrpContNo()
                                +"' and contno ='"+tContNo+"' and a.getnoticeno = b.actugetno";
           tLJAGetSchema = tLJAGetDB.executeQuery(getLJAGet).get(1).getSchema();

           if ("1".equals(tLJAGetSchema.getBankOnTheWayFlag())) {
               mErrors.addOneError("银行在途不能修改"+ tLJAGetSchema.getGetNoticeNo());
               return false;
           }

           if (null != tLJAGetSchema.getEnterAccDate() &&
               !tLJAGetSchema.getEnterAccDate().equals("")) {
               this.mErrors.addOneError("银行已经回盘不能修改!");
               return false;
           }

        }
        return true ;
    }


    private boolean setLJAGet()
    {
        for (int i = 1; i <= mLPDiskImportSet.size(); i++) {
            String sql = " select ContNo From lccont where grpcontno ='" +
                         mLPDiskImportSet.get(i).getGrpContNo() + "' "
                         + " and insuredname ='" +
                         mLPDiskImportSet.get(i).getInsuredName() + "' "
                         + " and insuredidno ='" + mLPDiskImportSet.get(i).getIDNo() +
                         "' ";
            SSRS tSSRS = new ExeSQL().execSQL(sql);
            String tContNo = tSSRS.GetText(1, 1);
            if (tContNo.equals("") || tContNo == "") {
                mErrors.addOneError("查询团单下个单:"+tContNo+"信息失败！");
                return false;
            }
            LJAGetDB tLJAGetDB = new LJAGetDB();
            LJAGetSchema tLJAGetSchema = new LJAGetSchema();
            String getLJAGet =" select b.* from LJSGetDraw a, LJAGet b where grpcontno ='" +mLPDiskImportSet.get(i).getGrpContNo()
                             +"' and contno ='" + tContNo +
                             "' and a.getnoticeno = b.actugetno"
                             ;
            tLJAGetSchema = tLJAGetDB.executeQuery(getLJAGet).get(1).getSchema();
            if (tLJAGetSchema == null)
            {
                mErrors.addOneError("查询保单:"+tContNo+"的应付信息失败！");
                return false;
            }
            tLJAGetSchema.setBankCode(mLPDiskImportSet.get(i).getBankCode());
            tLJAGetSchema.setBankAccNo(mLPDiskImportSet.get(i).getBankAccNo());
            tLJAGetSchema.setAccName(mLPDiskImportSet.get(i).getAccName());
            tLJAGetSchema.setCanSendBank(null);
            tLJAGetSchema.setModifyDate(mCurrentDate);
            tLJAGetSchema.setModifyTime(mCurrentTime);
            map.put(tLJAGetSchema,SysConst.UPDATE);
            if(!setLJSGetDraw(tLJAGetSchema))
            {
            	return false;
            }
        }
        return true ;
    }

    /**
     * 得到保单信息
     * @param contNo String
     * @return LCContSchema
     */
    public static LCContSchema getLCCont(String contNo) {
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(contNo);
        if (!tLCContDB.getInfo()) {
            return null;
        }
        return tLCContDB.getSchema();
    }


    public CErrors getErrors()
	{
		return mErrors;
	}
    
    private boolean setLJSGetDraw(LJAGetSchema aLJAGetSchema)
    {
    	String tGetNoticeNo = aLJAGetSchema.getActuGetNo();
    	LJSGetDB tLJSGetDB = new LJSGetDB();
    	tLJSGetDB.setGetNoticeNo(tGetNoticeNo);
    	LJSGetSchema tLJSGetSchema = null;
    	if(!tLJSGetDB.getInfo())
    	{
    		this.mErrors.addOneError("查询LJSGet失败-->"+tGetNoticeNo);
            return false;
    	}
    	tLJSGetSchema = tLJSGetDB.getSchema();
    	tLJSGetSchema.setBankCode(aLJAGetSchema.getBankCode());
    	tLJSGetSchema.setBankAccNo(aLJAGetSchema.getBankAccNo());
    	tLJSGetSchema.setAccName(aLJAGetSchema.getAccName());
    	tLJSGetSchema.setModifyDate(mCurrentDate);
    	tLJSGetSchema.setModifyTime(mCurrentTime);
        map.put(tLJSGetSchema,SysConst.UPDATE);
        
        LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
        tLJSGetDrawDB.setGetNoticeNo(tGetNoticeNo);
        LJSGetDrawSet tLJSGetDrawSet = tLJSGetDrawDB.query();
        if(tLJSGetDrawSet.size()==0)
    	{
    		this.mErrors.addOneError("查询LJSGetDraw失败-->"+tGetNoticeNo);
            return false;
    	}
        for(int i=1; i<=tLJSGetDrawSet.size(); i++)
        {
        	LJSGetDrawSchema tLJSGetDrawSchema = new LJSGetDrawSchema();
        	tLJSGetDrawSchema.setSchema(tLJSGetDrawSet.get(i));
        	tLJSGetDrawSchema.setBankCode(aLJAGetSchema.getBankCode());
        	tLJSGetDrawSchema.setBankAccNo(aLJAGetSchema.getBankAccNo());
        	tLJSGetDrawSchema.setAccName(aLJAGetSchema.getAccName());
        	tLJSGetDrawSchema.setModifyDate(mCurrentDate);
        	tLJSGetDrawSchema.setModifyTime(mCurrentTime);
            map.put(tLJSGetDrawSchema,SysConst.UPDATE);
        }
    	return true;
    }

    public static void main(String[] args)
    {

    }
}
