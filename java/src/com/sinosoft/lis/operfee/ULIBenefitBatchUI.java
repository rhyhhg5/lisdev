package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;

//程序名称：ULIBenefitBatchUI.java
//程序功能：万能老年关爱给付抽档
//创建日期：2009-09-03 
//创建人  ：zhanggm
//更新记录：更新人    更新日期     更新原因/内容

public class ULIBenefitBatchUI 
{
    //业务处理相关变量
    private VData mInputData;
    public CErrors mErrors = new CErrors();

    public ULIBenefitBatchUI() 
    {
    }

    public boolean submitData(VData cInputData, String cOperate) 
    {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        ULIBenefitBatchBL tULIBenefitBatchBL = new ULIBenefitBatchBL();
        System.out.println("Start submitData submitData...");
        tULIBenefitBatchBL.submitData(mInputData, cOperate);
        System.out.println("End submitData submitData...");

        mInputData = null;
        mInputData = tULIBenefitBatchBL.getResult();
        //如果有需要处理的错误，则返回
        if (tULIBenefitBatchBL.mErrors.needDealError()) 
        {
            this.mErrors.copyAllErrors(tULIBenefitBatchBL.mErrors);
            return false;
        }
        System.out.println("error num=" + mErrors.getErrorCount());
        return true;
    }
    public VData getResult()
    {
        return mInputData;
    }
}
