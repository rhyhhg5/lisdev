package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LYSendToBankDB;
import com.sinosoft.lis.vschema.LYSendToBankSet;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.db.LJSPayBDB;
import com.sinosoft.lis.vschema.LJSPayBSet;
import com.sinosoft.lis.schema.LJSPayBSchema;
import com.sinosoft.lis.db.LJTempFeeDB;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJTempFeeSet;
import com.sinosoft.lis.vschema.LJAPaySet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.schema.LCAppAccTraceSchema;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.LCAppAccTraceDB;
import com.sinosoft.lis.vschema.LCAppAccTraceSet;
import com.sinosoft.lis.xb.PRnewAppCancelBL;
import com.sinosoft.lis.xb.XBConst;
import com.sinosoft.lis.schema.LJTempFeeSchema;
import com.sinosoft.lis.db.LGPhoneHastenDB;
import com.sinosoft.lis.vschema.LGPhoneHastenSet;
import com.sinosoft.lis.taskservice.NextFeeHastenTask;
import com.sinosoft.lis.schema.LGPhoneHastenSchema;
import com.sinosoft.task.TaskPhoneFinishBL;
import com.sinosoft.lis.db.LCAppAccDB;
import com.sinosoft.utility.SysConst;

/**
 * <p>Title: lis</p>
 *
 * <p>Description:
 * 处理应收作废业务逻辑
 * a)	删除财务应收记录，应收备份表状态变更为已作废
 * b)	回退续保操作，删除续保临时数据
 * c)	若已收费，则财务暂收转入投保人帐户

 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class IndiLJSCancelBL {

    public CErrors mCErrors = new CErrors();

    private VData mInputData = new VData();

    private String mOperate = "";

    private GlobalInput mGlobalInput = new GlobalInput();

    private TransferData mTransferData = new TransferData();

    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();

    private boolean mIsRepeal = false;  //撤销标志

    private String mCancelMode = "";

    private String mGetNoticeNo = "";

    private String mCurrentDate = PubFun.getCurrentDate();

    private String mCurrentTime = PubFun.getCurrentTime();

    private LYSendToBankSet mLYSendToBankSet = new LYSendToBankSet();

    private LCContSet mLCContSet = new LCContSet();

    private LCContSchema mLCContSchema = new LCContSchema();

    private LJSPayBSet tLJSPayBSet = new LJSPayBSet();

    private LJSPayBSchema mLJSPayBSchema = new LJSPayBSchema();

    private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();

    private LJSPayBSet mLJSPayBSet = new LJSPayBSet();

    private String mPersonSql = "";

    private String mPersonBSql = "";

    private MMap map = new MMap();

    public IndiLJSCancelBL() {
    }

    /**
     * 为外部调用提供的接口，进行作废逻辑处理
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

       if(getSubmitMap(cInputData, cOperate) == null)
       {
           return false;
       }

        PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            System.out.println("delete fail!");
            return false;
        }

        return true;
    }

    /**
     * 为外部调用提供的接口，进行作废逻辑处理
     * @param cInputData VData
     * @param cOperate String
     * @return MMap
     */
    public MMap getSubmitMap(VData cInputData, String cOperate){

        mOperate = cOperate;

        mInputData = (VData) cInputData;

        if (!mOperate.equals("INSERT")) {

            return null;
        }

        if (!this.getInputData(mInputData)) {
            return null;
        }

        if (!this.checkData()) {
            return null;
        }

        if (!this.dealData()) {
            return null;
        }
        if(!prepareData())
        {
            this.bulidError("prepareData",
                            "存数据到MAP错误");
            return null;
        }

        if(!dealPhoneHasten())
        {
            return null;
        }

        return map;
    }

    /**
     * 个单应收记录作废/撤销类，在IndiLJSCancelBL.getSubmitMap方法 中增加对催缴工单的处理
     * @return boolean
     */
    private boolean dealPhoneHasten()
    {
        String sql = "select * "
                     + "from LGPhoneHasten "
                     + "where getNoticeNo = '"
                     + mLJSPaySchema.getGetNoticeNo() + "' "
                     + "   and HastenType = '"
                     + NextFeeHastenTask.HASTEN_TYPE_NEXTFEEP + "' "
                     + "   and FinishType is null ";
        LGPhoneHastenSet set = new LGPhoneHastenDB().executeQuery(sql);
        if(set.size() == 0)
        {
            //若无催缴就查不出来也
            return true;
        }

        LGPhoneHastenSchema schema = set.get(1);
        schema.setFinishType(mIsRepeal ? "3" : "2");
        schema.setRemark("应收记录" + mLJSPaySchema.getGetNoticeNo()
                         + (mIsRepeal ? "已撤销" : "已作废"));

        VData data = new VData();
        data.add(schema);
        data.add(mGlobalInput);

        TaskPhoneFinishBL bl = new TaskPhoneFinishBL();
        MMap tMMap = bl.getSubmitMap(data, "");
        if(tMMap == null)
        {
            mCErrors.copyAllErrors(bl.mErrors);
            return false;
        }
        this.map.add(tMMap);

        return true;
    }

    /**
     * 得到外部传入的数据
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {

        try {
            mGlobalInput.setSchema((GlobalInput) cInputData.
                                   getObjectByObjectName(
                                           "GlobalInput", 0));

            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
            if (mGlobalInput == null || mTransferData == null || mLJSPaySchema == null) {
                this.bulidError("getInputData", "数据不完整");
                return false;
            }
            mLJSPaySchema = ((LJSPaySchema) mInputData.getObjectByObjectName(
                    "LJSPaySchema", 0));
            LJSPayDB tLJSPayDB = new LJSPayDB();
            tLJSPayDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
            if (tLJSPayDB.getInfo() == false) {
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GrpDueFeeBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "没有查询到财务应收数据，请您确认!";
                this.mCErrors.addOneError(tError);
                return false;
            }
            mLJSPaySchema.setSchema(tLJSPayDB.getSchema());
            mCancelMode = (String) mTransferData.getValueByName("CancelMode");
            mGetNoticeNo = mLJSPaySchema.getGetNoticeNo();
            String isRepeal = (String)mTransferData.getValueByName("IsRepeal");
//          modify by xp 090730 页面续期应收作废不会设置终止缴费状态的错误修改
            if(isRepeal != null && !isRepeal.equals("")||("0".equals(mCancelMode))){
                mIsRepeal = true;
            }
        } catch (Exception ex) {
            this.mCErrors.addOneError("数据不完整");
            return false;
        }

        return true;
    }

    /**
     *校验操作的合法性
     * @return boolean
     */
    private boolean checkData() {

        if (mGetNoticeNo.equals("")) {
            this.bulidError("getInputData", "数据不完整");
            return false;
        }

        StringBuffer strSQL = new StringBuffer();
        strSQL.append(
                "SELECT COUNT(*) FROM LJSPay WHERE BankOnTheWayFlag='1'  AND ");
        strSQL.append("GetNoticeNo='" + mGetNoticeNo + "'");

        ExeSQL tExeSQL = new ExeSQL();
        String strCount = "";

        strCount = tExeSQL.getOneValue(
                "SELECT COUNT(*) FROM LJSPay WHERE BankOnTheWayFlag='1'  AND GetNoticeNo='" +
                mGetNoticeNo + "'");

        if (tExeSQL.mErrors.needDealError()) {
            this.mCErrors.addOneError("应收记录:" + mLJSPaySchema.getGetNoticeNo() +
                                      "查询银行在途标志时错误:" +
                                      tExeSQL.mErrors.getFirstError());
            return false;
        }

        if (!strCount.equals("") && !strCount.equals("0")) {
            this.mCErrors.addOneError("应收记录:" + mLJSPaySchema.getGetNoticeNo() +
                                      "操作发生错误:银行在途");
            return false;
        }


        //查询发盘数据
        String SqlStr =
                "select a.* from LYSendToBank a ,LJSPay b where a.PolNo= b.OtherNo"
                + " and b.OtherNoType='2' and paycode = getnoticeno  and b.GetNoticeNo='" +
                mLJSPaySchema.getGetNoticeNo() + "'";
        System.out.println(SqlStr);
        LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
        mLYSendToBankSet = tLYSendToBankDB.executeQuery(SqlStr);
        if (mLYSendToBankSet.size() != 0) {
            this.bulidError("dealData",
                            "应收记录:" + mLJSPaySchema.getGetNoticeNo() +
                            "操作发生错误:已经发盘，不能进行本操作");
            return false;
        }

        if (this.mCancelMode.equals("")) {
            this.bulidError("checkData", "数据不完整！");
            return false;
        }

        return true;
    }

    /**
     * 进行作废逻辑处理
     * @return boolean
     */
    private boolean dealData() {

        if(!dealPRnew()){
            return false;
        }

        //保单
        String SqlCont = "select a.* from LCCont a where a.ContNo='" +
                         mLJSPaySchema.getOtherNo() + "'";
        LCContDB tLCContDB = new LCContDB();
        System.out.println(SqlCont);
        mLCContSet = tLCContDB.executeQuery(SqlCont);
        if (mLCContSet.size() == 0) {
            this.bulidError("dealData",
                            "应收记录:" + mLJSPaySchema.getGetNoticeNo() +
                            "操作发生错误:保单不存在");
            return false;
        }

        mLCContSchema = mLCContSet.get(1);

        //下面查询出符合条件的LJSPayPerson
        String ljsPersonSql =
                " SELECT COUNT(*) FROM LJSPayPerson where GetNoticeNo='" +
                mLJSPaySchema.getGetNoticeNo() + "'";
        ExeSQL tExeSQL = new ExeSQL();
        String strPerbCont = tExeSQL.getOneValue(ljsPersonSql);
        if (tExeSQL.mErrors.needDealError()) {
            this.bulidError("dealData", "查询数据 LJSPayPerson 出错！");
            return false;
        }

        if (strPerbCont == null || strPerbCont == "0") {
            this.bulidError("dealData",
                            "应收记录:" + mLJSPaySchema.getGetNoticeNo() +
                            "操作发生错误:应收个人交费表缺少数据");

            return false;

        }

        //下面查询出符合条件的LJSPayB
        String ljsSqlB = "select * from LJSPayB where GetNoticeNo='" +
                         mLJSPaySchema.getGetNoticeNo() + "'";
        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        tLJSPayBSet = tLJSPayBDB.executeQuery(ljsSqlB);
        System.out.println(ljsSqlB);
        if (tLJSPayBSet.size() == 0) {
            this.bulidError("dealData",
                            "应收记录:" + mLJSPaySchema.getGetNoticeNo() +
                            "操作发生错误:应收总表备份表缺少数据");

            return false;
        }

        //下面查询出符合条件的LJSPayPersonB
        String ljsPersonSqlB =
                " SELECT COUNT(*) FROM LJSPayPersonB where GetNoticeNo='" +
                mLJSPaySchema.getGetNoticeNo() + "'";
        tExeSQL = new ExeSQL();
        String strPerCont = tExeSQL.getOneValue(ljsPersonSqlB);
        if (tExeSQL.mErrors.needDealError()) {
            this.bulidError("dealData", "查询数据 LJSPayPersonB 出错！");
            return false;
        }

        if (strPerCont == null || strPerCont == "0") {
            this.bulidError("dealData",
                            "应收记录:" + mLJSPaySchema.getGetNoticeNo() +
                            "操作发生错误:应收个人交费表备份表缺少数据");

            return false;

        }

        //未到帐处理
        String SqlFee =
                "select a.* from LJTempFee a  where a.tempfeeno='"+mLJSPaySchema.getGetNoticeNo() + "'";
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        System.out.println(SqlFee);
        mLJTempFeeSet = tLJTempFeeDB.executeQuery(SqlFee);
//        if (mLJTempFeeSet.size() != 0 ) {
//            this.bulidError("dealData",
//                            "应收记录:" + mLJSPaySchema.getGetNoticeNo() +
//                            "操作发生错误:已付费，未核销");
//            return false;
//        }

        mPersonSql = " DELETE FROM LJSPayPerson WHERE GetNoticeNo='" +
                     mLJSPaySchema.getGetNoticeNo() + "'";
        System.out.println(mPersonSql);
        mPersonBSql =
                " UPDATE  LJSPayPersonB SET DealState='"
                + (mIsRepeal ? "3" : "2")
                + "' ,CancelReason='" + mCancelMode
                + "' WHERE GetNoticeNo='" +
                mLJSPaySchema.getGetNoticeNo() + "'";
        System.out.println(mPersonBSql);

        //qulq 2007-2-9 add
        if(mIsRepeal)
        {
            String updatePol = "Update lcpol set polState ='"+BQ.POLSTATE_XBEND
                +"' where polno in (select distinct polno from LJSPayPersonB where GetNoticeNo ='"
                +mLJSPaySchema.getGetNoticeNo()+"')";
              map.put(updatePol,SysConst.UPDATE);
        }
        else
        {
        	String tmpContNo = mLJSPaySchema.getOtherNo();
        	String tCancelMode = (String)mTransferData.getValueByName("CancelMode");
        	//附加万能险种应收记录作废，不置缓交
            if(CommonBL.hasULIRisk(tmpContNo) && CommonBL.checkULIHuan(tmpContNo) && ("1".equals(tCancelMode)) && !CommonBL.hasSubULIRisk(tmpContNo) && !has340501(tmpContNo))
            {
            	String newHun="select 1 from lcpol a,ldcode b where a.contno='"+tmpContNo+"' and a.riskcode=b.code and b.codetype='ULIDEFER' ";
            	String newFlag=tExeSQL.getOneValue(newHun);
            	//如果保单不是新缓交状态的再置老状态
            	if(null==newFlag || "".equals(newFlag)){
            		String updatePolflag1 = "Update lcpol set standbyflag1 ='1' where ContNo='" + tmpContNo + "'";
            		map.put(updatePolflag1,SysConst.UPDATE);
            	}
            }
        }

        //作废处理
        for (int i = 1; i <= tLJSPayBSet.size(); i++) {
            mLJSPayBSchema = tLJSPayBSet.get(i);
            mLJSPayBSchema.setDealState(mIsRepeal ? "3" : "2"); //应收作废
            mLJSPayBSchema.setCancelReason(mCancelMode); //应收作废原因
            mLJSPayBSchema.setModifyDate(mCurrentDate);
            mLJSPayBSchema.setModifyTime(mCurrentTime);
            mLJSPayBSet.add(mLJSPayBSchema);
        }
        return true;
    }

    /**
     * 若已财务收费，则将暂收金额放入投保人帐户
     * @param tLJTempFeeSet LJTempFeeSet
     * @return MMap
     */
    private MMap dealTempFee()
    {
        MMap tMap = new MMap();

        double moneyTempFee = 0;
        for(int i = 1; i <= mLJTempFeeSet.size(); i++)
        {
			//判断财务暂收数据是否到账确认
			if(mLJTempFeeSet.get(i).getEnterAccDate()!=null&&!"".equals(mLJTempFeeSet.get(i).getEnterAccDate())&&!"null".equals(mLJTempFeeSet.get(i).getEnterAccDate())){
				 moneyTempFee += mLJTempFeeSet.get(i).getPayMoney();
			}else{
				moneyTempFee +=0;
			}
            LJTempFeeSchema schema = mLJTempFeeSet.get(i).getSchema();
            schema.setConfFlag("2");
            tMap.put(schema, "UPDATE");
        }
        //二次以上作废有用 modify by whl
        boolean isTempfee=true;
        if(moneyTempFee==0)
        {
        	String sql="select sumduepaymoney from ljspaypersonb " 
        		      +"where paytype='YEL' and getnoticeno='"+mLJSPaySchema.getGetNoticeNo()+"'";
        	ExeSQL tExeSQL=new ExeSQL();
        	SSRS tMsgNotifySSRS =tExeSQL.execSQL(sql);
        	if(tMsgNotifySSRS!=null)
        	{
	        	for (int i = 1; i <= tMsgNotifySSRS.getMaxRow(); i++) {
	            	String sumduepaymoney = tMsgNotifySSRS.GetText(i, 1);
	            	if((!sumduepaymoney.equals("0"))&&sumduepaymoney!=null)
	            	{
	            		double money=Double.parseDouble(sumduepaymoney);
	            		moneyTempFee+=(-money);
	            		isTempfee=false;
	            	}
	        	}
        	}
        }  
        
        if(Math.abs(moneyTempFee - 0) < 0.000001)
        {
        	//判断是否有收费暂收未到账确认
        	if(mLJTempFeeSet.size()>0){
        		return tMap;
        	}else{
        		return null;
        	}
        	
        }
        AppAcc tAppAcc = new AppAcc();

        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
        tLCAppAccTraceSchema.setCustomerNo(mLCContSchema.getAppntNo());
        tLCAppAccTraceSchema.setAccType("2");
        tLCAppAccTraceSchema.setOtherNo(mLCContSchema.getContNo());
        tLCAppAccTraceSchema.setBakNo(mLJSPaySchema.getGetNoticeNo());
        tLCAppAccTraceSchema.setOtherType("2");
        tLCAppAccTraceSchema.setDestSource("13");
        tLCAppAccTraceSchema.setMoney(moneyTempFee);
        tLCAppAccTraceSchema.setOperator(mGlobalInput.Operator);
        tMap.add(tAppAcc.accShiftToXSYIJ(tLCAppAccTraceSchema,isTempfee));

        return tMap;
    }

    /**
     * 撤销续保数据
     * @return boolean
     */
    private boolean dealPRnew()
    {
        String sql = "  select 1 "
                     + "from LCRnewStateLog "
                     + "where contNo = '" + mLJSPaySchema.getOtherNo() + "' "
                     + "   and state != '" + XBConst.RNEWSTATE_DELIVERED + "' ";
        ExeSQL e = new ExeSQL();
        String rs = e.getOneValue(sql);
        if(rs.equals("") || rs.equals("null"))
        {
            //没有续保数据，不需要续保撤销动作
            return true;
        }

        //先进行续保数据撤销
        PRnewAppCancelBL tPRnewAppCancelBL = new PRnewAppCancelBL();
        LCContSchema schema = new LCContSchema();
        schema.setContNo(mLJSPaySchema.getOtherNo());

        VData data = new VData();
        data.add(schema);
        data.add(mGlobalInput);
        data.add(mLJSPaySchema);

        MMap tMMap = tPRnewAppCancelBL.getSubmitMap(data, "");
        if(map == null && tPRnewAppCancelBL.mErrors.needDealError())
        {
            this.mCErrors.copyAllErrors(tPRnewAppCancelBL.mErrors);
            return false;
        }
        map.add(tMMap);

        return true;
    }

    /**
     * 准备数据
     * @return boolean
     */
    public boolean prepareData() {
        map.put(mPersonSql, "DELETE");
        map.put(mLJSPaySchema, "DELETE");
//        map.put(mLCContSchema, "UPDATE");
        map.put(mLJSPayBSet, "UPDATE");
        map.put(mPersonBSql, "UPDATE");

        map.add(dealAccBala());
        map.add(dealTempFee());

        mInputData.add(map);
        return true;
    }

    /*
     处理续期抵扣过的金额
     恢复可领金额，删除抵扣轨迹
     * @return
     */
    private MMap dealAccBala()
    {
        LCAppAccDB tLCAppAccDB = new LCAppAccDB();
        tLCAppAccDB.setCustomerNo(mLCContSchema.getAppntNo());
        if(!tLCAppAccDB.getInfo())
        {
            return null;
        }

        LCAppAccTraceDB tOldLCAppAccTraceDB = new LCAppAccTraceDB();
        tOldLCAppAccTraceDB.setCustomerNo(mLCContSchema.getAppntNo());
        tOldLCAppAccTraceDB.setOtherNo(mLCContSchema.getContNo());
        tOldLCAppAccTraceDB.setOtherType("3");
        tOldLCAppAccTraceDB.setAccType("0");
        tOldLCAppAccTraceDB.setDestSource("02");
        tOldLCAppAccTraceDB.setBakNo(mLJSPaySchema.getGetNoticeNo());
        LCAppAccTraceSet tOldLCAppAccTraceSet = tOldLCAppAccTraceDB.query();

        //若没有续期
        if(tOldLCAppAccTraceSet.size() == 0)
        {
            return null;
        }

        LCAppAccTraceSchema tOldLCAppAccTraceShema
            = tOldLCAppAccTraceSet.get(1).getSchema();

        tLCAppAccDB.setAccGetMoney(tLCAppAccDB.getAccGetMoney()
                                   - tOldLCAppAccTraceShema.getMoney());
        tLCAppAccDB.setOperator(mGlobalInput.Operator);
        tLCAppAccDB.setModifyDate(this.mCurrentDate);
        tLCAppAccDB.setModifyTime(this.mCurrentTime);

        MMap tMap = new MMap();
        tMap.put(tLCAppAccDB.getSchema(), SysConst.UPDATE);
        tMap.put(tOldLCAppAccTraceShema, SysConst.DELETE);

        return tMap;
    }
    
    private boolean has340501(String ContNo){
        LCPolDB tLCPolDB = new LCPolDB();
        tLCPolDB.setContNo(ContNo);
        LCPolSet tLCPolSet = tLCPolDB.query();
        for (int i = 1; i <= tLCPolSet.size(); i++)
        {   
            if (tLCPolSet.get(i).getRiskCode().equals("340501") || tLCPolSet.get(i).getRiskCode().equals("340601"))
            {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param cFunctionName String
     * @param cErrorsMsg String
     */
    private void bulidError(String cFunctionName, String cErrorsMsg) {

        CError tCError = new CError();
        tCError.moduleName = "IndiLJSCancelBL";
        tCError.functionName = cFunctionName;
        tCError.errorMessage = cErrorsMsg;

        this.mCErrors.addOneError(tCError);
    }

    public static void main(String[] args) {

//        LJSPaySchema tLJSPaySchema = new LJSPaySchema();
//        tLJSPaySchema.setGetNoticeNo("31000003372");
//
//        TransferData tTransferData = new TransferData();
//        tTransferData.setNameAndValue("CancelMode", "3");
//
//        GlobalInput g = new GlobalInput();
//        g.ComCode = "86";
//        g.Operator = "endor0";
//
//        VData tVData = new VData();
//        tVData.addElement(tLJSPaySchema);
//        tVData.addElement(g);
//        tVData.addElement(tTransferData);
//
//        IndiLJSCancelBL bl = new IndiLJSCancelBL();
//        if(!bl.submitData(tVData, "INSERT"))
//        {
            System.out.println(Integer.parseInt("1439.41"));
//        }
    }
}
