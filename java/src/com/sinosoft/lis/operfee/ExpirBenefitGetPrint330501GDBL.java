package com.sinosoft.lis.operfee;

import com.sinosoft.lis.f1print.PrintService;
import com.sinosoft.lis.schema.LJSGetSchema;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.schema.LCBnfSchema;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCAddressSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TextTag;
import com.sinosoft.lis.db.LDBankDB;
import com.sinosoft.lis.db.LABranchGroupDB;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.db.LAComDB;
import com.sinosoft.lis.db.LCBnfDB;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCAddressDB;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.ListTable;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.schema.LJSGetDrawSchema;
import com.sinosoft.lis.vschema.LCAddressSet;
import com.sinosoft.lis.vschema.LABranchGroupSet;
import com.sinosoft.lis.db.LJSGetDrawDB;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.db.LCAppntDB;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.db.LJSGetDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class ExpirBenefitGetPrint330501GDBL  implements PrintService{

    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    /** 全局数据 */
    private GlobalInput mGlobalInput = new GlobalInput();

    private VData mResult = new VData();

    private LCContSchema tLCContSchema = new LCContSchema();
    private LCAddressSchema tLCAddressSchema = new LCAddressSchema();
    private LJSGetSchema tLJSGetSchema = new LJSGetSchema();
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
//交费信息
    private String mOperate = "";
    TextTag textTag = new TextTag(); //新建一个TextTag的实例tLJSPaySchema


    public ExpirBenefitGetPrint330501GDBL() {
    }
    //By WangJH

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        System.out.println("ExpirBenefitGetPrint330501GBBL begin");
        mOperate = cOperate;
        // 得到外部传入的数据，将数据备份到本类中
        if (!getInputData(cInputData)) {
            return false;
        }

        mResult.clear();

        // 准备所有要打印的数据
        if (!getPrintData()) {
            return false;
        }

        if (!dealPrintMag()) {
            return false;
        }

        System.out.println("ExpirBenefitGetPrint330501GBBL end");
        return true;
    }

    private void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();

        cError.moduleName = "LCContF1PBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }


    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData cInputData) {
        mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0);
        LOPRTManagerSchema ttLOPRTManagerSchema = new LOPRTManagerSchema();
        ttLOPRTManagerSchema = (LOPRTManagerSchema) cInputData.
                               getObjectByObjectName("LOPRTManagerSchema", 0);
        LJSGetDB tLJSGetDB = new LJSGetDB();
        if (ttLOPRTManagerSchema == null) {
            tLJSGetDB = (LJSGetDB) cInputData.getObjectByObjectName("LJSGetDB",
                    0);
        } else {
            tLJSGetDB.setGetNoticeNo(ttLOPRTManagerSchema.getStandbyFlag2());
        }
        if (tLJSGetDB == null) {
            buildError("getInputData", "为空--VTS打印入口没有得到足够的信息！");
            return false;
        }
        tLJSGetSchema = tLJSGetDB.query().get(1);
        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }


    private boolean getPrintData() {
        LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
        tLJSGetDrawDB.setGetNoticeNo(tLJSGetSchema.getGetNoticeNo());
        LJSGetDrawSchema tLJSGetDrawSchema = tLJSGetDrawDB.query().get(1);
        String ContNo = tLJSGetDrawSchema.getContNo();
        System.out.println("ContNo=" + ContNo);
        String sql = "";
        String sqlone = "";

        //获取投保人地址
        LCAppntDB tLCAppntDB = new LCAppntDB();
        tLCAppntDB.setContNo(ContNo);

        if (!tLCAppntDB.getInfo()) {
            CError.buildErr(this,
                            "投保人表中缺少数据");
            return false;
        }

        sql = "select * from LCAddress where CustomerNo=(select appntno from LCCont where contno='" +
              ContNo + "') AND AddressNo = '" + tLCAppntDB.getAddressNo() +
              "'";
        System.out.println("sql=" + sql);
        LCAddressDB tLCAddressDB = new LCAddressDB();
        LCAddressSet tLCAddressSet = tLCAddressDB.executeQuery(sql);
        tLCAddressSchema = tLCAddressSet.get(tLCAddressSet.size()).
                           getSchema();

        sqlone = "select * from lccont where contno='" + ContNo + "'";
        System.out.println("sqlone=" + sqlone);
        LCContDB tLCContDB = new LCContDB();
        LCContSet tLCContSet = tLCContDB.executeQuery(sqlone);
        tLCContSchema = tLCContSet.get(1).getSchema();

        XmlExport xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("ExpirBenefitGetPrint.vts", "printer");
        
        textTag.add("JetFormType", "MX004");
        //借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='" +
                            mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            buildError("getInputData", "操作员机构查询出错！");
            return false;
        }
        String printcom =
                "select codename from ldcode where codetype='pdfprintcom' and code='" +
                comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);
        textTag.add("ManageComLength4", printcode);
        textTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        if ("batch".equals(mOperate)) {
            textTag.add("previewflag", "0");
        } else {
            textTag.add("previewflag", "1");
        }

        switch (Integer.parseInt(tLJSGetSchema.getPayMode())) {
        case 1:
        case 2:
        case 3:
        case 5:
        case 6:
        case 7:
        case 9:
            xmlexport.addDisplayControl("displaybank");
            break;
        case 4:
            xmlexport.addDisplayControl("displaybank");
            break;
        default:
        	xmlexport.addDisplayControl("displaycash");
        	break;
        }
        ;
        
        textTag.add("Operator", mGlobalInput.Operator);
        setFixedInfo();
        textTag.add("GrpZipCode", tLCAddressSchema.getZipCode());
        System.out.println("GrpZipCode=" + tLCAddressSchema.getZipCode());
        textTag.add("GrpAddress", tLCAddressSchema.getPostalAddress());
        String appntPhoneStr = " ";
        if (tLCAddressSchema.getPhone() != null &&
            !tLCAddressSchema.getPhone().equals("")) {
            appntPhoneStr += tLCAddressSchema.getPhone() + "、";
        }
        if (tLCAddressSchema.getHomePhone() != null &&
            !tLCAddressSchema.getHomePhone().equals("")) {
            appntPhoneStr += tLCAddressSchema.getHomePhone() + "、";
        }
        if (tLCAddressSchema.getCompanyPhone() != null &&
            !tLCAddressSchema.getCompanyPhone().equals("")) {
            appntPhoneStr += tLCAddressSchema.getCompanyPhone() + "、";
        }
        if (tLCAddressSchema.getMobile() != null &&
            !tLCAddressSchema.getMobile().equals("")) {
            appntPhoneStr += tLCAddressSchema.getMobile() + "、";
        }
        appntPhoneStr = appntPhoneStr.substring(0, appntPhoneStr.length() - 1);
        textTag.add("AppntPhone", appntPhoneStr);
        textTag.add("LinkMan1", tLCContSchema.getAppntName());
        textTag.add("AppntNo", tLCContSchema.getAppntNo());

        textTag.add("ContNo", ContNo);
        System.out.println("ContNo=" + "" + ContNo);
        textTag.add("AppntName", tLCContSchema.getAppntName());
        textTag.add("GetNoticeNo", tLJSGetSchema.getGetNoticeNo());
        textTag.add("MakeDate", tLJSGetSchema.getMakeDate());
        textTag.add("bankAccNo", tLJSGetSchema.getBankAccNo());
        textTag.add("AccName", tLJSGetSchema.getAccName());
        textTag.add("PayToDate",
                CommonBL.decodeDate(PubFun.calDate(tLCContSchema.
            getCInValiDate(), 0, "D", null))); //转换格式，显示为24时
        
//        String strSql = "select max(makedate) from ljsgetdraw where ContNo = '" + tLCContSchema.getContNo() + "' with ur";
//        String payDate = new ExeSQL().getOneValue(strSql);
//        
//        textTag.add("PayDate",
//                CommonBL.decodeDate(PubFun.calDate(payDate, 15, "D", null)));
        
        textTag.add("PayDate",CommonBL.decodeDate(PubFun.calDate(tLJSGetSchema.getGetDate(), 0, "D", null)));
        
        textTag.add("shouldDate", tLJSGetSchema.getGetDate());
        textTag.add("bankAccNo", tLJSGetSchema.getBankAccNo());
        textTag.add("Drawer", tLCContSchema.getAppntName());
        textTag.add("sumgetMoney", tLJSGetSchema.getSumGetMoney()); //合计给付金额。
        textTag.add("DrawerID", tLJSGetSchema.getDrawerID());
        textTag.add("PayEndDate",tLCContSchema.getCInValiDate()); //转换格式，显示为24时
        textTag.add("CreatDate", tLJSGetSchema.getMakeDate());
        
//添加被保人和受益人信息 
        textTag.add("InsuredName", tLCContSchema.getInsuredName());
        textTag.add("InsuredIDType",getIDType(tLCContSchema.getInsuredIDType()));
        textTag.add("InsuredIDNo", tLCContSchema.getInsuredIDNo());
        String tPolNo = tLJSGetDrawSchema.getPolNo();
        LCBnfDB tbnfdb=new LCBnfDB();
        tbnfdb.setPolNo(tPolNo);
        tbnfdb.setBnfGrade("1");
        LCBnfSchema tLCBnfSchema = tbnfdb.query().get(1);
        if(!(tLCBnfSchema==null))
        {	
        textTag.add("Customerno", tLCBnfSchema.getCustomerNo());
        textTag.add("CustomerIDType",getIDType(tLCBnfSchema.getIDType()));
        textTag.add("CustomerName", tLCBnfSchema.getName());
        }
        else
        {
        textTag.add("Customerno", "");
        textTag.add("CustomerIDType","");
        textTag.add("CustomerName", "");	
        }
       

        ExeSQL tExeSQL = new ExeSQL();

        String banksql = " select bankname From ldbank where bankcode in(select bankcode from lccont where contno ='" +
                         tLCContSchema.getContNo() + "')";

        String sqlMoney =
                " select getmoney from ljsgetdraw a where dutycode !='000000' "
                +
                " and getnoticeno in (select getnoticeno from ljsget where othernotype ='20' "
                + " and otherno ='" + tLJSGetSchema.getOtherNo() + "')"
                ;
        textTag.add("BankName", tExeSQL.getOneValue(banksql));
//        textTag.add("SumMoney", Double.parseDouble(tExeSQL.getOneValue(sqlMoney)));
        String sexCode = tLCAppntDB.getAppntSex();
        String sex;
        if (sexCode == null) {
            sex = "先生/女士";
        } else if (sexCode.equals("0")) {
            sex = "先生";
        } else if (sexCode.equals("1")) {
            sex = "女士";
        } else {
            sex = "先生/女士";
        }
        textTag.add("LinkManSex", sex);

        //业务员信息
        String agntPhone = "";
        String temPhone = "";
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(tLCContSchema.getAgentCode());
        tLaAgentDB.getInfo();
        textTag.add("AgentName", tLaAgentDB.getName());
        textTag.add("AgentCode", tLaAgentDB.getGroupAgentCode());
        temPhone = tLaAgentDB.getMobile();
        if (temPhone == null || temPhone.equals("") || temPhone.equals("null")) {
            agntPhone = tLaAgentDB.getPhone();
        } else {
            agntPhone = temPhone;
        }
        textTag.add("Phone", agntPhone);
        textTag.add("AgentPhone", agntPhone);

        //机构信息
        LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
        String branchSQL = " select * from LABranchGroup where agentgroup = "
                           +
                " (select subStr(branchseries,1,12) from LABranchGroup where agentgroup = "
                           +
                " (select agentgroup from laagent where agentcode ='"
                           + tLaAgentDB.getAgentCode() + "'))"
                           ;
        LABranchGroupSet tLABranchGroupSet = tLABranchGroupDB.executeQuery(
                branchSQL);
        if (tLABranchGroupSet == null || tLABranchGroupSet.size() == 0) {
            CError.buildErr(this, "查询业务员机构失败");
            return false;
        }
        textTag.add("AgentGroup", tLABranchGroupSet.get(1).getName());

        textTag.add("BarCode1", tLJSGetSchema.getOtherNo());
        textTag.add("BarCodeParam1",
                    "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

        //缴费方式
        String payMode = "";
        if ("1".equals(tLJSGetSchema.getPayMode())) 
        {
        	payMode = "现金";
        }
        else if ("4".equals(tLJSGetSchema.getPayMode())) 
        {
            LDBankDB tLDBankDB = new LDBankDB();
            tLDBankDB.setBankCode(tLJSGetSchema.getBankCode());
            tLDBankDB.getInfo();

            payMode = "银行转帐，开户银行：" + tLDBankDB.getBankName() + "，帐户名：" +
            tLJSGetSchema.getAccName() + "，帐号" +
            tLJSGetSchema.getBankAccNo();
        }
        else if ("9".equals(tLJSGetSchema.getPayMode())) 
        {
        	payMode = "其它";
        	String tBankCode = tLJSGetSchema.getBankCode();
        	if(tBankCode!=null && !"".equals(tBankCode))
        	{
        		LDBankDB tLDBankDB = new LDBankDB();
                tLDBankDB.setBankCode(tLJSGetSchema.getBankCode());
                tLDBankDB.getInfo();
                payMode += "，开户银行：" + tLDBankDB.getBankName(); 
        	}
        	if(tLJSGetSchema.getAccName()!=null && !"".equals(tLJSGetSchema.getAccName()))
        	{
                payMode += "，帐户名：" + tLJSGetSchema.getAccName() ;
        	}
        	if(tLJSGetSchema.getBankCode()!=null && !"".equals(tBankCode))
        	{
                payMode += "，帐号：" + tLJSGetSchema.getBankAccNo();
        	}
        } 
        else 
        {
            payMode = "其它";
        }
        textTag.add("PayMode", payMode);
        
        String tAgentCom = tLCContSchema.getAgentCom();
        String tAgentComName = "";
        if(tAgentCom==null)
        {
        	tAgentCom = "";
        }
        if(!tAgentCom.equals(""))
        {
        	LAComDB tLAComDB= new LAComDB();
        	tLAComDB.setAgentCom(tAgentCom);
        	tLAComDB.getInfo();
        	tAgentComName = tLAComDB.getName();
        }
        textTag.add("AgentCom", tAgentCom);
        textTag.add("AgentComName", tAgentComName);

        if (textTag.size() > 0) {
            xmlexport.addTextTag(textTag);
        }

        
        xmlexport.outputDocumentToFile("C:\\", "ExpirBenefitPrint330501GD");
        mResult.clear();
        mResult.addElement(xmlexport);

        return true;
    }

    private void setFixedInfo() 
    {
        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLCContSchema.getManageCom());
        tLDComDB.getInfo();
        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("letterservicename", tLDComDB.getLetterServiceName());
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getLetterServicePostZipcode());
        textTag.add("ComName", tLDComDB.getLetterServiceName());
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        textTag.add("Fax", tLDComDB.getFax());
    }
    
    /**
     *  得到ID类型
     * @param String
     * @return String
     */
    private String getIDType(String tIDType) 
    {
    	String tType=new String();
    	switch (Integer.parseInt(tIDType)) 
    	{
        case 0:tType="身份证";
        case 1:tType="护照";
        case 2:tType="军官证";
        case 3:tType="工作证";
        case 4:tType="其它";
        }
    	return tType;

    }


    public static void main(String[] a) 
    {

    }

    /**
     * 加入到打印列表
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealPrintMag() 
    {
        String tLimit = PubFun.getNoLimit(tLCContSchema.getManageCom());
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNo(tLCContSchema.getContNo());
        mLOPRTManagerSchema.setOtherNoType("00");
        mLOPRTManagerSchema.setCode("MX004");
        mLOPRTManagerSchema.setManageCom(tLCContSchema.getManageCom());
        mLOPRTManagerSchema.setAgentCode(tLCContSchema.getAgentCode());
        mLOPRTManagerSchema.setReqCom(tLCContSchema.getManageCom());
        mLOPRTManagerSchema.setReqOperator(tLCContSchema.getOperator());
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        mLOPRTManagerSchema.setStandbyFlag2(tLJSGetSchema.getGetNoticeNo()); //这里存放 （也为交费收据号）
        mLOPRTManagerSchema.setStandbyFlag1(tLJSGetSchema.getOtherNo());
        mResult.addElement(mLOPRTManagerSchema);
    	
        return true;
    }

}
