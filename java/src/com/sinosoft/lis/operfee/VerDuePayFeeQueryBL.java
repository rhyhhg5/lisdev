/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.operfee;

import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.vschema.LJSPaySet;
import com.sinosoft.utility.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单查询业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class VerDuePayFeeQueryBL
{
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    /** 往后面传输数据的容器 */
    private VData mResult = new VData();
    /** 数据操作字符串 */
    private String mOperate;

//    private String CurrentDate = PubFun.getCurrentDate();
//    private String CurrentTime = PubFun.getCurrentTime();

    /** 业务处理相关变量 */
    /** 应收总费表 */
    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();
    private LJSPaySet mLJSPaySet = new LJSPaySet();

    public VerDuePayFeeQueryBL()
    {}
    
    public static void main(String [] args)
    {
    	 VerDuePayFeeQueryUI tVerDuePayFeeQueryUI = new VerDuePayFeeQueryUI();
    	 VData tVData = new VData();
         LJSPaySchema tLJSPaySchema = new LJSPaySchema();
         tLJSPaySchema.setGetNoticeNo("");
//    	 LJSPaySchema tLJSPaySchema = null ;
         tVData.add(tLJSPaySchema);
         GlobalInput tGI = new GlobalInput();
         tVData.add(tGI);
         tVerDuePayFeeQueryUI.submitData(tVData, "QUERY");
         tVData.clear();
         tVData = tVerDuePayFeeQueryUI.getResult();
    }

    /**
     * 传输数据的公共方法
     * @param cInputData VData
     * @param cOperate String
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
//        System.out.println("---getInputData---");

        //进行业务处理
        if (cOperate.equals("QUERYDETAIL"))
        {
            if (!queryLJVerDuePayFeeDetail())
            {
                return false;
            }
        }
        else
        {
            if (!queryLJVerDuePayFee())
            {
                return false;
            }
//            System.out.println("---queryLJVerDuePayFee---");
        }

        return true;
    }

    public boolean submitDataForMult(VData cInputData, String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
        {
            return false;
        }
//        System.out.println("---getInputData---");

        //进行业务处理
        if (!queryMultLJVerDuePayFee())
        {
            return false;
        }
//        System.out.println("---queryLJVerDuePayFee---");

        return true;
    }

    public VData getResult()
    {
        return mResult;
    }

    /**
     * 从输入数据中得到所有对象
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData)
    {
        // 检验查询条件
        mLJSPaySchema.setSchema((LJSPaySchema) cInputData.getObjectByObjectName("LJSPaySchema", 0));
        if (mLJSPaySchema == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VerDuePayFeeQueryBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "查询催收记录失败";
            this.mErrors.addOneError(tError);
            return false;
        }
        String tGetNoticeNo = mLJSPaySchema.getGetNoticeNo();
        if(tGetNoticeNo==null || tGetNoticeNo.equals("null") || tGetNoticeNo.equals(""))
        {
//        	 @@错误处理
            CError tError = new CError();
            tError.moduleName = "VerDuePayFeeQueryBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "催收记录的应收号码丢失";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }


    /**
     * 查询应收总表信息
     * 输出：如果发生错误则返回false,否则返回true
     * @return boolean
     */
    private boolean queryLJVerDuePayFee()
    {
        // 应收总表
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setSchema(mLJSPaySchema);

        mLJSPaySet = tLJSPayDB.query();
        if (tLJSPayDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLJSPayDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "VerDuePayFeeQueryBL";
            tError.functionName = "queryData";
            tError.errorMessage = "应收总表查询失败!";
            this.mErrors.addOneError(tError);
            mLJSPaySet.clear();
            return false;
        }
        if (mLJSPaySet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VerDuePayFeeQueryBL";
            tError.functionName = "queryData";
            tError.errorMessage = "未找到相关数据!";
            this.mErrors.addOneError(tError);
            mLJSPaySet.clear();
            return false;
        }
//        FDate tFDate = new FDate();

        //如果应收总表中的交费日期（失效日期）小于系统日期:报错
//    if (tFDate.getDate(mLJSPaySet.get(1).getPayDate()).compareTo(tFDate.getDate(CurrentDate))<0)
//    {
//      // @@错误处理
//      CError tError = new CError();
//      tError.moduleName = "VerDuePayFeeQueryBL";
//      tError.functionName = "queryData";
//      tError.errorMessage = "已经过了失效日期!";
//      this.mErrors.addOneError(tError);
//      mLJSPaySet.clear();
//      return false;
//    }
        mResult.clear();
        mResult.add(mLJSPaySet);
        return true;
    }

    private boolean queryLJVerDuePayFeeDetail()
    {
        // 应收总表
        LJSPayDB tLJSPayDB = new LJSPayDB();
//    tLJSPayDB.setSchema(mLJSPaySchema);

//        System.out.println("mLJSPaySchema.getGetNoticeNo():----------------"
//                + mLJSPaySchema.getGetNoticeNo());

        tLJSPayDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
        mLJSPaySet = tLJSPayDB.query();

//        System.out.println("+mLJSPaySet.get(1).getOtherNo():---------------"
//                + mLJSPaySet.get(1).getOtherNo());

        if (tLJSPayDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLJSPayDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "VerDuePayFeeQueryBL";
            tError.functionName = "queryData";
            tError.errorMessage = "应收总表查询失败!";
            this.mErrors.addOneError(tError);
            mLJSPaySet.clear();
            return false;
        }
        if (mLJSPaySet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VerDuePayFeeQueryBL";
            tError.functionName = "queryData";
            tError.errorMessage = "未找到相关数据!";
            this.mErrors.addOneError(tError);
            mLJSPaySet.clear();
            return false;
        }
//        FDate tFDate = new FDate();

        //如果应收总表中的交费日期（失效日期）小于系统日期:报错
//    if (tFDate.getDate(mLJSPaySet.get(1).getPayDate()).compareTo(tFDate.getDate(CurrentDate))<0)
//    {
//      // @@错误处理
//      CError tError = new CError();
//      tError.moduleName = "VerDuePayFeeQueryBL";
//      tError.functionName = "queryData";
//      tError.errorMessage = "已经过了失效日期!";
//      this.mErrors.addOneError(tError);
//      mLJSPaySet.clear();
//      return false;
//    }
        StringBuffer tSBql = new StringBuffer(128);
        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = new SSRS();
        if (mLJSPaySet.get(1).getOtherNoType().equals("1"))
        {
            //集体保单续期收费
//            LJSPayGrpDB tLJSPayGrpDB = new LJSPayGrpDB();
            tSBql.append(
                    "select riskcode,sum(SumDuePayMoney),GrpContNo from LJSPayGrp where GetNoticeNo='");
            tSBql.append(mLJSPaySet.get(1).getGetNoticeNo());
            tSBql.append("' group by riskcode,GrpContNo");
            tSSRS = tExeSQL.execSQL(tSBql.toString());
        }
        else if (mLJSPaySet.get(1).getOtherNoType().equals("2"))
        {
            //个人保单续期收费
            tSBql.append(
                    "select riskcode,sum(SumDuePayMoney),ContNo from LJSPayPerson where GetNoticeNo='");
            tSBql.append(mLJSPaySet.get(1).getGetNoticeNo());
            tSBql.append("' group by riskcode,contno");
            tSSRS = tExeSQL.execSQL(tSBql.toString());
            //续保相同险不同责任收费修改 张君 2006-5-31
            int tMaxCount =0;
            int mMaxCount = 0;
            SSRS mSSRS = new SSRS();
            StringBuffer mSBql = new StringBuffer(128);
            mSBql.append(
                    "select riskcode,sum(SumDuePayMoney),ContNo from LJSPayPerson where GetNoticeNo='");
            mSBql.append(mLJSPaySet.get(1).getGetNoticeNo());
            mSBql.append("' group by riskcode,contno");
            mSSRS = tExeSQL.execSQL(mSBql.toString());
            tMaxCount = tSSRS.MaxNumber;
            mMaxCount = mSSRS.MaxNumber;
            if (mMaxCount<tMaxCount)
            {
                tSSRS.Clear();
                tSSRS = mSSRS;
            }

        }
        else
        {
            CError tError = new CError();
            tError.moduleName = "VerDuePayFeeQueryBL";
            tError.functionName = "queryData";
            tError.errorMessage = "非集体或个人续期收费!";
            this.mErrors.addOneError(tError);
            mLJSPaySet.clear();
            return false;
        }

        mResult.clear();
        mResult.add(mLJSPaySet);
        mResult.add(tSSRS);
        return true;
    }

    /**
     * 查询应收总表信息-批量
     * 输出：如果发生错误则返回false,否则返回true
     * @return boolean
     */
    public boolean queryMultLJVerDuePayFee()
    {
        LJSPayDB tLJSPayDB = new LJSPayDB();
        String StartDate = mLJSPaySchema.getPayDate();
        String EndDate = mLJSPaySchema.getMakeDate();

        StringBuffer tSBql = new StringBuffer(128);
        tSBql.append("select * from LJSPay where 1=1 ");
        if (StartDate != null)
        {
            tSBql.append(" and PayDate>='");
            tSBql.append(StartDate);
            tSBql.append("'");
        }
        if (EndDate != null)
        {
            tSBql.append(" and PayDate<='");
            tSBql.append(EndDate);
            tSBql.append("'");
        }

//        System.out.println("query ljspay:" + sqlStr);
        mLJSPaySet = tLJSPayDB.executeQuery(tSBql.toString());
        if (tLJSPayDB.mErrors.needDealError())
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLJSPayDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "VerDuePayFeeQueryBL";
            tError.functionName = "queryMultLJVerDuePayFee";
            tError.errorMessage = "应收总表查询失败!";
            this.mErrors.addOneError(tError);
            mLJSPaySet.clear();
            return false;
        }
        if (mLJSPaySet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "VerDuePayFeeQueryBL";
            tError.functionName = "queryMultLJVerDuePayFee";
            tError.errorMessage = "未找到相关数据!";
            this.mErrors.addOneError(tError);
            mLJSPaySet.clear();
            return false;
        }
        mResult.clear();
        mResult.add(mLJSPaySet);
        return true;
    }

//    /**
//     * 准备往后层输出所需要的数据
//     * 输出：如果准备数据时发生错误则返回false,否则返回true
//     * @return boolean
//     */
//    private boolean prepareOutputData()
//    {
//        mResult.clear();
//        try
//        {
//            mResult.add(mLJSPaySet);
//        }
//        catch (Exception ex)
//        {
//            // @@错误处理
//            CError tError = new CError();
//            tError.moduleName = "VerDuePayFeeQueryBL";
//            tError.functionName = "prepareOutputData";
//            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
//            this.mErrors.addOneError(tError);
//            return false;
//        }
//        return true;
//    }
}
