package com.sinosoft.lis.operfee;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.util.*;
import com.sinosoft.lis.bq.*;

//程序名称：ExpirBenefitConfirmBL.java
//程序功能：个险给付确认
//创建日期：2010-05-07 16:06
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

public class ExpirCanCelBL 
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData saveData = new VData();
    private MMap mMMap = new MMap();
    private GlobalInput tGI = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    /** 数据操作字符串 */

    private String mContNo = null;  //通知书号
    private String mGetNoticeNo = null;  //通知书号

    private ExeSQL mExeSQL = new ExeSQL();

    //个人保单表
    private LCContSchema mLCContSchema = new LCContSchema();

    //应付个人交费表
    private	LJSGetSchema mLJSGetSchema = new LJSGetSchema();
    
    public ExpirCanCelBL() {
    }

    public static void main(String[] args) {

    }
    
    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData)
    {
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0); 
        if(tGI == null || mTransferData == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        mGetNoticeNo = (String)mTransferData.getValueByName("getnoticeno");  //通知书号
        
        if(mGetNoticeNo == null || mGetNoticeNo.equals(""))
        {
        	CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入通知书号-mGetNoticeNo失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LJSGetDB tLJSGetDB = new LJSGetDB();
        tLJSGetDB.setGetNoticeNo(mGetNoticeNo);
        if(!tLJSGetDB.getInfo())
        {
        	CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "通过mGetNoticeNo获取LJSGet失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLJSGetSchema.setSchema(tLJSGetDB.getSchema());
        
        mContNo = (String)mTransferData.getValueByName("contno");
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mContNo);
        if(!tLCContDB.getInfo())
        {
        	CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "通过tContNo获取LCCont失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
        
        mLCContSchema.setSchema(tLCContDB.getSchema());
        return true;
    }

    /**
     * 校验是否有理赔，保全
     * @return boolean：通过true，否则false
     */
    public boolean checkData() 
    {
    	System.out.println("撤销满期抽挡校验已在JS页面校验通过");
    	String checkSQL = "select 1 from ljsget where getnoticeno='"+mGetNoticeNo+"' and exists ( " +
    			" select 1 from ljsgetdraw a,lcget b where getnoticeno=ljsget.getnoticeno and  " +
    			" a.contno=b.contno and a.insuredno=b.insuredno and a.getdutykind=b.getdutykind " +
    			" and a.CurGetToDate!=b.gettodate) with ur " ;
    	
    	String cancelFlag = mExeSQL.getOneValue(checkSQL);
    	
    	if(cancelFlag!=null&&!"".equals(cancelFlag)){
    		CError tError = new CError();
            tError.moduleName = "ExpirBenefitConfirmBL";
            tError.functionName = "dealData";
            tError.errorMessage = "该满期抽挡不是最新的抽挡记录，不能撤销处理";
            this.mErrors.addOneError(tError);
            return false;
    	}
    	
        return true;
    }
    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public VData getSubmitData(VData cInputData, String cOperate) 
    {
        if (!getInputData(cInputData)) 
        {
            return null;
        }
        System.out.println("After getinputdata");
        if (!checkData()) 
        {
            return null;
        }
        //进行业务处理
        if (!dealData()) 
        {
            return null;
        }

        return saveData;
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public MMap getSubmitMMap(VData cInputData, String cOperate) 
    {
        getSubmitData(cInputData, cOperate);
        return mMMap;
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public boolean submitData(VData cInputData, String cOperate) 
    {
        VData data = getSubmitData(cInputData, cOperate);
        if (data == null) 
        {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(data, "") == false) 
        {
            this.mErrors.addOneError("PubSubmit:撤销给付失败!");
            return false;
        }
        return true;
    }

    /**
     * 生成给付记录
     * @return boolean：成功true，否则false
     */
    private boolean dealData()
    {
        
        //存储给付对象
        //修改lcget
    	LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
    	tLJSGetDrawDB.setContNo(mContNo);
    	tLJSGetDrawDB.setGetNoticeNo(mGetNoticeNo);
    	LJSGetDrawSet tLJSGetDrawSet = new LJSGetDrawSet();
    	tLJSGetDrawSet = tLJSGetDrawDB.query();
    	for(int i =1;i<=tLJSGetDrawSet.size();i++){
    		
    		String getSQL= "update lcget set gettodate='"+tLJSGetDrawSet.get(i).getLastGettoDate()+"' " +
    		" ,modifydate=current date,modifytime=current time where contno='"+mContNo+"' and insuredno='"+tLJSGetDrawSet.get(i).getInsuredNo()+"' " +
    		"  and getdutykind='"+tLJSGetDrawSet.get(i).getGetDutyKind()+"' ";
    		mMMap.put(getSQL, SysConst.UPDATE);
    	}
        
        //删除ljsget
        mMMap.put(mLJSGetSchema, SysConst.DELETE);
        
        mMMap.put(tLJSGetDrawSet, SysConst.DELETE);
        
        saveData.clear();
        saveData.add(mMMap);
        return true;
    }
    
}
