 package com.sinosoft.lis.operfee;

import java.util.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 查询保单的各种方法</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class QueryLCPolTypeBL
{

  public  CErrors mErrors = new CErrors();
  private VData mResult = new VData();

  private LCPolSchema  mLCPolSchema = new LCPolSchema() ;
  private LCPolSet  mLCPolSet = new LCPolSet() ;

  public QueryLCPolTypeBL()
  {  }

  public static void main(String[] args)
  { }

  public LCPolSet getResult()
  {
      return mLCPolSet;
  }

  /**
   * 根据印刷号查询个人保单表
   * 附属查询条件：
   * @param tLCPolSchema
   * @return
   */
  public boolean queryA(LCPolSchema  tLCPolSchema)
  {
    if(tLCPolSchema==null)
    {
       getError("queryA");
       return false;
    }
    if(tLCPolSchema.getPrtNo()==null)
    {
       getError("queryA","传入的印刷号是空值！");
       return false;
    }
    String strSQL="select * from LCPol where PrtNo='"+tLCPolSchema.getPrtNo()+"'";
    strSQL=strSQL+" and (StopFlag='0' or StopFlag is null) and GrpPolNo='00000000000000000000'" ;
    if(tLCPolSchema.getManageCom()!=null)
    {
      String MaxManageCom=PubFun.RCh(tLCPolSchema.getManageCom(),"9",8);
      String MinManageCom=PubFun.RCh(tLCPolSchema.getManageCom(),"0",8);;
      strSQL=strSQL+" and ManageCom>='"+MinManageCom+"' and ManageCom<='"+MaxManageCom+"'";
    }
    if(tLCPolSchema.getAppFlag()!=null)
    {
        strSQL=strSQL+" and AppFlag='"+tLCPolSchema.getAppFlag()+"'" ;
    }

    LCPolDB tLCPolDB = new LCPolDB();
    tLCPolDB.setSchema(mLCPolSchema);
    mLCPolSet = tLCPolDB.executeQuery(strSQL);
    if (tLCPolDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCPolDB.mErrors);
      getError("queryA","数据库查询失败!");
      mLCPolSet.clear();
      return false;
    }
    if (mLCPolSet.size() == 0)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCPolDB.mErrors);
      getError("queryA","未找到相关数据!");
      mLCPolSet.clear();
      return false;
    }

      return true;
  }

  /**
   * 根据保单号查询个人保单表
   * @param tLCPolSchema
   * @return
   */
  public boolean queryB(LCPolSchema  tLCPolSchema)
  {
    if(tLCPolSchema==null)
    {
       getError("queryB");
       return false;
    }
    if(tLCPolSchema.getPolNo()==null)
    {
       getError("queryB","传入的保单号是空值！");
       return false;
    }
    String strSQL="select * from LCPol where PolNo='"+tLCPolSchema.getPolNo()+"'";
    strSQL=strSQL+" and (StopFlag='0' or StopFlag is null) and GrpPolNo='00000000000000000000'" ;
    if(tLCPolSchema.getManageCom()!=null)
    {
      String MaxManageCom=PubFun.RCh(tLCPolSchema.getManageCom(),"9",8);
      String MinManageCom=PubFun.RCh(tLCPolSchema.getManageCom(),"0",8);;
      strSQL=strSQL+" and ManageCom>='"+MinManageCom+"' and ManageCom<='"+MaxManageCom+"'";
    }
    if(tLCPolSchema.getAppFlag()!=null)
    {
        strSQL=strSQL+" and AppFlag='"+tLCPolSchema.getAppFlag()+"'" ;
    }
    if(tLCPolSchema.getPayLocation()!=null)
    {
      strSQL=strSQL+"  and PayLocation='"+tLCPolSchema.getPayLocation()+"'  ";
    }
    LCPolDB tLCPolDB = new LCPolDB();
    tLCPolDB.setSchema(mLCPolSchema);
    mLCPolSet = tLCPolDB.executeQuery(strSQL);
    if (tLCPolDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCPolDB.mErrors);
      getError("queryB","数据库查询失败!");
      mLCPolSet.clear();
      return false;
    }
    if (mLCPolSet.size() == 0)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCPolDB.mErrors);
      getError("queryB","未找到相关数据!");
      mLCPolSet.clear();
      return false;
    }

      return true;
  }

  /**
   * 根据时间段查询个人保单表（交费位置=8 --银行转帐(事后转账)）
   * @param tLCPolSchema
   * @return
   */
  public boolean queryC(LCPolSchema  tLCPolSchema)
  {
    if(tLCPolSchema==null)
    {
       getError("queryC");
       return false;
    }
    if(tLCPolSchema.getGetStartDate()==null||tLCPolSchema.getPayEndDate()==null)
    {
       getError("queryC","传入的起始日期是空值！");
       return false;
    }
    String strSQL="select * from LCPol ";
    strSQL=strSQL+" where ((MakeDate>='"+tLCPolSchema.getGetStartDate()+"' and MakeDate<='"+tLCPolSchema.getPayEndDate()+"')) ";
    strSQL=strSQL+" and (StopFlag='0' or StopFlag is null) and GrpPolNo='00000000000000000000'" ;
    if(tLCPolSchema.getManageCom()!=null)
    {
      String MaxManageCom=PubFun.RCh(tLCPolSchema.getManageCom(),"9",8);
      String MinManageCom=PubFun.RCh(tLCPolSchema.getManageCom(),"0",8);;
      strSQL=strSQL+" and ManageCom>='"+MinManageCom+"' and ManageCom<='"+MaxManageCom+"'";
    }
    if(tLCPolSchema.getAppFlag()!=null)
    {
        strSQL=strSQL+" and AppFlag='"+tLCPolSchema.getAppFlag()+"'" ;
    }
    if(tLCPolSchema.getPayLocation()!=null)
    {
      strSQL=strSQL+"  and PayLocation='"+tLCPolSchema.getPayLocation()+"'  ";
    }
    if(tLCPolSchema.getPayMode()!=null)
    {
       strSQL=strSQL+"  and PayMode='"+tLCPolSchema.getPayMode()+"'  ";
    }
    if(tLCPolSchema.getSaleChnl()!=null)
    {
       strSQL=strSQL+"  and SaleChnl='"+tLCPolSchema.getSaleChnl()+"'  ";
    }

    strSQL=strSQL+"  and (UWFlag is null or UWFlag not in ('a', '0', '1')) ";


    System.out.println("Sql"+strSQL);

    LCPolDB tLCPolDB = new LCPolDB();
    tLCPolDB.setSchema(mLCPolSchema);
    mLCPolSet = tLCPolDB.executeQuery(strSQL);
    if (tLCPolDB.mErrors.needDealError() == true)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCPolDB.mErrors);
      getError("queryC","数据库查询失败!");
      mLCPolSet.clear();
      return false;
    }
    if (mLCPolSet.size() == 0)
    {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCPolDB.mErrors);
      getError("queryC","未找到相关数据!");
      mLCPolSet.clear();
      return false;
    }

      return true;
  }

  /**
   * 填充错误信息
   * @param funName
   * @param errMsg
   */
  public void getError(String funName,String errMsg)
  {
    CError tError = new CError();
    tError.moduleName = "QueryLCPolTypeBL";
    tError.functionName = funName;
    tError.errorMessage = errMsg;
    mErrors .addOneError(tError) ;
  }

  /**
   * 填充错误信息(自动赋错误信息：传入参数不能为空)
   * @param funName
   */
  public void getError(String funName)
  {
    CError tError = new CError();
    tError.moduleName = "QueryLCPolTypeBL";
    tError.functionName = funName;
    tError.errorMessage = "传入参数不能为空！";
    mErrors .addOneError(tError) ;
  }
}
