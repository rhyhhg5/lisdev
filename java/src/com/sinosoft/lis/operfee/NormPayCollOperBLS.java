package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.JdbcUrl;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import java.sql.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class NormPayCollOperBLS  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 数据操作字符串 */
  private String mOperate;

  public NormPayCollOperBLS() {
  }
  public static void main(String[] args) {
    NormPayCollOperBLS mNormPayCollOperBLS1 = new NormPayCollOperBLS();
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    boolean tReturn =false;
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;
    System.out.println("Start NormPayCollOper BLS Submit...");
    //信息保存
    if(this.mOperate.equals("VERIFY"))
    {tReturn=verify(cInputData);}

    if (tReturn)
      System.out.println("Verify sucessful");
    else
      System.out.println("Verify failed") ;

      System.out.println("End NormPayCollOper BLS Submit...");

    return tReturn;
  }

//核销事务操作
  private boolean verify(VData mInputData)
  {
    boolean tReturn =true;
    System.out.println("Start Verify...");
    Connection conn = DBConnPool.getConnection();
    if (conn==null)
    {
    		// @@错误处理
    		CError tError = new CError();
		tError.moduleName = "NormPayCollOperBLS";
                tError.functionName = "verifyData";
	        tError.errorMessage = "数据库连接失败!";
		this.mErrors .addOneError(tError) ;
 		return false;
    }
    try{
      conn.setAutoCommit(false);

// 暂交费分类表更新
      System.out.println("Start 暂交费分类表...");
      LJTempFeeClassDBSet tLJTempFeeClassDBSet=new LJTempFeeClassDBSet(conn);
      tLJTempFeeClassDBSet.set((LJTempFeeClassSet)mInputData.getObjectByObjectName("LJTempFeeClassSet",0));
      if (!tLJTempFeeClassDBSet.update())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJTempFeeClassDBSet.mErrors);
    		CError tError = new CError();
	        tError.moduleName = "NormPayCollOperBLS";
	        tError.functionName = "updateData";
	        tError.errorMessage = "暂交费表数据保存失败!";
	        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 	        return false;
      }
      System.out.println("暂交费分类表更新");

// 暂交费表更新
      System.out.println("Start 暂交费表...");
      LJTempFeeDBSet tLJTempFeeDBSet=new LJTempFeeDBSet(conn);
      tLJTempFeeDBSet.set((LJTempFeeSet)mInputData.getObjectByObjectName("LJTempFeeSet",0));
      if (!tLJTempFeeDBSet.update())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJTempFeeDBSet.mErrors);
    		CError tError = new CError();
		tError.moduleName = "NormPayCollOperBLS";
		tError.functionName = "updateData";
		tError.errorMessage = "暂交费表数据保存失败!";
		this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 		return false;
      }
System.out.println("暂交费表更新");


//实收总表添加
System.out.println("Start 实收总表...");
      LJAPayDB tLJAPayDB=new LJAPayDB(conn);
      tLJAPayDB.setSchema((LJAPayBL)mInputData.getObjectByObjectName("LJAPayBL",0));
      if (!tLJAPayDB.insert())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJAPayDB.mErrors);
    		CError tError = new CError();
		tError.moduleName = "NormPayCollOperBLS";
		tError.functionName = "saveData";
		tError.errorMessage = "实收总表数据保存失败!";
		this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 		return false;
      }
System.out.println("实收总表添加");



//实收个人交费表添加
System.out.println("Start 实收个人交费表...");
      LJAPayPersonDBSet tLJAPayPersonDBSet=new LJAPayPersonDBSet(conn);
      tLJAPayPersonDBSet.set((LJAPayPersonSet)mInputData.getObjectByObjectName("LJAPayPersonSet",0));
      if (!tLJAPayPersonDBSet.insert())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJAPayPersonDBSet.mErrors);
    		CError tError = new CError();
	        tError.moduleName = "NormPayCollOperBLS";
	        tError.functionName = "saveData";
	        tError.errorMessage = "实收个人交费表数据保存失败!";
	        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 	        return false;
      }
System.out.println("实收个人交费表添加");

//实收集体表添加
System.out.println("Start 实收集体表...");
      LJAPayGrpDBSet tLJAPayGrpDBSet=new LJAPayGrpDBSet(conn);
      tLJAPayGrpDBSet.set((LJAPayGrpSet)mInputData.getObjectByObjectName("LJAPayGrpSet",0));
      if (!tLJAPayGrpDBSet.insert())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJAPayGrpDBSet.mErrors);
    		CError tError = new CError();
		tError.moduleName = "NormPayCollOperBLS";
		tError.functionName = "saveData";
		tError.errorMessage = "实收集体表数据保存失败!";
		this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 		return false;
      }
System.out.println("实收集体表添加");
//集体保单表更新
     System.out.println("Start 集体保单表...");
     LCGrpContDBSet tLCGrpContDBSet=new LCGrpContDBSet(conn);
     tLCGrpContDBSet.set((LCGrpContSet)mInputData.getObjectByObjectName("LCGrpContSet",0));
     if (!tLCGrpContDBSet.update())
     {
                   // @@错误处理
               this.mErrors.copyAllErrors(tLCGrpContDBSet.mErrors);
                   CError tError = new CError();
               tError.moduleName = "NormPayCollOperBLS";
               tError.functionName = "updateData";
               tError.errorMessage = "集体保单表数据保存失败!";
               this.mErrors .addOneError(tError) ;
       conn.rollback() ;
       conn.close();
                return false;
     }
System.out.println("集体险种保单表更新");


//集体险种保单表更新
      System.out.println("Start 集体险种保单表...");
      LCGrpPolDBSet tLCGrpPolDBSet=new LCGrpPolDBSet(conn);
      tLCGrpPolDBSet.set((LCGrpPolSet)mInputData.getObjectByObjectName("LCGrpPolSet",0));
      if (!tLCGrpPolDBSet.update())
      {
                    // @@错误处理
                this.mErrors.copyAllErrors(tLCGrpPolDBSet.mErrors);
                    CError tError = new CError();
                tError.moduleName = "NormPayCollOperBLS";
                tError.functionName = "updateData";
                tError.errorMessage = "集体险种保单表数据保存失败!";
                this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
                 return false;
      }
System.out.println("集体险种保单表更新");


//个人险种保单表更新
      System.out.println("Start 个人险种保单表...");
      LCPolDBSet tLCPolDBSet=new LCPolDBSet(conn);
      tLCPolDBSet.set((LCPolSet)mInputData.getObjectByObjectName("LCPolSet",0));
      if (!tLCPolDBSet.update())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLCPolDBSet.mErrors);
    		CError tError = new CError();
		tError.moduleName = "NormPayCollOperBLS";
		tError.functionName = "updateData";
		tError.errorMessage = "个人险种保单表数据保存失败!";
		this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 		return false;
      }
System.out.println("个人险种保单表更新");

//LCDuty 保险责任表更新
System.out.println("Start 保险责任表...");
      LCDutyDBSet tLCDutyDBSet=new LCDutyDBSet(conn);
      tLCDutyDBSet.set((LCDutySet)mInputData.getObjectByObjectName("LCDutySet",0));
      if (!tLCDutyDBSet.update())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLCDutyDBSet.mErrors);
    		CError tError = new CError();
	        tError.moduleName = "IndiNormalPayVerifyBLS";
	        tError.functionName = "saveData";
	        tError.errorMessage = "保险责任表数据保存失败!";
	        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 	        return false;
      }
System.out.println("保险责任表添加");

//保费项表更新
System.out.println("Start 保费项表...");
      LCPremDBSet tLCPremDBSet=new LCPremDBSet(conn);
      tLCPremDBSet.set((LCPremSet)mInputData.getObjectByObjectName("LCPremSet",0));
      if (!tLCPremDBSet.update())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLCPremDBSet.mErrors);
    		CError tError = new CError();
	        tError.moduleName = "NormPayCollOperBLS";
	        tError.functionName = "saveData";
	        tError.errorMessage = "保费项表数据更新失败!";
	        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 	        return false;
      }
System.out.println("保费项表更新");

//删除应收个人交费表
      System.out.println("Start 删除个人交费表...");
      LJSPayPersonDBSet tLJSPayPersonDBSet=new LJSPayPersonDBSet(conn);
      tLJSPayPersonDBSet.set((LJSPayPersonSet)mInputData.getObjectByObjectName("LJSPayPersonSet",0));
      if (!tLJSPayPersonDBSet.delete())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJSPayPersonDBSet.mErrors);
    		CError tError = new CError();
	        tError.moduleName = "NormPayCollOperBLS";
	        tError.functionName = "deleteData";
	        tError.errorMessage = "应收个人交费表数据删除失败!";
	        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 	        return false;
      }

System.out.println("删除应收个人交费表");

//删除应收集体交费表
      System.out.println("Start 删除集体交费表...");
      LJSPayGrpDBSet tLJSPayGrpDBSet=new LJSPayGrpDBSet(conn);
      tLJSPayGrpDBSet.set((LJSPayGrpSet)mInputData.getObjectByObjectName("LJSPayGrpSet",0));
      if (!tLJSPayGrpDBSet.delete())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJSPayGrpDBSet.mErrors);
    		CError tError = new CError();
	        tError.moduleName = "NormPayCollOperBLS";
	        tError.functionName = "deleteData";
	        tError.errorMessage = "应收集体交费表数据删除失败!";
	        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 	        return false;
      }

System.out.println("删除集体交费表");

//删除应收总表
      System.out.println("Start 删除应收总表...");
      LJSPayDB tLJSPayDB=new LJSPayDB(conn);
      tLJSPayDB.setSchema((LJSPayBL)mInputData.getObjectByObjectName("LJSPayBL",0));
      if (!tLJSPayDB.delete())
      {
    		// @@错误处理
		this.mErrors.copyAllErrors(tLJSPayDB.mErrors);
    		CError tError = new CError();
	        tError.moduleName = "NormPayCollOperBLS";
	        tError.functionName = "deleteData";
	        tError.errorMessage = "应收总表数据删除失败!";
	        this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
 	        return false;
      }
System.out.println("删除应收总表");


//删除保险帐户表
      System.out.println("Start 保险帐户表...");
      LCInsureAccDBSet tLCInsureAccDBSet=new LCInsureAccDBSet(conn);
      tLCInsureAccDBSet.set((LCInsureAccSet)mInputData.getObjectByObjectName("LCInsureAccSet",0));
      if (!tLCInsureAccDBSet.delete())
      {
                    // @@错误处理
                this.mErrors.copyAllErrors(tLCInsureAccDBSet.mErrors);
                    CError tError = new CError();
                tError.moduleName = "IndiFinUrgeVerifyBLS";
                tError.functionName = "deleteData";
                tError.errorMessage = "保险帐户表数据删除失败!";
                this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
                 return false;
      }
System.out.println("删除保险帐户表");

//插入保险帐户表
      System.out.println("Start 保险帐户表...");
      LCInsureAccDBSet pLCInsureAccDBSet=new LCInsureAccDBSet(conn);
      pLCInsureAccDBSet.set((LCInsureAccSet)mInputData.getObjectByObjectName("LCInsureAccSet",0));
      if (!pLCInsureAccDBSet.insert())
      {
                    // @@错误处理
                this.mErrors.copyAllErrors(pLCInsureAccDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "IndiFinUrgeVerifyBLS";
                tError.functionName = "deleteData";
                tError.errorMessage = "插入保险帐户表失败!";
                this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
                 return false;
      }
System.out.println("插入保险帐户表");

//插入保险帐户表记价履历表
      System.out.println("Start 保险帐户表记价履历表...");
      LCInsureAccTraceDBSet pLCInsureAccTraceDBSet=new LCInsureAccTraceDBSet(conn);
      pLCInsureAccTraceDBSet.set((LCInsureAccTraceSet)mInputData.getObjectByObjectName("LCInsureAccTraceSet",0));
      if (!pLCInsureAccTraceDBSet.insert())
      {
                    // @@错误处理
                this.mErrors.copyAllErrors(pLCInsureAccTraceDBSet.mErrors);
                CError tError = new CError();
                tError.moduleName = "IndiFinUrgeVerifyBLS";
                tError.functionName = "deleteData";
                tError.errorMessage = "插入保险帐户表记价履历表失败!";
                this.mErrors .addOneError(tError) ;
        conn.rollback() ;
        conn.close();
                 return false;
      }
System.out.println("插入保险帐户表记价履历表");


      conn.commit() ;
      conn.close();
    }
    catch (Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="NormPayCollOperBLS";
      tError.functionName="submitData";
      tError.errorMessage=ex.toString();
      this.mErrors .addOneError(tError);
      try{conn.rollback() ;} catch(Exception e){}
      tReturn=false;
    }
    return tReturn;
  }
}
