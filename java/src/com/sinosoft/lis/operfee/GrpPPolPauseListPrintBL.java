package com.sinosoft.lis.operfee;

import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.vschema.LDCodeSet;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GrpPPolPauseListPrintBL {

    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = new TransferData();
    private XmlExport xmlexport = null;
    private VData mResult = null;
    private SSRS mSSRS;
    private String mStartDate = ""; //起始日
    private String mEndDate = ""; //截止日
    private String mOrderSql = ""; //催收状态


    public GrpPPolPauseListPrintBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //取得外部传入数据
        if (!getInputData(cInputData)) {
            return false;
        }

        //校验数据合法性
        if (!getListData()) {
            return false;
        }

        //获取打印所需数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        try {

            mGlobalInput = (GlobalInput) cInputData.
                           getObjectByObjectName("GlobalInput", 0);
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
        } catch (Exception e) {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("传入的数据不完整，" + e.toString());
            return false;
        }
        mStartDate = (String) mTransferData.getValueByName("StartDate");
        mEndDate = (String) mTransferData.getValueByName("EndDate");
        mOrderSql = (String) mTransferData.getValueByName("OrderSql");
        return true;
    }

    /**
     * 查询列表显示数据
     * @param schema LCContSchema
     * @return String
     */

    private boolean getListData() {
        StringBuffer sqlSB = new StringBuffer();
        String sqlCons = "";
        if (!(mStartDate == null || mStartDate.equals(""))) {
            sqlCons = " and a.MakeDate >='" + mStartDate + "'";
        }
        if (!(mEndDate == null || mEndDate.equals(""))) {
            sqlCons = sqlCons + " and a.MakeDate <='" + mEndDate + "'";
        }
        sqlSB.append("select a.StandbyFlag2,b.GrpName,a.OtherNo,min(c.CurPayToDate),a.MakeDate,a.MakeDate,a.ReqOperator,")
                .append(" (SELECT NAME  FROM LAAGENT WHERE AgentCode=a.AgentCode),ShowManageName(a.ManageCom)")
                .append(" from LOPRTManager a,LCGrpCont b,LJSPayGrpB c where 1=1")
                .append(" and a.code = '42' and a.StateFlag = '0'")
                .append(" and a.OtherNoType = '01'   and a.otherNo = c.GrpContNo")
                .append(" and a.OtherNo = b.GrpContNo and b.appflag='1'")
                .append( sqlCons)
                .append(" group by a.StandbyFlag2,b.GrpName,a.OtherNo,a.MakeDate,a.MakeDate,a.ReqOperator,a.AgentCode,a.ManageCom")
                .append( mOrderSql)

               ;
        String tSQL = sqlSB.toString();
        System.out.println(tSQL);
        ExeSQL tExeSQL = new ExeSQL();

        mSSRS = tExeSQL.execSQL(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "creatFile";
            tError.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }

    /**
     * 获取打印所需数据
     * @param schema LCContSchema
     * @return String
     */
    private boolean getPrintData() {

        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("GrpPPolPauseList.vts", "printer"); //最好紧接着就初始化xml文档

        String[] title = {"失效通知书号", "投保人", "保单号", "应收日期",
                         "失效日期", "生成时间", "经办人", "业务员", "业务部门"};
        xmlexport.addListTable(getListTable(), title);
       // xmlexport.outputDocumentToFile("C:\\", "GrpPPolPauseList");
        mResult = new VData();
        mResult.addElement(xmlexport);

        return true;
    }

    /**
     *  获取列表数据
     * @param schema LCContSchema
     * @return String
     */
    private ListTable getListTable() {
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[9];
            for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
                info[j - 1] = mSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }

        tListTable.setName("ZT");
        return tListTable;
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }


    public static void main(String[] args) {
        GrpPPolPauseListPrintBL p = new GrpPPolPauseListPrintBL();
        LPEdorAppSchema schema = new LPEdorAppSchema();
        schema.setEdorAcceptNo("20050915000022");
        VData v = new VData();
        v.add(schema);
        if (!p.submitData(v, "")) {
            System.out.println(p.mErrors.getErrContent());
        } else {
            System.out.println("OK");
        }
    }

    private void jbInit() throws Exception {
    }



}
