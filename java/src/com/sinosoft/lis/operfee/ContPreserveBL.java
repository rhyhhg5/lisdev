package com.sinosoft.lis.operfee;

import com.sinosoft.lis.bl.LPContBL;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.bq.DetailDataQuery;
import com.sinosoft.lis.bq.UpdateEdorState;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.lis.db.LCInsuredDB;
import com.sinosoft.lis.db.LDCode1DB;
import com.sinosoft.lis.db.LMRiskAppDB;
import com.sinosoft.lis.db.LPEdorItemDB;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContRemainSchema;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LCInsuredSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LPEdorItemSchema;
import com.sinosoft.lis.schema.LPInsuredSchema;
import com.sinosoft.lis.schema.LPPolSchema;
import com.sinosoft.lis.vschema.LCContRemainSet;
import com.sinosoft.lis.vschema.LCContSet;
import com.sinosoft.lis.vschema.LCInsuredSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LPEdorItemSet;
import com.sinosoft.lis.vschema.LPPolSet;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class ContPreserveBL {
	  /** 传入数据的容器 */
	  private VData mInputData = new VData();
	  /** 传出数据的容器 */
	  private VData mResult = new VData();
	  /** 数据操作字符串 */
	  private String mOperate;
	  /** 错误处理类 */
	  public CErrors mErrors = new CErrors();
	  /** 全局基础数据 */
	  private GlobalInput mGlobalInput = null;
	  private LCContRemainSet mLCContRemainSet = new LCContRemainSet();
	  private MMap map = new MMap();
	  private Reflections ref = new Reflections();

	  private String currDate = PubFun.getCurrentDate();
	  private String currTime = PubFun.getCurrentTime();


	  public ContPreserveBL() {
	  }

	  /**
	   * 数据提交的公共方法，提交成功后将返回结果保存入内部VData对象中
	   * @param cInputData 传入的数据,VData对象
	   * @param cOperate 数据操作字符串，主要包括"INSERT"
	   * @return 布尔值（true--提交成功, false--提交失败）
	   */
	  public boolean submitData(VData cInputData, String cOperate) {

	    //将操作数据拷贝到本类中
	    this.mInputData = (VData) cInputData.clone();
	    this.mOperate = cOperate;

	    //得到外部传入的数据,将数据备份到本类中
	    if (!getInputData()) {
	      return false;
	    }

	    if (!checkData()) {
	      return false;
	    }

	    //进行业务处理
	    if (!dealData()) {
	      return false;
	    }

	    //准备往后台的数据
	    if (!prepareData()) {
	      return false;
	    }
	    PubSubmit tSubmit = new PubSubmit();

	    if (!tSubmit.submitData(mResult, "")) { //数据提交
	      // @@错误处理
	      this.mErrors.copyAllErrors(tSubmit.mErrors);
	      CError tError = new CError();
	      tError.moduleName = "PEdorCTDetailBL";
	      tError.functionName = "submitData";
	      tError.errorMessage = "数据提交失败!";
	      this.mErrors.addOneError(tError);
	      return false;
	    }
	    System.out.println("PEdorCTDetailBL End PubSubmit");
	    return true;
	  }

	  /**
	   * 将外部传入的数据分解到本类的属性中
	   * @param: 无
	   * @return: boolean
	   */
	  private boolean getInputData() {
	    try {
	      mGlobalInput = (GlobalInput) mInputData
	          .getObjectByObjectName("GlobalInput", 0);

	      mLCContRemainSet = (LCContRemainSet) mInputData.
	          getObjectByObjectName("LCContRemainSet", 0);
	    }
	    catch (Exception e) {
	      // @@错误处理
	      e.printStackTrace();
	      CError tError = new CError();
	      tError.moduleName = "PEdorCTDetailBL";
	      tError.functionName = "getInputData";
	      tError.errorMessage = "接收数据失败!!";
	      this.mErrors.addOneError(tError);
	      return false;
	    }

	    if (mGlobalInput == null 
	        || mLCContRemainSet == null || mLCContRemainSet.size() == 0) {
	      CError tError = new CError();
	      tError.moduleName = "PEdorCTDetailBL";
	      tError.functionName = "getInputData";
	      tError.errorMessage = "输入数据有误!";
	      this.mErrors.addOneError(tError);
	      return false;
	    }
	    return true;
	  }


	  /**
	   * 对留存的保单进行标识
	   * @return
	   */
	  private boolean dealData() {
	
		  for(int i=1;i<=mLCContRemainSet.size();i++){
			  LCContRemainSchema tLCContRemainSchema = new LCContRemainSchema();
			  tLCContRemainSchema = mLCContRemainSet.get(i).getSchema();
			  if(!setLCContRemain(tLCContRemainSchema)){
				  return false;
			  }
		  }

	    return true;
	  }

	  /**
	   * 根据前面的输入数据，进行校验处理
	   * @return 如果在处理过程中出错，则返回false,否则返回true
	   */
	  private boolean checkData()
	  {
	      return true;
	  }

	  private boolean setLCContRemain(LCContRemainSchema tLCContRemainSchema){
		  String tContNo = tLCContRemainSchema.getContNo();
//		  String aApplyYears = getApplyYears(tContNo);
		  String aApplyYears = "2";
		  if(!checkRemain(tContNo,aApplyYears)){
			  return false;
		  }
		       	//处理并发操作 同一个团单只能操作一次
	      	MMap tCekMap = null;
	      	tCekMap = lockLGWORK(tContNo);
	          if (tCekMap == null)
	          {
	              return false;
	          }
          
	  	    PubSubmit tSubmit = new PubSubmit();

	  	    VData tVData = new VData();
	  	    tVData.add(tCekMap);
		    if (!tSubmit.submitData(tVData, "")) { //数据提交
		      // @@错误处理
		      this.mErrors.copyAllErrors(tSubmit.mErrors);
		      CError tError = new CError();
		      tError.moduleName = "ContPreserveBL";
		      tError.functionName = "submitData";
		      tError.errorMessage = "数据提交失败!";
		      this.mErrors.addOneError(tError);
		      return false;
		    }
		  
		  
		  tLCContRemainSchema.setRiskCode(getRisk(tContNo));
		  tLCContRemainSchema.setApplyYears(aApplyYears);
		  tLCContRemainSchema.setApplyDate(currDate);
		  tLCContRemainSchema.setRemainState("1");
		  tLCContRemainSchema.setSerialNo(PubFun1.CreateMaxNo("SERIALNO",""));
		  tLCContRemainSchema.setOperator(mGlobalInput.Operator);
		  tLCContRemainSchema.setMakeDate(currDate);
		  tLCContRemainSchema.setMakeTime(currTime);
		  tLCContRemainSchema.setModifyDate(currDate);
		  tLCContRemainSchema.setModifyTime(currTime);
		  
		  map.put(tLCContRemainSchema, SysConst.DELETE_AND_INSERT);
		  return true;
	  }
	  
	  private String getRisk(String tContNo){
		  String tRiskSQL = "SELECT RISKCODE FROM LCPOL WHERE CONTNO='"+tContNo+"' and riskcode in ( " +
		  		" select code from  ldcode where codetype='remainrisk' )";
		  
		  String riskcode = new ExeSQL().getOneValue(tRiskSQL);

		  return riskcode;
	  }
	  
	   /**
	     * 锁定动作
	     * @param cLCContSchema
	     * @return
	     */
	    private MMap lockLGWORK(String tContNo)
	    {
	        MMap tMMap = null;
	        /**满期给付锁定标志"MJ"*/
	        String tLockNoType = "MJ";
	        /**锁定时间*/
	        String tAIS = "3600";
	        TransferData tTransferData = new TransferData();
	        tTransferData.setNameAndValue("LockNoKey", tContNo);
	        tTransferData.setNameAndValue("LockNoType", tLockNoType);
	        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

	        VData tVData = new VData();
	        tVData.add(mGlobalInput);
	        tVData.add(tTransferData);

	        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
	        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
	        if (tMMap == null)
	        {
	            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
	            return null;
	        }
	        return tMMap;
	    }  
	  
	  /**
	   * 校验保单是否能够留存
	   * @param tContNo
	   * @param aApplyYears
	   * @return
	   */
	  private boolean checkRemain(String tContNo,String aApplyYears){
		  LCContDB tLCContDB = new LCContDB();
		  tLCContDB.setContNo(tContNo);
		  LCContSet tLCContSet = tLCContDB.query();
		  
		  if(tLCContSet==null||tLCContSet.size()<=0){
			  System.out.println("保单信息不存在，无法操作保单留存");
	        	// @@错误处理
				System.out.println("ContPreserveBL+checkRemain++--");
				CError tError = new CError();
				tError.moduleName = "ContPreserveBL";
				tError.functionName = "checkRemain";
				tError.errorMessage = "保单信息不存在，无法操作保单留存";
				mErrors.addOneError(tError);
				return false;
		  }
		  
		  String remainFlag = new ExeSQL().getOneValue(" select 1 from LCContRemain where contno='"+tContNo+"' and ApplyYears='"+aApplyYears+"' ");
		 
		  if(remainFlag!=null&&"1".equals(remainFlag)){
			  System.out.println("该保单年度已经留存的保单无法再次留存");
	        	// @@错误处理
				System.out.println("ContPreserveBL+checkRemain++--");
				CError tError = new CError();
				tError.moduleName = "ContPreserveBL";
				tError.functionName = "checkRemain";
				tError.errorMessage = "该保单年度已经留存的保单无法再次留存";
				mErrors.addOneError(tError);
				return false;
		  }
		  
		  return true;
		  
	  }

	  /**
	   * 得到保单年度
	   * @param tContNo
	   * @return
	   */
	  private String getApplyYears(String tContNo){
		  int aApplyYears = 0;
		  int maxYear = 0;
		  String cvalidate = new ExeSQL().getOneValue("SELECT CVALIDATE FROM LCCONT WHERE CONTNO='"+tContNo+"'");
		  
		  maxYear = PubFun.calInterval(cvalidate,currDate, "Y");
		  
		  for(int i=1;i<=maxYear;i++){
			  String upPoldate = PubFun.calDate(cvalidate, i, "Y", "");
			  if(!(CommonBL.stringToDate(upPoldate).before(CommonBL.stringToDate(currDate)))){
				  aApplyYears = i+1;
				  break;
			  }
		  }
		  
		  return aApplyYears+"";
	  }


	  /**
	   * 准备往后层输出所需要的数据
	   * @return 如果准备数据时发生错误则返回false,否则返回true
	   */
	  private boolean prepareData() {
	    mResult.clear();
	    mResult.add(map);

	    return true;
	  }

	  /**
	   * 数据输出方法，供外界获取数据处理结果
	   * @return 包含有数据查询结果字符串的VData对象
	   */
	  public VData getResult() {
	    return mResult;
	  }
	  public static void main(String[] args) {
		System.out.println(PubFun.calDate("2014-01-01", 1, "Y", ""));
	}

}
