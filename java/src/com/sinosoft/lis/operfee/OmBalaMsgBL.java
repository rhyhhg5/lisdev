package com.sinosoft.lis.operfee;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.llcase.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.rmi.*;
import javax.xml.rpc.*;
import com.sinosoft.lis.message.*;

import java.util.*;

//程序名称：OmBalaMsgBL.java
//程序功能：万能月结客户通知短信
//创建日期：2009-5-22
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
public class OmBalaMsgBL 
{
	private String mContNo = null;
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    private ExeSQL tExeSQL = new ExeSQL();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();

    public OmBalaMsgBL() {}

    public boolean sendMessages() 
    {
        System.out.println("万能月结客户通知短信批处理开始......");
        SmsServiceSoapBindingStub binding = null;
        try 
        {
            binding = (SmsServiceSoapBindingStub)new SmsServiceServiceLocator().getSmsService(); //创建binding对象
        } 
        catch(javax.xml.rpc.ServiceException jre) 
        {
            jre.printStackTrace();
        }

        binding.setTimeout(60000);
        Response value = null; //声明Response对象，该对象将在提交短信后包含提交的结果。
        SmsMessages msgs = new SmsMessages(); //创建SmsMessages对象，该对象对应于上文下行短信格式中的Messages元素
        msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
        msgs.setExtension("024"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
        msgs.setServiceType("xuqi"); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
        msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
        msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
        msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
        msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素

        Vector vec = new Vector();
        vec = getMessages();

        if (!vec.isEmpty()) 
        {
            msgs.setMessages((SmsMessage[]) vec.toArray(new SmsMessage[vec.size()])); //设置该批短信的每一条短信，一批短信可以包含多条短信
            try 
            {
                value = binding.sendSMS("xuqi", "xuqi", msgs); //提交该批短信，UserName是短信服务平台管理员分配的用户名， Password则是其对应的密码，用户名和密码用于验证发送者，只有验证通过才可能提交短信，msgs即为刚才创建的短信对象。
                System.out.println(value.getStatus());
                System.out.println(value.getMessage());
            } 
            catch (RemoteException ex) 
            {
                ex.printStackTrace();
            }
        } 
        else 
        {
            System.out.print("万能月结客户通知无符合条件的短信！");
        }
        return true;
    }

    private Vector getMessages() 
    {
        Vector tVector = new Vector();
        StringBuffer sql = new StringBuffer(128);
        sql.append("select (select x.mobile from lcaddress x,lcappnt y where y.contno = a.contno and x.customerno = y.appntno and x.addressno = y.addressno fetch first 1 row only), ")
           .append("a.appntname,a.appntsex, a.contno, year(DueBalaDate - 1 days), month(DueBalaDate - 1 days), ")
           .append("nvl((select sum(Money) from LCInsureAccTrace where OtherType = '6' and MoneyType = 'LX' and OtherNo = b.SequenceNo),0), ")
           .append("cast(round(b.InsuAccBalaAfter,2) as  decimal(20,2)),a.managecom ")
           .append("from lccont a,LCInsureAccBalance b ")
           .append("where a.contno = b.contno ")
           .append("and b.rundate = current date - 1 day and b.BalaCount > 0 ")
           .append(mContNo != null ? (" and a.ContNo = '" + mContNo + "' ") : "")
           .append("with ur");
        System.out.println(sql.toString());
        SSRS tSSRS =tExeSQL.execSQL(sql.toString());
        System.out.println("拢共："+tSSRS.MaxRow+"条");
        for (int i = 1; i <= tSSRS.MaxRow; i++) 
        {
        	String tMobile = tSSRS.GetText(i, 1);
        	String tName = tSSRS.GetText(i, 2);
        	String tSex = tSSRS.GetText(i, 3);
        	String tContNo = tSSRS.GetText(i, 4);
        	String tYear = tSSRS.GetText(i, 5);
        	String tMonth = tSSRS.GetText(i, 6);
        	String tLX = tSSRS.GetText(i, 7);
        	String tBalaAfter = tSSRS.GetText(i, 8);
        	String tManageCom = tSSRS.GetText(i, 9);
        	
            if (tMobile == null || "".equals(tMobile) ||"null".equals(tMobile)) 
            {
                continue;
            }
            if (tName == null || "".equals(tName) ||"null".equals(tName)) 
            {
                continue;
            }
            if (tContNo == null || "".equals(tContNo) ||"null".equals(tContNo)) 
            {
                continue;
            }
            if (tSex == null || "".equals(tSex) ||"null".equals(tSex)) 
            {
                continue;
            }
            if (tYear == null || "".equals(tYear) ||"null".equals(tYear)) 
            {
                continue;
            }
            if (tMonth == null || "".equals(tMonth) ||"null".equals(tMonth)) 
            {
                continue;
            }
            if (tLX == null || "".equals(tLX) ||"null".equals(tLX)) 
            {
                continue;
            }
            if (tBalaAfter == null || "".equals(tBalaAfter) ||"null".equals(tBalaAfter)) 
            {
                continue;
            }
            if (tManageCom == null || "".equals(tManageCom) ||"null".equals(tManageCom)) 
            {
                continue;
            }
            
            //发送给客户的短信内容            
            SmsMessage tSmsMessage = new SmsMessage(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
            StringBuffer content = new StringBuffer(128);
//          **先生/女士：已将您的*****保单（万能险）***年***月利息***元计入保单账户，账户当前价值*****元，尽请留意。人保健康敬致，服务热线95591。
            content.append(tName).append("0".equals(tSex)?"先生":"女士").append("：已将您的").append(tContNo)
                   .append("保单（万能险）").append(tYear).append("年").append(tMonth).append("月利息").append(tLX)
                   .append("元计入保单账户，账户当前价值").append(tBalaAfter).append("元，尽请留意。人保健康敬致，服务热线95591。");
            System.out.println(content.toString());
           
            tSmsMessage.setReceiver(tMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
            tSmsMessage.setContents(content.toString()); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
            tSmsMessage.setOrgCode(tManageCom); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素

            //添加SmsMessage对象
            tVector.add(tSmsMessage);
        }
        return tVector;
    }
    
    /**
     * 给指定保单发消息
     * @param cContNo String
     */
    public void sendOne(String cContNo)
    {
    	mContNo = cContNo;
        this.sendMessages();
    }

    public static void main(String[] args) 
    {
        OmBalaMsgBL tCpayRnewMsgBL = new OmBalaMsgBL();
        tCpayRnewMsgBL.sendOne("000008857000002");
    }
}
