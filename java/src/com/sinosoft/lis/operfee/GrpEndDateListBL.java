package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.ListTable;
import com.sinosoft.utility.StrTool;
import java.io.File;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 打印满期终止清单
 * 清单打印业务处理类，接收GrpEndDateListUI.java传入的数据，
 * 生成并返回打印数据：执行GrpEndDateListUI.java传入的Sql，
 * 得到需要打印的数据，按批单格式生成XmlExport数据。
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class GrpEndDateListBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();
    private XmlExport xmlexport = new XmlExport();
    private String mSql = null;

    public GrpEndDateListBL()
    {
    }

    /**
     * 操作的提交方法，得到打印清单数据。
     * @param sql String
     * @return XmlExport
     */
    public XmlExport getXmlExport(String sql)
    {
        mSql = sql;

        if(!dealData())
        {
            return null;
        }

        return xmlexport;
    }

    /**
     * 处理业务逻辑，生成打印数据XmlExport。
     * @return boolean:操作成功true，否则false
     */
    private boolean dealData()
    {
        if(mSql == null || mSql.equals(""))
        {
            mErrors.addOneError("请传入mSql");
            return false;
        }

        System.out.println(mSql);
        SSRS tSSRS = new ExeSQL().execSQL(mSql);
        if(tSSRS.getMaxRow() == 0)
        {
            mErrors.addOneError("没有查询到需要的清单数据");
            return false;
        }

        //得到保单号标识符“grpContNo”(见GrpEndDateDeal.js)的位置，打印数据从position+1开始
        int position = 1;
        for(; position <= tSSRS.getMaxCol(); position++)
        {
            if(tSSRS.GetText(1, position) != null
               && tSSRS.GetText(1, position).equals("grpContNo"))
            {
                break;
            }
        }

        ListTable tListTable = new ListTable();
        for (int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String[] info = new String[tSSRS.getMaxCol() + 1];
            info[0] = "" + i;
            for(int t = 1; t <= tSSRS.getMaxCol() - position; t++)
            {
                info[t] = StrTool.cTrim(tSSRS.GetText(i, t + position));
            }

            tListTable.add(info);
        }

        tListTable.setName("List");

        xmlexport.createDocument("PrtGrpEndDateList.vts", "printer");
        xmlexport.addListTable(tListTable, new String[tSSRS.getMaxRow()]);


        return true;
    }

    public static void main(String[] args)
    {
        String sql = "select grpContNo, 'grpContNo', manageCom, (select Name from LAAgent where agentCode = a.agentCode), grpName, (select codeName from LDCode where codeType = 'businesstype' and code = a.businessType), grpContNo, varchar(prem), polApplyDate, (select max(endDate) from LCPol where grpContNo = a.grpContNo), (select max(payToDate) from LCPol where grpContNo = a.grpcontNo), varchar((select sum(sumActuPayMoney) from LJAPayGrp where grpContNo = a.grpContNo)), varchar(abs((select nvl((select sum(getMoney) from LJAGetEndorse where grpContNo = a.grpContNo), 0) + nvl((select sum(a.getMoney) from LJSGetEndorse a, LPEdorApp b where a.endorsementno=b.edorAcceptNo and b.edorState = '0' and a.grpContNo = '0000006201'), 0) from dual))), varchar(abs(nvl((select sum(pay) from LJAGetClaim where confDate is not null and contNo in(select contno from LCCont where grpContNo = a.grpContNo)) , 0))), peoples2 from LCGrpCont a where 1 = 1 and a.manageCom like '86%%' and (select max(endDate) from LCPol where grpContNo = a.grpContNo ) >= '2006-06-07' and (select max(endDate) from LCPol where grpContNo = a.grpContNo ) <= '2006-09-07' order by grpContNo ";

        GrpEndDateListBL grpenddatelistbl = new GrpEndDateListBL();
        XmlExport x = grpenddatelistbl.getXmlExport(sql);
    }
}
