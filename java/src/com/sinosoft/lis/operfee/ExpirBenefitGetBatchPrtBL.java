package com.sinosoft.lis.operfee;

import com.sinosoft.lis.vschema.LJSGetSet;
import com.sinosoft.lis.schema.LDSysVarSchema;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.f1print.ExpirBenefitPrintBL;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.f1print.PrintService;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.utility.StrTool;
import com.sinosoft.lis.db.LJSGetDB;
import java.io.File;
import com.sinosoft.lis.schema.LDCodeSchema;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.utility.XmlExport;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class ExpirBenefitGetBatchPrtBL {

    private String mOperate;
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private GlobalInput mG = new GlobalInput();

    private LDSysVarSchema mLDSysVarSchema = new LDSysVarSchema();
    private LJSGetSet mLJSGetSet = new LJSGetSet();

    private boolean operFlag = true;
    private String strLogs = "";
    private String Content = "";
    private String mxmlFileName = "";
    private int mCount = 0;
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap mMap = new MMap();
    private PubSubmit tPubSubmit = new PubSubmit();

    public ExpirBenefitGetBatchPrtBL() {
    }

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        cInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        if (!getInputData(cInputData)) {
            return false;
        }
        if (mOperate.equals("PRINT")) {
            if (!dealData()) {
                return false;
            }
        }
        if (!prepareOutputData()) {
            return false;
        }
        tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            if (tPubSubmit.mErrors.needDealError()) {
                buildError("submitData", "保存再保计算结果出错!");
            }
            return false;
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    private boolean getInputData(VData cInputData) {
        mLJSGetSet = (LJSGetSet) cInputData.getObjectByObjectName("LJSGetSet",
                0);
        if (mLJSGetSet == null) { //（服务事件关联表）
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitGetBatchPrtBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的数据为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        mLDSysVarSchema.setSchema((LDSysVarSchema) cInputData.
                                  getObjectByObjectName("LDSysVarSchema", 0));
        return true;
    }


    public int getCount() {
        return mCount;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData() {
        if (mOperate.equals("PRINT")) {
            VData tVData;

            XmlExport txmlExportAll = new XmlExport();
            txmlExportAll.createDocument("ON");
            mCount = mLJSGetSet.size();
            String mXmlFileName[] = new String[mCount];
            LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet(); //新增，用来更新打印状态，次数等信息。by huxl @ 20071024
            for (int i = 1; i <= mCount; i++) {
                LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
                LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
                tLOPRTManagerDB.setCode("MX001");
                System.out.println("*************" +
                                   mLJSGetSet.get(i).getGetNoticeNo());
                tLOPRTManagerDB.setStandbyFlag2(mLJSGetSet.get(i).
                                                getGetNoticeNo());
                tLOPRTManagerSchema = tLOPRTManagerDB.query().get(1);
                if (tLOPRTManagerSchema == null) {
                    System.out.println(
                            "*******LOPRTManagerSchema is null******");
                    LJSGetDB tLJSGetDB = new LJSGetDB();
                    tLJSGetDB.setGetNoticeNo(mLJSGetSet.get(i).getGetNoticeNo());
                    System.out.println(
                            "*******mLJSGetSet.get(i).getGetNoticeNo()******" +
                            mLJSGetSet.get(i).getGetNoticeNo());
                    if (!tLJSGetDB.getInfo()) {
                        // @@错误处理
                        CError tError = new CError();
                        tError.moduleName = "dealData";
                        tError.functionName = "getInputData";
                        tError.errorMessage = "传入的数据为空!";
                        this.mErrors.addOneError(tError);
                        continue;

                    }

                    VData xVData = new VData();
                    xVData.addElement(mG);
                    xVData.addElement(tLJSGetDB);

                    ExpirBenefitGetPrintBL tExpirBenefitGetPrintBL = new
                            ExpirBenefitGetPrintBL();
                    if (!tExpirBenefitGetPrintBL.submitData(xVData, "INSERT")) {
                        operFlag = false;
                        Content = tExpirBenefitGetPrintBL.mErrors.getErrContent();
                    }
                    tLOPRTManagerDB.setStandbyFlag2(mLJSGetSet.get(i).
                            getGetNoticeNo());
                    tLOPRTManagerSchema = tLOPRTManagerDB.query().get(1);
                }
                //下面封装要更新的打印管理信息
                tLOPRTManagerSchema.setStateFlag("1");
                tLOPRTManagerSchema.setPrintTimes(tLOPRTManagerSchema.
                                                  getPrintTimes() + 1);
                tLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
                tLOPRTManagerSchema.setDoneTime(PubFun.getCurrentTime());
                tLOPRTManagerSet.add(tLOPRTManagerSchema);

                mxmlFileName = StrTool.unicodeToGBK("0" +
                        tLOPRTManagerSchema.getCode()) + "-" +
                               tLOPRTManagerSchema.getStandbyFlag2();
                mXmlFileName[i - 1] = mxmlFileName;

                if (!callPrintService(tLOPRTManagerSchema)) {
                    Content = " 印刷号" + tLOPRTManagerSchema.getPrtSeq() + "：" +
                              this.mErrors.getFirstError().toString();
                    strLogs = strLogs + Content;
                    continue;
                }
                XmlExport txmlExport = (XmlExport) mResult.
                                       getObjectByObjectName("XmlExport", 0);
                if (txmlExport == null) {
                    Content = "印刷号" + tLOPRTManagerSchema.getPrtSeq() +
                              "没有得到要显示的数据文件！";
                    strLogs = strLogs + Content;
                    continue;
                }
                if (operFlag == true) {
                    File f = new File(mLDSysVarSchema.getSysVarValue());
                    f.mkdir();
                    System.out.println("PATH : " +
                                       mLDSysVarSchema.getSysVarValue());
                    System.out.println("PATH2 : " +
                                       mLDSysVarSchema.getSysVarValue().
                                       substring(0,
                                                 mLDSysVarSchema.getSysVarValue().
                                                 length() - 1));
                    txmlExport.outputDocumentToFile(mLDSysVarSchema.
                            getSysVarValue().substring(0,
                            mLDSysVarSchema.getSysVarValue().length() - 1) +
                            File.separator + "printdata" + File.separator +
                            "data" + File.separator + "brief" + File.separator,
                            mxmlFileName);
                }
            }
            this.mMap.put(tLOPRTManagerSet, "UPDATE");
            tVData = new VData();
            tVData.add(mXmlFileName);
            mResult = tVData;
        }
        return true;
    }

    public void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "ExpirBenefitGetBatchPrtBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean callPrintService(LOPRTManagerSchema aLOPRTManagerSchema) {

        // 查找打印服务
        String strSQL = "SELECT * FROM LDCode WHERE CodeType = 'print_service'";
        strSQL += " AND Code = '" + aLOPRTManagerSchema.getCode() + "'";
        strSQL += " AND OtherSign = '0'";
        System.out.println(strSQL);
        LDCodeSet tLDCodeSet = new LDCodeDB().executeQuery(strSQL);

        if (tLDCodeSet.size() == 0) {
            buildError("dealData",
                       "找不到对应的打印服务类(Code = '" + aLOPRTManagerSchema.getCode() +
                       "')");
            return false;
        }

        // 调用打印服务
        LDCodeSchema tLDCodeSchema = tLDCodeSet.get(1);

        try {
            Class cls = Class.forName(tLDCodeSchema.getCodeAlias());
            PrintService ps = (PrintService) cls.newInstance();

            // 准备数据
            String strOperate = tLDCodeSchema.getCodeName();

            VData vData = new VData();

            vData.add(mG);
            vData.add(aLOPRTManagerSchema);

            if (!ps.submitData(vData, strOperate)) {
                mErrors.copyAllErrors(ps.getErrors());
                return false;
            }

            mResult = ps.getResult();

        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("callPrintService", ex.toString());
            return false;
        }

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "FirstPayBatchPrtBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

}
