package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.xb.PRnewAppCancelBL;
import com.sinosoft.lis.xb.XBConst;
import com.sinosoft.lis.pubfun.*;

import java.util.*;
import java.lang.String;

import com.sinosoft.lis.bq.AppAcc;
import com.sinosoft.lis.taskservice.NextFeeHastenTask;
import com.sinosoft.task.TaskPhoneFinishBL;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class GRnewLJSCancelBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private GlobalInput tGI = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private TransferData mTransferData = new TransferData();
    //银行发盘
    private LYSendToBankSet mLYSendToBankSet = new LYSendToBankSet();
    //暂交费表
    private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
    //实收表
    private LJAPaySet mLJAPaySet = new LJAPaySet();

    private LJAGetSchema mLJAGetSchema = new LJAGetSchema(); //实付总表
    private LJAGetSet mLJAGetSet = new LJAGetSet();
    private LJAGetOtherSchema mLJAGetOtherShema = new LJAGetOtherSchema(); //其它其他退费实付表
    private LJAGetOtherSet mLJAGetOtherSet = new LJAGetOtherSet(); //其它其他退费实付表
    //应收总表
    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();
    //应收总表集
    private LJSPayGrpSet mLJSPayGrpSet = new LJSPayGrpSet();
    //应收个人交费表
    private LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet();

    //应收个人交费表备份表
    private LJSPayPersonBSet mLJSPayPersonBSet = new LJSPayPersonBSet();
    private LJSPayPersonBSet tLJSPayPersonBSet = new LJSPayPersonBSet();
    private LJSPayPersonBSchema mLJSPayPersonBSchema = new LJSPayPersonBSchema();
    //应收团体交费表备份表
    private LJSPayGrpBSet mLJSPayGrpBSet = new LJSPayGrpBSet();
    private LJSPayGrpBSet tLJSPayGrpBSet = new LJSPayGrpBSet();
    private LJSPayGrpBSchema mLJSPayGrpBSchema = new LJSPayGrpBSchema();
    //应收总表备份表
    private LJSPayBSchema mLJSPayBSchema = new LJSPayBSchema();
    private LJSPayBSet mLJSPayBSet = new LJSPayBSet();
    private LJSPayBSet tLJSPayBSet = new LJSPayBSet();

    //集体保单表
    private LCGrpContSet mLCGrpContSet = new LCGrpContSet();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private String mPersonSql = "";
    private String mPersonBSql = "";
    private MMap mAppMap = new MMap();   //存储投保人帐户信息

    //打印管理表
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private LOPRTManagerSubSet mLOPRTManagerSubSet = new
            LOPRTManagerSubSet();



    private MMap map = new MMap();
    public GRnewLJSCancelBL() {
    }

    public static void main(String[] args) {
        GrpLJSCancelBL GrpDueFeeBL1 = new GrpLJSCancelBL();

        GlobalInput tGt = new GlobalInput();
        tGt.ComCode = "86";
        tGt.Operator = "001";
        tGt.ManageCom = "86";

        LJSPaySchema tLJSPaySchema =new LJSPaySchema();
        tLJSPaySchema.setGetNoticeNo("31000003486");

        TransferData tTransferData= new TransferData();
        tTransferData.setNameAndValue("CancelMode","1");

        VData tVData = new VData();
        tVData.add(tLJSPaySchema);
        tVData.add(tGt);
        tVData.add(tTransferData);
        GrpDueFeeBL1.submitData(tVData, "INSERT");
    }

    /**
     * 杨红于2005-07-19添加数据校验，判断页面中查询出的信息是否已被催收
     * @return boolean
     */
    public boolean checkData() {
        ExeSQL tExeSQL = new ExeSQL();
        String count;
        tExeSQL = new ExeSQL();
        // count = null;
        count = tExeSQL.getOneValue(
                "SELECT COUNT(*) FROM LJSPay WHERE BankOnTheWayFlag='1'  AND GetNoticeNo='" +
                mLJSPaySchema.getGetNoticeNo() + "'");

        if (tExeSQL.mErrors.needDealError()) {
            this.mErrors.addOneError("应收记录:" + mLJSPaySchema.getGetNoticeNo() +
                                     "查询银行在途标志时错误:" +
                                     tExeSQL.mErrors.getFirstError());

            return false;
        }

        if ((count != null) && !count.equals("0")) {
            //this.mErrors.addOneError("保单号:" + mLCContSchema.getContNo() +
            //   "催收错误:正在做保全!");
            CError.buildErr(this,
                            "应收记录:" + mLJSPaySchema.getGetNoticeNo() +
                            "作废错误：银行在途，不能作废");
            return false;
        }
        
        
      //查询发盘数据
        String SqlStr = "select a.* from LYSendToBank a ,LJSPay b where a.PolNo= b.OtherNo and b.tempfeetype=2"
                        + " and b.OtherNoType='1'  and b.OtherNo='" +
                        mLJSPaySchema.getOtherNo() + "'";
        LYSendToBankDB tLYSendToBankDB = new LYSendToBankDB();
        mLYSendToBankSet = tLYSendToBankDB.executeQuery(SqlStr);
        if (mLYSendToBankSet.size() != 0) {
            CError.buildErr(this,
                            "应收记录:" + mLJSPaySchema.getGetNoticeNo() +
                            "作废错误:已经发盘");
            return false;
        }
        return true;
    }


    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {
        this.mOperate = cOperate;

        if (!getInputData(cInputData)) {
            return false;
        }
        if (!checkData()) {
            return false;
        }
        //进行业务处理
        if (!dealData()) {
            return false;
        }
        if(!dealPhoneHasten())
        {
            return false;
        }
        if (!prepareData()) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(mInputData, "") == false) {
            System.out.println("delete fail!");
            return false;
        }

        return true;
    }

    /**
     * 个单应收记录作废/撤销类，在IndiLJSCancelBL.getSubmitMap方法 中增加对催缴工单的处理
     * @return boolean
     */
    private boolean dealPhoneHasten()
    {
        String sql = "select * "
                     + "from LGPhoneHasten "
                     + "where getNoticeNo = '"
                     + mLJSPaySchema.getGetNoticeNo() + "' "
                     + "   and HastenType = '"
                     + NextFeeHastenTask.HASTEN_TYPE_NEXTFEEG + "' "
                     + "   and FinishType is null ";
        LGPhoneHastenSet set = new LGPhoneHastenDB().executeQuery(sql);
        if(set.size() == 0)
        {
            //若无催缴就查不出来也
            return true;
        }

        LGPhoneHastenSchema schema = set.get(1);
        schema.setFinishType("2");
        schema.setRemark("应收记录" + mLJSPaySchema.getGetNoticeNo() + "已作废");

        VData data = new VData();
        data.add(schema);
        data.add(tGI);

        TaskPhoneFinishBL bl = new TaskPhoneFinishBL();
        MMap tMMap = bl.getSubmitMap(data, "");
        if(tMMap == null)
        {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }
        map.add(tMMap);

        return true;
    }

    /**
     * Yangh于2005-07-19创建新的dealData的处理逻辑
     * @return boolean
     */
    private boolean dealData() {
        String cancelMode = (String) mTransferData.getValueByName("CancelMode");

        if(!dealGRnew()){
            return false;
        }
        
        //保单
        String SqlGrpCont = "select a.* from LCGrpCont a where a.GrpContNo='" +
                            mLJSPaySchema.getOtherNo() + "'";
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        mLCGrpContSet = tLCGrpContDB.executeQuery(SqlGrpCont);
        if (mLCGrpContSet.size() == 0) {
            CError.buildErr(this,
                            "应收记录:" + mLJSPaySchema.getGetNoticeNo() +
                            "作废错误:保单不存在");
            return false;
        }
        mLCGrpContSchema = mLCGrpContSet.get(1);

        //下面查询出符合条件的LJSPayGrp
        String ljsgrpSqlStr = "select * from LJSPayGrp where GetNoticeNo='" +
                              mLJSPaySchema.getGetNoticeNo() + "'";
        LJSPayGrpDB tLJSPayGrpDB = new LJSPayGrpDB();
        mLJSPayGrpSet = tLJSPayGrpDB.executeQuery(ljsgrpSqlStr);
        if (mLJSPayGrpSet.size() == 0) {
            CError.buildErr(this,
                            "应收记录:" + mLJSPaySchema.getGetNoticeNo() +
                            "作废错误:应收集体缴费表缺少数据");

            return false;
        }

        //下面查询出符合条件的LJSPayPerson
        String ljsPersonSql =
                " SELECT COUNT(*) FROM LJSPayPerson where GetNoticeNo='" +
                mLJSPaySchema.getGetNoticeNo() + "'";
        ExeSQL tExeSQL = new ExeSQL();
        String strPerbCont = tExeSQL.getOneValue(ljsPersonSql);
        if (tExeSQL.mErrors.needDealError()) {
            CError.buildErr(this, "查询数据 LJSPayPerson 出错！");
            return false;
        }

        if (strPerbCont == null || strPerbCont == "0") {
            CError.buildErr(this,
                            "应收记录:" + mLJSPaySchema.getGetNoticeNo() +
                            "作废错误:应收个人交费表缺少数据");

            return false;

        }

        //下面查询出符合条件的LJSPayB
        String ljsSqlB = "select * from LJSPayB where GetNoticeNo='" +
                         mLJSPaySchema.getGetNoticeNo() + "'";
        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        tLJSPayBSet = tLJSPayBDB.executeQuery(ljsSqlB);
        if (tLJSPayBSet.size() == 0) {
            CError.buildErr(this,
                            "应收记录:" + mLJSPaySchema.getGetNoticeNo() +
                            "作废错误:应收总表备份表缺少数据");

            return false;
        }

        //下面查询出符合条件的LJSPayGrpB
        String ljsgrpSqlB = "select * from LJSPayGrpB where GetNoticeNo='" +
                            mLJSPaySchema.getGetNoticeNo() + "'";
        LJSPayGrpBDB tLJSPayGrpBDB = new LJSPayGrpBDB();
        tLJSPayGrpBSet = tLJSPayGrpBDB.executeQuery(ljsgrpSqlB);
        if (tLJSPayGrpBSet.size() == 0) {
            CError.buildErr(this,
                            "应收记录:" + mLJSPaySchema.getGetNoticeNo() +
                            "作废错误:应收集体缴费表备份表缺少数据");

            return false;
        }


        //未到帐处理
        String SqlFee = "select a.* from LJTempFee a where a.tempfeeno='" +
                        mLJSPaySchema.getGetNoticeNo() + "'";
        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        mLJTempFeeSet = tLJTempFeeDB.executeQuery(SqlFee);
        double moneyToAppAcc = 0;
        for(int i = 1; i <= mLJTempFeeSet.size(); i++)
        {
            mLJTempFeeSet.get(i).setConfFlag("2");  //财务暂收放入帐户
			//判断财务暂收数据是否到账确认
			if(mLJTempFeeSet.get(i).getEnterAccDate()!=null&&!"".equals(mLJTempFeeSet.get(i).getEnterAccDate())&&!"null".equals(mLJTempFeeSet.get(i).getEnterAccDate())){
				 moneyToAppAcc += mLJTempFeeSet.get(i).getPayMoney();
			}else{
				moneyToAppAcc +=0;
			}            
        }
        if(Math.abs(moneyToAppAcc - 0) > 0.000001)
        {
            LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
            tLCAppAccTraceSchema.setCustomerNo(mLJSPaySchema.getAppntNo());
            tLCAppAccTraceSchema.setOtherNo(mLJSPaySchema.getOtherNo());
            tLCAppAccTraceSchema.setOtherType("1");
            tLCAppAccTraceSchema.setMoney(moneyToAppAcc);
            tLCAppAccTraceSchema.setBakNo(mLJSPaySchema.getGetNoticeNo());
            tLCAppAccTraceSchema.setOperator(tGI.Operator);

            AppAcc tAppAcc = new AppAcc();
            MMap tMMap = tAppAcc.accShiftToXSYIJ(tLCAppAccTraceSchema);
            mAppMap.add(tMMap);
        }
        //抵扣的帐户余额放入帐户
        String sql = "  select sum(sumActuPayMoney) from LJSPayGrp "
                     + "where getNoticeNo = '" + mLJSPaySchema.getGetNoticeNo()
                     + "'  and payType = 'YEL' ";
        String money = new ExeSQL().getOneValue(sql);
        if(!money.equals("") && !money.equals("null"))
        {
            String sqlAcc = "update LCAppAcc set accGetMoney = accGetMoney + "
                            + -Double.parseDouble(money) + ", " //与原操作金额正负相反
                            + "  state = '1' "
                            + "where customerNo = '"
                            + mLJSPaySchema.getAppntNo() + "' ";
           mAppMap.put(sqlAcc, "UPDATE");
           sqlAcc = "  delete from LCAppAccTrace "
                     + "where customerNo = '"
                     + mLJSPaySchema.getAppntNo() + "' "
                     + "   and accType = '0' "
                     + "   and otherNo = '" + mLJSPaySchema.getOtherNo() + "' "
                     + "   and otherType = '1' "
                     + "   and state = '0' "
                     + "   and destSource = '02' ";
           mAppMap.put(sqlAcc, "DELETE");
        }


        //调整需求，若已收费，则转入投保人帐户
//        if (mLJTempFeeSet != null && mLJTempFeeSet.size() != 0) {
//            CError.buildErr(this,
//                            "应收记录:" + mLJSPaySchema.getGetNoticeNo() +
//                            "作废错误:已付费，未核销");
//            return false;
//        }

        mPersonSql = " DELETE FROM LJSPayPerson WHERE  grpcontno='"
                     + mLJSPaySchema.getOtherNo() + "'";

         mPersonBSql =
                 " UPDATE  LJSPayPersonB SET DealState='2' ,CancelReason='"
                 + cancelMode
                 + "' WHERE GetNoticeNo='" +
                 mLJSPaySchema.getGetNoticeNo() + "'";

        //作废处理
        for (int i = 1; i <= tLJSPayBSet.size(); i++) {
            mLJSPayBSchema = tLJSPayBSet.get(i);
            mLJSPayBSchema.setDealState("2"); //应收作废
            mLJSPayBSchema.setCancelReason(cancelMode); //应收作废原因
            mLJSPayBSchema.setModifyDate(CurrentDate);
            mLJSPayBSchema.setModifyTime(CurrentTime);
            mLJSPayBSet.add(mLJSPayBSchema);
        }

        for (int j = 1; j <= tLJSPayGrpBSet.size(); j++) {
            mLJSPayGrpBSchema = tLJSPayGrpBSet.get(j);
            mLJSPayGrpBSchema.setDealState("2"); //应收作废
            mLJSPayGrpBSchema.setCancelReason(cancelMode); //应收作废
            mLJSPayGrpBSchema.setModifyDate(CurrentDate);
            mLJSPayGrpBSchema.setModifyTime(CurrentTime);
            mLJSPayGrpBSet.add(mLJSPayGrpBSchema);
        }

       /* LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setOtherNo(mLCGrpContSchema.getGrpContNo());
        tLOPRTManagerDB.setOtherNoType("01");
        tLOPRTManagerDB.setCode(PrintManagerBL.CODE_REPAY);
        tLOPRTManagerDB.setStandbyFlag2(mLJSPaySchema.getGetNoticeNo());

        LOPRTManagerSet tLOPRTManagerSet = tLOPRTManagerDB.query();

        if (tLOPRTManagerSet.size() <= 0) {
            CError.buildErr(this, "没有找到续期打印主表数据，不能撤销！");

            return false;
        }

        mLOPRTManagerSchema = tLOPRTManagerSet.get(1);
        LOPRTManagerSubDB tLOPRTManagerSubDB = new LOPRTManagerSubDB();
        tLOPRTManagerSubDB.setPrtSeq(mLOPRTManagerSchema.getPrtSeq());
        mLOPRTManagerSubSet = tLOPRTManagerSubDB.query();

        if (mLOPRTManagerSubSet.size() <= 0) {
            CError.buildErr(this, "没有找到续期打印子表数据，不能撤销！");
            return false;
        }*/

        return true;
    }

    private boolean dealGRnew() {

        String sql = "  select 1 "
                     + "from LCRnewStateLog "
                     + "where grpcontNo = '" + mLJSPaySchema.getOtherNo() + "' "
                     + "   and state != '" + XBConst.RNEWSTATE_DELIVERED + "' ";
        ExeSQL e = new ExeSQL();
        String rs = e.getOneValue(sql);
        if(rs.equals("") || rs.equals("null"))
        {
            //没有续保数据，不需要续保撤销动作
            return true;
        }

        //先进行续保数据撤销
        GRnewAppCancelBL tGRnewAppCancelBL = new GRnewAppCancelBL();
        LCGrpContSchema schema = new LCGrpContSchema();
        schema.setGrpContNo(mLJSPaySchema.getOtherNo());
        
        TransferData tTransferData = new TransferData();
        String tCancelMode = (String) mTransferData.getValueByName("CancelMode");
        tTransferData.setNameAndValue("CancelMode", tCancelMode);
        
        VData data = new VData();
        data.add(schema);
        data.add(tGI);
        data.add(mLJSPaySchema);
        data.add(tTransferData);

        MMap tMMap = tGRnewAppCancelBL.getSubmitMap(data, "");
        if(map == null && null == tMMap )
        {
            this.mErrors.copyAllErrors(tGRnewAppCancelBL.mErrors);
            return false;
        }
        map.add(tMMap);

        return true;
    
	}

	public boolean prepareData() {
        map.put(mPersonSql, "DELETE");
        map.put(mLJTempFeeSet, "UPDATE");
        map.add(mAppMap);
        map.put(mPersonBSql,"UPDATE");
        map.put(mLJSPayGrpSet, "DELETE");
        map.put(mLJSPaySchema, "DELETE");
//        map.put(mLCGrpContSchema, "UPDATE");
        map.put(mLJSPayBSet, "UPDATE");
        map.put(mLJSPayGrpBSet, "UPDATE");
        map.put(mLOPRTManagerSchema, "DELETE");
        map.put(mLOPRTManagerSubSet, "DELETE");
        mInputData.add(map);
        return true;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) {
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mLJSPaySchema = ((LJSPaySchema) mInputData.getObjectByObjectName(
                "LJSPaySchema", 0));
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);

        if (tGI == null || mLJSPaySchema == null || mTransferData == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpDueFeeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
        if (tLJSPayDB.getInfo() == false) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpDueFeeBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mLJSPaySchema.setSchema(tLJSPayDB.getSchema());
        return true;
    }


}
