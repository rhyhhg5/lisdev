package com.sinosoft.lis.operfee;

import com.sinosoft.lis.schema.LJSGetSchema;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LJSGetDrawSet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.ExeSQL;
import java.util.ArrayList;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.db.LCGetDB;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.db.LMDutyGetAliveDB;
import com.sinosoft.lis.vschema.LMDutyGetAliveSet;
import com.sinosoft.lis.schema.LMDutyGetAliveSchema;
import com.sinosoft.lis.schema.LJSGetDrawSchema;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 * 团体满期给付--团体统一给付
 */
public class GrpExpirBenefitGetBL {

    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData saveData = new VData();
    private MMap mMMap = new MMap();
    private GlobalInput tGI = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    /** 数据操作字符串 */
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String mserNo = "1"; //批次号
    private String mSerGrpNo = ""; //接受外部参数
    private String mGetNoticeNo = null; //通知书号

    private String StartDate = ""; //应付开始时间
    private String EndDate = ""; //应付结束时间
    private String mPayMode ="";

    //应付个人明细表
    private LJSGetDrawSet tLJSGetDrawSet = new LJSGetDrawSet();

    private String mGrpContNo =""; //团体保单号
    private String mGrpPolNo ="" ;//团体险种号
    private String mRiskCode = ""; //险种号
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    public GrpExpirBenefitGetBL() {
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public boolean submitData(VData cInputData, String cOperate) {

        VData data = getSubmitData(cInputData, cOperate);
        if (data == null) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(data, "") == false) {
            this.mErrors.addOneError("PubSubmit:生成给付失败!");
            return false;
        }
        return true;
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public VData getSubmitData(VData cInputData, String cOperate) {

        if (!getInputData(cInputData)) {
            return null;
        }
        System.out.println("After getinputdata");
        if (!checkData()) {
            return null;
        }
        //进行业务处理
        if (!dealData()) {
            return null;
        }
        return saveData;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) {
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mLCGrpContSchema = (LCGrpContSchema)mInputData.getObjectByObjectName("LCGrpContSchema",0);
        mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
        mPayMode =(String) mTransferData.getValueByName("GPayMode");
        if(tGI == null ||  mLCGrpContSchema == null || mLCGrpContSchema.equals("") || mTransferData == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpExpirBenefitGetBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true ;
    }

    /**
     * 数据校验
     * @return boolean
     */

    private boolean checkData()
    {
        if(!checkBQ())
        {
            return false ;
        }

        if(!checkLP())
        {
            return false;
        }
        return true;
    }

    /**
     * 业务处理
     * @return boolean
     */

    private boolean dealData()
    {
        String tManageCom = new ExeSQL().getOneValue("select managecom from lcgrpcont where grpcontno ='"+mLCGrpContSchema.getGrpContNo()+"'");
        String tLimit = PubFun.getNoLimit(tManageCom);
        String serMultNo = (String) mTransferData.getValueByName("serNo");
        mserNo = serMultNo;

        //给付任务编号
        String taskno = " select max(otherno) from ljsget where getnoticeno in (select getnoticeno from "
                        + " ljsgetdraw where Grpcontno ='" + mLCGrpContSchema.getGrpContNo() +
                        "')"
                        ;
        SSRS noSSRS = new ExeSQL().execSQL(taskno);
        int no = 0;
        if (noSSRS.getMaxRow() > 0 &&
            !(noSSRS.GetText(1, 1).equals("") || noSSRS.GetText(1, 1).equals("null"))) {
            String noString = noSSRS.GetText(1, 1);
            no = Integer.parseInt(noString.substring(noString.length() - 1)); //modify by fuxin 2008-5-5 取最后一位。
        }

        //String startDate = (String) mTransferData.getValueByName("StartDate");
        String endDate = (String) mTransferData.getValueByName("EndDate");
           //给付任务号
           no++;
           String noString = String.valueOf(no);
           String taskNo = "P" + mLCGrpContSchema.getGrpContNo();
           for (int k = noString.length(); k < 4; k++) {
               taskNo += "0";
           }
           taskNo += noString;

           //给付通知书号码
           if (!createNo(tManageCom)) {
               return false;
           }

           LCGetSet tLCGetSet = getLCGetNeedGet(mLCGrpContSchema.getGrpContNo(),endDate);

           if (tLCGetSet == null || tLCGetSet.size()==0)
           {
               CError tError = new CError();
               tError.moduleName = "GrpExpirBenefi" ;
               tError.functionName = "getLCGetNeed "  ;
               tError.errorMessage ="查询保单给付责任失败或该责任已经被给付抽档！";
               this.mErrors.addOneError(tError);
               return false ;
           }
            double sumGetMoney = 0; //总领取金额
            for ( int j = 1 ; j <= tLCGetSet.size() ; j ++)
            {
                Calculator tCalculator = new Calculator();
                LCGetSchema tLCGetSchema = tLCGetSet.get(j).getSchema();
                SSRS tSSRS = new ExeSQL().execSQL("select grppolno,riskcode From lcpol where polno ='"+tLCGetSchema.getPolNo()+"'");
                mGrpPolNo = tSSRS.GetText(1,1);
                mRiskCode = tSSRS.GetText(1,2);

                LMDutyGetAliveDB tLMDutyGetaLiveDB = new LMDutyGetAliveDB();
                tLMDutyGetaLiveDB.setGetDutyCode(tLCGetSchema.getGetDutyCode());
                LMDutyGetAliveSet tLMDutyGetAliveSet=tLMDutyGetaLiveDB.query();
                LMDutyGetAliveSchema tLMDutyGetAliveSchema= tLMDutyGetAliveSet.get(1);

                String pCalCode = tLMDutyGetAliveSchema.getCnterCalCode();
                tCalculator.setCalCode(pCalCode);
                String tAmnt =" select amnt from lcget a,lmdutygetalive b ,lcduty c "
                             +" where a.getdutycode = b.getdutycode and a.dutycode = c.dutycode and a.polno = c.polno "
                             +" and grpContNo = '"+mLCGrpContSchema.getGrpContNo()+"' "
                             +" and insuredno ='"+tLCGetSchema.getInsuredNo()+"'"
                             ;
               System.out.println(tAmnt);
               tCalculator.addBasicFactor("Get",new ExeSQL().getOneValue(tAmnt));
               String tStr = tCalculator.calculate();

               double tValue = 0;

               if (tStr == null || tStr.trim().equals(""))
               {
                   tValue = 0;
               } else
               {
                   tValue = Double.parseDouble(tStr);
               }
               if (!dealClaim(tLCGetSchema.getInsuredNo(),tLCGetSchema.getContNo()))
               {
                   tValue = 0;
               }
               sumGetMoney += tValue;

               LJSGetDrawSchema tLJSGetDrawSchema = new LJSGetDrawSchema();
               tLJSGetDrawSchema.setGetNoticeNo(this.mGetNoticeNo);
               tLJSGetDrawSchema.setGrpContNo(mLCGrpContSchema.getGrpContNo());
               tLJSGetDrawSchema.setGrpPolNo(mGrpPolNo);
               tLJSGetDrawSchema.setContNo(tLCGetSchema.getContNo());
               tLJSGetDrawSchema.setPolNo(tLCGetSchema.getPolNo());
               tLJSGetDrawSchema.setDutyCode(tLCGetSchema.getDutyCode());
               tLJSGetDrawSchema.setRiskCode(mRiskCode);
               tLJSGetDrawSchema.setGetDutyKind(tLCGetSchema.getGetDutyKind());
               tLJSGetDrawSchema.setGetDutyCode(tLCGetSchema.getGetDutyCode());
               tLJSGetDrawSchema.setContNo(tLCGetSchema.getContNo());
               tLJSGetDrawSchema.setAppntNo(mLCGrpContSchema.getAppntNo());
               tLJSGetDrawSchema.setInsuredNo(tLCGetSchema.getInsuredNo());
               tLJSGetDrawSchema.setCurGetToDate(tLCGetSchema.getGetEndDate());
               tLJSGetDrawSchema.setLastGettoDate(tLCGetSchema.getGetEndDate());
               tLJSGetDrawSchema.setGetDate(CurrentDate);
               tLJSGetDrawSchema.setGetMoney(tValue);
               tLJSGetDrawSchema.setEnterAccDate(CurrentDate);
               tLJSGetDrawSchema.setFeeOperationType("QB");
               tLJSGetDrawSchema.setFeeFinaType("TF");
               tLJSGetDrawSchema.setGrpName(new ExeSQL().getOneValue("select grpname From lcgrpcont where grpcontno ='"+mLCGrpContSchema.getGrpContNo()+"'"));
               tLJSGetDrawSchema.setManageCom(new ExeSQL().getOneValue("select ManageCom From lcgrpcont where grpcontno ='"+mLCGrpContSchema.getGrpContNo()+"'"));
               tLJSGetDrawSchema.setAgentCom(new ExeSQL().getOneValue("select AgentCom From lcgrpcont where grpcontno ='"+mLCGrpContSchema.getGrpContNo()+"'"));
               tLJSGetDrawSchema.setAgentType(new ExeSQL().getOneValue("select AgentType From lcgrpcont where grpcontno ='"+mLCGrpContSchema.getGrpContNo()+"'"));
               tLJSGetDrawSchema.setAgentCode(new ExeSQL().getOneValue("select AgentCode From lcgrpcont where grpcontno ='"+mLCGrpContSchema.getGrpContNo()+"'"));
               tLJSGetDrawSchema.setAgentGroup(new ExeSQL().getOneValue("select AgentGroup From lcgrpcont where grpcontno ='"+mLCGrpContSchema.getGrpContNo()+"'"));
               tLJSGetDrawSchema.setSerialNo(mserNo);
               tLJSGetDrawSchema.setOperator(tGI.Operator);
               tLJSGetDrawSchema.setMakeDate(CurrentDate);
               tLJSGetDrawSchema.setMakeTime(CurrentTime);
               tLJSGetDrawSchema.setModifyDate(CurrentDate);
               tLJSGetDrawSchema.setModifyTime(CurrentTime);
               this.tLJSGetDrawSet.add(tLJSGetDrawSchema);
               mMMap.put(tLJSGetDrawSet, "DELETE&INSERT");
            }
            LJSGetSchema tLJSGetSchema = new LJSGetSchema();
            tLJSGetSchema.setGetNoticeNo(mGetNoticeNo);
            tLJSGetSchema.setOtherNo(taskNo);
            tLJSGetSchema.setOtherNoType("21");
            tLJSGetSchema.setPayMode(mPayMode);
            tLJSGetSchema.setSumGetMoney(sumGetMoney);
            tLJSGetSchema.setGetDate(CurrentDate);
            tLJSGetSchema.setManageCom(new ExeSQL().getOneValue("select ManageCom From lcgrpcont where grpcontno ='"+mLCGrpContSchema.getGrpContNo()+"'"));
            tLJSGetSchema.setAgentCom(new ExeSQL().getOneValue("select AgentCom From lcgrpcont where grpcontno ='"+mLCGrpContSchema.getGrpContNo()+"'"));
            tLJSGetSchema.setAgentType(new ExeSQL().getOneValue("select AgentType From lcgrpcont where grpcontno ='"+mLCGrpContSchema.getGrpContNo()+"'"));
            tLJSGetSchema.setAgentCode(new ExeSQL().getOneValue("select AgentCode From lcgrpcont where grpcontno ='"+mLCGrpContSchema.getGrpContNo()+"'"));
            tLJSGetSchema.setAgentGroup(new ExeSQL().getOneValue("select AgentGroup From lcgrpcont where grpcontno ='"+mLCGrpContSchema.getGrpContNo()+"'"));
            tLJSGetSchema.setAppntNo(new ExeSQL().getOneValue("select AppntNo From lcgrpcont where grpcontno ='"+mLCGrpContSchema.getGrpContNo()+"'"));
            tLJSGetSchema.setSerialNo(mserNo);
            tLJSGetSchema.setOperator(tGI.Operator);
            tLJSGetSchema.setMakeDate(CurrentDate);
            tLJSGetSchema.setMakeTime(CurrentTime);
            tLJSGetSchema.setModifyDate(CurrentDate);
            tLJSGetSchema.setModifyTime(CurrentTime);
            tLJSGetSchema.setDealState("0"); //待给付
            mMMap.put(tLJSGetSchema, "DELETE&INSERT");

            // LJAGet
            LJAGetSchema tLJAGetSchema = new LJAGetSchema();
            tLJAGetSchema.setActuGetNo(tLJSGetSchema.getGetNoticeNo());
            tLJAGetSchema.setGetNoticeNo(tLJSGetSchema.getGetNoticeNo());
            tLJAGetSchema.setOtherNo(tLJSGetSchema.getOtherNo());
            tLJAGetSchema.setOtherNoType(tLJSGetSchema.getOtherNoType());
            tLJAGetSchema.setPayMode(tLJSGetSchema.getPayMode());
            tLJAGetSchema.setManageCom(tLJSGetSchema.getManageCom());
            tLJAGetSchema.setAgentCom(tLJSGetSchema.getAgentCom());
            tLJAGetSchema.setAgentType(tLJSGetSchema.getAgentType());
            tLJAGetSchema.setAgentCode(tLJSGetSchema.getAgentCode());
            tLJAGetSchema.setAgentGroup(tLJSGetSchema.getAgentGroup());
            tLJAGetSchema.setAppntNo(tLJSGetSchema.getAppntNo());
            tLJAGetSchema.setAccName(tLJSGetSchema.getAccName());
            tLJAGetSchema.setSumGetMoney(tLJSGetSchema.getSumGetMoney());
            tLJAGetSchema.setBankAccNo(tLJSGetSchema.getBankAccNo());
            tLJAGetSchema.setBankCode(tLJSGetSchema.getBankCode());
            tLJAGetSchema.setDrawer(tLJSGetSchema.getDrawer());
            tLJAGetSchema.setDrawerID(tLJSGetSchema.getDrawerID());
            tLJAGetSchema.setSerialNo(mserNo);
            tLJAGetSchema.setOperator(tGI.Operator);
            tLJAGetSchema.setMakeDate(CurrentDate);
            tLJAGetSchema.setMakeTime(CurrentTime);
            tLJAGetSchema.setModifyDate(CurrentDate);
            tLJAGetSchema.setModifyTime(CurrentTime);
            mMMap.put(tLJAGetSchema, "DELETE&INSERT");

            saveData.clear();
            saveData.add(mMMap);
            System.out.println("合计给付金额:"+sumGetMoney);
       return true;
    }

    private boolean checkBQ()
    {
        String bqSQL = " select 1 From lpgrpedoritem a,lpedorapp b where a.edoracceptno = b.edoracceptno "
                     + " and a.grpcontno ='" + mLCGrpContSchema.getGrpContNo() + "' "
                     + " and b.edorstate !='0' "
                     + " with ur  "
                     ;
        SSRS tSSRS = new ExeSQL().execSQL(bqSQL);
        if (tSSRS.getMaxRow() > 0) {
            CError tError = new CError();
            tError.moduleName = "GrpExpirBenefitGetBL";
            tError.functionName = "checkBQ";
            tError.errorMessage = "该保单有没有结案的保全项目，不能抽档！";
            this.mErrors.addOneError(tError);
        }
        return true;
    }

    public boolean checkLP() {
        ArrayList tArrayList = new ArrayList();
        String lpSQL =" select distinct  customerno from llcase where customerno in(select insuredno from lccont where grpcontno='"+mLCGrpContSchema.getGrpContNo()+"')"
                     +" and rgtstate not in ('11','12','14') with ur "
                     ;
        SSRS tSSRS = new ExeSQL().execSQL(lpSQL);
        if (tSSRS != null && tSSRS.getMaxRow() > 0) {
            CError tError = new CError();
            tError.moduleName = "GrpExpirBenefitGetBL";
            tError.functionName = "checkLP";
            tError.errorMessage = "该保单有没有结案的理赔受理，不能抽档！";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true ;
    }

    public LCGetSet getLCGetNeedGet(String grpContNo,String endDate)
    {
        String sql =" select b.* from lcgrpcont a,lcget b , lmdutygetalive c "
                   +" where 1=1 and a.grpcontno = b.grpcontno and a.appflag = '1' "
                   +" and a.stateflag in ('1','3') "
                   +" and b.getdutycode = c.getdutycode and c.getdutykind  = '0' "
                   +" and a.grpcontno ='"+grpContNo+"' "
                   +" and b.getenddate <='"+endDate+"'"
                   +" and not exists(select 1 from ljsgetdraw d,ljsget e where b.contno = d.ContNo and d.getnoticeno = e.getnoticeno and dealstate !='2'  ) "
                   ;
       LCGetSet tLCGetSet = new LCGetDB().executeQuery(sql);
       if (tLCGetSet==null || tLCGetSet.size() == 0)
       {
           CError tError = new CError();
           tError.moduleName = "GrpExpirBenefitGetBL";
           tError.functionName = "getLCGetNeedGet";
           tError.errorMessage = "查询保单给付责任失败！";
           this.mErrors.addOneError(tError);
           return null;
       }
       return tLCGetSet;
    }

    /**
     * 生成相关流水号
     * @return boolean：成功true，否则false
     */
    private boolean createNo(String ManageCom) {
        String tLimit = PubFun.getNoLimit(ManageCom);
        //产生退费通知书号
        if (mGetNoticeNo == null || mGetNoticeNo.equals("")) {
            mGetNoticeNo = PubFun1.CreateMaxNo("GETNO", tLimit);
        }
        return true;
    }

    /**
     * 如果返回 false,有过理赔赔付满期给付金额为0
     * @param insuredno String
     * @return boolean
     */
    private boolean dealClaim(String insuredno,String contno)
    {
        String sql = " select sum(b.pay) From llcase a,ljagetclaim b where a.caseno=b.otherno and a.rgtstate in ('11','12') "
                     + " and a.customerno = '" + insuredno + "' "
                     + " and b.contno ='"+contno+"'"
                     + " with ur "
                     ;
        String payMoney = new ExeSQL().getOneValue(sql);
        if(payMoney.equals("")||payMoney=="null")
        {
            return true ;
        }
        double x = Math.abs(Double.parseDouble(payMoney));
        if (x > 0.0) 
        {
            return false;
        }
        return true;
    }

}
