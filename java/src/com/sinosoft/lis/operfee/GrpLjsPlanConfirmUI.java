package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:应交费用类（界面输入）（暂对个人）
 * 从错误对象处理类继承，用来保存错误对象,在每个类中都存在
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class GrpLjsPlanConfirmUI {

    //业务处理相关变量
    private VData mInputData;
    public CErrors mErrors = new CErrors();

    public GrpLjsPlanConfirmUI() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {

        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "wuser";
        tGI.ManageCom = "86";
        StringBuffer tSBql = new StringBuffer(128);



        GrpLjsPlanConfirmUI GrpLjsPlanConfirmUI1 = new GrpLjsPlanConfirmUI();
        VData tVData = new VData();
        tVData.add(tGI);
        GrpLjsPlanConfirmUI1.submitData(tVData, "VERIFY");

    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();
        GrpLjsPlanConfirmBL tGrpLjsPlanConfirmBL = new GrpLjsPlanConfirmBL();
        System.out.println("Start GrpLjsPlanConfirm UI Submit...");
        tGrpLjsPlanConfirmBL.submitData(mInputData, cOperate);
        //如果有需要处理的错误，则返回
        if (tGrpLjsPlanConfirmBL.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tGrpLjsPlanConfirmBL.mErrors);
            System.out.println("error num=" + mErrors.getErrorCount());
            return false;
        }
        System.out.println("End GrpLjsPlanConfirm UI Submit...");
        return true;
    }

    private void jbInit() throws Exception {
    }

}

