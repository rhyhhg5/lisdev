package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;
import java.lang.String;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class GrpDueFeeMultiBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private GlobalInput tGI = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String CurrStartDate = ""; //操作开始时间
    private String CurrStartTime = ""; //操作结束时间
    private String StartDate = ""; //应收开始时间
    private String EndDate = ""; //应收结束时间
    private String LoadFlag = ""; //应收结束时间


    /** 数据表  保存数据*/
    //个人保单表
    private LCPolSet mLCPolSet = new LCPolSet();
    private LCPolSchema mLCPolSchema = new LCPolSchema();

    //保费项表
    private LCPremSet mLCPremSet = new LCPremSet();
    private LCPremSchema mLCPremSchema = new LCPremSchema();

    //应收个人交费表
    private LJSPayPersonSet tLJSPayPersonSet = new LJSPayPersonSet();
    private LJSPayPersonSchema mLJSPayPersonSchema = new LJSPayPersonSchema();

    //因收总表
    private LJSPaySet tLJSPaySet = new LJSPaySet();
    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();

    //保险责任表
    private LCDutySet mLCDutySet = new LCDutySet();
    private LCDutySchema mLCDutySchema = new LCDutySchema();

    //集体保单表
    private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();
    private LCGrpPolSchema mLCGrpPolSchema = new LCGrpPolSchema();

    //应收集体交费表
    private LJSPayGrpSchema tLJSPayGrpSchema;
    private LJSPayGrpSet tLJSPayGrpSet = new LJSPayGrpSet();
    private String mserNo = ""; //批次号


    //通知书打印表
    private LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();

    private TransferData mTransferData = new TransferData();

    public GrpDueFeeMultiBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        GrpDueFeeMultiBL tGrpDueFeeMultiBL = new GrpDueFeeMultiBL();

        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "000006";
        tGI.ManageCom = "86";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("StartDate", "2006-04-04");
        tTransferData.setNameAndValue("EndDate", "2006-08-22");
        tTransferData.setNameAndValue("ManageCom", "86");

        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tTransferData);

        if (tGrpDueFeeMultiBL.submitData(tVData, "INSERT") == false) {
            System.out.println("团单批处理催收信息提示：：" +
                               tGrpDueFeeMultiBL.mErrors.getFirstError());
        } else {
            System.out.println("团单批处理催收完成");
        }
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {

        System.out.println("Start GrpDueFeeMulti BL Submit...");

        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        if (!getInputData(cInputData)) {
            return false;
        }

        if (!dealData()) {
            return false;
        }

        return true;
    }

    private boolean dealData() {
        //先查出符合条件的团单，然后接着调用GrpDueFeeBL的Submit方法
        String managecom = (String) mTransferData.getValueByName("ManageCom");
        StringBuffer GrpContSql = new StringBuffer();
        GrpContSql.append("select * from LCGrpCont b where appflag='1' and (StateFlag is null or StateFlag = '1') ")
                  .append("and GrpContNo not in (select otherno from ljspay where othernotype='1') ")
                  .append("and GrpContNo in (select GrpContNo from LCGrpPol where (PaytoDate>='").append(StartDate)
                  .append("' and PaytoDate<='").append(EndDate).append("' and PaytoDate<PayEndDate) ")
                  .append("and managecom like '").append(managecom).append("%%' ")
                  .append("and PayIntv>0 and AppFlag='1' and (StateFlag is null or StateFlag = '1') ")
                  .append("and RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y')) ");
                  
        if("WMD".equals(LoadFlag))
        {
        	GrpContSql.append("and exists (select 1 from LCCont ")
        	          .append("where polType = '1' and appFlag = '1' and (StateFlag is null or StateFlag = '1') ")
        	          .append("and grpContNo = b.grpContNo) with ur");
        }
        System.out.println(GrpContSql.toString());
        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        tLCGrpContSet = tLCGrpContDB.executeQuery(GrpContSql.toString());
        if (tLCGrpContDB.mErrors.needDealError()) {
            CError.buildErr(this, "查询符合催收条件的团单失败！");
            return false;
        }
        if (tLCGrpContSet.size() == 0) {
            CError.buildErr(this, "没有符合催收条件的团单信息！");
            return false;
        }

        int succCount = 0;
        String tLimit = PubFun.getNoLimit(managecom);
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        mserNo = serNo;
        if (!dealUrgeLog("1", "INSERT")) {
            return false;
        }
        //int failCount=0;
        for (int i = 1; i <= tLCGrpContSet.size(); i++) {

            String grpContNo = tLCGrpContSet.get(i).getGrpContNo();
            LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
            tLCGrpContSchema.setGrpContNo(grpContNo);
            VData tVData = new VData();
            tVData.add(tLCGrpContSchema);
            tVData.add(tGI);
            mTransferData.setNameAndValue("serNo", serNo);
            mTransferData.setNameAndValue("CurrStartDate", CurrStartDate);
            mTransferData.setNameAndValue("CurrStartTime", CurrStartTime);
            tVData.add(mTransferData);
            GrpDueFeeUI tGrpDueFeeUI = new GrpDueFeeUI();
            if (!tGrpDueFeeUI.submitData(tVData, "INSERT")) {
                //如果处理失败
                this.mErrors.addOneError("团单号" + grpContNo + "催收失败，原因如下：" +
                                         tGrpDueFeeUI.mErrors.getFirstError());

            } else {
                succCount++;
            }
        }
        if (succCount == tLCGrpContSet.size()) {
            System.out.println("本次批量催收全部成功！");
        }
        if (!dealUrgeLog("3", "UPDATE")) {
            return false;
        }

        return true;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) {
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
                "TransferData", 0);
        if (tGI == null || mTransferData == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpDueFeeMultiBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        StartDate = (String) mTransferData.getValueByName("StartDate");
        EndDate = (String) mTransferData.getValueByName("EndDate");
        LoadFlag = (String) mTransferData.getValueByName("LoadFlag");
        return true;
    }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData() {
        mInputData = new VData();
        return true;
    }

    /**
     * 加入到催收核销日志表数据
     * @param
     * @return boolean
     */
    private boolean dealUrgeLog(String pmDealState, String pmOpreat) {

        LCUrgeVerifyLogSchema tLCUrgeVerifyLogSchema = new
                LCUrgeVerifyLogSchema();
        //加到催收核销日志表
        tLCUrgeVerifyLogSchema.setSerialNo(mserNo);
        tLCUrgeVerifyLogSchema.setRiskFlag("1");
        tLCUrgeVerifyLogSchema.setStartDate(StartDate);
        tLCUrgeVerifyLogSchema.setEndDate(EndDate);

        tLCUrgeVerifyLogSchema.setOperateType("1"); //1：续期催收操作,2：续期核销操作
        tLCUrgeVerifyLogSchema.setOperateFlag("2"); //1：个案操作,2：批次操作
        tLCUrgeVerifyLogSchema.setOperator(tGI.Operator);
        tLCUrgeVerifyLogSchema.setDealState(pmDealState); //1 : 正在处理中,2 : 处理错误,3 : 处理已完成
        if (pmOpreat.equals("INSERT")) {
            tLCUrgeVerifyLogSchema.setMakeDate(CurrentDate);
            CurrStartDate = CurrentDate;
            tLCUrgeVerifyLogSchema.setMakeTime(CurrentTime);
            CurrStartTime = CurrentTime;
            tLCUrgeVerifyLogSchema.setModifyDate(CurrentDate);
            tLCUrgeVerifyLogSchema.setModifyTime(CurrentTime);
            tLCUrgeVerifyLogSchema.setErrReason("");

        } else {
            LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new LCUrgeVerifyLogDB();
            LCUrgeVerifyLogSet tLCUrgeVerifyLogSet = new
                    LCUrgeVerifyLogSet();
            tLCUrgeVerifyLogDB.setSerialNo(mserNo);
            tLCUrgeVerifyLogDB.setOperateType("1");
            tLCUrgeVerifyLogDB.setOperateFlag("2");
            tLCUrgeVerifyLogDB.setRiskFlag("1");
            tLCUrgeVerifyLogSet = tLCUrgeVerifyLogDB.query();
             if (!(tLCUrgeVerifyLogSet ==null )&& tLCUrgeVerifyLogSet.size() > 0) {
                 tLCUrgeVerifyLogSchema = tLCUrgeVerifyLogSet.get(1);
                 tLCUrgeVerifyLogSchema.setModifyDate(PubFun.getCurrentDate());
                 tLCUrgeVerifyLogSchema.setModifyTime(PubFun.getCurrentTime());
                 tLCUrgeVerifyLogSchema.setDealState(pmDealState);
             }else{
                return false;
            }


        }

        MMap tMap = new MMap();
        tMap.put(tLCUrgeVerifyLogSchema, pmOpreat);
        VData tInputData = new VData();
        tInputData.add(tMap);
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(tInputData, "") == false) {
            this.mErrors.addOneError("PubSubmit:处理LCUrgeVerifyLog 表失败!");
            return false;
        }
        return true;
    }

    /**
     * 准备打印数据
     * @param tLCPolSchema
     * @return
     */
    public LOPRTManagerSchema getPrintData(LCGrpContSchema tLCGrpContSchema) {
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        try {
            String tLimit = PubFun.getNoLimit(tLCGrpContSchema.getManageCom());
            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
            tLOPRTManagerSchema.setOtherNo(tLCGrpContSchema.getGrpContNo());
            tLOPRTManagerSchema.setOtherNoType("01");
            tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_REPAY);
            tLOPRTManagerSchema.setManageCom(tLCGrpContSchema.getManageCom());
            tLOPRTManagerSchema.setAgentCode(tLCGrpContSchema.getAgentCode());
            tLOPRTManagerSchema.setReqCom(tLCGrpContSchema.getManageCom());
            tLOPRTManagerSchema.setReqOperator(tLCGrpContSchema.getOperator());
            tLOPRTManagerSchema.setPrtType("0");
            tLOPRTManagerSchema.setStateFlag("0");
            tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
        } catch (Exception ex) {
            return null;
        }
        return tLOPRTManagerSchema;
    }

    private void jbInit() throws Exception {
    }

}
