 package com.sinosoft.lis.operfee;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import java.util.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单查询业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class LCGrpPolQueryBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  /** 业务处理相关变量 */
  /** 集体保单费表 */
  private LCGrpPolSchema  mLCGrpPolSchema = new LCGrpPolSchema() ;
  private LCGrpPolSet  mLCGrpPolSet = new LCGrpPolSet() ;
  private TransferData mTransferData = new TransferData();

  public LCGrpPolQueryBL() {}

  public static void main(String[] args) {
  }


  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
System.out.println("---getInputData---");

    //进行业务处理
	    if (!queryLJGrpPol())
	      return false;
System.out.println("---queryLJGrpPol---");

    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    // 检验查询条件
    mLCGrpPolSchema.setSchema((LCGrpPolSchema)cInputData.getObjectByObjectName("LCGrpPolSchema",0));
   mTransferData=(TransferData)cInputData.getObjectByObjectName("TransferData",0);
    if(mLCGrpPolSchema == null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "LCGrpPolQueryBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "请输入查询条件!";
      this.mErrors.addOneError(tError) ;
      return false;
    }
    return true;
  }


  /**
   * 查询集体保单表信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean queryLJGrpPol()
  {

      //拼写SQL串,多条件查询
      String GrpPolNo=mLCGrpPolSchema.getGrpPolNo();
      String GrpContNo=mLCGrpPolSchema.getGrpContNo();
      String ManageCom=mLCGrpPolSchema.getManageCom();
      String RiskCode=mLCGrpPolSchema.getRiskCode(); //险种编码是否催交标志
      String CurrentDate="";
      String SubDate="";
      //下面是催收的时间范围--当天至当天+提前日期
//      String CurrentDate = PubFun.getCurrentDate();
//      String AheadDays="";//催收提前天数--默认30天
//      LDSysVarDB tLDSysVarDB=new LDSysVarDB();
//      tLDSysVarDB.setSysVar("aheaddays");
//      if(tLDSysVarDB.getInfo()==false)
//      {
//          AheadDays="30";
//      }
//      else
//      {
//          AheadDays= tLDSysVarDB.getSysVarValue();
//      }
//      //判断当前日期<=交至日期且当前日期+AheadDays>=交至日期
//      FDate tD=new FDate();
//      Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
//      String SubDate=tD.getString(AfterDate);
//      //因为存在已经过了催缴时间的保单-为简易操作-可以将系统变量-提前催收天数改为负数，则可以灵活处理-对个单和批量都有效
//      if(tD.getDate(CurrentDate).before(tD.getDate(SubDate)))
//      {
//          //如果系统变量-催收天数是正数，那么SubDate大于CurrentDate，则催收范围是>=CurrentDate&&<=SubDate
//          System.out.println("Current Date:"+CurrentDate);
//          System.out.println("AfterDate:"+SubDate);
//      }
//      else
//      {
//          //如果系统变量-催收天数是负数，那么SubDate小于CurrentDate，则催收范围是<=CurrentDate&&>=SubDate
//          //为了统一处理，则将CurrentDate和SubDate值互换,则后续催收范围保持为>=CurrentDate&&<=SubDate
//          System.out.println("Current Date:"+CurrentDate);
//          System.out.println("BeforeDate:"+SubDate);
//          CurrentDate=SubDate;
//          SubDate=PubFun.getCurrentDate();
//      }

      String   sqlStr="select * from LCGrpPol";

      if(RiskCode==null) RiskCode="";

      if(GrpPolNo!=null&&GrpPolNo!="")   //单个集体催收
       {
          sqlStr=sqlStr+" where GrpPolNo='"+GrpPolNo+"' and AppFlag='1' ";
          if(ManageCom!=null&&ManageCom!="")
          {
              String MaxManageCom=PubFun.RCh(ManageCom,"9",8);
              String MinManageCom=PubFun.RCh(ManageCom,"0",8);;
              sqlStr=sqlStr+" and ManageCom>='"+MinManageCom+"' and ManageCom<='"+MaxManageCom+"'";
          }
          //sqlStr=sqlStr+" and (PaytoDate>='"+CurrentDate+"' and PaytoDate<='"+SubDate+"' and PaytoDate<PayEndDate)";
          sqlStr=sqlStr+" and  PaytoDate<PayEndDate ";
          if(RiskCode.equals("N"))
              sqlStr=sqlStr+" and RiskCode in(select RiskCode from LMRiskPay where UrgePayFlag='N') ";
          else if(RiskCode.equals("Y"))//如果险种编码对应催交
              sqlStr=sqlStr+" and RiskCode in(select RiskCode from LMRiskPay where UrgePayFlag='Y') ";
          sqlStr=sqlStr+" and GrpPolNo not in(select GrpPolNo from LJSPayGrp)";
       }
       else  //批量集体催收 日期范围
       {
         if(GrpContNo!=null&&!GrpContNo.equals(""))
         {
           sqlStr=sqlStr+" where grpcontno='"+GrpContNo+"'";
           if(mTransferData!=null)
           {
               CurrentDate=(String)mTransferData.getValueByName("StartDate");
               SubDate=(String)mTransferData.getValueByName("EndDate");
               if(CurrentDate==null||SubDate==null||CurrentDate.equals("")||SubDate.equals(""))
               {
                   CError.buildErr(this,"必须录入日期范围!");
                   return false;
               }
               FDate tD=new FDate();
               if(!tD.getDate(CurrentDate).before(tD.getDate(SubDate)))
               {
                   CError.buildErr(this,"日期范围录入错误!");
                   return false;
               }
           }
           else
           {
               CError.buildErr(this,"必须录入日期范围!");
               return false;
           }
           sqlStr=sqlStr+" and (PaytoDate>='"+CurrentDate+"' and PaytoDate<='"+SubDate+"' and PaytoDate<PayEndDate ) and AppFlag='1'";
           if(ManageCom!=null&&ManageCom!="")
           {
               String MaxManageCom=PubFun.RCh(ManageCom,"9",8);
               String MinManageCom=PubFun.RCh(ManageCom,"0",8);;
               sqlStr=sqlStr+" and ManageCom>='"+MinManageCom+"' and ManageCom<='"+MaxManageCom+"'";
           }
           if(RiskCode.equals("N"))
               sqlStr=sqlStr+" and RiskCode in(select RiskCode from LMRiskPay where UrgePayFlag='N') ";
           else if(RiskCode.equals("Y"))//如果险种编码对应催交
               sqlStr=sqlStr+" and RiskCode in(select RiskCode from LMRiskPay where UrgePayFlag='Y') ";
           sqlStr=sqlStr+" and GrpPolNo not in(select GrpPolNo from LJSPayGrp)";
       }
       }
       System.out.println("SQL of Finding LCGrpPol : ="+sqlStr);

      LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
      tLCGrpPolDB.setSchema( mLCGrpPolSchema);

      mLCGrpPolSet = tLCGrpPolDB.executeQuery(sqlStr);

      if (tLCGrpPolDB.mErrors.needDealError() == true)
      {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "LCGrpPolQueryBL";
      tError.functionName = "queryData";
      tError.errorMessage = "集体保单表查询失败!";
      this.mErrors.addOneError(tError);
      mLCGrpPolSet.clear();
      return false;
    }
    if (mLCGrpPolSet.size() == 0)
    {
    // @@错误处理
    CError tError = new CError();
    tError.moduleName = "LCGrpPolQueryBL";
    tError.functionName = "queryData";
    tError.errorMessage = "没有查询到符合条件的集体保单表!";
    this.mErrors.addOneError(tError);
    mLCGrpPolSet.clear();
    return false;
    }

	mResult.clear();
	mResult.add( mLCGrpPolSet );
	return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData()
  {
    mResult.clear();
    try
    {
      mResult.add( mLCGrpPolSet );
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="LCGrpPolQueryBL";
      tError.functionName="prepareOutputData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
    }
//团体续期批量催收获取符合条件的集体合同号码
    public LCGrpContSet queryLJGrpCont(TransferData mTransferData,String ManageCom)
    {
       String CurrentDate="";
       String SubDate="";
       LCGrpContSet tLCGrpContSet=new LCGrpContSet();
       LCGrpContDB tLCGrpContDB=new LCGrpContDB();
       String sqlstr="select *from lcgrpcont where grpcontno in (select grpcontno from lcgrppol where ";
       if(mTransferData!=null)
       {
           CurrentDate=(String)mTransferData.getValueByName("StartDate");
           SubDate=(String)mTransferData.getValueByName("EndDate");
           if(CurrentDate==null||SubDate==null||CurrentDate.equals("")||SubDate.equals(""))
           {
               CError.buildErr(this,"必须录入日期范围!");
           }
           FDate tD=new FDate();
           if(!tD.getDate(CurrentDate).before(tD.getDate(SubDate)))
           {
               CError.buildErr(this,"日期范围录入错误!");
           }
       }
       else
       {
           CError.buildErr(this,"必须录入日期范围!");
       }
       sqlstr=sqlstr+" (PaytoDate>='"+CurrentDate+"' and PaytoDate<='"+SubDate+"' and PaytoDate<PayEndDate ) and AppFlag='1'";
       if(ManageCom!=null&&ManageCom!="")
       {
           String MaxManageCom=PubFun.RCh(ManageCom,"9",8);
           String MinManageCom=PubFun.RCh(ManageCom,"0",8);;
           sqlstr=sqlstr+" and ManageCom>='"+MinManageCom+"' and ManageCom<='"+MaxManageCom+"'";
       }//如果险种编码对应催交
           sqlstr=sqlstr+" and RiskCode in(select RiskCode from LMRiskPay where UrgePayFlag='Y') ";
           sqlstr=sqlstr+" and GrpPolNo not in(select GrpPolNo from LJSPayGrp)";
           sqlstr=sqlstr+")";
       System.out.println("SQL of Finding LCGrpCont:="+sqlstr);
       tLCGrpContSet=tLCGrpContDB.executeQuery(sqlstr);
       return tLCGrpContSet;
   }

    /**
 * 针对团体催收的批量查询
 * @return
 */
    public boolean queryLJGrpPol2(LCGrpPolSchema tLCGrpPolSchema)
    {
        if(tLCGrpPolSchema==null)
        {
            CError tError = new CError();
            tError.moduleName = "LCGrpQueryBL";
            tError.functionName = "queryLCGrp2";
            tError.errorMessage = "保单表无效!";
            this.mErrors.addOneError(tError);
            return false;
        }
        //拼写SQL串,多条件查询
        String ManageCom=tLCGrpPolSchema.getManageCom();
        String RiskCode=tLCGrpPolSchema.getRiskCode();
        String CurrentDate=tLCGrpPolSchema.getPaytoDate();//存放那天可以催收的日期
        String GrpPolNo=tLCGrpPolSchema.getGrpPolNo();
        int    PayIntv = tLCGrpPolSchema.getPayIntv();
//默认是系统第二天
        if(CurrentDate==null)
        {
            CurrentDate= PubFun.getCurrentDate();
        }

        String AheadDays=""; //催收提前天数--默认30天
        LDSysVarDB tLDSysVarDB=new LDSysVarDB();
        tLDSysVarDB.setSysVar("aheaddays");
        if(tLDSysVarDB.getInfo()==false)
        {
            AheadDays="30";
        }
        else
        {
            AheadDays= tLDSysVarDB.getSysVarValue();
        }
//判断当前日期<=交至日期且当前日期+AheadDays>=交至日期
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        String SubDate=tD.getString(AfterDate);
//因为存在已经过了催缴时间的保单-为简易操作-可以将系统变量-提前催收天数改为负数，则可以灵活处理-对个单和批量都有效
        if(tD.getDate(CurrentDate).before(tD.getDate(SubDate)))
        {
            //如果系统变量-催收天数是正数，那么SubDate大于CurrentDate，则催收范围是>=CurrentDate&&<=SubDate
            System.out.println("Current Date:"+CurrentDate);
            System.out.println("AfterDate:"+SubDate);
        }
        else
        {
            //如果系统变量-催收天数是负数，那么SubDate小于CurrentDate，则催收范围是<=CurrentDate&&>=SubDate
            //为了统一处理，则将CurrentDate和SubDate值互换,则后续催收范围保持为>=CurrentDate&&<=SubDate
            System.out.println("Current Date:"+CurrentDate);
            System.out.println("BeforeDate:"+SubDate);
            CurrentDate=SubDate;
            SubDate=PubFun.getCurrentDate();
        }

        String sqlStr="select * from LCGrpPol";
        sqlStr=sqlStr+" where (PaytoDate>='"+CurrentDate+"' and PaytoDate<='"+SubDate+"' and PaytoDate<PayEndDate ) and AppFlag='1'";
        if(ManageCom!=null)
        {
            String MaxManageCom=PubFun.RCh(ManageCom,"9",8);
            String MinManageCom=PubFun.RCh(ManageCom,"0",8);
            sqlStr=sqlStr+" and ManageCom>='"+MinManageCom+"' and ManageCom<='"+MaxManageCom+"'";
        }
        if(PayIntv==-1||PayIntv==0)
            sqlStr=sqlStr+" and PayIntv>0";
        else
            sqlStr=sqlStr+" and PayIntv="+PayIntv+"";
        if(GrpPolNo!=null)
            sqlStr=sqlStr+" and GrpPolNo='"+GrpPolNo+"'";
        if(RiskCode!=null)
            sqlStr=sqlStr+" and 1=(select 1 from LMRiskPay where UrgePayFlag='Y' and riskcode='"+RiskCode+"') ";

        sqlStr=sqlStr+" and GrpPolNo not in(select GrpPolNo from LJSPayGrp)";
        sqlStr=sqlStr+" and RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y')" ;

        System.out.println("in BL SQL="+sqlStr);

        LCGrpPolDB tLCGrpPolDB = new LCGrpPolDB();
        mLCGrpPolSet = tLCGrpPolDB.executeQuery(sqlStr);
        if (tLCGrpPolDB.mErrors.needDealError() == true)
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tLCGrpPolDB.mErrors);
            CError tError = new CError();
            tError.moduleName = "LCGrpQueryBL";
            tError.functionName = "queryData";
            tError.errorMessage = "团体保单表查询失败!";
            this.mErrors.addOneError(tError);
            mLCGrpPolSet.clear();
            return false;
        }
        if (mLCGrpPolSet.size() == 0)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "LCGrpQueryBL";
            tError.functionName = "queryData";
            tError.errorMessage = "没有查询到符合条件的团体保单表!";
            this.mErrors.addOneError(tError);
            mLCGrpPolSet.clear();
            return false;
        }

        mResult.clear();
        mResult.add( mLCGrpPolSet );
        return true;
    }

}
