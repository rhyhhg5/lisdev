package com.sinosoft.lis.operfee;

import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.vschema.LDCodeSet;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.db.LJAPayDB;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.f1print.PrintService;
import com.sinosoft.lis.vschema.LJAPaySet;
import java.io.File;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.lis.pubfun.PubFun;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class GrpPayCompBatchPrintBL implements PrintService {
    /**错误的容器*/

    TextTag textTag = new TextTag();
    private SSRS mSSRS;
    private VData mResult = null;
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = null;

    private LJAPaySet mLJAPaySet = new LJAPaySet();
    private LDSysVarSchema mLDSysVarSchema = new LDSysVarSchema();
    private LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();

    private int mCount = 0;
    private String mPayNo = ""; //收据号
    private String mxmlFileName = "";
    private String strLogs = "";
    private String Content = "";
    private String FlagStr = "";
    private boolean operFlag = true;
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap mMap = new MMap();
    private PubSubmit tPubSubmit = new PubSubmit();

    public GrpPayCompBatchPrintBL() {}

    /**
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //取得外部传入数据
        if (!getInputData(cInputData)) {
            return false;
        }

        //获取打印所需数据
        if (!getPrintData()) {
            return false;
        }
        if (!prepareOutputData()) {
            return false;
        }
        tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            if (tPubSubmit.mErrors.needDealError()) {
                buildError("submitData", "保存再保计算结果出错!");
            }
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        try {
            mLDSysVarSchema.setSchema((LDSysVarSchema) cInputData.
                                      getObjectByObjectName("LDSysVarSchema", 0));
            mGlobalInput = (GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0);
            mLJAPaySet = (LJAPaySet) cInputData.getObjectByObjectName(
                    "LJAPaySet", 0);
            if (mLJAPaySet == null) { //（服务事件关联表）
                // @@错误处理
                CError tError = new CError();
                tError.moduleName = "GrpPayCompBatchPrintBL";
                tError.functionName = "getInputData";
                tError.errorMessage = "传入的数据为空!";
                this.mErrors.addOneError(tError);
                return false;
            }
        } catch (Exception e) {
            mErrors.addOneError("getInputData中传入的数据不完整。");
            System.out.println("getInputData传入的数据不完整，" + e.toString());
            return false;
        }

        return true;
    }

    /**
     * 获取打印所需数据
     * @param schema LCContSchema
     * @return String
     */
    private boolean getPrintData() {
        VData tVData;
        XmlExport txmlExportAll = new XmlExport();
        txmlExportAll.createDocument("ON");
        mCount = mLJAPaySet.size();
        String mXmlFileName[] = new String[mCount];
        LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet(); //新增，用来更新打印状态，次数等信息。by huxl @ 20071024
        for (int i = 1; i <= mCount; i++) {
            LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
            LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
            tLOPRTManagerDB.setCode("90");
            tLOPRTManagerDB.setStandbyFlag2(mLJAPaySet.get(i).getPayNo());
            tLOPRTManagerSchema = tLOPRTManagerDB.query().get(1);

            if (tLOPRTManagerSchema == null) {
                VData xVData = new VData();
                TransferData xTransferData = new TransferData();
                xTransferData.setNameAndValue("0", "0");

                LJAPaySchema xLJAPaySchema = new LJAPaySchema();

                LJAPayDB t = new LJAPayDB();
                t.setPayNo(mLJAPaySet.get(i).getPayNo());
                xLJAPaySchema = t.query().get(1);

                xVData.addElement(mGlobalInput);
                xVData.addElement(xTransferData);
                xVData.addElement(xLJAPaySchema);
                GrpPayComptPrintBL tGrpPayComptPrintBL = new GrpPayComptPrintBL();
                tGrpPayComptPrintBL.submitData(xVData, "INSERT");

                LOPRTManagerDB xLOPRTManagerDB = new LOPRTManagerDB();
                xLOPRTManagerDB.setCode("90");
                xLOPRTManagerDB.setStandbyFlag2(mLJAPaySet.get(i).getPayNo());
                tLOPRTManagerSchema = null;
                tLOPRTManagerSchema = tLOPRTManagerDB.query().get(1);
            }
            //下面封装要更新的打印管理信息
            tLOPRTManagerSchema.setStateFlag("1");
            tLOPRTManagerSchema.setPrintTimes(tLOPRTManagerSchema.
                                              getPrintTimes() + 1);
            tLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
            tLOPRTManagerSchema.setDoneTime(PubFun.getCurrentTime());
            tLOPRTManagerSet.add(tLOPRTManagerSchema);

            System.out.println("~~~~~第" + i + "个StandbyFlag2--" +
                               tLOPRTManagerSchema.getStandbyFlag2());
            mxmlFileName = StrTool.unicodeToGBK("0" +
                                                tLOPRTManagerSchema.getCode()) +
                           "-" + tLOPRTManagerSchema.getPrtSeq() + "-" +
                           tLOPRTManagerSchema.getOtherNo();
            mXmlFileName[i - 1] = mxmlFileName;
            System.out.println("~~~~~第" + i + "个文件--" + mxmlFileName);
            if (!callPrintService(tLOPRTManagerSchema)) {
                FlagStr = "Fail";
                Content = " 印刷号" + tLOPRTManagerSchema.getPrtSeq() + "：" +
                          this.mErrors.getFirstError().toString();
                strLogs = strLogs + Content;
                continue;
            }
            XmlExport txmlExport = (XmlExport) mResult.getObjectByObjectName(
                    "XmlExport", 0);
            if (txmlExport == null) {
                FlagStr = "Fail";
                Content = "印刷号" + tLOPRTManagerSchema.getPrtSeq() +
                          "没有得到要显示的数据文件！";
                strLogs = strLogs + Content;
                continue;
            }
            if (operFlag == true) {
                File f = new File(mLDSysVarSchema.getSysVarValue());
                f.mkdir();
                System.out.println("PATH : " + mLDSysVarSchema.getSysVarValue());
                System.out.println("PATH2 : " +
                                   mLDSysVarSchema.getSysVarValue().
                                   substring(0,
                                             mLDSysVarSchema.getSysVarValue().
                                             length() -
                                             1));
                txmlExport.outputDocumentToFile(mLDSysVarSchema.getSysVarValue().
                                                substring(0,
                        mLDSysVarSchema.getSysVarValue().length() - 1) +
                                                File.separator + "printdata"
                                                + File.separator + "data" +
                                                File.separator + "brief" +
                                                File.separator, mxmlFileName);
            }
        }
        this.mMap.put(tLOPRTManagerSet, "UPDATE");
        tVData = new VData();
        tVData.add(mXmlFileName);
        mResult = tVData;
        return true;

    }


    /**
     * 查询投保人账户余额
     * @param appntNo String
     * @return String
     */
    private String getAccBala(String customerNo) {
        StringBuffer sql = new StringBuffer();
        sql.append("select accBala ")
                .append("from LCAppAcc ")
                .append("where customerNo = '")
                .append(customerNo)
                .append("' ");
        String bala = new ExeSQL().getOneValue(sql.toString());
        if (bala.equals("") || bala.equals("null")) {
            return "0.00";
        } else {
            return CommonBL.bigDoubleToCommonString(Double.parseDouble(bala),
                    "0.00");
        }
    }

    /**
     * 计算总应收
     * @param tLJAPayDB LJAPayDB
     * @return String
     */
    private String getSumDuePayMoney(LJAPayDB tLJAPayDB, String payType) {
        StringBuffer sql = new StringBuffer();
        sql.append("select sum(abs(sumDuePayMoney)) ")
                .append("from LJAPayGrp ")
                .append("where payNo = '")
                .append(tLJAPayDB.getPayNo())
                .append("'  and payType = '").append(payType).append("' ");
        String money = new ExeSQL().getOneValue(sql.toString());
        if (money.equals("") || money.equals("null")) {
            return "0.00";
        } else {
            money = CommonBL.bigDoubleToCommonString(Double.parseDouble(money),
                    "0.00");
        }
        return money;
    }

    /**
     * 查询列表显示数据
     * @param schema LCContSchema
     * @return String
     */

    private boolean getListData() {
        String peoples2;
        if (isWMD(tLCGrpContSchema.getGrpContNo())) {
            peoples2 = " (select peoples2Input from LCNoNamePremTrace "
                       + "where getNoticeNo = b.getNoticeNo "
                       + "   and contPlanCode = a.contPlanCode) ";
        } else {
            peoples2 = " count(distinct a.insuredNo) ";
        }

        StringBuffer sqlSB = new StringBuffer();
        sqlSB.append("select a.contPlanCode, ")
                .append(peoples2)
                .append(
                        ", rtrim(char(min(b.LastPayToDate)))||'~'||rtrim(char(min(b.CurPayToDate))), ")
                .append(
                        " varchar(sum(b.sumActuPayMoney)), varchar(sum(b.sumActuPayMoney)) ")
                .append("from LCPol a, LJAPayPerson b ")
                .append("where a.polNo = b.polNo ")
                .append("and b.payNo = '")
                .append(mPayNo)
                .append("' group by a.contPlanCode, b.getNoticeNo ");

        String tSQL = sqlSB.toString();
        System.out.println(tSQL);
        ExeSQL tExeSQL = new ExeSQL();

        mSSRS = tExeSQL.execSQL(tSQL);
        if (tExeSQL.mErrors.needDealError()) {
            CError tError = new CError();
            tError.moduleName = "MakeXMLBL";
            tError.functionName = "creatFile";
            tError.errorMessage = "查询XML数据出错！";
            this.mErrors.addOneError(tError);
            return false;
        }

        return true;
    }


    /**
     *  获取列表数据
     * @param schema LCContSchema
     * @return String
     */
    private ListTable getListTable() {
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[8];
            info[0] = Integer.toString(i);
            for (int j = 1; j <= mSSRS.getMaxCol(); j++) {
                info[j] = mSSRS.GetText(i, j);
            }
            tListTable.add(info);
        }

        tListTable.setName("ZT");
        return tListTable;
    }

    /**
     * 校验保单是否无名单
     * @param grpContNo String
     * @return boolean
     */
    private boolean isWMD(String grpContNo) {
        StringBuffer sql = new StringBuffer();
        sql.append("select 1 ")
                .append("from LCCont ")
                .append("where polType = '1' ")
                .append("   and grpContNo = '")
                .append(grpContNo)
                .append("' ");
        String t = new ExeSQL().getOneValue(sql.toString());
        if (t.equals("") || t.equals("null")) {
            return false;
        }
        return true;
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    public int getCount() {
        return mCount;
    }


    public static void main(String[] args) {
        GrpPayCompBatchPrintBL p = new GrpPayCompBatchPrintBL();
        LDSysVarSchema mLDSysVarSchema = new LDSysVarSchema();
        mLDSysVarSchema.setSysVarValue("E:\\workspace\\ui\\");

        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.Operator = "wuser";
        tGlobalInput.ComCode = "86";
        tGlobalInput.ManageCom = "86";

        LJAPaySet tLJAPaySet = new LJAPaySet();
        LJAPaySchema tLJAPaySchema = new LJAPaySchema();
        tLJAPaySchema.setPayNo("32000272287");
        tLJAPaySet.add(tLJAPaySchema);
        LJAPaySchema tLJAPaySchema2 = new LJAPaySchema();
        tLJAPaySchema2.setPayNo("32000271882");
        tLJAPaySet.add(tLJAPaySchema2);

        VData v = new VData();
        v.add(tGlobalInput);
        v.add(tLJAPaySet);
        v.add(mLDSysVarSchema);

        if (!p.submitData(v, "PRINT")) {
            System.out.println(p.mErrors.getErrContent());
        } else {
            System.out.println("OK");
        }
    }

    public void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "GrpPayCompBatchPrintBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    private boolean callPrintService(LOPRTManagerSchema aLOPRTManagerSchema) {
        // 查找打印服务
        String strSQL = "SELECT * FROM LDCode WHERE CodeType = 'print_service'";
        strSQL += " AND Code = '" + aLOPRTManagerSchema.getCode() + "'";
        strSQL += " AND OtherSign = '0'";
        System.out.println(strSQL);
        LDCodeSet tLDCodeSet = new LDCodeDB().executeQuery(strSQL);

        if (tLDCodeSet.size() == 0) {
            buildError("dealData",
                       "找不到对应的打印服务类(Code = '" + aLOPRTManagerSchema.getCode() +
                       "')");
            return false;
        }

        // 调用打印服务
        LDCodeSchema tLDCodeSchema = tLDCodeSet.get(1);

        try {
            Class cls = Class.forName(tLDCodeSchema.getCodeAlias());
            PrintService ps = (PrintService) cls.newInstance();

            // 准备数据
            String strOperate = tLDCodeSchema.getCodeName();

            VData vData = new VData();

            vData.add(mGlobalInput);
            vData.add(aLOPRTManagerSchema);

            if (!ps.submitData(vData, strOperate)) {
                mErrors.copyAllErrors(ps.getErrors());
                return false;
            }
            mResult = ps.getResult();

        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("callPrintService", ex.toString());
            return false;
        }
        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpPayCompBatchPrintBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

}
