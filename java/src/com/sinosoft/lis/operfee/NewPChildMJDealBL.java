package com.sinosoft.lis.operfee;

import com.sinosoft.lis.db.LJSPayDB;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.bq.EdorItemSpecialData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class NewPChildMJDealBL {

    private String mManageCom = "";
    private GlobalInput mG = new GlobalInput();

     public CErrors mErrors = new CErrors();

    public NewPChildMJDealBL() {
    }

    private boolean getInputData(VData cInputData) {
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
//        mManageCom = mG.ManageCom;
        return true;
    }

      public boolean submitData(VData cInputData, String cOperate) {
          cInputData = (VData) cInputData.clone();

          if (!getInputData(cInputData)) {
              return false;
          }
          //关联续期应收应收，对已经续期抽档的少儿险保单进行满期给付抽档。
          String sql = "select distinct a.contno,a.cvalidate,a.enddate,a.managecom,a.agentgroup,a.agentcode,a.enddate,getnoticeno "
                       + " from lcpol a, lcget b, lmdutygetalive c ,ljspayperson d "
                       + " where a.conttype = '1' "
                       + " and a.appflag = '1' "
                       + " and a.stateflag in ('1', '3') "
                       + " and a.polno = b.polno "
                       + " and b.getdutycode = c.getdutycode "
                       + " and c.getdutykind = '0' "
                       + " and a.contno = d.contno and b.contno = d.contno "
                       + " and a.polno = d.polno "
                       + " and a.enddate >= '2008-03-01' "
                       + " and a.enddate <= '" + PubFun.getCurrentDate() + "' "
                       +" and not exists(select 1 from ljsgetdraw where a.contno = contno and feefinatype !='YEI') "
                       +" and exists (select 1 from ljspay where a.contno = otherno ) "
                       + " and a.riskcode in ('320106', '120706') "
//                       +" and a.contno ='002182296000001' "
                       + " and a.managecom like '86%' with ur "
                       ;

          SSRS tSSRS = new ExeSQL().execSQL(sql);

          for (int i = 1; i <= tSSRS.getMaxRow(); i++) {
              String getNoticeNo = tSSRS.GetText(i, 8);
              try {
                  LJSPayDB tLJSPayDB = new LJSPayDB();
                  tLJSPayDB.setGetNoticeNo(getNoticeNo);
                  if (!tLJSPayDB.getInfo()) {
                      return false;
                  }
                  //删除暂收，避免出错
                  VData tVData = new VData();
                  String delTempFee = "delete from ljtempfee where tempfeeno ='" +getNoticeNo + "'";
                  String delTempFeeClass ="delete from ljtempfeeclass where tempfeeno ='" +getNoticeNo + "'";

                  //生成客户回复意见。
                  EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(getNoticeNo, "XB");
                  tEdorItemSpecialData.add("BackDate",PubFun.getCurrentDate());
                  tEdorItemSpecialData.add("dealType", "2");//满期终止

                  if (!tEdorItemSpecialData.insert()) {
                      return false;
                  }

                  //撤消应收
                  MMap tMMap = new MMap();
                  tMMap.put(delTempFee, "DELETE");
                  tMMap.put(delTempFeeClass, "DELETE");
                  tVData.add(tMMap);
                  PubSubmit tPubSubmit = new PubSubmit();
                  tPubSubmit.submitData(tVData, "");
                  TransferData tTransferData = new TransferData();
                  tTransferData.setNameAndValue("CancelMode", "3"); //撤销
                  tTransferData.setNameAndValue("IsRepeal", "Y");

                  tVData.clear();
                  tVData.addElement(tLJSPayDB.getSchema());
                  tVData.addElement(mG);
                  tVData.addElement(tTransferData);

                  IndiLJSCancelBL tIndiLJSCancelBL = new IndiLJSCancelBL();

                  if (!tIndiLJSCancelBL.submitData(tVData, "INSERT")) {
                      mErrors.addOneError("少儿险满期处理失败");
                      return false;
                  }

                  String tContNo = tSSRS.GetText(i, 1);
                  String cvalidate = tSSRS.GetText(i, 2);
                  String enddate = tSSRS.GetText(i, 3);
                  String managecom = tSSRS.GetText(i, 4);

                  String SQL = "select distinct a.contno,a.cvalidate,a.enddate,a.managecom,a.agentgroup,a.agentcode,a.enddate,getnoticeno "
                       + " from lcpol a, lcget b, lmdutygetalive c ,ljspaypersonb d "
                       + " where a.conttype = '1' "
                       + " and a.appflag = '1' "
                       + " and a.stateflag in ('1', '3') "
                       + " and a.polno = b.polno "
                       + " and b.getdutycode = c.getdutycode "
                       + " and c.getdutykind = '0' "
                       + " and a.contno = d.contno and b.contno = d.contno "
                       + " and a.polno = d.polno "
                       + " and a.enddate >= '2008-03-01' "
                       + " and a.enddate <= '" + PubFun.getCurrentDate() + "' "
                       +" and not exists(select 1 from ljsgetdraw where a.contno = contno and feefinatype !='YEI' ) "
//                       +" and exists (select 1 from ljspay where a.contno = otherno ) "
                       +" and d.dealstate ='3' "
                       +" and a.riskcode in ('320106', '120706') "
                       +" and a.ContNo='"+tContNo+"'"
                       + " and a.managecom like '86%' with ur "
                       ;

                  tTransferData.setNameAndValue("ContNo", tContNo);
                  tTransferData.setNameAndValue("dealType", "2");
                  tTransferData.setNameAndValue("", cvalidate);
                  tTransferData.setNameAndValue("EndDate", enddate);
                  tTransferData.setNameAndValue("ManageCom", managecom);
                  tTransferData.setNameAndValue("PayMode", "Q");
                  tTransferData.setNameAndValue("StartDate", enddate);
                  tTransferData.setNameAndValue("QuerySql", SQL);
                  tTransferData.setNameAndValue("QueryType","3");

                  tVData.add(tTransferData);
                  tVData.add(mG);
                  ExpirBenefitBatchUI tExpirBenefitBatchUI = new ExpirBenefitBatchUI();
                  tExpirBenefitBatchUI.submitData(tVData, "");
                  if (tExpirBenefitBatchUI.mErrors.needDealError()) {
                      this.mErrors.addOneError("少儿险处理失败");
                      continue;
                  } else {
                      System.out.println("少儿险满期终止成功！");
                  }
              } catch (Exception e) {
                  System.out.println(e.getMessage());
              }
          }
          return true ;
      }

}
