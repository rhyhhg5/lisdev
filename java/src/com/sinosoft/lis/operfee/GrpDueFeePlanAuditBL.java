package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bq.CommonBL;
import java.util.*;
import java.lang.String;
import com.sinosoft.lis.bq.AppAcc;

/**
 * <p>
 * Title: Web业务系统
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: Sinosoft
 * </p>
 * 
 * @author HZM
 * @version 1.0
 */

public class GrpDueFeePlanAuditBL {
	// 错误处理类，每个需要错误处理的类中都放置该类
	public CErrors mErrors = new CErrors();

	/** 往后面传输数据的容器 */
	private VData mInputData = new VData();

	private GlobalInput tGI = new GlobalInput();

	/** 数据操作字符串 */
	private String mOperate = "";

	private String CurrentDate = PubFun.getCurrentDate();

	private String CurrentTime = PubFun.getCurrentTime();
	
	private Reflections mRef = new Reflections();

	private String mGrpContNo = ""; // 团体保单号

	private String mPrtNo = ""; // 印刷号

	private String mPassFlag = ""; // 团体投保单号

	private String mUWIdea = ""; // 缴费代码
	
	private String mFlag = "";//是否审批标记 Y为需要审批
	
	double totalPay = 0;//一共收费

	private String tNo = ""; //缴费通知书号
	
	private String prtSeqNo = "";
	
	private TransferData mTransferData = new TransferData();

	/** 数据表 保存数据 */
	// 集体保单表
	private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();

	// 集体险种保单表
	private LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet();

	private LCGrpPayPlanSet mLCGrpPayPlanSet = new LCGrpPayPlanSet();
	
	private LCGrpPayPlanDetailSet mLCGrpPayPlanDetailSet = new LCGrpPayPlanDetailSet(); 

	private LCGrpPayActuSet mLCGrpPayActuSet = new LCGrpPayActuSet();
	
	private LCGrpPayActuDetailSet mLCGrpPayActuDetailSet = new LCGrpPayActuDetailSet();
	// 集体险种保单表,接受页面传入的参数
	private LCGrpPayActuSet tLCGrpPayActuSet = new LCGrpPayActuSet();

	// 应收总表
	private LJSPaySchema mLJSPaySchema = new LJSPaySchema();

	private LPEdorEspecialDataSet mLPEdorEspecialDataSet = new LPEdorEspecialDataSet();

	// 应收总表集
	private LJSPayGrpSet mLJSPayGrpSet = new LJSPayGrpSet();

	private LJSPayGrpSet mYelLJSPayGrpSet = new LJSPayGrpSet();

	// 应收总表集备份表
	private LJSPayGrpBSet mLJSPayGrpBSet = new LJSPayGrpBSet();

	private LJSPayGrpBSet mYelLJSPayGrpBSet = new LJSPayGrpBSet();

	// 应收总表交费日期
	private String strNewLJSPayDate;

	// 应收总表最早交费日期
	private String strNewLJSStartDate;
	
	// 团单生效日
	private String mLastPayToDate="";

	// 团单交至日
	private String mCurPayToDate="";

	// 应收总表费额
	private double totalsumPay = 0;

	private LCUrgeVerifyLogSchema mLCUrgeVerifyLogSchema = new LCUrgeVerifyLogSchema();

	// 缴费通知书
	private LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();

	private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();

	private LOPRTManagerSubSet mLOPRTManagerSubSet = new LOPRTManagerSubSet();

	private MMap map = new MMap();
	
	private MMap mapAcc = new MMap();

	private String mOpertor = "INSERT";

	private String mOperateFlag = "1"; // 1 : 个案催收，2：批量催收

	private String mserNo = ""; // 批次号

	public GrpDueFeePlanAuditBL() {
	}

	public static void main(String[] args) {

		GlobalInput tGt = new GlobalInput();
		tGt.ComCode = "86";
		tGt.Operator = "wuser";
		tGt.ManageCom = "86";

		LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
		tLCGrpContSchema.setGrpContNo("0000019706");

		TransferData tempTransferData = new TransferData();
		tempTransferData.setNameAndValue("StartDate", "2005-01-01");
		tempTransferData.setNameAndValue("EndDate", "2008-8-13");
		tempTransferData.setNameAndValue("ManageCom", "86");
		tempTransferData.setNameAndValue("LoadFlag", "WMD");

		VData tVData = new VData();
		tVData.add(tLCGrpContSchema);
		tVData.add(tGt);
		tVData.add(tempTransferData);

		// LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
		// tLCGrpPolSchema.setGrpPolNo("86110020040220000102");
		// VData tv = new VData();
		// tv.add(tLCGrpPolSchema);
		// tv.add(tGt);

		GrpDueFeePlanBL GrpDueFeeBL1 = new GrpDueFeePlanBL();
		GrpDueFeeBL1.submitData(tVData, "INSERT");
	}

	/**
	 * 从输入数据中得到所有对象 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData mInputData) {

		tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
		
		mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0);
		if (tGI == null || tLCGrpPayActuSet == null || mTransferData == null) {
			// @@错误处理
			CError tError = new CError();
			tError.moduleName = "GrpDueFeeBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "没有得到足够的数据，请您确认!";
			this.mErrors.addOneError(tError);
			return false;
		}
		mPrtNo = (String) mTransferData.getValueByName("PrtNo");
		mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
		mPassFlag = (String) mTransferData.getValueByName("PassFlag");
		mUWIdea = (String) mTransferData.getValueByName("UWIdea");
		tNo = (String) mTransferData.getValueByName("GetNoticeNo");
		
		if (mPrtNo == null || mPrtNo.equals("")) {
			// @@错误处理
			System.out.println("GrpDueFeePlanBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "GrpDueFeePlanBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入印刷号为空!";
			mErrors.addOneError(tError);
			return false;
		}
		if (mGrpContNo == null || mGrpContNo.equals("")) {
			// @@错误处理
			System.out.println("GrpDueFeePlanBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "GrpDueFeePlanBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入保单号为空!";
			mErrors.addOneError(tError);
			return false;
		}
		if (mPassFlag == null || mPassFlag.equals("")) {
			// @@错误处理
			System.out.println("GrpDueFeePlanBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "GrpDueFeePlanBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入投保号为空!";
			mErrors.addOneError(tError);
			return false;
		}
		
		if (tNo == null || tNo.equals("")) {
			// @@错误处理
			System.out.println("GrpDueFeePlanBL+getInputData++--");
			CError tError = new CError();
			tError.moduleName = "GrpDueFeePlanBL";
			tError.functionName = "getInputData";
			tError.errorMessage = "传入约定缴费代码号为空!";
			mErrors.addOneError(tError);
			return false;
		}
		return true;
	}

	/**
	 * 按照GrpContNo查询是否有保全案件正在处理
	 * 
	 * @param aPolNo
	 *            String
	 * @return boolean
	 */
	public boolean checkEdorItemState(String aGrpContNo) {
		String sql = "select a.* " + "from LPGrpEdorItem a, LPEdorApp b " + "where a.EdorAcceptNo = b.EdorAcceptNo " + "   and a.grpcontno = '" + aGrpContNo + "' " + "   and b.EdorState != '0' ";
		System.out.println(sql);
		LPEdorItemSet set = new LPEdorItemDB().executeQuery(sql);
		if (set.size() > 0) {
			String edorInfo = "";
			for (int i = 1; i <= set.size(); i++) {
				edorInfo += set.get(i).getEdorNo() + " " + set.get(i).getEdorType() + ", ";
			}
			mErrors.addOneError("保单有未结案的保全项目:" + edorInfo);
			return false;
		}
		return true;
	}

	/**
	 * 按照GrpContNo查询是否有理赔案件正在处理
	 * 
	 * @param aPolNo
	 *            String
	 * @return boolean
	 */
	public boolean checkClaimState(String aGrpContNo) {
		boolean tClaimFlag = true;
		String tSQL = "SELECT 1 FROM llcasepolicy a,llcase b WHERE a.caseno=b.caseno " + "AND b.rgtstate NOT IN ('11','12','14')" // 11
				// 通知状态
				// 12
				// 给付状态
				// 14
				// 撤件状态
				+ " AND a.grpcontno='" + aGrpContNo + "'";
		ExeSQL exesql = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = exesql.execSQL(tSQL);
		if (tSSRS.getMaxRow() > 0) {
			tClaimFlag = false;
		}
		return tClaimFlag;
	}

	/**
	 * 按照GrpContNo查询是否已经被催收
	 * 
	 * @param aPolNo
	 *            String
	 * @return boolean
	 */
	public boolean checkLjsState(String aGrpContNo) {
		boolean tClaimFlag = true;
		String tSQL = "SELECT 1 FROM ljspayb a WHERE   a.dealstate IN ('0','4','5')" + " AND a.otherno='" + aGrpContNo + "' and a.othernotype='1' ";
		ExeSQL exesql = new ExeSQL();
		SSRS tSSRS = new SSRS();
		tSSRS = exesql.execSQL(tSQL);
		if (tSSRS.getMaxRow() > 0) {
			tClaimFlag = false;
		}
		return tClaimFlag;
	}

	/**
	 * 杨红于2005-07-19添加数据校验，判断页面中查询出的信息是否已被催收
	 * 
	 * @return boolean
	 */
	public boolean checkData() {
		return true;
	}

	public double getCalPrem(LCGrpPayPlanDetailSchema tLCGrpPayPlanDetailSchema ,double actu){
		String allprem= new ExeSQL().getOneValue("select prem from LCGrpPayPlan where prtno='"+mPrtNo+"' and plancode='"+tLCGrpPayPlanDetailSchema.getPlanCode()+"' and contplancode='"+tLCGrpPayPlanDetailSchema.getContPlanCode()+"'");
		double tallprem=Double.parseDouble(allprem);
		if(tallprem==0){
			return 0;
		}
		else
		return Arith.round(actu*tLCGrpPayPlanDetailSchema.getPrem()/Double.parseDouble(allprem),2);
	}
	
	
	// 为更新数据库准备数据
	public boolean prepareData() {

//		map.put(mLCGrpPayActuSet, "INSERT");
//		map.put(mLCGrpPayActuDetailSet, "INSERT");
//		map.put(mLJSPayGrpSet, "INSERT");
//		map.put(mLJSPaySchema, "INSERT");
//		map.put(mLJSPayGrpBSet, "INSERT");
//		map.add(mapAcc);
		map.put(mLPEdorEspecialDataSet, "INSERT");
//		map.put(mLOPRTManagerSubSet, "INSERT");
//		map.put(mLOPRTManagerSchema, "INSERT");
		if("01".equals(mPassFlag))
			updateDealState();
		mInputData.add(map);
		return true;
	}

	// 传输数据的公共方法
	public boolean submitData(VData cInputData, String cOperate) {

		System.out.println("GrpDueFeeBL Begin......");
		this.mOperate = cOperate;

		if (!getInputData(cInputData)) {
			return false;
		}
		if (!checkData()) {
			return false;
		}
	

		// 进行业务处理
		if (!dealData()) {
			return false;
		}
		if (!prepareData()) {
			return false;
		}
		PubSubmit tPubSubmit = new PubSubmit();
		if (tPubSubmit.submitData(mInputData, "") == false) {
			// 添加数据到催收核销日志表
			

			this.mErrors.addOneError("PubSubmit:处理数据库失败!");

			return false;
		}
		

		return true;
	}
	


	/**
	 * Yangh于2005-07-19创建新的dealData的处理逻辑
	 * 
	 * @return boolean
	 */
	private boolean dealData() {

		System.out.println("GrpDueFeeBL dealData Begin");
		System.out.println("约定缴费续期团险抽档开始:" + mGrpContNo);
       
		LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
		tLPEdorEspecialDataSchema.setEdorAcceptNo(tNo); // 通知书号
		tLPEdorEspecialDataSchema.setEdorNo(mGrpContNo);
		tLPEdorEspecialDataSchema.setEdorType("00");
		tLPEdorEspecialDataSchema.setDetailType("AuditResult");
		tLPEdorEspecialDataSchema.setState(mPassFlag);
		tLPEdorEspecialDataSchema.setEdorValue(mUWIdea);
		tLPEdorEspecialDataSchema.setPolNo(mGrpContNo);
		
		mLPEdorEspecialDataSet.add(tLPEdorEspecialDataSchema);
		
		LPEdorEspecialDataSchema ttLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
		ttLPEdorEspecialDataSchema.setEdorAcceptNo(tNo); // 通知书号
		ttLPEdorEspecialDataSchema.setEdorNo(mGrpContNo);
		ttLPEdorEspecialDataSchema.setEdorType("00");
		ttLPEdorEspecialDataSchema.setDetailType("AuditDate");
		ttLPEdorEspecialDataSchema.setState(mPassFlag);
		ttLPEdorEspecialDataSchema.setEdorValue(CurrentDate);
		ttLPEdorEspecialDataSchema.setPolNo(mGrpContNo);
		
		mLPEdorEspecialDataSet.add(ttLPEdorEspecialDataSchema);

		

		return true;
	}
	

	// 20081204 zhanggm 同步LJSPayGrpB表的DealState状态。
	// 当保费从LCAppAcc表抵充、SumActuPayMoney==0时,DealState应该为4而不是0,同LJSPayB是一样的
	private void updateDealState() {
		String sql = "update LJSPay set cansendbank = null " + " where GetNoticeNo = '" + tNo + "' and cansendbank='Y'";
		System.out.println(sql);
		map.put(sql, "UPDATE");
	}


}
