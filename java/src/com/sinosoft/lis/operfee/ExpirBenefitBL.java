package com.sinosoft.lis.operfee;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.util.*;
import com.sinosoft.lis.bq.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author qulq
 * @version 1.0
 */
public class ExpirBenefitBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData saveData = new VData();
    private MMap mMMap = new MMap();
    private GlobalInput tGI = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    /** 数据操作字符串 */
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String mserNo = "1"; //批次号
    private String mSerGrpNo = ""; //接受外部参数

    private String mGetNoticeNo = null;  //通知书号

    private String payMode = "Q"; //判断前台点击的是现金给付，还是普通给付。
    //保单缴费方式
    private String mBankCode = "";
    private String mBankAccNo = "";
    private String mAccName = "";

    private String StartDate = ""; //应付开始时间
    private String EndDate = ""; //应付结束时间

    private Reflections ref = new Reflections();

    //个人保单表
    private LCContSchema mLCContSchema = new LCContSchema();

    //应付个人交费表
    private	LJSGetSchema tLJSGetSchema = new LJSGetSchema();
    //应付个人明细表
    private LJSGetDrawSet tLJSGetDrawSet = new LJSGetDrawSet();
    //实付表
    private LJAGetSchema mLJAGetSchema = new LJAGetSchema();

    private String mPrtSeqNo = null;


    public ExpirBenefitBL() {
    }

    public static void main(String[] args) {

    }

    /**
     * 校验是否有理赔，保全
     * @return boolean：通过true，否则false
     */
    public boolean checkData() {
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setGrpContNo(mLCContSchema.getGrpContNo());
        tLCPolSchema.setGrpPolNo("00000000000000000000");
        tLCPolSchema.setContNo(mLCContSchema.getContNo());
        tLCPolSchema.setPolNo("000000");
        //校验保全项目
        String sql = "select a.edorNo "
                     + "from LPEdorItem a, LPEdorApp b "
                     + "where a.edorAcceptNo = b.edorAcceptNo "
                     + "   and a.ContNo = '" + mLCContSchema.getContNo() + "' "
                     + "   and edorType in ('CT','XT','BF','CC','WT') "
                     + "   and b.edorState != '" + BQ.EDORSTATE_CONFIRM
                     + "' ";
        String edorNo = new ExeSQL().getOneValue(sql);
        if (!edorNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitBL";
            tError.functionName = "checkData";
            tError.errorMessage = "保单"+mLCContSchema.getContNo()+"正在做保全业务：" + edorNo + "，不能给付抽档";
            mErrors.addOneError(tError);
            dealErrorLog(tLCPolSchema,tError.errorMessage);
            return false;
        }
        //校验是否正在理赔
        sql = "select 1 from llcase where customerno in (select insuredno from lcinsured where contno ='" +
              mLCContSchema.getContNo() + "')"
              + " and endcasedate is null and rgtstate<>'14' "
              ;
        String rgtNo = new ExeSQL().getOneValue(sql);
        if (rgtNo != null && !rgtNo.equals("")) {
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitBL";
            tError.functionName = "checkData";
            tError.errorMessage = "保单"+mLCContSchema.getContNo()+"有未结案的理陪，不能给付抽档";
            mErrors.addOneError(tError);
            dealErrorLog(tLCPolSchema,tError.errorMessage);
            return false;
        }
        return true;
    }
    /**
     * 加入到错误日志表日志表数据,记录因为各种原因抽档失败的单子
     * @param
     * @return boolean
     */
    private boolean dealErrorLog(LCPolSchema tLCPolSchema, String Error) {
           LGErrorLogSchema tLGErrorLogSchema = new LGErrorLogSchema();
           tLGErrorLogSchema.setSerialNo(mserNo);
           tLGErrorLogSchema.setErrorType("0002");
           tLGErrorLogSchema.setErrorTypeSub("01");
           tLGErrorLogSchema.setGrpContNo(tLCPolSchema.getGrpContNo());
           tLGErrorLogSchema.setGrpPolNo(tLCPolSchema.getGrpPolNo());
           tLGErrorLogSchema.setContNo(tLCPolSchema.getContNo());
           tLGErrorLogSchema.setPolNo(tLCPolSchema.getPolNo());
           tLGErrorLogSchema.setOperator(tGI.Operator);
           tLGErrorLogSchema.setMakeDate(CurrentDate);
           tLGErrorLogSchema.setMakeTime(CurrentTime);
           tLGErrorLogSchema.setModifyDate(CurrentDate);
           tLGErrorLogSchema.setModifyTime(CurrentTime);
           tLGErrorLogSchema.setDescribe(Error);

           MMap tMap = new MMap();
           tMap.put(tLGErrorLogSchema, "DELETE&INSERT");
           VData tInputData = new VData();
           tInputData.add(tMap);
           PubSubmit tPubSubmit = new PubSubmit();
           if (tPubSubmit.submitData(tInputData, "") == false) {
               this.mErrors.addOneError("PubSubmit:处理LGErrorLog 表失败!");
               return false;
           }
           return true;
    }
    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public VData getSubmitData(VData cInputData, String cOperate) {

        if (!getInputData(cInputData)) {
            return null;
        }
        System.out.println("After getinputdata");
        if (!checkData()) {
            return null;
        }
        //进行业务处理
        if (!dealData()) {
            return null;
        }

        return saveData;
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public MMap getSubmitMMap(VData cInputData, String cOperate) {
        getSubmitData(cInputData, cOperate);
        return mMMap;
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public boolean submitData(VData cInputData, String cOperate) {

        VData data = getSubmitData(cInputData, cOperate);
        if (data == null) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(data, "") == false) {
            this.mErrors.addOneError("PubSubmit:生成给付失败!");
            return false;
        }
        return true;
    }

    public LCGetSet getLCGetNeedGet(String startDate,
                                           String endDate,
                                           String contNo,String insuredNo)
    {
        StringBuffer tSBql = new StringBuffer(128);

        tSBql.append("select b.* from lcpol a,lcget b , lmdutygetalive c ")
                .append(" where a.conttype ='1' and a.appflag = '1' and a.stateflag in ('1','3')")
                .append(" and a.polno = b.polno and b.getdutycode = c.getdutycode")
                .append(" and a.riskcode  in ('320106','120706') ")
                .append(" and c.getdutykind = '0' and a.enddate >='"+startDate+"' and a.enddate <='"+endDate)
                .append("' and a.paytodate = a.payenddate and ( not exists ")
                .append(" (select 1 from ljsget,ljsgetdraw where ljsget.getnoticeno = ljsgetdraw.getnoticeno ")
                .append(" and ljsget.paymode !='5' and  ljsgetdraw.polno = a.polno)")
                .append(" or exists (select  1 from ljsgetdraw  where ljsgetdraw.polno = a.polno  and feefinatype = 'TF' having sum(getmoney)=0) ) ")
                .append(" and a.ContNo='" + contNo + "' and a.insuredno ='"+insuredNo+"'")
                .append(" union ")
                .append(" select b.* from lcpol a,lcget b , lmdutygetalive c ")
                .append(" where a.conttype ='1' and a.appflag = '1' and a.stateflag in ('1','3')")
                .append(" and a.polno = b.polno and b.getdutycode = c.getdutycode and c.getdutykind = '1'")
                .append(" and a.riskcode  in ('320106','120706') ")
                //可以处理一年领多次的情况
                .append(" and ( case when c.getintv is null or c.getintv = 12 then ")
                .append(" (a.cvalidate +(year('"+endDate+"')-year(a.cvalidate)) year )")
//                .append(" when c.getintv != 0 then (a.cvalidate + (month('"+endDate+"'-a.cvalidate)/int(c.getintv)) month )")
//                .append(" end between '"+startDate+"' and '"+endDate+"' ) and ( case c.STARTDATECALMODE when '0' then ")
                .append(" end between '"+startDate+"' and '"+endDate+"' )")
//                .append(" case c.STARTDATECALREF when 'S' then case c.GETSTARTUNIT when 'Y' then year('"+startDate+"'-a.cvalidate)")
//                .append(" when 'M' then month('"+startDate+"'-a.cvalidate) when 'D' then day('"+startDate+"'-a.cvalidate) when 'A' then ")
//                .append(" year('"+startDate+"'-a.cvalidate) end when 'B' then case c.GETSTARTUNIT when 'Y' then ")
//                .append(" year('"+startDate+"'-a.insuredbirthday) when 'M' then month('"+startDate+"'-a.insuredbirthday) ")
//                .append(" when 'D' then day('"+startDate+"'-a.insuredbirthday) when 'A' then year('"+startDate+"'-a.insuredbirthday) ")
//                .append(" end end end >c.GETSTARTPERIOD ) and not exists ( ")
                .append(" and not exists ( ")
                .append(" select 1 from ljsget,ljsgetdraw where ljsget.getnoticeno = ljsgetdraw.getnoticeno ")
                .append(" and ljsget.dealstate <> '2' and  ljsgetdraw.polno = a.polno and ljsgetdraw.curgettodate between '"+startDate+"' and '"+endDate+"')")
                .append(" and a.ContNo='" + contNo + "' and a.insuredno ='"+insuredNo+"'")
                ;

        LCGetSet tLCGetSet = new LCGetSet();
        LCGetDB tLCGetDB = new LCGetDB();
        tLCGetSet = tLCGetDB.executeQuery(tSBql.toString());
        System.out.println(tSBql.toString());

        if (tLCGetDB.mErrors.needDealError() == true) {
                // @@错误处理
                this.mErrors.copyAllErrors(tLCGetDB.mErrors);

                CError tError = new CError();
                tError.moduleName = "ExpirBenefitBL";
                tError.functionName = "queryData";
                tError.errorMessage = "lcpol表查询失败!";
                this.mErrors.addOneError(tError);
                return null;
            }
        if (tLCGetSet.size() == 0) {
            CError.buildErr(this,"保单号为：" + contNo+ "的保单已被其他业务员抽档。");
            return null;
        }

        return tLCGetSet;
    }

    /**
     * 生成给付记录
     * @return boolean：成功true，否则false
     */
    private boolean dealData()
    {
//20090306 zhanggm 防止并发，加锁
    	MMap tCekMap = null;
    	tCekMap = lockLJAGet(mLCContSchema);
        if (tCekMap == null)
        {
            return false;
        }
        mMMap.add(tCekMap);
//--------------------------
        Calculator tCalculator = new Calculator();
        String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
        String serMultNo = (String) mTransferData.getValueByName("serNo");
        mSerGrpNo = serMultNo;

        if (serMultNo == null || serMultNo.equals("")) {
            //生成批次号
            mserNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        } else {
            mserNo = serMultNo;
        }

        //给付任务编号
        String taskno = " select max(otherno) from ljsget where getnoticeno in (select getnoticeno from "
                        + " ljsgetdraw where contno ='" + mLCContSchema.getContNo() +
                        "')"
                        ;
        SSRS noSSRS = new ExeSQL().execSQL(taskno);
        int no = 0;
        if (noSSRS.getMaxRow() > 0&&!(noSSRS.GetText(1, 1).equals("")||noSSRS.GetText(1, 1).equals("null"))) {
            String noString = noSSRS.GetText(1, 1);
//            no = Integer.parseInt(noString.substring(noString.length()-1));//modify by fuxin 2008-5-5 取最后一位。
            no = Integer.parseInt(noString.substring(noString.length()-2));//modify by hehongwei 2015-7-1 取最后两位
        }

        //得到被保人需要给付的险种
        String startDate = (String) mTransferData.getValueByName("StartDate");
        String endDate = (String) mTransferData.getValueByName("EndDate");
        //取得被保人信息
        String sql = "select distinct insuredno from lcget a,lmdutygetalive b "
                     +" where a.getdutycode = b.getdutycode and contno ='"
                     +mLCContSchema.getContNo()+"'"
        					;
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        HashMap t = new HashMap();
        for(int i=1;i<=tSSRS.getMaxRow();i++)
        {

                //给付任务号
                no++;
                String noString = String.valueOf(no);
                String taskNo = "P" + mLCContSchema.getContNo();
                for (int k = noString.length(); k < 4; k++) {
                    taskNo += "0";
                }
                taskNo+=noString;
                LCGetSet tLCGetSet = getLCGetNeedGet(startDate, endDate,mLCContSchema.getContNo(),tSSRS.GetText(i,1));

            if(tLCGetSet==null)
            {
                continue;
            }
            //要素准备，被保人年龄
            String age ="select year(current date - birthday) from lcinsured where contno ='"
                        +mLCContSchema.getContNo()+"' and insuredno='"+tSSRS.GetText(i,1)+"'";

            tCalculator.addBasicFactor("AppntAge",new ExeSQL().getOneValue(age));

            //给付通知书号码
            if(!createNo())
            {
                return false;
            }
            double sumGetMoney = 0; //总领取金额
            //循环每个给付责任进行处理
            tLJSGetDrawSet.clear();
            for(int j = 1; j <= tLCGetSet.size(); j++)
            {

                //获取合同下的每条给付的明细信息
                LCGetSchema tLCGetSchema = tLCGetSet.get(j).getSchema();

                LMDutyGetAliveDB tLMDutyGetaLiveDB = new LMDutyGetAliveDB();

                tLMDutyGetaLiveDB.setGetDutyCode(tLCGetSchema.getGetDutyCode());
                LMDutyGetAliveSet tLMDutyGetAliveSet=tLMDutyGetaLiveDB.query();
                LMDutyGetAliveSchema tLMDutyGetAliveSchema= tLMDutyGetAliveSet.get(1);
                String pCalCode ="";
                if(tLMDutyGetAliveSchema.getCalCode()!=null&&!"".equals(tLMDutyGetAliveSchema.getCalCode()))
                {
                	pCalCode = tLMDutyGetAliveSchema.getCalCode();
                }
                if(tLMDutyGetAliveSchema.getCnterCalCode()!=null&&!"".equals(tLMDutyGetAliveSchema.getCnterCalCode()))
                {
                	pCalCode = tLMDutyGetAliveSchema.getCnterCalCode();
                }
                if(tLMDutyGetAliveSchema.getOthCalCode()!=null&&!"".equals(tLMDutyGetAliveSchema.getOthCalCode()))
                {
                	pCalCode = tLMDutyGetAliveSchema.getOthCalCode();
                }

                tCalculator.setCalCode(pCalCode);
            //责任保额
            String amnt = "select sum(amnt) from lcduty where polno='"
                          +tLCGetSchema.getPolNo()+"' and dutycode='"
                          +tLCGetSchema.getDutyCode()+"'";
            tCalculator.addBasicFactor("Amnt", new ExeSQL().getOneValue(amnt));
            tCalculator.addBasicFactor("Get",CommonBL.bigDoubleToCommonString(tLCGetSchema.getActuGet(),"0.00"));
            tCalculator.addBasicFactor("PolNo",tLCGetSchema.getPolNo());

            String tStr = tCalculator.calculate();
            double tValue = 0;

            if (tStr == null || tStr.trim().equals(""))
            {
                tValue = 0;
            }
            else
            {
                tValue = Double.parseDouble(tStr);
            }
            sumGetMoney +=tValue;

            LJSGetDrawSchema tLJSGetDrawSchema=new LJSGetDrawSchema();
            tLJSGetDrawSchema.setGetNoticeNo(this.mGetNoticeNo);
            tLJSGetDrawSchema.setPolNo(tLCGetSchema.getPolNo());
            tLJSGetDrawSchema.setDutyCode(tLCGetSchema.getDutyCode());
            tLJSGetDrawSchema.setGetDutyKind(tLCGetSchema.getGetDutyKind());
            tLJSGetDrawSchema.setGetDutyCode(tLCGetSchema.getGetDutyCode());
            tLJSGetDrawSchema.setContNo(tLCGetSchema.getContNo());
            tLJSGetDrawSchema.setAppntNo(mLCContSchema.getAppntNo());
            tLJSGetDrawSchema.setInsuredNo(tSSRS.GetText(i,1));
            tLJSGetDrawSchema.setGetDate(CurrentDate);
            tLJSGetDrawSchema.setGetMoney(tValue);
            tLJSGetDrawSchema.setEnterAccDate(CurrentDate);
            tLJSGetDrawSchema.setFeeOperationType("EB");
            tLJSGetDrawSchema.setFeeFinaType("TF");
            tLJSGetDrawSchema.setManageCom(mLCContSchema.getManageCom());
            tLJSGetDrawSchema.setAgentCom(mLCContSchema.getAgentCom());
            tLJSGetDrawSchema.setAgentType(mLCContSchema.getAgentType());
            tLJSGetDrawSchema.setAgentCode(mLCContSchema.getAgentCode());
            tLJSGetDrawSchema.setAgentGroup(mLCContSchema.getAgentGroup());
            LCPolDB tLCPolDB = new LCPolDB();
            tLCPolDB.setPolNo(tLCGetSchema.getPolNo());
            tLCPolDB.getInfo();
            tLJSGetDrawSchema.setRiskCode(tLCPolDB.getRiskCode());
            if(tLMDutyGetAliveSchema.getGetDutyKind().equals("0"))
            {
                tLJSGetDrawSchema.setLastGettoDate(tLCPolDB.getEndDate());
                tLJSGetDrawSchema.setCurGetToDate(tLCPolDB.getEndDate());
            }else if(tLMDutyGetAliveSchema.getGetDutyKind().equals("1"))
            {
                LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
                String query = " Select max(CurGetToDate) from ljsgetdraw a,ljsget b where "
                               +" a.getnoticeno = b.getnoticeno and b.dealstate <>'2' and a.polno='"
                               +tLCGetSchema.getPolNo()+"' and getdutycode ='"
                               +tLCGetSchema.getGetDutyCode()+"' and insuredno ='"
                               +tLCGetSchema.getInsuredNo()+"'";
                SSRS getToDateSSRS = new ExeSQL().execSQL(query);
                if((!getToDateSSRS.GetText(1,1).equals("") && getToDateSSRS.GetText(1,1) != null) && getToDateSSRS.getMaxRow()>0)
                {
                    tLJSGetDrawSchema.setLastGettoDate(getToDateSSRS.GetText(1,1));
                    if(tLMDutyGetAliveSchema.getGetIntv()==0)
                    {
                        tLJSGetDrawSchema.setCurGetToDate(PubFun.calDate(getToDateSSRS.GetText(1,1),1,"M",null));
                    }else
                    {
                        tLJSGetDrawSchema.setCurGetToDate(PubFun.calDate(getToDateSSRS.GetText(1,1),tLMDutyGetAliveSchema.getGetIntv(),"M",null));
                    }
                }else //第一次给付
                {
                    if(tLMDutyGetAliveSchema.getStartDateCalRef().equals("S"))
                    {
                        if(tLMDutyGetAliveSchema.getGetStartUnit().equals("A"))
                        {
                            tLJSGetDrawSchema.setLastGettoDate(PubFun.calDate(
                                    tLCPolDB.getInsuredBirthday(),tLMDutyGetAliveSchema.getGetStartPeriod(), "Y", null));
                            tLJSGetDrawSchema.setCurGetToDate(PubFun.calDate(
                                    tLCPolDB.getInsuredBirthday(),tLMDutyGetAliveSchema.getGetStartPeriod(), "Y", null));

                        }else
                        {
                            tLJSGetDrawSchema.setLastGettoDate(PubFun.calDate(
                                    tLCPolDB.getCValiDate(),tLMDutyGetAliveSchema.getGetStartPeriod(), tLMDutyGetAliveSchema.getGetStartUnit(), null));
                            tLJSGetDrawSchema.setCurGetToDate(PubFun.calDate(
                                    tLCPolDB.getCValiDate(),tLMDutyGetAliveSchema.getGetStartPeriod(), tLMDutyGetAliveSchema.getGetStartUnit(), null));
                        }

                    }else if(tLMDutyGetAliveSchema.getStartDateCalRef().equals("B"))
                    {
                        if(tLMDutyGetAliveSchema.getGetStartUnit().equals("A"))
                        {
                            tLJSGetDrawSchema.setLastGettoDate(PubFun.calDate(
                                    tLCPolDB.getInsuredBirthday(),
                                    tLMDutyGetAliveSchema.getGetStartPeriod(), "Y", null));
                            tLJSGetDrawSchema.setCurGetToDate(PubFun.calDate(
                                    tLCPolDB.getInsuredBirthday(),
                                    tLMDutyGetAliveSchema.getGetStartPeriod(), "Y", null));

                        } else {
                            tLJSGetDrawSchema.setLastGettoDate(PubFun.calDate(
                                    tLCPolDB.getInsuredBirthday(),
                                    tLMDutyGetAliveSchema.getGetStartPeriod(),
                                    tLMDutyGetAliveSchema.getGetStartUnit(), null));
                            tLJSGetDrawSchema.setCurGetToDate(PubFun.calDate(
                                    tLCPolDB.getInsuredBirthday(),
                                    tLMDutyGetAliveSchema.getGetStartPeriod(),
                                    tLMDutyGetAliveSchema.getGetStartUnit(), null));
                        }
                    }
                }
            }
            tLJSGetDrawSchema.setSerialNo(mserNo);
            tLJSGetDrawSchema.setOperator(tGI.Operator);
            tLJSGetDrawSchema.setMakeDate(CurrentDate);
            tLJSGetDrawSchema.setMakeTime(CurrentTime);
            tLJSGetDrawSchema.setModifyDate(CurrentDate);
            tLJSGetDrawSchema.setModifyTime(CurrentTime);
            this.tLJSGetDrawSet.add(tLJSGetDrawSchema);

            //计算其它奖金
            LDCodeDB tLDCodeDB = new LDCodeDB();
            tLDCodeDB.setCodeType("RiskGetAlive");
            tLDCodeDB.setCode(tLCPolDB.getRiskCode());
            if(tLDCodeDB.getInfo())
            {
                if(!t.containsKey(tLCPolDB.getPolNo())){
                    t.put(tLCPolDB.getPolNo(),"");
                    try {
                        Class tClass = Class.forName(tLDCodeDB.getCodeAlias());
                        CalOther tCalOther = (CalOther) tClass.newInstance();
                        double otherMoney = tCalOther.cal(tLCPolDB.getPolNo());
                        if (tCalOther.getError() == null) {
                             LJSGetDrawSchema ttLJSGetDrawSchema=new LJSGetDrawSchema();
                             ttLJSGetDrawSchema.setSchema(tLJSGetDrawSchema);
                             ttLJSGetDrawSchema.setDutyCode("000000");
                             ttLJSGetDrawSchema.setGetMoney(otherMoney);
                             this.tLJSGetDrawSet.add(ttLJSGetDrawSchema);
                             sumGetMoney +=otherMoney;
                        }
                        else
                        {
                            mErrors.copyAllErrors(tCalOther.getError());
                            return false;
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        CError.buildErr(this, tLCPolDB.getRiskCode() + "奖金给付问题");
                        return false;
                    }
                }
            }
            }
            mMMap.put(tLJSGetDrawSet, "DELETE&INSERT");
            LJSGetSchema tLJSGetSchema = new LJSGetSchema();
            tLJSGetSchema.setGetNoticeNo(mGetNoticeNo);
            tLJSGetSchema.setOtherNo(taskNo);
            tLJSGetSchema.setOtherNoType("20");
            if(this.payMode.equals("1"))
            {
                tLJSGetSchema.setPayMode("1");
            }else
            {
                tLJSGetSchema.setPayMode(mLCContSchema.getPayMode());
                if(mLCContSchema.getPayMode().equals("4"))
                {
                   tLJSGetSchema.setAccName(mLCContSchema.getAccName());
                   tLJSGetSchema.setBankAccNo(mLCContSchema.getBankAccNo());
                   tLJSGetSchema.setBankCode(mLCContSchema.getBankCode());
                }
            }
            tLJSGetSchema.setDrawer(mLCContSchema.getAppntName());
            tLJSGetSchema.setDrawerID(mLCContSchema.getAppntIDNo());
            tLJSGetSchema.setSumGetMoney(sumGetMoney);
            tLJSGetSchema.setGetDate(CurrentDate);
            tLJSGetSchema.setManageCom(mLCContSchema.getManageCom());
            tLJSGetSchema.setAgentCom(mLCContSchema.getAgentCom());
            tLJSGetSchema.setAgentType(mLCContSchema.getAgentType());
            tLJSGetSchema.setAgentCode(mLCContSchema.getAgentCode());
            tLJSGetSchema.setAgentGroup(mLCContSchema.getAgentGroup());
            tLJSGetSchema.setAppntNo(mLCContSchema.getAppntNo());
            tLJSGetSchema.setSerialNo(mserNo);
            tLJSGetSchema.setOperator(tGI.Operator);
            tLJSGetSchema.setMakeDate(CurrentDate);
            tLJSGetSchema.setMakeTime(CurrentTime);
            tLJSGetSchema.setModifyDate(CurrentDate);
            tLJSGetSchema.setModifyTime(CurrentTime);
            tLJSGetSchema.setDealState("0");//待给付
            mMMap.put(tLJSGetSchema, "DELETE&INSERT");
            String updateDate = " update ljsget a set getdate = (select max(CurGetToDate) from ljsgetdraw where getnoticeno = a.getnoticeno ) "
                                +" where a.getnoticeno ='"+mGetNoticeNo+"'"
                                ;
            mMMap.put(updateDate, "UPDATE");
            
            //20100423 zhanggm 给付抽档拆分成抽档和确认两步，抽档先不生成ljaget，在确认步骤生成。
            /*
            LJAGetSchema tLJAGetSchema = new LJAGetSchema();
            tLJAGetSchema.setActuGetNo(tLJSGetSchema.getGetNoticeNo());
            tLJAGetSchema.setGetNoticeNo(tLJSGetSchema.getGetNoticeNo());
            tLJAGetSchema.setOtherNo(tLJSGetSchema.getOtherNo());
            tLJAGetSchema.setOtherNoType(tLJSGetSchema.getOtherNoType());
            tLJAGetSchema.setPayMode(tLJSGetSchema.getPayMode());
            tLJAGetSchema.setManageCom(tLJSGetSchema.getManageCom());
            tLJAGetSchema.setAgentCom(tLJSGetSchema.getAgentCom());
            tLJAGetSchema.setAgentType(tLJSGetSchema.getAgentType());
            tLJAGetSchema.setAgentCode(tLJSGetSchema.getAgentCode());
            tLJAGetSchema.setAgentGroup(tLJSGetSchema.getAgentGroup());
            tLJAGetSchema.setAppntNo(tLJSGetSchema.getAppntNo());
            tLJAGetSchema.setAccName(tLJSGetSchema.getAccName());
            tLJAGetSchema.setSumGetMoney(tLJSGetSchema.getSumGetMoney());
            tLJAGetSchema.setBankAccNo(tLJSGetSchema.getBankAccNo());
            tLJAGetSchema.setBankCode(tLJSGetSchema.getBankCode());
            tLJAGetSchema.setDrawer(tLJSGetSchema.getDrawer());
            tLJAGetSchema.setDrawerID(tLJSGetSchema.getDrawerID());
            tLJAGetSchema.setSerialNo(mserNo);
            tLJAGetSchema.setOperator(tGI.Operator);
            tLJAGetSchema.setMakeDate(CurrentDate);
            tLJAGetSchema.setMakeTime(CurrentTime);
            tLJAGetSchema.setModifyDate(CurrentDate);
            tLJAGetSchema.setModifyTime(CurrentTime);
            mMMap.put(tLJAGetSchema, "DELETE&INSERT");
            String shouldDate = " update ljaget a set ShouldDate = (select max(getdate) from ljsget where getnoticeno = a.getnoticeno ) "
                                +" where a.getnoticeno ='"+mGetNoticeNo+"'"
                                ;
            mMMap.put(shouldDate, "UPDATE");
            */
            String chkSql = "select 1 from LJSGet a where OtherNo = '" + tLJSGetSchema.getOtherNo() + "' "
                          + "and OtherNoType = '" + tLJSGetSchema.getOtherNoType() + "' "
                          + "and (DealState = '0' or DealState is null) "
                          + "and exists(select 1 from LJSGet b where OtherNo = a.OtherNo "
                          + "  and OtherNoType = a.OtherNoType and (DealState = '0' or DealState is null) "
                          + "  and b.GetNoticeNo != a.GetNoticeNo) with ur ";
            System.out.println(chkSql);
            mMMap.put(chkSql, "SELECT"); 
                       
            
        }
        saveData.clear();
        saveData.add(mMMap);
        return true;
    }

    /**
     * 生成相关流水号
     * @return boolean：成功true，否则false
     */
    private boolean createNo()
    {
        String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
        //产生退费通知书号
        if(mGetNoticeNo == null || mGetNoticeNo.equals(""))
        {
            mGetNoticeNo = PubFun1.CreateMaxNo("GETNO", tLimit);
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData)
    {
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mLCContSchema = ((LCContSchema) mInputData.getObjectByObjectName(
            "LCContSchema",
            0));
        mTransferData = (TransferData) mInputData.getObjectByObjectName(
            "TransferData", 0); //传输页面输入的起止时间

        if((tGI == null) || (mLCContSchema == null) || mTransferData == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLCContSchema.getContNo());
        if(tLCContDB.getInfo() == false)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;

        }
        mLCContSchema.setSchema(tLCContDB.getSchema());
        payMode = (String) mTransferData.getValueByName("PayMode");

        return true;
    }

    /**
     * 锁定动作。
     * @param cLCContSchema
     * @return
     */
    private MMap lockLJAGet(LCContSchema cLCContSchema)
    {
        MMap tMMap = null;
        /**满期给付锁定标志为："MJ"*/
        String tLockNoType = "MJ";
        /** 锁定有效时间*/
        String tAIS = "3600";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("LockNoKey", cLCContSchema.getContNo());
        tTransferData.setNameAndValue("LockNoType", tLockNoType);
        tTransferData.setNameAndValue("AvailabilityIntervalSecond", tAIS);

        VData tVData = new VData();
        tVData.add(tGI);
        tVData.add(tTransferData);

        LockTableActionBL tLockTableActionBL = new LockTableActionBL();
        tMMap = tLockTableActionBL.getSubmitMap(tVData, null);
        if (tMMap == null)
        {
            mErrors.copyAllErrors(tLockTableActionBL.mErrors);
            return null;
        }
        return tMMap;
    }
}
