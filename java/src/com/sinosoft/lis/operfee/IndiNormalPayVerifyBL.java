package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.pubfun.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 执行个人正常交费核销事务（续期非催收-无应收表）</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class IndiNormalPayVerifyBL  {
  //错误处理类，每个需要错误处理的类中都放置该类
  public  CErrors mErrors=new CErrors();
  /** 往后面传输数据的容器 */
  private VData mInputData ;
  /** 数据操作字符串 */
  private String mOperate;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private String serNo="";//流水号
  private String tLimit="";
  private String payNo="";//交费收据号
  private boolean dingeFlag=false;//杨红于2005-07-07说明：默认是不定额，如果客户要求是定额的，在这里设置为true,

//保存操作员和管理机构的类
  private GlobalInput mGI = new GlobalInput();
//暂收费表
  private LJTempFeeSchema    pLJTempFeeSchema      = new LJTempFeeSchema();
  private LJTempFeeBL        mLJTempFeeBL          = new LJTempFeeBL();
  private LJTempFeeSet       mLJTempFeeSet         = new LJTempFeeSet();//Yangh于2005-07-07添加该私有变量
  private LJTempFeeSet       mLJTempFeeNewSet         = new LJTempFeeSet();
//暂收费分类表
  private LJTempFeeClassSet  mLJTempFeeClassSet    = new LJTempFeeClassSet();
  private LJTempFeeClassSet  mLJTempFeeClassNewSet = new LJTempFeeClassSet();
//实收个人交费表
  private LJAPayPersonSet    mLJAPayPersonSet      = new LJAPayPersonSet();
//实收总表
  private LJAPayBL           mLJAPayBL             = new LJAPayBL();
//个人保单表
  private LCContBL           mLCContBL             = new LCContBL();
  private LCContSchema       mLCContSchemaNew      =new LCContSchema();
//个人险种表
  private LCPolBL            mLCPolBL              = new LCPolBL();//此变量没怎么用
  private LCPolSet           mLCPolSet             = new LCPolSet();
  private LCPolSet           mLCPolNewSet          = new LCPolSet();//2005-07-07
//保费项表
  private LCPremSet          pLCPremSet            = new LCPremSet();
  private LCPremSet          mLCPremSet            = new LCPremSet();
  private LCPremSet          mLCPremNewSet         = new LCPremSet();
//保险责任表LCDuty
  private LCDutySet          mLCDutySet            = new LCDutySet();

//保险帐户表
  private LCInsureAccSet     mLCInsureAccSet       = new LCInsureAccSet();
//保险帐户表记价履历表
  private LCInsureAccTraceSet mLCInsureAccTraceSet = new LCInsureAccTraceSet();
  private MMap map = new MMap();             //Yangh于2005-07-07添加
  private VData mResult = new VData();       //Yangh于2005-07-07添加
  //业务处理相关变量
  public IndiNormalPayVerifyBL() {
  }
  public static void main(String[] args) {
    VData tVData = new VData();
    LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
   //   tLJTempFeeSchema.setTempFeeNo("86110020030360000010");
   //Yangh于2005-07-15准备数据
    tLJTempFeeSchema.setOtherNo("9026000000030088");
    tLJTempFeeSchema.setOtherNoType("2");
    tLJTempFeeSchema.setConfFlag("0");
    tLJTempFeeSchema.setTempFeeType("3");


   // LJTempFeeSchema tempLJTempFeeSchema = new LJTempFeeSchema();
   // tempLJTempFeeSchema = tLJTempFeeSchema;
   // tempLJTempFeeSchema.setOtherNo("hzm");

   // LJTempFeeSchema ttLJTempFeeSchema = new LJTempFeeSchema();
   // ttLJTempFeeSchema= tLJTempFeeSchema.getSchema();
   // ttLJTempFeeSchema.setOtherNo("hzmhzm");

    GlobalInput mGI = new GlobalInput();
    mGI.ComCode="86";
    mGI.Operator="001";
    mGI.ManageCom="86";
    LCPremSchema tLCPremSchema =  new LCPremSchema();
    tLCPremSchema.setPolNo("9021000000041388");
    tLCPremSchema.setDutyCode("806001");
    tLCPremSchema.setPayPlanCode("806101");
    tLCPremSchema.setPrem(10000);
    LCPremSet tLCPremSet =  new LCPremSet();
    tLCPremSet.add(tLCPremSchema);
    tVData.add(tLJTempFeeSchema);
    tVData.add(mGI);
    tVData.add(tLCPremSet);
    IndiNormalPayVerifyBL tIndiNormalPayVerifyBL = new IndiNormalPayVerifyBL();
    tIndiNormalPayVerifyBL.submitData(tVData,"VERIFY");
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate =cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
System.out.println("After getinputdata");

    //进行业务处理
    if (!dealData())
      return false;
System.out.println("After dealData！");
    //准备往后台的数据
    if (!prepareOutputData())
      return false;
System.out.println("After prepareOutputData");

    System.out.println("Start Public Submit...");
    PubSubmit tPubSubmit = new PubSubmit();
    if (!tPubSubmit.submitData(mResult, ""))
    {
       return false;
    }

   // IndiNormalPayVerifyBLS tIndiNormalPayVerifyBLS=new IndiNormalPayVerifyBLS();
   // tIndiNormalPayVerifyBLS.submitData(mInputData,cOperate);

  //  System.out.println("End LJIndiNormalPayVerify BL Submit...");

    //如果有需要处理的错误，则返回
  /*  if (tIndiNormalPayVerifyBLS.mErrors .needDealError())
    {
        this.mErrors .copyAllErrors(tIndiNormalPayVerifyBLS.mErrors ) ;
    }*/

    mResult=null;
    return true;
  }
  /**
   * 杨红于2005-07-14修改，说明：对帐户型险种的加费处理逻辑还未添加，待处理！要调用保全的逻辑。
   * @return boolean
   */
  private boolean dealData()
 {

 //  double tempFeeMoney=0;
 //  double sumPrem =0;
   LJTempFeeDB tLJTempFeeDB=new LJTempFeeDB();
   tLJTempFeeDB.setSchema(pLJTempFeeSchema);
   mLJTempFeeSet=tLJTempFeeDB.query();//杨红于20050714说明：提供不定期多次交费的支持，至于是否
                                      //允许多次交费，在财务收付里控制！
   if (tLJTempFeeDB.mErrors.needDealError() == true)
     {
     // @@错误处理
     this.mErrors.copyAllErrors(tLJTempFeeDB.mErrors);
     CError tError = new CError();
     tError.moduleName = "IndiNormalPayVerifyBL";
     tError.functionName = "dealData";
     tError.errorMessage = "暂交费查询失败!";
     this.mErrors.addOneError(tError);
     mLJTempFeeSet.clear();
     return false;
   }
   if (mLJTempFeeSet.size() == 0)
   {
     // @@错误处理
     CError tError = new CError();
     tError.moduleName = "IndiNormalPayVerifyBL";
     tError.functionName = "dealData";
     tError.errorMessage = "暂交费记录中未找到相关数据!";
     this.mErrors.addOneError(tError);
     return false;
   }
   double tempFeeSum=0.0;//保存多次交费的总额
   //如果暂交费未到帐则不能核销
   //获取交费日期较晚的那次暂交费记录
   LJTempFeeSchema tempLJTempFeeSchema=new LJTempFeeSchema();
   for(int i=1;i<=mLJTempFeeSet.size();i++)
   {
     if(mLJTempFeeSet.get(i).getEnterAccDate()==null)
     {
       CError.buildErr(this, "交费收据号为" +mLJTempFeeSet.get(i).getTempFeeNo()+"的暂交费未到帐，不能核销！");
       return false;
     }
     if(i==1)
     {
       tempLJTempFeeSchema.setSchema(mLJTempFeeSet.get(i));
     }
     else
     {
       FDate tFDate = new FDate();
       if(tFDate.getDate(tempLJTempFeeSchema.getPayDate()).before(tFDate.getDate(mLJTempFeeSet.get(i).getPayDate())))
       {
         tempLJTempFeeSchema.setSchema(mLJTempFeeSet.get(i));
       }
     }
     //System.out.println("查询暂交费分类表");
     LJTempFeeClassDB tLJTempFeeClassDB   = new LJTempFeeClassDB();
     tLJTempFeeClassDB.setTempFeeNo(mLJTempFeeSet.get(i).getTempFeeNo());
     tLJTempFeeClassDB.setConfFlag("0");
     LJTempFeeClassSet tLJTempFeeClassSet=new LJTempFeeClassSet();
     tLJTempFeeClassSet=tLJTempFeeClassDB.query();
     if(tLJTempFeeClassDB.mErrors.needDealError() == true)
     {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "IndiNormalPayVerifyBL";
      tError.functionName = "dealData";
      tError.errorMessage = "暂交费分类表表查询失败!";
      this.mErrors.addOneError(tError);
      tLJTempFeeClassSet.clear();
      return false;
     }
     if(tLJTempFeeClassSet.size()==0)
     {
       // @@错误处理
       this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
       CError tError = new CError();
       tError.moduleName = "IndiNormalPayVerifyBL";
       tError.functionName = "dealData";
       tError.errorMessage = "暂交费分类表没有查询到符合条件的数据!";
       this.mErrors.addOneError(tError);
       tLJTempFeeClassSet.clear();
       return false;
     }
     for(int j=1;j<=tLJTempFeeClassSet.size();j++)
     {
       mLJTempFeeClassSet.add(tLJTempFeeClassSet.get(j));//将多次交费对应的暂交费分类信息放入mLJTempFeeClassSet变量中
     }
     tLJTempFeeClassSet.clear();
     tempFeeSum+=mLJTempFeeSet.get(i).getPayMoney();//tempFeeSum保存暂交费总额
   }
   //获取lccont的信息


  // VData tVData =new VData();
  /** 杨红于2005-07-07将此段注销，注销开始
   tVData.add(pLJTempFeeSchema);
   TempFeeQueryForUrgeGetUI tTempFeeQueryUI = new TempFeeQueryForUrgeGetUI();
   tTempFeeQueryUI.submitData(tVData,"QUERY");
   if(tTempFeeQueryUI.mErrors.needDealError())
   {
     this.mErrors.copyAllErrors(tTempFeeQueryUI.mErrors);
     return false;
   }
   tVData.clear();
   tVData = tTempFeeQueryUI.getResult();
   LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
   tLJTempFeeSet.set((LJTempFeeSet)tVData.getObjectByObjectName("LJTempFeeSet",0));
   mLJTempFeeBL.setSchema((LJTempFeeSchema)tLJTempFeeSet.get(1));
   tempFeeMoney=mLJTempFeeBL.getPayMoney();
   System.out.print("tempFeeMoney:"+tempFeeMoney);注销结束*/
   //Yangh于2005-07-01说明：计算该合同下所有险种应交的保费，然后与暂交费比较，如果不等则不允许核销
   /**杨红于2005-07-02注销开始，目前处理个单都是不定期不定额，后期会对这里的逻辑作修改。
   for(int n=1;n<=pLCPremSet.size();n++)
   {
     System.out.print("money:"+pLCPremSet.get(n).getPrem());
     sumPrem=sumPrem+pLCPremSet.get(n).getPrem();
   }
   if(tempFeeMoney!=sumPrem)
   {
     this.mErrors.addOneError("交费金额和暂交费金额不等:暂交费金额:"+tempFeeMoney+",交费金额:"+sumPrem);
     return false;
   }注销结束*/
  /** if(mLJTempFeeBL.getEnterAccDate()==null)
   {
       CError tError = new CError();
       tError.moduleName = "IndiNormalPayVerifyBL";
       tError.functionName = "dealData";
      // tError.errorMessage = "投保单的财务缴费还没有到帐，不能签单!（暂交费收据号：" + mLJTempFeeBL.getTempFeeNo().trim() + "）";
       tError.errorMessage = "财务缴费还没有到帐，不能核销!（暂交费收据号：" + mLJTempFeeBL.getTempFeeNo().trim() + "）";
       this.mErrors .addOneError(tError) ;
       return false;
   }*/
   boolean tReturn =false;

//step one-查询数据

  String sqlStr="";
  //String PolNo= mLJTempFeeBL.getOtherNo();
  String ContNo= pLJTempFeeSchema.getOtherNo(); //续期交费时otherno对应的是合同号
 // String GetNoticeNo=mLJTempFeeBL.getTempFeeNo();
  double actuMoney=0;  //在页面中输入的金额总和，目的是与暂交费比较  校验是否相等
 // double tempMoney=mLJTempFeeBL.getPayMoney();
  double leaveMoney=0;

  DealAccount tDealAccount = new DealAccount();
  VData tempVData = new VData();
//保险帐户表
  LCInsureAccSet     tLCInsureAccSet       = new LCInsureAccSet();
//保险帐户表记价履历表
  LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();

  //在循环的最外层是合同
  //Yangh于2005-07-01添加,添加开始
  System.out.println("查询合同表");
  LCContDB tLCContDB=new LCContDB();
  tLCContDB.setContNo(ContNo);
  LCContSet tLCContSet=tLCContDB.query();
  if(tLCContDB.mErrors.needDealError() == true)
     {
     // @@错误处理
     this.mErrors.copyAllErrors(tLCContDB.mErrors);
     CError tError = new CError();
     tError.moduleName = "IndiNormalPayVerifyBL";
     tError.functionName = "dealData";
     tError.errorMessage = "个人保单表查询失败!";
     this.mErrors.addOneError(tError);
     tLCContSet.clear();
     return false;
     }
  if(tLCContSet.size()==0)
     {
       // @@错误处理
       CError tError = new CError();
       tError.moduleName = "IndiNormalPayVerifyBL";
       tError.functionName = "dealData";
       tError.errorMessage = "个人保单表没有查到相关纪录!";
       this.mErrors.addOneError(tError);
       return false;
     }
  mLCContSchemaNew.setSchema(tLCContSet.get(1));
  double dif=mLCContSchemaNew.getDif();//合同下的余额
  //用上次的余额+暂交费  一起作为本次交费和   actuMoney为在核销页面中输入的实交金额总和
  for(int j=1;j<=pLCPremSet.size();j++)
  {
    actuMoney+=pLCPremSet.get(j).getPrem();
    //杨红于2005-07-15对死亡报案作校验
    String polNo=pLCPremSet.get(j).getPolNo();
    ExeSQL tExeSQL = new ExeSQL();
    String count;
    count = tExeSQL.getOneValue(
                    "select count(*) from ldsystrace WHERE polstate=4001 AND valiflag='1' and polno='" +
                    polNo + "'");
   if (tExeSQL.mErrors.needDealError())
      {
      this.mErrors.addOneError("保单号:" + ContNo +"下的保单险种号:"+polNo+
      "查询死亡报案时错误:" + tExeSQL.mErrors.getFirstError());
       return false;
       }
   if ((count != null) && !count.equals("0"))
      {
       this.mErrors.addOneError("保单号:" + ContNo +
                                "催收错误:已有死亡报案记录!");
       return false;
      }

  }
  if((dif+tempFeeSum)!=actuMoney)
  {
    CError.buildErr(this, "个人保单号为"+ContNo+"的合同余额是"+dif
                   +"￥，该合同不定期交费"+tempFeeSum
       +"￥，目前输入的实交金额总和为"+actuMoney+"￥."
       +"请重新输入使实交金额总和为"+(dif+tempFeeSum));
     return false;
  }

  System.out.println("查询险种表");
//1-查询保单表
     LCPolDB tLCPolDB   = new LCPolDB();
    // LCPolSet tLCPolSet = new LCPolSet();
    // sqlStr = "select * from LCPol where PolNo='"+PolNo+"'";Yangh于2005-07-01注销
     tLCPolDB.setContNo(ContNo);
    //System.out.println("sql"+sqlStr);
    // tLCPolSet = tLCPolDB.executeQuery(sqlStr);
    //tLCPolSet=tLCPolDB.query();
    mLCPolSet=tLCPolDB.query();
     if(tLCPolDB.mErrors.needDealError() == true)
     {
     // @@错误处理
     this.mErrors.copyAllErrors(tLCPolDB.mErrors);
     CError tError = new CError();
     tError.moduleName = "IndiNormalPayVerifyBL";
     tError.functionName = "dealData";
     tError.errorMessage = "个人保单险种表查询失败!";
     this.mErrors.addOneError(tError);
     mLCPolSet.clear();
     return false;
     }
     if(mLCPolSet.size()==0)
     {
     // @@错误处理
     CError tError = new CError();
     tError.moduleName = "IndiNormalPayVerifyBL";
     tError.functionName = "dealData";
     tError.errorMessage = "个人保单险种表没有查到相关纪录!";
     this.mErrors.addOneError(tError);
     return false;
     }
    // mLCPolBL.setSchema(tLCPolSet.get(1));杨红于2005-07-02注销此段代码，因为返回的lcpol记录不止一条
    // tReturn=true;

     //产生流水号
     //tLimit=PubFun.getNoLimit(mLCPolBL.getManageCom());杨红于2005-07-02注销
     tLimit=PubFun.getNoLimit(mLCContSchemaNew.getManageCom());
     serNo=PubFun1.CreateMaxNo("SERIALNO",tLimit);
     //产生交费收据号
    // tLimit=PubFun.getNoLimit(mLCPolBL.getManageCom());
     tLimit=PubFun.getNoLimit(mLCContSchemaNew.getManageCom());
     payNo=PubFun1.CreateMaxNo("PayNo",tLimit);

    System.out.println("查询保费项表");
    //2-查询保费项表//根据保单号和催交标志查询该表项
   //  LCPremSet tLCPremSet;
     LCPremBL tLCPremBL;
     LCPremDB tLCPremDB   = new LCPremDB();
     /**杨红于2005-07-02注销sqlStr的查询条件判断
     sqlStr="select * from LCPrem where PolNo='"+PolNo+"'";
     sqlStr=sqlStr+" and UrgePayFlag='N'";
     sqlStr=sqlStr+" and DutyCode not in(select DutyCode from LCDuty where PolNo='"
             +PolNo+"' and FreeRate=1) ";*/
     //Yangh于2005-07-02根据新需求修改查询Sql
     //下面判断pLCPremSet的相关信息是否还在DB中 杨红于20050714添加校验逻辑
     for(int m=1;m<=pLCPremSet.size();m++)
     {
       String polNo=pLCPremSet.get(m).getPolNo();
       String dutyCode=pLCPremSet.get(m).getDutyCode();
       String payPlanCode=pLCPremSet.get(m).getPayPlanCode();
       LCPremDB tmpLCPremDB=new LCPremDB();
       tmpLCPremDB.setPolNo(polNo);
       tmpLCPremDB.setDutyCode(dutyCode);
       tmpLCPremDB.setPayPlanCode(payPlanCode);
       LCPremSet tempLCPremSet=new LCPremSet();
       tempLCPremSet=tmpLCPremDB.query();
       if(tmpLCPremDB.mErrors.needDealError())
      {
         // @@错误处理
         this.mErrors.copyAllErrors(tmpLCPremDB.mErrors);
         CError tError = new CError();
         tError.moduleName = "IndiNormalPayVerifyBL";
         tError.functionName = "dealData";
         tError.errorMessage = "保费项表查询失败!";
         this.mErrors.addOneError(tError);
        // mLCPremSet.clear();
         return false;
      }
      if(tempLCPremSet.size()==0)
      {
        //如果查询不到，则说明在页面上改动的保费项记录已经被删除
        CError.buildErr(this, "险种号为："+polNo+",责任编码为："+dutyCode
            +",交费计划编码为："+payPlanCode+"的保费项已被删除，请重新查询后再核销");
        return false;
      }
      //查询

     }

     sqlStr="select * from LCPrem where polno in (select polno from lcpol where contno='"
         +ContNo+"') and UrgePayFlag='N' and PayIntv='-1' and DutyCode not in(select DutyCode from LCDuty where PolNo=LCPrem.PolNo and FreeRate=1)";
System.out.println("查询保费项表:"+sqlStr);
     mLCPremSet=tLCPremDB.executeQuery(sqlStr);
     if(tLCPremDB.mErrors.needDealError())
      {
         // @@错误处理
         this.mErrors.copyAllErrors(tLCPremDB.mErrors);
         CError tError = new CError();
         tError.moduleName = "IndiNormalPayVerifyBL";
         tError.functionName = "dealData";
         tError.errorMessage = "保费项表查询失败!";
         this.mErrors.addOneError(tError);
         mLCPremSet.clear();
         return false;
      }
      if(mLCPremSet.size()==0)
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tLCPremDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "IndiNormalPayVerifyBL";
        tError.functionName = "dealData";
        tError.errorMessage = "保费项表中没有符合不定期交费的纪录!";
        this.mErrors.addOneError(tError);
        return false;
      }
    //  double premSum=0.0;//计算不定期下的保费项实交保费总额
    //下面校验mLCPremSet
    //  for(int i=1;i<=mLCPremSet.size();i++)
     // {
       // premSum+=mLCPremSet.get(i).getPrem();
     // }
       tReturn=true;
  //   System.out.println("查询LCDuty保险责任表");
//3-查询LCDuty保险责任表，保单号及免交比率不等于1
     LCDutySet tLCDutySet = new LCDutySet();
     LCDutyDB tLCDutyDB   = new LCDutyDB();
     sqlStr="select * from LCDuty where PolNo in (select polno from lcpol where contno='"
         +ContNo+"') and FreeRate<1 ";//杨红于2005-07-02修改此sql查询语句
     System.out.println("查询LCDuty保险责任表:"+sqlStr);
     tLCDutySet=tLCDutyDB.executeQuery(sqlStr);
     if(tLCDutyDB.mErrors.needDealError() == true)
      {
         // @@错误处理
         this.mErrors.copyAllErrors(tLCDutyDB.mErrors);
         CError tError = new CError();
         tError.moduleName = "IndiNormalPayVerifyBL";
         tError.functionName = "dealData";
         tError.errorMessage = "保险责任表查询失败!";
         this.mErrors.addOneError(tError);
         tLCDutySet.clear();
         return false;
      }
      if(tLCDutySet.size()==0)
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tLCDutyDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "IndiNormalPayVerifyBL";
        tError.functionName = "dealData";
        tError.errorMessage = "保险责任表没有查询到符合条件的纪录!";
        this.mErrors.addOneError(tError);
        return false;
      }
      // tReturn=true;
//System.out.println("查询暂交费分类表");
//4-查询暂交费分类表
       /**
     LJTempFeeClassDB tLJTempFeeClassDB   = new LJTempFeeClassDB();
     sqlStr="select * from LJTempFeeClass where TempFeeNo='"+mLJTempFeeBL.getTempFeeNo()+"' and ConfFlag!='1'";
System.out.println("查询暂交费分类表:"+sqlStr);

     mLJTempFeeClassSet=tLJTempFeeClassDB.executeQuery(sqlStr);
     if(tLJTempFeeClassDB.mErrors.needDealError() == true)
      {
       // @@错误处理
       this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
       CError tError = new CError();
       tError.moduleName = "IndiNormalPayVerifyBL";
       tError.functionName = "dealData";
       tError.errorMessage = "暂交费分类表表查询失败!";
       this.mErrors.addOneError(tError);
       mLJTempFeeClassSet.clear();
       return false;
      }
      if(mLJTempFeeClassSet.size()==0)
      {
        // @@错误处理
        this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "IndiNormalPayVerifyBL";
        tError.functionName = "dealData";
        tError.errorMessage = "暂交费分类表没有查询到符合条件的数据!";
        this.mErrors.addOneError(tError);
        mLJTempFeeClassSet.clear();
       return false;
      }*/
     tReturn=true;

System.out.println("处理数据");
//step two-处理数据
   int i,iMax;
   //添加纪录
   if(this.mOperate.equals("VERIFY"))
   {
    //如果是定额，则校验暂交费总额与符合条件的保费项的prem和是否相等
    //如果不是定额，则不需要进行校验，Yangh于2005-07-07说明
   // if(dingeFlag)
   // {
      //校验暂交费
    /**  double diff=mLCContBL.getDif();//上次余额
      tempFeeSum+=diff;//加上上次合同下的余额
      if(tempFeeSum==premSum)
      {;}
      else if(tempFeeSum<premSum)
      {
        CError.buildErr(this, "暂交费不足，请到财务收费处补足费用。"
            +"费用信息：交费金额:"+tempFeeSum+",应交金额:"+premSum
            +",需补交金额:"+(premSum-tempFeeSum)+"!");
        return false;
      }*/
      /**杨红于2005-07-07放开多交的限制，若取消注释，则不允许多交
      else
      {
        CError.buildErr(this,"暂交费超过应交金额，请到财务退费，金额退费为："
            +(tempFeeSum-premSum)+".退费后再到此财务核销！");
        return false;
      }*/
   // }
//2-暂交费表核销标志置为1
 //   mLJTempFeeBL.setConfFlag("1");//核销标志置为1
   // tReturn=true;
   //将暂交费表集合对应记录的 核销标志置1
   for(int j=1;j<=mLJTempFeeSet.size();j++)
   {
      LJTempFeeBL tLJTempFeeBL = new LJTempFeeBL() ;
      tLJTempFeeBL.setSchema(mLJTempFeeSet.get(j));
      tLJTempFeeBL.setConfFlag("1");//核销标志置为1
      mLJTempFeeNewSet.add(tLJTempFeeBL);
   }
   //mLJTempFeeNewSet
//3-暂交费分类表，核销标志置为1
    iMax=mLJTempFeeClassSet.size() ;//注意支持多次交费
    LJTempFeeClassBL tLJTempFeeClassBL;
    for (i=1;i<=iMax;i++)
    {
     tLJTempFeeClassBL = new LJTempFeeClassBL() ;
     tLJTempFeeClassBL.setSchema(mLJTempFeeClassSet.get(i).getSchema());
     tLJTempFeeClassBL.setConfFlag("1");//核销标志置为1
     mLJTempFeeClassNewSet.add(tLJTempFeeClassBL);
    // tReturn=true;
    }
System.out.println("更新保费项表字段必须先更新保费项表");
//4-更新保费项表字段必须先更新保费项表，再填充实收个人交费表
//repair：需要计算
   LCDutyBL tLCDutyBL ;
 //  double Prem=0;
  LCPremSchema tLCPremSchema ;
   double Prem=0;//保存页面传入的实交金额
   for(int num=1;num<=mLCPremSet.size();num++)
   {
     tLCPremBL=new LCPremBL();
     tLCPremBL.setSchema(mLCPremSet.get(num));
     for(int t=1;t<=pLCPremSet.size();t++)//增加这个循环是为了取页面传入的应交金额
     {
      tLCPremSchema = new LCPremSchema();
      tLCPremSchema=pLCPremSet.get(t);
      if(tLCPremBL.getPolNo().equals(tLCPremSchema.getPolNo())&&
          tLCPremBL.getDutyCode().equals(tLCPremSchema.getDutyCode())&&
          tLCPremBL.getPayPlanCode().equals(tLCPremSchema.getPayPlanCode()) )
      {
        Prem=tLCPremSchema.getPrem(); //实际缴纳费用-页面传入
        tLCPremSchema.setSchema(tLCPremBL.getSchema());
        tLCPremSchema.setPrem(Prem);
        tLCPremBL.setPrem(Prem);
     tLCPremBL.setSumPrem(tLCPremBL.getSumPrem()+Prem);
    // tLCPremBL.setPaytoDate(CurrentDate);//repair:交至日期,注意是不定期交费
    //杨红于20050714说明：这里先不改交至日期，到后面把相关信息插入实收个人表后再变
     tLCPremBL.setPayTimes(tLCPremBL.getPayTimes()+1); //已交费次数
     tLCPremBL.setModifyDate(CurrentDate); //最后一次修改日期
     tLCPremBL.setModifyTime(CurrentTime); //最后一次修改时间
     mLCPremNewSet.add(tLCPremBL);

      }
     }
     //yangh于2005-07-06修改这里的部分逻辑
     //
     //对帐户的处理逻辑暂时搁置，因为有管理费的新需求
     /**
     for(int q=1;q<=pLCPremSet.size();q++)
     {
       tLCPremSchema = new LCPremSchema();
       tLCPremSchema=pLCPremSet.get(q);
       if(tLCPremBL.getDutyCode().equals(tLCPremSchema.getDutyCode())&&tLCPremBL.getPayPlanCode().equals(tLCPremSchema.getPayPlanCode()))
       {
         System.out.print(tLCPremSchema.getPrem());
         Prem=tLCPremSchema.getPrem(); //实际缴纳费用-页面传入
         //将数据库保费项得到，并置上实际保费，传入帐户计算函数中
         tLCPremSchema.setSchema(tLCPremBL.getSchema());
         tLCPremSchema.setPrem(Prem);
         tempVData = tDealAccount.addPrem(tLCPremSchema,"2",GetNoticeNo,"2","BF",null);
         if(tempVData!=null)
         {
           tempVData=tDealAccount.updateLCInsureAccTraceDate(mLJTempFeeBL.getPayDate(),tempVData);
           if(tempVData==null)
           {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "IndiFinUrgeVerifyBL";
            tError.functionName = "dealData";
            tError.errorMessage = "修改保险帐户表记价履历表纪录的交费日期时出错!";
            this.mErrors.addOneError(tError);
            return false;
           }
          tLCInsureAccSet=(LCInsureAccSet)tempVData.getObjectByObjectName("LCInsureAccSet",0);
          tLCInsureAccTraceSet=(LCInsureAccTraceSet)tempVData.getObjectByObjectName("LCInsureAccTraceSet",0);
          mLCInsureAccSet.add(tLCInsureAccSet);
          mLCInsureAccTraceSet.add(tLCInsureAccTraceSet);
         }
         break;
       }
     }*/


    // int n=0;
  /**   for(int n=1;n<=tLCDutySet.size();n++)
     {
      tLCDutyBL=new LCDutyBL();
      tLCDutyBL.setSchema(tLCDutySet.get(n).getSchema());
      if(tLCPremBL.getPolNo().equals(tLCDutyBL.getPolNo())&&tLCPremBL.getDutyCode().equals(tLCDutyBL.getDutyCode()))
       {
        tLCDutyBL.setPrem(tLCDutyBL.getStandPrem()*(1-tLCDutyBL.getFreeRate()));//实际保费
        //tLCDutyBL.setSumPrem(tLCDutyBL.getSumPrem()+tLCDutyBL.getPrem());//累计保费
        //tLCDutyBL.setPrem(Prem);//实际保费不修改
        //tLCDutyBL.setSumPrem(tLCDutyBL.getSumPrem()+Prem);//Yangh于2005-07-07注销
        tLCDutyBL.setSumPrem(tLCDutyBL.getSumPrem()+tLCDutyBL.getPrem());
        tLCDutyBL.setPaytoDate(CurrentDate);//repair:交至日期,注意是不定期交费
        tLCDutyBL.setModifyDate(CurrentDate);//最后一次修改日期
        tLCDutyBL.setModifyTime(CurrentTime);//最后一次修改时间

        tLCPremBL.setPrem(tLCPremBL.getStandPrem()*(1-tLCDutyBL.getFreeRate()));//实际保费
        //tLCPremBL.setSumPrem(tLCPremBL.getSumPrem()+tLCPremBL.getPrem());//累计保费
        //tLCPremBL.setPrem(Prem);//实际保费不修改
        tLCPremBL.setSumPrem(tLCPremBL.getSumPrem()+tLCPremBL.getStandPrem()*(1-tLCDutyBL.getFreeRate()));//累计保费
        tLCPremBL.setPaytoDate(CurrentDate);//repair:交至日期,注意是不定期交费
        tLCPremBL.setPayTimes(tLCPremBL.getPayTimes()+1); //已交费次数
        tLCPremBL.setModifyDate(CurrentDate); //最后一次修改日期
        tLCPremBL.setModifyTime(CurrentTime); //最后一次修改时间
        mLCPremNewSet.add(tLCPremBL);
        mLCDutySet.add(tLCDutyBL);
        break;
       }//end if
     } *///end for
   }       //end for
   //下面修改LCDuty的相关信息
   for(int n=1;n<=tLCDutySet.size();n++)
   {
     LCDutyBL tempLCDutyBL=new LCDutyBL();
     tempLCDutyBL.setSchema(tLCDutySet.get(n));
     String polNo=tempLCDutyBL.getPolNo();
     String dutyCode=tempLCDutyBL.getDutyCode();
     double sumPrem=0;
     for(int m=1;m<=mLCPremNewSet.size();m++)
     {
       if(polNo.equals(mLCPremNewSet.get(m).getPolNo())&&
           dutyCode.equals(mLCPremNewSet.get(m).getDutyCode()))
       {
         sumPrem+=mLCPremNewSet.get(m).getPrem();
       }
     }
     tempLCDutyBL.setPrem(sumPrem);
     tempLCDutyBL.setSumPrem(tempLCDutyBL.getSumPrem()+sumPrem);
     tempLCDutyBL.setPaytoDate(CurrentDate);//repair:交至日期,注意是不定期交费
     tempLCDutyBL.setModifyDate(CurrentDate);//最后一次修改日期
     tempLCDutyBL.setModifyTime(CurrentTime);//最后一次修改时间
     mLCDutySet.add(tempLCDutyBL);
   }
   tReturn=true;
   System.out.println("保费项表和个人保单表填充实收个人表");
//5-保费项表和个人保单表填充实收个人表

   LJAPayPersonBL tLJAPayPersonBL;
   iMax=mLCPremNewSet.size() ;
   for (i=1;i<=iMax;i++)
   {
     tLCPremBL = new LCPremBL();
     tLCPremBL.setSchema(mLCPremNewSet.get(i).getSchema());
     //获取保单的险种号riskcode
     LCPolDB tmpLCPolDB=new LCPolDB();
     tmpLCPolDB.setPolNo(mLCPremNewSet.get(i).getPolNo());
     LCPolSet tmpLCPolSet=new LCPolSet();
     tmpLCPolSet=tmpLCPolDB.query();
     String riskCode=tmpLCPolSet.get(1).getRiskCode();
     tLJAPayPersonBL = new LJAPayPersonBL();
   //  tLJAPayPersonBL.setPolNo(PolNo);           //保单号码 杨红于2005-07-06将此注释掉
   //目前是根据合同处理，不是按险种 Yangh于20050714说明
    tLJAPayPersonBL.setPolNo(mLCPremNewSet.get(i).getPolNo());//于2005-07-07添加
     tLJAPayPersonBL.setPayCount(tLCPremBL.getPayTimes());     //第几次交费
     /*Lis5.3 upgrade get
     tLJAPayPersonBL.setGrpPolNo(tLCPremBL.getGrpPolNo());     //集体保单号码
     */
    // tLJAPayPersonBL.setContNo(mLCPolBL.getContNo());         //总单/合同号码
    // tLJAPayPersonBL.setAppntNo(mLCPolBL.getAppntNo());       //投保人客户号码
    tLJAPayPersonBL.setGrpContNo("00000000000000000000");
    tLJAPayPersonBL.setGrpPolNo("00000000000000000000");
     tLJAPayPersonBL.setContNo(mLCContSchemaNew.getContNo());
     tLJAPayPersonBL.setAppntNo(mLCPremNewSet.get(i).getAppntNo());
     tLJAPayPersonBL.setPayNo(payNo);          //交费收据号码
     tLJAPayPersonBL.setPayAimClass("1");//交费目的分类
     tLJAPayPersonBL.setDutyCode(tLCPremBL.getDutyCode());      //责任编码
     tLJAPayPersonBL.setPayPlanCode(tLCPremBL.getPayPlanCode());//交费计划编码
     tLJAPayPersonBL.setSumDuePayMoney(tLCPremBL.getPrem());//总应交金额
     tLJAPayPersonBL.setSumActuPayMoney(tLCPremBL.getPrem());//总实交金额
     tLJAPayPersonBL.setPayIntv(tLCPremBL.getPayIntv());        //交费间隔
     //tLJAPayPersonBL.setPayDate(mLJTempFeeBL.getPayDate());        //交费日期
     tLJAPayPersonBL.setPayDate(tempLJTempFeeSchema.getPayDate());//Yangh于2005-07-07作修改
     //关于交费时间的设定修改，取暂交费表最晚的交费日期存入ljapayperson中
     tLJAPayPersonBL.setPayType("ZC");                     //交费类型:ZC 正常
    // tLJAPayPersonBL.setEnterAccDate(mLJTempFeeBL.getEnterAccDate()); //到帐日期 2005-07-07
    // tLJAPayPersonBL.setConfDate(mLJTempFeeBL.getConfDate());         //确认日期 2005-07-07
     tLJAPayPersonBL.setEnterAccDate(tempLJTempFeeSchema.getEnterAccDate());
     tLJAPayPersonBL.setConfDate(tempLJTempFeeSchema.getConfDate());
     tLJAPayPersonBL.setLastPayToDate(tLCPremBL.getPaytoDate());      //原交至日期
     tLJAPayPersonBL.setCurPayToDate(CurrentDate);                //现交至日期
//      tLJAPayPersonBL.setInInsuAccState(tLJSPayPersonBL.getInInsuAccState());//转入保险帐户状态
   //  tLJAPayPersonBL.setApproveCode(mLCPolBL.getApproveCode());      //复核人编码 2005-07-07
   //  tLJAPayPersonBL.setApproveDate(mLCPolBL.getApproveDate());      //复核日期   2005-07-07
     tLJAPayPersonBL.setApproveCode(mLCContSchemaNew.getApproveCode());
     tLJAPayPersonBL.setApproveDate(mLCContSchemaNew.getApproveDate());
     tLJAPayPersonBL.setApproveTime(mLCContSchemaNew.getApproveTime());
     tLJAPayPersonBL.setSerialNo(serNo);                              //流水号
     tLJAPayPersonBL.setOperator(mGI.Operator);      //操作员
     tLJAPayPersonBL.setMakeDate(CurrentDate);                        //入机日期
     tLJAPayPersonBL.setMakeTime(CurrentTime);                        //入机时间
  //   tLJAPayPersonBL.setGetNoticeNo(mLJTempFeeBL.getTempFeeNo());//通知书号码，取较晚的那次
     tLJAPayPersonBL.setGetNoticeNo(tempLJTempFeeSchema.getTempFeeNo());
     tLJAPayPersonBL.setModifyDate(CurrentDate);                      //最后一次修改日期
     tLJAPayPersonBL.setModifyTime(CurrentTime);  //最后一次修改时间
     tLJAPayPersonBL.setAgentCode(mLCContSchemaNew.getAgentCode());
     tLJAPayPersonBL.setAgentGroup(mLCContSchemaNew.getAgentGroup());
     tLJAPayPersonBL.setManageCom(mLCContSchemaNew.getManageCom());    //管理机构
     tLJAPayPersonBL.setAgentCom(mLCContSchemaNew.getAgentCom());//代理机构
     tLJAPayPersonBL.setAgentType(mLCContSchemaNew.getAgentType());    //代理机构内部分类
   //  tLJAPayPersonBL.setRiskCode(mLCPolBL.getRiskCode());      //险种编码
     tLJAPayPersonBL.setRiskCode(riskCode);    //修改于2005-07-07
    //交至日期,注意前面填充实收个人表的原交至日期时需要用到这个日期,用完后再赋予新值
    // tLCPremBL.setPaytoDate(CurrentDate);
    mLCPremNewSet.get(i).setPaytoDate(CurrentDate);
     mLJAPayPersonSet.add(tLJAPayPersonBL);
    // actuMoney=actuMoney+tLCPremBL.getPrem();
    // tReturn=true;
   }
   //杨红于2005-06-30说明：目前交费是在合同级，而不是在险种级，在签单时的余额放在合同下的余额字段diff
   //在lcpol里面的leavingmoney字段没有起任何作用，险种的多交少补也是针对合同级
   //需要对余额重新处理
   //leaveMoney=mLCPolBL.getLeavingMoney();
   // leaveMoney=0;//余额
   //说明：合同和险种的余额如何处理？？２005-07-07
  // double sumMoney=actuMoney-leaveMoney-tempMoney;
  System.out.println("更新保单表字段");
//6-更新保单表字段


    mLCContSchemaNew.setDif(0);//说明不定期交费第一次后，合同表的余额肯定会变为0

    mLCContSchemaNew.setPaytoDate(CurrentDate);//交至日期
    mLCContSchemaNew.setSumPrem(mLCContSchemaNew.getSumPrem()+actuMoney);
    mLCContSchemaNew.setModifyDate(CurrentDate);//最后一次修改日期
    mLCContSchemaNew.setModifyTime(CurrentTime);
    //下面更新险种信息
    for(int m=1;m<=mLCPolSet.size();m++)
    {
      LCPolBL tLCPolBL=new LCPolBL();
      tLCPolBL.setSchema(mLCPolSet.get(m));
      tLCPolBL.setPaytoDate(CurrentDate);
      //因为是不定期，所以增加的保费值 不是mLCPolSet.get(m).getPrem()
      //根据页面输入的值存入到lcpol的
      double polPremSum=0;
      for(int q=1;q<=mLCPremNewSet.size();q++)
      {
        if(mLCPremNewSet.get(q).getPolNo().equals(tLCPolBL.getPolNo()))
        {
          polPremSum+=mLCPremNewSet.get(q).getPrem();
        }
      }
      tLCPolBL.setSumPrem(mLCPolSet.get(m).getSumPrem()+polPremSum);
      tLCPolBL.setModifyDate(CurrentDate);
      tLCPolBL.setModifyTime(CurrentTime);
      mLCPolNewSet.add(tLCPolBL);
    }
  /*Yangh于2005-07-07注销
   if(sumMoney<0)//如果应收款<实际交款
    mLCPolBL.setLeavingMoney(-sumMoney);
   else
    mLCPolBL.setLeavingMoney(0);

   //mLCPolBL.setPaytoDate(mLJTempFeeBL.getPayDate());                //交至日期
   mLCPolBL.setPaytoDate(CurrentDate);                //交至日期
   mLCPolBL.setSumPrem(mLCPolBL.getSumPrem()+mLJTempFeeBL.getPayMoney());//总累计保费
   mLCPolBL.setModifyDate(CurrentDate);//最后一次修改日期
   mLCPolBL.setModifyTime(CurrentTime);//最后一次修改时间
   tReturn=true;*/

//1-个人保单表和暂交费表数据填充实收总表
    mLJAPayBL.setPayNo(payNo);                             //交费收据号码
    mLJAPayBL.setIncomeNo(ContNo);                          //应收/实收编号//杨红于2005-07-06修改为合同号
    mLJAPayBL.setIncomeType("2");                          //应收/实收编号类型
    mLJAPayBL.setAppntNo(mLCContSchemaNew.getAppntNo());           //  投保人客户号码
    mLJAPayBL.setSumActuPayMoney(actuMoney);   // 总实交金额
    mLJAPayBL.setEnterAccDate(tempLJTempFeeSchema.getEnterAccDate());  // 到帐日期,取最晚的那次暂交费记录
    mLJAPayBL.setPayDate(tempLJTempFeeSchema.getPayDate());           //交费日期
    mLJAPayBL.setConfDate(tempLJTempFeeSchema.getConfDate());      //确认日期
    mLJAPayBL.setApproveCode(mLCContSchemaNew.getApproveCode());    //复核人编码
    mLJAPayBL.setApproveDate(mLCContSchemaNew.getApproveDate());    //复核日期
    mLJAPayBL.setGetNoticeNo(tempLJTempFeeSchema.getTempFeeNo());  //交费通知书号
    mLJAPayBL.setSerialNo(serNo);                           //流水号
    mLJAPayBL.setOperator(mGI.Operator);                    //操作员
    mLJAPayBL.setMakeDate(CurrentDate);                     //入机时间
    mLJAPayBL.setMakeTime(CurrentTime);                     //入机时间
    mLJAPayBL.setModifyDate(CurrentDate);                   //最后一次修改日期
    mLJAPayBL.setModifyTime(CurrentTime);                   //最后一次修改时间
    mLJAPayBL.setManageCom(mLCContSchemaNew.getManageCom());
/*Lis5.3 upgrade get
    mLJAPayBL.setBankCode(mLCPolBL.getBankCode());      //银行编码
    mLJAPayBL.setBankAccNo(mLCPolBL.getBankAccNo());   //银行帐号
    */
  //  mLJAPayBL.setRiskCode(mLCPolBL.getRiskCode());   // 险种编码
    mLJAPayBL.setAgentCode(mLCContSchemaNew.getAgentCode());
    mLJAPayBL.setAgentGroup(mLCContSchemaNew.getAgentGroup());
//更新完毕
}
 return true;
}

  private boolean dealData2()
  {

    double tempFeeMoney=0;
    double sumPrem =0;
    VData tVData =new VData();
    tVData.add(pLJTempFeeSchema);
    TempFeeQueryForUrgeGetUI tTempFeeQueryUI = new TempFeeQueryForUrgeGetUI();
    tTempFeeQueryUI.submitData(tVData,"QUERY");
    if(tTempFeeQueryUI.mErrors.needDealError())
    {
      this.mErrors.copyAllErrors(tTempFeeQueryUI.mErrors);
      return false;
    }
    tVData.clear();
    tVData = tTempFeeQueryUI.getResult();
    LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
    tLJTempFeeSet.set((LJTempFeeSet)tVData.getObjectByObjectName("LJTempFeeSet",0));
    mLJTempFeeBL.setSchema((LJTempFeeSchema)tLJTempFeeSet.get(1));
    tempFeeMoney=mLJTempFeeBL.getPayMoney();
    System.out.print("tempFeeMoney:"+tempFeeMoney);
    //Yangh于2005-07-01说明：计算该合同下所有险种应交的保费，然后与暂交费比较，如果不等则不允许核销
    /**杨红于2005-07-02注销开始，目前处理个单都是不定期不定额，后期会对这里的逻辑作修改。
    for(int n=1;n<=pLCPremSet.size();n++)
    {
      System.out.print("money:"+pLCPremSet.get(n).getPrem());
      sumPrem=sumPrem+pLCPremSet.get(n).getPrem();
    }
    if(tempFeeMoney!=sumPrem)
    {
      this.mErrors.addOneError("交费金额和暂交费金额不等:暂交费金额:"+tempFeeMoney+",交费金额:"+sumPrem);
      return false;
    }注销结束*/
    if(mLJTempFeeBL.getEnterAccDate()==null)
    {
        CError tError = new CError();
        tError.moduleName = "IndiNormalPayVerifyBL";
        tError.functionName = "dealData";
       // tError.errorMessage = "投保单的财务缴费还没有到帐，不能签单!（暂交费收据号：" + mLJTempFeeBL.getTempFeeNo().trim() + "）";
        tError.errorMessage = "财务缴费还没有到帐，不能核销!（暂交费收据号：" + mLJTempFeeBL.getTempFeeNo().trim() + "）";
        this.mErrors .addOneError(tError) ;
        return false;
    }
    boolean tReturn =false;

//step one-查询数据

   String sqlStr="";
   //String PolNo= mLJTempFeeBL.getOtherNo();
   String ContNo= mLJTempFeeBL.getOtherNo(); //续期交费时otherno对应的是合同号
   String GetNoticeNo=mLJTempFeeBL.getTempFeeNo();
   double actuMoney=0;
   double tempMoney=mLJTempFeeBL.getPayMoney();
   double leaveMoney=0;

   DealAccount tDealAccount = new DealAccount();
   VData tempVData = new VData();
//保险帐户表
   LCInsureAccSet     tLCInsureAccSet       = new LCInsureAccSet();
//保险帐户表记价履历表
   LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();

   //在循环的最外层是合同
   //Yangh于2005-07-01添加,添加开始
   System.out.println("查询合同表");
   LCContDB tLCContDB=new LCContDB();
   tLCContDB.setContNo(ContNo);
   LCContSet tLCContSet=tLCContDB.query();
   if(tLCContDB.mErrors.needDealError() == true)
      {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCContDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "IndiNormalPayVerifyBL";
      tError.functionName = "dealData";
      tError.errorMessage = "个人保单表查询失败!";
      this.mErrors.addOneError(tError);
      tLCContSet.clear();
      return false;
      }
   if(tLCContSet.size()==0)
      {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "IndiNormalPayVerifyBL";
        tError.functionName = "dealData";
        tError.errorMessage = "个人保单表没有查到相关纪录!";
        this.mErrors.addOneError(tError);
        return false;
      }
   mLCContBL.setSchema(tLCContSet.get(1));


   System.out.println("查询险种表");
//1-查询保单表
      LCPolDB tLCPolDB   = new LCPolDB();
     // LCPolSet tLCPolSet = new LCPolSet();
     // sqlStr = "select * from LCPol where PolNo='"+PolNo+"'";Yangh于2005-07-01注销
      tLCPolDB.setContNo(ContNo);
     //System.out.println("sql"+sqlStr);
     // tLCPolSet = tLCPolDB.executeQuery(sqlStr);
     //tLCPolSet=tLCPolDB.query();
     mLCPolSet=tLCPolDB.query();
      if(tLCPolDB.mErrors.needDealError() == true)
      {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCPolDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "IndiNormalPayVerifyBL";
      tError.functionName = "dealData";
      tError.errorMessage = "个人保单险种表查询失败!";
      this.mErrors.addOneError(tError);
      mLCPolSet.clear();
      return false;
      }
      if(mLCPolSet.size()==0)
      {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "IndiNormalPayVerifyBL";
      tError.functionName = "dealData";
      tError.errorMessage = "个人保单险种表没有查到相关纪录!";
      this.mErrors.addOneError(tError);
      return false;
      }
     // mLCPolBL.setSchema(tLCPolSet.get(1));杨红于2005-07-02注销此段代码，因为返回的lcpol记录不止一条
     // tReturn=true;

      //产生流水号
      //tLimit=PubFun.getNoLimit(mLCPolBL.getManageCom());杨红于2005-07-02注销
      tLimit=PubFun.getNoLimit(mLCContBL.getManageCom());
      serNo=PubFun1.CreateMaxNo("SERIALNO",tLimit);
      //产生交费收据号
     // tLimit=PubFun.getNoLimit(mLCPolBL.getManageCom());
      tLimit=PubFun.getNoLimit(mLCContBL.getManageCom());
      payNo=PubFun1.CreateMaxNo("PayNo",tLimit);

     System.out.println("查询保费项表");
     //2-查询保费项表//根据保单号和催交标志查询该表项
    //  LCPremSet tLCPremSet;
      LCPremBL tLCPremBL;
      LCPremDB tLCPremDB   = new LCPremDB();
      /**杨红于2005-07-02注销sqlStr的查询条件判断
      sqlStr="select * from LCPrem where PolNo='"+PolNo+"'";
      sqlStr=sqlStr+" and UrgePayFlag='N'";
      sqlStr=sqlStr+" and DutyCode not in(select DutyCode from LCDuty where PolNo='"
              +PolNo+"' and FreeRate=1) ";*/
      //Yangh于2005-07-02根据新需求修改查询Sql
      sqlStr="select * from LCPrem where polno in (select polno from lccont where contno='"
          +ContNo+"') and UrgePayFlag='N' and PayIntv='-1' and DutyCode not in(select DutyCode from LCDuty where PolNo=LCPrem.PolNo and FreeRate=1)";
System.out.println("查询保费项表:"+sqlStr);
      mLCPremSet=tLCPremDB.executeQuery(sqlStr);
      if(tLCPremDB.mErrors.needDealError())
       {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCPremDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "IndiNormalPayVerifyBL";
          tError.functionName = "dealData";
          tError.errorMessage = "保费项表查询失败!";
          this.mErrors.addOneError(tError);
          mLCPremSet.clear();
          return false;
       }
       if(mLCPremSet.size()==0)
       {
         // @@错误处理
         this.mErrors.copyAllErrors(tLCPremDB.mErrors);
         CError tError = new CError();
         tError.moduleName = "IndiNormalPayVerifyBL";
         tError.functionName = "dealData";
         tError.errorMessage = "保费项表中没有符合不定期交费的纪录!";
         this.mErrors.addOneError(tError);
         return false;
       }
        tReturn=true;
      System.out.println("查询LCDuty保险责任表");
//3-查询LCDuty保险责任表，保单号及免交比率不等于1
      LCDutySet tLCDutySet = new LCDutySet();
      LCDutyDB tLCDutyDB   = new LCDutyDB();
      sqlStr="select * from LCDuty where PolNo in (select polno from lccont where contno='"
          +ContNo+"') and FreeRate<1 ";//杨红于2005-07-02修改此sql查询语句
      System.out.println("查询LCDuty保险责任表:"+sqlStr);
      tLCDutySet=tLCDutyDB.executeQuery(sqlStr);
      if(tLCDutyDB.mErrors.needDealError() == true)
       {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCDutyDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "IndiNormalPayVerifyBL";
          tError.functionName = "dealData";
          tError.errorMessage = "保险责任表查询失败!";
          this.mErrors.addOneError(tError);
          tLCDutySet.clear();
          return false;
       }
       if(tLCDutySet.size()==0)
       {
         // @@错误处理
         this.mErrors.copyAllErrors(tLCDutyDB.mErrors);
         CError tError = new CError();
         tError.moduleName = "IndiNormalPayVerifyBL";
         tError.functionName = "dealData";
         tError.errorMessage = "保险责任表没有查询到符合条件的纪录!";
         this.mErrors.addOneError(tError);
         return false;
       }
        tReturn=true;
System.out.println("查询暂交费分类表");
//4-查询暂交费分类表
      LJTempFeeClassDB tLJTempFeeClassDB   = new LJTempFeeClassDB();
      sqlStr="select * from LJTempFeeClass where TempFeeNo='"+mLJTempFeeBL.getTempFeeNo()+"' and ConfFlag!='1'";
System.out.println("查询暂交费分类表:"+sqlStr);

      mLJTempFeeClassSet=tLJTempFeeClassDB.executeQuery(sqlStr);
      if(tLJTempFeeClassDB.mErrors.needDealError() == true)
       {
        // @@错误处理
        this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "IndiNormalPayVerifyBL";
        tError.functionName = "dealData";
        tError.errorMessage = "暂交费分类表表查询失败!";
        this.mErrors.addOneError(tError);
        mLJTempFeeClassSet.clear();
        return false;
       }
       if(mLJTempFeeClassSet.size()==0)
       {
         // @@错误处理
         this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
         CError tError = new CError();
         tError.moduleName = "IndiNormalPayVerifyBL";
         tError.functionName = "dealData";
         tError.errorMessage = "暂交费分类表没有查询到符合条件的数据!";
         this.mErrors.addOneError(tError);
         mLJTempFeeClassSet.clear();
        return false;
       }
      tReturn=true;

 System.out.println("处理数据");
//step two-处理数据
    int i,iMax;
    //添加纪录
    if(this.mOperate.equals("VERIFY"))
    {

//2-暂交费表核销标志置为1
     mLJTempFeeBL.setConfFlag("1");//核销标志置为1
     tReturn=true;

//3-暂交费分类表，核销标志置为1
     iMax=mLJTempFeeClassSet.size() ;
     LJTempFeeClassBL tLJTempFeeClassBL;
     for (i=1;i<=iMax;i++)
     {
      tLJTempFeeClassBL = new LJTempFeeClassBL() ;
      tLJTempFeeClassBL.setSchema(mLJTempFeeClassSet.get(i).getSchema());
      tLJTempFeeClassBL.setConfFlag("1");//核销标志置为1
      mLJTempFeeClassNewSet.add(tLJTempFeeClassBL);
      tReturn=true;
     }
 System.out.println("更新保费项表字段必须先更新保费项表");
//4-更新保费项表字段必须先更新保费项表，再填充实收个人交费表
//repair：需要计算
    LCDutyBL tLCDutyBL ;
    double Prem=0;
    LCPremSchema tLCPremSchema = new LCPremSchema();
    for(int num=1;num<=mLCPremSet.size();num++)
    {
      tLCPremBL=new LCPremBL();
      tLCPremBL.setSchema(mLCPremSet.get(num));
      //yangh于2005-07-06修改这里的部分逻辑
      for(int q=1;q<=pLCPremSet.size();q++)
      {
        tLCPremSchema = new LCPremSchema();
        tLCPremSchema=pLCPremSet.get(q);
        if(tLCPremBL.getDutyCode().equals(tLCPremSchema.getDutyCode())&&tLCPremBL.getPayPlanCode().equals(tLCPremSchema.getPayPlanCode()))
        {
          System.out.print(tLCPremSchema.getPrem());
          Prem=tLCPremSchema.getPrem(); //实际缴纳费用-页面传入
          //将数据库保费项得到，并置上实际保费，传入帐户计算函数中
          tLCPremSchema.setSchema(tLCPremBL.getSchema());
          tLCPremSchema.setPrem(Prem);
          tempVData = tDealAccount.addPrem(tLCPremSchema,"2",GetNoticeNo,"2","BF",null);
          if(tempVData!=null)
          {
            tempVData=tDealAccount.updateLCInsureAccTraceDate(mLJTempFeeBL.getPayDate(),tempVData);
            if(tempVData==null)
            {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "IndiFinUrgeVerifyBL";
             tError.functionName = "dealData";
             tError.errorMessage = "修改保险帐户表记价履历表纪录的交费日期时出错!";
             this.mErrors.addOneError(tError);
             return false;
            }
           tLCInsureAccSet=(LCInsureAccSet)tempVData.getObjectByObjectName("LCInsureAccSet",0);
           tLCInsureAccTraceSet=(LCInsureAccTraceSet)tempVData.getObjectByObjectName("LCInsureAccTraceSet",0);
           mLCInsureAccSet.add(tLCInsureAccSet);
           mLCInsureAccTraceSet.add(tLCInsureAccTraceSet);
          }
          break;
        }
      }

      int n=0;
      for(n=1;n<=tLCDutySet.size();n++)
      {
       tLCDutyBL=new LCDutyBL();
       tLCDutyBL.setSchema(tLCDutySet.get(n).getSchema());
       if(tLCPremBL.getPolNo().equals(tLCDutyBL.getPolNo())&&tLCPremBL.getDutyCode().equals(tLCDutyBL.getDutyCode()))
        {
         tLCDutyBL.setPrem(tLCDutyBL.getStandPrem()*(1-tLCDutyBL.getFreeRate()));//实际保费
         //tLCDutyBL.setSumPrem(tLCDutyBL.getSumPrem()+tLCDutyBL.getPrem());//累计保费
         //tLCDutyBL.setPrem(Prem);//实际保费不修改
         tLCDutyBL.setSumPrem(tLCDutyBL.getSumPrem()+Prem);
         tLCDutyBL.setPaytoDate(CurrentDate);//repair:交至日期,注意是不定期交费
         tLCDutyBL.setModifyDate(CurrentDate);//最后一次修改日期
         tLCDutyBL.setModifyTime(CurrentTime);//最后一次修改时间

         tLCPremBL.setPrem(tLCPremBL.getStandPrem()*(1-tLCDutyBL.getFreeRate()));//实际保费
         //tLCPremBL.setSumPrem(tLCPremBL.getSumPrem()+tLCPremBL.getPrem());//累计保费
         //tLCPremBL.setPrem(Prem);//实际保费不修改
         tLCPremBL.setSumPrem(tLCPremBL.getSumPrem()+Prem);//累计保费
         tLCPremBL.setPaytoDate(CurrentDate);//repair:交至日期,注意是不定期交费
         tLCPremBL.setPayTimes(tLCPremBL.getPayTimes()+1); //已交费次数
         tLCPremBL.setModifyDate(CurrentDate); //最后一次修改日期
         tLCPremBL.setModifyTime(CurrentTime); //最后一次修改时间
         mLCPremNewSet.add(tLCPremBL);
         mLCDutySet.add(tLCDutyBL);
         break;
        }//end if
      } //end for
    }//end for
    tReturn=true;
 System.out.println("保费项表和个人保单表填充实收个人表");
//5-保费项表和个人保单表填充实收个人表

    LJAPayPersonBL tLJAPayPersonBL;
    iMax=mLCPremNewSet.size() ;
    for (i=1;i<=iMax;i++)
    {
      tLCPremBL = new LCPremBL();
      tLCPremBL.setSchema(mLCPremNewSet.get(i).getSchema());
      tLJAPayPersonBL = new LJAPayPersonBL();
    //  tLJAPayPersonBL.setPolNo(PolNo);           //保单号码 杨红于2005-07-06将此注释掉
      tLJAPayPersonBL.setPayCount(tLCPremBL.getPayTimes()+1);     //第几次交费
      /*Lis5.3 upgrade get
      tLJAPayPersonBL.setGrpPolNo(tLCPremBL.getGrpPolNo());     //集体保单号码
      */
      tLJAPayPersonBL.setContNo(mLCPolBL.getContNo());         //总单/合同号码
      tLJAPayPersonBL.setAppntNo(mLCPolBL.getAppntNo());       //投保人客户号码
      tLJAPayPersonBL.setPayNo(payNo);          //交费收据号码
      tLJAPayPersonBL.setPayAimClass("1");//交费目的分类
      tLJAPayPersonBL.setDutyCode(tLCPremBL.getDutyCode());      //责任编码
      tLJAPayPersonBL.setPayPlanCode(tLCPremBL.getPayPlanCode());//交费计划编码
      tLJAPayPersonBL.setSumDuePayMoney(tLCPremBL.getPrem());//总应交金额
      tLJAPayPersonBL.setSumActuPayMoney(tLCPremBL.getPrem());//总实交金额
      tLJAPayPersonBL.setPayIntv(tLCPremBL.getPayIntv());        //交费间隔
      tLJAPayPersonBL.setPayDate(mLJTempFeeBL.getPayDate());        //交费日期
      tLJAPayPersonBL.setPayType("ZC");                     //交费类型:ZC 正常
      tLJAPayPersonBL.setEnterAccDate(mLJTempFeeBL.getEnterAccDate()); //到帐日期
      tLJAPayPersonBL.setConfDate(mLJTempFeeBL.getConfDate());         //确认日期
      tLJAPayPersonBL.setLastPayToDate(tLCPremBL.getPaytoDate());      //原交至日期
      tLJAPayPersonBL.setCurPayToDate(CurrentDate);                //现交至日期
//      tLJAPayPersonBL.setInInsuAccState(tLJSPayPersonBL.getInInsuAccState());//转入保险帐户状态
      tLJAPayPersonBL.setApproveCode(mLCPolBL.getApproveCode());      //复核人编码
      tLJAPayPersonBL.setApproveDate(mLCPolBL.getApproveDate());      //复核日期
      tLJAPayPersonBL.setSerialNo(serNo);                              //流水号
      tLJAPayPersonBL.setOperator(mGI.Operator);      //操作员
      tLJAPayPersonBL.setMakeDate(CurrentDate);                        //入机日期
      tLJAPayPersonBL.setMakeTime(CurrentTime);                        //入机时间
      tLJAPayPersonBL.setGetNoticeNo(mLJTempFeeBL.getTempFeeNo());//通知书号码
      tLJAPayPersonBL.setModifyDate(CurrentDate);                      //最后一次修改日期
      tLJAPayPersonBL.setModifyTime(CurrentTime);  //最后一次修改时间
      tLJAPayPersonBL.setAgentCode(mLCPolBL.getAgentCode());
      tLJAPayPersonBL.setAgentGroup(mLCPolBL.getAgentGroup());
      tLJAPayPersonBL.setManageCom(mLCPolBL.getManageCom());    //管理机构
      tLJAPayPersonBL.setAgentCom(mLCPolBL.getAgentCom());//代理机构
      tLJAPayPersonBL.setAgentType(mLCPolBL.getAgentType());    //代理机构内部分类
      tLJAPayPersonBL.setRiskCode(mLCPolBL.getRiskCode());      //险种编码

     //交至日期,注意前面填充实收个人表的原交至日期时需要用到这个日期,用完后再赋予新值
      tLCPremBL.setPaytoDate(CurrentDate);

      mLJAPayPersonSet.add(tLJAPayPersonBL);
      actuMoney=actuMoney+tLCPremBL.getPrem();
      tReturn=true;
    }
    //杨红于2005-06-30说明：目前交费是在合同级，而不是在险种级，在签单时的余额放在合同下的余额字段diff
    //在lcpol里面的leavingmoney字段没有起任何作用，险种的多交少补也是针对合同级
    //需要对余额重新处理
    leaveMoney=mLCPolBL.getLeavingMoney();
    double sumMoney=actuMoney-leaveMoney-tempMoney;
 System.out.println("更新保单表字段");
//6-更新保单表字段
    if(sumMoney<0)//如果应收款<实际交款
     mLCPolBL.setLeavingMoney(-sumMoney);
    else
     mLCPolBL.setLeavingMoney(0);

    //mLCPolBL.setPaytoDate(mLJTempFeeBL.getPayDate());                //交至日期
    mLCPolBL.setPaytoDate(CurrentDate);                //交至日期
    mLCPolBL.setSumPrem(mLCPolBL.getSumPrem()+mLJTempFeeBL.getPayMoney());//总累计保费
    mLCPolBL.setModifyDate(CurrentDate);//最后一次修改日期
    mLCPolBL.setModifyTime(CurrentTime);//最后一次修改时间
    tReturn=true;

//1-个人保单表和暂交费表数据填充实收总表
     mLJAPayBL.setPayNo(payNo);                             //交费收据号码
     mLJAPayBL.setIncomeNo(ContNo);                          //应收/实收编号//杨红于2005-07-06修改为合同号
     mLJAPayBL.setIncomeType("2");                          //应收/实收编号类型
     mLJAPayBL.setAppntNo(mLCPolBL.getAppntNo());           //  投保人客户号码
     mLJAPayBL.setSumActuPayMoney(actuMoney);   // 总实交金额
     mLJAPayBL.setEnterAccDate(mLJTempFeeBL.getEnterAccDate());  // 到帐日期
     mLJAPayBL.setPayDate(mLJTempFeeBL.getPayDate());           //交费日期
     mLJAPayBL.setConfDate(mLJTempFeeBL.getConfDate());      //确认日期
     mLJAPayBL.setApproveCode(mLCPolBL.getApproveCode());    //复核人编码
     mLJAPayBL.setApproveDate(mLCPolBL.getApproveDate());    //复核日期
     mLJAPayBL.setGetNoticeNo(mLJTempFeeBL.getTempFeeNo());  //交费通知书号
     mLJAPayBL.setSerialNo(serNo);                           //流水号
     mLJAPayBL.setOperator(mGI.Operator);                    //操作员
     mLJAPayBL.setMakeDate(CurrentDate);                     //入机时间
     mLJAPayBL.setMakeTime(CurrentTime);                     //入机时间
     mLJAPayBL.setModifyDate(CurrentDate);                   //最后一次修改日期
     mLJAPayBL.setModifyTime(CurrentTime);                   //最后一次修改时间
     mLJAPayBL.setManageCom(mLCPolBL.getManageCom());
/*Lis5.3 upgrade get
     mLJAPayBL.setBankCode(mLCPolBL.getBankCode());      //银行编码
     mLJAPayBL.setBankAccNo(mLCPolBL.getBankAccNo());   //银行帐号
     */
     mLJAPayBL.setRiskCode(mLCPolBL.getRiskCode());   // 险种编码
     mLJAPayBL.setAgentCode(mLCPolBL.getAgentCode());
     mLJAPayBL.setAgentGroup(mLCPolBL.getAgentGroup());
//更新完毕
 }
  return tReturn;
}

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  //杨红于2005-07-06将以前的dealData注释掉
/**
  private boolean dealData()
  {

    double tempFeeMoney=0;
    double sumPrem =0;
    VData tVData =new VData();
    tVData.add(pLJTempFeeSchema);
    TempFeeQueryForUrgeGetUI tTempFeeQueryUI = new TempFeeQueryForUrgeGetUI();
    tTempFeeQueryUI.submitData(tVData,"QUERY");
    if(tTempFeeQueryUI.mErrors.needDealError())
    {
      this.mErrors.copyAllErrors(tTempFeeQueryUI.mErrors);
      return false;
    }
    tVData.clear();
    tVData = tTempFeeQueryUI.getResult();
    LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
    tLJTempFeeSet.set((LJTempFeeSet)tVData.getObjectByObjectName("LJTempFeeSet",0));
    mLJTempFeeBL.setSchema((LJTempFeeSchema)tLJTempFeeSet.get(1));
    tempFeeMoney=mLJTempFeeBL.getPayMoney();
    System.out.print("tempFeeMoney:"+tempFeeMoney);
    //Yangh于2005-07-01说明：计算该合同下所有险种应交的保费，然后与暂交费比较，如果不等则不允许核销
    /**杨红于2005-07-02注销开始，目前处理个单都是不定期不定额，后期会对这里的逻辑作修改。
    for(int n=1;n<=pLCPremSet.size();n++)
    {
      System.out.print("money:"+pLCPremSet.get(n).getPrem());
      sumPrem=sumPrem+pLCPremSet.get(n).getPrem();
    }
    if(tempFeeMoney!=sumPrem)
    {
      this.mErrors.addOneError("交费金额和暂交费金额不等:暂交费金额:"+tempFeeMoney+",交费金额:"+sumPrem);
      return false;
    }注销结束
    if(mLJTempFeeBL.getEnterAccDate()==null)
    {
        CError tError = new CError();
        tError.moduleName = "IndiNormalPayVerifyBL";
        tError.functionName = "dealData";
       // tError.errorMessage = "投保单的财务缴费还没有到帐，不能签单!（暂交费收据号：" + mLJTempFeeBL.getTempFeeNo().trim() + "）";
        tError.errorMessage = "财务缴费还没有到帐，不能核销!（暂交费收据号：" + mLJTempFeeBL.getTempFeeNo().trim() + "）";
        this.mErrors .addOneError(tError) ;
        return false;
    }
    boolean tReturn =false;

//step one-查询数据

   String sqlStr="";
   //String PolNo= mLJTempFeeBL.getOtherNo();
   String ContNo= mLJTempFeeBL.getOtherNo(); //续期交费时otherno对应的是合同号
   String GetNoticeNo=mLJTempFeeBL.getTempFeeNo();
   double actuMoney=0;
   double tempMoney=mLJTempFeeBL.getPayMoney();
   double leaveMoney=0;

   DealAccount tDealAccount = new DealAccount();
   VData tempVData = new VData();
//保险帐户表
   LCInsureAccSet     tLCInsureAccSet       = new LCInsureAccSet();
//保险帐户表记价履历表
   LCInsureAccTraceSet tLCInsureAccTraceSet = new LCInsureAccTraceSet();

   //在循环的最外层是合同
   //Yangh于2005-07-01添加,添加开始
   System.out.println("查询合同表");
   LCContDB tLCContDB=new LCContDB();
   tLCContDB.setContNo(ContNo);
   LCContSet tLCContSet=tLCContDB.query();
   if(tLCContDB.mErrors.needDealError() == true)
      {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCContDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "IndiNormalPayVerifyBL";
      tError.functionName = "dealData";
      tError.errorMessage = "个人保单表查询失败!";
      this.mErrors.addOneError(tError);
      tLCContSet.clear();
      return false;
      }
   if(tLCContSet.size()==0)
      {
        // @@错误处理
        CError tError = new CError();
        tError.moduleName = "IndiNormalPayVerifyBL";
        tError.functionName = "dealData";
        tError.errorMessage = "个人保单表没有查到相关纪录!";
        this.mErrors.addOneError(tError);
        return false;
      }
   mLCContBL.setSchema(tLCContSet.get(1));


   System.out.println("查询险种表");
//1-查询保单表
      LCPolDB tLCPolDB   = new LCPolDB();
     // LCPolSet tLCPolSet = new LCPolSet();
     // sqlStr = "select * from LCPol where PolNo='"+PolNo+"'";Yangh于2005-07-01注销
      tLCPolDB.setContNo(ContNo);
     //System.out.println("sql"+sqlStr);
     // tLCPolSet = tLCPolDB.executeQuery(sqlStr);
     //tLCPolSet=tLCPolDB.query();
     mLCPolSet=tLCPolDB.query();
      if(tLCPolDB.mErrors.needDealError() == true)
      {
      // @@错误处理
      this.mErrors.copyAllErrors(tLCPolDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "IndiNormalPayVerifyBL";
      tError.functionName = "dealData";
      tError.errorMessage = "个人保单险种表查询失败!";
      this.mErrors.addOneError(tError);
      mLCPolSet.clear();
      return false;
      }
      if(mLCPolSet.size()==0)
      {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "IndiNormalPayVerifyBL";
      tError.functionName = "dealData";
      tError.errorMessage = "个人保单险种表没有查到相关纪录!";
      this.mErrors.addOneError(tError);
      return false;
      }
     // mLCPolBL.setSchema(tLCPolSet.get(1));杨红于2005-07-02注销此段代码，因为返回的lcpol记录不止一条
     // tReturn=true;

      //产生流水号
      //tLimit=PubFun.getNoLimit(mLCPolBL.getManageCom());杨红于2005-07-02注销
      tLimit=PubFun.getNoLimit(mLCContBL.getManageCom());
      serNo=PubFun1.CreateMaxNo("SERIALNO",tLimit);
      //产生交费收据号
     // tLimit=PubFun.getNoLimit(mLCPolBL.getManageCom());
      tLimit=PubFun.getNoLimit(mLCContBL.getManageCom());
      payNo=PubFun1.CreateMaxNo("PayNo",tLimit);

     System.out.println("查询保费项表");
     //2-查询保费项表//根据保单号和催交标志查询该表项
    //  LCPremSet tLCPremSet;
      LCPremBL tLCPremBL;
      LCPremDB tLCPremDB   = new LCPremDB();
      /**杨红于2005-07-02注销sqlStr的查询条件判断
      sqlStr="select * from LCPrem where PolNo='"+PolNo+"'";
      sqlStr=sqlStr+" and UrgePayFlag='N'";
      sqlStr=sqlStr+" and DutyCode not in(select DutyCode from LCDuty where PolNo='"
              +PolNo+"' and FreeRate=1) ";
      //Yangh于2005-07-02根据新需求修改查询Sql
      sqlStr="select * from LCPrem where polno in (select polno from lccont where contno='"
          +ContNo+"') and UrgePayFlag='N' and PayIntv='-1' and DutyCode not in(select DutyCode from LCDuty where PolNo=LCPrem.PolNo and FreeRate=1)";
System.out.println("查询保费项表:"+sqlStr);
      mLCPremSet=tLCPremDB.executeQuery(sqlStr);
      if(tLCPremDB.mErrors.needDealError())
       {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCPremDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "IndiNormalPayVerifyBL";
          tError.functionName = "dealData";
          tError.errorMessage = "保费项表查询失败!";
          this.mErrors.addOneError(tError);
          mLCPremSet.clear();
          return false;
       }
       if(mLCPremSet.size()==0)
       {
         // @@错误处理
         this.mErrors.copyAllErrors(tLCPremDB.mErrors);
         CError tError = new CError();
         tError.moduleName = "IndiNormalPayVerifyBL";
         tError.functionName = "dealData";
         tError.errorMessage = "保费项表中没有符合不定期交费的纪录!";
         this.mErrors.addOneError(tError);
         return false;
       }
        tReturn=true;
      System.out.println("查询LCDuty保险责任表");
//3-查询LCDuty保险责任表，保单号及免交比率不等于1
      LCDutySet tLCDutySet = new LCDutySet();
      LCDutyDB tLCDutyDB   = new LCDutyDB();
      sqlStr="select * from LCDuty where PolNo in (select polno from lccont where contno='"
          +ContNo+"') and FreeRate<1 ";//杨红于2005-07-02修改此sql查询语句
      System.out.println("查询LCDuty保险责任表:"+sqlStr);
      tLCDutySet=tLCDutyDB.executeQuery(sqlStr);
      if(tLCDutyDB.mErrors.needDealError() == true)
       {
          // @@错误处理
          this.mErrors.copyAllErrors(tLCDutyDB.mErrors);
          CError tError = new CError();
          tError.moduleName = "IndiNormalPayVerifyBL";
          tError.functionName = "dealData";
          tError.errorMessage = "保险责任表查询失败!";
          this.mErrors.addOneError(tError);
          tLCDutySet.clear();
          return false;
       }
       if(tLCDutySet.size()==0)
       {
         // @@错误处理
         this.mErrors.copyAllErrors(tLCDutyDB.mErrors);
         CError tError = new CError();
         tError.moduleName = "IndiNormalPayVerifyBL";
         tError.functionName = "dealData";
         tError.errorMessage = "保险责任表没有查询到符合条件的纪录!";
         this.mErrors.addOneError(tError);
         return false;
       }
        tReturn=true;
System.out.println("查询暂交费分类表");
//4-查询暂交费分类表
      LJTempFeeClassDB tLJTempFeeClassDB   = new LJTempFeeClassDB();
      sqlStr="select * from LJTempFeeClass where TempFeeNo='"+mLJTempFeeBL.getTempFeeNo()+"' and ConfFlag!='1'";
System.out.println("查询暂交费分类表:"+sqlStr);

      mLJTempFeeClassSet=tLJTempFeeClassDB.executeQuery(sqlStr);
      if(tLJTempFeeClassDB.mErrors.needDealError() == true)
       {
        // @@错误处理
        this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
        CError tError = new CError();
        tError.moduleName = "IndiNormalPayVerifyBL";
        tError.functionName = "dealData";
        tError.errorMessage = "暂交费分类表表查询失败!";
        this.mErrors.addOneError(tError);
        mLJTempFeeClassSet.clear();
        return false;
       }
       if(mLJTempFeeClassSet.size()==0)
       {
         // @@错误处理
         this.mErrors.copyAllErrors(tLJTempFeeClassDB.mErrors);
         CError tError = new CError();
         tError.moduleName = "IndiNormalPayVerifyBL";
         tError.functionName = "dealData";
         tError.errorMessage = "暂交费分类表没有查询到符合条件的数据!";
         this.mErrors.addOneError(tError);
         mLJTempFeeClassSet.clear();
        return false;
       }
      tReturn=true;

 System.out.println("处理数据");
//step two-处理数据
    int i,iMax;
    //添加纪录
    if(this.mOperate.equals("VERIFY"))
    {

//2-暂交费表核销标志置为1
     mLJTempFeeBL.setConfFlag("1");//核销标志置为1
     tReturn=true;

//3-暂交费分类表，核销标志置为1
     iMax=mLJTempFeeClassSet.size() ;
     LJTempFeeClassBL tLJTempFeeClassBL;
     for (i=1;i<=iMax;i++)
     {
      tLJTempFeeClassBL = new LJTempFeeClassBL() ;
      tLJTempFeeClassBL.setSchema(mLJTempFeeClassSet.get(i).getSchema());
      tLJTempFeeClassBL.setConfFlag("1");//核销标志置为1
      mLJTempFeeClassNewSet.add(tLJTempFeeClassBL);
      tReturn=true;
     }
 System.out.println("更新保费项表字段必须先更新保费项表");
//4-更新保费项表字段必须先更新保费项表，再填充实收个人交费表
//repair：需要计算
    LCDutyBL tLCDutyBL ;
    double Prem=0;
    LCPremSchema tLCPremSchema = new LCPremSchema();
    for(int num=1;num<=mLCPremSet.size();num++)
    {
      tLCPremBL=new LCPremBL();
      tLCPremBL.setSchema(mLCPremSet.get(num));
      //yangh于2005-07-06修改这里的部分逻辑
      for(int q=1;q<=pLCPremSet.size();q++)
      {
        tLCPremSchema = new LCPremSchema();
        tLCPremSchema=pLCPremSet.get(q);
        if(tLCPremBL.getDutyCode().equals(tLCPremSchema.getDutyCode())&&tLCPremBL.getPayPlanCode().equals(tLCPremSchema.getPayPlanCode()))
        {
          System.out.print(tLCPremSchema.getPrem());
          Prem=tLCPremSchema.getPrem(); //实际缴纳费用-页面传入
          //将数据库保费项得到，并置上实际保费，传入帐户计算函数中
          tLCPremSchema.setSchema(tLCPremBL.getSchema());
          tLCPremSchema.setPrem(Prem);
          tempVData = tDealAccount.addPrem(tLCPremSchema,"2",GetNoticeNo,"2","BF",null);
          if(tempVData!=null)
          {
            tempVData=tDealAccount.updateLCInsureAccTraceDate(mLJTempFeeBL.getPayDate(),tempVData);
            if(tempVData==null)
            {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "IndiFinUrgeVerifyBL";
             tError.functionName = "dealData";
             tError.errorMessage = "修改保险帐户表记价履历表纪录的交费日期时出错!";
             this.mErrors.addOneError(tError);
             return false;
            }
           tLCInsureAccSet=(LCInsureAccSet)tempVData.getObjectByObjectName("LCInsureAccSet",0);
           tLCInsureAccTraceSet=(LCInsureAccTraceSet)tempVData.getObjectByObjectName("LCInsureAccTraceSet",0);
           mLCInsureAccSet.add(tLCInsureAccSet);
           mLCInsureAccTraceSet.add(tLCInsureAccTraceSet);
          }
          break;
        }
      }

      int n=0;
      for(n=1;n<=tLCDutySet.size();n++)
      {
       tLCDutyBL=new LCDutyBL();
       tLCDutyBL.setSchema(tLCDutySet.get(n).getSchema());
       if(tLCPremBL.getPolNo().equals(tLCDutyBL.getPolNo())&&tLCPremBL.getDutyCode().equals(tLCDutyBL.getDutyCode()))
        {
         tLCDutyBL.setPrem(tLCDutyBL.getStandPrem()*(1-tLCDutyBL.getFreeRate()));//实际保费
         //tLCDutyBL.setSumPrem(tLCDutyBL.getSumPrem()+tLCDutyBL.getPrem());//累计保费
         //tLCDutyBL.setPrem(Prem);//实际保费不修改
         tLCDutyBL.setSumPrem(tLCDutyBL.getSumPrem()+Prem);
         tLCDutyBL.setPaytoDate(CurrentDate);//repair:交至日期,注意是不定期交费
         tLCDutyBL.setModifyDate(CurrentDate);//最后一次修改日期
         tLCDutyBL.setModifyTime(CurrentTime);//最后一次修改时间

         tLCPremBL.setPrem(tLCPremBL.getStandPrem()*(1-tLCDutyBL.getFreeRate()));//实际保费
         //tLCPremBL.setSumPrem(tLCPremBL.getSumPrem()+tLCPremBL.getPrem());//累计保费
         //tLCPremBL.setPrem(Prem);//实际保费不修改
         tLCPremBL.setSumPrem(tLCPremBL.getSumPrem()+Prem);//累计保费
         tLCPremBL.setPaytoDate(CurrentDate);//repair:交至日期,注意是不定期交费
         tLCPremBL.setPayTimes(tLCPremBL.getPayTimes()+1); //已交费次数
         tLCPremBL.setModifyDate(CurrentDate); //最后一次修改日期
         tLCPremBL.setModifyTime(CurrentTime); //最后一次修改时间
         mLCPremNewSet.add(tLCPremBL);
         mLCDutySet.add(tLCDutyBL);
         break;
        }//end if
      } //end for
    }//end for
    tReturn=true;
 System.out.println("保费项表和个人保单表填充实收个人表");
//5-保费项表和个人保单表填充实收个人表

    LJAPayPersonBL tLJAPayPersonBL;
    iMax=mLCPremNewSet.size() ;
    for (i=1;i<=iMax;i++)
    {
      tLCPremBL = new LCPremBL();
      tLCPremBL.setSchema(mLCPremNewSet.get(i).getSchema());
      tLJAPayPersonBL = new LJAPayPersonBL();
    //  tLJAPayPersonBL.setPolNo(PolNo);           //保单号码 杨红于2005-07-06将此注释掉
      tLJAPayPersonBL.setPayCount(tLCPremBL.getPayTimes()+1);     //第几次交费
      /*Lis5.3 upgrade get
      tLJAPayPersonBL.setGrpPolNo(tLCPremBL.getGrpPolNo());     //集体保单号码

      tLJAPayPersonBL.setContNo(mLCPolBL.getContNo());         //总单/合同号码
      tLJAPayPersonBL.setAppntNo(mLCPolBL.getAppntNo());       //投保人客户号码
      tLJAPayPersonBL.setPayNo(payNo);          //交费收据号码
      tLJAPayPersonBL.setPayAimClass("1");//交费目的分类
      tLJAPayPersonBL.setDutyCode(tLCPremBL.getDutyCode());      //责任编码
      tLJAPayPersonBL.setPayPlanCode(tLCPremBL.getPayPlanCode());//交费计划编码
      tLJAPayPersonBL.setSumDuePayMoney(tLCPremBL.getPrem());//总应交金额
      tLJAPayPersonBL.setSumActuPayMoney(tLCPremBL.getPrem());//总实交金额
      tLJAPayPersonBL.setPayIntv(tLCPremBL.getPayIntv());        //交费间隔
      tLJAPayPersonBL.setPayDate(mLJTempFeeBL.getPayDate());        //交费日期
      tLJAPayPersonBL.setPayType("ZC");                     //交费类型:ZC 正常
      tLJAPayPersonBL.setEnterAccDate(mLJTempFeeBL.getEnterAccDate()); //到帐日期
      tLJAPayPersonBL.setConfDate(mLJTempFeeBL.getConfDate());         //确认日期
      tLJAPayPersonBL.setLastPayToDate(tLCPremBL.getPaytoDate());      //原交至日期
      tLJAPayPersonBL.setCurPayToDate(CurrentDate);                //现交至日期
//      tLJAPayPersonBL.setInInsuAccState(tLJSPayPersonBL.getInInsuAccState());//转入保险帐户状态
      tLJAPayPersonBL.setApproveCode(mLCPolBL.getApproveCode());      //复核人编码
      tLJAPayPersonBL.setApproveDate(mLCPolBL.getApproveDate());      //复核日期
      tLJAPayPersonBL.setSerialNo(serNo);                              //流水号
      tLJAPayPersonBL.setOperator(mGI.Operator);      //操作员
      tLJAPayPersonBL.setMakeDate(CurrentDate);                        //入机日期
      tLJAPayPersonBL.setMakeTime(CurrentTime);                        //入机时间
      tLJAPayPersonBL.setGetNoticeNo(mLJTempFeeBL.getTempFeeNo());//通知书号码
      tLJAPayPersonBL.setModifyDate(CurrentDate);                      //最后一次修改日期
      tLJAPayPersonBL.setModifyTime(CurrentTime);  //最后一次修改时间
      tLJAPayPersonBL.setAgentCode(mLCPolBL.getAgentCode());
      tLJAPayPersonBL.setAgentGroup(mLCPolBL.getAgentGroup());
      tLJAPayPersonBL.setManageCom(mLCPolBL.getManageCom());    //管理机构
      tLJAPayPersonBL.setAgentCom(mLCPolBL.getAgentCom());//代理机构
      tLJAPayPersonBL.setAgentType(mLCPolBL.getAgentType());    //代理机构内部分类
      tLJAPayPersonBL.setRiskCode(mLCPolBL.getRiskCode());      //险种编码

     //交至日期,注意前面填充实收个人表的原交至日期时需要用到这个日期,用完后再赋予新值
      tLCPremBL.setPaytoDate(CurrentDate);

      mLJAPayPersonSet.add(tLJAPayPersonBL);
      actuMoney=actuMoney+tLCPremBL.getPrem();
      tReturn=true;
    }
    //杨红于2005-06-30说明：目前交费是在合同级，而不是在险种级，在签单时的余额放在合同下的余额字段diff
    //在lcpol里面的leavingmoney字段没有起任何作用，险种的多交少补也是针对合同级
    //需要对余额重新处理
    leaveMoney=mLCPolBL.getLeavingMoney();
    double sumMoney=actuMoney-leaveMoney-tempMoney;
 System.out.println("更新保单表字段");
//6-更新保单表字段
    if(sumMoney<0)//如果应收款<实际交款
     mLCPolBL.setLeavingMoney(-sumMoney);
    else
     mLCPolBL.setLeavingMoney(0);

    //mLCPolBL.setPaytoDate(mLJTempFeeBL.getPayDate());                //交至日期
    mLCPolBL.setPaytoDate(CurrentDate);                //交至日期
    mLCPolBL.setSumPrem(mLCPolBL.getSumPrem()+mLJTempFeeBL.getPayMoney());//总累计保费
    mLCPolBL.setModifyDate(CurrentDate);//最后一次修改日期
    mLCPolBL.setModifyTime(CurrentTime);//最后一次修改时间
    tReturn=true;

//1-个人保单表和暂交费表数据填充实收总表
     mLJAPayBL.setPayNo(payNo);                             //交费收据号码
     mLJAPayBL.setIncomeNo(ContNo);                          //应收/实收编号//杨红于2005-07-06修改为合同号
     mLJAPayBL.setIncomeType("2");                          //应收/实收编号类型
     mLJAPayBL.setAppntNo(mLCPolBL.getAppntNo());           //  投保人客户号码
     mLJAPayBL.setSumActuPayMoney(actuMoney);   // 总实交金额
     mLJAPayBL.setEnterAccDate(mLJTempFeeBL.getEnterAccDate());  // 到帐日期
     mLJAPayBL.setPayDate(mLJTempFeeBL.getPayDate());           //交费日期
     mLJAPayBL.setConfDate(mLJTempFeeBL.getConfDate());      //确认日期
     mLJAPayBL.setApproveCode(mLCPolBL.getApproveCode());    //复核人编码
     mLJAPayBL.setApproveDate(mLCPolBL.getApproveDate());    //复核日期
     mLJAPayBL.setGetNoticeNo(mLJTempFeeBL.getTempFeeNo());  //交费通知书号
     mLJAPayBL.setSerialNo(serNo);                           //流水号
     mLJAPayBL.setOperator(mGI.Operator);                    //操作员
     mLJAPayBL.setMakeDate(CurrentDate);                     //入机时间
     mLJAPayBL.setMakeTime(CurrentTime);                     //入机时间
     mLJAPayBL.setModifyDate(CurrentDate);                   //最后一次修改日期
     mLJAPayBL.setModifyTime(CurrentTime);                   //最后一次修改时间
     mLJAPayBL.setManageCom(mLCPolBL.getManageCom());
/*Lis5.3 upgrade get
     mLJAPayBL.setBankCode(mLCPolBL.getBankCode());      //银行编码
     mLJAPayBL.setBankAccNo(mLCPolBL.getBankAccNo());   //银行帐号

     mLJAPayBL.setRiskCode(mLCPolBL.getRiskCode());   // 险种编码
     mLJAPayBL.setAgentCode(mLCPolBL.getAgentCode());
     mLJAPayBL.setAgentGroup(mLCPolBL.getAgentGroup());
//更新完毕
 }
  return tReturn;
}*/

 /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData mInputData)
  {
    // 暂交费表
    pLJTempFeeSchema=((LJTempFeeSchema)mInputData.getObjectByObjectName("LJTempFeeSchema",0));
    mGI=(GlobalInput)mInputData.getObjectByObjectName("GlobalInput",0);
    pLCPremSet=(LCPremSet)mInputData.getObjectByObjectName("LCPremSet",0);

    if(pLJTempFeeSchema ==null||mGI==null||pLCPremSet==null )
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="IndiNormalPayVerifyBL";
      tError.functionName="getInputData";
      tError.errorMessage="没有得到足够的数据，请您确认!";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
  }

  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData()
  {
   /** mInputData=new VData();
    try
    {
    mInputData.add(mLJAPayBL);	          //实收总表
    mInputData.add(mLJAPayPersonSet);     //实收个人表
    mInputData.add(mLJTempFeeBL);         //暂交费表
    mInputData.add(mLJTempFeeClassNewSet);//暂交费分类表
  //  mInputData.add(mLCPolBL);             //个人保单表
    mInputData.add(mLCPremNewSet);        //保费项表
    mInputData.add(mLCDutySet);           //保险责任表
  //  mInputData.add(mLCInsureAccSet);       //保险帐户表
  //  mInputData.add(mLCInsureAccTraceSet);  //保险帐户表记价履历表
    mInputData.add(mGI);
System.out.println("prepareOutputData:");
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="IndiNormalPayVerifyBL";
      tError.functionName="prepareData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }*/
   map.put(mLCContSchemaNew,"UPDATE");
   map.put(mLCPolNewSet,"UPDATE");
   map.put(mLJTempFeeNewSet,"UPDATE");
   map.put(mLJTempFeeClassNewSet,"UPDATE");
   map.put(mLCDutySet,"UPDATE");
   map.put(mLCPremNewSet,"UPDATE");
   map.put(mLJAPayPersonSet,"INSERT");
   map.put(mLJAPayBL.getSchema(),"INSERT");
   mResult.clear();
   mResult.add(map);
   map=null;
   return true;
  }
}

