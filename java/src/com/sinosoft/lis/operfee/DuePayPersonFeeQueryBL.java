 package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: 保单查询业务逻辑处理类</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HST
 * @version 1.0
 */
public class DuePayPersonFeeQueryBL
{
  /** 错误处理类，每个需要错误处理的类中都放置该类 */
  public  CErrors mErrors = new CErrors();
  /** 往后面传输数据的容器 */
  private VData mResult = new VData();
  /** 数据操作字符串 */
  private String mOperate;

  /** 业务处理相关变量 */
  /** 应收个人交费表 */
  private LJSPayPersonSet  mLJSPayPersonSet = new LJSPayPersonSet() ;
  private LJSPayPersonSchema  mLJSPayPersonSchema = new LJSPayPersonSchema() ;

  public DuePayPersonFeeQueryBL() {}

  public static void main(String[] args) {
  }

  /**
  * 传输数据的公共方法
  * @param: cInputData 输入的数据
  *         cOperate 数据操作
  * @return:
  */
  public boolean submitData(VData cInputData,String cOperate)
  {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData))
      return false;
System.out.println("---getInputData---");

    //进行业务处理
	    if (!queryLJSPayPerson())
	      return false;
System.out.println("---queryLJSPayPerson---");

    return true;
  }

  public VData getResult()
  {
  	return mResult;
  }

  /**
   * 从输入数据中得到所有对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
    // 检验查询条件
    mLJSPayPersonSchema.setSchema((LJSPayPersonSchema)cInputData.getObjectByObjectName("LJSPayPersonSchema",0));

    if(mLJSPayPersonSchema == null)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "DuePayPersonFeeQueryBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "请输入查询条件!";
      this.mErrors.addOneError(tError) ;
      return false;
    }
    return true;
  }

  /**
   * 查询应收个人交费表信息
   * 输出：如果发生错误则返回false,否则返回true
   */
  private boolean queryLJSPayPerson()
  {
  	// 应收个人交费表信息
      LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
      tLJSPayPersonDB.setSchema(mLJSPayPersonSchema);

      mLJSPayPersonSet = tLJSPayPersonDB.query();
      if (tLJSPayPersonDB.mErrors.needDealError() == true)
      {
      // @@错误处理
      this.mErrors.copyAllErrors(tLJSPayPersonDB.mErrors);
      CError tError = new CError();
      tError.moduleName = "DuePayPersonFeeQueryBL";
      tError.functionName = "queryData";
      tError.errorMessage = "应收个人交费表查询失败!";
      this.mErrors.addOneError(tError);
      mLJSPayPersonSet.clear();
      return false;
    }
    if (mLJSPayPersonSet.size() == 0)
    {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "DuePayPersonFeeQueryBL";
      tError.functionName = "queryData";
      tError.errorMessage = "未找到相关数据!";
      this.mErrors.addOneError(tError);
      mLJSPayPersonSet.clear();
      return false;
    }
System.out.println("LJSPayPerson num="+mLJSPayPersonSet.size());
	mResult.add( mLJSPayPersonSet );
	return true;
  }

  /**
   * 准备往后层输出所需要的数据
   * 输出：如果准备数据时发生错误则返回false,否则返回true
   */
  private boolean prepareOutputData()
  {
    mResult.clear();
    try
    {
      mResult.add( mLJSPayPersonSet );
    }
    catch(Exception ex)
    {
      // @@错误处理
      CError tError =new CError();
      tError.moduleName="DuePayPersonFeeQueryBL";
      tError.functionName="prepareOutputData";
      tError.errorMessage="在准备往后层处理所需要的数据时出错。";
      this.mErrors .addOneError(tError) ;
      return false;
    }
    return true;
    }
}
