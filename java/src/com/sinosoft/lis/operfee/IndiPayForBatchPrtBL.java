package com.sinosoft.lis.operfee;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.utility.StrTool;
import java.io.File;

/**
 * <p>Title: 保险业务系统</p>
 * <p>Description: 应收通知书批量打印</p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: Sinosoft</p>
 * @author
 */
public class IndiPayForBatchPrtBL {
    private String mOperate;
    public CErrors mErrors = new CErrors();
    private VData mResult = new VData();
    private GlobalInput mG = new GlobalInput();
    public IndiPayForBatchPrtBL() {}

    private LDSysVarSchema mLDSysVarSchema = new LDSysVarSchema();
    private LJAPaySet mLJAPaySet = new LJAPaySet();

    private boolean operFlag = true;
    private String strLogs = "";
    private String Content = "";
    private String mxmlFileName = "";
    private int mCount = 0;
    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private MMap mMap = new MMap();
    private PubSubmit tPubSubmit = new PubSubmit();
    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) {
        cInputData = (VData) cInputData.clone();
        this.mOperate = cOperate;
        if (!getInputData(cInputData)) {
            return false;
        }
        if (mOperate.equals("PRINT")) {
            if (!dealData()) {
                return false;
            }
        }
        if (!prepareOutputData()) {
            return false;
        }
        tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(mInputData, "")) {
            if (tPubSubmit.mErrors.needDealError()) {
                buildError("submitData", "保存再保计算结果出错!");
            }
            return false;
        }

        return true;
    }

    public VData getResult() {
        return mResult;
    }

    private boolean getInputData(VData cInputData) {
        mLJAPaySet = (LJAPaySet) cInputData.getObjectByObjectName("LJAPaySet",
                0);
        if (mLJAPaySet == null) { //（服务事件关联表）
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "IndiPayForBatchPrtBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "传入的数据为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        System.out.println("++++++=================================" + mG);
        mLDSysVarSchema.setSchema((LDSysVarSchema) cInputData.
                                  getObjectByObjectName("LDSysVarSchema", 0));
        return true;
    }

    /*****************************************************************************************
     * Name     :checkdate
     * Function :判断数据逻辑上的正确性
     * 1:判断用户代码和用户姓名的关联是否正确
     * 2:校验用户和录入的管理机构是否匹配
     * 3:若不是最高的核赔权限的用户一定要有上级用户代码
     * 4:若是新增，判断该用户代码是否已经存在。
     *
     */
    private boolean checkdata() {
        return true;
    }

    public int getCount() {
        return mCount;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData() {
        if (mOperate.equals("PRINT")) {
            String mGetNowTime = PubFun.getCurrentTime();
            String mGetTime = StrTool.replace(mGetNowTime, ":", "-");
            VData tVData;

            XmlExport txmlExportAll = new XmlExport();
            txmlExportAll.createDocument("ON");
            mCount = mLJAPaySet.size();
            String mXmlFileName[] = new String[mCount];
            LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet(); //新增，用来更新打印状态，次数等信息。by huxl @ 20071024
            for (int i = 1; i <= mCount; i++) {
                LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
                LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
                tLOPRTManagerDB.setCode("92");
                tLOPRTManagerDB.setStandbyFlag2(mLJAPaySet.get(i).getPayNo());
                tLOPRTManagerSchema = tLOPRTManagerDB.query().get(1);

                if (tLOPRTManagerSchema == null) {
                    VData xVData = new VData();
                    TransferData xTransferData = new TransferData();
                    xTransferData.setNameAndValue("0", "0");

                    LJAPaySchema xLJAPaySchema = new LJAPaySchema();

                    LJAPayDB t = new LJAPayDB();
                    t.setPayNo(mLJAPaySet.get(i).getPayNo());
                    xLJAPaySchema = t.query().get(1);

                    xVData.addElement(mG);
                    xVData.addElement(xTransferData);
                    xVData.addElement(xLJAPaySchema);
                    IndiPayComptPrintBL tIndiPayComptPrintBL = new
                            IndiPayComptPrintBL();
                    tIndiPayComptPrintBL.submitData(xVData, "INSERT");

                    LOPRTManagerDB xLOPRTManagerDB = new LOPRTManagerDB();
                    xLOPRTManagerDB.setCode("92");
                    xLOPRTManagerDB.setStandbyFlag2(mLJAPaySet.get(i).getPayNo());
                    tLOPRTManagerSchema = null;
                    tLOPRTManagerSchema = tLOPRTManagerDB.query().get(1);

                }
                //下面封装要更新的打印管理信息
                tLOPRTManagerSchema.setStateFlag("1");
                tLOPRTManagerSchema.setPrintTimes(tLOPRTManagerSchema.
                                                  getPrintTimes() + 1);
                tLOPRTManagerSchema.setDoneDate(PubFun.getCurrentDate());
                tLOPRTManagerSchema.setDoneTime(PubFun.getCurrentTime());
                tLOPRTManagerSet.add(tLOPRTManagerSchema);

                System.out.println("~~~~~第" + i + "个StandbyFlag2--" +
                                   tLOPRTManagerSchema.getStandbyFlag2());
                mxmlFileName = StrTool.unicodeToGBK("0" +
                        tLOPRTManagerSchema.getCode()) + "-" +
                               tLOPRTManagerSchema.getPrtSeq() + "-" +
                               tLOPRTManagerSchema.getOtherNo();
                mXmlFileName[i - 1] = mxmlFileName;
                System.out.println("~~~~~第" + i + "个文件--" + mxmlFileName);
                if (!callPrintService(tLOPRTManagerSchema)) {
                    Content = " 印刷号" + tLOPRTManagerSchema.getPrtSeq() + "：" +
                              this.mErrors.getFirstError().toString();
                    strLogs = strLogs + Content;
                    continue;
                }
                XmlExport txmlExport = (XmlExport) mResult.
                                       getObjectByObjectName("XmlExport", 0);
                if (txmlExport == null) {
                    Content = "印刷号" + tLOPRTManagerSchema.getPrtSeq() +
                              "没有得到要显示的数据文件！";
                    strLogs = strLogs + Content;
                    continue;
                }
                if (operFlag == true) {
                    File f = new File(mLDSysVarSchema.getSysVarValue());
                    f.mkdir();
                    System.out.println("PATH : " +
                                       mLDSysVarSchema.getSysVarValue());
                    System.out.println("PATH2 : " +
                                       mLDSysVarSchema.getSysVarValue().
                                       substring(0,
                                                 mLDSysVarSchema.getSysVarValue().
                                                 length() - 1));
                    txmlExport.outputDocumentToFile(mLDSysVarSchema.
                            getSysVarValue().substring(0,
                            mLDSysVarSchema.getSysVarValue().length() - 1) +
                            File.separator + "printdata"
                            + File.separator + "data" + File.separator +
                            "brief" + File.separator, mxmlFileName);
                }
            }
            this.mMap.put(tLOPRTManagerSet, "UPDATE");
            tVData = new VData();
            tVData.add(mXmlFileName);
            mResult = tVData;
        }
        return true;
    }

    public void buildError(String szFunc, String szErrMsg) {
        CError cError = new CError();
        cError.moduleName = "IndiPayForBatchPrtBL";
        cError.functionName = szFunc;
        cError.errorMessage = szErrMsg;
        this.mErrors.addOneError(cError);
    }

    public static void main(String[] args) {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ComCode = "86";
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        LDSysVarSchema mLDSysVarSchema = new LDSysVarSchema();
        mLDSysVarSchema.setSysVarValue("E:\\workspace\\ui\\");

        LJAPaySet tLJAPaySet = new LJAPaySet();
        LJAPaySchema tLJAPaySchema = new LJAPaySchema();
        tLJAPaySchema.setPayNo("32000310672");
        tLJAPaySet.add(tLJAPaySchema);
        LJAPaySchema tLJAPaySchema2 = new LJAPaySchema();
        tLJAPaySchema2.setPayNo("32000310646");
        tLJAPaySet.add(tLJAPaySchema2);
//      LJAPaySchema tLJAPaySchema3 = new LJAPaySchema();
//      tLJAPaySchema3.setGetNoticeNo("32000307962");
//      tLJAPaySet.add(tLJAPaySchema3);


        VData aVData = new VData();
        aVData.add(tGlobalInput);
        aVData.add(tLJAPaySet);
        aVData.add(mLDSysVarSchema);
        /*
             String aUserCode = "000011";
             VData aVData = new VData();
             aVData.addElement(aUserCode);
         */
        IndiPayForBatchPrtBL tIndiPayForBatchPrt = new IndiPayForBatchPrtBL();
        tIndiPayForBatchPrt.submitData(aVData, "PRINT");
        int tCount = tIndiPayForBatchPrt.getCount();
        System.out.println("~~~~~~~~~~~~~~~~~~~" + tCount);
        String tFileName[] = new String[tCount];
        VData tResult = new VData();
        tResult = tIndiPayForBatchPrt.getResult();
        tFileName = (String[]) tResult.getObject(0);
        System.out.println("~~~~~~~~~~~~~~~~~~~2" + tFileName);

    }

    private boolean callPrintService(LOPRTManagerSchema aLOPRTManagerSchema) {

        // 查找打印服务
        String strSQL = "SELECT * FROM LDCode WHERE CodeType = 'print_service'";
        strSQL += " AND Code = '" + aLOPRTManagerSchema.getCode() + "'";
        strSQL += " AND OtherSign = '0'";
        System.out.println(strSQL);
        LDCodeSet tLDCodeSet = new LDCodeDB().executeQuery(strSQL);

        if (tLDCodeSet.size() == 0) {
            buildError("dealData",
                       "找不到对应的打印服务类(Code = '" + aLOPRTManagerSchema.getCode() +
                       "')");
            return false;
        }

        // 调用打印服务
        LDCodeSchema tLDCodeSchema = tLDCodeSet.get(1);

        try {
            Class cls = Class.forName(tLDCodeSchema.getCodeAlias());
            PrintService ps = (PrintService) cls.newInstance();

            // 准备数据
            String strOperate = tLDCodeSchema.getCodeName();

            VData vData = new VData();

            vData.add(mG);
            vData.add(aLOPRTManagerSchema);

            if (!ps.submitData(vData, strOperate)) {
                mErrors.copyAllErrors(ps.getErrors());
                return false;
            }

            mResult = ps.getResult();

        } catch (Exception ex) {
            ex.printStackTrace();
            buildError("callPrintService", ex.toString());
            return false;
        }

        return true;
    }

    /**
     * 准备往后层输出所需要的数据
     * 输出：如果准备数据时发生错误则返回false,否则返回true
     */
    private boolean prepareOutputData() {
        System.out.println("Come to prepareOutputData()...........");
        try {
            this.mInputData.clear();
            this.mInputData.add(mMap);

        } catch (Exception ex) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "IndiPayForBatchPrtBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

}
