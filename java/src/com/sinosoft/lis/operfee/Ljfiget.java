package com.sinosoft.lis.operfee;

import com.sinosoft.lis.vschema.LJFIGetSet;
import com.sinosoft.lis.schema.LJFIGetSchema;
import com.sinosoft.utility.SysConst;
import com.sinosoft.utility.Reflections;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.vschema.LJAGetSet;
import com.sinosoft.lis.db.LJAGetDB;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.utility.VData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class Ljfiget {
    MMap mMMap = new MMap();
    VData mSubmitData = new VData();

    public Ljfiget() {
    }

    public static void main(String[] args)
    {

        String sql = "select * from ljaget a where a.confdate>='2008-08-29' and not exists (select 1 from ljfiget b where b.actugetno=a.actugetno ) with ur";
        LJAGetDB tLJAGetDB = new LJAGetDB();
        LJAGetSet tLJAGetSet = tLJAGetDB.executeQuery(sql);
        Ljfiget tLjfiget = new Ljfiget();
        tLjfiget.dealDate(tLJAGetSet);
    }

    private boolean dealDate(LJAGetSet tLJAGetSet)
    {
        for (int i = 1; i <= tLJAGetSet.size(); i++) {
            LJAGetSchema tLJAGetSchema = tLJAGetSet.get(i).getSchema();

            LJFIGetSet tLJFIGetSet = new LJFIGetSet();
            LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();
            Reflections ref = new Reflections();
            ref.transFields(tLJFIGetSchema, tLJAGetSchema);
            tLJFIGetSchema.setGetMoney(tLJAGetSchema.getSumGetMoney());
            tLJFIGetSchema.setPolicyCom(tLJAGetSchema.getManageCom());
            tLJFIGetSet.add(tLJFIGetSchema);
            mMMap.put(tLJFIGetSet, SysConst.INSERT);
        }
        PubSubmit tPubSubmit = new PubSubmit();
        mSubmitData.add(mMMap);
        if (!tPubSubmit.submitData(mSubmitData, "")) {
            tPubSubmit.mErrors.addOneError("生成财务暂收失败");
            return false ;
        }
        return true ;
    }
}
