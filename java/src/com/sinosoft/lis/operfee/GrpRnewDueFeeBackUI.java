package com.sinosoft.lis.operfee;

import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.schema.LJSPayBSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

public class GrpRnewDueFeeBackUI {

	  public CErrors mCErrors = new CErrors();
	    private GrpRnewDueFeeBackBL tGrpDueFeeBackBL = new GrpRnewDueFeeBackBL();
	    public GrpRnewDueFeeBackUI() {
	    }
	    public boolean submitData(VData cInputData, String cOperate){
	       if(!tGrpDueFeeBackBL.submitData(cInputData,cOperate))
	       {
	           mCErrors.copyAllErrors(tGrpDueFeeBackBL.mErrors);
	           return false;
	       }
	       else
	           return true;
	    }
	    public static void main(String[] arg)
	    {
	        GrpRnewDueFeeBackUI test = new GrpRnewDueFeeBackUI();
	        VData tVData = new VData();
	        LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
	        tLJSPayBSchema.setGetNoticeNo(arg[0]);
	        tVData.add(0,tLJSPayBSchema);
	        GlobalInput tG = new GlobalInput();
	        tG.Operator = "qulq";
	        tG.ManageCom = "86";
	        tVData.add(1,tG);
	        test.submitData(tVData,"");
	        System.out.println(test.mCErrors.getErrContent());
	    }

	}
