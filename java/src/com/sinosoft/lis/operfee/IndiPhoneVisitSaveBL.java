package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;
import java.lang.String;
import com.sinosoft.task.Task;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:
 *
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author YangYalin
 * @version 1.3
 */

public class IndiPhoneVisitSaveBL
{

    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = new GlobalInput();
    private LGPhoneHastenSchema mLGPhoneHastenSchema = null;

    private String mFinishType = null;  //回访结果

    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    /** 数据表  保存数据*/
    private MMap map = new MMap();

    public IndiPhoneVisitSaveBL()
    {
    }

    /**
     * 存储回访处理结果集
     * @param cInputData VData：传入的数据待处理数据机和，包含：
     * 1、LGPhoneHastenSchema对象，回访结果结果，
     * 需要WorkNo,GetNoticeNo,FinishType,OldPayMode,OldPayDate,OldBankCode,OldBackAccNo，均为回访后的结果
     * 2、GlobalInput对象，完整的用户信息
     * @param cOperate String：此为""
     * @return MMap:处理后的数据集合
     */
    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }
        if(!checkData())
        {
            return null;
        }

        //进行业务处理
        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * 校验录入的数据是否符合业务需求
     * 1、如为银行转账则银行账号、银行不能为空
     * 2、结案的催缴不可更改答案
     * @return boolean：验证通过true，否则false
     */
    private boolean checkData()
    {
        if(mFinishType.equals(FeeConst.HASTEN_FINISHTYPE_REDATE)
            && (mLGPhoneHastenSchema.getOldPayMode() == null)
             || mLGPhoneHastenSchema.getOldPayMode().equals("4"))
        {
            if(mLGPhoneHastenSchema.getOldBackAccNo() == null
               || mLGPhoneHastenSchema.getOldBackAccNo().equals(""))
            {
                mErrors.addOneError("请录入银行帐号");
                return false;
            }
            if(mLGPhoneHastenSchema.getOldBankCode() == null
               || mLGPhoneHastenSchema.getOldBankCode().equals(""))
            {
                mErrors.addOneError("请录入银行编码");
                return false;
            }
        }

        LGPhoneHastenDB tLGPhoneHastenDB = new LGPhoneHastenDB();
        tLGPhoneHastenDB.setWorkNo(mLGPhoneHastenSchema.getWorkNo());
        if(!tLGPhoneHastenDB.getInfo())
        {
            mErrors.addOneError("查询催缴任务失败");
            return false;
        }
        if(tLGPhoneHastenDB.getFinishType() != null
           && (tLGPhoneHastenDB.getFinishType()
               .equals(FeeConst.HASTEN_FINISHTYPE_CANCEL)
               || tLGPhoneHastenDB.getFinishType()
               .equals(FeeConst.HASTEN_FINISHTYPE_REDATE)))
        {
            mErrors.addOneError("已录入回访结果，不能再次录入");
            return false;
        }

        return true;
    }

    /**
     * 存储回访处理结果集
     * @param cInputData VData：传入的数据待处理数据机和，包含：
     * 1、LGPhoneHastenSchema对象，回访结果结果，
     * 需要WorkNo,GetNoticeNo,FinishType,OldPayMode,OldPayDate,OldBankCode,OldBackAccNo，均为回访后的结果
     * 2、GlobalInput对象，完整的用户信息
     * @param cOperate String：此为""
     * @return MMap:处理后的数据集合
     */
    public boolean submitData(VData cInputData, String cOperate)
    {

        MMap map = getSubmitMap(cInputData, cOperate);
        if((map == null || map.size() == 0) && mErrors.needDealError())
        {
            return false;
        }

        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if(tPubSubmit.submitData(data, "") == false)
        {
            mErrors.addOneError("更新数据失败");
            return false;
        }

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     * submitData中传入的对象
     * @return boolean: 如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData data)
    {
        mGlobalInput = ((GlobalInput) data
                        .getObjectByObjectName("GlobalInput", 0));
        mLGPhoneHastenSchema = (LGPhoneHastenSchema) data.
                               getObjectByObjectName("LGPhoneHastenSchema", 0);
        TransferData tTransferData = (TransferData) data.
                                     getObjectByObjectName("TransferData", 0);

        if(mGlobalInput == null || mLGPhoneHastenSchema == null
           || mLGPhoneHastenSchema.getOldPayMode() == null
           || mLGPhoneHastenSchema.getOldPayMode().equals("")
           || mLGPhoneHastenSchema.getWorkNo() == null
           || mLGPhoneHastenSchema.getWorkNo().equals("")
           || mLGPhoneHastenSchema.getGetNoticeNo() == null
           || mLGPhoneHastenSchema.getGetNoticeNo().equals("")
              || tTransferData == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpPhoneVisitOverBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }

        mFinishType = (String) tTransferData.getValueByName("OperatorType");
        if(mFinishType == null)
        {
            mErrors.addOneError("请录入回访结果");
            return false;
        }

        return true;
    }

    /**
     * 处理回访结果
     * @return boolean：处理成功true，否则false
     */
    private boolean dealData()
    {
        String bankCode = "";
        String bankAccNo = "";
        if(mLGPhoneHastenSchema.getOldPayMode() != null
           && mLGPhoneHastenSchema.getOldPayMode().equals("4"))
        {
            bankCode = mLGPhoneHastenSchema.getOldBankCode();
            bankAccNo = mLGPhoneHastenSchema.getOldBackAccNo();
        }

        //应收总表, 主要是更新一些字段信息
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setGetNoticeNo(mLGPhoneHastenSchema.getGetNoticeNo());
        if(!tLJSPayDB.getInfo())
        {
            CError.buildErr(this, "没有查询到应收记录号"
                            + mLGPhoneHastenSchema.getGetNoticeNo()
                            + "在应收总表中的信息！");
            return false;
        }
        tLJSPayDB.setPayDate(mLGPhoneHastenSchema.getOldPayDate());
        tLJSPayDB.setBankOnTheWayFlag("");
        tLJSPayDB.setBankCode(bankCode);
        tLJSPayDB.setBankAccNo(bankAccNo);
        //qulq 070105  允许发送银行
        if((tLJSPayDB.getCanSendBank()!=null)&&(!tLJSPayDB.getCanSendBank().equals("3")))
        {
            tLJSPayDB.setCanSendBank("0");
        }
        tLJSPayDB.setOperator(mGlobalInput.Operator);
        tLJSPayDB.setModifyDate(this.CurrentDate);
        tLJSPayDB.setModifyTime(this.CurrentTime);
        map.put(tLJSPayDB.getSchema(), "UPDATE");


        //应收总表备份表, 主要是更新一些字段信息
        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        tLJSPayBDB.setGetNoticeNo(mLGPhoneHastenSchema.getGetNoticeNo());
        if(!tLJSPayBDB.getInfo())
        {
            CError.buildErr(this, "没有查询到应收记录号"
                            + mLGPhoneHastenSchema.getGetNoticeNo()
                            + "在应收总表备份表中的信息！");
            return false;
        }
        tLJSPayBDB.setPayDate(mLGPhoneHastenSchema.getOldPayDate());
        tLJSPayBDB.setBankOnTheWayFlag("");
        tLJSPayBDB.setBankCode(bankCode);
        tLJSPayBDB.setBankAccNo(bankAccNo);
        tLJSPayBDB.setOperator(mGlobalInput.Operator);
        tLJSPayBDB.setModifyDate(this.CurrentDate);
        tLJSPayBDB.setModifyTime(this.CurrentTime);
        map.put(tLJSPayBDB.getSchema(), "UPDATE");


        //个人应收
        LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
        tLJSPayPersonDB.setGetNoticeNo(mLGPhoneHastenSchema.getGetNoticeNo());
        LJSPayPersonSet tLJSPayPersonSet = new LJSPayPersonSet();
        tLJSPayPersonSet = tLJSPayPersonDB.query();
        if(tLJSPayPersonSet.size() == 0)
        {
            CError.buildErr(this, "没有查询到应收记录号"
                            + mLGPhoneHastenSchema.getGetNoticeNo()
                            + "在个人应收表中的信息！");
            return false;
        }
        for(int i = 1; i <= tLJSPayPersonSet.size(); i++)
        {
            tLJSPayPersonSet.get(i).setPayDate(mLGPhoneHastenSchema.getOldPayDate());
            tLJSPayPersonSet.get(i).setBankOnTheWayFlag("");
            tLJSPayPersonSet.get(i).setBankCode(bankCode);
            tLJSPayPersonSet.get(i).setBankAccNo(bankAccNo);
            tLJSPayPersonSet.get(i).setOperator(mGlobalInput.Operator);
            tLJSPayPersonSet.get(i).setModifyDate(this.CurrentDate);
            tLJSPayPersonSet.get(i).setModifyTime(this.CurrentTime);
            map.put(tLJSPayPersonSet.get(i).getSchema(), "UPDATE");
        }

        //个人应收备份表
        LJSPayPersonBDB tLJSPayPersonBDB = new LJSPayPersonBDB();
        tLJSPayPersonBDB.setGetNoticeNo(mLGPhoneHastenSchema.getGetNoticeNo());
        LJSPayPersonBSet tLJSPayPersonBSet = new LJSPayPersonBSet();
        tLJSPayPersonBSet = tLJSPayPersonBDB.query();
        if(tLJSPayPersonBSet.size() == 0)
        {
            CError.buildErr(this, "没有查询到应收记录号"
                            + mLGPhoneHastenSchema.getGetNoticeNo()
                            + "在个人应收表中的信息！");
            return false;
        }
        for(int i = 1; i <= tLJSPayPersonBSet.size(); i++)
        {
            tLJSPayPersonBSet.get(i).setPayDate(mLGPhoneHastenSchema.getOldPayDate());
            tLJSPayPersonBSet.get(i).setBankOnTheWayFlag("");
            tLJSPayPersonBSet.get(i).setBankCode(bankCode);
            tLJSPayPersonBSet.get(i).setBankAccNo(bankAccNo);
            tLJSPayPersonBSet.get(i).setOperator(mGlobalInput.Operator);
            tLJSPayPersonBSet.get(i).setModifyDate(this.CurrentDate);
            tLJSPayPersonBSet.get(i).setModifyTime(this.CurrentTime);
            map.put(tLJSPayPersonBSet.get(i).getSchema(), "UPDATE");
        }

        String sql = "  update LGPhoneHasten "
                     + "set FinishType = '" + this.mFinishType + "', "
                     + "   operator = '" + this.mGlobalInput.Operator + "', "
                     + "   modifyDate = '" + this.CurrentDate + "', "
                     + "   modifyTime = '" + this.CurrentTime + "' "
                     + "where workNo = '" + mLGPhoneHastenSchema.getWorkNo() + "' "
                     + "   and getNoticeNo = '"
                     + mLGPhoneHastenSchema.getGetNoticeNo() + "' ";
        map.put(sql, "UPDATE");

        return true;
    }

    public static void main(String[] args)
    {
        GlobalInput tGI = new GlobalInput();

        //合同号

        LGPhoneHastenSchema tLGPhoneHastenSchema = new LGPhoneHastenSchema();
        tLGPhoneHastenSchema.setWorkNo("20060829000006");
        tLGPhoneHastenSchema.setOldPayDate("2006-08-30");
        tLGPhoneHastenSchema.setOldPayMode("1");
        tLGPhoneHastenSchema.setOldBankCode("0153");
        tLGPhoneHastenSchema.setOldBackAccNo("2502120701010697781");
        tLGPhoneHastenSchema.setGetNoticeNo("31000000592");

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("OperatorType", "0");

        VData tVData = new VData();
        tVData.addElement(tLGPhoneHastenSchema);
        tVData.addElement(tGI);
        tVData.addElement(tTransferData);

        IndiPhoneVisitSaveBL tIndiPhoneVisitSaveBL = new IndiPhoneVisitSaveBL();
        if(!tIndiPhoneVisitSaveBL.submitData(tVData, ""))
        {
            System.out.println(tIndiPhoneVisitSaveBL.mErrors.getErrContent());
        }
    }
}
