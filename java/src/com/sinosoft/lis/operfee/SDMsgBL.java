package com.sinosoft.lis.operfee;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.ChangeCodeBL;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.rmi.*;
import com.sinosoft.lis.message.*;
import java.util.*;

/**
 * <p>Title: 客户山东烟台短信通知</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2013</p
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class SDMsgBL {
    private GlobalInput mG = new GlobalInput();
    private String mCurrentDate = PubFun.getCurrentDate();
    private ExeSQL mExeSQL = new ExeSQL();
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    private boolean getInputData(VData cInputData) 
    {
    	mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        return true;
    }

    public boolean submitData(VData cInputData, String cOperate) {
        cInputData = (VData) cInputData.clone();

        if (!getInputData(cInputData)) 
        {
        	mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0));
        	if(mG.Operator==null || mG.Operator.equals("") || mG.Operator.equals("null"))
        	{
        		mG.Operator="001";
        	}
        	if(mG.ManageCom==null || mG.ManageCom.equals("") || mG.ManageCom.equals("null"))
        	{
        		mG.ManageCom="86";
        	}
            return false;
        }
        if (cOperate != null && cOperate.trim().equals("SDMSG")) {
            sendMsg();
        } else {
            mErrors.addOneError(new CError("不支持的操作字符串！"));
            return false;
        }
        return true;
    }

    private boolean sendMsg() {
        System.out.println("山东烟台各种短信通知批处理开始......。。。。");
        SMSClient smsClient = new SMSClient();
        
        Vector vec = new Vector();
        vec = getMessage();
        Vector tempVec = new Vector(); //临时变量--实际发短信时传的参数
        //拆分Vector
        System.out.println("---------vec="+vec.size());

        if (!vec.isEmpty()) 
        {
        	for(int i=0;i<vec.size();i++)
            {
        		tempVec.clear();
            	tempVec.add(vec.get(i));
            	
            	SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessages对象，该对象对应于上文下行短信格式中的Messages元素
                msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                msgs.setTaskValue(SMSClient.mes_taskVlaue10);
                msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                
            	msgs.setMessages((SmsMessageNew[]) tempVec.toArray(new SmsMessageNew[tempVec.size()])); //设置该批短信的每一条短信，一批短信可以包含多条短信
            	smsClient.sendSMS(msgs);
            }
        } 
        else 
        {
            System.out.print("续期无符合条件的短信！");
        }
        System.out.println("客户续期短信通知批处理正常结束......");
        return true;
    }

    private Vector getMessage() {
    	System.out.println("进入获取信息 getMessage()------------------------");
        Vector tVector = new Vector();
        ///宽限期短信提醒(长险)--------------------------------
        String tSQLkx =
        	"select a.contNo, "   //保单号
//        	+"(select mobile from lcaddress x, lcappnt y  where x.customerno = y.appntno and x.addressno = y.addressno  and y.contno = a.contno fetch first 1 rows only),  "//客户手机号码
        	+" mobile(a.contno), "
        	+" b.sumduepaymoney, "       //应缴金额
//        	+"(select max(GracePeriod) from LMRiskPay rp, lcpol cp where cp.contno = a.contno  and rp.RiskCode = cp.RiskCode  and (a.stateflag = '1' or a.StateFlag is null)  and a.AppFlag = '1'),"//宽限期到期日
        	+ "(a.payToDate + 60 days),"
        	+"b.getNoticeNo, "
        	+"b.serialNo, "
			+"a.ManageCom "
        	+"from lccont a, ljspayb b "
            +"where a.contNo = b.OtherNo "
            +"and (a.payToDate + 31 days)  = current date "
            +"and b.dealstate='0' "
            +"and exists (select 1 from lmriskapp r, lcpol p where p.contno = a.contno and r.riskcode = p.riskcode  and r.riskperiod = 'L') "
            +"and a.payintv <> 0 "
//            +"and a.DueFeeMsgFlag is not null and a.DueFeeMsgFlag = 'N' "
            +" and a.DueFeeMsgFlag is not null and a.DueFeeMsgFlag = 'Y' "
            +"and not exists (select 1 from LCCSpec  where contno=a.contno and SpecType='kxL' and EndorsementNo=b.getNoticeNo ) "
            +"and a.ManageCom in ('86370600','86371000') with ur";
        System.out.println("宽限期短信提醒(长险)---------------"+tSQLkx);
        SSRS tMsgSuccSSRS =mExeSQL.execSQL(tSQLkx);
        for (int i = 1; i <= tMsgSuccSSRS.getMaxRow(); i++) 
        {
        	String tContNo = tMsgSuccSSRS.GetText(i, 1);
        	String tCustomerMobile = tMsgSuccSSRS.GetText(i, 2);
        	String tPayMoney = tMsgSuccSSRS.GetText(i,3);
        	String tLastDate = tMsgSuccSSRS.GetText(i,4);
        	String tGetNoticeNo = tMsgSuccSSRS.GetText(i,5);
        	String tSerialNo = tMsgSuccSSRS.GetText(i,6);
        	String tManageCom = tMsgSuccSSRS.GetText(i,7);
        	
        	if (tContNo == null || "".equals(tContNo)
					|| "null".equals(tContNo)) {
				continue;
		    }
		    if (tCustomerMobile == null || "".equals(tCustomerMobile)
					|| "null".equals(tCustomerMobile)) {
				continue;
		    }
		    if (tPayMoney == null || "".equals(tPayMoney)
					|| "null".equals(tPayMoney)) {
				continue;
		    }

		    if (tLastDate == null || "".equals(tLastDate) ||
                    "null".equals(tLastDate)) {
                    continue;
                }
		    if (tGetNoticeNo == null || "".equals(tGetNoticeNo) ||
                    "null".equals(tGetNoticeNo)) {
                    continue;
                }
		    if (tSerialNo == null || "".equals(tSerialNo) ||
                    "null".equals(tSerialNo)) {
                    continue;
                }
		    if (tManageCom == null || "".equals(tManageCom) ||
                    "null".equals(tManageCom)) {
                    continue;
                }
            
		   //短信内容
           String tMSGContents=""; 
            
            SmsMessageNew tKXMsg = new SmsMessageNew(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
            tMSGContents = "（人保健康）温馨提示：您所投保的保单号为"
            				+tContNo+"的保单已经进入宽限期，本次需缴费金额为"
            				+tPayMoney+"元，宽限期到期后保单将终止，为保障您的利益，请您于"
            				+tLastDate+"日前及时缴费。";
            tKXMsg.setReceiver(tCustomerMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
            tKXMsg.setContents(tMSGContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
            
            //direct by lc 2017-3-27 机构
            tKXMsg.setOrgCode(tManageCom); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
            
            tVector.add(tKXMsg);
        		
                                         
            // 在LCCSpec表中插入值  ********************************************************  
        	LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
            tLCCSpecSchema.setGrpContNo(BQ.GRPFILLDATA);
            tLCCSpecSchema.setContNo(tContNo);
            tLCCSpecSchema.setProposalContNo(BQ.GRPFILLDATA);               
            tLCCSpecSchema.setPrtSeq(tSerialNo);// 放 ljspayb   SerialNo
            String tLimit = PubFun.getNoLimit(tManageCom);
	           String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
	           tLCCSpecSchema.setSerialNo(serNo); //流水号                
            tLCCSpecSchema.setEndorsementNo(tGetNoticeNo);
            tLCCSpecSchema.setSpecType("kxL"); //kxD-短险宽限期发送短息
            tLCCSpecSchema.setSpecCode("001");////001-投保人 002-被保人 003-业务员
            tLCCSpecSchema.setSpecContent(tMSGContents+"客户手机号："+ tCustomerMobile);
            tLCCSpecSchema.setOperator(mG.Operator);
            tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
            tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
            tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
            tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
            tLCCSpecSchema.getDB().insert();
        }
//  
//        //宽限期短信提醒（短险）-------------------------------------------------30天
//        
        String ttSQLkxdq = 
        	"select a.contNo, "   //保单号
//        	+"(select mobile from lcaddress x, lcappnt y  where x.customerno = y.appntno and x.addressno = y.addressno  and y.contno = a.contno fetch first 1 rows only),  "//客户手机号码
        	+" mobile(a.contno), "
        	+" b.sumduepaymoney, "       //应缴金额
//        	+"(select max(GracePeriod) from LMRiskPay rp, lcpol cp where cp.contno = a.contno  and rp.RiskCode = cp.RiskCode  and (a.stateflag = '1' or a.StateFlag is null)  and a.AppFlag = '1'),"//宽限期到期日
        	+ " (a.payToDate + 30 days),"
        	+"b.getNoticeNo, "
        	+"b.serialNo, "
			+"a.ManageCom "
        	+"from lccont a, ljspayb b "
            +"where a.contNo = b.OtherNo  "
            +"and (a.payToDate + 16 days)  = current date "
            +"and b.dealstate='0' "
            +"and exists (select 1 from lmriskapp r, lcpol p where p.contno = a.contno and r.riskcode = p.riskcode  and r.riskperiod <> 'L') "
            +"and a.payintv <> 0 "
//            +"and a.DueFeeMsgFlag is not null and a.DueFeeMsgFlag = 'N' "
            +" and a.DueFeeMsgFlag is not null and a.DueFeeMsgFlag = 'Y' "
            +"and not exists (select 1 from LCCSpec  where contno=a.contno and SpecType='kxL' and EndorsementNo=b.getNoticeNo ) "
            +"and a.ManageCom in ('86370600','86371000') "
            + "with ur";
        System.out.println("宽限期短信提醒（短险）------------"+ttSQLkxdq);
        SSRS tKXDQMsgSSRS = mExeSQL.execSQL(ttSQLkxdq);
//        tKXDQMsgSSRS.setMaxRow(2);
        for(int i=1;i<=tKXDQMsgSSRS.getMaxRow();i++){
        	
        	String tContNo = tKXDQMsgSSRS.GetText(i, 1);
        	String tCustomerMobile = tKXDQMsgSSRS.GetText(i, 2);
        	String tShouldPay = tKXDQMsgSSRS.GetText(i, 3);
        	String tLastDay = tKXDQMsgSSRS.GetText(i,4);
        	String tGetNoticeNo = tKXDQMsgSSRS.GetText(i,5);
        	String tSerialNo = tKXDQMsgSSRS.GetText(i,6);
        	String tManageCom = tKXDQMsgSSRS.GetText(i,7);
        	
        	 if (tContNo == null || "".equals(tContNo) ||
                     "null".equals(tContNo)) {
                     continue;
                 }
             
             if (tCustomerMobile == null || "".equals(tCustomerMobile) ||
                     "null".equals(tCustomerMobile)) {
                     continue;
                 }
             if (tShouldPay == null || "".equals(tShouldPay) ||
                     "null".equals(tShouldPay)) {
                     continue;
                 }
         	
         	if (tLastDay == null || "".equals(tLastDay) ||
                     "null".equals(tLastDay)) {
                     continue;
                 }
         	if (tGetNoticeNo == null || "".equals(tGetNoticeNo) ||
                    "null".equals(tGetNoticeNo)) {
                    continue;
                }
         	if (tSerialNo == null || "".equals(tSerialNo) ||
                    "null".equals(tSerialNo)) {
                    continue;
                }
        	
        	if (tManageCom == null || "".equals(tManageCom) ||
                    "null".equals(tManageCom)) {
                    continue;
                }
//         	
         	String tCustomerContents = "";
//				发送给客户的短信            
         	SmsMessageNew tCustomerMsg = new SmsMessageNew(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
            tCustomerContents = "（人保健康）温馨提示：您所投保的保单号为"+tContNo+"的保单已经进入续保期，本次需缴费金额为"+tShouldPay+"元，续保期到期后保单将终止，为保障您的利益，请您于"+tLastDay+"日前及时缴费。";
            tCustomerMsg.setReceiver(tCustomerMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
            //tCustomerMsg.setReceiver("13581900861");
            tCustomerMsg.setContents(tCustomerContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
            
            //direct by lc 2017-3-27 机构
            tCustomerMsg.setOrgCode(tManageCom); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
            //添加SmsMessage对象
            tVector.add(tCustomerMsg);
                
//        	在LCCSpec表中插入值  ********************************************************  
        	LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
            tLCCSpecSchema.setGrpContNo(BQ.GRPFILLDATA);
            tLCCSpecSchema.setContNo(tContNo);
            tLCCSpecSchema.setProposalContNo(BQ.GRPFILLDATA);               
            tLCCSpecSchema.setPrtSeq(tSerialNo);// 放 ljspayb getNoticeNo 
            String tLimit = PubFun.getNoLimit(tManageCom);
	           String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
	           tLCCSpecSchema.setSerialNo(serNo); //流水号                
            tLCCSpecSchema.setEndorsementNo(tGetNoticeNo);//应缴保费
            tLCCSpecSchema.setSpecType("kxL"); //kxD-短险宽限期发送短息
            tLCCSpecSchema.setSpecCode("001");////001-投保人 002-被保人 003-业务员
            tLCCSpecSchema.setSpecContent(tCustomerContents+"客户手机号："+ tCustomerMobile);
            tLCCSpecSchema.setOperator(mG.Operator);
            tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
            tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
            tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
            tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
            tLCCSpecSchema.getDB().insert();
        }
        			
 
////  当保单做完解约后，在保全结案之后24小时内将发送对应短信给客户。-------------------------------
   
 
       String ttSQLct = "select a.confdate, " //结案日期
    	   			  +"a.getMoney, " //解约退保费
    	   			  +"b.contNo, "  //保单
//    	   			  +"(select  mobile  from lcaddress x, lbappnt y  where x.customerno = y.appntno and x.addressno = y.addressno and y.contno = b.contno fetch first 1 rows only), "//手机号码
    	   			  +"mobile(b.contno),"
    	   			  +"b.EDORNO, "
    	   			  +"b.ManageCom "
    	   			  +"from lpedorapp a, LPEDORITEM b "
    	   			  +"where a.edoracceptno = b.EDORNO "
    	   			  +"and b.edorType in ('CT','WT') "
    	   			  +"and a.edorstate = '0'   "
    	   			  +"and a.confDate = current date "
    	   			  +"and not exists(select 1 from LCCSpec  where contno=b.contno and SpecType='jy' and EndorsementNo=b.EDORNO ) "
//    	   			  //+"and exists (select 1 from lccont where contno = b.contno and DueFeeMsgFlag is not null and DueFeeMsgFlag = 'N')"
    	   			  +"and b.ManageCom in ('86370600','86371000') with ur ";
       System.out.println("保单做完解约后-------------" + ttSQLct);
        SSRS tMsgNotifySSRS =mExeSQL.execSQL(ttSQLct);
        
        for (int i = 1; i <= tMsgNotifySSRS.getMaxRow(); i++) 
        {
        	String tEdorEndDate = tMsgNotifySSRS.GetText(i, 1);
        	String tReturnMoney = tMsgNotifySSRS.GetText(i,2);
        	String tContNo = tMsgNotifySSRS.GetText(i, 3);
        	String tCustomerMobile = tMsgNotifySSRS.GetText(i, 4);
        	String tEdorNo = tMsgNotifySSRS.GetText(i, 5);
        	String tManageCom = tMsgNotifySSRS.GetText(i, 6);
        	
        	if (tEdorEndDate == null || "".equals(tEdorEndDate) ||
                    "null".equals(tEdorEndDate)) {
                    continue;
                }
        	
        	if (tReturnMoney == null || "".equals(tReturnMoney) ||
                    "null".equals(tContNo)) {
                    continue;
                }

            if (tContNo == null || "".equals(tContNo) ||
                    "null".equals(tContNo)) {
                    continue;
                }
            
            if (tCustomerMobile == null || "".equals(tCustomerMobile) ||
                    "null".equals(tCustomerMobile)) {
                    continue;
                }
            if (tEdorNo == null || "".equals(tEdorNo) ||
                    "null".equals(tEdorNo)) {
                    continue;
                }
            if (tManageCom == null || "".equals(tManageCom) ||
                    "null".equals(tManageCom)) {
                    continue;
                }
        	
            	String tCustomerContents = "";
//					发送给客户的短信            
            		SmsMessageNew tCustomerMsg = new SmsMessageNew(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
                    tCustomerContents = "（人保健康）温馨提示：客户您好，您所投保的"+ tContNo
                    				    +"号保单已于"+tEdorEndDate+"日办理退保手续，退保金额为"+Math.abs(Double.parseDouble(tReturnMoney))+"元，退保金将转入约定账户，请您及时查收。";
                    tCustomerMsg.setReceiver(tCustomerMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
                    tCustomerMsg.setContents(tCustomerContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
                    
                    
                    //direct by lc 2017-3-27 机构
                    int Mglength = tManageCom.length();
                    if(Mglength==8){
                    	tCustomerMsg.setOrgCode(tManageCom);
                    } else {
                    	tCustomerMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                    } 
                    
                    tVector.add(tCustomerMsg);
               
            	//银行转账方式缴费的
            	
//            	在LCCSpec表中插入值  ********************************************************  
            	 LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
                 tLCCSpecSchema.setGrpContNo(BQ.GRPFILLDATA);
                 tLCCSpecSchema.setContNo(tContNo);
                 tLCCSpecSchema.setProposalContNo(BQ.GRPFILLDATA);               
                 tLCCSpecSchema.setPrtSeq(tEdorNo);// 放 LPEDORITEM EdorNo 
                 String tLimit = PubFun.getNoLimit(tManageCom);
     	           String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
     	           tLCCSpecSchema.setSerialNo(serNo); //流水号                
                 tLCCSpecSchema.setEndorsementNo(tEdorNo);//
                 tLCCSpecSchema.setSpecType("jy"); //jy-解约结案后发送短息
                 tLCCSpecSchema.setSpecCode("001");////001-投保人 002-被保人 003-业务员
                 tLCCSpecSchema.setSpecContent(tCustomerContents+"客户手机号："+ tCustomerMobile);
                 tLCCSpecSchema.setOperator(mG.Operator);
                 tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
                 tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
                 tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
                 tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
                 tLCCSpecSchema.getDB().insert();
            
            }
       
////  满期给付短信提醒，收费成功的短信------------------------------------------------
//        
               String ttSQLsucc = "select a.contNo, "
			                	+ "e.sumGetMoney, "
//			                	+"(select mobile from lcaddress x, lcappnt y  where x.customerno = y.appntno and x.addressno = y.addressno  and y.contno = a.contno fetch first 1 rows only), "
			                	+" mobile(a.contno), "
			                	+"e.getnoticeno, "
			                	+"a.ManageCom "
			                	+"from lccont a, lcget b, ljsgetdraw d, ljaget e "
			                	+"where a.contno = b.contno "
			                	+"and a.contNo = d.contNo "
			                	+"and d.getnoticeno = e.actugetno "
			                	+"and exists (select 1 from lmdutygetalive where getdutycode = b.getdutycode and discntflag != '9' and getdutykind = '0') "
			                	+"and not exists (select 1 from LCCSpec  where contno=a.contno and SpecType='mqH' and EndorsementNo=e.getnoticeno ) "
//			                	+"and  a.DueFeeMsgFlag is not null and a.DueFeeMsgFlag = 'N' "
			                	+"and e.confDate = current date "
			                	+"and a.ManageCom in ('86370600','86371000') with ur";
               
               System.out.println("满期给付后短息-----"+ttSQLsucc);

                SSRS ttMsgSuccSSRS =mExeSQL.execSQL(ttSQLsucc);
                for (int i = 1; i <= ttMsgSuccSSRS.getMaxRow(); i++) 
                {
                	String tContNo = ttMsgSuccSSRS.GetText(i, 1);
                	String tsumGetMoney = ttMsgSuccSSRS.GetText(i, 2);
                	String tCustomerMobile = ttMsgSuccSSRS.GetText(i, 3);
                	String tGetNoticeNo = ttMsgSuccSSRS.GetText(i, 4);
                	String tManageCom = ttMsgSuccSSRS.GetText(i, 5);
                	
                    if (tContNo == null || "".equals(tContNo) ||
                        "null".equals(tContNo)) {
                        continue;
                    }
                    
                    if (tsumGetMoney == null || "".equals(tsumGetMoney) ||
                            "null".equals(tsumGetMoney)) {
                            continue;
                        }

                    if (tCustomerMobile == null || "".equals(tCustomerMobile) ||
                            "null".equals(tCustomerMobile)) {
                            continue;
                        }
                    if (tGetNoticeNo == null || "".equals(tGetNoticeNo) ||
                            "null".equals(tGetNoticeNo)) {
                            continue;
                        }
                    if (tManageCom == null || "".equals(tManageCom) ||
                            "null".equals(tManageCom)) {
                            continue;
                        }
                    //add wdl 无法从上面得到满期日期，因此添加
                    String sqlw = "select lastgettodate from ljsgetdraw where contno='"+tContNo+"' fetch first 1 rows only with ur " ;
                    SSRS ttMDSSRS =mExeSQL.execSQL(sqlw);
                    String tMDate = ttMDSSRS.GetText(1, 1);
                        //发送给客户的短信            
                    	SmsMessageNew tCustomerMsg = new SmsMessageNew(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
//                        String tCustomerContents = "（人保健康）温馨提示：客户您好，您所投保的"+tContNo+"保单的保险金"+tsumGetMoney+"元将转入合同约定账户，请您及时查收。";
                        String tCustomerContents = "（人保健康）温馨提示：客户您好，您所投保的"+tContNo+"保单将于"+tMDate+ "日到期，请您"
                        		+ "需携带以下材料到我公司办理相关手续：投保人/被保险人的身份证原件、保险合同、发票、保险金领取资格人的银行账户。如有疑问请致电95591。";
                        tCustomerMsg.setReceiver(tCustomerMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
                        tCustomerMsg.setContents(tCustomerContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素

                        //direct by lc 2017-3-27 机构
                        tCustomerMsg.setOrgCode(tManageCom); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素

                        tVector.add(tCustomerMsg);
                        
//                        在LCCSpec表中插入值  ******************************************************** 
                        LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
                        tLCCSpecSchema.setGrpContNo(BQ.GRPFILLDATA);
                        tLCCSpecSchema.setContNo(tContNo);
                        tLCCSpecSchema.setProposalContNo(BQ.GRPFILLDATA);               
                        tLCCSpecSchema.setPrtSeq(tGetNoticeNo);// 放 ljaget getnoticeNo 
                        String tLimit = PubFun.getNoLimit(tManageCom);
            	           String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
            	           tLCCSpecSchema.setSerialNo(serNo); //流水号                
                        tLCCSpecSchema.setEndorsementNo(tGetNoticeNo);//
                        tLCCSpecSchema.setSpecType("mqH"); //mqsucc-满期给付后发送短息
                        tLCCSpecSchema.setSpecCode("001");////001-投保人 002-被保人 003-业务员
                        tLCCSpecSchema.setSpecContent(tCustomerContents+"客户手机号："+ tCustomerMobile);
                        tLCCSpecSchema.setOperator(mG.Operator);
                        tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
                        tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
                        tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
                        tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
                        tLCCSpecSchema.getDB().insert();
                    
                }

        
//    客户资料变更后短信提醒----------------------------------

                String ttSQLzlbg = "select A.confdate, " //结案日期
                					 +"B.contNo,"
                					 +"(case when B.edortype = 'AE' then (select " 
//                					 +" mobile " 
                					 +" (case when mobile is not null then mobile when phone is not null then phone else homephone end) "	//依次取手机、联系电话、家庭电话号码
                					 +"from lcaddress x1, lpappnt y1 where x1.customerno = y1.appntno "
                					 + " and x1.addressno = y1.addressno and y1.edorno = b.edorno and y1.contno = b.contno fetch first 1 rows only) else "
//                					 + " (select mobile from lcaddress x, lcappnt y where x.customerno = y.appntno and x.addressno = y.addressno and y.contno = b.contno fetch first 1 rows only)"
                					 +" mobile(b.contno) end), " 
                					 +"B.edorAcceptNo, "
                					 +"B.edorType, "
                					 +"B.manageCom ,"
                					 +"B.edorNo "
                					 +"from lpedorapp A, lpedoritem B "
                					 +"where A.edoracceptno = B.edorNo  " 
                					 +"and B.edorType in ('CM', 'AE','CC') "
                					 +"and A.edorstate = '0'   "
                					 +"and A.confDate = current date "
                					 +"and b.ManageCom in ('86370600','86371000') "
                					 //+"and exists (select 1 from lccont where contno = B.contno and DueFeeMsgFlag is not null and DueFeeMsgFlag = 'N') "
                					 +"and not exists (select 1 from LCCSpec  where contno=B.contno and SpecType='zlbg' and EndorsementNo=B.edorNo) "
    			                	 + "with ur "; 
                   System.out.println("客户资料变更后:---------"+ttSQLzlbg);

                   SSRS ttShiXiaoMsgSuccSSRS =mExeSQL.execSQL(ttSQLzlbg);
                   for (int i = 1; i <= ttShiXiaoMsgSuccSSRS.getMaxRow(); i++) 
                   {
                	  String tConfdate = ttShiXiaoMsgSuccSSRS.GetText(i, 1);
                      String tContNo = ttShiXiaoMsgSuccSSRS.GetText(i, 2);
                      String tCustomerMobile = ttShiXiaoMsgSuccSSRS.GetText(i, 3);
                      String tEdorAcceptNo =  ttShiXiaoMsgSuccSSRS.GetText(i, 4);
                      String tEdorType =  ttShiXiaoMsgSuccSSRS.GetText(i, 5);
                      String tManageCom =  ttShiXiaoMsgSuccSSRS.GetText(i, 6);
                      String tEdorNo =  ttShiXiaoMsgSuccSSRS.GetText(i, 7);
                      
                       if (tConfdate == null || "".equals(tConfdate) ||"null".equals(tConfdate)) 
                       {
                           continue;
                       }
                       
                       if (tContNo == null || "".equals(tContNo) || "null".equals(tContNo))
                       {
                           continue;
                       }

                       if (tCustomerMobile == null || "".equals(tCustomerMobile) ||"null".equals(tCustomerMobile)) 
                       {
                           continue;
                       }
                       if (tEdorAcceptNo == null || "".equals(tEdorAcceptNo) ||"null".equals(tEdorAcceptNo)) 
                       {
                           continue;
                       }
                       
                       if (tEdorType == null || "".equals(tEdorType) || "null".equals(tEdorType))
                       {
                           continue;
                       }

                       if (tManageCom == null || "".equals(tManageCom) ||"null".equals(tManageCom)) 
                       {
                           continue;
                       }
                       if (tEdorNo == null || "".equals(tEdorNo) ||"null".equals(tEdorNo)) 
                       {
                           continue;
                       }
                       
                        //发送给客户的短信            
                       SmsMessageNew tCustomerMsg = new SmsMessageNew(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
                       String tCustomerContents = "客户您好，您所投保的"+tContNo+"保单于"+tConfdate+"日进行了保险信息变更，变更内容为  ";
                       //------------------------------------------查询具体变更内容--------------------------------------

                       System.out.println("contno----------资料变更-------" + tContNo);
                       //有CM相关记录，分别按投保人、被保人分别查询相关修改记录
                       boolean isChangeInsured = false;
                       String tCustomerNo = "";
                       String tFormerCustomerNo = "";

                       String sql = "select * from LPEdorItem " +
                                    "where EdorAcceptNo = '" +
                                    tEdorAcceptNo + "' " +
                                    "and EdorType='" +
                                    tEdorType + "' " +
                                    "order by edorno,insuredno";
                       LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
                       LPEdorItemSet tLPEdorItemSet = tLPEdorItemDB.executeQuery(sql);
                       if (tLPEdorItemSet == null || tLPEdorItemSet.size() < 1) {
                           CError tError = new CError();
                           tError.moduleName = "PrtAppEndorsementBL";
                           tError.functionName = "getInsuredCM";
                           tError.errorMessage = "申请号" + tEdorAcceptNo +
                                                 "下无相应的保全项目信息!";
                           this.mErrors.addOneError(tError);
                       }
                       TextTag textTag = new TextTag();	
                       textTag.add("CM_SIZE", tLPEdorItemSet.size());
                       
                       for (int k = 0; k < tLPEdorItemSet.size(); k++) {
                           LPEdorItemSchema tLPEdorItemSchema = tLPEdorItemSet.get(k + 1);
                           LPInsuredDB tLPInsuredDB = new LPInsuredDB();
                           tLPInsuredDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
                           tLPInsuredDB.setEdorType(tLPEdorItemSchema.getEdorType());
                           tLPInsuredDB.setContNo(tLPEdorItemSchema.getContNo());
                           tLPInsuredDB.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
                           LPInsuredSet tLPInsuredSet = new LPInsuredSet();
                           tLPInsuredSet = tLPInsuredDB.query();

                           LCInsuredDB tLCInsuredDB = new LCInsuredDB();
                           tLCInsuredDB.setContNo(tLPEdorItemSchema.getContNo());
                           tLCInsuredDB.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
                           LCInsuredSet tLCInsuredSet = new LCInsuredSet();
                           tLCInsuredSet = tLCInsuredDB.query();

                           LPInsuredSchema tLPInsuredSchema = new LPInsuredSchema();
                           LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
                           LPAppntSchema tLPAppntSchema = new LPAppntSchema();
                           LCAppntSchema tLCAppntSchema = new LCAppntSchema();

                           if (tLCInsuredSet.size() >= 1) {
                               tLCInsuredSchema = tLCInsuredSet.get(1);
                           }
                           if (tLPInsuredSet.size() >= 1) {
                               tLPInsuredSchema = tLPInsuredSet.get(1);
                               //ref.transFields(tLCInsuredSchema,tLPInsuredSchema);
                           }
                           if (tLPInsuredDB.getInfo()) {
                               //P表中有相应数据表示没有对被保人的信息进行修改
                               isChangeInsured = true;
                               tCustomerNo = tLPInsuredDB.getInsuredNo();
                           }
                           //否则查保全投保人表
                           else {
                               LPAppntDB tLPAppntDB = new LPAppntDB();
                               tLPAppntDB.setEdorType(tLPEdorItemSchema.getEdorType());
                               tLPAppntDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
                               tLPAppntDB.setContNo(tLPEdorItemSchema.getContNo());
                               LPAppntSet tLPAppntSet = new LPAppntSet();
                               tLPAppntSet = tLPAppntDB.query();
                               LCAppntDB tLCAppntDB = new LCAppntDB();
//                               tLCAppntDB.setAppntNo(tLPEdorItemSchema.getInsuredNo());
                               tLCAppntDB.setContNo(tLPEdorItemSchema.getContNo());
                               LCAppntSet tLCAppntSet = new LCAppntSet();
                               tLCAppntSet = tLCAppntDB.query();
                               if (tLCAppntSet.size() >= 1) {
                                   tLCAppntSchema = tLCAppntSet.get(1);
                               }
                               if (tLPAppntSet.size() >= 1) {
                                   tLPAppntSchema = tLPAppntSet.get(1);
                               }
                               if (tLPAppntDB.getInfo()) {
                                   //P表中有相应数据表示没有对投保人的信息进行修改
                                   isChangeInsured = false;
                                   tCustomerNo = tLPAppntDB.getAppntNo();
                               } else {
                                   CError tError = new CError();
                                   tError.moduleName = "PrtAppEndorsementBL";
                                   tError.functionName = "getInsuredCM";
                                   tError.errorMessage = "保全投保人表和被保人表中都不存在相应记录!";
                                   this.mErrors.addOneError(tError);
                               }
                           }
                           
                           //add 缴费资料变更
                           LCContDB tLCContDB = new LCContDB();
                           LCContSet tLCContSet = new LCContSet();
                           LCContSchema tLCContSchema = new LCContSchema();
                           tLCContDB.setContNo(tContNo);
                           tLCContSet = tLCContDB.query();
                           tLCContSchema = tLCContSet.get(1);
                           LPContDB tLPContDB = new LPContDB();
                           LPContSet tLPContSet = new LPContSet();
                           LPContSchema tLPContSchema = new LPContSchema();
                           tLPContDB.setContNo(tContNo);
                           tLPContSet = tLPContDB.query();
                           tLPContSchema = tLPContSet.get(1);

                           if("CC".equals(tEdorType)){
                        	   if(!StrTool.cTrim(tLPContSchema.getPayMode()).equals(
                                       StrTool.
                                       cTrim( tLCContSchema.getPayMode()))){
                        		   tCustomerContents += "缴费方式："+
                        				   ChangeCodeBL.getCodeName("paymode",tLPContSchema.getPayMode())+"  变更为："+
                        				   ChangeCodeBL.getCodeName("paymode",tLCContSchema.getPayMode())+ "    ";
                        	   }
                        	   System.out.println(tLPContSchema.getBankCode());
                        	   System.out.println(tLCContSchema.getBankCode());
                        	   if(!StrTool.cTrim(tLPContSchema.getBankCode()).equals(
                                       StrTool.
                                       cTrim( tLCContSchema.getBankCode()))){
                        		   tCustomerContents += "转账银行："+
                        				   mExeSQL.getOneValue("select bankname from ldbank where bankcode='"+tLPContSchema.getBankCode()+"'")+"  变更为："+
                        				   mExeSQL.getOneValue("select bankname from ldbank where bankcode='"+tLCContSchema.getBankCode()+"'")+ "    ";
//                        				   ChangeCodeBL.getCodeName("bankcom",tLPContSchema.getBankCode())+"  变更为："+
//                        				   ChangeCodeBL.getCodeName("bankcom",tLCContSchema.getBankCode())+ "    ";
                        	   }
                        	   if(!StrTool.cTrim(tLPContSchema.getBankAccNo()).equals(
                                       StrTool.
                                       cTrim( tLCContSchema.getBankAccNo()))){
                        		   tCustomerContents += "转账账号："+tLPContSchema.getBankAccNo()+"  变更为："+ tLCContSchema.getBankAccNo()+ "    ";
                        	   }
                        	   if(!StrTool.cTrim(tLPContSchema.getAccName()).equals(
                                       StrTool.
                                       cTrim( tLCContSchema.getAccName()))){
                        		   tCustomerContents += "账户名："+tLPContSchema.getAccName()+"  变更为："+tLCContSchema.getAccName()+ "    ";
                        	   }
                           }

                           if (!tFormerCustomerNo.equals(tCustomerNo)) {
                               LPPersonDB tLPPersonDB = new LPPersonDB();
                               tLPPersonDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
                               tLPPersonDB.setEdorType(tLPEdorItemSchema.getEdorType());
                               tLPPersonDB.setCustomerNo(tCustomerNo);
                               if (!tLPPersonDB.getInfo()) {
                                   CError tError = new CError();
                                   tError.moduleName = "PrtAppEndorsementBL";
                                   tError.functionName = "getInsuredCM";
                                   tError.errorMessage = "LPPersonDB不存在相应记录!";
                                   this.mErrors.addOneError(tError);
                               }

                               LDPersonDB tLDPersonDB = new LDPersonDB();
                               tLDPersonDB.setCustomerNo(tCustomerNo);
                               if (!tLDPersonDB.getInfo()) {
                                   CError tError = new CError();
                                   tError.moduleName = "PrtAppEndorsementBL";
                                   tError.functionName = "getInsuredCM";
                                   tError.errorMessage = "LDPersonDB不存在相应记录!";
                                   this.mErrors.addOneError(tError);
                               }
                               if (isChangeInsured) {
                                   //姓名变更
                                   if (!StrTool.cTrim(tLCInsuredSchema.getName()).equals(
                                          StrTool.cTrim(tLPInsuredSchema.getName()))) {
                                	   
                                       tCustomerContents += "姓名：" +
                                                   CommonBL.notNull(tLPInsuredSchema.getName()) +
                                                   "  变更为：" +
                                                 CommonBL.notNull(tLCInsuredSchema.getName()) + "    ";
                                   }
                                   //生日变更
                                   if (!StrTool.cTrim(tLCInsuredSchema.getBirthday()).equals(
                                           StrTool.cTrim(tLPInsuredSchema.getBirthday()))) {
                                       tCustomerContents += "生日：" +
                                                   CommonBL.decodeDate(tLPInsuredSchema.
                                               getBirthday()) +
                                                   "  变更为：" +
                                                   CommonBL.decodeDate(tLCInsuredSchema.
                                               getBirthday())+ "    ";
                                   }

                                   //性别变更
                                   if (!StrTool.cTrim(tLCInsuredSchema.getSex()).equals(
                                           StrTool.
                                           cTrim(tLPInsuredSchema.getSex()))) {
                                       tCustomerContents += "性别：" +
                                    		   mExeSQL.getOneValue("select nvl(codename,'') from ldcode where codetype='sex' and code='"+
                                					   tLPInsuredSchema.getSex()+ "'")+ "  变更为：" +
                                             mExeSQL.getOneValue("select nvl(codename,'') from ldcode where codetype='sex' and code='"+
                                            		 tLCInsuredSchema.getSex()+ "'")+  "    ";
//                                                   ChangeCodeBL.getCodeName("Sex",
//                                               tLCInsuredSchema.getSex()) +
//                                                   "  变更为：" +
//                                                   ChangeCodeBL.getCodeName("Sex",
//                                               tLPInsuredSchema.getSex())+ "    ";
                                   }

                                   //证件类型变更
                                   if (!StrTool.cTrim(tLCInsuredSchema.getIDType()).equals(
                                           StrTool.
                                           cTrim(tLPInsuredSchema.getIDType()))) {
                                	   tCustomerContents += "证件类型：" +
                                			   mExeSQL.getOneValue("select nvl(codename,'') from ldcode where codetype='idtype' and code='"+
                                					   tLPInsuredSchema.getIDType()+ "'")+ "  变更为：" +
                                             mExeSQL.getOneValue("select nvl(codename,'') from ldcode where codetype='idtype' and code='"+
                                            		 tLCInsuredSchema.getIDType()+ "'")+  "    ";
//                                                   ChangeCodeBL.getCodeName("IDType",
//                                               tLCInsuredSchema.getIDType()) +
//                                                   "  变更为：" +
//                                                   ChangeCodeBL.getCodeName("IDType",
//                                               tLPInsuredSchema.getIDType())+ "    ";
                                   }

                                   //证件号码变更
                                   if (!StrTool.cTrim(tLCInsuredSchema.getIDNo()).equals(
                                           StrTool.
                                           cTrim( tLPInsuredSchema.getIDNo()))) {
                                       tCustomerContents += "证件号码：" +
                                                   CommonBL.notNull(tLPInsuredSchema.getIDNo()) +
                                                   "  变更为：" +
                                                   CommonBL.notNull(tLCInsuredSchema.getIDNo())+ "    ";
                                   }

                                   //职业(Code)+职业等级变更
                                   if (!StrTool.cTrim(tLCInsuredSchema.getOccupationCode()).
                                       equals(
                                               StrTool.cTrim(tLPInsuredSchema.
                                                             getOccupationCode()))) {
                                	   tCustomerContents += "职业：" +
                                                   ChangeCodeBL.getCodeName("OccupationCode",
                                               tLCInsuredSchema.getOccupationCode(),
                                               "OccupationCode")
                                                   + "(风险" +
                                                   tLPInsuredSchema.getOccupationType()
                                                   + "级)"
                                                   + "\n变更为：" +
                                                   ChangeCodeBL.getCodeName("OccupationCode",
                                               tLPInsuredSchema.getOccupationCode(),
                                               "OccupationCode")
                                                   + "(风险" +
                                                   tLCInsuredSchema.getOccupationType()
                                                   + "级)     "
                                                   ;
                                   }

                                   //婚姻状况变更
                                   if (!StrTool.cTrim(tLCInsuredSchema.getMarriage()).equals(
                                           StrTool.cTrim(tLPInsuredSchema.getMarriage()))) {
                                	   tCustomerContents += "婚姻状况：" +
                                			   mExeSQL.getOneValue("select nvl(codename,'') from ldcode where codetype='marriage' and code='"+
                                					   tLPInsuredSchema.getMarriage()+ "'")+ "  变更为：" +
                                             mExeSQL.getOneValue("select nvl(codename,'') from ldcode where codetype='marriage' and code='"+
                                            		 tLCInsuredSchema.getMarriage()+ "'")+  "    ";
//                                                   ChangeCodeBL.getCodeName("Marriage",
//                                               tLCInsuredSchema.getMarriage())
//                                                   + "  变更为：" +
//                                                   ChangeCodeBL.getCodeName("Marriage",
//                                               tLPInsuredSchema.getMarriage())+ "    ";
                                   }

                                   //在职状态变更
                                   if (!StrTool.cTrim(tLCInsuredSchema.getInsuredStat()).
                                       equals(
                                               StrTool.cTrim(tLPInsuredSchema.getInsuredStat()))) {
                                	   tCustomerContents += "在职状态：" +
                                			   mExeSQL.getOneValue("select nvl(codename,'') from ldcode where codetype='workstate' and code='"+
                                					   tLPInsuredSchema.getInsuredStat()+ "'")+ "  变更为：" +
                                             mExeSQL.getOneValue("select nvl(codename,'') from ldcode where codetype='workstate' and code='"+
                                            		 tLCInsuredSchema.getInsuredStat()+ "'")+  "    ";
//                                	   ChangeCodeBL.getCodeName("workstate",
//                                               tLPInsuredSchema.getInsuredStat())
//                                                   + "  变更为：" +
//                                                   ChangeCodeBL.getCodeName("workstate",
//                                               tLCInsuredSchema.getInsuredStat())+ "    ";
                                   }

                                   //理赔金转帐银行变更
                                   if (!StrTool.cTrim(tLPInsuredSchema.getBankCode()).equals(
                                           StrTool.
                                           cTrim(
                                                   tLCInsuredSchema.getBankCode()))) {
                                	   tCustomerContents += "理赔金转帐银行：" +
                                			   mExeSQL.getOneValue("select bankname from ldbank where bankcode='"+tLPInsuredSchema.getBankCode()+"'")+"  变更为："+
                            				   mExeSQL.getOneValue("select bankname from ldbank where bankcode='"+tLCInsuredSchema.getBankCode()+"'")+ "    ";
//                                                   ChangeCodeBL.getCodeName("bank",
//                                               tLCInsuredSchema.getBankCode(), "BankCode") +
//                                                   "  变更为：" +
//                                                   ChangeCodeBL.getCodeName("bank",
//                                               tLPInsuredSchema.getBankCode(), "BankCode")+ "    ";

                                   }

//                理赔金转帐帐号变更
                                   if (!StrTool.cTrim(tLPInsuredSchema.getBankAccNo()).equals(
                                           StrTool.
                                           cTrim(
                                                   tLCInsuredSchema.getBankAccNo()))) {
                                	   tCustomerContents += "理赔金转帐帐号：" +
                                                   StrTool.cTrim(tLPInsuredSchema.getBankAccNo()) +
                                                   "  变更为：" +
                                                   StrTool.cTrim(tLCInsuredSchema.getBankAccNo())+ "    ";
                                   }

                                   if (!StrTool.cTrim(tLPInsuredSchema.getAccName()).equals(
                                           StrTool.
                                           cTrim(
                                                   tLCInsuredSchema.getAccName()))) {
                                	   tCustomerContents += "理赔金转帐帐户：" +
                                                   StrTool.cTrim(tLPInsuredSchema.getAccName()) +
                                                   "  变更为：" +
                                                   StrTool.cTrim(tLCInsuredSchema.getAccName())+ "    ";
                                   }

//                与主被保人关系变更
                                   String relation = "";
                                   String relationtoappnt = "";
                                   tLCInsuredDB.setContNo(tLPEdorItemSchema.getContNo());
                                   tLCInsuredDB.setInsuredNo(tLPEdorItemSchema.getInsuredNo());
                                   if (tLCInsuredDB.getInfo()) {
                                       relation = tLPInsuredDB.getRelationToMainInsured();
                                       relationtoappnt=tLPInsuredDB.getRelationToAppnt();
                                   }

                                   if (!StrTool.cTrim(relation).equals(
                                           StrTool.cTrim(tLCInsuredDB.getRelationToMainInsured()))) {
//                                	   tCustomerContents += "与主被保人关系：" +
//                                                   ChangeCodeBL.getCodeName("Relation",
//                                               tLCInsuredDB.getRelationToMainInsured()) +
//                                                   "  变更为：" +
//                                                   ChangeCodeBL.getCodeName("Relation",
//                                               relation)+ "    ";
                                	   tCustomerContents += "与主被保人关系：" +
                                			   mExeSQL.getOneValue("select nvl(codename,'') from ldcode where codetype='relation' and code='"+
                                					   tLPInsuredDB.getRelationToMainInsured()+ "'")+ "  变更为：" +
                                             mExeSQL.getOneValue("select nvl(codename,'') from ldcode where codetype='relation' and code='"+
                              					   tLCInsuredDB.getRelationToMainInsured()+ "'")+  "    ";
                                   }
                                 //  与投保人的关系：
                                   if (!StrTool.cTrim(relationtoappnt).equals(
                                           StrTool.cTrim(tLCInsuredDB.getRelationToAppnt()))) {
                                	   tCustomerContents += "与投保人的关系：" +
                                			   mExeSQL.getOneValue("select nvl(codename,'') from ldcode where codetype='relation' and code='"+
                                					   tLPInsuredDB.getRelationToAppnt()+ "'")+ "  变更为：" +
                                             mExeSQL.getOneValue("select nvl(codename,'') from ldcode where codetype='relation' and code='"+
                              					   tLCInsuredDB.getRelationToAppnt()+ "'")+  "    ";
//                                                   ChangeCodeBL.getCodeName("Relation",
//                                               tLCInsuredDB.getRelationToAppnt()) +
//                                                   "  变更为：" +
//                                                   ChangeCodeBL.getCodeName("Relation",
//                                                   		relationtoappnt)+ "    ";
                                   }
                               } else {
                                   //姓名变更
                                   if (!StrTool.cTrim(tLCAppntSchema.getAppntName()).equals(
                                           StrTool.
                                           cTrim(tLPAppntSchema.getAppntName()))) {
                                       tCustomerContents += "姓名：" + tLPAppntSchema.getAppntName() +
                                                   "  变更为：" + tLCAppntSchema.getAppntName()+ "    ";
                                   }
                                   //生日变更
                                   if (!StrTool.cTrim(tLCAppntSchema.getAppntBirthday()).
                                       equals(
                                               StrTool.cTrim(tLPAppntSchema.getAppntBirthday()))) {
                                       tCustomerContents += "生日：" +
                                                   CommonBL.decodeDate(tLPAppntSchema.
                                               getAppntBirthday()) +
                                                   "  变更为：" +
                                                   CommonBL.decodeDate(tLCAppntSchema.
                                               getAppntBirthday())+ "    ";
                                   }

                                   //性别变更
                                   if (!StrTool.cTrim(tLCAppntSchema.getAppntSex()).equals(
                                           StrTool.
                                           cTrim(tLPAppntSchema.getAppntSex()))) {
                                	   tCustomerContents += "性别：" +
                                			   mExeSQL.getOneValue("select nvl(codename,'') from ldcode where codetype='sex' and code='"+
                                					   tLPAppntSchema.getAppntSex()+ "'")+ "  变更为：" +
                                             mExeSQL.getOneValue("select nvl(codename,'') from ldcode where codetype='sex' and code='"+
                                            		 tLCAppntSchema.getAppntSex()+ "'")+  "    ";
//                                                   ChangeCodeBL.getCodeName("Sex",
//                                               tLPAppntSchema.getAppntSex()) +
//                                                   "  变更为：" +
//                                                   ChangeCodeBL.getCodeName("Sex",
//                                               tLCAppntSchema.getAppntSex())+ "    ";
                                   }
                                   //证件类型变更
                                   if (!StrTool.cTrim(tLCAppntSchema.getIDType()).equals(
                                           StrTool.
                                           cTrim(tLPAppntSchema.getIDType()))) {
                                       tCustomerContents += "证件类型：" +
                                    		   mExeSQL.getOneValue("select nvl(codename,' ') from ldcode where codetype='idtype' and code='"+
                                					   tLPAppntSchema.getAppntSex()+ "'")+ "  变更为：" +
                                             mExeSQL.getOneValue("select nvl(codename,' ') from ldcode where codetype='idtype' and code='"+
                                            		 tLCAppntSchema.getAppntSex()+ "'")+  "    ";
//                                                   ChangeCodeBL.getCodeName("IDType",
//                                               tLPAppntSchema.getIDType()) +
//                                                   "  变更为：" +
//                                                   ChangeCodeBL.getCodeName("IDType",
//                                               tLCAppntSchema.getIDType())+ "    ";
                                   }

                                   //证件号码变更
                                   if (!StrTool.cTrim(tLCAppntSchema.getIDNo()).equals(
                                           StrTool.
                                           cTrim(
                                                   tLPAppntSchema.getIDNo()))) {
                                	   tCustomerContents += "证件号码：" +
                                                   CommonBL.notNull(tLPAppntSchema.getIDNo()) +
                                                   "  变更为：" +
                                                   CommonBL.notNull(tLCAppntSchema.getIDNo())+ "    ";
                                   }

                                   //职业(Code)+职业等级变更
                                   if (!StrTool.cTrim(tLCAppntSchema.getOccupationCode()).
                                       equals(
                                               StrTool.cTrim(tLPAppntSchema.getOccupationCode()))) {
                                	   tCustomerContents += "职业：" +
                                                   ChangeCodeBL.getCodeName("OccupationCode",
                                               tLCAppntSchema.getOccupationCode(),
                                               "OccupationCode")
                                                   + "(风险" +
                                                   tLPAppntSchema.getOccupationType()
                                                   + "级)"
                                                   + "\n变更为：" +
                                                   ChangeCodeBL.getCodeName("OccupationCode",
                                               tLCAppntSchema.getOccupationCode(),
                                               "OccupationCode")
                                                   + "(风险" +
                                                   tLPAppntSchema.getOccupationType()
                                                   + "级)      "
                                                   ;
                                   }

                                   //婚姻状况变更
                                   if (!StrTool.cTrim(tLCAppntSchema.getMarriage()).equals(
                                           StrTool.cTrim(tLPAppntSchema.getMarriage()))) {
                                	   tCustomerContents += "婚姻状况：" +
                                			   mExeSQL.getOneValue("select nvl(codename,' ') from ldcode where codetype='marriage' and code='"+
                                					   tLPAppntSchema.getMarriage()+ "'")+ "  变更为：" +
                                             mExeSQL.getOneValue("select nvl(codename,' ') from ldcode where codetype='marriage' and code='"+
                                            		 tLCAppntSchema.getMarriage()+ "'")+  "    ";
//                                                   ChangeCodeBL.getCodeName("Marriage",
//                                               tLPAppntSchema.getMarriage()) + "  变更为：" +
//                                                   ChangeCodeBL.getCodeName("Marriage",
//                                               tLCAppntSchema.getMarriage())+ "    ";
                                   }

                               }
                              

                               //资料更新导致的费用变更
                               sql = "select sum(GetMoney) from LJSGetEndorse " +
                                     "where EndorsementNo = '" +
                                     tLPEdorItemSchema.getEdorNo() + "' " +
                                     "and FeeOperationType = '" +
                                     tLPEdorItemSchema.getEdorType() + "' " +
                                     "and InsuredNo = '" +
                                     tLPEdorItemSchema.getInsuredNo() + "'";
                               ExeSQL tExeSQL = new ExeSQL();
                               String chgPrem = tExeSQL.getOneValue(sql);
                               
                               String fenHongXian="select 1 from lcpol a where insuredno='"+tLPEdorItemSchema.getInsuredNo()+"' and  contno='"+tLPEdorItemSchema.getContNo()
                                                 +"' and exists (select 1 from lppol where amnt!=a.amnt and edorno ='"+tLPEdorItemSchema.getEdorNo()+"' and polno=a.polno and insuredno=a.insuredno and edortype='"+tLPEdorItemSchema.getEdorType()+"')"
                                                 ;
                               String fenHongXianFlag=tExeSQL.getOneValue(fenHongXian);
                               if ((chgPrem != null && !"".equals(chgPrem) &&
                                   Double.parseDouble(chgPrem) != 0)||(fenHongXianFlag!=null&&!fenHongXianFlag.equals(""))) { //保费变更，需要添加费用变更打印内容
                                   sql = "select distinct(contno) from LJSGetEndorse " +
                                         "where EndorsementNo = '" +
                                         tLPEdorItemSchema.getEdorNo() + "' " +
                                         "and FeeOperationType = '" +
                                         tLPEdorItemSchema.getEdorType() + "' " +
                                         "and InsuredNo = '" +
                                         tLPEdorItemSchema.getInsuredNo() + "' " +
                                         //此处需要过滤万能险种和附加重疾
                                         		" and riskcode not in (select riskcode from lmriskapp where risktype4='4') " +
                                         		" and riskcode not in ('231001') ";
                                   tExeSQL = new ExeSQL();
                                   SSRS g = tExeSQL.execSQL(sql);
                                   String[][] strContno = g.getAllData();

                                   for (int m = 0; m < strContno.length; m++) {
                                       sql = "select * from lcpol where polno in " +
                                             "(select polno from lppol where edorno= '" +
                                             tLPEdorItemSchema.getEdorNo() + "' and " +
                                             "edortype='" + tLPEdorItemSchema.getEdorType() +
                                             "' and insuredno='" +
                                             tLPEdorItemSchema.getInsuredNo() + "' and " +
                                             "contno ='" + strContno[m][0] + "')";
                                       LCPolDB tLCPolDB = new LCPolDB();
                                       LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql);
                                       if (tLCPolSet == null || tLCPolSet.size() < 1) {
                                           CError tError = new CError();
                                           tError.moduleName = "PrtAppEndorsementBL";
                                           tError.functionName = "getInsuredCM";
                                           tError.errorMessage = "LCPol不存在相应的记录!";
                                           this.mErrors.addOneError(tError);
                                       }

                                       tCustomerContents += " 。由于上述资料变更 ，保单" +
                                                   strContno[m][0] +
                                                   "相关险种承保内容变更如下：";

                                       for (int j = 0; j < tLCPolSet.size(); j++) {
                                           LCPolSchema tLCPOLSchema = tLCPolSet.get(j + 1);
                                           sql = "select sum(GetMoney) from LJSGetEndorse " +
                                                 "where EndorsementNo = '" +
                                                 tLPEdorItemSchema.getEdorNo() + "' " +
                                                 "and FeeOperationType = '" +
                                                 tLPEdorItemSchema.getEdorType() + "' " +
                                                 "and InsuredNo = '" +
                                                 tLPEdorItemSchema.getInsuredNo() + "' " +
                                                 "and PolNo = '" +
                                                 tLCPOLSchema.getPolNo() + "' " +
                                                 "and contno = '" + tLCPOLSchema.getContNo() +
                                                 "'";

                                           tExeSQL = new ExeSQL();
                                           chgPrem = tExeSQL.getOneValue(sql);
                                           if ((chgPrem == null) || (chgPrem.equals(""))) {
                                               continue;
                                           }

                                           LPPolDB tLPPolDB = new LPPolDB();
                                           tLPPolDB.setEdorNo(tLPEdorItemSchema.getEdorNo());
                                           tLPPolDB.setEdorType(tLPEdorItemSchema.getEdorType());
                                           tLPPolDB.setPolNo(tLCPOLSchema.getPolNo());
                                           if (!tLPPolDB.getInfo()) {
                                               CError tError = new CError();
                                               tError.moduleName = "PrtAppEndorsementBL";
                                               tError.functionName = "getInsuredCM";
                                               tError.errorMessage = "LPPol不存在相应的记录!";
                                               this.mErrors.addOneError(tError);
                                           }
                                           LMRiskDB tLMRiskDB = new LMRiskDB();
                                           tLMRiskDB.setRiskCode(tLCPOLSchema.getRiskCode());
                                           if (!tLMRiskDB.getInfo()) {
                                               CError tError = new CError();
                                               tError.moduleName = "PrtAppEndorsementBL";
                                               tError.functionName = "getInsuredCM";
                                               tError.errorMessage = "LMRisk不存在相应的记录!";
                                               this.mErrors.addOneError(tError);
                                           }

                                           tCustomerContents += "序号" + tLCPOLSchema.getRiskSeqNo() +
                                           "险种（" +
                                           tLMRiskDB.getRiskName() + "）保额变更为："+tLPPolDB.getAmnt()+",保费变更为：" +
                                           tLPPolDB.getPrem() + "    ";

                                  if (Double.parseDouble(chgPrem) < 0) {
                                	  tCustomerContents += ", 应退费" +
                                              Math.abs(Double.
                                                       parseDouble(
                                              chgPrem)) +
                                              "元。\n";

                                  } else if (Double.parseDouble(chgPrem) > 0) {
                                	  tCustomerContents += ", 应补费" +
                                              Double.parseDouble(
                                                      chgPrem) +
                                              "元。\n";
                                  }
                                  else
                                  {
                                	  tCustomerContents += "。";
                                  }
                                       }
                                   }
                               }
                           }
                           
//                         万能和万能附加重疾部分批单
                           String WNsql = "select distinct contno from LPInsureAccTrace where EdorNo = '" +tLPEdorItemSchema.getEdorNo() + "' and EdorType = '" + tLPEdorItemSchema.getEdorType() + "' ";
                           ExeSQL tExeSQL = new ExeSQL();
                           SSRS WNssrs = tExeSQL.execSQL(WNsql);
                           String[][] WNContno = WNssrs.getAllData();
                           for (int l = 0; l < WNContno.length; l++) {
                           	if(CommonBL.hasULIRisk(WNContno[l][0])){//如果是万能险
//                           		除了附加重疾外的万能费用
                           		String tmoney=tExeSQL.getOneValue("select sum(money) from LPInsureAccTrace where payplancode not in ('231001') and ContNo='"+WNContno[l][0]+"' and EdorNo = '" +tLPEdorItemSchema.getEdorNo() + "' and EdorType = '" + tLPEdorItemSchema.getEdorType() + "' ");
//                					万能附加重疾的客户资料变更费用            	
                           		String tEXmoney=tExeSQL.getOneValue("select sum(money) from LPInsureAccTrace where payplancode in ('231001') and ContNo='"+WNContno[l][0]+"' and EdorNo = '" +tLPEdorItemSchema.getEdorNo() + "' and EdorType = '" + tLPEdorItemSchema.getEdorType() + "' ");
                           		if(tmoney != null && !"".equals(tmoney) &&
                                           Double.parseDouble(tmoney) != 0){
                           			if(Double.parseDouble(tmoney)>0){//退费情况
                           				tCustomerContents +="保单号码"+WNContno[l][0]+"的万能保单主险因客户资料变更需要退回风险保费"+tmoney+"元，本次退回的风险保费将全部进入万能个人帐户。\n";
                               		}
                               		else{//补费情况
                               			tCustomerContents +="保单号码"+WNContno[l][0]+"的万能保单主险因客户资料变更需要补交风险保费"+Math.abs(Double.parseDouble(tmoney))+"元，本次补交的风险保费将全部从万能个人帐户中扣除。\n";
                               		}
                           		}
                           		if(tEXmoney != null && !"".equals(tEXmoney) &&
                                           Double.parseDouble(tEXmoney) != 0){
                           			if(Double.parseDouble(tEXmoney)>0){//退费情况
                           				tCustomerContents +="保单号码"+WNContno[l][0]+"的万能保单附加重疾险种因客户资料变更需要退回风险保费"+tEXmoney+"元，本次退回的风险保费将全部进入万能个人帐户。\n";
                               		}
                               		else{//补费情况
                               			tCustomerContents +="保单号码"+WNContno[l][0]+"的万能保单附加重疾险种因客户资料变更需要补交风险保费"+Math.abs(Double.parseDouble(tEXmoney))+"元，本次补交的风险保费将全部从万能个人帐户中扣除。\n";
                               		}
                           		}
                           	}
                           }
                       }
                   
                       
                       //-----------------------------------------------------------------------------------------查询变更内容结束
                       
                       
                       tCustomerMsg.setReceiver(tCustomerMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
                      
                       tCustomerMsg.setContents(tCustomerContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素

                       //direct by lc 2017-3-27 机构
                       int Mglength = tManageCom.length();
                       if(Mglength==8){
                       		tCustomerMsg.setOrgCode(tManageCom);
                       } else {
                       		tCustomerMsg.setOrgCode(tManageCom.substring(0, 4) + "0000"); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
                       } 
                       
                       //添加SmsMessage对象
                       tVector.add(tCustomerMsg);
                       
//                           在LCCSpec表中插入值  ******************************************************** 
                       LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
                       tLCCSpecSchema.setGrpContNo(BQ.GRPFILLDATA);
                       tLCCSpecSchema.setContNo(tContNo);
                       tLCCSpecSchema.setProposalContNo(BQ.GRPFILLDATA);               
                       tLCCSpecSchema.setPrtSeq(tEdorNo);// 放 保全批单号EdorNo 
                       String tLimit = PubFun.getNoLimit(tManageCom);
           	           String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
           	           tLCCSpecSchema.setSerialNo(serNo); //流水号                
                       tLCCSpecSchema.setEndorsementNo(tEdorNo);//结案日期
                       tLCCSpecSchema.setSpecType("zlbg"); //bg-客户资料变更
                       tLCCSpecSchema.setSpecCode("001");////001-投保人 002-被保人 003-业务员
                       tLCCSpecSchema.setSpecContent(tCustomerContents+"客户手机号："+ tCustomerMobile);
                       tLCCSpecSchema.setOperator(mG.Operator);
                       tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
                       tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
                       tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
                       tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
                       tLCCSpecSchema.getDB().insert();
                  
                       
                   }
                   
               //到此处
        System.out.println("都结束了----------------------------------------------------------------");
        return tVector;
    }
    
    
    public static void main(String[] args) {
        GlobalInput mGlobalInput = new GlobalInput();
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "86";
        mVData.add(mGlobalInput);
        
        SDMsgBL tSDwMsgBL = new SDMsgBL();
        tSDwMsgBL.submitData(mVData, "SDMSG");
    }

}
