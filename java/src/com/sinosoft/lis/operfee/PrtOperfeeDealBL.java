package com.sinosoft.lis.operfee;

import java.util.*;

import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.task.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 续期续保业务报表1-达成率分析(保单)
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class PrtOperfeeDealBL
{
    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private static String W = "W";  //无名单
    private static String N = "N";  //非名单
    private static String L = "L";  //长险保单
    private static String O = "O";  //非长险保单
    private static String P = "P";  //个人
    private static String G = "G";  //团体
    private static String SUCCESS = "Suc";  //成功
    private static String WAIT_CHARGE = "Waitcharge";  //团单成功
    private static String INVALID = "Invalid";  //失效
    private static String PAYMODE4 = "M4";  //银行转帐
    private static String PAYMODEO = "MO";  //其他收费方式
    private static String COM86 = "全系统";
    private static int COL_NUM = 24;

    private String[] mInfoSum1 = null;  //全系统团险无名单续期
    private String[] mInfoSum2 = null;  //全系统团险有名单续期
    private String[] mInfoSumSub1 = null;  //全系统团险合计
    private String[] mInfoSum3 = null;  //全系统个险长期保单
    private String[] mInfoSum4 = null;  //全系统个险一年期保单
    private String[] mInfoSumSub2 = null;  //全系统个险合计

    private GlobalInput mGI = null; //操作员信息
    private TransferData mTransferData = null;

    private String mStartDate = null;
    private String mEndDate = null;
    private String mManageCom = null;

    HashMap mHashMap = new HashMap(); //存储清单数据
    HashSet mHashSet = new HashSet();  //存储涉及的机构

    private ExeSQL mExeSQL = new ExeSQL();

    private XmlExport xmlexport = new XmlExport();

    public PrtOperfeeDealBL()
    {
    }

    /**
     * 操作的提交方法，得到打印清单数据。
     * @param sql String
     * @return XmlExport
     */
    public XmlExport getXmlExport(VData cInputData, String operate)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }

        if(!checkData())
        {
            return null;
        }

        if(!dealData())
        {
            return null;
        }

        return xmlexport;
    }

    /**
     * createXmlExport
     * 根据dealData中得到的数据生成清单
     * @return boolean
     */
    private boolean createXmlExport()
    {
        ListTable tListTable = new ListTable();

        SSRS tSSRS = getManageComs();
        if(tSSRS == null)
        {
            return false;
        }

        String[] sum1 = new String[COL_NUM];
        String[] sum2 = new String[COL_NUM];
        String[] sum3 = new String[COL_NUM];
        String[] sum4 = new String[COL_NUM];
        String[] sum5 = new String[COL_NUM];
        String[] sum6 = new String[COL_NUM];

        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String[] info1 = getOneGrpInfo(tSSRS.GetText(i, 1), W);
            info1[0] = GetMemberInfo.getComNameByComCode(tSSRS.GetText(i, 1));
            info1[1] = "团险";
            info1[2] = "无名单续期";
            tListTable.add(nullToEmpty(info1));
            sum1 = com.sinosoft.lis.bq.CommonBL.add(sum1, info1, 3);
            sum1[1] = info1[1];
            sum1[2] = "无名单续期";
            String[] info2 = getOneGrpInfo(tSSRS.GetText(i, 1), N);
            info2[0] = info1[0];
            info2[1] = info1[1];
            info2[2] = "有名单续期";
            tListTable.add(nullToEmpty(info2));
            sum2 = com.sinosoft.lis.bq.CommonBL.add(sum2, info2, 3);
            sum2[1] = info1[1];
            sum2[2] = "有名单续期";


            String[] infoSubG = com.sinosoft.lis.bq.CommonBL.add(info1, info2, 3);
            System.out.println((info1 == infoSubG) + ", "
                + (infoSubG.equals(info1)));
            infoSubG[0] = info2[0];
            infoSubG[1] = info2[1];
            infoSubG[2] = "团险合计";
            tListTable.add(nullToEmpty(infoSubG));
            sum3 = com.sinosoft.lis.bq.CommonBL.add(sum3, infoSubG, 3);
            sum3[1] = info2[1];
            sum3[2] = "团险合计";


            String[] info3 = getOneIndiInfo(tSSRS.GetText(i, 1), L);
            info3[0] = info1[0];
            info3[1] = "个险";
            info3[2] = "长期保单";
            tListTable.add(nullToEmpty(info3));
            sum4 = com.sinosoft.lis.bq.CommonBL.add(sum4, info3, 3);
            sum4[1] = "个险";
            sum4[2] = "长期保单";


            String[] info4 = getOneIndiInfo(tSSRS.GetText(i, 1), O);
            info4[0] = info3[0];
            info4[1] = info3[1];
            info4[2] = "一年期保单";
            tListTable.add(nullToEmpty(info4));
            sum5 = com.sinosoft.lis.bq.CommonBL.add(sum5, info4, 3);
            sum5[1] = info3[1];
            sum5[2] = "一年期保单";


            String[] infoSubP = com.sinosoft.lis.bq.CommonBL.add(info3, info4, 3);
            infoSubP[0] = info3[0];
            infoSubP[1] = info3[1];
            infoSubP[2] = "个险合计";
            tListTable.add(nullToEmpty(infoSubP));
            sum6 = com.sinosoft.lis.bq.CommonBL.add(sum6, infoSubP, 3);
            sum6[1] = info3[1];
            sum6[2] = "个险合计";
        }

        sum1[0] = COM86;
        sum2[0] = COM86;
        sum3[0] = COM86;
        sum4[0] = COM86;
        sum5[0] = COM86;
        sum6[0] = COM86;
        tListTable.add(nullToEmpty(sum1));
        tListTable.add(nullToEmpty(sum2));
        tListTable.add(nullToEmpty(sum3));
        tListTable.add(nullToEmpty(sum4));
        tListTable.add(nullToEmpty(sum5));
        tListTable.add(nullToEmpty(sum6));

        tListTable.setName("List");

        String[] colNames = {"机构","业务类型",
                            "应收件数","应收保费",
                            "应收成功件数","应收成功保费",
                            "收费成功率(%)件数","收费成功率(%)保费",
                            "平均收费时效",
                            "待收费件数","待收费保费",
                            "未收失效件数","未收失效保费",
                            "个险转账成功率件数","个险转账成功率保费",
                            "个险自缴成功率件数","个险自缴成功率保费", "处理时长"};

        xmlexport.addListTable(tListTable, colNames);

        return true;
    }

    private String[] nullToEmpty(String[] string)
    {
        if(string == null)
        {
            return null;
        }

        for(int i = 0; i < string.length; i++)
        {
            string[i]= StrTool.cTrim(string[i]);
        }

        return string;
    }

    /**
     * getOneIndiInfo
     *查询个单行数据
     * @param string String
     * @param L String
     * @return String[]
     */
    private String[] getOneIndiInfo(String manageCom, String type)
    {
        String[] info = new String[COL_NUM];

       String[] tLJsp = getLJSPayInfoP(manageCom, type);  //生成应收
       info[3] = tLJsp[0];
       info[4] = tLJsp[1];
       info[5] = tLJsp[2];
       info[6] = tLJsp[3];

       String[] tLJap = getLJAPayInfoP(manageCom, type);  //核销
       info[7] = tLJap[0];
       info[8] = tLJap[1];
       info[9] = tLJap[2];
       info[10] = tLJap[3];

       String[] thb = getHBInfoP(manageCom, type);  //核保
       info[11] = thb[0];
       info[12] = thb[1];
       info[13] = thb[2];

       String[] invali = getInvaliP(manageCom, type);
       info[14] = invali[0];
       info[15] = invali[1];

       String[] teminate = getTeminateP(manageCom, type);
       info[16] = teminate[0];
       info[17] = teminate[1];

       String[] hastenP = getPhoneHastenP(manageCom, type);
       info[18] = hastenP[0];
       info[19] = hastenP[1];
       info[20] = hastenP[2];
       info[21] = hastenP[3];

       String[] unchargeP = getUnChargeP(manageCom, type);
       info[22] = unchargeP[0];
       info[23] = unchargeP[1];

       return info;
    }

    /**
     * getUnChargeP
     * 查询终止缴费，即撤销保单数
     * @param manageCom String
     * @param type String
     * @return String[]
     */
    private String[] getUnChargeP(String manageCom, String type)
    {
        String sql = "select Count(a.ContNO), nvl(sum(SumActuPayMoney), 0) from LJSPayPersonB a "
                    + "where DealState = '3' "  //撤销状态
                    + "	  and exists (select 1 from LJSPayB where GetNoticeNo = a.GetNoticeNo and OtherNoType = '2') "  //个单
                    + "	  and hasLongRisk(a.ContNo) = '" + type + "' "
                    + "	  and PayType = 'ZC' "
                    + "   and ManageCom = '" + manageCom + "' "
                    + "   and ModifyDate between '" + mStartDate + "' and '" + mEndDate + "' "
                    + "with ur ";  //撤销时间
       SSRS tSSRS = mExeSQL.execSQL(sql);
       if(mExeSQL.mErrors.needDealError())
       {
           CError tError = new CError();
           tError.moduleName = "PrtOperfeeDealBL";
           tError.functionName = "getOneGrpInfo";
           tError.errorMessage = "查询个单数据出错";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return null;
       }

       if(tSSRS.getMaxRow() == 0 || tSSRS.GetText(1, 1).equals("0"))
       {
           String[] s = {"0", "0","0", "0"};
           return s;
       }

       return tSSRS.getRowData(1);
    }

    /**
     * getPhoneHastenP
     *查询电话催缴记录数
     * @param manageCom String
     * @param type String
     * @return String[]
     */
    private String[] getPhoneHastenP(String manageCom, String type)
    {
        String sql = "select count(GetNoticeNo), sum(Suc), count(GetNoticeNo) - sum(Suc) - sum(UnFinish), sum(UnFinish) from "
                     + "( "
                     + "    select a.GetNoticeNo,"
                     + "		case FinishType when '1' then 1 when '4' then 1 else 0 end Suc, "   //重设缴费时间或核销
                     + "		case when (FinishType is null or FinishType = '') then 1 else 0 end UnFinish  "  //未结案
                     + "	from LGPhoneHasten a, LJSPayB b "
                     + "	where a.GetNoticeNo = b.GetNoticeNo "
                     + "		and b.OtherNoType = '2' "  //个单
                     + "		and hasLongRisk(b.OtherNo) = '" + type + "' "
                     + "   and a.MakeDate between '" + mStartDate + "' and '" + mEndDate + "' "  //生成时间
                     + "   and ManageCom = '" + manageCom + "' "
                     + ") t "
                     + "with ur ";
        System.out.println(sql);
       SSRS tSSRS = mExeSQL.execSQL(sql);
       if(mExeSQL.mErrors.needDealError())
       {
           CError tError = new CError();
           tError.moduleName = "PrtOperfeeDealBL";
           tError.functionName = "getOneGrpInfo";
           tError.errorMessage = "查询能个单数据出错";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return null;
       }

       if(tSSRS.getMaxRow() == 0 || tSSRS.GetText(1, 1).equals("0"))
       {
           String[] s = {"0", "0","0", "0"};
           return s;
       }

       return tSSRS.getRowData(1);
    }

    /**
     * getTeminateP
     *
     * @param manageCom String
     * @param type String
     * @return String[]
     */
    private String[] getTeminateP(String manageCom, String type)
    {
        String sql = "select count(ContNo), nvl(sum(Prem), 0) from LCCont a "
                    + "where a.StateFlag = '" + BQ.STATE_FLAG_TERMINATE + "' "  //终止状态
                    + " and a.ContType ='1'  and hasLongRisk(a.ContNo) = '" + type + "' "
                    + "   and CInvaliDate between '" + mStartDate + "' and '" + mEndDate + "' "  //取保单终止时间
                    + "   and ManageCom = '" + manageCom + "' "
                    + " and (a.cardflag = '0' or a.cardflag is null) "
                    + "with ur ";
       SSRS tSSRS = mExeSQL.execSQL(sql);
       if(mExeSQL.mErrors.needDealError())
       {
           CError tError = new CError();
           tError.moduleName = "PrtOperfeeDealBL";
           tError.functionName = "getOneGrpInfo";
           tError.errorMessage = "查询能个单数据出错";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return null;
       }

       if(tSSRS.getMaxRow() == 0 || tSSRS.GetText(1, 1).equals("0"))
       {
           String[] s =
                        {"0", "0", "0", "0"};
           return s;
       }

       return tSSRS.getRowData(1);
    }

    /**
     * getHBInfoP
     * 查询续保核保记录
     * @param manageCom String
     * @param type String
     * @return String[]
     */
    private String[] getHBInfoP(String manageCom, String type)
    {
        String[] info = new String[COL_NUM];

        ExeSQL e = new ExeSQL();
        String sql = "select Count(EdorNo), sum(Finish), sum(UnFinish) "
                     + "from( "
                     + "	select ManageCom, EdorNo, "
                     + "		case EdorState when '0' then 1 else 0 end Finish, "  //核保结束
                     + "		case EdorState when '0' then 0 else 1 end UnFinish "  //核保中
                     + "	from( "
                     + "		select distinct b.ManageCom, a.EdorNo, "
                     + "			(select EdorState from LPEdorApp where EdorAcceptNo = a.EdorAcceptNo) "  //核保状态
                     + "		from LPEdorItem a, LPPOl b "
                     + "		where a.EdorNo = b.EdorNo and a.EdorType = b.EdorType "
                     + "			and a.EdorType = 'XB' "  //核保工单
                     + "            and b.ManageCom = '" + manageCom + "' "
                     + "            and hasLongRisk(a.ContNo) = '" + type + "' "  //长短期保单
                     + "            and a.MakeDate between '" + mStartDate + "' and '" + mEndDate + "' "  //生成核保工单时间
                     + "	) t "
                     + ") tb "
                     + "with ur ";
        SSRS tSSRS = mExeSQL.execSQL(sql);
        if(mExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "PrtOperfeeDealBL";
            tError.functionName = "getOneGrpInfo";
            tError.errorMessage = "查询个单实收自缴数据出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
        if(tSSRS.getMaxRow() == 0 || tSSRS.GetText(1, 1).equals("0"))
        {
            info[0] = "0";
            info[1] = "0";
            info[2] = "0";
        }
        info[0] = tSSRS.GetText(1, 1);
        info[1] = tSSRS.GetText(1, 2);
        info[2] = tSSRS.GetText(1, 3);

        return info;
    }

    /**
     * getInvaliP
     * 个单失效
     * @param manageCom String
     * @param type String
     * @return String[]
     */
    private String[] getInvaliP(String manageCom, String type)
    {
        String sql = "select count(ContNo), nvl(sum(Prem), 0) from LCCont a "
                    + "where a.StateFlag = '" + BQ.STATE_FLAG_AVAILABLE + "' "  //失效
                    + " and a.ContType ='1'  and PayToDate between '" + mStartDate + "' and '" + mEndDate + "' "  //取交至日期查询
                    + "   and ManageCom = '" + manageCom + "' "
                    + "   and PayIntv > 0 "
                    + "	  and hasLongRisk(a.ContNo) = '" + type + "' "
                    + " and (a.cardflag = '0' or a.cardflag is null)"
                    + "with ur ";
       SSRS tSSRS = mExeSQL.execSQL(sql);
       if(mExeSQL.mErrors.needDealError())
       {
           CError tError = new CError();
           tError.moduleName = "PrtOperfeeDealBL";
           tError.functionName = "getOneGrpInfo";
           tError.errorMessage = "查询能个单数据出错";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return null;
       }

       if(tSSRS.getMaxRow() == 0 || tSSRS.GetText(1, 1).equals("0"))
       {
           String[] s = {"0", "0","0", "0"};
           return s;
       }

       return tSSRS.getRowData(1);
    }

    /**
     * getLJAPayInfoP
     * 查询个单核销记录
     * @param manageCom String
     * @param type String
     * @return String[]
     */
    private String[] getLJAPayInfoP(String manageCom, String type)
    {
        String[] info = new String[COL_NUM];

        ExeSQL e = new ExeSQL();
        //自缴
        String sql = "select count(distinct GetNoticeNo), sum(SumActuPayMoney) "
                     + "from LJAPayPerson a "
                     + "where MakeDate between '" + mStartDate + "' and '" + mEndDate + "' "  //核销时间
                     + "   and ManageCom = '" + manageCom + "' "
                     + "   and hasLongRisk(a.ContNo) = '" + type + "' ";
        String temp = sql + "  and exists (select paymode from ljtempfeeclass A,LJSPAYB B where A.GETNOTICENO = B.GETNOTICENO "
                      +" AND paymode !='4' and tempfeeno = a.GetNoticeNo AND B.OTHERNOTYPE ='2')"
                      + "with ur ";
        System.out.println(temp);
        SSRS tSSRS = mExeSQL.execSQL(temp);
        if(mExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "PrtOperfeeDealBL";
            tError.functionName = "getOneGrpInfo";
            tError.errorMessage = "查询个单实收自缴数据出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
        if(tSSRS.getMaxRow() == 0 || tSSRS.GetText(1, 1).equals("0"))
        {
            info[0] = "0";
            info[1] = "0";
        }
        else
        {
            info[0] = tSSRS.GetText(1, 1);
            info[1] = tSSRS.GetText(1, 2);
        }

        //银行
        temp = sql +  " and exists (select paymode from ljtempfeeclass A,LJSPAYB B where A.GETNOTICENO = B.GETNOTICENO "
                      +" AND paymode ='4' and tempfeeno = a.GetNoticeNo AND B.OTHERNOTYPE ='2')"
                      + "with ur ";
        System.out.println(temp);
        tSSRS = mExeSQL.execSQL(temp);
        if(mExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "PrtOperfeeDealBL";
            tError.functionName = "getOneGrpInfo";
            tError.errorMessage = "查询个单实收银行转帐数据出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }
        if(tSSRS.getMaxRow() == 0 || tSSRS.GetText(1, 1).equals("0"))
        {
            info[2] = "0";
            info[3] = "0";
        }
        else
        {
            info[2] = tSSRS.GetText(1, 1);
            info[3] = tSSRS.GetText(1, 2);
        }
        return info;
    }

    /**
     * getLJSPayInfoP
     * 个单生成应收的数据
     * @param manageCom String
     * @param type String
     * @return String[]
     */
    private String[] getLJSPayInfoP(String manageCom, String type)
    {
        String[] info = new String[COL_NUM];

        String sql = "select count(distinct GetNoticeNo), sum(SumActuPayMoney), count(distinct SelfGetNoticeNo)-1, Sum(SelfMoney) "
                     + "from ( "
                     + "	select GetNoticeNo, SumActuPayMoney, "
                     + "		case when BankAccNo is null then GetNoticeNo else '1' end SelfGetNoticeNo, "  //根据BankAccNo判断是否银行转帐，若是银行转帐，记一件，因为此处else '1' 不应记为一件,在外层查询求和中需要减1
                     + "        case when BankAccNo is null then SumActuPayMoney else 0 end SelfMoney "
                     + "	from LJSPayPersonB a "
                     + "	where exists (select 1 from LJSPayB where GetNoticeNo = a.GetNoticeNo and OtherNoType = '2' and DealState in('0', '1', '4')) "  //待收费、核销、待核销
                     + "       and PayType = 'ZC' "
                     + "       and ManageCom = '" + manageCom + "' "
                     + "       and hasLongRisk(a.ContNo) = '" + type + "' "
                     + "       and MakeDate between '" + mStartDate + "' and '" + mEndDate + "' "
                     + ") t "
                     + "with ur ";
        SSRS tSSRS = mExeSQL.execSQL(sql);
        if(mExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "PrtOperfeeDealBL";
            tError.functionName = "getOneGrpInfo";
            tError.errorMessage = "查询能团单数据出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        if(tSSRS.getMaxRow() == 0 || tSSRS.GetText(1, 1).equals("0"))
        {
            String[] s = {"0", "0","0", "0"};
            return s;
        }

        int sumCount = Integer.parseInt(tSSRS.GetText(1, 1));
        double sumMoney = Double.parseDouble(tSSRS.GetText(1, 2));
        int selfCount = Integer.parseInt(tSSRS.GetText(1, 3));
        double selfMoney = Double.parseDouble(tSSRS.GetText(1, 4));

        int bankCount = sumCount - selfCount;
        double bankMoney = PubFun.setPrecision(sumMoney - selfMoney, "0.00");

        info[0] = tSSRS.GetText(1, 3);
        info[1] = tSSRS.GetText(1, 4);
        info[2] = String.valueOf(bankCount);
        info[3] = String.valueOf(bankMoney);

        return info;
   }

    /**
     * getOneNoNameInfo
     * 查询团单行数据
     * @param manageComs SSRS
     * @return String[]
     */
    private String[] getOneGrpInfo(String manageCom, String type)
    {
       String[] info = new String[COL_NUM];

       String[] tLJsp = getLJSPayInfoG(manageCom, type);  //生成应收
       info[3] = tLJsp[0];
       info[4] = tLJsp[1];
       info[5] = tLJsp[2];
       info[6] = tLJsp[3];

       String[] tLJap = getLJAPayInfoG(manageCom, type);  //核销
       info[7] = tLJap[0];
       info[8] = tLJap[1];
       info[9] = tLJap[2];
       info[10] = tLJap[3];

       //核保
       info[11] = "0";
       info[12] = "0";
       info[13] = "0";

       String[] invali = getInvaliG(manageCom, type);
       info[14] = invali[0];
       info[15] = invali[1];

       String[] teminate = getTeminateG(manageCom, type);
       info[16] = teminate[0];
       info[17] = teminate[1];

       String[] hastenG = getPhoneHastenG(manageCom, type);
       info[18] = hastenG[0];
       info[19] = hastenG[1];
       info[20] = hastenG[2];
       info[21] = hastenG[3];

       String[] unchargeG = getUnChargeG(manageCom, type);
       info[22] = unchargeG[0];
       info[23] = unchargeG[1];

       return info;
    }

    /**
     * getUnChargeG
     *终止交费，取发生续期续保撤销的应收记录数
     * @param manageCom String
     * @param type String
     * @return String[]
     */
    private String[] getUnChargeG(String manageCom, String type)
    {
        String sql = "select Count(a.GrpContNo), nvl(sum(SumActuPayMoney), 0) from LJSPayGrpB a "
                    + "where DealState = '3' "  //续期应收撤销
                    + "	and exists (select 1 from LJSPayB where GetNoticeNo = a.GetNoticeNo and OtherNoType = '1' and ManageCom = '" + manageCom + "') "  //续期产生
                    + "	and isNoNameCont(a.GrpContNo) = '" + type + "' "
                    + "	and PayType = 'ZC' "
                    + " and not exists(select 1 from LCGrpPol where GrpContNo = a.GrpContNo and RiskCode in(select RiskCode from LMRiskApp where RiskType3 = '7' and risktype !='M')) "
                    + " and ModifyDate between '" + mStartDate + "' and '" + mEndDate + "' "
                    + "with ur ";  //取撤销时间
       SSRS tSSRS = mExeSQL.execSQL(sql);
       if(mExeSQL.mErrors.needDealError())
       {
           CError tError = new CError();
           tError.moduleName = "PrtOperfeeDealBL";
           tError.functionName = "getOneGrpInfo";
           tError.errorMessage = "查询能团单数据出错";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return null;
       }

       if(tSSRS.getMaxRow() == 0 || tSSRS.GetText(1, 1).equals("0"))
       {
           String[] s = {"0", "0","0", "0"};
           return s;
       }

       return tSSRS.getRowData(1);
    }

    /**
     * getInvaliG
     *查询团单电话催缴数据，催缴失败终止数=生成催缴件数 - 催缴成功件数 - 催缴过程中件数
     * @param manageCom String
     * @param type String
     * @return String[]
     */
    private String[] getPhoneHastenG(String manageCom, String type)
    {
        String sql = "select count(GetNoticeNo), sum(Suc), count(GetNoticeNo) - sum(Suc) - sum(UnFinish), sum(UnFinish) from "
                     + "( "
                     + "    select a.GetNoticeNo,"
                     + "		case FinishType when '1' then 1 when '4' then 1 else 0 end Suc, "   //重设缴费时间或核销的催缴
                     + "		case when (FinishType is null or FinishType = '') then 1 else 0 end UnFinish  "  //未结案催缴
                     + "	from LGPhoneHasten a, LJSPayB b "
                     + "	where a.GetNoticeNo = b.GetNoticeNo "
                     + "		and b.OtherNoType = '1' "  //团单应收
                     + "		and isNoNameCont(b.OtherNo) = '" + type + "' "
                    + "        and not exists(select 1 from LCGrpPol where GrpContNo = b.OtherNo and RiskCode in(select RiskCode from LMRiskApp where RiskType3 = '7' AND RISKTYPE !='M')) "
                     + "       and a.MakeDate between '" + mStartDate + "' and '" + mEndDate + "' "  //按生成催缴任务的时间查询
                     + "       and ManageCom = '" + manageCom + "' "
                     + ") t "
                     + "with ur ";
        System.out.println(sql);
       SSRS tSSRS = mExeSQL.execSQL(sql);
       if(mExeSQL.mErrors.needDealError())
       {
           CError tError = new CError();
           tError.moduleName = "PrtOperfeeDealBL";
           tError.functionName = "getOneGrpInfo";
           tError.errorMessage = "查询能团单数据出错";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return null;
       }

       if(tSSRS.getMaxRow() == 0)
       {
           String[] s = {"0", "0","0", "0"};
           return s;
       }

       return tSSRS.getRowData(1);
    }

    /**
     * getInvaliG
     *查询团单续保满期终止的保单，取保单满期日判断起止日期
     * @param manageCom String
     * @param type String
     * @return String[]
     */
    private String[] getTeminateG(String manageCom, String type)
    {
        String sql = "select count(GrpContNo), nvl(sum(Prem), 0) from LCGrpCont a "
                     + "where a.StateFlag = '" + BQ.STATE_FLAG_TERMINATE + "' "  //终止
                     + "   and isNoNameCont(a.GrpContNo) = '" + type + "' "
                     + "   and CInvaliDate between '" + mStartDate + "' and '" + mEndDate + "' "
                     + "   and ManageCom = '" + manageCom + "' "
                    + "    and not exists(select 1 from LCGrpPol where GrpContNo = a.GrpContNo and RiskCode in(select RiskCode from LMRiskApp where RiskType3 = '7' AND RISKTYPE !='M')) "
                    + " and a.cardflag is null "
                    + "with ur ";
       SSRS tSSRS = mExeSQL.execSQL(sql);
       if(mExeSQL.mErrors.needDealError())
       {
           CError tError = new CError();
           tError.moduleName = "PrtOperfeeDealBL";
           tError.functionName = "getOneGrpInfo";
           tError.errorMessage = "查询能团单数据出错";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return null;
       }

       if(tSSRS.getMaxRow() == 0 || tSSRS.GetText(1, 1).equals("0"))
       {
           String[] s = {"0", "0","0", "0"};
           return s;
       }

       return tSSRS.getRowData(1);
    }

    /**
     * getInvaliG
     *查询机构manageCom团单续期未收费失效数据，以险种交至日期为时间段
     * @param manageCom String
     * @param type String
     * @return String[]
     */
    private String[] getInvaliG(String manageCom, String type)
    {
        String sql = "select count(GrpContNo), nvl(sum(Prem), 0) from LCGrpCont a "
                    + "where a.StateFlag = '" + BQ.STATE_FLAG_AVAILABLE + "' "  //失效
                    + "   and (select min(PayToDate) from LCGrpPol where GrpContNo = a.GrpContNo) "
                    + "         between '" + mStartDate + "' and '" + mEndDate + "' "  //险种交至时间在起止日内
                    + "   and ManageCom = '" + manageCom + "' "
                    + "   and not exists(select 1 from LCGrpPol where GrpContNo = a.GrpContNo and RiskCode in(select RiskCode from LMRiskApp where RiskType3 = '7' AND RISKTYPE !='M')) "
                    + "   and PayIntv > 0 "  //可续期
                    + "	  and isNoNameCont(a.GrpContNo) = '" + type + "' "
                    + " and a.cardflag is null "
                    + "with ur ";
       SSRS tSSRS = mExeSQL.execSQL(sql);
       if(mExeSQL.mErrors.needDealError())
       {
           CError tError = new CError();
           tError.moduleName = "PrtOperfeeDealBL";
           tError.functionName = "getOneGrpInfo";
           tError.errorMessage = "查询能团单数据出错";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return null;
       }

       if(tSSRS.getMaxRow() == 0 || tSSRS.GetText(1, 1).equals("0"))
       {
           String[] s = {"0", "0","0", "0"};
           return s;
       }

       return tSSRS.getRowData(1);
    }

    /**
     * getLJAPayInfo
     * 查询机构manageCom的团单核销信息，从实收表中查询
     * @param manageCom String
     * @param type String
     * @return String
     */
    private String[] getLJAPayInfoG(String manageCom, String type)
    {
        String sql = "select count(distinct GetNoticeNo), nvl(Sum(SumActuPayMoney), 0), 0, 0 "
                    + "from LJAPayGrp a "
                    + "where exists(select 1 from LJSPayB where GetNoticeNo = a.GetNoticeNo and OtherNoType = '1' and ManageCom = '" + manageCom + "') "  //续期生成
                    + "	and isNoNameCont(a.GrpContNo) = '" + type + "' "  //是否无名单标志
                    + "	and PayType = 'ZC' "
                    + " and not exists(select 1 from LCGrpPol where GrpContNo = a.GrpContNo and RiskCode in(select RiskCode from LMRiskApp where RiskType3 = '7' AND RISKTYPE !='M')) "
                    + " and MakeDate between '" + mStartDate + "' and '" + mEndDate + "' "
                    + "with ur ";
       SSRS tSSRS = mExeSQL.execSQL(sql);
       if(mExeSQL.mErrors.needDealError())
       {
           CError tError = new CError();
           tError.moduleName = "PrtOperfeeDealBL";
           tError.functionName = "getOneGrpInfo";
           tError.errorMessage = "查询能团单数据出错";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return null;
       }

       if(tSSRS.getMaxRow() == 0 || tSSRS.GetText(1, 1).equals("0"))
       {
           String[] s = {"0", "0","0", "0"};
           return s;
       }

       return tSSRS.getRowData(1);
    }

    /**
     * 查询机构managecom的团单生成应收信息，从应收备份表中读取
     * @param manageCom String
     * @param type String
     * @return String[]
     */
    private String[] getLJSPayInfoG(String manageCom, String type)
    {
        String sql = "select count(distinct GetNoticeNo), nvl(Sum(SumActuPayMoney), 0), 0, 0 "
                    + "from LJSPayGrpB a "
                    + "where DealState in('0', '1', '4') "  //待收费、核销、待核销
                    + "   and exists (select 1 from LJSPayB where GetNoticeNo = a.GetNoticeNo and OtherNoType = '1' and ManageCom = '" + manageCom + "') "  //续期生成的应收
                    + "	  and isNoNameCont(a.GrpContNo) = '" + type + "' "  //是否无名单
                    + "	  and PayType = 'ZC' "
                    + "   and not exists(select 1 from LCGrpPol where GrpContNo = a.GrpContNo and RiskCode in(select RiskCode from LMRiskApp where RiskType3 = '7' AND RISKTYPE !='M')) "
                    + "   and MakeDate between '" + mStartDate + "' and '" + mEndDate + "' "
                    + "with ur ";
       System.out.println(sql);
       SSRS tSSRS = mExeSQL.execSQL(sql);
       if(mExeSQL.mErrors.needDealError())
       {
           CError tError = new CError();
           tError.moduleName = "PrtOperfeeDealBL";
           tError.functionName = "getOneGrpInfo";
           tError.errorMessage = "查询能团单数据出错";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return null;
       }

       if(tSSRS.getMaxRow() == 0 || tSSRS.GetText(1, 1).equals("0"))
       {
           String[] s = {"0", "0","0", "0"};
           return s;
       }

       return tSSRS.getRowData(1);
    }

    /**
     * getManageComs
     *查询符合条件的机构
     * @return String[]
     */
    private SSRS getManageComs()
    {
        String sql = "select distinct ManageCom from LJSPayB a where DealState in('0', '1', '4', '3') "
                     + "   and a.OtherNoType in('1', '2') "
                     + "   and MakeDate between '" + mStartDate + "' "
                     + "   and '" + mEndDate + "' "
                     + "   and ManageCom like '" + mManageCom + "%' "

                     + "union "
                     + "select distinct Managecom from LJAPay a "
                     + "where exists(select 1 from LJSPayB where GetNoticeNo = a.GetNoticeNo and OtherNoType in('1', '2')) "
                     + "   and MakeDate between '" + mStartDate + "' "
                     + "   and '" + mEndDate + "' "
                     + "   and ManageCom like '" + mManageCom + "%' "

                     + "union "
                     + "select distinct b.ManageCom "
                     + "from LPEdorItem a, LPPOl b "
                     + "where a.EdorNo = b.EdorNo and a.EdorType = b.EdorType "
                     + "   and a.EdorType = 'XB' "
                     + "   and a.MakeDate between '" + mStartDate + "' "
                     + "       and '" + mEndDate + "' "
                     + "   and b.ManageCom like '" + mManageCom + "%' "

                     + "union "
                     + "select distinct ManageCom from LCGrpCont a "
                     + "where StateFlag in('2', '3') "
                     + "   and (select max(PayToDate) from LCGrpPol where GrpContNo = a.GrpContNo) between '" + mStartDate + "' "
                     + "       and '" + mEndDate + "' "
                     + "   and ManageCom like '" + mManageCom + "%' "
                     + "union "
                     + "select distinct ManageCom from LCCont "
                     + "where StateFlag in('2', '3') "
                     + "   and PayToDate between '" + mStartDate + "' "
                     + "       and '" + mEndDate + "' "
                     + "   and ManageCom like '" + mManageCom + "%' "
                     + "union "
                     + "select distinct b.ManageCom from LGPhoneHasten a, LJSPayB b "
                     + "where a.GetNoticeNo = b.GetNoticeNo "
                     + "   and b.OtherNoType in('1', '2') "
                     + "   and a.MakeDate between '" + mStartDate + "' and '" + mEndDate + "' "
                     + "   and b.ManageCom like '" + mManageCom + "%' ";
        System.out.println(sql);
        SSRS tSSRS = mExeSQL.execSQL(sql);
        if(mExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "PrtOperfeeDealBL";
            tError.functionName = "getManageComs";
            tError.errorMessage = "查询报表机构出错";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return null;
        }

        return tSSRS;
    }

    private boolean checkData()
    {
        mStartDate = (String) mTransferData
                           .getValueByName(FeeConst.STARTDATE);  //开始日期
        mEndDate = (String) mTransferData.getValueByName(FeeConst.ENDDATE);  //截至日期
        mManageCom = (String) mTransferData
                           .getValueByName(FeeConst.MANAGECOM);  //管理机构

        if(mStartDate == null || mStartDate.equals("")
           || mEndDate == null || mEndDate.equals("")
            || mManageCom == null || mManageCom.equals(""))
       {
           CError tError = new CError();
           tError.moduleName = "PrtContSuccessBL";
           tError.functionName = "dealData";
           tError.errorMessage = "应收起止日期个和管理机构均不能为空";
           mErrors.addOneError(tError);
           System.out.println(tError.errorMessage);
           return false;
       }

        return true;
    }

    /**
     * 得到录入的数据
     * @param data VData：包括TransferData对象和GlobalInput对象
     * @return boolean: 成功true，否则false
     */
    private boolean getInputData(VData data)
    {
        mGI = (GlobalInput) data.getObjectByObjectName("GlobalInput", 0);
        mTransferData = (TransferData) data
                        .getObjectByObjectName("TransferData", 0);

        if(mGI == null || mGI.Operator == null || mGI.ManageCom == null
            || mTransferData == null)
        {
            CError tError = new CError();
            tError.moduleName = "PrtContSuccessBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "";
            mErrors.addOneError(tError);
            System.out.println(tError.errorMessage);
            return false;
        }

        return true;
    }

    /**
     * 处理业务逻辑，生成打印数据XmlExport。
     * @return boolean:操作成功true，否则false
     */
    private boolean dealData()
    {
        xmlexport.createDocument("PrtOperfeeDeal.vts", "printer");

        if(!dealHead())
        {
            return false;
        }

        if(!dealMJ())
        {
            return false;
        }

        if(!createXmlExport())
        {
            return false;
        }

        xmlexport.outputDocumentToFile("D:\\", "PrtOperfeeDeal");

        return true;
    }

    /**
     * dealMJ
     *生成满期结算清单
     * @return boolean
     */
    private boolean dealMJ()
    {
        String sql =
        //满期结算未结案，从
            "select ManageCom, count(EndCount), sum(EndMoney), sum(XBCount), sum(XBMoney), sum(MJCount), sum(MJMoney), "
            + "   case when (sum(XBCount) + sum(MJCount)) = 0 then 0 else sum(limit)/(sum(XBCount) + sum(MJCount)) end "
            + "from ( "
            //未结案
            + "	select ManageCom, count(WorkNo) EndCount, sum(money) EndMoney, 0 XBCount,0 XBMoney,0 MJCount,0 MJMoney,0 limit "
            + "	from ( "
            + "		select (select ManageCom from LCGrpCont where GrpContNo = a.ContNo "
            + "				union select ManageCom from LBGrpCont where GrpContNo = a.ContNo) ManageCom, a.WorkNo WorkNo, "
            + "			case b.EdorValue "
            + "				when '0' then (select sum(decimal(EdorValue, 20, 2)) from LPEdorEspecialData where EdorNo = b.EdorNo and EdorType = b.EdorType and DetailType in('ACCBALAFIXED','ACCBALAGROUP','ACCBALAINSURED')) "
            + "				else (select sum(InsuAccBala) from LCInsureAcc where GrpContNo = a.ContNo) "
            + "			end money "
            + "		from LGWork a, LPEdorEspecialData b "
            + "		where a.WorkNo = b.EdorNo "
            + "			and b.EdorType = 'MJ' "
            + "			and b.DetailType = 'MJSTATE' "
            + "         and a.MakeDate between '" + mStartDate + "' and '" + mEndDate + "' "
            + "			and (exists (select 1 from LCGrpCont where GrpContNo = a.ContNo and ManageCom like '" + mManageCom + "%') "
            + "					or exists (select 1 from LBGrpCont where GrpContNo = a.ContNo and ManageCom like '" + mManageCom + "%')) "
            + "	) t "
            + "	group by ManageCom "
            + "	union "
              //结案
            + "	select a.ManageCom ManageCom, 0 EndCount, 0 EndMoney, "
            + "		case b.EdorValue when '1' then 1 else 0 end XBCount, "
            + "		case b.EdorValue when '1' then abs(sum(GetMoney)) else 0 end XBMoney, "
            + "		case b.EdorValue when '2' then 1 else 0 end MJCount, "
            + "		case b.EdorValue when '2' then abs(sum(GetMoney)) else 0 end MJMoney, "
            + "		days(a.MakeDate) - days((select MakeDate from LGWork where WorkNo = b.EdorNo)) limit "
            + "	from LJAGetEndorse a, LPEdorEspecialData b "
            + "	where a.EndorsementNo = b.EdorNo "
            + "		and a.FeeOperationType = b.EdorType "
            + "		and a.FeeOperationType = 'MJ' "
            + "		and b.DetailType = 'ENDTIME_DEAL' "
            + "		and a.MakeDate between '" + mStartDate + "' and '" + mEndDate + "' "
            + "		and a.ManageCom like '" + mManageCom + "%' "
            + "	group by a.ManageCom, a.EndorsementNo, b.EdorValue, b.EdorNo, a.MakeDate "
            + ") t "
            + "group by ManageCom "
            + "with ur ";
            System.out.println(sql);
        SSRS tSSRS = mExeSQL.execSQL(sql);
        if(mExeSQL.mErrors.needDealError())
        {
            CError tError = new CError();
            tError.moduleName = "PrtContSuccessBL";
            tError.functionName = "dealMJ";
            tError.errorMessage = "查询特需清单出错";
            mErrors.addOneError(tError);
            System.out.println(mExeSQL.mErrors.getErrContent());
            System.out.println(tError.errorMessage);
            return false;
        }

        if(tSSRS.getMaxRow() == 0)
        {
            return true;
        }
        String[] sumInfo = null;
        int sumDays = 0;
        ListTable tListTable = new ListTable();
        for(int i = 1; i <= tSSRS.getMaxRow(); i++)
        {
            String[] info = new String[COL_NUM];

            info[0] = GetMemberInfo.getComNameByComCode(tSSRS.GetText(i, 1));
            info[1] = StrTool.cTrim(tSSRS.GetText(i, 2));
            info[2] = StrTool.cTrim(tSSRS.GetText(i, 3));
            info[3] = StrTool.cTrim(tSSRS.GetText(i, 4));
            info[4] = StrTool.cTrim(tSSRS.GetText(i, 5));
            info[5] = StrTool.cTrim(tSSRS.GetText(i, 6));
            info[6] = StrTool.cTrim(tSSRS.GetText(i, 7));
            info[7] = StrTool.cTrim(tSSRS.GetText(i, 8));
            tListTable.add(info);
            sumInfo = com.sinosoft.lis.bq.CommonBL.add(sumInfo, info, 1);
            sumDays =(Integer.parseInt(info[3])+Integer.parseInt(info[5]))*Integer.parseInt(info[7]);

        }
        sumInfo[7]=String.valueOf(sumDays);
        String[] info = getInfoMJ(sumInfo);
        info[0] = "全系统";
        tListTable.add(info);

        tListTable.setName("MJ");

        String[] colNames = {"机构","满期件数","满期帐户金额",
                            "续保件数","续保金额", "时效",
                            "满期终止件数","满期终止给付金额", "时效",
                            "平均时效"};

        xmlexport.addListTable(tListTable, colNames);

        return true;
    }
    /**
 * 生成特需表行数据
 * @param tSSRS SSRS：查询的到的数据
 * @param i int：行标
 * @return String[]
 */
private String[] getInfoMJ(String[] row)
{
    String[] info = new String[COL_NUM];

    info[0] = StrTool.cTrim(row[0]);
    info[1] = StrTool.cTrim(row[1]);
    info[2] = StrTool.cTrim(row[2]);
    info[3] = StrTool.cTrim(row[3]);
    info[4] = StrTool.cTrim(row[4]);
    info[5] = StrTool.cTrim(row[5]);
    info[6] = StrTool.cTrim(row[6]);
    //时效
    int avg;
    int count = Integer.parseInt(info[3])+Integer.parseInt(info[5]);
    if(count==0)
    {
        avg = 0;
    }
    else
    {
        avg = Integer.parseInt(row[7]) / count;
    }
    info[7] = String.valueOf(avg);
    return info;
}


    /**
     * 计算data中件数-时效的平均总时效
     * @param data String[][]
     * @return double
     */
    private double getPeriod(String[][] data)
    {
        if(data == null)
        {
            return 0;
        }

        int count = 0;
        int days = 0;
        for(int i = 0; i < data.length; i++)
        {
            count += Integer.parseInt(data[i][0]);
            days += Integer.parseInt(data[i][1]);
        }

        if(count == 0)
        {
            return 0;
        }

        return PubFun.setPrecision(days/count, "0.00");
    }

    /**
     * dealHead
     *生成抬头信息
     * @return boolean
     */
    private boolean dealHead()
    {
        TextTag tag = new TextTag();
        tag.add("ManageCom", mGI.ManageCom);
        tag.add("ManageComName", GetMemberInfo.getComName(mGI.Operator));
        tag.add("Operator", mGI.Operator);
        tag.add("OperatorName", GetMemberInfo.getMemberName(mGI));
        tag.add("PrintDate", PubFun.getCurrentDate());
        tag.add("StartDate", StrTool.cTrim(this.mStartDate));
        tag.add("EndDate", StrTool.cTrim(this.mEndDate));

        String com = StrTool.cTrim(this.mManageCom);
        tag.add("ManageComSelect", com);
        tag.add("ManageComNameSelect",
                com.equals("") ? "" : GetMemberInfo.getComNameByComCode(com));

        String group03 = StrTool.cTrim((String) mTransferData
                                       .getValueByName(FeeConst.GROUP03));
        tag.add("Group01", group03);
        tag.add("Group01Name", getGroupName(group03));

        xmlexport.addTextTag(tag);

        return true;
    }

    /**
     * 得到agentGroup对应名字
     * @param agentGroup String
     * @return String
     */
    private String getGroupName(String agentGroup)
    {
        if("".equals(StrTool.cTrim(agentGroup)))
        {
            return "";
        }

        String sql = "select Name "
                     + "from LABranchGroup "
                     + "where AgentGroup = '" + agentGroup + "' ";
        return new ExeSQL().getOneValue(sql);
    }

    /**
     * 由于部、区、处的隶属关系，只需要的到最小级别的团体即可
     * @return String
     */
    private String groupPart(String table)
    {
        if(table == null )
        {
            table = "";
        }
        else
        {
            table = table + ".";
        }

        String group01 = (String) mTransferData.getValueByName(FeeConst.GROUP01); //部
        String group02 = (String) mTransferData.getValueByName(FeeConst.GROUP02); //区
        String group03 = (String) mTransferData.getValueByName(FeeConst.GROUP03); //处

        String group = null;
        if(group03 != null && !group03.equals(""))
        {
            group = group03;
        }
        else if(group02 != null && !group02.equals(""))
        {
            group = group02;
        }
        else if(group01 != null && !group01.equals(""))
        {
            group = group01;
        }

        if(group == null)
        {
            return " ";
        }

        return "   and " + table + "AgentGroup in  (select AgentGroup from LABranchGroup "
            + "where BranchSeries like '" + group + "%') ";
    }

    /**
     * 业务员
     * @return String
     */
    private String agentPart(String table)
    {
        if(table == null)
        {
            table = " ";
        }
        else
        {
            table = table + ".";
        }

        String agentCode = (String) mTransferData
                           .getValueByName(FeeConst.AgentCode); //销售人员代码
        if(agentCode != null && !agentCode.equals(""))
        {
            return "   and " + table + "AgentCode = '" + agentCode + "' ";
        }
        return " ";
    }

    /**
     * 页面选择的机构
     * @return String
     */
    private String manageComPart()
    {
        return "   and ManageCom like '" + mManageCom + "%' ";
    }

    public static void main(String[] args)
    {
        GlobalInput mGlobalInput = new GlobalInput(); //操作员信息
        mGlobalInput.Operator = "pa0001";
        mGlobalInput.ManageCom = "86";
        mGlobalInput.ComCode = mGlobalInput.ManageCom;

        TransferData t = new TransferData();
        t.setNameAndValue(FeeConst.STARTDATE, "2006-01-01");
        t.setNameAndValue(FeeConst.ENDDATE, "2007-1-24");
        t.setNameAndValue(FeeConst.MANAGECOM, "86110000");
        t.setNameAndValue(FeeConst.GROUP03, "");
        t.setNameAndValue(FeeConst.GROUP02, "");
        t.setNameAndValue(FeeConst.GROUP01, "");


        VData d = new VData();
        d.add(mGlobalInput);
        d.add(t);

        PrtOperfeeDealBL bl = new PrtOperfeeDealBL();
        if(bl.getXmlExport(d, "") == null)
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
