package com.sinosoft.lis.operfee;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.utility.*;
import com.sinosoft.task.Task;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.lis.bq.CommonBL;


//程序名称：CreateGrpWorkNoBL.java
//程序功能：
//创建日期：2008-11-13 
//创建人  ：Zhang Guoming
//更新记录：  更新人    更新日期     更新原因/内容

public class CreateGrpWorkNoBL
{
    public CErrors mErrors = new CErrors();
    private GlobalInput mGI = new GlobalInput();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private String mWorkNo = null;
    
    private MMap map = new MMap();

    public CreateGrpWorkNoBL()
    {
    }

    public boolean submitData(VData data, String operate)
    {
    	if(!getInputData(data))
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }
        
        PubSubmit tPubSubmit = new PubSubmit();
        VData tVData = new VData();
        tVData.add(map);
        if(!tPubSubmit.submitData(tVData, ""))
        {
            System.out.println(tPubSubmit.mErrors.getErrContent());
            mErrors.addOneError("提交数库失败");
            return false;
        }
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData()
    {
        MMap tMMap = createTask();
        if(tMMap == null)
        {
            return false;
        }
        map.add(tMMap);
        return true;
    }

    /**
     * createTask
     *
     * @return MMap
     */
    private MMap createTask()
    {
        LGWorkSchema tLGWorkSchema = new LGWorkSchema();
        tLGWorkSchema.setCustomerNo(mLCGrpContSchema.getAppntNo());
        tLGWorkSchema.setInnerSource("LPJ");
        tLGWorkSchema.setAcceptWayNo(Task.ACCEPTWAY_INNER);
        tLGWorkSchema.setStatusNo(Task.WORKSTATUS_UNDO);
        tLGWorkSchema.setApplyName("自动");
        tLGWorkSchema.setTypeNo("03");
        tLGWorkSchema.setContNo(mLCGrpContSchema.getGrpContNo());
        tLGWorkSchema.setRemark("自动批注：团险理赔金账户变更");
        String workBoxNo = getWorkBox();
        if(workBoxNo == null)
        {
        	workBoxNo = "";
        }

        VData tVData = new VData();
        tVData.add(tLGWorkSchema);
        tVData.add(mGI);
        tVData.add(workBoxNo);

        TaskInputBL tTaskInputBL = new TaskInputBL();
        MMap tMMap = tTaskInputBL.getSubmitData(tVData, "");
        if(tMMap == null)
        {
            mErrors.addOneError("生成工单信息失败:\n"
                + tTaskInputBL.mErrors.getFirstError());
            return null;
        }
        mWorkNo = tTaskInputBL.getWorkNo();
        return tMMap;
    }

    private String getWorkBox()
    {
        String sql = "select WorkBoxNo from LGWorkBox "
                     + "where OwnerNo = '" + mGI.Operator + "' ";
        ExeSQL tExeSQL = new ExeSQL();
        String tWorkBox = tExeSQL.getOneValue(sql);
        if(tWorkBox == null || tWorkBox.equals("") || tWorkBox.equals("null"))
        {
            mErrors.addOneError("没有查询到您的工单信息，不能继续处理业务");
            return null;
        }
        return tWorkBox;
    }

    private boolean getInputData(VData data)
    {
        mGI.setSchema((GlobalInput)data.getObjectByObjectName("GlobalInput", 0));
        if(mGI == null || mGI.Operator == null)
        {
            mErrors.addOneError("没有获取到操作员信息");
            return false;
        }
        mLCGrpContSchema = (LCGrpContSchema)data.getObjectByObjectName("LCGrpContSchema", 0);
        return true;
    }
    
    public String getWorkNo()
    {
    	return mWorkNo;
    }

    public static void main(String[] args)
    {
        CreateGrpWorkNoBL bl = new CreateGrpWorkNoBL();
    }
}
