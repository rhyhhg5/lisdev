package com.sinosoft.lis.operfee;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author GUOXIANG
 * @version 1.0
 */

import java.util.*;
import java.text.*;
import java.lang.*;
import java.io.*;
import com.f1j.ss.*;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.bl.LCContBL;


public class ShowActuFeeList {

  public CErrors mErrors=new CErrors();
  private VData mResult = new VData();

  private GlobalInput mGlobalInput =new GlobalInput() ;
  private String mOperate="";
  private String mManageCom="";
  private String mStartDate="";
  private String mEndDate="";
  private String mPolType="";
  private TransferData mTransferData=new TransferData();
  private BookModelImpl m_book = new BookModelImpl();
  private String  mFileModeDesc = "ActuFeeMode.xls"; //描述的模版名称
  private String  mFilePathDesc = "CreatListPath";  //描述的文件路径
  private String  mFilePath     = "";  //通过描述得到的文件路径
  private String  mFileName    = "";  //要生成的文件名
  private String  mModeRealPath="";   //模板的父路径，即ui对应在server的绝对路径
  private String  mRelationFlag="0"; //续期和续保是否关联标记 0-不相关 1-相关
  private int mCurrentRow=1; //行数
  private int mCount=100; //每次循环处理的纪录数

  public ShowActuFeeList()
  {
  }

  public static void main(String[] args)
  {
      ShowActuFeeList tShowActuFeeList =new ShowActuFeeList();
      VData tVData=new VData();
      TransferData tTransferData =new TransferData();
      tTransferData.setNameAndValue("ManageCom","86");
      tTransferData.setNameAndValue("StartDate","2004-9-25");
      tTransferData.setNameAndValue("EndDate","2007-9-25");
      tTransferData.setNameAndValue("PolType","2");
      GlobalInput tGI=new GlobalInput();
      tGI.ComCode="86";
      tGI.Operator="001";
      tVData.add(tTransferData);
      tVData.add(tGI);
      tShowActuFeeList.submitData(tVData,"");

  }

  /**
   * 传输数据的公共方法
   * @param cInputData
   * @param cOperate
   * @return
   */
  public boolean submitData(VData cInputData, String cOperate) {

      mOperate = cOperate;

      if( getInputData(cInputData)==false )
          return false;

        if(dealData()==false)
            return false;

        return true;

  }

  /**
   * 从输入数据中得到所有对象
   * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData cInputData)
  {
      mTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData",0);
      mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0);

      if(mTransferData==null||mGlobalInput==null)
      {
          buildError("getInputData","没有得到传入的数据");
          return false;
      }
      mManageCom=(String)mTransferData.getValueByName("ManageCom");
      mStartDate=(String)mTransferData.getValueByName("StartDate");
      mEndDate=(String)mTransferData.getValueByName("EndDate");
      mPolType=(String)mTransferData.getValueByName("PolType");//在职-1/孤儿单-0
      mModeRealPath=(String)mTransferData.getValueByName("ModeRealPath");
      if(mPolType==null||mPolType.equals(""))
      {
          buildError("getInputData","必须录入在职或孤儿单标记");
          return false;
      }
      if(mManageCom==null||mManageCom.equals("")) mManageCom=mGlobalInput.ComCode;
      if(mStartDate==null||mEndDate==null)
      {
          buildError("getInputData","必须传入起始日期");
          return false;
      }
      if(mStartDate.equals("")||mEndDate.equals(""))
      {
          buildError("getInputData","必须传入起始日期");
          return false;
      }
      int d=PubFun.calInterval(mStartDate,mEndDate,"D");
      if(d<0)
      {
          buildError("getInputData","起始日期不能小于终止日期");
          return false;
      }
      return true;
  }

  public VData getResult()
  {
      return this.mResult;
  }

  private void buildError(String szFunc, String szErrMsg)
  {
      CError cError = new CError( );
      cError.moduleName = "ShowActuFeeList";
      cError.functionName = szFunc;
      cError.errorMessage = szErrMsg;
      this.mErrors.addOneError(cError);
  }

// 准备所有要打印的数据
  private boolean dealData()
  {
      //数据量大时-可以做for循环
      try
      {
          if(checkDesc()==false)
              return false;

          m_book.read(mModeRealPath+mFilePath+mFileModeDesc,new com.f1j.ss.ReadParams());
          //m_book.read("E:/temp/"+mFileModeDesc,new com.f1j.ss.ReadParams());
          m_book.setSheetSelected(0,true);
          m_book.setCol(0);

          //续期主险实收
          if(dealLJAPay()==false)
              return false;
          //如果主险续期和附加险自动续保关联并且主险自动续保记入主险续期
        /*杨红于2005-07-27将此段注销，以后再添加
         if(mRelationFlag.equals("2"))
          {
              //处理实收表中续保的主险保单
              if(dealRnewMainPol()==false)
                  return false;
              //处理实收表中续保的附加险，其主险不需要续期缴费和续保
              if(dealRnewSpecSubPol()==false)
                  return false;
          }*/
          m_book.write(mModeRealPath+mFilePath+mFileName,new com.f1j.ss.WriteParams(com.f1j.ss.BookModelImpl.eFileExcel97));
          //m_book.write("E:/temp/"+mFileName,new com.f1j.ss.WriteParams(com.f1j.ss.BookModelImpl.eFileExcel97));
      }
      catch(Exception ex)
      {
          System.out.println(ex);
          buildError("FormateFile","操作失败："+ex);
          return false;
      }

      return true;
  }

  /**
   * 得到文件名
   * @return
   */
  private String getFileName()
  {
      String StartDate[]=PubFun.split(mStartDate,"-");
      String EndDate[]=PubFun.split(mEndDate,"-");
      //文件名=ActuFeeList+"_"+操作员代码+"_"+起始年月日+"_"+终止年月日+"_"+孤儿单在职单.xls.z
      String filename="ActuFeeList_"+mGlobalInput.Operator+"_";
      filename= filename+StartDate[0];
      if(StartDate[1].length()==1) filename= filename+"0";
      filename= filename+StartDate[1];
      if(StartDate[2].length()==1) filename= filename+"0";
      filename= filename+StartDate[2];
      filename= filename+"_";
      filename= filename+EndDate[0];
      if(EndDate[1].length()==1) filename= filename+"0";
      filename= filename+EndDate[1];
      if(EndDate[2].length()==1) filename= filename+"0";
      filename= filename+EndDate[2];
      filename= filename+"_";
      filename= filename+mPolType;
      filename= filename+".xls.z";
      System.out.println("生成文件名:"+filename);
      return filename;
  }

  /**
   * 根据条件判断：代理人是否在职
   * @param agentCode
   * @return
   */
  private boolean judgePolType(String agentCode)
  {
      if(!mPolType.equals("1")&&!mPolType.equals("0"))
      {
          return true;
      }

      LAAgentDB tLAAgentDB=new LAAgentDB();
      tLAAgentDB.setAgentCode(agentCode);
      if(tLAAgentDB.getInfo()==false)
      {
          return false;
      }
      String AgentState=tLAAgentDB.getAgentState();
      //如果需要是在职
      if(mPolType.equals("1"))
      {
          if(AgentState.equals("01")||AgentState.equals("02"))
          {
              return true;
          }
          return false;
      }
      else //如果需要是不在职
      {
          if(AgentState.equals("01")||AgentState.equals("02"))
          {
              return false;
          }
          return true;
      }
  }
  private boolean dealLJAPay()
  {
    String sqlStr="select * from ljapay where makedate>='"+mStartDate+"' and MakeDate<='"+mEndDate+"'"
        +" and IncomeType='2' and ManageCom like '"+mManageCom+"%%'";
    LJAPayDB tLJAPayDB=new LJAPayDB();
    LJAPaySet tLJAPaySet=new LJAPaySet();
    tLJAPaySet=tLJAPayDB.executeQuery(sqlStr);
    if(tLJAPaySet.size()==0)
    {
      CError.buildErr(this, "没有符合指定时间范围的实收数据!");
      return false;
    }
    for(int i=1;i<=tLJAPaySet.size();i++)
    {
      String payNo= tLJAPaySet. get(i).getPayNo(); //交费收据号
      String contNo=tLJAPaySet. get(i).getIncomeNo();//个单合同号
      LCContDB tLCContDB=new LCContDB();
      tLCContDB.setContNo(contNo);
      if(!tLCContDB.getInfo())
      {
        //CError.buildErr(this,"个单信息丢失，请与系统维护人员联系！");
       // return false;
       continue;
      }
      if(judgePolType(tLCContDB.getAgentCode())==false)//判断是在职单还是孤儿单，根据代理人的agentstate判断
       continue;
      LCContBL tLCContBL=new LCContBL();
      tLCContBL.setSchema(tLCContDB.getSchema());
      String AgentName="";
      String AgentBranch="";
     if(tLCContBL.getAgentCode()!=null)
         AgentName=ChangeCodetoName.getAgentName(tLCContBL.getAgentCode());
       if(tLCContBL.getAgentGroup()!=null)
        AgentBranch = findAgentBranch(tLCContBL.getAgentGroup());
      String SexName="";
     String SaleChnl="";
     if(tLCContBL.getSaleChnl()!=null)
       SaleChnl=ChangeCodetoName.getSaleChnl(tLCContBL.getSaleChnl());
     if(tLCContBL.getAppntSex()!=null)
        SexName = ChangeCodetoName.getSexName(tLCContBL.getAppntSex());
      String PayMode="";
      if(tLCContBL.getPayLocation()!=null)
        PayMode = ChangeCodetoName.getPayLocationName(tLCContBL.getPayLocation());
      String PostalAddress="";//收费地址
     String ZipCode="";
     String Phone="";
     LCAppntDB tLCAppntDB =new LCAppntDB();
     tLCAppntDB.setContNo(contNo);
     if(tLCAppntDB.getInfo())
     {
       //PostalAddress=tLCAppntDB.getPo
       //ZipCode=tLCAppntDB.getzip
       String addressNo=tLCAppntDB.getAddressNo();
       LCAddressDB tLCAddressDB=new LCAddressDB();
       tLCAddressDB.setAddressNo(addressNo);
       tLCAddressDB.setCustomerNo(tLCContBL.getAppntNo());
       if(tLCAddressDB.getInfo())
       {
         PostalAddress=tLCAddressDB.getPostalAddress();
         ZipCode=tLCAddressDB.getZipCode();
         Phone=tLCAddressDB.getPhone();
       }
     }


     try
     {
       m_book.setEntry(mCurrentRow,0,tLCContBL.getManageCom());//管理机构
       m_book.setEntry(mCurrentRow,1,AgentName);//业务员姓名
       m_book.setEntry(mCurrentRow,2,tLCContBL.getAgentCode());//业务员代码
       m_book.setEntry(mCurrentRow,3,AgentBranch);//业务员组别
       m_book.setEntry(mCurrentRow,4,tLCContBL.getAppntName());//客户姓名
       m_book.setEntry(mCurrentRow,5,SexName);
       m_book.setEntry(mCurrentRow,6,tLCContBL.getContNo());//保单号
       m_book.setEntry(mCurrentRow,7,tLJAPaySet. get(i).getMakeDate());//核销日期
       m_book.setEntry(mCurrentRow,8,PayMode);
       m_book.setEntry(mCurrentRow,9,String.valueOf(tLJAPaySet.get(i).getSumActuPayMoney()));//实交金额
       m_book.setEntry(mCurrentRow,10,PostalAddress);//投保人收费地址
       m_book.setEntry(mCurrentRow,11,ZipCode);//邮编
       m_book.setEntry(mCurrentRow,12,Phone);
       m_book.setEntry(mCurrentRow,13,SaleChnl);
     }
     catch(Exception e)
     {
       e.printStackTrace();//此语句仅供bug调试！
     }
     mCurrentRow++;
    }
    return true;
  }
  /**
   * 续期主险实收
   * @return
   */
  private boolean dealLJAPay2()
  {
      LJAPayPersonSet tLJAPayPersonSet = new LJAPayPersonSet();
      String MaxManageCom=PubFun.RCh(mManageCom,"9",8);
      String MinManageCom=PubFun.RCh(mManageCom,"0",8);

      LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
      String sqlhead="select count( distinct payno ) from ljapayperson";
      String strSQL=" where MakeDate>='"+mStartDate+"' and MakeDate<='"+mEndDate+"'";
      strSQL=strSQL+" and GrpPolNo='00000000000000000000'";
      strSQL=strSQL+" and (PayTypeFlag!='1' or PayTypeFlag is null)";
      strSQL=strSQL+" and ManageCom>='"+MinManageCom+"' and ManageCom<='"+MaxManageCom+"'";
      strSQL=strSQL+" and PayCount>1 ";
      strSQL=strSQL+" and length(trim(DutyCode))!=10 ";
      String SQL_Count=sqlhead+strSQL;
      System.out.println("查询续期主险实收:"+SQL_Count);

      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS=tExeSQL.execSQL(SQL_Count);
      String strCount=tSSRS.GetText(1,1);
      int SumCount = Integer.parseInt(strCount);
      int CurrentCounter=1;
      String SQL_PayNo="select distinct payno from ljapayperson"+strSQL;
      //循环处理数据
      LJAPayDB tLJAPayDB = new LJAPayDB();
      LJAPaySchema tLJAPaySchema = new LJAPaySchema();
      while(CurrentCounter<=SumCount)
      {
          tExeSQL = new ExeSQL();
          tSSRS=tExeSQL.execSQL(SQL_PayNo,CurrentCounter,mCount);
          if(tSSRS==null)
          {
              buildError("dealDueMainPol","实收个人表读取失败");
              return false;
          }

          for( int i = 1; i <=tSSRS.getMaxRow(); i++ )
          {
              tLJAPaySchema = new LJAPaySchema();
              tLJAPayDB = new LJAPayDB();
              tLJAPayDB.setPayNo(tSSRS.GetText(i,1));
              tLJAPayDB.getInfo();
              tLJAPaySchema.setSchema(tLJAPayDB.getSchema());
              //如果是预交保费，则不计入实收表
              if(tLJAPaySchema.getSumActuPayMoney()==0)
                  continue;
              //判断在职/孤儿单--如果不符合条件-继续下一条
              if(judgePolType(tLJAPaySchema.getAgentCode())==false)
                  continue;
              if(dealSinglePol(tLJAPaySchema)==false)
                  return false;
          }

          CurrentCounter=CurrentCounter+mCount; //计数器增加
          //考虑是否有方法减少内存的大数据量，可以将m_book数据写到excel文件,m_book清空
          //打开excel文件，从mCurrentRow位置继续写数据
      }
      return true;
  }

  /**
   * 取描述信息
   * @return
   */
  private boolean checkDesc()
  {
      //查询系统变量-是否主险续期和附加险的自动续保关联-如果关联，则续期应收保费和续保应收保费数据存放在同一个清单
      LDSysVarDB tLDSysVarDB=new LDSysVarDB();
      tLDSysVarDB.setSysVar("relationflag");
      if(tLDSysVarDB.getInfo()==false)
      {
      }
      else
      {
          mRelationFlag=tLDSysVarDB.getSysVarValue();
      }
      //取清单模版文件存放路径（即要生成文件的存放路径）
      tLDSysVarDB=new LDSysVarDB();
      tLDSysVarDB.setSysVar(mFilePathDesc);
      if(tLDSysVarDB.getInfo()==false)
      {
          buildError("checkDesc","LDSysVar取文件路径("+mFilePathDesc+")描述失败");
          return false;
      }
      mFilePath=tLDSysVarDB.getSysVarValue();
      mFileName=getFileName();
      return true;
  }

  /**
   * 添加每个保单的信息-针对续期主险
   * @param tLCPolSchema
   * @param t_book
   * @param n
   * @return
   */
  private boolean dealSinglePol(LJAPaySchema tLJAPaySchema)
  {
      int t=0;
      int subNum=0;
      String dianjiaoflag="N";
      try
      {
          String sqlCal="";
          ExeSQL tExeSQL = new ExeSQL();
          SSRS tSSRS=new SSRS();
          //如有数据错误，可以写在excel里
          m_book.setEntry(mCurrentRow,t+0,tLJAPaySchema.getManageCom());//管理机构
          m_book.setEntry(mCurrentRow,t+1,ChangeCodetoName.getAgentName(tLJAPaySchema.getAgentCode()));//业务员姓名
          m_book.setEntry(mCurrentRow,t+2,tLJAPaySchema.getAgentCode());//业务员代码
          String AgentBranch=findAgentBranch(tLJAPaySchema.getAgentGroup());
          m_book.setEntry(mCurrentRow,t+3,AgentBranch);//业务员组别

          m_book.setEntry(mCurrentRow,t+5,tLJAPaySchema.getIncomeNo());//保单号

          //得到应缴费日期
          sqlCal="select LastPayToDate,getnoticeno from ljapayperson where payno='"+tLJAPaySchema.getPayNo()+"'" ;
          tExeSQL = new ExeSQL();
          tSSRS=tExeSQL.execSQL(sqlCal);
          String LastPayToDate=tSSRS.GetText(1,1);
          m_book.setEntry(mCurrentRow,t+6,LastPayToDate);//应缴费日期

          m_book.setEntry(mCurrentRow,t+7,tLJAPaySchema.getMakeDate());//实际核销日期
          String PayMode=findPayMode(tLJAPaySchema.getIncomeNo(),tSSRS.GetText(1,2));
          m_book.setEntry(mCurrentRow,t+8,String.valueOf(PayMode));//缴费方式

          //险种信息
          m_book.setEntry(mCurrentRow,t+9,ChangeCodetoName.getRiskName(tLJAPaySchema.getRiskCode()));//主险险种
          //得到主险保费
          sqlCal="select sum(SumActuPayMoney) from ljapayperson where payno='"+tLJAPaySchema.getPayNo()+"'"
                        +" and paycount>1 and paytype='ZC' and riskcode='"+tLJAPaySchema.getRiskCode()+"'" ;
          tExeSQL = new ExeSQL();
          tSSRS=tExeSQL.execSQL(sqlCal);
          String MainPrem=tSSRS.GetText(1,1);

          m_book.setEntry(mCurrentRow,t+10,MainPrem);//主险保费
          double sumprem=0.0;//保费合计
          sumprem=sumprem+Double.parseDouble(MainPrem);

          //如果主险续期和附加险自动续保关联-查询需要续保的附加险
          if(!mRelationFlag.equals("0"))
          {
              LJAPayPersonSet tLJAPayPersonSet=findSubPol(tLJAPaySchema);
              if(tLJAPayPersonSet.size()>0)
              {
                  LCPolDB tLCPolDB=new LCPolDB();
                  tLCPolDB.setPolNo(tLJAPayPersonSet.get(1).getPolNo()) ;
                  if(tLCPolDB.getInfo()==true)
                  {
                      if(tLCPolDB.getPolState()!=null)
                      {
                          if(tLCPolDB.getPolState().substring(0,3).equals("0101"))
                          {
                              dianjiaoflag="Y";
                          }
                      }
                  }
                  //如果多于一条，则在excel的下一行的同一列位置显示，同时mSubNum增长
                  for(int i=1;i<=tLJAPayPersonSet.size();i++)
                  {
                      m_book.setEntry(mCurrentRow+i-1,t+11,ChangeCodetoName.getRiskName(tLJAPayPersonSet.get(i).getRiskCode()));//附险险种
                      m_book.setEntry(mCurrentRow+i-1,t+12,String.valueOf(tLJAPayPersonSet.get(i).getSumActuPayMoney() ));//附险保费
                      sumprem=sumprem+tLJAPayPersonSet.get(i).getSumActuPayMoney();
                  }
                  subNum=subNum+tLJAPayPersonSet.size()-1;
              }
          }

          m_book.setEntry(mCurrentRow,t+13,String.valueOf(sumprem));//保费合计
          //投保人信息
          LCAppntIndDB tLCAppntIndDB =new LCAppntIndDB();
          tLCAppntIndDB.setPolNo(tLJAPaySchema.getIncomeNo());
          tLCAppntIndDB.setCustomerNo(tLJAPaySchema.getAppntNo());
          if(tLCAppntIndDB.getInfo()==true)
          {
              m_book.setEntry(mCurrentRow,t+4,tLCAppntIndDB.getName());//客户姓名
              m_book.setEntry(mCurrentRow,t+14,tLCAppntIndDB.getPostalAddress());//收费地址
              m_book.setEntry(mCurrentRow,t+15,tLCAppntIndDB.getZipCode());//邮编
              m_book.setEntry(mCurrentRow,t+16,tLCAppntIndDB.getPhone());//电话
          }

          LCPolDB tLCPolDB=new LCPolDB();
          tLCPolDB.setPolNo(tLJAPaySchema.getIncomeNo()) ;
          if(tLCPolDB.getInfo()==true)
          {
              if(tLCPolDB.getPolState()!=null)
              {
                  if(tLCPolDB.getPolState().trim().equals("01010001"))
                  {
                      System.out.println("");

                  }
                  if(tLCPolDB.getPolState().substring(0,4).equals("0101"))
                  {
                      dianjiaoflag="Y";
                  }
              }
          }
          m_book.setEntry(mCurrentRow,t+17,dianjiaoflag);//垫交标记
      }
      catch(Exception ex)
      {
          try
          {
              m_book.setEntry(mCurrentRow,t+0,"***处理实收表"+tLJAPaySchema.getPayNo()+" 出错："+ex);//管理机构
              mCurrentRow=mCurrentRow+1;   //处理完每条纪录后添加一行
          }
          catch(Exception ex2)
          {
              buildError("dealSinglePol",ex.toString());
              return false;
          }
          return true;
      }
      mCurrentRow=mCurrentRow+subNum;//如果有超过1个以上附加险，添加新的行存放第二个（以上）附加险信息
      mCurrentRow=mCurrentRow+1;   //处理完每条纪录后添加一行
      return true;
  }

  /**
   * 得到展业机构外部编码-中文名称
   * @param tAgentGroup
   * @return
   */
  private String findAgentBranch(String tAgentGroup)
  {
      LABranchGroupDB tLABranchGroupDB = new LABranchGroupDB();
      tLABranchGroupDB.setAgentGroup(tAgentGroup) ;
      if(tLABranchGroupDB.getInfo()==false)
          return "unknow";
      else
          return tLABranchGroupDB.getName();
        //return tLABranchGroupDB.getBranchAttr();
  }

  /**
   * 查询实收总表中续保缴费的附加险-其主险是续期
   * @param pLJAPaySchema
   * @return
   */
  private LJAPayPersonSet findSubPol(LJAPaySchema pLJAPaySchema)
  {
      LJAPayPersonSet tLJAPayPersonSet=new LJAPayPersonSet();
      LJAPayPersonSchema tLJAPayPersonSchema=new LJAPayPersonSchema();

      String strSQL="select riskcode,Sum(SumActuPayMoney) from ljapayperson where payno ='";
      strSQL=strSQL+pLJAPaySchema.getPayNo() +"'";
      strSQL=strSQL+" and PayTypeFlag='1' and paytype='ZC' group by riskcode";

      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS=tExeSQL.execSQL(strSQL);
      if(tSSRS==null)
          return tLJAPayPersonSet;
      for( int i = 1; i <=tSSRS.getMaxRow(); i++ )
      {
          tLJAPayPersonSchema=new LJAPayPersonSchema();
          tLJAPayPersonSchema.setRiskCode(tSSRS.GetText(i,1));
          tLJAPayPersonSchema.setSumActuPayMoney(tSSRS.GetText(i,2));
          tLJAPayPersonSet.add(tLJAPayPersonSchema);
      }
      return tLJAPayPersonSet;
  }


  /**
   * 处理实收表中续保的主险保单
   * @return
   */
  private boolean dealRnewMainPol()
  {
      String sqlhead="select count(*) from LJAPay ";
      String sqlStr=" where (MakeDate>='"+mStartDate+"' and MakeDate<='"+mEndDate+"' ) ";
      sqlStr=sqlStr+" and (PayTypeFlag!='1' or PayTypeFlag is null)";
      sqlStr=sqlStr+" and RiskCode in (select riskcode from LMRiskApp where SubRiskFlag='M') ";
      String MaxmManageCom=PubFun.RCh(mManageCom,"9",8);
      String MinmManageCom=PubFun.RCh(mManageCom,"0",8);
      sqlStr=sqlStr+" and ManageCom>='"+MinmManageCom+"' and ManageCom<='"+MaxmManageCom+"' order by ManageCom";

      String SQL_Count=sqlhead+sqlStr;
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS=tExeSQL.execSQL(SQL_Count);
      String strCount=tSSRS.GetText(1,1);
      int SumCount = Integer.parseInt(strCount);
      int CurrentCounter=1;
      String SQL_PayNo="select PayNo from LJAPay "+sqlStr;

      if(SumCount>0)
      {
          addPrompt("***以下是主险自动续保数据***");
      }
      LJAPayDB tLJAPayDB = new LJAPayDB();
      LJAPaySchema tLJAPaySchema = new LJAPaySchema();
      //如果基数大与个人保单纪录数，跳出循环
      while(CurrentCounter<=SumCount)
      {
          tExeSQL = new ExeSQL();
          tSSRS=tExeSQL.execSQL(SQL_PayNo,CurrentCounter,mCount);
          if(tSSRS==null)
          {
              buildError("dealDueMainPol","实收总表读取失败");
              return false;
          }

          for( int i = 1; i <=tSSRS.getMaxRow(); i++ )
          {
              tLJAPaySchema = new LJAPaySchema();
              tLJAPayDB = new LJAPayDB();
              tLJAPayDB.setPayNo(tSSRS.GetText(i,1));
              tLJAPayDB.getInfo();
              tLJAPaySchema.setSchema(tLJAPayDB.getSchema());
              //判断在职/孤儿单--如果不符合条件-继续下一条
              if(judgePolType(tLJAPaySchema.getAgentCode())==false)
                  continue;
              if(dealSingleRnewPol(tLJAPaySchema)==false)
                  return false;
          }

          CurrentCounter=CurrentCounter+mCount; //计数器增加
          //考虑是否有方法减少内存的大数据量，可以将m_book数据写到excel文件,m_book清空
          //打开excel文件，从mCurrentRow位置继续写数据
      }
      return true;
  }

  /**
   * 不同类型的数据之间添加说明
   * @return
   */
  private boolean addPrompt(String sPrompt)
  {
      try
      {
          //如有数据错误，可以写在excel里
          m_book.setEntry(mCurrentRow,0,sPrompt);//管理机构
          mCurrentRow=mCurrentRow+1;
      }
      catch(Exception ex)
      {
//          buildError("addPrompt","添加说明“"+sPrompt+"失败");
//          return false;
      }
      return true;
  }

  /**
   * 添加每个保单的信息-针对自动续保主险
   * @param tLCPolSchema
   * @param t_book
   * @param n
   * @return
   */
  private boolean dealSingleRnewPol(LJAPaySchema tLJAPaySchema)
  {
      int t=0;
      int subNum=0;
      String dianjiaoflag="N";
      try
      {
          String sqlCal="";
          ExeSQL tExeSQL = new ExeSQL();
          SSRS tSSRS=new SSRS();

          //如有数据错误，可以写在excel里
          m_book.setEntry(mCurrentRow,t+0,tLJAPaySchema.getManageCom());//管理机构
          m_book.setEntry(mCurrentRow,t+1,ChangeCodetoName.getAgentName(tLJAPaySchema.getAgentCode()));//业务员姓名
          m_book.setEntry(mCurrentRow,t+2,tLJAPaySchema.getAgentCode());//业务员代码
          String AgentBranch=findAgentBranch(tLJAPaySchema.getAgentGroup());
          m_book.setEntry(mCurrentRow,t+3,AgentBranch);//业务员组别

          m_book.setEntry(mCurrentRow,t+5,tLJAPaySchema.getIncomeNo());//保单号

          //得到应缴费日期
          sqlCal="select LastPayToDate,getnoticeno from ljapayperson where payno='"+tLJAPaySchema.getPayNo()+"'" ;
          tExeSQL = new ExeSQL();
          tSSRS=tExeSQL.execSQL(sqlCal);
          String LastPayToDate=tSSRS.GetText(1,1);
          m_book.setEntry(mCurrentRow,t+6,LastPayToDate);//应缴费日期

          m_book.setEntry(mCurrentRow,t+7,tLJAPaySchema.getMakeDate());//实际核销日期
          String PayMode=findPayMode(tLJAPaySchema.getIncomeNo(),tSSRS.GetText(1,2));
          m_book.setEntry(mCurrentRow,t+8,String.valueOf(PayMode));//缴费方式

          //险种信息
          //得到主险保费
          m_book.setEntry(mCurrentRow,t+9,ChangeCodetoName.getRiskName(tLJAPaySchema.getRiskCode()));//主险险种
          m_book.setEntry(mCurrentRow,t+10,String.valueOf(tLJAPaySchema.getSumActuPayMoney()));//主险保费
          double sumprem=0.0;//保费合计
          sumprem=sumprem+tLJAPaySchema.getSumActuPayMoney();

          LJAPayPersonSet tLJAPayPersonSet=findSubPol(tLJAPaySchema);
          if(tLJAPayPersonSet.size()>0)
          {
              LCPolDB tLCPolDB=new LCPolDB();
              tLCPolDB.setPolNo(tLJAPayPersonSet.get(1).getPolNo()) ;
              if(tLCPolDB.getInfo()==true)
              {
                  if(tLCPolDB.getPolState()!=null)
                  {
                      if(tLCPolDB.getPolState().substring(0,3).equals("0101"))
                      {
                          dianjiaoflag="Y";
                      }
                  }
              }
              //如果多于一条，则在excel的下一行的同一列位置显示，同时mSubNum增长
              for(int i=1;i<=tLJAPayPersonSet.size();i++)
              {
                  m_book.setEntry(mCurrentRow+i-1,t+11,ChangeCodetoName.getRiskName(tLJAPayPersonSet.get(i).getRiskCode()));//附险险种
                  m_book.setEntry(mCurrentRow+i-1,t+12,String.valueOf(tLJAPayPersonSet.get(i).getSumActuPayMoney() ));//附险保费
                  sumprem=sumprem+tLJAPayPersonSet.get(i).getSumActuPayMoney();
              }
              subNum=subNum+tLJAPayPersonSet.size()-1;
          }

          m_book.setEntry(mCurrentRow,t+13,String.valueOf(sumprem));//保费合计
          //投保人信息
          LCAppntIndDB tLCAppntIndDB =new LCAppntIndDB();
          tLCAppntIndDB.setPolNo(tLJAPaySchema.getIncomeNo());
          tLCAppntIndDB.setCustomerNo(tLJAPaySchema.getAppntNo());
          if(tLCAppntIndDB.getInfo()==true)
          {
              m_book.setEntry(mCurrentRow,t+4,tLCAppntIndDB.getName());//客户姓名
              m_book.setEntry(mCurrentRow,t+14,tLCAppntIndDB.getPostalAddress());//收费地址
              m_book.setEntry(mCurrentRow,t+15,tLCAppntIndDB.getZipCode());//邮编
              m_book.setEntry(mCurrentRow,t+16,tLCAppntIndDB.getPhone());//电话
          }
          LCPolDB tLCPolDB=new LCPolDB();
          tLCPolDB.setPolNo(tLJAPaySchema.getIncomeNo()) ;
          if(tLCPolDB.getInfo()==true)
          {
              if(tLCPolDB.getPolState()!=null)
              {
                  if(tLCPolDB.getPolState().substring(0,3).equals("0101"))
                  {
                      dianjiaoflag="Y";
                  }
              }
          }
          m_book.setEntry(mCurrentRow,t+17,dianjiaoflag);//垫交标记

      }
      catch(Exception ex)
      {
          try
          {
              m_book.setEntry(mCurrentRow,t+0,"***处理实收总表"+tLJAPaySchema.getPayNo()+" 出错："+ex);//管理机构
              mCurrentRow=mCurrentRow+1;   //处理完每条纪录后添加一行
          }
          catch(Exception ex2)
          {
              buildError("dealSinglePol",ex.toString());
              return false;
          }
          return true;
      }
      mCurrentRow=mCurrentRow+subNum;//如果有超过1个以上附加险，添加新的行存放第二个（以上）附加险信息
      mCurrentRow=mCurrentRow+1;   //处理完每条纪录后添加一行
      return true;
  }


  /**
   * 处理需要续保的附加险，其主险不需要续期缴费和续保
   * @return
   */
  private boolean dealRnewSpecSubPol()
  {

      String sqlhead="select count(*) from LJAPay ";
      String sqlStr=" where (MakeDate>='"+mStartDate+"' and MakeDate<='"+mEndDate+"' ) ";
      sqlStr=sqlStr+" and PayTypeFlag='1'";
      sqlStr=sqlStr+" and RiskCode in (select riskcode from LMRiskApp where SubRiskFlag='S') ";
      String MaxmManageCom=PubFun.RCh(mManageCom,"9",8);
      String MinmManageCom=PubFun.RCh(mManageCom,"0",8);
      sqlStr=sqlStr+" and ManageCom>='"+MinmManageCom+"' and ManageCom<='"+MaxmManageCom+"' order by ManageCom";

      String SQL_Count=sqlhead+sqlStr;
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS=tExeSQL.execSQL(SQL_Count);
      String strCount=tSSRS.GetText(1,1);
      int SumCount = Integer.parseInt(strCount);
      int CurrentCounter=1;
      String SQL_PolNo="select PayNo,IncomeNo from LJAPay "+sqlStr;

      if(SumCount>0)
      {
          addPrompt("***以下是附加险续保数据(其主险不需要续期缴费和续保)***");
      }
      LJAPayDB tLJAPayDB = new LJAPayDB();
      LJAPaySchema tLJAPaySchema = new LJAPaySchema();
      //如果基数大与个人保单纪录数，跳出循环
      while(CurrentCounter<=SumCount)
      {
          tExeSQL = new ExeSQL();
          tSSRS=tExeSQL.execSQL(SQL_PolNo,CurrentCounter,mCount);
          if(tSSRS==null)
          {
              buildError("dealDueMainPol","保单读取失败");
              return false;
          }

          for( int i = 1; i <=tSSRS.getMaxRow(); i++ )
          {
              tLJAPaySchema = new LJAPaySchema();
              tLJAPayDB = new LJAPayDB();
              tLJAPayDB.setPayNo(tSSRS.GetText(i,1));
              tLJAPayDB.getInfo();
              tLJAPaySchema.setSchema(tLJAPayDB.getSchema());
              //判断主险是续期或自动续保-跳过
              if(judgeMainRisk(tSSRS.GetText(i,2))==false)
                  continue;
              if(judgePolType(tLJAPaySchema.getAgentCode())==false)
                  continue;
              if(dealSingleSubPol(tLJAPaySchema)==false)
                  return false;
          }

          CurrentCounter=CurrentCounter+mCount; //计数器增加
          //考虑是否有方法减少内存的大数据量，可以将m_book数据写到excel文件,m_book清空
          //打开excel文件，从mCurrentRow位置继续写数据
      }
      return true;
  }

  /**
   * 判断主险是否符合条件：非续期，非续保，未到期
   * @param tPolNo   主险号
   * @param tPayEndDate  附加险终交日期
   * @return
   */
  private boolean judgeMainRisk(String tPolNo)
  {
      LCPolDB tLCPolDB=new LCPolDB();
      String strSQL="select * from lcpol where polno='"+ tPolNo +"' ";
      strSQL=strSQL+" and appflag='1'";
      strSQL=strSQL+" and RnewFlag!=-1 "; //不是自动续保的
      strSQL=strSQL+" and payintv=0 ";//不是续期的
      //如果不符合条件-则返回false
      LCPolSet tLCPolSet=tLCPolDB.executeQuery(strSQL);
      if(tLCPolSet.size()==0)
          return false;

      return true;
  }


  /**
   * 处理单个需要自动续保的附加险的清单数据
   * @param tLCPolSchema
   * @return
   */
  private boolean dealSingleSubPol(LJAPaySchema tLJAPaySchema)
  {
      int t=0;
      String dianjiaoflag="N";
      try
      {
          String sqlCal="";
          ExeSQL tExeSQL = new ExeSQL();
          SSRS tSSRS=new SSRS();
          //如有数据错误，可以写在excel里
          m_book.setEntry(mCurrentRow,t+0,tLJAPaySchema.getManageCom());//管理机构
          m_book.setEntry(mCurrentRow,t+1,ChangeCodetoName.getAgentName(tLJAPaySchema.getAgentCode()));//业务员姓名
          m_book.setEntry(mCurrentRow,t+2,tLJAPaySchema.getAgentCode());//业务员代码
          String AgentBranch=findAgentBranch(tLJAPaySchema.getAgentGroup());
          m_book.setEntry(mCurrentRow,t+3,AgentBranch);//业务员组别

          m_book.setEntry(mCurrentRow,t+5,tLJAPaySchema.getIncomeNo());//保单号

          //得到应缴费日期
          sqlCal="select LastPayToDate,polno,getnoticeno from ljapayperson where payno='"+tLJAPaySchema.getPayNo()+"'" ;
          tExeSQL = new ExeSQL();
          tSSRS=tExeSQL.execSQL(sqlCal);
          String LastPayToDate=tSSRS.GetText(1,1);
          String tPolNo=tSSRS.GetText(1,2);
          m_book.setEntry(mCurrentRow,t+6,LastPayToDate);//应缴费日期

          m_book.setEntry(mCurrentRow,t+7,tLJAPaySchema.getMakeDate());//实际核销日期
          String PayMode=findPayMode(tLJAPaySchema.getIncomeNo(),tSSRS.GetText(1,3));
          m_book.setEntry(mCurrentRow,t+8,String.valueOf(PayMode));//缴费方式
          //险种信息
          //m_book.setEntry(mCurrentRow,t+9,ChangeCodetoName.getRiskName(tLCPolSchema.getRiskCode()));//主险险种
          //m_book.setEntry(mCurrentRow,t+10,String.valueOf(tLCPolSchema.getPrem()));//主险保费
          double sumprem=0.0;//保费合计
          m_book.setEntry(mCurrentRow,t+11,ChangeCodetoName.getRiskName(tLJAPaySchema.getRiskCode()));//附险险种
          m_book.setEntry(mCurrentRow,t+12,String.valueOf(tLJAPaySchema.getSumActuPayMoney() ));//附险保费
          sumprem=sumprem+tLJAPaySchema.getSumActuPayMoney();
          m_book.setEntry(mCurrentRow,t+13,String.valueOf(sumprem));//保费合计

          //投保人信息
          LCAppntIndDB tLCAppntIndDB =new LCAppntIndDB();
          tLCAppntIndDB.setPolNo(tLJAPaySchema.getIncomeNo());
          tLCAppntIndDB.setCustomerNo(tLJAPaySchema.getAppntNo());
          if(tLCAppntIndDB.getInfo()==true)
          {
              m_book.setEntry(mCurrentRow,t+4,tLCAppntIndDB.getName());//客户姓名
              m_book.setEntry(mCurrentRow,t+14,tLCAppntIndDB.getPostalAddress());//收费地址
              m_book.setEntry(mCurrentRow,t+15,tLCAppntIndDB.getZipCode());//邮编
              m_book.setEntry(mCurrentRow,t+16,tLCAppntIndDB.getPhone());//电话
          }
          LCPolDB tLCPolDB=new LCPolDB();
          tLCPolDB.setPolNo(tPolNo) ;
          if(tLCPolDB.getInfo()==true)
          {
              if(tLCPolDB.getPolState()!=null)
              {
                  if(tLCPolDB.getPolState().substring(0,3).equals("0101"))
                  {
                      dianjiaoflag="Y";
                  }
              }
          }
          m_book.setEntry(mCurrentRow,t+17,dianjiaoflag);//垫交标记
      }
      catch(Exception ex)
      {
          try
          {
              m_book.setEntry(mCurrentRow,t+0,"***处理实收总表"+tLJAPaySchema.getIncomeNo()+" 出错："+ex);
              mCurrentRow=mCurrentRow+1;   //处理完每条纪录后添加一行
          }
          catch(Exception ex2)
          {
              buildError("dealSinglePol",ex.toString());
              return false;
          }
          return true;
      }
      mCurrentRow=mCurrentRow+1;   //处理完每条纪录后添加一行
      return true;
  }

  /**
   * 找交费方式
   * @param tPolNo
   * @return
   */
  private String findPayMode(String tPolNo,String tGetNoticeno)
  {

      String SQL="select tempfeeno from LJTempFee where otherno='"+tPolNo+"' and confflag='1'  and tempfeeno='"+tGetNoticeno+"' order by makedate desc";
      ExeSQL tExeSQL = new ExeSQL();
      SSRS tSSRS=tExeSQL.execSQL(SQL);
      if(tSSRS==null)
          return "unknow";
      String tempfeeno=tSSRS.GetText(1,1);

      LJTempFeeClassDB tLJTempFeeClassDB=new LJTempFeeClassDB();
      tLJTempFeeClassDB.setTempFeeNo(tempfeeno);
      LJTempFeeClassSet tLJTempFeeClassSet=tLJTempFeeClassDB.query();
      if(tLJTempFeeClassSet.size()==0)
          return "unknow";
      else
          return ChangeCodetoName.getPayModeName(tLJTempFeeClassSet.get(1).getPayMode());
  }
}

