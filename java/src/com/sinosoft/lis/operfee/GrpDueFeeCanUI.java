package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;

public class GrpDueFeeCanUI
{
  public  CErrors mErrors=new CErrors();
  public GrpDueFeeCanUI()
  {
  }

  public static void main(String[] args)
  {
  }

  public boolean submitData(VData cInputData, String cOperate)
  {
    GrpDueFeeCanBL tGrpDueFeeCanBL = new GrpDueFeeCanBL();
    tGrpDueFeeCanBL.submitData(cInputData,cOperate);
    if (tGrpDueFeeCanBL .mErrors .needDealError())
    {
        this.mErrors.copyAllErrors(tGrpDueFeeCanBL.mErrors);
        return false;
    }
    return true;
  }


}
