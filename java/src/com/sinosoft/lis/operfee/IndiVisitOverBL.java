package com.sinosoft.lis.operfee;

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.task.Task;
import java.util.ArrayList;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 *  个险续期续保电话催缴结案逻辑处理类，接收IndiVisitOverUI传入的数据，
 * 进行工单结案业务逻辑的处理。
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class IndiVisitOverBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = new GlobalInput(); //完整的操作员信息
    private String mSql = null; //本次查询工单的sql
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private MMap map = new MMap();  //保存处理后的数据
    private ArrayList mWorkNoArray = new ArrayList();  //待处理的工单号

    public IndiVisitOverBL()
    {
    }

    /**
     * 操作的提交方法，作为页面数据的入口，准备完数据后执行入库操作。
     * @param cInputData VData：包含：
     a)	TransferData对象，存储本次查询工单的sql
     b)	GlobalInput对象，完整的登陆用户信息
     * @param cOperate String：操作类型，此可为空字符串“”
     * @return boolean：操作成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        MMap map = getSubmitMap(cInputData, cOperate);
        if((map == null || map.size() == 0) && mErrors.needDealError())
        {
            return false;
        }

        VData data = new VData();
        data.add(map);
        PubSubmit tPubSubmit = new PubSubmit();
        if(tPubSubmit.submitData(data, "") == false)
        {
            mErrors.addOneError("更新数据失败");
            return false;
        }

        return true;
    }

    /**
     * 操作的提交方法，作为页面数据的入口进行结案业务逻辑处理。
     * @param cInputData VData：包含：
     a)	TransferData对象，存储本次查询工单的sql
     b)	GlobalInput对象，完整的登陆用户信息
     * @param cOperate String：操作类型，此可为空字符串“”
     * @return MMap:处理后的数据集合，若失败则为null
     */
    public MMap getSubmitMap(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return null;
        }
        if(!checkData())
        {
            return null;
        }

        //进行业务处理
        if(!dealData())
        {
            return null;
        }

        return map;
    }

    /**
     * dealData
     * 进行工单结案业务逻辑，置工单状态为结案：Task.WORKSTATUS_DONE
     * @return boolean：操作成功true，否则false
     */
    private boolean dealData()
    {
        for(int i = 0; i < this.mWorkNoArray.size(); i++)
        {
            String workNo = (String) mWorkNoArray.get(i);

            String sql = "Update LGWork set " +
                  "StatusNo = '" + Task.WORKSTATUS_DONE + "', " +
                  "CurrDoing='1', " +
                  "Operator = '" + mGlobalInput.Operator + "', " +
                  "ModifyDate = '" + CurrentDate + "', " +
                  "ModifyTime = '" + CurrentTime + "' " +
                  "Where  WorkNo = '" + workNo + "' ";
            map.put(sql, "UPDATE"); //修改
        }

        return true;
    }

    /**
     * checkData
     *校验本次催收记录是否可结案，必须每个催缴任务都录入了回访结果
     * @return boolean:校验通过true，否则false
     */
    private boolean checkData()
    {
        System.out.println(mSql);
        SSRS tSSRS = new ExeSQL().execSQL(mSql);
        if(tSSRS.mErrors.needDealError())
        {
            mErrors.addOneError("查询催缴任务失败");
            return false;
        }

        if(tSSRS.getMaxRow() > 0)
        {
            String unSaveWork = "";
            boolean passflag = true;
            for(int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                String sql = "  select a.workNo, b.statusNo "
                             + "from LGPhoneHasten a, LGWork b "
                             + "where a.workNo = b.workNo "
                             + "   and a.getNoticeNo = '" + tSSRS.GetText(i, 3) + "' "
                             + "   and a.workNo = '"
                             + tSSRS.GetText(i, tSSRS.getMaxCol()) + "' "
                             + "   and (a.finishType is not null "
                             + "   and a.finishType != '') "
                             + "   ";
                System.out.println(sql);
                SSRS rs = new ExeSQL().execSQL(sql);
                if(rs.getMaxRow() != 0)
                {
                    //为处理完毕的工单
                    if("5678".indexOf(rs.GetText(1, 2)) >= 0)
                    {
                        continue;
                    }
                    mWorkNoArray.add(tSSRS.GetText(i, tSSRS.getMaxCol()));
                }
                else
                {
                    passflag = false;
                    unSaveWork += "工单号" + tSSRS.GetText(i, tSSRS.getMaxCol())
                        + "应收记录号" + tSSRS.GetText(i, 3) + ", ";
                }
            }
            if(!passflag)
            {
                mErrors.addOneError("以下催缴任务未录入回访结果，不能结案："
                                    + unSaveWork);
                return false;
            }
            if(mWorkNoArray.size() == 0)
            {
                mErrors.addOneError("没有需要进行结案的工单");
                return false;
            }
        }

        return true;
    }

    /**
     * getInputData
     *从输入数据中得到所有对象
     * @param cInputData VData：getSubmitMap中转入的对象
     * @return boolean：操作成功true，否则false
     */
    private boolean getInputData(VData cInputData)
    {
        mGlobalInput = ((GlobalInput) cInputData
                        .getObjectByObjectName("GlobalInput", 0));
        TransferData tTransferData = (TransferData) cInputData
                                     .getObjectByObjectName("TransferData", 0);
        if(mGlobalInput == null || mGlobalInput.Operator == null
           || tTransferData == null)
        {
            mErrors.addOneError("传入的数据不完整");
            return false;
        }
        mSql = (String) tTransferData.getValueByName("Sql");
        if(mSql == null || mSql.equals(""))
        {
            mErrors.addOneError("没有得到查询催缴任务的Sql");
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        String sql = "select b.contNo, a.serialNo, a.getNoticeNo, a.sumDuePayMoney,    (select min(lastPayToDate) from LJSPayPersonB where '1156838275000'='1156838275000' and  getNoticeNo = a.getNoticeNo),    (select codename from ldcode where codetype='paymode' and code=b.PayMode),    a.payDate, getAgentName(b.AgentCode), ShowManageName(a.ManageCom),    case c.FinishType when '0' then '??????·?' when '1' then '???è??·?????' else '' end, d.workNo from LJSPayB a, LCCont b, LGPhoneHasten c, LGWorkTrace d where a.otherNo = b.contNo    and a.getNoticeNo = c.getNoticeNo    and b.appntNo = c.customerNo    and c.workNo =d.workNo    and d.workBoxNo = (select workBoxNo from LGWorkBox where ownerNo = 'pa0004')    and b.appntNo = '000009302'    and exists (select 1 from LGWork where statusNo not in('7','8') and workNo =d.workNo) order by c.WorkNo desc";
        TransferData tTransferData= new TransferData();
        tTransferData.setNameAndValue("Sql", sql);

        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "pa0004";
        tGI.ComCode = "86";

        VData tVData = new VData();
        tVData.addElement(tGI);
        tVData.addElement(tTransferData);

        IndiVisitOverBL bl = new IndiVisitOverBL();
        if(!bl.submitData(tVData, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
    }
}
