package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;
/**
 * <p>Title: Web业务系统</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class IndiDueFeeCancelUI {

  //业务处理相关变量
  private VData mInputData ;
  public  CErrors mErrors=new CErrors();

  public IndiDueFeeCancelUI() {
  }
  public static void main(String[] args) {
    IndiDueFeeCancelUI IndiDueFeeCancelUI1 = new IndiDueFeeCancelUI();
  }

  //传输数据的公共方法
  public boolean submitData(VData cInputData,String cOperate)
  {
    //首先将数据在本类中做一个备份
    mInputData=(VData)cInputData.clone() ;

    IndiDueFeeCancelBL tIndiDueFeeCancelBL=new IndiDueFeeCancelBL();
    tIndiDueFeeCancelBL.submitData(mInputData,cOperate);

    mInputData=null;
    //如果有需要处理的错误，则返回
    if (tIndiDueFeeCancelBL .mErrors .needDealError() )
       {
       this.mErrors .copyAllErrors(tIndiDueFeeCancelBL.mErrors ) ;
       return false;
       }
System.out.println("error num="+mErrors.getErrorCount());
    return true;
  }

}
