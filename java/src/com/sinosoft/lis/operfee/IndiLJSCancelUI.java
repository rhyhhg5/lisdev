package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.operfee.IndiLJSCancelBL;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * <p>Title: lis</p>
 *
 * <p>Description:
 * 应收作废用户接口
 * 接受页面传入的数据，并出入后台处理
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class IndiLJSCancelUI {

    public CErrors mCErrors = new CErrors();

    private VData mInputData = new VData();

    private String mOperate = "";


    public IndiLJSCancelUI() {
    }

    /**
     * 操作的提交方法，作为页面数据的入口
     * @param cInputData VData：
     * 1、LJSPaySchem：a交费信息
     * @param cOperate String：""
     * @return boolean：操作成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate) {

        mInputData = (VData) cInputData.clone();

        mOperate = cOperate;

        IndiLJSCancelBL tIndiLJSCancelBL = new IndiLJSCancelBL();

        if (!tIndiLJSCancelBL.submitData(mInputData, mOperate)) {
            this.mCErrors.copyAllErrors(tIndiLJSCancelBL.mCErrors);
            return false;
        }

        return true;
    }


    public static void main(String[] args) {
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "wuser";
        tGI.ManageCom = "86";

        LJSPaySchema tLJSPaySchema = new LJSPaySchema();
        tLJSPaySchema.setGetNoticeNo("31000002916");

        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("CancelMode", "0");
        IndiLJSCancelUI tIndiLJSCancelUI = new IndiLJSCancelUI();


        VData tVData = new VData();
        tVData.addElement(tLJSPaySchema);
        tVData.addElement(tGI);
        tVData.addElement(tTransferData);
        tIndiLJSCancelUI.submitData(tVData,"INSERT");

    }
}
