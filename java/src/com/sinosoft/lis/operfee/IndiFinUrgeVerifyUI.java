package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.finfee.*;
import com.sinosoft.lis.pubfun.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description:应交费用类（界面输入）（暂对个人）
 * 从错误对象处理类继承，用来保存错误对象,在每个类中都存在
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class IndiFinUrgeVerifyUI {

    //业务处理相关变量
    private VData mInputData;
    public CErrors mErrors = new CErrors();

    public IndiFinUrgeVerifyUI() {
    }

    public static void main(String[] args) {

        VData tVData = new VData();
        String TempFeeNo = "86110020040310001662";
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "001";
        tGlobalInput.ComCode = "86";

        //查询暂交费表
       /* LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
        LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
        tLJTempFeeSchema.setTempFeeNo(TempFeeNo);
        tLJTempFeeSchema.setTempFeeType("2"); //交费类型为2：续期催收交费
        tVData.add(tLJTempFeeSchema);
        tVData.add(tGlobalInput);
        TempFeeQueryForUrgeGetUI tTempFeeQueryForUrgeGetUI = new
                TempFeeQueryForUrgeGetUI();
        tTempFeeQueryForUrgeGetUI.submitData(tVData, "QUERY");
        tVData.clear();
        tVData = tTempFeeQueryForUrgeGetUI.getResult();
        tLJTempFeeSet.set((LJTempFeeSet) tVData.getObjectByObjectName(
                "LJTempFeeSet", 0));
        tLJTempFeeSchema = (LJTempFeeSchema) tLJTempFeeSet.get(1);
        //查询应收总表
        tVData.clear();
        LJSPaySchema tLJSPaySchema = new LJSPaySchema();
        LJSPaySet tLJSPaySet = new LJSPaySet();
        VerDuePayFeeQueryUI tVerDuePayFeeQueryUI = new VerDuePayFeeQueryUI();
        tLJSPaySchema.setGetNoticeNo(TempFeeNo);
        tVData.add(tLJSPaySchema);
        tVData.add(tGlobalInput);
        tVerDuePayFeeQueryUI.submitData(tVData, "QUERY");
        tVData.clear();
        tVData = tVerDuePayFeeQueryUI.getResult();
        tLJSPaySet.set((LJSPaySet) tVData.getObjectByObjectName("LJSPaySet", 0));
        tLJSPaySchema = (LJSPaySchema) tLJSPaySet.get(1);*/

        String ContNo = "00002580401"; //保单号码
        LCContSet tLCContSet = new LCContSet(); ;
        LCContSchema tLCContSchema = new LCContSchema();
        // 目前这个这个变量后台未做任何处理
        System.out.println(ContNo);
        tLCContSchema.setContNo(ContNo);

        tVData.add(tLCContSchema);
        //tVData.add(tLJTempFeeSchema);
        tVData.add(tGlobalInput);
        IndiFinUrgeVerifyUI IndiFinUrgeVerifyUI1 = new IndiFinUrgeVerifyUI();
        IndiFinUrgeVerifyUI1.submitData(tVData, "VERIFY");

    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        IndiFinUrgeVerifyBL tIndiFinUrgeVerifyBL = new IndiFinUrgeVerifyBL();
        System.out.println("Start IndiFinUrgeVerify UI Submit...");
        tIndiFinUrgeVerifyBL.submitData(mInputData, cOperate);

        System.out.println("End FinFeeVerify UI Submit...");

        mInputData = null;
        //如果有需要处理的错误，则返回
        if (tIndiFinUrgeVerifyBL.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tIndiFinUrgeVerifyBL.mErrors);
            System.out.println("error num=" + mErrors.getErrorCount());
            return false;
        }
        return true;
    }

}

