package com.sinosoft.lis.operfee;

import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.utility.StrTool;
import java.io.File;
import java.util.ArrayList;

//程序名称： ULIBenefitBatchPrintBL.java
//程序功能： 万能给付通知打印
//创建日期：2009-09-27
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

public class ULIBenefitBatchPrintBL 
{
    public CErrors mErrors = new CErrors();
    
    private GlobalInput mG = new GlobalInput();
    
    public ULIBenefitBatchPrintBL() 
    {
    }
    
    ArrayList mArrayList = new ArrayList();

    /**
     * 传输数据的公共方法
     * @param: cInputData 输入的数据
     *         cOperate 数据操作
     * @return:
     */
    public boolean submitData(VData cInputData, String cOperate) 
    {
        cInputData = (VData) cInputData.clone();
        if (!getInputData(cInputData)) 
        {
            return false;
        }
        if (!dealData()) 
        {
            return false;
        }
        return true;
    }

    private boolean getInputData(VData cInputData)
    {
    	TransferData tTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
    	mArrayList = (ArrayList)tTransferData.getValueByName("arrayList");
        mG.setSchema((GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0));
        return true;
    }

    /**
     * 数据操作类业务处理
     * 输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean dealData() 
    {
    	LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet(); 
        for (int i = 0; i < mArrayList.size(); i++)
        {
            LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
            tLOPRTManagerSchema.setCode("OM003");
            tLOPRTManagerSchema.setStandbyFlag2((String)mArrayList.get(i));
            tLOPRTManagerSet.add(tLOPRTManagerSchema);
        }
        VData tVData = new VData();
        tVData.add(tLOPRTManagerSet);
        tVData.add(mG);
        PDFPrintBatchManagerBL tPDFPrintBatchManagerBL = new PDFPrintBatchManagerBL();
        if (!tPDFPrintBatchManagerBL.submitData(tVData, "batch")) 
        {
            mErrors.addOneError(tPDFPrintBatchManagerBL.mErrors.getError(1));
            return false;
        }
        return true;
    }

    public static void main(String[] args)
    {

    }
}
