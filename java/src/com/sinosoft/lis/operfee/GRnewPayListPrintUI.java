package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.operfee.GRnewPayListPrintBL;
import com.sinosoft.lis.pubfun.GlobalInput;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GRnewPayListPrintUI {
    /**错误的容器*/
    public CErrors mErrors = null;
    private VData mResult = null;

    public GRnewPayListPrintUI() {
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
    	GRnewPayListPrintBL bl = new GRnewPayListPrintBL();

        if (!bl.submitData(cInputData, cOperate)) {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }
        mResult = bl.getResult();

        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    public static void main(String[] args) {
        GRnewPayListPrintUI p = new GRnewPayListPrintUI();
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "001";
        tGI.ManageCom = "86";

        TransferData tTransferData = new TransferData();
        String tPayDate = "2005-10-09";
        String tGrpContNo = "0000018001";
        String tVerifyType = "1";
        tTransferData.setNameAndValue("PayDate", tPayDate);
        tTransferData.setNameAndValue("GrpContNo", tGrpContNo);
        tTransferData.setNameAndValue("VerifyType", tVerifyType);
        VData tVData = new VData();
        tVData.addElement(tGI);
        tVData.addElement(tTransferData);
        p.submitData(tVData,"PRINT");

    }

}
