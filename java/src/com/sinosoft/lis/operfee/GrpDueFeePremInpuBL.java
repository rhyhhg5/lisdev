package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubSubmit;

/**
 * <p>Title: lis</p>
 *
 * <p>Description:
 * 存储保障几乎录入保费状况
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author yangyalin
 * @version 1.2
 */
public class GrpDueFeePremInpuBL
{
    /*** 错误的容器*/
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = null;
    private LCContPlanSet mLCContPlanSet = null;
    private TransferData mTransferData = null;  //保障计划<->收费

    private MMap map = new MMap();

    public GrpDueFeePremInpuBL()
    {
    }

    /**
     * 外部操作的提交方法，处理录入保费和交费人数
     * @param data VData
     * @param operate String
     * @return boolean：成功true，否则false
     */
    public boolean submitData(VData data, String operate)
    {
        if(!getInputData(data))
        {
            return false;
        }
        if(!dealData())
        {
            return false;
        }

        VData d = new VData();
        d.add(map);
        PubSubmit p = new PubSubmit();
        if(!p.submitData(d, ""))
        {
            System.out.println(p.mErrors.getErrContent());
            mErrors.addOneError("存储录入保费和人数失败");
            return false;
        }

        return true;
    }

    /**
     * 得到录入的数据
     * @param data VData
     * @return boolean：成功true，否则false
     */
    private boolean getInputData(VData data)
    {
        mGlobalInput = (GlobalInput) data
                       .getObjectByObjectName("GlobalInput", 0);
        mLCContPlanSet = (LCContPlanSet) data
                         .getObjectByObjectName("LCContPlanSet", 0);
        mTransferData = (TransferData) data
                        .getObjectByObjectName("TransferData", 0);
        if(mLCContPlanSet == null || mLCContPlanSet.size() == 0
           || mTransferData == null)
        {
            mErrors.addOneError("程序发生异常，没有得到所需的数据");
            return false;
        }

        for(int i = 1; i <= mLCContPlanSet.size(); i++)
        {
            if(mLCContPlanSet.get(i).getGrpContNo() == null
               || mLCContPlanSet.get(i).getGrpContNo().equals("")
               || mLCContPlanSet.get(i).getContPlanCode() == null
               || mLCContPlanSet.get(i).getContPlanCode().equals("")
               || mLCContPlanSet.get(i).getPeoples2() == 0)
            {
                mErrors.addOneError("传入的数据不完整。");
                return false;
            }

            //得到保障计划的交费
            String contPlanCode = mLCContPlanSet.get(i).getContPlanCode();
            String prem = (String) mTransferData.getValueByName(contPlanCode);
            try
            {
                double p = Double.parseDouble(prem);
            }
            catch(Exception e)
            {
                mErrors.addOneError("保障计划" + contPlanCode + "保费应为非负数");
            }
        }

        return true;
    }

    /**
     * 处理业务逻辑
     * @return boolean
     */
    private boolean dealData()
    {
        LCNoNamePremTraceSet tLCNoNamePremTraceSet = new LCNoNamePremTraceSet();
        for(int i = 1; i <= mLCContPlanSet.size(); i++)
        {
            LCContPlanSchema s = mLCContPlanSet.get(i);

            LCNoNamePremTraceSchema schema = new LCNoNamePremTraceSchema();
            schema.setGrpContNo(s.getGrpContNo());
            schema.setContPlanCode(s.getContPlanCode());
            schema.setGetNoticeNo(s.getGrpContNo());
            schema.setPayToDate(getPayToDate(schema.getContPlanCode()));
            schema.setPrem(getPrem(schema.getContPlanCode()));
            schema.setPremInput((String)mTransferData
                           .getValueByName(s.getContPlanCode()));
            schema.setPeoples2(getPeoples2(schema.getContPlanCode()));
            schema.setPeoples2Input(s.getPeoples2());
            schema.setInputDate(PubFun.getCurrentDate());
            schema.setInputTime(PubFun.getCurrentTime());
            schema.setInputOperator(mGlobalInput.Operator);
            schema.setRemark(getRemark(schema));
            schema.setOperator(mGlobalInput.Operator);

            PubFun.fillDefaultField(schema);
            tLCNoNamePremTraceSet.add(schema);
        }

        map.put(tLCNoNamePremTraceSet, "DELETE&INSERT");

        return true;
    }

    /**
     * 对保障计划交费的描述
     * @param schema LCNoNamePremTraceSchema
     * @return String
     */
    private String getRemark(LCNoNamePremTraceSchema schema)
    {
        if(Math.abs(schema.getPrem() - schema.getPremInput()) > 0.000001)
        {
            return "保障计划" + schema.getContPlanCode()
                + "(原保费" + schema.getPrem() +")录入了本期交费"
                + schema.getPremInput() + "元";
        }
        else
        {
            return "保障计划" + schema.getContPlanCode()
                 + "交费维持原期交保费" + schema.getPrem() + "元";
        }
    }

    /**
     * 得到保障计划contPlanCode的交至日期
     * @param contPlanCode String：保障计划
     * @return String：交至日期
     */
    private String getPayToDate(String contPlanCode)
    {
        String sql = "  select min(payToDate) "
                     + "from LCPol "
                     + "where grpContNO = '"
                     + mLCContPlanSet.get(1).getGrpContNo()
                     + "'  and contPlanCode = '" + contPlanCode
                     + "'  and polTypeFlag = '1' ";
        String payToDate = new ExeSQL().getOneValue(sql);
        return payToDate;
    }

    /**
     * 得到保障计划contPlanCode的被保人数
     * @param contPlanCode String：保障计划
     * @return String：人数
     */
    private String getPeoples2(String contPlanCode)
    {
        String sql = "  select peoples2 "
                     + "from LCContPlan "
                     + "where grpContNo = '"
                     + mLCContPlanSet.get(1).getGrpContNo()
                     + "'  and contPlanCode = '" + contPlanCode + "' ";
        String peoples2 = new ExeSQL().getOneValue(sql);
        return peoples2;
    }

    /**
     * 得到保障计划contPlanCode的期交保费
     * @param contPlanCode String：保障计划
     * @param contPlanCode String：保障计划
     * @return String：保费
     */
    private String getPrem(String contPlanCode)
    {
        String sql = "  select sum(prem) "
                     + "from LCPol "
                     + "where grpContNO = '"
                     + mLCContPlanSet.get(1).getGrpContNo()
                     + "'  and contPlanCode = '" + contPlanCode
                     + "'  and polTypeFlag = '1' ";
        String prem = new ExeSQL().getOneValue(sql);
        return prem;
    }

    public static void main(String[] args)
    {
        LCContPlanSet tLCContPlanSet = new LCContPlanSet();
        TransferData tTransferData = new TransferData(); //记录每个保障计划录入的保费

        LCContPlanSchema tLCContPlanSchema = new LCContPlanSchema();
        tLCContPlanSchema.setGrpContNo("0000185401");
        tLCContPlanSchema.setContPlanCode("A");
        tLCContPlanSchema.setPeoples2(3);
        tLCContPlanSet.add(tLCContPlanSchema);

        tTransferData.setNameAndValue("A", "100");

        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        VData d = new VData();
        d.add(tLCContPlanSet);
        d.add(tTransferData);
        d.add(g);

        GrpDueFeePremInpuBL bl = new GrpDueFeePremInpuBL();
        if(!bl.submitData(d, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }

    }
}
