package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 接收页面传入的数据，并传入到IndiDueFeeBackBL进行业务逻辑的处理。
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class MOmniYJBackUI
{
    /**错误的容器。*/
    public CErrors mErrors = new CErrors();

    public MOmniYJBackUI()
    {
    }

    /**
     * 操作的提交方法，作为页面数据的入口。
     * @param cInputData VData:
     * A.	GlobalInput对象，完整的登陆用户信息。
     * B.	LJSPayB对象，应收记录备份信息，只需GetNoticeNo即可。
     * @param cOperate String：此为“”
     * @return boolean：boolean, 成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
    	MOmniYJBackBL bl = new MOmniYJBackBL();
        if(!bl.submitData(cInputData, cOperate))
        {
            mErrors = bl.mErrors;
            return false;
        }

        return true;
    }

    public static void main(String[] args)
    {
        MOmniYJBackUI indiduefeebackui = new MOmniYJBackUI();
    }
}
