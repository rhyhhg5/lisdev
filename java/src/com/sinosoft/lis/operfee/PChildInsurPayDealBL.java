package com.sinosoft.lis.operfee;


import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.bq.CommonBL;
import java.util.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.xb.*;

public class PChildInsurPayDealBL{

  //错误处理类，每个需要错误处理的类中都放置该类
  public CErrors mErrors = new CErrors();
  private String dealtype ;
  private String customerBackDate;
  private String getNoticeNo;
  private VData mSubmitData = new VData();
  private GlobalInput tGI = new GlobalInput();

  /** 数据操作字符串 */
  private String serNo = ""; //流水号
  private String tLimit = "";
  private String tNo = ""; //生成的暂交费号
  private String mOperate;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();

  private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
  private LJTempFeeSet mLJTempFeeSetNew = new LJTempFeeSet();
  private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
  private LJTempFeeClassSet mLJTempFeeClassSetNew = new LJTempFeeClassSet();
  private LJTempFeeClassSet mLJTempFeeClassSetDel = new LJTempFeeClassSet();

  //传输数据的公共方法
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    System.out.println("After getinputdata");

    //进行业务处理
    if (!dealData()) {
        return false;
  }
    System.out.println("After dealData！");
    return true;
}

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData() {
          //查找应收
          LJSPayDB tLJSPayDB = new LJSPayDB();
          tLJSPayDB.setGetNoticeNo(getNoticeNo);
          if (tLJSPayDB.getInfo()) {
                  //生成满期给付数据
                  LCContDB tLCContDB = new LCContDB();
                  LCContSchema tLCContSchema = new LCContSchema();
                  tLCContDB.setContNo(tLJSPayDB.getOtherNo());
                  if(!tLCContDB.getInfo())
                  {
                      mErrors.addOneError("少儿险满期查找保单失败");
                      return false;
                  }
                  tLCContSchema.setSchema(tLCContDB.getSchema());
                  tLimit = PubFun.getNoLimit(tLJSPayDB.getManageCom());
                  serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
                  VData tVData = new VData();
                  tVData = new VData();
                  tVData.add(tLCContSchema);
                  tVData.add(tGI);
                  tVData.add(serNo);
                  TransferData tTransferData= new TransferData();
                  tTransferData.setNameAndValue("serNo",serNo);
                  tTransferData.setNameAndValue("PayMode","Q");
                  tTransferData.setNameAndValue("StartDate",tLCContSchema.getCInValiDate());
                  tTransferData.setNameAndValue("EndDate",tLCContSchema.getCInValiDate());
                  tVData.add(tTransferData);

//                  //针对保单进行抽档操作（例如：生成应收数据，应收抽档通知书等）
//                  ExpirBenefitBL tExpirBenefitBL = new ExpirBenefitBL();
//                  if (!tExpirBenefitBL.submitData(tVData, "INSERT")) {
//                      CError.buildErr(this, "保单号为：" + tLJSPayDB.getOtherNo() + "的保单给付失败:"
//                                      + tExpirBenefitBL.mErrors.getFirstError());
//                      return false;
//                  }


                  //删除暂收，避免出错
                  tVData = new VData();
                  String delTempFee = "delete from ljtempfee where tempfeeno ='" +
                                      getNoticeNo + "'";
                  String delTempFeeClass =
                      "delete from ljtempfeeclass where tempfeeno ='" + getNoticeNo +
                      "'";
                  //撤消应收
                  MMap tMMap = new MMap();
                  tMMap.put(delTempFee, "DELETE");
                  tMMap.put(delTempFeeClass, "DELETE");
                  tVData.add(tMMap);
                  PubSubmit tPubSubmit = new PubSubmit();
                  tPubSubmit.submitData(tVData, "");
                  tTransferData = new TransferData();
                  tTransferData.setNameAndValue("CancelMode", "3"); //撤销
                  tTransferData.setNameAndValue("IsRepeal", "Y");

                  tVData.clear();
                  tVData.addElement(tLJSPayDB.getSchema());
                  tVData.addElement(tGI);
                  tVData.addElement(tTransferData);

                  IndiLJSCancelBL tIndiLJSCancelBL = new IndiLJSCancelBL();

                  if (!tIndiLJSCancelBL.submitData(tVData, "INSERT")) {
                      mErrors.addOneError("少儿险满期处理失败");
                      return false;
                  }
                  return true;
              }
              else
              {
                  mErrors.addOneError("少儿险满期处理失败,没有应收");
                  return true;
              }
  }
  private boolean getInputData(VData mInputData) {
              // 公用变量
              tGI = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
              TransferData tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);
              if (tGI == null || tTransferData == null)
              {
                  mErrors.addOneError("传入的数据不完整。");
                  return false;
              }
              dealtype = (String) tTransferData.getValueByName("dealType");
              customerBackDate = (String) tTransferData.getValueByName("customerBackDate");
              getNoticeNo = (String) tTransferData.getValueByName("getNoticeNo");
              if(!(valiData(dealtype)&&valiData(customerBackDate)&&valiData(getNoticeNo)))
              {
                  mErrors.addOneError("传入的数据不完整。");
                  return false;
              }
              return true;
          }
          private boolean valiData(String aData)
          {
              if(aData==null||"".equals(aData))
              {
                  return false;
              }
              return true;
          }
      }
