/**
 * Copyright (c) 2002 sinosoft  Co. Ltd.
 * All right reserved.
 */
package com.sinosoft.lis.operfee;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;


/**
 * <p>Title: Lis_New</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author Sinosoft
 * @version 1.0
 */

public class GrpPhonVisitBL
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();


    /** 往后面传输数据的容器 */
    private VData mInputData;


    /** 数据操作字符串 */
    private GlobalInput mGI = new GlobalInput();
    private String mOperate;
    private boolean specWithDrawFlag = false; //特殊的退费标记。该标记为真时，不考虑outpayflag标志
    private TransferData tTransferData = new TransferData();

    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private LJSPaySchema mLJSPaySchema = new LJSPaySchema(); //实付总表
    private LJSPaySet mLJSPaySet = new LJSPaySet();

//    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema(); //集体合同表
    private LCGrpContSet mLCGrpContSet = new LCGrpContSet();
    private LCGrpContSchema mNewLCGrpContSchema = new LCGrpContSchema(); //集体合同表
    private LCGrpContSet mNewLCGrpContSet = new LCGrpContSet();
    private LCGrpPolSchema mNewLCGrpPolSchema = new LCGrpPolSchema(); //集体合同表
    private LCGrpPolSet mNewLCGrpPolSet = new LCGrpPolSet();


    private String grpContSign = null; //签单调用标志


    public GrpPhonVisitBL()
    {
    }


    //传输数据的公共方法
    public boolean submitData(VData tVData)
    {
        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(tVData))
            return false;
        System.out.println("After getinputdata");

        //进行业务处理
        if (!dealData())
            return false;
        System.out.println("After dealData！");

        //准备往后台的数据
        if (!prepareOutputData())
            return false;

        if (grpContSign == null)
        {
            System.out.println("After prepareOutputData");

            System.out.println("Start GrpPhonVisitBL BL Submit...");

            GrpPhonVisitBLS tGrpPhonVisitBLS = new
                    GrpPhonVisitBLS();
            tGrpPhonVisitBLS.submitData(mInputData);

            System.out.println("End GrpPhonVisitBL BL Submit...");

            //如果有需要处理的错误，则返回
            if (tGrpPhonVisitBLS.mErrors.needDealError())
            {
                this.mErrors.copyAllErrors(tGrpPhonVisitBLS.mErrors);
            }

            mInputData = null;
        }
        return true;
    }

    public VData getVResult()
    {
        return this.mInputData;
    }


    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData tVData)
    {
        mLCGrpContSet = (LCGrpContSet) tVData.getObjectByObjectName(
                "LCGrpContSet", 0);
        mGI = (GlobalInput) tVData.getObjectByObjectName("GlobalInput", 0);
        tTransferData = (TransferData) tVData.getObjectByObjectName(
                "TransferData", 0);

        if (mLCGrpContSet == null || mGI == null)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpPhonVisitBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if (tTransferData != null)
        {
            String inStr = (String) tTransferData.getValueByName("SpecWithDraw");
            if (inStr != null)
            {
                if (inStr.equals("1"))
                {
                    specWithDrawFlag = true;
                }
            }
            //团单签单标记
            grpContSign = (String) tTransferData.getValueByName("GrpContSign");

        }

        return true;
    }


    //根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData()
    {
        //产生流水号
		String tPayMode=(String)tTransferData.getValueByName("PayMode");
        String tPayDate = (String) tTransferData.getValueByName("PayDate");
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        LCGrpContDB tLCGrpContDB = null;
        for (int index = 1; index <= mLCGrpContSet.size(); index++)
        {
            mNewLCGrpContSchema = mLCGrpContSet.get(index);
            //1-查询投保单号对应的已签单的保单，如果没有则跳过,否则保存查询到的纪录
            //如果是签单调用，则不用查询


            //4-更新集体保单数据
			mNewLCGrpContSchema.setPayMode(tPayMode);
            mNewLCGrpContSchema.setModifyDate(CurrentDate);
            mNewLCGrpContSchema.setModifyTime(CurrentTime);
            mNewLCGrpContSet.add(mNewLCGrpContSchema);

        }

        LJSPaySet tLJSPaySet= new  LJSPaySet();
		LJSPayDB tLJSPayDB = new LJSPayDB();
		 //tLJSPayDB.setOtherNo(mLCGrpContSchema.getGrpContNo());
		 tLJSPayDB.setOtherNoType("1");
		 tLJSPaySet = tLJSPayDB.query();
        if (tLJSPaySet.size() == 0)
        {
            CError.buildErr(this, "没有查询到团体保单续期催收的应收数据!");
            return false;
        }
        mLJSPaySchema = tLJSPaySet.get(1);
        mLJSPaySchema.setPayDate(tPayDate);
		mLJSPaySet.add(mLJSPaySchema);

        return true;
    }


    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData()
    {
        mInputData = new VData();
        try
        {
            mInputData.add(mNewLCGrpPolSet);
            System.out.println("lcgrpContSet.size:" + mNewLCGrpContSet.size());
            mInputData.add(mNewLCGrpContSet);

            mInputData.add(mLJSPaySet);

        }
        catch (Exception ex)
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "GrpPhonVisitBL";
            tError.functionName = "prepareData";
            tError.errorMessage = "在准备往后层处理所需要的数据时出错。";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }

    public static void main(String[] args)
    {
        GrpPhonVisitBL GrpPhonVisitBL1 = new
                GrpPhonVisitBL();
        VData tVData = new VData();
        GlobalInput tGI = new GlobalInput();
        tGI.ManageCom = "86110000";
        tGI.ComCode = "86";
        tGI.Operator = "001";
        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema.setGrpContNo("0000019601");
        tLCGrpContSet.add(tLCGrpContSchema);
        tVData.add(tLCGrpContSet);
        tVData.add(tGI);
        GrpPhonVisitBL1.submitData(tVData);
    }
}
