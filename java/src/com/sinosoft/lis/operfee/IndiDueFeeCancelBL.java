package com.sinosoft.lis.operfee;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;

import java.lang.String;

import java.util.*;


/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */
public class IndiDueFeeCancelBL {
    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData mInputData = new VData();
    private VData mResult = new VData();
    private GlobalInput tGI = new GlobalInput();

    /** 数据操作字符串 */
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();

    /** 数据表  保存数据*/

    //个人保单表
    private LCContSchema mLCContSchema = new LCContSchema();

    //应收个人交费表
    //  private LJSPayPersonSchema mLJSPayPersonSchema = new LJSPayPersonSchema();
    private LJSPayPersonSet mLJSPayPersonSet = new LJSPayPersonSet();
    //应收个人交费表备份表
    private LJSPayPersonBSet mLJSPayPersonBSet = new LJSPayPersonBSet();
    //因收总表
    private LJSPaySchema mLJSPaySchema = new LJSPaySchema();
    //因收总表备份表
    private LJSPayBSchema mLJSPayBSchema = new LJSPayBSchema();

    //打印管理表
    private LOPRTManagerSchema mLOPRTManagerSchema = new LOPRTManagerSchema();
    private LOPRTManagerSubSchema mLOPRTManagerSubSchema = new
            LOPRTManagerSubSchema();
    private MMap mMMap = new MMap(); //

    public IndiDueFeeCancelBL() {
    }

    public static void main(String[] args) {
        IndiDueFeeCancelBL IndiDueFeeCancelBL1 = new IndiDueFeeCancelBL();
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setPolNo("86110020030210000128");

        GlobalInput tGI = new GlobalInput();
        tGI.Operator = "001";
        tGI.ManageCom = "8611";
        tGI.ComCode = "8611";

        VData tv = new VData();
        tv.add(tGI);
        tv.add(tLCPolSchema);
        IndiDueFeeCancelBL1.submitData(tv, "INSERT");
    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData, String cOperate) {
        //将操作数据拷贝到本类中
        this.mOperate = cOperate;

        if (!getInputData(cInputData)) {
            return false;
        }

        System.out.println("After getinputdata");

        //如果附加险有续保，置标记退出
        if (checkData() == false) {
            return false;
        }

        //进行业务处理
        if (!dealData()) {
            return false;
        }

        //公共提交
        PubSubmit tPubSubmit = new PubSubmit();

        if (tPubSubmit.submitData(mResult, "") == false) {
            return false;
        }

        return true;
    }

    //根据前面的输入数据，进行逻辑处理
    //如果在处理过程中出错，则返回false,否则返回true
    private boolean dealData() {
        //准备Map数据-提交
        mMMap.put(mLJSPaySchema, "DELETE");
        mMMap.put(mLJSPayPersonSet, "DELETE");
        mMMap.put(mLJSPayBSchema, "DELETE");
        mMMap.put(mLJSPayPersonBSet, "DELETE");
        mMMap.put(mLOPRTManagerSchema, "DELETE");
        mMMap.put(mLOPRTManagerSubSchema, "DELETE");
        mResult.add(mMMap);

        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) {
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        mLCContSchema = ((LCContSchema) mInputData.getObjectByObjectName(
                "LCContSchema",
                0));

        if ((tGI == null) || (mLCContSchema == null)) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "IndiDueFeeCancelBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }

        return true;
    }


    /**
     *检查续保续期
     * @return
     */
    private boolean checkData() {

        LJSPaySet tLJSPaySet = new LJSPaySet();
        LJSPayDB tLJSPayDB = new LJSPayDB();
        tLJSPayDB.setOtherNo(mLCContSchema.getContNo());
        tLJSPayDB.setOtherNoType("2");
        tLJSPaySet = tLJSPayDB.query();

        if (tLJSPaySet.size() <= 0) {
            CError.buildErr(this, "没有查询到保单续期催收的应收数据");
            return false;
        }
        mLJSPaySchema = tLJSPaySet.get(1);
        if (mLJSPaySchema.getBankOnTheWayFlag() != null) {
            if (mLJSPaySchema.getBankOnTheWayFlag().equals("1")) {
                CError.buildErr(this, "应收数据目前银行在途中,不能撤销!");
                return false;
            }
        }
        //应收备份表
        LJSPayBSet tLJSPayBSet = new LJSPayBSet();
        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        tLJSPayBDB.setOtherNo(mLCContSchema.getContNo());
        tLJSPayBDB.setOtherNoType("2");
        tLJSPayBSet = tLJSPayBDB.query();

        if (tLJSPayBSet.size() <= 0) {
            CError.buildErr(this, "没有查询到保单续期催收的应收备份数据");
            return false;
        }
        mLJSPayBSchema = tLJSPayBSet.get(1);
        if (mLJSPayBSchema.getBankOnTheWayFlag() != null) {
            if (mLJSPayBSchema.getBankOnTheWayFlag().equals("1")) {
                CError.buildErr(this, "应收数据目前银行在途中,不能撤销!");
                return false;
            }
        }

        ExeSQL tExeSQL = new ExeSQL();
        String count = tExeSQL.getOneValue(
                "select count(*) from LCRnewStateLog where PrtNo='" +
                mLCContSchema.getPrtNo() + "' and State not in ('5','6')");
        if (tExeSQL.mErrors.needDealError() || count == null || count.equals("")) {
            CError.buildErr(this, "检查附加险状态失败！", mErrors);
            return false;
        }

        if (!count.equals("0")) {
            CError.buildErr(this, "有附加险续保的催收数据存在，请先做附加险的续保催收撤销！");
            return false;
        }

        LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
        tLJSPayPersonDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
        mLJSPayPersonSet = tLJSPayPersonDB.query();
        if (mLJSPayPersonSet.size() <= 0) {
            CError.buildErr(this, "没有查询到保单续期催收的个人应收数据,不能撤销！");
            return false;
        }

        LJSPayPersonBDB tLJSPayPersonBDB = new LJSPayPersonBDB();
        tLJSPayPersonBDB.setGetNoticeNo(mLJSPaySchema.getGetNoticeNo());
        mLJSPayPersonBSet = tLJSPayPersonBDB.query();
        if (mLJSPayPersonBSet.size() == 0) {
            CError.buildErr(this, "没有查询到保单续期催收的个人应收备份数据,不能撤销！");
            return false;
        }

        LJTempFeeDB tLJTempFeeDB = new LJTempFeeDB();
        tLJTempFeeDB.setTempFeeNo(mLJSPaySchema.getGetNoticeNo());

        LJTempFeeSet tLJTempFeeSet = tLJTempFeeDB.query();

        if (tLJTempFeeSet.size() > 0) {
            CError.buildErr(this, "财务已经录入暂交费,不能撤销！");

            return false;
        }

        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
        tLOPRTManagerDB.setOtherNo(mLCContSchema.getContNo());
        tLOPRTManagerDB.setOtherNoType("00");
        tLOPRTManagerDB.setCode(PrintManagerBL.CODE_REPAY);

        LOPRTManagerSet tLOPRTManagerSet = tLOPRTManagerDB.query();

        if (tLOPRTManagerSet.size() <= 0) {
            CError.buildErr(this, "没有找到续期打印主表数据，不能撤销！");

            return false;
        }

        mLOPRTManagerSchema = tLOPRTManagerSet.get(1);
        LOPRTManagerSubDB tLOPRTManagerSubDB = new LOPRTManagerSubDB();
        tLOPRTManagerSubDB.setPrtSeq(mLOPRTManagerSchema.getPrtSeq());
        LOPRTManagerSubSet tLOPRTManagerSubSet = tLOPRTManagerSubDB.query();

        if (tLOPRTManagerSubSet.size() <= 0) {
            CError.buildErr(this, "没有找到续期打印子表数据，不能撤销！");
            return false;
        }

        if (tLOPRTManagerSubSet.size() > 1) {
            CError.buildErr(this, "续期打印子表数据有多条，请告知技术人员，不能撤销！");
            return false;
        }

        mLOPRTManagerSubSchema = tLOPRTManagerSubSet.get(1);

        return true;
    }
}
