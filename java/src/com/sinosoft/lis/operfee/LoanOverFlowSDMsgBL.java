package com.sinosoft.lis.operfee;

import com.sinosoft.lis.bq.BQ;
import com.sinosoft.lis.bq.ChangeCodeBL;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;

import java.rmi.*;
import com.sinosoft.lis.message.*;

import java.util.*;

/**
 * <p>Title: 宽限期短信通知</p>
 *	
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2013</p
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class LoanOverFlowSDMsgBL {
    private GlobalInput mG = new GlobalInput();
    private String mCurrentDate = PubFun.getCurrentDate();
    private ExeSQL mExeSQL = new ExeSQL();
    private String mContNo = null;
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


    private boolean getInputData(VData cInputData) 
    {
    	mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
    	TransferData tTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
    	mContNo = (String)tTransferData.getValueByName("contno");
        return true;
    }
    
    public void runOneCont(String cContNo)
    {
        mContNo = cContNo;
        GlobalInput mGlobalInput = new GlobalInput();
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "86";//总公司
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("contno", mContNo);
        mVData.add(tTransferData);
        mVData.add(mGlobalInput);
        submitData(mVData, "SDMSG");
    }

    public boolean submitData(VData cInputData, String cOperate) {
        cInputData = (VData) cInputData.clone();

        if (!getInputData(cInputData)) 
        {
        	mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                    "GlobalInput", 0));
        	if(mG.Operator==null || mG.Operator.equals("") || mG.Operator.equals("null"))
        	{
        		mG.Operator="001";
        	}
        	if(mG.ManageCom==null || mG.ManageCom.equals("") || mG.ManageCom.equals("null"))
        	{
        		mG.ManageCom="86";
        	}
            return false;
        }
        if (cOperate != null&& mContNo!=null&&!"".equals(mContNo) && cOperate.trim().equals("SDMSG")) {
            sendMsg();
        } else {
            mErrors.addOneError(new CError("不支持的操作字符串！"));
            return false;
        }
        return true;
    }

    private boolean sendMsg() {
        System.out.println("保单贷款的本息和超过保单的现金价值97%短信提醒服务开始！");
        SMSClient smsClient = new SMSClient();
        
        Vector vec = new Vector();
        vec = getMessage();
        Vector tempVec = new Vector(); //临时变量--实际发短信时传的参数
        //拆分Vector
        System.out.println("---------vec="+vec.size());

        if (!vec.isEmpty()) 
        {
        	for(int i=0;i<vec.size();i++)
            {
        		tempVec.clear();
            	tempVec.add(vec.get(i));
            	
            	SmsMessagesNew msgs = new SmsMessagesNew(); //创建SmsMessages对象，该对象对应于上文下行短信格式中的Messages元素
                msgs.setOrganizationId("0"); //设置该批短信的OrganizationId，定义同上文下行短信格式中的Organization元素
                msgs.setExtension("false"); //设置该批短信的Extension，定义同上文下行短信格式中的Extension元素
                msgs.setServiceType(SMSClient.mes_serviceType1); //设置该批短信的ServiceType，定义同上文下行短信格式中的ServiceType元素
                msgs.setTaskValue(SMSClient.mes_taskVlaue15);
                msgs.setStartDate(mCurrentDate); //设置该批短信的StartDate，定义同上文下行短信格式中的StartDate元素
                msgs.setStartTime("9:00"); //设置该批短信的StartTime，定义同上文下行短信格式中的StartTime元素
                msgs.setEndDate(mCurrentDate); //设置该批短信的EndDate，定义同上文下行短信格式中的EndDate元素
                msgs.setEndTime("20:00"); //设置该批短信的EndTime，定义同上文下行短信格式中的EndTime元素
                
            	msgs.setMessages((SmsMessageNew[]) tempVec.toArray(new SmsMessageNew[tempVec.size()])); //设置该批短信的每一条短信，一批短信可以包含多条短信
            	smsClient.sendSMS(msgs);
            }
        } 
        else 
        {
            System.out.print("贷款无符合条件的短信！");
        }
        System.out.println("保单贷款的本息和超过保单的现金价值97%短信提醒服务结束！");
        return true;
    }

    private Vector getMessage() {
    	System.out.println("进入获取信息 getMessage()------------------------");
        Vector tVector = new Vector();
        ///宽限期短信提醒(长险)--------------------------------
        String tSQLkx =
        	"select a.contno, "   //保单号
//        	+" (select mobile from lcaddress x, lcappnt y  where x.customerno = y.appntno and x.addressno = y.addressno  and y.contno = a.contno fetch first 1 rows only),  "//客户手机号码
        	+" mobile(a.contno), "
        	+" a.appntname,a.managecom, "       //被保人姓名
			+" (case a.appntsex when '0' then '先生' when '1' then '女士'  end) "
        	+" from lccont a "
            +" where a.contno = '"+mContNo+"' " 
            +" and not exists (select 1 from LCCSpec  where contno=('"+BQ.EDORTYPE_LN+"'||a.contno) and SpecType='kxL' and EndorsementNo is null )  "
            +" with ur";
        System.out.println("保单贷款的本息和超过保单的现金价值97%短信提醒服务---------------"+tSQLkx);
        SSRS tMsgSuccSSRS =mExeSQL.execSQL(tSQLkx);
        for (int i = 1; i <= tMsgSuccSSRS.getMaxRow(); i++) 
        {
        	String tContNo = tMsgSuccSSRS.GetText(i, 1);
        	String tCustomerMobile = tMsgSuccSSRS.GetText(i,2);
//        	String tCustomerMobile = "13581569626";
        	String tManageCom = tMsgSuccSSRS.GetText(i,4);
        	String tAppntName = tMsgSuccSSRS.GetText(i,3);
        	String tAppntSex = tMsgSuccSSRS.GetText(i,5);
        	
        	if (tContNo == null || "".equals(tContNo)
					|| "null".equals(tContNo)) {
				continue;
		    }
		    if (tCustomerMobile == null || "".equals(tCustomerMobile)
					|| "null".equals(tCustomerMobile)) {
				continue;
		    }
		    if (tManageCom == null || "".equals(tManageCom) ||
                    "null".equals(tManageCom)) {
                    continue;
                }
		    if (tAppntName == null || "".equals(tAppntName) ||
                    "null".equals(tAppntName)) {
                    continue;
                }
		    if (tAppntSex == null || "".equals(tAppntSex) ||
                    "null".equals(tAppntSex)) {
                    continue;
                }
            
		   //短信内容
           String tMSGContents=""; 
            
            SmsMessageNew tKXMsg = new SmsMessageNew(); //创建SmsMessage对象，定义同上文下行短信格式中的Message元素
            	
            	 tMSGContents = "尊敬的"+tAppntName+tAppntSex+"，您好！您的保单（保单号："+tContNo+"）" +
            	 		"办理过保单贷款业务，当前账户现金价值较低，保单将有失效风险。" +
            	 		"为保障您的权益，请您于30日内及时办理保单还款业务，祝您健康。";
           
            tKXMsg.setReceiver(tCustomerMobile); //设置该条短信的Receiver，定义同上文下行短信格式中的Receiver元素
            tKXMsg.setContents(tMSGContents); //设置该条短信的Contents，定义同上文下行短信格式中的Contents元素
            
            //direct by lc 2017-4-1 机构
            tKXMsg.setOrgCode(tManageCom); //设置该条短信的发送机构orgCode，定义同上文下行短信格式中的orgCode元素
            
            tVector.add(tKXMsg);
        		
                                         
            // 在LCCSpec表中插入值  ********************************************************  
        	LCCSpecSchema tLCCSpecSchema = new LCCSpecSchema();
            tLCCSpecSchema.setGrpContNo(BQ.GRPFILLDATA);
            tLCCSpecSchema.setContNo(BQ.EDORTYPE_LN+tContNo);
            tLCCSpecSchema.setProposalContNo(BQ.GRPFILLDATA);      
            tLCCSpecSchema.setPrtSeq(null);// 放 ljspayb   SerialNo
            String tLimit = PubFun.getNoLimit(tManageCom);
	           String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit); 
	           tLCCSpecSchema.setSerialNo(serNo); //流水号                
            tLCCSpecSchema.setEndorsementNo(null);
            tLCCSpecSchema.setSpecType("kxL"); //kxD-短险宽限期发送短息
            tLCCSpecSchema.setSpecCode("001");////001-投保人 002-被保人 003-业务员
            tLCCSpecSchema.setSpecContent(tMSGContents+"客户手机号："+ tCustomerMobile);
            tLCCSpecSchema.setOperator(mG.Operator);
            tLCCSpecSchema.setMakeDate(PubFun.getCurrentDate());
            tLCCSpecSchema.setMakeTime(PubFun.getCurrentTime());
            tLCCSpecSchema.setModifyDate(PubFun.getCurrentDate());
            tLCCSpecSchema.setModifyTime(PubFun.getCurrentTime());
            tLCCSpecSchema.getDB().insert();
        }
               
               //到此处
        System.out.println("都结束了----------------------------------------------------------------");
        return tVector;
    }
    
    
    public static void main(String[] args) {
        GlobalInput mGlobalInput = new GlobalInput();
        VData mVData = new VData();
        mGlobalInput.Operator = "001";
        mGlobalInput.ManageCom = "86";
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("contno", "000916079000006");
        mVData.add(tTransferData);        
        mVData.add(mGlobalInput);
        
        LoanOverFlowSDMsgBL tSDwMsgBL = new LoanOverFlowSDMsgBL();
        tSDwMsgBL.submitData(mVData, "SDMSG");
    }

}
