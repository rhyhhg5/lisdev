package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;

/**
 * <p>Title: Web业务系统</p>
 * <p>
 * 用户接口类，接收页面传入的数据，传入后台进行批量抽档业务逻辑的处理
 * </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author qulq
 * @version 1.0
 */

public class ExpirBenefitBatchUI {

    //业务处理相关变量
    private VData mInputData;
    public CErrors mErrors = new CErrors();

    public ExpirBenefitBatchUI() {
    }

    public static void main(String[] args) {

    }

    /**
     * 用户接口类，接收页面传入的数据，传入后台进行批量抽档业务逻辑的处理
     * @param cInputData VData:包括
     * 1、	GlobalInput对象，完整的登陆用户信息
     * 2、	TransferData对象，包含StartDate， EndDate， ManageCom
     * @param cOperate String：操作类型，此可为空字符串“”
     * @return boolean，成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate) {
        //首先将数据在本类中做一个备份
        mInputData = (VData) cInputData.clone();

        ExpirBenefitBatchBL tExpirBenefitBatchBL = new ExpirBenefitBatchBL();
        System.out.println("Start ExpirBenefitBatch UI Submit...");
        tExpirBenefitBatchBL.submitData(mInputData, cOperate);

        System.out.println("End ExpirBenefitBatch UI Submit...");

        mInputData = null;
        mInputData = tExpirBenefitBatchBL.getResult();
        //如果有需要处理的错误，则返回
        if (tExpirBenefitBatchBL.mErrors.needDealError()) {
            this.mErrors.copyAllErrors(tExpirBenefitBatchBL.mErrors);
            return false;
        }
        System.out.println("error num=" + mErrors.getErrorCount());
        return true;
    }
    public VData getResult()
    {
        return mInputData;
    }

}
