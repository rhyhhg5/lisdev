package com.sinosoft.lis.operfee;
//程序名称：LJSXQMulUnlockBL.java
//程序功能：续期收付费批量解锁
//创建日期：2009-7-30
//创建人  ：zhanggm
//更新记录：  更新人  zhanggm  更新日期   2009-8-11  更新原因/内容 可以通过复选框选择多条付费记录进行解锁，如果不选择，默认解锁列表中全部付费记录。
import java.util.ArrayList;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;

public class LJSXQMulUnlockBL
{
	/** 错误处理类，每个需要错误处理的类中都放置该类 */
	public  CErrors mErrors = new CErrors();
	
	private MMap map = new MMap();
	
	/** 统一更新日期 */
	private String mCurrentDate = PubFun.getCurrentDate();
	
	/** 统一更新时间 */
	private String mCurrentTime = PubFun.getCurrentTime();
	
	/** 全局数据 */
	private GlobalInput mGlobalInput = new GlobalInput() ;
	
	/** 操作员 */
	private String mOperater ;
	
	private String mGetNoticeNo = null ;
	
	private String mManageCom = null ;
	
	private String mContNo = null ;
	
	private String mYWType = "";
	
	ArrayList mArrayList = new ArrayList();
	
	public LJSXQMulUnlockBL() 
	{
	}
	
	/**
	* 传输数据的公共方法
	* @param: cInputData 输入的数据
	*         cOperate 数据操作
	* @return:
	*/
    public boolean submitData(VData cInputData,String cYWType)
	{
    	this.mYWType  = cYWType;
        //将操作数据拷贝到本类中
	    if (!getInputData(cInputData))
	    {
	    	CError tError = new CError();
            tError.moduleName = "LJSXQMulUnlockBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "数据处理失败LJSXQMulUnlockBL-->getInputData!";
            this.mErrors.addOneError(tError);
	        return false;
	    }
	    
        //数据校验
        if (!checkData()) 
        {
            return false;
        }
	    
	    //进行业务处理
	    if (!dealData())
	    {
	        // @@错误处理
            CError tError = new CError();
	        tError.moduleName = "LJSXQMulUnlockBL";
	        tError.functionName = "submitData";
	        tError.errorMessage = "数据处理失败LJSXQMulUnlockBL-->dealData!";
	        this.mErrors .addOneError(tError) ;
	        return false;
	    }
	    
	    //提交
	    if(!submitData())
	    {
	    	return false;
	    }
	    return true;
	}
	 /**
	 * 根据前面的输入数据，进行BL逻辑处理
	 * 如果在处理过程中出错，则返回false,否则返回true
	*/
	private boolean dealData()
	{
		if("PAYMUL".equals(mYWType))
		{
			this.updateLJSPay();
		}
		else if("GETMUL".equals(mYWType))
		{
			this.updateLJAGet();
		}
		else
		{
			System.out.println("传入参数错误：mYWType = " + mYWType);
			return false;
		}
	    return true;
	}
	
	private void updateLJAGet() 
	{
		if(mArrayList.size()==0)
		{
			String where = " where CanSendBank = '1' and OtherNoType = '20' and SendBankCount >= 1 and PayMode = '4' ";
			if(!this.isNull(mManageCom))
			{
				where += "and managecom like '" + mManageCom + "%' ";
			}
			if(!this.isNull(mContNo))
			{
				where += "and substr(otherno,2,length(otherno)-5) = '" + mContNo + "' ";
			}
			if(!this.isNull(mGetNoticeNo))
			{
				where += "and ActuGetNo = '" + mGetNoticeNo + "' ";
			}
			StringBuffer sql = new StringBuffer();
			sql.append("update ljaget set cansendbank = null, operator = '")
			   .append(mOperater).append("', ")
			   .append("modifydate = '").append(mCurrentDate).append("', ")
			   .append("modifytime = '").append(mCurrentTime).append("' ")
			   .append(where);
			System.out.println("假更新：" + sql.toString()); 
			map.put(sql.toString(), SysConst.UPDATE);
		}
		else
		{
			for(int i=0; i<mArrayList.size(); i++)
			{
				String tActuGetNo = (String)mArrayList.get(i);
				if(!isNull(tActuGetNo))
				{
					StringBuffer sql = new StringBuffer();
					sql.append("update ljaget set cansendbank = null, operator = '")
					   .append(mOperater).append("', ")
					   .append("modifydate = '").append(mCurrentDate).append("', ")
					   .append("modifytime = '").append(mCurrentTime).append("' ")
					   .append("where actugetno = '").append(tActuGetNo).append("' ");
					System.out.println("假更新：" + sql.toString()); 
					map.put(sql.toString(), SysConst.UPDATE);
				}
			}
		}
	}

	private void updateLJSPay() 
	{
		if(mArrayList.size()==0)
		{
			String where = "where CanSendBank = '1' and OtherNoType = '2' and SendBankCount >= 1 and getnoticeno like '31%' ";
			if(!this.isNull(mManageCom))
			{
				where += "and managecom like '" + mManageCom + "%' ";
			}
			if(!this.isNull(mContNo))
			{
				where += "and otherno = '" + mContNo + "' ";
			}
			if(!this.isNull(mGetNoticeNo))
			{
				where += "and getnoticeno = '" + mGetNoticeNo + "' ";
			}
			StringBuffer sql = new StringBuffer();
			sql.append("update ljspay set cansendbank = null, operator = '")
			   .append(mOperater).append("', ")
			   .append("modifydate = '").append(mCurrentDate).append("', ")
			   .append("modifytime = '").append(mCurrentTime).append("' ")
			   .append(where);
			System.out.println("假更新：" +sql.toString()); 
			map.put(sql.toString(), SysConst.UPDATE);
		}
		else
		{
			for(int i=0; i<mArrayList.size(); i++)
			{
				String tGetNoticeNo = (String)mArrayList.get(i);
				if(!isNull(tGetNoticeNo))
				{
					StringBuffer sql = new StringBuffer();
					sql.append("update ljspay set cansendbank = null, operator = '")
					   .append(mOperater).append("', ")
					   .append("modifydate = '").append(mCurrentDate).append("', ")
					   .append("modifytime = '").append(mCurrentTime).append("' ")
					   .append("where getnoticeno = '").append((String)mArrayList.get(i)).append("' ");
					System.out.println("假更新：" +sql.toString()); 
					map.put(sql.toString(), SysConst.UPDATE);
				}
			}
		}
	}

	/**
	 * 从输入数据中得到所有对象
	 *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
	 */
	private boolean getInputData(VData cInputData)
	{
	    this.mGlobalInput = (GlobalInput)cInputData.getObjectByObjectName("GlobalInput", 0);
	    this.mOperater = this.mGlobalInput.Operator;
	    TransferData tTransferData = (TransferData)cInputData.getObjectByObjectName("TransferData", 0);
	    mManageCom = (String)tTransferData.getValueByName("ManageCom");
	    mManageCom = mManageCom.trim();
		mContNo = (String)tTransferData.getValueByName("ContNo");
		mContNo = mContNo.trim();
		mGetNoticeNo = (String)tTransferData.getValueByName("GetNoticeNo");
		mGetNoticeNo = mGetNoticeNo.trim();
		mArrayList = (ArrayList)tTransferData.getValueByName("arrayList");
		System.out.println("mManageCom="+mManageCom+",mContNo="+mContNo+",mGetNoticeNo="+mGetNoticeNo);
		return true;
	}
	 
    private boolean submitData()
    {
    	VData tVData = new VData();
    	tVData.add(map);
    	PubSubmit tPubSubmit = new PubSubmit();
        if (!tPubSubmit.submitData(tVData, ""))
        {
            // @@错误处理
            this.mErrors.copyAllErrors(tPubSubmit.mErrors);
            CError tError = new CError();
            tError.moduleName = "LJSXQMulUnlockBL";
            tError.functionName = "submitData";
            tError.errorMessage = "数据提交失败!";
            this.mErrors.addOneError(tError);
            return false;
        }
	    return true;
    }
    
    private boolean checkData() 
    {
        System.out.println("进行校验.....");
        if (mGlobalInput == null) 
        {
        	CError tError = new CError();
            tError.moduleName = "LJSXQMulUnlockBL";
            tError.functionName = "checkData";
            tError.errorMessage = "传入的mGlobalInput为空!";
            this.mErrors.addOneError(tError);
            return false;
        }
        if(isNull(mYWType))
        {
        	CError tError = new CError();
            tError.moduleName = "LJSXQMulUnlockBL";
            tError.functionName = "checkData";
            tError.errorMessage = "传入参数错误，业务类型mYWType不能为空";
            this.mErrors.addOneError(tError);
            return false;
        }
        if(isNull(mManageCom) && isNull(mContNo) && isNull(mGetNoticeNo))
        {
        	CError tError = new CError();
            tError.moduleName = "LJSXQMulUnlockBL";
            tError.functionName = "checkData";
            tError.errorMessage = "传入参数错误，管理机构、保单号、凭证号不能同时为空";
            this.mErrors.addOneError(tError);
            return false;
        }
        return true;
    }
    
    //判断字符串是否为空，空返回true，非空返回false
    //空包括 ""，null， "null" 
    private boolean isNull(String s)
    {
    	if("".equals(s) || null==s || "null".equals(s))
		{
    		return true;
		}
    	else
    	{
    		return false;
    	}
    }
}
