package com.sinosoft.lis.operfee;


import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.bq.CommonBL;
import java.util.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.xb.*;

public class PChildInsurMJDealBL{

  //错误处理类，每个需要错误处理的类中都放置该类
  public CErrors mErrors = new CErrors();
  private String dealtype ;
  private String customerBackDate;
  private String getNoticeNo;
  private VData mSubmitData = new VData();
  private GlobalInput tGI = new GlobalInput();

  /** 数据操作字符串 */
  private String serNo = ""; //流水号
  private String tLimit = "";
  private String tNo = ""; //生成的暂交费号
  private String mOperate;
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  MMap mMMap = new MMap();

  private LJTempFeeSet mLJTempFeeSet = new LJTempFeeSet();
  private LJTempFeeSet mLJTempFeeSetNew = new LJTempFeeSet();
  private LJTempFeeClassSet mLJTempFeeClassSet = new LJTempFeeClassSet();
  private LJTempFeeClassSet mLJTempFeeClassSetNew = new LJTempFeeClassSet();
  private LJTempFeeClassSet mLJTempFeeClassSetDel = new LJTempFeeClassSet();
  //应付个人明细表
  private LJSGetDrawSet tLJSGetDrawSet = new LJSGetDrawSet();

  //传输数据的公共方法
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;
    //得到外部传入的数据,将数据备份到本类中
    if (!getInputData(cInputData)) {
      return false;
    }
    System.out.println("After getinputdata");

    //进行业务处理
    if (!dealData()) {
        return false;
  }
    System.out.println("After dealData！");
    return true;
}

  //根据前面的输入数据，进行逻辑处理
  //如果在处理过程中出错，则返回false,否则返回true
  private boolean dealData() {

         if( !PChildLJAGet())
         {
             return false ;
         }
          //查找应收
          LJSPayDB tLJSPayDB = new LJSPayDB();
          tLJSPayDB.setGetNoticeNo(getNoticeNo);
          if(!tLJSPayDB.getInfo())
          {
              mErrors.addOneError("获取应收失败。");
              return false;
          }
          LJSPayPersonDB tLJSPayPersonDB = new LJSPayPersonDB();
          tLJSPayPersonDB.setGetNoticeNo(getNoticeNo);
          LJSPayPersonSet tLJSPayPersonSet = tLJSPayPersonDB.query();
          if(tLJSPayPersonSet.size()==0)
          {
              mErrors.addOneError("获取应收明细失败。");
              return false;
          }
          //生成暂收数据
          //产生流水号
          tLimit = PubFun.getNoLimit(tLJSPayDB.getManageCom());
          serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
          ExeSQL tExeSQL = new ExeSQL();
          // modify by fuxin 合并相同险种的保费，否则在ljtempfee无法插入。
          String sqlrisk = " select riskcode,sum(sumduepaymoney),managecom,ContNo From ljspayperson where contno ='"+tLJSPayDB.getOtherNo()+"' and riskcode in('320106','120706') group by riskcode,managecom,ContNo";
          SSRS tSSRS = tExeSQL.execSQL(sqlrisk);

          String[][] arr = tSSRS.getAllData() ;


          LJTempFeeSet tLJTempFeeSet = new LJTempFeeSet();
          double money =0;



          for(int i=0 ; i + 1 <= arr.length ; i++)
          {
//              LJSPayPersonSchema tLJSPayPersonSchema = tLJSPayPersonSet.get(i);
              LJTempFeeSchema tLJTempFeeSchema = new LJTempFeeSchema();
              tLJTempFeeSchema.setTempFeeNo(getNoticeNo);
              tLJTempFeeSchema.setTempFeeType("2");//续期续保
              tLJTempFeeSchema.setRiskCode(arr[i][0]);
              tLJTempFeeSchema.setOtherNo(arr[i][3]);
              tLJTempFeeSchema.setPayIntv(0);
//              tLJTempFeeSchema.setOtherNo(tLJSPayPersonSchema.getPolNo());
              tLJTempFeeSchema.setOtherNoType("0");
              tLJTempFeeSchema.setPayMoney(arr[i][1]);
              money += Double.parseDouble(arr[i][1]) ;
              tLJTempFeeSchema.setPayDate(CurrentDate);
              tLJTempFeeSchema.setEnterAccDate(CurrentDate);
              tLJTempFeeSchema.setConfMakeDate(CurrentDate);
              tLJTempFeeSchema.setConfMakeTime(CurrentTime);
              tLJTempFeeSchema.setManageCom(arr[i][2]);
              tLJTempFeeSchema.setConfFlag("0");
              tLJTempFeeSchema.setSerialNo(serNo);
              tLJTempFeeSchema.setOperator(tGI.Operator);
              tLJTempFeeSchema.setMakeDate(CurrentDate);
              tLJTempFeeSchema.setMakeTime(CurrentTime);
              tLJTempFeeSchema.setModifyDate(CurrentDate);
              tLJTempFeeSchema.setModifyTime(CurrentTime);
              tLJTempFeeSchema.setAgentCode(tLJSPayDB.getAgentCode());
              tLJTempFeeSchema.setAgentGroup(tLJSPayDB.getAgentGroup());
              tLJTempFeeSet.add(tLJTempFeeSchema);
          }
          //生成暂收分类表数据
          LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
          tLJTempFeeClassSchema.setTempFeeNo(getNoticeNo);
          tLJTempFeeClassSchema.setPayMode("5");
          tLJTempFeeClassSchema.setPayMoney(money);
          tLJTempFeeClassSchema.setPayDate(CurrentDate);
          tLJTempFeeClassSchema.setEnterAccDate(CurrentDate);
          tLJTempFeeClassSchema.setConfMakeDate(CurrentDate);
          tLJTempFeeClassSchema.setConfMakeTime(CurrentTime);
          tLJTempFeeClassSchema.setManageCom(tLJSPayDB.getManageCom());
          tLJTempFeeClassSchema.setConfFlag("0");
          tLJTempFeeClassSchema.setSerialNo(serNo);
          tLJTempFeeClassSchema.setOperator(tGI.Operator);
          tLJTempFeeClassSchema.setMakeDate(CurrentDate);
          tLJTempFeeClassSchema.setMakeTime(CurrentTime);
          tLJTempFeeClassSchema.setModifyDate(CurrentDate);
          tLJTempFeeClassSchema.setModifyTime(CurrentTime);

          //修改应收状态
          String updateLjspayb = "update ljspayb set dealstate ='4' where getnoticeno ='"+getNoticeNo+"'";
          String updateLjspayPersonB = "update ljspaypersonb set dealstate ='4' where getnoticeno ='"+getNoticeNo+"'";

          //查询保单
          LCContDB tLCContDB = new LCContDB();
          tLCContDB.setContNo(tLJSPayDB.getOtherNo());
          if (!tLCContDB.getInfo()) {
              mErrors.addOneError("查询保单错误");
              return false;
          }

          //生成暂收数据
          MMap tMMap = new MMap();
          tMMap.put(tLJTempFeeSet,"DELETE&INSERT");
          tMMap.put(tLJTempFeeClassSchema,"DELETE&INSERT");
          tMMap.put(updateLjspayb,"UPDATE");
          tMMap.put(updateLjspayPersonB,"UPDATE");
          tMMap.add(mMMap);
          //存储客户录入的信息

          mSubmitData.clear();
          mSubmitData.add(tMMap);
          PubSubmit tPubSubmit = new PubSubmit();
          if(!tPubSubmit.submitData(mSubmitData, mOperate))
          {
              mErrors.addOneError("生成财务暂收失败");
              return false;
          }
          //调用核销程序
          TransferData tTransferData = new TransferData();
          tTransferData.setNameAndValue("GetNoticeNo",getNoticeNo);

          PRnewDueVerifyUI tPRnewDueVerifyUI = new PRnewDueVerifyUI();
          VData tVData = new VData();
          tVData.add(tGI);
          tVData.add(tTransferData);
          tVData.addElement(tLCContDB.getSchema());
          tPRnewDueVerifyUI.submitData(tVData,"VERIFY");
          if(tPRnewDueVerifyUI.mErrors.needDealError())
          {
              mErrors.addOneError(" 核销失败，原因是: " + tPRnewDueVerifyUI.mErrors.getFirstError());
              mSubmitData.clear();
              tMMap = new MMap();
              tMMap.put(tLJTempFeeSet,"DELETE");
              tMMap.put(tLJTempFeeClassSchema,"DELETE");
              updateLjspayb = "update ljspayb set dealstate ='0' where getnoticeno ='"+getNoticeNo+"'";
              updateLjspayPersonB = "update ljspaypersonb set dealstate ='0' where getnoticeno ='"+getNoticeNo+"'";
              tMMap.put(updateLjspayb,"UPDATE");
              tMMap.put(updateLjspayPersonB,"UPDATE");
              mSubmitData.add(tMMap);
              tPubSubmit.submitData(mSubmitData, mOperate);
              return false;
          }

          return true;
      }
      private boolean getInputData(VData mInputData) {
              // 公用变量
              tGI = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
              TransferData tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);
              if (tGI == null || tTransferData == null)
              {
                  mErrors.addOneError("传入的数据不完整。");
                  return false;
              }
              dealtype = (String) tTransferData.getValueByName("dealType");
              customerBackDate = (String) tTransferData.getValueByName("customerBackDate");
              getNoticeNo = (String) tTransferData.getValueByName("getNoticeNo");
              if(!(valiData(dealtype)&&valiData(customerBackDate)&&valiData(getNoticeNo)))
              {
                  mErrors.addOneError("传入的数据不完整。");
                  return false;
              }
              return true;
          }
          private boolean valiData(String aData)
          {
              if(aData==null||"".equals(aData))
              {
                  return false;
              }
              return true;
          }

          private boolean PChildLJAGet() {

              LCContSchema mLCContSchema = new LCContSchema();
              LCContSet mLCContSet = new LCContSet();
              LCContDB mLCContDB = new LCContDB();
              String sqlCont = " select b.* From ljspayb a,lccont b where getnoticeno ='" +getNoticeNo + "' "
                             + " and a.otherno = b.contno ";
              mLCContSet = mLCContDB.executeQuery(sqlCont);
              if (mLCContSet == null || mLCContSet.size() == 0) {
                  mErrors.addOneError("查询保单信息失败！");
                  return false;
              }
              
              mLCContSchema = mLCContSet.get(1); //只有一个保单
              
              //判断若是由于续保核销被阻断的再次核销时，不需要生成应付
              String checkGET = "select 1 from ljsgetdraw where contno='"+mLCContSchema.getContNo()+"' " +
              		" and feefinatype='YEI' and exists (select 1 from lcget where getdutykind='0' and " +
              		" contno= ljsgetdraw.contno and insuredno=ljsgetdraw.insuredno and gettodate=ljsgetdraw.lastgettodate) with ur  ";

              String doGetFlag = new ExeSQL().getOneValue(checkGET);
              
              if(doGetFlag!=null&&"1".equals(doGetFlag)){
            	  System.out.println("该保单上次由于续保核销被阻断，已经生成满期数据，本次无需再次生成");
            	  return true;
              }
              
              Calculator tCalculator = new Calculator();
              String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
              //生成批次号
              String mserNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);

      //给付任务编号
              String taskno = " select max(otherno) from ljsget where getnoticeno in (select getnoticeno from "
                              + " ljsgetdraw where contno ='" +
                              mLCContSchema.getContNo() +
                              "')"
                              ;
              SSRS noSSRS = new ExeSQL().execSQL(taskno);
              int no = 0;
              if (noSSRS.getMaxRow() > 0 &&
                  !(noSSRS.GetText(1, 1).equals("") ||
                    noSSRS.GetText(1, 1).equals("null"))) {
                  String noString = noSSRS.GetText(1, 1);
                  no = Integer.parseInt(noString.substring(noString.length() - 1)); //modify by fuxin 2008-5-5 取最后一位。
              }

      //得到被保人需要给付的险种
              String startDate = "2005-1-1";
              String endDate = "3001-1-1";
      //取得被保人信息
              String sql = "select distinct insuredno from lcget a,lmdutygetalive b "
                           + " where a.getdutycode = b.getdutycode and contno ='"
                           + mLCContSchema.getContNo() + "'"
                           ;
              SSRS tSSRS = new ExeSQL().execSQL(sql);
              HashMap t = new HashMap();
              for (int i = 1; i <= tSSRS.getMaxRow(); i++) {

                  //给付任务号
                  no++;
                  String noString = String.valueOf(no);
                  String taskNo = "P" + mLCContSchema.getContNo();
                  for (int k = noString.length(); k < 4; k++) {
                      taskNo += "0";
                  }
                  taskNo += noString;
                  LCGetSet tLCGetSet = getLCGetNeedGet(startDate, endDate,
                                                       mLCContSchema.getContNo(),
                                                       tSSRS.GetText(i, 1));

                  if (tLCGetSet == null) {
                      continue;
                  }
                  //要素准备，被保人年龄
                  String age =
                          "select year(current date - birthday) from lcinsured where contno ='"
                          + mLCContSchema.getContNo() + "' and insuredno='" +
                          tSSRS.GetText(i, 1) + "'";

                  tCalculator.addBasicFactor("AppntAge", new ExeSQL().getOneValue(age));

                  //给付通知书号码
//                  String tLimit = PubFun.getNoLimit(mLCContSchema.getManageCom());
                  String tgetNoticeNo = "";
                  //产生退费通知书号
                  if (tgetNoticeNo == null || tgetNoticeNo.equals("")) {
                      tgetNoticeNo = PubFun1.CreateMaxNo("GETNO", tLimit);
                  }

                  double sumGetMoney = 0; //总领取金额
                  //循环每个给付责任进行处理

                  for (int j = 1; j <= tLCGetSet.size(); j++) {

                      //获取合同下的每条给付的明细信息
                      LCGetSchema tLCGetSchema = tLCGetSet.get(j).getSchema();

                      LMDutyGetAliveDB tLMDutyGetaLiveDB = new LMDutyGetAliveDB();

                      tLMDutyGetaLiveDB.setGetDutyCode(tLCGetSchema.getGetDutyCode());
                      LMDutyGetAliveSet tLMDutyGetAliveSet = tLMDutyGetaLiveDB.query();
                      LMDutyGetAliveSchema tLMDutyGetAliveSchema = tLMDutyGetAliveSet.
                              get(
                                      1);
                      String pCalCode = "";
                      if (tLMDutyGetAliveSchema.getCalCode() != null &&
                          !"".equals(tLMDutyGetAliveSchema.getCalCode())) {
                          pCalCode = tLMDutyGetAliveSchema.getCalCode();
                      }
                      if (tLMDutyGetAliveSchema.getCnterCalCode() != null &&
                          !"".equals(tLMDutyGetAliveSchema.getCnterCalCode())) {
                          pCalCode = tLMDutyGetAliveSchema.getCnterCalCode();
                      }
                      if (tLMDutyGetAliveSchema.getOthCalCode() != null &&
                          !"".equals(tLMDutyGetAliveSchema.getOthCalCode())) {
                          pCalCode = tLMDutyGetAliveSchema.getOthCalCode();
                      }

                      tCalculator.setCalCode(pCalCode);
                      //责任保额
                      String amnt = "select sum(amnt) from lcduty where polno='"
                                    + tLCGetSchema.getPolNo() + "' and dutycode='"
                                    + tLCGetSchema.getDutyCode() + "'";
                      tCalculator.addBasicFactor("Amnt",
                                                 new ExeSQL().getOneValue(amnt));
                      tCalculator.addBasicFactor("Get",
                                                 CommonBL.bigDoubleToCommonString(
                              tLCGetSchema.
                              getActuGet(), "0.00"));
                      tCalculator.addBasicFactor("PolNo", tLCGetSchema.getPolNo());

                      String tStr = tCalculator.calculate();
                      double tValue = 0;

                      if (tStr == null || tStr.trim().equals("")) {
                          tValue = 0;
                      } else {
                          tValue = Double.parseDouble(tStr);
                      }
                      sumGetMoney += tValue;

                      LJSGetDrawSchema tLJSGetDrawSchema = new LJSGetDrawSchema();
                      tLJSGetDrawSchema.setGetNoticeNo(tgetNoticeNo);
                      tLJSGetDrawSchema.setPolNo(tLCGetSchema.getPolNo());
                      tLJSGetDrawSchema.setDutyCode(tLCGetSchema.getDutyCode());
                      tLJSGetDrawSchema.setGetDutyKind(tLCGetSchema.getGetDutyKind());
                      tLJSGetDrawSchema.setGetDutyCode(tLCGetSchema.getGetDutyCode());
                      tLJSGetDrawSchema.setContNo(tLCGetSchema.getContNo());
                      tLJSGetDrawSchema.setAppntNo(mLCContSchema.getAppntNo());
                      tLJSGetDrawSchema.setInsuredNo(tSSRS.GetText(i, 1));
                      tLJSGetDrawSchema.setGetDate(CurrentDate);
                      tLJSGetDrawSchema.setGetMoney(tValue);
                      tLJSGetDrawSchema.setEnterAccDate(CurrentDate);
                      tLJSGetDrawSchema.setFeeOperationType("EB");
                      tLJSGetDrawSchema.setFeeFinaType("YEI");
                      tLJSGetDrawSchema.setManageCom(mLCContSchema.getManageCom());
                      tLJSGetDrawSchema.setAgentCom(mLCContSchema.getAgentCom());
                      tLJSGetDrawSchema.setAgentType(mLCContSchema.getAgentType());
                      tLJSGetDrawSchema.setAgentCode(mLCContSchema.getAgentCode());
                      tLJSGetDrawSchema.setAgentGroup(mLCContSchema.getAgentGroup());
                      LCPolDB tLCPolDB = new LCPolDB();
                      tLCPolDB.setPolNo(tLCGetSchema.getPolNo());
                      tLCPolDB.getInfo();
                      tLJSGetDrawSchema.setRiskCode(tLCPolDB.getRiskCode());
                      if (tLMDutyGetAliveSchema.getGetDutyKind().equals("0")) {
                          tLJSGetDrawSchema.setLastGettoDate(tLCPolDB.getEndDate());
                          tLJSGetDrawSchema.setCurGetToDate(tLCPolDB.getEndDate());
                      } else if (tLMDutyGetAliveSchema.getGetDutyKind().equals("1")) {
                          LJSGetDrawDB tLJSGetDrawDB = new LJSGetDrawDB();
                          String query =
                                  " Select max(CurGetToDate) from ljsgetdraw a,ljsget b where "
                                  +
                                  " a.getnoticeno = b.getnoticeno and b.dealstate <>'2' and a.polno='"
                                  + tLCGetSchema.getPolNo() + "' and getdutycode ='"
                                  + tLCGetSchema.getGetDutyCode() +
                                  "' and insuredno ='"
                                  + tLCGetSchema.getInsuredNo() + "'";
                          SSRS getToDateSSRS = new ExeSQL().execSQL(query);
                          if ((!getToDateSSRS.GetText(1, 1).equals("") &&
                               getToDateSSRS.GetText(1, 1) != null) &&
                              getToDateSSRS.getMaxRow() > 0) {
                              tLJSGetDrawSchema.setLastGettoDate(getToDateSSRS.
                                      GetText(1,
                                              1));
                              if (tLMDutyGetAliveSchema.getGetIntv() == 0) {
                                  tLJSGetDrawSchema.setCurGetToDate(PubFun.calDate(
                                          getToDateSSRS.GetText(1, 1), 1, "M", null));
                              } else {
                                  tLJSGetDrawSchema.setCurGetToDate(PubFun.calDate(
                                          getToDateSSRS.GetText(1, 1),
                                          tLMDutyGetAliveSchema.getGetIntv(), "M", null));
                              }
                          } else { //第一次给付
                              if (tLMDutyGetAliveSchema.getStartDateCalRef().equals(
                                      "S")) {
                                  if (tLMDutyGetAliveSchema.getGetStartUnit().equals(
                                          "A")) {
                                      tLJSGetDrawSchema.setLastGettoDate(PubFun.
                                              calDate(
                                                      tLCPolDB.getInsuredBirthday(),
                                                      tLMDutyGetAliveSchema.
                                                      getGetStartPeriod(),
                                                      "Y", null));
                                      tLJSGetDrawSchema.setCurGetToDate(PubFun.
                                              calDate(
                                                      tLCPolDB.getInsuredBirthday(),
                                                      tLMDutyGetAliveSchema.
                                                      getGetStartPeriod(),
                                                      "Y", null));

                                  } else {
                                      tLJSGetDrawSchema.setLastGettoDate(PubFun.
                                              calDate(
                                                      tLCPolDB.getCValiDate(),
                                                      tLMDutyGetAliveSchema.
                                                      getGetStartPeriod(),
                                                      tLMDutyGetAliveSchema.
                                                      getGetStartUnit(), null));
                                      tLJSGetDrawSchema.setCurGetToDate(PubFun.
                                              calDate(
                                                      tLCPolDB.getCValiDate(),
                                                      tLMDutyGetAliveSchema.
                                                      getGetStartPeriod(),
                                                      tLMDutyGetAliveSchema.
                                                      getGetStartUnit(), null));
                                  }

                              } else if (tLMDutyGetAliveSchema.getStartDateCalRef().
                                         equals("B")) {
                                  if (tLMDutyGetAliveSchema.getGetStartUnit().equals(
                                          "A")) {
                                      tLJSGetDrawSchema.setLastGettoDate(PubFun.
                                              calDate(
                                                      tLCPolDB.getInsuredBirthday(),
                                                      tLMDutyGetAliveSchema.
                                                      getGetStartPeriod(),
                                                      "Y", null));
                                      tLJSGetDrawSchema.setCurGetToDate(PubFun.
                                              calDate(
                                                      tLCPolDB.getInsuredBirthday(),
                                                      tLMDutyGetAliveSchema.
                                                      getGetStartPeriod(),
                                                      "Y", null));

                                  } else {
                                      tLJSGetDrawSchema.setLastGettoDate(PubFun.
                                              calDate(
                                                      tLCPolDB.getInsuredBirthday(),
                                                      tLMDutyGetAliveSchema.
                                                      getGetStartPeriod(),
                                                      tLMDutyGetAliveSchema.
                                                      getGetStartUnit(), null));
                                      tLJSGetDrawSchema.setCurGetToDate(PubFun.
                                              calDate(
                                                      tLCPolDB.getInsuredBirthday(),
                                                      tLMDutyGetAliveSchema.
                                                      getGetStartPeriod(),
                                                      tLMDutyGetAliveSchema.
                                                      getGetStartUnit(), null));
                                  }
                              }
                          }
                      }
                      tLJSGetDrawSchema.setSerialNo(mserNo);
                      tLJSGetDrawSchema.setOperator(tGI.Operator);
                      tLJSGetDrawSchema.setMakeDate(CurrentDate);
                      tLJSGetDrawSchema.setMakeTime(CurrentTime);
                      tLJSGetDrawSchema.setModifyDate(CurrentDate);
                      tLJSGetDrawSchema.setModifyTime(CurrentTime);
                      this.tLJSGetDrawSet.add(tLJSGetDrawSchema);

                      //计算其它奖金
                      LDCodeDB tLDCodeDB = new LDCodeDB();
                      tLDCodeDB.setCodeType("RiskGetAlive");
                      tLDCodeDB.setCode(tLCPolDB.getRiskCode());
                      if (tLDCodeDB.getInfo()) {
                          if (!t.containsKey(tLCPolDB.getPolNo())) {
                              t.put(tLCPolDB.getPolNo(), "");
                              try {
                                  Class tClass = Class.forName(tLDCodeDB.getCodeAlias());
                                  CalOther tCalOther = (CalOther) tClass.newInstance();
                                  double otherMoney = tCalOther.cal(tLCPolDB.getPolNo());
                                  if (tCalOther.getError() == null) {
                                      LJSGetDrawSchema ttLJSGetDrawSchema = new
                                              LJSGetDrawSchema();
                                      ttLJSGetDrawSchema.setSchema(tLJSGetDrawSchema);
                                      ttLJSGetDrawSchema.setDutyCode("000000");
                                      ttLJSGetDrawSchema.setGetMoney(otherMoney);
                                      this.tLJSGetDrawSet.add(ttLJSGetDrawSchema);
                                      sumGetMoney += otherMoney;
                                  } else {
                                      mErrors.copyAllErrors(tCalOther.getError());
                                      return false;
                                  }
                              } catch (Exception ex) {
                                  ex.printStackTrace();
                                  CError.buildErr(this,
                                                  tLCPolDB.getRiskCode() + "奖金给付问题");
                                  return false;
                              }
                          }
                      }
                  }
                  mMMap.put(tLJSGetDrawSet, "DELETE&INSERT");
                  LJSGetSchema tLJSGetSchema = new LJSGetSchema();
                  tLJSGetSchema.setGetNoticeNo(tgetNoticeNo);
                  tLJSGetSchema.setOtherNo(taskNo);
                  tLJSGetSchema.setOtherNoType("20");
                  tLJSGetSchema.setAccName(mLCContSchema.getAccName());
                  tLJSGetSchema.setBankAccNo(mLCContSchema.getBankAccNo());
                  tLJSGetSchema.setBankCode(mLCContSchema.getBankCode());
                  tLJSGetSchema.setDrawer(mLCContSchema.getAppntName());
                  tLJSGetSchema.setDrawerID(mLCContSchema.getAppntIDNo());
                  tLJSGetSchema.setSumGetMoney(sumGetMoney);
                  tLJSGetSchema.setGetDate(CurrentDate);
                  tLJSGetSchema.setManageCom(mLCContSchema.getManageCom());
                  tLJSGetSchema.setAgentCom(mLCContSchema.getAgentCom());
                  tLJSGetSchema.setAgentType(mLCContSchema.getAgentType());
                  tLJSGetSchema.setAgentCode(mLCContSchema.getAgentCode());
                  tLJSGetSchema.setAgentGroup(mLCContSchema.getAgentGroup());
                  tLJSGetSchema.setAppntNo(mLCContSchema.getAppntNo());
                  tLJSGetSchema.setSerialNo(mserNo);
                  tLJSGetSchema.setOperator(tGI.Operator);
                  tLJSGetSchema.setMakeDate(CurrentDate);
                  tLJSGetSchema.setMakeTime(CurrentTime);
                  tLJSGetSchema.setModifyDate(CurrentDate);
                  tLJSGetSchema.setModifyTime(CurrentTime);
                  tLJSGetSchema.setDealState("0"); //待给付
                  mMMap.put(tLJSGetSchema, "DELETE&INSERT");
                  String updateDate = " update ljsget a set getdate = (select max(CurGetToDate) from ljsgetdraw where getnoticeno = a.getnoticeno ) "
                                      + " where a.getnoticeno ='" + tgetNoticeNo +
                                      "'"
                                      ;
                  mMMap.put(updateDate, "UPDATE");
                  LJAGetSchema tLJAGetSchema = new LJAGetSchema();
                  tLJAGetSchema.setActuGetNo(tLJSGetSchema.getGetNoticeNo());
                  tLJAGetSchema.setGetNoticeNo(tLJSGetSchema.getGetNoticeNo());
                  tLJAGetSchema.setOtherNo(tLJSGetSchema.getOtherNo());
                  tLJAGetSchema.setOtherNoType(tLJSGetSchema.getOtherNoType());
                  tLJAGetSchema.setPayMode("5"); //内部转账
                  tLJAGetSchema.setManageCom(tLJSGetSchema.getManageCom());
                  tLJAGetSchema.setAgentCom(tLJSGetSchema.getAgentCom());
                  tLJAGetSchema.setAgentType(tLJSGetSchema.getAgentType());
                  tLJAGetSchema.setAgentCode(tLJSGetSchema.getAgentCode());
                  tLJAGetSchema.setAgentGroup(tLJSGetSchema.getAgentGroup());
                  tLJAGetSchema.setAppntNo(tLJSGetSchema.getAppntNo());
                  tLJAGetSchema.setAccName(tLJSGetSchema.getAccName());
                  tLJAGetSchema.setSumGetMoney(tLJSGetSchema.getSumGetMoney());
                  tLJAGetSchema.setBankAccNo(tLJSGetSchema.getBankAccNo());
                  tLJAGetSchema.setBankCode(tLJSGetSchema.getBankCode());
                  tLJAGetSchema.setDrawer(tLJSGetSchema.getDrawer());
                  tLJAGetSchema.setDrawerID(tLJSGetSchema.getDrawerID());
                  tLJAGetSchema.setEnterAccDate(CurrentDate);
                  tLJAGetSchema.setConfDate(CurrentDate);
                  tLJAGetSchema.setSerialNo(mserNo);
                  tLJAGetSchema.setOperator(tGI.Operator);
                  tLJAGetSchema.setMakeDate(CurrentDate);
                  tLJAGetSchema.setMakeTime(CurrentTime);
                  tLJAGetSchema.setModifyDate(CurrentDate);
                  tLJAGetSchema.setModifyTime(CurrentTime);
                  mMMap.put(tLJAGetSchema, "DELETE&INSERT");
                  String shouldDate = " update ljaget a set ShouldDate = (select max(getdate) from ljsget where getnoticeno = a.getnoticeno ) "
                                      + " where a.getnoticeno ='" + tgetNoticeNo +
                                      "'"
                                      ;
                  mMMap.put(shouldDate, "UPDATE");
                  //生成财务数据
                  LJFIGetSet tLJFIGetSet = new LJFIGetSet();
                  LJFIGetSchema tLJFIGetSchema = new LJFIGetSchema();
                  Reflections ref = new Reflections();
                  ref.transFields(tLJFIGetSchema,tLJAGetSchema);
                  tLJFIGetSchema.setGetMoney(tLJAGetSchema.getSumGetMoney());
                  tLJFIGetSchema.setPolicyCom(tLJAGetSchema.getManageCom());
                  tLJFIGetSet.add(tLJFIGetSchema);
                  mMMap.put(tLJFIGetSet,SysConst.INSERT);
              }
              return true;
          }

          public LCGetSet getLCGetNeedGet(String startDate,
                                          String endDate,
                                          String contNo, String insuredNo) {
              StringBuffer tSBql = new StringBuffer(128);

              tSBql.append("select b.* from lcpol a,lcget b , lmdutygetalive c ")
              .append(" where a.conttype ='1' and a.appflag = '1' and a.stateflag in ('1','3')")
              .append(" and a.polno = b.polno and b.getdutycode = c.getdutycode")
              .append(" and c.getdutykind = '0' and a.enddate >='"+startDate+"' and a.enddate <='"+endDate)
              .append("' and a.paytodate = a.payenddate and not exists ")
              .append(" (select 1 from ljaget,ljsgetdraw where ljaget.actugetno = ljsgetdraw.getnoticeno ")
              .append(" and ljaget.paymode !='5' and  ljsgetdraw.polno = a.polno)")
              .append(" and a.ContNo='" + contNo + "' and a.insuredno ='"+insuredNo+"'")
              .append(" union ")
              .append(" select b.* from lcpol a,lcget b , lmdutygetalive c ")
              .append(" where a.conttype ='1' and a.appflag = '1' and a.stateflag in ('1','3')")
              .append(" and a.polno = b.polno and b.getdutycode = c.getdutycode and c.getdutykind = '1'")
              .append(" and ( case when c.getintv is null or c.getintv = 12 then ")
              .append(" (a.cvalidate +(year('"+endDate+"')-year(a.cvalidate)) year )")
              .append(" end between '"+startDate+"' and '"+endDate+"' )")
              .append(" and not exists ( ")
              .append(" select 1 from ljsget,ljsgetdraw where ljsget.getnoticeno = ljsgetdraw.getnoticeno ")
              .append(" and ljsget.dealstate <> '2' and  ljsgetdraw.polno = a.polno and ljsgetdraw.curgettodate between '"+startDate+"' and '"+endDate+"')")
              .append(" and a.ContNo='" + contNo + "' and a.insuredno ='"+insuredNo+"'")
              ;

              LCGetSet tLCGetSet = new LCGetSet();
              LCGetDB tLCGetDB = new LCGetDB();
              tLCGetSet = tLCGetDB.executeQuery(tSBql.toString());
              System.out.println(tSBql.toString());

              if (tLCGetDB.mErrors.needDealError() == true) {
                  // @@错误处理
                  this.mErrors.copyAllErrors(tLCGetDB.mErrors);

                  CError tError = new CError();
                  tError.moduleName = "ExpirBenefitBL";
                  tError.functionName = "queryData";
                  tError.errorMessage = "lcpol表查询失败!";
                  this.mErrors.addOneError(tError);
                  return null;
              }
              if (tLCGetSet.size() == 0) {
                  CError.buildErr(this, "保单号为：" + contNo + "的保单已被其他业务员抽档。");
                  return null;
              }
              return tLCGetSet;
          }

      }
