package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
//import com.sinosoft.lis.vdb.;
import com.sinosoft.lis.vschema.LJSPayPersonSet;
import com.sinosoft.utility.*;



/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class SaveAllPersonInput
{
    //错误处理类，每个需要错误处理的类中都放置该类
    public  CErrors mErrors=new CErrors();
    /** 往后面传输数据的容器 */
    private VData mInputData ;
    private String mOperate;
    private String GrpNo;

    //业务处理相关变量
    public SaveAllPersonInput()
    {
    }
    public static void main(String[] args)
    {

    }

    //传输数据的公共方法
    public boolean submitData(VData cInputData,String cOperate)
    {
        //将操作数据拷贝到本类中
        this.mOperate =cOperate;

        //得到外部传入的数据,将数据备份到本类中
        if (!getInputData(cInputData))
            return false;
        if(!checkData())
        {
          return false;
        }
        //进行业务处理
        if (!dealData())
            return false;

        return true;
    }
    /**
     * 杨红于2005-07-21添加该校验方法
     * @return boolean
     */
    private boolean checkData()
    {
      LJSPayPersonDB tLJSPayPersonDB=new LJSPayPersonDB();
      LJSPayPersonSet tLJSPayPersonSet=new LJSPayPersonSet();
      tLJSPayPersonDB.setGrpContNo(GrpNo);
      tLJSPayPersonSet=tLJSPayPersonDB.query();
      if(tLJSPayPersonSet.size()==0)
      {
        CError.buildErr(this, "未查询到团单"+GrpNo+"下的应收个人信息！");
        return false;
      }
      return true;
    }
    private boolean dealData()
    {
        String sqlstr="update ljspayperson set inputflag='1' where grpcontno='"+GrpNo+"'";
        System.out.println("sql:"+sqlstr);
        ExeSQL tExeSQL = new ExeSQL();
        if(tExeSQL.execUpdateSQL(sqlstr)==false)
        {
            CError tError =new CError();
            tError.moduleName="SaveAllPersonInput";
            tError.functionName="dealData";
            tError.errorMessage="更新应收个人表失败!";
            this.mErrors .addOneError(tError) ;
            return false;
        }
        return true;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData)
    {
        GrpNo=(String)mInputData.getObjectByObjectName("String",0);
        if(GrpNo==null)
        {
            CError tError =new CError();
            tError.moduleName="SaveAllPersonInput";
            tError.functionName="getInputData";
            tError.errorMessage="没有得到集体保单号";
            this.mErrors .addOneError(tError) ;
            return false;
        }
        return true;
    }

}

