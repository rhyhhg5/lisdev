package com.sinosoft.lis.operfee;

import com.sinosoft.lis.schema.LJSGetSchema;
import com.sinosoft.lis.pubfun.LockTableActionBL;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.Reflections;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LJAGetSchema;
import com.sinosoft.lis.vschema.LCContStateSet;
import com.sinosoft.lis.vschema.LCPolSet;
import com.sinosoft.lis.vschema.LJSGetDrawSet;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.utility.TransferData;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.db.LCContDB;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.bq.BQ;
import com.sinosoft.utility.SSRS;
import com.sinosoft.lis.db.LDCodeDB;
import com.sinosoft.lis.vschema.LMDutyGetAliveSet;
import java.util.*;

import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.schema.LJSGetDrawSchema;
import com.sinosoft.lis.vschema.LCGetSet;
import com.sinosoft.lis.db.LCContStateDB;
import com.sinosoft.lis.db.LJSGetDB;
import com.sinosoft.lis.db.LMDutyGetAliveDB;
import com.sinosoft.lis.db.LJSGetDrawDB;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.schema.LCContStateSchema;
import com.sinosoft.lis.schema.LMDutyGetAliveSchema;
import com.sinosoft.lis.schema.LCGetSchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.pubfun.Calculator;
import com.sinosoft.lis.db.LCPolDB;
import com.sinosoft.lis.db.LCGetDB;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.utility.SysConst;

//程序名称：ExpirBenefitCalBL.java
//程序功能：重算忠诚奖
//创建日期：2010-06-10 16:06
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

public class ExpirBenefitSSBL {

    //错误处理类，每个需要错误处理的类中都放置该类
    public CErrors mErrors = new CErrors();

    /** 往后面传输数据的容器 */
    private VData saveData = new VData();
    private MMap mMMap = new MMap();
    private GlobalInput tGI = new GlobalInput();
    private TransferData mTransferData = new TransferData();
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String mserNo = "1"; //批次号
    
   
    
   // private String mGetNoticeNo = null;
    
    private String mFormula = null;
    
    private String mRateFlag = null;
    
    private String mAmnt = null;
    
    private String mCValiDate = null;
    
    private String mEndDate1 = null;
    
    private String mInsuredAppAge = null;
    
    private String mInsuYear = null;
    
    private String mInformation=null;
    
    private String mInformation1=null;
    
    private LCPolSchema mLCPolSchema = new LCPolSchema();


    private String mPrem  = "" ;

    private Reflections ref = new Reflections();

    //个人保单表
    private LCContSchema mLCContSchema = new LCContSchema();

    //应付个人明细表
    private LJSGetDrawSet tLJSGetDrawSet = new LJSGetDrawSet();


    private String anniversary = null ; //抽档当年对应的保单周年日
    

    public ExpirBenefitSSBL() {
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public boolean submitData(VData cInputData, String cOperate) {
    	
    	System.out.println("in ExpirBenefitSSBL----------------");

        VData data = getSubmitData(cInputData, cOperate);
        if (data == null) {
            return false;
        }
        PubSubmit tPubSubmit = new PubSubmit();
        if (tPubSubmit.submitData(data, "") == false) {
            this.mErrors.addOneError("PubSubmit:生成给付失败!");
            return false;
        }
        return true;
    }

    /**
     * 外部操作的提交方法
     * @param cInputData VData:包括
     * LCContSchema：保单信息
     * TransferData：是否提交标志“submit”
     * @param cOperate String
     * @return VData: 处理后的数据集合
     */
    public VData getSubmitData(VData cInputData, String cOperate) 
    {
        if (!getInputData(cInputData)) 
        {
            return null;
        }
        System.out.println("After ExpirBenefitSSBL getInputData");
        
        if (!checkData()) 
        {
            return null;
        }
        System.out.println("After ExpirBenefitSSBL checkData");
        
        //进行业务处理
        if (!dealData()) 
        {
            return null;
        }
        System.out.println("After ExpirBenefitSSBL dealData");
        
        return saveData;
    }

    /**
     * 从输入数据中得到所有对象
     *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
     */
    private boolean getInputData(VData mInputData) 
    {
        tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
        
        mTransferData = (TransferData) mInputData.getObjectByObjectName("TransferData", 0); 
        
        if ((tGI == null) || mTransferData == null) {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitCalBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到足够的数据，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        
        mFormula = (String)mTransferData.getValueByName("formula");
                
        mRateFlag = (String)mTransferData.getValueByName("rateflag");
          
        mAmnt = (String)mTransferData.getValueByName("Amnt");
        
        mCValiDate = (String)mTransferData.getValueByName("CValiDate");
        
        mEndDate1 = (String)mTransferData.getValueByName("EndDate1");
        
        mInsuredAppAge = (String)mTransferData.getValueByName("InsuredAppAge");
        
        mInsuYear = (String)mTransferData.getValueByName("InsuYear");
        
//        if (tContNo == null || tContNo == "" || tContNo.equals("")) 
//        {
//            // @@错误处理
//            CError tError = new CError();
//            tError.moduleName = "ExpirBenefitSSBL";
//            tError.functionName = "getInputData";
//            tError.errorMessage = "没有得到ContNo，请您确认!";
//            this.mErrors.addOneError(tError);
//
//            return false;
//        }
        
        if (mFormula == null || mFormula == "" || mFormula.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitSSBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到mFormula，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        if (mRateFlag == null || mRateFlag == "" || mRateFlag.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitSSBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到mRateFlag，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        if (mAmnt == null || mAmnt == "" || mAmnt.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitSSBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到mAmnt，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        if (mCValiDate == null || mCValiDate == "" || mCValiDate.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitSSBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到mCValiDate，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        if (mEndDate1 == null || mEndDate1 == "" || mEndDate1.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitSSBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到mEndDate1，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        if (mInsuredAppAge == null || mInsuredAppAge == "" || mInsuredAppAge.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitSSBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到mInsuredAppAge，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }
        
        if (mInsuYear == null || mInsuYear == "" || mInsuYear.equals("")) 
        {
            // @@错误处理
            CError tError = new CError();
            tError.moduleName = "ExpirBenefitSSBL";
            tError.functionName = "getInputData";
            tError.errorMessage = "没有得到mInsuYear，请您确认!";
            this.mErrors.addOneError(tError);

            return false;
        }

    	mLCPolSchema.setAmnt(mAmnt);
    	mLCPolSchema.setCValiDate(mCValiDate);
    	mLCPolSchema.setEndDate(mEndDate1);
    	mLCPolSchema.setInsuYear(mInsuYear);
    	mLCPolSchema.setInsuredAppAge(mInsuredAppAge);       
        return true;
    }

    /**
     *  数据校验
     */
    private boolean checkData() 
    {
        return true;
    }

    /**
     * 业务处理
     */
    private boolean dealData() 
    {
    	Cal330501BonusEasy t = new Cal330501BonusEasy(mLCPolSchema);
   	    HashMap hashMap3 = t.getBonus(mFormula,mRateFlag); //忠诚奖
   	    
   	    mInformation=t.getInformation()
   	    	        +"初始忠诚奖为："+hashMap3.get("InitMoney")
   	                +"QQQ外加忠诚奖为："+hashMap3.get("AddMoney")
   	                +"QQQ忠诚奖合计："+CommonBL.carry(Double.parseDouble((String)hashMap3.get("AddMoney"))+Double.parseDouble((String)hashMap3.get("InitMoney")))
   	    ;
   	    mInformation1="QQQ满期给付金额合计："+(mLCPolSchema.getAmnt()+CommonBL.carry(Double.parseDouble((String)hashMap3.get("AddMoney"))+Double.parseDouble((String)hashMap3.get("InitMoney"))))
   	    	+"QQQ忠诚奖合计："+CommonBL.carry(Double.parseDouble((String)hashMap3.get("AddMoney"))+Double.parseDouble((String)hashMap3.get("InitMoney")));
        System.out.println("initMoney = " + hashMap3.get("InitMoney"));
        System.out.println("addMoney = " + hashMap3.get("AddMoney"));
        System.out.println("initMoney+addMoney = " + CommonBL.carry(Double.parseDouble((String)hashMap3.get("AddMoney"))+Double.parseDouble((String)hashMap3.get("InitMoney"))));
   	    return true;
    }
    
    public String getInformation()
    {
    	return mInformation;
    }
    public String getInformation1()
    {
    	return mInformation1;
    }


    
}
