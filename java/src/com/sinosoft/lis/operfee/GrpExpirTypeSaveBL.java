package com.sinosoft.lis.operfee;
//程序名称：GrpExpirTypeSaveBL.java
//程序功能：
//创建日期：2008-11-18
//创建人  ：Zhang Guoming
//更新记录：  更新人    更新日期     更新原因/内容

import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.bq.CommonBL;
import java.util.*;
import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.xb.*;

public class GrpExpirTypeSaveBL{

  //错误处理类，每个需要错误处理的类中都放置该类
  public CErrors mErrors = new CErrors();
  private String mQueryType ;
  private String mGrpContNo;
  private GlobalInput tGI = new GlobalInput();

  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  MMap map = new MMap();

  //应付个人明细表

  //传输数据的公共方法
  public boolean submitData(VData cInputData, String cOperate) 
  {
    if (!getInputData(cInputData)) 
    {
      return false;
    }

    if (!dealData()) 
    {
        return false;
    }
    return true;
  }

  private boolean dealData() 
  {
	  EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(mGrpContNo,"TJ");
	  tEdorItemSpecialData.add("BackDate",CurrentDate);
	  tEdorItemSpecialData.add("QueryType",mQueryType);
	  if(!tEdorItemSpecialData.insert())
	  {
		  mErrors.addOneError("给付方式录入处理失败");
		  return false;
	  }
      return true;
    }
    private boolean getInputData(VData mInputData) 
    {
    	tGI = (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0);
        TransferData tTransferData = (TransferData)mInputData.getObjectByObjectName("TransferData", 0);
        if (tGI == null || tTransferData == null)
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }
        mQueryType = (String) tTransferData.getValueByName("queryType");
        mGrpContNo = (String) tTransferData.getValueByName("grpContNo");
        return true;
    }
    public CErrors getErrors()
  	{
  		return mErrors;
  	}
 }
