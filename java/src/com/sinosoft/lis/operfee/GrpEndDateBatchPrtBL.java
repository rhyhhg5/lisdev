package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.schema.LDSysVarSchema;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.db.*;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.StrTool;
import com.sinosoft.lis.schema.LDCodeSchema;
import java.io.File;
import com.sinosoft.lis.schema.LJAPaySchema;
import com.sinosoft.utility.TransferData;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GrpEndDateBatchPrtBL implements PrintService{

    private VData mResult = null;
    public CErrors mErrors = new CErrors();

    private LDSysVarSchema mLDSysVarSchema = new LDSysVarSchema();
    private LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
    private LCGrpContSet tLCGrpContSet = new LCGrpContSet();
    private GlobalInput mG = new GlobalInput();


    private int mCount ;
    private String mxmlFileName = "";
    private String strLogs = "";
    private String Content = "";
    private String FlagStr = "";
    private boolean operFlag = true;
    private String mOperate ;

    public void GrpEndDateBatchPrtBL(){

    }

    public VData getResult() {
        return mResult;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    public boolean submitData(VData cInputData, String cOperate) {
        cInputData =(VData)cInputData.clone();
        this.mOperate = cOperate;

        if(!getInputDate(cInputData)){
            return false ;
        }
        if(mOperate.equals("PRINT"))
        {
            if(!dealData())
            {
                return false;
            }
        }
        return true;
    }

    private boolean getInputDate(VData cInputData)
    {
        mLDSysVarSchema.setSchema((LDSysVarSchema)cInputData.getObjectByObjectName("LDSysVarSchema",0));
        tLCGrpContSet = (LCGrpContSet) cInputData.getObjectByObjectName("LCGrpContSet",0);
        mG = (GlobalInput) cInputData.getObjectByObjectName("GlobalInput", 0);
        if(tLCGrpContSet==null)
        {
            mErrors.addOneError("传入数据为空.");
            return false;
        }
        mG.setSchema((GlobalInput)cInputData.getObjectByObjectName("GlobalInput",0));
//        tLCGrpContSchema.setSchema((LCGrpContSchema)cInputData.getObjectByObjectName("LCGrpContSchema",0));
        return true;
    }

    private boolean dealData()
    {
        if(mOperate.equals("PRINT"))
        {
            VData tVData;
            XmlExport txmlExportAll = new XmlExport();
            txmlExportAll.createDocument("ON");
            mCount=tLCGrpContSet.size();
            String mXmlFileName[] = new String[mCount];
            System.out.println(""+mCount);

            for(int i = 1 ; i <= mCount ; i++)
            {
                System.out.println("xxxxxxxxxxxxxxxx 第"+i+"个保单号"+tLCGrpContSet.get(i).getGrpContNo());
               LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
               LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
               tLOPRTManagerDB.setCode(PrintPDFManagerBL.CODE_EXPIRY);
//               tLOPRTManagerDB.setStandbyFlag2(tLCGrpContSet.get(i).getGrpContNo());
               tLOPRTManagerDB.setOtherNo(tLCGrpContSet.get(i).getGrpContNo());
               tLOPRTManagerSchema = tLOPRTManagerDB.query().get(1);

               if(tLOPRTManagerSchema == null)
               {
                   VData xVData = new VData();
                   TransferData xTransferData = new TransferData();
                   xTransferData.setNameAndValue("0","0");

                   LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();

                   LCGrpContDB tLCGrpContDB = new LCGrpContDB();
                   tLCGrpContDB.setGrpContNo(tLCGrpContSet.get(i).getGrpContNo());
                   tLCGrpContSchema = tLCGrpContDB.query().get(1);

                   xVData.addElement(mG);
                   xVData.addElement(xTransferData);
                   xVData.addElement(tLCGrpContSchema);
                   GrpEndDateNoticeBL tGrpEndDateNoticeBL = new GrpEndDateNoticeBL();
                   tGrpEndDateNoticeBL.submitData(xVData,"INSERT");

                   LOPRTManagerDB xLOPRTManagerDB = new LOPRTManagerDB();
                   xLOPRTManagerDB.setCode(PrintPDFManagerBL.CODE_EXPIRY);
                   xLOPRTManagerDB.setOtherNo(tLCGrpContSet.get(i).getGrpContNo());
//                   tLOPRTManagerSchema = null;
                   tLOPRTManagerSchema = tLOPRTManagerDB.query().get(1);

               }
               mxmlFileName = StrTool.unicodeToGBK("0"+tLOPRTManagerSchema.getCode())+"-"+tLOPRTManagerSchema.getPrtSeq()+"-"+tLOPRTManagerSchema.getOtherNo();

               mXmlFileName[i-1] = mxmlFileName;
               System.out.println("第"+i+"个"+mxmlFileName);
               if(!callPrintService(tLOPRTManagerSchema))
               {
                   FlagStr = "Fail";
                   Content=" 印刷号"+tLOPRTManagerSchema.getPrtSeq()+"："+this.mErrors.getFirstError().toString();
                   strLogs=strLogs+Content;
                   continue;
               }

               XmlExport txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);

               if(txmlExport == null)
               {
                   FlagStr = "Fail";
                   Content="印刷号"+tLOPRTManagerSchema.getPrtSeq()+"没有得到要显示的数据文件！";
                   strLogs=strLogs+Content;
                   continue;
               }

               if (operFlag == true)
               {
                       File f = new File(mLDSysVarSchema.getSysVarValue());
                       f.mkdir() ;
                       System.out.println("PATH : "+mLDSysVarSchema.getSysVarValue());
                       System.out.println("PATH2 : "+mLDSysVarSchema.getSysVarValue().substring(0,mLDSysVarSchema.getSysVarValue().length()-1));
                       txmlExport.outputDocumentToFile(mLDSysVarSchema.getSysVarValue().substring(0,mLDSysVarSchema.getSysVarValue().length()-1) + File.separator + "printdata"
                                                       + File.separator + "data" + File.separator + "brief" + File.separator, mxmlFileName);
                }
            }

            //把数据放入 mResult 中
            tVData = new VData();
            tVData.add(mXmlFileName);
            mResult=tVData;
      }
      return true;
    }

    private boolean callPrintService(LOPRTManagerSchema aLOPRTManagerSchema)
    {
            // 查找打印服务
            String strSQL = "SELECT * FROM LDCode WHERE CodeType = 'print_service'";
            strSQL += " AND Code = '" + aLOPRTManagerSchema.getCode() + "'";
            strSQL += " AND OtherSign = '0'";
            System.out.println(strSQL);

            LDCodeSet tLDCodeSet = new LDCodeDB().executeQuery(strSQL);
            if(tLDCodeSet.size()== 0)
            {
                CError tError = new CError();
                tError.moduleName = "dealData";
                tError.functionName = "getInputData";
                tError.errorMessage = "传入的数据为空!";
                this.mErrors.addOneError(tError);
                return false;
            }

            LDCodeSchema tLDCodeSchema = tLDCodeSet.get(1);

            try
            {
                Class cls = Class.forName(tLDCodeSchema.getCodeAlias());
                PrintService ps = (PrintService) cls.newInstance();

                // 准备数据
                String strOperate = tLDCodeSchema.getCodeName();

                VData vData = new VData();

                vData.add(mG);
                vData.add(aLOPRTManagerSchema);

                if (!ps.submitData(vData, strOperate))
                {
                    mErrors.copyAllErrors(ps.getErrors());
                    return false;
                }

                mResult = ps.getResult();

            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                mErrors.addOneError("调用callPrintService方法失败");
                System.out.println("callPrintService 失败:"+ex.getMessage());
                return false;
            }

            return true ;
    }

    public int getCount()
    {
        return mCount;
    }

}
