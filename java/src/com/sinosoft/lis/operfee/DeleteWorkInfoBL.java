package com.sinosoft.lis.operfee;

import com.sinosoft.lis.db.*;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.task.Task;
import com.sinosoft.task.TaskInputBL;
import com.sinosoft.lis.bq.CommonBL;


//程序名称：DeleteWorkInfoBL.java
//程序功能：
//创建日期：2008-11-13 
//创建人  ：Zhang Guoming
//更新记录：  更新人    更新日期     更新原因/内容

public class DeleteWorkInfoBL
{
    public CErrors mErrors = new CErrors();
    private GlobalInput mGI = new GlobalInput();
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private String mWorkNo = null;
    
    private MMap map = new MMap();

    public DeleteWorkInfoBL()
    {
    }

    public boolean submitData(VData data, String operate)
    {
    	if(!getInputData(data))
        {
            return false;
        }

        if(!dealData())
        {
            return false;
        }
        
        PubSubmit tPubSubmit = new PubSubmit();
        VData tVData = new VData();
        tVData.add(map);
        if(!tPubSubmit.submitData(tVData, ""))
        {
            System.out.println(tPubSubmit.mErrors.getErrContent());
            mErrors.addOneError("提交数库失败");
            return false;
        }
        return true;
    }

    private boolean checkData()
    {
        return true;
    }

    /**
     * dealData
     *
     * @return boolean
     */
    private boolean dealData()
    {
    	LGWorkDB tLGWorkDB = new LGWorkDB();
    	tLGWorkDB.setContNo(mLCGrpContSchema.getGrpContNo());
    	tLGWorkDB.setInnerSource("LPJ");
    	tLGWorkDB.setAcceptWayNo("8");
    	LGWorkSet tLGWorkSet = tLGWorkDB.query();
    	for(int i=1;i<=tLGWorkSet.size();i++)
    	{
    		String tempWorkNo = tLGWorkSet.get(i).getWorkNo();
    		String sql1 = "delete from LGWork where WorkNo = '" + tempWorkNo + "' ";
    		map.put(sql1, "DELETE");
    		String sql2 = "delete from LGWorkTrace where WorkNo = '" + tempWorkNo + "' ";
    		map.put(sql2, "DELETE");
    		String sql3 = "delete from LGTraceNodeOp where WorkNo = '" + tempWorkNo + "' ";
    		map.put(sql3, "DELETE");
    		String sql4 = "delete from LPEdorApp where EdorAcceptNo = '" + tempWorkNo + "' ";
    		map.put(sql4, "DELETE");
    		String sql5 = "delete from LPDiskImport where EdorNo = '" + tempWorkNo + "' ";
    		map.put(sql5, "DELETE");
    	}
        return true;
    }

    private boolean getInputData(VData data)
    {
        mGI.setSchema((GlobalInput)data.getObjectByObjectName("GlobalInput", 0));
        if(mGI == null || mGI.Operator == null)
        {
            mErrors.addOneError("没有获取到操作员信息");
            return false;
        }
        mLCGrpContSchema = (LCGrpContSchema)data.getObjectByObjectName("LCGrpContSchema", 0);
        return true;
    }
    
    public String getWorkNo()
    {
    	return mWorkNo;
    }

    public static void main(String[] args)
    {
        DeleteWorkInfoBL bl = new DeleteWorkInfoBL();
    }
}
