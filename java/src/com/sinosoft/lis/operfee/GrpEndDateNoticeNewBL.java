package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.XmlExport;
import com.sinosoft.lis.vschema.LCGrpContSet;
import com.sinosoft.utility.VData;
import com.sinosoft.lis.bq.CommonBL;
import com.sinosoft.lis.db.LDComDB;
import com.sinosoft.utility.TextTag;
import com.sinosoft.lis.db.LCGrpContDB;
import com.sinosoft.lis.db.LCGrpAddressDB;
import com.sinosoft.lis.vschema.LCGrpAddressSet;
import com.sinosoft.lis.db.LCGrpAppntDB;
import com.sinosoft.utility.CError;
import com.sinosoft.lis.schema.LCGrpAddressSchema;
import com.sinosoft.lis.db.LAAgentDB;
import com.sinosoft.lis.schema.LCGrpContSchema;
import com.sinosoft.lis.db.LDUserDB;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.lis.f1print.PrintService;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.LOPRTManagerSet;
import com.sinosoft.lis.db.LOPRTManagerDB;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.f1print.PrintPDFManagerBL;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 通知书打印业务处理类，接收GrpEndDateNoticeUI.java传入的数据，
 * 执行Sql得到所要打印的保单记录，对每一保单进行校验，若是特需险种，则自动去除，不生成打印数据。
 * 若LCGrpContSet为空，则全部生成Sql得到的保单打印，否则只生成LCGrpContSet中保单打印数据。
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class GrpEndDateNoticeNewBL implements PrintService {
    /** 错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = null; //用户信息。
    private String mCurDate = PubFun.getCurrentDate(); //当前日期
    private String mCurTime = PubFun.getCurrentTime(); //当前时间
    private LCGrpContSet mLCGrpContSet = null; //选择的保单信息
    private LCGrpContSchema mLCGrpContSchema = new LCGrpContSchema();
    private LCGrpContDB mLCGrpContDB = new LCGrpContDB();
    private LOPRTManagerSchema mLOPRTManagerSchema;
    private XmlExport mXmlExport = null;
    private String mSql = null;
    private VData mData = null;
    private String mOperate = "";
    private int prt = -1; // 判断是否需要打印,0-为需要打印.

    public GrpEndDateNoticeNewBL() {
    }

    /*
     public boolean submitData(VData cInputData, String cOperate){

     }
     */

    public VData getResult() {
        return mData;
    }

    public CErrors getErrors() {
        return mErrors;
    }

    /**
     * 操作的提交方法，得到通知数据
     * @param cInputData VData：包括
     *   1.	Sql：页面查询清单的Sql语句。
     *   2. LCGrpContSet：选择的保单信息
     *   3. GlobalInput:对象，完整的登陆用户信息
     * @param cOperate String:操作方式，此为空
     * @return XmlExport：通知书数据，失败则为null
     */
    public XmlExport getXmlExport(VData cInputData, String cOperate) {
        mOperate = cOperate;
        /*   if(!getInputData(cInputData))
                {
                    return null;
                }
         */
        XmlExport xml = null;
        if (this.mLCGrpContSet == null || mLCGrpContSet.size() == 0) {
            mErrors.addOneError("没有传入保单信息");
            return null;
        }

        try {
            xml = getOneXmlExport(mLCGrpContSet.get(1).getGrpContNo());
            mData = new VData();
            mData.addElement(xml);
            xml.outputDocumentToFile("C:\\", "sinosoft");
            getPrtPDF();

        } catch (Exception e) {
            mErrors.addOneError("生成xml时发生问题.");
        }
        return xml;
    }

    /**
     * 调用getXmlExport得到通知数据，并存储到数据库
     *   1.	Sql：页面查询清单的Sql语句。
     *   2. LCGrpContSet：选择的保单信息
     *   3. GlobalInput:对象，完整的登陆用户信息
     * @param cOperate String:操作方式，此为空
     * @return boolean: 操作成功true，失败则为false
     */
    public boolean submitData(VData cInputData, String cOperate) {
        if (!getInputData(cInputData)) {
            return false;
        }

        // 获取打印数据
        getXmlExport(mData, cOperate);

        // 添加到打印列表
//        if(!addPrtManager())
//        {
//            return false;
//        }
        if (!dealPrintMag()) {
            return false;
        }

        return true;
    }

    /**
     * 得到传入的数据
     * @param data VData：submitData中传入的VData集合
     * @return boolean：成功true，否则false
     */
    private boolean getInputData(VData data) {

        try {
            mLOPRTManagerSchema = (LOPRTManagerSchema) data.
                                  getObjectByObjectName(
                                          "LOPRTManagerSchema", 0);
            if (mLOPRTManagerSchema == null) {
                mLCGrpContSchema = (LCGrpContSchema) data.getObjectByObjectName(
                        "LCGrpContSchema", 0);
                mLCGrpContDB.setGrpContNo(mLCGrpContSchema.getGrpContNo());
                if (!mLCGrpContDB.getInfo()) {
                    mErrors.addOneError("数据不完整.");
                    return false;
                }
                mLCGrpContSchema = mLCGrpContDB.getSchema();

            } else {
                mLOPRTManagerSchema = (LOPRTManagerSchema) data.
                                      getObjectByObjectName(
                                              "LOPRTManagerSchema",
                                              0);
                mLCGrpContDB.setGrpContNo(mLOPRTManagerSchema.getOtherNo());
                if (!mLCGrpContDB.getInfo()) {
                    mErrors.addOneError("数据不完整.");
                    return false;
                }
                mLCGrpContSchema = mLCGrpContDB.getSchema();

            }

            mGlobalInput = (GlobalInput) data
                           .getObjectByObjectName("GlobalInput", 0);

            mLCGrpContSet = (LCGrpContSet) data
                            .getObjectByObjectName("LCGrpContSet", 0);
            if (mLCGrpContSet == null) {
                mLCGrpContSet = new LCGrpContSet();
            }
            mLCGrpContSet.add(mLCGrpContSchema);

            if (mGlobalInput == null || mGlobalInput.Operator == null
                || mGlobalInput.Operator.equals("")) {
                mErrors.addOneError("传入的数据不完整。");
                return false;
            }

        } catch (Exception e) {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("传入的数据不完整，" + e.toString());
            return false;

        }
        return true;
    }

    /**
     * 处理业务逻辑，生成打印数据
     * @return boolean：成功true，否则false
     */
    private XmlExport getOneXmlExport(String grpContNo) {
        XmlExport xmlExport = new XmlExport();
        xmlExport.createDocument("PrtGrpEndDateNotice.vts", "printer");

        LCGrpContDB tLCGrpContDB = new LCGrpContDB();
        tLCGrpContDB.setGrpContNo(grpContNo);
        if (!tLCGrpContDB.getInfo()) {
            mErrors.addOneError("没有查询到保单" + grpContNo + "信息");
            return null;
        }

        LDComDB tLDComDB = new LDComDB();
        tLDComDB.setComCode(tLCGrpContDB.getManageCom());
        tLDComDB.getInfo();

        TextTag textTag = new TextTag();
        textTag.add("JetFormType", "98");
        //借操作员信息中的机构号存储打印所需要配置的机构号  修改于08/11/17
        String sqlusercom = "select comcode from lduser where usercode='" +
                            mGlobalInput.Operator + "' with ur";
        String comcode = new ExeSQL().getOneValue(sqlusercom);
        if (comcode.equals("86") || comcode.equals("8600") ||
            comcode.equals("86000000")) {
            comcode = "86";
        } else if (comcode.length() >= 4) {
            comcode = comcode.substring(0, 4);
        } else {
            mErrors.addOneError("操作员机构查询出错！");
        }
        String printcom =
                "select codename from ldcode where codetype='pdfprintcom' and code='" +
                comcode + "' with ur";
        String printcode = new ExeSQL().getOneValue(printcom);
        textTag.add("ManageComLength4", printcode);
        textTag.add("userIP", mGlobalInput.ClientIP.replaceAll("\\.", "_"));
        if ("batch".equals(mOperate)) {
            textTag.add("previewflag", "0");
        } else {
            textTag.add("previewflag", "1");
        }

        textTag.add("ServicePhone", tLDComDB.getServicePhone());
        textTag.add("letterservicename", tLDComDB.getLetterServiceName());
        textTag.add("ServiceAddress", tLDComDB.getServicePostAddress());
        textTag.add("ServiceZip", tLDComDB.getServicePostZipcode());
        textTag.add("ComName", tLDComDB.getLetterServiceName());
        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        textTag.add("Fax", tLDComDB.getFax());
        textTag.add("EndDate", getEndDate(grpContNo));

        textTag.add("BarCode1",
                    PubFun1.CreateMaxNo("PayNoticeNo",
                                        PubFun.getNoLimit(
                                                tLCGrpContDB.getManageCom()))); //受理号
        textTag.add("BarCodeParam1",
                    "BarHeight=20&BarRation=3&BarWidth=1&BgColor=FFFFFF&ForeColor=000000&XMargin=10&YMargin=10");

        //地址信息
        LCGrpAppntDB tLCGrpAppntDB = new LCGrpAppntDB();
        tLCGrpAppntDB.setGrpContNo(grpContNo);

        if (!tLCGrpAppntDB.getInfo()) {
            CError.buildErr(this,
                            "团单投保人表中缺少数据");
            return null;
        }

        String sql = "select * from LCGrpAddress where CustomerNo=(select appntno from LCGrpCont where grpcontno='" +
                     grpContNo + "') AND AddressNo = '"
                     + tLCGrpAppntDB.getAddressNo() +
                     "'";
        System.out.println("sql=" + sql);
        LCGrpAddressDB tLCGrpAddressDB = new LCGrpAddressDB();
        LCGrpAddressSet tLCGrpAddressSet = tLCGrpAddressDB.executeQuery(sql);
        LCGrpAddressSchema tLCGrpAddressSchema = tLCGrpAddressSet.get(
                tLCGrpAddressSet.size()).getSchema();
        textTag.add("GrpZipCode", tLCGrpAddressSchema.getGrpZipCode());
        textTag.add("GrpAddress", tLCGrpAddressSchema.getGrpAddress());
        textTag.add("LinkMan1", tLCGrpAddressSchema.getLinkMan1());
        textTag.add("AppntNo", tLCGrpContDB.getAppntNo());
        textTag.add("GrpContNo", grpContNo);
        textTag.add("GrpName", tLCGrpContDB.getGrpName());
        textTag.add("Phone1", tLCGrpAddressSchema.getPhone1());
        textTag.add("XI_ManageCom", tLCGrpContDB.getManageCom());

        LDUserDB tLDUserDB = new LDUserDB();
        tLDUserDB.setUserCode(mGlobalInput.Operator);
        tLDUserDB.getInfo();
        textTag.add("Operator", tLDUserDB.getUserName());

        //业务员信息
        LAAgentDB tLaAgentDB = new LAAgentDB();
        tLaAgentDB.setAgentCode(tLCGrpContDB.getAgentCode());
        tLaAgentDB.getInfo();
        textTag.add("AgentName", tLaAgentDB.getName());
        textTag.add("AgentCode", tLaAgentDB.getGroupAgentCode());
        textTag.add("Phone", tLaAgentDB.getPhone());
        textTag.add("AgentGroup", tLaAgentDB.getAgentGroup());

        textTag.add("PrintDate", CommonBL.decodeDate(PubFun.getCurrentDate()));
        xmlExport.addTextTag(textTag);

        return xmlExport;
    }

    /**
     * 得到终止日期
     * @param grpContNo String
     * @return String
     */
    private String getEndDate(String grpContNo) {
        String sql = "select max(endDate) from LCPol "
                     + "where grpContNo = '" + grpContNo + "' ";
        String endDate = new ExeSQL().getOneValue(sql);
        if (endDate.equals("") || endDate.equals("null")) {
            return "";
        } else {
            return CommonBL.decodeDate(endDate);
        }
    }

    public boolean getPrtPDF() {
//        LOPRTManagerSet tLOPRTManagerSet = new LOPRTManagerSet();
//        LOPRTManagerDB tLOPRTManagerDB = new LOPRTManagerDB();
//        tLOPRTManagerDB.setOtherNo(mLCGrpContDB.getGrpContNo());
//        tLOPRTManagerDB.setCode("98");
//        tLOPRTManagerSet = tLOPRTManagerDB.query();
//        prt = tLOPRTManagerSet.size();
//        if (prt == 0) {
//            try {
        String tLimit = PubFun.getNoLimit(mLCGrpContSchema.getManageCom());
        String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        mLOPRTManagerSchema = new LOPRTManagerSchema();
        mLOPRTManagerSchema.setPrtSeq(prtSeqNo);
        mLOPRTManagerSchema.setOtherNo(mLCGrpContSchema.getGrpContNo());
        mLOPRTManagerSchema.setCode(PrintPDFManagerBL.CODE_EXPIRY);
        mLOPRTManagerSchema.setOtherNoType("01");
        mLOPRTManagerSchema.setManageCom(mLCGrpContSchema.getManageCom());
        mLOPRTManagerSchema.setAgentCode(mLCGrpContSchema.getAgentCode());
        mLOPRTManagerSchema.setReqCom(mLCGrpContSchema.getManageCom());
        mLOPRTManagerSchema.setReqOperator(mLCGrpContSchema.getOperator());
        mLOPRTManagerSchema.setPrtType("0");
        mLOPRTManagerSchema.setStateFlag("0");
        mLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
        mLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
//            } catch (Exception e) {
//                mErrors.addOneError("添加打印数据失败");
//                return false;
//            }
//    }

        return true;
    }


    public boolean addPrtManager() {
        if (prt == 0) {
            if (mLOPRTManagerSchema != null) {
                MMap tMap = new MMap();
                tMap.put(mLOPRTManagerSchema, "INSERT");
                VData tInputData = new VData();
                tInputData.add(tMap);
                PubSubmit tPubSubmit = new PubSubmit();
                if (tPubSubmit.submitData(tInputData, "") == false) {
                    this.mErrors.addOneError("PubSubmit方法执行失败!");
                    return false;
                }
            } else {
                mErrors.addOneError("LOPRTManagerSchema为空");
                return false;
            }
        }
        return true;
    }

    /**
     * 加入到打印列表
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealPrintMag() {
        mData.addElement(mLOPRTManagerSchema);
        return true;
    }

    public static void main(String[] args) {
        GlobalInput tGlobalInput = new GlobalInput();
        tGlobalInput.ManageCom = "86";
        tGlobalInput.Operator = "pa0005";

        LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
        tLCGrpContSchema.setGrpContNo("0000005501");

        LCGrpContSet tLCGrpContSet = new LCGrpContSet();
        tLCGrpContSet.add(tLCGrpContSchema);

        VData tVData = new VData();
        tVData.addElement(tGlobalInput);
        tVData.add(tLCGrpContSet);

        GrpEndDateNoticeNewBL bl = new GrpEndDateNoticeNewBL();
        XmlExport xml = bl.getXmlExport(tVData, "");
    }
}
