package com.sinosoft.lis.operfee;

import java.math.BigDecimal;

import com.sinosoft.lis.bq.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.lis.pubfun.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.LCContStateDBSet;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.lis.xb.*;
import com.sinosoft.utility.*;

/**
 * <p>Title: PICCH核心业务系统</p>
 *
 * <p>Description:
 * 接收IndiDueFeeBackUI传入的数据，进行业务逻辑的处理。
 * 通过生成对应实收记录的负费记录方式进行原实收保费的抵消，实现需求的实收保费转出。
 * 1、	生成对应的负费记录，实现转出财务数据LJAPayPerson、LJAPayGrp、LJAPay操作
 * 2、	回退保单数据到续保续保抽档前状态。
 * </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: sinosoft</p>
 *
 * @author Yang Yalin
 * @version 1.3
 */
public class IndiDueFeeBackBL
{
    /**错误的容器。*/
    public CErrors mErrors = new CErrors();
    private GlobalInput mGlobalInput = null;  //完整的操作员信息
    private LJSPayBSchema mLJSPayBSchema = null;  //应收备份数据。
    private LJAPaySchema mLJAPaySchema = null;  //本次回退的应收数据。
    private LCContSchema mLCContSchema = null;  //本保单信息
    private String mCurrentDate = PubFun.getCurrentDate();
    private String mCurrentTime = PubFun.getCurrentTime();
    private String mPayNo = null;
    private boolean mIsXBPol = false;  //险种是续保险种标志
    private boolean mHasXBPol = false; //本次实收是否有续保险种
    private Reflections ref = new Reflections();
    private LCRnewStateLogSchema mLCRnewStateLogSchema = null;  //险种续保日志
    private boolean mDoBackOtherTask = false;  //其他任务正在做转出
    private boolean mIsChildRisk = false ; //少儿险标志
    private ExeSQL mExeSQL = new ExeSQL();
    private FDate fDate = new FDate();

    private MMap map = new MMap();  //待提交的数据集合

    public IndiDueFeeBackBL()
    {
    }

    /**
     * 操作的提交方法，进行续期续保实收保费转出业务逻辑处理并提交数据库。
     * 开始时将应收状态更新为正转出5，成功后为转出6，若操作失败则回退为核销成功1
     * @param cInputData VData:
     * A.	GlobalInput对象，完整的登陆用户信息。
     * B.	LJSPayB对象，应收记录备份信息，只需GetNoticeNo即可。
     * @param cOperate String：此为“”
     * @return boolean：boolean, 成功true，否则false
     */
    public boolean submitData(VData cInputData, String cOperate)
    {
        try
        {
            if(!dealSubmit(cInputData, cOperate))
            {
                if(!mDoBackOtherTask)
                {
                    dealUrgeLog(SysConst.DELETE);
                }
                return false;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            dealUrgeLog(SysConst.DELETE);
            return false;
        }
        dealUrgeLog(SysConst.DELETE);
        return true;
    }

    /**
     * 执行回退动作
     * @param cInputData VData：submitData中串入的VData
     * @param cOperate String：“”
     * @return boolean：boolean, 成功true，否则false
     */
    private boolean dealSubmit(VData cInputData, String cOperate)
    {
        if(!getInputData(cInputData))
        {
            return false;
        }

        //万能险不能做实收保费转出 add by xp 091029    
        /*
        ExeSQL tExeSQL=new ExeSQL();
        String tRiskCode ="";
        tRiskCode = tExeSQL.getOneValue("select riskcode From ljspaypersonb where riskcode='330801' and  getnoticeno ='"+mLJSPayBSchema.getGetNoticeNo()+"'");
        if (tRiskCode.equals("330801")&& tRiskCode != null) {
        	mErrors.addOneError("万能险不能做实收保费转出");
            return false;
		}
        */
        if(!checkData())
        {
            return false;
        }

        //进行业务处理
        if(!dealData())
        {
            return false;
        }

        VData d = new VData();
        d.add(map);
        PubSubmit p = new PubSubmit();
        if(!p.submitData(d, ""))
        {
            System.out.println(p.mErrors.getErrContent());
            mErrors.addOneError("提交数据错误");
            return false;
        }

        return true;
    }

    /**
     * 进行业务逻辑处理。
     i.	生成对应的负费记录，实现转出财务数据LJAPayPerson、LJAPay操作
     ii.调用类PRnewContBackBL进行保单数据的回退。
     * @return boolean：操作成功true，否则false
     */
    private boolean dealData()
    {
        //得到收据号
        String limit = PubFun.getNoLimit("86110000");
        mPayNo = PubFun1.CreateMaxNo("PayNo",limit);
        if(!backFinace())
        {
            return false;
        }

        if(!backPol())
        {
            return false;
        }

        if(mHasXBPol)
        {
            if(!backXBCont())
            {
                return false;
            }
        }
        else
        {
            backDueFeeCont();
        }

        if(!dealAcc())
        {
            return false;
        }
        //modify by fuxin 2008-4-17 少儿险实收保费转出后，把polstate弄成 03050001 满期标志
        //modify zhanggm 2010-4-26 少儿险实收保费转出后，直接终止
        if(mIsChildRisk)
        {
            if(!callSingleTerminate(mLJSPayBSchema))
            {
            	return false;
            }
        }

        map.put(changeState(FeeConst.DEALSTATE_BACK_SUCC), "UPDATE");

        return true;
    }

    /**
     * 更新应收状态为state
     */
    private String changeState(String state)
    {
        String sql = "  update LJSPayB "
                     + "set dealState = '" + state + "' "
                     + "where getNoticeNo = '"
                     + this.mLJAPaySchema.getGetNoticeNo() + "' ";
        return sql;
    }

    /**
     * 将转出的实收保费转入帐户
     * @return boolean
     */
    private boolean dealAcc()
    {

        LCAppAccDB tLCAppAccDB =new LCAppAccDB();
        tLCAppAccDB.setCustomerNo(mLCContSchema.getAppntNo());
        LCAppAccSet tLCAppAccSet = tLCAppAccDB.query();

        if(tLCAppAccSet.size()==0)
        {
            AppAcc mAppAcc = new AppAcc();

            MMap mMMap = new MMap();
            LCAppAccTraceSchema schema = new LCAppAccTraceSchema();
            schema.setCustomerNo(mLCContSchema.getAppntNo());

            schema.setOtherNo(mLJSPayBSchema.getGetNoticeNo());
            schema.setOtherType("1");
            schema.setMoney(0);
            schema.setOperator(mLCContSchema.getOperator());

            mMMap.add(mAppAcc.accShiftToXQY(schema, "0"));

            VData dd = new VData();
            dd.add(mMMap);
            PubSubmit pub = new PubSubmit();
            if(!pub.submitData(dd, ""))
            {
                System.out.println(pub.mErrors.getErrContent());
                mErrors.addOneError("自动创建帐户,提交失败!");
                return false;
            }
        }

        AppAcc tAppAcc = new AppAcc();

        LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
        tLCAppAccTraceSchema.setCustomerNo(mLCContSchema.getAppntNo());
        tLCAppAccTraceSchema.setAccType("1");
        tLCAppAccTraceSchema.setOtherNo(mLCContSchema.getContNo());
        tLCAppAccTraceSchema.setBakNo(mLJSPayBSchema.getGetNoticeNo());
        tLCAppAccTraceSchema.setOtherType("2");
        tLCAppAccTraceSchema.setMoney(getSumDuePayMoney());
        tLCAppAccTraceSchema.setOperator(mGlobalInput.Operator);
        MMap tMap = tAppAcc.accShiftToXSYIJ(tLCAppAccTraceSchema);
        if(tMap == null)
        {
            mErrors.copyAllErrors(tAppAcc.mErrors);
            return false;
        }
        map.add(tMap);

        return true;
    }

    /**
     * 计算本次续期总缴费，包括财务缴费和余额抵扣
     * @return double
     */
    private double getSumDuePayMoney()
    {
        String sql = "  select sum(sumDuePayMoney) "
                     + "from LJAPayPerson "
                     + "where getNoticeNo = '"
                     + mLJSPayBSchema.getGetNoticeNo() + "' "
                     + "   and payType = 'ZC' "
                     + "   and sumDuePayMoney > 0 ";
        String moneyStr = new ExeSQL().getOneValue(sql);
        if(moneyStr.equals("") || moneyStr.equals("null"))
        {
            mErrors.addOneError("查询续期总缴费出错");
            return Double.MIN_VALUE;
        }
        else
        {
            return Double.parseDouble(moneyStr);
        }
    }

    /**
     * 回退有续保险种保单数据
     * @return boolean：操作成功true，否则false
     */
    private boolean backXBCont()
    {
        //从LBPrem表得到原缴费项信息，回退缴费项数据
        LBContDB tLBContDB = new LBContDB();
        tLBContDB.setContNo(mLCRnewStateLogSchema.getNewContNo());
        if(!tLBContDB.getInfo())
        {
            mErrors.addOneError("没有查询到险种的缴费项信息"
                                + mLCRnewStateLogSchema.getContNo());
            return false;
        }

        LCContSchema schema = new LCContSchema();
        ref.transFields(schema, tLBContDB.getSchema());

        schema.setContNo(mLCContSchema.getContNo()); //号码换成续保前号码
        schema.setProposalContNo(mLCContSchema.getProposalContNo());
        schema.setOperator(mGlobalInput.Operator);
        schema.setModifyDate(this.mCurrentDate);
        schema.setModifyTime(this.mCurrentTime);
        schema.setStateFlag(BQ.STATE_FLAG_SIGN);
        map.put(schema, "DELETE&INSERT");
        map.put(tLBContDB.getSchema(), "DELETE");

        //删除lccontstate数据 modify by wanghl
        LCContStateDB tLCContStateDB=new LCContStateDB();
        tLCContStateDB.setOtherNo(tLBContDB.getSchema().getEdorNo());
        tLCContStateDB.setContNo(tLBContDB.getSchema().getContNo());
        LCContStateSet LCContStateSet=tLCContStateDB.query();
        if(LCContStateSet!=null&&LCContStateSet.size()>0)
        {
        	 map.put(LCContStateSet, "DELETE");	
        }
       
        
        //删除直接从C表复制到B表的数据
        String[] tables = {"LBCustomerImpart", "LBCustomerImpartDetail",
                          "LBCustomerImpartParams", "LBAppnt", "LBInsured"};
        for(int i = 0; i < tables.length; i++)
        {
            String sql = "delete from " + tables[i]
                         + " where contNo = '"
                         + mLCRnewStateLogSchema.getNewContNo() + "' ";
            map.put(sql, "DELETE");
        }

        return true;
    }

    /**
     * 回退只有续期险种的保单数据
     * @return boolean
     */
    private boolean backDueFeeCont()
    {
        String sql = "update LCCont a "
              + "set (prem, sumPrem, payToDate) = "
              + "   (select sum(prem), sum(sumPrem), min(payToDate) "
              + "   from LCPol "
              + "   where contNo = a.contNo), "
              + "   operator = '" + this.mGlobalInput.Operator + "', "
              + "   modifyDate = '" + this.mCurrentDate + "', "
              + "   modifyTime = '" + this.mCurrentTime + "' "
              + "where contNo = '" + this.mLCContSchema.getContNo() + "' ";
        map.put(sql, "UPDATE");

        return true;
    }

    /**
     * 回退险种数据
     * 将保单备份数据从B表恢复到C表（直接覆盖），删除B表数据
     * @return boolean：操作成功true，否则false
     */
    private boolean backPol()
    {
        //缴费次数还没搞定
        LCPolDB tLCPolDB = new LCPolDB();
//        tLCPolDB.setContNo(mLCContSchema.getContNo());
        /* modify by fuxin 2008-6-12
         * 原程序根据保单的险种做实收保费转出，现在根据实收对应的险种做转出。
         * 为了解决少儿险和其它险种一起销售的问题。
         */
        String  sql =" select a.* From LCPol a where a.contno ='"+mLCContSchema.getContNo()+"' "
                    +" and polno in(select distinct polno from ljapayperson where getNoticeNo='"+mLJSPayBSchema.getGetNoticeNo()+"' and a.contno= contno ) "
                    +" with ur "
                  ;
        LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql);

        if(tLCPolSet.size() == 0)
        {
            mErrors.addOneError("没有查询到保单" + mLCContSchema.getContNo()
                                + "的险种信息");
            return false;
        }

        for(int i = 1; i <= tLCPolSet.size(); i++)
        {
            LCPolSchema tLCPolSchema = tLCPolSet.get(i);
            mIsXBPol = isXBPol(tLCPolSchema);
            mHasXBPol = mHasXBPol || mIsXBPol;

            if(mIsXBPol)
            {
                if(!backXBPrem(tLCPolSchema)
                   || !backXBGet(tLCPolSchema)
                   || !backXBDuty(tLCPolSchema)
                   || !backXBPol(tLCPolSchema)
                   || !backXBInsureAcc(tLCPolSchema)
                   || !backOtherXBPolInfo()
                   || !changeXBPolState())
                {
                    return false;
                }
            }
            else
            {
                backDueFeePol(tLCPolSchema);
            }
        }
        
        //add by xp 处理相关保单同时做实收保费转出时的错误
        if(mLCRnewStateLogSchema!=null)
        {	
        String sqlcontstate = "delete from LCContState "
            + "where contno = '"
            + mLCRnewStateLogSchema.getNewContNo() + "' "
            + "   and stateType = '" + XBConst.TERMINATE + "' "
            + "   and state = '1' "
            + "   and StateReason = 'XB' and polno='000000' ";
        map.put(sqlcontstate, "DELETE");
        }
        return true;
    }

    /**
     * 若续保时险种续保终止，则需要将LCContState表记录删除
     * @return boolean
     */
    private boolean changeXBPolState()
    {
        String sql = "delete from LCContState "
                     + "where polNo = '"
                     + mLCRnewStateLogSchema.getPolNo() + "' "
                     + "   and stateType = '" + XBConst.TERMINATE + "' "
                     + "   and state = '1' "
                     + "   and StateReason = '11' ";
        map.put(sql, "DELETE");

        map.put(mLCRnewStateLogSchema, "DELETE");
        //qulq add 2007-8-25
        LBRnewStateLogSchema tLBRnewStateLogSchema = new LBRnewStateLogSchema();

        Reflections tReflections = new Reflections();

        tReflections.transFields(tLBRnewStateLogSchema,mLCRnewStateLogSchema);

        tLBRnewStateLogSchema.setOperator(this.mGlobalInput.Operator);

        tLBRnewStateLogSchema.setMakeDate(this.mCurrentDate);
        tLBRnewStateLogSchema.setModifyDate(this.mCurrentDate);
        tLBRnewStateLogSchema.setMakeTime(this.mCurrentTime);
        tLBRnewStateLogSchema.setModifyTime(this.mCurrentTime);
        map.put(tLBRnewStateLogSchema, "INSERT");

        return true;
    }

    /**
     * 回退在续保中并不需要直接处理的险种相关数据（仅仅从C表备份到B表，数据在业务上没有经过更改）
     * @return boolean：操作成功true，否则false
     */
    private boolean backOtherXBPolInfo()
    {
        String[] tables =
            {"LBBnf", "LBInsuredRelated", "LBPremToAcc",
            "LBGetToAcc", "LBInsureAccFee", "LBInsureAccClassFee"};
        for(int i = 0; i < tables.length; i++)
        {
            String sql = "delete from " + tables[i]
                         + " where polNo = '"
                         + mLCRnewStateLogSchema.getNewPolNo() + "' ";
            map.put(sql, "DELETE");
        }

        return true;
    }

    /**
     * backXBInsureAcc
     * 回退帐户信息
     * @param tLCPolSchema LCPolSchema：待回退帐户信息
     * @return boolean：操作成功true，否则false
     */
    private boolean backXBInsureAcc(LCPolSchema tLCPolSchema)
    {
        //从LBInsureAcc表得到原缴费项信息，回退帐户数据
        LBInsureAccDB tLBInsureAccDB = new LBInsureAccDB();
        tLBInsureAccDB.setPolNo(mLCRnewStateLogSchema.getNewPolNo());
        LBInsureAccSet tLBInsureAccSet = tLBInsureAccDB.query();

        //有的险种可能没有帐户
        if(tLBInsureAccSet.size() == 0)
        {
            return true;
        }
        for(int i = 1; i <= tLBInsureAccSet.size(); i++)
        {
            LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
            ref.transFields(tLCInsureAccSchema, tLBInsureAccSet.get(i));

            tLCInsureAccSchema.setContNo(mLCRnewStateLogSchema.getContNo());  //号码换成续保前号码
            tLCInsureAccSchema.setPolNo(mLCRnewStateLogSchema.getPolNo());
            tLCInsureAccSchema.setOperator(mGlobalInput.Operator);
            tLCInsureAccSchema.setModifyDate(this.mCurrentDate);
            tLCInsureAccSchema.setModifyTime(this.mCurrentTime);
            map.put(tLCInsureAccSchema, "DELETE&INSERT");
        }
        map.put(tLBInsureAccSet, "DELETE");

        //从LBInsureAccClass表得到原缴费项信息，回退帐户分类数据
        LBInsureAccClassDB tLBInsureAccClassDB = new LBInsureAccClassDB();
        tLBInsureAccClassDB.setPolNo(mLCRnewStateLogSchema.getNewPolNo());
        LBInsureAccClassSet tLBInsureAccClassSet = tLBInsureAccClassDB.query();
        if(tLBInsureAccClassSet.size() == 0)
        {
            mErrors.addOneError("没有查询到险种的缴费项信息"
                                + tLCPolSchema.getRiskCode());
            return false;
        }
        for(int i = 1; i <= tLBInsureAccClassSet.size(); i++)
        {
            LCInsureAccClassSchema tLCInsureAccClassSchema = new LCInsureAccClassSchema();
            ref.transFields(tLCInsureAccClassSchema, tLBInsureAccClassSet.get(i));

            tLCInsureAccClassSchema.setContNo(mLCRnewStateLogSchema.getPolNo());  //号码换成续保前号码
            tLCInsureAccClassSchema.setPolNo(mLCRnewStateLogSchema.getPolNo());
            tLCInsureAccClassSchema.setOperator(mGlobalInput.Operator);
            tLCInsureAccClassSchema.setModifyDate(this.mCurrentDate);
            tLCInsureAccClassSchema.setModifyTime(this.mCurrentTime);
            map.put(tLCInsureAccClassSchema, "DELETE&INSERT");
        }
        map.put(tLBInsureAccClassSet, "DELETE");

        //从LBInsureAccClass表得到原缴费项信息，回退帐户轨迹数据
        LBInsureAccTraceDB tLBInsureAccTraceDB = new LBInsureAccTraceDB();
        tLBInsureAccTraceDB.setPolNo(mLCRnewStateLogSchema.getNewPolNo());
        LBInsureAccTraceSet tLBInsureAccTraceSet = tLBInsureAccTraceDB.query();
        if(tLBInsureAccTraceSet.size() == 0)
        {
            mErrors.addOneError("没有查询到险种的缴费项信息"
                                + tLCPolSchema.getRiskCode());
            return false;
        }
        for(int i = 1; i <= tLBInsureAccTraceSet.size(); i++)
        {
            LCInsureAccTraceSchema tLCInsureAccTraceSchema = new LCInsureAccTraceSchema();
            ref.transFields(tLCInsureAccTraceSchema, tLBInsureAccTraceSet.get(i));

            tLCInsureAccTraceSchema.setContNo(mLCRnewStateLogSchema.getPolNo());  //号码换成续保前号码
            tLCInsureAccTraceSchema.setPolNo(mLCRnewStateLogSchema.getPolNo());
            tLCInsureAccTraceSchema.setOperator(mGlobalInput.Operator);
            tLCInsureAccTraceSchema.setModifyDate(this.mCurrentDate);
            tLCInsureAccTraceSchema.setModifyTime(this.mCurrentTime);
            map.put(tLCInsureAccTraceSchema, "DELETE&INSERT");
        }
        map.put(tLBInsureAccTraceSet, "DELETE");

        return true;
    }

    /**
     * backXBDuty
     *
     * @param tLCPolSchema LCPolSchema
     */
    private boolean backXBDuty(LCPolSchema tLCPolSchema)
    {
        //从LBDuty表得到原缴费项信息，回退责任项数据
        LBDutyDB tLBDutyDB = new LBDutyDB();
        tLBDutyDB.setPolNo(mLCRnewStateLogSchema.getNewPolNo());
        LBDutySet tLBDutySet = tLBDutyDB.query();
        if(tLBDutySet.size() == 0)
        {
            mErrors.addOneError("没有查询到险种的缴费项信息"
                                + tLCPolSchema.getRiskCode());
            return false;
        }
        for(int i = 1; i <= tLBDutySet.size(); i++)
        {
            LCDutySchema tLCDutySchema = new LCDutySchema();
            ref.transFields(tLCDutySchema, tLBDutySet.get(i));

            tLCDutySchema.setContNo(mLCRnewStateLogSchema.getContNo());  //号码换成续保前号码
            tLCDutySchema.setPolNo(mLCRnewStateLogSchema.getPolNo());
            tLCDutySchema.setOperator(mGlobalInput.Operator);
            tLCDutySchema.setModifyDate(this.mCurrentDate);
            tLCDutySchema.setModifyTime(this.mCurrentTime);
            map.put(tLCDutySchema, "DELETE&INSERT");
        }
        map.put(tLBDutySet, "DELETE");

        return true;
    }

    /**
     * backXBGet
     *回退责任项数据
     * @param tLCPolSchema LCPolSchema：待回退险种信息
     */
    private boolean backXBGet(LCPolSchema tLCPolSchema)
    {
        //从LBGet表得到原缴费项信息，回退责任项数据
        LBGetDB tLBGetDB = new LBGetDB();
        tLBGetDB.setPolNo(mLCRnewStateLogSchema.getNewPolNo());
        LBGetSet tLBGetSet = tLBGetDB.query();
        if(tLBGetSet.size() == 0)
        {
            mErrors.addOneError("没有查询到险种的缴费项信息"
                                + tLCPolSchema.getRiskCode());
            return false;
        }
        for(int i = 1; i <= tLBGetSet.size(); i++)
        {
            LCGetSchema tLCGetSchema = new LCGetSchema();
            ref.transFields(tLCGetSchema, tLBGetSet.get(i));

            tLCGetSchema.setContNo(mLCRnewStateLogSchema.getContNo());  //号码换成续保前号码
            tLCGetSchema.setPolNo(mLCRnewStateLogSchema.getPolNo());
            tLCGetSchema.setOperator(mGlobalInput.Operator);
            tLCGetSchema.setModifyDate(this.mCurrentDate);
            tLCGetSchema.setModifyTime(this.mCurrentTime);
            map.put(tLCGetSchema, "DELETE&INSERT");
        }
        map.put(tLBGetSet, "DELETE");

        return true;
    }

    /**
     * 回退责任信息
     * @param tLCPolSchema LCPolSchema
     * @return boolean
     */
    private boolean backXBPrem(LCPolSchema tLCPolSchema)
    {
        //从LBPrem表得到原缴费项信息，回退缴费项数据
        LBPremDB tLBPremDB = new LBPremDB();
        tLBPremDB.setPolNo(mLCRnewStateLogSchema.getNewPolNo());
        LBPremSet tLBPremSet = tLBPremDB.query();
        if(tLBPremSet.size() == 0)
        {
            mErrors.addOneError("没有查询到险种的缴费项信息"
                                + tLCPolSchema.getRiskCode());
            return false;
        }
        for(int i = 1; i <= tLBPremSet.size(); i++)
        {
            LCPremSchema tLCPremSchema = new LCPremSchema();
            ref.transFields(tLCPremSchema, tLBPremSet.get(i));

            tLCPremSchema.setContNo(mLCRnewStateLogSchema.getContNo());  //号码换成续保前号码
            tLCPremSchema.setPolNo(mLCRnewStateLogSchema.getPolNo());
            tLCPremSchema.setOperator(mGlobalInput.Operator);
            tLCPremSchema.setModifyDate(this.mCurrentDate);
            tLCPremSchema.setModifyTime(this.mCurrentTime);
            map.put(tLCPremSchema, "DELETE&INSERT");
        }
        map.put(tLBPremSet, "DELETE");

        return true;
    }

    /**
     * 回退续期险种
     * @param tLCPolSchema LCPolSchema：待回退险中
     * @return boolean
     */
    private boolean backDueFeePol(LCPolSchema tLCPolSchema)
    {
        //回退缴费项
        String sql = "update LCPrem a "
                     + "set paytimes = paytimes -1 , "
                     + "   sumPrem = sumPrem - prem, "
                     + "   Operator = '" + mGlobalInput.Operator + "', "
                     + "   ModifyDate = '" + mCurrentDate + "', "
                     + "   Modifytime = '" + mCurrentTime + "', "
                     + "   (prem, payToDate) = "
                     + "      (select sum(sumDuePayMoney / (1 - a.FreeRate)), "
                     + "         min(lastPayToDate) "
                     + "      from LJAPayPerson "
                     + "      where polNo = a.polNo and dutyCode = a.dutyCode "
                     + "         and payPlanCode = a.payPlanCode "
                     + " and  paytype = 'ZC' "  // 在统计保费时,只取正常的 2007-9-9 by fuxin
                     + "         and payNo = '"
                     + mLJAPaySchema.getPayNo() + "') "
                     + "where polNo = '" + tLCPolSchema.getPolNo() + "' ";
        map.put(sql, "UPDATE");

        String[] tables = {" LCPol", " LCDuty"};
        for(int i = 0; i < tables.length; i++)
        {
            sql = "update " + tables[i] + " a "
                  + "set (prem, sumPrem, payToDate) = "
                  + "   (select sum(prem), sum(sumPrem), min(payToDate) "
                  + "   from LCPrem "
                  + "   where polNo = a.polNo), "
                  + "   operator = '" + this.mGlobalInput.Operator + "', "
                  + "   modifyDate = '" + this.mCurrentDate + "', "
                  + "   modifyTime = '" + this.mCurrentTime + "' "
                  + "where polNo = '" + tLCPolSchema.getPolNo() + "' ";
            map.put(sql, "UPDATE");
        }

        return true;
    }

    /**
     * 回退续保险种
     * @param tLCPolSchema LCPolSchema：待回退险中
     * @return boolean：成功true，否则false
     */
    private boolean backXBPol(LCPolSchema tLCPolSchema)
    {
        //从LBPrem表得到原缴费项信息，回退缴费项数据
        LBPolDB tLBPolDB = new LBPolDB();
        tLBPolDB.setPolNo(mLCRnewStateLogSchema.getNewPolNo());
        LBPolSet tLBPolSet = tLBPolDB.query();
        if(tLBPolSet.size() == 0)
        {
            mErrors.addOneError("没有查询到险种的缴费项信息"
                                + tLCPolSchema.getRiskCode());
            return false;
        }
        for(int i = 1; i <= tLBPolSet.size(); i++)
        {
            LCPolSchema schema = new LCPolSchema();
            ref.transFields(schema, tLBPolSet.get(i));

            schema.setContNo(mLCRnewStateLogSchema.getContNo());  //号码换成续保前号码
            schema.setPolNo(mLCRnewStateLogSchema.getPolNo());
            schema.setOperator(mGlobalInput.Operator);
            schema.setModifyDate(this.mCurrentDate);
            schema.setModifyTime(this.mCurrentTime);
            schema.setStateFlag(BQ.STATE_FLAG_SIGN);
            map.put(schema, "DELETE&INSERT");
        }
        map.put(tLBPolSet, "DELETE");

        return true;
    }

    /**
     * 校验险种是否是续保险种
     * @param tLCPolSchema LCPolSchema
     * @return boolean：成功true，否则false
     */
    private boolean isXBPol(LCPolSchema tLCPolSchema)
    {
        String sql = "select * from LCRnewStateLog "
                     + "where polNo = '" + tLCPolSchema.getPolNo() + "' "
                     + "   and renewCount = "
                     + tLCPolSchema.getRenewCount() + " ";
        LCRnewStateLogSet tLCRnewStateLogSet
            = new LCRnewStateLogDB().executeQuery(sql);
        if(tLCRnewStateLogSet.size() > 0)
        {
            this.mLCRnewStateLogSchema = tLCRnewStateLogSet.get(1);
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * 财务回退
     */
    private boolean backFinace()
    {
        //总实收表回退
        LJAPaySchema tLJAPaySchema = new LJAPaySchema();
        ref.transFields(tLJAPaySchema, mLJAPaySchema);
        tLJAPaySchema.setSumActuPayMoney( -mLJAPaySchema.getSumActuPayMoney());
        tLJAPaySchema.setPayDate(mCurrentDate);
        tLJAPaySchema.setEnterAccDate(mCurrentDate);
        tLJAPaySchema.setConfDate(mCurrentDate);
        tLJAPaySchema.setPayNo(mPayNo);
        tLJAPaySchema.setMakeDate(mCurrentDate);
        tLJAPaySchema.setMakeTime(mCurrentTime);
        tLJAPaySchema.setModifyDate(mCurrentDate);
        tLJAPaySchema.setModifyTime(mCurrentTime);
        tLJAPaySchema.setOperator(mGlobalInput.Operator);
        tLJAPaySchema.setDueFeeType("1");//modify by fuxin 2008-7-3 新财务接口提数要求。
        map.put(tLJAPaySchema, "INSERT");

        //个人财务实收表回退
        LJAPayPersonDB tLJAPayPersonDB = new LJAPayPersonDB();
        tLJAPayPersonDB.setPayNo(mLJAPaySchema.getPayNo());
        LJAPayPersonSet tLJAPayPersonSet = tLJAPayPersonDB.query();
        if(tLJAPayPersonSet.size() == 0)
        {
            mErrors.addOneError("没有查询到被保人实收数据");
            return false;
        }

        LJAPayPersonSet tLJAPayPersonSetDealt = new LJAPayPersonSet();
        for(int i = 1; i <= tLJAPayPersonSet.size(); i++)
        {
            LJAPayPersonSchema tLJAPayPersonSchema = new LJAPayPersonSchema();
            ref.transFields(tLJAPayPersonSchema, tLJAPayPersonSet.get(i));

            tLJAPayPersonSchema.setSumDuePayMoney(
                -tLJAPayPersonSchema.getSumDuePayMoney());
            tLJAPayPersonSchema.setSumActuPayMoney(
                -tLJAPayPersonSchema.getSumActuPayMoney());
            
          //add fr 营改增关于2016-05-01以前反冲的数据不做价税分离
          	if(fDate.getDate(tLJAPayPersonSchema.getConfDate()).before(fDate.getDate("2016-05-01"))){
            	if( null == tLJAPayPersonSchema.getTaxRate() || "".equals(tLJAPayPersonSchema.getTaxRate())){
            		tLJAPayPersonSchema.setMoneyNoTax(""+tLJAPayPersonSchema.getSumActuPayMoney());
            		tLJAPayPersonSchema.setMoneyTax("0");
            		tLJAPayPersonSchema.setBusiType("01");
                    tLJAPayPersonSchema.setTaxRate("0"); 
            	}
            	else{
            		tLJAPayPersonSchema.setMoneyNoTax(null);
                    tLJAPayPersonSchema.setMoneyTax(null);
                    tLJAPayPersonSchema.setBusiType(null);
                    tLJAPayPersonSchema.setTaxRate(null); 
            	}
            	
            }else{
                tLJAPayPersonSchema.setMoneyNoTax(null);
                tLJAPayPersonSchema.setMoneyTax(null);
                tLJAPayPersonSchema.setBusiType(null);
                tLJAPayPersonSchema.setTaxRate(null);  
            }
            
  
            tLJAPayPersonSchema.setPayDate(this.mCurrentDate);
            tLJAPayPersonSchema.setEnterAccDate(this.mCurrentDate);
            tLJAPayPersonSchema.setPayNo(mPayNo);
            tLJAPayPersonSchema.setConfDate(mCurrentDate);
            tLJAPayPersonSchema.setEnterAccDate(mCurrentDate);
            tLJAPayPersonSchema.setMakeDate(this.mCurrentDate);
            tLJAPayPersonSchema.setMakeTime(this.mCurrentTime);
            tLJAPayPersonSchema.setModifyDate(this.mCurrentDate);
            tLJAPayPersonSchema.setModifyTime(this.mCurrentTime);
            tLJAPayPersonSchema.setOperator(mGlobalInput.Operator);

            tLJAPayPersonSetDealt.add(tLJAPayPersonSchema);
        }
        map.put(tLJAPayPersonSetDealt, "INSERT");

        return true;
    }

    /**
     * 得到待转出的实收信息
     * @return boolean：操作成功true，否则false
     */
    private boolean queryContInfo()
    {
        LJAPayDB tLJAPayDB = new LJAPayDB();
        tLJAPayDB.setGetNoticeNo(mLJSPayBSchema.getGetNoticeNo());
        LJAPaySet tLJAPaySet = tLJAPayDB.query();
        if(tLJAPaySet.size() == 0)
        {
            mErrors.addOneError("没有查询都实收记录");
            return false;
        }
        mLJAPaySchema = tLJAPaySet.get(1);

        LCContDB tLCContDB = new LCContDB();
        tLCContDB.setContNo(mLJSPayBSchema.getOtherNo());
        tLCContDB.getInfo();
        mLCContSchema = tLCContDB.getSchema();

        return true;
    }

    /**
     * 校验是否可进行实收保费转出操作，校验细节间数据流图“校验部分”。
     * @return boolean，校验通过true，否则false
     */
    private boolean checkData()
    {
        if(!queryLJSPayBInfo())
        {
            return false;
        }
        if(!queryContInfo())
        {
            return false;
        }

        if(!canBack())
        {
            return false;
        }
        if(!dealUrgeLog(SysConst.INSERT))
        {
           return false;
        }

        if(!checkOrder())
        {
            return false;
        }
        if(!checkDueFee())
        {
            return false;
        }
        if(!checkClaim())
        {
            return false;
        }
        if(!checkBQ())
        {
            return false;
        }
        if(!checkPolState())
        {
            return false;
        }

        return true;
    }

    /**
     * 若其他任务正在做实收保费转出，则不能再做实收保费转出
     * @return boolean
     */
    private boolean canBack()
    {
        String sql = "  select 1 from LCUrgeVerifyLog "
                     + "where serialNo = '"
                     + mLJSPayBSchema.getSerialNo() + "' "
                     + "   and OperateType = '3' ";
        String temp = new ExeSQL().getOneValue(sql);
        if(temp.equals("") || temp.equals("null"))
        {
            //可以进行实收保费转出
            return true;
        }
        mDoBackOtherTask = true;

        mErrors.addOneError("其他任务正在进行转出操作");
        return false;
    }

    /**
     * 加入到催收核销日志表数据
     * @param pmDealState
     * @param pmReason
     * @param pmOpreat : INSERT,UPDATE,DELETE
     * @return
     */
    private boolean dealUrgeLog(String dealType)
    {
        LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new
                                               LCUrgeVerifyLogDB();
        tLCUrgeVerifyLogDB.setSerialNo(mLJSPayBSchema.getSerialNo());
        tLCUrgeVerifyLogDB.setRiskFlag("2");
        tLCUrgeVerifyLogDB.setOperateType("3"); //1：续期催收操作,2：续期核销操作,3：实收保费转出
        tLCUrgeVerifyLogDB.setOperateFlag("1"); //1：个案操作,2：批次操作
        tLCUrgeVerifyLogDB.setOperator(mGlobalInput.Operator);
        tLCUrgeVerifyLogDB.setDealState("1"); //1 : 正在处理中,2 : 处理错误,3 : 处理已完成

        tLCUrgeVerifyLogDB.setContNo(mLJSPayBSchema.getOtherNo());
        tLCUrgeVerifyLogDB.setMakeDate(mCurrentDate);
        tLCUrgeVerifyLogDB.setMakeTime(mCurrentTime);
        tLCUrgeVerifyLogDB.setModifyDate(mCurrentDate);
        tLCUrgeVerifyLogDB.setModifyTime(mCurrentTime);

        if(dealType.equals(SysConst.INSERT))
        {
            if(!tLCUrgeVerifyLogDB.insert())
            {
                mErrors.addOneError("生成处理记录时出错");
                return false;
            }
        }
        else if(dealType.equals(SysConst.DELETE))
        {
            if(!tLCUrgeVerifyLogDB.delete())
            {
                mErrors.addOneError("删除处理记录时出错");
                return false;
            }
        }

        return true;
    }


    /**
     * 校验保单状态
     * @return boolean
     */
    private boolean checkPolState()
    {
        String sql = " select 1 from LCContState a "
                     + " where contNo = '" + mLJSPayBSchema.getOtherNo() + "' "
                     + "   and startDate < '" + mCurrentDate + "' "
                     + "   and ( endDate is null or endDate > '"
                     + mCurrentDate + "') "
                     + " and state = '1' "
                     +" and  exists (select 1 from ljapayperson where a.polno=polno and payno = '"
                     +  mLJAPaySchema.getPayNo() + "')"
                     ;

        String result = new ExeSQL().getOneValue(sql);
        if(!result.equals(""))
        {
            mErrors.addOneError("当前保单或其险种处于失效或终止状态，不能转出。");
            return false;
        }
        return true;
    }

    /**
     * 校验保全
     * @return boolean
     */
    private boolean checkBQ()
    {
        //是否有未处理完毕的保全业务
        String sql = " select b.edorNo from LPEdorApp a, LPEdorItem b "
                     + "where a.edorAcceptNo = b.edorAcceptNo "
                     + "   and b.contNo = '" + mLJSPayBSchema.getOtherNo() + "' "
                     + "   and a.edorState != '" + BQ.EDORSTATE_CONFIRM + "' ";
        System.out.println(sql);
        SSRS tSSRS = new ExeSQL().execSQL(sql);
        if(tSSRS.getMaxRow() != 0)
        {
            String edorNos = "";
            for(int i = 1; i <= tSSRS.getMaxRow(); i++)
            {
                edorNos += tSSRS.GetText(i, 1) + ", ";
            }
            mErrors.addOneError("保单有未处理完毕的保全业务，不能转出，受理号为："
                                + edorNos);
            return false;
        }


        //续期续保核销后发生过收退费的项目
        sql = " select sum(getmoney) from LJAGetEndorse "
              + "where contNo = '" + mLJSPayBSchema.getOtherNo() + "' "
              + "   and makeDate > '" + mLJAPaySchema.getMakeDate() + "' " 
              + "   and feeoperationtype not in (select Code from LDCode where CodeType = 'edortypechangeprem') ";
        String result = new ExeSQL().getOneValue(sql);
        if(!(result.equals("0.00")|result.equals("")))
        {
            mErrors.addOneError("保单有过对保费有影响的保全业务，不能转出。");
            return false;
        }

        //无理赔优惠特殊校验 direct by lc 2017-1-22
        //医诊专家无理赔奖励特殊校验
        ExeSQL tExeSQL = new ExeSQL(); 
        String Risksql = "select riskcode from ljapayperson where contno = '" + mLJSPayBSchema.getOtherNo() + "' and getNoticeNo = '" + mLJSPayBSchema.getGetNoticeNo() + "' and riskcode in (select code from ldcode1 where codetype='WLPYH')"; 
        String resultRisk = tExeSQL.getOneValue(Risksql);
        if(resultRisk!=null&&(!"".equals(resultRisk))&&(!"null".equals(resultRisk))){  
	        String WLPsql= "select code,code1 from ldcode1 where codetype='WLPYH' and code = '" + resultRisk + "'";
	        SSRS tLDCode1SSRS = null;
	        tLDCode1SSRS = tExeSQL.execSQL(WLPsql);
	        if(tLDCode1SSRS.getMaxRow()>0){
	        	String sqlprem = "select sum(prem) from lcpol where contno = '" + mLJSPayBSchema.getOtherNo() + "' and riskcode='" + tLDCode1SSRS.GetText(1, 1) + "'";
	        	double a = Double.parseDouble(tExeSQL.getOneValue(sqlprem));
	        	String sqllja = "select sum(sumDuePayMoney) from ljspaypersonb where contno = '" + mLJSPayBSchema.getOtherNo() + "' and paytype = 'ZC' and getnoticeno='" + mLJSPayBSchema.getGetNoticeNo() +"' and riskcode='" + tLDCode1SSRS.GetText(1, 1) + "'";
	        	double b = Double.parseDouble(tExeSQL.getOneValue(sqllja));
	        	double WLPRate = Double.parseDouble(tLDCode1SSRS.GetText(1, 2));
	        	double lja = b / WLPRate;     	
	        	BigDecimal c= new BigDecimal(lja);
	        	c = c.setScale(2,BigDecimal.ROUND_HALF_UP);
	        	double d = c.doubleValue() + 0.01;
	        	double e = c.doubleValue() - 0.01;
	        	double f = c.doubleValue();
	        	if(a != f && a != d && a != e){
	        		if(a != b){
	        			mErrors.addOneError("本次转出的保费与保单实交保费金额不等，不能转出。");
	     	            return false;
	        		}        		
	        	}
	        } 
	        else {   
		        //校验个险种保费与实交是否相等
		        sql = "select 1 from LCPol a "
		              + "where prem != "
		              + "      nvl((select sum(sumDuePayMoney) from LJAPayPerson "
		              + "      where polNo = a.polNo "
		              + "         and payType = 'ZC' "
		              + "         and getNoticeNo = '"
		              + mLJSPayBSchema.getGetNoticeNo() + "'), -1) "
		              + "   and polNo in "
		              + "      (select polNo from LJAPayPerson "
		              + "      where contNo = '" + mLJSPayBSchema.getOtherNo() + "' and getNoticeNo='"+mLJSPayBSchema.getGetNoticeNo()+"')";
		        result = new ExeSQL().getOneValue(sql);
		        if(!result.equals(""))
		        {
		            mErrors.addOneError("本次转出的保费与保单实交保费金额不等，不能转出。");
		            return false;
		        }
	        }
        } else {
        	//校验个险种保费与实交是否相等
	        sql = "select 1 from LCPol a "
	              + "where prem != "
	              + "      nvl((select sum(sumDuePayMoney) from LJAPayPerson "
	              + "      where polNo = a.polNo "
	              + "         and payType = 'ZC' "
	              + "         and getNoticeNo = '"
	              + mLJSPayBSchema.getGetNoticeNo() + "'), -1) "
	              + "   and polNo in "
	              + "      (select polNo from LJAPayPerson "
	              + "      where contNo = '" + mLJSPayBSchema.getOtherNo() + "' and getNoticeNo='"+mLJSPayBSchema.getGetNoticeNo()+"')";
	        result = new ExeSQL().getOneValue(sql);
	        if(!result.equals(""))
	        {
	            mErrors.addOneError("本次转出的保费与保单实交保费金额不等，不能转出。");
	            return false;
	        }
        }
//        //校验整单保费与实交是否相等
//        sql = "select 1 from LCCont a "
//              + "where prem != "
//              + "      nvl((select sum(sumDuePayMoney) from LJAPayPerson "
//              + "      where contNo = a.contNo "
//              + "         and payType = 'ZC' "
//              + "         and getNoticeNo = '"
//              + mLJSPayBSchema.getGetNoticeNo() + "'), -1) "
//              + "   and contNo = '" + mLJSPayBSchema.getOtherNo() + "' ";
//        result = new ExeSQL().getOneValue(sql);
//        if(!result.equals(""))
//        {
//            mErrors.addOneError("保单有过对保费有影响的保全业务，不能转出。");
//            return false;
//        }

        return true;
    }

    /**
     * by【OoO?】杨天政 20120203
     * 1）个人保单存在正在操作的理赔和没有结案的理赔时，无法进行实收保费转出。
     * 2）检查当前保单是否有“出险时间”在申请实收保费转出的相应保障年度内的理赔案件，
     *   除撤件状态外，如果有则阻断并提示，不允许进行实收保费转出。
     * 3）该校验只针对保单。例如张某有两张保单，A保单和B保单。A保单出险并产生理赔，不影响B保单进行实收保费转出。
     * @return boolean: 发生true
     */
    private boolean checkClaim()
    {
    	//先查看是否有正在进行的理赔
    	String currentclaimsql = "select 1 from llcase a where " +
		       "exists (select 1 from lcinsured where contno ='" + mLJSPayBSchema.getOtherNo() 
             + "' and insuredno = a.customerno) and endcasedate is null and rgtstate <> '14' ";
    	System.out.println(currentclaimsql);
        String rgtNo = new ExeSQL().getOneValue(currentclaimsql);
        if (rgtNo != null && !rgtNo.equals("")) 
           {
            mErrors.addOneError("保单"+mLJSPayBSchema.getOtherNo()+"有理陪，不能给付确认。");
            return false;
           }
        //然后查看是否有某个保障年度操作的理赔
    	System.out.println("续收保费号"+mLJSPayBSchema.getGetNoticeNo());
        String sqlClaim = "select  1 from " +
    			"llcase a," +
    			"llcaserela b," +
    			"llsubreport c," +
    			"llclaimpolicy d " +
    			"where a.caseno=b.caseno " +
    			"and b.subrptno=c.subrptno " +
    			"and a.caseno=d.caseno " +
    			"and b.caserelano=d.caserelano " +
    			"and a.rgtstate in ('09','11','12') " +
    			"and a.endcasedate is not null " +
    			"and c.accdate <(select max(curpaytodate) from ljapayperson where getnoticeno='"+mLJSPayBSchema.getGetNoticeNo()+"')" +
    			"and c.accdate >(select min(lastpaytodate) from ljapayperson where getnoticeno='"+mLJSPayBSchema.getGetNoticeNo()+"')" +
    			"and d.contno='"+mLJSPayBSchema.getOtherNo()+"' with ur";	
    	System.out.println(sqlClaim);
    	String result1 = new ExeSQL().getOneValue(sqlClaim);
    	if(!result1.equals(""))
        {
            mErrors.addOneError("该保障年度内发生过结案的理陪，不能转出。");
            return false;
        }
        return true;
    }
    
    /**
     * 只能按实收保费核销时间从后往前的顺序进行实收保费转出
     * @return boolean
     */
    private boolean checkOrder()
    {
        ExeSQL tExeSQL = new ExeSQL();
        String sql = " select 1 "
                     + "from LJSPayB "
                     + "where otherno = '"
                     + mLJSPayBSchema.getOtherNo()
                     + "' and getnoticeno > '"
                     + mLJSPayBSchema.getGetNoticeNo() + "' "
                     + "  and dealstate = '"
                     + FeeConst.DEALSTATE_URGESUCCEED + "' ";
        String result = tExeSQL.getOneValue(sql);
        if(!result.equals(""))
        {
            mErrors.addOneError("只能按实收保费核销时间从后往前的顺序进行实收保费转出");
            return false;
        }
        return true;
    }

    /**
     * 校验报单是否正在做续期续保
     * @return boolean
     */
    private boolean checkDueFee()
    {
        if(mLJSPayBSchema.getDealState().equals(FeeConst.DEALSTATE_BACK))
        {
            mErrors.addOneError("正在进行实收转出，不能再次进行");
            return false;
        }

        String sql = " select getNoticeNo from ljspay "
              + "where GetNoticeNo = '" + mLJSPayBSchema.getGetNoticeNo() + "' "
              + "  and othernotype = '2'";
        String result = new ExeSQL().getOneValue(sql);
        if(!result.equals(""))
        {
            mErrors.addOneError("保单正在做续期续保业务，不能转出，应收记录号" + result);
            return false;
        }
        sql = "  select 1 from LCRnewStateLog "
              + "where contNo = '" + mLJSPayBSchema.getOtherNo() + "' "
              + "   and state in('" +  XBConst.RNEWSTATE_APP + "', '"
              + XBConst.RNEWSTATE_UNUW + "', '" + XBConst.RNEWSTATE_UNHASTEN
              + "')";
        result = new ExeSQL().getOneValue(sql);
        if(!result.equals(""))
        {
            mErrors.addOneError("保单正在做续期续保业务，不能转出。");
            return false;
        }

        return true;

    }

    /**
     * 查询应收备份信息
     * @return boolean
     */
    private boolean queryLJSPayBInfo()
    {
        LJSPayBDB tLJSPayBDB = new LJSPayBDB();
        tLJSPayBDB.setGetNoticeNo(mLJSPayBSchema.getGetNoticeNo());
        tLJSPayBDB.setDealState(FeeConst.DEALSTATE_URGESUCCEED);
        LJSPayBSet set = tLJSPayBDB.query();
        if(set.size() == 0)
        {
            mErrors.addOneError("没有查询到以下通知书号的应收备份信息"
                                + mLJSPayBSchema.getGetNoticeNo());
            return false;
        }
        mLJSPayBSchema = set.get(1);

        return true;
    }


    /**
     * 从输入数据中得到所有对象
     * @param inputData VData: submitData中传入的VData
     * @return boolean: 操作成功true，否则false
     */
    private boolean getInputData(VData inputData)
    {
        mGlobalInput = (GlobalInput) inputData
                       .getObjectByObjectName("GlobalInput", 0);
        mLJSPayBSchema = (LJSPayBSchema) inputData
                       .getObjectByObjectName("LJSPayBSchema", 0);
        if(mGlobalInput == null || mLJSPayBSchema == null
           || mLJSPayBSchema.getGetNoticeNo() == null
            || mLJSPayBSchema.getGetNoticeNo().equals(""))
        {
            mErrors.addOneError("传入的数据不完整。");
            return false;
        }
        String tRiskCode = new ExeSQL().getOneValue("select riskcode From ljspaypersonb where getnoticeno ='"+mLJSPayBSchema.getGetNoticeNo()+"'");
        if( tRiskCode.equals("320106") || tRiskCode.equals("120706"))
        {
        	System.out.println("少儿险实收保费转出，转出后保单终止。--" + tRiskCode);
        	mIsChildRisk = true;
        }
        else
        {
        	System.out.println("普通险实收保费转出。");
        	mIsChildRisk = false;
        }
        
        return true;
    }
    
    //设置本次实收保费转出的险种为终止状态，如果所有的险种都终止，则保单终止。
    private boolean callSingleTerminate(LJSPayBSchema aLJSPayBSchema)
    {
    	String sql = "select * from lcpol a where exists (select 1 from ljspaypersonb where grpcontno = '00000000000000000000' "
    		       + "and getnoticeno = '" + aLJSPayBSchema.getGetNoticeNo() + "' and polno = a.polno) ";
    	
    	LCPolDB tLCPolDB = new LCPolDB();
    	LCPolSet tLCPolSet = tLCPolDB.executeQuery(sql);
    	
    	if(tLCPolSet!=null && tLCPolSet.size()> 0)
    	{
    		for(int i=1; i<=tLCPolSet.size(); i++)
    		{
                LCContStateDB tLCContStateDB = new LCContStateDB();
                LCContStateSet tLCContStateSet = new LCContStateSet();
                LCContStateSchema tLCContStateSchema = new LCContStateSchema();
                LCPolSchema tLCPolSchema = new LCPolSchema();
                tLCPolSchema.setSchema(tLCPolSet.get(i));
            	 
            	tLCContStateDB.setContNo(tLCPolSet.get(i).getContNo());
                tLCContStateDB.setPolNo(tLCPolSet.get(i).getPolNo());
                tLCContStateDB.setStateType("Available");
                tLCContStateSet = tLCContStateDB.query();
                if (!tLCContStateDB.mErrors.needDealError()) 
                {
                    if (tLCContStateSet != null && tLCContStateSet.size() > 0) 
                    {
                    	sql = "update LCContState Set Enddate = '" + mCurrentDate + "', State = '0', Operator = '" + mGlobalInput.Operator 
                        	+ "',ModifyDate = '" + mCurrentDate + "',ModifyTime = '" + mCurrentTime 
                        	+ "' where ContNo = '" +tLCPolSchema.getContNo()+ "' and PolNo = '" + tLCPolSchema.getPolNo() + "' ";
                        map.put(sql, SysConst.UPDATE);
                    }
                    tLCContStateSchema.setContNo(tLCPolSchema.getContNo());
                    tLCContStateSchema.setPolNo(tLCPolSchema.getPolNo());

                    tLCContStateSchema.setGrpContNo("000000");
                    String tInsuredNo = "";
                    if (tLCPolSchema.getInsuredNo().trim().equals("")) 
                    {
                    	tInsuredNo = "000000";
                    } 
                    else 
                    {
                    	tInsuredNo = tLCPolSchema.getInsuredNo();
                    }
                    tLCContStateSchema.setInsuredNo(tInsuredNo);
                    tLCContStateSchema.setStateType("Terminate");
                    tLCContStateSchema.setOtherNo(mLJSPayBSchema.getGetNoticeNo());
                    tLCContStateSchema.setOtherNoType(mLJSPayBSchema.getOtherNoType());
                    tLCContStateSchema.setState("1");
                    tLCContStateSchema.setStateReason("02");
                    tLCContStateSchema.setStartDate(mCurrentDate);
                    tLCContStateSchema.setRemark("");
                    tLCContStateSchema.setOperator(mGlobalInput.Operator);
                    tLCContStateSchema.setMakeDate(mCurrentDate);
                    tLCContStateSchema.setMakeTime(mCurrentTime);
                    tLCContStateSchema.setModifyDate(mCurrentDate);
                    tLCContStateSchema.setModifyTime(mCurrentTime);
                    map.put(tLCContStateSchema, "DELETE&INSERT");
                    String updatepol = "update LCPol Set StateFlag = '3',PolState = '" + BQ.POLSTATE_XBEND 
                        + "', Operator = '" + mGlobalInput.Operator + "',ModifyDate = '" + mCurrentDate + "',ModifyTime = '" + mCurrentTime 
                        + "' where PolNo = '" + tLCPolSchema.getPolNo() + "' ";
                    map.put(updatepol, SysConst.UPDATE);
                }
    		}
    	}
    	
    	//除本次转出涉及的几个险种外，本保单下其它险种状态
    	sql = "select sum(case when a.stateflag = '1' then 1 else 0 end),sum(case when a.stateflag = '2' then 1 else 0 end), "
			+ "sum(case when a.stateflag = '3' then 1 else 0 end) from lcpol a where contno = '" + aLJSPayBSchema.getOtherNo()
			+ "' and not exists (select 1 from ljspaypersonb where polno = a.polno and getnoticeno = '" + aLJSPayBSchema.getGetNoticeNo() 
			+ "') with ur ";
    	SSRS tSSRS = new SSRS();
    	tSSRS = mExeSQL.execSQL(sql);
    	String tCount1 = tSSRS.GetText(1, 1); //有效
    	String tCount2 = tSSRS.GetText(1, 2); //失效
    	String tCount3 = tSSRS.GetText(1, 3); //终止
    	
//    	判断其它险种状态
		if(!tCount1.equals("0"))//该保单下存在有效险种
    	{
			//不处理保单状态
			return true;
    	}
		else if(!tCount2.equals("0")) //该保单下不存在有效险种但存在失效险种
		{
//			设置保单状态为失效
    		if (!setPolicyAbate(mLCContSchema,"Available","2")) 
    		{
    			System.out.println("保单" + mLCContSchema.getContNo() +"设置失效失败");
    			return false;
    		} 
    		else 
    		{
    			sendTerminateNotice(mLCContSchema, mGlobalInput.ManageCom, "42");
    		}
			
		}	
		else //该保单下不存在有效和失效险种但存在终止险种 或者 该保单下只有本次催收的几个险种
		{
//			设置保单状态为终止
    		if (!setPolicyAbate(mLCContSchema,"Terminate","3")) 
    		{
    			System.out.println("保单" + mLCContSchema.getContNo() +"设置终止失败");
    			return false;
    		} 
    		else 
    		{
    			sendTerminateNotice(mLCContSchema, mGlobalInput.ManageCom, "21");
    		}
		}
		
    	return true;
    }
    
    /*设置个险保单终止*/
    private boolean setPolicyAbate(LCContSchema aLCContSchema,String aStateType,String aStateFlag) 
    {
        //新状态起始日期为旧状态结束日期次日
        LCContStateDB tLCContStateDB = new LCContStateDB();
        
        LCContStateSet tLCContStateSet = new LCContStateSet();
        tLCContStateDB.setContNo(aLCContSchema.getContNo());
        tLCContStateDB.setPolNo("000000");
        tLCContStateDB.setStateType("Available");
        
        String sql = null;
        tLCContStateSet = tLCContStateDB.query();
        if (!tLCContStateDB.mErrors.needDealError()) 
        {
            if (tLCContStateSet != null && tLCContStateSet.size() > 0) 
            {
            	sql = "update LCContState Set Enddate = '" + mCurrentDate + "', State = '0', Operator = '" + mGlobalInput.Operator 
            		+ "',ModifyDate = '" + mCurrentDate + "',ModifyTime = '" + mCurrentTime 
            		+ "' where ContNo = '" +aLCContSchema.getContNo()+ "' and PolNo = '000000' ";
            	map.put(sql, SysConst.UPDATE);
            }
        } 
        else 
        {
            System.out.println("保单状态查询失败");
            return false;
        }
            
        LCContStateSchema tLCContStateSchema = new LCContStateSchema();
        tLCContStateSchema.setGrpContNo("000000");
        tLCContStateSchema.setContNo(aLCContSchema.getContNo());
        tLCContStateSchema.setInsuredNo(aLCContSchema.getInsuredNo());
        tLCContStateSchema.setPolNo("000000");
        tLCContStateSchema.setStateType(aStateType);
        tLCContStateSchema.setStateReason("02");
        tLCContStateSchema.setOtherNo(mLJSPayBSchema.getGetNoticeNo());
        tLCContStateSchema.setOtherNoType(mLJSPayBSchema.getOtherNoType());
        tLCContStateSchema.setState("1");
        tLCContStateSchema.setOperator(mGlobalInput.Operator);
        tLCContStateSchema.setStartDate(mCurrentDate);
        tLCContStateSchema.setMakeDate(mCurrentDate);
        tLCContStateSchema.setMakeTime(mCurrentTime);
        tLCContStateSchema.setModifyDate(mCurrentDate);
        tLCContStateSchema.setModifyTime(mCurrentTime);
        map.put(tLCContStateSchema, SysConst.DELETE_AND_INSERT);
        
        String updatecont = "update LCCont Set StateFlag = '"+aStateFlag+"',Operator = '" +mGlobalInput.Operator+ "', ModifyDate = '" + mCurrentDate
            + "',ModifyTime = '" + mCurrentTime + "' where Contno = '" + aLCContSchema.getContNo() + "'";
        map.put(updatecont, SysConst.INSERT);

        return true;
    }
    
    /*发送保单终止通知书*/
    private boolean sendTerminateNotice(LCContSchema aLCContSchema, String aCom,String aCode) 
    {
        String tLimit = PubFun.getNoLimit(aCom);
        String serNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        tLOPRTManagerSchema.setPrtSeq(serNo);
        tLOPRTManagerSchema.setOtherNo(aLCContSchema.getContNo());
        tLOPRTManagerSchema.setOtherNoType(PrintManagerBL.ONT_CONT); //个人保单号
        tLOPRTManagerSchema.setCode(aCode); //21终止 42失效
        tLOPRTManagerSchema.setManageCom(aLCContSchema.getManageCom());
        tLOPRTManagerSchema.setAgentCode(aLCContSchema.getAgentCode());
        tLOPRTManagerSchema.setReqCom(aCom);
        tLOPRTManagerSchema.setReqOperator(mGlobalInput.Operator);
        tLOPRTManagerSchema.setPrtType("0"); //前台打印
        tLOPRTManagerSchema.setStateFlag("0"); //提交打印
        tLOPRTManagerSchema.setStandbyFlag1(aLCContSchema.getCInValiDate());  //tLCContSchema.getCInValiDate()=保单保障满期日+1天
        tLOPRTManagerSchema.setStandbyFlag2(aLCContSchema.getCInValiDate());
        tLOPRTManagerSchema.setStandbyFlag3(mLJSPayBSchema.getGetNoticeNo());
        tLOPRTManagerSchema.setMakeDate(mCurrentDate);
        tLOPRTManagerSchema.setMakeTime(mCurrentTime);
        map.put(tLOPRTManagerSchema, SysConst.DELETE_AND_INSERT);
        return true;
    }
    	

    public static void main(String[] args)
    {
        GlobalInput g = new GlobalInput();
        g.Operator = "endor0";
        g.ComCode = "86";

        LJSPayBSchema lj = new LJSPayBSchema();
        lj.setGetNoticeNo("31001883126");

        VData d = new VData();
        d.add(g);
        d.add(lj);

        IndiDueFeeBackBL bl = new IndiDueFeeBackBL();
        if(!bl.submitData(d, ""))
        {
            System.out.println(bl.mErrors.getErrContent());
        }
        else
        {
            System.out.println("OK");
        }
    }
}
