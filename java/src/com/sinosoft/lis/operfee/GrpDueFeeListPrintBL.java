package com.sinosoft.lis.operfee;

import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GrpDueFeeListPrintBL {

    /**错误的容器*/
    public CErrors mErrors = new CErrors();

    private GlobalInput mGlobalInput = null;
    private TransferData mTransferData = new TransferData();
    private XmlExport xmlexport = null;
    private VData mResult = null;
    private SSRS mSSRS;
    private SSRS mRiskSSRS;
    private String mInNoticeNo = ""; //应收记录号
    private String mGrpContNo = "" ; // 团单号
    private String mAppNo= "" ; // 客户号
    private String mDealState = "" ; // 团单号
    private boolean mINNC = false;  //无名单入口标志Is NoName Count

    public GrpDueFeeListPrintBL() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {

        //取得外部传入数据
        if (!getInputData(cInputData)) {
            return false;
        }

        //校验数据合法性
        if (!getListData()) {
            return false;
        }

        //获取打印所需数据
        if (!getPrintData()) {
            return false;
        }
        return true;
    }

    /**
     * getInputData
     *
     * @param cInputData VData
     * @return boolean
     */
    private boolean getInputData(VData cInputData) {
        try {

            mGlobalInput = (GlobalInput) cInputData.
                           getObjectByObjectName("GlobalInput", 0);
            mTransferData = (TransferData) cInputData.getObjectByObjectName(
                    "TransferData", 0);
        } catch (Exception e) {
            mErrors.addOneError("传入的数据不完整。");
            System.out.println("传入的数据不完整，" + e.toString());
            return false;
        }
        mInNoticeNo = (String) mTransferData.getValueByName("InNoticeNo");
        mGrpContNo = (String) mTransferData.getValueByName("GrpContNo");
        mAppNo = (String) mTransferData.getValueByName("AppNo");
        mDealState = (String) mTransferData.getValueByName("DealState");
        String loadFlag = (String) mTransferData.getValueByName("LoadFlag");
        if(loadFlag != null && loadFlag.equals("WMD"))
        {
            mINNC = true;
        }

        return true;
    }

    /**
   * 查询列表显示数据
   * @param schema LCContSchema
   * @return String
   */

   private boolean getListData() {
       StringBuffer sqlSB = new StringBuffer();

       String strCont = "";
       String dealWMD = " exists (select 1 from LCCont "
                        + "where grpContNo = a.grpContNo and polType = '1') ";

       if(mINNC)
       {
           dealWMD = " and " + dealWMD;
       }
       else
       {
           dealWMD = " and not " + dealWMD;
       }

       if (!mInNoticeNo.equals("1") && !mInNoticeNo.equals(""))
       {
           sqlSB.append( "select '1', b.makedate,ShowManageName(a.ManageCom),")
//             .append("(select name from LABranchGroup where agentgroup = a.agentgroup),")
             .append("(select (select name from LABranchGroup z where z.agentgroup = subStr(x.branchseries,1,12)  ) from LABranchGroup x , LAAgent y where x.agentgroup = y.agentgroup and y.agentcode = a.AgentCode),")
             .append(" a.GrpContNo,b.GetNoticeNo,(select codename  from ldcode where codetype='dealstate' and code=b.dealstate),")
             .append(" a.GrpName,case when e.Mobile1 is null then '' else e.mobile1 end ||' '||case when e.Phone1 is null then '' else e.Phone1 end,e.GrpAddress,a.Cvalidate,")
             .append(" value((select sum(abs(sumDuePayMoney)) from LJSPayGrpB where getNoticeNo = b.getNoticeNo and payType = 'YEL'), 0),")
             .append(" b.GetNoticeNo,b.SumDuePayMoney,min(c.LastPayToDate),")
             .append(" (select codename  from ldcode where codetype='paymode' and code=a.PayMode),")
             .append(" db2inst1.getUniteCode(a.AgentCode),case when d.Mobile is null then '' else d.mobile end ||' '||case when d.Phone is null then '' else d.Phone end,d.Name,b.PayDate")
             .append(" from LCGrpCont a,LJSPayB b,LJSPayGrpB c ,LAAgent d, LCGrpAddress e")
             .append(" where   a.AppFlag='1' and b.OtherNo=a.GrpContNo and b.OtherNoType='1' and a.AgentCode = d.AgentCode and a.appntno =e.customerno ")
             .append(" and e.AddressNo = (select AddressNo from LCGrpAppnt where GrpContNo = a.GrpContNo)")
             .append(" and c.GrpContNo=b.OtherNo")
             .append(" and b.GetNoticeNo = c.GetNoticeNo and a.managecom like '")
             .append(mGlobalInput.ManageCom)
             .append("%'")
             .append(dealWMD)
             .append(" and  b.SerialNo in (")
             .append(mInNoticeNo)
             .append(")")
             .append(" group by  b.makedate,a.grpcontno,a.CValiDate,a.managecom,a.AgentCode,b.GetNoticeNo,a.PayMode,b.dealstate,")
             .append(" a.appntno,a.agentgroup,a.GrpName,b.SumDuePayMoney,b.paydate,e.Mobile1,e.Phone1,e.Grpaddress,d.Name,d.Mobile,d.Phone,c.LastPaytoDate")
             .append(" order by b.GetNoticeNo desc");


       }else
       {
           if (mGrpContNo !=null && !mGrpContNo.equals("")) strCont = " and a.GrpContNo='" + mGrpContNo + "'";
           if (mAppNo!=null && !mAppNo.equals("")) strCont = strCont + " and b.AppntNo='" + mAppNo + "'";
           System.out.println("mGrpContNo="+ mGrpContNo);
           System.out.println("mAppNo="+ mAppNo);
           System.out.println("mDealState="+ mDealState);
           if ("5".equals(mDealState))
           {
               sqlSB.append( "select '1', b.makedate,ShowManageName(a.ManageCom),")
//                    .append("(select name from LABranchGroup where agentgroup = a.agentgroup),")
                    .append("(select (select name from LABranchGroup z where z.agentgroup = subStr(x.branchseries,1,12)  ) from LABranchGroup x , LAAgent y where x.agentgroup = y.agentgroup and y.agentcode = a.AgentCode),")
                    .append(" a.GrpContNo,b.GetNoticeNo,(select codename  from ldcode where codetype='dealstate' and code=b.dealstate),")
                    .append(" a.GrpName,case when e.Mobile1 is null then '' else e.mobile1 end ||' '||case when e.Phone1 is null then '' else e.Phone1 end,e.GrpAddress,a.Cvalidate,")
                    .append(" value((select sum(abs(sumDuePayMoney)) from LJSPayGrpB where getNoticeNo = b.getNoticeNo and payType = 'YEL'), 0),")
                    .append(" b.GetNoticeNo,b.SumDuePayMoney,min(c.LastPayToDate),")
                    .append(" (select codename  from ldcode where codetype='paymode' and code=a.PayMode),")
                    .append(" db2inst1.getUniteCode(a.AgentCode),case when d.Mobile is null then '' else d.mobile end ||' '||case when d.Phone is null then '' else d.Phone end,d.Name,b.PayDate")
                    .append(" from LCGrpCont a,LJSPayB b,LJSPayGrpB c ,LAAgent d, LCGrpAddress e")
                    .append(" where   a.AppFlag='1' and b.OtherNo=a.GrpContNo and b.OtherNoType='1' and a.AgentCode = d.AgentCode and a.appntno =e.customerno ")
                    .append(" and e.AddressNo = (select AddressNo from LCGrpAppnt where GrpContNo = a.GrpContNo)")
                    .append(" and c.GrpContNo=b.OtherNo")
                    .append(" and b.GetNoticeNo = c.GetNoticeNo and a.managecom like '")
                    .append(mGlobalInput.ManageCom)
                    .append("%'")
                    .append(dealWMD)
                    .append(strCont)
                    .append(" group by  b.makedate,a.grpcontno,a.CValiDate,a.managecom,a.AgentCode,b.GetNoticeNo,a.PayMode,b.dealstate,")
                    .append(" a.appntno,a.agentgroup,a.GrpName,b.SumDuePayMoney,b.paydate,e.Mobile1,e.Phone1,e.Grpaddress,d.Name,d.Mobile,d.Phone,c.LastPaytoDate")
                    .append(" order by b.GetNoticeNo desc")
                   ;
           } else {
               sqlSB.append( "select '1', b.makedate,ShowManageName(a.ManageCom),")
//                   .append("(select name from LABranchGroup where agentgroup = a.agentgroup),")
                   .append("(select (select name from LABranchGroup z where z.agentgroup = subStr(x.branchseries,1,12)  ) from LABranchGroup x , LAAgent y where x.agentgroup = y.agentgroup and y.agentcode = a.AgentCode),")
                   .append(" a.GrpContNo,b.GetNoticeNo,(select codename  from ldcode where codetype='dealstate' and code=b.dealstate),")
                   .append(" a.GrpName,case when e.Mobile1 is null then '' else e.mobile1 end ||' '||case when e.Phone1 is null then '' else e.Phone1 end,e.GrpAddress,a.Cvalidate,")
                   .append(" value((select sum(abs(sumDuePayMoney)) from LJSPayGrpB where getNoticeNo = b.getNoticeNo and payType = 'YEL'), 0),")
                   .append(" b.GetNoticeNo,b.SumDuePayMoney,min(c.LastPayToDate),")
                   .append(" (select codename  from ldcode where codetype='paymode' and code=a.PayMode),")
                   .append(" db2inst1.getUniteCode(a.AgentCode),case when d.Mobile is null then '' else d.mobile end ||' '||case when d.Phone is null then '' else d.Phone end,d.Name,b.PayDate")
                   .append(" from LCGrpCont a,LJSPayB b,LJSPayGrpB c ,LAAgent d, LCGrpAddress e")
                   .append(" where   a.AppFlag='1' and b.OtherNo=a.GrpContNo and b.OtherNoType='1' and a.AgentCode = d.AgentCode and a.appntno =e.customerno ")
                   .append(" and e.AddressNo = (select AddressNo from LCGrpAppnt where GrpContNo = a.GrpContNo)")
                   .append(" and c.GrpContNo=b.OtherNo")
                   .append(" and b.GetNoticeNo = c.GetNoticeNo and a.managecom like '")
                   .append(mGlobalInput.ManageCom)
                   .append("%'")
                   .append( " and  b.DealState='" + mDealState + "'")
                   .append(dealWMD)
                   .append( strCont)
                   .append(" group by  b.makedate,a.grpcontno,a.CValiDate,a.managecom,a.AgentCode,b.GetNoticeNo,a.PayMode,b.dealstate,")
                   .append(" a.appntno,a.agentgroup,a.GrpName,b.SumDuePayMoney,b.paydate,e.Mobile1,e.Phone1,e.Grpaddress,d.Name,d.Mobile,d.Phone,c.LastPaytoDate")
                   .append(" order by b.GetNoticeNo desc");
           }

       }

       String tSQL = sqlSB.toString();
       System.out.println(tSQL);
       ExeSQL tExeSQL = new ExeSQL();

       mSSRS = tExeSQL.execSQL(tSQL);
       if (tExeSQL.mErrors.needDealError()) {
           CError tError = new CError();
           tError.moduleName = "MakeXMLBL";
           tError.functionName = "creatFile";
           tError.errorMessage = "查询XML数据出错！";
           this.mErrors.addOneError(tError);
           return false;
       }

       return true;
   }


   /**
    * getPrintData
    * 获取打印所需数据
    * @param 无
    * @return boolean
    */

    private boolean getPrintData() {

        xmlexport = new XmlExport(); //新建一个XmlExport的实例
        xmlexport.createDocument("GrpDueFeeList.vts", "printer"); //最好紧接着就初始化xml文档

        String[] title = {"序", "抽档日期", "管理机构", "营销部门", "保单号",
                         "应收记录号", "催收状态", "投保人", "投保人电话", "投保人联系地址",
                         "保单生效日","可抵余额","应交险种","应交保费","应收时间","收费方式",
                         "代理人编码","代理人电话","代理人姓名","缴费截至日期","交费人数"};
        xmlexport.addListTable(getListTable(), title);
        xmlexport.outputDocumentToFile("C:\\", "GrpPremDueFeeList123");
        mResult = new VData();
        mResult.addElement(xmlexport);

        return true;
    }


    /**
     * getListTable
     * 获取打印所需数据
     * @param 无
     * @return ListTable
    */
    private ListTable getListTable() {
        ListTable tListTable = new ListTable();
        for (int i = 1; i <= mSSRS.getMaxRow(); i++) {
            String[] info = new String[21];
            //标记序列号
           info[0] = String.valueOf(i);
           for (int j = 2; j <= mSSRS.getMaxCol(); j++) {
               if (j == 13) {
                   //获取险种信息
                   String tRiskcodeSQL =
                           "select distinct RiskCode from LJSPayGrpB where GetNoticeNO = '" +
                           mSSRS.GetText(i, j) + "'";
                   ExeSQL tExeSQL = new ExeSQL();
                   mRiskSSRS = tExeSQL.execSQL(tRiskcodeSQL);
                   info[12] = "";
                   for (int m = 1; m < mRiskSSRS.getMaxRow(); m++) {
                       info[12] += mRiskSSRS.GetText(m, 1) + "、";
                   }
                   info[12] += mRiskSSRS.GetText(mRiskSSRS.getMaxRow(), 1);
               } else {
                   info[j - 1] = mSSRS.GetText(i, j);
               }
           }
            //计算交费人数
            if(isWMD(mSSRS.GetText(i, 5)))
            {
                String sql = " select sum(peoples2Input) "
                             + "from LCNoNamePremTrace "
                             + "where getNoticeNo = '"
                             + mSSRS.GetText(i, 6) + "' ";
                String peoples2Input = new ExeSQL().getOneValue(sql);
                if(!peoples2Input.equals("") && !peoples2Input.equals("null"))
                {
                    info[20] = String.valueOf(peoples2Input);
                }
                else
                {
                    info[20] = " ";
                }
            }
            else
            {
                String sql = "  select peoples2 from LCGrpCont "
                             + "where grpContNo='" + mSSRS.GetText(1, 5) + "' ";
                String peoples2 = new ExeSQL().getOneValue(sql);
                if(!peoples2.equals("") && !peoples2.equals("null"))
                {
                    info[20] = String.valueOf(peoples2);
                }
                else
                {
                    info[20] = " ";
                }
            }

            tListTable.add(info);
        }

       tListTable.setName("ZT");
       return tListTable;
    }

    /**
     * 校验报单是否无名单
     * @param GrpContNo String
     * @return boolean
     */
    private boolean isWMD(String grpContNo)
    {
        String sql = "  select 1 "
                     + "from LCCont "
                     + "where grpContNo = '" + grpContNo
                     + "'  and polType = '1' ";
        String t = new ExeSQL().getOneValue(sql);
        if(t.equals("") || t.equals("null"))
        {
            return false;
        }

        return true;
    }

    /**
     * 返回清单数据
     * @return VData
     */
    public VData getResult() {
        return mResult;
    }



    public static void main(String[] args) {
        GrpDueFeeListPrintBL p = new GrpDueFeeListPrintBL();
        TransferData tTransferData= new TransferData();
        tTransferData.setNameAndValue("GrpContNo","0000000703");
        tTransferData.setNameAndValue("AppNo","");
        tTransferData.setNameAndValue("DealState","3");
        tTransferData.setNameAndValue("InNoticeNo","1");

        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "000006";
        tGI.ManageCom = "86";


        VData v = new VData();
        v.addElement(tGI);
	v.addElement(tTransferData);
        if (!p.submitData(v, "")) {
            System.out.println(p.mErrors.getErrContent());
        } else {
            System.out.println("OK");
        }
    }

    private void jbInit() throws Exception {
    }
}
