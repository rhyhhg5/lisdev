package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;

//程序名称：IndiDueFeePrintUI.java
//程序功能：打印、下载应收清单
//创建日期：2008-01-07
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

public class IndiDueFeePrintUI 
{
    /**错误的容器*/
    public CErrors mErrors = null;
    
    private VData mResult = null;

    public IndiDueFeePrintUI() 
    {
    }

    public boolean submitData(VData cInputData, String cOperate) 
    {
        IndiDueFeePrintBL tIndiDueFeePrintBL = new IndiDueFeePrintBL();
        if (!tIndiDueFeePrintBL.submitData(cInputData, cOperate)) 
        {
            mErrors.copyAllErrors(tIndiDueFeePrintBL.mErrors);
            return false;
        }
        mResult = tIndiDueFeePrintBL.getResult();
        return true;
    }

    public VData getResult() 
    {
        return this.mResult;
    }
}
