package com.sinosoft.lis.operfee;

import com.sinosoft.lis.db.LCUrgeVerifyLogDB;
import com.sinosoft.lis.db.LDSysVarDB;
import com.sinosoft.lis.f1print.PrintManagerBL;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.MMap;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.lis.pubfun.PubFun1;
import com.sinosoft.lis.pubfun.PubSubmit;
import com.sinosoft.lis.schema.LCContSchema;
import com.sinosoft.lis.schema.LCPolSchema;
import com.sinosoft.lis.schema.LCUrgeVerifyLogSchema;
import com.sinosoft.lis.schema.LJSPaySchema;
import com.sinosoft.lis.schema.LOPRTManagerSchema;
import com.sinosoft.lis.schema.LOPRTManagerSubSchema;
import com.sinosoft.lis.vschema.LCUrgeVerifyLogSet;
import com.sinosoft.lis.xb.PRnewDueFeeBL;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.TransferData;
import com.sinosoft.utility.VData;

public class OmnipFeeBatchBL {
	
    public OmnipFeeBatchBL(){
	
    }
    
    public CErrors mErrors = new CErrors();   
    private VData mInputData = new VData();
    private GlobalInput tGI = new GlobalInput();
    /** 数据操作字符串 */
    private String mOperate = "";
    private String CurrentDate = PubFun.getCurrentDate();
    private String CurrentTime = PubFun.getCurrentTime();
    private String StartDate = ""; //应收开始时间
    private String EndDate = ""; //应收结束时间
    private String mserNo = ""; //批次号
    private TransferData mTransferData = new TransferData();
    public boolean submitData(VData cInputData, String cOperate) {
    	 this.mOperate = cOperate;
         //接收传入数据
         if (!getInputData(cInputData)) {
             return false;
         }
         //处理数据
         if (!dealData()) {
             return false;
         }
         if (!dealUrgeLog("3", "UPDATE")) {
                    return false;
         }
         return true;
    	
    }
    /**
     * 
     * @return boolean
     */
    public boolean dealData() {
        //接收外部传入数据
        StartDate = (String) mTransferData.getValueByName("StartDate");
        EndDate = (String) mTransferData.getValueByName("EndDate");
        String manageCom = (String) mTransferData.getValueByName("ManageCom");
        String querySql = (String)mTransferData.getValueByName("QuerySql");
      
       
        //批量抽档数据查询

        if(querySql == null || querySql.equals(""))
        {
            
        	 querySql = "select ContNo,PrtNo,AppntName,CValiDate,SumPrem,"
                       + "  (select AccGetMoney from LCAppAcc  where CustomerNo=lccont.appntno),prem,min(paytodate),"
                       + "  (select codename from ldcode where codetype='paymode' and code=PayMode),"
                       + "  ShowManageName(ManageCom),AgentCode,'' "
                       + "from LCCont "
                       + "where AppFlag='1'  and ContType='1' and (StateFlag is null or StateFlag = '1') "
                       +" and not exists(select * from LCContState where StateType='GracePeriod' and state='0' and  LCContState.contNo = LCCont.ContNo)"
                       + " and exists (select 1 from lcpol a,lmriskapp b where  a.riskcode = b.RiskCode and (a.StandbyFlag1  is null or a.StandbyFlag1='0' or a.StandbyFlag1='' or a.StandbyFlag1='2')and b.risktype4 ='4' and a.Contno = lccont.ContNO)"
//                       + " and (cardflag='0' or cardflag is null) "
                       + "    and ContNo not in (select otherno from ljspay where othernotype='2' and OtherNo=ContNo )  "
                       + "    and exists "
                       + "       (select 'X' from LCPol "
                       + "       where contno=lccont.contno "
                       + "          and (PaytoDate>='" + StartDate
                       + "'             and PaytoDate<='" + EndDate + "' "
                       + "              and (polstate is null or (polstate is not null and polstate not like '02%%' and polstate not like '03%%'))"
                       + "              and managecom like '" + manageCom + "%%'"
                       //续期
			                 + "              and ( PaytoDate<payEndDate "
			                 + "                    and PayIntv>0 "
			                 + "                    and exists (select riskcode from lmrisk WHERE cPayFlag='Y' and lmrisk.riskcode=lcpol.riskcode) "
			                 + "                  )"
			                 + "              and AppFlag='1' and (StopFlag='0' or StopFlag is null)"
                       + "              and PolNo not in (select PolNo from LJSPayPerson a where a.contno=contno)"
                       + "              ) "
                       + "      ) "
			                 + "group by contNo,PrtNo,AppntName,CValiDate,SumPrem,appntno,prem,PayMode,ManageCom,AgentCode "
			                 + "order by contNo "
			                 + "with ur "
			                  ;
        }

        ExeSQL tExeSQL = new ExeSQL();
        SSRS tSSRS = tExeSQL.execSQL(querySql);
        if (tSSRS == null || tSSRS.getMaxRow() == 0) {
            CError.buildErr(this, "系统中没有符合催收时间范围的保单信息！");
            return false;
        }

        //抽档批次号
        String tLimit = PubFun.getNoLimit(manageCom);
        String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);
        mserNo = serNo;
        //插入催收核销日志表
        if (!dealUrgeLog("1", "INSERT")) {
            return false;
        }


        //循环处理数据
        for (int i = 1; i <= tSSRS.getMaxRow(); i++) {

            String contNo = tSSRS.GetText(i, 1);
            LCContSchema tempLCContSchema = new LCContSchema();
            tempLCContSchema.setContNo(contNo);

            VData tVData = new VData();
            tVData.add(tempLCContSchema);
            tVData.add(tGI);
            tVData.add(serNo);
            mTransferData.setNameAndValue("serNo",serNo);
            tVData.add(mTransferData);

            //针对保单进行抽档操作（例如：生成应收数据，应收抽档通知书等）
            PRnewDueFeeBL tPRnewDueFeeBL = new PRnewDueFeeBL();
            if (!tPRnewDueFeeBL.submitData(tVData, "INSERT")) {
                CError.buildErr(this, "保单号为：" + contNo + "的保单催收失败:"
                    + tPRnewDueFeeBL.mErrors.getFirstError());
                continue;
            }
        }
        return true;
    }
     /**
      * 从输入数据中得到所有对象
      * @param mInputData:
      *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
      */
     private boolean getInputData(VData mInputData) {
         tGI = ((GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));       
         mTransferData = (TransferData) mInputData.getObjectByObjectName(
                 "TransferData",
                 0);         
         if (tGI == null) {
             // @@错误处理
             CError tError = new CError();
             tError.moduleName = "IndiDueFeeMultiBL";
             tError.functionName = "getInputData";
             tError.errorMessage = "没有得到足够的数据，请您确认!";
             this.mErrors.addOneError(tError);

             return false;
         }

         return true;
     }

    //准备往后层输出所需要的数据
    //输出：如果准备数据时发生错误则返回false,否则返回true
    private boolean prepareOutputData() {
        mInputData = new VData();

        return true;
    }

    private VData getResult() {
        mInputData.clear();

        return mInputData;
    }
    /**
        * 加入到催收核销日志表数据
        * @param
        * @return boolean
        */
       private boolean dealUrgeLog(String pmDealState, String pmOpreat) {

           LCUrgeVerifyLogSchema tLCUrgeVerifyLogSchema = new
                   LCUrgeVerifyLogSchema();
           //加到催收核销日志表
           tLCUrgeVerifyLogSchema.setSerialNo(mserNo);
           tLCUrgeVerifyLogSchema.setRiskFlag("2");
           tLCUrgeVerifyLogSchema.setStartDate(StartDate);
           tLCUrgeVerifyLogSchema.setEndDate(EndDate);

           tLCUrgeVerifyLogSchema.setOperateType("1"); //1：续期催收操作,2：续期核销操作
           tLCUrgeVerifyLogSchema.setOperateFlag("2"); //1：个案操作,2：批次操作
           tLCUrgeVerifyLogSchema.setOperator(tGI.Operator);
           tLCUrgeVerifyLogSchema.setDealState(pmDealState); //1 : 正在处理中,2 : 处理错误,3 : 处理已完成
           if (pmOpreat.equals("INSERT")) {
               tLCUrgeVerifyLogSchema.setMakeDate(CurrentDate);
               tLCUrgeVerifyLogSchema.setMakeTime(CurrentTime);
               tLCUrgeVerifyLogSchema.setModifyDate(CurrentDate);
               tLCUrgeVerifyLogSchema.setModifyTime(CurrentTime);
               tLCUrgeVerifyLogSchema.setErrReason("");

           } else {
               LCUrgeVerifyLogDB tLCUrgeVerifyLogDB = new LCUrgeVerifyLogDB();
               LCUrgeVerifyLogSet tLCUrgeVerifyLogSet = new
                       LCUrgeVerifyLogSet();
               tLCUrgeVerifyLogDB.setSerialNo(mserNo);
               tLCUrgeVerifyLogDB.setOperateType("1");
               tLCUrgeVerifyLogDB.setOperateFlag("2");
               tLCUrgeVerifyLogDB.setRiskFlag("2");
               tLCUrgeVerifyLogSet = tLCUrgeVerifyLogDB.query();
                if (!(tLCUrgeVerifyLogSet ==null )&& tLCUrgeVerifyLogSet.size() > 0) {
                    tLCUrgeVerifyLogSchema = tLCUrgeVerifyLogSet.get(1);
                    tLCUrgeVerifyLogSchema.setModifyDate(PubFun.getCurrentDate());
                    tLCUrgeVerifyLogSchema.setModifyTime(PubFun.getCurrentTime());
                    tLCUrgeVerifyLogSchema.setDealState(pmDealState);
                }else{
                   return false;
               }


           }

           MMap tMap = new MMap();
           tMap.put(tLCUrgeVerifyLogSchema, pmOpreat);
           VData tInputData = new VData();
           tInputData.add(tMap);
           PubSubmit tPubSubmit = new PubSubmit();
           if (tPubSubmit.submitData(tInputData, "") == false) {
               this.mErrors.addOneError("PubSubmit:处理LCUrgeVerifyLog 表失败!");
               return false;
           }
           return true;
       }


    /**
     * 准备打印数据
     * @param tLCPolSchema
     * @return
     */
    public VData getPrintData(LCContSchema tLCContchema,
                              LJSPaySchema tLJSPaySchema, String prtSeqNo) {
        VData tVData = new VData();
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        LOPRTManagerSubSchema tLOPRTManagerSubSchema = new
                LOPRTManagerSubSchema();

        try {
            tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
            tLOPRTManagerSchema.setOtherNo(tLCContchema.getContNo());
            tLOPRTManagerSchema.setOtherNoType("00");
            tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_PRnewNotice);
            tLOPRTManagerSchema.setManageCom(tLCContchema.getManageCom());
            tLOPRTManagerSchema.setAgentCode(tLCContchema.getAgentCode());
            tLOPRTManagerSchema.setReqCom(tLCContchema.getManageCom());
            tLOPRTManagerSchema.setReqOperator(tLCContchema.getOperator());
            tLOPRTManagerSchema.setPrtType("0");
            tLOPRTManagerSchema.setStateFlag("0");
            tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
            tLOPRTManagerSchema.setOldPrtSeq(prtSeqNo);
            tLOPRTManagerSchema.setStandbyFlag1(tLCContchema.getInsuredName());
            tLOPRTManagerSchema.setStandbyFlag2(tLJSPaySchema.getGetNoticeNo());
            tLOPRTManagerSchema.setPatchFlag("0");
        } catch (Exception ex) {
            return null;
        }

        tVData.add(tLOPRTManagerSchema);
        tVData.add(tLOPRTManagerSubSchema);

        return tVData;
    }

    /**
     * 准备打印数据
     * @param tLCPolSchema
     * @return
     */
    public VData getPrintData(LCPolSchema tLCPolSchema,
                              LJSPaySchema tLJSPaySchema) {
        VData tVData = new VData();
        LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
        LOPRTManagerSubSchema tLOPRTManagerSubSchema = new
                LOPRTManagerSubSchema();

        try {
            String tLimit = PubFun.getNoLimit(tLCPolSchema.getManageCom());
            String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
            tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
            tLOPRTManagerSchema.setOtherNo(tLCPolSchema.getPrtNo());
            tLOPRTManagerSchema.setOtherNoType("00");
            tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_PRnewNotice);
            tLOPRTManagerSchema.setManageCom(tLCPolSchema.getManageCom());
            tLOPRTManagerSchema.setAgentCode(tLCPolSchema.getAgentCode());
            tLOPRTManagerSchema.setReqCom(tLCPolSchema.getManageCom());
            tLOPRTManagerSchema.setReqOperator(tLCPolSchema.getOperator());
            tLOPRTManagerSchema.setPrtType("0");
            tLOPRTManagerSchema.setStateFlag("0");
            tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
            tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
            tLOPRTManagerSchema.setOldPrtSeq(prtSeqNo);
            tLOPRTManagerSchema.setStandbyFlag1(tLCPolSchema.getInsuredName());
            tLOPRTManagerSchema.setStandbyFlag2(tLJSPaySchema.getGetNoticeNo());
            tLOPRTManagerSchema.setStandbyFlag2(tLJSPaySchema.getGetNoticeNo());
            tLOPRTManagerSchema.setPatchFlag("0");

            tLOPRTManagerSubSchema.setPrtSeq(prtSeqNo);
            tLOPRTManagerSubSchema.setGetNoticeNo(tLJSPaySchema.getGetNoticeNo());
            tLOPRTManagerSubSchema.setOtherNo(tLCPolSchema.getPolNo());
            tLOPRTManagerSubSchema.setOtherNoType("00");
            tLOPRTManagerSubSchema.setRiskCode(tLCPolSchema.getRiskCode());
            tLOPRTManagerSubSchema.setDuePayMoney(tLJSPaySchema.
                                                  getSumDuePayMoney());
            tLOPRTManagerSubSchema.setAppntName(tLCPolSchema.getAppntName());
            tLOPRTManagerSubSchema.setTypeFlag("1");
        } catch (Exception ex) {
            return null;
        }

        tVData.add(tLOPRTManagerSchema);
        tVData.add(tLOPRTManagerSubSchema);

        return tVData;
    }

    /**
     * 检查续保续期
     * @param tLCPolSchema
     * @return
     */
    private boolean checkRnew(LCPolSchema tLCPolSchema) {
        LDSysVarDB tLDSysVarDB = new LDSysVarDB();
        tLDSysVarDB.setSysVar("relationflag");

        if (tLDSysVarDB.getInfo() == false) {
            return false;
        }

        if (tLDSysVarDB.getSysVarValue().equals("0")) { //0-续期续保不关联，1，2为关联
            return false;
        }

        String strSQL = "select count(*) from LCRnewStateLog where PrtNo='" +
                        tLCPolSchema.getPrtNo() + "' and State not in('5','6')";
        ExeSQL tExeSQL = new ExeSQL();

        if (tExeSQL.getOneValue(strSQL).equals("0")) {
            return false;
        }

        strSQL = "update LCRnewStateLog set MainRiskStatus='1' where prtno='" +
                 tLCPolSchema.getPrtNo() + "'";
        tExeSQL = new ExeSQL();
        tExeSQL.execUpdateSQL(strSQL);

        return true;
    }
   
}
