package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;
import java.lang.String;
import com.sinosoft.lis.xb.PRnewDueVerifyBL;

/**
 * <p>Title: Web业务系统</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Sinosoft</p>
 * @author HZM
 * @version 1.0
 */

public class IndiFinVerifyMultBL {
  //错误处理类，每个需要错误处理的类中都放置该类
  public CErrors mErrors = new CErrors();

  /** 往后面传输数据的容器 */
  private VData mInputData = new VData();
  private GlobalInput tGI = new GlobalInput();
  private SSRS mSSRS = new SSRS();
  private ExeSQL mEexSQL = new ExeSQL();

  /** 数据操作字符串 */
  private String mOperate = "";
  private String CurrentDate = PubFun.getCurrentDate();
  private String CurrentTime = PubFun.getCurrentTime();
  private String tGetNoticeNo ;

  /** 数据表  保存数据*/
  //个人保单表

  private TransferData mTransferData = new TransferData();

  public IndiFinVerifyMultBL() {
  }

  public static void main(String[] args) {
    IndiFinVerifyMultBL tIndiFinVerifyMultBL = new IndiFinVerifyMultBL();

    GlobalInput tGI = new GlobalInput();
    tGI.ComCode = "86";
    tGI.Operator = "endor0";
    tGI.ManageCom = "8611";

    TransferData tTransferData=new TransferData();
    tTransferData.setNameAndValue("PayDate","2008-8-3");

    VData tVData = new VData();
    tVData.add(tGI);
    tVData.add(tTransferData);

    if (tIndiFinVerifyMultBL.submitData(tVData, "INSERT") == false) {
      System.out.println("团单批处理核销信息提示：：" +
                         tIndiFinVerifyMultBL.mErrors.getFirstError());
    }
    else {
      System.out.println("团单批处理核销完成");
    }
  }

  /**
   * 传输数据的公共方法
   * @param cInputData VData: 包含：
   * 1、	GlobalInput对象，完整的登陆用户信息
   * 2、	TransferData对象，包含EndDate
   * @param cOperate String操作类型，此可为空字符串“”
   * @return boolean：操作成功true，否则false
   */
  public boolean submitData(VData cInputData, String cOperate) {
    //将操作数据拷贝到本类中
    this.mOperate = cOperate;

    if (!getInputData(cInputData))
      return false;

    if (!dealData())
      return false;

    return true;
  }
  private boolean dealData()
  {
    String EndDate=(String)mTransferData.getValueByName("PayDate");
    String managecom = (String) tGI.ManageCom;
    String ContSql = null ;
    
//  续期收费
    ContSql = "select c.getnoticeno,c.otherno "
	     + "from LJSPayPerson a ,LCCont  b,LJSPay c "
	     + " where a.LastPayToDate <='" + EndDate  + "' " 
	     + "   and c.OtherNo=b.ContNo"
	     + "   and a.ContNo=b.ContNo"
	     + " and a.getnoticeno = c.getnoticeno "
	     + "   and b.PayIntv > 0 "
	     + "   and c.othernotype='2' "
	     + "   and c.manageCom like '" + managecom + "%' "
	     + "   and exists(select RiskCode from LMRisk where (CPayFlag='Y') and riskcode=a.RiskCode )"
	     + "   and ((select count(*) from LJTempFee where TempFeeNo= c.GetNoticeNo  and ConfFlag='0') > 0 or c.sumDuePayMoney <= 0)"
	     //zhanggm 20100929 修改万能校验，因为系统中存在一个保单同时包含万能与普通险种。
		 + " and not exists(select 1 from lcpol ,lmriskapp  where lcpol.riskcode = lmriskapp.RiskCode and lmriskapp.risktype4='4' and lcpol.Contno = b.ContNo ) "
	     + "group by c.getnoticeno,c.otherno "
	     
	     + " union "
	     
	     //续保收费
	     + "select c.getnoticeno,c.otherno "
	     + "from LJSPayPerson a ,LCCont  b,LJSPay c "
	     + " where a.LastPayToDate <='" + EndDate  + "' "
	     + "   and c.othernotype='2' "
	     + " and a.getnoticeno = c.getnoticeno "
	     + "   and c.manageCom like '" + managecom + "%%' "
	     + "   and ( b.ContNo =(select distinct contNo from LCRnewStateLog where contno=c.otherNo and state > '3')) "
	     + "   and exists(select RiskCode from LMRisk where ( RNewFlAg!='N') and riskcode=a.RiskCode ) "
	     + "   and a.ContNo=b.ContNo "
	     + "   and ((select count(*) from LJTempFee where TempFeeNo= c.GetNoticeNo  and ConfFlag='0') > 0 or c.sumDuePayMoney <= 0)"
	     + " and a.riskcode not in('320106','120706') "
	     //zhanggm 20100929 修改万能校验，因为系统中存在一个保单同时包含万能与普通险种。
		 + " and not exists(select 1 from lcpol ,lmriskapp  where lcpol.riskcode = lmriskapp.RiskCode and lmriskapp.risktype4='4' and lcpol.Contno = b.ContNo ) "
	     + "group by c.getnoticeno,c.otherno ";
	  
	  
    System.out.println(ContSql);
    mSSRS = mEexSQL.execSQL(ContSql);
    int succCount=0;
    String tLimit = PubFun.getNoLimit(managecom);
    String serNo = PubFun1.CreateMaxNo("SERIALNO", tLimit);

    for(int i=1;i<=mSSRS.getMaxRow();i++)
    {
        tGetNoticeNo =mSSRS.GetText(i,1);
        String Contno =mSSRS.GetText(i,2);
        LCContDB tLCcontDB = new LCContDB();
        LCContSchema tLCContSchema = new LCContSchema();
        tLCcontDB.setContNo(Contno);
        if(!tLCcontDB.getInfo())
        {
            this.mErrors.addOneError("查询保单" + Contno + "失败！");
            return false ;
        }
      tLCContSchema =tLCcontDB.getSchema();
      VData tVData = new VData();

      tVData.add(tLCContSchema);
      tVData.add(tGI);
      //这个原来用了一个全局的TransferData，每次放GetNoticeNo的时候会重复所以改成局部的。 modify by fuxin 2008-5-10
      TransferData  tTransferData = new TransferData();
      tTransferData.setNameAndValue("GrpSerNo",serNo);
      tTransferData.setNameAndValue("GetNoticeNo",tGetNoticeNo);
      System.out.println("说你是谁："+tGetNoticeNo);

      tVData.add(tTransferData);
      PRnewDueVerifyBL tPRnewDueVerifyBL = new PRnewDueVerifyBL();
      if(!tPRnewDueVerifyBL.submitData(tVData,"VERIFY"))
      {
        //如果处理失败
        this.mErrors.addOneError("收费号" + tGetNoticeNo + "核销失败，原因如下："
                                 + tPRnewDueVerifyBL.mErrors.getFirstError());
      }
      else
      {
        succCount++;
      }
    }
    if(succCount==mSSRS.getMaxRow())
    {
      System.out.println("本次批量核销全部成功！");
    }
    return true;
  }


  /**
   * 从输入数据中得到所有对象
   * @param mInputData: 传入submitData中的VData对象
   *输出：如果没有得到足够的业务数据对象，则返回false,否则返回true
   */
  private boolean getInputData(VData mInputData) {
    tGI = ( (GlobalInput) mInputData.getObjectByObjectName("GlobalInput", 0));
    mTransferData = (TransferData) mInputData.getObjectByObjectName(
        "TransferData", 0);
    if (tGI == null || mTransferData == null) {
      // @@错误处理
      CError tError = new CError();
      tError.moduleName = "GrpDueFeeMultiBL";
      tError.functionName = "getInputData";
      tError.errorMessage = "没有得到足够的数据，请您确认!";
      this.mErrors.addOneError(tError);
      return false;
    }
    return true;
  }

  //准备往后层输出所需要的数据
  //输出：如果准备数据时发生错误则返回false,否则返回true
  private boolean prepareOutputData() {
    mInputData = new VData();
    return true;
  }

  /**
   * 准备打印数据
   * @param tLCPolSchema
   * @return
   */
  public LOPRTManagerSchema getPrintData(LCGrpContSchema tLCGrpContSchema) {
    LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
    try {
      String tLimit = PubFun.getNoLimit(tLCGrpContSchema.getManageCom());
      String prtSeqNo = PubFun1.CreateMaxNo("PRTSEQNO", tLimit);
      tLOPRTManagerSchema.setPrtSeq(prtSeqNo);
      tLOPRTManagerSchema.setOtherNo(tLCGrpContSchema.getGrpContNo());
      tLOPRTManagerSchema.setOtherNoType("01");
      tLOPRTManagerSchema.setCode(PrintManagerBL.CODE_REPAY);
      tLOPRTManagerSchema.setManageCom(tLCGrpContSchema.getManageCom());
      tLOPRTManagerSchema.setAgentCode(tLCGrpContSchema.getAgentCode());
      tLOPRTManagerSchema.setReqCom(tLCGrpContSchema.getManageCom());
      tLOPRTManagerSchema.setReqOperator(tLCGrpContSchema.getOperator());
      tLOPRTManagerSchema.setPrtType("0");
      tLOPRTManagerSchema.setStateFlag("0");
      tLOPRTManagerSchema.setMakeDate(PubFun.getCurrentDate());
      tLOPRTManagerSchema.setMakeTime(PubFun.getCurrentTime());
    }
    catch (Exception ex) {
      return null;
    }
    return tLOPRTManagerSchema;
  }

}
