package com.sinosoft.lis.operfee;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.xml.namespace.QName;

import org.apache.axis2.AxisFault;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import utils.system;

import com.cbsws.obj.WeChatRenewalMesBody;
import com.sinosoft.lis.pubfun.GlobalInput;
import com.sinosoft.lis.pubfun.PubFun;
import com.sinosoft.utility.CError;
import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.ExeSQL;
import com.sinosoft.utility.SSRS;
import com.sinosoft.utility.VData;

/**
 * <p>Title: 客户续期微信公众号通知</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2017</p
 *
 * <p>Company: </p>
 *
 * @author LZJ
 * 
 */

public class WeChatRenewalAlertBL {
    private GlobalInput mG = new GlobalInput();
    private String mCurrentDate = PubFun.getCurrentDate();
    private ExeSQL mExeSQL = new ExeSQL();
    /**错误处理类，每个需要错误处理的类中都放置该类 */
    public CErrors mErrors = new CErrors();


	public WeChatRenewalAlertBL() {

	}
	
	/**
	 * 生成批次号
	 */
	public static String  makeBatchNO(){
		return "XQ"+PubFun.getCurrentDate2()+PubFun.getCurrentTime2()+genRandomNum(5);
	}
	
	/**
	 * 生成5位流水号
	 * @param pwd_len
	 * @return
	 */
	private static String genRandomNum(int pwd_len) {
		int pwd = (int)(Math.random()*Math.pow(10,pwd_len));
		return String.valueOf(pwd);
	}
	
	public boolean submitData(VData cInputData, String cOperate) {
        cInputData = (VData) cInputData.clone();

        if (!getInputData(cInputData)) 
        {
            return false;
        }
        if (cOperate != null && cOperate.trim().equals("XQMSG")) {
        	List<WeChatRenewalMesBody> bodies = creatBodyInfo();
        	if(bodies.size()!=0){
        		int count = 0;
        		List<WeChatRenewalMesBody> list = new ArrayList<WeChatRenewalMesBody>();
        		Iterator<WeChatRenewalMesBody> it1 = bodies.iterator();
        		while(it1.hasNext()){
        		   list.add(it1.next());
        		   count++;
        		   if(count==500){
        				String strXml = creatXml(list);
        				if(strXml!=null){
        					long startTime = System.currentTimeMillis();
        					System.out.println(startTime);
        					Axis2MethodSend(strXml);
        					long endTime = System.currentTimeMillis();
        					System.out.println(endTime);
        					System.out.println("返回报文用时:"+(endTime-startTime)/1000+"秒");
        				}
        				list.clear();        			   
        				count = 0;
        		   }        		   
        		}
        		if(count>0){
        			String strXml = creatXml(list);
    				if(strXml!=null){
    					Axis2MethodSend(strXml);   		
    				}
        		}

        	}
        } else {
            mErrors.addOneError(new CError("不支持的操作字符串！"));
            return false;
        }
        return true;
    }
	private boolean getInputData(VData cInputData) 
    {
    	mG.setSchema((GlobalInput) cInputData.getObjectByObjectName(
                "GlobalInput", 0));
        return true;
    }
	public List<WeChatRenewalMesBody> creatBodyInfo(){
		String sqlStr = "select 	b.AppntName, 	b.AppntSex, 	b.AppntIDType, 	" 
 			   + "b.AppntIDNo, 	b.AppntBirthday, 	mobile(b.contno), 	b.contno,	b.PayToDate,	 " 
 			   + "a.SpecContent	" 
 			   + "from LCCSpec a,lccont b " 
 			   + "where  	a.contno = b.contno " 
 			   + "and b.conttype = '1' " 
 			   + "and b.appflag = '1' " 
 			   + "and a.MakeDate = '" + mCurrentDate + "' " 
 			   + "and a.SpecCode = '001'" 
 			   + "and a.SpecType in ('xq01','xq02','xq03') with ur";
		System.out.println(sqlStr);
		SSRS tMsgSuccSSRS =mExeSQL.execSQL(sqlStr);
		List<WeChatRenewalMesBody> bodies = new ArrayList<WeChatRenewalMesBody>();
		for (int i = 1; i <= tMsgSuccSSRS.getMaxRow(); i++) {
	    	String name = tMsgSuccSSRS.GetText(i, 1);
	    	String sex = tMsgSuccSSRS.GetText(i, 2);
	    	String idType = tMsgSuccSSRS.GetText(i, 3);
	    	String idNo = tMsgSuccSSRS.GetText(i, 4);
	    	String birthday = tMsgSuccSSRS.GetText(i, 5);
	    	String phone = tMsgSuccSSRS.GetText(i, 6);
	    	String contno = tMsgSuccSSRS.GetText(i, 7);
	    	String payToDate = tMsgSuccSSRS.GetText(i, 8);
	    	String messageContent = tMsgSuccSSRS.GetText(i, 9);
	    	WeChatRenewalMesBody body = new WeChatRenewalMesBody();
	    	body.setName(name);
	    	body.setSex(sex);
	    	body.setIdType(idType);
	    	body.setIdNo(idNo);
	    	body.setBirthday(birthday);
	    	body.setPhone(phone);
	    	body.setContno(contno);
	    	body.setPayToDate(payToDate);
	    	body.setMessageContent(messageContent);
	    	bodies.add(body);
		}
		return bodies;
	}
    /**
     * 生成请求报文    
     * @return
     */
	public String creatXml(List<WeChatRenewalMesBody> bodies){
		Element weiXinMessages = new Element("WeiXinMessages");
		Document document = new Document(weiXinMessages);
		Element BatchNo = new Element("BatchNo");
		BatchNo.setText(makeBatchNO());
		weiXinMessages.addContent(BatchNo);
		
		Element SendOperator = new Element("SendOperator");
		SendOperator.setText("01");
		weiXinMessages.addContent(SendOperator);

		
		Element SendDate = new Element("SendDate");
		SendDate.setText(PubFun.getCurrentDate());
		weiXinMessages.addContent(SendDate);
		
		Element SendTime = new Element("SendTime");
		SendTime.setText(PubFun.getCurrentTime());
		weiXinMessages.addContent(SendTime);
		
		Element PersonMessage = new Element("PersonMessage");
		
	    for (WeChatRenewalMesBody body:bodies) {
	    	String name = body.getName();
	    	String sex = body.getSex();
	    	String idType = body.getIdType();
	    	String idNo = body.getIdNo();
	    	String birthday = body.getBirthday();
	    	String phone = body.getPhone();
	    	String contno = body.getContno();
	    	String payToDate = body.getPayToDate();
	    	String messageContent = body.getMessageContent();
	   	 
	    	Element itemEle = new Element("Item");
	   	 
	    	Element nameEle = new Element("Name");
	    	nameEle.setText(name);
	    	itemEle.addContent(nameEle);
	    	
	    	Element sexEle = new Element("SEX");
	    	sexEle.setText(sex);
	    	itemEle.addContent(sexEle);
	    	
	    	Element idTypeEle = new Element("IDType");
	    	idTypeEle.setText(idType);
	    	itemEle.addContent(idTypeEle);
	    	
	    	Element idNoEle = new Element("IDNO");
	    	idNoEle.setText(idNo);
	    	itemEle.addContent(idNoEle);
	    	
	    	Element birthdayEle = new Element("Birthday");
	    	birthdayEle.setText(birthday);
	    	itemEle.addContent(birthdayEle);
	    	
	    	Element phoneEle = new Element("Phone");
	    	phoneEle.setText(phone);
	    	itemEle.addContent(phoneEle);
	    	
	    	Element businessTypeEle = new Element("BusinessType");
	    	businessTypeEle.setText("00");
	    	itemEle.addContent(businessTypeEle);
	    	
	    	Element remarkEle = new Element("Remark");
	    	remarkEle.setText(contno);
	    	itemEle.addContent(remarkEle);
	    	
	    	Element messageContentEle = new Element("MessageContent");
	    	messageContentEle.setText(messageContent);
	    	itemEle.addContent(messageContentEle);
	    	
	    	
	    	PersonMessage.addContent(itemEle);
	    	
	    }
	    weiXinMessages.addContent(PersonMessage);
	    XMLOutputter out = new XMLOutputter();
	    String outXml = null;
	    try {
	    	outXml = out.outputString(document);
	    	System.out.println(outXml);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return outXml;
		
	}
	/**
	 * 
	 * @param Om
	 * @return
	 */
	public String Axis2MethodSend(String Om){
		String sql = "select codename from ldcode where codetype = 'wechatrenewaladdress' and code = '001'";
		String tStrTargetEendPoint = mExeSQL.getOneValue(sql);
		String tStrNamespace = "http://ecwebservice.ECPH.com";
	  	System.out.println(tStrNamespace);
	  	System.out.println(tStrTargetEendPoint);
	  	String p =  null;
	  	try {
	  		RPCServiceClient client = new RPCServiceClient();
	  		EndpointReference erf = new EndpointReference(tStrTargetEendPoint);
	  		Options option = client.getOptions();   
	  		option.setTo(erf);
	  		option.setTimeOutInMilliSeconds(10000000000L);
	  		option.setAction("service");
	  		QName name = new QName(tStrNamespace, "service");
	  		Object[] object = new Object[]{Om} ;
	  		Class[] returnTypes = new Class[] { String.class };
	  		
	  		Object[] response = client.invokeBlocking(name, object, returnTypes);
	  		p = (String) response[0];
	  		
	  		System.out.println("返回报文:" + p);
	  		
	  		
	  	} catch (AxisFault e) {
	  		e.printStackTrace();
	  	}
	  	
	  	return p;
	}

   

}
