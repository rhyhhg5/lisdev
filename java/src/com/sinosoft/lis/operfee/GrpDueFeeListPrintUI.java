package com.sinosoft.lis.operfee;

import com.sinosoft.utility.CErrors;
import com.sinosoft.utility.VData;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.GlobalInput;


/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GrpDueFeeListPrintUI {
    /**错误的容器*/
    public CErrors mErrors = null;
    private VData mResult = null;

    public GrpDueFeeListPrintUI() {
    }

    /**
     *
     * @param args VData 需包含LGEdorApp(需受理号EdorAcceptNO即可)、GlobalIiput
     * @param args String 操作方式
     * @return boolean
     */
    public boolean submitData(VData cInputData, String cOperate) {
        GrpDueFeeListPrintBL bl = new GrpDueFeeListPrintBL();

        if (!bl.submitData(cInputData, cOperate)) {
            mErrors.copyAllErrors(bl.mErrors);
            return false;
        }
        mResult = bl.getResult();

        return true;
    }

    public VData getResult() {
        return this.mResult;
    }

    public static void main(String[] args) {
        GrpDueFeeListPrintUI p = new GrpDueFeeListPrintUI();
        GlobalInput tGI = new GlobalInput();
        tGI.ComCode = "86";
        tGI.Operator = "001";
        tGI.ManageCom = "86";

        TransferData tTransferData = new TransferData();
        String tInNoticeNo = "'98000013707','98000013710','98000013711'";
        tTransferData.setNameAndValue("InNoticeNo", tInNoticeNo);
        VData tVData = new VData();
        tVData.addElement(tGI);
        tVData.addElement(tTransferData);
        p.submitData(tVData,"PRINT");

    }

}
