package com.sinosoft.lis.operfee;

import com.sinosoft.lis.operfee.*;
import com.sinosoft.lis.f1print.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vschema.*;
import com.sinosoft.utility.*;
import com.sinosoft.lis.pubfun.*;
import java.util.*;
import java.lang.String;
import com.sinosoft.lis.bq.AppAcc;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GrpDueFeeBackUI {
    public CErrors mCErrors = new CErrors();
    private GrpDueFeeBackBL tGrpDueFeeBackBL = new GrpDueFeeBackBL();
    public GrpDueFeeBackUI() {
    }
    public boolean submitData(VData cInputData, String cOperate){
       if(!tGrpDueFeeBackBL.submitData(cInputData,cOperate))
       {
           mCErrors.copyAllErrors(tGrpDueFeeBackBL.mErrors);
           return false;
       }
       else
           return true;
    }
    public static void main(String[] arg)
    {
        GrpDueFeeBackUI test = new GrpDueFeeBackUI();
        VData tVData = new VData();
        LJSPayBSchema tLJSPayBSchema = new LJSPayBSchema();
        tLJSPayBSchema.setGetNoticeNo(arg[0]);
        tVData.add(0,tLJSPayBSchema);
        GlobalInput tG = new GlobalInput();
        tG.Operator = "qulq";
        tG.ManageCom = "86";
        tVData.add(1,tG);
        test.submitData(tVData,"");
        System.out.println(test.mCErrors.getErrContent());
    }

}
