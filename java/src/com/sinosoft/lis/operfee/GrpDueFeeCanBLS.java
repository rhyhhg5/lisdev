package com.sinosoft.lis.operfee;

import com.sinosoft.lis.bl.*;
import com.sinosoft.lis.db.*;
import com.sinosoft.lis.schema.*;
import com.sinosoft.lis.vbl.*;
import com.sinosoft.lis.vdb.*;
import com.sinosoft.lis.vschema.*;

import com.sinosoft.utility.*;
import com.sinosoft.utility.JdbcUrl;
import com.sinosoft.utility.VData;

import java.sql.*;

public class GrpDueFeeCanBLS
{
    public CErrors mErrors = new CErrors();
    private String mOperate;

    public GrpDueFeeCanBLS()
    {
    }

    public static void main(String[] args)
    {
    }

    public boolean submitData(VData cInputData, String cOperate)
    {
        boolean tReturn = false;
        this.mOperate = cOperate;

        if (this.mOperate.equalsIgnoreCase("DELETE"))
        {
            tReturn = delete(cInputData);
        }

        return tReturn;
    }

    private boolean delete(VData cInputData)
    {
        boolean tReturn = true;
        System.out.println("Start Delete...");

        Connection conn = DBConnPool.getConnection();

        if (conn == null)
        {
            CError.buildErr(this, "���ݿ�����ʧ��!");

            return false;
        }

        try
        {
            conn.setAutoCommit(false);

            LJSPayGrpDBSet tLJSPayGrpDBSet = new LJSPayGrpDBSet(conn);
            tLJSPayGrpDBSet.set((LJSPayGrpSet) cInputData.getObjectByObjectName("LJSPayGrpSet",
                                                                                0));

            if (!tLJSPayGrpDBSet.delete())
            {
                CError.buildErr(this, "LJSPayGrpɾ��ʧ��!");
                conn.rollback();
                conn.close();
                return false;
            }

            LJSPayPersonDBSet tLJSPayPersonDBSet = new LJSPayPersonDBSet(conn);
            tLJSPayPersonDBSet.set((LJSPayPersonSet) cInputData.getObjectByObjectName("LJSPayPersonSet",
                                                                                      0));

            if (!tLJSPayPersonDBSet.delete())
            {
                CError.buildErr(this, "LJSPayPersonɾ��ʧ��!");
                conn.rollback();
                conn.close();

                return false;
            }

            LJSPayDB tLJSPayDB = new LJSPayDB(conn);
            tLJSPayDB.setSchema((LJSPaySchema) cInputData.getObjectByObjectName("LJSPaySchema",0));

            if (!tLJSPayDB.delete())
            {
                CError.buildErr(this, "LJSPayɾ��ʧ��!");
                conn.rollback();
                conn.close();
                return false;
            }

            LOPRTManagerDBSet tLOPRTManagerDBSet = new LOPRTManagerDBSet(conn);
            tLOPRTManagerDBSet.set((LOPRTManagerSet) cInputData.getObjectByObjectName("LOPRTManagerSet",0));

            if (!tLOPRTManagerDBSet.delete())
            {
                CError.buildErr(this, "LOPRTManagerɾ��ʧ��!");
                conn.rollback();
                conn.close();
                return false;
            }

            conn.commit();
            conn.close();
            System.out.println("commit end");
        }
        catch (Exception ex)
        {
            // @@������
            CError.buildErr(this, ex.toString());

            try
            {
                conn.rollback();
            }
            catch (Exception e)
            {
            }

            tReturn = false;
        }

        return tReturn;
    }
}
